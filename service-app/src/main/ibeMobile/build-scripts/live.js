var connect  = require('connect');
var static = require('serve-static');

var url = require('url');
var proxy = require('proxy-middleware');


var server = connect();
server.use('/service-app/controller', proxy(url.parse('http://10.20.11.82:8180/service-app/controller')));


server.use(  static('./'));

server.listen(3000);

livereload = require('livereload');
server = livereload.createServer();
server.watch("./");
