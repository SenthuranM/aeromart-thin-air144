(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .factory('paymentFactory', paymentFactory);

    function paymentFactory() {
        var PaymentFactory = {};
        PaymentFactory.getCardDetail = function () {
            return {
                'cardType': 0,
                'paymentAmount': 0,
                'expiryDate': '',
                'cardNo': '',
                'cardHoldersName': '',
                'cardCvv': ''
            };
        };

        PaymentFactory.getExtCardDetail = function () {
            return {
                'cardType': 0,
                'paymentAmount': 0
            };
        };
        return PaymentFactory;
    }
})();
