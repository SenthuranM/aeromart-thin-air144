(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('paymentAdaptor', [paymentAdapter]);

    function paymentAdapter() {

        this.adaptPaymentOptions = function (paymentOptionsRS) {

            var paymentOptResults = {}; // target response
            var paymentOptions = [];
            paymentOptResults.hasPromoOption = paymentOptionsRS.hasPromoOption;
            paymentOptResults.binPromotionDetails = paymentOptionsRS.binPromotionDetails;
            if (paymentOptionsRS.paymentOptions !== null) {
                var paymentGateways = paymentOptionsRS.paymentOptions.paymentGateways;

                if (paymentGateways) {
                    for (var i = 0; i < paymentGateways.length; i++) {

                        paymentOptResults.totalAmount = paymentOptionsRS.totalAmount;
                        // set PG details
                        var resultPG = {};
                        resultPG.gatewayId = paymentGateways[i].gatewayId;
                        resultPG.providerName = paymentGateways[i].providerName;
                        resultPG.paymentCurrency = paymentGateways[i].paymentCurrency;
                        resultPG.providerCode = paymentGateways[i].providerCode;
                        resultPG.description = paymentGateways[i].description;
                        resultPG.brokerType = paymentGateways[i].brokerType;
                        resultPG.switchToExternalUrl = paymentGateways[i].switchToExternalUrl;
                        resultPG.viewPaymentInIframe = paymentGateways[i].viewPaymentInIframe;
                        resultPG.effectivePaymentAmount = paymentGateways[i].effectivePaymentAmount;
                        resultPG.transactionFee = paymentGateways[i].transactionFee;

                        var cards = paymentGateways[i].cards;

                        for (var j = 0; j < cards.length; j++) {

                            var resultPayOpt = {};
                            resultPayOpt.paymentGateway = resultPG;
                            resultPayOpt.cardType = cards[j].cardType;
                            resultPayOpt.cssClass = cards[j].cssClassName;
                            resultPayOpt.cardName = cards[j].cardName;
                            resultPayOpt.displayName = cards[j].displayName;
                            resultPayOpt.cardNo = cards[j].cardNo;
                            resultPayOpt.expiryDate = cards[j].expiryDate;
                            resultPayOpt.cardHoldersName = cards[j].cardHoldersName;
                            resultPayOpt.paymentAmount = cards[j].paymentAmount;
                            resultPayOpt.cardCvv = cards[j].cardCvv;
                            paymentOptions.push(resultPayOpt);
                        }
                    }
                }

                if (paymentOptionsRS.paymentOptions.cash !== null) {
                    var cashOption = {};
                    cashOption.cardType = 0;
                    cashOption.cardName = 'Cash';
                    cashOption.cssClass = 'CASH';
                    cashOption.displayName = 'Cash Payment';
                    paymentOptions.push(cashOption);
                }

                paymentOptResults.paymentOptions = paymentOptions;
                paymentOptResults.cash = paymentOptionsRS.paymentOptions.cash;
                paymentOptResults.cash = paymentOptionsRS.paymentOptions.cash;

                if (paymentOptionsRS.paymentOptions.lmsCredits !== null) {
                    var lmsOptions = {};
                    lmsOptions.airRewardId = paymentOptionsRS.paymentOptions.lmsCredits.airRewardId;

                    if (paymentOptionsRS.paymentOptions.lmsCredits.redeemed) {
                        lmsOptions.redeemedAmount = paymentOptionsRS.paymentOptions.lmsCredits.redeemedAmount;
                        lmsOptions.lmsRedeemed = paymentOptionsRS.paymentOptions.lmsCredits.redeemed;
                    } else {
                        lmsOptions.availablePoints = paymentOptionsRS.paymentOptions.lmsCredits.availablePoints;
                        lmsOptions.equivalentAmount =
                            paymentOptionsRS.paymentOptions.lmsCredits.availablePointsAmount;
                        lmsOptions.redeemableAmount =
                            paymentOptionsRS.paymentOptions.lmsCredits.maxRedeemableAmount;
                    }

                    paymentOptResults.lmsOptions = lmsOptions;
                    paymentOptResults.lmsCredits = paymentOptionsRS.paymentOptions.lmsCredits;

                }
                paymentOptResults.success = paymentOptionsRS.success;
            }
            return paymentOptResults;

        };
    }

})(angular);
