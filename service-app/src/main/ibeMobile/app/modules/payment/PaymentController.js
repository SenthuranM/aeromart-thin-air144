(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('PaymentCtrl', [
            'paymentService',
            'paymentFactory',
            'remoteService',
            '$window',
            '$sce',
            PaymentController]);

    function PaymentController(paymentService, paymentFactory, remoteService, $window, $sce) {
        var self = this;
        var binPromoData = null;
        var selectedPaymentOptionbeforeBinPromo;

        self.paymentSummary = {};
        self.paymentOptions = {};

        var path = '/ancillary/availability/reservation/create';
        var params = paymentService.getAncillaryDetails();

        remoteService.post(path, params).then(function () {
            paymentService.setAvailableAncillaryData(function success(response) {
                console.log(response);
                paymentService.getPaymentSummary(function (response) {
                    // Set payment summary
                    self.paymentSummary = response;
                    paymentService.setTransactionId(response.transactionId);

                    paymentService.getPaymentOptions(function (response) {
                        // Set payment options
                        self.paymentOptions = response.paymentOptions;

                        // Set values required for selecting payment option

                        self.payableAmount = response.totalAmount;
                        self.onHoldStr = response.cash && response.cash.releaseTimeDisplay;
                        self.cashTotal = response.cash && response.cash.payableAmount;
                        self.totalAmount = response.totalAmount;
                        self.airewards = response.lmsOptions;
                        self.hasPromoOption = response.hasPromoOption;
                        //
                        //self.selectedPaymentOptionFlag = self.paymentOptions[0].cardType + '_' +
                        //    self.paymentOptions[0].paymentGateway.gatewayId;
                    });
                });
            });
        });

        self.additionalDetails = {};

        self.setPayOption = function () {
            //set on hold String
            self.selectedPaymentOption = getSelectedPayoption();
            self.paymentDisplayAmounts = [];

            var effectivePayment = {};

            var tpg = self.selectedPaymentOption,
                displayFlg = true;

            if (tpg !== null) {

                //check if paymentCurrency equals to selected or baseCurrency
                if (tpg.paymentGateway !== undefined) {
                    //TODO: Remove hard coded value
                    if (tpg.paymentGateway.paymentCurrency !== 'USD') {
                        displayFlg = false;
                    }
                }

                if (tpg.cssClass.toLowerCase() === 'cash') {
                    effectivePayment.transactionFee = 0;

                    //TODO: Remove hard coded value
                    effectivePayment.currencyType = 'USD';
                    effectivePayment.effectiveAmount = self.cashTotal;
                    paymentService.setTotalAmountToPay(self.payableAmount);
                    self.paymentDisplayAmounts.push(effectivePayment);

                    // Update the payment summary and
                    //updatePaymentSummary({cashPayment: true});
                }
                else {
                    var effAmountObj = tpg.paymentGateway.effectivePaymentAmount;
                    var trasFree = tpg.paymentGateway.transactionFee;
                    // TODO: Check weather this is required
                    paymentService.setTotalAmountToPay(effAmountObj.baseCurrencyValue);

                    //set Base Currency Values
                    effectivePayment.transactionFee = trasFree.baseCurrencyValue;
                    effectivePayment.effectiveAmount = effAmountObj.baseCurrencyValue;

                    // TODO: Remove hard coded value
                    effectivePayment.currencyType = 'USD';

                    if (tpg.paymentGateway.brokerType === 'external') {

                        self.cardInfo = paymentFactory.getExtCardDetail();
                        if (tpg.cssClass.toLowerCase() === 'payfort_paystore') {
                            self.cardInfo.additionalDetails = self.additionalDetails;
                        }
                        if (tpg.cssClass.toLowerCase() === 'tap_offline') {
                            self.cardInfo.additionalDetails = self.additionalDetails;
                        }
                        if (tpg.cssClass.toLowerCase() === 'qiwi' || tpg.cssClass.toLowerCase() === 'qiwi_offline') {
                            self.cardInfo.additionalDetails = self.additionalDetails;
                        }
                    }
                    else {
                        self.cardInfo = paymentFactory.getCardDetail();
                        if (self.binPromotion) {
                            self.cardInfo.cardNo = tpg.cardNo;
                        }

                        self.isBinPromotionEnabled = true;
                    }
                    self.paymentDisplayAmounts.push(effectivePayment);

                    if (!displayFlg) {
                        //set Payment Currency Values
                        var effectivePaymentObj = {};
                        effectivePaymentObj.transactionFee = trasFree.paymentCurrencyValue;
                        effectivePaymentObj.effectiveAmount = effAmountObj.paymentCurrencyValue;
                        effectivePaymentObj.isPaymentCurrency = true;
                        effectivePaymentObj.currencyType = tpg.paymentGateway.paymentCurrency;
                        self.paymentDisplayAmounts.push(effectivePaymentObj);
                    }

                    // Update the payment summary if the previous payment method was cash
                    if (self.previousSelectionCash) {
                        //updatePaymentSummary({cashPayment: false});
                    }
                }

            } // if (tpg !== null)

            if (self.paymentDisplayAmounts.effectiveAmount == 0) {
                self.payOnlyFromLMS = true;
            }
        };

        self.continuePayment = function () {
            if (self.payOnlyFromLMS || checkCardValidation()) {
                self.setExpireDate();
                paymentService.setPaymentGateWayDetails(self.selectedPaymentOption);
                paymentService.setCardInfo(self.cardInfo);
                var param = paymentService.createPaymentParams(self.payOnlyFromLMS);
                param.captcha = self.additionalDetails.captcha;
                param.binPromotion = self.binPromotion;

                // IBEDataConsole.log(param);
                // loadingService.showPageLoading();

                self.paymentRetry = false;

                paymentService.submitPayment(param, function (response) {
                        createFormSubmit(response.data.externalPGDetails);
                        //commonLocalStorageService.setData('APP_MODE', applicationService.getApplicationMode());
                        //commonLocalStorageService.setData('langTOStore', languageService.getSelectedLanguage());
                        //commonLocalStorageService.setData('CurrencyToStore', currencyService.getSelectedCurrency());

                        /*                        if (response.paymentStatus === paymentService.PaymentStatus.SUCCESS ||
                         response.paymentStatus === paymentService.PaymentStatus.ALREADY_COMPLETED) {//for payment status SUCCESS
                         if (response.actionStatus === paymentService.ActionStatus.BOOKING_CREATION_SUCCESS ||
                         response.actionStatus === paymentService.ActionStatus.BOOKING_MODIFICATION_SUCCESS) {
                         if (response.pnr !== undefined) {
                         commonLocalStorageService.setData('PNR', response.pnr);
                         commonLocalStorageService.setData('PNR_lastName', response.contactLastName);
                         commonLocalStorageService.setData('PNR_firstDepartureDate', response.firstDepartureDate);

                         $state.go('confirm', {
                         lang: languageService.getSelectedLanguage(),
                         currency: currencyService.getSelectedCurrency(),
                         mode: applicationService.getApplicationMode()
                         });
                         }
                         }
                         } else if (response.paymentStatus === paymentService.PaymentStatus.PENDING) {//for payment status PENDING
                         if (response.actionStatus === paymentService.ActionStatus.REDIRET_EXTERNAL) {//for REDIRET_EXTERNAL
                         createFormSubmit(response.externalPGDetails);
                         commonLocalStorageService.setData('APP_MODE', applicationService.getApplicationMode());
                         commonLocalStorageService.setData('langTOStore', languageService.getSelectedLanguage());
                         commonLocalStorageService.setData('CurrencyToStore', currencyService.getSelectedCurrency());
                         }
                         if (response.actionStatus === paymentService.ActionStatus.ONHOLD_CREATED_CASH ||
                         response.actionStatus === paymentService.ActionStatus.ONHOLD_CREATED_OFFLINE) {//for cash or offline
                         if (response.pnr !== undefined) {
                         commonLocalStorageService.setData('resObject', response);
                         $state.go('confirm_onhold', {
                         lang: languageService.getSelectedLanguage(),
                         currency: currencyService.getSelectedCurrency(),
                         mode: applicationService.getApplicationMode()
                         });
                         }
                         }

                         } else if (response.paymentStatus === paymentService.PaymentStatus.ERROR) {//for payment status ERROR
                         var alertObj = {};
                         alertObj.msg = 'Payment Failed';
                         alertObj.type = 'error';
                         alertService.setAlert(alertObj);
                         } else {
                         var alertObj = {};
                         alertObj.msg = 'Payment Failed';
                         alertObj.type = 'error';
                         alertService.setAlert(alertObj);
                         }*/
                    },
                    function (error) {
                        if (error.errorType === 'PAYMENT_ERROR') {
                            self.paymentRetry = true;
                            self.paymentRetryMsg = $sce.trustAsHtml(error.error);
                            //loadingService.hidePageLoading();
                        } else {
                            //alertService.setPageAlertMessage(error);
                        }
                    });
            } else {
                self.paymentCard.$setSubmitted();
            }
        };

        function createFormSubmit(extPG) {
            var htmlStr = '';
            if (extPG.requestMethod === 'POST') {
                htmlStr = extPG.postInputData;
                angular.element('#dummyForm').append(htmlStr);

                if (angular.element('#dummyForm').find('form').length > -1) {
                    if (angular.element('#dummyForm').find('form')[0].name !== undefined) {
                        var formName = angular.element('#dummyForm').find('form')[0].getAttribute('name');
                        document.forms[formName].submit();
                    } else if (angular.element('#dummyForm').find('form')[0].id !== undefined) {
                        var formID = angular.element('#dummyForm').find('form')[0].name;
                        document.getElementById(formID).submit();
                    }
                }

            } else if (extPG.requestMethod === 'GET') {
                htmlStr = extPG.redirectionURL;
                $window.location = htmlStr;
            }
        }

        function getSelectedPayoption() {
            var toSend = null;
            for (var i = 0; i < self.paymentOptions.length; i++) {
                var option = self.paymentOptions[i];
                var optionPg = (option.paymentGateway) ? option.paymentGateway.gatewayId : '';
                if (self.selectedPaymentOptionFlag !== null) {
                    if ((option.cardType + '_' + optionPg) === self.selectedPaymentOptionFlag) {
                        if (self.binPromotion) {
                            selectedPaymentOptionbeforeBinPromo = angular.copy(option);
                            option.paymentGateway.effectivePaymentAmount = binPromoData.effectivePaymentAmount;
                            option.paymentGateway.transactionFee = binPromoData.transactionFee;
                            //setCard info to the gateway
                            option.cardNo = self.cardInfo.cardNo;
                            option.cardHoldersName = self.cardInfo.cardHoldersName;
                            option.cardCvv = self.cardInfo.cardHoldersName;
                            option.binPromotionApplied = self.binPromoModalStatus.promotionApplied;
                            toSend = option;
                        } else {
                            toSend = selectedPaymentOptionbeforeBinPromo || option;
                        }
                        break;
                    }
                }
            }
            return toSend;
        }

        function checkCardValidation() {
            var toSend = true;
            if (self.selectedPaymentOption.cssClass === 'CASH') {
                return toSend;
            }
            if (!self.selectedPaymentOption.paymentGateway.switchToExternalUrl) {
                if (self.selectedPaymentOption.cssClass === 'VISA' ||
                    self.selectedPaymentOption.cssClass === 'MASTER') {
                    if (!self.paymentCard.$valid) {
                        toSend = false;
                    }
                }
            }

            return toSend;
        }

        self.setExpireDate = function () {
            var expDate = moment([self.expDateYear, parseInt(self.expDateMonth) - 1, 1]);
            self.cardInfo.expiryDate = expDate.format('YYYY-MM-DDTHH:mm:ss');
        };
    }

})();
