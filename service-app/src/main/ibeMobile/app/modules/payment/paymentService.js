(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('paymentService', [
            'remoteService',
            'passengerService',
            'paymentAdaptor',
            'paymentFactory',
            'applicationService', paymentService]);

    function paymentService(remoteService, passengerService, paymentAdaptor, paymentFactory, applicationService) {
        var self = this;
        var passengerType = {
            'Adult': 'AD',
            'Child': 'CH',
            'Infant': 'IN'
        };

        self.getPaymentOptions = function (successFn) {
            var path = '/payment/paymentOptions';
            var params = getPaymentOptionParams();

            remoteService
                .post(path, params)
                .success(function (response) {
                    var adaptedPayment = paymentAdaptor.adaptPaymentOptions(response);
                    successFn(adaptedPayment);
                });
        };

        self.getPaymentSummary = function (successFn) {
            var path = '/payment/create/paymentSummary';
            var params = getPaymentSummaryParams();

            remoteService
                .post(path, params)
                .success(function (response) {
                    successFn(response);
                });
        };

        function getPaymentOptionParams() {
            return {
                binPromotion: false,
                originCountryCode: 'LK',
                paxInfo: getAdaptedPassengers(),
                reservationContact: passengerService.getContactDetails()
            };
        }

        function getPaymentSummaryParams() {
            return {
                binPromo: false,
                onhold: false,
                paxInfo: getAdaptedPassengers()
            };
        }

        // Adapt passenger to the format that is required by the payment requests
        function getAdaptedPassengers() {
            var passengerList = passengerService.getPassengers();

            passengerList = _.map(passengerList, function (passenger) {
                var adaptedPassenger = {};
                adaptedPassenger.title = passenger.title;
                adaptedPassenger.firstName = passenger.firstName;
                adaptedPassenger.lastName = passenger.lastName;
                adaptedPassenger.nationality = passenger.nationality;
                adaptedPassenger.dateOfBirth = passenger.dob;
                adaptedPassenger.additionalInfo = passenger.additionalInfo;
                adaptedPassenger.paxSequence = passenger.paxSequence;

                if(passenger.travelWith){
                    adaptedPassenger.travelWith = passenger.travelWith;
                }
                adaptedPassenger.paxType = passengerType[passenger.category];

                return adaptedPassenger;
            });

            return passengerList;
        }

        // Taken from service-app
        var selectedPaymentOption, payCardInfor, totalAmount, transactionId, recaptchaResponse = null;

        this.setTotalAmountToPay = function (amount) {
            totalAmount = amount;
        };

        this.setPaymentGateWayDetails = function (payOption) {
            selectedPaymentOption = payOption;
        };

        this.getPaymentGateWayDetails = function () {
            return formatPaymentOption(selectedPaymentOption);
        };

        this.setCardInfo = function (cardInfo) {
            payCardInfor = cardInfo;
        };

        this.createPaymentParams = function (lmsOnly) {
            var params = {};

            // TODO: Add applicationService
            //params.paymode = applicationService.getApplicationMode();

            if (lmsOnly) {
                params.makePaymentRQ = {
                    'transactionId': transactionId,
                    'bookingType': null,
                    'paymentOptions': null,
                    'captchaResponse': null,
                    'binPromotion': false
                };
            } else {
                params.makePaymentRQ = self.getPaymentGateWayDetails();
            }

            //TODO: Make dynamic depending on the application
            params.makePaymentRQ.mobile = true;

            self.getPaymentGateWayDetails = function () {
                return formatPaymentOption(selectedPaymentOption);
            };

            //TODO: Add applicationService
            if (false && (applicationService.getApplicationMode() === applicationService.APP_MODES.BALANCE_PAYMENT ||
                applicationService.getApplicationMode() === applicationService.APP_MODES.MODIFY_RESERVATION ||
                applicationService.getApplicationMode() === applicationService.APP_MODES.NAME_CHANGE ||
                applicationService.getApplicationMode() === applicationService.APP_MODES.MODIFY_SEGMENT ||
                applicationService.getApplicationMode() === applicationService.APP_MODES.ADD_MODIFY_ANCI)) {
                //params.rqParams = modifyReservationService.getReservationSearchCriteria();
            } else {
                params.reservationContact = passengerService.getContactDetails();
                params.paxInfo = getAdaptedPassengers();
            }
            return params;
        };

        this.setTransactionId = function (id) {
            transactionId = id;
        };

        this.setAvailableAncillaryData = function (success) {
            var path = '/ancillary/set/reservation/create';

            var selectedAncillaryData = {
                ancillaries: [],
                metaData: {
                    ondPreferences: [],
                    monetaryAmendmentDefinitions: []
                }
            };

            remoteService.post(path, selectedAncillaryData).then(function (response) {
                success(response);
            });
        };

        this.getAncillaryDetails = function () {
            var passengerList = getAdaptedPassengers();

            _.map(passengerList, function(passenger){
                passenger.passengerRph = passenger.paxSequence;
                passenger.type = passenger.paxType;

                return passenger;
            });

            return {
                'reservationData': {
                    'mode': 'CREATE',
                    'passengers': passengerList,
                    'contactInfo': passengerService.getContactDetails()
                },
                'ancillaries': [{'type': 'SEAT'}]
            };

        };

        this.submitPayment = function (requestParams, successFn) {
            var path = '';
            var bookingRQ = {};

            /*        // for balance payment submit payment
             if (requestParams.paymode === applicationService.APP_MODES.BALANCE_PAYMENT) {
             bookingRQ = requestParams.makePaymentRQ;
             bookingRQ.pnr = requestParams.rqParams.pnr;
             path = '/payment/balance/booking/makePayment';
             }
             // for modify reservation submit payment
             else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_RESERVATION) {
             bookingRQ = requestParams.makePaymentRQ;
             bookingRQ.pnr = requestParams.rqParams.pnr;
             path = '/payment/requote/booking/makePayment';
             } else if (requestParams.paymode === applicationService.APP_MODES.NAME_CHANGE) {
             bookingRQ = requestParams.makePaymentRQ;
             bookingRQ.pnr = requestParams.rqParams.pnr;
             path = '/payment/requote/booking/makePayment';
             }
             // for add modify anci submit payment
             else if (requestParams.paymode === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
             path = '/payment/anci/modification/makePayment';
             bookingRQ = requestParams.makePaymentRQ;
             bookingRQ.pnr = requestParams.rqParams.pnr;
             }
             // for add modify segment submit payment
             else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_SEGMENT) {
             bookingRQ = requestParams.makePaymentRQ;
             bookingRQ.pnr = requestParams.rqParams.pnr;
             path = '/payment/requote/booking/makePayment';
             }
             // for create flow submit payment
             else if (requestParams.paymode === applicationService.APP_MODES.CREATE_RESERVATION) {*/
            var reservationContact = {
                'title': requestParams.reservationContact.salutation,
                'firstName': requestParams.reservationContact.firstName,
                'lastName': requestParams.reservationContact.lastName,
                'nationality': requestParams.reservationContact.nationality,
                'country': requestParams.reservationContact.country,
                'preferredLangauge': 'en',
                'emailAddress': requestParams.reservationContact.emailAddress,
                'address': {
                    'streetAddress2': null,
                    'zipCode': null
                },
                'mobileNumber': requestParams.reservationContact.mobileNo,
                'telephoneNumber': requestParams.reservationContact.mobileNoTravel,
                'fax': null,
                'zipCode': null
            };

            path = '/reservation/create';
            bookingRQ.makePaymentRQ = requestParams.makePaymentRQ;
            bookingRQ.paxInfo = requestParams.paxInfo;
            bookingRQ.reservationContact = reservationContact;
            bookingRQ.captcha = requestParams.captcha;
            /*        }
             loadingService.showLoading();*/
            remoteService.post(path, bookingRQ).then(function (value) {
                successFn(value);
            });
            /*        $http({
            'method': 'POST',
            'url': this._basePath + path,
            'data': bookingRQ
            }).success(function (response) {
            successFn(response);
            }).error(function (response) {
            errorFn(response);
            });*/
        };

        this.paymentReceipt = function (requestParams, successFn, errorFn) {
            var path = '';

            // for balance payment submit payment
            if (requestParams.paymode === applicationService.APP_MODES.BALANCE_PAYMENT) {
                path = '/payment/receipt/confirmation/create';
            }
            // for modify reservation submit payment
            else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_RESERVATION) {
                path = '/payment/receipt/confirmation/modify';
            } else if (requestParams.paymode === applicationService.APP_MODES.NAME_CHANGE) {
                path = '/payment/receipt/confirmation/modify';
            }
            // for add modify anci submit payment
            else if (requestParams.paymode === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                path = '/payment/receipt/confirmation/ancillary';
            }
            // for add modify segment submit payment
            else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_SEGMENT) {
                path = '/payment/receipt/confirmation/modify';
            }
            // for create flow submit payment
            else if (requestParams.paymode === applicationService.APP_MODES.CREATE_RESERVATION) {
                path = '/payment/receipt/confirmation/create';
            }

            remoteService
                .post(path, requestParams)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (error) {
                    errorFn(error);
                });
        };

        function formatPaymentOption(option) {
            var bookingType = null, payGateway = null, paymentOption, additionalDetails = null, cardInfo = null;
            var binPromotionApplied = (option.binPromotionApplied != undefined) ? option.binPromotionApplied : false;
            if (option.cssClass.toLowerCase() === 'cash') {
                cardInfo = null;
                bookingType = 'ONHOLD';
                payGateway = null;
                paymentOption = null;
            } else {
                payGateway = option.paymentGateway.gatewayId;
                if (option.paymentGateway.brokerType === 'external') {
                    cardInfo = paymentFactory.getExtCardDetail();
                    cardInfo.cardType = option.cardType;
                    cardInfo.paymentAmount = totalAmount;
                    if (option.cssClass.toLowerCase() === 'payfort_paystore') {
                        additionalDetails = payCardInfor.additionalDetails;
                    }
                    if (option.cssClass.toLowerCase() === 'tap_offline') {
                        additionalDetails = payCardInfor.additionalDetails;
                    }
                    if (option.cssClass.toLowerCase() === 'qiwi' || option.cssClass.toLowerCase() === 'qiwi_offline') {
                        additionalDetails = payCardInfor.additionalDetails;
                    }
                } else {
                    cardInfo = paymentFactory.getCardDetail();
                    cardInfo.cardType = option.cardType;
                    cardInfo.paymentAmount = totalAmount;
                    cardInfo.expiryDate = payCardInfor.expiryDate;
                    cardInfo.cardNo = payCardInfor.cardNo;
                    cardInfo.cardHoldersName = payCardInfor.cardHoldersName;
                    cardInfo.cardCvv = payCardInfor.cardCvv;
                }
                paymentOption = {
                    'paymentGateways': [{
                        'gatewayId': payGateway,
                        'cards': [cardInfo]
                    }],
                    'additionalDetails': additionalDetails
                }; // payment option
            } // end if

            return {
                'transactionId': transactionId,
                'bookingType': bookingType,
                'paymentOptions': paymentOption,
                'captchaResponse': recaptchaResponse,
                'binPromotion': binPromotionApplied
            };
        }
    }
})();
