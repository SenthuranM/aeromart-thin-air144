(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('passengerService',
            ['passengerFactory', 'commonLocalStorageService', 'remoteService', passengerService]);

    function passengerService(passengerFactory, commonLocalStorageService, remoteService) {
        var self = this;

        var ageRules = {
            Adult: {
                min: 12,
                max: 99
            },
            Child: {
                min: 2,
                max: 12
            },
            Infant: {
                min: 0,
                max: 2
            }
        };

        self.passengers = [];
        self.contactPassenger = null;
        self.contactInfo = null;

        self.getPassengers = function () {

            var paxSeq = 1,
                passengers = [],
                i;

            if (commonLocalStorageService.getPassengerDetails()) {
                self.passengers = commonLocalStorageService.getPassengerDetails();
            }

            if (self.passengers.length === 0) {
                var searchCriteria = commonLocalStorageService.getAvailabilityParams();

                for (i = 0; i < searchCriteria.adult; i++) {
                    passengers.push(passengerFactory.PassengerDTO('Adult', paxSeq++));
                }
                for (i = 0; i < searchCriteria.child; i++) {
                    passengers.push(passengerFactory.PassengerDTO('Child', paxSeq++));
                }
                for (i = 0; i < searchCriteria.infant; i++) {
                    passengers.push(passengerFactory.PassengerDTO('Infant', paxSeq++));
                }
            } else {
                passengers = self.passengers;
            }

            return passengers;
        };

        self.setPassengers = function (passengers, contactPassenger) {
            commonLocalStorageService.setPassengerDetails(passengers);

            if (contactPassenger) {
                self.contactPassenger = contactPassenger;
                commonLocalStorageService.setContactPassenger(contactPassenger);
            }
        };

        self.setContactDetails = function (contactInfo) {
            commonLocalStorageService.setContactDetails(contactInfo);
        };

        self.getContactDetails = function () {
            if (commonLocalStorageService.getContactDetails()) {
                self.contactInfo = commonLocalStorageService.getContactDetails();
            }

            if (self.contactInfo) {
                return self.contactInfo;
            } else {
                return passengerFactory.ContactDTO();
            }
        };

        // Get the passenger details of the selected contact person from the Personal information page
        self.getContactPassenger = function () {

            if (!self.contactPassenger) {
                self.contactPassenger = commonLocalStorageService.getContactPassenger();
            }

            return self.contactPassenger;
        };

        self.getAgeRules = function (passengerCategory) {

            var ageRulesForPax = ageRules[passengerCategory];

            var maxDate = moment().subtract(ageRulesForPax.min, 'years');

            // Min date is reduced by one day
            var minDate = moment().subtract(ageRulesForPax.max, 'years');
            maxDate = maxDate.subtract(1, 'days');

            if (passengerCategory === 'Infant') {
                maxDate = moment().subtract(3, 'days');

                // Max dob of infant must always be on or before the day that the booking is done
                if (moment() < maxDate) {
                    maxDate = moment();
                }
            }

            return {
                minDate: minDate.format('YYYY-MM-DD'),
                maxDate: maxDate.format('YYYY-MM-DD')
            };
        };

        self.duplicateCheck = function (successFn) {
            var path = '/passenger/duplicateCheck/';

            remoteService
                .post(path, {duplicateCheckList: self.passengers})
                .success(function (response) {
                    successFn(response);
                })
                .error(function (error) {
                    debugger;
                    console.log(error);
                });
        };
    }
})();
