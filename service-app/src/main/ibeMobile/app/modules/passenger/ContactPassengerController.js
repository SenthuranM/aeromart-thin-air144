(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('ContactPassengerCtrl', [
            '$state',
            '$anchorScroll',
            'passengerService',
            'masterDataService',
            'availabilityService',
            ContactPassengerController
        ]);

    function ContactPassengerController($state, $anchorScroll, passengerService, masterDataService, availabilityService) {

        // Global vars.
        var self = this;

        // Properties.
        self.passengerAsContactPerson = false;
        self.sameTravelMoble = true;
        self.selectedContactName = '';
        self.outputData = {};
        self.countryList = masterDataService.masterData.CountryList;
        self.titleList = masterDataService.masterData.dropDownLists.paxTitels;

        self.contactInfo = passengerService.getContactDetails();

        // Functions.
        self.changeTravelMobile = function () {
            self.sameTravelMoble = !self.sameTravelMoble;
        };

        self.passengerAsContact = function () {
            self.passengerAsContactPerson = !self.passengerAsContactPerson;
        };

        self.navigateToPayment = function () {
            //TODO: Duplicate check -> goToPayment
            passengerService.duplicateCheck(function (response) {
                if (!response.hasDuplicates) {
                    if (self.sameTravelMoble) {
                        self.contactInfo.mobileNoTravel = self.contactInfo.mobileNo;
                    }

                    passengerService.setContactDetails(self.contactInfo);
                    $state.go('payment');
                }
            });
        };

        self.mobileNumberChange = function () {
            var country = _.find(self.countryList, function (country) {
                return country.countryCode == self.contactInfo.country;
            });

            if (country.phoneCode) {
                self.contactInfo.mobileNo.countryCode = country.phoneCode;
            }
        };

        // IFFE
        (function () {
            $anchorScroll();
            var contactPerson = passengerService.getContactPassenger();
            if (contactPerson) {
                //TODO: Set the parameters taken from
                self.passengerAsContactPerson = true;
                self.contactInfo.firstName = contactPerson.firstName;
                self.contactInfo.lastName = contactPerson.lastName;
                self.contactInfo.salutation = contactPerson.title;
                self.contactInfo.nationality = contactPerson.nationality;

                self.selectedContactName = (contactPerson.title || '') +
                    ' ' + (contactPerson.firstName || '') +
                    ' ' + (contactPerson.lastName || '');
            }

            self.outputData = availabilityService.getAvailabilityOutputData();
        })();

    }

})();
