(function() {
    'use strict';

    angular.module('ibeMobileApp')
        .factory('passengerFactory', passengerFactory);

    function passengerFactory() {
        return {
            PassengerDTO: function (type, paxSeq) {
                var pax = new PassengerDTO();
                pax.category = type;
                pax.paxSequence = paxSeq;
                pax.type = type.toLowerCase();
                if (type === 'Adult') {
                    this.infantSequence = '';
                } else if (type === 'Infant') {
                    pax.travelWith = '';
                }
                return pax;
            },
            ContactDTO: function () {
                return new ContactDetailsDTO();
            },
            PaxRegistrationDTO: function () {
                return new PaxRegistrationDTO();
            },
            paxSignInDTO: function () {
                return new PaxSignInDTO();
            },
            LmsDetailsDTO: function () {
                return new LmsDetailsDTO();
            },
            PaxRegistrationDetailDTO: function () {
                return new PaxRegistrationDetailsDTO();
            },
            PaxContactsCongigsParamsDTO: function () {
                return new PaxContactConfigsParams();
            }
        };

        function PaxContactConfigsParams() {
            this.origin = '';
            this.destination = '';
            this.appCode = '';
            this.carriers = '';
        }

        function PaxRegistrationDetailsDTO() {
            this.emailId = '';
            this.password = '';
            this.dateOfBirth = '';
            this.confirmPassword = '';
            this.isJoinAirewards = true;
        }

        function PaxRegistrationDTO() {
            this.emailId = '';
            this.password = '';
            this.title = '';
            this.firstName = '';
            this.lastName = '';
            this.nationalityCode = '';
            this.countryCode = '';
            this.telephone = '';
            this.mobile = '';
            this.fax = '';
            this.dateOfBirth = '';
        }

        function PaxSignInDTO() {
            this.emailId = '';
            this.password = '';
        }

        function LmsDetailsDTO() {
            this.mobileNumber = '';
            this.headOFEmailId = '';
            this.refferedEmail = '';
            this.genderCode = '';
            this.dateOfBirth = '';
            this.firstName = '';
            this.lastName = '';
            this.passportNum = '';
            this.phoneNumber = '';
            this.nationalityCode = '';
            this.residencyCode = '';
            this.emailId = '';
            this.language = '';
            this.appCode = '';
        }

        function PassengerDTO() {
            this.paxSequence = null;
            this.type = null;
            this.category = null;
            this.passportNo = null;
            this.visaDocIssueDate = null;
            this.lastName = null;
            this.placeOfBirth = null;
            this.travelDocumentType = null;
            this.travelWith = null;
            this.groupId = null;
            this.visaDocNumber = null;
            this.eticketNo = null;
            this.title = null;
            this.salutation = null;
            this.passportExpiry = null;
            this.firstName = null;
            this.nationalIDNo = null;
            this.passportIssuedCntry = null;
            this.ffid = null;
            this.nationality = null;
            this.visaDocPlaceOfIssue = null;
            this.dob = null;
            this.visaApplicableCountry = null;
            this.additionalInfo = {
                foidInfo: {
                    foidNumber: null,
                    foidExpiry: null,
                    foidPlace: null,
                    placeOfBirth: null
                }
            };
        }

        function ContactDetailsDTO() {
            this.address = {};
            this.firstName = '';
            this.lastName = '';
            this.emailAddress = '';
            this.verifyEmail = '';
            this.mobileCountryCode = '';
            this.mobileNo = {countryCode: '', areaCode: '', number: ''};
            this.mobileNoTravel = {countryCode: '', areaCode: '', number: ''};

            this.salutation = '';
            this.language = '';
            this.nationality = '';
            this.country = '';

            this.travelMobile = true;

            this.password = '';
            this.confirmPassword = '';
            this.isJoinAirewards = '';
        }
    }
})();
