(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('IndexController', ['masterDataService', '$state', '$window', mainController]);

    function mainController(masterDataService, $state, $window) {
        var self = this,
            title = '';

        self.appStatus = masterDataService.appStatus;

        self.getTitle = function () {
            switch ($state.current.name) {
                case 'selectFlight':
                    title = 'Select Flight';
                    break;
                case 'availability':
                    title = 'Select Outbound Flight';
                    break;
                case 'return':
                    title = 'Select Return Flight';
                    break;
                case 'passenger':
                    title = 'Passenger Details';
                    break;
                case 'contactPassenger':
                    title = 'Contact Details';
                    break;
                case 'payment':
                    title = 'Payment';
                    break;
                case 'confirm':
                    title = 'Confirm';
                    break;
            }
            return title;
        };

        self.goBack = function () {
            $window.history.back();
        };

    }
})();
