(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('ProgressController', ['applicationService', '$state', progressController]);

    function progressController(applicationService, $state) {
        var self = this;
        var stateName;

        self.stateIndex = 0;

        self.isReturn = function() {
            return applicationService.getFlightType() == applicationService.FLIGHT_TYPES.RETURN;
        };

        self.getStateIndex = function() {
            return self.stateIndex;
        };

        (function() {
            self.stateIndex = 0;
            stateName = $state.current.name;
            switch (stateName){
                case 'return':
                    self.stateIndex = 1;
                    break;
                case 'passenger':
                    self.stateIndex = 2;
                    break;
                case 'contactPassenger':
                    self.stateIndex = 3;
                    break;
                case 'payment':
                    self.stateIndex = 5;
                    break;
            }
        }());
    }
})();
