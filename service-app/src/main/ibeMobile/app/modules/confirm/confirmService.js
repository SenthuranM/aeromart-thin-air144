(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('confirmService', [
            'remoteService',
            confirmService
        ]);

    function confirmService(remoteService) {
        var self = this,
            pnrNumber = null;

        self.getReservationData = function (params, successFn, errorFn) {
            var path = '/reservation/loadConfirmationPage';

            remoteService
                .post(path, params)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (response) {
                    errorFn(response);
                });
        };

        self.setPNR = function (pnr) {
            pnrNumber = pnr;
        };

        self.getPNR = function () {
            return pnrNumber;
        };
    }

})();
