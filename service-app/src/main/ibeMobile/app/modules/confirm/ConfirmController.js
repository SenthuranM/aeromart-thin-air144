(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('ConfirmCtrl', [
            'commonLocalStorageService',
            'applicationService',
            'confirmService',
            ConfirmController
        ]);

    function ConfirmController(commonLocalStorageService, applicationService, confirmService) {
        //stepProgressService.setCurrentStep('confirm');

        //loadingService.showPageLoading();

        // Global vars.
        var self = this;

        //var promotionPopupWaitTime = 2000; // milliseconds

        // Properties.
        //self.isPopupVisible = false;
        //self.hasDataLoadingErrorOccured = false; // Hide whole page content if initial data loading failed.
        //self.isUserLoggedIn = applicationService.getCustomerLoggedState();
        //self.paymentGatewayCurrency = currencyService.getAppDefaultPGCurrency();
        //self.baseCurrency = currencyService.getAppBaseCurrency();
        self.igp = null;

        if (commonLocalStorageService.getData('ipgResults')) {
            self.igp = commonLocalStorageService.getData('ipgResults');
            commonLocalStorageService.remove('ipgResults');
        }

        // Functions.
        // self.print = print;
        // self.sendEmail = sendEmail;
        // self.finish = finish;
        // self.getTrustedUrl = getTrustedUrl;
        // self.getFlightDirection = getFlightDirection;

        var confirmReq = {};
        confirmReq.pnr = commonLocalStorageService.getData('PNR');
        confirmReq.lastName = commonLocalStorageService.getData('PNR_lastName');
        confirmReq.departureDateTime = commonLocalStorageService.getData('PNR_firstDepartureDate');

        applicationService.setTransactionID(null);
        confirmService.getReservationData(confirmReq, function (data) {

            self.reservationSegments = data.reservationSegments;
            self.balanceSummary = data.balanceSummary;
            self.pnr = data.bookingInfoTO.pnr;
            // Setting the payment gateway currency
            /*            if(data.paymentDetails && data.paymentDetails.currency){
             self.paymentGatewayCurrency = data.paymentDetails.currency;
             }*/

            //loadingService.hidePageLoading();

            console.log(data);

            if (!data.success) {
                self.hasDataLoadingErrorOccured = true;
                console.warn('Something went wrong. Please try again.', 'danger');
            }

            /*            // Show promotion popup.
             $timeout(function () {
             self.isPopupVisible = true;
             }, promotionPopupWaitTime);

             // Set confirm flight data to null, to stop modifing
             // previous data.
             //commonLocalStorageService.setConfirmedFlightData(null);

             // Booking Details section.
             var bookingDetails = data.bookingInfoTO;
             self.reservationNumber = bookingDetails.pnr;
             self.reservationDate = moment(bookingDetails.bookingDate).format('DD-MM-YYYY');
             self.policyCode = bookingDetails.policyCode;
             self.groupPnr = bookingDetails.groupPnr;

             // Itinerary section.
             self.itinerary = data.reservationSegments;

             // Passenger Details section.
             var passengers = data.passengers;

             // Other details.
             self.isInfantAvailable = false;
             self.isChildrenAvailable = false;

             // Attach adult object for infant, set children, infant availability.
             for (var paxIndex = 0; paxIndex < passengers.length; paxIndex++) {
             if (passengers[paxIndex].paxType === 'infant') {
             self.isInfantAvailable = true;
             passengers[paxIndex].travelWithAdult = getTravelsWithPax(passengers[paxIndex].travelWith, passengers);
             }
             if (passengers[paxIndex].paxType === 'child') {
             self.isChildrenAvailable = true;
             }
             }

             self.passengers = passengers;

             var charges = data.balanceSummary.chargeBreakdown;

             // Total Charges, is sending as the last element in array.
             // using isTotal flag to set 'total' class to LI tag.
             var obj = charges[charges.length - 1];
             obj.isTotal = true;
             charges[charges.length - 1] = obj;
             self.charges = charges;

             // Contact Information section.
             var contactInfo = data.contactInfo;
             self.contactPerson = {
             title: contactInfo.title,
             firstName: contactInfo.firstName,
             lastName: contactInfo.lastName
             };
             self.nationality = contactInfo.nationality;
             self.countryOfResidence = contactInfo.country;
             self.mobilePhone = contactInfo.mobileNumber != null ? contactInfo.mobileNumber.countryCode + contactInfo.mobileNumber.areaCode + contactInfo.mobileNumber.number : '';
             self.phone = contactInfo.telephoneNumber != null ? contactInfo.telephoneNumber.countryCode + contactInfo.telephoneNumber.areaCode + contactInfo.telephoneNumber.number : '';
             self.phone = self.phone !== 0 ? self.phone : undefined; // null + null + null = 0
             self.emailAddress = contactInfo.emailAddress !== null ? contactInfo.emailAddress : undefined;
             self.itineraryLanguage = contactInfo.preferredLangauge;

             self.advertisements = data.advertisements;

             self.language = data.language;*/

        }, function (error) {
            console.log('----------error----------');
            console.log(error);
        });

        // Get adult user for infant.
        /*        function getTravelsWithPax(paxId, paxList) {
         for (var j = 0; j < paxList.length; j++) {
         if (paxList[j].paxSequence === paxId && paxList[j].paxType === 'adult') {
         return paxList[j];
         }
         }
         }*/

    }

})();
