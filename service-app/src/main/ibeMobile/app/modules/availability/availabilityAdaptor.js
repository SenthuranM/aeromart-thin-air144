(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('availabilityAdaptor', [
            'commonLocalStorageService',
            'currencyService',
            availabilityAdaptor
        ]);

    function availabilityAdaptor(commonLocalStorageService, currencyService) {

        var self = this;
        var JSON_DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss';
        var DASH_SEPERATED_DATE = 'DD/MM/YYYY';

        self.adaptAvailabilityReq = function (availRQ) {
            // TODO assumes receiving only one journeyInfo
            // in availRQ.(for single
            // sequence number)

            var availabilitySearchReq = {};
            availabilitySearchReq.travellerQuantity = {};
            availabilitySearchReq.travellerQuantity.adultCount = availRQ.passengers.adults;
            availabilitySearchReq.travellerQuantity.childCount = availRQ.passengers.children;
            availabilitySearchReq.travellerQuantity.infantCount = availRQ.passengers.infants;
            availabilitySearchReq.preferences = {};
            availabilitySearchReq.preferences.cabinClass = availRQ.cabinClass;
            availabilitySearchReq.preferences.logicalCabinClass = availRQ.cabinClass;
            availabilitySearchReq.preferences.currency = availRQ.currency;
            availabilitySearchReq.preferences.promotion = {};
            availabilitySearchReq.preferences.promotion.type = '';
            availabilitySearchReq.preferences.promotion.code = '';
            if (availRQ.promoCode) {
                availabilitySearchReq.preferences.promotion.type = 'PROMO_CODE';
                availabilitySearchReq.preferences.promotion.code = availRQ.promoCode;
            }
            availabilitySearchReq.journeyInfo = [];
            availabilitySearchReq.journeyInfo[0] = {};
            availabilitySearchReq.journeyInfo[0].origin = availRQ.origin.toUpperCase();
            availabilitySearchReq.journeyInfo[0].destination = availRQ.destination.toUpperCase();
            availabilitySearchReq.journeyInfo[0].originCity = false;
            availabilitySearchReq.journeyInfo[0].destinationCity = false;
                                              
            var diff = 0;
            var startDate = moment(availRQ.departureDate, JSON_DATE_FORMAT).subtract(3, 'days');
            if (moment(startDate, JSON_DATE_FORMAT).isBefore(moment())) {
                diff = moment().diff(moment(startDate, JSON_DATE_FORMAT), 'days');
            }
            availabilitySearchReq.journeyInfo[0].departureVariance = availRQ.departureVariance +
                diff;
            return availabilitySearchReq;

        };

        self.adaptFareClasses = function (response) {
            var fareTypeInfo = [];
            for (var i = 0; i < response.fareClasses.length; i++) {
                var fareClass = {};
                fareClass.fareTypeId = response.fareClasses[i].fareClassCode;
                fareClass.fareTypeName = response.fareClasses[i].description;
                fareClass.description = response.fareClasses[i].comment;
                fareClass.imageUrl = response.fareClasses[i].imageUrl;
                fareClass.rank = response.fareClasses[i].rank;
                fareClass.uniqueIdentification = i;
                fareClass.fareClassType = response.fareClasses[i].fareClassType;
                fareClass.fareClassId = response.fareClasses[i].fareClassId;
                fareTypeInfo.push(fareClass);
            }
            fareTypeInfo.sort(fareClassCompare);
            return fareTypeInfo;
        };

        self.adaptTimeline = function (response, departureDate, departureVariance) {
            var timelineInfo = {}, i;
            timelineInfo.success = response.success;
            timelineInfo.data = {};
            timelineInfo.data.results = [];

            for (var ond = 0; ond < response.originDestinationResponse.length; ond++) {

                var ondRes = response.originDestinationResponse[ond];
                ondRes.availableOptions.sort(sortAvailOptByDepartureDate);

                var prevDeptDate = '';
                var prevFare = 0;

                for (var avOpt = 0; avOpt < ondRes.availableOptions.length; avOpt++) {
                    var availOpt = ondRes.availableOptions[avOpt];
                    var timeLineData = {};
                    // get minimum fare value from avail fare
                    // classes
                    var availableFareClasses = availOpt.availableFareClasses;
                    var avFarePrices = [];
                    for (i = 0; i < availableFareClasses.length; i++) {
                        if (availableFareClasses[i].price != null && availableFareClasses[i].price > 0) {
                            avFarePrices.push(Math.ceil(availableFareClasses[i].price));
                        }
                    }
                    if (avFarePrices.length > 0) {
                        var sortedFareArr = avFarePrices.sort(function (prev, next) {
                            return prev - next;
                        });
                        timeLineData.fareValue = sortedFareArr[0];
                    } else {
                        timeLineData.fareValue = -1;
                    }
                    timeLineData.selected = availOpt.selected;
                    timeLineData.flights = [];
                    var flight = _adaptAvailOption(availOpt, ondRes.origin, ondRes.destination);
                    if (prevDeptDate !== '' && prevDeptDate == flight.departureDate) {
                        if (timeLineData.fareValue != -1 &&
                            (timeLineData.fareValue < prevFare || prevFare === -1)) {
                            timelineInfo.data.results[timelineInfo.data.results.length - 1].fareValue =
                                timeLineData.fareValue;
                            prevFare = timeLineData.fareValue;
                        }
                        if (timeLineData.selected) {
                            timelineInfo.data.results[timelineInfo.data.results.length - 1].selected = true;
                        }
                        timelineInfo.data.results[timelineInfo.data.results.length - 1].flights
                            .push(flight);
                    } else {
                        prevFare = timeLineData.fareValue;
                        timeLineData.flights.push(flight);
                        timeLineData.available = availOpt.seatAvailable;
                        var departureDateTime = availOpt.segments[0].departureDateTime;
                        var dateArr = departureDateTime.local.split('T')[0].split('-');
                        timeLineData.date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0];
                        prevDeptDate = timeLineData.date;
                        timelineInfo.data.results.push(timeLineData);
                    }
                }
            }
            var results = timelineInfo.data.results;
            if (departureVariance > 0 && results.length < 7) {
                var startDate = moment(departureDate, JSON_DATE_FORMAT).subtract(3, 'days');
                var diff = 0;
                if (moment(startDate, JSON_DATE_FORMAT).isBefore(moment())) {
                    diff = moment().diff(moment(startDate, JSON_DATE_FORMAT), 'days');
                }
                startDate = moment(startDate, JSON_DATE_FORMAT).add(diff, 'days');
                var endDate = moment(departureDate, JSON_DATE_FORMAT).add(3 + diff, 'days');
                var newTimeLineArr = [];
                var existTimeLineData = {};
                while (startDate <= endDate) {
                    var isExistDate = false;
                    for (i = 0; i < results.length; i++) {
                        if (moment(results[i].date, DASH_SEPERATED_DATE).isSame(startDate)) {
                            isExistDate = true;
                            existTimeLineData = results[i];
                            break;
                        }
                    }
                    if (!isExistDate) {
                        var dummyTimeLine = angular.copy(dummyTimeLineData);
                        dummyTimeLine.date = moment(startDate, JSON_DATE_FORMAT).format(
                            DASH_SEPERATED_DATE);
                        if (moment(departureDate, JSON_DATE_FORMAT).isSame(
                                moment(startDate, JSON_DATE_FORMAT))) {
                            dummyTimeLine.selected = true;
                        }
                        newTimeLineArr.push(dummyTimeLine);
                    } else {
                        newTimeLineArr.push(existTimeLineData);
                    }
                    startDate = moment(startDate, JSON_DATE_FORMAT).add(1, 'days');
                }
                timelineInfo.data.results = newTimeLineArr;
            } else if (departureVariance === 0 && results.length === 0) {
                var dummyEmptyTimeLine = angular.copy(dummyTimeLineData);
                dummyEmptyTimeLine.date = moment(departureDate, JSON_DATE_FORMAT).format(
                    DASH_SEPERATED_DATE);
                timelineInfo.data.results.push(dummyEmptyTimeLine);
            }
            _AddDateWiseFareClasses(this.adaptFareClasses(response), timelineInfo, departureDate); //
            for (var fl = 0; fl < timelineInfo.data.results.length; fl++) {
                timelineInfo.data.results[fl].flights.sort(sortDateWiseFlights);
            }
            return timelineInfo;
        };

        self.adaptTimelineForAllOnd = function (response, availRQ, departureVariance) {
            var timelineInfo = {}, i;
            timelineInfo.success = response.success;
            timelineInfo.data = [];

            for (var ond = 0; ond < response.originDestinationResponse.length; ond++) {
                timelineInfo.data[ond] = {};
                timelineInfo.data[ond].results = [];
                var ondRes = response.originDestinationResponse[ond];
                ondRes.availableOptions.sort(sortAvailOptByDepartureDate);
                var prevDeptDate = '';
                var prevFare = 0;
                for (var avOpt = 0; avOpt < ondRes.availableOptions.length; avOpt++) {
                    var availOpt = ondRes.availableOptions[avOpt];
                    var timeLineData = {};
                    // get minimum fare value from avail fare classes
                    var availableFareClasses = availOpt.availableFareClasses;
                    var avFarePrices = [];
                    for (i = 0; i < availableFareClasses.length; i++) {
                        if (availableFareClasses[i].price != null && availableFareClasses[i].price > 0) {
                            avFarePrices.push(Math.ceil(availableFareClasses[i].price));
                        }
                    }
                    if (avFarePrices.length > 0) {
                        var sortedFareArr = avFarePrices.sort(function (prev, next) {
                            return prev - next;
                        });
                        timeLineData.fareValue = sortedFareArr[0];
                    } else {
                        timeLineData.fareValue = -1;
                    }
                    timeLineData.selected = availOpt.selected;
                    timeLineData.flights = [];
                    var flight = _adaptAvailOption(availOpt, ondRes.origin, ondRes.destination);
                    if (prevDeptDate !== '' && prevDeptDate == flight.departureDate) {
                        if (timeLineData.fareValue != -1 &&
                            (timeLineData.fareValue < prevFare || prevFare === -1)) {
                            timelineInfo.data[ond].results[timelineInfo.data[ond].results.length - 1].fareValue =
                                timeLineData.fareValue;
                            prevFare = timeLineData.fareValue;
                        }
                        if (timeLineData.selected) {
                            timelineInfo.data[ond].results[timelineInfo.data[ond].results.length - 1].selected = true;
                        }
                        timelineInfo.data[ond].results[timelineInfo.data[ond].results.length - 1].flights
                            .push(flight);
                    } else {
                        prevFare = timeLineData.fareValue;
                        timeLineData.flights.push(flight);
                        timeLineData.available = availOpt.seatAvailable;
                        var departureDateTime = availOpt.segments[0].departureDateTime;
                        var dateArr = departureDateTime.local.split('T')[0].split('-');
                        timeLineData.date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0];
                        prevDeptDate = timeLineData.date;
                        timelineInfo.data[ond].results.push(timeLineData);
                    }
                }
            }
            var timeInfo = timelineInfo.data;
            for (var j = 0; j < timeInfo.length; j++) {
                var departureDate = availRQ.journeyInfo[j].departureDateTime;
                if (departureVariance > 0 && timeInfo[j].results.length < 7) {
                    var startDate = moment(departureDate, JSON_DATE_FORMAT).subtract(3, 'days');
                    var diff = 0;
                    if (moment(startDate, JSON_DATE_FORMAT).isBefore(moment())) {
                        diff = moment().diff(moment(startDate, JSON_DATE_FORMAT), 'days');
                    }
                    startDate = moment(startDate, JSON_DATE_FORMAT).add(diff, 'days');
                    var endDate = moment(departureDate, JSON_DATE_FORMAT).add(3 + diff, 'days');
                    var newTimeLineArr = [];
                    var existTimeLineData = {};
                    while (startDate <= endDate) {
                        var isExistDate = false;
                        for (i = 0; i < timeInfo[j].results.length; i++) {
                            if (moment(timeInfo[j].results[i].date, DASH_SEPERATED_DATE).isSame(startDate)) {
                                isExistDate = true;
                                existTimeLineData = timeInfo[j].results[i];
                                break;
                            }
                        }
                        if (!isExistDate) {
                            var dummyTimeLine = angular.copy(dummyTimeLineData);
                            dummyTimeLine.date = moment(startDate, JSON_DATE_FORMAT).format(DASH_SEPERATED_DATE);
                            if (moment(departureDate, JSON_DATE_FORMAT).isSame(
                                    moment(startDate, JSON_DATE_FORMAT))) {
                                dummyTimeLine.selected = true;
                            }
                            newTimeLineArr.push(dummyTimeLine);
                        } else {
                            newTimeLineArr.push(existTimeLineData);
                        }
                        startDate = moment(startDate, JSON_DATE_FORMAT).add(1, 'days');
                    }
                    timelineInfo.data[j].results = newTimeLineArr;
                } else if (departureVariance === 0 && timeInfo[j].results.length === 0) {
                    var dummyEmptyTimeLine = angular.copy(dummyTimeLineData);
                    dummyEmptyTimeLine.date = moment(departureDate, JSON_DATE_FORMAT).format(
                        DASH_SEPERATED_DATE);
                    timelineInfo.data[j].results.push(dummyEmptyTimeLine);
                }

                _AddDateWiseFare(this.adaptFareClasses(response), timelineInfo, j); //
                for (var fl = 0; fl < timelineInfo.data[j].results.length; fl++) {
                    timelineInfo.data[j].results[fl].flights.sort(sortDateWiseFlights);
                }
            }
            return timelineInfo;
        };

        self.adaptFlightFare = function (response) {
            var ondInfo = {};
            ondInfo.success = response.success;
            ondInfo.data = {};
            ondInfo.data.fareTypeInfo = self.adaptFareClasses(response);
            ondInfo.data.fareFareResults = _adaptAvailableOptions(response);
            return ondInfo;
        };

        self.setFareQuoteReqData = function (sectorsData) {
            var temp = {};
            var searchParam = commonLocalStorageService.getAvailabilityParams(); //localStorageService.get('SEARCH_CRITERIA');
            var sectorData = sectorsData;
            temp.journeyInfo = [];
            temp.travellerQuantity = {};
            temp.preferences = {};
            if (searchParam.adult) {
                temp.travellerQuantity.adultCount = searchParam.adult;
            }
            if (searchParam.child) {
                temp.travellerQuantity.childCount = searchParam.child;
            }
            if (searchParam.infant) {
                temp.travellerQuantity.infantCount = searchParam.infant;
            }
            temp.preferences.cabinClass = searchParam.cabin;
            temp.preferences.logicalCabinClass = searchParam.cabin;
            temp.preferences.currency = currencyService.getAppBaseCurrency();
            temp.preferences.originCountryCode = commonLocalStorageService.getData('CARRIER_CODE');
            temp.preferences.promotion = {};
            temp.preferences.promotion.type = '';
            temp.preferences.promotion.code = '';

            if (searchParam.promoCode) {
                temp.preferences.promotion.type = 'PROMO_CODE';
                temp.preferences.promotion.code = searchParam.promoCode;
            }

            for (var i = 0; i < sectorData.length; i++) {
                var secData = {};
                var selectedFlight = sectorData[i].selectedFlight.flight;
                secData.origin = selectedFlight.originCode;
                secData.destination = selectedFlight.destinationCode;
                var depTime = selectedFlight.departureDate + ' ' + selectedFlight.departureTime;
                secData.departureDateTime = moment(depTime, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm:ss');
                var arrTime = selectedFlight.arrivalDate + ' ' + selectedFlight.arrivalTime;
                secData.arrivalDateTime = moment(arrTime, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm:ss');
                secData.departureVariance = 0;
                secData.specificFlights = [];
                var flightTemp = {};
                flightTemp.filghtDesignator = selectedFlight.destinationCode;
                flightTemp.segmentCode = selectedFlight.originCode + '/' + selectedFlight.destinationCode;
                flightTemp.departureDateTime = secData.departureDateTime;
                flightTemp.flightSegmentRPH = selectedFlight.flightRPH;
                flightTemp.routeRefNumber = selectedFlight.routeRefNumber;
                secData.specificFlights.push(flightTemp);
                if (selectedFlight.stops.length) {
                    for (var j = 0; j < selectedFlight.stops.length; j++) {
                        var stopData = selectedFlight.stops[j];
                        var fTemp = {};
                        fTemp.filghtDesignator = stopData.destinationCode;
                        fTemp.segmentCode = stopData.originCode + '/' + stopData.destinationCode;
                        depTime = stopData.departureDate + ' ' + stopData.departureTime;
                        fTemp.departureDateTime = moment(depTime, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm:ss');
                        fTemp.flightSegmentRPH = stopData.flightRPH;
                        fTemp.routeRefNumber = stopData.flightRPH;
                        secData.specificFlights.push(fTemp);
                    }
                }
                secData.preferences = {};
                secData.preferences.additionalPreferences = {};
                secData.preferences.additionalPreferences.fareClassType =
                    getFareClassType(sectorData[i].fareClasses, sectorData[i].selectedFlight.fare.fareTypeId);
                secData.preferences.additionalPreferences.fareClassId =
                    getFareClassID(sectorData[i].fareClasses, sectorData[i].selectedFlight.fare.fareTypeId);

                temp.journeyInfo.push(secData);
            }
            return temp;
        };

        function getFareClassType(fareClasses, id) {
            for (var i = 0; i < fareClasses.length; i++) {
                if (fareClasses[i].fareTypeId == id) {
                    return fareClasses[i].fareClassType;
                }
            }
        }

        function getFareClassID(fareClasses, id) {
            for (var i = 0; i < fareClasses.length; i++) {
                if (fareClasses[i].fareTypeId == id) {
                    return fareClasses[i].fareClassId;
                }
            }
        }

        function _AddDateWiseFare(fareClasses, timelineInfo, j) {
            var timeLineArr = timelineInfo.data[j].results;
            var fareClass;
            for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                var timeLineData = timeLineArr[timeLnIndx];
                var dateWiseFareClassArr = [];
                // var nonExistFareId = '';
                for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                    var flight = timeLineData.flights[opt];
                    var flightAvailFareClsList = flight.flightFaresAvailability;
                    if (flightAvailFareClsList.length > 0) {
                        for (var fc = 0; fc < flightAvailFareClsList.length; fc++) {
                            var avFareCls = flightAvailFareClsList[fc];
                            var isExist = false;
                            if (dateWiseFareClassArr.length > 0) {
                                for (var dInx = 0; dInx < dateWiseFareClassArr.length; dInx++) {
                                    var dateWiseFc = dateWiseFareClassArr[dInx];
                                    if ((dateWiseFc.fareTypeId === avFareCls.fareTypeId)) {
                                        if (dateWiseFc.fareValue < avFareCls.fareValue) {
                                            isExist = true;
                                            break;
                                        } else {
                                            dateWiseFareClassArr.splice(dInx, 1);
                                            break;
                                        }
                                    }
                                }
                                if (!isExist) {
                                    for (var avlafc = 0; avlafc < fareClasses.length; avlafc++) {
                                        fareClass = fareClasses[avlafc];
                                        if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                            fareClass.fareValue = avFareCls.fareValue;
                                            dateWiseFareClassArr.push(fareClass);
                                            break;
                                        }
                                    }
                                }
                            } else {
                                for (var avlbfc = 0; avlbfc < fareClasses.length; avlbfc++) {
                                    fareClass = fareClasses[avlbfc];
                                    if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                        fareClass.fareValue = avFareCls.fareValue;
                                        dateWiseFareClassArr.push(fareClass);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (dateWiseFareClassArr.length > 0) {
                    timelineInfo.data[j].results[timeLnIndx].fareClasses = dateWiseFareClassArr
                        .sort(sortFareClassByPrice);
                } else {
                    timelineInfo.data[j].results[timeLnIndx].fareClasses = fareClasses
                        .sort(sortFareClassByPrice);
                }
            }
            _addDummyForNonAvailFareClasses(timelineInfo, j);
        }

        function _addDummyForNonAvailFareClasses(timelineInfo, j) {
            var timeLineArr = timelineInfo.data[j].results;
            for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                var timeLineData = timeLineArr[timeLnIndx];
                for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                    var flight = timeLineData.flights[opt];
                    var fAvlFareClsList = flight.flightFaresAvailability;
                    if (fAvlFareClsList.length) {
                        flight.isFlightFull = false;
                    } else {
                        flight.isFlightFull = true;
                    }

                    if (moment(flight.departureDate, JSON_DATE_FORMAT).isSame(
                            moment(timeLineData.date, DASH_SEPERATED_DATE))) {
                        timeLineData.selected = true;
                    }

                    var newFlightFareAvailability = [];
                    for (var fc = 0; fc < timeLineData.fareClasses.length; fc++) {
                        var fareCls = timeLineData.fareClasses[fc];
                        var isExist = false;
                        //var tempFareTypeId = '';
                        for (var avfc = 0; avfc < fAvlFareClsList.length; avfc++) {
                            if (fAvlFareClsList[avfc].fareTypeId === fareCls.fareTypeId) {
                                isExist = true;
                                newFlightFareAvailability.push(fAvlFareClsList[avfc]);
                                break;
                            }
                        }
                        if (!isExist) {
                            var dummyAvFc = angular.copy(dummyAvailFareClass);
                            dummyAvFc.fareTypeId = fareCls.fareTypeId;
                            newFlightFareAvailability.push(dummyAvFc);
                        }
                    }
                    timelineInfo.data[j].results[timeLnIndx].flights[opt].flightFaresAvailability =
                        newFlightFareAvailability;
                }
            }
        }

        function _adaptAvailableOptions(response) {
            var availOptionList = [];
            var ondResList = response.originDestinationResponse;
            for (var onds = 0; onds < ondResList.length; onds++) {
                var ondRes = ondResList[onds];
                for (var avOpt = 0; avOpt < ondRes.availableOptions.length; avOpt++) {
                    availOptionList.push(_adaptAvailOption(ondRes.availableOptions[avOpt], ondRes.origin,
                        ondRes.destination));
                }
            }
            return availOptionList;
        }

        function _adaptAvailOption(availOpt, origin, destination) {
            var newOndRes = {};
            newOndRes.originCode = origin;
            newOndRes.destinationCode = destination;
            newOndRes.originName = commonLocalStorageService.getAirportName(origin);
            newOndRes.destinationName = commonLocalStorageService.getAirportName(destination);
            // ond.js
            newOndRes.careerCode = availOpt.segments[0].carrierCode;
            newOndRes.returnFlag = false; // hard coded
            newOndRes.flightRPH = availOpt.segments[0].flightSegmentRPH;
            newOndRes.routeRefNumber = availOpt.segments[0].routeRefNumber;
            newOndRes.flightId = newOndRes.flightRPH.substring(2);
            newOndRes.flightNumber = availOpt.segments[0].filghtDesignator;
            newOndRes.travelMode = availOpt.segments[0].travelMode;
            var depDateTimeArr = availOpt.segments[0].departureDateTime.local.split('T');
            var dateArr = depDateTimeArr[0].split('-');
            newOndRes.departureDate = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0];
            var depTimeArr = depDateTimeArr[1].split(':');
            newOndRes.departureTime = depTimeArr[0] + ':' + depTimeArr[1];
            // Arrival Time
            var arrDateTimeArr = availOpt.segments[availOpt.segments.length - 1].arrivalDateTime.local
                .split('T');
            var arDateArr = arrDateTimeArr[0].split('-');
            newOndRes.arrivalDate = arDateArr[2] + '/' + arDateArr[1] + '/' + arDateArr[0];
            var arrTimeArr = arrDateTimeArr[1].split(':');
            newOndRes.arrivalTime = arrTimeArr[0] + ':' + arrTimeArr[1];

            // Arrival Time for first segment
            var arrDateTimeFirstSeg = availOpt.segments[0].arrivalDateTime.local.split('T');
            var arDateArrFirstSeg = arrDateTimeFirstSeg[0].split('-');
            newOndRes.arrivalDateFirstSeg = arDateArrFirstSeg[2] + '/' + arDateArrFirstSeg[1] + '/' +
                arDateArrFirstSeg[0];
            var arrTimeArrFirstSeg = arrDateTimeFirstSeg[1].split(':');
            newOndRes.arrivalTimeFirstSeg = arrTimeArrFirstSeg[0] + ':' + arrTimeArrFirstSeg[1];

            newOndRes.segmentDuration = _duration(availOpt.segments[0].departureDateTime.local,
                availOpt.segments[0].arrivalDateTime.local);
            newOndRes.totalDuration = _duration(availOpt.segments[0].departureDateTime.local,
                availOpt.segments[availOpt.segments.length - 1].arrivalDateTime.local);

            var showNextDay = function (date1, date2) {
                var d1 = moment(date1, 'DD/MM/YYY');
                var d2 = moment(date2, 'DD/MM/YYY');

                //var days = _getRoundedDays(availOpt.segments[0].departureDateTime.local,
                //    availOpt.segments[availOpt.segments.length - 1].arrivalDateTime.local);
                if (d1 < d2) {
                    return '(+1)';
                } else {
                    return '';
                }
            };

            newOndRes.nextDayFlag = showNextDay(newOndRes.departureDate, newOndRes.arrivalDate);
            // availOpt.totalDuration;
            newOndRes.stops = [];
            for (var seg = 1; seg < availOpt.segments.length; seg++) {
                newOndRes.stops.push(_adaptStop(availOpt.segments[seg - 1], availOpt,
                    availOpt.segments[seg]));
            }
            newOndRes.flightFaresAvailability = [];
            for (var avFareCls = 0; avFareCls < availOpt.availableFareClasses.length; avFareCls++) {
                var avFareClsArr = availOpt.availableFareClasses;
                newOndRes.flightFaresAvailability.push(_adaptAvailFareCls(avFareClsArr[avFareCls]));
            }
            newOndRes.flightFaresAvailability.sort(sortFareClassByPrice);
            return newOndRes;
        }

        function _duration(firstDeparture, lastArrival) {
            var from = moment(firstDeparture, JSON_DATE_FORMAT);
            var to = moment(lastArrival, JSON_DATE_FORMAT);
            var duration = moment.duration(to.diff(from));
            return duration.days() + ':' + duration.hours() + ':' + duration.minutes();
            // return moment.utc(to.diff(from)).format('HH:mm');
        }

        /*        function _getRoundedDays(firstDeparture, lastArrival) {
         var from = moment(firstDeparture, JSON_DATE_FORMAT);
         var to = moment(lastArrival, JSON_DATE_FORMAT);
         var duration = moment.duration(to.diff(from));
         var h = parseInt(duration.days()) * 24 + parseInt(duration.hours());
         var days = Math.ceil(h / 24);
         return days;
         }*/

        function _adaptStop(prevSeg, availOpt, segment) {
            var stop = {};
            stop.originCode = segment.segmentCode.split('/')[0];
            stop.destinationCode = segment.segmentCode.split('/')[1];
            stop.originName = commonLocalStorageService.getAirportName(stop.originCode);
            stop.destinationName = commonLocalStorageService.getAirportName(stop.destinationCode);

            stop.travelMode = segment.travelMode;
            var arrSegDateTimeArr = segment.arrivalDateTime.local.split('T');
            // stop.arrivalDate =
            // arrSegDateTimeArr[0].replace('-', '/');
            // var arrSegDateTimeArr =
            // segment.arrivalDateTime.local.split('T');
            var arrDateArr = arrSegDateTimeArr[0].split('-');
            stop.arrivalDate = arrDateArr[2] + '/' + arrDateArr[1] + '/' + arrDateArr[0];
            var arrTimeArr = arrSegDateTimeArr[1].split(':');
            stop.arrivalTime = arrTimeArr[0] + ':' + arrTimeArr[1];

            var depSegDateTimeArr = segment.departureDateTime.local.split('T');
            // stop.departureDate =
            // depSegDateTimeArr[0].replace('-', '/');
            var depDateArr = depSegDateTimeArr[0].split('-');
            stop.departureDate = depDateArr[2] + '/' + depDateArr[1] + '/' + depDateArr[0];
            var depTimeArr = depSegDateTimeArr[1].split(':');
            stop.departureTime = depTimeArr[0] + ':' + depTimeArr[1];

            stop.airportCode = segment.segmentCode.split('/')[0];
            stop.airportName = commonLocalStorageService.getAirportName(stop.airportCode);
            stop.careerCode = segment.carrierCode;
            stop.flightRPH = segment.flightSegmentRPH;
            stop.routeRefNumber = segment.routeRefNumber;
            stop.flightNumber = segment.filghtDesignator;
            stop.segmentDuration = _duration(segment.departureDateTime.local,
                segment.arrivalDateTime.local);
            stop.durationWaiting = _duration(prevSeg.arrivalDateTime.local,
                segment.departureDateTime.local);
            return stop;
        }

        function _adaptAvailFareCls(availFareCls) {
            var flightFareAv = {};
            flightFareAv.availableSeats = availFareCls.availableSeats;
            if (availFareCls.additionalFareClassInfo == null) {
                flightFareAv.additionalFareClassInfo = null;
            } else {
                flightFareAv.additionalFareClassInfo = {};
                flightFareAv.additionalFareClassInfo.discountAvailable =
                    availFareCls.additionalFareClassInfo.discountAvailable;
                flightFareAv.additionalFareClassInfo.discountAmount =
                    availFareCls.additionalFareClassInfo.discountAmount;
            }
            flightFareAv.fareValue = Math.ceil(availFareCls.price);
            flightFareAv.fareTypeId = availFareCls.fareClassCode;
            flightFareAv.returnFare = false; // TODO hard coded
            flightFareAv.visibleChannels = availFareCls.visibleChannels;
            return flightFareAv;
        }

        function _AddDateWiseFareClasses(fareClasses, timelineInfo, departureDate) {
            var timeLineArr = timelineInfo.data.results;
            for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                var timeLineData = timeLineArr[timeLnIndx];
                var dateWiseFareClassArr = [];
                for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                    var flight = timeLineData.flights[opt];
                    var flightAvailFareClsList = flight.flightFaresAvailability;
                    if (flightAvailFareClsList.length > 0) {
                        for (var fc = 0; fc < flightAvailFareClsList.length; fc++) {
                            var avFareCls = flightAvailFareClsList[fc];
                            var isExist = false, fareClass;
                            if (dateWiseFareClassArr.length > 0) {
                                for (var dInx = 0; dInx < dateWiseFareClassArr.length; dInx++) {
                                    var dateWiseFc = dateWiseFareClassArr[dInx];
                                    if ((dateWiseFc.fareTypeId === avFareCls.fareTypeId)) {
                                        if (dateWiseFc.fareValue < avFareCls.fareValue) {
                                            isExist = true;
                                            break;
                                        } else {
                                            dateWiseFareClassArr.splice(dInx, 1);
                                            break;
                                        }
                                    }
                                }
                                if (!isExist) {
                                    for (var avlafc = 0; avlafc < fareClasses.length; avlafc++) {
                                        fareClass = fareClasses[avlafc];
                                        if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                            fareClass.fareValue = avFareCls.fareValue;
                                            dateWiseFareClassArr.push(fareClass);
                                            break;
                                        }
                                    }
                                }
                            } else {
                                for (var avlbfc = 0; avlbfc < fareClasses.length; avlbfc++) {
                                    fareClass = fareClasses[avlbfc];
                                    if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                        fareClass.fareValue = avFareCls.fareValue;
                                        dateWiseFareClassArr.push(fareClass);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (dateWiseFareClassArr.length > 0) {
                    timelineInfo.data.results[timeLnIndx].fareClasses = dateWiseFareClassArr
                        .sort(sortFareClassByPrice);
                } else {
                    timelineInfo.data.results[timeLnIndx].fareClasses = fareClasses
                        .sort(sortFareClassByPrice);
                }
            }
            _addDummyForNonAvailFareCls(timelineInfo, departureDate);
        }

        function _addDummyForNonAvailFareCls(timelineInfo, departureDate) {
            var timeLineArr = timelineInfo.data.results;
            for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                var timeLineData = timeLineArr[timeLnIndx];
                for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                    var flight = timeLineData.flights[opt];
                    var fAvlFareClsList = flight.flightFaresAvailability;
                    if (fAvlFareClsList.length) {
                        flight.isFlightFull = false;
                    } else {
                        flight.isFlightFull = true;
                    }

                    if (moment(departureDate, JSON_DATE_FORMAT)) {
                        if (moment(departureDate, JSON_DATE_FORMAT).isSame(
                                moment(timeLineData.date, DASH_SEPERATED_DATE))) {
                            timeLineData.selected = true;
                        }
                    }

                    var newFlightFareAvailability = [];
                    for (var fc = 0; fc < timeLineData.fareClasses.length; fc++) {
                        var fareCls = timeLineData.fareClasses[fc];
                        var isExist = false;
                        for (var avfc = 0; avfc < fAvlFareClsList.length; avfc++) {
                            if (fAvlFareClsList[avfc].fareTypeId === fareCls.fareTypeId) {
                                isExist = true;
                                newFlightFareAvailability.push(fAvlFareClsList[avfc]);
                                break;
                            }
                        }
                        if (!isExist) {
                            var dummyAvFc = angular.copy(dummyAvailFareClass);
                            dummyAvFc.fareTypeId = fareCls.fareTypeId;
                            newFlightFareAvailability.push(dummyAvFc);
                        }
                    }
                    timelineInfo.data.results[timeLnIndx].flights[opt].flightFaresAvailability =
                        newFlightFareAvailability;
                }
            }
        }

        // utill functions
        function fareClassCompare(fareClassA, fareClassB) {
            if (fareClassA.fareTypeId < fareClassB.fareTypeId) {
                return -1;
            }
            else if (fareClassA.fareTypeId > fareClassB.fareTypeId) {
                return 1;
            }
            else {
                return 0;

            }
        }

        function sortFareClassByPrice(fareClassA, fareClassB) {
            if (fareClassA.fareValue < fareClassB.fareValue) {
                return -1;
            }
            else if (fareClassA.fareValue > fareClassB.fareValue) {
                return 1;
            }
            else {
                return 0;
            }
        }

        function sortAvailOptByDepartureDate(availOptA, availOptB) {
            if (availOptA.segments[0].departureDateTime.local < availOptB.segments[0].departureDateTime.local) {
                return -1;
            }
            else if (availOptA.segments[0].departureDateTime.local > availOptB.segments[0].departureDateTime.local) {
                return 1;
            }
            else {
                return 0;
            }
        }

        function sortDateWiseFlights(flightA, flightB) {
            var depDateTimeStrA = flightA.departureDate + ' ' + flightA.departureTime;
            var arriveDateTimeStrA = flightA.arrivalDate + ' ' + flightA.arrivalTime;
            var depDateTimeStrB = flightB.departureDate + ' ' + flightB.departureTime;
            var arriveDateTimeStrB = flightB.arrivalDate + ' ' + flightB.arrivalTime;
            var hubCount = flightA.stops.length - flightB.stops.length;
            if (flightA.stops.length > 0) {
                arriveDateTimeStrA = flightA.stops[flightA.stops.length - 1].arrivalDate + ' ' +
                    flightA.stops[flightA.stops.length - 1].arrivalTime;
            }
            if (flightB.stops.length > 0) {
                arriveDateTimeStrB = flightB.stops[flightB.stops.length - 1].arrivalDate + ' ' +
                    flightB.stops[flightB.stops.length - 1].arrivalTime;
            }
            var totalDurationA = moment(arriveDateTimeStrA, 'DD-MM-YYYY HH:mm').diff(
                moment(depDateTimeStrA, 'DD-MM-YYYY HH:mm'), 'minutes');
            var totalDurationB = moment(arriveDateTimeStrB, 'DD-MM-YYYY HH:mm').diff(
                moment(depDateTimeStrB, 'DD-MM-YYYY HH:mm'), 'minutes');
            var totalDuration = totalDurationA - totalDurationB;
            if (hubCount != 0) {
                return hubCount;
            }
            var comparison = moment(depDateTimeStrA, 'DD-MM-YYYY HH:mm').diff(
                moment(depDateTimeStrB, 'DD-MM-YYYY HH:mm'), 'minutes');
            if (comparison != 0) {
                return comparison;
            }
            return totalDuration;
        }

        // dummy data
        var dummyAvailFareClass = {
            'availableSeats': '',
            'fareValue': 'null',
            'fareClassCode': '',
            'returnFare': false,
            'fareTypeId': '',
            'visibleChannels': []
        };

        var dummyTimeLineData = {
            'available': true,
            'date': '',
            'selected': false,
            'flights': [],
            'fareValue': '',
            'fareClasses': []
        };

    } // End of adaptor service.

})();
