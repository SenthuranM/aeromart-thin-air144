(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('errorPageController', [
            'alertService', '$window', 'applicationService',
            errorPageController
        ]);

    function errorPageController(alertService, $window, applicationService) {
        var self = this;

        self.errorMessage = alertService.getPageAlertMessage();

        self.startOver = function () {
            $window.location.href = 'index.html';
        };

        init();
        function init() {
            if (alertService.getErrorType() === alertService.ERRORTYPE.TRANSACTION_ID_ERROR) {
                applicationService.setTransactionID(null);
            }
        }
    }
})();
