(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('FlightSearchCtrl', ['$state', 'flightService', 'commonLocalStorageService', FlightSearchCtrl]);

    function FlightSearchCtrl($state, flightService, commonLocalStorageService) {
        var self = this,
            dateFormat = 'DD-MM-YYYY';

        self.flightTypes = {
            'ONE_WAY': 'oneway',
            'RETURN': 'return'
        };

        // TODO: Move to factory
        self.searchCriteria = {
            'language': 'en',
            'currency': null,
            'originCountry': 'LK',
            'from': '',
            'to': '',
            'departureDate': null,
            'returnDate': null,
            'adults': 1,
            'children': 0,
            'infants': 0,
            'cabinClass': 'Y',
            'promoCode': ''
        };

        self.passengerDetails = {
            adult: {
                labelSingle: 'Adult',
                labelMulti: 'Adults',
                value: self.searchCriteria.adults,
                max: 9,
                min: 1
            },
            child: {
                labelSingle: 'Child',
                labelMulti: 'Children',
                value: self.searchCriteria.children,
                max: 8,
                min: 0
            },
            infant: {
                labelSingle: 'Infant',
                labelMulti: 'Infants',
                value: self.searchCriteria.infants,
                max: 9,
                min: 0
            }
        };

        self.selectedTab = self.flightTypes.ONE_WAY;

        // Initial airport list is set to the list of all airports taken from the ond.js airports array
        self.departureAirports = airports;
        self.destinationAirports = airports;
        self.currencies = currencies;

        self.ondDataMap = []; //Hold a list of available destinations for each airport

        self.setSelectedTab = function (flightType) {
            self.selectedTab = flightType;

            // Reset return date if one way flight is selected
            if (flightType === self.flightTypes.ONE_WAY) {
                self.searchCriteria.returnDate = '';
            }
        };

        self.filterDesAirport = function () {
            //TODO: Uncomment once the OND.js file is fixed
            //self.destinationAirports = self.ondDataMap[self.searchCriteria.from];
        };

        self.searchFlight = function () {

            var tempDeparture, tempReturn;

            if (moment(self.searchCriteria.departureDate).isValid()) {
                tempDeparture = moment(self.searchCriteria.departureDate).format(dateFormat);
            }
            if (moment(self.searchCriteria.returnDate).isValid()) {
                tempReturn = moment(self.searchCriteria.returnDate).format(dateFormat);
            }

            $state.go('availability', {
                lang: self.searchCriteria.language,
                currency: self.searchCriteria.currency.code,
                originCountry: self.searchCriteria.originCountry,
                from: self.searchCriteria.from,
                to: self.searchCriteria.to,
                depDate: tempDeparture,
                retDate: tempReturn || 'N',
                adult: self.passengerDetails.adult.value,
                child: self.passengerDetails.child.value,
                infant: self.passengerDetails.infant.value,
                cabin: self.searchCriteria.cabinClass || 'N',
                promoCode: self.searchCriteria.promoCode
            });
        };

        self.getFormattedReturnDate = function () {
            if (self.searchCriteria.departureDate) {
                return moment(self.searchCriteria.departureDate).format('YYYY-MM-DD');
            }
            else {
                return moment().format('YYYY-MM-DD');
            }
        };

        self.getTodayFormatted = function () {
            return moment().format('YYYY-MM-DD');
        };

        self.departureChanged = function () {
            if (self.searchCriteria.returnDate &&
                (self.searchCriteria.returnDate < self.searchCriteria.departureDate)) {
                self.searchCriteria.returnDate = self.searchCriteria.departureDate;
            }
        };

        // IIFE
        (function () {
            self.ondDataMap = flightService.getToAirports();
            commonLocalStorageService.clearAll();
        })();
    }
})();
