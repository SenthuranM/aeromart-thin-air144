(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('alertService', ['$sce', '$location', 'currencyService', 'languageService', alertService]);

    function alertService($sce, $location, currencyService, languageService) {
        var self = this;

        var arrAlert = {}, pageAlertMessage = '', erroTyp = null;
        self.ERRORTYPE = {
            TRANSACTION_ID_ERROR: 'TRANSACTION_ID_ERROR',
            OTHER_ERROR: 'OTHER_ERROR'
        };

        var listeners = [];

        var notifyListeners = function () {
            angular.forEach(listeners, function (list) {
                list();
            });
        };

        self.setPageAlertMessage = function (errorObj) {
            pageAlertMessage = errorObj.error || 'System Error';
            erroTyp = errorObj.errorType || 'OTHER_ERROR';
            $location.path('/error/' + languageService.getSelectedLanguage() + '/' +
                currencyService.getSelectedCurrency());
            return false;
        };

        self.getPageAlertMessage = function () {
            return pageAlertMessage;
        };

        self.setErrorType = function (typ) {
            erroTyp = typ;
        };

        self.getErrorType = function () {
            return erroTyp;
        };

        self.registerListeners = function (listener) {
            listeners.push(listener);
        };

        self.setAlert = function (msg, type, fn) {
            arrAlert.msg = $sce.trustAsHtml(msg);
            arrAlert.type = type;
            arrAlert.fn = fn;
            notifyListeners();
        };

        self.getAlert = function () {
            return arrAlert;
        };
    }
})();
