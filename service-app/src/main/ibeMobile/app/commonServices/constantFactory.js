(function () {
    'use strict';
    angular.module('ibeMobileApp')
        .factory('constantFactory', ['masterDataService', function (masterDataService) {

            var MasterData = masterDataService.getMasterData();

            var Countries = function () {
                return getCountries();
            };
            var Nationality = function () {
                return getNationalities();
            };
            var ExchangeRates = function () {
                return MasterData.ExchangeRates;
            };
            var AppConfigs = function () {
                return MasterData.AppParams;
            };

            var getCountries = function () {
                var returnArr = [];
                angular.forEach(MasterData.CountryList, function (country, key) {
                    var dummy = {};
                    dummy.name = country.countryName;
                    dummy.code = country.countryCode;
                    dummy.phoneCode = country.phoneCode;
                    returnArr[returnArr.length] = dummy;
                });
                return returnArr;
            };

            var PaxTitles = function () {
                return MasterData.dropDownLists.paxTitels;
            };

            var PaxTypeWiseTitle = function () {
                return MasterData.dropDownLists.paxTypeWiseTitles;
            };

            var LangaugeLists = function () {
                return MasterData.dropDownLists.languages;
            };

            var getNationalities = function () {
                var returnArr = [];
                angular.forEach(MasterData.CountryList, function (country, key) {
                    var dummy = {};
                    dummy.name = country.nationalityDes;
                    dummy.code = country.nationalityCode;
                    returnArr[returnArr.length] = dummy;
                });
                return returnArr;
            };

            // Get weight unit for the site.
            var getWeightUnit = function () {
                return "";
            };

            var getAppConfigValue = function (appConfigKey) {
                var val = false;
                angular.forEach(MasterData.AppParams, function (appCongfig, key) {
                    if (appCongfig.key === appConfigKey) {
                        val = (appCongfig.value === "true");
                    }
                });

                return val;
            };
            var getAppConfigStringValue = function (appConfigKey) {
                var val = false;
                angular.forEach(MasterData.AppParams, function (appCongfig, key) {
                    if (appCongfig.key === appConfigKey) {
                        val = appCongfig.value;
                    }
                });

                return val;
            };
            
            var CountryWiseStates = function (){
            	return MasterData.dropDownLists.countryWiseStates;
            };
            
            return {
                AppConfigs: function () {
                    return AppConfigs();
                },
                Countries: function () {
                    return new Countries();
                },
                Nationalities: function () {
                    return new Nationality();
                },
                ExchangeRates: function () {
                    return ExchangeRates();
                },
                PaxTitles: function () {
                    return PaxTitles();
                },
                PaxTypeWiseTitle: function () {
                    return PaxTypeWiseTitle();
                },
                Langauges: function () {
                    return LangaugeLists();
                },
                AppConfigValue: function (appConfigKey) {
                    return getAppConfigValue(appConfigKey);
                },
                AppConfigStringValue: function (appConfigKey) {
                    return getAppConfigStringValue(appConfigKey);
                },
                WeightUnit: getWeightUnit(),
                defaultWeight: function () {
                    return "10 Kg"
                },
                CountryWiseStates: function () {
                	return CountryWiseStates();
                }

            }
        }]);
})();
