(function (angular) {
    'use strict';
    var app = angular.module('ibeMobileApp');
    app.service('masterDataService', ['remoteService', function (remoteService) {

        var self = this;

        self.appStatus = {
            loading: true
        };

        self.masterData = {
            AppParams: [],
            CountryList: [],
            ExchangeRates: [],
            dropDownLists: {}
        };

        self.getAppStatus = function () {
            return self.appStatus;
        };

        self.retrieveCountries = function () {
            return remoteService.get('/masterData/countryList');
        };

        self.retrieveExchangeRates = function () {
            return remoteService.get('/masterData/currencyExRates');
        };

        self.retrieveApplicationParams = function () {
            return remoteService.post('/parameters/ibe', {});
        };

        //TODO: Should be POST requests
        /*        self.retrievePassengerContactParams = function () {
         return remoteService.get('/paxConfig/paxContactDetails');
         };

         self.retrievePassengerDetailParams = function () {
         return remoteService.get('/paxConfig/paxDetails');
         };

         self.retrivePassengerRegistrationParams = function () {
         return remoteService.get('/paxConfig/usrRegDetails');
         };*/

        self.retrieveCommonLists = function () {
            return remoteService.get('/masterData/dropDownLists');
        };

        self.retrieveAirportMessages = function () {
            return remoteService.get('/masterData/airportMessages');
        };

        self.retrieveTermsConditions = function () {
            return remoteService.get('/paxConfig/termsNConditions');
        };

        self.setMasterData = function (masterData) {
            self.masterData.AppParams = _.get(masterData, 'AppParams.ibeParamKeyResponse');
            self.masterData.CountryList = masterData.CountryList;
            self.masterData.ExchangeRates = masterData.ExchangeRates;
            self.masterData.dropDownLists = masterData.dropDownLists;

            self.appStatus.loading = false;
        };

        self.getMasterData = function () {
            return self.masterData;
        };
    }]);
})(angular);
