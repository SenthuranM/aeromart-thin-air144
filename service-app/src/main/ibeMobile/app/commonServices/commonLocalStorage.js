(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('commonLocalStorageService', [
            'localStorageService',
            'languageService',
            commonLocalStorageService
        ]);

    function commonLocalStorageService(localStorageService, languageService) {
        var self = this;

        var keys = {
            AVAILABILITY_PARAMS: 'availability_params',
            CONTACT_DETAIL: 'contact_details',
            PASSENGER_DETAILS: 'passenger_details',
            DEPARTURE_FLIGHT: 'departure_flight_details',
            AVAILABILITY_OUTPUT_DATA: 'availability_output_data',
            CONTACT_PASSENGER_DETAILS: 'contact_passenger_details'
        };

        self.clearAll = function () {
            return localStorageService.clearAll();
        };

        self.setData = function (id, data) {
            localStorageService.set(id, data);
        };

        self.getData = function (id) {
            return localStorageService.get(id);
        };

        self.remove = function (key) {
            localStorageService.remove(key);
        };

        self.setDepartureFlightData = function (params) {
            localStorageService.set(keys.DEPARTURE_FLIGHT, params);
        };
        self.getDepartureFlightData = function () {
            return localStorageService.get(keys.DEPARTURE_FLIGHT);
        };

        self.setAvailabilityOutputData = function (outputData) {
            localStorageService.set(keys.AVAILABILITY_OUTPUT_DATA, outputData);
        };
        self.getAvailabilityOutputData = function () {
            return localStorageService.get(keys.AVAILABILITY_OUTPUT_DATA);
        };

        self.setAvailabilityParams = function (params) {
            localStorageService.set(keys.AVAILABILITY_PARAMS, params);
        };
        self.getAvailabilityParams = function () {
            return localStorageService.get(keys.AVAILABILITY_PARAMS);
        };

        self.setContactDetails = function (params) {
            localStorageService.set(keys.CONTACT_DETAIL, params);
        };
        self.getContactDetails = function () {
            return localStorageService.get(keys.CONTACT_DETAIL);
        };

        self.setPassengerDetails = function (params) {
            localStorageService.set(keys.PASSENGER_DETAILS, params);
        };
        self.getPassengerDetails = function () {
            return localStorageService.get(keys.PASSENGER_DETAILS);
        };

        self.setContactPassenger = function (contactPassenger) {
            localStorageService.set(keys.CONTACT_PASSENGER_DETAILS, contactPassenger);
        };
        self.getContactPassenger = function () {
            return localStorageService.get(keys.CONTACT_PASSENGER_DETAILS);
        };

        // airports from ond...
        self.getAirportName = function (code) {
            var returnCode = code || '';

            if (airports !== undefined) {
                angular.forEach(airports, function (obj) {
                    if (obj.code.toUpperCase() === returnCode.toUpperCase()) {
                        returnCode = obj[languageService.getSelectedLanguage().toLowerCase()];
                    }
                });
            }
            return returnCode;
        };
    }
})();
