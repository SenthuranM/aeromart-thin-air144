(function () {
    'use strict';
    angular.module('ibeMobileApp')
        .service('remoteService', [
            '$http',
            remoteService
        ]);

    function remoteService($http) {

        // Global vars.
        var self = this,
            baseUrl = '/service-app/controller';
        var requestConfigPOST = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var requestConfigGET = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        self.post = function (url, params) {
            return $http.post(baseUrl + url, params, requestConfigPOST);
        };

        self.get = function (url) {
            return $http.get(baseUrl + url, requestConfigGET);
        };
    }

})();
