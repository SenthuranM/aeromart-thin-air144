(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('applicationService', ['localStorageService', applicationService]);

    function applicationService(localStorageService) {
        var self = this,
            transactionID,
            applicationMode,
            flightType,
            currentStep;

        self.FLIGHT_TYPES = {
            'ONE_WAY': 'one_way',
            'RETURN': 'return',
            'MULTI_CITY': 'multi_city'
        };

        self.APP_MODES = {
            'CREATE_RESERVATION': 'create',
            'MODIFY_RESERVATION': 'modify',
            'CANCEL_RESERVATION': 'cancel_reservation',
            'MODIFY_SEGMENT': 'modify_segment',
            'ADD_MODIFY_ANCI': 'modifyAnci',
            'MULTICITY': 'multicity',
            'BALANCE_PAYMENT': 'balancePayment',
            'NAME_CHANGE': 'nameChange',
            'CONTACT_CHANGE': 'changeContactInformation'
        };

        self.setTransactionID = function (id) {
            transactionID = id;
            localStorageService.set('TRANSACTION_ID', id);
        };

        self.getTransactionID = function () {
            if (!!transactionID) {
                return transactionID;
            } else {
                return localStorageService.get('TRANSACTION_ID') || null;
            }
        };

        self.setApplicationMode = function (mode) {
            if (!!mode) {
                applicationMode = mode;
            }
        };

        self.getApplicationMode = function () {
            return applicationMode;
        };

        self.setFlightType = function (type) {
            flightType = type;
            localStorageService.set('FLIGHT_TYPE', type);
        };

        self.getFlightType = function () {
            return flightType;
        };

        self.setCurrentStep = function (step) {
            currentStep = step;
        };

        self.getCurrentStep = function () {
            return currentStep;
        };

        (function () {
            currentStep = 0;
            if (localStorageService.get('FLIGHT_TYPE')) {
                flightType = localStorageService.get('FLIGHT_TYPE');
            } else {
                flightType = self.FLIGHT_TYPES.ONE_WAY;
            }

            applicationMode = self.APP_MODES.CREATE_RESERVATION;
        })();
    }
})();
