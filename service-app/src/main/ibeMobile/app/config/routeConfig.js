(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .config(appConfig);

    function appConfig($stateProvider, $urlRouterProvider) {

        // route otherwise.
        $urlRouterProvider.otherwise('/selectFlight');

        var headerTemplate = {
            templateUrl: 'modules/common/progress_bar.html'
        };

        $stateProvider
            .state('parentState', {
                abstract: true,
                templateUrl: 'modules/common/main_container.html'
            })
            .state('selectFlight', {
                parent: 'parentState',
                url: '/selectFlight',
                views: {
                    'content': {
                        templateUrl: 'modules/flightSearch/select_flight.html'
                    }
                }
            })
            .state('availability', {
                parent: 'parentState',
//                url: '/availability/:lang/:currency/:originCountry/:from/:originCity/:to/:destCity/:depDate/:retDate/:adult/:child/:infant/' +
//                ':cabin/:promoCode',
                url: '/availability/:lang/:currency/:originCountry/:from/:to/:depDate/:retDate/:adult/:child/:infant/' +
                ':cabin/:promoCode',
                views: {
                    'header': headerTemplate,
                    'content': {
                        templateUrl: 'modules/availability/flight_list.html'
                    }
                }
            })
            .state('return', {
                parent: 'parentState',
                url: '/return',
                views: {
                    'header': headerTemplate,
                    'content': {
                        templateUrl: 'modules/availability/flight_list_return.html'
                    }
                }
            })
            .state('passenger', {
                parent: 'parentState',
                url: '/passenger',
                views: {
                    'header': headerTemplate,
                    'content': {
                        templateUrl: 'modules/passenger/personal_details.html'
                    }
                }
            })
            .state('contactPassenger', {
                parent: 'parentState',
                url: '/contactPassenger',
                views: {
                    'header': headerTemplate,
                    'content': {
                        templateUrl: 'modules/passenger/contact_details.html'
                    }
                }
            })
            .state('payment', {
                parent: 'parentState',
                url: '/payment',
                views: {
                    'header': headerTemplate,
                    'content': {
                        templateUrl: 'modules/payment/payment_method.html'
                    }
                }
            })
            .state('postPayment', {
                parent: 'parentState',
                url: '/postPayment',
                controller: 'PostPaymentCtrl',
                views: {
                    'content': {
                        templateUrl: 'modules/postPayment/post_payment.html'
                    }
                }
            })
            .state('confirm', {
                parent: 'parentState',
                url: '/confirm',
                views: {
                    'content': {
                        templateUrl: 'modules/confirm/confirm.html'
                    }
                }
            })
            .state('error', {
                parent: 'parentState',
                url: '/error',
                views: {
                    'content': {
                        templateUrl: 'modules/confirm/confirm.html'
                    }
                }
            });
    }

})();
