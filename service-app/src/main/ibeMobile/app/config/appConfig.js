(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .config([
            '$httpProvider',
            appConfig
        ])
        .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
            cfpLoadingBarProvider.spinnerTemplate = '<div class="loading"></div>';
            cfpLoadingBarProvider.latencyThreshold = 100;
        }]);

    function appConfig($httpProvider) {
        //Set HTTP Interceptor
        $httpProvider.interceptors.push('ibeMobileHttpInterceptor');
    }

})();
