(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .factory('ibeMobileHttpInterceptor', [
            'applicationService',
            interceptorFactory
        ]);

    function interceptorFactory(applicationService) {

        // FIXME: Temporary hack to make sure that the transactionId does not get reset when
        var masterDataUrls = [
            '/service-app/controller/masterData/countryList',
            '/service-app/controller/masterData/currencyExRates',
            '/service-app/controller/masterData/dropDownLists',
            '/service-app/controller/parameters/ibe'
        ];

        var reqResInterceptor = {
            request: function (config) {
                if (config.method == 'POST') {
                    if (!!config.data) {
                        config.data.transactionId = applicationService.getTransactionID();
                    }
                }
                return config;
            },
            response: function (response) {
                if (response.config.method == 'POST' &&
                    response.data.hasOwnProperty('transactionId') &&
                    _.indexOf(masterDataUrls, response.config.url) == -1) {
                    applicationService.setTransactionID(response.data.transactionId);
                }
                return response;
            }
        };
        return reqResInterceptor;
    }

})();
