(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .config(localStorageConfig);

    function localStorageConfig(localStorageServiceProvider) {

        localStorageServiceProvider
            .setPrefix('ibe_mobile_')
            .setStorageType('sessionStorage');
    }

})();
