'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngDialog'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'landing/landing.html'
        })
        .when('/login', {
            templateUrl: 'login/login.html'
        })
        .when('/success', {
            templateUrl: 'login/success.html'
        })
        .when('/error', {
            templateUrl: 'error/error.html'
        })
        .otherwise({redirectTo: '/'});
}]);
