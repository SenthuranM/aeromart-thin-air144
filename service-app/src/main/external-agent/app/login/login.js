'use strict';

angular.module('myApp')
    .controller('LoginCtrl', ['$location', 'loginService', 'landingService', 'ngDialog', '$timeout', '$window',
        function ($location, loginService, landingService, ngDialog, $timeout, $window) {
        var self = this;

        self.user = "";
        self.pw = "";

        self.logInUser = function () {
            var returnUrl = landingService.getReturnUrl() || $location.search().returnUrl;
            var merchantID = $location.search().merchantID;

            var params = {
                username: 'ABY' + self.user.toUpperCase(),
                password: self.pw,
                transactionId: null,
                returnurl: returnUrl,
                merchantId: merchantID
            };

            loginService.logInUser(params,
                function success(success) {

                    var response = success.data;

                    if(response.success === true){
                        ngDialog.open(
                            {
                                template: '<p>Login Successful - Re-directing</p>',
                                plain: true,
                                className: 'ngdialog-theme-default'
                            }
                        );

                        $timeout(function(){
                            ngDialog.close();
                            $window.location.href = returnUrl 
                                     + '?token=' + response.token 
                                     + '&expiryTimeStamp='+ response.expiryTimeStamp 
                                     + '&success=' +response.success;
                        }, 1000)

                    } else {
                        errorRedirect(response.messages[0]);
                    }
                },
                function (error) {
                    errorRedirect();
                }
            );

            function errorRedirect(errorMessage) {
                errorMessage = errorMessage || "";

                ngDialog.open(
                    {
                        template: '<h4>Login Failed</h4> </br> <p ng-show="ngDialogData.message">{{ngDialogData.message}}</p>',
                        plain: true,
                        data: {message: errorMessage}
                    }
                );

                $timeout(function(){
                    ngDialog.close();
                    $window.location.href = returnUrl + '#error';
                }, 1000)
            }
        };

    }])

    .service('loginService', ['$http', function loginService($http) {
        var self = this;

        self.logInUser = function (params, successFn, errorFn) {
            var path = '/service-app/controller/externalagent/auth/login';
            $http.post(path, params)
                .then(
                    function success(res) {
                        successFn(res);
                    },
                    function error(err) {
                        errorFn(err)
                    }
                )
        }
    }]);