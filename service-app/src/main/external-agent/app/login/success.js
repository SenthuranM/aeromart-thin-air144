'use strict';

angular.module('myApp')
    .controller('SuccessCtrl', ['$location', function ($location) {
        var self = this;

        var searchObject = $location.search();
        self.token = searchObject.token;
    }]);
