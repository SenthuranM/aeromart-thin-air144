// Generated on 2015-08-03 using generator-angular 0.12.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'
process.execArgv = [];


module.exports = function (grunt) {

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);
    grunt.loadNpmTasks('grunt-connect-proxy');
    grunt.loadNpmTasks('grunt-processhtml');
    
    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin',
        ngtemplates: 'grunt-angular-templates',
        cdnify: 'grunt-google-cdn'
    });


    //Configurable paths for the application
    var appConfig = {
        app: 'src',
	    dist: 'dist',
        bower: 'bower_components'
    };
    var minification = true;
    var client = grunt.option('client') || 'ISA';
    console.log("*****************************************************");
    console.log(grunt.option('client'));
    var disableLiveLoad = grunt.option('useHapiServer') || grunt.option('disableLiveLoad');
    var syncHapiServer = (grunt.option('useHapiServer')| false) ;
    var themeable = grunt.option('themeable') || false;
    //var clientData = grunt.file.readJSON('src/client.json');
    // Remove copying client.json to src folder
    //FIXME validation is somewhat errorneous - some tasks should validate with src/client.json - the current source file
    var clientData = grunt.file.readJSON('clients/' + client + '/json/client.json');
	
    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        yeoman: appConfig,
        client:client,
        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            },
            js: {
                files: ['<%= yeoman.app %>/js/{,*/}*.js'],
                tasks: ['newer:jshint:all'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            jsTest: {
                files: ['test/spec/{,*/}*.js'],
                tasks: ['newer:jshint:test', 'karma']
            },
            compass: {
                files: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
                tasks: ['compass:server', 'autoprefixer:server']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= yeoman.app %>/{,*/}*.html',
                     '<%= yeoman.app %>/{,*/}*.js',
                    '.tmp/styles/{,*/}*.css',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },        
        
        processhtml: {        	
            options: {
              data: {
                message: 'document.writeln("<script src="+filename+"><\\/script>");</script><script type="text/javascript" src="'+clientData.tagscript+'"></script></head><body>'
              }              
            },
            buildToProd: {
              files: {
                'dist/reservation.html': ['dist/reservation.html']
              }
            }
          },

        // The actual grunt server settingss
        connect: {
            options: {
                port: 9000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: '127.0.0.1',//10.20.11.185
                livereload: 35730
            }, //Uncomment to set proxy
            proxies: [{
                context: '/service-app/controller', //the context of the data service
                host: '10.30.21.55', //wherever the data service is running
                port: 8080//, //the port that the data service is running on
                /* rewrite: {
                 '^/sita-web': '/sita-web'
                 }*/
            },
            {
                context: '/jcaptcha.jpg', //the context of the data service
                host: '10.30.21.55', //wherever the data service is running
                port: 8080, //, //the port that the data service is running on
                rewrite: {
                 '/jcaptcha.jpg': '/ibe/jcaptcha.jpg'
                 }
            }],
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        var middlewares = [];
                        middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);
                        middlewares.push(connect.static('.tmp'));
                        middlewares.push(connect().use(
                            '/bower_components',
                            connect.static('./bower_components')
                        ));
                        middlewares.push( connect.static(appConfig.app));
                        middlewares.push( connect().use(
                            '/src/styles',
                            connect.static('./src/styles')
                        ));
                        return middlewares;
                    },
                files: [
                    '<%= yeoman.app %>/{,*/}*.html',
                     '<%= yeoman.app %>{,*/}*.js',
                    '.tmp/styles/{,*/}*.css',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
                }
            },
            test: {
                options: {
                    port: 9001,
                    middleware: function (connect) {
                        return [
                            connect.static('.tmp'),
                            connect.static('test'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= yeoman.dist %>'
                }
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: {
                src: [
                    'Gruntfile.js',
                    '<%= yeoman.app %>/scripts/{,*/}*.js'
                ]
            },
            test: {
                options: {
                    jshintrc: 'test/.jshintrc'
                },
                src: ['test/spec/{,*/}*.js']
            }
        },

        // Empties folders to start fresh
        clean: {
            cssStyles:{
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.app %>/styles/*.css',
                    ]
                }]
            },
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/{,*/}*',
                        '!<%= yeoman.dist %>/.git{,*/}*'
                    ]
                }]
            },
            clients:{
                src:['<%= yeoman.app %>/styles/common/_colors.scss', 
                '<%= yeoman.app %>/styles/common/_client_style.scss', 
                '<%= yeoman.app %>/styles/common/theme.scss',
                '<%= yeoman.app %>/styles/{,*/}*.css',
                '<%= yeoman.app %>/styles/{,*/}*.css.map',
                '<%= yeoman.app %>/images/{,*/}*.*',
                '<%= yeoman.app %>/client.json',
                '<%= yeoman.app %>/i18n/**/override.json']
            },
            clientDist:{
                src:['clients/<%= client %>/dist/*.html'
                ,'clients/<%= client %>/dist/styles/**/*',
                'clients/<%= client %>/dist/scripts/*']
            },
            hapiServerResources : {
                src: ['server/public/ibe/*']
            },
            server: '.tmp'
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            server: {
                options: {
                    map: true
                },
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            }
        },

        // Automatically inject Bower components into the app
        wiredep: {
            app: {
                src: ['<%= yeoman.app %>/index.html'],
                ignorePath:  /\.\.\//
            },
            test: {
                devDependencies: true,
                src: '<%= karma.unit.configFile %>',
                ignorePath:  /\.\.\//,
                fileTypes:{
                    js: {
                        block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
                        detect: {
                            js: /'(.*\.js)'/gi
                        },
                        replace: {
                            js: '\'{{filePath}}\','
                        }
                    }
                }
            },
            sass: {
                src: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
                ignorePath: /(\.\.\/){1,2}bower_components\//
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        compass: {
            options: {
                sassDir: '<%= yeoman.app %>/styles',
                cssDir: '<%= yeoman.app %>/styles',
                generatedImagesDir: '.tmp/images/generated',
                imagesDir: '<%= yeoman.app %>/images',
                javascriptsDir: '<%= yeoman.app %>/js',
                fontsDir: '<%= yeoman.app %>/fonts',
                //importPath: './bower_components',
                httpImagesPath: '/images',
                httpGeneratedImagesPath: '/images/generated',
                httpFontsPath: '/styles/fonts',
                relativeAssets: false,
                assetCacheBuster: false,
                raw: 'Sass::Script::Number.precision = 10\n'
            },
            dist: {
                options: {
                    generatedImagesDir: '<%= yeoman.dist %>/images/generated'
                }
            },
            server: {
                options: {
                    sourcemap: true
                }
            }
        },

        // Renames files for browser caching purposes
        filerev: {
            options: {
                algorithm: 'sha256',
                length: 5
              },
            dist: {
                src: [
                    '<%= yeoman.dist %>/scripts/{,*/}*.js',
                    '<%= yeoman.dist %>/styles/styles.css',
                    '<%= yeoman.dist %>/styles/main.css',
                    '<%= yeoman.dist %>/styles/dashboard.css'
                    //'<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    // '<%= yeoman.dist %>/styles/fonts/*'
                ]
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: '<%= yeoman.app %>/reservation.html',
            options: {
                dest: '<%= yeoman.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'], //Add or Remove ,'uglifyjs'  for enable or disable js minification
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        useminPrepareDist: {
          html: '<%= yeoman.app %>/reservation.html',
          options: {
              dest: '<%= yeoman.dist %>',
              flow: {
                  html: {
                      steps: {
                          js: ['uglifyjs'], //Add or Remove ,'uglifyjs'  for enable or disable js minification
                          css: ['cssmin']
                      },
                      post: {}
                  }
              }
          }
        },

        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%= yeoman.dist %>/{,**/}*.html'],
            css: ['<%= yeoman.dist %>/styles/{,**/}*.css'],
            js: ['<%= yeoman.dist %>/scripts/{,**/}*.js',
		 '<%= yeoman.dist %>/js/{,**/}*.js'],
            options: {
                assetsDirs: [
                    '<%= yeoman.dist %>',
                    '<%= yeoman.dist %>/images',
                    '<%= yeoman.dist %>/styles'
                ],
                patterns: {
                    js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
                }
            }
        },

        // The following *-min tasks will produce minified files in the dist folder
        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        // cssmin: {
        //   dist: {
        //     files: {
        //       '<%= yeoman.dist %>/styles/main.css': [
        //         '.tmp/styles/{,*/}*.css'
        //       ]
        //     }
        //   }
        // },
        /*uglify: {
           dist: {
             files: {
               '.tmp/concat/scripts/scripts.js': [
                 '<%= yeoman.dist %>/scripts/scripts.js'
               ]
             }
           }
         }*/
        /**
         * bower components file path, place all bower components file here
         */
        concat: {
            jsFiles: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/jquery-ui/jquery-ui.js',
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-ui-date/dist/date.js',
                    'bower_components/angular-animate/angular-animate.min.js',
                    'bower_components/angular-messages/angular-messages.min.js',
                    'bower_components/angular-sanitize/angular-sanitize.min.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'bower_components/angular-ui-mask/dist/mask.min.js',
                    'bower_components/angular-cookies/angular-cookies.min.js',
                    'bower_components/angular-resource/angular-resource.min.js',
                    'bower_components/angular-touch/angular-touch.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'bower_components/angular-bootstrap/ui-bootstrap.min.js',
                    'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'bower_components/jquery-sticky/jquery.sticky.js',
                    'bower_components/angular-translate/angular-translate.min.js',
                    'bower_components/angular-translate-loader-url/angular-translate-loader-url.min.js',
                    'bower_components/moment/min/moment.min.js',
                    'bower_components/angularjs-slider/dist/rzslider.min.js',
                    'bower_components/angular-recaptcha/release/angular-recaptcha.min.js',
                    'bower_components/angular-local-storage/dist/angular-local-storage.min.js',
                    'bower_components/valdr/valdr.min.js',
                    'bower_components/valdr/valdr-message.min.js',
                    'bower_components/underscore/underscore-min.js',
                    'bower_components/ng-dialog/js/ngDialog.min.js',
                    'bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js'
                    //'bower_components/adm-dtp/dist/ADM-dateTimePicker.min.js'
                ],
                dest: 'src/js/libs/scripts/bower_libs.js'
            },
            cssFiles:{
                src: [
                    'bower_components/bootstrap/dist/css/bootstrap.min.css',
                    'bower_components/ng-dialog/css/ngDialog.min.css',
                    'bower_components/ng-dialog/css/ngDialog-theme-default.min.css',
                    'bower_components/jquery-ui/themes/smoothness/jquery-ui.css',
                    'bower_components/angular-bootstrap-colorpicker/css/colorpicker.min.css',
                    'bower_components/components-font-awesome/css/font-awesome.min.css',
                    'bower_components/angularjs-slider/dist/rzslider.min.css'
                   // 'bower_components/adm-dtp/dist/ADM-dateTimePicker.min.css'
                ],
                dest: 'src/js/libs/styles/bower_styles.css'
            }
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.{png,jpg,jpeg,gif}',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>',
                    src: ['{,**/}*.html'],
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        ngtemplates: {
            isaApp: {
                options: {
                    //module: 'onlineCheckInApp',
                    htmlmin: '<%= htmlmin.dist.options %>'
                    //usemin: 'scripts/scripts.js'
                },
                cwd: '<%= yeoman.app %>',
                src: 'modules/{,**/}/templates/*.html',
                dest: '<%= yeoman.app %>/js/commonServices/templateCache.js'
            }
        },

        // ng-annotate tries to make the code safe for minification automatically
        // by using the Angular long form for dependency injection.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/scripts',
                    src: '*.js',
                    dest: '.tmp/concat/scripts'
                }]
            }
        },

        // Replace Google CDN references
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/*.html']
            }
        },
        sync: {
            updateCommonStyle:{
                files: [{
                    cwd: '<%= yeoman.app %>/styles',
                    src: [
                        '**/*', /* Include everything */
                        '!**/_client_style.scss',
                        '!**/_colors.scss',
                        '!**/theme.*',
                        '!**/*.css',
                        '!**/*.css.map'
                    ],
                    dest: 'clients/commonStyles/styles'
                }],
                pretend: false, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
                verbose: true, // Display log messages when copying files
                compareUsing: "md5"
            },
            updateClientSource: {
                files: [{
                    cwd: '<%= yeoman.app %>/styles',
                    src: [
                        '**/*', /* Include everything */
                        '!**/_client_style.scss',
                        '!**/_colors.scss',
                        '!**/theme.*',
                        '!**/*.css',
                        '!**/*.css.map'
                    ],
                    dest: 'clients/commonStyles/styles'
                },{
                    cwd: '<%= yeoman.app %>/styles',
                    src: [
                        '**/_client_style.scss',
                        '**/_colors.scss',
                        '**/theme.*'
                    ],
                    dest: 'clients/<%= client %>/styles'
                },{
                    cwd: '<%= yeoman.app %>/i18n',
                    src: '**/override.json',
                    dest: 'clients/<%= client %>/i18n'
                }],
                pretend: false, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
                verbose: true, // Display log messages when copying files
                compareUsing: "md5"
            },
            updateAirarabiaSource: {
                files: [{
                    cwd: '<%= yeoman.app %>/styles',
                    src: [
                        '**/*', /* Include everything */
                        '!**/_client_style.scss',
                        '!**/theme.*',
                    ],
                    dest: 'clients/AirarabiaCommon/styles'
                }],
                pretend: false, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
                verbose: true // Display log messages when copying files
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            clientJson:{
                files:[
                {
                    //copy common client styles to the source
                    expand: false,
                    dest: '<%= yeoman.app %>/client.json',
                    src: 'clients/<%= client %>/json/client.json'
                }]
            },
            clients:{
                files:[
                    {
                        //copy common client styles to the source
                        expand: true,
                        cwd: 'clients/<%= client %>/styles',
                        dest: '<%= yeoman.app %>/styles',
                        src: '**/*'
                    },

                    {   //copy clients sass files to the source
                        expand: false,
                        dest: '<%= yeoman.app %>/styles/common/theme.scss',
                        src: 'clients/<%= client %>/styles/common/theme.scss'
                    },

                    {
                        //copy clients images to the source
                        expand: true,
                        cwd: 'clients/<%= client %>/images',
                        dest: '<%= yeoman.app %>/images',
                        src: '**/*'
                    },
                    {
                        //copy clients images to the source
                        expand: true,
                        cwd: 'clients/<%= client %>/i18n',
                        dest: '<%= yeoman.app %>/i18n',
                        src: '**/*'
                    },
                    {
                        //copy common client styles to the source
                        expand: true,
                        cwd: 'clients/commonStyles/styles',
                        dest: '<%= yeoman.app %>/styles',
                        src: '**/*'
                    },
                    {
                        //copy common client styles to the source
                        expand: false,
                        dest: '<%= yeoman.app %>/client.json',
                        src: 'clients/<%= client %>/json/client.json'
                    }
                ]
            },
            airArabiaClient:{
                files:[
                    {
                        //copy Airarabia styles to the source
                        expand: true,
                        cwd: 'clients/AirarabiaCommon/styles',
                        dest: '<%= yeoman.app %>/styles',
                        src: '**/*'
                    },
                    {
                        //copy Airarabia images to the source
                        expand: true,
                        cwd: 'clients/AirarabiaCommon/images',
                        dest: '<%= yeoman.app %>/images',
                        src: '**/*'
                    },
                    {
                        //copy Airarabia images to the source
                        expand: true,
                        cwd: 'clients/AirarabiaCommon/i18n',
                        dest: '<%= yeoman.app %>/i18n',
                        src: '**/*'
                    },
                    {
                        //copy common client styles to the source
                        expand: false,
                        dest: '<%= yeoman.app %>/client.json',
                        src: 'clients/<%= client %>/json/client.json'
                    },
                    {
                        //copy dist html to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>',
                        dest: 'clients/<%= client %>/dist',
                        src: ['index.html','reservation.html']
                    },
                    {
                        //copy dist scripts to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>/scripts',
                        dest: 'clients/<%= client %>/dist/scripts',
                        src: '**/*'
                    }
                ]
            },
            clientdist:{
                files:[
                    {
                        //copy dist style to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>/styles',
                        dest: 'clients/<%= client %>/dist/styles',
                        src: '**/*'
                    },
                    {
                        //copy dist style to the client dist
                        expand: true,
                        cwd: 'clients/<%= client %>/images',
                        dest: 'clients/<%= client %>/dist/images',
                        src: '**/*'
                    },
                    {
                        //copy dist html to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>',
                        dest: 'clients/<%= client %>/dist',
                        src: ['index.html','reservation.html']
                    },
                    {
                        //copy dist scripts to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>/scripts',
                        dest: 'clients/<%= client %>/dist/scripts',
                        src: '**/*'
                    },
                    {
                        //copy dist scripts to the client dist
                        expand: true,
                        cwd: '<%= yeoman.app %>',
                        dest: 'clients/<%= client %>/dist',
                        src: ['client.json']
                    },
                    {
                        //copy dist scripts to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>/i18n',
                        dest: 'clients/<%= client %>/dist/i18n',
                        src: '**/*'
                    },
                    {
                        //copy dist scripts to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>/i18n/en',
                        dest: 'clients/<%= client %>/i18n/en',
                        src: ['override.json']
                    }

                ]
            },
            airArabiadist:{
                files:[
                    {
                        //copy dist style to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>/styles',
                        dest: 'clients/AirarabiaDist/styles',
                        src: '**/*'
                    },
                    {
                        //copy dist style to the client dist
                        expand: true,
                        cwd: '<%= yeoman.dist %>/images',
                        dest: 'clients/AirarabiaDist/images',
                        src: '**/*'
                    }

                ]
            },
            //updateClientSource:{
            //    //copy source common file in to client
            //    expand: true,
            //    cwd: '<%= yeoman.app %>/styles',
            //    dest: 'clients/commonStyles/styles',
            //    src: '**/*'
            //},
            //updateAirarabiaSource:{
            //    //copy source common file in to client
            //    expand: true,
            //    cwd: '<%= yeoman.app %>/styles',
            //    dest: 'clients/AirarabiaCommon/styles',
            //    src: '**/*'
            //},
            createClient:{
                files:[
                    {   //create new client from ISA
                        expand: true,
                        cwd:"clients/ISA/",
                        dest: 'clients/<%= client %>/',
                        src: '**/*'
                    }
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '.htaccess',
                        '*.html',
                        'modules/{,**/}*.html',
                        'i18n/**/*.json',
                        '*.properties',
                        'js/util/isaui_lib/dist/{,**/}*.*',
                        'js/libs/{,**/}*.*',
                        'js/dummyPage.js',
                        'fonts/*.*',
	                    '*.json',
                        'views/{,**/}*.html',
                        'partials/{,**/}*.html',
                        'scripts/accelaero-ui/partials/{,**/}*.*',
                        'sample_json/{,**/}*.json',
                        'images/{,*/}*.{png,jpg,jpeg,gif,svg}',
                        'styles/fonts/{,*/}*.*',
                        'styles/{,**/}*.css'
                    ]
                }, {
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: 'clients/<%= client %>',
                    src: 'styles/common/_client_style.scss'
                }, {
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: 'clients/<%= client %>/json',
                    src: 'client.json'
                }, {
                    expand: true,
                    cwd: 'bower_components/jquery-ui/themes/smoothness',
                    src: 'images/*',
                    dest: '<%= yeoman.app %>/js/libs/styles/images'
                }, {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/images',
                    src: [
                        'generated/*'
                    ]
                }, {
                    expand: true,
                    cwd: 'bower_components/bootstrap/dist',
                    src: 'fonts/*',
                    dest: '<%= yeoman.dist %>'
                }, {
                    expand: true,
                    cwd: 'bower_components/font-awesome',
                    src: 'fonts/*',
                    dest: '<%= yeoman.dist %>'
                },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/js/libs/styles/',
                        src: 'bower_styles.css',
                        dest: '<%= yeoman.dist %>/styles'
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/scripts/accelaero-ui/images/',
                        dest: '<%= yeoman.dist %>/images',
                        src: [
                            '{,**/}*.{png,jpg,jpeg,gif}'
                        ]
                    }]
            },
            sourcehtml:{
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: ['src/*.{html}'] }]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            },
            bower_fonts:{
                expand: true,
                cwd: 'bower_components/bootstrap/dist',
                dest: '<%= yeoman.app %>/js/libs',
                src: ['fonts/*']
            },
            jquery_ui:{
                expand: true,
                cwd: 'bower_components/jquery-ui/themes/smoothness',
                src: 'images/*',
                dest: '<%= yeoman.app %>/js/libs/styles'
            },
            staging : {
                expand: true,
                cwd : '<%= yeoman.app %>',
                dest: '<%= yeoman.dist %>',
                src : [
                    '*.html',
                    'styles/{,**/}*.css',
                    'fonts/*.*',
                    'images/{,**/}*.*',
                    'js/{,**/}*.*',
                    'js/util/isaui_lib/dist/{,**/}*.*',
                    'js/libs/{,**/}*.*',
                    'js/commonServices/{,**/}*.*',
                    'modules/{,**/}*.js',
                    'sampleData/{,**/}*.*',
                    'services/{,**/}*.*'
                ]
            },

            commonHapiServerResources: {
                files:[
                    {
                        expand: true,
                        cwd:"<%= yeoman.dist %>",
                        dest: 'server/public/ibe',
                        src: '**/*'
                    }
                ]
            },

            clientSpecificHapiServerResources: {
                files:[
                    {
                        expand: true,
                        cwd:"clients/<%= client %>/dist/",
                        dest: 'server/public/ibe',
                        src: '**/*'
                    }
                ]
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: [
                'copy:clients','compass:server'
            ],
            test: [
                'compass'
            ],
            dist: [
                'compass:dist',
                //'imagemin',
                'svgmin'
            ]
        },

        // Test settings
        karma: {
            unit: {
                configFile: 'test/karma.conf.js',
                singleRun: true
            }
        }
    });
    grunt.registerTask('setMode', function (key, value) {
        var appConfig = "src/themeProperty.json";


        if (!grunt.file.exists(appConfig)) {
            grunt.log.error("file " + appConfig + " not found");
            return true;//return false to abort the execution
        }
        var configProp = grunt.file.readJSON(appConfig);//get file as json object

        configProp["themeEnginStatus"]= themeable;//edit the value of json object, you can also use projec.key if you know what you are updating

        grunt.file.write(appConfig, JSON.stringify(configProp, null, 2));//serialize it back to file

        //grunt.task.run(['copy:createClient'])

    });
    grunt.registerTask('createClient',['copy:createClient'])
    grunt.registerTask('resetClient',['clean:clients'])
    grunt.registerTask('useminPrepareDist', function () {
      var useminPrepareDistConfig = grunt.config('useminPrepareDist');
      grunt.config.set('useminPrepare', useminPrepareDistConfig);
      grunt.task.run('useminPrepare');
    });

    grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
        console.log('--------------------------------**************----------------');
        console.log(target)
        grunt.task.requires('resetClient');
        if (target === 'dist') {
            return grunt.task.run(['staging', 'connect:dist:keepalive']);
        }
        grunt.task.run([ 'clean:server']);
        if(false){//client === '3O' || client  == '9P' || client === 'E5'
           grunt.registerTask('makeAirarabiaClient',['copy:airArabiaClient','compass:server']);
           grunt.task.run(['makeAirarabiaClient:server']);
        }else{
            grunt.registerTask('makeClient',['copy:clients','compass:server']);
            grunt.task.run(['makeClient:server']);
        }
        console.log('---------------->'+client);

        if (disableLiveLoad) {
            grunt.task.run([
                'autoprefixer:server',
                'concat',
                'copy:bower_fonts',
                'copy:jquery_ui'
            ]);
        } else {
            grunt.task.run([
                'ngtemplates',
                'configureProxies:server', //Uncomment to set proxy
                'autoprefixer:server',
                'concat',
                'copy:bower_fonts',
                'copy:jquery_ui',
                'connect:livereload',
                'watch'
            ]);
        }
    });

    grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve:' + target]);
    });

    grunt.registerTask('test', [
        'clean:server',
        //'wiredep',
        'concurrent:test',
        'configureProxies:server',
        'autoprefixer',
        'connect:test',
        'karma'
    ]);

    grunt.registerTask('distribute','Final destribution',function(){
        console.log(clientData.code)
        if(clientData.code == client){
            grunt.task.run([
                'clean:dist',
                //'wiredep',
                'autoprefixer',
                //'ngtemplates',
                //'concat',
                'ngAnnotate',
                'copy:dist'
                ]);
            if(minification == true){
                grunt.task.run(['useminPrepareDist','cssmin','uglify','filerev','usemin','htmlmin','clean:clientDist','copy:clientdist']);
            }else{
                grunt.task.run(['copy:sourcehtml','clean:clientDist','copy:clientdist']);
            }
            if(false){//client  == '3O' || client  == '9P' || client  == 'E5'
                grunt.task.run('sync:updateAirarabiaSource');
            }else{
                grunt.task.run(['sync:updateClientSource', 'clean:clients']);
            }
            if(syncHapiServer){
            	grunt.task.run('syncHapiServer');
            }
        }else{
            grunt.fail.warn('Client does not mach Please create client before distribute')
        }
    })

    grunt.registerTask('syncHapiServer','Clean and copy resources in happ server resources',function(){
        console.log(clientData.code);
        grunt.task.run('clean:hapiServerResources');
        if(clientData.code == client){
            grunt.task.run('copy:commonHapiServerResources');
            grunt.task.run('copy:clientSpecificHapiServerResources');
        }else{
            grunt.fail.warn('Client does not mach Please create client before distribute')
        }
    });



    grunt.registerTask('build', [
        'clean:dist',
        //'wiredep',
        'useminPrepareDist',
        'concurrent:dist',
        'autoprefixer',
        //'ngtemplates',
        //'concat',
        'ngAnnotate',
        'copy:dist',
        //'cdnify',
        'cssmin',
        'uglify', //Comment or Uncomment this line for ignore js modification
        'filerev', //Comment or Uncomment this line for ignore js modification
        'usemin',
        'htmlmin'
    ]);

    grunt.registerTask('staging',[
        'clean:dist',
        'useminPrepare',
        // 'concurrent:test',
        'concat',
        'copy:staging',
        'ngtemplates',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        //'htmlmin'
    ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'
    ]);


    grunt.registerTask('compileSass',function(){
        grunt.task.run([
            'clean:cssStyles',
            'compass:server'
        ])
    })

    grunt.registerTask('updateCommon',function(){
        grunt.task.run([
            'sync:updateCommonStyle'
        ])
    })


    grunt.registerTask('buildToProd',function(){
        
        if(clientData.code == client){
            grunt.task.run(
            [
                'ngtemplates',
                'sync:updateCommonStyle',// Copy the styles to Common in case the developer didn't do it
                'copy:clients',
                'copy:bower_fonts',
                'copy:jquery_ui',
                'compileSass',
                'clean:dist',
                'autoprefixer',
                'ngAnnotate',
                'sync:updateClientSource',
                'copy:dist'      
            ])
            if(minification == true){
                grunt.task.run(['useminPrepareDist',
                                'cssmin',
                                'uglify',
                                'filerev',
                                'usemin',
                                'htmlmin']);
            }else{
                grunt.task.run([
                    'copy:sourcehtml']);
            }
            if(clientData.tagscript != "" && clientData.tagscript.length>0){
            	 grunt.task.run(['processhtml']);
            }
            
            grunt.task.run([
                    'clean:clientDist',
                    'copy:clientdist', 'clean:clients' ]);
            
        }else{
            grunt.fail.warn('Client does not mach Please create client before distribute')
        }
    })
};
