/**
 * Created by sumudu on 1/19/16.
 */
'use strict';

(function (angular) {
    angular.module('modification.common')

        //Modification related Common Services
        .service('modificationCommonService', [function () {

        }])

        //Modification related Remote service call params factory
        .factory('modificationModelFactory', [function () {

            var loadReservationRQ = function () {
                this.pnr = "";
                this.lastName = "";
                this.departureDateTime = "";
            };

            var cancelReservationReq = function () {
                this.pnr = "";
                this.version = "0";
            };

            return {
                CancelReservationReq: function () {
                    return new cancelReservationReq()
                },
                LoadReservationRQ: function () {
                    return new loadReservationRQ();
                }
            };
        }]);

})(angular);