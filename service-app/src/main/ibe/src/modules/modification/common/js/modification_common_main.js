/**
 * Created by sumudu on 1/19/16.
 */
'use strict';
(function(angular){
    angular.module('modification.common',[]);
    angular.module('modification.ancillary',[]);
})(angular);