/**
 * Created by sumudu on 1/20/16.
 */
'use strict';

(function (angular) {
    var module = angular.module('modification.reservation')


        /**
         * ********************************************************************
         * Modify reservation controller
         * @param $stateParams
         * @param modifyReservationService
         * @param quoteService
         * @param IBEDataConsole
         * ********************************************************************
         */
        .controller('ModifyReservationController', [
            '$stateParams',
            '$state',
            'modifyReservationService',
            'quoteService',
            'languageService',
            'currencyService',
            'passengerService',
            'applicationService',
            'travelFareService',
            'commonLocalStorageService',
            'IBEDataConsole',
            'extrasService',
            'ancillaryRemoteService',
            'loadingService',
            'alertService',
            'navigationService',
            '$timeout',
            '$sce',
            'popupService',
            'confirmService',
            '$window',
            'constantFactory',
            'dateTransformationService',
            'PersianDateFormatService',
            '$translate',
            'ancillaryService',
            '$rootScope',
            'signoutServiceCheck',
            function modifyReservationController($stateParams, $state, modifyReservationService, quoteService, languageService, currencyService, passengerService, applicationService, travelFareService, commonLocalStorageService, IBEDataConsole, extrasService, ancillaryRemoteService, loadingService, alertService, navigationService, $timeout ,$sce, popupService, confirmService, $window,constantFactory,dateTransformationService,PersianDateFormatService,$translate,ancillaryService,$rootScope,signoutServiceCheck) {

                var self = this;

                self.isUserLoggedIn; // Holds user logged in status.
                signoutServiceCheck.checkSessionExpiry();
                self.showAncillaryList = true; // Hold ancy list visibility.
                self.charges; // Holds charges break down.
                self.isForcedConfirm; // Holds forced confirm status.

                self.paymentGatewayCurrency = currencyService.getAppDefaultPGCurrency();
                self.baseCurrency = currencyService.getAppBaseCurrency();
                self.chargeTypes = {"Credit Balance":'lbl_credit_balance'}
                self.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');
                self.mbottom = '20px';
                self.ondPreferrences={
                    ondId:[],
                    flighSegmentRPH:[]
                }
                this.duplicateIndexLength = {
                    baggageDuplicateIndex : 0,
                    seatDuplicateIndex : 0,
                    mealDuplicateIndex : 0,
                    inflightDuplicateIndex : 0,
                    insuranceDuplicateIndex : 0,
                    airportDuplicateIndex : 0,
                    flexiDuplicateIndex : 0
                }
                this.duplicateIndex = {
                    baggageDuplicateIndex : -1,
                    seatDuplicateIndex : -1,
                    mealDuplicateIndex : -1,
                    inflightDuplicateIndex : -1,
                    insuranceDuplicateIndex : -1,
                    airportDuplicateIndex : -1,
                    flexiDuplicateIndex : -1
            };
                this.model = {
                    bookingDetails: {},
                    passengerDetails: {},
                    contacts: {},
                    ancillaries: [],
                    reservationSelectedAncillaries: [],
                    reservationSegments: [],
                    reservationStatus: '',
                    isNameChangeEnable: false,
                    isSegmentModificationEnable: false,
                    isAncillaryModificationEnable: false,
                    isCancelReservationEnable: false,
                    isPayAndConfirmEnable: false,
                    isContactEnable: false,
                    isTaxInvoiceGenerated: false,
                    isUserLoggedIn :  applicationService.getCustomerLoggedState(),
                    advertisements :{},
                    reservationSegments2: []
                };
                var buttonLabels = {
                           'SELECT_NOW': "Select Now",
                           'EDIT_SELECTION': "Edit Selection"
                       };
                 
                var setStatusFlags = function (responseData) {
                    if (self.model.bookingDetails.statusCode == modifyReservationService.RESERVATION_STATUS.ON_HOLD) {
                        self.model.isPayAndConfirmEnable = true;
                        applicationService.setApplicationMode(applicationService.APP_MODES.BALANCE_PAYMENT);
                    } else {
                        self.model.isPayAndConfirmEnable = false;
                    }

                    if ([modifyReservationService.RESERVATION_STATUS.CANCEL, modifyReservationService.RESERVATION_STATUS.ON_HOLD].indexOf(self.model.bookingDetails.statusCode) >= 0) {

                        $timeout(function () {
                            self.model.isNameChangeEnable = false;
                            self.model.isCancelReservationEnable = false;
                            self.model.isSegmentModificationEnable = false;
                        },0);

                        self.model.isAncillaryModificationEnable = false;
                        self.model.isContactEnable = false;
                    } else {
                        self.model.isNameChangeEnable = true;
                        self.model.isCancelReservationEnable = true;
                        self.model.isSegmentModificationEnable = true;
                        self.model.isAncillaryModificationEnable = true;
                        self.model.isContactEnable = true;
                    }

                    self.model.isNameChangeEnable = self.model.statusParams.isNameChangeEnable;
                    self.model.isSegmentModificationEnable = self.model.statusParams.isSegmentModificationEnable;
                    if(responseData.modificationParams && responseData.modificationParams.all && responseData.modificationParams.all.reservationModificationParams && responseData.modificationParams.all.reservationModificationParams.cancellable) {
                        self.model.isCancelReservationEnable = true;
                    }else{
                        self.model.isCancelReservationEnable = false;
                    }

                    if (responseData && responseData.bookingDetails.balanceAvailable) {
                        self.model.bookingDetails.status = "ON-HOLD";
                        self.model.isCancelReservationEnable = false;
                        self.model.isSegmentModificationEnable = false;
                        self.model.isPayAndConfirmEnable = true;
                        self.isBalanceAvailable = true;
                    }
                    if(responseData.bookingDetails.balanceAvailable){
                        self.model.isContactEnable = false;
                        self.model.isNameChangeEnable = false;
                    }
                };
                    var dialogTranslate = {
                               popupblocker: "Please disable pop-up blocker to print reservation",
                           };
                var goToLookup = function () {
                    $state.go('reservationLookup', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        pnr: $stateParams.pnr,
                        lastName: $stateParams.lastName,
                        departureDate: $stateParams.departureDate
                    });
                };

                var goToDashboard = function(){
                    $state.go('modifyDashboard', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode : applicationService.getApplicationMode()
                    });
                };
                var updatePaxcount = function(paxCountTxt){
                    if(paxCountTxt.indexOf('adult') !== -1){
                        paxCountTxt = paxCountTxt.replace('adult','Adult');
                    }
                    if(paxCountTxt.indexOf('child') !== -1){
                        paxCountTxt = paxCountTxt.replace('child','Child');
                    }
                    if(paxCountTxt.indexOf('infant') !== -1){
                        paxCountTxt = paxCountTxt.replace('infant','Infant');
                    }
                    self.model.passengerDetails.paxCountTxt = paxCountTxt;
                }
                modifyReservationService.getAncillaryAvailability(function(response){
                	applicationService.setTransactionID(null);
                    var d = _.where(response.availableAncillariesResponse.availability,{available:true});
                    var availability = (d.length) ? true : false;
                    console.log(availability)
                    commonLocalStorageService.setData("isAncilaryAvailable", availability);
                },function(error){
                })

                this.loadAncillarySummery = function () {
                    modifyReservationService.getAncillarySummery(
                        function (ancillaries) {
                        	var selectedConfirmAnci = [];
                            self.model.ancillaries = ancillaries;
                            angular.forEach(ancillaries, function(anci){ 
                            	if(anci.isSelected){
                            		selectedConfirmAnci.push(anci.ancillaryType);
                            	}
                            })  
                            if(self.model.flexiInfoList.length > 0){
                            	selectedConfirmAnci.push('FLEXIBILITY');
                             }	  
                            quoteService.setSelectedAnci(selectedConfirmAnci);
                            loadingService.hidePageLoading();
                        },
                        function (error) {
                            loadingService.hidePageLoading();
                            IBEDataConsole.log("GET ANCILLARY SUMMERY ERROR");
                        }
                    );
                };

                /**
                 * Redirect user to home page.
                 */
                this.backToHome = function () {
                    // Since we hide the link if user is not logged in, no need to
                    // handle the else part.
                    if (applicationService.getCustomerLoggedState()) {
                        navigationService.userHome();
                    }
                };

                function getTranslatedDateDayAndMonth(monthsArray, daysArray, dateToBeTranslated){
                	var daysArrayTemp = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
                   	var dateForTranslation = moment(dateToBeTranslated);
                   	var dayOfTheWeek = dateForTranslation.day();
                   	var month = dateForTranslation.month();
                   	var translatedDayOfWeek = daysArrayTemp[dayOfTheWeek].replace(' ','');
                   	var translatedMonth = monthsArray[month].replace(' ','');
                   	var translatedDate = translatedDayOfWeek + ' ' + (dateForTranslation.date()<9?'0'+dateForTranslation.date():dateForTranslation.date()) + ' ' + translatedMonth + ' ' + dateForTranslation.year();
                    return translatedDate;
                }
                function getTranslatedDateMonth(monthsArray, dateToBeTranslated){
                	var dateForTranslation = moment(dateToBeTranslated);                   	
                   	var month = dateForTranslation.month();
                   	var translatedMonth = monthsArray[month].replace(' ','');
                   	var translatedDate = (dateForTranslation.date()<9?'0'+dateForTranslation.date():dateForTranslation.date()) + ' ' + translatedMonth + ' ' + dateForTranslation.year();                    
                   	return translatedDate;
                }
                this.loadReservation = function () {
                    ancillaryRemoteService.clearExtras();
                    loadingService.showPageLoading();
                    modifyReservationService.loadReservation($stateParams.pnr, $stateParams.lastName, $stateParams.departureDate,
                        function onSuccess(data) {
                            var lang = data.bookingDetails.prefferedLanguage
                            if(lang !== null && lang !== "en"){
                                var url = window.location.href;
                                var newUrl = url.replace('/reservation/en/', '/reservation/' + lang + '/');
                                window.location = newUrl;
                            }
                            if (!!data.errors && data.errors.length > 0) {
                                if (self.model.isUserLoggedIn) {
                                    alertService.setAlert(data.errors[0],'danger');
                                    goToDashboard();
                                }else{
                                    goToLookup();
                                    alertService.setAlert(data.errors[0],'danger');
                                }
                            } else {
                            	$translate.onReady().then(function(){
                                 var daysArrayString = $translate.instant('arr_common_days');
                                 var monthArrayString = $translate.instant('arr_common_months');
                                 if(daysArrayString.indexOf('arr') < 0 && monthArrayString.indexOf('arr') < 0)
                                 {
                                  var daysArray = daysArrayString.split(';');
                                  var monthsArray = monthArrayString.split(';');
                                  var bookingDateString = data.bookingDetails.bookingDate;                                  
                                  commonLocalStorageService.setData("APP_MODE", applicationService.getApplicationMode());
                                  commonLocalStorageService.setData('PNR', $stateParams.pnr);
                                  modifyReservationService.setBalanceSummaryForOnhold(data.balanceSummary);
                                  var dateWithTranslatedMonth = getTranslatedDateMonth(monthsArray,data.bookingDetails.bookingDate);
                                  self.model.bookingDetails = data.bookingDetails;
                                  self.model.bookingDetails.bookingDateTxt = dateWithTranslatedMonth;
                                  self.model.passengerDetails = data.passengerDetails;
                                  updatePaxcount(self.model.passengerDetails.paxCountTxt);
                                  self.model.reservationSelectedAncillaries = data.ancillaries;                                   
                                  for(var segment = 0;segment < data.segments.length; segment++)
                                  {
                                	  var translatedDate = getTranslatedDateDayAndMonth(monthsArray,daysArray,data.segments[segment].flight.arrivalDateObj);
                                	  data.segments[segment].flight.arrivalDateTxt = angular.copy(translatedDate);
                                	  translatedDate = undefined;
                                	  translatedDate = getTranslatedDateDayAndMonth(monthsArray,daysArray,data.segments[segment].flight.departureDateObj);
                                	  data.segments[segment].flight.departureDateTxt = angular.copy(translatedDate);
                                  }                                  
                                  self.model.reservationSegments = data.segments;
                                  self.model.contactInfo = data.contactInfo;
                                  self.model.statusParams = data.modificationParams;
                                  self.model.isCancellable = data.isCancellable;
                                  self.model.advertisements = data.advertisements;
                                  self.model.promoInfo = data.promoInfo;
                                  self.model.flexiInfoList = data.flexiInfoList;
                                  self.model.isTaxInvoiceGenerated = data.taxInvoiceGenerated;
                                  self.selectedAncillaries = data.ancillaries;
                                  self.selectedAncillaries2 = data.ancillaries;


                                
                                IBEDataConsole.log("LOAD RESERVATION SUCCESSFUL");
                                if(applicationService.getIsPersianCalendar()){
                                	var formatedDate =PersianDateFormatService.reformatDateString(self.model.bookingDetails.bookingDate,false);
                                	var dateString = formatedDate.split("-");
                                	var jalaaliObject = dateTransformationService.toJalaali(parseInt(dateString[0]),parseInt(dateString[1]),parseInt(dateString[2]));
                                	self.model.bookingDetails.bookingDate = jalaaliObject.jy+"-"+jalaaliObject.jm+"-"+jalaaliObject.jd;
                                	for(var i=0;i<data.segments.length;i++){
                                        var arrivalTimeArray = data.segments[i].flight.arrivalDateObj;
                                        var departureTimeArray = data.segments[i].flight.departureDateObj;
                                         arrivalTimeArray = arrivalTimeArray.replace(/:/, '-'); // replaces first ":" character
                                         arrivalTimeArray = arrivalTimeArray.replace(/:/, '-'); // replaces second ":" character
                                         
                                         departureTimeArray = departureTimeArray.replace(/:/, '-'); // replaces first ":" character
                                         departureTimeArray = departureTimeArray.replace(/:/, '-'); // replaces second ":" character
                                         
                                         var reservArray = arrivalTimeArray.split("-");
                                         
                                         var reservDepArray = departureTimeArray.split("-");
                                         
                                         jalaaliObject = dateTransformationService.toJalaali(parseInt(reservArray[0]),parseInt(reservArray[1]),parseInt(reservArray[2].split("T")[0]));
                                          
                                         var jalaaliObject2 = dateTransformationService.toJalaali(parseInt(reservDepArray[0]),parseInt(reservDepArray[1]),parseInt(reservDepArray[2].split("T")[0]));
                                          
                                          
                                         self.model.reservationSegments[i].flight.arrivalDate =jalaaliObject.jd+"-"+jalaaliObject.jm+"-"+jalaaliObject.jy;
                                          
                                         self.model.reservationSegments[i].flight.departureDate =jalaaliObject2.jd+"-"+jalaaliObject2.jm+"-"+jalaaliObject2.jy;
                                          
                                     }
                                	
                                }

                                var charges = data.balanceSummary.chargeBreakdown;

                                // Total Charges, is sending as the last element in array.
                                // using isTotal flag to set 'total' class to LI tag.
                                if (charges && charges.length > 0) {
                                    var obj = charges[charges.length - 1];
                                    obj.isTotal = true;
                                    charges[charges.length - 1] = obj;
                                    self.charges = charges;
                                }

                                //Set Pax Info from reservation info
                                var adaptedPaxDetails = passengerService.getPaxDetailsFromReservation(self.model.passengerDetails.allPassengers);
                                adaptedPaxDetails.paxContact =  self.model.contactInfo;
                                passengerService.setPaxDetails(adaptedPaxDetails);

                                //Set Segment Info
                                modifyReservationService.setReservation(data);
                                modifyReservationService.setConfirmedFlightData(self.model.reservationSegments);

                                //Change UI Base on Reservation Status
                                setStatusFlags(data);

                                // Setting currency for the modification flow if provided in the load response
                                if(data.paymentDetails.currency){
                                    currencyService.setSelectedCurrency(data.paymentDetails.currency);
                                    self.paymentGatewayCurrency = data.paymentDetails.currency;
				}

                                self.loadAncillarySummery();
                                for(var i=0;i<data.ondPreferences.length;i++){
                                    self.ondPreferrences.ondId[i]=data.ondPreferences[i].ondId;
                                    self.ondPreferrences.flighSegmentRPH[i]=data.ondPreferences[i].flightSegmentRPH[0];
                                }
                                
                                for(var i=0;i<data.segments.length;i++){
                                    if(data.segments[i].flight.status === 'CNF')
                                     self.model.reservationSegments2.push(data.segments[i]);
                                }
                               var sectorFirst = [];
                               var sectorSecond = [];
                               var flightRph = [];
                                for(var i=0;i< self.selectedAncillaries.length;i++){
                                    if(self.selectedAncillaries[i].ancillaryType === "BAGGAGE"){
                                       
                                       // self.model.reservationSelectedAncillaries[i].flightRPH.reverse();
                                            if(self.duplicateIndex.baggageDuplicateIndex === -1)
                                            self.duplicateIndex.baggageDuplicateIndex = i;
                                         if(self.selectedAncillaries[i].flightRPH.length > 1){
                                              self.selectedAncillaries[i].ancillaryValue.reverse();
                                                 self.selectedAncillaries[i].ondId.reverse();
                                         }
                                         if(self.selectedAncillaries[i].ondId[1] !== undefined && self.selectedAncillaries[i].ondId[0] > self.selectedAncillaries[i].ondId[1]){
                                              self.selectedAncillaries[i].ancillaryValue.reverse();
                                            self.selectedAncillaries[i].ondId.reverse();
                                         }
                                         self.selectedAncillaries[i].flightRPH=[];
                                          for(var x=0; x < self.selectedAncillaries[i].ondId.length;x++){
                                                var ondIndex= self.ondPreferrences.ondId.lastIndexOf(self.selectedAncillaries[i].ondId[x]);
                                                self.selectedAncillaries[i].flightRPH.push(self.ondPreferrences.flighSegmentRPH[ondIndex]);
                                                flightRph.push(self.ondPreferrences.flighSegmentRPH[ondIndex]);

                                            }
//                                        for(var x=0;x < data.ondPreferences.length;x++){
//                                            if(self.model.reservationSelectedAncillaries[i].ondId[x] === self.ondPreferrences.ondId[x]){
//                                                
//                                            }
//                                        }

                                    self.duplicateIndexLength.baggageDuplicateIndex++;
                                    sectorFirst.push(self.selectedAncillaries[i].ancillaryValue[0]);
                                    sectorSecond.push(self.selectedAncillaries[i].ancillaryValue[1]);
                                    }else if(self.selectedAncillaries[i].ancillaryType === "SEAT"){
                                        if(self.selectedAncillaries[i].flightRPH.length > 1 ){
                                            self.selectedAncillaries[i].ancillaryValue.reverse();
                                           self.selectedAncillaries[i].flightRPH.reverse();
                                    }
                                        if(self.duplicateIndex.seatDuplicateIndex === -1)
                                            self.duplicateIndex.seatDuplicateIndex = i;
//                                        else{
//                                           if(self.selectedAncillaries2.includes(self.model.reservationSelectedAncillaries[i])){
//                                            self.selectedAncillaries.splice(self.selectedAncillaries.indexOf(self.model.reservationSelectedAncillaries[i]), 1 );
//                                            i--;
//                                            }else{
//                                                     console.log("test");
//                                            }
//                                        }
                                        self.duplicateIndexLength.seatDuplicateIndex++;
                                    }else if(self.selectedAncillaries[i].ancillaryType === "MEAL"){
                                        if(self.duplicateIndex.mealDuplicateIndex === -1)
                                            self.duplicateIndex.mealDuplicateIndex = i;
                                        if(self.selectedAncillaries[i].flightRPH.length > 1){
                                            self.selectedAncillaries[i].ancillaryValue.reverse();
                                            self.selectedAncillaries[i].flightRPH.reverse();
                                        }
//                                        else{
//                                            if(self.selectedAncillaries2.includes(self.model.reservationSelectedAncillaries[i])){
//                                            self.selectedAncillaries.splice(self.selectedAncillaries.indexOf(self.model.reservationSelectedAncillaries[i]), 1 );
//                                            i--;
//                                            }else{
//                                                     console.log("test");
//                                            }
//                                        }
                                       self.duplicateIndexLength.mealDuplicateIndex++;
                                    }else if(self.selectedAncillaries[i].ancillaryType === "SSR_IN_FLIGHT"){
                                        if(self.selectedAncillaries[i].flightRPH.length > 1){
                                             self.selectedAncillaries[i].flightRPH.reverse();
                                            if(self.selectedAncillaries[i].ancillaryValue.length > 1){
                                                var temp =  self.selectedAncillaries[i].ancillaryValue[0];
                                                self.selectedAncillaries[i].ancillaryValue[0]  = self.selectedAncillaries[i].ancillaryValue[1];
                                                self.selectedAncillaries[i].ancillaryValue[1] = temp;
                                        }
                                        
                                    }
                                        if(self.duplicateIndex.inflightDuplicateIndex === -1)
                                            self.duplicateIndex.inflightDuplicateIndex = i;
//                                        else{
//                                            if(self.selectedAncillaries2.includes(self.model.reservationSelectedAncillaries[i])){
//                                            self.selectedAncillaries.splice(self.selectedAncillaries.indexOf(self.model.reservationSelectedAncillaries[i]), 1 );
//                                            i--;
//                                            }else{
//                                                     console.log("test");
//                                            }
//                                        }
                                        self.duplicateIndexLength.inflightDuplicateIndex++;
                                    }else if(self.selectedAncillaries[i].ancillaryType === "SSR_AIRPORT"){
                                        if(self.selectedAncillaries[i].flightRPH.length > 1){
                                         self.selectedAncillaries[i].ancillaryValue.reverse();
                                        self.selectedAncillaries[i].flightRPH.reverse();
                                    }
                                        if(self.duplicateIndex.airportDuplicateIndex === -1)
                                            self.duplicateIndex.airportDuplicateIndex = i;
//                                        else{
//                                           if(self.selectedAncillaries2.includes(self.model.reservationSelectedAncillaries[i])){
//                                            self.selectedAncillaries.splice(self.selectedAncillaries.indexOf(self.model.reservationSelectedAncillaries[i]), 1 );
//                                            i--;
//                                            }else{
//                                                     console.log("test");
//                                            }
//                                        }
                                    self.duplicateIndexLength.airportDuplicateIndex++;
                                    }else if(self.selectedAncillaries[i].ancillaryType === "INSURANCE"){
                                        if(self.duplicateIndex.insuranceDuplicateIndex === -1)
                                            self.duplicateIndex.insuranceDuplicateIndex = i;
//                                        else{
//                                           if(self.selectedAncillaries2.includes(self.model.reservationSelectedAncillaries[i])){
//                                            self.selectedAncillaries.splice(self.selectedAncillaries.indexOf(self.model.reservationSelectedAncillaries[i]), 1 );
//                                            i--;
//                                            }else{
//                                                     console.log("test");
//                                            }
//                                        }
                                    self.duplicateIndexLength.insuranceDuplicateIndex++;
                                    }else if(self.selectedAncillaries[i].ancillaryType === "FLEXIBILITY"){
                                        if(self.duplicateIndex.flexiDuplicateIndex === -1)
                                            self.duplicateIndex.flexiDuplicateIndex = i;
//                                        else{
//                                           if(self.selectedAncillaries2.includes(self.model.reservationSelectedAncillaries[i])){
//                                            self.selectedAncillaries.splice(self.selectedAncillaries.indexOf(self.model.reservationSelectedAncillaries[i]), 1 );
//                                            i--;
//                                            }else{
//                                                     console.log("test");
//                                            }
//                                        }
                                    self.duplicateIndexLength.flexiDuplicateIndex++;
                                    }
                                        
                                }  
                                var sectorOne = setBaggageAncillaryValues(sectorFirst);
                                self.baggageForSectorFirst = [];
                                self.baggageForSectorFirst[0] = flightRph[0];
                                for(var x=0; x < sectorOne.length; x++){
                                    if(self.baggageForSectorFirst[1] === undefined)
                                        self.baggageForSectorFirst[1] = sectorOne[x];
                                    else
                                        self.baggageForSectorFirst[1] +=","+sectorOne[x];
                                }
                                var sectorTwo = setBaggageAncillaryValues(sectorSecond);
                                self.baggageForSectorSecond = [];
                                self.baggageForSectorSecond[0] = flightRph[1];
                                for(var x=0; x < sectorTwo.length; x++){
                                    if(self.baggageForSectorSecond[1] === undefined)
                                        self.baggageForSectorSecond[1] = sectorTwo[x];
                                    else
                                        self.baggageForSectorSecond[1] +=","+sectorTwo[x];
                                }
                            }
                        });
                            }
                        },
                        function onError(data) {
                            IBEDataConsole.log('LOAD RESERVATION ERROR');

                            var errorMessage = 'Could not load the reservation';

                            if (!!data.errors && data.errors.length > 0) {
                                errorMessage = data.errors[0]
                            }

                            if (self.model.isUserLoggedIn) {
                                alertService.setAlert(errorMessage, 'warning');
                                goToDashboard();
                            }else{
                                alertService.setAlert(errorMessage, 'warning');
                                goToLookup();
                            }
                        });
                };
                
                var setBaggageAncillaryValues = function (tmpancillaryValues) {
                        var ancillary = [];
                        var baggage_elements = tmpancillaryValues;
                        baggage_elements.sort();
                        var current = null;
                        var cnt = 0;
                        var temp = null;
                        for (var i = 0; i < baggage_elements.length; i++) {
                            if (baggage_elements[i] != current) {
                                if (cnt > 0) {
                                    temp = cnt + " X" + current.split('X')[1];
                                    ancillary.push(temp);
                                }
                                current = baggage_elements[i];
                                cnt = 1;
                            } else {
                                cnt++;
                            }
                        }
                        if (cnt > 0) {
                            temp = cnt + " X" + current.split('X')[1];
                            ancillary.push(temp);
                        }
                        return ancillary;
                    };


                /**
                 * Populate Cancelled Reservation Data
                 * And set Status flags to disable reservation modification options
                 */
                this.loadCancelledReservation = function(){
                    var data = modifyReservationService.getReservation();
                    self.model.bookingDetails = data.bookingDetails;
                    self.model.passengerDetails = data.passengerDetails;
                    self.model.reservationSelectedAncillaries = data.ancillaries;
                    self.model.reservationSegments = data.segments;
                    self.model.contactInfo = data.contactInfo;

                    // Set the balance summary according to cancellation request
                    if(data.balanceSummary){
                        self.charges = data.balanceSummary.chargeBreakdown;
                    }

                    // Hide ancy list.
                    self.showAncillaryList = false;

                    setStatusFlags();
                }


                /**
                 * Cancel Reservation Button Click Listener
                 * DO a service call for get Balance Summery for cancel Reservation
                 */
                this.onCancelReservationClick = function () {
                    modifyReservationService.getBalanceSummaryForCancelReservation(
                        function (data) {
                            data.pnr = $stateParams.pnr;
                            modifyReservationService.setCancelReservationBalanceSummery(data);
                            quoteService.changeShowQuote();
                            quoteService.setShowSummaryDrawer(true);
                            self.mbottom = '70px';
                            IBEDataConsole.log("BALANCE SUMMERY - CANCEL RESERVATION SUCCESSFUL");
                        },
                        function (error) {
                            IBEDataConsole.log("CANCEL RESERVATION BALANCE SUMMERY REQUEST ERROR");
                        }
                    );
                };


                this.onMakePaymentClick = function () {
                    applicationService.setApplicationMode(applicationService.APP_MODES.BALANCE_PAYMENT);
                    $state.go('payments', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
                };

                /**
                 * Edit Passenger (Passenger Name Change) Click Listener
                 * Navigate User to Pax Information Page
                 */
                this.onEditPassengerClick = function () {
                    //Navigate to Pax Page
                    applicationService.setApplicationMode(applicationService.APP_MODES.NAME_CHANGE);
                    $state.go('passenger', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
                };

                /**
                 * Edit Passenger (Contact Change) Click Listener
                 * Navigate User to Pax Information Page
                 */
                this.onEditContactClick = function () {
                    //Navigate to Contact Pax Page
                    applicationService.setApplicationMode(applicationService.APP_MODES.CONTACT_CHANGE);
                    $state.go('passenger', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
                };


                this.loadModifyOptions = function (ancillaryType) {
                    loadingService.showPageLoading();
                    var state = 'extras';
                    switch (ancillaryType) {
                        case 'INSURANCE' :
                        {
                            state = 'insurance';
                            break;
                        }
                        case 'SEAT' :
                        {
                            state = 'seats';
                            break;
                        }
                        case 'MEAL' :
                        {
                            state = 'meals';
                            break;
                        }
                        case 'BAGGAGE' :
                        {
                            state = 'baggage';
                            break;
                        }
                        case 'SSR_AIRPORT' :
                        {
                            state = 'airport_services';
                            break;
                        }
                        case 'AIRPORTTRANSFER' :
                        {
                            state = 'airport_transfer';
                            break;
                        }
                    }

                    modifyReservationService.getAncillaryAvailability(
                        function (setAnciAvailabilityList) {
                            modifyReservationService.loadAvailableAncillaryData(
                                setAnciAvailabilityList.availableAncillariesResponse,
                                function (AnciFullDataSet) {
                                    if (ancillaryRemoteService.metaData) {
                                        commonLocalStorageService.setOndDetails(ancillaryRemoteService.metaData.ondPreferences);
                                    }

                                    IBEDataConsole.log(AnciFullDataSet);
                                    ancillaryRemoteService.availableAncillary = AnciFullDataSet;

                                    $state.go(state, {
                                        lang: languageService.getSelectedLanguage(),
                                        currency: currencyService.getSelectedCurrency(),
                                        mode: applicationService.getApplicationMode()
                                    });

                                }, function (error) {
                                    IBEDataConsole.log('LOAD AVAILABLE ANCILLARY ERROR');
                                    loadingService.hidePageLoading();
                                })

                        }, function (error) {
                            IBEDataConsole.log('INITIALIZE ANCILLARY ERROR');
                            loadingService.hidePageLoading();
                        },"modifyEdit");


                };


                /**
                 * Modify/Cancel Button Click Handler
                 */
                this.onModifyFlightClick = function () {
                    applicationService.setApplicationMode(applicationService.APP_MODES.MODIFY_SEGMENT);
                    ancillaryRemoteService.clearExtras();
                    modifyReservationService.setSelectedFareGroupSegmentsToModify(null);
                    $state.go('modifyFlightHome.flightHome', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
                };


                /**
                 * Show More Passenger Popup
                 */
                this.showPopup = function () {

                    var popWidth = 275;
                    //var popHeight = 260;
                    self.blockSection = true;
                    self.popupPos = {
                        "top": angular.element('#paxSelect').position().top,
                        "left": angular.element('#paxSelect').position().left,
                        "width": popWidth,
                        "height": "auto"
                    };
                    self.applyMask = {"display": "block", "z-index": 10};
                };

                /**
                 * Close Mode Passenger popup
                 */
                this.closePopup = function () {
                    self.applyMask = {"display": "hide", "z-index": -10};
                    self.blockSection = false;
                    self.isPopupVisible = true;
                    self.isContinuClicked = false;
                };

                this.getTrustedUrl= function(url) {
                    return $sce.trustAsResourceUrl(url);
                };

                this.viewItinerary = function() {

                     var params = getRequestParameters();

                     confirmService.print(params, function (response) {
                     // Stop execution if success is false.
                     if (!response.success) {
                     IBEDataConsole.log('Error: something went wrong, ', 'confirmService.print function');
                     return;
                     }

                     //toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top="+(screen.height-400)+", left="+(screen.width-840)
                     var printWindow = $window.open("");

                     //If window was not created due to popup blocker, show a warning
                     if(!printWindow){
                        popupService.alert(dialogTranslate.popupblocker);
                     }else{
                     printWindow.document.body.innerHTML = response.itinerary;
                     printWindow.print();
                     }

                     }, function (response) {
                     // on error.
                     IBEDataConsole.log(response);
                     });
                };
                
                this.viewTaxInvoice = function(selectedInvoiceIdsStr) {

                    var params = getRequestParameters();
                    params["selectedInvoiceIdsStr"] = selectedInvoiceIdsStr;

                    confirmService.printTaxInvoice(params, function (response) {
                    // Stop execution if success is false.
                    if (!response.success) {
                    IBEDataConsole.log('Error: something went wrong, ', 'confirmService.printTaxInvoice function');
                    return;
                    }
                    
                    if (selectedInvoiceIdsStr == ''){
                    	popupService.alert("Please select At least on check box");
                    } else {
                    	//toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top="+(screen.height-400)+", left="+(screen.width-840)
                        var printWindow = $window.open("");

                        //If window was not created due to popup blocker, show a warning
                        if(!printWindow){
                        	popupService.alert("Please disable pop-up blocker to print TaxInvoice");
                        }else{
                        	printWindow.document.body.innerHTML = response.taxInvoice;
                        	printWindow.print();
                        }
                    }
                  
                    }, function (response) {
                        // on error.
                        IBEDataConsole.log(response);
                    });

               };
               
               // Controller for getTaxInvoiceList function.
               this.getTaxInvoiceList = function() {
                   var params = getRequestParameters();

                   confirmService.getTaxInvoiceList(params, function (response) {
                       // Stop execution if success is false.
                       if (!response.success) {
                           IBEDataConsole.log('Error: something went wrong, ', 'confirmService.getTaxInvoiceList function');
                           return;
                       }
                       
                       var popupMsg = "Following are the list of Invoices, Please select invoices to Print.";
                       var taxInvoicesListArray = response.taxInvoicesList;
                       var selectedInvoiceIdsStr = '';
                       popupService.showTaxInvoiceSelectionPopup('tax.invoice.select.tpl.html',{taxInvoicesList:taxInvoicesListArray}, self.viewTaxInvoice, function(){});
                           
                   }, function (response) {
                       // on error.
                       IBEDataConsole.log(response);
                   });
               };
               
                this.emailItinerary = function(){
                    var params = getRequestParameters();

                    confirmService.sendEmail(params, function (response) {
                        // Stop execution if success is false.
                        if (!response.success) {
                            IBEDataConsole.log('Error: something went wrong, ', 'confirmService.sendEmail function');
                            return;
                        }
                        else{
                            alertService.setAlert("Email sent to your mail address.",'success')
                        }

                        IBEDataConsole.log(response.success);

                    }, function (response) {
                        // on error.
                        IBEDataConsole.log(response);
                    });
                };

                function getRequestParameters(){
                    var applicationMode = applicationService.getApplicationMode();
                    applicationMode = (typeof applicationMode === "string") ? applicationMode.toUpperCase() : applicationMode;

                    return {
                        pnr: self.model.bookingDetails.pnr,
                        groupPnr: self.model.bookingDetails.groupPnr,
                        itineraryLanguage: self.model.bookingDetails.prefferedLanguage || languageService.getSelectedLanguage() ,
                        operationType: applicationMode,
                        viewMode: "loadRes" // Hard coded value.
                    };
                }
                /**
                 * View Initialization Self invoking Method
                 */
                (function init() {
                    if( $window.localStorage && $rootScope.deviceType.isMobileDevice )
                      {
                        if( !$window.localStorage.getItem( 'firstLoad' ) )
                        {
                          $window.localStorage[ 'firstLoad' ] = true;
                          $window.location.reload();
                        }  
                        else
                         $window.localStorage.removeItem( 'firstLoad' );
                      }
                    self.paymentOnholdError = false;
                    if(!!commonLocalStorageService.getData('PAYMENTONHOLD')){
                        var errorMsg = commonLocalStorageService.getData('PAYMENTONHOLD').error
                        popupService.showPopup('onhold.payment.error.tpl.html',{error:errorMsg},function selectAccept(){
                                self.onMakePaymentClick();
                        },function(){});
                    }

                    applicationService.setTransactionID(null);
                    quoteService.setShowQuoteStatus(false);
                    quoteService.setShowSummaryDrawer(false);

                    // Get user logged in status.
                    self.isUserLoggedIn = applicationService.getCustomerLoggedState();

                    self.loadReservation();

                    modifyReservationService.registerEventListener(modifyReservationService.events.RELOAD_RESERVATION, function () {
                        if (applicationService.getApplicationMode() == applicationService.APP_MODES.CANCEL_RESERVATION) {
                            self.loadCancelledReservation();
                        } else {
                            self.loadReservation();
                        }
                    });
                     $translate.onReady().then(function() {
                    ancillaryService.setAncillaryName('SEAT',$translate.instant('lbl_extra_seatsHeader'));
                     ancillaryService.setAncillaryDescription('SEAT',$translate.instant('lbl_extra_seat_default'));
                    ancillaryService.setAncillaryName('MEAL',$translate.instant('lbl_extra_mealsHeader'));
                     ancillaryService.setAncillaryDescription('MEAL',$translate.instant('lbl_extra_meals_selected'));
                    ancillaryService.setAncillaryName('FLEXIBILITY',$translate.instant('lbl_extra_flexiSelected'));
                     ancillaryService.setAncillaryDescription('FLEXIBILITY',$translate.instant('lbl_extra_seatsHeader'));
                    ancillaryService.setAncillaryName('SSR_AIRPORT',$translate.instant('lbl_extra_ssrHeader'));
                     ancillaryService.setAncillaryDescription('SSR_AIRPORT',$translate.instant('lbl_extra_ssr_airport_selected'));
                    ancillaryService.setAncillaryName('AIRPORT_TRANSFER',$translate.instant('lbl_extra_airportTransferHeader'));
                     ancillaryService.setAncillaryDescription('AIRPORT_TRANSFER',$translate.instant('lbl_extra_inFlightSsrSelected'));
                    ancillaryService.setAncillaryName('BAGGAGE',$translate.instant('lbl_extra_baggageHeader'));
                     ancillaryService.setAncillaryDescription('BAGGAGE',$translate.instant(' lbl_extra_baggage_selected '));
                    ancillaryService.setAncillaryName('INSURANCE',$translate.instant('lbl_extra_insuranceHeader'));
                     ancillaryService.setAncillaryDescription('INSURANCE',$translate.instant('lbl_extra_insurance_not_selected'));
                    ancillaryService.setAncillaryName('SSR_IN_FLIGHT',$translate.instant('lbl_extra_inFlightHeader'));
                     ancillaryService.setAncillaryDescription('SSR_IN_FLIGHT',$translate.instant('lbl_extra_ssrInFlight'));

                    if($rootScope.deviceType.isMobileDevice){
                    	buttonLabels.SELECT_NOW = $translate.instant('lbl_extra_select');
                        buttonLabels.EDIT_SELECTION = $translate.instant('lbl_extra_edit');
                    }else{
                    	buttonLabels.SELECT_NOW = $translate.instant('lbl_extra_selectNow');
                        buttonLabels.EDIT_SELECTION = $translate.instant('lbl_extra_editSelection');
                    }
                });


                })();
            }]);


    /**
     * Reservation Lookup Controller
     */
    module.controller('ReservationLookupController', [
        'loadingService',
        'alertService',
        'languageService',
        'currencyService',
        'datePickerService',
        '$stateParams',
        '$state',
        '$filter',
        'commonLocalStorageService',
        'ancillaryService',
        '$translate',
        'constantFactory',
        '$rootScope',
        function (loadingService, alertService, languageService, currencyService, datePickerService, $stateParams, $state, $filter,commonLocalStorageService,ancillaryService,$translate,constantFactory,$rootScope) {
            var self = this;
            this.model = {
                dateFormat: 'yyyy-MM-dd',
                searchCriteria: {
                    pnr: $stateParams.pnr,
                    lastName: $stateParams.lastName,
                    departureDate: $stateParams.departureDate
                }
            };
            var buttonLabels = {
                           'SELECT_NOW': "Select Now",
                           'EDIT_SELECTION': "Edit Selection"
                       };
            (function init() {
                loadingService.hidePageLoading();
            })();

            this.findReservation = function (event) {
                if (self.findReservationFrm.$valid) {
                    $state.go('modifyReservation', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        pnr: self.model.searchCriteria.pnr,
                        lastName: self.model.searchCriteria.lastName,
                        departureDate: $filter('date')(self.model.searchCriteria.departureDate, datePickerService.setDateFormat(5))
                    });
                    $window.localStorage.setItem('PNR_LOGIN',true);
                }else{
                    $window.localStorage.removeItem('PNR_LOGIN');
                }
            };
        }
    ]);


    /**
     * ************************************************************************************
     * Modification Drawer Controller
     * @param quoteService
     * @param modifyReservationService
     * ************************************************************************************
     */
    module.controller('ModificationSummeryDrawerController', [
        '$scope',
        'quoteService',
        'modifyReservationService',
        'applicationService',
        'alertService',
        'IBEDataConsole','$timeout','configsService', '$translate',function modificationSummeryDrawerController($scope,quoteService, modifyReservationService, applicationService, alertService, IBEDataConsole,$timeout,configsService,$translate) {
            var self = this;
            this.showSummaryDrawer = false;
            this.quoteStatus = false;
            this.title = "MY OWN SUMMERY DRAWER";
            self.appConfigs = configsService.clientConfigs.configs;
            self.callCenterAvailable = true;
            if(self.appConfigs.isCallCenterAvailable != undefined){
                self.callCenterAvailable = self.appConfigs.isCallCenterAvailable;
            }
            
            var dialogTranslate = {
                               popupblocker: "Please disable pop-up blocker to print reservation",
                           };


            $scope.$on('event:reservationDataUpdate', function(){
                self.init();
            });


            this.changeQuoteStatus = function () {
                quoteService.changeShowQuote();
            };

            /**
             * Confirm Cancel Reservation Click Listener
             * Perform CancelReservation service call
             */
            this.onConfirmCancelReservationClick = function () {
                modifyReservationService.cancelReservation(
                    function (data) {
                        applicationService.setApplicationMode(applicationService.APP_MODES.CANCEL_RESERVATION);

                        //set Reservation Status to Cancel
                        modifyReservationService.makeCancelReservationChanges();

                        quoteService.changeShowQuote();
                        self.showSummaryDrawer = false;

                        // Set the new charge breakdown after cancellation to the reservation model
                        var modifyModel = modifyReservationService.getReservation();
                        if(data.balanceSummary){
                            modifyModel.balanceSummary = data.balanceSummary;
                            modifyReservationService.setReservation(modifyModel);
                        }

                        modifyReservationService.triggerEvent(modifyReservationService.events.RELOAD_RESERVATION);
                        IBEDataConsole.log("CANCEL RESERVATION REQUEST SUCCESS");
                    },
                    function (error) {
                        alertService.setAlert(error.error, 'danger');
                        quoteService.changeShowQuote();

                        IBEDataConsole.log("CANCEL RESERVATION REQUEST ERROR");
                    }
                );
            };

            /**
             * Controller View Initialization function
             */
            (function init() {
                //var modifyPriceBreakdown = modifyReservationService.modifyPriceBreakdown();
                var modifyPriceBreakdown = {};
                $timeout(
                    function(){
                        modifyPriceBreakdown = modifyReservationService.modifyPriceBreakdown();
                    }
                    ,2000);
                    $translate.onReady().then(function() {
                                         dialogTranslate.popupblocker = $translate.instant('lbl_confirm_popupblocker');
                                     });
                quoteService.registerListeners(function () {
                    self.showSummaryDrawer = quoteService.getShowSummaryDrawer();
                    self.priceBreakdown = modifyReservationService.getCancelReservationBalanceSummery();

                    if(self.priceBreakdown){
                        self.paxSummary = self.priceBreakdown.paxSummary;
                        self.modifyCharges = self.priceBreakdown.modifyCharges;
                    }

                    self.groupWiseSegment = modifyPriceBreakdown.groupWiseSegment;
                    self.quoteStatus = quoteService.getQuoteStatus();
                });
            })();

        }]);


    /**
     * ************************************************************************************
     * Modification Drawer Controller
     * @param quoteService
     * @param modifyReservationService
     * ************************************************************************************
     */
    module.controller('NameChangeController', [
        '$state',
        'languageService',
        'currencyService',
        'applicationService',
        'modifyReservationService',
        'paymentService',
        'quoteService',
        'IBEDataConsole',
        'alertService',
        function ($state, languageService, currencyService, applicationService, modifyReservationService, paymentService, quoteService, IBEDataConsole, alertService) {
            var self = this;

            var model = modifyReservationService.getBalanceSummery();

            if (model.type == modifyReservationService.BALANCE_SUMMERY_TYPE.PAX_NAME_CHANGE) {
                this.charges = model.balanceSummery.modifyCharges;
            } else {
                this.charges = {};
            }

            // Setting up display charges for modify drawer
            var modifyPriceBreakdown = modifyReservationService.modifyPriceBreakdown();

            self.paxSummary = modifyPriceBreakdown.paxSummary;
            self.modifyCharges = modifyPriceBreakdown.modifyCharges;

            self.passengerSummary = modifyPriceBreakdown.passengerSummary;
            self.displayCharges = modifyPriceBreakdown.displayCharges;
            self.groupWiseSegment = modifyPriceBreakdown.groupWiseSegment;
            // END Setting up display charges for modify drawer

            this.onConfirmNameChange = function () {
                paymentService.setModificationPaymentSummery(self.charges);
                if(self.charges.total > 0){
                    $state.go('payments', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
                }else{
                    quoteService.changeShowQuote();
                    quoteService.setShowSummaryDrawer(false);

                    modifyReservationService.confirmNameChange(
                        function(){
                            alertService.setAlert('Passenger name change successful','success');
                            modifyReservationService.reloadReservation();
                        },
                        function(){
                            IBEDataConsole.log("NAME CHANGE CONFIRMATION ERROR");
                        }
                    );
                }
            };

        }]);


})(angular);
