/**
 * Created by indika on 1/28/16.
 */
(function (angular) {
    angular.module('modification.ancillary')
        .factory('modificationAncillaryFactory', [function () {
        //DTO for a single ancillary type (ex- MEAL, SEAT)
        var AncillaryItemDTO = function (ancillaryType, initialTotal) {
            this.ancillary = ancillaryType;
            this.total = initialTotal || 0;
            this.scopes = [];
        };

        //DTO for a scope (ex- OND, SEGMENT)
        var AncillaryScopes = function (scopeType) {
            this.scopeType = scopeType;
            this.total = 0;
            this.preferences = [];
        };

        //DTO for an ancillary preference for a given segment (ex- CMB-SHJ in segment scope)

        var AncillaryPreference = function (preferenceId, segmentDetails,passengers) {
            this.preferanceId = preferenceId;
            this.segments = segmentDetails; //[{"from":"CMB", "to":"CMB"}]
            this.total = 0;
            this.passengers = passengers;
        };

        //DTO for a passenger inside a preference
        var AncillaryPassenger = function (passenger, selected, key) {
            this.passenger = passenger;
            this.passengerRph = key;
            this.selectedItems = selected;
        };

        //Ancillary preferences for insurance
        var AncillaryInsurancePreference = function(){
            this.selectedItems=[];
            this.total = 0;
        }
        return {
            AncillaryItemDTO: function (ancillaryType, initialTotal) {
                    return new AncillaryItemDTO(ancillaryType, initialTotal)
            },
            AncillaryScopes: function (scopeType) {
                    return new AncillaryScopes(scopeType);
            },
            AncillaryPreference: function (preferenceId, segmentDetails,passengers) {
                return new AncillaryPreference(preferenceId, segmentDetails,passengers);
            },
            AncillaryPassenger: function (passenger,selected, key) {
                return new AncillaryPassenger(passenger,selected, key);
            },
            AncillaryInsurancePreference: function () {
                return new AncillaryInsurancePreference();
            }
        };
    }
])
})(angular);