/**
 * Created by sumudu on 1/20/16.
 */

'use strict';

(function (angular) {
    angular.module('modification.reservation')
        .service('modifyReservationService', [
            '$rootScope',
            'modificationRemoteService',
            'modificationModelFactory',
            '$state',
            '$filter',
            'languageService',
            'currencyService',
            'modificationAncillaryFactory',
            'commonLocalStorageService',
            'IBEDataConsole',
            'travelFareService',
            'appConstants',
            'ancillaryRemoteService',
            'extrasService',
            'loadingService',
            'ancillaryService',
            modifyReservationService]);

    function modifyReservationService($rootScope, modificationRemoteService, modificationModelFactory, $state, $filter, languageService, currencyService, modificationAncillaryFactory, commonLocalStorageService, IBEDataConsole, travelFareService, appConstants, ancillaryRemoteService, extrasService, loadingService, ancillaryService) {
        var self = this;
        var eventListeners = {};
        this.ancillaryModel = {};
        var isAncillaryAvailable = true,selectedFareGroupIndex = 0;

        this.BALANCE_SUMMERY_TYPE = {
            CANCEL_FLIGHT: 'cancel_flight',
            MODIFY_FLIGHT: 'modify_flight',
            PAX_NAME_CHANGE: 'pax_name_change',
            MODIFY_SEGMENT_WITH_ANCILLARY: 'modify_segment_with_ancillary',
            MODIFY_ANCILLARY: 'modify_ancillary',
            ON_HOLD:"on_hold"
        };

        this.RESERVATION_STATUS = {
            /*Denotes reservation confirmed*/
            CONFIRMED: "CNF",

            /** Denotes reservation cancellation */
            CANCEL: "CNX",

            /** Denotes reservation on hold */
            ON_HOLD: "OHD",

            /** Denotes standay reservation */
            STAND_BY: "STB",

            /** Denotes a Void reservation */
            VOID: "VOD"
        };

        this.unChangedAnciModel; // Keep a backup of ancillary model at local

        var model = {
            pnr: "",
            lastName: "",
            departureDate: "",
            reservation: null,
            cancelReservationBalanceSummery: null,
            selectedFareGroupSegmentsToModify: null,
            balanceSummery: null,
            flightSearchCriteria: null,
            selectedNewFlight: null,
            selectedNewFareClass: null,
            ancillarySummery: null,
            selectedFareIndex:''
        };

        /**
         * Initialize Model with Local Storage Data if Present
         */
        (function initService() {
            var tModel = commonLocalStorageService.getData('MODIFY.RESERVATION.MODEL');
            if (!!tModel) {
                model = tModel;
            }
        })();


        var persistServiceDataModel = function () {
            commonLocalStorageService.setData('MODIFY.RESERVATION.MODEL', model);
            if(!!model.selectedNewFlight && !!model.reservation.segments && !! model.selectedNewFareClass){
            	if(model.selectedNewFlight.originCode === model.reservation.segments[0].flight.originCode){
            	 	commonLocalStorageService.setData('MODIFY.NEW.OUTBOUNDFARE', model.selectedNewFareClass.description);
           	 }
            	if(model.selectedNewFlight.originCode === model.reservation.segments[0].flight.destinationCode){
           	 	commonLocalStorageService.setData('MODIFY.NEW.INBOUNDFARE', model.selectedNewFareClass.description);
          	 }
           }
        };

        var getNameChangeReq = function (nameChangedPaxList) {

            var req = {
                nameChangePassengerList: [],
                nameChangeRequote: true,
                pnr: model.pnr,
                groupPnr: model.reservation.bookingDetails.groupPnr
            };

            if (!!nameChangedPaxList) {
                angular.forEach(nameChangedPaxList, function (pax) {
                    req.nameChangePassengerList.push(
                        {
                            title: pax.salutation,
                            firstName: pax.firstName,
                            lastName: pax.lastName,
                            paxType: pax.category,
                            paxSeqNo: pax.paxSequence,
                            ffid: ""
                        }
                    );
                });
            }

            return req;

        };


        var getFfidChangeReq = function (ffidChangePaxList) {
            var req = {
                ffidChangePaxList: [],
                pnr: model.pnr,
                groupPnr: model.reservation.bookingDetails.groupPnr
            };

            if (!!ffidChangePaxList) {
                angular.forEach(ffidChangePaxList, function (pax) {
                    req.ffidChangePaxList.push(
                        {
                            paxSeqNo: pax.paxSequence,
                            ffid: pax.airewardsEmail
                        }
                    );
                });
            }

            return req;
        };


        var getCancelSegmentRequotReq = function () {
            var req = {
                cancelSegment: true,
                modificationInfo: {
                    pnr: model.pnr,
                    cnxFlights: []
                },
                groupPnr: model.reservation.bookingDetails.groupPnr
            };

            if (!!model.selectedFareGroupSegmentsToModify && !!model.selectedFareGroupSegmentsToModify.segments) {
                angular.forEach(model.selectedFareGroupSegmentsToModify.segments, function (segment) {
                    req.modificationInfo.cnxFlights.push(segment.flight.reservationSegmentRPH);
                });
            }
            return req;
        };


        var getSegmentModificationRequotRq = function (type) {
            var searchCriteria = self.getFlightSearchCriteria();
            var cabinClass = (searchCriteria != undefined) ? searchCriteria.cabinClass : "Economy";
            var cabinType = self.getCabinClassType(cabinClass);

            var req = {
                modifySegment: true,
                flightSegments: [],
                modificationInfo: {
                    pnr: model.pnr,
                    cnxFlights: [],
                    addedFlights: [],
                    modifiedFlights: []
                },
                groupPnr: model.reservation.bookingDetails.groupPnr
            };

            var getFromSegmentRPH = function () {
                var fromSegmentRPH = [];
                var toBeModified = model.selectedFareGroupSegmentsToModify;
                if (!!toBeModified && !!toBeModified.segments) {
                    angular.forEach(toBeModified.segments, function (segment) {
                        fromSegmentRPH.push(segment.flight.reservationSegmentRPH);
                    });
                }
                return fromSegmentRPH;
            };

            var getToSegmentRPH = function () {
                var to = [];
                angular.forEach(req.flightSegments, function (flight) {
                    to.push(flight.flightSegmentRPH);
                });
                return to;
            };

            var getFlight = function (flightData) {
                var tmp = {
                    flightDesignator: flightData.flightNumber,
                    segmentCode: flightData.originCode + '/' + flightData.destinationCode,
                    departureDateTime: moment(flightData.departureDate, 'DD/MM/YYYY').format('YYYY-MM-DD') + "T" + flightData.departureTime + ":00",
                    flightSegmentRPH: flightData.flightRPH
                };
                return tmp;
            };

            //set flight segment about to change
            var newFlight = model.selectedNewFlight || {};
            var hasStops = false;
            if(typeof newFlight.stops !== "undefined"){
               if(newFlight.stops.length) {
                   hasStops = true;
               }else{
                   hasStops = false;
               }
            }
            //set Stopover flights
            if (hasStops) {

                req.flightSegments.push(getFlight({
                    flightNumber : newFlight.flightNumber,
                    originCode : newFlight.originCode,
                    destinationCode : newFlight.stops[0].originCode,
                    flightRPH : newFlight.flightRPH,
                    departureDate : newFlight.departureDate,
                    departureTime : newFlight.departureTime
                }));

                angular.forEach(newFlight.stops, function (flight) {
                    req.flightSegments.push(getFlight(flight));
                });

            }else{
                req.flightSegments.push(getFlight(newFlight));
            }



            if(!newFlight.selectedFare || type === 'default'){
                newFlight.selectedFare ={};
                newFlight.selectedFare.fareClassId=null;
                newFlight.selectedFare.fareClassType='DEFAULT';
            }

            req.modificationInfo.modifiedFlights.push({
                fromResSegRPH: getFromSegmentRPH(),
                toFlightSegRPH: getToSegmentRPH(),
                preferences: {
                    seatType: "NORMAL",
                    cabinClass: cabinType,
                    logicalCabinClass: cabinType,
                    bookingCode: null,
                    additionalPreferences: {
                        fareClassType: newFlight.selectedFare.fareClassType ||'',
                        fareClassId: newFlight.selectedFare.fareClassId||''
                    }
                }
            });

            return req;
        };


        var getConfirmCancelSegmentReq = function () {
            var req = getCancelSegmentRequotReq();
            delete req.cancelSegment;
            return req;
        };

        var getModificationFlightSearchRq = function () {
            var searchCriteria = self.getFlightSearchCriteria();
            var cabinClass = (searchCriteria != undefined) ? searchCriteria.cabinClass : "Economy";
            var cabinType = self.getCabinClassType(cabinClass);
            var selFlightSegSeq = self.getSelectedFareGroupSegmentsToModify().segments[0].flight.segmentSequence;
            var req = {
                journeyInfo: [
                    {
                        origin : model.flightSearchCriteria.origin.code,
                        originCity : model.flightSearchCriteria.origin.city == undefined ? false :model.flightSearchCriteria.origin.city,
                        destination : model.flightSearchCriteria.destination.code,
                        destinationCity : model.flightSearchCriteria.destination.city == undefined ? false :model.flightSearchCriteria.destination.city,
                        departureDateTime: $filter('date')(model.flightSearchCriteria.departureDate, 'yyyy-MM-dd') + "T00:00:00",
                        departureVariance: 0
                    }
                ],
                travellerQuantity: {
                    adultCount: model.reservation.passengerDetails.adultCount
                },
                preferences: {
                    cabinClass: cabinType,
                    logicalCabinClass: cabinType,
                    currency: currencyService.getSelectedCurrency()
                },
                groupPnr: model.reservation.bookingDetails.groupPnr,
                pnr: model.pnr,
                selectedFlightSegmentSequence : selFlightSegSeq
            };

            if (model.reservation.passengerDetails.childCount > 0) {
                req.travellerQuantity.childCount = model.reservation.passengerDetails.childCount;
            }
            if (model.reservation.passengerDetails.infantCount > 0) {
                req.travellerQuantity.infantCount = model.reservation.passengerDetails.infantCount;
            }

            return req;
        };


        var getDefaultAncillarySelectionsRQ = function () {
            var req = {};
            return req;
        };


        var getInitializeAncillaryRq = function () {
            var params = {
                reservationData: {
                    "mode": "MODIFY",
                    "pnr": model.pnr
                }
            }
            params.ancillaries = [
                {
                    "type": "SEAT"
                },
                {
                    "type": "BAGGAGE"
                },
                {
                    "type": "MEAL"
                },
                {
                    "type": "SSR_AIRPORT"
                },
                {
                    "type": "INSURANCE"
                },
                {
                    "type": "SSR_IN_FLIGHT"
                },
                {
                    "type": "FLEXIBILITY"
                },
                {
                    "type": "AIRPORT_TRANSFER"
                }
            ];
            return params;
        };


        var getLoadAvailableAncillaryRq = function (availabilityAncillaryResponse) {
            var param = extrasService.getAvailableAncyTypes(availabilityAncillaryResponse);
            return param;
        };


        var getAncillaryAvailabilityRQ = function () {
            var req = {
                reservationData: {
                    mode: 'MODIFY',
                    pnr: model.pnr
                },
                ancillaries: [
                    {
                        "type": "SEAT"
                    },
                    {
                        "type": "BAGGAGE"
                    },
                    {
                        "type": "MEAL"
                    },
                    {
                        "type": "SSR_AIRPORT"
                    },
                    {
                        "type": "SSR_IN_FLIGHT"
                    },
                    {
                        "type": "INSURANCE"
                    },
                    {
                        "type": "AIRPORT_TRANSFER"
                    }
                ]
            };
            return req;
        };


        /**
         * Service EVENT TYPES
         * @type {{RELOAD_RESERVATION: string}}
         */
        this.events = {
            RELOAD_RESERVATION: 'reload_reservation'
        }

        this.registerEventListener = function (type, listener) {
            if (!!Object.keys(eventListeners)[type]) {
                eventListeners[type].push(listener);
            } else {
                eventListeners[type] = [listener];
            }
        };

        this.triggerEvent = function (type) {
            if (!!eventListeners[type]) {
                angular.forEach(eventListeners[type], function (listnere) {
                    listnere();
                });
            }
        };

        /**
         * Getters AND Setters
         *****************************************
         */

        this.getSelectedNewFlight = function () {
            return model.selectedNewFlight;
        };

        this.setSelectedNewFlight = function (flight) {
            model.selectedNewFlight = flight;
            persistServiceDataModel();
        };

        // this.getAncillarySummery = function () {
        //     return model.ancillarySummery;
        // };

        this.setAncillarySummery = function (summery) {
            model.ancillarySummery = summery;
            persistServiceDataModel();
        };

        this.getSelectedNewFareClass = function () {
            return model.selectedNewFareClass;
        };

        this.setSelectedNewFareClass = function (fareClass) {
            model.selectedNewFareClass = fareClass;
            persistServiceDataModel();
        };

        this.setReservationSearchCriteria = function (pnr, lastName, departureDate) {
            model.pnr = pnr;
            model.lastName = lastName;
            model.departureDate = departureDate;
            persistServiceDataModel();
        };

        this.getReservationSearchCriteria = function () {
            return {
                pnr: model.pnr,
                lastName: model.lastName,
                departureDate: model.departureDate
            };
        };

        this.setFlightSearchCriteria = function (originParam, destinationParam, departureDateParam, cabinClassParam) {
            var cabinClassType = this.getCabinClassType(cabinClassParam);
            model.flightSearchCriteria = {
                origin: originParam,
                destination: destinationParam,
                departureDate: departureDateParam,
                cabinClass: cabinClassParam,
                cabinClassType : cabinClassType
            };
            persistServiceDataModel();
        };

        this.getFlightSearchCriteria = function () {
            return model.flightSearchCriteria;
        };
        this.getCabinClassType = function(cabinClass){
            var  cabinClassType = "Y";
            switch (cabinClass){
                case "Economy":
                    cabinClassType = "Y"
                    break;
                case "Business":
                    cabinClassType = "C"
                    break;
            }
            return cabinClassType;
        }

        this.setSelectedFareGroupSegmentsToModify = function (fareGroupSegments,selectedFareIndex) {
            model.selectedFareGroupSegmentsToModify = fareGroupSegments;
            model.selectedFareIndex = selectedFareIndex;
            persistServiceDataModel();
        };

        this.getSelectedFareGroupSegmentsToModify = function () {
            return model.selectedFareGroupSegmentsToModify;
        };

        /**
         * Ensure that the index returned is only considering the confirmed flights
         * */
        this.getSelectedFareIndex = function(){
            angular.forEach(model.reservation.fareGroupWiseSegments, function(segment, index){
                segment.selected = index === model.selectedFareIndex;
            });

            var confirmedFlights = _.filter(model.reservation.fareGroupWiseSegments, function(fareGroup){
                return (fareGroup.status.indexOf('CNF') >= 0) && !isGroupBusSegment(fareGroup);
            });

            return _.findIndex(confirmedFlights, function(flight){
                return flight.selected;
            });
        };

        function isGroupBusSegment(fareGroup){
            var isBusSegment = false;
            if(fareGroup && fareGroup.segments[0] && fareGroup.segments[0].flight){
                if(fareGroup.segments[0].flight.travelMode == 'BUS'){
                    isBusSegment = true;
                }
            }
            return isBusSegment;
        }

        this.getBalanceSummery = function () {
            return model.balanceSummery;
        };

        this.getFareGroupWiseSegmentFromReservation = function(){
            return model.reservation.fareGroupWiseSegments;
        };

        this.setBalanceSummery = function (balanceSummeryData, balanceSummeryType) {
            model.balanceSummery = {
                type: balanceSummeryType,
                balanceSummery: balanceSummeryData
            };
            persistServiceDataModel();
        };

        this.getReservation = function () {
            return model.reservation;
        };

        this.setReservation = function (reservationData) {
            model.reservation = reservationData;
            persistServiceDataModel();
        };


        this.getLoadReservationRQ = function (pnr, lastName, departureDate) {
            model.pnr = pnr;
            model.lastName = lastName;
            model.departureDate = departureDate;

            var loadReservationRQ = modificationModelFactory.LoadReservationRQ();
            loadReservationRQ.pnr = pnr;
            loadReservationRQ.lastName = lastName;
            loadReservationRQ.departureDateTime = departureDate + "T00:00:00";
            return loadReservationRQ;
        };

        this.getCancelReservationReq = function () {
            var cancelReservationRequest = modificationModelFactory.CancelReservationReq();
            cancelReservationRequest.pnr = model.pnr;
            cancelReservationRequest.version = model.reservation.bookingDetails.version;
            cancelReservationRequest.groupPnr = model.reservation.bookingDetails.groupPnr;
            return cancelReservationRequest;
        };


        /**
         * get last requested cancel reservation balance summery
         * @returns {{pnr: string, airFare: string, tax: string, discount: string, creditableAmount: string, cancelChrage: string, perPaxCancelCharge: string, availableCredit: string, finalCredit: string}}
         */
        this.getCancelReservationBalanceSummery = function () {
            return model.cancelReservationBalanceSummery;
        };

        /**
         * Set cancel Reservation balance summery
         * @param balanceSummery
         */
        this.setCancelReservationBalanceSummery = function (balanceSummery) {
            model.cancelReservationBalanceSummery = balanceSummery;
            persistServiceDataModel();
        };

        /*
         set sellected ancillary
         */
        this.setAncillaryModel = function (ancillaries, segmentdata, passenger, preferances) {
            var flsegment = [];
            var pasObj = [];
            var scopeType = "SEGMENT";
            var ancillaryPreferance
            ancillaryPreferance = []
            var tempAnci = {};
            var ancillaryModel = {};
            var ssrAirportScope = {}
            var flightSegments = []
            var ondvisePassenger = {};
            var isConnectionFlights = false;

            var getOndFromRph = function (rph) {
                var ond;
                for (var i = 0; i < preferances.length; i++) {
                    var pre = preferances[i];
                    if (pre.flightSegmentRPH.indexOf(rph) >= 0) {
                        ond = pre.ondId;
                        break;
                    }
                }
                return ond;
            };

            /**
             * Crate Segment Model from Provided segments
             */
            angular.forEach(segmentdata, function (segmentval, segkey) {
                var seg = {}
                if(segmentval.flight.stops.length){
                	seg.from = segmentval.flight.originCode;
                    seg.to = segmentval.flight.stops[0].originCode;
                    seg.flightSegmentRPH = segmentval.flight.flightSegmentRPH;
                    flsegment.push(seg);
                    angular.forEach(segmentval.flight.stops,function(st){
                    	isConnectionFlights = true;
                    	var connection = {}
                        connection.from = st.originCode;
                        connection.to = st.destinationCode;
                        connection.flightSegmentRPH = st.flightRPH;
                        flsegment.push(connection);
                    })

                }else{
                    seg.from = segmentval.flight.originCode;
                    seg.to = segmentval.flight.destinationCode;
                    seg.flightSegmentRPH = segmentval.flight.flightSegmentRPH;
                    flsegment.push(seg);
                }
            })

            angular.forEach(preferances,function(p){
                if (p.ondId != undefined) {
                    ondvisePassenger[p.ondId] = []
                }else{
                    seg.from = segmentval.flight.originCode;
                    seg.to = segmentval.flight.destinationCode;
                    seg.flightSegmentRPH = segmentval.flight.flightSegmentRPH;
                    flsegment.push(seg);
                }

            })

            angular.forEach(preferances,function(p){
                if (p.ondId != undefined) {
                    ondvisePassenger[p.ondId] = []
                }

            })

            //angular.forEach(flsegment, function (segment, segmentkey) {
                angular.forEach(passenger.allPassengers, function (passVal, pasKey) {
                    if (passVal.paxType != "Infant") {
                        var tempPas = {}
                        tempPas.passenger = {};
                        tempPas.passenger.passengerRph = passVal.paxSequence;
                        tempPas.passenger.firstName = passVal.firstName
                        tempPas.passenger.lastName = passVal.lastName
                        tempPas.passenger.displayName = passVal.title + ' ' + passVal.firstName + ' ' + passVal.lastName
                        tempPas.passenger.paxType = passVal.paxType;
                        //var anciArray = (passVal.selectedAncillaries[val.ancillaryType] != undefined) ? passVal.selectedAncillaries[val.ancillaryType].ancillaryScopes : [];
                        var anciObj = {}
                        tempAnci[passVal.paxSequence] = {}

                        angular.forEach(passVal.selectedAncillaries, function (anciValue, anciKx) {
                            tempAnci[passVal.paxSequence][anciKx] = {}
                            var anciType = anciValue.ancillaryType
                            angular.forEach(anciValue.ancillaryScopes, function (anciVal, anciK) {
                                ssrAirportScope [anciVal.scope.airportCode] = anciVal.scope;
                                var airportCode = anciVal.scope.airportCode;
                                //traverse through each ancillary scope and store them with segmentID or OND ID
                                var id = (anciVal.scope.flightSegmentRPH != undefined) ? anciVal.scope.flightSegmentRPH : anciVal.scope.ondId //anciVal.scope.ondId
                                /*var scopeType,id;
                                 if(anciVal.scope.flightSegmentRPH == undefined){
                                 scopeType = "OND"
                                 id = anciVal.scope.ondId;
                                 }else{
                                 scopeType = "SEGMENT"
                                 id = anciVal.scope.flightSegmentRPH;
                                 }*/
                                if(anciType == "SSR_AIRPORT") {
                                    var airportid = airportCode+'-'+id
                                    tempAnci[passVal.paxSequence][anciKx][airportid] = {};
                                }else{
                                    tempAnci[passVal.paxSequence][anciKx][id] = {}
                                }
                                angular.forEach(anciVal.ancillaries, function (ancinl, aKeynl) {
                                    var anciId = ancinl.id;
                                    if (anciType == "BAGGAGE") {
                                        ancinl.value = ancinl.name;
                                        ancinl.selected = true;
                                        ancinl.defaultBaggage = true;
                                        tempAnci[passVal.paxSequence][anciKx][id][0] = ancinl;
                                    } else if(anciType == "SEAT"){
                                        tempAnci[passVal.paxSequence][anciKx][id] = [];
                                        tempAnci[passVal.paxSequence][anciKx][id][0] = ancinl;
                                    } else if(anciType == "SSR_AIRPORT") {
//                                        console.log(airportCode)
                                        var airportid = airportCode+'-'+id;
                                        tempAnci[passVal.paxSequence][anciKx][airportid][anciId] = ancinl;
                                    }else {
                                        if(anciType == "MEAL") {
                                            ancinl.selectedItemTotal = ancinl.amount * ancinl.quantity;
                                        }
                                        tempAnci[passVal.paxSequence][anciKx][id][anciId] = ancinl;
                                    }
                                    //tempAnci[pasKey][anciKx][id][aKeynl].scopeType = scopeType;
                                })
                            })
                        });
                        pasObj[passVal.paxSequence]=tempPas;
                    }

                })

            //})

            angular.forEach(ancillaries, function (val, key) {
                var AncillaryItemDTO = modificationAncillaryFactory.AncillaryItemDTO(val.ancillaryType, 0);
                ancillaryModel[val.ancillaryType] = AncillaryItemDTO
                var ancillaryScope = modificationAncillaryFactory.AncillaryScopes(val.scopeType);
                var flSegement = []
                var apSegmentVisePassenger = {};
                var apSelectedItems = {};
                //if scope type is OND related segements should group under one ond id
                if(val.scopeType == "OND"){
                    var tempSeg =[];
                    angular.forEach(preferances,function(pref,pk){
                        tempSeg.push([])
                        angular.forEach(flsegment,function(seg,skey){
                            angular.forEach(pref.flightSegmentRPH, function(rph, idx) {
                                if (_.isEqual(rph, seg.flightSegmentRPH)) {
                                    tempSeg[tempSeg.length-1].push([]);
                                    tempSeg[tempSeg.length-1][tempSeg[tempSeg.length-1].length-1].push(seg);
                                }
                            });
                        })

                    })
                    flightSegments = tempSeg;
                }else{
                    flightSegments = flsegment;
                }
                angular.forEach(flightSegments, function (flseg, flSegmentKey) {
                    var segmentVisePassenger = [];
                    var airportVisePassenger = [];
                    var airportPassenger = {};
                    var airportSegmentDetails = {};
                    if(val.ancillaryType =='SSR_AIRPORT') {
                        var selectedItems = {};

                        for (var n = 0; n <= 1; n++) {
                            var key
                            var segm = (n == 0) ? flseg.from : flseg.to;
                            var point = (!isConnectionFlights)?((n == 0) ? "DEPARTURE" : "ARRIVAL"):"TRANSIT";
                            //var point = (_.findIndex(_.filter(flightSegments,{flightSegmentRPH:flightSegments[flSegmentKey].flightSegmentRPH}),{'from':flseg.from}) != -1) ? "DEPARTURE" : "ARRIVAL";
                            var segmentCode = (!!flightSegments[flSegmentKey]) ? flightSegments[flSegmentKey].flightSegmentRPH : undefined;
                            key = segm + '-' + segmentCode;
                            var airportKey = key+'-'+point;
                            apSegmentVisePassenger[airportKey] = [];
                            var apscop = {airportCode:segm,flightSegmentRPH:segmentCode,point:point,scopeType:"AIRPORT"}
                            angular.forEach(passenger.allPassengers, function (passVal, pasKey) {
                                //var scopeLength = Object.keys(passVal.selectedAncillaries[val.ancillaryType].ancillaryScopes).length;
                                if(passVal.paxType != "Infant") {
                                    if (!!passVal.selectedAncillaries[val.ancillaryType]) {
                                        var tp = _.filter(passVal.selectedAncillaries[val.ancillaryType].ancillaryScopes,function(item){
                                            return (item.scope.point == point && item.scope.airportCode == segm && item.scope.flightSegmentRPH == segmentCode)
                                        });
                                        if(tp.length){
                                            //if there is a passenger who has a selection get their selection
                                            apSelectedItems[airportKey] = _.each(_.indexBy(tp[0].ancillaries, 'id'),function(item){
                                                return _.extend(item,{'ssrName':item.name}) });
                                        }else{
                                            //if there is a passenger dose not has a selection add empty selection
                                            apSelectedItems[airportKey] = {};
                                        }
                                        var withAncipassenger = modificationAncillaryFactory.AncillaryPassenger(pasObj[pasKey + 1]['passenger'], apSelectedItems[airportKey], pasObj[pasKey + 1]['passenger'].passengerRph)
                                        apSegmentVisePassenger[airportKey].push(withAncipassenger)
                                        airportSegmentDetails[airportKey] = {"from": key.split('-')[0]};

                                    } else {
                                        //if there is a segment which any of the passenger dose not has a selection add them with empty selection
                                        var withoutAncipassenger = modificationAncillaryFactory.AncillaryPassenger(pasObj[pasKey + 1]['passenger'], {}, pasObj[pasKey + 1]['passenger'].passengerRph)
                                        apSegmentVisePassenger[airportKey].push(withoutAncipassenger);
                                        airportSegmentDetails[airportKey] = {"from": key.split('-')[0]};
                                    }
                                }

                            })
                            if(apSegmentVisePassenger[airportKey].length) {
                               airportPassenger[airportKey] = apSegmentVisePassenger[airportKey];
                            }
                        }
                    }else {
                        //create passenger with selected ancillaries
                        angular.forEach(passenger.allPassengers, function (passVal, pasKey) {
                            if(passVal.paxType != "Infant") {
                                if (!!tempAnci[pasKey + 1]) {
                                    if (tempAnci[pasKey + 1][val.ancillaryType] != undefined) {
                                        var selectedItems = [];
                                        if (val.scopeType == "OND") {
                                            if ((Object.keys(preferances).length) > 1) {
                                                _.each(preferances, function (pref, idx) {
                                                    if (!!tempAnci[pasKey + 1][val.ancillaryType][pref.ondId]) {
                                                        selectedItems = tempAnci[pasKey + 1][val.ancillaryType][pref.ondId];
                                                    }
                                                    var ondpassenger = modificationAncillaryFactory.AncillaryPassenger(pasObj[pasKey + 1]['passenger'], selectedItems, pasObj[pasKey + 1]['passenger'].passengerRph)
                                                    if(_.findIndex(ondvisePassenger[pref.ondId],{passengerRph:ondpassenger.passengerRph}) == -1) {
                                                        ondvisePassenger[pref.ondId].push(ondpassenger)
                                                    }
                                                });
                                            } else {
                                                selectedItems = tempAnci[pasKey + 1][val.ancillaryType][getOndFromRph(flseg[0][0].flightSegmentRPH)];
                                                var ondpassenger = modificationAncillaryFactory.AncillaryPassenger(pasObj[pasKey + 1]['passenger'], selectedItems, pasObj[pasKey + 1]['passenger'].passengerRph)
                                                ondvisePassenger[preferances[0].ondId].push(ondpassenger)
                                            }
                                        } else {
                                            selectedItems = (tempAnci[pasKey + 1][val.ancillaryType][flseg.flightSegmentRPH] != undefined) ? tempAnci[pasKey + 1][val.ancillaryType][flseg.flightSegmentRPH] : tempAnci[pasKey + 1][val.ancillaryType][getOndFromRph(flseg.flightSegmentRPH)];
                                            selectedItems = (!!selectedItems) ? selectedItems : {};
                                            //angular.forEach(passenger.allPassengers, function (passVal, pasKey) {
                                            //if (passVal.paxType != "Infant") {
                                            var withAncipassenger = modificationAncillaryFactory.AncillaryPassenger(pasObj[pasKey + 1]['passenger'], selectedItems, pasObj[pasKey + 1]['passenger'].passengerRph)
                                            segmentVisePassenger.push(withAncipassenger)
                                            //}
                                            //})
                                        }
                                    } else {
                                        selectedItems = {};
                                        var withoutAncipassenger = modificationAncillaryFactory.AncillaryPassenger(pasObj[pasKey + 1]['passenger'], selectedItems, pasObj[pasKey + 1]['passenger'].passengerRph)
                                        segmentVisePassenger.push(withoutAncipassenger)

                                    }
                                }
                            }
                        })
                    }

                    if (val.scopeType == "OND" && !!preferances[flSegmentKey]) {
                        if ((Object.keys(preferances).length) > 1) {
                            //------------------ Return flight----------------
                            flSegement[flSegmentKey] = [];
                            flSegement[flSegmentKey].push(flseg)
                            angular.forEach(preferances, function (pref, prefKey) {
                                ancillaryPreferance = modificationAncillaryFactory.AncillaryPreference(preferances[prefKey].ondId, flSegement[prefKey], ondvisePassenger[pref.ondId]);
                                ancillaryScope.preferences[prefKey] = (ancillaryPreferance);
                            })
                        } else {
                            //----------------- Oneway Flight -----------------
                            flSegement.push(flseg)
                            if (preferances.length > 0) {
                                ancillaryPreferance = modificationAncillaryFactory.AncillaryPreference(preferances[0].ondId, flSegement, ondvisePassenger[preferances[0].ondId]);
                                ancillaryScope.preferences[0] = (ancillaryPreferance);
                            }
                        }

                    } else if (val.scopeType == "AIRPORT") {
                        if(!!airportSegmentDetails) {
                            angular.forEach (airportSegmentDetails, function(seg,sk){
                                console.log(airportSegmentDetails)
                                var segmentDetail = [];
                                var prefid = {};
                                segmentDetail.push(seg);
                                prefid.airportCode = seg.from
                                prefid.flightSegmentRPH = sk.split('-')[1];
                                var segment = _.filter(flightSegments,{flightSegmentRPH:sk.split('-')[1]});
                                prefid.point = (_.findIndex(_.filter(flightSegments,{flightSegmentRPH:sk.split('-')[1]}),{'from':seg.from}) != -1) ? "DEPARTURE" : "ARRIVAL";
                                if(isConnectionFlights) {
                                	prefid.point = "TRANSIT";
                                }
                                if (!!airportPassenger[sk]) {
                                    console.log(flseg)
                                    ancillaryPreferance = modificationAncillaryFactory.AncillaryPreference(prefid, segmentDetail, airportPassenger[sk]);
                                    ancillaryScope.preferences.push(ancillaryPreferance);
                                }
                            })
                        }

                    } else {
                        flSegement = []
                        flSegement.push(flseg)
                        ancillaryPreferance = modificationAncillaryFactory.AncillaryPreference(flseg.flightSegmentRPH, flSegement, segmentVisePassenger);
                        ancillaryScope.preferences.push(ancillaryPreferance);
                    }
                })
                if(ancillaryModel[val.ancillaryType] != undefined) {
                    if(ancillaryScope.preferences.length) {
                        if(ancillaryScope.preferences[0].passengers.length) {
                            if (val.ancillaryType == "BAGGAGE" && ancillaryScope.scopeType == "OND") {
                                var tpsegment = [];
                                angular.forEach(ancillaryScope.preferences, function (pref, k) {
                                    var ts = pref.segments;
                                    pref.segments = [];
                                    if (!!ts) {
                                        angular.forEach(ts[0], function (s) {
                                            pref.segments.push(s[0])
                                        })
                                    }
                                })
                            }
                            //re arrange flight segments in ascending order according to preferance id
                            ancillaryScope.preferences = ancillaryScope.preferences.sort(function (a, b) {
                                var valueA = Number(a.preferanceId);
                                var valueB = Number(b.preferanceId)
                                return (valueA > valueB) ? 1 : ((valueA > valueB) ? -1 : 0);
                            });

                            ancillaryModel[val.ancillaryType].scopes.push(ancillaryScope);
                        }else{
                            delete ancillaryModel[val.ancillaryType];
                        }
                    }else{
                        delete ancillaryModel[val.ancillaryType];
                    }
                }

            });


            ancillaryService.setAncillaryModel(ancillaryModel);
        };


        this.setAncillaryModelFromReProtectResponse = function (reProtectResponse) {
            commonLocalStorageService.setData("ancillaryReProtect", reProtectResponse.ancillaryReProtect);
            ancillaryService.setAncillaryModel({});

            var getPaxFromRPH = function (allPax, rph) {
                var pax = null;
                for (var i = 0; i < allPax.length; i++) {
                    var obj = allPax[i];
                    if (obj.paxSequence == rph) {
                        pax = obj;
                        break;
                    }
                }
                return pax;
            }

            var updatePaxAncillaries = function (passengerDetails, reProtectPaxCol) {
                var reservationPaxCol = passengerDetails.allPassengers;
                angular.forEach(reProtectPaxCol, function (pax) {
                    var p = getPaxFromRPH(reservationPaxCol, pax.passengerRph);
                    if (!!p) {
                        p.selectedAncillaries = pax.selectedAncillaries;
                    }
                });
                return passengerDetails;
            }


            if (!!reProtectResponse) {
                var passengerDetails = updatePaxAncillaries(model.reservation.passengerDetails, reProtectResponse.passengers);
                var ancillaries = reProtectResponse.ancillaries;
                var ondPreferences = reProtectResponse.ondPreferences;
                var segments = commonLocalStorageService.getConfirmedFlightData();

                var validSegments = self.getValidSegments(segments);
                self.setAncillaryModel(ancillaries, validSegments, passengerDetails, ondPreferences);
            }

            var unModifiedSegmentAnci = getUnmodifiedSegmentAnci();
            ancillaryService.setUnmodifiedAnci(unModifiedSegmentAnci)

            // TODO: Pushing of the unmodified segments to existing anci model temporaraliy removed.

/*          // Backup un-modififed ancillary

            var modifiedSegmentAnci = ancillaryService.getAncillaryModel();

            _.forEach(unModifiedSegmentAnci, function(anciType) {
                if(_.has(modifiedSegmentAnci, anciType.ancillary)){
                    //If the modifed segment has values for the given anci type, add preference to existing model
                    _.forEach(anciType.scopes[0].preferences, function(pref) {
                        modifiedSegmentAnci[anciType.ancillary].scopes[0].preferences.push(pref);
                    })
                }else{
                    //If modified segment does not contain the anci type, set the value directly
                    modifiedSegmentAnci[anciType.ancillary] = anciType;
                }
            });

            ancillaryService.setAncillaryModel(modifiedSegmentAnci);*/
        };

        // Get the ancillaries form the segments that were not modified
        function getUnmodifiedSegmentAnci() {
            // Get the rph and ond ids of the modified segments
            var modifiedSegment = model.selectedFareGroupSegmentsToModify;
            var modifiedFlightRphList = [];
            var modifiedOnd = null;

            // Get the modified segment rph and ONDs
            _.forEach(modifiedSegment.segments, function(segment) {
                modifiedFlightRphList.push(segment.flight.flightRPH);
                modifiedOnd = getOndFromFlightRph(segment.flight.flightRPH);
            });

            var tempAnci = angular.copy(self.getUnchangedAnciModel());

            //Remove ancillaries of the modified flight
            _.forEach(tempAnci, function(anciType) {
                _.forEach(anciType.scopes[0].preferences, function(pref){

                    //Add unEditable flag
                    pref.unEditable = true;

                    var ancillaryScope = anciType.scopes[0];

                    switch (ancillaryScope.scopeType) {
                        case 'OND':
                            ancillaryScope.preferences = _.filter(ancillaryScope.preferences, function(pref) {
                                return pref.preferanceId !== modifiedOnd;
                            });
                            break;
                        case 'SEGMENT':
                            ancillaryScope.preferences = _.filter(ancillaryScope.preferences, function(pref) {
                                return (_.indexOf(modifiedFlightRphList, pref.preferanceId) === -1);
                            });
                            break;
                        case 'ALL_SEGMENTS':
                            // TODO: Logic to handel insurance
                            ancillaryScope.preferences = [];
                            break;
                        case 'AIRPORT':
                            ancillaryScope.preferences = _.filter(ancillaryScope.preferences, function(pref) {
                                return ((_.indexOf(modifiedFlightRphList, pref.preferanceId.flightSegmentRPH) === -1) && preferenceHasSelection(pref));
                            });
                            break;
                    }
                });
            });

            return tempAnci;
        }

        //If at least one passenger has made a selection for the ancillary prefence, will return true
        function preferenceHasSelection(pref){
            var hasSelection = false;

            _.forEach(pref.passengers, function(passenger){
                if(!_.isEmpty(passenger.selectedItems)){
                    hasSelection = true;
                }
            });

            return hasSelection;
        }

        // Given an flightRph return the associated ondId
        function getOndFromFlightRph(flightRph) {
            var ondPrefs = self.getOndPreferences();
            var ondId = null;

            _.forEach(ondPrefs, function(ond) {
                _.find(ond.flightSegmentRPH, function(segment) {
                    if(segment == flightRph) {
                        ondId = ond.ondId;
                    }
                })
            });

            return ondId;
        }

        this.getNameChangedPassengerList = function (originalPaxList, modifiedPaxList) {
            var nameChangedPaxList = [];

            if (!!originalPaxList && !!modifiedPaxList) {
                angular.forEach(originalPaxList, function (oriPax, index) {
                    if ((oriPax.firstName !== modifiedPaxList[index].firstName ) || (oriPax.lastName !== modifiedPaxList[index].lastName)) {
                        nameChangedPaxList.push(modifiedPaxList[index]);
                    }
                });
            }
            return nameChangedPaxList;
        };

        this.getFfidChangePassengerList = function (originalPaxList, modifiedPaxList) {
            var nameChangedPaxList = [];

            if (!!originalPaxList && !!modifiedPaxList) {
                angular.forEach(originalPaxList, function (oriPax, index) {
                    if (oriPax.airewardsEmail !== modifiedPaxList[index].airewardsEmail ) {
                        nameChangedPaxList.push(modifiedPaxList[index]);
                    }
                });
            }
            return nameChangedPaxList;
        };

        this.reloadReservation = function (depDate) {
            // If departure date is provided as a parameter update the model value
            if(depDate){
                model.departureDate = depDate;
            }

            $state.go('modifyReservation', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                pnr: model.pnr,
                lastName: model.lastName,
                departureDate: model.departureDate
            });
        };

        this.setConfirmedFlightData = function (segments) {
            var confirmedFlights = travelFareService.getConfirmedFlights();
            //remove All
            angular.forEach(confirmedFlights, function (o, k) {
                delete confirmedFlights[k]
            });

            if (!!segments) {
                angular.forEach(segments, function (segment, index) {
                    travelFareService.setConfirmedFlights(segment, index);
                });
            }
            commonLocalStorageService.setConfirmedFlightData(travelFareService.getConfirmedFlights());
        };


        this.replaceModifiedSegmentsWithNewSegments = function () {
            var confirmedSegments = angular.copy(model.reservation.segments);
            var segmentsToRemove = angular.copy(model.selectedFareGroupSegmentsToModify.segments);

            var removeSegmentByRPH = function (allSegments, rph) {
                var seq = null;
                if (!!allSegments) {
                    for (var i = 0; i < allSegments.length; i++) {
                        var segment = allSegments[i];
                        if (segment.flight.reservationSegmentRPH == rph) {
                            seq = i;
                            break;
                        }
                    }
                }
                allSegments.splice(seq, 1);
            }

            angular.forEach(segmentsToRemove, function (segment) {
                removeSegmentByRPH(confirmedSegments, segment.flight.reservationSegmentRPH);
            });

            confirmedSegments.push({
                fare: self.getSelectedNewFareClass(),
                flight: self.getSelectedNewFlight()
            });

            self.setConfirmedFlightData(confirmedSegments);
        }


        this.getModifiableAncillaries = function (availableAncillaries, reservationSelectedAncillaries) {
            var list = [];
            var getAncillary = function (type, allSelected) {
                var found = null;
                if (!!allSelected) {
                    for (var i = 0; i < allSelected.length; i++) {
                        var obj = allSelected[i];
                        if (obj.ancillaryType == type) {
                            found = obj;
                            break;
                        }
                    }
                }
                return found;
            };

            angular.forEach(availableAncillaries, function (item) {
                if (item.available) {
                    var isSelected = getAncillary(item.type, reservationSelectedAncillaries);
                    list.push({
                        ancillaryType: item.type,
                        ancillaryImageClass: appConstants.ancillaryImageClasses[item.type] || appConstants.ancillaryImageClasses['DEFAULT'],
                        isSelected: (!!isSelected) ? true : false,
                        amount: (!!isSelected) ? isSelected.amount : 0,
                        scopeType: (!!isSelected) ? isSelected.scopeType : '',
                        isEnable: true
                    });
                }
            });

            //Add selected Ancillaries that are not available now
            angular.forEach(reservationSelectedAncillaries, function (selectedItem) {
                var tmp = getAncillary(selectedItem.ancillaryType, list);
                if (tmp == null) {
                    selectedItem.isEnable = false;
                    list.push(selectedItem);
                }
            });

            return list;
        };


        this.makeCancelReservationChanges = function () {
            model.reservation.bookingDetails.statusCode = self.RESERVATION_STATUS.CANCEL;
            model.reservation.bookingDetails.status = 'Cancelled';
            angular.forEach(model.reservation.segments, function (segment) {
                segment.flight.status = 'CNX';
            });
            persistServiceDataModel();
        };

        /**
         * Load Reservation Api Helper function
         * @param loadReservationRQ
         * @param successFn
         * @param errorFn
         */
        this.loadReservation = function (pnr, lastName, departureDate, successFn, errorFn) {
            var param = this.getLoadReservationRQ(pnr, lastName, departureDate);
            IBEDataConsole.log("---LoadReservationReq ---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.loadReservation(param);
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Cancel Reservation API Helper function
         * @param successFn
         * @param errorFn
         */
        this.cancelReservation = function (successFn, errorFn) {
            var param = this.getCancelReservationReq();
            IBEDataConsole.log("---CancelReservationReq ---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.cancelReservation(param);
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Get Balance Summery API helper function
         * @param successFn
         * @param errorFn
         */
        this.getBalanceSummaryForCancelReservation = function (successFn, errorFn) {
            var param = this.getCancelReservationReq();
            IBEDataConsole.log("---CancelReservationReq - Balance Summery ---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.getBalanceSummaryForCancelReservation(param);
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Get Balance Summery For Cancel Segment
         * @param successFn
         * @param errorFn
         */
        this.getBalanceSummaryForCancelSegment = function (successFn, errorFn) {
            var promise = modificationRemoteService.getBalanceSummaryForSegmentModification();
            promise.then(
                function (response) {
                    self.setBalanceSummery(response, self.BALANCE_SUMMERY_TYPE.CANCEL_FLIGHT);
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Get Balance Summery For Modify Segment
         * @param successFn
         * @param errorFn
         */
        this.getBalanceSummaryForModifySegment = function (successFn, errorFn) {
            var promise = modificationRemoteService.getBalanceSummaryForSegmentModification();
            promise.then(
                function (response) {
                    self.setBalanceSummery(response, self.BALANCE_SUMMERY_TYPE.MODIFY_FLIGHT);
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Get Balance Summery For Add Remove Anci After Segment Modification
         * @param successFn
         * @param errorFn
         */
        this.getBalanceSummaryForModifySegmentAfterAnci = function (successFn, errorFn) {
            var promise = modificationRemoteService.getBalanceSummaryForModifySegmentAfterAnci();
            promise.then(
                function (response) {
                    self.setBalanceSummery(response, self.BALANCE_SUMMERY_TYPE.MODIFY_SEGMENT_WITH_ANCILLARY);
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Get Balance Summery for Ancillary Modification
         * @param successFn
         * @param errorFn
         */
        this.getBalanceSummaryForAncillaryModification = function (successFn, errorFn) {
            var promise = modificationRemoteService.getBalanceSummaryForAncillaryModification();
            promise.then(
                function (response) {
                    self.setBalanceSummery(response, self.BALANCE_SUMMERY_TYPE.MODIFY_ANCILLARY);
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Updeate Reservation Passenger Name list
         * @param nameChangedPaxList
         * @param successFn
         * @param errorFn
         */
        this.updateReservationPaxNames = function (nameChangedPaxList, successFn, errorFn) {
            var param = getNameChangeReq(nameChangedPaxList);
            IBEDataConsole.log("---NameChangeReq---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.updateReservationPaxNames(param);
            promise.then(
                function (response) {
                    self.setBalanceSummery(response, self.BALANCE_SUMMERY_TYPE.PAX_NAME_CHANGE);
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };

        /**
         * Updeate Passenger FFID information
         * @param nameChangedPaxList
         * @param successFn
         * @param errorFn
         */
        this.changeReservationFfid = function(nameChangedPaxParam, successFn, errorFn) {
            var param = getFfidChangeReq(nameChangedPaxParam.paxList);
            param.withNameChange = nameChangedPaxParam.withNameChange;

            IBEDataConsole.log("---FFID ChangeRQ---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.changeReservationFfid(param);
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };

        /**
         *
         * @param balanceSummary
         */
        this.setBalanceSummaryForOnhold = function(balanceSummary){
            self.setBalanceSummery(balanceSummary, self.BALANCE_SUMMERY_TYPE.ON_HOLD);
        };


        /**
         * Perform Requot for Cancel Flight Segment
         * @param successFn
         * @param errorFn
         */
        this.doRequotForcancelFlightSegments = function (successFn, errorFn) {
            var param = getCancelSegmentRequotReq();
            IBEDataConsole.log("---CancelSegmentRequotReq---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.doRequote(param);
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Perform Requot for Flight Modification
         * @param successFn
         * @param errorFn
         */
        this.doRequotForModifyFlightSegment = function (successFn, errorFn,type) {
            var param = getSegmentModificationRequotRq(type);
            var modifiedFlightRph = param.modificationInfo && param.modificationInfo.modifiedFlights
                && param.modificationInfo.modifiedFlights[0] && param.modificationInfo.modifiedFlights[0].toFlightSegRPH
                && param.modificationInfo.modifiedFlights[0].toFlightSegRPH[0];

            IBEDataConsole.log("---SegmentModificationRequotRq---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.doRequote(param,type);
            promise.then(
                function (response) {
                    successFn(response, modifiedFlightRph);
                },
                function (error) {
                    model.selectedNewFlight={}
                    errorFn(error);
                }
            );
        };

        this.adaptFareClasses = function(fareClasses,selectedFareClasses ){
            var fare=[];
            for(var i=0;i<selectedFareClasses.length;i++){
                for (var j=0;j<fareClasses.length;j++){
                    if(selectedFareClasses[i].fareClassCode === fareClasses[j].fareClassCode){
                        fare.push(_.extend(fareClasses[j], {
                                fareValue: selectedFareClasses[i].price,
                                fareTypeId: selectedFareClasses[i].fareClassCode,
                                fareTypeName: selectedFareClasses[i].description
                            }
                        ));
                    }
                }
            }
            return fare;

        }
        /**
         * Confirm Cancel Flight segment service call
         * @param successFn
         * @param errorFn
         */
        this.confirmCancelFlightSegments = function (successFn, errorFn) {
            var param = getConfirmCancelSegmentReq();
            IBEDataConsole.log("---ConfirmCancelSegmentReq---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.confirmCancelFlightSegments(param);
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        /**
         * Flight Modification Flight Search Service call
         * @param successFn
         * @param errorFn
         */
        this.doModificationFlightSearch = function (successFn, errorFn) {
            var param = getModificationFlightSearchRq();
            IBEDataConsole.log("---ModificationFlightSearchRq---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.doModificationFlightSearch(param);
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        this.getDefaultAncillariesOnModifyReservation = function (successFn, errorFn, alwaysFn) {
            var promise = modificationRemoteService.getDefaultAncillariesOnModifyReservation();
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
            promise.finally(function () {
                alwaysFn();
            });
        };


        this.getAncillaryAvailability = function (successFn, errorFn,modifyEdit) {
            ancillaryRemoteService.initializeAncillary(getInitializeAncillaryRq(),
                function (data) {
                    successFn(data);
                },
                function (error) {
                    errorFn(error)
                },modifyEdit);
        };

        this.loadAvailableAncillaryData = function (availabilityAncillaryResponse, successFn, errorFn) {
            ancillaryRemoteService.loadAvailableAncillaryData(getLoadAvailableAncillaryRq(availabilityAncillaryResponse),
                function (data) {
                    successFn(data);
                },
                function (error) {
                    errorFn(error)
                }
            );
        };


        this.getValidSegments = function (segments) {
            var validSegments = [];
            angular.forEach(segments, function (segment) {
                if (segment.flight.status != 'CNX') {
                    validSegments.push(segment);
                }
            });
            return validSegments;
        };
        this.setAncillaryStatus = function(status){
            isAncillaryAvailable = status;
        }
        this.getAncillaryStatus = function(){
            return isAncillaryAvailable;
        }


        this.getAncillarySummery = function (successFn, errorFn) {
            var param = getAncillaryAvailabilityRQ();
            IBEDataConsole.log("---Ancillary summery - AncillaryAvailabilityRQ ---");
            IBEDataConsole.log(param);
            var promise = modificationRemoteService.getAncillarySummery(param);
            promise.then(
                function (response) {
                    self.setAncillarySummery(response);
                    var tSegments = self.getValidSegments(model.reservation.segments);
                    ancillaryService.setAncillaryModel({});
                    self.setAncillaryModel(response, tSegments, model.reservation.passengerDetails, model.reservation.ondPreferences);

                    //At init load backup ancillaryModel to re-use if user does partial modifications.
                    self.setUnchangedAnciModel(ancillaryService.getAncillaryModel());
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };

        this.setUnchangedAnciModel = function(model) {
            commonLocalStorageService.setData('MODIFY.RESERVATION.UNCHANGED_MODEL', model);
            self.unChangedAnciModel = angular.copy(model);
        };

        this.getUnchangedAnciModel = function() {
            if(_.isUndefined(self.unChangedAnciModel)) {
                self.unChangedAnciModel = commonLocalStorageService.getData('MODIFY.RESERVATION.UNCHANGED_MODEL');
            }
            return self.unChangedAnciModel;
        };

        this.confirmModifyFlightSegments = function (successFn, errorFn) {
            var promise = modificationRemoteService.confirmModifyFlightSegments();
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };
        this.getLatestFlightDepartureDate = function(reservationData){
            var flightDateArray = [];

            // Add non-canceled flights in to array and push dates to array
            angular.forEach(reservationData, function(reservation){
                if(reservation.status != self.RESERVATION_STATUS.CANCEL){
                    flightDateArray.push(
                        {
                            unixTimeStamp: new Date(reservation.departureDateTime.local).getTime(),
                            date: reservation.departureDateTime.local
                        }
                    );
                }
            });

            // Find the earliest flight date of a non canceled fight and look up the reservation
            return _.min(flightDateArray, function(date){
                return date.unixTimeStamp;
            });
        }


        this.confirmAncillaryModification = function (successFn, errorFn) {
            var promise = modificationRemoteService.confirmAncillaryModification();
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };


        this.confirmNameChange  = function (successFn, errorFn) {
            var promise = modificationRemoteService.confirmNameChange();
            promise.then(
                function (response) {
                    successFn(response);
                },
                function (error) {
                    errorFn(error);
                }
            );
        };

        this.getOndPreferences = function() {
            return model.reservation.ondPreferences;
        };

        this.modifyPriceBreakdown = function(){
            var groupWiseSegment = angular.copy(self.getFareGroupWiseSegmentFromReservation());
            _.forEach(groupWiseSegment, function(segmentGroup){
                segmentGroup.status = segmentGroup.status[0];
            });

            var balanceSummeryData = self.getBalanceSummery();
            var charges = balanceSummeryData.balanceSummery.updatedChargeForDisplay || balanceSummeryData.balanceSummery.chargeBreakdown || {};

            var formattedCharges = _.indexBy(charges.chargeListToDisplay, 'displayDescription');

            var displayCharges = {};

            if(balanceSummeryData.balanceSummery.modifiedBalanceInfo){
                var passengerList = balanceSummeryData.balanceSummery.modifiedBalanceInfo.passengerSummaryList;
                var passengerSummary = {
                    adult: {
                        count: 0,
                        total: 0
                    },
                    child: {
                        count: 0,
                        total: 0
                    },
                    infant: {
                        count: 0,
                        total: 0
                    }
                };

                _.forEach(passengerList, function(passenger){
                    switch(passenger.paxType){
                        case 'AD':
                            passengerSummary.adult.count++;
                            passengerSummary.adult.total = passengerSummary.adult.total + passenger.totalPrice;
                            break;
                        case 'CH':
                            passengerSummary.child.count++;
                            passengerSummary.child.total = passengerSummary.child.total + passenger.totalPrice;
                            break;
                        case 'IN':
                            passengerSummary.child.count++;
                            passengerSummary.child.total = passengerSummary.child.total + passenger.totalPrice;
                            break;
                    }
                });
            }

            if (!_.isEmpty(formattedCharges)) {
                if(formattedCharges["Air fare"] && formattedCharges["Taxes & Surcharges"]) {
                    displayCharges.newTotal = Number(formattedCharges["Air fare"].displayNewCharge) + Number(formattedCharges["Taxes & Surcharges"].displayNewCharge);
                    displayCharges.oldTotal = Number(formattedCharges["Air fare"].displayOldCharge) + Number(formattedCharges["Taxes & Surcharges"].displayOldCharge);
                }
                if(formattedCharges["Non Refundable Amount"]) {
                    displayCharges.nonRefundable = Number(formattedCharges["Non Refundable Amount"].displayOldCharge);
                }
                if(formattedCharges["Modification Charge"]) {
                    displayCharges.modification = Number(formattedCharges["Modification Charge"].displayNewCharge);
                }
                if(formattedCharges["Taxes & Surcharges"]) {
                    displayCharges.taxes = Number(formattedCharges["Taxes & Surcharges"].displayNewCharge);
                }
            }

            return {
                passengerSummary: passengerSummary,
                displayCharges: displayCharges,
                groupWiseSegment: groupWiseSegment,
                paxSummary: balanceSummeryData.balanceSummery.paxSummary,
                modifyCharges: balanceSummeryData.balanceSummery.modifyCharges
            }
        };

        this.getOndPreferences = function() {
            return model.reservation.ondPreferences;
        };
        this.setSelectedFareGroupIndex = function(index){
            selectedFareGroupIndex = index;
        }
        this.getSelectedFareGroupIndex = function(){
            return selectedFareGroupIndex;
        }

    }
})(angular);
