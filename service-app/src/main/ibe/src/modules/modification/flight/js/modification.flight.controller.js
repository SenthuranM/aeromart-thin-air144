/**
 * Created by sumudu on 1/26/16.
 */
'use strict';

(function (angular) {
    var module = angular.module('modification.reservation');


    /**
     * Modification Home Controller
     * Show All Available Flight Segments to Modify
     */
    module.controller('ModifyFlightHomeController', [
        '$state',
        '$location',
        'loadingService',
        'modifyReservationService',
        'languageService',
        'currencyService',
        'applicationService',
        'quoteService',
        'IBEDataConsole',
        'navigationService',
        '$translate',
        'alertService',
        '$timeout',
        '$window',
        'constantFactory',
        '$rootScope',
        'passengerRemoteService',
        'eventListeners',
        'commonLocalStorageService',
        'signoutServiceCheck',
        function ($state, $location, loadingService, modifyReservationService, languageService, currencyService,
                applicationService, quoteService, IBEDataConsole, navigationService, $translate, alertService, $timeout,$window,constantFactory,$rootScope,passengerRemoteService,eventListeners,commonLocalStorageService,signoutServiceCheck) {
            var self = this;
            $rootScope.showConfirmButtons = false;
            
            self.selectedLanguage =languageService.getSelectedLanguage();
        
            this.getFlightDirection = function (toCode, fromCode, isModify) {
                return applicationService.getFlightDirection(toCode, fromCode, isModify)
            }

            window.scrollTo(0, 0);
            navigationService.setReprotectAlertSatet(false);

            loadingService.hidePageLoading();
            quoteService.setShowSummaryDrawer(false);

            this.model = modifyReservationService.getReservation();
            
            this.getReservationSearchCriteria = modifyReservationService.getReservationSearchCriteria();
            self.isUserLoggedIn = applicationService.getCustomerLoggedState();
            $rootScope.removePaddingFromXSContainerClass = true;
            
            if (this.model && this.model.fareGroupWiseSegments) {
                var segment = this.model.fareGroupWiseSegments.sort(function (a, b) {
                    var valueA = moment(a.segments[0].flight.arrivalDateObj).valueOf();
                    var valueB = moment(b.segments[0].flight.arrivalDateObj).valueOf();
                    return (valueA > valueB) ? 1 : ((valueA > valueB) ? -1 : 0);
                });
                this.model.fareGroupWiseSegments = segment;
            }
            if(!self.isUserLoggedIn){
            	signoutServiceCheck.checkSignoutOperation();
            }
            
            var counter=0;
            self.modifyStyle; 
            this.model.fareGroupWiseSegments.forEach(function(fare) {
                if(fare.segments.length > 1){
                    counter++;
                }
                self.modifyStyle = (counter > 1) ?  { "height": "100%" } : {'display': 'flex','align-items': 'flex-start','flex-direction':'column', 'height':'auto'};
            });
            

            this.selectedFareGroupIndex = null;
            this.formatDateTime = function (date, time) {
                var d
                if (date.indexOf('/') == -1) {
                    d = moment(date).format('DD/MM/YYYY');
                    //console.log(date);
                } else {
                    d = date;
                }
                return d + ' ' + time;
            }
            this.cancelModification = function () {
                $state.go('modifyFlightHome.flightSearchResult',
                        {lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode()}
                );
                ;
            };
            this.onConfirmModification = function () {
                var paxDetails = modifyReservationService.getReservation().passengerDetails.allPassengers;

                passengerRemoteService.duplicateCheck(paxDetails,
                        function (response) {
                            if (response.hasDuplicates) {
                                var message = "Duplicate name(s) found in the flight segment";
                                alertService.setAlert(message, 'warning');
                            } else {
                                modifyReservationService.doRequotForModifyFlightSegment(
                                        function (data) {
                                            modifyReservationService.getBalanceSummaryForModifySegment(
                                                    function (data) {
                                                        quoteService.changeShowQuote();
                                                        quoteService.setShowSummaryDrawer(true);
                                                    },
                                                    function (error) {
                                                        IBEDataConsole.log('FLIGHT MODIFICATION BALANCE SUMMERY ERROR');
                                                    }
                                            );
                                        },
                                        function (error) {
                                            alertService.setAlert(error.error, 'danger');
                                        },
                                        'requote'
                                        );
                            }
                        },
                        function (data) {
                            if (data.errorType === alertService.ERRORTYPE.TRANSACTION_ID_ERROR) {
                                alertService.setPageAlertMessage(data);
                            } else {
                                alertService.setAlert(data.error, 'danger');
                            }
                        }
                );
            };
            
            self.fareGroupWiseSegCount = 0;
            this.model.fareGroupWiseSegments.forEach(function(fareGroupSegment) {
            	if(fareGroupSegment.status[0] != "CNX"){
            		self.fareGroupWiseSegCount += 1;
            	}
            });
            
            this.onFlightSelected = function (selectedIndex, fareGroup) {
            	var depTime =fareGroup.segments[0].flight.departureDate.replace("-","/").replace("-","/");
            	$window.localStorage.setItem('depTime',depTime);
                var carrierCode =  constantFactory.AppConfigStringValue('defaultCarrierCode');
                var selectedSegmentRph = fareGroup.segments[0].flight.reservationSegmentRPH;

                var selectedSegmentModifyParams = _.find(self.model.modificationParams.all.segmentModificationParams, function (segmentModifyParams) {
                    return selectedSegmentRph == segmentModifyParams.reservationSegmentRPH;
                });

                if(self.fareGroupWiseSegCount == 1 && selectedSegmentModifyParams.cancellable){
                	selectedSegmentModifyParams.cancellable = false;
                }

                self.selectedFareGroupIndex = selectedIndex;
                modifyReservationService.setSelectedFareGroupIndex(selectedIndex);
                $state.get('modifyFlightHome.flightSelected').data.cancellable = String(selectedSegmentModifyParams.cancellable);
                $state.get('modifyFlightHome.flightSelected').data.modifible = String(selectedSegmentModifyParams.modifible);
                //if(!isUNSegment(self.model.fareGroupWiseSegments[self.selectedFareGroupIndex])) {
                modifyReservationService.setSelectedFareGroupSegmentsToModify(self.model.fareGroupWiseSegments[self.selectedFareGroupIndex], self.selectedFareGroupIndex);

                $state.go('modifyFlightHome.flightSelected',
                        {lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode()}
                );
                /*}else{
                 $translate.use(languageService.getSelectedLanguage().toLowerCase()).then(function () {
                 $translate('msg_modification_flight_un_segment').then(function (msg) {
                 self.flightSelectMsg = msg;
                 })
                 })
                 $state.go('modifyFlightHome.flightHome', {
                 lang: languageService.getSelectedLanguage(),
                 currency: currencyService.getSelectedCurrency(),
                 mode: applicationService.getApplicationMode()
                 });
                 }*/
            };
            function isUNSegment(fareGroupWiseSegments) {
                var isUNSegement = false;
                for (var n = 0; n < fareGroupWiseSegments.segments.length; ) {
                    if (fareGroupWiseSegments.segments[n].flight.standardStatus == 'UN' || fareGroupWiseSegments.segments[n].flight.flownSegment) {
                        isUNSegement = true;
                        break;
                    }
                    n++;
                }
                return isUNSegement;
            }
            this.backToHome = function () {
                if (self.isUserLoggedIn) {
                    navigationService.modifyFlowHome();
                } else {
                    //loadReservation
                    navigationService.modifyFlowHome();
                }
            };
            
            eventListeners.registerListeners(eventListeners.events.CUSTOMER_LOGIN_STATUS_CHANGE, function () {
            	signoutServiceCheck.checkSignoutOperation();
            });

            self.flightHeightList = []; //Array of heights of flightList elements used for resizing modify boxes
            (function () {
                if( $window.localStorage )
                      {
                        if( !$window.localStorage.getItem( 'firstLoad1' ) )
                        {
                          $window.localStorage[ 'firstLoad1' ] = true;
                          $window.location.reload();
                        }  
                        else
                         $window.localStorage.removeItem( 'firstLoad1' );
                      }
                var tmpSelected = modifyReservationService.getSelectedFareGroupSegmentsToModify();
                self.flightSelectMsg = "";
                $translate.use(languageService.getSelectedLanguage().toLowerCase()).then(function () {
                    $translate('msg_modification_flight_select').then(function (msg) {
                        self.flightSelectMsg = msg;
                    })
                });

                /**
                 * Set Selected Segment upon refresh
                 */
                if (!!tmpSelected && !!self.model.fareGroupWiseSegments) {
                    for (var i = 0; i < self.model.fareGroupWiseSegments.length; i++) {
                        var obj = self.model.fareGroupWiseSegments[i];
                        if (tmpSelected.fareGroupID == obj.fareGroupID) {
                            self.selectedFareGroupIndex = i;
                            break;
                        }
                    }
                }

                $timeout(function () {
                    var segment = angular.element('#flight-segment-list').children('li');
                    _.forEach(segment, function (flightElement) {
                        var height = angular.element(flightElement).outerHeight();
                        self.flightHeightList.push(height);
                    });
                });

            })();
        }]);


    /**
     * Selected Flight Modification Options View Controller
     * Show All Available modification options for the selected flight
     */
    module.controller('ModifySelectedFlightController', [
        'quoteService',
        'modifyReservationService',
        '$state',
        'IBEDataConsole',
        'languageService',
        'currencyService',
        'applicationService',
        'alertService',
        '$rootScope',
        function (quoteService, modifyReservationService, $state, IBEDataConsole, languageService, currencyService, applicationService, alertService,$rootScope) {
            var self = this;
            $rootScope.showConfirmButtons = false;

            this.model = modifyReservationService.getReservation();
            self.selectedSegmentParams = $state.current.data;


            this.onCancelSegmentClick = function () {
                modifyReservationService.doRequotForcancelFlightSegments(
                        function (data) {
                            modifyReservationService.getBalanceSummaryForCancelSegment(
                                    function (data) {
                                        quoteService.changeShowQuote();
                                        quoteService.setShowSummaryDrawer(true);
                                    },
                                    function (error) {
                                        IBEDataConsole.log('CANCEL FLIGHT SEGMENT BALANCE SUMMERY ERROR');
                                    }
                            );
                        },
                        function (error) {
                            IBEDataConsole.log('CANCEL FLIGHT SEGMENT ERROR');
                            alertService.setAlert(error.error, 'warning');
                        }
                );
            };

            this.onModifyFlightClick = function () {
                $state.go('modifyFlightHome.flightSearch',
                        {lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode()}
                );
            };

        }]);


    /**
     * Flight Modification Price Quot Info Drawer Controller
     * view pops out and show price Quot info
     */
    module.controller('ModifyFlightDrawerController', [
        'modifyReservationService',
        'quoteService',
        '$state',
        'languageService',
        'currencyService',
        'applicationService',
        'IBEDataConsole',
        'alertService',
        'commonLocalStorageService',
        'extrasService',
        'ancillaryRemoteService',
        'dateTransformationService',
        '$rootScope',
        '$window',
        function (modifyReservationService, quoteService, $state, languageService, currencyService, applicationService, IBEDataConsole, alertService, commonLocalStorageService,extrasService,ancillaryRemoteService,dateTransformationService,$rootScope,$window){
            var self = this;
            $rootScope.showConfirmButtons = true;
            this.heading = "";
            this.appMode = applicationService.getApplicationMode();
            self.selectedIndexStatus = {};

            var balanceSummeryData = modifyReservationService.getBalanceSummery();
            this.modifyCharges = balanceSummeryData.balanceSummery.modifyCharges;
            this.paxSummary = balanceSummeryData.balanceSummery.paxSummary;
            self.mobileDevice = $rootScope.deviceType.isMobileDevice;

            // Setting up display charges for modify drawer
            var modifyPriceBreakdown = modifyReservationService.modifyPriceBreakdown();

            self.passengerSummary = modifyPriceBreakdown.passengerSummary;
            self.displayCharges = modifyPriceBreakdown.displayCharges;
            self.groupWiseSegment = modifyPriceBreakdown.groupWiseSegment;
            // END Setting up display charges for modify drawer

            var onConfirmClickFn = null;

            function getLatestFlightDepartureDate(reservationData) {
                var flightDateArray = [];

                // Add non-canceled flights in to array and push dates to array
                angular.forEach(reservationData, function (reservation) {
                    if (reservation.status != modifyReservationService.RESERVATION_STATUS.CANCEL) {
                        flightDateArray.push(
                                {
                                    unixTimeStamp: new Date(reservation.departureDateTime.local).getTime(),
                                    date: reservation.departureDateTime.local
                                }
                        );
                    }
                });

                // Find the earliest flight date of a non canceled fight and look up the reservation
                return _.min(flightDateArray, function (date) {
                    return date.unixTimeStamp;
                });
            }

            function confirmWithoutAncillary() {
                if (self.modifyCharges.total > 0) {
                    $state.go('payments',
                            {lang: languageService.getSelectedLanguage(),
                                currency: currencyService.getSelectedCurrency(),
                                mode: applicationService.getApplicationMode()}
                    );
                } else {
                    modifyReservationService.confirmModifyFlightSegments(
                            function (data) {
                                quoteService.changeShowQuote();
                                quoteService.setShowSummaryDrawer(false);

                                var minDate = getLatestFlightDepartureDate(data.reservationSegments);

                                if (minDate.date) {
                                    var dateParm = (minDate.date).slice(0, -9);
                                    modifyReservationService.reloadReservation(dateParm);
                                } else {
                                    modifyReservationService.reloadReservation();
                                }

                            },
                            function (error) {
                                IBEDataConsole.log('ERROR ON CONFIRM MODIFY SEGMENT');
                            }
                    );

                }

            }

            switch (balanceSummeryData.type) {

                case modifyReservationService.BALANCE_SUMMERY_TYPE.CANCEL_FLIGHT :
                {
                    this.heading = "lbl_modification_cancellationSummary";
                    if (modifyReservationService.getSelectedFareGroupIndex() != undefined) {
                        self.selectedIndexStatus[modifyReservationService.getSelectedFareGroupIndex()] = true;
                    }
                    onConfirmClickFn = function () {
                        if (self.modifyCharges.total > 0) {
                            applicationService.setApplicationMode(applicationService.APP_MODES.BALANCE_PAYMENT);
                            $state.go('payments',
                                    {lang: languageService.getSelectedLanguage(),
                                        currency: currencyService.getSelectedCurrency(),
                                        mode: applicationService.getApplicationMode()}
                            );
                        } else {
                            modifyReservationService.confirmCancelFlightSegments(
                                    function (data) {
                                        quoteService.changeShowQuote();
                                        quoteService.setShowSummaryDrawer(false);

                                        var minDate = getLatestFlightDepartureDate(data.reservationSegments);

                                        if (minDate.date) {
                                            var dateParm = (minDate.date).slice(0, -9);
                                            modifyReservationService.reloadReservation(dateParm);
                                        } else {
                                            modifyReservationService.reloadReservation();
                                        }

                                    },
                                    function (error) {
                                        IBEDataConsole.log('CANCEL FLIGHT SEGMENT ERROR');
                                    });
                        }
                    };
                    break;
                }


                case modifyReservationService.BALANCE_SUMMERY_TYPE.MODIFY_FLIGHT :
                {
                    this.heading = "lbl_modify_summaryFlightModification";
                    self.groupWiseSegment = replaceModifiedFlightSegment(self.groupWiseSegment);
                   if(applicationService.getIsPersianCalendar()) {
                	   
                	   for(var groupsegmentList=0;groupsegmentList < self.groupWiseSegment.length; groupsegmentList++) {
                		   
                		   var segmentsList = self.groupWiseSegment[groupsegmentList].segments;
                		   
                		   for(var segments=0;segments< segmentsList.length ; segments++) {
                			   var flight = segmentsList[segments].flight;
                			   var arrivalArray = flight.arrivalDate.split("/");
                			   var departureArray = flight.departureDate.split("/");
                			   var jalaaliObject={};
	                   			if(arrivalArray.length !=1) {
	                				jalaaliObject =dateTransformationService.toJalaali(parseInt(arrivalArray[2]),parseInt(arrivalArray[1]),parseInt(arrivalArray[0]));
	                				flight.arrivalDate =jalaaliObject.jd+"-"+jalaaliObject.jm+"-"+jalaaliObject.jy;
	                			}
								if(departureArray.length !=1) {
									jalaaliObject =dateTransformationService.toJalaali(parseInt(departureArray[2]),parseInt(departureArray[1]),parseInt(departureArray[0]));
	                				flight.departureDate =jalaaliObject.jd+"-"+jalaaliObject.jm+"-"+jalaaliObject.jy;	
								}
                		   }
                	   }
                   }

                    onConfirmClickFn = function(){
                        var params = extrasService.setIniAncillaryReqData();
                        //Check for ancillary availability
                        ancillaryRemoteService.initializeAncillary(params, function (response) {
                            var d = _.where(response.availableAncillariesResponse.availability, {available: true});
                            var availability = (d.length) ? true : false;
                            console.log(availability)
                            commonLocalStorageService.setData("isAncilaryAvailable", availability);



                            IBEDataConsole.log('=====CHANGE Confirmed Flights =============');
                            // Save a copy of the old flight segments and the new one combined
                            modifyReservationService.replaceModifiedSegmentsWithNewSegments();
				var confirmedFlights = commonLocalStorageService.getConfirmedFlightData();
                            modifyReservationService.setConfirmedFlightData([{
                                    fare: modifyReservationService.getSelectedNewFareClass(),
                                    flight: modifyReservationService.getSelectedNewFlight()
                                }]);

				 var selectedGroupSegments1 = modifyReservationService.getSelectedFareGroupSegmentsToModify();
                           // for(var i=0;i< confirmedFlights.length;i++){
                                if(confirmedFlights[0] !== undefined && confirmedFlights[0].flight.originCode === modifyReservationService.getSelectedNewFlight().originCode){
                                    confirmedFlights[0].fare = modifyReservationService.getSelectedNewFareClass();
                                    confirmedFlights[0].flight = modifyReservationService.getSelectedNewFlight();
                                    confirmedFlights[0].changed = selectedGroupSegments1.fareGroupID;
                                }
                                if(confirmedFlights[1] !== undefined &&  confirmedFlights[1].flight.originCode === modifyReservationService.getSelectedNewFlight().originCode){
                                    confirmedFlights[1].fare = modifyReservationService.getSelectedNewFareClass();
                                    confirmedFlights[1].flight = modifyReservationService.getSelectedNewFlight();
                                    confirmedFlights[1].changed = selectedGroupSegments1.fareGroupID;
                                }
                            //}
                            commonLocalStorageService.setConfirmedFlightDataDataForModExtras(confirmedFlights);

                            //self.isAncillaryAvailable = modifyReservationService.getAncillaryStatus();
                            //This need to be implement with an back end configuration
                            self.isAncillaryAvailable = commonLocalStorageService.getData("isAncilaryAvailable");

                            IBEDataConsole.log('=====Ancillary Reprotect Call =============');
                            if (self.isAncillaryAvailable) {
                                modifyReservationService.getDefaultAncillariesOnModifyReservation(
                                        function (data) {
                                            modifyReservationService.setAncillaryModelFromReProtectResponse(data);
                                        },
                                        function (error) {
                                            IBEDataConsole.log('ERROR ON getDefaultAncillariesOnModifyReservation Call');
                                        },
                                        function () {
                                        	$window.localStorage.setItem('MOD_SEG_INIT', true);
                                            $state.go('extras',
                                                    {lang: languageService.getSelectedLanguage(),
                                                        currency: currencyService.getSelectedCurrency(),
                                                        mode: applicationService.getApplicationMode()}
                                            );
                                        }
                                );
                            } else {
                                confirmWithoutAncillary();
                            }
                        }, function () {
                            confirmWithoutAncillary();
                        })

                    };
                    break;
                }

                case modifyReservationService.BALANCE_SUMMERY_TYPE.MODIFY_SEGMENT_WITH_ANCILLARY :
                {
                    this.heading = "lbl_modify_summaryFlightModification";
                    self.groupWiseSegment = replaceModifiedFlightSegment(self.groupWiseSegment);
                    onConfirmClickFn = function () {
                        if (self.modifyCharges.total > 0) {
                            $state.go('payments',
                                    {lang: languageService.getSelectedLanguage(),
                                        currency: currencyService.getSelectedCurrency(),
                                        mode: applicationService.getApplicationMode()}
                            );
                        } else {
                            modifyReservationService.confirmModifyFlightSegments(
                                    function (data) {

                                        var minDate = getLatestFlightDepartureDate(data.reservationSegments);

                                        if (minDate.date) {
                                            var dateParm = (minDate.date).slice(0, -9);
                                            modifyReservationService.reloadReservation(dateParm);
                                        } else {
                                            modifyReservationService.reloadReservation();
                                        }

                                    },
                                    function (error) {
                                        IBEDataConsole.log('ERROR ON CONFIRM MODIFY SEGMENT');
                                    }
                            );
                        }
                    }
                    break;
                }

                case modifyReservationService.BALANCE_SUMMERY_TYPE.MODIFY_ANCILLARY :
                {
                    this.heading = "lbl_modify_flightAncillarySummaryHeading";
                    onConfirmClickFn = function () {
                        if (self.modifyCharges.total > 0) {
                            $state.go('payments',
                                    {lang: languageService.getSelectedLanguage(),
                                        currency: currencyService.getSelectedCurrency(),
                                        mode: applicationService.getApplicationMode()}
                            );
                        } else {
                            modifyReservationService.confirmAncillaryModification(
                                    function (data) {
                                        alertService.setAlert('Services successfully updated', 'success');
                                        commonLocalStorageService.setData('PNR_lastName', data.contactLastName);
                                        commonLocalStorageService.setData('PNR_firstDepartureDate', data.firstDepartureDate);
                                        //modifyReservationService.reloadReservation();
                                        $state.go('confirm', {
                                            lang: languageService.getSelectedLanguage(),
                                            currency: currencyService.getSelectedCurrency(),
                                            mode: applicationService.getApplicationMode()
                                        });
                                    },
                                    function (error) {
                                        IBEDataConsole.log('ERROR ON CONFIRM MODIFY SEGMENT');
                                    }
                            );
                        }
                    }
                    break;
                }
            }

            // Get flight details for modified segment
            function getNewFlightSegment(newFlight) {
                var flightSegments = [];
                var hasStops = false;

                if (!_.isUndefined(newFlight.stops)) {
                    hasStops = !!newFlight.stops.length;
                }

                //Set stopover flights
                if (hasStops) {
                    newFlight.destinationName = newFlight.stops[0].originName;

                    flightSegments.push({flight: newFlight});

                    angular.forEach(newFlight.stops, function (flight) {
                        flightSegments.push({flight: flight});
                    });
                } else {
                    flightSegments.push({flight: newFlight});
                }

                return flightSegments;
            }

            // Replace modified segment in the group wise segments
            function replaceModifiedFlightSegment(groupWiseSegments) {
                var newFlight = modifyReservationService.getSelectedNewFlight();
                var selectedGroupSegments = modifyReservationService.getSelectedFareGroupSegmentsToModify();

                var flightSegments = getNewFlightSegment(newFlight);

                _.forEach(groupWiseSegments, function (segmentGroup) {
                    if (selectedGroupSegments.fareGroupID == segmentGroup.fareGroupID) {
                        segmentGroup.segments = flightSegments;
                    }
                });

                return groupWiseSegments;
            }

            this.onConfirmCancelSegment = function () {
                if (!!onConfirmClickFn) {
                    onConfirmClickFn();
                }
            };

        }]);


    /**
     * Flight Modification Search Flight view Controller
     */
    module.controller('ModifyFlightSearchController', [
        'remoteStorage',
        'datePickerService',
        '$state',
        '$filter',
        'languageService',
        'currencyService',
        'applicationService',
        'modifyReservationService',
        'commonLocalStorageService',
        'alertService',
        'dateTransformationService',
        '$window',
        'constantFactory',
        '$rootScope',
        function (remoteStorage, datePickerService, $state, $filter, languageService, currencyService, applicationService, modifyReservationService, commonLocalStorageService, alertService,dateTransformationService,$window,constantFactory,$rootScope) {

            var self = this;
            $rootScope.showConfirmButtons = false;
            
            self.model = {
                searchCriteria: {
                    from: "",
                    to: "",
                    departureDate: "",
                    cabinClass: ""
                   
                },
                dateformat: datePickerService.setDateFormat(1),
                airports: remoteStorage.getAirports(),
                minDepartureDate: datePickerService.today(),
                AllCabinTypes: remoteStorage.getAllCabinsTypes()
            };
            self.isPersianCalendarType = applicationService.getIsPersianCalendar();
            self.selectedLanguage = languageService.getSelectedLanguage();
            self.minDateForCal="";
            self.depTime="";

            if(self.isPersianCalendarType){
            	var today = moment().toDate();            	
            	var jalaaliObjectForMinDate = dateTransformationService.toJalaali(parseInt(today.getFullYear()),parseInt(today.getMonth())+1,parseInt(today.getDate()));
            	self.minDateForCal= "\'"+jalaaliObjectForMinDate.jd+"/"+jalaaliObjectForMinDate.jm+"/"+jalaaliObjectForMinDate.jy+"\'";
            }
            
            

            // Used to replicate $dirty functionality. could not use $dirty due to
            // calendar same date change set $dirty to true;
            self.initData = {
                depDate: '',
                from: '',
                to: '',
                cabin: ''
            };
            this.selectedLang = languageService.getSelectedLanguage();

            this.onCabinItemSelect = function (cabinClass) {
                self.model.searchCriteria.cabinClass = cabinClass;
            };
            
            this.searchFlight = function (form) {

                if (!form.$valid) {
                    alertService.setAlert("Please complete the form.");
                    return;
                }

                // Check for $dirty.
                if (self.initData.depDate === $filter('date')(self.model.searchCriteria.departureDate, 'yyyy/MM/dd') &&
                        self.initData.from === self.model.searchCriteria.from &&
                        self.initData.to === self.model.searchCriteria.to &&
                        self.initData.cabin === self.model.searchCriteria.cabinClass) {

                    alertService.setAlert("Please change any reservation detail to show available flights.");
                    return;
                }
                if(self.isPersianCalendarType){
	               	var departureArray = self.model.searchCriteria.departureDate.split("/");
	               	var jalaaliObjectForDepDate = dateTransformationService.toGregorian(parseInt(departureArray[2]),parseInt(departureArray[1]),parseInt(departureArray[0]));
	               	self.model.searchCriteria.departureDate = jalaaliObjectForDepDate.gy+"-"+jalaaliObjectForDepDate.gm+"-"+jalaaliObjectForDepDate.gd;
                }
                modifyReservationService.setFlightSearchCriteria(
                        self.model.searchCriteria.from,
                        self.model.searchCriteria.to,
                        self.model.searchCriteria.departureDate,
                        self.model.searchCriteria.cabinClass);

                $state.go('modifyFlightHome.flightSearchResult', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()
                });

            };

            (function init() {
                var fareGroupToBeModified = modifyReservationService.getSelectedFareGroupSegmentsToModify();
                var segmentsToBeModified = fareGroupToBeModified.segments;
                var sortedSegments = $filter('orderBy')(segmentsToBeModified, 'flight.segmentSequence');
                self.model.searchCriteria.carrierCode= constantFactory.AppConfigStringValue('defaultCarrierCode');

                if (!!sortedSegments) {
                    self.model.searchCriteria.from = remoteStorage.getAirportByCode(sortedSegments[0].flight.originCode);
                    self.model.searchCriteria.to = remoteStorage.getAirportByCode(sortedSegments[sortedSegments.length - 1].flight.destinationCode);

                    if(self.isPersianCalendarType){
                    	self.model.searchCriteria.departureDate = $window.localStorage.getItem('depTime');
                    }else{
                    	self.model.searchCriteria.departureDate =  moment(sortedSegments[0].flight.departureDate,'ddd DD MMM YYYY').toDate();
                    }

                    self.model.searchCriteria.cabinClass = remoteStorage.getCabinByKey(sortedSegments[0].flight.cabinClass);

                    self.initData.depDate = self.model.searchCriteria.departureDate;
                    self.initData.from = self.model.searchCriteria.from;
                    self.initData.to = self.model.searchCriteria.to;
                    self.initData.cabin = self.model.searchCriteria.cabinClass;                    
                }

            })();

        }]);

    /**
     * Flight Modification Search result Controller
     */
    module.controller('ModifyFlightSearchResultController', [
        '$state',
        'languageService',
        'currencyService',
        'applicationService',
        'modifyReservationService',
        'ancillaryRemoteService',
        'alertService',
        '$rootScope',
        function ($state, languageService, currencyService, applicationService, modifyReservationService, ancillaryRemoteService, alertService,$rootScope) {
            var self = this;
            $rootScope.showConfirmButtons = false;
            this.model = {
                flightOptions: []
            };

            this.isPopupVisible = false;
            self.noFlightsFound = false;

            self.fareRulesDescriptions = [];

            var doFlightSearch = function () {
                self.noFlightsFound = false;
                self.showBusyLoader = true;
                modifyReservationService.doModificationFlightSearch(
                        function (data) {
                            self.model.flightOptions = data;
                            if (data.length === 0) {
                                self.noFlightsFound = true;
                            }
                            self.showBusyLoader = false;
                        },
                        function (error) {
                            alertService.setAlert(error.error, 'warning');
                            self.showBusyLoader = false;
                        }
                );
            };

            this.onSelectFare = function (selectedFlight, selectedFare) {

                modifyReservationService.setSelectedNewFlight(selectedFlight);
                modifyReservationService.setSelectedNewFareClass(selectedFare);

                $state.go('modifyFlightHome.changedFlight',
                        {lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode()}
                );
            };
            this.onEditFlight = function (id) {
                self.showBusyLoader = true;
                var selectedFareId = modifyReservationService.getSelectedFareIndex();
                modifyReservationService.setSelectedNewFlight(self.model.flightOptions[id]);
                modifyReservationService.doRequotForModifyFlightSegment(
                        function (data, modifiedFlightRph) {
                            // selectedFareId here is the ond sequence
                            // There is a possibility ond sequence changes if order of the journeys change for a modification
                            // Hence modifiedFlightRph is used to filter out the ondInfo of the API
                            var ondInfo = data.originDestinationResponse[selectedFareId];
                            var ondInfoWithModifiedFlight;

                            if (!_.isEmpty(modifiedFlightRph)) {
                                ondInfoWithModifiedFlight = _.find(data.originDestinationResponse, function (item) {
                                    return !_.isEmpty(_.where(item.availableOptions[0].segments,
                                        {flightSegmentRPH: modifiedFlightRph}));
                                });

                                if (!_.isEmpty(ondInfoWithModifiedFlight)) {
                                    ondInfo = ondInfoWithModifiedFlight;
                                }
                            }
                            self.model.flightOptions[id].flightFaresAvailability = [];
                            self.model.flightOptions[id].flightFaresAvailability = (ondInfo != undefined) ?
                                modifyReservationService.adaptFareClasses(data.fareClasses, ondInfo.availableOptions[0].availableFareClasses) : [];
                            self.model.flightOptions[id].isFareListVisible = true;
                            self.showBusyLoader = false;
                            _.each(data.selectedFlightPricing.fareRules, function (val) {
                                if (!!val.comment) {
                                    self.fareRulesDescriptions.push(val.comment)
                                }
                            });
                            applicationService.setFareRules(self.fareRulesDescriptions);

                        },
                        function (error) {
                            alertService.setAlert(error.error, 'danger');
                            self.showBusyLoader = false;
                        },
                        'default'
                        );
            }

            self.searchCriteria = modifyReservationService.getFlightSearchCriteria();

            var lang = languageService.getSelectedLanguage();
            self.originName = self.searchCriteria.origin[lang] || self.searchCriteria.origin['code'];
            self.destinationName = self.searchCriteria.destination[lang] || self.searchCriteria.destination['code'];

            (function init() {
                ancillaryRemoteService.clearExtras();
                doFlightSearch();
            })();

        }]);


    /**
     * Selected New Flight view Controller     *
     */
    module.controller('ModifyFlightChangedFlightController', [
        '$state',
        'languageService',
        'currencyService',
        'applicationService',
        'modifyReservationService',
        'IBEDataConsole',
        'quoteService',
        'alertService',
        'passengerRemoteService',
        '$rootScope',
        function ($state, languageService, currencyService, applicationService, modifyReservationService,
                IBEDataConsole, quoteService, alertService, passengerRemoteService,$rootScope) {
            var self = this;
            $rootScope.showConfirmButtons = true;
            self.searchCriteria = modifyReservationService.getFlightSearchCriteria();var lang = languageService.getSelectedLanguage();
            self.originName = self.searchCriteria.origin[lang] || self.searchCriteria.origin['code'];
            self.destinationName = self.searchCriteria.destination[lang] || self.searchCriteria.destination['code'];
            self.mobileDevice = $rootScope.deviceType.isMobileDevice;

            var getAdaptedSelectedFlight = function (selectedFlight, selectedFare) {
                if (!!selectedFlight) {
                    selectedFlight.selectedFare = selectedFare;
                }
                return selectedFlight;
            }

            this.model = {
                selectedFlight: modifyReservationService.getSelectedNewFlight(),
                selectedFare: modifyReservationService.getSelectedNewFareClass(),
                adaptedSelectedFlight: []
            };

            (function init() {
                self.model.adaptedSelectedFlight.push(getAdaptedSelectedFlight(self.model.selectedFlight, self.model.selectedFare));
            })();

            this.cancelModification = function () {
                $state.go('modifyFlightHome.flightSearchResult',
                        {lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode()}
                );
                ;
            };

            this.onFareChange = function (selectedFlight, selectedFare) {

                getAdaptedSelectedFlight(self.model.selectedFlight, selectedFare);
                modifyReservationService.setSelectedNewFlight(selectedFlight);
                modifyReservationService.setSelectedNewFareClass(selectedFare);
            };

            this.onConfirmModification = function () {
                var paxDetails = modifyReservationService.getReservation().passengerDetails.allPassengers;

                passengerRemoteService.duplicateCheck(paxDetails,
                        function (response) {
                            if (response.hasDuplicates) {
                                var message = "Duplicate name(s) found in the flight segment";
                                alertService.setAlert(message, 'warning');
                            } else {
                                modifyReservationService.doRequotForModifyFlightSegment(
                                        function (data) {
                                            modifyReservationService.getBalanceSummaryForModifySegment(
                                                    function (data) {
                                                        quoteService.changeShowQuote();
                                                        quoteService.setShowSummaryDrawer(true);
                                                    },
                                                    function (error) {
                                                        IBEDataConsole.log('FLIGHT MODIFICATION BALANCE SUMMERY ERROR');
                                                    }
                                            );
                                        },
                                        function (error) {
                                            alertService.setAlert(error.error, 'danger');
                                        },
                                        'requote'
                                        );
                            }
                        },
                        function (data) {
                            if (data.errorType === alertService.ERRORTYPE.TRANSACTION_ID_ERROR) {
                                alertService.setPageAlertMessage(data);
                            } else {
                                alertService.setAlert(data.error, 'danger');
                            }
                        }
                );
            };

        }
    ]);


})(angular);
