/**
 *      Created by Chamil on 19.01.2016
 *      This file defines dashboard module.
 */

(function (angular) {

    // Define dashboard module.
    angular
        .module("modification.dashboard", []);

})(angular); // End of IIFE.
