/**
 *      Created by Chamil on 10.02.2016
 *      This file holds functions for the dashboard controller.
 */

(function () {

    // Define dashboard controller.
    angular
        .module("modification.dashboard")
        .controller("dashboardHeaderCtrl", [
            "$state",
            "$timeout",
            "languageService",
            "currencyService",
            "applicationService",
            "commonLocalStorageService",
            "customerRemoteService",
            "popupService",
            "signInService",
            "eventListeners",
            "constantFactory",            
            "$rootScope",
            headerController]);

    function headerController($state, $timeout, languageService, currencyService, applicationService, commonLocalStorageService, customerRemoteService, popupService, signInService, eventListeners,constantFactory, $rootScope) {

        // Global vars.
        var self = this;

        $rootScope.removePaddingFromXSContainerClass=false;
        // Properties.
        self.isUserLoggedIn = false;
        self.isFullColor;
        self.userProfileVisible = false;
        self.extrasVisible = false;
        self.logoLink = constantFactory.AppConfigStringValue('defaultAirlineURL');
        self.xsLogoSize = 'xs-full-logo';

        // Functions.
        self.updateProfile = updateProfile;
        self.logout = logout;
        self.goToPaymentInfo = goToPaymentInfo
        self.goToDashboard = goToDashboard;
        self.goToMyAccount = goToMyAccount;
        self.goToMyReservation = goToMyReservation;
        self.goToFamilyAndFriends = goToFamilyAndFriends;
        self.goToMyStatement = goToMyStatement;
        self.goToBookAFlight = goToBookAFlight;
        self.goToChangePassword = goToChangePassword;
        self.goToSeatMealPreferences = goToSeatMealPreferences;
        self.carrierCode = applicationService.getCarrerCode();  

        /*
         *  Init function.
         */
        var init = function () {
            // Set dashboard header in specific color.
            self.isFullColor = applicationService.getCurrentState() === "signIn" || applicationService.getCurrentState() === "registration";
            if(!self.isFullColor){
            	self.xsLogoSize = 'xs-half-logo';
            }
            
            if(!self.isFullColor){
            	self.isFullColor = $(window).width() < 768; 	
            }
            
            self.isLoyaltyManagementEnabled = currencyService.getLoyaltyManagementStatus();
            // Get user details from local storage.
            var user = signInService.getUserDataFromLocalStorage();

            // Set visibility for topbar controllers.
            if (!_.isEmpty(user)) {
                self.isUserLoggedIn = true;
                self.name = user.loggedInCustomerDetails.firstName + " " + user.loggedInCustomerDetails.lastName;
            }
            else {
                self.isUserLoggedIn = false;
            }

        }();


        /*
         *  Changed state to update profile page.
         */
        function updateProfile() {

            $state.go('updateProfile', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });

        } // End of updateProfile.


        /*
         *  Logout function.
         */
        function logout() {

            var params = {};

            popupService.confirm("If you continue, data will be lost.Do you want to continue?&lrm;", function (result) {

                signInService.signoutUser(params, function (results) {

                    self.isLogin = false;
                    self.isUserLoggedIn = false;

                    // Change status to sign in.
                    $state.go('signIn', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()}
                    );

                }, function (results) {
                }); // End of signoutUser.

            }); // End of popupService.confirm.

        } // End of logout function.

         /*
         *  Topbar quick link: goToDashboard function.
         * 
         */
        function goToPaymentInfo() {
            $state.go('modifyDashboard', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
            angular.element(document).ready(function () {
                var element = $(".reservations-displayblock");
                $("html, body").animate({
                    scrollTop: element.position().top
                }, 1500);
            });

            closeTopMenus();
        }

 
 
        /*
         *  Topbar quick link: goToMyAccount function.
         *  Change state to update profile page
         */
         function goToMyAccount() {
             updateProfile();
             closeTopMenus();
         }
         
 /*
         *  Topbar quick link: goToDashboard function.
         * 
         */
        function goToDashboard() {
            $state.go('modifyDashboard', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
            angular.element(document).ready(function () {
                var element = $(".dashboard-menu");
                $("html, body").animate({
                    scrollTop: element.position().top
                }, 1500);
            });
            closeTopMenus();
        }
        

        /**
         *  Topbar quick link: go to change password.
         *  Change state to change password
         */
        function goToChangePassword() {
            $state.go('changePassword', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
            closeTopMenus();
        }


        /*
         *  Topbar quick link: goToFamilyAndFriends function.
         *  Change state to view all family members page.
         */
        function goToFamilyAndFriends(){
        	$state.go('viewFamilyFriends', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
             });
        	
        	closeTopMenus();
        }
        
        /*
         *  Topbar quick link: goToSeatMealPreferences function.
         *  Change state to view meal page.
         */
        function goToSeatMealPreferences(){
        	$state.go('viewSeat', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
             });
        	closeTopMenus();
        }
        
         /*
          *  Topbar quick link: goToMyReservation function.
          *  Change state to view all reservation page.
          */
          function goToMyReservation() {
             $state.go('viewReservations', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
             });

             closeTopMenus();
          }

          /*
           *  Topbar quick link: goToMyStatement function.
           */
           function goToMyStatement() {
//               console.log('goToMyStatement');

               closeTopMenus();
           }

           /*
            *  Topbar quick link: goToBookAFlight function.
            *  Call book a flight function.
            */
            function goToBookAFlight() {
//                console.log('goToBookAFlight');
                closeTopMenus();

                // Clear modify search section.
                var searchParams = {
                    from: "",
                    to: "",
                    depDate: "N",
                    retDate: "N",
                    cabin: 'Y', // "Y":"Economy", "C":"Business"
                    promoCode: "",
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency()
                };
                commonLocalStorageService.setSearchCriteria(searchParams);

                eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);


            }

        function closeTopMenus() {
            
            $timeout(function () {
                self.userProfileVisible = false;
                self.extrasVisible = false;
            }, 0);

        }

        eventListeners.registerListeners(eventListeners.events.SHOW_MODIFY_SEARCH, function () {
            self.extrasVisible = false;
        })

    } // End of headerController.

})();
