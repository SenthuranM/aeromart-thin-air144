
(function () {

    angular
        .module("popup", ["ngDialog"])
        .service("popupService", ["ngDialog", popupService]);

    function popupService(ngDialog) {
        var self = this;

        /**
         *  Show alert with default OK button.
         */
        self.alert = function (content, options) {

            var defaults = {
                buttonText: 'OK'
            };

            angular.extend(defaults, options);

            ngDialog.open({
                template: 'modules/common/Popup/templates/alert.tpl.html',
                controller: ["$scope", function ($scope) {
                    $scope.content = content;
                    $scope.buttonText = defaults.buttonText;
                }],
                closeByDocument: false,
                closeByEscape: false,
                showClose: false
            });

        }; // End of alert.

        /**
         *  Show alert with No button.
         */
        self.alertnobtn = function (content, options) {

            angular.extend(options);

            ngDialog.open({
                template: 'modules/common/Popup/templates/alertnobtn.tpl.html',
                controller: ["$scope", function ($scope) {
                    $scope.content = content;
                    //$scope.closeThisDialog();
                }],
                closeByDocument: false,
                closeByEscape: false,
                showClose: false
            });

            setTimeout(function () {
                    ngDialog.close();
                }, 1500);

            


        }; // End of alert.


        /**
         *  Confirm alert box with default OK, Cancel buttons.
         */
        self.confirm = function (content, okFunction, cancelFunction, options) {

            var defaults = {
                okButtonText: 'OK',
                cancelButtonText: 'Cancel'
            };

            angular.extend(defaults, options);

            ngDialog.openConfirm({
                closeByDocument: false,
                closeByEscape: false,
                showClose: false,
                template: 'modules/common/Popup/templates/confirm.tpl.html',
                controller: ["$scope", function ($scope) {
                    $scope.content = content;
                    $scope.okButtonText = defaults.okButtonText;
                    $scope.cancelButtonText = defaults.cancelButtonText;


                }]
            }).then(function (data) {
                // confirm.
                if (typeof okFunction === "function") {  okFunction(data); }
            }, function (data) {
                // cancel.
                if (typeof cancelFunction === "function") {  cancelFunction(data); }
            });

        }; // End of confirm.


        /**
         *  Show popup with content retrieved from html file.
         *  @param cssClass: is used to set width to the popup.
         */
        self.showPopup = function (templateFile, options, okFunction, cancelFunction) {

            var defaults = {
                cssClassName: ""
            };

            angular.extend(defaults, options);

            ngDialog.openConfirm({
                template: 'modules/common/Popup/templates/' + templateFile,
                className: 'ngdialog-theme-default ' + defaults.cssClassName,
                closeByDocument: false,
                closeByEscape: false,
                showClose: true,
                controller: ["$scope", function ($scope) {
                    $scope.seat = options.seat;
                    $scope.passengers = options.passengers;
                    $scope.error = options.error
                }]
            }).then(function data(){
                okFunction();
            }, function(){
                cancelFunction();
            });

        }; // End of alert.
        
        /**
         *  Show popup for pending air rewards with content retrieved from html file.
         *  @param cssClass: is used to set width to the popup.
         */
        self.showPopupPendingAirrewards = function (templateFile, options, okFunction, cancelFunction) {

            var defaults = {
                cssClassName: ""
            };

            angular.extend(defaults, options);

            ngDialog.openConfirm({
                template: 'modules/common/Popup/templates/' + templateFile,
                className: 'ngdialog-theme-default ' + defaults.cssClassName,
                closeByDocument: false,
                closeByEscape: false,
                showClose: false,
                controller: ["$scope", function ($scope) {
                    $scope.seat = options.seat;
                    $scope.passengers = options.passengers;
                    $scope.error = options.error
                }]
            }).then(function data(){
                okFunction();
            }, function(){
                cancelFunction();
            });

        };// End of alert.
        
        /**
         *  Show popup with content retrieved from html file.
         *  @param cssClass: is used to set width to the popup.
         */
        self.showTaxInvoiceSelectionPopup = function (templateFile, options, okFunction, cancelFunction) {

            var defaults = {
                cssClassName: ""
            };
            
            var selectedInvoiceIdsStr = "";

            angular.extend(defaults, options);

            ngDialog.openConfirm({
                template: 'modules/common/Popup/templates/' + templateFile,
                className: 'ngdialog-theme-default ' + defaults.cssClassName,
                closeByDocument: false,
                closeByEscape: false,
                showClose: true,
                controller: ["$window", "$scope", function ($window, $scope) {
                    $scope.taxInvoicesList = options.taxInvoicesList;

                    $scope.records = options.taxInvoicesList;
                    $scope.selected = {};
                    $scope.ShowSelected = function() {
                      $scope.records = $.grep($scope.records, function( record ) {
                        return $scope.selected[ record.taxInvoiceId ];
                      });
                    };   
                    
                    $scope.selectedInvoiceIdsStr = '';
                    $scope.atLeastOneCheckBoxSelected = false;
                    $scope.selectedInvoiceIds = function(){
                    	$scope.selectedInvoiceIdsStr = "";
                    	for (var i=0;i<$scope.records.length;i++){
             
                        	if ($scope.selected[$scope.records[i].taxInvoiceId]){
                        		$scope.selectedInvoiceIdsStr +=  $scope.records[i].taxInvoiceId + ",";
                        		$scope.atLeastOneCheckBoxSelected = true;
                        	}
                        }
                    	
                    	if ($scope.atLeastOneCheckBoxSelected){
                    		selectedInvoiceIdsStr = $scope.selectedInvoiceIdsStr;
                    	} else {
                    		selectedInvoiceIdsStr = '';
                    	}
                    	
                    }
                    
                }]
            }).then(function data(){
            	okFunction(selectedInvoiceIdsStr);
            }, function(){
                cancelFunction();
            });

        }; // End of alert.

        /**
         *  Show popup with content retrieved from html file.
         *  @param cssClass: is used to set width to the popup.
         */
        self.openSaveCardDialog = function (templateFile, options, okFunction, cancelFunction) {
            var defaults = {
                cssClassName: options.className || ''
            };
            var isFormValid = false;
            var selectedInvoiceIdsStr = "";

            angular.extend(defaults, options);
            ngDialog.open({
                template: 'modules/common/Popup/templates/' + templateFile,
                className: 'ngdialog-theme-default ' + defaults.cssClassName,
                closeByDocument: false,
                closeByEscape: false,
                showClose: true,
                controller: ["$window", "$scope", function ($window, $scope) {
                	var self = $scope;
                	self.cardInfo = {};
                	self.numericValidationPattern = /^[1-9]\d*$/;
                	self.isFormValid = false;
                	self.aliasBasePaymentAmount =defaults.aliasBasePaymentAmount;
                	self.baseCurrency = defaults.baseCurrency;
                	self.expDate = {
                        month: [],
                        year: []
                    };
                    var year = parseInt(moment().format("YYYY"), 10);
                    var month = parseInt(moment().format("MM"), 10);
                    for (var i = 0; i < 15; i++) {
                        self.expDate.year[i] = year + i;
                    }
                    for (var i = 0; i < 12; i++) {
                        self.expDate.month[i] = i + 1;
                    }
                    
                  //TODO Once support for other card types, add it
                    self.cardInfo.cardType = 5;

                    self.showBinPromoText = false;
                    self.creditCardValidation = {
                        creditCard: {
                            'usercardNumber': {
                                required: {
                                    name: "ng-required",
                                    value: "true",
                                    message: "lbl_validation_creditCardRequired"
                                },
                                'pattern': {
                                    'message': 'lbl_validation_creditCardInvalid'
                                },
                                minlength: {
                                    name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                                    value: "16",
                                    message: "lbl_validation_creditCardLength"
                                }
                            },
                            'expDateMonth': {
                                required: {
                                    name: "ng-required",
                                    value: "true",
                                    message: "lbl_validation_expireDateRequired"
                                }
                            },
                            'expDateYear': {
                                required: {
                                    name: "ng-required",
                                    value: "true",
                                    message: "lbl_validation_expireDateRequired"
                                }
                            },
                            'usercardCvv': {
                                required: {
                                    name: "ng-required",
                                    value: "true",
                                    message: "lbl_validation_cvvRequired"
                                },
                                'pattern': {
                                    'message': 'lbl_validation_creditCardCvvInvalid'
                                },
                                minlength: {
                                    name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                                    value: "3",
                                    message: "lbl_validation_creditCardCVVLength"
                                }
                            },
                            'cardHolderName': {
                                required: {
                                    name: "ng-required",
                                    value: "true",
                                    message: "lbl_validation_cardHolderNameRequired"
                                }
                            },
                            'cardName': {
                            	required: {
                                    name: "ng-required",
                                    value: "true",
                                    message: "lbl_validation_cardNameRequired"
                                }
                            }
                            
                        }
                    };
                    
                   self.showBinPromoText=false;
                   
                   self.setExpireDate = function() {
                	   var expDate = moment([self.expDateYear, parseInt(self.expDateMonth) - 1, 1]);
                       self.cardInfo.expiryDate = expDate.format("YYYY-MM-DDTHH:mm:ss");
                   };
                   
                   self.savedCardFormSubmit = function (){
                	   self.duplicateCardMessage = self.checkDuplicateSavedCardValidation();
                       if (self.duplicateCardMessage) {
                           setTimeout(function() {
                               self.duplicateCardMessage = false;
                           }, 5000);
                           return false;
                       }
                	   if(self.saveCardForm.$valid == true && self.cardInfo !== undefined) {
                		   okFunction(self.cardInfo);
                		   ngDialog.close();                		   
                	   }
                   }
                   self.checkDuplicateSavedCardValidation = function () {
                       var duplicateCardMessage = false;
                       var savedCreditCardList = JSON.parse(sessionStorage.getItem('saveCardListObj'));                      
                       if (self.expDateMonth && self.expDateYear && self.cardInfo.cardHoldersName && self.cardInfo.cardNo && savedCreditCardList) {
                           angular.forEach(savedCreditCardList, function (value, key) {
                               var dateObj = value.expiryDate.split("-");
                               if (dateObj[1]) {
                                   var expDateYearObj = parseInt(dateObj[0]);
                                   var expDateMonthObj = parseInt(dateObj[1].replace(/^0+/, ''));
                                   var cardHolderNameObj = value.holderName;
                                   var cardNameObj = value.ccName;
                                   var lastFourDigitOfCardObj = value.noLastDigits.slice(-4);
                                   var inputcardNoLastFourDigit = self.cardInfo.cardNo.slice(-4);
                                   if (expDateMonthObj == self.expDateMonth && expDateYearObj == self.expDateYear && cardHolderNameObj == self.cardInfo.cardHoldersName && lastFourDigitOfCardObj == inputcardNoLastFourDigit) {
                                       duplicateCardMessage = true;
                                   } else if (angular.lowercase(cardHolderNameObj)  == angular.lowercase(self.cardInfo.cardHoldersName) && angular.lowercase(cardNameObj) == angular.lowercase(self.cardInfo.cardName)){
                                	   duplicateCardMessage = true;
                                   }
                               }
                           });
                       }
                       return duplicateCardMessage;
                   }
                   
                }]
            });

        }; // End of alert
    } // End of popupService.

})();
