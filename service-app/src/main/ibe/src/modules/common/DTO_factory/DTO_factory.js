/**
 *      Created by Chamil on 26.01.2016
 *      loginbar controller.
 */

(function () {

    angular
        .module('IBE')
        .factory("DTO", [DTOFactory]);

    function DTOFactory() {

        // Forget password recovery request object.
        var passwordRecoveryRequest = {
            "customerID"        : "", // string.
            "secretQuestion"    : "", // string.
            "secretAnswer"      : ""  // string.
        };

        var LMSValidationRequest = {
            emailId         : "",   // string.
            firstName       : "",   // string.
            lastName        : "",   // string.
            headOfEmailId   : null, // string.
            refferedEmailId : null, // string.
            registration    : true  // boolean.
        };

        var LMSRegistrationRequest = {
            "customerID" : "",
            "lmsDetails": {
                "dateOfBirth": "",
                "ffid": "",
                "headOFEmailId": "",
                "language": "",
                "passportNum": "",
                "refferedEmail": "",
                "appCode" : "APP_IBE"
            }
        };


        return {
            passwordRecoveryRequest: passwordRecoveryRequest,
            LMSValidationRequest: LMSValidationRequest,
            LMSRegistrationRequest: LMSRegistrationRequest
        };

    } // End of DTOFactory.

})();
