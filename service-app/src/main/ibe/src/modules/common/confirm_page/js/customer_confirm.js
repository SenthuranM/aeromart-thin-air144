/**
 * Created by nipuna on 4/6/2016.
 */
var customerPageCtrl = function () {
    var controller = {};
    controller.customerConfirmController = [
        "loadingService",
        "alertService",
        "$window",
        "$stateParams",
        "remoteRegistration",
        "$state",
        "languageService",
        "currencyService",
        function (loadingService, alertService, $window, $stateParams, remoteRegistration, $state, languageService, currencyService) {
            var self = this;

            self.customerConfirm = false;
            self.requestComplete = false;
            self.message = "";

            self.startOver = function () {
                $window.location.href = "index.html";
            };

            self.goToLogin = function() {
                $state.go('signIn',{
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency()
                });
            };

            self.goToRegister = function() {
                $state.go('registration');
            };

            function confirmCustomerSuccess(response){
            	if(response.success){
            		if(response.customerConfirmed){
                        //Confirmation success
            			self.customerConfirm = true;
            			self.message = "Customer Confirmation successful!"
            		}else{
            			//Invalid or expired URL
            			self.message = "Confirmation failed: Invalid or expired URL"
            		}
            		self.requestComplete = true;
            	}
            }

            function confirmCustomerError(response){
                self.requestComplete = true;
            }

            remoteRegistration.confirmCutomer(
                {
                    'emailId': $stateParams.emailId,
                    'vlidationText': $stateParams.validationText
                },
                confirmCustomerSuccess,
                confirmCustomerError
            );
        }];
    return controller;
};