/**
 * Created by nipuna on 4/6/2016.
 */
var confirmPageCtrl = function () {
    var controller = {};
    controller.confirmPageController = [
        "loadingService",
        "alertService",
        "$window",
        "$stateParams",
        "remoteRegistration",
        "$state",
        "languageService",
        "currencyService",
        "constantFactory",
        function (loadingService, alertService, $window, $stateParams, remoteRegistration, $state, languageService, currencyService, constantFactory) {
            var self = this;

            var OPERATIONS = {
                'CONFIRM': 'CONFIRM',
                'MERGE': 'MERGE'
            };

            self.lmsConfirmSuccess = false;
            self.requestComplete = false;
            self.message = "";

            self.startOver = function () {
                $window.location.href = constantFactory.AppConfigStringValue('defaultAirlineURL');
            };

            self.goToLogin = function() {
                $state.go('signIn',{
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency()
                });
            };

            self.goToRegister = function() {
                $state.go('registration');
            };

            function confirmLmsSuccess(response){
                if(response.success){
                    if(response.operation === OPERATIONS.CONFIRM){
                        if(response.operationSuccess && response.valid){
                            //Confirmation success
                            self.lmsConfirmSuccess = true;
                            self.message = "LMS Confirmation successful!"
                        }else{
                            //Invalid or expired URL
                            self.message = "LMS Confirmation failed: Invalid or expired URL"
                        }
                    }else if(response.operation === OPERATIONS.MERGE){
                        if(response.operationSuccess && response.valid && response.nameMatch){
                            //merge successful
                            self.lmsConfirmSuccess = true;
                            self.message = "LMS Confirmation successful!"
                        }else{
                            self.message = "LMS Confirmation failed: Invalid or expired URL"

                            if(!response.nameMatch){
                                // merge failed - name unmatched
                                self.message = "LMS Confirmation failed: Name unmatched"
                            }else{
                                // merge failed - Invalid or expired URL
                            }
                        }
                    }else{
                        self.message = "LMS Confirmation failed: Invalid or expired URL"
                    }
                }
                self.requestComplete = true;
            }

            function confirmLmsError(response){
//                console.log("Request failed");
                self.requestComplete = true;
            }

            remoteRegistration.confirmLmsUser(
                {
                    'ffid': $stateParams.ffid,
                    'validationText': $stateParams.validationText
                },
                confirmLmsSuccess,
                confirmLmsError
            );
        }];
    return controller;
};