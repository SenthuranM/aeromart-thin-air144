/**
 *      Created by Chamil on 26.01.2016
 *      loginbar controller.
 */

(function () {

    angular
            .module('loginBar')
            .controller('loginBarCtrl', [
                '$timeout',
                '$state',
                '$filter',
                'applicationService',
                'languageService',
                'currencyService',
                'eventListeners',
                'commonLocalStorageService',
                'customerRemoteService',
                'signInService',
                'navigationService',
                'popupService',
                'userSignInRemoteService',
                'alertService',
                'stepProgressService',
                'constantFactory',
                '$rootScope',
                '$window',
                loginBarController]);

    function loginBarController($timeout, $state, $filter, applicationService, languageService, currencyService, eventListeners, commonLocalStorageService, customerRemoteService, signInService, navigationService, popupService, userSignInRemoteService, alertService,stepProgressService,constantFactory,$rootScope,$window) {

        // Global variables.
        var self = this;

        // Properties.
        self.user;
        self.isUserLoggedIn;
        self.userDisplayName;
        self.lmsPoints;
        self.userProfileVisible = false;
        self.extrasVisible = false;
        self.credits; // Holds available credits.
        self.isDashboard; // Holds whether the current page is dashboard or not.
        self.isEmailConfirmed; // Holds airewards email confirmation status.
        self.isDashboard = $state.params.data && $state.params.data.isDashboard;;
        if($rootScope.deviceType.isMobileDevice){
            self.isMobileHeaderVisible =true;
        }
      

        // Functions.
        self.signout = signout;
        self.backToHome = backToHome;

        self.goToPaymentInfo = goToPaymentInfo
        self.updateProfile = updateProfile;
        self.goToMyAccount = goToMyAccount;
        self.goToMyReservation = goToMyReservation;
        self.goToMyStatement = goToMyStatement;
        self.goToBookAFlight = goToBookAFlight;
        self.resendEmail = resendEmail;
        self.goToChangePassword = goToChangePassword;
        self.carrierCode = applicationService.getCarrerCode();  
        self.goToSeatMealPreferences = goToSeatMealPreferences;
        self.goToFamilyAndFriends = goToFamilyAndFriends;


        /**
         *  Init function.
         */
        (function () {

           
            if($state.current.data){
                console.log($state.current)
                self.isUpdateProfile = $state.current.data.isUpdateProfile;
                self.isChangePassword = $state.current.data.isChangePassword;
                self.isSeatMeatPreferences = $state.current.data.isSeatMeatPreferences;
                self.isFamilyAndFriends = $state.current.data.isFamilyAndFriends;
                self.isMyReservations = $state.current.data.isMyReservations;
                self.isMobileHeaderVisible = $state.current.data.isMobileHeaderVisible;
            }

            // Get user logged in status.
            //self.isUserLoggedIn = signInService.getLoggedInStatus();

            // Set relevant properties according to user login in status.
            var userToken = signInService.getCookie("userToken");
			if (userToken != '') {
				$rootScope.userToken=userToken;
				$rootScope.index=1;
				tokenBasedUserLogin(userToken);
			} else {
				userLogin();
			}
            if (self.isUserLoggedIn) {
                var signInUser = signInService.getUserDataFromLocalStorage();
                var signInUserId = signInUser.loggedInCustomerDetails.emailId
                signInService.isServerHasLoggedInUser({loginId: signInUserId.toLowerCase()}, function (data) {
                    self.isUserLoggedIn = data.customerLoggedIn;
                }, function () {
                    //console.log('error')
                })
            }

            // Called when user login status changed.
            eventListeners.registerListeners(eventListeners.events.CUSTOMER_LOGIN_STATUS_CHANGE, function () {

                $timeout(function () {
                    userLogin();
                }, 0)
            });

            // Set updated airewards points amount.
            eventListeners.registerListeners(eventListeners.events.SET_LMS_DETAILS, function (lmsPoints) {
                self.lmsPoints = lmsPoints;
            });

        })();
    
	    function tokenBasedUserLogin(userToken) {
	    	signInService.tokenBasedSignInUser({
	    		"userToken" : userToken
	    	}, function(response) {
	    		if (response.success) {
	    			console.log('Token based login successful');
	    		}
	    	}, function(error) {
	    		console.log('Token based login error');
	    	});
	    };

        /**
		 * Resend email confirmation for LMS.
		 */
        function resendEmail() {

            customerRemoteService.resendConfirmationEmail({}, function (response) {
                if (response.success) {
                    alertService.setAlert("Confirmation email is successfully resent.", "success");
                }
            }, function (error) {

            });
        } // End of resendEmail.


        /**
         *  Occurs when user login status changed and init function.
         *  DO NOT, cause user login state change in the function, it may cause
         *  Infinite loop.
         */
        function userLogin() {

            self.isUserLoggedIn = signInService.getLoggedInStatus();

            if (self.isUserLoggedIn) {

            	var flagName = signInService.getFlagNameFromCookie("flag");
            	if(flagName !='search' && $rootScope.index>0){
            		if ($rootScope.userToken != '') {
            			$state.go('modifyDashboard', {
            				lang: languageService.getSelectedLanguage(),
            				currency: currencyService.getSelectedCurrency(),
            				mode: applicationService.getApplicationMode()
            			});
            			$window.localStorage.removeItem('PNR_LOGIN');
            		}
            		$rootScope.userToken ='';
            	}
            	$rootScope.index=0;
            	
                self.user = signInService.getUserDataFromLocalStorage();

                if (self.user === undefined || !self.user.success) {
                    self.isUserLoggedIn = false;
                    return;
                }

                // Set email confirmation status.
                self.isEmailConfirmed = self.user.loggedInLmsDetails ? self.user.loggedInLmsDetails.emailConfirmed : false;

                // Set user name and lms points.
                var title = self.user.loggedInCustomerDetails.title ? $filter('salutationFilter')(self.user.loggedInCustomerDetails.title) + " " : "";
                var firstName = self.user.loggedInCustomerDetails.firstName + " ";
                var lastName = self.user.loggedInCustomerDetails.lastName;
                self.userDisplayName = title + firstName + lastName;

                self.lmsPoints = self.user.loggedInLmsDetails ? self.user.loggedInLmsDetails.availablePoints : 0;
                self.credits = self.user.totalCustomerCredit ? self.user.totalCustomerCredit : 0;
            }
        }


        /**
         *  Prompt confirmation from user before sign out.
         */
        function signout() {

            var params = {};

            popupService.confirm("If you continue, data will be lost.Do you want to continue?&lrm;", function (result) {

                signInService.signoutUser(params, function (results) {
                    self.isUserLoggedIn = false;
                    var mode = applicationService.getApplicationMode();
                    if(mode !== 'modify_segment' && mode !== 'changeContactInformation' && mode !== 'nameChange'){
                        if(stepProgressService.getStepByState('fare').status !== 'current'){
                        	  var sp = commonLocalStorageService.getSearchCriteria();
                              var params = {};
                              angular.forEach(sp, function (pram, key) {
                                  params[key] = pram;
                              });
                              var defaultCarrierCode = constantFactory.AppConfigStringValue("defaultCarrierCode");
                              var originCountry = applicationService.getOriginCountry(defaultCarrierCode);
                              params["originCountry"] = originCountry;
                              $state.go('fare',params);
                        	  $timeout(function () {
                                  $state.reload();
                              }, 50);
    						
                        }
                       
                   	}
                }, function (results) {
                }); // End of signoutUser.

            }); // End of popupService.confirm.

        } // End of signout.


        /**
         *  Handles back button functionality.
         */
        function backToHome() {
            if (!self.isUserLoggedIn) {
                return;
            }
            userSignInRemoteService.viewReservations('', function (reservationData) {
                var unsortedReservation = reservationData.reservationDetails;
                commonLocalStorageService.setReservationData(unsortedReservation);
                $state.go('modifyDashboard', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()
                });
            }, function (error) {
                alertService.setPageAlertMessage(error);
            })
            closeTopMenus();
        } 
           /*
         *  Topbar quick link: goToDashboard function.
         * 
         */
        function goToPaymentInfo() {
            $state.go('modifyDashboard', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
            angular.element(document).ready(function () {
                var element = $(".reservations-displayblock");
                $("html, body").animate({
                    scrollTop: element.position().top
                }, 1500);
            });

            closeTopMenus();
        }

        // End of backToHome function.
        /*
         *  Topbar quick link: goToMyAccount function.
         *  Change state to update profile page
         */
        function goToMyAccount() {
            updateProfile();
            closeTopMenus();
        }
        /**
         *  Topbar quick link: go to change password.
         *  Change state to change password
         */
        function goToChangePassword() {
            $state.go('changePassword', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
            closeTopMenus();
        }

        /*
         *  Topbar quick link: goToMyReservation function.
         *  Change state to view all reservation page.
         */
        function goToMyReservation() {
            $state.go('viewReservations', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });

            closeTopMenus();
        }
        
        /*
         *  Topbar quick link: goToFamilyAndFriends function.
         *  Change state to view all family members page.
         */
        function goToFamilyAndFriends(){
        	$state.go('viewFamilyFriends', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
             });
        	
        	closeTopMenus();
        }
        
        /*
         *  Topbar quick link: goToSeatMealPreferences function.
         *  Change state to view meal page.
         */
        function goToSeatMealPreferences(){
        	$state.go('viewSeat', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
             });
        	closeTopMenus();
        }
        

        /*
         *  Topbar quick link: goToMyStatement function.
         */
        function goToMyStatement() {
//               console.log('goToMyStatement');

            closeTopMenus();
        }

        /*
         *  Topbar quick link: goToBookAFlight function.
         *  Call book a flight function.
         */
        function goToBookAFlight() {
//                console.log('goToBookAFlight');
            closeTopMenus();

            // Clear modify search section.
            var searchParams = {
                from: "",
                to: "",
                depDate: "N",
                retDate: "N",
                cabin: 'Y', // "Y":"Economy", "C":"Business"
                promoCode: "",
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency()
            };
            commonLocalStorageService.setSearchCriteria(searchParams);

            eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);


        }

        function closeTopMenus() {

            $timeout(function () {
                self.userProfileVisible = false;
                self.extrasVisible = false;
            }, 0);

        }
        /*
         *  Changed state to update profile page.
         */
        function updateProfile() {

            $state.go('updateProfile', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });

        } // End of updateProfile.

        eventListeners.registerListeners(eventListeners.events.SHOW_MODIFY_SEARCH, function () {
            self.extrasVisible = false;
        })

    } // End of login bar functionality.



})();
