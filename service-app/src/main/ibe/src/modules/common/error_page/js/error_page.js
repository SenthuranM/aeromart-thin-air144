/**
 * Created by baladewa on 29/1/2016.
 */
var errorPageCtrl = function(){
    var controller = {};
    controller.errorPageController = ["loadingService", "alertService", "$window", "applicationService","constantFactory",
        function(loadingService, alertService, $window, applicationService,constantFactory){
        this.errorMessage = alertService.getPageAlertMessage();
        loadingService.hidePageLoading();
        this.startOver = function(){
            $window.location.href = constantFactory.AppConfigStringValue("defaultAirlineURL");
        }
        init();
        function init(){
            if (alertService.getErrorType() === alertService.ERRORTYPE.TRANSACTION_ID_ERROR){
                applicationService.setTransactionID(null);
            }
        }
    }];
    return controller;
};