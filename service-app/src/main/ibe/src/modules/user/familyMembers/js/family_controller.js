angular
		.module('viewFamilyFriends', [])
		.controller(
				'familyController',
				[
				 		"$filter",
						"$http",
						"$stateParams",
						"remoteViewProfile",
						"remoteMaster",
						"$state",
						"$location",
						"alertService",
						"loadingService",
						"languageService",
						"currencyService",
						"applicationService",
						"IBEDataConsole",
						"$timeout",
						"captchaService",
						"$translate",
						"commonLocalStorageService",
						"DTO",
						"popupService",
						"constantFactory",
						"$rootScope",
						"signoutServiceCheck",
                                                "formValidatorService",
						function($filter, $http, $stateParams, remoteViewProfile, remoteMaster, $state, $location,
								alertService, loadingService, languageService,
								currencyService, applicationService,
								IBEDataConsole, $timeout, captchaService,
								$translate, commonLocalStorageService,DTO,popupService,constantFactory,$rootScope,signoutServiceCheck,formValidatorService) {
				 			// To close the loading
							loadingService.hidePageLoading();
							signoutServiceCheck.checkSessionExpiry();
							var self = this;
							this.model = {
									isPopupOpen : false
							};
							
							self.family = {};
							self.isEdit=false;
							self.isvalid=false;
							self.isDobError = false;
							self.family = {
								'title' : "",
								'firstName' : "",
								'lastName' : "",
								'relationshipId' : "",
								'nationalityCode' : "",
								'dateOfBirth' : ""
							};
				            self.nationalityList = constantFactory.Nationalities();	
				            self.paxTypeWiseSalutation = constantFactory.PaxTypeWiseTitle();				            
				            self.adultSalutationList = self.paxTypeWiseSalutation["AD"];
				            self.childSalutationList = self.paxTypeWiseSalutation["CH"];
				            self.infantSalutationList = self.paxTypeWiseSalutation["IN"];
				            self.salutationList = self.adultSalutationList;
				            $rootScope.addMarginForFamilyFriendsPage  = true;
							for(var i=0;i<self.childSalutationList.length;i++){
								var elementFound = false;
								for(var j=0;j<self.salutationList.length;j++){
									if(self.childSalutationList[i].titleCode==self.salutationList[j].titleCode){
										elementFound = true;
										break;
									}
								}
								if(!elementFound) {
									self.salutationList.push(self.childSalutationList[i]);
								}
							}
							for(var i=0;i<self.infantSalutationList.length;i++){
								var elementFound = false;
								for(var j=0;j<self.salutationList.length;j++){
									if(self.infantSalutationList[i].titleCode==self.salutationList[j].titleCode){
										elementFound = true;
										break;
									}
								}
								if(!elementFound) {
									self.salutationList.push(self.infantSalutationList[i]);
								}
							}

							self.display = {};
							self.display = {
								'titleName' : "",
								'nationalityName' : "",
								'relationshipName' : ""
							};
							self.setSalutation = setSalutation;
							
							 // DTO
							self.contactValidation = formValidatorService.getValidations();
							self.backToDashboard = function(){
								$state.go('modifyDashboard', {
									lang: languageService.getSelectedLanguage(),
									currency: currencyService.getSelectedCurrency(),
									mode : applicationService.getApplicationMode()
									});
							};
							
							// To Access DropDown for Relationship
							self.relationshipListData = remoteMaster
							.retrieveRelationshipLists()
							.then(
									function(response) {
										if(response.data.success){
											self.relationshipList = response.data.relationshipList;
											getFamilyData();
										}
									}, function(error) {
										console.log(error);
									});
							
							// To get data of User Profile
							function getFamilyData() {
								self.familyMembersDetails = remoteViewProfile
								.getFamilyMembersDetails()
								.then(
										function(response) {
											mapFamilyMemberDetails(response.data.familyMemberDTOList)
										}, function(error) {
											alertService.setPageAlertMessage(error);
										});
							}
							//Mapping RelationshipId and nationality code to there names
							
							//To clear form fields
							function clearFamilyData(){
								self.family = {
										'title' : "",
										'firstName' : "",
										'lastName' : "",
										'relationshipId' : "",
										'nationalityCode' : "",
										'dateOfBirth' : ""
									};
			
									self.display = {
										'titleName' : "",
										'nationalityName' : "",
										'relationshipName' : ""
									};								
							}
							
							function mapFamilyMemberDetails(data){
								self.familyMemberList = data;
								mapNationalities(data);
								mapRelationships(data);
								mapSalutations(data);
							}
							
							function mapSalutations(data) {
								for(var j=0;j<data.length;j++){
									for(var i=0;i<self.salutationList.length;i++){
										if(self.salutationList[i].titleCode==data[j].title){
											self.familyMemberList[j].title=self.salutationList[i].titleCode;
											break;
										}
									}
								}
							}
							
							function mapNationalities(data) {
								for(var j=0;j<data.length;j++){
									for(var i=0;i<self.nationalityList.length;i++){
										if(self.nationalityList[i].code==data[j].nationalityCode){
											self.familyMemberList[j].nationalityName=self.nationalityList[i].name;
											break;
										}
									}
								}
							}
							
							function mapRelationships(data) {
								for(var j=0;j<data.length;j++){
									for(var i=0;i<self.relationshipList.length;i++){
										if(self.relationshipList[i].id==data[j].relationshipId){
											self.familyMemberList[j].relationshipName=self.relationshipList[i].name;
											break;
										}
									}
								}
							}
							
							// Method to retrieve the membersList from back-end and display in HTML section
							self.setNationality = function(nationality) {
									self.family.nationalityCode = nationality.code;
									self.display.nationalityName = nationality.name;
							}

							self.setRelationship = function(relationship) {
								console.log(relationship);
								self.family.relationshipId = relationship.id;
								self.display.relationshipName = relationship.name;
								//min max dates are inverse of the min max age
								self.dateOptions.maxDate = angular.copy(moment().subtract(relationship.minimumAgeInDays, 'days')).toDate();
								self.dateOptions.minDate = angular.copy(moment().subtract(relationship.maximumAgeInDays, 'days')).toDate();								
							}
							
				            function setSalutation(salutation) {
				            	self.family.title = salutation.titleCode;
				            	self.display.title = salutation.titleCode;
				            	self.display.titleName = salutation.titleName;
				            }
							
							self.startsWith = function(state, viewValue) {
								if (viewValue === null || viewValue.trim() === "" ) {
								    return true;
								} else {
								    var value = viewValue.trim();
								    return state.substr(0, value.length).toLowerCase() == value.toLowerCase();
								}
							}

							// Sending the value after clicking on Add-Member function
							self.familySubmit = function(form) {
								self.isvalid=true;
								if(self.family.dateOfBirth == '' || self.family.dateOfBirth == null || self.family.dateOfBirth == undefined){
									self.isDobError = true;
								}
								if(self.isEdit){
									self.family.familyMemberId=self.familyMemberId;
									self.isDobError = false;
								}
								
								if (form.$valid) {
									if(self.isDuplicateFamilyMember()) {
										alertService.setAlert("msg_common_familyDuplicationError", "danger");
										return;
									}
									self.model.isPopupOpen = false;
									self.isvalid = false;
									self.isDobError = false;
									self.family.dateOfBirth = moment(self.family.dateOfBirth).local().format();
									var familyData = {
										"familyMemberDTOList" : [ self.family ]
									};

							        remoteViewProfile.addFamilyMemberDetails(familyData).then(function(response) {
										if (response.data.success) {
											self.familyMemberList=response.data.familyMemberDTOList;
											mapFamilyMemberDetails(self.familyMemberList);
											clearFamilyData();
											form.$setPristine();
											form.$setUntouched();
											
											if(!self.isEdit){
												alertService.setAlert("msg_common_familySuccess", "success");
											}else{
												alertService.setAlert("msg_common_family_Update_Success", "success");
												self.isEdit=false;
											}
											
										} else {
											alertService.setAlert("msg_common_familyError", "danger");
										}

									}, function(error) {
										alertService.setPageAlertMessage(error);
									});
								} else {
									if($rootScope.deviceType.isMobileDevice){
										this.model.isPopupOpen = true
									}
								}
							};
							self.onDobChange = function() {
									self.isDobError = false;
							}
							// Calendar functions add
							self.today = function() {
								self.dateOfBirth = new Date();
								self.dateOfBirth =$filter('date')(new Date(), 'dd/mm/yy');
							};
							self.today();
							self.clear = function() {
								self.dateOfBirth = null;
							};
							self.inlineOptions = {
								customClass : getDayClass,
								minDate : new Date(),
								showWeeks : true
							};
							
							self.maxDate = angular.copy(moment().subtract(1, 'days')).toDate();
							self.minDate = angular.copy(moment().subtract(100, 'years').add(1, 'days')).toDate();
							self.dateOptions = {
					                changeYear: true,
					                changeMonth: true,
					                minDate : self.minDate,
					                maxDate : self.maxDate,
					                dateFormat: 'dd/mm/yy',
					                weekHeader: "Wk",
					                yearRange: "-100:+0",
					                showOn: "both",
					                buttonImage: "images/calendar.png"
					            };

							// Disable weekend selection
							function disabled(data) {
								var date = data.date, mode = data.mode;
								return mode === 'day'
										&& (date.getDay() === 0 || date
												.getDay() === 6);
							}
							self.toggleMin = function() {
								self.inlineOptions.minDate = self.inlineOptions.minDate ? null
										: new Date();
								self.dateOptions.minDate = self.inlineOptions.minDate;
							};
							self.toggleMin();

							self.open = function() {
								self.popup.opened = true;
								this.model.isPopupOpen = true;
								self.isEdit = false;
								clearFamilyData();
							};
							self.closeFamilyPopup = function(form){
								form.$setPristine();
								this.model.isPopupOpen = false
							}
							
							self.openDeletePopup = function() {
								this.model.isDeletePopup = true
							};
							self.closeDeletePopup = function() {
								this.model.isDeletePopup = false
							}
							

							self.setDate = function(year, month, day) {
								self.dateOfBirth = new Date(year, month, day);
							};
							self.formats = [ 'dd-MMMM-yyyy', 'dd/MM/yyyy',
									'dd.MM.yyyy', 'shortDate' ];
							self.format = self.formats[0];
							self.altInputFormats = [ 'M!/d!/yyyy' ];
							self.popup = {
								opened : false
							};
							var tomorrow = new Date();
							tomorrow.setDate(tomorrow.getDate() + 1);
							var afterTomorrow = new Date();
							afterTomorrow.setDate(tomorrow.getDate() + 1);
							self.events = [ {
								date : tomorrow,
								status : 'full'
							}, {
								date : afterTomorrow,
								status : 'partially'
							} ];

							function getDayClass(data) {
								var date = data.date, mode = data.mode;
								if (mode === 'day') {
									var dayToCheck = new Date(date).setHours(0,
											0, 0, 0);

									for (var i = 0; i < self.events.length; i++) {
										var currentDay = new Date(
												self.events[i].date)
												.setHours(0, 0, 0, 0);

										if (dayToCheck === currentDay) {
											return self.events[i].status;
										}
									}
								}

								return '';
							}// Calendar functions end
							
							// edit Family details
							self.edit=function(familyData){
								self.isEdit=true;
								if($rootScope.deviceType.isMobileDevice){
									this.model.isPopupOpen = true
								}

								self.familyMemberId=familyData.familyMemberId;
								self.family.title=familyData.title;
								self.family.firstName=familyData.firstName;
								self.family.lastName=familyData.lastName;
								self.family.dateOfBirth=familyData.dateOfBirth;
								self.family.relationshipId=familyData.relationshipId;
								self.family.nationalityCode=familyData.nationalityCode;
								self.family.familyMemberId=familyData.familyMemberId;
								self.display.title=familyData.title;
								self.display.firstName=familyData.firstName;
								self.display.lastName=familyData.lastName;
								self.display.relationshipId=familyData.relationshipId;
								self.display.nationalityCode=familyData.nationalityCode;
								self.display.nationalityName=familyData.nationalityName;
								self.display.relationshipName=familyData.relationshipName;
							}

							self.isDuplicateFamilyMember = function () {
								for (var i = 0; i < self.familyMemberList.length; i++) {
									if (self.familyMemberList[i].firstName.toUpperCase() === self.family.firstName.toUpperCase()
										&& self.familyMemberList[i].lastName.toUpperCase() === self.family.lastName.toUpperCase()
										&& self.familyMemberList[i].relationshipId === self.family.relationshipId
										&& moment(self.familyMemberList[i].dateOfBirth).format('YYYYMMDD') === moment(self.family.dateOfBirth).format('YYYYMMDD')
										&& self.familyMemberList[i].familyMemberId !== self.family.familyMemberId && self.familyMemberList[i].title.toUpperCase() === self.family.title.toUpperCase()) {
										return true;
									}
								}
								return false;
							}
							self.deleteMember = function (familyData){
								self.family.firstName=familyData.firstName;
								self.family.lastName=familyData.lastName;
								self.family.familyMemberId=familyData.familyMemberId;
								self.model.isDeletePopup = true;
							}
							self.deleteValue =function(familyData,form){
								self.isEdit=false;
								self.model.isPopupOpen = false;
								self.model.isDeletePopup = false;
								var familyMemberDropdown = remoteViewProfile.deleteFamilyMember(familyData)
								.then(
										function(response) {
											getFamilyData();
											clearFamilyData();
											form.$setPristine();
											self.model.isPopupOpen = false;
											alertService.setAlert("msg_common_familySuccessDelete", "success");
									}, function(error) {
										alertService.setPageAlertMessage(error);
										});
							}
						} 
				 	]);