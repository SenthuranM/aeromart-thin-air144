/**
 *      Created by Dushyantha
 *      05.02.2016
 *      viewReservations controller.
 */
(function (argument) {

    angular
        .module("viewReservations", [])
        .controller("viewReservationsCtrl", ["$state","signInService", "languageService", "currencyService", "IBEDataConsole","alertService","userSignInRemoteService","applicationService",'commonLocalStorageService', "loadingService", "eventListeners","PersianDateFormatService","dateTransformationService","signoutServiceCheck", viewReservationsCtrl]);

    function viewReservationsCtrl($state, signInService, languageService, currencyService, IBEDataConsole,alertService,userSignInRemoteService,applicationService,commonLocalStorageService, loadingService, eventListeners,PersianDateFormatService,dateTransformationService,signoutServiceCheck) {
        var self = this;
        this.reservationAllData ='';
        signoutServiceCheck.checkSessionExpiry();

        self.bookFlight = bookFlight;
        self.bookingDate="";
        self.selectedLanguage=languageService.getSelectedLanguage();
        self.isPersianCalendarType = applicationService.getIsPersianCalendar();
        

        init();
        function init(){
            self.reservationAllData = commonLocalStorageService.getReservationData();
            if(window.matchMedia("only screen and (max-width: 767px)").matches) {
                 for(var reservLv1 = 0;reservLv1 < self.reservationAllData.length;reservLv1++) {
                        for(var reservLv2=0;reservLv2<self.reservationAllData[reservLv1].segmentDetails.length;reservLv2++){
                                self.reservationAllData[reservLv1].segmentDetails[reservLv2]['dayarrivalTime']=moment(self.reservationAllData[reservLv1].segmentDetails[reservLv2].departureDateTime).format('ddd DD MMMM  YYYY hh:mm');
	                    	self.reservationAllData[reservLv1].segmentDetails[reservLv2]['daydepartureTime']= moment(self.reservationAllData[reservLv1].segmentDetails[reservLv2].departureDateTime).format('ddd DD MMMM  YYYY hh:mm');
                        }
                    }
//                    angular.forEach(self.reservationAllData, function(reservationList) {
//                        angular.forEach(reservationList.segmentDetails, function(segmentDetail) {
//                            segmentDetail.departureDateTime = moment(segmentDetail.departureDateTime).format('ddd DD MMM  YYYY hh:mm');
//                            segmentDetail.arrivalDateTime = moment(segmentDetail.arrivalDateTime).format('ddd DD MMM  YYYY hh:mm');                          
//                        });
//                    });
                }
            setPagination();
        }

        this.redirectToReservation = function (pnr, lastName, departureDate) {
            // Format date to YYYY-MM-DD.
            departureDate = moment(departureDate, 'Do MMM YYYY hh:mm').format('YYYY-MM-DD');

            // Redirect to reservation page.
            // modify/reservation/:lang/:currency/:pnr/:lastName/:departureDate'
            $state.go('modifyReservation', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode(),
                pnr: pnr,
                lastName: lastName,
                departureDate: departureDate
            });
        };
        this.backToDashboard  = function(){
            $state.go('modifyDashboard', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
        };
        function setPagination(){
            var itemsPerPage = 5;
            var startingIndex = 1;
            var endIndex = 5;
            var reservationList = self.reservationAllData;
            self.startingIndex  = startingIndex;
            self.endingIndex  = endIndex;
            self.totalRecords = self.reservationAllData.length;
            self.isPrev = false;
            self.isNext = false;
            if(self.reservationAllData.length>itemsPerPage){
                self.isNext =true;
            }
            loadPageData();
            self.prevPage = function () {
                if(self.isPrev && self.startingIndex !=1){
                    startingIndex= self.startingIndex-itemsPerPage;
                    endIndex = startingIndex+(itemsPerPage-1);
                    if(startingIndex=== 1){
                        self.isPrev = false;
                    }
                    else{
                        self.isPrev =true;
                    }
                    self.isNext = true
                    self.startingIndex = startingIndex;
                    self.endingIndex  = endIndex;
                    loadPageData();
                }

            };
            self.nextPage = function () {
                if(self.isNext && self.endingIndex !=self.totalRecords){
                    startingIndex= self.endingIndex+1;
                    endIndex = startingIndex+(itemsPerPage-1);
                    if(endIndex>self.totalRecords){
                        endIndex = self.totalRecords;
                        self.isNext =false;

                    }
                    else{
                        self.isNext = true;
                    }
                    self.isPrev = true;
                    self.startingIndex = startingIndex;
                    self.endingIndex  = endIndex;
                    loadPageData();
                }

            };


        }
        function loadPageData(){
            self.reservationPageData=self.reservationAllData.slice((self.startingIndex-1), (self.endingIndex));
            if(applicationService.getIsPersianCalendar()){
            	
            	for(var reservLv1 = 0;reservLv1 < self.reservationPageData.length;reservLv1++) {
            		
            		var bookingDateWithTime = self.reservationPageData[reservLv1].bookingDate.replace(/:/, '-'); 
            		bookingDateWithTime = bookingDateWithTime.replace(/:/, '-');
            		
            		 var bookArray = bookingDateWithTime.split("-");
            		 
            		 var jalaaliBookingObject = dateTransformationService.toJalaali(parseInt(bookArray[0]),parseInt(bookArray[1]),parseInt(bookArray[2].split("T")[0]));
            		 
            		self.bookingDate = jalaaliBookingObject.jd+"-"+jalaaliBookingObject.jm+"-"+jalaaliBookingObject.jy+" "+bookArray[2].split("T")[1]+":"+bookArray[3];
            		for(var reservLv2=0;reservLv2<self.reservationPageData[reservLv1].segmentDetails.length;reservLv2++){
            			
            			var arrivalStringWithTime = PersianDateFormatService.reformatDateString(self.reservationPageData[reservLv1].segmentDetails[reservLv2].arrivalDateTime,true);
            			var departureStringWithTime = PersianDateFormatService.reformatDateString(self.reservationPageData[reservLv1].segmentDetails[reservLv2].departureDateTime,true);
            			
            			var arrivateString = arrivalStringWithTime.split("-");
            			var departureString = departureStringWithTime.split("-")
            			
                    	var jalaaliArrivalObject = dateTransformationService.toJalaali(parseInt(arrivateString[0]),parseInt(arrivateString[1]),parseInt(arrivateString[2]));
                    	
                    	var jalaaliDepartureObject = dateTransformationService.toJalaali(parseInt(departureString[0]),parseInt(departureString[1]),parseInt(departureString[2]));
                    	
                    	 var arrivalTime = jalaaliArrivalObject.jd+"-"+jalaaliArrivalObject.jm+"-"+jalaaliArrivalObject.jy+" "+arrivateString[3];
                    	
                    	var departureTime = jalaaliDepartureObject.jd+"-"+jalaaliDepartureObject.jm+"-"+jalaaliDepartureObject.jy+" "+departureString[3];
                    	
                    	self.reservationPageData[reservLv1].segmentDetails[reservLv2]['persianArrivalTime']=arrivalTime;
                    	self.reservationPageData[reservLv1].segmentDetails[reservLv2]['persianDepartureTime']=departureTime;
                    	//["persianArrivalTime]=self.arrivalTime;
                    	
            	}
            }
            }
            loadingService.hidePageLoading();
        }

        function bookFlight() {
            self.isModifyVisible = true;

            // Clear modify search section.
            var searchParams = {
                from: "",
                to: "",
                depDate: "N",
                retDate: "N",
                cabin: 'Y', // "Y":"Economy", "C":"Business"
                promoCode: "",
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency()
            };
            commonLocalStorageService.setSearchCriteria(searchParams);

            eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
        }


    }

})();