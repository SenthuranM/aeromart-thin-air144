/**
 *      Created by Chamil on 26.01.2016
 *      signIn service.
 */

(function () {

    angular
        .module("signIn")
        .service("signInService", [
            "userSignInRemoteService",
            "commonLocalStorageService",
            "applicationService",
            "customerRemoteService",
            "eventListeners",'$window',
            signInService]);

    function signInService(userSignInRemoteService, commonLocalStorageService, applicationService, customerRemoteService, eventListeners,$window) {

        var self = this;
        var KEYS = {
            LOGIN_STATUS: "login_status",
            USER_DATA: "user_data",
            MASHREQ_STATUS: "mashreq_status"
        };
        var localStorage = window.localStorage;

        self.getLoggedInStatus = getLoggedInStatus;
        self.getUserDataFromLocalStorage = getUserDataFromLocalStorage;
        self.setUserDataToLocalStorage = setUserDataToLocalStorage;
        self.setUserLoginData = setUserLoginData;
        self.isServerHasLoggedInUser = isServerHasLoggedInUser;
        self.getMashreqStatus = getMashreqStatus;
        self.setMashreqStatus = setMashreqStatus;
        self.getCookie = getCookie;
        self.getFlagNameFromCookie=getFlagNameFromCookie;

        function getMashreqStatus() {
            var mashreqStatus = localStorage.getItem(KEYS.MASHREQ_STATUS);
            if(!_.isNull(mashreqStatus)){
                return mashreqStatus;
            }else{
                return false;
            }
        }

        function setMashreqStatus(status) {
            localStorage.setItem(KEYS.MASHREQ_STATUS, status);
        }

        //var windowsEvent = $window.attachEvent || $window.addEventListener
        //var chkevent = $window.attachEvent ? 'onbeforeunload' : 'beforeunload';

        /**
         * When user close the browser reset the login data
         */

        /*windowsEvent('beforeunload', function(e) {
            // If system need to show a message uncomment following code line;
            //var confirmationMessage = ' ';
            //(e || $window.event).returnValue = "Message";
            //return confirmationMessage;
            localStorage.setItem(KEYS.LOGIN_STATUS,  false);
            getLoggedInStatus();
        })*/

        /**
         *  Check whether front end logged in user's session expired in server.
         */
        function isServerHasLoggedInUser(params, successFn, errorFn) {

            customerRemoteService.isUserLoggedIn(params, function (data) {
                localStorage.setItem(KEYS.LOGIN_STATUS,  data.customerLoggedIn);
                getLoggedInStatus();
                successFn(data);

            }, errorFn);
        }


        /**
         *  Get user login status from local storage.
         *  In this function, if user logged in status is true, we assign user logged in
         *  data from local storage to session storage, since current codebase depends on
         *  session storage.
         */
        function getLoggedInStatus() {
            var loginStatus = localStorage.getItem(KEYS.LOGIN_STATUS) === "true" ? true : false;

            if (loginStatus) {
                setUserLoginData();
            }
            else {
                clearUserDetails();
            }

            return loginStatus;
        }


        /**
         *  Get user data from local storage.
         */
        function getUserDataFromLocalStorage() {
            return JSON.parse(localStorage.getItem(KEYS.USER_DATA));
        }


        /**
         *  Set user data from local storage.
         */
        function setUserDataToLocalStorage(data) {
            eventListeners.notifyListeners(eventListeners.events.CUSTOMER_LOGIN_STATUS_CHANGE);
            localStorage.setItem(KEYS.USER_DATA, JSON.stringify(data));
            commonLocalStorageService.setUserSignInData(data);
        }


        /**
         *  Set user data from local storage to session storage, only if current tab has
         *  no user login, doesn't matter whether local storage has user data or not.
         */
        function setUserLoginData() {

            var userData = getUserDataFromLocalStorage();
            commonLocalStorageService.setUserSignInData(userData);
            commonLocalStorageService.setReservationData(userData.reservationList);
        	
            if (!applicationService.getCustomerLoggedState()) {

                applicationService.setCustomerLoggedState(true);
            }
        }


        /**
         *  Sign in user function. here we set both local storage and session storage.
         *  @param : loginParams, required paramters for login service call.
         *  @param : successFn, service call success function.
         *  @param : errorFn, service call error function.
         */
        self.signInUser = function (loginParams, successFn, errorFn) {

            userSignInRemoteService
                .signInUser(loginParams)
                .then(function (data) {

                    // Stop setting setting user details when user sign in
                    // is failed.
                    if (data.success) {

                        // Since other login function use this service to set
                        // user data to the localstorage, handled customer status
                        // and login response in the service.

                        // Set user logging status.
                        applicationService.setCustomerLoggedState(true);
                        localStorage.setItem(KEYS.LOGIN_STATUS, true);

                        // Add logged in user data object.
                        commonLocalStorageService.setUserSignInData(data);
                        localStorage.setItem(KEYS.USER_DATA, JSON.stringify(data));
                        setUserTokenInCookie(data);
                    }

                    successFn(data);

                }, function (data) {
                    errorFn(data);
                });

        }; // End of signInUser function.

        self.tokenBasedSignInUser = function (loginParams, successFn, errorFn) {

            userSignInRemoteService
                .tokenBasedSignInUser(loginParams)
                .then(function (data) {

                    if (data.success) {
                        applicationService.setCustomerLoggedState(true);
                        localStorage.setItem(KEYS.LOGIN_STATUS, true);
                        commonLocalStorageService.setUserSignInData(data);
                        localStorage.setItem(KEYS.USER_DATA, JSON.stringify(data));
                    }

                    successFn(data);

                }, function (data) {
                    errorFn(data);
                });

        };
        
        function setUserTokenInCookie(data) {
       	    var userToken = data.userToken.token;
            document.cookie = {"userToken" : userToken};
       }
        
        /**
         *  Sign out user function. here we set/clear both local storage and session storage.
         *  @param : params, required paramters for signout service call.
         *  @param : successFn, service call success function.
         *  @param : errorFn, service call error function.
         */
        self.signoutUser = function (params, successFn, errorFn) {

            customerRemoteService.signOut(params, function (data) {

                applicationService.setCustomerLoggedState(false);
                localStorage.setItem(KEYS.LOGIN_STATUS, false);

                // Remove logged in user data object.
                clearUserDetails();

                successFn(data);

            }, errorFn);

        }; // End of signoutUser function.


        // ======================== Private Functions ========================


        /**
         *  Clear user from both local storage and session storage.
         */
        function clearUserDetails() {
            commonLocalStorageService.setUserSignInData({});
            localStorage.setItem(KEYS.USER_DATA, JSON.stringify({}));
            var userToken = getCookie("userToken");
            if (userToken != null && userToken.length > 0) {
            	document.cookie = "userToken=" + "";
            }
        }

    } // End of signInController function.
    

    function getCookie(userToken) {
    	var userTokenCookieName = userToken + "=";
    	var decodedCookies = decodeURIComponent(document.cookie);
    	var splittedCookies = decodedCookies.split(';');
    	for (var i = 0; i < splittedCookies.length; i++) {
    		var cookie = splittedCookies[i];
    		while (cookie.charAt(0) == ' ') {
    			cookie = cookie.substring(1);
    		}
    		if (cookie.indexOf(userTokenCookieName) == 0) {
    			return cookie.substring(userTokenCookieName.length, cookie.length);
    		}
    	}
    	return "";
    }
    
    function getFlagNameFromCookie(flagName) {
    	var flagCookieName = flagName + "=";
    	var decodedCookies = decodeURIComponent(document.cookie);
    	var splittedCookies = decodedCookies.split(';');
    	for (var i = 0; i < splittedCookies.length; i++) {
    		var cookie = splittedCookies[i];
    		while (cookie.charAt(0) == ' ') {
    			cookie = cookie.substring(1);
    		}
    		if (cookie.indexOf(flagCookieName) == 0) {
    			return cookie.substring(flagCookieName.length, cookie.length);
    		}
    	}
    	return "";
    }

})(); // End of IIFE.
