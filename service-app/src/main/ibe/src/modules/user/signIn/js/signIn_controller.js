/**
 *      Created by Chamil on 26.01.2016
 *      signIn controller.
 */

(function (argument) {

    angular
        .module("signIn")
        .controller("signInCtrl", ["$state",
            "signInService",
            "languageService",
            "currencyService",
            "applicationService",
            "loadingService",
            "alertService",
            "passengerService",
            "DTO",
            "popupService",
            "commonLocalStorageService",
            "$timeout",
            "captchaService",
            "constantFactory",
            "remoteMaster",
            "$translate",
            'formValidatorService',
            '$window',
            signInController]);

    function signInController($state, signInService, languageService, currencyService, applicationService,
        loadingService, alertService, passengerService, DTO, popupService,
        commonLocalStorageService, $timeout, captchaService, constantFactory, remoteMaster, $translate, formValidatorService, $window) {

        // Global vars.
        var self = this;


        // DTO.
        self.recoveryReqDTO = DTO.passwordRecoveryRequest;

        // GET FORM VALIDATIONS
        self.signInValidation = formValidatorService.getValidations();

        // Properties.
        self.isLoaderVisible = false; // Set loader initial state.
        self.isForgetPassword = false; // Holds forget password panel visibility.
        self.loginButtonText = "Log in";
        $translate.onReady().then(function () {
            self.loginButtonText = $translate.instant('btn_common_login');
        });

        self.isLoginProcessing = false;


        // Functions.
        self.getForgetPassword = getForgetPassword;
        self.submitSignIn = submitSignIn;
        self.refreshCaptcha = refreshCaptcha;
        self.captchaSrc = captchaService.getNewCaptcha();
        self.isInvalidCaptcha = false;

        /**
         * Init funciton.
         */
        var init = function () {

            // Handles already logged in user.
            self.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode')
            self.isLoyaltyManagementEnabled = currencyService.getLoyaltyManagementStatus();
            if (signInService.getLoggedInStatus()) {

                var userData = commonLocalStorageService.getUserSignInData(),
                    displayName = userData.loggedInCustomerDetails.firstName + " " + userData.loggedInCustomerDetails.lastName,
                    confirmMessage = "You have already logged in as " + displayName + ". What do you want to do?&lrm;"

                popupService.confirm(confirmMessage, function gotoDashboard() {

                    $timeout(function () {
                        // Go to modifyDashboard page.
                        $state.go('modifyDashboard', {
                            lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode()
                        });
                    }, 300)

                }, function loginAsDifferntUser() {

                    // Sign out the current user.
                    signInService.signoutUser({}, function () { }, function () {
                        // calling init function again if something went wrong.
                        init();
                    });

                }, {
                        okButtonText: 'Go To Dashboard',
                        cancelButtonText: 'Login As Different User'
                    }); // End of popupService.confirm.

            } // End of if.

        }(); // End of init function.


        /**
         * Sign user in.
         */
        function submitSignIn(form) {

            if (!form.$valid) {
                return;
            }

            // Set loader visible.
            self.isLoaderVisible = true;
            self.loginButtonText = "Log in...";
            self.loginButtonText = $translate.instant('btn_common_loginProgress');
            self.isLoginProcessing = true;

            signInService.signInUser(self.user, function (data) {

                // Hide loader.
                self.isLoaderVisible = false;
                self.loginButtonText = "Log in";
                self.loginButtonText = $translate.instant('btn_common_login');
                self.isLoginProcessing = false;

                if (!data.success) {
                    if (data.messages && data.messages.length > 0) {
                        alertService.setAlert(data.messages[0], 'warning');
                    }
                    return;
                }

                // Change stage to modifyDashboard page.
                $state.go('modifyDashboard', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()
                });
                $window.localStorage.removeItem('PNR_LOGIN');

            }, function (data) {
                // error
            });

        }; // End of submitSignIn.

        self.showForgetPasswordPanel = function (evt) {
            evt.preventDefault()
            self.isForgetPassword = true;
            remoteMaster.retriveForgetPasswordParams({}).then(function (responese) {
                console.log(responese)
                self.secretQuestionList = responese.data.questionInfo
            })
        }

        function refreshCaptcha() {
            self.captchaSrc = captchaService.getNewCaptcha();
        }

        self.setSecretQuestion = function (question) {
            self.forgetPasswordQuestion = question;
        }


        /**
         *  Get forget password by providing the registered email.
         *  @param emailAddress: recovery email address.
         */
        function getForgetPassword(forgetPasswordForm) {

            if (!forgetPasswordForm.$valid) {
                return;
            }

            self.recoveryReqDTO.customerID = self.forgetPasswordEmail;
            self.recoveryReqDTO.secretQuestion = self.forgetPasswordQuestion;
            self.recoveryReqDTO.secretAnswer = self.forgetPasswordAnswer;

            captchaService.validateCaptcha({ captcha: self.captcha },
                function () {
                    self.isInvalidCaptcha = false;
                    passengerService.getForgetPassword(self.recoveryReqDTO, function (data) {

                        if (!data.success) {
                            alertService.setAlert("Something went wrong, Please try again.", "danger")
                            return;
                        }

                        // Switch view back to default login view.
                        self.isForgetPassword = false;

                        if (data.messages && data.messages.length) {
                            alertService.setAlert(data.messages[0], "success")
                        }

                    }, function (data) {
                        // Request failed.
                    });
                },
                function (response) {
                    self.isInvalidCaptcha = true;
                    self.refreshCaptcha();
                },
                function (response) {

                });

        } // End of getForgetPassword.


        // Show topNav.
        loadingService.hidePageLoading();

    } // End of signInController function.

})(); // End of IIFE.
