var seatController = function() {
	var controller = {};

	controller.seatController = [
			"$http",
			"$q",
			"filterFilter",
			"remoteMaster",
			"$state",
			"$location",
			"loadingService",
			"alertService",
			"languageService",
			"currencyService",
			"applicationService",
			"IBEDataConsole",
			"$timeout",
			"captchaService",
			"$translate",
			"commonLocalStorageService",
			"remoteCustomer",
			"$scope", 
			"popupService",
			"signInService",
			"signoutServiceCheck",
			"$rootScope",
			function($http,$q, filterFilter, remoteMaster, $state,
					$location, loadingService, alertService, languageService,
					currencyService, applicationService, IBEDataConsole,
					$timeout, captchaService, $translate,
					commonLocalStorageService,remoteCustomer,$scope,popupService,signInService,signoutServiceCheck,$rootScope) {

				// to hide page loader
				loadingService.hidePageLoading();
				
				var self = this;

				// Set mealprefference
				self.customerPreferredMealList = []
				self.summaryView= false;
				self.isMobileDevice = $rootScope.deviceType.isMobileDevice;
				
				self.toggleSummary = function(){
					self.summaryView = !self.summaryView; 
				}
				
				signoutServiceCheck.checkSessionExpiry();
				// Getting MealList
				var remoteMealList = remoteMaster.retrieveMealLists().then(
						function(response) {
							self.items = [];
								response.data.mealList.forEach(function (meal) {
									if(meal.mealStatus === 'ACT') {
										meal.quantity=0;
										self.items.push(meal);
									}
								});
								var dataset = self.items;
								self.mealCategory =[];
								angular.forEach(dataset, function (obj, key) {
									if(self.mealCategory.indexOf(obj.mealCategoryCode)===-1){
										self.mealCategory.push(obj.mealCategoryCode)
									}
								})
								paginationControls(self.items);
								remoteCustomer
											.getUserProfile()
											.then(
													function(response) {
														mapMealCode(response.data.customer.cutomerPreferredMealList);
														
												
													}, function(error) {
														console.log(error);
													});
							
						}, function(error) {
							console.log(error);
						});
				
				// Getting seat types
				var seatType = remoteMaster
						.retrieveSeatType()
						.then(
								function(response) {
									self.seatList = response.data.seatPreferencesList;
									var noPreference = {"seatType":"-","description":"No Preference","status":"-"};
									self.seatList.push(noPreference);
								}, function(error) {
									console.log(error);
								});

				// Getting user profile data
				var userProfile = remoteCustomer
						.getUserProfile()
						.then(
								function(response) {
									if (response.data.customer.customerPreferredSeatType) {
										self.seatPreferencesList = response.data.customer.customerPreferredSeatType;
									}
								}, function(error) {
									console.log(error);
								});

				// updating seat preference
				self.updateSeatPreference = function(seat) {
						var preferredSeat = {
								"customerPreferredSeatType" : seat
						};
						var seatType = remoteCustomer.updateSeatPreference(
								preferredSeat).then(function(response) {
							}, function(error) {
								console.log(error);
							});
						alertService.setAlert("Seat preferences updated Successfully.", "success");
				}
				//True if there are meals added.
				self.mealsAdded =function() {
					var elementFound;
					elementFound =_.find(self.items, function(element){						
						return element.quantity > 0;
					});
					return elementFound ? true : false;
				}
				  /**
				 * Get the added meals for a specific section
				 * @parm: sector - The sector selected
				 */
				self.getAddedMeals = function(sector) {
					self.addedMealList = {};

					for (var passenger in sector.passengers) {
					for (var item in sector.passengers[passenger].selectedItems) {
							self.addedMealList[item] = true;
						}

					}
				}
				self.selectedRowIndex=-1
				self.onElementClick = function (index) {
					
					if (self.selectedRowIndex === -1) {
						self.selectedRowIndex = index;
					} else {
						self.selectedRowIndex = -1;
					}
				};
           
				

				self.onIncrement = function(item,isMobileDevice,e){
					if(!isMobileDevice)
						item.quantity++;
					else{
						 var elem = angular.element(e.currentTarget).parent().find('input[type="number"]');
						 var count = Number(elem.val());
		                 elem.val(++count);
					}
				}

				self.onDecrement= function(item,isMobileDevice,e){
					if(!isMobileDevice)
						item.quantity =  --item.quantity > 0 ? item.quantity : 0
					else{
						 var elem = angular.element(e.currentTarget).parent().find('input[type="number"]');
						 var count = Number(elem.val());
		                 count =  --count > 0 ? count : 0;
		                 elem.val(Number(count));
					}
				}
				self.onDoneClick = function(e,item){
				   var elem = angular.element(e.currentTarget).parent().find('input[type="number"]');
                		   var count = Number(elem.val());
              			     item.quantity = count;
                		     self.selectedRowIndex = -1;
					
				}
				// sending the value to back-end to update meal preference
				var preferredMeal = [];
				self.updateMealPreference = function(data) {
					preferredMeal = [];
					var result = _.filter(data, function(value){
						return (value.quantity >0 && value.mealName);
					})
					setDefaultPrefferedList(result).then(function(mealPrefList){
						var mealListPreferred = {
							"customerPreferredMealList" : mealPrefList
						};
						remoteCustomer.updateMealPreference(mealListPreferred).then(function(response) {
							alertService.setAlert("Meal preferences updated Successfully.", "success");
						}, function(error) {
							console.log(error);
							alertService.setAlert(error.data.error, "danger");
						});
					})
				}

	

				// Mapping meal-code and meal-name
				function mapMealCode(data) { 
					for (var j = 0; j < data.length; j++) {
						for (var i = 0; i < self.items.length; i++) {
							if (self.items[i].mealCode == data[j].mealCode) {
								self.items[i].quantity =  data[j].quantity;
								break;
							}
						}
					}
				}

				function setDefaultPrefferedList(data) {
					var deferred = $q.defer();
					self.customerPreferredMealList = [];
						for(var i=0; i < data.length; i++){
							var meal;
							meal = {
								"$$hashKey": data[i].$$hashKey,
								"mealName" : data[i].mealName,
								"mealCode" : data[i].mealCode,
								"quantity" : data[i].quantity
							};
							self.customerPreferredMealList.push(meal);
						}
						deferred.resolve(self.customerPreferredMealList) 
						return deferred.promise;
				}
				// To delete meal from the preferredList
				self.removeMeal = function(index) {
					
					// ACCORDING TO CLIENT THIS POP UP SHOULDN't EXIST

					// var deleteMessage = $translate.instant('msg_delete_meal_confirmation');
					// popupService.confirm(deleteMessage, function () {
					// 	console.log(self.items[index])
						self.items[index].quantity=0;
					// });
				}

				// to perform pagination
				function paginationControls(data) {
					$scope.search = {};
					self.resetFilters = function() {
						$scope.search = {};
					};
					
					// pagination controls
					self.currentPage = 1;
					self.totalItems = data.length;
					self.entryLimit = 10; // items per page
					self.noOfPages = Math.ceil(self.totalItems
							/ self.entryLimit);
					
					// $watch search to update pagination
					$scope.$watch('search', function(newVal, oldVal) {
						self.filtered = filterFilter(data, newVal);
						self.totalItems = self.filtered.length;
						self.noOfPages = Math.ceil(self.totalItems
								/ self.entryLimit);
						self.currentPage = 1;
					}, true);
				}
				
				self.sortBy = function(option){
					var sortedList = []
	                if (option == "mealName") {
	                    sortedList = _.sortBy(self.items, function (obj) {
	                        return obj[option]
	                    });
	                }
	                self.items = sortedList;
				}
			} ];
	return controller;
}