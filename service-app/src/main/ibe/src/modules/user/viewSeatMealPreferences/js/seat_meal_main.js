(function() {
	var meal = new seatController();
	var meals = angular.module("viewSeatMeal", [ 'ui.bootstrap' ]);

	meals.controller(meal);
	
	meals.filter('startFrom', function () {
		return function (input, start) {
			if (input) {
				start = +start;
				return input.slice(start);
			}
			return [];
		};
	});

})();