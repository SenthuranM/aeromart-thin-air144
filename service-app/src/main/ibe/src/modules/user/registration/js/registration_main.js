/**
 *      Created by Chamil on 08/02.2016
 *      Registration module.
 */

 (function () {

     angular
        .module("registration", []);
 })();
