/**
 *      Created by Chamil on 08/02.2016
 *      Registration module.
 */

 (function () {

     angular
        .module("registration")
        .service("registrationService", ["registrationRemoteService", "userProfileRemoteService", "customerRemoteService", regService]);

    function regService(registrationRemoteService, userProfileRemoteService, customerRemoteService) {
        var self = this;

        self.registerUser = function (params, successFn, errorFn) {
            return registrationRemoteService.registerUser(params, successFn, errorFn);
        };

        self.updateUser = function (params, successFn, errorFn) {
            return registrationRemoteService.updateUser(params, successFn, errorFn);
        };

        self.changePassword = function (params, successFn, errorFn) {
            return registrationRemoteService.changePassword(params, successFn, errorFn);
        };

        self.getEmailFromToken = function (token, successFn, errorFn) {
            return registrationRemoteService.getEmailFromToken(token, successFn, errorFn);
        };


        self.getUserProfileData = function (params, successFn, errorFn) {
            return userProfileRemoteService.getUserProfileData(params, successFn, errorFn);
        };

        self.LMSValidation = function (params, successFn, errorFn) {
            return customerRemoteService.validateLMSDetails(params,successFn, errorFn);
        };

        self.mashreqRegister = function (params, successFn, errorFn) {
            return customerRemoteService.mashreqRegister(params,successFn, errorFn);
        };
    }

 })();
