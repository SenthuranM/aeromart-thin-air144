/**
 *      Created by indika on 11/5/15.
 *      Modified by Chamil on 14/01/16.
 */

var ConfirmService = function(){
    var service = {};

    service.confirmService = ["httpServices", "confirmRemoteService",
        function (httpServices, confirmRemoteService) {

        var self = this,
            pnrNumber = null;

        self.getReservationData = function (params, successFn, errorFn) {
            return confirmRemoteService.getReservationDetails(params, successFn, errorFn);
        };

        self.setPNR = function(pnr){
            pnrNumber = pnr;
        };

        self.getPNR = function(){
            return pnrNumber;
        };

        self.print = function (params, successFn, errorFn) {
            return confirmRemoteService.getPrintMarkup(params, successFn, errorFn);
        };

        self.sendEmail = function (params, successFn, errorFn) {
            return confirmRemoteService.sendEmail(params, successFn, errorFn);
        };
        
        self.printTaxInvoice = function (params, successFn, errorFn) {
            return confirmRemoteService.getPrintMarkupForTaxInvoice(params, successFn, errorFn);
        };
        
        self.getTaxInvoiceList = function (params, successFn, errorFn) {
            return confirmRemoteService.getTaxInvoiceList(params, successFn, errorFn);
        };

    }]; // End of confirmService.

    return service;
}
