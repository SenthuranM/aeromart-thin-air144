/**
 *      Created by indika on 11/5/15.
 *      Modified by Chamil on 14/01/16.
 */

(function(){

    var confirmCtrl = new confirmController();
    var onHoldCtrl = new confirmOnHoldController();
    var confirmService = new ConfirmService();

    angular
        .module("confirm", [])
        .controller(confirmCtrl)
        .controller(onHoldCtrl)
        .service(confirmService, [commonServices]);
})();
