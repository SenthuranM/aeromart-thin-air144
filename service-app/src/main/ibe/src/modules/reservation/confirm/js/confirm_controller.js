/**
 *      Created by indika on 11/5/15.
 *      Modified by Chamil on 14/01/16.
 */

var confirmController = function () {
    var controllers = {};

    controllers.confirmController = [
        "$window",
        "$timeout",
        "$injector",
        "stepProgressService",
        "confirmService",
        "constantFactory",
        "commonLocalStorageService",
        "confirmRemoteService",
        "applicationService",
        "loadingService",
        "IBEDataConsole",
        "$sce",
        "alertService",
        "$state",
        "languageService",
        "currencyService",
        "popupService",
        "signInService",
        'GTMService',
        "dateTransformationService",
        "$translate",
        "$rootScope",
        function ($window, $timeout, $injector, stepProgressService, confirmService, constantFactory,
                  commonLocalStorageService, confirmRemoteService, applicationService, loadingService,
                  IBEDataConsole, $sce, alertService, $state, languageService, currencyService, popupService,
                  signInService,GTMService,dateTransformationService,$translate, $rootScope) {
             // For mobile menus
             if(!$rootScope.deviceType.isMobileDevice){
                $rootScope.removePaddingFromXSContainerClass=true;
             }
             
            const DATE_FORMAT = "YYYY-MM-DD";
            stepProgressService.setCurrentStep('confirm');

            loadingService.showPageLoading();

        
            // Global vars.
            var self = this;
                
            //ADDING BOOKING URL
            self.langUrl = languageService.getSelectedLanguage();
            self.bookingUrl = "http://www.booking.com/index.html?aid=364197;label=airarabia-confpage&lang=" + self.langUrl;
            
                
            var promotionPopupWaitTime = 2000; // milliseconds
            self.hasAnciSelected = false; // Used to hide the ancillary heading if none has been selected

            // Properties.
            self.isPopupVisible = false;
            self.hasDataLoadingErrorOccured = false; // Hide whole page content if initial data loading failed.
            self.isUserLoggedIn = applicationService.getCustomerLoggedState();
            self.paymentGatewayCurrency = currencyService.getAppDefaultPGCurrency();
            self.baseCurrency = currencyService.getAppBaseCurrency();
            self.igp = null;
            var segmentMap = [];

            if(commonLocalStorageService.getData('ipgResults')){
                self.igp = commonLocalStorageService.getData('ipgResults');
                commonLocalStorageService.remove('ipgResults');
            }

            // Functions.
            self.print = print;
            self.printTaxInvoice = printTaxInvoice;
            self.getTaxInvoiceList = getTaxInvoiceList;
            self.sendEmail = sendEmail;
            self.finish = finish;
            self.getTrustedUrl = getTrustedUrl;
            self.getFlightDirection = getFlightDirection;

            var confirmReq = {};
            confirmReq.pnr = commonLocalStorageService.getData('PNR');
            confirmReq.lastName = commonLocalStorageService.getData('PNR_lastName');
            confirmReq.departureDateTime =commonLocalStorageService.getData('PNR_firstDepartureDate');
            
            var dialogTranslate = {
                popupblocker: "Please disable pop-up blocker to print reservation",
            };

       
        
            applicationService.setTransactionID(null);
            confirmService.getReservationData(confirmReq, function (data) {

                var intireraryArrival = _.map(data.reservationSegments, function(segmItinerary){
                    return segmItinerary.arrivalDateTime.local.split('T')[0];
                })
            
                loadScripts(intireraryArrival);
            
                // Setting the payment gateway currency
                if(data.paymentDetails && data.paymentDetails.currency){
                    self.paymentGatewayCurrency = data.paymentDetails.currency;
                }

                loadingService.hidePageLoading();

                if (!data.success) {
                    self.hasDataLoadingErrorOccured = true;
                    alertService.setAlert("Something went wrong. Please try again.", "danger");
                    return;
                }
                var popupholiday = constantFactory.AppConfigStringValue("enableHolidayUrl");
                var rentacar = constantFactory.AppConfigStringValue("enableRentACarUrl");

                if( popupholiday && rentacar){
                    // Show promotion popup.
                    $timeout(function () {
                        self.isPopupVisible = true;
                    }, promotionPopupWaitTime);
                }
                

                // Set confirm flight data to null, to stop modifing
                // previous data.
                //commonLocalStorageService.setConfirmedFlightData(null);

                // Booking Details section.
                GTMService.setGoogleTagLayer([{'Revenue':data.balanceSummary.totalPayment},{'ID':data.bookingInfoTO.pnr}]);
                var gtag = GTMService.getGoogleTagLayer()
                var dataLayer = [];
                _.each(gtag,function(item,index){
                    if(_.findIndex(dataLayer,item) == -1) {
                        dataLayer.push(item)
                    }
                })
                console.log(dataLayer)

                var bookingDetails = data.bookingInfoTO;
                self.reservationNumber = bookingDetails.pnr;
                self.reservationDate = moment(bookingDetails.bookingDate).format("DD-MM-YYYY");
                self.policyCode = bookingDetails.policyCode;
                self.groupPnr = bookingDetails.groupPnr;
                self.reservationDate = moment(bookingDetails.bookingDate).format("DD-MM-YYYY");
                
                
                //conversion on georgian to jalaali dates
                  if(applicationService.getIsPersianCalendar()){
                     var reservArray = new Array();
                     reservArray = self.reservationDate.split("-");
                    var jalaaliObject = dateTransformationService.toJalaali(parseInt(reservArray[2]),parseInt(reservArray[1]),parseInt(reservArray[0]));
                    self.reservationDate = jalaaliObject.jd+"-"+jalaaliObject.jm+"-"+jalaaliObject.jy;
                    for(var i=0;i<data.reservationSegments.length;i++){
                       var arrivalTimeArray = data.reservationSegments[i].arrivalDateTime.local;
                       var departureTimeArray = data.reservationSegments[i].departureDateTime.local;
                        arrivalTimeArray = arrivalTimeArray.replace(/:/, '-'); // replaces first ":" character
                        arrivalTimeArray = arrivalTimeArray.replace(/:/, '-'); // replaces second ":" character
                        
                        departureTimeArray = departureTimeArray.replace(/:/, '-'); // replaces first ":" character
                        departureTimeArray = departureTimeArray.replace(/:/, '-'); // replaces second ":" character
                        
                        reservArray = arrivalTimeArray.split("-");
                        
                        var reservArray2 = departureTimeArray.split("-");
                        
                       // var arrivalTemp = new Date(parseInt(reservArray[0])+parseInt(reservArray[1])+parseInt(reservArray[2].split("T")[0])+parseInt());
                        //alert(arrivalTemp.getFullYear());
                         jalaaliObject = dateTransformationService.toJalaali(parseInt(reservArray[0]),parseInt(reservArray[1]),parseInt(reservArray[2].split("T")[0]));
                         
                         var jalaaliObject2 = dateTransformationService.toJalaali(parseInt(reservArray2[0]),parseInt(reservArray2[1]),parseInt(reservArray2[2].split("T")[0]));
                         
                         
                         data.reservationSegments[i].arrivalDateTime.local =jalaaliObject.jd+"-"+jalaaliObject.jm+"-"+jalaaliObject.jy+":"+reservArray[2].split("T")[1]+":"+reservArray[3];
                         
                         data.reservationSegments[i].departureDateTime.local =jalaaliObject2.jd+"-"+jalaaliObject2.jm+"-"+jalaaliObject2.jy+":"+reservArray2[2].split("T")[1]+":"+reservArray2[3];
                         
                         
                         //alert(data.reservationSegments[i].arrivalDateTime.local);
                    }
                  }
                  //alert(data.reservationSegments.length);
                  
                // Itinerary section.
                self.itinerary = data.reservationSegments;

                _.each(self.itinerary,function(item){
                	if (segmentMap.indexOf(item.segmentCode)==-1){
                		segmentMap.push(item.segmentCode);
                	}
                })

                // Passenger Details section.
                var passengers = data.passengers;
                _.forEach(passengers, function(passenger){
                    if (passenger.extras && passenger.extras.length > 0) {
                        self.hasAnciSelected = true;
                    }
                });

                // Other details.
                self.isInfantAvailable = false;
                self.isChildrenAvailable = false;

                // Attach adult object for infant, set children, infant availability.
                for (var paxIndex = 0; paxIndex < passengers.length; paxIndex++) {
                    if (passengers[paxIndex].paxType === "infant") {
                        self.isInfantAvailable = true;
                        passengers[paxIndex].travelWithAdult = getTravelsWithPax(passengers[paxIndex].travelWith, passengers);
                    }
                    if (passengers[paxIndex].paxType === "child") {
                        self.isChildrenAvailable = true;
                    }
                }
                //self.passengers = passengers;
                var segmentPassengers = [];
                _.each(passengers,function(pax){
                    if(pax.extras.length) {
                        var temp = [];
                        _.each(segmentMap,function(s,k){
                            _.each( pax.extras,function(item,key){
                                if(s == item.flightSegmentCode){
                                    temp[k]=item;
                                }
                            })
                        })
                        console.log(temp);
                        pax.extras = angular.copy(temp)
                    }
                    segmentPassengers.push(pax);
                })

                for(var i=0;i<segmentPassengers.length;i++){
                	var tempArray = segmentPassengers[i].extras;
                	for(var j=0;j<segmentPassengers[i].extras.length;j++){
                		if(segmentPassengers[i].extras[j] === undefined){
                			tempArray.splice(j, 1);
                		}
                	}
                	segmentPassengers[i].extras = tempArray;
                	
                }
                self.passengers = segmentPassengers;

                var charges = data.balanceSummary.chargeBreakdown;

                // Total Charges, is sending as the last element in array.
                // using isTotal flag to set 'total' class to LI tag.
                var obj = charges[charges.length - 1];
                obj.isTotal = true;
                charges[charges.length - 1] = obj;
                self.charges = charges;

                // Contact Information section.
                var contactInfo = data.contactInfo;
                self.contactPerson = {
                    title: contactInfo.title,
                    firstName: contactInfo.firstName,
                    lastName: contactInfo.lastName
                };
                self.nationality = contactInfo.nationality;
                self.countryOfResidence = contactInfo.country;
                self.mobilePhone = contactInfo.mobileNumber && contactInfo.mobileNumber.countryCode &&  contactInfo.mobileNumber.areaCode && contactInfo.mobileNumber.number ?
                    contactInfo.mobileNumber.countryCode + contactInfo.mobileNumber.areaCode + contactInfo.mobileNumber.number : '-';
                self.phone = contactInfo.telephoneNumber && contactInfo.telephoneNumber.countryCode &&  contactInfo.telephoneNumber.areaCode && contactInfo.telephoneNumber.number ?
                    contactInfo.telephoneNumber.countryCode + contactInfo.telephoneNumber.areaCode + contactInfo.telephoneNumber.number : '-';
                self.phone = self.phone !== 0 ? self.phone : undefined; // null + null + null = 0
                self.emailAddress = contactInfo.emailAddress !== null ? contactInfo.emailAddress : undefined;
                self.itineraryLanguage = contactInfo.preferredLangauge;

                self.advertisements = data.advertisements;

                self.language = data.language;
                
                self.isTaxInvoiceGenerated = data.taxInvoiceGenerated;
                
                var appMode = applicationService.getApplicationMode();
                
                // send google analytics data
                try {
                	// this should be taken from app paramter
                	var googleAnalyticsID = constantFactory.AppConfigStringValue('googleAnalyticsKeyForServiceApp');
                	
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                    ga('create', googleAnalyticsID, 'auto');
                    ga('require', 'ecommerce');
                    
                    var strPnr = (confirmReq.pnr != undefined && confirmReq.pnr != null) ? confirmReq.pnr : '';
                    var strRoute = '';
                    var strCareerCode = '';
                    var strRevanue = charges[charges.length - 1].amount;
                    var strSelectedCurrency = (charges[charges.length - 1].currencyType == 'BASE') ? self.baseCurrency : self.paymentGatewayCurrency;
                    var baseCurrency = self.baseCurrency;
                    
                    if (commonLocalStorageService.getData('CONFIRMED_FLIGHT')){
                    	var confirmFlight  = JSON.parse(sessionStorage.getItem('IBE.CONFIRMED_FLIGHT'));
                    	var careerCode = (!!confirmFlight && !!confirmFlight[0] && !!confirmFlight[0].flight) ? confirmFlight[0].flight.flightNumber.substring(0, 2) : '';
                        var from = (!!confirmFlight && !!confirmFlight[0] && !!confirmFlight[0].flight) ? confirmFlight[0].flight.destinationName : null;
                        var to = (!!confirmFlight && !!confirmFlight[1] && !!confirmFlight[1].flight) ? confirmFlight[1].flight.destinationName : null;
                        var rout = null;
                        if(!!from){
                            if(!!to) {
                                rout = from + '-' + to
                            }else{
                                rout = confirmFlight[0].flight.originCode + '-' + confirmFlight[0].flight.destinationName
                            }
                        }else{
                            rout = '';
                        }
                        strRoute = rout;
                        strCareerCode = careerCode;
                    }
                    
                    ga('ecommerce:addItem', { 'id': strPnr,'name': strRoute,'sku': strCareerCode,'category': 'Booking','price': strRevanue,'quantity': '1', 'currency': baseCurrency });
            		ga('ecommerce:addTransaction', {'id':strPnr,'affiliation': 'AirArabia','revenue': strRevanue,'shipping': '0','tax': '0.00','currency': baseCurrency});
            		
            		// send transaction data for google analytics in create and balancePayment
            		if (appMode == 'create' || appMode == 'balancePayment'){
            			ga('ecommerce:send');
            		}
            		
                }catch (ex){
                	console.log(ex);
                }

            }, function () {
                IBEDataConsole.log('----------error----------');
            });

            // ADDING HOTEL WIDGET AD
            function loadScripts(intireraryArrival){
                var depDate = moment(intireraryArrival[0]).format(DATE_FORMAT);
                var retDate = moment(intireraryArrival[1]).format(DATE_FORMAT);

                self.flight = commonLocalStorageService.getData('SEARCH_CRITERIA');
                if(self.flight){
                    var lang = languageService.getSelectedLanguage() || 'en';
                    var currency = currencyService.getSelectedCurrency() || 'AED';
                    var flightTo = self.flight.to || '';
                    
                    self.hotelWUrl = getTrustedUrl("http://aff.bstatic.com/static/affiliate_base/js/booking_sp_widget.js?selected_currency=" + currency + "&lang=" + lang + "&checkin="+ depDate +"&checkout="+retDate+ "&dest_type=country&iata=" + flightTo +"&iata_orr=1&iata=AMS");
                    var scriptElement = document.createElement('script');
                    scriptElement.setAttribute("id", 'sp_widget' );
                    scriptElement.setAttribute("data-hash", '_05e0c34845319a');
                    scriptElement.setAttribute("data-container", 'b_container' );
                    scriptElement.setAttribute("data-size", '1220x700' );
                    scriptElement.setAttribute("data-tpncy", 'false' );
                    scriptElement.setAttribute("src", self.hotelWUrl);
                    var widgetContainer = angular.element('#hotelWidget');
                    widgetContainer.append(scriptElement);
                }
            }

            // Get adult user for infant.
            function getTravelsWithPax(paxId, paxList) {
                for (var j = 0; j < paxList.length; j++) {
                    if (paxList[j].paxSequence === paxId && paxList[j].paxType === "adult") {
                        return paxList[j];
                    }
                }
            }

            // Controller print function.
            function print() {

                var params = getServiceParameters();

                confirmService.print(params, function (response) {
                    // Stop execution if success is false.
                    if (!response.success) {
                        IBEDataConsole.log('Error: something went wrong, ', 'confirmService.print function');
                        return;
                    }

                    //toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top="+(screen.height-400)+", left="+(screen.width-840)
                    var printWindow = $window.open("");

                    //If window was not created due to popup blocker, show a warning
                    if(!printWindow){
                        popupService.alert(dialogTranslate.popupblocker);
                    }else{
                        printWindow.document.body.innerHTML = response.itinerary;
                        printWindow.print();
                    }

                }, function (response) {
                    // on error.
                    IBEDataConsole.log(response);
                });
            }
            
            // Controller for print taxInvoice function.
            function printTaxInvoice(selectedInvoiceIdsStr) {
                var params = getServiceParameters();
                params["selectedInvoiceIdsStr"] = selectedInvoiceIdsStr;

                confirmService.printTaxInvoice(params, function (response) {
                    // Stop execution if success is false.
                    if (!response.success) {
                        IBEDataConsole.log('Error: something went wrong, ', 'confirmService.printTaxInvoice function');
                        return;
                    }
                    
                    if (selectedInvoiceIdsStr == ''){
                    	popupService.alert("Please select At least one check box");
                    } else {
                    	//toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top="+(screen.height-400)+", left="+(screen.width-840)
                        var printWindow = $window.open("");

                        //If window was not created due to popup blocker, show a warning
                        if(!printWindow){
                            popupService.alert("Please disable pop-up blocker to view/print taxInvoice");
                        }else{
                            printWindow.document.body.innerHTML = response.taxInvoice;
                            printWindow.print();
                        }
                    }
                    
                }, function (response) {
                    // on error.
                    IBEDataConsole.log(response);
                });
            }
            
            // Controller for getTaxInvoiceList function.
            function getTaxInvoiceList() {
                var params = getServiceParameters();

                confirmService.getTaxInvoiceList(params, function (response) {
                    // Stop execution if success is false.
                    if (!response.success) {
                        IBEDataConsole.log('Error: something went wrong, ', 'confirmService.getTaxInvoiceList function');
                        return;
                    }
                    
                    var popupMsg = "Following are the list of Invoices, Please select invoices to Print.";
                    var taxInvoicesListArray = response.taxInvoicesList;
                    var selectedInvoiceIdsStr = '';
                    popupService.showTaxInvoiceSelectionPopup('tax.invoice.select.tpl.html',{taxInvoicesList:taxInvoicesListArray},printTaxInvoice, function(){});
                        
                }, function (response) {
                    // on error.
                    IBEDataConsole.log(response);
                });
            }

            // Controller sendEmail function.
            function sendEmail() {
                
                var params = getServiceParameters();

                confirmService.sendEmail(params, function (response) {
                    // Stop execution if success is false.
                    if (response.success) {
                       alertService.setAlert("Email sent successfully.&lrm;", "success");     
                    }
                    
                }, function (error) {
                    // on error.
                    alertService.setAlert(error);
                });

            }

            // Handeling finish button press based on login status
            function finish() {
                // Get application mode
                var mode = applicationService.getApplicationMode();

                // Get the url to be redirected to
                var url = constantFactory.AppConfigStringValue("defaultAirlineURL");

                if(mode == 'create'){
                    if(self.isUserLoggedIn){
                        // Change stage to modifyDashboard page.
                        $state.go('modifyDashboard', {
                            lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode()
                        });
                    }else{
                        $window.location.href = url;
                    }
                }else{
                    var modifyReservationService = $injector.get('modifyReservationService');
                    // Get modify search criteria.
                    var modifySearchParams = modifyReservationService.getReservationSearchCriteria();

                    // Change state to modify reservation.
                    $state.go('modifyReservation', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        pnr: modifySearchParams.pnr,
                        lastName: modifySearchParams.lastName,
                        departureDate: confirmReq.departureDateTime.slice(0,10)
                    });
                }
            }

            // Get url for iframe to load advertisements data.
            function getTrustedUrl(url) {
                return $sce.trustAsResourceUrl(url);
            }


            /**
             * Get flight direction.
             */
            function getFlightDirection(toCode, fromCode) {
                return applicationService.getFlightDirection(toCode, fromCode)
            }

            // Return parameters for service call.
            function getServiceParameters() {

                // Get operation type using application services.
                var applicationMode = applicationService.getApplicationMode();
                applicationMode = (typeof applicationMode === "string") ? applicationMode.toUpperCase() : applicationMode;

                return {
                    pnr: self.reservationNumber,
                    groupPnr: self.groupPnr,
                    itineraryLanguage: self.itineraryLanguage ? self.itineraryLanguage : self.language,
                    operationType: applicationMode,
                    viewMode: "loadRes" // Hard coded value.
                };
            }

            (function init(){
                //If user is logged in, LMS points are updated
                self.carrierCode= constantFactory.AppConfigStringValue('defaultCarrierCode');
                if(signInService.getLoggedInStatus()){

                    //Get LMS points and set the appropriate values
                    confirmRemoteService.getLMSPoints(function success(response){
                        var userData = signInService.getUserDataFromLocalStorage();

                        if(!_.isUndefined(userData.loggedInLmsDetails)){
                            userData.loggedInLmsDetails.availablePoints = response.pointBalance;
                            signInService.setUserDataToLocalStorage(userData);
                        }
                    });
                }
                
                  $translate.onReady().then(function() {
                      dialogTranslate.popupblocker = $translate.instant('lbl_confirm_popupblocker');
                  });

            }())
        }];

    return controllers;
};