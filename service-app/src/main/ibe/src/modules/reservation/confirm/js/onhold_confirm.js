/**
 *  Create by baladewa 25/01/2016
 *  Modified by Chamil 18/04/2016
 */

var confirmOnHoldController = function () {

    var controllers = {};

    controllers.confirmOnHoldController = [
        "stepProgressService",
        "commonLocalStorageService",
        "loadingService",
        "languageService",
        "applicationService",
        "constantFactory",
        "currencyService",

        "$window",
        "$state"
                , onHoldController];

    function onHoldController(stepProgressService, commonLocalStorageService, loadingService,
            languageService, applicationService, constantFactory, currencyService, $window, $state) {


        var self = this;
        var confirmRes = commonLocalStorageService.getData('resObject');
        stepProgressService.setCurrentStep('confirm');

        if (confirmRes) {
            self.pnr = confirmRes.pnr;
            self.onHoldStr = confirmRes.onholdReleaseDuration;
        } else {
            self.pnr = $state.params.pnr;
            self.onHoldStr = $state.params.releaseTime;
        }

        self.showIframe = false;
        self.showOfflineMsg = false;

        self.finish = function () {
            if (self.isUserLoggedIn) {
                // Change stage to modifyDashboard page.
                $state.go('modifyDashboard', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()
                });
            }else{
                $window.location.href = constantFactory.AppConfigStringValue("defaultAirlineURL");
            }
        };

        self.isUserLoggedIn = applicationService.getCustomerLoggedState();

        if (confirmRes) {
            if (confirmRes.externalPGDetails !== null) {
                self.showIframe = confirmRes.externalPGDetails.viewPaymentInIframe;
                self.payFortOfflineBillNumber = confirmRes.externalPGDetails.payFortOfflineBillNumber;
            }

            if (self.showIframe) {
                var scriptUrl = confirmRes.externalPGDetails.scriptUrl;
                var requestId = confirmRes.externalPGDetails.requestId;
                var voucherId = confirmRes.externalPGDetails.voucherId;

                self.iframeURL = "misc/voucherDisplay.html?lang=" + languageService.getSelectedLanguage() +
                        "&scripturl=" + scriptUrl + "&requestId=" + requestId + "&voucherId=" + voucherId;

                self.displayMsg = confirmRes.messages[0];
            }
            else
            	{
            	 self.displayMsg = confirmRes.messages[0];
            	}
        }
        
        
        // send google analytics 
        try {
        	// this should be taken from app paramter
        	var googleAnalyticsID = constantFactory.AppConfigStringValue('googleAnalyticsKeyForServiceApp');
        	var quoteData = commonLocalStorageService.getData('QUOTE_DETAILS');
        	
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', googleAnalyticsID, 'auto');
            ga('require', 'ecommerce');
            
            var strPnr = self.pnr;
            var strRoute = '';
            var strCareerCode = '';
            var strRevanue = (!!quoteData) ? quoteData.totalPrice.toString() : '';
            var strSelectedCurrency = (!!quoteData) ? quoteData.selectedCurrency : '';
            
            if (commonLocalStorageService.getData('CONFIRMED_FLIGHT')){
            	var confirmFlight  = JSON.parse(sessionStorage.getItem('IBE.CONFIRMED_FLIGHT'));
            	var careerCode = (!!confirmFlight && !!confirmFlight[0] && !!confirmFlight[0].flight) ? confirmFlight[0].flight.flightNumber.substring(0, 2) : '';
                var from = (!!confirmFlight && !!confirmFlight[0] && !!confirmFlight[0].flight) ? confirmFlight[0].flight.destinationName : null;
                var to = (!!confirmFlight && !!confirmFlight[1] && !!confirmFlight[1].flight) ? confirmFlight[1].flight.destinationName : null;
                var rout = null;
                if(!!from){
                    if(!!to) {
                        rout = from + '-' + to
                    }else{
                        rout = confirmFlight[0].flight.originCode + '-' + confirmFlight[0].flight.destinationName
                    }
                }else{
                    rout = '';
                }
                strRoute = rout;
                strCareerCode = careerCode;
            }
            
//            ga('ecommerce:addItem', { 'id': strPnr,'name': strRoute,'sku': strCareerCode,'category': 'Booking','price': strRevanue,'quantity': '1', 'currency': strSelectedCurrency });
//    		ga('ecommerce:addTransaction', {'id':strPnr,'affiliation': 'AirArabia','revenue': strRevanue,'shipping': '0','tax': '0.00','currency': strSelectedCurrency});
//    		ga('ecommerce:send');
    		
        }catch (ex){
        	console.log(ex);
        }
        

        loadingService.hidePageLoading();
		        
        self.isPopupVisible = (constantFactory.AppConfigStringValue("enableChamWingsOhdPopup") === 'true');
		self.selectedCountryIndex = 0;

		self.contactDetails = [
					{
						country : 'Syria',
						city : 'Damascus',
						officeName : 'Air Arabia',
						officeAddress : 'Fardos Street',
						phone : [ '+963112244086' ],
						email : [ 'info@chamwings.com' ],
						hours : 'From 09:00 till 17:00 (Fri - Off)'
					},
					{
						country : 'Syria',
						city : 'Kamishli',
						officeName : 'Air Arabia',
						officeAddress : 'Hai Al Gharbi- Alraees street',
						phone : [ '+93652426782', '+93652447654' ],
						email : [ 'bmakko@chamwings.com' ],
						hours : 'From 09.00 till 14:00 From 17:00 till 20:00'
					},
					{
						country : 'Syria',
						city : 'Lattakia',
						officeName : 'Air Arabia',
						officeAddress : '8 March street',
						phone : [ '+96341465246' ],
						email : [ 'ltksales@chamwings.com' ],
						hours : 'From 10:00 till 17:00'
					},

					{
						country : 'Iraq',
						city : 'Baghdad',
						officeName : 'Air Arabia',
						officeAddress : 'Saadoon Street',
						phone : [ '+9647707962651', '+9647905143434' ],
						email : [ 'chamwings.bgw@gmail.com',
								'dmbgw@chamwing.com' ],
						hours : 'From 09.00  till 16.00'
					},
					{
						country : 'Iraq',
						city : 'Al Najaf',
						officeName : 'Al Rafidain Travel & Tours',
						officeAddress : 'Saad Muthana street - opposite the Bank of Babel',
						phone : [ '+9647810615819' ],
						email : [ 'njfgsa@chamwings.com' ],
						hours : 'From 09:00 till 16:00 & From 18.00 till 23:00'
					},
					{
						country : 'Lebanon',
						city : 'Beirut',
						officeName : 'Mr. Samir Hareb (Station Manager)',
						officeAddress : 'Beirut Airport Terminal, First Floor',
						phone : [ '+9611628226', '+9613653494' ],
						email : [ 'BEY_station@chamwings.com' ],
						hours : 'Mon To Fri 08:30 till 17:00 Sat till 13.00'
					},
					{
						country : 'Kuwait',
						city : 'Kuwait',
						officeName : 'Malek Travel & Tourism',
						officeAddress : 'Alqublah, Alli Salem Street, Al Bassam Building, next to the Fire Service Directorate',
						phone : [ '+96522444434' ],
						email : [ 'kwi_gsa@chamwings.com' ],
						hours : 'From 08.30 till 13.00 &  From 16.30  till 20.00'
					},
					{
						country : 'Turkey',
						city : 'Istanbul',
						officeName : 'Adonis Travel',
						officeAddress : 'Cumhuriyet caddesi , Adli Han 173/1 , Harbyie , Istanbul',
						phone : [ '+902122960384', '+905075647675' ],
						email : [ 'chamwings@adonis.com' ],
						hours : 'Mon - Fri  9.00 till 18.00- Sat till 15.00 &  Sun Off'
					},
					{
						country : 'Qatar',
						city : 'Doha',
						officeName : 'Jouri Travel & Tours',
						officeAddress : 'Near Sana Signal, El Emadi Building next to Mc Donalds',
						phone : [ '+0097444442644' ],
						email : [ 'dohsales@chamwings.com' ],
						hours : 'From 08:00 Am till 08:00 Pm'
					},
					{
						country : 'United Arab Emirates',
						city : 'Dubai',
						officeName : 'Centurion Travel',
						officeAddress : 'Dubai, Deira, Port saeed, Alaman house office 106',
						phone : [ '+97142942405' ],
						email : [ 'dxbsales@chamwings.com' ],
						hours : 'Sat-Thu 10:00 till 18:00 '
					},
					{
						country : 'Oman',
						city : 'Muscat',
						officeName : 'Blue Sky Travel & Tours',
						officeAddress : 'Muscat - maabella - Seeb',
						phone : [ '+96899330711', '+96822040999' ],
						email : [ 'bluesky.travel99@gmail.com' ],
						hours : 'Sat-Thu 09:00 till 13:00 & 17:00 till 21:00'
					},
					{
						country : 'Soudan',
						city : 'Khartoum',
						officeName : 'SABRA 2 TOURS',
						officeAddress : 'Ste 2, 2ND floor Sudan Air Building, Intersection of MC nimir st & baladia St',
						phone : [ '+249183746859', '+249912377739' ],
						email : [ 'krt_gsa@chamwings.com' ],
						hours : 'Sun - Thu 09:00 till 16:30'
					},
					{
						country : 'Iran',
						city : 'Tehran',
						officeName : 'Mahd alburaq',
						officeAddress : 'unit 2, 1st  floor, No 94, At the corner of Jahanmehr st., Fathi Shaghaghi st., seyed jamaledin Ave., Tehran',
						phone : [ '+00982188007070' ],
						email : [ 'tehranalburaq@yahoo.com' ],
						hours : '08:30 a.m – 18:00 p.m'
					} ];

		self.countryChanged = function() {
			self.selectedCountry = self.contactDetails[self.selectedCountryIndex];
		};

		(function init() {
			self.selectedCountry = self.contactDetails[self.selectedCountryIndex];

		}())
    } 
    // End of onHoldController.

    return controllers;
}
