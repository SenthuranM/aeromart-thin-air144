/**
 * Created by nipuna on 12/22/16.
 */
var multiCityFareController = function () {
    var controllers = {};

    controllers.multiCityFareController = [
        'multiCityService', 'loadingService',
        function (multiCityService, loadingService) {
            var self = this;

            self.showPriceBreakdown = false;
            self.fareClasses = [];
            self.originDestinationResponse = {};
            self.selectedFlightPricing = {};

            self.selectFareClass = function(ond, fareClass){
                // Set the appropriate fare class
                ond.selected = fareClass;

                //TODO: Set all other fare classes to false
                fareClass.selected = true;

                updateCharge();
            };

            self.onContinueToPassenger = function () {
                multiCityService.navigateToPassenger(self.originDestinationResponse);
            };

            function updateCharge(){
                var allFaresNotSelected = false;
                _.forEach(self.originDestinationResponse, function(ond){
                    if(!ond.selected){
                        allFaresNotSelected = true;
                    }
                });

                // If fareClasses selected for all ONDs, send request
                if(!allFaresNotSelected){
                    self.showPriceBreakdown = true;
                    multiCityService.getFareQuote(self.originDestinationResponse,
                        function(success){
                            self.selectedFlightPricing = success.selectedFlightPricing;
                            self.displayCharges = success.displayCharges;
                        }, function(error){

                        }
                    )
                }
            }

            (function(){
                loadingService.showPageLoading();
                multiCityService.getFareResults(
                    function success(response){
                        self.fareClasses = response.fareClasses;
                        self.originDestinationResponse = response.originDestinationResponse;

                        // Set initial selected fares.
                        _.forEach(self.originDestinationResponse, function(ond){
                            _.forEach(ond.availableOptions[0].availableFareClasses, function(fare){
                                if(fare.selected){ self.selectFareClass(ond, fare); }
                            });
                        });
                        loadingService.hidePageLoading();
                    }
                )
            })();
        }];

    return controllers;
};
