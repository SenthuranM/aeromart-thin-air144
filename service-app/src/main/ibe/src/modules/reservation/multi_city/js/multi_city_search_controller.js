var multiCitySearchController = function () {
    var controllers = {};

    controllers.multiCitySearchController = [
        'remoteStorage',
        'currencyService',
        'languageService',
        'datePickerService',
        'multiCityService',
        'multiCityFactory',
        'constantFactory',
        'loadingService',
        function (remoteStorage, currencyService, languageService, datePickerService, multiCityService,
                  multiCityFactory, constantFactory, loadingService) {
            var self = this;

            self.airports = remoteStorage.getAirports();
            self.toAirports = remoteStorage.getToAirports();
            self.currencyList = currencyService.getCurrencyList();
            self.selectedLanguage = languageService.getSelectedLanguage();

            self.dateformat = datePickerService.setDateFormat(4);
            self.searchList = multiCityService.getSearchList();
            self.minDepartureDate = new Date();

            //Set initial values for paxCount
            self.passengerDetails = {
                adult: {
                    labelSingle: "Adult",
                    labelMulti: "Adults",
                    value: 1,
                    max: 9,
                    min: 1,
                    ageText: "(over 12 years)"
                },
                child: {
                    labelSingle: "Child",
                    labelMulti: "Children",
                    value: 0,
                    max: 8,
                    min: 0,
                    ageText: "(2-12 years)"
                },
                infant: {
                    labelSingle: "Infant",
                    labelMulti: "Infants",
                    value: 0,
                    max: 9,
                    min: 0,
                    ageText: "(under 2 years)"
                }
            };

            self.searchCriteria = {
                "adult": 1,
                "child": 0,
                "infant": 0,
                "cabin": "Y",
                "promoCode": ""
            };

            self.addSearchSegment = function() {
                self.searchList.push(new multiCityFactory.JourneyInfoSegment());
            };

            self.removeSearchSegment = function(index) {
                self.searchList.splice(index, 1);
            };

            self.datePickerOnClick = function(flight) {
                flight.isDatePickerOpen = !flight.isDatePickerOpen;
            };

            self.setCurrency = function(selectedCurrency){
                var languageSpecificCurrency = selectedCurrency[self.selectedLanguage] ? selectedCurrency[self.selectedLanguage] : selectedCurrency.en;
                self.selectedCurrency = languageSpecificCurrency;
            };

            self.searchFlight = function() {
                self.searchCriteria.adult = self.passengerDetails.adult.value;
                self.searchCriteria.child = self.passengerDetails.child.value;
                self.searchCriteria.infant = self.passengerDetails.infant.value;
                self.searchCriteria.journeyInfo = self.searchList;
                multiCityService.navigateToSearchFlight(self.searchCriteria);
            };

            (function(){
                self.setCurrency(currencyService.getSelectedCurrencyObject(currencyService.getSelectedCurrency()));
                self.isPromoEnabled= JSON.parse(constantFactory.AppConfigStringValue('enablePromoCode'));

                // Hide page loading when coming back
                loadingService.hidePageLoading();
            })();
        }];

    return controllers;
};
