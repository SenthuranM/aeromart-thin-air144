var multiCityFlightController = function () {
    var controllers = {};

    controllers.multiCityFlightController = [
        'multiCityService', 'multiCityRemoteService', 'loadingService',
        function (multiCityService, multiCityRemoteService, loadingService) {
            var self = this;
            self.flightDataModel = {};

            (function(){
                loadingService.showPageLoading();
                multiCityService.searchFlights(
                    function success(response){
                        self.ondResponse =  response.originDestinationResponse;
                        
                        _.forEach(response.originDestinationResponse, function(ond){
                            self.flightDataModel[ond.ondSequence] = null;
                        });

                        loadingService.hidePageLoading();
                    }
                )
            })();

            self.allFlightsSelected = function(){
                var allFlightSelected = true;
                _.forEach(self.flightDataModel, function(ond){
                    allFlightSelected = _.isNull(ond) ? false : allFlightSelected;
                });

                return allFlightSelected;
            };

            self.flightSelected = function(flight, ond){
                _.map(ond.availableOptions, function(option){
                    option.selected = false;
                });

                flight.selected = true;
                self.flightDataModel[ond.ondSequence] = flight.segments;
            };

            self.getPriceQuote = function(){
                multiCityService.navigateToSelectFare(self.flightDataModel);
            };
        }];

    return controllers;
};
