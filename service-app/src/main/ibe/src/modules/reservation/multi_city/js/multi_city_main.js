/**
 * Created by Nipuna on 22/12/16.
 */
(function(){
    var mcFactory = new multiCityFactory();
    var mcService = new multiCityService();
    var mcSearchController = new multiCitySearchController();
    var mcFlightController = new multiCityFlightController();
    var mcFareController = new multiCityFareController();

    var multiCity = angular.module("multiCity", []);

    multiCity.controller(mcSearchController)
        .factory(mcFactory)
        .service(mcService, [commonServices])
        .controller(mcFlightController)
        .controller(mcFareController);
})();