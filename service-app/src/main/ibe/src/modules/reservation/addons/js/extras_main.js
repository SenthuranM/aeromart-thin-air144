/**
 * Created by indika on 11/5/15.
 */
(function() {
    var extc = new extrasController();
    var baggageControlle = new baggageController();
    var mealCtrl = new mealController();
    var automaticCheckinCtrl = new automaticCheckinController();
    var extService = new extrasService();
    var seatmap = new seatmapController();
    var inFlightServices = new inFlightServicesController();
    var airportServices = new extrasAirportServicesController();
    var airportTransfer = new extrasAirportTransferController();
    var insurance = new insuranceController();
    var ancyServices = new ancillaryService();
    var ancyFactory = new ancillaryFactory();
    var flexiCtrl =  new flexiController();
    var mealService = new extrasMealService();
    var extras = angular.module("extras", []);

    extras.controller(extc);
    extras.controller(baggageControlle);
    extras.controller(mealCtrl);
    extras.controller(automaticCheckinCtrl);
    extras.controller(seatmap);
    extras.controller(inFlightServices);
    extras.controller(insurance);
    extras.controller(airportServices);
    extras.controller(airportTransfer);
    extras.controller(flexiCtrl);

    extras.service(mealService, [commonServices]);
    extras.service(ancyFactory, [commonServices]);
    extras.service(ancyServices, [commonServices]);
    

    extras.service(extService, [commonServices]);
    extras.factory('extrasFactory', ["$http", "travelFareService", "commonLocalStorageService","httpServices","passengerService","ancillaryRemoteService",
    	"$injector","IBEDataConsole",
        function($http, travelFareService, commonLocalStorageService, httpServices,passengerService,ancillaryRemoteService,$injector,IBEDataConsole) {

        var extrasFactory = {};

        var getSeatMap = function(seatMapResponse,requestedSegmentRPH){
            var seatMap = null;

            if(!!seatMapResponse){
                for (var i = 0; i < seatMapResponse.length; i++) {
                    var seatMapObj = seatMapResponse[i];
                    if(requestedSegmentRPH == seatMapObj.scope.flightSegmentRPH ){
                        var type = seatMapObj.scope.scopeType;
                        switch(type){
                            case  'SEGMENT' : {
                                seatMap = seatMapObj.items;
                                break;
                            };
                        }
                        break;
                    }
                }
            }
            return seatMap;
        };

        extrasFactory.getPassenger = function() {
            var pas = angular.extend({}, passenger)
            return pas;
        };

        return extrasFactory;

    }])

})();
