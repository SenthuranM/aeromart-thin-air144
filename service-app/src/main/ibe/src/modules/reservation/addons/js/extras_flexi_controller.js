/**
 * Created by indika on 2/17/16.
 */
var flexiController = function () {
    var controllers = {};
    controllers.flexiController = [
        "$location",
        "passengerService",
        "extrasFactory",
        "extrasService",
        "$rootScope",
        "ancillaryRemoteService",
        "loadingService",
        "IBEDataConsole",
        "ancillaryService",
        "alertService",
        "quoteService",
        "languageService",
        "currencyService",
        "applicationService",
        "$state",
        "modifyReservationService",
        "commonLocalStorageService",
        "popupService",
        "navigationService",
        "$translate",
        function ($location, passengerService, extrasFactory, extrasService, $rootScope, ancillaryRemoteService,
                  loadingService, IBEDataConsole, ancillaryService, alertService, quoteService, languageService,
                  currencyService, applicationService, $state, modifyReservationService, commonLocalStorageService,
                  popupService, navigationService, $translate) {

            var _this = this, sectorCount = 0, appState, urlParams, currentPnr;           
            var flexi = {flexiId: "FL",price: 100,selected:false};
            var ancillaryType = "FLEXIBILITY";
            var isSelectable = [];
            _this.accordionArray = [];
            _this.isSelected = [];
            _this.currentOndId = sectorCount;
            _this.currentFlexiCharge = 0;
            _this.searchCriteria = commonLocalStorageService.getSearchCriteria();
            _this.currency = currencyService.getSelectedCurrency();

            (function () {
                var param = "FLEXIBILITY";
                _this.appMode = applicationService.getApplicationMode();
                currentPnr = modifyReservationService.getReservationSearchCriteria();
                if (_this.appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || _this.appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    appState = 'modifyReservation'
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),pnr:currentPnr.pnr,lastName:currentPnr.lastName,departureDate:currentPnr.departureDate}
                }else{
                    appState = 'extras';
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),mode: applicationService.getApplicationMode()}
                }
                _this.showExtrasTotal = {index: sectorCount, section:sectorCount};

                ancillaryRemoteService.loadFlexiData(param, function (response) {
                    loadingService.hidePageLoading();
                    var ondIds = [];

//                    console.log(response);
                    if(response == null){
                        $state.go(appState, urlParams);
                    }else {
                        //@TODO re check when modify segment
                        if ((_this.appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI ) ) {
                            // _this.baggagePreferance = ancillaryService.getAncillaryModel()[param];
                            //_this.flexiPreferance = ancillaryService.getAncillary("FLEXIBILITY");
                            if(ancillaryService.getAncillaryModel()["FLEXIBILITY"] != undefined) {
                                _this.flexiPreferance = ancillaryService.getAncillaryModel()["FLEXIBILITY"]
                            }else{
                                _this.flexiPreferance = ancillaryService.getAncillaryFromResponse(param, response);
                            }
                        }else{
                            _this.flexiPreferance = ancillaryService.getAncillaryFromResponse(param, response);

                        }
                        angular.forEach(response, function (sector, key) {
                            if(!!_this.flexiPreferance.scopes[0].preferences[key]) {
                                if (_this.flexiPreferance.scopes[0].preferences[key].selectedItems == undefined) {
                                    _this.flexiPreferance.scopes[0].preferences[key].selectedItems = sector.items[0];
                                    if (sector.items[0].selected) {
                                        _this.flexiPreferance.scopes[0].preferences[key].total = sector.items[0].price;
                                    } else {
                                        _this.flexiPreferance.scopes[0].preferences[key].total = 0;
                                    }
                                }
                                isSelectable  [key] = sector.items[0].selected;
                                _this.flexiPreferance.scopes[0].preferences[key].passengers = [];
                                _this.isSelected[key] = _this.flexiPreferance.scopes[0].preferences[key].selectedItems.selected;
                                ondIds.push(sector.scope.ondId)
                            }


                        });

/*                        _this.flexiPreferance.scopes[0].preferences = _.sortBy(_this.flexiPreferance.scopes[0].preferences, function(pref){
                            return pref.preferanceId;
                        });

                        ondIds = ondIds.sort();*/

                        ancillaryService.removeUnMatchPreference(_this.flexiPreferance.scopes, ondIds);

                        _this.currentSector = _this.flexiPreferance.scopes[0].preferences[sectorCount];
                        _this.currentFlexiCharge = _this.currentSector.selectedItems.price;
                        _this.airportNames = getAirportNams();
                        flexiTotal();
                    }
                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);

                })

            })()
            function flexiTotal() {
                angular.forEach(_this.flexiPreferance.scopes[0].preferences, function (pref, key) {
                    var flexiObj = {}
                    flexiObj.selectedItems = {}
                    flexiObj.selectedItems.total = pref.selectedItems;
                    var anciTotal = ancillaryService.getSegmentTotal({pref: flexiObj});

                    if (pref.selectedItems.selected) {
                        pref.total = anciTotal;
                    }else{
                        pref.total = 0;
                    }
                })
                var segmentTotal = ancillaryService.getAncileryTotal(_this.flexiPreferance.scopes[0].preferences);
                _this.flexiPreferance.scopes[0].total = segmentTotal;
                _this.flexiPreferance.total = segmentTotal;

            }

            function getAirportNams() {
                var airport = {}
                var fromCode = "";
                var toCode = "";
                if (_this.flexiPreferance.scopes[0].scopeType == "SEGMENT") {
                    fromCode = _this.currentSector.segments[0].from
                    toCode = _this.currentSector.segments[0].to
                } else if (_this.flexiPreferance.scopes[0].scopeType == "OND") {
                    var lastIndex = _this.currentSector.segments.length - 1
                    fromCode = _this.currentSector.segments[0].from
                    toCode = _this.currentSector.segments[lastIndex].to
                }

                airport.departureAirportCode = fromCode;
                airport.arrivalAirportCode = toCode;

                airport.departureAirportName = extrasService.getAirportName(fromCode);
                airport.arrivalAirportName = extrasService.getAirportName(toCode);
                return airport;
            };

            this.nextSector = function (index, section) {

                // var obj =
                // extrasModel[Object.keys(extrasModel)[trip]][sectorCount].passenger
//                console.log(index)

                if (index == undefined) {
                    sectorCount++;

                    if (this.flexiPreferance.scopes[0].preferences[sectorCount] == undefined) {
                        sectorCount = 0;
                    }
                    //this.tripType = Object.keys(this.sectorData)[trip];
                    this.currentSector = this.flexiPreferance.scopes[0].preferences[sectorCount];
                    this.showExtrasTotal = {
                        index: sectorCount,
                        section: sectorCount
                    }
                } else {
                    //this.tripType = Object.keys(this.sectorData)[trip];
                    this.currentSector = this.flexiPreferance.scopes[0].preferences[index];
                    sectorCount = index;
                    //trip = Object.keys(this.sectorData).indexOf(section);

                }
                _this.currentFlexiCharge = _this.currentSector.selectedItems.price;
                _this.airportNames = getAirportNams()
//                console.log(sectorCount);
                _this.currentOndId = sectorCount;

            };

            this.optionOnClick = function (id) {
            	if(typeof id.index != 'undefined' ){
            		sectorCount = id.index;
            		_this.currentOndId=id.index;
            	}
                if(isSelectable[sectorCount]){
                   return false;
                }
                var tempPref = angular.copy(_this.flexiPreferance.scopes[0].preferences[sectorCount]);
                if (_this.isSelected[sectorCount]) {
                    _this.isSelected[sectorCount] = false;
                    tempPref.selectedItems.selected=false;
                    tempPref.total = 0;
                } else {
                    _this.isSelected[sectorCount] = true;
                    tempPref.selectedItems.selected=true;
                    tempPref.total = tempPref.selectedItems.price;
                }
                _this.flexiPreferance.scopes[0].preferences[sectorCount] = tempPref
                flexiTotal();
            };

            this.backToExtras = function () {
                $rootScope.extraType = '';
                if(!ancillaryService.getAncillarySelected(ancillaryType)){
                    var ancillaryContinueMessage = $translate.instant('lbl_extra_flexiContinueMessage');
                    popupService.confirm(ancillaryContinueMessage,
                        function(){
                            ancillaryService.backToExtras();
                        }
                    );
                }else{
                    ancillaryService.backToExtras();
                }
            };

            this.backToHome = function() {
                navigationService.modifyFlowHome();
            };

            this.skipServices = function () {
                ancillaryService.skipServices(ancillaryType);
                $rootScope.extraType = '';
            };

        }]
    return controllers;
}