/**
 * Created by indika on 12/17/15.
 */
var mealController = function () {
    var controller = {};

    controller.mealController = [
        "$http",
        "$rootScope",
        "$location",
        "$translate",
        "passengerService",
        "extrasService",
        "extrasFactory",
        "$element",
        "ancillaryRemoteService",
        "ancillaryService",
        "IBEDataConsole",
        "loadingService",
        "applicationService",
        "quoteService",
        "$state",
        "languageService",
        "currencyService",
        "alertService",
        "modifyReservationService",
        "commonLocalStorageService",
        "popupService",
        "navigationService",
        "remoteMaster",
        "remoteCustomer",
        "$window",
        function ($http, $rootScope, $location, $translate, passengerService, extrasService, extrasFactory, $element,
                ancillaryRemoteService, ancillaryService, IBEDataConsole, loadingService, applicationService,
                quoteService, $state, languageService, currencyService, alertService, modifyReservationService,
                commonLocalStorageService, popupService, navigationService, remoteMaster, remoteCustomer, $window) {

 
            var self = this;
            self.accordionArray = [];
            var selectedCats = [], appMode, appState, urlParams, currentPnr;
            var unModifiedMealList;
            self.isNextSectorClick = false;
            var extrasModel = unModifiedMealList = {};
            var meal = {
                name: '',
                amount: '',
                value: '',
                mealId: ''
            };
            var sectorCount = 0, unModifiedSectorData;
            var trip = 0;
            var ancillaryType = "MEAL";
            var smallChunk = [], currentSlection = [], filteredSelection = [];


            self.currencyType = currencyService.getSelectedCurrency();

            self.mealList = [];
            self.addedMealList = {}; // Meals that have been added to any of the passengers
            self.categoriesMealList = {};
            self.mealCategories = [];
            self.selectedCategories = [];
            self.selectedMealList = [];
            self.currentSegment = 0;
            self.buttonCap = ['Add This Meal', 'done'];
            self.currentMealSelection = {}; // Object containing passengerRph and item count for currently selected item
            self.currentScope = {};
            self.currentAncillary = {};
            self.searchCriteria = commonLocalStorageService.getSearchCriteria();
            self.resetInputData = {};
            self.validationDefinitions = {};
            self.itemCount = {max: 9999, min: 0};
            self.mDevice = $rootScope.deviceType.isMobileDevice;
            self.selectedSortingCategory = '';
            self.restrictedCategory = {};
            $rootScope.removePaddingFromXSContainerClass = true;

            var sortByOption = "mealCharge";

            _.mixin({
                chunk: function (array, unit) {
                    if (!_.isArray(array))
                        return array;
                    unit = Math.abs(unit);
                    var results = [],
                            length = Math.ceil(array.length / unit);

                    for (var i = 0; i < length; i++) {
                        results.push(array.slice(i * unit, (i + 1) * unit));
                    }
                    return results;
                }
            });
            function removeExistingMealsForCategoryOfMeal(meal, sector, paxIndex){
            	var prefSector = self.mealPreferance.scopes[0].preferences[sector];
            	var segmentPax = prefSector.passengers[paxIndex];
            	var isMultiSelect = self.validationDefinitions[prefSector.preferanceId].multipleSelect;

            	var meals = angular.copy(segmentPax.selectedItems);
            	angular.forEach(segmentPax.selectedItems, function(selectedMeal, selectedMealCode){
            		if(selectedMeal.mealCategoryCode === meal.mealCategoryCode){
            			var count = selectedMeal.count || selectedMeal.quantity;
            			selectedMeal.mealCode = selectedMeal.id;
            			var newCount = ancillaryService.adjustInventory(ancillaryType, self.mealPreferance.scopes[0].preferences, sector,
            					paxIndex,{
            				isMultiSelect:isMultiSelect,
            				oldCount: count,
            				newCount: 0,
            				meal:selectedMeal
            			});

            			if(newCount == 0){
            				delete meals[selectedMealCode];
            			} else if(count > 0) {
            				throw "Meal removal failed";
            			}
            		}
            	});

            	segmentPax.selectedItems = meals;
            	segmentPax.itmeTotalCharge =ancillaryService.getMealTotalPerPassenger(meals);
                setMealTotalAllPassenges(appMode);

                return prefSector.qtyList[meal.mealCode].avl;
            };

            self.getInv = function(oldCount,newCount,paxIndex,meal){
            	var diff = newCount - oldCount;
            	if(diff == 0){
            		return newCount;
            	}
            	var segment = self.mealPreferance.scopes[0].preferences[sectorCount];
            	var isMultiSelect = self.validationDefinitions[segment.preferanceId].multipleSelect;
            	return ancillaryService.adjustInventory(ancillaryType,self.mealPreferance.scopes[0].preferences, sectorCount, paxIndex,
            			{isMultiSelect:isMultiSelect,
            				oldCount: oldCount,
            				newCount: newCount,
            				meal:meal,
            				callback: removeExistingMealsForCategoryOfMeal});
            }

            self.backToExtras = function () {

                $rootScope.extraType = '';
                if (!ancillaryService.getAncillarySelected(ancillaryType)) {
                    var ancillaryContinueMessage = $translate.instant('lbl_extra_mealContinueMessage');
                    popupService.confirm(ancillaryContinueMessage,
                            function () {
                                ancillaryService.backToExtras();
                            }
                    );
                } else {
                    ancillaryService.backToExtras();
                }
            };

            self.skipServices = function () {
                ancillaryService.skipServices(ancillaryType);
                $rootScope.extraType = '';
            };

            self.backToHome = function () {
                navigationService.modifyFlowHome();
            };

            self.pageSelection = function (index) {
                this.selectedMealList = filteredSelection.slice(index * 10);
            };

            /**
			 * When meal category change update the meal display according to the
			 * @param: id selected category
			 */
            self.updateMealSelection = function (id) {
                var selectedCats = [];

                if (id == "All") {
                    selectedCats = this.mealCategories;
                } else {
                    selectedCats[0] = this.mealCategories[id];
                }
                var mealList = new Object();
                var concatList = [];
                this.selectedMealList = [];
                smallChunk = [];

                var mealSelection = _.findWhere(self.categoriesMealList, selectedCats)
                angular.forEach(selectedCats, function (value, key) {
                    mealList[value] = []
                    mealList[value] = self.categoriesMealList[value]
                });
                angular.forEach(mealList, function (value, key) {
                    concatList = concatList.concat(value);
                });
                var longList = (selectedCats == null) ? [] : (selectedCats.length > 0) ? concatList : [];
                filteredSelection = longList;
                var numberofPages = _.range(0, longList.length, 10);
                currentSlection = longList;
                this.selectedMealList = longList;
                if (self.selectedSortingCategory != undefined && self.selectedSortingCategory != ''){
                	sortByOption = self.selectedSortingCategory;
                }
                self.sortBy(sortByOption);
                self.paginationData = {numberofPages: numberofPages, defaultselection: 0, isvisible: true}

                if ($rootScope.deviceType.isMobileDevice)
                    return this.selectedMealList;
            };

            /*
			 * Meal filtering functionality
			 * @param mealName : user input in search box
			 */
            self.onFiltering = function (mealName) {
                var length = 0;
                var filtered = _.filter(currentSlection, function (meal) {
                    if (meal.mealName != undefined) {
                        var name = meal.mealName.toLowerCase();
                        if (name.indexOf(mealName.toLowerCase()) > -1) {
                            return meal.mealName;
                        }
                    }
                })

                if (mealName.length > 1) {
                    self.selectedMealList = filtered;
                    length = filtered.length
                } else {
                    self.selectedMealList = currentSlection
                    length = currentSlection.length
                }
                filteredSelection = self.selectedMealList;
                var numberofPages = _.range(0, length, 10)
                self.paginationData = {numberofPages: numberofPages, defaultselection: 0, isvisible: true}

                self.sortBy(self.selectedSortingCategory);
            }
            /*
			 * on oder by option clicked
			 */
            self.sortBy = function (option) {
                var sortedList = []
                if (option == "mealName") {
                    sortedList = _.sortBy(filteredSelection, function (obj) {
                        return obj[option]
                    });
                } else {
                    sortedList = _.sortBy(filteredSelection, function (obj) {
                        return parseFloat(obj[option])
                    });
                }
                self.selectedMealList = sortedList;
                filteredSelection = sortedList;
                self.selectedSortingCategory = option;
            };
            /**
             * update meal preferance when user click on add button
             * @param mealCount - number of items
             * @param index - passenger index
             * @param value - meal object
             */
            self.setMealSelection = function (mealCount, index,fromMobileClick, value) {
                if(!fromMobileClick){
	                //TODO self.mealPreferance.scopes[0] when inbound & outbound trip implementation done scope array index should change according to the trip type
	                var tempSegment = angular.copy(self.mealPreferance.scopes[0].preferences[sectorCount]);
	                var tempPerson = angular.copy(tempSegment.passengers[index]);

	                if (_.isUndefined(tempPerson.selectedItems)) {
	                    tempPerson.selectedItems = {};
	                }
	                tempPerson.selectedItems[value.meal.mealCode] = meal;
	                var selectedItem = angular.copy(tempPerson.selectedItems[value.meal.mealCode]);
	                selectedItem.name = value.name;
	                selectedItem.count = mealCount;
	                selectedItem.price = Number(value.price || value.amount);
	                selectedItem.amount = Number(value.price || value.amount);
	                selectedItem.quantity = mealCount;
	                selectedItem.selectedItemTotal = Number(value.price || value.amount) * Number(mealCount);
	                selectedItem.mealCategoryCode = value.meal.mealCategoryCode;
	                selectedItem.categoryRestricted = value.meal.categoryRestricted;

	                selectedItem.id = value.meal.mealCode;
	                if (self.validationDefinitions[self.mealPreferance.scopes[0].preferences[sectorCount].preferanceId].multipleSelect) {
	                    tempPerson.selectedItems[value.meal.mealCode] = selectedItem;
	                    if (selectedItem.categoryRestricted) {
	                    	self.itemCount.max = 1;
	                    } else {
	                    	self.itemCount.max = value.meal.allocatedMeals;
	                    }
	                } else {
	                    self.itemCount.max = 1;
	                    tempPerson.selectedItems = {};
	                    tempPerson.selectedItems[value.meal.mealCode] = selectedItem;
	                }
	                tempPerson.itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(tempPerson.selectedItems, appMode);

	                if (mealCount === 0) {
	                    delete tempPerson.selectedItems[value.meal.mealCode];
	                }
	                tempSegment.passengers[index] = tempPerson;

	                self.mealPreferance.scopes[0].preferences[sectorCount].passengers[index] = tempPerson;

	                getAddedMeals(tempSegment);
	                setMealTotalAllPassenges(appMode);
	            }
            };

            /**
			 * Delete selected meal and update meal preferance when user click delete icon
			 * @param el-selected meal object
			 * @param event - event
			 * @param index - selected element index
			 */
            self.onDelete = function (el, event, index) {
                if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI && self.selectedMealList.length === 0) {
                    popupService.confirm("If removed, this meal cannot be re-added, do you want to continue?&lrm;",
                            function () {
                                var tempMeals = angular.copy(self.mealPreferance.scopes[0].preferences[sectorCount].passengers[index].selectedItems);
                                var objkey = el.$parent.item.id;

                                var inv = self.mealPreferance.scopes[0].preferences[sectorCount].qtyList[objkey];
                            	var count = tempMeals[objkey].count || tempMeals[objkey].quantity;

                                delete tempMeals[objkey];
                                inv.avl += count;

                                self.mealPreferance.scopes[0].preferences[sectorCount].passengers[index].itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(tempMeals);
                                self.mealPreferance.scopes[0].preferences[sectorCount].passengers[index].selectedItems = tempMeals;
                                setMealTotalAllPassenges(appMode);
                            }
                    );
                } else {
                    var tempMeals = angular.copy(self.mealPreferance.scopes[0].preferences[sectorCount].passengers[index].selectedItems);
                    var objkey = el.$parent.item.id;

                	var inv = self.mealPreferance.scopes[0].preferences[sectorCount].qtyList[objkey];
                	var count = tempMeals[objkey].count || tempMeals[objkey].quantity;

                    delete tempMeals[objkey];
                    inv.avl += count;

                    self.mealPreferance.scopes[0].preferences[sectorCount].passengers[index].itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(tempMeals);
                    self.mealPreferance.scopes[0].preferences[sectorCount].passengers[index].selectedItems = tempMeals;
                    setMealTotalAllPassenges(appMode);
                }
            };

            self.removeMeal = function(el, event, index) {
            	self.customerPreferredMealList.splice(el.$index, 1);
                self.customerPreferredMealList.totalItemPrice = self.customerPreferredMealList.totalItemPrice - el.$parent.item.totalPrice;
			}

            self.showHideMealPanel = function (evt, index, selectd, categoryCode,isClose) {
                var duplicateMealSelection = self.currentMealSelection;
                if (selectd === -1) {
                    angular.element(evt.currentTarget).find('.modify').addClass('fa-times');
                    angular.element(evt.currentTarget).find('.modify').removeClass('fa-angle-down');
                } else {
                    angular.element(evt.currentTarget).find('.modify').removeClass('fa-times');
                    angular.element(evt.currentTarget).find('.modify').addClass('fa-angle-down');
                }
                var selectedMealId = self.selectedMealList[index].mealCode;
                if(!isClose && isClose !== undefined){
                       for(var i=0;i<Object.keys(self.currentMealSelection).length;i++){
                           var count = (self.currentMealSelection[i] !== undefined) ? self.currentMealSelection[i].count : 0;
                           if(self.currentMealSelection[i].count !== 0)
                             self.setMealSelection(self.currentMealSelection[i].count,i,false,{name:self.selectedMealList[index].mealName,price:self.selectedMealList[index].mealCharge,meal:self.selectedMealList[index],mealIndex:index});
                           else{
                               var savedCount = $window.localStorage.getItem( 'savedDoneCount_'+i);
                               var itemId = $window.localStorage.getItem( 'item_'+i);
                               if(!isNaN(parseInt(savedCount)) && parseInt(savedCount) !== count && itemId === selectedMealId){
                                   self.setMealSelection(count,i,false,{name:self.selectedMealList[index].mealName,price:self.selectedMealList[index].mealCharge,meal:self.selectedMealList[index],mealIndex:index});
                               }
                           }
                       }

                }
                //Reset values in the current meal selection
                //if(!self.mDevice)
                    self.currentMealSelection = {};

                //Get meal ID for the selected index


                // For each passenger, get the meal preferences and set object with the current selections
                angular.forEach(self.mealPreferance.scopes[0].preferences[sectorCount].passengers, function (passenger, key) {
                    var count = (self.currentMealSelection[key] !== undefined) ? self.currentMealSelection[key].count : 0;
                    self.currentMealSelection[key] = {paxRph: passenger.passenger.paxSequence, count: count};

                    angular.forEach(passenger.selectedItems, function (item) {
                        if(self.mDevice){
                            //typeof categoryCode == 'undefined' || typeof item.categoryCode == 'undefined' ||
                            if (item.id === selectedMealId && (item.mealCategoryCode === categoryCode || categoryCode === '')) {
                               // if(!isClose){
                                    //alert('coun'+selectedMealId)
                                    console.log("item.mealCategoryCode" + item.mealCategoryCode + "--" + categoryCode)
                                    self.currentMealSelection[key].count = item.count || item.quantity;
                                    self.currentMealSelection[key].oldCount = item.count || item.quantity;
                                    console.log(self.currentMealSelection);
                                    $window.localStorage.removeItem( 'savedDoneCount_'+key );
                                    $window.localStorage.removeItem( 'item_'+key );
                                    $window.localStorage[ 'savedDoneCount_'+key ] = item.count || item.quantity;
                                    $window.localStorage[ 'item_'+key ] = item.id;
                               // }

                            }
                        }else{
                            //typeof categoryCode == 'undefined' || typeof item.categoryCode == 'undefined' ||
                        if (item.id === selectedMealId && (item.mealCategoryCode === categoryCode || categoryCode === '')) {
                            //alert('coun'+selectedMealId)
                            console.log("item.mealCategoryCode" + item.mealCategoryCode + "--" + categoryCode)
                            self.currentMealSelection[key].count = item.count || item.quantity;
                            self.currentMealSelection[key].oldCount = item.count || item.quantity;
                            console.log(self.currentMealSelection)
                        }
                        }

                    });
                });

            };

            self.resetOptions = function (evt) {
                angular.element(evt.currentTarget).find('.modify').removeClass('fa-times');
                angular.element(evt.currentTarget).find('.modify').addClass('fa-angle-down');
            };

            self.closeAllMealBlock = function () {
                $(".meal-slider .meal-block").hide();
                $('.fa-meal-block').removeClass('fa-minus');
                $('.fa-meal-block').addClass('fa-plus');
            }

            self.nextSector = function (index, section,event) {
                self.isNextSectorClick = true;
                console.log(' from ctrl index' + index)
                self.currentSegment = index;
//                console.log(self.mealPreferance.scopes[0])
                self.resetInputData.SearchText();
                if (index == undefined) {
                    sectorCount++;

                    if (self.mealPreferance.scopes[0].preferences[sectorCount] == undefined) {
                        sectorCount = 0;
                    }
                    self.currentSector = self.mealPreferance.scopes[0].preferences[sectorCount];
                    self.showExtrasTotal = {
                        index: sectorCount,
                        section: sectorCount
                    }
                } else {
                    sectorCount = index;
                    self.currentSector = self.mealPreferance.scopes[0].preferences[sectorCount];
                }
                self.airportNames = ancillaryService.getAirports(this.mealPreferance, sectorCount);

                categorizeMealList();
                getAddedMeals(self.currentSector);
                self.updateMealSelection('All');
                if (typeof self.resetInputData.Pagination === 'function') {
                    self.resetInputData.Pagination();
                }
//                if(event){
//                    event.stopPropagation();
//                }
            };

            /**
			 * Get the added meals for a specific section
			 * @parm: sector - The sector selected
			 */
            function getAddedMeals(sector) {
                self.addedMealList = {};

                for (var passenger in sector.passengers) {
                   for (var item in sector.passengers[passenger].selectedItems) {
                        self.addedMealList[item] = true;
                    }

                }
            }

            function setMealTotalAllPassenges(appMode) {
                angular.forEach(self.mealPreferance.scopes[0].preferences, function (pref, prefKey) {
                    var anciTotal = ancillaryService.getSegmentTotal(pref.passengers, appMode);
                    angular.forEach(pref.passengers, function (currentPassenger, passengerKey) {
                        if (!!currentPassenger.selectedItems) {
                            var totalForPassenger = ancillaryService.getMealTotalPerPassenger(currentPassenger.selectedItems, appMode);
                            self.mealPreferance.scopes[0].preferences[prefKey].passengers[passengerKey].itmeTotalCharge = totalForPassenger;
                        }
                    });
                    pref.total = anciTotal;
                });

                var segmentTotal = ancillaryService.getAncileryTotal(self.mealPreferance.scopes[0].preferences);
                self.mealPreferance.total = segmentTotal;
            }

//            function initilizeInventory(){
//            	angular.forEach(self.mealPreferance.scopes[0].preferences, function(segment){
//            		if(!segment.qtyList){
//            			var mealList = [];
//            			if(unModifiedMealList[segment.preferanceId]){
//            				mealList = unModifiedMealList[segment.preferanceId].items;
//            			}
//            			segment.qtyList = _.object(
//            					_.map(mealList,function(meal){
//            						return [meal.mealCode, {max:meal.availableMeals,avl:meal.availableMeals}];
//            					}));
//            		}
//            	});
//            }
//
            /**
			 *process uncategorized meals list and categorized them
			 */
            function categorizeMealList() {
                self.selectedMealList = [];
                currentSlection = [];
                filteredSelection = [];

                // In modification flow, if there are no meals in the list, remove existing meals and categories
                // var selectedFlights = commonLocalStorageService.getConfirmedFlightData();
                var selectedRPH = self.mealPreferance.scopes[0].preferences[sectorCount].preferanceId;
                if (unModifiedMealList[selectedRPH]) {
                    self.mealList = unModifiedMealList[selectedRPH].items;
                    var catList = _.groupBy(unModifiedMealList[selectedRPH].items, function (meal) {
                        return meal.mealCategoryCode;
                    });
                    self.categoriesMealList = catList;
                    self.mealCategories = selectedCats = _.keys(catList);
                    //self.selectedMealList = unModifiedMealList[sectorCount].items.slice(0, 10);
                    self.selectedMealList = currentSlection = filteredSelection = unModifiedMealList[selectedRPH].items;
                    self.selectedCategories = selectedCats;

                    self.addedMealList = _.object(
                            _.map(self.mealList, function (meal) {
                                var addedMeal = [meal.mealCode, 0];
                                return addedMeal;
                            }));


                } else {
                    self.categoriesMealList = null;
                    self.mealCategories = null;
                }


            }

            //IFFE
            (function () {
                window.scrollTo(0, 0);

                quoteService.setShowSummaryDrawer(false);
                appMode = applicationService.getApplicationMode();
                self.appMode = appMode;
                currentPnr = modifyReservationService.getReservationSearchCriteria();

                extrasModel = extrasService.getExtras();
                self.sectorData = extrasModel;

                if (Object.keys(passengerService.getPassengerDetails()).length) {
                    self.passengerList = passengerService.getPassengerDetailsForAnci();
                }
                for(var i=0;i<self.passengerList.length;i++){
                    $window.localStorage.removeItem( 'savedDoneCount_'+i );
                    $window.localStorage.removeItem( 'item_'+i );
                }
                self.sectorData = extrasModel;
                unModifiedSectorData = extrasModel;

                IBEDataConsole.log(ancillaryType);
                if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    appState = 'modifyReservation';
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        pnr: currentPnr.pnr,
                        lastName: currentPnr.lastName,
                        departureDate: currentPnr.departureDate
                    }
                } else {
                    appState = 'extras';
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    }
                }
                ancillaryRemoteService.loadMealList(ancillaryType, function (response) {
                        if(response == null){
                            response = commonLocalStorageService.getData('EXTRA_MEAL');
                        }
                        self.toFilterMeal = response[0].items;
                        IBEDataConsole.log(response);
                        loadingService.hidePageLoading();
                        angular.forEach(response, function (sector, sectorKey) {
                            unModifiedMealList[sector.scope.flightSegmentRPH] = sector;
                            self.validationDefinitions[sector.scope.flightSegmentRPH] = sector.validationDefinitions[sectorKey];

                        })

                        if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                            if (ancillaryService.getAncillaryModel()[ancillaryType] != undefined) {
                                var selectedMeals = ancillaryService.getAncillaryModel()[ancillaryType]
                                // self.mealPreferance =
								// ancillaryService.getAncillaryModel()[ancillaryType]//commonLocalStorageService.getData("ANCILLARY")['MEAL'];
                                self.mealPreferance = ancillaryService.setSelectedMealName(response[0].items, selectedMeals);
                                ancillaryService.setCurrentTempAncillary(self.mealPreferance);
                            } else {
                                self.mealPreferance = ancillaryService.getAncillaryFromResponse(ancillaryType, response);
                            }
                            ancillaryService.initilizeInventory("MEAL",self.mealPreferance.scopes[0].preferences, response);
                        } else {
                            self.mealPreferance = ancillaryService.getAncillaryFromResponse(ancillaryType, response);
                            ancillaryService.initilizeInventory("MEAL",self.mealPreferance.scopes[0].preferences, response);
                        }
                        self.customerPreferredMealList = []

//                        initilizeInventory();
                        if(!self.mealPreferance.isPageLoaded){
                        	// function to get the meal name
                			function getMealData() {
                				var mealNameList = remoteMaster
                						.retrieveMealLists()
                						.then(
                							function(response) {
                								self.mealList = response.data.mealList;
                								self.mealName = self.mealList.name;
           									var mealDetails = remoteCustomer
          											.getUserProfile()
                									.then(
                										function(response) {
                												mapMealCode(response.data.customer.cutomerPreferredMealList);
                												console.log(self.toFilterMeal,"W")
                											},function(error) {
                												console.log(error);
                										});
                								}, function(error) {
                        							console.log(error);
                							});
                			}
	                		// getMealData();
	            			// Mapping meal-code and meal-name
	            			function mapMealCode(data) {
	            				self.customerPreferredMealList.totalItemPrice = 0;

	            				var servedMeaCategoryMap = {};
	            				var tmpSectorCount = sectorCount;
	            				angular.forEach(self.mealPreferance.scopes[0].preferences, function(segment, segmentIndex){
	            					sectorCount = segmentIndex;
	                        		var mealList = [];
	                        		if(unModifiedMealList[segment.preferanceId]){
	                        			mealList = unModifiedMealList[segment.preferanceId].items;
	                        		}
	                        		angular.forEach(segment.passengers, function(pax, paxIndex){
	                            		angular.forEach(mealList, function(availableMeal, mealKey) {
	    	                        		angular.forEach(data, function(prefferedMeal, prefKey) {
	    	                        			if (availableMeal.mealCode === prefferedMeal.mealCode){
	    	                        				var count = self.getInv(0,prefferedMeal.quantity,paxIndex,availableMeal);
	    	                        				if(count > 0){
	    	                        					self.setMealSelection(count, paxIndex, false, {
	    	                        						name:availableMeal.mealName,
	    	                        						price:availableMeal.mealCharge,
	    	                        						meal:availableMeal,
	    	                        						mealIndex:mealKey
	    	                        					});
	    	                        				}
	    	                        			}
	    	                        		});
	                            		});
	                        		})

	                        	});
	            				sectorCount = tmpSectorCount;
	            				self.mealPreferance.isPageLoaded = true;
	            			}

                        }
                        self.currentScope = response[sectorCount].scope;
                        self.airportNames = ancillaryService.getAirports(self.mealPreferance, sectorCount);

                        categorizeMealList();
                        setMealTotalAllPassenges(appMode);
                        getAddedMeals(self.mealPreferance.scopes[0].preferences[0]);
                        self.updateMealSelection('All');
                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);

                });

                self.tripType = Object.keys(extrasModel)[trip];
                self.showExtrasTotal = {
                    index: sectorCount,
                    section: self.tripType
                };

                if (appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci();

                    if (unmodifiedAnci && unmodifiedAnci['MEAL']) {
                        self.previousSelection = unmodifiedAnci['MEAL'];
                    }
                }


            })();
        }];
    return controller;
};
