/**
 * Created by baladewa on 12/16/15.
 */
var seatmapController = function () {

    var controllers = {};

    controllers.seatmapController = [
        "passengerService",
        "IBEDataConsole",
        "commonLocalStorageService",
        "$timeout",
        "ancillaryRemoteService",
        "ancillaryService",
        "loadingService",
        "quoteService",
        "languageService",
        "currencyService",
        "applicationService",
        "$state",
        "alertService",
        "modifyReservationService",
        "popupService",
        "$q",
        "$translate",
        "navigationService",
        "$rootScope",
        "remoteCustomer",
        seatMapController];

    function seatMapController(passengerService, IBEDataConsole, commonLocalStorageService, $timeout,
            ancillaryRemoteService, ancillaryService, loadingService, quoteService, languageService,
            currencyService, applicationService, $state, alertService, modifyReservationService,
            popupService, $q, $translate, navigationService, $rootScope, remoteCustomer) {
         // For mobile menus
         if(!$rootScope.deviceType.isMobileDevice){
            $rootScope.removePaddingFromXSContainerClass=true;
         }
         

        // For mobile menu        
        var self = this;
        var ancillaryType = ancillaryService.ANCI_TYPES.SEAT;
        self.sectorCount = 0;
        var availableSeatData = {};
        var segMentRPHData = [];

        var currentPax,
                appMode,
                urlParams,
                currentPnr;
        var unmodifiedSelection = {}
        var selectedRPH

        self.jsonMapForSeats = {
            "availability": {
                "VAC": "VAC",
                "SEL": "SEL",
                "INA": "INA",
                "RES": "RES"
            }
        };


        self.assignToIndex = 0;
        self.passengerList = [];
        self.currentSector = {};
        self.seatPreferance = {};
        self.unmodifiedSeatList = [];
        self.isSeatPopupOpen = false;
        self.popUpData = "";
        self.flightId = "";
        self.appState = "";
        self.previousSeats = {};
        self.previousPaxSeats = {};
        self.previousSegmentPaxSeats = {};
        self.flightDirection = ancillaryService.FLIGHT_DIRECTION.OUT;
        self.searchCriteria = commonLocalStorageService.getSearchCriteria();
        self.accordionArray = [true];
        self.currency = currencyService.getSelectedCurrency();

        function setSeatTotal() {
            angular.forEach(self.seatPreferance.scopes[0].preferences, function (pref) {
                pref.total = ancillaryService.getSegmentTotal(pref.passengers);
            });
            self.seatPreferance.total = ancillaryService.getAncileryTotal(self.seatPreferance.scopes[0].preferences);
        }

        this.closeSeatMap = function () {
            if ($rootScope.deviceType.isMobileDevice) {
                $("#aircraft-modal-map").hide(1000);
            }
        };

        function showSeatMap() {
            if ($rootScope.deviceType.isMobileDevice)
                $("#aircraft-modal-map").slideToggle(1000);
                $("#aircraft-modal-map").scrollTop(0);
        }

        //Setting the seat information in the ancillary service after a seat has being selected
        this.afterSelectSeat = function (seat, pax, paxIndex, msg) {
        	self.seatPreferance.isPageLoaded = true;
            currentPax = paxIndex;
            //Set seat selection of the passenger
            var seatPrefs = self.seatPreferance.scopes[0].preferences[self.sectorCount].passengers[paxIndex];
            seatPrefs.selectedItems = [{
                id: seat.itemId,
                quantity: 1,
                input: null,
                price: seat.price
            }];
            ancillaryService.setAnciState('SEAT',true);
            setSeatTotal()
        };

        this.selectionChanged = function (e, element) {
            if ($(e.currentTarget).hasClass('seat-selected') && currentPax != undefined) {
                self.seatPreferance.scopes[0].preferences[self.sectorCount].passengers[currentPax].selectedItems = [];
            }
            updateSeatsForPassenger(self.selectedFlightSeatDetails);
            setSeatTotal();
        };

        // Validate seatmap against the validation rules provided
        function validatePassenger(pax, seat) {
            var isValid = true;
            var paxType = '';
            if (pax.category === 'Adult') {
                paxType = 'AD';
            } else if (pax.category === 'Child') {
                paxType = 'CH';
            } else if (pax.category === 'Infant') {
                paxType = 'IN';
            }

            if (seat.validations) {
                for (var i = 0; i < seat.validations.length; i++) {
                    if (isValid) {
                        var validationRule = getValidationRule(seat.validations[i]);

                        if (validationRule.applicability === "SEAT") {
                            var applyScope = validationRule.paxTypes;
                            for (var j = 0; j < applyScope.length; j++) {
                                if (applyScope[j] == paxType) {
                                    isValid = false;
                                    if (!validationRule.ruleMessage) {  // remove this after backend support to error messages
                                        validationRule.ruleMessage = "You cannot travel in this seat";
                                    }
                                    popupService.alert(validationRule.ruleMessage);
                                    break;
                                } else if (paxType === 'AD' && applyScope[j] === 'IN' && pax.infantSequence) {
                                    isValid = false;
                                    if (!validationRule.ruleMessage) {  // remove this after backend support to error messages
                                        validationRule.ruleMessage = "You cannot travel in this seat";
                                    }
                                    popupService.alert(validationRule.ruleMessage);
                                    break;
                                }
                            }
                        } else if (validationRule.applicability === "SEAT_ROW") {
                            // If the ruleCode is provided, we run the appropriate validation function
                            if (validationRule.ruleCode = "pax-type-count-for-row-seat") {
                                if (!paxTypeCountforRowValidation(validationRule, pax)) {
                                    isValid = false;
                                }
                            }

                            // Add future validations in the same format
                        }
                    }
                }
            }
            return isValid;
        }

        this.beforeSelectSeat = function (seat, pax, onSuccess) {
            var validSelection = true;

            // Set warning for emergency seat type.
            if (ancillaryRemoteService.previousSegmentPaxSeats[self.sectorCount] && ancillaryRemoteService.previousSegmentPaxSeats[self.sectorCount][pax.paxSequence] === seat.seatNumber) {
                seat.seatAvailability = "RES";
                onSuccess();
            } else {
                if (!ancillaryRemoteService.previousSegmentPaxSeats && ancillaryRemoteService.previousSegmentPaxSeats[self.sectorCount][pax.paxSequence] === pax.previousSeat.seatNumber) {
                    var preSeat = getSeatByNumber(pax.previousSeat.seatNumber);
                    preSeat.seatAvailability = "RES";
                }
                if (seat.type === "EXIT" && seat.seatAvailability === "VAC") {
                    popupService.showPopup('emergency.seat.alert.html',
                            {
                                cssClassName: 'mid-width',
                                seat: seat
                            },
                            function selectAccept() {
                                if (validatePassenger(pax, seat)) {
                                    onSuccess();
                                }
                            });
                } else if (seat.seatAvailability === "VAC") {
                    if (validatePassenger(pax, seat)) {
                        onSuccess();
                    }
                }
            }
        };

        function paxTypeCountforRowValidation(validationRule, pax) {
            var valid = true;
            if (pax.infantSequence) {
                var allowedInfants = validationRule.rowSeatPassengetCountMap.IN;
                allowedInfants = allowedInfants - getInfantsInRow(validationRule, pax);
                if (allowedInfants == 0) {
                    popupService.alert(validationRule.ruleMessage);
                    valid = false;
                }
            }
            return valid;
        }

        // Get any infants in the current seat row
        function getInfantsInRow(validationRule, selectedPassenger) {
            var infantCount = 0;

            // Check if the same rule is applied for any of the current passengers
            angular.forEach(self.seatPreferance.scopes[0].preferences[self.sectorCount].passengers, function (passenger) {
                if (passenger.selectedItems[0] && selectedPassenger.infantSequence && selectedPassenger.paxSequence !== passenger.passengerRph) {
                    // Get the validation rules for the current seat
                    var rulesIdsForSeat = getValidationRulesForSeat(passenger.selectedItems[0].seatNumber);

                    // Check if the validation rules are equal
                    angular.forEach(rulesIdsForSeat, function (ruleId) {
                        if (ruleId == validationRule.ruleValidationId) {
                            infantCount += 1;
                        }
                    });
                }
            });

            return infantCount;
        }

        // Get validation rules given a seatId
        function getValidationRulesForSeat(seatId) {
            var currentSeatObject = getSeatByNumber(seatId);

            if (currentSeatObject) {
                return currentSeatObject.validations;
            }
        }

        
        
        // Get the seat object using the seat number
        function getSeatByNumber (seatNumber) {
            //go through cabins
            for (var i = 0; i < self.currentSector.seatMapData.cabinClass.length; i++) {
                var cabin = self.currentSector.seatMapData.cabinClass[i];
                //go through airrow rowgroups
                for (var j = 0; j < cabin.airRowGroupDTOs.length; j++) {
                    var rowGroup = cabin.airRowGroupDTOs[j];
                    //go through airrow airColumnGroups
                    for (var k = 0; k < rowGroup.airColumnGroups.length; k++) {
                        var colGroup = rowGroup.airColumnGroups[k];

                        for (var l = 0; l < colGroup.airRows.length; l++) {
                            var airRow = colGroup.airRows[l];

                            for (var m = 0; m < airRow.airSeats.length; m++) {
                                var seat = airRow.airSeats[m];

                                if (seat.seatNumber === seatNumber) {
                                    return seat;
                                }
                            }
                        }
                    }
                }
            }
        }

        this.showExtrasTotal = {index: self.sectorCount, section: self.tripType};

        // Called when passenger is selected from passenger list
        this.passengerChange = function (passenger) {
            self.assignToIndex = passenger;

            showSeatMap();
        };

        //Gets flight information from localstorage and organizes them according to the Id
        this.getFlightDetailsById = function () {
            var selectedFlights = commonLocalStorageService.getConfirmedFlightData();
            var flightDetailsById = {};

            angular.forEach(selectedFlights, function (flight) {
                var flightDetails = flight.flight;

                //If there is more than one stop, we need to get the destination of the first segment from the first stops origin
                if (flight.flight.stops.length > 0) {
                    flightDetails.destinationCode = flight.flight.stops[0].originCode;
                    flightDetails.destinationName = flight.flight.stops[0].originName;
                }

                flightDetailsById[flightDetails.flightRPH] = flightDetails;

                angular.forEach(flight.flight.stops, function (flightDetails) {
                    flightDetailsById[flightDetails.flightRPH] = flightDetails;
                });
            });
            return flightDetailsById;
        };

        this.nextSector = function (index,$event) {
            self.currentSector.validationDefinitions = null;
            self.currentSector.seatMapData = null;
            if (index == undefined) {
                self.sectorCount++;

                if (self.seatPreferance.scopes[0].preferences[self.sectorCount] == undefined) {
                    self.sectorCount = 0;
                }
                selectedRPH = self.seatPreferance.scopes[0].preferences[self.sectorCount].preferanceId
                if (availableSeatData[selectedRPH] != undefined) {
                    self.currentSector.validationDefinitions = availableSeatData[selectedRPH].validationDefinitions;
                    self.currentSector.seatMapData = availableSeatData[selectedRPH].items;
                } else {
                    //no seat data available for this segement
                    self.currentSector.seatMapData = null;
                }
                this.showExtrasTotal = {
                    index: self.sectorCount,
                    section: self.sectorCount
                }
            } else {
                selectedRPH = self.seatPreferance.scopes[0].preferences[index].preferanceId
                if (availableSeatData[selectedRPH] != undefined) {
                    self.currentSector.validationDefinitions = availableSeatData[selectedRPH].validationDefinitions;
                    self.currentSector.seatMapData = availableSeatData[selectedRPH].items;
                } else {
                    //no seat data available for this segement
                    self.currentSector.seatMapData = null;
                }
                self.sectorCount = index;
            }
            self.selectedFlightSeatDetails = self.seatPreferance.scopes[0].preferences[self.sectorCount];
            self.airportNames = ancillaryService.getAirports(self.seatPreferance, self.sectorCount);

            var flightDetailsById = self.getFlightDetailsById();
            self.flightId = flightDetailsById[self.selectedFlightSeatDetails.preferanceId].flightNumber;

            self.flightDirection = applicationService.getFlightDirectionFromRph(self.selectedFlightSeatDetails.preferanceId);
            updateSeatsForPassenger(self.selectedFlightSeatDetails);
            if($event){
                $event.preventDefault();
                $event.stopPropagation();
            }
        };

        this.backToExtras = function () {
            var currentSelection = angular.copy(self.seatPreferance.scopes[0].preferences);

            function compareSeatSelection() {
                var deferred = $q.defer();


                var isEqual = true;
                angular.forEach(currentSelection, function (pref, key) {
                    var unmodifyPref = unmodifiedSelection[key];
                    angular.forEach(pref.passengers, function (passenger, pasKey) {
                        var unmodifyPas = unmodifyPref.passengers[pasKey].selectedItems[0];
                        isEqual = isEqual && _.isEqual(unmodifyPas, passenger.selectedItems[0]);
                    })
                });

                deferred.resolve(isEqual);
                return deferred.promise;
            }

            var compareSelection = compareSeatSelection();

            compareSelection.then(function (isChanged) {
                $rootScope.extraType = '';
                if (!ancillaryService.getAncillarySelected(ancillaryType)) {
                    var ancillaryContinueMessage = $translate.instant('lbl_extra_seatContinueMessage');
                    popupService.confirm(ancillaryContinueMessage,
                            function () {
                                ancillaryService.backToExtras();
                            }
                    );
                } else {
                    ancillaryService.backToExtras();
                    if (!isChanged) {
                        ancillaryService.setBlockSeatStatus(true);
                    }
                }

            })
        };

        this.skipServices = function (e) {
            e.preventDefault();
            angular.forEach(self.seatPreferance.scopes[0].preferences, function (sector) {
                angular.forEach(sector.passengers, function (passenger) {
                    passenger.selectedItems = [];

                })
            });

            updateSeatsForPassenger(self.selectedFlightSeatDetails);
            setSeatTotal();
            ancillaryService.backToExtras();
            ancillaryService.setAnciState('SEAT', false);
            $rootScope.extraType = '';

        };

        this.clearSelections = function () {
            angular.forEach(self.seatPreferance.scopes[0].preferences, function (sector) {
                angular.forEach(sector.passengers, function (passenger) {
                    passenger.selectedItems = [];

                })
            });

            updateSeatsForPassenger(self.selectedFlightSeatDetails);
            setSeatTotal();
        };

        this.backToHome = function () {
            navigationService.modifyFlowHome();
        };

        this.onDelete = function (element, event, index) {
        	self.seatPreferance.scopes[0].preferences[self.sectorCount].passengers[element.$parent.pasIndex].selectedItems = [];
            updateSeatsForPassenger(self.selectedFlightSeatDetails);
            setSeatTotal();
        };

        this.closePopup = function () {
            self.isSeatPopupOpen = false;
        };

        this.getFlightDirection = function (to, from) {
            return applicationService.getFlightDirection(to, from);
        };

        this.deleteSeatFromPassenger = function (paxIndex) {
            self.seatPreferance.scopes[0].preferences[self.sectorCount].passengers[paxIndex].selectedItems = [];
            updateSeatsForPassenger(self.selectedFlightSeatDetails);
            setSeatTotal();
            //$rootScope.deviceType.isMobile 
            if (true)
                showSeatMap();
        };


        //Sets the seat in passenger list according to the given sector details
        function updateSeatsForPassenger(sectorDetails) {
            angular.forEach(sectorDetails.passengers, function (passenger) {

                if (passenger.selectedItems.length > 0 || Object.keys(passenger.selectedItems).length > 0) {
                    self.passengerList[passenger.passengerRph - 1].seat = passenger.selectedItems[0];
                } else {
                    self.passengerList[passenger.passengerRph - 1].seat = null;
                }
            });
        }

        function getValidationRule(id) {
            var rules = self.currentSector.validationDefinitions;
            for (var i = 0; i < rules.length; i++) {
                if (rules[i].ruleValidationId == id) {
                    return rules[i];
                }
            }
        }

        (function init() {
            if ($rootScope.deviceType.isMobileDevice)
                $("#aircraft-modal-map").hide();

            window.scrollTo(0, 0);
            quoteService.setShowSummaryDrawer(false);
            //TODO - please check why is the model creating partially
            if (!!ancillaryService.getAncillaryModel()['SEAT']) {
                if (!ancillaryService.getAncillaryModel()['SEAT'].scopes) {
                    delete ancillaryService.getAncillaryModel()['SEAT'];
                }
            }


            appMode = applicationService.getApplicationMode();
            self.appMode = appMode;
            currentPnr = modifyReservationService.getReservationSearchCriteria();

            self.passengerList = passengerService.getPassengerDetailsForAnci();

            // Set app URL parameters according to the flow
            if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                var allPassengers = passengerService.getPassengerDetails();

                // TODO: Should be moved to the step where the model is created in modification flow
                angular.forEach(allPassengers.paxList,
                        function (infant) {
                            if (infant.type === "infant") {
                                var parentPassenger = _.find(self.passengerList, function (passenger) {
                                    return passenger.paxSequence === infant.travelingWith;
                                });
                                parentPassenger.infantSequence = infant.paxSequence;
                            }
                        }
                );

                self.appState = 'modifyReservation';
                urlParams = {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    pnr: currentPnr.pnr,
                    lastName: currentPnr.lastName,
                    departureDate: currentPnr.departureDate
                }
            } else {
                self.appState = 'extras';
                urlParams = {
                    lang: languageService.getSelectedLanguage(),
                    currency: self.currency,
                    mode: applicationService.getApplicationMode()
                }
            }

            ancillaryRemoteService.loadSeatMapData(ancillaryType, function (response) {
                var firstSegmentRPH
                if (response == null) {
                    $state.go(self.appState, urlParams);
                } else {
                    response = ancillaryService.orderResponse(response);



                    IBEDataConsole.log("Seatmap response");
                    IBEDataConsole.log(response);

                    angular.forEach(response, function (sector, key) {
                        availableSeatData[sector.scope.flightSegmentRPH] = sector;
                        segMentRPHData.push(sector.preferanceId);
                    });

                    /*
                     Since seat map passenger object initiation removed from modification_reservation service following code line will be added
                     remove the conditional check if there is no issue in the flow
                     */
                    if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                        if (ancillaryService.getAncillaryModel()['SEAT'] != undefined) {
                            self.seatPreferance = ancillaryService.getAncillaryModel()['SEAT'];
                        } else {
                            self.seatPreferance = ancillaryService.getAncillaryFromResponse("SEAT", response);
                        }

                    } else {
                        self.seatPreferance = ancillaryService.getAncillaryFromResponse("SEAT", response);
                    }
                    firstSegmentRPH = self.seatPreferance.scopes[0].preferences[0].preferanceId

                    if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                        //self.seatPreferance = ancillaryService.getAncillaryModel()[ancillaryType];
                        //self.seatPreferance = ancillaryService.getAncillaryFromResponse( "SEAT", response);
                        if (!ancillaryRemoteService.previousSeatsCreated) {
                            try {
                                angular.forEach(self.seatPreferance.scopes[0].preferences, function (seatPref, index) {
                                    angular.forEach(seatPref.passengers, function (passenger) {
                                        if (passenger.selectedItems.length > 0) {

                                            var currentSector = 0;
                                            angular.forEach(self.seatPreferance.scopes[0].preferences, function (pref, index) {
                                                if (pref.preferanceId === seatPref.preferanceId) {
                                                    currentSector = index;
                                                }
                                            });

                                            if (!self.previousSeats[currentSector]) {
                                                self.previousSeats[currentSector] = [];
                                            }
                                            if (!ancillaryRemoteService.previousSegmentPaxSeats[currentSector]) {
                                                ancillaryRemoteService.previousSegmentPaxSeats[currentSector] = [];
                                            }

                                            self.previousSeats[currentSector].push(passenger.selectedItems[0].id);
                                            var tempPreviousPaxSeats = ancillaryRemoteService.previousSegmentPaxSeats[currentSector];
                                            tempPreviousPaxSeats[passenger.passengerRph] = passenger.selectedItems[0].id;
                                            ancillaryRemoteService.previousSegmentPaxSeats[currentSector] = tempPreviousPaxSeats;
                                        }
                                    });
                                });
                            } catch (error) {
                                console.warn("Error setting default seats")
                            }
                            ancillaryRemoteService.previousSeatsCreated = true;
                        } else {
                            angular.forEach(ancillaryRemoteService.previousSegmentPaxSeats, function (segmentPreviousPaxSeats, index) {
                                self.previousSeats[index] = [];
                                angular.forEach(segmentPreviousPaxSeats, function (passengerSeats) {
                                    self.previousSeats[index].push(passengerSeats);
                                });
                            });
                        }
                    } else {
                        //self.seatPreferance = ancillaryService.getAncillary(ancillaryType);
                    }
                    selectedRPH = self.seatPreferance.scopes[0].preferences[0].preferanceId;
                    if (!!availableSeatData[selectedRPH]) {
                        self.currentSector.validationDefinitions = availableSeatData[selectedRPH].validationDefinitions;
                        self.currentSector.seatMapData = availableSeatData[selectedRPH].items;
                    }else{
                        self.currentSector.seatMapData = null;
                    }

                    //ancillaryService.removeUnMatchPreference(self.seatPreferance.scopes, segMentRPHData);
                    self.selectedFlightSeatDetails = self.seatPreferance.scopes[0].preferences[self.sectorCount];
                    self.airportNames = ancillaryService.getAirports(self.seatPreferance, self.sectorCount);
                    unmodifiedSelection = angular.copy(self.seatPreferance.scopes[0].preferences);
                    setSeatTotal();

                    var flightDetailsById = self.getFlightDetailsById();
                    if (appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                        self.flightId = flightDetailsById[Object.keys(flightDetailsById)[0]].flightNumber;
                    } else {
                        self.flightId = flightDetailsById[self.selectedFlightSeatDetails.preferanceId].flightNumber;
                    }

                    self.flightDirection = applicationService.getFlightDirectionFromRph(self.selectedFlightSeatDetails.preferanceId);
                    updateSeatsForPassenger(self.selectedFlightSeatDetails);
                }


                if (appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci();

                    if (unmodifiedAnci && unmodifiedAnci['SEAT']) {
                        self.previousSelection = unmodifiedAnci['SEAT'];
                    }
                }

            }, function (error) {
                loadingService.hidePageLoading();
                alertService.setPageAlertMessage(error);
                IBEDataConsole.log("Seatmap request failed");
            });

            $timeout(function () {
                loadingService.hidePageLoading()
            });

        })();

    } // End of seatMapController.

    return controllers;
};
