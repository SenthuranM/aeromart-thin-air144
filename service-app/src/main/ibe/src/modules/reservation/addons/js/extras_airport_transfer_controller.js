/**
 * Created by nipuna on 12/12/16.
 */
'use strict';
var extrasAirportTransferController = function () {
    var controllers = {};
    controllers.extrasAirportTransferController = [
        "passengerService",
        "ancillaryRemoteService",
        "loadingService",
        "ancillaryService",
        "IBEDataConsole",
        "quoteService",
        "currencyService",
        "languageService",
        "applicationService",
        "$state",
        "alertService",
        "modifyReservationService",
        "popupService",
        '$translate',
        'navigationService',
        function (passengerService, ancillaryRemoteService, loadingService, ancillaryService, IBEDataConsole,
                  quoteService, currencyService, languageService, applicationService, $state, alertService,
                  modifyReservationService, popupService, $translate, navigationService) {
            var self = this;
            var ANCILLARY_TYPE = "AIRPORT_TRANSFER",
                DATE_FORMAT = 'YYYY-MM-DD',
                TIME_FORMAT = 'HH:mm:ss',
                DATE_TIME_FORMAT = DATE_FORMAT + 'T' + TIME_FORMAT;

            var sectorCount = 0, appState, urlParams, currentPnr;

            self.allServices = []; //holds the backend response with all service information
            self.paxDataCollection = {}; //Collection of paxData objects for each segment
            self.selectedPax = 0; //index of the currently selected passenger
            self.servicesPreference = {}; //ancillary object for services
            self.availableServices = []; //holds available services for the current flight
            self.validationDefinitions = []; //holds validation definitions for the current segment

            //TODO: Should change with passenger and airport change
            self.currentlySelectedServices = {}; //holds selected items for current passenger and segment

            /* -------------------------COMMON ANCILLARY SERVICES--------------------------- */
            self.skipServices = function () {
                ancillaryService.skipServices(ANCILLARY_TYPE);
                $rootScope.extraType = '';
            };

            self.backToHome = function() {
                navigationService.modifyFlowHome();
            };

            self.backToExtras = function () {
                $rootScope.extraType = '';
                if (!ancillaryService.getAncillarySelected(ANCILLARY_TYPE)) {
                    var ancillaryContinueMessage = $translate.instant('lbl_continue_message');
                    popupService.confirm(ancillaryContinueMessage,
                        function () {
                            ancillaryService.backToExtras();
                        }
                    );
                } else {
                    ancillaryService.backToExtras();
                }
            };
            /* ----------------------------------------------------------------------------- */

            /**
             * Called when transfer item is removed from ancillary model
             * @param item - Item to be deleted, the passenger will be automatically selected
             */
            self.removeTransfer = function(item){
                delete self.currentlySelectedServices.selectedItems[item.airportTransferId];
                setAirportTransferServicesTotal();
            };

            /**
             * Called once the confirm button is clicked in the directive, validates and adds airport transfer
             * @param selectedItem - Airport Transfer item to get id and other details
             * @param comment - User input as comment which is to be validated
             */
            self.confirmAddTransfer = function(selectedItem, comment, dateValidations){
                var item = angular.copy(selectedItem);
                //Validate inputs
                if(!isAirportTransferValid(item, comment, dateValidations)){
                    return;
                }

                item.id = item.airportTransferId;
                item.quantity = 1;
                item.input = null;
                item.price = item.currentCharge;

                var transferDateTime = getCombinedDateTime(comment.transferDate, comment.transferTime);
                var dateTimeString = moment(transferDateTime).format(DATE_TIME_FORMAT);

                item.input = {
                    ancillaryType: "AIRPORT_TRANSFER",
                    "tranferDate": dateTimeString,
                    "transferAddress": comment.transferAddress,
                    "transferContact": comment.transferContact,
                    "transferType": comment.transferType,
                    "applyOn":  self.allServices[self.airportKey].scope.point
                };

                self.currentlySelectedServices.selectedItems[item.airportTransferId] = item;
                self.currentlySelectedServices.itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(self.currentlySelectedServices.selectedItems);
                self.paxDataCollection[sectorCount][self.selectedPax].assigned = (!_.isEmpty(self.currentlySelectedServices.selectedItems));
                setAirportTransferServicesTotal();
            };

            // Validate item
            function isAirportTransferValid(item, comment, dateValidations){
                var errors = [];

                if(!comment.transferAddress){
                    errors.push('Transfer Address cannot be empty');
                }if(!comment.transferContact){
                    errors.push('Transfer Contact cannot be empty');
                }if(!getCombinedDateTime(comment.transferDate, comment.transferTime)){
                    errors.push('Invalid date time');
                }else{
                    var selectedDateTime = getCombinedDateTime(comment.transferDate, comment.transferTime);

                    if(selectedDateTime < dateValidations.minDate) {
                        errors.push( 'Minimum date time is: ' + moment(dateValidations.minDate).format('DD MMM YYYY, HH:mm') );
                    } else if(selectedDateTime > dateValidations.maxDate) {
                        errors.push( 'Maximum date time is: ' + moment(dateValidations.maxDate).format('DD MMM YYYY, HH:mm') );
                    }
                }

                if(!comment.transferType){
                    errors.push('Transfer Type cannot be empty');
                }

                if(_.isEmpty(errors)){
                    return true;
                }else{
                    var errorMessage = "";

                    _.forEach(errors, function(err){
                        errorMessage = errorMessage + err + ', ';
                    });

                    popupService.alert(errorMessage);
                    return false;
                }
            }

            /**
             * Given two date objects containing, date and time in order, the objects are concatinated to produce
             * a JS date object.
             * @param date - JS Date object containing the correct date
             * @param time - JS Date object containing the correct time
             * @returns {*} - Combined date
             */
            function getCombinedDateTime(date, time){
                if (!date || !time || !moment(date).isValid() || !moment(time).isValid()) {
                    return false;
                } else {
                    var dateString = moment(date).format(DATE_FORMAT);
                    var timeString = moment(time).format(TIME_FORMAT);
                    var combinedDateTimeString = dateString + timeString;

                    return moment(combinedDateTimeString, DATE_FORMAT + TIME_FORMAT).toDate();
                }
            }

            /**
             * On passenger change, change pax type and update the selected service list
             * @param pax passenger object
             * */
            self.passengerChange = function (pax) {
                self.currentlySelectedServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[pax];
            };

            self.nextSector = function (index) {
                
               
                if (_.isUndefined(index)) {
                    sectorCount++;

                    if (self.servicesPreference.scopes[0].preferences[sectorCount] === undefined) {
                        sectorCount = 0;
                    }

                    self.currentServicePreference = self.servicesPreference.scopes[0].preferences[sectorCount];
                    self.sectionSelection = {
                        index: sectorCount,
                        section: sectorCount
                    };
                } else {
                    self.currentServicePreference = self.servicesPreference.scopes[0].preferences[0];
                    sectorCount = index;
                }
                var selectedRPH = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.flightSegmentRPH;
                var selectedPoint = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.point;
                var selectedAirportCode = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.airportCode;
                self.airportKey = selectedRPH + selectedPoint + selectedAirportCode;
                if(!!self.allServices[self.airportKey]) {
                    self.availableServices = self.allServices[self.airportKey].items;
                    self.validationDefinitions = self.allServices[self.airportKey].validationDefinitions;
                    self.noServices = false;
                }else{
                    self.availableServices = null;
                    self.noServices = true;
                }
                self.currentlySelectedServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[self.selectedPax];
                self.paxData = self.paxDataCollection[sectorCount];
                self.airportNames = ancillaryService.getAirports(self.servicesPreference, sectorCount);
//                 if($event){
//                    $event.stopPropagation();
//                }
                
            };


            // Private functions
            function setAirportTransferServicesTotal() {
                if(Object.keys(self.servicesPreference).length) {
                    angular.forEach(self.servicesPreference.scopes[0].preferences, function (pref) {
                        pref.total = ancillaryService.getSegmentTotal(pref.passengers);
                    });
                    self.servicesPreference.total = ancillaryService.getAncileryTotal(self.servicesPreference.scopes[0].preferences);
                }
            }

            (function init() {
                window.scrollTo(0, 0);
                quoteService.setShowSummaryDrawer(false);
                self.currency = currencyService.getSelectedCurrency();
                self.appMode = applicationService.getApplicationMode();
                self.noServices = false;

                // Setup correct re-direction in case ancillary model is empty (When refreshing etc)
                if (self.appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    currentPnr = modifyReservationService.getReservationSearchCriteria();
                    appState = 'modifyReservation';
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        pnr: currentPnr.pnr,
                        lastName: currentPnr.lastName,
                        departureDate: currentPnr.departureDate
                    }
                } else {
                    appState = 'extras';
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    }
                }

                // Setup unmodified ancillary in modify segment flow
                if(self.appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci();

                    if(unmodifiedAnci && unmodifiedAnci[ANCILLARY_TYPE]) {
                        if(!_.isEmpty(unmodifiedAnci[ANCILLARY_TYPE].scopes[0].preferences)){
                            self.previousSelection = unmodifiedAnci[ANCILLARY_TYPE];
                        }
                    }
                }

                // Loads up Airport Transfer available data
                ancillaryRemoteService.loadAirportTransferData(ANCILLARY_TYPE, function (response) {
                    loadingService.hidePageLoading();

                    // Navigate back to extras if no availability data is received for Airport transfer
                    if (response == null || !response.length) {
                        $state.go(appState, urlParams);
                    } else {
                        self.sectionSelection = {index: sectorCount};

                        // If ancillary model is created return it. Else, get it from response
                        if (self.appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || self.appMode == applicationService.APP_MODES.MODIFY_SEGMENT ) {
                            if(ancillaryService.getAncillaryModel()[ANCILLARY_TYPE] != undefined) {
                                self.servicesPreference = ancillaryService.getAncillaryModel()[ANCILLARY_TYPE];
                            }
                            else{
                                self.servicesPreference = ancillaryService.getAncillaryFromResponse(ANCILLARY_TYPE, response);
                            }
                        } else {;
                            self.servicesPreference = ancillaryService.getAncillaryFromResponse(ANCILLARY_TYPE, response);
                        }

                        // Setting available services and passengers
                        angular.forEach(response, function (section, key) {
                            var currentSectorAirportKey = section.scope.flightSegmentRPH + section.scope.point + section.scope.airportCode;
                            self.allServices[currentSectorAirportKey] = section;

                            // Set pax data for the segment
                            var passengerList = passengerService.getPassengerDetailsForAnci();
                            self.paxDataCollection[key] = (_.extend({}, passengerList));

                            // Setting preferenceId for each segment
                            if (self.servicesPreference.scopes[0].preferences[key] != undefined) {
                                if (self.appMode != applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                                    self.servicesPreference.scopes[0].preferences[key].preferanceId = section.scope;
                                }
                            }
                        });

                        // Setting airport key to the first one
                        var selectedRPH = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.flightSegmentRPH;
                        var selectedPoint = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.point;
                        var selectedAirportCode = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.airportCode;
                        self.airportKey = selectedRPH + selectedPoint + selectedAirportCode;

                        // Set initial available services and noServices status
                        if(!!self.allServices[self.airportKey]) {
                            self.availableServices = self.allServices[self.airportKey].items;
                            self.validationDefinitions = self.allServices[self.airportKey].validationDefinitions;
                            self.noServices = false;
                        }else{
                            self.availableServices = null;
                            self.noServices = true;
                        }

                        // Set initial pax data
                        self.paxData = self.paxDataCollection[sectorCount];

                        if (self.appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || self.appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {

                            var allPassengers = passengerService.getPassengerDetails();

                            // Adding infant sequance to passenger
                            // TODO: Should be moved to the step where the model is created in modification flow
                            angular.forEach(self.paxDataCollection, function(currentPaxList){
                                angular.forEach(allPassengers.paxList,
                                    function (infant) {
                                        if (infant.type === "infant") {
                                            var parentPassenger = _.find(currentPaxList, function (passenger) {
                                                return passenger.paxSequence === infant.travelingWith;
                                            });
                                            parentPassenger.infantSequence = infant.paxSequence;
                                        }
                                    }
                                );
                            });
                        }

                        //By default set the first passenger in the first segments selection as the current one
                        self.currentlySelectedServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[self.selectedPax];

                        self.airportNames = ancillaryService.getAirports(self.servicesPreference, sectorCount);
                        setAirportTransferServicesTotal();
                    }
                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);

                });
            }());

        }];
    return controllers;
};