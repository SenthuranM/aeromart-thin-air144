'use strict';

var extrasMealService = function () {
    var service = {};
    service.extrasMealService = ["commonLocalStorageService", "extrasFactory", "$rootScope", "$location", "passengerService", "httpServices",
    	"currencyService", "applicationService", "remoteCustomer",
        function (commonLocalStorageService, extrasFactory, $rootScope, $location, passengerService, httpServices,
          currencyService, applicationService, remoteCustomer) {

      var _this = this;

    	this.adjustInventory = function(preferences, sectorIndex, paxIndex, config){
    		//var config = {isMultiSelect: false, oldCount:0, newCount:0,
    		//meal:{categoryRestricted: true,mealCategoryCode:'',callback:function(){}};
    		var oldCount = config.oldCount;
    		var newCount = config.newCount;
    		var meal = config.meal;
    		var categoryMealRemoveCallBack = config.callback
    		var segment = preferences[sectorIndex];
        	var segmentPax = segment.passengers[paxIndex];
        	if(!segmentPax.restrictedCategory){
        		segmentPax.restrictedCategory = {};
        	}
	        var inv = segment.qtyList[meal.mealCode];
	        var isMultiSelect = inv.isMultiSelect;
	        var categoryRestricted = inv.categoryRestricted;
	        var mealCategoryCode = inv.mealCategoryCode;
    		if (isMultiSelect) {
	            var qty = 0;
	            var diff = newCount - oldCount;

	        	if(diff == 0){
	        		return newCount;
	        	}

             	if(categoryRestricted){
             		if(diff > 0){
             			if(!segmentPax.restrictedCategory[mealCategoryCode]){
             				segmentPax.restrictedCategory[mealCategoryCode] = true;
             				inv.avl = inv.avl+diff - 1;
             				qty = 1;
             			} else if(categoryMealRemoveCallBack && categoryMealRemoveCallBack(meal,sectorIndex, paxIndex) >= 1){
             				segmentPax.restrictedCategory[mealCategoryCode] = true;
             				inv.avl = inv.avl+diff - 1;
             				qty = 1;
             			} else {
             				inv.avl = inv.avl+diff
             				qty = 0;
             			}
             		} else if(diff < 0 && segmentPax.restrictedCategory[mealCategoryCode]){
             			inv.avl = inv.avl-diff
             			segmentPax.restrictedCategory[mealCategoryCode] = false;
             			qty = 0;
             		}
             	} else {
             		if (diff > 0){
 	                	var inc = Math.min(inv.avl, diff);
 	                	inv.avl = inv.avl-inc;
 	                	qty =  oldCount + inc;
 	                } else if (diff < 0){
 	                	var dec = Math.max((-inv.max + inv.avl),diff);
 	                	dec = (oldCount + dec ) < 0? (-oldCount): dec;
 	                	inv.avl = inv.avl-dec;
 	                	qty = oldCount + dec;
 	                }
             	}

             	return qty;

            } else {
             	if(newCount >= 1){
             		return 1;
             	} else {
             		return 0;
             	}
            }
    	};

    	this.initilizeInventory = function(preferences, sectors){

        function isMultiSelectectionEnabled(sector, rules){
            var rule =  _.filter(sector.validationDefinitions, function(def){
              for(var i = 0; i< rules.length; i++){
                return (def.ruleValidationId == rules[i] && def.ruleCode == 'common-multiple-selections');
              }
              return false;
            });

            if(rule && rule.length >= 1){
              return rule[0].multipleSelect;
            }
            return false;
        }

    		angular.forEach(sectors, function(sector){
    			angular.forEach(preferences, function(segment){
    				if(!segment.qtyList){
    					if(sector.scope.flightSegmentRPH == segment.preferanceId){
    						var mealList = [];
    						if(sector.items){
    							mealList = sector.items;
    						}
    						segment.qtyList = _.object(
    								_.map(mealList,function(meal){
    									return [meal.mealCode, {
                        max:meal.allocatedMeals,
                        avl:meal.availableMeals,
                        isMultiSelect: isMultiSelectectionEnabled(sector, meal.validations),
                        categoryRestricted: meal.categoryRestricted,
                        mealCategoryCode:meal.mealCategoryCode
                      }];
    								}));
    					}
    				}
    			});
    		});
        };

        function loadCustomerPreferredMeals(callback){
          remoteCustomer.getUserProfile().then(function(response){
            if(response && response.data && response.data.customer){
              callback(response.data.customer.cutomerPreferredMealList);
            }else {
              console.log(response);
            }
          },function(error) {
                  console.log(error);
              });
        }

        function createMeal(mealCount, value){
          var meal = {
              name: '',
              amount: '',
              value: '',
              mealId: ''
          };


          var mealItem = angular.copy(meal);
          mealItem.name = value.mealName;
          mealItem.count = mealCount;
          mealItem.price = Number(value.mealCharge);
          mealItem.amount = Number(value.mealCharge);
          mealItem.quantity = mealCount;
          mealItem.selectedItemTotal = Number(value.mealCharge) * Number(mealCount);
          mealItem.mealCategoryCode = value.mealCategoryCode;
          mealItem.categoryRestricted = value.categoryRestricted;
          mealItem.mealId = value.mealCode;
          mealItem.id = value.mealCode;
          mealItem.isCustomerPreferredMeal = true;
          return mealItem;
        }

        this.setCustomerPreferredAnci = function(preferences, sectors, callback){

          loadCustomerPreferredMeals(function(response){
            if(!preferences.customerPreferencesSet){
              if(response && response.length > 0){
                angular.forEach(preferences, function(segment, segmentIndex){
                  angular.forEach(sectors, function(sector){
                    if(segment.preferanceId == sector.scope.flightSegmentRPH){
                      var mealList = sector.items;
                      angular.forEach(segment.passengers, function(pax, paxIndex){
                        if(_.isEmpty(pax.selectedItems)){
                          if (_.isUndefined(pax.selectedItems)) {
                              tempPerson.selectedItems = {};
                          }
                          angular.forEach(response, function(prefMeal, prefMealCode){
                            angular.forEach(mealList, function(availableMeal, mealKey) {
                              if (availableMeal.mealCode === prefMeal.mealCode){
                                var config = {
                                  oldCount:0,
                                  newCount:prefMeal.quantity,
                                  meal:{
                                    mealCode:availableMeal.mealCode,
                                    callback:function(){ return 0}
                                  }
                                };
                                var count = _this.adjustInventory(preferences,segmentIndex, paxIndex, config);
                                if(count> 0){
                                  var meal  = createMeal(count, availableMeal);
                                  pax.selectedItems[availableMeal.mealCode] = meal;
                                }
                              }
                            });
                          });
                        }

                      });

                    }
                  })
                });
                preferences.customerPreferencesSet = true;
                callback();
              }
            }

          });
        }

    }];
    return service;
};
