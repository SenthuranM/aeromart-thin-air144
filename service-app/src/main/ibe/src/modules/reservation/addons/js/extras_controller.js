/**
 * Created by indika on 11/5/15.
 */
var extrasController = function () {
    var controllers = {};
    controllers.extrasController = [
        "ancillaryRemoteService",
        "$location",
        "stepProgressService",
        "$rootScope",
        "commonLocalStorageService",
        "extrasService",
        "extrasFactory",
        "IBEDataConsole",
        "ancillaryService",
        "loadingService",
        "currencyService",
        "$state",
        "languageService",
        "applicationService",
        "alertService",
        "modifyReservationService",
        "quoteService",
        "navigationService",
        "popupService",
        "passengerService",
        "appConstants",
        "$sce",
        '$translate',
        '$window',
        '$timeout',
        'eventListeners',
        'constantFactory',
        'scrollService',
        'remoteCustomer',
        'signoutServiceCheck',
        'signInService',
        function (ancillaryRemoteService, $location, stepProgressService, $rootScope, commonLocalStorageService,
                  extrasService, extrasFactory, IBEDataConsole, ancillaryService, loadingService, currencyService,
                  $state, languageService, applicationService, alertService, modifyReservationService, quoteService,
                  navigationService, popupService, passengerService, appConstants,$sce,$translate, $window, $timeout,
                  eventListeners, constantFactory, scrollService, remoteCustomer,signoutServiceCheck,signInService) {

            // For mobile menus
            $rootScope.removePaddingFromXSContainerClass=true;
            $rootScope.extraType = '';
            var sectorCount = 0,appMode,currentPnr;
            var trip = 0;
            var extrasModel = {},flightData,availableAcniList =[];
            //var extrasTotatlFare = {};
            var _this = this;
            _this.isopen = true;
            _this.anciLabels = {};


            _this.curencyTypes = commonLocalStorageService.getCurrencies();
            _this.defaultCurrencyType = currencyService.getSelectedCurrency() || "AED";

            _this.showModifySearch = function () {
                eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
            };
            
            

            var baggage = {
                price: 0,
                value: 0,
                baggageId: null
            };

            var buttonLabels = {
                'SELECT_NOW': "Select Now",
                'EDIT_SELECTION': "Edit Selection"
            };

            var dialogTranslate = {
                haveNot: "You have not selected ancillary types of, ",
                continue: " do you want to continue?"
            };

            var toTranslation = "to";
            var selecteLanguage = languageService.getSelectedLanguage();
            _this.searchCriteria = commonLocalStorageService.getSearchCriteria();

            // If translate is ready, translate items, else translate later.
            if($translate.isReady()){
            	if($rootScope.deviceType.isMobileDevice){
            		 buttonLabels = {
                             'SELECT_NOW': $translate.instant('lbl_extra_select'),
                             'EDIT_SELECTION': $translate.instant('lbl_extra_edit')
                         };
                }else{
                	 buttonLabels = {
                             'SELECT_NOW': $translate.instant('lbl_extra_selectNow'),
                             'EDIT_SELECTION': $translate.instant('lbl_extra_editSelection')
                         };
                }


                toTranslation = $translate.instant('msg_common_to');
            }

            _this.contentLoaded = false; //Show busyLoader until ancillary data is generated

            stepProgressService.setCurrentStep('extras');



            this.accumulatedTotal = function (type) {
                var anciType = type.replace(' ', '').toUpperCase();
                return ancillaryService.getSelectedAncillaryTotal(anciType);
            }
            /*
             To accumulate default baggage total
             */
            function setAnciTotal(preferance) {
                var segmentTotal =0;
                if(preferance.scopes.length > 0){
                    angular.forEach(preferance.scopes[0].preferences, function (pref, key) {
                    var anciTotal = ancillaryService.getSegmentTotal(pref.passengers,appMode);
                    pref.total = (isNaN(anciTotal)) ? 0 : anciTotal;
                })
                var segmentTotal = ancillaryService.getAncileryTotal(preferance.scopes[0].preferences);

                }

                // If modify_segment mode, add un-modified segments totals to the segment totals
                if(appMode == applicationService.APP_MODES.MODIFY_SEGMENT){
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci();

                    if(!!unmodifiedAnci){
                        var anciType = unmodifiedAnci[preferance.ancillary];
                    }
                    if(anciType){
                        segmentTotal += ancillaryService.getAncileryTotal(anciType.scopes[0].preferences);
                    }
                }

                return segmentTotal;

            }

            function insuranceTotal() {
                angular.forEach(_this.insurancePreferance.scopes[0].preferences, function (pref, key) {
                    var insuranceObj = {}
                    insuranceObj.selectedItems = pref.selectedItems;
                    var anciTotal = ancillaryService.getSegmentTotal({pref: insuranceObj},appMode);
                    pref.total = anciTotal;
                })
                var segmentTotal = ancillaryService.getAncileryTotal(_this.insurancePreferance.scopes[0].preferences);
                _this.insurancePreferance.total = segmentTotal;

            }
            function flexiTotal() {
                angular.forEach(_this.flexiPreferance.scopes[0].preferences, function (pref, key) {
                    var flexiObj = {}
                    flexiObj.selectedItems = {}
                    flexiObj.selectedItems.total = pref.selectedItems;
                    var anciTotal = ancillaryService.getSegmentTotal({pref: flexiObj},appMode);
                    if(pref.total == 0){
                        pref.total = 0;
                    }else{
                        pref.total = anciTotal;
                    }
                })
                var segmentTotal = ancillaryService.getAncileryTotal(_this.flexiPreferance.scopes[0].preferences);
                _this.flexiPreferance.total = segmentTotal;

            }

            function setAnciAvailabilityList(data) {
//                console.log('<-------------success------------->')
//                console.log(data)
            }

            function onAniError() {
//                console.log('<------------ERROR------------>')
            }

            this.title = "Select extras for your trip";

            this.anciOptions = ancillaryService.getDefaultAncillary();
            this.backToHome = function () {
                if (applicationService.getApplicationMode() == applicationService.APP_MODES.CREATE_RESERVATION) {
                    navigationService.createFlowHome();
                } else {
                    navigationService.modifyFlowHome();
                }
            };

            this.goBack = function () {
                $state.go('modifyFlightHome.flightHome', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()
                });
            };

            function isAncilarySelected(preferance){
                var hasItems = false;
                angular.forEach(preferance,function(pref){
                    if(!hasItems) {
                        if(pref.passengers != undefined && pref.passengers.length > 0) {
                            for (var i = 0; i < pref.passengers.length;) {
                                if (pref.passengers[i].selectedItems != undefined) {
                                    if (Object.keys(pref.passengers[i].selectedItems).length > 0) {
                                        hasItems = true;
                                        break;
                                    }
                                }
                                i++;
                            }
                        }else if(pref.selectedItems != undefined){ // if type of anci is all passenger
                            if (pref.selectedItems.length > 0 || pref.selectedItems.selected) {
                                hasItems = true;
                            }
                        }
                    }
                });
                return hasItems;
            }


            /*
            ** produce ancillary preferances for the given ancillary type
            *  @param anciType: type of the ancillary ie: "MEAL"
            *  @param ancidata: ancillary data
            *  @appMode : application mode
             */
            function getAnciPreferances(anciType,anciData,appMode){
                var preferances = ancillaryService.getAncillaryFromResponse( anciType, anciData);
                return preferances;
            }


            /*
            ** If an Ancillary selected for at least single segment of given journey set the summery
            * @parama isAnciSelected : Boolean for check anci selection
             */
            function setAnciSummerySellection(isAnciSelected,type,summery){
                _this.anciOptions[type].currentSelection = {};

                // Generate segment summary for un-modified segments when modifying segment
                if(appMode == applicationService.APP_MODES.MODIFY_SEGMENT){
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci(),
                        anciType;

                    if(unmodifiedAnci){
                        anciType = unmodifiedAnci[type];
                    }

                    if(anciType){
                        angular.forEach(anciType.scopes[0].preferences, function (sector, key) {
                            var passengers = sector.passengers;
                            var segments = sector.segments || {};
                            summery[getSegmentCombination(anciType.scopes[0], segments) + ' '] = setAnciSummery(passengers, appMode, type);
                            summery[getSegmentCombination(anciType.scopes[0], segments) + ' '].unmodified = true;
                        });
                    }
                }

                var summaryWithSummary = _.filter(summery,
                	     function(val) {
                	         return !_.isUndefined(val.summaryText)
                	     }
                );

                if (summaryWithSummary.length > 0) {
                    angular.forEach(summery, function(val, key) {


                        if (val.summaryText == undefined || val.summaryText == null ||
                            val.summaryText == "null") {
                            _this.anciOptions[type].currentSelection[key] = {
                                unmodified: val.unmodified,
                                summaryText: "No selection is done for this sector"
                            }
                        } else {
                            if (type === "FLEXIBILITY") {
                                val.summaryText = "Flexi added";
                                if ($translate.instant('lbl_extra_flexiAdded') !=
                                    'lbl_extra_flexiAdded') {
                                    val.summaryText =
                                        $translate.instant('lbl_extra_flexiAdded');
                                }
                            }
                            if (type !== "SEAT") {
                                val.summaryText = val.summaryText.split(',').join(',\n');
                            }
                            _this.anciOptions[type].currentSelection[key] = val;
                        }

                    });
                }

                var selectionArray = [];
                _.each(_this.anciOptions[type].currentSelection, function(value, key) {
                    selectionArray.push({ 'segkey': key, 'segments': value.summaryText, 'unmodified': value.unmodified });
                });

                _this.anciOptions[type].currentSelection = selectionArray;
            }


            /*
            set summery for each segment for a given ancillary type
            @param passenger : passenger object
            @param apmode : current application mode
            @param type : Ancillary type
             */
            function setAnciSummery(passengers,appMode,type){
                var anciSummery = {};
                var summery;
                var passengerSummery = [];
                angular.forEach(passengers, function (pas, paskey) {
                    passengerSummery[paskey] = {};
                    if (pas.selectedItems != undefined ) {
                        angular.forEach(pas.selectedItems, function (item, key) {
                            var id = (item.name || item.seatNumber || item.value || item.ssrName);
                            id = (id==undefined) ? 'Item(s) Selected' : id;
                            if(type == "SEAT"){
                                id = (item.seatNumber || item.id)
                            }else if(type == "AIRPORT_TRANSFER"){
                                var input = item.input;
                                id = input.applyOn + ' ' + item.ssrName;
                            }

                            if(anciSummery[id] != undefined){
                               anciSummery[id] = anciSummery[id] + item.quantity || 1;
                            }else{
                                anciSummery[id] = item.quantity || 1
                            }
                        })
                    }
                });
                angular.forEach(anciSummery,function(val,key){
                    if(type == "SEAT"){
                        if(summery != undefined){
                            summery = summery +', ' + key;
                        }else{
                            summery = key;

                        }
                    }else{
                        if(summery != undefined){
                            summery = summery +', ' + val + ' X ' + key;
                        }else{
                            summery = val + ' X ' + key;

                        }

                    }
                })
                //summery = (summery == undefined)? "No selection is done for this sector" : summery;
                return {
                    'summaryText': summery,
                    'unmodified': false
                };
            }


            function setAnciDetailSummery(passengers,appMode){
                var anciSummery = [];
                angular.forEach(passengers, function (pas, key) {

                    if (pas.selectedItems != undefined ) {
                        var passengerSummery = {}
                        passengerSummery.passengerName = (pas.passenger != undefined)?pas.passenger.displayName : "ALL PASSENGER";
                        var items ,totalCharge = 0
                        angular.forEach(pas.selectedItems, function (item, key) {
                            if(items != undefined){
                                items = items + "," + ((item.quantity + ' x ') + (item.name || item.seatNumber || item.value || item.ssrName));
                            }else{
                                if(item.quantity != undefined || item.quantity != null) {
                                    items = (item.quantity + ' x ') + (item.name || item.seatNumber || item.value || item.ssrName);
                                }
                            }
                            if((appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT)){
                                totalCharge = totalCharge + (item.price || item.amount) * item.quantity;
                            }else{
                                totalCharge = totalCharge + (item.price || item.amount);
                            }
                        })
                        passengerSummery.item = items;
                        passengerSummery.charge = totalCharge;
                        anciSummery.push(passengerSummery);
                    }
                });
                return anciSummery;
            }

            function addOndPreferences(baggagePreferences)
            {
            	var tempOndPref = [];

            	for(var j=0;j < ancillaryRemoteService.metaData.ondPreferences.length;j++)
            	{
            		tempOndPref[ancillaryRemoteService.metaData.ondPreferences[j].ondId] = "0";
            	}

            	for(var i=0;i < baggagePreferences[0].preferences.length;i++)
            	{
            		for(var j=0;j < ancillaryRemoteService.metaData.ondPreferences.length;j++)
            		{
            			var ondId = angular.copy(ancillaryRemoteService.metaData.ondPreferences[i].ondId);
            			if(ondId !== baggagePreferences[0].preferences[i].preferanceId && tempOndPref[baggagePreferences[0].preferences[i].preferanceId] === undefined)
            		    {
            				var rphArray = ["0"];
            				var ondPrefObject =
            					{
            						flightSegmentRPH : rphArray,
            						ondId : baggagePreferences[0].preferences[i].preferanceId
            					}
            				tempOndPref[baggagePreferences[0].preferences[i].preferanceId] = "0"
            				ancillaryRemoteService.metaData.ondPreferences.push(ondPrefObject);
            		    }
            		}
            	}
            }

            function addSegmentToSummary(summary, addedSegments, totalSegmentScope)
            {
            		for(var j=0;j < addedSegments.length;j++)
            		{
            		  for(var i=0;i < totalSegmentScope[0].preferences.length;i++)
                	  {
                		 if(totalSegmentScope[0].preferences[i].preferanceId !== addedSegments[j].scope.ondId)
                		 {
                			var passengers = totalSegmentScope[0].preferences[i].passengers;
                			var segments = totalSegmentScope[0].preferences[i].segments || {};
                			if(summary[getSegmentCombination(addedSegments[j].scope,segments)] === undefined)
                				summary[getSegmentCombination(addedSegments[j].scope,segments)] = setAnciSummery(passengers,appMode);
                		 }
                	   }
            		}
            	return summary;
            }

            function getSegmentCombination(scope,segments){
                var combination = ""
                if(segments.length) {
                    if (scope.scopeType == "OND") {
                        combination = segments[0].from + " " + toTranslation + " " + segments[segments.length-1].to;
                    } else if (scope.scopeType == "SEGMENT") {
                        combination = segments[0].from + " " + toTranslation + " " + segments[0].to;
                    } else if (scope.scopeType == "AIRPORT") {
                        combination = segments[0].from;
                    } else if (scope.scopeType == "INSURANCE") {
                        combination = "ALL SEGMENTS"
                    }
                }
                return combination;

            }
            /*
            get flight detail by passing OndID or flightSegmentRPH
            @param scope : scope data
             */
            function getFlightDetails(scope){
                var flightData = {};
                if(scope.scopeType == "OND"){
                    var ondId = scope.OndId || scope.ondId;
                    flightData = ancillaryService.getOndDetails(ondId);
                }else if(scope.scopeType == "SEGMENT"){
                    flightData = ancillaryService.getFlightDetails(scope.flightSegmentRPH);
                }else if(scope.scopeType == "AIRPORT"){
                    flightData[0] = {}
                    flightData[0].from = scope.airportCode;
                }else if(scope.scopeType ==  "INSURANCE"){
                    flightData[0] = {}
                }
                return flightData;
            }
            /*
            produce segment key by combining airport code
            @param segment : segment with airport codes
             */
            function getsegmentKey(segment){
                var segmentKey = "";
                if(segment != undefined){
                    if(segment[0] != undefined) {
                        if (segment[0].from != undefined && segment[0].to != undefined) {
                            segmentKey = segment[0].from + ' to ' + segment[0].to
                        } else if (segment[0].from != undefined) {
                            segmentKey = segment[0].from
                        } else {
                            segmentKey = "ALL SEGMENTS"
                        }
                    }
                }
                return segmentKey;
            }

            /** Set passengers for the ancillary model. This should fire incase the passenger details have changed
             * */
            function setPassengers(){
                var ancillaryModel = ancillaryService.getAncillaryModel();
                var passengerList = passengerService.getPassengerDetailsForAnci();
                var passengerMap = _.object(_.map(passengerList, function(passenger){
                    return [passenger.paxSequence, passenger];
                }));

                angular.forEach(ancillaryModel, function(ancillary){

                    if (!_.isUndefined(ancillary) && !_.isEmpty(ancillary.scopes)) {
                        var scopePrefs = ancillary.scopes[0].preferences;

                        angular.forEach(scopePrefs, function(preference){
                            if(preference.passengers){
                                angular.forEach(preference.passengers, function(passenger){
                                    //pax.passenger = passengerList
                                    passenger.passenger = passengerMap[passenger.passengerRph];
                                });
                            }
                        });
                    }
                });
            }

            function getOndSequence(ondSelection, segmentRPH){
            	if (ondSelection != undefined && ondSelection != null){
            		for (var i=0;i<ondSelection.length;i++){
            			if (ondSelection[i].segmentRPH.indexOf(segmentRPH) != -1){
            				return ondSelection[i].ondSequence;
            			}
            		}
            	}
            	return -1;
            }

            function getSegmentSequence(ondDetails, ondId){
            	if (ondDetails != undefined && ondDetails != null){
            		for (var i=0;i<ondDetails.length;i++){
            			if (ondDetails[i].ondId == ondId){
            				if (ondDetails[i].flightSegmentRPH != null && ondDetails[i].flightSegmentRPH.length > 0){
            					return ondDetails[i].flightSegmentRPH[0];
            				}
            			}
            		}
            	}
            	return -1;
            }

            this.continueToPayment = function () {
                var unselectedAnci = [];
                var availableAnci = _this.anciOptions;
                var selectedAnci = ancillaryService.getAncillaryModel();
                var selectedConfirmAnci = [];

                var alertForBundleFreeServices = false;
                var bundleFareNotSelectedFreeAnci = new Object();

                var bundleFarePopupEnabled = constantFactory.AppConfigStringValue('isBundleFarePopupEnabled');
                if (bundleFarePopupEnabled == 'true'){
                	var ondSelection = commonLocalStorageService.getData('OND_FARE_SELECTION');
                    if (ondSelection != null){
                    	for (var i=0;i<ondSelection.length;i++){
                        	if (ondSelection[i].selectedFareClass.fareClassType == 'BUNDLE'){
                        		var freeAnciServices = ondSelection[i].selectedFareClass.freeServices;

                        		for (var j=0;j<freeAnciServices.length;j++){
                        			var ancillary = freeAnciServices[j];

                        			if (ancillary == 'SEAT_MAP'){
                        				ancillary = 'SEAT';
                        			} else if (ancillary == 'FLEXI_CHARGES'){
                        				ancillary = 'FLEXIBILITY';
                        			} else if (ancillary == 'AIRPORT_SERVICE') {
                        				ancillary = 'SSR_AIRPORT'
                        			}


                        			var currentAnci = selectedAnci[ancillary];
                        			if (currentAnci){
                        				var anciScopes = currentAnci.scopes[0];
                            			var anciPreferences = anciScopes.preferences;
                            			var anciScopeType = anciScopes.scopeType;

                            			for (var l=0;l<anciPreferences.length;l++){

                            				var anciSelectionForOnd = null;

                            				if (anciScopeType == 'OND') {
                            					if (ondSelection[i].ondSequence == getOndSequence(ondSelection, getSegmentSequence(commonLocalStorageService.getOndDetails(), anciPreferences[l].preferanceId))){
                            						anciSelectionForOnd = anciPreferences[l];
                            					} else {
                            						continue;
                            					}
                            				} else if (anciScopeType == 'SEGMENT') {
                            					if (ondSelection[i].ondSequence == getOndSequence(ondSelection, anciPreferences[l].preferanceId)) {
                            						anciSelectionForOnd = anciPreferences[l];
                            					} else {
                            						continue;
                            					}
                            				} else if (anciScopeType == 'AIRPORT') {
                            					if (ondSelection[i].ondSequence == getOndSequence(ondSelection, anciPreferences[l].preferanceId.flightSegmentRPH)) {
                            						anciSelectionForOnd = anciPreferences[l];
                            					} else {
                            						continue;
                            					}
                            				} else {
                            					anciSelectionForOnd = anciPreferences[l];
                            				}

                                			if (anciSelectionForOnd != undefined && anciSelectionForOnd != null){
                                				if (ancillary == 'FLEXIBILITY'){
                                    				if (!anciSelectionForOnd.selectedItems.selected){
                                    					alertForBundleFreeServices = true;
                                    					bundleFareNotSelectedFreeAnci[ancillary] = 'Flexibility';
                                    				}
                                    			} else {
                                    				var paxList = anciSelectionForOnd.passengers;

                                        			for (var k=0;k<paxList.length;k++){
                                        				var pax = paxList[k];
                                        				if (pax != undefined){
                                        					var paxWiseSelectItem = pax.selectedItems;
                                        					var quantity = paxWiseSelectItem.quantity;
                                            				if (paxWiseSelectItem == undefined || Object.keys(paxWiseSelectItem).length === 0 ){
                                            					alertForBundleFreeServices = true;
                                            					if (ancillary == 'SEAT'){
                                            						bundleFareNotSelectedFreeAnci[ancillary] = 'Seat';
                                            					}else if (ancillary == 'SSR_AIRPORT'){
                                            						bundleFareNotSelectedFreeAnci[ancillary] = 'Airport Services';
                                            					}else if (ancillary == 'BAGGAGE'){
                                            						bundleFareNotSelectedFreeAnci[ancillary] = 'Baggage';
                                            					}else if (ancillary == 'MEAL'){
                                            						bundleFareNotSelectedFreeAnci[ancillary] = 'Meal';
                                            					}
                                            				}
                                        				}
                                        			}
                                    			}
                                			}
                            			}
                        			}

                        		}
                        	}
                        }
                    } else if (modifyReservationService != null &&  modifyReservationService.getSelectedNewFareClass() != null) {
                    	var selectedFareClassObj =  modifyReservationService.getSelectedNewFareClass();
                    	if (selectedFareClassObj.fareClassType == 'BUNDLE'){
                    		var freeAnciServices = selectedFareClassObj.freeServices;

                    		for (var j=0;j<freeAnciServices.length;j++){
                    			var ancillary = freeAnciServices[j];

                    			if (ancillary == 'SEAT_MAP'){
                    				ancillary = 'SEAT';
                    			} else if (ancillary == 'FLEXI_CHARGES'){
                    				ancillary = 'FLEXIBILITY';
                    			} else if (ancillary == 'AIRPORT_SERVICE') {
                    				ancillary = 'SSR_AIRPORT'
                    			}


                    			var currentAnci = selectedAnci[ancillary];

                    			if (currentAnci){
                    				var anciScopes = currentAnci.scopes[0];
                        			var anciPreferences = anciScopes.preferences;

                        			for (var l=0;l<anciPreferences.length;l++){
                        				var anciSelectionForOnd = anciPreferences[l];


                            			if (anciSelectionForOnd != undefined){
                            				if (ancillary == 'FLEXIBILITY'){
                                				if (!anciSelectionForOnd.selectedItems.selected){
                                					alertForBundleFreeServices = true;
                                					bundleFareNotSelectedFreeAnci[ancillary] = 'Flexibility';
                                				}
                                			} else {
                                				var paxList = anciSelectionForOnd.passengers;

                                    			for (var k=0;k<paxList.length;k++){
                                    				var pax = paxList[k];
                                    				if (pax != undefined){
                                    					var paxWiseSelectItem = pax.selectedItems;
                                    					var quantity = paxWiseSelectItem.quantity;
                                        				if (paxWiseSelectItem == undefined || Object.keys(paxWiseSelectItem).length === 0 ){
                                        					alertForBundleFreeServices = true;
                                        					if (ancillary == 'SEAT'){
                                        						bundleFareNotSelectedFreeAnci[ancillary] = 'Seat';
                                        					}else if (ancillary == 'SSR_AIRPORT'){
                                        						bundleFareNotSelectedFreeAnci[ancillary] = 'Airport Services';
                                        					}else if (ancillary == 'BAGGAGE'){
                                        						bundleFareNotSelectedFreeAnci[ancillary] = 'Baggage';
                                        					}else if (ancillary == 'MEAL'){
                                        						bundleFareNotSelectedFreeAnci[ancillary] = 'Meal';
                                        					}
                                        				}
                                    				}
                                    			}
                                			}
                            			}
                        			}


                    			}

                    		}
                    	}
                    }
                }



                if (alertForBundleFreeServices){
                	var bundleFareNotSelectedFreeAnciArray = $.map(bundleFareNotSelectedFreeAnci, function(value, index) {
                        return [value];
                    });

                    var xxx = bundleFareNotSelectedFreeAnciArray.join(", ");
                    var content = 'The fare you have selected includes the following extras: ' + xxx + '. Please make your selection to continue.';
                    popupService.alert($sce.trustAsHtml(content));
                } else {
                	anciSelectionConfirm();
                }


                function anciSelectionConfirm (){

	                angular.forEach(availableAnci, function(anci, key){
	                    if(anci.available){
	//                        console.log(key);
	                        if(selectedAnci[key]){
	                            if(!_.isUndefined(selectedAnci[key].scopes[0]) && !isAncilarySelected(selectedAnci[key].scopes[0].preferences)){
	                                unselectedAnci.push(key)
	                            }else{
	                            	selectedConfirmAnci.push(key)
	                            }
	                        }else{
	                            unselectedAnci.push(key)
	                        }
	                    }
	                });

	                var isValidMEDA = ancillaryService.validateMedicalAncillary();

	                if(_.isEmpty(unselectedAnci) && isValidMEDA){
	                    continuePaymentFlow();
	                }else if(!isValidMEDA){
	                    popupService.confirm($sce.trustAsHtml("MEDA Error!"), function(){});
	                }else{
	                    //Alert users of the anci they haven't selected
	                    unselectedAnci = _.map(unselectedAnci, function(anciType){
	                        if(selecteLanguage == 'en') {
	                            return availableAnci[anciType].optionName.charAt(0).toUpperCase() + availableAnci[anciType].optionName.slice(1);
	                        }else{
	                            return availableAnci[anciType].optionNameTemp.charAt(0).toUpperCase() + availableAnci[anciType].optionNameTemp.slice(1);
	                        }
	                    });

	                    var unselectedAnciString = unselectedAnci.join(', ');

	                    var content = dialogTranslate.haveNot + ' ' + unselectedAnciString + '<br>' +dialogTranslate.continue;
	                    popupService.confirm($sce.trustAsHtml(content),
	                        function(){
	                            continuePaymentFlow();
	                        }
	                    );
	                }
                }

                function continuePaymentFlow(){
                    //TODO: transform the object to the ancillaries format
                	if(!_.isEmpty(selectedConfirmAnci)){
                    	ancillaryService.setSelectedConfirmAnci(selectedConfirmAnci);
                	}
                    ancillaryRemoteService.setAvailableAncillaryData(ancillaryService.getAncillaryRequestJson(),
                        function (data) {
                            IBEDataConsole.log("Setting available ancillary: " + data.success)

                            if (applicationService.getApplicationMode() == applicationService.APP_MODES.MODIFY_SEGMENT) {
                                modifyReservationService.getBalanceSummaryForModifySegmentAfterAnci(
                                    function (data) {
                                        quoteService.changeShowQuote();
                                        quoteService.setShowSummaryDrawer(true);
                                        if($rootScope.deviceType.isSmallDevice){
                                             $rootScope.hgt =  quoteService.getQuoteStatus() ? 427 : 46 ;
                                        }

                                    },
                                    function (error) {
                                        IBEDataConsole.log('SEGMENT MODIFICATION WITH ANCILLARY BALANCE SUMMERY ERROR');
                                    }
                                );
                            } else if (applicationService.getApplicationMode() == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                                modifyReservationService.getBalanceSummaryForAncillaryModification(
                                    function (data) {
                                        quoteService.changeShowQuote();
                                        quoteService.setShowSummaryDrawer(true);
                                        $location.hash('bottom');

                                        // call $anchorScroll()
                                        scrollService.scrollTo('bottom');
                                    },
                                    function (error) {
                                        IBEDataConsole.log('ANCILLARY MODIFICATION BALANCE SUMMERY ERROR');
                                    }
                                );
                            } else {
                                $state.go('payments',
                                    {
                                        lang: languageService.getSelectedLanguage(),
                                        currency: currencyService.getSelectedCurrency(),
                                        mode: applicationService.getApplicationMode()
                                    }
                                );
                            }

                        }, function (error) {
                            alertService.setPageAlertMessage(error);
                        }
                    );
                }
            };
            function backToLoadReservation(){
                var appState = 'modifyReservation'
                var urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),pnr:currentPnr.pnr,lastName:currentPnr.lastName,departureDate:currentPnr.departureDate}
                $state.go(appState, urlParams);
            }

            function getCheapestBundleFare(){
                var availableFares = commonLocalStorageService.getData('AVAILABLE_FARE_CLASSES');
                var bundleFareArray = [];
                if(availableFares !=null){
	                for(var i= 0 ; i<availableFares.length;i++){
	                    var cheapestBundle = [];
	                    for(var k = 0 ;k<availableFares[i].length;k++){
	                        if(availableFares[i][k].additionalFareFee){
	                            if(_this.segmentViceTotal[i]< availableFares[i][k].additionalFareFee){
	                                cheapestBundle.push(availableFares[i][k].fareTypeName);
	                            }
	                        }
	                    }
	                    if(cheapestBundle.length){
	                        bundleFareArray.push(cheapestBundle);
	                    }
	                }
                }
                return bundleFareArray;
            }

            this.loadModifyOptions = function (id) {
                var option = '';

                switch (id) {
                    case 'baggage' :
                        option = 'baggage';
                        break;
                    case 'seats' :
                        option = 'seats';
                        break;
                    case 'meals' :
                        option = 'meals';
                        break;
                    case 'automatic checkin' :
                        option = 'automatic_checkin';    
                        break;
                    case 'insurance' :
                        option = 'insurance';
                        break;
                    case 'flight services' :
                        option = 'services';
                        break;
                    case 'airport services' :
                        option = 'airport_services';
                        break;
                    case 'flexibility' :
                        option = 'flexibility';
                        break;
                    case 'Airport Transfer' :
                        option = 'airport_transfer';
                        break;
                }
                $rootScope.extraType = option;
                $location.path('/extras/'+option+'/'+languageService.getSelectedLanguage()+'/'+currencyService.getSelectedCurrency()+'/'+ applicationService.getApplicationMode());
            };

            init();
            function init() {
                appMode = applicationService.getApplicationMode();
                signoutServiceCheck.checkSignoutOperation();
                _this.appMode = appMode;
                if(_this.carrierCode === 'W5' && appMode === 'create'){
                 var passengerDetails = passengerService.getPassengerDetails();
                 var details=commonLocalStorageService.getData('QUOTE_DETAILS');
                 var passengerList=[];
                 var adultArray=[];
                 var childArray=[];
                 var infantArray=[];
                 var adultString="";
                 var childString="";
                 var infantString="";
                 for(var paxValue=0; paxValue<passengerDetails.paxList.length; paxValue++){
                     var category=passengerDetails.paxList[paxValue].category;
                     var name=passengerDetails.paxList[paxValue].firstName;
                     var sequence=passengerDetails.paxList[paxValue].paxSequence;
                     if(category === 'Adult' || category === 'adult')
                         adultArray.push(name);
                     else if(category === 'Child' || category === 'child')
                         childArray.push(name);
                     else if(category === 'Infant' || category === 'infant')
                         infantArray.push(name);
                 }
                 for(var index=0;index<adultArray.length;index++){
                     if(adultArray.length === 1){
                         adultString = "("+adultArray[index]+")";
                     }else  if(index === 0)
                         adultString = "("+adultArray[index]+",";
                     else
                         adultString += adultArray[index]+")";
                 }
                 for(var index=0;index<childArray.length;index++){
                     if(childArray.length === 1){
                         childString = "("+childArray[index]+")";
                     }else if(index === 0 )
                         childString = "("+childArray[index]+",";
                     else
                         childString += childArray[index]+")";
                 }
                 for(var index=0;index<infantArray.length;index++){
                     if(infantArray.length === 1){
                         infantString = "("+infantArray[index]+")";
                     }else if(index === 0)
                         infantString = "("+infantArray[index]+",";
                     else
                         infantString += infantArray[index]+")";
                 }

                 if(passengerDetails.paxList.length > 0){
                    passengerList.push({category:"Adult",name:adultString});
                    passengerList.push({category:"Child",name:childString});
                    passengerList.push({category:"Infant",name:infantString});
                    details.paxListt=passengerList;
                 }
                  commonLocalStorageService.setData('QUOTE_DETAILS',details);
               }
                angular.forEach(_this.anciOptions, function (val) {
                    val.ancillaryImageClass = null;
                    val.available = false;
                    val.currentSelection = [];
                });
                currentPnr = modifyReservationService.getReservationSearchCriteria();
                var reprotectStatus = commonLocalStorageService.getData("ancillaryReProtect");
                    if(reprotectStatus != undefined && appMode == applicationService.APP_MODES.MODIFY_SEGMENT){
                        if (!reprotectStatus.allAncillariesReProtected &&  !navigationService.getReprotectAlertSatet()) {
                            navigationService.setReprotectAlertSatet(true)
                            $translate('lbl_common_reprotect').then(function (msg) {
                                popupService.alert($sce.trustAsHtml(msg));
                            })
                        }
                    }

                window.scrollTo(0, 0);
                var segmentViceTotal = [];

                var p = applicationService.getApplicationMode();
                quoteService.setShowQuoteStatus(false);
                quoteService.setShowSummaryDrawer(true);
                /*
                 if(extrasService.getExtras() == undefined){
                 extrasModel = extrasFactory.getSectorModel();
                 }else{
                 extrasModel = extrasService.getExtras();
                 }
                 */

                var anciAvailability = {};
                var params = extrasService.setIniAncillaryReqData();
                _this.ancillariesPreferance = {};
                IBEDataConsole.log(params);
                loadingService.showPageLoading();

                $translate.onReady().then(function() {
                    var defaultCarrierCode = constantFactory.AppConfigStringValue("defaultCarrierCode");
                    ancillaryService.setAncillaryName('SEAT',$translate.instant('lbl_extra_seatsHeader'));
                    ancillaryService.setAncillaryName('MEAL',$translate.instant('lbl_extra_mealsHeader'));
                    ancillaryService.setAncillaryName('AUTOMATIC_CHECKIN',$translate.instant('lbl_extra_AutomaticCheckinHeader'));
                    ancillaryService.setAncillaryName('FLEXIBILITY',$translate.instant('lbl_extra_flexiHeader'));
                    ancillaryService.setAncillaryName('SSR_AIRPORT',$translate.instant('lbl_extra_ssrHeader'));
                    ancillaryService.setAncillaryName('SSR_IN_FLIGHT', $translate.instant('lbl_extra_services'));
                    ancillaryService.setAncillaryName('AIRPORT_TRANSFER',$translate.instant('lbl_extra_airportTransferHeader'));
                    ancillaryService.setAncillaryName('BAGGAGE',$translate.instant('lbl_extra_baggageHeader'));
                    ancillaryService.setAncillaryName('INSURANCE',$translate.instant('lbl_extra_insuranceHeader'));
                    ancillaryService.setAncillaryName('SSR_IN_FLIGHT',$translate.instant('lbl_extra_inFlightHeader'));

                    if($rootScope.deviceType.isMobileDevice){
                    	buttonLabels.SELECT_NOW = $translate.instant('lbl_extra_select');
                        buttonLabels.EDIT_SELECTION = $translate.instant('lbl_extra_edit');
                    }else{
                    	buttonLabels.SELECT_NOW = $translate.instant('lbl_extra_selectNow');
                        buttonLabels.EDIT_SELECTION = $translate.instant('lbl_extra_editSelection');
                    }

                    dialogTranslate.haveNot = $translate.instant('lbl_extras_dialogHaveNot');
                    dialogTranslate.continue = $translate.instant('lbl_extras_dialogContinue');

                    toTranslation = $translate.instant('msg_common_to');
                });

                // ancillaryRemoteService.initializeAncillary(iniAncillaryReqData,setAnciAvailabilityList,onAniError)
                ancillaryRemoteService.initializeAncillary(params, function (setAnciAvailabilityList) {

                    availableAcniList = extrasService.getAvailableAncyTypes(setAnciAvailabilityList.availableAncillariesResponse);
                    if(!availableAcniList.length){
                        if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                            //alertService.setAlert("Ancillaries not available for this segment", 'warning');
                        }
                    }
                    var reProtectableAncillaries;

                    ancillaryRemoteService.loadAvailableAncillaryData(availableAcniList, function (AnciFullDataSet) {
                        if (ancillaryRemoteService.metaData) {
                            ancillaryService.setMetaData(ancillaryRemoteService.metaData.ondPreferences);
                        }

                        ancillaryRemoteService.availableAncillary = AnciFullDataSet;

                        if (AnciFullDataSet.hasOwnProperty('SEAT')) {
                            IBEDataConsole.log(param);
                            var param = "SEAT";
                            var seatAvailablePreferances = [];

                            ancillaryRemoteService.loadSeatMapData(param, function (response) {

                                // If in MODIFY_SEGMENT flow remove the created object
                                if(!ancillaryRemoteService.defaultSeatsSet && appMode == applicationService.APP_MODES.MODIFY_SEGMENT){
                                    var anciModel = ancillaryService.getAncillaryModel();
                                    delete anciModel[param];
                                    ancillaryService.setAncillaryModel(anciModel);
                                }

                                response = ancillaryService.orderResponse(response);
                                _this.seatPreferance = getAnciPreferances("SEAT",response,appMode);
                                _this.seatPreferance.ancillary =  "SEAT";
                                //_this.seatPreferance = ancillaryService.getAncillary(param);
                                var summery = {};

                                angular.forEach(response, function (sector, key) {
                                    var flightSegment = sector.scope.flightSegmentRPH;
                                    var scopeType = sector.scope.scopeType;
                                    var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
                                    var seg = _.filter(_this.seatPreferance.scopes[0].preferences,function(item){
                                        return (sector.scope.flightSegmentRPH == item.preferanceId)
                                    });

                                    //var ancillaryPrefs = ancillaryService.getAncillaryPreference(param, scopeType, flightSegment, sectorDetails);
                                    //_this.seatPreferance = ancillaryService.getAncillary(param);
                                    if(_this.seatPreferance.scopes != undefined) {
                                        if (_this.seatPreferance.scopes.length > 0) {
                                            if(sector.items.cabinClass.length) {
                                                //_this.seatPreferance.scopes[0].preferences[key].preferanceId = flightSegment;
                                                var passengers = _this.seatPreferance.scopes[0].preferences[key].passengers
                                                var segments = _this.seatPreferance.scopes[0].preferences[key].segments || {};
                                                summery[getSegmentCombination(sector.scope, segments)] = setAnciSummery(passengers, appMode, 'SEAT');
                                                //select the preferance which seat map available
                                                if(!!seg) {
                                                    seatAvailablePreferances.push(seg[0])
                                                }
                                            }else{
                                                /*
                                                 var tempPref = angular.copy(_this.seatPreferance.scopes[0].preferences);
                                                 tempPref.splice(key,1);
                                                 _this.seatPreferance.scopes[0].preferences = tempPref;
                                                 */
                                            }
                                            _this.seatPreferance.total = setAnciTotal(_this.seatPreferance);
                                            if (isAncilarySelected(_this.seatPreferance.scopes[0].preferences)) {
                                                _this.anciOptions['SEAT'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                            } else {
                                                _this.anciOptions['SEAT'].buttonCaption = buttonLabels.SELECT_NOW;
                                            }
                                            segmentViceTotal[key] = (segmentViceTotal[key] != undefined) ? segmentViceTotal[key] + _this.seatPreferance.scopes[0].preferences[key].total: _this.seatPreferance.scopes[0].preferences[key].total;

                                        } else {
                                            _this.seatPreferance.total = 0;
                                        }
                                    }
                                });
                                var isSeatSelected = false;
                                angular.forEach(_this.seatPreferance.scopes[0].preferences, function(segment){
                                	angular.forEach(segment.passengers, function(pax){
                                		if(!_.isEmpty(pax.selectedItems)){
                                			isSeatSelected = true;
                                		}
                                	});
                                });
                                if((ancillaryService.getAnciState('SEAT') || isSeatSelected) && _this.seatPreferance.scopes[0].preferences){
                                    $translate('lbl_extra_seat_selected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('SEAT',msg)
                                    })
                                }else{
                                    $translate('lbl_extra_seat_not_selected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('SEAT',msg)
                                    })
                                }

                                // In the MODIFY_SEGMENT flow, the re-quote request is sent and the values are populated as received.
                                if(!ancillaryRemoteService.defaultSeatsSet && appMode == applicationService.APP_MODES.MODIFY_SEGMENT){
                                    var isSeatSelectedForModifySegment = false;
                                	modifyReservationService.getDefaultAncillariesOnModifyReservation(
                                        function(requotedResponse){
                                            angular.forEach(requotedResponse.passengers, function(passenger){
                                                var currentPaxRph = passenger.passengerRph;
                                                if(passenger.selectedAncillaries.SEAT){
                                                    angular.forEach(passenger.selectedAncillaries.SEAT.ancillaryScopes, function(seatScope){

                                                        var seatSelection = seatScope.ancillaries[0];
                                                        var scopeSegmentRph = seatScope.scope.flightSegmentRPH;


                                                        var selectedPref = _.find(_this.seatPreferance.scopes[0].preferences,
                                                            function (preference) {
                                                                if(preference.preferanceId == scopeSegmentRph){
                                                                    return preference;
                                                                }
                                                            }
                                                        );

                                                        var selectedPaxForPref = _.find(selectedPref.passengers,
                                                            function(passenger){
                                                                if(passenger.passengerRph == currentPaxRph){
                                                                    return passenger;
                                                                }
                                                            }
                                                        );

                                                        if(seatSelection){
                                                            selectedPaxForPref.selectedItems = [seatSelection];
                                                            isSeatSelectedForModifySegment = true;
                                                        }
                                                    });
                                                }
                                            });

                                            angular.forEach(response, function (sector, key) {
                                                var passengers = _this.seatPreferance.scopes[0].preferences[key].passengers;
                                                var segments = _this.seatPreferance.scopes[0].preferences[key].segments || {};
                                                summery[getSegmentCombination(sector.scope, segments)] = setAnciSummery(passengers, appMode, 'SEAT');
                                            });

                                            _this.seatPreferance.total = setAnciTotal(_this.seatPreferance); //calculating seats total

                                            var isAnciSelected = _.every(_.values(summery), function(v) {return (v == undefined)});
                                            setAnciSummerySellection(isAnciSelected,'SEAT',summery);

                                            ancillaryRemoteService.defaultSeatsSet = true;

                                            if(isSeatSelectedForModifySegment){
                                            	_this.anciOptions['SEAT'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                                $translate('lbl_extra_seat_selected').then(function (msg) {
                                                    ancillaryService.setAncillaryDescription('SEAT',msg)
                                                });
                                            }
                                        },function(error){
                                            IBEDataConsole.log('LOAD REQUOTE ANCILLARY ERROR');
                                        }
                                    );
                                }

                                if(!ancillaryRemoteService.defaultSeatsSet && appMode == applicationService.APP_MODES.CREATE_RESERVATION ){
                                    ancillaryRemoteService.getDefaultSeats({transactionId: AnciFullDataSet.transactionId},
                                        function success(defaultSeatResponse){


                                            _this.seatPreferance.scopes[0].preferences = seatAvailablePreferances;

                                            if(!ancillaryRemoteService.defaultSeatsSet){
                                                //setting default seats
                                                if(!ancillaryService.getAnciState('SEAT')) {
                                                    $translate('lbl_extra_seat_default').then(function (msg) {
                                                        ancillaryService.setAncillaryDescription('SEAT',msg)
                                                    })
                                                }

                                                try{
                                                    var isOnlyPreferredSeatSelected = false;
                                                    angular.forEach(defaultSeatResponse.ancillaryReservation.passengers, function(passenger){
                                                        var currentPaxRph = passenger.passengerRph;
                                                        angular.forEach(passenger.ancillaryTypes[0].ancillaryScopes, function(scope){

                                                            var defaultSeat = scope.ancillaries[0];
                                                            var segmentID = scope.scope.flightSegmentRPH;
                                                            var seatPrefs = _this.seatPreferance;

                                                            var selectedPref = _.find(seatPrefs.scopes[0].preferences,
                                                                function (preference) {
                                                                    if(preference.preferanceId == segmentID){
                                                                        return preference;
                                                                    }
                                                                });

                                                            var selectedPaxForPref = _.find(selectedPref.passengers,
                                                                function(passenger){
                                                                    if(passenger.passengerRph == currentPaxRph){
                                                                        return passenger;
                                                                    }
                                                                });


                                                            if(defaultSeat && _.isEmpty(selectedPaxForPref.selectedItems)){
                                                                selectedPaxForPref.selectedItems = [defaultSeat];
                                                                isOnlyPreferredSeatSelected = true;
                                                            }
                                                        });
                                                    });
                                                    ancillaryRemoteService.defaultSeatsSet = true;
                                                    ancillaryService.setAnciState('SEAT', true);
                                                    if (isOnlyPreferredSeatSelected) {
                                                        $translate('lbl_extra_seat_preferred').then(function (msg) {
                                                            ancillaryService.setAncillaryDescription('SEAT', msg)
                                                        })
                                                    }
                                                }catch(e){
                                                    IBEDataConsole.log("Unable to set seat defaults");
                                                    IBEDataConsole.log(e);
                                                }
                                            }

                                            angular.forEach(response, function (sector, key) {
                                                var passengers = _this.seatPreferance.scopes[0].preferences[key].passengers;
                                                var segments = _this.seatPreferance.scopes[0].preferences[key].segments || {};
                                                summery[getSegmentCombination(sector.scope, segments)] = setAnciSummery(passengers, appMode, 'SEAT');
                                            });

                                            _this.seatPreferance.total = setAnciTotal(_this.seatPreferance); //calculating seats total
//                                            console.log(_this.seatPreferance);
                                            var isAnciSelected =    _.every(_.values(summery), function(v) {return ( !_.isUndefined(v.summaryText) && !_.isEmpty(v.summaryText) )});
                                            setAnciSummerySellection(isAnciSelected,'SEAT',summery);

                                            //Get total after seats are set
                                            ancillaryRemoteService.setAvailableAncillaryData(ancillaryService.getAncillaryRequestJson(),
                                                function (data) {
                                                    ancillaryRemoteService.reservationAnsiSummary(ancillaryService.getAncillaryRequestJson(), function (response) {
                                                            if(applicationService.getApplicationMode()==='create'){
                                                                ancillaryService.updateFareQuoteData(response);
                                                            }
                                                        },
                                                        function (error) {
                                                            alertService.setPageAlertMessage(error);
                                                            self.isRewardsLoading = false;
                                                        });
                                                }, function (error) {
                                                    IBEDataConsole.log('LOAD AVAILABLE ANCILLARY ERROR');
                                                });
                                        },
                                        function failed(response){
                                            var isAnciNotSelected =    _.every(_.values(summery), function(v) {return (v == undefined)});
                                            if(!!summery) {
                                                setAnciSummerySellection(isAnciNotSelected, 'SEAT', summery);
                                            }
                                        }
                                    );
                                }else if(!$window.localStorage.getItem('MOD_SEG_INIT')) {
                                    angular.forEach(response, function (sector, key) {
                                        var passengers = _this.seatPreferance.scopes[0].preferences[key].passengers;
                                        var segments = _this.seatPreferance.scopes[0].preferences[key].segments || {};
                                        summery[getSegmentCombination(sector.scope, segments)] = setAnciSummery(passengers, appMode, 'SEAT');
                                    });

                                    _this.seatPreferance.total = setAnciTotal(_this.seatPreferance); //calculating seats total
//                                    console.log(_this.seatPreferance);
                                    var isAnciSelected =    _.every(_.values(summery), function(v) {return ( !_.isUndefined(v.summaryText) && !_.isEmpty(v.summaryText) )});
                                    setAnciSummerySellection(isAnciSelected,'SEAT',summery);
                                }

                            }, function (error) {

                            });
                        }else{
/*
                            var anciModel = ancillaryService.getAncillaryModel();
                            if(anciModel != undefined) {
                                delete anciModel['SEAT'];
                                ancillaryService.setAncillaryModel(anciModel);
                            }
*/

                        }

                        if (AnciFullDataSet.hasOwnProperty('MEAL')) {
                            var param = "MEAL";
                            ancillaryRemoteService.loadMealList(param, function (response) {
                            	if(appMode == applicationService.APP_MODES.CREATE_RESERVATION) {
	                            	angular.forEach(response, function (sector, sectorKey) {
	                            		var availableMealsList = [];
	                                	angular.forEach(sector.items, function (item, index) {
	                            			if(item.availableMeals > 0){
	                            				availableMealsList.push(item);
	                            			}
	                                	});
	                                	response[sectorKey].items = availableMealsList;
	                                });
                            	}
                                _this.mealPreferance = getAnciPreferances("MEAL",response,appMode);
                                ancillaryService.initilizeInventory("MEAL", _this.mealPreferance.scopes[0].preferences, response);

                                if(_this.mealPreferance.scopes.length> 0 && _this.mealPreferance.scopes[0].preferences){
                                  angular.forEach(_this.mealPreferance.scopes[0].preferences, function(segment, segmentIndex){
                                    var passengers = segment.passengers

                                    var secFilter = _.filter(response, function(sector){
                                      if(sector.scope.flightSegmentRPH == segment.preferanceId){
                                        return true;
                                      }
                                      return false;
                                    });
                                    var sector = (secFilter && secFilter.length> 0)? secFilter[0]:{};
                                    if(appMode == applicationService.APP_MODES.MODIFY_SEGMENT && $window.localStorage.getItem('MOD_SEG_INIT')){
                                        angular.forEach(segment.passengers, function(passenger, paxIndex){
                                          var passengerAssignableItems ={};

                                          angular.forEach(passenger.selectedItems, function(meal, mealCode){
                                            var selectedMeal = _.filter(sector.items, function(item){
                                                return (item.mealCode === mealCode);
                                              });
                                            meal.mealCode = mealCode;
                                            var allowedQty =  ancillaryService.adjustInventory("MEAL",
                                            _this.mealPreferance.scopes[0].preferences, segmentIndex,
                                                paxIndex,{
                                                  oldCount: 0,
                                                  newCount: meal.quantity,
                                                  meal:meal
                                                });

                                            if(allowedQty > 0){
                                              meal.quantity = allowedQty;
                                                meal.selectedItemTotal = Number(meal.quantity)* Number(meal.amount);
                                                var passengerAssignableItem = {};
                                                    passengerAssignableItem[mealCode] = meal;
                                                    angular.extend(passengerAssignableItems, passengerAssignableItem);
                                            }
                                          });
                                          passenger.selectedItems = passengerAssignableItems;
                                        });
                                        $window.localStorage.removeItem('MOD_SEG_INIT');
                                    }
                                  });


                                }

                                function updateSummaryForMeal(){
                                  var summery = {};

                                  angular.forEach(response, function (sector, key) {
                                      var flightSegment = sector.scope.flightSegmentRPH;
                                      var scopeType = sector.scope.scopeType;
                                      var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
                                      var selectedRPH = sector.scope.flightSegmentRPH;

                                      if (_this.mealPreferance.scopes.length > 0) {
                                           var passengers = _this.mealPreferance.scopes[0].preferences[key].passengers
                                           var segments = _this.mealPreferance.scopes[0].preferences[key].segments || {};

                                          _this.mealPreferance.total = setAnciTotal(_this.mealPreferance);
                                          if(isAncilarySelected(_this.mealPreferance.scopes[0].preferences)){
                                              _this.anciOptions['MEAL'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                          }else{
                                              _this.anciOptions['MEAL'].buttonCaption = buttonLabels.SELECT_NOW;
                                          }
                                          summery[getSegmentCombination(sector.scope,segments)] = setAnciSummery(passengers,appMode);
                                          segmentViceTotal[key] = (segmentViceTotal[key] != undefined) ? segmentViceTotal[key] + _this.mealPreferance.scopes[0].preferences[key].total: _this.mealPreferance.scopes[0].preferences[key].total

                                      } else {
                                          _this.mealPreferance.total = 0;
                                      }
                                  });


                                  var isAnciSelected =    _.every(_.values(summery), function(v) {return ( !_.isUndefined(v.summaryText) && !_.isEmpty(v.summaryText) )});
                                  setAnciSummerySellection(isAnciSelected,'MEAL',summery);
                                  var isOnlyPrefferedMeals = true;
                                  angular.forEach(_this.mealPreferance.scopes[0].preferences, function (segment) {
                                      angular.forEach(segment.passengers, function (pax) {
                                          angular.forEach(pax.selectedItems, function (selectedItems) {
                                              if (selectedItems.isCustomerPreferredMeal == undefined) {
                                                  isOnlyPrefferedMeals = false;
                                              }
                                          })
                                      })
                                  });
                                  if(isAnciSelected){
                                      if (isOnlyPrefferedMeals){
                                          $translate('lbl_extra_meals_preffered').then(function (msg) {
                                              ancillaryService.setAncillaryDescription('MEAL', msg)
                                          })
                                      }else {
                                          $translate('lbl_extra_meals_selected').then(function (msg) {
                                              ancillaryService.setAncillaryDescription('MEAL',msg)
                                          })
                                      }
                                  }else{
                                      $translate('lbl_extra_meals_not_selected').then(function (msg) {
                                          ancillaryService.setAncillaryDescription('MEAL',msg)
                                      })
                                  }
                                }
                                var isSelectedItems = true;
                                angular.forEach(_this.mealPreferance.scopes[0].preferences, function(segment){
                                	angular.forEach(segment.passengers, function(pax){
                                		if(_.isEmpty(pax.selectedItems)){
                                			isSelectedItems = false;
                                		}
                                	})
                                });
                                
                                if(signInService.getLoggedInStatus() && !isSelectedItems && appMode != applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                                	ancillaryService.setCustomerPreferredAnci("MEAL", _this.mealPreferance.scopes[0].preferences, response, function(){
                                        updateSummaryForMeal();
                                    });
                                } else {
                                	updateSummaryForMeal();
                                }


                            }, function (error) {
                            });
                        }else{
/*
                            var anciModel = ancillaryService.getAncillaryModel();
                            if(anciModel != undefined) {
                                delete anciModel['MEAL'];
                                ancillaryService.setAncillaryModel(anciModel);
                            }
*/

                        }
						if (AnciFullDataSet.hasOwnProperty('AUTOMATIC_CHECKIN')) {
						    var param = "AUTOMATIC_CHECKIN";
						    ancillaryRemoteService.loadAutomaticCheckinData(param, function(response) {
							_this.automaticCheckinPreferance = getAnciPreferances("AUTOMATIC_CHECKIN", response, appMode);
							angular.forEach(response, function(sector, key) {
							    var flightSegment = sector.scope.flightSegmentRPH;
							    var scopeType = sector.scope.scopeType;
							    var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
							    var selectedRPH = sector.scope.flightSegmentRPH;
							    if (_this.automaticCheckinPreferance.scopes.length > 0) {
								_this.automaticCheckinPreferance.total = setAnciTotal(_this.automaticCheckinPreferance);
								var passengers = [];
								passengers[0] = {};
								passengers[0].selectedItems = _this.automaticCheckinPreferance.scopes[0].preferences[0].selectedItems;
								if (isAncilarySelected(_this.automaticCheckinPreferance.scopes[0].preferences)) {
								    _this.anciOptions['AUTOMATIC_CHECKIN'].buttonCaption = buttonLabels.EDIT_SELECTION;
								} else {
								    _this.anciOptions['AUTOMATIC_CHECKIN'].buttonCaption = buttonLabels.SELECT_NOW;
								}
			
							    } else {
								_this.automaticCheckinPreferance.total = 0;
			
							    }
			
							});
							$translate('lbl_autocheckin_description').then(function (msg) {
			                         ancillaryService.setAncillaryDescription('AUTOMATIC_CHECKIN',msg)
			                     })
						    }, function(error) {});
						}
                        if (AnciFullDataSet.hasOwnProperty('FLEXIBILITY')) {
                            var param = "FLEXIBILITY";
                            ancillaryRemoteService.loadFlexiData(param, function (response) {
                                _this.flexiPreferance = getAnciPreferances("FLEXIBILITY",response,appMode)
                                var FlexibilitySegment = [];
                                var summery = {};

                                response = response.sort(function(a,b){
                                    if(a.scope.ondId > b.scope.ondId){
                                        return 1;
                                    }else if (a.scope.ondId < b.scope.ondId){
                                        return -1
                                    }else{
                                        return 0;
                                    }
                                });

/*                                response = _.sortBy(response, function(sector){
                                    return sector.scope.ondId;
                                });*/

                                angular.forEach(response, function (sector, key) {
                                    var flightSegment = sector.scope.flightSegmentRPH;
                                    var scopeType = sector.scope.scopeType;
                                    var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
                                    //_this.flexiPreferance = ancillaryService.getAncillaryFromResponse(param, response);
                                    FlexibilitySegment.push(sector.scope.ondId)
                                    if (_this.flexiPreferance.scopes.length > 0) {
                                        if (!!_this.flexiPreferance.scopes[0].preferences[key]) {
                                            var selected = _this.flexiPreferance.scopes[0].preferences[key].selectedItems
                                            if (selected == undefined) {
                                                _this.flexiPreferance.scopes[0].preferences[key].selectedItems = sector.items[0];
                                                if (sector.items[0].selected) {
                                                    _this.flexiPreferance.scopes[0].preferences[key].total = sector.items[0].price;
                                                } else {
                                                    _this.flexiPreferance.scopes[0].preferences[key].total = 0;
                                                }
                                            }

                                            _this.flexiPreferance.scopes[0].preferences[key].passengers = [];
                                            _this.flexiPreferance.scopes[0].preferences[key].defaultselection = sector.items[0].selected;


                                            if (isAncilarySelected(_this.flexiPreferance.scopes[0].preferences)) {
                                                _this.anciOptions['FLEXIBILITY'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                            } else {
                                                _this.anciOptions['FLEXIBILITY'].buttonCaption = buttonLabels.SELECT_NOW;
                                            }
                                            if (flightSegment != undefined) {
                                                _this.flexiPreferance.scopes[0].preferences[key].preferanceId = flightSegment;
                                            }
                                            flexiTotal();
                                            var passengers = [];
                                            passengers[0] = {}
                                            passengers[0].selectedItems = []
                                            if (_this.flexiPreferance.scopes[0].preferences[key].selectedItems.selected) {
                                                passengers[0].selectedItems[0] = _this.flexiPreferance.scopes[0].preferences[key].selectedItems;
                                            } else {
                                                passengers[0].selectedItems = undefined;
                                            }
                                            var segments = _this.flexiPreferance.scopes[0].preferences[key].segments || {};

                                            summery[getSegmentCombination(sector.scope, segments)] = setAnciSummery(passengers, appMode);
                                            segmentViceTotal[key] = (segmentViceTotal[key] != undefined) ? segmentViceTotal[key] + _this.flexiPreferance.scopes[0].preferences[key].total : _this.flexiPreferance.scopes[0].preferences[key].total;
                                        }


                                    } else {
                                        _this.flexiPreferance.total = 0;
                                    }
                                });
                                var isAnciSelected =    _.every(_.values(summery), function(v) {return ( !_.isUndefined(v.summaryText) && !_.isEmpty(v.summaryText) )});
                                setAnciSummerySellection(isAnciSelected,'FLEXIBILITY',summery);
                                ancillaryService.removeUnMatchPreference(_this.flexiPreferance.scopes, FlexibilitySegment);

                                if(isAnciSelected){
                                    $translate('lbl_extra_flexiSelected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('FLEXIBILITY',msg)
                                    })
                                }else{
                                    $translate('lbl_extra_flexiNotSelected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('FLEXIBILITY',msg)
                                    })
                                }

                            }, function (error) {
                            });
                        }else{
                            var anciModel = ancillaryService.getAncillaryModel();
                            if(anciModel != undefined) {
                                delete anciModel['FLEXIBILITY'];
                                ancillaryService.setAncillaryModel(anciModel);
                            }

                        }

                        if (AnciFullDataSet.hasOwnProperty('SSR_IN_FLIGHT')) {
                            var param = "SSR_IN_FLIGHT";
                            ancillaryRemoteService.loadInFlightServices(param, function (response) {
                                _this.ssrInFlightPreferance = getAnciPreferances("SSR_IN_FLIGHT",response,appMode);

                                $translate('lbl_extra_ssrInFlight').then(function (msg) {
                                    ancillaryService.setAncillaryDescription('SSR_IN_FLIGHT',msg)
                                });

                                if(appMode === applicationService.APP_MODES.ADD_MODIFY_ANCI){
                                    //Remove un matched preference
                                    _this.ssrInFlightPreferance.scopes[0].preferences = _.filter(_this.ssrInFlightPreferance.scopes[0].preferences, function(segment){
                                        var hasValidSegment = false;
                                        _.forEach(response, function(flight){
                                            if (flight.scope.flightSegmentRPH == segment.preferanceId){
                                                hasValidSegment = true;
                                            }
                                        });
                                        return hasValidSegment;
                                    });
                                }

                                var summery = {};

                                angular.forEach(response, function (sector, key) {
                                    var flightSegment = sector.scope.flightSegmentRPH;
                                    var scopeType = sector.scope.scopeType;
                                    var sectorDetails = ancillaryService.getFlightDetails(flightSegment);

                                    //_this.ssrInFlightPreferance = ancillaryService.getAncillary(param);
                                    if (_this.ssrInFlightPreferance.scopes.length > 0) {
                                        _this.ssrInFlightPreferance.scopes[0].preferences[key].preferanceId = flightSegment;
                                        _this.ssrInFlightPreferance.total = setAnciTotal(_this.ssrInFlightPreferance);
                                        if(isAncilarySelected(_this.ssrInFlightPreferance.scopes[0].preferences)){
                                            _this.anciOptions['SSR_IN_FLIGHT'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                        }else{
                                            _this.anciOptions['SSR_IN_FLIGHT'].buttonCaption = buttonLabels.SELECT_NOW;
                                        }
                                        var passengers = _this.ssrInFlightPreferance.scopes[0].preferences[key].passengers
                                        var segments = _this.ssrInFlightPreferance.scopes[0].preferences[key].segments || {};
                                        summery[getSegmentCombination(sector.scope,segments)] = setAnciSummery(passengers,appMode);
                                        segmentViceTotal[key] = (segmentViceTotal[key] != undefined) ? segmentViceTotal[key] + _this.ssrInFlightPreferance.scopes[0].preferences[key].total: _this.ssrInFlightPreferance.scopes[0].preferences[key].total;

                                    } else {
                                        _this.ssrInFlightPreferance.total = 0;
                                    }
                                });
                                var isAnciSelected =    _.every(_.values(summery), function(v) {return ( !_.isUndefined(v.summaryText) && !_.isEmpty(v.summaryText) )});

                                if(isAnciSelected){
                                    $translate('lbl_extra_ssrInFlight').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('SSR_IN_FLIGHT',msg)
                                    })
                                }else {
                                    $translate('lbl_extra_ssrInFlight').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('SSR_IN_FLIGHT', msg)
                                    })
                                }

                                setAnciSummerySellection(isAnciSelected,'SSR_IN_FLIGHT',summery);
                            }, function (error) {
                            });
                        }else{
/*
                            var anciModel = ancillaryService.getAncillaryModel();
                            if(anciModel != undefined) {
                                delete anciModel['SSR_IN_FLIGHT'];
                                ancillaryService.setAncillaryModel(anciModel);
                            }
*/

                        }

                        if (AnciFullDataSet.hasOwnProperty('SSR_AIRPORT')) {
                            var param = "SSR_AIRPORT";
                            ancillaryRemoteService.loadAirportServicesData(param, function (response) {
                                var summery = {};
                                if(ancillaryService.getAncillaryModel()['SSR_AIRPORT'] != undefined) {
                                    var selectedServices = ancillaryService.getAncillaryModel()['SSR_AIRPORT'];
                                    angular.forEach(response, function (sector, key) {
                                        _this.ssrAirportPreferance = ancillaryService.setSelectedAirportServiceName(sector.items, selectedServices);
                                    })
                                }else{
                                    _this.ssrAirportPreferance = ancillaryService.getAncillaryFromResponse("SSR_AIRPORT", response);
                                }

                                angular.forEach(response, function (sector, key) {
                                    var apCode = sector.scope.flightSegmentRPH + sector.scope.point + sector.scope.airportCode;
                                    if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                                        var prf = [];
                                        if(_this.ssrAirportPreferance.scopes[0].preferences[key] != undefined) {
                                            var selectedAirport = _.filter(_this.ssrAirportPreferance.scopes[0].preferences,function(item){
                                                var code = item.preferanceId.flightSegmentRPH + item.preferanceId.point + item.preferanceId.airportCode;
                                                return (code == apCode)
                                            })
                                            if(selectedAirport[0]){
                                                var passengers = selectedAirport[0].passengers
                                                var segKey = sector.scope.airportCode + ' (' + (sector.scope.point).toLowerCase() + ')';
                                                summery[segKey] = setAnciSummery(passengers, appMode);
                                            }

                                        }

                                    }else{
                                        var flightSegment = sector.scope.flightSegmentRPH;
                                        var scopeType = sector.scope.scopeType;
                                        var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
                                        var prefId = {};
                                        prefId.airportCode = sector.scope.airportCode;
                                        prefId.point = sector.scope.point;
                                        prefId.scopeType = sector.scope.scopeType;

                                        prefId.flightSegmentRPH = sector.scope.flightSegmentRPH;
                                        _this.ssrAirportPreferance = ancillaryService.getAncillaryFromResponse("SSR_AIRPORT", response);

                                        //_this.ssrAirportPreferance = ancillaryService.getAncillary(param);
                                        if (_this.ssrAirportPreferance.scopes.length > 0) {
                                            if(_this.ssrAirportPreferance.scopes[0].preferences[key] != undefined) {
                                                _this.ssrAirportPreferance.scopes[0].preferences[key].preferanceId = {}
                                                if (sector.scope.airportCode == _this.ssrAirportPreferance.scopes[0].preferences[key].segments[0].from) {
                                                    _this.ssrAirportPreferance.scopes[0].preferences[key].preferanceId = prefId;
                                                }
                                                var passengers = _this.ssrAirportPreferance.scopes[0].preferences[key].passengers
                                                var segments = _this.ssrAirportPreferance.scopes[0].preferences[key].segments || {};
                                                var segKey = getSegmentCombination(sector.scope, segments) + ' (' + (prefId.point).toLowerCase() + ')';
                                                summery[segKey] = setAnciSummery(passengers, appMode);
                                                segmentViceTotal[key] = (segmentViceTotal[key] != undefined) ? segmentViceTotal[key] + _this.ssrAirportPreferance.scopes[0].preferences[key].total: _this.ssrAirportPreferance.scopes[0].preferences[key].total;

                                            }
                                        }


                                    }


                                });
                                var isAnciSelected = _.some(summery,function(val){ return !_.isEmpty(val.summaryText);})
                                setAnciSummerySellection(isAnciSelected,'SSR_AIRPORT',summery);
                                if(_this.ssrAirportPreferance && _this.ssrAirportPreferance.scopes) {
                                    angular.forEach(_this.ssrAirportPreferance.scopes[0].preferences, function (pref, prefKey) {
                                        angular.forEach(pref.passengers, function (pass, pasKey) {
                                            var passType = pass.passenger.paxType || pass.passenger.type;
                                            angular.forEach(pass.selectedItems, function (item, itemKey) {
                                                var charge = item.amount || item.charges;
                                                if (charge != undefined || charge != null) {
                                                    for (var i = 0; i < charge.length; i++) {
                                                        var obj = charge[i];
                                                        if (charge[i].chargeBasis == passType.toUpperCase()) {
                                                            item.amount = charge[i].amount;
                                                        }
                                                        if(pass.passenger.infantSequance){
                                                            item.amount = charge[3].amount;
                                                        }
                                                    }
                                                }
                                            })

                                        })

                                    })
                                    if (_this.ssrAirportPreferance.scopes.length > 0) {
                                        _this.ssrAirportPreferance.total = setAnciTotal(_this.ssrAirportPreferance);
                                        if(isAnciSelected){
                                            _this.anciOptions['SSR_AIRPORT'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                        }else{
                                            _this.anciOptions['SSR_AIRPORT'].buttonCaption = buttonLabels.SELECT_NOW;
                                        }
                                    }else{
                                        _this.ssrAirportPreferance.total = 0;
                                    }
                                }
                                if(isAnciSelected){
                                    $translate('lbl_extra_ssr_airport_selected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('SSR_AIRPORT',msg)
                                    })
                                }else{
                                    $translate('lbl_extra_ssr_airport_not_selected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('SSR_AIRPORT',msg)
                                    })
                                }

                                });
                        }else{
/*
                            var anciModel = ancillaryService.getAncillaryModel();
                            if(anciModel != undefined) {
                                delete anciModel['SSR_AIRPORT'];
                                ancillaryService.setAncillaryModel(anciModel);
                            }
*/

                        }

                        /*
                         since passenger can assigned default baggage total should calculate
                         before move to baggage selection.
                         */
                        if (AnciFullDataSet.hasOwnProperty('BAGGAGE')) {

                            var setDefaultSelectedBaggage = function (preferences,defaultItem,items) {
                                // inset default weight in to each passenger model
                                if(!!preferences && !!preferences.passengers) {
                                    angular.forEach(preferences.passengers, function (pas, key) {
                                        if (pas.selectedItems[0] === undefined || Object.keys(pas.selectedItems).length == 0) {
                                                pas.selectedItems[0] = baggage;
                                                pas.selectedItems[0].price = items[defaultItem].price;
                                                pas.selectedItems[0].value = items[defaultItem].weight;
                                                pas.selectedItems[0].id = items[defaultItem].baggageId;
                                                pas.selectedItems[0].quantity = 1;
                                                pas.selectedItems[0].input = null;
                                        }
                                        var passanger = angular.copy(preferences.passengers[key]);
                                        preferences.passengers[key] = passanger;
                                    });
                                }
                            };

                            ancillaryRemoteService.loadBaggageData("BAGGAGE", function (response) {
//                                console.log(response);
                                var baggageSegmentId = [];
                                var summery = {};
                                var weightsList = {}

                                _this.baggagePreferance = getAnciPreferances("BAGGAGE",response,appMode);
                                if(ancillaryService.getAnciState('BAGGAGE')){
                                    $translate('lbl_extra_baggage_selected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('BAGGAGE',msg)
                                    })
                                }else{
                                    $translate('lbl_extra_baggage_not_selected').then(function (msg) {
                                        ancillaryService.setAncillaryDescription('BAGGAGE',msg)
                                    })
                                }

                                //When the baggage list is recived, the ancillary service object is created for each of the sectors.
                                angular.forEach(response, function (sector, key) {
                                    var flightSegment = sector.scope.flightSegmentRPH || sector.scope.ondId;
                                    var scopeType = sector.scope.scopeType;
                                    var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
                                    var appMode = applicationService.getApplicationMode();
                                    baggageSegmentId.push(sector.scope.ondId)
                                    var selectedRPH = sector.scope.flightSegmentRPH;

                                    var ancillaryPrefs = ancillaryService.getAncillaryPreference("BAGGAGE", scopeType, flightSegment, sectorDetails);
                                    //weightsList[key] = {}
                                    //weightsList[key].items = sector.items
                                    var defaultItem = _.findIndex(sector.items, {selected: true})

                                    if (appMode == applicationService.APP_MODES.CREATE_RESERVATION ) {
                                        if(defaultItem > -1 && !ancillaryService.getAnciState('BAGGAGE')){
                                            $translate('lbl_extra_baggage_default').then(function (msg) {
                                                ancillaryService.setAncillaryDescription('BAGGAGE',msg)
                                            })
                                            setDefaultSelectedBaggage(ancillaryPrefs, defaultItem, sector.items);
                                        }
                                    } else if (appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                                        var selectedBgSegment = _.filter(_this.baggagePreferance.scopes[0].preferences,{preferanceId:sector.scope.ondId})
                                        var hasItem = _.findIndex(selectedBgSegment[0].passengers,function(pas){
                                                        return (!pas.selectedItems && pas.selectedItems.length > 0)
                                                      })

                                    	if(hasItem == -1 && defaultItem > -1 && !ancillaryService.getAnciState('BAGGAGE')){
                                            $translate('lbl_extra_baggage_default').then(function (msg) {
                                                ancillaryService.setAncillaryDescription('BAGGAGE',msg)
                                            })
                                            _// this.baggagePreferance.scopes[0].preferences[key] = ancillaryPrefs;
                                            setDefaultSelectedBaggage(_this.baggagePreferance.scopes[0].preferences[key], defaultItem, sector.items);
                                        }
                                    }

                                    //_this.baggagePreferance = ancillaryService.getAncillary("BAGGAGE")
                                    if (_this.baggagePreferance.scopes.length > 0) {
                                    	var passengers;
                                    	 var segments;
                                        if(_this.baggagePreferance.scopes[0].preferences[key] != undefined){
                                        	for(var i = 0;i < _this.baggagePreferance.scopes[0].preferences.length;i++)
                                        	{
                                        		if(_this.baggagePreferance.scopes[0].preferences[i].preferanceId === flightSegment)
                                        		{
                                        			passengers = _this.baggagePreferance.scopes[0].preferences[i].passengers;
                                        			segments = _this.baggagePreferance.scopes[0].preferences[i].segments || {};
                                        			break;
                                        		}
                                        	}
                                           // _this.baggagePreferance.scopes[0].preferences[key].preferanceId = flightSegment;

                                            if(!!selectedBgSegment) {
                                                passengers = selectedBgSegment[0].passengers
                                                segments = selectedBgSegment[0].segments || {};
                                            }

                                            summery[getSegmentCombination(sector.scope,segments)] = setAnciSummery(passengers,appMode);
                                        }
                                        //_this.currentAncillary = ancillaryPrefs;
                                        if (isAncilarySelected(_this.baggagePreferance.scopes[0].preferences)) {
                                            _this.anciOptions['BAGGAGE'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                        } else {
                                            _this.anciOptions['BAGGAGE'].buttonCaption = buttonLabels.SELECT_NOW;
                                        }
                                        segmentViceTotal[key] = (segmentViceTotal[key] != undefined) ? segmentViceTotal[key] + _this.baggagePreferance.scopes[0].preferences[key].total: _this.baggagePreferance.scopes[0].preferences[key].total;

                                    }else{
                                        _this.baggagePreferance.total = 0;
                                    }
                                });

                                var isAnciSelected = _.every(_.values(summery), function(v) {return ( !_.isUndefined(v.summaryText) && !_.isEmpty(v.summaryText) )});
                                if(appMode === applicationService.APP_MODES.ADD_MODIFY_ANCI && response.length < _this.baggagePreferance.scopes[0].preferences.length)
                                {
                                 summery = addSegmentToSummary(summery,response,_this.baggagePreferance.scopes);
                                }

                                if(appMode === applicationService.APP_MODES.ADD_MODIFY_ANCI && ancillaryRemoteService.metaData.ondPreferences.length < _this.baggagePreferance.scopes[0].preferences.length)
                                {
                                    addOndPreferences(_this.baggagePreferance.scopes);
                                }
                                 setAnciSummerySellection(isAnciSelected,'BAGGAGE',summery);
                                 if(appMode === applicationService.APP_MODES.MODIFY_SEGMENT)
                                  ancillaryService.removeUnMatchPreference(_this.baggagePreferance.scopes, baggageSegmentId);
                                 if((appMode !== applicationService.APP_MODES.ADD_MODIFY_ANCI) || (response.length >= _this.baggagePreferance.scopes[0].preferences.length && ancillaryRemoteService.metaData.ondPreferences.length >= _this.baggagePreferance.scopes[0].preferences.length))
                                 _this.baggagePreferance.total = setAnciTotal(_this.baggagePreferance);
                            }, function (error) {

                            });
                        }else{
/*
                            var anciModel = ancillaryService.getAncillaryModel();
                            if(anciModel != undefined) {
                                delete anciModel['BAGGAGE'];
                                ancillaryService.setAncillaryModel(anciModel);
                            }
*/

                        }


                        if (AnciFullDataSet.hasOwnProperty('INSURANCE')) {
                            ancillaryRemoteService.retrieveInsuranceData("INSURANCE", function (response) {
                                _this.allInsurancePolicies = response;
                                ancillaryService.getAncillaryPreferenceForInsurance("INSURANCE", "ALL_SEGMENTS", _this.allInsurancePolicies);
                                _this.insurancePreferance = ancillaryService.getAncillary("INSURANCE");
                                var selectedPolicies = _this.insurancePreferance.scopes[0].preferences[0].selectedItems || [];
                                var insuranceTypes = _this.allInsurancePolicies;
                                    if(!selectedPolicies.length) {
                                        _this.insurancePreferance.scopes[0].preferences[0].selectedItems = []

                                        for (var i = 0; i < insuranceTypes.length; i++) {
                                            for (var j = 0; j < insuranceTypes[i].planGroups.length; j++) {
                                                for (var k = 0; k < insuranceTypes[i].planGroups[j].plans.length; k++) {
                                                    if (insuranceTypes[i].planGroups[j].plans[k].isSelected && insuranceTypes[i].planGroups[j].plans[k].defaultSelection ) {
                                                        var temp = {}
                                                        temp.id = insuranceTypes[i].planGroups[j].plans[k].policyID;
                                                        temp.quantity = 1;
                                                        temp.input = null;
                                                        temp.price = insuranceTypes[i].planGroups[j].plans[k].amount;
                                                        //temp.amount= insuranceTypes[i].amount;
                                                        _this.insurancePreferance.scopes[0].preferences[0].selectedItems.push(temp);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                if (_this.insurancePreferance.scopes.length > 0) {
                                    _this.insurancePreferance.total = setAnciTotal(_this.insurancePreferance);
                                    var passengers = [];
                                    passengers[0] = {}
                                    passengers[0].selectedItems = _this.insurancePreferance.scopes[0].preferences[0].selectedItems
                                    if(isAncilarySelected(passengers)){
                                        _this.anciOptions['INSURANCE'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                    }else{
                                        _this.anciOptions['INSURANCE'].buttonCaption = buttonLabels.SELECT_NOW;
                                    }
                                    //_this.anciOptions['INSURANCE'].currentSelection[getsegmentKey(getFlightDetails({scopeType : "INSURANCE"}))] = setAnciSummery(passengers,appMode);

                                }else{
                                    _this.insurancePreferance.total = 0;

                                }
                                var defaultPolicy = _this.insurancePreferance.scopes[0].preferences[0].selectedItems || []
                                if(defaultPolicy.length && ancillaryService.getAnciState('INSURANCE')){
                                    $translate('lbl_extra_insurance_selected').then(function (msg) {
                                        _this.anciOptions.INSURANCE.description = msg;
                                    })
                                }else if(defaultPolicy.length) {
                                    $translate('lbl_extra_insurance_default').then(function (msg) {
                                        _this.anciOptions.INSURANCE.description = msg;
                                    })
                                }else{
                                    $translate('lbl_extra_insurance_not_selected').then(function (msg) {
                                        _this.anciOptions.INSURANCE.description = msg;
                                    })

                                }


                                insuranceTotal();
                            }, function (error) {

                            })
                        }else{
/*
                            var anciModel = ancillaryService.getAncillaryModel();
                            if(anciModel != undefined) {
                                delete anciModel['INSURANCE'];
                                ancillaryService.setAncillaryModel(anciModel);
                            }
*/

                        }

                        if (AnciFullDataSet.hasOwnProperty('AIRPORT_TRANSFER')) {
                            var param = "AIRPORT_TRANSFER";

                            ancillaryRemoteService.loadAirportTransferData(param, function (response) {
                                _this.airpotTransferPerferences = getAnciPreferances("AIRPORT_TRANSFER",response,appMode);
                                var summery = {};


                                if(isAncilarySelected(_this.airpotTransferPerferences.scopes[0].preferences)){
                                        _this.anciOptions['AIRPORT_TRANSFER'].buttonCaption = buttonLabels.EDIT_SELECTION;
                                    }else{
                                        _this.anciOptions['AIRPORT_TRANSFER'].buttonCaption = buttonLabels.SELECT_NOW;
                                    }

                                angular.forEach(response, function (sector, key) {
                                    if (_this.airpotTransferPerferences.scopes.length > 0) {
                                        var passengers = _this.airpotTransferPerferences.scopes[0].preferences[key].passengers;
                                        var segments = _this.airpotTransferPerferences.scopes[0].preferences[key].segments || {};

                                        summery[getSegmentCombination(sector.scope,segments)] = setAnciSummery(passengers, appMode, 'AIRPORT_TRANSFER');
                                        segmentViceTotal[key] = (segmentViceTotal[key] != undefined) ? segmentViceTotal[key] + _this.airpotTransferPerferences.scopes[0].preferences[key].total: _this.airpotTransferPerferences.scopes[0].preferences[key].total;
                                    } else {
                                        _this.airpotTransferPerferences.total = 0;
                                    }
                                });
                                var isAnciSelected =    _.every(_.values(summery), function(v) {return ( !_.isUndefined(v.summaryText) && !_.isEmpty(v.summaryText) )});
                                setAnciSummerySellection(isAnciSelected,'AIRPORT_TRANSFER',summery);
                            })
                        }

                        ancillaryRemoteService.setAvailableAncillaryData(ancillaryService.getAncillaryRequestJson(),
                            function (data) {
                                ancillaryRemoteService.reservationAnsiSummary(ancillaryService.getAncillaryRequestJson(), function (response) {
                                       if(applicationService.getApplicationMode()==='create'){
                                           ancillaryService.updateFareQuoteData(response);
                                       }
                                    },
                                    function (error) {
                                        alertService.setPageAlertMessage(error);
                                        self.isRewardsLoading = false;
                                    });
                            }, function (error) {
                                IBEDataConsole.log('LOAD AVAILABLE ANCILLARY ERROR');
                            });
                        _this.contentLoaded = true;
                        if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                            modifyReservationService.getBalanceSummaryForAncillaryModification(
                                function (data) {
                                    quoteService.setShowQuoteStatus(true);
                                    quoteService.changeShowQuote();
                                    quoteService.setShowSummaryDrawer(true);
                                },
                                function (error) {
                                    IBEDataConsole.log('ANCILLARY MODIFICATION BALANCE SUMMERY ERROR');
                                }
                            );
                        }else if (applicationService.getApplicationMode() == applicationService.APP_MODES.MODIFY_SEGMENT) {
                            modifyReservationService.getBalanceSummaryForModifySegmentAfterAnci(
                                function (data) {
                                    if(!availableAcniList.ancillaries.length) {
                                        if(data.updatedChargeForDisplay.dueAmountToDisplay > 0 ) {
                                            $state.go('payments',
                                                {
                                                    lang: languageService.getSelectedLanguage(),
                                                    currency: currencyService.getSelectedCurrency(),
                                                    mode: applicationService.getApplicationMode()
                                                }
                                            );
                                        }else{
                                            backToLoadReservation();
                                        }
                                    }

                                    quoteService.setShowQuoteStatus(false);
                                    quoteService.changeShowQuote(false);
                                    quoteService.setShowSummaryDrawer(true);
                                    if(!availableAcniList.ancillaries.length) {
                                        if(data.updatedChargeForDisplay.dueAmountToDisplay > 0 ) {
                                            $state.go('payments',
                                                {
                                                    lang: languageService.getSelectedLanguage(),
                                                    currency: currencyService.getSelectedCurrency(),
                                                    mode: applicationService.getApplicationMode()
                                                }
                                            );
                                        }else{
                                            modifyReservationService.confirmModifyFlightSegments(
                                                function(data){

                                                    var minDate = modifyReservationService.getLatestFlightDepartureDate(data.reservationSegments);

                                                    if(minDate.date){
                                                        var dateParm = (minDate.date).slice(0,-9);
                                                        modifyReservationService.reloadReservation(dateParm);
                                                    }else{
                                                        modifyReservationService.reloadReservation();
                                                    }

                                                },
                                                function(error){
                                                    IBEDataConsole.log('ERROR ON CONFIRM MODIFY SEGMENT');
                                                }
                                            );
                                        }
                                    }

                                },
                                function (error) {
                                    IBEDataConsole.log('SEGMENT MODIFICATION WITH ANCILLARY BALANCE SUMMERY ERROR');
                                }
                            );
                        }
                        }, function (error) {
                        IBEDataConsole.log('LOAD AVAILABLE ANCILLARY ERROR');
                    });

                    extrasModel = extrasService.getExtras();
                    _this.sectorData = extrasModel;

                    angular.forEach(availableAcniList.ancillaries, function (val, key) {
                        var anci = _.filter(_this.anciOptions,function(item,index){
                            return val.type == index
                        })
                        if (!!anci){
                            // Set ancillaryImageClass by getting common css class.
                            _this.anciOptions[val.type].ancillaryImageClass = appConstants.ancillaryImageClasses[val.type] || appConstants.ancillaryImageClasses['DEFAULT'];
                            _this.anciOptions[val.type].available = true;
                        }
                    });

                    var extrasTotal = 0;
                    angular.forEach(_this.sectorData, function (sector, seckey) {
                        var currentSec = sector;
                        angular.forEach(currentSec, function (legs, legKey) {
                            extrasTotal = extrasTotal + legs.total;

                        })
                    });
                    _this.extrasTotalFare = extrasTotal;
                    extrasService.setExtrasTotal(extrasTotal);
                    _this.indivExtrasTotal = extrasService.calAddonTotalFare(extrasModel);
                    _this.tripType = Object.keys(extrasModel)[trip];
                    _this.showExtrasTotal = {index: sectorCount, section: _this.tripType};
                    _this.segmentViceTotal = segmentViceTotal;
                    loadingService.hidePageLoading();

                    if(availableAcniList.ancillaries.length === 0){
                        _this.continueToPayment()
                    }

                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);
                });

                setPassengers();
            }

        }];
    return controllers;
};
