/**
 * Created by Rinku on 03/07/18.
 */
var automaticCheckinController = function () {

    var controllers = {};

    controllers.automaticCheckinController = [
        "$scope", "$location", "stepProgressService", "$rootScope", "passengerService", "extrasFactory",
        "extrasService", "ancillaryService", "ancillaryRemoteService", "loadingService",
        "applicationService", "quoteService","languageService", "currencyService", "$state", "alertService",
        "modifyReservationService", "constantFactory", "commonLocalStorageService", "popupService", "navigationService","masterRemoteService", "$translate",

        function  ($scope, $location, stepProgressService, $rootScope, passengerService,extrasFactory,
                                    extrasService, ancillaryService, ancillaryRemoteService,
                                    loadingService,applicationService, quoteService, languageService, currencyService,
                                    $state,alertService,modifyReservationService, constantFactory,
                                    commonLocalStorageService, popupService, navigationService, masterRemoteService, $translate) {
    
            // For mobile menus            
            var self = this;
            self.accordionArray = [];
            self.currency = currencyService.getSelectedCurrency();
            stepProgressService.setCurrentStep('extras');

            var sectorCount = 0;
            var trip = 0;
            var extrasModel = {};
            var selectedTemp, unModifiedSectorData, journeyType,appMode,appState,urlParams,currentPnr;
            var autoCheckinPriceArray = {};

            var autoCheckin = {
                price: 0,
                value: 0,
                Id:null

            };
            
            this.nextSector = nextSector;
            this.backToExtras = backToExtras;
            this.skipServices = skipServices;
            this.backToHome = backToHome;
            
            var paxDetails = JSON.parse(sessionStorage.getItem('IBE.PAX_DETAIL'));  
            var ancillary = JSON.parse(sessionStorage.getItem('IBE.ANCILLARY')); 
            self.preferredSeat = {};
            self.seatPreferenceStatus = {};
            self.seatPreference = [];
            var seatPreferenceData = [];
          
        	self.seatPreference = setAncillarySeatPreferences(0);   
            
            self.passengersList  = paxDetails.paxList;
            self.passengerClicked = 1;
            
            //self.onCountryChange = onCountryChange;
            self.countryList = constantFactory.Countries();
            self.formatLabelCountry = formatLabelCountry;
            self.startsWith = startsWith;
            self.seatPreferenceStatus = false;
            
            self.sitTogetherTrigger = function($index){
            	
            	if(self.seatPreferenceStatus===false){
            		self.seatPreferenceStatus = true;
            		self.sitTogetherClicked = true;
            	}
            	else{
            		self.seatPreferenceStatus = false;
            		self.sitTogetherClicked = false;
            	}
            	
            	var temp = self.preferredSeat[0];
            	self.preferredSeat = {};            	
            	self.preferredSeat[0] = temp;
            	
            }
            
            self.setSalutation = function (salutation,$index) {    
            self.preferredSeat[$index] = {seat:salutation.titleCode};
            };
            
            $translate(['lbl_autocheckin_WindowSeat', 'lbl_autocheckin_AisleSeat', 'lbl_autocheckin_MiddleSeat']).then(function(seats) {
                self.salutationList = [{
                        "titleCode": "W",
                        "titleName": seats.lbl_autocheckin_WindowSeat
                    },
                    {
                        "titleCode": "A",
                        "titleName": seats.lbl_autocheckin_AisleSeat
                    },
                    {
                        "titleCode": "M",
                        "titleName": seats.lbl_autocheckin_MiddleSeat
                    }
                ]
            });
            
            function startsWith(state, viewValue) {
                if (viewValue === null || viewValue.trim() === "" ) {
                    return true;
                } else {
                    var value = viewValue.trim();
                    return state.substr(0, value.length).toLowerCase() == value.toLowerCase();
                }
            }
            
            function formatLabelCountry(model) {
                var tempN = self.countryList;
                if (tempN) {
                    for (var i = 0; i < tempN.length; i++) {
                        if (model === tempN[i].code) {
                            return tempN[i].name;
                            break;
                        }
                    }
                }
            }
            
            
            (function () {
                window.scrollTo(0, 0);
               
                extrasModel = extrasService.getExtras();
                quoteService.setShowSummaryDrawer(false);
                appMode = applicationService.getApplicationMode();
                self.appMode = appMode;
                currentPnr = modifyReservationService.getReservationSearchCriteria();

                if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    if (ancillaryRemoteService.metaData) {
                        ancillaryService.setMetaData(ancillaryRemoteService.metaData.ondPreferences);
                    }
                    appState = 'modifyReservation';
                    self.appState = 'modifyReservation';
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),pnr:currentPnr.pnr,lastName:currentPnr.lastName,departureDate:currentPnr.departureDate}
                }
                else{
                    appState = 'extras';
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),mode: applicationService.getApplicationMode()}
                }

                self.tripType = Object.keys(extrasModel)[trip];
                self.showExtrasTotal = {index: sectorCount, section: self.tripType};
                self.sectorData = extrasModel;
                self.airportNames = {}

                if (Object.keys(passengerService.getPassengerDetails()).length) {
                    var paxData = passengerService.getPassengerDetails().paxList;
                }
                unModifiedSectorData = angular.copy(extrasModel);
                var param = "AUTOMATIC_CHECKIN";

                ancillaryRemoteService.loadAutomaticCheckinData(param, function (response) {
                    loadingService.hidePageLoading();
                    if(response == null){
                        $state.go(appState, urlParams);
                    }
                    else {
                    	angular.forEach(response, function(sector, key) {
                    	    if (key == 0) {
                    	        var flightSegment = response[key].scope.flightSegmentRPH;
                    	        var scopeType = response[key].scope.scopeType;
                    	        var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
                    	        var ancillaryPrefs = ancillaryService.getAncillaryPreference(param, scopeType, flightSegment, sectorDetails);
                    	        setDefaultAutoCheckinPrice(ancillaryPrefs, 0, response[key].items);
                    	    }
                    	    autoCheckinPriceArray[key] = response[key].items;
                    	});
                    	self.journeyType = response[sectorCount].scope.scopeType;
                    	self.autoCheckinPreferance = ancillaryService.getAncillaryFromResponse(param, response);
                    	var selectedOndId = self.autoCheckinPreferance.scopes[0].preferences[sectorCount].preferanceId;
                    	self.currentScope = response[sectorCount].scope;
                    	self.currentSector = self.autoCheckinPreferance.scopes[0].preferences[sectorCount];
                    	self.passengers = self.autoCheckinPreferance.scopes[0].preferences[sectorCount].passengers;
                    	self.airportNames = getAirportName();
                    	setAutoCheckinTotal(appMode);

                    } 

                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);
                });

                if(appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci();

                    if(unmodifiedAnci && unmodifiedAnci[param]) {
                        self.previousSelection = unmodifiedAnci[param];
                    }
                }

        })();
            
        function setDefaultAutoCheckinPrice(preferences,defaultItem,items) {
        	if (preferences.passengers != undefined) {
                    angular.forEach(preferences.passengers, function (pas, key) {
                        pas.selectedItems = (pas.selectedItems != undefined) ? pas.selectedItems : [];
                        if (appMode == applicationService.APP_MODES.CREATE_RESERVATION) {
                            if (pas.selectedItems[0] === undefined || Object.keys(pas.selectedItems).length == 0) {
                                if (defaultItem > -1) {
                                	pas.selectedItems[0] = autoCheckin;
                                    pas.selectedItems[0].price = items[defaultItem].price;
                                    pas.selectedItems[0].value = items[defaultItem].weight;
                                    pas.selectedItems[0].id = items[defaultItem].Id;
                                    pas.selectedItems[0].quantity = 1;
                                    pas.selectedItems[0].input = null;
                                }
                        }
                        var passanger = angular.copy(preferences.passengers[key]);
                        preferences.passengers[key] = passanger;
                        }
                    });
                }

        };

        function getAirportName () {
            var airport = {}
            var fromCode = "";
            var toCode = "";
            if (self.autoCheckinPreferance.scopes[0].scopeType == "SEGMENT") {
                fromCode = self.currentSector.segments[0].from
                toCode = self.currentSector.segments[0].to
            } else if (self.autoCheckinPreferance.scopes[0].scopeType == "OND") {
                var lastIndex = self.currentSector.segments.length - 1
                fromCode = self.currentSector.segments[0].from
                toCode = self.currentSector.segments[lastIndex].to
            }
            airport.searchCriteria = commonLocalStorageService.getSearchCriteria();

            airport.departureAirportName = extrasService.getAirportName(fromCode);
            airport.departureAirportCode = fromCode;

            airport.arrivalAirportName = extrasService.getAirportName(toCode);
            airport.arrivalAirportCode = toCode;

            return airport;
        };
        
        function setAutoCheckinTotal (appMode) {
        	angular.forEach( self.autoCheckinPreferance.scopes[0].preferences, function (pref, key) {
        		 var anciTotal = ancillaryService.getSegmentTotal(pref.passengers,appMode);
                 pref.total = (isNaN(anciTotal)) ? 0 : anciTotal;
             })
             var segmentTotal = ancillaryService.getAncileryTotal(self.autoCheckinPreferance.scopes[0].preferences);
            self.autoCheckinPreferance.scopes[0].total = segmentTotal;
            self.autoCheckinPreferance.total = segmentTotal;
        }


        function nextSector (index, section,$event) {
        	
        	self.seatPreference = setAncillarySeatPreferences(index); 

            if (index === undefined) {
                sectorCount++;

                if (this.autoCheckinPreferance.scopes[0].preferences[sectorCount] == undefined) {
                    sectorCount = 0;
                }
                this.currentSector = this.autoCheckinPreferance.scopes[0].preferences[sectorCount];
                this.showExtrasTotal = {
                    index: sectorCount,
                    section: sectorCount
                }
            } else {
                this.currentSector = this.autoCheckinPreferance.scopes[0].preferences[index];
                sectorCount = index;
            }
            self.passengers = self.autoCheckinPreferance.scopes[0].preferences[sectorCount].passengers
            self.airportNames = getAirportName()
            setDefaultAutoCheckinPrice(this.currentSector, 0 ,autoCheckinPriceArray[sectorCount]);
            setAutoCheckinTotal(appMode);
            if($event){
                $event.preventDefault();
                $event.stopPropagation();
            }
        }

        function backToHome () {
            navigationService.modifyFlowHome();
        }

        function backToExtras () {
        	var paxDetails = JSON.parse(sessionStorage.getItem('IBE.PAX_DETAIL'));             
        	paxDetails.paxList = self.passengersList;            
        	var updatedPassengerList = JSON.stringify(paxDetails);
        	sessionStorage.setItem('IBE.PAX_DETAIL',updatedPassengerList);
            var allItemsSelected = true;
            angular.forEach(self.autoCheckinPreferance.scopes[0].preferences, function(pref, key){
                angular.forEach(pref.passengers, function(pax){
                    if(_.isEmpty(pax.selectedItems)){
                        allItemsSelected = false;
                    } else {
                        angular.forEach(paxDetails.paxList, function (passenger) {
                            if (pax.passenger.paxSequence == passenger.paxSequence && pax.passenger.firstName == passenger.firstName && pax.passenger.lastName == passenger.lastName) {
                                pax.selectedItems[0].input = {email:passenger.email};
                            }
                        });
                    }
                })
            });
            $rootScope.extraType = '';
            if(allItemsSelected){
                ancillaryService.backToExtras();
            }else{
                $translate('lbl_autocheckin_PopupService').then(function(PopupServiceMessage) {
                	popupService.confirm(PopupServiceMessage, function(){
                        ancillaryService.backToExtras();
                    });
                });   
            }
        };

        this.onDelete = function () {   
        	angular.forEach(self.autoCheckinPreferance.scopes[0].preferences, function (sector) {
                angular.forEach(sector.passengers, function (passenger) {
                    passenger.selectedItems = [];

                })
            });
            setAutoCheckinTotal();
        };

        function skipServices() {
        	angular.forEach(ancillaryService.currentTempAncillary.scopes[0].preferences, function (sector) {
                angular.forEach(sector.passengers, function (passenger) {
                    passenger.selectedItems = [];

                })
            });
            setAutoCheckinTotal(appMode);
            ancillaryService.backToExtras();
            ancillaryService.skipServices("AUTOMATIC_CHECKIN");
            $rootScope.extraType = '';
        };
        
        function setAncillarySeatPreferences(index){
        	var ancillary = JSON.parse(sessionStorage.getItem('IBE.ANCILLARY'));
        	if(ancillary){
        		angular.forEach(ancillary['SEAT']['scopes'][0].preferences[index].passengers, function (value, key) {
        			if(value.selectedItems.length>0){
        				seatPreferenceData[key] = value.selectedItems[0].seatNumber;
        			}
        			else{
        				seatPreferenceData[key] = '';
        			}
        		});
        		return seatPreferenceData;
        	}
        };


        $scope.getLength = function (arr) {
            var length = Object.keys(arr).length - 1;
            return length;
        };


    }]

    return controllers;
};