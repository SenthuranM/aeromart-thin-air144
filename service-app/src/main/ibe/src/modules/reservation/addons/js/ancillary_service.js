/**
 * This service generates and maintains ancillary information.
 * Uses ancillaryFactory to generate the ancillary information
 * This service would be used at the initialization of each extra-services page to create a unique object
 */
'use strict';

var ancillaryService = function () {
    var service = {};
    service.ancillaryService = [
        "ancillaryFactory",
        "passengerService",
        "commonLocalStorageService",
        "IBEDataConsole",
        "$rootScope",
        "$location",
        "quoteService",
        "$timeout",
        "currencyService",
        "languageService",
        "applicationService",
        "$state",
        "ancillaryRemoteService",
        "alertService",
        "extrasMealService",
        function (ancillaryFactory, passengerService, commonLocalStorageService, IBEDataConsole, $rootScope, $location,
                  quoteService, $timeout, currencyService, languageService, applicationService, $state, ancillaryRemoteService,
                  alertService, extrasMealService) {
            var self = this, quoteDetails,blockSeat=false;

            //Main object to hold all ancillary types
            self.ancillaryModel = {};
            self.currentTempAncillary = {}; //Used to hold a temporary model
            self.paxListWithSeat = passengerService.getPassengerDetailsForAnci();
            self.startingRowNumber = 23;
            self.previousSeatsCreated = false;

            self.ANCI_TYPES = {
                'SEAT': 'SEAT',
                'MEALS': 'MEALS',
                'AUTOMATIC_CHECKIN': 'AUTOMATIC_CHECKIN',
                'FLEXIBILITY': 'FLEXIBILITY',
                'BAGGAGE': 'BAGGAGE',
                'SSR_AIRPORT': 'SSR_AIRPORT',
                'SSR_IN_FLIGHT':'SSR_IN_FLIGHT',
                'INSURANCE': 'INSURANCE',
                'AIRPORTTRANSFER':'AIRPORTTRANSFER'
            };
            //track user selection
            self.ANCI_STATE ={
                'SEAT' : false,
                'MEALS': false,
                'AUTOMATIC_CHECKIN':false,
                'FLEXIBILITY': false,
                'BAGGAGE': false,
                'SSR_AIRPORT': false,
                'SSR_IN_FLIGHT':false,
                'INSURANCE': false,
                'AIRPORTTRANSFER':true
            }

            self.FLIGHT_DIRECTION = {
                'IN': 'inbound',
                'OUT': 'outbound'
            };

            self.SCOPE_TYPE = {
                'SEGMENT': 'SEGMENT',
                'OND': 'OND',
                'AIRPORT': 'AIRPORT'
            };
            self.defaultAncillary = {
                BAGGAGE: {
                    optionHeading: 'Adjust Your Baggage Limits',
                    description: 'Don’t wait until the last minute! Select your checked baggage allowance now and save on baggage fees!',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'baggage',
                    currentSelection:{}
                },
                SEAT: {
                    optionHeading: 'Change Your Seats',
                    description: 'Do you prefer to sit in the front row? Aisle or Window? or simply want to be seated together with friends and family. Select your preferred seat and ensure it is reserved for you.',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'seats',
                    currentSelection:{}
                },
                AUTOMATIC_CHECKIN: {
                    optionHeading: 'Automatic check-in',
                    description: 'Save time and finish your check-in right now! With Automatic Check-in, you get  checked-in before anybody else, get the best available seats and receive your boarding pass 36h-24h before your flight.',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'automatic checkin',
                    currentSelection:{}
                },
                MEAL: {
                    optionHeading: 'Select Your Meals ',
                    description: 'Pre-order your preferred meal from our Sky Café menu to get access to exclusive meals available only online and ensure that you are served first onboard the flight.',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'meals',
                    currentSelection:{}
                },
                INSURANCE: {
                    optionHeading: 'Select Travel Insurance ',
                    description: 'Get a comprehensive cover that compensates you whenever you need to cancel or shorten your trip, meet an accident overseas, need to return for medical treatments…',
                    buttonCaption: 'Upgrade Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'insurance',
                    currentSelection:{}
                },
                SSR_IN_FLIGHT: {
                    optionHeading: 'Choose Inflight Services ',
                    description: 'Upgrade to a better travel experience with in-flight Services.',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'flight services',
                    currentSelection:{}
                },
                SSR_AIRPORT: {
                    optionHeading: 'Choose Airport Services ',
                    description: 'Upgrade to a better travel experience with Hala Services (Meet & Assist, Priority Immigration, QuickCheck-in and Quick Exit…) or choose to check-in from our conveniently located city check-in offices across the UAE.',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'airport services',
                    currentSelection:{}
                },
                FLEXIBILITY:{
                    optionHeading: 'Choose Flexibility',
                    description: 'Upgrade to a better travel experience with Hala Services.',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'flexibility',
                    currentSelection:{}
                },
                AIRPORT_TRANSFER:{
                    optionHeading: 'Choose Flexibility',
                    description: 'Select airport transfer services.',
                    buttonCaption: 'Select Now',
                    value: {currency: 'AED', amount: 720},
                    available: false,
                    optionName: 'Airport Transfer',
                    currentSelection:{}
                }
            }
            self.setBlockSeatStatus = function(status){
                blockSeat = status;
            }
            self.getBlockSeatStatus = function(){
                return blockSeat;
            }

            self.getDefaultAncillary = function(){
                return self.defaultAncillary;
            }
            self.setAncillaryDescription = function(type,description){
                self.defaultAncillary[type].description = description;
            }
            self.setAncillaryName = function(type, name){
                self.defaultAncillary[type].optionNameTemp = name;
            }
            self.setAnciState = function(type,state){
                self.ANCI_STATE[type] = state;
            }
            self.getAnciState = function(type){
                return self.ANCI_STATE[type];
            }
            self.backToExtras = function () {
                commonLocalStorageService.setData("ANCILLARY", self.ancillaryModel);
                quoteService.setShowSummaryDrawer(true);

                $state.go('extras',
                    {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
            };

            self.setMetaData = function (metaData) {
                commonLocalStorageService.setOndDetails(metaData);
            };

            self.skipServices = function (anciType) {
                if (anciType) {
                    // Empty check for the current object for the modification flow
                    if(!_.isEmpty(self.currentTempAncillary)){
                        //Set currentTempAncillary as the current type of ancillary
                        self.ancillaryModel[anciType] = self.currentTempAncillary;
                    }
                }
                quoteService.setShowSummaryDrawer(true);

                $state.go('extras',
                    {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
            };

            // Saves ancillaries to localStorage
            self.setAncillaryModel = function (ancillaryModel) {
                self.ancillaryModel = ancillaryModel;
                commonLocalStorageService.setData("ANCILLARY", self.ancillaryModel);
            };

            // Get an instance of the AncillaryModel (Main model holding all ancillaries)
            self.getAncillaryModel = function () {
                if (_.isEmpty(self.ancillaryModel)) {
                    //If available in localStorage, get from local storage
                    if (commonLocalStorageService.getData("ANCILLARY")) {
                        self.ancillaryModel = commonLocalStorageService.getData("ANCILLARY");
                    }
                }

                return self.ancillaryModel;
            };

            self.clearAncillaryModel = function () {
                self.ancillaryModel = {};
            };

            self.getAnciFromLocalStorage = function () {
                return commonLocalStorageService.getData("ANCILLARY");
            };

            /**
             * Create initial ancillary object and return existing object for the ancillary type
             * Should be used for getting an object for a specific ancillary type ex-getAncillary('MEALS')
             * */
            self.getAncillary = function (ancillaryType) {
                var ancillary;
                var ancillaryModel = self.getAncillaryModel();

                if (ancillaryModel[ancillaryType]) {
                    ancillary = ancillaryModel[ancillaryType];
                } else {
                    ancillary = new ancillaryFactory.AncillaryItemDTO(ancillaryType);
                    ancillaryModel[ancillaryType] = ancillary;
                }

                self.currentTempAncillary = angular.copy(ancillary);

                return ancillary;
            };

            self.setCurrentTempAncillary = function(ancillary){
                self.currentTempAncillary = angular.copy(ancillary);
            };

            //Generate ancillary using response
            self.getAncillaryFromResponse = function (ancillaryType, response) {

/*                //Sort items in order
                angular.forEach(response, function (sector) {
                    if (sector.scope.scopeType === "SEGMENT") {
                        var orderedFlightArray = commonLocalStorageService.getOrderedFlights();
                        sector.index = _.indexOf(orderedFlightArray, sector.scope.flightSegmentRPH);
                    }
                });
                response = _.sortBy(response, 'index');*/

                angular.forEach(response, function (sector) {
                    var scopeType = sector.scope.scopeType;
                    var currentScope = sector.scope;
                    self.getAncillaryPreferenceRequest(ancillaryType, currentScope);
                });

                return self.getAncillary(ancillaryType);
            };

            //Check if ancillary is already created. Used for assigning user selected values instead of response defaults
            self.checkAncillaryCreated = function (ancillaryType) {
                var ancillaryModel = self.getAncillaryModel();
                return !!ancillaryModel[ancillaryType];
            };

            /**
             * Get a scope (ex-SECTOR, OND etc) for a given ancillary type
             * Should be created for each of the Scope types
             * */
            self.getAncillaryScope = function (ancillaryType, scopeType) {
                var ancillary = self.getAncillary(ancillaryType);
                var scope;

                //If ancillary.scopes contains an scope object with a key of 'scopeType' return that scope
                angular.forEach(ancillary.scopes, function (currentScope, key) {
                    if (currentScope.scopeType === scopeType) {
                        scope = currentScope;
                    }
                });

                //If no such scope exists in ancillary model, create a new one of the 'scopeType' and the scope to the object;
                if (!scope) {
                    scope = new ancillaryFactory.AncillaryScopes(scopeType);
                    ancillary.scopes = [];
                    ancillary.scopes.push(scope);
                }

                return scope;
            };

            /**
             * Create appropriate preference with the passengers included
             * This should be the method called by each of the sectors
             * This method uses the 'getAncillary' and the getAncillaryScope methods and the PassengerService
             * */
            self.getAncillaryPreference = function (ancillaryType, scopeType, preferenceId, segmentDetails) {

                //return if anci type, scope type or the preferenceId has not being defined
                if (!ancillaryType || !scopeType || !preferenceId) {
                    return;
                }

                var preference;
                var scope = self.getAncillaryScope(ancillaryType, scopeType);

                if (_.isObject(preferenceId)) {
                    if (scopeType === "AIRPORT") {

                    } else if (scopeType === "SEGMENT") {
                        preferenceId = self.getFlightDetails(currentScope.flightSegmentRPH);
                    }
                }

                //set the segment details for OND
                if (scopeType === "OND") {
                    segmentDetails = self.getOndDetails(preferenceId);
                }

                //Loop through all preference types in the given scope and check for the given ID
                angular.forEach(scope.preferences, function (currentPref) {
                    if (currentPref.preferanceId === preferenceId) {
                        preference = currentPref;
                    }
                });

                //If not such preference exists, create new one
                if (!preference) {
                    preference = new ancillaryFactory.AncillaryPreference(preferenceId, segmentDetails);

                    //Use passengerService to add pax details to each preference
                    var passengerDetails = passengerService.getPassengerDetails();
                    var passengerList = passengerDetails.paxList;

                    //Add each passenger in to the preference
                    angular.forEach(passengerList, function (passenger, key) {
                        //preference.passengers
                        //Infant not showing in ancillary page
                        //@TODO in create flow passengerrph should same as passenger.paxSequence
                        if (passenger.paxSequence != undefined) {
                            var passengerRph = passenger.paxSequence;
                        } else {
                            var passengerRph = key + 1;
                        }
                        if (passenger.category !== 'Infant') {
                            var currentPax = new ancillaryFactory.AncillaryPassenger(passenger, passengerRph);
                            preference.passengers.push(currentPax);
                        }

                    });

                    scope.preferences.push(preference);
                }
                return preference;
            };

            /**
             * Create appropriate preference with the passengers included
             * This should be the method called by each of the sectors
             * This method uses the 'getAncillary' and the 'getAncillaryScope' methods and the PassengerService
             * */
            self.getAncillaryPreferenceRequest = function (ancillaryType, selectedScope) {
                var preference;
                var segmentDetails;
                var scopeType = selectedScope.scopeType;
                var preferenceId; //Could be flightSegmentRPH, OndID or Scope

                var scope = self.getAncillaryScope(ancillaryType, scopeType);

                if (scopeType === "AIRPORT") {
                    preferenceId = selectedScope;
                    segmentDetails = [
                        {"from": selectedScope.airportCode}
                    ];
                    /*                    angular.forEach(scope.preferences, function (ancillaryPreference, prefKey) {
                     if (ancillaryPreference.segments[0].from == preferenceId.airportCode) {
                     ancillaryPreference.preferanceId = preferenceId;
                     }
                     });*/

                } else if (scopeType === "SEGMENT") {
                    preferenceId = selectedScope.flightSegmentRPH;
                    segmentDetails = self.getFlightDetails(preferenceId);
                } else if (scopeType === "OND") {
                    preferenceId = selectedScope.ondId;
                    segmentDetails = self.getOndDetails(preferenceId);
                }

                //Loop through all preference types in the given scope and check for the given ID
                angular.forEach(scope.preferences, function (currentPref) {
                    //TODO: Remove once the cause of missing scopeType is found
                    if (_.isObject(currentPref.preferanceId)) {
                        currentPref.preferanceId.scopeType = "AIRPORT";
                    }
                    if (_.isEqual(currentPref.preferanceId, preferenceId)) {
                        preference = currentPref;
                    }
                });

                //If such preference doesn't exist, create new one
                if (!preference) {
                    preference = new ancillaryFactory.AncillaryPreference(preferenceId, segmentDetails);

                    //Use passengerService to add pax details to each preference
                    var passengerDetails = passengerService.getPassengerDetails();
                    var passengerList = passengerDetails.paxList;

                    //Add each passenger in to the preference
                    angular.forEach(passengerList, function (passenger, key) {
                        //preference.passengers
                        if (passenger.paxSequence != undefined) {
                            var passengerRph = passenger.paxSequence;
                        } else {
                            var passengerRph = key + 1;
                        }
                        if (passenger.category !== 'Infant') {
                            var currentPax = new ancillaryFactory.AncillaryPassenger(passenger, passengerRph);
                            preference.passengers.push(currentPax);
                        }
                    });

                    scope.preferences.push(preference);
                }
                return preference;
            };

            self.getAncillaryPreferenceForInsurance = function (ancillaryType, scopeType, insurancePolicies) {
                var preference;
                var scope = self.getAncillaryScope(ancillaryType, scopeType);
                preference = scope.preferences;

                //If not such preference exists, create new one
                if (!preference.length) {
                    preference = new ancillaryFactory.AncillaryInsurancePreference();

                    for (var i = 0; i < insurancePolicies.length; i++) {
                        if (insurancePolicies[i].isSelected) {
                            var temp = {};
                            temp.id = insurancePolicies[i].policyID;
                            temp.quantity = null;
                            temp.input = null;
                            preference.selectedItems.push(temp);
                        }
                    }

                    scope.preferences.push(preference);
                }
                return preference;
            };

            self.orderResponse = function(response){
                angular.forEach(response, function (sector) {
                    if (sector.scope.scopeType === self.SCOPE_TYPE.SEGMENT) {
                        var orderedFlightArray = commonLocalStorageService.getOrderedFlights();
                        sector.index = _.indexOf(orderedFlightArray, sector.scope.flightSegmentRPH);
                    }
                });
                return _.sortBy(response, 'index');
            };

            /**
             * if calculate total for each segment for given Ancillary
             * @param passenger - all passengers
             */
            self.getSegmentTotal = function (passenger, appMode) {
                var total = 0;
                angular.forEach(passenger, function (ancilary) {
                    var passengerTotal = 0;
                    angular.forEach(ancilary.selectedItems, function (item) {
                        if(item != undefined) {
                            var price = (typeof item.price === 'undefined') ? item.amount : item.price;
                            if (!Array.isArray(price)) {
                                if (item != undefined) {
                                    /*if ((appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT)) {
                                        total = total + price * item.quantity
                                    } else {
                                        total = total + price;
                                    }*/

                                    // Quantity can be null or undefined for FLEXI, SEATS
                                    // Temporary quantity is set to 1 in order to calculate the totals correctly
                                    var quantity = item.quantity;
                                    if(_.isNull(item.quantity) || _.isUndefined(item.quantity)){
                                        quantity = 1;
                                    }

                                    total = total + price * quantity;
                                    total = (isNaN(total)) ? 0 : total;

                                    passengerTotal = passengerTotal + price;
                                    passengerTotal = (isNaN(passengerTotal)) ? 0 : passengerTotal;
                                }
                            } else {
                                var paxType = (ancilary.passenger.paxType == undefined) ? ancilary.passenger.type : ancilary.passenger.paxType;
                                if(ancilary.passenger.infantSequence) {
                                    paxType ='ADULT_WITH_INFANT';
                                }
                                var priceCat = _.findWhere(price, {chargeBasis: paxType.toUpperCase()});
                                var price = priceCat.amount;
                                /*if ((appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT)) {
                                    total = total + price * item.quantity
                                } else {
                                    total = total + price;
                                }*/
                                total = total + price;
                                total = (isNaN(total)) ? 0 : total;

                                passengerTotal = passengerTotal + price;
                                passengerTotal = (isNaN(passengerTotal)) ? 0 : passengerTotal;
                            }
                        }
                    });
                    ancilary.itmeTotalCharge = passengerTotal;
                });
                return Math.ceil(total);
            };

            /**
             *calculating total for the given ancillary in complete trip
             * @param preferences - all segments
             *
             */
            self.getAncileryTotal = function (preferences) {
                var total = 0;
                angular.forEach(preferences, function (segment) {
                    total = total + segment.total;
                });
                self.accumulatedAncillaryCharge();
                return total;

            };

            self.getAccumulatedChargeFor = function (type) {
                var ancillaryModel = self.getAncillaryModel();
                return (ancillaryModel[type].total || 0);
            };

            self.accumulatedAncillaryCharge = function () {

                $timeout(function () {
                    var ancillaryModel = self.getAncillaryModel();
                    var total = 0;
                    angular.forEach(ancillaryModel, function (anciMod, anciKey) {
                        if (anciKey != "total" && anciMod != undefined) {
                            total = total + anciMod.total
                        }
                    });
                    ancillaryModel.total = total;

                    quoteService.setExtraTotal(total);
                }, 0);
            };

            self.getAncillaryTotalTypewise = function () {
                var ancillaryModel = self.getAncillaryModel(), _sendArray = [];
                angular.forEach(ancillaryModel, function (anciMod, anciKey) {
                    if (anciKey != "total") {
                        var tObj = {};
                        tObj.type = anciKey;
                        tObj.amount = anciMod.total;
                        _sendArray.push(tObj);
                    }
                });
                return _sendArray
            };

            self.getSelectedAncillaryTotal = function (type) {
                var ancillaryModel = self.getAncillaryModel();
                var total = (ancillaryModel[type] != undefined) ? ancillaryModel[type].total : 0
                return total;
            };

            self.getMealTotalPerPassenger = function (selectedItems) {
                var totalCharge = 0;
                angular.forEach(selectedItems, function (item, key) {
                    var ItemPrice = (typeof item.price === 'undefined') ? item.amount : item.price;
                    totalCharge = totalCharge + (ItemPrice * item.quantity);
                });
                return Math.ceil(totalCharge);
            };

            // Convert ancillary object to backend format
            self.getAncillaryRequestJson = function () {

                var requestJson = {
                    ancillaries: []
                };

                var ancillaryModel = angular.copy(self.getAncillaryModel());

                //Keep only the ancillary model objects
                ancillaryModel = _.filter(ancillaryModel, function (model) {
                    return !_.isUndefined(model.ancillary);
                });

                angular.forEach(ancillaryModel, function (ancillary, ancModeliKey) {
                    // Remove un-editable preference from response
                    if(ancillary.scopes.length > 0){
                    ancillary.scopes[0].preferences = _.filter(ancillary.scopes[0].preferences, function(preference){
                        return !preference.unEditable;
                    });
                }
                    var ancillaryName = ancillary.ancillary;

                    var ancillariesObject = {
                        type: ancillaryName,
                        preferences: []
                    };
                    if (ancillaryName === "INSURANCE") {
                        if (!_.isEmpty(ancillary.scopes[0].preferences) && ancillary.scopes[0].preferences[0].selectedItems != undefined) {
                            if (ancillary.scopes[0].preferences[0].selectedItems.length) {
                                ancillariesObject.preferences.push(
                                    {

                                        assignee: {
                                            assignType: "ALL_PASSENGERS"
                                        },
                                        selections: {
                                            scope: {
                                                scopeType: "ALL_SEGMENTS"
                                            },
                                            selectedItems: ancillary.scopes[0].preferences[0].selectedItems
                                        }

                                    }
                                );
                            }
                        }
                    } else if (ancillaryName === "FLEXIBILITY") {
                        //TODO: Undefined check added to compensate for faulty data from backend
                        angular.forEach(!_.isUndefined(ancillary.scopes[0]) && ancillary.scopes[0].preferences, function (item) {
                            if (item.selectedItems.selected && !item.defaultselection) {
                                var currentPreferenceID = item.preferanceId;
                                var selectedItems = [];
                                selectedItems[0] = {};
                                selectedItems[0].id = item.selectedItems.flexiId;
                                ancillariesObject.preferences.push(
                                    {
                                        assignee: {
                                            assignType: "ALL_PASSENGERS"
                                        },
                                        selections: {
                                            scope: {
                                                scopeType: "OND",
                                                ondId: currentPreferenceID
                                            },
                                            selectedItems: selectedItems
                                        }
                                    }
                                );
                            }

                        })
                    }
                    else {
                        angular.forEach(ancillary.scopes, function (scope) {
                            var currentScopeType = scope.scopeType;

                            angular.forEach(scope.preferences, function (preference) {
                                var currentPreferenceID = preference.preferanceId;

                                //TODO: add items without passengers with an 'assignType' of 'ALL PASSENGERS'
                                angular.forEach(preference.passengers, function (pax) {
                                    var paxRph
                                    /*if (applicationService.getApplicationMode() == applicationService.APP_MODES.CREATE_RESERVATION ) {
                                     paxRph = pax.passengerRph + 1;
                                     }else{
                                     paxRph = pax.passengerRph;
                                     }*/
                                    paxRph = pax.passengerRph;
                                    var itemArray = _.toArray(pax.selectedItems);
                                    itemArray = angular.copy(itemArray);
                                    _.map(itemArray, function (item) {

                                        var tempId = item.id;
                                        var tempQuantity = item.quantity;
                                        var tempInput = item.input;

                                        for (var key in item) delete item[key];

                                        item.id = tempId;
                                        item.quantity = tempQuantity;
                                        item.input = tempInput;

                                    });

                                    //Use the [] notation to push the "segmentID" (ex- flightSegmentRPH etc.)
                                    var tempScope = {
                                        scopeType: currentScopeType
                                    };


                                    //ScopeType depends on the preference

                                    if (currentScopeType === "SEGMENT") {
                                        tempScope["flightSegmentRPH"] = currentPreferenceID;
                                    } else if (currentScopeType === "OND") {
                                        tempScope["ondId"] = currentPreferenceID;
                                    } else if (currentScopeType === "AIRPORT") {
                                        if (currentPreferenceID.airportCode != undefined) {
                                            tempScope["airportCode"] = currentPreferenceID.airportCode;
                                            tempScope["flightSegmentRPH"] = currentPreferenceID.flightSegmentRPH;
                                            tempScope["point"] = currentPreferenceID.point;
                                        } else {
                                            tempScope = null;
                                        }
                                    }
                                    if (itemArray.length && tempScope != null) {
                                        ancillariesObject.preferences.push(
                                            {

                                                assignee: {
                                                    assignType: "PASSENGER",
                                                    passengerRph: paxRph
                                                },
                                                selections: {
                                                    scope: tempScope,
                                                    selectedItems: itemArray
                                                }

                                            }
                                        );
                                    }
                                });
                            });
                        });
                    }

                    if (ancillariesObject.preferences.length) {
                        requestJson.ancillaries.push(ancillariesObject);
                    }

                });

                angular.forEach(requestJson.ancillaries, function (ancillary) {
                    var preferenceByPassenger = {};

                    var allPassengers = false;

                    //Check if the ancillary is to be applied to all passengers
                    if (ancillary.preferences[0]) {
                        if (ancillary.preferences[0].assignee.assignType == "ALL_PASSENGERS") {
                            allPassengers = true;
                        }

                    }

                    angular.forEach(ancillary.preferences, function (pref) {

                        if (preferenceByPassenger[pref.assignee.passengerRph]) {
                            preferenceByPassenger[pref.assignee.passengerRph].push(pref.selections)
                        } else {
                            preferenceByPassenger[pref.assignee.passengerRph] = [];
                            preferenceByPassenger[pref.assignee.passengerRph].push(pref.selections)
                        }
                    });

                    ancillary.preferences = [];

                    angular.forEach(preferenceByPassenger, function (value, key) {

                        if (!allPassengers) {
                            var preferences = {
                                assignee: {
                                    assignType: "PASSENGER",
                                    passengerRph: key
                                },
                                selections: value
                            };
                        } else {
                            var preferences = {
                                assignee: {
                                    assignType: "ALL_PASSENGERS",
                                },
                                selections: value
                            };
                        }
                        ancillary.preferences.push(preferences);
                    });

                });

                return requestJson;
            };

            self.removeUnMatchPreference = function (scope, ondID) {
                angular.forEach(scope, function (s) {
                    var prf = [];
                    angular.forEach(s.preferences, function (ancillaryPreference, prefKey) {
                    	 angular.forEach(ondID, function (currentOndId, ondIdIdx) {
                             if (ancillaryPreference.preferanceId === ondID[ondIdIdx]) {
                                 prf.push(ancillaryPreference);
                             }
                         });
                    });
                    s.preferences = prf;
                });
            };

            self.getFlightDetails = function (flightRph) {
                var selectedFlights = commonLocalStorageService.getConfirmedFlightData();
                var flightDetails;

                angular.forEach(selectedFlights, function (flight) {
                    if (flight.flight.flightRPH === flightRph) {
                        flightDetails = flight.flight;

                        //If there is more than one stop, we need to get the destination of the first segment from the first stops origin
                        if (flight.flight.stops.length > 0) {
                            flightDetails.destinationCode = flight.flight.stops[0].originCode;
                            flightDetails.destinationName = flight.flight.stops[0].originName;
                        }
                    }

                    angular.forEach(flight.flight.stops, function (flight) {
                        if (flight.flightRPH === flightRph) {
                            flightDetails = flight;
                        }
                    });
                });

                //Only sets flight details if they exist
                if (flightDetails) {
                    var segmentDetails = [
                        {
                            to: flightDetails.destinationCode,
                            from: flightDetails.originCode
                        }
                    ];
                }
                return segmentDetails;
            };

            self.getFlightFromRph = function (flightRph) {

                //If in modify segment mode, get flight details from the concatinated previous flight data
                var mode = applicationService.getApplicationMode();
                var selectedFlights = commonLocalStorageService.getConfirmedFlightData();

                if(mode == applicationService.APP_MODES.MODIFY_SEGMENT && commonLocalStorageService.getPreviousConfirmedFlightData()){
                    selectedFlights = commonLocalStorageService.getPreviousConfirmedFlightData();
                }

                var flightDetails;

                angular.forEach(selectedFlights, function (flight) {
                    if (flight.flight.flightRPH === flightRph) {
                        flightDetails = flight.flight;
                    }

                    angular.forEach(flight.flight.stops, function (flight) {
                        if (flight.flightRPH === flightRph) {
                            flightDetails = flight;
                        }
                    });
                });

                return flightDetails;
            };

            self.getFlightAdvanceDetails = function (flightRph) {
                var selectedFlights = commonLocalStorageService.getConfirmedFlightData();

                //If in modify segment mode, get flight details from the concatinated previous flight data
                var mode = applicationService.getApplicationMode();
                if(mode == applicationService.APP_MODES.MODIFY_SEGMENT && commonLocalStorageService.getPreviousConfirmedFlightData()){
                    selectedFlights = commonLocalStorageService.getPreviousConfirmedFlightData();
                }

                var flightDetails;

                angular.forEach(selectedFlights, function (flight) {
                    if (flight.flight.flightRPH === flightRph) {
                        flightDetails = flight.flight;

                        //If there is more than one stop, we need to get the destination of the first segment from the first stops origin
                        if (flight.flight.stops.length > 0) {
                            flightDetails.destinationCode = flight.flight.stops[0].originCode;
                            flightDetails.destinationName = flight.flight.stops[0].originName;
                        }
                    }

                    angular.forEach(flight.flight.stops, function (flight) {
                        if (flight.flightRPH === flightRph) {
                            flightDetails = flight;
                        }
                    });
                });

                return flightDetails;
            };

            self.getOndDetails = function (OndId) {
                var ondDetails = commonLocalStorageService.getOndDetails();
                var selectedOnd;

                angular.forEach(ondDetails, function (ondPrefs) {
                    if (ondPrefs.ondId == OndId) {
                        selectedOnd = ondPrefs;
                    }
                });

                if (selectedOnd) {
                    selectedOnd.segmentDetails = [];
                    angular.forEach(selectedOnd.flightSegmentRPH, function (flightSegment) {
                        if (self.getFlightDetails(flightSegment)) {
                            selectedOnd.segmentDetails.push(self.getFlightDetails(flightSegment)[0]);
                        }
                    })
                }

                if (selectedOnd) {
                    return selectedOnd.segmentDetails;
                }
            };

            self.getAirportName = function (code) {
                return commonLocalStorageService.getAirportCityName(code, false)
            };

            self.getAirports = function (preference, sectorCount) {
                var airport = {};
                var fromCode = "";
                var toCode = "";
                var point = ""; //Departure or Arrival point for Airport sections

                var scopeType = preference.scopes[0].scopeType

                var curSelection = preference.scopes[0].preferences[sectorCount].segments;
                if (scopeType == "SEGMENT") {
                    fromCode = curSelection[0].from;
                    toCode = curSelection[0].to;
                } else if (scopeType == "OND") {
                    var lastIndex = curSelection.segments.length - 1;
                    fromCode = curSelection.segments[0].from;
                    toCode = curSelection.segments[lastIndex].to;

                } else if (scopeType == "AIRPORT") {
                    try {
                        point = preference.scopes[0].preferences[sectorCount].preferanceId.point;
                    } catch (e) {
                        IBEDataConsole("Error getting airport point");
                    }

                    fromCode = curSelection[0].from;
                    toCode = "";

                }
                airport.departureAirportName = self.getAirportName(fromCode);
                airport.departureAirportCode = fromCode;

                if (scopeType == "AIRPORT") {
                    airport.point = point;
                }

                if (toCode) {
                    airport.arrivalAirportCode = toCode;
                    airport.arrivalAirportName = self.getAirportName(toCode);

                }
                else {
                    airport.arrivalAirportName = "";
                }

                return airport;
            };

            /**
             *Compare selected meal id with complete meal list and set selected meal name
             * @param mealList : complete meal list
             * @param selectedMealList :selected items
             */
            self.setSelectedMealName = function (mealList, selectedMealList) {
                angular.forEach(selectedMealList.scopes, function (scopes) {
                    angular.forEach(scopes.preferences, function (pref) {
                        angular.forEach(pref.passengers, function (pass) {
                            angular.forEach(pass.selectedItems, function (selecteditem) {
                                var selectedid = selecteditem.id;
                                var mealSelection = _.findWhere(mealList, {mealCode: selectedid})
                                if (mealSelection != undefined) {
                                    selecteditem.name = mealSelection.mealName;
                                    selecteditem.amount = mealSelection.mealCharge;

                                }
                            })

                        })

                    })

                });
                return selectedMealList;
            };

            self.setSelectedAirportServiceName = function (servicesList, selectedServicesList) {
                if(selectedServicesList != undefined) {
                    angular.forEach(selectedServicesList.scopes, function (scopes) {
                        angular.forEach(scopes.preferences, function (pref) {
                            angular.forEach(pref.passengers, function (pass) {
                                angular.forEach(pass.selectedItems, function (selecteditem) {
                                    var selectedid = selecteditem.id;
                                    var serviceSelection = _.findWhere(servicesList, {ssrCode: selectedid})
                                    if (serviceSelection != undefined) {
                                        selecteditem.ssrName = serviceSelection.ssrName;
                                        selecteditem.amount = serviceSelection.charges;

                                    }
                                })

                            })

                        })

                    });
                }
                return selectedServicesList;

            };

            function getServiceFactory(type){
            	if(type == 'MEAL'){
            		return extrasMealService;
            	}
            	return function(){
            		this.adjustInventory = function(){
            			throw "Not implemented";
            		};
            		this.initilizeInventory = function(){
            			throw "Not implemented";
            		};
                this.setCustomerPreferredAnci = function(preferences, sectors, callback){
                  throw "Not implemented";
                };
            	};
            };

            self.adjustInventory = function(type, preferences, segIndex, paxIndex, config){
            	return getServiceFactory(type).adjustInventory(preferences, segIndex, paxIndex, config);
            };

            self.initilizeInventory = function(type, preferences, sectors){
            	return getServiceFactory(type).initilizeInventory(preferences, sectors);
            };
            self.setCustomerPreferredAnci = function(type, preferences, sectors, callback){
              return getServiceFactory(type).setCustomerPreferredAnci(preferences, sectors, callback);
            };
            self.setSeatForPax = function (item, applyScope) {

                var currentSeatSellection = this.getAncillaryModel()["SEAT"];
                if (currentSeatSellection == undefined) {

                    var flightSegment = applyScope.flightSegmentRPH;
                    var scopeType = applyScope.scopeType;
                    var sectorDetails = self.getFlightDetails(flightSegment);

                    var ancillaryPrefs = self.getAncillaryPreference("SEAT", scopeType, flightSegment, sectorDetails);

                    self.currentAncillary = ancillaryPrefs;
                    self.ancillaryModel["SEAT"] = ancillaryPrefs;


                    var paxList = ancillaryPrefs.passengers;

                    for (var i = 0; i < paxList.length; i++) {
                        if (paxList[i].passenger.paxSequence != undefined) {
                            paxList[i].passengerRph = paxList[i].passenger.paxSequence;
                        }
                        if (!paxList[i].selectedItems[0] && (item.rowNumber >= self.startingRowNumber) && item.availability) {
                            if (item.validations) {
                                if (isValidSeat(item.notAllowedPassengerType, paxList[i])) {
                                    if (paxList[i].selectedItems[0] === undefined) {
                                        paxList[i].selectedItems[0] = {};
                                        paxList[i].selectedItems[0].id = item.itemId;
                                        paxList[i].selectedItems[0].quantity = 1;
                                        paxList[i].selectedItems[0].input = null;
                                        paxList[i].selectedItems[0].price = item.charges[0].amount;
                                        paxList[i].selectedItems[0].seatNumber = item.itemId;
                                        item.availability = false;

                                        break;
                                    }
                                }
                            }
                            else {

                                if (paxList[i].selectedItems[0] === undefined) {
                                    paxList[i].selectedItems[0] = {};
                                    paxList[i].selectedItems[0].id = item.itemId;
                                    paxList[i].selectedItems[0].quantity = 1;
                                    paxList[i].selectedItems[0].input = null;
                                    paxList[i].selectedItems[0].price = item.charges[0].amount;
                                    paxList[i].selectedItems[0].seatNumber = item.itemId;
                                    item.availability = false;

                                    break;
                                }
                            }
                        }
                    }
                    self.seatPreferance = self.getAncillary("SEAT");
                    return item;
                } else {
                    return null;
                }

            };

            self.getPaxListWithSeat = function () {
                return self.paxListWithSeat;
            };

            self.getAncillarySelected = function (ancillaryType) {
                var ancillary = self.getAncillary(ancillaryType);
                var anciSelected = {};
                var anciSelectedForAllSegments = true;

                //Check each segment to see if ancillary has been selected.
                angular.forEach(ancillary.scopes[0].preferences, function (pref, key) {
                    anciSelected[key] = false;
                    angular.forEach(pref.passengers, function (passenger) {
                        if (!_.isEmpty(passenger.selectedItems)) {
                            anciSelected[key] = true;
                        }
                    });

                    //Special check for Flexi fare where services are applied per sector
                    if (pref.passengers.length === 0) {
                        if (pref.selectedItems.selected === true) {
                            anciSelected[key] = true;
                        }
                    }
                });

                angular.forEach(anciSelected, function (val) {
                    if (val === false) {
                        anciSelectedForAllSegments = false;
                    }
                });

                return anciSelectedForAllSegments;
            };

            self.updateFareQuoteData = function (data) {

                quoteDetails = commonLocalStorageService.getData('QUOTE_DETAILS');
                var chargesDetails = data.monetaryAmmendments;
                if(quoteDetails){
                    quoteDetails.jnTax=0;
                    for (var i = 0; i < chargesDetails.length; i++) {
                        if (chargesDetails[i].name == 'JN_TAX' && chargesDetails[i].category == 'DEBIT') {
                            quoteDetails.jnTax = chargesDetails[i].amount;

                        }
                        else if (chargesDetails[i].name != 'JN_TAX' && chargesDetails[i].category == 'MONEY') {
                            quoteDetails.anciPromotion = chargesDetails[i].amount;

                        }
                        else if (chargesDetails[i].name != 'JN_TAX' && chargesDetails[i].category == 'CREDIT') {
                            quoteDetails.anciCredit = chargesDetails[i].amount;

                        }
                    }
                    commonLocalStorageService.setData('QUOTE_DETAILS', quoteDetails);
                    quoteService.setExtraTotal(data.total);
                }

            };

            self.getAnciRequestJsonForBlockSeat = function () {

                var requestJson = {
                    ancillaries: []
                };

                var ancillaryModel = self.getAncillaryModel()["SEAT"];

                //Keep only the ancillary model objects
                ancillaryModel = _.filter(ancillaryModel, function (model) {
                    return !_.isUndefined(model.ancillary);
                });


                var ancillary = self.getAncillaryModel()["SEAT"];
                if(ancillary) {


                    var ancillaryName = ancillary.ancillary;

                    var ancillariesObject = {
                        type: ancillaryName,
                        preferences: []
                    };
                    angular.forEach(ancillary.scopes, function (scope) {
                        var currentScopeType = scope.scopeType;

                        angular.forEach(scope.preferences, function (preference) {
                            var currentPreferenceID = preference.preferanceId;

                            //TODO: add items without passengers with an 'assignType' of 'ALL PASSENGERS'
                            angular.forEach(preference.passengers, function (pax) {
                                var paxRph
                                /*if (applicationService.getApplicationMode() == applicationService.APP_MODES.CREATE_RESERVATION ) {
                                 paxRph = pax.passengerRph + 1;
                                 }else{
                                 paxRph = pax.passengerRph;
                                 }*/
                                paxRph = pax.passengerRph;
                                var itemArray = _.toArray(pax.selectedItems);
                                itemArray = angular.copy(itemArray);
                                _.map(itemArray, function (item) {

                                    var tempId = item.id;
                                    var tempQuantity = item.quantity;
                                    var tempInput = item.input;

                                    for (var key in item) delete item[key];

                                    item.id = tempId;
                                    item.quantity = tempQuantity;
                                    item.input = tempInput;

                                });

                                //Use the [] notation to push the "segmentID" (ex- flightSegmentRPH etc.)
                                var tempScope = {
                                    scopeType: currentScopeType
                                };


                                //ScopeType depends on the preference

                                if (currentScopeType === "SEGMENT") {
                                    tempScope["flightSegmentRPH"] = currentPreferenceID;
                                } else if (currentScopeType === "OND") {
                                    tempScope["ondId"] = currentPreferenceID;
                                } else if (currentScopeType === "AIRPORT") {
                                    if (currentPreferenceID.airportCode != undefined) {
                                        tempScope["airportCode"] = currentPreferenceID.airportCode;
                                        tempScope["flightSegmentRPH"] = currentPreferenceID.flightSegmentRPH;
                                        tempScope["point"] = currentPreferenceID.point;
                                    } else {
                                        tempScope = null;
                                    }
                                }
                                if (itemArray.length && tempScope != null) {
                                    ancillariesObject.preferences.push(
                                        {

                                            assignee: {
                                                assignType: "PASSENGER",
                                                passengerRph: paxRph
                                            },
                                            selections: {
                                                scope: tempScope,
                                                selectedItems: itemArray
                                            }

                                        }
                                    );
                                }
                            });
                        });
                    });


                    if (ancillariesObject.preferences.length) {
                        requestJson.ancillaries.push(ancillariesObject);
                    }


                    angular.forEach(requestJson.ancillaries, function (ancillary) {
                        var preferenceByPassenger = {};

                        var allPassengers = false;

                        //Check if the ancillary is to be applied to all passengers
                        if (ancillary.preferences[0]) {
                            if (ancillary.preferences[0].assignee.assignType == "ALL_PASSENGERS") {
                                allPassengers = true;
                            }

                        }

                        angular.forEach(ancillary.preferences, function (pref) {

                            if (preferenceByPassenger[pref.assignee.passengerRph]) {
                                preferenceByPassenger[pref.assignee.passengerRph].push(pref.selections)
                            } else {
                                preferenceByPassenger[pref.assignee.passengerRph] = [];
                                preferenceByPassenger[pref.assignee.passengerRph].push(pref.selections)
                            }
                        });

                        ancillary.preferences = [];

                        angular.forEach(preferenceByPassenger, function (value, key) {
                            var preferences = {};
                            if (!allPassengers) {
                                preferences = {
                                    assignee: {
                                        assignType: "PASSENGER",
                                        passengerRph: key
                                    },
                                    selections: value
                                };
                            } else {
                                preferences = {
                                    assignee: {
                                        assignType: "ALL_PASSENGERS"
                                    },
                                    selections: value
                                };
                            }
                            ancillary.preferences.push(preferences);
                        });
                    });
                }
                return requestJson;
            };

            self.setSelectedConfirmAnci = function (preferences) {

                $timeout(function () {
                   quoteService.setSelectedAnci(preferences);
                }, 0);
            };

            function isValidSeat(applyScope, pax) {
                var isValid = true;
                var paxType = '';
                if (pax.category === 'Adult') {
                    paxType = 'AD';
                }
                else if (pax.category === 'Child') {
                    paxType = 'CH';
                }
                else if (pax.category === 'Infant') {
                    paxType = 'IN';
                }
                for (var j = 0; j < applyScope.length; j++) {
                    if (applyScope[j] == paxType) {
                        isValid = false;
                        break;
                    }
                    else if (paxType === 'AD' && applyScope[j] === 'IN' && pax.infantSequence) {
                        isValid = false;
                        break;
                    }
                }
                return isValid;
            }

             self.updateSeatMap = function(){
                 var param={"ancillaries":[{"type":"SEAT"}]};
                 ancillaryRemoteService.updateSeatMapModel(param, function (response) {
                     response = self.orderResponse(response);
                     self.clearOldSelectedSeats(response);
                     setNewDefaultSeat();

                     alertService.setAlert(" Error occured while allocating requested seat(s). Please try again",'danger');
                     $location.path( '/extras/seats/'+languageService.getSelectedLanguage()+'/'+currencyService.getSelectedCurrency()+'/'+ applicationService.getApplicationMode());

                 }, function (error) {

                 });
             };

            self.clearOldSelectedSeats = function(response){
                self.seatPreferance = self.getAncillaryFromResponse( "SEAT", response);
                angular.forEach(self.seatPreferance.scopes[0].preferences, function (sector) {
                    angular.forEach(sector.passengers, function (passenger) {
                        passenger.selectedItems = [];

                    })
                });
            };

            // Setting un-modified ancillary for a flight modification
            self.setUnmodifiedAnci = function(model) {
                // Calculate totals for each preference segment and anci type
                _.forEach(model, function(anciType){
                    var anciTotal = 0;
                    _.forEach(anciType.scopes[0].preferences, function(pref){
                        var prefTotal = 0;

                        _.forEach(pref.passengers, function(passenger) {
                            var paxTotal = 0;
                            _.forEach(passenger.selectedItems, function(item) {
                                prefTotal = prefTotal + item.amount;
                                paxTotal = paxTotal + item.amount;
                            });

                            // Set passenger-wise total for SSR
                            if(anciType.ancillary === "SSR_AIRPORT"){
                                passenger.itmeTotalCharge = paxTotal;
                            }
                        });

                        anciTotal = prefTotal + anciTotal;
                        //is not empty selectedItems

                        pref.total = prefTotal;
                    });
                    anciType.total = anciTotal;
                });

                commonLocalStorageService.setData('MODIFY.RESERVATION.UNMODIFIED_ANCI', model);
                self.unmodifiedAnci = angular.copy(model);
            };

            // Getting un-modified ancillary for a flight modification
            self.getUnmodifiedAnci = function() {
                if(_.isUndefined(self.unmodifiedAnci)) {
                    self.unmodifiedAnci = commonLocalStorageService.getData('MODIFY.RESERVATION.UNMODIFIED_ANCI');
                }
                return self.unmodifiedAnci;
            };

            /*
            * Validation Medical Ancillary
            * */
            self.validateMedicalAncillary = function(){
                var isMedicalAssistantValid = true;

                //Get the valid segment count
                var validSegmentCount = getValidMedicalAssistantSegmentCount();

                //Get ancillary modal of current and previous segments
                var ancillaryModel = getCombinedAncillaryModel();
                var mode = applicationService.getApplicationMode();

                if(ancillaryModel.SSR_IN_FLIGHT) {
                    var ssrSelection = ancillaryModel.SSR_IN_FLIGHT.scopes[0].preferences;
                    var unModified = self.getUnmodifiedAnci();
                    //Check validity per passenger in model
                    //For each passenger that is not an infant.
                    var passengers = passengerService.getPassengerDetailsForAnci();

                    _.forEach(passengers, function(passenger){
                        //Check if assigned Medical Assistant correctly
                        var currentPassengerMedicalCount = getMedicalAssistantCountForPassenger(passenger, ssrSelection);

                        if (!(currentPassengerMedicalCount === validSegmentCount || currentPassengerMedicalCount === 0)) {
                            isMedicalAssistantValid = false;
                        }

                        if(mode == applicationService.APP_MODES.MODIFY_SEGMENT &&
                        		unModified.SSR_IN_FLIGHT.scopes[0].preferences.length > 0){
                        	var bookedPassengerMedicalCount = getBookedMedicalAssistantCountForPassenger(passenger, unModified.SSR_IN_FLIGHT.scopes[0].preferences);
                        	if(bookedPassengerMedicalCount>0 && currentPassengerMedicalCount < 1){
                        		isMedicalAssistantValid = false;
                        	}
                        }
                    });
                }

                return isMedicalAssistantValid;
            };

            // Get the number of valid segments that could contain medical assistance ssr
            function getValidMedicalAssistantSegmentCount() {
                var currentSegments = {};
                var mode = applicationService.getApplicationMode();

                //Get segments for modify segment
                if(mode == applicationService.APP_MODES.MODIFY_SEGMENT
                		&& commonLocalStorageService.getPreviousConfirmedFlightData()){
                    currentSegments = commonLocalStorageService.getPreviousConfirmedFlightData();
                }else{
                    //Get segments for the create flow
                    currentSegments = commonLocalStorageService.getConfirmedFlightData();
                    var tempCurrentSegmentArray = [];

                    //Adapt segments into array
                    _.forEach(currentSegments, function(segment){
                        tempCurrentSegmentArray.push(segment);

                        _.forEach(segment.flight.stops, function(stop){
                            tempCurrentSegmentArray.push({fare:{},flight:stop});
                        })
                    });

                    currentSegments = tempCurrentSegmentArray;
                }

                var segmentCount = 0;
                _.forEach(currentSegments, function(segment){
                    if(segment.flight
                        && (segment.flight.status != 'CNX')
                        && (segment.flight.travelMode != 'BUS')
                        && !segment.flight.flownSegment){
                        segmentCount++;
                    }
                });

                return segmentCount;
            }

            // Get the combined ancillary model with all the current ancillary
            function getCombinedAncillaryModel(){
                var mode = applicationService.getApplicationMode();
                var currentAncillaryModel = self.getAncillaryModel();

                //If modify segment return combined model
                if(mode === applicationService.APP_MODES.MODIFY_SEGMENT){
                    var unModified = self.getUnmodifiedAnci();
                    if(unModified.SSR_IN_FLIGHT){
                        //Iff there are not SSR in new selection simply use old model
                        if(!currentAncillaryModel.SSR_IN_FLIGHT){
                            currentAncillaryModel = unModified;
                        }else{
//                        	currentAncillaryModel.SSR_IN_FLIGHT.scopes[0].preferences =
//                                currentAncillaryModel.SSR_IN_FLIGHT.scopes[0].preferences.concat(unModified.SSR_IN_FLIGHT.scopes[0].preferences);
                        	if(unModified.SSR_IN_FLIGHT.scopes[0].preferences.length > 0){
                            	var prefernceId = unModified.SSR_IN_FLIGHT.scopes[0].preferences[0].preferanceId;
                                _.forEach(currentAncillaryModel.SSR_IN_FLIGHT.scopes[0].preferences, function(preference){
                                    if(preference.preferanceId == prefernceId){
                                    	preference = unModified.SSR_IN_FLIGHT.scopes[0].preferences;
                                    }
                                });
                        	}

                        }
                    }
                }

                return currentAncillaryModel;
            }

            // Get the count of segments with medical SSR
            function getMedicalAssistantCountForPassenger(currentPassenger, ssrSelection){
                //Return the number of segments containing MESA for a given passenger.
                var passengerMedicalAssistantCount = 0;

                _.forEach(ssrSelection, function(segment){
                    var flight = self.getFlightFromRph(segment.preferanceId);

                    // Ignore flown segments ancillaries
                    if(!!flight && !!flight.flownSegment && !flight.flownSegment){
                        _.forEach(segment.passengers, function(passenger){
                            if(passenger.selectedItems.MEDA && (passenger.passengerRph == currentPassenger.paxSequence)){
                                passengerMedicalAssistantCount++;
                            }
                        });
                    }
                });

                return passengerMedicalAssistantCount;
            }


            function getBookedMedicalAssistantCountForPassenger(currentPassenger, ssrSelection){
                var passengerMedicalAssistantCount = 0;
                _.forEach(ssrSelection, function(segment){
                        _.forEach(segment.passengers, function(passenger){
                            if(passenger.selectedItems.MEDA && (passenger.passengerRph == currentPassenger.paxSequence)){
                                passengerMedicalAssistantCount++;
                            }
                        });
                });

                return passengerMedicalAssistantCount;
            }
            /*
             * END - Validation Medical Ancillary
             * */



            function getValidSegmentCount(ssrSelection){
                //Return the count of valid segments that could contain MESA SSR
                var validSegmentCount = 0;

                _.forEach(ssrSelection, function(segment){
                    var flight = self.getFlightFromRph(segment.preferanceId);

                    if(flight.travelMode !== 'BUS' && !flight.flownSegment){
                        validSegmentCount = validSegmentCount + 1;
                    }
                });

                return validSegmentCount;
            }


            self.getPaymentDisplayAncillary = function() {
                var ancillary = self.getAncillaryRequestJson();
                var ancillaryForPayment = {};

                _.forEach(ancillary.ancillaries, function(anci){
                    var anciType = anci.type;

                    _.forEach(anci.preferences, function(pref){
                        var pax = pref.assignee;
                        pax = getPassengerFromRph(pax.passengerRph);

                        _.forEach(pref.selections, function (selection){
                            var rph = selection.scope.flightSegmentRPH;

                            var details = self.getFlightAdvanceDetails(rph);
                            var selectedItems = selection.selectedItems;

                            ancillaryForPayment[pax.paxSequence] = ancillaryForPayment[pax.paxSequence] || {};
                            ancillaryForPayment[pax.paxSequence].displayName = pax.displayName;

                            ancillaryForPayment[pax.paxSequence].segments = ancillaryForPayment[pax.paxSequence].segments || {};
                            ancillaryForPayment[pax.paxSequence].segments[rph] = ancillaryForPayment[pax.paxSequence].segments[rph] || {};
                            ancillaryForPayment[pax.paxSequence].segments[rph].details = details;

                            ancillaryForPayment[pax.paxSequence].segments[rph].anciList = ancillaryForPayment[pax.paxSequence].segments[rph].anciList || {};
                            ancillaryForPayment[pax.paxSequence].segments[rph].anciList[anciType] = ancillaryForPayment[pax.paxSequence].segments[rph].anciList[anciType] || {};

                            ancillaryForPayment[pax.paxSequence].segments[rph].anciList[anciType] = selectedItems
                        });
                    })
                });

                var itemArrCollection = [];

                _.forEach(ancillaryForPayment, function(passenger){
                    var namePrinted = false;
                    _.forEach(passenger.segments, function(segment){

                        var itemArr = [];

                        if(namePrinted){
                            itemArr.push('');
                        }else{
                            itemArr.push(passenger.displayName);
                            namePrinted = true;
                        }

                        itemArr.push(segment.details);

                        if(segment.anciList['SSR_IN_FLIGHT']){
                            itemArr.push(segment.anciList['SSR_IN_FLIGHT']);
                        }else{
                            itemArr.push('');
                        }

                        if(segment.anciList['AIRPORT_TRANSFER']){
                            itemArr.push(segment.anciList['AIRPORT_TRANSFER']);
                        }else{
                            itemArr.push('');
                        }

                        itemArrCollection.push(itemArr);
                    });
                });

                return itemArrCollection;
            };

            function getPassengerFromRph(rph){
                var paxList = passengerService.getPassengerDetails().paxList;
                return _.find(paxList, function(pax){
                    return rph == pax.paxSequence;
                })
            }

            function setNewDefaultSeat(){


                ancillaryRemoteService.getDefaultSeats('',
                    function success(defaultSeatResponse){


                        ancillaryRemoteService.defaultSeatsSet = false;

                        if(!ancillaryRemoteService.defaultSeatsSet){
                            //setting default seats
                            try{
                                angular.forEach(defaultSeatResponse.ancillaryReservation.passengers, function(passenger){
                                    var currentPaxRph = passenger.passengerRph;
                                    angular.forEach(passenger.ancillaryTypes[0].ancillaryScopes, function(scope){

                                        var defaultSeat = scope.ancillaries[0];
                                        var segmentID = scope.scope.flightSegmentRPH;
                                        var seatPrefs = self.seatPreferance;

                                        var selectedPref = _.find(seatPrefs.scopes[0].preferences,
                                            function (preference) {
                                                if(preference.preferanceId == segmentID){
                                                    return preference;
                                                }
                                            });

                                        var selectedPaxForPref = _.find(selectedPref.passengers,
                                            function(passenger){
                                                if(passenger.passengerRph == currentPaxRph){
                                                    return passenger;
                                                }
                                            });


                                        if(defaultSeat){
                                            selectedPaxForPref.selectedItems = [defaultSeat];
                                        }
                                    });
                                });
                                ancillaryRemoteService.defaultSeatsSet = true;
                            }catch(e){
                                IBEDataConsole.log("Unable to set seat defaults");
                                IBEDataConsole.log(e);
                            }
                        }
                    },
                    function failed(response){
                        var error = {
                            error:msg,
                            errorType:"OTHER_ERROR"
                        };
                        alertService.setPageAlertMessage(error);
                    }
                );
            }
        }];
    return service;
};
