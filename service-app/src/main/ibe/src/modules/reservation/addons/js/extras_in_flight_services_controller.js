var inFlightServicesController = function () {
    var controllers = {};
    controllers.inFlightServicesController = [
        "$location",
        "passengerService",
        "extrasFactory",
        "extrasService",
        "$rootScope",
        "ancillaryRemoteService",
        "loadingService",
        "IBEDataConsole",
        "ancillaryService",
        "alertService",
        "quoteService",
        "languageService",
        "currencyService",
        "applicationService",
        "$state",
        "modifyReservationService",
        "commonLocalStorageService",
        "popupService",
        "navigationService",
        "constantFactory",
        "$translate",
        function ($location, passengerService, extrasFactory, extrasService, $rootScope, ancillaryRemoteService,
                  loadingService, IBEDataConsole, ancillaryService, alertService, quoteService, languageService,
                  currencyService, applicationService, $state, modifyReservationService, commonLocalStorageService,
                  popupService, navigationService,constantFactory,$translate) {
            var self = this;
            var ancillaryType = "SSR_IN_FLIGHT";
            var fqtvSelection = [];
            var ssrApplyBothSegment = [],fqtvServices;

            this.servicesPreference = {}; //ancillary object for services

            this.availableServices = []; //holds available services for the current flight
            this.allInFlightServicesList = []; //holds the backend response with all service information

            this.paxData = {}; //list of passengers used for the passenger list directive;
            this.paxDataCollection = {}; //Collection of paxData objects for each segment
            this.selectedPax = 0; //index of the currently selected passenger
            this.passengerType = 0; //The type of passenger (0-Adult, 1-Child, 2-Infant)
            this.passengerType = 0; //The type of passenger (0-Adult, 1-Child, 2-Infant)
            this.accordionArray = [];
            this.searchCriteria = commonLocalStorageService.getSearchCriteria();
            var sectorCount = 0, appState, urlParams, currentPnr;


            //Used to update the passenger.'assigned' and service.'selected' values on load or sector/passenger change
            //Used 1. when loading the page, 2. changing passenger, 3. changing sector
            this.updateSelectedValues = function () {
                //On set selected values for currently selected passenger and the currently selected sector
                angular.forEach(self.allInFlightServicesList, function (section, key) {
                    angular.forEach(self.allInFlightServicesList[key].items, function (service) {
                        //check if exists in the servicePreference
                        if(!!service) {
                            service.selected = false;
                            if (self.servicesPreference != undefined) {
                                if (_.has(self.servicesPreference.scopes[0].preferences[sectorCount].passengers[self.selectedPax].selectedItems, service.ssrCode)) {
                                    service.selected = true;
                                    if(service.ssrCode == "FQTV") {
                                    	service.input = {};
                                    	service.input.comment = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[self.selectedPax].selectedItems[service.ssrCode].input.comment;                                    	
                                    }                      
                                }
                            }
                        }
                    })
                });

                //Checks if each passenger has values in the current services array and if so, mark them as assigned
                //Used for the passenger list directives tick box
                angular.forEach(self.paxDataCollection[sectorCount], function (passenger, key) {
                    var currentServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[key].selectedItems;

                    if (!_.isEmpty(currentServices)) {
                        passenger.assigned = true;
                    }
                });
            };

            this.onCommentChange = function(val,type){
                if(type == "FQTV") {
                    var item = angular.copy(fqtvServices);
                    item.input = {
                        comment: val,
                        ancillaryType: ancillaryType
                    };
                    fqtvSelection[self.selectedPax] = item;
                }
            }

            //Change sector and associated data based on option list click or from "Go to Next Sector" click
            this.nextSector = function (index, section) {


                _.each(self.servicesPreference.scopes[0].preferences,function(secVal,segkey){
                    if(secVal.segments.length){
                        _.each(secVal.passengers,function(itemVal,paxkey){
                            if(fqtvSelection[paxkey] != undefined && ssrApplyBothSegment[paxkey]) {
                                self.servicesPreference.scopes[0].preferences[segkey].passengers[paxkey].selectedItems["FQTV"] = fqtvSelection[paxkey];
                            }
                        })
                    }
                })


                if (_.isUndefined(index)) {
                    sectorCount++;

                    if (self.servicesPreference.scopes[0].preferences[sectorCount] == undefined) {
                        sectorCount = 0;
                    }

                    self.currentServicePreference = self.servicesPreference.scopes[0].preferences[sectorCount];
                    self.sectionSelection = {
                        index: sectorCount,
                        section: sectorCount
                    };
                } else {
                    self.currentServicePreference = self.servicesPreference.scopes[0].preferences[0];
                    sectorCount = index;
                }
                self.availableServices = self.allInFlightServicesList[sectorCount].items;
                fqtvServices = _.findWhere(self.availableServices, {id: "FQTV"}) || _.findWhere(self.availableServices, {itemId: "FQTV"});
                self.paxData = self.paxDataCollection[sectorCount];
                self.updateSelectedValues();
                self.airportNames = ancillaryService.getAirports(self.servicesPreference, sectorCount);
                var fqtvArry = angular.copy(fqtvSelection);

                _.each(self.availableServices,function(item,k){
                        var id = item.itemId || item.id;
                        if(id == "FQTV" && ssrApplyBothSegment[self.selectedPax]){
                            if(fqtvArry.length) {
                                self.availableServices[k] = fqtvArry[self.selectedPax];
                            }
                        }
                    })
            };

            //On passenger change, change pax type and update the selected service list
            this.passengerChange = function (pax) {

                //Change the pax type
                self.selectedPax = pax;
                if (self.paxData[pax].category === "Adult") {
                    self.passengerType = 0;
                } else if (self.paxData[pax].category === "Child") {
                    self.passengerType = 1;
                } else if (self.paxData[pax].category === "Infant") {
                    self.passengerType = 2;
                }
                var fqtvArry = angular.copy(fqtvSelection);
                _.each(self.availableServices,function(item,k){
                    var id = item.itemId || item.id;
                    if(id == "FQTV" && fqtvArry[pax] != undefined && ssrApplyBothSegment[pax]){
                        if(fqtvArry.length) {
                            self.availableServices[k] = fqtvArry[pax];
                        }
                    }
                })

                self.updateSelectedValues();
            };

            this.optionClick = function (option,index) {
//            	if(typeof index != 'undefined' && !(index >= self.servicesPreference.scopes[0].preferences.length))
//            		sectorCount = index;
                var currentServices =self.servicesPreference.scopes[0].preferences[sectorCount].passengers[self.selectedPax];
                if (currentServices.selectedItems[option.ssrCode]) {
                    delete currentServices.selectedItems[option.ssrCode];
                    if(option.ssrCode == "FQTV") {
                        ssrApplyBothSegment[self.selectedPax] = false;
                    }
                } else {
                    option.id = option.ssrCode;
                    option.quantity = 1;
                    option.input = {
                        comment: "",
                        ancillaryType: ancillaryType
                    };
                    currentServices.selectedItems[option.ssrCode] = option;
                    if(option.ssrCode == "FQTV") {
                        var item = angular.copy(fqtvServices);
                        item.selected = true;
                        item.input = {
                            comment: "",
                            ancillaryType: ancillaryType
                        };
                        fqtvSelection[self.selectedPax] = item;
                    }

                }
                currentServices.itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(currentServices.selectedItems);
                self.paxDataCollection[sectorCount][self.selectedPax].assigned = (!_.isEmpty(currentServices));
                setInFlightServicesTotal();
            };

            this.backToExtras = function () {
                $rootScope.extraType = '';
                if(!isValidMEDA()){
                    console.log('MEDA Invalid');
                }else if (!ancillaryService.getAncillarySelected(ancillaryType)) {
                    var label="";
                     $translate.onReady().then(function() {
                          label = $translate.instant('lbl_common_flightservices_text');
                          if(label === "" || label === 'lbl_common_flightservices_text'){
                            popupService.confirm("You have not selected in-flight services for all sectors, do you want to continue?",
                            function () {
                                ancillaryService.backToExtras();
                            });
                          }else{
                                popupService.confirm(label,
                                    function () {
                                        ancillaryService.backToExtras();
                                    }
                                );
                            }
                });    
                } else {
                    ancillaryService.backToExtras();
                }
            };

            this.skipServices = function () {
                ancillaryService.skipServices("SSR_IN_FLIGHT");
                $rootScope.extraType = '';
            };

            function isValidMEDA() {
                var selectedAnci = ancillaryService.getAncillaryModel();
                var isValidMEDA = ancillaryService.validateMedicalAncillary(selectedAnci);
                if(!isValidMEDA){
                    popupService.alert('MEDA Should either be added for all segments or none.');
                }
                return isValidMEDA;
            }

            /*
             * Delete selected service and update service preference when user click delete icon
             * el - selected object
             * event - event
             * index - selected element index
             */
            this.onDelete = function (el, event, index) {
                var objkey = el.$parent.item.id;

                var currentServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[self.selectedPax].selectedItems;
                delete currentServices[objkey];

                self.paxDataCollection[sectorCount][self.selectedPax].assigned = (!_.isEmpty(currentServices));
                self.updateSelectedValues();
                setInFlightServicesTotal();
            };

            self.backToHome = function() {
                navigationService.modifyFlowHome();
            };

            function setInFlightServicesTotal() {
                angular.forEach(self.servicesPreference.scopes[0].preferences, function (pref, key) {
                    var anciTotal = ancillaryService.getSegmentTotal(pref.passengers);
                    pref.total = anciTotal;
                })
                var segmentTotal = ancillaryService.getAncileryTotal(self.servicesPreference.scopes[0].preferences);
                self.servicesPreference.total = segmentTotal;

            }

            (function init() {
                window.scrollTo(0, 0);

                quoteService.setShowSummaryDrawer(false);
                var param = "SSR_IN_FLIGHT";
                self.appmode = applicationService.getApplicationMode();
                currentPnr = modifyReservationService.getReservationSearchCriteria();
                if (self.appmode == applicationService.APP_MODES.ADD_MODIFY_ANCI || self.appmode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    appState = 'modifyReservation'
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        pnr: currentPnr.pnr,
                        lastName: currentPnr.lastName,
                        departureDate: currentPnr.departureDate
                    }
                } else {
                    appState = 'extras';
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    }
                }

                ancillaryRemoteService.loadInFlightServices(param, function (response) {
                    loadingService.hidePageLoading();
                    if (response == null) {
                        $state.go(appState, urlParams);
                    } else {
                        self.allInFlightServicesList = response;
                        self.availableServices = self.allInFlightServicesList[0].items;
                        fqtvServices = _.findWhere(self.availableServices, {id: "FQTV"}) || _.findWhere(self.availableServices, {itemId: "FQTV"});

                        if (self.appmode == applicationService.APP_MODES.ADD_MODIFY_ANCI || self.appmode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                            if (ancillaryService.getAncillaryModel()[param]) {
                                self.servicesPreference = ancillaryService.getAncillaryModel()[param];
                                ancillaryService.setCurrentTempAncillary(self.servicesPreference);
                            } else {
                                self.servicesPreference = ancillaryService.getAncillaryFromResponse(param, response);
                            }
                        } else {
                            self.servicesPreference = ancillaryService.getAncillaryFromResponse(param, response);
                        }

                        //Set default sector count
                        self.sectionSelection = {index: sectorCount};
                        var passengerList = passengerService.getPassengerDetailsForAnci();

                        angular.forEach(response, function (section, key) {
                            var passengerList = passengerService.getPassengerDetailsForAnci();
                            self.paxDataCollection[key] = (_.extend({}, passengerList));
                        });

                        self.paxData = self.paxDataCollection[sectorCount];
                        var applyFqtvForAllSegments = constantFactory.AppConfigStringValue('applyFqtvForAllSegments')
                        _.each(passengerList,function(pax,key){
                            ssrApplyBothSegment[key] = (applyFqtvForAllSegments) ? true : false;
                        })

                        //setAssigned on load if already selected
                        self.updateSelectedValues();
                        self.airportNames = ancillaryService.getAirports(self.servicesPreference, sectorCount)

                        //self.availableServices ='';
                        //alertService.setAlert('System error occurred', 'warning');

                    }

                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);
                });

            }());
        }];
    return controllers;
};