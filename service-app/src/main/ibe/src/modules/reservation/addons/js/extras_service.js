/**
 * Created by indika on 11/5/15.
 */
var extrasService = function () {
    var service = {};
    service.extrasService = ["commonLocalStorageService", "extrasFactory", "$rootScope", "$location", "passengerService", "httpServices", 
    	"currencyService", "applicationService",
        function (commonLocalStorageService, extrasFactory, $rootScope, $location, passengerService, httpServices, currencyService, applicationService) {

            var self = this;
            this.serviceObject; //Object to hold all the extra services

            // Common method to travers through sectors
            $rootScope.$on('ON_NEXTSECTOR_CLICK', function (e, data) {
                var sector = {};
//                console.log();
                var secCount = data.sectorCount;
                var tripcount = data.trip;
                if (data.index == undefined) {
                    secCount++;

                    if (data.sectorData[Object.keys(data.sectorData)[tripcount]] != undefined) {
                        if (data.sectorData[Object.keys(data.sectorData)[tripcount]][secCount] == undefined) {
                            secCount = 0;
                            tripcount++
                        }
                    } else {
                        tripcount = 0;
                        secCount = 0;
                    }
                    sector.tripType = Object.keys(data.sectorData)[tripcount];
                    sector.currentSector = (data.sectorData[Object.keys(data.sectorData)[tripcount]] != undefined) ? data.sectorData[Object.keys(data.sectorData)[tripcount]][secCount] : data.sectorData[Object.keys(data.sectorData)[0]][secCount];
                    sector.showExtrasTotal = {index: secCount, section: sector.tripType}
                } else {
                    sector.tripType = Object.keys(data.sectorData)[tripcount];
                    sector.currentSector = data.sectorData[data.section][data.index];
                    data.sectorCount = data.index;
                    data.trip = Object.keys(data.sectorData).indexOf(data.section);

                }
                sector.sectorCount = secCount;
                sector.trip = tripcount;
//                console.log('=========NEX CLICK SEE U IN NEXT=========>>>>');
//                console.log(sector)

                return sector;

            });

            $rootScope.$on('ON_BACKTOEXTRAS_CLICK', function (e, secdata) {

                var _lang = angular.element(document)[0].location.hash.split("/")[3];
                var tail = _lang + '/' + currencyService.getSelectedCurrency();
                var path = 'extras/' + tail;
                $location.path(path);

                angular.forEach(secdata, function (sector, seckey) {
                    var currentSec = sector;
                    angular.forEach(currentSec, function (passenger, pasKey) {
                        var curPas = passenger.extrasTotal;
                        var extrasTotal = 0;
                        angular.forEach(curPas, function (value, key) {
                            extrasTotal = extrasTotal + value[Object.keys(value)[0]];
                        });
                        secdata[seckey][pasKey].total = extrasTotal;

                    })
                });
                self.setExtras(secdata);
            });

            $rootScope.$on('ON_SKIP_SERVICES_CLICK', function (e, secdata) {
                var _lang = angular.element(document)[0].location.hash.split("/")[3];
                var tail = _lang + '/' + currencyService.getSelectedCurrency();
                var path = 'extras/' + tail
                $location.path(path);
                self.setExtras(secdata);
            });

            this.setExtras = function (data) {
                commonLocalStorageService.setData('SECTORS', data);
            };

            this.getExtras = function () {

                //If the serviceObject is not defined (ie: The first time getExtras is called) we instantiate new object
                /*if(!self.serviceObject){
                 //If there is data set in the local storage that data is called
                 if(commonLocalStorageService.getData('EXTRAS')){
                 self.serviceObject = commonLocalStorageService.getData('EXTRAS');
                 }
                 //Else, the factory is called in order to generate the sectors
                 else{
                 self.serviceObject = extrasFactory.getSectorModel();
                 }
                 }
                 //else, the already created serviceObject is returned
                 return self.serviceObject;*/
                return passengerService.getPaxDetails()
            };

            this.getAirportName = function (code) {
                return commonLocalStorageService.getAirportCityName(code, false)
            };

            this.setExtrasTotal = function (data) {
                commonLocalStorageService.setData('EXTRAS_TOTAL', data);
            };

            this.getExtrasTotal = function () {
                return commonLocalStorageService.getData('EXTRAS_TOTAL');

            };

            this.calAddonTotalFare = function (extrasModel) {
                var sectorTotal = 0;
                var extrasTotatlFare = {};
                angular.forEach(extrasModel, function (sector, seckey) {
                    var currentSec = sector;
                    angular.forEach(currentSec, function (legs, legKey) {
                        var count = 0;
                        angular.forEach(legs.extrasTotal, function (extras, extKey) {
//                            console.log(extKey)
                            extrasTotatlFare[extKey] = (extrasTotatlFare[extKey] == undefined) ? extrasTotatlFare[extKey] = extras.total : extrasTotatlFare[extKey] + extras.total;
                            count++;
                        })
                    })
                })
//                console.log('===========EXTRAS TOTALS===================');
//                console.log(extrasTotatlFare);
                return extrasTotatlFare;
            };
            this.getSelectedWeight = function (weights) {
                var selectedObj = {}
                for (var i = 0; i < weights.length; i++) {
                    if (weights[i].selected) {
                        selectedObj.weight = weights[i].weight;
                        selectedObj.amount = weights[i].amount;
                        selectedObj.index = i;
                        return selectedObj;
                        break;
                    }
                }

            };
            /*
             function loadBaggage (){
             var weights =[];
             httpServices.sendHttpRequest('GET', "sampleData/baggage_list.json")
             .success(function (results) {
             weights = results;
             }).
             error(function (results) {

             });
             }
             */

            this.getWeight = function (clBack) {
                var weights = [];
                httpServices.sendHttpRequest('GET', "sampleData/baggage_list.json")
                    .success(function (results) {
                        clBack(results);
                    }).error(function (results) {

                });

            };

            this.setIniAncillaryReqData = function () {
                var paxData = commonLocalStorageService.getData('PAX_DETAIL');
                var reqData = {};
                var appMode = applicationService.getApplicationMode();
                var mode = '';
                if (appMode == applicationService.APP_MODES.CREATE_RESERVATION) {
                    mode = 'CREATE';
                } else {
                    mode = 'MODIFY';
                }
                var paxList = paxData.paxList;
                var paxContact = paxData.paxContact;

                //TODO: Passenger data modification should be moved to the PassengerService
                try {
                    paxContact.mobileNo = paxContact.mobileNo.countryCode + paxContact.mobileNo.number;
                    paxContact.mobileNoTravel = paxContact.mobileNoTravel.countryCode + paxContact.mobileNoTravel.number;
                } catch (error) {
//                    console.log(error)
                }

                reqData.reservationData = {};
                reqData.reservationData.mode = mode;
                reqData.reservationData.passengers = [];

                for (var i = 0; i < paxList.length; i++) {
                    var temp = {};
                    temp.title = paxList[i].salutation;
                    temp.firstName = paxList[i].firstName;
                    temp.lastName = paxList[i].lastName;
                    temp.dateOfBirth = paxList[i].dob;
                    temp.nationality = paxList[i].nationality;
                    temp.passengerRph = paxList[i].paxSequence;
                    temp.additionalInfo = paxList[i].additionalInfo;

                    if (paxList[i].category === 'Adult') {
                        temp.type = 'AD';
                    }
                    else if (paxList[i].category === 'Child') {
                        temp.type = 'CH';
                    }
                    else if (paxList[i].category === 'Infant') {
                        temp.type = 'IN';
                        temp.travelWith = paxList[i].travelingWith;
                    }
                    else {
                        temp.type = ''
                    }
                    reqData.reservationData.passengers.push(temp)
                }
                reqData.reservationData.contactInfo = {};
                reqData.reservationData.contactInfo.title = paxContact.salutation;
                reqData.reservationData.contactInfo.firstName = paxContact.firstName;
                reqData.reservationData.contactInfo.lastName = paxContact.lastName;
                reqData.reservationData.contactInfo.dateOfBirth = "";
                reqData.reservationData.contactInfo.nationality = paxContact.nationality;
                reqData.reservationData.contactInfo.address = {
                    "streetAddress1": "",
                    "streetAddress2": "",
                    "city": "",
                    "zipCode": ""
                };
                reqData.reservationData.contactInfo.landNumber = {
                    "countryCode": "",
                    "areaCode": "",
                    "number": ""

                };
                reqData.reservationData.contactInfo.mobileNumber = {
                    "countryCode": paxContact.mobileNo,
                    "areaCode": "",
                    "number": ""

                };
                reqData.reservationData.contactInfo.emailAddress = paxContact.email;
                reqData.reservationData.contactInfo.country = paxContact.country;

                reqData.ancillaries = [
                    {
                        "type": "SEAT"
                    },
                    {
                        "type": "BAGGAGE"
                    },
                    {
                        "type": "MEAL"
                    },
                    {
                        "type": "AUTOMATIC_CHECKIN"
                    },                    
                    {
                        "type": "INSURANCE"
                    },
                    {
                        "type": "SSR_AIRPORT"
                    },
                    {
                        "type": "SSR_IN_FLIGHT"
                    },
                    {
                        "type": "FLEXIBILITY"
                    },
                    {
                        "type": "AIRPORT_TRANSFER"
                    }
                ];

                return reqData;

            }
            this.getAvailableAncyTypes = function (list) {
                var temp = {};
                temp.ancillaries = [];
                for (var i = 0; i < list.availability.length; i++) {
                    if (list.availability[i].available === true) {
                        var t = {};
                        t.type = list.availability[i].type;
                        temp.ancillaries.push(t);
                    }
                }
                return temp;

            }

          

        }];
    return service;
};
