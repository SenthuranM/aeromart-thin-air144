/**
 * Factory for generating and maintains ancillary information.
 */
var ancillaryFactory = function () {
    var factory = {};
    factory.ancillaryFactory = [function () {
        //DTO for a single ancillary type (ex- MEAL, SEAT)
        this.AncillaryItemDTO = function (ancillaryType, initialTotal) {
            this.ancillary = ancillaryType;
            this.total = initialTotal || 0;
            this.scopes = [];
        };

        //DTO for a scope (ex- OND, SEGMENT)
        this.AncillaryScopes = function (scopeType) {
            this.scopeType = scopeType;
            this.total = 0;
            this.preferences = [];
        };

        //DTO for an ancillary preference for a given segment (ex- CMB-SHJ in segment scope)

        this.AncillaryPreference = function (preferenceId, segmentDetails) {
            this.preferanceId = preferenceId;
            this.segments = segmentDetails; //[{"from":"CMB", "to":"CMB"}]
            this.total = 0;
            this.passengers = [];
        };

        //DTO for a passenger inside a preference
        this.AncillaryPassenger = function (passenger, key) {
            this.passenger = passenger;
            this.passengerRph = key;
            this.selectedItems = {};
            this.itmeTotalCharge = 0;
        };

        //Ancillary preferences for insurance
        this.AncillaryInsurancePreference = function(){
            this.selectedItems=[];
            this.total = 0;
        }

        //TODO: changeSector function to switch sectors
        //TODO: simplified function to generate ancillaries using backend requests
    }];
    return factory;
};
