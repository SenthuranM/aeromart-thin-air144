/**
 * Created by indika on 11/5/15.
 */
(function () {
    var pc = new passengerController();
    var passenger = angular.module("passenger", []);

    //directive that checks if the value in the element is equal to another value passed in as attribute
    passenger.directive('isEqual', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                attrs.$observe('isEqual', function () {
                    ctrl.$setValidity('equal', elem.val() === attrs.isEqual);
                });
                scope.$watch(attrs.ngModel, function (v) {
                    ctrl.$setValidity('equal', elem.val() === attrs.isEqual);
                });
            }
        }
    });

    //Directive added to show typeahead dropdown
    passenger.directive('typeaheadFocus', [function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModel) {

                // Array of keyCode values for arrow keys
                //Fixme do not use const in js it si not working in IE
                //const  ARROW_KEYS = [37, 38, 39, 40];
                var  ARROW_KEYS = [37, 38, 39, 40];

                function manipulateViewValue(e) {
                    /* we have to check to see if the arrow keys were in the input because if they were trying to select
                     * a menu option in the typeahead, this may cause unexpected behavior if we were to execute the rest
                     * of this function
                     */
                    if (ARROW_KEYS.indexOf(e.keyCode) >= 0)
                        return;

                    var viewValue = ngModel.$viewValue;

                    //restore to null value so that the typeahead can detect a change
                    if (ngModel.$viewValue == ' ') {
                        ngModel.$setViewValue(null);
                    }

                    if (_.isUndefined(viewValue) || _.isEmpty(viewValue)) {
                        //force trigger the popup
                        ngModel.$setViewValue(' ');
                    }

                    //set the actual value in case there was already a value in the input
                    ngModel.$setViewValue(viewValue || ' ');
                }

                /* trigger the popup on 'click' because 'focus'
                 * is also triggered after the item selection.
                 * also trigger when input is deleted via keyboard
                 */
                element.bind('click keyup', manipulateViewValue);

                //compare function that treats the empty space as a match
                scope.$emptyOrMatch = function (actual, expected) {
                    if (expected == ' ') {
                        return true;
                    }
                    return actual ? actual.toString().toLowerCase().indexOf(expected.toLowerCase()) > -1 : false;
                };
            }
        };
    }]);

    passenger.factory('passengerFactory', [function () {
        return {
            Passenger: function (type, paxSeq) {
                var pax = new passenger();
                pax.category = type;
                pax.paxSequence = paxSeq;
                pax.type = type.toLowerCase();
                if (type === 'Adult') {
                    this.infantSequence = '';
                }
                return pax;
            },
            PaxRegistrationDTO: function () {
                return new paxRegistrationDTO();
            },
            paxSignInDTO: function () {
                return new paxSignInDTO();
            },
            LmsDetailsDTO: function () {
                return new lmsDetailsDTO();
            },
            PaxRegistrationDetailDTO: function () {
                return new paxRegistrationDetailsDTO();
            },
            PaxContactsCongigsParamsDTO: function () {
                return new paxContactConfigsParams();
            }
        };

        function paxContactConfigsParams() {
            this.origin = "";
            this.destination = "";
            this.appCode = "";
            this.carriers = "";
        }

        function paxRegistrationDetailsDTO() {
            this.emailId = "";
            this.password = "";
            this.dateOfBirth = "";
            this.confirmPassword = "";
            this.isJoinAirewards = true;
        }

        function paxRegistrationDTO() {
            this.emailId = "";
            this.password = "";
            this.title = "";
            this.firstName = "";
            this.lastName = "";
            this.nationalityCode = "";
            this.countryCode = "";
            this.telephone = "";
            this.mobile = "";
            this.fax = "";
            this.dateOfBirth = "";
        }

        function paxSignInDTO() {
            this.emailId = "";
            this.password = "";
        }

        function lmsDetailsDTO() {
            this.mobileNumber = "";
            this.headOFEmailId = "";
            this.refferedEmail = "";
            this.genderCode = "";
            this.dateOfBirth = "";
            this.firstName = "";
            this.lastName = "";
            this.passportNum = "";
            this.phoneNumber = "";
            this.nationalityCode = "";
            this.residencyCode = "";
            this.emailId = "";
            this.language = "";
            this.appCode = "";
        }

        function passenger() {
            this.paxSequancce = null;
            this.type = null;
            this.category = null;
            this.passportNo = null;
            this.visaDocIssueDate = null;
            this.lastName = null;
            this.placeOfBirth = null;
            this.travelDocumentType = null;
            this.travelWith = null;
            this.groupId = null;
            this.visaDocNumber = null;
            this.eticketNo = null;
            this.title = null;
            this.salutation = null;
            this.passportExpiry = null;
            this.firstName = null;
            this.nationalIDNo = null;
            this.passportIssuedCntry = null;
            this.ffid = null;
            this.nationality = null;
            this.visaDocPlaceOfIssue = null;
            this.dob = null;
            this.visaApplicableCountry = null;
            this.additionalInfo = {
                foidInfo: {
                    foidNumber: null
                }
            }
        }
    }]);

    var paxService = new passengerService();

    passenger.controller(pc);
    passenger.service(paxService, [commonServices]);

    //ui datepicker validation fix for setting the required setting
    passenger.directive('uiDateFix', ['$timeout', function ($timeout) {
        return {
            restrict: "A",
            require: 'ngModel',
            priority: 100,
            scope: {
                ngRequired: "="
            },
            link: function (scope, elem, attr, ngModel) {
                var originalValidate = null;
                $timeout(function () {
                    if (!originalValidate)
                        originalValidate = ngModel.$validators.uiDateValidator;
                    ngModel.$validators.uiDateValidator = function uiDateValidator2(modelValue, viewValue) {
                        //Define correct validations
                        if (viewValue || scope.ngRequired)
                            return originalValidate(modelValue, viewValue);
                        else
                            return true;
                    };
                    ngModel.$validate();
                }, 0);
            }
        }
    }]);

})();

