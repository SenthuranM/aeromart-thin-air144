/**
 * Created by Baladewa on 11/05/15.
 */
var passengerController = function () {

    var controllers = {};

    controllers.passengerController = [
        'stepProgressService',
        'alertService',
        'loadingService',
        '$location',
        'constantFactory',
        'passengerService',
        'passengerFactory',
        'httpServices',
        'customerRemoteService',
        'passengerRemoteService',
        'IBEDataConsole',
        'applicationService',
        'languageService',
        'modifyReservationService',
        '$stateParams',
        'quoteService',
        'currencyService',
        '$anchorScroll',
        'masterRemoteService',
        '$state',
        'modificationRemoteService',
        'commonLocalStorageService',
        '$timeout',
        'navigationService',
        'signInService',
        'registrationService',
        'popupService',
        'eventListeners',
        'datePickerService',
        'DTO',
        '$filter',
        '$scope',
        'remoteViewProfile',
        'travelFareService',
        '$translate',
        'remoteMaster',
        'configsService',
        '$rootScope',
        '$window',
        'formValidatorService',
        'signoutServiceCheck',
        function (stepProgressService, alertService, loadingService, $location, constantFactory, passengerService,
                passengerFactory, httpServices, customerRemoteService, passengerRemoteService, IBEDataConsole,
                applicationService, languageService, modifyReservationService, $stateParams, quoteService,
                currencyService, $anchorScroll, masterRemoteService, $state, modificationRemoteService,
                commonLocalStorageService, $timeout, navigationService, signInService, registrationService,
                popupService, eventListeners, datePickerService, DTO, $filter, $scope, remoteViewProfile, travelFareService, $translate, remoteMaster, configsService, $rootScope,$window, formValidatorService,signoutServiceCheck) {


            stepProgressService.setCurrentStep('passenger');
            if(!$rootScope.deviceType.isMobileDevice){
                $rootScope.removePaddingFromXSContainerClass=true;
            }

            // Global variables.
            var self = this;
            var paxContact = null;
            
          //Functions for Modal
			this.model = {
				isPopupOpen : false

			};
			self.selectedPaxCount = commonLocalStorageService.getSearchCriteria();
			self.adultAgeCutOff = 0;
			self.childAgeCutOff = 0;
			self.infantAgeCutOff = 0;
            self.flightData = commonLocalStorageService.getConfirmedFlightData();
            self.firstDepartureDate = "";
            self.selectedFamilyMembersPaxTypeCount = {adult:0, child:0, infant:0};
            if(self.flightData !== null){
            	self.firstDepartureDate = self.flightData[0].flight.departureDate;
            }
            
			self.open = function() {
				this.model = {
					isPopupOpen : true
				};
				self.setNationalityAndRelationship();
			}
			self.close = function() {
				this.model = {
					isPopupOpen : false

				};

			};
			
			self.display={
					nationalityCode:"",
					nationalityName:"",
					passFirstName:"",
					passLastName:"",
					passDateOfBirth:"",
                    emailId:"",
                    selected: false
			}

            self.setNationalityAndRelationship = function () {
                var localUserDetails = commonLocalStorageService.getUserSignInData();
                //To display existing User Details in Modal
                if (!angular.equals({}, localUserDetails)) {
                    for (var i = 0; i < self.allSalutationList.length; i++) {
                        if (self.allSalutationList[i].titleCode == localUserDetails.loggedInCustomerDetails.title) {
                        	self.display.titleName = self.allSalutationList[i].titleName;
                            break;
                        }
                    }
                    self.display.passFirstName = localUserDetails.loggedInCustomerDetails.firstName;
                    self.display.passLastName = localUserDetails.loggedInCustomerDetails.lastName;
                    self.display.emailId = localUserDetails.loggedInCustomerDetails.emailId;
                    self.display.nationalityName = localUserDetails.loggedInCustomerDetails.nationalityName;
                    if(localUserDetails.loggedInCustomerDetails.dateOfBirth) {
                        self.display.passdateOfBirth = moment(localUserDetails.loggedInCustomerDetails.dateOfBirth).local().format("L");
                    } else {
                        self.display.passdateOfBirth = "";
                    }
                    
                    //remote call to fetch family details
                    self.relationshipListData = remoteMaster.retrieveRelationshipLists()
                        .then(function (response) {
                            if (response.data.success) {
                                self.relationshipList = response.data.relationshipList;
                                // To get data of User Profile
                                self.familyMembersDetails = remoteViewProfile.getFamilyMembersDetails()
                                    .then(function (response) {
                                        mapNationalityAndRelationship(response.data.familyMemberDTOList);
                                        anotateLoggedInUser();
                                        anotatePopulatedUsers();
                                    }, function (error) {
                                        console.log(error);
                                    });
                            }
                        }, function (error) {
                            console.log(error);
                        });
                }
            };
	    					
            function anotateLoggedInUser() {

                var isLoggedInFoundInFamily = false;

                self.familyMemberList.forEach(function (familyMember) {
                    if (familyMember.firstName === self.display.passFirstName
                        && familyMember.lastName === self.display.passLastName
                        && moment(familyMember.dateOfBirth).local().format("L") === self.display.passdateOfBirth) {

                        familyMember.isNotLoggedInUser = false;
                        isLoggedInFoundInFamily = true;
                    } else {
                        familyMember.isNotLoggedInUser = true;
                    }
                });

                if (!isLoggedInFoundInFamily) {

                    var localUserDetails = commonLocalStorageService.getUserSignInData();
                    var registredDOB = "";
                    if(localUserDetails.loggedInCustomerDetails.dateOfBirth) {
                        registredDOB = moment(localUserDetails.loggedInCustomerDetails.dateOfBirth).format("MM/DD/YYYY")
                    }

                    self.familyMemberList.push({
                        firstName: localUserDetails.loggedInCustomerDetails.firstName,
                        lastName: localUserDetails.loggedInCustomerDetails.lastName,
                        dateOfBirth: registredDOB,
                        nationalityCode: localUserDetails.loggedInCustomerDetails.nationality,
                        nationalityName: localUserDetails.loggedInCustomerDetails.nationalityName,
                        isNotLoggedInUser: false
                    });
                }
            }

            function anotatePopulatedUsers() {

                var isLoggedInUserSelected = false;

                self.familyMemberList.forEach(function (familyMember) {
                    for (var i = 0; i < self.passengers.length; i++) {
                        var populatedPassenger = self.passengers[i];
                        if (familyMember.firstName === populatedPassenger.firstName
                            && familyMember.lastName === populatedPassenger.lastName
                            && (familyMember.dateOfBirth === moment(populatedPassenger.dob).format("MM/DD/YYYY") 
                                || (!familyMember.isNotLoggedInUser && familyMember.dateOfBirth === "") )) {
                            familyMember.selected = true;
                            // if logged in user also included in the current passenger list
                            if (!familyMember.isNotLoggedInUser) {
                                isLoggedInUserSelected = true;
                            }
                            break;
                        }
                    }
                });

                //if logged in user not included in the current passegenger list, then reset the selected flag
                self.display.selected = isLoggedInUserSelected;
            }            
       	  	
            //function to map nationality code and nationality name along with relationship
            function mapNationalityAndRelationship(data) {
                self.familyMemberList = data;
                for (var j = 0; j < data.length; j++) {
                    self.familyMemberList[j].dateOfBirth = moment(data[j].dateOfBirth).local().format("L");
                }
                for (var j = 0; j < data.length; j++) {
                    for (var i = 0; i < self.relationshipList.length; i++) {
                        if (self.relationshipList[i].id == data[j].relationshipId) {
                            self.familyMemberList[j].relationshipName = self.relationshipList[i].name;
                            break;
                        }
                    }
                }
                for (var j = 0; j < data.length; j++) {
                    for (var i = 0; i < self.nationalityList.length; i++) {
                        if (self.nationalityList[i].code == data[j].nationalityCode) {
                            self.familyMemberList[j].nationalityName = self.nationalityList[i].name;
                            break;
                        }
                    }
                }
                for (var j = 0; j < data.length; j++) {
                    for (var i = 0; i < self.allSalutationList.length; i++) {
                        if (self.allSalutationList[i].titleCode == data[j].title) {
                            self.familyMemberList[j].titleName = self.allSalutationList[i].titleName;
                            break;
                        }
                    }
                }
            }

			//Modal functionality Ends

            var dateFormat = "DD/MM/YYYY";
            var localStorage = window.localStorage;
            self.isMobileDevice = $rootScope.deviceType.isMobileDevice;
            self.appConfigs = configsService.clientConfigs.configs;
            self.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');
            self.languagepref = languageService.getSelectedLanguage();
            self.isThreeTwentyDevice = $window.matchMedia("only screen and (max-width: 320px)").matches
            self.sendPromoEmailChecked = true;

            // Get FORM VALIDATIONS
            self.contactValidation = formValidatorService.getValidations();
            self.classInput='';
            self.contact = {
                firstName: "",
                lastName: "",
                email: "",
                verifyEmail: "",
                mobileCountryCode: "",
                mobileNo: {countryCode: "", number: "", areaCode: ""},
                mobileNoTravel: {countryCode: "", number: "", areaCode: ""},

                salutation: "",
                language: languageService.getSelectedLanguage(),
                nationality: "",
                country: "",

                travelMobile: true,
                address: {
                    city: null,
                    streetAddress1: null,
                    streetAddress2: null,
                    zipCode: null
                },

                password: "",
                confirmPassword: "",
                isJoinAirewards: "",
                state: "",
                taxRegNo: "",
                displayNationalityModal: "",
                sendPromoEmail: self.sendPromoEmailChecked
            };
            self.passengerChangeRequest = {
                contactInfo: {},
                pnr: "",
                groupPnr: true,
                version: ""
            };

            self.curencyTypes = commonLocalStorageService.getCurrencies();
            self.defaultCurrencyType = currencyService.getSelectedCurrency() || "AED";

            self.showModifySearch = function () {
                eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
            };

            self.recoveryReqDTO = DTO.passwordRecoveryRequest;
            self.maxDate = angular.copy(moment().subtract(12, 'years').subtract(1, 'days')).toDate();
            self.minDate = angular.copy(moment().subtract(100, 'years').add(1, 'days')).toDate();

            // Properties.
            self.isRegisterChecked;
            self.applicationMode = applicationService.getApplicationMode();
            self.paxDetailsCompleted = false;
            self.MODE = $stateParams.mode;
            self.isLogin = false;
            self.displayLogin = false;
            self.selectedPassenger = 0;
            self.paxSignInDetails = passengerFactory.paxSignInDTO();
            self.lmsDetails = passengerFactory.LmsDetailsDTO();
            self.contactParms = {};
            self.paxParms = {};
            self.firstPaxAsContact = true;
            self.passengers;
            self.originalPassengers;
            self.paxModificationParams; // Name change modification parameters
            self.paxDetailsCompleted;
            self.paxTypeWiseSalutation = constantFactory.PaxTypeWiseTitle();
            self.salutationList = self.paxTypeWiseSalutation["AD"];
            
            self.childSalutationList = self.paxTypeWiseSalutation["CH"];
            self.infantSalutationList = self.paxTypeWiseSalutation["IN"];
            self.allSalutationList = self.salutationList;
			for(var i=0;i<self.childSalutationList.length;i++){
				var elementFound = false;
				for(var j=0;j<self.allSalutationList.length;j++){
					if(self.childSalutationList[i].titleCode==self.allSalutationList[j].titleCode){
						elementFound = true;
						break;
					}
				}
				if(!elementFound) {
					self.allSalutationList.push(self.childSalutationList[i]);
				}
			}
			for(var i=0;i<self.infantSalutationList.length;i++){
				var elementFound = false;
				for(var j=0;j<self.allSalutationList.length;j++){
					if(self.infantSalutationList[i].titleCode==self.allSalutationList[j].titleCode){
						elementFound = true;
						break;
					}
				}
				if(!elementFound) {
					self.allSalutationList.push(self.infantSalutationList[i]);
				}
			}
			
            self.languageList = constantFactory.Langauges();
            self.nationalityList = constantFactory.Nationalities();
            self.countryList = constantFactory.Countries();
            self.dateformat = datePickerService.setDateFormat(4); // Holds date format for calendar controllers.
            self.isBusyLoaderVisible = false; // Holds busy loader visible status.
            self.isForgetPassword = false; // Holds forget password panel visibility.
            self.forgetPasswordEmail; // Holds recovery email address.
            self.airwards = {}; // Holds data for airewards registration.
            self.isLoginProccessing = false;
            self.airewardsCheckedStatus = [];
            self.originalContact = {}; // Holds original contact details in contact change flow
            self.countryWiseStates = constantFactory.CountryWiseStates();
            console.log(self.countryWiseStates)
            self.statesList = [];

            self.showState = false;
            self.showTaxRegNo = false;
            self.taxRegNoAdded = false;
            self.disableState = false;
            self.disableTaxRegNo = false;
            self.disableCountry = false;
            self.showAddress = false;
            self.isServiceTaxApplicable = false;
            self.taxRegNumberLabel = constantFactory.AppConfigStringValue('taxRegistrationNumberLabel') || 'lbl_common_taxRegNo';
            var strTaxRegNoEnabledCountries = constantFactory.AppConfigStringValue('taxRegNoEnabledCountries');
            var taxRegNoEnabledCountries = _.isEmpty(strTaxRegNoEnabledCountries) ? [] : strTaxRegNoEnabledCountries.split(',');
            
            self.dateOptions = {
                changeYear: true,
                changeMonth: true,
                minDate: self.minDate,
                maxDate: self.maxDate,
                dateFormat: 'dd/mm/yy',
                weekHeader: "Wk",
                yearRange: "-100:+0",
                showOn: "both",
                buttonImage: "images/calendar.png"
            };


            // Functions.
            self.backToHome = backToHome; // remove ?

            self.changeLoginDisplay = changeLoginDisplay;
            self.onCountryChange = onCountryChange;

            self.showLangTitle = showLangTitle;
            self.formatLabelCountry = formatLabelCountry;
            self.formatLabelNationality = formatLabelNationality;
            self.changeTravelMobile = changeTravelMobile;
            self.mobileChanged = mobileChanged;
            self.setSalutation = setSalutation;
            self.setLanguage = setLanguage;
            self.setFamilyEmailStatus = setFamilyEmailStatus;
            self.dpOnClick = dropdownOnClick;

            self.checkForDuplicates = checkForDuplicates; // Continue To Extras button click.
            self.submitSignIn = submitSignIn;
            self.getForgetPassword = getForgetPassword;

            self.updateContact = updateContact;
            self.airewardsChecked = airewardsChecked; // remove ?
            self.onNameChangeCompleted = onNameChangeCompleted;
            self.isContactFieldValidated = isContactFieldValidated;
            self.toggleFirstPaxContact = toggleFirstPaxContact;
            self.formatLabelLanguage = formatLabelLanguage;
            self.onLanguageChange = onLanguageChange;
            self.backToModifyReservation = backToModifyReservation;

            self.clearAllPassengerDetails = clearAllPassengerDetails;

            self.startsWith = startsWith;
            self.resetValidationData = resetValidationData;
            self.validPhoneProvided = validPhoneProvided;
            self.namePhoneTraveller= 'phone';
            self.nameMobileTraveller= 'mobile';

            self.registerCheckChange = registerCheckChange;
            self.setSecretQuestion = setSecretQuestion;
            self.showForgetPasswordPanel = showForgetPasswordPanel;
            self.isForgetPassword = false; // Holds forget password panel visibility.
            self.isValidCareerFor = isValidCareerFor;
            // From AA	
            self.formatLabelState = formatLabelState;
            self.sendPromoEmailChange = sendPromoEmailChange;
            self.populateData = populateData;

            /**
             *  Init function.
             */
            (function () {
            	//function to fetch relationship data from server
                self.setNationalityAndRelationship();
                //Check for modification flow
                if (self.applicationMode === 'nameChange') {
                	signoutServiceCheck.checkSessionExpiry();
                }
                if (self.applicationMode === 'changeContactInformation') {
                	signoutServiceCheck.checkSessionExpiry();
                    self.firstPaxAsContact = false;
                    self.isServiceTaxApplicable = modifyReservationService.getReservation() &&
                        modifyReservationService.getReservation().taxInvoiceGenerated;

                    if (self.isServiceTaxApplicable) {
                        self.disableState = true;
                        self.disableTaxRegNo = true;
                        self.disableCountry = true;
                    }
                }
                console.log(currencyService.getLoyaltyManagementStatus());
                self.isLoyaltyManagementEnabled = currencyService.getLoyaltyManagementStatus();
                quoteService.setShowSummaryDrawer(true);

                //Tic the 'I will keep the same mobile number..' checkbox'
                changeTravelMobile();

                // Get pax configurations and enable/disable applicable fields.
                // GET Passenger configuration
                var prams = passengerService.getParamsWithData();
                if (self.applicationMode !== applicationService.APP_MODES.MODIFY_SEGMENT &&
                    self.applicationMode !== applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    self.ageCutOff = remoteMaster.retrievePassengerDetailParams(prams).then(
                        function (response) {
                            self.adultAgeCutOff = parseInt(response.data.paxCutOffYears.adultAgeCutOverYears);//99
                            self.childAgeCutOff = parseInt(response.data.paxCutOffYears.childAgeCutOverYears);//12
                            self.infantAgeCutOff = parseInt(response.data.paxCutOffYears.infantAgeCutOverYears);//2
                        }, function (error) {
                            console.log(error);
                        });
                }

                self.phoneNumberConfigs = {countryCode: true, areaCode: false, phoneNumber: true};

                //TODO: Check for contact detail change as well
                if (self.applicationMode !== applicationService.APP_MODES.NAME_CHANGE) {
                    if (self.applicationMode !== applicationService.APP_MODES.MODIFY_SEGMENT &&
                        self.applicationMode !== applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                        masterRemoteService.retrievePassengerContactParams(prams).then(function (response) {
                            self.contactParms = response.data.contactDetails;

                            self.isServiceTaxApplicable = response.data.serviceTaxApplicable;
                            if(self.isServiceTaxApplicable){
                                self.showState = true;
                                self.showTaxRegNo = true;
                            }else{
                                self.showState = false;
                                self.showTaxRegNo = false;
                            }
                            var val = {
                                    code: self.contact.country,
                                    phoneCode :  self.contact.mobileNo.countryCode
                                };
                            onCountryChange(val);

                        }).catch(function (error){
                            sessionValidation(error.data);
                        });

                        if (self.applicationMode !== applicationService.APP_MODES.CONTACT_CHANGE) {
                            masterRemoteService.retrievePassengerDetailParams(prams).then(function (response) {
                                self.paxParms = response.data.paxDetailsConfig;
                                if (travelFareService.isjourneyTypeLocal()) {
                                    response.data.paxDetailsConfig.nationalIDNo.mandatory = {
                                        "adult": false,
                                        "child": false,
                                        "infant": false
                                    }
                                }
                            }).catch(function (error){
                                sessionValidation(error.data);
                            });
                        }
                    }
                } else {
                    self.paxParms = {
                        "firstName": {
                            "fieldName": "firstName",
                            "visibility": {
                                "adult": true,
                                "child": true,
                                "infant": true
                            },
                            "mandatory": {
                                "adult": true,
                                "child": true,
                                "infant": true
                            },
                            "maxLength": 0,
                            "validation": ""
                        },

                        "lastName": {
                            "fieldName": "lastName",
                            "visibility": {
                                "adult": true,
                                "child": true,
                                "infant": true
                            },
                            "mandatory": {
                                "adult": true,
                                "child": true,
                                "infant": true
                            },
                            "maxLength": 0,
                            "validation": ""
                        },

                        "title": {
                            "fieldName": "title",
                            "visibility": {
                                "adult": true,
                                "child": true,
                                "infant": false
                            },
                            "mandatory": {
                                "adult": true,
                                "child": true,
                                "infant": false
                            },
                            "maxLength": 0,
                            "validation": ""
                        },

                        "dob": {
                            "fieldName": "dob",
                            "visibility": {
                                "adult": true,
                                "child": true,
                                "infant": true
                            },
                            "mandatory": {
                                "adult": false,
                                "child": false,
                                "infant": true
                            },
                            "maxLength": 0,
                            "validation": ""
                        },

                        "nationality": {
                            "fieldName": "nationality",
                            "visibility": {
                                "adult": true,
                                "child": true,
                                "infant": false
                            },
                            "mandatory": {
                                "adult": true,
                                "child": true,
                                "infant": false
                            },
                            "maxLength": 0,
                            "validation": ""
                        },
                        "nationalIDNo": {
                            "fieldName": "nationalIDNo",
                            "visibility": {
                                "adult": true,
                                "child": true,
                                "infant": false
                            },
                            "mandatory": {
                                "adult": true,
                                "child": true,
                                "infant": false
                            },
                            "maxLength": 0,
                            "validation": ""
                        },

                        "ffid": {
                            "fieldName": "ffid",
                            "visibility": {
                                "adult": true,
                                "child": true,
                                "infant": false
                            },
                            "mandatory": {
                                "adult": false,
                                "child": false,
                                "infant": false
                            },
                            "maxLength": 0,
                            "validation": ""
                        },

                        "travelWith": {
                            "fieldName": "travelWith",
                            "visibility": {
                                "adult": false,
                                "child": false,
                                "infant": true
                            },
                            "mandatory": {
                                "adult": false,
                                "child": false,
                                "infant": true
                            },
                            "maxLength": 0,
                            "validation": ""
                        },
                    }
                }



                var passengerDetails = passengerService.getPassengerDetails();

                // Set dob from string toDate in the passenger controller
                angular.forEach(passengerDetails.paxList, function (value) {
                    if (value.dob) {
                        value.dob = moment(value.dob).toDate();
                    }
                });


                /**
                 *  If the passenger page has been visited before and, the passenger details are saved,
                 *  the details will be extracted from the passengerService.
                 *
                 *  Else if the passenger count has been setup in the searchCriteria, the passenger details will be
                 *  generated from the count of Adults, Children and Infants.
                 */

                // If the passenger page has been visited before and, the passenger details are saved,
                // the details will be extracted from the passengerService.
                if (passengerDetails && Object.keys(passengerDetails).length != 0) {
                    self.passengers = passengerDetails.paxList;
                    self.contact = passengerDetails.paxContact;
                    self.paxDetailsCompleted = passengerDetails.paxDetailsCompleted;

                    //Modification MODE
                    if (self.applicationMode == applicationService.APP_MODES.NAME_CHANGE) {

                        //Keep original Pax list to compare with Changes
                        self.originalPassengers = angular.copy(passengerDetails.paxList);

                        // Retrieve list of modification criteria
                        self.paxModificationParams = modifyReservationService.getReservation().modificationParams.all.paxModificationParams;

                        _.forEach(self.passengers, function (passenger, key) {
                            if (passenger.airewardsEmail) {
                                self.airewardsCheckedStatus[key] = true;
                            }
                        });

                        _.map(self.paxModificationParams, function (param, key) {
                            var passenger = _.find(self.passengers, function (passenger) {
                                return passenger.paxSequence === param.paxSequence
                            });
                            passenger.rules = param;
                        });

                        //Hide Summery Drawer
                        quoteService.setShowQuoteStatus(false);
                        quoteService.setShowSummaryDrawer(false);

                    } // End of if.

                }
                // Else if the passenger count has been setup in the searchCriteria, the passenger details will be
                // generated from the count of Adults, Children and Infants*/
                else if (Object.keys(passengerService.getSearchCriteria()).length != 0) {

                    var searchCriteria = passengerService.getSearchCriteria(),
                            i;

                    self.passengers = [];

                    var paxSeq = 1;

                    for (i = 0; i < searchCriteria.adult; i++) {
                        self.passengers.push(passengerFactory.Passenger('Adult', paxSeq++));
                    }
                    for (i = 0; i < searchCriteria.child; i++) {
                        self.passengers.push(passengerFactory.Passenger('Child', paxSeq++));

                    }
                    for (i = 0; i < searchCriteria.infant; i++) {
                        self.passengers.push(passengerFactory.Passenger('Infant', paxSeq++));
                    }
                }

                if (signInService.getLoggedInStatus()) {
                    var localUserDetails = commonLocalStorageService.getUserSignInData();
                    var signInUserId = localUserDetails.loggedInCustomerDetails.emailId;
                    self.isLogin = true;
                    // Salutation is checked since the user can't reset the value salutation to null manually
                    if (self.passengers[0].salutation === null) {
                        signInService.isServerHasLoggedInUser({loginId: signInUserId.toLowerCase()}, function (data) {
                            self.isUserLoggedIn = data.customerLoggedIn;
                            if (data.customerLoggedIn) {
                                setUserDetails(localUserDetails);
                            }
                        }, function () {

                        })
                    }
                }

                // If in modification flow retrive and set the contact.
                if (self.applicationMode === 'changeContactInformation') {

                    var reservationDetails = modifyReservationService.getReservation();

                    self.contact = reservationDetails.contactInfo;
                    self.statesList = self.contact.country && self.countryWiseStates[self.contact.country];
                    self.passengerChangeRequest.contactInfo = self.contact;

                    var address = self.contact.address;
                    self.contact.address = address;

                    // Adapting the reservationDetails.contactInfo to contact object
                    self.contact.language = reservationDetails.contactInfo.preferredLangauge;
                    // From AA
                    if (self.contact.taxRegNo) {
                        self.showAddress = true;
                        var address = self.contact.address.streetAddress1;
                        self.contact.address = address;
                    }

                    if (self.contact.mobileNumber) {

                        var mobileNoObject = createMobileObject(self.contact.mobileNumber);
                        var mobileNoTravelObject = createMobileObject(self.contact.telephoneNumber);
                        self.passengerChangeRequest.contactInfo.mobileNo = mobileNoObject.countryCode + mobileNoObject.number;

                        if (mobileNoObject.countryCode == mobileNoTravelObject.countryCode && 
                        		mobileNoObject.number == mobileNoTravelObject.number) {
                        	self.contact.travelMobile = false;
                        	self.contact.mobileNoTravel = self.contact.mobileNo = mobileNoObject;
                        } else {
                        	self.contact.travelMobile = true;
                        	self.contact.mobileNo = mobileNoObject;
                        	self.contact.mobileNoTravel = mobileNoTravelObject;
                        }

                    }
                    self.contact.sendPromoEmail = self.sendPromoEmailChecked = reservationDetails.contactInfo.sendPromoEmail;



                    self.passengerChangeRequest.contactInfo.salutation = self.contact.title;
                    self.passengerChangeRequest.contactInfo.email = self.contact.emailAddress;
                    self.passengerChangeRequest.contactInfo.nationality = self.contact.nationality + "";

                    self.passengerChangeRequest.groupPnr = reservationDetails.bookingDetails.groupPnr;
                    self.passengerChangeRequest.pnr = reservationDetails.bookingDetails.pnr;
                    self.passengerChangeRequest.version = reservationDetails.bookingDetails.version;


                    self.originalContact = angular.copy(self.contact); // Copy original contact information
                    //Hide Summery Drawer
                        quoteService.setShowQuoteStatus(false);
                        quoteService.setShowSummaryDrawer(false);

                    if ((_.contains(taxRegNoEnabledCountries, self.contact.country))) {
                        self.showState = true;
                        self.showAddress = true;
                        self.showTaxRegNo = true;
                    }

                }

                self.airewardsCheckedStatus = [];

                angular.forEach(self.passengers, function (pax) {
                    self.airewardsCheckedStatus.push(!!pax.ffid);
                });

                loadingService.hidePageLoading();

            }()); // End of init function.


            /**
             *  Called when user login status changed.
             */
            eventListeners.registerListeners(eventListeners.events.CUSTOMER_LOGIN_STATUS_CHANGE, function () {
                // Show registration checkbox when user signout from the page.
                self.isLogin = false;
            });

            function createMobileObject(mobileNumber) {
            	var mobileObject = {countryCode: "", number: ""};
            	if (mobileNumber) {
            		mobileObject.countryCode = mobileNumber.countryCode;
            		mobileObject.number = mobileNumber.areaCode + mobileNumber.number
            	}
            	return mobileObject;
            }
            
            function isValidCareerFor(airlineName) {
                return applicationService.isValidCareerFor(airlineName);
            }

            function backToHome() {
                if (applicationService.getApplicationMode() == applicationService.APP_MODES.CREATE_RESERVATION) {
                    navigationService.createFlowHome();
                } else if (
                        (applicationService.getApplicationMode() == applicationService.APP_MODES.NAME_CHANGE) ||
                        (applicationService.getApplicationMode() == applicationService.APP_MODES.CONTACT_CHANGE)
                        ) {
                    navigationService.modifyFlowHome();
                }
            }

            function setSecretQuestion(question) {
                self.forgetPasswordQuestion = question;
            }

            function showForgetPasswordPanel(evt) {
                evt.preventDefault()
                self.isForgetPassword = true;
                remoteMaster.retriveForgetPasswordParams({}).then(function (responese) {
                    console.log(responese)
                    self.secretQuestionList = responese.data.questionInfo
                })
            }


            /**
             *  Hide login Overlay.
             */
            function changeLoginDisplay() {
            	
                // If login is still processing show Overlay.
                if (self.isLoginProccessing) {
                    return;
                }

                if (!self.isLogin) {
                    self.displayLogin = !self.displayLogin;
                }
            }


            function closeLoginDisplay() {
                if (self.displayLogin) {
                    self.displayLogin = !self.displayLogin;
                    self.isForgetPassword = false;
                }
            }


            function onCountryChange(val) {
                if(self.contactParms.mobileNo.visibility) {
                    self.contact.mobileCountryCode = val.phoneCode;
                    //Resets mobile number to reflect the coutnry code
                    self.contact.mobileNo.countryCode = val.phoneCode;
                    if(val.phoneCode !== ""){
                        //Resets mobile number to reflect the coutnry code
                             self.contact.mobileNo.countryCode = val.phoneCode;
                        if(!self.contact.travelMobile)
                            self.contact.mobileNoTravel.countryCode = val.phoneCode;
                    }
                }
                
                // From AA
                if(self.countryWiseStates){
                    self.statesList = self.countryWiseStates[val.code];
                }

                changeServiceTaxDetailsOnCountryChange(val.code);

            }


            function changeServiceTaxDetailsOnCountryChange(countryCode) {
                self.statesList = self.countryWiseStates[countryCode];
                if (_.contains(taxRegNoEnabledCountries, countryCode) && _.contains(taxRegNoEnabledCountries, self.flightData[0].flight.originCountryCode)) {
                    self.showTaxRegNo = true;
                    self.showState = true;
                    self.showAddress = true;
                } else {
                    self.showTaxRegNo = false;
                    self.showState = false;
                    self.showAddress = false;
                    self.contact.taxRegNo = null;
                    self.contact.state = null;

                    if (self.contactParms && self.contactParms.address && !self.contactParms.address.visibility) {
                        self.contact.address = null;
                    }

                }
            }

            function toggleFirstPaxContact(newVal) {

                if (self.firstPaxAsContact) {
                    //Set first pax details as contact information
                    self.contact.firstName = self.passengers[0].firstName;
                    self.contact.lastName = self.passengers[0].lastName;
                    self.contact.salutation = self.passengers[0].salutation;
                    self.contact.nationality = self.passengers[0].nationality;
                } else {
                    //Empty values for contact
                    self.contact.firstName = null;
                    self.contact.lastName = null;
                    self.contact.salutation = "";
                    self.contact.nationality = null;
                }

            } // End of toggleFirstPaxContact.

            /**
             *  Format label for language.
             */
            function formatLabelLanguage(model) {
                var tempN = self.languageList;
                if (tempN) {
                    for (var i = 0; i < tempN.length; i++) {
                        if (model === tempN[i].languageCode) {
                            return tempN[i].language;
                            break;
                        }
                    }
                }
            }


            function onLanguageChange(lang) {
                self.airwards.language = lang.languageCode;
            }

            function dropdownOnClick($event, opened, isEnabled) {
                if (isEnabled) {
                    self[opened] = datePickerService.open();
                }
            }


            /**
             *  Calculate year difference between provided date to set
             *  required state.
             */
            function setFamilyEmailStatus(dob) {
                if (!dob) {
                    return;
                }

                var currentYear = new Date().getFullYear(),
                        birthYear = new Date(dob).getFullYear();

                if ((currentYear - birthYear) < 18) {
                    self.isFamilyEmailRequired = true;
                } else {
                    self.isFamilyEmailRequired = false;
                }
            }


            /**
             *  Set salutation titleCode.
             *  filter will convert titleCode titleName.
             */
            function setSalutation(salutation) {
                self.contact.salutation = salutation;
            }


            function setLanguage(language) {
                self.contact.language = language.languageCode;
            }


            function showLangTitle(code) {
                for (var i = 0; i < self.languageList.length; i++) {
                    if (self.languageList[i].languageCode === code) {
                        return self.languageList[i].language;
                        break;
                    }
                }
            }


            function formatLabelCountry(model) {
                var tempN = self.countryList;
                if (tempN) {
                    for (var i = 0; i < tempN.length; i++) {
                        if (model === tempN[i].code) {
                            return tempN[i].name;
                            break;
                        }
                    }
                }
            }


            function formatLabelNationality(model) {
                var tempN = self.nationalityList;
                if (tempN) {
                    for (var i = 0; i < tempN.length; i++) {
                        if (model === tempN[i].code) {
                            return tempN[i].name;
                            break;
                        }
                    }
                }
            }


            function changeTravelMobile() {
                if (self.contact.travelMobile == true) {
                    self.contact.travelMobile = false;
                    self.contact.mobileNoTravel = self.contact.mobileNo;
                } else {
                    self.contact.travelMobile = true;
                    self.contact.mobileNoTravel = {countryCode: "", number: ""};
                }
            }

            function formatLabelState(model) {
                var tempN = self.statesList;
                if (tempN) {
                    for (var i = 0; i < tempN.length; i++) {
                        if (model === tempN[i].stateCode) {
                            return tempN[i].stateName;
                            break;
                        }
                    }
                }
            }


            function mobileChanged() {
                if (!self.contact.travelMobile) {
                    self.contact.mobileNoTravel = self.contact.mobileNo;
                }

            }

            function sessionValidation(data){
            	if (data.errorType === alertService.ERRORTYPE.TRANSACTION_ID_ERROR || applicationService.getTransactionID() == null) {
                    alertService.setPageAlertMessage(data);
                } else {
                    alertService.setAlert(data.error, 'danger');
                }
            }

            /**
             *  Get forget password by providing the registered email.
             *  @param emailAddress: recovery email address.
             */
            function getForgetPassword(forgetPasswordForm) {

                if (!forgetPasswordForm.$valid) {
                    return;
                }

                self.recoveryReqDTO.customerID = self.forgetPasswordEmail;
                self.recoveryReqDTO.secretQuestion = self.forgetPasswordQuestion;
                self.recoveryReqDTO.secretAnswer = self.forgetPasswordAnswer;

                passengerService.getForgetPassword(self.recoveryReqDTO, function (data) {

                    if (!data.success) {
                        alertService.setAlert("lbl_passenger_somethingWrong.", "danger");
                        return;
                    }

                    // Email sent successfully.
                    closeLoginDisplay();

                    // Switch view back to default login view.
                    self.isForgetPassword = false;

                    if (data.messages && data.messages.length) {
                        alertService.setAlert(data.messages[0], "success")
                    }

                }, function (data) {
                    // Request failed.
                });

            } // End of getForgetPassword.


            /**
             *  Sign in button click.
             */
            function submitSignIn(signInForm) {

                if (!signInForm.$valid) {
                    return;
                }

                var signInReq = {};
                signInReq = self.paxSignInDetails;
                signInReq.page = 'interline';
                signInUser(signInReq);
            }
            this.getDir = function () {
                var trlEnabled = ['ar', 'fa'], rtlValue = false;
                if (trlEnabled.indexOf(languageService.getSelectedLanguage()) > -1) {
                    rtlValue = true;
                }
                return rtlValue;
            };



            /**
             *  Close sign in popup and sign in user.
             */
            function signInUser(signInReqData) {

                self.loginButtonText = "Log in...";
                self.isLoginProccessing = true;

                signInService.signInUser(signInReqData, function (results) {

                    self.loginButtonText = "Log in";
                    self.isLoginProccessing = false;

                    closeLoginDisplay();

                    if (!results.success) {
                        if (results.messages && results.messages.length > 0) {
                            alertService.setAlert(results.messages[0], 'warning', callback);
                        }
                        return;
                    }

                    setUserDetails(results);

                }, function (error) {

                });
            }


            /**
             *  Set signed in user's details.
             */
            function setUserDetails(userDetails) {
                if (_.isEmpty(userDetails) || (self.applicationMode == applicationService.APP_MODES.NAME_CHANGE)) {
                	self.isLogin = true;
                    return;
                }

                // Set login status to set ui behaviours.
                // Hide registration checkbox.
                self.isLogin = true;

                var contactDetails = userDetails.loggedInCustomerDetails;
                var lmsDetails = userDetails.loggedInLmsDetails;

                self.loginUserName = contactDetails.firstName + " " + contactDetails.lastName;

                self.open();
 
                self.setNationalityAndRelationship();
                
                $window.localStorage.removeItem('PNR_LOGIN');
            } // End of setUserDetails.

            function populateData() {
                this.model = {
                    isPopupOpen: false
                };

                self.updateLoggedInFamilyMemberSelection();
                if (self.validateSelectedFamilyMembers()) {
                	self.fillMatchedPassengerDetails();
                    self.setContactInfomationFromLoggedInUser();
                    self.selectedPassenger = 0;
                    for( var i=0; i < self.passengers.length; i++) {
                        var passenger = self.passengers[i];
                        if (passenger.firstName === null || passenger.firstName === undefined
                            || passenger.lastName === null || passenger.lastName === undefined) {
                            self.passengers[i].valid = false;
                        } else if (passenger.category !== "Infant" && passenger.salutation !== null
                            && passenger.nationality !== null) {
                            self.passengers[i].valid = true;
                        }
                    }
                }
                
            }

            self.getContactPersonFullName = function() {
                var userDetails = commonLocalStorageService.getUserSignInData();
                if(userDetails) {
                    var contactDetails = userDetails.loggedInCustomerDetails;
                    if(contactDetails) {
                        return contactDetails.title + " " + contactDetails.firstName + " " + contactDetails.lastName;
                    }
                }
                return self.passengers[0].displayName;
            }

            self.setContactInfomationFromLoggedInUser = function() {

                var userDetails = commonLocalStorageService.getUserSignInData();
                var contactDetails = userDetails.loggedInCustomerDetails;

                if(!self.contact.email) {

                    self.contact.email = (contactDetails.emailId).toLowerCase();
                    self.contact.verifyEmail = (contactDetails.emailId).toLowerCase();
                    self.contact.salutation = contactDetails.title;
                    self.contact.firstName = contactDetails.firstName;
                    self.contact.lastName = contactDetails.lastName;
                    self.contact.nationality = contactDetails.nationality;
                    self.contact.country = contactDetails.country;
                    self.contact.mobileNo = contactDetails.mobile;
                    self.contact.sendPromoEmail = self.sendPromoEmailChecked = contactDetails.sendPromoEmail;
                    var val = {
                            code: self.contact.country,
                            phoneCode : self.contact.mobileNo.countryCode
                        };
                    onCountryChange(val);
                    // From AA
                    changeServiceTaxDetailsOnCountryChange(self.contact.country);

                    if (contactDetails.mobile) {
                        self.contact.mobileNo.number = self.contact.mobileNo.areaCode + "" + self.contact.mobileNo.number;
                    }
                    self.contact.mobileNoTravel = contactDetails.mobile;
                }
            }

            self.validateSelectedFamilyMembers = function () {
            	self.selectedFamilyMembersPaxTypeCount = {adult: 0, child: 0, infant: 0};

            	//check selected passengers for validation
            	self.passengers.forEach(function (passenger) {
            		if (isSelectedFamilyMember(passenger)) {
            			if (passenger.category === 'Adult') {
                			self.selectedFamilyMembersPaxTypeCount.adult += 1;
                		}
                		if (passenger.category === 'Child') {
                			self.selectedFamilyMembersPaxTypeCount.child += 1;
                		}
                		if (passenger.category === 'Infant') {
                			self.selectedFamilyMembersPaxTypeCount.infant += 1;
                		}
            		}
            	});

            	self.familyMemberList.forEach(function (familyMember) {
                	if (familyMember.selected && familyMember.isLoggedInUser) {
                		if (self.isValidAdultMember(familyMember)) {
                            self.selectedFamilyMembersPaxTypeCount.adult += 1;
                            if (self.selectedFamilyMembersPaxTypeCount.adult > parseInt(self.selectedPaxCount.adult)) {
                            	familyMember.selected = false;
                            	self.display.selected = false;
                            }
                        }
                	}
                });

                self.familyMemberList.forEach(function (familyMember) {
                	if (familyMember.selected && familyMember.isNotLoggedInUser) {
                        if (self.isValidAdultMember(familyMember)) {
                            self.selectedFamilyMembersPaxTypeCount.adult += 1;
                            if (self.selectedFamilyMembersPaxTypeCount.adult > parseInt(self.selectedPaxCount.adult)) {
                            	familyMember.selected = false;
                            }
                        }
                        else if (self.isValidChildMember(familyMember)) {
                            self.selectedFamilyMembersPaxTypeCount.child += 1;
                            if (self.selectedFamilyMembersPaxTypeCount.child > parseInt(self.selectedPaxCount.child)) {
                            	familyMember.selected = false;
                            }
                        }
                        else if (self.isValidInfantMember(familyMember)) {
                            self.selectedFamilyMembersPaxTypeCount.infant += 1;
                            if (self.selectedFamilyMembersPaxTypeCount.infant > parseInt(self.selectedPaxCount.infant)) {
                            	familyMember.selected = false;
                            }
                        }
                    }
                });
                if (self.selectedFamilyMembersPaxTypeCount.adult + self.selectedFamilyMembersPaxTypeCount.child + self.selectedFamilyMembersPaxTypeCount.infant >
                    parseInt(self.selectedPaxCount.adult) + parseInt(self.selectedPaxCount.child) + parseInt(self.selectedPaxCount.infant)) {
                    self.selectedFamilyMembersPaxTypeCount = {adult: 0, child: 0, infant: 0};
                    $translate("lbl_validation_selected_family_members_count_mismatch").then(function (msg) {
                        popupService.alert(msg);
                        self.reOpen();
                        return false;
                    });
                } else if (self.selectedFamilyMembersPaxTypeCount.adult > parseInt(self.selectedPaxCount.adult) || self.selectedFamilyMembersPaxTypeCount.child >
                    parseInt(self.selectedPaxCount.child) || self.selectedFamilyMembersPaxTypeCount.infant > parseInt(self.selectedPaxCount.infant)) {
                    self.selectedFamilyMembersPaxTypeCount = {adult: 0, child: 0, infant: 0};
                    $translate("lbl_validation_selected_family_members_age_mismatch").then(function (msg) {
                        popupService.alert(msg);
                        self.reOpen();
                        return false;
                    });
                }
                return true;
            }
            
            self.reOpen = function() {
				this.model = {
					isPopupOpen : true
				};
			}
            
            function isSelectedFamilyMember(passenger) {
            	if (passenger.firstName != null && passenger.firstName != "" 
        			&& passenger.lastName != null && passenger.lastName != "") {
            		for( var i=0; i < self.familyMemberList.length; i++) {
                        var familyMember = self.familyMemberList[i];
                        if(familyMember.firstName === passenger.firstName
                            && familyMember.lastName === passenger.lastName
                            && (moment(familyMember.dateOfBirth).local().format("L") === moment(passenger.dob).local().format("L")
                                || familyMember.dateOfBirth === "")) {
                            return familyMember.selected;
                        }
                    }
            	}     
                return false;
            }

            self.updateLoggedInFamilyMemberSelection = function () {
                for (var i = 0; i < self.familyMemberList.length; i++) {
                    var familyMember = self.familyMemberList[i];
                    if (familyMember.firstName === self.display.passFirstName
                        && familyMember.lastName === self.display.passLastName
                        && (moment(familyMember.dateOfBirth).local().format("L") === self.display.passdateOfBirth
                            || familyMember.dateOfBirth === "")) {

                        familyMember.isLoggedInUser = true;
                        if (self.display.selected) {
                            familyMember.selected = true;
                        } else {
                            familyMember.selected = false;
                        }
                        break;
                    }
                }
            }

            self.aFamilyMemberIsSelected = function () {
            	if(self.familyMemberList !== undefined){
	                for (var i = 0; i < self.familyMemberList.length; i++) {
	                    var familyMember = self.familyMemberList[i];
	                    if (familyMember.selected == true) {
	                        return true;
	                    }
	                }
            	}
                return false;
            }

            self.familyMembersAvailableToDisplay = function () {
                var familyMembersAvailable = false;
                if(self.familyMemberList !== undefined && self.familyMemberList.length > 1){
                    for (var i = 0; i < self.familyMemberList.length; i++) {
                        var familyMember = self.familyMemberList[i];
                        if (familyMember.isNotLoggedInUser == false) {
                            familyMembersAvailable = true;
                        }
                    }
                }
                return familyMembersAvailable;
            }

            self.fillMatchedPassengerDetails = function() {
                // remove the unselected passegners first, to get vacant places as much as possible
                self.familyMemberList.forEach(function (familyMember) {
                    if(!familyMember.selected) {
                        self.removeFilledPassenger(familyMember);
                    }
                }); 
                // fill logged in user first
                self.familyMemberList.forEach(function (familyMember) {
                    if(familyMember.selected && familyMember.isLoggedInUser) {
                        self.populateAutoFillablePassenger(familyMember);
                    }
                });
                // fill the newly selected passengers secondly
                self.familyMemberList.forEach(function (familyMember) {
                    if(familyMember.selected) {
                        self.populateAutoFillablePassenger(familyMember);
                    }
                });                
            }

            self.removeFilledPassenger = function (familyMember) {
                var categoryPaxCount = {};
                for( var i=0; i < self.passengers.length; i++) {
                    var matchedPassenger = self.passengers[i];
                    if(_.isUndefined(categoryPaxCount[matchedPassenger.category])){
                        categoryPaxCount[matchedPassenger.category] = 1;
                    }else{
                        categoryPaxCount[matchedPassenger.category] = categoryPaxCount[matchedPassenger.category] + 1;
                    }

                    if(familyMember.firstName === matchedPassenger.firstName
                        && familyMember.lastName === matchedPassenger.lastName
                        && moment(familyMember.dateOfBirth).local().format("L") === moment(matchedPassenger.dob).local().format("L")) {
                    	self.passengers[i] = passengerFactory.Passenger(matchedPassenger.category, matchedPassenger.paxSequence);
                        setTranslatedDefaultDisplayName(self.passengers[i], categoryPaxCount);
                    }
                } 
            }

            self.populateAutoFillablePassenger = function (familyMember) {
                if (self.isValidAdultMember(familyMember)) {
                    self.populatePassengerFromAdultMember(familyMember);
                } else if (self.isValidChildMember(familyMember)) {
                    self.populatePassengerFromChildMember(familyMember);
                } else if (self.isValidInfantMember(familyMember)) {
                    self.populatePassengerFromInfantMember(familyMember);
                }
            }

            self.isValidAdultMember = function(familyMember) {
                var familyMemeberAge = self.adultAgeCutOff - 1;
                if(familyMember.dateOfBirth !== "") {
                    familyMemeberAge = moment(self.firstDepartureDate, dateFormat).diff(familyMember.dateOfBirth, 'years', false);
                }
                var isvalidAge = familyMemeberAge >= self.childAgeCutOff && familyMemeberAge < self.adultAgeCutOff;
                var isUnpopulated = self.isUnpopulatedMember(familyMember);
                return isvalidAge && isUnpopulated;
            }

            self.isValidChildMember = function(familyMember) {
                var familyMemeberAge = moment(self.firstDepartureDate, dateFormat).diff(familyMember.dateOfBirth, 'years', false);
                var isvalidAge = familyMemeberAge < self.childAgeCutOff && familyMemeberAge >= self.infantAgeCutOff;
                var isUnpopulated = self.isUnpopulatedMember(familyMember);
                return isvalidAge && isUnpopulated;
            }

            self.isValidInfantMember = function(familyMember) {
                var familyMemeberAge = moment(self.firstDepartureDate, dateFormat).diff(familyMember.dateOfBirth, 'years', false);
                var isvalidAge = familyMemeberAge < self.infantAgeCutOff;
                var isUnpopulated = self.isUnpopulatedMember(familyMember);
                return isvalidAge && isUnpopulated;
            }
            
            self.isUnpopulatedMember = function(familyMember) {
                for( var i=0; i < self.passengers.length; i++) {
                    var travellingPassenger = self.passengers[i];
                    if(familyMember.firstName === travellingPassenger.firstName
                        && familyMember.lastName === travellingPassenger.lastName
                        && (moment(familyMember.dateOfBirth).local().format("L") === moment(travellingPassenger.dob).local().format("L")
                            || familyMember.dateOfBirth === "")) {
                        return false;
                    }
                }                
                return true;
            }

            self.populatePassengerFromAdultMember = function (familyMember) {
                for( var i=0; i < self.passengers.length; i++) {
                    var matchablePassenger = self.passengers[i];
                    if(matchablePassenger.category === 'Adult' 
                        && ( matchablePassenger.firstName == null || matchablePassenger.firstName === "" 
                        	|| matchablePassenger.dob == null || matchablePassenger.dob === ""  
                        	|| matchablePassenger.lastName == null || matchablePassenger.lastName === "" 
                        	|| matchablePassenger.nationality == null || matchablePassenger.nationality === "" )) {

                        self.populatePassengerFromFamilyMember(matchablePassenger, familyMember);
                        break;
                    }
                }
            }
            
            self.populatePassengerFromChildMember = function (familyMember) {
                for( var i=0; i < self.passengers.length; i++) {
                    var matchablePassenger = self.passengers[i];
                    if(matchablePassenger.category === 'Child'  
                        && ( matchablePassenger.firstName == null || matchablePassenger.firstName === "")) {

                        self.populatePassengerFromFamilyMember(matchablePassenger, familyMember);
                        break;
                    }
                }
            }
            
            self.populatePassengerFromInfantMember = function (familyMember) {
                for( var i=0; i < self.passengers.length; i++) {
                    var matchablePassenger = self.passengers[i];
                    if(matchablePassenger.category === 'Infant' 
                        && ( matchablePassenger.firstName == null || matchablePassenger.firstName === "")) {
                            
                        self.populatePassengerFromFamilyMember(matchablePassenger, familyMember);
                        break;
                    }
                }
            }              

            self.populatePassengerFromFamilyMember = function (matchablePassenger, familyMember) {
            	matchablePassenger.salutation = familyMember.title;;
                matchablePassenger.firstName = familyMember.firstName;
                matchablePassenger.lastName = familyMember.lastName;
                matchablePassenger.nationality = familyMember.nationalityCode;
                if(familyMember.dateOfBirth !== "") {
                    matchablePassenger.dob = moment(familyMember.dateOfBirth).toDate();
                }

                if(familyMember.isLoggedInUser) {

                    var localUserDetails = commonLocalStorageService.getUserSignInData();
                    var lmsDetails = localUserDetails.loggedInLmsDetails;

                    if (localUserDetails.loggedInLmsDetails) {
                        if (localUserDetails.loggedInLmsDetails.emailConfirmed) {
                            matchablePassenger.isLMSStatus = "CONFIRMED";
                            self.airewardsCheckedStatus[0] = true;
                            matchablePassenger.airewardsEmail = lmsDetails ? lmsDetails.ffid.toLowerCase() : "";
                        } else {
                            matchablePassenger.isLMSStatus = "PENDING";
                        }
                    } else {
                        matchablePassenger.isLMSStatus = "NOTREGISTERD";
                    }

                    matchablePassenger.salutation = localUserDetails.loggedInCustomerDetails.title;
                    matchablePassenger.displayName = matchablePassenger.salutation + " " 
                        + matchablePassenger.firstName + " " + matchablePassenger.lastName;
                } else {
                    matchablePassenger.displayName = matchablePassenger.firstName + " " + matchablePassenger.lastName;
                    matchablePassenger.isLMSStatus = null;
                    matchablePassenger.airewardsEmail = "";
                }
            }            

            /**
             *  Update contact information using the first pax in the passenger list.
             */
            function updateContact(passenger) {
                if (passenger.salutation)
                    self.contact.salutation = passenger.salutation;
                if (passenger.firstName)
                    self.contact.firstName = passenger.firstName;
                if (passenger.lastName)
                    self.contact.lastName = passenger.lastName;
                if (passenger.nationality)
                    self.contact.nationality = passenger.nationality;
                if (passenger.dob)
                    self.contact.dob = passenger.dob;
            }

            function resetValidationData(data) {
                console.log(data)
                self.passengers[data.index][data.valFieald] = false;
            }
            function airewardsChecked() {
                self.contact.isJoinAirewards = !self.contact.isJoinAirewards;
            }

            eventListeners.registerListeners(eventListeners.events.CONTINUE_AFTER_SUMMERY, function (data) {
                checkForDuplicates(self.passengerContact);
            });

            /**
             *  Continue to extras button click.
             *  Need to check for duplicate passengers.
             */
            function checkForDuplicates(passengerContact) {

                if (!_.isEmpty(self.originalContact.country) &&
                    !_.contains(taxRegNoEnabledCountries, self.originalContact.country) &&
                    _.contains(taxRegNoEnabledCountries, self.contact.country)) {
                    alertService.setAlert("lbl_validation_contact_country", 'warning', callback);
                    return;
                }
            	
            	if(!validateStateAndTaxRegNo()){
            		return;
            	}
            	
                if (passengerContact.$invalid) {
                    var message = "lbl_passenger_fillAllContactDetail";
                    alertService.setAlert(message, 'warning', callback);
                    return;
                }

                var allPassengersValid = true;
                var foundInvalid = false;

                for (var person = 0; person < self.passengers.length; person++) {

                    // Skip bellow section in change contact in formation in modifyflow.
                    if (self.applicationMode === "changeContactInformation") {
                        self.passengers[person].valid = true;
                    }

                    self.passengers[person].submitted = true;

                    if (!self.passengers[person].valid) {
                        allPassengersValid = false;

                        if (!foundInvalid) {
                            self.selectedPassenger = person;
                            foundInvalid = true;
                        }
                    }
                }

                if (!allPassengersValid) {
                    hideBusyLoader();

                    var message = "lbl_passenger_fillAllPaxDetail";
                    alertService.setAlert(message, 'warning', callback);

                    var old = $location.hash();
                    $location.hash('passengers-information-panel');
                    $anchorScroll.yOffset = 100;
                    $anchorScroll();
                    $location.hash(old);
                    $timeout(function() {
                    	var wrappedResult =  angular.element(document.querySelectorAll("#passengers-information-panel span .checkbox-circle"));
                    	wrappedResult[self.selectedPassenger].click();
                    }, 0);
                    return;
                }

                commonLocalStorageService.setData('contact_email', self.contact.email);

                // Show busy loader.
                self.isBusyLoaderVisible = true;

                if (self.applicationMode !== 'changeContactInformation') {
                    paxContact = passengerContact;
                    passengerRemoteService.duplicateCheck(self.passengers, duplicateCheckSuccess, function (data) {
                        hideBusyLoader();
                        if (data.errorType === alertService.ERRORTYPE.TRANSACTION_ID_ERROR) {
                            alertService.setPageAlertMessage(data);
                        } else {
                            alertService.setAlert(data.error, 'danger');
                        }

                    });
                } else {
                	
                	if(!validateAddress()){
                		return false;
                	}
                	
                    //Keeping original changed contact values without adapting
                    var passengerChangeReqCopy = angular.copy(self.passengerChangeRequest);

                    // Before submitting, remove leading zero from the area-code
                    if (self.contact.mobileNo.number && self.contact.mobileNo.number.charAt(0) == "0") {
                        self.contact.mobileNo.number = self.contact.mobileNo.number.substring(1);
                    }

                    if (self.contact.mobileNoTravel.number && self.contact.mobileNoTravel.number.charAt(0) == "0") {
                        self.contact.mobileNoTravel.number = self.contact.mobileNoTravel.number.substring(1);
                    }

                    modificationRemoteService.changeContactInformation(self.passengerChangeRequest).then(function (response) {
                        IBEDataConsole.log("Contact change request successful");

                        // Setting search criteria if last name has changed.
                        var modifySearch = modifyReservationService.getReservationSearchCriteria();
                        var changedLastName = self.passengerChangeRequest.contactInfo.lastName || modifySearch.lastName;
                        modifyReservationService.setReservationSearchCriteria(modifySearch.pnr, changedLastName, modifySearch.departureDate);

                        var contactInfoChanged = false;

                        _.forEach(passengerChangeReqCopy.contactInfo, function (val, key) {
                            if (_.isObject(self.originalContact[key])) {
                                if (!_.isEqual(self.originalContact[key], val)) {
                                    contactInfoChanged = true;
                                }

                            } else {
                                // Equality operator used to check only values not type
                                // Nationality and other values could be converted to string when modifying
                                if (self.originalContact[key] != val) {
                                    contactInfoChanged = true;

                                }
                            }
                        });

                        if (contactInfoChanged) {
                            alertService.setAlert("lbl_passenger_contactChangeSuccess", 'success');
                        }
                        modifyReservationService.reloadReservation();
                        hideBusyLoader();

                    }, function (error) {
                        alertService.setAlert('lbl_passenger_contactChangeError', 'warning');
                        modifyReservationService.reloadReservation();
                        hideBusyLoader();
                    });
                }
            }

            function validateStateAndTaxRegNo(){
            	
            	if(self.showState && (self.contact.state == null || self.contact.state == '')){
                    var message = "lbl_passenger_stateEmpty";
                    alertService.setAlert(message, 'warning', callback);
                	angular.element( document.querySelector( '#contactState' ) ).addClass('ng-invalid');
                    return false;
                }

            	if(self.contact.taxRegNo != null && self.contact.taxRegNo.length >= 2 && self.contact.taxRegNo.substring(0,2) !== self.contact.state.trim()){
                    var message = "lbl_passenger_taxRegNoDoesNotMatchState";
            		angular.element( document.querySelector( '#contactTaxRegNo' ) ).addClass('ng-invalid');
                    alertService.setAlert(message, 'warning');
                    return false;
                }

            	if(self.contact.taxRegNo != null && self.contact.taxRegNo != '' && self.contact.taxRegNo.length != 15){
                    var message = "lbl_passenger_invalidTaxRegNo";
                    alertService.setAlert(message, 'warning');
            		angular.element( document.querySelector( '#contactTaxRegNo' ) ).addClass('ng-invalid');
                    return false;
                }

            	if(!validateAddress()){
                    return false;
                }

                return true;

            }

            function validateAddress() {
                if (self.showAddress && (self.contact.address === undefined || self.contact.address == null || self.contact.address == '')) {
                    var message = "lbl_passenger_addressIsMandatoryForGST";
                    alertService.setAlert(message, 'warning', callback);
                    angular.element(document.querySelector('#contactAddress')).addClass('ng-invalid');
                    return false;
                }

                return true;
            }

            function duplicateCheckSuccess(response) {

                var allPassengersValid = true;

                if (response.hasDuplicates) {

                    hideBusyLoader();

                    var message = "lbl_passenger_duplicateNames";
                    alertService.setAlert(message, 'warning', callback);
                    allPassengersValid = false;
                } else {

                    if (self.passengers) {
                        checkLmsPassengers(allPassengersValid);
                    } else {
                        submitContact();
                    }
                }

            }


            function checkLmsPassengers(allPassengersValid) {
                passengerRemoteService.lmsPassengerCheck(self.passengers, lmsPassengerCheckSuccess, function (data) {
                    if (data) {
                        alertService.setAlert(data.error, 'danger');
                    } else {
                        alertService.setAlert('lbl_passenger_airewardsValidationFailed', 'danger');
                    }

                    hideBusyLoader();

                }, allPassengersValid);
            }


            function lmsPassengerCheckSuccess(ffidCheckList, paxValidationList, passengersValid) {

                // check for suceess..
                // if fails, give an warning message and stop execution

                var allPassengersValid = true
                var passengerNames = [];
                var pendingPaxName = [];
                _.each(paxValidationList, function (item, index) {
                    self.passengers[index].lmsValidation = item;
                    self.passengers[index].isLMSValidationDone = true;
                    if (!item.validFFID || !item.validName) {

                        passengerNames.push({name: ffidCheckList[index].firstName});
                    }
                    if (item.validFFID && item.validName && allPassengersValid) {
							if (!item.emailConfirmed) {
								pendingPaxName.push({
									name : ffidCheckList[index].firstName
								});
							}
                        allPassengersValid = true;
                    } else {
                        allPassengersValid = false
                    }
                })

                //Setting verifyEmail to the same as email
                self.contact.verifyEmail = self.contact.email;

                if (allPassengersValid) {
                	if(pendingPaxName.length > 0){
	                   	popupService.showPopupPendingAirrewards('airewardsemail.pending.tpl.html', {passengers: pendingPaxName}, function selectAccept() {
	                        $timeout(function () {
	                            var element = $("#passengers-information-panel");
	                            $("html, body").animate({
	                                scrollTop: element.offset().top - 94
	                            }, 600);
	                        }, 10);
	                        submitContact();
	                    });
                	} else {
                		submitContact();
                	}
                } else {
                    popupService.showPopup('airewards.validation.tpl.html', {passengers: passengerNames}, function selectAccept() {
                        $timeout(function () {
                            var element = $("#passengers-information-panel");
                            $("html, body").animate({
                                scrollTop: element.offset().top - 94
                            }, 600);
                        }, 10);

                    });
                    hideBusyLoader();
                }
            }
            var recentScroll= false;
            $scope.scrollToNationality = function () {
                if (self.isMobileDevice) {
                    var element = $('#nationalityInContact');
                    $("html, body").animate({
                        scrollTop: element.offset().top - 94
                    }, 600);
                } else {
                     //do nothing
                }
            };


            function submitContact() {
                if (paxContact.$valid) {

                    //If the date is given in string format convert it into a date object
                    for (var pax in self.passengers) {
                        if (/^\d{2}\/\d{2}\/\d{4}/.test(self.passengers[pax].dob)) {
                            self.passengers[pax].dob = moment(self.passengers[pax].dob, "DD/MM/YYYY").toDate();
                        }

                        if (!_.isUndefined(self.passengers[pax].dob) && !_.isNull(self.passengers[pax].dob)) {
                            // On date change, the value is to be converted to the UTF 0000
                            var formattedDate = moment(self.passengers[pax].dob).format(dateFormat);
                            self.passengers[pax].dob = moment.utc(formattedDate, dateFormat).toDate();
                        }
                    }
                    self.contact.preferredCurrency = currencyService.getSelectedCurrency();

                    // Before submitting, remove leading zero from the area-code
                    if (self.contact.mobileNo.number && self.contact.mobileNo.number.charAt(0) == "0") {
                        self.contact.mobileNo.number = self.contact.mobileNo.number.substring(1);
                    }
                    if (self.contact.mobileNoTravel.number && self.contact.mobileNoTravel.number.charAt(0) == "0") {
                        self.contact.mobileNoTravel.number = self.contact.mobileNoTravel.number.substring(1);
                    }

                    passengerService.setPaxDetails({
                        paxList: self.passengers,
                        paxContact: self.contact,
                        paxDetailsCompleted: self.paxDetailsCompleted
                    });


                    // If user already login to the system, skip registration part.
                    if (!signInService.getLoggedInStatus() && self.isRegisterChecked) {
                        registerAndLoginUser();
                    } else {

                        hideBusyLoader();

                        // Go to extras page.
                        continueToExtras();
                    }

                }
            } // End of submitContact.


            function registerAndLoginUser() {

                // Get area code from mobile number.
                var number = self.contact.mobileNo.number,
                        areaCode = number.substr(0, 2);

                number = number.substr(2, number.length);

                var userParams = {

                    "customer": {
                        "title": self.contact.salutation,
                        "firstName": self.contact.firstName,
                        "lastName": self.contact.lastName,
                        "customerId": self.contact.email,
                        "password": self.contact.password,
                        "gender": null,
                        "alternativeEmailId": null,
                        "dateOfBirth": null,
                        "zipCode": self.zipCode,
                        "countryCode": self.contact.country,
                        "nationalityCode": self.contact.nationality,
                        "status": null,
                        "secretQuestion": null,
                        "secretAnswer": null,
                        "customerQuestionaire": null,
                        "customerContact": {
                            "address": {
                                "addresline": null,
                                "addresStreet": null,
                                "city": null
                            },
                            "emailAddress": self.contact.email,
                            "mobileNumber": {
                                "countryCode": self.contact.mobileNo.countryCode,
                                "areaCode": areaCode,
                                "number": number
                            },
                            "telephoneNumber": null,
                            "fax": null,
                            "emergencyContact": null,
                            "sendPromoEmail": self.sendPromoEmailChecked
                        }
                    },
                    "lmsDetails": {
                        appCode: "APP_IBE",
                        dateOfBirth: "",
                        ffid: self.contact.email,
                        language: ""
                    },
                    "mashreqDetails": null,
                    "optLMS": true,
                };

                // Get lms details.
                angular.extend(userParams.lmsDetails, self.airwards);

                var LMSValidationReq = DTO.LMSValidationRequest;
                LMSValidationReq.emailId = self.contact.email;
                LMSValidationReq.firstName = self.contact.firstName;
                LMSValidationReq.lastName = self.contact.lastName;
                LMSValidationReq.headOfEmailId = self.airwards.headOFEmailId;
                LMSValidationReq.refferedEmailId = self.airwards.refferedEmailId;
                LMSValidationReq.registration = true;

                customerRemoteService.validateLMSDetails(LMSValidationReq, function (response) {

                    if (!response.success) {
                        alertService.setAlert(response.messages[0], 'danger', callback);
                        hideBusyLoader();
                        return;
                    }

                    if (response.accountExists) {
                        alertService.setAlert(response.messages[0], 'danger', callback);
                        return;
                    } else if (response.lmsNameMismatch) {
                        alertService.setAlert(response.messages[0], 'danger', callback);
                        return;
                    } else if (!response.existingFamilyHeadEmail && self.airwards.headOFEmailId) {
                        alertService.setAlert(response.messages[0], 'danger', callback);
                        return;
                    } else if (!response.existingRefferdEmail  && self.airwards.refferedEmailId) {
                        alertService.setAlert(response.messages[0], 'danger', callback);
                        return;
                    }

                    registrationService.registerUser(userParams, function (successData) {

                        if (!successData.success) {
                            popupService.alert("User Registration Error.");
                            return;
                        }

                        var loginParams = {
                            customerID: self.contact.email,
                            password: self.contact.password
                        };

                        signInService.signInUser(loginParams, function (data) {

                            // Login completed.
                            hideBusyLoader();

                            continueToExtras();

                        }, function (data) {
                            // User login failed.
                            hideBusyLoader();
                        })


                    }, function (errorData) {
                        // User registration failed.
                        hideBusyLoader();
                    })

                }, function (data) {
                    if (data.error) {
                        alertService.setAlert(data.error, 'danger');
                    }
                    hideBusyLoader();
                }); // End of LMS validation.
            }


            /**
             *  Return back to modify reservation page.
             */
            function backToModifyReservation() {

                // Get modify search criteria.
                var modifySearchParams = modifyReservationService.getReservationSearchCriteria();

                // Change state to modify reservation.
                $state.go('modifyReservation', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode(),
                    pnr: modifySearchParams.pnr,
                    lastName: modifySearchParams.lastName,
                    departureDate: modifySearchParams.departureDate
                });
            } // End of backToModifyReservation.


            /**
             *  Hide busy loader.
             */
            function hideBusyLoader() {
                // Hide busy loader.
                self.isBusyLoaderVisible = false;
            }


            /**
             *  Go to extras page.
             */
            function continueToExtras() {
                $location.path('/extras/' + languageService.getSelectedLanguage() + '/' + currencyService.getSelectedCurrency());
            }


            /**
             *  Used in isa-passenger-details directive.
             */
            function onNameChangeCompleted() {
                var getChangedPaxList = modifyReservationService.getNameChangedPassengerList(self.originalPassengers, self.passengers);
                var getFfidChangedPaxList = modifyReservationService.getFfidChangePassengerList(self.originalPassengers, self.passengers);

                if (_.isEmpty(getChangedPaxList) && _.isEmpty(getFfidChangedPaxList)) {
                    alertService.setAlert('lbl_passenger_noNameChange', 'warning', callback);
                } else if (_.isEmpty(getChangedPaxList)) {
                    loadingService.showPageLoading();

                    //No name change, only lms changed, continue after prompt
                    passengerRemoteService.lmsPassengerCheck(getFfidChangedPaxList, function (success) {
                        popupService.confirm($filter('translate')('msg_passenger_ffidChange'), changeFfid);
                        loadingService.hidePageLoading();
                    }, function (errorResponse) {
                        alertService.setAlert(errorResponse.error);
                        loadingService.hidePageLoading();
                    })

                } else if (_.isEmpty(getFfidChangedPaxList)) {
                    loadingService.showPageLoading();
                    passengerRemoteService.lmsPassengerCheck(getChangedPaxList, function (success) {
                        confirmNameChange(getChangedPaxList);
                    }, function (errorResponse) {
                        alertService.setAlert(errorResponse.error);
                        loadingService.hidePageLoading();
                    });
                } else {
                    //Call ffid change and then continue as normal
                    loadingService.showPageLoading();
                    passengerRemoteService.lmsPassengerCheck(getFfidChangedPaxList, function (success) {
                        nameAndFfidChange();
                    }, function (errorResponse) {
                        alertService.setAlert(errorResponse.error);
                    });
                }

            } // End of onNameChangeCompleted.


            function confirmNameChange(getChangedPaxList) {
                if (getChangedPaxList && getChangedPaxList.length > 0) {
                    if (self.passengers.length > getChangedPaxList.length) {
                        loadingService.hidePageLoading();
                        popupService.confirm($filter('translate')('msg_passenger_nameChangeList'), changeNames);
                    } else {
                        changeNames();
                    }
                }
            }


            function nameAndFfidChange() {
                var getChangedPaxList = modifyReservationService.getFfidChangePassengerList(self.originalPassengers, self.passengers);

                if (getChangedPaxList && getChangedPaxList.length > 0) {
                    if (self.passengers.length > getChangedPaxList.length) {
                        loadingService.hidePageLoading();
                        popupService.confirm($filter('translate')('msg_passenger_nameChangeList'), changeNamesWithFfid);
                    } else {
                        changeNamesWithFfid();
                    }
                }


            }

            function changeFfid() {
                var getFfidChangedPaxList = modifyReservationService.getFfidChangePassengerList(self.originalPassengers, self.passengers);

                modifyReservationService.changeReservationFfid({paxList: getFfidChangedPaxList, withNameChange: false}, function (data) {
                    modifyReservationService.reloadReservation();
                    alertService.setAlert("Airewards ID Change successful", 'success');
                }, function (error) {
                    alertService.setAlert(error.error);
                })
            }


            /**
             * Send name change request in modification
             * */
            function changeNames() {
                var getChangedPaxList = modifyReservationService.getNameChangedPassengerList(self.originalPassengers, self.passengers);
                loadingService.showPageLoading();
                modifyReservationService.updateReservationPaxNames(getChangedPaxList, function (data) {
                    quoteService.changeShowQuote();
                    quoteService.setShowSummaryDrawer(true);
                    loadingService.hidePageLoading();
                }, function (error) {
                    alertService.setAlert(error.error);
                    loadingService.hidePageLoading();
                });
            }


            /**
             * Send name change request with FFID change
             * */
            function changeNamesWithFfid() {

                var getChangedPaxList = modifyReservationService.getNameChangedPassengerList(self.originalPassengers, self.passengers);

                modifyReservationService.updateReservationPaxNames(getChangedPaxList, function (data) {
                    modifyReservationService.changeReservationFfid({
                        paxList: getChangedPaxList,
                        withNameChange: true
                    }, function (data) {
                        //confirmNameChange(getChangedPaxList);
                        quoteService.changeShowQuote();
                        quoteService.setShowSummaryDrawer(true);
                        loadingService.hidePageLoading();


                    }, function (error) {
                        alertService.setAlert(error.error);
                        loadingService.hidePageLoading();
                    })
                }, function (error) {
                    alertService.setAlert(error.error);
                    loadingService.hidePageLoading();
                });
            }


            function isContactFieldValidated(field) {
                

                if (self.contactParms[field].mandatory) {
                    return true;
                } else if (!_.isEmpty(self.contact[field])) {
                    return true;
                } else if (_.isUndefined(self.contact[field])) {
                    return true;
                } else {
                    return false;
                }

            }


            function doNothingErr(response) {
                // Hide busy loader.
                self.isBusyLoaderVisible = false;
            }


            function callback() {
//                console.log("call to callback")
            }

            /**
             *  Filter typeaheads according to view value.
             */
            function startsWith(state, viewValue) {
                if (viewValue === null || viewValue.trim() === "" ) {
                    return true;
                } else {
                    var value = viewValue.trim();
                    return state.substr(0, value.length).toLowerCase() == value.toLowerCase();
                }
            }

            function setTranslatedDefaultDisplayName(passenger, categoryPaxCount) {
                var translatedDisplayName = "";
                if (!_.isUndefined(passenger)) {
                    if (passenger.category == 'Adult') {
                        translatedDisplayName = $translate.instant('lbl_common_adult');
                    } else if (passenger.category == 'Child') {
                        translatedDisplayName = $translate.instant('lbl_common_child');
                    } else if (passenger.category == 'Infant') {
                        translatedDisplayName = $translate.instant('lbl_common_infant');
                    }
                }
                passenger.displayName = translatedDisplayName + " " +
                    (categoryPaxCount[passenger.category] || "");
            }

            // Clears all passengers details
            function clearAllPassengerDetails() {
                //self.passengers[0] = (passengerFactory.Passenger('Adult', 1));
                if (Object.keys(passengerService.getSearchCriteria()).length != 0) {

                    var searchCriteria = passengerService.getSearchCriteria(),
                            i;

                    self.passengers = [];

                    var paxSeq = 1;

                    for (i = 0; i < searchCriteria.adult; i++) {
                        self.passengers.push(passengerFactory.Passenger('Adult', paxSeq++));
                    }
                    for (i = 0; i < searchCriteria.child; i++) {
                        self.passengers.push(passengerFactory.Passenger('Child', paxSeq++));

                    }
                    for (i = 0; i < searchCriteria.infant; i++) {
                        self.passengers.push(passengerFactory.Passenger('Infant', paxSeq++));
                    }
                }
                var categoryPaxCount = {};

                angular.forEach(self.passengers, function (value, key) {
                    if (!self.passengers[key].displayName) {

                        if (_.isUndefined(categoryPaxCount[self.passengers[key].category])) {
                            categoryPaxCount[self.passengers[key].category] = 1;
                        } else {
                            categoryPaxCount[self.passengers[key].category] = categoryPaxCount[self.passengers[key].category] + 1;
                        }
                        setTranslatedDefaultDisplayName(self.passengers[key], categoryPaxCount);
                    }
                });
            }


            // Merge two salutation arrays based on the titleCode value
            function mergeSalutations(sal1, sal2) {
                var mergedSalutation = angular.copy(sal2);
                for (var i in sal1) {
                    var inArray = false;
                    for (var j in sal2) {
                        if (sal2[j].titleCode === sal1[i].titleCode) {
                            inArray = true;
                        }
                    }

                    if (!inArray) {
                        mergedSalutation.push(sal1[i]);
                    }
                }
                return mergedSalutation;
            }
            this.airewardStateChange = function (index) {
                self.airewardsCheckedStatus[index] = !self.airewardsCheckedStatus[index];
                if (!self.airewardsCheckedStatus[index]) {
                    self.passengers[index].airewardsEmail = "";
                }
                return;
            };

            //Check if either mobile or travel mobile phone numbers in the contact info has been filled.
            function validPhoneProvided() {
                var phoneRequired = true;

                if (self.contact.mobileNo.countryCode && self.contact.mobileNo.number) {
                    phoneRequired = false;
                } else if (self.contact.mobileNoTravel.countryCode && self.contact.mobileNoTravel.number) {
                    phoneRequired = false;
                } else if (!self.contactParms.mobileNo.visibility) {
                    phoneRequired = false;
                } else if (!self.contactParms.mobileNo.mandatory) {
                    phoneRequired = false;
                }
                return phoneRequired;
            }

            // When Airewards is checked, and the dob is for the first pax, use that dob
            function registerCheckChange() {
                if (self.isRegisterChecked && self.firstPaxAsContact && self.passengers[0].dob) {
                    self.airwards.dateOfBirth = self.passengers[0].dob;
                }
            }
            
            function sendPromoEmailChange(){
                self.contact.sendPromoEmail = self.sendPromoEmailChecked;
                
            }
        }];

    return controllers;
};