/**
 * Created by indika on 11/5/15.
 */
(function(){
    var payc = new paymentController();
    var ppay = new postPaymentController();
    var vppay = new voucherPostPaymentController();
    var payment = angular.module("payment", []);
    payment.controller(payc);
    payment.controller(ppay);
    payment.controller(vppay);
    var payService = new paymentService();
    payment.service(payService, [commonServices]);


    payment.factory('paymentFactory', ["commonLocalStorageService",function(commonLocalStorageService) {
        var paymentFactory = {};
        paymentFactory.getCardDetail = function(){
            var send = {
                "cardType": 0,
                "paymentAmount": 0,
                "expiryDate": "",
                "cardNo": "",
                "cardHoldersName": "",
                "cardCvv": ""
            };
          return send
        };

        paymentFactory.getExtCardDetail = function(){
            var send = {
                "cardType": 0,
                "paymentAmount": 0
            };
            return send
        };
        return paymentFactory;
    }]);
    
    payment.directive('basicClick', ['$parse','$rootScope',function($parse, $rootScope) {
    	  return {
    	    compile: function(elem, attr) {
    	      var fn = $parse(attr.basicClick);
    	      return function(scope, elem) {
    	        elem.on('click', function(e) {
    	          fn(scope, {$event: e});
    	          scope.$apply();
    	        });
    	      };
    	    }
    	  };
}]);

    payment.directive('sTop', ['$parse','$rootScope',function($parse, $rootScope) {
                        return {
                         restrict: 'A',
                         link: function(scope, element) {
                         scope.collapsing = false;
                         var jqElement = $( element) ;
                         scope.$watch( function() {
                             return jqElement.find( '.panel-collapse' ).hasClass( 'collapsing' );
                         }, function( status ) {
                             if ( scope.collapsing && !status ) {
                                 if ( jqElement.hasClass( 'panel-open' ) ) {
                                     var isFirstTime = true;
                                     if(isFirstTime){
                                         $( 'html,body' ).animate({
                                         scrollTop: jqElement.offset().top - 210
                                     }, 500 );
                                     isFirstTime = false;
                                 }else{
                                     $( 'html,body' ).animate({
                                         scrollTop: jqElement.offset().top - 95
                                     }, 500 );
                                 }
                                     
                                 }
                                 //isFirstTime++;
                             }
                             scope.collapsing = status;
                             
                             
                             } );
                        }
                     };
             }]);
    payment.directive('onError', function() {
        return {
            restrict:'A',
            link: function(scope, element, attr) {
            element.on('error', function() {
                //alert('sdsd');
                element.addClass('img-error');

            })
            }
        }
    });


})();

