var voucherPostPaymentController = function () {

    var controllers = {};

    controllers.voucherPostPaymentController = [
        "stepProgressService",
        "$location",
        "paymentRemoteService",
        "paymentService",
        "loadingService",
        "commonLocalStorageService",
        "$state",
        "languageService",
        "currencyService",
        "alertService",
        "applicationService", "$sce",
        function (stepProgressService,
                  $location,
                  paymentRemoteService,
                  paymentService,
                  loadingService,
                  commonLocalStorageService,
                  $state,
                  languageService,
                  currencyService,
                  alertService,
                  applicationService,
                  $sce) {

            (function init() {
                loadingService.showPageLoading();
                callToConfirmPayment();
            })();

            this.errorMessage = false;
            var self = this;

            function callToConfirmPayment() {
                var urlParams = commonLocalStorageService.getData("postMap");
                var postParam = commonLocalStorageService.getData("postUrl");

                if (postParam) {
                    postParam = postParam.substr(postParam.indexOf('?') + 1);
                }

                applicationService.setApplicationMode(commonLocalStorageService.getData('APP_MODE'));

                var params = {
                    "paymentReceiptMap": urlParams
                };

                if (urlParams.encoded) {
                    params = {
                        "paymentReceiptMap": null,
                        "postParam": postParam
                    };
                }

                params.paymode = applicationService.getApplicationMode();
                languageService.setSelectedLanguage(commonLocalStorageService.getData('langTOStore'));
                currencyService.setSelectedCurrency(commonLocalStorageService.getData('CurrencyToStore'));

                // TODO: uncomment after paymentReceipt function is completed.

                paymentRemoteService.voucherPaymentReceipt(params, function (response) {
                        commonLocalStorageService.remove("postMap");
                        commonLocalStorageService.remove('langTOStore');
                        commonLocalStorageService.remove('CurrencyToStore');

                        commonLocalStorageService.setData('voucherGroupId', response.voucherGroupId);

                        if (response.ipgTransactionResult) {
                            commonLocalStorageService.setData('ipgResults', response.ipgTransactionResult);
                        }

                        $state.go('voucherThank', {
                            lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode(),
                        });
                    },
                    function (responce) {
                        if (responce.errorType === "PAYMENT_ERROR") {
                            self.errorMessage = "Payment was not successful. Please try again later.";
                        } else if (responce.errorType === "VOUCHER_CREATION_FAILED") {
                            self.errorMessage = "Payment was successful but voucher generation failed." +
                                "Please contact administrator for assistance";
                        } else {
                            alertService.setPageAlertMessage(responce);
                        }
                    });

            } // End of callToConfirmPayment

        }]; // End of controllers.postPaymentController.

    return controllers;

} // End of PostPaymentController.
