/**
 * Created by indika on 11/5/15.
 */

var paymentController = function () {
    var controllers = {};

    controllers.paymentController = [
        '$q',
        "stepProgressService",
        "$location",
        "$timeout",
        "quoteService",
        "$anchorScroll",
        "paymentRemoteService",
        "paymentService",
        "paymentFactory",
        "$state",
        "alertService",
        "commonLocalStorageService",
        "IBEDataConsole",
        "loadingService",
        "MasterData",
        "currencyService",
        "languageService",
        "applicationService",
        "$window",
        "modifyReservationService",
        "ancillaryService",
        "customerRemoteService",
        "constantFactory",
        "navigationService",
        "eventListeners",
        "signInService",
        "popupService",
        "$sce",
        "travelFareService",
        "$translate",
        "ancillaryRemoteService",
        "passengerService",
        "$rootScope",
        "$scope",
        'formValidatorService',
        'signoutServiceCheck',
        paymentsController
    ];


    function paymentsController($q, stepProgressService, $location, $timeout, quoteService, $anchorScroll, paymentRemoteService, paymentService, paymentFactory, $state,
            alertService, commonLocalStorageService, IBEDataConsole, loadingService, MasterData, currencyService, languageService, applicationService,
            $window, modifyReservationService, ancillaryService, customerRemoteService, constantFactory, navigationService, eventListeners, signInService, 
            popupService, $sce, travelFareService, $translate, ancillaryRemoteService, passengerService, $rootScope, $scope, formValidatorService,signoutServiceCheck) {

        /**
         *    ------- Flow in the Airewards --------
         *    If logged in set to true (send a request, get the airewards information)
         *    If not logged in set to false
         *
         *    If status changed, make sure that the (check if the user is logged in).
         *    If not show log in window
         *
         */

        // Global vars.
        $rootScope.extrasNavTag = 'lbl_header_nav_payment';
        $rootScope.removePaddingFromXSContainerClass=false;
        var self = this,
                binPromoData = null,
                selectedPaymentOptionbeforeBinPromo = null,
                airRewards = null,
                totalRedeemedAmount = 0,
                totalToPayWithoutPromo = 0,
                paymentOptionParams = null,
                passengers = {};



        // Properties.
        self.masterCardValidationPattern = /^5[1-5][0-9]{14}$/;
        self.visaCardValidationPattern = /^4[0-9]{12}(?:[0-9]{3})?$/;
        self.selectExistingCreditCardDropdownList = false;
        self.showLogin = false;
        self.airewardsChecked = false;
        self.isRedeemed = false; // Holds redeemed status to show success message.
        self.availableAirewardsPoints;
        self.isPaymentRedeemed = false; // Used to reset airewards values.
        self.redeemedPoints; // Holds temp redeemed points amount.
        self.showFlightDetails = true;
        self.selectedPaymentOption = null;
        self.selectedPaymentOptionFlag = null;
        self.paymentRetry = false;
        self.paymentRetryMsg = "";
        self.expDate = {
            month: [],
            year: []
        };
        self.loginText = "Log in";
        self.showBinPromoText=false;
        self.totalAmount = 0;
        self.payableAmount = 0;
        self.lmsRedeemed = false;
        self.airewards = {};
        self.cardInfo = {};
        self.redeem = 0.0;
        self.fareAditioalDisplay = {};
        self.onHoldStr = "";
        self.paymentDisplayAmounts = [];
        self.transactionFee = 0;
        self.additionalDetails = {};
        self.currentAppMode = applicationService.getApplicationMode();
        self.careers = applicationService.AIRLINE_CAREER_CODES;
        self.captchaSrc = "jcaptcha.jpg?p=" + new Date().getTime();
        self.onholdCaptchaEnable = constantFactory.AppConfigValue("onHldImageCaptcha");
        self.isBinPromotionEnabled = false;
        self.isPopupBinPromoModalOpen = false;
        self.binPromoCode = null;
        self.isInvalidCaptcha = false; // Hold captcha error message.
        self.binPromoModalStatus = {
            "promotionApplied": false,
            "alreadyApplied": false,
            "hasExistingPromotion": false,
            "noPromotion": null,
            "existingPromotionAmount": null,
            "discountAmount": 0.0,
            "lmsApplicableAmount": 0.0,
            "transactionFee": {},
            "effectivePaymentAmount": {},
            "promotionLoading": false
        };
        self.quoteDetails;
        self.hasPromoOption = false;
        self.isSignInProcessing = false; // holds sign in loading proccess status.
        self.binPromotion = false;
        self.isRewardsLoading = false; // Holds airewards section loading status.
        self.baseCurrency=currencyService.getAppBaseCurrency();
        self.getSelectedCurrencyType = currencyService.getSelectedCurrency();
        self.currentPaymentCurrency = self.getSelectedCurrencyType ? self.getSelectedCurrencyType : self.baseCurrency;
        self.sectorsData = null;
        self.previousSelectionCash = false;
        self.paymentSection = {paymentOptionsStyle: "fullwidth", totalAmount: "col-md-12"}
        self.adultNames = self.childNames = self.infantNames = "";
        self.onlyCashPaymentOption = false;
        self.isThreeTwentyDevice = $window.matchMedia("only screen and (max-width: 320px)").matches

        //To display special messages in new IBE
        self.isSpecialMsgsAvailable = false;
        self.specialMessages = [];
        self.isLoggedIn = signInService.getLoggedInStatus();
        loadSpecialMsgs();

        self.pgwMandatoryParamsValidation = {
            mandatoryParams : {
                'emailAddress' :{
                    required: {
                        name: "ng-required",
                        value: "true",
                        message: "lbl_validation_emailRequired"
                    },
                    'pattern': {
                        'message': 'lbl_validation_emailFormat'
                    }
                }
            }
        };

        self.creditCardValidation = formValidatorService.getValidations();
        
        self.payOnlyFromLMS = false;
        self.noPaymentRequiredWithCC = false;

        // Functions.
        self.closeDropdown = closeDropdown;
        self.setShowFlightDetails = setShowFlightDetails;
        self.showBinPromoTextOnClick = showBinPromoTextOnClick;
        self.refreshCaptcha = refreshCaptcha;
        self.setPayOption = setPayOption;
        self.totalPayable = totalPayable;
        self.hideLogin = hideLogin;
        self.signOut = signOut;
        self.checkLogin = checkLogin;
        self.setExpireDate = setExpireDate;
        self.airewardsChanged = airewardsChanged;
        self.changeAirewardsUser = changeAirewardsUser;
        self.scrollToPayment = scrollToPayment;
        self.redemptionAmount = redemptionAmount;
        self.gotoFarePage = gotoFarePage;
        self.payLater = payLater;
        self.checkBinPromo = checkBinPromo;

        self.binPromoPopupClose = binPromoPopupClose;
        self.resetBinPromotion = resetBinPromotion;
        self.changeBinPromo = changeBinPromo;
        self.applyCreditCardPromo = applyCreditCardPromo;
        self.applyBinPromo = applyBinPromo;
        self.submitPaymentData = submitPaymentData;
        self.backToHome = backToHome;
        self.navigateToPaymentOptions = navigateToPaymentOptions;
        self.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');
        self.isVoucherEnabled = constantFactory.AppConfigStringValue("voucherEnabled");
        self.isValidCareerFor = isValidCareerFor;
        self.passName = passengerService.getPassengerList('displayName');
        self.goBack = goBack;
        self.showLMSPonitsSlider = showLMSPonitsSlider;

		self.voucherChecked = false;
        self.addVoucherId = null
		self.otpSuccessMessage = false;
		self.otpErrorMessage = null;
		self.unsuccessfulVoucherAttempts = 0;
		self.reedemedGiftVoucherAmount = 0;
		self.totalReedemedGiftVoucherAmount = 0;
		self.addVoucherId = null;
		self.voucherOtpValue = null
		self.voucherValue = null
		self.voucherOtpModel = null;
		self.voucherCheckedSubmit = voucherCheckedSubmit;
		self.confirmVoucherOtp = confirmVoucherOtp;
		self.resendVoucherOtp = resendVoucherOtp;
		self.useAnotherVoucherCode = useAnotherVoucherCode;
		self.reedemedGiftoucher = reedemedGiftoucher;
		self.useAnotherVoucher = true;
        self.payOnlyFromVoucher = false;
        self.voucherRedeemed = false;
        self.voucherOtpSent = false;
        self.isVoucherRedeemInProgress = false;
        self.voucherSuccessMessage = false;

        var contactEmail = commonLocalStorageService.getData("contact_email");
        if (!_.isEmpty(contactEmail)) {
            self.additionalDetails.param1 = contactEmail;
	    }

        try {
            self.paymentForAnci = ancillaryService.getPaymentDisplayAncillary();
        } catch (ex) {
            console.log(ex);
        }

        /**
         *  Init function.
         */
        init();
        function init() {
        	signoutServiceCheck.checkSignoutOperation();
            var passObj = passengerService.getPassengerList();
            commonLocalStorageService.setData("isAncilaryAvailable", false);
            self.paxDetail = commonLocalStorageService.getData("PAX_DETAIL");
            if (!!passObj) {
                _.each(passObj, function (passenger) {
                    console.log(passObj);
                    console.log(passenger.displayName);

                    if (passenger.category == "Adult") {
                        if (!_.isEmpty(self.adultNames)) {
                            self.adultNames = self.adultNames + ', '
                        }
                        self.adultNames = self.adultNames + passenger.firstName + ' ' + passenger.lastName;
                    }
                    if (passenger.category == "Child") {
                        if (!_.isEmpty(self.childNames)) {
                            self.childNames = self.childNames + ', '
                        }
                        self.childNames = self.childNames + passenger.firstName + ' ' + passenger.lastName;
                    }
                    if (passenger.category == "Infant") {
                        if (!_.isEmpty(self.infantNames)) {
                            self.infantNames = self.infantNames + ', '
                        }
                        self.infantNames = self.infantNames + passenger.firstName + ' ' + passenger.lastName;
                    }
                })
            }

            stepProgressService.setCurrentStep('payment');

            self.isRewardsLoading = true; // Show busy loader.

            self.isAnciSummaryAtPaymentEnabled = (constantFactory.AppConfigStringValue('enableAnciSummaryAtPayment') === 'true');

            self.quoteDetails = {};
            self.isLoyaltyManagementEnabled = currencyService.getLoyaltyManagementStatus();
            
            //Hide summary drawer on load
            quoteService.setShowSummaryDrawer(false);
            commonLocalStorageService.setData('appMode', self.currentAppMode);
            fillExpDateArrays();
            //Load Payment options
            paymentOptionParams = paymentService.createPaymentParamsForOptions();
            paymentOptionParams.binPromotion = self.binPromotion;
            paymentOptionParams.originCountryCode = commonLocalStorageService.getData('CARRIER_CODE');

            //Add fare details
            //Payment summary call should always gone before payment option call.Please make sure to keep that order.
            if (self.currentAppMode === applicationService.APP_MODES.CREATE_RESERVATION) {
                self.fareDisplay = commonLocalStorageService.getData('QUOTE');
                updateInitialPaymentSummary();
            }
            else if(self.currentAppMode === applicationService.APP_MODES.BALANCE_PAYMENT){
                var bal_summary = modifyReservationService.getBalanceSummery();
                retrieveAndSetAdministrationFee();
                if (!!bal_summary && !!bal_summary.balanceSummery) {
                    self.fareDisplay = bal_summary.balanceSummery;
                    self.quoteDetails.totalAllInclusive = bal_summary.balanceSummery.totalCredits;

                }

                self.sectorsData = modifyReservationService.getFareGroupWiseSegmentFromReservation();
                loadPaymentOptions();
            }
            else {
                var bal_summary = modifyReservationService.getBalanceSummery();
                var modifyPriceBreakdown = modifyReservationService.modifyPriceBreakdown();
                self.groupWiseSegment = replaceModifiedFlightSegment(modifyPriceBreakdown.groupWiseSegment);
                self.paxSummary = bal_summary.balanceSummery.paxSummary;
                self.modifyCharges = bal_summary.balanceSummery.modifyCharges;
                retrieveAndSetAdministrationFee();
                if (!!bal_summary && !!bal_summary.balanceSummery) {
                    self.fareDisplay = bal_summary.balanceSummery.updatedChargeForDisplay;
                    // API is not supporting for updatedChargeForDisplay
                    if (bal_summary.balanceSummery.updatedChargeForDisplay) {
                        self.quoteDetails.totalAllInclusive = bal_summary.balanceSummery.updatedChargeForDisplay.dueAmountToDisplay;
                    }
                }
                loadPaymentOptions();
            }

            if (self.currentAppMode !== applicationService.APP_MODES.CREATE_RESERVATION) {
                self.isBackVisible = true;
                self.paymentStyle = {minHeight: 0};
            }

            self.fareAditioalDisplay = ancillaryService.getAncillaryTotalTypewise();

            //Add Service details

            // If logged in enable airewards section
            //Check user logged in status
            self.showLogin = false;

            //Load Payment options
            var params = paymentService.createPaymentParamsForOptions();
            params.binPromotion = self.binPromotion;
            params.originCountryCode = commonLocalStorageService.getData('CARRIER_CODE');
            var userDetails = JSON.parse(sessionStorage.getItem('IBE.USER_SIGN_IN'));
            // Select Payment Type drop down list
            self.savedCreditCardList = {};
            if(userDetails.loggedInCustomerDetails!==undefined){
	            self.selectPaymentTypeDropDownFlag = false;
	            paymentRemoteService.savedCardPaymentTypeList(function(response) {
                    if (response.customerAliasList !== null) {
        				var customerAliasListData = response.customerAliasList;
                        self.selectPaymentTypeDropDownFlag = true;
                        $translate("lbl_payment_selectPatmentTypeTitle").then(function(selectPatmentTypeDropdownDefaultValue) {
        					var defaultValue =  {  
        						"version": 0,
                                "customerAliasId": '',
                                "customerId": '',
                                "alias": "",
                                "noLastDigits": "",
                                "holderName": selectPatmentTypeDropdownDefaultValue,
                                "expiryDate": "",
                                "status": "",
                                "createDate": "",
                                "ccName": selectPatmentTypeDropdownDefaultValue
                            };                         
        					customerAliasListData.push(defaultValue);
                            self.savedCreditCardList = customerAliasListData;
                        });

                    }
                }, function(error) {
                    alertService.setPageAlertMessage(error);
                    self.isRewardsLoading = false;
                });
            }
            		self.onPaymentTypeItemSelect = function(paymentMethodType) {

            			var dateObj = paymentMethodType.expiryDate.split("-");

            			if (paymentMethodType.alias !== '') {
            				self.masterCardValidationPattern = "";
            		        self.visaCardValidationPattern = "";
            				self.selectExistingCreditCardDropdownList = true;
            				self.cardInfo.cardNo = '************' + paymentMethodType.noLastDigits;
            				self.expDateMonth = dateObj[1];
            				self.expDateMonth = parseInt(self.expDateMonth.replace(/^0+/, ''));
            				self.expDateYear = parseInt(dateObj[0]);
            				self.cardInfo.cardHoldersName = paymentMethodType.holderName;
            				self.cardInfo.alias = paymentMethodType.alias;
            				self.cardInfo.cardCvv = '';
            			} else {
            				self.masterCardValidationPattern = /^5[1-5][0-9]{14}$/;
            		        self.visaCardValidationPattern = /^4[0-9]{12}(?:[0-9]{3})?$/;
            				self.selectExistingCreditCardDropdownList = false;
            				self.cardInfo.cardNo = '';
            				self.expDateMonth = '';
            				self.expDateYear = '';
            				self.cardInfo.cardHoldersName = '';
            				self.cardInfo.alias = '';            				
            			}
            			self.selectPaymentType = paymentMethodType.alias;
            			self.selectedPaymentType = paymentMethodType.ccName;
                        self.closeDropdown();
            		}

            // end of Select Payment Type

        }
        
        function closeDropdown() {
			if (angular.element('.xs-btn-group-class').hasClass('open')) {
				angular.element('.xs-btn-group-class').removeClass('open');
			}
		}

        function isValidCareerFor(airlineName) {
            return applicationService.isValidCareerFor(airlineName);
        }

        // Get charge breakdown for the modification flow
        function getChargeDetails() {
            loadingService.showPageLoading();

            paymentRemoteService.getChargeDetails(function (response) {
                self.quoteDetails.reservationChargeDetails = response.reservationChargeDetails;
                loadingService.hidePageLoading();
            }, function (error) {
                loadingService.hidePageLoading();
            })
        }
        
        // Replace modified segment in the group wise segments
        function replaceModifiedFlightSegment(groupWiseSegments){
            var newFlight = modifyReservationService.getSelectedNewFlight();
            var selectedGroupSegments = modifyReservationService.getSelectedFareGroupSegmentsToModify();
            if(newFlight !=null && selectedGroupSegments!=null){
            	var flightSegments = getNewFlightSegment(newFlight);

            	_.forEach(groupWiseSegments, function(segmentGroup){
            		if(selectedGroupSegments.fareGroupID == segmentGroup.fareGroupID){
            			segmentGroup.segments = flightSegments;
            		}
            	});
            }

            return groupWiseSegments;
        }
        
        // Get flight details for modified segment
        function getNewFlightSegment(newFlight){
            var flightSegments = [];
            var hasStops = false;

            if(!_.isUndefined(newFlight.stops)){
                hasStops = !!newFlight.stops.length;
            }

            //Set stopover flights
            if (hasStops) {
                newFlight.destinationName = newFlight.stops[0].originName;

                flightSegments.push({flight: newFlight});

                angular.forEach(newFlight.stops, function (flight) {
                    flightSegments.push({flight: flight});
                });
            }else{
                flightSegments.push({flight: newFlight});
            }

            return flightSegments;
        }

        function retrieveAndSetAdministrationFee() {
            var adminFeeRQparams = paymentService.createAdminFeeRQParams();
            paymentRemoteService.retrieveAdministrationFee(adminFeeRQparams, function (response) {
                //self.fareDisplay.chargeListToDisplay.push({'displayDescription':'Transaction fee','displayOldCharge':response.administrationFee,'displayNewCharge':response.administrationFee,"enableDisplay":response.displayAdministrationFee})
                self.transactionFee = response.administrationFee;
                self.modifyCharges.totalAllInclusive = self.modifyCharges.total + self.transactionFee;
                self.quoteDetails.totalAllInclusive = self.modifyCharges.totalAllInclusive;
                
                paymentService.setTotalEffectivePaymentwithAdminFee({'adminFee': response.administrationFee, 'totalAllInclusive': response.totalAllInclusive})
                //  retrivePaymentOPtions(paymentOptionParams);
            }, function (error) {
                alertService.setPageAlertMessage(error);
                self.isRewardsLoading = false;
            })

        }

        function GetTotalAfterPromoCodeDiscount() {
            var promoCodeDiscount = 0, promocodeDicountType;
            angular.forEach(self.quoteDetails.promoInfo, function (promotion, index) {

                if (promotion.type != "RETURN_DISCOUNT" && promotion.discountAs === "Money") {
                    promoCodeDiscount = promoCodeDiscount + promotion.discountAmount;
                    promocodeDicountType = promotion.discountAs;
                }
            });

            if (self.quoteDetails.anciPromotion) {
                promoCodeDiscount = promoCodeDiscount + self.quoteDetails.anciPromotion;
            }

            if (promocodeDicountType == "Credit") {
                return self.payableAmount;
            } else {
                return self.payableAmount - promoCodeDiscount;
            }
        }

        function GetTotalAfterBinPromoDiscount() {

        }

        /**
         *  If user sign out from top login bar. set the behavior accordingly.
         */
        eventListeners.registerListeners(eventListeners.events.CUSTOMER_LOGIN_STATUS_CHANGE, function () {
            $timeout(function () {
                if (!applicationService.getCustomerLoggedState()) {
                    self.airewardsChecked = false;
                }
            }, 0)
        });


        function setShowFlightDetails(value) {
            self.showFlightDetails = value;
        }


        function showBinPromoTextOnClick() {
            self.showBinPromoText = true;
        }


        function refreshCaptcha() {
            self.captchaSrc = "jcaptcha.jpg?p=" + new Date().getTime();
        }


        function setPayOption() {    	
        
            $translate("lbl_payment_selectPatmentTypeTitle").then(function(selectPatmentTypeDropdownDefaultValue) {
            	self.selectExistingCreditCardDropdownList = false;
            	self.cardInfo.cardNo = '';
            	self.expDateMonth = '';
            	self.expDateYear = '';
            	self.cardInfo.cardHoldersName = '';
            	self.cardInfo.alias = '';
            	self.cardInfo.cardCvv = '';
            	self.selectPaymentType = selectPatmentTypeDropdownDefaultValue;
    			self.selectedPaymentType = selectPatmentTypeDropdownDefaultValue;				
            });
        	
            if (self.binPromotion) {
                var isConfirmed = confirm("You applied promotion will reset with this action, Do you want continue?");
                if (isConfirmed) {
                    self.binPromotion = false;
                    self.airewards = airRewards;
                    self.payableAmount = totalToPayWithoutPromo;
                    selectedPaymentOptionbeforeBinPromo = null;

                    updatePaymentSummary();
                } else {
                    self.selectedPaymentOptionFlag = selectedPaymentOptionbeforeBinPromo.cardType + "_" +
                            selectedPaymentOptionbeforeBinPromo.paymentGateway.gatewayId;
                    return false;
                }
            }

            if (!self.binPromotion) {
                selSelectedPayOption();
            }
            self.paymentRetryMsg = $sce.trustAsHtml(paymentService.getPaymetErrorMsg());
            self.paymentRetry = (!!self.paymentRetryMsg) ? true : false;
        }


        function totalPayable() {
            commonLocalStorageService.setData('TOTAL_ALL_INCLUSIVE', self.quoteDetails.totalAllInclusive);
            if (self.airewardsChecked && self.airewards) {
                if (self.airewards.redeemableAmount.toFixed(0) == self.redeem) {
                    return self.quoteDetails.totalAllInclusive - self.airewards.redeemableAmount;
                } else {
                    return self.quoteDetails.totalAllInclusive - self.redeem;
                }
            } else {
                return self.payableAmount;
            }
        }
        ;


        /**
         *  Hide login overlay.
         */
        function hideLogin() {

            // If login is still processing keep the overlay.
            if (self.isSignInProcessing) {
                return;
            }

            self.showLogin = false;
            self.airewardsChecked = false;
            self.loginText = "Log in";
        }


        function signOut() {

            var params = {};

            popupService.confirm("If you continue, data will be lost.Do you want to continue?&lrm;", function () {

                signInService.signoutUser(params, function (results) {
                    self.airewardsChecked = false;

                }, function (results) {

                }); // End of signInService.

            }); // End of popupService.

        }
        ; // End of signOut.


        function checkLogin(formData) {

            if (formData.$valid) {

                self.loginText = "Log in...";
                self.isSignInProcessing = true;

                paymentService.signInUser(self.user, function (data) {

                    if (!data.success) {
                        alertService.setAlert(data.messages[0], "warning");
                        self.showLogin = false;
                        self.airewardsChecked = false;
                        self.loginText = "Log in";
                        self.isSignInProcessing = false;
                    } else {
                        self.showLogin = false;
                        self.airewardsChecked = true;
                        self.loginText = "Log in";
                        loadAirRewardsOnUserLogging();
                        self.isSignInProcessing = false;
                    }
                    $window.localStorage.removeItem('PNR_LOGIN');
                    
                }, function (data) {
                    alertService.setPageAlertMessage(data);
                });
            }

        }


        function getSelectedPayoption() {
            var deferred = $q.defer();

            var toSend = null;
            for (var i = 0; i < self.paymentOption.length; i++) {
                var option = self.paymentOption[i];
                var optionPg = (option.paymentGateway) ? option.paymentGateway.gatewayId : "";
                if (self.selectedPaymentOptionFlag !== null) {

                    if (option.cardType+"_"+optionPg+"_"+option.cssClass  === self.selectedPaymentOptionFlag) {
                        if (self.binPromotion) {
                            selectedPaymentOptionbeforeBinPromo = angular.copy(option);
                            option.paymentGateway.effectivePaymentAmount = binPromoData.effectivePaymentAmount;
                            option.paymentGateway.transactionFee = binPromoData.transactionFee;
                            //setCard info to the gateway
                            option.cardNo = self.cardInfo.cardNo;
                            option.cardHoldersName = self.cardInfo.cardHoldersName;
                            option.cardCvv = self.cardInfo.cardHoldersName;
                            option.binPromotionApplied = self.binPromoModalStatus.promotionApplied;
                            toSend = option;
                        } else {
                            toSend = selectedPaymentOptionbeforeBinPromo || option;
                        }
                        break;
                    }
                }
            }
            
            if(!toSend){
                deferred.reject(toSend);
            }else{
                deferred.resolve(toSend)
            }
            return deferred.promise;
        };


        function selSelectedPayOption() {
            //set on hold String
            getSelectedPayoption().then(function(resp){
                console.log(resp)
                self.selectedPaymentOption =resp;

                if (self.selectedPaymentOption.paymentGateway) {
                    self.currentPaymentCurrency = self.selectedPaymentOption.paymentGateway.paymentCurrency;
                }
                self.currentPaymentCurrency = self.currentPaymentCurrency || self.baseCurrency;
                self.paymentDisplayAmounts = [];

                var effectivePayment = {};

                var tpg = self.selectedPaymentOption,
                        displayFlg = true;

                if (tpg !== null) {

                    //check if paymentCurrency equals to selected or baseCurrency
                    if (tpg.paymentGateway !== undefined) {
                        if (tpg.paymentGateway.paymentCurrency !== currencyService.getAppBaseCurrency()) {
                            displayFlg = false;
                        }
                    }

                    if (tpg.cssClass.toLowerCase() === "cash") {
                        effectivePayment.transactionFee = 0;
                        effectivePayment.currencyType = currencyService.getAppBaseCurrency();
                        //effectivePayment.effectiveAmount = self.payableAmount;
                        effectivePayment.effectiveAmount = self.cashTotal;
                        paymentService.setTotalAmountToPay(self.payableAmount);
                        self.paymentDisplayAmounts.push(effectivePayment);

                        // Update the payment summary and
                        updatePaymentSummary({cashPayment: true});
                    } else {
                        var effAmountObj = tpg.paymentGateway.effectivePaymentAmount;
                        var trasFree = tpg.paymentGateway.transactionFee;
                        paymentService.setTotalAmountToPay(effAmountObj.baseCurrencyValue);

                        //set Base Currency Values
                        effectivePayment.transactionFee = (!!trasFree) ? trasFree.baseCurrencyValue : 0;
                        effectivePayment.effectiveAmount = effAmountObj.baseCurrencyValue;
                        effectivePayment.currencyType = currencyService.getAppBaseCurrency();
                        commonLocalStorageService.setData('GoogleAnalytics_TotalWithAdminFee', effAmountObj.baseCurrencyValue);

                        if (tpg.paymentGateway.brokerType === "external") {

                            self.cardInfo = paymentFactory.getExtCardDetail();
                            if (tpg.cssClass.toLowerCase() === "payfort_paystore") {
                                self.cardInfo.additionalDetails = self.additionalDetails;
                            }
                            if (tpg.cssClass.toLowerCase() === "qiwi" || tpg.cssClass.toLowerCase() === "qiwi_offline") {
                                self.cardInfo.additionalDetails = self.additionalDetails;
                            }
                            if (_.contains(["PAYFORT_PAYSTORE", "PAY_FORT_ONLINE_INSTALLMENTS",
                                        "PAY_FORT_ONLINE_SADAD", "PAY_FORT_ONLINE_KNET"],
                                    tpg.cssClass)) {
                                self.cardInfo.additionalDetails = self.additionalDetails;
                                if (tpg.cssClass == "PAY_FORT_ONLINE_INSTALLMENTS") {
                                    $translate("msg_payFort_pgw_installments_" + tpg.paymentGateway.paymentCurrency.toUpperCase())
                                        .then(function (name) {
                                            self.selectedPaymentOption.translatedMessage = name;
                                        });
                                }
                            }
                        } else {
                            self.cardInfo = paymentFactory.getCardDetail();
                            if (self.binPromotion) {
                                self.cardInfo.cardNo = tpg.cardNo;
                            }

                            self.isBinPromotionEnabled = true;
                        }
                        self.paymentDisplayAmounts.push(effectivePayment);

                        if (!displayFlg) {
                            //set Payment Currency Values
                            var effectivePaymentObj = {};
                            effectivePaymentObj.transactionFee = (!!trasFree) ? trasFree.paymentCurrencyValue : 0;
                            effectivePaymentObj.effectiveAmount = effAmountObj.paymentCurrencyValue;
                            effectivePaymentObj.isPaymentCurrency = true;
                            effectivePaymentObj.currencyType = tpg.paymentGateway.paymentCurrency;
                            self.paymentDisplayAmounts.push(effectivePaymentObj);
                        }

                        // Update the payment summary if the previous payment method was cash
                        if (self.previousSelectionCash) {
                            updatePaymentSummary({cashPayment: false});
                        }
                    }
                    /*var effectivePayment = paymentService.getTotalEffectivePaymentwithAdminFee()
                    if(!!effectivePayment){
                        /*
                        ** set total value with admin free
                        */
                        /*_.each(self.paymentDisplayAmounts,function(item){
                            item.effectiveAmount = effectivePayment.totalAllInclusive;
                        })
                    }*/

                } // if (tpg !== null)
         
                if (self.lmsRedeemed && (!self.paymentDisplayAmounts.length || self.paymentDisplayAmounts[0].effectiveAmount == 0)) {
                    self.payOnlyFromLMS = true;
                }
            })

        }


        function loadAirRewardsOnUserLogging() {
            // Show busy loader.
            self.isRewardsLoading = true;

            var params = paymentService.createPaymentParamsForOptions();
            params.binPromotion = self.binPromotion;
	    params.originCountryCode = commonLocalStorageService.getData('CARRIER_CODE');
            paymentRemoteService
                    .retrievePaymentOptions(params, function (response) {
                        // Hide busy loader.
                        self.isRewardsLoading = false;

                    paymentService.setTransactionId(response.transactionId);
                    if(response.binPromotionDetails && response.binPromotionDetails.lmsCredits!==null){
                        var lmsOption = {};
                        lmsOption.redeemedAmount = response.binPromotionDetails.lmsCredits.redeemedAmount;
                        lmsOption.lmsRedeemed = response.binPromotionDetails.lmsCredits.redeemed;
                        lmsOption.availablePoints = response.binPromotionDetails.lmsCredits.availablePoints;
                        lmsOption.equivalentAmount = response.binPromotionDetails.lmsCredits.availablePointsAmount;
                        lmsOption.redeemableAmount = response.binPromotionDetails.lmsCredits.maxRedeemableAmount;
                        self.airewards = lmsOption;
                        self.payableAmount = response.binPromotionDetails.effectivePaymentAmount.baseCurrencyValue;
                    }else{
                        self.airewards = response.lmsOptions;
                        self.payableAmount = response.totalAmount;
                    }

                    var balanceToPay = self.quoteDetails.totalAllInclusive
										- self.totalReedemedGiftVoucherAmount;

                    if (self.airewards !== undefined
							&& self.airewards.redeemableAmount > balanceToPay) {
						self.airewards.redeemableAmount = balanceToPay;
					}

                    self.paymentOption = filterPaymentOptions(response.paymentOptions);
                    self.selectedPaymentOptionFlag = (self.paymentOption.length) ? self.paymentOption[0].cardType + "_" +self.paymentOption[0].paymentGateway.gatewayId + "_" +self.paymentOption[0].cssClass : null;
                    self.totalAmount = response.totalAmount;
                    //keep backup lms obj
                    airRewards = response.lmsOptions;
                    totalToPayWithoutPromo = response.totalAmount
                    if (applicationService.getCustomerLoggedState() && response.lmsOptions !== null) {
                        self.airewardsChecked = true;
                    }

                    }, function (error) {
                        // Hide busy loader.
                        alertService.setPageAlertMessage(error);
                        self.isRewardsLoading = false;
                    });
        }


        function setExpireDate() {
            var expDate = moment([self.expDateYear, parseInt(self.expDateMonth) - 1, 1]);
            self.cardInfo.expiryDate = expDate.format("YYYY-MM-DDTHH:mm:ss");
        }


        function airewardsChanged(value) {
         
            if (value) {
                self.redeem = 0; // Reset redeemed amount to zero when switching airewards redeem option

                if (signInService.getLoggedInStatus()) {
                    self.airewardsChecked = true;
                    self.showLogin = false;
                } else {
                    self.showLogin = true;
                    self.airewardsChecked = false;
                }
            } else {
                self.airewardsChecked = false;

                if (signInService.getLoggedInStatus()) {

                    var param = paymentService.createPaymentParamsForOptions();

                    param.paymentOptions = {
                        "lmsCredits": {
                            "redeemRequestAmount": 0
                        }
                    };

                    param.binPromotion = self.binPromotion;

                    paymentRemoteService.redemptionPaymentAmount(param,
                            function (response) {
                                eventListeners.notifyListeners(eventListeners.events.SET_LMS_DETAILS, response.lmsCredits.availablePoints);
                            }
                    );

                }

                // Handles use changed selection on using airewards.
                if (self.isPaymentRedeemed) {

                    self.isPaymentRedeemed = false;
                    self.isRedeemed = false; // Hide redeemed success message.
                    totalRedeemedAmount = 0;
                    self.redeemedPoints = 0;
                    self.selectedPaymentOptionFlag = "";
                    self.selectedPaymentOption = null;
                    init();
                    displayHideCCOptionDetails();
                }
            }

            self.user = {};
        }
        ;


        function changeAirewardsUser() {
            self.showLogin = true;
            clearForm();
        }


        function scrollToPayment() {
            var element = $(".waypoint_PaymentOption");
            $("html, body").animate({
                scrollTop: element.position().top
            }, 600);
        }


        /**
         *  Confirm Redemption click.
         */
        function redemptionAmount() {

            if (self.redeem > 0) {
                var param = paymentService.createPaymentParamsForOptions();

                // Calculate redeem value is redeemable amount contains decimal values.
                if (self.airewards.redeemableAmount.toFixed(0) == self.redeem) {
                    self.redeem = self.redeem + (self.airewards.redeemableAmount - self.redeem);
                }

                // Was used to set the redeemed amount in 'Redeem again'
                totalRedeemedAmount += self.redeem;

                param.paymentOptions = {
                    "lmsCredits": {
                        "redeemRequestAmount": self.redeem
                    }
                };

                param.binPromotion = self.binPromotion;
                self.isRewardsLoading = true;

                paymentRemoteService.redemptionPaymentAmount(param, function (response) {

                    if (!response.success) {
                        alertService.setAlert("Something went wrong. Please try again.");
                        return;
                    }

                    if (response.lmsOptions && response.lmsOptions.lmsRedeemed) {
                        self.isRedeemed = true;
                        self.isPaymentRedeemed = true;
                        self.redeemedPoints = response.lmsOptions.redeemedAmount;
                        self.availableAirewardsPoints = response.lmsCredits.availablePoints;

                        // Update lms details in login bar according to redeemed points.
                        eventListeners.notifyListeners(eventListeners.events.SET_LMS_DETAILS, self.availableAirewardsPoints);

                        // self.airewards.redeemableAmount & self.redeem is not changed due to 'redeem again' functionality change
                        // self.airewards.redeemableAmount = self.airewards.redeemableAmount - self.redeemedPoints;
                        // self.redeem = 0;


                        self.payableAmount = self.payableAmount - self.redeemedPoints;


                    }

                    var lmsOption = {};

                    if (self.binPromotion) {
                        lmsOption.redeemedAmount = response.binPromotionDetails.lmsCredits.redeemedAmount;
                        lmsOption.lmsRedeemed = response.binPromotionDetails.lmsCredits.redeemed;
                        lmsOption.availablePoints = response.binPromotionDetails.lmsCredits.availablePoints;
                        lmsOption.equivalentAmount = response.binPromotionDetails.lmsCredits.availablePointsAmount;
                        lmsOption.redeemableAmount = response.binPromotionDetails.lmsCredits.maxRedeemableAmount;
                        //self.payableAmount = response.binPromotionDetails.effectivePaymentAmount.baseCurrencyValue;

                        if (lmsOption.lmsRedeemed) {
                            self.redeemedPoints = response.binPromotionDetails.lmsCredits.redeemedAmount;
                            self.availableAirewardsPoints = response.binPromotionDetails.lmsCredits.availablePoints;
                            eventListeners.notifyListeners(eventListeners.events.SET_LMS_DETAILS, self.availableAirewardsPoints);
                        }

                        self.binPromoModalStatus = {
                            "promotionApplied": response.binPromotionDetails.promotionApplied,
                            "alreadyApplied": response.binPromotionDetails.alreadyApplied,
                            "hasExistingPromotion": response.binPromotionDetails.hasExistingPromotion,
                            "noPromotion": response.binPromotionDetails.noPromotion,
                            "existingPromotionAmount": response.binPromotionDetails.existingPromotionAmount,
                            "discountAmount": response.binPromotionDetails.discountAmount,
                            "lmsApplicableAmount": response.binPromotionDetails.lmsApplicableAmount,
                            "transactionFee": response.binPromotionDetails.transactionFee,
                            "effectivePaymentAmount": response.binPromotionDetails.effectivePaymentAmount,
                            "promotionLoading": false
                        };
                        binPromoData = self.binPromoModalStatus;
                    } else {
                        lmsOption = response.lmsOptions;
                        //self.payableAmount = response.totalAmount;
                    }

                        if (lmsOption.lmsRedeemed) {
                            paymentService.setTransactionId(response.transactionId);
                            paymentService.setTotalAmountToPay(response.totalAmount);
                            //self.selectedPaymentOption = null;
                            self.paymentOption = filterPaymentOptions(response.paymentOptions);
                            self.totalAmount = response.totalAmount;
                            self.isRewardsLoading = false;
                            self.lmsRedeemed = true;
                            self.isRedeemed = true;
                            self.hasPromoOption = response.hasPromoOption;
                            
                            if(self.quoteDetails.totalAllInclusive!=undefined && self.quoteDetails.totalAllInclusive!=null 
                            		&& response.lmsOptions.redeemedAmount!=undefined && response.lmsOptions.redeemedAmount!=null 
                            		&& parseFloat(self.quoteDetails.totalAllInclusive) == parseFloat(response.lmsOptions.redeemedAmount)){
                                self.payOnlyFromLMS = true;
                            }
                            
                            selSelectedPayOption();

                        if (self.quoteDetails.totalAllInclusive == response.lmsOptions.redeemedAmount) {
                            self.payOnlyFromLMS = true;
                        }

                        displayHideCCOptionDetails();
                    }
                },
                        function (error) {
                            self.isRewardsLoading = false;
                            alertService.setAlert(error.error, "danger");
                        });
            } else {
                $translate("msg_payment_redeem_amount").then(function (name) {
                    alertService.setAlert(name, "warning");
                });
                return false;
            }
        }


        function gotoFarePage() {
            var sp = commonLocalStorageService.getSearchCriteria();
            var params = {};
            angular.forEach(sp, function (pram, key) {
                params[key] = pram;
            });
            $state.go('fare', params);
        }


        function payLater() {

            // captcha empty string validation.
            if (self.onholdCaptchaEnable) {
                if (self.additionalDetails.captcha === undefined || self.additionalDetails.captcha === "") {
                    self.isInvalidCaptcha = true;
                    return;
                } else {
                    self.isInvalidCaptcha = false;
                }

                var tpg = self.selectedPaymentOption;

                if (tpg.cssClass.toLowerCase() === "cash") {


                    var param = {
                        "captcha": self.additionalDetails.captcha
                    }

                    paymentRemoteService.validateCaptcha(param, function (response) {

                        if (response.success && response.captchaTrue) {
                            self.isInvalidCaptcha = false;
                            self.submitPaymentData();
                        } else {
                            self.isInvalidCaptcha = true;
                            self.additionalDetails.captcha = "";
                            self.refreshCaptcha();
                        }
                    }, function (response) {
                    });


                }
            } else {
                self.submitPaymentData();
            }

        } // End of payLater.


        function backToHome() {

            $state.go('modifyDashboard', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
        }


        function checkBinPromo() {
            self.isPopupBinPromoModalOpen = true;
        }


        function binPromoPopupClose() {
            self.isPopupBinPromoModalOpen = false;
        }


        function resetBinPromotion() {
            self.binPromoModalStatus = {
                "promotionApplied": false,
                "alreadyApplied": false,
                "hasExistingPromotion": false,
                "noPromotion": null,
                "existingPromotionAmount": null,
                "discountAmount": 0.0,
                "lmsApplicableAmount": 0.0,
                "transactionFee": {},
                "effectivePaymentAmount": {},
                "promotionLoading": false
            };
        }


        function changeBinPromo() {
            self.resetBinPromotion();
        }


        function applyCreditCardPromo() {
            var params = paymentService.createPaymentParamsForOptions();
            params.promoCode = self.binPromoCode;
            params.overrideExistingPromotion = true;
            params.paymentGatewayId = self.selectedPaymentOption.paymentGateway.gatewayId;
            params.cardNo = self.cardInfo.cardNo;
            applyBinPromotion(params);
        }


        function applyBinPromo() {
            self.resetBinPromotion();
            var params = paymentService.createPaymentParamsForOptions();
            params.promoCode = self.binPromoCode;
            params.overrideExistingPromotion = false;
            params.paymentGatewayId = self.selectedPaymentOption.paymentGateway.gatewayId;
            params.cardNo = self.cardInfo.cardNo;
            params.cardType = self.cardInfo.cardType;
            applyBinPromotion(params);
        }


        function applyBinPromotion(params) {
            self.binPromoModalStatus.promotionLoading = true;
            paymentRemoteService.applyBinPromotion(params,
                function(response){
                    self.binPromoModalStatus = {
                        "promotionApplied":response.binPromotionDetails.promotionApplied,
                        "alreadyApplied":response.binPromotionDetails.alreadyApplied,
                        "hasExistingPromotion":response.binPromotionDetails.hasExistingPromotion,
                        "noPromotion":response.binPromotionDetails.noPromotion,
                        "existingPromotionAmount":response.binPromotionDetails.existingPromotionAmount,
                        "discountAmount":response.binPromotionDetails.discountAmount,
                        "lmsApplicableAmount":response.binPromotionDetails.lmsApplicableAmount,
                        "transactionFee": response.binPromotionDetails.transactionFee,
                        "effectivePaymentAmount": response.binPromotionDetails.effectivePaymentAmount,
                        "promotionLoading":false
                    };
                    binPromoData = self.binPromoModalStatus;
                    if(self.binPromoModalStatus.promotionApplied){
                        if(response.binPromotionDetails.lmsCredits!==null){
                            var lmsOption = {};
                            lmsOption.redeemedAmount = response.binPromotionDetails.lmsCredits.redeemedAmount;
                            lmsOption.lmsRedeemed = response.binPromotionDetails.lmsCredits.redeemed;
                            lmsOption.availablePoints = response.binPromotionDetails.lmsCredits.availablePoints;
                            lmsOption.equivalentAmount = response.binPromotionDetails.lmsCredits.availablePointsAmount;
                            lmsOption.redeemableAmount = response.binPromotionDetails.lmsCredits.maxRedeemableAmount;
                            self.airewards = lmsOption;
                            self.payableAmount = self.binPromoModalStatus.lmsApplicableAmount;
                        }else{
                            self.airewards = response.lmsOptions;
                            self.payableAmount = response.totalAmount;
                        }
                        self.paymentOption = filterPaymentOptions(response.paymentOptions);
                        self.totalAmount = response.totalAmount;
                        //backup lms
                        airRewards = response.lmsOptions;
                        totalToPayWithoutPromo = response.totalAmount;
                        //$timeout(function(){
                            self.binPromoCode = "";
                            //self.isPopupBinPromoModalOpen = false;
                            self.binPromoModalStatus.promotionLoading = false;
                            self.hasPromoOption = response.hasPromoOption;
                            self.binPromotion = true;
                            selSelectedPayOption();
                            //},1000);

                            updatePaymentSummary({binPromo: true});
                        }

                    },
                    function (response) {
                        self.isPopupBinPromoModalOpen = false;
                    });
        }


        function checkCardValidation() {
            var toSend = true;
            if (self.selectedPaymentOption.cssClass === 'CASH') {
                return toSend;
            }
            if (!self.selectedPaymentOption.paymentGateway.switchToExternalUrl) {
                if (self.selectedPaymentOption.cssClass === 'VISA' || self.selectedPaymentOption.cssClass === 'MASTER') {
                    if (!self.paymentCard.$valid) {
                        toSend = false
                    }
                }
            }
            if(toSend === true && self.cardInfo.alias != ""){
            	toSend = checkDuplicateSavedCardValidation();            	
            } else if(toSend === true && self.cardInfo.saveCreditCard === true) {
            	toSend = checkDuplicateSavedCardValidation();
            }

            return toSend
        }
        
        function checkDuplicateSavedCardValidation() {
        	var duplicateCardFlag = true;
        	if (self.savedCreditCardList.length && self.cardInfo.alias == "") {
        		angular.forEach(self.savedCreditCardList, function (value, key) {
        			var dateObj = value.expiryDate.split("-");
        			if (dateObj[1]) {
        				var expDateYearObj = parseInt(dateObj[0]);
        				var expDateMonthObj = parseInt(dateObj[1].replace(/^0+/, ''));
        				var cardHolderNameObj = value.holderName;
        				var cardNameObj = value.ccName;
        				var lastFourDigitOfCardObj = value.noLastDigits;
        				var inputcardNoLastFourDigit = self.cardInfo.cardNo.slice(-4);
        				if (expDateMonthObj == self.expDateMonth && expDateYearObj == self.expDateYear && angular.lowercase(cardHolderNameObj) == angular.lowercase(self.cardInfo.cardHoldersName) && lastFourDigitOfCardObj == inputcardNoLastFourDigit) {
        					duplicateCardFlag = false;
        					$translate("lbl_payment_duplicateCard").then(function (duplicateCardTitle) {
        						alertService.setAlert(duplicateCardTitle, "warning");
        					});
        				} else if (angular.lowercase(cardHolderNameObj)  == angular.lowercase(self.cardInfo.cardHoldersName) && angular.lowercase(cardNameObj) == angular.lowercase(self.cardInfo.cardName)){
        					duplicateCardFlag = false;
        					$translate("lbl_payment_duplicateCard").then(function (duplicateCardTitle) {
        						alertService.setAlert(duplicateCardTitle, "warning");
        					});
                        }
        			}
        		});
        	}
        	return duplicateCardFlag;
        }

        function checkPGWMandatoryFields() {
            if (_.contains(["PAYFORT_PAYSTORE", "PAY_FORT_ONLINE_INSTALLMENTS", "PAY_FORT_ONLINE_SADAD", "PAY_FORT_ONLINE_KNET"],
                    self.selectedPaymentOption.cssClass)) {
                return (self.pgwMandatoryParams.$valid);
            }
            return true;
        }


        /**
         *  Confirm Payment.
         */
        function submitPaymentData() {
            // isBlockSeatEnabled was used earlier to continue direclty to payment only if seats were manually set
            //var isBlockSeatEnabled = ancillaryService.getBlockSeatStatus();
            paymentService.setPaymetErrorMsg(null);
            if (self.airewards !== undefined && self.payOnlyFromLMS) {
                blockSelectedSeats();
                return;
            } else if (!self.payOnlyFromLMS && !self.payOnlyFromVoucher && getSelectedPayoption() === null) {
                alertService.setAlert("Please select a payment method.", "warning");
                return;
            }

            if (!self.payOnlyFromLMS && !self.payOnlyFromVoucher && self.selectedPaymentOption.paymentGateway !== undefined) {
                if (self.selectedPaymentOption.paymentGateway.brokerType === 'external' &&
                        (self.selectedPaymentOption.cssClass === 'PAYFORT_PAYSTORE' || self.selectedPaymentOption.cssClass === 'TAP_OFFLINE')) {
                    if (self.additionalDetails.param1 == undefined || self.additionalDetails.param1 == "") {
                        alertService.setAlert("Please add Email Address.", "warning");
                        return;
                    } else if (self.additionalDetails.param2 == undefined || self.additionalDetails.param2 == "") {
                        alertService.setAlert("Please add Mobile Number.", "warning");
                        return;
                    }
                }
            }
            blockSelectedSeats();
        }

        function blockSelectedSeats() {
            var param = ancillaryService.getAnciRequestJsonForBlockSeat();

            var onholdPaymentError = commonLocalStorageService.getData('PAYMENTONHOLD');
            if (param.ancillaries.length && self.currentAppMode !== applicationService.APP_MODES.BALANCE_PAYMENT
                    && self.currentAppMode !== applicationService.APP_MODES.NAME_CHANGE && !onholdPaymentError) {
                ancillaryRemoteService.blockAncillary(param, function (response) {
                    if (response.success) {
                        continuePayment();
                    } else {
                        ancillaryService.updateSeatMap();
                        return false;
                    }
                }, function (error) {
                    alertService.setPageAlertMessage(error);
                });
            } else {
                continuePayment()
            }

        }

        function continuePayment() {
            commonLocalStorageService.remove('PAYMENTONHOLD');
            var isPGWMandatoryFieldsValid = checkPGWMandatoryFields();
            if ((self.payOnlyFromLMS || self.payOnlyFromVoucher || checkCardValidation() || self.noPaymentRequiredWithCC) && isPGWMandatoryFieldsValid){
                self.setExpireDate();
                paymentService.setPaymentGateWayDetails(self.selectedPaymentOption);
                paymentService.setCardInfo(self.cardInfo);
                var isPaidTotalAmount = (self.payOnlyFromLMS || self.payOnlyFromVoucher || self.noPaymentRequiredWithCC) ? true : false;
                var param = paymentService.createPaymentParams(isPaidTotalAmount);
                param.captcha = self.additionalDetails.captcha;
                param.binPromotion = self.binPromotion;
                IBEDataConsole.log(param);
                loadingService.showPageLoading();
                self.paymentRetry = false;
                commonLocalStorageService.setData('SELECTED_PAYMENT_OPTION', self.selectedPaymentOption.cssClass +'_'+ 
                		(self.selectedPaymentOption.paymentGateway != undefined ? self.selectedPaymentOption.paymentGateway.providerCode : self.selectedPaymentOption.cardName));
                
                paymentRemoteService.submitPayment(param, function (response) {
                        IBEDataConsole.log(param);
	                    if (response.success) {
	                        if (response.paymentStatus === paymentService.PaymentStatus.SUCCESS ||
	                            response.paymentStatus === paymentService.PaymentStatus.ALREADY_COMPLETED) {//for payment status SUCCESS
	                            if (response.actionStatus === paymentService.ActionStatus.BOOKING_CREATION_SUCCESS ||
	                                response.actionStatus === paymentService.ActionStatus.BOOKING_MODIFICATION_SUCCESS) {
	                                if (response.pnr !== undefined) {
	                                	if(!self.payOnlyFromLMS && !self.payOnlyFromPromotion && !self.payOnlyFromVoucher && !self.noPaymentRequiredWithCC){
	                                		var totalWithAdminFee = param.makePaymentRQ.paymentOptions.paymentGateways[0].cards[0].paymentAmount;
	                                	}
	                                	commonLocalStorageService.setData('PNR', response.pnr);
	                                    commonLocalStorageService.setData('PNR_lastName', response.contactLastName);
	                                    commonLocalStorageService.setData('PNR_firstDepartureDate', response.firstDepartureDate);
	                                    commonLocalStorageService.setData('GoogleAnalytics_TotalWithAdminFee', totalWithAdminFee != undefined ? totalWithAdminFee : '');
	                                    commonLocalStorageService.setData("APP_MODE", applicationService.getApplicationMode());
	                                    
	                                    $state.go('confirm', {
	                                        lang: languageService.getSelectedLanguage(),
	                                        currency: currencyService.getSelectedCurrency(),
	                                        mode: applicationService.getApplicationMode()
	                                    });
	                                }
	                            }
	                        } else if (response.paymentStatus === paymentService.PaymentStatus.PENDING) {//for payment status PENDING
	                            if (response.actionStatus === paymentService.ActionStatus.REDIRET_EXTERNAL) {//for REDIRET_EXTERNAL
	                                createFormSubmit(response.externalPGDetails);
	                                commonLocalStorageService.setData('APP_MODE', applicationService.getApplicationMode());
	                                commonLocalStorageService.setData('langTOStore', languageService.getSelectedLanguage());
	                                commonLocalStorageService.setData('CurrencyToStore', currencyService.getSelectedCurrency());
	                            }
	                            if (response.actionStatus === paymentService.ActionStatus.ONHOLD_CREATED_CASH ||
	                                response.actionStatus === paymentService.ActionStatus.ONHOLD_CREATED_OFFLINE) {//for cash or offline
	                                if (response.pnr !== undefined) {
	                                    commonLocalStorageService.setData('resObject', response);
	                                    commonLocalStorageService.setData('ONHOLD_PNR', response.pnr);
	                                    $state.go('confirm_onhold', {
	                                        lang: languageService.getSelectedLanguage(),
	                                        currency: currencyService.getSelectedCurrency(),
	                                        mode: applicationService.getApplicationMode()
	                                    });
	                                }
	                            }
	
	                        } else if (response.paymentStatus === paymentService.PaymentStatus.ERROR) {//for payment status ERROR
	                            var alertObj = {};
	                            alertObj.msg = "Payment Failed";
	                            alertObj.type = "error";
	                            alertService.setAlert(alertObj);
	                        } else {
	                            var alertObj = {};
	                            alertObj.msg = "Payment Failed";
	                            alertObj.type = "error";
	                            alertService.setAlert(alertObj);
	                        }
	                    } else {
	                    	var error = {
	                                error:response.errors[0],
	                                errorType:"OTHER_ERROR"
	                            };
	                    	alertService.setPageAlertMessage(error);
	                    }
                    },
                    function (error) {
                        if (error.errorType === "PAYMENT_ERROR"){
                            self.paymentRetry = true;
                            paymentService.setPaymetErrorMsg(error.error);
                            self.paymentRetryMsg = $sce.trustAsHtml(paymentService.getPaymetErrorMsg());
                            loadingService.hidePageLoading();
                        }else if(error.errorType === "PAYMENT_ERROR_ONHOLD"){
                            // TODO - Need to handle this issue
                            paymentRemoteService.getOnholdPNR(function(data){
                                var errorMsg = error.error + " Your booking is placed ON-HOLD for "+data.onholdReleaseDuration+", please make the payment & confirm your booking. Bookings not paid for within the time limit will automatically be canceled"
                                commonLocalStorageService.setData('PAYMENTONHOLD', {pnr:data.pnr,error:errorMsg});
                                commonLocalStorageService.setData('RES_STATUS', "OHD");
                                applicationService.setApplicationMode(applicationService.APP_MODES.BALANCE_PAYMENT);
                                $state.go('modifyReservation', {
                                    lang: languageService.getSelectedLanguage(),
                                    currency: currencyService.getSelectedCurrency(),
                                    pnr: data.pnr,
                                    lastName: data.lastName,
                                    departureDate: data.firstDepartureDate
                                });

                            },function(){

                            });
                        }else{
                            alertService.setPageAlertMessage(error);
                        }
                    });
            }else{
                if (!isPGWMandatoryFieldsValid) {
                    self.pgwMandatoryParams.$setSubmitted();
                } else {
                    self.paymentCard.$setSubmitted();
                }
            }
        }
        function clearForm() {
            self.user = undefined;
        }


        /**
         *  Navigates to payment option.
         */
        function navigateToPaymentOptions() {
            var url = "http://www.airarabia.com/en/payment-options";
            window.open(url, "_blank", "modal=yes");
        }


        function fillExpDateArrays() {
            var year = parseInt(moment().format("YYYY"), 10);
            var month = parseInt(moment().format("MM"), 10);
            //self.expDateYear = year;
            for (var i = 0; i < 15; i++) {
                self.expDate.year[i] = year + i;
            }
            //self.expDateMonth = month;
            for (var i = 0; i < 12; i++) {
                self.expDate.month[i] = i + 1;
            }
        }


        function createFormSubmit(extPG) {
            var htmlStr = "";
            if (extPG.requestMethod === "POST") {
                htmlStr = extPG.postInputData;
                angular.element("#dummyForm").append(htmlStr);
                if (angular.element("#dummyForm").find("form").length > -1) {
                    if (angular.element("#dummyForm").find("form")[0].name !== undefined) {
                        var formName = angular.element("#dummyForm").find("form")[0].getAttribute("name");
                        document.forms[formName].submit();
                    } else if (angular.element("#dummyForm").find("form")[0].id !== undefined) {
                        var formID = angular.element("#dummyForm").find("form")[0].name;
                        document.getElementById(formID).submit();
                    }
                }
            } else if (extPG.requestMethod === "GET") {
                htmlStr = extPG.redirectionURL;
                $window.location = htmlStr;
            }
        }


        function updatePaymentSummary(prefs) {
            var req = paymentService.createParamsForPaymentSummaryCreateFlow();

            prefs = prefs || {};

            if (prefs.cashPayment) {
                req.onhold = true;
                self.previousSelectionCash = true;
            } else {
                req.onhold = false;
                self.previousSelectionCash = false;
            }

            req.binPromo = !!prefs.binPromo;

            self.loadingPaymentSummary = true;

            paymentRemoteService.retrievePaymentSummary(req,
                function (response) {
                    self.quoteDetails = response;
                    self.loadingPaymentSummary = false;
                    quoteService.setServiceTaxTotal(self.quoteDetails.totalServiceTaxes);
                    quoteService.setServiceTaxForAdminFee(self.quoteDetails.serviceTaxForAdminFee);
                },
                function(response){
                    alertService.setPageAlertMessage(response);
                    self.loadingPaymentSummary = false;
                }
            )
        }
        
        function updateInitialPaymentSummary(prefs){
            var req  = paymentService.createParamsForPaymentSummaryCreateFlow();

            prefs = prefs || {};

            if(prefs.cashPayment){
                req.onhold = true;
                self.previousSelectionCash = true;
            }else{
                req.onhold = false;
                self.previousSelectionCash = false;
            }

            req.binPromo = !!prefs.binPromo;

            self.loadingPaymentSummary = true;

            paymentRemoteService.retrievePaymentSummary(req,
                function (response) {
                    self.quoteDetails = response;
                    self.loadingPaymentSummary = false;
                    quoteService.setServiceTaxTotal(self.quoteDetails.totalServiceTaxes);
                    quoteService.setServiceTaxForAdminFee(self.quoteDetails.serviceTaxForAdminFee);
                    loadPaymentOptions();
                },
                function(response){
                    alertService.setPageAlertMessage(response);
                    self.loadingPaymentSummary = false;
                }
            )
	}

        function goBack() {
            $window.history.back();
        }
        
        function loadPaymentOptions(){
        	self.fareAditioalDisplay = ancillaryService.getAncillaryTotalTypewise();

            //Add Service details

            // If logged in enable airewards section
            //Check user logged in status
            self.showLogin = false;

            //Load Payment options
            var params = paymentService.createPaymentParamsForOptions();
            params.binPromotion = self.binPromotion;
            params.originCountryCode = commonLocalStorageService.getData('CARRIER_CODE');

            paymentRemoteService.retrievePaymentOptions(params, function (response) {

                // If user is logged in from frontend, but user is logged out from
                // server side, signout user.
                var userData = signInService.getUserDataFromLocalStorage();
                var loginId = _.isEmpty(userData) ? undefined : userData.loggedInCustomerDetails.emailId;

                if (loginId) {

                    signInService.isServerHasLoggedInUser({ loginId: loginId }, function (data) {
                        if (!data.customerLoggedIn && data.success) {
                            signInService.signoutUser({},function () {
                                alertService.setAlert("Your session has expired.");
                            }, function () {});
                        }
                    }, function () {
                    });
                }

                paymentService.setTransactionId(response.transactionId);
                airRewards = response.lmsOptions;
                totalToPayWithoutPromo = response.totalAmount;
                self.payableAmount = response.totalAmount;


                // Set total after promo code discount if available.
                // No need to do promotion deduction since all the deduction is handled
                // by server side.
                //self.payableAmount = GetTotalAfterPromoCodeDiscount();

                // Set total value in summary.
                eventListeners.notifyListeners(eventListeners.events.SET_TOTAL, self.payableAmount);

                self.onHoldStr = response.cash && response.cash.releaseTimeDisplay;
                self.cashTotal = response.cash && response.cash.payableAmount;

                self.paymentOption = filterPaymentOptions(response.paymentOptions);
                self.totalAmount = response.totalAmount;
                self.airewards = response.lmsOptions;
                self.hasPromoOption = response.hasPromoOption;

                // Set redeemed voucher information
                if (response.voucherOption !== null && response.voucherOption !== undefined) {
                	
    				self.reedemedGiftVoucherAmount = parseFloat(response.voucherOption.redeemedAmount);
    				self.totalReedemedGiftVoucherAmount = response.voucherOption.redeemedAmount;
    				self.voucherChecked = true
    				self.otpSuccessMessage = true;

				var balanceToPay = self.quoteDetails.totalAllInclusive
											- self.totalReedemedGiftVoucherAmount;
					if (self.airewards !== undefined
							&& self.airewards.redeemableAmount > balanceToPay) {
						self.airewards.redeemableAmount = balanceToPay;
					}

					if (response.voucherOption.totalAmountRedeem
							&& self.quoteDetails.totalAllInclusive === self.reedemedGiftVoucherAmount) {
						self.payOnlyFromVoucher = true;
					}

				if (balanceToPay !== undefined && balanceToPay === 0) {
    					self.useAnotherVoucher = null;
    				} else {
    					self.useAnotherVoucher = true;
    				}
    				
    				self.voucherValue = response.voucherOption.lastRedeemedVoucherId;
    				self.addVoucherId = self.voucherValue;
    				
    				self.voucherOtpModel = null;
    				self.otpErrorMessage = null;
    				
    				selSelectedPayOption();
                }
                displayHideCCOptionDetails();
                // Set the first payment option as the default payment method
                self.selectedPaymentOptionFlag = self.paymentOption[0].cardType + "_" +
                    self.paymentOption[0].paymentGateway.gatewayId + "_" + self.paymentOption[0].cssClass;
                self.setPayOption();
                
                selSelectedPayOption();

                loadingService.hidePageLoading();
                // Show busy loader.
                self.isRewardsLoading = false;

            },
            function (error) {
                alertService.setPageAlertMessage(error);
                self.isRewardsLoading = false;
            });
        }
        
        /*
         * Implementation is done to support mobile channels
         * But keeping the method to use in future incase an emergency
         */
        function filterPaymentOptions(paymentOptionsResponse){
        	if (paymentOptionsResponse != undefined && paymentOptionsResponse != null){
        		var filteredPaymentOptions = [];
        		for (var i=0; i < paymentOptionsResponse.length; i++){
        			if (!isDisabledPG(paymentOptionsResponse[i].paymentGateway)){
        				filteredPaymentOptions.push(paymentOptionsResponse[i]);
        			}
            	}
        		return filteredPaymentOptions;
        	} else {
        		return paymentOptionsResponse;
        	}
        }
        
        function showLMSPonitsSlider() {
        	self.isRedeemed = false
        	$timeout(function () {
			    $scope.$broadcast('rzSliderForceRender');
			}, 0, false);
        }
        
        function isDisabledPG(paymentGateway){
        	return false; // This is short cut hack to disable/enable specifc payment gateways in an emergency
        	/**
        	 * example filtering out payment gateways
        	if (paymentGateway != undefined && paymentGateway != null){
        		return (paymentGateway.brokerType === "external" && self.defaultCarrierCode === "G9" 
					&& (paymentGateway.gatewayId === 6));
        	} else {
        		return false;
        	}
        	**/
        }
        
        function loadSpecialMsgs() {
        	var flightDetails = commonLocalStorageService.getConfirmedFlightData();
        	if(flightDetails !== null)
        	{
        	var  params = { 
        		"languageCode": languageService.getSelectedLanguage(),
        		"originCode" : flightDetails[0].flight.originCode,
        		"destinationCode" : flightDetails[0].flight.destinationCode
        	};
             paymentService.airportSpecialMessages(params, 
            function (response) {
            	 if(response){
            		for (var i = 0; i < response.messages.length; i++) {
            			if(response.messages[i] === "Message code set is null or does not exists")
            				continue;
            			 self.specialMessages.push($sce.trustAsHtml(response.messages[i]));
            			 self.isSpecialMsgsAvailable = true;
					}
            	 } else
            		 self.isSpecialMsgsAvailable = false;

             },function (error) {
            		self.isSpecialMsgsAvailable = false;
            		console.log(error);

             });
        	}
        };
        
    

	function voucherCheckedSubmit(voucherChecked) {
		if (voucherChecked) {
			self.voucherChecked = true;
		} else {
			self.voucherChecked = false;			
			
			var param = paymentService.createPaymentParamsForOptions();
			param.voidRedemption = true;
			paymentRemoteService.voidReedemedGiftVoucher(param, function (response) {});
				
			self.totalReedemedGiftVoucherAmount = 0;
	        self.selectedPaymentOptionFlag = "";
	        self.selectedPaymentOption = null;
	        self.payOnlyFromVoucher = false;
	        self.voucherRedeemed = false;
	            
	        self.addVoucherId = null;
	    	self.voucherValue = null;
	    	self.reedemedGiftVoucherAmount = 0;
	    	self.unsuccessfulVoucherAttempts = 0;
	    	self.otpSuccessMessage = null;
	    	self.otpErrorMessage = null;
	    	self.voucherOtpValue = null;
	    	self.voucherOtpModel = null;

	        self.isPaymentRedeemed = false;
            self.isRedeemed = false; // Hide redeemed success message.
            totalRedeemedAmount = 0;
            self.redeemedPoints=0;
            self.selectedPaymentOptionFlag = "";
            self.selectedPaymentOption = null;
            self.redeem=0;
            init();
		}			
	}
	
	function reedemedGiftoucher(voucherValue) {
		var paxNameList = self.paxDetail;
		var voucherRedeemRequest =  {
			"paxNameList" : [ paxNameList.paxContact.firstName + " "
							+ paxNameList.paxContact.lastName ],
			"balanceToPay" : self.paymentDisplayAmounts[0].effectiveAmount,
			"voucherIDList" : [ voucherValue ],
			"unsuccessfulAttempts" : self.unsuccessfulVoucherAttempts
		}
		var params = {
			"voucherRedeemRequest":voucherRedeemRequest
		}
		paymentRemoteService.reedemedGiftoucher(params, function(response) {
			if (response.success === true || response.success === 'true') {
				if (response.payByVoucherInfo === null) {
					self.addVoucherId = voucherValue;
					self.voucherOtpModel = true;
				} else {
					self.addVoucherId = voucherValue;
					self.reedemedGiftVoucherAmount = parseFloat(response.payByVoucherInfo.redeemedTotal);
					var totalVoucherRedeemableAmount = self.paymentDisplayAmounts[0].effectiveAmount + self.totalReedemedGiftVoucherAmount;
					self.totalReedemedGiftVoucherAmount += self.reedemedGiftVoucherAmount;
					
					if(self.totalReedemedGiftVoucherAmount > totalVoucherRedeemableAmount) {
						self.totalReedemedGiftVoucherAmount = totalVoucherRedeemableAmount;
					}

					
					if (parseFloat(response.payByVoucherInfo.balTotalPay) <= 0) {
						self.payOnlyFromVoucher = true;
						self.useAnotherVoucher = null;
					} else {
						self.useAnotherVoucher = true;
					}
					
					if(self.airewards != undefined && self.airewards.redeemableAmount > response.payByVoucherInfo.balTotalPay){
						self.airewards.redeemableAmount = parseFloat(response.payByVoucherInfo.balTotalPay) + self.redeemedPoints;
					}
					displayHideCCOptionDetails()

					self.unsuccessfulVoucherAttempts = 0;
					
		            var params = paymentService.createPaymentParamsForOptions();
		            params.binPromotion = self.binPromotion;
		            params.voucherPaymentInProcess=true;
		            paymentRemoteService
		                    .retrievePaymentOptions(params, function (response) {
		                        var pgExists = false;
                                self.voucherSuccessMessage = true;
		                    	for (var i = 0; i < response.paymentOptions.length; i++) {
		                            var option = response.paymentOptions[i];
		                            if(option.paymentGateway != undefined && option.paymentGateway != null) {
		                            	pgExists = true;
		                            }
		                        }
		                    	if (!pgExists && response.voucherOption != null 
		                    			&& response.voucherOption.totalAmountRedeem == true) {
		                    		for (var i = 0; i < self.paymentDisplayAmounts.length; i++) {
		                    	        var effectivePayment = self.paymentDisplayAmounts[i];
		                    	        effectivePayment.effectiveAmount = 0;
		                    		}
		                    	} else {
		                    		self.paymentOption = filterPaymentOptions(response.paymentOptions);
		                    		selSelectedPayOption();
		                    	}
		                    
		                    }, function (error) {
		                        // Hide busy loader.
		                        alertService.setPageAlertMessage(error);
		                        self.isRewardsLoading = false;
		                    });
					
					//selSelectedPayOption();
				}
			} else {
				if (response.messages[0] !="") {
					alertService.setAlert(response.messages[0], "danger");
				} else {
					alertService.setAlert("error_voucher_invalid", "danger");
				}

				self.unsuccessfulVoucherAttempts++;
			}
		}, function(error) {
			self.unsuccessfulVoucherAttempts++;
			alertService.setAlert(error, "danger");
		})
	};
	

    function displayHideCCOptionDetails() {
			var totalPaidByLMSandVoucher = 0;
			
			if(self.redeemedPoints != undefined || self.redeemedPoints != null) {
				totalPaidByLMSandVoucher += self.redeemedPoints;
			}
			if(self.totalReedemedGiftVoucherAmount != undefined || self.totalReedemedGiftVoucherAmount != null) {
				totalPaidByLMSandVoucher += self.totalReedemedGiftVoucherAmount;
			}

			if (parseFloat(self.quoteDetails.totalAllInclusive) <= totalPaidByLMSandVoucher) {
				self.noPaymentRequiredWithCC = true;
			} else {
				self.noPaymentRequiredWithCC = false;
			}
    }

	function confirmVoucherOtp(voucherValue, otpValue) {
		self.addVoucherId = voucherValue
		var redeeemGiftVAmount = self.reedemedGiftVoucherAmount
		var paxNameList = self.paxDetail;
		var voucherRedeemRequest = {
			"paxNameList" : [ paxNameList.paxContact.firstName + " "
					+ paxNameList.paxContact.lastName ],
			"balanceToPay" : self.paymentDisplayAmounts[0].effectiveAmount,
			"voucherIDList" : [ voucherValue ],
			"unsuccessfulAttempts" : self.unsuccessfulVoucherAttempts,
			"otp" : otpValue
		}
		var params = {
			"voucherRedeemRequest":voucherRedeemRequest
		}
        self.isVoucherRedeemInProgress  = true;
		paymentRemoteService.reedemedGiftoucher(params, function(response) {
			if (response.success === true || response.success === "true") {
                self.reedemedGiftVoucherAmount = parseFloat(response.payByVoucherInfo.redeemedTotal);

				if(parseFloat(response.payByVoucherInfo.balTotalPay) <= 0) {
					self.payOnlyFromVoucher = true;
					self.useAnotherVoucher = null;
				} else {
					self.useAnotherVoucher = true;
				}

				if(self.airewards != undefined && self.airewards.redeemableAmount > response.payByVoucherInfo.balTotalPay){
					self.airewards.redeemableAmount = parseFloat(response.payByVoucherInfo.balTotalPay) + self.redeemedPoints;
				}
				displayHideCCOptionDetails()

				self.voucherOtpModel = null;
				self.otpErrorMessage = null;
				self.unsuccessfulVoucherAttempts = 0;
				self.voucherRedeemed = true;

	            var params = paymentService.createPaymentParamsForOptions();
	            params.binPromotion = self.binPromotion;
	            params.voucherPaymentInProcess=true;
	            paymentRemoteService
	                    .retrievePaymentOptions(params, function (response) {
                        var totalVoucherRedeemableAmount = self.paymentDisplayAmounts[0].effectiveAmount + self.totalReedemedGiftVoucherAmount;
                        self.totalReedemedGiftVoucherAmount += self.reedemedGiftVoucherAmount;

                        if (self.totalReedemedGiftVoucherAmount > totalVoucherRedeemableAmount) {
                            self.totalReedemedGiftVoucherAmount = totalVoucherRedeemableAmount;
                        }
                        self.otpSuccessMessage = true;
	                        var pgExists = false;
	                    	for (var i = 0; i < response.paymentOptions.length; i++) {
	                            var option = response.paymentOptions[i];
	                            if(option.paymentGateway != undefined && option.paymentGateway != null) {
	                            	pgExists = true;
	                            }
	                        }
	                    	if (!pgExists && response.voucherOption != null 
	                    			&& response.voucherOption.totalAmountRedeem == true) {
	                    		for (var i = 0; i < self.paymentDisplayAmounts.length; i++) {
	                    	        var effectivePayment = self.paymentDisplayAmounts[i];
	                    	        effectivePayment.effectiveAmount = 0;
	                    		}
	                    	} else {
	                    		self.paymentOption = filterPaymentOptions(response.paymentOptions);
	                    		selSelectedPayOption();
	                    	}
                        self.isVoucherRedeemInProgress = false;
	                    }, function (error) {
	                        // Hide busy loader.
                            self.isVoucherRedeemInProgress = false;
	                        alertService.setPageAlertMessage(error);
	                        self.isRewardsLoading = false;
	                    });

				//selSelectedPayOption();
			}else{
				self.otpSuccessMessage = null;
				self.otpErrorMessage = true;
				self.unsuccessfulVoucherAttempts++;
                self.isVoucherRedeemInProgress = false;
			}
		},
		function(error) {
			self.otpErrorMessage = true;
			self.unsuccessfulVoucherAttempts++;
            self.isVoucherRedeemInProgress = false;
		})
	};
	
	function resendVoucherOtp(voucherValue) {
		self.voucherOtpValue = null;
		var redeeemGiftVAmount = self.reedemedGiftVoucherAmount;
		var paxNameList = self.paxDetail;
		var voucherRedeemRequest = {
			"paxNameList" : [ paxNameList.paxContact.firstName + " "
					+ paxNameList.paxContact.lastName ],
			"balanceToPay" : self.paymentDisplayAmounts[0].effectiveAmount,
			"voucherIDList" : [ voucherValue ],
			"unsuccessfulAttempts" : self.unsuccessfulVoucherAttempts

		}
		var params = {
			"voucherRedeemRequest":voucherRedeemRequest
		}
		paymentRemoteService.reedemedGiftoucher(params, function(response) {
		}, function(error) {
			alertService.setAlert(error, "danger");
		})
	}
	;
	function useAnotherVoucherCode() {
		self.addVoucherId = null;
		self.voucherValue = null;
		self.reedemedGiftVoucherAmount = 0;
		self.unsuccessfulVoucherAttempts = 0;
		self.otpSuccessMessage = null;
		self.otpErrorMessage = null;
		self.voucherOtpValue = null;
		self.voucherOtpModel = null;
        self.voucherSuccessMessage = false;
	}
	;
    }

    return controllers;
};
