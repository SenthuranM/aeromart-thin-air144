/**
 * Created by indika on 11/5/15.
 */
var paymentService = function() {
  var service = {};
  service.paymentService = [
      "passengerService",
      "paymentFactory",
      "signInService",
      "applicationService",
      "modifyReservationService",
      "commonLocalStorageService",
      "$http",
      "ConnectionProps",
      function(passengerService, paymentFactory, signInService, applicationService, modifyReservationService, commonLocalStorageService, $http, ConnectionProps) {
    	
    	this._basePath = ConnectionProps.host;  
        this.PaymentStatus = {
          "SUCCESS": "SUCCESS",
          "PENDING": "PENDING",
          "ERROR": "ERROR",
          "ALREADY_COMPLETED": "ALREADY_COMPLETED"
        };

        this.ActionStatus = {
          "REDIRET_EXTERNAL": "REDIRET_EXTERNAL",
          "ONHOLD_CREATED_PAYMENT_FAILED": "ONHOLD_CREATED_PAYMENT_FAILED",
          "ONHOLD_CREATED_CASH": "ONHOLD_CREATED_CASH",
          "ONHOLD_CREATED_OFFLINE": "ONHOLD_CREATED_OFFLINE",
          "BOOKING_CREATION_SUCCESS": "BOOKING_CREATION_SUCCESS",
          "BOOKING_CREATION_FAILED": "BOOKING_CREATION_FAILED",
          "BOOKING_MODIFICATION_SUCCESS": "BOOKING_MODIFICATION_SUCCESS",
          "BOOKING_MODIFICATION_FAILED": "BOOKING_MODIFICATION_FAILED"
        };

        var selectedPaymentOption = null, transactionId = null, totalAmount = 0, payCardInfor = null;
        var recaptcha_Key = "6LfUVBYTAAAAAKfatbYTctv80r6YSDrYMg5arUwK", recaptcha_Response = null;

        // keep data related to modification price quot
        var modificationPaymentSummery = null;
        var totalEffectivePayment = {};
        var paymentErrorMsg = "" ;

        var _self = this;

        this.signInUser = function(loginParams, successFn, errorFn) {
          signInService.signInUser(loginParams, successFn, errorFn);
        };

        this.createPaymentParams = function(isPaidTotalAmount) {
          var params = {};
          totalEffectivePayment = {};
          params.paymode = applicationService.getApplicationMode();
          if(isPaidTotalAmount){
            params.makePaymentRQ = {
              "transactionId": transactionId,
              "bookingType": null,
              "paymentOptions": null,
              "captchaResponse": null,
              "binPromotion" : false
            };
          }else{
            params.makePaymentRQ = _self.getPaymentGateWayDetails();
          }

          if (applicationService.getApplicationMode() === applicationService.APP_MODES.BALANCE_PAYMENT
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.MODIFY_RESERVATION
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.NAME_CHANGE
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.MODIFY_SEGMENT
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
            params.rqParams = modifyReservationService.getReservationSearchCriteria();
          } else {
            var tempPaxData = passengerService.getPassengerDetails();
            params.reservationContact = tempPaxData.paxContact;
            params.paxInfo = tempPaxData.paxList;
          }
          return params;
        };

        this.createPaymentParamsForOptions = function() {
          var params = {};
          params.paymode = applicationService.getApplicationMode();
          if (applicationService.getApplicationMode() === applicationService.APP_MODES.BALANCE_PAYMENT
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.MODIFY_RESERVATION
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.NAME_CHANGE
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.MODIFY_SEGMENT
                  || applicationService.getApplicationMode() === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
            params.rqParams = modifyReservationService.getReservationSearchCriteria();
          } else {
            var tempPaxData = passengerService.getPassengerDetails();
            params.reservationContact = tempPaxData.paxContact;
            params.paxInfo = tempPaxData.paxList;
          }

          return params;
        };
        
        this.setPaymetErrorMsg = function(msg){
            paymentErrorMsg = msg
        }
        this.getPaymetErrorMsg = function(){
              return paymentErrorMsg;
        }

        this.createAdminFeeRQParams = function(){
            var param  = {};
            param.rqParam = {};
            param.rqParam.transactionId = this.getTransactionId();
            param.rqParam.pnr = modifyReservationService.getReservationSearchCriteria().pnr;
            param.appMode = applicationService.getApplicationMode();

            return param;

        }

        this.setTotalEffectivePaymentwithAdminFee = function(payment){
            totalEffectivePayment = payment;
        }
        this.getTotalEffectivePaymentwithAdminFee = function(amount){
            return totalEffectivePayment;
        }
        this.createParamsForPaymentSummaryCreateFlow = function(){
          var tempPaxData = passengerService.getPassengerDetails(),params={};
          var paxData = commonLocalStorageService.getData('PAX_DETAIL');
          
          params.paxInfo = tempPaxData.paxList;
          params.countryCode = paxData.paxContact.country;
          params.stateCode = paxData.paxContact.state;
          params.taxRegNo = paxData.paxContact.taxRegNo;
          
          return params;
        }

        this.setPaymentGateWayDetails = function(payOption) {
          selectedPaymentOption = payOption;
        };

        this.getPaymentGateWayDetails = function() {
          return formatPaymentOption(selectedPaymentOption);
        };

        this.setTransactionId = function(id) {
          transactionId = id
        };

        this.getTransactionId = function() {
          return transactionId;
        };

        this.setTotalAmountToPay = function(amount) {
          totalAmount = amount;
        };

        this.setCardInfo = function(cardInfo) {
          payCardInfor = cardInfo;
        };

        this.setModificationPaymentSummery = function(paymentSummery) {
          modificationPaymentSummery = paymentSummery;
        };

        this.getModificationPaymentSummery = function() {
          return modificationPaymentSummery;
        };
        
        this.airportSpecialMessages = function(requestParams, success, errorFn) {            
            var apiURL = this._basePath + "/postPayment/getAirportSpecialMessages";
            $http({
				"method" : 'POST',
				"url" : apiURL,
				"data" : requestParams
			}).success(function(response) {
				success(response);
			}).error(function(error) {
				errorFn(error);
			});
        };

        function formatPaymentOption(option) {
          var bookingType = null, payGateway = null, paymentOption, additionalDetails = null;
          var binPromotionApplied  = (option.binPromotionApplied != undefined) ? option.binPromotionApplied : false;
          if (option.cssClass.toLowerCase() === "cash") {
            var cardInfo = null;
            bookingType = "ONHOLD";
            payGateway = null;
            paymentOption = null;
          } else {
            payGateway = option.paymentGateway.gatewayId;
            if (option.paymentGateway.brokerType === "external") {
              var cardInfo = paymentFactory.getExtCardDetail();
              cardInfo.cardType = option.cardType;
              cardInfo.paymentAmount = totalAmount;
              cardInfo.cardName = option.cardName;
              if (option.cssClass.toLowerCase() === "payfort_paystore") {
                additionalDetails = payCardInfor.additionalDetails;
              }
              if (option.cssClass.toLowerCase() === "qiwi" || option.cssClass.toLowerCase() === "qiwi_offline") {
                additionalDetails = payCardInfor.additionalDetails;
              }
              if (_.contains(["PAYFORT_PAYSTORE", "PAY_FORT_ONLINE_INSTALLMENTS", "PAY_FORT_ONLINE_SADAD", "PAY_FORT_ONLINE_KNET"],
                      option.cssClass.toUpperCase())) {
                additionalDetails = payCardInfor.additionalDetails;
              }
            } else {
              var cardInfo = paymentFactory.getCardDetail();
              cardInfo.cardType = option.cardType;
              cardInfo.paymentAmount = totalAmount;
              cardInfo.expiryDate = payCardInfor.expiryDate;
              cardInfo.cardNo = payCardInfor.cardNo;
              cardInfo.cardHoldersName = payCardInfor.cardHoldersName;
              cardInfo.cardCvv = payCardInfor.cardCvv;
              cardInfo.saveCreditCard = payCardInfor.saveCreditCard;
              cardInfo.cardName = payCardInfor.cardName;
              cardInfo.alias = payCardInfor.alias;
            }
            paymentOption = {
              "paymentGateways": [{
                "gatewayId": payGateway,
                "cards": [cardInfo]
              }],
              "additionalDetails": additionalDetails
            }// payment option
          }// end if

          return {
            "transactionId": transactionId,
            "bookingType": bookingType,
            "paymentOptions": paymentOption,
            "captchaResponse": recaptcha_Response,
            "binPromotion" : binPromotionApplied
          };
        }

      }];
  return service;
}
