/**
 * Created at 22 Mar
 */
(function(){ 
	var giftT = new voucherThankController();
	var giftV = new voucherController();
	
    var voucher = angular.module("voucher", []);
    
    voucher.controller(giftV);
    voucher.controller(giftT);
    var vouService = new voucherService();
    voucher.service(vouService, [commonServices]);
    voucher.directive('counter', function() {
        return {
            restrict: 'E',
            scope: { value: '=value' },
            template: '<button type="button" class="btn-minus" ng-click="minus()" ng-disabled="value<=0">-</button>\
                      <input type="number" min="0" class="voucher-count" ng-model="value">\
                      <button type="button" class="btn-plus" ng-click="plus()">+</button>',
            link: function( scope , element , attributes ) {
                // Make sure the value attribute is not missing.
                if ( angular.isUndefined(scope.value) ) {
                    throw "Missing the value attribute on the counter directive.";
                }
                
                var min = angular.isUndefined(attributes.min) ? null : parseInt(attributes.min);
                var max = angular.isUndefined(attributes.max) ? null : parseInt(attributes.max);
                var step = angular.isUndefined(attributes.step) ? 1 : parseInt(attributes.step);
                
                scope.readonly = angular.isUndefined(attributes.editable) ? true : false;
                /**
                 * Sets the value as an integer.
                 */
                var setValue = function( val ) {
                    scope.value = parseInt( val );
                }
                
                // Set the value initially, as an integer.
                   setValue( scope.value );
                
                /**
                 * Decrement the value and make sure we stay within the limits, if defined.
                 */
                scope.minus = function() {
                    if ( min && (scope.value <= min || scope.value - step <= min) || min === 0 && scope.value < 1 ) {
                        setValue( min );
                        return false;
                    }
                    setValue( scope.value - step );
                };
                
                /**
                 * Increment the value and make sure we stay within the limits, if defined.
                 */
                scope.plus = function() {
                    if ( max && (scope.value >= max || scope.value + step >= max) ) {
                        setValue( max );
                        return false;
                    }
                    setValue( scope.value + step );
                };
                
            }
            
        }
    });
    
    voucher.directive('increment', function(){
    	  return {
    	    restrict: "C",
    	    link: function(scope, element, attrs) {
    	     element.on('click',function(){
    	        var index = element.attr("data-indexValue");
    	        $('#counter-'+index).find('.btn-plus').trigger('click');
    	      });
    	    }
    	  } 

    	});
    voucher.directive('voucherissue', function(){
  	  return {
  	    restrict: "C",
  	    link: function(scope, element, attrs) {
  	     element.on('click',function(){
  	        $('#voucherCheckout').trigger('click');
  	      });
  	    }
  	  } 

  	});
})();

