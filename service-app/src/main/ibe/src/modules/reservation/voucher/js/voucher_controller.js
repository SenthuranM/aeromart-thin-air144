var voucherController = function () {
    var controllers = {};
    controllers.voucherController = ["$filter", "$rootScope", "$stateParams", "$http", "stepProgressService", "$state", "alertService",
        "commonLocalStorageService", "currencyService", "languageService", "applicationService", "remoteVoucher", "eventListeners",
        "formValidatorService", "$anchorScroll", "$location", "$translate", "constantFactory", "popupService", "$q", "$sce",
        "paymentFactory", "paymentService", "loadingService", "$window", "voucherService",
        function ($filter, $rootScope, $stateParams, $http, stepProgressService, $state, alertService,
                  commonLocalStorageService, currencyService, languageService, applicationService, remoteVoucher,
                  eventListeners, formValidatorService, $anchorScroll, $location, $translate, constantFactory, popupService, $q, $sce,
                  paymentFactory, paymentService, loadingService, $window, voucherService) {
            var self = this;
            self.voucherDetail = [];
            self.showVoucherDetail = true;
            self.cardInfo = {};
            self.quoteDetails;
            self.baseCurrency = currencyService.getAppBaseCurrency();
            self.steps = stepProgressService.getAllSteps();
            self.steps[6].status = "current";
            self.steps[7].status = "pending";
            var contactData = null;
            self.contactValidation = formValidatorService.getValidations();
            self.selectedPaymentOptionFlag = null;
            self.showVisa = false;
            self.expDate = {month: [], year: []};
            self.isNoVouchersAvailable = false;
            self.showCart = !($state.current.data.isPaymentPage);
            self.contact = {
                firstName: "",
                lastName: "",
                mobileNumber: {countryCode: "", number: ""},
                remarks: ""
            };
            self.countryList = constantFactory.Countries();
            self.phoneNumberConfigs = {countryCode: true, areaCode: false, phoneNumber: true};
            self.nameTravellerMobileNumber = 'phone';
            self.countryCode = 'CountryCode';
            self.isValidCareerFor = isValidCareerFor;
            self.masterCardValidationPattern = /^5[1-5][0-9]{14}$/;
            self.visaCardValidationPattern = /^4[0-9]{12}(?:[0-9]{3})?$/;
            self.errorMessage = "";
            self.creditCardFee = false;
            self.showCreditCardFee = false;


            (function init() {
                if (!self.showCart) {
                    loadPaymentOptions();
                    fillExpDateArrays();
                }
            })();

            loadingService.showPageLoading();

            remoteVoucher.retrieveVoucherBannerData(function (data) {
                if (data == null || data.length == 0) {
                    self.isNoVouchersAvailable = true;
                }
                var voucherDetail = data;
                var selectionData = voucherService.getVoucherInformation();

                var currentDate = new Date();
                self.voucherVal = [];
                for (var n = 0; n < voucherDetail.length; n++) {
                    voucherDetail[n].voucherQuantity = 0;
                    voucherDetail[n].expiry = $filter('date')(new Date(currentDate.getTime()
                        + voucherDetail[n].voucherValidity * 24 * 60 * 60 * 1000), 'd MMM, y');
                    var indexOfDot = voucherDetail[n].amountInLocal.indexOf(".") - voucherDetail[n].amountInLocal.length;
                    if (parseInt(voucherDetail[n].amountInLocal) / 1000 >= 1) {
                        voucherDetail[n].amountInRequiredFormat =
                            voucherDetail[n].amountInLocal.slice(0, indexOfDot - 3) + ","
                            + voucherDetail[n].amountInLocal.slice(1, indexOfDot);
                    } else {
                        voucherDetail[n].amountInRequiredFormat = voucherDetail[n].amountInLocal.slice(0, indexOfDot);
                    }

                    if (voucherDetail[n].voucherId &&  _.isNumber(selectionData[voucherDetail[n].voucherId]) &&
                        (selectionData[voucherDetail[n].voucherId] > 0)) {
                        voucherDetail[n].voucherQuantity = selectionData[voucherDetail[n].voucherId];
                    }
                }
                self.voucherDetail = voucherDetail;
                loadingService.hidePageLoading();
            }, function (err) {
                loadingService.hidePageLoading();
            });

            self.isVoucherAvaiable = function () {
                var voucher;
                voucher = _.find(self.voucherDetail, function (element) {
                    return element.voucherQuantity > 0;
                });

                return voucher ? true : false;
            };

            self.getTotal = function () {
                var total = 0;
                var ccFee = 0;

                for (var i = 0; i < self.voucherDetail.length; i++) {
                    var x = self.voucherDetail[i];
                    if (x.voucherQuantity != undefined && x.voucherQuantity != null && !Number.isInteger(x.voucherQuantity)) {
                        alertService.setAlert("error_voucher_quantity_not_integer", "danger");
                        x.voucherQuantity = 0;
                    } else {
                        total += (x.amount * (x.voucherQuantity > 0 ? x.voucherQuantity : 0));
                    }
                }

                if (!_.isEmpty(self.voucherDetail)) {
                    var currentSelectionData = [];

                    _.forEach(self.voucherDetail, function (voucher) {
                        if (_.isNumber(voucher.voucherQuantity) && voucher.voucherQuantity > 0) {
                            currentSelectionData[voucher.voucherId] = voucher.voucherQuantity;
                        }
                    });
                    voucherService.setUpdatedVoucherInformation(currentSelectionData);
                }

                if (self.selectedPaymentOption && self.selectedPaymentOption.paymentGateway &&
                    _.isNumber(self.selectedPaymentOption.paymentGateway.transactionFeeValue)) {
                    if (self.selectedPaymentOption.paymentGateway.transactionFeeInPercentage) {
                        ccFee = total * self.selectedPaymentOption.paymentGateway.transactionFeeValue / 100;
                    } else {
                        ccFee = self.selectedPaymentOption.paymentGateway.transactionFeeValue;
                    }
                }

                self.showCreditCardFee = ccFee > 0;
                self.creditCardFee = ccFee = ccFee.toFixed(2);
                total = total.toFixed(2);
                total = (parseFloat(total) + parseFloat(ccFee)).toFixed(2);

                return total;
            };

            self.onContinueToCustomert = function () {
                if (getTotalVoucherQty() <= 0) {
                    $translate("msg_gift_vouchers_not_selected").then(function (msgSuccessfullyCardDeleted) {
                        popupService.alert(msgSuccessfullyCardDeleted);
                    });
                    return;
                }
                $state.go('voucherPayment', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()
                });
                //self.showCart = false;
                //$window.scrollTo(0, 0);
            };


            self.checkEmail = function (s) {
                var re = new RegExp("^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$");
                var ve = re.exec(s);
                if (ve == null) {
                    return false;
                } else {
                    return true
                }
            };

            function setExpireDate() {
                var expDate = moment([self.expDateYear, parseInt(self.expDateMonth) - 1, 1]);
                self.cardInfo.expiryDate = expDate.format("YYYY-MM-DDTHH:mm:ss");
            }

            self.valid = function (contact, cardInfo) {
                if (contact == undefined || contact == null || contact.mobileNumber.countryCode == null
                    || contact.mobileNumber.countryCode.trim() == ""
                    || contact.mobileNumber.number == null || contact.mobileNumber.number.trim() == ""
                    || parseInt(contact.mobileNumber.number) == 0
                    || contact.firstName == null || contact.firstName.trim() == "" || contact.lastName == null
                    || contact.lastName.trim() == "" || contact.emailAddress == null || contact.emailAddress.trim() == "") {
                    var old = $location.hash();
                    console.log("Locaton: ", old);
                    $location.hash('voucher-contact-panel');
                    $anchorScroll.yOffset = 100;
                    $anchorScroll();
                    $location.hash(old);
                    return false;
                }

                if (!self.selectedPaymentOption.paymentGateway.switchToExternalUrl) {
                    if (self.selectedPaymentOption.cssClass === 'VISA' || self.selectedPaymentOption.cssClass === 'MASTER') {
                        if (cardInfo == undefined || cardInfo == null || cardInfo.cardNo == null || cardInfo.cardNo.trim() == ""
                            || self.expDateMonth == null || self.expDateMonth.trim() == "" || self.expDateYear == undefined
                            || self.expDateYear == null || cardInfo.cardCvv == null || cardInfo.cardCvv.trim() == ""
                            || cardInfo.cardHoldersName.trim() == null || cardInfo.cardHoldersName.trim() == ""
                            || (!_.isNumber(cardInfo.cardType) && _.isEmpty(cardInfo.cardType))) {

                            var old = $location.hash();
                            console.log("Locaton: ", old);
                            $location.hash('voucher-cardinfo-panel');
                            $anchorScroll.yOffset = 100;
                            $anchorScroll();
                            $location.hash(old);
                            return false;
                        }
                    }
                }

                if (!self.checkEmail(contact.emailAddress.trim()))
                    return false;
                return true;

            };

            function createFormSubmit(extPG) {
                var htmlStr = "";
                if (extPG.requestMethod === "POST") {
                    htmlStr = extPG.postInputData;
                    angular.element("#dummyForm").append(htmlStr);
                    if (angular.element("#dummyForm").find("form").length > -1) {
                        if (angular.element("#dummyForm").find("form")[0].name !== undefined) {
                            var formName = angular.element("#dummyForm").find("form")[0].getAttribute("name");
                            document.forms[formName].submit();
                        } else if (angular.element("#dummyForm").find("form")[0].id !== undefined) {
                            var formID = angular.element("#dummyForm").find("form")[0].name;
                            document.getElementById(formID).submit();
                        }
                    }
                } else if (extPG.requestMethod === "GET") {
                    htmlStr = extPG.redirectionURL;
                    $window.location = htmlStr;
                }
            }

            self.onCheckout = function (form, cardInfo, contact) {
                var serviceData = {};
                var voucherDetail = []
                var vDetail = []
                for (i in self.voucherDetail) {
                    var voucherQuantity = self.voucherDetail[i].voucherQuantity;
                    if (voucherQuantity > 0) {
                        var voucherParams = {"voucherQuantity": self.voucherDetail[i].voucherQuantity}
                        vDetail.push(voucherParams);
                    }
                }
                if (vDetail.length > 0) {
                    if (self.valid(contact, cardInfo)) {
                        setExpireDate();
                        for (var i in self.voucherDetail) {
                            var voucherQuantity = self.voucherDetail[i].voucherQuantity;
                            if (voucherQuantity > 0) {
                                var cardInfoP = {
                                    "amount": self.voucherDetail[i].amount,
                                    "amountLocal": self.voucherDetail[i].amountInLocal,
                                    "cardExpiry": "" + self.expDateMonth + "" + self.expDateYear,
                                    "cardType": self.selectedPaymentOption.cardType,
                                    "cardNo": cardInfo.cardNo,
                                    "cardCvv": cardInfo.cardCvv,
                                    "cardHolderName": cardInfo.cardHoldersName,
                                    "cardTxnFeeLocal": 0.00
                                }
                                var voucherParams = {
                                    "amount": self.voucherDetail[i].amount,
                                    "amountInLocal": self.voucherDetail[i].amountInLocal,
                                    "currencyCode": self.voucherDetail[i].currencyCode,
                                    "voucherQuantity": self.voucherDetail[i].voucherQuantity,
                                    "paxFirstName": contact.firstName,
                                    "paxLastName": contact.lastName,
                                    "email": contact.emailAddress,
                                    "remarks": contact.remarks,
                                    "mobileNumber": contact.mobileNumber.countryCode + contact.mobileNumber.number,
                                    "validFrom": $filter('date')(new Date(new Date().getTime()), 'dd/MM/yyyy'),
                                    "validTo": $filter('date')(new Date(new Date().getTime() +
                                        self.voucherDetail[i].voucherValidity * 24 * 60 * 60 * 1000), 'dd/MM/yyyy'),
                                    "quantity": self.voucherDetail[i].voucherQuantity,
                                    "amountInBase": self.voucherDetail[i].amount,
                                    "templateId": self.voucherDetail[i].voucherId,
                                    "voucherPaymentDTO": cardInfoP

                                };
                                voucherDetail.push(voucherParams);
                            }
                        }
                        paymentService.setPaymentGateWayDetails(self.selectedPaymentOption);
                        paymentService.setCardInfo(self.cardInfo);

                        serviceData.voucherDTOList = voucherDetail;
                        serviceData.paymentGateways = paymentService.getPaymentGateWayDetails().paymentOptions.paymentGateways;
                        loadingService.showPageLoading();
                        remoteVoucher.retrieveVoucherPaymentData(serviceData).then(function (response) {
                            if (response.data.success) {

                                if (response.data.paymentStatus === paymentService.PaymentStatus.SUCCESS ||
                                    response.data.paymentStatus === paymentService.PaymentStatus.ALREADY_COMPLETED) {
                                    commonLocalStorageService.setData('voucherGroupId', response.data.voucherGroupId);
                                    commonLocalStorageService.setData('voucher_transactionId', response.data.transactionId);
                                    $state.go('voucherThank', {
                                        lang: languageService.getSelectedLanguage(),
                                        currency: currencyService.getSelectedCurrency(),
                                        mode: applicationService.getApplicationMode()
                                    });
                                    loadingService.hidePageLoading();

                                } else if (response.data.paymentStatus === paymentService.PaymentStatus.PENDING) {//for payment status PENDING
                                    if (response.data.actionStatus === paymentService.ActionStatus.REDIRET_EXTERNAL) {//for REDIRET_EXTERNAL
                                        createFormSubmit(response.data.externalPGDetails);
                                        commonLocalStorageService.setData('APP_MODE', applicationService.getApplicationMode());
                                        commonLocalStorageService.setData('langTOStore', languageService.getSelectedLanguage());
                                        commonLocalStorageService.setData('CurrencyToStore', currencyService.getSelectedCurrency());
                                    }
                                } else {
                                    alertService.setAlert("Payment Failed", "danger");
                                    loadingService.hidePageLoading();
                                }
                            } else {
                                alertService.setAlert("Payment Failed", "danger");
                                loadingService.hidePageLoading();
                            }
                        }, function (err) {
                            alertService.setAlert(err, "danger");
                            loadingService.hidePageLoading();
                        });
                    } else {
                        if (parseInt(contact.mobileNumber.number) == 0) {
                            alertService.setAlert("error_mobile_number_invalid", "danger");
                        } else {
                            alertService.setAlert("error_issue_voucher_checkout", "danger");
                        }
                    }
                } else {
                    alertService.setAlert("error_issue_voucher_checkout1", "danger");
                }
            };

            function filterPaymentOptions(paymentOptionsResponse) {
                if (paymentOptionsResponse != undefined && paymentOptionsResponse != null) {
                    var filteredPaymentOptions = [];
                    for (var i = 0; i < paymentOptionsResponse.length; i++) {
                        filteredPaymentOptions.push(paymentOptionsResponse[i]);
                    }
                    return filteredPaymentOptions;
                } else {
                    return paymentOptionsResponse;
                }
            }

            // Card Type
            function loadPaymentOptions() {
                remoteVoucher.loadPaymentOptions(function (response) {
                    if (response.success) {
                        self.steps[6].status = "completed";
                        self.steps[7].status = "current";
                        self.paymentOption = response.paymentOptions;
                        self.showVoucherDetail = false;
                        self.selectedPaymentOptionFlag = self.paymentOption[0].cardType + "_" +
                            self.paymentOption[0].paymentGateway.gatewayId + "_" + self.paymentOption[0].cssClass;
                        self.paymentOption = filterPaymentOptions(response.paymentOptions);
                        self.setPayOption(self.paymentOption[0].cssClass, self.paymentOption[0].cardType);
                    } else {
                        alertService.setPageAlertMessage(response.message);
                    }
                }, function (error) {
                    alertService.setPageAlertMessage(error);
                })
            }

            function getTotalVoucherQty() {
                var total = 0;
                for (var i = 0; i < self.voucherDetail.length; i++) {
                    var voucher = self.voucherDetail[i];
                    if (voucher.voucherQuantity != undefined && voucher.voucherQuantity != null && !Number.isInteger(voucher.voucherQuantity)) {
                        alertService.setAlert("error_voucher_quantity_not_integer", "danger");
                        voucher.voucherQuantity = 0;
                    }
                    total += voucher.voucherQuantity;
                }
                return total;
            }

            self.setPayOption = function () {

                $translate("lbl_payment_selectPatmentTypeTitle").then(function (selectPatmentTypeDropdownDefaultValue) {
                    self.selectExistingCreditCardDropdownList = false;
                    self.cardInfo.cardNo = '';
                    self.expDateMonth = '';
                    self.expDateYear = '';
                    self.cardInfo.cardHoldersName = '';
                    self.cardInfo.alias = '';
                    self.cardInfo.cardCvv = '';
                    self.selectPaymentType = selectPatmentTypeDropdownDefaultValue;
                    self.selectedPaymentType = selectPatmentTypeDropdownDefaultValue;
                });

                selSelectedPayOption();
            };

            function getSelectedPayoption() {
                var deferred = $q.defer();

                var toSend = null;
                for (var i = 0; i < self.paymentOption.length; i++) {
                    var option = self.paymentOption[i];
                    var optionPg = (option.paymentGateway) ? option.paymentGateway.gatewayId : "";
                    if (self.selectedPaymentOptionFlag !== null) {

                        if (option.cardType + "_" + optionPg + "_" + option.cssClass === self.selectedPaymentOptionFlag) {
                            option.cardNo = self.cardInfo.cardNo;
                            option.cardHoldersName = self.cardInfo.cardHoldersName;
                            option.cardCvv = self.cardInfo.cardHoldersName;
                            toSend = option;
                            break;
                        }
                    }
                }

                if (!toSend) {
                    deferred.reject(toSend);
                } else {
                    deferred.resolve(toSend)
                }
                return deferred.promise;
            };


            function selSelectedPayOption() {
                //set on hold String
                getSelectedPayoption().then(function (resp) {
                    self.selectedPaymentOption = resp;

                    if (self.selectedPaymentOption.paymentGateway) {
                        self.currentPaymentCurrency = self.selectedPaymentOption.paymentGateway.paymentCurrency;
                    }
                    self.currentPaymentCurrency = self.currentPaymentCurrency || self.baseCurrency;
                    self.paymentDisplayAmounts = [];

                    var effectivePayment = {};

                    var tpg = self.selectedPaymentOption,
                        displayFlg = true;

                    if (tpg !== null) {

                        //check if paymentCurrency equals to selected or baseCurrency
                        if (tpg.paymentGateway !== undefined) {
                            if (tpg.paymentGateway.paymentCurrency !== currencyService.getAppBaseCurrency()) {
                                displayFlg = false;
                            }
                        }

                        if (tpg.paymentGateway.brokerType === "external") {

                            self.cardInfo = paymentFactory.getExtCardDetail();
                            if (tpg.cssClass.toLowerCase() === "payfort_paystore") {
                                self.cardInfo.additionalDetails = self.additionalDetails;
                            }
                            if (tpg.cssClass.toLowerCase() === "qiwi" || tpg.cssClass.toLowerCase() === "qiwi_offline") {
                                self.cardInfo.additionalDetails = self.additionalDetails;
                            }
                            if (_.contains(["PAYFORT_PAYSTORE", "PAY_FORT_ONLINE_INSTALLMENTS",
                                        "PAY_FORT_ONLINE_SADAD", "PAY_FORT_ONLINE_KNET"],
                                    tpg.cssClass)) {
                                self.cardInfo.additionalDetails = self.additionalDetails;
                                if (tpg.cssClass == "PAY_FORT_ONLINE_INSTALLMENTS") {
                                    $translate("msg_payFort_pgw_installments_" + tpg.paymentGateway.paymentCurrency.toUpperCase())
                                        .then(function (name) {
                                            self.selectedPaymentOption.translatedMessage = name;
                                        });
                                }
                            }
                        } else {
                            self.cardInfo = paymentFactory.getCardDetail();
                        }
                        self.paymentDisplayAmounts.push(effectivePayment);

                    } // if (tpg !== null)
                })

            }

            function scrollToCardDetails() {
                $('html, body').animate({
                    scrollTop: $(".master-card-details").offset().top
                }, 1000);
            }

            function fillExpDateArrays() {
                var year = parseInt(moment().format("YYYY"), 10);
                var month = parseInt(moment().format("MM"), 10);
                // self.expDateYear = year;
                for (var i = 0; i < 15; i++) {
                    self.expDate.year[i] = year + i;
                }
                // self.expDateMonth = month;
                for (var i = 0; i < 12; i++) {
                    var month = i + 1;
                    if (month > 9)
                        self.expDate.month[i] = "" + month;
                    else
                        self.expDate.month[i] = "0" + month;
                }
            }

            self.setExpireDate = function () {
                var expDate = moment([this.expDateYear,
                    parseInt(this.expDateMonth) - 1, 1]);
                self.cardInfo.cardExpiry = (expDate.format("MM-YY"))
                    .replace(/-/g, '');
            };


            self.onClear = function () {
                self.cardInfo.cardCvv = "";
                self.cardInfo.cardHoldersName = "";
                self.cardInfo.cardNo = "";
                self.cardInfo.cardExpiry = "";
                self.expDateMonth = "";
                self.expDateYear = "";
                self.contact.firstName = "";
                self.contact.lastName = "";
                self.contact.emailAddress = "";
                self.contact.mobileNumber.countryCode = "";
                self.contact.mobileNumber.number = "";
                self.contact.remarks = "";
            };


            function onCountryChange(val) {
                self.contact.mobileNumber.countryCode = val.phoneCode;
            }

            function isValidCareerFor(airlineName) {
                return applicationService.isValidCareerFor(airlineName);
            }
        }];
    return controllers;

};
