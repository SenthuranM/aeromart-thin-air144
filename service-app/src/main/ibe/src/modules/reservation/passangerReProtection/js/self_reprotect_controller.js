/**
 * Created by NIIT
 */

var reProtectController = function () {
    var controllers = {};

    controllers.reProtectController = [
        "$scope",
        "$location",
        "$window",
        "$state",
        "$stateParams",
        "$translate",
        "IBEDataConsole",
        "applicationService",
        "popupService",
        "alertService",
        "confirmRemoteService",
        "reProtectRemoteService",
        reProtectsController
    ];

    function reProtectsController($scope, $location,$window, $translate, $state, $stateParams, IBEDataConsole, applicationService, popupService, alertService, confirmRemoteService, reProtectRemoteService) {

       // Global vars.
    	var self = this,
    	transferredFlight,
    	SelectedId = 0,
    	reqData = {};
    	
    	
      // Properties.	
    	self.mainTtl = true;
    	self.confirmTtl = false;
    	self.congrateTtl = false;
    	self.enableProceed = false;
    	self.enableChooseAnother = true;
    	self.contShow = false;
    	self.chekBoxShow = true;
    	self.confirmStatus = false;
    	self.isErrorMsgs = false;
        self.noSelectedFlight = true;
    	
    // Functions.
    	self.splitOrgDest = splitOrgDest;
    	self.timeDuration = timeDuration;
    	self.handleRadioClick = handleRadioClick;
    	self.chooseAnotherFlight = chooseAnotherFlight;
    	self.continueFlight = continueFlight;
    	self.confirmFlight = confirmFlight;
    	self.print = print;
    	self.sendEmail = sendEmail;
    	self.adultArray = [];
    	self.childArray = [];
    	self.infantArray = [];
    	
    	
        var reprotectParams = $location.url().split("/");
        var searchParams = {        		
        		"language" : reprotectParams[2],
                "pnr" : reprotectParams[3],
                "alertId" : reprotectParams[4],
                "pnrSegmentId" : reprotectParams[5],
                "oldFlightSegmentId" : reprotectParams[6]
         }
    	
        reProtectRemoteService.retrieveGetReservationDetailsGridData(searchParams, function(response) {
        	if (response.success) {
                self.reservationDetailsGridData = response;
                for(var i=0;i< self.reservationDetailsGridData.reservationDetailsList.length;i++){
                	var paxValue =  self.reservationDetailsGridData.reservationDetailsList[i].reservationDetailsDTO.paxType;
                	var arrayValue = self.reservationDetailsGridData.reservationDetailsList[i];
                	if(paxValue === 'AD')
                		self.adultArray.push(arrayValue);
                	if(paxValue === 'CH')
                		self.childArray.push(arrayValue);
                	if(paxValue === 'IN')
                		self.infantArray.push(arrayValue);
                }
              self.reservationDetailsList = self.adultArray.concat(self.childArray).concat(self.infantArray);
                self.cancelledFlight = self.reservationDetailsGridData.oldFlightSegment;
                self.cancelledFlightSegmentCode = splitOrgDest(self.cancelledFlight.segmentCode);
                var defaultReprotectedFlightSegId = self.reservationDetailsList[0].reservationDetailsDTO.flightSegId;
                self.reprotectedFlts = self.reservationDetailsGridData.flightsList;
                for(var i in self.reprotectedFlts){
                	if (self.reprotectedFlts[i].fltSegId == defaultReprotectedFlightSegId){
                		self.reprotectedFlts[i].show= true;
                		self.reprotectedFlts[i].isFlight=true;  
                		SelectedId = i;
                        self.noSelectedFlight = false;
                    } 
                }
                
        		} else {
        			self.isErrorMsgs = true;
        			self.message = response.messages[0]
        		}
        	}, function (error) {
                alertService.setAlert(error);
            });
        
         function splitOrgDest(segCode) {
	    	return segCode.split("/");
	    }
	
		function timeDuration(depDate, arvalDate) {
				var departureDate = moment(depDate).format(), 
				    arrivalDate = moment(arvalDate).format(),
				    duration = moment.duration(moment(arrivalDate).diff(departureDate)),
				    hours = parseInt(duration.asHours()),
				    minutes = parseInt(duration.asMinutes()) - hours * 60;
				if (hours < 10) {
					hours = '0' + hours;
				}
				return hours + ':' + minutes;
		}
        
      
      function handleRadioClick(Id) {
    	  SelectedId = Id;
    	  self.reprotectedFlts[Id].isFlight=true;
      };
      
      
      function chooseAnotherFlight(){
    	 
    	  for(var i=0; i< self.reprotectedFlts.length; i++){
              self.reprotectedFlts[i].isFlight=false;
              self.reprotectedFlts[i].show=true;
    	  }
    	 
    	  self.reprotectedFlts[SelectedId].isFlight=true;
    	  
    	  self.enableChooseAnother = false;
    	  self.contShow = true;
    	  self.enableProceed = false;
    	  self.mainTtl = true;
          self.confirmTtl = false;
          self.chekBoxShow = true;
      }
      
      function continueFlight() {
    	  for(var i=0; i< self.reprotectedFlts.length; i++){
    		  self.reprotectedFlts[i].show=false;
    	  }
    	  self.reprotectedFlts[SelectedId].show=true;
      	self.mainTtl = false;
      	self.confirmTtl = true;
      	self.congrateTtl = false;
      	self.enableProceed = true;
      	self.enableChooseAnother = false;
      	self.contShow = false;
      	self.chekBoxShow = false;
      }

      function confirmFlight(){
    	 reqData.pnr = searchParams.pnr;
    	 reqData.alertId = searchParams.alertId;
    	 reqData.pnrSegmentId = searchParams.pnrSegmentId;
    	 reqData.arrivalDate = self.reprotectedFlts[SelectedId].arrivalDate;
    	 reqData.departureDate = self.reprotectedFlts[SelectedId].departureDate;
    	 reqData.fltSegId = self.reprotectedFlts[SelectedId].fltSegId;
    	 self.isBusyLoaderVisible = true;
         reProtectRemoteService.transferReservationSegments(reqData, function(response) {
        	 if (response.success) {
        		 self.enableProceed = false;
            	 self.confirmTtl = false;
            	 self.congrateTtl = true;
            	 self.confirmStatus = true;
	        	 console.log("Success : ", response);
	        	 transferredFlight = response;
        	   } else {
	       			popupService.alert(response.messages[0]);
        	   }
    	        	 	self.isBusyLoaderVisible = false;
        	 }, function (error) {
    	        		 self.isBusyLoaderVisible = false;
                 alertService.setAlert(error);
             });
    	 
     }
     

     var dialogTranslate = {
             popupblocker: "Please disable pop-up blocker to print reservation",
         };
     
     
     function print() {

         var params = getServiceParameters();

         confirmRemoteService.getPrintMarkup(params, function (response) {
             if (!response.success) {
                 IBEDataConsole.log('Error: something went wrong, ', 'reprotectionService.print function');
                 return;
             }

             var printWindow = $window.open("");

             if(!printWindow){
                 popupService.alert(dialogTranslate.popupblocker);
             }else{
                 printWindow.document.body.innerHTML = response.itinerary;
                 printWindow.print();
             }

         }, function (response) {
             IBEDataConsole.log(response);
         });
     }
     
     function sendEmail() {
         
         var params = getServiceParameters();

         confirmRemoteService.sendEmail(params, function (response) {
             if (response.success) {
                alertService.setAlert("Email sent successfully.&lrm;", "success");     
             }
             
         }, function (error) {
             alertService.setAlert(error);
         });

     }
     
     function getServiceParameters() {

         var applicationMode = applicationService.getApplicationMode();
         applicationMode = (typeof applicationMode === "string") ? applicationMode.toUpperCase() : applicationMode;

         return {
             pnr: transferredFlight.pnr,
             groupPnr: transferredFlight.groupPnr,
             itineraryLanguage: searchParams.language,
             operationType: applicationMode,
             viewMode: "loadRes" 
         };
     }
    }

    return controllers;
};