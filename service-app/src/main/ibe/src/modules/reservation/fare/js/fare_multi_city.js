(function(){
    var tfc = new travelFareController();
    var msc = new modifySearchController();
    var fareService = new travelFareService();
    var travelFare = angular.module("travelFare", []);


    travelFare.directive('validateDate', [function(){
        return{
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                attrs.$observe('validateDate', function(){
                    var timeDiff = Date.parse(attrs.validateDate.replace(/['"]+/g, '')) - moment(elem.val(), "YYYY-MM-DD");

                    if(timeDiff < 0){
                        ctrl.$setValidity('Arrival date must be after departure', true);
                    }else{
                        ctrl.$setValidity('Arrival date must be after departure', false);
                    }

                });
                scope.$watch(attrs.ngModel, function (v) {
                    var timeDiff = Date.parse(attrs.validateDate.replace(/['"]+/g, '')) - moment(elem.val(), "YYYY-MM-DD");

                    if(timeDiff < 0){
                        ctrl.$setValidity('Arrival date must be after departure', true);
                    }else{
                        ctrl.$setValidity('Arrival date must be after departure', false);
                    }
                });
            }
        }
    }]);

    travelFare
        .controller(msc)
        .controller(tfc);
    travelFare.service(fareService, [commonServices]);
    var travelFareDataModel = function (){
        var travelFare = {};
        var modifySearch ={};

           /*fromAirport:"",
            toAirPort:"",
            departureDate:"",
            arrivalDate:"",
            adultsCount:"",
            infantsCount:"",
            ChildCount:"",
            cabinClass:"",
            promocode:""*/

        travelFare.setFromAireport = function(airport){
            modifySearch.fromAirport = airport;
        }
        travelFare.getFromAireport = function(airport){
            return modifySearch.fromAirport;
        }
        return travelFare;
    }
})();
