/**
 * Created by indika on 11/5/15.
 */
var travelFareService = function(){
    var service = {};
    service.travelFareService = ["$http", "commonLocalStorageService", "eventListeners","httpServices","quoteService",
        "localStorageService","currencyService","busyLoaderService","constantFactory","applicationService",
        function ($http, commonLocalStorageService, eventListeners, httpServices, quoteService, localStorageService, currencyService,
                  busyLoaderService, constantFactory,applicationService) {
        var _sectors = [], listeners =[], selectedFlight = {} ,quoteDetails,drawerBusyLoaderShow = false,journeyTypeLocal = false;
            var sectorsFareData={
                noOfSectors: 0,
                sectors: []
            };

        var _this = this;

        this.initService = function(){
            _sectors = [];
            listeners =[];
            selectedFlight = {};
            quoteDetails;
            drawerBusyLoaderShow = false;
            var sectorsFareData={
                noOfSectors: 0,
                sectors: []
            };
        };

        this.serviceObject; //Object to hold all the extra services
        this.EVENTS = {
            FARE_TIMELINE_FILLED:"fare_timeline_filled",
            FARE_TIMELINE_DATE_FILLED:"fare_timeline_date_filled",
            SELECTED_DATE_CHANGED:"change_selected_date",
            SELECTED_FARE_CHANGED:"selected_fare_changed",
            FLIGHT_SELECTION_CONFIRMED:"flight_selection_confirmed",
            MODIFY_SEARCH: "modify_search_clicked"
        };
        this.notifyListeners = function(type, data){
            if(listeners[type] !== undefined) {
                angular.forEach(listeners[type], function (list) {
                    list(data);
                });
            }
        };
        var passenger = {firstName:"Brendan",
            lastName:"Eich",
            extras: {
                meal: {},
                baggage: {
                    handLuggage: 10,
                    mainLuggage: 0,
                    amount: 0
                },
                seat: {},
                seatAssigned:false,
                previousSeat:null,
                insurance: {},
                //Services should contain an object for each service purchased
                services: {}
            }

        }
        var flightSector = {from: 'SHJ', to: 'CMB',flightRPH:"001",
            extrasTotal:{meals:{total: 0},seats: {total: 0}, baggage:{total: 0}, insurance:{total: 0},services:{total:0}},
            passenger:[passenger],
            total:0
        }

        var createOndInfo = function(searchCriteria, flg){
            var dest = 'destination',
                origin = 'origin',
                destName = 'destinationName',
                originName = 'originName',
                originCity = 'originCity',
                destCity = 'destCity',
                depDate = 'departureDate',
                departureVar = "departureVariance",
                lMessage = "msg_fare_loadingMsgForOutbound";
            if (flg==='return'){
                dest = 'origin';
                origin = 'destination';
                destName = 'originName';
                originCity = 'destCity';
                destCity = 'originCity';
                originName = 'destinationName';
                depDate = 'returnDate';
                departureVar = "returnVariance";
                lMessage = "msg_fare_loadingMsgForInbound";
            }
            return {
                origin:searchCriteria[origin],
                originCity:searchCriteria[originCity],                
                destCity:searchCriteria[destCity],

                originName:searchCriteria[originName],
                destination:searchCriteria[dest],
                destinationName: searchCriteria[destName],
                departureDate: moment(searchCriteria[depDate], "DD-MM-YYYY").format("YYYY-MM-DDTHH:mm:ss"),
                departureVariance :searchCriteria[departureVar],
                cabinClass:searchCriteria['cabinClass'],
                promoCode:searchCriteria['promoCode'],
                passengers:searchCriteria['passengers'],
                loadingMsg:lMessage
            }
        };

        this.registerListeners = function(type,listener){
            if(listeners[type] !== undefined){
                listeners[type] = [listener];
            }else{
                listeners[type] = [listener];
            }
        };

        this.getSearchCriteria = function(){
            var criteria = commonLocalStorageService.getSearchCriteria();
            return {
                origin: criteria.from,
                originName: commonLocalStorageService.getAirportCityName(criteria.from, (criteria.originCity === 'Y')),
                destination: criteria.to,
                destinationName: commonLocalStorageService.getAirportCityName(criteria.to, (criteria.destCity === 'Y')),
                departureDate: criteria.depDate,
                originCity:criteria.originCity,
                destCity:criteria.destCity,
                departureVariance: 3,
                returnDate: criteria.retDate,
                returnVariance: 3,
                cabinClass:criteria.cabin,
                passengers:{
                    adults: criteria.adult,
                    children: criteria.child,
                    infants: criteria.infant
                },
                journeyType: (criteria.retDate==undefined)?"multiCity":(criteria.retDate=='N')?"oneway":"return",
                promoCode: criteria.promoCode,
                noOfONDs:1
            };
        };

        this.setSectors = function(data){
            commonLocalStorageService.setData('SECTORS',data);
        };

        this.getSectors = function(){

            //If the serviceObject is not defined (ie: The first time getExtras is called) we instantiate new object
            if(!_this.serviceObject){
                //If there is data set in the local storage that data is called
                if(commonLocalStorageService.getData('SECTORS')){
                    _this.serviceObject = commonLocalStorageService.getData('SECTORS');
                }
                //Else, the factory is called in order to generate the sectors
                else{
                    _this.serviceObject = setFlightData();
                }
            }
            //else, the already created serviceObject is returned
            return _this.serviceObject;
        };
        this.isjourneyTypeLocal = function(){
            return journeyTypeLocal;
        }

        function setFlightData(){
            var sectorData = {};
            if(selectedFlight != undefined) {
                angular.forEach(selectedFlight, function (sector, key) {
                    var flsector = angular.extend({},flightSector);
                    if (!sector.flight.returnFlag) {
                        if(sectorData['Outbound'] == undefined){
                            sectorData.Outbound = [];
                        }
                        var count = sectorData['Outbound'].length;
                        sectorData['Outbound'][sectorData['Outbound'].length] = flsector;
                        sectorData['Outbound'][sectorData['Outbound'].length-1]['from'] = sector.flight.originCode;
                        sectorData['Outbound'][sectorData['Outbound'].length-1]['to'] = sector.flight.destinationCode;
                        sectorData['Outbound'][sectorData['Outbound'].length-1]['flightRPH'] = sector.flight.flightRPH;

                    } else {
                        if(sectorData['Inbound'] == undefined){
                            sectorData.Inbound = [];
                        }
                        var count = sectorData['Inbound'].length;
                        sectorData['Inbound'][sectorData['Inbound'].length] = flsector;
                        sectorData['Inbound'][sectorData['Inbound'].length-1]['from'] = sector.flight.originCode;
                        sectorData['Inbound'][sectorData['Inbound'].length-1]['to'] = sector.flight.destinationCode;
                        sectorData['Outbound'][sectorData['Outbound'].length-1]['flightRPH'] = sector.flight.flightRPH;

                    }

                    if(!journeyTypeLocal && sector.flight.journeyType == "INTERNATIONAL"){
                        journeyTypeLocal = true;
                    }

                })
            }else{
                //for development purpose
                /*var searchData = travelFareService.getSearchCriteria();

                 if(sectorData.Outbound != undefined) {
                 sectorData.Outbound[0].from = searchData.origin
                 sectorData.Outbound[0].to = searchData.destination
                 sectorData.Outbound[0].passenger.push(passenger);
                 }*/
            }
            return sectorData;
        }

        this.setSectorsOndInfo = function(searchCriteria){

            _sectors = [];
            var flags = ["oneway"];

            if (searchCriteria.journeyType==="return"){
                searchCriteria.noOfONDs = 2;
                flags[flags.length] = "return";
            }else{
                searchCriteria.noOfONDs = 1;
            }

            for (var i=0;i<searchCriteria.noOfONDs;i++){
                _sectors[_sectors.length] = createOndInfo(searchCriteria, flags[i]);
            }

            _this.notifyListeners(_this.EVENTS.FARE_TIMELINE_FILLED, searchCriteria);
        };
        
        this.setUpdatedSectorsOndInfo = function(sectorFlight, sectorIndex){
            _sectors[sectorIndex].origin = sectorFlight.originCode;
            _sectors[sectorIndex].originName = sectorFlight.originName;
            _sectors[sectorIndex].destination = sectorFlight.destinationCode;
            _sectors[sectorIndex].destinationName = sectorFlight.destinationName;
        };

        this.getSectorsInfo = function(index){
            return _sectors[index];
        };
        function getSectorData(index){
            return _sectors[index];
        }
        this.setConfirmedFlights = function (data,seq){
            selectedFlight[seq] = data;
        }
        this.getConfirmedFlights = function(){
            return selectedFlight;
        }

        this.removeConfirmedFlight = function(seq){
            delete selectedFlight[seq];
        };

        this.fareQuoteDataMapping = function(fareQuoteData,sectorsData) {
            ;
            var t = this.isInbound;
            var searchParam = localStorageService.get('SEARCH_CRITERIA');
            var inSelectedCurr = {};
            var flightPricing = fareQuoteData.selectedFlightPricing;
            var segmentData = flightPricing.ondOWPricing;
            for(var i =0;i<segmentData.length  ;i++ ){
                segmentData[i].sectorInfo = getSectorData(i);
                if(!segmentData[i].totalSurcharges){
                    segmentData[i].totalSurcharges = 0;
                }
                if(!segmentData[i].totalTaxes){
                    segmentData[i].totalTaxes = 0;
                }

            }
            inSelectedCurr.totalTaxAndSurcharges = parseFloat(flightPricing.total.surcharge)+parseFloat(flightPricing.total.tax);
            inSelectedCurr.segmentFare = segmentData;
            inSelectedCurr.selectedCurrency =currencyService.getSelectedCurrency();
            inSelectedCurr.totalForJourney = flightPricing.total ;
            inSelectedCurr.totalPrice = flightPricing.total.price ;
            inSelectedCurr.totalIbeFareDiscount = flightPricing.total.discount.amount;
            inSelectedCurr.totalServiceTax = flightPricing.serviceTaxAmountForSegmentFares;
            inSelectedCurr.serviceTaxForAdminFee = flightPricing.serviceTaxAmountForAdminFee;
            inSelectedCurr.pax={};
            inSelectedCurr.pax.adult = searchParam.adult;
            inSelectedCurr.pax.child = searchParam.child;
            inSelectedCurr.pax.infant = searchParam.infant;
            inSelectedCurr.promoInfo = flightPricing.promoInfo;
            inSelectedCurr.extrasAndFlightTotal = parseFloat(inSelectedCurr.selectedCurrency)+parseFloat(inSelectedCurr.extraCost);
            inSelectedCurr.journeyType = (searchParam.retDate=='N')?"oneway":"return";
            inSelectedCurr.sectorsData = [];

            for(var i=0;i<sectorsData.length;i++){
                sectorsData[i].selectedFlight.fare.fareType=getFareClassName(sectorsData[i].fareClasses,sectorsData[i].selectedFlight.fare.fareTypeId);
                var selectedFlight = sectorsData[i].selectedFlight.flight;
                selectedFlight.originName = commonLocalStorageService.getAirportCityName(selectedFlight.originCode, false);
                selectedFlight.destinationName = commonLocalStorageService.getAirportCityName(selectedFlight.destinationCode, false);
                var depTime = selectedFlight.departureDate;
                var arrTime = selectedFlight.arrivalDateFirstSeg;
                if(applicationService.getIsPersianCalendar()){
                          //alert("="+dateTransformationService.toJalaali(2016, 4, 11).jy);
                          // 31/10/2017 
                          var arrivalDateArray = new Array();
                          var departureDateArray = new Array();
                          arrivalDateArray = arrTime.split("/");
                          departureDateArray = depTime.split("/");
                          var jalaaliObject1 = dateTransformationService.toJalaali(parseInt(arrivalDateArray[2]),parseInt(arrivalDateArray[1]),parseInt(arrivalDateArray[0]));
                          var jalaaliObject2 = dateTransformationService.toJalaali(parseInt(departureDateArray[2]),parseInt(departureDateArray[1]),parseInt(departureDateArray[0]));
                          arrTime = jalaaliObject1.jd+"/"+jalaaliObject1.jm+"/"+jalaaliObject1.jy;
                          depTime = jalaaliObject2.jd+"/"+jalaaliObject2.jm+"/"+jalaaliObject2.jy;
                         
                           depTime = depTime+" "+selectedFlight.departureTime;
                            arrTime = arrTime+" "+selectedFlight.arrivalTimeFirstSeg;
                        }else{
                            depTime = selectedFlight.departureDate+" "+selectedFlight.departureTime;
                            arrTime = selectedFlight.arrivalDateFirstSeg+" "+selectedFlight.arrivalTimeFirstSeg;
                        }
                selectedFlight.departureDateTime=moment(depTime, "DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm");
                selectedFlight.arrivalDateTime = moment(arrTime, "DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm");
                if(selectedFlight.stops.length){
                    for(var j=0;j<selectedFlight.stops.length;j++){
                        var stopData= selectedFlight.stops[j];
                        /*if(j==0){
                            selectedFlight.destination = stopData.originCode;
                            selectedFlight.destinationName = commonLocalStorageService.getAirportName(stopData.originCode);
                            inSelectedCurr.sectorsData.push(selectedFlight);
                        }
*/
                        stopData.originName = commonLocalStorageService.getAirportCityName(stopData.originCode, false);
                        stopData.destinationName = commonLocalStorageService.getAirportCityName(stopData.destinationCode, false);
                        var dep_time = stopData.departureDate+" "+stopData.departureTime;
                        var arrTime = stopData.arrivalDate+" "+stopData.arrivalTime;
                        stopData.departureDateTime=moment(dep_time, "DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm");
                        stopData.arrivalDateTime = moment(arrTime, "DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm");
                        stopData.flightNumber = stopData.flightNumber;

                    }
                }

                //inSelectedCurr.sectorsData.push(secData);
            }
            inSelectedCurr.sectorsData= sectorsData;
            inSelectedCurr.selectedCurrencyType = currencyService.getSelectedCurrency();
            inSelectedCurr.administrationFee = 0 ;
            
            quoteDetails = inSelectedCurr;
            //quoteService.setQuoteStatus(true);
            busyLoaderService.setHideBusyLoader();

            commonLocalStorageService.setData('QUOTE_DETAILS', quoteDetails);
            manageAvailableFareClasses(fareQuoteData,sectorsData);
            var ondInfoSelection = [];
            for(var i = 0; i < fareQuoteData.originDestinationResponse.length; i++){
            	var ond = fareQuoteData.originDestinationResponse[i];
            	var obj = {ondSequence: ond.ondSequence, availableClassSelected:{}, selectedFareClass: {}, segmentRPH: []};
            	bp: for( var j = 0; j< ond.availableOptions.length; j++){
            		if(ond.availableOptions[j].selected){
            			for (var l=0;l<ond.availableOptions[j].segments.length;l++){
            				obj.segmentRPH.push(ond.availableOptions[j].segments[l].flightSegmentRPH);
            			}
	            		for (var k =0; k < ond.availableOptions[j].availableFareClasses.length; k++){
	            			if(ond.availableOptions[j].availableFareClasses[k].selected){
	            				obj.availableClassSelected = ond.availableOptions[j].availableFareClasses[k];
	            				obj.selectedFareClass = getFareBundle( ond.availableOptions[j].availableFareClasses[k].fareClassCode);
	            				break bp;
	            			}
	            		}
            		}
            	}
            	
            	ondInfoSelection[ondInfoSelection.length] = obj;
            }
            
            function getFareBundle( fareClassCode){
            	for(var i =0 ; i < fareQuoteData.fareClasses.length; i++){
            		if(fareQuoteData.fareClasses[i].fareClassCode == fareClassCode){
            			return fareQuoteData.fareClasses[i];
            		}
            	}
            	return {};
            }
            console.log(ondInfoSelection);
            commonLocalStorageService.setData('OND_FARE_SELECTION', ondInfoSelection);
            //commonLocalStorageService.setData('AVAILABLE_FARE_CLASSES',fareQuoteData.originDestinationResponse);
            return quoteDetails
        };
        function manageAvailableFareClasses(fare,sectorsData){
                var availableFareClasses = [];
                mapFareFeeToClass(fare.fareClasses,sectorsData);
                    for(var k=0;k<sectorsData.length;k++){
                        var temp=[];
                        var availableFares  = sectorsData[k].flightInfo.fareTypeInfo;
                        if(availableFares.length >5 ){
                            availableFares = availableFares.slice(0,5);
                        }
                        else{
                            availableFares = availableFares;
                        }

                        for(var j = 0 ;j<availableFares.length;j++){
                            var selectedFlightFareFee = getFareFee(fare.fareClasses,sectorsData[k].selectedFlight.fare.fareTypeId);
                            if(sectorsData[k].selectedFlight.fare.fareTypeId != availableFares[j].fareClassCode  && selectedFlightFareFee<availableFares[j].additionalFareFee){
                                temp.push(availableFares[j]);
                            }
                        }
                        availableFareClasses.push(temp);
                    }
                commonLocalStorageService.setData('AVAILABLE_FARE_CLASSES',availableFareClasses);

        }
        function mapFareFeeToClass(fareClasses,sectorsData){
            var temp=[];
            for(var k=0;k<sectorsData.length;k++){
                var availableFares  = sectorsData[k].flightInfo.fareTypeInfo;    //fareClasses[i].availableOptions[0].availableFareClasses;
                for(var i=0;i<availableFares.length;i++){
                    availableFares[i].additionalFareFee = getFareFee(fareClasses,availableFares[i].fareTypeId)
                }
            }


        }
        function getFareFee(fareClasses,id){
            var value=null;
            for(var i=0;i<fareClasses.length;i++){
                if(fareClasses[i].fareClassCode === id){
                    if(fareClasses[i].freeServices){
                        value = fareClasses[i].additionalFareFee;
                    }
                    return value;
                }
            }
        }

        this.setReturnFareRequestData = function(sectorsData,departureDate,t){
            ;
            var temp ={};
            var searchParam = localStorageService.get('SEARCH_CRITERIA');
            var sectorData = sectorsData;
            temp.journeyInfo=[];
            temp.travellerQuantity={}
            temp.preferences={};
            temp.travellerQuantity.adultCount=searchParam.adult;
            temp.travellerQuantity.childCount=searchParam.child;
            temp.travellerQuantity.infantCount=searchParam.infant;
            temp.preferences.cabinClass=searchParam.cabin;
            temp.preferences.logicalCabinClass="Y";
            temp.preferences.currency=currencyService.getAppBaseCurrency();

            temp.preferences.promotion ={};
            temp.preferences.promotion.type='';
            temp.preferences.promotion.code='';
            temp.promoCode = "";
            if(searchParam.promoCode){
                temp.preferences.promotion.type='PROMO_CODE';
                temp.preferences.promotion.code=searchParam.promoCode;
                temp.promoCode = searchParam.promoCode;
            }

            for(var i=0;i<sectorData.length;i++){
                var secData = {}
                var selectedFlight = sectorData[i].selectedFlight && sectorData[i].selectedFlight.flight;
                var ondInfo = sectorData[i].ondInfo;
                secData.origin=ondInfo.origin.toUpperCase();
                secData.originCity=(ondInfo.originCity == 'Y' ? true : false);
                secData.destination=ondInfo.destination.toUpperCase();
                secData.destinationCity=(ondInfo.destCity == 'Y' ? true : false);

                secData.departureDateTime = ondInfo.departureDate;
                if(i === 0){
                    if(t){
                        secData.departureVariance=3;
                    }
                    else {
                        secData.departureVariance = 0;
                        if (constantFactory.AppConfigStringValue("showHalfReturnFaresInCalendar") != "true" && selectedFlight) {
                            var depDateTime = selectedFlight.departureDate + " " + selectedFlight.departureTime;
                            var arrTime = selectedFlight.arrivalDate + " " + selectedFlight.arrivalTime;
                        	var flightTemp = {};
                        	flightTemp.flightDesignator = selectedFlight.flightNumber;
                        	flightTemp.segmentCode = selectedFlight.originCode + "/" + selectedFlight.destinationCode;
                        	flightTemp.departureDateTime = moment(depDateTime, "DD-MM-YYYY HH:mm").format("YYYY-MM-DDTHH:mm:ss");
                        	flightTemp.flightSegmentRPH = selectedFlight.flightRPH;
                        	flightTemp.routeRefNumber = selectedFlight.routeRefNumber;
                        	secData.specificFlights=[];
                        	secData.departureDateTime = flightTemp.departureDateTime // set departure date time of ond to specific flight departure date
                        	secData.arrivalDateTime = moment(arrTime, "DD/MM/YYYY HH:mm").format("YYYY-MM-DDTHH:mm:ss");
                        	secData.specificFlights.push(flightTemp);
                        	if (selectedFlight.stops.length) {
                        		for (var j = 0; j < selectedFlight.stops.length; j++) {
                        			var stopData = selectedFlight.stops[j];
                        			var fTemp = {};
                        			fTemp.flightDesignator = stopData.flightNumber;
                        			fTemp.segmentCode = stopData.originCode + "/" + stopData.destinationCode;
                        			var dep_time = stopData.departureDate + " " + stopData.departureTime;
                        			fTemp.departureDateTime = moment(dep_time, "DD/MM/YYYY HH:mm").format("YYYY-MM-DDTHH:mm:ss");
                        			fTemp.flightSegmentRPH = stopData.flightRPH;
                        			secData.specificFlights.push(fTemp);
                        		}
                        		secData.specificFlights[0].segmentCode = selectedFlight.originCode + "/" + selectedFlight.stops[0].originCode;
                        	}

                        	secData.preferences = {};
                        	secData.preferences.additionalPreferences = {};
                        	// fix for issue which return discount gets wrong due to ob bundle fee includes in ob promo fare
                        	// secData.preferences.additionalPreferences.fareClassType = getFareClassType(sectorData[i].fareClasses, sectorData[i].selectedFlight.fare.fareTypeId);
                        	//secData.preferences.additionalPreferences.fareClassId = getFareClassID(sectorData[i].fareClasses, sectorData[i].selectedFlight.fare.fareTypeId);

                        }
                     }
                }
                else{
                    if(departureDate){
                        secData.departureDateTime = departureDate;
                        secData.departureVariance=0;
                    }
                    else{
                        secData.departureVariance=3;
                    }

                }

                temp.journeyInfo.push(secData);
            }


            return temp;

        };
        this.setFareQuoteReqData= function(sectorsData){
            ;
            var temp ={};
            var searchParam = localStorageService.get('SEARCH_CRITERIA');
            var sectorData = sectorsData;
            temp.journeyInfo=[];
            temp.travellerQuantity={}
            temp.preferences={};
            if(searchParam.adult){
                temp.travellerQuantity.adultCount=searchParam.adult;
            }
            if(searchParam.child){
                temp.travellerQuantity.childCount=searchParam.child;
            }
            if(searchParam.infant){
                temp.travellerQuantity.infantCount=searchParam.infant;
            }
            temp.preferences.cabinClass=searchParam.cabin;
            temp.preferences.logicalCabinClass=searchParam.cabin;
            temp.preferences.currency=currencyService.getAppBaseCurrency();
            temp.preferences.originCountryCode=commonLocalStorageService.getData('CARRIER_CODE');
            temp.preferences.promotion ={};
            temp.preferences.promotion.type='';
            temp.preferences.promotion.code='';

            if(searchParam.promoCode){
                temp.preferences.promotion.type='PROMO_CODE';
                temp.preferences.promotion.code=searchParam.promoCode;
            }

            for(var i=0;i<sectorData.length;i++){
                var secData = {}
                var selectedFlight = sectorData[i].selectedFlight.flight;
                this.setUpdatedSectorsOndInfo(selectedFlight, i);
                secData.origin=selectedFlight.originCode;
                secData.originCity = false;
                secData.destination=selectedFlight.destinationCode;
                secData.destinationCity = false;
                var depTime = selectedFlight.departureDate+" "+selectedFlight.departureTime;
                secData.departureDateTime=moment(depTime, "DD/MM/YYYY HH:mm").format("YYYY-MM-DDTHH:mm:ss");
                var arrTime = selectedFlight.arrivalDate+" "+selectedFlight.arrivalTime;
                secData.arrivalDateTime=moment(arrTime, "DD/MM/YYYY HH:mm").format("YYYY-MM-DDTHH:mm:ss");
                secData.departureVariance=0;
                secData.specificFlights=[];
                var flightTemp={};
                flightTemp.filghtDesignator=selectedFlight.destinationCode;
                flightTemp.segmentCode=selectedFlight.originCode+"/"+selectedFlight.destinationCode;
                flightTemp.departureDateTime=secData.departureDateTime;
                flightTemp.flightSegmentRPH=selectedFlight.flightRPH;
                flightTemp.routeRefNumber = selectedFlight.routeRefNumber;
                secData.specificFlights.push(flightTemp);
                if(selectedFlight.stops.length){
                    for(var j=0;j<selectedFlight.stops.length;j++){
                        var stopData= selectedFlight.stops[j];
                        var fTemp={};
                        fTemp.filghtDesignator=stopData.destinationCode;
                        fTemp.segmentCode=stopData.originCode+"/"+stopData.destinationCode;
                        var dep_time = stopData.departureDate+" "+stopData.departureTime;
                        fTemp.departureDateTime=moment(dep_time, "DD/MM/YYYY HH:mm").format("YYYY-MM-DDTHH:mm:ss");
                        fTemp.flightSegmentRPH=stopData.flightRPH;
                        fTemp.routeRefNumber = stopData.routeRefNumber;
                        secData.specificFlights.push(fTemp);
                    }
                }
                secData.preferences={};
                secData.preferences.additionalPreferences={};
                secData.preferences.additionalPreferences.fareClassType=getFareClassType(sectorData[i].fareClasses,sectorData[i].selectedFlight.fare.fareTypeId);
                secData.preferences.additionalPreferences.fareClassId=getFareClassID(sectorData[i].fareClasses,sectorData[i].selectedFlight.fare.fareTypeId);

                temp.journeyInfo.push(secData);
            }
            return temp
        };

        function getFareClassType(fareclasses,id){
            for(var i=0;i<fareclasses.length;i++){
                if(fareclasses[i].fareTypeId == id){
                    return fareclasses[i].fareClassType;
                    break;
                }
            }
        };
        function getFareClassName(fareclasses,id){
            for(var i=0;i<fareclasses.length;i++){
                if(fareclasses[i].fareTypeId == id){
                    return fareclasses[i].fareTypeName;
                    break;
                 }
                }
         };
        function getFareClassID(fareclasses,id){
            for(var i=0;i<fareclasses.length;i++){
                if(fareclasses[i].fareTypeId == id){
                    return fareclasses[i].fareClassId;
                    break;
                }
            }
        };
        this.setQuoteDetails = function (data) {
            ;
            commonLocalStorageService.setData('QUOTE',data);
        };

        this.setSectorsDetails = function(data){
            sectorsFareData.noOfSectors = data.noOfSectors;
            sectorsFareData.sectors = data.sectors;
        };
        this.getSectorsDetails = function(){
            return sectorsFareData;
        }

        this.getQuoteDetails = function () {
            if(commonLocalStorageService.getData('QUOTE_DETAILS')) {

                return commonLocalStorageService.getData('QUOTE_DETAILS')
            }else if(commonLocalStorageService.getData('QUOTE')) {
                return commonLocalStorageService.getData('QUOTE')
            }else{
                return {};
            }
        };
        this.getBusyLoaderStatus = function(){
            return drawerBusyLoaderShow;
        };
        this.setBusyLoaderStatus = function(value){
            drawerBusyLoaderShow = value;
        };
    }];
    return service;
}