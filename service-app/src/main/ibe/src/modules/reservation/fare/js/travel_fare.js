/**
 * Created by Baladewa on 11/11/15.
 */
var travelFareController = function () {
    var controllers = {};
    var selectedFlightData = {};
    controllers.travelFareController = ["remoteStorage","stepProgressService", "travelFareService", "loadingService", "$timeout", "commonLocalStorageService", "quoteService", "availabilityRemoteService", "localStorageService",
        "currencyService", "IBEDataConsole", "alertService", "busyLoaderService", "applicationService", "languageService", "$location", "eventListeners", "popupService", "constantFactory", "ancillaryService",
        'ancillaryRemoteService', '$state', "masterRemoteService", "$sce", 'GTMService', "$translate", "configsService","dateTransformationService","$rootScope","$anchorScroll","$window",
        function (remoteStorage, stepProgressService, travelFareService, loadingService, $timeout, commonLocalStorageService,
                  quoteService, availabilityRemoteService, localStorageService, currencyService, IBEDataConsole,
                  alertService, busyLoaderService, applicationService, languageService, $location, eventListeners,
                  popupService, constantFactory, ancillaryService, ancillaryRemoteService, $state, masterRemoteService,
                $sce, GTMService, $translate, configsService,dateTransformationService,$rootScope,$anchorScroll,$window) {
            
            if(!$rootScope.deviceType.isMobileDevice){
                $rootScope.removePaddingFromXSContainerClass=true;
            }
            else{
                $rootScope.removePaddingFromXSContainerClass= false;
            }
            var self = this, selectedReturnDate;
            var journeyType = travelFareService.getSearchCriteria()['journeyType'],
                    nextPreviousRequiseDate = "";
            var depDate, arrDate ;
            var firstSegmentFirstClick = false;
            self.fareStepProgressStatus;
            self.fareRulesDescriptions = [];
            self.paymentOptions = [];
            self.loadingMessageForFareQuote = "msg_fare_loadingMsgForFareQuote";
            self.curencyTypes = commonLocalStorageService.getCurrencies();
            self.airlineMap = null;
            self.temp =null;
            self.selectedLanguage=languageService.getSelectedLanguage();
            if(configsService.clientConfigs && configsService.clientConfigs.airlines) {
                self.airlineMap = configsService.clientConfigs.airlines;
            }

            self.getSelectedCurrencyType = function () {
                //FIXME get this logic in to currency service do not hardcode "AED"
                return currencyService.getSelectedCurrency() || "AED";
            };
            self.defaultCurrencyType = currencyService.getSelectedCurrency() || "AED";
            self.baseCurrency = currencyService.getAppBaseCurrency() || "AED";
            self.isBlockFare = false;
            self.airportMessages = []; // Array of messages specific to the selected flight
            self.hidePaymentOptions = false;

            self.showModifySearch = function () {
                eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
            };

            self.updatedDates = [];

            // Used to sanitize html recived as messages from backend
            var language = languageService.getSelectedLanguage();
            var airportList = remoteStorage.getAirports();
            self.trust = $sce.trustAsHtml;
            self.airports = _.map(airportList,function(item){
                if(item[language] != undefined){
                    return {name:item[language],code:item.code}
                }else{
                    return {name:item.en,code:item.code}
                }
            })
            console.log(self.airports);

            this.getAirportName = function(code){
                var selected = _.where(self.airports,{"code":code});
                if(selected != undefined){
                    return selected[0].name;
                }
            }

            // Check app parameter to enable/disable checkbox next proceed to passenger button
            self.putCheckboxToAcceptTerms = constantFactory.AppConfigValue("putCheckboxToAcceptTerms");
            // Fare bundle description images are shown in tooltip only if "showFareBundleDescription" is enabled
            self.showFareBundleDescription = constantFactory.AppConfigValue("showFareBundleDescription");
            // This will call after date selected on the time line
            this.afterSelected = function(selectedDate, dayObj, selectedSequence) {
                resetUpdatedFareValues();
                self.updatedDates[selectedSequence] = selectedDate.replace(/\//g, '-');
                self.quoteDetailsSummary = '';
                getSelectedDateFlightsFares(selectedDate, dayObj, selectedSequence);
                self.sectorsData.sectors[selectedSequence].selectedFlight = null;
            };

            this.setNextPreviousData = function (setData, direction, secquenceNo) {
                /**
                 * setData: callback function to set the data for the particular day
                 * direction: where it was navigated 'NEXT' or 'PREV'
                 */
                callNextPrevTimeLineData(secquenceNo, setData, direction);
            };

            this.onSelectFare = function (selectedFlight, selectedFare, sequence) {
                var flightData = {
                    flight: selectedFlight,
                    fare: selectedFare
                };
                if (journeyType == "return" && sequence == 0) {
                    this.popupCaption = "Continue To Select Return Flight";
                } else {
                    this.popupCaption = "Continue"
                }
                selectedFlightData = flightData;

                // Setting the outboundFlight object in the commonLocalStorage
                self.sectorsData.sectors[sequence].selectedFlight = selectedFlightData;
                // Setting the outboundFlight object in the commonLocalStorage
                if (sequence == 0) {
                    commonLocalStorageService.setOutboundTripInfo(selectedFlightData);
                }
            };

            // This will call before Next Previous navigation
            // Can be use to validate before navigate and return false to avoid the
            // navigation
            this.validate = function (date, direction, secquenceNo) {
                /**
                 * date: date to search or to navigate if direction is equals to 'NEXT' +1
                 * date of last date of the time line direction: where it will navigates
                 * 'NEXT' or 'PREV'
                 */
               
                var currentDate = moment().format("YYYY-MM-DD");
                var expectDate = moment(date, "DD/MM/YYYY").format("YYYY-MM-DD");

                if (moment(currentDate).isSame(expectDate) || moment(currentDate).isBefore(expectDate)) {
                    nextPreviousRequiseDate = date;
                    return true; // return true of the validation success
                } else {
                    return false;
                }

            };
            
            /**
             * Calling for timeline data request
             *
             * @param secSequence
             */
            var callReturnFares = function (secSequence) {
                self.sectorsData.sectors[secSequence].showBusyLoader = true;
                self.customLoadingMessage = "Please Wait Inbound..."

                var params = travelFareService.setReturnFareRequestData(self.sectorsData.sectors, '', '');
                self.isBlockFare = true;
                availabilityRemoteService.retrieveReturnFareTimeline(params, function(fareResponse) {
                    var dayObj;
                    var timelndata = {
                        'data': fareResponse.timelineInfo
                    };
                    _.each(timelndata.data, function (item) {
                        item.selected = (item.date == selectedReturnDate);
                        if (item.selected) {
                            dayObj = item;
                        }
                    })
                    self.sectorsData.sectors[secSequence].timeLineData = timelndata;
                    self.sectorsData.sectors[secSequence].fareClasses = fareResponse.fareClasses;
                    self.isBlockFare = false;
                    loadingService.hideLoading();
                    self.sectorsData.sectors[secSequence].showBusyLoader = false;
                    if (secSequence === 1) {
                        self.afterSelected(selectedReturnDate, dayObj, 1);
                    }

                }, function(results) {
                    self.sectorsData.sectors[secSequence].showBusyLoader = false;
                    self.isBlockFare = false;
                    popupService.alert(results.error);

                });
            };

            function resetUpdatedFareValues() {
                _.forEach(self.selectedFlightFares, function (fareClasses) {
                    _.forEach(fareClasses, function (fareClass) {
                        fareClass.updatedFareValue = null;
                    });
                });
            }

            this.onConfirmFare = function (selectedFlightFares, secNo) {
                
                resetUpdatedFareValues();

                self.selectedFlightFares = selectedFlightFares;

                commonLocalStorageService.remove("ANCILLARY");

                // Remove previous ancillary data on flight select
                commonLocalStorageService.remove("ANCILLARY");
                ancillaryService.clearAncillaryModel();
                quoteService.setExtraTotal(0);
                quoteService.setTotalWithAdminFee('');
                ancillaryRemoteService.clearExtras();
                quoteService.setServiceTaxForAdminFee(0);

                // Get airport messages associated with the currently selected fares
                getAirportMessages(secNo);
                secNo == 1 ? focusSummaryDetails(secNo): callScroll(secNo);
                // clearSelectedFlight(secNo)
                // alertService.setAlert(error.error, 'danger');
                $('.summary-wrap').removeClass('summary-content');
                if ((constantFactory.AppConfigStringValue("disableIBCalendarRefresh") != "true") &&
                    (journeyType == 'return' && secNo < 1)) {
                    callReturnFares(1);
                    self.quoteDetailsSummary = '';
                    clearSelectedFlight(secNo + 1);
                    renderSectors(secNo);
                } else if (isValidSelection(secNo, selectedFlightFares)) {
                    quoteService.setShowQuoteStatus(false);
                    quoteService.setShowSummaryDrawer(false);
                    busyLoaderService.setShowBusyLoader(true);
                    var params = travelFareService.setFareQuoteReqData(self.sectorsData.sectors);
                    self.quoteDetailsSummary = '';
                    $('.summary-wrap').addClass('summary-content');
                    self.isFareQuoteBusyLoaderVisible = true;
                    self.isBlockFare = true;
                    $timeout(function () {
                        availabilityRemoteService.retrieveFareQuoteData(params, function (fareQuuoteResponse) {
                        	   
                                self.quoteDetailsSummary = travelFareService.fareQuoteDataMapping(fareQuuoteResponse, self.sectorsData.sectors);
                                quoteService.setServiceTaxTotal(self.quoteDetailsSummary.totalServiceTax);
                                quoteService.setServiceTaxForAdminFee(fareQuuoteResponse.selectedFlightPricing.serviceTaxAmountForAdminFee);
                                quoteService.changeShowQuote(false);
                                self.isFareQuoteBusyLoaderVisible = false;
                                self.fareSelectCompleted = true;
                                self.isBlockFare = false;
                                self.paymentOptions = fareQuuoteResponse.paymentOptions;
                                self.administrationFee = fareQuuoteResponse.selectedFlightPricing.administrationFee;
                                $rootScope.adminFee=self.administrationFee;
                                commonLocalStorageService.setData('ADMINFEE', self.administrationFee);
                                _.each(fareQuuoteResponse.selectedFlightPricing.fareRules, function (val) {
                                    if (!!val.comment) {
                                        self.fareRulesDescriptions.push(val.comment)
                                    }
                                })
                                applicationService.setFareRules(self.fareRulesDescriptions);
                                focusSummaryDetails(secNo);

                            updateSelectionBasedFareValues(fareQuuoteResponse);
                            self.quoteDetailsSummary = travelFareService.fareQuoteDataMapping(fareQuuoteResponse, self.sectorsData.sectors);
                            quoteService.setServiceTaxTotal(self.quoteDetailsSummary.totalServiceTax);
                            quoteService.changeShowQuote(false);
                            self.isFareQuoteBusyLoaderVisible = false;
                            self.fareSelectCompleted = true;
                            self.isBlockFare = false;
                            self.paymentOptions = fareQuuoteResponse.paymentOptions;

                            function updateSelectionBasedFareValues(fareQuuoteResponse) {
                                if (constantFactory.AppConfigStringValue("disableIBCalendarRefresh") == "true") {
                                    var pricesOfQuotedValues = _.pluck(fareQuuoteResponse.originDestinationResponse,
                                        'availableOptions');
                                    _.forEach(pricesOfQuotedValues, function (flightFares, index) {
                                        var self = {selectedFlightFares: this.selectedFlightFares[index]};
                                        _.forEach(_.first(_.pluck(flightFares, 'availableFareClasses')), function (fareClass) {
                                            var quotedFareClass = _.findWhere(this.selectedFlightFares,
                                                {fareTypeId: fareClass.fareClassCode});
                                            if (quotedFareClass) {
                                                quotedFareClass.updatedFareValue = fareClass.price;
                                            }
                                        }, self);
                                    }, self);
                                }
                            }

                        }, function (error) {
                            //alertService.setPageAlertMessage('Error');
                            self.isFareQuoteBusyLoaderVisible = false;
                            self.isBlockFare = false;
                            clearSelectedFlight(secNo)
                            alertService.setAlert(error.error, 'danger');
                            $('.summary-wrap').removeClass('summary-content');
                        });
                    }, 100);
                }
               
                travelFareService.setConfirmedFlights(selectedFlightData, secNo);
            };
            function callScroll(secNo) {
            //do nothing handled at on confirm fare method
            var element = $("#section-"+(secNo+1))
                if(element.offset() && firstSegmentFirstClick != true){   
                    var heightScroll = element.offset().top-100;
                    $("html, body").animate({ scrollTop: heightScroll }, 1200);
                    self.quoteDetailsSummary = '';
                }
                firstSegmentFirstClick = true;
                
            }

            var focusSummaryDetails = function (secNo) {

                    var element = $("#travelfare-summary-bottom").offset().top;

                    $("html, body").animate({
                        scrollTop: element
                    }, 1200);
                    
            }
            function getAirportMessages(secNo) {
                // Setting up data for calling airportMessage
                var airportMessageParams = {
                    "stage": "SEARCH_RESULT",
                    "languageCode": languageService.getSelectedLanguage(),
                    "flightSegInfo": []
                };

                // Setting up array of sectors for airport messages
                angular.forEach(self.sectorsData.sectors, function (sector, index) {
                    if (!_.isEmpty(sector.selectedFlight)) {
                        var currentFlight = self.sectorsData.sectors[index].selectedFlight.flight;
                        var arrivalString = currentFlight.arrivalDate + " " + currentFlight.arrivalTime;
                         var departureString = currentFlight.departureDate ;
                        if(applicationService.getIsPersianCalendar()){
                          //alert("="+dateTransformationService.toJalaali(2016, 4, 11).jy);
                          // 31/10/2017 
                          var arrivalDateArray = new Array();
                          var departureDateArray = new Array();
                          arrivalDateArray = arrivalString.split("/");
                          departureDateArray = departureString.split("/");
                          var jalaaliObject1 = dateTransformationService.toJalaali(parseInt(arrivalDateArray[2]),parseInt(arrivalDateArray[1]),parseInt(arrivalDateArray[0]));
                          var jalaaliObject2 = dateTransformationService.toJalaali(parseInt(departureDateArray[2]),parseInt(departureDateArray[1]),parseInt(departureDateArray[0]));
                          arrivalString = jalaaliObject1.jd+"/"+jalaaliObject1.jm+"/"+jalaaliObject1.jy;
                          departureString = jalaaliObject2.jd+"/"+jalaaliObject2.jm+"/"+jalaaliObject2.jy;
                          //alert("arrival: "+arrivalString+" departure : "+departureString);  
                          arrivalString = arrivalString + " " + currentFlight.arrivalTime;
                           departureString = departureString + " " + currentFlight.departureTime;
                           self.temp = arrivalString;
                           
                        }else{
                           arrivalString = currentFlight.arrivalDate + " " + currentFlight.arrivalTime;
                            departureString = currentFlight.departureDate + " " + currentFlight.departureTime;
                        }
                      
                        arrivalString = moment.utc(arrivalString, "DD/MM/YYYY HH:mm").toISOString();

                        var departureString = currentFlight.departureDate + " " + currentFlight.departureTime;
                        departureString = moment.utc(departureString, "DD/MM/YYYY HH:mm").toISOString();
                        
                        if (currentFlight.stops.length > 0){
                        	var stop = currentFlight.stops[0];                        	
                        	var stoparrivalString = stop.arrivalDate + " " + stop.arrivalTime;
                        	stoparrivalString = moment.utc(stoparrivalString, "DD/MM/YYYY HH:mm").toISOString();
                        	
                        	var stopdepartureString = stop.departureDate + " " + stop.departureTime;
                        	stopdepartureString = moment.utc(stopdepartureString, "DD/MM/YYYY HH:mm").toISOString();
                        	if (index <= secNo) {
	                        	airportMessageParams.flightSegInfo.push(
		                                {
		                                    "returnFlag": index > 0,
		                                    "origin": currentFlight.originCode,
		                                    "destination": stop.originCode,
		                                    "departureDateTimeZulu": departureString,
		                                    "arrivalDateTimeZulu": stopdepartureString,
		                                    "filghtDesignator": currentFlight.flightNumber
		                                }
		                            );
                        	}
                        	for(var i=0;i< currentFlight.stops.length;i++){
                                stop  = currentFlight.stops[i];
                                stoparrivalString = stop.arrivalDate + " " + stop.arrivalTime;
                                stoparrivalString = moment.utc(stoparrivalString, "DD/MM/YYYY HH:mm").toISOString();
    	
    	                        stopdepartureString = stop.departureDate + " " + stop.departureTime;
    	                        stopdepartureString = moment.utc(stopdepartureString, "DD/MM/YYYY HH:mm").toISOString();
    	                        if (index <= secNo) {
    	                        	airportMessageParams.flightSegInfo.push(
        		                                {
        		                                    "returnFlag": index > 0,
        		                                    "origin": stop.originCode,
        		                                    "destination": stop.destinationCode,
        		                                    "departureDateTimeZulu": stopdepartureString,
        		                                    "arrivalDateTimeZulu": stoparrivalString,
        		                                    "filghtDesignator": stop.flightNumber
        		                                }
    	                        		); 
    	                        }
                           }
                        } else {	
	                        if (index <= secNo) {
	                            airportMessageParams.flightSegInfo.push(
	                                {
	                                    "returnFlag": index > 0,
	                                    "origin": currentFlight.originCode,
	                                    "destination": currentFlight.destinationCode,
	                                    "departureDateTimeZulu": departureString,
	                                    "arrivalDateTimeZulu": arrivalString,
	                                    "filghtDesignator": currentFlight.flightNumber
	                                }
	                            );
	                        }
	                    }
                    }
                });

                // Calling remote service to retrive the airport messages
                masterRemoteService.retrieveAirportMessages(airportMessageParams).then(
                    function (response) {
                        if (response.data.success) {
                            self.airportMessages = _.map(response.data.airportMessages,
                                function (message) {
                                    return message.airportMessage;
                                }
                            ).filter(function(message){
                                return message;
                            })
                        }
                    }
                );
            }

            function isValidSelection(secNo, selectedFlightFares) {
                var isValid = true;
                if (journeyType == 'return') {

                    if (!(self.sectorsData.sectors[0].selectedFlight && self.sectorsData.sectors[1].selectedFlight)) {
                        return false;
                    }

                    if ((constantFactory.AppConfigStringValue("disableIBCalendarRefresh") == "true" &&
                        _.size(selectedFlightFares) == 2) || secNo == 1) {

                        var secInfo = self.sectorsData.sectors;
                        var minTransitTime = constantFactory.AppConfigStringValue("minimum transit time in millis");
                        var outbLastArrivalZulu = moment(self.sectorsData.sectors[0].selectedFlight.flight.arrivalDateFirstSeg + " " + self.sectorsData.sectors[0].selectedFlight.flight.arrivalTimeFirstSeg, 'DD/MM/YYYY HH:mm');
                        var inFirstDepatureZulu = moment(self.sectorsData.sectors[1].selectedFlight.flight.departureDate + " " + self.sectorsData.sectors[1].selectedFlight.flight.departureTime, 'DD/MM/YYYY HH:mm');


                        if ((outbLastArrivalZulu.valueOf() + parseInt(minTransitTime)) >= inFirstDepatureZulu.valueOf()) {
                            isValid = false;
                            var error = "The Return Flight selected does not meet the minimum transfer time required. Please select again.";
                            alertService.setAlert(error, 'danger');

                        } else if (!(moment(inFirstDepatureZulu).isAfter(outbLastArrivalZulu))) {
                            isValid = false;
                            var error = "Your return flight date cannot be prior to your departure flight date. Please select a future return flight or change the dates.";
                            alertService.setAlert(error, 'danger');
                        }
                    }
                }
                return isValid;

            }

            function clearSelectedFlight(secNo) {
                var flightOptions = self.sectorsData.sectors[secNo].flightInfo.fareFareResults;
                for (var i = 0; i < flightOptions.length; i++) {
                    var obj = flightOptions[i].flightFaresAvailability;
                    for (var j = 0; j < obj.length; j++) {
                        if (obj[j].isFlightSelect) {
                            obj[j].isFlightSelect = false;
                            break;
                        }
                    }

                }
            }
            /**
             * Calling for timeline next previous date request
             *
             * @param secSequence
             * @param requestedDate
             */
            var callNextPrevTimeLineData = function (secSequence, setData, direction) {

                var params = self.sectorsData.sectors[secSequence].ondInfo;
                loadingService.showLoading();
                params.departureVariance = 0;
                params.departureDate = moment(nextPreviousRequiseDate, "DD/MM/YYYY").format("YYYY-MM-DDTHH:mm:ss");

                if (journeyType == 'return' &&
                    (constantFactory.AppConfigStringValue("showHalfReturnFaresInCalendar") == "true" || secSequence == 1)) {
                    var param = travelFareService.setReturnFareRequestData(self.sectorsData.sectors, params.departureDate, '');
                    param.selectedSecSeq = secSequence;
                    availabilityRemoteService.retrieveReturnFareTimeline(param, function (fareResponse) {
                        var timelndata = {
                            'data': fareResponse.timelineInfo
                        };
                        self.dummyNextPrevDataBlock = timelndata.data;
                        setData(self.dummyNextPrevDataBlock[0]);
                        loadingService.hideLoading();

                    }, function (error) {
                        self.sectorsData.sectors[secSequence].showBusyLoader = false;
                        loadingService.hideLoading();

                    });
                } else {
                    availabilityRemoteService.retrieveNextPrevTimeLineData(params, function (fareResponse) {
                        var timelndata = {
                            'data': fareResponse.timelineInfo
                        };
                        //timelndata.data.date = requestedDate;
                        self.dummyNextPrevDataBlock = timelndata.data;
                        setData(self.dummyNextPrevDataBlock);

                        loadingService.hideLoading();
                    }, function (error) {
                        loadingService.hideLoading();
                    });
                }

            };
        
            
            var renderSectors = function (secNo) {
                var element = $(".waypoint_" + secNo);
                $("html, body").animate({
                    scrollTop: element.outerHeight()
                }, 1000);


            };

            var getSelectedDateFlightsFares = function (slDate, dayObj, sectorIndex) {

                // Show bust loader.
                self.sectorsData.sectors[sectorIndex].showBusyLoader = true;
                $timeout(function () {
                    self.sectorsData.sectors[sectorIndex].showBusyLoader = false;
                }, 100);

                angular.forEach(dayObj.flights, function (obj) {
                    if (obj !== undefined) {
                        //obj.flightFaresAvailability.sort(farecomparator);
                        angular.forEach(obj.flightFaresAvailability, function (fare) {
                            if (fare.isFlightSelect) {
                                fare.isFlightSelect = true;
                            } else {
                                fare.isFlightSelect = false;
                            }

                        });
                    }
                });
                var setFlights = {
                    fareFareResults: dayObj.flights,
                    fareTypeInfo: dayObj.fareClasses
                };
                self.sectorsData.sectors[sectorIndex].flightInfo = setFlights;
                _.each(self.sectorsData.sectors[sectorIndex].flightInfo['fareFareResults'],function(item){
                    item['airports'] = self.airports;
                    _.each(item['stops'],function(stop){
                    	stop['airports'] = self.airports;
                    });
                })
                console.log(self.sectorsData.sectors[sectorIndex].flightInfo['fareFareResults'])
                // When the date is changed, we set the updated dates in searchCriteria in the localStorage
                var searchCriteria = commonLocalStorageService.getSearchCriteria();
                var formattedDateString = (dayObj.date).replace(/\//g, '-');

                if (sectorIndex === 0) {
                    if (formattedDateString !== searchCriteria.depDate) {
                        searchCriteria.depDate = formattedDateString;
                        commonLocalStorageService.setSearchCriteria(searchCriteria);
                    }
                } else if (sectorIndex === 1) {
                    if (formattedDateString !== searchCriteria.retDate) {
                        searchCriteria.retDate = formattedDateString;
                        commonLocalStorageService.setSearchCriteria(searchCriteria);
                    }
                }

            };

            travelFareService.registerListeners(travelFareService.EVENTS.MODIFY_SEARCH, function () {
                // clear sector data for modify search.
                self.sectorsData.sectors = [];
                self.sectorSequenceNo = 0; // Set due to non-dirty modify search form submit caused an issue.
                self.quoteDetailsSummary = ''; // Hide drawer on modify search, if drawer is visible.
            });

            travelFareService.registerListeners(travelFareService.EVENTS.FARE_TIMELINE_FILLED, function (searchCriteria) {

                // Set transaction ID to null when firing the search request
                applicationService.setTransactionID(null);

                if (searchCriteria) {
                    self.searchCriteria = searchCriteria;
                }

                // create new sector with the sectorSequenceNo
                if (travelFareService.getSearchCriteria()['journeyType'] === 'return') {
                    for (var i = 0; i < self.searchCriteria.noOfONDs; i++) {
                        var sectorInfor = {
                            sequenceNo: self.sectorSequenceNo,
                            ondInfo: travelFareService.getSectorsInfo(i),
                            timeLineData: {},
                            flightInfo: {},
                            selectedFlight: {},
                            showBusyLoader: true,
                            busyLoaderMsg: travelFareService.getSectorsInfo(i).loadingMsg
                        };

                        self.sectorsData.sectors.push(sectorInfor);
                    }
                    loadAllOND();

                } else if (travelFareService.getSearchCriteria()['journeyType'] === 'oneway') {
                    if (self.searchCriteria.noOfONDs > self.sectorSequenceNo) {

                        var sectorInfor = {
                            sequenceNo: self.sectorSequenceNo,
                            ondInfo: travelFareService.getSectorsInfo(self.sectorSequenceNo),
                            timeLineData: {},
                            flightInfo: {},
                            selectedFlight: {},
                            showBusyLoader: true,
                            busyLoaderMsg: travelFareService.getSectorsInfo(self.sectorSequenceNo).loadingMsg
                        };

                        self.sectorsData.sectors.push(sectorInfor);

                        // Call to service for timeline data
                        callMinimumFares(self.sectorSequenceNo);

                    }
                }


            });



            var callMinimumFares = function (secSequence) {

                if (!self.sectorsData.sectors[secSequence]) {
                    return;
                }

                var params = self.sectorsData.sectors[secSequence].ondInfo;
                params.currency = currencyService.getAppBaseCurrency();
                self.sectorsData.sectors[self.sectorSequenceNo].showBusyLoader = true;
                self.sectorSequenceNo++;
                self.isBlockFare = true;

                // travelFareService.notifyListeners(travelFareService.EVENTS.FARE_TIMELINE_FILLED);
                availabilityRemoteService.retrieveFareTimeline(params, function (fareResponse) {

                    var timelndata = {
                        'data': fareResponse.timelineInfo
                    };

                    self.sectorsData.sectors[secSequence].timeLineData = timelndata;
                    self.sectorsData.sectors[secSequence].fareClasses = fareResponse.fareClasses;

                    loadingService.hidePageLoading();
                    self.sectorsData.sectors[secSequence].showBusyLoader = false;
                    self.isBlockFare = false;

                    //travelFareService.notifyListeners(travelFareService.EVENTS.FARE_TIMELINE_FILLED);

                    // Check validity of promoCode only if promoCode available.
                    if (params.promoCode !== "") {

                        var promoCodeErrorMsg = 'Promo code no longer exists.';

                        if (fareResponse.promoInfo && fareResponse.promoInfo.length > 0) {
                            angular.forEach(fareResponse.promoInfo, function(obj, i) {
                                if (params.promoCode.toUpperCase() !== obj.code.toUpperCase()) {
                                    alertService.setAlert(promoCodeErrorMsg, 'warning');
                                }
                            });
                        } else {
                            alertService.setAlert(promoCodeErrorMsg, 'warning');
                        }
                    }


                }, function (error) {
                    self.isBlockFare = false;
                    alertService.setPageAlertMessage(error);
                    loadingService.hidePageLoading();
                });

            };

            var loadAllOND = function () {
                //self.sectorsData.sectors[secSequence].showBusyLoader = true;
                self.customLoadingMessage = "Please Wait Inbound...";
                var params = travelFareService.setReturnFareRequestData(self.sectorsData.sectors, '', 1);
                self.isBlockFare = true;
                availabilityRemoteService.retrieveFareTimelineForAllOND(params, function (fareResponse) {

                    for (var i = 0; i < fareResponse.timelineInfo.length; i++) {
                        var timelndata = {
                            'data': fareResponse.timelineInfo[i].results
                        };
                        self.sectorsData.sectors[i].timeLineData = timelndata;
                        self.sectorsData.sectors[i].fareClasses = fareResponse.fareClasses;

                        $timeout(function (index) {
                            self.sectorsData.sectors[index].showBusyLoader = false;
                        }, 100, true, i);

                        self.isBlockFare = false;
                    }
                    loadingService.hideLoading();
                    loadingService.hidePageLoading();

                    if (params.promoCode !== "") {

                        var promoCodeErrorMsg = 'Promo code no longer exists.';

                        if (fareResponse.promoInfo && fareResponse.promoInfo.length > 0) {
                            angular.forEach(fareResponse.promoInfo, function(obj, i) {
                                if (params.promoCode.toUpperCase() !== obj.code.toUpperCase()) {
                                    alertService.setAlert(promoCodeErrorMsg, 'warning');
                                }
                            });
                        } else {
                            alertService.setAlert(promoCodeErrorMsg, 'warning');
                        }
                    }

                }, function (error) {
                    self.isBlockFare = false;
                    alertService.setPageAlertMessage(error);
                    loadingService.hidePageLoading();
                });
            };


            init();
            stepProgressService.setCurrentStep('fare');
            function init() {
                self.fareStepProgressStatus = stepProgressService.getStepByState('fare').status;
                self.sectorsData = travelFareService.getSectorsDetails();
                self.isFarePage = $state.is('fare');
                self.savingsAvailable = [];
                self.searchCriteria = travelFareService.getSearchCriteria();
                depDate = moment(self.searchCriteria.departureDate, "DD-MM-YYYY").format('YYYY-MM-DD');
                arrDate = moment(self.searchCriteria.returnDate, "DD-MM-YYYY").format('YYYY-MM-DD');
                selectedReturnDate = moment(self.searchCriteria.returnDate, "DD-MM-YYYY").format('DD/MM/YYYY');

                self.monthsText = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                 if ($rootScope.deviceType.isMobileDevice) {
                        self.daysText = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                    }else{
                       self.daysText = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                    }
                
                //var isMobile = $rootScope.deviceType;
                // Replace date and month texts with translations if available
                $translate.onReady().then(function () {
                    var translatedMonths = $translate.instant('arr_common_months');
                    if (translatedMonths != 'arr_common_months') {
                        self.monthsText = translatedMonths.split(';');
                    }
                    var translatedDays = '';
                    if ($rootScope.deviceType.isMobileDevice) {
                        translatedDays = $translate.instant('arr_common_days_mobile');
                    }else{
                       translatedDays =  $translate.instant('arr_common_days');
                    }

                    if (translatedDays != 'arr_common_days' && translatedDays != 'arr_common_day_mobile') {
                       self.daysText = translatedDays.split(';');
                    }

                });


                if (self.fareStepProgressStatus == 'completed' && self.sectorsData.sectors.length) {
                    self.sectorsData = travelFareService.getSectorsDetails();
                    self.quoteDetailsSummary = travelFareService.getQuoteDetails();
                    self.paymentOptions = commonLocalStorageService.getData("payment_options") || [];
                    //self.searchCriteria = travelFareService.getSearchCriteria();
                    quoteService.setShowQuoteStatus(false);
                    quoteService.setShowSummaryDrawer(false);
                    loadingService.hideLoading();
                    loadingService.hidePageLoading();

                } else {
                    var journeyType = travelFareService.getSearchCriteria()['journeyType'],
                            nextPreviousRequiseDate = "";

                    self.sectorSequenceNo = 0;
                    self.dummyNextPrevDataBlock = {};

                    self.searchCriteria = travelFareService.getSearchCriteria();
                    self.quoteDetailsSummary = '';
                    depDate = moment(self.searchCriteria.departureDate, "DD-MM-YYYY").format('YYYY-MM-DD')
                    arrDate = moment(self.searchCriteria.returnDate, "DD-MM-YYYY").format('YYYY-MM-DD')

                    quoteService.setShowSummaryDrawer(false);

                    // Set the Inbound status to set the proper title.
                    self.isInbound = travelFareService.getSearchCriteria().journeyType;

                    self.sectorsData = {
                        noOfSectors: 0,
                        sectors: []
                    };
                    travelFareService.setSectorsOndInfo(self.searchCriteria);
                }

                if (constantFactory.AppConfigValue("showPaymentIcons") === false) {
                    self.hidePaymentOptions = true;
                }
            }


            this.onContinueToPassenger = function () {

                // This functions handles timeline date changes. If user modify
                // timeline dates, this function update the local storage and timeline data
                // array accordingly.
                setUpdatedTimeLineData();

                var currency = currencyService.getSelectedCurrency(),
                    language = languageService.getSelectedLanguage();
                
                var selectedOrigin = _.where(remoteStorage.getAirports(),{"code":self.searchCriteria.origin});
                var selectedDest = _.where(remoteStorage.getAirports(),{"code":self.searchCriteria.destination});
                
                if (remoteStorage.getAirports()[0].city !== undefined) {
                    selectedOrigin = _.where(remoteStorage.getAirports(),{"code":self.searchCriteria.origin, "city":(self.searchCriteria.originCity==='Y')});
                    selectedDest = _.where(remoteStorage.getAirports(),{"code":self.searchCriteria.destination, "city":(self.searchCriteria.destCity==='Y')});
                }


                var searchCriteria = commonLocalStorageService.getSearchCriteria();
                searchCriteria.originCityAirports = selectedOrigin[0].cityAirportCodes;
                searchCriteria.destCityAirports = selectedDest[0].cityAirportCodes;
                commonLocalStorageService.setSearchCriteria(searchCriteria);
                
                commonLocalStorageService.setConfirmedFlightData(travelFareService.getConfirmedFlights());
                $location.path('/passenger/' + language + '/' + currency);

                //set extra data if searchData is set
                if (commonLocalStorageService.getConfirmedFlightData() != undefined) {
                    self.extModel = travelFareService.getSectors(); //Extra model for the summary drawer
                    var rout = []
                    angular.forEach(self.sectorsData.sectors, function (item) {
                        rout.push(item.ondInfo.originName + '-' + item.ondInfo.destinationName)
                    })
                    var routStr = rout.join('--')
                    GTMService.setGoogleTagLayer([{'Currency': currency}, {'Name': routStr}]);
                }
                travelFareService.setSectorsDetails(self.sectorsData);
                travelFareService.setQuoteDetails(self.quoteDetailsSummary);
            };


            /**
             *  This functions handles timeline date changes. If user modify
             *  timeline dates, this function update the local storage and timeline data
             *  array accordingly.
             */
            function setUpdatedTimeLineData() {

                if (self.updatedDates.length === 0) {
                    self.updatedDates = commonLocalStorageService.getData("updated_dates");
                } else {
                    commonLocalStorageService.setData("updated_dates", self.updatedDates);
                }

                if (self.paymentOptions.length === 0) {
                    self.paymentOptions = commonLocalStorageService.getData("payment_options");
                } else {
                    commonLocalStorageService.setData("payment_options", self.paymentOptions);
                }


                var searchCriteria = travelFareService.getSearchCriteria(),
                    internalSearchCriteria = commonLocalStorageService.getSearchCriteria(),
                    depDate = self.updatedDates[0],
                    retDate = "N";
                
                if(self.updatedDates.length === 2 && !(self.updatedDates[0])){
                    depDate =  searchCriteria.departureDate.replace(/\//g, '-');                	
                }

                if (searchCriteria.journeyType === 'return') {
                    retDate = searchCriteria.returnDate.replace(/\//g, '-');   
                }

                var selectedDepartureDate = depDate.replace(/-/g,'/');
                var selectedArrivalDate = retDate.replace(/-/g,'/');

                angular.forEach(self.sectorsData.sectors, function(sector, sectorSequence) {
                    var days = sector.timeLineData.data;
                    if (sectorSequence === 0) {
                        setSeletedDateOnTimeLine(days, selectedDepartureDate);
                    }
                    else if (sectorSequence === 1) {
                        setSeletedDateOnTimeLine(days, selectedArrivalDate);
                    }
                });

                // No need to update search criteria and timeline data if user did not change the dates.
                if (internalSearchCriteria.depDate === depDate && internalSearchCriteria.retDate === retDate) {
                    return;
                }

                internalSearchCriteria.depDate = depDate;
                internalSearchCriteria.retDate = retDate;

                commonLocalStorageService.setSearchCriteria(internalSearchCriteria);

                var selectedDepartureDate = depDate.replace(/-/g, '/');
                var selectedArrivalDate = retDate.replace(/-/g, '/');

                angular.forEach(self.sectorsData.sectors, function (sector, sectorSequence) {
                    var days = sector.timeLineData.data;
                    if (sectorSequence === 0) {
                        setSeletedDateOnTimeLine(days, selectedDepartureDate);
                    } else if (sectorSequence === 1) {
                        setSeletedDateOnTimeLine(days, selectedArrivalDate);
                    }
                });
            }


            /**
             *  Helper functions for setUpdatedTimeLineData. change selectedArrivalDate
             *  date in timeline data.
             */
            function setSeletedDateOnTimeLine(timelineData, selectedDate) {
                angular.forEach(timelineData, function (date, key) {
                    if (date.date === selectedDate) {
                        date.selected = true;
                    } else {
                        date.selected = false;
                    }
                });
            }


            this.getFlightDirection = function (toCode, fromCode) {
                return applicationService.getFlightDirection(toCode, fromCode)
            }

            this.onBeforeSelectDate = function (selectedDate, dayObj, selectedSequence, isOriginalEvent) {
                var isValid = true;
                var isReturnDateSelected = false;

                // Stop firing select date event.
                if (self.fareStepProgressStatus == 'completed' && self.sectorsData.sectors.length && !isOriginalEvent) {
                    return false;
                } else {
                    self.searchCriteria = travelFareService.getSearchCriteria();
                }

                if (self.searchCriteria.journeyType == 'return') {
                    if (selectedSequence == 0) {
                        depDate = moment(selectedDate, "DD/MM/YYYY").format('YYYY-MM-DD');
                    } else {
                        arrDate = moment(selectedDate, "DD/MM/YYYY").format('YYYY-MM-DD');
                        isReturnDateSelected = true;
                    }
                    if (moment(arrDate).isAfter(depDate) || moment(arrDate).isSame(depDate)) {
                        isValid = true;
                        if (isReturnDateSelected) {
                            selectedReturnDate = selectedDate;
                        }
                    } else {
                        isValid = false;
                        popupService.alert("Your return flight date cannot be prior to your departure flight date. Please select a future return flight or change the dates.​");

                    }
                }

                return isValid
            };

            this.isProceedDisabled = function () {
                if (!self.putCheckboxToAcceptTerms) {
                    return !self.quoteDetailsSummary;
                } else {
                    return !self.quoteDetailsSummary || !self.agreeToTerms;
                }
            };

        }
    ];
    return controllers;
};
