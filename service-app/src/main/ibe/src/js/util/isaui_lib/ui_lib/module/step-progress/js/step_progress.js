/**
 * Created by baladewa on 11/4/15.
 */
'use strict';

(function () {
    var isa_step_progress = angular.module("isaStepProgress", []);
    var directives = {};
    directives.isaStepProgress = [
        '$compile', '$timeout', '$window',
        function ($compile, $timeout, $window) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                currentFlag: "@",
                currentStep: "="
            };
            directive.link = function (scope, element, attr) {
                scope.selectedElement = angular.element("." + scope.currentFlag);
                scope.toLeft = 0;
                $timeout(function () {
                    if (angular.element("." + scope.currentFlag)[0] !== undefined) {
                        var tempObj = angular.element("." + scope.currentFlag);
                        var lf = parseInt(tempObj.offset().left);
                        lf += parseInt(tempObj[0].offsetWidth);
                        angular.element(element[0].children[0]).css({
                            'width': lf
                        });
                    } else {
                        angular.element(element[0].children[0]).css({
                            'width': '100%'
                        });
                    }
                }, 2000);

                scope.$watch('currentStep', function (newVal, oldVal) {
                    if (newVal.id) {
                        $timeout(function () {
                            if (angular.element("." + scope.currentFlag)[0] !== undefined) {
                                var tempObj = angular.element("." + scope.currentFlag);
                                scope.toLeft = parseInt(tempObj.offset().left);
                                scope.toLeft += parseInt(tempObj[0].offsetWidth);
                                angular.element(element[0].children[0]).animate({
                                    width: scope.toLeft
                                }, "slow");
                            } else {
                                angular.element(element[0].children[0]).animate({
                                    width: '100%'
                                }, "slow");
                            }
                        });
                    }

                });

                /**
                 * Window Resize
                 */
                angular.element($window).bind('resize', function () {
                    if (angular.element("." + scope.currentFlag)[0] !== undefined) {
                        var tempObj = angular.element("." + scope.currentFlag);
                        scope.toLeft = parseInt(tempObj.offset().left);
                        scope.toLeft += parseInt(tempObj[0].offsetWidth);
                        angular.element(element[0].children[0]).css({
                            width: scope.toLeft
                        });
                    }
                });
            };
            return directive;
        }];
    isa_step_progress.directive(directives);
})();