/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_radioOptionlist = angular.module("isaradiooptionlist", ['ngAnimate']);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaradiooptionlist = [
        '$compile', '$http', '$templateCache', 'currencyService','constantFactory',
        function ($compile, $http, $templateCache, currencyService,constantFactory) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                template: '@',
                availableOptions: '=',
                source: '=',
                previousSource: '=',
                preferedsource:'=',
                setOption: '=',
                selectOption: '&',
                level: '=',
                type: '@',
                headerValue: '@',
                iconClickpm: '&',
                iconClick: '&',
                listType: '@'
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    //scope.selectedSection = "Outbound";

                    var template = $templateCache.get(isaconfig.getTemplates("isaradiooptionlist")[scope.template || 'default']);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);
                    //TODO refactor this using isolalte scope
                    scope.currencyType = currencyService.getSelectedCurrency();
                    scope.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');

                    // If array of prices provided get the appropriate charge
                    scope.getPriceForPax = function (passenger, priceList) {
                        var paxType = passenger.type;
                        if(passenger.infantSequence) {
                            paxType ='ADULT_WITH_INFANT';
                        }

                        var amountForPax = _.findWhere(priceList, {chargeBasis: paxType.toUpperCase()});
                        if(amountForPax){
                            return amountForPax.amount;
                        }
                    }

                };
            return directive;

        }];

    isa_radioOptionlist.controller('radioOptionListController', ['$scope', '$element', function ($scope, $element) {
        //$('#radio-Outbound-0').prop('checked', true);
        $scope.$watch('setOption', function (newval, oldval) {
//            console.log("changed!" + JSON.stringify(newval));
            if (newval !== undefined) {
                $scope.selectedIndex = newval.index;
                $scope.selectedSection = newval.section;
                $('#radio-0' + '-' + newval.index).prop('checked', true);
            }
        });

        $scope.optionOnSelect = function (event, index, section) {
            // var type = $(event.currentTarget).data('section-key');
            $scope.selectedIndex = index;
            $scope.selectedSection = index;
            $scope.isSectionSelected(section);
            var sectorData = {index: index, section: index};
            $scope.selectOption(sectorData);
        };

        $scope.isSectionSelected = function (section) {
            var isselected = ($scope.selectedSection === section) ? true : false;
            return isselected;
        };

        $scope.getSource = function (obj) {
            return obj[$scope.type];
        };

        $scope.getLabel = function (obj, key) {
            return obj[Object.keys(obj)[key]];
        };

        $scope.capitalizeFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        };
        $scope.onChildIconClick = function (el, event, index) {
//            console.log(el);
            $scope.iconClick({element: el, event: event, index: index});
        }
        $scope.onChildIconClickPM = function (el, event, index) {
          $scope.iconClickpm({element: el, event: event, index: index});
      }
        $scope.getLength = function (arr) {
            var length = Object.keys(arr).length - 1;
            return length;
        }


    }]);
    isa_radioOptionlist.directive(directives);
})();

