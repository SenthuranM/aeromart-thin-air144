/**
 * Created by indika on 9/12/16.
 */
/**
 * Created by indika on 9/7/16.
 */
/**
 * Created by indika on 10/16/15.
 */
'use strict';
(function(angular){
    var module = angular.module('isaFileUploader',[]);
    module.directive('fileuploader',[function(){
        return  {
            restrict :'AE',
            scope : {
                setFileSelection : '&',
                maxSize : '=',
                fileType : '@',
                fileName : '@'
            },
            link : function(scope,ele,attr){
                ele.bind('change', function( evt ) {
                    scope.$apply(function() {
                        scope[ attr.name ] = evt.target.files;
                        console.log( scope[ attr.name ] );
                        var file = evt.target.files[0];
                        var fileType = file.type;
                        var imageType = /image.*/;
                        var zipType =  /zip.*/;

                        if (fileType.match(scope.fileType)) {
                            var reader = new FileReader();
                            if(fileType.match(imageType)) {
                                reader.onload = function (e) {
                                    var img = new Image();
                                    img.src = reader.result;
                                    img.onload = function (imageEvent) {
                                        ele.parent().find('.icon-img').innerHTML = "";
                                        var canvas = ele.parent().parent().find('.icon-img canvas')[0],
                                            max_size = scope.maxSize || 50,
                                            width = img.width,
                                            height = img.height;
                                        if (width > height) {
                                            if (width > max_size) {
                                                height *= max_size / width;
                                                width = max_size;
                                            }
                                        } else {
                                            if (height > max_size) {
                                                width *= max_size / height;
                                                height = max_size;
                                            }
                                        }
                                        canvas.width = width;
                                        canvas.height = height;
                                        canvas.getContext('2d').drawImage(img, 0, 0, width, height);
                                        //Getting base64 string;
                                        var dataUrl = canvas.toDataURL(fileType);
                                        scope.setFileSelection({data: {file: dataUrl, type: fileType, id: attr.id, name:scope.fileName}});

                                    }

                                }
                            }else if(fileType.match(zipType)){
                                ele.parent().parent().find('.icon-img label').text(file.name);
                                ele.parent().parent().find('.icon-img i').css({'display':'block'});
                                reader.onload = function (e) {
                                    scope.setFileSelection({data: {file: reader.result, type: fileType, id: attr.id, name:scope.fileName}});
                                }
                            }

                            reader.readAsDataURL(file);
                        } else {
                            alert('file type is not suported')
                        }
                    });
                });
            }
        };
    }]);
})(angular);