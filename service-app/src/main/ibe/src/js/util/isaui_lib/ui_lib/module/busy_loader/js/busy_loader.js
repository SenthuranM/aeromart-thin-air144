/**
 *     Author  : Chamil Udayanga
 *     Date    : 11/01/2016
 */
'use strict';
(function () {

    var isaBusyLoader = angular.module('isaBusyLoader', []);
    var isaConfig = new IsaModuleConfig();

    isaBusyLoader.directive('isaBusyLoader', [
        '$compile', '$window', '$timeout', '$rootScope', '$templateCache',
        function ($compile, $window, $timeout, $rootScope, $templateCache) {

            return {
                restrict: 'AEC',
                scope: {},
                link: function (scope, element, attr) {
                    scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                    if(attr['isFixedPosition'] == "true") {
                        if(!scope.isMobileDevice){
                            scope.positionFixed = {"position":"fixed","top":"45%","left":"45%"};
                        }
                        if(scope.isMobileDevice){
                            scope.positionFixed = {"position":"fixed","top":"45%","left":"0","right":"0"};
                        }
                    }else{                        
                        scope.positionFixed = {};
                    }
                    if(attr['msgText'] === undefined) {
                        scope.msgText = "Please Wait...";
                        }else{
                        scope.msgText = attr['msgText'];
                    }

                    element.parent().css("position", "relative");
                    //element.attr() //is-fixed-position
                    var htmlText = $templateCache.get(isaConfig.getTemplates("isaBusyLoader")['default']);
                    var template = angular.element($compile(htmlText)(scope));
                    element.append(template);

                }

            }; // End of return.

        }]); // End of isaBusyLoader function.

})(); // End if IIFE.
