/**
 * Created by indika on 10/19/15.
 */

'use strict';
/*exported IsaModuleConfig*/
/*global IsaModuleConfig: true*/
var IsaModuleConfig = function(){
    var isamodules = {};
    //var _root = "ui_lib/module"; // UI Library test
    //var _root = "js/util/isaui_lib/ui_lib/module";
    //all
    var data = {
        scrollTop : {
            name: 'scrollTop'
        },
        isaButton : {
            name: 'isaButton',
            templates: {
                default:"module/button/view/button-view.html",
                radio:"module/button/view/radio_button.html",
                checkbox:"module/button/view/checkbox_view.html"
            }
        },
        isaCounterButton:{
            name:'isaCounterButton',
            templates : {
                counter:"module/counter_button/view/counter-button-view.html"
            }
        },
        isaStepProgress:{
            name:'isaStepProgress'
        },
        isaCurrencyConverter:{
            name:'isaCurrencyConverter',
            templates : {
                default:"module/currency_converter/view/currency-converter-view.html"
            }
        },
        fareFloater:{
            name:'fareFloater'
        },
        stickMe:{
            name:'stickMe'
        },
        isaspinner:{
            name:'isaspinner',
            templates : {
                default:"module/spinner/view/spinner-view.html"
            }
        },
        isaBusyLoader: {
            name: 'isaBusyLoader',
            templates: {
                default: 'module/busy_loader/view/busy_loader_tpl.html'
            }
        },
        isaLoadingProgress:{
            name:'isaLoadingProgress',
            templates : {
                default:"module/loading_progress/view/loading-progress-tpl.html"
            }
        },
        isaplaceholder:{
            name:'isaplaceholder',
            templates : {
                default:"module/placeholder/view/placeholder-tpl.html"
            }
        },
        isaAttrPlaceholder:{
            name:'isaAttrPlaceholder',
            templates : {
                default:"module/attr-placeholder/view/attr-placeholder-tpl.html"
            }
        },
        isaConnector:{
            name:'isaConnector'
        },
        isaFlightSelect:{
            name:'isaFlightSelect',
            templates : {
                v1: {
                    default_body: "module/flight_select/view/v1/flight-select-body.tpl.html",
                    modify_body: "module/flight_select/view/v1/modify_flight_select_body.html",
                    modify_selected_Flight_body: "module/flight_select/view/v1/modify_selected_flights_body.html"
                },
                v2: {
                    default_body: "module/flight_select/view/v2/flight-select-body.tpl.html",
                    modify_body: "module/flight_select/view/v2/modify_flight_select_body.html",
                    modify_selected_Flight_body: "module/flight_select/view/v2/modify_selected_flights_body.html"
                },
                default_header: "module/flight_select/view/flight-select-header.tpl.html",
                default_popup: "module/flight_select/view/flight-select-popup.tpl.html"
            }
        },
        isaFareAndServicesDetails:{
            name:'isaFareAndServicesDetails',
            templates : {
                default: "module/fare_and_services_details/view/fare-and-services-details.tpl.html"
            }
        },
        isaSavedCard:{
            name:'isaSavedCard',
            templates : {
            	default:"module/saved_card/view/saved-card-view.html"
            }
        },
        isaAutoCheckin:{
            name:'isaAutoCheckin',
            templates : {
            	default:"module/auto_checkin/view/auto-checkin-view.html"
            }
        },
        isaPassengerList:{
            name:'isaPassengerList',
            templates : {
                default:"module/passenger_list/view/passenger-list-view.html",
                seatmap:"module/passenger_list/view/passenger-list-for-seatmap.tpl.html",
                services:"module/passenger_list/view/passenger-list-for-services.tpl.html"
            }
        },
        isaPassengerDetails:{
            name:'isaPassengerDetails',
            templates : {
                default:"module/passenger_details/view/passenger-details-view.html"
            }
        },
        isaoptionlist:{
            name:'isaoptionlist',
            templates : {
                extras:"module/option_list/view/option_list_tpl.html",
                baggage:"module/option_list/view/baggage_option_list_tpl.html",
                meals:"module/option_list/view/meal_option_list_tpl.html",
                services:"module/option_list/view/services_option_list_tpl.html",
                insurance : "module/option_list/view/insurance_option_list_tpl.html",
                modificationFlightSegment :"module/option_list/view/modification_flight_segment_list_tpl.html",
                modificationExtras:"module/option_list/view/modification_extras_list_tpl.html"
            }
        },
        isaservicesoptionlist:{
            name:'isaservicesoptionlist',
            templates : {
                extras:"module/services_option_list/view/option_list_tpl.html",
                services:"module/services_option_list/view/services_option_list_tpl.html",
                inFlightServices:"module/services_option_list/view/in_flight_services_option_list_tpl.html"

            }
        },
        isaradiooptionlist:{
            name:'isaradiooptionlist',
            templates : {
                default:"module/radio_option_list/view/radio_option_list_tpl.html",
                services: "module/radio_option_list/view/services_radio_option_list_tpl.html",
                seat: "module/radio_option_list/view/seat_radio_option_list_tpl.html"
            }
        },
        seatMap:{
            name:'seatMap',
            templates : {
                default:"module/seatMap/view/seatmap.tpl.html"
            }
        },
        isaSpinnerList:{
            name:'isaSpinnerList',
            templates : {
                default:"module/spinner_list/view/spinner_list_tpl.html"
            }
        },
        isaNoMouse : {
            name : 'isaNoMouse'
        },
        isaPassengerPicker : {
            name : 'isaPassengerPicker',
            templates : {
                default:"module/passenger-picker/view/passenger-picker-view.html",
                multiCity:"module/passenger-picker/view/passenger-picker-multi-city-view.html"
            }
        },
        isaProgress : {
            name : 'isaProgress',
            templates : {
                default:"module/progress/view/progress-view.html",                
            }
        },
        clickOutside : {
            name : 'clickOutside'
        },
        typeaheadFocus : {
            name : 'typeaheadFocus'
        },
        isaValidator : {
            name : 'isaValidator'
        },
        isaValidatorType : {
            name : 'isaValidatorType'
        },
        isaPhoneNumber : {
            name: 'isaPhoneNumber',
            templates : {
                default:"module/phone-number/view/phone-number-view.html"
            }
        },
        isaBackButton : {
            name: 'isaBackButton',
            templates: {
                default:"module/back_button/view/back_button_view.html"
            }
        },
        isaJqDateUtc : {
            name: 'jqDateUtc'
        },
        isacolorpicker : {
            name: 'isacolorpicker',
            templates: {
                default:"module/color_picker/view/isa_color_picker_view.html"
            }
        },
        isaFileUploader :{
            name: 'isaFileUploader'

        },
        'isaTranslate': {
            name:'isaTranslate'
        },
        'airportTransferList': {
            name:'airportTransferList',
            templates: {
                default:"module/airport_transfer/view/airport_transfer_list.html"
            }
        },
        'admDatePicker':{
            name:'admDatePicker'
        }


    };

    isamodules.getModuleNames = function(){
        var modNamess = [];
        for(var key in data) {
            modNamess.push(key);
        }
        return modNamess;

    };
    isamodules.getTemplates = function(name){
        return data[name].templates;
    };
    return isamodules;

};
;
'use strict';

(function () {
     'use strict';
    if (!angular.merge)
        angular.merge = angular.extend;

    String.prototype.toPersianDigits = function(){
        var id= ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'];
        return this.replace(/[0-9]/g, function(w){
            return id[+w]
        });
    };
    String.prototype.toEnglishDigits = function(){
        var id= {'۰':'0','۱':'1','۲':'2','۳':'3','۴':'4','۵':'5','۶':'6','۷':'7','۸':'8','۹':'9'};
        return this.replace(/[^0-9.]/g, function(w){
            return id[w]||w;
        });
    };
    String.prototype.lZero = function() {
        return (this.length<2 ? '0'+this : this);
    };
Object.defineProperty(Array.prototype, "toNumber", {
    enumerable: false,
    writable: true,
    value: function(testFun) {
       return this.map(function(item) {return Number(item);});
    }
});
Object.defineProperty(Array.prototype, "dtp_toDate", {
    enumerable: false,
    writable: true,
    value: function(type) {
         var splitter = '-';
        if (/invalid/i.test(new Date('1991-9-12')))
            splitter = '/';
        
        var date = this.join(splitter);
        if (this.length == 5)
            date = this.slice(0,3).join(splitter) +' '+ this.slice(3,5).join(':')
        if (!type) return date;
        date = new Date(date);
        if (type == 'unix')
            return date.getTime();
        return date;
    }
});
 Number.prototype.lZero = function() {
        return (this<10 ? '0'+this : this);
    };
    Date.prototype.dtp_shortDate = function () {
        return [this.getFullYear(), this.getMonth() + 1, this.getDate()].dtp_toDate();
    }
    var ADMdtpDigitTypeFilter = function() {
        return function(input, type) {
            return type=='jalali' ? String(input).toPersianDigits() : input;
        };
    };
    var ADMdtpConvertor = function() {
        
        function getJalaliDate(date) {
            var daysPassedInGregorianCalender = getDaysPassedInGregorianCalender(date);
            daysPassedInGregorianCalender -= 226894;
            return getJalaliDateOfDay(daysPassedInGregorianCalender);
        }
        function getJalaliDateOfDay(daysPassedInJalaliCalender) {
            var yearOfDay = getYearJalaliCalender(daysPassedInJalaliCalender);

            var monthOfDay = getMonthJalaliCalender(daysPassedInJalaliCalender, yearOfDay);

            var dayOfMonth = getDayJalaliCalender(daysPassedInJalaliCalender, yearOfDay, monthOfDay);
            var date =
                {
                    day: dayOfMonth,
                    month: monthOfDay,
                    year: yearOfDay
                };
            return date;
        }
        function getDayJalaliCalender(daysPassedInJalaliCalender, yearOfDay, monthOfDay) {
            var leaps = howManyLeapsYearPassedInJalaliCalender(yearOfDay);
            daysPassedInJalaliCalender -= leaps + ((yearOfDay - 1) * 365);
            for (var i = 1; i < monthOfDay; i++) {
                if (i <= 6) {
                    daysPassedInJalaliCalender -= 31;
                }
                else {
                    daysPassedInJalaliCalender -= 30;
                }
            }
            return daysPassedInJalaliCalender;
        }
        function getMonthJalaliCalender(daysPassedInJalaliCalender, yearOfDay) {
            var leaps = howManyLeapsYearPassedInJalaliCalender(yearOfDay);
            daysPassedInJalaliCalender -= leaps + ((yearOfDay - 1) * 365);
            var jalaliMonths = getJalaliMonths();
            for (var i = 0; i < jalaliMonths.length; i++) {
                if (daysPassedInJalaliCalender <= jalaliMonths[i].count) {
                    return jalaliMonths[i].id;
                }
                daysPassedInJalaliCalender -= jalaliMonths[i].count;
            }
            return 12;
        }
        function getJalaliMonths() {
            return [
                { id: 1, count: 31 },
                { id: 2, count: 31 },
                { id: 3, count: 31 },
                { id: 4, count: 31 },
                { id: 5, count: 31 },
                { id: 6, count: 31 },
                { id: 7, count: 30 },
                { id: 8, count: 30 },
                { id: 9, count: 30 },
                { id: 10, count: 30 },
                { id: 11, count: 30 },
                { id: 12, count: 29 }
            ];
        }
        function getYearJalaliCalender(daysPassedInJalaliCalender) {
            var years = Math.floor((daysPassedInJalaliCalender - 1) / 365);
            var leapsCount = 0;
            if (years > 22) {
                var year1 = years - 22 - 1;
                var year2 = years - 22;

                var siose = Math.floor(year1 / 33);
                var remainYear = (year2 - (siose * 33));
                if (remainYear >= 28) {
                    remainYear = 28;
                }
                var a = Math.floor(remainYear / 4);
                var sum = a + (siose * 8) + 6;
                var sal = Math.floor((daysPassedInJalaliCalender - sum) / 365);
                leapsCount = howManyLeapsYearPassedInJalaliCalender(sal);
                if (daysPassedInJalaliCalender - (sal * 365) - leapsCount - (isLeapYearInJalaliCalender(sal) ? 1 : 0) > 0) {
                    sal++;
                    return sal;
                }
                else if (daysPassedInJalaliCalender - (sal * 365) + leapsCount <= 0) {
                    return sal;
                }
                return sal;

            }
            else {
                if (years < 1) {
                    leapsCount = 0;
                }
                else if (years >= 1 && years <= 4) {
                    leapsCount = 1;
                }
                else if (years >= 5 && years <= 8) {
                    leapsCount = 2;
                }
                else if (years >= 9 && years <= 12) {
                    leapsCount = 3;
                }
                else if (years >= 13 && years <= 16) {
                    leapsCount = 4;
                }
                else if (years >= 17 && years < 22) {
                    leapsCount = 5;
                }
                else {
                    leapsCount = 6;
                }
                years = Math.floor((daysPassedInJalaliCalender - leapsCount - 1) / 365);

                return years + 1;
            }
        }
        function howManyLeapsYearPassedInJalaliCalender(year) {
            if (year < 23) {
                switch (year) {
                    case 1:
                        return 0;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        return 1;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        return 2;
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                        return 3;
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                        return 4;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        return 5;


                }
            }
            var yearAfterFirstEra = year - 22;
            var countSioSe = Math.floor((yearAfterFirstEra - 1) / 33);
            var remainOfNormalLeapsYear = yearAfterFirstEra - countSioSe * 33;
            if (remainOfNormalLeapsYear > 28) {
                remainOfNormalLeapsYear = 28;
            }
            var leapsCount = Math.floor(remainOfNormalLeapsYear / 4) + countSioSe * 8 + 6;
            if (isLeapYearInJalaliCalender(year) && (yearAfterFirstEra - countSioSe * 33) <= 28) {
                leapsCount--;
            }
            return leapsCount;
        }
        
        function getDaysPassedInGregorianCalender(date) {
            var gregorianMonths = getGregorianMonths();
            var passedLeapYears = howManyGregorianLeapsYearPassed(date.year);
            var days = passedLeapYears;
            var isMiladiLeaps = isGregorianLeapYear(date.year);
            days += (date.year - 1) * 365;
            for (var i = 0; i < date.month - 1; i++) {
                if (isMiladiLeaps && i + 1 == 2) {
                    gregorianMonths[i].count = 29;
                }
                days += gregorianMonths[i].count;

            }
            days += date.day;
            return days;
        }
        function getGregorianMonths() {
            return [
                { id: 1, count: 31 },
                { id: 2, count: 28 },
                { id: 3, count: 31 },
                { id: 4, count: 30 },
                { id: 5, count: 31 },
                { id: 6, count: 30 },
                { id: 7, count: 31 },
                { id: 8, count: 31 },
                { id: 9, count: 30 },
                { id: 10, count: 31 },
                { id: 11, count: 30 },
                { id: 12, count: 31 }
            ];
        }
        function isGregorianLeapYear(year) {
            if (year % 4 != 0) {
                return false;
            }
            if (year % 100 != 0) {
                return true;
            }
            if (year % 400 != 0) {
                return false;
            }
            return true;
        }
        function howManyGregorianLeapsYearPassed(year) {
            var yearsPassed = year - 1;
            var countOfFourYears = Math.floor(yearsPassed / 4);
            var countOfHandredYears = Math.floor(yearsPassed / 100);
            var countOfFourHandredYears = Math.floor(yearsPassed / 400);
            return countOfFourYears - countOfHandredYears + countOfFourHandredYears;
        }
        function getGregorianYear(gregorianPassedDays) {
            var pureYear = Math.floor((gregorianPassedDays) / 365);
            var gregorianLeapsYear = howManyGregorianLeapsYearPassed(pureYear);
            var year = Math.floor((gregorianPassedDays - gregorianLeapsYear) / 365);
            var remainDay = gregorianPassedDays - year * 365 - gregorianLeapsYear;
            if (remainDay != 0) {
                year++;
            }
            else if (isGregorianLeapYear(year + 1)) {
                year += gregorianLeapsYear / 365;
            }
            return Math.floor(year);
        }
        function getGregorianMonth(daysPassed) {
            var year = getGregorianYear(daysPassed);
            var leaspYearCount = howManyGregorianLeapsYearPassed(year);
            daysPassed -= (year - 1) * 365 + leaspYearCount;
            var months = getGregorianMonths();
            var month = 0;
            var isCurrentYearLeaps = isGregorianLeapYear(year);
            for (var i = 0; i < months.length; i++) {
                if (isCurrentYearLeaps && months[i].id == 2) {
                    months[i].count = 29;
                }
                if (daysPassed < months[i].count) {
                    if (daysPassed != 0 || month == 0) {
                        month++;
                    }
                    return month;
                }
                daysPassed -= months[i].count;
                month = months[i].id;
            }
            return month;
        }
        function getGregorianDayOfMonthByPassedDay(daysPassed) {
            var year = getGregorianYear(daysPassed);
            var month = getGregorianMonth(daysPassed);
            return getGregorianDayOfMonth(year, month, daysPassed);
        }

        function getGregorianDayOfMonth(year, month, daysPassed) {
            var leaspYearCount = howManyGregorianLeapsYearPassed(year);
            var months = getGregorianMonths();
            var sumOfMonths = 0;
            for (var i = 0; i < months.length; i++) {
                if (months[i].id < month) {
                    sumOfMonths += months[i].count;
                }
            }
            if (isGregorianLeapYear(year) && month > 2) {
                sumOfMonths++;
            }
            return daysPassed - (year - 1) * 365 - leaspYearCount - sumOfMonths;
        }
        function getDaysPassedInJalaliCalander(date) {
            var days = date.day;
            days += passedDaysFromMonthsInJalaliCalander(date.month);
            days += passedDaysToYearsInJalaliCalander(date.year);
            return days;
        }

        function passedDaysFromMonthsInJalaliCalander(month) {
            var days = 0;
            var months = getJalaliMonths();
            for (var i = 0; i < month - 1; i++) {
                days += months[i].count;
            }
            return days;
        }

        function passedDaysToYearsInJalaliCalander(years) {
            var days = howManyLeapsYearPassedInJalaliCalender(years);
            days += (years - 1) * 365;
            return days;
        }

        function getGregorianDate(date) {
            var daysPassed = getDaysPassedInJalaliCalander(date) + 226894;
            var day = getGregorianDayOfMonthByPassedDay(daysPassed);
            var month = getGregorianMonth(daysPassed);
            var year = getGregorianYear(daysPassed);
            if (day == 0)
            {
                day = 31;
                month = 12;
                year--;
            }
            return {
                day: day,
                month: month,
                year: year
            };

        }
        var isLeapYearInJalaliCalender = function(years) {
            if (years == 1 || years == 5 || years == 9 || years == 13 || years == 17 || years == 22) {
                return true;
            }
            else if (years < 22) {
                return false;
            }
            var year = years - 22;
            var siosesal = Math.floor(year / 33);
            return ((year - (siosesal * 33)) % 4 == 0) && (year - (siosesal * 33)) / 4 != 8;
        }
        var getPersianDate = function(year, month, day) {
            year = (year<=99)?(2000+year):year;
            var date = { year: year, month: month, day: day };
            date = getJalaliDate(date);
            return date;
        }
        var getGregorianDates = function(year, mont, day) {
            year = (year<=99)?(1300+year):year;
            var date= getGregorianDate( {
                day: day,
                month: mont,
                year: year
            });
            return date;
        }
        
        return {
            toJalali: getPersianDate,
            toGregorian: getGregorianDates,
            isLeapJalali: isLeapYearInJalaliCalender
        }
    }
    var ADMdtpProvider = function() {

        var options = {
            calType: 'gregorian',
            format: 'YYYY/MM/DD hh:mm', 
            multiple: true,
            autoClose: false,
            transition: true,
            disabled: [],
            smartDisabling: true,
            minuteStep: 1,
            gregorianStartDay: 0,
            gregorianDic: {
                title: 'Gregorian',
                monthsNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                daysNames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                todayBtn: 'Today',
            },
            jalaliDic: {
                title: 'جلالی',
                monthsNames: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
                daysNames: ['ش', 'ی', 'د', 'س', 'چ', 'پ', 'ج'],
                todayBtn: 'امروز'
            }
        };

        var ADMdtp = {
            getOptions: function(type) {
                var typeOptions = type && options[type] || options;
                return typeOptions;
            }
        };

        this.setOptions = function(type, customOptions) {
            if (!customOptions) {
                customOptions = type;
                options = angular.merge(options, customOptions);
                return;
            }
            options[type] = angular.merge(options[type] || {}, customOptions);
        };

        this.$get = function() {
            return ADMdtp;
        };

    };
     var ADMdtpFactory = function(ADMdtpConvertor) {

        this.dateFormat = function(date, time, format, notView) {
            if (!date.year) return '';

            var year = date.year;
            var halfYear = notView ? date.year : date.year%100;
            var month = date.month.lZero();
            var day = date.day.lZero();
            var hour = time.hour.lZero();
            var minute = time.minute.lZero();
            
            var replaceMap = [
                {key: 'YYYY', value: year},
                {key: 'YY', value: halfYear},
                {key: 'MM', value: month},
                {key: 'DD', value: day},
                {key: 'hh', value: hour},
                {key: 'mm', value: minute}
            ]
            
            for(var i=0,j=replaceMap.length;i<j;i++) {
                format = format.replace(replaceMap[i].key, replaceMap[i].value);
            }
            
            return format;
        };
        this.parseString = function(str, format) {
            var _keys = [], _date = {};
            var formats = ['YY/MM/DD', 'YY/MM/DD hh:mm', 'YY-MM-DD', 'YY-MM-DD hh:mm', 'MM/DD/YY', 'MM-DD-YY', 'MM/DD/YY hh:mm', 'MM-DD-YY hh:mm'];
            formats.unshift(format);
            
            for(var i=0,j=formats.length;i<j;i++) {
                var _isValid = new RegExp('^' + formats[i].replace(/[a-z]+/gi, function(key) {
                    var _mustReplace = false;
                    if (key.indexOf('YY') !== -1)
                        _keys.push('year'), _mustReplace=true;
                    else if (key.indexOf('MM') !== -1)
                        _keys.push('month'), _mustReplace=true;
                    else if (key.indexOf('DD') !== -1)
                        _keys.push('day'), _mustReplace=true;
                    else if (key.indexOf('hh') !== -1)
                        _keys.push('hour'), _mustReplace=true;
                    else if (key.indexOf('mm') !== -1)
                        _keys.push('minute'), _mustReplace=true;

                    if (_mustReplace)
                        return '[0-9]+';
                    else 
                        return key;
                    
                }).replace(/[(]/g, '[(]').replace(/[)]/g, '[)]') + '$').test(str);

                if (!_isValid){
                    continue;
                }

                _keys.reverse();
                
                str.replace(/[0-9]+/g, function(value) {
                    _date[_keys.pop()] = Number(value);
                    return value;
                });
                _date.hour = _date.hour || 0;
                _date.minute = _date.minute || 0;

                return _date;
            }
            
            return false;
        };
        this.toRegularFormat = function(date, type, format) {
            if (!date) return false;
            
            if (typeof date == "string")
                date = this.parseString(date, format);
            else if (typeof date == "number")
                date = this.convertFromUnix(date, type);
            
            if (!date) return false;

            if (date.year<=99)
                date.year = ((type == 'jalali') ? 1300+date.year : 2000+date.year);
            
            return [date.year, date.month.lZero(), date.day.lZero(), date.hour.lZero(), date.minute.lZero()].dtp_toDate();
        };
        this.isDateEqual = function(date1, date2) {
            var diff = new Date(date1) - new Date(date2);
            return diff==0;
        };
        this.isDateBigger = function(date1, date2) {
            var diff = new Date(date1) - new Date(date2);
            return diff>=0;
        };
        this.isMonthBigger = function(date1, date2) {
            var diff = new Date(date1.year, date1.month) - new Date(date2.year, date2.month);
            return diff>=0;
        };
        this.joinTime = function(date, time) {
            return new Date(new Date(new Date(date).setHours(time.hour)).setMinutes(time.minute));
        };
        this.removeTime = function(date) {
            return [date.getFullYear(), date.getMonth()+1, date.getDate()].dtp_toDate('date');
        }
        this.validateJalaliDateSeparate = function(date, time) {
            if (date.length!=3 || time.length!=2)
                return false;
            
            if (time[0]>23 || time[0]<0 || time[1]>59 || time[1]<0 || date[0]<0 || date[1]<1 || date[1]>12)
                return false;
            
            if (date[1]>0 && date[1]<7) {
                if (date[2]<1 || date[2]>31)
                    return false;
            }
            else if (date[1]>6 && date[1]<12) {
                if (date[2]<1 || date[2]>30)
                    return false;
            }
            else if (date[1] == 12) {
                var isLeap = ADMdtpConvertor.isLeapJalali(date[0]);
                if ((isLeap && (date[2]<1 || date[2]>30)) || (!isLeap && (date[2]<1 || date[2]>29)))
                    return false;
            }

            return true;
        }
        this.validateJalaliDate = function(input, format) {
            var _dateTime;
            
            if (typeof input == "number") {
                var _gDate = new Date(input);
                if (/invalid/i.test(_gDate))
                    return false;
                var _pDate = this.convertToJalali(_gDate);
                _dateTime = angular.merge(_pDate, {hour: _gDate.getHours(), minute: _gDate.getMinutes()});
            }
            else if (typeof input == "string")
                _dateTime = this.parseString(input, format);
            
            else if (input instanceof Object)
                _dateTime = input;
            
            if (!_dateTime) return false;
            
            var _date = [_dateTime.year, _dateTime.month, _dateTime.day];
            var _time = [_dateTime.hour, _dateTime.minute];
            
            if (this.validateJalaliDateSeparate(_date, _time)) {
                var _gDateC = ADMdtpConvertor.toGregorian(_date[0],_date[1],_date[2]);
                var _gDate = [_gDateC.year, _gDateC.month, _gDateC.day, _time[0], _time[1]].dtp_toDate('date');
                
                return {
                    year: _date[0],
                    month: _date[1],
                    day: _date[2],
                    hour: _time[0],
                    minute: _time[1],
                    unix: _gDate.getTime(),
                    gDate: _gDate
                }
            }
            return false;

        };
        this.convertToUnix = function(value, type, format) {
            if (!value)
                return null;
            if (typeof value == "number")
                return value;
            
            if (typeof value == "string") {
                value = this.parseString(value, format);
            }
            else if (value instanceof Date)
                value = {year: value.getFullYear(), month: value.getMonth()+1, day: value.getDate(), hour: value.getHours(), minute: value.getMinutes()};
            else
                return null;
            
            if (value.year<=99)
                value.year = ((type == 'jalali') ? 1300+value.year : 2000+value.year);
            
            
            if (type == 'jalali') {
                var _dateTime = this.validateJalaliDate(value, format);
                return _dateTime.unix || null;
            }
            else if (type == 'gregorian') {
                var _dateTime = new Date(this.toRegularFormat(value, type));
                return (/invalid/i.test(_dateTime))?null:_dateTime.getTime();
            }
            
            return null;
        };
        this.convertFromUnix = function(unix, type) {
            var _gDate = new Date(unix);
            if (type == 'jalali')
                return this.convertToJalali(_gDate);
            else if (type == 'gregorian')
                return {
                    year: _gDate.getFullYear(),
                    month: _gDate.getMonth()+1,
                    day: _gDate.getDate(),
                    unix: unix
                };
        };
        this.convertToJalali = function(date) {
            
            if (date instanceof Date) {
                var _date = {
                    year: date.getFullYear(),
                    month: date.getMonth()+1,
                    day: date.getDate(),
                    unix: date.getTime()
                }
                date = _date;
            }
            if (date instanceof Object) {
                return angular.merge(ADMdtpConvertor.toJalali(date.year, date.month, date.day), {unix: date.unix});
            }
        };
        this.parseDisablePattern = function(options) {
            var arr = options.disabled, smart = options.smartDisabling, calType = options.calType, format = options.format;
            
            var _inWeek = Array.apply(null, Array(7)).map(Number.prototype.valueOf,0);
            var _inMonth = Array.apply(null, Array(31)).map(Number.prototype.valueOf,0);
            var _static = {};
            
            if (arr instanceof Array) {
                for (var i=0,j=arr.length; i<j; i++) {
                    if (typeof arr[i] == "number") {
                        var _gDate = new Date(arr[i]);
                        if (!/invalid/i.test(_gDate))
                            _static[this.removeTime(_gDate).getTime()] = true;
                    }
                    else if (typeof arr[i] == "string") {
                        arr[i] = arr[i].toLowerCase();
                        if (arr[i].indexOf('d') == -1 && arr[i].indexOf('i') == -1) {
                            var _unix = this.convertToUnix(arr[i], calType, format);
                            if (_unix)
                                _static[_unix] = true;
                        }
                        else {
                            var _inMonthValid = new RegExp("^[!]?(([0-9]?[0-9])?[d]([+][0-9][0-9]?)?)([&]([0-9]?[0-9])?[d]([+][0-9][0-9]?)?)*?$").test(arr[i]);
                            var _inWeekhValid = new RegExp("^[!]?([i]([+][0-9][0-9]?)?)([&][i]([+][0-9][0-9]?)?)*?$").test(arr[i]);
                            
                            if (_inMonthValid || _inWeekhValid) {
                                var _not = arr[i][0]=='!';
                                arr[i] = _not?arr[i].split('!')[1]:arr[i];
                                var _patt = arr[i].split('&');
                                
                                if (_inMonthValid) {
                                    var _tmpObj = {};
                                    _patt.forEach(function(item) {
                                        var _params = item.split(/d[+]?/).map(function(str) {return Number(str);});
                                        _params[0] = _params[0]?_params[0]:1;
                                        _params[1]%=31;

                                        for (var k=0; k<31; k++) {
                                            if (_params[0]!=1 && k%_params[0] == _params[1] || _params[0]==1 && k==_params[1])
                                                _tmpObj[k] = 1;
                                        }
                                    });
                                    for (var k=0; k<31; k++) {
                                        if (_not) {
                                            if (!_tmpObj[k])
                                                _inMonth[k] = 1;
                                        }
                                        else {
                                            if (_tmpObj[k])
                                                _inMonth[k] = 1;
                                        }
                                    }
                                }
                                else if (_inWeekhValid) {
                                    var _tmpObj = {};
                                    _patt.forEach(function(item) {
                                        var _params = item.split(/i[+]?/).map(function(str) {return Number(str);});
                                        _params[1]%=7;
                                        _tmpObj[_params[1]] = 1;
                                    });
                                    for (var k=0; k<7; k++) {
                                        if (_not) {
                                            if (!_tmpObj[k])
                                                _inWeek[k] = 1;
                                        }
                                        else {
                                            if (_tmpObj[k])
                                                _inWeek[k] = 1;
                                        }
                                        
                                    }
                                }
                            }
                            else {
                                console.warn(arr[i] + " is not valid!");
                            }
                        }
                    }
                }
            }
            return {smart: smart, calType: calType, static: _static, inWeek: _inWeek, inMonth: _inMonth};
        }
        this.isDayDisable = function(calType, disabled, day) {
            if (disabled.static[day.unix])
                return true;
            
            var _gap = 0;
            
            if (disabled.smart) {
                if (disabled.calType=='gregorian' && calType=='jalali')
                    _gap = +1;
                else if (disabled.calType=='jalali' && calType=='gregorian')
                    _gap = -1;
            }
            else {
                if (disabled.calType=='gregorian' && calType=='jalali')
                    _gap = -1;
                else if (disabled.calType=='jalali' && calType=='gregorian')
                    _gap = +1;
            }
                
            
            var _dayName = (day.dayName + 7 + _gap)%7;
            
            if (disabled.inMonth[day.day-1])
                return true;
            
            return !!+disabled.inWeek[_dayName];
        }
        
        return {
            dateFormat: this.dateFormat,
            parseString: this.parseString,
            toRegularFormat: this.toRegularFormat,
            isDateEqual: this.isDateEqual,
            isDateBigger: this.isDateBigger,
            isMonthBigger: this.isMonthBigger,
            joinTime: this.joinTime,
            removeTime: this.removeTime,
            validateJalaliDateSeparate: this.validateJalaliDateSeparate,
            validateJalaliDate: this.validateJalaliDate,
            convertToUnix: this.convertToUnix,
            convertFromUnix: this.convertFromUnix,
            convertToJalali: this.convertToJalali,
            parseDisablePattern: this.parseDisablePattern,
            isDayDisable: this.isDayDisable,
            counter: 0
        }
    }
     var ADMdtpCalendarDirective = function(ADMdtp, ADMdtpConvertor, ADMdtpFactory, constants, $timeout) {

        return {
            restrict: 'E',
            replace: true,
            //require: '^^admDtp',
            link: function(scope, element, attrs) {

                var admDtp = scope.api;
                
                var _standValue;
                if (!scope.dtpValue.unix)
                    _standValue = new Date();                   
                else
                    _standValue = new Date(scope.dtpValue.fullDate);
                
                if (scope.calType == 'jalali')
                    _standValue = ADMdtpFactory.convertToJalali(_standValue);
                
                admDtp.fillDays(_standValue, !scope.option.transition);

                scope.previousMonth = function(flag) {
                    if (scope.calType == 'jalali' && !flag) {
                        scope.nextMonth(true);
                        return;
                    }
                    
                    if (scope.current.month == 1)
                        scope.current.month = 12, scope.current.year--;
                    else
                        scope.current.month--
                    admDtp.reload();
                }

                scope.nextMonth = function(flag) {
                    if (scope.calType == 'jalali' && !flag) {
                        scope.previousMonth(true);
                        return;
                    }
                        
                    if (scope.current.month == 12)
                        scope.current.month = 1, scope.current.year++;
                    else
                        scope.current.month++
                    admDtp.reload();
                }
                
                scope.previousYear = function(flag) {
                    if (scope.calType == 'jalali' && !flag) {
                        scope.nextYear(true);
                        return;
                    }

                    var _firstYear = scope.generatedYears.shift();
                    scope.generatedYears = [];
                    for (var i=1;i<17;i++) {
                        scope.generatedYears.push(_firstYear - 17 + i);
                    }
                }

                scope.nextYear = function(flag) {
                    if (scope.calType == 'jalali' && !flag) {
                        scope.previousYear(true);
                        return;
                    }

                    var _lastYear = scope.generatedYears.pop();
                    scope.generatedYears = [];
                    for (var i=1;i<17;i++) {
                        scope.generatedYears.push(_lastYear + i);
                    }
                }
                
                scope.selectMonthInit = function() {
                    scope.yearSelectStat = false;
                    scope.monthPickerStat = true;
                }
                
                scope.selectYearInit = function() {
                    scope.yearSelectStat = true;
                    scope.generatedYears = [];
                    for (var i=0;i<16;i++) {
                        scope.generatedYears.push(scope.current.year + i - 7);
                    }
                }
                
                scope.selectMonth = function(monthIdx) {
                    if (monthIdx+1 != scope.current.month) {
                        scope.current.month = monthIdx+1;
                        admDtp.reload();
                    }
                    scope.monthPickerStat = false;
                }
                
                scope.selectYear = function(yearName) {
                    if (yearName != scope.current.year) {
                        scope.current.year = yearName;
                        admDtp.reload();
                    }
                    scope.monthPickerStat = false;
                    //scope.yearSelectStat = false;
                }

                scope.selectThisDay = function(day) {
                    if (day.valid == 0)
                        return;
                    
                    scope.dtpValue.selected = false;
                    
                    admDtp.updateMasterValue(day, 'day');
                    
                    if (scope.option.autoClose) {
                        $timeout(function() {
                            scope.closeCalendar();
                        },100);
                        return;
                    }
                        
                    
                    if (day.disable) {
                        $timeout(function() {
                            if (ADMdtpFactory.isMonthBigger(day, scope.current))
                                scope.nextMonth(true);
                            else
                                scope.previousMonth(true);
                        }, 0);
                    } else
                        day.selected = true;
                }
                
                scope.today = function() {
                    var _standValue = new Date();

                    if (scope.calType == 'jalali')
                        _standValue = ADMdtpFactory.convertToJalali(_standValue);

                    admDtp.fillDays(_standValue, !scope.option.transition);
                }

                scope.changeTimeValue = function(variable, value) {
                    value *= (variable=='minute' ? scope.option.minuteStep : 1);
                    
                    var _num = (Number(scope.time[variable]) + value + ((variable=='hour')?24:60)) % ((variable=='hour')?24:60);
                    var _timeCopy = angular.copy(scope.time);
                    _timeCopy[variable] = _num.lZero();
                    
                    if (scope.dtpValue.unix) {
                        if (scope.minDate || scope.maxDate) {
                            var _dateTime = ADMdtpFactory.joinTime(scope.dtpValue.unix, _timeCopy);
                            if ((scope.minDate && !ADMdtpFactory.isDateBigger(_dateTime,scope.minDate)) || (scope.maxDate && !ADMdtpFactory.isDateBigger(scope.maxDate,_dateTime)))
                                return;
                        }
                    }
                    
                    scope.time[variable] = _num.lZero();
                    
                    
                    if (scope.dtpValue.unix)
                        admDtp.updateMasterValue(false, 'time');
                    
                    admDtp.reload();
                }
                
                scope.modelChanged = function(input) {
                    
                    var _value = (angular.isDefined(input) ? input : scope.dtpValue.formated);
                    
                    if (!_value) {
                        if (scope.dtpValue.unix)
                            scope.destroy();
                        return;
                    }
                    
                    var _inputUnix = ADMdtpFactory.convertToUnix(_value, scope.calType, scope.option.format);
                    if (!_inputUnix || scope.option.freezeInput || scope.disable || ((scope.minDate && !ADMdtpFactory.isDateBigger(_inputUnix,scope.minDate)) || (scope.maxDate && !ADMdtpFactory.isDateBigger(scope.maxDate,_inputUnix)))) {
                        admDtp.updateMasterValue(false);
                        return;
                    }
                        
                    if (_inputUnix == scope.fullData.unix)
                        return;
                    
                    scope.parseInputValue(_value, false, true);
                    
                    var _gDate = new Date(_inputUnix);
                    if (scope.calType == 'jalali')
                        _gDate = ADMdtpFactory.convertToJalali(_gDate);

                    admDtp.fillDays(_gDate, true);
                    
                }
                admDtp.modelChanged = scope.modelChanged;
                
                scope.calTypeChanged = function(calType) {
                    scope.calType = (calType ? calType : ((scope.calType=='gregorian')?'jalali':'gregorian'));
                    
                    scope.monthNames = scope.option[scope.calType + 'Dic'].monthsNames;
                    scope.daysNames = scope.option[scope.calType + 'Dic'].daysNames;
                    
                    var _cur = angular.copy(scope.current);
                    var _mainDate;
                    
                    if (scope.calType == 'jalali') {
                        _mainDate = ADMdtpConvertor.toJalali(_cur.year, _cur.month, 15);
                    }
                    else {
                        _mainDate = ADMdtpConvertor.toGregorian(_cur.year, _cur.month, 15);
                        _mainDate = [_mainDate.year, _mainDate.month, _mainDate.day].dtp_toDate('date');
                    }
                    
                    if (scope.dtpValue.unix) {
                        admDtp.updateMasterValue(ADMdtpFactory.convertFromUnix(scope.dtpValue.unix, scope.calType));
                    }
                    
                    admDtp.fillDays(_mainDate, true);
                    
                }

                
            },
            //templateUrl: 'js/ADM-dateTimePicker/ADM-dateTimePicker_calendar.html'
            template: '<div class="ADMdtp-box ADMdtp-calendar-container" ng-class="{rtl: (calType==\'jalali\'), square: monthPickerStat||timePickerStat}"> <div class="dtpNewBox" ng-class="{active: monthPickerStat}"> <i class="calendarIcon" ng-class="{show: monthPickerStat}" ng-click="monthPickerStat = false"> <svg class="dtp-i" viewBox="0 0 24 24"> <use xlink:href="#dtp-i-calendar" /> </svg> </i> <div class="content"> <div class="ADMdtpMonths" ng-class="{onYear: yearSelectStat, rtl: (calType==\'jalali\')}"> <div class="ADMdtpYears"> <svg class="dtp-i dtp-i-180 dtp-trs-3 arrow left" viewBox="0 0 24 24" ng-if="yearSelectStat" ng-click="previousYear()"> <use xlink:href="#dtp-i-right" /> </svg> <p class="dtp-trs-3" ng-click="selectYearInit()">{{current.year | digitType:calType}}</p> <svg class="dtp-i dtp-trs-3 arrow right" viewBox="0 0 24 24" ng-if="yearSelectStat" ng-click="nextYear()"> <use xlink:href="#dtp-i-right" /> </svg> </div> <span ng-repeat="yearName in generatedYears" ng-if="yearSelectStat"><span class="dtp-trs-5" ng-class="{selected: yearName==current.year}" ng-click="selectYear(yearName)">{{yearName | digitType:calType}}</span></span> <span ng-repeat="monthName in monthNames" ng-if="!yearSelectStat"><span class="dtp-trs-5" ng-class="{selected: monthName==current.monthDscr}" ng-click="selectMonth($index)">{{monthName}}</span></span> </div> </div> </div> <div class="dtpNewBox" ng-class="{active: timePickerStat}"> <i class="calendarIcon" ng-class="{show: timePickerStat}" ng-click="timePickerStat = false"> <svg class="dtp-i" viewBox="0 0 24 24"> <use xlink:href="#dtp-i-calendar" /> </svg> </i> <div class="content"> <div class="ADMdtpTime"> <span class="dtpIcon null up" ng-click="changeTimeValue(\'hour\', 1)"><svg class="dtp-i dtp-trs-5 dtp-i-270" viewBox="0 0 24 24"><use xlink:href="#dtp-i-right" /></svg></span><!-- --><span></span><!-- --><span class="dtpIcon null up" ng-click="changeTimeValue(\'minute\', 1)"><svg class="dtp-i dtp-trs-5 dtp-i-270" viewBox="0 0 24 24"><use xlink:href="#dtp-i-right" /></svg></span><!-- --><span>{{time.hour}}</span><!-- --><span class="period">:</span><!-- --><span>{{time.minute}}</span><!-- --><span class="dtpIcon null down" ng-click="changeTimeValue(\'hour\', -1)"><svg class="dtp-i dtp-trs-5 dtp-i-90" viewBox="0 0 24 24"><use xlink:href="#dtp-i-right" /></svg></span><!-- --><span></span><!-- --><span class="dtpIcon null down" ng-click="changeTimeValue(\'minute\', -1)"><svg class="dtp-i dtp-trs-5 dtp-i-90" viewBox="0 0 24 24"><use xlink:href="#dtp-i-right" /></svg></span> </div> </div> </div> <header> <svg class="dtp-i dtp-i-180 dtp-trs-3 arrow left" viewBox="0 0 24 24" ng-click="previousMonth()"> <use xlink:href="#dtp-i-right" /> </svg> <span class="yearMonth" ng-click="selectMonthInit()">{{current.monthDscr}} {{current.year | digitType:calType}}</span> <svg class="dtp-i dtp-trs-3 arrow right" viewBox="0 0 24 24" ng-click="nextMonth()"> <use xlink:href="#dtp-i-right" /> </svg> </header> <div class="daysNames"> <span ng-repeat="dayName in daysNames">{{dayName}}</span> </div> <hr> <div class="ADMdtpDays" ng-class="{loading:loadingDays}"> <span ng-repeat="day in current.days" ng-click="selectThisDay(day)"><span ng-class="[{disable: day.disable||!day.valid, today: day.today, selected: day.selected, valid:(day.valid==2)}, (day.isMin)?((calType==\'jalali\')?\'max\':\'min\'):\'\', (day.isMax)?((calType==\'jalali\')?\'min\':\'max\'):\'\']">{{day.day | digitType:calType}}</span></span> </div> <hr> <footer> <div class="calTypeContainer dtp-trs-3" ng-class="$parent.calType" ng-click="calTypeChanged()" ng-if="option.multiple"> <p class="gregorian">{{option.gregorianDic.title}}</p> <p class="jalali">{{option.jalaliDic.title}}</p> </div> <button type="button" class="today dtp-trs-3" ng-click="today()">{{option[calType + "Dic"].todayBtn}}</button> <svg class="dtp-i dtp-trs-5 timeSelectIcon" viewBox="0 0 24 24" ng-show="option.dtpType != \'date\'" ng-click="timePickerStat = !timePickerStat"> <use xlink:href="#dtp-i-clock" /> </svg> </footer> </div>'
        }
    }
    var ADMdtpDirective = function(ADMdtp, ADMdtpConvertor, ADMdtpFactory, constants, $compile, $timeout) {

        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            require: ['ngModel', 'admDtp'],
            scope: {
                options: '=?',
                fullData: '=?',
                name: '=?',
                ngRequired: '=?',
                onChange: '&?',
                onDatechange: '&?',
                onTimechange: '&?',
                onOpen: '&?',
                onClose: '&?',
            },
            link: function(scope, element, attrs, ctrls) {
                var ngModel = ctrls[0], admDtp = ctrls[1];
                scope.api = admDtp;
                scope.dtpId = 'adm-' + (++ADMdtpFactory.counter);
                
                if (!element.find('ng-transclude').children().length) {
                    scope.defaultTemplate = true;
                    element.find('ng-transclude').remove();
                }
                
                var _options = scope.options;
                if (!(_options instanceof Object))
                    _options = {};
                scope.option = angular.merge(angular.copy(ADMdtp.getOptions()), _options);
                scope.option.minuteStep = Math.max(Math.min(scope.option.minuteStep, 60), 1);
                var dayNames = angular.copy(scope.option.gregorianDic.daysNames);
                scope.option.gregorianDic.daysNamesUntouched = dayNames;
                scope.option.gregorianDic.daysNames = dayNames.slice(scope.option.gregorianStartDay,7).concat(dayNames.slice(0,scope.option.gregorianStartDay));
                
                scope.disableDays = ADMdtpFactory.parseDisablePattern(scope.option);
                scope.calType = scope.option.calType;
                scope.monthNames = scope.option[scope.calType + 'Dic'].monthsNames;
                scope.daysNames = scope.option[scope.calType + 'Dic'].daysNames;
                scope.timeoutValue = [0,0];
                scope.dtpValue = {};

                scope.minDate = scope.mindate?new Date(scope.mindate):null;
                scope.maxDate = scope.maxdate?new Date(scope.maxdate):null;
                
                scope.current = {
                    year: '',
                    month: '',
                    monthDscr: '',
                    days: []
                };
                

                scope.updateMasterValue = function(newDate, releaseTheBeast) {
                    if (!newDate)
                        newDate = (scope.dtpValue.unix ? scope.dtpValue : {});
                        

                    scope.$applyAsync(function() {
                        scope.dtpValue = newDate;
                        var minute = Number(scope.time.minute);
                        minute -= minute % scope.option.minuteStep;
                        scope.time.minute = minute.lZero();
                        scope.dtpValue.formated = ADMdtpFactory.dateFormat(newDate, scope.time, scope.option.format);
                        scope.dtpValue.fullDate = ADMdtpFactory.joinTime(newDate.unix, scope.time);
                        scope.fullData = {
                            formated: scope.dtpValue.formated,
                            lDate: scope.dtpValue.fullDate.dtp_shortDate(),
                            gDate: scope.dtpValue.fullDate,
                            unix: scope.dtpValue.fullDate.getTime(),
                            year: newDate.year,
                            month: newDate.month,
                            day: newDate.day,
                            hour: Number(scope.time.hour),
                            minute: Number(scope.time.minute),
                            minDate: scope.minDate,
                            maxDate: scope.maxDate,
                            calType: scope.calType,
                            format: scope.option.format
                        }

                        ngModel.$setViewValue( scope.dtpValue.formated );
                        ngModel.$render();
                            
                        if (scope.hasInputDtp)
                            element[0].querySelector('[dtp-input]').value = scope.dtpValue.formated;

                        if (releaseTheBeast) {
                            if (scope.onChange)
                                scope.onChange({date:scope.fullData});
                            if (releaseTheBeast == 'time' && scope.onTimechange)
                                scope.onTimechange({date:scope.fullData});
                            else if (releaseTheBeast == 'day' && scope.onDatechange)
                                scope.onDatechange({date:scope.fullData});
                        }

                    });
                }
                
                scope.parseInputValue = function(valueStr, resetTime, releaseTheBeast) {
                    if (valueStr == 'today') {
                        valueStr = ADMdtpFactory.removeTime(new Date()).getTime();
                    }
                    
                    var _dateTime = false;

                    if (valueStr) {

                        if (scope.calType == 'jalali') {
                            _dateTime = ADMdtpFactory.validateJalaliDate(valueStr, scope.option.format);
                        }
                        else {
                            if (typeof valueStr == "string")
                                valueStr = ADMdtpFactory.toRegularFormat(valueStr, scope.calType, scope.option.format);
                                
                            _dateTime = new Date(valueStr);
                            _dateTime = (/invalid/i.test(_dateTime))?false:_dateTime;
                        }
                    }

                    if (_dateTime) {
                        scope.dtpValue = {
                            year: _dateTime.year || _dateTime.getFullYear(),
                            month: _dateTime.month || _dateTime.getMonth()+1,
                            day: _dateTime.day || _dateTime.getDate(),
                            unix: _dateTime.unix || _dateTime.getTime(),
                            fullDate: _dateTime.gDate || _dateTime
                        }
                        
                        scope.dtpValue.fullDate = ADMdtpFactory.removeTime(scope.dtpValue.fullDate);
                        scope.dtpValue.unix = scope.dtpValue.fullDate.getTime();

                        scope.time = {
                            hour: ( _dateTime.getHours?_dateTime.getHours():_dateTime.hour ).lZero(),
                            minute: ( _dateTime.getMinutes?_dateTime.getMinutes():_dateTime.minute ).lZero()
                        }

                        scope.updateMasterValue(false, releaseTheBeast);
                    }
                    else {
                        if (resetTime)
                            scope.time = {
                                hour: '00',
                                minute: '00'
                            }
                        }
                }
                scope.parseInputValue(ngModel.$viewValue || scope.option.default, true, false);
                
                ngModel.$formatters.push(function (val) {
                    if (!val && scope.dtpValue.unix) {
                        scope.destroy();
                    }
                    else if (scope.dtpValue && val == scope.dtpValue.formated) {
                    }
                    else {
                        scope.parseInputValue(val, false, true);
                    }

                    return val;
                });
                
                if (scope.option.watchingOptions) {
                    //return;
                    scope.$watch('options', function(nuVal, old) {
                        if (!nuVal || typeof nuVal != 'object') return;
                        if (old && JSON.stringify(old) == JSON.stringify(nuVal)) return;
                        
                        var daysNamesUntouched = scope.option.gregorianDic.daysNamesUntouched;
                        scope.option = angular.merge(angular.copy(ADMdtp.getOptions()), nuVal);
                        scope.option.minuteStep = Math.max(Math.min(scope.option.minuteStep, 60), 1);
                        
                        if (nuVal.gregorianDic && nuVal.gregorianDic.daysNames)
                            scope.option.gregorianDic.daysNamesUntouched = nuVal.gregorianDic.daysNames;
                        else
                            scope.option.gregorianDic.daysNamesUntouched = daysNamesUntouched;
                        
                        var dayNames = angular.copy(scope.option.gregorianDic.daysNamesUntouched);
                        scope.option.gregorianDic.daysNames = dayNames.slice(scope.option.gregorianStartDay,7).concat(dayNames.slice(0,scope.option.gregorianStartDay));

                        scope.disableDays = ADMdtpFactory.parseDisablePattern(scope.option);
                        if (scope.calTypeChanged) scope.calTypeChanged(scope.option.calType);
                    }, true);
                }                
                
                attrs.$observe("disable", function (_newVal) {
                    scope.$applyAsync(function() {
                        _newVal = scope.$eval(_newVal);
                        scope.disable = _newVal;
                    });
                });
                
                attrs.$observe("mindate", function (_newVal) {
                    scope.$applyAsync(function() {
                        _newVal = scope.$eval(_newVal);
                        scope.minDate = ADMdtpFactory.convertToUnix(_newVal, scope.calType, scope.option.format);
                    });
                });
                
                attrs.$observe("maxdate", function (_newVal) {
                    scope.$applyAsync(function() {
                        _newVal = scope.$eval(_newVal);
                        scope.maxDate = ADMdtpFactory.convertToUnix(_newVal, scope.calType, scope.option.format);
                    });
                }); 

                scope.onKeydown = function(e) {
                    if (e.keyCode == 9)
                        scope.closeCalendar();
                }
                
                scope.openCalendar = function() {
                    if (scope.showCalendarStat || scope.disable)
                        return;
                    
                    scope.timeoutValue[0] = 0;
                    scope.showCalendarStat = true;
                    
                    var _admDtpCalendarHtml = angular.element('<adm-dtp-calendar id="'+ scope.dtpId +'" style="opacity:0; z-index: '+ scope.option.zIndex +';"></adm-dtp-calendar>');
                    angular.element(document.body).append(_admDtpCalendarHtml);

                    scope.$applyAsync(function () {
                        $compile(_admDtpCalendarHtml)(scope);
                    });
                    
                    $timeout(function() {
                        var top = document.documentElement.scrollTop || document.body.scrollTop;
                        var popup = document.getElementById(scope.dtpId);
                        var popupBound = popup.getBoundingClientRect();
                        var _input = element.children().children()[0];
                        var _inputBound = _input.getBoundingClientRect();
                        var _corner = {
                            x: _inputBound.left,
                            y: _inputBound.top + _inputBound.height
                        }

                        var _totalSize = {
                            width: popupBound.width + _corner.x,
                            height: popupBound.height + _corner.y
                        }
                        
                        var _pos = {
                            top: '',
                            bottom: '',
                            left: '',
                            right: ''
                        }
                        if (_totalSize.height > window.innerHeight)
                            _pos.top = (top + _inputBound.top - popupBound.height) + 'px';
                        else
                            _pos.top = (top + _inputBound.top + _inputBound.height) + 'px';
                        
                        if (_totalSize.width > window.innerWidth)
                            _pos.left = (_corner.x + window.innerWidth - _totalSize.width - 20) + 'px';
                        else
                            _pos.left = _corner.x + 'px';
                        
                        angular.element(popup).css({top: _pos.top, bottom: _pos.bottom, left: _pos.left, opacity: 1});
                        
                    }, 70);
                    
                    if (scope.onOpen)
                        scope.onOpen();
                }
                
                scope.closeCalendar = function() {
                    if (!scope.showCalendarStat)
                        return;
                    
                    scope.$applyAsync(function() {
                        scope.monthPickerStat = false;
                        scope.timePickerStat = false;
                        scope.showCalendarStat = false;
                    });
                
                    var popup = document.getElementById(scope.dtpId);
                    if (popup) {
                        angular.element(popup).remove();
                        
                        if (scope.onClose)
                            scope.onClose();
                    }
                    
                }
                
                scope.toggleCalendar = function() {
                    if (scope.showCalendarStat)
                        scope.closeCalendar();
                    else
                        scope.openCalendar();
                }

                scope.destroy = function(noRefresh) {
                    if (scope.disable)
                        return;
                    
                    scope.monthPickerStat = false;
                    scope.timePickerStat = false;
                    
                    scope.current = {
                        year: '',
                        month: '',
                        monthDscr: '',
                        days: []
                    };
                    scope.dtpValue = {};
                    scope.fullData = {
                        minDate: scope.minDate,
                        maxDate: scope.maxDate
                    }
                    scope.time = {
                        hour: '00',
                        minute: '00'
                    }
                    var _standValue = new Date();                   

                    if (scope.calType == 'jalali')
                        _standValue = ADMdtpFactory.convertToJalali(_standValue);

                    ngModel.$setViewValue('');
                    ngModel.$render();
                    
                    if (!noRefresh)
                        admDtp.fillDays(_standValue, !scope.option.transition);
                    
                    if (scope.onChange)
                        scope.onChange({date:scope.fullData});
                }
                                
                var dtpOpen = element[0].querySelector('[dtp-open]') || {};
                dtpOpen.onclick = scope.openCalendar;
                
                var dtpClose = element[0].querySelector('[dtp-close]') || {};
                dtpClose.onclick = scope.closeCalendar;

                var dtpToggle = element[0].querySelector('[dtp-toggle]') || {};
                dtpToggle.onclick = scope.toggleCalendar;
                
                var dtpDestroy = element[0].querySelector('[dtp-destroy]') || {};
                dtpDestroy.onclick = scope.destroy;
            },
            controller: ['$scope', function($scope) {

                this.updateMasterValue = function(newDate, releaseTheBeast) {
                    $scope.updateMasterValue(newDate, releaseTheBeast);
                }

                this.fillDays = function(date, noTransition) {

                    if (noTransition)
                        $scope.timeoutValue[0] = 0;
                    else
                        $scope.loadingDays = true;


                    var _mainDate = angular.copy(date);

                    if ($scope.calType == 'jalali') {
                        var _gDate = ADMdtpConvertor.toGregorian(date.year, date.month, 29);
                        date = [_gDate.year, _gDate.month, _gDate.day].dtp_toDate('date');
                    }

                    var _input = {
                        year: date.getFullYear(),
                        month: date.getMonth()+1,
                        day: date.getDate()
                    }

                    $scope.$applyAsync(function() {
                        var _month = _mainDate.month || (_mainDate.getMonth()+1);
                        angular.merge($scope.current, {
                            year: _mainDate.year || _mainDate.getFullYear(),
                            month: _month,
                            monthDscr: $scope.monthNames[_month-1]
                        });
                    });

                    var startDay, shift = startDay = $scope.option.gregorianStartDay;

                    if ($scope.calType == 'jalali')
                        shift = 0, startDay = 6;

                    var _today = new Date();
                    _today = [_today.getFullYear(), _today.getMonth()+1, _today.getDate(), 1, 0].dtp_toDate('unix');

                    var _selected = ($scope.dtpValue.unix || -1), _selectedIdx;

                    var _currDay = [_input.year, _input.month, _input.day, 1, 0].dtp_toDate('date');
                    var _firstDayName = new Date(angular.copy(_currDay).setDate(1)).getDay();

                    var _days = [];

                    var _diff = -1 * (_firstDayName - shift + 7) % 7,
                        _ite_date, _disable = true;
                    var _lastValidStat = -1;

                    if ($scope.calType == 'jalali') {
                        var _ite_date = new Date(angular.copy(_currDay).setDate(_diff));
                        var _pDate = ADMdtpConvertor.toJalali(_ite_date.getFullYear(), _ite_date.getMonth()+1, _ite_date.getDate());
                        _diff -= (Math.ceil((_pDate.day-1)/7)*7 + 1);
                    }


                    while (true) {
                        _diff++;
                        var _ite_date = new Date(angular.copy(_currDay).setDate(_diff));
                        var _pDate = false;

                        if ($scope.calType == 'jalali') {
                            _pDate = ADMdtpConvertor.toJalali(_ite_date.getFullYear(), _ite_date.getMonth()+1, _ite_date.getDate());
                        }

                        var _thisDay = _pDate.day || _ite_date.getDate();

                        if (_thisDay == 1)
                            _disable = !_disable;

                        if (_disable && _thisDay < 8 && _ite_date.getDay() == startDay)
                            break;


                        var _isMin = false;
                        var _valid = 1;
                        if ($scope.minDate || $scope.maxDate) {
                            var _dateTime = ADMdtpFactory.joinTime(_ite_date, $scope.time);
                            if (($scope.minDate && !ADMdtpFactory.isDateBigger(_dateTime,$scope.minDate)) || ($scope.maxDate && !ADMdtpFactory.isDateBigger($scope.maxDate,_dateTime))) {
                                _valid = 0;

                                if (_lastValidStat == 2)
                                    _days[_days.length-1].isMax = true;
                            }
                            else {
                                _valid = 2;

                                if (_lastValidStat == 0)
                                    _isMin = true;
                            }
                            _lastValidStat = _valid;
                        }

                        var _unix = _ite_date.getTime();
                        var _dayName = _ite_date.getDay() + (($scope.calType=='jalali')?1:0);
                        
                        var _day = {
                            day: _thisDay,
                            month: _pDate.month || _ite_date.getMonth()+1,
                            year: _pDate.year || _ite_date.getFullYear(),
                            dayName: _dayName,
                            fullDate: _ite_date,
                            disable: _disable,
                            today: (_unix == _today),
                            selected: (_unix == _selected),
                            unix: _unix,
                            valid: _valid,
                            isMin: _isMin
                        }

                        if (ADMdtpFactory.isDayDisable($scope.calType, $scope.disableDays, _day))
                            _day.valid = 0;

                        if (_day.selected)
                            _selectedIdx = _days.length;

                        _days.push(_day);
                    }

                    $timeout(function() {

                        $scope.timeoutValue[0] = 500;

                        $scope.$applyAsync(function() {
                            $scope.current.days = _days;
                            if (_selectedIdx)
                                $scope.updateMasterValue($scope.current.days[_selectedIdx]);
                            $timeout(function() {
                                $scope.loadingDays = false;
                            }, $scope.timeoutValue[1]);
                        });

                    }, $scope.timeoutValue[0]);
                }

                this.reload = function() {
                    var _cur = angular.copy($scope.current);
                    _cur.day = 29;
                    var _date = [_cur.year, _cur.month, 8].dtp_toDate('date');
                    if ($scope.calType == 'jalali')
                        _date = _cur;
                    this.fillDays(_date, !$scope.option.transition);
                }

                this.vm = $scope;
            }],
            //templateUrl: 'js/ADM-dateTimePicker/ADM-dateTimePicker_view.html'
            template: '<div class="ADMdtp ADMdtp-container" ng-class="{rtl: (calType==\'jalali\'), touch: option.isDeviceTouch, disable: disable}"> <div class="clickOutContainer" click-out="closeCalendar()"  alias="{{dtpId}}"> <ng-transclude></ng-transclude> <div ng-if="defaultTemplate" class="ADMdtpInput masterInput" ng-class="{touch: option.isDeviceTouch, disable: disable, open: showCalendarStat}"> <input type="text" name="{{name}}" ng-model="dtpValue.formated" ng-focus="openCalendar()" ng-click="openCalendar()" ng-readonly="option.freezeInput || option.isDeviceTouch || disable" ng-blur="modelChanged()" ng-keydown="onKeydown($event)" ng-required="ngRequired" > <div class="dtp-ig" ng-click="toggleCalendar()"> <svg class="dtp-i fakeIcon" viewBox="0 0 24 24"> <use xlink:href="#dtp-i-right" /> </svg> <svg class="dtp-i calendarIcon" viewBox="0 0 24 24"> <use xlink:href="#dtp-i-calendar" /> </svg> <svg class="dtp-i closeIcon" viewBox="0 0 24 24"> <use xlink:href="#dtp-i-off" /> </svg> </div> <svg class="removeIcon" viewBox="0 0 24 24" ng-if="dtpValue.formated" ng-click="destroy()"> <use xlink:href="#dtp-i-close" /> </svg> </div> </div> <svg style="display:none;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"> <defs> <g id="dtp-i-calendar"> <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z"/> <path d="M0 0h24v24H0z" fill="none"/> </g> <g id="dtp-i-clock"> <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/> <path d="M0 0h24v24H0z" fill="none"/> <path d="M12.5 7H11v6l5.25 3.15.75-1.23-4.5-2.67z"/> </g> <g id="dtp-i-right"> <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"/> <path d="M0 0h24v24H0z" fill="none"/> </g> <g id="dtp-i-close"> <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/> <path d="M0 0h24v24H0z" fill="none"/> </g> <g id="dtp-i-off"> <path d="M0 0h24v24H0z" fill="none"/> <path d="M13 3h-2v10h2V3zm4.83 2.17l-1.42 1.42C17.99 7.86 19 9.81 19 12c0 3.87-3.13 7-7 7s-7-3.13-7-7c0-2.19 1.01-4.14 2.58-5.42L6.17 5.17C4.23 6.82 3 9.26 3 12c0 4.97 4.03 9 9 9s9-4.03 9-9c0-2.74-1.23-5.18-3.17-6.83z"/> </g> </defs> </svg> </div>'
        };
    }
    var dtpInputDirective = function() {
        return {
            require: ['^^admDtp', 'ngModel'],
            link: function(scope, element, attrs, ctrls) {
                var admDtp = ctrls[0], ngModel = ctrls[1];

                ngModel.$parsers.push(function() {
                    return ngModel.$modelValue;
                })

                admDtp.vm.hasInputDtp = true;

                element.on('focus', function() {
                    admDtp.vm.openCalendar();
                });
                element.on('blur', function() {
                    admDtp.vm.modelChanged(element[0].value);
                });
                
            }
        }
    }
     /* https://github.com/IamAdamJowett/angular-click-outside */
    var clickOutside = function($document) {
        return {
            restrict: 'A',
            scope: {
                clickOut: '&'
            },
            link: function ($scope, elem, attr) {
                if (attr.id == undefined) attr.$set('id', 'id_' + Math.random());

                $document.on('click contextmenu', function (e) {
                    var i = 0,
                        element;

                    if (!e.target) return;
                    
                    var classList = (attr.alias !== undefined) ? attr.alias.replace(', ', ',').split(',') : [];
                    classList.push(attr.id);

                    for (element = e.target; element; element = element.parentNode) {
                        var id = element.id;
                        var classNames = element.className;

                        if (id !== undefined) {
                            for (i = 0; i < classList.length; i++) {
                                if (id.indexOf(classList[i]) > -1 || (typeof classNames == 'string' && classNames.indexOf(classList[i]) > -1)) {
                                    return;
                                }
                            }
                        }
                    }

                    $scope.$eval($scope.clickOut);
                });
            }
        };
    }
     var ADMdtpConfig = function(ADMdtp) {
        
        ADMdtp.setOptions({isDeviceTouch: ('ontouchstart' in window || navigator.maxTouchPoints)});
        
        var style = document.createElement('style');
        style.type = 'text/css';
        
        var vendor = function(css) {
            return '-moz-' + css + '-o-' + css + '-webkit-' + css + css;
        }
        
        for (var i=1; i<51; i++)
            style.innerHTML += '.ADMdtpDays>span:nth-child('+ i +')>span {'+ vendor('transition: all .5s, transform 0.2s '+ i*.01 +'s cubic-bezier(0.680, -0.550, 0.265, 1.550); ') +'}';

        document.getElementsByTagName('head')[0].appendChild(style);
        
    }
//    //constant
//    angular.module('isaadmDatePicker', [])
//        .constant('constants', {});
//
//    //provider
//    angular.module('ADM-dateTimePicker', [])
//        .provider('ADMdtp', ADMdtpProvider);
//    
//  angular.module('ADM-dateTimePicker', [])
//        .filter('digitType', [ADMdtpDigitTypeFilter])
    angular.module('admDatePicker', [])
        .constant('constants', {})
        .provider('ADMdtp', ADMdtpProvider)
        .filter('digitType', [ADMdtpDigitTypeFilter])
        .factory('ADMdtpConvertor', [ADMdtpConvertor])
        .factory('ADMdtpFactory', ['ADMdtpConvertor', ADMdtpFactory])
        .directive('admDtp', ['ADMdtp', 'ADMdtpConvertor', 'ADMdtpFactory', 'constants', '$compile', '$timeout', ADMdtpDirective])
        .directive('admDtpCalendar', ['ADMdtp', 'ADMdtpConvertor', 'ADMdtpFactory', 'constants', '$timeout', ADMdtpCalendarDirective])
        .directive('dtpInput', [dtpInputDirective])
        .directive('clickOut', ['$document', clickOutside])
        .config(['ADMdtpProvider', ADMdtpConfig]);
   
})();
;/**
 * Temporary option list for the Hala services
 * TODO: Integrate into the option_list directive
 */
'use strict';

(function () {
    var airport_transfer_list = angular.module("airportTransferList", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.airportTransferList = [
        '$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                availableOptions: '=',
                addTransfer: '&',
                removeTransfer: '&',
                source: '=',
                selectedItems: '=',
                passenger: '=',
                validations: '=',
                currentSelection: '=',
                index:'='
            };

            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    var template = $templateCache.get(isaconfig.getTemplates("airportTransferList")['default']);
                    var DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss';

                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);
                    scope.comment = {};
                    scope.isModify = false;
                    scope.datePicker = {
                        isOpenDp : false
                    };
                    scope.dateOptions = {
                        maxDate: null,
                        minDate: null
                    };

                    scope.getCharge = function(item){
                        var currentCharge = 0;
                        var infantCharge = 0;

                        _.forEach(item.charges, function(charge){
                            if (charge.chargeBasis.toLowerCase() == scope.passenger.type){
                                currentCharge = charge.amount;
                            }

                            if(charge.chargeBasis == 'ADULT_WITH_INFANT'){
                                infantCharge = charge.amount;
                            }
                        });

                        if(scope.passenger.infantSequence){
                            currentCharge = infantCharge;
                        }
                        item.currentCharge = currentCharge;
                        return currentCharge;
                    };

                    scope.setSelected = function(index, item){
                        setValidations(item);
                        scope.selectedItem = index;
                    };

                    scope.collapse = function(){
                        scope.selectedItem = null;
                        scope.isModify = false;
                    };

                    scope.isItemSelected = function(item){
                        var isSelected = false;
                        _.forEach(scope.currentSelection.selectedItems, function(val){
                            if(val.airportTransferId === item.airportTransferId){
                                isSelected = true;
                            }
                        });
                        return isSelected;
                    };

                    scope.addAirportTransfer = function(item){
                        scope.selectedItem = null;
                        scope.isModify = false;
                        scope.addTransfer({item: item, comment: scope.comment, dateValidations: scope.dateOptions});
                    };

                    scope.removeAirportTransfer = function(item){
                        scope.removeTransfer({item: item});
                        scope.comment = {};
                    };

                    scope.modifyAirportTransfer = function(item, index){
                        scope.isModify = true;

                        var selectedItemInput = {};
                        _.forEach(scope.currentSelection.selectedItems, function(val){
                            if(val.airportTransferId === item.airportTransferId){
                                selectedItemInput = val.input;
                            }
                        });

                        scope.comment = selectedItemInput;
                        scope.comment.transferTime = moment(selectedItemInput.tranferDate, DATE_TIME_FORMAT).toDate();
                        scope.comment.transferDate = moment(selectedItemInput.tranferDate, DATE_TIME_FORMAT).toDate();

                        scope.setSelected(index, item);
                    };

                    scope.openDp = function(){
                        scope.datePicker.isOpenDp = true;
                    };

                    scope.$watch('currentSelection', function() {
                        scope.comment = {};
                        scope.collapse();
                    });

                    function setValidations(item){
                        var dateValidations = {};

                        _.forEach(item.validations, function(valId){
                            dateValidations = _.find(scope.validations, function(validationRule){
                                return ((validationRule.ruleValidationId == valId) && (validationRule.ruleCode == 'ibe.ancillary.rule.calander.date.restrict'));
                            });
                        });

                        if(dateValidations.maxDate){
                            scope.maxDate = new Date(moment(dateValidations.maxDate).format());
                        }else{
                            scope.maxDate = null;
                        }

                        if(dateValidations.minDate){
                            scope.minDate = new Date(moment(dateValidations.minDate).format());
                        }else{
                            scope.minDate = null;
                        }

                        scope.dateOptions.minDate = scope.minDate;
                        scope.dateOptions.maxDate = scope.maxDate;
                    }
                };
            return directive;
        }
    ];

    airport_transfer_list.directive(directives);
})();

;'use strict';

(function () {
    var isa_attr_placeholder = angular.module("isaAttrPlaceholder", []);
    var directives = {};
    var isaConfig = new IsaModuleConfig();

    directives.isaattrplaceholder = ['$compile', '$http', '$templateCache', function ($compile, $http, $templateCache) {
        var directive = {};
        directive.restrict = 'AEC';
        directive.scope = {
            placeholderCaption: '@',
            placeholderSymbol: '@',
            isVisible: '=',
            isInputDisabled: '=',
            onChange: '=',
            bindTo: '@'
        };
        directive.require = 'ngModel';
        directive.link = ['scope', 'element', 'attr', 'ngModel', function (scope, element, attr, ngModel) {
            scope.onChange = "";
            scope.modelVal = null;

            var template = $templateCache.get(isaConfig.getTemplates("isaAttrPlaceholder")['default']);

            template = angular.element(template);
            element.after(template);
            $compile(template)(scope);

            scope.removePlaceholder = function () {
                if (scope.isInputDisabled) {
                    scope.isVisible = true;
                    element.focus();
                    $(element).siblings('.placeholder').hide();
                }
            };

            //Hides placeholder without getting focus
            scope.hidePlaceholder = function () {
                scope.isVisible = true;
                $(element).siblings('.placeholder').hide();
            };

            element.on('blur', function (e) {
                if ((element.val().length === 0)) {
                    scope.isVisible = true;
                    $(element).siblings('.placeholder').show();
                    $('[bind-to = ' + scope.bindTo + ']').find('.placeholder').show();
                }
            });

            element.on('change paste keyup focus', function (e) {
                scope.isVisible = false;
                $(element).siblings('.placeholder').hide();
                $('[bind-to = ' + scope.bindTo + ']').siblings('.placeholder').hide();
            });

            //Watches a change in the ngModel and hides the placeholder if the model is not empty
            scope.$watch(function () {
                    return ngModel.$modelValue;
                },
                function () {
                    if (ngModel.$modelValue) {
                        scope.hidePlaceholder();
                    }
                });
        }];
        return directive;
    }];
    isa_attr_placeholder.directive(directives);
})();;'use strict';

(function () {
    var isa_auto_checkin = angular.module("isaAutoCheckin", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaAutoCheckin = [
        '$compile',
        '$http',
        '$state',
        '$templateCache',
        'constantFactory',
        'modifyReservationService',
        'languageService',
        'currencyService',
        'applicationService',
        'commonLocalStorageService',
        '$translate',
        'dateTransformationService',
        '$sce',
        'formValidatorService',
        function ($compile, $http, $state, $templateCache, constantFactory, modifyReservationService, languageService,
                  currencyService, applicationService, commonLocalStorageService, $translate,dateTransformationService,$sce,formValidatorService) {
            var directive = {};

            directive.restrict = 'E';

            directive.scope = {
                applicationMode: '@',
                passenger: '=',
                selectedPassenger: '=',               
                passengerList: '=',                
                updateContact: '&'               
            };

            directive.link = function (scope, element) {
                //Setting passenger detail template
                var template = $templateCache.get(isaconfig.getTemplates("isaAutoCheckin")['default']);

                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
            };
            
            directive.controller = [
                '$scope', '$element', '$rootScope', 'datePickerService', '$location', '$anchorScroll', '$timeout', '$filter','$translate','formValidatorService',
                function ($scope, $element, $rootScope, datePickerService, $location, $anchorScroll, $timeout, $filter,$translate, formValidatorService) {
                                 

                    $scope.requiredRules = {};
                    //GET FORM VALIDATIONS
                    $scope.paxValidation = formValidatorService.getValidations();
                    
                    //variables
                    $scope.dobInput = '';
                    $scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                    $scope.isLargeDevice = $rootScope.deviceType.isLargeDevice;
                    $scope.appMode =  applicationService.getApplicationMode();
                    $scope.isNameChange = ($scope.applicationMode === 'nameChange');
                    $scope.nationalityList = constantFactory.Nationalities();
                    $scope.countryList = constantFactory.Countries();
                    $scope.isPersianCalendarType = applicationService.getIsPersianCalendar();
                    $scope.passengerIndex = 0;
                    $scope.dob = $scope.passengerList[0].dob;
                    
                    $scope.changeSelected = function ($index){                     	
                    	$scope.passengerIndex = $index;                  
                    	$scope.passportNo = $scope.passengerList[$index].passportNo;
                    	$scope.passportExpiry = $scope.passengerList[$index].passportExpiry;
                    	$scope.passportIssuedCntry = $scope.passengerList[$index].passportIssuedCntry;
                    	$scope.dob = $scope.passengerList[$index].dob;
                    	$scope.email = $scope.passengerList[$index].email;
                    }
                    
                                    
                    $scope.passengerDetailsFilled = []; 
                    
                    $scope.nextPassenger = function(){                    	
                    	$scope.nextIndex = $scope.passengerIndex+1;                    	
                    	if($scope.nextIndex > $scope.passengerList.length-1){
                    		$scope.passengerIndex = 0;
                    		$scope.nextIndex = 0;
                    	}                    	
                    	$scope.changeSelected($scope.nextIndex);
                    	
                    };
                    	
                    $scope.changeSelectedUpdateData = function (){   
                    	if($scope.passportNo && $scope.passportExpiry && $scope.passportIssuedCntry && $scope.dob && $scope.email){                    		
                    		$scope.passengerList[$scope.passengerIndex].passportNo  = $scope.passportNo;                    		
                        	$scope.passengerList[$scope.passengerIndex].passportExpiry = $scope.passportExpiry;
                        	$scope.passengerList[$scope.passengerIndex].passportIssuedCntry = $scope.passportIssuedCntry;
                        	$scope.passengerList[$scope.passengerIndex].dob = $scope.dob;
                        	$scope.passengerList[$scope.passengerIndex].email = $scope.email;
                        	$scope.passengerDetailsFilled[$scope.passengerIndex] = true;
                    	}
                    	else{
                    		$scope.passengerDetailsFilled[$scope.passengerIndex] = false;
                    	}
                    	
                    }

                    $scope.nationalityNameList = _.map($scope.nationalityList, function(nationality){
                        return nationality.name;
                    });
                    $scope.countryNameList = _.map($scope.countryList, function(country){
                        return country.name;
                    });

                    $scope.mod = {};
                    $scope.lastPassenger = false;
                    $scope.salutationList = [];

                    $scope.minDob = new Date();
                    $scope.maxDob = new Date();
                    $scope.dataTip = "";                   
                             
                    $scope.formatLabelCountry = formatLabelCountry;
                    $scope.startsWith = startsWith;
                    
                    $scope.dateOptions = {
                            changeYear: true,
                            changeMonth: true,
                            minDate: '-100Y',
                            maxDate: '0',
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            yearRange: "-100:+0",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true,
                            isRTL: $scope.isRtl
                        };
                    
                               
                    $scope.passportExpiry = {
                    		changeYear: true,
                            changeMonth: true,
                            minDate: '0',
                            maxDate: '+20Y',
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            yearRange: "20:+20",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true,
                            isRTL: $scope.isRtl
                        }
                    
                    
                    $scope.scrollToCountry = function() {
                    	if (window.matchMedia("only screen and (max-width: 767px)").matches) {
                    		var element = angular.element('#country');
                    		angular.element("html, body").animate({
                    			scrollTop : element.offset().top - 94
                    		}, 600);
                    	}
                    }
                    
            
                    function startsWith(state, viewValue) {
                        if (viewValue === null || viewValue.trim() === "" ) {
                            return true;
                        } else {
                            var value = viewValue.trim();
                            return state.substr(0, value.length).toLowerCase() == value.toLowerCase();
                        }
                    }
                    
                    function formatLabelCountry(model) {
                        var tempN = $scope.countryList;
                        if (tempN) {
                            for (var i = 0; i < tempN.length; i++) {
                                if (model === tempN[i].code) {
                                    return tempN[i].name;
                                    break;
                                }
                            }
                        }
                    }
                }];
            return directive;
        }];
    isa_auto_checkin.directive(directives);
})();
;/**
 * Created by Nipuna on 10/05/17.
 */
'use strict';

var isa_back_button = angular.module("isaBackButton", []);
var directives = {};
directives.isaBackButton = [
    '$compile', '$http', '$templateCache', '$state', '$stateParams', 'languageService', 'currencyService', 'applicationService', '$rootScope',
    function ($compile, $http, $templateCache, $state, $stateParams, languageService, currencyService, applicationService, $rootScope) {
        var directive = {};
        var isaconfig = new IsaModuleConfig();

        directive.restrict = 'AEC';
        directive.scope = {caption: '=name'};

        directive.link = function (scope, element, attr) {

            var STATES = {
                FARE: 'fare',
                PASSENGER: 'passenger'
            };

            var MODES = {
                CREATE: 'create',
                CHANGE_CONTACT: 'changeContactInformation',
                NAME_CHANGE: 'nameChange'
            };

            scope.navigateBack = function () {
                var state = $state.current.name;
                var mode = $stateParams.mode;

                // call back function from common_services, navigation Service
                navigateTo(state, mode)
            };

            //TODO: Implementation of navigating based on mode and state name
            function navigateTo(state, mode) {

                //TODO: Discuss issues with this implementation
                $state.go($rootScope.fromState.fromStateName, $rootScope.fromState.fromParam);

                switch(state) {
                    case STATES.FARE:
                        switch(mode) {
                            case MODES.CREATE:
                                goToState(STATES.FARE);
                                break;
                            case MODES.CHANGE_CONTACT:
                            case MODES.NAME_CHANGE:
                                break;
                        }
                        break;

                    case STATES.PASSENGER:
                        switch(mode) {
                            case MODES.CREATE:
                                goToState(STATES.FARE);
                                break;
                            case MODES.CHANGE_CONTACT:
                            case MODES.NAME_CHANGE:
                                break;
                        }
                        break;
                }
            }

            function goToState(stateName){
                $state.go(stateName,
                    {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
            }

            var template = $templateCache.get(isaconfig.getTemplates("isaBackButton")['default']);

            template = angular.element(template);
            element.append(template);
            $compile(template)(scope);
        };

        return directive;

    }];
isa_back_button.directive(directives);

;/**
 *     Author  : Chamil Udayanga
 *     Date    : 11/01/2016
 */
'use strict';
(function () {

    var isaBusyLoader = angular.module('isaBusyLoader', []);
    var isaConfig = new IsaModuleConfig();

    isaBusyLoader.directive('isaBusyLoader', [
        '$compile', '$window', '$timeout', '$rootScope', '$templateCache',
        function ($compile, $window, $timeout, $rootScope, $templateCache) {

            return {
                restrict: 'AEC',
                scope: {},
                link: function (scope, element, attr) {
                    scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                    if(attr['isFixedPosition'] == "true") {
                        if(!scope.isMobileDevice){
                            scope.positionFixed = {"position":"fixed","top":"45%","left":"45%"};
                        }
                        if(scope.isMobileDevice){
                            scope.positionFixed = {"position":"fixed","top":"45%","left":"0","right":"0"};
                        }
                    }else{                        
                        scope.positionFixed = {};
                    }
                    if(attr['msgText'] === undefined) {
                        scope.msgText = "Please Wait...";
                        }else{
                        scope.msgText = attr['msgText'];
                    }

                    element.parent().css("position", "relative");
                    //element.attr() //is-fixed-position
                    var htmlText = $templateCache.get(isaConfig.getTemplates("isaBusyLoader")['default']);
                    var template = angular.element($compile(htmlText)(scope));
                    element.append(template);

                }

            }; // End of return.

        }]); // End of isaBusyLoader function.

})(); // End if IIFE.
;/**
 * Created by indika on 10/16/15.
 */
'use strict';

var isa_btn = angular.module("isaButton", []);
var directives = {};
directives.isabtn = [
    '$compile', '$http', '$templateCache',
    function ($compile, $http, $templateCache) {
        var directive = {};
        var isaconfig = new IsaModuleConfig();
        //load relavent tamplate according to the button type
        var getTemplate = function (type) {
            var template = '';
            switch (type) {
                case "radio" :
                    template = $templateCache.get(isaconfig.getTemplates("isaButton")[type]);
                    break;
                case "checkbox" :
                    template = $templateCache.get(isaconfig.getTemplates("isaButton")[type]);
                    break;
                default :
                    template = $templateCache.get(isaconfig.getTemplates("isaButton")['default']);
                    break;
            }
            return template;
        };
        directive.restrict = 'AEC';
        directive.scope = {caption: '=name'};
        directive.require = //other sibling directives;
            directive.link = function (scope, element, attr) {
                var template = angular.element(getTemplate(attr.type));
                element.append(template);
                $compile(template)(scope);
            };
        return directive;

    }];
isa_btn.directive(directives);

;/*
 * Directive used for identifying clicks outside the directive.
 *
 * Using 'angular-click-outside' directive
 * MIT Licence: https://github.com/IamAdamJowett/angular-click-outside/blob/master/LICENSE.md
 * */

'use strict';

(function () {
    var click_outside = angular.module("clickOutside", []);
    var directives = {};

    directives.clickOutside = ['$document', '$parse', function ($document, $parse) {
        var directive = {};
        directive.restrict = 'A';
        directive.link = function ($scope, elem, attr) {
            var classList = (attr.outsideIfNot !== undefined) ? attr.outsideIfNot.replace(', ', ',').split(',') : [],
                fn = $parse(attr['clickOutside']);

            // add the elements id so it is not counted in the click listening
            if (attr.id !== undefined) {
                classList.push(attr.id);
            }

            var eventHandler = function (e) {
                //check if our element already hiden
                if (angular.element(elem).hasClass("ng-hide")) {
                    return;
                }

                var i = 0,
                    element;

                // if there is no click target, no point going on
                if (!e || !e.target) {
                    return;
                }

                // loop through the available elements, looking for classes in the class list that might match and so will eat
                for (element = e.target; element; element = element.parentNode) {
                    var id = element.id,
                        classNames = element.className,
                        l = classList.length;

                    // Unwrap SVGAnimatedString
                    if (classNames && classNames.baseVal !== undefined) {
                        classNames = classNames.baseVal;
                    }

                    // loop through the elements id's and classnames looking for exceptions
                    for (i = 0; i < l; i++) {
                        // check for id's or classes, but only if they exist in the first place
                        if ((id !== undefined && id.indexOf(classList[i]) > -1) || (classNames && classNames.indexOf(classList[i]) > -1)) {
                            // now let's exit out as it is an element that has been defined as being ignored for clicking outside
                            return;
                        }
                    }
                }

                // if we have got this far, then we are good to go with processing the command passed in via the click-outside attribute
                return $scope.$apply(function () {
                    return fn($scope);
                });

            };

            // assign the document click handler to a variable so we can un-register it when the directive is destroyed
            $document.on('click', eventHandler);

            // when the scope is destroyed, clean up the documents click handler as we don't want it hanging around
            $scope.$on('$destroy', function () {
                $document.off('click', eventHandler);
            });
        };
        return directive;
    }];
    click_outside.directive(directives);
})();;/**
 * Created by indika on 9/7/16.
 */
/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_colorPicker = angular.module("isacolorpicker", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isacolorpicker = [
        '$compile', '$http', '$templateCache', '$rootScope',
        function ($compile, $http, $templateCache, $rootScope) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                onColorschemeSave: '&',
                onImageClick:'&',
                fileSelection:'&'
            };
            directive.require = //other sibling directives;
            directive.link = function (scope, element,attr) {
                var template = $templateCache.get(isaconfig.getTemplates("isacolorpicker")["default"]);
                var imageSelection = [];

                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
                scope.colorscheme = {primaryColor:'',secondaryColor:'','color_variation_1':''}
                scope.imagePath = {logo:""}
                scope.testMsg= "TEST COLOR PICKER"

                function dataURItoBlob(dataURI, callback) {
                    // convert base64 to raw binary data held in a string
                    var byteString = atob(dataURI.split(',')[1]);

                    // separate out the mime component
                    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

                    // write the bytes of the string to an ArrayBuffer
                    var ab = new ArrayBuffer(byteString.length);
                    var ia = new Uint8Array(ab);
                    for (var i = 0; i < byteString.length; i++) {
                        ia[i] = byteString.charCodeAt(i);
                    }

                    // write the ArrayBuffer to a blob, and you're done
                    var bb = new Blob([ab]);
                    return bb;
                }
                scope.fileSelection = function(data){
                    if(!!data.id) {
                        imageSelection[data.id] = data;
                    }
                }
                scope.onColorSelectionSave = function(){
                    var isEmpty = false;
                    var data = {selection:scope.colorscheme};
                    for(var item in scope.colorscheme){
                        if(!scope.colorscheme[item].length){
                            isEmpty = true;
                            break;
                        }
                    }
                    console.log(isEmpty)
                    if(!isEmpty) {
                        scope.onColorschemeSave(data)
                    }else{
                        alert('PLEASE SELECT ALL THREE COLORS');
                    }
                }
                scope.onImageEdit = function(){
                }
                scope.onSelectionSave = function(e){
                    var id = $(e.currentTarget).attr('id').split('-')[0];
                    if(!!imageSelection[id]) {
                        var fileType = imageSelection[id].type;
                        var blob = new Blob([dataURItoBlob(imageSelection[id].file)], {type: fileType});
                        var ext = fileType.split('/')[1];
                        saveAs(blob, (imageSelection[id].name + '.' + ext));
                    }else{
                        alert("PLEASE UPLOAD AN IMAGE");
                    }

                }

            };

            return directive;

        }];
    isa_colorPicker.controller('isaColorPickerController', ['$scope','$element', function ($scope,$element) {
        $scope.$watch('[colorscheme.primaryColor,colorscheme.secondaryColor,colorscheme.color_variation_1]',function(newval,oldval){
            setBackgroundColor(newval,oldval);
        })
        function setBackgroundColor(newVal,oldVal){
            var element = $($element.find('.sub_panel .header-img .preview-wrapper .container ul'));
            console.log(newVal);
            console.log(oldVal);
            console.log($element)
            if(newVal[0] != oldVal[0]){
                element.find('li.logo').css({'background-color':newVal[0]})
            }
            if(newVal[1] != oldVal[1]){
                element.find('li.current').css({'background-color':newVal[1]})
            }
            if(newVal[2] != oldVal[2]){
                element.parent().css({'background-color':newVal[2]})
            }
        }
    }]);
    isa_colorPicker.directive(directives);
})();

;/**
 * Created by indika on 11/18/15.
 */
'use strict';

(function () {
    var isa_connector = angular.module("isaConnector", []);
    var directives = {};

    directives.isaConnector = ['$rootScope', 'currencyService', '$timeout','$filter', '$translate','constantFactory',
        function ($rootScope, currencyService, $timeout,$filter, $translate, constantFactory) {
            var directive = {};
            directive.restrict = 'A';
            directive.scope = {
                isaConnector: "@",
                decimalPoints: "@",
                isPromotion:"@",
                currency: "@",
                withDecimal:'=',
                isBaseCurrency:'@',
                noExchangeCalc:'@'
            };
            directive.link = function (scope, element, attr) {
                
                var passAmount = scope.isaConnector || 0;
                var decimalPoints = parseInt(scope.decimalPoints) || 0;
                var isPromotion= scope.isPromotion || false;
                var currency = scope.isBaseCurrency ? currencyService.getAppBaseCurrency() :  currencyService.getSelectedCurrency();

                function calculateCurrency(currentAmount) {
                    if (isNaN(currentAmount)) {
                        currentAmount = 0;
                    }
                    var currencyCode = ( currency == currencyService.getSelectedCurrency() ) ? currency : currencyService.getSelectedCurrency() ; 
                    var selectedCurrency = currencyService.getSelectedCurrencyInfo(currencyCode);
                    var base = selectedCurrency.baseToCurrExRate;
                    var amount = 0;
                    var showDecimalPoints;
                    
                    scope.isBaseCurrency ? amount = currentAmount : amount = currentAmount * (1 / base);

                    // scope.noExchangeCalc ? amount = currentAmount : amount = currentAmount * (1 / base);
                    
                    if (constantFactory.AppConfigStringValue('showDecimalPlacesForCurrencyValues') === 'true') {
                        decimalPoints = selectedCurrency.decimalPlaces;
                        showDecimalPoints = true;
                    }

                    if (showDecimalPoints || scope.withDecimal) {
                        
                        amount = $filter('number')(amount, decimalPoints);

                    } else {
                        amount = Math.ceil(amount);
                    }

                    if(isPromotion){
                        amount = '-'+amount
                    }

                    //If element is not in the DOM set a timeout and set the currency and amount
                    if(!angular.element(element)){
                        $timeout(function(){
                            angular.element(element).text(amount);
                            angular.element(".currency").text(currencyService.getSelectedCurrency());
                        })
                    }else{
                        angular.element(element).text(amount);
                        angular.element(".currency").text(currencyService.getSelectedCurrency());

                        var translationString = 'lbl_payment_currency' + currencyService.getSelectedCurrency();
                        var translated = $translate.instant(translationString);

                        if(translated != translationString){
                            angular.element(".currency").text(translated);
                        }
                    }

                }

                calculateCurrency(passAmount);

                //TODO remove the $rootScope by using a event listener service
                $rootScope.$on('ON_CURRENCY_CHANGE', function (e, data) {
                    currencyService.setSelectedCurrency(currencyService.getSelectedCurrency());
                    calculateCurrency(passAmount);
                    $('.currency').text(currencyService.getSelectedCurrency());
                });

                scope.$watch(
                    "isaConnector",
                    function handleFooChange(newValue, oldValue) {
                        calculateCurrency(newValue);
                    }
                );

            };
            return directive;
        }];
    isa_connector.directive(directives);
})();
;/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_btn = angular.module("isaCounterButton", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isacbtn = ['$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};

            //load relavent tamplate according to the button type

            directive.restrict = 'AEC';
            directive.scope = {caption: '=name'};
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    var template = $templateCache.get(isaconfig.getTemplates("isaCounterButton"));
                    element.append(angular.element(template));
                    $compile(template)(scope);

                };
            return directive;

        }];
    isa_btn.directive(directives);
})();

;/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_currency_converter = angular.module("isaCurrencyConverter", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isacurconverter = [
        '$compile', '$timeout', '$http', '$document', '$rootScope', 'currencyService', '$templateCache','$window',
        function ($compile, $timeout, $http, $document, $rootScope, currencyService, $templateCache,$window) {
            var directive = {};

            directive.restrict = 'AEC';
            directive.scope = {
                curruncytypes: '=',
                isopen: '=',
                selectedCurrency: '='
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {

                    // Sort currency list by currency code.
                    scope.curruncytypes = scope.curruncytypes.sort(function (a, b) {
                        if (a.code < b.code) {
                            return -1;
                        }
                        if (a.code > b.code) {
                            return 1;
                        }
                        return 0;
                    });

                    var getSelectedCurrency = function (code) {
                        var toRrturn = {
                            'code': scope.selectedCurrency,
                            'title': '',
                            'icon': scope.selectedCurrency.toLowerCase()
                        };
                        angular.forEach(scope.curruncytypes, function (curr, i) {
                            if (curr.code === code) {
                                toRrturn = curr;
                            }
                        });
                        return toRrturn;
                    };

                    scope.selectedCurrencyType = getSelectedCurrency(scope.curruncytypes);
                    scope.isMobileDevice = $window.matchMedia("only screen and (max-width: 767px)").matches;
                    var template = $templateCache.get(isaconfig.getTemplates("isaCurrencyConverter")['default']);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);

                    scope.showpanel = false;
                    angular.element('body').on('click', function (e) {
                        var cname = e.target.className;
                        // check for mouse click is inside the pannel and not on the item in the list
                        var isVisible = element.find(e.target).length > 0 && (cname.indexOf('menu-item') === -1);
                        scope.$apply(function () {
                            if(!$(e.target).hasClass('glyphicon')) {
                                scope.showpanel = isVisible;
                            }
                        });
                    });
                    scope.showHideMenu = function (e) {
                        scope.showpanel = !scope.showpanel;
                    };
                    scope.onItemSelect = function (e) {
                        var newValue = {};
                        scope.selectedCurrencyType = scope.curruncytypes[e.$index];
                        newValue.baseAmount = currencyService.getSelectedCurrencyBaseValue();
                        newValue.currencyType = scope.selectedCurrencyType.code;
                        currencyService.setSelectedCurrency(scope.selectedCurrencyType.code);
                        //TODO remove rootScope and get the event out
                        $rootScope.$emit('ON_CURRENCY_CHANGE', newValue);

                        $timeout(function () {
                            scope.showpanel = false;
                        }, 0)

                    };
                };
            return directive;

        }];
    isa_currency_converter.directive(directives);
})();;'use strict';
(function () {
    var isaFareAndServicesDetails = angular.module("isaFareAndServicesDetails", []);
    var directives = {};

    var isaConfig = new IsaModuleConfig();
    directives.isaFareAndServicesDetails = [
        '$timeout', '$templateCache', '$compile', '$sce', '$rootScope', 'constantFactory', '$window', 'availabilityRemoteService',
        'localStorageService', 'modifyReservationService', 'currencyService', 'languageService',
        function ($timeout, $templateCache, $compile, $sce, $rootScope, constantFactory, $window, availabilityRemoteService,
                  localStorageService, modifyReservationService, currencyService, languageService) {
            var directive = {};

            directive.scope = {
                headerFareInfo: "=",
                flight: "=",
                currency: "@",
                isFlightModification: "=",
                isFlightChanged: "="
            };

            directive.link = function postLink(scope, element, attr) {

                if (!attr['templateBody']) {
                    scope.templateBody = 'default';
                }

                scope.selectedFareBaggageRates = false;
                scope.showBaggagePopup = false;

                scope.fareClassInfo = {};
                scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                scope.isSmallDevice = $rootScope.deviceType.isSmallDevice;
                scope.isLargeDevice = $rootScope.deviceType.isLargeDevice;

                var visibleFares = [];
                _.forEach(scope.flight.flightFaresAvailability, function(fare) {
                    if (fare.fareValue && fare.fareValue != "null") {
                        visibleFares.push(fare);
                    }
                });
                scope.flight.flightFaresAvailability = visibleFares;


                setFareDetails();
                setFlightSelectValueForFares();

                var templates = {};
                templates.body = {
                    url: isaConfig.getTemplates("isaFareAndServicesDetails")[scope.templateBody],
                    html: ""
                };

                angular.forEach(templates, function (template, key) {
                    template.html = angular.element($templateCache.get(template.url));
                    element.append(template.html);
                    $compile(template.html)(scope);
                });

                // Needs to analyze is there a better way to prevent calling parent.
                // At the moment this a dependency
                // should at least try to bring this whole module inside the flight_select to
                // produce the meaning that this module can only be used as a child isa_flight_select_directive
                scope.selectThisFare = function (event, flight, fare, parentId) {
                    if (scope.isFlightModification || scope.isFlightChanged) {
                        scope.$parent.onSelectFareNoPopup({selectedFlight: flight, selectedFare: fare});
                    } else {
                        scope.$parent.selectThisFare(event, flight, fare, parentId);
                    }
                    setFlightSelectValueForFares();
                };

                scope.showBaggageRates = function (event, flight, fare) {
                    var searchCriteria = localStorageService.get('SEARCH_CRITERIA');
                    scope.showBaggagePopup = true;

                    if (scope.isFlightModification || scope.isFlightChanged) {
                        var modifyingFlightSearchParams = modifyReservationService.getFlightSearchCriteria();
                        var reservationPaxDetails = modifyReservationService.getReservation()['passengerDetails'];
                        searchCriteria = {
                            cabin: modifyingFlightSearchParams.cabinClassType,
                            adult: reservationPaxDetails.adult || 1,
                            child: reservationPaxDetails.child || 0,
                            infant: reservationPaxDetails.infant || 0
                        }
                    }
                    searchCriteria.currency = currencyService.getSelectedCurrency();

                    if (!scope.fareClassInfo[fare.fareTypeId].baggageRates) {
                        availabilityRemoteService.getBaggageInfo(scope.fareClassInfo[fare.fareTypeId] || fare
                            , flight, searchCriteria, function (response) {
                                scope.fareClassInfo[fare.fareTypeId].baggageRates = (!_.isEmpty(response.baggageRates)  &&
                                    _.sortBy(response.baggageRates[0].baggageRates, function (baggageRate) {
                                        return baggageRate.baggageCharge;
                                    })) || [];
                                scope.selectedFareBaggageRates = scope.fareClassInfo[fare.fareTypeId].baggageRates;

                            }, function () {
                                scope.fareClassInfo[fare.fareTypeId].baggageRates = false;
                                scope.showBaggagePopup = false;
                            });
                    } else {
                        scope.selectedFareBaggageRates = scope.fareClassInfo[fare.fareTypeId].baggageRates;
                    }
                    event.stopPropagation();
                };

                scope.closeBaggagePopup = function () {
                    scope.showBaggagePopup = false;
                    scope.selectedFareBaggageRates = false;
                };

                function setFlightSelectValueForFares() {
                    if (scope.isFlightChanged && scope.flight.selectedFare) {
                        _.forEach(scope.flight.flightFaresAvailability, function (flightFare) {
                            flightFare.isFlightSelect = _.isEqual(scope.flight.selectedFare.fareTypeId, flightFare.fareTypeId);
                        });
                    }
                }

                function updateFareBundleBodyTemplate(template, iconClass, fareDetails, updateSummary) {

                    $(template).find('li').each(function (index, listOption) {
                        $(listOption).find('pre').replaceWith(function () {
                            return "<span>" + this.innerHTML + "</span>";
                        });

                        var innerHtml = $(listOption).html();
                        if (!_.isEmpty(innerHtml)) {

                            fareDetails.bodyContent = fareDetails.bodyContent + '<div class="fare-and-services-item">' +
                                '<img class="' + iconClass +
                                '" src="' + "js/util/isaui_lib/dist/images/" + iconClass + '.svg"></img>' +
                                '<span class="fare-and-services-item-description">'
                                + innerHtml + '</span>' +
                                '</div>';

                            $(listOption).find('a[href="BAGGAGE"]').each(function (index, element) {
                                fareDetails.baggageRatesElement = $(element).html();
                                $(element).remove();
                                innerHtml = $(listOption).html().replace(/&nbsp;/g, '');
                            });
                            if (updateSummary) {
                                if (!_.isEmpty(fareDetails.summary)) {

                                    fareDetails.summary =fareDetails.summary + ','
                                }
                                fareDetails.summary = fareDetails.summary + innerHtml;
                                
                            }
                        }
                    });
                    fareDetails.bodyContent = fareDetails.bodyContent.replace('href="BAGGAGE"',
                        'ng-click="showBaggageRates($event, flight, flightFare)"');

                }

                function getTemplateContent(fare, languageCode, contentType, defaultType) {
                    return fare.templateDTO &&
                        ((fare.templateDTO.translationTemplates && fare.templateDTO.translationTemplates[languageCode]
                        && fare.templateDTO.translationTemplates[languageCode][contentType]) ||
                        fare.templateDTO[defaultType]);
                }


                function setFareDetails() {
                    var fareClasses = scope.headerFareInfo;
                    var languageCode = languageService.getSelectedLanguage() ?
                        languageService.getSelectedLanguage().toLowerCase() : "en";
                    if (scope.isFlightModification || scope.isFlightChanged) {
                        fareClasses = scope.flight.flightFaresAvailability;
                    }

                    _.forEach(fareClasses, function (fareInfo) {
                        var fareBundleName = fareInfo.fareTypeName || fareInfo.description || '';
                        var fareDetails = {
                            name: fareBundleName,
                            description: fareInfo.description,
                            imageUrl: fareInfo.imageUrl,
                            rank: fareInfo.rank,
                            bodyContent: '',
                            summary: '',
                            fareClassType: fareInfo.fareClassType,
                            fareClassId: fareInfo.fareClassId,
                            baggageRatesElement: ''
                        };

                        updateFareBundleBodyTemplate(
                            getTemplateContent(fareInfo, languageCode, 'freeServicesContent',
                                'defaultFreeServicesContent'),
                            "free-item", fareDetails, true);
                        updateFareBundleBodyTemplate(getTemplateContent(fareInfo, languageCode, 'paidServicesContent',
                                'defaultPaidServicesContent'),
                            "dollar-item", fareDetails, false);
                        scope.fareClassInfo[fareInfo.fareTypeId] = fareDetails;
                    });
                }
            };
            return directive;
        }];


    directives.isaCompileHtml = [
        '$timeout', '$templateCache', '$compile', '$sce', '$rootScope', 'constantFactory', '$window',
        function ($timeout, $templateCache, $compile, $sce, $rootScope, constantFactory, $window) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    scope.$watch(function () {

                        return scope.$eval(attrs.isaCompileHtml);
                    }, function (value) {
                        element.html(value);
                        $compile(element.contents())(scope);
                    });
                }
            };
        }];


    isaFareAndServicesDetails.directive(directives);
})();
;/**
 * Created by baladewa on 11/4/15.
 */
'use strict';

(function () {
    var isa_fare_timeline = angular.module("fareFloater", []);
    var directives = {};
    directives.fareFloater = [
        '$compile', '$window', '$animateCss', '$timeout', '$rootScope','languageService','$translate','dateTransformationService','PersianDateFormatService', 'applicationService',
        function ($compile, $window, $animateCss, $timeout, $rootScope,languageService,$translate,dateTransformationService,PersianDateFormatService,applicationService) {
        return {
            restrict: 'A',
            scope: {
                calData: "=datasource",
                onAfterSlide: "&",
                onBeforeSlide: "&",
                onSelectDate: "&",
                currency: "@",
                cacheData: "=",
                autoSelectDate: "=",
                timelineSpeed: "=",
                animationEasing: "@",
                displayAnimation: "@",
                isRtl: "=",
                monthText: "=",
                daysText: "=",
                onBeforeSelectDate: "&"
            },
            template: $rootScope.deviceType.isMobileDevice? '<ul class="clearfix" ng-style="sliderWidth" ng-class="{rtl:isRtl}"></ul>' +
            '<a href="javascript:;" class="slide-nav slide-prev" ng-click="navigate(\'PREV\')"><i class="fa fa-angle-left"></i></a>' +
            '<a href="javascript:;" class="slide-nav slide-next" ng-click="navigate(\'NEXT\')"><i class="fa fa-angle-right"></i></a>' : '<ul class="clearfix" ng-style="sliderWidth" ng-class="{rtl:isRtl}"></ul>' +
            '<a href="javascript:;" class="slide-nav slide-prev" ng-click="navigate(\'PREV\')"><i class="glyphicon glyphicon-chevron-left"></i></a>' +
            '<a href="javascript:;" class="slide-nav slide-next" ng-click="navigate(\'NEXT\')"><i class="glyphicon glyphicon-chevron-right"></i></a>',
            link: function (scope, element, attrs) {
                //scope vars
                scope.slideCount = 7; //Slide Count
                scope.slideWidth = 0; // Slide Width
                scope.sliderWidth = {}; // Full Slider width
                scope.sliderData = []; // Slider data

                //local vars
                var slideWidth, timeLineData = [], nextCache = [], prevCache = [], navigationDone = true,
                    //DAYARRAY = (scope.daysText === undefined) ? ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] : scope.daysText,
                    //MONTHARRAY = (scope.monthText === undefined) ? ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] : scope.monthText,
                    speed = (scope.timelineSpeed === undefined) ? 0.3 : scope.timelineSpeed,
                    easing = (scope.animationEasing === undefined) ? "" : scope.animationEasing,
                    displayAnim = (scope.displayAnimation === undefined) ? "" : scope.displayAnimation,
                    leftOrRight = scope.isRtl ? "right" : "left";


                    /**
                     * Navigates Next Previous event
                     * @param direction
                     */
                    scope.navigate = function (direction) {
                        if (leftOrRight === "right") {
                            if (direction === "PREV") {
                                direction = "NEXT";
                            } else {
                                direction = "PREV";
                            }
                        }
                        if (navigationDone) {//to avoid multiple click
                            navigationDone = false;
                            var beforeSlideOption = {
                                "direction": direction,
                                date: getNextPrevDateBeforeNavigate(direction)
                            };
                            if (scope.onBeforeSlide(beforeSlideOption)) {//validation check onBeforeSlide

                                //Create New Element and compile with scope
                                var html = '<li ng-click="selectDate($event)" >' +
                                        '<div class="day-block" >' +
                                        '<div class=""><img src="js/util/isaui_lib/dist/images/bx_loader.gif" alt="loading" /></div>' +
                                        '</div></li>';
                                var newLi = angular.element(html);
                                var slider = angular.element(element[0].children[0].firstChild);
                                $compile(newLi)(scope);
                                //callback option for onAfterSlide
                                var callbackOption = {
                                    "direction": direction,
                                    "setData": function (dayObj) {
                                        setDataArrayToCalendar(direction, dayObj, false);
                                    }
                                };
                                // get cache data if dataCache==true
                                var cacheDate = getCacheData(direction);

                                if (direction === 'PREV') {// Next Previous Check
                                    //Remove border in before the last slide
                                    $animateCss(angular.element(slider[0].children[slider[0].children.length - 2].children[0]), {
                                        easing: easing,
                                        from: {borderWidth: '1px'},
                                        to: {borderWidth: '0px'},
                                        duration: speed // one second
                                    }).start();
                                    //Animate removing slide
                                    $animateCss(angular.element(slider[0].children[slider[0].children.length - 1]), {
                                        addClass: "dying",
                                        easing: easing,
                                        from: {width: slideWidth + 'px'},
                                        to: {width: '0px'},
                                        duration: speed // one second
                                    }).start()
                                            .then(function () {
                                                //remove element
                                                angular.element(slider[0].children[slider[0].children.length - 1]).remove();
                                                //set auto select date on next/previous if autoSelectDate true
                                                if (scope.autoSelectDate) {
                                                    autoSelectOnNavigation(direction);
                                                }

                                                if (cacheDate !== null) { //data cache check bore sending the call to main controller
                                                    //setting up date to the array
                                                    setDataArrayToCalendar(direction, cacheDate, true);
                                                } else {
                                                    //Call back
                                                    scope.onAfterSlide(callbackOption);
                                                }
                                            });
                                    slider.prepend(newLi);
                                    //Newly Created element Animation
                                    $animateCss(newLi, {
                                        addClass: "born",
                                        easing: easing,
                                        from: {width: '0px'},
                                        to: {width: slideWidth + 'px'},
                                        duration: speed // one second
                                    }).start();

                                } else {
                                    $animateCss(angular.element(slider[0].children[1]), {
                                        addClass: "dying",
                                        easing: easing,
                                        from: {width: slideWidth + 'px'},
                                        to: {width: '0px'},
                                        duration: 0.4 // one second
                                    }).start()
                                            .then(function () {
                                                //remove element
                                                angular.element(slider[0].children[1]).remove();

                                                //set auto select date on next/previous if autoSelectDate true
                                                if (scope.autoSelectDate) {
                                                    autoSelectOnNavigation(direction);
                                                }

                                                if (cacheDate !== null) { //data cache check bore sending the call to main controller
                                                    //setting up date to the array
                                                    setDataArrayToCalendar(direction, cacheDate, true);
                                                } else {
                                                    //Call back
                                                    scope.onAfterSlide(callbackOption);
                                                }
                                            });
                                    $animateCss(angular.element(slider[0].children[slider[0].children.length - 1].children[0]), {
                                        easing: easing,
                                        from: {borderWidth: '0px'},
                                        to: {borderWidth: '1px'},
                                        duration: speed // one second
                                    }).start();
                                    slider.append(newLi);
                                    $animateCss(newLi, {
                                        addClass: "born",
                                        easing: easing,
                                        from: {width: '0px'},
                                        to: {width: slideWidth + 'px'},
                                        duration: speed // one second
                                    }).start();
                                }// Next Previous Check
                            }//validation check onBeforeSlide
                            else {
                                navigationDone = true;
                            }
                        }//to avoid multiple click
                    };

                    /**
                     * select date event
                     * @param event
                     */

                    scope.selectDate = function (event) {
                        var ele = angular.element(event.delegateTarget);
                        var currentDate = $.trim(ele.find("span.hid-date").text());
                        var dateObj = null;
                        var currentDateOption = {
                            selectedDate: currentDate,
                            dayObj: dateObj,
                            sequence: scope.$parent.$index,
                            isOriginalEvent: !!event.originalEvent
                        };

                    if(scope.onBeforeSelectDate(currentDateOption)){
                        selectNewDate(event);
                    }

                    //for retaining the selected dates after traversing back to the flight select page from the navigation bar
                    if(!currentDateOption.isOriginalEvent){
                        angular.element(event.delegateTarget).addClass("selected-date");
                    }
                };

                    function selectNewDate(event) {
                        var slider = angular.element(element[0].children[0].firstChild);
                        angular.element(slider[0].children).removeClass("selected-date");
                        var ele = angular.element(event.delegateTarget).addClass("selected-date");

                        var currentDate = $.trim(ele.find("span.hid-date").text());
                        var dateObj = null;
                        angular.forEach(timeLineData, function (obj, key) {
                            if (currentDate === obj.date) {
                                dateObj = obj;
                            }
                        });
                        var currentDateOption = {
                            selectedDate: currentDate,
                            dayObj: dateObj,
                            sequence: scope.$parent.$index
                        };
                        unSelectDataInArray(currentDate);

                        scope.onSelectDate(currentDateOption);
                    }
                    /* scope.selectDate = function (event) {
                     var slider = angular.element(element[0].children[0].firstChild);
                     angular.element(slider[0].children).removeClass("selected-date");
                     var ele = angular.element(event.delegateTarget).addClass("selected-date");
                     var currentDate = $.trim(ele.find("span.hid-date").text());
                     var dateObj = null;
                     angular.forEach(timeLineData, function (obj, key) {
                     if (currentDate === obj.date) {
                     dateObj = obj;
                     }
                     });
                     var currentDateOption = {
                     selectedDate: currentDate,
                     dayObj: dateObj,
                     sequence: scope.$parent.$index
                     };
                     unSelectDataInArray(currentDate);
                     
                     scope.onSelectDate(currentDateOption);
                     }*/

                    /**
                     * set selected flag true or false when selecting a date
                     * @param currentDate
                     */

                    function unSelectDataInArray(currentDate) {
                        angular.forEach(timeLineData, function (obj, key) {
                            if (currentDate === obj.date) {
                                obj.selected = true;
                            } else {
                                obj.selected = false;
                            }
                        });
                    }

                    /**
                     * get next or  previous date before navigates
                     * @param direction
                     * @returns {*}
                     */

                    function getNextPrevDateBeforeNavigate(direction) {
                        var slider = angular.element(element[0].children[0].firstChild);
                        var ele, dayCount = 0;
                        if (direction === 'PREV') {// Next Previous Check
                            ele = angular.element(slider[0].children[1].children[0]);
                            dayCount = -1;
                        } else {
                            ele = angular.element(slider[0].children[slider[0].children.length - 1].children[0]);
                            dayCount = 1;
                        }
                        var currentDate = $.trim(ele.find("span.hid-date").text());
                        var nextOrPrev = moment(currentDate, "DD/MM/YYYY").add(dayCount, 'days').format("D/M/YYYY");
                        return nextOrPrev;
                    }

                    /**
                     * sets the date to the next or previous element either cached or new
                     * @param direction
                     * @param dataObj
                     * @param fromCache
                     */

                    function setDataArrayToCalendar(direction, dataObj, fromCache) {
                        var slider = angular.element(element[0].children[0].firstChild);
                        var elementPlace, formattedObj = fromCache ? dataObj : formatDaySlide(dataObj);
                        if (direction === "PREV") {
                            elementPlace = angular.element(slider[0].children[1].children[0]);
                        } else {
                            elementPlace = angular.element(slider[0].children[slider[0].children.length - 1].children[0]);
                        }
                        maintainDataArray(direction, formattedObj);
                        var temp = angular.element(sendDayHTML(formattedObj));
                        elementPlace
                                .empty()
                                .addClass(formattedObj.selected ? "selected-date" : "")
                                .append(temp);
                        $compile(temp)(scope);
                        navigationDone = true;
                    }

                    /**
                     * maintain the timeLineData, nextCache and prevCache arrays
                     * @param direction
                     * @param fmtObj
                     */

                    function maintainDataArray(direction, fmtObj) {
                        var temp;
                        fmtObj.selected = false;
                        if (direction === "PREV") {
                            if (scope.slideCount === 3 && timeLineData.length > 2) {
                                var length = timeLineData.length;
                                for (var i = length - 1; i >= 2; i--) {
                                    nextCache.unshift(timeLineData[i]);
                                }
                                for (var i = 2; i < length; i++) {
                                    timeLineData.pop();
                                }
                                timeLineData.unshift(fmtObj);
                            } else {
                                temp = timeLineData[timeLineData.length - 1];
                                timeLineData.pop();
                                timeLineData.unshift(fmtObj);
                                nextCache.unshift(temp);
                            }
                        } else {
                            temp = timeLineData[0];
                            timeLineData.shift();
                            timeLineData.push(fmtObj);
                            prevCache.push(temp);
                            if (scope.slideCount === 3 && timeLineData.length > 2) {
                                var length = timeLineData.length;
                                timeLineData.splice(3, length);
                            }
                        }
                    }

                    /**
                     * get cache data form the related arrays if cacheData==true
                     * @param direction
                     * @returns {*}
                     */

                    function getCacheData(direction) {
                        var ret;
                        if (!scope.cacheData) {
                            return null;
                        }
                        if (direction === 'PREV') {// Next Previous Check
                            ret = prevCache[prevCache.length - 1];
                            if (ret !== undefined) {
                                prevCache.pop();
                            }
                        } else {
                            ret = nextCache[0];
                            if (ret !== undefined) {
                                nextCache.shift();
                            }
                        }
                        if (ret === undefined) {
                            ret = null;
                        }
                        return ret;
                    }

                    /**
                     * auto selecting a date when there are no selected date
                     * on time line according to the navigation if only autoSelectDate==true
                     * @param direction
                     */

                    function autoSelectOnNavigation(direction) {
                        var slider = angular.element(element[0].children[0].firstChild), elementToSelect;
                        var hasSelectedDate = angular.element(slider[0].children).hasClass("selected-date");
                        if (!hasSelectedDate) {
                            if (direction === "PREV") {
                                elementToSelect = angular.element(slider[0].children[slider[0].children.length - 1]);
                            } else {
                                elementToSelect = angular.element(slider[0].children[1]);
                            }
                            $timeout(function () {
                                elementToSelect.triggerHandler('click');
                            });
                        }
                    }

                    /**
                     * Set Sizes of the Slider and slides
                     * @param scope
                     */
                    function setSizes(scope) {
                        angular.element(element).css({"width": "auto"});
                        angular.element(element[0].parentNode).css("width", "auto");
                        var slideCount = 7,
                                elementWidth = angular.element(element)[0].offsetWidth - 50;
                        if (elementWidth >= 718) {
                            scope.slideCount = 7;
                        } else {
                            scope.slideCount = 3;
                            slideCount = 3;
                        }
                        slideWidth = Math.floor(elementWidth / slideCount) - 2;
                        var varDist = (leftOrRight === "left") ? -(slideWidth) : slideWidth;
                        scope.slideWidth = {'width': slideWidth};
                        scope.sliderWidth = {'width': slideWidth * (slideCount + 2), "left": varDist};
                        angular.element(element).css({"width": (slideWidth * slideCount) + 50});
                        angular.element(element[0].children[0]).css({"width": (slideWidth * slideCount)});
                    }

                    /**
                     * Draw slider
                     * @param scope
                     */
                    function drawSlider(scope) {
                        var liHtml = '<li ng-click="selectDate($event)" ng-repeat="day in sliderData track by $index" ng-style="slideWidth" >' +
                                '<div class="day-block">' +
                                '<div class="animated bounceIn">' +
                                '<img src="js/util/isaui_lib/dist/images/bx_loader.gif" alt="loading" />' +
                                '</div>' +
                                '</div></li>';
                        var newLi = angular.element(liHtml);
                        $compile(newLi)(scope);
                        var slider = angular.element(element[0].children[0].firstChild);
                        slider.empty().append(newLi);
                    }


                    /**
                     * HTML formatter function to set the date to present return HTML String
                     * @param dayObj
                     * @returns {string}
                     */
                    function sendDayHTML(dayObj) {
                        var x, y, z = false;
                        if (!dayObj.dateFare) {
                            x = true;
                        } else if (dayObj.dateFare && dayObj.dateFare != -1) {
                            y = true;
                        } else if (dayObj.dateFare && dayObj.dateFare == -1) {
                            z = true;
                        }
                        var html = '<div class="animated ' + displayAnim + '">' +
                                '<span class="day-date-text">' + dayObj.dateText + '</span><span class="hid-date hide">' + dayObj.date + '</span>' +
                                '<span class="day-month-date-text">' + dayObj.dateNumber + ' ' + dayObj.monthText + '</span>' +
                                '<span class="day-fare" ng-show="' + y + '">' +
                                '<span class="currency">' + scope.currency + '</span> <span class="day-fare-value fare-value" isa-connector ="' + dayObj.dateFare + '" ></span> <i class="more">+</i>' +
                                '</span>' +
                                '<span class="day-fare not-fare" ng-show="' + x + '">' +
                                '<i class="more">No Flights</i></span>' +
                                '<span class="day-fare not-fare" ng-show="' + z + '">' +
                                '<i class="more">Flight Full</i>' +
                                '</span></div>';
                        return html;
                    }

                    /**
                     * setting the data to the created dates of the time line initially
                     */
                    function setTimeLineDataFirst() {

                        var slideElementArray = element.find(".day-block");

                        angular.forEach(slideElementArray, function (obj, key) {
                            if (key !== 0) {
                                var tm = timeLineData[key - 1];
                                var temp = angular.element(sendDayHTML(tm));
                                angular.element(obj).html(temp);
                                $compile(temp)(scope);

                                //angular.element( temp ).scope().$destroy();
                                //angular.element(obj.parentElement).addClass(tm.selected?"selected-date":"");

                                if (tm.selected) {
                                    $timeout(function () {
                                        angular.element(obj.parentElement).triggerHandler('click');
                                    });
                                }
                            }
                        });
                    }

                    /**
                     * data formatter function
                     * @param data
                     * @returns {{date: (data.date|*), dateText: string, monthText: string, dateNumber: *, dateFare: ($scope.dummyDateObj.fareValue|*), moreFares: boolean, selected: *}}
                     */
                    function formatDaySlide(data) {
                        if (languageService.getSelectedLanguage() == 'fa') {
                            var dateString = data.date.split("/");
                            var jalaaliObject = dateTransformationService.toJalaali(parseInt(dateString[2]), parseInt(dateString[1]), parseInt(dateString[0]));
                            console.log(scope.daysText[moment(data.date, "DD/MM/YYYY").day()]);
                            var daysText = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
                            return {
                                date: data.date,
                                dateText: PersianDateFormatService.reformatJalaaliDayStringFromEnglish(daysText[moment(data.date, "DD/MM/YYYY").day()]),
                                monthText: PersianDateFormatService.reformatJalaaliMonthStringFromEnglish(jalaaliObject.jm),
                                dateNumber: jalaaliObject.jd,
                                dateFare: data.fareValue,
                                moreFares: true,
                                selected: data.selected,
                                flights: data.flights,
                                fareClasses: data.fareClasses
                            }
                        } else {
                            return {
                                date: data.date,
                                dateText: scope.daysText[moment(data.date, "DD/MM/YYYY").day()],
                                monthText: scope.monthText[moment(data.date, "DD/MM/YYYY").month()],
                                dateNumber: moment(data.date, "DD/MM/YYYY").date(),
                                dateFare: data.fareValue,
                                moreFares: true,
                                selected: data.selected,
                                flights: data.flights,
                                fareClasses: data.fareClasses
                            }
                        }

                    }

                    /**
                     * format row data as when data received
                     * @param caldata
                     * @returns {Array}
                     */
                    function formatDataWithSlider(caldata) {
                        var returnArr = filterDataForMobileSlider(caldata);
                        // Will return if it is for mobile(3 slide)
                        if (returnArr.length == 0) {
                            angular.forEach(caldata, function (obj, key) {
                                returnArr.push(formatDaySlide(obj));
                            });
                        }

                        return returnArr;
                    }


                    /**
                     * Returns only -1, current and +1 dates data in case of mobile 
                     * @param caldata
                     * @returns {Array}
                     */
                    function filterDataForMobileSlider(caldata) {
                        var returnArr = [];
                        if (scope.slideCount == 3) {
                            // if first date is last possible date 
                            // or user selected date is today's date
                            if (caldata[0].selected) {
                                for (var i = 0; i < 3; i++) {
                                    returnArr.push(formatDaySlide(caldata[i]));
                                }
                                return returnArr;
                            } else {
                                var prev = null;
                                var midDateFound = false;
                                angular.forEach(caldata, function (obj, key) {
                                    if (midDateFound) {
                                        returnArr.push(formatDaySlide(obj));
                                        return false;
                                    } else if (obj.selected) {
                                        midDateFound = true;
                                        returnArr.push(formatDaySlide(prev));
                                        returnArr.push(formatDaySlide(obj));
                                    }
                                    prev = obj;
                                });
                                return returnArr;
                            }
                        } else {
                            return [];
                        }
                    }

                    // Directive Style Class
                    element.addClass("accelAero-slide").css("direction", scope.isRtl ? "rtl" : "ltr");
                    // Wrapping up the <ul>
                    var nav = angular.element('<div class="fare-floater-wrapper"></div>');
                    angular.element(element[0].children[0]).wrap(nav);
                    $compile(nav)(scope);//apply scope


                    /**
                     * ends scope watches
                     */

                    /**
                     * Directive Init function on element ready
                     */
                    element.ready(function () {
                        setSizes(scope);
                        drawSlider(scope);
                        for (var i = 0; i <= scope.slideCount; i++) {
                            scope.sliderData[i] = null;
                        }

                        /**
                         * starts scope watches
                         */

                        scope.$watch('calData', function (newVal, oldVal) {
                            if (newVal.data !== undefined) {
                                timeLineData = formatDataWithSlider(newVal.data);
                                setTimeLineDataFirst();
                            }
                        });

                        scope.$watch('slideCount', function (newVal, oldVal) {
                            if (newVal !== oldVal) {
                                scope.sliderData = [];
                                for (var i = 0; i <= scope.slideCount; i++) {
                                    scope.sliderData[i] = null;
                                }
                            }
                        });

                    });

                    /**
                     * Window Resize
                     */
                    angular.element($window).bind('resize', function () {
                        //setSizes(scope);
                        //drawSlider(scope);
                        //scope.$apply();
                        //setTimeLineDataFirst();
                    });

                }
            };
        }];
    isa_fare_timeline.directive(directives);
})();
;/**
 * Created by indika on 9/12/16.
 */
/**
 * Created by indika on 9/7/16.
 */
/**
 * Created by indika on 10/16/15.
 */
'use strict';
(function(angular){
    var module = angular.module('isaFileUploader',[]);
    module.directive('fileuploader',[function(){
        return  {
            restrict :'AE',
            scope : {
                setFileSelection : '&',
                maxSize : '=',
                fileType : '@',
                fileName : '@'
            },
            link : function(scope,ele,attr){
                ele.bind('change', function( evt ) {
                    scope.$apply(function() {
                        scope[ attr.name ] = evt.target.files;
                        console.log( scope[ attr.name ] );
                        var file = evt.target.files[0];
                        var fileType = file.type;
                        var imageType = /image.*/;
                        var zipType =  /zip.*/;

                        if (fileType.match(scope.fileType)) {
                            var reader = new FileReader();
                            if(fileType.match(imageType)) {
                                reader.onload = function (e) {
                                    var img = new Image();
                                    img.src = reader.result;
                                    img.onload = function (imageEvent) {
                                        ele.parent().find('.icon-img').innerHTML = "";
                                        var canvas = ele.parent().parent().find('.icon-img canvas')[0],
                                            max_size = scope.maxSize || 50,
                                            width = img.width,
                                            height = img.height;
                                        if (width > height) {
                                            if (width > max_size) {
                                                height *= max_size / width;
                                                width = max_size;
                                            }
                                        } else {
                                            if (height > max_size) {
                                                width *= max_size / height;
                                                height = max_size;
                                            }
                                        }
                                        canvas.width = width;
                                        canvas.height = height;
                                        canvas.getContext('2d').drawImage(img, 0, 0, width, height);
                                        //Getting base64 string;
                                        var dataUrl = canvas.toDataURL(fileType);
                                        scope.setFileSelection({data: {file: dataUrl, type: fileType, id: attr.id, name:scope.fileName}});

                                    }

                                }
                            }else if(fileType.match(zipType)){
                                ele.parent().parent().find('.icon-img label').text(file.name);
                                ele.parent().parent().find('.icon-img i').css({'display':'block'});
                                reader.onload = function (e) {
                                    scope.setFileSelection({data: {file: reader.result, type: fileType, id: attr.id, name:scope.fileName}});
                                }
                            }

                            reader.readAsDataURL(file);
                        } else {
                            alert('file type is not suported')
                        }
                    });
                });
            }
        };
    }]);
})(angular);;/**
 * Created by baladewa on 11/13/15.
 */
'use strict';

(function () {
    var isa_flight_select = angular.module("isaFlightSelect", []);
    var directives = {};
    var fareTooltip = {};
    var selectedOptions = {};

    var isaconfig = new IsaModuleConfig();
    directives.isaFlightSelect = [
        '$timeout', '$templateCache', '$compile', '$sce', '$rootScope','constantFactory','$window',
        function ($timeout, $templateCache, $compile, $sce, $rootScope,constantFactory,$window) {
            var directive = {};
            directive.restrict = 'AE';
            directive.scope = {
                headerInfo: "=",
                flightOptions: "=",
                currency: "@",
                onSelectFare: "&",
                onSelectFareNoPopup: '&',
                onConfirm: "&",
                isPopupVisible: '=',
                popupButtonCaption: '=',
                isBusyLoaderVisible: '=',
                templateHeader: '@',
                templateBody: '@',
                templatePopup: '@',
                savingsAvailable: '=',
                segmentId:'=',
                loadingMsg:"@",
                blockFare:'=',
                onEditFlight: "&",
                flightHeightList: "=",
                selectedSegment: "=",
                airports:"=",
                airlineMap: '=',
                showFareBundleDescription: '=',
                outerIndex:"=",
                isCreateFlow:"="
            };
            directive.link = function postLink(scope, element, attr) {

                scope.bodyTemplateVersion = constantFactory.AppConfigStringValue('bundleFareDescriptionTemplateVersion') || 'v1';

                if (!attr['templateHeader']) {
                    scope.templateHeader = 'default_header'
                }

                if (!attr['templateBody']) {
                    scope.templateBody = 'default_body'
                }

                if (!attr['templatePopup']) {
                    scope.templatePopup = 'default_popup'
                }

                // Logic has been changed to make header is not shown for v2
                // Needs to revist whether there is a better way to handle this
                if (!attr['isHeaderVisible']) {
                    scope.isHeaderVisible = true && scope.bodyTemplateVersion != 'v2';
                }
                scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
                scope.fTypeWidth = {};
                scope.headerFType = !_.isEmpty(scope.headerInfo) ? scope.headerInfo : [];
                scope.bodyFType = [];
                scope.blockSection = false;
                scope.applyMask = {};
                scope.selectedFareItems = {};
                scope.baseAmount = 1;
                scope.isContinuClicked = false;
                scope.mobileDevice = $rootScope.deviceType.isMobileDevice;
                scope.largeDevice = $rootScope.deviceType.isLargeDevice;
                scope.smallDevice= $rootScope.deviceType.isSmallDevice;
                scope.isCreateFlow = scope.isCreateFlow || false;
                scope.isThreeTwentyDevice = $window.matchMedia("only screen and (max-width: 320px)").matches;

                scope.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');

                var templetes = {};

                if (scope.isHeaderVisible) {
                    templetes.header = {
                        url: isaconfig.getTemplates("isaFlightSelect")[scope.templateHeader],
                        html: ""
                    }
                }

                templetes.body = {
                    url: isaconfig.getTemplates("isaFlightSelect")[scope.bodyTemplateVersion][scope.templateBody],
                    html: ""
                };
                templetes.popup = {
                    url: isaconfig.getTemplates("isaFlightSelect")[scope.templatePopup],
                    html: ""
                };

                angular.forEach(templetes, function (template, key) {
                    template.html = angular.element($templateCache.get(template.url));
                    element.append(template.html);
                    $compile(template.html)(scope);

                    /*$templateRequest(template.url).then(function(html){
                     template.html = angular.element(html);
                     element.append(template.html);
                     $compile(template.html)(scope);
                     });*/
                });

                var farecomparator = function (a, b) {
                    return parseFloat(a.fareTypeId) - parseFloat(b.fareTypeId);
                };

                scope.setSavingsAvailable = function(availability){
                    scope.savingsAvailable[scope.segmentId] = availability;
                };

                scope.$watch("flightHeightList", function(newVal, oldVal){
                    if(scope.flightHeightList && _.isNumber(scope.selectedSegment)){
                        scope.beforeHeight = 0;
                        scope.afterHeight = 0;

                        for(var i = 0; i < scope.selectedSegment ; i++){
                            scope.beforeHeight += scope.flightHeightList[i];
                        }

                        for(var j = scope.selectedSegment + 1; j <= scope.flightHeightList.length - 1 ; j++){
                            scope.afterHeight += scope.flightHeightList[j];
                        }
                    }
                }, true);

                scope.$watch("headerInfo", function (newVal, oldVal) {
                    if (newVal !== oldVal && !!newVal) {
                        //newVal.sort(farecomparator);
                        scope.headerFType = newVal;
                    }
                });

                scope.setDurationOfWidth = function (stops) {
                    var dev = stops.length + 1;
                    var temp = 91 / dev;
                    var temp = parseInt(temp);
                    temp = temp - 3;
                    return {"margin-right": temp + "%"};
                };

                scope.$watch("isPopupVisible", function (newVal, oldVal) {
                    scope.closePopup();
                    scope.isContinuClicked = true;
                });
                scope.$watch("flightOptions", function (newVal, oldVal) {
                    if (newVal !== oldVal && newVal !== undefined) {
                        clearPreviousSelection();

                        _.forEach(scope.flightOptions, function (flight) {
                            flight.isFlightSelected = false;
                        });

                        angular.forEach(newVal, function (obj) {
                            if (obj !== undefined) {
                                if(scope.airlineMap && scope.airlineMap[obj.careerCode]) {
                                    obj.displayCarrier = scope.airlineMap[obj.careerCode]
                                } else {
                                    obj.displayCarrier =  obj.careerCode;
                                }

                                //obj.flightFaresAvailability.sort(farecomparator);
                                angular.forEach(obj.flightFaresAvailability, function (fare) {
                                // fare.isFlightSelect = false;
                                });
                                scope.bodyFType = obj.flightFaresAvailability;
                                if (scope.isHeaderVisible) {
                                    missMatchLength();
                                }
                            }
                        });
                    }
                });

                //TODO check this
                scope.selectedFarePopup = function(index){
                    
                    if(Object.keys(selectedOptions).length) {
                        if(selectedOptions[scope.segmentId] != undefined) {
                            var pos = {};
                            pos.left = selectedOptions[scope.segmentId].position.left;
                            pos.top = selectedOptions[scope.segmentId].position.top;
                            scope.selectedFareItems = selectedOptions[scope.segmentId].selectedFlightFare;
                            showPopup(selectedOptions[scope.segmentId].currentDateOption);
                            scope.onSelectFare(selectedOptions[scope.segmentId].passToEvent);
                        }
                    }
                };

                scope.selectFlight = function (selectedFlight) {
                    if (selectedFlight && selectedFlight.isFlightSelected) {
                        selectedFlight.isFlightSelected = false;
                        return;
                    }

                    _.forEach(scope.flightOptions, function (flight) {
                        flight.isFlightSelected = false;
                    });

                    if (selectedFlight && (!scope.isCreateFlow || !selectedFlight.isFlightFull)) {
                        selectedFlight.isFlightSelected = true;
                    }
                };

                scope.getMinimumFare = function (flight) {
                    var minimum;
                    _.forEach(flight.flightFaresAvailability, function (fare) {
                        if (!minimum) {
                            minimum = fare.fareValue;
                        } else {
                        	if (fare.fareValue != "null"){
                        		minimum = Math.min(minimum, fare.fareValue);
                        	}
                        }
                    });
                    return minimum;
                };

                scope.selectThisFare = function (event, flight, fare, parentId) {
                    selectedOptions[scope.segmentId] = {}
                    selectedOptions[scope.segmentId].position = {}
                    selectedOptions[scope.segmentId].selectedFlightFare = {};
                    selectedOptions[scope.segmentId].currentDateOption = {};
                    selectedOptions[scope.segmentId].flightFares = {};
                    selectedOptions[scope.segmentId].flightFares = flight && flight.flightFaresAvailability;
                    var pos = {};
                    pos.left = selectedOptions[scope.segmentId].position.left = angular.element(event.currentTarget.parentNode)[0].offsetLeft;
                    pos.top = selectedOptions[scope.segmentId].position.top=  angular.element(event.currentTarget.parentNode)[0].offsetTop;
                    clearPreviousSelection();
                    fare.isFlightSelect = true;
                    var currentDateOption = {
                        position: pos,
                        selectedFlight: flight,
                        selectedFare: fare
                    };
                    var selectedFlightFare = {
                        flightId: flight.flightId,
                        flightRPH: flight.flightRPH,
                        flightNumber: flight.flightNumber,
                        fareTypeId: fare.fareTypeId,
                        fareValue: fare.fareValue,
                        parentId: parentId
                    };
                    scope.selectedFareItems = selectedFlightFare;
                    //showPopup(currentDateOption);

                    var passToEvent = {
                        selectedFlight: flight,
                        selectedFare: fare
                    };
                    scope.onSelectFare(passToEvent);
                    selectedOptions[scope.segmentId].selectedFlightFare = selectedFlightFare;
                    selectedOptions[scope.segmentId].currentDateOption = currentDateOption;
                    selectedOptions[scope.segmentId].passToEvent = passToEvent;

                    scope.onConfirm({selectedFlightFares: _.pluck(selectedOptions, "flightFares")});
                    event.preventDefault();
                };

                scope.closePopup = function () {
                    scope.applyMask = {"display": "hide", "z-index": -10};
                    scope.blockSection = false;
                    scope.isPopupVisible = true;
                    scope.isContinuClicked = false;
                    //scope.selectedFareItems = {}
                };

                scope.showNextDay = function(){

                }

                console.log(scope.headerFType.length);
                function missMatchLength() {
                    // if (scope.bodyFType.length !== scope.headerFType.length) {
                    //     alert("Length Miss Match");
                    // } else {
                    //     var fLength = Math.floor(100 / scope.headerFType.length);
                    //     scope.fTypeWidth = {"width": fLength + "%"};
                    // }
                }

                function getFareType(faretpyId) {
                    var fareName;
                    angular.forEach(scope.headerFType, function (value, key) {
                        if (value.hasOwnProperty("fareTypeId")) {
                            if (value.fareTypeId === faretpyId) {
                                fareName = value.fareTypeName;
                            }
                            return false;
                        }
                    });
                    return fareName;
                }

                function showPopup(options) {
                    var popWidth = 305;
                    var popHeight = 210;
                    scope.blockSection = true;
                    scope.popupPos = {
                        "top": options.position.top - (popHeight - 70) / 2,
                        "left": options.position.left - (popWidth - 70) / 2,
                        "width": popWidth,
                        "height": popHeight
                    };
                    scope.applyMask = {"display": "block", "z-index": 10};
                    scope.farePopupHeader = getFareType(options.selectedFare.fareTypeId);

                    scope.flightArrivalTime = options.selectedFlight.arrivalTime;
                    scope.flightArrivalDate = options.selectedFlight.arrivalDate;
                    scope.flightDepartureTime = options.selectedFlight.departureTime;
                    scope.flightDepartureDate = options.selectedFlight.departureDate;
                    scope.originName = options.selectedFlight.originName;
                    scope.destinationName = options.selectedFlight.destinationName;

                    scope.incomingFare = options.selectedFare.fareValue;
                    scope.currencyType = scope.currency;

                }
                function clearPreviousSelection(){
                    for(var i=0;i< scope.flightOptions.length;i++){
                        var obj  = scope.flightOptions[i].flightFaresAvailability;
                        for(var j= 0;j<obj.length;j++){
                            if(obj[j].isFlightSelect){
                                obj[j].isFlightSelect = false;
                                break;
                            }
                        }

                    }
                }

                scope.onClickEditFlight = function (child, parent, flight) {
                    scope.selectFlight(flight);
                    if (child !== parent) {
                        scope.onEditFlight({id: child});
                    }

                };


            };
            return directive;
        }];
    isa_flight_select.directive(directives);
    isa_flight_select.filter('flightDuration', ['$translate', function ($translate) {
        return function (data) {
            var hoursString = $translate.instant('lbl_common_hours');
            var minutesString = $translate.instant('lbl_common_minutes');
            var directFlightString = $translate.instant('lbl_common_directFlight');
            var stopsString = $translate.instant('lbl_common_stops');
            var stopString = $translate.instant('lbl_common_stop');

            var duration = data.totalDuration;
//            data.stops[0]={flightNumber:"G5987"};
//            data.stops[1]={flightNumber:"G5987"};
//            data.stops[2]={flightNumber:"G5987"};
            var stops = (Object.keys(data.stops).length > 0 ) ? Object.keys(data.stops).length + ' '+ ( (Object.keys(data.stops).length > 1 ) ? stopsString : stopString ) : directFlightString;
            var days = (duration.split(":")[0] > 0) ? parseInt(duration.split(":")[0], 10) : '';
            var hours = (duration.split(":")[1] > 0) ? parseInt(duration.split(":")[1], 10)  : '';
            var totalH= parseInt(days*24) || 0 + parseInt(hours) || 0;
            var totalHours = (totalH>0)?totalH+ ' ' +hoursString:'';
            var minutes = (duration.split(":")[2] > 0) ? duration.split(":")[2] + ' ' + minutesString : '';
            return totalHours + ' ' + minutes + ' / ' + stops;
        };
    }]);
    isa_flight_select.filter('stopType', ['$sce', '$translate', function ($sce, $translate) {
        return function (data) {
            var daysString = $translate.instant('lbl_common_days');
            var hoursString = $translate.instant('lbl_common_hours');
            var minutesString = $translate.instant('lbl_common_minutes');

            var toolTip,wt= 0;
            if(data.durationWaiting){
                wt = data.durationWaiting.split(':');
            }
            var compareDate = function(date1, date2){
                var d1 = moment(date1, 'DD/MM/YYY');
                var d2 = moment(date2, 'DD/MM/YYY');
                if (d1 < d2){
                    return  ' (+1)';
                }else{
                    return  '';
                }

            };
//            var tmpFrom = angular.copy(data.airports);
//            var airportFrom = _.where(tmpFrom,{code:data.originCode});
            var airportFrom = [];
             airportFrom[0] = {
                 code:data.originCode,
                 name:data.originName
             }
            if(airportFrom.length) {
                var labelFrom = $translate.instant('lbl_flight_tooltip_from', {
                    fromName: data.originName,
                    fromCode: data.originCode
                });
            }
            var valueFrom = $translate.instant('lbl_flight_tooltip_from_dep_arr',{
           	 dep: data.departureTime,
        	 arr: data.arrivalTimeFirstSeg,
        	 plus: compareDate(data.departureDate, data.arrivalDateFirstSeg)
        	});

            var tmpTransit = angular.copy(data.airports);
            var airportTransit = _.where(tmpTransit,{code:data.airportCode});
            if(airportTransit.length) {
                var labelTransit = $translate.instant('lbl_flight_tooltip_transit', {
                    airportName: airportTransit[0].name,
                    airportCode: data.airportCode
                });
            }
            	
            var valueTransit = $translate.instant('lbl_flight_tooltip_transit_dep_arr',{
            	dep: data.departureTime,
            	arr: data.arrivalTime 
            	});
            var waitingTransit = $translate.instant('lbl_flight_tooltip_transit_waiting',{
            	days: days,
            	hours: hours,
            	minutes: minutes
            	});
                var tmpTransit = angular.copy(data.airports);
            var airportTransit = _.where(tmpTransit,{code:data.airportCode});
            if(airportTransit.length) {
                var labelTransit = $translate.instant('lbl_flight_tooltip_transit', {
                    airportName: airportTransit[0].name,
                    airportCode: data.airportCode
                });
            }
            	
            var valueTransit = $translate.instant('lbl_flight_tooltip_transit_dep_arr',{
            	dep: data.departureTime,
            	arr: data.arrivalTime 
            	});
            var waitingTransit = $translate.instant('lbl_flight_tooltip_transit_waiting',{
            	days: days,
            	hours: hours,
            	minutes: minutes
            	});
            var tmpTouchdown = angular.copy(data.airports);
            var airportTouchdown = _.where(tmpTouchdown,{code:data.airportCode});
            if(airportTouchdown.length) {
                var labelTouchdown = $translate.instant('lbl_flight_tooltip_touchdown', {
                    airportName: airportTouchdown[0].name,
                    airportCode: data.airportCode
                });
            }
            var valueTouchdown = $translate.instant('lbl_flight_tooltip_touchdown_dep_arr',{
            	arr: data.arrivalTime, 
            	dep: data.departureTime
            	});
            var waitingTouchdown = $translate.instant('lbl_flight_tooltip_touchdown_waiting',{
            	hours: hours,
            	minutes: minutes
            	}); 
            var labelDeparture =$translate.instant('msg_common_departure');
            var labelArrival = $translate.instant('msg_common_arrival');
            var toolTip,wt= 0;
            if(data.durationWaiting){
                wt = data.durationWaiting.split(':');
            }

            var days = (wt[0] > 0) ? wt[0] + ' ' + daysString : '';
            var hours = (wt[1] > 0) ? wt[1] + ' ' + hoursString : '';
            var minutes = (wt[2] > 0) ? wt[2] + ' ' + minutesString : '';
            var waitingTransit = $translate.instant('lbl_flight_tooltip_transit_waiting',{
            	days: days,
            	hours: hours,
            	minutes: minutes
            	});
            var waitingTouchdown = $translate.instant('lbl_flight_tooltip_touchdown_waiting',{
            	hours: hours,
            	minutes: minutes
            	}); 
            
            if(data.stops){
                toolTip = (
                    '<div class="fare-tooltip-wrappe"><span class=" fare-tooltip-heading">'+data.originName+'</span> ' +
                    '<span class="fare-tooltip-icon"></span></br>' +
                    '<span >' + data.flightNumber + '</span>' +
                    '<span> (' + data.equipmentModelNumber + ') </span>' + 
                    // Operated By: ' + data.displayCarrier + '</span>' +
                    '</br><span> '+labelDeparture+' <b>' + data.departureTime + ' </b><br/>'+labelArrival+' <b>' + data.arrivalTimeFirstSeg +
                    compareDate(data.departureDate, data.arrivalDateFirstSeg) + '</b></span><br/>' +
                    '<span class="fare-tooltip-icon"></span>'
                );
            }else if (data.flightNumber) {
                toolTip = (
                    '<div class="fare-tooltip-wrappe"><span class=" fare-tooltip-heading">'+data.originName+'</span>' +
                    '<span class="fare-tooltip-icon"></span></br>' +
                    '<span >' + data.flightNumber + '</span>' +
                    '<span> (' + data.equipmentModelNumber + ')  </span>' + 
                    // Operated By: ' + data.displayCarrier + '</span>' +
                    '</br><span> '+labelDeparture+' <b>' + data.departureTime + '</b><br>'+labelArrival+' <b>' + data.arrivalTime +
                    compareDate(data.departureDate, data.arrivalDate) + '</b></spna><br/>' +
                    '<span class="fare-tooltip-icon"></span>' +
                    '<span > Waiting time <b>' + days + '</b> <b>' + hours + '</b> <b>' + minutes + '</b></span></div>'
                );
            } else {
                toolTip = (
                    '<div class="fare-tooltip-wrappe"><span class=" fare-tooltip-heading">'+labelTouchdown+'</span> ' +
                    '</br><span>'+valueTouchdown+'</span><br/>' +
                    '<span class="fare-tooltip-icon"></span>' +
                    '<span > Waiting time <b>' + hours + '</b> <b>' + minutes + '</b></span></div>'
                );
            }
            return fareTooltip[toolTip] || (fareTooltip[toolTip] = $sce.trustAsHtml(toolTip));
        };
    }]);
    isa_flight_select.filter('fareTypeTip', ['$sce', function ($sce) {
        return function (data) {
            var toolTip;
            if (data != undefined && data.length > 0 ) {
                toolTip = (
                    '<div class="fare-tooltip-wrappe">' + data + '</div>'
                );
            }
            return $sce.trustAsHtml(toolTip);
        };
    }]);
})();
;/**
 * Created by indika on 10/18/16.
 */
'use strict';
(function () {
    var isa_translate = angular.module("isaTranslate", []);
    var directives = {};

    directives.isaTranslate = ['$translate', function ($translate) {
        return {
            restrict: 'A',
            replace: false,
            terminal: true,
            priority: 10000, //this setting is important to make sure it executes before other directives
            scope:{
                key:'@'
            },


            link: function (scope, element, attrs) {
                $translate(scope.key).then(function (msg) {
                    if (msg != scope.key) {
                        element.html(msg);
                    } else {
                        element.css({'display':'none'});
                    }
                })

            }
        }
    }]
    isa_translate.directive(directives);
})()
;'use strict';

(function (angular) {
    var module = angular.module('isaJqDateUtc', []);
    module.directive('isaJqDateUtc', ['$parse', function ($parse) {
        return {
            restrict: 'AE',
            require: '?ngModel',
            link: function (scope, ele, attr) {
                scope.$watch(attr.ngModel, function (value) {
                    if(value){
                        var DATE_FORMAT = "YYYY-MM-DD";

                        var model = $parse(attr.isaJqDateUtc);
                        var formattedDateString = moment(value).format(DATE_FORMAT);
                        var utcDate = moment.utc(formattedDateString, DATE_FORMAT).toDate();
                        model.assign(scope, utcDate);
                    }
                })
            }
        };
    }]);
})(angular);;/**
 * Created by baladewa on 11/10/15.
 */
'use strict';

(function () {
    var isa_loading_progress = angular.module("isaLoadingProgress", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaLoadingProgress = ['$timeout', '$templateCache', '$translate', function ($timeout, $templateCache, $translate) {
        var directive = {};
        directive.template = $templateCache.get(isaconfig.getTemplates("isaLoadingProgress")['default']);
        directive.replace = true;
        directive.restrict = 'AE';
        directive.scope = {
            loadingStatus: "=",
            loaderText: "@",
            timeOut: '@',
        };
        directive.link = function postLink(scope, element) {
            var timeout = scope.timeOut || "0";
            if (scope.loaderText === undefined) {
                $translate.onReady(function(){
                    var loadingMessage = $translate.instant('lbl_common_loadingMain');
                    scope.loaderText = loadingMessage || "Please wait, loading...";
                });
            }
            scope.$watch("loadingStatus", function (oldVal) {
                if (oldVal) {
                    element.removeClass('ng-hide');
                    $("html").css("overflow", "hidden");
                    scope.loadingStatus = oldVal;
                    var timeInMins = parseInt(timeout, 10), timeInMils = 0;
                    if (timeInMins !== 0) {
                        timeInMils = timeInMins * 60 * 1000;
                        scope.loadingStatus = oldVal;
                        /*$timeout(function () {

                        }, timeInMils);*/
                        //scope.loadingStatus = false;
                    }
                } else {
                    element.addClass('ng-hide');
                    $("html").css("overflow", "auto");
                }

            });
        };
        return directive;
    }];
    isa_loading_progress.directive(directives);
})();;/**
 * Created by sumudu on 1/31/16.
 */
'use strict';

(function(angular){
    var module = angular.module('isaNoMouse',[]);
    module.directive('noMouse',[function(){
        return  {
            restrict :'AE',
            scope : {
                isNoMouseActive : '='
            },
            link : function(scope,ele,attr){
                if(attr['isNoMouseActive'] == undefined) {
                    scope.isNoMouseActive = false;
                }

                var changeUI = function(){
                    if(scope.isNoMouseActive){
                        angular.element(ele).addClass('no-mouse-relative');
                        angular.element(ele).prepend( '<div class="no-mouse-inner">&nbsp;</div>');
                    }else{
                        angular.element(ele).removeClass('no-mouse-relative');
                        angular.element(ele).find('.no-mouse-inner').remove();
                    }

                };

                scope.$watch('isNoMouseActive',function(){
                    changeUI();
                })
            }
        };
    }]);
})(angular);;/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_optionlist = angular.module("isaoptionlist", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaoptionlist = [
        'applicationService','$compile', '$http', '$templateCache', '$rootScope', 'currencyService','ancillaryService','stepProgressService',
        function (applicationService, $compile, $http, $templateCache, $rootScope, currencyService,ancillaryService,stepProgressService) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                availableOptions: '=',
                optionClick: '&',
                source: '=',
                template: '@',
                dropdownSource: '=',
                secondDropDownSource: '=',
                dropdownClick: '&',
                secondaryOptionClick: '&',
                elementButtonCaptions: '=',
                widgetOptions: '=',
                widgetData: '&',
                widgetOptionClick: '&',
                reset: '&',
                selectedList: '=',
                panelType: '@',
                currentItemSelection: '=',
                addedItems: '=',
                paginationData: '=',
                currentPage: '&',
                onPagination: '&',
                onEnd: '&',
                textFiltering: '&',
                resetValues: '=',
                currentSegment: '=',
                insuranceAmount:'=',
                flight:'=',
                selectedAncillaries:'=',
                ondPrefernces:'=',
                duplicateIndex:'=',
                isNextSectorClick:'=',
                baggageForSectorFirst:'=',
                baggageForSectorSecond:'=',
                getInv:'&'
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element) {
                        scope.mealData = [];
                        scope.accordionArray = [];
                    scope.isCatListVisible = false;
                    scope.orderByOption = "";
                    scope.selectedMobileRow = -1;
                    scope.selectedRowIndex = -1;
                        scope.selectedCategoryCode = '';
                        scope.showSpinner = 0;
                    scope.currencyType = currencyService.getSelectedCurrency();
                        scope.selectedPage = (scope.paginationData != undefined) ? scope.paginationData.defaultselection : 0;
                        scope.resetData = scope.resetValues || {};
                        scope.orderbyText = "lbl_extra_mealPrice";
                        scope.onEditClickParam = false;
                        //scope.mealnumber = scope.duplicateIndex.mealDuplicateIndex;
                        scope.getNumber = function(num) {
                                return new Array(num);   
                        }
                        var template = $templateCache.get(isaconfig.getTemplates("isaoptionlist")[scope.template]);
                        template = angular.element(template);
                        element.append(template);
                        $compile(template)(scope);

                        scope.getFlightDirection = function (toCode, fromCode, isModify) {
                            return applicationService.getFlightDirection(toCode, fromCode, isModify)
                        }
            

                        scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                        
                        scope.anciOptions = ancillaryService.getDefaultAncillary();

                        scope.resetData.SearchText = function () {
                            scope.searchText = null;
                        };

                        scope.resetData.Pagination = function () {
                            scope.selectedPage = 0;
                            scope.resetStyle();
                        };

                        scope.onFiltering = function (data) {
                            scope.textFiltering(data);
                        };

                        scope.onPageSelection = function (index) {
                            scope.selectedPage = index.index;
                            scope.onPagination(index)
                        };

                        scope.toogleMealSection = function (id, $event) {
                            var $this;
                            var $fa;
                            if($event.target.tagName === "I"){
                                $this = $($($event.target).parent().parent()).find('.meal-block');    
                                $fa = $($event.target);
                            }else{
                                $this = $($($event.target).parent()).find('.meal-block');    
                                $fa = $($event.target).find('.fa-meal-block');
                            }
                            

                            $(".meal-slider .meal-block").not($this).hide();
                            $this.slideToggle("100");

                            $('.fa-meal-block').not($fa).removeClass('fa-minus');
                            $('.fa-meal-block').not($fa).addClass('fa-plus');
                            
                            if ($fa.hasClass('fa-plus')) {
                                $fa.removeClass('fa-plus')
                                $fa.addClass('fa-minus')
                            } else {
                                $fa.removeClass('fa-minus')
                                $fa.addClass('fa-plus')
                            }

                            scope.selectedIndex = id.id;
                            scope.optionClick(id);

                            scope.showSpinner = id.id;

                           // scope.selectedCategoryCode = id.catCode;
                        }

                        scope.onNextSector = function (index) {
                            console.log('index' + index)
                            // scope.selectOption(index);
                        }

                        
                        scope.returnFlightBySegments = function(flight){
                        var flightArray=new Array();
                        var flightSegment=new Array();
                            if(flight != undefined){
                                if(flight.length > 2){
                                    flight.forEach(function(element) {
                                        flightArray.push(element)
                                        if(flightArray.length == 2){
                                            flightSegment.push(flightArray);
                                            flightArray= new Array();
                                        }
                                    });
                                }else{
                                    flightSegment.push(flight);
                                }
                                return flightSegment;
                            }
                        }

                        scope.optionOnClick = function (id) {
                            //console.log('id'+id.id)
                            //debugger;
                            var selected = {};
                            //scope.orderbyText = "lbl_extra_mealPrice";

                            scope.onPageSelection({index: 0});
                            if (typeof Number(id) === 'number') {
                                selected = scope.optionClick(id);
                                scope.mealData = selected;
                            } else {
                                scope.optionClick(id);
                            }
                            scope.selectedIndex = id.id;
                            if (id.hasOwnProperty('event')) {
                                
                                scope.selectedRowIndex = -1;

                                if ($(id.event.currentTarget).find('i span').hasClass('icon-close')) {
                                    scope.onEditClickParam = false;
                                    scope.selectedIndex = -1;
                                } else if ($(id.event.currentTarget).find('i span').hasClass('icon-plus')) {
                                    scope.selectedIndex = id.id;
                                    scope.onEditClickParam = true;
                                }
                                if ($(id.event.currentTarget).hasClass('cat-button')) {
                                    $(id.event.currentTarget.parentNode.parentNode).find('span').removeClass('gray-btn');
                                    $(id.event.currentTarget.parentNode.parentNode).find('span').removeClass('check-btn');

                                    $(id.event.currentTarget).addClass('gray-btn');
                                    $(id.event.currentTarget).addClass('check-btn');
                                }
                                
                            }
                            if (selected !== undefined) {
                                scope.selectedOptionIndex = selected.optionIndex;
                                scope.selectedDropDown = selected.dropDownIndex;
                            }
                            scope.selectedCat = selected;
                            if ($rootScope.deviceType.isMobileDevice){
                                scope.setExtraType(id.id);
                                if(id.event){
                                //id.event.preventDefault();
                               // id.event.stopPropagation();
                            }
                                return scope.mealData;
                                
                            }
                                
                        };
                        scope.setExtraType = function (id) {
                            var option = '';
                            switch (id) {
                                case 'BAGGAGE' :
                                    option = 'baggage';
                                    break;
                                case 'SEAT' :
                                    option = 'seats';
                                    break;
                                case 'MEAL' :
                                    option = 'meals';
                                    break;
                                case 'AUTOMATIC_CHECKIN' :
                                    option = 'automatic_checkin';    
                                    break;
                                case 'INSURANCE' :
                                    option = 'insurance';
                                    break;
                                case 'FLIGHT SERVICES' :
                                    option = 'services';
                                    break;
                                case 'SSR_AIRPORT' :
                                    option = 'airport_services';
                                    break;
                                case 'FLEXIBILITY' :
                                    option = 'flexibility';
                                    break;
                                case 'AIRPORT TRANSFER' :
                                    option = 'airport_transfer';
                                    break;
                            }
                            if(option!== ''){
                                stepProgressService.setCurrentStep('extras');
                                $rootScope.extraType = option;
                            }
                        }

                        scope.resetStyle = function () {
                            element.find('span').removeClass('gray-btn');
                            element.find('span').removeClass('check-btn');

                            var categoryElement = element.find('.selected-cat');
                            var selectAllElement = categoryElement.find('span').first();

                            selectAllElement.addClass('gray-btn');
                            selectAllElement.addClass('check-btn');
                        };

                        scope.onSecondaryOptingClick = function (option) {
                            scope.secondaryOptionClick({option: option});
                            scope.orderByOption = option;
                            scope.onPageSelection({index: 0});
                            switch(option) {
	                            case 'mealCharge':
	                            	scope.orderbyText = 'lbl_extra_mealPrice';
	                                break;
	                            case 'mealName': 
	                            	scope.orderbyText = 'lbl_extra_mealName';
	                                break;
	                            case 'popularity':
	                            	scope.orderbyText = 'lbl_extra_mealPopularity';
	                                break;
	                            default:
	                            	scope.orderbyText = 'lbl_extra_mealPrice';
                            }
                        };

                        scope.dropdownOptionClick = function (id) {
                            scope.isCatListVisible = (scope.isCatListVisible) ? false : true;
                            scope.selectedIndex = -1;
                            if (id !== undefined) {
                                scope.dropdownClick(id);
                                scope.selectedOptionIndex = id.index;
                                scope.selectedDropDown = id.section;
                                scope.selectedIndex = id.id;
                            }
                            if(id.event && $rootScope.deviceType.isMobileDevice){
                                id.event.preventDefault();
                                id.event.stopPropagation();
                            }
                          //  angular.element(".dropdown-content").height($(document).height());
                        };
                        
                      
                        scope.onMobileElementClick = function (event, index, isClose){
                            scope.widgetOptionClick({target: event, index: index, selectedRow: scope.selectedRowIndex, categoryCode: scope.selectedCategoryCode,isClose:isClose});
                            if ( scope.selectedMobileRow == -1) {
                                scope.selectedMobileRow  = index;
                            } else {
                                scope.selectedMobileRow  = -1;

                            }
                        }

                        scope.onElementClick = function (event, index,isClose) {
                            scope.widgetOptionClick({target: event, index: index, selectedRow: scope.selectedRowIndex, categoryCode: scope.selectedCategoryCode,isClose:isClose});
                            if (scope.selectedRowIndex === -1) {
                                scope.selectedRowIndex = index;
                            } else {
                                scope.selectedRowIndex = -1;
                            }
//                            if(event){
//                                event.preventDefault();
//                                event.stopPropagation();
//                            }
                        };

                        scope.onDoneClick = function (event, index) {
                            if (scope.selectedRowIndex === -1) {
                                scope.selectedRowIndex = index;
                            } else {
                                scope.selectedRowIndex = -1;
                            }
                        };

                        scope.onElementClose = function (index) {
                            scope.selectedRowIndex = -1;
                        };

                        scope.setWidgetData = function (data, index,fromMobileClick, val) {
                            scope.widgetData({count: data, index: index,fromMobileClick:fromMobileClick, value: val});
                        };

                        scope.categoryName = function (name) {
                            return name.split(' ').join('-')
                        };

                        angular.element('body').on('click', function (e) {
                            if (element.find(e.target).length <= 0) {
                                scope.selectedRowIndex = -1;
                                scope.reset({evt: e});
                                scope.selectedIndex = -1;
                                scope.isCatListVisible = false;
                            }
                            scope.$apply(function () {
                            });
                        });

                        scope.doneMealOptionClick = function () {
                            scope.isCatListVisible = false;
                        };
                        
                        scope.getInventory = function(oldCount, newCount, key, value){
                        	return scope.getInv({oldCount: oldCount,newCount:newCount, key:key, value:value});
                        }

                    };

            return directive;

        }];
    isa_optionlist.controller('optionListController', ['$scope', function ($scope) {
        }]);
    isa_optionlist.directive(directives);
})();

;'use strict';

(function () {
    var isa_passenger_picker = angular.module("isaPassengerPicker", []);
    var directives = {};
    var isaConfig = new IsaModuleConfig();

    directives.isaPassengerPicker = ["$compile", "$http", "$templateCache", "$timeout" , function ($compile, $http, $templateCache, $timeout) {
        var directive = {};

        directive.restrict = 'AEC';
        directive.scope = {
            isOpen: '=',
            passengerDetails: '=',
            template: '@'
        };

        directive.link = function (scope, element) {
            var template = $templateCache.get(isaConfig.getTemplates("isaPassengerPicker")['default']);
            if(scope.template){
                template = $templateCache.get(isaConfig.getTemplates("isaPassengerPicker")[scope.template]);
            }
            template = angular.element(template);
            element.append(template);
            $compile(template)(scope);

            scope.showError = false;
            scope.errorMessage = "";
        };

        directive.controller = [ '$scope', function ($scope) {
            $scope.showDropDown = function () {
                $scope.isOpen = true;
            };

            $scope.hideDropDown = function () {
                $scope.showError = false;
                $scope.isOpen = false;
            };

            $scope.addPassenger = function (key) {
                var validityMessage = $scope.isValid($scope.passengerDetails[key].value + 1, key);
                if (validityMessage.validity) {
                    $scope.passengerDetails[key].value++;
                    $scope.showError = false;
                } else {
                    $scope.activateError(validityMessage.message);
                }

                $scope.paxDetailsChange();
            };

            $scope.removePassenger = function (key) {
                var validityMessage = $scope.isValid($scope.passengerDetails[key].value - 1, key);
                if (validityMessage.validity) {
                    $scope.passengerDetails[key].value--;
                    $scope.showError = false;
                } else {
                    $scope.activateError(validityMessage.message);
                }

                $scope.paxDetailsChange();
            };

            $scope.activateError = function (message) {
                $scope.showError = true;
                $scope.errorMessage = message;

                $timeout(function () {
                    $scope.showError = false;
                }, 2500);
            };

            //These rules can be moved to controller where the directive is called if being generalized
            $scope.isValid = function (newVal, type) {

                var isValidMessage = {
                    validity: true,
                    message: ""
                };

                if (type === 'adult') {
                    //When removing adults, the number of infants should be reduced accordingly
                    if (newVal < $scope.passengerDetails.infant.value && newVal >= $scope.passengerDetails[type].min) {
                        $scope.passengerDetails.infant.value = newVal;
                    }

                    if (newVal + $scope.passengerDetails['child'].value + 1 > 10) {
                        isValidMessage.validity = false;
                        isValidMessage.message = "Adults and Children total should be 9 or less";
                        return isValidMessage;
                    }
                }

                if (type === 'child') {
                    if ($scope.passengerDetails['adult'].value + newVal + 1 > 10) {
                        isValidMessage.validity = false;
                        isValidMessage.message = "Adults and Children total should be 9 or less";
                        return isValidMessage;
                    }
                }

                if (type === 'infant' && newVal > $scope.passengerDetails['adult'].value) {
                    isValidMessage.validity = false;
                    isValidMessage.message = "Only one infant per adult is allowed";
                    return isValidMessage;
                }

                if (newVal <= $scope.passengerDetails[type].max && newVal >= $scope.passengerDetails[type].min) {
                    return isValidMessage;
                } else {
                    isValidMessage.validity = false;
                    isValidMessage.message = "The " + type + " count should be between " + $scope.passengerDetails[type].min +
                        " and " + $scope.passengerDetails[type].max;
                    return isValidMessage;
                }

                return isValidMessage;
            };

            //Regenerate the value shown in the input
            $scope.paxDetailsChange = function () {
                var displayText = "";
                angular.forEach($scope.passengerDetails, function (paxType) {
                    var label = "";
                    if (paxType.value === 1) {
                        label = paxType.labelSingle;
                    } else {
                        label = paxType.labelMulti;
                    }

                    displayText = displayText + paxType.value + " " + label + " ";
                });

                $scope.displayText = displayText;
            };
            $scope.paxDetailsChange();

            $scope.clickOutside = function(){
                $scope.hideDropDown();
            };

            $scope.onItemClick = function($event){
                $event.target.select();
            };

            $scope.passengerAmountChange = function(newValue, oldValue, key){
                newValue = Number(newValue);
                oldValue = Number(oldValue);

                if(_.isNaN(newValue)){
                    $scope.activateError("Value must be set to a number");
                    $scope.passengerDetails[key].value = oldValue;
                    return;
                }

                var validityMessage = $scope.isValid(newValue, key);
                if (validityMessage.validity) {
                    $scope.passengerDetails[key].value = newValue;
                    $scope.showError = false;
                } else {
                    $scope.activateError(validityMessage.message);
                    $scope.passengerDetails[key].value = oldValue;
                    return;
                }

                $scope.paxDetailsChange();
            }
        }];

        return directive;
    }];
    isa_passenger_picker.directive(directives);
})();
;'use strict';

(function () {
    var isa_passenger_details = angular.module("isaPassengerDetails", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaPassengerDetails = [
        '$compile',
        '$http',
        '$state',
        '$templateCache',
        'constantFactory',
        'modifyReservationService',
        'languageService',
        'currencyService',
        'applicationService',
        'commonLocalStorageService',
        '$translate',
        'dateTransformationService',
        '$sce',
        'formValidatorService',
        function ($compile, $http, $state, $templateCache, constantFactory, modifyReservationService, languageService,
                  currencyService, applicationService, commonLocalStorageService, $translate,dateTransformationService,$sce) {
            var directive = {};

            directive.restrict = 'AEC';

            directive.scope = {
                applicationMode: '@',
                passenger: '=',
                selectedPassenger: '=',
                passengerCount: '@',
                passengerList: '=',
                updateContact: '&',
                passengerCompleted: '=',
                onModificationCompleted: '&',
                paxDetailConfig: '=',
                firstPaxContact: '=',
                submitted: '=',
                airewardsChecked:'=',
                updateAirewardsStatus : '&',
                onModificationStart : '&',
                countryList : '=',
                isAirwordEnabled : '=',
                validationMessages: '=',
                carrier:'=',
                language: '='
            };

            directive.link = function (scope, element) {
                //Setting passenger detail template
                var template = $templateCache.get(isaconfig.getTemplates("isaPassengerDetails")['default']);

                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
            };
            directive.controller = [
                '$scope', '$element', '$rootScope', 'datePickerService', '$location', '$anchorScroll', '$timeout', '$filter','$translate','formValidatorService',
                function ($scope, $element, $rootScope, datePickerService, $location, $anchorScroll, $timeout, $filter,$translate, formValidatorService) {
                    var PAX_TYPE = {
                        'Adult': 'AD',
                        'Child': 'CH',
                        'Infant': 'IN'
                    };

                    var dateFormat = "DD/MM/YYYY";
                    //dto
                    $scope.ageRules = {
                        Adult: {
                            min: 12,
                            max: 99
                        },
                        Child: {
                            min: 2,
                            max: 12
                        },
                        Infant: {
                            min: 0,
                            max: 2
                        }
                    };

                    $scope.requiredRules = {};
                    //GET FORM VALIDATIONS
                    $scope.paxValidation = formValidatorService.getValidations();
                    
                    //variables
                    $scope.dobInput = '';
                    $scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                    $scope.isLargeDevice = $rootScope.deviceType.isLargeDevice;
                    $scope.appMode =  applicationService.getApplicationMode();
                    $scope.isNameChange = ($scope.applicationMode === 'nameChange');
                    $scope.nationalityList = constantFactory.Nationalities();
                    $scope.countryList = constantFactory.Countries();
                    $scope.isPersianCalendarType = applicationService.getIsPersianCalendar();

                    $scope.nationalityNameList = _.map($scope.nationalityList, function(nationality){
                        return nationality.name;
                    });
                    $scope.countryNameList = _.map($scope.countryList, function(country){
                        return country.name;
                    });

                    $scope.mod = {};
                    $scope.lastPassenger = false;
                    $scope.salutationList = [];

                    $scope.minDob = new Date();
                    $scope.maxDob = new Date();
                    $scope.dataTip = "";
                    
                    init();
            
                    function init() {                
                        $translate.onReady().then(function () {
                            // $scope.dataTip =$translate.instant('msg_passport_format');
                        //$sce.dataTip = $sce.trustAsHtml(value);
                        $scope.dataTip = $sce.trustAsHtml($translate.instant('msg_passport_format'));
                        
                        });
                        
                    }            
                        


                    //iife
                    (function (){
                       // console.log($scope.isAirwordEnabled);
                       
                        var flightData = commonLocalStorageService.getConfirmedFlightData();
                        if(flightData !== null){
                        var firstDepartureDate = flightData[0].flight.departureDate;
                        var lastArrivalDate = flightData[Object.keys(flightData).length - 1].flight.arrivalDate;     
                        }
                       
                        //Do the changes required changes to paxValidation
                        if($scope.isNameChange){
                            $scope.paxValidation.Adult.nationality.required = {};
                            $scope.paxValidation.Child.nationality.required = {};
                            $scope.paxValidation.Infant.nationality.required = {};
                        }

                        var ageRulesForPax = $scope.ageRules[$scope.passenger.category];
                        var lastArrivalDateIso8601 = moment(lastArrivalDate, dateFormat).format(dateFormat);
                        $scope.maxDate = moment(lastArrivalDateIso8601, dateFormat).subtract(ageRulesForPax.min, 'years');

                        // Min date is reduced by one day
                        $scope.minDate = moment(lastArrivalDateIso8601, dateFormat).subtract(ageRulesForPax.max, 'years');
                        $scope.maxDate = $scope.maxDate.subtract(1, 'days');

                        if($scope.passenger.category === "Infant"){
                        	 var firstDepartureDateIso8601 = moment(firstDepartureDate, dateFormat).format(dateFormat);
                            $scope.maxDate = moment(firstDepartureDateIso8601, dateFormat).subtract(3, 'days');

                            // Max dob of infant must always be on or before the day that the booking is done
                            if(moment() < $scope.maxDate){
                                $scope.maxDate = moment();
                            }
                        }
                        
                        $scope.minDate = $scope.minDate.toDate();
                        $scope.maxDate = $scope.maxDate.toDate();
                        $scope.minExpiry = moment().toDate();
                        $scope.maxExpiry = moment().add(10, 'years').toDate();
                        $scope.minVisaIssue = moment().subtract(5,'years').toDate();
                        $scope.maxVisaIssue = moment().add(5,'years').toDate();
                        var calenderType="gregorian";
                        $scope.minDateForCal="";
                        $scope.maxDateForCal="";
                        
                        $scope.minDateForExpiry="";
                        $scope.maxDateForExpiry="";
                        
                        $scope.minDateForVissaIssue="";
                        $scope.maxDateForVissaIssue="";
                        
                    	var defautlValueForDob=$scope.maxDate.getDate()+"/"+$scope.maxDate.getMonth()+"/"+$scope.maxDate.getFullYear();
                    	
                    	var defautlValueForExpiry=$scope.maxExpiry.getDate()+"/"+$scope.maxExpiry.getMonth()+"/"+$scope.maxExpiry.getFullYear();
                    	
                    	var defautlValueForVissaIssue= $scope.maxVisaIssue.getDate()+"/"+$scope.maxVisaIssue.getMonth()+"/"+$scope.maxVisaIssue.getFullYear();
                    	
//                    	if(languageService.getSelectedLanguage() == 'fa'){
                        if($scope.isPersianCalendarType){
                          calenderType="jalali";
                        	var jalaaliObjectForMaxDate = getJalaaliDate($scope.maxDate.getFullYear(),$scope.maxDate.getMonth(),$scope.maxDate.getDate());
                        	var jalaaliObjectForMinDate = getJalaaliDate($scope.minDate.getFullYear(),$scope.minDate.getMonth(),$scope.minDate.getDate());
                        	
                        	var jalaaliObjectForMaxExpiry =getJalaaliDate($scope.maxExpiry.getFullYear(),$scope.maxExpiry.getMonth(),$scope.maxExpiry.getDate());
                        	var jalaaliObjectForMinExpiry = getJalaaliDate($scope.minExpiry.getFullYear(),$scope.minExpiry.getMonth(),$scope.minExpiry.getDate());
                        	
                        	var jalaaliObjectForMaxVisaIssue =getJalaaliDate($scope.maxVisaIssue.getFullYear(),$scope.maxVisaIssue.getMonth(),$scope.maxVisaIssue.getDate());
                        	var jalaaliObjectForMinVisaIssue = getJalaaliDate($scope.minVisaIssue.getFullYear(),$scope.minVisaIssue.getMonth(),$scope.minVisaIssue.getDate());
                        	
                        	
                        	$scope.maxDateForCal = "\'"+jalaaliObjectForMaxDate.jd+"/"+jalaaliObjectForMaxDate.jm+"/"+jalaaliObjectForMaxDate.jy+"\'";
                        	$scope.minDateForCal= "\'"+jalaaliObjectForMinDate.jd+"/"+jalaaliObjectForMinDate.jm+"/"+jalaaliObjectForMinDate.jy+"\'";
                        	
                        	$scope.maxDateForExpiry ="\'"+jalaaliObjectForMaxExpiry.jd+"/"+jalaaliObjectForMaxExpiry.jm+"/"+jalaaliObjectForMaxExpiry.jy+"\'"; 
                        	$scope.minDateForExpiry= "\'"+jalaaliObjectForMinExpiry.jd+"/"+jalaaliObjectForMinExpiry.jm+"/"+jalaaliObjectForMinExpiry.jy+"\'";
                        	
                        	$scope.maxDateForVissaIssue ="\'"+jalaaliObjectForMaxVisaIssue.jd+"/"+jalaaliObjectForMaxVisaIssue.jm+"/"+jalaaliObjectForMaxVisaIssue.jy+"\'"; 
                        	$scope.minDateForVissaIssue= "\'"+jalaaliObjectForMinVisaIssue.jd+"/"+jalaaliObjectForMinVisaIssue.jm+"/"+jalaaliObjectForMinVisaIssue.jy+"\'";
                        	
                        	
                        	defautlValueForDob = jalaaliObjectForMaxDate.jd+"/"+jalaaliObjectForMaxDate.jm+"/"+jalaaliObjectForMaxDate.jy;
                        	
                        	defautlValueForExpiry = jalaaliObjectForMaxExpiry.jd+"/"+jalaaliObjectForMaxExpiry.jm+"/"+jalaaliObjectForMaxExpiry.jy;
                        	
                        	defautlValueForVissaIssue = jalaaliObjectForMaxVisaIssue.jd+"/"+jalaaliObjectForMaxVisaIssue.jm+"/"+jalaaliObjectForMaxVisaIssue.jy;
                        	
                        }else{ 	
                        	console.log($scope.maxDate.getDate());
                        	$scope.maxDateForCal = "\'"+$scope.maxDate.getDate()+"/"+$scope.maxDate.getMonth()+"/"+$scope.maxDate.getFullYear()+"\'";
                        	$scope.minDateForCal= "\'"+$scope.minDate.getDate()+"/"+$scope.minDate.getMonth()+"/"+$scope.minDate.getFullYear()+"\'";
                        	
                        	$scope.maxDateForExpiry = "\'"+$scope.maxExpiry.getDate()+"/"+$scope.maxExpiry.getMonth()+"/"+$scope.maxExpiry.getFullYear()+"\'";
                        	$scope.minDateForExpiry= "\'"+$scope.minExpiry.getDate()+"/"+$scope.minExpiry.getMonth()+"/"+$scope.minExpiry.getFullYear()+"\'";
                        }

                        $scope.salutationList = constantFactory.PaxTypeWiseTitle()[ PAX_TYPE[$scope.passenger.category] ];

                        $scope.passportExpiry = {
                            changeYear: true,
                            changeMonth: true,
                            minDate:  $scope.minExpiry,
                            maxDate: $scope.maxExpiry,
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true

                        }
                        $scope.visaIssue = {
                            changeYear: true,
                            changeMonth: true,
                            minDate:  $scope.minVisaIssue,
                            maxDate: $scope.maxVisaIssue,
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true

                        }

                        $scope.dateOptions = {
                            changeYear: true,
                            changeMonth: true,
                            minDate: $scope.minDate,
                            maxDate: $scope.maxDate,
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            yearRange: "-100:+0",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true,
                            isRTL: $scope.isRtl
                        };
                        
                        $scope.admOptions = {
                        		calType: calenderType,
                        		dtpType:"date",
                        		zIndex:9,
                                autoClose:true,
                                format: "DD/MM/YYYY",
                                multiple:false,
                                default:defautlValueForDob
                        		
                            };
                        
                        $scope.passportExpiry1 = {
                        		calType: calenderType,
                        		dtpType:"date",
                        		zIndex:9,
                                autoClose:true,
                                format: "DD/MM/YYYY",
                                multiple:false,
                                default:defautlValueForExpiry
                        		
                            };
                        
                        $scope.visaIssue1 = {
                        		calType: calenderType,
                        		dtpType:"date",
                        		zIndex:9,
                                autoClose:true,
                                format: "DD/MM/YYYY",
                                multiple:false,
                                default:defautlValueForVissaIssue
                        		
                            };
                    }());
                    $scope.formatLabelCountry = function(model) {
                        var tempN = $scope.countryList;
                        if (tempN) {
                            for (var i = 0; i < tempN.length; i++) {
                                if (model === tempN[i].code) {
                                    return tempN[i].name;
                                    break;
                                }
                            }
                        }
                    }

                    //functions
                    $scope.onAirwardsEmailFocus = function(index){
                        $scope.onModificationStart({data:{index:index,valFieald:'isLMSValidationDone'}});
                    }
                    function stringOrEmpty(entity) {
                        return entity || "";
                    }
                    $scope.onairewardsEmailChange = function(index){
                        $scope.onModificationStart({data:{index:index,valFieald:'isLMSValidationDone'}});
                    }
                    $scope.datePickerOnClick = function () {
                        $scope.mod.dobOpen = true;
                    };

                    $scope.nextPassenger = function (passenger, paxForm) {
                        paxForm.$setSubmitted(true);
                        if (passenger.valid) {
                            $scope.selectedPassenger = $scope.selectedPassenger + 1;
                        }
                    };

                    $scope.setSalutation = function (salutation) {
                        $scope.passenger.salutation = salutation.titleCode;
                    };

                    $scope.setNationality = function (nationality) {
                        $scope.passenger.nationality = nationality;
                    };

                    $scope.formatLabelCitizen = function (model) {
                        if ($scope.nationalityList) {
                            for (var i = 0; i < $scope.nationalityList.length; i++) {
                                if (model === $scope.nationalityList[i].code) {
                                    return $scope.nationalityList[i].name;
                                }
                            }
                        }
                    };
					function getJalaaliDate(year,month,date){
						return dateTransformationService.toJalaali(year,month,date);
					}
                    $scope.formatLabelNationality = function (model) {
                        var tempN = this.nationalityList;
                        if (tempN) {
                            for (var i = 0; i < tempN.length; i++) {
                                if (model == tempN[i].code) {
                                    return tempN[i].name;
                                }
                            }
                        }
                    };

                    $scope.setTravelDocument = function(type){
                        $scope.passenger.travelDocumentType = type;
                    }

                    $scope.addInfantToParent = function(parentSequence, infantSequence) {
                        for (var i = 0; i < $scope.passengerList.length; i++) {
                            if ($scope.passengerList[i].paxSequence === parentSequence) {
                                $scope.passengerList[i].infantSequence = infantSequence;
                            }
                        }
                    };

                    $scope.removeInfantFromParent = function(parentSequence){
                        for (var i = 0; i < $scope.passengerList.length; i++) {
                            if ($scope.passengerList[i].paxSequence === parentSequence) {
                                $scope.passengerList[i].infantSequence = null;
                            }
                        }
                    };

                    $scope.setTravelingWith = function (travelingWith) {
                        //If traveling with was already set, we should remove the binding form the parent

//                        console.log($scope.passenger.travelingWith);
                        if(!_.isUndefined($scope.passenger.travelingWith)){
                            var currentParent = $scope.passenger.travelingWith;
                            $scope.removeInfantFromParent(currentParent);
                        }

                        $scope.passenger.travelingWith = travelingWith;
                        $scope.addInfantToParent(travelingWith, $scope.passenger.paxSequence);
                    };

                    $scope.scrollToContactDetails = function (passenger, paxForm) {
                        paxForm.$setSubmitted(true);
                        if (passenger.valid) {
                            $scope.passengerCompleted = true;

                            /**
                             * If user is in modification flow(Name Change)
                             * Prevent navigate to contact info page
                             */
                            if ($scope.appMode === 'nameChange') {
                                $scope.onModificationCompleted();
                            } else {
                                $timeout(function () {
                                    var element = $(".waypoint_paxContact");
                                    $("html, body").animate({
                                        scrollTop: element.offset().top - 94
                                    }, 600);
                                }, 10);
                            }
                        }
                    };
                    $scope.scrollToNationality = function () { 
                        if ($scope.isMobileDevice) {
                            // var element = $('#nationality');
                            // $("html, body").animate({
                            //     scrollTop: element.offset().top - 94
                            // }, 600);
                        } else {
                            //do nothing
                        }                        
                    };


                    $scope.openDate = function(){
                        var dob = angular.element("#pax-dob");

                        if(dob.is(':focus')){
                            dob.trigger('blur');
                        } else {
                            dob.trigger('focus');
                        }

                    };

                    $scope.backToReservationModication = function () {

                        // Get modify search criteria.
                        var modifySearchParams = modifyReservationService.getReservationSearchCriteria();

                        // Change state to modify reservation.
                        $state.go('modifyReservation', {
                            lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode(),
                            pnr: modifySearchParams.pnr,
                            lastName: modifySearchParams.lastName,
                            departureDate: modifySearchParams.departureDate
                        });
                    };

                    $scope.startsWith = function (state, viewValue) {
                        if(viewValue === " " || viewValue ===  null){
                            return true;
                        }else{
                            return state.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
                        }
                    };

                    $scope.getPassengerName = function(paxIndex){
                        var passenger = _.find($scope.passengerList,
                            function(pax) {
                                return pax.paxSequence == paxIndex;
                            }
                        );
                        if(!_.isUndefined(passenger) && !_.isUndefined(passenger.displayName)){
                            return passenger.displayName;
                        }else{
                            return 'Traveling With';
                        }
                    };

                    //Setting lastPassenger to true if selected index is equal to passengerCount
                    $scope.$watch('selectedPassenger', function (value) {
                        $scope.lastPassenger = $scope.selectedPassenger === $scope.passengerCount - 1;
                    });

                    //Setting the display name based on the current model value changes
                    $scope.$watch('passenger.firstName + passenger.lastName + passenger.salutation', function () {
                        if ($scope.passenger.salutation || $scope.passenger.firstName || $scope.passenger.lastName) {

                            var salutation =  $filter('listFilter')($scope.passenger.salutation,
                                $scope.salutationList, "titleCode", "titleName");

                            $scope.passenger.displayName = stringOrEmpty(salutation) + " "
                                + stringOrEmpty($scope.passenger.firstName) + " "
                                + stringOrEmpty($scope.passenger.lastName);
                        } else {
                            $scope.passenger.displayName = $scope.passenger.category;

                            $translate.onReady(function(){
                                var label = $translate.instant('lbl_common_' + $scope.passenger.category.toLowerCase());

//                                if(label != 'lbl_passenger_' + $scope.passenger.category.toLowerCase()){
//                                    label = $scope.passenger.category;
//                                }

                                var categoryPaxCount = {};

                                angular.forEach($scope.passengerList, function(passenger, key){
                                    if(_.isUndefined(categoryPaxCount[$scope.passengerList[key].category])){
                                        categoryPaxCount[passenger.category] = 1;
                                    }else{
                                        categoryPaxCount[passenger.category] = categoryPaxCount[passenger.category] + 1;
                                    }

                                    if($scope.passenger.paxSequence === passenger.paxSequence){
                                        $scope.passenger.displayName = label
                                            + " " + (categoryPaxCount[passenger.category] || "");
                                    }
                                })
                            });

                        }
                    });

                    //Setting the display name based on the current model value changes
                    $scope.$watch('passenger.submitted', function () {
                        if($scope.passenger.submitted){
                            $scope['userForm_' + $scope.selectedPassenger].$setSubmitted(true);
                        }
                    });

                    //Checks validity of the specific passenger of form change.
                    $scope.$watch('userForm_' + $scope.selectedPassenger + '.$valid', function (validity) {
                        $scope.passenger.valid = validity;
                    });

                    //If the user starts filling a form they should no longer be allowed to switch between
                    $scope.$watch('userForm_' + $scope.selectedPassenger + '.$dirty', function (dirty) {
                        $scope.passenger.dirty = dirty;
                    });

                    //Watch for changes in the first passenger and update the values in contact form accordingly
                    $scope.$watchCollection('passengerList[0]', function (newVal, oldVal) {
                        //Only call the function if the values have changed
                        if (newVal !== oldVal && $scope.firstPaxContact) {
                            var changedPassenger = {};

                            if (oldVal.firstName !== newVal.firstName) {
                                changedPassenger.firstName = newVal.firstName;
                            }
                            if (oldVal.lastName !== newVal.lastName) {
                                changedPassenger.lastName = newVal.lastName;
                            }
                            if (oldVal.salutation !== newVal.salutation) {
                                changedPassenger.salutation = newVal.salutation;
                            }
                            if (oldVal.nationality !== newVal.nationality) {
                                changedPassenger.nationality = newVal.nationality;
                            }
                            if (oldVal.dob !== newVal.dob) {
                                changedPassenger.dob = newVal.dob;
                            }
                            $scope.updateContact({passenger: changedPassenger});
                        }
                    });
                    $scope.updateReward = function(index){
                        $scope.updateAirewardsStatus({index:index.index});
                      //  $scope.airewardsChecked = !$scope.airewardsChecked;

                    }

                }];
            return directive;
        }];
    isa_passenger_details.directive(directives);
})();
// $scope.paxValidation = {
//     Adult: {
//         firstName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_firstNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
//                 value: "2",
//                 message: "lbl_validation_firstNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': "lbl_validation_firstNameSymbol"
//             }
//         },
//         lastName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_lastNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "2",
//                 message: "lbl_validation_lastNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': "lbl_validation_lastNameSymbol"
//             }
//         },
//         dob: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.dob.mandatory[passenger.type]",
//                 message: "lbl_validation_dobRequired"
//             }
//         },
//         nationality: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationality.mandatory[passenger.type]",
//                 message: "lbl_validation_nationalityRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{nationalityNameList}}",
//                 message: "lbl_validation_nationalityInList"
//             }
//         },
//         'nationalIDNo':{
//             'required': {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationalIDNo.mandatory[passenger.type]",
//                 message: "lbl_validation_national_Id_Number_Required"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_national_Id_Number_Pattern'
//             }

//         },
//         'salutation': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: 'lbl_validation_titleRequired'
//             }
//         },
//         'airewardsEmail': {
//             'pattern':{
//                 name: 'pattern',
//                 value: '^[a-z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$',
//                 message: "lbl_validation_airewardsEmail"
//             }
//         },
//         'passportNo': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
//                 message: "lbl_validation_passportRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "7",
//                 message: "lbl_validation_passportNumMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_passportPattern'
//             }
//         },
//         'passportExpiry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportExpiry.mandatory[passenger.type]",
//                 message: "{{::(carrier == 'W5')?validationMessages.passportExpiry + ' for Adult ' + ' # ' + passenger.paxSequence : validationMessages.passportExpiry}}"

//             }
//         },
//         'passportIssuedCntry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportIssuedCntry.mandatory[passenger.type]",
//                 message: "lbl_validation_countryRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{countryNameList}}",
//                 message: "lbl_validation_countryInList"
//             }
//         }

//     },
//     Child: {
//         firstName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_firstNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
//                 value: "2",
//                 message: "lbl_validation_firstNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_firstNameSymbol'
//             }
//         },
//         lastName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_lastNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "2",
//                 message: "lbl_validation_lastNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_lastNameSymbol'
//             }
//         },
//         dob: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.dob.mandatory[passenger.type]",
//                 message: "lbl_validation_dobRequired"
//             }
//         },
//         nationality: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationality.mandatory[passenger.type]",
//                 message: "lbl_validation_nationalityRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{nationalityNameList}}",
//                 message: "lbl_validation_nationalityInList"
//             }
//         },
//         'nationalIDNo':{
//             'required': {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationalIDNo.mandatory[passenger.type]",
//                 message: "lbl_validation_national_Id_Number_Required"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_national_Id_Number_Pattern'
//             }
//         },
//         'salutation': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: 'lbl_validation_titleRequired'
//             }
//         },
//         'passportNo': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
//                 message: "lbl_validation_passportRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "7",
//                 message: "lbl_validation_passportNumMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_passportPattern'
//             }
//         },
//         'airewardsEmail': {
//             'pattern':{
//                 name: 'pattern',
//                 value: '^[a-z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$',
//                 message: 'lbl_validation_airewardsEmail'
//             }
//         },
//         'passportExpiry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportExpiry.mandatory[passenger.type]",
//                 message: "{{::(carrier == 'W5')?validationMessages.passportExpiry + ' for Child ' + ' # ' + passenger.paxSequence : validationMessages.passportExpiry}}"

//             }
//         }

//     },
//     Infant: {
//         'travelWith': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: 'lbl_validation_travelingWith'
//             }
//         },
//         firstName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_firstNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
//                 value: "2",
//                 message: "lbl_validation_firstNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_firstNameSymbol'
//             }
//         },
//         lastName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_lastNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "2",
//                 message: "lbl_validation_lastNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_lastNameSymbol'
//             }
//         },
//         dob: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.dob.mandatory[passenger.type]",
//                 message: "lbl_validation_dobRequired"
//             }
//         },
//         nationality: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationality.mandatory[passenger.type]",
//                 message: "lbl_validation_nationalityRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{nationalityNameList}}",
//                 message: "lbl_validation_nationalityInList"
//             }
//         },
//         'salutation': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_titleRequired"
//             }
//         },
//         'passportNo': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
//                 message: "lbl_validation_passportRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "7",
//                 message: "lbl_validation_passportNumMinChar"
//             }
//         },
//         'passportExpiry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportExpiry.mandatory[passenger.type]",
//                 message: "{{::(carrier == 'W5')?validationMessages.passportExpiry + ' for Infant ' + ' # ' + passenger.paxSequence : validationMessages.passportExpiry}}"

//             }
//         },
//         'nationalIDNo':{
//             'required': {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationalIDNo.mandatory[passenger.type]",
//                 message: "lbl_validation_national_Id_Number_Required"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_national_Id_Number_Pattern'
//             }

//         }

//     }
// };
;/**
 * Created by nipuna on 11/23/15.
 */
'use strict';

(function () {
    var isa_passenger_list = angular.module("isaPassengerList", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaPassengerList = [
        '$compile', '$http', '$templateCache', '$document', '$rootScope', '$translate','alertService',
        function ($compile, $http, $templateCache, $document, $rootScope, $translate,alertService) {
            var directive = {};

            directive.restrict = 'AEC';
            directive.scope = {
                optionClick: '&',
                optionClickparent: '&',
                passengerList: '=',
                selectedPassenger: '=',
                passengerChange: '&',
                onActionClick: '&',
                applicationMode: '@',
                passenger: '=',
                passengerCount: '@',
                updateContact1: '&',
                passengerCompleted: '=',
                onModificationCompletedparent: '&',
                paxDetailConfig: '=',
                firstPaxContact: '=',
                submitted: '=',
                airewardsChecked: '=',
                updateAirewardsStatus1: '&',
                onModificationStart: '&',
                countryList: '=',
                isAirwordEnabled: '=',
                validationMessages: '=',
                carrier: '=',
                deviceMode: '=',
                source: '=',
                template: '@',
                selectedItems: '=',
                passengerType: '=',
                outerIndex: '=',
                service: '@',
                addTransferParent: '&',
                removeTransferParent: '&',
                validations: '=',
                currentSelection: '=',
                language: '='
            };


            directive.link = function (scope, element, attr) {
                var type = (attr.listType === undefined) ? 'default' : attr.listType;
                scope.isActive = false;

                scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                $translate.onReady().then(function () {
                    scope.add = $translate.instant('lbl_common_add');
                    scope.remove = $translate.instant('lbl_common_remove');
                });

                var categoryPaxCount = {};

                //Set the display names to category if not defined
                angular.forEach(scope.passengerList, function (value, key) {
                    if (!scope.passengerList[key].displayName) {

                        if (_.isUndefined(categoryPaxCount[scope.passengerList[key].category])) {
                            categoryPaxCount[scope.passengerList[key].category] = 1;
                        } else {
                            categoryPaxCount[scope.passengerList[key].category] = categoryPaxCount[scope.passengerList[key].category] + 1;
                        }
                        var translatedDisplayName = scope.passengerList[key].category;
                        if (scope.passengerList[key].category == 'Adult') {
                            translatedDisplayName = $translate.instant('lbl_common_adult');
                        } else if (scope.passengerList[key].category == 'Child') {
                            translatedDisplayName = $translate.instant('lbl_common_child');
                        } else if (scope.passengerList[key].category == 'Infant') {
                            translatedDisplayName = $translate.instant('lbl_common_infant');
                        }

                        scope.passengerList[key].displayName = translatedDisplayName + " " +
                                (categoryPaxCount[scope.passengerList[key].category] || "");
                    }
                });

                var template = $templateCache.get(isaconfig.getTemplates("isaPassengerList")[type]);
                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);

                scope.optionOnClick = function (options, $event) {
                    scope.optionClickparent({id: options.id, index: options.outerindex});

                    scope.selectedIndex = options.id;

                    if (scope.source[options.index].selected === true) {
                        scope.source[options.index].selected = false;
                    } else {
                        scope.source[options.index].selected = true;
                    }
                    if ($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                };




                scope.accordionArray = [];

                scope.changeSelected = function (e, event) {
                    //alert("into changeselected")
                    if (type === "seatmap") {
                        scope.selectedPassenger = e;
                    } else {
                        if (scope.passengerList[scope.selectedPassenger].dirty === true && scope.passengerList[scope.selectedPassenger].valid === false) {
                        	var message = "lbl_passenger_fillAllPaxDetail";
                            alertService.setAlert(message, 'warning');
                            scope.passengerList[scope.selectedPassenger].submitted = true;
                            if (event)
                                event.stopPropagation();
                        } else {
                            scope.selectedPassenger = e;
                        }
                    }
                    scope.passengerChange({pax: e});
                };
                scope.updateContact = function (passenger) {
                    scope.updateContact1({passenger: passenger});
                }
                scope.updateAirewardsStatus = function (index) {
                    scope.updateAirewardsStatus1({index: index});
                }
                scope.actionClick = function ($event, paxIndex) {
                    $event.stopPropagation();
                    scope.onActionClick({paxIndex: paxIndex});
                }
                scope.addTransfer = function (item, comment, dateValidations) {
                    scope.addTransferParent({item: item, comment: comment, dateValidations: dateValidations});
                }
                scope.removeTransfer = function (item) {
                    scope.removeTransferParent({item: item});

                };
                scope.onModificationCompleted = function () {
                    scope.onModificationCompletedparent();
                }
                /**
                 * Show More Passenger Popup
                 */
                scope.showPopup = function () {

                    var popWidth = 275;
                    //var popHeight = 260;
                    scope.blockSection = true;
                    scope.popupPos = {
//                        "top": angular.element('#data').position().top,
//                        "left": angular.element('#data').position().left,
                        "width": popWidth,
                        "height": "auto"
                    };
                    scope.applyMask = {"display": "block", "z-index": 10};
                };

                /**
                 * Close Mode Passenger popup
                 */
                scope.closePopup = function () {
                    scope.applyMask = {"display": "hide", "z-index": -10};
                    scope.blockSection = false;
                    scope.isPopupVisible = true;
                    scope.isContinuClicked = false;
                };

            };
            directive.controller = ['$scope', '$element', '$rootScope', function ($scope, $element, $rootScope) {

                }];
            return directive;

        }];
    isa_passenger_list.directive(directives);
})();

;'use strict';

(function () {
    var isa_phone_number = angular.module("isaPhoneNumber", []);
    var directives = {};
    var isaConfig = new IsaModuleConfig();

    directives.isaPhoneNumber = ["$compile", "$http", "$templateCache", function ($compile, $http, $templateCache) {
        var directive = {};

        directive.restrict = 'AEC';

        directive.scope = {
            phoneNumber: '=',
            countryList: '=',
            phoneRequired: '=',
            fieldName: '=',
            formName: '@',
            labelVisible:'@labelVisible',
            configs:'=',
            validateWithList:'='
            
        };

        directive.link = function (scope, element) {
            var template = $templateCache.get(isaConfig.getTemplates("isaPhoneNumber")['default']);
            template = angular.element(template);
            element.append(template);
            $compile(template)(scope);

            scope.isLabelsVisible = !(scope.labelVisible === "false")

        };

        directive.controller = ["$scope","$element", function ($scope,$element) {
            $scope.classInput='';
            $scope.classAreaInput= '';
            $scope.countryCodeList = $scope.countryList.map(function(country){ return country.phoneCode});

            $scope.formatLabelCountry = function (model) {
                var tempN = $scope.countryList;
                if (tempN) {
                    for (var i = 0; i < tempN.length; i++) {
                        if (model === tempN[i].phoneCode) {
                            return tempN[i].phoneCode;
                        }
                    }
                }
            };

            $scope.onCountryChange = function(val){
                $scope.phoneNumber.countryCode = val.phoneCode;
            };
            /**
             *  Filter typeaheads according to view value.
             */
            $scope.startsWith = function(phoneName, viewValue) {
                if(viewValue === " "){
                    return true;
                }else{
                    return phoneName.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
                }
            }
        }];

        return directive;
    }];
    isa_phone_number.directive(directives);
})();
;/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function(){
    var isa_placeholder = angular.module("isaplaceholder", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaplaceholder = [
        '$compile', '$http','$templateCache','$timeout',
        function ($compile, $http,$templateCache,$timeout) {
        var directive = {};
        //load relavent tamplate according to the button type

        directive.restrict = 'AEC';
        directive.scope = {placeholderCaption: '@',
                           placeholderSymbol:'@',
                           isVisible:'=',
                           isInputDisabled:'=',
                           onChange:'=',
                           bindTo:'@'};
        directive.require = //other sibling directives;
        directive.link = function (scope, element, attr) {
            scope.onChange = "";
            scope.modelVal = null;
            //var template = isaconfig.getTemplates("isaplaceholder");
            var template = $templateCache.get(isaconfig.getTemplates("isaplaceholder")['default']);

            var returnNgModel = function(title, scopeObj){
                var ttArry = title.split("."), valueToSend = scopeObj;
                for (var i=0;i<ttArry.length;i++){
                    valueToSend = valueToSend[ttArry[i]];
                }
                return valueToSend || null;
            };

            template = angular.element(template);
            element.append(template);
            $compile(template)(scope);

            var modalTitle = element.find('input').attr("ng-model");
            var inputScope = angular.element(element.find('input')).scope();
            scope.modelVal = returnNgModel(modalTitle, inputScope);
            if(scope.modelVal!==null){
                $(element).find('.placeholder').hide();
            }

            /*$templateRequest(template['default'],[false]).then(function(html){
                var returnNgModel = function(title, scopeObj){
                    var ttArry = title.split("."), valueToSend = scopeObj;
                    for (var i=0;i<ttArry.length;i++){
                        valueToSend = valueToSend[ttArry[i]]
                    }
                    return valueToSend || null;
                }

                 var template = angular.element(html);
                 element.append(template);
                 $compile(template)(scope);
                var modalTitle = element.find('input').attr("ng-model");
                var inputScope = angular.element(element.find('input')).scope();
                scope.modelVal = returnNgModel(modalTitle, inputScope);
                    if(scope.modelVal!==null){
                        $(element).find('.placeholder').hide();
                    }
            });*/

            scope.removePlaceholder = function(){
                if(scope.isInputDisabled) {
                    scope.isVisible = false;
                    element.find('input').focus();
                    $(element).find('.placeholder').hide();
                }
            };
            element.find('input').on('blur', function(e) {
                if((element.find('input').val().length === 0) ){
                    scope.isVisible = true;
                    $(element).find('.placeholder').show();
                    $('[bind-to = '+scope.bindTo+']').find('.placeholder').show();
                }
            });
            element.find('input').on('change paste keyup focus', function(e) {
                scope.isVisible = false;
                $(element).find('.placeholder').hide();
                $('[bind-to = '+scope.bindTo+']').find('.placeholder').hide();
            });


        };
        return directive;

    }];

    isa_placeholder.directive('compileCallback',function(){
        return {
            priority: 1001,
            scope : {
                compileCallback: "&"
            },
            link: function (scope){
                scope.compileCallback();
            }
        };
    });

    isa_placeholder.controller('phController',[
        '$scope', '$element',
        function($scope,$element){
        $scope.cmpileDone = function(){
//            console.log('cmopile done---------------------------->>>');
        };
    }]);
    isa_placeholder.directive(directives);
})();

;'use strict';
(function () {
    var isa_progress = angular.module("isaProgress", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaProgress = [
        '$compile',
        '$templateCache',
        'ancillaryService',
        '$rootScope',
        function ($compile, $templateCache, ancillaryService, $rootScope) {
            var directive = {};
            directive.scope = {
                main: '=',

            };
            directive.restrict = 'AEC';
            directive.link = function (scope, element) {
                var template = $templateCache.get(isaconfig.getTemplates("isaProgress")['default']);
                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
            };
            directive.controller = ["$rootScope", '$scope', 'eventListeners', "commonLocalStorageService", 'applicationService', "ancillaryService","$translate","currencyService",
                function ($rootScope, $scope, eventListeners, commonLocalStorageService, applicationService, ancillaryService,$translate,currencyService) {
                    $scope.skipService = '';
                    $scope.extraNavTag = ''; 
                    $scope.type = '';
                    $scope.curencyTypes = commonLocalStorageService.getCurrencies(); 
                    $scope.defaultCurrencyType = currencyService.getSelectedCurrency() || "AED";
                    $scope.applicationMode = applicationService.getApplicationMode();
                    
                     $translate.onReady().then(function() {
                         $scope.modifyValue = $translate.instant('btn_selectFare_ModifySearch').length;
                     });
                    
                    $scope.showModifySearch = function () {
                        eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
                    }; 
                    
                    $scope.skipServices = function () {                       
                        ancillaryService.skipServices($scope.type); 
                        $scope.skipService = '';
                        $scope.extraNavTag = '';
                        $rootScope.extraType = '';
                    };

                    switch ($rootScope.extraType) {
                        case 'baggage' :
                            $scope.skipService = "btn_extra_skipBag";
                            $scope.extraNavTag = "lbl_header_nav_baggage";
                            $scope.type = 'BAGGAGE';
                            break;
                        case 'seats' :
                            $scope.skipService = "btn_extra_skipSeat_mobile";
                            $scope.extraNavTag = 'lbl_header_nav_seat';
                            $scope.type = 'SEAT';
                            break;
                        case 'meals' :
                            $scope.skipService = "btn_extra_skipMeal";
                            $scope.extraNavTag = 'lbl_header_nav_meals';
                            $scope.type = 'MEAL';
                            break;
                        case 'insurance' :
                            $scope.skipService = "btn_extra_skipIns";
                            $scope.extraNavTag = 'lbl_header_nav_insurance';
                            $scope.type = 'INSURANCE';
                            break;
                        case 'services' :
                            $scope.skipService = "btn_extra_skipSvc";
                            $scope.extraNavTag = 'lbl_header_nav_inFlightServices';
                            $scope.type = 'SSR_IN_FLIGHT';
                            break;
                        case 'airport_services' :
                            $scope.skipService = "btn_extra_skip";
                            $scope.extraNavTag = 'lbl_header_nav_airport_service';
                            $scope.type = 'SSR_AIRPORT';
                            break;
                        case 'flexibility' :
                            $scope.skipService = "btn_extra_skipFlexi";
                            $scope.extraNavTag = 'lbl_header_nav_flexi';
                            $scope.type = 'FLEXIBILITY';
                            break;
                        case 'airport_transfer' :
                            $scope.skipService = "btn_extra_skip";
                            $scope.extraNavTag = 'lbl_header_nav_airport_service';
                            $scope.type = 'AIRPORTTRANSFER';
                            break;
                    }

                }];
            return directive;
        }];
    isa_progress.directive(directives);
})();
;/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_radioOptionlist = angular.module("isaradiooptionlist", ['ngAnimate']);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaradiooptionlist = [
        '$compile', '$http', '$templateCache', 'currencyService','constantFactory',
        function ($compile, $http, $templateCache, currencyService,constantFactory) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                template: '@',
                availableOptions: '=',
                source: '=',
                previousSource: '=',
                preferedsource:'=',
                setOption: '=',
                selectOption: '&',
                level: '=',
                type: '@',
                headerValue: '@',
                iconClickpm: '&',
                iconClick: '&',
                listType: '@'
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    //scope.selectedSection = "Outbound";

                    var template = $templateCache.get(isaconfig.getTemplates("isaradiooptionlist")[scope.template || 'default']);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);
                    //TODO refactor this using isolalte scope
                    scope.currencyType = currencyService.getSelectedCurrency();
                    scope.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');

                    // If array of prices provided get the appropriate charge
                    scope.getPriceForPax = function (passenger, priceList) {
                        var paxType = passenger.type;
                        if(passenger.infantSequence) {
                            paxType ='ADULT_WITH_INFANT';
                        }

                        var amountForPax = _.findWhere(priceList, {chargeBasis: paxType.toUpperCase()});
                        if(amountForPax){
                            return amountForPax.amount;
                        }
                    }

                };
            return directive;

        }];

    isa_radioOptionlist.controller('radioOptionListController', ['$scope', '$element', function ($scope, $element) {
        //$('#radio-Outbound-0').prop('checked', true);
        $scope.$watch('setOption', function (newval, oldval) {
//            console.log("changed!" + JSON.stringify(newval));
            if (newval !== undefined) {
                $scope.selectedIndex = newval.index;
                $scope.selectedSection = newval.section;
                $('#radio-0' + '-' + newval.index).prop('checked', true);
            }
        });

        $scope.optionOnSelect = function (event, index, section) {
            // var type = $(event.currentTarget).data('section-key');
            $scope.selectedIndex = index;
            $scope.selectedSection = index;
            $scope.isSectionSelected(section);
            var sectorData = {index: index, section: index};
            $scope.selectOption(sectorData);
        };

        $scope.isSectionSelected = function (section) {
            var isselected = ($scope.selectedSection === section) ? true : false;
            return isselected;
        };

        $scope.getSource = function (obj) {
            return obj[$scope.type];
        };

        $scope.getLabel = function (obj, key) {
            return obj[Object.keys(obj)[key]];
        };

        $scope.capitalizeFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        };
        $scope.onChildIconClick = function (el, event, index) {
//            console.log(el);
            $scope.iconClick({element: el, event: event, index: index});
        }
        $scope.onChildIconClickPM = function (el, event, index) {
          $scope.iconClickpm({element: el, event: event, index: index});
      }
        $scope.getLength = function (arr) {
            var length = Object.keys(arr).length - 1;
            return length;
        }


    }]);
    isa_radioOptionlist.directive(directives);
})();

;'use strict';

(function() {
	var isa_saved_card = angular.module("isaSavedCard", []);
	var directives = {};
	var isaConfig = new IsaModuleConfig();

	directives.isaSavedCard = [
			"$compile",
			"$http",
			"$templateCache",
			function($compile, $http, $templateCache) {
				var directive = {};

				directive.restrict = 'EA';
				directive.scope = {
					paymentCardObject : "=",
					deleteCard : "&",
					addNewCard : "&"
				};
				directive.link = function(scope, element) {

					var template = $templateCache.get(isaConfig.getTemplates("isaSavedCard")['default']);
					template = angular.element(template);
					element.append(template);
					$compile(template)(scope);
					

				};
				directive.controller = ["$scope","$element",function($scope, $element) {
					
				}];

				return directive;
			} ];
	isa_saved_card.directive(directives);
})();;/**
 * Created by nipuna on 10/4/15.
 */

'use strict';

(function () {
    var isa_scroll_top = angular.module("scrollTop", []);
    var directives = {};

    directives.scrollTop = [function () {
        return {
            link: function (scope, element) {
                element.removeClass("drop-shadow-bottom");

                scope.$watch(function () {
                    return element.offset().top
                }, function (topOffset) {
                    if (topOffset < 110) {
                        element.removeClass("drop-shadow-bottom");
                    } else {
                        element.addClass("drop-shadow-bottom");
                    }
                });
            }
        };
    }];
    isa_scroll_top.directive(directives);
})();;/**
 * Seat Map Directive
 * baladewa
 */
'use strict';

(function () {
    var isa_seatMap = angular.module("seatMap", []);
    var isaconfig = new IsaModuleConfig();

    isa_seatMap.directive('seatMap', [
        '$window', '$timeout', '$compile', '$templateCache','$rootScope',
        function ($window, $timeout, $compile, $templateCache,$rootScope) {
            return {
                restrict: 'AE',
                scope: {
                    seatMapDataModel: "=",
                    seatPrefs: "=",
                    mapId: "@",
                    onSeatSelect: "&",
                    jsonMap: "=",
                    assignToIndex: "=",
                    assignPaxList: "=",
                    onAfterDraw: "&",
                    navigateOnselect: "@",
                    onBeforeSelect: "&",
                    selectionOnChange: "&",
                    previousSeats: "="
                },
                template: $templateCache.get(isaconfig.getTemplates("seatMap")['default']),
                link: function (scope, element, attrs) {
                    //scope.seatModelCopy = angular.copy(scope.seatMapDataModel);
                    var _jsonMap = scope.jsonMap, paxArray = angular.copy(scope.assignPaxList);
                    var autoNavigate = (scope.navigateOnselect === "true");
                    scope.cabinLetters = [];
                    scope.mapDrawn = false;

                    //Function to update the passenger list according to the model changes
                    function setPaxToSeats() {
                        //Clear the seatAssigned value of passenger list
                        _.map(scope.assignPaxList, function (passenger) {
                            passenger.seatAssigned = false;
                        });
                        if (scope.seatPrefs != undefined) {
                            angular.forEach(scope.seatPrefs.passengers, function (currentPassenger) {
                                angular.forEach(currentPassenger.selectedItems, function (item) {
                                    //Check if passenger is assigned to a seat and set that passenger as
                                    // seatAssigned triggering the tick in the Passenger List
                                    angular.forEach(scope.assignPaxList, function (passenger) {
                                        if (passenger.paxSequence === currentPassenger.passengerRph) {
                                            passenger.seatAssigned = true;
                                            passenger.previousSeat = item;
                                            passenger.previousSeat.seatNumber = item.id;
                                        }
                                    });

                                    //passenger.seatAssigned = true;
                                    var sNumber = item.id;
                                    var seat = getSeatByNumber(sNumber);
                                    seat.seatAvailability = "SEL";
                                });
                            });
                        }
                        
                        angular.forEach(scope.previousSeats, function(seatId){
                            var seat = getSeatByNumber(seatId);
                            seat.seatAvailability = _jsonMap.availability['INA'];
                        });


                        paxArray = angular.copy(scope.assignPaxList);
                    }

                    function getSeatByNumber (seatNumber) {
                        //go through cabins
                        if(scope.seatMapDataModel != null) {
                            for (var i = 0; i < scope.seatMapDataModel.cabinClass.length; i++) {
                                var cabin = scope.seatMapDataModel.cabinClass[i];
                                //go through airrow rowgroups
                                for (var j = 0; j < cabin.airRowGroupDTOs.length; j++) {
                                    var rowGroup = cabin.airRowGroupDTOs[j];
                                    //go through airrow airColumnGroups
                                    for (var k = 0; k < rowGroup.airColumnGroups.length; k++) {
                                        var colGroup = rowGroup.airColumnGroups[k];

                                        for (var l = 0; l < colGroup.airRows.length; l++) {
                                            var airRow = colGroup.airRows[l];

                                            for (var m = 0; m < airRow.airSeats.length; m++) {
                                                var seat = airRow.airSeats[m];

                                                if (seat.seatNumber === seatNumber) {
                                                    return seat;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    scope.isSeatMapDataAvailable = function(){
                        var isDataAvailable = (scope.seatMapDataModel != null) ? false : true;
                        return isDataAvailable;
                    }
                    scope.checkAvailability = function (seat, flg) {
                        return (seat.seatAvailability === _jsonMap.availability[flg]);
                    };

                    scope.selectSeat = function (e, obj) {
                        scope.selectionOnChange({event: e, element: obj});

                        var currentIndex = scope.assignToIndex || 0;
                        
                        var assignToPax = angular.copy(paxArray[currentIndex]);
                        var option = {
                            "selectedSeat": obj,
                            "selectedPax": assignToPax,
                            "onSuccess": onSuccess
                        };

                        scope.onBeforeSelect(option);

                        function onSuccess(){
                            assignSeat(e, obj);
                        }

/*                        scope.onBeforeSelect(option, function(){
                            assignSeat(e, obj);
                        });*/

/*                        if (scope.onBeforeSelect(option)) {
                            assignSeat(e, obj);
                        }*/
                    };

                    scope.setSeatValue = function(seat){
                        // Append <span isa-connector="{{airSeat.price}}"></span>
                        // element.find('#' + seat.itemId).parent().find('.tooltip').find('.seat-price').append($compile('<span class="currency"></span>&nbsp; <span>{{airSeat.price}}</span>'));

                    };

                    function assignSeat(e, airSeat) {
                        var currentIndex = scope.assignToIndex || 0;
                        var currentSelectedPassengerIndex = scope.assignToIndex ;

                        var message = "Select a pax to assign a seat", sendIndex = 0;

                        if(airSeat.seatAvailability === _jsonMap.availability['INA']){
                            return;
                        }

                        if (airSeat.seatAvailability === _jsonMap.availability['VAC'] || airSeat.seatAvailability === _jsonMap.availability['RES']) {
                            //get the pax
                            var assignToPax = angular.copy(paxArray[currentIndex]);

                            if (assignToPax !== undefined) {
                                //Reset seat with previous state
                                if (assignToPax.previousSeat !== null && !_.isUndefined(assignToPax.previousSeat)) {
                                    var seat = getSeatByNumber(assignToPax.previousSeat.seatNumber);

                                    if(seat.seatAvailability === _jsonMap.availability['RES']){
                                        seat.seatAvailability = _jsonMap.availability['RES'];
                                    }else if(seat.seatAvailability !== _jsonMap.availability['INA']){
                                        seat.seatAvailability = _jsonMap.availability['VAC'];
                                    }
                                }

                                //Change seat status
                                //assignToPax.previousSeat = angular.copy(airSeat);
                                paxArray[currentIndex] = assignToPax;
                                if(airSeat.seatAvailability !== _jsonMap.availability['RES']){
                                	airSeat.seatAvailability = _jsonMap.availability['SEL'];
                                }
                                message = "Success";

                                var temPax = angular.copy(scope.assignPaxList[currentIndex]);
                                temPax.seat = airSeat;
                                temPax.seatAssigned = true;
                                scope.assignPaxList[currentIndex] = temPax;
                            } else {
                                assignToPax = null;
                            }
                            //!($rootScope.deviceType.isMobileDevice) &&

                            if ( autoNavigate) {
                                //Reset the current index
                                sendIndex = currentIndex;
                                currentIndex++;
                                if (currentIndex > paxArray.length - 1) {
                                    currentIndex = 0;
                                }
                            }
                            if($rootScope.deviceType.isMobileDevice){
                                scope.assignToIndex = currentSelectedPassengerIndex === currentIndex ? currentIndex : currentSelectedPassengerIndex;
                            }else{
                                scope.assignToIndex = currentIndex;
                            }
                            
                            //set to the function
                            var option = {
                                "selectedSeat": airSeat,
                                "selectedPax": assignToPax,
                                "paxIndex": sendIndex,
                                "msg": message
                            };
                            scope.onSeatSelect(option);
                        } else {
                            //If the same seat as the previously selected, remove the selection
                            var assignToPax = angular.copy(paxArray[currentIndex]);

                            if (assignToPax !== undefined) {
                                if (airSeat.itemId === assignToPax.previousSeat.seatNumber) {
                                    //change the seat to VAC
                                    var seat = getSeatByNumber(assignToPax.previousSeat.seatNumber);
                                    seat.seatAvailability = _jsonMap.availability['VAC'];
                                    scope.seatPrefs.passengers[assignToPax.paxSequence - 1].selectedItems = [];
                                    //Remove selection

                                }
                            }
                        }
                    }

                    $timeout(function () {
                        setPaxToSeats();

                        angular.forEach(scope.previousSeats, function(seatId){
                            var seat = getSeatByNumber(seatId);
                            seat.seatAvailability = _jsonMap.availability['INA'];
                        });

                        scope.onAfterDraw();
                    }, 0);

                    scope.$watch('seatMapDataModel', function () {
                        setPaxToSeats();
                    });

                    scope.$watch('seatPrefs', function (newVal, oldVal) {
                        //If old and new values are equal, the seat is removed.
                        if (oldVal) {
                        	if (_.isEqual(oldVal.segments[0].flightSegmentRPH, newVal.segments[0].flightSegmentRPH)) {
	                            angular.forEach(oldVal.passengers, function (val, key) {
	                                if (!_.isEqual(oldVal.passengers[key].selectedItems[0], newVal.passengers[key].selectedItems[0])) {
	                                    if (_.isUndefined(newVal.passengers[key].selectedItems[0])) {
	                                        var emptySeat = getSeatByNumber(oldVal.passengers[key].selectedItems[0].id);
	                                        if(!!emptySeat) {
	                                            emptySeat.seatAvailability = _jsonMap.availability['VAC'];
	                                        }
	                                    }
	                                }
	                            })
                        	}
                        }
                        setPaxToSeats();
                    }, true);

                    scope.$watch('assignPaxList', function (newVal, oldVal){
                        angular.forEach(newVal, function(pax){
                            if(pax.seat){
                                if(pax.seat.seatNumber){
                                    var seat = getSeatByNumber(pax.seat.seatNumber);
                                    seat.passengerName = pax.displayName;
                                }
                            }
                        });
                    }, true);
                }
            };
        }]);

})();;/**
 * Temporary option list for the Hala services
 * TODO: Integrate into the option_list directive
 */
'use strict';

(function () {
    var isa_services_optionlist = angular.module("isaservicesoptionlist", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaservicesoptionlist = [
        '$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                availableOptions: '=',
                optionClick: '&',
                source: '=',
                template: '@',
                dropdownSource: '=',
                secondDropDownSource: '=',
                dropdownClick: '&',
                secondaryOptionClick: '&',
                elementButtonCaptions: '=',
                widgetOptions: '=',
                selectedItems: '=',
                passengerType: '=',
                passenger: '=',
                onInputChange : '&'
            };

            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {

                    scope.pax = 0;
                    scope.flight = 0;
                    scope.trip = 'Outbound';

                    scope.isCatListVisible = false;
                    scope.orderByOption = "";
                    scope.selectedRowIndex = -1;

                    var template = $templateCache.get(isaconfig.getTemplates("isaservicesoptionlist")[scope.template]);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);

                    scope.onCommentChange = function(type){
                        scope.onInputChange({value:element.find('.data #comment_'+type).val(),type:type})
                    }

                    scope.optionOnClick = function (id) {
                        scope.optionClick(id);
                        scope.selectedIndex = id.id;

                        if (scope.source[id.index].selected === true) {
                            scope.source[id.index].selected = false;
                        } else {
                            scope.source[id.index].selected = true;
                        }
                    };

                    scope.moreInfoOnClick = function ($event) {
                        $event.stopPropagation();
                    };

                    scope.onSecondaryOptingClick = function (option) {
                        scope.secondaryOptionClick({option: option});
                        scope.orderByOption = option;
                    };
                    scope.dropdownOptionClick = function (id) {
                        scope.isCatListVisible = (scope.isCatListVisible) ? false : true;
                        scope.selectedIndex = -1;
                        if (id !== undefined) {
                            scope.dropdownClick(id);
                            scope.selectedOptionIndex = id.index;
                            scope.selectedDropDown = id.section;
                        }
                    };
                    function resetToDefault(e, scope) {
                        scope.selectedRowIndex = -1;
                        angular.element(e.currentTarget).find('.modify').removeClass('fa-times');
                        angular.element(e.currentTarget).find('.modify').addClass('fa-angle-down');
                        if (scope.elementButtonCaptions !== undefined) {
                            angular.element(e.currentTarget).find('.meal-select-caption').text(scope.elementButtonCaptions[0]);
                        }
                    }

                    scope.onElementClick = function (event, index) {
                        element.find('.meal-select-caption').text(scope.elementButtonCaptions[0]);
                        if (scope.selectedRowIndex === -1) {
                            scope.selectedRowIndex = index;
                            angular.element(event.currentTarget).find('.modify').addClass('fa-times');
                            angular.element(event.currentTarget).find('.modify').removeClass('fa-angle-down');
                            angular.element(event.currentTarget).find('.meal-select-caption').text(scope.elementButtonCaptions[1]);

                        } else {
                            scope.selectedRowIndex = -1;
                            angular.element(event.currentTarget).find('.modify').removeClass('fa-times');
                            angular.element(event.currentTarget).find('.modify').addClass('fa-angle-down');
                            angular.element(event.currentTarget).find('.meal-select-caption').text(scope.elementButtonCaptions[0]);

                        }

                    };
                    scope.onElementClose = function (index) {
                        //scope.selectedRowIndex = -1;
                    };
                    angular.element('body').on('click', function (e) {
                        if (element.find(e.target).length <= 0) {
                            resetToDefault(e, scope);
                            scope.selectedIndex = -1;
                            scope.isCatListVisible = false;
                        }
                        scope.$apply(function () {
                        });
                    });

                    (function init() {
//                        console.log("Some items: " + JSON.stringify(scope.selectedItems));
                        scope.source.map(function (extra) {

                        });
                    })();
                };
            return directive;

        }];
    isa_services_optionlist.controller('isaOptionListController', [function ($scope) {

    }]);
    isa_services_optionlist.directive(directives);
})();

;/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_spinner = angular.module("isaspinner", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    var currentValue = 1;

    directives.isaspinner = [
        '$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};
            //load relavent tamplate according to the button type

            directive.restrict = 'AEC';
            directive.scope = {
                max: '=',
                min: '=',
                setCount: '&',
                value: '='
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    //var template = isaconfig.getTemplates("isaspinner");
                    var template = $templateCache.get(isaconfig.getTemplates("isaspinner")['default']);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);

                    /*$templateRequest(template['default'],[false]).then(function(html){
                     var template = angular.element(html);
                     element.append(template);
                     $compile(template)(scope);
                     });*/
                    //console.log(template)


                };
            return directive;

        }];
    isa_spinner.controller('spinnerController', ['$scope', '$element', function ($scope, $element) {
        $element.find('input').on('keypress', function (event) {
            if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
                event.preventDefault(); //stop character from entering input
            } else {
//                console.log(String.fromCharCode(event.which));
            }
        });
        $element.find('input').on('blur', function () {
            var input = Number($(this).val());
            if (input > $scope.max || input < $scope.min || $(this).val().trim().length <= 0) {
                $(this).val(currentValue);
            }

        });
        $element.find('input').on('focus', function () {
            currentValue = Number($(this).val());
        });

        $scope.onIncriment = function () {
            var count = Number($element.find('input').val());
            if (count < $scope.max) {
                count++;
            }
            $element.find('input').val(count);
            var data = {spinnerValue: count};
            $scope.setCount(data);

        };

        $scope.onDecriment = function () {
            var count = Number($element.find('input').val());
            if (count > $scope.min) {
                count--;
            }
            $element.find('input').val(count);
            var data = {spinnerValue: count};
            $scope.setCount(data);
        };

    }]);
    isa_spinner.directive(directives);
})();

;/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_spinnerList = angular.module("isaSpinnerList", ["isaspinner"]);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaSpinnerList = [
        '$compile', '$http', '$templateCache','$rootScope',
        function ($compile, $http, $templateCache,$rootScope) {
            var directive = {};

            directive.restrict = 'AEC';
            directive.scope = {
                max: '=',
                min: '=',
                setCount: '&',
                value: '=',
                meal:'=',
                isVisible: '=',
                bindTo: '=',
                defaultValue: '=',
                spinnerSource: '=',
                currentItemSelection: '=',
                isMobileDevice: '=',
                selectedCategoryCode: '=',
                indexCategory: '=',
                selectedSegment: '=',
                indexSegment: '=',
                onElementParentClick:'&',
                index:'=',
                getInvent : '&'
            };
            directive.require = '';
            directive.link = function (scope, element, attr) {

                if (!("defaultValue" in attr)) {
                    scope.defaultValue = 1;
                }

                var count = 0;
                var template = $templateCache.get(isaconfig.getTemplates("isaSpinnerList")['default']);
                 scope.mDevice = $rootScope.deviceType.isMobileDevice;
                var currentTarget
                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);

                scope.onIncriment = function (e, index, value) {
                    var elem = angular.element(e.currentTarget).parent().parent().find('input[type="text"]');
                    count = Number(elem.val());
                    count = parseInt(count);
                    count = scope.getInvent({oldCount:count, newCount: count + 1, key: index, value: value});
                    if (count <= scope.max) {
                    	this.currentItemSelection[index].count = count;
                    	this.currentItemSelection[index].oldCount = count;
                    	var data = {spinnerValue: count, index: index};
                    	if(scope.mDevice )
                    		data = {spinnerValue: count, index: index,fromMobileClick:true}
                    	scope.setCount(data);
                    }
                    //elem.val(count);
                };
                scope.onDecriment = function (e, index, value) {
                    var elem = angular.element(e.currentTarget).parent().parent().find('input[type="text"]');
                    count = Number(elem.val());
                    count = parseInt(count);
                    count = scope.getInvent({oldCount:count, newCount: count - 1, key: index, value: value});

                    if (count >= scope.min) {
                    	//elem.val(count);
                    	this.currentItemSelection[index].count = count;
                    	this.currentItemSelection[index].oldCount = count;
                    	var data = {spinnerValue: count, index: index};
                    	if(scope.mDevice )
                    		data = {spinnerValue: count, index: index,fromMobileClick:true}
                    	scope.setCount(data);
                    }
                };
                scope.onDoneClick = function (event,index ,isClose) {
                                    scope.onElementParentClick({event:event,index:index,isClose:isClose});
                                    if(event){
                                        event.preventDefault();
                                        event.stopPropagation();
                                    }
                                }
                angular.element('body').on('click', function (e) {
                    if (element.find(e.target).length <= 0) {
                        //scope.isVisible = false;
                    }
                });
                /*
                 count changed on enter key press
                 */
                scope.onKeyInput = function (e) {
                    if (e.keyCode == 13) {
                        var key = angular.element(e.currentTarget).data('meal-key')
                        currentTarget = e.currentTarget;
                        count = angular.element(e.currentTarget).val();
                        
                        var currentCount = (this.currentItemSelection[key].oldCount)?this.currentItemSelection[key].oldCount:0;
                        count = scope.getInvent({oldCount:currentCount, newCount: count, key: key, value: scope.meal});
                        if (count < scope.max && count > scope.min) {
                            var data = {spinnerValue: parseInt(count, 10), index: key};
                            scope.setCount(data);
                        } else if (count <= scope.min) {
                            var data = {spinnerValue: 0, index: key};
                            scope.setCount(data);
                        }
                        this.currentItemSelection[key].count = count;
                        this.currentItemSelection[key].oldCount = count;

                        scope.$watch(
                                function (scope) {
                                    return scope.setCount
                                },
                                function () {
                                    if (count > scope.max) {
                                        scope.setCount({spinnerValue: parseInt(scope.max, 10), index: key});
                                        angular.element(e.currentTarget).val(scope.max);
                                    }
                                }
                        );

                    }
                };

                scope.$watch('isVisible', function (visible, prev) {
                    if (visible && prev) {
                        var dropDown = element.find('.meals-qty-dropdown');
                        var keyElementList = dropDown.find('input[type="text"]');

                        angular.forEach(keyElementList, function (keyElement) {
                            var count = parseInt(angular.element(keyElement).val());
                            var key = angular.element(keyElement).data('meal-key');
                            var currentCount = this.currentItemSelection[key].count;

                            count = scope.getInvent({oldCount:currentCount, newCount: count, key: key, value: scope.meal});
                            this.currentItemSelection[key].count = count;

                            if (count <= scope.max && count > scope.min) {
                                var data = {spinnerValue: parseInt(count, 10), index: key};
                                scope.setCount(data);
                            }
                        });
                    }
                });

            };
            return directive;
        }];
    isa_spinnerList.directive(directives);
})();

;/**
 * Created by baladewa on 11/4/15.
 */
'use strict';

(function () {
    var isa_step_progress = angular.module("isaStepProgress", []);
    var directives = {};
    directives.isaStepProgress = [
        '$compile', '$timeout', '$window',
        function ($compile, $timeout, $window) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                currentFlag: "@",
                currentStep: "="
            };
            directive.link = function (scope, element, attr) {
                scope.selectedElement = angular.element("." + scope.currentFlag);
                scope.toLeft = 0;
                $timeout(function () {
                    if (angular.element("." + scope.currentFlag)[0] !== undefined) {
                        var tempObj = angular.element("." + scope.currentFlag);
                        var lf = parseInt(tempObj.offset().left);
                        lf += parseInt(tempObj[0].offsetWidth);
                        angular.element(element[0].children[0]).css({
                            'width': lf
                        });
                    } else {
                        angular.element(element[0].children[0]).css({
                            'width': '100%'
                        });
                    }
                }, 2000);

                scope.$watch('currentStep', function (newVal, oldVal) {
                    if (newVal.id) {
                        $timeout(function () {
                            if (angular.element("." + scope.currentFlag)[0] !== undefined) {
                                var tempObj = angular.element("." + scope.currentFlag);
                                scope.toLeft = parseInt(tempObj.offset().left);
                                scope.toLeft += parseInt(tempObj[0].offsetWidth);
                                angular.element(element[0].children[0]).animate({
                                    width: scope.toLeft
                                }, "slow");
                            } else {
                                angular.element(element[0].children[0]).animate({
                                    width: '100%'
                                }, "slow");
                            }
                        });
                    }

                });

                /**
                 * Window Resize
                 */
                angular.element($window).bind('resize', function () {
                    if (angular.element("." + scope.currentFlag)[0] !== undefined) {
                        var tempObj = angular.element("." + scope.currentFlag);
                        scope.toLeft = parseInt(tempObj.offset().left);
                        scope.toLeft += parseInt(tempObj[0].offsetWidth);
                        angular.element(element[0].children[0]).css({
                            width: scope.toLeft
                        });
                    }
                });
            };
            return directive;
        }];
    isa_step_progress.directive(directives);
})();;/**
 * Created by baladewa on 10/4/15.
 */

'use strict';

(function () {
    var isa_stick_me = angular.module("stickMe", []);
    var directives = {};
    directives.stickMe = ['$window', function ($window) {
        return {
            restrict: 'AE',
            scope: {
                stickyOffset: "@",
                stickTo: "@",
                stickyParent: "@"
            },
            link: function (scope, element) {
                element.addClass("sticky-bg");
                var stickTo = scope.stickTo || "body";
                /* var elementWidth = element[0].offsetWidth;
                 var spacing = 0;
                 var args;
                 var parentContainer;*/
                var stickyOptions = {
                    responsiveWidth: true
                };

                stickyOptions.topSpacing = parseInt(scope.stickyOffset, 10);
                if (!!scope.stickyParent) {
                    stickyOptions.getWidthFrom = scope.stickyParent;
                }

                var ele = angular.element(element);
                ele.sticky(stickyOptions);
                scope.stickyEnbeld = false;
                if (parseInt(scope.stickyOffset, 10) === 0) {
                    scope.stickyEnbeld = true;
                }


                scope.$watch('stickyEnbeld', function (newVal, oldVal) {
                    if (newVal === true) {
                        ele.sticky(stickyOptions);
                    } else {
                        ele.unstick();
                    }
                });

/*              //Added to prevent element going out of view when called using $anchorScroll
                scope.$watch(function () {
                    return element.offset().top > 100;
                }, function (topOffset) {
                    if (this.pageYOffset >= angular.element(stickTo)[0].offsetTop + angular.element(stickTo)[0].offsetHeight - (stickyOptions.topSpacing * 2)) {
                        scope.stickyEnbeld = false;
                        //console.log('Scrolled below header.' + this.pageYOffset);
                    } else {
                        scope.stickyEnbeld = true;
                        //console.log('Header is in view.' +this.pageYOffset);
                    }
                });*/

                angular.element($window).bind("scroll", function () {

                    if (this.pageYOffset >= angular.element(stickTo)[0].offsetTop + angular.element(stickTo)[0].offsetHeight - (stickyOptions.topSpacing * 2)) {
                        scope.stickyEnbeld = false;
                        //console.log('Scrolled below header.' + this.pageYOffset);
                    } else {
                        scope.stickyEnbeld = true;
                        //console.log('Header is in view.' +this.pageYOffset);
                    }

                    scope.$apply();
                });
                //

            }
        };
    }];
    isa_stick_me.directive(directives);
})();;'use strict';

(function() {
    var typeahead_focus = angular.module("typeaheadFocus", []);
    var directives = {};

    directives.typeaheadFocus = [ function(){
        var directive = {};
        directive.restrict = 'EA';
        directive.require = 'ngModel';

        directive.link = function (scope, element, attr, ngModel) {
            // Array of keyCode values for arrow keys
            //Fixme do not use const in js it si not working in IE
            //const  ARROW_KEYS = [37, 38, 39, 40];
            var  ARROW_KEYS = [37, 38, 39, 40];

            function manipulateViewValue(e) {
                /* we have to check to see if the arrow keys were in the input because if they were trying to select
                 * a menu option in the typeahead, this may cause unexpected behavior if we were to execute the rest
                 * of this function
                 */
                if (ARROW_KEYS.indexOf(e.keyCode) >= 0)
                    return;

                var viewValue = ngModel.$viewValue;

                //restore to null value so that the typeahead can detect a change
                if (ngModel.$viewValue == ' ') {
                    ngModel.$setViewValue(null);
                }

                if (_.isUndefined(viewValue) || _.isEmpty(viewValue)) {
                    //force trigger the popup
                    ngModel.$setViewValue(' ');
                }

                //set the actual value in case there was already a value in the input
                ngModel.$setViewValue(viewValue || ' ');
            }

            /* trigger the popup on 'click' because 'focus'
             * is also triggered after the item selection.
             * also trigger when input is deleted via keyboard
             */
            element.bind('click keyup', manipulateViewValue);

            //compare function that treats the empty space as a match
            scope.$emptyOrMatch = function (actual, expected) {
                if (expected == ' ') {
                    return true;
                }
                return actual ? actual.toString().toLowerCase().indexOf(expected.toLowerCase()) > -1 : false;
            };
        };
        return directive;
    }];

    typeahead_focus.directive(directives);
})();;'use strict';

(function () {
    var isa_validator_type = angular.module("isaValidatorType", []);
    var directives = {};
    directives.isaValidatorType = function () {
        return {
            priority: 1,
            controller: ['$attrs', function ($attrs) {

                this.getType = function () {
                    return $attrs.isaValidatorType;
                };

                this.getRules = function () {
                    return $attrs.isaValidatorRules;
                };

                this.getName = function () {
                    return $attrs.name;
                };

            }]
        };
    };
    isa_validator_type.directive(directives);
})();;'use strict';

(function () {
    var isa_validator = angular.module("isaValidator", []);
    var directives = {};

    directives.isaValidator = ['$rootScope','$compile', '$translate', function ($rootScope, $compile, $translate) {
        return {
            restrict: 'A',
            replace: false,
            terminal: true,
            priority: 1000, //this setting is important to make sure it executes before other directives
            require: '?^isaValidatorType', //inject validator-type form as the forth parameter to the link function

            link: function (scope, element, attrs, isaValidatorType) {
                var type;
                var rules;
                var formName;
                var validationEnabled = true;
                var isMobileDevice = $rootScope.deviceType.isMobileDevice;
                var inputElement = element.find("input");
                if(scope[element.attr("isa-validation-disabled")] || element.attr("isa-validation-disabled") === 'true'){
                    validationEnabled = false;
                }


                var inputName;
                if (!_.isUndefined(inputElement[0])) {
                    inputName = inputElement[0].getAttribute('name');
                } else {
                    for (var i = 0; i < element[0].children.length; i++) {
                        if (element[0].children[i].hasAttribute('ng-model')) {
                            inputElement = angular.element(element[0].children[i]);
                            inputName = element[0].children[i].getAttribute('name');
                        }
                    }
                }

                if (isaValidatorType) {
                    rules = JSON.parse(isaValidatorType.getRules());
                    type = isaValidatorType.getType();
                    formName = isaValidatorType.getName();

                    if(!_.isUndefined(rules[type])){
                        if (rules[type][inputName]) {
                            rules = rules[type][inputName];
                        }
                    }

                } else {
                    console.warn("Provide isa-validator-type in order to activate validation");
                }

                inputElement.bind('blur', function(evt){
                    $(evt.currentTarget).removeClass('on-focus');
                    angular.element('.'+inputName).css("display","");
                })

                inputElement.bind('focus', function(evt){
                    $(evt.currentTarget).addClass('on-focus');
                    angular.element('.'+inputName).css("display","none");
                  
                })

                if(validationEnabled){
                    var inputPath = formName + '.' + inputName;
                    var ngMessage = '<div class= "'+inputName+' " ng-messages="' + inputPath + '.$error" ng-show="' + formName + '.$submitted || ' + inputPath + '.$touched">';

                    angular.forEach(rules, function (rule, key) {
                        inputElement.attr(rule.name, rule.value);
                        if(inputElement.parent().attr('isa-validatin-on') == 'focus') {
                            inputElement.bind('blur', function(evt){
                                // console.log(inputElement)
                                // angular.element(evt.currentTarget).removeClass('ng-invalid');
                                // angular.element(evt.currentTarget).removeClass('ng-touched');
                                // var validationMessage = $translate.instant(rule.message);
                                // var focusMessage = '<p class="valdr-message on-focus" ng-message=' + key + '>' + validationMessage + '</p>';

                                // inputElement.next().append(focusMessage);
                                // inputElement.next().attr({'ng-show': 'true', 'class': 'ng-active'});
                            })

                        }
                        ngMessage = ngMessage + '<p class="valdr-message" ng-message=' + key + '> <span translate="' + rule.message + '">' + rule.message + '</span></p>';
                    });

                    ngMessage = ngMessage + '</div>';
                    element.append($compile(ngMessage)(scope));

                }

                element.removeAttr("isa-validator"); //remove the attribute to avoid indefinite loop
                element.removeAttr("data-isa-validator"); //also remove the same attribute with data- prefix in case users specify data-common-things in the html

                $compile(element)(scope);
            }
        };
    }];

    directives.ageValidation = ['$timeout', function ($timeout) {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                ctrl.$validators.ageValidation = function (modelValue, viewValue) {

                    //Get the age of the given passenger
                    var givenDate = moment(modelValue);
                    var todayDate = moment();
                    var diffYears = todayDate.diff(givenDate, 'years');

                    var ageLimit;

                    try{
                        //Parse and santize the age limits provided
                        ageLimit = JSON.parse(attrs['ageValidation']);
                    }catch(ex){
                        //Set default values to prevent errors
                        console.warn("min, max age limits not set for the validator set to default");
                        ageLimit = {min: 0, max: 100};
                    }

                    if(diffYears >= ageLimit.max || diffYears < ageLimit.min){
                        return false;
                    }else{
                        return true;
                    }
                };
            }
        };
    }];

    directives.itemInList = ['$timeout', function ($timeout) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$validators.itemInList = function (modelValue, viewValue) {
                    var itemList;

                    try {
                        itemList = JSON.parse(attrs['itemInList']);
                    } catch (ex) {
                        itemList = [];
                    }

                    if (_.contains(itemList, viewValue)) {
                        return true;
                    } else {
                        //Checks for space(" ") to compensate for uib typeahead directive adding space on click
                        return !!(_.isUndefined(modelValue) || _.isEmpty(modelValue) || modelValue === " ");
                    }
                };
            }
        };
    }];

    /** Makes fields required based on another fields filled state
     *
    * @param dependentFilled if this field is filled, the other field is set to required
    * */
    directives.dependentFilled = function() {
        return {
            require: 'ngModel',
            link: function(scope, $element, $attrs, ctrl) {
                ctrl.$validators.dependentFilled = function(modelValue) {

                    var dependentFilled = _.isEmpty($attrs.dependentFilled);

                    if(dependentFilled && !_.isEmpty(modelValue)){
                        return false;
                    }else{
                        return true;
                    }
                };

                $attrs.$observe('dependentFilled', function (){
                    ctrl.$validate();
                });
            }
        }
    };

    /** Check if two fields are equal
     *
     * @param compareField field which is checked against the model
     * */
    directives.compareField = function() {
        return {
            require: 'ngModel',
            link: function(scope, $element, $attrs, ctrl) {
                ctrl.$validators.compareField = function(modelValue) {
                    return $attrs.compareField === modelValue;
                };

                $attrs.$observe('compareField', function (){
                    ctrl.$validate();
                });
            }
        }
    };


    isa_validator.directive(directives);
})();;/**
 * Created by indika on 10/16/15.
 */
'use strict';

/*exported IsaControllers*/
/*global IsaControllers: true*/
var IsaControllers = function() {
    var controllers = {};
    controllers.btnController = ['$scope', function ($scope) {
        $scope.caption = "My -----Element";
    }];
    return controllers;
};





;/**
 * Created by indika on 10/29/15.
 */
'use strict';

(function () {
    var isaconfig = new IsaModuleConfig();
    var isaCtrl = new IsaControllers();

    

    var isa_ui = angular.module('isaApp', isaconfig.getModuleNames()).run(['$rootScope', function ($rootScope) {
        var deviceType = {};

        deviceType.isMobileDevice = window.matchMedia("only screen and (max-width: 767px)").matches;

        deviceType.isSmallDevice = window.matchMedia("only screen and (min-width: 768px) and (max-width: 991px)").matches;

        deviceType.isLargeDevice = window.matchMedia("only screen and (min-width: 992px)").matches;

        $rootScope.deviceType  = deviceType;

    }]);

    isa_ui.controller(isaCtrl);
})();
