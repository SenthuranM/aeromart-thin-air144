/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_optionlist = angular.module("isaoptionlist", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaoptionlist = [
        'applicationService','$compile', '$http', '$templateCache', '$rootScope', 'currencyService','ancillaryService','stepProgressService',
        function (applicationService, $compile, $http, $templateCache, $rootScope, currencyService,ancillaryService,stepProgressService) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                availableOptions: '=',
                optionClick: '&',
                source: '=',
                template: '@',
                dropdownSource: '=',
                secondDropDownSource: '=',
                dropdownClick: '&',
                secondaryOptionClick: '&',
                elementButtonCaptions: '=',
                widgetOptions: '=',
                widgetData: '&',
                widgetOptionClick: '&',
                reset: '&',
                selectedList: '=',
                panelType: '@',
                currentItemSelection: '=',
                addedItems: '=',
                paginationData: '=',
                currentPage: '&',
                onPagination: '&',
                onEnd: '&',
                textFiltering: '&',
                resetValues: '=',
                currentSegment: '=',
                insuranceAmount:'=',
                flight:'=',
                selectedAncillaries:'=',
                ondPrefernces:'=',
                duplicateIndex:'=',
                isNextSectorClick:'=',
                baggageForSectorFirst:'=',
                baggageForSectorSecond:'=',
                getInv:'&'
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element) {
                        scope.mealData = [];
                        scope.accordionArray = [];
                    scope.isCatListVisible = false;
                    scope.orderByOption = "";
                    scope.selectedMobileRow = -1;
                    scope.selectedRowIndex = -1;
                        scope.selectedCategoryCode = '';
                        scope.showSpinner = 0;
                    scope.currencyType = currencyService.getSelectedCurrency();
                        scope.selectedPage = (scope.paginationData != undefined) ? scope.paginationData.defaultselection : 0;
                        scope.resetData = scope.resetValues || {};
                        scope.orderbyText = "lbl_extra_mealPrice";
                        scope.onEditClickParam = false;
                        //scope.mealnumber = scope.duplicateIndex.mealDuplicateIndex;
                        scope.getNumber = function(num) {
                                return new Array(num);   
                        }
                        var template = $templateCache.get(isaconfig.getTemplates("isaoptionlist")[scope.template]);
                        template = angular.element(template);
                        element.append(template);
                        $compile(template)(scope);

                        scope.getFlightDirection = function (toCode, fromCode, isModify) {
                            return applicationService.getFlightDirection(toCode, fromCode, isModify)
                        }
            

                        scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                        
                        scope.anciOptions = ancillaryService.getDefaultAncillary();

                        scope.resetData.SearchText = function () {
                            scope.searchText = null;
                        };

                        scope.resetData.Pagination = function () {
                            scope.selectedPage = 0;
                            scope.resetStyle();
                        };

                        scope.onFiltering = function (data) {
                            scope.textFiltering(data);
                        };

                        scope.onPageSelection = function (index) {
                            scope.selectedPage = index.index;
                            scope.onPagination(index)
                        };

                        scope.toogleMealSection = function (id, $event) {
                            var $this;
                            var $fa;
                            if($event.target.tagName === "I"){
                                $this = $($($event.target).parent().parent()).find('.meal-block');    
                                $fa = $($event.target);
                            }else{
                                $this = $($($event.target).parent()).find('.meal-block');    
                                $fa = $($event.target).find('.fa-meal-block');
                            }
                            

                            $(".meal-slider .meal-block").not($this).hide();
                            $this.slideToggle("100");

                            $('.fa-meal-block').not($fa).removeClass('fa-minus');
                            $('.fa-meal-block').not($fa).addClass('fa-plus');
                            
                            if ($fa.hasClass('fa-plus')) {
                                $fa.removeClass('fa-plus')
                                $fa.addClass('fa-minus')
                            } else {
                                $fa.removeClass('fa-minus')
                                $fa.addClass('fa-plus')
                            }

                            scope.selectedIndex = id.id;
                            scope.optionClick(id);

                            scope.showSpinner = id.id;

                           // scope.selectedCategoryCode = id.catCode;
                        }

                        scope.onNextSector = function (index) {
                            console.log('index' + index)
                            // scope.selectOption(index);
                        }

                        
                        scope.returnFlightBySegments = function(flight){
                        var flightArray=new Array();
                        var flightSegment=new Array();
                            if(flight != undefined){
                                if(flight.length > 2){
                                    flight.forEach(function(element) {
                                        flightArray.push(element)
                                        if(flightArray.length == 2){
                                            flightSegment.push(flightArray);
                                            flightArray= new Array();
                                        }
                                    });
                                }else{
                                    flightSegment.push(flight);
                                }
                                return flightSegment;
                            }
                        }

                        scope.optionOnClick = function (id) {
                            //console.log('id'+id.id)
                            //debugger;
                            var selected = {};
                            //scope.orderbyText = "lbl_extra_mealPrice";

                            scope.onPageSelection({index: 0});
                            if (typeof Number(id) === 'number') {
                                selected = scope.optionClick(id);
                                scope.mealData = selected;
                            } else {
                                scope.optionClick(id);
                            }
                            scope.selectedIndex = id.id;
                            if (id.hasOwnProperty('event')) {
                                
                                scope.selectedRowIndex = -1;

                                if ($(id.event.currentTarget).find('i span').hasClass('icon-close')) {
                                    scope.onEditClickParam = false;
                                    scope.selectedIndex = -1;
                                } else if ($(id.event.currentTarget).find('i span').hasClass('icon-plus')) {
                                    scope.selectedIndex = id.id;
                                    scope.onEditClickParam = true;
                                }
                                if ($(id.event.currentTarget).hasClass('cat-button')) {
                                    $(id.event.currentTarget.parentNode.parentNode).find('span').removeClass('gray-btn');
                                    $(id.event.currentTarget.parentNode.parentNode).find('span').removeClass('check-btn');

                                    $(id.event.currentTarget).addClass('gray-btn');
                                    $(id.event.currentTarget).addClass('check-btn');
                                }
                                
                            }
                            if (selected !== undefined) {
                                scope.selectedOptionIndex = selected.optionIndex;
                                scope.selectedDropDown = selected.dropDownIndex;
                            }
                            scope.selectedCat = selected;
                            if ($rootScope.deviceType.isMobileDevice){
                                scope.setExtraType(id.id);
                                if(id.event){
                                //id.event.preventDefault();
                               // id.event.stopPropagation();
                            }
                                return scope.mealData;
                                
                            }
                                
                        };
                        scope.setExtraType = function (id) {
                            var option = '';
                            switch (id) {
                                case 'BAGGAGE' :
                                    option = 'baggage';
                                    break;
                                case 'SEAT' :
                                    option = 'seats';
                                    break;
                                case 'MEAL' :
                                    option = 'meals';
                                    break;
                                case 'AUTOMATIC_CHECKIN' :
                                    option = 'automatic_checkin';    
                                    break;
                                case 'INSURANCE' :
                                    option = 'insurance';
                                    break;
                                case 'FLIGHT SERVICES' :
                                    option = 'services';
                                    break;
                                case 'SSR_AIRPORT' :
                                    option = 'airport_services';
                                    break;
                                case 'FLEXIBILITY' :
                                    option = 'flexibility';
                                    break;
                                case 'AIRPORT TRANSFER' :
                                    option = 'airport_transfer';
                                    break;
                            }
                            if(option!== ''){
                                stepProgressService.setCurrentStep('extras');
                                $rootScope.extraType = option;
                            }
                        }

                        scope.resetStyle = function () {
                            element.find('span').removeClass('gray-btn');
                            element.find('span').removeClass('check-btn');

                            var categoryElement = element.find('.selected-cat');
                            var selectAllElement = categoryElement.find('span').first();

                            selectAllElement.addClass('gray-btn');
                            selectAllElement.addClass('check-btn');
                        };

                        scope.onSecondaryOptingClick = function (option) {
                            scope.secondaryOptionClick({option: option});
                            scope.orderByOption = option;
                            scope.onPageSelection({index: 0});
                            switch(option) {
	                            case 'mealCharge':
	                            	scope.orderbyText = 'lbl_extra_mealPrice';
	                                break;
	                            case 'mealName': 
	                            	scope.orderbyText = 'lbl_extra_mealName';
	                                break;
	                            case 'popularity':
	                            	scope.orderbyText = 'lbl_extra_mealPopularity';
	                                break;
	                            default:
	                            	scope.orderbyText = 'lbl_extra_mealPrice';
                            }
                        };

                        scope.dropdownOptionClick = function (id) {
                            scope.isCatListVisible = (scope.isCatListVisible) ? false : true;
                            scope.selectedIndex = -1;
                            if (id !== undefined) {
                                scope.dropdownClick(id);
                                scope.selectedOptionIndex = id.index;
                                scope.selectedDropDown = id.section;
                                scope.selectedIndex = id.id;
                            }
                            if(id.event && $rootScope.deviceType.isMobileDevice){
                                id.event.preventDefault();
                                id.event.stopPropagation();
                            }
                          //  angular.element(".dropdown-content").height($(document).height());
                        };
                        
                      
                        scope.onMobileElementClick = function (event, index, isClose){
                            scope.widgetOptionClick({target: event, index: index, selectedRow: scope.selectedRowIndex, categoryCode: scope.selectedCategoryCode,isClose:isClose});
                            if ( scope.selectedMobileRow == -1) {
                                scope.selectedMobileRow  = index;
                            } else {
                                scope.selectedMobileRow  = -1;

                            }
                        }

                        scope.onElementClick = function (event, index,isClose) {
                            scope.widgetOptionClick({target: event, index: index, selectedRow: scope.selectedRowIndex, categoryCode: scope.selectedCategoryCode,isClose:isClose});
                            if (scope.selectedRowIndex === -1) {
                                scope.selectedRowIndex = index;
                            } else {
                                scope.selectedRowIndex = -1;
                            }
//                            if(event){
//                                event.preventDefault();
//                                event.stopPropagation();
//                            }
                        };

                        scope.onDoneClick = function (event, index) {
                            if (scope.selectedRowIndex === -1) {
                                scope.selectedRowIndex = index;
                            } else {
                                scope.selectedRowIndex = -1;
                            }
                        };

                        scope.onElementClose = function (index) {
                            scope.selectedRowIndex = -1;
                        };

                        scope.setWidgetData = function (data, index,fromMobileClick, val) {
                            scope.widgetData({count: data, index: index,fromMobileClick:fromMobileClick, value: val});
                        };

                        scope.categoryName = function (name) {
                            return name.split(' ').join('-')
                        };

                        angular.element('body').on('click', function (e) {
                            if (element.find(e.target).length <= 0) {
                                scope.selectedRowIndex = -1;
                                scope.reset({evt: e});
                                scope.selectedIndex = -1;
                                scope.isCatListVisible = false;
                            }
                            scope.$apply(function () {
                            });
                        });

                        scope.doneMealOptionClick = function () {
                            scope.isCatListVisible = false;
                        };
                        
                        scope.getInventory = function(oldCount, newCount, key, value){
                        	return scope.getInv({oldCount: oldCount,newCount:newCount, key:key, value:value});
                        }

                    };

            return directive;

        }];
    isa_optionlist.controller('optionListController', ['$scope', function ($scope) {
        }]);
    isa_optionlist.directive(directives);
})();

