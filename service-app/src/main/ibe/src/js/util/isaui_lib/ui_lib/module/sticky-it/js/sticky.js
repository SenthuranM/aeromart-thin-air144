/**
 * Created by baladewa on 10/4/15.
 */

'use strict';

(function () {
    var isa_stick_me = angular.module("stickMe", []);
    var directives = {};
    directives.stickMe = ['$window', function ($window) {
        return {
            restrict: 'AE',
            scope: {
                stickyOffset: "@",
                stickTo: "@",
                stickyParent: "@"
            },
            link: function (scope, element) {
                element.addClass("sticky-bg");
                var stickTo = scope.stickTo || "body";
                /* var elementWidth = element[0].offsetWidth;
                 var spacing = 0;
                 var args;
                 var parentContainer;*/
                var stickyOptions = {
                    responsiveWidth: true
                };

                stickyOptions.topSpacing = parseInt(scope.stickyOffset, 10);
                if (!!scope.stickyParent) {
                    stickyOptions.getWidthFrom = scope.stickyParent;
                }

                var ele = angular.element(element);
                ele.sticky(stickyOptions);
                scope.stickyEnbeld = false;
                if (parseInt(scope.stickyOffset, 10) === 0) {
                    scope.stickyEnbeld = true;
                }


                scope.$watch('stickyEnbeld', function (newVal, oldVal) {
                    if (newVal === true) {
                        ele.sticky(stickyOptions);
                    } else {
                        ele.unstick();
                    }
                });

/*              //Added to prevent element going out of view when called using $anchorScroll
                scope.$watch(function () {
                    return element.offset().top > 100;
                }, function (topOffset) {
                    if (this.pageYOffset >= angular.element(stickTo)[0].offsetTop + angular.element(stickTo)[0].offsetHeight - (stickyOptions.topSpacing * 2)) {
                        scope.stickyEnbeld = false;
                        //console.log('Scrolled below header.' + this.pageYOffset);
                    } else {
                        scope.stickyEnbeld = true;
                        //console.log('Header is in view.' +this.pageYOffset);
                    }
                });*/

                angular.element($window).bind("scroll", function () {

                    if (this.pageYOffset >= angular.element(stickTo)[0].offsetTop + angular.element(stickTo)[0].offsetHeight - (stickyOptions.topSpacing * 2)) {
                        scope.stickyEnbeld = false;
                        //console.log('Scrolled below header.' + this.pageYOffset);
                    } else {
                        scope.stickyEnbeld = true;
                        //console.log('Header is in view.' +this.pageYOffset);
                    }

                    scope.$apply();
                });
                //

            }
        };
    }];
    isa_stick_me.directive(directives);
})();