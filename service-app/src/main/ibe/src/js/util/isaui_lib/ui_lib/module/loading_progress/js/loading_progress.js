/**
 * Created by baladewa on 11/10/15.
 */
'use strict';

(function () {
    var isa_loading_progress = angular.module("isaLoadingProgress", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaLoadingProgress = ['$timeout', '$templateCache', '$translate', function ($timeout, $templateCache, $translate) {
        var directive = {};
        directive.template = $templateCache.get(isaconfig.getTemplates("isaLoadingProgress")['default']);
        directive.replace = true;
        directive.restrict = 'AE';
        directive.scope = {
            loadingStatus: "=",
            loaderText: "@",
            timeOut: '@',
        };
        directive.link = function postLink(scope, element) {
            var timeout = scope.timeOut || "0";
            if (scope.loaderText === undefined) {
                $translate.onReady(function(){
                    var loadingMessage = $translate.instant('lbl_common_loadingMain');
                    scope.loaderText = loadingMessage || "Please wait, loading...";
                });
            }
            scope.$watch("loadingStatus", function (oldVal) {
                if (oldVal) {
                    element.removeClass('ng-hide');
                    $("html").css("overflow", "hidden");
                    scope.loadingStatus = oldVal;
                    var timeInMins = parseInt(timeout, 10), timeInMils = 0;
                    if (timeInMins !== 0) {
                        timeInMils = timeInMins * 60 * 1000;
                        scope.loadingStatus = oldVal;
                        /*$timeout(function () {

                        }, timeInMils);*/
                        //scope.loadingStatus = false;
                    }
                } else {
                    element.addClass('ng-hide');
                    $("html").css("overflow", "auto");
                }

            });
        };
        return directive;
    }];
    isa_loading_progress.directive(directives);
})();