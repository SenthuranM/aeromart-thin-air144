/**
 * Created by baladewa on 11/4/15.
 */
'use strict';

(function () {
    var isa_fare_timeline = angular.module("fareFloater", []);
    var directives = {};
    directives.fareFloater = [
        '$compile', '$window', '$animateCss', '$timeout', '$rootScope','languageService','$translate','dateTransformationService','PersianDateFormatService', 'applicationService',
        function ($compile, $window, $animateCss, $timeout, $rootScope,languageService,$translate,dateTransformationService,PersianDateFormatService,applicationService) {
        return {
            restrict: 'A',
            scope: {
                calData: "=datasource",
                onAfterSlide: "&",
                onBeforeSlide: "&",
                onSelectDate: "&",
                currency: "@",
                cacheData: "=",
                autoSelectDate: "=",
                timelineSpeed: "=",
                animationEasing: "@",
                displayAnimation: "@",
                isRtl: "=",
                monthText: "=",
                daysText: "=",
                onBeforeSelectDate: "&"
            },
            template: $rootScope.deviceType.isMobileDevice? '<ul class="clearfix" ng-style="sliderWidth" ng-class="{rtl:isRtl}"></ul>' +
            '<a href="javascript:;" class="slide-nav slide-prev" ng-click="navigate(\'PREV\')"><i class="fa fa-angle-left"></i></a>' +
            '<a href="javascript:;" class="slide-nav slide-next" ng-click="navigate(\'NEXT\')"><i class="fa fa-angle-right"></i></a>' : '<ul class="clearfix" ng-style="sliderWidth" ng-class="{rtl:isRtl}"></ul>' +
            '<a href="javascript:;" class="slide-nav slide-prev" ng-click="navigate(\'PREV\')"><i class="glyphicon glyphicon-chevron-left"></i></a>' +
            '<a href="javascript:;" class="slide-nav slide-next" ng-click="navigate(\'NEXT\')"><i class="glyphicon glyphicon-chevron-right"></i></a>',
            link: function (scope, element, attrs) {
                //scope vars
                scope.slideCount = 7; //Slide Count
                scope.slideWidth = 0; // Slide Width
                scope.sliderWidth = {}; // Full Slider width
                scope.sliderData = []; // Slider data

                //local vars
                var slideWidth, timeLineData = [], nextCache = [], prevCache = [], navigationDone = true,
                    //DAYARRAY = (scope.daysText === undefined) ? ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] : scope.daysText,
                    //MONTHARRAY = (scope.monthText === undefined) ? ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] : scope.monthText,
                    speed = (scope.timelineSpeed === undefined) ? 0.3 : scope.timelineSpeed,
                    easing = (scope.animationEasing === undefined) ? "" : scope.animationEasing,
                    displayAnim = (scope.displayAnimation === undefined) ? "" : scope.displayAnimation,
                    leftOrRight = scope.isRtl ? "right" : "left";


                    /**
                     * Navigates Next Previous event
                     * @param direction
                     */
                    scope.navigate = function (direction) {
                        if (leftOrRight === "right") {
                            if (direction === "PREV") {
                                direction = "NEXT";
                            } else {
                                direction = "PREV";
                            }
                        }
                        if (navigationDone) {//to avoid multiple click
                            navigationDone = false;
                            var beforeSlideOption = {
                                "direction": direction,
                                date: getNextPrevDateBeforeNavigate(direction)
                            };
                            if (scope.onBeforeSlide(beforeSlideOption)) {//validation check onBeforeSlide

                                //Create New Element and compile with scope
                                var html = '<li ng-click="selectDate($event)" >' +
                                        '<div class="day-block" >' +
                                        '<div class=""><img src="js/util/isaui_lib/dist/images/bx_loader.gif" alt="loading" /></div>' +
                                        '</div></li>';
                                var newLi = angular.element(html);
                                var slider = angular.element(element[0].children[0].firstChild);
                                $compile(newLi)(scope);
                                //callback option for onAfterSlide
                                var callbackOption = {
                                    "direction": direction,
                                    "setData": function (dayObj) {
                                        setDataArrayToCalendar(direction, dayObj, false);
                                    }
                                };
                                // get cache data if dataCache==true
                                var cacheDate = getCacheData(direction);

                                if (direction === 'PREV') {// Next Previous Check
                                    //Remove border in before the last slide
                                    $animateCss(angular.element(slider[0].children[slider[0].children.length - 2].children[0]), {
                                        easing: easing,
                                        from: {borderWidth: '1px'},
                                        to: {borderWidth: '0px'},
                                        duration: speed // one second
                                    }).start();
                                    //Animate removing slide
                                    $animateCss(angular.element(slider[0].children[slider[0].children.length - 1]), {
                                        addClass: "dying",
                                        easing: easing,
                                        from: {width: slideWidth + 'px'},
                                        to: {width: '0px'},
                                        duration: speed // one second
                                    }).start()
                                            .then(function () {
                                                //remove element
                                                angular.element(slider[0].children[slider[0].children.length - 1]).remove();
                                                //set auto select date on next/previous if autoSelectDate true
                                                if (scope.autoSelectDate) {
                                                    autoSelectOnNavigation(direction);
                                                }

                                                if (cacheDate !== null) { //data cache check bore sending the call to main controller
                                                    //setting up date to the array
                                                    setDataArrayToCalendar(direction, cacheDate, true);
                                                } else {
                                                    //Call back
                                                    scope.onAfterSlide(callbackOption);
                                                }
                                            });
                                    slider.prepend(newLi);
                                    //Newly Created element Animation
                                    $animateCss(newLi, {
                                        addClass: "born",
                                        easing: easing,
                                        from: {width: '0px'},
                                        to: {width: slideWidth + 'px'},
                                        duration: speed // one second
                                    }).start();

                                } else {
                                    $animateCss(angular.element(slider[0].children[1]), {
                                        addClass: "dying",
                                        easing: easing,
                                        from: {width: slideWidth + 'px'},
                                        to: {width: '0px'},
                                        duration: 0.4 // one second
                                    }).start()
                                            .then(function () {
                                                //remove element
                                                angular.element(slider[0].children[1]).remove();

                                                //set auto select date on next/previous if autoSelectDate true
                                                if (scope.autoSelectDate) {
                                                    autoSelectOnNavigation(direction);
                                                }

                                                if (cacheDate !== null) { //data cache check bore sending the call to main controller
                                                    //setting up date to the array
                                                    setDataArrayToCalendar(direction, cacheDate, true);
                                                } else {
                                                    //Call back
                                                    scope.onAfterSlide(callbackOption);
                                                }
                                            });
                                    $animateCss(angular.element(slider[0].children[slider[0].children.length - 1].children[0]), {
                                        easing: easing,
                                        from: {borderWidth: '0px'},
                                        to: {borderWidth: '1px'},
                                        duration: speed // one second
                                    }).start();
                                    slider.append(newLi);
                                    $animateCss(newLi, {
                                        addClass: "born",
                                        easing: easing,
                                        from: {width: '0px'},
                                        to: {width: slideWidth + 'px'},
                                        duration: speed // one second
                                    }).start();
                                }// Next Previous Check
                            }//validation check onBeforeSlide
                            else {
                                navigationDone = true;
                            }
                        }//to avoid multiple click
                    };

                    /**
                     * select date event
                     * @param event
                     */

                    scope.selectDate = function (event) {
                        var ele = angular.element(event.delegateTarget);
                        var currentDate = $.trim(ele.find("span.hid-date").text());
                        var dateObj = null;
                        var currentDateOption = {
                            selectedDate: currentDate,
                            dayObj: dateObj,
                            sequence: scope.$parent.$index,
                            isOriginalEvent: !!event.originalEvent
                        };

                    if(scope.onBeforeSelectDate(currentDateOption)){
                        selectNewDate(event);
                    }

                    //for retaining the selected dates after traversing back to the flight select page from the navigation bar
                    if(!currentDateOption.isOriginalEvent){
                        angular.element(event.delegateTarget).addClass("selected-date");
                    }
                };

                    function selectNewDate(event) {
                        var slider = angular.element(element[0].children[0].firstChild);
                        angular.element(slider[0].children).removeClass("selected-date");
                        var ele = angular.element(event.delegateTarget).addClass("selected-date");

                        var currentDate = $.trim(ele.find("span.hid-date").text());
                        var dateObj = null;
                        angular.forEach(timeLineData, function (obj, key) {
                            if (currentDate === obj.date) {
                                dateObj = obj;
                            }
                        });
                        var currentDateOption = {
                            selectedDate: currentDate,
                            dayObj: dateObj,
                            sequence: scope.$parent.$index
                        };
                        unSelectDataInArray(currentDate);

                        scope.onSelectDate(currentDateOption);
                    }
                    /* scope.selectDate = function (event) {
                     var slider = angular.element(element[0].children[0].firstChild);
                     angular.element(slider[0].children).removeClass("selected-date");
                     var ele = angular.element(event.delegateTarget).addClass("selected-date");
                     var currentDate = $.trim(ele.find("span.hid-date").text());
                     var dateObj = null;
                     angular.forEach(timeLineData, function (obj, key) {
                     if (currentDate === obj.date) {
                     dateObj = obj;
                     }
                     });
                     var currentDateOption = {
                     selectedDate: currentDate,
                     dayObj: dateObj,
                     sequence: scope.$parent.$index
                     };
                     unSelectDataInArray(currentDate);
                     
                     scope.onSelectDate(currentDateOption);
                     }*/

                    /**
                     * set selected flag true or false when selecting a date
                     * @param currentDate
                     */

                    function unSelectDataInArray(currentDate) {
                        angular.forEach(timeLineData, function (obj, key) {
                            if (currentDate === obj.date) {
                                obj.selected = true;
                            } else {
                                obj.selected = false;
                            }
                        });
                    }

                    /**
                     * get next or  previous date before navigates
                     * @param direction
                     * @returns {*}
                     */

                    function getNextPrevDateBeforeNavigate(direction) {
                        var slider = angular.element(element[0].children[0].firstChild);
                        var ele, dayCount = 0;
                        if (direction === 'PREV') {// Next Previous Check
                            ele = angular.element(slider[0].children[1].children[0]);
                            dayCount = -1;
                        } else {
                            ele = angular.element(slider[0].children[slider[0].children.length - 1].children[0]);
                            dayCount = 1;
                        }
                        var currentDate = $.trim(ele.find("span.hid-date").text());
                        var nextOrPrev = moment(currentDate, "DD/MM/YYYY").add(dayCount, 'days').format("D/M/YYYY");
                        return nextOrPrev;
                    }

                    /**
                     * sets the date to the next or previous element either cached or new
                     * @param direction
                     * @param dataObj
                     * @param fromCache
                     */

                    function setDataArrayToCalendar(direction, dataObj, fromCache) {
                        var slider = angular.element(element[0].children[0].firstChild);
                        var elementPlace, formattedObj = fromCache ? dataObj : formatDaySlide(dataObj);
                        if (direction === "PREV") {
                            elementPlace = angular.element(slider[0].children[1].children[0]);
                        } else {
                            elementPlace = angular.element(slider[0].children[slider[0].children.length - 1].children[0]);
                        }
                        maintainDataArray(direction, formattedObj);
                        var temp = angular.element(sendDayHTML(formattedObj));
                        elementPlace
                                .empty()
                                .addClass(formattedObj.selected ? "selected-date" : "")
                                .append(temp);
                        $compile(temp)(scope);
                        navigationDone = true;
                    }

                    /**
                     * maintain the timeLineData, nextCache and prevCache arrays
                     * @param direction
                     * @param fmtObj
                     */

                    function maintainDataArray(direction, fmtObj) {
                        var temp;
                        fmtObj.selected = false;
                        if (direction === "PREV") {
                            if (scope.slideCount === 3 && timeLineData.length > 2) {
                                var length = timeLineData.length;
                                for (var i = length - 1; i >= 2; i--) {
                                    nextCache.unshift(timeLineData[i]);
                                }
                                for (var i = 2; i < length; i++) {
                                    timeLineData.pop();
                                }
                                timeLineData.unshift(fmtObj);
                            } else {
                                temp = timeLineData[timeLineData.length - 1];
                                timeLineData.pop();
                                timeLineData.unshift(fmtObj);
                                nextCache.unshift(temp);
                            }
                        } else {
                            temp = timeLineData[0];
                            timeLineData.shift();
                            timeLineData.push(fmtObj);
                            prevCache.push(temp);
                            if (scope.slideCount === 3 && timeLineData.length > 2) {
                                var length = timeLineData.length;
                                timeLineData.splice(3, length);
                            }
                        }
                    }

                    /**
                     * get cache data form the related arrays if cacheData==true
                     * @param direction
                     * @returns {*}
                     */

                    function getCacheData(direction) {
                        var ret;
                        if (!scope.cacheData) {
                            return null;
                        }
                        if (direction === 'PREV') {// Next Previous Check
                            ret = prevCache[prevCache.length - 1];
                            if (ret !== undefined) {
                                prevCache.pop();
                            }
                        } else {
                            ret = nextCache[0];
                            if (ret !== undefined) {
                                nextCache.shift();
                            }
                        }
                        if (ret === undefined) {
                            ret = null;
                        }
                        return ret;
                    }

                    /**
                     * auto selecting a date when there are no selected date
                     * on time line according to the navigation if only autoSelectDate==true
                     * @param direction
                     */

                    function autoSelectOnNavigation(direction) {
                        var slider = angular.element(element[0].children[0].firstChild), elementToSelect;
                        var hasSelectedDate = angular.element(slider[0].children).hasClass("selected-date");
                        if (!hasSelectedDate) {
                            if (direction === "PREV") {
                                elementToSelect = angular.element(slider[0].children[slider[0].children.length - 1]);
                            } else {
                                elementToSelect = angular.element(slider[0].children[1]);
                            }
                            $timeout(function () {
                                elementToSelect.triggerHandler('click');
                            });
                        }
                    }

                    /**
                     * Set Sizes of the Slider and slides
                     * @param scope
                     */
                    function setSizes(scope) {
                        angular.element(element).css({"width": "auto"});
                        angular.element(element[0].parentNode).css("width", "auto");
                        var slideCount = 7,
                                elementWidth = angular.element(element)[0].offsetWidth - 50;
                        if (elementWidth >= 718) {
                            scope.slideCount = 7;
                        } else {
                            scope.slideCount = 3;
                            slideCount = 3;
                        }
                        slideWidth = Math.floor(elementWidth / slideCount) - 2;
                        var varDist = (leftOrRight === "left") ? -(slideWidth) : slideWidth;
                        scope.slideWidth = {'width': slideWidth};
                        scope.sliderWidth = {'width': slideWidth * (slideCount + 2), "left": varDist};
                        angular.element(element).css({"width": (slideWidth * slideCount) + 50});
                        angular.element(element[0].children[0]).css({"width": (slideWidth * slideCount)});
                    }

                    /**
                     * Draw slider
                     * @param scope
                     */
                    function drawSlider(scope) {
                        var liHtml = '<li ng-click="selectDate($event)" ng-repeat="day in sliderData track by $index" ng-style="slideWidth" >' +
                                '<div class="day-block">' +
                                '<div class="animated bounceIn">' +
                                '<img src="js/util/isaui_lib/dist/images/bx_loader.gif" alt="loading" />' +
                                '</div>' +
                                '</div></li>';
                        var newLi = angular.element(liHtml);
                        $compile(newLi)(scope);
                        var slider = angular.element(element[0].children[0].firstChild);
                        slider.empty().append(newLi);
                    }


                    /**
                     * HTML formatter function to set the date to present return HTML String
                     * @param dayObj
                     * @returns {string}
                     */
                    function sendDayHTML(dayObj) {
                        var x, y, z = false;
                        if (!dayObj.dateFare) {
                            x = true;
                        } else if (dayObj.dateFare && dayObj.dateFare != -1) {
                            y = true;
                        } else if (dayObj.dateFare && dayObj.dateFare == -1) {
                            z = true;
                        }
                        var html = '<div class="animated ' + displayAnim + '">' +
                                '<span class="day-date-text">' + dayObj.dateText + '</span><span class="hid-date hide">' + dayObj.date + '</span>' +
                                '<span class="day-month-date-text">' + dayObj.dateNumber + ' ' + dayObj.monthText + '</span>' +
                                '<span class="day-fare" ng-show="' + y + '">' +
                                '<span class="currency">' + scope.currency + '</span> <span class="day-fare-value fare-value" isa-connector ="' + dayObj.dateFare + '" ></span> <i class="more">+</i>' +
                                '</span>' +
                                '<span class="day-fare not-fare" ng-show="' + x + '">' +
                                '<i class="more">No Flights</i></span>' +
                                '<span class="day-fare not-fare" ng-show="' + z + '">' +
                                '<i class="more">Flight Full</i>' +
                                '</span></div>';
                        return html;
                    }

                    /**
                     * setting the data to the created dates of the time line initially
                     */
                    function setTimeLineDataFirst() {

                        var slideElementArray = element.find(".day-block");

                        angular.forEach(slideElementArray, function (obj, key) {
                            if (key !== 0) {
                                var tm = timeLineData[key - 1];
                                var temp = angular.element(sendDayHTML(tm));
                                angular.element(obj).html(temp);
                                $compile(temp)(scope);

                                //angular.element( temp ).scope().$destroy();
                                //angular.element(obj.parentElement).addClass(tm.selected?"selected-date":"");

                                if (tm.selected) {
                                    $timeout(function () {
                                        angular.element(obj.parentElement).triggerHandler('click');
                                    });
                                }
                            }
                        });
                    }

                    /**
                     * data formatter function
                     * @param data
                     * @returns {{date: (data.date|*), dateText: string, monthText: string, dateNumber: *, dateFare: ($scope.dummyDateObj.fareValue|*), moreFares: boolean, selected: *}}
                     */
                    function formatDaySlide(data) {
                        if (languageService.getSelectedLanguage() == 'fa') {
                            var dateString = data.date.split("/");
                            var jalaaliObject = dateTransformationService.toJalaali(parseInt(dateString[2]), parseInt(dateString[1]), parseInt(dateString[0]));
                            console.log(scope.daysText[moment(data.date, "DD/MM/YYYY").day()]);
                            var daysText = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
                            return {
                                date: data.date,
                                dateText: PersianDateFormatService.reformatJalaaliDayStringFromEnglish(daysText[moment(data.date, "DD/MM/YYYY").day()]),
                                monthText: PersianDateFormatService.reformatJalaaliMonthStringFromEnglish(jalaaliObject.jm),
                                dateNumber: jalaaliObject.jd,
                                dateFare: data.fareValue,
                                moreFares: true,
                                selected: data.selected,
                                flights: data.flights,
                                fareClasses: data.fareClasses
                            }
                        } else {
                            return {
                                date: data.date,
                                dateText: scope.daysText[moment(data.date, "DD/MM/YYYY").day()],
                                monthText: scope.monthText[moment(data.date, "DD/MM/YYYY").month()],
                                dateNumber: moment(data.date, "DD/MM/YYYY").date(),
                                dateFare: data.fareValue,
                                moreFares: true,
                                selected: data.selected,
                                flights: data.flights,
                                fareClasses: data.fareClasses
                            }
                        }

                    }

                    /**
                     * format row data as when data received
                     * @param caldata
                     * @returns {Array}
                     */
                    function formatDataWithSlider(caldata) {
                        var returnArr = filterDataForMobileSlider(caldata);
                        // Will return if it is for mobile(3 slide)
                        if (returnArr.length == 0) {
                            angular.forEach(caldata, function (obj, key) {
                                returnArr.push(formatDaySlide(obj));
                            });
                        }

                        return returnArr;
                    }


                    /**
                     * Returns only -1, current and +1 dates data in case of mobile 
                     * @param caldata
                     * @returns {Array}
                     */
                    function filterDataForMobileSlider(caldata) {
                        var returnArr = [];
                        if (scope.slideCount == 3) {
                            // if first date is last possible date 
                            // or user selected date is today's date
                            if (caldata[0].selected) {
                                for (var i = 0; i < 3; i++) {
                                    returnArr.push(formatDaySlide(caldata[i]));
                                }
                                return returnArr;
                            } else {
                                var prev = null;
                                var midDateFound = false;
                                angular.forEach(caldata, function (obj, key) {
                                    if (midDateFound) {
                                        returnArr.push(formatDaySlide(obj));
                                        return false;
                                    } else if (obj.selected) {
                                        midDateFound = true;
                                        returnArr.push(formatDaySlide(prev));
                                        returnArr.push(formatDaySlide(obj));
                                    }
                                    prev = obj;
                                });
                                return returnArr;
                            }
                        } else {
                            return [];
                        }
                    }

                    // Directive Style Class
                    element.addClass("accelAero-slide").css("direction", scope.isRtl ? "rtl" : "ltr");
                    // Wrapping up the <ul>
                    var nav = angular.element('<div class="fare-floater-wrapper"></div>');
                    angular.element(element[0].children[0]).wrap(nav);
                    $compile(nav)(scope);//apply scope


                    /**
                     * ends scope watches
                     */

                    /**
                     * Directive Init function on element ready
                     */
                    element.ready(function () {
                        setSizes(scope);
                        drawSlider(scope);
                        for (var i = 0; i <= scope.slideCount; i++) {
                            scope.sliderData[i] = null;
                        }

                        /**
                         * starts scope watches
                         */

                        scope.$watch('calData', function (newVal, oldVal) {
                            if (newVal.data !== undefined) {
                                timeLineData = formatDataWithSlider(newVal.data);
                                setTimeLineDataFirst();
                            }
                        });

                        scope.$watch('slideCount', function (newVal, oldVal) {
                            if (newVal !== oldVal) {
                                scope.sliderData = [];
                                for (var i = 0; i <= scope.slideCount; i++) {
                                    scope.sliderData[i] = null;
                                }
                            }
                        });

                    });

                    /**
                     * Window Resize
                     */
                    angular.element($window).bind('resize', function () {
                        //setSizes(scope);
                        //drawSlider(scope);
                        //scope.$apply();
                        //setTimeLineDataFirst();
                    });

                }
            };
        }];
    isa_fare_timeline.directive(directives);
})();
