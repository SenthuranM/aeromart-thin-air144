'use strict';

(function (angular) {
    var module = angular.module('isaJqDateUtc', []);
    module.directive('isaJqDateUtc', ['$parse', function ($parse) {
        return {
            restrict: 'AE',
            require: '?ngModel',
            link: function (scope, ele, attr) {
                scope.$watch(attr.ngModel, function (value) {
                    if(value){
                        var DATE_FORMAT = "YYYY-MM-DD";

                        var model = $parse(attr.isaJqDateUtc);
                        var formattedDateString = moment(value).format(DATE_FORMAT);
                        var utcDate = moment.utc(formattedDateString, DATE_FORMAT).toDate();
                        model.assign(scope, utcDate);
                    }
                })
            }
        };
    }]);
})(angular);