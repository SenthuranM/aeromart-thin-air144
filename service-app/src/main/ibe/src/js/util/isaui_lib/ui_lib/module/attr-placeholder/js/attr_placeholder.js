'use strict';

(function () {
    var isa_attr_placeholder = angular.module("isaAttrPlaceholder", []);
    var directives = {};
    var isaConfig = new IsaModuleConfig();

    directives.isaattrplaceholder = ['$compile', '$http', '$templateCache', function ($compile, $http, $templateCache) {
        var directive = {};
        directive.restrict = 'AEC';
        directive.scope = {
            placeholderCaption: '@',
            placeholderSymbol: '@',
            isVisible: '=',
            isInputDisabled: '=',
            onChange: '=',
            bindTo: '@'
        };
        directive.require = 'ngModel';
        directive.link = ['scope', 'element', 'attr', 'ngModel', function (scope, element, attr, ngModel) {
            scope.onChange = "";
            scope.modelVal = null;

            var template = $templateCache.get(isaConfig.getTemplates("isaAttrPlaceholder")['default']);

            template = angular.element(template);
            element.after(template);
            $compile(template)(scope);

            scope.removePlaceholder = function () {
                if (scope.isInputDisabled) {
                    scope.isVisible = true;
                    element.focus();
                    $(element).siblings('.placeholder').hide();
                }
            };

            //Hides placeholder without getting focus
            scope.hidePlaceholder = function () {
                scope.isVisible = true;
                $(element).siblings('.placeholder').hide();
            };

            element.on('blur', function (e) {
                if ((element.val().length === 0)) {
                    scope.isVisible = true;
                    $(element).siblings('.placeholder').show();
                    $('[bind-to = ' + scope.bindTo + ']').find('.placeholder').show();
                }
            });

            element.on('change paste keyup focus', function (e) {
                scope.isVisible = false;
                $(element).siblings('.placeholder').hide();
                $('[bind-to = ' + scope.bindTo + ']').siblings('.placeholder').hide();
            });

            //Watches a change in the ngModel and hides the placeholder if the model is not empty
            scope.$watch(function () {
                    return ngModel.$modelValue;
                },
                function () {
                    if (ngModel.$modelValue) {
                        scope.hidePlaceholder();
                    }
                });
        }];
        return directive;
    }];
    isa_attr_placeholder.directive(directives);
})();