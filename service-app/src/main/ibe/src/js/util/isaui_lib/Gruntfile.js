//Grunt is just JavaScript running in node, after all...
'use strict';
module.exports = function(grunt) {

    // All upfront config goes in a massive nested object.
    grunt.initConfig({
        // You can set arbitrary key-value pairs.
        distFolder: 'dist',
        srcFolder: 'ui_lib',
        // You can also set the value of a key as parsed JSON.
        // Allows us to reference properties we declared in package.json.
        pkg: grunt.file.readJSON('package.json'),
        // Grunt tasks are associated with specific properties.
        // these names generally match their npm package name.
        clean: ['<%= distFolder %>'],
        concat: {
            // Specify some options, usually specific to each plugin.
            options: {
                // Specifies string to be inserted between concatenated files.
                separator: ';'
            },
            // 'dist' is what is called a "target."
            // It's a way of specifying different sub-tasks or modes.
            dist: {
                // The files to concatenate:
                // Notice the wildcard, which is automatically expanded.
                src: ['js/isaui_module_config.js', 'ui_lib/module/{,*/}/js/*.js', 'ui_lib/services/{,*/}*.js','js/isaui_controller.js', 'js/isa_directives.js'],
                // The destination file:
                // Notice the angle-bracketed ERB-like templating,
                // which allows you to reference other properties.
                // This is equivalent to 'dist/main.js'.loadNpmTasks
                dest: '<%= distFolder %>/js/isaui_lib.js'
                // You can reference any grunt config property you want.
                // Ex: '<%= concat.options.separator %>' instead of ';'
            }
        },
        uglify: {
            files: {
                src: '<%= distFolder %>/js/isaui_lib.js',  // source files mask
                dest: '<%= distFolder %>/js',    // destination folder
                expand: true,    // allow dynamic building
                flatten: true,   // remove all unnecessary nesting
                ext: '.min.js'   // replace .js to .min.js
            }
        },
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd:'ui_lib/module/styles/',
                    src: '*.scss',
                    dest: '<%= distFolder %>/css',
                    ext: '.css'
                }]
            }
        },
        compass: {
            dist: {                   // Target
                options: {              // Target options
                  sassDir: 'ui_lib/module/styles',
                  cssDir: '<%= distFolder %>/css',
                  environment: 'production'
                }
            }
        },
        ngtemplates:  {
            isaApp:{
                cwd:'ui_lib/',
                src:['module/{,*/}/view/*.html', 'module/{,*/}/view/*/*.html'],
                dest:'<%= distFolder %>/js/lib.templates.js',
                options:    {
                    htmlmin:  { collapseWhitespace: true, collapseBooleanAttributes: true }
                }
            }
        },
        copy: {
            dist:{
                src: ['**/images/{,*/}*.*'],
                dest: '<%= distFolder %>/images/',
                cwd: 'ui_lib/module/',
                expand: true,
                flatten:false,
                rename: function(dest, src) {
                    // Split into array, remove modules/*
                    var parts = src.split('/').slice(2);
                    // Prepend tmp
                    parts.unshift(dest);
                    // Rejoin.
                    return parts.join('/');
                }
            }
        },
        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish'),
                force : true
            },
            all: {
                src: [
                    'Gruntfile.js',
                    'js/{,**/}*.js',
                    'ui_lib/{,**/}*.js'
                ]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand:true,
                    cwd: '<%= distFolder %>/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: '<%= distFolder %>/css',
                    ext: '.min.css'
                }]
            }
        }
    }); // The end of grunt.initConfig

    // We've set up each task's configuration.
    // Now actually load the tasks.
    // This will do a lookup similar to node's require() function.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Register our own custom task alias.
    grunt.registerTask('build', ['clean','jshint:all','concat', 'uglify', 'ngtemplates', 'copy', 'compass', 'cssmin']);
};