/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_spinner = angular.module("isaspinner", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    var currentValue = 1;

    directives.isaspinner = [
        '$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};
            //load relavent tamplate according to the button type

            directive.restrict = 'AEC';
            directive.scope = {
                max: '=',
                min: '=',
                setCount: '&',
                value: '='
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    //var template = isaconfig.getTemplates("isaspinner");
                    var template = $templateCache.get(isaconfig.getTemplates("isaspinner")['default']);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);

                    /*$templateRequest(template['default'],[false]).then(function(html){
                     var template = angular.element(html);
                     element.append(template);
                     $compile(template)(scope);
                     });*/
                    //console.log(template)


                };
            return directive;

        }];
    isa_spinner.controller('spinnerController', ['$scope', '$element', function ($scope, $element) {
        $element.find('input').on('keypress', function (event) {
            if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
                event.preventDefault(); //stop character from entering input
            } else {
//                console.log(String.fromCharCode(event.which));
            }
        });
        $element.find('input').on('blur', function () {
            var input = Number($(this).val());
            if (input > $scope.max || input < $scope.min || $(this).val().trim().length <= 0) {
                $(this).val(currentValue);
            }

        });
        $element.find('input').on('focus', function () {
            currentValue = Number($(this).val());
        });

        $scope.onIncriment = function () {
            var count = Number($element.find('input').val());
            if (count < $scope.max) {
                count++;
            }
            $element.find('input').val(count);
            var data = {spinnerValue: count};
            $scope.setCount(data);

        };

        $scope.onDecriment = function () {
            var count = Number($element.find('input').val());
            if (count > $scope.min) {
                count--;
            }
            $element.find('input').val(count);
            var data = {spinnerValue: count};
            $scope.setCount(data);
        };

    }]);
    isa_spinner.directive(directives);
})();

