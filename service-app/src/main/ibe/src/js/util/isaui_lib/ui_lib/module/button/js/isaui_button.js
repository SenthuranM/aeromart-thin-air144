/**
 * Created by indika on 10/16/15.
 */
'use strict';

var isa_btn = angular.module("isaButton", []);
var directives = {};
directives.isabtn = [
    '$compile', '$http', '$templateCache',
    function ($compile, $http, $templateCache) {
        var directive = {};
        var isaconfig = new IsaModuleConfig();
        //load relavent tamplate according to the button type
        var getTemplate = function (type) {
            var template = '';
            switch (type) {
                case "radio" :
                    template = $templateCache.get(isaconfig.getTemplates("isaButton")[type]);
                    break;
                case "checkbox" :
                    template = $templateCache.get(isaconfig.getTemplates("isaButton")[type]);
                    break;
                default :
                    template = $templateCache.get(isaconfig.getTemplates("isaButton")['default']);
                    break;
            }
            return template;
        };
        directive.restrict = 'AEC';
        directive.scope = {caption: '=name'};
        directive.require = //other sibling directives;
            directive.link = function (scope, element, attr) {
                var template = angular.element(getTemplate(attr.type));
                element.append(template);
                $compile(template)(scope);
            };
        return directive;

    }];
isa_btn.directive(directives);

