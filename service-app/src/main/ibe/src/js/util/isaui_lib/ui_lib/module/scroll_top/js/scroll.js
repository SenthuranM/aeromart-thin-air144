/**
 * Created by nipuna on 10/4/15.
 */

'use strict';

(function () {
    var isa_scroll_top = angular.module("scrollTop", []);
    var directives = {};

    directives.scrollTop = [function () {
        return {
            link: function (scope, element) {
                element.removeClass("drop-shadow-bottom");

                scope.$watch(function () {
                    return element.offset().top
                }, function (topOffset) {
                    if (topOffset < 110) {
                        element.removeClass("drop-shadow-bottom");
                    } else {
                        element.addClass("drop-shadow-bottom");
                    }
                });
            }
        };
    }];
    isa_scroll_top.directive(directives);
})();