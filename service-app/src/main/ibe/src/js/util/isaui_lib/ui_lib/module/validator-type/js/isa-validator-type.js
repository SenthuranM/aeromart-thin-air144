'use strict';

(function () {
    var isa_validator_type = angular.module("isaValidatorType", []);
    var directives = {};
    directives.isaValidatorType = function () {
        return {
            priority: 1,
            controller: ['$attrs', function ($attrs) {

                this.getType = function () {
                    return $attrs.isaValidatorType;
                };

                this.getRules = function () {
                    return $attrs.isaValidatorRules;
                };

                this.getName = function () {
                    return $attrs.name;
                };

            }]
        };
    };
    isa_validator_type.directive(directives);
})();