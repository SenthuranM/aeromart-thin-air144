'use strict';
(function () {
    var isa_progress = angular.module("isaProgress", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaProgress = [
        '$compile',
        '$templateCache',
        'ancillaryService',
        '$rootScope',
        function ($compile, $templateCache, ancillaryService, $rootScope) {
            var directive = {};
            directive.scope = {
                main: '=',

            };
            directive.restrict = 'AEC';
            directive.link = function (scope, element) {
                var template = $templateCache.get(isaconfig.getTemplates("isaProgress")['default']);
                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
            };
            directive.controller = ["$rootScope", '$scope', 'eventListeners', "commonLocalStorageService", 'applicationService', "ancillaryService","$translate","currencyService",
                function ($rootScope, $scope, eventListeners, commonLocalStorageService, applicationService, ancillaryService,$translate,currencyService) {
                    $scope.skipService = '';
                    $scope.extraNavTag = ''; 
                    $scope.type = '';
                    $scope.curencyTypes = commonLocalStorageService.getCurrencies(); 
                    $scope.defaultCurrencyType = currencyService.getSelectedCurrency() || "AED";
                    $scope.applicationMode = applicationService.getApplicationMode();
                    
                     $translate.onReady().then(function() {
                         $scope.modifyValue = $translate.instant('btn_selectFare_ModifySearch').length;
                     });
                    
                    $scope.showModifySearch = function () {
                        eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
                    }; 
                    
                    $scope.skipServices = function () {                       
                        ancillaryService.skipServices($scope.type); 
                        $scope.skipService = '';
                        $scope.extraNavTag = '';
                        $rootScope.extraType = '';
                    };

                    switch ($rootScope.extraType) {
                        case 'baggage' :
                            $scope.skipService = "btn_extra_skipBag";
                            $scope.extraNavTag = "lbl_header_nav_baggage";
                            $scope.type = 'BAGGAGE';
                            break;
                        case 'seats' :
                            $scope.skipService = "btn_extra_skipSeat_mobile";
                            $scope.extraNavTag = 'lbl_header_nav_seat';
                            $scope.type = 'SEAT';
                            break;
                        case 'meals' :
                            $scope.skipService = "btn_extra_skipMeal";
                            $scope.extraNavTag = 'lbl_header_nav_meals';
                            $scope.type = 'MEAL';
                            break;
                        case 'insurance' :
                            $scope.skipService = "btn_extra_skipIns";
                            $scope.extraNavTag = 'lbl_header_nav_insurance';
                            $scope.type = 'INSURANCE';
                            break;
                        case 'services' :
                            $scope.skipService = "btn_extra_skipSvc";
                            $scope.extraNavTag = 'lbl_header_nav_inFlightServices';
                            $scope.type = 'SSR_IN_FLIGHT';
                            break;
                        case 'airport_services' :
                            $scope.skipService = "btn_extra_skip";
                            $scope.extraNavTag = 'lbl_header_nav_airport_service';
                            $scope.type = 'SSR_AIRPORT';
                            break;
                        case 'flexibility' :
                            $scope.skipService = "btn_extra_skipFlexi";
                            $scope.extraNavTag = 'lbl_header_nav_flexi';
                            $scope.type = 'FLEXIBILITY';
                            break;
                        case 'airport_transfer' :
                            $scope.skipService = "btn_extra_skip";
                            $scope.extraNavTag = 'lbl_header_nav_airport_service';
                            $scope.type = 'AIRPORTTRANSFER';
                            break;
                    }

                }];
            return directive;
        }];
    isa_progress.directive(directives);
})();
