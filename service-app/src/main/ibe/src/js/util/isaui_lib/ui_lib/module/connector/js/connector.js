/**
 * Created by indika on 11/18/15.
 */
'use strict';

(function () {
    var isa_connector = angular.module("isaConnector", []);
    var directives = {};

    directives.isaConnector = ['$rootScope', 'currencyService', '$timeout','$filter', '$translate','constantFactory',
        function ($rootScope, currencyService, $timeout,$filter, $translate, constantFactory) {
            var directive = {};
            directive.restrict = 'A';
            directive.scope = {
                isaConnector: "@",
                decimalPoints: "@",
                isPromotion:"@",
                currency: "@",
                withDecimal:'=',
                isBaseCurrency:'@',
                noExchangeCalc:'@'
            };
            directive.link = function (scope, element, attr) {
                
                var passAmount = scope.isaConnector || 0;
                var decimalPoints = parseInt(scope.decimalPoints) || 0;
                var isPromotion= scope.isPromotion || false;
                var currency = scope.isBaseCurrency ? currencyService.getAppBaseCurrency() :  currencyService.getSelectedCurrency();

                function calculateCurrency(currentAmount) {
                    if (isNaN(currentAmount)) {
                        currentAmount = 0;
                    }
                    var currencyCode = ( currency == currencyService.getSelectedCurrency() ) ? currency : currencyService.getSelectedCurrency() ; 
                    var selectedCurrency = currencyService.getSelectedCurrencyInfo(currencyCode);
                    var base = selectedCurrency.baseToCurrExRate;
                    var amount = 0;
                    var showDecimalPoints;
                    
                    scope.isBaseCurrency ? amount = currentAmount : amount = currentAmount * (1 / base);

                    // scope.noExchangeCalc ? amount = currentAmount : amount = currentAmount * (1 / base);
                    
                    if (constantFactory.AppConfigStringValue('showDecimalPlacesForCurrencyValues') === 'true') {
                        decimalPoints = selectedCurrency.decimalPlaces;
                        showDecimalPoints = true;
                    }

                    if (showDecimalPoints || scope.withDecimal) {
                        
                        amount = $filter('number')(amount, decimalPoints);

                    } else {
                        amount = Math.ceil(amount);
                    }

                    if(isPromotion){
                        amount = '-'+amount
                    }

                    //If element is not in the DOM set a timeout and set the currency and amount
                    if(!angular.element(element)){
                        $timeout(function(){
                            angular.element(element).text(amount);
                            angular.element(".currency").text(currencyService.getSelectedCurrency());
                        })
                    }else{
                        angular.element(element).text(amount);
                        angular.element(".currency").text(currencyService.getSelectedCurrency());

                        var translationString = 'lbl_payment_currency' + currencyService.getSelectedCurrency();
                        var translated = $translate.instant(translationString);

                        if(translated != translationString){
                            angular.element(".currency").text(translated);
                        }
                    }

                }

                calculateCurrency(passAmount);

                //TODO remove the $rootScope by using a event listener service
                $rootScope.$on('ON_CURRENCY_CHANGE', function (e, data) {
                    currencyService.setSelectedCurrency(currencyService.getSelectedCurrency());
                    calculateCurrency(passAmount);
                    $('.currency').text(currencyService.getSelectedCurrency());
                });

                scope.$watch(
                    "isaConnector",
                    function handleFooChange(newValue, oldValue) {
                        calculateCurrency(newValue);
                    }
                );

            };
            return directive;
        }];
    isa_connector.directive(directives);
})();
