/**
 * Temporary option list for the Hala services
 * TODO: Integrate into the option_list directive
 */
'use strict';

(function () {
    var airport_transfer_list = angular.module("airportTransferList", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.airportTransferList = [
        '$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                availableOptions: '=',
                addTransfer: '&',
                removeTransfer: '&',
                source: '=',
                selectedItems: '=',
                passenger: '=',
                validations: '=',
                currentSelection: '=',
                index:'='
            };

            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    var template = $templateCache.get(isaconfig.getTemplates("airportTransferList")['default']);
                    var DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss';

                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);
                    scope.comment = {};
                    scope.isModify = false;
                    scope.datePicker = {
                        isOpenDp : false
                    };
                    scope.dateOptions = {
                        maxDate: null,
                        minDate: null
                    };

                    scope.getCharge = function(item){
                        var currentCharge = 0;
                        var infantCharge = 0;

                        _.forEach(item.charges, function(charge){
                            if (charge.chargeBasis.toLowerCase() == scope.passenger.type){
                                currentCharge = charge.amount;
                            }

                            if(charge.chargeBasis == 'ADULT_WITH_INFANT'){
                                infantCharge = charge.amount;
                            }
                        });

                        if(scope.passenger.infantSequence){
                            currentCharge = infantCharge;
                        }
                        item.currentCharge = currentCharge;
                        return currentCharge;
                    };

                    scope.setSelected = function(index, item){
                        setValidations(item);
                        scope.selectedItem = index;
                    };

                    scope.collapse = function(){
                        scope.selectedItem = null;
                        scope.isModify = false;
                    };

                    scope.isItemSelected = function(item){
                        var isSelected = false;
                        _.forEach(scope.currentSelection.selectedItems, function(val){
                            if(val.airportTransferId === item.airportTransferId){
                                isSelected = true;
                            }
                        });
                        return isSelected;
                    };

                    scope.addAirportTransfer = function(item){
                        scope.selectedItem = null;
                        scope.isModify = false;
                        scope.addTransfer({item: item, comment: scope.comment, dateValidations: scope.dateOptions});
                    };

                    scope.removeAirportTransfer = function(item){
                        scope.removeTransfer({item: item});
                        scope.comment = {};
                    };

                    scope.modifyAirportTransfer = function(item, index){
                        scope.isModify = true;

                        var selectedItemInput = {};
                        _.forEach(scope.currentSelection.selectedItems, function(val){
                            if(val.airportTransferId === item.airportTransferId){
                                selectedItemInput = val.input;
                            }
                        });

                        scope.comment = selectedItemInput;
                        scope.comment.transferTime = moment(selectedItemInput.tranferDate, DATE_TIME_FORMAT).toDate();
                        scope.comment.transferDate = moment(selectedItemInput.tranferDate, DATE_TIME_FORMAT).toDate();

                        scope.setSelected(index, item);
                    };

                    scope.openDp = function(){
                        scope.datePicker.isOpenDp = true;
                    };

                    scope.$watch('currentSelection', function() {
                        scope.comment = {};
                        scope.collapse();
                    });

                    function setValidations(item){
                        var dateValidations = {};

                        _.forEach(item.validations, function(valId){
                            dateValidations = _.find(scope.validations, function(validationRule){
                                return ((validationRule.ruleValidationId == valId) && (validationRule.ruleCode == 'ibe.ancillary.rule.calander.date.restrict'));
                            });
                        });

                        if(dateValidations.maxDate){
                            scope.maxDate = new Date(moment(dateValidations.maxDate).format());
                        }else{
                            scope.maxDate = null;
                        }

                        if(dateValidations.minDate){
                            scope.minDate = new Date(moment(dateValidations.minDate).format());
                        }else{
                            scope.minDate = null;
                        }

                        scope.dateOptions.minDate = scope.minDate;
                        scope.dateOptions.maxDate = scope.maxDate;
                    }
                };
            return directive;
        }
    ];

    airport_transfer_list.directive(directives);
})();

