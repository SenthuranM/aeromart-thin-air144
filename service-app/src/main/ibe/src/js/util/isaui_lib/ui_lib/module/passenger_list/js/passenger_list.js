/**
 * Created by nipuna on 11/23/15.
 */
'use strict';

(function () {
    var isa_passenger_list = angular.module("isaPassengerList", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaPassengerList = [
        '$compile', '$http', '$templateCache', '$document', '$rootScope', '$translate','alertService',
        function ($compile, $http, $templateCache, $document, $rootScope, $translate,alertService) {
            var directive = {};

            directive.restrict = 'AEC';
            directive.scope = {
                optionClick: '&',
                optionClickparent: '&',
                passengerList: '=',
                selectedPassenger: '=',
                passengerChange: '&',
                onActionClick: '&',
                applicationMode: '@',
                passenger: '=',
                passengerCount: '@',
                updateContact1: '&',
                passengerCompleted: '=',
                onModificationCompletedparent: '&',
                paxDetailConfig: '=',
                firstPaxContact: '=',
                submitted: '=',
                airewardsChecked: '=',
                updateAirewardsStatus1: '&',
                onModificationStart: '&',
                countryList: '=',
                isAirwordEnabled: '=',
                validationMessages: '=',
                carrier: '=',
                deviceMode: '=',
                source: '=',
                template: '@',
                selectedItems: '=',
                passengerType: '=',
                outerIndex: '=',
                service: '@',
                addTransferParent: '&',
                removeTransferParent: '&',
                validations: '=',
                currentSelection: '=',
                language: '='
            };


            directive.link = function (scope, element, attr) {
                var type = (attr.listType === undefined) ? 'default' : attr.listType;
                scope.isActive = false;

                scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                $translate.onReady().then(function () {
                    scope.add = $translate.instant('lbl_common_add');
                    scope.remove = $translate.instant('lbl_common_remove');
                });

                var categoryPaxCount = {};

                //Set the display names to category if not defined
                angular.forEach(scope.passengerList, function (value, key) {
                    if (!scope.passengerList[key].displayName) {

                        if (_.isUndefined(categoryPaxCount[scope.passengerList[key].category])) {
                            categoryPaxCount[scope.passengerList[key].category] = 1;
                        } else {
                            categoryPaxCount[scope.passengerList[key].category] = categoryPaxCount[scope.passengerList[key].category] + 1;
                        }
                        var translatedDisplayName = scope.passengerList[key].category;
                        if (scope.passengerList[key].category == 'Adult') {
                            translatedDisplayName = $translate.instant('lbl_common_adult');
                        } else if (scope.passengerList[key].category == 'Child') {
                            translatedDisplayName = $translate.instant('lbl_common_child');
                        } else if (scope.passengerList[key].category == 'Infant') {
                            translatedDisplayName = $translate.instant('lbl_common_infant');
                        }

                        scope.passengerList[key].displayName = translatedDisplayName + " " +
                                (categoryPaxCount[scope.passengerList[key].category] || "");
                    }
                });

                var template = $templateCache.get(isaconfig.getTemplates("isaPassengerList")[type]);
                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);

                scope.optionOnClick = function (options, $event) {
                    scope.optionClickparent({id: options.id, index: options.outerindex});

                    scope.selectedIndex = options.id;

                    if (scope.source[options.index].selected === true) {
                        scope.source[options.index].selected = false;
                    } else {
                        scope.source[options.index].selected = true;
                    }
                    if ($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                };




                scope.accordionArray = [];

                scope.changeSelected = function (e, event) {
                    //alert("into changeselected")
                    if (type === "seatmap") {
                        scope.selectedPassenger = e;
                    } else {
                        if (scope.passengerList[scope.selectedPassenger].dirty === true && scope.passengerList[scope.selectedPassenger].valid === false) {
                        	var message = "lbl_passenger_fillAllPaxDetail";
                            alertService.setAlert(message, 'warning');
                            scope.passengerList[scope.selectedPassenger].submitted = true;
                            if (event)
                                event.stopPropagation();
                        } else {
                            scope.selectedPassenger = e;
                        }
                    }
                    scope.passengerChange({pax: e});
                };
                scope.updateContact = function (passenger) {
                    scope.updateContact1({passenger: passenger});
                }
                scope.updateAirewardsStatus = function (index) {
                    scope.updateAirewardsStatus1({index: index});
                }
                scope.actionClick = function ($event, paxIndex) {
                    $event.stopPropagation();
                    scope.onActionClick({paxIndex: paxIndex});
                }
                scope.addTransfer = function (item, comment, dateValidations) {
                    scope.addTransferParent({item: item, comment: comment, dateValidations: dateValidations});
                }
                scope.removeTransfer = function (item) {
                    scope.removeTransferParent({item: item});

                };
                scope.onModificationCompleted = function () {
                    scope.onModificationCompletedparent();
                }
                /**
                 * Show More Passenger Popup
                 */
                scope.showPopup = function () {

                    var popWidth = 275;
                    //var popHeight = 260;
                    scope.blockSection = true;
                    scope.popupPos = {
//                        "top": angular.element('#data').position().top,
//                        "left": angular.element('#data').position().left,
                        "width": popWidth,
                        "height": "auto"
                    };
                    scope.applyMask = {"display": "block", "z-index": 10};
                };

                /**
                 * Close Mode Passenger popup
                 */
                scope.closePopup = function () {
                    scope.applyMask = {"display": "hide", "z-index": -10};
                    scope.blockSection = false;
                    scope.isPopupVisible = true;
                    scope.isContinuClicked = false;
                };

            };
            directive.controller = ['$scope', '$element', '$rootScope', function ($scope, $element, $rootScope) {

                }];
            return directive;

        }];
    isa_passenger_list.directive(directives);
})();

