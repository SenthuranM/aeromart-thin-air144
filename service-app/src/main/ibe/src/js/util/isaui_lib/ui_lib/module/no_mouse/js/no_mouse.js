/**
 * Created by sumudu on 1/31/16.
 */
'use strict';

(function(angular){
    var module = angular.module('isaNoMouse',[]);
    module.directive('noMouse',[function(){
        return  {
            restrict :'AE',
            scope : {
                isNoMouseActive : '='
            },
            link : function(scope,ele,attr){
                if(attr['isNoMouseActive'] == undefined) {
                    scope.isNoMouseActive = false;
                }

                var changeUI = function(){
                    if(scope.isNoMouseActive){
                        angular.element(ele).addClass('no-mouse-relative');
                        angular.element(ele).prepend( '<div class="no-mouse-inner">&nbsp;</div>');
                    }else{
                        angular.element(ele).removeClass('no-mouse-relative');
                        angular.element(ele).find('.no-mouse-inner').remove();
                    }

                };

                scope.$watch('isNoMouseActive',function(){
                    changeUI();
                })
            }
        };
    }]);
})(angular);