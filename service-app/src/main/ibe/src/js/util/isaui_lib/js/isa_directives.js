/**
 * Created by indika on 10/29/15.
 */
'use strict';

(function () {
    var isaconfig = new IsaModuleConfig();
    var isaCtrl = new IsaControllers();

    

    var isa_ui = angular.module('isaApp', isaconfig.getModuleNames()).run(['$rootScope', function ($rootScope) {
        var deviceType = {};

        deviceType.isMobileDevice = window.matchMedia("only screen and (max-width: 767px)").matches;

        deviceType.isSmallDevice = window.matchMedia("only screen and (min-width: 768px) and (max-width: 991px)").matches;

        deviceType.isLargeDevice = window.matchMedia("only screen and (min-width: 992px)").matches;

        $rootScope.deviceType  = deviceType;

    }]);

    isa_ui.controller(isaCtrl);
})();
