/**
 * Temporary option list for the Hala services
 * TODO: Integrate into the option_list directive
 */
'use strict';

(function () {
    var isa_services_optionlist = angular.module("isaservicesoptionlist", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaservicesoptionlist = [
        '$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                availableOptions: '=',
                optionClick: '&',
                source: '=',
                template: '@',
                dropdownSource: '=',
                secondDropDownSource: '=',
                dropdownClick: '&',
                secondaryOptionClick: '&',
                elementButtonCaptions: '=',
                widgetOptions: '=',
                selectedItems: '=',
                passengerType: '=',
                passenger: '=',
                onInputChange : '&'
            };

            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {

                    scope.pax = 0;
                    scope.flight = 0;
                    scope.trip = 'Outbound';

                    scope.isCatListVisible = false;
                    scope.orderByOption = "";
                    scope.selectedRowIndex = -1;

                    var template = $templateCache.get(isaconfig.getTemplates("isaservicesoptionlist")[scope.template]);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);

                    scope.onCommentChange = function(type){
                        scope.onInputChange({value:element.find('.data #comment_'+type).val(),type:type})
                    }

                    scope.optionOnClick = function (id) {
                        scope.optionClick(id);
                        scope.selectedIndex = id.id;

                        if (scope.source[id.index].selected === true) {
                            scope.source[id.index].selected = false;
                        } else {
                            scope.source[id.index].selected = true;
                        }
                    };

                    scope.moreInfoOnClick = function ($event) {
                        $event.stopPropagation();
                    };

                    scope.onSecondaryOptingClick = function (option) {
                        scope.secondaryOptionClick({option: option});
                        scope.orderByOption = option;
                    };
                    scope.dropdownOptionClick = function (id) {
                        scope.isCatListVisible = (scope.isCatListVisible) ? false : true;
                        scope.selectedIndex = -1;
                        if (id !== undefined) {
                            scope.dropdownClick(id);
                            scope.selectedOptionIndex = id.index;
                            scope.selectedDropDown = id.section;
                        }
                    };
                    function resetToDefault(e, scope) {
                        scope.selectedRowIndex = -1;
                        angular.element(e.currentTarget).find('.modify').removeClass('fa-times');
                        angular.element(e.currentTarget).find('.modify').addClass('fa-angle-down');
                        if (scope.elementButtonCaptions !== undefined) {
                            angular.element(e.currentTarget).find('.meal-select-caption').text(scope.elementButtonCaptions[0]);
                        }
                    }

                    scope.onElementClick = function (event, index) {
                        element.find('.meal-select-caption').text(scope.elementButtonCaptions[0]);
                        if (scope.selectedRowIndex === -1) {
                            scope.selectedRowIndex = index;
                            angular.element(event.currentTarget).find('.modify').addClass('fa-times');
                            angular.element(event.currentTarget).find('.modify').removeClass('fa-angle-down');
                            angular.element(event.currentTarget).find('.meal-select-caption').text(scope.elementButtonCaptions[1]);

                        } else {
                            scope.selectedRowIndex = -1;
                            angular.element(event.currentTarget).find('.modify').removeClass('fa-times');
                            angular.element(event.currentTarget).find('.modify').addClass('fa-angle-down');
                            angular.element(event.currentTarget).find('.meal-select-caption').text(scope.elementButtonCaptions[0]);

                        }

                    };
                    scope.onElementClose = function (index) {
                        //scope.selectedRowIndex = -1;
                    };
                    angular.element('body').on('click', function (e) {
                        if (element.find(e.target).length <= 0) {
                            resetToDefault(e, scope);
                            scope.selectedIndex = -1;
                            scope.isCatListVisible = false;
                        }
                        scope.$apply(function () {
                        });
                    });

                    (function init() {
//                        console.log("Some items: " + JSON.stringify(scope.selectedItems));
                        scope.source.map(function (extra) {

                        });
                    })();
                };
            return directive;

        }];
    isa_services_optionlist.controller('isaOptionListController', [function ($scope) {

    }]);
    isa_services_optionlist.directive(directives);
})();

