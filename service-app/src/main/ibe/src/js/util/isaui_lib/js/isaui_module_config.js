/**
 * Created by indika on 10/19/15.
 */

'use strict';
/*exported IsaModuleConfig*/
/*global IsaModuleConfig: true*/
var IsaModuleConfig = function(){
    var isamodules = {};
    //var _root = "ui_lib/module"; // UI Library test
    //var _root = "js/util/isaui_lib/ui_lib/module";
    //all
    var data = {
        scrollTop : {
            name: 'scrollTop'
        },
        isaButton : {
            name: 'isaButton',
            templates: {
                default:"module/button/view/button-view.html",
                radio:"module/button/view/radio_button.html",
                checkbox:"module/button/view/checkbox_view.html"
            }
        },
        isaCounterButton:{
            name:'isaCounterButton',
            templates : {
                counter:"module/counter_button/view/counter-button-view.html"
            }
        },
        isaStepProgress:{
            name:'isaStepProgress'
        },
        isaCurrencyConverter:{
            name:'isaCurrencyConverter',
            templates : {
                default:"module/currency_converter/view/currency-converter-view.html"
            }
        },
        fareFloater:{
            name:'fareFloater'
        },
        stickMe:{
            name:'stickMe'
        },
        isaspinner:{
            name:'isaspinner',
            templates : {
                default:"module/spinner/view/spinner-view.html"
            }
        },
        isaBusyLoader: {
            name: 'isaBusyLoader',
            templates: {
                default: 'module/busy_loader/view/busy_loader_tpl.html'
            }
        },
        isaLoadingProgress:{
            name:'isaLoadingProgress',
            templates : {
                default:"module/loading_progress/view/loading-progress-tpl.html"
            }
        },
        isaplaceholder:{
            name:'isaplaceholder',
            templates : {
                default:"module/placeholder/view/placeholder-tpl.html"
            }
        },
        isaAttrPlaceholder:{
            name:'isaAttrPlaceholder',
            templates : {
                default:"module/attr-placeholder/view/attr-placeholder-tpl.html"
            }
        },
        isaConnector:{
            name:'isaConnector'
        },
        isaFlightSelect:{
            name:'isaFlightSelect',
            templates : {
                v1: {
                    default_body: "module/flight_select/view/v1/flight-select-body.tpl.html",
                    modify_body: "module/flight_select/view/v1/modify_flight_select_body.html",
                    modify_selected_Flight_body: "module/flight_select/view/v1/modify_selected_flights_body.html"
                },
                v2: {
                    default_body: "module/flight_select/view/v2/flight-select-body.tpl.html",
                    modify_body: "module/flight_select/view/v2/modify_flight_select_body.html",
                    modify_selected_Flight_body: "module/flight_select/view/v2/modify_selected_flights_body.html"
                },
                default_header: "module/flight_select/view/flight-select-header.tpl.html",
                default_popup: "module/flight_select/view/flight-select-popup.tpl.html"
            }
        },
        isaFareAndServicesDetails:{
            name:'isaFareAndServicesDetails',
            templates : {
                default: "module/fare_and_services_details/view/fare-and-services-details.tpl.html"
            }
        },
        isaSavedCard:{
            name:'isaSavedCard',
            templates : {
            	default:"module/saved_card/view/saved-card-view.html"
            }
        },
        isaAutoCheckin:{
            name:'isaAutoCheckin',
            templates : {
            	default:"module/auto_checkin/view/auto-checkin-view.html"
            }
        },
        isaPassengerList:{
            name:'isaPassengerList',
            templates : {
                default:"module/passenger_list/view/passenger-list-view.html",
                seatmap:"module/passenger_list/view/passenger-list-for-seatmap.tpl.html",
                services:"module/passenger_list/view/passenger-list-for-services.tpl.html"
            }
        },
        isaPassengerDetails:{
            name:'isaPassengerDetails',
            templates : {
                default:"module/passenger_details/view/passenger-details-view.html"
            }
        },
        isaoptionlist:{
            name:'isaoptionlist',
            templates : {
                extras:"module/option_list/view/option_list_tpl.html",
                baggage:"module/option_list/view/baggage_option_list_tpl.html",
                meals:"module/option_list/view/meal_option_list_tpl.html",
                services:"module/option_list/view/services_option_list_tpl.html",
                insurance : "module/option_list/view/insurance_option_list_tpl.html",
                modificationFlightSegment :"module/option_list/view/modification_flight_segment_list_tpl.html",
                modificationExtras:"module/option_list/view/modification_extras_list_tpl.html"
            }
        },
        isaservicesoptionlist:{
            name:'isaservicesoptionlist',
            templates : {
                extras:"module/services_option_list/view/option_list_tpl.html",
                services:"module/services_option_list/view/services_option_list_tpl.html",
                inFlightServices:"module/services_option_list/view/in_flight_services_option_list_tpl.html"

            }
        },
        isaradiooptionlist:{
            name:'isaradiooptionlist',
            templates : {
                default:"module/radio_option_list/view/radio_option_list_tpl.html",
                services: "module/radio_option_list/view/services_radio_option_list_tpl.html",
                seat: "module/radio_option_list/view/seat_radio_option_list_tpl.html"
            }
        },
        seatMap:{
            name:'seatMap',
            templates : {
                default:"module/seatMap/view/seatmap.tpl.html"
            }
        },
        isaSpinnerList:{
            name:'isaSpinnerList',
            templates : {
                default:"module/spinner_list/view/spinner_list_tpl.html"
            }
        },
        isaNoMouse : {
            name : 'isaNoMouse'
        },
        isaPassengerPicker : {
            name : 'isaPassengerPicker',
            templates : {
                default:"module/passenger-picker/view/passenger-picker-view.html",
                multiCity:"module/passenger-picker/view/passenger-picker-multi-city-view.html"
            }
        },
        isaProgress : {
            name : 'isaProgress',
            templates : {
                default:"module/progress/view/progress-view.html",                
            }
        },
        clickOutside : {
            name : 'clickOutside'
        },
        typeaheadFocus : {
            name : 'typeaheadFocus'
        },
        isaValidator : {
            name : 'isaValidator'
        },
        isaValidatorType : {
            name : 'isaValidatorType'
        },
        isaPhoneNumber : {
            name: 'isaPhoneNumber',
            templates : {
                default:"module/phone-number/view/phone-number-view.html"
            }
        },
        isaBackButton : {
            name: 'isaBackButton',
            templates: {
                default:"module/back_button/view/back_button_view.html"
            }
        },
        isaJqDateUtc : {
            name: 'jqDateUtc'
        },
        isacolorpicker : {
            name: 'isacolorpicker',
            templates: {
                default:"module/color_picker/view/isa_color_picker_view.html"
            }
        },
        isaFileUploader :{
            name: 'isaFileUploader'

        },
        'isaTranslate': {
            name:'isaTranslate'
        },
        'airportTransferList': {
            name:'airportTransferList',
            templates: {
                default:"module/airport_transfer/view/airport_transfer_list.html"
            }
        },
        'admDatePicker':{
            name:'admDatePicker'
        }


    };

    isamodules.getModuleNames = function(){
        var modNamess = [];
        for(var key in data) {
            modNamess.push(key);
        }
        return modNamess;

    };
    isamodules.getTemplates = function(name){
        return data[name].templates;
    };
    return isamodules;

};
