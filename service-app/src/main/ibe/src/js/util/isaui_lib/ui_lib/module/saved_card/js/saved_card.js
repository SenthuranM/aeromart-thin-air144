'use strict';

(function() {
	var isa_saved_card = angular.module("isaSavedCard", []);
	var directives = {};
	var isaConfig = new IsaModuleConfig();

	directives.isaSavedCard = [
			"$compile",
			"$http",
			"$templateCache",
			function($compile, $http, $templateCache) {
				var directive = {};

				directive.restrict = 'EA';
				directive.scope = {
					paymentCardObject : "=",
					deleteCard : "&",
					addNewCard : "&"
				};
				directive.link = function(scope, element) {

					var template = $templateCache.get(isaConfig.getTemplates("isaSavedCard")['default']);
					template = angular.element(template);
					element.append(template);
					$compile(template)(scope);
					

				};
				directive.controller = ["$scope","$element",function($scope, $element) {
					
				}];

				return directive;
			} ];
	isa_saved_card.directive(directives);
})();