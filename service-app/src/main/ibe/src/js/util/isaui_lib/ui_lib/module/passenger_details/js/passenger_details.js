'use strict';

(function () {
    var isa_passenger_details = angular.module("isaPassengerDetails", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaPassengerDetails = [
        '$compile',
        '$http',
        '$state',
        '$templateCache',
        'constantFactory',
        'modifyReservationService',
        'languageService',
        'currencyService',
        'applicationService',
        'commonLocalStorageService',
        '$translate',
        'dateTransformationService',
        '$sce',
        'formValidatorService',
        function ($compile, $http, $state, $templateCache, constantFactory, modifyReservationService, languageService,
                  currencyService, applicationService, commonLocalStorageService, $translate,dateTransformationService,$sce) {
            var directive = {};

            directive.restrict = 'AEC';

            directive.scope = {
                applicationMode: '@',
                passenger: '=',
                selectedPassenger: '=',
                passengerCount: '@',
                passengerList: '=',
                updateContact: '&',
                passengerCompleted: '=',
                onModificationCompleted: '&',
                paxDetailConfig: '=',
                firstPaxContact: '=',
                submitted: '=',
                airewardsChecked:'=',
                updateAirewardsStatus : '&',
                onModificationStart : '&',
                countryList : '=',
                isAirwordEnabled : '=',
                validationMessages: '=',
                carrier:'=',
                language: '='
            };

            directive.link = function (scope, element) {
                //Setting passenger detail template
                var template = $templateCache.get(isaconfig.getTemplates("isaPassengerDetails")['default']);

                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
            };
            directive.controller = [
                '$scope', '$element', '$rootScope', 'datePickerService', '$location', '$anchorScroll', '$timeout', '$filter','$translate','formValidatorService',
                function ($scope, $element, $rootScope, datePickerService, $location, $anchorScroll, $timeout, $filter,$translate, formValidatorService) {
                    var PAX_TYPE = {
                        'Adult': 'AD',
                        'Child': 'CH',
                        'Infant': 'IN'
                    };

                    var dateFormat = "DD/MM/YYYY";
                    //dto
                    $scope.ageRules = {
                        Adult: {
                            min: 12,
                            max: 99
                        },
                        Child: {
                            min: 2,
                            max: 12
                        },
                        Infant: {
                            min: 0,
                            max: 2
                        }
                    };

                    $scope.requiredRules = {};
                    //GET FORM VALIDATIONS
                    $scope.paxValidation = formValidatorService.getValidations();
                    
                    //variables
                    $scope.dobInput = '';
                    $scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                    $scope.isLargeDevice = $rootScope.deviceType.isLargeDevice;
                    $scope.appMode =  applicationService.getApplicationMode();
                    $scope.isNameChange = ($scope.applicationMode === 'nameChange');
                    $scope.nationalityList = constantFactory.Nationalities();
                    $scope.countryList = constantFactory.Countries();
                    $scope.isPersianCalendarType = applicationService.getIsPersianCalendar();

                    $scope.nationalityNameList = _.map($scope.nationalityList, function(nationality){
                        return nationality.name;
                    });
                    $scope.countryNameList = _.map($scope.countryList, function(country){
                        return country.name;
                    });

                    $scope.mod = {};
                    $scope.lastPassenger = false;
                    $scope.salutationList = [];

                    $scope.minDob = new Date();
                    $scope.maxDob = new Date();
                    $scope.dataTip = "";
                    
                    init();
            
                    function init() {                
                        $translate.onReady().then(function () {
                            // $scope.dataTip =$translate.instant('msg_passport_format');
                        //$sce.dataTip = $sce.trustAsHtml(value);
                        $scope.dataTip = $sce.trustAsHtml($translate.instant('msg_passport_format'));
                        
                        });
                        
                    }            
                        


                    //iife
                    (function (){
                       // console.log($scope.isAirwordEnabled);
                       
                        var flightData = commonLocalStorageService.getConfirmedFlightData();
                        if(flightData !== null){
                        var firstDepartureDate = flightData[0].flight.departureDate;
                        var lastArrivalDate = flightData[Object.keys(flightData).length - 1].flight.arrivalDate;     
                        }
                       
                        //Do the changes required changes to paxValidation
                        if($scope.isNameChange){
                            $scope.paxValidation.Adult.nationality.required = {};
                            $scope.paxValidation.Child.nationality.required = {};
                            $scope.paxValidation.Infant.nationality.required = {};
                        }

                        var ageRulesForPax = $scope.ageRules[$scope.passenger.category];
                        var lastArrivalDateIso8601 = moment(lastArrivalDate, dateFormat).format(dateFormat);
                        $scope.maxDate = moment(lastArrivalDateIso8601, dateFormat).subtract(ageRulesForPax.min, 'years');

                        // Min date is reduced by one day
                        $scope.minDate = moment(lastArrivalDateIso8601, dateFormat).subtract(ageRulesForPax.max, 'years');
                        $scope.maxDate = $scope.maxDate.subtract(1, 'days');

                        if($scope.passenger.category === "Infant"){
                        	 var firstDepartureDateIso8601 = moment(firstDepartureDate, dateFormat).format(dateFormat);
                            $scope.maxDate = moment(firstDepartureDateIso8601, dateFormat).subtract(3, 'days');

                            // Max dob of infant must always be on or before the day that the booking is done
                            if(moment() < $scope.maxDate){
                                $scope.maxDate = moment();
                            }
                        }
                        
                        $scope.minDate = $scope.minDate.toDate();
                        $scope.maxDate = $scope.maxDate.toDate();
                        $scope.minExpiry = moment().toDate();
                        $scope.maxExpiry = moment().add(10, 'years').toDate();
                        $scope.minVisaIssue = moment().subtract(5,'years').toDate();
                        $scope.maxVisaIssue = moment().add(5,'years').toDate();
                        var calenderType="gregorian";
                        $scope.minDateForCal="";
                        $scope.maxDateForCal="";
                        
                        $scope.minDateForExpiry="";
                        $scope.maxDateForExpiry="";
                        
                        $scope.minDateForVissaIssue="";
                        $scope.maxDateForVissaIssue="";
                        
                    	var defautlValueForDob=$scope.maxDate.getDate()+"/"+$scope.maxDate.getMonth()+"/"+$scope.maxDate.getFullYear();
                    	
                    	var defautlValueForExpiry=$scope.maxExpiry.getDate()+"/"+$scope.maxExpiry.getMonth()+"/"+$scope.maxExpiry.getFullYear();
                    	
                    	var defautlValueForVissaIssue= $scope.maxVisaIssue.getDate()+"/"+$scope.maxVisaIssue.getMonth()+"/"+$scope.maxVisaIssue.getFullYear();
                    	
//                    	if(languageService.getSelectedLanguage() == 'fa'){
                        if($scope.isPersianCalendarType){
                          calenderType="jalali";
                        	var jalaaliObjectForMaxDate = getJalaaliDate($scope.maxDate.getFullYear(),$scope.maxDate.getMonth(),$scope.maxDate.getDate());
                        	var jalaaliObjectForMinDate = getJalaaliDate($scope.minDate.getFullYear(),$scope.minDate.getMonth(),$scope.minDate.getDate());
                        	
                        	var jalaaliObjectForMaxExpiry =getJalaaliDate($scope.maxExpiry.getFullYear(),$scope.maxExpiry.getMonth(),$scope.maxExpiry.getDate());
                        	var jalaaliObjectForMinExpiry = getJalaaliDate($scope.minExpiry.getFullYear(),$scope.minExpiry.getMonth(),$scope.minExpiry.getDate());
                        	
                        	var jalaaliObjectForMaxVisaIssue =getJalaaliDate($scope.maxVisaIssue.getFullYear(),$scope.maxVisaIssue.getMonth(),$scope.maxVisaIssue.getDate());
                        	var jalaaliObjectForMinVisaIssue = getJalaaliDate($scope.minVisaIssue.getFullYear(),$scope.minVisaIssue.getMonth(),$scope.minVisaIssue.getDate());
                        	
                        	
                        	$scope.maxDateForCal = "\'"+jalaaliObjectForMaxDate.jd+"/"+jalaaliObjectForMaxDate.jm+"/"+jalaaliObjectForMaxDate.jy+"\'";
                        	$scope.minDateForCal= "\'"+jalaaliObjectForMinDate.jd+"/"+jalaaliObjectForMinDate.jm+"/"+jalaaliObjectForMinDate.jy+"\'";
                        	
                        	$scope.maxDateForExpiry ="\'"+jalaaliObjectForMaxExpiry.jd+"/"+jalaaliObjectForMaxExpiry.jm+"/"+jalaaliObjectForMaxExpiry.jy+"\'"; 
                        	$scope.minDateForExpiry= "\'"+jalaaliObjectForMinExpiry.jd+"/"+jalaaliObjectForMinExpiry.jm+"/"+jalaaliObjectForMinExpiry.jy+"\'";
                        	
                        	$scope.maxDateForVissaIssue ="\'"+jalaaliObjectForMaxVisaIssue.jd+"/"+jalaaliObjectForMaxVisaIssue.jm+"/"+jalaaliObjectForMaxVisaIssue.jy+"\'"; 
                        	$scope.minDateForVissaIssue= "\'"+jalaaliObjectForMinVisaIssue.jd+"/"+jalaaliObjectForMinVisaIssue.jm+"/"+jalaaliObjectForMinVisaIssue.jy+"\'";
                        	
                        	
                        	defautlValueForDob = jalaaliObjectForMaxDate.jd+"/"+jalaaliObjectForMaxDate.jm+"/"+jalaaliObjectForMaxDate.jy;
                        	
                        	defautlValueForExpiry = jalaaliObjectForMaxExpiry.jd+"/"+jalaaliObjectForMaxExpiry.jm+"/"+jalaaliObjectForMaxExpiry.jy;
                        	
                        	defautlValueForVissaIssue = jalaaliObjectForMaxVisaIssue.jd+"/"+jalaaliObjectForMaxVisaIssue.jm+"/"+jalaaliObjectForMaxVisaIssue.jy;
                        	
                        }else{ 	
                        	console.log($scope.maxDate.getDate());
                        	$scope.maxDateForCal = "\'"+$scope.maxDate.getDate()+"/"+$scope.maxDate.getMonth()+"/"+$scope.maxDate.getFullYear()+"\'";
                        	$scope.minDateForCal= "\'"+$scope.minDate.getDate()+"/"+$scope.minDate.getMonth()+"/"+$scope.minDate.getFullYear()+"\'";
                        	
                        	$scope.maxDateForExpiry = "\'"+$scope.maxExpiry.getDate()+"/"+$scope.maxExpiry.getMonth()+"/"+$scope.maxExpiry.getFullYear()+"\'";
                        	$scope.minDateForExpiry= "\'"+$scope.minExpiry.getDate()+"/"+$scope.minExpiry.getMonth()+"/"+$scope.minExpiry.getFullYear()+"\'";
                        }

                        $scope.salutationList = constantFactory.PaxTypeWiseTitle()[ PAX_TYPE[$scope.passenger.category] ];

                        $scope.passportExpiry = {
                            changeYear: true,
                            changeMonth: true,
                            minDate:  $scope.minExpiry,
                            maxDate: $scope.maxExpiry,
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true

                        }
                        $scope.visaIssue = {
                            changeYear: true,
                            changeMonth: true,
                            minDate:  $scope.minVisaIssue,
                            maxDate: $scope.maxVisaIssue,
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true

                        }

                        $scope.dateOptions = {
                            changeYear: true,
                            changeMonth: true,
                            minDate: $scope.minDate,
                            maxDate: $scope.maxDate,
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            yearRange: "-100:+0",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true,
                            isRTL: $scope.isRtl
                        };
                        
                        $scope.admOptions = {
                        		calType: calenderType,
                        		dtpType:"date",
                        		zIndex:9,
                                autoClose:true,
                                format: "DD/MM/YYYY",
                                multiple:false,
                                default:defautlValueForDob
                        		
                            };
                        
                        $scope.passportExpiry1 = {
                        		calType: calenderType,
                        		dtpType:"date",
                        		zIndex:9,
                                autoClose:true,
                                format: "DD/MM/YYYY",
                                multiple:false,
                                default:defautlValueForExpiry
                        		
                            };
                        
                        $scope.visaIssue1 = {
                        		calType: calenderType,
                        		dtpType:"date",
                        		zIndex:9,
                                autoClose:true,
                                format: "DD/MM/YYYY",
                                multiple:false,
                                default:defautlValueForVissaIssue
                        		
                            };
                    }());
                    $scope.formatLabelCountry = function(model) {
                        var tempN = $scope.countryList;
                        if (tempN) {
                            for (var i = 0; i < tempN.length; i++) {
                                if (model === tempN[i].code) {
                                    return tempN[i].name;
                                    break;
                                }
                            }
                        }
                    }

                    //functions
                    $scope.onAirwardsEmailFocus = function(index){
                        $scope.onModificationStart({data:{index:index,valFieald:'isLMSValidationDone'}});
                    }
                    function stringOrEmpty(entity) {
                        return entity || "";
                    }
                    $scope.onairewardsEmailChange = function(index){
                        $scope.onModificationStart({data:{index:index,valFieald:'isLMSValidationDone'}});
                    }
                    $scope.datePickerOnClick = function () {
                        $scope.mod.dobOpen = true;
                    };

                    $scope.nextPassenger = function (passenger, paxForm) {
                        paxForm.$setSubmitted(true);
                        if (passenger.valid) {
                            $scope.selectedPassenger = $scope.selectedPassenger + 1;
                        }
                    };

                    $scope.setSalutation = function (salutation) {
                        $scope.passenger.salutation = salutation.titleCode;
                    };

                    $scope.setNationality = function (nationality) {
                        $scope.passenger.nationality = nationality;
                    };

                    $scope.formatLabelCitizen = function (model) {
                        if ($scope.nationalityList) {
                            for (var i = 0; i < $scope.nationalityList.length; i++) {
                                if (model === $scope.nationalityList[i].code) {
                                    return $scope.nationalityList[i].name;
                                }
                            }
                        }
                    };
					function getJalaaliDate(year,month,date){
						return dateTransformationService.toJalaali(year,month,date);
					}
                    $scope.formatLabelNationality = function (model) {
                        var tempN = this.nationalityList;
                        if (tempN) {
                            for (var i = 0; i < tempN.length; i++) {
                                if (model == tempN[i].code) {
                                    return tempN[i].name;
                                }
                            }
                        }
                    };

                    $scope.setTravelDocument = function(type){
                        $scope.passenger.travelDocumentType = type;
                    }

                    $scope.addInfantToParent = function(parentSequence, infantSequence) {
                        for (var i = 0; i < $scope.passengerList.length; i++) {
                            if ($scope.passengerList[i].paxSequence === parentSequence) {
                                $scope.passengerList[i].infantSequence = infantSequence;
                            }
                        }
                    };

                    $scope.removeInfantFromParent = function(parentSequence){
                        for (var i = 0; i < $scope.passengerList.length; i++) {
                            if ($scope.passengerList[i].paxSequence === parentSequence) {
                                $scope.passengerList[i].infantSequence = null;
                            }
                        }
                    };

                    $scope.setTravelingWith = function (travelingWith) {
                        //If traveling with was already set, we should remove the binding form the parent

//                        console.log($scope.passenger.travelingWith);
                        if(!_.isUndefined($scope.passenger.travelingWith)){
                            var currentParent = $scope.passenger.travelingWith;
                            $scope.removeInfantFromParent(currentParent);
                        }

                        $scope.passenger.travelingWith = travelingWith;
                        $scope.addInfantToParent(travelingWith, $scope.passenger.paxSequence);
                    };

                    $scope.scrollToContactDetails = function (passenger, paxForm) {
                        paxForm.$setSubmitted(true);
                        if (passenger.valid) {
                            $scope.passengerCompleted = true;

                            /**
                             * If user is in modification flow(Name Change)
                             * Prevent navigate to contact info page
                             */
                            if ($scope.appMode === 'nameChange') {
                                $scope.onModificationCompleted();
                            } else {
                                $timeout(function () {
                                    var element = $(".waypoint_paxContact");
                                    $("html, body").animate({
                                        scrollTop: element.offset().top - 94
                                    }, 600);
                                }, 10);
                            }
                        }
                    };
                    $scope.scrollToNationality = function () { 
                        if ($scope.isMobileDevice) {
                            // var element = $('#nationality');
                            // $("html, body").animate({
                            //     scrollTop: element.offset().top - 94
                            // }, 600);
                        } else {
                            //do nothing
                        }                        
                    };


                    $scope.openDate = function(){
                        var dob = angular.element("#pax-dob");

                        if(dob.is(':focus')){
                            dob.trigger('blur');
                        } else {
                            dob.trigger('focus');
                        }

                    };

                    $scope.backToReservationModication = function () {

                        // Get modify search criteria.
                        var modifySearchParams = modifyReservationService.getReservationSearchCriteria();

                        // Change state to modify reservation.
                        $state.go('modifyReservation', {
                            lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode(),
                            pnr: modifySearchParams.pnr,
                            lastName: modifySearchParams.lastName,
                            departureDate: modifySearchParams.departureDate
                        });
                    };

                    $scope.startsWith = function (state, viewValue) {
                        if(viewValue === " " || viewValue ===  null){
                            return true;
                        }else{
                            return state.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
                        }
                    };

                    $scope.getPassengerName = function(paxIndex){
                        var passenger = _.find($scope.passengerList,
                            function(pax) {
                                return pax.paxSequence == paxIndex;
                            }
                        );
                        if(!_.isUndefined(passenger) && !_.isUndefined(passenger.displayName)){
                            return passenger.displayName;
                        }else{
                            return 'Traveling With';
                        }
                    };

                    //Setting lastPassenger to true if selected index is equal to passengerCount
                    $scope.$watch('selectedPassenger', function (value) {
                        $scope.lastPassenger = $scope.selectedPassenger === $scope.passengerCount - 1;
                    });

                    //Setting the display name based on the current model value changes
                    $scope.$watch('passenger.firstName + passenger.lastName + passenger.salutation', function () {
                        if ($scope.passenger.salutation || $scope.passenger.firstName || $scope.passenger.lastName) {

                            var salutation =  $filter('listFilter')($scope.passenger.salutation,
                                $scope.salutationList, "titleCode", "titleName");

                            $scope.passenger.displayName = stringOrEmpty(salutation) + " "
                                + stringOrEmpty($scope.passenger.firstName) + " "
                                + stringOrEmpty($scope.passenger.lastName);
                        } else {
                            $scope.passenger.displayName = $scope.passenger.category;

                            $translate.onReady(function(){
                                var label = $translate.instant('lbl_common_' + $scope.passenger.category.toLowerCase());

//                                if(label != 'lbl_passenger_' + $scope.passenger.category.toLowerCase()){
//                                    label = $scope.passenger.category;
//                                }

                                var categoryPaxCount = {};

                                angular.forEach($scope.passengerList, function(passenger, key){
                                    if(_.isUndefined(categoryPaxCount[$scope.passengerList[key].category])){
                                        categoryPaxCount[passenger.category] = 1;
                                    }else{
                                        categoryPaxCount[passenger.category] = categoryPaxCount[passenger.category] + 1;
                                    }

                                    if($scope.passenger.paxSequence === passenger.paxSequence){
                                        $scope.passenger.displayName = label
                                            + " " + (categoryPaxCount[passenger.category] || "");
                                    }
                                })
                            });

                        }
                    });

                    //Setting the display name based on the current model value changes
                    $scope.$watch('passenger.submitted', function () {
                        if($scope.passenger.submitted){
                            $scope['userForm_' + $scope.selectedPassenger].$setSubmitted(true);
                        }
                    });

                    //Checks validity of the specific passenger of form change.
                    $scope.$watch('userForm_' + $scope.selectedPassenger + '.$valid', function (validity) {
                        $scope.passenger.valid = validity;
                    });

                    //If the user starts filling a form they should no longer be allowed to switch between
                    $scope.$watch('userForm_' + $scope.selectedPassenger + '.$dirty', function (dirty) {
                        $scope.passenger.dirty = dirty;
                    });

                    //Watch for changes in the first passenger and update the values in contact form accordingly
                    $scope.$watchCollection('passengerList[0]', function (newVal, oldVal) {
                        //Only call the function if the values have changed
                        if (newVal !== oldVal && $scope.firstPaxContact) {
                            var changedPassenger = {};

                            if (oldVal.firstName !== newVal.firstName) {
                                changedPassenger.firstName = newVal.firstName;
                            }
                            if (oldVal.lastName !== newVal.lastName) {
                                changedPassenger.lastName = newVal.lastName;
                            }
                            if (oldVal.salutation !== newVal.salutation) {
                                changedPassenger.salutation = newVal.salutation;
                            }
                            if (oldVal.nationality !== newVal.nationality) {
                                changedPassenger.nationality = newVal.nationality;
                            }
                            if (oldVal.dob !== newVal.dob) {
                                changedPassenger.dob = newVal.dob;
                            }
                            $scope.updateContact({passenger: changedPassenger});
                        }
                    });
                    $scope.updateReward = function(index){
                        $scope.updateAirewardsStatus({index:index.index});
                      //  $scope.airewardsChecked = !$scope.airewardsChecked;

                    }

                }];
            return directive;
        }];
    isa_passenger_details.directive(directives);
})();
// $scope.paxValidation = {
//     Adult: {
//         firstName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_firstNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
//                 value: "2",
//                 message: "lbl_validation_firstNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': "lbl_validation_firstNameSymbol"
//             }
//         },
//         lastName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_lastNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "2",
//                 message: "lbl_validation_lastNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': "lbl_validation_lastNameSymbol"
//             }
//         },
//         dob: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.dob.mandatory[passenger.type]",
//                 message: "lbl_validation_dobRequired"
//             }
//         },
//         nationality: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationality.mandatory[passenger.type]",
//                 message: "lbl_validation_nationalityRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{nationalityNameList}}",
//                 message: "lbl_validation_nationalityInList"
//             }
//         },
//         'nationalIDNo':{
//             'required': {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationalIDNo.mandatory[passenger.type]",
//                 message: "lbl_validation_national_Id_Number_Required"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_national_Id_Number_Pattern'
//             }

//         },
//         'salutation': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: 'lbl_validation_titleRequired'
//             }
//         },
//         'airewardsEmail': {
//             'pattern':{
//                 name: 'pattern',
//                 value: '^[a-z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$',
//                 message: "lbl_validation_airewardsEmail"
//             }
//         },
//         'passportNo': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
//                 message: "lbl_validation_passportRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "7",
//                 message: "lbl_validation_passportNumMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_passportPattern'
//             }
//         },
//         'passportExpiry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportExpiry.mandatory[passenger.type]",
//                 message: "{{::(carrier == 'W5')?validationMessages.passportExpiry + ' for Adult ' + ' # ' + passenger.paxSequence : validationMessages.passportExpiry}}"

//             }
//         },
//         'passportIssuedCntry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportIssuedCntry.mandatory[passenger.type]",
//                 message: "lbl_validation_countryRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{countryNameList}}",
//                 message: "lbl_validation_countryInList"
//             }
//         }

//     },
//     Child: {
//         firstName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_firstNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
//                 value: "2",
//                 message: "lbl_validation_firstNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_firstNameSymbol'
//             }
//         },
//         lastName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_lastNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "2",
//                 message: "lbl_validation_lastNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_lastNameSymbol'
//             }
//         },
//         dob: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.dob.mandatory[passenger.type]",
//                 message: "lbl_validation_dobRequired"
//             }
//         },
//         nationality: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationality.mandatory[passenger.type]",
//                 message: "lbl_validation_nationalityRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{nationalityNameList}}",
//                 message: "lbl_validation_nationalityInList"
//             }
//         },
//         'nationalIDNo':{
//             'required': {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationalIDNo.mandatory[passenger.type]",
//                 message: "lbl_validation_national_Id_Number_Required"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_national_Id_Number_Pattern'
//             }
//         },
//         'salutation': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: 'lbl_validation_titleRequired'
//             }
//         },
//         'passportNo': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
//                 message: "lbl_validation_passportRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "7",
//                 message: "lbl_validation_passportNumMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_passportPattern'
//             }
//         },
//         'airewardsEmail': {
//             'pattern':{
//                 name: 'pattern',
//                 value: '^[a-z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$',
//                 message: 'lbl_validation_airewardsEmail'
//             }
//         },
//         'passportExpiry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportExpiry.mandatory[passenger.type]",
//                 message: "{{::(carrier == 'W5')?validationMessages.passportExpiry + ' for Child ' + ' # ' + passenger.paxSequence : validationMessages.passportExpiry}}"

//             }
//         }

//     },
//     Infant: {
//         'travelWith': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: 'lbl_validation_travelingWith'
//             }
//         },
//         firstName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_firstNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
//                 value: "2",
//                 message: "lbl_validation_firstNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_firstNameSymbol'
//             }
//         },
//         lastName: {
//             required: {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_lastNameRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "2",
//                 message: "lbl_validation_lastNameMinChar"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z ]+$/",
//                 'message': 'lbl_validation_lastNameSymbol'
//             }
//         },
//         dob: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.dob.mandatory[passenger.type]",
//                 message: "lbl_validation_dobRequired"
//             }
//         },
//         nationality: {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationality.mandatory[passenger.type]",
//                 message: "lbl_validation_nationalityRequired"
//             },
//             'itemInList': {
//                 name: "item-in-list",
//                 value: "{{nationalityNameList}}",
//                 message: "lbl_validation_nationalityInList"
//             }
//         },
//         'salutation': {
//             'required': {
//                 name: "required",
//                 value: "required",
//                 message: "lbl_validation_titleRequired"
//             }
//         },
//         'passportNo': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
//                 message: "lbl_validation_passportRequired"
//             },
//             minlength: {
//                 name: "ng-minlength",
//                 value: "7",
//                 message: "lbl_validation_passportNumMinChar"
//             }
//         },
//         'passportExpiry': {
//             required: {
//                 name: "ng-required",
//                 value: "paxDetailConfig.passportExpiry.mandatory[passenger.type]",
//                 message: "{{::(carrier == 'W5')?validationMessages.passportExpiry + ' for Infant ' + ' # ' + passenger.paxSequence : validationMessages.passportExpiry}}"

//             }
//         },
//         'nationalIDNo':{
//             'required': {
//                 name: "ng-required",
//                 value: "paxDetailConfig.nationalIDNo.mandatory[passenger.type]",
//                 message: "lbl_validation_national_Id_Number_Required"
//             },
//             'pattern': {
//                 'name': "ng-pattern",
//                 'value': "/^[A-Za-z0-9]+$/",
//                 'message': 'lbl_validation_national_Id_Number_Pattern'
//             }

//         }

//     }
// };
