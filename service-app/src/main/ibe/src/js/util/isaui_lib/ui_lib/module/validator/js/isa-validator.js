'use strict';

(function () {
    var isa_validator = angular.module("isaValidator", []);
    var directives = {};

    directives.isaValidator = ['$rootScope','$compile', '$translate', function ($rootScope, $compile, $translate) {
        return {
            restrict: 'A',
            replace: false,
            terminal: true,
            priority: 1000, //this setting is important to make sure it executes before other directives
            require: '?^isaValidatorType', //inject validator-type form as the forth parameter to the link function

            link: function (scope, element, attrs, isaValidatorType) {
                var type;
                var rules;
                var formName;
                var validationEnabled = true;
                var isMobileDevice = $rootScope.deviceType.isMobileDevice;
                var inputElement = element.find("input");
                if(scope[element.attr("isa-validation-disabled")] || element.attr("isa-validation-disabled") === 'true'){
                    validationEnabled = false;
                }


                var inputName;
                if (!_.isUndefined(inputElement[0])) {
                    inputName = inputElement[0].getAttribute('name');
                } else {
                    for (var i = 0; i < element[0].children.length; i++) {
                        if (element[0].children[i].hasAttribute('ng-model')) {
                            inputElement = angular.element(element[0].children[i]);
                            inputName = element[0].children[i].getAttribute('name');
                        }
                    }
                }

                if (isaValidatorType) {
                    rules = JSON.parse(isaValidatorType.getRules());
                    type = isaValidatorType.getType();
                    formName = isaValidatorType.getName();

                    if(!_.isUndefined(rules[type])){
                        if (rules[type][inputName]) {
                            rules = rules[type][inputName];
                        }
                    }

                } else {
                    console.warn("Provide isa-validator-type in order to activate validation");
                }

                inputElement.bind('blur', function(evt){
                    $(evt.currentTarget).removeClass('on-focus');
                    angular.element('.'+inputName).css("display","");
                })

                inputElement.bind('focus', function(evt){
                    $(evt.currentTarget).addClass('on-focus');
                    angular.element('.'+inputName).css("display","none");
                  
                })

                if(validationEnabled){
                    var inputPath = formName + '.' + inputName;
                    var ngMessage = '<div class= "'+inputName+' " ng-messages="' + inputPath + '.$error" ng-show="' + formName + '.$submitted || ' + inputPath + '.$touched">';

                    angular.forEach(rules, function (rule, key) {
                        inputElement.attr(rule.name, rule.value);
                        if(inputElement.parent().attr('isa-validatin-on') == 'focus') {
                            inputElement.bind('blur', function(evt){
                                // console.log(inputElement)
                                // angular.element(evt.currentTarget).removeClass('ng-invalid');
                                // angular.element(evt.currentTarget).removeClass('ng-touched');
                                // var validationMessage = $translate.instant(rule.message);
                                // var focusMessage = '<p class="valdr-message on-focus" ng-message=' + key + '>' + validationMessage + '</p>';

                                // inputElement.next().append(focusMessage);
                                // inputElement.next().attr({'ng-show': 'true', 'class': 'ng-active'});
                            })

                        }
                        ngMessage = ngMessage + '<p class="valdr-message" ng-message=' + key + '> <span translate="' + rule.message + '">' + rule.message + '</span></p>';
                    });

                    ngMessage = ngMessage + '</div>';
                    element.append($compile(ngMessage)(scope));

                }

                element.removeAttr("isa-validator"); //remove the attribute to avoid indefinite loop
                element.removeAttr("data-isa-validator"); //also remove the same attribute with data- prefix in case users specify data-common-things in the html

                $compile(element)(scope);
            }
        };
    }];

    directives.ageValidation = ['$timeout', function ($timeout) {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                ctrl.$validators.ageValidation = function (modelValue, viewValue) {

                    //Get the age of the given passenger
                    var givenDate = moment(modelValue);
                    var todayDate = moment();
                    var diffYears = todayDate.diff(givenDate, 'years');

                    var ageLimit;

                    try{
                        //Parse and santize the age limits provided
                        ageLimit = JSON.parse(attrs['ageValidation']);
                    }catch(ex){
                        //Set default values to prevent errors
                        console.warn("min, max age limits not set for the validator set to default");
                        ageLimit = {min: 0, max: 100};
                    }

                    if(diffYears >= ageLimit.max || diffYears < ageLimit.min){
                        return false;
                    }else{
                        return true;
                    }
                };
            }
        };
    }];

    directives.itemInList = ['$timeout', function ($timeout) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$validators.itemInList = function (modelValue, viewValue) {
                    var itemList;

                    try {
                        itemList = JSON.parse(attrs['itemInList']);
                    } catch (ex) {
                        itemList = [];
                    }

                    if (_.contains(itemList, viewValue)) {
                        return true;
                    } else {
                        //Checks for space(" ") to compensate for uib typeahead directive adding space on click
                        return !!(_.isUndefined(modelValue) || _.isEmpty(modelValue) || modelValue === " ");
                    }
                };
            }
        };
    }];

    /** Makes fields required based on another fields filled state
     *
    * @param dependentFilled if this field is filled, the other field is set to required
    * */
    directives.dependentFilled = function() {
        return {
            require: 'ngModel',
            link: function(scope, $element, $attrs, ctrl) {
                ctrl.$validators.dependentFilled = function(modelValue) {

                    var dependentFilled = _.isEmpty($attrs.dependentFilled);

                    if(dependentFilled && !_.isEmpty(modelValue)){
                        return false;
                    }else{
                        return true;
                    }
                };

                $attrs.$observe('dependentFilled', function (){
                    ctrl.$validate();
                });
            }
        }
    };

    /** Check if two fields are equal
     *
     * @param compareField field which is checked against the model
     * */
    directives.compareField = function() {
        return {
            require: 'ngModel',
            link: function(scope, $element, $attrs, ctrl) {
                ctrl.$validators.compareField = function(modelValue) {
                    return $attrs.compareField === modelValue;
                };

                $attrs.$observe('compareField', function (){
                    ctrl.$validate();
                });
            }
        }
    };


    isa_validator.directive(directives);
})();