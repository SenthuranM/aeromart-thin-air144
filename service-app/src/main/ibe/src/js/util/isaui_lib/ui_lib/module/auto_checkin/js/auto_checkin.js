'use strict';

(function () {
    var isa_auto_checkin = angular.module("isaAutoCheckin", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();
    directives.isaAutoCheckin = [
        '$compile',
        '$http',
        '$state',
        '$templateCache',
        'constantFactory',
        'modifyReservationService',
        'languageService',
        'currencyService',
        'applicationService',
        'commonLocalStorageService',
        '$translate',
        'dateTransformationService',
        '$sce',
        'formValidatorService',
        function ($compile, $http, $state, $templateCache, constantFactory, modifyReservationService, languageService,
                  currencyService, applicationService, commonLocalStorageService, $translate,dateTransformationService,$sce,formValidatorService) {
            var directive = {};

            directive.restrict = 'E';

            directive.scope = {
                applicationMode: '@',
                passenger: '=',
                selectedPassenger: '=',               
                passengerList: '=',                
                updateContact: '&'               
            };

            directive.link = function (scope, element) {
                //Setting passenger detail template
                var template = $templateCache.get(isaconfig.getTemplates("isaAutoCheckin")['default']);

                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
            };
            
            directive.controller = [
                '$scope', '$element', '$rootScope', 'datePickerService', '$location', '$anchorScroll', '$timeout', '$filter','$translate','formValidatorService',
                function ($scope, $element, $rootScope, datePickerService, $location, $anchorScroll, $timeout, $filter,$translate, formValidatorService) {
                                 

                    $scope.requiredRules = {};
                    //GET FORM VALIDATIONS
                    $scope.paxValidation = formValidatorService.getValidations();
                    
                    //variables
                    $scope.dobInput = '';
                    $scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                    $scope.isLargeDevice = $rootScope.deviceType.isLargeDevice;
                    $scope.appMode =  applicationService.getApplicationMode();
                    $scope.isNameChange = ($scope.applicationMode === 'nameChange');
                    $scope.nationalityList = constantFactory.Nationalities();
                    $scope.countryList = constantFactory.Countries();
                    $scope.isPersianCalendarType = applicationService.getIsPersianCalendar();
                    $scope.passengerIndex = 0;
                    $scope.dob = $scope.passengerList[0].dob;
                    
                    $scope.changeSelected = function ($index){                     	
                    	$scope.passengerIndex = $index;                  
                    	$scope.passportNo = $scope.passengerList[$index].passportNo;
                    	$scope.passportExpiry = $scope.passengerList[$index].passportExpiry;
                    	$scope.passportIssuedCntry = $scope.passengerList[$index].passportIssuedCntry;
                    	$scope.dob = $scope.passengerList[$index].dob;
                    	$scope.email = $scope.passengerList[$index].email;
                    }
                    
                                    
                    $scope.passengerDetailsFilled = []; 
                    
                    $scope.nextPassenger = function(){                    	
                    	$scope.nextIndex = $scope.passengerIndex+1;                    	
                    	if($scope.nextIndex > $scope.passengerList.length-1){
                    		$scope.passengerIndex = 0;
                    		$scope.nextIndex = 0;
                    	}                    	
                    	$scope.changeSelected($scope.nextIndex);
                    	
                    };
                    	
                    $scope.changeSelectedUpdateData = function (){   
                    	if($scope.passportNo && $scope.passportExpiry && $scope.passportIssuedCntry && $scope.dob && $scope.email){                    		
                    		$scope.passengerList[$scope.passengerIndex].passportNo  = $scope.passportNo;                    		
                        	$scope.passengerList[$scope.passengerIndex].passportExpiry = $scope.passportExpiry;
                        	$scope.passengerList[$scope.passengerIndex].passportIssuedCntry = $scope.passportIssuedCntry;
                        	$scope.passengerList[$scope.passengerIndex].dob = $scope.dob;
                        	$scope.passengerList[$scope.passengerIndex].email = $scope.email;
                        	$scope.passengerDetailsFilled[$scope.passengerIndex] = true;
                    	}
                    	else{
                    		$scope.passengerDetailsFilled[$scope.passengerIndex] = false;
                    	}
                    	
                    }

                    $scope.nationalityNameList = _.map($scope.nationalityList, function(nationality){
                        return nationality.name;
                    });
                    $scope.countryNameList = _.map($scope.countryList, function(country){
                        return country.name;
                    });

                    $scope.mod = {};
                    $scope.lastPassenger = false;
                    $scope.salutationList = [];

                    $scope.minDob = new Date();
                    $scope.maxDob = new Date();
                    $scope.dataTip = "";                   
                             
                    $scope.formatLabelCountry = formatLabelCountry;
                    $scope.startsWith = startsWith;
                    
                    $scope.dateOptions = {
                            changeYear: true,
                            changeMonth: true,
                            minDate: '-100Y',
                            maxDate: '0',
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            yearRange: "-100:+0",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true,
                            isRTL: $scope.isRtl
                        };
                    
                               
                    $scope.passportExpiry = {
                    		changeYear: true,
                            changeMonth: true,
                            minDate: '0',
                            maxDate: '+20Y',
                            dateFormat: 'dd/mm/yy',
                            weekHeader: "Wk",
                            yearRange: "20:+20",
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            constraintInput: true,
                            autoclose: true,
                            isRTL: $scope.isRtl
                        }
                    
                    
                    $scope.scrollToCountry = function() {
                    	if (window.matchMedia("only screen and (max-width: 767px)").matches) {
                    		var element = angular.element('#country');
                    		angular.element("html, body").animate({
                    			scrollTop : element.offset().top - 94
                    		}, 600);
                    	}
                    }
                    
            
                    function startsWith(state, viewValue) {
                        if (viewValue === null || viewValue.trim() === "" ) {
                            return true;
                        } else {
                            var value = viewValue.trim();
                            return state.substr(0, value.length).toLowerCase() == value.toLowerCase();
                        }
                    }
                    
                    function formatLabelCountry(model) {
                        var tempN = $scope.countryList;
                        if (tempN) {
                            for (var i = 0; i < tempN.length; i++) {
                                if (model === tempN[i].code) {
                                    return tempN[i].name;
                                    break;
                                }
                            }
                        }
                    }
                }];
            return directive;
        }];
    isa_auto_checkin.directive(directives);
})();
