/**
 * Created by indika on 9/7/16.
 */
/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_colorPicker = angular.module("isacolorpicker", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isacolorpicker = [
        '$compile', '$http', '$templateCache', '$rootScope',
        function ($compile, $http, $templateCache, $rootScope) {
            var directive = {};
            directive.restrict = 'AEC';
            directive.scope = {
                onColorschemeSave: '&',
                onImageClick:'&',
                fileSelection:'&'
            };
            directive.require = //other sibling directives;
            directive.link = function (scope, element,attr) {
                var template = $templateCache.get(isaconfig.getTemplates("isacolorpicker")["default"]);
                var imageSelection = [];

                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);
                scope.colorscheme = {primaryColor:'',secondaryColor:'','color_variation_1':''}
                scope.imagePath = {logo:""}
                scope.testMsg= "TEST COLOR PICKER"

                function dataURItoBlob(dataURI, callback) {
                    // convert base64 to raw binary data held in a string
                    var byteString = atob(dataURI.split(',')[1]);

                    // separate out the mime component
                    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

                    // write the bytes of the string to an ArrayBuffer
                    var ab = new ArrayBuffer(byteString.length);
                    var ia = new Uint8Array(ab);
                    for (var i = 0; i < byteString.length; i++) {
                        ia[i] = byteString.charCodeAt(i);
                    }

                    // write the ArrayBuffer to a blob, and you're done
                    var bb = new Blob([ab]);
                    return bb;
                }
                scope.fileSelection = function(data){
                    if(!!data.id) {
                        imageSelection[data.id] = data;
                    }
                }
                scope.onColorSelectionSave = function(){
                    var isEmpty = false;
                    var data = {selection:scope.colorscheme};
                    for(var item in scope.colorscheme){
                        if(!scope.colorscheme[item].length){
                            isEmpty = true;
                            break;
                        }
                    }
                    console.log(isEmpty)
                    if(!isEmpty) {
                        scope.onColorschemeSave(data)
                    }else{
                        alert('PLEASE SELECT ALL THREE COLORS');
                    }
                }
                scope.onImageEdit = function(){
                }
                scope.onSelectionSave = function(e){
                    var id = $(e.currentTarget).attr('id').split('-')[0];
                    if(!!imageSelection[id]) {
                        var fileType = imageSelection[id].type;
                        var blob = new Blob([dataURItoBlob(imageSelection[id].file)], {type: fileType});
                        var ext = fileType.split('/')[1];
                        saveAs(blob, (imageSelection[id].name + '.' + ext));
                    }else{
                        alert("PLEASE UPLOAD AN IMAGE");
                    }

                }

            };

            return directive;

        }];
    isa_colorPicker.controller('isaColorPickerController', ['$scope','$element', function ($scope,$element) {
        $scope.$watch('[colorscheme.primaryColor,colorscheme.secondaryColor,colorscheme.color_variation_1]',function(newval,oldval){
            setBackgroundColor(newval,oldval);
        })
        function setBackgroundColor(newVal,oldVal){
            var element = $($element.find('.sub_panel .header-img .preview-wrapper .container ul'));
            console.log(newVal);
            console.log(oldVal);
            console.log($element)
            if(newVal[0] != oldVal[0]){
                element.find('li.logo').css({'background-color':newVal[0]})
            }
            if(newVal[1] != oldVal[1]){
                element.find('li.current').css({'background-color':newVal[1]})
            }
            if(newVal[2] != oldVal[2]){
                element.parent().css({'background-color':newVal[2]})
            }
        }
    }]);
    isa_colorPicker.directive(directives);
})();

