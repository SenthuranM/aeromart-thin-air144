/**
 * Seat Map Directive
 * baladewa
 */
'use strict';

(function () {
    var isa_seatMap = angular.module("seatMap", []);
    var isaconfig = new IsaModuleConfig();

    isa_seatMap.directive('seatMap', [
        '$window', '$timeout', '$compile', '$templateCache','$rootScope',
        function ($window, $timeout, $compile, $templateCache,$rootScope) {
            return {
                restrict: 'AE',
                scope: {
                    seatMapDataModel: "=",
                    seatPrefs: "=",
                    mapId: "@",
                    onSeatSelect: "&",
                    jsonMap: "=",
                    assignToIndex: "=",
                    assignPaxList: "=",
                    onAfterDraw: "&",
                    navigateOnselect: "@",
                    onBeforeSelect: "&",
                    selectionOnChange: "&",
                    previousSeats: "="
                },
                template: $templateCache.get(isaconfig.getTemplates("seatMap")['default']),
                link: function (scope, element, attrs) {
                    //scope.seatModelCopy = angular.copy(scope.seatMapDataModel);
                    var _jsonMap = scope.jsonMap, paxArray = angular.copy(scope.assignPaxList);
                    var autoNavigate = (scope.navigateOnselect === "true");
                    scope.cabinLetters = [];
                    scope.mapDrawn = false;

                    //Function to update the passenger list according to the model changes
                    function setPaxToSeats() {
                        //Clear the seatAssigned value of passenger list
                        _.map(scope.assignPaxList, function (passenger) {
                            passenger.seatAssigned = false;
                        });
                        if (scope.seatPrefs != undefined) {
                            angular.forEach(scope.seatPrefs.passengers, function (currentPassenger) {
                                angular.forEach(currentPassenger.selectedItems, function (item) {
                                    //Check if passenger is assigned to a seat and set that passenger as
                                    // seatAssigned triggering the tick in the Passenger List
                                    angular.forEach(scope.assignPaxList, function (passenger) {
                                        if (passenger.paxSequence === currentPassenger.passengerRph) {
                                            passenger.seatAssigned = true;
                                            passenger.previousSeat = item;
                                            passenger.previousSeat.seatNumber = item.id;
                                        }
                                    });

                                    //passenger.seatAssigned = true;
                                    var sNumber = item.id;
                                    var seat = getSeatByNumber(sNumber);
                                    seat.seatAvailability = "SEL";
                                });
                            });
                        }
                        
                        angular.forEach(scope.previousSeats, function(seatId){
                            var seat = getSeatByNumber(seatId);
                            seat.seatAvailability = _jsonMap.availability['INA'];
                        });


                        paxArray = angular.copy(scope.assignPaxList);
                    }

                    function getSeatByNumber (seatNumber) {
                        //go through cabins
                        if(scope.seatMapDataModel != null) {
                            for (var i = 0; i < scope.seatMapDataModel.cabinClass.length; i++) {
                                var cabin = scope.seatMapDataModel.cabinClass[i];
                                //go through airrow rowgroups
                                for (var j = 0; j < cabin.airRowGroupDTOs.length; j++) {
                                    var rowGroup = cabin.airRowGroupDTOs[j];
                                    //go through airrow airColumnGroups
                                    for (var k = 0; k < rowGroup.airColumnGroups.length; k++) {
                                        var colGroup = rowGroup.airColumnGroups[k];

                                        for (var l = 0; l < colGroup.airRows.length; l++) {
                                            var airRow = colGroup.airRows[l];

                                            for (var m = 0; m < airRow.airSeats.length; m++) {
                                                var seat = airRow.airSeats[m];

                                                if (seat.seatNumber === seatNumber) {
                                                    return seat;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    scope.isSeatMapDataAvailable = function(){
                        var isDataAvailable = (scope.seatMapDataModel != null) ? false : true;
                        return isDataAvailable;
                    }
                    scope.checkAvailability = function (seat, flg) {
                        return (seat.seatAvailability === _jsonMap.availability[flg]);
                    };

                    scope.selectSeat = function (e, obj) {
                        scope.selectionOnChange({event: e, element: obj});

                        var currentIndex = scope.assignToIndex || 0;
                        
                        var assignToPax = angular.copy(paxArray[currentIndex]);
                        var option = {
                            "selectedSeat": obj,
                            "selectedPax": assignToPax,
                            "onSuccess": onSuccess
                        };

                        scope.onBeforeSelect(option);

                        function onSuccess(){
                            assignSeat(e, obj);
                        }

/*                        scope.onBeforeSelect(option, function(){
                            assignSeat(e, obj);
                        });*/

/*                        if (scope.onBeforeSelect(option)) {
                            assignSeat(e, obj);
                        }*/
                    };

                    scope.setSeatValue = function(seat){
                        // Append <span isa-connector="{{airSeat.price}}"></span>
                        // element.find('#' + seat.itemId).parent().find('.tooltip').find('.seat-price').append($compile('<span class="currency"></span>&nbsp; <span>{{airSeat.price}}</span>'));

                    };

                    function assignSeat(e, airSeat) {
                        var currentIndex = scope.assignToIndex || 0;
                        var currentSelectedPassengerIndex = scope.assignToIndex ;

                        var message = "Select a pax to assign a seat", sendIndex = 0;

                        if(airSeat.seatAvailability === _jsonMap.availability['INA']){
                            return;
                        }

                        if (airSeat.seatAvailability === _jsonMap.availability['VAC'] || airSeat.seatAvailability === _jsonMap.availability['RES']) {
                            //get the pax
                            var assignToPax = angular.copy(paxArray[currentIndex]);

                            if (assignToPax !== undefined) {
                                //Reset seat with previous state
                                if (assignToPax.previousSeat !== null && !_.isUndefined(assignToPax.previousSeat)) {
                                    var seat = getSeatByNumber(assignToPax.previousSeat.seatNumber);

                                    if(seat.seatAvailability === _jsonMap.availability['RES']){
                                        seat.seatAvailability = _jsonMap.availability['RES'];
                                    }else if(seat.seatAvailability !== _jsonMap.availability['INA']){
                                        seat.seatAvailability = _jsonMap.availability['VAC'];
                                    }
                                }

                                //Change seat status
                                //assignToPax.previousSeat = angular.copy(airSeat);
                                paxArray[currentIndex] = assignToPax;
                                if(airSeat.seatAvailability !== _jsonMap.availability['RES']){
                                	airSeat.seatAvailability = _jsonMap.availability['SEL'];
                                }
                                message = "Success";

                                var temPax = angular.copy(scope.assignPaxList[currentIndex]);
                                temPax.seat = airSeat;
                                temPax.seatAssigned = true;
                                scope.assignPaxList[currentIndex] = temPax;
                            } else {
                                assignToPax = null;
                            }
                            //!($rootScope.deviceType.isMobileDevice) &&

                            if ( autoNavigate) {
                                //Reset the current index
                                sendIndex = currentIndex;
                                currentIndex++;
                                if (currentIndex > paxArray.length - 1) {
                                    currentIndex = 0;
                                }
                            }
                            if($rootScope.deviceType.isMobileDevice){
                                scope.assignToIndex = currentSelectedPassengerIndex === currentIndex ? currentIndex : currentSelectedPassengerIndex;
                            }else{
                                scope.assignToIndex = currentIndex;
                            }
                            
                            //set to the function
                            var option = {
                                "selectedSeat": airSeat,
                                "selectedPax": assignToPax,
                                "paxIndex": sendIndex,
                                "msg": message
                            };
                            scope.onSeatSelect(option);
                        } else {
                            //If the same seat as the previously selected, remove the selection
                            var assignToPax = angular.copy(paxArray[currentIndex]);

                            if (assignToPax !== undefined) {
                                if (airSeat.itemId === assignToPax.previousSeat.seatNumber) {
                                    //change the seat to VAC
                                    var seat = getSeatByNumber(assignToPax.previousSeat.seatNumber);
                                    seat.seatAvailability = _jsonMap.availability['VAC'];
                                    scope.seatPrefs.passengers[assignToPax.paxSequence - 1].selectedItems = [];
                                    //Remove selection

                                }
                            }
                        }
                    }

                    $timeout(function () {
                        setPaxToSeats();

                        angular.forEach(scope.previousSeats, function(seatId){
                            var seat = getSeatByNumber(seatId);
                            seat.seatAvailability = _jsonMap.availability['INA'];
                        });

                        scope.onAfterDraw();
                    }, 0);

                    scope.$watch('seatMapDataModel', function () {
                        setPaxToSeats();
                    });

                    scope.$watch('seatPrefs', function (newVal, oldVal) {
                        //If old and new values are equal, the seat is removed.
                        if (oldVal) {
                        	if (_.isEqual(oldVal.segments[0].flightSegmentRPH, newVal.segments[0].flightSegmentRPH)) {
	                            angular.forEach(oldVal.passengers, function (val, key) {
	                                if (!_.isEqual(oldVal.passengers[key].selectedItems[0], newVal.passengers[key].selectedItems[0])) {
	                                    if (_.isUndefined(newVal.passengers[key].selectedItems[0])) {
	                                        var emptySeat = getSeatByNumber(oldVal.passengers[key].selectedItems[0].id);
	                                        if(!!emptySeat) {
	                                            emptySeat.seatAvailability = _jsonMap.availability['VAC'];
	                                        }
	                                    }
	                                }
	                            })
                        	}
                        }
                        setPaxToSeats();
                    }, true);

                    scope.$watch('assignPaxList', function (newVal, oldVal){
                        angular.forEach(newVal, function(pax){
                            if(pax.seat){
                                if(pax.seat.seatNumber){
                                    var seat = getSeatByNumber(pax.seat.seatNumber);
                                    seat.passengerName = pax.displayName;
                                }
                            }
                        });
                    }, true);
                }
            };
        }]);

})();