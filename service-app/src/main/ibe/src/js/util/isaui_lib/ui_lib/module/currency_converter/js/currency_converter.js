/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_currency_converter = angular.module("isaCurrencyConverter", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isacurconverter = [
        '$compile', '$timeout', '$http', '$document', '$rootScope', 'currencyService', '$templateCache','$window',
        function ($compile, $timeout, $http, $document, $rootScope, currencyService, $templateCache,$window) {
            var directive = {};

            directive.restrict = 'AEC';
            directive.scope = {
                curruncytypes: '=',
                isopen: '=',
                selectedCurrency: '='
            };
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {

                    // Sort currency list by currency code.
                    scope.curruncytypes = scope.curruncytypes.sort(function (a, b) {
                        if (a.code < b.code) {
                            return -1;
                        }
                        if (a.code > b.code) {
                            return 1;
                        }
                        return 0;
                    });

                    var getSelectedCurrency = function (code) {
                        var toRrturn = {
                            'code': scope.selectedCurrency,
                            'title': '',
                            'icon': scope.selectedCurrency.toLowerCase()
                        };
                        angular.forEach(scope.curruncytypes, function (curr, i) {
                            if (curr.code === code) {
                                toRrturn = curr;
                            }
                        });
                        return toRrturn;
                    };

                    scope.selectedCurrencyType = getSelectedCurrency(scope.curruncytypes);
                    scope.isMobileDevice = $window.matchMedia("only screen and (max-width: 767px)").matches;
                    var template = $templateCache.get(isaconfig.getTemplates("isaCurrencyConverter")['default']);
                    template = angular.element(template);
                    element.append(template);
                    $compile(template)(scope);

                    scope.showpanel = false;
                    angular.element('body').on('click', function (e) {
                        var cname = e.target.className;
                        // check for mouse click is inside the pannel and not on the item in the list
                        var isVisible = element.find(e.target).length > 0 && (cname.indexOf('menu-item') === -1);
                        scope.$apply(function () {
                            if(!$(e.target).hasClass('glyphicon')) {
                                scope.showpanel = isVisible;
                            }
                        });
                    });
                    scope.showHideMenu = function (e) {
                        scope.showpanel = !scope.showpanel;
                    };
                    scope.onItemSelect = function (e) {
                        var newValue = {};
                        scope.selectedCurrencyType = scope.curruncytypes[e.$index];
                        newValue.baseAmount = currencyService.getSelectedCurrencyBaseValue();
                        newValue.currencyType = scope.selectedCurrencyType.code;
                        currencyService.setSelectedCurrency(scope.selectedCurrencyType.code);
                        //TODO remove rootScope and get the event out
                        $rootScope.$emit('ON_CURRENCY_CHANGE', newValue);

                        $timeout(function () {
                            scope.showpanel = false;
                        }, 0)

                    };
                };
            return directive;

        }];
    isa_currency_converter.directive(directives);
})();