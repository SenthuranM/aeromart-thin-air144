'use strict';
(function () {
    var isaFareAndServicesDetails = angular.module("isaFareAndServicesDetails", []);
    var directives = {};

    var isaConfig = new IsaModuleConfig();
    directives.isaFareAndServicesDetails = [
        '$timeout', '$templateCache', '$compile', '$sce', '$rootScope', 'constantFactory', '$window', 'availabilityRemoteService',
        'localStorageService', 'modifyReservationService', 'currencyService', 'languageService',
        function ($timeout, $templateCache, $compile, $sce, $rootScope, constantFactory, $window, availabilityRemoteService,
                  localStorageService, modifyReservationService, currencyService, languageService) {
            var directive = {};

            directive.scope = {
                headerFareInfo: "=",
                flight: "=",
                currency: "@",
                isFlightModification: "=",
                isFlightChanged: "="
            };

            directive.link = function postLink(scope, element, attr) {

                if (!attr['templateBody']) {
                    scope.templateBody = 'default';
                }

                scope.selectedFareBaggageRates = false;
                scope.showBaggagePopup = false;

                scope.fareClassInfo = {};
                scope.isMobileDevice = $rootScope.deviceType.isMobileDevice;
                scope.isSmallDevice = $rootScope.deviceType.isSmallDevice;
                scope.isLargeDevice = $rootScope.deviceType.isLargeDevice;

                var visibleFares = [];
                _.forEach(scope.flight.flightFaresAvailability, function(fare) {
                    if (fare.fareValue && fare.fareValue != "null") {
                        visibleFares.push(fare);
                    }
                });
                scope.flight.flightFaresAvailability = visibleFares;


                setFareDetails();
                setFlightSelectValueForFares();

                var templates = {};
                templates.body = {
                    url: isaConfig.getTemplates("isaFareAndServicesDetails")[scope.templateBody],
                    html: ""
                };

                angular.forEach(templates, function (template, key) {
                    template.html = angular.element($templateCache.get(template.url));
                    element.append(template.html);
                    $compile(template.html)(scope);
                });

                // Needs to analyze is there a better way to prevent calling parent.
                // At the moment this a dependency
                // should at least try to bring this whole module inside the flight_select to
                // produce the meaning that this module can only be used as a child isa_flight_select_directive
                scope.selectThisFare = function (event, flight, fare, parentId) {
                    if (scope.isFlightModification || scope.isFlightChanged) {
                        scope.$parent.onSelectFareNoPopup({selectedFlight: flight, selectedFare: fare});
                    } else {
                        scope.$parent.selectThisFare(event, flight, fare, parentId);
                    }
                    setFlightSelectValueForFares();
                };

                scope.showBaggageRates = function (event, flight, fare) {
                    var searchCriteria = localStorageService.get('SEARCH_CRITERIA');
                    scope.showBaggagePopup = true;

                    if (scope.isFlightModification || scope.isFlightChanged) {
                        var modifyingFlightSearchParams = modifyReservationService.getFlightSearchCriteria();
                        var reservationPaxDetails = modifyReservationService.getReservation()['passengerDetails'];
                        searchCriteria = {
                            cabin: modifyingFlightSearchParams.cabinClassType,
                            adult: reservationPaxDetails.adult || 1,
                            child: reservationPaxDetails.child || 0,
                            infant: reservationPaxDetails.infant || 0
                        }
                    }
                    searchCriteria.currency = currencyService.getSelectedCurrency();

                    if (!scope.fareClassInfo[fare.fareTypeId].baggageRates) {
                        availabilityRemoteService.getBaggageInfo(scope.fareClassInfo[fare.fareTypeId] || fare
                            , flight, searchCriteria, function (response) {
                                scope.fareClassInfo[fare.fareTypeId].baggageRates = (!_.isEmpty(response.baggageRates)  &&
                                    _.sortBy(response.baggageRates[0].baggageRates, function (baggageRate) {
                                        return baggageRate.baggageCharge;
                                    })) || [];
                                scope.selectedFareBaggageRates = scope.fareClassInfo[fare.fareTypeId].baggageRates;

                            }, function () {
                                scope.fareClassInfo[fare.fareTypeId].baggageRates = false;
                                scope.showBaggagePopup = false;
                            });
                    } else {
                        scope.selectedFareBaggageRates = scope.fareClassInfo[fare.fareTypeId].baggageRates;
                    }
                    event.stopPropagation();
                };

                scope.closeBaggagePopup = function () {
                    scope.showBaggagePopup = false;
                    scope.selectedFareBaggageRates = false;
                };

                function setFlightSelectValueForFares() {
                    if (scope.isFlightChanged && scope.flight.selectedFare) {
                        _.forEach(scope.flight.flightFaresAvailability, function (flightFare) {
                            flightFare.isFlightSelect = _.isEqual(scope.flight.selectedFare.fareTypeId, flightFare.fareTypeId);
                        });
                    }
                }

                function updateFareBundleBodyTemplate(template, iconClass, fareDetails, updateSummary) {

                    $(template).find('li').each(function (index, listOption) {
                        $(listOption).find('pre').replaceWith(function () {
                            return "<span>" + this.innerHTML + "</span>";
                        });

                        var innerHtml = $(listOption).html();
                        if (!_.isEmpty(innerHtml)) {

                            fareDetails.bodyContent = fareDetails.bodyContent + '<div class="fare-and-services-item">' +
                                '<img class="' + iconClass +
                                '" src="' + "js/util/isaui_lib/dist/images/" + iconClass + '.svg"></img>' +
                                '<span class="fare-and-services-item-description">'
                                + innerHtml + '</span>' +
                                '</div>';

                            $(listOption).find('a[href="BAGGAGE"]').each(function (index, element) {
                                fareDetails.baggageRatesElement = $(element).html();
                                $(element).remove();
                                innerHtml = $(listOption).html().replace(/&nbsp;/g, '');
                            });
                            if (updateSummary) {
                                if (!_.isEmpty(fareDetails.summary)) {

                                    fareDetails.summary =fareDetails.summary + ','
                                }
                                fareDetails.summary = fareDetails.summary + innerHtml;
                                
                            }
                        }
                    });
                    fareDetails.bodyContent = fareDetails.bodyContent.replace('href="BAGGAGE"',
                        'ng-click="showBaggageRates($event, flight, flightFare)"');

                }

                function getTemplateContent(fare, languageCode, contentType, defaultType) {
                    return fare.templateDTO &&
                        ((fare.templateDTO.translationTemplates && fare.templateDTO.translationTemplates[languageCode]
                        && fare.templateDTO.translationTemplates[languageCode][contentType]) ||
                        fare.templateDTO[defaultType]);
                }


                function setFareDetails() {
                    var fareClasses = scope.headerFareInfo;
                    var languageCode = languageService.getSelectedLanguage() ?
                        languageService.getSelectedLanguage().toLowerCase() : "en";
                    if (scope.isFlightModification || scope.isFlightChanged) {
                        fareClasses = scope.flight.flightFaresAvailability;
                    }

                    _.forEach(fareClasses, function (fareInfo) {
                        var fareBundleName = fareInfo.fareTypeName || fareInfo.description || '';
                        var fareDetails = {
                            name: fareBundleName,
                            description: fareInfo.description,
                            imageUrl: fareInfo.imageUrl,
                            rank: fareInfo.rank,
                            bodyContent: '',
                            summary: '',
                            fareClassType: fareInfo.fareClassType,
                            fareClassId: fareInfo.fareClassId,
                            baggageRatesElement: ''
                        };

                        updateFareBundleBodyTemplate(
                            getTemplateContent(fareInfo, languageCode, 'freeServicesContent',
                                'defaultFreeServicesContent'),
                            "free-item", fareDetails, true);
                        updateFareBundleBodyTemplate(getTemplateContent(fareInfo, languageCode, 'paidServicesContent',
                                'defaultPaidServicesContent'),
                            "dollar-item", fareDetails, false);
                        scope.fareClassInfo[fareInfo.fareTypeId] = fareDetails;
                    });
                }
            };
            return directive;
        }];


    directives.isaCompileHtml = [
        '$timeout', '$templateCache', '$compile', '$sce', '$rootScope', 'constantFactory', '$window',
        function ($timeout, $templateCache, $compile, $sce, $rootScope, constantFactory, $window) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    scope.$watch(function () {

                        return scope.$eval(attrs.isaCompileHtml);
                    }, function (value) {
                        element.html(value);
                        $compile(element.contents())(scope);
                    });
                }
            };
        }];


    isaFareAndServicesDetails.directive(directives);
})();
