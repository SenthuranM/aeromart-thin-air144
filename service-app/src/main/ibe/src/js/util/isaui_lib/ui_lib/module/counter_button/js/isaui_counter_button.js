/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_btn = angular.module("isaCounterButton", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isacbtn = ['$compile', '$http', '$templateCache',
        function ($compile, $http, $templateCache) {
            var directive = {};

            //load relavent tamplate according to the button type

            directive.restrict = 'AEC';
            directive.scope = {caption: '=name'};
            directive.require = //other sibling directives;
                directive.link = function (scope, element, attr) {
                    var template = $templateCache.get(isaconfig.getTemplates("isaCounterButton"));
                    element.append(angular.element(template));
                    $compile(template)(scope);

                };
            return directive;

        }];
    isa_btn.directive(directives);
})();

