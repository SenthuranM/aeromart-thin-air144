/**
 * Created by indika on 10/26/15.
 */
var IBEControllers = function () {
    var ibeControllers = {};
    ibeControllers.inputController = ['$scope', '$rootScope', function ($scope, $rootScope) {
        $rootScope.currencyType = 'AED';
        $rootScope.baseAmount = 1;
    }];
    return ibeControllers;
};
