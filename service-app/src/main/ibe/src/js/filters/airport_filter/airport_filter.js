
/**
 *      Created by Chamil on 03/02/16.
 */

 (function () {

     angular
         .module("IBE")
         .filter('airport', ["remoteStorage", "languageService", airportFilter]);

     function airportFilter(remoteStorage, languageService) {

         return function (code) {

             var i;
             var airport;
             var language = languageService.getSelectedLanguage() ? languageService.getSelectedLanguage().toLowerCase() : "en";
             var airportList = remoteStorage.getAirports();

             for (i = 0; i < airportList.length; i++) {
                 if( airportList[i]['code'] === code && !airportList[i]['city']){
                     airport = airportList[i][language];
                     break;
                 }
             }

             return airport;

         }; // End of return function.

     } // End of airportFilter function.

 })();
