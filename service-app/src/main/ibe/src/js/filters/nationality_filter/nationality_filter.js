
/**
 *      Created by Chamil on 15/01/16.
 */

 (function () {

     angular
         .module("IBE")
         .filter('nationality', ["constantFactory", nationalityFilter]);

     function nationalityFilter(constantFactory) {

         return function (code) {

             // converts code into a string.
             code = code + "";

             var i;
             var nationality;
             var nationalityList = constantFactory.Nationalities();

             for (i = 0; i < nationalityList.length; i++) {
                 if(nationalityList[i]['code'] === code){
                     nationality = nationalityList[i]['name'];
                     break;
                 }
             }

             return nationality;
         };
     }

 })();
