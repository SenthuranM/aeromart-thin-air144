
/**
 *      Created by Chamil on 02/03/16.
 */

 (function () {

     angular
         .module("IBE")
         .filter('salutationFilter', ["constantFactory", salutationFilter]);

     function salutationFilter(constantFactory) {

         return function (code, salutationList) {

             // converts code into a string.
             code = code + "";

             var i;
             var salutation;

             if(_.isUndefined(salutationList)){
                 salutationList = constantFactory.PaxTitles();

                 var paxWiseSalutations = constantFactory.PaxTypeWiseTitle();
                 angular.forEach(paxWiseSalutations, function(salutations){
                     salutationList = _.union(salutationList, salutations);
                 })
             }


             for (i = 0; i < salutationList.length; i++) {
                 if(salutationList[i]['titleCode'] === code){
                     salutation = salutationList[i]['titleName'];
                     break;
                 }
             }

             return salutation;
         };
     }

 })();
