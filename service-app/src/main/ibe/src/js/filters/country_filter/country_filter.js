
/**
 *      Created by Chamil on 15/01/16.
 */

(function () {

    angular
        .module("IBE")
        .filter('country', ["constantFactory", countryFilter]);

    function countryFilter(constantFactory) {

        return function (code) {

            var i;
            var country;
            var countries = constantFactory.Countries();

            for (i = 0; i < countries.length; i++) {
                if(countries[i]['code'] === code){
                    country = countries[i]['name'];
                    break;
                }
            }

            return country;
        };

    }

})();
