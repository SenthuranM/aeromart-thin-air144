/**
 *      Created by Nipuna on 20/09/16.
 *
 *      Converts date from a given format to another using moment.js
 *
 *      Seperate filters used for the different filters to avoid duplication of atrribute declarations in the HTML
 */

(function () {
    angular
        .module('IBE')
        .filter('convertDateFilter', [convertDate])
        .filter('convertFlightDateFilter', [convertFlightDate])
        .filter('convertFlightDateTimeFilter', [convertFlightDateTime]);

    function convertDate() {
        return function (inputDate, outputFormat, inputFormat) {
            var formattedDate;

            if(!outputFormat){
                console.warn('outputFormat mandatory for convertDateFilter');
                return inputDate;
            }

            if(inputFormat){
                formattedDate = moment(inputDate, inputFormat)
            }else{
                formattedDate = moment(inputDate);
            }

            if(formattedDate.isValid()){
                return formattedDate.format(outputFormat);
            }else{
                return inputDate;
            }
        };
    }

    function convertFlightDate() {
        return function (inputDate) {
            return convertDateTime(inputDate, 'DD-MM-YYYY');
        };
    }

    function convertFlightDateTime() {
        return function (inputDate) {
            return convertDateTime(inputDate, 'DD-MM-YYYY HH:mm');
        };
    }

    function convertDateTime(inputDate, outputFormat){
        var dateTimeRegex = new RegExp(/(\d{4}-\d{2}-\d{2} \d{2}:\d{2})/);
        var dateRegex = new RegExp(/(\d{2}\/\d{2}\/\d{4})/);
        var literalDateRegex = new RegExp(/\b\w{3} \d{2} \b\w{3} \d{4}/);

        var formattedDate, inputFormat;

        if(dateTimeRegex.test(inputDate)){
            inputFormat = 'YYYY-MM-DD HH:mm';
        }else if(dateRegex.test(inputDate)){
            inputFormat = 'DD/MM/YYYY';
        }else if(literalDateRegex.test(inputDate)){
            inputFormat = 'ddd DD MMM YYYY';
        }

        formattedDate = moment(inputDate, inputFormat);

        if(formattedDate.isValid()){
            return formattedDate.format(outputFormat);
        }else{
            return inputDate;
        }
    }

})();
