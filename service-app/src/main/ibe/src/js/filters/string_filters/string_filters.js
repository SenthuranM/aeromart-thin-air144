/**
 *      Created by Nipuna on 18/03/16.
 */

(function () {
    angular
        .module("IBE")
        .filter('capitalizeFirstFilter', [capitalizeFirstFilter])
        .filter('startsWithFilter', [startsWithFilter]);

    function capitalizeFirstFilter() {
        return function (inputString) {
            return inputString.charAt(0).toUpperCase() + inputString.slice(1).toLowerCase();
        };
    }

    // Check if string starts with a given value
    function startsWithFilter() {
        function strStartsWith(str, prefix) {
            return (str.toLowerCase()).indexOf(prefix.toLowerCase()) === 0;
        }

        return function(items, prefix, itemProperty) {
            var filtered = [];

            angular.forEach(items, function(item) {
                if(strStartsWith(item[itemProperty], prefix)){
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }
})();
