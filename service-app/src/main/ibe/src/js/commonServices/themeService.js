
(function(){
    angular
        .module("themEngin", [])
        .service("themeEnginService", [ themeEnginService]);

    function themeEnginService () {
        var _this = this;
        var colorModel = {
            primary: '#C0FF33',
            secondary: '#B4D455'
        }
        var isThemable = false;
        _this.setTheme = function (theme) {
            colorModel = theme;
        };
        _this.getTheme = function () {
            return colorModel;
        };
        _this.setThemeEnginStatus = function (status) {
            isThemable = status;
        }
        _this.getThemeEnginStatus = function () {
            return isThemable;
        }

    }

})()