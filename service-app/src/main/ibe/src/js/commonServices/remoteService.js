var RemoteService = function (){
    var service = {};
    service.remoteService = ['$http', '$q', 'ConnectionProps', function($http, $q, ConnectionProps){
        var requestMethod = "GET";
        if(ConnectionProps.apiService){
            requestMethod = "POST";
        }

        this.availabilitySearch = function(sectorSequenceNo,params){
            /**
             * this block has to be duplicate to each api call to separate out to call dummy json or api end point
             * @type {string}
             */
            var path = "/sampleData/timeline_"+sectorSequenceNo+".json";
            if(ConnectionProps.apiService){
                //TODO set the correct path of the api end point with out the host
                path = "";
            }
            return $http({
                "method":requestMethod,
                "url": ConnectionProps.host + path,
                "data":{}
            })
                .success(function (response){
                    if (ConnectionProps.apiService){
                        //TODO write the transformer adaptor here and transform the data object as it is the dummy json
                    	return [];	
                    }
                    return response;

                }).
                error(function (response) {
                    return response;
                });

        };
    }];
    return service;
};