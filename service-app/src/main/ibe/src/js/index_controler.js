/**
 * Created by indika on 10/26/15.
 */
var indexCtrl = function () {
    var indexController = {};
    indexController.indexCtrl = ['$location', 'stepProgressService', 'alertService', 'loadingService', 'quoteService',
        'travelFareService', 'commonLocalStorageService', '$http', 'currencyService', 'languageService',
        '$translate', 'FrontendProps', '$state', 'applicationService', 'busyLoaderService', 'masterRemoteService',
        'eventListeners', 'constantFactory','GTMService','modifyReservationService','ancillaryRemoteService',
        '$timeout', 'ancillaryService','themeEnginService','httpServices','configsService','$window','$rootScope',
        function ($location, stepProgressService, alertService, loadingService, quoteService,
                  travelFareService, commonLocalStorageService, $http, currencyService, languageService,
                  $translate, FrontendProps, $state, applicationService, busyLoaderService, masterRemoteService,
                  eventListeners, constantFactory,GTMService,modifyReservationService,ancillaryRemoteService,
                  $timeout, ancillaryService, themeEnginService,httpServices,configsService,$window,$rootScope) {
            var self = this;
            var temsNcondition = "";

            var dataLayer = [];
            window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
            var confirmedFlightList = {};
            this.alertInfo = alertService.getAlert();
            this.steps = stepProgressService.getAllSteps();
            this.loadingStatus = false;
            this.placeHolder = true;
            this.searchCriteria = {};
            self.extModel = {};
            self.allInclusiveTotal;
            self.dpgTotala;
            self.calculateTotal = calculateTotal;
            this.showSummaryDrawer = quoteService.getShowSummaryDrawer();
            this.drawerLoaderShow = travelFareService.getBusyLoaderStatus();
            this.frontEndParams = FrontendProps;
            this.baseCurrency=currencyService.getAppBaseCurrency();
            this.defaultPGCurrency = currencyService.getAppDefaultPGCurrency();                      
            this.getSelectedCurrencyType = function(){
                return    currencyService.getSelectedCurrency() || "AED";
            };
            this.defaultCurrencyType = currencyService.getSelectedCurrency();
            self.hgt;
            self.selectedLanguage = languageService.getSelectedLanguage();
            
            this.isThreeTwentyDevice = $window.matchMedia("only screen and (max-width: 320px)").matches
            this.quoteDetails = {
                flightCost: "0.00",
                totalPrice: "0.00",
                extraCost: "0.00"
            };
            this.popUpData = "";
            this.showMooreDetails= {};

            this.ismodalModal = false;
            this.showSettings = false;
            this.showThemePanel = false;
            this.showLangugaePanel = false;
            this.carrerCode = applicationService.getCarrerCode();  
            self.isopen= false;
            $rootScope.hgt
            this.appConfigs = {};
            $rootScope.removePaddingFromXSContainerClass = false;
            $rootScope.addMarginForFamilyFriendsPage = false;
            $rootScope.adminFee = commonLocalStorageService.getData('ADMINFEE');

            // This variable used to set banner visiblity in user registration module.
            this.isRegistrationPage = location.hash.indexOf('registration') > 0;

            this.changeQuoteStatus = function () {
                console.log(applicationService.getApplicationMode());
                if(!quoteService.getQuoteStatus() && $state.current.name == "extras" && applicationService.getApplicationMode() == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    ancillaryRemoteService.setAvailableAncillaryData(ancillaryService.getAncillaryRequestJson(),
                        function (data) {
                            modifyReservationService.getBalanceSummaryForModifySegmentAfterAnci(
                                function (data) {
                                    quoteService.changeShowQuote();
                                     if($rootScope.deviceType.isSmallDevice){
                                         $rootScope.hgt =  quoteService.getQuoteStatus() ? 427 : 46 ;
                                }
                                },
                                function (error) {
                                }
                            );
                    
                        })
                }else{
                    quoteService.changeShowQuote();
                    if(applicationService.getApplicationMode() == applicationService.APP_MODES.MODIFY_SEGMENT){
                        if($rootScope.deviceType.isSmallDevice){
                                 if($rootScope.deviceType.isSmallDevice){
                                         $rootScope.hgt =  quoteService.getQuoteStatus() ? 427 : 46 ;
                                }
                            }
                    }
                    
                }
            };

            this.continueAfterSummery = function(){
                console.log('jjjj')
                this.quoteStatus = false;
                eventListeners.notifyListeners(eventListeners.events.CONTINUE_AFTER_SUMMERY);
            }



            this.selectedStep = stepProgressService.getSelectedStep();
            this.isValidCareerFor = function(airline){
                return applicationService.isValidCareerFor(airline);
            }
            
            this.closeAlert = function (index) {
                self.alertInfo = {};
            };

            this.showTermsAndConditions = function () {
                self.ismodalModal = true;
                self.termsNCondition = true;
                self.fareRules = false;

                if (temsNcondition === "") {
                    masterRemoteService.retrieveTermsConditions().then(function (response) {
                        if (response.data.success) {
                            temsNcondition  = response.data.termsNConditionsFull;
                            self.popUpData = temsNcondition;
                        }
                    });
                }else{
                    self.popUpData = temsNcondition;
                }
                quoteService.changeShowQuote();
            };
            this.showFareRule = function(){
                self.ismodalModal = true;
                self.termsNCondition = false;
                self.fareRules = true;
                quoteService.changeShowQuote();
                self.fareRulesDescriptions =  applicationService.getFareRules();
                console.log('fare rule ' + self.fareRulesDescriptions)
                if(!self.fareRulesDescriptions.length) {
                    $translate('msg_fare_no_fare_rules').then(function (msg) {
                        self.fareRulesDescriptions[0] = msg;
                    })
                }

            }

            this.closePopup = function () {
                self.ismodalModal = false;
                quoteService.changeShowQuote();
            };

            this.getDir = function () {
                var trlEnabled = ['ar', 'fa'], rtlValue = 'LTR';
                if (trlEnabled.indexOf(languageService.getSelectedLanguage()) > -1) {
                    rtlValue = 'RTL'
                }
                return rtlValue;
            };
            this.getLangCode = function() {
                return languageService.getSelectedLanguage();
            };
            function onInitFs(fs) {

                fs.root.getFile('new-color-theme.txt', {create: true, exclusive: false}, function(fileEntry) {

                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwrite = function(e) {
                            console.log('Write completed.');
                            console.log(e)
                        };

                        fileWriter.onerror = function(e) {
                            console.log('Write failed: ' + e.toString());
                        };

                        var blob = new Blob(["Lorem Ipsum"], {type: "text/plain"});
                        fileWriter.write(blob);

                    }, function(err){
                        console.log(err)
                    });

                }, function(err){
                    console.log(err)
                });

            }

            this.onColorschemeSave = function(selection){
                console.log(selection)
                localStorage.setItem("colorscheme", JSON.stringify(selection));
                var blob = new Blob([JSON.stringify(selection)], {type: "application/json"});
                saveAs(blob, "new-color-theme.json");
                //window.requestFileSystem(window.TEMPORARY, 1024*1024, onInitFs, function(error){console.log('error')});

            }
            this.onImageClick = function(){
                //handleFiles(document.getElementById('file').files[0])
            }
            this.onSettingPanelClick = function(){
                this.showSettings = (!this.showSettings) ? true :false;
                this.showThemePanel = false;
                this.showLangugaePanel = false;
            }
            this.settingItemClick = function(index){
                this.showSettings = (!this.showSettings) ? true :false;
                console.log('----index---' + index);
                if(index == 0){
                    this.showLangugaePanel = false;
                    this.showThemePanel = true;
                }else if(index == 1){
                    this.showLangugaePanel = true;
                    this.showThemePanel = false;

                }
            }

            //languageService.setSelectedPage(self.selectedStep.id);
            //Using self invoking method for intialization
            init();
            function init() {
                $http({
                    method: "GET",
                    url: "./themeProperty.json"
                }).success(function (data) {
                    themeEnginService.setThemeEnginStatus(data.themeEnginStatus);
                    self.themeEnginStatus = themeEnginService.getThemeEnginStatus();
                }).error(function (data) {
                    return data;
                });
                self.appConfigs = configsService.clientConfigs.configs;
                self.putCheckboxToAcceptTerms = constantFactory.AppConfigValue("putCheckboxToAcceptTerms");

                applicationService.setAirLine(configsService.clientConfigs.airline.name);
                applicationService.setCareerCodes(configsService.clientConfigs.airline);

                if(typeof airports != 'undefined') {
                    if (airports !== undefined) {//loaded ond_js
                        commonLocalStorageService.setAirports(airports);
                        commonLocalStorageService.setCurrencies(currencies);
                        commonLocalStorageService.setOrigins(origins);
                    }
                }
                currencyService.setAppBaseCurrency(constantFactory.AppConfigStringValue('baseCurrency'));
                GTMService.setGoogleTagLayer([{'Category':constantFactory.AppConfigStringValue('defaultCarrierCode')}]);
                self.logoLink = constantFactory.AppConfigStringValue('defaultAirlineURL');

                var gtag = GTMService.getGoogleTagLayer()
                _.each(gtag,function(item,index){
                    if(_.findIndex(dataLayer,item) == -1) {
                        dataLayer.push(item)
                    }
                })
                if (commonLocalStorageService.getConfirmedFlightData() != undefined) {
                    self.extModel = travelFareService.getSectors(); //Extra model for the summary drawer
                }
                self.quoteDetails = travelFareService.getQuoteDetails();
                self.extraTotal = quoteService.getExtraTotal();
                quoteService.setServiceTaxTotal(self.quoteDetails.totalServiceTax);
                self.allInclusiveTotal = self.calculateTotal(self.quoteDetails.totalPrice, self.extraTotal, self.quoteDetails.jnTax, self.quoteDetails.anciPromotion, self.quoteDetails.totalServiceTaxes, self.quoteDetails.administrationFee, self.quoteDetails.serviceTaxForAdminFee);
                var dpgBaseValue = currencyService.getSelectedCurrencyBaseValue(self.defaultPGCurrency);
                self.dpgTotala = self.allInclusiveTotal * (1 / dpgBaseValue);
                eventListeners.registerListeners(eventListeners.events.SET_TOTAL, function (data) {
                    self.allInclusiveTotal = data;
                });
            };

            this.navigateToState = function (state) {
                var routes = applicationService.getApplicationRoute();
                if (routes.indexOf(state) > -1) {
                    if (stepProgressService.getStepByState(state).status === 'completed') {
                        var params = {
                            lang: languageService.getSelectedLanguage(),
                            currency: currencyService.getSelectedCurrency(),
                            mode: applicationService.getApplicationMode(),
                            originCountry: commonLocalStorageService.getData('CARRIER_CODE')
                        };
                        var sp = commonLocalStorageService.getSearchCriteria();
                        if (state === "fare" || state === "search") {
                            angular.forEach(sp, function (pram, key) {
                                params[key] = pram;
                            });

                            if(state === 'search') {
                                if ((sp.originCity == "Y" || sp.destCity == "Y")) {
                                    state = 'fare_city';
                                } else {
                                    state = 'fare';
                                }
                                $timeout(function(){
                                    eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
                                }, 500);
                            }
                        }
                        $state.go(state, params);
                    }
                }
            };

            function setSearchCriteria() {
                /*self.searchCriteria = commonLocalStorageService.getSearchCriteria();

                 commonLocalStorageService.setAppLanguage(self.searchCriteria.lang);
                 currencyService.setSelectedCurrency(self.searchCriteria.currency);*/
            };

            //Getting initial values
            //this.quoteDetails.outboundTrip = commonLocalStorageService.getOutboundTripInfo();

            this.onContinueToPassenger = function () {
                var currency = currencyService.getSelectedCurrency(),
                    language = languageService.getSelectedLanguage();

                commonLocalStorageService.setConfirmedFlightData(travelFareService.getConfirmedFlights());
                $location.path('/passenger/' + language + '/' + currency);


                //set extra data if searchData is set
                if (commonLocalStorageService.getConfirmedFlightData() != undefined) {
                    self.extModel = travelFareService.getSectors(); //Extra model for the summary drawer
                }

                travelFareService.setQuoteDetails(self.quoteDetails);
                //quoteService.changeShowQuote();
            };

            this.getExtrasTotal = function (allSectors) {
                var total = 0;

                for (var tripKey in allSectors) {
                    var tripArray = allSectors[tripKey];
                    for (var allExtrasModel in tripArray) {
                        var extraTotals = tripArray[allExtrasModel].extrasTotal;
                        for (var extra in extraTotals) {
                            total = total + extraTotals[extra].total;
                        }
                    }
                }


                return total;
            };

            function calculateTotal(value1, value2, value3, value4, value5, value6, value7, value8) {
                if (!value1) {
                    value1 = 0;
                }
                if (!value2) {
                    value2 = 0;
                }
                if(!value3){
                    value3=0;
                }
                if(!value4){
                    value4=0;
                }
                if(!value5){
                	value5=0;
                }
                if(!value6){
                	value6=0;
                }
                if(!value7){
                	value7=0;
                }
                if(!value8){
                	value8=0;
                }
                var total = parseFloat(value1) + parseFloat(value2)+ parseFloat(value3)-parseFloat(value4)+parseFloat(value5)+parseFloat(value6)+parseFloat(value7)+parseFloat(value8);
                return total;
            }
            //Using commonLocalStorage to listen for changes in the outbound trip information
            commonLocalStorageService.registerListener(commonLocalStorageService.EVENTS.OUTBOUND_TRIP_INFO_CHANGED, function () {
                self.quoteDetails.outboundTrip = commonLocalStorageService.getOutboundTripInfo();
            });


            stepProgressService.registerListeners(function () {
                self.selectedStep = stepProgressService.getSelectedStep();
                $(window).scrollTop(0);
                //languageService.setSelectedPage(self.selectedStep);
            });
            alertService.registerListeners(function () {
                self.alertInfo = alertService.getAlert();
            });
            loadingService.registerListeners(function () {
                self.loadingStatus = loadingService.getLoadingStatus();
            });
            quoteService.registerListeners(function () {
                self.quoteStatus = quoteService.getQuoteStatus();
                self.quoteDetails = travelFareService.getQuoteDetails();
                self.showSummaryDrawer = quoteService.getShowSummaryDrawer();
                self.extraTotal = quoteService.getExtraTotal();
                self.serviceTaxTotal = quoteService.getServiceTaxTotal();
                self.serviceTaxForAdminFee = quoteService.getServiceTaxForAdminFee();
                self.serviceTaxWithoutAdminFee = parseFloat(self.serviceTaxTotal) - parseFloat(self.serviceTaxForAdminFee);
            });
            busyLoaderService.registerListeners(function () {
                self.drawerLoaderShow = busyLoaderService.getBusyLoaderStatus();
            });
            this.changeDetailsStatus = function(id){
                /*if(self.showMooreDetails+"_"+id){
                    self.showMooreDetails+"_"+id = ! self.showMooreDetails+"_"+id
                }
                else{
                    self.showMooreDetails+"_"+id = true;
                }*/
                if(self.showMooreDetails[id]){
                    self.showMooreDetails[id] = !self.showMooreDetails[id];
                }
                else{
                    self.showMooreDetails[id] = true;
                }

            }
            this.getFlightDirection = function(toCode, fromCode,isModify){
                return  applicationService.getFlightDirection(toCode, fromCode,isModify)
            }

        }];
    return indexController;
};
