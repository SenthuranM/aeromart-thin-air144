/**
 * Created by sumudu on 1/25/16.
 */
'use strict';

angular.module("IBE")
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        var ibeConfig = new IBEConfig();
        $urlRouterProvider.otherwise("/")

        $stateProvider
            .state('fare', {
                url: "/fare/:lang/:currency/:originCountry/:from/:to/:depDate/:retDate/:adult/:child/:infant/:cabin/:promoCode",                
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['default'],
                        controller: "travelFareController",
                        controllerAs: 'fare'
                    },
                    onEnter: ['applicationService', function (applicationService) {
                        applicationService.setApplicationMode(applicationService.APP_MODES.CREATE_RESERVATION);
                    }],
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'footer': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['createSummeryDrawer']
                    },
                    'modifySearch@fare': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'modifySearch@passenger': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('multi-city', {
                url: "/multi-city/:lang/:currency/:originCountry",
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("multiCity")['default'],
                        controller: "multiCitySearchController",
                        controllerAs: 'vm'
                    },
                    onEnter: ['applicationService', function (applicationService) {
                        applicationService.setApplicationMode(applicationService.APP_MODES.CREATE_RESERVATION);
                    }],
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('multi-city-flight', {
                url: "/multi-city/flight/:lang/:currency/:originCountry",
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("multiCity")['flight'],
                        controller: "multiCityFlightController",
                        controllerAs: 'vm'
                    },
                    onEnter: ['applicationService', function (applicationService) {
                        applicationService.setApplicationMode(applicationService.APP_MODES.CREATE_RESERVATION);
                    }],
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('multi-city-fare', {
                url: "/multi-city/fare/:lang/:currency/:originCountry",
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("multiCity")['fare'],
                        controller: "multiCityFareController",
                        controllerAs: 'vm'
                    },
                    onEnter: ['applicationService', function (applicationService) {
                        applicationService.setApplicationMode(applicationService.APP_MODES.CREATE_RESERVATION);
                    }],
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('fare_city', {                
                url: "/fare/:lang/:currency/:originCountry/:from/:to/:depDate/:retDate/:adult/:child/:infant/:cabin/:promoCode/:originCity/:destCity",
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['default'],
                        controller: "travelFareController",
                        controllerAs: 'fare'
                    },
                    onEnter: ['applicationService', function (applicationService) {
                        applicationService.setApplicationMode(applicationService.APP_MODES.CREATE_RESERVATION);
                    }],
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'footer': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['createSummeryDrawer']
                    },
                    'modifySearch@fare_city': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('passenger', {
                url: "/passenger/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("passenger")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    },
                    'footer': {
                        templateUrl: function (urlParams) {
                            if (!!urlParams.mode && urlParams.mode == 'nameChange') {
                                return  ibeConfig.getTemplates("modifyReservation")["nameChangeDrawer"]
                            } else {
                                return  ibeConfig.getTemplates("travelFare")['createSummeryDrawer'];
                            }
                        }
                    },
                    'modifySearch@passenger': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    }
                }
            })
            .state('passenger_material', {
                url: "/passenger-1/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("passenger")['material'],
                        controller: "passengerController",
                        controllerAs: 'pax'
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'footer': {
                        templateUrl: function (urlParams) {
                            if (!!urlParams.mode && urlParams.mode == 'modify') {
                                return  ibeConfig.getTemplates("modifyReservation")["nameChangeDrawer"]
                            } else {
                                return  ibeConfig.getTemplates("travelFare")['createSummeryDrawer'];
                            }
                        }
                    }
                }
            })
            .state('extras', {
                url: "/extras/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extras")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'footer': {
                        templateUrl: function (urlParams) {
                            if (!!urlParams.mode && (urlParams.mode == 'modify_segment' || urlParams.mode == 'modifyAnci')) {
                                return  ibeConfig.getTemplates("modifyFlight")["flightModificationDrawer"]
                            } else {
                                return  ibeConfig.getTemplates("travelFare")['createSummeryDrawer'];
                            }
                        }
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    },
                    'modifySearch@extras': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    }
                }

            })
            .state('payments', {
                url: "/payment/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("payment")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'paymentSummary@payments': {
                        templateUrl: function ($stateParams) {
                            var template = 'createPaymentSummary';
                            if ($stateParams.mode != 'create') {
                                template = 'modifyPaymentSummary';
                            }
                            if ($stateParams.mode == 'balancePayment'){
                                template = 'onholdPaymentSummary';
                            }
                            return ibeConfig.getTemplates("payment")[template];
                        }
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('postPayment', {
                url: "/postPayment",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("payment")['postPayment']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'footer': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['createSummeryDrawer']
                    }
                }
            })
            .state('voucherPostPayment', {
                url: "/voucherPostPayment",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("payment")['voucherPostPayment']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("voucher")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    }
                }
            })
            .state('confirm_onhold', {
                url: "/confirmOnhold/:lang/:currency/:mode?pnr&trackingNumber&releaseTime",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("confirm")['onhold']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('confirm_onhold_external', {
                url: "/confirmOnhold?pnr&trackingNumber&releaseTime",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("confirm")['onhold']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('confirm', {
                url: "/confirmReservation/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("confirm")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('voucher', {
                url: "/voucher/:lang/:currency/:originCountry/:mode",
                params: {
                    mode: {
                        value: 'purchaseVoucher',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("voucher")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("voucher")['header']
                    }
                },
                data: {
                    isPaymentPage: false
                }
            })
            .state('voucherPayment', {
                url: "/voucherPayment/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'payVoucher',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("voucher")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("voucher")['header']
                    }
                },
                data: {
                    isPaymentPage: true
                }
            })
            .state('voucherThank', {
                url: "/voucherThank/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    },
                    obj:null
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("voucherThank")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("voucher")['header']
                    }
                }
            })
            .state('404/:lang', {
                url: '*//*location',
                templateUrl: '404.tpl.html'
            })
            .state('system_error', {
                url: '/error/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("errorPage")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    }
                }
            })
            .state('confirm_lms', {
                url: '/lmsMemberConfirmation/:lang?ffid&validationText',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views:{
                    '': {
                        templateUrl: ibeConfig.getTemplates("confirmPage")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    }
                }
            })
            .state('confirm_customer', {
                url: '/confirmCustomer/:lang?emailId&validationText',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views:{
                    '': {
                        templateUrl: ibeConfig.getTemplates("confirmPage")['customer']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    }
                }
            })
            .state('baggage', {
                url: '/extras/baggage/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraBaggage")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }

            })
            .state('seats', {
                url: '/extras/seats/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraSeats")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('meals', {
                url: '/extras/meals/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraMeal")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('automatic_checkin', {
                url: '/extras/automatic_checkin/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraAutomaticCheckin")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('insurance', {
                url: '/extras/insurance/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraInsurance")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('airport_transfer', {
                url: '/extras/airport_transfer/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraAirportTransfer")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('flexibility', {
                url: '/extras/flexibility/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraflexibility")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('services', {
                url: '/extras/services/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraServices")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('airport_services', {
                url: '/extras/airport_services/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("extraAirportServices")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('modifyDashboard', {
                url: "/modify/dashboard/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    },
                    data: {
                        isDashboard: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'modifySearch@modifyDashboard': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    }
                }
            })
            .state('modifyReservation', {
                url: '/modify/reservation/:lang/:currency/:originCountry/:pnr/:lastName/:departureDate',
                onEnter: ['applicationService', function (applicationService) {
                    applicationService.setApplicationMode(applicationService.APP_MODES.ADD_MODIFY_ANCI);
                }],
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("modifyReservation")['default'],
                        controller: 'ModifyReservationController',
                        controllerAs: 'reservation'
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'modifySearch@modifyReservation': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'footer': {
                        templateUrl: ibeConfig.getTemplates("modifyReservation")["modificationDrawer"],
                        controller: "ModificationSummeryDrawerController",
                        controllerAs: 'drawer'
                    }
                }
            })
            .state('reservationLookup', {
                url: '/modify/reservations/lookup/:lang/:currency/:pnr/:lastName/:departureDate',
                onEnter: ['applicationService', function (applicationService) {
                    applicationService.setApplicationMode(applicationService.APP_MODES.ADD_MODIFY_ANCI);
                }],
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("modifyReservation")['lookup'],
                        controller: 'ReservationLookupController',
                        controllerAs: 'lookup'
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'modifySearch@reservationLookup': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    }
                }
            })
            .state('signIn', {
                url: "/signIn/:lang/:currency/:originCountry/:mode",
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("signIn")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'banner': {
                        templateUrl: ibeConfig.getTemplates("banner")['default']
                    }
                }
            })
            .state('redirect', {
                url: '/redirect/:lang/:currency/:success/:isJoinedLMS',
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("redirect")['default']
                    }
                }
            })
            .state('registration', {
                url: "/registration/:lang/:currency/:originCountry/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("registration")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'modifySearch@registration': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'banner': {
                        templateUrl: ibeConfig.getTemplates("banner")['default']
                    }
                }
            })
            .state('updateProfile', {
                url: "/updateProfile/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    },
                    '': {
                        templateUrl: ibeConfig.getTemplates("registration")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'modifySearch@updateProfile': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'banner': {
                        templateUrl: ibeConfig.getTemplates("banner")['default']
                    }
                },
                data: {
                    isUpdateProfile: true,
                    isMobileHeaderVisible: true
                }
            })
            .state('changePassword', {
                url: "/changePassword/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    },
                    '': {
                        templateUrl: ibeConfig.getTemplates("registration")['changePassword']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'modifySearch@updateProfile': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    },
                    'banner': {
                        templateUrl: ibeConfig.getTemplates("banner")['default']
                    }
                },
                data: {
                    isChangePassword: true,
                    isMobileHeaderVisible: true
                }
            })
            .state('resetPassword', {
                url: "/resetPassword/:lang/:currency/:mode/{token:.*}",
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("registration")['changePassword']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'banner': {
                        templateUrl: ibeConfig.getTemplates("banner")['default']
                    }
                },
                data: {
                    isResetPassword: true
                }
            })
            .state('modifyFlightHome', {
                url: '/modify/flights',
                abstract: true,
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("modifyFlight")['default'],
                        controller: 'ModifyFlightHomeController',
                        controllerAs: 'modifyFlights'
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'footer': {
                        templateUrl: ibeConfig.getTemplates("modifyFlight")["flightModificationDrawer"]
                    },
                    'modifySearch@modifyFlightHome': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    }
                }
            })
            .state('modifyFlightHome.flightHome', {
                url: '/home/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    'modifyContentView@modifyFlightHome': {
                        templateUrl: ibeConfig.getTemplates("modifyFlight")['selectFlightHome']
                    }
                }



            })
            .state('modifyFlightHome.flightSelected', {
                url: '/selected/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    'modifyContentView@modifyFlightHome': {
                        templateUrl: ibeConfig.getTemplates("modifyFlight")['selectedFlight'],
                        controller: 'ModifySelectedFlightController',
                        controllerAs: 'selectedFlight'

                    }
                },
                data: {
                    cancellable : true,
                    modifible: true
                }

            })
            .state('modifyFlightHome.flightSearch', {
                url: '/search/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    'modifyContentView@modifyFlightHome': {
                        templateUrl: ibeConfig.getTemplates("modifyFlight")['flightSearch'],
                        controller: 'ModifyFlightSearchController',
                        controllerAs: 'searchCtrl'
                    }
                }
            })
            .state('modifyFlightHome.flightSearchResult', {
                url: '/search_result/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    'modifyContentView@modifyFlightHome': {
                        templateUrl: ibeConfig.getTemplates("modifyFlight")['flightSearchResult'],
                        controller: 'ModifyFlightSearchResultController',
                        controllerAs: 'resultCtrl'

                    }
                }
            })

            .state('modifyFlightHome.changedFlight', {
                url: '/changed_flight/:lang/:currency/:mode',
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    'modifyContentView@modifyFlightHome': {
                        templateUrl: ibeConfig.getTemplates("modifyFlight")['changedFlight'],
                        controller: 'ModifyFlightChangedFlightController',
                        controllerAs: 'changedFlight'

                    }
                }
            })
            .state('viewReservations', {
                url: "/modify/viewReservations/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    },
                    '': {
                        templateUrl: ibeConfig.getTemplates("viewReservations")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'modifySearch@viewReservations': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    }
                },
                data : {
                	isMyReservations : true,
                	isMobileHeaderVisible: true
                }

            })
            .state('viewCreditHistory', {
                url: "/modify/viewCreditHistory/:lang/:currency/:mode",
                params: {
                    mode: {
                        value: 'modify',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("viewCreditHistory")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("modifyDashboard")['header']
                    },
                    'liveChat':{
                        templateUrl: ibeConfig.getTemplates("liveChat")['default']
                    },
                    'loginBar': {
                        templateUrl: ibeConfig.getTemplates("loginBar")['default']
                    },
                    'modifySearch@viewCreditHistory': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    }
                }

            })
              .state('reProtect', {
            	  url: "/self-reprotect/:lang/:pnr/:alertId/:pnrSegmentId/:oldFlightSegmentId",
                params: {
                    mode: {
                        value: 'create',
                        squash: true
                    }
                },
                views: {
                    '': {
                        templateUrl: ibeConfig.getTemplates("reProtect")['default']
                    },
                    'topNav': {
                        templateUrl: ibeConfig.getTemplates("reProtect")['header']
                    }
                }
            })
            .state('viewFamilyFriends',	{
				url : "/modify/viewFamilyFriends/:lang/:currency/:mode",
			params : {
				mode : {
					value : 'modify',
					squash : true
					}
			},
			views : {
                'loginBar': {
                    templateUrl: ibeConfig.getTemplates("loginBar")['default']
                },
		    	'' : {
					templateUrl : ibeConfig.getTemplates("viewFamilyFriends")['default']
				},
				'topNav' : {
					templateUrl : ibeConfig.getTemplates("modifyDashboard")['header']
	     			},
				'modifySearch@viewFamilyFriends': {
                    templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                	}
     			},
     		data : {
     			isFamilyAndFriends : true,
     			isMobileHeaderVisible: true
     		}
			})
			.state('viewSeat',	{
				url : "/modify/viewSeat/:lang/:currency/:mode",
			params : {
				mode : {
					value : 'modify',
					squash : true
					}
			},
			views : {
                'loginBar': {
                    templateUrl: ibeConfig.getTemplates("loginBar")['default']
                },
		    	'' : {
					templateUrl : ibeConfig.getTemplates("viewSeat")['default']
				},
				'topNav' : {
					templateUrl : ibeConfig.getTemplates("modifyDashboard")['header']
	     			},
                 'modifySearch@viewSeat': {
                        templateUrl: ibeConfig.getTemplates("travelFare")['modifySearch']
                    }
     			},
     		data : {
     			isSeatMeatPreferences : true,
     			isMobileHeaderVisible: true
     		}
			})

    }]);
