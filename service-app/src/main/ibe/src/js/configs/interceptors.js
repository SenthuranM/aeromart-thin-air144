/**
 * Created by sumudu on 2/9/16.
 */

angular.module('IBE').factory('ibeHttpInterceptor', [
    'applicationService',
    function (applicationService) {

        var reqResInterceptor = {
            request: function (config) {
                if (config.method == 'POST') {
                    if (!!config.data) {
                        config.data['transactionId'] = applicationService.getTransactionID();
                    }
                }
                return config;
            },
            response: function (response) {
                if (response.config.method == 'POST' && response.data.hasOwnProperty('transactionId')) {
                    applicationService.setTransactionID(response.data.transactionId);
                }
                return response
            }
        };
        return reqResInterceptor;
    }]);