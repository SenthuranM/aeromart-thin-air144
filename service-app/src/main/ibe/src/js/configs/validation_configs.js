/**
 * Created by sumudu on 1/25/16.
 */
'use strict';

angular.module("IBE").config([
    'valdrProvider',
    'valdrMessageProvider',
    function (valdrProvider, valdrMessageProvider) {
        //Valdr initial configuration
        valdrProvider.addValidator('customValidator');

        valdrMessageProvider.setTemplate('<div class="valdr-message">{{ violation.message }}</div>');

        //TODO: validation rules should be loaded from web service
        valdrProvider.addConstraints({
            'Adult': {
                'lastName': {
                    'required': {
                        'message': 'Last name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'Last name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'Last name cannot contain numbers or symbols.'
                    }
                },
                'firstName': {
                    'required': {
                        'message': 'First name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'First name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'First name cannot contain numbers or symbols.'
                    }
                },
                'salutation': {
                    'required': {
                        'message': 'Salutation is required.'
                    }
                },
                'nationality': {
                    'required': {
                        'message': 'Nationality is required.'
                    }
                },

                'dob': {
                    'required': {
                        'message': 'Date of Birth is required.'
                    },
                    'ageConstraints': {
                        'over': 18,
                        'under': 200,
                        'message': "age Must be over 18"
                    }

                },
                'airewardsEmail': {
                    'pattern': {
                        'value': "^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$",
                        'message': "Email must be properly formatted"
                    }
                },
                'travelWith': {
                    'required': {
                        'message': 'Traveling with.'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/"
                    }
                }
            },
            'Child': {
                'lastName': {
                    'required': {
                        'message': 'Last name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'Last name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'Last name cannot contain numbers or symbols.'
                    }
                },
                'firstName': {
                    'required': {
                        'message': 'First name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'First name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'First name cannot contain numbers or symbols.'
                    }
                },
                'salutation': {
                    'required': {
                        'message': 'Salutation is required.'
                    }
                },
                'nationality': {
                    'required': {
                        'message': 'Nationality is required.'
                    }
                },
                'dob': {
                    'required': {
                        'message': 'Date of Birth is required.'
                    },
                    'ageConstraints': {
                        'over': 2,
                        'under': 18,
                        'message': "Age between 3 and 18"
                    }
                }
            },
            'Infant': {
                'lastName': {
                    'required': {
                        'message': 'Last name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'Last name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'Last name cannot contain numbers or symbols.'
                    }
                },
                'firstName': {
                    'required': {
                        'message': 'First name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'First name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'First name cannot contain numbers or symbols.'
                    }
                },
                'dob': {
                    'required': {
                        'message': 'Date of Birth is required.'
                    },
                    'ageConstraints': {
                        'under': 1,
                        'message': "Age must be under 2 and over 0 years"
                    }
                },
                'travelWith': {
                    'required': {
                        'message': 'Traveling with.'
                    }
                }
            },
            'Contact': {
                'lastName': {
                    'required': {
                        'message': 'Last name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'Last name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'Last name cannot contain numbers or symbols.'
                    }
                },
                'firstName': {
                    'required': {
                        'message': 'First name is required.'
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'First name should contain at least 2 character(s).'
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'First name cannot contain numbers or symbols.'
                    }
                },
                'salutation': {
                    'required': {
                        'message': 'Salutation is required.'
                    }
                },
                'language': {
                    'required': {
                        'message': 'Please select language.'
                    }
                },
                'email': {
                    'required': {
                        'message': "Email is required"
                    },
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Email must be properly formatted"
                    }
                },
                'verifyEmail': {
                    'required': {
                        'message': "Email is required"
                    },
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Email must be properly formatted"
                    }
                },
                'mobileNo': {
                    'required': {
                        'message': "Mobile number is required"
                    },
                    'pattern': {
                        'value': "/^([+]{1})?[0-9]+$/",
                        'message': "Mobile number must only contain numeric data"
                    }, 'size': {
                        'min': 8,
                        'message': 'Must contain more than 8 numbers.'
                    }
                },
                'mobileNoTravel': {
                    'required': {
                        'message': "Mobile number is required"
                    },
                    'pattern': {
                        'value': "/^[0-9]+$/",
                        'message': "Mobile number must only contain numeric data"
                    }, 'size': {
                        'min': 8,
                        'message': 'Must contain more than 8 numbers.'
                    }
                }, 'country': {
                    'required': {
                        'message': "Country is required"
                    }
                }, 'nationality': {
                    'required': {
                        'message': "Nationality is required"
                    }
                },
                'taxRegNo' : {
                    'name': "ng-pattern",
                    'value': "/[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[a-zA-Z0-9]{1}[a-zA-Z]{1}[a-zA-Z0-9]{1}/",
                    'label': "lbl_passenger_invalidTaxRegNoFormat"
                }
            },
            'Registration': {
                'password': {
                    'required': {
                        'message': 'Password is required.'
                    },
                    'size': {
                        'min': 6,
                        'max': 20,
                        'message': 'Password must be 6 characters and have alpha-numeric'
                    },
                    'pattern': {
                        'value': "/^[^<>\"\'\/\^|]{6,}$/"
                    }
                },
                'confirmPassword': {
                    'required': {
                        'message': 'Password is required.'
                    },
                    'size': {
                        'min': 6,
                        'max': 20,
                        'message': 'Password must be 6 characters and have alpha-numeric'
                    },
                    'pattern': {
                        'value': "/^[^<>\"\'\/\^|]{6,}$/"
                    }
                },
                'email': {
                    'required': {
                        'message': "Email is required"
                    },
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Email must be properly formatted"
                    }
                }
            },
            'airwardsReg': {
                'airwardsDOB': {
                    'required': {
                        'message': 'Date of Birth is required.'
                    }
                },
                'language': {
                    'required': {
                        'message': 'Communucation Language is required.'
                    }
                },
                'refEmail': {
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Email must be properly formatted"
                    }
                },
                'familyEmail': {
                    'required': {
                        'message': 'Family Head Email is required.'
                    },
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Family Head Email must be properly formatted."
                    }
                },
                'loyaltyAccountNo':{
                    'required': {
                        'message': 'Account Number is required.'
                    },
                    'size': {
                        'min': 12,
                        'max': 12,
                        'message': 'Account number should contains 12 digits.'
                    }
                },
                'mashreqDOB': {
                    'required': {
                        'message': 'Date of Birth is required.'
                    }
                },
                'city': {
                    'required': {
                        'message': 'City is required.'
                    }
                }
            },
            'ChangePassword': {
                'email': {
                    'required': {
                        'message': "Email is required"
                    },
                    'pattern': {
                        'value': "/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/",
                        'message': "Email must be properly formatted"
                    }
                },
                'password': {
                    'required': {
                        'message': 'Password is required.'
                    },
                    'pattern': {
                        'value': "/^[^<>\"\'\/\^|]{6,}$/",
                        'message': 'Password must contain at least 6 characters and must be alpha-numeric. No special characters are allowed.'
                    }
                },
                'newPassword': {
                    'required': {
                        'message': 'Password is required.'
                    },
                    'pattern': {
                        'value': "/^[^<>\"\'\/\^|]{6,}$/",
                        'message': 'Password must contain at least 6 characters and must be alpha-numeric. No special characters are allowed.'
                    }
                },
                'confirmPassword': {
                    'required': {
                        'message': 'Confirm Password is required.'
                    },
                    'pattern': {
                        'value': "/^[^<>\"\'\/\^|]{6,}$/",
                        'message': 'Password must contain at least 6 characters and must be alpha-numeric. No special characters are allowed.'
                    }
                }
            },
            'CustomerRegistration': {
                'email': {
                    'required': {
                        'message': "Email is required",
                        'label': "lbl_validation_emailRequired"
                    },
                    'pattern': {
                        'value': "/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/",
                        'message': "Email must be properly formatted",
                        'label': "lbl_validation_emailFormat"
                    }
                },
                'password': {
                    'required': {
                        'message': 'Password is required.',
                        'label': "lbl_validation_passwordRequired"
                    },
                    'pattern': {
                        'value': "/^[^<>\"\'\/\^|]{6,}$/",
                        'message': 'Password must contain at least 6 characters and must be alpha-numeric. No special characters are allowed.'
                    }
                },
                'confirmPassword': {
                    'required': {
                        'message': 'Confirm Password is required.',
                        'label': "lbl_validation_confirmPasswordRequired"
                    },
                    'pattern': {
                        'value': "/^[^<>\"\'\/\^|]{6,}$/",
                        'message': 'Password must contain at least 6 characters and must be alpha-numeric. No special characters are allowed.'
                    }
                },
                'salutation': {
                    'required': {
                        'message': 'Salutation is required.'
                    }
                },
                'firstName': {
                    'required': {
                        'message': 'First name is required.',
                        'label': "lbl_validation_firstNameRequired"
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'First name should contain at least 2 character(s).',
                        'label': "lbl_validation_firstNameMinChar"
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'First name cannot contain numbers or symbols.',
                        'label': "lbl_validation_firstNameSymbol"
                    }
                },
                'lastName': {
                    'required': {
                        'message': 'Last name is required.',
                        'label': "lbl_validation_lastNameRequired"
                    },
                    'size': {
                        'min': 2,
                        'max': 20,
                        'message': 'Last name should contain at least 2 character(s).',
                        'label': "lbl_validation_lastNameMinChar"
                    },
                    'pattern': {
                        'value': "/^([^0-9]*)$/",
                        'message': 'Last name cannot contain numbers or symbols.',
                        'label': "lbl_validation_lastNameSymbol"
                    }
                },
                'nationality': {
                    'required': {
                        'message': 'Nationality is required.',
                        'label': "lbl_validation_nationalityRequired"
                    }
                },
                'country': {
                    'required': {
                        'message': "Country of Residence is required"
                    }
                },
                'mobile': {
                    'required': {
                        'message': "Mobile number is required"
                    },
                    'pattern': {
                        'value': "/^([+]{1})?[0-9]+$/",
                        'message': "Mobile number must only contain numeric data",
                        'label': "lbl_validation_phoneNumNumeric"
                    }, 'size': {
                        'min': 8,
                        'message': 'Must contain more than 8 numbers.'
                    }
                },
                'airwardsDOB': {
                    'required': {
                        'message': 'Date of Birth is required.',
                        'label': "lbl_validation_dobRequired"
                    }
                },
                'language': {
                    'required': {
                        'message': 'Communucation Language is required.'
                    }
                },
                'refEmail': {
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Email must be properly formatted",
                        'label': "lbl_validation_emailFormat"
                    }
                },
                'familyEmail': {
                    'required': {
                        'message': 'Family Head Email is required'
                    },
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Family Head Email must be properly formatted."
                    }
                },
                'secretQuestion': {
                    'required': {
                        'message': 'Secret Question is required.'
                    }
                },
                'secretAnswer': {
                    'required': {
                        'message': 'Secret Answer is required.'
                    }
                },
                'alternativeEmailId': {
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Alternative Email must be properly formatted."
                    }
                },
                'loyaltyAccountNo':{
                    'required': {
                        'message': 'Account Number is required.'
                    },
                    'size': {
                        'min': 12,
                        'max': 12,
                        'message': 'Account number should contains 12 digits.'
                    }
                },
                'mashreqDOB': {
                    'required': {
                        'message': 'Date of Birth is required.',
                        'label': "lbl_validation_dobRequired"
                    }
                },
                'city': {
                    'required': {
                        'message': 'City is required.'
                    }
                }
            },
            'SignIn': {
                'password': {
                    'required': {
                        'message': 'Password is required.'
                    }
                },
                'email': {
                    'required': {
                        'message': "Email is required"
                    },
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Email must be properly formatted"
                    }
                }
            },
            'forgetPassword': {
                'forgetEmail': {
                    'required': {
                        'message': "Email is required"
                    },
                    'pattern': {
                        'value': "/^.*@.*\\..+/",
                        'message': "Email must be properly formatted"
                    }
                },
                'captcha' : {
                    'required': {
                        'message': "Captcha is required"
                    }
                }
            },
            'ModSearch': {
                'search-to': {
                    'required': {
                        'message': 'Destination is required.'
                    }
                },
                'search-from': {
                    'required': {
                        'message': 'Arrival is required.'
                    }
                },
                'search-departure-date': {
                    'required': {
                        'message': 'Departure date is required.'
                    }
                },
                'search-arrival-date': {
                    'required': {
                        'message': 'Arrival date is required.'
                    }
                    // ,
                    // 'pattern': {
                    //     'value': '/^[^N]+$/',
                    //     'message': 'Valid arrival date is required'
                    // }
                }
            },
            'ModificationFlightSearch' : {
                'search_from': {
                    'required': {
                        'message': 'Destination is required.'
                    }
                },
                'search_to': {
                    'required': {
                        'message': 'Arrival is required.'
                    }
                },
                'search_departure_date': {
                    'required': {
                        'message': 'Departure date is required'
                    }
                }
            }

        });
    }]);
