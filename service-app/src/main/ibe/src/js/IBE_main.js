/**
 * Created by indika on 10/22/15.
 */
(function (angular) {
    var IBEMod = angular.module("IBE", [
        'LocalStorageModule',
        'ui.router',
        'ui.bootstrap',
        'ui.mask',
        'ngAnimate',
        'ngSanitize',
        'pascalprecht.translate',
        'ngTouch',
        'ngResource',
        'ngMessages',
        'isaApp',
        'ibeRemoteService',
        'travelFare',
        'multiCity',
        'passenger',
        'extras',
        'payment',
        'confirm',
        'rzModule',
        'valdr',
        'signIn',
        'viewReservations',
        'voucher',
        "viewCreditHistory",
        'modification.common',
        'modification.dashboard',
        'modification.reservation',
        'modification.flight',
        'vcRecaptcha',
        'modification.ancillary',
        'registration',
        'loginBar',
        'popup',
        'viewFamilyFriends',
        'viewSeatMeal',
        'ui.date',
        'themEngin',
        'colorpicker.module',
        'reProtect'
    ]);

    IBEMod.constant("ConnectionProps", connectionParams);
    IBEMod.constant("FrontendProps", frontEndParams);
    IBEMod.constant("appConstants", {
        ancillaryImageClasses: {
            INSURANCE: "insurance-image",
            SEAT: "seat-image",
            MEAL: "meal-image",
            AUTOMATIC_CHECKIN: "automatic-checkin-image",
            BAGGAGE: "baggage-image",
            SSR_AIRPORT: "ssr-airport-image",
            SSR_IN_FLIGHT: "ssr-flight-image",
            FLEXIBILITY: "flexi-fare-image",
            DEFAULT: "default-ancillary-image"
        }
    });
    IBEMod.provider('configsService', function () {

        var options = {
            configs: null,
            airline: null
        };

        this.config = function (opt) {
            angular.extend(options, opt);
        };

        this.$get = [function () {
                if (!options.configs) {
                    throw new Error('configs not loaded');
                }
                var svc = {
                    clientConfigs: options
                };

                return svc;
            }];
    });

    $.get('./client.json', function (data) {
        angular.module('IBE').config(['configsServiceProvider', function (configsServiceProvider) {
                configsServiceProvider.config({
                    configs: data.clientConfigs,
                    airline: data.airline,
                    airlines: data.airlines
                });
            }]);
        fetchData();
    }, 'json');

    //Fetch data for app_params from service side & common language translations
    function fetchData() {
        //------------------ AERO-1485 26/01/17----------------
        var pattern = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/g;
        var isValidIP = pattern.test(document.location.hostname);
        if (document.location.hostname != "localhost") {
            if (!isValidIP) {
                if (window.location.protocol != "https:") {
                    var url = window.location.href;
                    var newUrl = url.replace('http:', 'https:');
                    window.location = newUrl;
                }
            }
        }
        //------------------------------------------------------
        var appConst = angular.module("constanceApp", []);
        appConst.constant("ConnectionProps", connectionParams);
        var masterData = {};
        var initInjector = angular.injector(["ng", "ibeRemoteService", "constanceApp"]);
        var $http = initInjector.get("$http");
        $http.defaults.headers.common['Accept-Language'] = 'en';
        var dummyMasterData = initInjector.get("masterRemoteService");
        dummyMasterData.retrieveCountries().then(function (response) {
            masterData.CountryList = response.data.countryList;
            bootstrapApplication(masterData);
        });
        dummyMasterData.retrieveExchangeRates().then(function (response) {
            masterData.ExchangeRates = response.data.currencyExRates;
            bootstrapApplication(masterData);
        });
        dummyMasterData.retrieveApplicationParams().then(function (response) {
            masterData.AppParams = response.data;
            bootstrapApplication(masterData);
        });
        dummyMasterData.retrieveCommonLists().then(function (response) {
            masterData.dropDownLists = response.data;
            bootstrapApplication(masterData);
        });
        IBEMod.constant("MasterData", masterData);
    }
    ;

    //bootstrapping the app
    function bootstrapApplication(masterData) {
    	var initInjector = angular.injector(["ng", "ibeRemoteService", "constanceApp"]);
        var $http = initInjector.get("$http");
        $http.defaults.headers.common['Accept-Language'] = 'en';
        var dummyMasterData = initInjector.get("masterRemoteService");

        if (masterData.CountryList !== undefined &&
                masterData.ExchangeRates !== undefined &&
                masterData.AppParams !== undefined &&
                masterData.dropDownLists !== undefined) {
            angular.element(document).ready(function () {
            	
            	try {
            		angular.bootstrap(document, ["IBE"]);
            	} catch (ex){
            		console.log(ex);
            	}

                // Injecting google tag manager
                try {
                	
                    //temporaly changing for testing
                    window.dataLayer = window.dataLayer || [];
                    $(window).on('hashchange', function(){
                    	
                      var googleTagMangerId = _.indexBy(masterData.AppParams.ibeParamKeyResponse, 'key'). googleTagManagerKey.value;
                    	var googleAnalyticsID = _.indexBy(masterData.AppParams.ibeParamKeyResponse, 'key'). googleAnalyticsKeyForServiceApp.value;

                    	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                        ga('create', googleAnalyticsID, 'auto');
                        ga('require', 'ecommerce');
                    	
                    	
                        var page = (!!location.hash) ? location.hash.split('/')[1] : null;
                        var extras = (!!location.hash && location.hash.split('/')[1]=='extras' &&location.hash.split('/')[2].length>2) ? '-'+location.hash.split('/')[2] : null;
                        var flights = (!!location.hash && location.hash.split('/')[1]=='flights' &&location.hash.split('/')[2].length>2) ? '-'+location.hash.split('/')[2] : null;
                        var pageMode = location.hash.split('/')[location.hash.split('/').length-1];
                        
                        if(!!extras) {
                        	page = page +extras;
                        }
                        
                        var quoteData  = JSON.parse(sessionStorage.getItem('IBE.QUOTE_DETAILS'));
                        quoteData = (quoteData != '' && quoteData != null)?quoteData : JSON.parse(sessionStorage.getItem('IBE.QUOTE'));
                        var confirmFlight  = JSON.parse(sessionStorage.getItem('IBE.CONFIRMED_FLIGHT'));
                        var extraChages = JSON.parse(sessionStorage.getItem('IBE.extra-total'));
                        var pnr = JSON.parse(sessionStorage.getItem('IBE.PNR'));
                        var transactionID = JSON.parse(sessionStorage.getItem('IBE.TRANSACTION_ID'));
                        var ibelanguage = JSON.parse(sessionStorage.getItem('IBE.APP_LANGUAGE'));
                        var searchCriteria = JSON.parse(sessionStorage.getItem('IBE.SEARCH_CRITERIA'));
                        var statusCode = JSON.parse(sessionStorage.getItem('IBE.RES_STATUS'));
                        
                        if (pnr == null || pnr == undefined) {
                            pnr = JSON.parse(sessionStorage.getItem('IBE.ONHOLD_PNR'));
                            if (pnr != null) {
                                statusCode = (statusCode != null) ? statusCode : "PND";
                            }
                        }
                        if (pnr != null) {
                            statusCode = (statusCode != null) ? statusCode : "CNF";
                        }
                        
                        var totalWithAdminFee = JSON.parse(sessionStorage.getItem('IBE.GoogleAnalytics_TotalWithAdminFee'));
                        
                        
                        var ibeModifiReservationModel = JSON.parse(sessionStorage.getItem('IBE.MODIFY.RESERVATION.MODEL'));
                        var userDetails = JSON.parse(sessionStorage.getItem('IBE.USER_SIGN_IN'));
                        var selectedAnci = JSON.parse(sessionStorage.getItem('IBE.SELECTED_ANCI'));
                        var paymentOption = JSON.parse(sessionStorage.getItem('IBE.SELECTED_PAYMENT_OPTION'));
                        var appMode = JSON.parse(sessionStorage.getItem('IBE.APP_MODE'));
                        appMode = (appMode == null || appMode == '') ? JSON.parse(sessionStorage.getItem('IBE.appMode')) : appMode;
                        var userRegistration = JSON.parse(sessionStorage.getItem('IBE.USER_REGISTRATION_DETAILS'));
                        var paxDetails = JSON.parse(sessionStorage.getItem('IBE.PAX_DETAIL'));
                        var totalAllInclusive = JSON.parse(sessionStorage.getItem('IBE.TOTAL_ALL_INCLUSIVE'));

                        var isModify = false;
                        
                        if ((ibeModifiReservationModel != undefined && ibeModifiReservationModel != null) &&
                        		ibeModifiReservationModel.pnr == pnr){
                        	isModify = true;
                        	statusCode = (!!ibeModifiReservationModel.reservation) ? ibeModifiReservationModel.reservation.bookingDetails.statusCode : statusCode;
                        } else {
                        	isModify = false;
                        }
                        
                        var revanue = '';
                        
                        if (isModify){
                        	var previousPaidCharge = ibeModifiReservationModel.reservation.bookingDetails.amountPaid;
                        	var newlyPaidCharge = ( totalWithAdminFee != '' && totalWithAdminFee != null ) ? totalWithAdminFee : 0 ;
                        	var floatRevanue = parseFloat(previousPaidCharge) + parseFloat(newlyPaidCharge);
                        	revanue = (floatRevanue != 0.0 ) ? floatRevanue.toFixed(2).toString() : '';
                        }else{
                        	revanue = ( totalWithAdminFee != '' && totalWithAdminFee != null ) ? totalWithAdminFee : ( (!!quoteData) ? (quoteData.totalPrice + extraChages) : 0 ) ;
                        }
                        
                        
                        var totalTaxAndSurcharges = (!!quoteData) ? (quoteData.totalTaxAndSurcharges + extraChages) : 0;
                        var selectedCurrency = _.indexBy(masterData.AppParams.ibeParamKeyResponse, 'key').baseCurrency.value;
                        var careerCode = JSON.parse(sessionStorage.getItem('IBE.DEFAULT_CARRIER_CODE'));
                        //var careerCode = (!!confirmFlight && !!confirmFlight[0] && !!confirmFlight[0].flight) ? confirmFlight[0].flight.careerCode: null;
                        var from = (!!confirmFlight && !!confirmFlight[0] && !!confirmFlight[0].flight) ? confirmFlight[0].flight.originCode : null;
                        var to = (!!confirmFlight && !!confirmFlight[1] && !!confirmFlight[1].flight) ? confirmFlight[1].flight.originCode : null;
                        var rout = null;
                        if(isModify){
                        	from = (!!ibeModifiReservationModel.reservation.fareGroupWiseSegments) ? ibeModifiReservationModel.reservation.fareGroupWiseSegments[0].segments[0].flight.originCode : null;
                        	to = (!!ibeModifiReservationModel.reservation.fareGroupWiseSegments) ? ibeModifiReservationModel.reservation.fareGroupWiseSegments[0].segments[0].flight.destinationCode : null;
                        }
                        if(!!from){
                            if(!!to) {
                                rout = from + '-' + to
                            }else{
                                rout = confirmFlight[0].flight.originCode + '-' + confirmFlight[0].flight.destinationCode
                            }
                        }else{
                            rout = null;
                        }
                        
                        var outboundFare = (!!confirmFlight && !!confirmFlight[0] && !!confirmFlight[0].flight) ? confirmFlight[0].fare.fareType : null;
                        var inboundFare = (!!confirmFlight && !!confirmFlight[1] && !!confirmFlight[1].flight) ? confirmFlight[1].fare.fareType : null;
                        
                        if(isModify){
                        	outboundFare = (!!JSON.parse(sessionStorage.getItem('IBE.MODIFY.NEW.OUTBOUNDFARE'))) ? JSON.parse(sessionStorage.getItem('IBE.MODIFY.NEW.OUTBOUNDFARE')) : outboundFare;
                        	inboundFare = (!!JSON.parse(sessionStorage.getItem('IBE.MODIFY.NEW.INBOUNDFARE')))  ? JSON.parse(sessionStorage.getItem('IBE.MODIFY.NEW.INBOUNDFARE'))  : inboundFare;
                        }
                        
                        var selectedFare = null;
                        if(!!outboundFare){
                            if(!!inboundFare) {
//                            	selectedFare = '0_' + outboundFare + '/' + '1_' +inboundFare
                            	selectedFare = {'selectedFare':{'outboundFare': outboundFare,'inboundFare': inboundFare}}
                            }else{
//                            	selectedFare = '0_' + outboundFare 
                            	selectedFare = {'selectedFare':{'outboundFare': outboundFare}}
                            }
                        }else{
                        	selectedFare = null;
                        }
                        if (isModify){
                        	selectedCurrency = ibeModifiReservationModel.reservation.paymentDetails.currency;
                    	}
                        
                        var userEmail = null;
                        var userEmailMD5 = null;
                        var userCountry = null;
                        var userNationality = null;
                        var userMd5Email = null;
                        var userAccStatus = "N";
                        
                        if(!isEmptyObject(userDetails) && Object.keys(userDetails).length != 0 ){
                        	 userEmail = userDetails.loggedInCustomerDetails.emailId.toLowerCase();
                             userCountry = userDetails.loggedInCustomerDetails.country;
                             userNationality = userDetails.loggedInCustomerDetails.nationalityName;
                             userMd5Email = userDetails.loggedInCustomerDetails.md5EncryptedEmailId;
                             userAccStatus = "Y";   
                        }

                        var origin = null;
                        var	destination = null;
                        var	depDate = null;
                        var	retDate = null;
                        var	adultCount = null;
                        var	childCount = null;
                        var	infantCount = null;
                        var	promoCode = null;
                        var	language = null;
                        var currency = null;
                        var journeyType = null;
                        var originCity = 'N';
                        var destCity = 'N';
                        if(!isEmptyObject(searchCriteria) && Object.keys(searchCriteria).length != 0 ){
                        	origin = searchCriteria.from;
                        	destination = searchCriteria.to;
                        	depDate = searchCriteria.depDate;
                        	retDate = searchCriteria.retDate;
                        	adultCount = searchCriteria.adult;
                        	childCount = searchCriteria.child;
                        	infantCount = searchCriteria.infant;
                        	promoCode = searchCriteria.promoCode;
                        	language = searchCriteria.lang;
                        	currency = searchCriteria.currency;
                        	originCity = searchCriteria.originCity;
                        	destCity = searchCriteria.destCity;
                        }
                        
                        if(!!retDate){
                        	journeyType = (retDate == "N" && retDate.length < 2) ? "ONEWAY" : "RETURN";
                        }
                        var baggageSelected = false;
                        var seatSelected = false;
                        var insuranceSelected = false;
                        var mealSelected = false;
                        var flexibilitySelected = false;
                        var ssrAirportSelected = false;
                        var ssrInFlightSelected = false;
                        
                        if(selectedAnci != undefined && selectedAnci != null){
	                        for (var j = 0; j < selectedAnci.length; j++) {
	                        	switch(selectedAnci[j]) {
	                            case "BAGGAGE":
	                            	baggageSelected = true;
	                                break;
	                            case "SEAT":
	                            	seatSelected = true;
	                                break;
	                            case "INSURANCE":
	                            	insuranceSelected = true;
	                                break;
	                            case "MEAL":
	                            	mealSelected = true;
	                                break;
	                            case "FLEXIBILITY":
	                            	flexibilitySelected = true;
	                                break;
	                            case "SSR_AIRPORT":
	                            	ssrAirportSelected = true;
	                                break;
	                            case "SSR_IN_FLIGHT":
	                            	ssrInFlightSelected = true;
	                                break;
	                        	}
	                        }
                        }
                        
                        var regUserId = null;
                        var regUserName = null;
                        var regUserCountry = null;
                        var regUserNationality = null;
                        
                        if(!isEmptyObject(userRegistration) && Object.keys(userRegistration).length != 0 ){
                        	regUserId = userRegistration.customer.customerId;
                        	regUserName = userRegistration.customer.title +" "+ userRegistration.customer.firstName+" "+ userRegistration.customer.lastName;
                        	regUserCountry = userRegistration.customer.countryCode;
                        	regUserNationality =userRegistration.customer.nationalityName;
                        }
                        
                        appMode = ((appMode != null && appMode == 'modifyAnci') || (appMode == null && pageMode == 'modifyAnci' || pageMode == 'modify_segment'|| pageMode == 'nameChange' || pageMode == 'changeContactInformation' || page == 'modify')) ? 'modify' : appMode;
                        appMode = (appMode == null && (page == 'fare' || page == 'fare_city' || page == 'passenger'|| page == 'extras'|| page == 'payment' || page == 'reProtect'|| pageMode == 'create')) ? 'create' : appMode;

                        if(page == 'redirect' ){
                        	page = 'redirect_signIn' ;
                        }
                        
                        var userIpAddress = JSON.parse(sessionStorage.getItem('IBE.USER_IP_ADDRESS'));
                        
                        var itemUserDetails =  {'userDetails':{'language': ibelanguage,'emailId': userEmail,'userCountry': userCountry,'userNationality':userNationality,'userAccStatus':userAccStatus, 'userMd5EncryptedEmail':userMd5Email}}
                       
                        var itemSearch =  {'searchCriteria':{'origin':origin, 'originCity':originCity,'destination':destination, 'destCity':destCity ,'depDate': depDate,'retDate':retDate,'journeyType':journeyType,'adultCount':adultCount,'childCount':childCount,'infantCount':infantCount,
                        					'promoCode':promoCode,'language':language,'currency':currency}} 
                         
                        var itemAnci =  {'selectedAnci':{'baggage':baggageSelected ,'seat':seatSelected ,'insurance': insuranceSelected,'meal':mealSelected,'flexibility':flexibilitySelected,'ssrAirport':ssrAirportSelected,'ssrInFlight':ssrInFlightSelected}} 

                        var datalayerEcommerce = {'transaction':{'pnr': pnr, 'status': statusCode,'affiliation': careerCode,'transactionTotal': ((newlyPaidCharge == null || newlyPaidCharge == undefined) ? revanue : newlyPaidCharge),'baseCurrencyCode': selectedCurrency, 'paymentOption':paymentOption,
                        							'route':rout, 'appMode':appMode}}
                        
                        var registration = {'userRegistration':{'customerId' : regUserId, 'customerName' : regUserName, 'customerCountryCode' : regUserCountry, 'customerNationality' : regUserNationality}}
                        
                        dataLayer.push({'event': page,'currentPage': location.hash,'revenue':(!!totalAllInclusive) ? totalAllInclusive : revanue,'transactionId':pnr,'outboundFare':(!!outboundFare ? outboundFare : null) , 'inboundFare': (!!inboundFare ? inboundFare : null),'baseCurrencyCode':selectedCurrency, 'userIpAddress':userIpAddress, 'userCountry' : 
                        				(!!paxDetails  ? paxDetails.paxContact.country : userCountry), 'carrierCode':careerCode,'transactionProducts':[itemUserDetails,itemSearch,itemAnci],'userRegistration':registration,'ecommerce':datalayerEcommerce});

                        var strPnr = (pnr != null && pnr != undefined) ? pnr.toString() : '';
                        var strRevanue = (revanue != null && revanue != undefined) ? revanue.toString() : '';
                        var strSelectedCurrency = (selectedCurrency != null && selectedCurrency != undefined) ? selectedCurrency.toString() : '';
                        var strRoute = (rout != null && rout != undefined) ? rout.toString() : '';
                        var strCareerCode = (careerCode != null && careerCode != undefined) ? careerCode.toString() : '';
                        
                        ga('send', 'pageview', location.hash);
                        //ga('ecommerce:addItem', item);
                        
                        
                        if ((pnr != null && pnr != undefined) && 
                        		(revanue != null && revanue != undefined && revanue != '')){
                        	if (isModify){
                        		if (newlyPaidCharge > 0){
                        			// for analyse in log
                        			dummyMasterData.addGoogleAnalyticsLogs(strPnr, strRevanue, '', 'modify flow', location.hash, location.href, transactionID);
                        		}else{
                        			// for analyse in log
                            		dummyMasterData.addGoogleAnalyticsLogs(strPnr, strRevanue, '', 'newlypaidcharge > 0 clause skiping', location.hash, location.href, transactionID);
                        		}
                        	} else {
                        		// for analyse in log
                        		dummyMasterData.addGoogleAnalyticsLogs(strPnr, strRevanue, '', 'create flow', location.hash, location.href, transactionID);
                        		
                        	}
                        }else{
                        	// for analyse in log
                    		dummyMasterData.addGoogleAnalyticsLogs(strPnr, strRevanue, '', 'pnr revanue if clause skiping', location.hash, location.href, transactionID);
                        }
                        
                     // googleTagMangerId = "GTM-MLQVFBM";
                        //<!-- Google Tag Manager -->
                        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                        })(window,document,'script','dataLayer',googleTagMangerId);
                        //<!-- End Google Tag Manager -->


                    }).trigger('hashchange');

                }catch(ex){
                    console.log(ex);
                    try {
                    	// for analyse in log
                        dummyMasterData.addGoogleAnalyticsLogs('', '', 'GA exceptionOccured: ' + ex.message, '', '', '', '');
                    } catch (ex){
                    	console.log(ex);
                    }
                }
            });
        }
    }


    var ibeMainCtrl = new indexCtrl();
    var errorPage = new errorPageCtrl();
    var confrimPage = new confirmPageCtrl();
    var customerConfrimPage = new customerPageCtrl();

    var ideCommonServices = new commonServices();
    var remoteService = new RemoteService();

    IBEMod.run([
        '$rootScope', '$state', '$location', 'commonLocalStorageService', '$http', 'languageService', '$translate',
        'valdrMessage', 'loadingService', 'currencyService', 'constantFactory', 'applicationService', 'alertService',
        'popupService', 'voucherService',
        function ($rootScope, $state, $location, commonLocalStorageService, $http, languageService,
                  $translate, valdrMessage, loadingService, currencyService, constantFactory, applicationService,
                  alertService, popupService, voucherService) {
            $rootScope.$state = $state;
            $rootScope.$location = $location;
            $rootScope.pageLoaded = false;
            $rootScope.pageTransistion = false;
            var defaultCarrierCode = constantFactory.AppConfigStringValue("defaultCarrierCode");
            var originCountry = applicationService.getOriginCountry(defaultCarrierCode);
            commonLocalStorageService.setDefaultCarrierCode(defaultCarrierCode);
            commonLocalStorageService.setOriginCountry(originCountry);
            
            $.getJSON('//api.ipify.org?format=jsonp&callback=?', function(data) {
            		console.log(JSON.parse(JSON.stringify(data, null, 2)).ip);
            		commonLocalStorageService.setUserIpAddress(JSON.parse(JSON.stringify(data, null, 2)).ip);
            	});

            currencyService.setCurrencyTable(constantFactory.ExchangeRates());
            var mapSearchCriteria = function (arrC) {
                return {
                    adult: arrC.adult,
                    cabin: arrC.cabin,
                    child: arrC.child,
                    currency: arrC.currency,
                    depDate: arrC.depDate,
                    from: arrC.from,
                    to: arrC.to,
                    infant: arrC.infant,
                    lang: arrC.lang,
                    promoCode: arrC.promoCode,
                    retDate: arrC.retDate,
                    destCity: arrC.destCity,
                    originCity: arrC.originCity                   
                };
            };
            //applicationService.setOriginCountry();
            $rootScope.$on("$stateChangeStart", function (event, to, toP, from, fromP) {
                if (toP.lang == undefined) {
                    toP.lang = languageService.getSelectedLanguage();
                }
                if (toP.currency == undefined) {
                    toP.currency = currencyService.getSelectedCurrency();
                }
                if(to.name === 'passenger' && (from.name === 'fare' || from.name === 'fare_city' || from.name === 'extras' || from.name === 'payment')){
                    if(_.isNull(commonLocalStorageService.getConfirmedFlightData())){
                        event.preventDefault();
                        loadingService.hidePageLoading();
                        popupService.alert("Please select a flight and continue with 'Continue to Passenger Details' button.");
                        return;
                    }
                }

                if(!((to.name === 'voucherPayment' && from.name === 'voucher') ||
                    (from.name === 'voucherPayment' && to.name === 'voucher'))) {
                    voucherService.clearVoucherInformation();
                }

                $http.defaults.headers.common['Accept-Language'] = toP.lang.toLowerCase();
                languageService.setSelectedLanguage(toP.lang.toLowerCase());
                languageService.setSelectedPage(to.name);
                if(!commonLocalStorageService.getData('CurrencyToStore')){
                    currencyService.setSelectedCurrency(toP.currency);
                }else{
                    toP.currency = commonLocalStorageService.getData('CurrencyToStore');
                    currencyService.setSelectedCurrency(commonLocalStorageService.getData('CurrencyToStore'))
                }
                applicationService.setApplicationMode(toP.mode);
                $translate.use(languageService.getSelectedLanguage().toLowerCase());

                if (to.name === 'system_error') {
                    return false;
                }
                var isValideLang = _.findIndex(constantFactory.Langauges(), {languageCode: toP.lang.toLowerCase()});
                var isValideCurrency = _.findIndex(constantFactory.ExchangeRates(), {currencyCode: toP.currency});
                var isValideCountry = _.findIndex(constantFactory.Countries(), {code: originCountry});
                //To ignore the validation on "postPayment"
                if (to.name === 'postPayment' || from.name === "confirm") {
                    isValideCountry = 0;
                }

                //Fallback to 'en' in case some error messages are missing in other translations
                $translate.fallbackLanguage('en');

                if (isValideLang < 0) {
                    // If language is invalid set the error message in English
                    $translate.use('en').then(function () {
                        $translate('msg_selectFare_wrong_language_type').then(function (msg) {
                            var error = {
                                error: msg,
                                errorType: "OTHER_ERROR"
                            };
                            alertService.setPageAlertMessage(error);
                        });
                    });
                } else if (isValideCurrency < 0) {
                    $translate.use(languageService.getSelectedLanguage().toLowerCase()).then(
                            function () {
                                $translate('msg_selectFare_wrong_currency_type').then(
                                        function (msg) {
                                            var error = {
                                                error: msg,
                                                errorType: "OTHER_ERROR"
                                            };
                                            alertService.setPageAlertMessage(error);
                                        })
                            }
                    )
                } else if (isValideCountry < 0) {
                    $translate.use(languageService.getSelectedLanguage().toLowerCase()).then(function () {
                        $translate('msg_selectFare_wrong_origin_country').then(function (msg) {
                            if(to.name === 'fare' || to.name === 'fare_city') {
                                var error = {
                                    error: msg,
                                    errorType: "OTHER_ERROR"
                                };
                                alertService.setPageAlertMessage(error);
                            }
                        })
                    });
                } else {

                    /*Exclude shwoing view transition loading animation for following states*/
                    var excludeLoading = [
                        'modifyFlightHome.flightHome',
                        'modifyFlightHome.flightSelected',
                        'modifyFlightHome.flightSearch',
                        'modifyFlightHome.changedFlight',
                        'modifyFlightHome.flightSearchResult'];

                    if (excludeLoading.indexOf(to.name) < 0) {
                        loadingService.showPageLoading();
                    }

                    // Set curret state name.
                    applicationService.setCurrentState(to.name);

                    if (to.name === 'fare' || to.name === 'fare_city') {
                        commonLocalStorageService.setSearchCriteria(mapSearchCriteria(toP));
                        commonLocalStorageService.setOriginCountry(toP.originCountry);
                    } else if (to.name === 'postPayment') {

                        if (!isEmptyObject($location.search())) {
                            commonLocalStorageService.setData("postMap", $location.search());
                            commonLocalStorageService.setData("postUrl", $location.url());
                        }

                        // TODO: get language and currency from LocalStorageModule.
                        location.hash = location.hash.split("?")[0];
                        //toP.lang = languageService.getSelectedLanguage();
                        //toP.currency = currencyService.getSelectedCurrency();

                    } else if (to.name === 'voucherPostPayment') {

                        if (!isEmptyObject($location.search())) {
                            commonLocalStorageService.setData("postMap", $location.search());
                            commonLocalStorageService.setData("postUrl", $location.url());
                        }

                        // TODO: get language and currency from LocalStorageModule.
                        location.hash = location.hash.split("?")[0];
                        //toP.lang = languageService.getSelectedLanguage();
                        //toP.currency = currencyService.getSelectedCurrency();

                    } else {
                        var sc = commonLocalStorageService.getSearchCriteria();
                        sc.lang = toP.lang.toLowerCase();
                        sc.currency = toP.currency;
                        commonLocalStorageService.setSearchCriteria(sc);
                        if (toP.originCountry) {
                            commonLocalStorageService.setOriginCountry(toP.originCountry);
                        }
                    }
                    // Avoid navigation on other routes
                    if (from.name === "confirm_onhold" || from.name === "confirm") {

                        //TODO: optimize the logic.

                        // Allow a logged in user to navigate to modifyDashboard from the confirmation page
                        // Allow users to navigate back to modify reservation page when in modification flow
                        if (!((from.name === "confirm" || from.name === "confirm_onhold") && to.name === "modifyDashboard" && applicationService.getCustomerLoggedState()) && to.name !== 'modifyReservation') {
                            event.preventDefault();
                            loadingService.hidePageLoading();
                        }
                    }

                }

                // Store the previous state in order allow navigating back
                $rootScope.fromState = {
                    fromStateName: from.name,
                    fromParam: fromP
                };
            });

            $rootScope.$on('ON_CURRENCY_CHANGE', function (e, data) {
                var sc = commonLocalStorageService.getSearchCriteria();
                sc.currency = currencyService.getSelectedCurrency();
                commonLocalStorageService.setSearchCriteria(sc);
            });

            valdrMessage.angularMessagesEnabled = true;

            valdrMessage.addMessages({
                'equal': "The fields must match",
                'date': "Must be a correctly formatted date",
                'editable': "Please select an option from the list"
            })

        }]);

    function isEmptyObject(obj) {
        for (var currentObject in obj) {
            return false;
        }
        return true;
    }

    IBEMod.controller(ibeMainCtrl);
    IBEMod.controller(errorPage);
    IBEMod.controller(confrimPage);
    IBEMod.controller(customerConfrimPage);

    IBEMod.service(ideCommonServices);
    IBEMod.service(remoteService);

    IBEMod.factory('customValidator', [function () {
            return {
                name: 'ageConstraints',
                validate: function (value, args) {
                    var minimumAge = args.over;
                    var maxAge = args.under;

                    var givenDate = moment(value);
                    var todayDate = moment();
                    var diffYears = todayDate.diff(givenDate, 'years');

                    if (givenDate > todayDate) {
                        return false;
                    } else if (minimumAge && diffYears < minimumAge) {
                        return false;
                    } else if (maxAge && diffYears > maxAge) {
                        return false;
                    } else {
                        return true;
                    }
                }
            };
        }]);

    IBEMod.factory('asyncLanguageLoader', [
        '$q', '$http', '$location', 'languageService', '$timeout', 'loadingService', '$state',
        function ($q, $http, $location, languageService, $timeout, loadingService, $state) {
            return function (options) {
                var deferred = $q.defer(),
                        translations = null,
                        lang;


                if (options && options.key) {
                    lang = options.key;
                } else {
                    lang = languageService.getSelectedLanguage().toLowerCase();
                }

                var callCommon = {
                    "url": "i18n/" + lang + "/labels.json",
                    "method": "GET"
                };
                var overrideLang = {
                    "url": "i18n/" + lang + "/override.json",
                    "method": "GET"
                };

                $http(callCommon).then(function (results) {

                    $http(overrideLang).then(function (overrideResults) {

                        translations = results.data;

                        // Replace content with override resutls.
                        angular.extend(translations, overrideResults.data);

                        $timeout(function () {
                            deferred.resolve(translations);
                            if ($state.current.name !== 'postPayment') {
                                loadingService.hidePageLoading();
                            }
                        }, 500)

                    }, function (overrideResults) {

                    });

                },
                        function (results) {
                            $timeout(function () {
                            }, 500)
                        });

                return deferred.promise;
            };
       }]);

    IBEMod.factory('constantFactory', ['MasterData', function (MasterData) {
            var Countries = function () {
                return getCountries();
            };
            var Nationality = function () {
                return getNationalities();
            }
            var ExchangeRates = function () {
                return MasterData.ExchangeRates;
            };
            var AppConfigs = function () {
                return MasterData.AppParams.ibeParamKeyResponse;
            };

            var getCountries = function () {
                var returnArr = [];
                angular.forEach(MasterData.CountryList, function (country, key) {
                    var dummy = {};
                    dummy.name = country.countryName;
                    dummy.code = country.countryCode;
                    dummy.phoneCode = country.phoneCode;
                    returnArr[returnArr.length] = dummy;
                });
                return returnArr;
            };

            var PaxTitles = function () {
                return MasterData.dropDownLists.paxTitels;
            };

            var PaxTypeWiseTitle = function () {
                return MasterData.dropDownLists.paxTypeWiseTitles;
            };

            var LangaugeLists = function () {
                return MasterData.dropDownLists.languages;
            };
            var getNationalities = function () {
                var returnArr = [];
                angular.forEach(MasterData.CountryList, function (country, key) {
                    var dummy = {};
                    dummy.name = country.nationalityDes;
                    dummy.code = country.nationalityCode;
                    returnArr[returnArr.length] = dummy;
                });
                return returnArr;
            };
            // Get weight unit for the site.
            var getWeightUnit = function () {
                return "";
            };

            var getAppConfigValue = function (appConfigKey) {
                var val = false;
                angular.forEach(MasterData.AppParams.ibeParamKeyResponse, function (appCongfig, key) {
                    if (appCongfig.key === appConfigKey) {
                        val = (appCongfig.value === "true");
                    }
                });

                return val;
            };
            var getAppConfigStringValue = function (appConfigKey) {
                var val = false;
                angular.forEach(MasterData.AppParams.ibeParamKeyResponse, function (appCongfig, key) {
                    if (appCongfig.key === appConfigKey) {
                        val = appCongfig.value;
                    }
                });

                return val;
            };
                
            var CountryWiseStates = function (){
                return MasterData.dropDownLists.countryWiseStates;
            };
            
            return {
                AppConfigs: function () {
                    return AppConfigs();
                },
                Countries: function () {
                    return new Countries();
                },
                Nationalities: function () {
                    return new Nationality();
                },
                ExchangeRates: function () {
                    return ExchangeRates();
                },
                PaxTitles: function () {
                    return PaxTitles();
                },
                PaxTypeWiseTitle: function () {
                    return PaxTypeWiseTitle();
                },
                Langauges: function () {
                    return LangaugeLists();
                },
                AppConfigValue: function (appConfigKey) {
                    return getAppConfigValue(appConfigKey);
                },
                AppConfigStringValue: function (appConfigKey) {
                    return getAppConfigStringValue(appConfigKey);
                },
                WeightUnit: getWeightUnit(),
                defaultWeight: function () {
                    return "10 Kg"
                },
                CountryWiseStates: function () {
                    return CountryWiseStates();
                },
                PrefSeatTypeList : function(){
              	  return new PrefSeatTypeList();
                }

            }
        }]);
})(angular);


