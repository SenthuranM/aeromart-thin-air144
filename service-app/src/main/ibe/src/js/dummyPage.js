var appDummy;

(function () {
    appDummy = angular.module('appDummy', [
        'ui.bootstrap',
        'ngAnimate',
        'ngTouch'
    ]);
    appDummy.service('dummyService', [
        '$http',
        function ($http) {
            var _searchCriteria = {};
            var  _originCountry = '';
            this.getAirports = function () {
                return (typeof airports != 'undefined') ? airports:[{ code:'',en:''}];
            };
            this.ondDataMap = [];
            this.originList=[];
            this.getToAirports= function(){
                if(typeof origins != 'undefined') {
                    for (var originIndex = 0; originIndex < origins.length; originIndex++) {

                        var tempDestArray = [];
                        for (var destIndex in origins[originIndex]) {
                            // check whether operating carrier exists
                            var curDest = origins[originIndex][destIndex];

                            if (curDest[0] != null) {
                                var currentSize = tempDestArray.length;
                                tempDestArray[currentSize] = [];
                                tempDestArray[currentSize] = airports[curDest[0]];
                            }
                        }
                        if (tempDestArray.length > 0) {
                            this.ondDataMap[airports[originIndex]['code']] = tempDestArray;

                        }
                    }
                }

                return this.ondDataMap;// airports.concat(origins);
            }
            this.setSearchCriteria = function(sc){
                _searchCriteria=sc
            };
            this.setOriginCountry = function(origin){
                _originCountry = origin;
            };
            this.getUrlParams = function(){
                var strParams = "";
                var strCitySearchParams = "";
                var isCitySearch = false;
                
                angular.forEach(_searchCriteria, function (obj, i) {
                    if (i == 'from' || i == 'to') {
                        strParams += "/" + obj.code;
                        if (obj.city != undefined) {
                        	if(obj.city){
                        		isCitySearch = true;
                        	}
                        	strCitySearchParams += "/" + (obj.city ? "Y" : "N")                        	
                        } else {
                        	strCitySearchParams += "/N"
                        }
                    } else if (i == "departureDate" || i == "returnDate") {
                        if (obj == "") {
                            strParams += "/N"
                        } else {
                            strParams += "/" + moment(new Date(obj)).format("DD-MM-YYYY");
                        }
                    } else {
                        strParams += "/" + obj;
                    }

                });
                
                if(isCitySearch){
                	strParams = strParams+strCitySearchParams;
                }
                
                return strParams;
            };
        }]);
    appDummy.service('datePickerService', [function () {
        var datePicker = {};

        datePicker.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        var dateformats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd'];

        datePicker.today = function () {
            return new Date();
        }
        datePicker.clearDate = function (date) {
            date = null;
            return date;
        }
        datePicker.disabled = function (date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        datePicker.toggleMin = function (mindate) {
            return mindate ? null : new Date();
        };
        datePicker.setDate = function (yy, mm, dd) {
            return new Date(yy, mm, dd);

        }
        datePicker.setDateFormat = function (n) {
            return dateformats[n]
        }
        datePicker.open = function ($event) {
            return true;
        };
        return datePicker;
    }])
    appDummy.controller('dummyCtrl', [
        'dummyService',
        'datePickerService',
        '$window',
        '$filter',
        '$rootScope',
        function (dummyService, datePickerService, $window, $filter, $rootScope) {
            scope = $rootScope.$new();
            this.tripType = 'return';
            this.paxADArray = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
            this.paxCHArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            this.paxINArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            this.currency = (typeof currencies != 'undefined') ? currencies: [{ code:'USD',en:'US Dollar'}] ;
            this.origins = (typeof origins != 'undefined') ? origins : [];
            this.countryList = CountryList;
            this.cabins = {
                'Y': 'Economy Class',
                'C': 'Business Class'
            };
            this.languages = {
                'en': 'English',
                'ar': 'Arabic',
                'es': 'Spanish',
                'fr': 'French',
                'ru': 'Russian',
                'tr': 'Turkish',
                'it': 'Italian',
                'fa': 'Persian',
                'de': 'German',
                'km': 'Khmer',
                'zh': 'Chinese',
                'hy': 'Armenian'
            };
            this.ORIGIN_COUNTRY={
                "E5" : "EG",
                "G9" : "AE",
                "9P" :"JO",
                "3O" : "MA",
                "C7" : "LK",
                "W5" : "IR",
                "Q9" : "KW",
                "QD" : "KH",
                "RM" : "AM",
                "6Q" : "KW",
                "RI" : "LY"
            };
            this.maxPaxCount = 9;
            this.dateformat = datePickerService.setDateFormat(1);
            this.airports = dummyService.getAirports();
            this.toAirports = dummyService.getToAirports();
            this.desAirports = this.airports;
           // this.countryList = constantFactory.Countries();

            //search criteria params
            this.searchCriteria = {
                "language": 'en',
                "currency": 'AED',
                "originCountry" :'AE',
                "from": "",
                "to": "",
                "departureDate": "",
                "returnDate": "",
                "adults": 1,
                "children": 0,
                "infants": 0,
                "cabinClass": "Y",
                "promoCode": ""
            };

            this.manage = {
                dateformat: datePickerService.setDateFormat(4),
                searchParam: {
                    departureDate: "",
                    pnr: "",
                    lastName: ""
                },
                dpOnClick: function ($event, opened) {
                    this['departureDateOpened'] = datePickerService.open();
                }
            };

            this.getAppParamValue = function(paramKey){
                var val = false;
                for (var i=0;i<AppParams.length;i++){
                    if(AppParams[i].key === paramKey){
                        val = AppParams[i].value ;
                    }
                }
                return val;
            };


            var self = this;
            self.searchCriteria.currency = self.getAppParamValue('baseCurrency');
            self.searchCriteria.originCountry = this.ORIGIN_COUNTRY[self.getAppParamValue('defaultCarrierCode')];
            this.dpOnClick = function ($event, opened) {
                this[opened] = datePickerService.open();
            };

            this.changePax = function (type) {
                var tempPax = [], tempINF = [], cc = 0;
                for (var i = parseInt(self.searchCriteria[type], 10); i <= self.maxPaxCount; i++) {
                    tempPax[tempPax.length] = cc;
                    cc++;
                }
                for (var j = 0; j <= parseInt(self.searchCriteria[type], 10); j++) {
                    tempINF[tempINF.length] = j;
                }

                if (type === "adults") {
                    self.paxCHArray = tempPax;
                    self.paxINArray = tempINF;
                } else {
                    self.paxADArray = tempPax;
                }

            };

            this.setOneWayReturn = function (flg) {
                if (flg === "one_way") {
                    self.searchForm.txtReturnDate.$valid = true;
                    self.searchForm.txtReturnDate.$error.required = false;
                } else {
                    self.searchForm.txtReturnDate.$valid = false;
                    self.searchForm.txtReturnDate.$error.required = true;
                }

            };

            this.onSubmit = function (event) {
                if (self.searchForm.$valid) {
                    var appURL = "reservation.html#/fare";
                    if (typeof self.userToken != 'undefined') {
                    	document.cookie = "userToken=" + self.userToken;
                    	document.cookie = "flag=search";
                    }
                    if(self.searchCriteria.returnDate){
                        var departureDate= moment(self.searchCriteria.departureDate).format('YYYY-MM-DD');
                        var returnDate = moment(self.searchCriteria.returnDate).format('YYYY-MM-DD');
                        if(moment(returnDate).isAfter(departureDate) || moment(returnDate).isSame(departureDate)){
                            dummyService.setSearchCriteria(self.searchCriteria);
                            appURL += dummyService.getUrlParams();
                            event.preventDefault();
                            $window.location.href = appURL;
                        }
                        else{
                            alert("Your return flight date cannot be prior to your departure flight date. Please select a future return flight or change the dates.")
                        }
                    }
                    else{
                        dummyService.setSearchCriteria(self.searchCriteria);
                        appURL += dummyService.getUrlParams();
                        event.preventDefault();
                        $window.location.href = appURL;
                    }
                }
            }

            this.findReservation = function (event) {
                if (self.manageBooking.$valid) {
                    var appURL = "reservation.html#/modify/reservation/"+self.searchCriteria.language+"/"+ self.searchCriteria.currency + "/" + self.searchCriteria.originCountry + "/";
                    appURL += (this.manage.searchParam.pnr + "/" + this.manage.searchParam.lastName + "/" + $filter('date')(this.manage.searchParam.departureDate, datePickerService.setDateFormat(4)));
                    event.preventDefault();
                    $window.location.href = appURL;
                    $window.localStorage.setItem('PNR_LOGIN', true);
                }else{
                	$window.localStorage.removeItem('PNR_LOGIN');
                }
            };

            this.onPassengerLogIn = function () {
                event.preventDefault();
                $window.location.href = "reservation.html#/modify/dashboard/EN/AED";
            };

            this.redirectToSignIn = function (event) {
                event.preventDefault();
                $window.location.href = "reservation.html#/signIn/"+self.searchCriteria.language+"/"+self.searchCriteria.currency+"/"+self.searchCriteria.originCountry;
            };

            this.redirectToRegistration = function (event) {
                event.preventDefault();
                $window.location.href = "reservation.html#/registration/" + self.searchCriteria.language + "/"+self.searchCriteria.currency+"/"+self.searchCriteria.originCountry;
            };
            
            this.redirectToGiftVoucher = function (event) {
                event.preventDefault();
                $window.location.href = "reservation.html#/voucher/EN/"+self.searchCriteria.currency+"/"+self.searchCriteria.originCountry;
            };

            this.filterDesAirport = function(){
               self.desAirports = self.toAirports[self.searchCriteria.from.code]
            };

            this.redirectToMultiCity = function (event) {
                event.preventDefault();
                $window.location.href = "reservation.html#/multi-city/" + self.searchCriteria.language + "/"+self.searchCriteria.currency+"/"+self.searchCriteria.originCountry;
            };
            
            this.redirectToUserTokenLogin = function (event) {
                event.preventDefault();
                if (typeof self.userTokenLogin != 'undefined') {
                	document.cookie = "userToken=" + self.userTokenLogin;
                	document.cookie = "flag=userTokenLogin";
                }
                $window.location.href = "reservation.html#/modify/dashboard/EN/AED";                     
            };
            
            self.changePax('adults');
    }]);

})();
