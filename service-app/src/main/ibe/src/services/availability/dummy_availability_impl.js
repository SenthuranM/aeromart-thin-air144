(function(angular) {
  var app = angular.module("ibeDummyService");
  var sectorSequenceNo = 0;
  var fareSequenceNo = 0;

  app.service('dummyAvailability', ['$http',function($http) {
    this._basePath = '';
    this.calendarFareSearch = function(availRQ, successFn, errorFn) {
      console.log('inside avail search');
      var __$this = this;
      this.retrieveFareTimeline(availRQ, function(timelineResponse) {
        __$this.retrieveFlightFare({}, function(fareResponse) {
          var data = angular.extend({}, timelineResponse, fareResponse);
          successFn(data);
        }, function(fareError) {
          errorFn();
        })
      }, function(errorResponse) {
        errorFn();
      });

    },

    this.retrieveFareTimeline = function(availRQ, successFn, errorFn) {

      var path = "sampleData/timeline_" + sectorSequenceNo + ".json";
      var retFn = function() {
        this.success = function(response) {

        }, this.error = function(error) {

        }
      }
      $http({
        "method": 'GET',
        "url": this._basePath + path,
        "data": {}
      }).success(function(response) {
        var data = {};
        data = {
          'timelineInfo': response.data.results,
          'fareClasses': response.data.fareClasses
        };
        sectorSequenceNo++;
        successFn(data);
      }).error(function(response) {
        errorFn();
        console.log(response);
      });
    },
    this.retrieveReturnFareTimeline = function(availRQ, successFn, errorFn) {

            var path = "sampleData/timeline_" + sectorSequenceNo + ".json";
            var retFn = function() {
                this.success = function(response) {

                }, this.error = function(error) {

                }
            }
            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {}
            }).success(function(response) {
                var data = {};
                data = {
                    'timelineInfo': response.data.results,
                    'fareClasses': response.data.fareClasses
                };
                sectorSequenceNo++;
                successFn(data);
            }).error(function(response) {
                errorFn();
                console.log(response);
            });
    },

    this.retrieveFlightFare = function(availRQ, successFn, errorFn) {

      var path = 'sampleData/flightFare_' + fareSequenceNo + '.json';
      var returndvalue = {};
      $http({
        "method": 'GET',
        "url": this._basePath + path,
        "data": {}
      }).success(function(response) {
        var data = {
          'ondInfo': response.data
        };
        fareSequenceNo++;
        successFn(data);
      }).error(function(response) {
        errorFn();
      });

    }
    this.retrieveNextPrevTimeLineData = function(availRQ, successFn, errorFn) {
      var path = 'sampleData/timeline1.json';
      $http({
        "method": 'GET',
        "url": this._basePath + path,
        "data": {}
      }).success(function(response) {
        response.data.date = availRQ.departureDate;
        var data = {};
        data = {
          'timelineInfo': response.data.results
        };
        successFn(data);
      }).error(function(response) {
        errorFn();
        console.log(response);
      });
    }
    this.retrieveFareQuoteData = function(availRQ, successFn, errorFn) {
      var path = 'sampleData/fareQuoteResponse.json';
      $http({
        "method": 'GET',
        "url": this._basePath + path,
        "data": {}
      }).success(function(response) {
        successFn(response);
      }).error(function(response) {
        errorFn();
        console.log(response);
      });
    }

  }]);

})(angular);