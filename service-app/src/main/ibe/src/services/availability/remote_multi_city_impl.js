(function(angular) {
    var app = angular.module('ibeAPIService');

    app.service('remoteMultiCityAvailability', 
    ['$http',
    'ConnectionProps',
    'alertService', 
    function($http, ConnectionProps,alertService) {
        var self = this;
        self._basePath = ConnectionProps.host;

        self.retrieveMultiCityAvailability = function(availRQ, successFn, errorFn) {
            var path = "/availability/search/flight/calendar";
            
            $http({
                method: 'POST',
                url: self._basePath + path,
                data: availRQ
            }).success(function(response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });
        };

        self.multiCityFareSearch = function(availRQ, successFn, errorFn) {
            var path = "/availability/search/fare/flight/calendar";
            
            $http({
                method: 'POST',
                url: self._basePath + path,
                data: availRQ
            }).success(function(response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });
        };

        self.multiCityFareQuote = function(fareRQ, successFn, errorFn) {
            var path = "/availability/selected/pricing/minimum";

            $http({
                method: 'POST',
                url: self._basePath + path,
                data: fareRQ
            }).success(function(response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });
        }
    }]);
})(angular);
