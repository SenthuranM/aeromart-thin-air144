(function(angular) {
    var app = angular.module("ibeAPIAdaptor");

    app
        .service(
        'ancillaryAdaptor',["$injector",
            function($injector) {

                this.getAvailableAncillary = function (availableAncillaryRs) {

                    var anciTypeIndex;
                    var availUnitIndex;

                    var anciType;
                    var availUnit;
                    var items;

                    var transformedAvailableAncillary = [];
                    var transformedAnciScope;

                    for (anciTypeIndex in availableAncillaryRs.ancillaries) {

                        anciType = availableAncillaryRs.ancillaries[anciTypeIndex];

                        transformedAvailableAncillary[anciType.ancillaryType] = [];

                        if(angular.equals(anciType.ancillaryType, "INSURANCE")){

                            var transformedAvailableAncillaryProviders = [];

                            for(var provider in anciType.providers){

                                var insuranceProvider = anciType.providers[provider];

                                var transformedAvailableAncillaryProvider = {};
                                transformedAvailableAncillaryProvider.provider = insuranceProvider.providerDescription;
                                transformedAvailableAncillaryProvider.planGroups = [];


                                var availUnitArray = insuranceProvider.availableAncillaries.availableUnits;
                                var validationDefinitionsArray = insuranceProvider.availableAncillaries.validationDefinitions;

                                var transformedAvailUnitArray = [];

                                for(availUnitIndex in availUnitArray){

                                    availUnit = availUnitArray[availUnitIndex];
                                    items = availUnit.itemsGroup.items;

                                    transformedAnciScope = {};
                                    transformedAnciScope.scope = availUnit.scope;
                                    transformedAnciScope.applicability = availUnit.applicability;
                                    transformedAnciScope.items = toInsuranceModel(items);
                                    transformedAvailUnitArray.push(transformedAnciScope);
                                }

                                var planGroups = [];

                                for(var ruleIndex in validationDefinitionsArray[0].insurancePlanGroups){
                                    var insuranceRule = validationDefinitionsArray[0].insurancePlanGroups[ruleIndex];
                                    var planGroup = {};
                                    var validation = {};
                                    validation.maxSelections = insuranceRule.maxSelections;
                                    validation.minSelections = insuranceRule.minSelections;
                                    planGroup.validation = validation;
                                    planGroup.plans = [];

                                    for(var insuranceScopeItemIndex in transformedAvailUnitArray){
                                        var insuranceScopeItem = transformedAvailUnitArray[insuranceScopeItemIndex];
                                        for(var insurnaceItemIndex in insuranceScopeItem.items){
                                            var insuranceItem = insuranceScopeItem.items[insurnaceItemIndex];
                                            if(insuranceRule.itemIds.indexOf(insuranceItem.policyID) > -1){
                                                planGroup.plans.push(insuranceItem);
                                            }
                                        }
                                    }
                                    planGroups.push(planGroup);
                                }
                                transformedAvailableAncillaryProvider.planGroups = planGroups;
                                transformedAvailableAncillaryProviders.push(transformedAvailableAncillaryProvider);
                            }
                            transformedAvailableAncillary[anciType.ancillaryType].push(transformedAvailableAncillaryProviders);

                        } else if(angular.equals(anciType.ancillaryType, "AIRPORT_TRANSFER")){
                            for (var provider in anciType.providers) {
                                for (availUnitIndex in anciType.providers[provider].availableAncillaries.availableUnits) {
                                    availUnit = anciType.providers[provider].availableAncillaries.availableUnits[availUnitIndex];
                                    items = availUnit.itemsGroup.items;

                                    transformedAnciScope = {};
                                    transformedAnciScope.scope = availUnit.scope;
                                    transformedAnciScope.applicability = availUnit.applicability;

                                    if (angular.equals(anciType.ancillaryType, "AIRPORT_TRANSFER")) {
                                        transformedAnciScope.validationDefinitions = anciType.providers[provider].availableAncillaries.validationDefinitions;
                                        transformedAnciScope.items = toAirportTransferModel(items, anciType.providers[provider]);
                                    }

                                    transformedAvailableAncillary[anciType.ancillaryType].push(transformedAnciScope);
                                }
                            }

                        } else {
                            for (availUnitIndex in anciType.providers[0].availableAncillaries.availableUnits) {

                                availUnit = anciType.providers[0].availableAncillaries.availableUnits[availUnitIndex];
                                items = availUnit.itemsGroup.items;

                                transformedAnciScope = {};
                                transformedAnciScope.scope = availUnit.scope;
                                transformedAnciScope.applicability = availUnit.applicability;


                                if (angular.equals(anciType.ancillaryType, "BAGGAGE")) {
                                    transformedAnciScope.items = toBaggageModel(items);
                                } else if (angular.equals(anciType.ancillaryType, "MEAL")) {
                                    transformedAnciScope.validationDefinitions = anciType.providers[0].availableAncillaries.validationDefinitions;
                                    transformedAnciScope.items = toMealModel(items);
                                } else if (angular.equals(anciType.ancillaryType, "SEAT")) {
                                    transformedAnciScope.validationDefinitions = anciType.providers[0].availableAncillaries.validationDefinitions;
                                    transformedAnciScope.items = toSeatModel(items,transformedAnciScope.scope );
                                } else if (angular.equals(anciType.ancillaryType, "SSR_IN_FLIGHT")) {
                                    //Only add push SSR segment if it contains valid items.
                                    if(items.length === 0){ continue }
                                    transformedAnciScope.items = toSsrInFlightModel(items);
                                } else if (angular.equals(anciType.ancillaryType, "SSR_AIRPORT")) {
                                    transformedAnciScope.items = toSsrAirportModel(items);
                                } else if (angular.equals(anciType.ancillaryType, "AIRPORT_TRANSFER")) {
                                    transformedAnciScope.items = toAirportTransferModel(items);
                                } else if (angular.equals(anciType.ancillaryType, "FLEXIBILITY")) {
                                    transformedAnciScope.items = toFlexibilityModel(items);
                                } else if (angular.equals(anciType.ancillaryType, "AUTOMATIC_CHECKIN")){
                                    transformedAnciScope.items = toAutoCheckinModel(items);
                                }

                                transformedAvailableAncillary[anciType.ancillaryType].push(transformedAnciScope);
                            }
                        }

                    }

                    return transformedAvailableAncillary;
                };

                var toAutoCheckinModel = function(items) {
                	var availableItemList = [];
                	for ( var item in items) {
                		var autoCheckinItem = {};
                		autoCheckinItem.autoCheckinId = items[item].itemId;
                		autoCheckinItem.price = items[item].chargePerPax.amount;
                		availableItemList.push(autoCheckinItem);
                	}
                	return availableItemList;
                };

                var toBaggageModel = function (items) {
                    var availableItemList = [];
                    for(var item in items){
                        var baggageItem = {};
                        baggageItem.baggageId = items[item].itemId;
                        baggageItem.weight = items[item].itemName;
                        baggageItem.price = items[item].charges[0].amount;
                        baggageItem.selected = items[item].defaultBaggage;
                        availableItemList.push(baggageItem);
                    }
                    return availableItemList;
                };
                
                var toMealModel = function (items) {
                    var availableItemList = [];
                    var item;
                    var mealItem;
                    for(var item in items){
                        mealItem = {};
                        mealItem.alertAutoCancellation = false;
                        mealItem.availableMeals = items[item].available;
                        mealItem.allocatedMeals = items[item].allocated;
                        mealItem.mealCategoryCode = items[item].mealCategoryCode;
                        mealItem.mealCategoryID = items[item].mealCategoryId;
                        mealItem.mealCharge = items[item].charges[0].amount;
                        mealItem.popularity = items[item].popularity;
                        mealItem.mealCode = items[item].itemId;
                        mealItem.mealDescription = items[item].description;
                        mealItem.mealImagePath = items[item].mealImagePath;
                        mealItem.mealName = items[item].itemName;
                        mealItem.mealThumbnailImagePath = items[item].mealThumbnailImagePath;
                        mealItem.validations = items[item].validations;
                        mealItem.categoryRestricted = items[item].categoryRestricted;
                        availableItemList.push(mealItem);
                    }
                    return availableItemList;
                };

                var toSsrInFlightModel = function (items) {
                    var availableItemList = [];
                    var item;
                    var inflightItem;
                    for(var item in items){
                        inflightItem = items[item];
                        inflightItem.price = items[item].charges[0].amount;
                        availableItemList.push(inflightItem);
                    }
                    return availableItemList;
                };

                var toSsrAirportModel = function (items) {
                    var availableItemList = [];
                    var item;
                    var airportServiceItem;
                    for(var item in items){
                        airportServiceItem = items[item];
                        availableItemList.push(airportServiceItem);
                    }
                    return availableItemList;
                };

                var toSeatModel = function (items,applyScope) {
                    var itemsMap = [];
                    var i;
                    var item;
                    var seat;
                    var ancillaryService;
                    for(i in items){
                        if (!ancillaryService) { ancillaryService = $injector.get('ancillaryService'); }
                        item =ancillaryService.setSeatForPax(items[i],applyScope);
                        if(item == null){
                            item =  items[i]
                        }

                        if (itemsMap[item.cabinType] === undefined) {
                            itemsMap[item.cabinType] = [];
                        }

                        if (itemsMap[item.cabinType][item.rowGroupId] === undefined) {
                            itemsMap[item.cabinType][item.rowGroupId] = [];
                        }

                        if (itemsMap[item.cabinType][item.rowGroupId][item.columnGroupId] === undefined) {
                            itemsMap[item.cabinType][item.rowGroupId][item.columnGroupId] = [];
                        }

                        if (itemsMap[item.cabinType][item.rowGroupId][item.columnGroupId][item.rowNumber] === undefined) {
                            itemsMap[item.cabinType][item.rowGroupId][item.columnGroupId][item.rowNumber] = [];
                        }

                        seat = {};
                        seat.itemId = item.itemId;
                        seat.seatNumber = item.itemName;
                        if (item.availability) {
                            seat.seatAvailability = "VAC"
                        } else {
                            seat.seatAvailability = "RES"
                        }
                        seat.type = item.seatType;
                        seat.price = item.charges[0].amount;
                        seat.validations = item.validations;

                        itemsMap[item.cabinType][item.rowGroupId][item.columnGroupId][item.rowNumber].push(seat);
                    }

                    var availableItems = {};

                    var cabinClass;
                    var rowGroup;
                    var colGroup;
                    var row;
                    var itemIndex;

                    availableItems.cabinClass = [];

                    var cabinClassItem;
                    var rowGroupItem;
                    var colGroupItem;
                    var rowItem;

                    for (cabinClass in itemsMap) {


                        cabinClassItem = {};
                        cabinClassItem.cabinType = cabinClass;
                        cabinClassItem.airRowGroupDTOs = [];

                        for (rowGroup in itemsMap[cabinClass]) {

                            rowGroupItem = {};
                            rowGroupItem.rowGroupId = rowGroup;
                            rowGroupItem.airColumnGroups = [];

                            for (colGroup in itemsMap[cabinClass][rowGroup]) {

                                colGroupItem = {};
                                colGroupItem.columnGroupId = colGroup;
                                colGroupItem.airRows = [];

                                for (row in itemsMap[cabinClass][rowGroup][colGroup]) {

                                    rowItem = {};
                                    rowItem.rowNumber = row;
                                    rowItem.airSeats = itemsMap[cabinClass][rowGroup][colGroup][row];

                                    colGroupItem.airRows.push(rowItem);
                                }
                                rowGroupItem.airColumnGroups.push(colGroupItem);
                            }
                            cabinClassItem.airRowGroupDTOs.push(rowGroupItem);
                        }
                        availableItems.cabinClass.push(cabinClassItem);
                    }

                    return availableItems;
                };

                var toInsuranceModel = function (items) {
                    var availableItemList = [];
                    var item;
                    var insItem;
                    for(var item in items){
                        insItem = {};
                        insItem.policyID = items[item].itemId;
                        insItem.isSelected = (items[0].defaultSelection && item == 0 ) ? true : false;
                        insItem.headingTxt = items[item].planCode;
                        if(items[item].itemName){
                            insItem.itemName = items[item].itemName;
                        }
                        else{
                            insItem.itemName =items[item].planCode;
                        }
                        insItem.description = items[item].description;
                        insItem.amount = items[item].charges[0].amount;
                        insItem.thingsCovered = items[item].planCover;
                        insItem.termsAndConditions = items[item].termsAndConditions;
                        insItem.defaultSelection = items[item].defaultSelection;

                        availableItemList.push(insItem);
                    }
                    return availableItemList;
                };

                var toAirportTransferModel = function (items, providerInfo) {
                    var availableItemList = [];
                    var airportServiceItem;
                    for(var item in items){
                        var airportTransferItem = items[item];

                        airportTransferItem.address = providerInfo.address;
                        airportTransferItem.airportTransferId = providerInfo.airportTransferId;
                        airportTransferItem.emailId = providerInfo.emailId;
                        airportTransferItem.name = providerInfo.name;
                        airportTransferItem.number = providerInfo.number;

                        airportServiceItem = airportTransferItem;
                        availableItemList.push(airportServiceItem);
                    }
                    return availableItemList;
                };

                var toFlexibilityModel = function (items) {
                    var availableItemList = [];
                    for(var item in items){
                        var flexiItem = {};
                        flexiItem.flexiId = items[item].itemId;
                        flexiItem.price = items[item].charges[0].amount;
                        flexiItem.selected = items[item].defaultSelected;
                        availableItemList.push(flexiItem);
                    }
                    return availableItemList;
                };

                this.getPriceQuoteAncillary = function (priceQuoteAncillaryRS) {

                    var totalAnciAmount = 0;
                    var paxMonetaryAmount = 0;
                    var transformedMonetaryAmmendment = [];
                    var paxMonetaryAmmendmentList;
                    var reservationMonetaryAmmendmentList;
                    var transformedMonetaryAmmendmentList;
                    var monetaryAmmendmentItem = {};
                    var monetaryAmmendmentItems = [];
                    var ammendmentDefinition;
                    var monetaryAmmendmentList;
                    var priceQuoteAncillaryResponse = {};
                    var reservationAncillaryTypes;
                    var pricedAncillaryType;
                    var ancillaryScope;
                    var priceItem;

                    var ancillaryReservation = priceQuoteAncillaryRS.ancillaryReservation;
                    var passengerList = ancillaryReservation.passengers;
                    var metaData = priceQuoteAncillaryRS.metaData;

                    //update passenger ancillary charges
                    for(var passenger in passengerList){
                        totalAnciAmount = totalAnciAmount + passengerList[passenger].amount;
                        paxMonetaryAmmendmentList = passengerList[passenger].amendments;
                        for(var monetaryAmendment in paxMonetaryAmmendmentList){
                            if(angular.isUndefined(transformedMonetaryAmmendment[paxMonetaryAmmendmentList[monetaryAmendment].definitionId])){
                                transformedMonetaryAmmendment[paxMonetaryAmmendmentList[monetaryAmendment].definitionId] = paxMonetaryAmmendmentList[monetaryAmendment].amount;
                            } else {
                                paxMonetaryAmount = transformedMonetaryAmmendment[paxMonetaryAmmendmentList[monetaryAmendment].definitionId];
                                paxMonetaryAmount = paxMonetaryAmount + paxMonetaryAmmendmentList[monetaryAmendment].amount;
                                transformedMonetaryAmmendment[paxMonetaryAmmendmentList[monetaryAmendment].definitionId] = paxMonetaryAmount;
                            }

                        }
                    }

                    //update reservation ancillary charges
                    //totalAnciAmount = totalAnciAmount + ancillaryReservation.amount;
                    reservationAncillaryTypes = ancillaryReservation.ancillaryTypes;
                    reservationMonetaryAmmendmentList = ancillaryReservation.amendments;
                    for(var index in reservationAncillaryTypes){
                        pricedAncillaryType = reservationAncillaryTypes[index];
                        for(var ancillaryScopeIndex in pricedAncillaryType.ancillaryScopes){
                            ancillaryScope = pricedAncillaryType.ancillaryScopes[ancillaryScopeIndex];
                            for(var priceItemindex in ancillaryScope.ancillaries){
                                priceItem = ancillaryScope.ancillaries[priceItemindex];
                                totalAnciAmount = totalAnciAmount + priceItem.amount;
                            }
                        }
                    }
                    for(var monetaryAmendment in reservationMonetaryAmmendmentList){
                        if(angular.isUndefined(transformedMonetaryAmmendment[reservationMonetaryAmmendmentList[monetaryAmendment].definitionId])){
                            transformedMonetaryAmmendment[reservationMonetaryAmmendmentList[monetaryAmendment].definitionId] = reservationMonetaryAmmendmentList[monetaryAmendment].amount;
                        } else {
                            paxMonetaryAmount = transformedMonetaryAmmendment[reservationMonetaryAmmendmentList[monetaryAmendment].definitionId];
                            paxMonetaryAmount = paxMonetaryAmount + reservationMonetaryAmmendmentList[monetaryAmendment].amount;
                            transformedMonetaryAmmendment[reservationMonetaryAmmendmentList[monetaryAmendment].definitionId] = paxMonetaryAmount;
                        }
                    }

                    for(var monetaryAmendmentDefinition in metaData.monetaryAmendmentDefinitions){
                        ammendmentDefinition = metaData.monetaryAmendmentDefinitions[monetaryAmendmentDefinition];
                        monetaryAmmendmentItem = {};
                        monetaryAmmendmentItem.name = ammendmentDefinition.type;
                        monetaryAmmendmentItem.description = ammendmentDefinition.description;
                        monetaryAmmendmentItem.category = ammendmentDefinition.category;
                        monetaryAmmendmentItem.amount = transformedMonetaryAmmendment[ammendmentDefinition.definitionId];
                        monetaryAmmendmentItems.push(monetaryAmmendmentItem);
                    }

                    priceQuoteAncillaryResponse.total = totalAnciAmount;
                    priceQuoteAncillaryResponse.monetaryAmmendments = monetaryAmmendmentItems;

                    return priceQuoteAncillaryResponse;
                }

            }]);
})(angular);