(function (angular) {
    var app = angular.module("ibeAPIService");

    app.service("remoteAncillary", [
        "$http",
        "ancillaryAdaptor",
        "ConnectionProps",
        "applicationService",
        "loadingService",
        function ($http, ancillaryAdaptor, ConnectionProps, applicationService, loadingService) {

            this._basePath = ConnectionProps.host;

            
            this.availableAncillary;

            this.ancillaryInitialData;

            this.metaData;

            this.defaultSeatsSet = false; //Flag to check if the default seats have been set
            
            this.previousSeatsCreated = false;
            
            this.previousPaxSeats = {};
            
            this.previousSegmentPaxSeats = {};

            var self = this;
            
            self.ancillaryAvailability = null;


            this.initializeAncillary = function (ancillariesAvailabilityRq, success, error,modifyEdit) {
                var path = '/ancillary/availability/reservation/create';
                if (applicationService.APP_MODES.MODIFY_SEGMENT == applicationService.getApplicationMode()) {
                    path = '/ancillary/availability/reservation/modify';
                } else if (applicationService.APP_MODES.ADD_MODIFY_ANCI == applicationService.getApplicationMode()) {
                    path = '/ancillary/availability/ancillaries/modify';
                }

                if(modifyEdit === 'modifyEdit'){
                                        $http({
                                            "method": 'POST',
                                            "url": this._basePath + path,
                                            "data": ancillariesAvailabilityRq
                                        }).success(function (response) {
                                            self.ancillaryAvailability = response;
                                            var data = {
                                                'availableAncillariesResponse': response
                                            };
                                            return success(data);
                                        }).error(function (response) {
                                            error(response);
                                        });
                }else{
                    if (isUndefinedOrNull(self.ancillaryAvailability)) {
                                        $http({
                                            "method": 'POST',
                                            "url": this._basePath + path,
                                            "data": ancillariesAvailabilityRq
                                        }).success(function (response) {
                                            self.ancillaryAvailability = response;
                                            var data = {
                                                'availableAncillariesResponse': response
                                            };
                                            return success(data);
                                        }).error(function (response) {
                                            error(response);
                                        });
                                    } else {
                                        var data = {
                                            'availableAncillariesResponse': self.ancillaryAvailability
                                        };
                                        return success(data);
                                    }
                }
                
            };
            
            this.loadAvailableAncillaryData = function (availableData, success, error) {
                var mode = applicationService.getApplicationMode();
                var path = '/ancillary/available/reservation/create';
                if (mode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    path = '/ancillary/available/reservation/modify';
                } else if (mode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    path = '/ancillary/available/ancillaries/modify';

                }

                if (isUndefinedOrNull(self.metaData) && isUndefinedOrNull(self.ancillaryInitialData)) {
                    $http({
                        "method": 'POST',
                        "url": this._basePath + path,
                        "data": availableData
                    }).success(function (response) {
                        self.metaData = response.metaData;
                        self.ancillaryInitialData = getAncillaryInitialData(response);
                        success(ancillaryAdaptor.getAvailableAncillary(response));
                    }).error(function (response) {
                        error();
//                        console.log(response);
                    });
                } else {
                    success(self.availableAncillary);
                }
            };


            this.getDefaults = function (successFn,errorFn) {
                var path = '/ancillary/default/reservation/modify';
                loadingService.showPageLoading();
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": {}
                }).success(function (response) {
                    successFn(response);
                }).error(function (error) {
                   errorFn(error);
                }).finally(function () {
                    loadingService.hidePageLoading();
                });
            };


            this.setAvailableAncillaryData = function (selectedAncillaryData, success, error) {
                var mode = applicationService.getApplicationMode();
                var path = '/ancillary/set/reservation/create';

                if (mode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    path = '/ancillary/set/reservation/modify';
                } else if (mode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    path = '/ancillary/set/ancillaries/modify';

                }


                selectedAncillaryData.metaData = self.metaData;
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": selectedAncillaryData
                }).success(function (response) {
                    success(response);
                }).error(function (response) {
                    error(response);
//                    console.log(response);
                });
            }

            this.loadSeatMapData = function (availableData, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    success(self.availableAncillary.SEAT);
                }
            }

            this.loadMealList = function (availableData, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    success(self.availableAncillary.MEAL);
                }
            }
            
            this.loadAutomaticCheckinData = function (availableData, success, error) {
            	if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    success(self.availableAncillary.AUTOMATIC_CHECKIN);
                }
            }
            
            this.loadBaggageData = function (availableData, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    success(self.availableAncillary.BAGGAGE);
                }
            }

            this.retrieveInsuranceData = function (insuranceRq, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    success(self.availableAncillary.INSURANCE[0]);
                }
            };

            this.loadAirportServicesData = function (airportServiceReq, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    // Set charge for adult with infant
                    _.forEach(self.availableAncillary.SSR_AIRPORT, function(section) {
                        _.forEach(section.items, function(item) {

                            // If chargeBasis is for all passengers replace charges for all pax types with that value
                            if(item.charges[0] && item.charges[0].chargeBasis === "ALL_PASSENGERS") {
                              if(item.applicablityType == 'P') {
                                item.charges = [
                                    {chargeBasis: 'ADULT', amount: item.charges[0].amount},
                                    {chargeBasis: 'CHILD', amount: item.charges[0].amount},
                                    {chargeBasis: 'INFANT', amount: item.charges[0].amount}
                                ]
                              }
                              else {
                                item.charges = [
                                    {chargeBasis: 'ADULT', amount: item.charges[0].amount},
                                    {chargeBasis: 'CHILD', amount: 0},
                                    {chargeBasis: 'INFANT', amount: 0}
                                ]
                              }
                            }

                            if(item.charges[0] && item.charges[2] && !item.charges[3]) {
                                var adultInfantAmount = item.charges[0].amount + item.charges[2].amount;
                                item.charges.push({amount: adultInfantAmount, chargeBasis: 'ADULT_WITH_INFANT'});
                            }
                        })
                    });

                    success(self.availableAncillary.SSR_AIRPORT);
                }
            };

            this.loadAirportTransferData = function (airportServiceReq, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    // Set charge for adult with infant
                    _.forEach(self.availableAncillary.AIRPORT_TRANSFER, function(section) {
                        _.forEach(section.items, function(item) {
                            if(item.charges[0] && item.charges[2] && !item.charges[3]) {
                                var adultInfantAmount = item.charges[0].amount + item.charges[2].amount;
                                item.charges.push({amount: adultInfantAmount, chargeBasis: 'ADULT_WITH_INFANT'});
                            }
                        })
                    });

                    success(self.availableAncillary.AIRPORT_TRANSFER);
                }
            };

            this.loadInFlightServices = function (airportServiceReq, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    success(self.availableAncillary.SSR_IN_FLIGHT);
                }
            };

            this.loadFlexiData = function (airportServiceReq, success, error) {
                if (angular.isUndefined(self.availableAncillary)) {
                    success(null);
                } else {
                    success(self.availableAncillary.FLEXIBILITY);
                }
            };

            this.reservationAnsiSummary = function(param, successFn, errorFn) {
                var mode = applicationService.getApplicationMode();
                var path = "/ancillary/price/reservation/create";
                if (mode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    path = '/ancillary/price/reservation/modify';
                } else if (mode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    path = '/ancillary/price/ancillaries/modify';
                }

                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": param
                }).success(function(response) {
                    successFn(ancillaryAdaptor.getPriceQuoteAncillary(response));
                }).error(function(response) {
                    errorFn(response);
                });
            };

            this.blockAncillary = function(param, successFn, errorFn){
                var path = "/ancillary/block";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": param
                }).success(function(response) {
                    successFn(response);
                }).error(function(response) {
                    errorFn(response);
                });
            };

            this.getDefaultSeats = function(param, successFn, errorFn){
                var path = "/ancillary/default/reservation/create";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": {}
                }).success(function(response) {
                    successFn(response);
                }).error(function(response) {
                    errorFn(response);
                });
            };

            this.clearExtras = function () {
                self.ancillaryAvailability = null;
                self.availableAncillary = null;
                self.ancillaryInitialData = null;
                self.metaData = null;
                self.defaultSeatsSet = false;
            };

            function isUndefinedOrNull(val) {
                return angular.isUndefined(val) || val === null
            }

            function getAncillaryInitialData(response) {
                var initData = {};
                var availableAncillaries = response;

                initData.ondInfo = [];
                initData.anciTypeScopes = [];
                initData.defaultSelections = [];

                var a;
                var b;
                var c;
                var d;

                var tempAnciName;
                var tempAnciType;
                var tempAnciScope;
                var tempItem;

                var defaultAnciProvider;
                var anciScope;
                var defaultItem;

                for (a in availableAncillaries.ancillaries) {
                    tempAnciType = availableAncillaries.ancillaries[a];

                    tempAnciName = tempAnciType.ancillaryType;

                    if (tempAnciName == 'BAGGAGE' || tempAnciName == 'INSURANCE') {
                        initData.defaultSelections[tempAnciName] = [];
                    }

                    if (tempAnciName == 'BAGGAGE') {
                        initData.ondInfo[tempAnciName] = [];
                    }

                    defaultAnciProvider = tempAnciType.providers[0];
                    for (b in defaultAnciProvider.availableAncillaries.availableUnits) {
                        tempAnciScope = defaultAnciProvider.availableAncillaries.availableUnits[b];

                        anciScope = {};
                        anciScope.scope = tempAnciScope.scope;
                        anciScope.applicability = tempAnciScope.applicability;

                        defaultItem = null;

                        initData.anciTypeScopes[tempAnciName] = tempAnciScope.scope.scopeType;

                        if (tempAnciName == 'BAGGAGE') {
                            for (d in availableAncillaries.metaData.ondPreferences) {
                                if (tempAnciScope.scope.ondId == availableAncillaries.metaData.ondPreferences[d].ondId) {
                                    initData.ondInfo[tempAnciName].push(availableAncillaries.metaData.ondPreferences[d]);
                                }
                            }
                        }

                        for (c in tempAnciScope.itemsGroup.items) {
                            tempItem = tempAnciScope.itemsGroup.items[c];

                            if (tempAnciName == 'BAGGAGE' && tempItem.defaultBaggage) {
                                defaultItem = tempItem;
                            } else if (tempAnciName == 'INSURANCE' && defaultItem === null) {
                                defaultItem = tempItem;
                            }
                        }
                        anciScope.default = defaultItem;

                        if (tempAnciName == 'BAGGAGE' || tempAnciName == 'INSURANCE') {
                            initData.defaultSelections[tempAnciType.ancillaryType].push(anciScope);
                        }

                    }

                }

                return initData;
            }
            this.updateSeatMapModel = function(param, success, error){
                var mode = applicationService.getApplicationMode();
                var path = '/ancillary/available/reservation/create';
                if (mode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    path = '/ancillary/available/reservation/modify';
                } else if (mode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    path = '/ancillary/available/ancillaries/modify';

                }
                    $http({
                        "method": 'POST',
                        "url": this._basePath + path,
                        "data": param
                    }).success(function (response) {
                       var temp = ancillaryAdaptor.getAvailableAncillary(response);
                        self.availableAncillary.SEAT = temp.SEAT;
                        success(self.availableAncillary.SEAT);
                    }).error(function (response) {
                        error();
                    });

            }

        }]);
})(angular);