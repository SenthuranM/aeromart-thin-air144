(function (angular) {

    angular
        .module("ibeAPIService")
        .service("remoteConfirm", ["$http", "$q", "ConnectionProps", remoteConfirmService]);

    function remoteConfirmService($http, $q, ConnectionProps) {
        var self = this;

        self._basePath = ConnectionProps.host;

        self.getReservationDetails = function (confirmReq, successFn, errorFn) {
            var path = "/reservation/loadConfirmationPage",

                reqParams = {
                    "method": 'POST',
                    "url": self._basePath + path,
                    "data": confirmReq
                };

            $http(reqParams)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (response) {
                    errorFn(response);
                });

        }; // End of getReservationDetails function.

        // Get page markup for printing.
        self.getPrintMarkup = function (params, successFn, errorFn) {

            var path = "/reservation/itinerary/print";
            var params = {
                "method": 'POST',
                "url": this._basePath + path,
                "data": params
            };

            $http(params)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (response) {
                    errorFn(response);
                });

        }; // End of getPrintMarkup function.
        
        // Get page markup for printing taxInvoice.
        self.getPrintMarkupForTaxInvoice = function (params, successFn, errorFn) {

            var path = "/reservation/taxInvoice/print";
            var params = {
                "method": 'POST',
                "url": this._basePath + path,
                "data": params
            };

            $http(params)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (response) {
                    errorFn(response);
                });

        }; // End of getPrintMarkup function for taxInvoice.
        
        // Get page markup for printing taxInvoice.
        self.getTaxInvoiceList = function (params, successFn, errorFn) {

            var path = "/reservation/taxInvoice/getList";
            var params = {
                "method": 'POST',
                "url": this._basePath + path,
                "data": params
            };

            $http(params)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (response) {
                    errorFn(response);
                });

        }; // End of getPrintMarkup function for taxInvoice.

        // Send email.
        self.sendEmail = function (params, successFn, errorFn) {

            var path = "/reservation/itinerary/email";
            var reqParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(reqParams)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (response) {
                    errorFn(response);
                });

        }; // End of sendEmail function.

        // Get url by tag.
        self.getUrlByTag = function (params, successFn, errorFn) {

            var path = "/customer/crossPortalLogin";
          //Adding time out to by-pass request which is going in pending
			var timeout = $q.defer();
			var result = $q.defer();
			var timedOut = false;
			setTimeout(function() {
				timedOut = true;
				timeout.resolve();
			}, (500));
            var reqParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(reqParams)
                .success(function (response) {
                    successFn(response);
                })
                .error(function (response) {
                    errorFn(response);
                });

        }; // End of getUrlByTag.

        //Get lms point balance
        self.getLMSPoints = function (successFn, errorFn) {
            var path = "/customer/getLMSPoints";

            $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data": {}
            }).success(function (response) {
                successFn(response);
            }).error(function (response) {
                errorFn(response);
            });
        };

    } // End of remoteConfirmService function.

})(angular);
