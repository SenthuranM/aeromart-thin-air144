
(function(angular) {

    angular
        .module("ibeDummyService")
        .service('dummyConfirm', ["$http", dummyConfirmService]);

    function dummyConfirmService($http) {
        var self = this;

        self._basePath = '';

        self.getReservationDetails = function(confirmReq, successFn, errorFn) {
            var path = "sampleData/reservationSummary.json";
            var params = {
                "method": 'GET',
                "url": self._basePath + path,
                "data": confirmReq
            };

            $http(params)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn();
                });

        }; // End of getReservationDetails function.

        // Get page markup for printing.
        self.getPrintMarkup = function (params, successFn, errorFn) {

            var path = "sampleData/printMarkup.json";
            var params = {
                "method": 'GET',
                "url": self._basePath + path,
                "data": params
            };

            $http(params)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });

        }; // End of getPrintMarkup function.

        // Send email.
        self.sendEmail = function (params, successFn, errorFn) {

            var path = "sampleData/sendEmailResponse.json";
            var params = {
                "method": 'GET',
                "url": self._basePath + path,
                "data": params
            };

            $http(params)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });

        }; // End of sendEmail function.

        self.getUrlByTag = function (params, successFn, errorFn) {
            console.log('not implemented.');
            return {};
        };

    } // End of dummyConfirmService function,

})(angular);
