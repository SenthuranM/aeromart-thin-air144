(function (angular) {
    var app = angular.module("ibeAPIService");

    app.service('remoteReProtect', ['$http', 'ConnectionProps', 'loadingService', 'applicationService',
        function ($http, ConnectionProps, paymentAdaptor, loadingService, applicationService) {

            this._basePath = ConnectionProps.host;

            this.retrieveGetReservationDetailsGridData = function(requestParams, successFn, errorFn){
                var path = "/selfReprotection/getReservationDetailsGridData";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": requestParams
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };
            
            this.transferReservationSegments = function(data, successFn, errorFn){
                var path = "/selfReprotection/transferReservationSegments";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": data
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

        }]);
})(angular);
