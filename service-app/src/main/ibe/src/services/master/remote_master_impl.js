(function (angular) {
    var app = angular.module("ibeAPIService");
    app.service('remoteMaster', ["$http", "ConnectionProps", function ($http, ConnectionProps) {

        this._basePath = ConnectionProps.host;
        this.retrieveCountries = function () {
            var path = "/masterData/countryList";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {}
            }).success(function(response) {
                return response.countryList;
            }).error(function(response) {
                return response;
            });
        };
        
      //get relationshipList
        this.retrieveRelationshipLists = function() {
            var path = "/masterData/getRelationshipDropdownList";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {},
                "headers": {
                    'content-Type': 'application/json'
                }
                }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
        this.retrieveExchangeRates = function () {
            var path = "/masterData/currencyExRates";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {}
            }).success(function(response) {
                return response.currencyExRates;
            }).error(function(response) {
                return response;
            });
        };
        
       this.retrieveApplicationParams = function () {
            var path = "/parameters/ibe";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data": {}
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };

        //TODO: replace with actual data
        this.retrievePassengerContactParams = function(params) {
            var path = "/paxConfig/v2/paxContactDetails";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data": params
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };

        this.retrivePassengerRegistrationParams = function(params) {
            var path = "/paxConfig/usrRegDetails";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data": params
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };

        this.retriveForgetPasswordParams = function(params) {
            var path = "/customer/changePassword";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": params
            }).success(function(response) {
                return response;
            }).error(function(response) {
                return response;
            });
        };

        //TODO: replace with actual data
        this.retrievePassengerDetailParams = function(params) {
            var path = "/paxConfig/v2/paxDetails";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data": params
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };

        this.retrieveCommonLists = function() {
            var path = "/masterData/dropDownLists";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {}
                
            }).success(function(response) {
                return response;
            }).error(function(response) {
                return response;
            });
        };
        
        //get meal list
        this.retrieveMealLists = function() {
            var path = "/masterData/getAllMealDetails";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {},
                "headers": {
                    'content-Type': 'application/json'
                }
                }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
        //get seat types for seat and meal
        this.retrieveSeatType = function() {
            var path = "/masterData/getAllSeatTypes";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {},
                "headers": {
                    'content-Type': 'application/json'
                }
                }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
       
        this.retrieveAirportMessages = function(params) {
            var path = "/masterData/airportMessages";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data": params
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };

        this.retrieveTermsConditions = function() {
            var path = "/masterData/termsNConditions";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {}
            }).success(function(response) {
                return response;
            }).error(function(response) {
                return response;
            });
        };
        
        this.addGoogleAnalyticsLogs = function(pnr, revanue, exception, flow, pathNew, fullPath, transactionID) {
            var path = "/masterData/addGALogs";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data": {"pnr":pnr , "revanue": revanue, "exception": exception, "flow": flow, "path": pathNew, "fullPath": fullPath, "transactionid": transactionID }
            })
        };
    }]);
})(angular);