(function(angular) {
    var app = angular.module("ibeDummyService");
    app.service("dummyMaster", ["$http", function($http) {

        this._basePath = '';
        this.retrieveCountries = function() {
                var path = "sampleData/countryList.json";
                return $http.get(path).then(function(data){
                    return data;
                });
        };

        this.retrieveExchangeRates = function() {
            var path = "sampleData/exchangeRates.json";
            return $http.get(path).then(function(data){
                return data;
            });
        };

        this.retrieveApplicationParams = function(){
            var path = "sampleData/appParam.json";
            return $http.get(path).then(function(data){
                return data;
            });
        };

        this.retrievePassengerContactParams = function() {
            var path = "sampleData/passengerContactConfig.json";
            return $http.get(path).then(function(data){
                return data;
            });
        };

        this.retrievePassengerDetailParams = function() {
            var path = "sampleData/passengerDetailConfig.json";
            return $http.get(path).then(function(data){
                return data;
            });
        };

        this.retrieveCommonLists = function() {
            var path = "sampleData/commonList.json";
            return $http.get(path).then(function(data){
                return data;
            });
        };

        this.retrieveTermsConditions = function() {
            var path = "sampleData/commonList.json";
            return $http.get(path).then(function(data){
                return data;
            });
        };

    }]);
})(angular);