(function(angular) {
  var app = angular.module("ibeDummyService");

  app.service('dummyCustomer', ['$http', function($http) {
    this._basePath = '';

    this.login = function(signInReqData, successFn, errorFn) {

      var path = "sampleData/loginResponse.json";
      var retFn = function() {
        this.success = function(response) {

        }, this.error = function(error) {

        }
      }
      $http({
        "method": 'GET',
        "url": this._basePath + path,
        "data": signInReqData
      }).success(function(response) {
        successFn(response);
      }).error(function(response) {
        errorFn();
//        console.log(response);
      });
    },

    this.register = function(regRequestData, successFn, errorFn) {

        var path = "sampleData/registrationResponse.json";
        var retFn = function() {
          this.success = function(response) {

          }, this.error = function(error) {

          }
        }
        $http({
          "method": 'GET',
          "url": this._basePath + path,
          "data": regRequestData
        }).success(function(response) {
          successFn(response);
        }).error(function(response) {
          errorFn();
//          console.log(response);
        });
     };

     this.passwordRecovery = function (params, successFn, errorFn) {
         return {
           success: true
         };
     }

     this.LMSRegistration = function(LMSValidationReq, successFn, errorFn){

        return {
          success: true
        };
    };

     this.validateLMSDetails = function(LMSValidationReq, successFn, errorFn){

        return {
          success: true
        };
    };

    this.getReservationList = function (params, successFn, errorFn) {

        return {
          success: true
        };
    };

  }]);

  app.service('apiService', ['$http', function($http) {

    this.login = function(signInReqData, successFn, errorFn) {
    },

    this.register = function(regRequestData, successFn, errorFn) {
    }

  }]);
})(angular);
