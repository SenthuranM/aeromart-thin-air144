
(function(angular) {

    angular
        .module('ibeAPIService')
        .service('remoteCustomer', ['$http', 'customerAdaptor', 'ConnectionProps', remoteCustomerService]);

    function remoteCustomerService ($http, customerAdaptor, ConnectionProps) {

        // Global vars.
        var self = this;
        self._basePath = ConnectionProps.host;


        // Functions.
        self.login = login;
        self.validateLMSDetails = validateLMSDetails;
        self.register = register;
        self.signOut = signOut;
        self.passwordRecovery = passwordRecovery;
        self.LMSRegistration = LMSRegistration;
        self.getReservationList = getReservationList;
        self.resendConfirmationEmail = resendConfirmationEmail;
        self.isUserLoggedIn = isUserLoggedIn;
        self.mashreqRegister = mashreqRegister;
        self.getUserProfile = getUserProfile;
        self.updateSeatPreference = updateSeatPreference;
        self.updateMealPreference = updateMealPreference;
        self.getSignInUserPaymentDetails = getSignInUserPaymentDetails;
        self.deleteUserCardDetail = deleteUserCardDetail;
        

        function resendConfirmationEmail(params, successFn, errorFn) {

            var path = "/customer/sendLMSConfEmail";

            var requestParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(requestParams)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });
        }


        function getReservationList(params, successFn, errorFn) {

            var path = "/customer/reservationList";

            var requestParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(requestParams)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });
        }


        function isUserLoggedIn(params, successFn, errorFn) {

            var path = "/customer/customerLoggedInCheck";

            var requestParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(requestParams)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });
        }


        function login(signInReqData, successFn, errorFn) {

            var path = "/customer/login";
            var loginReq = customerAdaptor.adaptLoginReq(signInReqData);

            var params = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": loginReq
            };

            $http(params)
                .success(function(response) {
                    var loginResponse = customerAdaptor.adaptLoginResponse(response);
                    successFn(loginResponse);
                })
                .error(function(response) {
                    errorFn(response);
                });

        } // End of login.


        function validateLMSDetails(LMSValidationReq, successFn, errorFn) {

            var path = "/customer/lmsValidation";
            var params = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": LMSValidationReq
            };

            $http(params)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });

        } // End of validateLMSDetails.


        function register(regRequestData, successFn, errorFn) {

            var regCustomerRequest = customerAdaptor.adaptRegCustomerRequest(regRequestData);
            var path = "/customer/register";
            var params = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": regCustomerRequest
            };

            $http(params)
                .success(function(response) {
                    successFn(response);
                }).error(function(response) {
                    errorFn(response);
                });

        } // End of register.


        function signOut(baseReq , successFn, errorFn) {

            var path = "/customer/signOut";
            var params = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": baseReq
            };

            $http(params)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });

        } // End of signOut.


        /**
         *  LMS registration remote function.
         *  @param params: lms registration request object.
         *  @param successFn: success function.
         *  @param errorFn: error function.
         */
        function LMSRegistration(params, successFn, errorFn) {

            var path = "/customer/lmsRegister";

            var reqParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(reqParams)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });
        }


        /**
         *  Password recovery remote function.
         *  @param params: password recovery request object.
         *  @param successFn: success function.
         *  @param errorFn: error function.
         */
        function passwordRecovery(params, successFn, errorFn) {

            var path = "/customer/passwordRecovery";

            var reqParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(reqParams)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });

        } //  End of passwordRecovery

        /**
         * Service call for registering Mashreq users
         * */
        function mashreqRegister(params, successFn, errorFn){
            var path = "/customer/loyaltyRegister";

            var reqParams = {
                "method": 'POST',
                "url": self._basePath + path,
                "data": params
            };

            $http(reqParams)
                .success(function(response) {
                    successFn(response);
                })
                .error(function(response) {
                    errorFn(response);
                });
        }
        
        //to get user profiledetails
        function getUserProfile() {
            var path = "/customer/profileDetails";
            return $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {},
                "headers": {
                    'content-Type': 'application/json'
                }
                }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
      //update user seat preference
        function updateSeatPreference(params) {
            var path = "/customer/updateSeatTypePreference";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data":params,
                "headers": {
                    'content-Type': 'application/json'
                }
                }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
        //update user meal preference
        function updateMealPreference(params) {
            var path = "/customer/updateMealListPreference";
            return $http({
                "method": 'POST',
                "url": this._basePath + path,
                "data":params,
                "headers": {
                    'content-Type': 'application/json'
                }
                }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
        function getSignInUserPaymentDetails(success, errorFn) {
        	var path = this._basePath + "/customer/alias/customerAliasList";
        	
            $http({
				"method" : 'GET',
				"url" : path,
				"data": {},
	               "headers": {
	                   'content-Type': 'application/json'
	               }
			}).success(function(response) {
				success(response.customerAliasList);
			}).error(function(error) {
				errorFn(error.message);
			});
            
        };
        
        function deleteUserCardDetail(userCardData, success, errorFn) {
        	var path = this._basePath + "/customer/alias/deleteAlias";
        	
            $http({
				"method" : 'POST',
				"url" : path,
				"data" : userCardData
			}).success(function(response) {
				success(response);
			}).error(function(error) {
				errorFn(error);
			});
        };
        
    } // End of customerRemoteService.

})(angular);
