(function(angular) {
  var app = angular.module("ibeAPIAdaptor");

  app.service('customerAdaptor', ['$http', function($http) {

	  this.adaptLoginReq = function(signInReqData){

		  var loginReq = {};
		  loginReq.customerID = signInReqData.emailId;
		  loginReq.password = signInReqData.password;

		  return loginReq;
	  },

	  this.adaptLoginResponse = function(response){

          if (!response.success) {
              return response;
          }

		  var results = {};
          results.success = response.success;
		  results.contactInfo = {};
		  results.contactInfo.title = response.loggedInCustomerDetails.title;
		  results.contactInfo.firstName = response.loggedInCustomerDetails.firstName;
		  results.contactInfo.lastName = response.loggedInCustomerDetails.lastName;
		  results.contactInfo.emailAddress = response.loggedInCustomerDetails.emailId;
		  results.contactInfo.nationality = response.loggedInCustomerDetails.nationality;
		  results.contactInfo.country = response.loggedInCustomerDetails.country;

          var mobile = response.loggedInCustomerDetails.mobile;
		  results.contactInfo.mobile = mobile ? mobile.countryCode + mobile.areaCode + mobile.number : "";

		  results.customer = {};
		  results.customer.dateOfBirth = response.loggedInCustomerDetails.dateOfBirth;
                  results.customer.sendPromoEmail = response.loggedInCustomerDetails.sendPromoEmail;

		  if (response.loggedInLmsDetails != null) {
			  results.joinLms = true;
		  }
          else {
			  results.joinLms = false;
		  }

		  results.lmsDetails = {};

		  results.lmsDetails.emailId = response.loggedInLmsDetails.ffid;

          // Need original response to match with other logins.
          // like in dashboard, payment.
          results.user = response;

		  return results;

	  },

	  this.adaptRegCustomerRequest = function(regRequestData){

		  var regCustomerReguest = {};
		  regCustomerReguest.customer = {};
		  regCustomerReguest.lmsDetails = {};
		  regCustomerReguest.customer.customerContact = {};
		  regCustomerReguest.customer.customerContact.mobileNumber = {};

		  if(regRequestData.customer.title == 'Mr. '){
			  regCustomerReguest.customer.title = "MR";
			  regCustomerReguest.customer.gender = "M";
		  }else if(regRequestData.customer.title == 'Miss. '){
			  regCustomerReguest.customer.title = "MS";
			  regCustomerReguest.customer.gender = "F";
		  }else if(regRequestData.customer.title == 'Mrs. '){
			  regCustomerReguest.customer.title = "MRS";
			  regCustomerReguest.customer.gender = "F";
		  }

		  regCustomerReguest.customer.firstName = regRequestData.customer.firstName;
		  regCustomerReguest.customer.lastName = regRequestData.customer.lastName;
		  regCustomerReguest.customer.password = regRequestData.customer.password;
		  regCustomerReguest.customer.dateOfBirth = regRequestData.customer.dateOfBirth;
		  regCustomerReguest.customer.countryCode = regRequestData.customer.countryCode;
		//TODO remove hardcoded nationalityCode and get it from the system
		  regCustomerReguest.customer.nationalityCode = "670";
                  regCustomerReguest.customer.sendPromoEmail = regRequestData.customer.sendPromoEmail;

		  var mobile = regRequestData.mobile;
		  //TODO remove hardcoded mobile no
		  regCustomerReguest.customer.customerContact.mobileNumber.countryCode = "94";
		  regCustomerReguest.customer.customerContact.mobileNumber.areaCode = "71";
		  regCustomerReguest.customer.customerContact.mobileNumber.number = "123455";

		  regCustomerReguest.customer.customerContact.emailAddress = regRequestData.customer.emailId;
		  if(regRequestData.optLMS){
			  regCustomerReguest.lmsDetails.dateOfBirth = regRequestData.customer.dateOfBirth;
			  regCustomerReguest.lmsDetails.ffid = regRequestData.customer.emailId;
			  regCustomerReguest.lmsDetails.emailStatus = null;
			  regCustomerReguest.lmsDetails.language = "en";
			  regCustomerReguest.lmsDetails.appCode = "APP_IBE";
		  }else{
			  regCustomerReguest.lmsDetails = null;
		  }

		  regCustomerReguest.optLMS = regRequestData.optLMS;
          regCustomerReguest.userDetails = regRequestData.passengerDetails;

		  return regCustomerReguest;
	  }

  }]);

})(angular);
