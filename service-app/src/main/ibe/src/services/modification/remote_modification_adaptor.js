/**
 * Created by sumudu on 1/20/16.
 */
'use strict';

(function (angular) {
    var app = angular.module("ibeAPIAdaptor");

    //Adaptor for LoadReservation Api Call
    app.service("modifyReservationAdaptorService", [
        "commonLocalStorageService",
        "appConstants",
        "$translate",
        "$filter",
        "$window",
        function (commonLocalStorageService, appConstants, $translate, $filter,$window) {

            var DATE_FORMAT = [
                "YYYY-MM-DDTHH:mm:ss",
                "ddd DD MMM YYYY HH:mm",
                "HH:mm",
                "ddd DD MMM YYYY",
                "DD MMM YYYY"
            ];

            var getDuration = function (fromDateTime, toDateTime) {
                var from = moment(fromDateTime, DATE_FORMAT[0]);
                var to = moment(toDateTime, DATE_FORMAT[0]);
                var duration = moment.duration(to.diff(from));
                var h = duration.hours();
                var m = duration.minutes();
                return (h <= 9 ? '0' : '') + h + ":" + (m <= 9 ? '0' : '') + m;
            }

            var getFormattedDate = function (sourceDate, sourceFormat, requestedFormat) {
                if (!!sourceDate) {
                    var tmpDate = moment(sourceDate, sourceFormat);
                    return tmpDate.format(requestedFormat);
                }
                return "";
            };


            var getPropertyValue = function (propertyNameString, obj) {
                var strArr = propertyNameString.split(".");
                var resObj = '';

                if (!!obj) {
                    for (var i = 0; i < strArr.length; i++) {
                        if (obj.hasOwnProperty(strArr[i])) {
                            obj = obj[strArr[i]];
                            resObj = obj;

                            if (resObj)
                                continue;
                            else
                                break;
                        } else {
                            resObj = null;
                            break;
                        }
                    }
                }

                return resObj;
            }

            var getOndPreferences = function (pref) {
                var ondPreferance = [];
                if (!!pref) {
                    angular.forEach(pref, function (val, key) {
                        var ondPref = {}
                        ondPref.ondId = val.ondId
                        ondPref.flightSegmentRPH = val.flightSegmentRPH;
                        ondPreferance.push(ondPref);
                    });
                }
                return ondPreferance;

            }
            var getPassengerWiseExtras = function (pref) {
                var extras = [];
                if (!!pref) {
                    angular.forEach(pref, function (val, key) {
                        var ondPref = {}
                        ondPref.ondId = val.ondId
                        ondPref.flightSegmentRPH = val.flightSegmentRPH;
                        ondPreferance.push(ondPref);
                    });
                }
                return extras;

            }

            var setResponseStatus = function (adaptedResult, response) {
                if(response.balanceSummary){
                    adaptedResult.balanceSummary = response.balanceSummary;
                }
                adaptedResult.success = response.success;
                adaptedResult.messages = response.messages || null;
                adaptedResult.errors = response.errors || null;
                adaptedResult.warnings = response.warnings || null;
                return adaptedResult;
            };

            var getSavedAncillary = function (type, allAncillaries) {
                 var found = null;
                for (var i = 0; i < allAncillaries.length; i++) {
                    var obj = allAncillaries[i];
                    if (obj.ancillaryType == type) {
                        found = obj;
                        break;
                    }
                }
                return found;
            };


            var getAncillaryTotalAmount = function (allAncillaries) {
                var total = 0;
                if (!!allAncillaries) {
                    angular.forEach(allAncillaries, function (ancyllary) {
                        total += ( (parseFloat(ancyllary.amount) || 0) * (parseFloat(ancyllary.quantity) || 1) );
                    });
                }
                return total;
            };
            var setAncillaryValues = function (allAncillaries,scopeType,ancillaryType) {
                var ancillary = null;
                if (!!allAncillaries) {
                    angular.forEach(allAncillaries, function (ancyllary) {
                        if(scopeType === 'OND'){
                            if(ancillary === null )
                                ancillary = ancyllary.quantity+" X "+ancyllary.name;
                            else
                                 ancillary += " , "+ancyllary.quantity+" X "+ancyllary.name;
                     }else if(scopeType === 'SEGMENT' ){
                         if(ancillaryType === 'SEAT'){
                             if(ancillary === null )
                                ancillary =ancyllary.id;
                            else
                                 ancillary +=" ,"+ ancyllary.id;
                         }else{
                             if(ancillary === null )
                                ancillary = ancyllary.quantity+" X "+ancyllary.name;
                            else
                                 ancillary +=" ,"+ancyllary.quantity+" X "+ancyllary.name;
                         }
                         
                     }else if(scopeType === 'AIRPORT' ){
                         if(ancillary === null )
                                ancillary = ancyllary.quantity+" X "+ancyllary.name;
                            else
                                ancillary += " , "+ancyllary.quantity+" X "+ancyllary.name;
                         
                     }
                    });
                }
                return ancillary;
            };


            var populateAncillaries = function (resAncillary, populateToArray,selectedExtras) {
                if (!!resAncillary) {
                    angular.forEach(resAncillary, function (ancillary) {
                        var tmpAmount = 0;
                        var tmpScopeType = null;
                        var tmpOndId = [];
                        var tmpRPH = [];
                        var tmpancillaryValues = [];
                        var count=0;
                        
                        var ancillaryScopes = getPropertyValue("ancillaryScopes", ancillary);
                        if (!!ancillaryScopes) {
                            angular.forEach(ancillaryScopes, function (scopeItem) {
                                var priceItems = getPropertyValue("ancillaries", scopeItem);
                                if (!!priceItems) {
                                    tmpAmount = getAncillaryTotalAmount(priceItems);
                                }
                                tmpScopeType = scopeItem.scope.scopeType;
                               // if(tmpancillaryValues === null ){
                                    tmpOndId[count] =scopeItem.scope.ondId;
                                    tmpRPH[count] = scopeItem.scope.flightSegmentRPH;
                                    tmpancillaryValues[count] = setAncillaryValues(priceItems,tmpScopeType,ancillary.ancillaryType);
                                    count++;
                                //}
                                
                            });
                        }
                        var savedAncillary = getSavedAncillary(ancillary.ancillaryType, populateToArray);
                        if (!!savedAncillary) {
                            if(window.matchMedia("only screen and (max-width: 767px)").matches){
                                var tmpAncillary = {
                                    ancillaryType: ancillary.ancillaryType,
                                    ancillaryImageClass: appConstants.ancillaryImageClasses[ancillary.ancillaryType] || appConstants.ancillaryImageClasses['DEFAULT'],
                                    isSelected: true,
                                    amount: tmpAmount,
                                    scopeType: tmpScopeType,
                                    isEnable: false,
                                    ancillaryValue:tmpancillaryValues,
                                    ondId:tmpOndId,
                                    flightRPH:tmpRPH     
                                    
                                };
                                populateToArray.push(tmpAncillary);
                            }else{
                                 savedAncillary.amount = savedAncillary.amount + parseFloat(tmpAmount);
                            }
                        } else {
                            if (tmpScopeType != null) {
                                var tmpAncillary = {
                                    ancillaryType: ancillary.ancillaryType,
                                    ancillaryImageClass: appConstants.ancillaryImageClasses[ancillary.ancillaryType] || appConstants.ancillaryImageClasses['DEFAULT'],
                                    isSelected: true,
                                    amount: tmpAmount,
                                    scopeType: tmpScopeType,
                                    isEnable: false,
                                    ancillaryValue:tmpancillaryValues,
                                   ondId:tmpOndId,
                                    flightRPH:tmpRPH     
                                    
                                };
                                populateToArray.push(tmpAncillary);
                            }
                        }
                    });
                }
            }


            /**
             * UI adaptor for loadReservation Api Result
             * @param response
             * @returns {{bookingDetails: {}, passengerDetails: {}, ancillaries: Array, flightDetails: {}}}
             */
            this.loadReservationAdaptor = function (response) {
                var adapted = {
                    success: response.success,
                    messages: null,
                    errors: null,
                    warnings: null,
                    bookingDetails: {
                        pnr: getPropertyValue("bookingInfoTO.pnr", response),
                        groupPnr: getPropertyValue("bookingInfoTO.groupPnr", response),
                        bookingDate: getFormattedDate(getPropertyValue("bookingInfoTO.bookingDate", response), DATE_FORMAT[0], DATE_FORMAT[4]),
                        amountPaid: getPropertyValue("balanceSummary.totalPayment", response),
                        version: getPropertyValue("bookingInfoTO.version", response),
                        policyId: getPropertyValue("bookingInfoTO.policyCode", response),
                        balanceAvailable: getPropertyValue("bookingInfoTO.balanceAvailable", response),
                        prefferedLanguage:getPropertyValue("bookingInfoTO.prefferedLanguage", response)
                    },
                    passengerDetails: {
                        allPassengers: [],
                        paxCountTxt: '',
                        adultCount: 0,
                        childCount: 0,
                        infantCount: 0
                    },
                    contactInfo: {},
                    ancillaries: [],
                    paxWiseSelectedAncillariesForMobile:[],
                    segments: [],
                    fareGroupWiseSegments: [],
                    ondPreferences: [],
                    modificationParams: {
                        isNameChangeEnable: false,
                        isSegmentModificationEnable: false,
                        all: {}
                    },
                    advertisements:response.advertisements,
                    balanceSummary:getPropertyValue("balanceSummary", response),
                    promoInfo:getPropertyValue("promoInfo", response),
                    flexiInfoList:getPropertyValue("flexiInfoList", response),
                    paymentDetails:response.paymentDetails,
                    taxInvoiceGenerated:response.taxInvoiceGenerated
                };


                /**
                 * Set Modification Params
                 */
                var modificationParam = getPropertyValue("modificationParams", response);
                var nameChangeParam = modificationParam && modificationParam.paxModificationParams;
                if (!!nameChangeParam) {
                    for (var i = 0; i < nameChangeParam.length; i++) {
                        var t = nameChangeParam[i];
                        if (t.nameEditable || t.ffidEditable) {
                            adapted.modificationParams.isNameChangeEnable = true;
                            break;
                        }
                    }
                }


                if (modificationParam && modificationParam.segmentModificationParams) {
                    for (var i = 0; i < modificationParam.segmentModificationParams.length; i++) {
                        var s = modificationParam.segmentModificationParams[i];
                        if (s.cancellable || s.modifible) {
                            adapted.modificationParams.isSegmentModificationEnable = true;
                            break;
                        }
                    }
                }
                adapted.modificationParams.all = modificationParam;

                /**
                 * Set OND Preferences
                 */
                var ondPre = getPropertyValue("metaData.ondPreferences", response);

                adapted.ondPreferences = getOndPreferences(ondPre);


                /**
                 *******************************************************************
                 * Set Version
                 *******************************************************************
                 */
                var tmpVersion = getPropertyValue("bookingInfoTO.version", response);
                if (!!tmpVersion) {
                    if(!adapted.bookingDetails.groupPnr) {
                        var res = tmpVersion.split('-');
                        if (res.length > 1) {
                            adapted.bookingDetails.version = res[1] || "0";
                        }
                    }else{
                        adapted.bookingDetails.version = tmpVersion;
                    }
                }

                /**
                 *******************************************************************
                 *Set Total Paid
                 *******************************************************************
                 */
                var chargeBreakdown = getPropertyValue("balanceSummary.chargeBreakdown", response);
                if (!!chargeBreakdown) {
                    for (var i = 0; i < chargeBreakdown.length; i++) {
                        var obj = chargeBreakdown[i];
                        if (obj.type == 'TOTAL_PAID') {
                            adapted.bookingDetails.amountPaid = obj.amount;
                            break;
                        }
                    }
                }

                /**
                 *******************************************************************
                 *SetBooking Status
                 *******************************************************************
                 */
                var status = getPropertyValue("bookingInfoTO.status", response);
                adapted.bookingDetails.statusCode = status;
                switch (status) {
                    case 'CNF':
                    {
                        adapted.bookingDetails.status = 'Confirmed';
                        break;
                    }
                    case 'CNX':
                    {
                        adapted.bookingDetails.status = 'Cancelled';
                        break;
                    }
                    case 'OHD':
                    {
                        adapted.bookingDetails.status = 'On-hold';
                        break;
                    }
                    case 'STB':
                    {
                        adapted.bookingDetails.status = 'Standby';
                        break;
                    }
                    default :
                    {
                        adapted.bookingDetails.status = status;
                    }
                }

                /**
                 ********************************************************************
                 *Set Reservation wise Ancillaries
                 ********************************************************************
                 */
                var resAncillary = getPropertyValue("reservationWiseSelectedAncillary", response);
                populateAncillaries(resAncillary, adapted.ancillaries);


                /**
                 ***********************************************************************
                 *Set Passengers
                 ***********************************************************************
                 */
                var adtCount = getPropertyValue("bookingInfoTO.travelerQuantity.adultCount", response);
                var chdCount = getPropertyValue("bookingInfoTO.travelerQuantity.childCount", response);
                var infCount = getPropertyValue("bookingInfoTO.travelerQuantity.infantCount", response);
                $translate.onReady().then(function(){
                 adapted.passengerDetails.adultCount = adtCount;
                 adapted.passengerDetails.childCount = chdCount;
                 adapted.passengerDetails.infantCount = infCount;
                 var adultSingle = $translate.instant("lbl_common_adult");
                 var adultMultiple = $translate.instant("lbl_common_adults");
                 var childSingle = $translate.instant("lbl_common_child");
                 var childMultiple = $translate.instant("lbl_common_children");
                 var infantSingle = $translate.instant("lbl_common_infant");
                 var infantMultiple = $translate.instant("lbl_common_infants");
                
                 var paxAdultString = adtCount + " " +(adtCount > 0 ? (adtCount == 1 ? adultSingle : adultMultiple) : ""),
                    paxChildString = (chdCount > 0) ? ((infCount > 0 ? ", " : " & ") + chdCount + ((chdCount > 1) ? " "+childMultiple : " "+childSingle)) : "",
                    paxInfantString = (infCount > 0) ? " & " + infCount + " " + ((infCount === 1) ?  infantSingle : infantMultiple) : "";

                adapted.passengerDetails.adultCount = adtCount;
                adapted.passengerDetails.childCount = chdCount;
                adapted.passengerDetails.infantCount = infCount;

                var paxAdultString = adtCount + " adult" + (adtCount > 1 ? "s" : ""),
                    paxChildString = (chdCount > 0) ? ((infCount > 0 ? ", " : " & ") + chdCount + ((chdCount > 1) ? " children" : " child")) : "",
                    paxInfantString = (infCount > 0) ? (" & " + infCount + " infant" + ((infCount > 1) ? "s" : "")) : "";

                adapted.passengerDetails.paxCountTxt = paxAdultString + paxChildString + paxInfantString;
                });

                var paxCol = getPropertyValue("passengers", response);
               
                if (!!paxCol) {
                    angular.forEach(paxCol, function (pax) {
                        var paxTypeCapitalized = pax.paxType.charAt(0).toUpperCase() + pax.paxType.substr(1).toLowerCase();
                        var selectedAncillaries = {}
                        var extras = angular.copy(pax.extras)
                        angular.forEach(pax.passengerWiseSelectedAncillaries, function (anci, key) {
                            var ancillary = [];
                            /*
                             angular.forEach(anci.ancillaryScopes,function(anciScope){
                             if(anciScope.ancillaryScopes[0].scope.scopeType == "SEGMENT"){
                             ancillary[anci.ancillaryScopes[0].scope.flightSegmentRPH]
                             }else{

                             }

                             })
                             */
                            ancillary[anci.ancillaryScopes[0].scope.scopeType]
                            selectedAncillaries[anci.ancillaryType] = anci;
                        })
                        adapted.passengerDetails.allPassengers.push({
                            additionalInfo: pax.additionalInfo,
                            paxSequence: pax.paxSequence,
                            title: pax.title || "BABY",
                            firstName: pax.firstName,
                            lastName: pax.lastName,
                            paxType: paxTypeCapitalized,
                            dateOfBirth: pax.dateOfBirth,
                            nationality: pax.nationality,
                            selectedAncillaries: selectedAncillaries,
                            extras: pax.extras,
                            travelingWith: pax.travelWith || null

                        });

                        //Populate Pax Wise Ancillary
                        var paxWiseAllAncillaries = getPropertyValue("passengerWiseSelectedAncillaries", pax);
                        populateAncillaries(paxWiseAllAncillaries, adapted.ancillaries,pax.extras);
                    });
                }

                // Re-order passenger details
                adapted.passengerDetails.allPassengers = _.sortBy( adapted.passengerDetails.allPassengers, 'paxSequence' );

                var getFareGroup = function (fareGroup, allFareGroups) {
                    var resultFareGroup = null;
                    for (var i = 0; i < allFareGroups.length; i++) {
                        var o = allFareGroups[i];
                        if (o.fareGroupID == fareGroup) {
                            resultFareGroup = o;
                            break;
                        }

                    }
                    return resultFareGroup;
                };

                var addFareGroupWiseSegments = function (segment) {
                    var fareGroup = getFareGroup(segment.flight.fareGroupID, adapted.fareGroupWiseSegments);
                    if (!!fareGroup) {
                        fareGroup.segments.push(segment);
                        if (!!fareGroup.status) {
                            fareGroup.status.push(segment.status)
                        } else {
                            fareGroup.status = [segment.status];
                        }
                    } else {
                        adapted.fareGroupWiseSegments.push({
                            fareGroupID: segment.flight.fareGroupID,
                            segments: [segment],
                            status: [segment.flight.status]
                        });
                    }
                }


                /**
                 ************************************************************************
                 *Set Contact Information
                 ************************************************************************
                 */

                adapted.contactInfo = getPropertyValue("contactInfo", response);


                /**
                 ************************************************************************
                 *Set Reservation Flight Segments
                 ************************************************************************
                 */
                var segments = getPropertyValue("reservationSegments", response);
                if (!!segments) {
                    angular.forEach(segments, function (segment) {
                        var departureTime = getPropertyValue("segment.departureDateTime.local", segment);
                        var arrivalTime = getPropertyValue("segment.arrivalDateTime.local", segment);

                        var departureTimeZulu = getPropertyValue("segment.departureDateTime.zulu", segment);
                        var arrivalTimeZulu = getPropertyValue("segment.arrivalDateTime.zulu", segment);

                        var adaptedSegment = {
                            fare: {},
                            flight: {}
                        };

                        adaptedSegment.flight.cabinClass = segment.cabinClass;
                        adaptedSegment.flight.arrivalDate = getFormattedDate(arrivalTime, DATE_FORMAT[0], DATE_FORMAT[3]);
                        adaptedSegment.flight.arrivalTime = getFormattedDate(arrivalTime, DATE_FORMAT[0], DATE_FORMAT[2]);
                        adaptedSegment.flight.departureDate = getFormattedDate(departureTime, DATE_FORMAT[0], DATE_FORMAT[3]);
                        adaptedSegment.flight.departureTime = getFormattedDate(departureTime, DATE_FORMAT[0], DATE_FORMAT[2]);

                        adaptedSegment.flight.arrivalDateObj = arrivalTime;
                        adaptedSegment.flight.departureDateObj = departureTime;

                        var segmentStr = getPropertyValue("segment.segmentCode", segment);
                        if (!!segmentStr) {
                            var tmp = segmentStr.split('/');
                            adaptedSegment.flight.originCode = tmp[0];
                            adaptedSegment.flight.originName = commonLocalStorageService.getAirportCityName(tmp[0], false);
                            adaptedSegment.flight.destinationCode = tmp[tmp.length-1];
                            adaptedSegment.flight.destinationName = commonLocalStorageService.getAirportCityName(tmp[tmp.length-1], false);
                        }
                        adaptedSegment.flight.flightFaresAvailability = [];
                        adaptedSegment.flight.flightId = "501";
                        adaptedSegment.flight.travelMode = getPropertyValue("segment.travelMode", segment);
                        adaptedSegment.flight.flightNumber = getPropertyValue("segment.filghtDesignator", segment);
                        adaptedSegment.flight.flightRPH = getPropertyValue("segment.flightSegmentRPH", segment);
                        adaptedSegment.flight.reservationSegmentRPH = getPropertyValue("reservationSegmentRPH", segment);
                        adaptedSegment.flight.returnFlag = false;
                        adaptedSegment.flight.stops = [];
                        adaptedSegment.flight.totalDuration = getDuration(departureTimeZulu, arrivalTimeZulu);
                        adaptedSegment.flight.fareGroupID = getPropertyValue("fareGroupID", segment);
                        adaptedSegment.flight.journeySequence = getPropertyValue("journeySequence", segment);
                        adaptedSegment.flight.flightSegmentRPH = segment.segment.flightSegmentRPH;
                        adaptedSegment.flight.status = getPropertyValue("status", segment);
                        adaptedSegment.flight.segmentSequence = getPropertyValue("segmentSequence", segment);
                        adaptedSegment.flight.standardStatus = segment.standardStatus;
                        adaptedSegment.flight.flownSegment = segment.flownSegment;
                        adapted.segments.push(adaptedSegment);

                        addFareGroupWiseSegments(adaptedSegment);
                    });
                }

                var sortedFareGroups = $filter('orderBy')(adapted.fareGroupWiseSegments, 'fareGroupID');
                adapted.fareGroupWiseSegments = sortedFareGroups;

                return setResponseStatus(adapted, response);
            };


            /**
             *
             * UI adaptor for Dashboard airewards Api Result
             * @param response
             * @returns {{reservationDetails: [], totalCustomerCredit: 1}}
             */
            this.getDashboardReservationsAdaptor = function (response) {

                var adapted = {
                    success: response.success,
                    messages: null,
                    errors: null,
                    warnings: null,
                    reservations: response.reservationList,
                    lmsDetails: response.loggedInLmsDetails.availablePoints,
                    totalCustomerCredit: response.totalCustomerCredit,
                    loyaltyManagmentEnabled: response.loyaltyManagmentEnabled,
                    firstName: response.loggedInCustomerDetails.firstName,
                    lastName: response.loggedInCustomerDetails.lastName
                };

                return setResponseStatus(adapted, response);
            };

            /**
             * UI adaptor for cancelReservation API call
             * @param response
             * @returns {{}}
             */
            this.cancelReservationAdaptor = function (response) {
                var adaptedResponse = {};
                return setResponseStatus(adaptedResponse, response);
            };


            /**
             * UI Adaptor for Cancel Reservation Balance Summery API call
             * @param response
             * @returns {{}}
             */
            this.cancelReservationBalanceSummeryAdaptor = function (response) {

                var adaptedResponse = adaptModifySummary(response);

                adaptedResponse.pnr = "";
                adaptedResponse.priceItems = [];

                return setResponseStatus(adaptedResponse, response);
            };


            this.nameChangeResponseAdaptor = function (response) {
                var adaptedResponse = adaptModifySummary(response);

                adaptedResponse.updatedChargeForDisplay = response.updatedChargeForDisplay;
                adaptedResponse.modifiedBalanceInfo = response.modifiedBalanceInfo;
                return setResponseStatus(adaptedResponse, response);
            };

            this.requotAdaptor = function (response) {
                try{
                    angular.forEach(response.originDestinationResponse,function(originDestinationResponse, index){
                        // Index of 0 is hard coded due to the re-quote always request being done for a single flight
                        response.originDestinationResponse[index].availableOptions[0].availableFareClasses.sort(sortRequoteFareClassByPrice);
                    });
                }catch(e){
                    IBEDataConsole.log(e);
                }

                return response;
            };

            function sortRequoteFareClassByPrice(fareClassA,fareClassB){
                if(fareClassA.price < fareClassB.price)
                    return -1;
                else if (fareClassA.price  > fareClassB.price )
                    return 1;
                else
                    return 0;
            }

            this.confirmCancelSegmentAdaptor = function (response) {
                var adaptedResponse = {};
                return response;
            };

            this.confirmNameChangeAdaptor = function (response) {
                var adaptedResponse = {};
                return response;
            };

            this.confirmAncillaryModificationAdaptor = function (response) {
                var adaptedResponse = {};
                return response;
            };

            this.segmentModificationBalanceSummeryAdaptor = function (response) {
                //adaptedResponse.updatedChargeForDisplay = response.updatedChargeForDisplay;
                //adaptedResponse.modifiedBalanceInfo = response.modifiedBalanceInfo;

                var adaptedResponse = adaptModifySummary(response);
                return setResponseStatus(adaptedResponse, response);
            };

            this.balanceSummaryForModifySegmentAfterAnciAdaptor = function (response) {
                //var adaptedResponse = {};
                var adaptedResponse = adaptModifySummary(response);
                adaptedResponse.updatedChargeForDisplay = response.updatedChargeForDisplay;
                adaptedResponse.modifiedBalanceInfo = response.modifiedBalanceInfo;
                return setResponseStatus(adaptedResponse, response);
                return adaptedResponse;
            };

            function adaptModifySummary(response){
                var adaptedResponse = {};

                var paxList = _.map(response.paxList, function(passenger){
                    return {
                        type: passenger.paxInfo.paxType,
                        fareCharge: passenger.paxCharges.fareAmount
                    }
                });

                var paxSummary = {};

                _.forEach(paxList, function(passenger){
                    if(paxSummary[passenger.type]){
                        paxSummary[passenger.type].totalCharge += passenger.fareCharge;
                        paxSummary[passenger.type].count += 1;
                    }else{
                        paxSummary[passenger.type] = {
                            type: passenger.type,
                            totalCharge: passenger.fareCharge,
                            count: 1
                        }
                    }
                });

                adaptedResponse.paxSummary = paxSummary;

                var balanceInfo = response.reservationBalanceInfo;
                var postCharge = balanceInfo.postModifiedSummary;

                var taxSurcharge = postCharge.taxAmount + postCharge.surchargeAmount;
                var postTotal = postCharge.fareAmount + taxSurcharge - postCharge.discount;

                adaptedResponse.modifyCharges = {
                    previousCharge: balanceInfo.paidAmount,
                    total: balanceInfo.amountDue,
                    credit: balanceInfo.creditAmount,
                    nonRefundable: balanceInfo.nonRefundableAmount,
                    chargeInfo: balanceInfo.chargeInfo,
                    postCharge: postTotal,
                    taxSurcharge: taxSurcharge
                };

                return adaptedResponse;
            }

            this.ancillaryModificationBalanceSummeryAdaptor = function (response) {
                var adaptedResponse = adaptModifySummary(response);
                adaptedResponse.updatedChargeForDisplay = response.updatedChargeForDisplay;
                adaptedResponse.modifiedBalanceInfo = response.modifiedBalanceInfo;
                return setResponseStatus(adaptedResponse, response);
            };


            this.flightSearchResultAdaptor = function (response, allAvailableFareClasses) {

                var getFareClassByCode = function (fareClassCode, allFareClasses) {
                    var fare = null;
                    if (!!fareClassCode && !!allFareClasses) {
                        for (var i = 0; i < allFareClasses.length; i++) {
                            var obj = allFareClasses[i];
                            if (obj.fareTypeId == fareClassCode) {
                                fare = obj;
                                break;
                            }
                        }
                    }
                    return fare;
                };

                var flights = response.data.results[0].flights;

                if (!!flights) {
                    if (!!allAvailableFareClasses) {
                        angular.forEach(flights, function (f) {
                            var allAvailableFares = f.flightFaresAvailability;
                            f.flightSegmentRPH = f.flightRPH;
                            angular.forEach(allAvailableFares, function (fare) {
                                var tFare = getFareClassByCode(fare.fareTypeId, allAvailableFareClasses);
                                fare.fareTypeName = tFare.fareTypeName;
                                fare.fareClassId = tFare.fareClassId;
                                fare.fareClassType = tFare.fareClassType;
                                fare.description = tFare.description;
                                fare.templateDTO = tFare.templateDTO;
                                fare.imageUrl = tFare.imageUrl;
                            });
                        });
                    }
                } else {
                    flights = [];
                }

                return flights;
            };

            this.getContactInformationAdaptor = function (response) {
                var adaptedResponse = {};
                var mobileObject = response.contactInfo.mobileNumber;
                response.contactInfo.mobileNo = mobileObject.countryCode + mobileObject.areaCode + mobileObject.number;
                response.contactInfo.salutation = response.contactInfo.title;
                response.contactInfo.email = response.contactInfo.emailAddress;
                return response;
            };

            this.changeContactInformationAdaptor = function (data) {

                var contact = data.contactInfo;

                var reservationContact = {};
                reservationContact.title = contact.salutation;
                reservationContact.firstName = contact.firstName;
                reservationContact.lastName = contact.lastName;
                reservationContact.nationality = contact.nationality;
                reservationContact.country = contact.country;
                reservationContact.preferredLangauge = contact.language;
                reservationContact.emailAddress = contact.email;
                reservationContact.state = contact.state;
                reservationContact.taxRegNo = contact.taxRegNo;
                reservationContact.sendPromoEmail = contact.sendPromoEmail;
                var address = {};
                if(typeof contact.address === 'object'){
                	address = contact.address;
                }else{
                	address.streetAddress1 = null;
                    address.streetAddress2 = null;
                    address.city = null;
                    address.zipCode = null;
                }
                reservationContact.address = address;

                var countryCodeLength = 2;

                if (contact.mobileCountryCode) {
                    countryCodeLength = contact.mobileCountryCode.length;
                }

                if (contact.mobileNo) {
                    var mobile = {};
                    mobile.countryCode = contact.mobileNo.countryCode;
                    mobile.areaCode = contact.mobileNo.number.substring(0, 2);
                    mobile.number = contact.mobileNo.number.substring(2, contact.mobileNo.number.length);

                    reservationContact.mobileNumber = mobile;
                }

                if (contact.mobileNoTravel) {
                    var mobileTravel = {};
                    mobileTravel.countryCode = contact.mobileNoTravel.countryCode;
                    mobileTravel.areaCode = contact.mobileNoTravel.number.substring(0, 2);
                    mobileTravel.number = contact.mobileNoTravel.number.substring(2, contact.mobileNoTravel.number.length);

                    reservationContact.telephoneNumber = mobileTravel;
                }

                reservationContact.fax = null;
                reservationContact.zipCode = null;

                data.contactInfo = reservationContact;
                return data;
            };


            this.DefaultAncillariesOnModifyReservationAdaptor = function (response) {
                var adapted = {
                    ondPreferences: [],
                    ancillaries: [],
                    passengers: [],
                    ancillaryReProtect:{}
                };

                //SET OND Preferences
                var ondPre = getPropertyValue("metaData.ondPreferences", response);
                adapted.ondPreferences = getOndPreferences(ondPre);
                adapted.ancillaryReProtect = response.ancillaryReProtect;
                //Populate OND wise Ancillary
                var resAncillary = getPropertyValue("ancillaryReservation.ancillaryTypes", response);
                populateAncillaries(resAncillary, adapted.ancillaries);


                //Populate PaxWise Ancillaries
                var paxCol = getPropertyValue("ancillaryReservation.passengers", response);
                if (!!paxCol) {
                    angular.forEach(paxCol, function (pax) {
                        var tSelectedAncillaries = {};
                        angular.forEach(pax.ancillaryTypes, function (anci) {
                            if (!!anci.ancillaryScopes && anci.ancillaryScopes.length > 0) {
                                var ancillary = [];
                                ancillary[anci.ancillaryScopes[0].scope.scopeType]
                                tSelectedAncillaries[anci.ancillaryType] = anci;
                            }
                        });
                        adapted.passengers.push({
                            passengerRph: pax.passengerRph,
                            selectedAncillaries: tSelectedAncillaries
                        });

                        //Populate Pax Wise Ancillary
                        var paxWiseAllAncillaries = getPropertyValue("ancillaryTypes", pax);
                        populateAncillaries(paxWiseAllAncillaries, adapted.ancillaries);
                    });
                }

                return adapted;
            };


            this.getAncillarySummeryAdaptor = function (response) {

                var adapted = [];

                if (!!response && response.ancillarySummeryList) {

                    angular.forEach(response.ancillarySummeryList, function (item) {

                        if (item.available || item.selected) {

                            var tmp = {
                                ancillaryType: item.anciType,
                                ancillaryImageClass: appConstants.ancillaryImageClasses[item.anciType] || appConstants.ancillaryImageClasses['DEFAULT'],
                                isSelected: item.selected,
                                amount: item.totalAmount,
                                isEnable: item.editable,
                                scopeType: item.scopeType
                            };

                            // Here contains translation keys for relevant ancy types.
                            // lbl_extra_meal, MODIFY WITH CAUTION.
                            // lbl_extra_baggage, MODIFY WITH CAUTION.
                            // lbl_extra_seat, MODIFY WITH CAUTION.
                            // lbl_extra_ssr_in_flight, MODIFY WITH CAUTION.
                            // lbl_extra_ssr_airport, MODIFY WITH CAUTION.
                            // lbl_extra_insurance, MODIFY WITH CAUTION.
                            var key = 'lbl_extra_' + item.anciType.toLowerCase();
                            $translate(key).then(function (name) {
                                tmp.ancillaryName = name;
                            });
                            adapted.push(tmp);
                        }
                    });
                }

                return adapted;
            };

        }]);
})(angular);
