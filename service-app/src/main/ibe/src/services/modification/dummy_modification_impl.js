/**
 * Created by sumudu on 1/20/16.
 */
'use strict';

(function (angular) {
    var app = angular.module("ibeDummyService");

    app.service("dummyModification", [
        "$q",
        "$http",
        "modifyReservationAdaptorService",
        "loadingService",
        "availabilityAdaptor",
        function ($q, $http, modifyReservationAdaptorService, loadingService, availabilityAdaptor) {

            /**
             * Load Reservation Dummy service call
             * @param loadReservationRQ
             * @returns {deferred.promise|*}
             */
            this.loadReservation = function (loadReservationRQ) {
                loadingService.showPageLoading();
                //var path = 'sampleData/loadReservationResponse.json';
                var path = 'sampleData/loadReservationResponse_1.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": loadReservationRQ
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.loadReservationAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };

            /**
             * Get Dashboard LMS Details Dummy service call
             * @param data
             * @returns {deferred.promise|*}
             */
            this.getDashboardReservations = function (data) {
                loadingService.showPageLoading();
                var path = 'sampleData/customerLoginDetails.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": data
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.getDashboardReservationsAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };


            /**
             * Cancel Reservation Dummy service call
             * @param loadReservationRQ
             * @returns {deferred.promise|*}
             */
            this.cancelReservation = function (cancelReservationReq) {
                loadingService.showPageLoading();
                var path = 'sampleData/cancelReservationResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": cancelReservationReq
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.cancelReservationAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };

            /**
             * Cancel Reservation Balance Summery DUMMY Service call
             * @param balanceSummaryRequest
             * @returns {deferred.promise|*}
             */
            this.getBalanceSummaryForCancelReservation = function (balanceSummaryRequest) {
                loadingService.showPageLoading();
                var path = 'sampleData/cancelReservationBalanceSummeryResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": balanceSummaryRequest
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.cancelReservationBalanceSummeryAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };


            this.getBalanceSummaryForSegmentModification = function () {
                loadingService.showPageLoading();
                var path = 'sampleData/segmentModificationBalanceSummeryResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.segmentModificationBalanceSummeryAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };


            /**
             * Reservation Passenger Name change DUMMY service call
             * @param nameChangeReq
             * @returns {deferred.promise|*}
             */
            this.updateReservationPaxNames = function (nameChangeReq) {
                loadingService.showPageLoading();
                var path = 'sampleData/nameChangeResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": nameChangeReq
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.nameChangeResponseAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };


            /**
             * Cancel Flight Segment DUMMY Service call
             * @param cancelSegmentRq
             * @returns {deferred.promise|*}
             */
            this.doRequote = function (requoteRQ) {
                loadingService.showPageLoading();
                var path = 'sampleData/nameChangeResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": requoteRQ
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.requotAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            }


            /**
             * Confirm Cancel Flight Segment DUMMY Service Call
             * @param cancelSegmentRq
             * @returns {deferred.promise|*}
             */
            this.confirmCancelFlightSegments = function (cancelSegmentRq) {
                loadingService.showPageLoading();
                var path = 'sampleData/nameChangeResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": cancelSegmentRq
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.confirmCancelSegmentAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };

            this.doModificationFlightSearch = function (availabilitySearchReq) {
                loadingService.showPageLoading();
                var path = 'sampleData/modificationFlightSearchResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": availabilitySearchReq
                }).success(function (response) {
                    var adapted = availabilityAdaptor.adaptTimeline(response, self.departureDate, 0);
                    var fareClasses = availabilityAdaptor.adaptFareClasses(response);
                    var adaptedAgain = modifyReservationAdaptorService.flightSearchResultAdaptor(adapted, fareClasses);
                    deferred.resolve(adaptedAgain);
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };


            this.getDefaultAncillariesOnModifyReservation = function () {
                loadingService.showPageLoading();
                var path = 'sampleData/defaultAncillarySelectionsResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path
                }).success(function (response) {
                    var adapted = modifyReservationAdaptorService.DefaultAncillariesOnModifyReservationAdaptor(response);
                    deferred.resolve(adapted);
                }).error(function (error) {
                    deferred.reject(error);
                });

                return deferred.promise;
            };

            /**
             * Get Contact information DUMMY Service Call
             * @param data
             * @returns {deferred.promise|*}
             */
            this.getContactInformation = function (data) {
                loadingService.showPageLoading();
                var path = 'sampleData/contactChangeResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": data
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.getContactInformationAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };

            /**
             * Contact Change DUMMY Service Call
             * @param data
             * @returns {deferred.promise|*}
             */
            this.changeContactInformation = function (data) {
                loadingService.showPageLoading();
                var path = 'sampleData/contactChangeResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": data
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.changeContactInformationAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };


            this.getAncillarySummery = function (ancillaryAvailabilityRQ) {
                loadingService.showPageLoading();
                var path = 'sampleData/modificationAncillarySummeryResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": ancillaryAvailabilityRQ
                }).success(function (response) {
                    deferred.resolve(modifyReservationAdaptorService.getAncillarySummeryAdaptor(response));
                }).error(function (error) {
                    deferred.reject(error);
                })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };

            this.confirmNameChange = function(){
                loadingService.showPageLoading();
                var path = 'sampleData/modificationAncillarySummeryResponse.json';
                var deferred = $q.defer();
                $http({
                    "method": 'GET',
                    "url": path,
                    "data": {}})
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.confirmNameChangeAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };

        }
    ]);
})(angular);
