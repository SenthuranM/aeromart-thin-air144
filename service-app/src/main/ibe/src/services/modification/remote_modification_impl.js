/**
 * Created by sumudu on 1/20/16.
 */
'use strict';

(function (angular) {
    var app = angular.module("ibeAPIService");

    app.service("remoteModification", [
        "$q",
        "$http",
        "modifyReservationAdaptorService",
        "loadingService",
        "ConnectionProps",
        "availabilityAdaptor",
        "$state",
        "languageService",
        "currencyService",
        "applicationService",
        function ($q, $http, modifyReservationAdaptorService, loadingService, ConnectionProps, availabilityAdaptor, $state
        , languageService, currencyService, applicationService) {

            var _basePath = ConnectionProps.host;

            var _postRequestOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            };

            var _getRequestOptions = {
                method: 'GET',
                headers: {
                    'Content-Type': 'text/plain'
                }
            }

            /**
             * Load Reservation Backend API call
             * @param loadReservationRQ
             * @returns {deferred.promise|*}
             */
            this.loadReservation = function (loadReservationRQ) {
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/reservation/load',
                    data: loadReservationRQ
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.loadReservationAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    });
                return deferred.promise;
            };

            /**
             * Get dashboard LMS details Backend API call
             * @param reservationReq
             * @returns {deferred.promise|*}
             */
            this.getDashboardReservations = function (req) {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/customer/login',
                    data: req
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.getDashboardReservationsAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };

            /**
             * Cancel Reservation API service call
             * @param loadReservationRQ
             * @returns {deferred.promise|*}
             */
            this.cancelReservation = function (cancelReservationReq) {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/modification/cancelReservation',
                    data: cancelReservationReq
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.cancelReservationAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };

            /**
             * Cancel Reservation Balance Summery API Service call
             * @param balanceSummaryRequest
             * @returns {deferred.promise|*}
             */
            this.getBalanceSummaryForCancelReservation = function (balanceSummaryRequest) {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/balanceSummary/cancelReservation',
                    data: balanceSummaryRequest
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.cancelReservationBalanceSummeryAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            this.getBalanceSummaryForSegmentModification = function () {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/balanceSummary/segmentModification',
                    data : {}
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.segmentModificationBalanceSummeryAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            this.getBalanceSummaryForModifySegmentAfterAnci = function () {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/balanceSummary/anciModification',
                    data : {}
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.balanceSummaryForModifySegmentAfterAnciAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            this.getBalanceSummaryForAncillaryModification = function(){
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/ancillary/ancillaries/modify/balance',
                    data : {}
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.ancillaryModificationBalanceSummeryAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };



            /**
             * Reservation Passenger Name Change API service call
             * @param nameChangeReq
             * @returns {deferred.promise|*}
             */
            this.updateReservationPaxNames = function (nameChangeReq) {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/modification/nameChange',
                    data: nameChangeReq
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.nameChangeResponseAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            /**
             * Reservation Passenger Ffid Change API service call
             * @param ffidChangeReq
             * @returns {deferred.promise|*}
             */
            this.changeReservationFfid = function (ffidChangeReq) {

                var withNameChange = ffidChangeReq.withNameChange;

                var targetUrl = _basePath + '/modification/addEditFFID';
                if(ffidChangeReq.withNameChange){
                    targetUrl = _basePath + '/modification/addEditFFIDWithNameChange';
                }
                delete ffidChangeReq["withNameChange"];

                var req = angular.extend({}, _postRequestOptions, {
                    url: targetUrl,
                    data: ffidChangeReq
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(data);
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    });
                return deferred.promise;
            };


            /**
             * Cancel Flight Segment API Service call
             * @param cancelSegmentRq
             * @returns {deferred.promise|*}
             */
            this.doRequote = function (requoteRQ,type) {
                if(type === 'requote'){
                    loadingService.showPageLoading();
                }
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/modification/requote',
                    data: requoteRQ
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.requotAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                        $state.go('modifyFlightHome.flightHome',
                            {
                                lang: languageService.getSelectedLanguage(),
                                currency: currencyService.getSelectedCurrency(),
                                mode: applicationService.getApplicationMode()
                            }
                        );
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            /**
             * Confirm Cancel Flight Segment API Service call
             * @param cancelSegmentRq
             * @returns {deferred.promise|*}
             */
            this.confirmCancelFlightSegments = function (cancelSegmentRq) {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/modification/requoteConfirmation',
                    data: cancelSegmentRq
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.confirmCancelSegmentAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            /**
             * Confirm modify Flight Segment API Service call
             * @param cancelSegmentRq
             * @returns {deferred.promise|*}
             */
            this.confirmModifyFlightSegments = function () {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/modification/requoteConfirmation',
                    data: {}
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.confirmCancelSegmentAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            this.confirmNameChange = function(){
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/modification/requoteConfirmation',
                    data: {}
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.confirmNameChangeAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };



            /**
             * Confirm ancillary modification API service call
             * @returns {*}
             */
            this.confirmAncillaryModification = function () {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/ancillary/update/ancillaries/modify',
                    data: {}
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(modifyReservationAdaptorService.confirmAncillaryModificationAdaptor(data));
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            this.doModificationFlightSearch = function (availabilitySearchReq) {
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/availability/requote/search/fare/flight/calendar',
                    data: availabilitySearchReq
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        var adapted = availabilityAdaptor.adaptTimeline(data, availabilitySearchReq.journeyInfo.departureDateTime, 0);
                        var fareClasses = availabilityAdaptor.adaptFareClasses(data);
                        var adaptedAgain = modifyReservationAdaptorService.flightSearchResultAdaptor(adapted, fareClasses);
                        deferred.resolve(adaptedAgain);
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            this.getDefaultAncillariesOnModifyReservation = function(){
                loadingService.showPageLoading();
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/ancillary/default/reservation/modify',
                    data: {}
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        var adapted = modifyReservationAdaptorService.DefaultAncillariesOnModifyReservationAdaptor(data);
                        deferred.resolve(adapted);
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });
                return deferred.promise;
            };


            this.getAncillarySummery = function(ancillaryAvailabilityRQ){
                var req = angular.extend({}, _postRequestOptions, {
                    url: _basePath + '/ancillary/availability/ancillaries/modify/summary',
                    data: ancillaryAvailabilityRQ
                });

                var deferred = $q.defer();
                $http(req)
                    .success(function (data, status, headers, config) {
                        var adapted = modifyReservationAdaptorService.getAncillarySummeryAdaptor(data);
                        deferred.resolve(adapted);
                    })
                    .error(function (error, status, headers, config) {
                        deferred.reject(error);
                    });
                return deferred.promise;
            }

            /**
             * Contact Change REMOTE Service Call
             * @param data
             * @returns {deferred.promise|*}
             */
            this.changeContactInformation = function (data) {
                loadingService.showPageLoading();
                var path = _basePath + '/modification/updateContact';
                var deferred = $q.defer();

                data = modifyReservationAdaptorService.changeContactInformationAdaptor(data);

                $http({
                    "method": 'POST',
                    "url": path,
                    "data": data
                }).success(function (response) {
                    deferred.resolve(response);
                }).error(function (error) {
                        deferred.reject(error);
                    })
                    .finally(function () {
                        loadingService.hidePageLoading();
                    });

                return deferred.promise;
            };


        }]);
})(angular);
