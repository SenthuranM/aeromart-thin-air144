/**
 *      Created by Chamil on 26.01.2016
 */

 (function () {

     angular
        .module("ibeAPIService")
        .service("remoteViewProfile", ["$http", "$q", "ConnectionProps", "viewProfileAdaptorService", remoteViewProfileService]);

    function remoteViewProfileService($http, $q, ConnectionProps, viewProfileAdaptorService) {
        var self = this;
        var _basePath = ConnectionProps.host;

        var _postRequestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var _getRequestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        self.getUserProfileData = function (params, successFn, errorFn) {

            var requestParams = angular.extend({}, _getRequestOptions, {
                url: _basePath + '/customer/profileDetails',
                data: params
            });

            $http(requestParams)
                .success(function (data, status, headers, config) {
                    successFn(viewProfileAdaptorService.formatViewProfileData(data));
                })
                .error(function (error, status, headers, config) {
                    errorFn(error);
                });

        };
        
        self.addFamilyMemberDetails = function(params) {
            var path = "/customer/saveOrUpdateFamilyMember";
            return $http({
                "method": 'POST',
                "url": _basePath + path,
                "data": params
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
        self.getFamilyMembersDetails = function() {
        	var path = "/customer/getFamilyMembersDetails";
            return $http({
                "method": 'GET',
                "headers": {
                    'Content-Type': 'application/json'
                },
                "url": _basePath + path,
                "data": {}
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
        
        self.deleteFamilyMember = function(params) {
        	var path = "/customer/removeFamilyMember/"+params.familyMemberId;
            return $http({
                "method": 'POST',
                "headers": {
                    'Content-Type': 'application/json'
                },
                "url": _basePath + path,
                "data": {}
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };
        
        self.getFamilyMemberDropdownList = function() {
        	var path = "/customer/getFamilyMemberDropdownList";
            return $http({
                "method": 'GET',
                "headers": {
                    'Content-Type': 'application/json'
                },
                "url": _basePath + path,
                "data": {}
            }).success(function(response) {
                return response.data;
            }).error(function(response) {
                return response;
            });
        };

    } // End of remoteUserSignInService.

 })();
