/**
 * Created by indika on 12/3/15.
 */
(function () {
    
    angular.module("ibeDummyService")
    .service("dummyFormValidator", ["$http", "$q"]);
    
    function formValidator($http, $q){
        var self = this;
        
        self.getValidations = function(){
            return {
                signIn:{
                    email: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_emailRequired"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            'message': "lbl_validation_emailFormat"
                        }
                    },
                    password: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_passwordRequired"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[a-zA-Z0-9]*$/",
                            'message': "lbl_validation_passwordAlpha"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "6",
                            message: "lbl_validation_passwordMinChar"
                        }
                    }
                }
            }
        }
    }// End of Validation Service
    return formValidator;
})();