/**
 * Created by indika on 12/3/15.
 */
(function () {
    
    angular.module("ibeAPIService")
    .service("remoteFormValidator", ["$http", "$q", formValidatorService]);
    
    function formValidatorService($http, $q){
        var self = this;
        
        self.getValidations = function(){
            return { 
                searchFlight:{
                    "searchfrom": {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "msg_selectFare_NoResultsFound"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "Salutation must be selected from the list"
                        }
                    },
                    "searchto": {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "msg_selectFare_NoResultsFound"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "Title must be selected from the list"
                        }
                    },
                    'search-departure-date': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_expireDateRequired"
                        }
                    },
                    'search-arrival-date': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_expireDateRequired"
                        }
                    }
                },
                signIn:{
                    email: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_emailRequired"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            'message': "lbl_validation_emailFormat"
                        }
                    },
                    password: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_passwordRequired"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^\\S*$/",
                            'message': "lbl_validation_passwordAlpha"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "6",
                            message: "lbl_validation_passwordMinChar"
                        }
                    }
                },
                contact: {
                    salutation: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_titleRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "Salutation must be selected from the list"
                        }
                    },
                    title: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_titleRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "Title must be selected from the list"
                        }
                    },
                    firstName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_firstNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "2",
                            message: "lbl_validation_firstNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_firstNameSymbol'
                        }
                    },
                    lastName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_lastNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "2",
                            message: "lbl_validation_lastNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': "lbl_validation_lastNameSymbol"
                        }
                    },
                    relationship: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_relationshipRequired"
                        },
                        'editable': {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_relationshipInList"
                        }
                    },
                    nationality: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_nationalityRequired"
                        },
                        'editable': {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_nationalityInList"
                        }
                    },
                    dob: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    country: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_countryRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_countryInList"
                        }
                    },
                    email: {
                        required: {
                            name: "ng-required",
                            value: "passengerCtrl.contactParms.email.mandatory",
                            message: "lbl_validation_emailRequired"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            'message': "lbl_validation_emailFormat"
                        }
                    },
                    password: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_passwordRequired"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^\\S*$/",
                            'message': "lbl_validation_passwordAlpha"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "6",
                            message: "lbl_validation_passwordMinChar"
                        }
                    },
                    confirmPassword: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_confirmPasswordRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^\\S*$/",
                            message: "lbl_validation_confirmPasswordAlpha"
                        },
                        compareField: {
                            name: "compare-field",
                            value: "{{passengerCtrl.contact.password}}",
                            message: "lbl_validation_confirmPasswordMatch"
                        }
                    },
                    airwardsDOB: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    language: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_languageRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_languageInList"
                        }
                    },
                    taxRegNo :{
                        required: {
                            name: "ng-required",
                            value: "passengerCtrl.contactParms.taxRegNo.mandatory",
                            message: "lbl_common_taxRegNoRequired"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "15",
                            message: "lbl_passenger_invalidTaxRegNo"
             
                        },
                        maxlength: {
                            name: "ng-maxlength",
                            value: "15",
                            message: "lbl_passenger_invalidTaxRegNo"
             
                        },
                        'pattern':{
                            'name': "ng-pattern",
                            'value': "/^(\\d{2}[a-zA-Z]{5})+(\\d{4})+([a-zA-Z]{1})+([\\da-zA-Z]{1})+([a-zA-Z]{1})+([\\da-zA-Z]{1})$/",
                            'message': "lbl_passenger_invalidTaxRegNoFormat"
                        }
                    },
                    city: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_cityRequired"
                        }
                    },
                    'refEmail': {
                        required: {
                            name: "ng-required",
                            value: "false",
                            message: "lbl_validation_emailRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailFormat"
                        }
                    }
                },
                CustomerRegistration: {
                    email: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_emailRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailFormat"
                        }
                    },
                    password: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_passwordRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^\\S*$/",
                            message: "lbl_validation_passwordAlpha"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "6",
                            message: "lbl_validation_passwordMinChar"
                        }
                    },
                    newPassword :{
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_passwordRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^\\S*$/",
                            message: "lbl_validation_passwordAlpha"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "6",
                            message: "lbl_validation_passwordMinChar"
                        }
                    },
                    confirmPassword: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_confirmPasswordRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^\\S*$/",
                            message: "lbl_validation_confirmPasswordAlpha"
                        },
                        compareField: {
                            name: "compare-field",
                            value: "{{reg.password}}",
                            message: "lbl_validation_confirmPasswordMatch"
                        }
                    },
                    salutation: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_titleRequired"
                        }
                    },
                    firstName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_firstNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "2",
                            message: "lbl_validation_firstNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_firstNameSymbol'
                        }
                    },
                    lastName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_lastNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "2",
                            message: "lbl_validation_lastNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': "lbl_validation_lastNameSymbol"
                        }
                    },
                    nationality: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_nationalityRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_nationalityInList"
                        }
                    },
                    country: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_countryRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_countryInList"
                        }
                    },
                    airwardsDOB: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    language: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_languageRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_languageInList"
                        }
                    },
                    'refEmail': {
                        required: {
                            name: "ng-required",
                            value: "false",
                            message: "lbl_validation_emailRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailFormat"
                        }
                    },
                    'familyEmail': {
                        'required': {
                            name: "ng-required",
                            value:'reg.isFamilyEmailRequired',
                            message:"lbl_validation_familyEmailRequired"
                        },
                        'pattern': {
                            name: "ng-pattern",
                            value: "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailFormat"
                        }
                    },
                    'secretQuestion': {
                        'required': {
                            'message': 'Secret Question is required.'
                        }
                    },
                    'secretAnswer': {
                        'required': {
                            'message': 'Secret Answer is required.'
                        }
                    },
                    'alternativeEmailId': {
                        'pattern': {
                            'value': "/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/",
                            'message': "lbl_validation_emailFormat"
                        }                        
                    },
                    'loyaltyAccountNo':{
                        'required': {
                            'message': 'Account Number is required.'
                        },
                        'size': {
                            'min': 12,
                            'max': 12,
                            'message': 'Account number should contains 12 digits.'
                        }
                    },
                    'mashreqDOB': {
                        'required': {
                            'message': 'Date of Birth is required.'
                        }
                    },
                    'city': {
                        'required': {
                            'message': 'City is required.'
                        }
                    },
                    telNumber: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_phoneOrTravel"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "^[0-9]*",
                            message: "lbl_validation_phoneNumNumeric"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "5",
                            message: "lbl_validation_phoneNumMin"
             
                        },
                        maxlength: {
                            name: "ng-maxlength",
                            value: "13",
                            message: "lbl_validation_phoneNumMax"
             
                        }

                    }
                },
                Adult: {
                    firstName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_firstNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "2",
                            message: "lbl_validation_firstNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': "lbl_validation_firstNameSymbol"
                        }
                    },
                    lastName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_lastNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "2",
                            message: "lbl_validation_lastNameMinChar"
                        },
                        pattern: {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': "lbl_validation_lastNameSymbol"
                        }
                    },
                    dob: {
                        required: {
                            name: "ng-required",
                            value: "paxDetailConfig.dob.mandatory[passenger.type]",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    nationality: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_nationalityRequired"
                        },
                        'editable': {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_nationalityInList"
                        }
                    },
                    salutation: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_titleRequired"
                        },
                        editable: {
                            name: "typeahead-editable",
                            value: "false",
                            message: "Salutation must be selected from the list"
                        }
                    },
                    'airewardsEmail': {
                        required: {
                            name: "ng-required",
                            value: "false",
                            message: "lbl_validation_emailRequired"
                        },
                        'pattern': {
                            name: "ng-pattern",
                            value:  "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_airewardsEmail"
                        }
                    },
                    'passportNo': {
                        required: {
                            name: "ng-required",
                            value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
                            message: "lbl_validation_passportRequired"

                        }
                    }
                },
                Child: {
                    firstName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_firstNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "2",
                            message: "lbl_validation_firstNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_firstNameSymbol'
                        }
                    },
                    lastName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_lastNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "2",
                            message: "lbl_validation_lastNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_lastNameSymbol'
                        }
                    },
                    dob: {
                        required: {
                            name: "ng-required",
                            value: "paxDetailConfig.dob.mandatory[passenger.type]",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    nationality: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_nationalityRequired"
                        },
                        'editable': {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_nationalityInList"
                        }
                    },
                    'salutation': {
                        'required': {
                            name: "ng-required",
                            value: "true",
                            message: 'lbl_validation_titleRequired'
                        }
                    },
                    'passportNo': {
                        required: {
                            name: "ng-required",
                            value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
                            message: "lbl_validation_passportRequired"
                        }
                    },
                    'airewardsEmail': {
                        required: {
                            name: "ng-required",
                            value: "false",
                            message: "lbl_validation_emailRequired"
                        },
                        'pattern': {
                           'name': "ng-pattern",
                            value:  "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_airewardsEmail"
                        }
                    }

                },
                Infant: {
                    'travelWith': {
                        'required': {
                            name: "required",
                            value: "required",
                            message: 'lbl_validation_travelingWith'
                        }
                    },
                    firstName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_firstNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "2",
                            message: "lbl_validation_firstNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_firstNameSymbol'
                        }
                    },
                    lastName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_lastNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "2",
                            message: "lbl_validation_lastNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_lastNameSymbol'
                        }
                    },
                    dob: {
                        required: {
                            name: "ng-required",
                            value: "paxDetailConfig.dob.mandatory[passenger.type]",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    nationality: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_nationalityRequired"
                        },
                        'editable': {
                            name: "typeahead-editable",
                            value: "false",
                            message: "lbl_validation_nationalityInList"
                        }
                    },
                    'salutation': {
                        'required': {
                            name: "required",
                            value: "required",
                            message: 'Title is required.'
                        }
                    },
                    'passportNo': {
                        required: {
                            name: "ng-required",
                            value: "paxDetailConfig.passportNo.mandatory[passenger.type]",
                            message: "lbl_validation_titleRequired"
                        }
                    }

                },
                forgetPassword: {
                    'forgetEmail': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_emailRequired"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            'message': "lbl_validation_emailFormat"
                        }
                    },
                    'captcha' :
                        {
                            required: {
                                name: "ng-required",
                                value: "true",
                                message: "lbl_validation_captcha"
                            }
                        }
                    },
                airwardsReg: {
                    airwardsDOB: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    language: {
                        'required': {
                            name: "ng-required",
                            value: "true",
                            message: 'Communucation Language is required.'
                        }
                    },
                    'refEmail': {
                        'pattern': {
                            name: "ng-pattern",
                            value:  "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailRequired"
                        }
                    },
                    'familyEmail': {
                        required: {
                            name: "ng-required",
                            value: "false",
                            message: "lbl_validation_emailRequired"
                        },
                        'pattern': {
                            name: "ng-pattern",
                            value:  "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailRequired"
                        }
                    },
                    'loyaltyAccountNo':{
                        required: {
                            name: "ng-required",
                            value: "false",
                            'message': 'Account Number is required.'
                        }, 
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "12",
                            message: "Account number should contains 12 digits"
                        },
                        maxlength: {
                            name: "ng-maxlength", //ng-can't be a key value in JS therefor a property is created
                            value: "12",
                            message: "Account number should contains 12 digits"
                        }

                    },
                    'mashreqDOB': {
                        required: {
                            name: "ng-required",
                            value: "false",
                            message: "lbl_validation_dobRequired"
                        }
                    },
                    'city': {
                        required: {
                            name: "ng-required",
                            value: "false",
                            message: "City is required."
                        }
                    } 
                },
                ChangePassword:{
                    email: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_emailRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value:  "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailFormat"
                        }
                    },
                    password: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_passwordRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^\\S*$/",
                            message: "lbl_validation_passwordAlpha"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "6",
                            message: "lbl_validation_passwordMinChar"
                        }
                    },
                    newPassword :{
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_passwordRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^\\S*$/",
                            message: "lbl_validation_passwordAlpha"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "6",
                            message: "lbl_validation_passwordMinChar"
                        }
                    },
                    confirmPassword: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_confirmPasswordRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^\\S*$/",
                            message: "lbl_validation_confirmPasswordAlpha"
                        },
                        compareField: {
                            name: "compare-field",
                            value: "{{reg.newPassword}}",
                            message: "lbl_validation_confirmPasswordMatch"
                        }
                    }
                },
                creditCard: {
                    'visaNumber': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_creditCardRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            message: 'lbl_validation_creditCardInvalid'
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "16",
                            message: "lbl_validation_creditCardLength"
                        },
                        maxlength: {
                            name: "ng-maxlength",
                            value: "16",
                            message: "lbl_passenger_invalidTaxRegNo"
             
                        }
                    },'masterNumber': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_creditCardRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            message: 'lbl_validation_creditCardInvalid'
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "16",
                            message: "lbl_validation_creditCardLength"
                        }, 
                        maxlength: {
                            name: "ng-maxlength",
                            value: "16",
                            message: "lbl_passenger_invalidTaxRegNo"
             
                        }
                    },
                    'expDateMonth': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_expireDateRequired"
                        }
                    },
                    'expDateYear': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_expireDateRequired"
                        }
                    },
                    'cardCvv': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_cvvRequired"
                        }
                    },
                    'cardHolderName': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_cardHolderNameRequired"
                        }
                    },
                    'cardName': {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_cardNameRequired"
                        }
                    }
                },
                giftVoucher:{
                	email: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_emailRequired"
                        },
                        pattern: {
                            name: "ng-pattern",
                            value: "/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/",
                            message: "lbl_validation_emailFormat"
                        }
                    },
                    telNumber: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_phoneOrTravel"
                        },
                        pattern: {
                            name: "pattern",
                            value: "^[0-9]*",
                            message: "lbl_validation_phoneNumNumeric"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "5",
                            message: "lbl_validation_phoneNumMin"
             
                        },
                        maxlength: {
                            name: "ng-maxlength",
                            value: "13",
                            message: "lbl_validation_phoneNumMax"
             
                        }

                    },
                    firstName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_firstNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                            value: "2",
                            message: "lbl_validation_firstNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_firstNameSymbol'
                        }
                    },
                    lastName: {
                        required: {
                            name: "ng-required",
                            value: "true",
                            message: "lbl_validation_lastNameRequired"
                        },
                        minlength: {
                            name: "ng-minlength",
                            value: "2",
                            message: "lbl_validation_lastNameMinChar"
                        },
                        'pattern': {
                            'name': "ng-pattern",
                            'value': "/^[A-Za-z ]+$/",
                            'message': 'lbl_validation_lastNameSymbol'
                        }
                    },
                   'visaNumber': {
                       required: {
                           name: "ng-required",
                           value: "true",
                           message: "lbl_validation_creditCardRequired"
                       },
                       pattern: {
                           name: "ng-pattern",
                           message: 'lbl_validation_creditCardInvalid'
                       },
                       minlength: {
                           name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                           value: "16",
                           message: "lbl_validation_creditCardLength"
                       },
                       maxlength: {
                           name: "ng-maxlength",
                           value: "16",
                           message: "lbl_validation_creditCardLength"
            
                       }
                   },'masterNumber': {
                       required: {
                           name: "ng-required",
                           value: "true",
                           message: "lbl_validation_creditCardRequired"
                       },
                       pattern: {
                           name: "ng-pattern",
                           message: 'lbl_validation_creditCardInvalid'
                       },
                       minlength: {
                           name: "ng-minlength", //ng-can't be a key value in JS therefor a property is created
                           value: "16",
                           message: "lbl_validation_creditCardLength"
                       }, 
                       maxlength: {
                           name: "ng-maxlength",
                           value: "16",
                           message: "lbl_passenger_invalidTaxRegNo"
            
                       }
                   },
                   'expDateMonth': {
                       required: {
                           name: "ng-required",
                           value: "true",
                           message: "lbl_validation_expireDateRequired"
                       }
                   },
                   'expDateYear': {
                       required: {
                           name: "ng-required",
                           value: "true",
                           message: "lbl_validation_expireDateRequired"
                       }
                   },
                   'cardCvv': {
                       required: {
                           name: "ng-required",
                           value: "true",
                           message: "lbl_validation_cvvRequired"
                       }
                   },
                   'cardHoldersName': {
                       required: {
                           name: "ng-required",
                           value: "true",
                           message: "lbl_validation_cardHolderNameRequired"
                       }
                   }
                }
                
            }
            
        }
        
    }// End of Validation Service
})();