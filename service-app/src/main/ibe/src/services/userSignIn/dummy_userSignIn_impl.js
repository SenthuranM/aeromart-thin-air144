/**
 *      Created by Chamil on 26.01.2016
 */

 (function () {

     angular
        .module("ibeDummyService")
        .service("dummyUserSignIn", ["$http", "$q", "userSignInAdaptorService","loadingService", dummyUserSignInService]);

    function dummyUserSignInService($http, $q, userSignInAdaptorService,loadingService) {

        /**
         * Get Dashboard LMS Details Dummy service call
         * @param data
         * @returns {deferred.promise|*}
         */
        this.signInUser = function (requestData) {

            var path = 'sampleData/customerLoginDetails.json';
            var deferred = $q.defer();

            $http({
                "method": 'GET',
                "url": path,
                "data": requestData
            }).success(function (response) {
                deferred.resolve(userSignInAdaptorService.signInUserAdaptor(response));
            }).error(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };
        this.viewReservations = function (requestData,successFn,errorFn) {
            loadingService.showPageLoading();
            var path = 'sampleData/viewReservations.json';
            var deferred = $q.defer();

            $http({
                "method": 'GET',
                "url": path,
                "data": requestData
            }).success(function (response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            })
            .finally(function () {
              loadingService.hidePageLoading();
             });

            return deferred.promise;
        };
        this.viewCreditHistory = function (requestData,successFn,errorFn) {
            loadingService.showPageLoading();
            var path = 'sampleData/viewCreditHistory.json';
            var deferred = $q.defer();

            $http({
                "method": 'GET',
                "url": path,
                "data": requestData
            }).success(function (response) {
                successFn(response.customerCreditList);
            }).error(function(response) {
                errorFn(response);
            })
             .finally(function () {
                loadingService.hidePageLoading();
           });

            return deferred.promise;
        };
    }

 })();
