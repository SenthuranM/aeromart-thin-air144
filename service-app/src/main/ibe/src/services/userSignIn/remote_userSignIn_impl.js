/**
 *      Created by Chamil on 26.01.2016
 */

 (function () {

     angular
        .module("ibeAPIService")
        .service("remoteUserSignIn", ["$http", "$q", "ConnectionProps", "userSignInAdaptorService","loadingService", remoteUserSignInService]);

    function remoteUserSignInService($http, $q, ConnectionProps, userSignInAdaptorService,loadingService) {
        var self = this;

        var _basePath = ConnectionProps.host;
        var _postRequestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var _getRequestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'text/plain'
            }
        };

        /**
         * Get dashboard LMS details Backend API call
         * @param reservationReq
         * @returns {deferred.promise|*}
         */
        self.signInUser = function (requestData) {
            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/login',
                data: requestData
            });
            var deferred = $q.defer();

            $http(requestParams)
                .success(function (data, status, headers, config) {
                    deferred.resolve(userSignInAdaptorService.signInUserAdaptor(data));
                })
                .error(function (error, status, headers, config) {
                    deferred.reject(error);
                });


            return deferred.promise;
        };
        self.tokenBasedSignInUser = function (requestData) {
            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/login',
                data: requestData
            });
            var deferred = $q.defer();

            $http(requestParams)
                .success(function (data, status, headers, config) {
                    deferred.resolve(userSignInAdaptorService.signInUserAdaptor(data));
                })
                .error(function (error, status, headers, config) {
                    deferred.reject(error);
                });


            return deferred.promise;
        };
        self.viewReservations=function(requestData,successFn,errorFn) {
            loadingService.showPageLoading();
            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/reservationList',
                data: requestData
            });
            var deferred = $q.defer();

            $http(requestParams)
                .success(function (data) {
                    successFn(data);
                })
                .error(function (data) {
                    errorFn(data)
                })
                .finally(function () {
                    loadingService.hidePageLoading();
                });

            return deferred.promise;
        };
        this.viewCreditHistory = function (requestData,successFn,errorFn) {
            loadingService.showPageLoading();
            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/creditDetails',
                data: requestData
            });
            var deferred = $q.defer();

            $http(requestParams)
                .success(function (data) {
                    successFn(data.customerCreditList);
                })
                .error(function (data) {
                    errorFn(data)
                })
                .finally(function () {
                    loadingService.hidePageLoading();
                });

            return deferred.promise;

        };


    } // End of remoteUserSignInService.

 })();
