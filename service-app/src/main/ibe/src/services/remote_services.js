(function (angular) {
    angular.module('ibeAPIService', ['ibeAPIAdaptor']);
    angular.module('ibeAPIAdaptor', []);
    angular.module('ibeDummyService', []);

    var app = angular.module('ibeRemoteService', ['ibeAPIService', 'ibeDummyService']);

    var services = [
        {
            'name': 'availabilityRemoteService',
            'remote': 'remoteAvailability',
            'dummy': 'dummyAvailability'
        },
        {
            'name': 'customerRemoteService',
            'remote': 'remoteCustomer',
            'dummy': 'dummyCustomer'
        },
        {
            'name': 'ancillaryRemoteService',
            'remote': 'remoteAncillary',
            'dummy': 'dummyAncillary'
        },
        {
            'name': 'passengerRemoteService',
            'remote': 'remotePassenger',
            'dummy': 'dummyPassenger'
        },
        {
            'name': 'paymentRemoteService',
            'remote': 'remotePayment',
            'dummy': 'dummyPayment'
        },
        {
            'name': 'voucherRemoteService',
            'remote': 'remoteVoucher',
            'dummy': 'dummyVoucher'
        },
        {
            'name': 'confirmRemoteService',
            'remote': 'remoteConfirm',
            'dummy': 'dummyConfirm'
        },
        {
            'name': 'masterRemoteService',
            'remote': 'remoteMaster',
            'dummy': 'dummyMaster'
        },
        {
            'name': 'modificationRemoteService',
            'remote': 'remoteModification',
            'dummy': 'dummyModification'
        },
        {
            'name': 'userSignInRemoteService',
            'remote': 'remoteUserSignIn',
            'dummy': 'dummyUserSignIn'
        },
        {
            'name': 'registrationRemoteService',
            'remote': 'remoteRegistration',
            'dummy': 'dummyRegistration'
        },
        {
            'name': 'userProfileRemoteService',
            'remote': 'remoteViewProfile',
            'dummy': 'dummyViewProfile'
        },
        {
            'name': 'multiCityRemoteService',
            'remote': 'remoteMultiCityAvailability'
        },
        {
            'name': 'formValidatorService',
            'remote': 'remoteFormValidator',
            'dummy': 'dummyFormValidator'
        },
        {
            'name': 'reProtectRemoteService',
            'remote': 'remoteReProtect'
        }
    ];

    for (var i = 0; i < services.length; i++) {
        app.factory(services[i].name, (function (service) {
            return ["$injector", "ConnectionProps", function ($injector, ConnectionProps) {
                if (ConnectionProps.apiService) {
                    return $injector.get(service.remote);
                } else {
                    return $injector.get(service.dummy);
                }
            }]
        })(services[i]));
    }
})(angular);
