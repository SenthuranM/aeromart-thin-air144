(function(angular) {
    var app = angular.module("ibeDummyService");

    app.service('dummyPayment', ['$http', function($http) {
        this._basePath = '';

        this.retrievePaymentSummary = function(requestParams, successFn, errorFn){
            var path = "sampleData/paymentOptions.json";
            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {}
            }).success(function (response) {
                successFn(response);
            }).error(function (response) {
                errorFn(response);
            });
        };

        this.retrievePaymentOptions = function(availRQ, successFn, errorFn) {
            var path = "sampleData/paymentOptions.json";
            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": {}
            }).success(function(response) {
                var data = response;
                successFn(data);
            }).error(function(response) {
                errorFn();
                console.log(response);
            });
        };

        this.redemptionPaymentAmount = function(redeemData, successFn, errorFn){
            var path = "sampleData/paymentOptions_1.json";
            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": redeemData
            }).success(function(response) {
                var data = response;
                successFn(data);
            }).error(function(response) {
                errorFn();
            });
        };

        this.submitPayment = function(requestParams, successFn, errorFn){
            var path = "sampleData/paymentSuccess.json";
            if (requestParams.makePaymentRQ.paymentOptions==null && requestParams.makePaymentRequest.bookingType ==="ONHOLD"){
                path = "sampleData/paymenntSuccess_onHold.json";
            }else{
                if (requestParams.makePaymentRQ.paymentOptions.paymentGateways[0].gatewayId===4){
                    path = "sampleData/payment3D.json";
                }
                if (requestParams.makePaymentRQ.paymentOptions.paymentGateways[0].gatewayId===2){
                    path = "sampleData/paymentSuccess.json";
                }
                if (requestParams.makePaymentRQ.paymentOptions.paymentGateways[0].gatewayId===5){
                    path = "sampleData/paymentSuccess.json";
                }
                if (requestParams.makePaymentRQ.paymentOptions.paymentGateways[0].gatewayId===1){
                    path = "sampleData/paymentExternal1.json";
                }
            }

            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": requestParams
            }).success(function(response) {
                var data = response;
                successFn(data);
            }).error(function(response) {
                errorFn(response);
            });
        };

        this.paymentReceipt = function(requestParams, successFn, errorFn){
            var path ="sampleData/confirmReservation.json";
            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": requestParams
            }).success(function(response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });
        };

        this.validateCaptcha = function(captchaParam, successFn, errorFn){
            var path = "sampleData/captchaResponse.json";
            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": captchaParam
            }).success(function(response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });
        };

        this.applyBinPromotion = function(binParam, successFn, errorFn) {
            var path = "sampleData/payment_binPromo.json";
            $http({
                "method": 'GET',
                "url": this._basePath + path,
                "data": binParam
            }).success(function(response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });
        };

    }]);
})(angular);