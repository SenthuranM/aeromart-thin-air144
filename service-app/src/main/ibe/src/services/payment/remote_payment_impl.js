(function (angular) {
    var app = angular.module("ibeAPIService");

    app.service('remotePayment', ['$http', 'ConnectionProps', 'paymentAdaptor', 'loadingService', 'applicationService',
        function ($http, ConnectionProps, paymentAdaptor, loadingService, applicationService) {

            this._basePath = ConnectionProps.host;

            this.retrievePaymentOptions = function (requestParams, successFn, errorFn) {
                var path = "";
                var retrievePaymentRQ = {};
                // for balance payment
                if (requestParams.paymode === applicationService.APP_MODES.BALANCE_PAYMENT) {
                    path = "/payment/balance/paymentOptions";
                    retrievePaymentRQ.pnr = requestParams.rqParams.pnr;
                }
                // for modify reservation
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_RESERVATION) {
                    path = "/payment/requote/paymentOptions";
                    retrievePaymentRQ.pnr = requestParams.rqParams.pnr;
                }
                // for modify reservation
                else if (requestParams.paymode === applicationService.APP_MODES.NAME_CHANGE) {
                    path = "/payment/requote/paymentOptions";
                    retrievePaymentRQ.pnr = requestParams.rqParams.pnr;
                }
                // for add modify anci
                else if (requestParams.paymode === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    path = "/payment/anci/modification/paymentOptions";
                    retrievePaymentRQ.pnr = requestParams.rqParams.pnr;
                }
                // for modify segments
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_SEGMENT) {
                    path = "/payment/requote/paymentOptions";
                    retrievePaymentRQ.pnr = requestParams.rqParams.pnr;
                }
                // for add create
                else if (requestParams.paymode === applicationService.APP_MODES.CREATE_RESERVATION) {
                    path = "/payment/paymentOptions";
                    retrievePaymentRQ.paxInfo = paymentAdaptor.adaptPaxInfo(requestParams.paxInfo);
                    retrievePaymentRQ.reservationContact = paymentAdaptor.adaptContact(requestParams.reservationContact);
                    retrievePaymentRQ.binPromotion = requestParams.binPromotion;
                    retrievePaymentRQ.countryCode = requestParams.countryCode;
                    retrievePaymentRQ.stateCode = requestParams.stateCode;
                    retrievePaymentRQ.taxRegNo = requestParams.taxRegNo;
                }
                retrievePaymentRQ.voucherPaymentInProcess = requestParams.voucherPaymentInProcess;
                retrievePaymentRQ.originCountryCode = requestParams.originCountryCode;

                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": retrievePaymentRQ
                }).success(function (response) {

                    // get PaymentOptionsResponse
                    var paymentOptions = paymentAdaptor.adaptPaymentOptions(response);

                    successFn(paymentOptions);

                }).error(function (response) {
                    errorFn(response);
//                    console.log(response);
                });
            };

            this.retrievePaymentSummary = function(requestParams, successFn, errorFn){
                var path = "/payment/create/paymentSummary";
                var retrievePaymentSummaryRQ = {};
                retrievePaymentSummaryRQ.paxInfo = paymentAdaptor.adaptPaxInfo(requestParams.paxInfo);
                retrievePaymentSummaryRQ.onhold = requestParams.onhold;
                retrievePaymentSummaryRQ.binPromo = requestParams.binPromo;
                retrievePaymentSummaryRQ.countryCode = requestParams.countryCode;
                retrievePaymentSummaryRQ.stateCode = requestParams.stateCode;
                retrievePaymentSummaryRQ.taxRegNo = requestParams.taxRegNo;

                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": retrievePaymentSummaryRQ
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };
            this.retrieveAdministrationFee = function (requestParams, successFn, errorFn){
                var path = "";
                switch(requestParams.appMode){
                    case    applicationService.APP_MODES.MODIFY_SEGMENT:
                            path = "/payment/requote/adminFee";
                            break;
                    case    applicationService.APP_MODES.ADD_MODIFY_ANCI:
                            path = "/payment/anci/modification/adminFee";
                            break;
                    case	applicationService.APP_MODES.BALANCE_PAYMENT:
                    		path = "/payment/balance/adminFee";
                    		break;
                    default:
                            path = "/payment/requote/adminFee";
                            break;

                }
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": requestParams.rqParam
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });

            }

            this.redemptionPaymentAmount = function (requestParams, successFn, errorFn) {
                var path = "";
                var redeemDataRQ = {};

                // for balance payment
                if (requestParams.paymode === applicationService.APP_MODES.BALANCE_PAYMENT) {
                    path = "/payment/balance/effectivePayment";
                    redeemDataRQ.pnr = requestParams.rqParams.pnr;
                }
                // for modify reservation
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_RESERVATION) {
                    path = "/payment/requote/effectivePayment";
                    redeemDataRQ.pnr = requestParams.rqParams.pnr;
                }
                // for modify reservation
                else if (requestParams.paymode === applicationService.APP_MODES.NAME_CHANGE) {
                    path = "/payment/requote/effectivePayment";
                    redeemDataRQ.pnr = requestParams.rqParams.pnr;
                }
                // for add modify anci
                else if (requestParams.paymode === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    path = "/payment/anci/modification/effectivePayment";
                    redeemDataRQ.pnr = requestParams.rqParams.pnr;
                }
                // for modify segments
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_SEGMENT) {
                    path = "/payment/requote/effectivePayment";
                    redeemDataRQ.pnr = requestParams.rqParams.pnr;
                }
                // for add create
                else if (requestParams.paymode === applicationService.APP_MODES.CREATE_RESERVATION) {
                    path = "/payment/effectivePayment";
                    redeemDataRQ.paxInfo = paymentAdaptor.adaptPaxInfo(requestParams.paxInfo);
                    redeemDataRQ.reservationContact = paymentAdaptor.adaptContact(requestParams.reservationContact);
                    redeemDataRQ.binPromotion = requestParams.binPromotion;
                }

                redeemDataRQ.paymentOptions = requestParams.paymentOptions;

                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": redeemDataRQ
                }).success(function (response) {
                    var paymentOptions = paymentAdaptor.adaptPaymentOptions(response);
                    successFn(paymentOptions);
                }).error(function (response) {
                    errorFn(response);
                });
            };
            this.getOnholdPNR = function(successFn,errorFn){
                var req = angular.extend({
                    "method": 'POST',
                    url: this._basePath + '/reservation/get/onhold/pnr',
                    data: {}
                });
                $http(req)
                    .success(function (data) {
                        successFn(data)
                    })
                    .error(function (error) {
                        errorFn(error)
                    });

            }


            this.submitPayment = function (requestParams, successFn, errorFn) {
                var path = "";
                var bookingRQ = {};

                // for balance payment submit payment
                if (requestParams.paymode === applicationService.APP_MODES.BALANCE_PAYMENT) {
                    bookingRQ = requestParams.makePaymentRQ;
                    bookingRQ.pnr = requestParams.rqParams.pnr;
                    path = "/payment/balance/booking/makePayment";
                }
                // for modify reservation submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_RESERVATION) {
                    bookingRQ = requestParams.makePaymentRQ;
                    bookingRQ.pnr = requestParams.rqParams.pnr;
                    path = "/payment/requote/booking/makePayment";
                } else if (requestParams.paymode === applicationService.APP_MODES.NAME_CHANGE) {
                    bookingRQ = requestParams.makePaymentRQ;
                    bookingRQ.pnr = requestParams.rqParams.pnr;
                    path = "/payment/requote/booking/makePayment";
                }
                // for add modify anci submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    path = "/payment/anci/modification/makePayment";
                    bookingRQ = requestParams.makePaymentRQ;
                    bookingRQ.pnr = requestParams.rqParams.pnr;
                }
                // for add modify segment submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_SEGMENT) {
                    bookingRQ = requestParams.makePaymentRQ;
                    bookingRQ.pnr = requestParams.rqParams.pnr;
                    path = "/payment/requote/booking/makePayment";
                }
                // for create flow submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.CREATE_RESERVATION) {
                    path = "/reservation/create";
                    bookingRQ.makePaymentRQ = requestParams.makePaymentRQ;
                    bookingRQ.paxInfo = paymentAdaptor.adaptPaxInfo(requestParams.paxInfo);
                    bookingRQ.reservationContact = paymentAdaptor.adaptContact(requestParams.reservationContact);
                    bookingRQ.captcha = requestParams.captcha;
                }
                loadingService.showLoading();

                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": bookingRQ
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

            this.voucherPaymentReceipt = function (requestParams, successFn, errorFn) {
                var path = "/voucher/payment/confirmation";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": requestParams
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

            this.paymentReceipt = function (requestParams, successFn, errorFn) {

                // for balance payment submit payment
                if (requestParams.paymode === applicationService.APP_MODES.BALANCE_PAYMENT) {
                    var path = "/payment/receipt/confirmation/create";
                }
                // for modify reservation submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_RESERVATION) {
                    var path = "/payment/receipt/confirmation/modify";
                } else if (requestParams.paymode === applicationService.APP_MODES.NAME_CHANGE) {
                    var path = "/payment/receipt/confirmation/modify";
                }
                // for add modify anci submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    var path = "/payment/receipt/confirmation/ancillary";
                }
                // for add modify segment submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.MODIFY_SEGMENT) {
                    var path = "/payment/receipt/confirmation/modify";
                }
                // for create flow submit payment
                else if (requestParams.paymode === applicationService.APP_MODES.CREATE_RESERVATION) {
                    var path = "/payment/receipt/confirmation/create";
                }
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": requestParams
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

            this.validateCaptcha = function (captchaParam, successFn, errorFn) {
                var path = "/captcha/verify";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": captchaParam
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

            this.applyBinPromotion = function (binParam, successFn, errorFn) {
                var path = "/payment/binPromotion";

                binParam.paxInfo = paymentAdaptor.adaptPaxInfo(binParam.paxInfo);
                binParam.reservationContact = paymentAdaptor.adaptContact(binParam.reservationContact);

                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": binParam
                }).success(function (response) {
                    var paymentOptions = paymentAdaptor.adaptPaymentOptions(response);
                    successFn(paymentOptions);
                }).error(function (response) {
                    errorFn(response);
                });
            };
            
            this.voidReedemedGiftVoucher = function (params,successFn,errorFn) {
                var path = "/voucher/redeem/void";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": params
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

            /**
             * This Implementation is for redeem voucher code : server G9
             */
            this.reedemedGiftoucher = function (params,successFn,errorFn) {
                var path = "/voucher/redeemVouchers";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": params
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

            this.getLMSPoints = function (successFn, errorFn) {
                var path = "/customer/getLMSPoints";

                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": {}
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };

            this.printVouchers = function (params,successFn,errorFn) {
                var path = "/voucher/printVouchers";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": params
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };
            
            this.emailVouchers = function (params,successFn,errorFn) {
                var path = "/voucher/emailVouchers";
                $http({
                    "method": 'POST',
                    "url": this._basePath + path,
                    "data": params
                }).success(function (response) {
                    successFn(response);
                }).error(function (response) {
                    errorFn(response);
                });
            };
            
            this.saveUserCardDetail = function (saveCardData, success, errorFn) {
            	var path = this._basePath + "/payment/createAlias";
            	
                $http({
    				"method" : 'POST',
    				"url" : path,
    				"data" : saveCardData
    			}).success(function(response) {
    				success(response);
    			}).error(function(error) {
    				errorFn(error);
    			});
            };
            
           // get data for select payment type drop down list
            this.savedCardPaymentTypeList = function(success, errorFn) {
                var apiURL = this._basePath + "/customer/alias/customerAliasList";
                $http({
                    "method": "GET",
                    "url": apiURL,
                    "data": {},
                    "headers": {
                        'content-Type': 'application/json'
                    }
                }).success(function(response) {
                    success(response);
                }).error(function(error) {
                    errorFn(error);
                });
            };
            // end of get data for select payment type drop down list
            
            /* this.reservationSummary = function(param, successFn, errorFn) {
             var path = "/ancillary/price/reservation/create";
             $http({
             "method": 'POST',
             "url": this._basePath + path,
             "data": param
             }).success(function(response) {
             successFn(ancillaryAdaptor.getPriceQuoteAncillary(response));
             }).error(function(response) {
             errorFn(response);
             });
             };*/

        }]);
})(angular);
