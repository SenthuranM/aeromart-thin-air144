/**
 *      Created by Chamil on 26.01.2016
 */

 (function () {

     angular
        .module("ibeAPIService")
        .service("remoteRegistration", ["$http", "$q", "ConnectionProps", "registrationAdaptorService", remoteRegistrationService]);

    function remoteRegistrationService($http, $q, ConnectionProps, registrationAdaptorService) {
        var self = this;

        var _basePath = ConnectionProps.host;
        var _postRequestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var _getRequestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'text/plain'
            }
        };


        self.registerUser = function (params, successFn, errorFn) {
            params = registrationAdaptorService.adaptRegisterUserRequest(params);

            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/register',
                data: params
            });

            $http(requestParams)
                .success(function (data, status, headers, config) {
                    successFn(registrationAdaptorService.registerUser(data));
                })
                .error(function (error, status, headers, config) {
                    errorFn(error);
                });

        }; // End of registerUser.

        self.updateUser = function (params, successFn, errorFn) {
            params = registrationAdaptorService.adaptRegisterUserRequest(params);

            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/update',
                data: params
            });

            $http(requestParams)
                .success(function (data, status, headers, config) {
                    successFn(data);
                })
                .error(function (error, status, headers, config) {
                    errorFn(error);
                });

        }; // End of updateUser.

        self.changePassword = function (params, successFn, errorFn) {

            debugger;
            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/changePassword',
                data: params
            });

            $http(requestParams)
                .success(function (data) {
                    successFn(data);
                })
                .error(function (error) {
                    errorFn(error);
                });

        };


        self.getEmailFromToken = function (token, successFn, errorFn) {

            var requestParams = angular.extend({}, _getRequestOptions, {
                url: _basePath + '/customer/getEmailFromToken?token=' + encodeURIComponent(token)
            });

            $http(requestParams)
                .success(function (data) {
                    successFn(data);
                })
                .error(function (error) {
                    errorFn(error);
                });

        };
        self.confirmLmsUser = function (params, successFn, errorFn) {

            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/confirmLMSMember',
                data: params
            });

            $http(requestParams)
                .success(function (data) {
                    successFn(data);
                })
                .error(function (error) {
                    errorFn(error);
                });
        }
    self.confirmCutomer = function (params, successFn, errorFn) {
            
            var requestParams = angular.extend({}, _postRequestOptions, {
                url: _basePath + '/customer/confirmCustomer',
                data: params
            });

            $http(requestParams)
                .success(function (data, status, headers, config) {
                    successFn(data);
                })
                .error(function (error, status, headers, config) {
                    errorFn(error);
                });
        }


    } // End of remoteUserSignInService.

 })();
