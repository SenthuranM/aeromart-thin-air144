/**
 *      Created by Chamil on 26.01.2016
 */

 (function () {

     angular
        .module("ibeDummyService")
        .service("dummyRegistration", ["$http", "$q", "registrationAdaptorService", dummyRegistrationService]);

    function dummyRegistrationService($http, $q, registrationAdaptorService) {
        var self = this;

        self.registerUser = function (params, successFn, errorFn) {

            var path = 'sampleData/userRegistrationResponse.json';
            var deferred = $q.defer();

            $http({
                "method": 'GET',
                "url": path,
                "data": params
            }).success(function (response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });

            return deferred.promise;
        }; // End of registerUser.

        self.updateUser = function (params, successFn, errorFn) {

            var path = 'sampleData/userRegistrationResponse.json';
            var deferred = $q.defer();

            $http({
                "method": 'GET',
                "url": path,
                "data": params
            }).success(function (response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });

            return deferred.promise;
        }; // End of updateUser.

    }

 })();
