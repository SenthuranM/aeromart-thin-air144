## How to do a frontend build for a given client

Please refer to the frontend build guide at:
http://confluence.cmb.isaaviations.com/display/dev/Build+Guide For the updated build information.


---------------------------
| Steps to IBE theme      |
---------------------------

Set new Client & theme mode for grunt serve
1.grunt setMode --themeable=true
2.grunt createClient --client=<new client name>

To start modification
-----------------------------------------------------
3.grunt resetClient serve --client=<new client name>
4.then create the theme and save the theme as a json in the json folder of newly built client
build new theme
to buildthe theme got ThemEngine and run following commands
5.node buildTheme --appClient "<new client name>"
Finally
6.grunt distribute --client=<client name>
-----------------------------------------------------
to minify js file make minification = true in grunt file