const Glue = require('glue');
const Config = require('./config');
const proxy = require('./private/config/server_path_config.js');

const options = {
    relativeTo: __dirname + '/private/modules'
};

Glue.compose(Config.manifest, options, function (err, server) {

    if (err) {
        throw err;
    }

    server.route([{
        method: 'GET',
        path: "/" + Config.context.applicationContext + '/{param*}',
        handler: {
            directory: {
                path: 'server/public',
                listing: false,
                index: true
            }
        }
    },
        {
            method: 'GET',
            path: '/js_scripts/ond.js',
            handler: {
                proxy: {
                    uri: 'https://'+proxy.clientProxy.host+'/js_scripts/ond.js'
                }
            }
        }
    ]);

    // Start the server
    server.start(function (err) {
        if (err) {
            throw err;
        }
        console.log('Server running at:', server.info.uri);
    });
})
;