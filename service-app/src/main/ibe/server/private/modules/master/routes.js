'use strict';

const proxy = require('../../config/server_path_config.js');

module.exports = [
    {
        method: ['POST', 'GET'],
        path: '/controller/{path*}',
        handler: {
            proxy:proxy.clientProxy
        }
        ,
        config: {
            tags: ['api'],
            description: 'IBE configuration',
            notes: 'This api return IBE configurations such as seat map enabled, meals enabled ... etc'
        }
    },
    {
        method: ['GET'],
        path: '/ibe/jcaptcha.jpg',
        handler: {
            proxy: proxy.clientProxy
        }
        ,
        config: {
            tags: ['api'],
            description: 'IBE configuration',
            notes: 'This api return IBE configurations such as seat map enabled, meals enabled ... etc'
        }
    }
];
