var connect = require('connect');
var staticServer = require('serve-static');
var livereload = require('livereload');
var opn = require('opn');
var url = require('url');
var proxy = require('proxy-middleware');

//TODO: Copy live reload script to html

var port = process.env.npm_package_config_port || 35730;
var server = connect();

server.use('/service-app/controller', proxy(url.parse('http://10.20.11.186:8180/service-app/controller')));

server.use(staticServer('./'));
server.listen(port);

server = livereload.createServer();
server.watch("./");

console.log('started server at port ' + port);
opn("http://localhost:" + port + "/www");