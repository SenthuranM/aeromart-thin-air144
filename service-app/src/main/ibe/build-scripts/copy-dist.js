var copy = require('copy');
var mkdirp = require('mkdirp');

mkdirp('./dist', function (err) {
    if (err) console.error(err);
    else {
        console.log('dist created!');

        copy('./www/assets/**/*.*', './dist/assets', function (err, files) {
            console.log('Assets Added');
        });

        copy('./www/*.*', './dist', function (err, files) {
            console.log('Base level files copied');
        });
    }
});
