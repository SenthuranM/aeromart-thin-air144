var copy = require('copy');
var DEMO_CLIENT = 'ISA';
var fs = require('fs');

var client = process.env.npm_package_config_client || DEMO_CLIENT;
var apStatus = process.env.npm_package_config_manageTheme;
var cretePorperties = fs.createWriteStream('themeProperty.json');
var sassport = require('sassport');
var compressor = require('yuicompressor');

cretePorperties.on('close', function() {
    console.log('property file file created');
});

console.log('Copying AeroMart base files');
console.log('aplication themable' + apStatus);
var property ={ "themeEnginStatus" : apStatus}
fs.writeFile('./src/themeProperty.json', JSON.stringify(property), function(err){
    if(!err){
        console.log('---------------- new theme created ----------------');
        copy('clients/' + client + '/styles/common/*.*', 'src/js/util/isaui_lib/ui_lib/module/styles/common/', function (err, files) {
            copy('src/**/*.*', 'www', function (err, files) {
                console.log('Base file copy complete!');

                console.log('Copying Client files for ' + client);

                copy('clients/' + client + '/**/*.*', 'www', function (err, files) {
                    console.log('Client files copied');
                    fs.unlink('./www/js/util/isaui_lib/dist/css/isaui_lib.css',function(error){
                        console.log('inside unlink');
                        console.log(error)
                        if(!error){
                            sassport().render({
                                file: './www/js/util/isaui_lib/ui_lib/module/styles/isaui_lib.scss',
                                outFile: './www/js/util/isaui_lib/dist/css/isaui_lib.css',
                                sourceMap: true
                            }, function(error, result){
                                console.log(result)
                                fs.writeFile('./www/js/util/isaui_lib/dist/css/isaui_lib.css', result.css, function(err){
                                    if(!err){
                                        console.log('---------------- ui library updates Complete ----------------');
                                        compressor.compress('./www/js/util/isaui_lib/dist/css/isaui_lib.css', {
                                            //Compressor Options:
                                            charset: 'utf8',
                                            type: 'css',
                                            nomunge: true,
                                            'line-break': 80
                                        }, function(err, data, extra) {
                                            //err   If compressor encounters an error, it's stderr will be here
                                            //data  The compressed string, you write it out where you want it
                                            //extra The stderr (warnings are printed here in case you want to echo them
                                            //console.log(data)
                                            if(!err){
                                                fs.writeFile('./www/js/util/isaui_lib/dist/css/isaui_lib.min.css', data, function(err){
                                                    if(!err){
                                                        console.log('---------------- minify completed for ui lib ----------------');
                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                            })
                        }
                    });
                });


            });
        });

    }else{
        console.log(err)
    }
});

