var watch = require('watch');
var copy = require('copy');
var sass = require('node-sass');
var fs = require('fs');
var sassport = require('sassport');
var compressor = require('yuicompressor');

var DEMO_CLIENT = 'de';
var client = process.env.npm_package_config_client || DEMO_CLIENT;

var appPath = 'src';
var clientPath = 'clients/' + client;

watch.watchTree(appPath, function (f, curr, prev) {


    if (typeof f == "object" && prev === null && curr === null) {
        // Finished walking the tree
    } else if (curr.nlink === 0) {
        // f was removed
        //TODO: if f !exist in client remove it
    } else {
        // f was changed
        if (!isFileInClient(f)) {
            console.log('------ FILE CHANGED in /app - Updating... ------');
            updateFile(f, appPath);
        }
    }
});

watch.watchTree(clientPath, function (f, curr, prev) {
    console.log('------ FILE CHANGED in /'+ clientPath +' - Updating... ------');
    if (typeof f == "object" && prev === null && curr === null) {
        // Finished walking the tree
    } else if (curr.nlink === 0) {
        // f was removed
        //TODO: if f !exist in /app remove it
        //TODO: if f exist in /app, move app/../file to www/../file
    } else {
        updateFile(f, clientPath);
    }
});

function isFileInClient(file) {
    // Check for file in client folder
    try{
        var stats = fs.lstatSync(file.replace(appPath, clientPath));
        return true;
    }catch(ex){
        return false;
    }
}

function updateFile(file, path) {
    //Copy file to www
    var ext = file.substr(file.lastIndexOf('.') + 1);
    var copyToPath = file.replace(path, './www');
    copyToPath = copyToPath.substring(0, copyToPath.lastIndexOf("/") + 1);

    copy(file, copyToPath, {flatten: true}, function (err, files) {
        if (err) { console.log('error' + err); }

        if (ext === 'scss') {
            //Compile scss and move app.css
            console.log('scss changed: Compiling scss');
            fs.unlink('./www/js/util/isaui_lib/dist/css/isaui_lib.css',function(error){
                if(error){
                    console.log('ui lib css not removed')
                }
            });

            sassport().render({
                file: './www/styles/main.scss',
                outFile: './www/styles/main.css',
                sourceMap: true
            }, function(error, result){
                //console.log(result.css.toString())
                fs.writeFile('./www/styles/main.css', result.css, function(err){
                    if(!err){
                        console.log('---------------- Update Complete ----------------');
                    }
                });
            });

            sassport().render({
                file: './www/styles/dashboard.scss',
                outFile: './www/styles/dashboard.css',
                sourceMap: true
            }, function(error, result){
                fs.writeFile('./www/styles/dashboard.css', result.css, function(err){
                    if(!err){
                        console.log('---------------- Update Complete ----------------');
                    }
                });
            })
            sassport().render({
                file: './www/js/util/isaui_lib/ui_lib/module/styles/isaui_lib.scss',
                outFile: './www/js/util/isaui_lib/dist/css/isaui_lib.css',
                sourceMap: true
            }, function(error, result){
                console.log(result)
                fs.writeFile('./www/js/util/isaui_lib/dist/css/isaui_lib.css', result.css, function(err){
                    if(!err){
                        console.log('---------------- ui library updates Complete ----------------');
                        compressor.compress('./www/js/util/isaui_lib/dist/css/isaui_lib.css', {
                            //Compressor Options:
                            charset: 'utf8',
                            type: 'css',
                            nomunge: true,
                            'line-break': 80
                        }, function(err, data, extra) {
                            //err   If compressor encounters an error, it's stderr will be here
                            //data  The compressed string, you write it out where you want it
                            //extra The stderr (warnings are printed here in case you want to echo them
                            //console.log(data)
                            if(!err){
                                fs.writeFile('./www/js/util/isaui_lib/dist/css/isaui_lib.min.css', data, function(err){
                                    if(!err){
                                        console.log('---------------- minify completed for ui lib ----------------');
                                    }
                                });

                            }
                        });
                    }
                });
            })

        }else{
            console.log('---------------- Update Complete ----------------');
        }
    });

}