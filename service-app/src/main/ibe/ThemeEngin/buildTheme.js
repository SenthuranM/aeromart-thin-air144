/**
 * Created by indika on 9/5/16.
 */
var fs = require('fs');
var jsonSass = require('json-sass');
var argv = require('minimist')(process.argv.slice(2));
var copy = require('copy');
var unzip  = require('unzip')
var fstream = require('fstream');

console.log(argv.appClient)
fs.createReadStream('../clients/'+argv.appClient+'/json/new-color-theme.json')
    .pipe(jsonSass({
        prefix: '$colors: '
    }))
    .pipe(fs.createWriteStream('../clients/'+argv.appClient+'/styles/common/theme.scss'))
    .on('finish',function(){

        copy('../clients/' + argv.appClient + '/styles/common/*.*', '../src/styles/common/', function (err, files) {
            if (!err) {
                console.log('Theme styles copied to the source');
            }
        });
        copy('../clients/' + argv.appClient + '/images/*.*', '../src/images/', function (err, files) {
            if (!err) {
                console.log('Theme images copied to the source');

                fs.createReadStream('../src/images/images.zip')
                    .on('error', function(e){
                        console.log('images.zip not used');
                    })
                    .pipe(unzip.Extract({ path: '../src' }))
                    .on('close', function () {
                        console.log('-------------- file unzip completed---------------');
                        fs.unlinkSync('../clients/' + argv.appClient + '/images/images.zip');
                        fs.unlinkSync('../src/images/images.zip');
                    })
            }
        });

        console.log('json to sass done')
    });
