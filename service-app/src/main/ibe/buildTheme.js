var fs = require('fs');
var jsonSass = require('json-sass');
var argv = require('minimist')(process.argv.slice(2));
var copy = require('copy');
var unzip  = require('unzip');
var fstream = require('fstream');

console.log(argv.client);
fs.createReadStream('clients/'+argv.client+'/json/new-color-theme.json')
    .pipe(jsonSass({
        prefix: '$colors: '
    }))
    .pipe(fs.createWriteStream('clients/'+argv.client+'/styles/common/theme.scss'))
    .on('finish',function(){

        copy('clients/' + argv.client + '/styles/common/*.*', 'src/styles/common/', function (err, files) {
            if (!err) {
                console.log('Theme styles copied to the source');
            }
        });
        copy('clients/' + argv.client + '/images/*.*', 'src/images/', function (err, files) {
            if (!err) {
                console.log('Theme images copied to the source');

                fs.createReadStream('src/images/images.zip')
                    .on('error', function(e){
                        console.log('images.zip not used');
                    })
                    .pipe(unzip.Extract({ path: 'src' }))
                    .on('close', function () {
                        console.log('-------------- file unzip completed---------------');
                        fs.unlinkSync('clients/' + argv.client + '/images/images.zip');
                        fs.unlinkSync('src/images/images.zip');
                    })
            }
        });

        console.log('json to sass done')
    });
