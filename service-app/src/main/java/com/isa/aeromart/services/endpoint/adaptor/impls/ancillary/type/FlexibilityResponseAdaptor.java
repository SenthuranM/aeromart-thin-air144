package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItems;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

public class FlexibilityResponseAdaptor extends AncillariesResponseAdaptor {
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability, LCCAncillaryAvailabilityInDTO.LCCAncillaryType.FLEXI);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	public List<OndUnit> getOndUnits(Object ancillaries, RPHGenerator rphGenerator) {
		List<OndUnit> ondUnits = new ArrayList<>();
		OndUnit ondUnit;

		AvailableFlexiResponseWrapper flexiWrapper = (AvailableFlexiResponseWrapper) ancillaries;

		Map<String, List<String>> onds = new LinkedHashMap<>();
		List<String> segments;
		
		List<SessionFlexiDetail> sessionFlexiDetailList = flexiWrapper.getSessionFlexiDetailList();
		List<InventoryWrapper> inventory = flexiWrapper.getInventory();
		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);

		for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiDetailList) {
			if(sessionFlexiDetail.isFlexiAvailable()){
				segments = new ArrayList<String>();
				for(FlightSegmentTO flightSegmentTO : flightSegmentTOs){
					try {
						if (sessionFlexiDetail.getOndSequence() == flightSegmentTO.getOndSequence()
								&& !ModuleServiceLocator.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())) {
							segments.add(rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber()));
						}
					} catch (ModuleException e) {
						throw new ValidationException(ValidationException.Code.ANCI_AVAILABLE_GENERIC_ERROR, e);
					}
				}
				onds.put(Integer.toString(sessionFlexiDetail.getOndSequence()), segments);	
			}
		}

		for (String ondId : onds.keySet()) {
			ondUnit = new OndUnit();
			ondUnit.setOndId(ondId);
			ondUnit.setFlightSegmentRPH(onds.get(ondId));
			ondUnits.add(ondUnit);
		}

		return ondUnits;
	}

	public AncillaryType
			toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		AncillaryType ancillaryType = new AncillaryType();
		AvailableFlexiResponseWrapper flexiWrapper = (AvailableFlexiResponseWrapper) ancillaries;

		List<SessionFlexiDetail> sessionFlexiDetailList = flexiWrapper.getSessionFlexiDetailList();

		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode());

		List<Provider> providers = new ArrayList<>();
		Provider provider = new SystemProvided();

		AvailableAncillary availableAncillary = new AvailableAncillary();

		List<AvailableAncillaryUnit> availableAncillaryUnits = new ArrayList<>();
		AvailableAncillaryUnit availableAncillaryUnit;
		OndScope ondScope;

		Applicability applicability = new Applicability();
		applicability.setApplicableType(Applicability.ApplicableType.PASSENGER.name());

		FlexiItems itemsGroup;
		List<FlexiItem> items;
		FlexiItem item;
		List<Charge> charges;
		Charge charge;
		
		for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiDetailList) {
			if (sessionFlexiDetail.isFlexiAvailable()) {
				ondScope = new OndScope();
				ondScope.setScopeType(Scope.ScopeType.OND);
				ondScope.setOndId(Integer.toString(sessionFlexiDetail.getOndSequence()));

				availableAncillaryUnit = new AvailableAncillaryUnit();
				availableAncillaryUnit.setScope(ondScope);
				availableAncillaryUnit.setApplicability(applicability);

				itemsGroup = new FlexiItems();
				items = new ArrayList<>();

				item = new FlexiItem();
				item.setItemId(sessionFlexiDetail.getExternalChargeSummary().get(0).getSurchargeCode());
				item.setDefaultSelected(sessionFlexiDetail.isFlexiSelected());

				charges = new ArrayList<>();
				charge = new Charge();
				charge.setChargeBasis(Charge.ChargeBasis.ALL_PASSENGERS.name());
				charge.setAmount(sessionFlexiDetail.getTotalFlexiCharge());
				charges.add(charge);
				item.setCharges(charges);

				items.add(item);

				itemsGroup.setItems(items);
				availableAncillaryUnit.setItemsGroup(itemsGroup);
				availableAncillaryUnits.add(availableAncillaryUnit);
			}

		}

		availableAncillary.setAvailableUnits(availableAncillaryUnits);
		provider.setAvailableAncillaries(availableAncillary);
		providers.add(provider);
		ancillaryType.setProviders(providers);

		return ancillaryType;
	}

	@Override
	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {
		return null;
	}

	public static class AvailableFlexiResponseWrapper {
		private List<SessionFlexiDetail> SessionFlexiDetailList;

		private List<InventoryWrapper> inventory;

		public List<SessionFlexiDetail> getSessionFlexiDetailList() {
			return SessionFlexiDetailList;
		}

		public void setSessionFlexiDetailList(List<SessionFlexiDetail> sessionFlexiDetailList) {
			SessionFlexiDetailList = sessionFlexiDetailList;
		}

		public List<InventoryWrapper> getInventory() {
			return inventory;
		}

		public void setInventory(List<InventoryWrapper> inventory) {
			this.inventory = inventory;
		}

	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

	}
}
