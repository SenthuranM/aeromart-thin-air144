package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;

public  class PricedAncillaryItem {

	private PricedSelectedItem pricedSelectedItem;

	private List<MonetaryAmendment> amendments;

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}

	public PricedSelectedItem getPricedSelectedItem() {
		return pricedSelectedItem;
	}

	public void setPricedSelectedItem(PricedSelectedItem pricedSelectedItem) {
		this.pricedSelectedItem = pricedSelectedItem;
	}
}
