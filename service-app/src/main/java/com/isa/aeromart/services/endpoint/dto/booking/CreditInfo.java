package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Date;

public class CreditInfo {

	private String amount;

	private String operatingCarrier;

	private String credRef;

	private Date expireDate;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCredRef() {
		return credRef;
	}

	public void setCredRef(String credRef) {
		this.credRef = credRef;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

}
