package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

public class AmountObject {

	private BigDecimal paymentCurrencyValue;
	private BigDecimal baseCurrencyValue;

	public AmountObject() {
		this.paymentCurrencyValue = BigDecimal.valueOf(0.00);
		this.baseCurrencyValue = BigDecimal.valueOf(0.00);
	}

	public BigDecimal getPaymentCurrencyValue() {
		return paymentCurrencyValue;
	}

	public void setPaymentCurrencyValue(BigDecimal paymentCurrency) {
		this.paymentCurrencyValue = paymentCurrency;
	}

	public BigDecimal getBaseCurrencyValue() {
		return baseCurrencyValue;
	}

	public void setBaseCurrencyValue(BigDecimal baseCurrency) {
		this.baseCurrencyValue = baseCurrency;
	}

}
