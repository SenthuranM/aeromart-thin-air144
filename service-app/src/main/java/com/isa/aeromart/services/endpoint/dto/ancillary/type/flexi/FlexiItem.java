package com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;

public class FlexiItem extends AncillaryItem{

	private Boolean defaultSelected;

	public Boolean getDefaultSelected() {
		return defaultSelected;
	}

	public void setDefaultSelected(Boolean defaultSelected) {
		this.defaultSelected = defaultSelected;
	}

}
