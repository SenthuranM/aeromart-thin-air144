package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.util.Iterator;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ONDFareAdaptor extends ONDBaseChargeAdaptor {

	public ONDFareAdaptor(ONDPriceAdaptor ondAdaptor) {
		super(ondAdaptor);
	}

	public void addOWFare(String paxType, List<BaseFareTO> fares) {
		Iterator<BaseFareTO> iterator = fares.iterator();
		while (iterator.hasNext()) {
			BaseFareTO fare = iterator.next();
			addCharge(paxType, fare);
		}
	}

	public void addCharge(String paxType, BaseFareTO fare) {
		PaxPrice paxPrice = getPaxPrice(paxType, fare);
		OndOWPricing ondPrice = getOndPrice(fare.getOndSequence());
		paxPrice.setFare(fare.getAmount());
		paxPrice.setTotalFare(AccelAeroCalculator.add(paxPrice.getTotalFare(), fare.getAmount()));
		paxPrice.setTotal(AccelAeroCalculator.add(paxPrice.getTotal(), fare.getAmount()));
		ondPrice.setTotalPrice(AccelAeroCalculator.add(ondPrice.getTotalPrice(), fare.getAmount()));
	}

}
