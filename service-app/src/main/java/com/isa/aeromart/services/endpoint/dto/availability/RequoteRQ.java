package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.SpecificFlight;

public class RequoteRQ extends TransactionalBaseRQ {

	private boolean cancelSegment;

	private boolean modifySegment;
	
	private boolean groupPnr;

	private ModificationInfo modificationInfo;

	private List<SpecificFlight> flightSegments;

	public ModificationInfo getModificationInfo() {
		return modificationInfo;
	}

	public void setModificationInfo(ModificationInfo modificationInfo) {
		this.modificationInfo = modificationInfo;
	}

	public List<SpecificFlight> getFlightSegments() {
		return flightSegments;
	}

	public void setFlightSegments(List<SpecificFlight> flightSegments) {
		this.flightSegments = flightSegments;
	}

	public boolean isCancelSegment() {
		return cancelSegment;
	}

	public void setCancelSegment(boolean cancelSegment) {
		this.cancelSegment = cancelSegment;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}
	
	
}
