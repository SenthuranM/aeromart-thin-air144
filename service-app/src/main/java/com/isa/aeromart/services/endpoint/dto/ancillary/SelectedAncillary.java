package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class SelectedAncillary extends Ancillary {

    private List<Preference> preferences;

    public List<Preference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
    }
}
