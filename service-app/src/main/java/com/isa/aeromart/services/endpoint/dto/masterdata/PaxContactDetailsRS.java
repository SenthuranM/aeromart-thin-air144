package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PaxContactDetailsRS extends TransactionalBaseRS{

	private Map<String , PaxContactDetailsField> contactDetails;
	
	private boolean isServiceTaxApplicable;

	public Map<String, PaxContactDetailsField> getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(Map<String, PaxContactDetailsField> contactDetails) {
		this.contactDetails = contactDetails;
	}

	public boolean isServiceTaxApplicable() {
		return isServiceTaxApplicable;
	}

	public void setServiceTaxApplicable(boolean isServiceTaxApplicable) {
		this.isServiceTaxApplicable = isServiceTaxApplicable;
	}

}
