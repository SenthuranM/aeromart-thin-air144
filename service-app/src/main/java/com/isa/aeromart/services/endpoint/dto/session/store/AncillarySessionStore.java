package com.isa.aeromart.services.endpoint.dto.session.store;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherInformation;
import com.isa.aeromart.services.endpoint.dto.session.AncillaryTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySession;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;

public interface AncillarySessionStore {
	public static String SESSION_KEY = AncillaryTransaction.SESSION_KEY;

    RPHGenerator getRPHGenerator();

	SessionBasicReservation getReservationData();

	AncillarySession getAncillarySession();
	
	List<SessionFlightSegment> getSegmentWithInventory();

    FinancialStore getFinancialStore();

	FareSegChargeTO getFarePricingInformation();
	
	TravellerQuantity getTravellerQuantityForAncillary();
	
	PreferenceInfo getPreferencesForAncillary();
	

	ReservationInfo getResInfo();

    List<ModifiedFlight> getModifiedFlightsInfo();

    Map<String, String> getResSegRphToFlightSegMap();

    PostPaymentDTO getPostPaymentInformation();

    LoyaltyInformation getLoyaltyInformation();

	void storeResInfo(ReservationInfo resInfo);

    void storeRPHGeneratorForAncillaryModification(RPHGenerator rphGenerator);
    
    void storeReservationDataForAncillaryModification(ReservationInfo resInfo);
    
    void storeSessionFlightsForAncillaryModification(List<FlightSegmentTO> segments);
    
    void storePreferenceInfoForAncillaryModification(PreferenceInfo preferenceInfo);    
    
    void storePricingInformationForAncillaryModification(FareSegChargeTO pricingInformation);  
    
    List<SessionFlexiDetail> getSessionFlexiDetailForAncillary();
    
    DiscountedFareDetails getDiscountedFareDetailsForAncillary();
    
    List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalChargesForAncillary();
    
    List<SessionFlightSegment> getSelectedSegmentsForAncillary();
    
    void storeUpdatedSessionFlightSegment(List<SessionFlightSegment> segments);
    
    void setDiscountAmount(BigDecimal discountAmount);
    
    void storeServiceCount(Map<String, Integer>  serviceCount);
    
    void storeApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes);
    
    Map<String, Integer> getServiceCount();
    
    boolean isOnHoldCreated();

	List<SessionFlightSegment> getAllSegmentWithInventory();

	public PayByVoucherInfo getPayByVoucherInfo();

    public VoucherInformation getVoucherInformation();

	public Map<String, String> getVoucherOTPMap();
}
