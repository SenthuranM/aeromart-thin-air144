package com.isa.aeromart.services.endpoint.dto.ancillary;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.BasicRuleValidation;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.DateRestrict;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.EditAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.InsurancePlansCombinations;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.MultipleSelections;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.RowSeatPassengerCount;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.SeatsPassengerTypes;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "ruleCode", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = MultipleSelections.class, name = BasicRuleValidation.RuleCode.MULTIPLE_SELECTIONS),
        @JsonSubTypes.Type(value = SeatsPassengerTypes.class, name = BasicRuleValidation.RuleCode.RESTRICTED_PAX_TYPES_FOR_SEAT),
        @JsonSubTypes.Type(value = InsurancePlansCombinations.class, name = BasicRuleValidation.RuleCode.INSURANCE_PLANS_COMBINATIONS),
        @JsonSubTypes.Type(value = RowSeatPassengerCount.class, name = BasicRuleValidation.RuleCode.PAX_TYPE_COUNT_FOR_ROW_SEATS),
        @JsonSubTypes.Type(value = EditAncillary.class, name = BasicRuleValidation.RuleCode.EDIT_ANCILLARY),
        @JsonSubTypes.Type(value = DateRestrict.class, name = BasicRuleValidation.RuleCode.CALENDAR_DATE_RESTRICT),
})
public abstract class Rule {
	
	private enum RuleType {
		VALIDATOR, DEFINITION
	}

    private Integer ruleValidationId;

    private String ruleCode;

    public Integer getRuleValidationId() {
        return ruleValidationId;
    }

    public void setRuleValidationId(Integer ruleValidationId) {
        this.ruleValidationId = ruleValidationId;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }
}
