package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;


public class CountryListRS extends TransactionalBaseRS{

	private List<CountryDetails> countryList;

	public List<CountryDetails> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<CountryDetails> countryList) {
		this.countryList = countryList;
	}
	
}
