package com.isa.aeromart.services.endpoint.adaptor.impls.booking;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.TravelModeAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.PassengerResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.BookingConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.booking.PromoTypeInfo;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationAlert;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationSegmentLite;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;

public class ConfirmationResAdaptor extends LoadReservationAdaptor<BookingConfirmationRS> {

	private String carURL;

	private String hotelURL;

	CurrencyExchangeRate currencyExchangeRate;
	
	private final static String YES = "Y";
	
	private final static String NO = "N";


	public ConfirmationResAdaptor(String carURL, String hotelURL, CurrencyExchangeRate currencyExchangeRate) {
		this.carURL = carURL;
		this.hotelURL = hotelURL;
		this.currencyExchangeRate = currencyExchangeRate;
	}
	@Override
	public BookingConfirmationRS adapt(LCCClientReservation reservation) {
		BookingConfirmationRS bookingConfirmationRS = new BookingConfirmationRS();
		Set<LCCClientReservationPax> lccClientReservationPassengers = reservation.getPassengers();

		PassengerResAdaptor passengerResAdaptor = new PassengerResAdaptor();
		ReservationContactResAdaptor reservationContactResAdaptor = new ReservationContactResAdaptor();
		ReservationAlertAdaptor reservationAlertAdaptor = new ReservationAlertAdaptor(null);
		PromoInfoResAdaptor promoInfoResAdaptor = new PromoInfoResAdaptor();
		BookingInfoResAdaptor bookingInfoResAdaptor = new BookingInfoResAdaptor();
		ReservationSegmentLiteAdaptor reservationSegmentLiteAdaptor = new ReservationSegmentLiteAdaptor();
		PaymentDetailsAdaptor paymentDetailsAdaptor = new PaymentDetailsAdaptor();
		BalanceSummaryAdaptor balanceSummaryAdaptor = new BalanceSummaryAdaptor(currencyExchangeRate);

		ArrayList<Passenger> passengers = new ArrayList<>();
		ArrayList<ReservationAlert> reservationAlertInfo = new ArrayList<>();
		ArrayList<ReservationSegmentLite> reservationSegments = new ArrayList<>();


		bookingConfirmationRS.setContactInfo(reservationContactResAdaptor.adapt(reservation.getContactInfo()));

		PromoTypeInfo promoTypeInfo = new PromoTypeInfo();
		if (reservation.getLccPromotionInfoTO() != null) {
			promoTypeInfo = promoInfoResAdaptor.adapt(reservation.getLccPromotionInfoTO());
		}
		bookingConfirmationRS.setPromoTypeInfo(promoTypeInfo);

		List<LCCClientReservationSegment> resSegmentList = new ArrayList<>(reservation.getSegments());
		Collections.sort(resSegmentList);

		ReservationSegmentLite reservationSegmentLite;
		setReturnFlagForSegments(resSegmentList);
		for (LCCClientReservationSegment segment : resSegmentList) {
			if (segment.getSubStatus() == null || !segment.getSubStatus().equals(ReservationSegmentSubStatus.EXCHANGED)) {
				reservationSegmentLite = reservationSegmentLiteAdaptor.adapt(segment);
				reservationSegmentLite.setTravelMode(TravelModeAdaptor.adapt(segment.getSegmentCode()));
				reservationSegments.add(reservationSegmentLite);
			}
		}

		AdaptorUtils.adaptCollection(lccClientReservationPassengers, passengers, passengerResAdaptor);
		if (reservation.getAlertInfoTOs() != null && !reservation.getAlertInfoTOs().isEmpty()) {
			AdaptorUtils.adaptCollection(reservation.getAlertInfoTOs(), reservationAlertInfo, reservationAlertAdaptor);
		}

		bookingConfirmationRS.setAdvertisements(getAdvertisements(carURL, hotelURL));

		Collections.sort(passengers);
		bookingConfirmationRS.setPassengers(passengers);

		bookingConfirmationRS.setReservationSegments(reservationSegments);

		bookingConfirmationRS.setBookingInfoTO(bookingInfoResAdaptor.adapt(reservation));
		
		bookingConfirmationRS.setPaymentDetails(paymentDetailsAdaptor.adapt(reservation));
		
		bookingConfirmationRS.setBalanceSummary(balanceSummaryAdaptor.adapt(reservation));
		
		bookingConfirmationRS.setSuccessfulRefund(reservation.isSuccessfulRefund());
		
		bookingConfirmationRS.setReservationType(ReservationApiUtils.getReservationType(resSegmentList));

		return bookingConfirmationRS;
	}
	
	private void setReturnFlagForSegments(List<LCCClientReservationSegment> resSegmentList) {
		Map<String, String> segmentReturnMap = ReservationApiUtils.getSegmentReturnMap(resSegmentList);
		if (segmentReturnMap != null && !segmentReturnMap.isEmpty()) {
			for (LCCClientReservationSegment resSegment : resSegmentList) {
				String returnFlag = ReservationApiUtils.getReturnFlag(segmentReturnMap, resSegment);
				resSegment.setReturnFlag(returnFlag);
			}
		}
	}
}
