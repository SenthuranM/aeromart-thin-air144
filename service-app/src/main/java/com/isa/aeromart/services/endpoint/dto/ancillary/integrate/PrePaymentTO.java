package com.isa.aeromart.services.endpoint.dto.ancillary.integrate;

import java.math.BigDecimal;

public class PrePaymentTO {

    private BigDecimal balanceDue;

    public BigDecimal getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(BigDecimal balanceDue) {
        this.balanceDue = balanceDue;
    }
}
