package com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;

public class InsuranceItem extends AncillaryItem {
	
	private String insuranceRefNumber;

    private String feeCode;

	private String planCode;

	private String planCover;
	
	private String planNoConsideration;

	private String termsAndConditions;
	
	private boolean defaultSelection;

    public String getInsuranceRefNumber() {
        return insuranceRefNumber;
    }

    public void setInsuranceRefNumber(String insuranceRefNumber) {
        this.insuranceRefNumber = insuranceRefNumber;
    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getPlanCover() {
        return planCover;
    }

    public void setPlanCover(String planCover) {
        this.planCover = planCover;
    }

    public String getPlanNoConsideration() {
        return planNoConsideration;
    }

    public void setPlanNoConsideration(String planNoConsideration) {
        this.planNoConsideration = planNoConsideration;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

	public boolean isDefaultSelection() {
		return defaultSelection;
	}

	public void setDefaultSelection(boolean defaultSelection) {
		this.defaultSelection = defaultSelection;
	}
    
}
