package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciIntegration;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Assignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationAncillarySelection;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AirportTransferInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AutoCheckinInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.InFlightSsrInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BaseAncillarySelectionsRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateReservationTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItems;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySelection;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionMetaData;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionExternalChargeSummary;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.ReservationChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.aeromart.services.endpoint.utils.common.RPHBuilder;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;

public class AncillaryIntegrationAdaptor extends
		TransactionAwareAdaptor<AncillaryIntegrationAdaptor.AncillaryIntegrationAdaptorWrapper, AncillaryIntegrateTO> {

	private Map<String, AncillaryIntegratePassengerTO> passengerMap;

	private AncillarySelection addedAncillarySelection;

	private List<InventoryWrapper> inventory;

	private SessionMetaData metadata;

	private RPHGenerator rphGenerator;

	private List<ReservationPaxTO> paxList;

	private List<SessionPassenger> sessionPassengers;

	private ReservationInfo resInfo;

	private TravellerQuantity travellerQuantity;

	private List<SessionFlexiDetail> sessionFlexiDetailList;
	
	private boolean isRegUser;
	
	private LCCClientReservation reservation;

	@Override
	public AncillaryIntegrateTO adapt(AncillaryIntegrationAdaptorWrapper source) {

		List<AncillaryIntegratePassengerTO> passengers = new ArrayList<>();
		AncillaryIntegrateTO ancillaryIntegrateTo = new AncillaryIntegrateTO();
		AncillaryIntegratePassengerTO ancillaryIntegratePassengerTO;

		ancillaryIntegrateTo.setReservation(new AncillaryIntegrateReservationTO());
		ancillaryIntegrateTo.getReservation().setPassengers(new ArrayList<AncillaryIntegratePassengerTO>());
		ReservationAncillarySelection reservationSelectedAncillary = source.getReservationAncillarySelection();
		BaseAncillarySelectionsRS allSelectedAncillary = source.getPriceQuatedSelectdAncillary();
		passengerMap = new HashMap<String, AncillaryIntegratePassengerTO>();
		metadata = getTransactionalAncillaryService().getAncillarySelectionsFromSession(getTransactionId()).getMetaData();
		inventory = getTransactionalAncillaryService().getFlightSegments(getTransactionId());
		rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());
		paxList = getTransactionalAncillaryService().getReservationInfo(getTransactionId()).getPaxList();
		// for insurance purposes following variables are needed
		AncillaryQuotation ancillaryQuotation = getTransactionalAncillaryService().getAncillaryQuotation(getTransactionId());
		AncillarySelection ancillarySelection = getTransactionalAncillaryService().getAncillarySelectionsFromSession(
				getTransactionId());
		addedAncillarySelection = getTransactionalAncillaryService().getAncillarySelectionsFromSession(
				getTransactionId());
		sessionPassengers = (List<SessionPassenger>) (getTransactionalAncillaryService().getReservationData(getTransactionId()) == null ? new ArrayList<>()
				: getTransactionalAncillaryService().getReservationData(getTransactionId()).getPassengers());
		resInfo = getTransactionalAncillaryService().getReservationInfo(getTransactionId());
		travellerQuantity = getTransactionalAncillaryService().getTravellerQuantityFromSession(getTransactionId());
		sessionFlexiDetailList = getTransactionalAncillaryService().getSessionFlexiDetail(getTransactionId());
		isRegUser = getTransactionalAncillaryService().isRegUser();

		if (reservationSelectedAncillary != null) {
			updateAncillaryIntegrateTo(
					createAncillaryPassengerList(reservationSelectedAncillary.getAncillaryReservation().getPassengers(),
							allSelectedAncillary.getAncillaryReservation().getPassengers(), true, source.getReservation()), true);
			updateAncillaryIntegrateTo(
					createAncillaryPassengerList(allSelectedAncillary.getAncillaryReservation().getPassengers(),
							reservationSelectedAncillary.getAncillaryReservation().getPassengers(), false, source.getReservation()), false);
		} else {
			updateAncillaryIntegrateTo(allSelectedAncillary.getAncillaryReservation().getPassengers(), false);
		}

		ReservationChargeTo<LCCClientExternalChgDTO> reservationChargeTo = new ReservationChargeTo<>();
		List<PassengerChargeTo<LCCClientExternalChgDTO>> passengerChargeTos = new ArrayList<>();
		PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo;

		// populate passenger map with all passengers who were not added before to passenger map
		for (SessionPassenger sessionPassenger : sessionPassengers) {
			// assuming we ignore infants to allocate ancillaries
			if (!passengerMap.containsKey(sessionPassenger.getPassengerRph())
					&& !PaxTypeTO.INFANT.equals(sessionPassenger.getType())) {
				ancillaryIntegratePassengerTO = new AncillaryIntegratePassengerTO();
				ancillaryIntegratePassengerTO.setAncillariesToRemove(new ArrayList<LCCSelectedSegmentAncillaryDTO>());
				ancillaryIntegratePassengerTO.setExternalCharges(new ArrayList<LCCClientExternalChgDTO>());
				ancillaryIntegratePassengerTO.setNetAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
				ancillaryIntegratePassengerTO.setSelectedAncillaries(new ArrayList<LCCSelectedSegmentAncillaryDTO>());
				ancillaryIntegratePassengerTO.setPassengerRph(sessionPassenger.getPassengerRph());
				passengerMap.put(sessionPassenger.getPassengerRph(), ancillaryIntegratePassengerTO);
			}
		}

		// include insurance as external charges
		updateInsurancDetails(ancillaryIntegrateTo, ancillaryQuotation, ancillarySelection);
		// include flexibility as external charges
		if (sessionFlexiDetailList != null && sessionFlexiDetailList.size() > 0) {
			updateFlexiDetails(ancillaryIntegrateTo, ancillaryQuotation, ancillarySelection);
		}

		for (AncillaryIntegratePassengerTO ancillaryIntegratePassengerTo : passengerMap.values()) {
			passengers.add(ancillaryIntegratePassengerTo);

			passengerChargeTo = new PassengerChargeTo<>();
			passengerChargeTo.setPassengerRph(ancillaryIntegratePassengerTo.getPassengerRph());

			if (source.isAncillaryModification()) {
				// update passenger charges
				Map<String, LCCClientExternalChgDTO> addedChgs = getChgCodeFltRefWiseAddedChargeDTOs(
						ancillaryIntegratePassengerTo);
				Map<String, BigDecimal> removedChgAmts = getChgCodeFltRefWiseRemovedChgAmts(ancillaryIntegratePassengerTo);
				setEffectiveChgs(addedChgs, removedChgAmts);
				List<LCCClientExternalChgDTO> charges = new ArrayList<>();
				for (LCCClientExternalChgDTO extChg : addedChgs.values()) {
					charges.add(extChg);
				}
				passengerChargeTo.setGetPassengerCharges(charges);
			} else {
				passengerChargeTo.setGetPassengerCharges(ancillaryIntegratePassengerTo.getExternalCharges());
			}
			passengerChargeTos.add(passengerChargeTo);
		}

		reservationChargeTo.setPassengers(passengerChargeTos);
		getTransactionalAncillaryService().storeAncillaryCharges(getTransactionId(), reservationChargeTo);

		ancillaryIntegrateTo.getReservation().getPassengers().addAll(passengers);

		return ancillaryIntegrateTo;
	}
	
	private static Map<String, LCCClientExternalChgDTO> getChgCodeFltRefWiseAddedChargeDTOs(AncillaryIntegratePassengerTO anciPassenger) {
		Map<String, LCCClientExternalChgDTO> addedChgs = new HashMap<>();
		for (LCCClientExternalChgDTO extChg : anciPassenger.getExternalCharges()) {
			String key = extChg.getFlightRefNumber() + "|" + extChg.getExternalCharges().toString();

			if (!addedChgs.containsKey(key)) {
				addedChgs.put(key, extChg.clone());
			} else {
				LCCClientExternalChgDTO existinChg = addedChgs.get(key);
				existinChg.setAmount(AccelAeroCalculator.add(extChg.getAmount(), existinChg.getAmount()));
			}
		}
		return addedChgs;
	}
	
	private static Map<String, BigDecimal> getChgCodeFltRefWiseRemovedChgAmts(AncillaryIntegratePassengerTO anciPassenger) {
		Map<String, BigDecimal> removedChgAmts = new HashMap<>();
		for (LCCSelectedSegmentAncillaryDTO segAnci : anciPassenger.getAncillariesToRemove()) {
			
			if (segAnci.getAirSeatDTO() != null && segAnci.getAirSeatDTO().getSeatNumber() != null
					&& !segAnci.getAirSeatDTO().getSeatNumber().trim().equals("")) {
				BigDecimal charge = segAnci.getAirSeatDTO().getSeatCharge();
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.SEAT_MAP.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getMealDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCMealDTO meal : segAnci.getMealDTOs()) {
					charge = AccelAeroCalculator.add(charge,
							AccelAeroCalculator.multiply(meal.getMealCharge(), new BigDecimal(meal.getAllocatedMeals())));

				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.MEAL.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getBaggageDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCBaggageDTO baggage : segAnci.getBaggageDTOs()) {
					charge = AccelAeroCalculator.add(charge, baggage.getBaggageCharge());
				}

				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.BAGGAGE.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getSpecialServiceRequestDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
					charge = AccelAeroCalculator.add(charge, ssr.getCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAirportServiceDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAirportServiceDTO airportService : segAnci.getAirportServiceDTOs()) {
					charge = AccelAeroCalculator.add(charge, airportService.getServiceCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AIRPORT_SERVICE.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAirportTransferDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAirportServiceDTO airportTrasnsfer : segAnci.getAirportTransferDTOs()) {
					charge = AccelAeroCalculator.add(charge, airportTrasnsfer.getServiceCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAutomaticCheckinDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAutomaticCheckinDTO autoCheckin : segAnci.getAutomaticCheckinDTOs()) {
					charge = AccelAeroCalculator.add(charge, autoCheckin.getAutomaticCheckinCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
		}
		return removedChgAmts;
	}
	
	private static void setEffectiveChgs(Map<String, LCCClientExternalChgDTO> addedChgs, Map<String, BigDecimal> removedChgAmts) {
		Iterator<Entry<String, LCCClientExternalChgDTO>> itr = addedChgs.entrySet().iterator();
		while (itr.hasNext()) {
			String key = itr.next().getKey();
			if (removedChgAmts.containsKey(key)) {
				LCCClientExternalChgDTO chg = addedChgs.get(key);
				if (chg.getAmount().compareTo(removedChgAmts.get(key)) <= 0) {
					itr.remove();
				} else {
					chg.setAmount(AccelAeroCalculator.subtract(chg.getAmount(), removedChgAmts.get(key)));
				}
			}
		}
	}

	private void updateAncillaryIntegrateTo(List<AncillaryPassenger> ancillaryPassengerList, boolean skipExternalCharges) {

		AncillaryIntegratePassengerTO passenger;
		List<LCCClientExternalChgDTO> externalChgDTOList;
		List<LCCSelectedSegmentAncillaryDTO> segmentSelections;
		LCCSelectedSegmentAncillaryDTO segmentSelection;
		PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo;
		String anciType;
		AncillariesConstants.AncillaryType ancillaryType;
		LCCClientExternalChgDTO externalChgDTO;
		AncillariesResponseAdaptor responseAdaptor;
		List<String> flightRefNumberList;
		InventoryWrapper inventoryWrapper;
		FlightSegmentTO flightSegmentTO;
		String travelerRefNumber = "";

		List<FlightSegmentTO> flightSegments;
		String firstDepartCarrierCode;
		int segmentsCount;
		int travelWith = 0;
		int infantSequence;
		BigDecimal amount;
		String paxType = "";

		Map<String, LCCSelectedSegmentAncillaryDTO> anciSelections = new HashMap<>();

		for (AncillaryPassenger ancillaryPassenger : ancillaryPassengerList) {
			anciSelections = new HashMap<>();
			passenger = resolvePassenger(ancillaryPassenger.getPassengerRph());
			externalChgDTOList = new ArrayList<>();
			segmentSelections = new ArrayList<>();

			passengerChargeTo = new PassengerChargeTo<>();
			passengerChargeTo.setPassengerRph(ancillaryPassenger.getPassengerRph());

			for (ReservationPaxTO reservationPaxTO : paxList) {
				if (reservationPaxTO.getSeqNumber().toString().equals(ancillaryPassenger.getPassengerRph())) {
					paxType = reservationPaxTO.getPaxType();
					if (reservationPaxTO.getTravelerRefNumber() != null) {
						travelerRefNumber = reservationPaxTO.getTravelerRefNumber();
					} else {
						if (reservationPaxTO.getPaxType().equals(PaxTypeTO.INFANT)) {
							travelWith = 0;
							infantSequence = reservationPaxTO.getSeqNumber();
							for (ReservationPaxTO reservationPaxTORef : paxList) {
								if (infantSequence == reservationPaxTORef.getInfantWith()) {
									travelWith = reservationPaxTORef.getSeqNumber();
								}
							}
						}
						travelerRefNumber = RPHBuilder.PassengerRPHBuilder(reservationPaxTO.getSeqNumber(),
								reservationPaxTO.getPaxType(), travelWith);
					}
				}
			}

			for (PricedAncillaryType pricedAncillaryType : ancillaryPassenger.getAncillaryTypes()) {

				anciType = pricedAncillaryType.getAncillaryType();
				ancillaryType = AncillariesConstants.AncillaryType.resolveAncillaryType(anciType);
				responseAdaptor = AncillaryAdaptorFactory.getResponseAdaptor(anciType);

				for (AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()) {

					flightRefNumberList = new ArrayList<>();
					if (ancillaryScope.getScope() instanceof AirportScope) {
						AirportScope airportScope = (AirportScope) ancillaryScope.getScope();
						flightRefNumberList.add(rphGenerator.getUniqueIdentifier(airportScope.getFlightSegmentRPH()));
					} else if (ancillaryScope.getScope() instanceof SegmentScope) {
						SegmentScope segmentScope = (SegmentScope) ancillaryScope.getScope();
						flightRefNumberList.add(rphGenerator.getUniqueIdentifier(segmentScope.getFlightSegmentRPH()));
					} else if (ancillaryScope.getScope() instanceof OndScope) {
						OndScope ondScope = (OndScope) ancillaryScope.getScope();
						for (com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit OndUnit : metadata.getOndUnits()) {
							if (OndUnit.getOndId().equals(ondScope.getOndId())) {
								for (String fltSegments : OndUnit.getFlightSegmentRPH()) {
									flightRefNumberList.add(rphGenerator.getUniqueIdentifier(fltSegments));
								}

							}
						}
					}

					flightSegments = new ArrayList<>();
					for (String flightSegRph : flightRefNumberList) {
						if (!anciSelections.containsKey(flightSegRph)) {
							inventoryWrapper = AnciCommon.resolveInventory(inventory, flightSegRph);
							flightSegmentTO = inventoryWrapper.getFlightSegmentTO();

							segmentSelection = new LCCSelectedSegmentAncillaryDTO();
							segmentSelection.setBaggageDTOs(new ArrayList<LCCBaggageDTO>());
							segmentSelection.setMealDTOs(new ArrayList<LCCMealDTO>());
							segmentSelection.setSpecialServiceRequestDTOs(new ArrayList<LCCSpecialServiceRequestDTO>());
							segmentSelection.setAirportServiceDTOs(new ArrayList<LCCAirportServiceDTO>());
							segmentSelection.setInsuranceQuotations(new ArrayList<LCCInsuranceQuotationDTO>());
							segmentSelection.setAirportTransferDTOs(new ArrayList<LCCAirportServiceDTO>());
							segmentSelection.setAutomaticCheckinDTOs(new ArrayList<LCCAutomaticCheckinDTO>());
							segmentSelection.setCarrierCode(flightSegmentTO.getOperatingAirline());
							segmentSelection.setFlightSegmentTO(flightSegmentTO);
							segmentSelection.setTravelerRefNumber(travelerRefNumber);
							segmentSelection.setPaxType(paxType);
							anciSelections.put(flightSegRph, segmentSelection);

							segmentSelections.add(segmentSelection);

							flightSegments.add(flightSegmentTO);

						} else {
							inventoryWrapper = AnciCommon.resolveInventory(inventory, flightSegRph);
							flightSegmentTO = inventoryWrapper.getFlightSegmentTO();
							flightSegments.add(flightSegmentTO);
						}
					}

					Collections.sort(flightSegments);

					for (PricedSelectedItem pricedSelectedItem : ancillaryScope.getAncillaries()) {

						firstDepartCarrierCode = flightSegments.iterator().next().getOperatingAirline();
						segmentsCount = AnciCommon.getSegmentsCount(flightSegments, firstDepartCarrierCode);

						amount = pricedSelectedItem.getAmount();

						for (String flightRefNumber : flightRefNumberList) {
							segmentSelection = anciSelections.get(flightRefNumber);

							externalChgDTO = new LCCClientExternalChgDTO();
							externalChgDTO.setAmount(amount);
							externalChgDTO.setExternalCharges(AnciCommon.getExternalCharge(ancillaryType));
							externalChgDTO.setFlightRefNumber(flightRefNumber);
							externalChgDTO.setCarrierCode(segmentSelection.getFlightSegmentTO().getOperatingAirline());
							boolean isAnciSelectionValidSector = true;
							if (ancillaryType == AncillariesConstants.AncillaryType.BAGGAGE) {
								if (segmentSelection.getFlightSegmentTO().getOperatingAirline().equals(firstDepartCarrierCode)) {
									externalChgDTO.setCode(AnciCommon.getBaggageName(pricedSelectedItem.getId()));
									externalChgDTO.setOndBaggageChargeGroupId(
											AnciCommon.getBaggageOndChargeId(pricedSelectedItem.getId()));
									OndScope ondScope = (OndScope) ancillaryScope.getScope();
									externalChgDTO.setOndBaggageGroupId(ondScope.getOndId());
								} else {
									isAnciSelectionValidSector = false;
								}
							} else {
								externalChgDTO.setCode(pricedSelectedItem.getId());
								externalChgDTO.setFlightRefNumber(flightRefNumber);
								externalChgDTO.setCarrierCode(segmentSelection.getFlightSegmentTO().getOperatingAirline());
								if (ancillaryType == AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN) {
									externalChgDTO.setSeatTypePreference(pricedSelectedItem.getName());
									AutoCheckinInput autoCheckinInput = (AutoCheckinInput) getAncillaryInputData(
											ancillaryPassenger, pricedSelectedItem, ancillaryScope.getScope(),
											AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN.name());
									if (autoCheckinInput != null) {
										externalChgDTO.setEmail(autoCheckinInput.getEmail());
										externalChgDTO.setSeatCode(autoCheckinInput.getSeatCode());
									}
								} else if (pricedSelectedItem.getInput() != null) {
									externalChgDTO.setUserNote(((InFlightSsrInput) pricedSelectedItem.getInput()).getComment());
								}
								if(ancillaryScope.getScope() instanceof AirportScope){
									AirportScope airportScope = (AirportScope) ancillaryScope.getScope();
									externalChgDTO.setAirportCode(airportScope.getAirportCode());
									if (ancillaryType == AncillariesConstants.AncillaryType.AIRPORT_TRANSFER) {
										AirportTransferInput airportTransferInput = (AirportTransferInput) getAncillaryInputData(
												ancillaryPassenger, pricedSelectedItem, airportScope,
												AncillariesConstants.AncillaryType.AIRPORT_TRANSFER.name());
										externalChgDTO.setTransferAddress(airportTransferInput.getTransferAddress());
										externalChgDTO.setTransferContact(airportTransferInput.getTransferContact());
										externalChgDTO.setTransferDate(CalendarUtil.getDateInFormattedString(CalendarUtil.PATTERN11, airportTransferInput.getTranferDate()));
										externalChgDTO.setTransferType(airportTransferInput.getTransferType());
										externalChgDTO.setOfferedAnciTemplateID(Integer.parseInt(pricedSelectedItem.getId()));										
									}

								}

								if (ancillaryType == AncillariesConstants.AncillaryType.SSR_IN_FLIGHT) {
									InFlightSsrInput inFlightSsrInput = (InFlightSsrInput) getAncillaryInputData(
											ancillaryPassenger, pricedSelectedItem, ancillaryScope.getScope(),
											AncillariesConstants.AncillaryType.SSR_IN_FLIGHT.name());
									if (inFlightSsrInput != null) {

										if (inFlightSsrInput.getComment() != null) {
											externalChgDTO.setUserNote(inFlightSsrInput.getComment());
										}
									}
								}
							}

							if (isAnciSelectionValidSector) {
								if (pricedSelectedItem.getQuantity() != null && pricedSelectedItem.getQuantity().SIZE > 0) {
									for (int i = 0; i < pricedSelectedItem.getQuantity(); i++) {
										externalChgDTOList.add(externalChgDTO);
										if (ancillaryType == AncillariesConstants.AncillaryType.BAGGAGE) {
											break;
										}
									}
								} else {
									externalChgDTOList.add(externalChgDTO);
								}
							}

							switch (ancillaryType) {
							case BAGGAGE:
								if (segmentSelection.getFlightSegmentTO().getOperatingAirline().equals(firstDepartCarrierCode)) {
									segmentSelection.getBaggageDTOs().add(
											(LCCBaggageDTO) responseAdaptor.toAncillaryItemModel(ancillaryScope.getScope(),
													pricedSelectedItem));
								}
								break;
							case SEAT:
								LCCAirSeatDTO lccAirSeatDTO = (LCCAirSeatDTO) responseAdaptor
										.toAncillaryItemModel(ancillaryScope.getScope(), pricedSelectedItem);
								lccAirSeatDTO.setBookedPassengerType(paxType);
								segmentSelection.setAirSeatDTO(lccAirSeatDTO);
								break;
							case MEAL:
								segmentSelection.getMealDTOs().add(
										(LCCMealDTO) responseAdaptor.toAncillaryItemModel(ancillaryScope.getScope(),
												pricedSelectedItem));
								break;
							case SSR_IN_FLIGHT:
								InFlightSsrInput inFlightSsrInput = (InFlightSsrInput) getAncillaryInputData(
										ancillaryPassenger, pricedSelectedItem, ancillaryScope.getScope(),
										AncillariesConstants.AncillaryType.SSR_IN_FLIGHT.name());
								if(inFlightSsrInput != null){
									pricedSelectedItem.setInput(inFlightSsrInput);
								}
								segmentSelection.getSpecialServiceRequestDTOs().add(
										(LCCSpecialServiceRequestDTO) responseAdaptor.toAncillaryItemModel(
												ancillaryScope.getScope(), pricedSelectedItem));
								break;
							case SSR_AIRPORT:
								segmentSelection.getAirportServiceDTOs().add(
										(LCCAirportServiceDTO) responseAdaptor.toAncillaryItemModel(ancillaryScope.getScope(),
												pricedSelectedItem));
								break;
							case INSURANCE:
								segmentSelection.getInsuranceQuotations().add(
										(LCCInsuranceQuotationDTO) responseAdaptor.toAncillaryItemModel(
												ancillaryScope.getScope(), pricedSelectedItem));
								break;
							case FLEXIBILITY:
								break;
							case AIRPORT_TRANSFER:
								AirportTransferInput airportTransferInput = (AirportTransferInput) getAncillaryInputData(
										ancillaryPassenger, pricedSelectedItem, ancillaryScope.getScope(),
										AncillariesConstants.AncillaryType.AIRPORT_TRANSFER.name());
								pricedSelectedItem.setInput(airportTransferInput);
								segmentSelection.getAirportTransferDTOs().add(
										(LCCAirportServiceDTO) responseAdaptor.toAncillaryItemModel(ancillaryScope.getScope(),
												pricedSelectedItem));
								break;
							case AUTOMATIC_CHECKIN:
								segmentSelection.getAutomaticCheckinDTOs().add(
										(LCCAutomaticCheckinDTO) responseAdaptor.toAncillaryItemModel(ancillaryScope.getScope(),
												pricedSelectedItem));
								break;
							}
						}
					}
				}
			}
			if (skipExternalCharges) {
				passenger.setAncillariesToRemove(segmentSelections);
				if (ancillaryPassenger.getAmount() != null) {
					passenger.setNetAmount(passenger.getNetAmount() == null
							? ancillaryPassenger.getAmount()
							: AccelAeroCalculator.add(passenger.getNetAmount(), ancillaryPassenger.getAmount().negate()));
				}
			} else {
				passenger.setExternalCharges(externalChgDTOList);
				passenger.setSelectedAncillaries(segmentSelections);
				if (ancillaryPassenger.getAmount() != null) {
					passenger.setNetAmount(passenger.getNetAmount() == null
							? ancillaryPassenger.getAmount()
							: AccelAeroCalculator.add(passenger.getNetAmount(), ancillaryPassenger.getAmount()));
				}
			}
			passengerMap.put(passenger.getPassengerRph(), passenger);
		}
	}

	private List<AncillaryPassenger>
			createAncillaryPassengerList(List<AncillaryPassenger> source, List<AncillaryPassenger> common, boolean removeFlag, LCCClientReservation reservation) {

		AncillaryPassenger removedAddPassenger;
		AncillaryPassenger passengerTemp;
		PricedAncillaryType pricedAncillaryType;
		AncillaryScope ancillaryScope;
		AncillaryScope ancillaryScopeTemp;
		PricedAncillaryType anciTypeTemp;
		PricedSelectedItem pricedSelectedItem;
		PricedSelectedItem pricedSelectedItemTemp;
		List<String> anciTypeList;
		List<Scope> anciScopeList;
		List<String> pricedSelectedItemList;

		List<String> commonAnciPassengerList = new ArrayList<String>();
		List<AncillaryPassenger> removedAnciPassenger = new ArrayList<AncillaryPassenger>();
		Map<String, AncillaryPassenger> anciPassengerMap = new HashMap<String, AncillaryPassenger>();
		Map<String, PricedAncillaryType> pricedAncillaryTypeMap = new HashMap<String, PricedAncillaryType>();
		Map<Scope, AncillaryScope> ancillaryScopeMap = new HashMap<Scope, AncillaryScope>();
		Map<String, PricedSelectedItem> pricedSelectedItemMap = new HashMap<String, PricedSelectedItem>();

		for (AncillaryPassenger ancillaryPassenger : common) {
			commonAnciPassengerList.add(ancillaryPassenger.getPassengerRph());
			anciPassengerMap.put(ancillaryPassenger.getPassengerRph(), ancillaryPassenger);
		}

		for (AncillaryPassenger anciResPassenger : source) {
			removedAddPassenger = new AncillaryPassenger();
			removedAddPassenger.setAncillaryTypes(new ArrayList<PricedAncillaryType>());
			if (!commonAnciPassengerList.contains(anciResPassenger.getPassengerRph())) {
				removedAnciPassenger.add(anciResPassenger);
			} else {
				anciTypeList = new ArrayList<String>();
				removedAddPassenger.setPassengerRph(anciResPassenger.getPassengerRph());
				passengerTemp = anciPassengerMap.get(anciResPassenger.getPassengerRph());

				for (PricedAncillaryType tempPricedAncillaryType : passengerTemp.getAncillaryTypes()) {
					anciTypeList.add(tempPricedAncillaryType.getAncillaryType());
					pricedAncillaryTypeMap.put(tempPricedAncillaryType.getAncillaryType(), tempPricedAncillaryType);
				}

				for (PricedAncillaryType resPricedAncillaryType : anciResPassenger.getAncillaryTypes()) {
					pricedAncillaryType = new PricedAncillaryType();
					if (!anciTypeList.contains(resPricedAncillaryType.getAncillaryType())) {
						if(removeFlag && !AnciCommon.isAncillaryEditableForModificationIBE(reservation, resPricedAncillaryType.getAncillaryType(), true, true, isRegUser)){
							break;
						} else {
							removedAddPassenger.getAncillaryTypes().add(resPricedAncillaryType);
						}
					} else {
						anciScopeList = new ArrayList<Scope>();

						pricedAncillaryType.setAncillaryType(resPricedAncillaryType.getAncillaryType());
						pricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());
						anciTypeTemp = pricedAncillaryTypeMap.get(resPricedAncillaryType.getAncillaryType());

						for (AncillaryScope tempAncillaryScope : anciTypeTemp.getAncillaryScopes()) {
							anciScopeList.add(tempAncillaryScope.getScope());
							ancillaryScopeMap.put(tempAncillaryScope.getScope(), tempAncillaryScope);
						}

						for (AncillaryScope resAncillaryScope : resPricedAncillaryType.getAncillaryScopes()) {
							ancillaryScope = new AncillaryScope();
							if (!anciScopeList.contains(resAncillaryScope.getScope())) {
								pricedAncillaryType.getAncillaryScopes().add(resAncillaryScope);
							} else {
								pricedSelectedItemList = new ArrayList<String>();
								ancillaryScope.setScope(resAncillaryScope.getScope());
								ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());
								ancillaryScopeTemp = ancillaryScopeMap.get(resAncillaryScope.getScope());

								for (PricedSelectedItem tempPricedSelectedItem : ancillaryScopeTemp.getAncillaries()) {
									pricedSelectedItemList.add(tempPricedSelectedItem.getId());
									pricedSelectedItemMap.put(tempPricedSelectedItem.getId(), tempPricedSelectedItem);
								}

								for (PricedSelectedItem resPricedSelectedItem : resAncillaryScope.getAncillaries()) {
									if (!pricedSelectedItemList.contains(resPricedSelectedItem.getId())) {
										ancillaryScope.getAncillaries().add(resPricedSelectedItem);
									} else {
										pricedSelectedItemTemp = pricedSelectedItemMap.get(resPricedSelectedItem.getId());
										if (!pricedSelectedItemTemp.getQuantity().equals(resPricedSelectedItem.getQuantity())) {
											if (resPricedSelectedItem.getQuantity() > pricedSelectedItemTemp.getQuantity()) {
												pricedSelectedItem = pricedSelectedItemTemp;
												pricedSelectedItem.setQuantity(resPricedSelectedItem.getQuantity()
														- pricedSelectedItemTemp.getQuantity());
												ancillaryScope.getAncillaries().add(pricedSelectedItem);
											}
										} else if (pricedAncillaryType.getAncillaryType().equalsIgnoreCase(
												AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN.name())) {
											Boolean isDiffered = false;
											isDiffered = isModified(pricedSelectedItemTemp, resPricedSelectedItem);
											if (isDiffered) {
												ancillaryScope.getAncillaries().add(resPricedSelectedItem);
											}
										}
									}
								}
							}
							if (ancillaryScope.getAncillaries() != null && ancillaryScope.getAncillaries().size() > 0) {
								pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
							}
						}
					}
					if (pricedAncillaryType.getAncillaryScopes() != null && pricedAncillaryType.getAncillaryScopes().size() > 0) {
						removedAddPassenger.getAncillaryTypes().add(pricedAncillaryType);
					}
				}
			}
			if (removedAddPassenger.getAncillaryTypes() != null && removedAddPassenger.getAncillaryTypes().size() > 0) {
				removedAddPassenger
						.setAmount(AnciIntegration.getAncillarySelectionAmount(removedAddPassenger.getAncillaryTypes()));
				removedAnciPassenger.add(removedAddPassenger);
			}
		}

		return removedAnciPassenger;
	}

	/**
	 * This method will return if auto checkin request modified or not
	 * 
	 * @param pricedSelectedItemTemp
	 * @param resPricedSelectedItem
	 */
	private Boolean isModified(PricedSelectedItem pricedSelectedItemTemp, PricedSelectedItem resPricedSelectedItem) {
		AutoCheckinInput pricedInput = (AutoCheckinInput) pricedSelectedItemTemp.getInput();
		AutoCheckinInput resPricedInput = (AutoCheckinInput) resPricedSelectedItem.getInput();
		if (!StringUtil.getNotNullString(pricedSelectedItemTemp.getName()).equals(
				StringUtil.getNotNullString(resPricedSelectedItem.getName()))) {
			return true;
		}

		if (pricedInput != null && resPricedInput != null) {
			if (pricedInput.getEmail() != null && !pricedInput.getEmail().equals(resPricedInput.getEmail())) {
				return true;
			}

			if (!StringUtil.getNotNullString(pricedInput.getSeatCode()).equals(
					StringUtil.getNotNullString(resPricedInput.getSeatCode()))) {
				return true;
			}
		}

		return false;
	}

	private void updateFlexiDetails(AncillaryIntegrateTO ancillaryIntegrateTo, AncillaryQuotation ancillaryQuotation,
			AncillarySelection ancillarySelection) {

		Map<String, List<String>> ondFlexiSelecionMap = new HashMap<>();
		Map<String, BigDecimal> ondFlexiChargeMap = new HashMap<>();

		// set flexi charges handle multiple flexi charges for same ond also implemented
		if (ancillaryQuotation.getQuotedAncillaries() != null) {

			for (SelectedAncillary selectedAncillary : ancillarySelection.getAncillaryPreferences()) {
				if (selectedAncillary.getType().equals(AncillariesConstants.AncillaryType.FLEXIBILITY.name())) {
					if (selectedAncillary.getPreferences() != null && selectedAncillary.getPreferences().size() > 0) {
						for (Preference Preference : selectedAncillary.getPreferences()) {
							for (Selection selection : Preference.getSelections()) {
								OndScope ondScope = (OndScope) selection.getScope();
								String ondId = ondScope.getOndId();
								List<String> flexiSelections = new ArrayList<String>();
								for (SelectedItem selectedItem : selection.getSelectedItems()) {
									flexiSelections.add(selectedItem.getId());
								}
								ondFlexiSelecionMap.put(ondId, flexiSelections);
							}
						}
					}
				}
			}
			if (ondFlexiSelecionMap.size() > 0) {
				for (AncillaryType quotedAncillaryType : ancillaryQuotation.getQuotedAncillaries()) {
					BigDecimal ondFlexiCharge;
					if (quotedAncillaryType.getAncillaryType().equals(AncillariesConstants.AncillaryType.FLEXIBILITY.name())) {
						for (Provider provider : quotedAncillaryType.getProviders()) {
							for (AvailableAncillaryUnit availableAncillaryUnit : provider.getAvailableAncillaries()
									.getAvailableUnits()) {
								OndScope ondScope = (OndScope) availableAncillaryUnit.getScope();
								String ondId = ondScope.getOndId();
								if (ondFlexiSelecionMap.containsKey(ondId)) {
									ondFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
									FlexiItems flexiItems = (FlexiItems) availableAncillaryUnit.getItemsGroup();
									for (FlexiItem flexiItem : flexiItems.getItems()) {
										if (ondFlexiSelecionMap.get(ondId).contains(flexiItem.getItemId())
												&& !flexiItem.getDefaultSelected()) {
											ondFlexiCharge = ondFlexiCharge.add(flexiItem.getCharges().get(0).getAmount());
											ondFlexiChargeMap.put(ondId, ondFlexiCharge);
										}
									}

								}
							}
						}
					}
				}
				if (ondFlexiChargeMap.size() > 0) {

					Map<String, SessionFlexiDetail> ondWiseSessionFlexi = new HashMap<>();

					for (String ondSequence : ondFlexiChargeMap.keySet()) {
						for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiDetailList) {
							if (ondSequence.equals(Integer.toString(sessionFlexiDetail.getOndSequence()))) {
								ondWiseSessionFlexi.put(ondSequence, sessionFlexiDetail);
							}
						}
					}

					int nonInfants = 0;
					nonInfants = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();

					int passengerIndex = 0;
					int segmentIndex = 0;
					for (AncillaryIntegratePassengerTO pax : passengerMap.values()) {
						for (String ondSequence : ondWiseSessionFlexi.keySet()) {

							SessionFlexiDetail SessionFlexiDetail = resolveFlexiSessionInfo(Integer.parseInt(ondSequence));
							for (SessionExternalChargeSummary sessionExternalChargeSummary : SessionFlexiDetail
									.getExternalChargeSummary()) {
								List<String> fltSegRefList = resolveFlightSegments(sessionExternalChargeSummary.getSegmentCode());
								int segmentCount = fltSegRefList.size();
								BigDecimal[] flexiPassengerQuota = AccelAeroCalculator.roundAndSplit(
										sessionExternalChargeSummary.getFlexiCharge(), nonInfants);
								BigDecimal[] flexiSegmentQuota = AccelAeroCalculator.roundAndSplit(
										flexiPassengerQuota[passengerIndex], segmentCount);
								segmentIndex = 0;
								for (String flightRefNumber : fltSegRefList) {
									if (!PaxTypeTO.INFANT.equals(resolvePassengerType(pax.getPassengerRph()))) {
										LCCClientFlexiExternalChgDTO tmpChgDTO = new LCCClientFlexiExternalChgDTO();
										tmpChgDTO.setExternalCharges(AnciCommon
												.getExternalCharge(AncillariesConstants.AncillaryType.FLEXIBILITY));
										tmpChgDTO.setCarrierCode(sessionExternalChargeSummary.getCarrierCode());
										tmpChgDTO.setCode(sessionExternalChargeSummary.getSurchargeCode());
										tmpChgDTO.setFlightRefNumber(flightRefNumber);
										tmpChgDTO.setSegmentCode(sessionExternalChargeSummary.getSegmentCode());
										tmpChgDTO.setAmount(flexiSegmentQuota[segmentIndex]);
										tmpChgDTO.setFlexiBilities(sessionExternalChargeSummary.getAdditionalDetails());
										pax.getExternalCharges().add(tmpChgDTO);
									}
									segmentIndex++;
								}
							}
						}
						passengerIndex++;
					}
				}
			}
		}

	}

	private void updateInsurancDetails(AncillaryIntegrateTO ancillaryIntegrateTo, AncillaryQuotation ancillaryQuotation,
			AncillarySelection ancillarySelection) {
		List<String> selectedInsuranceRefList = new ArrayList<String>();
		List<LCCClientReservationInsurance> reservationInsurances = new ArrayList<>();
		LCCClientReservationInsurance lccClientReservationInsurance;
		LCCInsuranceQuotationDTO lccInsuranceQuotationDTO;
		LCCSelectedSegmentAncillaryDTO segmentSelection;

		if (ancillaryQuotation.getQuotedAncillaries() != null) {

			for (SelectedAncillary selectedAncillary : ancillarySelection.getAncillaryPreferences()) {
				if (selectedAncillary.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.name())) {
					if (selectedAncillary.getPreferences() != null && selectedAncillary.getPreferences().size() > 0) {
						for (Preference Preference : selectedAncillary.getPreferences()) {
							for (Selection selection : Preference.getSelections()) {
								for (SelectedItem selectedItem : selection.getSelectedItems()) {
									selectedInsuranceRefList.add(selectedItem.getId());
								}
							}
						}
					}
				}
			}
			if (selectedInsuranceRefList.size() > 0) {
				// creating LCCInsuredJourneyDTO
				// assuming insurance are generated for whole reservation and all the quoted insurances contains same
				// LCCInsuredJourneyDTO
				List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(getTransactionalAncillaryService()
						.getFlightSegments(getTransactionId()));
				InsuranceFltSegBuilder insuranceFltSegBuilder = null;
				FlightSegmentTO operatingSegment = null;
				try {
					insuranceFltSegBuilder = new InsuranceFltSegBuilder(flightSegmentTOs);
				} catch (ModuleException e) {
					throw new ValidationException(ValidationException.Code.ANCI_GENERIC_ERROR, e);
				}

				LCCInsuredJourneyDTO insuredJourneyDTO = AncillaryDTOUtil.getInsuranceJourneyDetails(
						insuranceFltSegBuilder.getInsurableFlightSegments(), insuranceFltSegBuilder.isReturnJourney(),
						CommonServiceUtil.getChannelId(getTrackInfoDTO().getAppIndicator()));

				for (AncillaryType quotedAncillaryType : ancillaryQuotation.getQuotedAncillaries()) {
					if (quotedAncillaryType.getAncillaryType().equals(AncillariesConstants.AncillaryType.INSURANCE.name())) {
						for (Provider provider : quotedAncillaryType.getProviders()) {
							for (AvailableAncillaryUnit availableAncillaryUnit : provider.getAvailableAncillaries()
									.getAvailableUnits()) {
								InsuranceItems insuranceItems = (InsuranceItems) availableAncillaryUnit.getItemsGroup();
								for (InsuranceItem insuranceItem : insuranceItems.getItems()) {
									if (selectedInsuranceRefList.contains(insuranceItem.getInsuranceRefNumber())) {
										lccClientReservationInsurance = new LCCClientReservationInsurance();
										lccClientReservationInsurance.setInsuranceQuoteRefNumber(insuranceItem
												.getInsuranceRefNumber());
										lccClientReservationInsurance.setPlanCode(insuranceItem.getPlanCode());
										// assuming only one charge type exists in insurances
										lccClientReservationInsurance.setQuotedTotalPremium(insuranceItem.getCharges().get(0)
												.getAmount());
										lccClientReservationInsurance.setInsuredJourneyDTO(insuredJourneyDTO);
										lccClientReservationInsurance.setOrigin(insuredJourneyDTO.getJourneyStartAirportCode());
										lccClientReservationInsurance
												.setDestination(insuredJourneyDTO.getJourneyEndAirportCode());
										lccClientReservationInsurance.setDateOfTravel(insuredJourneyDTO.getJourneyStartDate());
										lccClientReservationInsurance.setDateOfReturn(insuredJourneyDTO.getJourneyEndDate());
										lccClientReservationInsurance.setOperatingAirline(insuranceFltSegBuilder
												.getFlightSegment().getOperatingAirline());
										reservationInsurances.add(lccClientReservationInsurance);
									}
								}
							}
						}
					}
				}
				if (!reservationInsurances.isEmpty()) {

					int nonInfants = 0;
					int paxCount = 0;
					paxCount = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount()
							+ travellerQuantity.getInfantCount();
					nonInfants = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();
					BigDecimal totalInsuracePremium = AccelAeroCalculator.getDefaultBigDecimalZero();
					String operatingCarrier = null;

					for (LCCClientReservationInsurance insuranceQuotation : reservationInsurances) {
						// operatingCarrier = insuranceQuotation.getOperatingAirline(); // all quotes will be having
						// same
						operatingCarrier = insuranceFltSegBuilder.getFlightSegment().getOperatingAirline();
						totalInsuracePremium = AccelAeroCalculator.add(totalInsuracePremium,
								insuranceQuotation.getQuotedTotalPremium());
					}

					BigDecimal[] insuranceQuota = null;
					if (AppSysParamsUtil.allowAddInsurnaceForInfants()) {
						insuranceQuota = AccelAeroCalculator.roundAndSplit(totalInsuracePremium, paxCount);
					} else {
						insuranceQuota = AccelAeroCalculator.roundAndSplit(totalInsuracePremium, nonInfants);
					}
					LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
					chgDTO.setExternalCharges(AnciCommon.getExternalCharge(AncillariesConstants.AncillaryType.INSURANCE));
					chgDTO.setCarrierCode(operatingCarrier);

					for (FlightSegmentTO fltSegment : insuranceFltSegBuilder.getInsurableFlightSegments()) {
						if (FlightRefNumberUtil.getOperatingAirline(fltSegment.getFlightRefNumber()).equals(operatingCarrier)) {
							operatingSegment = fltSegment;
							break;
						}
					}
					int insIndex = 0;

					ancillaryIntegrateTo.getReservation().setReservationInsurances(reservationInsurances);
					for (AncillaryIntegratePassengerTO pax : passengerMap.values()) {
						LCCClientExternalChgDTO tmpChgDTO = chgDTO.clone();
						if (!PaxTypeTO.INFANT.equals(resolvePassengerType(pax.getPassengerRph()))) {
							tmpChgDTO.setFlightRefNumber(operatingSegment.getFlightRefNumber());
							if (AppSysParamsUtil.allowAddInsurnaceForInfants()) { 
								tmpChgDTO.setAmount(AccelAeroCalculator.add(insuranceQuota[insIndex++],
										insuranceQuota[insIndex++]));
							} else {
								tmpChgDTO.setAmount(insuranceQuota[insIndex++]);
							}
							pax.getExternalCharges().add(tmpChgDTO);
						}
						for (LCCClientReservationInsurance lccClientReservationInsuranceRef : reservationInsurances) {
							lccInsuranceQuotationDTO = new LCCInsuranceQuotationDTO();
							lccInsuranceQuotationDTO
									.setQuotedTotalPremiumAmount(lccClientReservationInsuranceRef.getQuotedTotalPremium());
							lccInsuranceQuotationDTO.setPlanCode(lccClientReservationInsuranceRef.getPlanCode());
							lccInsuranceQuotationDTO
									.setInsuranceRefNumber(lccClientReservationInsuranceRef.getInsuranceQuoteRefNumber());
							lccInsuranceQuotationDTO.setInsuredJourney(lccClientReservationInsuranceRef.getInsuredJourneyDTO());
							lccInsuranceQuotationDTO.setOperatingAirline(operatingSegment.getOperatingAirline());
							if (pax.getSelectedAncillaries() != null && pax.getSelectedAncillaries().size() > 0) {
								for (LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO : pax
										.getSelectedAncillaries()) {
									lccSelectedSegmentAncillaryDTO.getInsuranceQuotations().add(lccInsuranceQuotationDTO);
								}
							} else {
								for (FlightSegmentTO fltSegment : insuranceFltSegBuilder.getInsurableFlightSegments()) {
									if (FlightRefNumberUtil.getOperatingAirline(fltSegment.getFlightRefNumber())
											.equals(lccClientReservationInsuranceRef.getOperatingAirline())) {
										segmentSelection = new LCCSelectedSegmentAncillaryDTO();
										segmentSelection.setBaggageDTOs(new ArrayList<LCCBaggageDTO>());
										segmentSelection.setMealDTOs(new ArrayList<LCCMealDTO>());
										segmentSelection
												.setSpecialServiceRequestDTOs(new ArrayList<LCCSpecialServiceRequestDTO>());
										segmentSelection.setAirportServiceDTOs(new ArrayList<LCCAirportServiceDTO>());
										segmentSelection.setInsuranceQuotations(new ArrayList<LCCInsuranceQuotationDTO>());
										segmentSelection.setFlightSegmentTO(fltSegment);
										segmentSelection.getInsuranceQuotations().add(lccInsuranceQuotationDTO);
										segmentSelection.setTravelerRefNumber(fltSegment.getFlightRefNumber());
										pax.getSelectedAncillaries().add(segmentSelection);
									}
								}
							}
							pax.setNetAmount(AccelAeroCalculator.add(pax.getNetAmount(), tmpChgDTO.getAmount()));
						}
					}
				}

			}
		}
	}

	private SessionFlexiDetail resolveFlexiSessionInfo(int ondId) {

		for (SessionFlexiDetail SessionFlexiDetail : sessionFlexiDetailList) {
			if (SessionFlexiDetail.getOndSequence() == ondId) {
				return SessionFlexiDetail;
			}
		}
		return null;
	}

	private AncillaryIntegratePassengerTO resolvePassenger(String passengerRPH) {
		AncillaryIntegratePassengerTO passenger;
		if (!passengerMap.containsKey(passengerRPH)) {
			passenger = new AncillaryIntegratePassengerTO();
			passenger.setPassengerRph(passengerRPH);
			passenger.setNetAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			passengerMap.put(passengerRPH, passenger);
		}
		return passengerMap.get(passengerRPH);
	}

	private String resolvePassengerType(String passengerRPH) {

		for (SessionPassenger sessionPassenger : sessionPassengers) {
			if (passengerRPH.equals(sessionPassenger.getPassengerRph())) {
				return sessionPassenger.getType();
			}
		}
		return null;
	}

	private List<String> resolveFlightSegments(String segmentCode) {
		List<String> fltSegRefList = new ArrayList<String>();
		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (segmentCode.contains(flightSegmentTO.getSegmentCode())) {
				fltSegRefList.add(flightSegmentTO.getFlightRefNumber());
			}
		}
		return fltSegRefList;
	}
	
	private AncillaryInput getAncillaryInputData(AncillaryPassenger ancillaryPassenger, PricedSelectedItem pricedSelectedItem,
			Scope scope, String anicType) {
		AncillaryInput ancillaryInput = null;
		for (SelectedAncillary selectedAncillary : addedAncillarySelection.getAncillaryPreferences()) {
			if (anicType.equals(selectedAncillary.getType())) {
				for (Preference passengerPreference : selectedAncillary.getPreferences()) {
					if (Assignee.AssignType.PASSENGER.equals(passengerPreference.getAssignee().getAssignType())) {
						PassengerAssignee passenger = (PassengerAssignee) passengerPreference.getAssignee();
						if (passenger.getPassengerRph().equals(ancillaryPassenger.getPassengerRph())) {
							for (Selection passengerSelection : passengerPreference.getSelections()) {
								if (passengerSelection.getScope().equals(scope)) {
									for (SelectedItem paxSelectedItem : passengerSelection.getSelectedItems()) {
										if (pricedSelectedItem.getId().equals(paxSelectedItem.getId())) {
											ancillaryInput = paxSelectedItem.getInput();
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return ancillaryInput;
	}

	public static class AncillaryIntegrationAdaptorWrapper {

		private ReservationAncillarySelection reservationAncillarySelection;

		private BaseAncillarySelectionsRS priceQuatedSelectdAncillary;
		
		private LCCClientReservation reservation;
		
		private boolean ancillaryModification;

		public ReservationAncillarySelection getReservationAncillarySelection() {
			return reservationAncillarySelection;
		}

		public void setReservationAncillarySelection(ReservationAncillarySelection reservationAncillarySelection) {
			this.reservationAncillarySelection = reservationAncillarySelection;
		}

		public BaseAncillarySelectionsRS getPriceQuatedSelectdAncillary() {
			return priceQuatedSelectdAncillary;
		}

		public void setPriceQuatedSelectdAncillary(BaseAncillarySelectionsRS priceQuatedSelectdAncillary) {
			this.priceQuatedSelectdAncillary = priceQuatedSelectdAncillary;
		}
		
		public LCCClientReservation getReservation() {
			return reservation;
		}

		public void setReservation(LCCClientReservation reservation) {
			this.reservation = reservation;
		}

		public boolean isAncillaryModification() {
			return ancillaryModification;
		}

		public void setAncillaryModification(boolean ancillaryModification) {
			this.ancillaryModification = ancillaryModification;
		}
	}

	public AncillarySelection getAddedAncillarySelection() {
		return addedAncillarySelection;
	}

	public void setAddedAncillarySelection(AncillarySelection addedAncillarySelection) {
		this.addedAncillarySelection = addedAncillarySelection;
	}

}
