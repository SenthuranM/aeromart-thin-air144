package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class BasicReservation extends ReservationData {

	private List<Passenger> passengers;

	private ContactInformation contactInfo;

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public ContactInformation getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInformation contactInfo) {
		this.contactInfo = contactInfo;
	}
}
