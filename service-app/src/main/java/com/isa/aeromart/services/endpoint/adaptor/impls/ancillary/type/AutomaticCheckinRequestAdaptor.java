package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public class AutomaticCheckinRequestAdaptor extends AncillaryRequestAdaptor {

	@Override
	public String getLCCAncillaryType() {
		return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AUTOMATIC_CHECKIN;
	}

	@Override
	public Object toAvailableAncillaries(TrackInfoDTO trackInfoDTO, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {
		AvailableAutomaticCheckinRequestWrapper wrapper = new AvailableAutomaticCheckinRequestWrapper();

		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
		Collections.sort(flightSegmentTOs);

		wrapper.setFlightSegmentTOs(flightSegmentTOs);
		wrapper.setAppIndicator(trackInfoDTO.getAppIndicator());
		wrapper.setModifyAnci(isModifyAnci);
		wrapper.setServiceCount(serviceCount == null ? new HashMap<>() : serviceCount);
		wrapper.setSystem(system);

		return wrapper;
	}

	@Override
	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {

		List<SelectedItem> selectedItems;
		SegmentScope segmentScope;
		String flightSegmentRPH;

		LCCAutomaticCheckinDTO selectedAutoCheckin;
		List<LCCAutomaticCheckinDTO> selectedAutoCheckins;
		Map<String, List<LCCAutomaticCheckinDTO>> selectedAutoCheckinsMapping = new HashMap<>();

		for (Preference preference : preferences) {
			List<Selection> selections = preference.getSelections();
			for (Selection selection : selections) {
				segmentScope = (SegmentScope) selection.getScope();
				flightSegmentRPH = segmentScope.getFlightSegmentRPH();

				if (!selectedAutoCheckinsMapping.containsKey(flightSegmentRPH)) {
					selectedAutoCheckinsMapping.put(flightSegmentRPH, new ArrayList<>());
				}

				selectedAutoCheckins = selectedAutoCheckinsMapping.get(flightSegmentRPH);

				selectedItems = selection.getSelectedItems();
				for (SelectedItem selectedItem : selectedItems) {
					selectedAutoCheckin = new LCCAutomaticCheckinDTO();
					selectedAutoCheckin.setAutoCheckinId(Integer.valueOf(selectedItem.getId()));
					selectedAutoCheckin.setSeatPref(selectedItem.getName());
					selectedAutoCheckins.add(selectedAutoCheckin);
				}

			}
		}

		return selectedAutoCheckinsMapping;
	}

	@Override
	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;
	}

	@Override
	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;
	}

	public class AvailableAutomaticCheckinRequestWrapper {

		private List<FlightSegmentTO> flightSegmentTOs;
		private AppIndicatorEnum appIndicator;
		private ProxyConstants.SYSTEM system;
		private boolean isModifyAnci;
		private Map<String, Integer> serviceCount;

		/**
		 * @return the flightSegmentTOs
		 */
		public List<FlightSegmentTO> getFlightSegmentTOs() {
			return flightSegmentTOs;
		}

		/**
		 * @param flightSegmentTOs
		 *            the flightSegmentTOs to set
		 */
		public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
			this.flightSegmentTOs = flightSegmentTOs;
		}

		/**
		 * @return the appIndicator
		 */
		public AppIndicatorEnum getAppIndicator() {
			return appIndicator;
		}

		/**
		 * @param appIndicator
		 *            the appIndicator to set
		 */
		public void setAppIndicator(AppIndicatorEnum appIndicator) {
			this.appIndicator = appIndicator;
		}

		/**
		 * @return the system
		 */
		public ProxyConstants.SYSTEM getSystem() {
			return system;
		}

		/**
		 * @param system
		 *            the system to set
		 */
		public void setSystem(ProxyConstants.SYSTEM system) {
			this.system = system;
		}

		/**
		 * @return the isModifyAnci
		 */
		public boolean isModifyAnci() {
			return isModifyAnci;
		}

		/**
		 * @param isModifyAnci
		 *            the isModifyAnci to set
		 */
		public void setModifyAnci(boolean isModifyAnci) {
			this.isModifyAnci = isModifyAnci;
		}

		/**
		 * @return the serviceCount
		 */
		public Map<String, Integer> getServiceCount() {
			return serviceCount;
		}

		/**
		 * @param serviceCount
		 *            the serviceCount to set
		 */
		public void setServiceCount(Map<String, Integer> serviceCount) {
			this.serviceCount = serviceCount;
		}

	}
}
