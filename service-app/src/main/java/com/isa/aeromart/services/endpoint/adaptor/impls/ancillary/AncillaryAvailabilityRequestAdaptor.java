package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.constants.AncillariesConstants.AncillaryType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;

public class AncillaryAvailabilityRequestAdaptor extends
		TransactionAwareAdaptor<AncillaryAvailabilityRQ, LCCAncillaryAvailabilityInDTO> {

	public LCCAncillaryAvailabilityInDTO adapt(AncillaryAvailabilityRQ source) {

		LCCAncillaryAvailabilityInDTO transformed;
		AncillaryRequestAdaptor transformer;
		InsuranceFltSegBuilder insFltSegBldr = null;
		List<FlightSegmentTO> filteredFlightSegmentTOs = new ArrayList<>();
		Ancillary anciType;

		List<Ancillary> ancillaries = source.getAncillaries();
		if(ancillaries.isEmpty()){
			List<AncillaryType> allAnciTypes = AncillariesConstants.AncillaryType.getAllAncillaryTypes();
			for(AncillaryType ancillaryType : allAnciTypes){
				anciType = new Ancillary();
				anciType.setType(ancillaryType.name());
				ancillaries.add(anciType);
			}
		}

		transformed = new LCCAncillaryAvailabilityInDTO();

		List<InventoryWrapper> flightSegments = AnciCommon.sort(getTransactionalAncillaryService().getFlightSegments(source.getTransactionId()));
		FareSegChargeTO pricingInformation = getTransactionalAncillaryService().getPricingInformation(source.getTransactionId());
		TravellerQuantity travellerQuantity = getTransactionalAncillaryService().getTravellerQuantityFromSession(source.getTransactionId());
		boolean isAnciModify = getTransactionalAncillaryService().isAncillaryTransaction(source.getTransactionId());

		for (Ancillary ancillary : ancillaries) {
			transformer = AncillaryAdaptorFactory.getRequestAdaptor(ancillary.getType());
			transformed.addAncillaryType(transformer.getLCCAncillaryType());
		}

		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(flightSegments);
		flightSegmentTOs = AnciCommon.populateAnciOfferData(flightSegmentTOs, pricingInformation, travellerQuantity);

		// getting insurance origin airport
		try {
			insFltSegBldr = new InsuranceFltSegBuilder(flightSegmentTOs);
			for(FlightSegmentTO flightSegmentTO : flightSegmentTOs){
				if(!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())){
					filteredFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		
		transformed.addAllFlightSegmentTOs(filteredFlightSegmentTOs);
		transformed.setTransactionIdentifier(isAnciModify ? null : source.getTransactionId() != null ? source.getTransactionId() : "");
		transformed.setQueryingSystem(ProxyConstants.SYSTEM.getEnum(getTransactionalAncillaryService().getPreferencesForAncillary(source.getTransactionId()).getSelectedSystem()));
		transformed.setInsuranceOrigin(insFltSegBldr.getOriginAirport());
		transformed.setDepartureSegmentCode(flightSegmentTOs.get(0).getSegmentCode());
		transformed.setAppIndicator(getTrackInfoDTO().getAppIndicator());
		/*
		 * setAirportServiceCountMap
		 */

		transformed.setBaggageSummaryTo(AnciCommon.toBaggageSummaryTo(flightSegmentTOs));

		return transformed;
	}
}
