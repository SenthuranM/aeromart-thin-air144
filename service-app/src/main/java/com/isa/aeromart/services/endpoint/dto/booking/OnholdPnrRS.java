package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class OnholdPnrRS extends TransactionalBaseRS {
	private String pnr;
	private String onholdReleaseDuration;
	private String lastName;
	private Date firstDepartureDate;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getOnholdReleaseDuration() {
		return onholdReleaseDuration;
	}

	public void setOnholdReleaseDuration(String onholdReleaseDuration) {
		this.onholdReleaseDuration = onholdReleaseDuration;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getFirstDepartureDate() {
		return firstDepartureDate;
	}

	public void setFirstDepartureDate(Date firstDepartureDate) {
		this.firstDepartureDate = firstDepartureDate;
	}

}
