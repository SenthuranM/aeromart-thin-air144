package com.isa.aeromart.services.endpoint.dto.booking;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class LoadReservationValidateRes extends TransactionalBaseRS {

	private String pnr;

	private String sellingAirlineCode;

	private String airlineCode;

	private boolean groupPnr;

	private boolean validLoadRequest;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getSellingAirlineCode() {
		return sellingAirlineCode;
	}

	public void setSellingAirlineCode(String sellingAirlineCode) {
		this.sellingAirlineCode = sellingAirlineCode;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	public boolean isValidLoadRequest() {
		return validLoadRequest;
	}

	public void setValidLoadRequest(boolean validLoadRequest) {
		this.validLoadRequest = validLoadRequest;
	}
}
