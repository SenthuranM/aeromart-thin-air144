package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class DropDownListRS extends TransactionalBaseRS{

	private List<PaxTitle> paxTitels;
	
	private Map<String, List<PaxTitle>> paxTypeWiseTitles;

	private List<IBELanguage> languages;
	
	private Map<String, List<StateDetails>> countryWiseStates;

	public List<PaxTitle> getPaxTitels() {
		return paxTitels;
	}

	public void setPaxTitels(List<PaxTitle> paxTitels) {
		this.paxTitels = paxTitels;
	}

	public List<IBELanguage> getLanguages() {
		return languages;
	}

	public void setLanguages(List<IBELanguage> languages) {
		this.languages = languages;
	}

	public Map<String, List<PaxTitle>> getPaxTypeWiseTitles() {
		return paxTypeWiseTitles;
	}

	public void setPaxTypeWiseTitles(Map<String, List<PaxTitle>> paxTypeWiseTitles) {
		this.paxTypeWiseTitles = paxTypeWiseTitles;
	}

	public Map<String, List<StateDetails>> getCountryWiseStates() {
		return countryWiseStates;
	}

	public void setCountryWiseStates(Map<String, List<StateDetails>> countryWiseStates) {
		this.countryWiseStates = countryWiseStates;
	}
		
}
