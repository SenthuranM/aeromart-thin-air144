package com.isa.aeromart.services.endpoint.utils.common;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class CurrencyDecorator implements VisitorCommand<BigDecimal> {

	private String currency = null;
	private ExchangeRateProxy exchangeRateProxy;
	private static final String SERVICE_APP_PACKAGE = "com.isa.aeromart.services.endpoint.dto";

	public CurrencyDecorator(String preferredCurrency, TrackInfoDTO trackInfo) {
		this.currency = AppSysParamsUtil.getBaseCurrency();
		if (preferredCurrency != null && !"".equals(preferredCurrency.trim())
				&& !AppSysParamsUtil.getBaseCurrency().equals(preferredCurrency)) {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(new Date());
			try {
				if (exchangeRateProxy.isValidCurrencyExchangeRateExists(preferredCurrency,
						CommonServiceUtil.getApplicationEngine(trackInfo.getAppIndicator()))) {
					this.currency = preferredCurrency;
					this.exchangeRateProxy = exchangeRateProxy;
				}
			} catch (ModuleException e) {
				// TODO Auto-generated catch block
			}
		}
	}

	public String getCurrency() {
		return this.currency;
	}

	public void decorate(Object target) throws ModuleException {
		if (exchangeRateProxy != null) {
			FieldVisitor<CurrencyValue, BigDecimal> visitor = new FieldVisitor<CurrencyValue, BigDecimal>(CurrencyValue.class,
					BigDecimal.class, this, SERVICE_APP_PACKAGE);
			visitor.visit(target);
		}
	}

	private BigDecimal applyConvertion(BigDecimal input) throws ModuleException {
		if (exchangeRateProxy != null) {
			return exchangeRateProxy.convert(this.currency, input, true);
		}
		return input;
	}

	@Override
	public BigDecimal execute(BigDecimal input) throws ModuleException {
		return applyConvertion(input);
	}

}

interface VisitorCommand<C> {
	public C execute(C input) throws ModuleException;
}

class FieldVisitor<A extends Annotation, B> {
	private static final String GET = "get";
	private static final String SET = "set";
	private Class<A> annotationFilter = null;
	private String[] filterPackages = null;
	private Class<B> fieldType = null;
	private VisitorCommand<B> command;
	private List<String> visitedPath;

	public FieldVisitor(Class<A> annotationFilter, Class<B> fieldType, VisitorCommand<B> command, String... filterPackages) {
		this.fieldType = fieldType;
		this.annotationFilter = annotationFilter;
		this.filterPackages = filterPackages;
		this.command = command;
		this.visitedPath = new ArrayList<String>();
	}

	private boolean isEligiblePackage(String packageName) {
		if (filterPackages != null) {
			for (String packageF : filterPackages) {
				if (packageName.startsWith(packageF)) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}

	private boolean isEligibleField(Field field) {
		if (annotationFilter != null) {
			return field.isAnnotationPresent(annotationFilter);
		} else {
			return true;
		}
	}

	private boolean isValidFieldType(Class<?> class1) {
		if (fieldType != null) {
			return class1.isAssignableFrom(fieldType);
		} else {
			return true;
		}
	}

	private boolean isExactAnnotationMatch(Field field) {
		return (isEligibleField(field) && isValidFieldType(field.getType()));
	}

	private boolean isTraverableField(Class<?> type) {
		return !type.isPrimitive() && !type.isEnum() && !type.isArray();
	}

	private void invokeCommand(Object t, Field field) throws ModuleException {
		Class<? extends Object> claxx = t.getClass();
		try {
			Method getterMethod = claxx.getMethod(getGetterName(field.getName()));
			if (getterMethod != null && getterMethod.getReturnType().isAssignableFrom(fieldType)) {
				B value = (B) getterMethod.invoke(t, null);
				if (value != null) {
					Method setterMethod = claxx.getMethod(getSetterName(field.getName()), fieldType);
					if (setterMethod != null && command != null) {
						B commandValue = command.execute(value);
						setterMethod.invoke(t, commandValue);
					}
				}
			}
		} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new ModuleException("currency.decoration.failed", e);
		}

	}

	public Object getObject(Object t, Field field) throws ModuleException {
		Object obj = null;
		try {
			Class<? extends Object> claxx = t.getClass();
			Method getterMethod = claxx.getMethod(getGetterName(field.getName()));
			if (getterMethod != null) {
				obj = getterMethod.invoke(t, null);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new ModuleException("currency.decoration.failed", e);
		}
		return obj;
	}

	private void listVisitor(Collection<?> list) throws ModuleException {
		if (list != null) {
			Iterator<?> itr = list.iterator();
			while (itr.hasNext()) {
				Object obj = itr.next();
				if (obj != null) {
					visit(obj);
				}
			}
		}
	}

	public void visit(Object t) throws ModuleException {
		if (t == null) {
			return;
		}
		Class<? extends Object> claxx = t.getClass();
		if (isTraverableField(claxx) && isNotVisited(t.toString())) {
			pushVistor(t.toString());
			if (Collection.class.isAssignableFrom(claxx)) {
				listVisitor((Collection<?>) t);
			} else {
				for (Field field : getFields(claxx)) {
					if (isTraverableField(field.getType())) {
						if (isExactAnnotationMatch(field)) {
							invokeCommand(t, field);
						} else if (isTraverableField(field.getType())
								&& isEligiblePackage(field.getType().getPackage().getName())) {
							Object obj = getObject(t, field);
							if (obj != null) {
								visit(obj);
							}
						} else if (Collection.class.isAssignableFrom(field.getType())) {
							Collection<?> list = (Collection<?>) getObject(t, field);
							listVisitor(list);
						}
					}
				}
			}
			popVistor(t.toString());

		}
	}

	private void popVistor(String objId) {
		visitedPath.remove(objId);
	}

	private void pushVistor(String objId) {
		visitedPath.add(objId);
	}

	private boolean isNotVisited(String objId) {
		return !visitedPath.contains(objId);
	}

	private String getGetterName(String fieldName) {
		return GET + captalizeFirstLetter(fieldName);
	}

	private String captalizeFirstLetter(String fieldName) {
		return fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}

	private String getSetterName(String fieldName) {
		return SET + captalizeFirstLetter(fieldName);
	}

	private void removeDuplicates(List<Field> fields) {
		if (fields != null && fields.size() > 0) {
			Set<String> names = new HashSet<String>();
			Iterator<Field> itr = fields.iterator();
			while (itr.hasNext()) {
				Field field = itr.next();
				if (names.contains(field.getName())) {
					itr.remove();
				} else {
					names.add(field.getName());
				}
			}
		}
	}

	private Field[] getFields(Class<?> claxx) {
		List<Field> fields = new ArrayList<Field>();
		Class<?> itrClass = claxx;
		while (!itrClass.getSuperclass().getTypeName().equals(Object.class.getTypeName())) {
			fields.addAll(Arrays.asList(itrClass.getDeclaredFields()));
			itrClass = itrClass.getSuperclass();
		}
		fields.addAll(Arrays.asList(claxx.getDeclaredFields()));
		removeDuplicates(fields);
		Field[] arrFields = new Field[fields.size()];
		if (fields.size() > 0) {
			fields.toArray(arrFields);
		}
		return arrFields;
	}
}
