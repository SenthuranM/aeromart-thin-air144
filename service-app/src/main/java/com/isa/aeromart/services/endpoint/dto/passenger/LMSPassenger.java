package com.isa.aeromart.services.endpoint.dto.passenger;

public class LMSPassenger {

	private String ffid;

	private String firstName;

	private String lastName;

	private int passengerSequence;

	private String paxType;

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public int getPassengerSequence() {
		return passengerSequence;
	}

	public void setPassengerSequence(int passengerSequence) {
		this.passengerSequence = passengerSequence;
	}

}
