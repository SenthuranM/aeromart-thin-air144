package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.PromoInfo;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;

public class PromotionRSAdaptor implements Adaptor<ApplicablePromotionDetailsTO, PromoInfo> {

	private SYSTEM searchSystem;
	private TravellerQuantity travellerQuantity;
	private TrackInfoDTO trackInfo;
	private PriceInfoTO priceInfoTO;
	private String transactionId;

	public PromotionRSAdaptor(SYSTEM searchSystem, TravellerQuantity travellerQuantity, TrackInfoDTO trackInfo,
			PriceInfoTO priceInfoTO, String transactionId) {
		this.searchSystem = searchSystem;
		this.travellerQuantity = travellerQuantity;
		this.trackInfo = trackInfo;
		this.priceInfoTO = priceInfoTO;
		this.transactionId = transactionId;
	}

	@Override
	public PromoInfo adapt(ApplicablePromotionDetailsTO source) {
		PromoInfo target = new PromoInfo();
		DiscountedFareDetailAdaptor discountDetailAdapt = new DiscountedFareDetailAdaptor();
		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();

		baseAvailRQ.getAvailPreferences().setSearchSystem(searchSystem);
		TravelerQuantityAdaptor travQtyAdapt = new TravelerQuantityAdaptor(baseAvailRQ.getTravelerInfoSummary());
		travQtyAdapt.adapt(travellerQuantity);
		DiscountedFareDetails discountDetails = discountDetailAdapt.adapt(source);
		ReservationDiscountDTOAdaptor resDiscAdapt = new ReservationDiscountDTOAdaptor(baseAvailRQ, trackInfo, true, priceInfoTO,
				transactionId, false);
		ReservationDiscountDTO resDiscDTO = resDiscAdapt.adapt(discountDetails);

		target.setType(source.getPromoType());
		target.setNotes(source.getDescription());
		target.setDiscountType(source.getDiscountType());
		target.setDiscountAs(source.getDiscountAs());
		target.setApplicableOnds(source.getApplicableOnds());
		target.setCode(source.getPromoCode());
		target.setDiscountAmount(resDiscDTO.getTotalDiscount());
		target.setDiscountCode(resDiscDTO.getDiscountCode());

		return target;
	}

}
