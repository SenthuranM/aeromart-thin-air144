package com.isa.aeromart.services.endpoint.dto.ancillary.type.airporttransfer;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class AirportTransferItems extends AncillaryItemLeaf {

	private List<AirportTransferItem> items;

    public AirportTransferItems() {
        setType(AncillariesConstants.Type.AIRPORT_TRANSFER);
    }

    public List<AirportTransferItem> getItems() {
		return items;
	}

	public void setItems(List<AirportTransferItem> items) {
		this.items = items;
	}
	
}
