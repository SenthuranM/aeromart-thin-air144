package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.ContactInformation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.MonetaryAmendmentDefinition;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Passenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationAncillarySelection;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySelection;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionContactInformation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCPassengerTypeQuantityDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class AnciCommon {

	public static final String SEGMENT_SEPERATOR = "/";

	public static final Integer INBOUND = 2;

	public static final Integer OUTBOUND = 1;

	public static final String CANCEL_STATUS = "CNX";

	public static final String MONEY = "Money";

	public static final String CREDIT = "Credit";

	public static final String DEBIT = "Debit";

	public static AncillariesConstants.AncillaryType getAncillaryType(String lccAncillaryType) {

		AncillariesConstants.AncillaryType ancillaryType = null;

		if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.BAGGAGE)) {
			ancillaryType = AncillariesConstants.AncillaryType.BAGGAGE;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.SEAT_MAP)) {
			ancillaryType = AncillariesConstants.AncillaryType.SEAT;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.MEALS)) {
			ancillaryType = AncillariesConstants.AncillaryType.MEAL;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.INSURANCE)) {
			ancillaryType = AncillariesConstants.AncillaryType.INSURANCE;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.SSR)) {
			ancillaryType = AncillariesConstants.AncillaryType.SSR_IN_FLIGHT;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AIRPORT_SERVICE)) {
			ancillaryType = AncillariesConstants.AncillaryType.SSR_AIRPORT;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AIRPORT_TRANSFER)) {
			ancillaryType = AncillariesConstants.AncillaryType.AIRPORT_TRANSFER;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.FLEXI)) {
			ancillaryType = AncillariesConstants.AncillaryType.FLEXIBILITY;
		} else if (lccAncillaryType.equals(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AUTOMATIC_CHECKIN)) {
			ancillaryType = AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN;
		}

		return ancillaryType;
	}

	public static EXTERNAL_CHARGES getExternalCharge(AncillariesConstants.AncillaryType ancillaryType) {

		EXTERNAL_CHARGES externalChangeType = null;

		switch (ancillaryType) {
		case BAGGAGE:
			externalChangeType = EXTERNAL_CHARGES.BAGGAGE;
			break;
		case MEAL:
			externalChangeType = EXTERNAL_CHARGES.MEAL;
			break;
		case SEAT:
			externalChangeType = EXTERNAL_CHARGES.SEAT_MAP;
			break;
		case INSURANCE:
			externalChangeType = EXTERNAL_CHARGES.INSURANCE;
			break;
		case SSR_IN_FLIGHT:
			externalChangeType = EXTERNAL_CHARGES.INFLIGHT_SERVICES;
			break;
		case SSR_AIRPORT:
			externalChangeType = EXTERNAL_CHARGES.AIRPORT_SERVICE;
			break;
		case FLEXIBILITY:
			externalChangeType = EXTERNAL_CHARGES.FLEXI_CHARGES;
			break;
		case AIRPORT_TRANSFER:
			externalChangeType = EXTERNAL_CHARGES.AIRPORT_TRANSFER;
			break;
		case AUTOMATIC_CHECKIN:
			externalChangeType = EXTERNAL_CHARGES.AUTOMATIC_CHECKIN;
			break;

		}

		return externalChangeType;
	}

	public static EXTERNAL_CHARGES getExternalCharge(AncillariesConstants.MonetaryAmendment monetaryAmendment) {

		EXTERNAL_CHARGES externalChangeType = null;

		switch (monetaryAmendment) {
		case JN_TAX:
			externalChangeType = EXTERNAL_CHARGES.JN_ANCI;
			break;
		}

		return externalChangeType;
	}

	public static LCCReservationBaggageSummaryTo toBaggageSummaryTo(List<FlightSegmentTO> flightSegmentTOs) {

		LCCReservationBaggageSummaryTo baggageSummaryTo = new LCCReservationBaggageSummaryTo();

		Map<String, String> bookingClasses = new HashMap<>();
		Map<String, String> classesOfService = new HashMap<>();
		Map<String, String> logicalCC = new HashMap<>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			bookingClasses.put(flightSegmentTO.getSegmentCode(), flightSegmentTO.getBookingClass());
			classesOfService.put(flightSegmentTO.getFlightRefNumber(), flightSegmentTO.getCabinClassCode());
			if (flightSegmentTO.getLogicalCabinClassCode() != null && !"".equals(flightSegmentTO.getLogicalCabinClassCode())) {
				logicalCC.put(flightSegmentTO.getFlightRefNumber(), flightSegmentTO.getLogicalCabinClassCode());
			}
		}

		baggageSummaryTo.setBookingClasses(bookingClasses);
		baggageSummaryTo.setClassesOfService(classesOfService);
		baggageSummaryTo.setLogicalCC(logicalCC);
		return baggageSummaryTo;
	}

	public static Map<String, String> toFlightSegmentRefToOndIdMapping(List<OndUnit> ondUnits) {
		Map<String, String> mapping = new HashMap<>();

		for (OndUnit ondUnit : ondUnits) {
			for (String flightSegmentRef : ondUnit.getFlightSegmentRPH()) {
				mapping.put(flightSegmentRef, ondUnit.getOndId());
			}
		}

		return mapping;
	}

	public static String getBaggageId(String... vals) {
		String baggageId = "";
		for (String val : vals) {
			baggageId += val + "|";
		}

		return baggageId;
	}

	public static String getBaggageOndChargeId(String baggageId) {
		return baggageId.split("\\|")[0];
	}

	public static String getBaggageName(String baggageId) {
		return baggageId.split("\\|")[1];
	}

	public static String toTextualRule(String ruleCode, Object... vals) {
		String textualRule = ruleCode + "|";

		for (Object o : vals) {
			textualRule += toString(o);
		}

		return textualRule;
	}

	private static String toString(Object o) {

		String val = "";
		Collection<?> collection;

		if (o instanceof Collection) {
			collection = (Collection) o;
			for (Object i : collection) {
				val += AnciCommon.toString(i) + "|";
			}
		} else {
			val = o.toString();
		}
		return val;
	}

	public static List<FlightSegmentTO> toFlightSegmentTOs(List<InventoryWrapper> inventory) {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<>();

		for (InventoryWrapper i : inventory) {
			flightSegmentTOs.add(i.getFlightSegmentTO());
		}

		return flightSegmentTOs;
	}

	public static List<InventoryWrapper> toFlightInventories(List<SessionFlightSegment> flightSegmentTOs) {
		List<InventoryWrapper> inventory = new ArrayList<>();

		if (flightSegmentTOs != null) {
			for (SessionFlightSegment flightSegment : flightSegmentTOs) {
				inventory.add(new InventoryWrapper(flightSegment));
			}
		}

		return inventory;
	}

	public static List<PassengerTypeQuantityTO> getPaxQtyList(TravellerQuantity travellerQuantity) {

		List<PassengerTypeQuantityTO> paxQtyList = new ArrayList<PassengerTypeQuantityTO>();

		PassengerTypeQuantityTO adultPassenger = new PassengerTypeQuantityTO();
		adultPassenger.setPassengerType(PaxTypeTO.ADULT);
		adultPassenger.setQuantity(travellerQuantity.getAdultCount());
		paxQtyList.add(adultPassenger);

		PassengerTypeQuantityTO childPassenger = new PassengerTypeQuantityTO();
		childPassenger.setPassengerType(PaxTypeTO.CHILD);
		childPassenger.setQuantity(travellerQuantity.getChildCount());
		paxQtyList.add(childPassenger);

		PassengerTypeQuantityTO infantPassenger = new PassengerTypeQuantityTO();
		infantPassenger.setPassengerType(PaxTypeTO.INFANT);
		infantPassenger.setQuantity(travellerQuantity.getInfantCount());
		paxQtyList.add(infantPassenger);

		return paxQtyList;
	}

	public static BaseAvailRQ toBaseAvailRQ(List<PassengerTypeQuantityTO> passengerTypeList, PreferenceInfo preferenceInfo,
			ApplicationEngine appEngine) {
		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		baseAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList().addAll(passengerTypeList);
		baseAvailRQ.getAvailPreferences().setSearchSystem(ProxyConstants.SYSTEM.getEnum(preferenceInfo.getSelectedSystem()));
		baseAvailRQ.getAvailPreferences().setAppIndicator(appEngine);
		baseAvailRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(false); // TODO need to get this from the
																					// session
		baseAvailRQ.getTravelPreferences().setBookingType(null);// TODO for IBE booking doesn't allow to select booking
																// type, if service-App is exposed to XBE need to store
																// this in the transaction

		return baseAvailRQ;
	}

	public static List<FlightSegmentTO> getFlightSegments(Set<LCCClientReservationSegment> segments) {
		List<FlightSegmentTO> flightSegmentList = new ArrayList<FlightSegmentTO>();
		for (LCCClientReservationSegment resSeg : segments) {
			if (!resSeg.getStatus().equals(CANCEL_STATUS)) {
				FlightSegmentTO fltSegTO = new FlightSegmentTO();
				fltSegTO.setFlightNumber(resSeg.getFlightNo());
				fltSegTO.setArrivalDateTime(resSeg.getArrivalDate());
				fltSegTO.setArrivalDateTimeZulu(resSeg.getArrivalDateZulu());
				fltSegTO.setDepartureDateTime(resSeg.getDepartureDate());
				fltSegTO.setDepartureDateTimeZulu(resSeg.getDepartureDateZulu());
				fltSegTO.setFlightDuration(resSeg.getFlightDuration());
				fltSegTO.setFlightModelNumber(resSeg.getFlightModelNumber());
				fltSegTO.setFlightRefNumber(resSeg.getFlightSegmentRefNumber());
				fltSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));
				fltSegTO.setPnrSegId(resSeg.getPnrSegID().toString());
				fltSegTO.setReturnFlag(resSeg.getReturnFlag().equals("Y") ? true : false);
				fltSegTO.setRouteRefNumber(resSeg.getRouteRefNumber());
				fltSegTO.setSegmentCode(resSeg.getSegmentCode());
				fltSegTO.setBaggageONDGroupId(resSeg.getBaggageONDGroupId());
				fltSegTO.setFareBasisCode(resSeg.getFareTO().getFareBasisCode());
				fltSegTO.setBookingClass(resSeg.getFareTO().getBookingClassCode());
				fltSegTO.setBookingType(resSeg.getBookingType());
				fltSegTO.setFlexiID(resSeg.getFlexiRuleID());
				fltSegTO.setSegmentSequence(resSeg.getSegmentSeq());
				fltSegTO.setCabinClassCode(resSeg.getCabinClassCode());
				fltSegTO.setOperatingAirline(resSeg.getCarrierCode());
				fltSegTO.setLogicalCabinClassCode(resSeg.getLogicalCabinClass());
				flightSegmentList.add(fltSegTO);
			}
		}

		return flightSegmentList;
	}

	public static FlightSegmentTO toFlightSegmentTO(SessionFlightSegment sessionFlightSegment) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();

		flightSegmentTO.setSegmentCode(sessionFlightSegment.getSegmentCode());
		flightSegmentTO.setDepartureDateTime(sessionFlightSegment.getDepartureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(sessionFlightSegment.getDepartureDateTimeZulu());
		flightSegmentTO.setArrivalDateTime(sessionFlightSegment.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(sessionFlightSegment.getArrivalDateTimeZulu());
		flightSegmentTO.setFlightNumber(sessionFlightSegment.getFlightDesignator());
		flightSegmentTO.setOperatingAirline(sessionFlightSegment.getOperatingAirline());
		flightSegmentTO.setFlightRefNumber(sessionFlightSegment.getLCCFlightReference());
		flightSegmentTO.setCabinClassCode(sessionFlightSegment.getCabinClass());
		flightSegmentTO.setLogicalCabinClassCode(sessionFlightSegment.getLogicalCabinClass());
		flightSegmentTO.setFlightSegId(sessionFlightSegment.getFlightSegmentId());
		flightSegmentTO.setOndSequence(sessionFlightSegment.getOndSequence());
		flightSegmentTO.setPnrSegId(sessionFlightSegment.getPnrSegId());
		flightSegmentTO.setSegmentSequence(sessionFlightSegment.getSegmentSequence());
		// flightSegmentTO.setAdultCount();
		// flightSegmentTO.setChildCount();
		// flightSegmentTO.setInfantCount();
		// flightSegmentTO.setOndSequence();
		flightSegmentTO.setFareBasisCode(sessionFlightSegment.getFareBasisCode());
		flightSegmentTO.setSectorONDCode(sessionFlightSegment.getSectorONDCode());
		flightSegmentTO.setBookingClass(sessionFlightSegment.getBookingClass());
		// flightSegmentTO.setReturnFlag(false);
		flightSegmentTO.setFlexiID(sessionFlightSegment.getFlexiId());
		flightSegmentTO.setBaggageONDGroupId(sessionFlightSegment.getBaggageONDGroupId());

		return flightSegmentTO;
	}

	public static InventoryWrapper resolveInventory(List<InventoryWrapper> inventory, String flightSegmentRph) {

		InventoryWrapper inventoryWrapper = null;

		for (InventoryWrapper i : inventory) {
			if (i.getFlightSegmentTO().getFlightRefNumber().equals(flightSegmentRph)) {
				inventoryWrapper = i;
			}
		}

		return inventoryWrapper;
	}

	public static String resolveResSegmentBaggageOndId(Set<LCCClientReservationSegment> segments, String flightSegmentRph) {

		String baggageOndGroupId = null;

		for (LCCClientReservationSegment lccClientReservationSegment : segments) {
			if (lccClientReservationSegment.getFlightSegmentRefNumber().equals(flightSegmentRph)) {
				baggageOndGroupId = lccClientReservationSegment.getBaggageONDGroupId();
			}
		}

		return baggageOndGroupId;
	}

	public static String resolvePassengerType(List<SessionPassenger> passengers, String passengerRPH) {

		for (SessionPassenger sessionPassenger : passengers) {
			if (sessionPassenger.getPassengerRph().equals(passengerRPH)) {
				return sessionPassenger.getType();
			}
		}

		return null;
	}

	public static String resolvePassengerTypeForValidation(List<Passenger> passengers, String passengerRPH) {

		for (Passenger passenger : passengers) {
			if (passenger.getPassengerRph().equals(passengerRPH)) {
				return passenger.getType();
			}
		}

		return null;
	}

	public static BasicReservation toBasicReservation(SessionBasicReservation reservation) {

		SessionContactInformation sessionContactInformation;

		BasicReservation res = new BasicReservation();
		List<Passenger> passengers;
		Passenger passenger;
		ContactInformation contactInformation;

		passengers = new ArrayList<>();
		if (reservation != null) {
			for (SessionPassenger sessionPassenger : reservation.getPassengers()) {
				passenger = new Passenger();
				passenger.setTitle(sessionPassenger.getTitle());
				passenger.setFirstName(sessionPassenger.getFirstName());
				passenger.setLastName(sessionPassenger.getLastName());
				passenger.setPassengerRph(sessionPassenger.getPassengerRph());
				passenger.setType(sessionPassenger.getType());
				passenger.setDateOfBirth(sessionPassenger.getDateOfBirth());

				passengers.add(passenger);
			}
			res.setPassengers(passengers);

			sessionContactInformation = reservation.getContactInformation();
			if (sessionContactInformation != null) {
				contactInformation = new ContactInformation();
				contactInformation.setTitle(sessionContactInformation.getTitle());
				contactInformation.setFirstName(sessionContactInformation.getFirstName());
				contactInformation.setLastName(sessionContactInformation.getLastName());
				contactInformation.setAddress(sessionContactInformation.getAddress());
				contactInformation.setLandNumber(sessionContactInformation.getLandNumber());
				contactInformation.setMobileNumber(sessionContactInformation.getMobileNumber());
				contactInformation.setEmailAddress(sessionContactInformation.getEmailAddress());
				contactInformation.setCountry(sessionContactInformation.getCountry());

				res.setContactInfo(contactInformation);
			}
		}

		return res;

	}

	public static List<Passenger> toPassengers(List<SessionPassenger> sessionPassengers) {
		List<Passenger> passengerList = new ArrayList<Passenger>();
		Passenger passenger;
		if (sessionPassengers != null && sessionPassengers.size() > 0) {
			for (SessionPassenger sessionPassenger : sessionPassengers) {
				passenger = new Passenger();
				passenger.setFirstName(sessionPassenger.getFirstName());
				passenger.setLastName(sessionPassenger.getLastName());
				passenger.setPassengerRph(sessionPassenger.getPassengerRph());
				passenger.setTitle(sessionPassenger.getTitle());
				passenger.setType(sessionPassenger.getType());
				passengerList.add(passenger);
			}

		}

		return passengerList;
	}

	public static boolean isAncillaryQuoted(AncillaryQuotation ancillaryQuotation,
			AncillariesConstants.AncillaryType ancillaryType) {

		boolean isAncillaryQuoted = false;
		AncillariesConstants.AncillaryType tempAnciType;

		if (ancillaryQuotation.getQuotedAncillaries() != null) {
			for (AncillaryType anciType : ancillaryQuotation.getQuotedAncillaries()) {

				tempAnciType = AncillariesConstants.AncillaryType.resolveAncillaryType(anciType.getAncillaryType());
				if (ancillaryType == tempAnciType) {
					isAncillaryQuoted = true;
					break;
				}
			}
		}

		return isAncillaryQuoted;
	}

	public static AncillaryType getQuotedAncillary(AncillaryQuotation ancillaryQuotation,
			AncillariesConstants.AncillaryType ancillaryType) {

		AncillaryType quotedAncillary = null;
		AncillariesConstants.AncillaryType tempAnciType;
		for (AncillaryType anciType : ancillaryQuotation.getQuotedAncillaries()) {

			tempAnciType = AncillariesConstants.AncillaryType.resolveAncillaryType(anciType.getAncillaryType());
			if (ancillaryType == tempAnciType) {
				quotedAncillary = anciType;
				break;
			}
		}

		return quotedAncillary;
	}

	public static List<FlightSegmentTO> populateAnciOfferData(List<FlightSegmentTO> flightSegmentTOs,
			FareSegChargeTO pricingInformation, TravellerQuantity travellerQuantity) {

		int adultCount = travellerQuantity.getAdultCount();
		int childCount = travellerQuantity.getChildCount();
		int infantCount = travellerQuantity.getInfantCount();

		for (FlightSegmentTO flightSegment : flightSegmentTOs) {

			flightSegment.setAdultCount(adultCount);
			flightSegment.setChildCount(childCount);
			flightSegment.setInfantCount(infantCount);

			if (pricingInformation != null && pricingInformation.getOndFareSegChargeTOs() != null) {
				for (OndFareSegChargeTO ondFareSegChargeTO : pricingInformation.getOndFareSegChargeTOs()) {
					if (ondFareSegChargeTO.getFsegBCAlloc().containsKey(flightSegment.getFlightSegId())) {
						flightSegment.setFareBasisCode(ondFareSegChargeTO.getFareBasisCode());
						flightSegment.setBookingClass(
								resolveBookingClass(flightSegment.getFlightSegId(), ondFareSegChargeTO.getFsegBCAlloc()));
						/*
						 * AMSWAT-130 fix 
						 * correct ond sequence is retrieved by ondFareSegChargeTO.getOndSequence()
						 */
						flightSegment.setOndSequence(ondFareSegChargeTO.getOndSequence());
						break;
					}
				}
			}

			boolean hasReturnSegment = hasReturnSegments(flightSegmentTOs);
			if (hasReturnSegment) {
				flightSegment.setParticipatingJourneyReturn(true);
				Date inverseDeparture = getInverseSegmentsDeparture(flightSegmentTOs, flightSegment);
				if (inverseDeparture == null) {
					inverseDeparture = getInverseONDDeparture(flightSegmentTOs, flightSegment);
				}
				flightSegment.setInverseSegmentDeparture(inverseDeparture);
			}

			String sectorCode = null;
			Integer mapKey = flightSegment.isReturnFlag() ? INBOUND : OUTBOUND;
			Map<Integer, Map<String, String>> journeyCarrierWiseONDs = getJourneyCarrierWiseSectorCodes(flightSegmentTOs);
			sectorCode = journeyCarrierWiseONDs.get(mapKey).get(flightSegment.getOperatingAirline());

			sectorCode = sectorCode == null ? "" : sectorCode;
			flightSegment.setSectorONDCode(sectorCode);
		}

		return flightSegmentTOs;
	}

	public static BigDecimal getRemoveAncillaryTotal(List<AncillaryIntegratePassengerTO> passengers) {
		BigDecimal total = BigDecimal.ZERO;
		for (AncillaryIntegratePassengerTO passenger : passengers) {
			total = AccelAeroCalculator.add(total, getToRemoveAncillaryTotal(passenger.getAncillariesToRemove()));
		}
		return total;
	}

	private static BigDecimal getToRemoveAncillaryTotal(List<LCCSelectedSegmentAncillaryDTO> toRList) {
		BigDecimal total = BigDecimal.ZERO;
		if (toRList != null) {
			for (LCCSelectedSegmentAncillaryDTO segAnci : toRList) {
				if (segAnci.getAirSeatDTO() != null && segAnci.getAirSeatDTO().getSeatNumber() != null
						&& !segAnci.getAirSeatDTO().getSeatNumber().trim().equals("")) {
					total = AccelAeroCalculator.add(total, segAnci.getAirSeatDTO().getSeatCharge());
				}

				if (segAnci.getMealDTOs() != null) {
					for (LCCMealDTO meal : segAnci.getMealDTOs()) {
						total = AccelAeroCalculator.add(total,
								AccelAeroCalculator.multiply(meal.getMealCharge(), new BigDecimal(meal.getAllocatedMeals())));
					}
				}

				if (segAnci.getBaggageDTOs() != null) {
					for (LCCBaggageDTO baggage : segAnci.getBaggageDTOs()) {
						total = AccelAeroCalculator.add(total, baggage.getBaggageCharge());
					}
				}

				if (segAnci.getSpecialServiceRequestDTOs() != null) {
					for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
						total = AccelAeroCalculator.add(total, ssr.getCharge());
					}
				}

				if (segAnci.getAirportServiceDTOs() != null) {
					for (LCCAirportServiceDTO airportService : segAnci.getAirportServiceDTOs()) {
						total = AccelAeroCalculator.add(total, airportService.getServiceCharge());
					}
				}

				if (segAnci.getAirportTransferDTOs() != null) {
					for (LCCAirportServiceDTO airportTransfer : segAnci.getAirportTransferDTOs()) {
						total = AccelAeroCalculator.add(total, airportTransfer.getServiceCharge());
					}
				}

				if (segAnci.getAutomaticCheckinDTOs() != null) {
					for (LCCAutomaticCheckinDTO autoCheckin : segAnci.getAutomaticCheckinDTOs()) {
						total = AccelAeroCalculator.add(total, autoCheckin.getAutomaticCheckinCharge());
					}
				}
			}

		}
		return total;
	}

	private static String resolveBookingClass(Integer fltSegId,
			LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc) {
		return fsegBCAlloc.get(fltSegId).getLast().getBookingClassCode();
	}

	private static boolean hasReturnSegments(List<FlightSegmentTO> flightSegments) {
		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			if (flightSegmentTO.isReturnFlag()) {
				return true;
			}
		}
		return false;
	}

	private static Date getInverseSegmentsDeparture(List<FlightSegmentTO> allSegments, FlightSegmentTO segment) {
		String inverseSegCode = getInverseSegmentCode(segment.getSegmentCode());
		for (FlightSegmentTO seg : allSegments) {
			if (seg.getSegmentCode().equals(inverseSegCode)) {
				return seg.getDepartureDateTime();
			}
		}
		return null;
	}

	private static String getInverseSegmentCode(String ond) {
		String inverseOND = "";
		String airports[] = ond.split(SEGMENT_SEPERATOR);
		for (int i = airports.length - 1; i >= 0; i--) {
			inverseOND += i == airports.length - 1 ? airports[i] : SEGMENT_SEPERATOR + airports[i];
		}
		return inverseOND;
	}

	private static Date getInverseONDDeparture(List<FlightSegmentTO> allSegments, FlightSegmentTO segment) {
		String inverseONDCode = getInverseONDCode(segment.getSegmentCode());
		for (FlightSegmentTO seg : allSegments) {
			String ondCode = getOND(seg.getSegmentCode());
			if (ondCode.equals(inverseONDCode)) {
				return seg.getDepartureDateTime();
			}
		}
		return null;
	}

	private static String getInverseONDCode(String segCode) {
		String airports[] = segCode.split(SEGMENT_SEPERATOR);
		String origin = airports[0];
		String destination = airports[airports.length - 1];
		return destination + SEGMENT_SEPERATOR + origin;
	}

	private static String getOND(String segCode) {
		String airports[] = segCode.split(SEGMENT_SEPERATOR);
		String origin = airports[0];
		String destination = airports[airports.length - 1];
		return origin + SEGMENT_SEPERATOR + destination;
	}

	private static Map<Integer, Map<String, String>> getJourneyCarrierWiseSectorCodes(List<FlightSegmentTO> flightSegments) {

		Map<Integer, Map<String, String>> journeyCarrierWiseSectorCodes = new HashMap<Integer, Map<String, String>>();

		Map<Integer, List<FlightSegmentTO>> journeyWiseSegments = getInBoundOutBoundSegments(flightSegments);
		for (Entry<Integer, List<FlightSegmentTO>> journeySegEntry : journeyWiseSegments.entrySet()) {
			Map<String, List<FlightSegmentTO>> carrierWiseSegments = getCarrierWiseSegments(journeySegEntry.getValue());
			for (Entry<String, List<FlightSegmentTO>> carrierWiseSegEntries : carrierWiseSegments.entrySet()) {

				String carrierWiseOND = getONDCode(carrierWiseSegEntries.getValue());

				if (journeyCarrierWiseSectorCodes.get(journeySegEntry.getKey()) == null) {
					journeyCarrierWiseSectorCodes.put(journeySegEntry.getKey(), new HashMap<String, String>());
				}

				journeyCarrierWiseSectorCodes.get(journeySegEntry.getKey()).put(carrierWiseSegEntries.getKey(), carrierWiseOND);

			}
		}

		return journeyCarrierWiseSectorCodes;
	}

	private static Map<Integer, List<FlightSegmentTO>> getInBoundOutBoundSegments(List<FlightSegmentTO> flightSegments) {
		Map<Integer, List<FlightSegmentTO>> journeyWiseSegs = new HashMap<Integer, List<FlightSegmentTO>>();
		for (FlightSegmentTO seg : flightSegments) {
			Integer mapKey = seg.isReturnFlag() ? INBOUND : OUTBOUND;
			if (journeyWiseSegs.get(mapKey) == null) {
				journeyWiseSegs.put(mapKey, new ArrayList<FlightSegmentTO>());
			}
			journeyWiseSegs.get(mapKey).add(seg);
		}
		return journeyWiseSegs;
	}

	private static Map<String, List<FlightSegmentTO>> getCarrierWiseSegments(List<FlightSegmentTO> segments) {
		Map<String, List<FlightSegmentTO>> carrierWiseSegments = new HashMap<String, List<FlightSegmentTO>>();
		for (FlightSegmentTO flightSegmentTO : segments) {
			if (carrierWiseSegments.get(flightSegmentTO.getOperatingAirline()) == null) {
				carrierWiseSegments.put(flightSegmentTO.getOperatingAirline(), new ArrayList<FlightSegmentTO>());
			}
			carrierWiseSegments.get(flightSegmentTO.getOperatingAirline()).add(flightSegmentTO);
		}

		return carrierWiseSegments;
	}

	private static String getONDCode(List<FlightSegmentTO> segments) {
		List<String> ondCodes = new ArrayList<String>();
		Collections.sort(segments, new Comparator<FlightSegmentTO>() {

			@Override
			public int compare(FlightSegmentTO o1, FlightSegmentTO o2) {
				return o1.getSegmentSequence() - o2.getSegmentSequence();
			}
		});

		for (int i = 0; i < segments.size(); i++) {
			FlightSegmentTO seg = segments.get(i);
			String airportCodes[] = seg.getSegmentCode().split(SEGMENT_SEPERATOR);
			if (i == 0) {
				// First segment
				for (int j = 0; j < airportCodes.length; j++) {
					ondCodes.add(airportCodes[j]);
				}
			} else {
				for (int j = 0; j < airportCodes.length; j++) {
					if (j == 0) {
						continue;
					} else {
						ondCodes.add(airportCodes[j]);
					}
				}
			}
		}

		return generateONDFromAirportList(ondCodes);
	}

	private static String generateONDFromAirportList(List<String> airportCodes) {
		String ond = "";
		for (int i = 0; i < airportCodes.size(); i++) {
			ond += i == 0 ? airportCodes.get(i) : SEGMENT_SEPERATOR + airportCodes.get(i);
		}
		return ond;
	}

	public static String toPassengerRph(LCCClientReservationPax lccClientReservationPax) {
		String passengerRph = String.valueOf(lccClientReservationPax.getPaxSequence());
		return passengerRph;
	}

	public static boolean areEqual(Scope scope1, Scope scope2) {
		boolean areEqual = false;

		String scopeType;
		OndScope ondScope1, ondScope2;
		SegmentScope segmentScope1, segmentScope2;
		AirportScope airportScope1, airportScope2;

		if (scope1.getScopeType().equals(scope2.getScopeType())) {
			scopeType = scope1.getScopeType();

			if (scopeType.equals(Scope.ScopeType.ALL_SEGMENTS)) {
				areEqual = true;
			} else if (scopeType.equals(Scope.ScopeType.OND)) {
				ondScope1 = (OndScope) scope1;
				ondScope2 = (OndScope) scope2;

				areEqual = ondScope1.getOndId().equals(ondScope2.getOndId());
			} else if (scopeType.equals(Scope.ScopeType.SEGMENT)) {
				segmentScope1 = (SegmentScope) scope1;
				segmentScope2 = (SegmentScope) scope2;

				areEqual = segmentScope1.getFlightSegmentRPH().equals(segmentScope2.getFlightSegmentRPH());
			} else if (scopeType.equals(Scope.ScopeType.AIRPORT)) {
				airportScope1 = (AirportScope) scope1;
				airportScope2 = (AirportScope) scope2;

				areEqual = airportScope1.getFlightSegmentRPH().equals(airportScope2.getFlightSegmentRPH())
						&& airportScope1.getAirportCode().equals(airportScope2.getAirportCode());
			}
		} else {
			areEqual = false;
		}

		return areEqual;
	}

	public static List<Transition<List<String>>> toModificationTransitions(List<ModifiedFlight> modifications,
			Map<String, String> resSegRphToFlightSegRph) {

		List<Transition<List<String>>> transitions = new ArrayList<>();
		Transition<List<String>> transition;

		for (ModifiedFlight modification : modifications) {
			for (int i = 0; i < modification.getFromResSegRPH().size(); i++) {
				transition = new Transition<>();
				transition.setOld(new ArrayList<>());
				transition.setNew(new ArrayList<>());
				transition.getOld().add(resSegRphToFlightSegRph.get(modification.getFromResSegRPH().get(i)));
				transition.getNew().add(modification.getToFlightSegRPH().get(i));
				transitions.add(transition);
			}

		}

		return transitions;
	}

	public static InventoryWrapper getFirstDepartingSegment(List<InventoryWrapper> inventory) {
		InventoryWrapper firstDepartingSegment = null;

		List<InventoryWrapper> sortedInventory = sort(inventory);
		if (!sortedInventory.isEmpty()) {
			firstDepartingSegment = sortedInventory.iterator().next();
		}

		return firstDepartingSegment;
	}

	public static InventoryWrapper getSegmentForAnciPenalty(List<InventoryWrapper> inventory) {
		InventoryWrapper segmentToPass = null;

		List<InventoryWrapper> sortedInventory = sort(inventory);
		Date now = CalendarUtil.getCurrentSystemTimeInZulu();

		for (InventoryWrapper inventoryRef : sortedInventory) {
			if (now.before(inventoryRef.getFlightSegmentTO().getDepartureDateTimeZulu())) {
				segmentToPass = inventoryRef;
				break;
			}
		}

		if (segmentToPass == null) {
			segmentToPass = sortedInventory.get(0);
		}

		return segmentToPass;
	}

	public static List<InventoryWrapper> sort(List<InventoryWrapper> inventory) {

		List<InventoryWrapper> clonedInventory = new ArrayList<>(inventory);

		clonedInventory.sort(new Comparator<InventoryWrapper>() {
			public int compare(InventoryWrapper o1, InventoryWrapper o2) {
				return o1.getFlightSegmentTO().getDepartureDateTimeZulu()
						.compareTo(o2.getFlightSegmentTO().getDepartureDateTimeZulu());
			}
		});

		return clonedInventory;
	}

	public static int getSegmentsCount(List<FlightSegmentTO> flightSegments, String carrierCode) {
		int segmentsCount = 0;

		for (FlightSegmentTO flightSegment : flightSegments) {
			if (flightSegment.getOperatingAirline().equals(carrierCode)) {
				segmentsCount++;
			}
		}

		return segmentsCount;
	}

	public static void applyAncillaryModifyPenalty(ReservationPaxTO pax, FlightSegmentTO flightSegmentTO) {
		if (pax != null) {
			String flightRefNumber = flightSegmentTO.getFlightRefNumber();

			if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				BigDecimal addAnciTotal = getPaxExtCharges(pax.getExternalCharges());
				BigDecimal removeAnciTotal = pax.getToRemoveAncillaryTotal();
				BigDecimal updateAnciTotal = pax.getToUpdateAncillaryTotal();
				BigDecimal nonRefundableAnciTotal = getNonRefundableAnciTotal(pax);
				BigDecimal paxChgs = AccelAeroCalculator.add(addAnciTotal, updateAnciTotal, removeAnciTotal.negate(),
						nonRefundableAnciTotal);

				if (AccelAeroCalculator.isLessThan(paxChgs, AccelAeroCalculator.getDefaultBigDecimalZero())) {
					BigDecimal anciPenalty = paxChgs.negate();
					LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
					chgDTO.setExternalCharges(EXTERNAL_CHARGES.ANCI_PENALTY);
					chgDTO.setAmount(anciPenalty);
					chgDTO.setFlightRefNumber(flightRefNumber);
					chgDTO.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightRefNumber));
					chgDTO.setAmountConsumedForPayment(true);
					pax.addExternalCharges(chgDTO);
				}
			}

		}
	}

	private static BigDecimal getNonRefundableAnciTotal(ReservationPaxTO pax) {
		BigDecimal total = BigDecimal.ZERO;
		List<LCCSelectedSegmentAncillaryDTO> toRList = pax.getAncillariesToRemove();
		if (toRList != null) {
			for (LCCSelectedSegmentAncillaryDTO segAnci : toRList) {

				if (segAnci.getSpecialServiceRequestDTOs() != null && !isSsrRefundable()) {
					for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
						total = AccelAeroCalculator.add(total, ssr.getCharge());
					}
				}

				/* TODO: implement for other ancilaries */

			}

		}
		return total;
	}

	private static boolean isSsrRefundable() {
		Charge ssrCharge = null;
		boolean isSsrRefundable = true;

		try {
			ssrCharge = ModuleServiceLocator.getChargeBD().getCharge("SSR");
			if (ssrCharge.getRefundableChargeFlag() == 0 && ssrCharge.getRefundableOnlyforModifications().equals("N")) {
				isSsrRefundable = false;
			} else {
				isSsrRefundable = true;
			}
		} catch (ModuleException e) {
			e.printStackTrace();
		}

		return isSsrRefundable;
	}

	public static BigDecimal getPaxExtCharges(List<LCCClientExternalChgDTO> list) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : list) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	public static BigDecimal getAncillaryPassengerTotal(AncillaryPassenger passenger) {
		BigDecimal anciPaxTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (PricedAncillaryType pricedAncillaryType : passenger.getAncillaryTypes()) {
			for (AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()) {
				if (ancillaryScope.getAncillaries() != null && ancillaryScope.getAncillaries().size() > 0) {
					for (PricedSelectedItem pricedSelectedItem : ancillaryScope.getAncillaries()) {
						if (pricedSelectedItem.getQuantity() != null && pricedSelectedItem.getQuantity().SIZE > 1) {
							anciPaxTotal = AccelAeroCalculator.add(anciPaxTotal, AccelAeroCalculator
									.multiply(pricedSelectedItem.getAmount(), pricedSelectedItem.getQuantity()));
						} else {
							anciPaxTotal = AccelAeroCalculator.add(anciPaxTotal, pricedSelectedItem.getAmount());
						}
					}
				}
			}
		}
		return anciPaxTotal;
	}

	public static BigDecimal calculateTaxCharge(BigDecimal effectiveAnciChargeTotal, BigDecimal taxRatio) {
		BigDecimal anciTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (AccelAeroCalculator.isGreaterThan(effectiveAnciChargeTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			anciTax = AccelAeroCalculator.multiplyDefaultScale(effectiveAnciChargeTotal, taxRatio);
		}
		return anciTax;
	}

	public static AncillaryPassenger filterJNTaxRelatedAncillary(RPHGenerator rphGenerator, AncillaryPassenger passenger,
			MetaData metaData, String flightSegRPH) {
		PricedAncillaryType filteredpricedAncillaryType;
		String fltSegCarrierCode;
		AncillaryPassenger ancillaryPassenger = new AncillaryPassenger();
		ancillaryPassenger.setPassengerRph(passenger.getPassengerRph());
		ancillaryPassenger.setAncillaryTypes(new ArrayList<PricedAncillaryType>());

		String taxApplicableCarrier = FlightRefNumberUtil.getOperatingAirline(flightSegRPH);

		for (PricedAncillaryType pricedAncillaryType : passenger.getAncillaryTypes()) {
			filteredpricedAncillaryType = new PricedAncillaryType();
			filteredpricedAncillaryType.setAncillaryType(pricedAncillaryType.getAncillaryType());
			filteredpricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());
			for (AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()) {
				if (ancillaryScope.getScope() instanceof SegmentScope) {
					SegmentScope segmentScope = (SegmentScope) ancillaryScope.getScope();
					fltSegCarrierCode = FlightRefNumberUtil
							.getOperatingAirline(rphGenerator.getUniqueIdentifier(segmentScope.getFlightSegmentRPH()));
					if (taxApplicableCarrier.equals(fltSegCarrierCode)) {
						filteredpricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
					}
				} else if (ancillaryScope.getScope() instanceof AirportScope) {
					AirportScope airportScope = (AirportScope) ancillaryScope.getScope();
					fltSegCarrierCode = FlightRefNumberUtil
							.getOperatingAirline(rphGenerator.getUniqueIdentifier(airportScope.getFlightSegmentRPH()));
					if (taxApplicableCarrier.equals(fltSegCarrierCode)) {
						filteredpricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
					}
				} else if (ancillaryScope.getScope() instanceof OndScope) {
					OndScope ondScope = (OndScope) ancillaryScope.getScope();
					for (OndUnit ondUnit : metaData.getOndPreferences()) {
						if (ondUnit.getOndId().equals(ondScope.getOndId()) && isOndContainsGivenCarrierCode(rphGenerator,
								ondUnit.getFlightSegmentRPH(), taxApplicableCarrier)) {
							filteredpricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
						}
					}
				}
			}
			if (filteredpricedAncillaryType.getAncillaryScopes().size() > 0) {
				ancillaryPassenger.getAncillaryTypes().add(filteredpricedAncillaryType);
			}
		}
		return ancillaryPassenger;
	}

	public static boolean isOndContainsGivenCarrierCode(RPHGenerator rphGenerator, List<String> ondFltSegList,
			String carrierCode) {

		String fltSegCarrierCode;

		for (String fltSegRPH : ondFltSegList) {
			fltSegCarrierCode = FlightRefNumberUtil.getOperatingAirline(rphGenerator.getUniqueIdentifier(fltSegRPH));
			if (carrierCode.equals(fltSegCarrierCode)) {
				return true;
			}
		}

		return false;

	}

	public static MonetaryAmendmentDefinition.AmendmentCategory resolveMonetaryAmendmentCategory(String discountAs) {
		MonetaryAmendmentDefinition.AmendmentCategory amendmentType = null;
		switch (discountAs) {
		case CREDIT:
			amendmentType = MonetaryAmendmentDefinition.AmendmentCategory.CREDIT;
			break;
		case DEBIT:
			amendmentType = MonetaryAmendmentDefinition.AmendmentCategory.DEBIT;
			break;
		case MONEY:
			amendmentType = MonetaryAmendmentDefinition.AmendmentCategory.MONEY;
			break;
		}

		return amendmentType;
	}

	/**
	 * get the segmetn code from flightRedNum by using the method in
	 * FlightRefNumberUtil.getSegmentCodeFromFlightRPH(String flightRPH)
	 * 
	 * @return
	 */
	public static String getSegmentCodeFromFlightRPH(String flightRPH) {
		return FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightRPH);
	}

	/**
	 * filter ancillary with buffer times
	 * 
	 * @return
	 */
	public static List<LCCAncillaryAvailabilityOutDTO> checkBufferTimes(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			boolean isModifyOperation) throws ModuleException {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				// FIXME this should be changed to the zulu time and checking
				// should be done against zulu tooo...
				Date departureDate = FlightRefNumberUtil
						.getDepartureDateFromFlightRPH(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber());
				String segmentCode = FlightRefNumberUtil
						.getSegmentCodeFromFlightRPH(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber());
				boolean isDomesticFlight = ancillaryAvailabilityOutDTO.getFlightSegmentTO().isDomesticFlight();
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					ancillaryStatus.setAvailable(ancillaryStatus.isAvailable()
							&& isEnableService(ancillaryStatus.getAncillaryType().getAncillaryType(), departureDate, segmentCode,
									isModifyOperation, isDomesticFlight));

				}
			}
		}
		return availabilityOutDTO;
	}

	private static boolean isEnableService(String anciType, Date departDate, String segmentCode, boolean isModifyOperation,
			boolean isDomesticFlight) throws ModuleException {
		String airportCode = null;
		
		if (anciType.equals(LCCAncillaryType.AUTOMATIC_CHECKIN)) {
			// Stop cut over time has been checked already for automatic check-in
			return true;
		}
		
		if (segmentCode != null && segmentCode.length() > 0) {
			airportCode = segmentCode.split("/")[0];
		}

		long diffMils = 0;
		Calendar currentTime = Calendar.getInstance();
		currentTime.setTime(getZuluTime());
		if (airportCode != null) {
			departDate = getZuluDateTime(departDate, airportCode);
		}

		if (anciType.equals(LCCAncillaryType.SEAT_MAP)) {
			diffMils = AppSysParamsUtil.getIBESeatmapStopCutoverInMillis();
		} else if (anciType.equals(LCCAncillaryType.MEALS)) {
			diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
		} else if (anciType.equals(LCCAncillaryType.BAGGAGE)) {
			diffMils = BaggageTimeWatcher.getBaggageCutOverTimeForIBEInMillis(isModifyOperation);
		} else if (anciType.equals(LCCAncillaryType.SSR)) {
			if (AppSysParamsUtil.isMaintainSSRWiseCutoffTime()) {
				return true;
			}
			diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();
		} else if (anciType.equals(LCCAncillaryType.INSURANCE)) {
			diffMils = 0;
		} else if (anciType.equals(LCCAncillaryType.AIRPORT_TRANSFER)) {
			if (isDomesticFlight) {
				diffMils = AppSysParamsUtil.getAirportTransferStopCutoverForDomesticInMillis();
			} else {
				diffMils = AppSysParamsUtil.getAirportTransferStopCutoverForInternationalInMillis();
			}
		}

		if (departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
			return false;
		} else {
			return true;
		}
	}

	private static Date getZuluTime() {
		Calendar c = Calendar.getInstance();
		TimeZone z = c.getTimeZone();
		c.add(Calendar.MILLISECOND, -(z.getRawOffset() + z.getDSTSavings()));
		return c.getTime();
	}

	private static Date getZuluDateTime(Date localDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportBD());

		return helper.getZuluDateTime(airportCode, localDate);
	}

	public static boolean isModifyOperation(TransactionalAncillaryService transactionalAncillaryService, String transactionId) {
		String pnr;
		if (transactionId != null && transactionalAncillaryService.getReservationInfo(transactionId) != null) {
			pnr = transactionalAncillaryService.getReservationInfo(transactionId).getPnr();
			if (pnr == null || pnr.equals("")) {
				return false;
			}
			return true;
		}
		return false;
	}

	public static Map<String, Integer> getRowSeatPassengerMap(List<LCCPassengerTypeQuantityDTO> passengerTypeQuantityList) {
		Map<String, Integer> rowSeatCountMap = new HashMap<String, Integer>();
		if (passengerTypeQuantityList != null && !passengerTypeQuantityList.isEmpty()) {
			for (LCCPassengerTypeQuantityDTO lccPassengerTypeQuantityDTO : passengerTypeQuantityList) {
				rowSeatCountMap.put(lccPassengerTypeQuantityDTO.getPaxType(), lccPassengerTypeQuantityDTO.getQuantity());
			}
		}

		return rowSeatCountMap;
	}

	public static String resolvePaxType(String paxType, boolean isParent) {
		String passengerType = "";
		if (paxType.equals("AD") && isParent) {
			passengerType = "IN";
		} else if (paxType.equals("AD")) {
			passengerType = "AD";
		} else if (paxType.equals("CH")) {
			passengerType = "CH";
		}
		return passengerType;
	}

	public static boolean isParent(Passenger passenger, List<Passenger> passengers) {
		if (passenger.getType().equals("AD")) {
			for (Passenger passengerRef : passengers) {
				if (passengerRef.getType().equals("IN")
						&& passengerRef.getTravelWith() == Integer.parseInt(passenger.getPassengerRph())) {
					return true;
				}
			}
		}
		return false;
	}

	public static Integer getInfantWith(Passenger passenger, List<Passenger> passengers) {
		if (passenger.getType().equals("IN")) {
			for (Passenger passengerRef : passengers) {
				if (passengerRef.getType().equals("AD")
						&& passenger.getTravelWith() == Integer.parseInt(passengerRef.getPassengerRph())) {
					return Integer.parseInt(passengerRef.getPassengerRph());
				}
			}
		}
		return null;
	}

	public static List<com.isa.aeromart.services.endpoint.dto.common.Passenger>
			sessionPassengerToCommomPassenger(List<SessionPassenger> sessionPassengerList) {

		com.isa.aeromart.services.endpoint.dto.common.Passenger passenger;
		List<com.isa.aeromart.services.endpoint.dto.common.Passenger> commonPassengerList = new ArrayList<com.isa.aeromart.services.endpoint.dto.common.Passenger>();

		for (SessionPassenger sessionPassenger : sessionPassengerList) {
			passenger = new com.isa.aeromart.services.endpoint.dto.common.Passenger();
			passenger.setPaxType(sessionPassenger.getType());
			passenger.setPaxSequence(Integer.parseInt(sessionPassenger.getPassengerRph()));
			passenger.setFirstName(sessionPassenger.getFirstName());
			passenger.setLastName(sessionPassenger.getLastName());
			commonPassengerList.add(passenger);
		}

		return commonPassengerList;
	}

	public static DiscountRQ createDiscountRQ(DiscountedFareDetails discountedFareDetails, TravellerQuantity travellerQuantity,
			String transactionIdentifier, FareSegChargeTO fareSegChargeTO, PreferenceInfo preferenceInfo) {

		DiscountRQ discountRQ = null;
		IPaxCountAssembler paxAssm;
		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();
		QuotedFareRebuildDTO quotedFareRebuildDTO;

		List<PassengerTypeQuantityTO> paxQtyList = new ArrayList<PassengerTypeQuantityTO>();

		PassengerTypeQuantityTO adultPassenger = new PassengerTypeQuantityTO();
		adultPassenger.setPassengerType(PaxTypeTO.ADULT);
		adultPassenger.setQuantity(travellerQuantity.getAdultCount());
		paxQtyList.add(adultPassenger);

		PassengerTypeQuantityTO childPassenger = new PassengerTypeQuantityTO();
		childPassenger.setPassengerType(PaxTypeTO.CHILD);
		childPassenger.setQuantity(travellerQuantity.getChildCount());
		paxQtyList.add(childPassenger);

		PassengerTypeQuantityTO infantPassenger = new PassengerTypeQuantityTO();
		infantPassenger.setPassengerType(PaxTypeTO.INFANT);
		infantPassenger.setQuantity(travellerQuantity.getInfantCount());
		paxQtyList.add(infantPassenger);

		paxAssm = new PaxCountAssembler(travellerQuantity.getAdultCount(), travellerQuantity.getChildCount(),
				travellerQuantity.getInfantCount());

		DISCOUNT_METHOD discountMethod = null;
		if (discountedFareDetails.getPromotionId() == null) {
			if (discountedFareDetails.getDomesticSegmentCodeList().size() > 0
					&& AppSysParamsUtil.isDomesticFareDiscountEnabled()) {
				discountMethod = DISCOUNT_METHOD.DOM_FARE_DISCOUNT;

			} else if (discountedFareDetails.getFarePercentage() > 0) {
				discountMethod = DISCOUNT_METHOD.FARE_DISCOUNT;

			}
		} else {
			discountMethod = DISCOUNT_METHOD.PROMOTION;
		}
		fareSegChargeTOs.add(fareSegChargeTO);

		quotedFareRebuildDTO = new QuotedFareRebuildDTO(fareSegChargeTOs, paxAssm, discountedFareDetails,
				preferenceInfo.getSeatType());

		discountRQ = new DiscountRQ(discountMethod);
		discountRQ.setCalculateTotalOnly(true);
		discountRQ.setPaxQtyList(paxQtyList);
		discountRQ.setQuotedFareRebuildDTO(quotedFareRebuildDTO);
		discountRQ.setDiscountInfoDTO(discountedFareDetails);
		discountRQ.setPaxChargesList(null);
		discountRQ.setSystem(ProxyConstants.SYSTEM.getEnum(preferenceInfo.getSelectedSystem()));
		discountRQ.setTransactionIdentifier(transactionIdentifier);

		return discountRQ;
	}

	public static List<String> getModifiedFltSegRefList(RPHGenerator rphGenerator, List<ModifiedFlight> modifiedFlights,
			Map<String, String> resSegRphToFlightSegMap) {
		List<String> modifiedFltSegRefList = new ArrayList<String>();

		for (ModifiedFlight modifiedFlight : modifiedFlights) {
			for (String resSegRPH : modifiedFlight.getFromResSegRPH()) {
				modifiedFltSegRefList.add(rphGenerator.getUniqueIdentifier(resSegRphToFlightSegMap.get(resSegRPH)));
			}
		}

		return modifiedFltSegRefList;
	}

	public static Map<String, Integer> extractAiportServiceCount(Collection<LCCClientReservationPax> colpaxs) {
		Map<String, Integer> apServiceCount = new HashMap<String, Integer>();

		for (LCCClientReservationPax pax : colpaxs) {
			List<LCCSelectedSegmentAncillaryDTO> anciList = pax.getSelectedAncillaries();
			for (LCCSelectedSegmentAncillaryDTO anciDTO : anciList) {
				if (anciDTO != null && anciDTO.getAirportServiceDTOs() != null && anciDTO.getAirportServiceDTOs().size() > 0) {
					String fltRef = anciDTO.getFlightSegmentTO().getFlightRefNumber();
					List<LCCAirportServiceDTO> apServiceList = anciDTO.getAirportServiceDTOs();

					for (LCCAirportServiceDTO apService : apServiceList) {
						if (apServiceCount.get(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode()) == null) {
							apServiceCount.put(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode(), 1);
						} else {
							int c = apServiceCount.get(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode());
							apServiceCount.put(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode(), ++c);
						}
					}

				}
			}
		}

		return apServiceCount;
	}

	public static boolean isAncillaryEditableForModificationIBE(LCCClientReservation reservation, String anciType,
			boolean isAvailable, boolean isSelected, boolean isRegUser) {

		boolean anciEditable = false;

		if (reservation != null && reservation.isGroupPNR()) {
			switch (anciType) {

			case AncillariesConstants.Type.INSURANCE:
				if (isSelected) {
					anciEditable = false;
				} else {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.ADD_INSURANCE);
				}
				break;
			case AncillariesConstants.Type.AIRPORT_TRANSFER:
				if (isSelected) {
					anciEditable = reservation.getInterlineModificationParams()
							.contains(ModifcationParamTypes.EDIT_AIRPORT_SERVICE);
				} else {
					anciEditable = reservation.getInterlineModificationParams()
							.contains(ModifcationParamTypes.ADD_AIRPORT_TRANSFER);
				}
				break;
			case AncillariesConstants.Type.BAGGAGE:
				if (isSelected) {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.EDIT_BAGGAGE);
				} else {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.ADD_BAGGAGE);
				}
				break;
			case AncillariesConstants.Type.MEAL:
				if (isSelected) {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.EDIT_MEAL);
				} else {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.ADD_MEAL);
				}
				break;
			case AncillariesConstants.Type.SEAT:
				if (isSelected) {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.EDIT_SEAT);
				} else {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.ADD_SEAT);
				}
				break;
			case AncillariesConstants.Type.SSR_AIRPORT:
				if (isSelected) {
					anciEditable = reservation.getInterlineModificationParams()
							.contains(ModifcationParamTypes.EDIT_AIRPORT_SERVICE);
				} else {
					anciEditable = reservation.getInterlineModificationParams()
							.contains(ModifcationParamTypes.ADD_AIRPORT_SERVICE);
				}
				break;
			case AncillariesConstants.Type.SSR_IN_FLIGHT:
				if (isSelected) {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.EDIT_SSR);
				} else {
					anciEditable = reservation.getInterlineModificationParams().contains(ModifcationParamTypes.ADD_SSR);
				}
				break;
			}

		} else {
			boolean editServiceByUser = false;

			// since no usage of IBE_25 app param this code is unused
			// if (isRegUser && isSelected && AppSysParamsUtil.isRegisterdUserCanEditServices()) {
			// editServiceByUser = true;
			// } else if (isRegUser && !isSelected && AppSysParamsUtil.isRegisterdUserCanAddServices()) {
			// editServiceByUser = true;
			// } else if (!isRegUser && isSelected && AppSysParamsUtil.isUnregisterdUserCanEditServices()) {
			// editServiceByUser = true;
			// } else if (!isRegUser && !isSelected && AppSysParamsUtil.isUnregisterdUserCanAddServices()) {
			// editServiceByUser = true;
			// }

			if (isRegUser && AppSysParamsUtil.isRegisterdUserCanAddServices()) {
				editServiceByUser = true;
			} else if (!isRegUser && AppSysParamsUtil.isUnregisterdUserCanAddServices()) {
				editServiceByUser = true;
			}

			switch (anciType) {

			case AncillariesConstants.Type.INSURANCE:
				if (isSelected) {
					anciEditable = false;
				} else if (!isSelected && AppSysParamsUtil.isIBEAddInsurance()) {
					anciEditable = editServiceByUser;
				}
				break;
			case AncillariesConstants.Type.AIRPORT_TRANSFER:
				if ((isSelected && AppSysParamsUtil.isAllowEditAirportTransfers())) {
					anciEditable = editServiceByUser;
				}if((!isSelected && AppSysParamsUtil.isIBEAddAirportTransfers())){
					anciEditable = true;
					//TODO further improvements need to be done.
				}
				break;
			case AncillariesConstants.Type.BAGGAGE:
				if ((isSelected && AppSysParamsUtil.isIBEEditBaggage()) || (!isSelected && AppSysParamsUtil.isIBEAddBaggage())) {
					anciEditable = editServiceByUser;
				}
				break;
			case AncillariesConstants.Type.MEAL:
				if ((isSelected && AppSysParamsUtil.isIBEEditMeal()) || (!isSelected && AppSysParamsUtil.isIBEAddMeal())) {
					anciEditable = editServiceByUser;
				}
				break;
			case AncillariesConstants.Type.SEAT:
				if ((isSelected && AppSysParamsUtil.isIBEEditSeat()) || (!isSelected && AppSysParamsUtil.isIBEAddSeat())) {
					anciEditable = editServiceByUser;
				}
				break;
			case AncillariesConstants.Type.AUTOMATIC_CHECKIN:
				if (isSelected && AppSysParamsUtil.isAutomaticCheckinEnabled()) {
					anciEditable = false;
				} else if (!isSelected && AppSysParamsUtil.isAutomaticCheckinEnabled()) {
					anciEditable = editServiceByUser;
				}
				break;
			case AncillariesConstants.Type.SSR_AIRPORT:
				if ((isSelected && AppSysParamsUtil.isIBEEditAirportServices())
						|| (!isSelected && AppSysParamsUtil.isIBEAddAirportServices())) {
					anciEditable = editServiceByUser;
				}
				break;
			case AncillariesConstants.Type.SSR_IN_FLIGHT:
				if ((isSelected && AppSysParamsUtil.isIBEEditSSR()) || (!isSelected && AppSysParamsUtil.isIBEAddSSR())) {
					anciEditable = editServiceByUser;
				}
				break;
			}
		}
		return anciEditable && isAvailable;

	}

	public static List<FlightSegmentTO> updateOndSequence(List<LCCClientCarrierOndGroup> ondGroups,
			List<FlightSegmentTO> flightSegmentTOs) {
		Collections.sort(flightSegmentTOs);
		if (ondGroups != null && !ondGroups.isEmpty()) {
			ondGroups = filterConfirmedOndGroups(ondGroups);
			ondGroups = SortUtil.sortByDepartureDateTime(ondGroups);

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (flightSegmentTO.getPnrSegId() != null && !"".equals(flightSegmentTO.getPnrSegId())) {
					int pnrSegId = Integer.parseInt(flightSegmentTO.getPnrSegId());
					int ondSequence = 0;
					for (LCCClientCarrierOndGroup ondGroup : ondGroups) {
						Collection<Integer> ondPnrSegIds = ondGroup.getCarrierPnrSegIds();
						if (ondPnrSegIds != null && ondPnrSegIds.contains(pnrSegId)
								&& WebplatformUtil.nullHandler(ondGroup.getCarrierCode())
										.equals(WebplatformUtil.nullHandler(flightSegmentTO.getOperatingAirline()))) {
							flightSegmentTO.setOndSequence(ondSequence);
							break;
						}
						ondSequence++;
					}
				}
			}
		}

		return flightSegmentTOs;
	}

	private static List<LCCClientCarrierOndGroup> filterConfirmedOndGroups(List<LCCClientCarrierOndGroup> allOndGroups) {
		List<LCCClientCarrierOndGroup> ondGroups = new ArrayList<LCCClientCarrierOndGroup>();

		for (LCCClientCarrierOndGroup lccClientCarrierOndGroup : allOndGroups) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(lccClientCarrierOndGroup.getStatus())) {
				ondGroups.add(lccClientCarrierOndGroup);
			}
		}

		return ondGroups;
	}

	// TODO when meal count is zero even passengers already selected the meals, DB will not pick the meal segments, this
	// is
	// a temporary function used to add missing meal segment to priceQuote in the ancillary
	public static void updatePriceQuoteForUnavailableMealSegments(AncillarySelection ancillarySelection,
			PriceQuoteAncillaryRS priceQuoteAncillariesResponse, ReservationAncillarySelection baseAncillarySelectionsResponse) {
		List<SegmentScope> existingScopeList = new ArrayList<>();
		for (SelectedAncillary selectedAncillary : ancillarySelection.getAncillaryPreferences()) {
			if (selectedAncillary.getType().equals(AncillariesConstants.Type.MEAL)) {
				for (Preference preference : selectedAncillary.getPreferences()) {
					if (preference.getAssignee() instanceof PassengerAssignee) {
						PassengerAssignee passengerAssignee = (PassengerAssignee) preference.getAssignee();
						for (AncillaryPassenger ancillaryPassenger : priceQuoteAncillariesResponse.getAncillaryReservation()
								.getPassengers()) {
							for (PricedAncillaryType pricedAncillaryType : ancillaryPassenger.getAncillaryTypes()) {
								if (pricedAncillaryType.getAncillaryType().equals(AncillariesConstants.Type.MEAL)
										&& passengerAssignee.getPassengerRph().equals(ancillaryPassenger.getPassengerRph())) {
									for (Selection selection : preference.getSelections()) {
										for (AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()) {
											if (ancillaryScope.getScope().equals(selection.getScope())) {
												existingScopeList.add((SegmentScope) ancillaryScope.getScope());
											}
										}
									}
									for (Selection selection : preference.getSelections()) {
										if (!existingScopeList.contains(selection.getScope())) {
											for (AncillaryPassenger resAncillaryPassenger : baseAncillarySelectionsResponse
													.getAncillaryReservation().getPassengers()) {
												if (resAncillaryPassenger.getPassengerRph()
														.equals(passengerAssignee.getPassengerRph())) {
													for (PricedAncillaryType resPricedAncillaryType : resAncillaryPassenger
															.getAncillaryTypes()) {
														if (resPricedAncillaryType.getAncillaryType()
																.equals(AncillariesConstants.Type.MEAL)) {
															for (AncillaryScope resAncillaryScope : resPricedAncillaryType
																	.getAncillaryScopes()) {
																if (!existingScopeList.contains(resAncillaryScope.getScope())) {
																	pricedAncillaryType.getAncillaryScopes()
																			.add(resAncillaryScope);
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

}
