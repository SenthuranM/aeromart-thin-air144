package com.isa.aeromart.services.endpoint.dto.masterdata;

public class PaxCutOffYears {

	private int adultAgeCutOverYears;
	private int childAgeCutOverYears;
	private int infantAgeCutOverYears;

	public int getAdultAgeCutOverYears() {
		return adultAgeCutOverYears;
	}

	public void setAdultAgeCutOverYears(int adultAgeCutOverYears) {
		this.adultAgeCutOverYears = adultAgeCutOverYears;
	}

	public int getChildAgeCutOverYears() {
		return childAgeCutOverYears;
	}

	public void setChildAgeCutOverYears(int childAgeCutOverYears) {
		this.childAgeCutOverYears = childAgeCutOverYears;
	}

	public int getInfantAgeCutOverYears() {
		return infantAgeCutOverYears;
	}

	public void setInfantAgeCutOverYears(int infantAgeCutOverYears) {
		this.infantAgeCutOverYears = infantAgeCutOverYears;
	}
	
}
