package com.isa.aeromart.services.endpoint.dto.common;

public class AdditionalInfo {

	private String ffid;

	private String nic;

	private FoidInfo foidInfo;

	private DocInfo docInfo;

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public FoidInfo getFoidInfo() {
		return foidInfo;
	}

	public void setFoidInfo(FoidInfo foidInfo) {
		this.foidInfo = foidInfo;
	}

	public DocInfo getDocInfo() {
		return docInfo;
	}

	public void setDocInfo(DocInfo visaInfo) {
		this.docInfo = visaInfo;
	}

}
