package com.isa.aeromart.services.endpoint.dto.ancillary.assignee;

import com.isa.aeromart.services.endpoint.dto.ancillary.Assignee;

public class PassengerAssignee extends Assignee {

    private String passengerRph;

    public String getPassengerRph() {
        return passengerRph;
    }

    public void setPassengerRph(String passengerRph) {
        this.passengerRph = passengerRph;
    }
}
