package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class FlightCalendarRS extends TransactionalBaseRS {

	private List<OriginDestinationRS> originDestinationResponse;

	public FlightCalendarRS() {
	}

	public FlightCalendarRS(FlightCalendarRS flightCalendarResponse) {
		setOriginDestinationResponse(flightCalendarResponse.getOriginDestinationResponse());
		setTransactionId(flightCalendarResponse.getTransactionId());
	}

	public FlightCalendarRS(FareQuoteRS flightCalendarResponse) {
		setOriginDestinationResponse(flightCalendarResponse.getOriginDestinationResponse());
		setTransactionId(flightCalendarResponse.getTransactionId());
	}

	public List<OriginDestinationRS> getOriginDestinationResponse() {
		if (originDestinationResponse == null) {
			originDestinationResponse = new ArrayList<OriginDestinationRS>();
		}
		return originDestinationResponse;
	}

	public void setOriginDestinationResponse(List<OriginDestinationRS> originDestinationResponse) {
		this.originDestinationResponse = originDestinationResponse;
	}

}
