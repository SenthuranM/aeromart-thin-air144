package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AirportTransferInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AutoCheckinInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.InFlightSsrInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRS;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class PriceQuoteAncillaryAdaptor implements Adaptor<LCCClientReservation, LoadReservationRS> {
	
	private static Log log = LogFactory.getLog(PriceQuoteAncillaryAdaptor.class);

	private Map<AncillariesConstants.AncillaryType, PricedAncillaryType> anciTypeMap;

	private Map<Scope, AncillaryScope> scopeWiseAnciMap;

	private Map<Scope, AncillaryScope> ondWiseAnciMap;
	
	private Map<String , OndUnit> ondIdMap;
	
	private Map<String , LCCInsuranceQuotationDTO> reservationInsuranceMap;

	private LoadReservationRS loadReservationRS;
	
	private String fltRefNumber;
	
	private String airportCode;

	private RPHGenerator rphGenerator;

	public PriceQuoteAncillaryAdaptor(LoadReservationRS loadReservationRS, RPHGenerator rphGenerator) {
		this.loadReservationRS = loadReservationRS;
		this.rphGenerator = rphGenerator;
	}

	@Override
	public LoadReservationRS adapt(LCCClientReservation source) {
		int i = 0;
		List<OndUnit> ondPreferences = new ArrayList<OndUnit>();
		ondIdMap = new HashMap<String, OndUnit>();
		reservationInsuranceMap = new HashMap<String, LCCInsuranceQuotationDTO>();
		PricedAncillaryType reservationPricedAncillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		String ondBaggageGroupId;
		AncillariesConstants.AncillaryType anciType;
		String point;
		
		for (LCCClientReservationPax lccClientReservationPax : source.getPassengers()) {
			PricedAncillaryType pricedAncillaryType;
			
			anciTypeMap = new HashMap<AncillariesConstants.AncillaryType, PricedAncillaryType>();
			ondWiseAnciMap = new HashMap<Scope, AncillaryScope>();
			loadReservationRS.getPassengers().get(i)
			.setPassengerWiseSelectedAncillaries(new ArrayList<PricedAncillaryType>());

			for (LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO : lccClientReservationPax.getSelectedAncillaries()) {
				fltRefNumber = rphGenerator.getRPH(lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber());
				ondBaggageGroupId = getBaggageONDGroupId(lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber(),
						source.getSegments());
				if (lccSelectedSegmentAncillaryDTO.getAirportServiceDTOs() != null
						&& lccSelectedSegmentAncillaryDTO.getAirportServiceDTOs().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.SSR_AIRPORT;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					scopeWiseAnciMap = new HashMap<Scope, AncillaryScope>();

					for (LCCAirportServiceDTO airportServiceDTO : lccSelectedSegmentAncillaryDTO.getAirportServiceDTOs()) {

						AirportScope airportScope = new AirportScope();
						airportScope.setScopeType(ScopeType.AIRPORT); // check scope type exists
						airportScope.setFlightSegmentRPH(fltRefNumber);
						airportScope.setAirportCode(airportServiceDTO.getAirportCode());

						point = null;
						if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode() != null) {

							if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode().startsWith(airportServiceDTO.getAirportCode())) {
								point = AirportScope.Point.DEPARTURE.name();
							} else if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode().endsWith(
									airportServiceDTO.getAirportCode())) {
								point = AirportScope.Point.ARRIVAL.name();
							} else if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode().contains(
									airportServiceDTO.getAirportCode())) {
								point = AirportScope.Point.TRANSIT.name();
							}
						}

						airportScope.setPoint(point);
						ancillaryScope = updateScopeWiseAnciMap(airportScope);

						pricedSelectedItem = new PricedSelectedItem();
						pricedSelectedItem.setId(airportServiceDTO.getSsrCode());
						pricedSelectedItem.setName(airportServiceDTO.getSsrName());
						pricedSelectedItem.setQuantity(1);
						pricedSelectedItem.setAmount(airportServiceDTO.getServiceCharge());
						ancillaryScope.getAncillaries().add(pricedSelectedItem);
						scopeWiseAnciMap.put(airportScope, ancillaryScope);

					}
					for (AncillaryScope AncillaryScope : scopeWiseAnciMap.values()) {
						pricedAncillaryType.getAncillaryScopes().add(AncillaryScope);
					}
					anciTypeMap.put(anciType, pricedAncillaryType);
				}

				if (lccSelectedSegmentAncillaryDTO.getMealDTOs() != null
						&& lccSelectedSegmentAncillaryDTO.getMealDTOs().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.MEAL;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					ancillaryScope = new AncillaryScope();
					SegmentScope segmentScope = new SegmentScope();
					segmentScope.setScopeType(ScopeType.SEGMENT);
					segmentScope.setFlightSegmentRPH(fltRefNumber);
					ancillaryScope.setScope(segmentScope);
					ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());

					for (LCCMealDTO meal : lccSelectedSegmentAncillaryDTO.getMealDTOs()) {

						pricedSelectedItem = new PricedSelectedItem();
						pricedSelectedItem.setId(meal.getMealCode());
						pricedSelectedItem.setName(meal.getMealName());
						pricedSelectedItem.setAmount(meal.getMealCharge());
						pricedSelectedItem.setQuantity(meal.getSoldMeals());
						pricedSelectedItem.setMealCategoryCode(meal.getMealCategoryCode());
						ancillaryScope.getAncillaries().add(pricedSelectedItem);
					}

					pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
					anciTypeMap.put(anciType, pricedAncillaryType);
				}

				if (lccSelectedSegmentAncillaryDTO.getAirSeatDTO() != null
						&& lccSelectedSegmentAncillaryDTO.getAirSeatDTO().getSeatNumber() != null) {
					anciType = AncillariesConstants.AncillaryType.SEAT;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					ancillaryScope = new AncillaryScope();
					LCCAirSeatDTO seat = lccSelectedSegmentAncillaryDTO.getAirSeatDTO();
					SegmentScope segmentScope = new SegmentScope();
					segmentScope.setScopeType(ScopeType.SEGMENT);
					segmentScope.setFlightSegmentRPH(fltRefNumber);
					ancillaryScope.setScope(segmentScope);
					ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());

					pricedSelectedItem = new PricedSelectedItem();
					pricedSelectedItem.setId(seat.getSeatNumber());
					pricedSelectedItem.setAmount(seat.getSeatCharge());
					pricedSelectedItem.setQuantity(1);// TODO
					ancillaryScope.getAncillaries().add(pricedSelectedItem);
					pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
					anciTypeMap.put(anciType, pricedAncillaryType);
				}

				if (lccSelectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs() != null
						&& lccSelectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.SSR_IN_FLIGHT;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					ancillaryScope = new AncillaryScope();
					SegmentScope segmentScope = new SegmentScope();
					segmentScope.setScopeType(ScopeType.SEGMENT);
					segmentScope.setFlightSegmentRPH(fltRefNumber);
					ancillaryScope.setScope(segmentScope);
					ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());

					for (LCCSpecialServiceRequestDTO inflightService : lccSelectedSegmentAncillaryDTO
							.getSpecialServiceRequestDTOs()) {

						pricedSelectedItem = new PricedSelectedItem();
						pricedSelectedItem.setId(inflightService.getSsrCode());
						pricedSelectedItem.setName(inflightService.getSsrName());
						pricedSelectedItem.setAmount(inflightService.getCharge());
						pricedSelectedItem.setQuantity(1); // TODO
						InFlightSsrInput input = new InFlightSsrInput();
						input.setComment(inflightService.getText());
						pricedSelectedItem.setInput(input);
						ancillaryScope.getAncillaries().add(pricedSelectedItem);
					}

					pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
					anciTypeMap.put(anciType, pricedAncillaryType);
				}

				if (lccSelectedSegmentAncillaryDTO.getExtraSeatDTOs() != null
						&& lccSelectedSegmentAncillaryDTO.getExtraSeatDTOs().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.SEAT;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					ancillaryScope = new AncillaryScope();
					SegmentScope segmentScope = new SegmentScope();
					segmentScope.setScopeType(ScopeType.SEGMENT);
					segmentScope.setFlightSegmentRPH(fltRefNumber);
					ancillaryScope.setScope(segmentScope);
					ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());

					for (LCCAirSeatDTO seat : lccSelectedSegmentAncillaryDTO.getExtraSeatDTOs()) {

						pricedSelectedItem = new PricedSelectedItem();
						pricedSelectedItem.setId(seat.getSeatNumber());
						pricedSelectedItem.setAmount(seat.getSeatCharge());
						pricedSelectedItem.setQuantity(1);// TODO
						ancillaryScope.getAncillaries().add(pricedSelectedItem);
					}

					pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
					anciTypeMap.put(anciType, pricedAncillaryType);
				}

				if (lccSelectedSegmentAncillaryDTO.getBaggageDTOs() != null
						&& lccSelectedSegmentAncillaryDTO.getBaggageDTOs().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.BAGGAGE;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					for (LCCBaggageDTO baggage : lccSelectedSegmentAncillaryDTO.getBaggageDTOs()) {
						updateMetaData(ondBaggageGroupId, fltRefNumber);
						OndScope ondScope = new OndScope();
						ondScope.setScopeType(ScopeType.OND);
						ondScope.setOndId(ondBaggageGroupId); // TODO resolve ondId by meta data
						ancillaryScope = updateONDWiseAnciMap(ondScope);

						pricedSelectedItem = new PricedSelectedItem();
						pricedSelectedItem.setId(baggage.getOndBaggageChargeId() +"|"+ baggage.getBaggageName() +"|");
						pricedSelectedItem.setName(baggage.getBaggageName());
						pricedSelectedItem.setAmount(baggage.getBaggageCharge());
						pricedSelectedItem.setQuantity(1);// TODO
						
						if(!checkPriceItemExists(pricedSelectedItem, ancillaryScope.getAncillaries())){
							ancillaryScope.getAncillaries().add(pricedSelectedItem);
							ondWiseAnciMap.put(ondScope, ancillaryScope);
						}
					}

					for (AncillaryScope ancillaryScopeRef : ondWiseAnciMap.values()) {
						if(!ancillaryScopeRef.getAncillaries().isEmpty() && !checkONDExists(ancillaryScopeRef, pricedAncillaryType.getAncillaryScopes())){
							pricedAncillaryType.getAncillaryScopes().add(ancillaryScopeRef);
						}
					}
					anciTypeMap.put(anciType, pricedAncillaryType);
				}
				
				if (lccSelectedSegmentAncillaryDTO.getAutomaticCheckinDTOs() != null
						&& lccSelectedSegmentAncillaryDTO.getAutomaticCheckinDTOs().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					ancillaryScope = new AncillaryScope();
					SegmentScope segmentScope = new SegmentScope();
					segmentScope.setScopeType(ScopeType.SEGMENT);
					segmentScope.setFlightSegmentRPH(fltRefNumber);
					ancillaryScope.setScope(segmentScope);
					ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());

					for (LCCAutomaticCheckinDTO autoCheckin : lccSelectedSegmentAncillaryDTO.getAutomaticCheckinDTOs()) {

						pricedSelectedItem = new PricedSelectedItem();
						pricedSelectedItem.setId(autoCheckin.getAutoCheckinId().toString());
						pricedSelectedItem.setName(autoCheckin.getSeatPref());
						pricedSelectedItem.setAmount(autoCheckin.getAutomaticCheckinCharge());
						pricedSelectedItem.setQuantity(1);
						AutoCheckinInput input = new AutoCheckinInput();
						input.setEmail(autoCheckin.getEmail());
						input.setSeatCode(autoCheckin.getSeatCode());
						pricedSelectedItem.setInput(input);
						ancillaryScope.getAncillaries().add(pricedSelectedItem);
					}

					pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
					anciTypeMap.put(anciType, pricedAncillaryType);
				}

				if (lccSelectedSegmentAncillaryDTO.getInsuranceQuotations() != null
						&& lccSelectedSegmentAncillaryDTO.getInsuranceQuotations().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.INSURANCE;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					for (LCCInsuranceQuotationDTO insurance : lccSelectedSegmentAncillaryDTO.getInsuranceQuotations()) {
						updateReservationInsurance(insurance);
					}
				}
				
				if (lccSelectedSegmentAncillaryDTO.getAirportTransferDTOs() != null
						&& lccSelectedSegmentAncillaryDTO.getAirportTransferDTOs().size() > 0) {
					anciType = AncillariesConstants.AncillaryType.AIRPORT_TRANSFER;
					pricedAncillaryType = updateAnciTypeMap(anciType);

					scopeWiseAnciMap = new HashMap<Scope, AncillaryScope>();

					for (LCCAirportServiceDTO airportServiceDTO : lccSelectedSegmentAncillaryDTO.getAirportTransferDTOs()) {

						AirportScope airportScope = new AirportScope();
						airportScope.setScopeType(ScopeType.AIRPORT); // check scope type exists
						airportScope.setFlightSegmentRPH(fltRefNumber);
						airportScope.setAirportCode(airportServiceDTO.getAirportCode());

						point = null;
						if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode() != null) {

							if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode().startsWith(airportServiceDTO.getAirportCode())) {
								point = AirportScope.Point.DEPARTURE.name();
							} else if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode().endsWith(
									airportServiceDTO.getAirportCode())) {
								point = AirportScope.Point.ARRIVAL.name();
							} else if (lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode().contains(
									airportServiceDTO.getAirportCode())) {
								point = AirportScope.Point.TRANSIT.name();
							}
						}

						airportScope.setPoint(point);
						ancillaryScope = updateScopeWiseAnciMap(airportScope);

						pricedSelectedItem = new PricedSelectedItem();
						pricedSelectedItem.setId(airportServiceDTO.getAirportTransferId().toString());
						pricedSelectedItem.setName(airportServiceDTO.getSsrName());
						pricedSelectedItem.setQuantity(1);
						pricedSelectedItem.setAmount(airportServiceDTO.getServiceCharge());
						AirportTransferInput aptInput = new  AirportTransferInput();
						try {
							aptInput.setTranferDate(CalendarUtil.getParsedTime(airportServiceDTO.getTranferDate(), CalendarUtil.PATTERN11));
						} catch (ParseException e) {
							log.error(e);
						}
						aptInput.setTransferAddress(airportServiceDTO.getTransferAddress());
						aptInput.setTransferContact(airportServiceDTO.getTransferContact());
						aptInput.setTransferType(airportServiceDTO.getTransferType());
						pricedSelectedItem.setInput(aptInput);
						ancillaryScope.getAncillaries().add(pricedSelectedItem);
						scopeWiseAnciMap.put(airportScope, ancillaryScope);

					}
					for (AncillaryScope AncillaryScope : scopeWiseAnciMap.values()) {
						pricedAncillaryType.getAncillaryScopes().add(AncillaryScope);
					}
					anciTypeMap.put(anciType, pricedAncillaryType);
				}
			}
			List<PricedAncillaryType> passengerWiseAnicList =  new ArrayList<PricedAncillaryType>();
			for(PricedAncillaryType pricedAncillaryTypeRef : anciTypeMap.values()){
				passengerWiseAnicList.add(pricedAncillaryTypeRef);
			}
			
			loadReservationRS.getPassengers().get(i)
					.setPassengerWiseSelectedAncillaries(passengerWiseAnicList);
			i++;
		}
		for(OndUnit ondUnit : ondIdMap.values()){
			ondPreferences.add(ondUnit);
		}
		if(reservationInsuranceMap.values().size() > 0){
			loadReservationRS.setReservationWiseSelectedAncillary(new ArrayList<PricedAncillaryType>());
			// assuming insurances are provided for all segments
			for(LCCInsuranceQuotationDTO lccInsuranceQuotationDTO : reservationInsuranceMap.values()){
				reservationPricedAncillaryType = new PricedAncillaryType();
				ancillaryScope = new AncillaryScope();
				Scope scope = new Scope();
				reservationPricedAncillaryType.setAncillaryType(AncillariesConstants.AncillaryType.INSURANCE.name());
				reservationPricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());
				scope.setScopeType(Scope.ScopeType.ALL_SEGMENTS);
				ancillaryScope.setScope(scope);
				ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());
				pricedSelectedItem = new PricedSelectedItem();
				pricedSelectedItem.setId(lccInsuranceQuotationDTO.getInsuranceRefNumber());
				pricedSelectedItem.setName(lccInsuranceQuotationDTO.getDisplayName());
				pricedSelectedItem.setAmount(lccInsuranceQuotationDTO.getQuotedTotalPremiumAmount());
				ancillaryScope.getAncillaries().add(pricedSelectedItem);
				reservationPricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
				loadReservationRS.getReservationWiseSelectedAncillary().add(reservationPricedAncillaryType);
			}
		}
		if(ondPreferences != null && ondPreferences.size() > 0){
			loadReservationRS.setMetaData(new MetaData());
			loadReservationRS.getMetaData().setOndPreferences(ondPreferences);
		}
		
		return loadReservationRS;
	}

	private String getBaggageONDGroupId(String flightRefNumber, Set<LCCClientReservationSegment> segments) {
		String baggageONDGroupId = null;
		for (LCCClientReservationSegment segment : segments) {
			if (flightRefNumber.equals(segment.getFlightSegmentRefNumber())) {
				baggageONDGroupId = segment.getBaggageONDGroupId();
			}
		}
		return baggageONDGroupId;
	}

	private PricedAncillaryType updateAnciTypeMap(AncillariesConstants.AncillaryType anciType) {
		PricedAncillaryType pricedAncillaryType;
		if (!anciTypeMap.containsKey(anciType)) {
			pricedAncillaryType = new PricedAncillaryType();// check anci type exists
			pricedAncillaryType.setAncillaryType(anciType.name());
			pricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());
			anciTypeMap.put(anciType, pricedAncillaryType);
		}
		return anciTypeMap.get(anciType);
	}

	private AncillaryScope updateScopeWiseAnciMap(Scope scope) {
		AncillaryScope ancillaryScope;
		if (!scopeWiseAnciMap.containsKey(scope)) {
			ancillaryScope = new AncillaryScope();// check anci type exists
			ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());
			ancillaryScope.setScope(scope);
			scopeWiseAnciMap.put(scope, ancillaryScope);
		}
		return scopeWiseAnciMap.get(scope);
	}

	private AncillaryScope updateONDWiseAnciMap(Scope scope) {
		AncillaryScope ancillaryScope;
		if (!ondWiseAnciMap.containsKey(scope)) {
			ancillaryScope = new AncillaryScope();// check anci type exists
			ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());
			ancillaryScope.setScope(scope);
			ondWiseAnciMap.put(scope, ancillaryScope);
		}
		return ondWiseAnciMap.get(scope);
	}
	
	private void updateMetaData(String ondId, String flightSegRPH){
		OndUnit ondUnit;
		if(!ondIdMap.containsKey(ondId)){
			ondUnit = new OndUnit();
			ondUnit.setOndId(ondId);
			ondUnit.setFlightSegmentRPH(new ArrayList<String>());
			ondIdMap.put(ondId, ondUnit);
		}
		ondUnit = ondIdMap.get(ondId);
		if(!ondUnit.getFlightSegmentRPH().contains(flightSegRPH)){
			ondUnit.getFlightSegmentRPH().add(flightSegRPH);
		}
	}
	
	private void updateReservationInsurance(LCCInsuranceQuotationDTO insuranceRef){
		String insuranceRefNumber = insuranceRef.getInsuranceRefNumber();
		if(!reservationInsuranceMap.containsKey(insuranceRefNumber)){
			reservationInsuranceMap.put(insuranceRefNumber, insuranceRef);
		}
	}
	
	private boolean checkPriceItemExists(PricedSelectedItem pricedItem, List<PricedSelectedItem> pricedAncList){
		for(PricedSelectedItem refPricedSelectedItem : pricedAncList){
			if(pricedItem.getId().equals(refPricedSelectedItem.getId())){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkONDExists(AncillaryScope ancillaryScope, List<AncillaryScope> anciScopeList){
		for(AncillaryScope refAncillaryScope : anciScopeList){
			if(ancillaryScope.getScope().equals(refAncillaryScope.getScope())){
				return true;
			}
		}
		return false;
	}
	
	private BigDecimal getPaxTypeWiseAPSCharge(String paxType, LCCAirportServiceDTO airportServiceDTO){
		BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
		
		switch(paxType){
		case PaxTypeTO.ADULT:
			charge = airportServiceDTO.getAdultAmount();
			break;
		case PaxTypeTO.CHILD:
			charge = airportServiceDTO.getChildAmount();
			break; 
		case PaxTypeTO.INFANT:
			charge = airportServiceDTO.getInfantAmount();
			break; 
		case PaxTypeTO.PARENT:
			charge = airportServiceDTO.getAdultAmount();
			break; 
		}
		return charge;
	}

}
