package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;

public class AdditionalFareClassInfo {

	private boolean discountAvailable;

	@CurrencyValue
	private BigDecimal discountAmount;


	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public boolean isDiscountAvailable() {
		return discountAvailable;
	}

	public void setDiscountAvailable(boolean discountAvailable) {
		this.discountAvailable = discountAvailable;
	}




}
