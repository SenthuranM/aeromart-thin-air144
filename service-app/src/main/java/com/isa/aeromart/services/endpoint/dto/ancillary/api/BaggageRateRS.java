package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import java.util.Collection;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.FlightSegmentBaggageRate;

public class BaggageRateRS extends TransactionalBaseRS {

	private Collection<FlightSegmentBaggageRate> baggageRates;

	public Collection<FlightSegmentBaggageRate> getBaggageRates() {
		return baggageRates;
	}

	public void setBaggageRates(Collection<FlightSegmentBaggageRate> baggageRates) {
		this.baggageRates = baggageRates;
	}
}
