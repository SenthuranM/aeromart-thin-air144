package com.isa.aeromart.services.endpoint.dto.voucher;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;

public class IssueVoucherRQ extends TransactionalBaseRQ{
	
	
	private List<GiftVoucherDTO> voucherDTOList = new ArrayList<GiftVoucherDTO>();

	private String cardHolderName = null;

	private List<PaymentGateway> paymentGateways;

	public List<GiftVoucherDTO> getVoucherDTOList() {
		return voucherDTOList;
	}

	public void setVoucherDTOList(List<GiftVoucherDTO> voucherDTOList) {
		this.voucherDTOList = voucherDTOList;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public List<PaymentGateway> getPaymentGateways() {
		return paymentGateways;
	}

	public void setPaymentGateways(List<PaymentGateway> paymentGateways) {
		this.paymentGateways = paymentGateways;
	}
}