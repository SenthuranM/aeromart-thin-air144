package com.isa.aeromart.services.endpoint.dto.session;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.modification.FFIDUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.modification.RequoteBalanceInfo;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherInformation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySession;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.BalanceQueryRequiredData;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.RequoteONDInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationExternalChargeInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationSegmentInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.AvailabilityRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class RequoteTransaction extends BaseTransaction implements Serializable, AvailabilityRequoteSessionStore,
		AncillarySessionStore, PaymentRequoteSessionStore, RequoteSessionStore {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String SESSION_KEY = "REQUOTE_TRNX";
	private RPHGenerator rphGenerator;
	private RequoteBalanceInfo requoteBalanceInfo;
	private List<SessionFlightSegment> segments;
	private Double remainingLoyaltyPoints;
	private List<String> fltSegUnqIds;
	private List<ServiceTaxContainer> applicableServiceTaxes;

	private BalanceQueryRequiredData balanceQueryRequiredData = new BalanceQueryRequiredData();

	// anci session data bundle
	private AncillarySession ancillarySession;

	private ReservationSegmentInfo reservationSegmentInfo;
	private List<RequoteONDInfo> ondInfo;
	private PreferenceInfo preferenceInfo;
	private ReservationInfo resInfo;
	private FareInfo fareInfo;
	private Map<Integer, NameDTO> nameChangedPaxMap;
	private List<ModifiedFlight> modifiedFlightsInfo;
	private Map<String, String> resSegRphToFlightSegMap; // TODO -- need a review

	// payment related
	private PostPaymentDTO postPaymentDTO;
	private LoyaltyInformation loyaltyInformation = null;
	private PaymentStatus paymentStatus;
	private DiscountedFareDetails discountedFareDetails;
	private OperationTypes operationTypes;
	private BigDecimal serviceTaxForTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private FinancialStore financialStore;

	private List<SessionFlexiDetail> sessionFlexiDetailList;
	private String originCountryCodeForTest;

	private Map<String, Integer> serviceCount;
	private int paymentAttempts;
	private FFIDUpdateRQ ffidUpdateRQ;

	private boolean adminFeeInitiated;
	private String voucherOTP;

	private Map<String, String> voucherOTPMap;
	private PayByVoucherInfo payByVoucherInfo;
	private VoucherInformation voucherInformation;

	public RequoteTransaction() {
		super.addInitHook(() -> {
			ancillarySession = new AncillarySession();
			ancillarySession.init();
			financialStore = new FinancialStore();
		});

		super.init();
	}

	@Override
	public FareInfo getFareInfo() {
		if (fareInfo == null) {
			return new FareInfo();
		}

		return fareInfo;
	}

	@Override
	public BalanceQueryRequiredData getRequoteBalanceReqiredParams() {
		if (balanceQueryRequiredData == null) {
			return new BalanceQueryRequiredData();
		}
		return balanceQueryRequiredData;
	}

	@Override
	public void storePriceInventoryInfo(FareSegChargeTO pricingInformation) {
		// TODO Auto-generated method stub

	}

	public void storeSelectedSegments(List<FlightSegmentTO> segments) {
		this.segments = new ArrayList<SessionFlightSegment>();
		for (FlightSegmentTO segmentTO : segments) {
			this.segments.add(new SessionFlightSegment(segmentTO));
		}
	}

	@Override
	public RPHGenerator getRPHGenerator() {
		if (rphGenerator == null) {
			rphGenerator = new RPHGenerator();
		}
		return rphGenerator;
	}

	@Override
	public ReservationSegmentInfo getResSegmentInfo() {
		if (reservationSegmentInfo == null) {
			return new ReservationSegmentInfo();
		}

		return reservationSegmentInfo;
	}

	@Override
	public List<RequoteONDInfo> getOndInfo() {
		if (ondInfo == null) {
			return new ArrayList<RequoteONDInfo>();
		}
		return ondInfo;
	}

	@Override
	public PreferenceInfo getPreferences() {
		if (preferenceInfo == null) {
			return new PreferenceInfo();
		}

		return preferenceInfo;
	}

	@Override
	public ReservationInfo getResInfo() {
		if (resInfo == null) {
			return new ReservationInfo();
		}
		return resInfo;
	}

	@Override
	public ReservationExternalChargeInfo getExtChargeInfo() {
		return new ReservationExternalChargeInfo();
	}

	@Override
	public RequoteBalanceInfo getRequoteBalanceInfo() {
		if (requoteBalanceInfo == null) {
			return new RequoteBalanceInfo();
		}
		return requoteBalanceInfo;
	}

	@Override
	public void storeBalanceInfo(RequoteBalanceInfo requoteBalanceInfo) {
		this.requoteBalanceInfo = requoteBalanceInfo;
	}

	@Override
	public Double getRemainingLoyaltyPoints() {
		return remainingLoyaltyPoints;
	}

	@Override
	public void storeRemainingAvailableLoyaltyPoints(Double loyaltyPoints) {
		this.remainingLoyaltyPoints = loyaltyPoints;
	}

	@Override
	public void storeOndInfo(List<RequoteONDInfo> ondInfo) {
		this.ondInfo = ondInfo;
	}

	@Override
	public void storeResSegmentInfo(ReservationSegmentInfo reservationSegmentInfo) {
		this.reservationSegmentInfo = reservationSegmentInfo;
	}

	@Override
	public void storeRequoteBalanceReqiredParams(BalanceQueryRequiredData balanceQueryRequiredData) {
		this.balanceQueryRequiredData = balanceQueryRequiredData;
	}

	@Override
	public void storePreferences(PreferenceInfo preferenceInfo) {
		this.preferenceInfo = preferenceInfo;
	}

	@Override
	public void storeResInfo(ReservationInfo resInfo) {
		this.resInfo = resInfo;
	}

	@Override
	public void storeFareInfo(FareInfo fareInfo) {
		this.fareInfo = fareInfo;
	}

	@Override
	public PostPaymentDTO getPostPaymentInformation() {
		return postPaymentDTO;
	}

	@Override
	public PostPaymentDTO getRequotePostPaymentInformation() {
		return postPaymentDTO;
	}

	@Override
	public Map<Integer, NameDTO> getNameChangedPaxMap() {
		return nameChangedPaxMap;
	}

	@Override
	public void storeNameChangedPaxMap(Map<Integer, NameDTO> nameChangedPaxMap) {
		this.nameChangedPaxMap = nameChangedPaxMap;
	}

	// anci impl ------------

	public SessionBasicReservation getReservationData() {
		return null;
	}

	public AncillarySession getAncillarySession() {
		return ancillarySession;
	}

	public List<SessionFlightSegment> getSegmentWithInventory() {
		List<SessionFlightSegment> newFlightSegments = new ArrayList<SessionFlightSegment>();

		Map<String, SessionFlightSegment> flightSegmentsMap = new HashMap<String, SessionFlightSegment>();
		String flightSegmentRph;
		for (SessionFlightSegment flightSegment : segments) {
			flightSegmentRph = rphGenerator.getRPH(flightSegment.getLCCFlightReference());
			flightSegmentsMap.put(flightSegmentRph, flightSegment);
		}

		if (modifiedFlightsInfo != null) {
			for (ModifiedFlight modification : modifiedFlightsInfo) {
				if (modification.getToFlightSegRPH() != null) {
					for (String newFlightSegRph : modification.getToFlightSegRPH()) {
						newFlightSegments.add(flightSegmentsMap.get(newFlightSegRph));
					}
				}
			}
		}
		return newFlightSegments;
	}

	public FareSegChargeTO getFarePricingInformation() {
		return fareInfo.getFareSegmentChargeTO();
	}

	public void addPaymentCharge(ExternalChgDTO ccChgDTO) {
		financialStore.getPaymentExternalCharges().getReservationCharges().add(new LCCClientExternalChgDTO(ccChgDTO));
	}

	public void removePaymentCharge(EXTERNAL_CHARGES extChgEnum) {
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getReservationCharges() != null
				&& !financialStore.getPaymentExternalCharges().getReservationCharges().isEmpty()) {
			Iterator<LCCClientExternalChgDTO> paymentChargeItr = financialStore.getPaymentExternalCharges()
					.getReservationCharges().iterator();
			while (paymentChargeItr.hasNext()) {
				LCCClientExternalChgDTO charge = paymentChargeItr.next();
				if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
					paymentChargeItr.remove();
				}
			}
		}
	}

	@Override
	public void storePostPaymentInformation(PostPaymentDTO postPaymentDTO) {
		this.postPaymentDTO = postPaymentDTO;
	}

	@Override
	public List<SessionFlightSegment> getSelectedSegmentsForPayment() {
		return segments;
	}

	@Override
	public PreferenceInfo getPreferencesForPayment() {
		return preferenceInfo;
	}

	@Override
	public FareSegChargeTO getFareSegChargeTOForPayment() {
		if (fareInfo != null)
			return fareInfo.getFareSegmentChargeTO();

		return null;
	}

	@Override
	public LoyaltyInformation getLoyaltyInformation() {
		if (loyaltyInformation == null) {
			loyaltyInformation = new LoyaltyInformation();
			loyaltyInformation.setLoyaltyPaymentOption(LoyaltyPaymentOption.NONE);
		}
		return loyaltyInformation;
	}

	@Override
	public void storeLoyaltyInformation(LoyaltyInformation loyaltyInformation) {
		this.loyaltyInformation = loyaltyInformation;
	}

	@Override
	public FinancialStore getFinancialStore() {
		if (financialStore == null) {
			return new FinancialStore();
		}
		return financialStore;
	}

	@Override
	public void storeTravellerQuantity(TravellerQuantity travellerQuantity) {
		// TODO Auto-generated method stub

	}

	public TravellerQuantity getTravellerQuantityForPayment() {
		return resInfo.getPaxTypeWiseCount();
	}

	public TravellerQuantity getTravellerQuantityForAncillary() {
		return resInfo.getPaxTypeWiseCount();
	}

	@Override
	public void storeDiscountDetails(DiscountedFareDetails discountDetails) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storeReservationDiscountDTO(ReservationDiscountDTO reservationDiscountDTO) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<LCCClientExternalChgDTO> getExternalCharges() {
		return financialStore.getAllExternalCharges();
	}

	@Override
	public List<ServiceTaxContainer> getApplicableServiceTaxes() {
		if (applicableServiceTaxes == null) {
			applicableServiceTaxes = new ArrayList<ServiceTaxContainer>();
		}
		return applicableServiceTaxes;
	}

	@Override
	public void storeApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		getApplicableServiceTaxes().clear();
		getApplicableServiceTaxes().addAll(applicableServiceTaxes);
	}

	@Override
	public void storePaymentStatus(PaymentStatus actionStatus) {
		this.paymentStatus = actionStatus;
	}

	@Override
	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	@Override
	public void clearPaymentInformation() {
		this.paymentStatus = null;
		this.postPaymentDTO = null;
		this.loyaltyInformation = null;
		removeAllPaymentCharge();

	}

	@Override
	public void storeOperationType(OperationTypes operationTypes) {
		this.operationTypes = operationTypes;
	}

	@Override
	public OperationTypes getOperationType() {
		return operationTypes;
	}

	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalCharges() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PreferenceInfo getPreferencesForAncillary() {
		return preferenceInfo;
	}

	@Override
	public BigDecimal getBalanceToPayAmount() {
		return requoteBalanceInfo.getBalanceToPay();

	}

	public List<ModifiedFlight> getModifiedFlightsInfo() {
		return modifiedFlightsInfo;
	}

	public void storeModifiedFlightsInfo(List<ModifiedFlight> modifiedFlightsInfo) {
		this.modifiedFlightsInfo = modifiedFlightsInfo;
	}

	private void removeAllPaymentCharge() {
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getReservationCharges() != null
				&& !financialStore.getPaymentExternalCharges().getReservationCharges().isEmpty()) {
			financialStore.getPaymentExternalCharges().getReservationCharges().clear();
			financialStore.getPaymentExternalCharges().getPassengers().clear();

		}
	}

	public Collection<LCCClientPassengerSummaryTO> getPassengerSummaryList() {
		return requoteBalanceInfo.getPassengerSummaryList();
	}

	public Map<String, String> getResSegRphToFlightSegMap() {
		return resSegRphToFlightSegMap;
	}

	@Override
	public void storeResSegRphToFlightSegMap(String resSegmentRPH, String flightSegmentRPH) {
		if (resSegRphToFlightSegMap == null) {
			resSegRphToFlightSegMap = new HashMap<String, String>();
		}
		resSegRphToFlightSegMap.put(resSegmentRPH, flightSegmentRPH);
	}

	@Override
	public void storeRPHGeneratorForAncillaryModification(RPHGenerator rphGenerator) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storeReservationDataForAncillaryModification(ReservationInfo resInfo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storeSessionFlightsForAncillaryModification(List<FlightSegmentTO> segments) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storePreferenceInfoForAncillaryModification(PreferenceInfo preferenceInfo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storePricingInformationForAncillaryModification(FareSegChargeTO pricingInformation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isGroupPNR() {
		return getResInfo().isGroupPNR();
	}

	@Override
	public void storeSessionFlexiDetails(List<SessionFlexiDetail> sessionFlexiDetailList) {
		this.sessionFlexiDetailList = sessionFlexiDetailList;
	}

	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetails() {
		return sessionFlexiDetailList;
	}

	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetailForAncillary() {
		return sessionFlexiDetailList;
	}

	@Override
	public void clearRequoteSessionTansaction() {
		this.rphGenerator = null;
		this.applicableServiceTaxes = null;
		this.balanceQueryRequiredData = null;
		this.fareInfo = null;
		this.discountedFareDetails = null;
		this.financialStore = null;
		this.modifiedFlightsInfo = null;
		this.nameChangedPaxMap = null;
		this.ondInfo = null;
		this.preferenceInfo = null;
		this.remainingLoyaltyPoints = null;
		this.requoteBalanceInfo = null;
		this.reservationSegmentInfo = null;
		this.resInfo = null;
		this.resSegRphToFlightSegMap = null;
		this.segments = null;
		this.sessionFlexiDetailList = null;
	}

	@Override
	public List<SessionFlightSegment> getFlightSegments() {
		return segments;
	}

	@Override
	public DiscountedFareDetails getDiscountedFareDetailsForAncillary() {
		return discountedFareDetails;
	}

	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalChargesForAncillary() {
		if (financialStore.getAncillaryExternalCharges() != null) {
			return financialStore.getAncillaryExternalCharges().getPassengers();
		}
		return null;
	}

	@Override
	public List<SessionFlightSegment> getSelectedSegmentsForAncillary() {
		return segments;
	}

	@Override
	public void storeUpdatedSessionFlightSegment(List<SessionFlightSegment> segments) {
		this.segments = segments;
	}

	@Override
	public void setDiscountAmount(BigDecimal discountAmount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setOriginCountryCodeForTest(String originCountryCodeForTest) {
		this.originCountryCodeForTest = originCountryCodeForTest;

	}

	@Override
	public String getOriginCountryCodeForTest() {
		return this.originCountryCodeForTest;
	}

	@Override
	public void storeServiceCount(Map<String, Integer> serviceCount) {
		this.serviceCount = serviceCount;

	}

	@Override
	public Map<String, Integer> getServiceCount() {
		return this.serviceCount;
	}

	@Override
	public List<String> getExistingFlightSegunqIdList() {
		if (fltSegUnqIds == null) {
			fltSegUnqIds = new ArrayList<String>();
		}
		return fltSegUnqIds;
	}

	@Override
	public List<SessionFlightSegment> getSegments() {
		return segments;
	}

	@Override
	public void storeFFIDChangeRQ(FFIDUpdateRQ ffidUpdateRQ) {
		this.ffidUpdateRQ = ffidUpdateRQ;

	}

	@Override
	public FFIDUpdateRQ getFFIDChangeRQ() {
		if (ffidUpdateRQ == null) {
			ffidUpdateRQ = new FFIDUpdateRQ();
		}
		return ffidUpdateRQ;
	}

	@Override
	public boolean isOnHoldCreated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void trackPaymentAttempts() throws ModuleException {
		paymentAttempts++;
		if (paymentAttempts > PaymentConsts.PAYMENT_ATTEMPT_LIMIT) {
			throw new ModuleException("payment.api.payment.attempt.limit.exceeded");
		}

	}

	public boolean isAdminFeeInitiated() throws ModuleException {
		if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			throw new ModuleException("payment.api.invalid.administration.fee.access");
		}
		return this.adminFeeInitiated;
	}

	public void setAdminFeeInitiated() throws ModuleException {
		if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			throw new ModuleException("payment.api.invalid.administration.fee.access");
		}
		this.adminFeeInitiated = true;
	}

	@Override
	public void removePaymentPaxCharge(EXTERNAL_CHARGES extChgEnum) {
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getPassengers() != null
				&& !financialStore.getPaymentExternalCharges().getPassengers().isEmpty()) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> passengerCharges = financialStore.getPaymentExternalCharges()
					.getPassengers();
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : passengerCharges) {
				Iterator<LCCClientExternalChgDTO> paymentChargeItr = passengerChargeTo.getGetPassengerCharges().iterator();
				while (paymentChargeItr.hasNext()) {
					LCCClientExternalChgDTO charge = paymentChargeItr.next();
					if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
						paymentChargeItr.remove();
					}
				}
			}
		}
	}

	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetailForPayment() {
		// TODO Auto-generated method stubgetAllSegmentWithInventory
		return null;
	}

	public BigDecimal getServiceTaxForTransactionFee() {
		return serviceTaxForTransactionFee;
	}

	public void setServiceTaxForTransactionFee(BigDecimal serviceTaxForTransactionFee) {
		this.serviceTaxForTransactionFee = serviceTaxForTransactionFee;
	}

	@Override
	public List<SessionFlightSegment> getAllSegmentWithInventory() {
		return getSegments();
	}

	@Override
	public String getVoucherOTP() {
		return voucherOTP;
	}

	@Override
	public void setVoucherOTP(String voucherOTP) {
		this.voucherOTP = voucherOTP;
	}

	@Override
	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	@Override
	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	@Override public VoucherInformation getVoucherInformation() {
		if (voucherInformation == null) {
			voucherInformation = new VoucherInformation();
			voucherInformation.setIsTotalAmountPaidFromVoucher(false);
		}
		return voucherInformation;
	}

	@Override
	public Map<String, String> getVoucherOTPMap() {
		if (this.voucherOTPMap == null) {
			this.voucherOTPMap = new HashMap<>();
		}
		return voucherOTPMap;
	}

}
