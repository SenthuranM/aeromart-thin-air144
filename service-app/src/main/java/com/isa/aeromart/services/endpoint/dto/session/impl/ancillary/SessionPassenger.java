package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

public class SessionPassenger extends SessionPerson {

	private String passengerRph;

	private String type;
	
	private boolean isParent;

	public String getPassengerRph() {
		return passengerRph;
	}

	public void setPassengerRph(String passengerRph) {
		this.passengerRph = passengerRph;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isParent() {
		return isParent;
	}

	public void setParent(boolean isParent) {
		this.isParent = isParent;
	}
	
}
