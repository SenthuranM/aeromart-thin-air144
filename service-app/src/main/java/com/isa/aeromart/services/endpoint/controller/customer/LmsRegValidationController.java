package com.isa.aeromart.services.endpoint.controller.customer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;

@Controller
@RequestMapping("customer")
public class LmsRegValidationController extends StatefulController {

	// private static Log log = LogFactory.getLog(LmsRegValidationController.class);
	//
	// @ResponseBody
	// @RequestMapping(value = "/lmsRegistrationValidation", method = RequestMethod.POST, consumes = {
	// MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	// public
	// LMSRegValidationRes validateLMSInfo(@RequestBody LMSRegValidationReq lmsRegValidationReq) {
	// LMSRegValidationRes lmsRegValidationRes = new LMSRegValidationRes();
	// lmsRegValidationRes.setSuccess(true);
	// if (AppSysParamsUtil.isLMSEnabled()) {
	// LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
	// LMSRegValidationInfo lmsRegValidationInfo = lmsRegValidationReq.getLmsRegValidationInfo();
	// if (lmsRegValidationInfo.getRefferedFFID() != null && !lmsRegValidationInfo.getRefferedFFID().equals("")) {
	// if (LmsCommonUtil.isNonMandAccountAvailable(lmsRegValidationInfo.getRefferedFFID(), loyaltyManagementBD)) {
	// lmsRegValidationRes.setValidRefferdEmail(true);
	// } else {
	// lmsRegValidationRes.setValidRefferdEmail(false);
	// return lmsRegValidationRes;
	// }
	// } else {
	// lmsRegValidationRes.setValidRefferdEmail(true);
	// }
	// if (lmsRegValidationInfo.getHeadFFID() != null && !lmsRegValidationInfo.getHeadFFID().equals("")) {
	// if (LmsCommonUtil.isNonMandAccountAvailable(lmsRegValidationInfo.getHeadFFID(), loyaltyManagementBD)) {
	// lmsRegValidationRes.setValidFamilyHeadEmail(true);
	// } else {
	// lmsRegValidationRes.setValidFamilyHeadEmail(false);
	// return lmsRegValidationRes;
	// }
	// } else {
	// lmsRegValidationRes.setValidFamilyHeadEmail(true);
	// }
	// if (lmsRegValidationInfo.getFfid() != null && !lmsRegValidationInfo.getFfid().equals("")) {
	//
	// Customer customer;
	// try {
	// customer = ModuleServiceLocator.getCustomerBD().getCustomer(lmsRegValidationInfo.getFfid());
	// if (!lmsRegValidationReq.isMyAACustomerOnly()) {
	// if (customer != null) {
	// lmsRegValidationRes.setRegisteredCustomer(true);
	// return lmsRegValidationRes;
	// }
	// }
	//
	// if (customer != null) {
	// if (lmsRegValidationInfo.getFirstName() == null || "".equals(lmsRegValidationInfo.getFirstName())) {
	// lmsRegValidationInfo.setFirstName(customer.getFirstName());
	// }
	// if (lmsRegValidationInfo.getLastName() == null || "".equals(lmsRegValidationInfo.getLastName())) {
	// lmsRegValidationInfo.setLastName(customer.getLastName());
	// }
	// }
	//
	// if (isExistingLMSNameMatches(lmsRegValidationInfo.getFirstName(), lmsRegValidationInfo.getLastName(),
	// lmsRegValidationInfo.getFfid())) {
	// lmsRegValidationRes.setLmsNameMatch(true);
	// return lmsRegValidationRes;
	// } else {
	// lmsRegValidationRes.setLmsNameMatch(false);
	// }
	// } catch (ModuleException e) {
	// lmsRegValidationRes.setSuccess(false);
	// log.error("Unhandled Error ==> LmsRegValidationController", e);
	// }
	// }
	// }
	// return lmsRegValidationRes;
	// }
	//
	// public static boolean isExistingLMSNameMatches(String firstName, String lastName, String ffid) throws
	// ModuleException {
	// LoyaltyMemberCoreDTO existingMember =
	// ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyMemberCoreDetails(ffid);
	//
	// if (existingMember != null
	// && existingMember.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK
	// && (!existingMember.getMemberFirstName().equalsIgnoreCase(firstName) || !existingMember.getMemberLastName()
	// .equalsIgnoreCase(lastName))) {
	// return false;
	// }
	//
	// return true;
	// }
}
