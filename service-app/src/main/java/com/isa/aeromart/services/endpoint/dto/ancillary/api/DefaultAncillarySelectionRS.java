package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReProtect;

public class DefaultAncillarySelectionRS extends BaseAncillarySelectionsRS {

	private AncillaryReProtect ancillaryReProtect;

	public AncillaryReProtect getAncillaryReProtect() {
		return ancillaryReProtect;
	}

	public void setAncillaryReProtect(AncillaryReProtect ancillaryReProtect) {
		this.ancillaryReProtect = ancillaryReProtect;
	}
}
