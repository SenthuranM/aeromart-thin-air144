package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Set;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;


public class CustomerCreditResponse extends BaseRS{

	private Set<CustomerCredit> customerCreditList;

	public Set<CustomerCredit> getCustomerCreditList() {
		return customerCreditList;
	}

	public void setCustomerCreditList(Set<CustomerCredit> customerCreditList) {
		this.customerCreditList = customerCreditList;
	}
	
}
