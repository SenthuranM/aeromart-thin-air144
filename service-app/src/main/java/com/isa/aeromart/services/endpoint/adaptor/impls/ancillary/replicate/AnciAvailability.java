package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate;

import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;

public class AnciAvailability {
	public static boolean isAnciActive(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
			for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
				if ((ancillaryStatus.isAvailable())
						&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isAnciActive(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
	                                   List<FlightSegmentTO> flightSegmentTOs, String ancilaryType) {

		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (isAvailableForSelecetedSegment(ancillaryAvailabilityOutDTO.getFlightSegmentTO()
						.getFlightRefNumber(), flightSegmentTOs)) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(ancilaryType))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	private static boolean isAvailableForSelecetedSegment(String fltRefNo, List<FlightSegmentTO> flightSegmentTOs) {
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getFlightRefNumber() != null) {
				String[] fltRefArr1 = flightSegmentTO.getFlightRefNumber().split("-");
				String[] fltRefArr2 = fltRefNo.split("-");
				if (fltRefArr1[0].equals(fltRefArr2[0])) {
					return true;
				}
			}
		}

		return false;
	}

	public static boolean isAnciActive(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
	                                   String fltRefNo, String ancilaryType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (FlightRefNumberUtil.getSegmentIdFromFlightRPH(
						ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber()).intValue() == FlightRefNumberUtil
						.getSegmentIdFromFlightRPH(fltRefNo).intValue()) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(ancilaryType))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
					break;
				}
			}
		}
		return false;
	}
}
