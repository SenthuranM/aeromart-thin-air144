package com.isa.aeromart.services.endpoint.delegate.common;

import javax.servlet.http.HttpServletRequest;

public interface CaptchaVerifyService {

	public boolean verifyCaptcha(String captcha , HttpServletRequest request);
	
}
