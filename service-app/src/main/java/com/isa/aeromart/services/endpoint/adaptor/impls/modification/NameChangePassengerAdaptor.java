package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.modification.NameChangePassenger;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class NameChangePassengerAdaptor implements Adaptor<NameChangePassenger, LCCClientReservationPax> {

	Map<Integer,LCCClientReservationPax> reservationPaxMap = new HashMap<Integer, LCCClientReservationPax>();  
 	
	public NameChangePassengerAdaptor(Set<LCCClientReservationPax> lccPax){
		for(LCCClientReservationPax pax : lccPax){
			reservationPaxMap.put(pax.getPaxSequence(), pax);
		}
	}
	
	
	public LCCClientReservationPax adapt(NameChangePassenger source) {

		LCCClientReservationPax resPax = new LCCClientReservationPax();
		resPax.setFirstName(source.getFirstName());
		resPax.setLastName(source.getLastName());
		resPax.setTitle(source.getTitle());
		resPax.setPaxSequence(source.getPaxSeqNo());
		resPax.setPaxType(source.getPaxType());
		resPax.setTravelerRefNumber(reservationPaxMap.get(source.getPaxSeqNo()).getTravelerRefNumber());
		
		LCCClientReservationAdditionalPax lccClientReservationAdditionalPax = new LCCClientReservationAdditionalPax();
		lccClientReservationAdditionalPax.setFfid(source.getFfid());				
		lccClientReservationAdditionalPax.setPassportNo(reservationPaxMap.get(source.getPaxSeqNo()).getLccClientAdditionPax().getPassportNo());
			
		resPax.setLccClientAdditionPax(lccClientReservationAdditionalPax);
		
		return resPax;
	}
}

