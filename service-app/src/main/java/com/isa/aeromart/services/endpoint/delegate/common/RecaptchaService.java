package com.isa.aeromart.services.endpoint.delegate.common;

import com.isa.aeromart.services.endpoint.dto.common.CaptchaVerifyRQ;

public interface RecaptchaService {

	public boolean verifyRequest(CaptchaVerifyRQ captchaReq);

}