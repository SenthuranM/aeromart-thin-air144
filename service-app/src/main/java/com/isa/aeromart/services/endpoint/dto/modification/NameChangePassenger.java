package com.isa.aeromart.services.endpoint.dto.modification;

public class NameChangePassenger {

	private String title;

	private String firstName;

	private String lastName;

	private String paxType;

	private int paxSeqNo;

	private String ffid;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the paxSeqNo
	 */
	public int getPaxSeqNo() {
		return paxSeqNo;
	}

	/**
	 * @param paxSeqNo
	 *            the paxSeqNo to set
	 */
	public void setPaxSeqNo(int paxSeqNo) {
		this.paxSeqNo = paxSeqNo;
	}

	/**
	 * @return the ffid
	 */
	public String getFfid() {
		return ffid;
	}

	/**
	 * @param ffid
	 *            the ffid to set
	 */
	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

}
