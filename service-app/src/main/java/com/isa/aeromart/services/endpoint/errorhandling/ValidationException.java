package com.isa.aeromart.services.endpoint.errorhandling;

import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;

public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final Code code;
	private ErrorType errorType = ErrorType.OTHER_ERROR;

	public ValidationException(Code code, Throwable cause) {

		super(cause);
		this.code = code;
	}

	public ValidationException(String messageCode, Code defaultCode, Throwable cause) {
		super(cause);
		this.code = getEnum(messageCode, defaultCode);
	}

	public ValidationException(Code code) {
		this.code = code;
	}

	public Code getCode() {
		return code;
	}

	private Code getEnum(String messageCode, Code defaultCode) {
		for (Code code : Code.values()) {
			if (code.getMessageCode().equals(messageCode)) {
				return code;
			}
		}
		return defaultCode;
	}

	public interface BaseCode {
		public String getMessageCode();
	}

	public enum Code implements BaseCode {

		GENERIC_SYSTEM_ERROR("generic.system.error"), SYSTEM_ERROR("system.error"), NULL_ERROR("null.error"), INVALID_CHANNEL(
				"common.channel.invalid"), INVALID_TRANSACTION_ID("invalid.transaction.id"), NULL_TRANSACTION_ID(
				"null.transaction.id"),
		
		SELECTED_FLIGHT_FARES_NOT_AVAILABLE("selected.flight.fares.not.available"),

		CONTACT_DETAILS_VALIDATION_ERROR("contact.details.not.found"), PAXINFO_VALIDATION_ERROR("paxinfo.not.found"), DUPLICATE_PAX_NAMES_FOUND(
				"modification.api.name.change.duplicate.names.exist"), NAME_CHANGE_LIMIT_EXCEEDED(
				"modification.api.name.change.limit.exceeded"), NAME_CHANGE_NOT_ALLOWED(
				"modification.api.name.change.not.allowed"), NO_CHANGE_FOUND(
				"modification.api.name.change.name.modification.not.found"), SAME_FLIGHT_MODIFICATION_FOUND(
				"modification.same.flight.modification.found"), UN_SEG_FOUND("modification.api.un.segment.found"), FLIGHT_SEARCH_FARES_NOT_AVAILABLE(
				"modification.flight.search.fares.not.available"), INVALID_AIRPORT_MESSAGE_STAGE(
				"ibe.airport.message.stage.invalid"), NO_FFID_CHANGE("modification.api.ffid.change.modification.not.found"), INVALID_FFID_CHANGE_FOR_PNR(
				"modification.api.ffid.change.invalid.pnr"),INVALID_ROUTE_MODIFICATION(
				"modification.flight.search.route.not.allowed"),INSUFFICIENT_CONTACT_INFO_FOR_ROUTE_MODIFICATION(
				"modification.flight.search.contact.info.insufficient"), INVALID_CANCEL_SEGMENT_OPERATION(
				"modification.cancel.segment.not.allowed"),

		EQUAL_ORIGIN_DESTINATION("search.availability.validation.ond.inavalid"), WRONG_DEPARTURE_DATE(
				"search.availability.validation.date.inavalid"), WRONG_PASSENGER_COUNT(
				"search.availability.validation.passenger.inavalid"), SEARCH_ONLY_BUS_SEGMENT("errors.resv.flt.bus.only"), COUNTRY_CODE_INVALIED(
				"search.availability.validation.countryCode.invalied"), CURRENCY_CODE_INVALIED(
				"search.availability.validation.currencyCode.invalied"), INBOUND_DATE_BEFORE_OUTBOUND(
				"search.availability.validation.inbound.date.before.outbound"), MINIMUM_RETURN_TRANSITION_TIME_ERROR(
				"search.availability.validation.minimum.return.transition.time"), FLEXI_NOT_AVAILABLE(
				"search.availability.fare.quote.flexi.not.available"), INVALID_DEPARTURE_VARIENCE(
				"search.availability.validation.departure.varience.invalid"),INVALID_ORIGIN_DESTINATION(
				"search.availability.validation.ond.not.exist"),

		ANCI_GENERIC_ERROR("ancillary.generic"), ANCI_AVAILABILITY_GENERIC_ERROR("ancillary.availability.generic"), ANCI_AVAILABLE_GENERIC_ERROR(
				"ancillary.available.generic"), NULL_OR_REQUEST_EMPTY("ibe.ancillary.validation.request.epmpty"), WRONG_ANCI_MODE(
				"ibe.ancillary.validation.mode.error"), WRONG_ANCI_PASSENGER("ibe.ancillary.validation.passenger.error"), WRONG_INFANT_ALLOCATION(
				"ibe.ancillary.validation.passenger.infant"), WRONG_ANCI_SCOPE("ibe.ancillary.validation.scope.error"), WRONG_ANCI_OND_ID(
				"ibe.ancillary.validation.ondid.error"), NO_SELECTED_ITEMS("ibe.ancillary.validation.epmty.selection"), NO_PNR_FOUND(
				"ibe.ancillary.validation.epmty.pnr"), WRONG_ANCI_ALLOCATION("ibe.ancillary.validation.invalid.allocation"), INVALID_PASSENGER_ALLOCATION(
				"ibe.ancillary.validation.invalid.passenger"), INVALID_PNR("ibe.ancillary.validation.invalid.pnr"), EMPTY_ANCI_ID(
				"ibe.ancillary.validation.epmty.id"), ZERO_ANCI_QTY("ibe.ancillary.validation.zero.qty"), REQUESTED_SEATS_NOT_AVAILABLE(
				"ibe.ancillary.error.seats.notAvailable"),WRONG_OND_CODE("ibe.flight.availability.ond.error"),

		NULL_LOGIN_REQUEST("ibe.customer.login.request.empty"), NULL_REGISTER_UPDATE_CUSTOMER_REQUEST(
				"ibe.customer.save.update.request.empty"), NULL_FORGOTPASS_REQUEST("ibe.forgot.password.request.empty"), NULL_LMS_VALIDATION_REQUEST(
				"ibe.lms.validation.request.empty"), NULL_LMS_REGISTER_REQUEST("ibe.lms.register.empty"),

		DIFFERENT_EMAIL("ibe.customer.different.email"), DIFFERENT_FIRSTNAME("ibe.customer.different.firstName"), DIFFERENT_LASTNAME(
				"ibe.customer.different.lastName"), DIFFERENT_DATE_OF_BIRTH("ibe.customer.different.dob"), FAMILYHEAD_EMAIL_DOES_NOT_EXIST(
				"ibe.customer.lms.familyHead.error"), REFERRAL_EMAIL_DOES_NOT_EXIST("ibe.customer.lms.referral.error"), FAMILY_HEAD_SAME_AS_LOGIN_EMAIL("ibe.customer.lms.family.head.is.same"), CUSTOMER_ALREADY_REGISTERED(
				"error.customer.email.already.exists"), PASSWORD_MISMATCH("error.customer.password.mismatch"), INCORRECT_CURRENT_PASSWORD("error.customer.password.incorrect"),
				PASSWORD_RESET_LINK_EXPIRED("error.customer.password.reset.link.expired"),
				
		ONHOLD_WRONG_CAPTCHA("booking.onhold.wrong.captcha"), BOOKING_NOT_ALLOWED_TO_MODIFY_THROUGH_WEB(
				"ibe.booking.not.allowed.to.modify.through.web"),

		NULL_PAYMENT_OPTIONS_REQUEST("ibe.payment.payment.options.request.empty"),
		INVALID_PAYMENT_AMOUNT("payment.api.invalid.amount"),
		NULL_PAYMENT_EFFECTIVE_PAYMENT_REQUEST("ibe.payment.effective.payment.request.empty"), NULL_PAYMENT_MAKE_PAYMENT_REQUEST(
				"ibe.payment.make.payment.request.empty"), NULL_PAYMENT_PROMOTION_REQUEST("ibe.payment.promotion.request.empty"), NULL_PAYMENT_PAX_INFO(
				"ibe.payment.paxinfo.null"), NULL_PAYMENT_PROMOCODE("ibe.payment.promocode.null"), ERROR_PAYMENT_CARD_NO(
				"ibe.payment.cardno.error"), ERROR_PAYMENT_PAYMENT_GATEWAY("ibe.payment.payment.gateway.error"), NULL_PAYMENT_CONTACT_INFO(
				"ibe.payment.contactinfo.null"), NULL_PAYMENT_PNR("ibe.payment.pnr.null"), NULL_PAYMENT_PAYMENT_OPTIONS(
				"ibe.payment.payment.options.null"), FFID_NAME_MISMATCH("errors.resv.lms.ffid.not.matched"), FFID_INVALID(
				"errors.resv.lms.ffid.invalid"), FFID_INVALID_PAX_TYPE("errors.resv.lms.ffid.invalid.pax.type"), SEAT_NOLONGER_AVAILABLE(
				"error.fare.nolonger.available"), INVALID_COMBINATION("validate.load.res.invalid.combination"), CNX_RES_LOAD(
				"validate.load.res.cnx.modify"), LOYALTY_ACCOUNT_INVALID("loyalty.account.not.exist"), LOYALTY_ACCOUNT_LINKED(
				"loyalty.account.already.linked"), LOYALTY_CUSTOMER_INVALID("loyalty.account.customer.invalid"), LOYALTY_LMS_INVALID(
				"loyalty.account.lms.invalid"),

		TRANSACTION_ID_EMPTY("payment.api.transactionid.empty"), OPERATIONS_TYPE_EMPTY("payment.api.operations.type.empty"), EMPTY_REQUEST(
				"payment.api.request.empty"), ORIGIN_COUNTRY_CODE_EMPTY("payment.api.origin.country.code.empty"), PNR_EMPTY(
				"payment.api.pnr.empty"), TRANSACTION_FEE_EMPTY("payment.api.transaction.fee.empty"),TRANSACTION_EXIST("payment.api.transaction.exist"), PROMOCODE_EMPTY(
				"payment.api.promo.code.empty"), INVALID_IPG_ID("payment.api.invalid.payment.gateway.id"), INVALID_PASSENGER_INFORMATION(
				"payment.api.invalid.passenger.information"), CARD_NUMBER_EMPTY("payment.api.card.number.empty"), INVALID_IPG_CARD_ID(
				"payment.api.invalid.card.type"), INVALID_RESERVATION_CONTACT_DETAILS(
				"payment.api.invalid.reservation.contact.details"), PAYMENT_OPTIONS_EMPTY("payment.api.paymentoptions.empty"), LMS_OPTION_EMPTY(
				"payment.api.paymentoptions.lmscredits.empty"), LMS_REQUEST_AMOUNT_INVALID(
				"payment.api.paymentoptions.lmscredits.redeemamount.empty"), CARD_REQUEST_AMOUNT_INVALID(
				"payment.api.paymentoptions.pgw.cards.card.amount.empty"), INVALID_SESSION("payment.api.invalid.session"), PAYMENT_GATEWAYS_EMPTY(
				"payment.api.paymentoptions.pgws.empty"), PAYMENT_CARDS_EMPTY("payment.api.paymentoptions.pgw.cards.empty"), PAYMENT_GATEWAY_EMPTY(
				"payment.api.paymentoptions.pgw.empty"), PAYMENT_CARD_EMPTY("payment.api.paymentoptions.pgw.card.empty"), CARD_EXPIRY_DATE_EMPTY(
				"payment.api.paymentoptions.pgw.cards.card.date.empty"), CARD_EXPIRY_DATE_INVALID(
				"payment.api.paymentoptions.pgw.cards.card.date.invalid"), CARD_HOLDER_NAME_EMPTY(
				"payment.api.paymentoptions.pgw.cards.card.holder.empty"), EMPTY_IPG_CARD_CVV(
				"payment.api.paymentoptions.pgw.cards.card.cvv.empty"), INVALID_IPG_CARD_CVV(
				"payment.api.paymentoptions.pgw.cards.card.cvv.invalid"), LMS_CUSTOMER_NOT_LOGGED_IN(
				"payment.api.lms.customer.session.invalid"), LMS_CUSTOMER_ID_EMPTY("payment.api.lms.customer.id.invalid"), 
		LMS_INVALID_HEAD_FAMILY("errors.resv.lms.head.family.ffid.invalid"), LMS_INVALID_REFERED(
				"errors.resv.lms.refered.ffid.invalid"), LMS_ACCOUT_ALREADY_EXIST("errors.resv.lms.email.already.exists"), 
		LMS_HEAD_FAMILY_ALREADY_LINKED("errors.resv.lms.refered.ffid.already.linked"),
		LMS_REG_NAME_MISMATCH("errors.resv.lms.reg.name.mismatch"), LMS_NAME_MISMATCH("errors.resv.lms.name.mismatch"),
		LMS_LOGIN_INVALID("loyalty.account.lms.not.in.session"), LMS_CROSS_PORTAL_LOGIN_INVALID("loyalty.account.lms.not.in.session"),
		LMS_CROSS_PORTAL_LOGIN_ERROR("loyalty.account.lms.error.crossportal.login");
		

		private final String messageCode;

		private Code(String messageCode) {

			this.messageCode = messageCode;
		}

		public String getMessageCode() {

			return messageCode;
		}
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}

}