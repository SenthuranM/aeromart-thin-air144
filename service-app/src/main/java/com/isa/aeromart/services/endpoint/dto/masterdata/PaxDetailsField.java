package com.isa.aeromart.services.endpoint.dto.masterdata;

public class PaxDetailsField {

	private String fieldName;
	
	private PaxDetailsConfig visibility;
	
	private PaxDetailsConfig mandatory;
	
	private PaxDetailsConfig autoCheckinVisibility;

	private PaxDetailsConfig autoCheckinMandatory;

	private int maxLength;
	
	private String validation;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public PaxDetailsConfig getVisibility() {
		return visibility;
	}

	public void setVisibility(PaxDetailsConfig visibility) {
		this.visibility = visibility;
	}

	public PaxDetailsConfig getMandatory() {
		return mandatory;
	}

	public void setMandatory(PaxDetailsConfig mandatory) {
		this.mandatory = mandatory;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}
	
	/**
	 * @return the autoCheckinVisibility
	 */
	public PaxDetailsConfig getAutoCheckinVisibility() {
		return autoCheckinVisibility;
	}

	/**
	 * @param autoCheckinVisibility
	 *            the autoCheckinVisibility to set
	 */
	public void setAutoCheckinVisibility(PaxDetailsConfig autoCheckinVisibility) {
		this.autoCheckinVisibility = autoCheckinVisibility;
	}

	/**
	 * @return the autoCheckinMandatory
	 */
	public PaxDetailsConfig getAutoCheckinMandatory() {
		return autoCheckinMandatory;
	}

	/**
	 * @param autoCheckinMandatory
	 *            the autoCheckinMandatory to set
	 */
	public void setAutoCheckinMandatory(PaxDetailsConfig autoCheckinMandatory) {
		this.autoCheckinMandatory = autoCheckinMandatory;
	}
}
