package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.math.BigDecimal;
import java.util.List;

public class PricedSelectedItem extends SelectedItem {
	
	private BigDecimal amount;

	private List<MonetaryAmendment> amendments;
	
	private String mealCategoryCode;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}
}
