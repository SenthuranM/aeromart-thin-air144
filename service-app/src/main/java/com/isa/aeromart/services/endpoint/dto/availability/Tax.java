package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.List;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;

public class Tax {
	@CurrencyValue
	private BigDecimal amount;

	private List<String> applicablePaxTypes;

	private String carrierCode;

	private String taxCode;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public List<String> getApplicablePaxTypes() {
		return applicablePaxTypes;
	}

	public void setApplicablePaxTypes(List<String> applicablePaxTypes) {
		this.applicablePaxTypes = applicablePaxTypes;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

}
