package com.isa.aeromart.services.endpoint.dto.voucher;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;


public class VoucherRedeemRQ extends TransactionalBaseRQ {
	
	private VoucherRedeemRequest  voucherRedeemRequest;

	public VoucherRedeemRequest getVoucherRedeemRequest() {
		return voucherRedeemRequest;
	}

	public void setVoucherRedeemRequest(VoucherRedeemRequest voucherRedeemRequest) {
		this.voucherRedeemRequest = voucherRedeemRequest;
	}

}

