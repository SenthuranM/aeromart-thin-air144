package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.math.BigDecimal;

public class MonetaryAmendment {

	private Integer definitionId;

	private BigDecimal amount;

	public Integer getDefinitionId() {
		return definitionId;
	}

	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
