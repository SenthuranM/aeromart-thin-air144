package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.modification.FFIDChangePax;
import com.isa.aeromart.services.endpoint.dto.passenger.LMSPassenger;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.NameDTO;

public class UpdateValidateFFIDAdaptor implements Adaptor<List<FFIDChangePax>, ValidatePaxFFIDsRQ> {

	private LCCClientReservation reservation;
	private Map<Integer, NameDTO> nameChangeMap;

	public UpdateValidateFFIDAdaptor(LCCClientReservation reservation, Map<Integer, NameDTO> nameChangeMap) {
		this.reservation = reservation;
		this.nameChangeMap = nameChangeMap;
	}

	@Override
	public ValidatePaxFFIDsRQ adapt(List<FFIDChangePax> ffidChangePaxList) {
		ValidatePaxFFIDsRQ validatePaxFFIDsRQ = new ValidatePaxFFIDsRQ();
		List<LMSPassenger> lmsPassengers = new ArrayList<>();
		for (FFIDChangePax ffidChangePax : ffidChangePaxList) {
			for (LCCClientReservationPax passenger : reservation.getPassengers()) {
				if (passenger.getPaxSequence().intValue() == ffidChangePax.getPaxSeqNo()) {
					LMSPassenger lmsPassenger = new LMSPassenger();
					lmsPassenger.setFfid(ffidChangePax.getFfid());
					NameDTO nameChange = null;
					if (nameChangeMap != null) {
						nameChange = nameChangeMap.get(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()));
					}
					if (nameChange != null) {
						lmsPassenger.setFirstName(nameChange.getFirstname());
						lmsPassenger.setLastName(nameChange.getLastName());
					} else {
						lmsPassenger.setFirstName(passenger.getFirstName());
						lmsPassenger.setLastName(passenger.getLastName());
					}
					lmsPassenger.setPassengerSequence(passenger.getPaxSequence());
					lmsPassenger.setPaxType(passenger.getPaxType());
					lmsPassengers.add(lmsPassenger);
				}
			}
		}
		validatePaxFFIDsRQ.setLmsPaxDetails(lmsPassengers);
		return validatePaxFFIDsRQ;
	}

}
