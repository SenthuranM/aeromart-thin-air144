package com.isa.aeromart.services.endpoint.dto.availability;

public class FareRule {

	// booking class eg:- N1
	private String bookingClass;

	// origin destination code eg :- SHJ/CMB
	private String ondCode;

	// description about fare rule eg :- Normal fare rule
	private String comment;

	// sequence number of ond
	private int ondSequence;

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

}
