package com.isa.aeromart.services.endpoint.delegate.passenger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.dto.passenger.PaxDOBValidationDetails;
import com.isa.aeromart.services.endpoint.dto.passenger.PaxDOBValidationOp;
import com.isa.aeromart.services.endpoint.dto.passenger.PaxDOBValidationRequest;
import com.isa.aeromart.services.endpoint.dto.passenger.PaxDOBValidationResponse;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

@Service("paxDOBValidator")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaxDOBValidator {

	public PaxDOBValidationResponse getPaxValidationResponse(
			PaxDOBValidationRequest paxDOBValidationRequest) {

		PaxDOBValidationResponse resp = new PaxDOBValidationResponse();
		List<PaxDOBValidationOp> listOfOp = new ArrayList<PaxDOBValidationOp>();
		List<PaxDOBValidationDetails> paxDOBValidationDetailsList = paxDOBValidationRequest
				.getPaxDOBValidationDetailsList();
		Iterator it = paxDOBValidationDetailsList.iterator();
		while (it.hasNext()) {
			PaxDOBValidationDetails current = (PaxDOBValidationDetails) it
					.next();
			String currentPaxType = current.getPaxType();
			String firstName = current.getFirstName();
			java.util.Date dob = current.getDateOfBirth();
			String depDateString = current.getDepartureDate();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date depDate = null;
			try {
				depDate = formatter.parse(current.getDepartureDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
			
			if (current.getDateOfBirth() != null) {

				Iterator paxIterator = AirproxyModuleUtils.getGlobalConfig()
						.getPaxTypeMap().entrySet().iterator();
				while (paxIterator.hasNext()) {
					@SuppressWarnings("unchecked")
					Entry<String, PaxTypeTO> paxEntry = (Entry<String, PaxTypeTO>) paxIterator
							.next();

					String paxType = paxEntry.getKey();
					PaxTypeTO paxTypeTo = paxEntry.getValue();

					if (paxTypeTo.getDescription().equalsIgnoreCase(
							currentPaxType)) {

						Calendar birthDay = Calendar.getInstance();
						birthDay.setTime(dob);
						Calendar departureDay = Calendar.getInstance();
						departureDay.setTime(depDate);

						int yearsInBetween = departureDay.get(Calendar.YEAR)
								- birthDay.get(Calendar.YEAR);

						long diff = depDate.getTime() - dob.getTime();
						long daysInBetween = TimeUnit.DAYS.convert(diff,
								TimeUnit.MILLISECONDS);

						int cutOff = paxTypeTo.getCutOffAgeInYears();

						if (yearsInBetween < cutOff
								&& daysInBetween > getDaysLowerCutover(paxTypeTo)) {
							PaxDOBValidationOp output = new PaxDOBValidationOp();
							output.setIsValidPaxType(paxTypeTo.getDescription()
									.equals(currentPaxType));
							output.setFirstName(firstName);
							output.setPaxType(currentPaxType);
							listOfOp.add(output);
						}
						else
						{
							PaxDOBValidationOp output = new PaxDOBValidationOp();
							output.setIsValidPaxType(false);
							output.setFirstName(firstName);
							output.setPaxType(currentPaxType);
							listOfOp.add(output);
						}
					}
				}
			} else {
				PaxDOBValidationOp output = new PaxDOBValidationOp();
				output.setIsValidPaxType(true);
				output.setFirstName(firstName);
				output.setPaxType(currentPaxType);
				listOfOp.add(output);
			}

		}
		resp.setPaxDOBResp(listOfOp);
		return resp;
	}

	private int getDaysLowerCutover(PaxTypeTO paxtype) {
		if (paxtype.getAgeLowerBoundaryInDays() > 0) {
			return paxtype.getAgeLowerBoundaryInDays();
		} else {
			return paxtype.getAgeLowerBoundaryInMonths() * 356 / 12;
		}
	}

}
