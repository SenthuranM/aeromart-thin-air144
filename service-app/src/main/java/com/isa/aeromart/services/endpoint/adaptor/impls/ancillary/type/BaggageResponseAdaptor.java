package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.baggage.BaggageItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.baggage.BaggageItems;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class BaggageResponseAdaptor extends AncillariesResponseAdaptor {

	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability, LCCAncillaryAvailabilityInDTO.LCCAncillaryType.BAGGAGE);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	public List<OndUnit> getOndUnits(Object ancillaries, RPHGenerator rphGenerator) {
		List<OndUnit> ondUnits = new ArrayList<>();
		OndUnit ondUnit;

		LCCBaggageResponseDTO baggageResponse = (LCCBaggageResponseDTO) ancillaries;

		Map<String, List<String>> onds = new LinkedHashMap<>();
		List<String> segments;


		FlightSegmentTO flightSegmentTO;
		for (LCCFlightSegmentBaggagesDTO baggageSegment : baggageResponse.getFlightSegmentBaggages()) {
			if (baggageSegment.getBaggages() != null && !baggageSegment.getBaggages().isEmpty()) {
				flightSegmentTO = baggageSegment.getFlightSegmentTO();

				if (!onds.containsKey(flightSegmentTO.getBaggageONDGroupId())) {
					segments = new ArrayList<>();
					onds.put(flightSegmentTO.getBaggageONDGroupId(), segments);
				}

				segments = onds.get(flightSegmentTO.getBaggageONDGroupId());
				segments.add(rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber()));
			}

		}

		for (String ondId : onds.keySet()) {
			ondUnit = new OndUnit();
			ondUnit.setOndId(ondId);
			ondUnit.setFlightSegmentRPH(onds.get(ondId));
			ondUnits.add(ondUnit);
		}

		return ondUnits;
	}

	public AncillaryType toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		AncillaryType ancillaryType = new AncillaryType();
		LCCBaggageResponseDTO baggageResponse = (LCCBaggageResponseDTO) ancillaries;

		Set<String> onds = new HashSet<>();

		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.BAGGAGE.getCode());

		List<Provider> providers = new ArrayList<>();
		Provider provider = new SystemProvided();

		AvailableAncillary availableAncillary = new AvailableAncillary();

		List<AvailableAncillaryUnit> availableAncillaryUnits = new ArrayList<>();
		AvailableAncillaryUnit availableAncillaryUnit;
		OndScope ondScope;

		Applicability applicability = new Applicability();
		applicability.setApplicableType(Applicability.ApplicableType.PASSENGER.name());

		BaggageItems itemsGroup;
		List<BaggageItem> items;
		BaggageItem item;
		List<Charge> charges;
		Charge charge;
		FlightSegmentTO flightSegmentTO;
		List<LCCFlightSegmentBaggagesDTO> lccFlightSegmentBaggageList = baggageResponse.getFlightSegmentBaggages();
		Collections.sort(lccFlightSegmentBaggageList);
		
		for (LCCFlightSegmentBaggagesDTO baggageSegment : lccFlightSegmentBaggageList) {
			if (baggageSegment.getBaggages() != null && !baggageSegment.getBaggages().isEmpty()) {
				flightSegmentTO = baggageSegment.getFlightSegmentTO();

				if (!onds.contains(flightSegmentTO.getBaggageONDGroupId())) {
					ondScope = new OndScope();
					ondScope.setScopeType(Scope.ScopeType.OND);
					ondScope.setOndId(flightSegmentTO.getBaggageONDGroupId());

					availableAncillaryUnit = new AvailableAncillaryUnit();
					availableAncillaryUnit.setScope(ondScope);
					availableAncillaryUnit.setApplicability(applicability);

					itemsGroup = new BaggageItems();
					items = new ArrayList<>();

					boolean isDefaultSelected = false;
					for (LCCBaggageDTO baggage : baggageSegment.getBaggages()) {
						item = new BaggageItem();
						item.setItemId(AnciCommon.getBaggageId(baggage.getOndBaggageChargeId(), baggage.getBaggageName()));
						item.setItemName(baggage.getBaggageDescription());
						item.setDefaultBaggage(baggage.getDefaultBaggage().equals("Y"));

						if (baggage.getDefaultBaggage().equals("Y")) {
							isDefaultSelected = true;
						}

						charges = new ArrayList<>();
						charge = new Charge();
						charge.setChargeBasis(Charge.ChargeBasis.PER_PASSENGER.name());
						charge.setAmount(baggage.getBaggageCharge());
						charges.add(charge);
						item.setCharges(charges);

						items.add(item);
					}
					if (!isDefaultSelected && !items.isEmpty()) {
						items.get(0).setDefaultBaggage(true);
					}
					itemsGroup.setItems(items);
					availableAncillaryUnit.setItemsGroup(itemsGroup);
					onds.add(flightSegmentTO.getBaggageONDGroupId());
					availableAncillaryUnits.add(availableAncillaryUnit);
				}
			}
		}

		availableAncillary.setAvailableUnits(availableAncillaryUnits);
		provider.setAvailableAncillaries(availableAncillary);
		providers.add(provider);
		ancillaryType.setProviders(providers);

		return ancillaryType;
	}

	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {

        OndScope ondScope = (OndScope) scope;

		LCCBaggageDTO selectedBaggage = new LCCBaggageDTO();
		selectedBaggage.setBaggageName(AnciCommon.getBaggageName(item.getId()));
		selectedBaggage.setOndBaggageChargeId(AnciCommon.getBaggageOndChargeId(item.getId()));
        selectedBaggage.setOndBaggageGroupId(ondScope.getOndId());
        selectedBaggage.setBaggageCharge(item.getAmount());
        if (item.getQuantity() != null) {
            selectedBaggage.setSoldPieces(item.getQuantity());
        }

		return selectedBaggage;
	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

		LCCBaggageResponseDTO baggageResponse = (LCCBaggageResponseDTO) ancillaries;
		List<SessionFlightSegment> segments = transactionalAncillaryService.getSelectedSegments(transactionId);

		FlightSegmentTO flightSegmentTO;
		for (LCCFlightSegmentBaggagesDTO baggageSegment : baggageResponse.getFlightSegmentBaggages()) {
			flightSegmentTO = baggageSegment.getFlightSegmentTO();
			for(SessionFlightSegment sessionFlightSegment : segments){
				if(sessionFlightSegment.getFlightRefNumber().equals(flightSegmentTO.getFlightRefNumber())){
					sessionFlightSegment.setBaggageONDGroupId(flightSegmentTO.getBaggageONDGroupId());
				}
			}
		}
		transactionalAncillaryService.storeUpdatedSessionFlightSegment(transactionId, segments);
				
	}
}
