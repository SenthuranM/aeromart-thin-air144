package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;

public class PaymentOptionsRQ extends ServiceTaxRQ {

	private String pnr;

	private List<Passenger> paxInfo;

	private ReservationContact reservationContact;

	private String originCountryCode;

	private boolean binPromotion;

	private boolean voucherPaymentInProcess;

	public List<Passenger> getPaxInfo() {
		return paxInfo;
	}

	public void setPaxInfo(List<Passenger> paxInfo) {
		this.paxInfo = paxInfo;
	}

	public ReservationContact getReservationContact() {
		return reservationContact;
	}

	public void setReservationContact(ReservationContact reservationContact) {
		this.reservationContact = reservationContact;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isBinPromotion() {
		return binPromotion;
	}

	public void setBinPromotion(boolean binPromotion) {
		this.binPromotion = binPromotion;
	}

	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}

	public boolean isVoucherPaymentInProcess() {
		return voucherPaymentInProcess;
	}

	public void setVoucherPaymentInProcess(boolean voucherPaymentInProcess) {
		this.voucherPaymentInProcess = voucherPaymentInProcess;
	}

}