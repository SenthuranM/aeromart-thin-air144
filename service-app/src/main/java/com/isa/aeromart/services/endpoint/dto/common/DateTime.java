package com.isa.aeromart.services.endpoint.dto.common;

import java.util.Date;

public class DateTime {

	private Date local;

	private Date zulu;

	public Date getLocal() {
		return local;
	}

	public void setLocal(Date local) {
		this.local = local;
	}

	public Date getZulu() {
		return zulu;
	}

	public void setZulu(Date zulu) {
		this.zulu = zulu;
	}

}
