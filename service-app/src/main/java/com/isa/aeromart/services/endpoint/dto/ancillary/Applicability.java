package com.isa.aeromart.services.endpoint.dto.ancillary;

public class Applicability {

	public enum ApplicableType {
		ALL_PASSENGERS, ALL_SEGMENTS, PASSENGER
	}
	
	private String applicableType;

	public String getApplicableType() {
		return applicableType;
	}

	public void setApplicableType(String applicableType) {
		this.applicableType = applicableType;
	}
}
