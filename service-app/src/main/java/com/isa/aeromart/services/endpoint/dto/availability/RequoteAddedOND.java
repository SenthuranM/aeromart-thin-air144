package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.List;

public class RequoteAddedOND {

	private BasePreferences preferences;

	// new flight segment RPHs
	private List<String> toFlightSegRPH;

	public BasePreferences getPreferences() {
		return preferences;
	}

	public void setPreferences(BasePreferences preferences) {
		this.preferences = preferences;
	}

	public List<String> getToFlightSegRPH() {
		return toFlightSegRPH;
	}

	public void setToFlightSegRPH(List<String> toFlightSegRPH) {
		this.toFlightSegRPH = toFlightSegRPH;
	}

	
	
}
