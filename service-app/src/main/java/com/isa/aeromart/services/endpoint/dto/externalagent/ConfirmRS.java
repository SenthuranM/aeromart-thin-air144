package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class ConfirmRS extends BaseRS {
	private PaymentInfo paymentInfo;
	private PaymentInfo paymentInfoInAgentCurrency;
	private String orderId;
	private String paymentReferenceId;

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPaymentReferenceId() {
		return paymentReferenceId;
	}

	public void setPaymentReferenceId(String paymentReferenceId) {
		this.paymentReferenceId = paymentReferenceId;
	}

	public PaymentInfo getPaymentInfoInAgentCurrency() {
		return paymentInfoInAgentCurrency;
	}

	public void setPaymentInfoInAgentCurrency(PaymentInfo paymentInfoInAgentCurrency) {
		this.paymentInfoInAgentCurrency = paymentInfoInAgentCurrency;
	}

}
