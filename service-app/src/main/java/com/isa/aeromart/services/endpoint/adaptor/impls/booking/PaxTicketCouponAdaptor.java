package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.PaxSegmentTicketCoupons;
import com.isa.aeromart.services.endpoint.dto.booking.PaxTicketCoupons;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class PaxTicketCouponAdaptor implements Adaptor<LCCClientReservationPax, PaxTicketCoupons> {

	private RPHGenerator rphGenerator;

	public PaxTicketCouponAdaptor(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;
	}

	@Override
	public PaxTicketCoupons adapt(LCCClientReservationPax passenger) {

		PaxTicketCoupons paxTicketCoupons = new PaxTicketCoupons();

		SegTicketCouponAdaptor segTicketCouponAdaptor = new SegTicketCouponAdaptor(rphGenerator);

		List<PaxSegmentTicketCoupons> paxSegmentTicketCoupons = new ArrayList<>();

		AdaptorUtils.adaptCollection(passenger.geteTickets(), paxSegmentTicketCoupons, segTicketCouponAdaptor);

		paxTicketCoupons.setPaxSequence(passenger.getPaxSequence());
		paxTicketCoupons.setReservationSegmentTicketCoupons(paxSegmentTicketCoupons);
		return paxTicketCoupons;
	}

}
