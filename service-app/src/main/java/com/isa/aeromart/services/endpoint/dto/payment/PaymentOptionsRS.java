package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PaymentOptionsRS extends TransactionalBaseRS {
	private boolean hasPromoOption;
	private BinPromotionDetails binPromotionDetails;
	private PaymentOptions paymentOptions;

	public PaymentOptions getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(PaymentOptions paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

	public boolean isHasPromoOption() {
		return hasPromoOption;
	}

	public void setHasPromoOption(boolean hasPromoOption) {
		this.hasPromoOption = hasPromoOption;
	}

	public BinPromotionDetails getBinPromotionDetails() {
		return binPromotionDetails;
	}

	public void setBinPromotionDetails(BinPromotionDetails binPromotionDetails) {
		this.binPromotionDetails = binPromotionDetails;
	}

}
