package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.math.BigDecimal;

public class Charge {

	private BigDecimal amount;
	
	public enum ChargeBasis {
		ALL_PASSENGERS, PER_PASSENGER, ADULT, CHILD, INFANT, ALL_SEGMENTS, PER_SEGMENT
	}
	
	private String chargeBasis;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getChargeBasis() {
		return chargeBasis;
	}

	public void setChargeBasis(String chargeBasis) {
		this.chargeBasis = chargeBasis;
	}
}
