package com.isa.aeromart.services.endpoint.dto.modification;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class CancelReservationRQ extends TransactionalBaseRQ {

	private String pnr;

	private String version;
	
	private boolean groupPnr;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

}
