package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.Address;
import com.isa.aeromart.services.endpoint.dto.common.EmergencyContact;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;

public class CustomerContact {

	private Address address;

	private String emailAddress;

	private PhoneNumber mobileNumber;

	private PhoneNumber telephoneNumber;

	private PhoneNumber fax;

	private EmergencyContact emergencyContact;
	
	private String alternativeEmailId;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public PhoneNumber getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(PhoneNumber mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public PhoneNumber getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(PhoneNumber telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public PhoneNumber getFax() {
		return fax;
	}

	public void setFax(PhoneNumber fax) {
		this.fax = fax;
	}

	public EmergencyContact getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(EmergencyContact emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}

	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}

}
