package com.isa.aeromart.services.endpoint.dto.booking;

public class ChargeTemplate {
	
	private Category category = Category.DEFAULT;

	public enum Category {
		DEFAULT, FARE, TAX, SURCHRGE, ANCILLARY, MODIFICATION_CHARGE, CANCELLATION_CHARGE, TRANSACTION_FEE,PENALTY,DISCOUNT,
		CREDIT_BALANCE,BALANCE_TO_PAY, TOTAl_PAID_IN_BASE_CURRENCY, TOTAl_TICKET_PRICE_IN_SELECTED_CURRENCY
	}
	
	private String type;

	private String amount;

	private CurrencyType currencyType = CurrencyType.BASE;

	public enum CurrencyType {
		BASE, PAID, SELECTED;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public CurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(CurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	

}
