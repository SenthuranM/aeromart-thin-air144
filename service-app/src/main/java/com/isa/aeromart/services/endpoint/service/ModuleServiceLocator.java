package com.isa.aeromart.services.endpoint.service;

import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCreditServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.aircustomer.api.utils.AircustomerConstants;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.TranslationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airproxy.api.service.AirproxyAncillaryBD;
import com.isa.thinair.airproxy.api.service.AirproxyPassengerBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.service.AirproxySegmentBD;
import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.lccclient.api.service.LCCPaxCreditBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.service.VoucherTemplateBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

public final class ModuleServiceLocator {

	/**
	 * Gets the Reservation Query BD
	 * 
	 * @return ReservationQueryBD the ReservationQueryBD
	 */
	public final static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationQueryBD.SERVICE_NAME);
	}

	/**
	 * Gets the Reservation BD
	 * 
	 * @return ReservationBD the ReservationBD
	 */
	public final static ReservationBD getReservationBD() {
		return (ReservationBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	/**
	 * gets the CommonMasterBD
	 * 
	 * @return CommonMasterBD the CommonMasterBD
	 */
	public final static CommonMasterBD getCommoMasterBD() {
		return (CommonMasterBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				CommonMasterBD.SERVICE_NAME);
	}

	/**
	 * Gets the Global Config
	 * 
	 * @return GlobalConfig the GlobalConfig
	 */
	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public final static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) ModuleServiceLocator.lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME,
				PaymentBrokerBD.SERVICE_NAME);
	}

	/**
	 * Gets Schedule Service(Flight) BD
	 * 
	 * @return ScheduleBD the Flight schedule delegate
	 */
	public final static ScheduleBD getScheduleServiceBD() {
		return (ScheduleBD) ModuleServiceLocator.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) ModuleServiceLocator.lookupServiceBD(CryptoConstants.MODULE_NAME,
				CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) ModuleServiceLocator.lookupEJB3Service("airsecurity", SecurityBD.SERVICE_NAME);
	}

	public static AirCustomerServiceBD getCustomerBD() {
		return (AirCustomerServiceBD) ModuleServiceLocator.lookupEJB3Service("aircustomer", AirCustomerServiceBD.SERVICE_NAME);
	}

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getConfig(), "ibe",
				"ibe.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getConfig(), "ibe",
				"ibe.config.dependencymap.invalid");
	}

	/**
	 * Gets TravelAgent service fro Air Travel Agent
	 * 
	 * @return TravelAgentBD the Travel Agent delegate
	 */
	public final static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) ModuleServiceLocator.lookupEJB3Service("airtravelagents", TravelAgentBD.SERVICE_NAME);
	}

	public static LoyaltyCustomerServiceBD getLoyaltyCustomerBD() {
		return (LoyaltyCustomerServiceBD) ModuleServiceLocator.lookupEJB3Service("aircustomer",
				LoyaltyCustomerServiceBD.SERVICE_NAME);
	}

	public static LoyaltyCreditServiceBD getLoyaltyCreditBD() {
		return (LoyaltyCreditServiceBD) ModuleServiceLocator
				.lookupEJB3Service("aircustomer", LoyaltyCreditServiceBD.SERVICE_NAME);
	}

	/**
	 * gets the flight
	 * 
	 * @return FlightBD
	 */
	public final static FlightBD getFlightBD() {
		return (FlightBD) ModuleServiceLocator.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public final static LCCPaxCreditBD getLCCPaxCreditBD() {
		return (LCCPaxCreditBD) ModuleServiceLocator.lookupEJB3Service(LccclientConstants.MODULE_NAME,
				LCCPaxCreditBD.SERVICE_NAME);
	}

	public final static AirportBD getAirportBD() {
		return (AirportBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);

	}

	/**
	 * Get AirproxyReservationQuery BD
	 * 
	 * @return AirproxySearchAndQuoteBD the Proxy Search and Quote Delegate
	 */
	public final static AirproxyReservationQueryBD getAirproxySearchAndQuoteBD() {
		return (AirproxyReservationQueryBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyReservationQueryBD.SERVICE_NAME);
	}

	/**
	 * Get AirproxyAncillary BD
	 * 
	 * @return AirproxyAncillaryBD the Proxy Ancillary Delegate
	 */
	public final static AirproxyAncillaryBD getAirproxyAncillaryBD() {
		return (AirproxyAncillaryBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyAncillaryBD.SERVICE_NAME);
	}

	/**
	 * Get AirproxyAncillary BD
	 * 
	 * @return AirproxyAncillaryBD the Proxy Reservation Delegate
	 */
	public final static AirproxyReservationBD getAirproxyReservationBD() {
		return (AirproxyReservationBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyReservationBD.SERVICE_NAME);
	}

	/**
	 * Get AirproxyPassengerBD
	 * 
	 * @return AirproxyPassengerBD the Proxy Reservation Delegate
	 */
	public final static AirproxyPassengerBD getAirproxyPassengerBD() {
		return (AirproxyPassengerBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyPassengerBD.SERVICE_NAME);
	}

	/**
	 * Get AirproxySegmentBD
	 * 
	 * @return AirproxySegmentBD the Proxy Reservation Delegate
	 */
	public final static AirproxySegmentBD getAirproxySegmentBD() {
		return (AirproxySegmentBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxySegmentBD.SERVICE_NAME);

	}

	public final static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionManagementBD.SERVICE_NAME);
	}
	
	// for new ibe voucherBD
	
	public final static VoucherBD getVoucherBD() {
		return (VoucherBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				VoucherBD.SERVICE_NAME);
	}
	
	public final static VoucherTemplateBD getVoucherTemplateBD() {
		return (VoucherTemplateBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				VoucherTemplateBD.SERVICE_NAME);
	}
	
	
	

	/**
	 * Get LocationBD
	 * 
	 * @return LocationBD the location Delegate
	 */
	public final static LocationBD getLocationBD() {
		return (LocationBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);

	}

	public final static TranslationBD getTranslationBD() {
		return (TranslationBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, TranslationBD.SERVICE_NAME);
	}

	public final static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) ModuleServiceLocator.lookupEJB3Service(MessagingConstants.MODULE_NAME,
				MessagingServiceBD.SERVICE_NAME);
	}

	public final static PassengerBD getPassengerBD() {
		return (PassengerBD) ModuleServiceLocator.lookupEJB3Service(MessagingConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				LoyaltyManagementBD.SERVICE_NAME);
	}

	public final static LmsMemberServiceBD getLmsMemberServiceBD() {
		return (LmsMemberServiceBD) ModuleServiceLocator.lookupEJB3Service(AircustomerConstants.MODULE_NAME,
				LmsMemberServiceBD.SERVICE_NAME);

	}

	public final static WSClientBD getWSClientBD() {
		return (WSClientBD) ModuleServiceLocator.lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	private final static ServiceAppConfig getConfig() {
		return (ServiceAppConfig) LookupServiceFactory.getInstance().getBean("isa:base://modules/service-app?id=serviceConfig");
	}

	public final static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) ModuleServiceLocator.lookupEJB3Service(AirTravelAgentConstants.MODULE_NAME,
				TravelAgentFinanceBD.SERVICE_NAME);
	}
	
	public static AlertingBD getAlertingBD() {
		return (AlertingBD) ModuleServiceLocator.lookupServiceBD(AlertingConstants.MODULE_NAME,
				AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	public final static AirproxyVoucherBD getAirproxyVoucherBD() {
		AirproxyVoucherBD airproxyVoucherBD = null;
		airproxyVoucherBD = (AirproxyVoucherBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, AirproxyVoucherBD.SERVICE_NAME);
		return airproxyVoucherBD;
	}
}
