package com.isa.aeromart.services.endpoint.controller.modification;

import java.text.ParseException;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.modification.ModificationService;
import com.isa.aeromart.services.endpoint.dto.common.BaseRS;
import com.isa.aeromart.services.endpoint.dto.modification.FFIDUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.service.ValidationService;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

@Controller
@RequestMapping(value = "modification", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
public class AddEditFFIDController  extends StatefulController {

	@Autowired
	BookingService bookingService;
	
	@Autowired
	ValidationService validationService;

	@Autowired
	ModificationService modificationService;

	@ResponseBody
	@RequestMapping(value = "/addEditFFIDWithNameChange", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseRS addEditFFIDWithNameChange(@Validated @RequestBody FFIDUpdateRQ ffidUpdateRQ)
			throws ParseException, ModuleException {
		TransactionalBaseRS response = new TransactionalBaseRS();
		TrackInfoDTO trackInfo = getTrackInfo();
		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		String customerID = customerSessionStore.getLoggedInCustomerID();

		RequoteSessionStore requoteSessionStore = getRequoteSessionStore(ffidUpdateRQ.getTransactionId());

		LCCClientReservation reservation = bookingService.loadProxyReservation(requoteSessionStore.getResInfo().getPnr(),
				requoteSessionStore.getResInfo().isGroupPNR(), trackInfo, true,
				(customerID != null && !customerID.isEmpty()) ? true : false, null, null, AppSysParamsUtil.isPromoCodeEnabled());

		validationService.ffidAddEditValidate(ffidUpdateRQ, reservation, requoteSessionStore.getNameChangedPaxMap());
		requoteSessionStore.storeFFIDChangeRQ(ffidUpdateRQ);
		response.setSuccess(true);
		response.setTransactionId(ffidUpdateRQ.getTransactionId());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/addEditFFID", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseRS addEditFFIDWithoutNameChange(@Validated @RequestBody FFIDUpdateRQ ffidUpdateRQ)
			throws ParseException, ModuleException {
		TransactionalBaseRS response = new TransactionalBaseRS();
		TrackInfoDTO trackInfo = getTrackInfo();
		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		String customerID = customerSessionStore.getLoggedInCustomerID();
		
		LCCClientReservation reservation = bookingService.loadProxyReservation(ffidUpdateRQ.getPnr(), ffidUpdateRQ.isGroupPnr(), trackInfo, true,
				(customerID != null && !customerID.isEmpty()) ? true : false, null, null, AppSysParamsUtil.isPromoCodeEnabled());

		validationService.ffidAddEditValidate(ffidUpdateRQ, reservation, null);

		modificationService.modifyFFIDInfo(ffidUpdateRQ, reservation, getTrackInfo());
		
		response.setSuccess(true);
		response.setTransactionId(ffidUpdateRQ.getTransactionId());
		return response;
	}


	private RequoteSessionStore getRequoteSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

}
