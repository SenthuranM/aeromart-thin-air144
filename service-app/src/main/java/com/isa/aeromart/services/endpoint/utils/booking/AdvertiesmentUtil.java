package com.isa.aeromart.services.endpoint.utils.booking;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.webplatform.api.dto.CarLinkParamsTO;
import com.isa.thinair.webplatform.api.dto.HolidaysLinkParamsTO;
import com.isa.thinair.webplatform.api.util.ExternalLinkCreatorUtil;
import com.isa.thinair.webplatform.core.commons.MapGenerator;

public class AdvertiesmentUtil {

	private static Log log = LogFactory.getLog(AdvertiesmentUtil.class);

	private AdvertiesmentUtil() {
	}

	public static String createHolidaysURL(HolidaysLinkParamsTO holidaysLinkParamsTO) throws ModuleException {
		String holidaysURL = BeanUtils.nullHandler(AppSysParamsUtil.getHolidaysURL());
		if (holidaysURL.length() > 0 && AppSysParamsUtil.isShowHolidaysLink()) {
			if (holidaysURL.charAt(holidaysURL.length() - 1) != '&') {
				holidaysURL += "&";
			}
			holidaysLinkParamsTO.setStaticURL(holidaysURL);
			return ExternalLinkCreatorUtil.createLink(holidaysLinkParamsTO);
		} else {
			return ""; // should be empty for UI validation
		}
	}

	public static String createRentACarURL(String language, Set<LCCClientReservationSegment> resSegments,
			String currencyCode, String pnr) throws ModuleException {
		String carURL = BeanUtils.nullHandler(AppSysParamsUtil.getRentACarURL());
		if (carURL.length() > 0 && AppSysParamsUtil.isShowRentACarLink()) {
			if (AppSysParamsUtil.isRentACarLinkDynamic()) {
				carURL = getDynamicCarURL(language, resSegments, currencyCode, pnr, carURL);
			}
			return carURL;
		} else {
			return ""; // should be empty for UI validation
		}
	}

	private static String getDynamicCarURL(String language, Set<LCCClientReservationSegment> resSegments, String currencyCode,
			String pnr, String staticCarUrl) throws ModuleException {
		String carURL = staticCarUrl;
		CarLinkParamsTO carLinkParams = new CarLinkParamsTO();
		ArrayList<LCCClientReservationSegment> flightDepDetails = (ArrayList<LCCClientReservationSegment>) (getOutboundInboundList(
				resSegments, true)[0]);
		ArrayList<LCCClientReservationSegment> flightRetDetails = (ArrayList<LCCClientReservationSegment>) (getOutboundInboundList(
				resSegments, true)[1]);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
		String rtDateTime = "";
		String pkDateTime = getOutboundSegArrivalDate(carLinkParams, flightDepDetails, formatter);
		if (flightRetDetails != null && !flightRetDetails.isEmpty() && flightRetDetails.get(0) != null) {
			rtDateTime = formatter.format(flightRetDetails.get(0).getDepartureDate());
		}
		if (("").equals(rtDateTime)) {
			rtDateTime = pkDateTime;
		}
		carLinkParams.setRtDateTime(rtDateTime);
		if (language != null) {
			carLinkParams.setLang(language.toUpperCase());
		} else {
			carLinkParams.setLang(Locale.ENGLISH.getLanguage().toUpperCase());
		}
		if (currencyCode != null && currencyCode.length() > 0) {
			carLinkParams.setCurrency(currencyCode);
		}
		carLinkParams.setOrderId(pnr);
		if (carURL.charAt(carURL.length() - 1) != '?') {
			carURL += "?";
		}
		carLinkParams.setStaticURL(carURL);
		carURL = ExternalLinkCreatorUtil.createCarLink(carLinkParams);
		return carURL;
	}

	private static String getOutboundSegArrivalDate(CarLinkParamsTO carLinkParams, ArrayList<LCCClientReservationSegment> flightDepDetails,
			SimpleDateFormat formatter) throws ModuleException {
		String locCode;
		String pkDateTime = "";
		if (flightDepDetails != null && !flightDepDetails.isEmpty()) {
			LCCClientReservationSegment lastOutboundSeg = flightDepDetails.get(flightDepDetails.size() - 1);
			if (lastOutboundSeg != null) {
				long pkDateTimeLong = lastOutboundSeg.getArrivalDate().getTime() + 7200000; // 2 hours after arrival
				// time
				pkDateTime = formatter.format(new Date(pkDateTimeLong));
				carLinkParams.setPkDateTime(pkDateTime);
				String[] locCodes = lastOutboundSeg.getSegmentCode().split("/");
				locCode = locCodes[locCodes.length - 1];
				carLinkParams.setLocCode(locCode);
				Collection<String> colAirPortCodes = new ArrayList<>();
				colAirPortCodes.add(locCode);
				Map<String, CachedAirportDTO> mapAirports = new HashMap<>();
				if (colAirPortCodes.size() != mapAirports.size()) {
					mapAirports = ModuleServiceLocator.getAirportBD().getCachedAllAirportMap(colAirPortCodes);
					carLinkParams
							.setResidencyId(mapAirports.get(locCode) != null ? mapAirports.get(locCode).getCountryCode() : "");
				}
			}
		}
		return pkDateTime;
	}

	public static String createRentACarLinkForManageBooking(String language, Collection<FlightInfoTO> flights, String pnr)
			throws ModuleException {
		String carURL = BeanUtils.nullHandler(AppSysParamsUtil.getRentACarURL());
		if (carURL.length() > 0 && AppSysParamsUtil.isShowRentACarLink()) {
			if (AppSysParamsUtil.isRentACarLinkDynamic()) {
				CarLinkParamsTO carLinkParams = new CarLinkParamsTO();
				carLinkParams.setLang(language.toUpperCase());
				carLinkParams.setCurrency(AppSysParamsUtil.getBaseCurrency());
				String clientId = AppSysParamsUtil.getRentACarManageBookingClientId();
				if (clientId != null) {
					carLinkParams.setClientId(clientId);
				}
				carLinkParams.setOrderId(pnr);
				if (carURL.charAt(carURL.length() - 1) != '?') {
					carURL += "?";
				}
				carLinkParams.setStaticURL(carURL);
				carURL = ExternalLinkCreatorUtil.createCarLinkForManageBooking(flights, carLinkParams);
			}
			return carURL;
		} else {
			return ""; // should be empty for UI validation
		}
	}


	public static String getSegementDetails(String strSegement) {
		return getSegementDetails(strSegement, null);
	}

	private static String getSegementDetails(String strSegement, String subStationShortCode) {
		Map mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strSegement.split("/");
		for (int i = 0; i < strArrSegment.length; i++) {
			// Haider 4Feb09
			strSegements = concatSegment(subStationShortCode, mapAirports, strSegements, strArrSegment, i);
		}
		return strSegements;
	}

	private static String concatSegment(String subStationShortCode, Map mapAirports, String strSegements, String[] strArrSegment,
			int i) {
		String segmentString = strSegements;
		if (AppSysParamsUtil.isHideStopOverEnabled() && strArrSegment.length > 2 && i > 0 && i < (strArrSegment.length - 1))
			return segmentString;
		if (!("").equals(segmentString)) {
			segmentString += " / ";
		}
		if (mapAirports.get(strArrSegment[i]) != null) {
			if (AppSysParamsUtil.isGroundServiceEnabled() && subStationShortCode != null) {
				segmentString += SubStationUtil.getSubStationName(strArrSegment[i], subStationShortCode,
						mapAirports.get(strArrSegment[i]).toString());
			} else {
				segmentString += mapAirports.get(strArrSegment[i]);
			}
		} else {
			segmentString += strArrSegment[i];
		}
		return segmentString;
	}

	/**
	 * Generate segment code with terminal information.
	 * 
	 * @param strSegement
	 * @param subStationShortCode
	 * @param departureTerminal
	 * @param arrivalTerminal
	 * 
	 * @return
	 */
	public static String getSegementDetailsWithTermainals(String strSegement, String subStationShortCode,
			String departureTerminal, String arrivalTerminal) {
		Map mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strSegement.split("/");
		for (int i = 0; i < strArrSegment.length; i++) {

			strSegements = concatSegment(subStationShortCode, mapAirports, strSegements, strArrSegment, i);

			// add the departure terminal for the first segment.
			if (i == 0 && departureTerminal != null) {
				strSegements += " - " + departureTerminal;
			}

			// add the arrival terminal for the last segment.
			if (i == (strArrSegment.length - 1) && arrivalTerminal != null) {
				strSegements += " - " + arrivalTerminal;
			}
		}
		return strSegements;
	}

	public static String createTermsNCond(String local) throws ModuleException {
		Locale locale = new Locale(local == null ? "en" : local);
		return ReservationTemplateUtils.getTermsNConditions("TermsSummary_IBE", locale);
	}

	/*
	 * Terms & Cond
	 */
	public static String createTermsNCondFromXML(String strXMLFilePath) throws ModuleException {
		StringBuilder strbData = new StringBuilder();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilderFactory.setCoalescing(true);
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(strXMLFilePath);

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("terms");
			int intRecords = listOfRecords.getLength();
			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				strbData.append(menuNode.getFirstChild().getNodeValue());
			}
		} catch (SAXParseException err) {
			log.error("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId(), err);
		} catch (Exception t) {
			log.error(t);
		}
		return strbData.toString();
	}

	public static Object[] getOutboundInboundList(Set<LCCClientReservationSegment> flightSegmentTOs, boolean mergeToOutbound) {
		LCCClientReservationSegment[] clientReservationSegments = SortUtil.sortByDepDate(flightSegmentTOs);

		ArrayList<LCCClientReservationSegment> outboundFlightSegmentTOs = new ArrayList<>();
		ArrayList<LCCClientReservationSegment> inboundFlightSegmentTOs = new ArrayList<>();

		for (LCCClientReservationSegment flightSegmentTO : clientReservationSegments) {
			if (!ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(flightSegmentTO.getReturnFlag())) {
				outboundFlightSegmentTOs.add(flightSegmentTO);
			} else {
				if (mergeToOutbound) {
					outboundFlightSegmentTOs.add(flightSegmentTO);
				} else {
					inboundFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		}
		return new ArrayList[] { outboundFlightSegmentTOs, inboundFlightSegmentTOs };
	}

	public static String getHotelURLForAdvertiesment(LCCClientReservation reservation) {
		HolidaysLinkParamsTO holidaysLinkParamsTO = new HolidaysLinkParamsTO();
		holidaysLinkParamsTO.setLanguage(reservation.getPreferrenceInfo().getPreferredLanguage());
		holidaysLinkParamsTO.setPassengers(reservation.getPassengers());
		holidaysLinkParamsTO.setSegmentView(reservation.getSegments());
		try {
			return createHolidaysURL(holidaysLinkParamsTO);
		} catch (ModuleException e) {
			log.error("Error creating car link ==> AdvertiesmentUtil", e);
		}
		return null;
	}

	public static String getCarURLForAdvertiesment(LCCClientReservation reservation) {
		try {
			return createRentACarURL(reservation.getPreferrenceInfo().getPreferredLanguage(),
					reservation.getSegments(), reservation.getLastCurrencyCode(), reservation.getPNR());
		} catch (ModuleException e) {
			log.error("Error creating hotel link ==> AdvertiesmentUtil", e);
		}
		return null;
	}
}

