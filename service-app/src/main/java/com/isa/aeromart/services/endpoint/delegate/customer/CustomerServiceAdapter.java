package com.isa.aeromart.services.endpoint.delegate.customer;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.dto.customer.ConfirmCustomerRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ConfirmCustomerRS;
import com.isa.aeromart.services.endpoint.dto.customer.LMSConfirmationRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSConfirmationRS;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.aircustomer.api.dto.lms.LMSMemberDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSSmartButtonWebServiceInvoker;

@Service("customerService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CustomerServiceAdapter implements CustomerService {

	private static Log log = LogFactory.getLog(CustomerServiceAdapter.class);

	private String LOG_SEPERATOR = "###########################################################";

	@Override
	public LMSConfirmationRS confirmLMSMember(LMSConfirmationRQ lmsConfirmationRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		LMSConfirmationRS lmsConfirmationRS = new LMSConfirmationRS();
		lmsConfirmationRS.setSuccess(true);
		lmsConfirmationRS.setTransactionId(lmsConfirmationRQ.getTransactionId());
		if (AppSysParamsUtil.isLMSEnabled()) {
			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			int merges = lmsMemberDelegate.numberOfMerges(lmsConfirmationRQ.getValidationText(), lmsConfirmationRQ.getFfid());

			if (merges > 0) {
				AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
				Customer customer = customerDelegate.getCustomer(lmsConfirmationRQ.getFfid());
				executeOperation(lmsConfirmationRQ, trackInfoDTO, lmsConfirmationRS, customer);
			}
		}

		return lmsConfirmationRS;

	}

	public boolean registerLoyaltyCustomer(LoyaltyCustomerProfile loyaltyProfile)
			throws ModuleException {
		boolean mashreqSuccess = true;
		Customer modelCustomer = ModuleServiceLocator.getCustomerBD().getCustomer(loyaltyProfile.getCustomerId());
		if (AppSysParamsUtil.isIntegrateMashreqWithLMS() && modelCustomer.getIsLmsMember() == 'Y') {
			LmsMember lmsMember = ModuleServiceLocator.getLmsMemberServiceBD().getLmsMember(modelCustomer.getEmailId());
			ModuleServiceLocator.getWSClientBD().addMemberAccountId(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(),
					modelCustomer.getEmailId(), lmsMember.getMemberExternalId(), "Mid", loyaltyProfile.getLoyaltyAccountNo());

			log.debug("Successfully called Smart Button API to link Mashreq Account " + loyaltyProfile.getLoyaltyAccountNo()
					+ " and LMS account " + modelCustomer.getEmailId());
		} else {
			mashreqSuccess = false;
		}

		if (loyaltyProfile != null) {
			loyaltyProfile.setCustomerId(modelCustomer.getCustomerId());
			ModuleServiceLocator.getLoyaltyCustomerBD().saveOrUpdate(loyaltyProfile);
		} else {
			mashreqSuccess = false;
		}

		return mashreqSuccess;
	}

	public void saveOrUpdateLoyaltyCustomerProfile(LoyaltyCustomerProfile loyaltyCustomerProfile) throws ModuleException {
		log.info(LOG_SEPERATOR);
		log.info("Saving/Updating LoyaltyCustomerProfile");
		log.info("Account no : " + loyaltyCustomerProfile.getLoyaltyAccountNo());
		log.info("Email : " + loyaltyCustomerProfile.getEmail());
		LoyaltyCustomerServiceBD loyaltyCustomerServiceBD = ModuleServiceLocator.getLoyaltyCustomerBD();
		loyaltyCustomerServiceBD.saveOrUpdate(loyaltyCustomerProfile);
		log.info("Finished Saving/Updating LoyaltyCustomerProfile");
		log.info(LOG_SEPERATOR);
	}

	public boolean isLoyaltyCustomer(int customerID) throws ModuleException {
		return getCustomerProfileByCustomerID(customerID) != null ? true : false;
	}

	public LoyaltyCustomerProfile getCustomerProfileByCustomerID(int customerID) throws ModuleException {
		log.info(LOG_SEPERATOR);
		log.info("Retrieving LoyaltyCustomerProfile");
		LoyaltyCustomerServiceBD loyaltyCustomerServiceBD = ModuleServiceLocator.getLoyaltyCustomerBD();
		LoyaltyCustomerProfile loyaltyCustomerProfile = loyaltyCustomerServiceBD.getLoyaltyCustomerByCustomerId(customerID);
		if(loyaltyCustomerProfile != null){
			log.info("Account no : " + loyaltyCustomerProfile.getLoyaltyAccountNo());
			log.info("Email : " + loyaltyCustomerProfile.getEmail());
		}
		log.info("Finished Retrieving LoyaltyCustomerProfile");
		log.info(LOG_SEPERATOR);
		return loyaltyCustomerProfile;
	}

	private void executeOperation(LMSConfirmationRQ lmsConfirmationRQ, TrackInfoDTO trackInfoDTO,
			LMSConfirmationRS lmsConfirmationRS, Customer customer) throws ModuleException {
		LmsMember lmsMember = customer.getLMSMemberDetails();
		LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();

		if (lmsMember != null) {
			LoyaltyMemberCoreDTO existingMember = lmsManagementBD.getLoyaltyMemberCoreDetails(lmsMember.getFfid());
			if (existingMember.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				lmsConfirmationRS.setOperation(LMSConfirmationRS.Operetion.CONFIRM);
				String memberExternalId = lmsManagementBD.createLoyaltyMember(customer);

				lmsMember = customer.getLMSMemberDetails();
				//lmsMember.setSbInternalId(sbInternalId);
				lmsMember.setMemberExternalId(memberExternalId);
				lmsMember.setEmailConfirmed('Y');
				lmsMemberDelegate.saveOrUpdate(lmsMember);
				customerDelegate.confirmLMSMemberStatus(customer.getEmailId());
				lmsMemberDelegate.mergeLmsMember(lmsConfirmationRQ.getValidationText(), lmsConfirmationRQ.getFfid());
				lmsConfirmationRS.setOperationSuccess(true);
				lmsConfirmationRS.setValid(true);
			} else {
				lmsConfirmationRS.setOperation(LMSConfirmationRS.Operetion.MERGE);
				LMSMemberDTO lmsMemberDTO = ModuleServiceLocator.getLoyaltyManagementBD()
						.getMergeLoyaltyMember(lmsConfirmationRQ.getFfid(), trackInfoDTO);
				lmsMember = lmsMemberDelegate.getLmsMember(lmsConfirmationRQ.getFfid());
				if (lmsMemberDTO != null) {
					mergeLMSMember(lmsConfirmationRQ, lmsConfirmationRS, customer, lmsMember, lmsMemberDTO);
				} else {
					lmsConfirmationRS.setNameMatch(false);
				}
			}
		}
	}

	private void mergeLMSMember(LMSConfirmationRQ lmsConfirmationRQ, LMSConfirmationRS lmsConfirmationRS, Customer customer,
			LmsMember lmsMember, LMSMemberDTO lmsMemberDTO) throws ModuleException {
		LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		if (lmsMemberDTO.getFirstName().equalsIgnoreCase(lmsMember.getFirstName())
				&& lmsMemberDTO.getLastName().equalsIgnoreCase(lmsMember.getLastName())) {
			lmsMember.setSbInternalId(lmsMemberDTO.getSbInternalId());
			lmsMember.setMemberExternalId(lmsMemberDTO.getMemberExternalId());
			lmsMember.setEnrollingCarrier(lmsMemberDTO.getEnrollingCarrier());
			lmsMember.setEnrollingChannelExtRef(lmsMemberDTO.getEnrollingChannelExtRef());
			lmsMember.setEnrollingChannelIntRef(lmsMemberDTO.getEnrollingChannelIntRef());
			
			lmsMember.setEmailConfirmed('Y');
			lmsMember.setPassword(lmsMemberDTO.getPassword());
			lmsMemberDelegate.saveOrUpdate(lmsMember);
			if (lmsMemberDTO.getConfirmed() == 'Y') {
				LmsCommonUtil.sendLMSMergeSuccessEmail(lmsMember, AppSysParamsUtil.getDefaultCarrierCode(),
						lmsMemberDelegate);
			}
			customerDelegate.confirmLMSMemberStatus(customer.getEmailId());
			lmsMemberDelegate.mergeLmsMember(lmsConfirmationRQ.getValidationText(),
					lmsConfirmationRQ.getFfid());
			lmsConfirmationRS.setOperationSuccess(true);
			lmsConfirmationRS.setValid(true);
		} else {
			lmsConfirmationRS.setNameMatch(false);
			lmsConfirmationRS.setValid(true);
		}
	}

	@Override
	public ConfirmCustomerRS confirmCustomer(ConfirmCustomerRQ confirmCustomerRQ, TrackInfoDTO trackInfo)
			throws ModuleException {
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		ConfirmCustomerRS confirmCustomerRS = new ConfirmCustomerRS();
		confirmCustomerRS.setTransactionId(confirmCustomerRQ.getTransactionId());
		boolean confirmed;
		try {
			confirmed = customerDelegate.confirmCustomer(confirmCustomerRQ.getEmailId(),
					URLDecoder.decode(confirmCustomerRQ.getVlidationText(), "UTF-8"));
			confirmCustomerRS.setSuccess(true);
		} catch (UnsupportedEncodingException e) {
			confirmed = false;
		}

		if (confirmed == false) {
			confirmed = customerDelegate.confirmCustomer(confirmCustomerRQ.getEmailId(), confirmCustomerRQ.getVlidationText());
		}

		if (confirmed == false) {
			confirmed = customerDelegate.confirmCustomer(confirmCustomerRQ.getEmailId(),
					confirmCustomerRQ.getVlidationText().replace(" ", "+"));
		}
		confirmCustomerRS.setCustomerConfirmed(confirmed);
		return confirmCustomerRS;
	}

}
