package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class AncillaryType {
	
	private String ancillaryType;

	private List<Provider> providers;

	public String getAncillaryType() {
		return ancillaryType;
	}

	public void setAncillaryType(String ancillaryType) {
		this.ancillaryType = ancillaryType;
	}

	public List<Provider> getProviders() {
		return providers;
	}

	public void setProviders(List<Provider> providers) {
		this.providers = providers;
	}
}
