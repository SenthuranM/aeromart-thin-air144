package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Set;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;

public class CustomerLoginRS extends TransactionalBaseRS{
	
	private LoggedInCustomerDetails loggedInCustomerDetails = null;
        
    private LoggedInLmsDetails loggedInLmsDetails = null;
    
    private Set<ReservationDetails> reservationList = null;
      
    private boolean loyaltyManagmentEnabled = false;
    
	private boolean hasMashreqLoyalty = false;

    private double totalCustomerCredit;
    
    private String authToken = null;
    
    private String lmsRemoteId = null;

	private UserTokenDetails userToken;

	private IPGPaymentOptionDTO iPGPaymentOptionDTO = null;

	public LoggedInCustomerDetails getLoggedInCustomerDetails() {
		return loggedInCustomerDetails;
	}

	public void setLoggedInCustomerDetails(
			LoggedInCustomerDetails loggedInCustomerDetails) {
		this.loggedInCustomerDetails = loggedInCustomerDetails;
	}

	public LoggedInLmsDetails getLoggedInLmsDetails() {
		return loggedInLmsDetails;
	}

	public void setLoggedInLmsDetails(LoggedInLmsDetails loggedInLmsDetails) {
		this.loggedInLmsDetails = loggedInLmsDetails;
	}

	public Set<ReservationDetails> getReservationList() {
		return reservationList;
	}

	public void setReservationList(Set<ReservationDetails> reservationList) {
		this.reservationList = reservationList;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

	public void setTotalCustomerCredit(double totalCustomerCredit) {
		this.totalCustomerCredit = totalCustomerCredit;
	}
	
	public double getTotalCustomerCredit() {
		return totalCustomerCredit;
	}

	public boolean isHasMashreqLoyalty() {
		return hasMashreqLoyalty;
	}

	public void setHasMashreqLoyalty(boolean hasMashreqLoyalty) {
		this.hasMashreqLoyalty = hasMashreqLoyalty;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getLmsRemoteId() {
		return lmsRemoteId;
	}

	public void setLmsRemoteId(String lmsRemoteId) {
		this.lmsRemoteId = lmsRemoteId;
	}
		public UserTokenDetails getUserToken() {
		return userToken;
	}

	public void setUserToken(UserTokenDetails userToken) {
		this.userToken = userToken;
	}

	public IPGPaymentOptionDTO getiPGPaymentOptionDTO() {
		return iPGPaymentOptionDTO;
	}

	public void setiPGPaymentOptionDTO(IPGPaymentOptionDTO iPGPaymentOptionDTO) {
		this.iPGPaymentOptionDTO = iPGPaymentOptionDTO;
	}
}
