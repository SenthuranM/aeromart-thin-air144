package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Set;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;


public class TravelHistoryResponse extends BaseRS{
	
	private Set<ReservationDetails> ReservationDetails = null;

	public Set<ReservationDetails> getReservationDetails() {
		return ReservationDetails;
	}

	public void setReservationDetails(Set<ReservationDetails> reservationDetails) {
		ReservationDetails = reservationDetails;
	}

}
