package com.isa.aeromart.services.endpoint.dto.ancillary;

public class SelectedItem {

	private String id;
	
	private Integer quantity;

	private AncillaryInput input;

	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public AncillaryInput getInput() {
		return input;
	}

	public void setInput(AncillaryInput input) {
		this.input = input;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
