package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.Surcharge;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;

public class SurchargeAdaptor implements Adaptor<SurchargeTO, Surcharge> {

	@Override
	public Surcharge adapt(SurchargeTO source) {
		Surcharge target = new Surcharge();
		target.setSurchargeCode(source.getSurchargeCode());
		target.setAmount(source.getAmount());
		target.setApplicablePaxTypes(source.getApplicablePassengerTypeCode());
		target.setCarrierCode(source.getCarrierCode());
		return target;
	}

}
