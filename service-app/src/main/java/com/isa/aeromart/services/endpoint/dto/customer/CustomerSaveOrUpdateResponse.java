package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class CustomerSaveOrUpdateResponse extends BaseRS{

	private boolean autoLogin = false;

	public boolean isAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(boolean autoLogin) {
		this.autoLogin = autoLogin;
	}
	
}
