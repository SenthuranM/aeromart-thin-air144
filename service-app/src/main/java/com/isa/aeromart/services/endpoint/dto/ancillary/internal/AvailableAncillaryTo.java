package com.isa.aeromart.services.endpoint.dto.ancillary.internal;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;

public class AvailableAncillaryTo {

    private MetaData metaData;

    private List<AncillaryType> ancillaries;

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public List<AncillaryType> getAncillaries() {
        return ancillaries;
    }

    public void setAncillaries(List<AncillaryType> ancillaries) {
        this.ancillaries = ancillaries;
    }
}
