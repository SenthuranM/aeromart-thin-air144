package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationSegmentLite;
import com.isa.aeromart.services.endpoint.dto.common.DateTime;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;

public class ReservationSegmentLiteAdaptor implements Adaptor<LCCClientReservationSegment, ReservationSegmentLite> {

	private final static String YES = "Y";
	
	@Override
	public ReservationSegmentLite adapt(LCCClientReservationSegment reservationSegment) {
		ReservationSegmentLite reservationSegmentLite = new ReservationSegmentLite();
		String departureAirportTerminal = "";
		DateTime arrivalDateTime = new DateTime();
		arrivalDateTime.setLocal(reservationSegment.getArrivalDate());
		arrivalDateTime.setZulu(reservationSegment.getArrivalDateZulu());
		reservationSegmentLite.setArrivalDateTime(arrivalDateTime);

		DateTime departureDateTime = new DateTime();
		departureDateTime.setLocal(reservationSegment.getDepartureDate());
		departureDateTime.setZulu(reservationSegment.getDepartureDateZulu());
		reservationSegmentLite.setDepartureDateTime(departureDateTime);

		reservationSegmentLite.setFlightDesignator(reservationSegment.getFlightNo());
		if (reservationSegment.getFareTO() != null) {
			reservationSegmentLite.setOndSequence(reservationSegment.getFareTO().getOndSequence());
			reservationSegmentLite.setTotalChargeAmount(reservationSegment.getFareTO().getAmount().toString());
		}
		reservationSegmentLite.setOperatingCarrier(reservationSegment.getCarrierCode());
		reservationSegmentLite.setReservationSegmentRPH(reservationSegment.getSegmentSeq().toString());
		reservationSegmentLite.setSegmentCode(reservationSegment.getSegmentCode());
		reservationSegmentLite.setSegmentSequence(reservationSegment.getSegmentSeq());
		reservationSegmentLite.setStatus(reservationSegment.getStatus());
		reservationSegmentLite.setSubStatus(reservationSegment.getSubStatus());
		departureAirportTerminal += reservationSegment.getDepartureAirportName();
		
		if(reservationSegment.getDepartureTerminalName() != null && !reservationSegment.getDepartureTerminalName().isEmpty()){
			departureAirportTerminal += " " + reservationSegment.getDepartureTerminalName();
		}
		reservationSegmentLite.setDepartureAirportTerminalName(departureAirportTerminal);
		if (reservationSegment.getReturnFlag() != null) {
			reservationSegmentLite.setReturnSegment(YES.equals(reservationSegment.getReturnFlag()) ? true : false);
		}
		reservationSegmentLite.setFlexiAvailable(reservationSegment.getFlexiRuleID() != null);
		
		return reservationSegmentLite;
	}

}
