package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemComponent;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.InFlightSsrInput;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class OneToOneModification extends DefaultAncillarySelectionStrategy {

    @Override
	public PricedAncillaryType getDefaultPricedAncillaryType(PricedAncillaryType prevPricedAncillaryType,
			AncillaryType availableAncillaryType, Transition<List<Scope>> transition) {

		PricedAncillaryType pricedAncillaryType = new PricedAncillaryType();
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		AncillaryScope ancillaryScope;
		PricedSelectedItem defaultItem;

		List<Transition<Scope>> sortedScope = prepareScopes(transition);
		AncillariesConstants.AncillaryType ancillaryType = AncillariesConstants.AncillaryType
				.resolveAncillaryType(availableAncillaryType.getAncillaryType());

		Scope prevScope;
		Scope newScope;

		List<PricedSelectedItem> prevItems;
		AncillaryItemComponent availableItems;
		AncillaryItem equivalentItem;

		for (Transition<Scope> scope : sortedScope) {
			prevScope = scope.getOld();
			newScope = scope.getNew();

			ancillaryScope = new AncillaryScope();
			ancillaryScope.setScope(newScope);
			ancillaryScope.setAncillaries(new ArrayList<>());

			prevItems = getPreviousAncillaryItems(prevPricedAncillaryType, prevScope);
			availableItems = getAvailableAncillaryItems(availableAncillaryType, newScope);

			if (prevScope.equals(newScope)) {
				// update previous items with newest amounts
				for (PricedSelectedItem prevItem : prevItems) {
					AncillaryItemLeaf itemsLeaf = (AncillaryItemLeaf) availableItems;
					for (AncillaryItem ancillaryItem : itemsLeaf.getItems()) {
						if (prevItem.getId().equals(ancillaryItem.getItemId()) && ancillaryItem.getCharges().get(0).getAmount() != null) {
							prevItem.setAmount(ancillaryItem.getCharges().get(0).getAmount());
							ancillaryScope.getAncillaries().add(prevItem);
							break;
						}
					}
				}
			} else {
				for (PricedSelectedItem prevItem : prevItems) {
					equivalentItem = getEquivalentItem(ancillaryType, prevItem, availableItems);
					if (equivalentItem != null) {
						defaultItem = new PricedSelectedItem();
						defaultItem.setId(equivalentItem.getItemId());
						defaultItem.setName(prevItem.getName());
						defaultItem.setQuantity(prevItem.getQuantity()); // TODO -- inventory validation
						defaultItem.setMealCategoryCode(prevItem.getMealCategoryCode());
						if(prevItem.getInput()!= null && prevItem.getInput() instanceof InFlightSsrInput) {
							InFlightSsrInput previousInput = (InFlightSsrInput) prevItem.getInput();
							InFlightSsrInput input = new InFlightSsrInput();
							input.setComment(previousInput.getComment());
							defaultItem.setInput(input);
						}						
						// TODO -- get relevant charge
						if (!equivalentItem.getCharges().isEmpty()) {
							defaultItem.setAmount(equivalentItem.getCharges().iterator().next().getAmount());
						} else {
							defaultItem.setAmount(new BigDecimal(0));
						}

						ancillaryScope.getAncillaries().add(defaultItem);
					}
				}

			}
			if (!ancillaryScope.getAncillaries().isEmpty()) {
				pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
			}

		}

		return pricedAncillaryType;
	}
    
}
