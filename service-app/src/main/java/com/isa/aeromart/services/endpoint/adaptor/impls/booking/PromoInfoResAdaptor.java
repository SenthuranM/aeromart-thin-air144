package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.PromoTypeInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;

public class PromoInfoResAdaptor implements Adaptor<LCCPromotionInfoTO, PromoTypeInfo> {

	@Override
	public PromoTypeInfo adapt(LCCPromotionInfoTO lccPromotionInfoTO) {
		PromoTypeInfo promoTypeInfo = new PromoTypeInfo();
		if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
			promoTypeInfo.setCreditPromo(true);
		} else if (DiscountApplyAsTypes.MONEY.equals(lccPromotionInfoTO.getDiscountAs())) {
			promoTypeInfo.setMoneyPromo(true);
		}
		return promoTypeInfo;
	}

}
