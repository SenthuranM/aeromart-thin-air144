package com.isa.aeromart.services.endpoint.adaptor.impls.payment;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.payment.Card;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;

public class CardAdaptor implements Adaptor<CountryPaymentCardBehaviourDTO, Card> {

	@Override
	public Card adapt(CountryPaymentCardBehaviourDTO cardInfo) {

		Card card = new Card();
		card.setCardType(cardInfo.getCardType());
		card.setDisplayName(cardInfo.getCardDisplayName());
		card.setCardName(cardInfo.getCardDisplayName());
		card.setCssClassName(cardInfo.getCssClassName());
		card.setI18nMsgKey(cardInfo.getI18nMsgKey());
		card.setAllowedInModification(cardInfo.isModicifationAllowed());

		return card;
	}

}
