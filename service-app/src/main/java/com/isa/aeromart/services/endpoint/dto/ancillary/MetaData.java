package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class MetaData {
	
	private List<OndUnit> ondPreferences;
	
	private List<MonetaryAmendmentDefinition> monetaryAmendmentDefinitions;

	public List<OndUnit> getOndPreferences() {
		return ondPreferences;
	}

	public void setOndPreferences(List<OndUnit> ondPreferences) {
		this.ondPreferences = ondPreferences;
	}

	public List<MonetaryAmendmentDefinition> getMonetaryAmendmentDefinitions() {
		return monetaryAmendmentDefinitions;
	}

	public void setMonetaryAmendmentDefinitions(List<MonetaryAmendmentDefinition> monetaryAmendmentDefinitions) {
		this.monetaryAmendmentDefinitions = monetaryAmendmentDefinitions;
	}
}
