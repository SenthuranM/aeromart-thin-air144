package com.isa.aeromart.services.endpoint.service;

import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionRQ;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;

public interface PaymentValidationService {

	public void vlaidateCreatePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException;
	
	public void validateCreateEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException;
	
	public void validateCreateMakePaymentRQ(MakePaymentRQ makePaymentRQ) throws ValidationException;
	
	public void vlaidateBalancePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException;
	
	public void validateBalanceEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException;
	
	public void validateBalanceMakePaymentRQ(MakePaymentRQ makePaymentRQ) throws ValidationException;
	
	public void validateReqouteMakePaymentRQ(MakePaymentRQ makePaymentRQ) throws ValidationException;
	
	public void vlaidateReqoutePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException;
	
	public void validateReqouteEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException;
	
	public void vlaidateModifyPaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException;
	
	public void validateModifyEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException;
	
	public void validateModifyMakePaymentRQ(MakePaymentRQ makePaymentRQ) throws ValidationException;
	
	public void vlaidateConfirmPaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException;
	
	public void validateConfirmEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException;
	
	public void validateConfirmMakePaymentRQ(MakePaymentRQ makePaymentRQ) throws ValidationException;
	
	public void validateBinPromotionRQ(BinPromotionRQ binPromotionRQ);
	
}
