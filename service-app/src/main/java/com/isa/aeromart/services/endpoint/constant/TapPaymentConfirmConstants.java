package com.isa.aeromart.services.endpoint.constant;

public class TapPaymentConfirmConstants {
	
	public static final String ORDER_NUMBER = "OrdNo";
	
	public static final String RESPONSE_CODE = "ResCode";
	
	public static final String RESPONSE_MESSAGE = "ResMsg";
	
	public static final String PAY_MODE = "Paymode";
	
	public static final String PAY_REFERENCE_ID = "PayRefID";
	
	public static final String TAP_REFERENCE_ID = "TapRefID";
	
	public static final String ORDER_DONE_BY = "OrderPlacedBy";

}
