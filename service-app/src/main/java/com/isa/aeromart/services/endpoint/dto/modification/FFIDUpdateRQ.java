package com.isa.aeromart.services.endpoint.dto.modification;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class FFIDUpdateRQ extends TransactionalBaseRQ {

	@NotNull
	private String pnr;

	@NotNull
	private List<FFIDChangePax> ffidChangePaxList;

	private boolean groupPnr;

	public FFIDUpdateRQ() {
		super();
		this.ffidChangePaxList = new ArrayList<>();
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<FFIDChangePax> getFfidChangePaxList() {
		return ffidChangePaxList;
	}

	public void setFfidChangePaxList(List<FFIDChangePax> ffidChangePaxList) {
		this.ffidChangePaxList = ffidChangePaxList;
	}
}
