package com.isa.aeromart.services.endpoint.dto.modification;

public class ChargeDescriptionInfo {

	private String displayDescription;

	private String displayNewCharge;

	private String displayOldCharge;

	private boolean enableDisplay = true;

	public String getDisplayDescription() {
		return displayDescription;
	}

	public void setDisplayDescription(String displayDescription) {
		this.displayDescription = displayDescription;
	}

	public String getDisplayNewCharge() {
		return displayNewCharge;
	}

	public void setDisplayNewCharge(String displayNewCharge) {
		this.displayNewCharge = displayNewCharge;
	}

	public String getDisplayOldCharge() {
		return displayOldCharge;
	}

	public void setDisplayOldCharge(String displayOldCharge) {
		this.displayOldCharge = displayOldCharge;
	}

	public boolean isEnableDisplay() {
		return enableDisplay;
	}

	public void setEnableDisplay(boolean enableDisplay) {
		this.enableDisplay = enableDisplay;
	}

}
