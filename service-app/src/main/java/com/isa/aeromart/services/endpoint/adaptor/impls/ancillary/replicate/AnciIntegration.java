package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateReservationTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryUpdateRequestTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.PostPaymentTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.PrePaymentRequestTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.PrePaymentTO;
import com.isa.aeromart.services.endpoint.dto.common.Address;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRS;
import com.isa.aeromart.services.endpoint.dto.modification.ChargeBreakdownTemplate;
import com.isa.aeromart.services.endpoint.dto.modification.ChargeDescriptionInfo;
import com.isa.aeromart.services.endpoint.dto.modification.ChargeSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.CommonSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.PassengerInfo;
import com.isa.aeromart.services.endpoint.dto.modification.PassengerSummary;
import com.isa.aeromart.services.endpoint.dto.modification.StateSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.UpdatedChargeForDisplay;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionContactInformation;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.booking.BookingUtil;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.aeromart.services.endpoint.utils.common.LableServiceUtil;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaxPaymentUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;


public class AnciIntegration {
	
	private static final String JN_ANCI_CHARGE_CODE = "JNANCI";
	
	private static final String SERVICE_TAX_NAME_PREFIX = "SERVICE TAX FOR ";
	
	private static final String LANGUAGE = LocaleContextHolder.getLocale().getLanguage();
	
	private static final String AIRPORT_SERVICE_LABEL = LableServiceUtil.getBalanceSummaryDescription(
			"PgPriceBreakDown_lblHalaSelection", LANGUAGE);
	private static final String INFLIGHT_SERVICE_LABEL = LableServiceUtil.getBalanceSummaryDescription(
			"PgPriceBreakDown_lblSSRSelection", LANGUAGE);
	private static final String SEAT_LABEL = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblSeatSelection",
			LANGUAGE);
	private static final String AUTOCHECKIN_LABEL = LableServiceUtil.getBalanceSummaryDescription(
			"PgPriceBreakDown_lblAutoCheckinSelection", LANGUAGE);
	private static final String BAGGAGE_LABEL = LableServiceUtil.getBalanceSummaryDescription(
			"PgPriceBreakDown_lblBaggageSelection", LANGUAGE);
	private static final String MEAL_LABEL = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblMealSelection",
			LANGUAGE);
	private static final String INSURANCE_LABEL = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblInsurance",
			LANGUAGE);
	private static final String ANCI_PENALTY_LABEL = LableServiceUtil.getBalanceSummaryDescription(
	"PgPriceBreakDown_lblAnciPenalty", LANGUAGE);
	private static final String JN_ANCI_LABEL = LableServiceUtil.getBalanceSummaryDescription(
			"PgPriceBreakDown_lblJnTaxForAnci", LANGUAGE);
	private static final String BALANCE_TO_PAY = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblBalanceToPay",
			LANGUAGE);
	private static final String SERVICE_TAX_LABEL = LableServiceUtil.getBalanceSummaryDescription(
			"PgPriceBreakDown_lblServiceTaxForAnci", LANGUAGE);
	
    private AnciIntegration() {
    }


    public static PrePaymentTO getPrePaymentTo(PrePaymentRequestTO prePaymentRequestTo) {

        PrePaymentTO prePaymentTo = new PrePaymentTO();
        BigDecimal paymentDue = AccelAeroCalculator.getDefaultBigDecimalZero();
        String passengerRph;
        BigDecimal passengerCredit;
        Map<String, BigDecimal> paxCreditMap;
        BigDecimal passengerLevelBalance;

        ReservationInfo reservationInfo = prePaymentRequestTo.getReservationInfo();
        String pnr = reservationInfo.getPnr();

        if (reservationInfo.getPnrPaxCreditMap().containsKey(pnr)) {
            paxCreditMap = reservationInfo.getPnrPaxCreditMap().get(pnr);
        } else {
            paxCreditMap = new HashMap<>();
        }

        AncillaryIntegrateReservationTO ancillaryReservation = prePaymentRequestTo.getAncillaryIntegrateTo().getReservation();

        for (AncillaryIntegratePassengerTO passenger : ancillaryReservation.getPassengers()) {
            passengerRph = passenger.getPassengerRph();

            passengerCredit = BigDecimal.ZERO;
            if (paxCreditMap.containsKey(passengerRph)) {
                passengerCredit = paxCreditMap.get(passengerRph);
            }      
            passengerLevelBalance = AccelAeroCalculator.add(passenger.getNetAmount(), passengerCredit);

            if (passengerLevelBalance.compareTo(BigDecimal.ZERO) > 0) {
                paymentDue = paymentDue.add(passengerLevelBalance);
            }

        }

        prePaymentTo.setBalanceDue(paymentDue);

        return prePaymentTo;
    }

	public static AncillaryUpdateRequestTO getAncillaryUpdateRequestTo(PostPaymentTO postPaymentTo, TrackInfoDTO trackInfo) throws ModuleException {

		ProxyConstants.BookingType bookingType = null;
		IPGIdentificationParamsDTO ipgDTO = null;
		PayCurrencyDTO payCurrencyDTO = null;
		Date paymentTimestamp = null;
		CommonCreditCardPaymentInfo cardPaymentInfo = null;
		boolean isExtCardPay = false;
		BigDecimal loyaltyPayAmount = BigDecimal.ZERO;
		LinkedList perPaxExternalCharges = null;
		boolean anciPenalty;
		AncillaryIntegratePassengerTO ancillaryPassenger;
		List<LCCClientExternalChgDTO> externalCharges;
		BigDecimal voucherRedeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		ReservationInfo reservationInfo = postPaymentTo.getReservationInfo();
		PostPaymentDTO postPayDto = postPaymentTo.getPostPaymentDto();
		LoyaltyInformation loyaltyInformation = postPaymentTo.getLoyaltyInformation();
		AncillaryIntegrateTO ancillaryIntegrateTo = postPaymentTo.getAncillaryIntegrateTo();
		SessionBasicReservation basicReservation = postPaymentTo.getBasicReservation();
		Collection<ReservationPaxTO> paxList = reservationInfo.getPaxList();
		Map<String, BigDecimal> paxCreditMap = reservationInfo.getPaxCreditMap(reservationInfo.getPnr());
		AncillaryIntegrateReservationTO ancillaryReservation = ancillaryIntegrateTo.getReservation();
		
		// for interline anci modificaiton penalty is hardcoded as true
		if (reservationInfo.isGroupPNR()) {
			anciPenalty = true;
		} else {
			anciPenalty = !AppSysParamsUtil.isRefundIBEAnciModifyCredit();
		}

		BigDecimal paymentAdjustment;

		int paxWithPaymentCount = 0;
		for (ReservationPaxTO pax : paxList) {
			Integer paxSequence = pax.getSeqNumber();
			ancillaryPassenger = resolveAncillaryPassenger(ancillaryReservation, String.valueOf(paxSequence));
			updateReservationPax(ancillaryPassenger, pax);
			if(postPayDto != null ){
				addPaxPaymentExternalCharges(postPayDto.getPaxWiseServiceTaxCCFee(), pax);				
			}

			if ((PaxTypeTO.ADULT.equals(pax.getPaxType()) || PaxTypeTO.CHILD.equals(pax.getPaxType())
					|| PaxTypeTO.PARENT.equals(pax.getPaxType())
					|| (reservationInfo.isInfantPaymentSeparated() && PaxTypeTO.INFANT.equals(pax.getPaxType())))
					&& paxHasPayment(pax, paxCreditMap)) {

				BigDecimal paxCredit = null;
				if (paxCreditMap != null) {
					paxCredit = paxCreditMap.get(pax.getTravelerRefNumber());
				}
				if (paxCredit == null)
					paxCredit = BigDecimal.ZERO;
				BigDecimal addAnciTotal = AccelAeroCalculator.add(BookingUtil.getTotalExtCharge(pax.getExternalCharges()),
						paxCredit);
				BigDecimal remAnciTotal = pax.getToRemoveAncillaryTotal();

				if (AccelAeroCalculator.isGreaterThan(addAnciTotal, remAnciTotal)) {
					paxWithPaymentCount++;
				}
			}
		}

		// boolean isCCPayment = !postPayDTO.hasNoCCPayment() && !postPayDTO.isNoPay();
		// ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
		// postPayDTO.getSelectedExternalCharges(), isCCPayment, false);
		// externalChargesMediator.setCalculateJNTaxForCCCharge(true);
		// LinkedList perPaxExternalCharges =
		// externalChargesMediator.getExternalChargesForAFreshPayment(paxWithPaymentCount);

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo();
		Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
				.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);

		String loyaltyMemberAccountId = null;
		String[] rewardIDs = null;

		Date currentDatetime = new Date();

		// -----------------------------------------

		AncillaryUpdateRequestTO ancillaryUpdateRequestTo = new AncillaryUpdateRequestTO();

		CommonAncillaryModifyAssembler anciAssembler = new CommonAncillaryModifyAssembler();

		anciAssembler.setPnr(reservationInfo.getPnr());
		anciAssembler.setVersion(reservationInfo.getVersion());
		anciAssembler.setTargetSystem(reservationInfo.isGroupPNR() ? ProxyConstants.SYSTEM.INT : ProxyConstants.SYSTEM.AA);
		anciAssembler.setTransactionIdentifier(reservationInfo.getTransactionId());
		anciAssembler.setReservationStatus(reservationInfo.getReservationStatus());
		anciAssembler.setLccSegments(reservationInfo.getExistingAllLccSegments());
		anciAssembler.setApplyPenaltyForAnciModification(anciPenalty);
		if(ancillaryIntegrateTo.getReservation() != null && ancillaryIntegrateTo.getReservation().isJnTaxApplicable()){
			anciAssembler.setServiceTaxRatio(ancillaryIntegrateTo.getReservation().getTaxRatio());
		}
		
		Collection<LCCClientReservationPax> lccpassengers = new ArrayList<>();
		if (reservationInfo.getPaxList() != null) {
			for (ReservationPaxTO paxTO : reservationInfo.getPaxList()) {
				LCCClientReservationPax lccPax = new LCCClientReservationPax();
				lccPax.setPaxSequence(paxTO.getSeqNumber());
				lccpassengers.add(lccPax);
			}
		}
		anciAssembler.setLccPassengers(lccpassengers);

		if (postPayDto != null) {
			boolean isCCPayment = !postPayDto.hasNoCCPayment() && !postPayDto.isNoPay() && !postPaymentTo.getTotalPaidFromVoucherLMS();
			ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
					postPayDto.getSelectedExternalCharges(), isCCPayment, false);
			externalChargesMediator.setCalculateJNTaxForCCCharge(true);
			perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(paxWithPaymentCount, 0);
			loyaltyPayAmount = postPayDto.getLoyaltyCredit();
			if (postPaymentTo.getPayByVoucherInfo() != null) {
				voucherRedeemedAmount = voucherRedeemedAmount.add(postPaymentTo.getPayByVoucherInfo().getRedeemedTotal());
			}
			anciAssembler.setTemporyPaymentMap(postPayDto.getTemporyPaymentMap());
			anciAssembler
					.setLoyaltyPaymentInfo(postPayDto.getCarrierLoyaltyPaymentInfo(AppSysParamsUtil.getDefaultCarrierCode()));

			IPGResponseDTO ipgResponseDTO = postPayDto.getIpgResponseDTO();
			cardPaymentInfo = getCommonCreditCardPaymentInfo(postPayDto.getTemporyPaymentMap(), ipgResponseDTO == null ? false
					: ipgResponseDTO.isSuccess());
			isExtCardPay = true;

			boolean isPaymentSuccess = false;
			if (postPayDto.getIpgResponseDTO() != null)
				isPaymentSuccess = postPayDto.getIpgResponseDTO().isSuccess();

			if (!postPayDto.isNoPay() && bookingType != ProxyConstants.BookingType.ONHOLD && !postPayDto.hasNoCCPayment()
					&& !postPaymentTo.getIsTotalAmountPaidFromVoucher() && !postPaymentTo.getTotalPaidFromVoucherLMS()) {
				int paymentGatewayId =
						postPayDto.getPaymentGateWay() != null ? postPayDto.getPaymentGateWay().getGatewayId() : null;
				String currencyCode =
						postPayDto.getPaymentGateWay() != null ? postPayDto.getPaymentGateWay().getPaymentCurrency() : null;
				ipgDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(paymentGatewayId, currencyCode);

				CommonCreditCardPaymentInfo creditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils
						.getFirstElement(postPayDto.getTemporyPaymentMap().values());
				payCurrencyDTO = creditCardPaymentInfo.getPayCurrencyDTO();
				paymentTimestamp = creditCardPaymentInfo.getTxnDateTime();

			}
			String brokerType = getPaymentBrokerType(ipgDTO);

			if (brokerType != null
					&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)
					&& cardPaymentInfo != null) {
				isExtCardPay = false;
			}

		}

		PayByVoucherInfo sessionPayByVoucherInfo = null;
		if (postPaymentTo.getPayByVoucherInfo() != null) {
			sessionPayByVoucherInfo = postPaymentTo.getPayByVoucherInfo().clone();
		}

		for (ReservationPaxTO reservationPax : reservationInfo.getPaxList()) {
			Integer paxSequence = reservationPax.getSeqNumber();
			ancillaryPassenger = resolveAncillaryPassenger(ancillaryReservation, String.valueOf(paxSequence));

			if (!reservationInfo.isInfantPaymentSeparated() && reservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
				continue;
			}
			List<LCCClientExternalChgDTO> extCharges = new ArrayList<LCCClientExternalChgDTO>();
			if (paxHasPayment(reservationPax, paxCreditMap) && perPaxExternalCharges != null) {
				extCharges = BookingUtil.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
			}
			 extCharges.addAll(reservationPax.getExternalCharges());
			PaymentAssemblerComposer paymentComposer = new PaymentAssemblerComposer(extCharges);

			if (postPayDto != null) {

				if (!postPayDto.isNoPay()) {
					BigDecimal totalPaymentAmount = BigDecimal.ZERO;
					paymentAdjustment = BigDecimal.ZERO;

					BigDecimal totalWithAncillary = AccelAeroCalculator.add(totalPaymentAmount, getTotalExtCharge(extCharges),
							reservationPax.getToRemoveAncillaryTotal().negate());
					paymentAdjustment = paymentAdjustment.add(reservationPax.getToRemoveAncillaryTotal());

					if (paxCreditMap != null && paxCreditMap.get(reservationPax.getSeqNumber().toString()) != null) {
						BigDecimal paxCredit = paxCreditMap.get(reservationPax.getSeqNumber().toString());
						if (paxCredit.negate().compareTo(BigDecimal.ZERO) > 0) {
							if (paxCredit.negate().compareTo(totalWithAncillary) < 0) {
								totalWithAncillary = AccelAeroCalculator.add(totalWithAncillary, paxCredit);
								totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, paxCredit);
							} else {
								totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, totalWithAncillary.negate());
								totalWithAncillary = BigDecimal.ZERO;
							}
						} else if (paxCredit.compareTo(BigDecimal.ZERO) > 0) {
							totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, paxCredit);
							totalWithAncillary = AccelAeroCalculator.add(totalWithAncillary, paxCredit);
						}
					}

					BigDecimal lmsMemberPayment = reconcileLmsPayAmount(paxWiseRedeemedAmounts.get(paxSequence),
							totalWithAncillary);

					String loyaltyAgent = CommonServiceUtil.getGlobalConfig().getBizParam(SystemParamKeys.LOYALITY_AGENT_CODE);
					IPGResponseDTO creditInfo = postPayDto.getIpgResponseDTO();

					if (totalWithAncillary.compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal totalExtChgs = getTotalExtCharge(extCharges);
						PaxPaymentUtil paxPayUtil = new PaxPaymentUtil(totalWithAncillary, totalExtChgs, loyaltyPayAmount,
								lmsMemberPayment, voucherRedeemedAmount, AccelAeroCalculator.getDefaultBigDecimalZero());

						loyaltyPayAmount = paxPayUtil.getRemainingLoyalty();
						while (paxPayUtil.hasConsumablePayments()) {
							PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
							BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
							if (payMethod == PaymentMethod.LMS) {
								for (String operatingCarrierCode : carrierWiseLoyaltyPayments.keySet()) {
									LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = carrierWiseLoyaltyPayments
											.get(operatingCarrierCode);
									Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
											.getPaxProductPaymentBreakdown();
									BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil.getPaxRedeemedTotal(
											carrierPaxProductRedeemed, paxSequence);

									loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
									rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
									if (payCurrencyDTO != null) {
										payCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
									}
									if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
										paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs,
												carrierPaxProductRedeemed, carrierLmsMemberPayment, payCurrencyDTO,
												currentDatetime, operatingCarrierCode);
									}
								}

							} else if (payMethod == PaymentMethod.MASHREQ_LOYALTY) {
								// paymentComposer.addLoyaltyCreditPayment(loyaltyAgent, paxPayAmt, loyaltyAccount,
								// payCurrencyDTO,
								// currentDatetime);
							} else if (payMethod == PaymentMethod.VOUCHER) {
								if (sessionPayByVoucherInfo != null) {
									paymentComposer.addVoucherPayment(sessionPayByVoucherInfo, paxPayAmt,
											payCurrencyDTO, currentDatetime);
									voucherRedeemedAmount = paxPayUtil.getRemainingVoucherAmount();
								}
							} else {
								if (isExtCardPay) {
									paymentComposer.addCreditCardPayment(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
											paymentTimestamp, null);
								} else {
									paymentComposer.addCreditCardPaymentInternal(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
											paymentTimestamp, cardPaymentInfo.getNo(), cardPaymentInfo.geteDate(),
											cardPaymentInfo.getSecurityCode(), cardPaymentInfo.getName(), null);
								}
							}

						}
					}
				}

			}

			LCCClientPaymentAssembler payment = paymentComposer.getPaymentAssembler();

			payment.setPaxType(com.isa.thinair.webplatform.api.util.CommonUtil.getActualPaxType(reservationPax));

			anciAssembler.addPassengerPayment(reservationPax.getSeqNumber(), payment);

			// there can be situations where no payments exits but ancillary is
			// added (e.g. no charge ssr)
			List<LCCSelectedSegmentAncillaryDTO> selectedAnci = reservationPax.getSelectedAncillaries();
			if (ancillaryPassenger != null) {
				anciAssembler.addAncillary(reservationPax.getSeqNumber(), ancillaryPassenger.getSelectedAncillaries());
				anciAssembler.removeAncillary(reservationPax.getSeqNumber(), ancillaryPassenger.getAncillariesToRemove());

			}
			anciAssembler.updateAncillary(reservationPax.getSeqNumber(), reservationPax.getAncillariesToUpdate());			

		}

		Map<String, Set<String>> fltRefWiseSelectedMeal = AncillaryDTOUtil
				.getFlightReferenceWiseSelectedMeals((List<ReservationPaxTO>) paxList);

		ancillaryUpdateRequestTo.setAnciAssembler(anciAssembler);
		ancillaryUpdateRequestTo.setEnableFraudCheck(AppSysParamsUtil.isFraudCheckEnabled(trackInfo.getOriginChannelId()));
		ancillaryUpdateRequestTo.setContactInfo(composeCommonReservationContactInfo(basicReservation.getContactInformation()));
		ancillaryUpdateRequestTo.setInterlinePaymentAllowed(isInterlinePaymentAllowed(anciAssembler.getPaxAddAncillaryMap()
				.values()));
		ancillaryUpdateRequestTo.setFlightSegWiseSelectedMeals(fltRefWiseSelectedMeal);

		return ancillaryUpdateRequestTo;
	}

	private static void updateReservationPax(AncillaryIntegratePassengerTO ancillaryPassenger, ReservationPaxTO reservationPax) {
		if (ancillaryPassenger != null) {
			reservationPax.addExternalCharges(ancillaryPassenger.getExternalCharges());
			for (LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO : ancillaryPassenger.getAncillariesToRemove()) {
				reservationPax.addAncillariesToRemove(lccSelectedSegmentAncillaryDTO);
			}
			for (LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO : ancillaryPassenger.getSelectedAncillaries()) {
				reservationPax.addSelectedAncillaries(lccSelectedSegmentAncillaryDTO);
			}
		}
	}

    private static String getPaymentBrokerType(IPGIdentificationParamsDTO ipgDTO) throws ModuleException {
        String brokerType = null;
        if (ipgDTO != null && ipgDTO.getIpgId() != null) {
            Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgDTO);
            brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
        }
        return brokerType;
    }

    private static boolean paxHasPayment(ReservationPaxTO pax, Map<String, BigDecimal> paxCreditMap) {
        BigDecimal paxCredit = null;
        if (paxCreditMap != null) {
            paxCredit = paxCreditMap.get(pax.getSeqNumber().toString());
        }
        if (paxCredit == null)
            paxCredit = BigDecimal.ZERO;

        BigDecimal totalPaxPayment = AccelAeroCalculator.add(pax.getAncillaryTotal(true), paxCredit, pax
                .getToRemoveAncillaryTotal().negate());
        if (totalPaxPayment.compareTo(BigDecimal.ZERO) <= 0) {
            return false;
        } else {
            return true;
        }
    }

    private static BigDecimal reconcileLmsPayAmount(BigDecimal lmsPayAmount, BigDecimal amountDue) {
        if ((lmsPayAmount != null && amountDue != null)
                && (lmsPayAmount.equals(amountDue)
                || AccelAeroCalculator.subtract(amountDue, lmsPayAmount).abs()
                .compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0)) {
            return amountDue;
        }
        return lmsPayAmount;
    }

    private static AncillaryIntegratePassengerTO resolveAncillaryPassenger(AncillaryIntegrateReservationTO ancillaryReservation, String passengerRph) {
        AncillaryIntegratePassengerTO passengerTo = null;

        for (AncillaryIntegratePassengerTO passenger : ancillaryReservation.getPassengers()) {
            if (passengerRph.equals(passenger.getPassengerRph())) {
                passengerTo = passenger;
                break;
            }
        }

        return passengerTo;
    }

    private static boolean isInterlinePaymentAllowed(Collection<List<LCCSelectedSegmentAncillaryDTO>> selectedAnci) {
        boolean paymentAllowed = false;
        for (List<LCCSelectedSegmentAncillaryDTO> anciList : selectedAnci)
            for (LCCSelectedSegmentAncillaryDTO ancillaryDTO : anciList) {
                if (ancillaryDTO.getAirSeatDTO() != null) {
                    paymentAllowed = true;
                } else if (ancillaryDTO.getInsuranceQuotations() != null && !ancillaryDTO.getInsuranceQuotations().isEmpty()) {
                    paymentAllowed = true;
                } else if (ancillaryDTO.getMealDTOs() != null && ancillaryDTO.getMealDTOs().size() > 0) {
                    paymentAllowed = true;
                } else if (ancillaryDTO.getBaggageDTOs() != null && ancillaryDTO.getBaggageDTOs().size() > 0) {
                    paymentAllowed = true;
                } else if (ancillaryDTO.getAirportServiceDTOs() != null && ancillaryDTO.getAirportServiceDTOs().size() > 0) {
                    paymentAllowed = true;
                } else if (ancillaryDTO.getAirportTransferDTOs() != null && ancillaryDTO.getAirportTransferDTOs().size() > 0) {
                    paymentAllowed = true;
                }
            }
        return paymentAllowed;
    }

    private static CommonReservationContactInfo composeCommonReservationContactInfo(SessionContactInformation contactInformation) {
        CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();
        lccClientReservationContactInfo.setTitle(contactInformation.getTitle());
        lccClientReservationContactInfo.setFirstName(contactInformation.getFirstName());
        lccClientReservationContactInfo.setLastName(contactInformation.getLastName());

        Address address = contactInformation.getAddress();
        lccClientReservationContactInfo.setStreetAddress1(address.getStreetAddress1());
        lccClientReservationContactInfo.setStreetAddress2(address.getStreetAddress2());
        lccClientReservationContactInfo.setCity(address.getCity());
        lccClientReservationContactInfo.setZipCode(address.getZipCode());
        lccClientReservationContactInfo.setCountryCode(contactInformation.getCountry());

        PhoneNumber phoneNumber;

        phoneNumber = contactInformation.getMobileNumber();
        lccClientReservationContactInfo.setMobileNo(phoneNumber.getCountryCode() + "-" + phoneNumber.getAreaCode() + "-"
                + phoneNumber.getNumber());

        phoneNumber = contactInformation.getLandNumber();
        lccClientReservationContactInfo.setPhoneNo(phoneNumber.getCountryCode() + "-" + phoneNumber.getAreaCode() + "-"
                + phoneNumber.getNumber());

        lccClientReservationContactInfo.setEmail(contactInformation.getEmailAddress());
        // Fix Me, When loading XBE booking for IBE Modification
//        if (contactInformation.getNationality() != null && !contactInformation.getNationality().isEmpty()) {
//            lccClientReservationContactInfo.setNationalityCode(new Integer(contactInformation.getNationality()));
//        }
//        lccClientReservationContactInfo.setState(contactInformation.getState());
//        lccClientReservationContactInfo.setCustomerId();
//        lccClientReservationContactInfo.setPreferredLanguage();


        return lccClientReservationContactInfo;
    }

    public static CommonCreditCardPaymentInfo getCommonCreditCardPaymentInfo(
            Map<Integer, CommonCreditCardPaymentInfo> tempPayMap, boolean isPaymentSuccess) {
        CommonCreditCardPaymentInfo cardPaymentInfo = null;

        if (tempPayMap != null && !tempPayMap.isEmpty()) {
            Set<Integer> tmpKeySet = tempPayMap.keySet();

            for (Integer key : tmpKeySet) {
                if (key != null) {
                    cardPaymentInfo = tempPayMap.get(key);
                    cardPaymentInfo.setPaymentSuccess(isPaymentSuccess);
                    if (cardPaymentInfo != null) {
                        break;
                    }
                }
            }
        }
        return cardPaymentInfo;
    }

    public static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList) {
        BigDecimal total = BigDecimal.ZERO;
        for (LCCClientExternalChgDTO chg : extChgList) {
            total = AccelAeroCalculator.add(total, chg.getAmount());
        }
        return total;
    }

	public static BigDecimal getAncillarySelectionAmount(List<PricedAncillaryType> ancillaryTypes) {
		BigDecimal amount = BigDecimal.ZERO;

		if (ancillaryTypes != null) {
			for (PricedAncillaryType ancillaryType : ancillaryTypes) {
				for (AncillaryScope ancillaryScope : ancillaryType.getAncillaryScopes()) {
					for (PricedSelectedItem ancillaryItem : ancillaryScope.getAncillaries()) {
						if (ancillaryItem.getAmount() != null) {
							if (ancillaryItem.getQuantity() != null && ancillaryItem.getQuantity().SIZE > 0) {
								amount = amount.add(AccelAeroCalculator.multiply(ancillaryItem.getAmount(),
										ancillaryItem.getQuantity()));
							} else {
								amount = amount.add(ancillaryItem.getAmount());
							}
						}
					}
				}
			}
		}

		return amount;
	}
    
	private static BigDecimal getReservationCredit(AncillaryIntegrateTO ancillaryIntergrationTo, ReservationInfo reservationInfo) {

		Map<String, BigDecimal> paxCreditMap;
		BigDecimal creditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		AncillaryIntegrateReservationTO ancillaryReservation = ancillaryIntergrationTo.getReservation();
		String pnr = reservationInfo.getPnr();

		if (reservationInfo.getPnrPaxCreditMap().containsKey(pnr)) {
			paxCreditMap = reservationInfo.getPnrPaxCreditMap().get(pnr);
			for (AncillaryIntegratePassengerTO passenger : ancillaryReservation.getPassengers()) {
				if (paxCreditMap.containsKey(passenger.getPassengerRph())) {
					creditBalance = AccelAeroCalculator.add(creditBalance, paxCreditMap.get(passenger.getPassengerRph()));
				}
			}
		}

		return creditBalance;
	}

	private static Map<String, BigDecimal> externalChargesForPassenger(AncillaryIntegratePassengerTO passenger) {

		List<LCCClientExternalChgDTO> externalCharges = passenger.getExternalCharges();
		BigDecimal anciPenalty = BigDecimal.ZERO;
		BigDecimal jnAnci = BigDecimal.ZERO;

		Map<String, BigDecimal> externalChargeForPax = new HashMap<String, BigDecimal>();
		for (LCCClientExternalChgDTO lCCClientExternalChgDTO : externalCharges) {
			if (lCCClientExternalChgDTO.getExternalCharges().equals(ReservationInternalConstants.EXTERNAL_CHARGES.ANCI_PENALTY)) {
				anciPenalty = anciPenalty.add(lCCClientExternalChgDTO.getAmount());
				externalChargeForPax.put(ANCI_PENALTY_LABEL, anciPenalty);
			} else {
				externalChargeForPax.put(ANCI_PENALTY_LABEL, BigDecimal.ZERO);
			}
			if (lCCClientExternalChgDTO.getExternalCharges().equals(ReservationInternalConstants.EXTERNAL_CHARGES.JN_ANCI)) {
				jnAnci = jnAnci.add(lCCClientExternalChgDTO.getAmount());
				externalChargeForPax.put(JN_ANCI_LABEL, jnAnci);
			} else {
				externalChargeForPax.put(JN_ANCI_LABEL, BigDecimal.ZERO);
			}
			
			if (lCCClientExternalChgDTO.getExternalCharges().equals(ReservationInternalConstants.EXTERNAL_CHARGES.SERVICE_TAX)) {
				jnAnci = jnAnci.add(lCCClientExternalChgDTO.getAmount());
				externalChargeForPax.put(SERVICE_TAX_LABEL, jnAnci);
			} else {
				externalChargeForPax.put(SERVICE_TAX_LABEL, BigDecimal.ZERO);
			}
			
		}

		return externalChargeForPax;

	}

	private static Map<String, BigDecimal> getExternalChargeMap(LCCClientReservation lccClientReservation) {

		Map<String, BigDecimal> externalChargeMap = new HashMap<String, BigDecimal>();
		BigDecimal totalJnAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalServiceTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		
		Map<EXTERNAL_CHARGES, BigDecimal> externalChargersSummary = lccClientReservation.getExternalChargersSummary();
	
		externalChargeMap.put(SEAT_LABEL, externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.SEAT_MAP));
		externalChargeMap.put(AUTOCHECKIN_LABEL,
				externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.AUTOMATIC_CHECKIN));
		externalChargeMap.put(MEAL_LABEL, externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.MEAL));
		externalChargeMap.put(INSURANCE_LABEL,
				externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.INSURANCE));
		externalChargeMap.put(AIRPORT_SERVICE_LABEL,
				externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.AIRPORT_SERVICE));
		externalChargeMap.put(INFLIGHT_SERVICE_LABEL,
				externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.INFLIGHT_SERVICES));
		externalChargeMap.put(BAGGAGE_LABEL, externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE));
		externalChargeMap.put(ANCI_PENALTY_LABEL,
				externalChargersSummary.get(ReservationInternalConstants.EXTERNAL_CHARGES.ANCI_PENALTY));
		
		//calculate JN ANCI from pax wise taxes
		for (LCCClientReservationPax passenger : lccClientReservation.getPassengers()) {
			for (TaxTO taxes : passenger.getTaxes()) {
				if (JN_ANCI_CHARGE_CODE.equals(taxes.getChargeCode())) {
					totalJnAnci = AccelAeroCalculator.add(totalJnAnci,taxes.getAmount());
				}
				if (taxes.getTaxName() != null && taxes.getTaxName().startsWith(SERVICE_TAX_NAME_PREFIX)) {
					totalServiceTax = AccelAeroCalculator.add(totalServiceTax,taxes.getAmount());
				}				
			}
		}
		externalChargeMap.put(JN_ANCI_LABEL,totalJnAnci);
		externalChargeMap.put(SERVICE_TAX_LABEL,totalServiceTax);

		return externalChargeMap;
	}
	
	private static Map<String, BigDecimal> getTotalValueOfAnci(List<LCCSelectedSegmentAncillaryDTO> ancillariesToAddOrRemove) {

		Map<String, BigDecimal> ancillaryBalanceSummeryMap = new HashMap<String, BigDecimal>();

		BigDecimal amountOfAirportServices = BigDecimal.ZERO;
		BigDecimal amountOfSpecialServices = BigDecimal.ZERO;
		BigDecimal amountOfairSeat = BigDecimal.ZERO;
		BigDecimal amountOfBaggageItems = BigDecimal.ZERO;
		BigDecimal amountOfAutoCheckinItems = BigDecimal.ZERO;
		BigDecimal amountOfmealItems = BigDecimal.ZERO;

		for (LCCSelectedSegmentAncillaryDTO ancillaryItemToAddOrRemove : ancillariesToAddOrRemove) {

			if (ancillaryItemToAddOrRemove.getAirportServiceDTOs() != null
					&& !ancillaryItemToAddOrRemove.getAirportServiceDTOs().isEmpty()) {

				List<LCCAirportServiceDTO> airportServiceDTOs = ancillaryItemToAddOrRemove.getAirportServiceDTOs();
				for (LCCAirportServiceDTO airportService : airportServiceDTOs) {
					amountOfAirportServices = amountOfAirportServices.add(airportService.getAdultAmount());
				}

				if (ancillaryBalanceSummeryMap.containsKey(AIRPORT_SERVICE_LABEL)) {
					ancillaryBalanceSummeryMap.replace(AIRPORT_SERVICE_LABEL, amountOfAirportServices);
				} else {
					ancillaryBalanceSummeryMap.put(AIRPORT_SERVICE_LABEL, amountOfAirportServices);
				}
			}
			
			if (ancillaryItemToAddOrRemove.getSpecialServiceRequestDTOs() != null
					&& !ancillaryItemToAddOrRemove.getSpecialServiceRequestDTOs().isEmpty()) {

				List<LCCSpecialServiceRequestDTO> specialServiceDTOs = ancillaryItemToAddOrRemove.getSpecialServiceRequestDTOs();
				for (LCCSpecialServiceRequestDTO specialService : specialServiceDTOs) {
					amountOfSpecialServices = amountOfSpecialServices.add(specialService.getCharge());
				}

				if (ancillaryBalanceSummeryMap.containsKey(INFLIGHT_SERVICE_LABEL)) {
					ancillaryBalanceSummeryMap.replace(INFLIGHT_SERVICE_LABEL, amountOfSpecialServices);
				} else {
					ancillaryBalanceSummeryMap.put(INFLIGHT_SERVICE_LABEL, amountOfSpecialServices);
				}
			}

			if (ancillaryItemToAddOrRemove.getAirSeatDTO() != null) {

				LCCAirSeatDTO airSeatDTO = ancillaryItemToAddOrRemove.getAirSeatDTO();
				amountOfairSeat = amountOfairSeat.add(airSeatDTO.getSeatCharge());

				if (ancillaryBalanceSummeryMap.containsKey(SEAT_LABEL)) {
					ancillaryBalanceSummeryMap.replace(SEAT_LABEL, amountOfairSeat);
				} else {
					ancillaryBalanceSummeryMap.put(SEAT_LABEL, amountOfairSeat);
				}

			}

			if (ancillaryItemToAddOrRemove.getBaggageDTOs() != null && !ancillaryItemToAddOrRemove.getBaggageDTOs().isEmpty()) {

				List<LCCBaggageDTO> baggageDTOs = ancillaryItemToAddOrRemove.getBaggageDTOs();
				for (LCCBaggageDTO baggageItem : baggageDTOs) {
					amountOfBaggageItems = amountOfBaggageItems.add(baggageItem.getBaggageCharge());
				}
				if (ancillaryBalanceSummeryMap.containsKey(BAGGAGE_LABEL)) {
					ancillaryBalanceSummeryMap.replace(BAGGAGE_LABEL, amountOfBaggageItems);
				} else {
					ancillaryBalanceSummeryMap.put(BAGGAGE_LABEL, amountOfBaggageItems);
				}

			}

			if (ancillaryItemToAddOrRemove.getMealDTOs() != null && !ancillaryItemToAddOrRemove.getMealDTOs().isEmpty()) {

				List<LCCMealDTO> mealDTOs = ancillaryItemToAddOrRemove.getMealDTOs();
				for (LCCMealDTO mealItem : mealDTOs) {
					if (mealItem.getAllocatedMeals() > 0) {
						amountOfmealItems = amountOfmealItems
								.add(AccelAeroCalculator.multiply(mealItem.getMealCharge(), mealItem.getAllocatedMeals()));
					} else {
						amountOfmealItems = amountOfmealItems.add(mealItem.getMealCharge());
					}
				}
				if (ancillaryBalanceSummeryMap.containsKey(MEAL_LABEL)) {
					ancillaryBalanceSummeryMap.replace(MEAL_LABEL, amountOfmealItems);
				} else {
					ancillaryBalanceSummeryMap.put(MEAL_LABEL, amountOfmealItems);
				}

			}
			
			if (ancillaryItemToAddOrRemove.getAutomaticCheckinDTOs() != null && !ancillaryItemToAddOrRemove.getAutomaticCheckinDTOs().isEmpty()) {

				List<LCCAutomaticCheckinDTO> autoCheckinDTOs = ancillaryItemToAddOrRemove.getAutomaticCheckinDTOs();
				for (LCCAutomaticCheckinDTO autoCheckinItem : autoCheckinDTOs) {
					amountOfAutoCheckinItems = amountOfAutoCheckinItems.add(autoCheckinItem.getAutomaticCheckinCharge());
				}
				if (ancillaryBalanceSummeryMap.containsKey(AUTOCHECKIN_LABEL)) {
					ancillaryBalanceSummeryMap.replace(AUTOCHECKIN_LABEL, amountOfAutoCheckinItems);
				} else {
					ancillaryBalanceSummeryMap.put(AUTOCHECKIN_LABEL, amountOfAutoCheckinItems);
				}

			}

		}

		return ancillaryBalanceSummeryMap;

	}

	/**
	 * 
	 * @param ancillaryIntergrationTo
	 * @param transactionId
	 * @param reservationInfo
	 * @param lccClientReservation
	 * @return
	 */
	public static BalanceSummaryRS genarateAncillaryBalanceSummary(AncillaryIntegrateTO ancillaryIntergrationTo,
			String transactionId, ReservationInfo reservationInfo, LCCClientReservation lccClientReservation) {

		BigDecimal anciPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal jnAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal serviceTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalReservationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<String, Map<String, BigDecimal>> allPaxAmountOfRemovedAnciMap = new HashMap<String, Map<String, BigDecimal>>();
		Map<String, Map<String, BigDecimal>> allPaxAmountOfAddedAnciMap = new HashMap<String, Map<String, BigDecimal>>();
		//List<ChargeDescriptionInfo> chargeListToDisplay = new ArrayList<ChargeDescriptionInfo>();
		List<String> passengerRphList = new ArrayList<String>();

		PrePaymentRequestTO prePaymentRequestTO = new PrePaymentRequestTO();
		prePaymentRequestTO.setAncillaryIntegrateTo(ancillaryIntergrationTo);
		prePaymentRequestTO.setReservationInfo(reservationInfo);

		BigDecimal creditBalance = getReservationCredit(ancillaryIntergrationTo, reservationInfo);
		List<AncillaryIntegratePassengerTO> passengers = ancillaryIntergrationTo.getReservation().getPassengers();
		Set<String> keySet = getKeySet();

		for (AncillaryIntegratePassengerTO passenger : passengers) {

			passengerRphList.add(passenger.getPassengerRph());
			if (passenger.getAncillariesToRemove() != null && !passenger.getAncillariesToRemove().isEmpty()) {

				Map<String, BigDecimal> removedAncillarySummery = getTotalValueOfAnci(passenger.getAncillariesToRemove());
				allPaxAmountOfRemovedAnciMap.put(passenger.getPassengerRph(), removedAncillarySummery);
			}
			if (passenger.getSelectedAncillaries() != null && !passenger.getSelectedAncillaries().isEmpty()) {

				Map<String, BigDecimal> selectedAncillarySummery = getTotalValueOfAnci(passenger.getSelectedAncillaries());
				allPaxAmountOfAddedAnciMap.put(passenger.getPassengerRph(), selectedAncillarySummery);
			}
			if (passenger.getExternalCharges() != null && !passenger.getExternalCharges().isEmpty()) {

				Map<String, BigDecimal> externalChargeForPax = externalChargesForPassenger(passenger);
				anciPenalty = anciPenalty.add(externalChargeForPax.get(ANCI_PENALTY_LABEL));
				jnAnci = jnAnci.add(externalChargeForPax.get(JN_ANCI_LABEL));
				serviceTax = serviceTax.add(externalChargeForPax.get(SERVICE_TAX_LABEL));
			}
			totalReservationCharge = AccelAeroCalculator.add(totalReservationCharge, passenger.getNetAmount());
		}

		Map<String, BigDecimal> itemWiseAnciRemovedMap = getItemWiseBalance(passengerRphList, allPaxAmountOfRemovedAnciMap,
				keySet);
		Map<String, BigDecimal> itemWiseAnciAddeddMap = getItemWiseBalance(passengerRphList, allPaxAmountOfAddedAnciMap, keySet);
		Map<String, BigDecimal> externalChargeMap = getExternalChargeMap(lccClientReservation);

		getReservationInsuranceAmmount(ancillaryIntergrationTo, itemWiseAnciAddeddMap);
		//updateAnciBalanceSummaryLables(keySet, itemWiseAnciRemovedMap, itemWiseAnciAddeddMap, externalChargeMap,
		//		chargeListToDisplay, anciPenalty, jnAnci, totalReservationCharge);

		Object[] objarr = getModifiedChargesBreakDown(keySet, itemWiseAnciRemovedMap, itemWiseAnciAddeddMap, externalChargeMap,
				 anciPenalty, jnAnci, totalReservationCharge, serviceTax);
		
		List<ChargeBreakdownTemplate> preModifiedBreakDown = (List<ChargeBreakdownTemplate>) objarr[0];
		List<ChargeBreakdownTemplate> postModifiedBreakDown = (List<ChargeBreakdownTemplate>) objarr[1];
		
		BalanceSummaryRS balanceSummery = prepareBalanceSummeryRS(getPrePaymentTo(prePaymentRequestTO).getBalanceDue(),
				totalReservationCharge, creditBalance, lccClientReservation, preModifiedBreakDown,postModifiedBreakDown);
		balanceSummery.setTransactionId(transactionId);
		balanceSummery.setSuccess(true);
		return balanceSummery;

	}

	private static Set<String> getKeySet() {

		Set<String> keySet = new HashSet<String>();
		keySet.add(INSURANCE_LABEL);
		keySet.add(AIRPORT_SERVICE_LABEL);
		keySet.add(INFLIGHT_SERVICE_LABEL);
		keySet.add(SEAT_LABEL);
		keySet.add(AUTOCHECKIN_LABEL);
		keySet.add(BAGGAGE_LABEL);
		keySet.add(MEAL_LABEL);
		keySet.add(ANCI_PENALTY_LABEL);
		keySet.add(JN_ANCI_LABEL);
		keySet.add(SERVICE_TAX_LABEL);
		return keySet;
	}
	
	private static Map<String, String> getAnciTypeMapWithLabel(){
		Map<String,String> keyMap = new HashMap<String, String>();
		keyMap.put(INSURANCE_LABEL, EXTERNAL_CHARGES.INSURANCE.toString());
		keyMap.put(AIRPORT_SERVICE_LABEL, EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());
		keyMap.put(SEAT_LABEL, EXTERNAL_CHARGES.SEAT_MAP.toString());
		keyMap.put(AUTOCHECKIN_LABEL, EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString());
		keyMap.put(BAGGAGE_LABEL, EXTERNAL_CHARGES.BAGGAGE.toString());
		keyMap.put(MEAL_LABEL, EXTERNAL_CHARGES.MEAL.toString());
		keyMap.put(ANCI_PENALTY_LABEL, EXTERNAL_CHARGES.ANCI_PENALTY.toString());
		keyMap.put(JN_ANCI_LABEL, EXTERNAL_CHARGES.JN_ANCI.toString());
		keyMap.put(SERVICE_TAX_LABEL, EXTERNAL_CHARGES.SERVICE_TAX.toString());
		
		return keyMap;
	}

	private static Map<String, BigDecimal> getItemWiseBalance(List<String> passengerRphList,
			Map<String, Map<String, BigDecimal>> allPaxAnciMap, Set<String> keySet) {

		Map<String, BigDecimal> paxWiseAnciMap;
		Map<String, BigDecimal> itemWiseBalance = new HashMap<String, BigDecimal>();

		for (String paxRph : passengerRphList) {

			paxWiseAnciMap = allPaxAnciMap.get(paxRph);

			for (String key : keySet) {

				if (paxWiseAnciMap != null && paxWiseAnciMap.containsKey(key)) {
					BigDecimal itemBalance = (itemWiseBalance.containsKey(key) && itemWiseBalance.get(key) != null)
							? itemWiseBalance.get(key)
							: AccelAeroCalculator.getDefaultBigDecimalZero();
					itemWiseBalance.put(key, AccelAeroCalculator.add(itemBalance, paxWiseAnciMap.get(key)));
				}
			}
		}

		return itemWiseBalance;
	}
//TODO remove comments
/*	private static BalanceSummaryRS prepareBalanceSummeryRS(BigDecimal payable, BigDecimal totalReservationCharge,
			BigDecimal creditBalance, List<ChargeDescriptionInfo> chargeListToDisplay) {

		BalanceSummaryRS balanceSummery = new BalanceSummaryRS();
		UpdatedChargeForDisplay updatedChargeForDisplay = new UpdatedChargeForDisplay();

		if (payable.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			updatedChargeForDisplay.setDueAmountToDisplay(payable);
		} else {
			updatedChargeForDisplay.setDueAmountToDisplay(AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		if (totalReservationCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0
				&& AccelAeroCalculator.subtract(creditBalance.negate(), totalReservationCharge).compareTo(
						AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			updatedChargeForDisplay.setCreditToDisplay(AccelAeroCalculator.subtract(creditBalance.negate(),
					totalReservationCharge));
		} else if (totalReservationCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0
				&& AccelAeroCalculator.add(creditBalance.negate(), totalReservationCharge.negate()).compareTo(
						AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			updatedChargeForDisplay.setCreditToDisplay(AccelAeroCalculator.add(creditBalance.negate(),
					totalReservationCharge.negate()));
		} else {
			updatedChargeForDisplay.setCreditToDisplay(AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		updatedChargeForDisplay.setChargeListToDisplay(chargeListToDisplay);
		balanceSummery.setUpdatedChargeForDisplay(updatedChargeForDisplay);
		return balanceSummery;

	}*/
	
	private static Object[]  getModifiedChargesBreakDown(Set<String> keySet, Map<String, BigDecimal> itemWiseAnciRemovedMap,
			Map<String, BigDecimal> itemWiseAnciAddeddMap, Map<String, BigDecimal> externalChargeMap, BigDecimal anciPenalty, BigDecimal jnAnci,
			BigDecimal totalReservationCharge, BigDecimal serviceTax) {

		List<ChargeBreakdownTemplate> preModifiedBreakDown = new ArrayList<ChargeBreakdownTemplate>();
		List<ChargeBreakdownTemplate> postModifiedBreakDown = new ArrayList<ChargeBreakdownTemplate>();
		
		ChargeBreakdownTemplate preModified;
		ChargeBreakdownTemplate postModified;
		
		BigDecimal newCharge;

		for (String key : keySet) {
			if (externalChargeMap.containsKey(key)) {
				newCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
				
				preModified = new ChargeBreakdownTemplate();
				preModified.setChargeType(getAnciTypeMapWithLabel().get(key));
				preModified.setChargeAmount(externalChargeMap.get(key));
				preModified.setAnci(true);
				
				postModified = new ChargeBreakdownTemplate();
				postModified.setChargeType(getAnciTypeMapWithLabel().get(key));
				postModified.setAnci(true);
				
				
				if (key.equals(ANCI_PENALTY_LABEL)) {
					postModified.setChargeAmount((AccelAeroCalculator.add(anciPenalty, externalChargeMap.get(key))));
					preModified.setAnci(false);
					postModified.setAnci(false);
				} else if (key.equals(JN_ANCI_LABEL)) {
					postModified.setChargeAmount((AccelAeroCalculator.add(jnAnci, externalChargeMap.get(key))));
					preModified.setAnci(false);
					postModified.setAnci(false);
				}else if (key.equals(SERVICE_TAX_LABEL)) {
					postModified.setChargeAmount((AccelAeroCalculator.add(serviceTax, externalChargeMap.get(key))));
				} else if (!itemWiseAnciRemovedMap.containsKey(key) && !itemWiseAnciAddeddMap.containsKey(key)) {
					postModified.setChargeAmount(externalChargeMap.get(key));
				} else {
					newCharge = AccelAeroCalculator.add(
							externalChargeMap.get(key),
							itemWiseAnciAddeddMap.get(key) != null ? itemWiseAnciAddeddMap.get(key) : AccelAeroCalculator
									.getDefaultBigDecimalZero(), itemWiseAnciRemovedMap.get(key) != null ? itemWiseAnciRemovedMap
									.get(key).negate() : AccelAeroCalculator.getDefaultBigDecimalZero());
					if (newCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						postModified.setChargeAmount(newCharge);
					} else {
						postModified.setChargeAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}
				}
				preModifiedBreakDown.add(preModified);
				postModifiedBreakDown.add(postModified);
			}
		}
		Object[] breakdownlistArray = { preModifiedBreakDown, postModifiedBreakDown };
		return breakdownlistArray;
	}
	
	private static BalanceSummaryRS prepareBalanceSummeryRS(BigDecimal payable, BigDecimal totalReservationCharge,
			BigDecimal creditBalance ,LCCClientReservation reservation, 
			List<ChargeBreakdownTemplate> preModifiedBreakDown,List<ChargeBreakdownTemplate> postModifiedBreakDown) {

		BalanceSummaryRS balanceSummery = new BalanceSummaryRS();
		UpdatedChargeForDisplay updatedChargeForDisplay = new UpdatedChargeForDisplay();

		if (payable.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			updatedChargeForDisplay.setDueAmountToDisplay(payable);
		} else {
			updatedChargeForDisplay.setDueAmountToDisplay(AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		if (totalReservationCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0
				&& AccelAeroCalculator.subtract(creditBalance.negate(), totalReservationCharge).compareTo(
						AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			updatedChargeForDisplay.setCreditToDisplay(AccelAeroCalculator.subtract(creditBalance.negate(),
					totalReservationCharge));
		} else if (totalReservationCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0
				&& AccelAeroCalculator.add(creditBalance.negate(), totalReservationCharge.negate()).compareTo(
						AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			updatedChargeForDisplay.setCreditToDisplay(AccelAeroCalculator.add(creditBalance.negate(),
					totalReservationCharge.negate()));
		} else {
			updatedChargeForDisplay.setCreditToDisplay(AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		//------------------------------------------------------------

		BigDecimal newTotalPrice = reservation.getTotalPaidAmount();
		BigDecimal newSurCharge = reservation.getTotalTicketSurCharge();
		BigDecimal newPnaltyCharge = getChargeValueFromBreakDown(postModifiedBreakDown, EXTERNAL_CHARGES.ANCI_PENALTY.toString()) == null
				? AccelAeroCalculator.getDefaultBigDecimalZero()
				: getChargeValueFromBreakDown(postModifiedBreakDown, EXTERNAL_CHARGES.ANCI_PENALTY.toString());

		if(updatedChargeForDisplay.getDueAmountToDisplay().compareTo(BigDecimal.ZERO)>0){
			newTotalPrice = AccelAeroCalculator.add(newTotalPrice, updatedChargeForDisplay.getDueAmountToDisplay());
			newSurCharge  = AccelAeroCalculator.add(newSurCharge, updatedChargeForDisplay.getDueAmountToDisplay());
		}else if(updatedChargeForDisplay.getCreditToDisplay().compareTo(BigDecimal.ZERO)>0){
			newTotalPrice = AccelAeroCalculator.subtract(newTotalPrice, updatedChargeForDisplay.getCreditToDisplay());
			newSurCharge  = AccelAeroCalculator.subtract(newSurCharge, updatedChargeForDisplay.getCreditToDisplay());
		}

		if(newPnaltyCharge.compareTo(BigDecimal.ZERO)>0){
			newSurCharge  = AccelAeroCalculator.subtract(newSurCharge, newPnaltyCharge);
		}

		CommonSummaryInfo summaryInfo = new CommonSummaryInfo();
		
		summaryInfo.setAmountDue(updatedChargeForDisplay.getDueAmountToDisplay());
		summaryInfo.setCreditAmount(updatedChargeForDisplay.getCreditToDisplay());
		summaryInfo.setFareAmount(reservation.getTotalTicketFare());
		
		ChargeSummaryInfo chargeinfo = new ChargeSummaryInfo();
		chargeinfo.setPenaltyCharge(newPnaltyCharge);
		
		summaryInfo.setChargeInfo(chargeinfo);
		summaryInfo.setPaidAmount(reservation.getTotalPaidAmount());
		
		StateSummaryInfo postModifiedSummary = new StateSummaryInfo();
		StateSummaryInfo preModifiedSummary = new StateSummaryInfo();
		
		postModifiedSummary.setAdjAmount(reservation.getTotalTicketAdjustmentCharge());
		postModifiedSummary.setChargeInfo(chargeinfo);
		postModifiedSummary.setDiscount(reservation.getTotalDiscount());
		postModifiedSummary.setFareAmount(reservation.getTotalTicketFare());
		postModifiedSummary.setSurchargeAmount(newSurCharge);
		postModifiedSummary.setTaxAmount(AccelAeroCalculator.add(getChargeValueFromBreakDown(postModifiedBreakDown, EXTERNAL_CHARGES.JN_ANCI.toString()),reservation.getTotalTicketTaxCharge()));
		postModifiedSummary.setTotalPrice(newTotalPrice);

		ChargeSummaryInfo preModifyChargeinfo = new ChargeSummaryInfo();
		preModifyChargeinfo.setPenaltyCharge(getChargeValueFromBreakDown(preModifiedBreakDown, EXTERNAL_CHARGES.ANCI_PENALTY.toString()));
	
		preModifiedSummary.setAdjAmount(reservation.getTotalTicketAdjustmentCharge());
		preModifiedSummary.setChargeInfo(preModifyChargeinfo);
		preModifiedSummary.setDiscount(reservation.getTotalDiscount());
		preModifiedSummary.setFareAmount(reservation.getTotalTicketFare());
		preModifiedSummary.setSurchargeAmount(reservation.getTotalTicketSurCharge());
		preModifiedSummary.setTaxAmount(reservation.getTotalTicketTaxCharge());
		preModifiedSummary.setTotalPrice(reservation.getTotalPaidAmount());
		
		getChargeBreakDownOnlyAnci(postModifiedBreakDown);
		getChargeBreakDownOnlyAnci(preModifiedBreakDown);
		postModifiedSummary.setChargeBreakDown(postModifiedBreakDown);
		preModifiedSummary.setChargeBreakDown(preModifiedBreakDown);
		
		summaryInfo.setPostModifiedSummary(postModifiedSummary);
		summaryInfo.setPreModifiedSummary(preModifiedSummary);
		
		balanceSummery.setReservationBalanceInfo(summaryInfo);
		
		
		List<PassengerSummary> paxList = getPaxList(reservation.getPassengers());
		balanceSummery.setPaxList(paxList);
		
		
		//------------------------------------------------------------
		//updatedChargeForDisplay.setChargeListToDisplay(chargeListToDisplay);
		//balanceSummery.setUpdatedChargeForDisplay(updatedChargeForDisplay);
		return balanceSummery;

	}

	private static List<PassengerSummary> getPaxList(Set<LCCClientReservationPax> passengers) {
		
		List<PassengerSummary> paxList = new ArrayList<PassengerSummary>();
		
		for(LCCClientReservationPax pax : passengers){			
			if(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED.equals(pax.getStatus())){
				PassengerSummary paxSummary = new PassengerSummary();
				PassengerInfo paxInfo = new PassengerInfo();				
				CommonSummaryInfo paxCharges = new CommonSummaryInfo();
				
				//paxInfo.setAccompaniedPaxRPH(pax.getI);
				paxInfo.setPaxType(pax.getPaxType());
				paxInfo.setRph(pax.getTravelerRefNumber());
				String paxName = (pax.getTitle() == null ? "" : pax.getTitle()).concat(" " ).concat(pax.getFirstName()).concat(" ").concat(pax.getLastName());
				paxInfo.setPassengerName(paxName);
				
				
				paxCharges.setAmountDue(pax.getTotalAvailableBalance());
				paxCharges.setCreditAmount(pax.getTotalActualCredit());
				paxCharges.setFareAmount(pax.getTotalFare());
				
				paxSummary.setPaxInfo(paxInfo);
				paxSummary.setPaxCharges(paxCharges);

				paxList.add(paxSummary);
			}
		}
		return paxList;
	}


	private static void getChargeBreakDownOnlyAnci(List<ChargeBreakdownTemplate> chargeBreakDown) {
		Iterator itr = (Iterator) chargeBreakDown.iterator();
		
		while(itr.hasNext()){
			ChargeBreakdownTemplate chg = (ChargeBreakdownTemplate) itr.next();
			if(!chg.isAnci()){
				itr.remove();
			}			
		}
	}


	private static BigDecimal getChargeValueFromBreakDown(List<ChargeBreakdownTemplate>chargeValList,String chargeType){
		BigDecimal chargeVal = AccelAeroCalculator.getDefaultBigDecimalZero();
		
		for(ChargeBreakdownTemplate charge : chargeValList){
			if(chargeType.equals(charge.getChargeType())){
				return charge.getChargeAmount();
			}
		}
		
		return chargeVal;
	}
	
	private static void getReservationInsuranceAmmount(AncillaryIntegrateTO ancillaryIntergrationTo,
			Map<String, BigDecimal> itemWiseAnciAddeddMap) {
		
		BigDecimal reservationInsuranceAmmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		
		if (ancillaryIntergrationTo.getReservation().getReservationInsurances() != null
				&& !ancillaryIntergrationTo.getReservation().getReservationInsurances().isEmpty()) {

			for (LCCClientReservationInsurance insuranceItem : ancillaryIntergrationTo.getReservation()
					.getReservationInsurances()) {
				reservationInsuranceAmmount = AccelAeroCalculator.add(reservationInsuranceAmmount, insuranceItem.getQuotedTotalPremium());
			}
			itemWiseAnciAddeddMap.put(INSURANCE_LABEL, reservationInsuranceAmmount);

		}
	}

	private static void updateAnciBalanceSummaryLables(Set<String> keySet, Map<String, BigDecimal> itemWiseAnciRemovedMap,
			Map<String, BigDecimal> itemWiseAnciAddeddMap, Map<String, BigDecimal> externalChargeMap,
			List<ChargeDescriptionInfo> chargeListToDisplay, BigDecimal anciPenalty, BigDecimal jnAnci,
			BigDecimal totalReservationCharge) {

		BigDecimal newCharge;
		ChargeDescriptionInfo descriptionInfo;

		for (String key : keySet) {
			if (externalChargeMap.containsKey(key)) {
				newCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

				descriptionInfo = new ChargeDescriptionInfo();
				descriptionInfo.setDisplayDescription(key);
				descriptionInfo.setDisplayOldCharge(externalChargeMap.get(key).toString());

				if (key.equals(ANCI_PENALTY_LABEL)) {
					descriptionInfo.setDisplayNewCharge((AccelAeroCalculator.add(anciPenalty, externalChargeMap.get(key)))
							.toString());
					chargeListToDisplay.add(descriptionInfo);
				} else if (key.equals(JN_ANCI_LABEL)) {
					descriptionInfo.setDisplayNewCharge((AccelAeroCalculator.add(jnAnci, externalChargeMap.get(key))).toString());
					chargeListToDisplay.add(descriptionInfo);
				} else if (!itemWiseAnciRemovedMap.containsKey(key) && !itemWiseAnciAddeddMap.containsKey(key)) {
					descriptionInfo.setDisplayNewCharge(externalChargeMap.get(key).toString());
					chargeListToDisplay.add(descriptionInfo);
				} else {
					newCharge = AccelAeroCalculator.add(
							externalChargeMap.get(key),
							itemWiseAnciAddeddMap.get(key) != null ? itemWiseAnciAddeddMap.get(key) : AccelAeroCalculator
									.getDefaultBigDecimalZero(), itemWiseAnciRemovedMap.get(key) != null ? itemWiseAnciRemovedMap
									.get(key).negate() : AccelAeroCalculator.getDefaultBigDecimalZero());
					if (newCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						descriptionInfo.setDisplayNewCharge(newCharge.toString());
					} else {
						descriptionInfo.setDisplayNewCharge(AccelAeroCalculator.getDefaultBigDecimalZero().toString());
					}
					chargeListToDisplay.add(descriptionInfo);
				}
			}
		}
		descriptionInfo = new ChargeDescriptionInfo();
		descriptionInfo.setDisplayNewCharge(totalReservationCharge.toString());
		descriptionInfo.setDisplayDescription(BALANCE_TO_PAY);
		chargeListToDisplay.add(descriptionInfo);
	}
	
	// adding service tax calculated for CC fee/Admin Fee
	private static void addPaxPaymentExternalCharges(Map<Integer, List<LCCClientExternalChgDTO>> paxWiseServiceTaxCCFee,
			ReservationPaxTO pax) {
		if (paxWiseServiceTaxCCFee != null && paxWiseServiceTaxCCFee.get(pax.getSeqNumber()) != null
				&& !paxWiseServiceTaxCCFee.get(pax.getSeqNumber()).isEmpty()) {
			pax.addExternalCharges(paxWiseServiceTaxCCFee.get(pax.getSeqNumber()));
		}
	}

}
