package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.common.ReservationChargeDetail;

public class ReservationChargesRS extends TransactionalBaseRS {

	private String currency;
	private List<ReservationChargeDetail> reservationChargeDetails;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<ReservationChargeDetail> getReservationChargeDetails() {
		return reservationChargeDetails;
	}

	public void setReservationChargeDetails(List<ReservationChargeDetail> reservationChargeDetails) {
		this.reservationChargeDetails = reservationChargeDetails;
	}

}
