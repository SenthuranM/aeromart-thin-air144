package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class AvailableAnci {
	public static List<LCCBaggageRequestDTO> composeBaggageRequest(List<FlightSegmentTO> flightSegmentTOs, String txnId) {
		List<LCCBaggageRequestDTO> bL = new ArrayList<>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {

			LCCBaggageRequestDTO brd = new LCCBaggageRequestDTO();
			brd.setTransactionIdentifier(txnId);
			brd.setCabinClass(flightSegmentTO.getCabinClassCode());
			brd.setFlightSegment(flightSegmentTO);
			bL.add(brd);
		}
		return bL;
	}

	public static List<LCCMealRequestDTO> composeMealRequest(List<FlightSegmentTO> flightSegmentTOs, String txnId) {
		List<LCCMealRequestDTO> mL = new ArrayList<LCCMealRequestDTO>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {

			LCCMealRequestDTO mrd = new LCCMealRequestDTO();
			mrd.setTransactionIdentifier(txnId);
			mrd.setCabinClass(flightSegmentTO.getCabinClassCode());
			mrd.getFlightSegment().add(flightSegmentTO);
			mL.add(mrd);
		}
		return mL;
	}
}
