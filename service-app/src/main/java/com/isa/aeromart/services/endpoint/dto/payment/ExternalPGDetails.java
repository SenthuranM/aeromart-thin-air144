package com.isa.aeromart.services.endpoint.dto.payment;

public class ExternalPGDetails {

	private String redirectionURL;
	private String postInputData;
	private String requestMethod;
	private String brokerType;

	// Pay_AT_StoreResponseDTO specific
	private String requestId;
	private String voucherId;
	private String scriptUrl;
	private boolean viewPaymentInIframe;
	private boolean displayBanners;
	
	// Tap Offline specific
	private boolean displayOfflinePaymentMessage;

	// Pay fort Offline
	private String payFortOfflineBillNumber;

	public String getRedirectionURL() {
		return redirectionURL;
	}

	public void setRedirectionURL(String redirectionURL) {
		this.redirectionURL = redirectionURL;
	}

	public String getPostInputData() {
		return postInputData;
	}

	public void setPostInputData(String postInputData) {
		this.postInputData = postInputData;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getBrokerType() {
		return brokerType;
	}

	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	public String getRequestId() {
		return requestId;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public String getScriptUrl() {
		return scriptUrl;
	}

	public void setScriptUrl(String scriptUrl) {
		this.scriptUrl = scriptUrl;
	}

	public boolean isViewPaymentInIframe() {
		return viewPaymentInIframe;
	}

	public void setViewPaymentInIframe(boolean viewPaymentInIframe) {
		this.viewPaymentInIframe = viewPaymentInIframe;
	}

	public boolean isDisplayBanners() {
		return displayBanners;
	}

	public void setDisplayBanners(boolean displayBanners) {
		this.displayBanners = displayBanners;
	}

	public boolean isDisplayOfflinePaymentMessage() {
		return displayOfflinePaymentMessage;
	}

	public void setDisplayOfflinePaymentMessage(boolean displayOfflinePaymentMessage) {
		this.displayOfflinePaymentMessage = displayOfflinePaymentMessage;
	}

	public String getPayFortOfflineBillNumber() {
		return payFortOfflineBillNumber;
	}

	public void setPayFortOfflineBillNumber(String payFortOfflineBillNumber) {
		this.payFortOfflineBillNumber = payFortOfflineBillNumber;
	}
}
