package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class AncillaryMonetaryAmendmentRequestAdaptor extends
		TransactionAwareAdaptor<TransactionalBaseRQ, AncillaryMonetaryAmendmentRequestAdaptor.MonetaryAmendmentWrapper> {

	public MonetaryAmendmentWrapper adapt(TransactionalBaseRQ source) {

		MonetaryAmendmentWrapper wrapper = new MonetaryAmendmentWrapper();
		List<InventoryWrapper> inventory;

		if (getTransactionalAncillaryService().isRequoteTransaction(getTransactionId())) {
			inventory = AnciCommon
					.toFlightInventories(getTransactionalAncillaryService().getSelectedSegments(getTransactionId()));
		} else {
			inventory = getTransactionalAncillaryService().getFlightSegments(getTransactionId());
		}

		InventoryWrapper firstDepartingSegment = AnciCommon.getFirstDepartingSegment(inventory);

		wrapper.setCarrierCode(firstDepartingSegment.getFlightSegmentTO().getOperatingAirline());
		wrapper.setDepartingSegmentCode(firstDepartingSegment.getFlightSegmentTO().getSegmentCode());
		wrapper.setExternalChargeType(ReservationInternalConstants.EXTERNAL_CHARGES.JN_ANCI);

		return wrapper;
	}

	public class MonetaryAmendmentWrapper {

		private String carrierCode;
		private String departingSegmentCode;
		private ReservationInternalConstants.EXTERNAL_CHARGES externalChargeType;

		public String getCarrierCode() {
			return carrierCode;
		}

		public void setCarrierCode(String carrierCode) {
			this.carrierCode = carrierCode;
		}

		public String getDepartingSegmentCode() {
			return departingSegmentCode;
		}

		public void setDepartingSegmentCode(String departingSegmentCode) {
			this.departingSegmentCode = departingSegmentCode;
		}

		public ReservationInternalConstants.EXTERNAL_CHARGES getExternalChargeType() {
			return externalChargeType;
		}

		public void setExternalChargeType(ReservationInternalConstants.EXTERNAL_CHARGES externalChargeType) {
			this.externalChargeType = externalChargeType;
		}
	}
}
