package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.passenger.DuplicateCheckInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class DuplicateCheckPaxAdaptor implements Adaptor<DuplicateCheckInfo, LCCClientReservationPax> {

	@Override
	public LCCClientReservationPax adapt(DuplicateCheckInfo duplicateCheckInfo) {
		LCCClientReservationPax lccClientReservationPax = new LCCClientReservationPax();
		LCCClientReservationAdditionalPax lccClientReservationAdditionalPax = new LCCClientReservationAdditionalPax();
		lccClientReservationAdditionalPax.setFfid(duplicateCheckInfo.getFfid());
		lccClientReservationAdditionalPax.setPassportNo(duplicateCheckInfo.getFoidNumber());
		lccClientReservationPax.setLccClientAdditionPax(lccClientReservationAdditionalPax);
		lccClientReservationPax.setFirstName(duplicateCheckInfo.getFirstName());
		lccClientReservationPax.setLastName(duplicateCheckInfo.getLastName());
		lccClientReservationPax.setNationality(duplicateCheckInfo.getNationality());
		lccClientReservationPax.setPaxType(duplicateCheckInfo.getPaxType());
		lccClientReservationPax.setTitle(duplicateCheckInfo.getTitle());
		return lccClientReservationPax;
	}

}
