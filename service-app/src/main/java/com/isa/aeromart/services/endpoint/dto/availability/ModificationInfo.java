package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.List;

import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class ModificationInfo {

	/*
	 * pnr of existing reservation.
	 */
	@NotNull
	private String pnr;

	/*
	 * Reservation segment RPHs of canceled segments.
	 */
	private List<String> cnxFlights;

	/*
	 * Flight segment RPH of newly added segments.
	 */
	private List<RequoteAddedOND> addedFlights;

	/*
	 * Reservation segments which modified.
	 */
	private List<ModifiedFlight> modifiedFlights;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<String> getCnxFlights() {
		return cnxFlights;
	}

	public void setCnxFlights(List<String> cnxFlights) {
		this.cnxFlights = cnxFlights;
	}

	public List<RequoteAddedOND> getAddedFlights() {
		return addedFlights;
	}

	public void setAddedFlights(List<RequoteAddedOND> addedFlights) {
		this.addedFlights = addedFlights;
	}

	public List<ModifiedFlight> getModifiedFlights() {
		return modifiedFlights;
	}

	public void setModifiedFlights(List<ModifiedFlight> modifiedFlights) {
		this.modifiedFlights = modifiedFlights;
	}


}
