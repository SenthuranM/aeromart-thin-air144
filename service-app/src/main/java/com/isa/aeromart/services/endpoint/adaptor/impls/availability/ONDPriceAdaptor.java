package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BasicChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class ONDPriceAdaptor {
	private Map<Integer, OndOWPricing> ondWisePricing = new HashMap<Integer, OndOWPricing>();
	private TravellerQuantity travellerQuantity;
	private Map<Integer, Boolean> ondFlexiSelection;

	public ONDPriceAdaptor(TravellerQuantity travellerQuantity, Map<Integer, Boolean> ondFlexiSelection) {
		this.travellerQuantity = travellerQuantity;
		this.ondFlexiSelection = ondFlexiSelection;
	}

	public void adaptPaxPrice(PerPaxPriceInfoTO paxPriceInfo) {
		FareTypeTO paxPrice = paxPriceInfo.getPassengerPrice();
		addCharges(paxPriceInfo.getPassengerType(), paxPrice.getSurcharges(), new ONDSurchargeAdaptor(this));
		addCharges(paxPriceInfo.getPassengerType(), paxPrice.getTaxes(), new ONDTaxAdaptor(this));
		addCharges(paxPriceInfo.getPassengerType(), paxPrice.getFees(), new ONDFeeAdaptor(this));
		new ONDFareAdaptor(this).addOWFare(paxPriceInfo.getPassengerType(), paxPrice.getBaseFares());
		new ONDFlexiChargeAdaptor(this, ondFlexiSelection).addOWFlexiFare(paxPriceInfo.getPassengerType(),
				paxPrice.getOndExternalCharges());
	}

	private <T extends BasicChargeTO> void
			addCharges(String passengerType, List<T> charges, ONDChargeAdaptor<T> surchargeAdaptor) {
		for (T x : charges) {
			surchargeAdaptor.addCharge(passengerType, x);
		}
	}

	List<OndOWPricing> getOwPricing() {
		return new ArrayList<OndOWPricing>(ondWisePricing.values());
	}

	OndOWPricing getOndPrice(Integer ondSequence) {
		if (!ondWisePricing.containsKey(ondSequence)) {
			ondWisePricing.put(ondSequence, new OndOWPricing());
		}
		return ondWisePricing.get(ondSequence);
	}

	PaxPrice getPaxPrice(String paxType, BasicChargeTO basicCharge) {
		OndOWPricing ondPrice = getOndPrice(basicCharge.getOndSequence());
		ondPrice.setOndSequence(basicCharge.getOndSequence());
		PaxPrice pax = null;
		for (PaxPrice paxPrice : ondPrice.getPaxWise()) {
			if (paxPrice.getPaxType().compareTo(paxType) == 0) {
				pax = paxPrice;
				break;
			}
		}
		if (pax == null) {
			pax = new PaxPrice();
			pax.setPaxType(paxType);
			if (paxType.equalsIgnoreCase(PaxTypeTO.ADULT)) {
				pax.setNoOfPax(travellerQuantity.getAdultCount());
			} else if (paxType.equalsIgnoreCase(PaxTypeTO.CHILD)) {
				pax.setNoOfPax(travellerQuantity.getChildCount());
			} else if (paxType.equalsIgnoreCase(PaxTypeTO.INFANT)) {
				pax.setNoOfPax(travellerQuantity.getInfantCount());
			}
			ondPrice.getPaxWise().add(pax);
		}

		return pax;
	}
	
	PaxPrice getPaxPrice(String paxType, BaseFareTO baseFare) {
		OndOWPricing ondPrice = getOndPrice(baseFare.getOndSequence());
		ondPrice.setOndSequence(baseFare.getOndSequence());
		PaxPrice pax = null;
		for (PaxPrice paxPrice : ondPrice.getPaxWise()) {
			if (paxPrice.getPaxType().compareTo(paxType) == 0) {
				pax = paxPrice;
				break;
			}
		}
		if (pax == null) {
			pax = new PaxPrice();
			pax.setPaxType(paxType);
			if (paxType.equalsIgnoreCase(PaxTypeTO.ADULT)) {
				pax.setNoOfPax(travellerQuantity.getAdultCount());
			} else if (paxType.equalsIgnoreCase(PaxTypeTO.CHILD)) {
				pax.setNoOfPax(travellerQuantity.getChildCount());
			} else if (paxType.equalsIgnoreCase(PaxTypeTO.INFANT)) {
				pax.setNoOfPax(travellerQuantity.getInfantCount());
			}
			ondPrice.getPaxWise().add(pax);
		}

		return pax;
	}
}