package com.isa.aeromart.services.endpoint.dto.masterdata;

public class PaxTitle {
	
	private String titleCode;
	
	private String titleName;

	public String getTitleCode() {
		return titleCode;
	}

	public void setTitleCode(String titleCode) {
		this.titleCode = titleCode;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	
}
