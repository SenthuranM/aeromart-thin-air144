package com.isa.aeromart.services.endpoint.adaptor.impls.customer;


import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerQuestionaireMap;
import com.isa.thinair.aircustomer.api.model.CustomerQuestionaire;


public class CustomerQuestionaireResAdaptor implements Adaptor<CustomerQuestionaire , CustomerQuestionaireMap>{

	@Override
	public CustomerQuestionaireMap adapt(CustomerQuestionaire customerQuestionaire) {
		
		CustomerQuestionaireMap customerQuestionaireMap = new CustomerQuestionaireMap();
		
		customerQuestionaireMap.setQuestionKey(String.valueOf(customerQuestionaire.getQuestionKey()));
		customerQuestionaireMap.setQuestionAnswer(customerQuestionaire.getQuestionAnswer());
		
		return customerQuestionaireMap;
	}

}
