package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.Date;

import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class AirportMessageFlightSegmentInfo {

	@NotNull
	private String origin;
	@NotNull
	private String destination;
	@NotNull
	private Date departureDateTimeZulu;
	private Date arrivalDateTimeZulu;
	private String filghtDesignator;
	private boolean returnFlag;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public String getFilghtDesignator() {
		return filghtDesignator;
	}

	public void setFilghtDesignator(String filghtDesignator) {
		this.filghtDesignator = filghtDesignator;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}
}
