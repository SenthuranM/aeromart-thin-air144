package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxTitle;
import com.isa.thinair.airmaster.api.dto.PaxTitleDTO;

public class PaxTitleAdaptor implements Adaptor<PaxTitleDTO, PaxTitle>{

	@Override
	public PaxTitle adapt(PaxTitleDTO source) {
		
		PaxTitle paxTitle = new PaxTitle();
		
		paxTitle.setTitleCode(source.getTitleCode());
		paxTitle.setTitleName(source.getTitleName());
		
		return paxTitle;
	}

}
