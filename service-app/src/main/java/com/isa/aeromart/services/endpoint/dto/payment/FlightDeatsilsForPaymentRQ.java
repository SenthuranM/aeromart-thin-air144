package com.isa.aeromart.services.endpoint.dto.payment;


/** only used by payment summary request */
public class FlightDeatsilsForPaymentRQ {

	private String languageCode;
	private String originCode;
	private String destinationCode;
	
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getOriginCode() {
		return originCode;
	}

	public void setOriginCode(String originCode) {
		this.originCode = originCode;
	}

	public String getDestinationCode() {
		return destinationCode;
	}

	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}

}
