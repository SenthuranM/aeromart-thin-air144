package com.isa.aeromart.services.endpoint.dto.common;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.utils.common.IBEParamKeyUtil;

/**
 * 
 * @author rajitha
 *
 */
public class IBEParamKeyResponse extends TransactionalBaseRS {
	
	List<IBEAppParameterDTO> ibeParamKeyResponse = new ArrayList<IBEAppParameterDTO>();
	
	public IBEParamKeyResponse(){
		setIbeParamKeyResponse(new IBEParamKeyUtil().prepareIbeParamKeys());
	}
	
	public List<IBEAppParameterDTO> getIbeParamKeyResponse() {
		return ibeParamKeyResponse;
	}

	public void setIbeParamKeyResponse(List<IBEAppParameterDTO> ibeParamKeyResponse) {
		this.ibeParamKeyResponse = ibeParamKeyResponse;
	}

	
	
}
