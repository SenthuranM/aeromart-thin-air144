package com.isa.aeromart.services.endpoint.dto.ancillary.type.baggage;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;

public class BaggageItem extends AncillaryItem {
	private boolean defaultBaggage;

	public boolean isDefaultBaggage() {
		return defaultBaggage;
	}

	public void setDefaultBaggage(boolean defaultBaggage) {
		this.defaultBaggage = defaultBaggage;
	}
}
