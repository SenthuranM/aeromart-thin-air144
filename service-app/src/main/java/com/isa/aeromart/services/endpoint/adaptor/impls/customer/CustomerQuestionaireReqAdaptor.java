package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerQuestionaireMap;
import com.isa.thinair.aircustomer.api.model.CustomerQuestionaire;

public class CustomerQuestionaireReqAdaptor implements Adaptor<CustomerQuestionaireMap,CustomerQuestionaire>{

	@Override
	public CustomerQuestionaire adapt(CustomerQuestionaireMap customerQuestionaireMap) {
		
		CustomerQuestionaire customerQuestionaire = new CustomerQuestionaire();
		
		customerQuestionaire.setQuestionKey(Integer.valueOf(customerQuestionaireMap.getQuestionKey()));
		customerQuestionaire.setQuestionAnswer(customerQuestionaireMap.getQuestionAnswer());
		
		return customerQuestionaire;
		
	}

}
