package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.PaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;

public class PaymentInfoAdaptor implements Adaptor<LCCClientPaymentInfo, PaymentInfo> {

	@Override
	public PaymentInfo adapt(LCCClientPaymentInfo lccClientPaymentInfo) {
		PaymentInfo paymentInfo = new PaymentInfo();

		paymentInfo.setDate(lccClientPaymentInfo.getPaymentTnxDateTime());
		if (lccClientPaymentInfo.getPayCurrencyAmount() != null) {
			paymentInfo.setAmountInPaidCurrency(lccClientPaymentInfo.getPayCurrencyAmount().toString());
		}
		if (lccClientPaymentInfo.getPayCurrencyDTO() != null) {
			paymentInfo.setPaidCurrency(lccClientPaymentInfo.getPayCurrencyDTO().getPayCurrencyCode());
		}
		if (lccClientPaymentInfo.getTotalAmount() != null) {
			paymentInfo.setPaymentAmount(lccClientPaymentInfo.getTotalAmount().toString());
		}
		if (lccClientPaymentInfo.getActualPaymentMethod() != null) {
			paymentInfo.setPaymentMethod(lccClientPaymentInfo.getActualPaymentMethod().toString());
		}

		return paymentInfo;
	}

}
