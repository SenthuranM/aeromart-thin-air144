package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class OndUnit {
	
	private String ondId;

	private List<String> flightSegmentRPH;

	public String getOndId() {
		return ondId;
	}

	public void setOndId(String ondId) {
		this.ondId = ondId;
	}

	public List<String> getFlightSegmentRPH() {
		return flightSegmentRPH;
	}

	public void setFlightSegmentRPH(List<String> flightSegmentRPH) {
		this.flightSegmentRPH = flightSegmentRPH;
	}

}
