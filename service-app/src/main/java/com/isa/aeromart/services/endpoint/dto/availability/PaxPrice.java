package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;

public class PaxPrice extends PaxPriceBasic {

	// pax fare price
	@CurrencyValue
	private BigDecimal fare;

	// surcharge for pax
	@CurrencyValue
	private BigDecimal surcharge;

	// tax for pax
	@CurrencyValue
	private BigDecimal tax;

	// number of pax of this pax type
	private int noOfPax;

	// total fare for pax type
	@CurrencyValue
	private BigDecimal totalFare;

	public PaxPrice() {
		fare = BigDecimal.ZERO;
		surcharge = BigDecimal.ZERO;
		tax = BigDecimal.ZERO;
		totalFare = BigDecimal.ZERO;
	}

	public PaxPrice(String paxType) {
		super(paxType);
		fare = BigDecimal.ZERO;
		surcharge = BigDecimal.ZERO;
		tax = BigDecimal.ZERO;
		totalFare = BigDecimal.ZERO;
	}

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	public BigDecimal getSurcharge() {
		return surcharge;
	}

	public void setSurcharge(BigDecimal surcharge) {
		this.surcharge = surcharge;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public int getNoOfPax() {
		return noOfPax;
	}

	public void setNoOfPax(int noOfPax) {
		this.noOfPax = noOfPax;
	}

	public BigDecimal getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

}
