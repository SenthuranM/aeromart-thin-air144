package com.isa.aeromart.services.endpoint.dto.ancillary;

public class AncillaryAvailability extends Ancillary {

	private boolean available;

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}