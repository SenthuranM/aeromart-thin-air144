package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;

public class AncillaryAvailabilityRQ extends BaseCheckAncillaryRQ {

	private ReservationData reservationData;

	public ReservationData getReservationData() {
		return reservationData;
	}

	public void setReservationData(ReservationData reservationData) {
		this.reservationData = reservationData;
	}

}
