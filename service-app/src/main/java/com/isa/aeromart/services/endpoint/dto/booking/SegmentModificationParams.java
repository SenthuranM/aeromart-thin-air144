package com.isa.aeromart.services.endpoint.dto.booking;

public class SegmentModificationParams {

	private String reservationSegmentRPH;

	private boolean cancellable;

	private boolean modifible;

	private boolean modifibleByDate;

	private boolean modifibleByRoute;

	public String getReservationSegmentRPH() {
		return reservationSegmentRPH;
	}

	public void setReservationSegmentRPH(String reservationSegmentRPH) {
		this.reservationSegmentRPH = reservationSegmentRPH;
	}

	public boolean isCancellable() {
		return cancellable;
	}

	public void setCancellable(boolean cancellable) {
		this.cancellable = cancellable;
	}

	public boolean isModifible() {
		return modifible;
	}

	public void setModifible(boolean modifible) {
		this.modifible = modifible;
	}

	public boolean isModifibleByDate() {
		return modifibleByDate;
	}

	public void setModifibleByDate(boolean modifyByDate) {
		this.modifibleByDate = modifyByDate;
	}

	public boolean isModifibleByRoute() {
		return modifibleByRoute;
	}

	public void setModifibleByRoute(boolean modifyByRoute) {
		this.modifibleByRoute = modifyByRoute;
	}

}
