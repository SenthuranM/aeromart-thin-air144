package com.isa.aeromart.services.endpoint.adaptor.impls.common;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;
import com.isa.aeromart.services.endpoint.utils.common.StringUtil;

public class PhoneNumberReqAdaptor implements Adaptor<PhoneNumber, String> {

	@Override
	public String adapt(PhoneNumber phoneNumber) {
		StringBuffer stringBuffer = new StringBuffer();
		if (phoneNumber != null) {
			stringBuffer.append(StringUtil.nullConvertToString(phoneNumber.getCountryCode()));
			stringBuffer.append("-");
			stringBuffer.append(StringUtil.nullConvertToString(phoneNumber.getAreaCode()));
			stringBuffer.append("-");
			stringBuffer.append(StringUtil.nullConvertToString(phoneNumber.getNumber()));
		} else {
			stringBuffer.append("--");
		}
		return stringBuffer.toString();
	}

}
