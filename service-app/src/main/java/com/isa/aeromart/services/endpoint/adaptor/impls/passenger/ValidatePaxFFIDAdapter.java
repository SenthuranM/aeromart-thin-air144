package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.passenger.LMSPassenger;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRQ;

public class ValidatePaxFFIDAdapter implements Adaptor<List<Passenger>, ValidatePaxFFIDsRQ> {

	@Override
	public ValidatePaxFFIDsRQ adapt(List<Passenger> passengers) {
		LMSPaxInfoAdapter lmsPaxInfoAdapter = new LMSPaxInfoAdapter();
		List<LMSPassenger> lmsPaxDetails = new ArrayList<>();
		ValidatePaxFFIDsRQ validatePaxFFIDsRQ = new ValidatePaxFFIDsRQ();
		AdaptorUtils.adaptCollection(passengers, lmsPaxDetails, lmsPaxInfoAdapter);
		validatePaxFFIDsRQ.setLmsPaxDetails(lmsPaxDetails);
		return validatePaxFFIDsRQ;
	}

}
