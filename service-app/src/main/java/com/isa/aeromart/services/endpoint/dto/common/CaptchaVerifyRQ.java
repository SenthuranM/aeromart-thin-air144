package com.isa.aeromart.services.endpoint.dto.common;

public class CaptchaVerifyRQ extends TransactionalBaseRQ {
	private String userReponseToken;

	public String getUserReponseToken() {
		return userReponseToken;
	}

	public void setUserReponseToken(String userReponseToken) {
		this.userReponseToken = userReponseToken;
	}

}
