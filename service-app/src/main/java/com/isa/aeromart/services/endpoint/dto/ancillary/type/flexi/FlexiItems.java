package com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class FlexiItems extends AncillaryItemLeaf {

	private List<FlexiItem> items;

	public FlexiItems() {
		setType(AncillariesConstants.Type.FLEXIBILITY);
	}

	public List<FlexiItem> getItems() {
		return items;
	}

	public void setItems(List<FlexiItem> items) {
		this.items = items;
	}

}
