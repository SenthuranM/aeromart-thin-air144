package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Collection;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;

public class BookingConfirmationRS extends TransactionalBaseRS {

	private BookingInfoTO bookingInfoTO;

	private BalanceSummary balanceSummary;

	private List<ReservationSegmentLite> reservationSegments;

	private ReservationContact contactInfo;

	private List<Advertisement> advertisements;

	private Collection<Passenger> passengers;

	private SocialTO socialTO;

	private PromoTypeInfo promoTypeInfo;

	private String language;

	private String inboundFlightDuration;

	private boolean successfulRefund;

	private boolean itineraryEmailGenerated;

	private boolean allowIndivPrint;
	
	private PaymentDetails paymentDetails;
	
	private boolean isTaxInvoiceGenerated;
	
	private String reservationType;
	

	public BookingInfoTO getBookingInfoTO() {
		return bookingInfoTO;
	}

	public void setBookingInfoTO(BookingInfoTO bookingInfoTO) {
		this.bookingInfoTO = bookingInfoTO;
	}

	public BalanceSummary getBalanceSummary() {
		return balanceSummary;
	}

	public void setBalanceSummary(BalanceSummary balanceSummary) {
		this.balanceSummary = balanceSummary;
	}

	public List<Advertisement> getAdvertisements() {
		return advertisements;
	}

	public void setAdvertisements(List<Advertisement> advertisements) {
		this.advertisements = advertisements;
	}

	public SocialTO getSocialTO() {
		return socialTO;
	}

	public void setSocialTO(SocialTO socialTO) {
		this.socialTO = socialTO;
	}

	public PromoTypeInfo getPromoTypeInfo() {
		return promoTypeInfo;
	}

	public void setPromoTypeInfo(PromoTypeInfo promoTypeInfo) {
		this.promoTypeInfo = promoTypeInfo;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getInboundFlightDuration() {
		return inboundFlightDuration;
	}

	public void setInboundFlightDuration(String inboundFlightDuration) {
		this.inboundFlightDuration = inboundFlightDuration;
	}

	public boolean isSuccessfulRefund() {
		return successfulRefund;
	}

	public void setSuccessfulRefund(boolean successfulRefund) {
		this.successfulRefund = successfulRefund;
	}

	public boolean isItineraryEmailGenerated() {
		return itineraryEmailGenerated;
	}

	public void setItineraryEmailGenerated(boolean itineraryEmailGenerated) {
		this.itineraryEmailGenerated = itineraryEmailGenerated;
	}

	public boolean isAllowIndivPrint() {
		return allowIndivPrint;
	}

	public void setAllowIndivPrint(boolean allowIndivPrint) {
		this.allowIndivPrint = allowIndivPrint;
	}

	public List<ReservationSegmentLite> getReservationSegments() {
		return reservationSegments;
	}

	public void setReservationSegments(List<ReservationSegmentLite> reservationSegments) {
		this.reservationSegments = reservationSegments;
	}

	public ReservationContact getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ReservationContact contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Collection<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(Collection<Passenger> passengers) {
		this.passengers = passengers;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public boolean isTaxInvoiceGenerated() {
		return isTaxInvoiceGenerated;
	}

	public void setTaxInvoiceGenerated(boolean isTaxInvoiceGenerated) {
		this.isTaxInvoiceGenerated = isTaxInvoiceGenerated;
	}

	public String getReservationType() {
		return reservationType;
	}

	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}
}
