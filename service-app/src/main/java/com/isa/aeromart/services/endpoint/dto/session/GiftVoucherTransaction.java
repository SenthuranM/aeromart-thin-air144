package com.isa.aeromart.services.endpoint.dto.session;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.store.GiftVoucherSessionStore;
import com.isa.thinair.commons.api.dto.VoucherDTO;

import java.io.Serializable;
import java.util.List;

public class GiftVoucherTransaction extends BaseTransaction implements GiftVoucherSessionStore, Serializable {

	public static final String SESSION_KEY = "GIFT_VOUCHER_TRNX";
	private static final long serialVersionUID = 7632487568432L;

	private List<VoucherDTO> giftVouchers;
	private BookingRS.PaymentStatus paymentStatus;
	private PostPaymentDTO postPaymentInformation;
	private String cardHolderName;

	@Override
	public List<VoucherDTO> getGiftVouchers() {
		return giftVouchers;
	}

	@Override
	public void storeGiftVouchers(List<VoucherDTO> giftVouchers) {
		this.giftVouchers = giftVouchers;
	}

	@Override
	public void setPaymentStatus(BookingRS.PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Override
	public BookingRS.PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	@Override
	public PostPaymentDTO getPostPaymentInformation() {
		return postPaymentInformation;
	}

	public void storePostPaymentInformation(PostPaymentDTO postPaymentInformation) {
		this.postPaymentInformation = postPaymentInformation;
	}

	@Override
	public void clearSessionStore() {
		this.giftVouchers = null;
		this.postPaymentInformation = null;
	}

	@Override public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
}
