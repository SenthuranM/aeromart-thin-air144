package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.AlertTO;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationAlert;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;

public class ReservationAlertAdaptor implements Adaptor<LCCClientAlertInfoTO, ReservationAlert> {

	private String alertType;

	public ReservationAlertAdaptor(String alertType) {
		this.alertType = alertType;
	}

	@Override
	public ReservationAlert adapt(LCCClientAlertInfoTO lccClientAlertInfoTO) {

		ReservationAlert reservationAlert = new ReservationAlert();

		AlertAdaptor alertAdaptor = new AlertAdaptor();

		List<AlertTO> alertTOs = new ArrayList<>();

		AdaptorUtils.adaptCollection(lccClientAlertInfoTO.getAlertTO(), alertTOs, alertAdaptor);

		if (alertType != null) {
			reservationAlert.setAlertCategory(alertType);
		} else {
			reservationAlert.setAlertCategory(lccClientAlertInfoTO.getAlertCategory());
		}

		reservationAlert.setFlightSegmentRPH(lccClientAlertInfoTO.getFlightSegmantRefNumber());
		reservationAlert.setAlerts(alertTOs);
		return reservationAlert;
	}

}
