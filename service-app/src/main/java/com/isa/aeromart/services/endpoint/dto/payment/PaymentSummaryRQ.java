package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;

public class PaymentSummaryRQ extends ServiceTaxRQ {

	private List<Passenger> paxInfo;
	private boolean onhold; //cash option selection
	private boolean binPromo; //BIN promotion applied

	public List<Passenger> getPaxInfo() {
		return paxInfo;
	}

	public void setPaxInfo(List<Passenger> paxInfo) {
		this.paxInfo = paxInfo;
	}

	public boolean isOnhold() {
		return onhold;
	}

	public void setOnhold(boolean isOnhold) {
		this.onhold = isOnhold;
	}

	public boolean isBinPromo() {
		return binPromo;
	}

	public void setBinPromo(boolean binPromo) {
		this.binPromo = binPromo;
	}

}
