package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.internal.AncillaryReservationTo;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.AvailableAncillaryTo;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

// TODO -- need to move these classes to the B/L layer

public abstract class DefaultAncillarySelector {

    private AvailableAncillaryTo availableAncillaries;

    private AncillaryReservationTo previousSelections;
    
    private boolean isAllAnciReportected;
    
    private AppIndicatorEnum appIndicator;

    public abstract AncillaryReservationTo toDefaultAncillarySelection(List<Transition<List<String>>> modifications);

    public AvailableAncillaryTo getAvailableAncillaries() {
        return availableAncillaries;
    }

    public void setAvailableAncillaries(AvailableAncillaryTo availableAncillaries) {
        this.availableAncillaries = availableAncillaries;
    }

    public AncillaryReservationTo getPreviousSelections() {
        return previousSelections;
    }

    public void setPreviousSelections(AncillaryReservationTo previousSelections) {
        this.previousSelections = previousSelections;
    }

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public boolean isAllAnciReportected() {
		return isAllAnciReportected;
	}

	public void setAllAnciReportected(boolean isAllAnciReportected) {
		this.isAllAnciReportected = isAllAnciReportected;
	}
   
}
