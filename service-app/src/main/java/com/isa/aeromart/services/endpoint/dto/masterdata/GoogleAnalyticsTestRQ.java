package com.isa.aeromart.services.endpoint.dto.masterdata;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class GoogleAnalyticsTestRQ extends TransactionalBaseRQ {
	
	@NotNull
	private String pnr;
	
	@NotNull
	private String revanue;
	
	@NotNull
	private String exception;
	
	@NotNull
	private String path;
	
	@NotNull
	private String flow;
	
	@NotNull
	private String fullPath;
	

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the revanue
	 */
	public String getRevanue() {
		return revanue;
	}

	/**
	 * @param revanue the revanue to set
	 */
	public void setRevanue(String revanue) {
		this.revanue = revanue;
	}

	/**
	 * @return the exception
	 */
	public String getException() {
		return exception;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(String exception) {
		this.exception = exception;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the flow
	 */
	public String getFlow() {
		return flow;
	}

	/**
	 * @param flow the flow to set
	 */
	public void setFlow(String flow) {
		this.flow = flow;
	}

	/**
	 * @return the fullPath
	 */
	public String getFullPath() {
		return fullPath;
	}

	/**
	 * @param fullPath the fullPath to set
	 */
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	
}
