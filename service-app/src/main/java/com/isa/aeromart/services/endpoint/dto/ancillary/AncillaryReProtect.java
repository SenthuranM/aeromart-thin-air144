package com.isa.aeromart.services.endpoint.dto.ancillary;

public class AncillaryReProtect {

	private boolean isReProtect;

	private boolean allAncillariesReProtected;

	public boolean isReProtect() {
		return isReProtect;
	}

	public void setReProtect(boolean isReprotect) {
		this.isReProtect = isReprotect;
	}

	public boolean isAllAncillariesReProtected() {
		return allAncillariesReProtected;
	}

	public void setAllAncillariesReProtected(boolean allAncillariesReProtected) {
		this.allAncillariesReProtected = allAncillariesReProtected;
	}
}
