package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import static com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils.adaptCollection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.availability.AvailableOption;
import com.isa.aeromart.services.endpoint.dto.availability.OriginDestinationRS;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.utils.common.RPHBuilder;
import com.isa.thinair.airinventory.api.dto.seatavailability.FarePriceOnd;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class OriginDestinationRSAdaptor implements Adaptor<OriginDestinationInformationTO, OriginDestinationRS> {
	private static Log log = LogFactory.getLog(OriginDestinationRSAdaptor.class);
	private boolean isFareResponse;
	private RPHGenerator rphGenerator;
	private ReturnFareDiscountBuilder returnFareDiscountBuilder;

	private OriginDestinationRSAdaptor(boolean isFareResponse, RPHGenerator rphGenerator) {
		this.isFareResponse = isFareResponse;
		this.rphGenerator = rphGenerator;
	}

	public OriginDestinationRSAdaptor(boolean isFareResponse, RPHGenerator rphGenerator,
			ReturnFareDiscountBuilder returnFareDiscountBuilder) {
		this(isFareResponse, rphGenerator);
		this.returnFareDiscountBuilder = returnFareDiscountBuilder;
	}

	public static OriginDestinationRSAdaptor getAdaptorForFareResponse(RPHGenerator rphGenerator) {
		return new OriginDestinationRSAdaptor(true, rphGenerator);
	}

	public static OriginDestinationRSAdaptor getAdaptorForFlightResponse(RPHGenerator rphGenerator) {
		return new OriginDestinationRSAdaptor(false, rphGenerator);
	}

	public static OriginDestinationRSAdaptor getAdaptorForReturnFareResponse(RPHGenerator rphGenerator,
			BigDecimal inboundAdultFlexiCharge, TravellerQuantity travellerQuantity) {
		return new OriginDestinationRSAdaptor(true, rphGenerator, new ReturnFareDiscountBuilder(inboundAdultFlexiCharge,
				travellerQuantity));
	}

	@Override
	public OriginDestinationRS adapt(OriginDestinationInformationTO source) {

		OriginDestinationRS target = new OriginDestinationRS();
		target.setOrigin(source.getOrigin());
		target.setDestination(source.getDestination());
		// ondSequence taken from first segment of first ondOption
		if ((source.getOrignDestinationOptions().size() > 0)
				&& (source.getOrignDestinationOptions().get(0).getFlightSegmentList().size() > 0)) {
			target.setOndSequence(source.getOrignDestinationOptions().get(0).getFlightSegmentList().get(0).getOndSequence());
			if (returnFareDiscountBuilder != null) {
				returnFareDiscountBuilder.setSequence(target.getOndSequence());
			}
			replaceSelectedDateMinimumFares(source);
			adaptCollection(source.getOrignDestinationOptions(), target.getAvailableOptions(), getOndAdaptor());
			setSelectedOndOption(target.getAvailableOptions(), source.getPreferredDate());
		}else{
			if (returnFareDiscountBuilder != null) {
				returnFareDiscountBuilder.setCurrentSequence(returnFareDiscountBuilder.getCurrentSequence() + 1);
			}
		}
		return target;
	}

	private Adaptor<OriginDestinationOptionTO, AvailableOption> getOndAdaptor() {
		if (isFareResponse) {
			return OndAvailableOptionAdaptor.getAdatorForFareOption(rphGenerator, returnFareDiscountBuilder);
		} else {
			return OndAvailableOptionAdaptor.getAdatorForFlightOption(rphGenerator);
		}
	}

	private void replaceSelectedDateMinimumFares(OriginDestinationInformationTO ondInfoList) {
		List<OriginDestinationOptionTO> orignDestList = ondInfoList.getOrignDestinationOptions();
		List<OriginDestinationOptionTO> originDestSegFareList = ondInfoList.getOriginDestinationSegFareOptions();
		if (originDestSegFareList.size() > 0) {
			Set<String> segFareFlightRefs = new HashSet<String>();
			Set<String> ondFareFlightRefs = new HashSet<String>();
			OriginDestinationOptionTO segFareOption = originDestSegFareList.get(0);
			for (FlightSegmentTO segment : segFareOption.getFlightSegmentList()) {
				segFareFlightRefs.add(segment.getFlightRefNumber());
			}
			Iterator<OriginDestinationOptionTO> it = orignDestList.iterator();
			while (it.hasNext()) {
				OriginDestinationOptionTO ondOption = it.next();
				for (FlightSegmentTO flightSeg : ondOption.getFlightSegmentList()) {
					ondFareFlightRefs.add(flightSeg.getFlightRefNumber());
				}
				if (segFareFlightRefs.containsAll(ondFareFlightRefs) && ondFareFlightRefs.containsAll(segFareFlightRefs)) {
						setMinimumFaresForDiscount(ondOption, segFareOption);
						fillBundleFaresForSegFares(ondOption, segFareOption);
						orignDestList.remove(ondOption);
						orignDestList.add(segFareOption);
						log.debug("removed fare:" + ondOption + "and replaced segFare : " + segFareOption);
						break;
				}
				ondFareFlightRefs.clear();
			}
		}
	}

	private void fillBundleFaresForSegFares(OriginDestinationOptionTO ondFares, OriginDestinationOptionTO ondSegFares) {
		List<FlightFareSummaryTO> sourceFareList = ondFares.getFlightFareSummaryList();
		List<FlightFareSummaryTO> targetFareList = ondSegFares.getFlightFareSummaryList();
		BigDecimal baseFare = null;
		BigDecimal baseTotalPrice = null;
		Set<String> segFareClasses = new HashSet<String>();
		for (FlightFareSummaryTO flightFare : targetFareList) {
			segFareClasses.add(RPHBuilder.FareClassCodeBuilder(flightFare));
			if (!flightFare.isWithFlexi() && flightFare.getBundledFarePeriodId() == null) {
				baseFare = flightFare.getBaseFareAmount();
				baseTotalPrice = flightFare.getTotalPrice();
			}
		}
		if (!segFareClasses.isEmpty()) {
			Iterator<FlightFareSummaryTO> it = sourceFareList.iterator();
			while (it.hasNext()) {
				FlightFareSummaryTO fareSummary = it.next();
				String fareClasskey = RPHBuilder.FareClassCodeBuilder(fareSummary);
				if (!segFareClasses.contains(fareClasskey) && fareSummary.getBundledFarePeriodId() != null) {
					FlightFareSummaryTO newFareSummary = new FlightFareSummaryTO();
					newFareSummary.setBaseFareAmount(baseFare);
					newFareSummary.setBundledFarePeriodId(fareSummary.getBundledFarePeriodId());
					newFareSummary.setBookingCodes(fareSummary.getBookingCodes());
					newFareSummary.setBundledFareFreeServiceNames(fareSummary.getBundledFareFreeServiceNames());
					newFareSummary.setBundleFareFee(fareSummary.getBundleFareFee());
					newFareSummary.setCalDisplayAmout(fareSummary.getCalDisplayAmout());
					newFareSummary.setComment(fareSummary.getComment());
					newFareSummary.setFlexiRuleId(fareSummary.getFlexiRuleId());
					newFareSummary.setFreeFlexi(fareSummary.isFreeFlexi());
					newFareSummary.setImageUrl(fareSummary.getImageUrl());
					newFareSummary.setBundleFareDescriptionTemplateDTO(fareSummary.getBundleFareDescriptionTemplateDTO());
					newFareSummary.setLogicalCCCode(fareSummary.getLogicalCCCode());
					newFareSummary.setLogicalCCDesc(fareSummary.getLogicalCCDesc());
					newFareSummary.setLogicalCCRank(fareSummary.getLogicalCCRank());
					newFareSummary.setMinimumFareByPriceOnd(fareSummary.getMinimumFareByPriceOnd());
					newFareSummary.setNoOfAvailableSeats(fareSummary.getNoOfAvailableSeats());
					newFareSummary.setRank(fareSummary.getRank());
					newFareSummary.setSeatAvailable(fareSummary.isSeatAvailable());
					newFareSummary.setSegmentBookingClasses(fareSummary.getSegmentBookingClasses());
					newFareSummary.setSelected(fareSummary.isSelected());
					if (fareSummary.getBundleFareFee() != null) {
						newFareSummary.setTotalPrice(AccelAeroCalculator.add(baseTotalPrice, fareSummary.getBundleFareFee()));
					} else {
						newFareSummary.setTotalPrice(AccelAeroCalculator.add(baseTotalPrice,
								AccelAeroCalculator.getDefaultBigDecimalZero()));
					}
					newFareSummary.setWithFlexi(fareSummary.isWithFlexi());
					newFareSummary.setVisibleChannelName(fareSummary.getVisibleChannelName());
					ondSegFares.getFlightFareSummaryList().add(newFareSummary);
				}
				segFareClasses.remove(fareClasskey);
			}
		}
		if (!segFareClasses.isEmpty()) {
			Iterator<FlightFareSummaryTO> it = targetFareList.iterator();
			while (it.hasNext()) {
				FlightFareSummaryTO fareSummary = it.next();
				String fareClasskey = RPHBuilder.FareClassCodeBuilder(fareSummary);
				if (segFareClasses.contains(fareClasskey)) {
					it.remove();
				}
			}
		}
	}

	private void setMinimumFaresForDiscount(OriginDestinationOptionTO ondFares, OriginDestinationOptionTO ondSegFares) {
		Map<FarePriceOnd, BigDecimal> promoMinimumFareByPriceOnd = new HashMap<FarePriceOnd, BigDecimal>();
		for (FlightFareSummaryTO ondFare : ondFares.getFlightFareSummaryList()) {
			if (ondFare.getMinimumFareByPriceOnd() != null && !ondFare.getMinimumFareByPriceOnd().isEmpty()) {
				promoMinimumFareByPriceOnd = ondFare.getMinimumFareByPriceOnd();
				break;
			}
		}
		for (FlightFareSummaryTO ondSegFare : ondSegFares.getFlightFareSummaryList()) {
			if (promoMinimumFareByPriceOnd != null && !promoMinimumFareByPriceOnd.isEmpty()) {
				ondSegFare.setMinimumFareByPriceOnd(promoMinimumFareByPriceOnd);
			}
		}
	}

	// private boolean checkFareReplaceabilityWithSegFare(OriginDestinationOptionTO ondFares, OriginDestinationOptionTO
	// ondSegFares) {
	// BigDecimal baseReturnFare = BigDecimal.ZERO;
	// BigDecimal baseSegFare = BigDecimal.ZERO;
	// if (ondFares.getFlightFareSummaryList() != null && ondFares.getFlightFareSummaryList().size() > 0) {
	// for (FlightFareSummaryTO ondFare : ondFares.getFlightFareSummaryList()) {
	// if (!ondFare.isWithFlexi() && ondFare.getBundledFareId() == null) {
	// baseReturnFare = ondFare.getTotalPrice();
	// break;
	// }
	// }
	// }
	// if (ondSegFares.getFlightFareSummaryList() != null && ondFares.getFlightFareSummaryList().size() > 0) {
	// for (FlightFareSummaryTO ondSegFare : ondSegFares.getFlightFareSummaryList()) {
	// if (!ondSegFare.isWithFlexi() && ondSegFare.getBundledFareId() == null) {
	// baseSegFare = ondSegFare.getTotalPrice();
	// break;
	// }
	// }
	// }
	// return (baseSegFare.compareTo(baseReturnFare) >= 0) ? true : false;
	// }

	private void setSelectedOndOption(List<AvailableOption> availOptions, Date preferredDate) {
		boolean isSelected = false;
		if (!availOptions.isEmpty()) {
			for (AvailableOption avOpt : availOptions) {
				if (avOpt.isSelected()) {
					isSelected = true;
					break;
				}
			}
			if (!isSelected) {
				for (AvailableOption avOpt : availOptions) {
					if (!avOpt.getSegments().isEmpty()
							&& CalendarUtil.getStartTimeOfDate((avOpt.getSegments().get(0).getDepartureDateTime().getLocal()))
									.compareTo(preferredDate) == 0) {
						avOpt.setSelected(true);
						break;
					}
				}
			}
		}
	}
}
