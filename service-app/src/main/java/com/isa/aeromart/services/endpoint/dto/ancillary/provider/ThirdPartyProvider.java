package com.isa.aeromart.services.endpoint.dto.ancillary.provider;

import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;

public class ThirdPartyProvider extends Provider {

	private Integer providerId;

    private String providerDescription;

    public ThirdPartyProvider() {
        setProviderType(ProviderType.THIRD_PARTY);
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public String getProviderDescription() {
        return providerDescription;
    }

    public void setProviderDescription(String providerDescription) {
        this.providerDescription = providerDescription;
    }
}
