package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import static com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils.adaptCollection;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.availability.AvailableFareClass;
import com.isa.aeromart.services.endpoint.dto.availability.AvailableOption;
import com.isa.aeromart.services.endpoint.dto.common.FlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class OndAvailableOptionAdaptor implements Adaptor<OriginDestinationOptionTO, AvailableOption> {

	private static Log log = LogFactory.getLog(OndAvailableOptionAdaptor.class);

	public boolean isFareOption;
	private RPHGenerator rphGenerator;
	private ReturnFareDiscountBuilder returnFareDiscountBuilder;

	private OndAvailableOptionAdaptor(boolean isFareOption, RPHGenerator rphGenerator,
			ReturnFareDiscountBuilder returnFareDiscountBuilder) {
		this.isFareOption = isFareOption;
		this.rphGenerator = rphGenerator;
		this.returnFareDiscountBuilder = returnFareDiscountBuilder;
	}

	@Override
	public AvailableOption adapt(OriginDestinationOptionTO source) {
		String totalDurationStr = "00:00";
		AvailableOption target = new AvailableOption();
		target.setSeatAvailable(source.isSeatAvailable());
		target.setSelected(source.isSelected());
		adaptCollection(source.getFlightSegmentList(), target.getSegments(), new SegmentResponseAdaptor(rphGenerator));
		setOriginAndDestination(source.getFlightSegmentList(), target);
	
		if (source.getFlightFareSummaryList() != null) {
			adaptCollection(source.getFlightFareSummaryList(), target.getAvailableFareClasses(), getAdaptor());
		}
		try {
			DateFormat HHmm = new SimpleDateFormat(CalendarUtil.PATTERN13);
			Date totalDuration = HHmm.parse(totalDurationStr);
			for (FlightSegment segment : target.getSegments()) {
				if (segment.getDuration() != null) {
					String[] segDurArr = segment.getDuration().split(":");
					totalDuration = CalendarUtil.add(totalDuration, 0, 0, 0, Integer.parseInt(segDurArr[0]),
						Integer.parseInt(segDurArr[1]), 0);
				}
			}
			totalDurationStr = Integer.toString(totalDuration.getHours()) + ":" + Integer.toString(totalDuration.getMinutes());
			target.setTotalDuration(totalDurationStr);
		} catch (ParseException e) {
			log.error("Error in segment duration : OndAvailableOptionAdaptor ==> adapt", e);
		}
		return target;
	}

	private void setOriginAndDestination(List<FlightSegmentTO> flightSegmentList, AvailableOption target) {
		String originAirportCode = null;
		String destinationAirportCode = null;
		if (flightSegmentList != null) {
			for (FlightSegmentTO flightSegmentTo : flightSegmentList) {
				String segmentCode = flightSegmentTo.getSegmentCode();
				if (originAirportCode == null) {
					originAirportCode = SegmentUtil.getFromAirport(segmentCode);
				}
				destinationAirportCode = SegmentUtil.getToAirport(segmentCode);
			}
		}
		target.setOriginAirportCode(originAirportCode);
		target.setDestinationAirportCode(destinationAirportCode);
	}

	private Adaptor<FlightFareSummaryTO, AvailableFareClass> getAdaptor() {
		if (isFareOption) {
			return new AvailableFareClassRSAdaptor(returnFareDiscountBuilder);
		} else {
			return new NullAdaptor<FlightFareSummaryTO, AvailableFareClass>();
		}
	}

	public static OndAvailableOptionAdaptor getAdatorForFareOption(RPHGenerator rphGenerator,
			ReturnFareDiscountBuilder returnFareDiscountBuilder) {
		return new OndAvailableOptionAdaptor(true, rphGenerator, returnFareDiscountBuilder);
	}

	public static OndAvailableOptionAdaptor getAdatorForFlightOption(RPHGenerator rphGenerator) {
		return new OndAvailableOptionAdaptor(false, rphGenerator, null);
	}
}
