package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

public class LmsCredits {
	private String airRewardId;
	private BigDecimal availablePoints;// payOptRes
	private BigDecimal availablePointsAmount;// payOptRes
	private String paymentCurrency;// payOptRes, effMulPayRes
	private BigDecimal maxRedeemableAmount;// payOptRes
	private BigDecimal minRedeemableAmount;// payOptRes
	private boolean redeemLoyaltyPoints;// effMulPayReq/Res, makePayReq
	private BigDecimal redeemedAmount; // effPayRes, effPayRes
	private BigDecimal redeemRequestAmount; // effPayReq, effPayReq, makePayReq
	private boolean isRedeemed;

	public BigDecimal getAvailablePoints() {
		return availablePoints;
	}

	public void setAvailablePoints(BigDecimal availablePoints) {
		this.availablePoints = availablePoints;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public BigDecimal getRedeemRequestAmount() {
		return redeemRequestAmount;
	}

	public void setRedeemRequestAmount(BigDecimal redeemRequestAmount) {
		this.redeemRequestAmount = redeemRequestAmount;
	}

	

	public boolean isRedeemLoyaltyPoints() {
		return redeemLoyaltyPoints;
	}

	public void setRedeemLoyaltyPoints(boolean redeemLoyaltyPoints) {
		this.redeemLoyaltyPoints = redeemLoyaltyPoints;
	}

	public String getAirRewardId() {
		return airRewardId;
	}

	public void setAirRewardId(String airRewardId) {
		this.airRewardId = airRewardId;
	}

	public BigDecimal getMaxRedeemableAmount() {
		return maxRedeemableAmount;
	}

	public void setMaxRedeemableAmount(BigDecimal maxRedeemableAmount) {
		this.maxRedeemableAmount = maxRedeemableAmount;
	}

	public BigDecimal getMinRedeemableAmount() {
		return minRedeemableAmount;
	}

	public void setMinRedeemableAmount(BigDecimal minRedeemableAmount) {
		this.minRedeemableAmount = minRedeemableAmount;
	}

	public BigDecimal getAvailablePointsAmount() {
		return availablePointsAmount;
	}

	public void setAvailablePointsAmount(BigDecimal availablePointsAmount) {
		this.availablePointsAmount = availablePointsAmount;
	}

	public boolean isRedeemed() {
		return isRedeemed;
	}

	public void setRedeemed(boolean isRedeemed) {
		this.isRedeemed = isRedeemed;
	}

	public BigDecimal getRedeemedAmount() {
		return redeemedAmount;
	}

	public void setRedeemedAmount(BigDecimal redeemedAmount) {
		this.redeemedAmount = redeemedAmount;
	}

}
