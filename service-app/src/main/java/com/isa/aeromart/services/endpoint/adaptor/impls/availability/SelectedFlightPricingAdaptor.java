package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import static com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils.adaptCollection;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.SelectedFlightPricing;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public class SelectedFlightPricingAdaptor implements Adaptor<PriceInfoTO, SelectedFlightPricing> {

	private TravellerQuantity travellerQuantity;
	private PriceInfoTO selectedSegFlightPriceInfo;
	private SYSTEM searchSystem;
	private TrackInfoDTO trackInfoDTO;
	private String transactionId;

	public SelectedFlightPricingAdaptor(TravellerQuantity travellerQuantity, PriceInfoTO selectedSegFlightPriceInfo,
			SYSTEM searchSystem, TrackInfoDTO trackInfoDTO, String transactionId) {
		this.travellerQuantity = travellerQuantity;
		this.selectedSegFlightPriceInfo = selectedSegFlightPriceInfo;
		this.searchSystem = searchSystem;
		this.trackInfoDTO = trackInfoDTO;
		this.transactionId = transactionId;
	}

	@Override
	public SelectedFlightPricing adapt(PriceInfoTO source) {
		boolean isDisplaySegFares = false;
		SelectedFlightPricing target = new SelectedFlightPricing();
		List<OndOWPricing> returnOndPricing = new ArrayList<OndOWPricing>();
		OndOWPricingAdaptor ondReturnPriceAdapt = new OndOWPricingAdaptor(travellerQuantity);
		OndOWPricingAdaptor ondPriceAdapt = new OndOWPricingAdaptor(travellerQuantity);
		adaptCollection(source.getFareTypeTO().getSurcharges(), target.getSurcharges(), new SurchargeAdaptor());
		adaptCollection(source.getFareTypeTO().getTaxes(), target.getTaxes(), new TaxAdaptor());
		adaptCollection(source.getFareTypeTO().getApplicableFareRules(), target.getFareRules(), new FareRuleRSAdaptor());
		if (selectedSegFlightPriceInfo != null && selectedSegFlightPriceInfo.getFareTypeTO() != null
				&& selectedSegFlightPriceInfo.getFareTypeTO().getTotalPrice() != null
				&& source.getFareTypeTO() != null
				&& source.getFareTypeTO().getTotalPrice() != null
				&& selectedSegFlightPriceInfo.getFareTypeTO().getTotalPrice().compareTo(source.getFareTypeTO().getTotalPrice()) > 0) {
			target.getOndOWPricing().addAll(ondPriceAdapt.adapt(selectedSegFlightPriceInfo.getFareTypeTO()));
			target.setServiceTaxAmountForSegmentFares(selectedSegFlightPriceInfo.getFareTypeTO().getServiceTaxAmount());
			returnOndPricing.addAll(ondReturnPriceAdapt.adapt(source.getFareTypeTO()));
			isDisplaySegFares = true;
		} else {
			target.getOndOWPricing().addAll(ondPriceAdapt.adapt(source.getFareTypeTO()));
			target.setServiceTaxAmountForSegmentFares(source.getFareTypeTO().getServiceTaxAmount());
		}
		if (source.getFareTypeTO().getPromotionTO() != null) {
			PromotionRSAdaptor promoTrans = new PromotionRSAdaptor(searchSystem, travellerQuantity, trackInfoDTO,
					source, transactionId);
			target.getPromoInfo().add(promoTrans.adapt(source.getFareTypeTO().getPromotionTO()));
		}
		TotalAdaptor totalAdapt = new TotalAdaptor(selectedSegFlightPriceInfo, target.getPromoInfo(), isDisplaySegFares
				? returnOndPricing
				: target.getOndOWPricing());
		target.setTotal(totalAdapt.adapt(source));

		return target;
	}

}
