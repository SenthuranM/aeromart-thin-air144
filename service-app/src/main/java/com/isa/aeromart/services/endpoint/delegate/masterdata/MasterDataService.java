package com.isa.aeromart.services.endpoint.delegate.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.masterdata.ContactConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.UserRegistrationConfigRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.UserRegistrationConfigRS;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface MasterDataService {

	public PaxDetailsConfigRS paxDetails(PaxDetailsConfigRQ paxDetailsConfigReq, TrackInfoDTO trackInfo, String searchSystem,
			List<String> carrierList, List<SessionFlightSegment> segments);
	
	public PaxContactDetailsRS paxContactDetails(PaxContactDetailsRQ paxContactDetailsReq, List<String> carrierList,
			TrackInfoDTO trackInfo);
	
	public UserRegistrationConfigRS getUserRegistrationConfig(UserRegistrationConfigRQ paxContactDetailsReq,  TrackInfoDTO trackInfo) throws ModuleException;

	public CurrencyExchangeRate getCurrencyExchangeRate(String currency);

	public PaxDetailsConfigRS unifiedPaxDetails(PaxConfigCollectionRQ paxDetailsConfigReq, TrackInfoDTO trackInfo,
			String searchSystem, List<String> carrierList, List<SessionFlightSegment> segments);

	public PaxContactDetailsRS unifiedContactDetails(ContactConfigCollectionRQ contactDetailsReq, List<String> carrierList,
			TrackInfoDTO trackInfo);
}
