package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SsrInFlightServiceRequestAdaptor extends AncillaryRequestAdaptor {

	public String getLCCAncillaryType() {
		return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.SSR;
	}

	public Object toAvailableAncillaries(TrackInfoDTO trackInfo, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {
		AvailableSsrInFlightServiceRequestWrapper wrapper = new AvailableSsrInFlightServiceRequestWrapper();

		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
		List<FlightSegmentTO> filteredFlightSegmentTOs = new ArrayList<>();
		
		try {
			for(FlightSegmentTO flightSegmentTO : flightSegmentTOs){
				if(!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())){
					filteredFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.ANCI_AVAILABLE_GENERIC_ERROR, e);
		}

		wrapper.setFlightSegmentTOs(filteredFlightSegmentTOs);
		wrapper.setTransactionIdentifier(isModifyAnci? null : lccTransactionId != null ? lccTransactionId : "");
		wrapper.setSystem(system); 
		wrapper.setSelectedLanguage(LocaleContextHolder.getLocale().toString());
		wrapper.setTrackInfo(trackInfo);
		wrapper.setFltRefWiseSelectedSSR(null);
		wrapper.setIsModifyAnci(isModifyAnci);
		wrapper.setIsGdsPnr(false);

		return wrapper;
	}

	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {

		List<SelectedItem> selectedItems;
		SegmentScope segmentScope;
		String flightSegmentRPH;

		LCCSpecialServiceRequestDTO selectedinFlihgtSSR;
		List<LCCSpecialServiceRequestDTO> selectedinFlihgtSSRs;
		Map<String, List<LCCSpecialServiceRequestDTO>> selectedInFlightSSRsMapping = new HashMap<>();

		for (Preference preference : preferences) {
			List<Selection> selections = preference.getSelections();
			for (Selection selection : selections) {
				segmentScope = (SegmentScope) selection.getScope();
				flightSegmentRPH = segmentScope.getFlightSegmentRPH();

				if (!selectedInFlightSSRsMapping.containsKey(flightSegmentRPH)) {
					selectedInFlightSSRsMapping.put(flightSegmentRPH, new ArrayList<>());
				}

				selectedinFlihgtSSRs = selectedInFlightSSRsMapping.get(flightSegmentRPH);

				selectedItems = selection.getSelectedItems();
				for (SelectedItem selectedItem : selectedItems) {
					selectedinFlihgtSSR = new LCCSpecialServiceRequestDTO();
					selectedinFlihgtSSR.setSsrCode(selectedItem.getId());
					selectedinFlihgtSSRs.add(selectedinFlihgtSSR);
				}

			}
		}

		return selectedInFlightSSRsMapping;
	}

	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;

	}

	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;

	}

	public class AvailableSsrInFlightServiceRequestWrapper {
		private List<FlightSegmentTO> flightSegmentTOs;
		private String transactionIdentifier;
		private ProxyConstants.SYSTEM system;
		private String selectedLanguage;
		private BasicTrackInfo trackInfo;
		private Map<String, Set<String>> fltRefWiseSelectedSSR;
		private boolean isModifyAnci;
		private boolean isGdsPnr;

		public List<FlightSegmentTO> getFlightSegmentTOs() {
			return flightSegmentTOs;
		}

		public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
			this.flightSegmentTOs = flightSegmentTOs;
		}

		public String getTransactionIdentifier() {
			return transactionIdentifier;
		}

		public void setTransactionIdentifier(String transactionIdentifier) {
			this.transactionIdentifier = transactionIdentifier;
		}

		public ProxyConstants.SYSTEM getSystem() {
			return system;
		}

		public void setSystem(ProxyConstants.SYSTEM system) {
			this.system = system;
		}

		public String getSelectedLanguage() {
			return selectedLanguage;
		}

		public void setSelectedLanguage(String selectedLanguage) {
			this.selectedLanguage = selectedLanguage;
		}

		public BasicTrackInfo getTrackInfo() {
			return trackInfo;
		}

		public void setTrackInfo(BasicTrackInfo trackInfo) {
			this.trackInfo = trackInfo;
		}

		public Map<String, Set<String>> getFltRefWiseSelectedSSR() {
			return fltRefWiseSelectedSSR;
		}

		public void setFltRefWiseSelectedSSR(Map<String, Set<String>> fltRefWiseSelectedSSR) {
			this.fltRefWiseSelectedSSR = fltRefWiseSelectedSSR;
		}

		public boolean isModifyAnci() {
			return isModifyAnci;
		}

		public void setIsModifyAnci(boolean isModifyAnci) {
			this.isModifyAnci = isModifyAnci;
		}

		public boolean isGdsPnr() {
			return isGdsPnr;
		}

		public void setIsGdsPnr(boolean isGdsPnr) {
			this.isGdsPnr = isGdsPnr;
		}
	}
}
