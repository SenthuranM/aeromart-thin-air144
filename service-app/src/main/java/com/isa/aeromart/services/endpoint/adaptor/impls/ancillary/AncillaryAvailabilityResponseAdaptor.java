package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRS;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

public class AncillaryAvailabilityResponseAdaptor extends TransactionAwareAdaptor<AnciAvailabilityRS, AncillaryAvailabilityRS> {

	public AncillaryAvailabilityRS adapt(AnciAvailabilityRS source) {

		AncillaryAvailabilityRS transformed = new AncillaryAvailabilityRS();
		List<LCCAncillaryAvailabilityOutDTO> filteredAvailableAncillaries = new ArrayList<>();
		transformed.setTransactionId(getTransactionId());

		Map<AncillariesConstants.AncillaryType, Boolean> availabilityMap = new LinkedHashMap<>();
		AncillariesConstants.AncillaryType ancillaryType;

		// filter ancillary with buffer times
		try {
			filteredAvailableAncillaries = AnciCommon.checkBufferTimes(source.getLccAncillaryAvailabilityOutDTOs(),
					AnciCommon.isModifyOperation(getTransactionalAncillaryService(), getTransactionId()));
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.ANCI_AVAILABILITY_GENERIC_ERROR, e);
		}

		for (LCCAncillaryAvailabilityOutDTO availability : filteredAvailableAncillaries) {
			for (LCCAncillaryStatus status : availability.getAncillaryStatusList()) {
				ancillaryType = AnciCommon.getAncillaryType(status.getAncillaryType().getAncillaryType());
				if(availabilityMap.containsKey(ancillaryType)){
					if(!availabilityMap.get(ancillaryType) && status.isAvailable()){
						availabilityMap.put(ancillaryType, status.isAvailable());
					}
				} else {
					availabilityMap.put(ancillaryType, status.isAvailable());
				}
				
			}

		}

		List<AncillaryAvailability> availabilityList = new ArrayList<>();
		AncillaryAvailability availability;
		boolean isAvailable;

		for (AncillariesConstants.AncillaryType anciType : availabilityMap.keySet()) {
			isAvailable = availabilityMap.get(anciType);

			if (anciType == AncillariesConstants.AncillaryType.FLEXIBILITY) {
				List<SessionFlexiDetail> sessionFlexiList = getTransactionalAncillaryService().getSessionFlexiDetail(
						getTransactionId());
				boolean sessionFlexi = false;
				if (sessionFlexiList != null && sessionFlexiList.size() > 0) {
					for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiList) {
						if (sessionFlexiDetail.isFlexiAvailable()) {
							sessionFlexi = true;
							break;
						}
					}
				}

				isAvailable = sessionFlexi;
			} else if (anciType == AncillariesConstants.AncillaryType.SSR_IN_FLIGHT) {
				if (isAvailable && !AppSysParamsUtil.isSSREnabled()
						&& SalesChannelsUtil.isAppIndicatorWebOrMobile(getTrackInfoDTO().getAppIndicator())) {
					isAvailable = false;
				}
			}

			availability = new AncillaryAvailability();
			availability.setType(anciType.getCode());
			availability.setAvailable(isAvailable);
			availabilityList.add(availability);
		}

		transformed.setAvailability(availabilityList);
		transformed.setSuccess(true);
		return transformed;
	}
}
