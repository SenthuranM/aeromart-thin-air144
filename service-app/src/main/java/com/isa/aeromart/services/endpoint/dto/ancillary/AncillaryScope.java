package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class AncillaryScope {

	private Scope scope;

	private List<PricedSelectedItem> ancillaries;

	private List<MonetaryAmendment> amendments;

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public List<PricedSelectedItem> getAncillaries() {
		return ancillaries;
	}

	public void setAncillaries(List<PricedSelectedItem> ancillaries) {
		this.ancillaries = ancillaries;
	}

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}
}
