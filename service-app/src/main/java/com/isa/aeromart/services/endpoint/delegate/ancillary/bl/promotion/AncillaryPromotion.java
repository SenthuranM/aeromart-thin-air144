package com.isa.aeromart.services.endpoint.delegate.ancillary.bl.promotion;

import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;


public interface AncillaryPromotion {

	public void setTransactionalId(String transactionId);
	
	public void setTransactionalAncillaryService(TransactionalAncillaryService transactionalAncillaryService);
	
	public void setTrackInfo(TrackInfoDTO trackInfoDTO);
	
	public ReservationDiscountDTO calculatePromotion() throws ModuleException;
	
	public PriceQuoteAncillaryRS applyPromotion(PriceQuoteAncillaryRS priceQuoteAncillaryRS, ReservationDiscountDTO resDiscountDTO, Integer definitionId);
	
}
