package com.isa.aeromart.services.endpoint.adaptor.impls.availability.response;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.SelectedFlightPricingAdaptor;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.FareCalendarSelectedRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public class FareCalendarSelectedRSAdaptor implements Adaptor<FlightAvailRS, FareCalendarSelectedRS> {

	private AvailabilitySearchRQ availabilitySearchReq;
	private RPHGenerator rphGenerator;
	private TrackInfoDTO trackInfo;
	private SYSTEM searchSystem;

	public FareCalendarSelectedRSAdaptor(AvailabilitySearchRQ availabilitySearchReq, RPHGenerator rphGenerator,
			TrackInfoDTO trackInfo, SYSTEM searchSystem) {
		this.availabilitySearchReq = availabilitySearchReq;
		this.rphGenerator = rphGenerator;
		this.trackInfo = trackInfo;
		this.searchSystem = searchSystem;
	}

	@Override
	public FareCalendarSelectedRS adapt(FlightAvailRS source) {
		FareCalendarSelectedRS response = new FareCalendarSelectedRS();
		if (source != null) {
			FareCalendarRSAdaptor fareCalAdaptor = new FareCalendarRSAdaptor(availabilitySearchReq, rphGenerator);
			response = new FareCalendarSelectedRS(fareCalAdaptor.adapt(source));
			if (source.getSelectedPriceFlightInfo() != null) {
				SelectedFlightPricingAdaptor selFlightPriceTrans = new SelectedFlightPricingAdaptor(
						availabilitySearchReq.getTravellerQuantity(), source.getSelectedSegmentFlightPriceInfo(), searchSystem,
						trackInfo, source.getTransactionIdentifier());
				response.setSelectedFlightPricing(selFlightPriceTrans.adapt(source.getSelectedPriceFlightInfo()));
			}
			response.setSuccess(true);
		}
		return response;
	}
}