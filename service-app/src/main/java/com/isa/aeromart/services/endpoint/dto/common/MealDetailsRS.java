package com.isa.aeromart.services.endpoint.dto.common;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;

public class MealDetailsRS extends TransactionalBaseRS {

	private List<LCCMealDTO> mealList;

	public List<LCCMealDTO> getMealList() {
		return mealList;
	}

	public void setMealList(List<LCCMealDTO> mealList) {
		this.mealList = mealList;
	}

}