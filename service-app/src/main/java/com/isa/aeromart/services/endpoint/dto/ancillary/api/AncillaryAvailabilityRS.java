package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class AncillaryAvailabilityRS extends TransactionalBaseRS{
	
	private List<AncillaryAvailability> availability;

	public List<AncillaryAvailability> getAvailability() {
		return availability;
	}

	public void setAvailability(List<AncillaryAvailability> availability) {
		this.availability = availability;
	}

}
