package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.ArrayList;
import java.util.List;

public class FareCalendarRS extends FlightCalendarRS {

	private List<FareClass> fareClasses;

	private String currency;

	private List<String> paymentOptions;

	public FareCalendarRS() {
	}

	public FareCalendarRS(FlightCalendarRS flightCalendarResponse) {
		super(flightCalendarResponse);
	}

	public FareCalendarRS(FareCalendarRS fareCalendarResponse) {
		super(fareCalendarResponse);
		setFareClasses(fareCalendarResponse.getFareClasses());
		setCurrency(fareCalendarResponse.getCurrency());
		setPaymentOptions(fareCalendarResponse.getPaymentOptions());
	}

	public List<FareClass> getFareClasses() {
		if (fareClasses == null) {
			fareClasses = new ArrayList<FareClass>();
		}
		return fareClasses;
	}

	public void setFareClasses(List<FareClass> fareClass) {
		this.fareClasses = fareClass;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<String> getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(List<String> paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

}
