package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

import java.util.Date;

public class DateRestrict extends BasicRuleValidation{
	
	public DateRestrict() {
		setRuleCode(RuleCode.CALENDAR_DATE_RESTRICT);
		
	}
	
	private Date minDate;
	
	private Date maxDate;

	public Date getMinDate() {
		return minDate;
	}

	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}

	public Date getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

}
