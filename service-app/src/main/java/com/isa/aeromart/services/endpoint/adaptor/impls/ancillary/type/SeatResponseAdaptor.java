package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability.ApplicableType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Rule;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.seat.SeatItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.seat.SeatItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.BasicRuleValidation;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.RowSeatPassengerCount;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.SeatsPassengerTypes;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirColumnGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCCabinClassDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCPassengerTypeQuantityDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class SeatResponseAdaptor extends AncillariesResponseAdaptor {
	@Override
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability
				.isAnciActive(availability, LCCAncillaryAvailabilityInDTO.LCCAncillaryType.SEAT_MAP);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	@Override
	public AncillaryType
			toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		FlightSegmentTO flightSegmentTO;
		String flightSegmentRPH;
		String cabinType;
		String rowGroupId;
		String columnGroupId;
		int rowNumber;
		boolean isVacant;
		boolean paxTypeRestrictedRuleApplied;
		boolean isSelectedCabin;

		AncillaryType ancillaryType = new AncillaryType();
		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.SEAT.name());

		List<Provider> providers = new ArrayList<>();
		SystemProvided provider = new SystemProvided();
		provider.setProviderType(Provider.ProviderType.SYSTEM);

		List<AvailableAncillary> availableAncillay = new ArrayList<>();
		AvailableAncillary availableAncillary = new AvailableAncillary();

		List<AvailableAncillaryUnit> availableUnits = new ArrayList<>();
		AvailableAncillaryUnit availableUnit;

		SegmentScope segmentScope;
		Applicability applicability;

		SeatItems seatItems;
		List<SeatItem> seatItemList;
		SeatItem item;

		List<Charge> charges;
		Charge charge;

		int ruleIndex = 1;
		int ruleId;
		List<Rule> rules = new ArrayList<>();
		List<Integer> validations;
		RowSeatPassengerCount rowSeatPassengerValidation;
		SeatsPassengerTypes validation;
		Map<String, Integer> validationsMap = new HashMap<>();
		String textualRule;
		LCCSeatMapDTO source = (LCCSeatMapDTO) ancillaries;
		List<LCCSegmentSeatMapDTO> lccSegmentSeatMapDTOList = source.getLccSegmentSeatMapDTOs();
		Collections.sort(lccSegmentSeatMapDTOList);

		for (LCCSegmentSeatMapDTO lccSeatMap : lccSegmentSeatMapDTOList) {
			if (!lccSeatMap.getLccSeatMapDetailDTO().getCabinClass().isEmpty()) {
				availableUnit = new AvailableAncillaryUnit();

				flightSegmentTO = lccSeatMap.getFlightSegmentDTO();

				segmentScope = new SegmentScope();
				flightSegmentRPH = rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber());

				segmentScope.setFlightSegmentRPH(flightSegmentRPH);
				segmentScope.setScopeType(ScopeType.SEGMENT);
				availableUnit.setScope(segmentScope);

				applicability = new Applicability();
				applicability.setApplicableType(ApplicableType.PASSENGER.name());
				availableUnit.setApplicability(applicability);

				seatItems = new SeatItems();
				seatItemList = new ArrayList<>();

				for (LCCCabinClassDTO lccCabinClass : lccSeatMap.getLccSeatMapDetailDTO().getCabinClass()) {
					cabinType = lccCabinClass.getCabinType();
					isSelectedCabin = cabinType.equals(flightSegmentTO.getCabinClassCode());
					for (LCCAirRowGroupDTO lccRowGroup : lccCabinClass.getAirRowGroupDTOs()) {
						rowGroupId = lccRowGroup.getRowGroupId();

						for (LCCAirColumnGroupDTO lccColumnGroup : lccRowGroup.getAirColumnGroups()) {
							columnGroupId = lccColumnGroup.getColumnGroupId();

							for (LCCAirRowDTO lccRow : lccColumnGroup.getAirRows()) {
								paxTypeRestrictedRuleApplied = false;
								rowNumber = lccRow.getRowNumber();
								List<LCCPassengerTypeQuantityDTO> passengerTypeQuantityList = lccRow.getPassengerTypeQuantity();
								rowSeatPassengerValidation = new RowSeatPassengerCount();
								rowSeatPassengerValidation.setSeatRowId(rowNumber);
								rowSeatPassengerValidation.setRuleValidationId(ruleIndex++);
								rowSeatPassengerValidation.setRowSeatPassengetCountMap(AnciCommon
										.getRowSeatPassengerMap(passengerTypeQuantityList));
								for (LCCPassengerTypeQuantityDTO lccPassengerTypeQuantityDTO : passengerTypeQuantityList) {
									if (lccPassengerTypeQuantityDTO.getPaxType().equals(PaxTypeTO.INFANT)
											&& lccPassengerTypeQuantityDTO.getQuantity() == 0) {
										rowSeatPassengerValidation.setRuleMessage(errorMessageSource.getMessage(
												BasicRuleValidation.RuleCode.RESTRICTED_PAX_TYPES_FOR_SEAT_DESCRIPTION, null,
												LocaleContextHolder.getLocale()));
										paxTypeRestrictedRuleApplied = true;
									}
								}
								if (!paxTypeRestrictedRuleApplied) {
									rowSeatPassengerValidation.setRuleMessage(errorMessageSource.getMessage(
											BasicRuleValidation.RuleCode.PAX_TYPE_COUNT_FOR_ROW_SEATS_DESCRIPTION, null,
											LocaleContextHolder.getLocale()));
								}
								rules.add(rowSeatPassengerValidation);

								for (LCCAirSeatDTO lccSeat : lccRow.getAirSeats()) {
									item = new SeatItem();
									isVacant = lccSeat.getSeatAvailability().equals(
											AirinventoryCustomConstants.FlightSeatStatuses.VACANT);

									item.setItemId(lccSeat.getSeatNumber());
									item.setItemName(lccSeat.getSeatNumber());
									if (isSelectedCabin) {
										item.setAvailability(isVacant);
									} else {
										item.setAvailability(false);
									}
									item.setCabinType(cabinType);
									item.setColumnGroupId(columnGroupId);
									item.setRowGroupId(rowGroupId);
									item.setRowNumber(rowNumber);
									item.setSeatVisibility(lccSeat.getSeatVisibility());
									item.setNotAllowedPassengerType(lccSeat.getNotAllowedPassengerType());
									item.setSeatType(lccSeat.getSeatType());
									item.setSeatLocationType(lccSeat.getSeatLocationType());
									item.setValidations(new ArrayList<Integer>());
									// setting pax type wise count validation
									item.getValidations().add(rowSeatPassengerValidation.getRuleValidationId());

									charges = new ArrayList<>();
									charge = new Charge();
									charge.setChargeBasis(Charge.ChargeBasis.PER_PASSENGER.name());
									charge.setAmount(lccSeat.getSeatCharge());
									charges.add(charge);

									if (lccSeat.getNotAllowedPassengerType() != null
											&& !lccSeat.getNotAllowedPassengerType().isEmpty()) {
										textualRule = AnciCommon.toTextualRule(
												BasicRuleValidation.RuleCode.RESTRICTED_PAX_TYPES_FOR_SEAT,
												lccSeat.getNotAllowedPassengerType());

										if (!validationsMap.containsKey(textualRule)) {
											validation = new SeatsPassengerTypes();
											validation.setRuleValidationId(ruleIndex++);
											validation.setPaxTypes(lccSeat.getNotAllowedPassengerType());
											validation.setRuleMessage(errorMessageSource.getMessage(
													BasicRuleValidation.RuleCode.RESTRICTED_PAX_TYPES_FOR_SEAT_DESCRIPTION, null,
													LocaleContextHolder.getLocale()));

											validationsMap.put(textualRule, validation.getRuleValidationId());
											rules.add(validation);
										}

										ruleId = validationsMap.get(textualRule);
										item.getValidations().add(ruleId);

									}

									item.setCharges(charges);
									seatItemList.add(item);
								}
							}
						}
					}
				}

				seatItems.setItems(seatItemList);
				availableUnit.setItemsGroup(seatItems);
				availableUnits.add(availableUnit);
			}
		}

		availableAncillary.setAvailableUnits(availableUnits);
		availableAncillary.setValidationDefinitions(rules);
		availableAncillay.add(availableAncillary);

		provider.setAvailableAncillaries(availableAncillary);
		providers.add(provider);
		ancillaryType.setProviders(providers);

		return ancillaryType;

	}

	@Override
	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {
		LCCAirSeatDTO selectedSeat = new LCCAirSeatDTO();
		selectedSeat.setSeatNumber(item.getId());
		selectedSeat.setSeatCharge(item.getAmount());

		return selectedSeat;
	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

	}
}
