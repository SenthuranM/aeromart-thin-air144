package com.isa.aeromart.services.endpoint.utils.modification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.AnciPassengerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.ReservationPassengerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.availability.RequoteRQ;
import com.isa.aeromart.services.endpoint.dto.common.BalanceTo;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccResSegmentInfo;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.RequoteONDInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

public class ModificationUtils {

	public static final String RPH_SPLITTER = "\\$";
	public static final String RPH_SEPERATOR = "$";
	public static final String FLT_SEG_SEPERATOR = "//$";
	public static final String CARRIER_SEPERATOR = "#";

	private static Log log = LogFactory.getLog(ModificationUtils.class);

	public static Set<String> getRemovedResRPHList(RequoteRQ request, LCCClientReservation reservation) {

		Set<String> removedResRPHList = new HashSet<String>();
		if (request.isModifySegment()) {
			List<ModifiedFlight> modifiedList = request.getModificationInfo().getModifiedFlights();
			for (ModifiedFlight modifiedFlight : modifiedList) {
				removedResRPHList.addAll(getResSegUniqueIdentifierList(modifiedFlight.getFromResSegRPH(),
						reservation.getSegments()));
			}
		} else if (request.isCancelSegment()) {
			removedResRPHList.addAll(getResSegUniqueIdentifierList(request.getModificationInfo().getCnxFlights(),
					reservation.getSegments()));
		}
		return removedResRPHList;
	}

	public static Map<String, String> getReQuoteResSegmentMap(List<RequoteONDInfo> requoteONDList) {

		Map<String, String> requoteSegmentMap = new HashMap<String, String>();

		if (requoteONDList != null && requoteONDList.size() > 0) {
			Iterator<RequoteONDInfo> requoteONDListItr = requoteONDList.iterator();
			while (requoteONDListItr.hasNext()) {
				RequoteONDInfo requoteONDInfo = requoteONDListItr.next();

				List<String> flightSegIdList = requoteONDInfo.getFlightSegIdList();
				List<String> existResSegIdList = requoteONDInfo.getExistingResSegRPHList();
				List<String> modResSegIdList = requoteONDInfo.getModifiedResSegList();

				if (flightSegIdList != null && flightSegIdList.size() > 0) {
					int index = 0;
					for (String flightSegId : flightSegIdList) {
						String existSegId = null;
						if (existResSegIdList != null && existResSegIdList.size() > 0) {
							existSegId = existResSegIdList.get(index);
						}
						if (existSegId == null && modResSegIdList != null && modResSegIdList.size() > 0) {
							// To handle, modifying one sector booking to a connection booking.
							if (modResSegIdList.size() >= index + 1) {
								existSegId = modResSegIdList.get(index);
							} else {
								existSegId = modResSegIdList.get(modResSegIdList.size() - 1);
							}
						}
						requoteSegmentMap.put(flightSegId, existSegId);
						index++;
					}
				}
			}
		}
		return requoteSegmentMap;
	}

	public static Map<String, List<String>> getCarrierWiseRPHs(List<RequoteONDInfo> ondList) {

		Map<String, List<String>> carrierWiseFlightRPHMap = new HashMap<String, List<String>>();
		try {
			Map<String, String> busAirCarrierCodes = WPModuleUtils.getFlightServiceBD().getBusAirCarrierCodes();
			if (ondList != null && ondList.size() > 0) {
				for (RequoteONDInfo ond : ondList) {
					List<String> flightRPHList = ond.getFlightSegRPHlist();
					if (flightRPHList != null && flightRPHList.size() > 0) {
						for (String flightRPH : flightRPHList) {
							String carrierCode;
							if (flightRPH.indexOf(CARRIER_SEPERATOR) != -1) {
								String arr[] = flightRPH.split(CARRIER_SEPERATOR);
								carrierCode = FlightRefNumberUtil.getOperatingAirline(arr[0]);
							} else {
								carrierCode = FlightRefNumberUtil.getOperatingAirline(flightRPH);
							}
							// get the respective airline carrier for bus segment
							if (busAirCarrierCodes.containsKey(carrierCode)) {
								carrierCode = busAirCarrierCodes.get(carrierCode);
							}
							if (carrierWiseFlightRPHMap.get(carrierCode) != null) {
								carrierWiseFlightRPHMap.get(carrierCode).add(flightRPH);
							} else {
								List<String> tmpFlightRPH = new ArrayList<String>();
								tmpFlightRPH.add(flightRPH);
								carrierWiseFlightRPHMap.put(carrierCode, tmpFlightRPH);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			log.error("ModificationAdaptor getCarrierWiseRPHs caused Error", ex);
		}
		return carrierWiseFlightRPHMap;
	}

	public static Map<Integer, String> getONDFareTypeByFareIdMap(List<RequoteONDInfo> ondSearchList) {
		Map<Integer, String> fareTypeByFareIdMap = new HashMap<Integer, String>();
		if (ondSearchList != null && ondSearchList.size() > 0) {
			Iterator<RequoteONDInfo> ondSearchItr = ondSearchList.iterator();
			while (ondSearchItr.hasNext()) {
				RequoteONDInfo ond = ondSearchItr.next();
				if (getOwnOldPerPaxFareTO(ond) != null && getOwnOldPerPaxFareTO(ond).getFareType() != null) {
					fareTypeByFareIdMap.put(getOwnOldPerPaxFareTO(ond).getFareId(), getOwnOldPerPaxFareTO(ond).getFareType());
				}
			}
		}
		return fareTypeByFareIdMap;
	}

	public static Map<Integer, Integer> getOldFareIdByFltSegIdMap(List<RequoteONDInfo> ondSearchList) {
		Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();
		if (ondSearchList != null && ondSearchList.size() > 0) {
			Iterator<RequoteONDInfo> ondSearchItr = ondSearchList.iterator();
			while (ondSearchItr.hasNext()) {
				RequoteONDInfo ond = ondSearchItr.next();
				Integer oldFareId = null;
				if (getOwnOldPerPaxFareTO(ond) != null && getOwnOldPerPaxFareTO(ond).getFareType() != null) {
					oldFareId = getOwnOldPerPaxFareTO(ond).getFareId();
				}
				List<String> flightSegIdList = ond.getFlightSegIdList();
				if (flightSegIdList != null && flightSegIdList.size() > 0) {
					for (String flightSegId : flightSegIdList) {
						oldFareIdByFltSegIdMap.put(new Integer(flightSegId), oldFareId);
					}
				}
			}
		}
		return oldFareIdByFltSegIdMap;
	}

	public static FareTO getOwnOldPerPaxFareTO(RequoteONDInfo ondInfo) {
		if (ondInfo.getOldPerPaxFareTOList() != null) {
			String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
			String busCarrier = null;
			try {
				busCarrier = SelectListGenerator.createBusCarrierCode();
			} catch (Exception e) {
				// log error
			}
			for (FareTO fareTO : ondInfo.getOldPerPaxFareTOList()) {
				if (fareTO != null
						&& (defaultCarrier.equals(fareTO.getCarrierCode()) || (busCarrier != null && busCarrier.equals(fareTO
								.getCarrierCode())))) {
					return fareTO;
				}
			}
		}
		return null;
	}

	public static BigDecimal getTotalPaxCredit(Map<String, BigDecimal> paxCreditMap) {
		BigDecimal totalPaxCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxCreditMap != null && !paxCreditMap.isEmpty()) {
			Set<String> tmpKeySet = paxCreditMap.keySet();
			for (String key : tmpKeySet) {
				if (paxCreditMap.get(key).compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
					totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, paxCreditMap.get(key));
				}
			}
		}
		return totalPaxCredit;
	}

	public static BigDecimal getPaxExtCharges(List<LCCClientExternalChgDTO> list) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : list) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	public static Map<String, String> getGroundSegmentByFlightSegmentIDMap(FlightPriceRS flightPriceRS,
			Set<LCCClientReservationSegment> pnrSegments) {

		Map<String, String> BusSegByFltSegMap = new HashMap<String, String>();
		Map<String, String> oldBusSegFltSegMap = new HashMap<String, String>();
		for (LCCClientReservationSegment resSegment : pnrSegments) {
			if (resSegment.getGroundStationPnrSegmentID() != null) {
				for (LCCClientReservationSegment resSegmentInner : pnrSegments) {
					if (resSegment.getGroundStationPnrSegmentID().equals(resSegmentInner.getPnrSegID())) {
						oldBusSegFltSegMap.put(
								FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSegment.getFlightSegmentRefNumber()).toString(),
								FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSegmentInner.getFlightSegmentRefNumber())
										.toString());
					}
				}
			}
		}

		List<OriginDestinationInformationTO> ondInfoToList = flightPriceRS.getOriginDestinationInformationList();
		for (OriginDestinationInformationTO ondInfoTo : ondInfoToList) {
			if (ondInfoTo.isUnTouchedOnd()) {
				for (OriginDestinationOptionTO ondOptionTo : ondInfoTo.getOrignDestinationOptions()) {
					for (Integer fltSegId : ondOptionTo.getFlightSegIdList()) {
						if (oldBusSegFltSegMap.containsKey(fltSegId.toString())) {
							BusSegByFltSegMap.put(fltSegId.toString(), oldBusSegFltSegMap.get(fltSegId.toString()));
						}
					}
				}
			}
		}
		return BusSegByFltSegMap;
	}

	public static BalanceTo getTotalBalanceToPayAmount(ReservationBalanceTO lCCClientReservationBalanceTO,
			Collection<ReservationPaxTO> paxList) {
		BalanceTo balanceTo = new BalanceTo();

		BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (lCCClientReservationBalanceTO != null) {
			Collection<LCCClientPassengerSummaryTO> colPassengerSummary = lCCClientReservationBalanceTO.getPassengerSummaryList();

			if (colPassengerSummary != null) {
				BigDecimal paxAmountWithAncilary = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal paxAmountDueWithOutAncilary = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (LCCClientPassengerSummaryTO passenger : colPassengerSummary) {
					if (!PaxTypeTO.INFANT.equalsIgnoreCase(passenger.getPaxType())
							|| lCCClientReservationBalanceTO.isInfantPaymentSeparated()) {
						paxAmountWithAncilary = AccelAeroCalculator.getDefaultBigDecimalZero();
						paxAmountDueWithOutAncilary = AccelAeroCalculator.add(passenger.getTotalCreditAmount().negate(),
								passenger.getTotalAmountDue());

						BigDecimal paxChgs = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (paxList != null) {
							for (ReservationPaxTO paxTO : paxList) {
								String travelerRef = paxTO.getTravelerRefNumber().split(",")[0].trim();
								if (passenger.getTravelerRefNumber().contains(travelerRef)) {
									paxChgs = ModificationUtils.getPaxExtCharges(paxTO.getExternalCharges());
									break;
								}
							}
						}

						paxAmountWithAncilary = AccelAeroCalculator.add(paxAmountDueWithOutAncilary, paxChgs);

						if (paxAmountWithAncilary.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
							balanceToPay = AccelAeroCalculator.add(balanceToPay, paxAmountWithAncilary);
						} else {
							creditBalance = AccelAeroCalculator.add(creditBalance, paxAmountWithAncilary);
						}
					}
				}
			}
		}

		balanceTo.setBalanceToPay(balanceToPay);
		balanceTo.setTotalCreditBalance(creditBalance.abs());
		return balanceTo;
	}

	public static List<ReservationPaxTO> createPaxList(Set<LCCClientReservationPax> resPassengers) {
		ReservationPassengerAdaptor reservationPassengerAdaptor = new ReservationPassengerAdaptor();
		List<ReservationPaxTO> reservationPaxList = new ArrayList<ReservationPaxTO>();
		AdaptorUtils.adaptCollection(resPassengers, reservationPaxList, reservationPassengerAdaptor);
		return reservationPaxList;
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, String operatingCarrier,
			String sellingCarrier) {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(false);
		pnrModesDTO.setAppIndicator(ApplicationEngine.IBE);

		if (StringUtils.isNotEmpty(sellingCarrier)) {
			if (sellingCarrier.trim().length() == 2) {
				pnrModesDTO.setMarketingAirlineCode(sellingCarrier);
			}
		}

		if (StringUtils.isNotEmpty(operatingCarrier)) {
			if (operatingCarrier.trim().length() == 2) {
				pnrModesDTO.setAirlineCode(operatingCarrier);
			}
		}
		return pnrModesDTO;
	}

	public static Map<String, List<LccResSegmentInfo>> getOndWiseSegmentMap(Set<LCCClientReservationSegment> resSegList) {
		Map<String, List<LccResSegmentInfo>> ondWiseSegMap = new HashMap<String, List<LccResSegmentInfo>>();
		List<LCCClientReservationSegment> sortedResSegList = new ArrayList<LCCClientReservationSegment>(resSegList);
		Collections.sort(sortedResSegList);

		for (LCCClientReservationSegment segment : sortedResSegList) {
			if (!segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				if (ondWiseSegMap.get(segment.getInterlineGroupKey()) == null) {
					ArrayList<LccResSegmentInfo> segList = new ArrayList<LccResSegmentInfo>();
					ondWiseSegMap.put(segment.getInterlineGroupKey(), segList);
				}
				ondWiseSegMap.get(segment.getInterlineGroupKey()).add(getResSegmentInfo(segment));
			}
		}

		return ondWiseSegMap;
	}

	public static LccReservationInfo getLccReservationInfo(LCCClientReservation clientReservation) {
		LccReservationInfo lccReservationInfo = new LccReservationInfo();
		lccReservationInfo.setLastFareQuoteDate(clientReservation.getSegments().iterator().next().getLastFareQuoteDate());
		lccReservationInfo.setTicketValiedityLastDate(clientReservation.getTicketValidTill());
		lccReservationInfo.setTotalChildPaxCount(clientReservation.getTotalPaxChildCount());
		lccReservationInfo.setTotalInfantPaxCount(clientReservation.getTotalPaxInfantCount());
		lccReservationInfo.setTotalPaxAdultCount(clientReservation.getTotalPaxAdultCount());
		lccReservationInfo.setLccSegmentInfos(getLccSegmentInfo(clientReservation.getSegments()));
		return lccReservationInfo;
	}

	private static LccResSegmentInfo getResSegmentInfo(LCCClientReservationSegment segment) {
		LccResSegmentInfo segmentInfo = new LccResSegmentInfo();
		segmentInfo.setCarrierCode(segment.getCarrierCode());
		segmentInfo.setGroundStationPnrSegmentID(segment.getGroundStationPnrSegmentID());
		segmentInfo.setPnrSegID(segment.getPnrSegID());
		segmentInfo.setSegmentSeq(segment.getSegmentSeq());
		segmentInfo.setDepartureDate(segment.getDepartureDate());
		segmentInfo.setFlightSegmentRefNumber(segment.getFlightSegmentRefNumber());
		segmentInfo.setFlownSegment(segment.isFlownSegment());
		segmentInfo.setBookingType(segment.getBookingType());
		segmentInfo.setSegmentCode(segment.getSegmentCode());
		segmentInfo.setFlexiRuleID(segment.getFlexiRuleID());
		segmentInfo.setUnSegment(segment.isUnSegment());
		segmentInfo.setFareTO(segment.getFareTO());
		segmentInfo.setLogicalCabinClass(segment.getLogicalCabinClass());
		segmentInfo.setCabinClass(segment.getCabinClassCode());
		segmentInfo.setBundleFarePeriodId(segment.getBundledServicePeriodId());
		segmentInfo.setFlightNo(segment.getFlightNo());
		segmentInfo.setRouteRefNumber(segment.getRouteRefNumber());
		segmentInfo.setStatus(segment.getStatus());
		return segmentInfo;
	}

	public static Set<LccResSegmentInfo> getLccSegmentInfo(Collection<LCCClientReservationSegment> segments) {
		Set<LccResSegmentInfo> lccSegmentInfos = new HashSet<LccResSegmentInfo>();
		for (LCCClientReservationSegment segment : segments) {
			lccSegmentInfos.add(getResSegmentInfo(segment));

		}
		return lccSegmentInfos;
	}

	/*
	 * public static Map<String, List<String>> getondWiseResSegUnqIdentifierMap(Set<LCCClientReservationSegment>
	 * resSegs) { Map<String, List<String>> ondwiseResSegUnqIdentifierMap = new HashMap<String, List<String>>();
	 * 
	 * for (LCCClientReservationSegment seg : resSegs) { if
	 * (!seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) { if
	 * (ondwiseResSegUnqIdentifierMap.get(seg.getInterlineGroupKey()) == null) { ArrayList<String> segUnqIdentifierList
	 * = new ArrayList<String>(); ondwiseResSegUnqIdentifierMap.put(seg.getInterlineGroupKey(), segUnqIdentifierList); }
	 * ondwiseResSegUnqIdentifierMap
	 * .get(seg.getInterlineGroupKey()).add(createResSegUnqIdatifier(seg.getFlightSegmentRefNumber(),
	 * seg.getPnrSegID())); } } return ondwiseResSegUnqIdentifierMap; }
	 */

	public static String createResSegUnqIdatifier(String flightSegRph, int pnrSegd) {
		String[] arr = flightSegRph.split(RPH_SPLITTER);
		arr[2] = String.valueOf(pnrSegd);
		String resSegRPH = StringUtils.join(arr, RPH_SEPERATOR);

		return resSegRPH;
	}

	public static List<String>
			getResSegUniqueIdentifierList(List<String> resRphList, Set<LCCClientReservationSegment> resSeglist) {
		List<String> resSegUnqIdlist = new ArrayList<String>();
		Map<String, LCCClientReservationSegment> resRPHtoSegMap = new HashMap<String, LCCClientReservationSegment>();

		for (LCCClientReservationSegment seg : resSeglist) {
			resRPHtoSegMap.put(createResSegRph(seg.getCarrierCode(), seg.getSegmentSeq()), seg);
		}

		for (String rph : resRphList) {
			LCCClientReservationSegment seg = resRPHtoSegMap.get(rph);
			resSegUnqIdlist.add(createResSegUnqIdatifier(seg.getFlightSegmentRefNumber(), seg.getPnrSegID()));
		}

		return resSegUnqIdlist;
	}

	public static List<ReservationPaxTO> getPaxDetailsWithAnciModification(FinancialStore financialStore,
			Collection<LCCClientPassengerSummaryTO> passengerSummaryList) throws ModuleException {
		List<ReservationPaxTO> resPaxList = new ArrayList<ReservationPaxTO>();
		if (financialStore.getAncillaryExternalCharges().getPassengers() != null) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> anciSessionPaxList = financialStore.getAncillaryExternalCharges()
					.getPassengers();
			AdaptorUtils.adaptCollection(anciSessionPaxList, resPaxList, new AnciPassengerAdaptor(passengerSummaryList));
		}
		return resPaxList;
	}

	public static List<IInsuranceRequest> populateInsurance(List<LCCClientReservationInsurance> lccInsQuoteDTOs, String pnr,
			int adultCount, TrackInfoDTO trackDTO) throws ModuleException {

		List<LCCClientReservationInsurance> lccInsurances = new ArrayList<LCCClientReservationInsurance>();

		if (lccInsQuoteDTOs != null && !lccInsQuoteDTOs.isEmpty()) {
			for (LCCClientReservationInsurance lccInsQuoteDTO : lccInsQuoteDTOs) {
				LCCClientReservationInsurance lccInsurance = new LCCClientReservationInsurance();
				LCCInsuredJourneyDTO insuredJourneyDTO = new LCCInsuredJourneyDTO();
				insuredJourneyDTO.setJourneyEndDate(lccInsQuoteDTO.getInsuredJourneyDTO().getJourneyEndDate());
				insuredJourneyDTO.setJourneyStartDate(lccInsQuoteDTO.getInsuredJourneyDTO().getJourneyStartDate());
				insuredJourneyDTO.setJourneyStartAirportCode(lccInsQuoteDTO.getInsuredJourneyDTO().getJourneyStartAirportCode());
				insuredJourneyDTO.setJourneyEndAirportCode(lccInsQuoteDTO.getInsuredJourneyDTO().getJourneyEndAirportCode());
				insuredJourneyDTO.setRoundTrip(lccInsQuoteDTO.getInsuredJourneyDTO().isRoundTrip());
				lccInsurance.setInsuranceQuoteRefNumber(lccInsQuoteDTO.getInsuranceQuoteRefNumber());
				lccInsurance.setInsuredJourneyDTO(insuredJourneyDTO);
				lccInsurance.setQuotedTotalPremium(lccInsQuoteDTO.getQuotedTotalPremium());
				lccInsurance.setApplicablePaxCount(adultCount);
				lccInsurance.setOperatingAirline(lccInsQuoteDTO.getOperatingAirline());
				lccInsurance.setSsrFeeCode(lccInsQuoteDTO.getSsrFeeCode());
				lccInsurance.setPlanCode(lccInsQuoteDTO.getPlanCode());

				lccInsurances.add(lccInsurance);
			}
		}

		List<IInsuranceRequest> insuranceRequests = new ArrayList<IInsuranceRequest>();

		IInsuranceRequest insurance = null;
		Reservation reservation = null;

		if (lccInsurances != null && !lccInsurances.isEmpty()) {
			LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
			modes.setPnr(pnr);
			reservation = ModuleServiceLocator.getReservationBD().getReservation(modes, trackDTO);

			for (LCCClientReservationInsurance lccInsurance : lccInsurances) {

				insurance = new InsuranceRequestAssembler();

				InsureSegment insSegment = new InsureSegment();
				LCCInsuredJourneyDTO insuredJourneyDTO = lccInsurance.getInsuredJourneyDTO();
				insSegment.setArrivalDate(insuredJourneyDTO.getJourneyEndDate());
				insSegment.setDepartureDate(insuredJourneyDTO.getJourneyStartDate());
				insSegment.setFromAirportCode(insuredJourneyDTO.getJourneyStartAirportCode());
				insSegment.setToAirportCode(insuredJourneyDTO.getJourneyEndAirportCode());
				insSegment.setRoundTrip(insuredJourneyDTO.isRoundTrip());
				insSegment.setSalesChanelCode(insuredJourneyDTO.getSalesChannel());

				insurance.setInsuranceId(new Integer(lccInsurance.getInsuranceQuoteRefNumber()));
				insurance.addFlightSegment(insSegment);
				insurance.setQuotedTotalPremiumAmount(lccInsurance.getQuotedTotalPremium());
				insurance.setInsuranceType(lccInsurance.getInsuranceType());
				insurance.setAllDomastic(lccInsQuoteDTOs.get(0).isAllDomastic());
				insurance.setSsrFeeCode(lccInsurance.getSsrFeeCode());
				insurance.setPlanCode(lccInsurance.getPlanCode());
				insurance.setOperatingAirline(lccInsurance.getOperatingAirline());

				int paxCount = 0;
				if (AppSysParamsUtil.allowAddInsurnaceForInfants()) {
					paxCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount()
							+ reservation.getTotalPaxInfantCount();
				} else {
					paxCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
				}

				BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(lccInsurance.getQuotedTotalPremium(), paxCount);
				int i = 0;
				for (ReservationPax pax : reservation.getPassengers()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType())
							|| (PaxTypeTO.INFANT.equals(pax.getPaxType()) && AppSysParamsUtil.allowAddInsurnaceForInfants())) {
						InsurePassenger insuredPassenger = new InsurePassenger();

						// adding AIG Pax info
						insuredPassenger.setFirstName(pax.getFirstName());
						insuredPassenger.setLastName(pax.getLastName());
						insuredPassenger.setDateOfBirth(pax.getDateOfBirth());

						ReservationContactInfo contactInfo = reservation.getContactInfo();
						// Adding AIG Contact Info
						insuredPassenger.setAddressLn1(contactInfo.getStreetAddress1());
						insuredPassenger.setAddressLn2(contactInfo.getStreetAddress2());
						insuredPassenger.setCity(contactInfo.getCity());
						insuredPassenger.setEmail(contactInfo.getEmail());
						insuredPassenger.setCountryOfAddress(contactInfo.getCountryCode());
						insuredPassenger.setHomePhoneNumber(contactInfo.getPhoneNo());

						insurance.addPassenger(insuredPassenger);
						if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
							insurance.addInsuranceCharge(pax.getPaxSequence(), BigDecimal.ZERO);
						} else {
							insurance.addInsuranceCharge(pax.getPaxSequence(), insPaxWise[i++]);
						}
					}
				}

				insuranceRequests.add(insurance);
			}

		}
		return insuranceRequests;

	}

	public static String createResSegRph(String carrierCode, Integer segmentSeq) {
		return carrierCode + RPH_SEPERATOR + segmentSeq.toString();
	}

	public static boolean isSameFlightModification(List<String> extFltSegRph, List<String> modifiedFlightSegRph,
			RPHGenerator rphGenerator) {
		List<String> fltSegRphList = getFltUniqueIdentifierList(modifiedFlightSegRph, rphGenerator);
		if (fltSegRphList.containsAll(extFltSegRph) && extFltSegRph.size() == fltSegRphList.size()) {
			return true;
		}
		return false;
	}

	public static List<String> getFltUniqueIdentifierList(List<String> rphList, RPHGenerator rphGenerator) {
		List<String> unqIdentifireList = new ArrayList<String>();
		for (String rph : rphList) {
			unqIdentifireList.add(rphGenerator.getUniqueIdentifier(rph));
		}
		return unqIdentifireList;
	}

	public static boolean isPartiallyFlownReservation(Set<LCCClientReservationSegment> segments) {
		boolean hasFlownSegment = false;
		boolean hasUnflownSegment = false;
		for (LCCClientReservationSegment seg : segments) {
			if (seg.isFlownSegment()) {
				hasFlownSegment = true;
			} else {
				hasUnflownSegment = true;
			}
		}

		if (hasUnflownSegment && hasFlownSegment) {
			return true;
		}

		return false;
	}

	public static boolean isUnFlownlownReservation(Set<LCCClientReservationSegment> segments) {
		for (LCCClientReservationSegment seg : segments) {
			if (seg.isFlownSegment()) {
				return false;
			}
		}
		return true;
	}

	public static void updateEffectiveTax(List<PassengerChargeTo<LCCClientExternalChgDTO>> anciPaxChargeTo,
			Map<Integer, List<ExternalChgDTO>> paxEffectiveTax) {

		for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : anciPaxChargeTo) {
			List<LCCClientExternalChgDTO> lccClientExternalChgDTOs = passengerChargeTo.getGetPassengerCharges();
			boolean resetAll = false;
			if (paxEffectiveTax != null && !paxEffectiveTax.isEmpty()) {
				if (paxEffectiveTax.containsKey(passengerChargeTo.getPassengerRph())) {
					List<ExternalChgDTO> effectiveExtCharges = paxEffectiveTax.get(passengerChargeTo.getPassengerRph());
					updateWithEffectiveCharges(lccClientExternalChgDTOs, effectiveExtCharges);
				} else {
					resetAll = true;
				}

			} else {
				resetAll = true;
			}

			if (resetAll) {
				for (LCCClientExternalChgDTO lccClientExternalChgDTO : lccClientExternalChgDTOs) {
					if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.JN_ANCI) {
						lccClientExternalChgDTO.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}
				}
			}
		}
	}

	private static void updateWithEffectiveCharges(List<LCCClientExternalChgDTO> externalCharges,
			List<ExternalChgDTO> effectiveExtCharges) {
		if (effectiveExtCharges != null && !effectiveExtCharges.isEmpty()) {
			for (ExternalChgDTO effectiveExtChg : effectiveExtCharges) {
				if (effectiveExtChg instanceof ServiceTaxExtChgDTO) {
					ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) effectiveExtChg;
					for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalCharges) {
						if (lccClientExternalChgDTO.getExternalCharges() == serviceTaxExtChgDTO.getExternalChargesEnum()
								&& serviceTaxExtChgDTO.getFlightRefNumber().equals(lccClientExternalChgDTO.getFlightRefNumber())) {
							lccClientExternalChgDTO.setAmount(serviceTaxExtChgDTO.getAmount());
							break;
						}
					}
				}
			}
		}
	}

	public static List<ReservationPaxTO> includePassangerExternalCharges(List<ReservationPaxTO> allResPaxList,
			List<ReservationPaxTO> anciUpdatedPaxList) {

		if (allResPaxList != null) {
			for (ReservationPaxTO reservationPaxTO : allResPaxList) {
				for (ReservationPaxTO anciSessionPaxTO : anciUpdatedPaxList) {
					if (reservationPaxTO.getTravelerRefNumber().equals(anciSessionPaxTO.getTravelerRefNumber())) {
						if (!reservationPaxTO.getExternalCharges().isEmpty()) {
							reservationPaxTO.getExternalCharges().clear();
						}
						reservationPaxTO.addExternalCharges(anciSessionPaxTO.getExternalCharges());
					}
				}
			}
		} else {
			return anciUpdatedPaxList;
		}

		return allResPaxList;
	}

}
