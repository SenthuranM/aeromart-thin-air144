package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;

public class ReservationDetailsGridDataRS extends TransactionalBaseRS {
	
	private String language;
	
	private String type;
	
	private String pnr;

    private String alertId;
    
    private String pnrSegmentId;
    
	private List<Map<String, Object>> reservationDetailsList = new ArrayList<Map<String,Object>>();
	
	private List<SelfReprotectFlightDTO> flightsList = new ArrayList<SelfReprotectFlightDTO>();
	
	private SelfReprotectFlightDTO oldFlightSegment;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getAlertId() {
		return alertId;
	}

	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	public String getPnrSegmentId() {
		return pnrSegmentId;
	}

	public void setPnrSegmentId(String pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	public List<Map<String, Object>> getReservationDetailsList() {
		return reservationDetailsList;
	}

	public void setReservationDetailsList(List<Map<String, Object>> reservationDetailsList) {
		this.reservationDetailsList = reservationDetailsList;
	}

	public List<SelfReprotectFlightDTO> getFlightsList() {
		return flightsList;
	}

	public void setFlightsList(List<SelfReprotectFlightDTO> flightsList) {
		this.flightsList = flightsList;
	}

	public SelfReprotectFlightDTO getOldFlightSegment() {
		return oldFlightSegment;
	}

	public void setOldFlightSegment(SelfReprotectFlightDTO oldFlightSegment) {
		this.oldFlightSegment = oldFlightSegment;
	}
	
}
