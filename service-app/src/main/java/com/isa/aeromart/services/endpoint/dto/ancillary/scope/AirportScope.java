package com.isa.aeromart.services.endpoint.dto.ancillary.scope;

public class AirportScope extends SegmentScope {

    public enum Point {
        DEPARTURE, TRANSIT, ARRIVAL
    }

    private String point;
    
    private String airportCode;

    public AirportScope() {
        setScopeType(ScopeType.AIRPORT);
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
    
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (getClass() != obj.getClass()) {
	        return false;
	    }
	    final AirportScope other = (AirportScope) obj;
	    if ((this.airportCode == null) ? (other.airportCode != null) : !this.airportCode.equals(other.airportCode)) {
	        return false;
	    }
	    if ((this.getFlightSegmentRPH() == null) ? (other.getFlightSegmentRPH() != null) : !this.getFlightSegmentRPH().equals(other.getFlightSegmentRPH())) {
	        return false;
	    }
	    return true;
	}
	
	@Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + (this.airportCode != null ? this.airportCode.hashCode() : 0);
	    return hash;
	}

}
