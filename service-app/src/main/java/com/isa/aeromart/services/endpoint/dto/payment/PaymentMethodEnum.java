package com.isa.aeromart.services.endpoint.dto.payment;

public enum PaymentMethodEnum {
	VISA, MASTER, CASH, LMS

}
