package com.isa.aeromart.services.endpoint.dto.booking;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.TravelModeAdaptor.TravelMode;
import com.isa.aeromart.services.endpoint.dto.common.DateTime;

public class ReservationSegmentLite {

	private String operatingCarrier;

	private DateTime departureDateTime;

	private DateTime arrivalDateTime;

	private String segmentCode;

	private String flightDesignator;

	private String reservationSegmentRPH;

	private String status;

	private String subStatus;

	private int segmentSequence;

	private int ondSequence;

	private String totalChargeAmount;
	
	private String departureAirportTerminalName;

	private TravelMode travelMode;
	
	private boolean returnSegment;
	
	private boolean flexiAvailable;

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public DateTime getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(DateTime departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public DateTime getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(DateTime arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	public String getReservationSegmentRPH() {
		return reservationSegmentRPH;
	}

	public void setReservationSegmentRPH(String reservationSegmentRPH) {
		this.reservationSegmentRPH = reservationSegmentRPH;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public int getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public String getTotalChargeAmount() {
		return totalChargeAmount;
	}

	public void setTotalChargeAmount(String totalChargeAmount) {
		this.totalChargeAmount = totalChargeAmount;
	}

	public String getDepartureAirportTerminalName() {
		return departureAirportTerminalName;
	}

	public void setDepartureAirportTerminalName(String departureAirportTerminalName) {
		this.departureAirportTerminalName = departureAirportTerminalName;
	}

	public TravelMode getTravelMode() {
		return travelMode;
	}

	public void setTravelMode(TravelMode travelMode) {
		this.travelMode = travelMode;
	}

    public boolean isReturnSegment() {
		return returnSegment;
	}

	public void setReturnSegment(boolean returnSegment) {
		this.returnSegment = returnSegment;
	}

	public boolean isFlexiAvailable() {
		return flexiAvailable;
	}

	public void setFlexiAvailable(boolean flexiAvailable) {
		this.flexiAvailable = flexiAvailable;
	}
}
