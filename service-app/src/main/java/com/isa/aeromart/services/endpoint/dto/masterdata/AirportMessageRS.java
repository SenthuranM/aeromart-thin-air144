package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class AirportMessageRS extends TransactionalBaseRS {

	List<AirportMessage> airportMessages;

	public List<AirportMessage> getAirportMessages() {
		return airportMessages;
	}

	public void setAirportMessages(List<AirportMessage> airportMessages) {
		this.airportMessages = airportMessages;
	}

}
