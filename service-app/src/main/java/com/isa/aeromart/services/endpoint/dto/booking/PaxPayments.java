package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Collection;

public class PaxPayments {

	private Collection<PaymentInfo> paymantInfo;

	private int paxSequence;

	public Collection<PaymentInfo> getPaymantInfo() {
		return paymantInfo;
	}

	public void setPaymantInfo(Collection<PaymentInfo> paymantInfo) {
		this.paymantInfo = paymantInfo;
	}

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

}
