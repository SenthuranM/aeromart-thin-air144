package com.isa.aeromart.services.endpoint.dto.modification;

public class OverridedCustomCharges {
	
	private double cancellationCharge;
	
	private double modificationCharge;
	
	private double oldCharge;
	
	private boolean absoluteValuesPassed;
	
	private String calculationType;

	/**
	 * @return the cancellationCharge
	 */
	public double getCancellationCharge() {
		return cancellationCharge;
	}

	/**
	 * @param cancellationCharge the cancellationCharge to set
	 */
	public void setCancellationCharge(double cancellationCharge) {
		this.cancellationCharge = cancellationCharge;
	}

	/**
	 * @return the modificationCharge
	 */
	public double getModificationCharge() {
		return modificationCharge;
	}

	/**
	 * @param modificationCharge the modificationCharge to set
	 */
	public void setModificationCharge(double modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	/**
	 * @return the oldCharge
	 */
	public double getOldCharge() {
		return oldCharge;
	}

	/**
	 * @param oldCharge the oldCharge to set
	 */
	public void setOldCharge(double oldCharge) {
		this.oldCharge = oldCharge;
	}

	/**
	 * @return the absoluteValuesPassed
	 */
	public boolean isAbsoluteValuesPassed() {
		return absoluteValuesPassed;
	}

	/**
	 * @param absoluteValuesPassed the absoluteValuesPassed to set
	 */
	public void setAbsoluteValuesPassed(boolean absoluteValuesPassed) {
		this.absoluteValuesPassed = absoluteValuesPassed;
	}

	/**
	 * @return the calculationType
	 */
	public String getCalculationType() {
		return calculationType;
	}

	/**
	 * @param calculationType the calculationType to set
	 */
	public void setCalculationType(String calculationType) {
		this.calculationType = calculationType;
	}
	
	
	
}
