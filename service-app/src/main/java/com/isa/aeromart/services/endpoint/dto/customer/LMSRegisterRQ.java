package com.isa.aeromart.services.endpoint.dto.customer;

public class LMSRegisterRQ {

	private String customerID;
    
    private LMSDetails lmsDetails;
       
    private boolean registration;

	private LoyaltyProfileDetailsRQ loyaltyProfileDetails;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public LMSDetails getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LMSDetails lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean isRegistration() {
		return registration;
	}

	public void setRegistration(boolean registration) {
		this.registration = registration;
	}

	public LoyaltyProfileDetailsRQ getLoyaltyProfileDetails() {
		return loyaltyProfileDetails;
	}

	public void setLoyaltyProfileDetails(LoyaltyProfileDetailsRQ loyaltyProfileDetails) {
		this.loyaltyProfileDetails = loyaltyProfileDetails;
	}

}
