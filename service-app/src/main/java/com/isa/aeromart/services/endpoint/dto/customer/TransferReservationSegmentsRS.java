package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class TransferReservationSegmentsRS extends TransactionalBaseRS {
	
	private String pnr;

	private boolean groupPnr;

	//private String operationType;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	/*public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}*/
	
}
