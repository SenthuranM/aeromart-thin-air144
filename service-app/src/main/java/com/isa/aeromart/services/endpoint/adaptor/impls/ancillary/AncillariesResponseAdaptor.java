package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;

public abstract class AncillariesResponseAdaptor {

	public abstract AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability);

	public List<OndUnit> getOndUnits(Object ancillaries, RPHGenerator rphGenerator) {
		return new ArrayList<>();
	}

	public abstract AncillaryType toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource );

	public abstract Object toAncillaryItemModel(Scope scope, PricedSelectedItem item);
	
	public abstract void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService, String transactionId);

}
