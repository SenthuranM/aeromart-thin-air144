package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxTitle;
import com.isa.thinair.airmaster.api.dto.PaxTitleDTO;

public class PaxTypeWiseTitleAdaptor implements Adaptor<Map<String, List<PaxTitleDTO>>, Map<String, List<PaxTitle>>> {

	@Override
	public Map<String, List<PaxTitle>> adapt(Map<String, List<PaxTitleDTO>> paxTitleMap) {
		PaxTitleAdaptor paxTitleAdaptor = new PaxTitleAdaptor();
		Map<String, List<PaxTitle>> paxTypeWiseTitles = new HashMap<>();
		for (Map.Entry<String, List<PaxTitleDTO>> entry : paxTitleMap.entrySet()) {
			List<PaxTitle> paxTitles = new ArrayList<>();
			AdaptorUtils.adaptCollection(entry.getValue(), paxTitles, paxTitleAdaptor);
			paxTypeWiseTitles.put(entry.getKey(), paxTitles);
		}
		return paxTypeWiseTitles;
	}

}
