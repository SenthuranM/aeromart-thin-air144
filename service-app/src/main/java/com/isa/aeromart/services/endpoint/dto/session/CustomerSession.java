package com.isa.aeromart.services.endpoint.dto.session;

import java.io.Serializable;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;

public class CustomerSession implements Serializable , CustomerSessionStore{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String SESSION_KEY = "USER_SESSION";

	private String loggedInCustomerID;
	
	private double availableCredit;

	private double availableLMSPoints;

	private String ffid;
	
	private String authToken;
	
	private String lmsRemoteId;
	
	private String memberEnrollingCarrier;

	private RPHGenerator familyMemberIDHolder;

	@Override
	public String getLoggedInCustomerID() {
		return loggedInCustomerID;
	}

	@Override
	public void storeLoggedInCustomerID(String customerID) {
		loggedInCustomerID = customerID;
	}

	@Override
	public double getAvailableCredit() {
		return availableCredit;
	}

	@Override
	public void storeAvailableCredit(double availableCredit) {
		this.availableCredit = availableCredit;
	}

	@Override
	public double getAvailableLMSPoints() {
		return availableLMSPoints;
	}

	@Override
	public void storeAvailableLMSPoints(double availableLMSPoints) {
		this.availableLMSPoints = availableLMSPoints;
	}

	@Override
	public void storeFFID(String ffid) {
		this.ffid = ffid;
	}

	@Override
	public String getFFID() {
		return ffid;
	}

	@Override
	public void clearSession() {
		
		loggedInCustomerID = null;
		availableCredit = 0;
		availableLMSPoints = 0;
		ffid = null;
		authToken = null;
		lmsRemoteId = null;
		memberEnrollingCarrier=null;
		
	}
	
	@Override
	public String getMemberAuth() {
		
		return authToken;
	}

	@Override
	public void storeMemberAuth(String authToken) {
		this.authToken = authToken;
	}
	

	@Override
	public String getLMSRemoteId() {
		return lmsRemoteId;
	}
	
	@Override
	public void storeLMSRemoteId(String lmsRemoteId) {
		this.lmsRemoteId = lmsRemoteId;
	}

	@Override
	public void storeMemberEnrollimgCarrier(String memberEnrollingCarrier) {
		this.memberEnrollingCarrier =memberEnrollingCarrier;
		
	}

	@Override
	public String getMemberEnrollingCarrier() {
		return this.memberEnrollingCarrier;
	}

	@Override
	public RPHGenerator getFamilyMemberIDHolder() {
		if (familyMemberIDHolder == null) {
			familyMemberIDHolder = new RPHGenerator();
		}
		return familyMemberIDHolder;
	}

}
