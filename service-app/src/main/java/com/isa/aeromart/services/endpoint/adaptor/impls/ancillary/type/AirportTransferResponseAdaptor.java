package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability.ApplicableType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Rule;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AirportTransferInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.airporttransfer.AirportTransferItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.airporttransfer.AirportTransferItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.airporttransfer.AirportTransferProvider;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.DateRestrict;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.EditAncillary;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AirportTransferResponseAdaptor extends AncillariesResponseAdaptor {

	@Override
	public AncillaryType toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		AncillaryType ancillaryType = new AncillaryType();
		List<Provider> providers = new ArrayList<Provider>();
		String flightSegmentRPH;
		FlightSegmentTO flightSegmentTO;
		DateRestrict dateRestrictRule;
		Calendar calendar;
		
		List<Rule> rules = new ArrayList<>();
		Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> source = (Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO>) ancillaries;

		int ruleIndex = 0;
		EditAncillary anciEditRule = new EditAncillary();
		anciEditRule.setEditAncillary(AppSysParamsUtil.isAllowEditAirportTransfers());
		anciEditRule.setRuleValidationId(++ruleIndex);
		if(anciModify){
			rules.add(anciEditRule);
		}	

		for (AirportServiceKeyTO airportServiceKeyTO : source.keySet()) {
			
			AirportTransferProvider airportTransferProvider = new AirportTransferProvider();
			AvailableAncillary availableAncillary = new AvailableAncillary();
			List<AvailableAncillaryUnit> availableUnits = new ArrayList<AvailableAncillaryUnit>();
			AvailableAncillaryUnit availableAncillaryUnit = new AvailableAncillaryUnit();
			AirportTransferItems airportTransferItems = new AirportTransferItems();
			List<AirportTransferItem> airportTransferItemsList = new ArrayList<AirportTransferItem>();
			AirportScope airportScope = new AirportScope();

			airportTransferProvider.setProviderType(Provider.ProviderType.THIRD_PARTY);
			LCCFlightSegmentAirportServiceDTO lCCFlightSegmentAirportServiceDTO = source.get(airportServiceKeyTO);
			flightSegmentTO = lCCFlightSegmentAirportServiceDTO.getFlightSegmentTO();
			flightSegmentRPH = rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber());

			dateRestrictRule = new DateRestrict();
			dateRestrictRule.setRuleValidationId(++ruleIndex);
			calendar = Calendar.getInstance();
						
			switch (airportServiceKeyTO.getAirportType()) {
			case "A":
				airportScope.setPoint(AirportScope.Point.ARRIVAL.name());
				dateRestrictRule.setMinDate(flightSegmentTO.getArrivalDateTime());
				calendar.setTime(flightSegmentTO.getArrivalDateTime());
				calendar.add(Calendar.DATE, 1); //TODO configure according to provider information
				dateRestrictRule.setMaxDate(calendar.getTime());
				break;
			case "D":
				airportScope.setPoint(AirportScope.Point.DEPARTURE.name());
				calendar.setTime(flightSegmentTO.getDepartureDateTime());
				calendar.add(Calendar.DATE, -1); //TODO configure according to provider information
				dateRestrictRule.setMinDate(calendar.getTime());
				dateRestrictRule.setMaxDate(flightSegmentTO.getDepartureDateTime());
				break;
			case "T":
				airportScope.setPoint(AirportScope.Point.TRANSIT.name());
				break;
			}

			rules.add(dateRestrictRule);
			Applicability applicability = new Applicability();
			applicability.setApplicableType(ApplicableType.PASSENGER.name());
			
			for (LCCAirportServiceDTO lccAirportServiceDTO : lCCFlightSegmentAirportServiceDTO.getAirportServices()) {

				AirportTransferItem airportTransferItem = new AirportTransferItem();
				airportTransferItem.setValidations(new ArrayList<Integer>());
				List<Charge> charges = new ArrayList<Charge>();
				Charge charge;

				airportScope.setScopeType(ScopeType.AIRPORT);
				airportScope.setFlightSegmentRPH(flightSegmentRPH);
				airportScope.setAirportCode(lccAirportServiceDTO.getAirportCode());
				
				availableAncillaryUnit.setScope(airportScope);
				availableAncillaryUnit.setApplicability(applicability);

				airportTransferItem.setDescription(lccAirportServiceDTO.getSsrDescription());
				airportTransferItem.setItemId(lccAirportServiceDTO.getAirportTransferId().toString());
				airportTransferItem.setAirportCode(lccAirportServiceDTO.getAirportCode());
				airportTransferItem.setSsrName(lccAirportServiceDTO.getSsrName());

				if (lccAirportServiceDTO.getApplicabilityType().equals(LCCAirportServiceDTO.APPLICABILITY_TYPE_PAX)) {

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.ADULT.name());
					charge.setAmount(lccAirportServiceDTO.getAdultAmount());
					charges.add(charge);

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.CHILD.name());
					charge.setAmount(lccAirportServiceDTO.getChildAmount());
					charges.add(charge);

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.INFANT.name());
					charge.setAmount(lccAirportServiceDTO.getInfantAmount());
					charges.add(charge);

				} else if (lccAirportServiceDTO.getApplicabilityType()
						.equals(LCCAirportServiceDTO.APPLICABILITY_TYPE_RESERVATION)) {

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.ALL_SEGMENTS.name());
					charge.setAmount(lccAirportServiceDTO.getReservationAmount());
					charges.add(charge);
				}

				airportTransferItem.setCharges(charges);
				if (anciModify) {
					airportTransferItem.getValidations().add(anciEditRule.getRuleValidationId());
				}
				airportTransferItem.getValidations().add(dateRestrictRule.getRuleValidationId());
				airportTransferItemsList.add(airportTransferItem);

				if (lccAirportServiceDTO.getProvider() != null) {
					airportTransferProvider.setAddress(lccAirportServiceDTO.getProvider().getAddress());
					airportTransferProvider.setAirportTransferId(lccAirportServiceDTO.getProvider().getAirportTransferId()
							.toString());
					airportTransferProvider.setEmailId(lccAirportServiceDTO.getProvider().getEmailId());
					airportTransferProvider.setName(lccAirportServiceDTO.getProvider().getName());
					airportTransferProvider.setNumber(lccAirportServiceDTO.getProvider().getNumber());
				}

				
				airportTransferItems.setItems(airportTransferItemsList);
				availableAncillaryUnit.setItemsGroup(airportTransferItems);
				availableUnits.add(availableAncillaryUnit);
				availableAncillary.setAvailableUnits(availableUnits);
				availableAncillary.setValidationDefinitions(new ArrayList<Rule>());
				availableAncillary.setValidationDefinitions(rules);
				airportTransferProvider.setAvailableAncillaries(availableAncillary);
				providers.add(airportTransferProvider);

			}
		}

		ancillaryType.setProviders(providers);
		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.AIRPORT_TRANSFER.name());

		return ancillaryType;
	}

	@Override
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability,
				LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AIRPORT_TRANSFER);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	@Override
	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {
		AirportScope airportScope = (AirportScope)scope;
		AirportTransferInput airportTransferInput = (AirportTransferInput) item.getInput();
		LCCAirportServiceDTO selectedAirportTransfer = new LCCAirportServiceDTO();
		selectedAirportTransfer.setSsrCode(item.getId());
		selectedAirportTransfer.setAirportCode(airportScope.getAirportCode());
		selectedAirportTransfer.setServiceCharge(item.getAmount());
		selectedAirportTransfer.setAirportTransferId(Integer.parseInt(item.getId()));
		selectedAirportTransfer.setApplyOn(airportTransferInput.getApplyOn());
		selectedAirportTransfer.setTranferDate(CalendarUtil.getDateInFormattedString(CalendarUtil.PATTERN11, airportTransferInput.getTranferDate()));
		selectedAirportTransfer.setTransferAddress(airportTransferInput.getTransferAddress());
		selectedAirportTransfer.setTransferContact(airportTransferInput.getTransferContact());
		selectedAirportTransfer.setTransferType(airportTransferInput.getTransferType());
		return selectedAirportTransfer;
	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

	}
}
