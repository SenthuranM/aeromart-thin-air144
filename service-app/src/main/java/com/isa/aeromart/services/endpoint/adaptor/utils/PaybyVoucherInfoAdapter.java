package com.isa.aeromart.services.endpoint.adaptor.utils;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;

public class PaybyVoucherInfoAdapter implements Adaptor<VoucherRedeemResponse,PayByVoucherInfo> {

	@Override
	public PayByVoucherInfo adapt(VoucherRedeemResponse voucherRedeemResponse) {
		PayByVoucherInfo payByVoucherInfo = new PayByVoucherInfo();
		payByVoucherInfo.getVoucherDTOList().addAll(voucherRedeemResponse.getRedeemVoucherList());
		payByVoucherInfo.setRedeemedTotal(new BigDecimal(voucherRedeemResponse.getRedeemedTotal()));
		payByVoucherInfo.setVouchersTotal(voucherRedeemResponse.getVouchersTotal());
		payByVoucherInfo.setVoucherPaymentOption(voucherRedeemResponse.getVoucherPaymentOption());
		payByVoucherInfo.setBalTotalPay(voucherRedeemResponse.getBalTotalPay());
		payByVoucherInfo.setVoucherRedeemDateTime(new Date());
		return payByVoucherInfo;
	}

}

