package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessageFlightSegmentInfo;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessageRQParams;
import com.isa.thinair.airreservation.api.dto.AirportMessageFilter;

public class AirportMessageFilterAdaptor implements Adaptor<AirportMessageFlightSegmentInfo, AirportMessageFilter> {
	private AirportMessageRQParams requestParams;

	public AirportMessageFilterAdaptor() {

	}

	public AirportMessageFilterAdaptor(AirportMessageRQParams requestParams) {
		this.requestParams = requestParams;
	}

	@Override
	public AirportMessageFilter adapt(AirportMessageFlightSegmentInfo source) {
		AirportMessageFilter target = new AirportMessageFilter();
		target.setArrAirport(source.getDestination());
		target.setDepAirport(source.getOrigin());
		target.setFlightDepDate(source.getDepartureDateTimeZulu());
		target.setFlightNumber(source.getFilghtDesignator());
		target.setFromDate(source.getDepartureDateTimeZulu());
		target.setToDate(source.getArrivalDateTimeZulu());

		target.setReturnFlag(source.isReturnFlag());
		target.setStage(requestParams.getStage());
		target.setSalesChanel(requestParams.getSalesChanel());

		return target;
	}
}
