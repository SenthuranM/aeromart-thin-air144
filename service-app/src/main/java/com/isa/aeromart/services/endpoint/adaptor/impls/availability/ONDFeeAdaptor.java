package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

class ONDFeeAdaptor extends ONDBaseChargeAdaptor implements ONDChargeAdaptor<FeeTO> {

	public ONDFeeAdaptor(ONDPriceAdaptor ondAdaptor) {
		super(ondAdaptor);
	}

	@Override
	public void addCharge(String paxType, FeeTO fee) {
		PaxPrice paxPrice = getPaxPrice(paxType, fee);
		paxPrice.setSurcharge(AccelAeroCalculator.add(paxPrice.getSurcharge(), fee.getAmount()));
		paxPrice.setTotal(AccelAeroCalculator.add(paxPrice.getTotal(), fee.getAmount()));
		OndOWPricing ondPrice = getOndPrice(fee.getOndSequence());
		ondPrice.setTotalSurcharges(AccelAeroCalculator.add(ondPrice.getTotalSurcharges(), fee.getAmount()));
		ondPrice.setTotalPrice(AccelAeroCalculator.add(ondPrice.getTotalPrice(), fee.getAmount()));
	}

}