package com.isa.aeromart.services.endpoint.dto.booking;

public class AnciModificationParams {

	private boolean anciModifiable;

	public boolean isAnciModifiable() {
		return anciModifiable;
	}

	public void setAnciModifiable(boolean anciModifiable) {
		this.anciModifiable = anciModifiable;
	}
}
