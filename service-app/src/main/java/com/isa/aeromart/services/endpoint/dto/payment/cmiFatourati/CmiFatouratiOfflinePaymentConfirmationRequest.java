package com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati;

import java.io.Serializable;

public class CmiFatouratiOfflinePaymentConfirmationRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private String orderNumber;
	private String pnr;
	private String  resCode;
	private String resMessage;
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getResCode() {
		return resCode;
	}
	public void setResCode(String resCode) {
		this.resCode = resCode;
	}
	public String getResMessage() {
		return resMessage;
	}
	public void setResMessage(String resMessage) {
		this.resMessage = resMessage;
	}
	@Override
	public String toString() {
		return "CmiFatouratiOfflinePaymentConfirmationRequest [orderNumber="
				+ orderNumber + ", pnr=" + pnr + ", resCode=" + resCode
				+ ", resMessage=" + resMessage + "]";
	}
	
	

}
