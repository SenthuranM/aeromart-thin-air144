package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRQ;

public class CustomerSaveOrUpdateReq extends BaseRQ{

	private CustomerDetails customer;
	
	private LMSDetails lmsDetails;

    private boolean optLMS;

	public CustomerDetails getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDetails customer) {
		this.customer = customer;
	}

	public LMSDetails getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LMSDetails lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean isOptLMS() {
		return optLMS;
	}

	public void setOptLMS(boolean optLMS) {
		this.optLMS = optLMS;
	}

}
