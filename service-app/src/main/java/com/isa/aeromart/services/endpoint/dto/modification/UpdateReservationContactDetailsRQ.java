package com.isa.aeromart.services.endpoint.dto.modification;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class UpdateReservationContactDetailsRQ extends TransactionalBaseRQ{
	@NotNull
	private ReservationContact contactInfo;
	@NotNull
	private String pnr;
	@NotNull
	private boolean groupPnr;
	@NotNull
	private String version;

	/**
	 * @return the contactInfo
	 */
	public ReservationContact getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo the contactInfo to set
	 */
	public void setContactInfo(ReservationContact contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the groupPnr
	 */
	public boolean isGroupPnr() {
		return groupPnr;
	}

	/**
	 * @param groupPnr the groupPnr to set
	 */
	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}	

}
