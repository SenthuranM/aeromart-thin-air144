package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.List;



public class ModifiedFlight {

	// old reservation segment RPHs
	private List<String> fromResSegRPH;
	
	private BasePreferences preferences;

	// new flight segment RPHs
	private List<String> toFlightSegRPH;

	public List<String> getFromResSegRPH() {
		return fromResSegRPH;
	}

	public void setFromResSegRPH(List<String> fromResSegRPH) {
		this.fromResSegRPH = fromResSegRPH;
	}

	public List<String> getToFlightSegRPH() {
		return toFlightSegRPH;
	}

	public void setToFlightSegRPH(List<String> toFlightSegRPH) {
		this.toFlightSegRPH = toFlightSegRPH;
	}

	public BasePreferences getPreferences() {
		return preferences;
	}

	public void setPreferences(BasePreferences preferences) {
		this.preferences = preferences;
	}
	
	

}
