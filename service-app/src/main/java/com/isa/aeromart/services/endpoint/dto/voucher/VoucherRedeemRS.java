package com.isa.aeromart.services.endpoint.dto.voucher;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;

public class VoucherRedeemRS extends TransactionalBaseRS {

    private PayByVoucherInfo payByVoucherInfo;
    public PayByVoucherInfo getPayByVoucherInfo() {
        return payByVoucherInfo;
    }
    public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
        this.payByVoucherInfo = payByVoucherInfo;
    }

}