package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.math.BigDecimal;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.BalanceTo;
import com.isa.aeromart.services.endpoint.dto.modification.RequoteBalanceInfo;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class RequoteBalanceAdaptor implements Adaptor<ReservationBalanceTO, RequoteBalanceInfo> {

	Map<String, BigDecimal> paxCreditMap;

	public RequoteBalanceAdaptor(Map<String, BigDecimal> paxCreditMap) {
		this.paxCreditMap = paxCreditMap;
	}

	@Override
	public RequoteBalanceInfo adapt(ReservationBalanceTO source) {
		RequoteBalanceInfo requoteBalanceInfo = new RequoteBalanceInfo();
		if (source != null) {
			BalanceTo balanceTo = ModificationUtils.getTotalBalanceToPayAmount(source, null);
			if (balanceTo.getBalanceToPay().compareTo(BigDecimal.ZERO) > 0) {
				requoteBalanceInfo.setBalanceToPay(balanceTo.getBalanceToPay());
			} else {
				requoteBalanceInfo.setTotalCreditBalance(balanceTo.getTotalCreditBalance());
			}
			requoteBalanceInfo.setModificationCharge(source.getSegmentSummary().getNewModAmount());
			requoteBalanceInfo.setCancelationCharge(source.getSegmentSummary().getNewCnxAmount());
			requoteBalanceInfo.setTotalReservationCredit(AccelAeroCalculator.add(source.getSegmentSummary().getCurrentRefunds(),
					ModificationUtils.getTotalPaxCredit(paxCreditMap).negate()));
			requoteBalanceInfo.setPaxWiseAdjustmentAmountMap(source.getPaxWiseAdjustmentAmountMap());
			requoteBalanceInfo.setPassengerSummaryList(source.getPassengerSummaryList());
		}
		return requoteBalanceInfo;
	}
}
