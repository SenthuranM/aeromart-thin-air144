package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public abstract class BaseCheckAncillaryRQ extends TransactionalBaseRQ {

    private MetaData metaData;

    private List<Ancillary> ancillaries;

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public List<Ancillary> getAncillaries() {
        return ancillaries;
    }

    public void setAncillaries(List<Ancillary> ancillaries) {
        this.ancillaries = ancillaries;
    }
}
