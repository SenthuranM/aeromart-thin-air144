package com.isa.aeromart.services.endpoint.dto.common;

public class SimpleResponse<T> {

	private T t;

	public SimpleResponse(T value) {
		this.t = value;
	}

	public T getValue() {
		return t;
	}

	public void setValue(T value) {
		this.t = value;
	}
}
