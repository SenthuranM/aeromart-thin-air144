package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryRS;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class AvailableAncillariesResponseAdaptor
		extends TransactionAwareAdaptor<Map<AncillariesConstants.AncillaryType, Object>, AncillaryRS> {

	public AncillaryRS adapt(Map<AncillariesConstants.AncillaryType, Object> source) {

		AncillaryRS response = new AncillaryRS();
        response.setTransactionId(getTransactionId());

		AncillariesResponseAdaptor responseTransformer;
		String anciType;
		List<AncillaryType> ancillaryTypes = new ArrayList<>();
		AncillaryType ancillaryType;

		MetaData metaData = new MetaData();
		List<OndUnit> ondPreferences = new ArrayList<>();

		AncillaryQuotation ancillaryQuotation = getTransactionalAncillaryService().getAncillaryQuotation(getTransactionId());
		AncillariesConstants.AncillaryType tempAncillaryType;

		for (AncillariesConstants.AncillaryType type : source.keySet()) {
			anciType = type.getCode();
			tempAncillaryType = AncillariesConstants.AncillaryType.resolveAncillaryType(anciType);

			if (tempAncillaryType == AncillariesConstants.AncillaryType.FLEXIBILITY && AnciCommon.isAncillaryQuoted(ancillaryQuotation, AncillariesConstants.AncillaryType.FLEXIBILITY)) {
				responseTransformer = AncillaryAdaptorFactory.getResponseAdaptor(anciType);
				ancillaryTypes.add(AnciCommon.getQuotedAncillary(ancillaryQuotation, AncillariesConstants.AncillaryType.FLEXIBILITY));

				ondPreferences.addAll(responseTransformer.getOndUnits(source.get(type), getTransactionalAncillaryService().getRphGenerator(getTransactionId())));
			} else if(tempAncillaryType == AncillariesConstants.AncillaryType.INSURANCE && AnciCommon.isAncillaryQuoted(ancillaryQuotation, AncillariesConstants.AncillaryType.INSURANCE)){
				ancillaryTypes.add(AnciCommon.getQuotedAncillary(ancillaryQuotation, AncillariesConstants.AncillaryType.INSURANCE));
			} else {
				if (source.get(type) != null) {
					responseTransformer = AncillaryAdaptorFactory.getResponseAdaptor(anciType);
					ancillaryType = responseTransformer.toAvailableAncillaryItems(source.get(type),
							getTransactionalAncillaryService().getRphGenerator(getTransactionId()), getTransactionalAncillaryService().isAncillaryTransaction(getTransactionId()), getErrorMessageSource());
					ancillaryTypes.add(ancillaryType);
					ondPreferences.addAll(responseTransformer.getOndUnits(source.get(type), getTransactionalAncillaryService()
							.getRphGenerator(getTransactionId())));
					responseTransformer.updateSessionData(source.get(type), getTransactionalAncillaryService(),
							getTransactionId());
				}
			}
		}

		response.setAncillaries(ancillaryTypes);

		metaData.setOndPreferences(ondPreferences);
		response.setMetaData(metaData);

		return response;
	}
}
