package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Date;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;

public class ReservationDetails implements Comparable<ReservationDetails> {

	private String airlineCode;

	private String marketingAirlineCode;

    private String paxName;

    private String pnr;

    private String pnrStatus;

	private List<SegmentDetails> segmentDetails;
    
    private Date bookingDate;
    
    private String contactLastName;
    
    private boolean flown = false;
    
    private TravellerQuantity travellerQuantity;
    
    private String reservationType;

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPnrStatus() {
		return pnrStatus;
	}

	public void setPnrStatus(String pnrStatus) {
		this.pnrStatus = pnrStatus;
	}

	public List<SegmentDetails> getSegmentDetails() {
		return segmentDetails;
	}

	public void setSegmentDetails(List<SegmentDetails> segmentDetails) {
		this.segmentDetails = segmentDetails;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public boolean isFlown() {
		return flown;
	}

	public void setFlown(boolean flown) {
		this.flown = flown;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public TravellerQuantity getTravellerQuantity() {
		return travellerQuantity;
	}

	public void setTravellerQuantity(TravellerQuantity travellerQuantity) {
		this.travellerQuantity = travellerQuantity;
	}

	public String getReservationType() {
		return reservationType;
	}

	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}

	@Override
	public int compareTo(ReservationDetails reservationDetails) {
		if (this.getBookingDate().before(reservationDetails.getBookingDate())) {
			return 1; // After
		} else if (this.getBookingDate().after(reservationDetails.getBookingDate())) {
			return -1; // Before
		} else {
			return 0; // Same Date
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ReservationDetails other = (ReservationDetails) obj;

		if ((this.getPnr() == null) ? (other.getPnr() != null) : !this.getPnr().equals(other.getPnr())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 53 * hash + (this.pnr != null ? this.pnr.hashCode() : 0);
		return hash;
	}

}
