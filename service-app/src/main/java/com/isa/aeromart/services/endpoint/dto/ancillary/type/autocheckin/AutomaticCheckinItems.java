package com.isa.aeromart.services.endpoint.dto.ancillary.type.autocheckin;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

/**
 * @author aravinth.r
 *
 */
public class AutomaticCheckinItems extends AncillaryItemLeaf {

	private List<AutomaticCheckinItem> items;

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(List<AutomaticCheckinItem> items) {
		this.items = items;
	}

	@Override
	public List<AutomaticCheckinItem> getItems() {
		return items;
	}

	public AutomaticCheckinItems() {
		setType(AncillariesConstants.Type.AUTOMATIC_CHECKIN);
	}
}
