package com.isa.aeromart.services.endpoint.dto.common;

public class Transition<T> {

    private T _old;

    private T _new;

    public T getOld() {
        return _old;
    }

    public void setOld(T n) {
        this._old = n;
    }

    public T getNew() {
        return _new;
    }

    public void setNew(T n) {
        this._new = n;
    }

}
