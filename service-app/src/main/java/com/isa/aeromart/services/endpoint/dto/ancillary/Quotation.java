package com.isa.aeromart.services.endpoint.dto.ancillary;

public class Quotation {
	
	private AncillariesDescriptor reservationAncillaries;

	public AncillariesDescriptor getReservationAncillaries() {
		return reservationAncillaries;
	}

	public void setReservationAncillaries(AncillariesDescriptor reservationAncillaries) {
		this.reservationAncillaries = reservationAncillaries;
	}

}
