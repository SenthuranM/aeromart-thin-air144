package com.isa.aeromart.services.endpoint.utils.masterdata;



import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * App parameters publishing for IBE
 * 
 * @author rajitha
 *
 */
public enum IBEParamKeys {

	TEST_SYSTEM("isTestSystem", new Boolean(AppSysParamsUtil.isTestStstem()).toString()), ACCELAERO_SPY_ENABLED(
			"enableAccelAeroSpy",
			new Boolean(AppSysParamsUtil.isAccelAeroSpyEnabled()).toString()), CHILD_VISIBILITY("childVisibility",
					new Boolean("Y".equals(getGlobalConfig().getBizParam(SystemParamKeys.CHILD_VISIBILITY)))
							.toString()), CARRIER_LOGO_WITH_FLT_INFO(
									"showCarrierLogoWithInfo",
									new Boolean("Y".equals(getGlobalConfig().getBizParam(
											SystemParamKeys.SHOW_CARRIER_LOGO_WITH_FLT_INFO))).toString()), SHOW_SEAT_MAP(
													"showSeatMap",
													new Boolean("Y".equals(getGlobalConfig().getBizParam(
															SystemParamKeys.SHOW_SEAT_MAP))).toString()), SHOW_TRAVEL_INSURANCE(
																	"showTravelInsurence",
																	new Boolean(AppSysParamsUtil.isShowTravelInsurance())
																			.toString()), ENABLE_HOLIDAYS_URL(
																					"enableHolidayUrl",
																					new Boolean(
																							AppSysParamsUtil.isShowHolidaysLink())
																									.toString()), DISPLAY_IBE_LISTS_DYNAMICALLY(
																											"displayIbeListDynamically",
																											new Boolean(
																													AppSysParamsUtil
																															.isDisplayIBEListsDynamicallyEnabled())
																																	.toString()), ENABLE_RENT_A_CAR_URL(
																																			"enableRentACarUrl",
																																			new Boolean(
																																					"Y".equals(
																																							getGlobalConfig()
																																									.getBizParam(
																																											SystemParamKeys.ENABLE_RENT_A_CAR_URL_IN_IBE)))
																																													.toString()), SHOW_MEAL(
																																															"showMealInformation",
																																															new Boolean(
																																																	"Y".equals(
																																																			getGlobalConfig()
																																																					.getBizParam(
																																																							SystemParamKeys.SHOW_MEAL)))
																																																									.toString()), SHOW_BAGGAGE(
																																																											"showBaggageInformation",
																																																											new Boolean(
																																																													"Y".equals(
																																																															getGlobalConfig()
																																																																	.getBizParam(
																																																																			SystemParamKeys.SHOW_BAGGAGE)))
																																																																					.toString()),

	SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY("showFareDefInDepAirCurrency",
			new Boolean(AppSysParamsUtil.isShowFareDefInDepAirPCurr()).toString()), ENABLE_LOYALITY("enableLoyalty",
					new Boolean("Y".equals(getGlobalConfig().getBizParam(SystemParamKeys.IBE_ENABLE_LOYALITY)))
							.toString()), INTEGRATE_MASHREQ_WITH_LMS(
									"intergrateMashreqWithLms",
									new Boolean("Y".equals(getGlobalConfig().getBizParam(
											SystemParamKeys.INTEGRATE_MASHREQ_WITH_LMS))).toString()), MULTIPLE_MEAL_SELECTION(
													"enableMultipleMealSelection",
													new Boolean("Y".equals(getGlobalConfig()
															.getBizParam(SystemParamKeys.MULTIPLE_MEAL_SELECTION)))
																	.toString()), ENABLE_GROUND_SERVICE(
																			"enableGroundService",
																			new Boolean("Y".equals(getGlobalConfig().getBizParam(
																					SystemParamKeys.ENABLE_GROUND_SERVICE)))
																							.toString()), FOC_MEAL_SELECTION_ENABLED(
																									"enableFocMealSelection",
																									new Boolean("Y".equals(
																											getGlobalConfig()
																													.getBizParam(
																															SystemParamKeys.FOC_MEAL_SELECTION_ENABLED)))
																																	.toString()), SHOW_INSURANCE_OPTIONS_IN_UI(
																																			"showInsuranceOptionInUi",
																																			new Boolean(
																																					"Y".equals(
																																							getGlobalConfig()
																																									.getBizParam(
																																											SystemParamKeys.SHOW_INSURANCE_OPTIONS_IN_UI)))
																																													.toString()), ENABLE_ANCI_REMINDER(
																																															"enableAnciReminder",
																																															new Boolean(
																																																	"Y".equals(
																																																			getGlobalConfig()
																																																					.getBizParam(
																																																							SystemParamKeys.ENABLE_ANCI_REMINDER)))
																																																									.toString()), COUNTRY_PAX_CONFIG_ENABLED(
																																																											"enableCountryWisePax",
																																																											new Boolean(
																																																													"Y".equals(
																																																															getGlobalConfig()
																																																																	.getBizParam(
																																																																			SystemParamKeys.COUNTRY_PAX_CONFIG_ENABLED)))
																																																																					.toString()), SHOW_INFANT_ETICKET(
																																																																							"showInfantETicket",
																																																																							new Boolean(
																																																																									AppSysParamsUtil
																																																																											.isShowInfantETicketInPNLADL())
																																																																													.toString()), SHOW_BAGGAGE_ALLOWANCE_REMARKS(
																																																																															"showBaggageAllowenceRemark",
																																																																															new Boolean(
																																																																																	"Y".equals(
																																																																																			getGlobalConfig()
																																																																																					.getBizParam(
																																																																																							SystemParamKeys.SHOW_BAGGAGE_ALLOWANCE_REMARKS)))
																																																																																									.toString()), SHOW_OND_BAGGAGES(
																																																																																											"showOndBaggages",
																																																																																											new Boolean(
																																																																																													"Y".equals(
																																																																																															getGlobalConfig()
																																																																																																	.getBizParam(
																																																																																																			SystemParamKeys.SHOW_OND_BAGGAGES)))
																																																																																																					.toString()), AUTO_SEAT_ASSIGNMENT(
																																																																																																							"showAutoSeatAssignment",
																																																																																																							new Boolean(
																																																																																																									"Y".equals(
																																																																																																											getGlobalConfig()
																																																																																																													.getBizParam(
																																																																																																															SystemParamKeys.SHOW_SEATS)))
																																																																																																																	.toString()),

	ENABLE_FLEXI_FARES("enableFlexiFares",
			new Boolean(AppSysParamsUtil.isFlexiFareEnabled()).toString()), ADD_INSURANCE_FOR_INFANTS(
					"allowAddInsuranceForInfants",
					new Boolean(AppSysParamsUtil.allowAddInsurnaceForInfants()).toString()), SHOW_NESTED_AVAILABLE_SEATS(
							"showNestedSeatsAvailable",
							new Boolean(AppSysParamsUtil.showNestedAvailableSeats())
									.toString()), APPLY_COOKIE_BASED_SURCHARGES_FOR_IBE(
											"applyCookieBasedSurCharges",
											new Boolean(AppSysParamsUtil.isCookieBasedSurchargesApplyForIBE())
													.toString()), ENABLE_EXIT_POPUP(
															"enableExitPopUp",
															new Boolean(AppSysParamsUtil.isEnableExitPopupForIBE())
																	.toString()), ENABLE_USER_DEFINE_TRANSIT_TIME(
																			"enableUserDefinedTransitTime",
																			new Boolean(AppSysParamsUtil
																					.isEnableUserDefineTransitTime())
																							.toString()), SHOW_CHARGES_PAYMENTS_IN_ITINERARY(
																									"showChargesPaymentInItinerary",
																									new Boolean("Y".equals(
																											getGlobalConfig()
																													.getBizParam(
																															SystemParamKeys.SHOW_CHARGES_PAYMENTS_IN_ITINERARY)))
																																	.toString()), ENABLE_COOKIE_SAVE_FUNCTIONALITY(
																																			"enableCookieBaseFunctionality",
																																			new Boolean(
																																					"Y".equals(
																																							getGlobalConfig()
																																									.getBizParam(
																																											SystemParamKeys.ENABLE_COOKIE_SAVE_FUNCTIONALITY)))
																																													.toString()), SHOW_MOBILE_AREA_CODE_PREFIX(
																																															"addMobileCodeAreaPrefix",
																																															new Boolean(
																																																	AppSysParamsUtil
																																																			.addMobileAreaCodePrefix())
																																																					.toString()), ENABLE_LCC_DATA_SYNC(
																																																							"enableLccDataSync",
																																																							new Boolean(
																																																									AppSysParamsUtil
																																																											.isLCCDataSyncEnabled())
																																																													.toString()), TRACKING_ENABLED(
																																																															"enableTracking",
																																																															new Boolean(
																																																																	AppSysParamsUtil
																																																																			.isTrackingEnabled())
																																																																					.toString()), ENABLE_LOYALTY_MANAGEMENT_SYSTEM(
																																																																							"enableLoyaltyManagement",
																																																																							new Boolean(
																																																																									AppSysParamsUtil
																																																																											.isLMSEnabled())
																																																																													.toString()), MAINTAIN_SSR_WISE_CUTOFF_TIME(
																																																																															"maintainSsrWiseCutOffTime",
																																																																															new Boolean(
																																																																																	AppSysParamsUtil
																																																																																			.isMaintainSSRWiseCutoffTime())
																																																																																					.toString()), ENFORCE_SAME_HIGHER(
																																																																																							"enforceSameHigher",
																																																																																							new Boolean(
																																																																																									"Y".equals(
																																																																																											getGlobalConfig()
																																																																																													.getBizParam(
																																																																																															SystemParamKeys.ENFORCE_SAME_HIGHER)))
																																																																																																	.toString()), DUPLICATE_NAME_CHECK(
																																																																																																			"EnableDuplicateNameCheck",
																																																																																																			new Boolean(
																																																																																																					"Y".equals(
																																																																																																							getGlobalConfig()
																																																																																																									.getBizParam(
																																																																																																											SystemParamKeys.DUPLICATE_NAME_CHECK)))
																																																																																																													.toString()), REQUOTE_ENABLED(
																																																																																																															"enableRequote",
																																																																																																															new Boolean(
																																																																																																																	"Y".equals(
																																																																																																																			getGlobalConfig()
																																																																																																																					.getBizParam(
																																																																																																																							SystemParamKeys.REQUOTE_ENABLED)))
																																																																																																																									.toString()), HTTPS_COMPATIBILITY_MAP(
																																																																																																																											"enableHttpsForIbe",
																																																																																																																											new Boolean(
																																																																																																																													AppSysParamsUtil
																																																																																																																															.isHttpsEnabledForIBE())
																																																																																																																																	.toString()), ENABLE_DOMESTIC_FARE_DISCOUNT(
																																																																																																																																			"enableDomesticFareDiscount",
																																																																																																																																			new Boolean(
																																																																																																																																					AppSysParamsUtil
																																																																																																																																							.isDomesticFareDiscountEnabled())
																																																																																																																																									.toString()), ENABLE_PROMO_CODE(
																																																																																																																																											"enablePromoCode",
																																																																																																																																											new Boolean(
																																																																																																																																													"Y".equals(
																																																																																																																																															getGlobalConfig()
																																																																																																																																																	.getBizParam(
																																																																																																																																																			SystemParamKeys.ENABLE_PROMO_CODE)))
																																																																																																																																																					.toString()),

	ENABLE_AUTO_REFUND_FOR_NOSHO_PAX_IN_IBE_BOOKINGS("EnableAutoRefundForNoshoPaxInIbe",
			new Boolean(AppSysParamsUtil.isAllowNOSHOPaxAutoRefundForIBEBookings()).toString()), ENABLE_OPENJAW_MULTICITY_SEARCH(
					"enableOpenJaxMultiCity",
					new Boolean(AppSysParamsUtil.isOpenJawMultiCitySearchEnabled())
							.toString()), REQUEST_NATIONALID_FOR_RESERVATIONS_HAVING_DOMESTIC_SEGMENT(
									"requestNationalIdForResDomesticSegments",
									new Boolean(AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments())
											.toString()), SHOW_ALERTS_IBE_ENABLED(
													"showAlertsInIbe",
													new Boolean(AppSysParamsUtil.isShowAlertsInIBEEnabled())
															.toString()), GOOGLE_ANALYTICS_ENABLED("EnableGoogleAnalytics",
																	new Boolean(AppSysParamsUtil.isGoogleAnalyticsEnabled())
																			.toString()),

	CREDIT_CARD_PAYMENTS_ENABLED("enableCreditCardpayment",
			new Boolean(AppSysParamsUtil.isCreditCardPaymentsEnabled()).toString()), RESERVATION_ONHOLD_ENABLED(
					"enableReservationOnHold",
					new Boolean(AppSysParamsUtil.isReservationOnholdEnabled()).toString()), COS_SELECTION_ENABLED(
							"enableCosSelection",
							new Boolean(AppSysParamsUtil.isCOSSelectionEnabled()).toString()), SHOW_IBE_CHARGES_BREAKDOWN(
									"showIbeChargesBreakdown",
									new Boolean(AppSysParamsUtil.isShowIBEChargesBreakdownEnabled())
											.toString()), MODIFY_RESERVATION_BY_UNREGISTERED_USER(
													"modifyReservationByUnregisteredUser",
													new Boolean(AppSysParamsUtil.isUnregisterdUserCanMofidyReservation())
															.toString()), CANCEL_SEGMENT_BY_UNREGISTERED_USER(
																	"cancelSegmentByUnregisteredUser",
																	new Boolean(
																			AppSysParamsUtil.isUnregisterdUserCanCancelSegment())
																					.toString()), CANCEL_RESERVATION_BY_UNREGISTERED_USER(
																							"cancelReservationByUnregisteredUser",
																							new Boolean(AppSysParamsUtil
																									.isUnregisterdUserCanCancelReservation())
																											.toString()), ADD_SERVICES_BY_UNREGISTERED_USER(
																													"addServiceByUnregisteredUser",
																													new Boolean(
																															AppSysParamsUtil
																																	.isUnregisterdUserCanAddServices())
																																			.toString()), EDIT_SERVICES_BY_UNREGISTERED_USER(
																																					"editServiceByUnregisteredUser",
																																					new Boolean(
																																							AppSysParamsUtil
																																									.isUnregisterdUserCanEditServices())
																																											.toString()), CAPTCHA_ENABLED_FOR_UNREGISTERED_USER(
																																													"enableCaptchaForUnregisteredUsers",
																																													new Boolean(
																																															AppSysParamsUtil
																																																	.isCaptchaEnabledForUnregisterdUser())
																																																			.toString()), ADD_SERVICES_BY_REGISTERED_USER(
																																																					"addServiceByRegisteredUser",
																																																					new Boolean(
																																																							AppSysParamsUtil
																																																									.isRegisterdUserCanAddServices())
																																																											.toString()), EDIT_SERVICES_BY_REGISTERED_USER(
																																																													"editServiceByRegisteredUser",
																																																													new Boolean(
																																																															AppSysParamsUtil
																																																																	.isRegisterdUserCanEditServices())
																																																																			.toString()), IBE_ADD_SEAT(
																																																																					"addSeatIbe",
																																																																					new Boolean(
																																																																							AppSysParamsUtil
																																																																									.isIBEAddSeat())
																																																																											.toString()), IBE_EDIT_SEAT(
																																																																													"editSeatIbe",
																																																																													new Boolean(
																																																																															AppSysParamsUtil
																																																																																	.isIBEEditSeat())
																																																																																			.toString()), IBE_ADD_INSURANCE(
																																																																																					"addInsuranceIbe",
																																																																																					new Boolean(
																																																																																							AppSysParamsUtil
																																																																																									.isIBEAddInsurance())
																																																																																											.toString()),

	IBE_EDIT_INSURANCE("editInsuranceIbe", new Boolean(AppSysParamsUtil.isIBEEditInsurance()).toString()), IBE_ADD_MEAL(
			"addMealIbe", new Boolean(AppSysParamsUtil.isIBEAddMeal()).toString()), IBE_EDIT_MEAL("editMealIbe",
					new Boolean(AppSysParamsUtil.isIBEEditMeal()).toString()), IBE_ADD_BAGGAGE("addBagageIbe",
							new Boolean(AppSysParamsUtil.isIBEAddBaggage()).toString()), IBE_EDIT_BAGGAGE("editBaggageIbe",
									new Boolean(AppSysParamsUtil.isIBEEditBaggage()).toString()), IBE_ADD_HALA("addHalaIbe",
											new Boolean(AppSysParamsUtil.isIBEAddAirportServices()).toString()), IBE_EDIT_HALA(
													"editHalaIbe",
													new Boolean(AppSysParamsUtil.isIBEEditAirportServices())
															.toString()), IBE_SHOW_LCC_RESULTS(
																	"showLccResultsInIbe",
																	new Boolean(AppSysParamsUtil.isLCCDataSyncEnabled())
																			.toString()), IBE_ALLOW_RETURN(
																					"enableReturnIbe",
																					new Boolean(
																							AppSysParamsUtil.isIBEReturnEnabled())
																									.toString()), IBE_CALENDAR_GRADIENT_VIEW(
																											"calenderGradientViewEnable",
																											new Boolean(
																													AppSysParamsUtil
																															.isCalendarGradiantView())
																																	.toString()), IBE_MODIFY_SEGMENT_BALANCE_DISPLAY_AS(
																																			"modifySegmentBalanceDisplay",
																																			new Boolean(
																																					AppSysParamsUtil
																																							.isModifySegmentBalanceDisplayAvailabilitySearch())
																																									.toString()), IBE_SESSION_TIMEOUT_DISPLAY(
																																											"displaySessionOutTime",
																																											new Boolean(
																																													AppSysParamsUtil
																																															.getSessionTimeOutDisplay())
																																																	.toString()),

	IBE_IMAGE_CAPCHA_AS("ibeCaptchaImage", new Boolean(AppSysParamsUtil.getImageCapchaDisplayAvailabilitySearch())
			.toString()), ENABLE_REGISTERED_USER_ACCESS_IN_KIOSK(
					"enableRegisteredUserAccess",
					new Boolean("Y".equals(getGlobalConfig().getBizParam(SystemParamKeys.ENABLE_REGISTERED_USER_ACCESS_IN_KIOSK)))
							.toString()), IBE_BLOCK_SEAT_ENABLE(
									"enableBlockSeatIbe",
									new Boolean(AppSysParamsUtil.isIBEBlockSeatEnabled())
											.toString()), IBE_SHOW_REG_USER_LOGIN_IN_PAX_PAGE(
													"showRegisteredUserInPaxDetailsPage",
													new Boolean(AppSysParamsUtil.isShowRegUserLoginInPaxDetailPage())
															.toString()), IBE_SHOW_CUSTOMIZED_ITINERARY(
																	"showCustomizedItinerary",
																	new Boolean(AppSysParamsUtil.getShowCustomizedItinerary())
																			.toString()), IBE_MODIFY_SEGMENT_BY_DATE(
																					"allowModifySegmentByDate",
																					new Boolean(AppSysParamsUtil
																							.isAllowModifySegmentDateIBE())
																									.toString()), IBE_MODIFY_SEGMENT_BY_ROUTE(
																											"allowModifySegmentByroute",
																											new Boolean(
																													AppSysParamsUtil
																															.isAllowModifySegmentRouteIBE())
																																	.toString()), IBE_CUSTOMER_REGISTER_CONFIRMATION(
																																			"customerRegisterConfirmation",
																																			new Boolean(
																																					AppSysParamsUtil
																																							.isCustomerRegiterConfirmation())
																																									.toString()), IBE_ADD_BUS(
																																											"allowAddBusIbe",
																																											new Boolean(
																																													AppSysParamsUtil
																																															.isIBEAddBusSegment())
																																																	.toString()), HOTEL_LINK_LOAD_WITHIN_FRAME(
																																																			"loadHotelLinkWithFrame",
																																																			new Boolean(
																																																					AppSysParamsUtil
																																																							.isHotelLinkLoadWithinFrame())
																																																									.toString()), IBE_ENABLE_PAX_CREDIT_AUTO_REFUND_FOR_CC(
																																																											"enablePaxCreditAutoRefund",
																																																											new Boolean(
																																																													AppSysParamsUtil
																																																															.isIBEPaxCreditAutoRefundEnabled())
																																																																	.toString()), IBE_ALLOW_NO_SHOW_SEGMENTS_TO_MODIFY(
																																																																			"allowNoShowSegment",
																																																																			new Boolean(
																																																																					AppSysParamsUtil
																																																																							.isAllowModifyNoShowSegmentsIBE())
																																																																									.toString()), DISPLAY_DIRECT_FLIGHTS_FIRST(
																																																																											"displayDirectFlights",
																																																																											new Boolean(
																																																																													AppSysParamsUtil
																																																																															.displayDirectFlightsFirst())
																																																																																	.toString()), ENABLE_IBE_FAKE_DISCOUNTS(
																																																																																			"enableIbeFakeDiscounts",
																																																																																			getGlobalConfig()
																																																																																					.getBizParam(
																																																																																							SystemParamKeys.ENABLE_IBE_FAKE_DISCOUNTS)), IBE_OFFLINE_PAYMENT_ENABLE(
																																																																																									"enableOfflinePayment",
																																																																																									getGlobalConfig()
																																																																																											.getBizParam(
																																																																																													SystemParamKeys.IBE_OFFLINE_PAYMENT_ENABLE)), IBE_AIRPORT_TRANSFER(
																																																																																															"enableAirportTransfer",
																																																																																															new Boolean(
																																																																																																	"Y".equals(
																																																																																																			getGlobalConfig()
																																																																																																					.getBizParam(
																																																																																																							SystemParamKeys.IBE_AIRPORT_TRANSFER)))
																																																																																																									.toString()), IBE_SHOW_SOCIAL_LOGIN_IN_PAX_PAGE(
																																																																																																											"showSocialLoginInPaxPage",
																																																																																																											new Boolean(
																																																																																																													AppSysParamsUtil
																																																																																																															.isShowIbeSocialLoginInPaxPage())
																																																																																																																	.toString()), IBE_CUSTOMER_AUTO_LOGIN_AFTER_REGISTRATION(
																																																																																																																			"autoLoginCustomerAfterRegistration",
																																																																																																																			new Boolean(
																																																																																																																					AppSysParamsUtil
																																																																																																																							.isAutoLoginAfterRegistration())
																																																																																																																									.toString()), IBE_CALENDAR_ENABLED_FOR_REQUOTE(
																																																																																																																											"enableCalendarViewForRequote",
																																																																																																																											new Boolean(
																																																																																																																													AppSysParamsUtil
																																																																																																																															.isCalendarViewEnabledForIBERequote())
																																																																																																																																	.toString()), APPLY_FQTV_FOR_ALL_SEGMENTS(
																																																																																																																																			"applyFqtvForAllSegments",
																																																																																																																																			new Boolean(
																																																																																																																																					AppSysParamsUtil
																																																																																																																																							.applyFQTVInAllSegments())
																																																																																																																																									.toString()),

	REFUND_IBE_ANCI_MODIFY_CREDIT_AMOUNT("refundIbeAnciModifyCredit", new Boolean(AppSysParamsUtil.isRefundIBEAnciModifyCredit())
			.toString()), DISPLAY_NOTIFICATION_FOR_LAST_NAME_IN_CONTACT_DETAILS_IN_IBE(
					"displayNotificationForContactDetailsInIbe",
					new Boolean(AppSysParamsUtil.isDisplayNotificationForLastNameInContactDetailsInIBE())
							.toString()), IBE_SSR_ENABLE("enableIbeSsr",
									getGlobalConfig().getBizParam(SystemParamKeys.IBE_SSR_ENABLE)), IBE_ADD_SSR("addIbeSsr",
											new Boolean(AppSysParamsUtil.isIBEAddSSR()).toString()), IBE_EDIT_SSR("editIbeSsr",
													new Boolean(AppSysParamsUtil.isIBEEditSSR())
															.toString()), ENABLE_STOPOVER_FOR_IBE(
																	"enableStopOverForIbe",
																	new Boolean(AppSysParamsUtil.isEnableStopOverInIBE())
																			.toString()), ALLOW_SAME_FLIGHT_MODIFICATION_IN_IBE(
																					"allowSameFlightModification",
																					new Boolean(AppSysParamsUtil
																							.isAllowSameFlightModificationInIBE())
																									.toString()), LCC_CONNCECTIVITY_ENABLED(
																											"enableLccConectivity",
																											new Boolean(
																													AppSysParamsUtil
																															.isLCCConnectivityEnabled())
																																	.toString()),

	MINIMUM_RETURN_TRANSIT_TIME_IN_MILLIS("minimum transit time in millis",
			new Long(AppSysParamsUtil.getMinimumReturnTransitTimeInMillis()).toString()), TRANSIT_TIME_THRESHOLD_IN_MILLIS(
					"transit time threshold in millis",
					new Long(AppSysParamsUtil.getTransitTimeThresholdInMillis()).toString()), BASE_CURRENCY("baseCurrency",
							AppSysParamsUtil.getBaseCurrency()), ONHOLD_IMAGE_CAPTCHA("onHldImageCaptcha",
									new Boolean(AppSysParamsUtil.isOHDImageCapcha()).toString()), DEFAULT_CARRIER_CODE(
											"defaultCarrierCode",
											AppSysParamsUtil.getDefaultCarrierCode()), DEFAULT_PAYMENT_GATEWAY_CURRENCY(
													"defaultPGCurrency",
													AppSysParamsUtil.getDefaultPGCurrency()), SECURE_SERVICE_APP_URL(
															"secureServiceAppURL",
															AppSysParamsUtil.getSecureServiceAppIBEUrl()), AIRLINE_URL(
																	"defaultAirlineURL",
																	AppSysParamsUtil
																			.getDefaultAirlineUrl()), GOOGLE_TAG_MANAGER_KEY(
																					"googleTagManagerKey",
																					AppSysParamsUtil
																							.getGoogleTagManagerKey()), SHOW_IBE_CHARGES_BREAKDOWN_IN_PAYMENT_PAGE(
																									"showReservationChargesDetails",
																									new Boolean(AppSysParamsUtil
																											.isShowIBEV3ChargesBreakdownInPaymentPage())
																													.toString()), SHOW_PAYMENT_ICONS_IN_AVAILABILITY_PAGE(
																															"showPaymentIcons",
																															new Boolean(
																																	AppSysParamsUtil
																																			.showPaymentMethodsOnIBE())
																																					.toString()), CHECK_BOX_TO_ACCEPT_TERMS_IN_AVAILABILITY_PAGE(
																																							"putCheckboxToAcceptTerms",
																																							new Boolean(
																																									AppSysParamsUtil
																																											.allowAvailabilityPageToHaveCheckBoxToAcceptTermsAndConditions())
																																													.toString()), ENABLE_ANCI_SUMMARY_AT_PAYMENT_PAGE(
																																															"enableAnciSummaryAtPayment",
																																															new Boolean(
																																																	AppSysParamsUtil
																																																			.isAnciSummaryAtPaymentEnabled())
																																																					.toString()), GOOGLE_ANALYTICS_KEY(
																																																							"googleAnalyticsKey",
																																																							AppSysParamsUtil
																																																									.getGoogleAnalyticsId()), GOOGLE_ANALYTICS_KEY_FOR_SERVICEAPP(
																																																											"googleAnalyticsKeyForServiceApp",
																																																											AppSysParamsUtil
																																																													.getGoogleAnalyticsIdForServiceApp()), ENABLE_BUNDLE_FARE_POPUP(
																																																															"isBundleFarePopupEnabled",
																																																															new Boolean(
																																																																	AppSysParamsUtil
																																																																			.isBundleFarePopupEnabled(
																																																																					ApplicationEngine.IBE))
																																																																							.toString()), TAX_REGISTRATION_NUMBER_VISIBLE_COUNTRIES(
																																																																									"taxRegNoEnabledCountries",
																																																																									AppSysParamsUtil
																																																																											.getTaxRegistrationNumberEnabledCountries()),

	ALLOW_DECIMAL_PLACES_IN_IBE("showDecimalPlacesForCurrencyValues",
			new Boolean(AppSysParamsUtil.showDecimalPlacesForCurrencyValuesInIBE())
					.toString()), SHOW_DESCRIPTION_FOR_FARE_BUNDLES(
							"showFareBundleDescription",
							new Boolean(AppSysParamsUtil.showDescriptionForFareBundlesInIBE())
									.toString()), ENABLE_CHAMWINGS_OHD_POPUP(
											"enableChamWingsOhdPopup",
											new Boolean("Y".equals(
													getGlobalConfig().getBizParam(SystemParamKeys.ENABLE_CHAMWINGS_OHD_POPUP)))
															.toString()),

	DISABLE_REFRESHING_IB_CALENDAR_FOR_RETURN_SEARCH("disableIBCalendarRefresh",
			Boolean.toString(AppSysParamsUtil.disableRefreshingIBCalendarInIBE())), SHOW_HALF_RETURN_FARES_IN_CALENDAR(
					"showHalfReturnFaresInCalendar",
					Boolean.toString(AppSysParamsUtil.showHalfReturnFaresInIBECalendar())), TAX_REGISTRATION_NUMBER_LABEL(
							"taxRegistrationNumberLabel", AppSysParamsUtil.getTaxRegistrationNumberLabelForIBE()),

	GET_ONLINE_CHECK_IN_URL("getOnlineCheckInURLText",
			AppSysParamsUtil.getOnlineCheckInURLText()), GET_TIME_DIFFERENCE_TO_DISPLAY_ONLINE_CHECKIN(
					"getTimeDifferenceToDisplayOnlineCheckin", AppSysParamsUtil.getTimeDifferenceToDisplayOnlineCheckin()),
	BUNDLE_FARE_DESCRIPTION_TEMPLATE_VERSION("bundleFareDescriptionTemplateVersion",
			AppSysParamsUtil.getBundleFareDescriptionTemplateVersion()), VOUCHER_ENABLED("voucherEnabled",
					new Boolean(AppSysParamsUtil.isVoucherEnabled()).toString());

	private final String value;
	private final String key;

	IBEParamKeys(String key, String value) {
		this.value = value;
		this.key = key;

	}

	public String getValue() {
		return this.value;
	}

	public String getKey() {
		return this.key;
	}

	static GlobalConfig getGlobalConfig() {

		return CommonsServices.getGlobalConfig();
	}

}
