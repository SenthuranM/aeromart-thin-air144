package com.isa.aeromart.services.endpoint.dto.ancillary;

public class AncillariesDescriptor {
	
	private enum typeEnum{
		RESERVATION, SELECTION
	}
	
	private typeEnum type;

	public typeEnum getType() {
		return type;
	}

	public void setType(typeEnum type) {
		this.type = type;
	}
	
}
