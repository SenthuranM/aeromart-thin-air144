package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.PromoType;
import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.aeromart.services.endpoint.dto.availability.Total;
import com.isa.aeromart.services.endpoint.dto.common.PromoInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;

public class TotalAdaptor implements Adaptor<PriceInfoTO, Total> {
	private PriceInfoTO selectedSegFlightPriceInfo;
	private List<PromoInfo> promoInfos;
	private PromoInfo returnDiscountPromo = new PromoInfo();
	private List<OndOWPricing> ondOWPricing;
	private BigDecimal deductableDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public TotalAdaptor(PriceInfoTO selectedSegFlightPriceInfo, List<PromoInfo> promoInfos, List<OndOWPricing> ondOWPricing) {
		this.selectedSegFlightPriceInfo = selectedSegFlightPriceInfo;
		this.promoInfos = promoInfos;
		this.ondOWPricing = ondOWPricing;
	}
	@Override
	public Total adapt(PriceInfoTO source) {
		Total target = new Total();
		List<PaxPrice> paxFareTotal = new ArrayList<PaxPrice>();
		target.setPrice(source.getFareTypeTO().getTotalPrice());
		PaxPrice adultTotal = new PaxPrice(PaxTypeTO.ADULT);
		PaxPrice childTotal = new PaxPrice(PaxTypeTO.CHILD);
		PaxPrice infantTotal = new PaxPrice(PaxTypeTO.INFANT);
		int adultCount = 0;
		int childCount = 0;
		int infantCount = 0;
		for (OndOWPricing ondWise : ondOWPricing) {
			for (PaxPrice paxPrice : ondWise.getPaxWise()) {
				if (paxPrice.getPaxType().equals(PaxTypeTO.ADULT)) {
					createTotalPaxWise(adultTotal, paxPrice);
					adultCount = paxPrice.getNoOfPax();
				} else if (paxPrice.getPaxType().equals(PaxTypeTO.CHILD)) {
					createTotalPaxWise(childTotal, paxPrice);
					childCount = paxPrice.getNoOfPax();
				} else if (paxPrice.getPaxType().equals(PaxTypeTO.INFANT)) {
					createTotalPaxWise(infantTotal, paxPrice);
					infantCount = paxPrice.getNoOfPax();
				}
			}
			target.setTax(AccelAeroCalculator.add(target.getTax(), ondWise.getTotalTaxes()));
			target.setSurcharge(AccelAeroCalculator.add(target.getSurcharge(), ondWise.getTotalSurcharges()));
		}
		target.setTax(AccelAeroCalculator.add(target.getTax(), source.getFareTypeTO().getServiceTaxAmount()));

		if (adultCount > 0) {
			paxFareTotal.add(updatePerPaxFare(adultCount, ondOWPricing, adultTotal));
		}
		if (childCount > 0) {
			paxFareTotal.add(updatePerPaxFare(childCount, ondOWPricing, childTotal));
		}
		if (infantCount > 0) {
			paxFareTotal.add(updatePerPaxFare(infantCount, ondOWPricing, infantTotal));
		}
		target.setPaxWise(paxFareTotal);
		target.setFare(AccelAeroCalculator.add(target.getFare(), adultTotal.getTotalFare(), childTotal.getTotalFare(),
				infantTotal.getTotalFare()));
		// Add external charges to total
		for (int exChg = 0; exChg < source.getFareTypeTO().getOndExternalCharges().size(); exChg++) {
			List<ONDExternalChargeTO> ondExternalCharges = source.getFareTypeTO().getOndExternalCharges();
			int ondSeq = ondExternalCharges.get(exChg).getOndSequence();
			boolean isOndFlexiSel = false;
			if (source.getFareTypeTO().getOndFlexiSelection() != null
					&& !source.getFareTypeTO().getOndFlexiSelection().isEmpty()) {
				isOndFlexiSel = source.getFareTypeTO().getOndFlexiSelection().get(ondSeq);
			} else {
				isOndFlexiSel = source.getFareTypeTO().getOndExternalCharges().get(ondSeq).isFlexiSelected();
			}
			if (isOndFlexiSel) {
				target.setPrice(AccelAeroCalculator.add(target.getPrice(), ondExternalCharges.get(exChg)
					.getTotalOndExternalCharges()));
			}
		}

		// return discount
		if (selectedSegFlightPriceInfo != null) {
			// Return discount is calculated considering the service taxes
			// Total price does not include service tax amount
			BigDecimal selectedSegmentTotalPrice = AccelAeroCalculator.add(selectedSegFlightPriceInfo.getFareTypeTO().getTotalPrice(),
					selectedSegFlightPriceInfo.getFareTypeTO().getServiceTaxAmount());
			BigDecimal selectedTotalPrice = AccelAeroCalculator.add(source.getFareTypeTO().getTotalPrice(),
					source.getFareTypeTO().getServiceTaxAmount());
			BigDecimal returnDiscount = AccelAeroCalculator.add(selectedSegmentTotalPrice, selectedTotalPrice.negate());
			if (returnDiscount.compareTo(BigDecimal.ZERO) > 0) {
				returnDiscountPromo.setDiscountAmount(returnDiscount);
				returnDiscountPromo.setType(PromoType.RETURN_DISCOUNT.toString());
				returnDiscountPromo.setDiscountAs(DiscountApplyAsTypes.MONEY);
				promoInfos.add(returnDiscountPromo);
			}
		}

		// add all money promotion discounts to total discount
		if (promoInfos.size() > 0) {
			for (PromoInfo promo : promoInfos) {
				if (promo.getDiscountAs() != null && DiscountApplyAsTypes.MONEY.equals(promo.getDiscountAs())) {
					if (!PromoType.RETURN_DISCOUNT.toString().equals(promo.getType())) {
						deductableDiscount = AccelAeroCalculator.add(deductableDiscount, promo.getDiscountAmount());
					}
					target.getDiscount().setAmount(
							AccelAeroCalculator.add(target.getDiscount().getAmount(), promo.getDiscountAmount()));
				}
			}
		}

		// Add service tax amount
		// Service tax amount is not added to the total price
		// Hence to get the total price we need to add service taxes to the effective total price
		target.setPrice(AccelAeroCalculator.add(target.getPrice(), source.getFareTypeTO().getServiceTaxAmount()));

		// reduce promotions from total
		target.setPrice(AccelAeroCalculator.add(target.getPrice(), deductableDiscount.negate()));
		return target;
	}

	public void createTotalPaxWise(PaxPrice totalPaxWise, PaxPrice ondPaxWise) {
		totalPaxWise.setTotal(AccelAeroCalculator.add(totalPaxWise.getTotal(), ondPaxWise.getTotalFare(), ondPaxWise.getTax(),
				ondPaxWise.getSurcharge()));
		totalPaxWise.setTotalFare(AccelAeroCalculator.add(totalPaxWise.getTotalFare(), ondPaxWise.getTotalFare()));
		totalPaxWise.setNoOfPax(ondPaxWise.getNoOfPax());
		totalPaxWise.setFare(ondPaxWise.getFare());
		totalPaxWise.setSurcharge(AccelAeroCalculator.add(totalPaxWise.getSurcharge(), ondPaxWise.getSurcharge()));
		totalPaxWise.setTax(AccelAeroCalculator.add(totalPaxWise.getTax(), ondPaxWise.getTax()));
	}

	private PaxPrice updatePerPaxFare(int paxCount, List<OndOWPricing> ondOWPricing, PaxPrice paxPrice) {
		if ((paxCount > 0 && !ondOWPricing.isEmpty())) {
			if (paxPrice.getTotalFare() != null && paxPrice.getTotalFare().intValue() > 0) {
				paxPrice.setFare(AccelAeroCalculator.divide(paxPrice.getTotalFare(), paxCount));
			}
		}
		return paxPrice;
	}

}
