package com.isa.aeromart.services.endpoint.dto.common;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;

public class SeatTypeDetailsRS extends TransactionalBaseRS {

	private List<SeatType> seatPreferencesList;

	public List<SeatType> getSeatPreferencesList() {
		return seatPreferencesList;
	}

	public void setSeatPreferencesList(List<SeatType> seatPreferencesList) {
		this.seatPreferencesList = seatPreferencesList;
	}

}