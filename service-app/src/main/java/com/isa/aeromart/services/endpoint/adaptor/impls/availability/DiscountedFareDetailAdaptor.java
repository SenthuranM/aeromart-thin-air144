package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;

public class DiscountedFareDetailAdaptor implements Adaptor<ApplicablePromotionDetailsTO, DiscountedFareDetails> {

	@Override
	public DiscountedFareDetails adapt(ApplicablePromotionDetailsTO source) {
		DiscountedFareDetails target = new DiscountedFareDetails();
		target.setPromotionId(source.getPromoCriteriaId());
		target.setPromotionType(source.getPromoType());
		target.setPromoCode(source.getPromoCode());
		target.setDescription(source.getDescription());
		target.setSystemGenerated(source.isSystemGenerated());
		target.setNotes(source.getPromoName());
		target.setFarePercentage(source.getDiscountValue());
		target.setDiscountType(source.getDiscountType());
		target.setDiscountApplyTo(source.getApplyTo());
		target.setDiscountAs(source.getDiscountAs());
		target.addApplicablePaxCount(PaxTypeTO.ADULT, source.getApplicableAdultCount());
		target.addApplicablePaxCount(PaxTypeTO.CHILD, source.getApplicableChildCount());
		target.addApplicablePaxCount(PaxTypeTO.INFANT, source.getApplicableInfantCount());
		target.setApplicableAncillaries(source.getApplicableAncillaries());
		target.setApplicableBINs(source.getApplicableBINs());
		target.setApplicableOnds(source.getApplicableOnds());
		target.setAllowSamePaxOnly(source.isAllowSamePaxOnly());
		target.setOriginPnr(source.getOriginPnr());
		target.setApplicableForOneway(source.isApplicableForOneWay());
		target.setApplicableForReturn(source.isApplicableForReturn());
		target.setApplicability(source.getDiscountApplicability());
		return target;
	}
}
