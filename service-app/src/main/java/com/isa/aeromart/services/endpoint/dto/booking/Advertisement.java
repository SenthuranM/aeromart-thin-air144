package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.List;

public class Advertisement {

	public enum Category {
		CAR, HOTEL
	}

	private Category category;

	private List<AdvertisementTemplate> advertisements;

	public List<AdvertisementTemplate> getAdvertisements() {
		return advertisements;
	}

	public void setAdvertisements(List<AdvertisementTemplate> advertisements) {
		this.advertisements = advertisements;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
