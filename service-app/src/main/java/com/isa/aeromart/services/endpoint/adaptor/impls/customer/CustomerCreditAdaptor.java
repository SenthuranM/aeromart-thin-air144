package com.isa.aeromart.services.endpoint.adaptor.impls.customer;


import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerCredit;
import com.isa.thinair.ibe.api.dto.CustomerCreditDTO;

public class CustomerCreditAdaptor implements Adaptor<CustomerCreditDTO , CustomerCredit >{

	@Override
	public CustomerCredit adapt(CustomerCreditDTO customerCreditDTO) {
		

		CustomerCredit customerCredit = new CustomerCredit();
		
		customerCredit.setPnr(customerCreditDTO.getPnr());
		customerCredit.setFullName(customerCreditDTO.getFullName());
		customerCredit.setBalance(customerCreditDTO.getBalance());
		customerCredit.setDateOfExpiry(customerCreditDTO.getDateOfExpiry());
		customerCredit.setGroupPnr(customerCreditDTO.getGroupPnr());
		
		return customerCredit;
	}

}
