package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;

public class AncillarySelection {

	private SessionMetaData metaData;

	private List<SelectedAncillary> blockedAncillaries;

	private List<SelectedAncillary> ancillaryPreferences;

	private AncillaryIntegrateTO integrateTO;

	private void cleanUpIntegrateTO() {
		integrateTO = null;
	}

	public SessionMetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(SessionMetaData metaData) {
		cleanUpIntegrateTO();
		this.metaData = metaData;
	}

	public List<SelectedAncillary> getBlockedAncillaries() {
		return blockedAncillaries;
	}

	public void setBlockedAncillaries(List<SelectedAncillary> blockedAncillaries) {
		this.blockedAncillaries = blockedAncillaries;
	}

	public List<SelectedAncillary> getAncillaryPreferences() {
		return ancillaryPreferences;
	}

	public void setAncillaryPreferences(List<SelectedAncillary> ancillaryPreferences) {
		cleanUpIntegrateTO();
		this.ancillaryPreferences = ancillaryPreferences;
	}

	public AncillaryIntegrateTO getIntegrateTO() {
		return integrateTO;
	}

	public void setIntegrateTO(AncillaryIntegrateTO integrateTO) {
		this.integrateTO = integrateTO;
	}

}
