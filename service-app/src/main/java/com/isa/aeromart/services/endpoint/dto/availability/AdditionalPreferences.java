package com.isa.aeromart.services.endpoint.dto.availability;

public class AdditionalPreferences {

	public String fareClassType;

	public int fareClassId;

	public String getFareClassType() {
		return fareClassType;
	}

	public void setFareClassType(String fareClassType) {
		this.fareClassType = fareClassType;
	}

	public int getFareClassId() {
		return fareClassId;
	}

	public void setFareClassId(int fareClassId) {
		this.fareClassId = fareClassId;
	}

}
