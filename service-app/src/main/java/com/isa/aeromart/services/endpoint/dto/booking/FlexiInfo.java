package com.isa.aeromart.services.endpoint.dto.booking;

public class FlexiInfo {

	private String flexiDetails;

	private String segmentCode;

	public String getFlexiDetails() {
		return flexiDetails;
	}

	public void setFlexiDetails(String flexiDetails) {
		this.flexiDetails = flexiDetails;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
}
