package com.isa.aeromart.services.endpoint.delegate.modification;

import com.isa.aeromart.services.endpoint.dto.modification.FFIDUpdateRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface ModificationService {

	public void modifyFFIDInfo(FFIDUpdateRQ ffidUpdateRQ, LCCClientReservation reservation, TrackInfoDTO trackinfo)
			throws ModuleException;

}
