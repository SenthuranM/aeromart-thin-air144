package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ConfirmCustomerRQ extends TransactionalBaseRQ {

	private String vlidationText;

	private String emailId;

	public String getVlidationText() {
		return vlidationText;
	}

	public void setVlidationText(String vlidationText) {
		this.vlidationText = vlidationText;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
}
