package com.isa.aeromart.services.endpoint.dto.booking;

public class PromoTypeInfo {

	private boolean creditPromo;

	private boolean moneyPromo;

	public boolean getCreditPromo() {
		return creditPromo;
	}

	public void setCreditPromo(boolean creditPromo) {
		this.creditPromo = creditPromo;
	}

	public boolean getMoneyPromo() {
		return moneyPromo;
	}

	public void setMoneyPromo(boolean moneyPromo) {
		this.moneyPromo = moneyPromo;
	}

}
