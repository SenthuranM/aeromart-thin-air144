package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;


public class LMSRegisterRS extends TransactionalBaseRS{

	private boolean accountExists;

    private boolean lmsNameMismatch;

	public boolean isAccountExists() {
		return accountExists;
	}

	public void setAccountExists(boolean accountExists) {
		this.accountExists = accountExists;
	}

	public boolean isLmsNameMismatch() {
		return lmsNameMismatch;
	}

	public void setLmsNameMismatch(boolean lmsNameMismatch) {
		this.lmsNameMismatch = lmsNameMismatch;
	}

}
