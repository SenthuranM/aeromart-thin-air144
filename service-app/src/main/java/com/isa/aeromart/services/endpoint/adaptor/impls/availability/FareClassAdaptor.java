package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.dto.availability.FareClass;
import com.isa.aeromart.services.endpoint.utils.common.RPHBuilder;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FareClassAdaptor implements Adaptor<LogicalCabinClassInfoTO, FareClass> {

	@Override
	public FareClass adapt(LogicalCabinClassInfoTO source) {

		FareClass target = new FareClass();
		target.setImageUrl(source.getImageUrl());
		target.setComment(source.getComment());
		target.setRank(source.getRank());
		target.setDescription(source.getLogicalCCDesc());
		target.setTemplateDTO(source.getBundleFareDescriptionTemplateDTO());
		if (source.getBundledFarePeriodId() != null) {
			target.setFareClassId(source.getBundledFarePeriodId());
			target.setFareClassType(FareClassType.BUNDLE);
			target.setAdditionalFareFee(source.getBundledFareFee());
		} else if (source.isFlexiAvailable() && AppSysParamsUtil.isDisplayFlexiAsSeparteBundle()) {
			target.setFareClassId(source.getFlexiRuleID());
			target.setFareClassType(FareClassType.FLEXI);
		} else {
			target.setFareClassType(FareClassType.DEFAULT);
		}
		target.setLogicalCabinClass(source.getLogicalCCCode());
		target.setFreeServices(source.getBundledFareFreeServiceName());
		// set fare class code
		target.setFareClassCode(RPHBuilder.FareClassCodeBuilder(source));

		return target;
	}

}
