package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ExternalAgentValidateTokenRQ extends TransactionalBaseRQ {
	
	private String token;
	
	private String hashvalue;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getHashvalue() {
		return hashvalue;
	}

	public void setHashvalue(String hashKey) {
		this.hashvalue = hashKey;
	}
	
}
