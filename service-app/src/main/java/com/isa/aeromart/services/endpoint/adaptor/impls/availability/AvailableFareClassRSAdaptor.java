package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.AdditionalFareClassInfo;
import com.isa.aeromart.services.endpoint.dto.availability.AvailableFareClass;
import com.isa.aeromart.services.endpoint.utils.common.RPHBuilder;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AvailableFareClassRSAdaptor implements Adaptor<FlightFareSummaryTO, AvailableFareClass> {

	private ReturnFareDiscountBuilder returnFareDiscountBuilder;

	public AvailableFareClassRSAdaptor(ReturnFareDiscountBuilder returnFareDiscountBuilder) {
		this.returnFareDiscountBuilder = returnFareDiscountBuilder;
	}

	@Override
	public AvailableFareClass adapt(FlightFareSummaryTO source) {
		boolean isFlexiFare = false;
		AvailableFareClass target = new AvailableFareClass();
		target.setAvailableSeats(source.getNoOfAvailableSeats());
		// create fare class code
		target.setFareClassCode(RPHBuilder.FareClassCodeBuilder(source));
		target.setSelected(source.isSelected());
		if (AppSysParamsUtil.showTotalFareInCalendar()) {
			target.setPrice(source.getTotalPrice());
		} else {
			target.setPrice(source.getBaseFareAmount());
		}
		target.getVisibleChannels().add(source.getVisibleChannelName());
		target.setDescription(source.getLogicalCCDesc());
		if (source.getBundledFarePeriodId() == null && source.isWithFlexi()) {
			isFlexiFare = true;
		}
		if (returnFareDiscountBuilder != null) {
			if (returnFareDiscountBuilder.isDiscountable()) {
				BigDecimal discount = returnFareDiscountBuilder.getDiscount(source, isFlexiFare);
				if (discount != null) {
					AdditionalFareClassInfo additionalFareClassInfo = new AdditionalFareClassInfo();
					additionalFareClassInfo.setDiscountAvailable(true);
					additionalFareClassInfo.setDiscountAmount(discount);
					target.setAdditionalFareClassInfo(additionalFareClassInfo);
				}
			} else {
				if (source.getBundledFarePeriodId() == null && !source.isWithFlexi()) {
					returnFareDiscountBuilder.setTotal(source.getTotalPrice());
				}
			}
		}
		return target;
	}
}