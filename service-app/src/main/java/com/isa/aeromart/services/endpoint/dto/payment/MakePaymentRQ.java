package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts.BookingType;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class MakePaymentRQ extends TransactionalBaseRQ {

	private PaymentOptions paymentOptions;
	private BookingType bookingType;
	private String pnr; // required except for creational
	private ReservationContact contactInfo;
	private boolean binPromotion;
	private boolean mobile;

	public PaymentOptions getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(PaymentOptions paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

	public BookingType getBookingType() {
		return bookingType;
	}

	public void setBookingType(BookingType bookingType) {
		this.bookingType = bookingType;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public ReservationContact getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ReservationContact contactInfo) {
		this.contactInfo = contactInfo;
	}

	public boolean isBinPromotion() {
		return binPromotion;
	}

	public void setBinPromotion(boolean binPromotion) {
		this.binPromotion = binPromotion;
	}

	public boolean isMobile() {
		return mobile;
	}

	public void setMobile(boolean mobile) {
		this.mobile = mobile;
	}

}
