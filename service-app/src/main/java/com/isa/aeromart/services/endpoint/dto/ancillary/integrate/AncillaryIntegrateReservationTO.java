package com.isa.aeromart.services.endpoint.dto.ancillary.integrate;

import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;

public class AncillaryIntegrateReservationTO {

	private List<AncillaryIntegratePassengerTO> passengers;

    private List<LCCClientReservationInsurance> reservationInsurances;
    
	private boolean jnTaxApplicable;
	
	private BigDecimal taxRatio;

    private BigDecimal netAmount;

    public List<AncillaryIntegratePassengerTO> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<AncillaryIntegratePassengerTO> passengers) {
		this.passengers = passengers;
	}

    public List<LCCClientReservationInsurance> getReservationInsurances() {
        return reservationInsurances;
    }

    public void setReservationInsurances(List<LCCClientReservationInsurance> reservationInsurances) {
        this.reservationInsurances = reservationInsurances;
    }

    public BigDecimal getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

	public boolean isJnTaxApplicable() {
		return jnTaxApplicable;
	}

	public void setJnTaxApplicable(boolean jnTaxApplicable) {
		this.jnTaxApplicable = jnTaxApplicable;
	}

	public BigDecimal getTaxRatio() {
		return taxRatio;
	}

	public void setTaxRatio(BigDecimal taxRatio) {
		this.taxRatio = taxRatio;
	}
    
}
