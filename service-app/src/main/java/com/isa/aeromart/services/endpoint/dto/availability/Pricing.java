package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.ArrayList;
import java.util.List;

public class Pricing {

	// Currency code
	private String currency;

	// Available payment options
	private List<String> paymentOptions;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<String> getPaymentOptions() {
		if (paymentOptions == null) {
			paymentOptions = new ArrayList<String>();
		}
		return paymentOptions;
	}

	public void setPaymentOptions(List<String> paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

}
