package com.isa.aeromart.services.endpoint.controller.booking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.dto.booking.EmailItineraryRQ;
import com.isa.aeromart.services.endpoint.dto.booking.PrintItineraryRQ;
import com.isa.aeromart.services.endpoint.dto.booking.PrintItineraryRS;
import com.isa.aeromart.services.endpoint.dto.booking.PrintTaxInvoiceRQ;
import com.isa.aeromart.services.endpoint.dto.booking.PrintTaxInvoiceRS;
import com.isa.aeromart.services.endpoint.dto.booking.GetTaxInvoiceListRS;
import com.isa.aeromart.services.endpoint.dto.booking.SendItineraryRS;
import com.isa.thinair.commons.api.exception.ModuleException;

@Controller
@RequestMapping("reservation")
public class ItineraryController extends StatefulController {

	@Autowired
	private BookingService bookingService;

	@ResponseBody
	@RequestMapping(value = "/itinerary/print", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			PrintItineraryRS confirmBooking(@RequestBody PrintItineraryRQ printItineraryRQ) throws ModuleException {
		PrintItineraryRS printItineraryRS = new PrintItineraryRS();
		printItineraryRS.setItinerary(bookingService.getItineraryInfo(printItineraryRQ, getTrackInfo()));
		printItineraryRS.setSuccess(true);
		return printItineraryRS;
	}
	
	@ResponseBody
	@RequestMapping(value = "/itinerary/email", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			SendItineraryRS emailItinerary(@RequestBody EmailItineraryRQ emailItineraryRQ) throws ModuleException {
		SendItineraryRS sendItineraryRS = new SendItineraryRS();
		bookingService.sendItineraryEmail(emailItineraryRQ, getTrackInfo());
		sendItineraryRS.setSuccess(true);
		return sendItineraryRS;
	}
	
	@ResponseBody
	@RequestMapping(value = "/taxInvoice/print", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			PrintTaxInvoiceRS taxInvoicePrint(@RequestBody PrintTaxInvoiceRQ printTaxInvoiceRQ) throws ModuleException {
		PrintTaxInvoiceRS printTaxInvoiceRS = new PrintTaxInvoiceRS();
		printTaxInvoiceRS.setTaxInvoice(bookingService.getTaxInvoiceInfo(printTaxInvoiceRQ, getTrackInfo()));
		printTaxInvoiceRS.setSuccess(true);
		return printTaxInvoiceRS;
	}
	
	@ResponseBody
	@RequestMapping(value = "/taxInvoice/getList", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			GetTaxInvoiceListRS taxInvoiceGetList(@RequestBody PrintTaxInvoiceRQ printTaxInvoiceRQ) throws ModuleException {
		GetTaxInvoiceListRS getTaxInvoiceListRS = new GetTaxInvoiceListRS();
		getTaxInvoiceListRS.setTaxInvoicesList(bookingService.getTaxInvoiceList(printTaxInvoiceRQ, getTrackInfo()));
		getTaxInvoiceListRS.setSuccess(true);
		return getTaxInvoiceListRS;
	}

}