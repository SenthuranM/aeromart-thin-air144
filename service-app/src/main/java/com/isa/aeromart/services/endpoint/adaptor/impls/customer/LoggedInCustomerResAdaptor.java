package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.LoggedInCustomer;
import com.isa.thinair.aircustomer.api.constants.SocialCustomerType;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

public class LoggedInCustomerResAdaptor implements Adaptor<Customer, LoggedInCustomer> {

	private static Log log = LogFactory.getLog(LoggedInCustomerResAdaptor.class);

	@Override
	public LoggedInCustomer adapt(Customer customer) {
		LoggedInCustomer loggedInCustomer = new LoggedInCustomer();
		loggedInCustomer.setFirstName(customer.getFirstName());
		loggedInCustomer.setLastName(customer.getLastName());
		loggedInCustomer.setTitle(customer.getTitle());
		loggedInCustomer.setDateOfBirth(customer.getDateOfBirth());
		loggedInCustomer.setGender(customer.getGender());
		loggedInCustomer.setNationalityCode(customer.getNationalityCode());
		Set<SocialCustomer> linkedSocialProfiles = customer.getSocialCustomer();
		String socialSiteCustId = null;
		int socialCustomerTypeId = -1;

		if (linkedSocialProfiles != null && !linkedSocialProfiles.isEmpty()) {
			Iterator<SocialCustomer> it = linkedSocialProfiles.iterator();
			while (it.hasNext()) {
				SocialCustomer socialProfile = it.next();

				if (socialProfile.getSocialCustomerTypeId().equals(SocialCustomerType.FACEBOOK.getId())) {
					socialSiteCustId = socialProfile.getSocialSiteCustId();
					socialCustomerTypeId = socialProfile.getSocialCustomerTypeId().intValue();
					break;

				}

			}

		}
		LoyaltyCustomerServiceBD loyaltyCustomerBD = ModuleServiceLocator.getLoyaltyCustomerBD();
		try {
			LoyaltyCustomerProfile loyaltyProfile = loyaltyCustomerBD.getLoyaltyCustomerByCustomerId(customer.getCustomerId());
			loggedInCustomer.setLoyaltyAccountExist(loyaltyProfile == null ? false : true);
			if (loyaltyProfile != null) {
				loggedInCustomer.setLoyaltyAccountNumber(loyaltyProfile.getLoyaltyAccountNo());
			}
		} catch (ModuleException me) {
			log.error("LoggedInCustomerResTransformer ==> Error retrieving loyalty customer details", me);
		}

		loggedInCustomer.setSocialSiteCustId(socialSiteCustId != null ? socialSiteCustId : "-1");
		loggedInCustomer.setSocialCustomerTypeId(socialCustomerTypeId);

		return loggedInCustomer;
	}

}
