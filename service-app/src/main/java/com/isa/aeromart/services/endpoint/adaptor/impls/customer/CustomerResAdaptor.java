package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import java.util.HashSet;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Address;
import com.isa.aeromart.services.endpoint.dto.common.EmergencyContact;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerContact;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerQuestionaireMap;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerQuestionaire;


public class CustomerResAdaptor implements Adaptor<Customer , CustomerDetails>{

	@Override
	public CustomerDetails adapt(Customer customerModel) {
		
		
		CustomerDetails customer = new CustomerDetails();
		
		customer.setTitle(customerModel.getTitle());
		customer.setFirstName(customerModel.getFirstName());
		customer.setLastName(customerModel.getLastName());
		customer.setCustomerID(String.valueOf(customerModel.getCustomerId()));
		customer.setPassword(customerModel.getPassword());
		customer.setGender(customerModel.getGender());
		customer.setDateOfBirth(customerModel.getDateOfBirth());
		customer.setCountryCode(customerModel.getCountryCode());
		customer.setNationalityCode(Integer.valueOf(customerModel.getNationalityCode()));
		customer.setStatus(customerModel.getStatus());
		customer.setSecretQuestion(customerModel.getSecretQuestion());
		customer.setSecretAnswer(customerModel.getSecretAnswer());
		customer.setRegistrationDate(customerModel.getRegistration_date().getTime());
		customer.setConfirmationDate(customerModel.getConfirmation_date().getTime());
		
		CustomerQuestionaireResAdaptor customerQuestionaireResTransformer = new CustomerQuestionaireResAdaptor();
		
		Set<CustomerQuestionaireMap> customerQuestionaireList = new HashSet<CustomerQuestionaireMap>();
		
		for(CustomerQuestionaire customerQuestionaire : customerModel.getCustomerQuestionaire()){
			customerQuestionaireList.add(customerQuestionaireResTransformer.adapt(customerQuestionaire));
		}
		
		customer.setCustomerQuestionaire(customerQuestionaireList);
		
		CustomerContact customerContact = new CustomerContact();
		PhoneNumberResAdaptor phoneNumberResTransformer = new PhoneNumberResAdaptor();
		Address address = new Address();
		EmergencyContact emergencyContact = new EmergencyContact();
		
		address.setStreetAddress1(customerModel.getAddressLine());
		address.setStreetAddress2(customerModel.getAddressStreet());
		address.setZipCode(customerModel.getZipCode());
		address.setCity(customerModel.getCity());
		
		customerContact.setAddress(address);
		customerContact.setEmailAddress(customerModel.getEmailId());		
		
		customerContact.setMobileNumber(phoneNumberResTransformer.adapt(customerModel.getMobile()));
		customerContact.setTelephoneNumber(phoneNumberResTransformer.adapt(customerModel.getTelephone()));
		customerContact.setFax(phoneNumberResTransformer.adapt(customerModel.getFax()));
		
		customerContact.setAlternativeEmailId(customerModel.getAlternativeEmailId());
		
		emergencyContact.setEmgnTitle(customerModel.getEmgnTitle());
		emergencyContact.setEmgnFirstName(customerModel.getEmgnFirstName());
		emergencyContact.setEmgnLastName(customerModel.getEmgnLastName());
		emergencyContact.setEmgnPhoneNumber(phoneNumberResTransformer.adapt(customerModel.getEmgnPhoneNo()));
		emergencyContact.setEmgnEmail(customerModel.getEmailId());
		
		customerContact.setEmergencyContact(emergencyContact);
		
		customer.setCustomerContact(customerContact);
		customer.setSendPromoEmail(customerModel.getSendPromoEmail());
		
		//TODO add social customer details
		return customer;
	}

}
