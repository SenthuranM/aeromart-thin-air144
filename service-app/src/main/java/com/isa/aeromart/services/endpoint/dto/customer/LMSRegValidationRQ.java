package com.isa.aeromart.services.endpoint.dto.customer;

public class LMSRegValidationRQ {

	private LMSRegValidationInfo lmsRegValidationInfo;

	private boolean myAACustomerOnly;

	public LMSRegValidationInfo getLmsRegValidationInfo() {
		return lmsRegValidationInfo;
	}

	public void setLmsRegValidationInfo(LMSRegValidationInfo lmsRegValidationInfo) {
		this.lmsRegValidationInfo = lmsRegValidationInfo;
	}

	public boolean isMyAACustomerOnly() {
		return myAACustomerOnly;
	}

	public void setMyAACustomerOnly(boolean myAACustomerOnly) {
		this.myAACustomerOnly = myAACustomerOnly;
	}

}
