package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.Set;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;


public class BundleFareSummary {
	private BigDecimal bundleFareFree = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Set<String> bundledFareFreeServiceNames;

	private Integer bundledFareId;

	public BigDecimal getBundleFareFree() {
		return bundleFareFree;
	}

	public void setBundleFareFree(BigDecimal bundleFareFree) {
		this.bundleFareFree = bundleFareFree;
	}

	public Set<String> getBundledFareFreeServiceNames() {
		return bundledFareFreeServiceNames;
	}

	public void setBundledFareFreeServiceNames(Set<String> bundledFareFreeServiceNames) {
		this.bundledFareFreeServiceNames = bundledFareFreeServiceNames;
	}

	public Integer getBundledFareId() {
		return bundledFareId;
	}

	public void setBundledFareId(Integer bundledFareId) {
		this.bundledFareId = bundledFareId;
	}
	
}
