package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

class ONDTaxAdaptor extends ONDBaseChargeAdaptor implements ONDChargeAdaptor<TaxTO> {

	public ONDTaxAdaptor(ONDPriceAdaptor ondAdaptor) {
		super(ondAdaptor);
	}

	@Override
	public void addCharge(String paxType, TaxTO tax) {
		PaxPrice paxPrice = getPaxPrice(paxType, tax);
		paxPrice.setTax(AccelAeroCalculator.add(paxPrice.getTax(), tax.getAmount()));
		paxPrice.setTotal(AccelAeroCalculator.add(paxPrice.getTotal(), tax.getAmount()));
		OndOWPricing ondPrice = getOndPrice(tax.getOndSequence());
		ondPrice.setTotalTaxes(AccelAeroCalculator.add(ondPrice.getTotalTaxes(), tax.getAmount()));
		ondPrice.setTotalPrice(AccelAeroCalculator.add(ondPrice.getTotalPrice(), tax.getAmount()));
	}

}