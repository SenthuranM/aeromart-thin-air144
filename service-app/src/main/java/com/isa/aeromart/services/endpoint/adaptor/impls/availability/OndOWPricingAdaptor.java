package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;

public class OndOWPricingAdaptor {

	private TravellerQuantity travellerQuantity;

	public OndOWPricingAdaptor(TravellerQuantity travellerQuantity) {
		this.travellerQuantity = travellerQuantity;
	}

	public List<OndOWPricing> adapt(FareTypeTO source) {
		ONDPriceAdaptor adaptor = new ONDPriceAdaptor(travellerQuantity, source.getOndFlexiSelection());
		for (PerPaxPriceInfoTO paxPriceInfo : source.getPerPaxPriceInfo()) {
			adaptor.adaptPaxPrice(paxPriceInfo);
		}
		return adaptor.getOwPricing();
	}
}
