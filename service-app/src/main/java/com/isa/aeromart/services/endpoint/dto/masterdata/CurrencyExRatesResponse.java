package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

public class CurrencyExRatesResponse {

	private List<CurrencyExchangeRate> currencyExRates;

	public List<CurrencyExchangeRate> getCurrencyExRates() {
		return currencyExRates;
	}

	public void setCurrencyExRates(List<CurrencyExchangeRate> currencyExRates) {
		this.currencyExRates = currencyExRates;
	}
	
}
