package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;

public class PricedAncillaryScope {

	private Scope scope;

	private List<PricedAncillaryItem> items;

	private List<MonetaryAmendment> amendments;

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public List<PricedAncillaryItem> getItems() {
		return items;
	}

	public void setItems(List<PricedAncillaryItem> items) {
		this.items = items;
	}

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}
}
