package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;


public class Preference {

	private Assignee assignee;
	
	private List<Selection> selections;

	public Assignee getAssignee() {
		return assignee;
	}

	public void setAssignee(Assignee assignee) {
		this.assignee = assignee;
	}

	public List<Selection> getSelections() {
		return selections;
	}

	public void setSelections(List<Selection> selections) {
		this.selections = selections;
	}

}
