package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public abstract class TransactionAwareAdaptor<S, T> implements Adaptor<S, T> {

	private TrackInfoDTO trackInfoDTO;
	private String transactionId;
	private MessageSource errorMessageSource;
	private TransactionalAncillaryService transactionalAncillaryService;


	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public TransactionalAncillaryService getTransactionalAncillaryService() {
		return transactionalAncillaryService;
	}

	public void setTransactionalAncillaryService(TransactionalAncillaryService transactionalAncillaryService) {
		this.transactionalAncillaryService = transactionalAncillaryService;
	}

	public MessageSource getErrorMessageSource() {
		return errorMessageSource;
	}

	public void setErrorMessageSource(MessageSource errorMessageSource) {
		this.errorMessageSource = errorMessageSource;
	}

	public TrackInfoDTO getTrackInfoDTO() {
		return trackInfoDTO;
	}

	public void setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;
	}


}
