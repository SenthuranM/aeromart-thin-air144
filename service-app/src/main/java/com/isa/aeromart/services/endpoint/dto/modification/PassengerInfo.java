package com.isa.aeromart.services.endpoint.dto.modification;

public class PassengerInfo {

	private String rph;

	private String passengerName;
	
	private String paxType;

	private String accompaniedPaxRPH;

	public String getRph() {
		return rph;
	}

	public void setRph(String rph) {
		this.rph = rph;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getAccompaniedPaxRPH() {
		return accompaniedPaxRPH;
	}

	public void setAccompaniedPaxRPH(String accompaniedPaxRPH) {
		this.accompaniedPaxRPH = accompaniedPaxRPH;
	}

}
