package com.isa.aeromart.services.endpoint.dto.customer;

public class SocialCustomer {

	private int socialCustomerId; 

	private int customerId;

    private String socialSiteCustId;

    private int socialCustomerTypeId;

	public int getSocialCustomerId() {
		return socialCustomerId;
	}

	public void setSocialCustomerId(int socialCustomerId) {
		this.socialCustomerId = socialCustomerId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getSocialSiteCustId() {
		return socialSiteCustId;
	}

	public void setSocialSiteCustId(String socialSiteCustId) {
		this.socialSiteCustId = socialSiteCustId;
	}

	public int getSocialCustomerTypeId() {
		return socialCustomerTypeId;
	}

	public void setSocialCustomerTypeId(int socialCustomerTypeId) {
		this.socialCustomerTypeId = socialCustomerTypeId;
	}
        
}
