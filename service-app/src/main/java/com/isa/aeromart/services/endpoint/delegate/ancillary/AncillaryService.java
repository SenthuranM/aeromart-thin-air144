package com.isa.aeromart.services.endpoint.delegate.ancillary;

import com.isa.aeromart.services.endpoint.dto.ancillary.api.AllocateAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AllocateAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillarySummaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AvailableAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BaggageRateRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.CustomerPreference;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.DefaultAncillarySelectionRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.DefaultAncillarySelectionsRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.PrePaymentTO;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface AncillaryService {

	// Check Ancillaries Availability --------
	AncillaryAvailabilityRS checkAncillaryAvailabilityOnCreateReservation(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request)
			throws ModuleException;

	AncillaryAvailabilityRS checkAncillaryAvailabilityOnModifyReservation(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request)
			throws ModuleException;

	AncillaryAvailabilityRS checkAncillaryAvailabilityOnModifyAncillary(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request)
			throws ModuleException;
	// Check Ancillaries Availability --------

	AncillarySummaryRS getAncillarySummary(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request) throws ModuleException;

	public BaggageRateRS getBaggageRates(TrackInfoDTO trackInfoDTO, FlightPriceRS flightPriceRS,
			AvailabilitySearchRQ availabilitySearchReq) throws ModuleException;

	// Get Available Ancillaries --------
	AncillaryRS getAncillaryOnCreateReservation(TrackInfoDTO trackInfo, AvailableAncillaryRQ request) throws ModuleException;

	AncillaryRS getAncillaryOnModifyReservation(TrackInfoDTO trackInfo, AvailableAncillaryRQ request) throws ModuleException;

	AncillaryRS getAncillaryOnModifyAncillary(TrackInfoDTO trackInfo, AvailableAncillaryRQ request) throws ModuleException;

	// Get Available Ancillaries --------

	// Get Default Ancillaries --------
	DefaultAncillarySelectionRS getDefaultAncillaryOnCreateReservation(TrackInfoDTO trackInfo,
			DefaultAncillarySelectionsRQ request, CustomerPreference customerPrefences) throws Exception;

	DefaultAncillarySelectionRS getDefaultAncillaryOnModifyReservation(TrackInfoDTO trackInfo,
			DefaultAncillarySelectionsRQ request) throws Exception;

	DefaultAncillarySelectionRS getDefaultAncillaryOnModifyAncillary(TrackInfoDTO trackInfo, DefaultAncillarySelectionsRQ request)
			throws Exception;

	// Get Default Ancillaries --------

	BlockAncillaryRS blockAncillary(TrackInfoDTO trackInfo, BlockAncillaryRQ request) throws ModuleException;

	PriceQuoteAncillaryRS getAncillaryPriceQuotation(TrackInfoDTO trackInfo, PriceQuoteAncillaryRQ request)
			throws ModuleException;

	PriceQuoteAncillaryRS getAncillaryPriceQuotationOnCreateReservation(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException;

	PriceQuoteAncillaryRS getAncillaryPriceQuotationOnModifyReservation(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException;

	PriceQuoteAncillaryRS getAncillaryPriceQuotationOnModifyAncillary(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException;

	// Set Ancillaries --------
	AllocateAncillaryRS allocateAncillaryOnCreateReservation(TrackInfoDTO trackInfo, AllocateAncillaryRQ request)
			throws ModuleException;

	AllocateAncillaryRS allocateAncillaryOnModifyReservation(TrackInfoDTO trackInfo, AllocateAncillaryRQ request)
			throws ModuleException;

	AllocateAncillaryRS allocateAncillaryOnModifyAncillary(TrackInfoDTO trackInfo, AllocateAncillaryRQ request)
			throws ModuleException;

	// Set Ancillaries --------

	TransactionalBaseRS updateAncillary(TrackInfoDTO trackInfo, TransactionalBaseRQ baseRequest) throws Exception;

	// Integration Services
	AncillaryIntegrateTO getAncillaryIntegrateTO(TrackInfoDTO trackInfo, TransactionalBaseRQ baseRequest) throws ModuleException;

	PrePaymentTO getPrePaymentTO(TrackInfoDTO trackInfo, TransactionalBaseRQ baseRequest) throws Exception;

	BalanceSummaryRS getAncillaryBalanceSummery(TrackInfoDTO trackInfo, TransactionalBaseRQ request) throws Exception;

	// Integration Services

}
