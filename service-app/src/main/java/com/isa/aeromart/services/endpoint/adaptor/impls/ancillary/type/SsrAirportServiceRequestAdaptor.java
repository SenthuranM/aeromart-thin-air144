package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public class SsrAirportServiceRequestAdaptor extends AncillaryRequestAdaptor {

    public String getLCCAncillaryType() {
        return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AIRPORT_SERVICE;
    }

	public Object toAvailableAncillaries(TrackInfoDTO trackInfo, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {
        AvailableAncillariesSsrAirportServiceRequestWrapper wrapper = new AvailableAncillariesSsrAirportServiceRequestWrapper();

		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);

		Collections.sort(flightSegmentTOs);

        wrapper.setFlightSegmentTOs(flightSegmentTOs);
        wrapper.setAppIndicator(trackInfo.getAppIndicator());
        wrapper.setSsrCategory(SSRCategory.ID_AIRPORT_SERVICE.intValue());
        wrapper.setSystem(system); 
        wrapper.setSelectedLanguage(LocaleContextHolder.getLocale().toString()); 
        wrapper.setTransactionIdentifier(isModifyAnci? null : lccTransactionId != null ? lccTransactionId : "");
        wrapper.setTrackInfo(trackInfo);
        wrapper.setServiceCount(serviceCount == null ? new HashMap<>() : serviceCount);
        wrapper.setIsModifyAnci(isModifyAnci);
        wrapper.setBundledFareDTOs(pricingInformation.getOndBundledFareDTOs());
        wrapper.setPnr(pnr); 

        return wrapper;
    }

	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {
		List<SelectedItem> selectedItems;
		AirportScope airportScope;
		String flightSegmentRPH;
		String airportCode;
		boolean alreadyExists;

		LCCAirportServiceDTO selectedAirportService;
		List<LCCAirportServiceDTO> selectedAirportServices, airportWiseSelectedAirportServices;
		Map<AirportScope, List<LCCAirportServiceDTO>> airportWiseSelectedAirportServicesMapping = new HashMap<>();
		Map<String, List<LCCAirportServiceDTO>> selectedAirportServicesMapping = new HashMap<>();

		for (Preference preference : preferences) {
			List<Selection> selections = preference.getSelections();
			for (Selection selection : selections) {
				airportScope = (AirportScope) selection.getScope();
				flightSegmentRPH = airportScope.getFlightSegmentRPH();
				airportCode = airportScope.getAirportCode();

				if (!airportWiseSelectedAirportServicesMapping.containsKey(airportScope)) {
					airportWiseSelectedAirportServicesMapping.put(airportScope, new ArrayList<>());
				}

				airportWiseSelectedAirportServices = airportWiseSelectedAirportServicesMapping.get(airportScope);

				selectedItems = selection.getSelectedItems();
				for (SelectedItem selectedItem : selectedItems) {
					alreadyExists = false;
					selectedAirportService = new LCCAirportServiceDTO();
					selectedAirportService.setSsrCode(selectedItem.getId());
					selectedAirportService.setAirportCode(airportCode);
					if (airportWiseSelectedAirportServices.isEmpty()) {
						airportWiseSelectedAirportServices.add(selectedAirportService);
					} else {
						for (LCCAirportServiceDTO lccAirportServiceDTO : airportWiseSelectedAirportServices) {
							if (lccAirportServiceDTO.getSsrCode().equals(selectedItem.getId())) {
								alreadyExists = true;
							}
						}
						if (!alreadyExists) {
							airportWiseSelectedAirportServices.add(selectedAirportService);
						}
					}
				}

			}
		}
		if (!airportWiseSelectedAirportServicesMapping.isEmpty()) {
			for (AirportScope airportScopeRef : airportWiseSelectedAirportServicesMapping.keySet()) {
				if (!selectedAirportServicesMapping.containsKey(airportScopeRef.getFlightSegmentRPH())) {
					selectedAirportServicesMapping.put(airportScopeRef.getFlightSegmentRPH(), new ArrayList<>());
				}
				selectedAirportServices = selectedAirportServicesMapping.get(airportScopeRef.getFlightSegmentRPH());
				selectedAirportServices.addAll(airportWiseSelectedAirportServicesMapping.get(airportScopeRef));
			}

		}

		return selectedAirportServicesMapping;

	}

	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;

	}

	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;

	}

	public class AvailableAncillariesSsrAirportServiceRequestWrapper {
        private List<FlightSegmentTO> flightSegmentTOs;
        private AppIndicatorEnum appIndicator;
        private int ssrCategory;
        private ProxyConstants.SYSTEM system;
        private String selectedLanguage;
        private String transactionIdentifier;
        private BasicTrackInfo trackInfo;
        private Map<String, Integer> serviceCount;
        private boolean isModifyAnci;
        private List<BundledFareDTO> bundledFareDTOs;
        private String pnr;

        public List<FlightSegmentTO> getFlightSegmentTOs() {
            return flightSegmentTOs;
        }

        public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
            this.flightSegmentTOs = flightSegmentTOs;
        }

        public AppIndicatorEnum getAppIndicator() {
            return appIndicator;
        }

        public void setAppIndicator(AppIndicatorEnum appIndicator) {
            this.appIndicator = appIndicator;
        }

        public int getSsrCategory() {
            return ssrCategory;
        }

        public void setSsrCategory(int ssrCategory) {
            this.ssrCategory = ssrCategory;
        }

        public ProxyConstants.SYSTEM getSystem() {
            return system;
        }

        public void setSystem(ProxyConstants.SYSTEM system) {
            this.system = system;
        }

        public String getSelectedLanguage() {
            return selectedLanguage;
        }

        public void setSelectedLanguage(String selectedLanguage) {
            this.selectedLanguage = selectedLanguage;
        }

        public String getTransactionIdentifier() {
            return transactionIdentifier;
        }

        public void setTransactionIdentifier(String transactionIdentifier) {
            this.transactionIdentifier = transactionIdentifier;
        }

        public BasicTrackInfo getTrackInfo() {
            return trackInfo;
        }

        public void setTrackInfo(BasicTrackInfo trackInfo) {
            this.trackInfo = trackInfo;
        }

        public Map<String, Integer> getServiceCount() {
            return serviceCount;
        }

        public void setServiceCount(Map<String, Integer> serviceCount) {
            this.serviceCount = serviceCount;
        }

        public boolean isModifyAnci() {
            return isModifyAnci;
        }

        public void setIsModifyAnci(boolean isModifyAnci) {
            this.isModifyAnci = isModifyAnci;
        }

        public List<BundledFareDTO> getBundledFareDTOs() {
            return bundledFareDTOs;
        }

        public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
            this.bundledFareDTOs = bundledFareDTOs;
        }

        public String getPnr() {
            return pnr;
        }

        public void setPnr(String pnr) {
            this.pnr = pnr;
        }
    }
}
