package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;

public class SessionMetaData {

	private List<OndUnit> ondUnits;

	public List<OndUnit> getOndUnits() {
		return ondUnits;
	}

	public void setOndUnits(List<OndUnit> ondUnits) {
		this.ondUnits = ondUnits;
	}
}
