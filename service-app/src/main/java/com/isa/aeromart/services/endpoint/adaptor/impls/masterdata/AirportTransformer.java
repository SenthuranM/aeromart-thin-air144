package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportDTO;
import com.isa.thinair.airmaster.api.model.Airport;

public class AirportTransformer implements Adaptor<Airport, AirportDTO> {

	@Override
	public AirportDTO adapt(Airport source) {
		AirportDTO target = new AirportDTO();
		target.setAirportCode(source.getAirportCode());
		target.setAirportName(source.getAirportName());
		return target;
	}

}
