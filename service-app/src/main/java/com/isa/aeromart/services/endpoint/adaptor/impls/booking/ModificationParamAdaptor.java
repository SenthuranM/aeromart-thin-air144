package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.ModificationParams;
import com.isa.aeromart.services.endpoint.dto.booking.PaxModificationParams;
import com.isa.aeromart.services.endpoint.dto.booking.SegmentModificationParams;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.PNRModificationAuthorizationUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;

public class ModificationParamAdaptor implements Adaptor<LCCClientReservation, ModificationParams> {

	private boolean isRegUser = false;

	private boolean isGroupPnr;

	private Map<String, Boolean> groundSegRef;

	public ModificationParamAdaptor(boolean isGroupPnr, boolean isRegUser, Map<String, Boolean> groundSegRef) {
		this.isGroupPnr = isGroupPnr;
		this.isRegUser = isRegUser;
		this.groundSegRef = groundSegRef;
	}

	@Override
	public ModificationParams adapt(LCCClientReservation reservation) {
		ModificationParams modificationParams = new ModificationParams();
		List<SegmentModificationParams> segmentModificationParams = new ArrayList<>();
		List<PaxModificationParams> paxModificationParams = new ArrayList<>();
		boolean singleSegmexist =singleSegExist(reservation.getSegments());
		boolean unCancelledUNsegExist = isUnceancelledUNSegmentExist(reservation.getSegments());
		SegmentModificationParamsAdaptor segmentmodificationParamsAdaptor = new SegmentModificationParamsAdaptor(isGroupPnr,
				isRegUser, reservation.getLccPromotionInfoTO(), unCancelledUNsegExist,
				reservation.getInterlineModificationParams(), reservation.isModifiableReservation(), singleSegmexist,
				groundSegRef);
		PaxModificationParamsAdaptor paxModificationParamsAdaptor = new PaxModificationParamsAdaptor(
				reservation.getNameChangeCount(), isBufferTimeExceeded(reservation),
				ModificationUtils.isUnFlownlownReservation(reservation.getSegments()),
				reservation.getInterlineModificationParams(), reservation.isGroupPNR());
		
		ContactModificationParamsAdaptor contactModificationParamsAdaptor = new ContactModificationParamsAdaptor();
		contactModificationParamsAdaptor.adapt(reservation);

		AnciModificationParamsAdaptor anciModificationParamsAdaptor = new AnciModificationParamsAdaptor();
		anciModificationParamsAdaptor.adapt(reservation);

		boolean allowResCancel = true;
		List<String> fareGroupIDs = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(reservation.getSegments())) {
			
			for (LCCClientReservationSegment segment : reservation.getSegments()) {
				if(!PNRModificationAuthorizationUtils.isSegmentModifiableAsPerETicketStatus(reservation,segment.getPnrSegID())){
					segment.setModifible(false);
					segment.setSegmentModifiableAsPerETicketStatus(false);
				}
				
				if (!segment.getSubStatus().equals(ReservationSegmentSubStatus.EXCHANGED)) {
					segmentModificationParams.add(segmentmodificationParamsAdaptor.adapt(segment));
				}

				if ((segment.isFlownSegment()) || (!segment.isModifible()
						&& !segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL))) {
					allowResCancel = false;
				}
				
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus()) &&
						!fareGroupIDs.contains(segment.getInterlineGroupKey())) {
					fareGroupIDs.add(segment.getInterlineGroupKey());
				}
			}
			adjustSegmentModificationParams(reservation.getSegments(), segmentModificationParams);			
		}

		if (reservation.getPassengers() != null && !reservation.getPassengers().isEmpty()) {
			AdaptorUtils.adaptCollection(reservation.getPassengers(), paxModificationParams, paxModificationParamsAdaptor);
		}

		ResModParamsAdaptor resModParamsAdaptor = new ResModParamsAdaptor(isGroupPnr, isRegUser, segmentModificationParams,
				singleSegmexist, allowResCancel, unCancelledUNsegExist);

		modificationParams.setReservationModificationParams(resModParamsAdaptor.adapt(reservation));
		adjustSegmentModificationParamsForFareGroupDetail(reservation.getSegments(), segmentModificationParams, fareGroupIDs);
		modificationParams.setSegmentModificationParams(segmentModificationParams);
		modificationParams.setPaxModificationParams(paxModificationParams);
		modificationParams.setContactModificationParams(contactModificationParamsAdaptor.adapt(reservation));
		modificationParams.setAnciModificationParams(anciModificationParamsAdaptor.adapt(reservation));
		
		return modificationParams;
	}

	private void adjustSegmentModificationParamsForFareGroupDetail(Set<LCCClientReservationSegment> segments, List<SegmentModificationParams> segmentModificationParams,
			List<String> fareGroupIDs) {
		
		if (fareGroupIDs.size() == 1) {
			
			Map<String, List<String>> interlineGroupKeyWiseSegments = buildInterlineGroupKeyWiseSegments(segments);
			
			for (LCCClientReservationSegment segment : segments) {
				
				List<String> rphList = interlineGroupKeyWiseSegments.get(segment.getInterlineGroupKey());
				
				for (SegmentModificationParams modParam : segmentModificationParams) {
					
					if (modParam.isCancellable() &&  rphList.contains(modParam.getReservationSegmentRPH())) {							
						modParam.setCancellable(false);					
					}
				}
			}
			
		}
		
	}

	private void adjustSegmentModificationParams(Set<LCCClientReservationSegment> segments,
			List<SegmentModificationParams> segmentModificationParams) {
		Map<String, List<String>> interlineGroupKeyWiseSegments = buildInterlineGroupKeyWiseSegments(segments);
		for (LCCClientReservationSegment segment : segments) {
			List<String> rphList = interlineGroupKeyWiseSegments.get(segment.getInterlineGroupKey());
			for (SegmentModificationParams modParam : segmentModificationParams) {
				if (!modParam.isCancellable() && !modParam.isModifible()
						&& rphList.contains(modParam.getReservationSegmentRPH())) {
					for (SegmentModificationParams subModParam : segmentModificationParams) {
						if (rphList.contains(subModParam.getReservationSegmentRPH())) {
							subModParam.setCancellable(false);
							subModParam.setModifible(false);
						}
					}
				}
			}
		}
	}

	private boolean isBufferTimeExceeded(LCCClientReservation reservation) {
		boolean bufferTimeExceded = false;
		List<LCCClientReservationSegment> segmentList = new ArrayList<>(reservation.getSegments());
		Collections.sort(segmentList);
		for (LCCClientReservationSegment segment : segmentList) {
			if (!ReservationSegmentSubStatus.EXCHANGED.equals(segment.getSubStatus())) {
				bufferTimeExceded = segment.getModifyTillBufferDateTime().before(new Date()) || segment.isFlownSegment();
				if (bufferTimeExceded) {
					break;
				}
			}
		}
		return bufferTimeExceded;
	}

	private Map<String, List<String>> buildInterlineGroupKeyWiseSegments(Set<LCCClientReservationSegment> segments) {
		Map<String, List<String>> interlineGroupKeyWiseSegments = new HashMap<>();
		for (LCCClientReservationSegment segment : segments) {
			List<String> segmentRPHList = interlineGroupKeyWiseSegments.get(segment.getInterlineGroupKey());
			String newResSegRPH = ModificationUtils.createResSegRph(segment.getCarrierCode(), segment.getSegmentSeq());
			if (segmentRPHList == null) {
				segmentRPHList = new ArrayList<>();
				interlineGroupKeyWiseSegments.put(segment.getInterlineGroupKey(), segmentRPHList);
			}
			segmentRPHList.add(newResSegRPH);
		}
		return interlineGroupKeyWiseSegments;
	}

	private boolean isUnceancelledUNSegmentExist(Set<LCCClientReservationSegment> segments) {
		boolean isUNSegmentExist = false;
		for (LCCClientReservationSegment segment : segments) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())
					&& segment.isUnSegment())
				isUNSegmentExist = true;

		}
		return isUNSegmentExist;

	}

	private boolean singleSegExist(Set<LCCClientReservationSegment> segList){
		byte segCount =0, busSegCount=0;
		for(LCCClientReservationSegment seg : segList){
			if(!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus())){
				segCount++;
				if(seg.getGroundStationPnrSegmentID() !=null){
					busSegCount++;
				}
			}
		}
		return (segCount-busSegCount)==1;
	}
}
