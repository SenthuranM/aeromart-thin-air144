package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;

/**
 * @author priyantha
 *
 */
public class AvailableFareClass {

	private String fareClassCode;

	private int availableSeats;

	@CurrencyValue
	private BigDecimal price;

	private boolean selected;

	private List<String> visibleChannels;

	private AdditionalFareClassInfo additionalFareClassInfo;

	private String description;

	public String getFareClassCode() {
		return fareClassCode;
	}

	public void setFareClassCode(String fareClassCode) {
		this.fareClassCode = fareClassCode;
	}

	public int getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public List<String> getVisibleChannels() {
		if (visibleChannels == null) {
			visibleChannels = new ArrayList<String>();
		}
		return visibleChannels;
	}

	public void setVisibleChannels(List<String> visibleChannels) {
		this.visibleChannels = visibleChannels;
	}

	public AdditionalFareClassInfo getAdditionalFareClassInfo() {
		return additionalFareClassInfo;
	}

	public void setAdditionalFareClassInfo(AdditionalFareClassInfo additionalFareClassInfo) {
		this.additionalFareClassInfo = additionalFareClassInfo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
