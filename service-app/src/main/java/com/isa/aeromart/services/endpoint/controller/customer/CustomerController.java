package com.isa.aeromart.services.endpoint.controller.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerDetailsAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerLiteReqAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerReqAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerSaveOrUpdateReqAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.FamilyMemberRQAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.FamilyMemberRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.LMSDetailsReqAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.LMSDetailsResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.LoggedInLMSDetailsAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.LoyaltyProfileAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.PassengerDetailsAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.customer.CustomerService;
import com.isa.aeromart.services.endpoint.dto.common.BaseRS;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.customer.AncillaryPreferenceRQ;
import com.isa.aeromart.services.endpoint.dto.customer.AncillaryPreferenceRS;
import com.isa.aeromart.services.endpoint.dto.customer.ConfirmCustomerRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ConfirmCustomerRS;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerAliasRQ;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerAliasRS;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerCredit;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerCreditRS;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerDetailsRS;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerLoginCheckRQ;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerLoginCheckRS;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerLoginRQ;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerLoginRS;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerPasswordResetRS;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerSaveOrUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerSaveOrUpdateRS;
import com.isa.aeromart.services.endpoint.dto.customer.FamilyMemberDetailsRQ;
import com.isa.aeromart.services.endpoint.dto.customer.FamilyMemberDetailsRS;
import com.isa.aeromart.services.endpoint.dto.customer.ForgotPasswordRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ForgotPasswordRS;
import com.isa.aeromart.services.endpoint.dto.customer.LMSConfirmationRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.customer.LMSCrossPortalLoginRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSCrossPortalLoginRS;
import com.isa.aeromart.services.endpoint.dto.customer.LMSDetails;
import com.isa.aeromart.services.endpoint.dto.customer.LMSPointBalanceRS;
import com.isa.aeromart.services.endpoint.dto.customer.LMSRegisterRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSRegisterRS;
import com.isa.aeromart.services.endpoint.dto.customer.LMSValidationRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSValidationRS;
import com.isa.aeromart.services.endpoint.dto.customer.LoggedInCustomerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.LoggedInLmsDetails;
import com.isa.aeromart.services.endpoint.dto.customer.LoyaltyProfileDetailsRQ;
import com.isa.aeromart.services.endpoint.dto.customer.PassengerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.PassengerPageLoginRS;
import com.isa.aeromart.services.endpoint.dto.customer.PasswordChangeRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ReservationDetails;
import com.isa.aeromart.services.endpoint.dto.customer.TravelHistoryRS;
import com.isa.aeromart.services.endpoint.dto.customer.UserTokenDetails;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.CustomerValidationService;
import com.isa.aeromart.services.endpoint.service.CustomerValidationServiceImpl;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.common.StringUtil;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.aeromart.services.endpoint.utils.payment.PaymentUtils;
import com.isa.thinair.aircustomer.api.constants.CustomAliasConstants;
import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMeal;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMealPK;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredSeat;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;

@Controller
@RequestMapping("customer")
public class CustomerController extends StatefulController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private MessageSource errorMessageSource;

	private static Log log = LogFactory.getLog(CustomerController.class);

	@ResponseBody
	@RequestMapping(value = "/lmsmember/remote/login", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerLoginRS resetGravityMemberLogin(@RequestBody CustomerLoginRQ customerLoginReq) {
		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();
		customerValidation.validateLoginRQ(customerLoginReq);
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
		CustomerLoginRS customerLoginRS = new CustomerLoginRS();
		CustomerSessionStore customerSession = getCustomerSession();
		MemberRemoteLoginResponseDTO lmsTokenData = null;
		ArrayList<String> messages = new ArrayList<>();
		boolean loyaltyManagmentEnabled = false;
		loyaltyManagmentEnabled = AppSysParamsUtil.isLMSEnabled();
		Customer customer = new Customer();

		try {
			customer = customerDelegate.authenticate(customerLoginReq.getCustomerID(), customerLoginReq.getPassword());
			if (loyaltyManagmentEnabled)
				lmsTokenData = lmsManagementBD.memberLogin(customer.getLMSMemberDetails().getMemberExternalId(),
						customer.getLMSMemberDetails().getPassword());
		} catch (ModuleException e) {
			messages.add("Module Exception" + e);
		}
		customerLoginRS.setTransactionId(customerLoginReq.getTransactionId());
		if (lmsTokenData != null) {
			customerSession.storeMemberAuth(lmsTokenData.getToken());
			// adding gravty auth token and id to customer session
			customerSession.storeLMSRemoteId(customer.getLMSMemberDetails().getMemberExternalId());
			// adding gravty auth token and id to login response
			customerLoginRS.setAuthToken(lmsTokenData.getToken());
			customerLoginRS.setLmsRemoteId(customer.getLMSMemberDetails().getMemberExternalId());
		}
		return customerLoginRS;
	}

	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public CustomerLoginRS customerLogin(@RequestBody CustomerLoginRQ customerLoginReq) {

		if (customerLoginReq.getUserToken() != null) {
			return tokenBasedCustomerloginResponse(customerLoginReq);
		}
		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();
		customerValidation.validateLoginRQ(customerLoginReq);

		Customer customerModel = null;
		boolean loyaltyManagmentEnabled = false;
		double totalCustomerCredit = 0;
		LmsMember lmsMember = null;
		Collection<ReservationDetails> reservationDetailsList = null;
		ArrayList<String> messages = new ArrayList<>();
		List<IPGPaymentOptionDTO> ipgPaymentOptionDTOList = null;

		CustomerLoginRS customerLoginRS = new CustomerLoginRS();
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		CustomerSessionStore customerSession = getCustomerSession();
		String nationalityName = null;
		try {

			customerLoginRS.setTransactionId(customerLoginReq.getTransactionId());
			customerModel = customerDelegate.authenticate(customerLoginReq.getCustomerID(), customerLoginReq.getPassword());
			loyaltyManagmentEnabled = AppSysParamsUtil.isLMSEnabled();
			customerSession.storeLoggedInCustomerID(String.valueOf(customerModel.getCustomerId()));
			if (customerModel != null) {
				int intCustomerID = customerModel.getCustomerId();
				Collection<CustomerCredit> customerCreditList = CustomerUtil.getCustomerCreditFromAllCarriers(intCustomerID,
						getTrackInfo());
				totalCustomerCredit = Double.valueOf(CustomerUtil.calculateCustomerTotalCredit(customerCreditList));

				customerLoginRS.setHasMashreqLoyalty(customerService.isLoyaltyCustomer(customerModel.getCustomerId()));
				if (loyaltyManagmentEnabled) {
					if (!customerModel.getCustomerLmsDetails().isEmpty()) {
						lmsMember = customerModel.getCustomerLmsDetails().iterator().next();
					}
				}

			} else {
				messages.add("login failed. Couldn't find customer with given email and password.");
				customerLoginRS.setSuccess(false);
				customerLoginRS.setMessages(messages);
				customerLoginRS.setErrors(null);
				customerLoginRS.setWarnings(null);
				return customerLoginRS;
			}

			if (customerModel.getNationalityCode() != null) {
				nationalityName = ModuleServiceLocator.getCommoMasterBD().getNationality(customerModel.getNationalityCode())
						.getDescription();

			}

		} catch (Exception e) {

			messages.add("Login failed. Please try again.");
			customerLoginRS.setSuccess(false);
			customerLoginRS.setMessages(messages);
			customerLoginRS.setErrors(null);
			customerLoginRS.setWarnings(null);
			return customerLoginRS;
		}

		try {

			reservationDetailsList = CustomerUtil.getAfterReservationsList(customerModel.getCustomerId(), new Date(),
					getTrackInfo());
			UserTokenDetails userToken = new UserTokenDetails();
			String token = customerDelegate.generateToken();
			userToken.setToken(token);
			customerDelegate.createCustomerSession(customerModel.getCustomerId(), token);
			customerLoginRS.setUserToken(userToken);

		} catch (Exception e) {
			messages.add("Login failed. Please try again.");
			customerLoginRS.setSuccess(false);
			customerLoginRS.setMessages(messages);
			customerLoginRS.setErrors(null);
			customerLoginRS.setWarnings(null);
			return customerLoginRS;
		}

		try {

			ipgPaymentOptionDTOList = PaymentUtils
					.getPaymentGatewayList(com.isa.thinair.commons.api.constants.ApplicationEngine.IBE.toString());

		} catch (Exception e) {
			messages.add("Login failed. Please try again.");
			customerLoginRS.setSuccess(false);
			customerLoginRS.setMessages(messages);
			customerLoginRS.setErrors(null);
			customerLoginRS.setWarnings(null);
			return customerLoginRS;
		}

		CustomerDetailsAdaptor customerDetailsAdaptor = new CustomerDetailsAdaptor();
		LoggedInLMSDetailsAdaptor loggedInLMSDetailsAdaptor = new LoggedInLMSDetailsAdaptor();

		LoggedInCustomerDetails loggedInCustomerDetails = customerDetailsAdaptor.adapt(customerModel);

		loggedInCustomerDetails.setNationalityName(nationalityName);

		customerSession.storeAvailableCredit(totalCustomerCredit);

		if (lmsMember != null) {
			MemberRemoteLoginResponseDTO lmsTokenData = null;
			LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();

			memberLogin(lmsMember, messages, customerLoginRS, customerSession, lmsManagementBD);

			LoggedInLmsDetails loggedInLmsDetails = loggedInLMSDetailsAdaptor.adapt(lmsMember);
			customerSession.storeAvailableLMSPoints(loggedInLmsDetails.getAvailablePoints());
			customerSession.storeFFID(lmsMember.getFfid());
			customerLoginRS.setLoggedInLmsDetails(loggedInLmsDetails);

			customerSession.storeMemberEnrollimgCarrier(lmsMember.getEnrollingCarrier());
			customerSession.storeLMSRemoteId(lmsMember.getMemberExternalId());
			customerLoginRS.setLmsRemoteId(lmsMember.getMemberExternalId());
		}

		customerLoginRS.setLoggedInCustomerDetails(loggedInCustomerDetails);
		customerLoginRS.setLoyaltyManagmentEnabled(loyaltyManagmentEnabled);
		customerLoginRS.setReservationList(new HashSet<ReservationDetails>(reservationDetailsList));
		customerLoginRS.setTotalCustomerCredit(totalCustomerCredit);

		if (ipgPaymentOptionDTOList.size() > 0) {
			for (IPGPaymentOptionDTO ipgPaymentOptionDTO : ipgPaymentOptionDTOList) {
				if (ipgPaymentOptionDTO.isSaveCard()) {
					customerLoginRS.setiPGPaymentOptionDTO(ipgPaymentOptionDTO);
					break;
				}
			}
		}

		customerLoginRS.setSuccess(true);
		customerLoginRS.setMessages(null);
		customerLoginRS.setErrors(null);
		customerLoginRS.setWarnings(null);

		return customerLoginRS;
	}

	private void memberLogin(LmsMember lmsMember, ArrayList<String> messages, CustomerLoginRS customerLoginRS,
			CustomerSessionStore customerSession, LoyaltyManagementBD lmsManagementBD) {
		MemberRemoteLoginResponseDTO lmsTokenData;
		if (AppSysParamsUtil.isLMSMemberAuthenticationRequired() && !StringUtils.isEmpty(lmsMember.getMemberExternalId())) {
			try {
				lmsTokenData = lmsManagementBD.memberLogin(lmsMember.getMemberExternalId(), lmsMember.getPassword());
				if (lmsTokenData != null) {
					customerSession.storeMemberAuth(lmsTokenData.getToken());
					customerLoginRS.setAuthToken(lmsTokenData.getToken());
				}

			} catch (ModuleException e) {
				messages.add("Module Exception" + e);
			}
		}
	}

	@ResponseBody
	@RequestMapping(value = "/pageLogin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public PassengerPageLoginRS customerLoginPassengerPage(@RequestBody CustomerLoginRQ customerLoginRQ) {

		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();
		customerValidation.validateLoginRQ(customerLoginRQ);

		Customer customerModel = null;
		boolean loyaltyManagmentEnabled = false;
		LmsMember lmsMember = null;
		double totalCustomerCredit = 0;

		PassengerPageLoginRS passengerPageLoginRS = new PassengerPageLoginRS();
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		CustomerSessionStore customerSession = getCustomerSession();
		ArrayList<String> messages = new ArrayList<>();

		try {

			customerModel = customerDelegate.authenticate(customerLoginRQ.getCustomerID(), customerLoginRQ.getPassword());
			loyaltyManagmentEnabled = AppSysParamsUtil.isLMSEnabled();
			customerSession.storeLoggedInCustomerID(String.valueOf(customerModel.getCustomerId()));
			if (customerModel != null) {

				int intCustomerID = customerModel.getCustomerId();
				Collection<CustomerCredit> customerCreditList = CustomerUtil.getCustomerCreditFromAllCarriers(intCustomerID,
						getTrackInfo());
				totalCustomerCredit = Double.valueOf(CustomerUtil.calculateCustomerTotalCredit(customerCreditList));

				if (loyaltyManagmentEnabled) {
					if (!customerModel.getCustomerLmsDetails().isEmpty()) {
						lmsMember = customerModel.getCustomerLmsDetails().iterator().next();
					}
				}

			}

		} catch (Exception e) {
			messages.add("Login failed. Please try again.");
			passengerPageLoginRS.setSuccess(false);
			passengerPageLoginRS.setMessages(messages);
			passengerPageLoginRS.setErrors(null);
			passengerPageLoginRS.setWarnings(null);
			return passengerPageLoginRS;
		}

		PassengerDetailsAdaptor passengerDetailsAdaptor = new PassengerDetailsAdaptor();
		LoggedInLMSDetailsAdaptor loggedInLMSDetailsAdaptor = new LoggedInLMSDetailsAdaptor();

		PassengerDetails passengerDetails = passengerDetailsAdaptor.adapt(customerModel);
		LoggedInLmsDetails loggedInLmsDetails = loggedInLMSDetailsAdaptor.adapt(lmsMember);

		customerSession.storeFFID(loggedInLmsDetails.getFfid());
		customerSession.storeAvailableCredit(totalCustomerCredit);
		customerSession.storeAvailableLMSPoints(loggedInLmsDetails.getAvailablePoints());

		passengerPageLoginRS.setPassengerDetails(passengerDetails);
		passengerPageLoginRS.setLoggedInLmsDetails(loggedInLmsDetails);
		passengerPageLoginRS.setLoyaltyManagmentEnabled(loyaltyManagmentEnabled);

		passengerPageLoginRS.setSuccess(true);
		passengerPageLoginRS.setMessages(null);
		passengerPageLoginRS.setErrors(null);
		passengerPageLoginRS.setWarnings(null);
		passengerPageLoginRS.setTransactionId(customerLoginRQ.getTransactionId());

		return passengerPageLoginRS;
	}

	@ResponseBody
	@RequestMapping(value = "/creditDetails", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerCreditRS customerCreditDetails() {

		CustomerSessionStore customerSession = getCustomerSession();
		ArrayList<String> messages = new ArrayList<>();

		int intCustomerID = Integer.valueOf(customerSession.getLoggedInCustomerID());
		CustomerCreditRS customerCreditRS = new CustomerCreditRS();
		Collection<CustomerCredit> customerCreditList = null;

		try {

			customerCreditList = CustomerUtil.getCustomerCreditFromAllCarriers(intCustomerID, getTrackInfo());

		} catch (Exception e) {
			messages.add("System error.");
			customerCreditRS.setSuccess(false);
			customerCreditRS.setMessages(messages);
			customerCreditRS.setErrors(null);
			customerCreditRS.setWarnings(null);
			return customerCreditRS;
		}

		customerCreditRS.setCustomerCreditList(new HashSet<CustomerCredit>(customerCreditList));

		customerCreditRS.setSuccess(true);
		customerCreditRS.setMessages(null);
		customerCreditRS.setErrors(null);
		customerCreditRS.setWarnings(null);

		return customerCreditRS;
	}

	@ResponseBody
	@RequestMapping(value = "/profileDetails", method = RequestMethod.GET, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerDetailsRS customerProfileDetails() {

		CustomerSessionStore customerSession = getCustomerSession();
		ArrayList<String> messages = new ArrayList<>();
		CustomerDetailsRS customerDetailsRS = new CustomerDetailsRS();
		try {
			int intCustomerID = Integer.valueOf(customerSession.getLoggedInCustomerID());
			Customer customerModel = null;
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			CustomerDetails customerDetails = null;
			LMSDetails lmsDetails = null;

			customerModel = customerDelegate.getCustomer(intCustomerID);

			LmsMember lmsMember = customerModel.getLMSMemberDetails();

			CustomerResAdaptor customerResAdaptor = new CustomerResAdaptor();
			LMSDetailsResAdaptor lmsDetailsResAdaptor = new LMSDetailsResAdaptor();

			customerDetails = customerResAdaptor.adapt(customerModel);

			if (lmsMember != null) {
				lmsDetails = lmsDetailsResAdaptor.adapt(lmsMember);
			} else {
				lmsDetails = null;
			}

			addPreferredSeatAndMealList(intCustomerID, customerDetails);
			customerDetailsRS.setCustomer(customerDetails);
			customerDetailsRS.setLmsDetails(lmsDetails);

			customerDetailsRS.setSuccess(true);
			customerDetailsRS.setMessages(null);
			customerDetailsRS.setErrors(null);
			customerDetailsRS.setWarnings(null);
		} catch (Exception e) {
			messages.add("System error.");
			customerDetailsRS.setSuccess(false);
			customerDetailsRS.setMessages(messages);
			customerDetailsRS.setErrors(null);
			customerDetailsRS.setWarnings(null);
			return customerDetailsRS;
		}
		return customerDetailsRS;
	}

	private void addPreferredSeatAndMealList(int intCustomerID, CustomerDetails customerDetails) {
		if (AppSysParamsUtil.isSeatMealPreferenceEnabled()) {
			String prefSeatType = CustomerUtil.getCustomerPreferredSeatType(intCustomerID);
			if (prefSeatType != null && !prefSeatType.isEmpty()) {
				customerDetails.setCustomerPreferredSeatType(prefSeatType);
			} else {
				customerDetails.setCustomerPreferredSeatType("-");
			}
			customerDetails.setCutomerPreferredMealList(CustomerUtil.getCustomerPreferredMeals(intCustomerID));
		}
	}

	@ResponseBody
	@RequestMapping(value = "/reservationList", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TravelHistoryRS customerTravelHistory() {

		CustomerSessionStore customerSession = getCustomerSession();

		int intCustomerID = Integer.valueOf(customerSession.getLoggedInCustomerID());
		TravelHistoryRS travelHistoryRS = new TravelHistoryRS();
		List<ReservationDetails> reservationDetailsList = null;
		ArrayList<String> messages = new ArrayList<>();

		try {

			reservationDetailsList = CustomerUtil.getAfterReservationsList(intCustomerID, new Date(), getTrackInfo());
			reservationDetailsList.addAll(CustomerUtil.getEarlyReservationsList(intCustomerID, new Date(), getTrackInfo()));

			// remove duplicates based on pnr
			reservationDetailsList = new ArrayList<ReservationDetails>(
					new LinkedHashSet<ReservationDetails>(reservationDetailsList));

		} catch (Exception e) {

			messages.add("System error.");
			travelHistoryRS.setSuccess(false);
			travelHistoryRS.setMessages(messages);
			travelHistoryRS.setErrors(null);
			travelHistoryRS.setWarnings(null);
			return travelHistoryRS;

		}
		Collections.sort(reservationDetailsList);
		travelHistoryRS.setReservationDetails(reservationDetailsList);

		travelHistoryRS.setSuccess(true);
		travelHistoryRS.setMessages(null);
		travelHistoryRS.setErrors(null);
		travelHistoryRS.setWarnings(null);

		return travelHistoryRS;
	}

	private CustomerSessionStore getCustomerSession() {
		return getSessionStore(CustomerSession.SESSION_KEY);
	}

	@ResponseBody
	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerSaveOrUpdateRS registerCustomer(@RequestBody CustomerSaveOrUpdateRQ customerSaveOrUpdateReq) {

		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();
		customerValidation.validateRegisterUpdateRQ(customerSaveOrUpdateReq);
		LoyaltyCustomerProfile loyaltyCustomerProfile = null;
		if (customerSaveOrUpdateReq.getLoyaltyProfileDetails() != null) {
			CustomerLiteReqAdaptor customerLiteReqAdaptor = new CustomerLiteReqAdaptor();
			LoyaltyCustomerProfile tempLoyaltyProfile = adaptLoyaltyCustomerProfile(
					customerSaveOrUpdateReq.getLoyaltyProfileDetails(), customerLiteReqAdaptor.adapt(customerSaveOrUpdateReq));
			loyaltyCustomerProfile = customerValidation.validateLoyaltyCustomerProfile(tempLoyaltyProfile, false);
		}

		String familyHead = "Y";

		String refferedEmail = "Y";

		boolean optLMS = customerSaveOrUpdateReq.isOptLMS();

		CustomerSaveOrUpdateRS customerSaveOrUpdateResponse = new CustomerSaveOrUpdateRS();

		customerSaveOrUpdateResponse.setTransactionId(customerSaveOrUpdateReq.getTransactionId());
		CustomerSessionStore customerSession = getCustomerSession();
		ArrayList<String> messages = new ArrayList<>();

		CustomerDetails customerDetails = customerSaveOrUpdateReq.getCustomer();
		LMSDetails lmsDetails = customerSaveOrUpdateReq.getLmsDetails();

		CustomerReqAdaptor customerReqAdaptor = new CustomerReqAdaptor();
		CustomerSaveOrUpdateReqAdaptor lmsDetailsAdaptor = new CustomerSaveOrUpdateReqAdaptor();

		Customer customer = customerReqAdaptor.adapt(customerDetails);
		LmsMember lmsMember = null;

		if (optLMS) {
			customer.setIsLmsMember('Y');
			lmsMember = lmsDetailsAdaptor.adapt(customerSaveOrUpdateReq);
		} else {
			customer.setIsLmsMember('N');
		}

		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();

		try {

			customerDelegate.registerCustomer(customer, getTrackInfo().getCarrierCode(), loyaltyCustomerProfile, true);

			int customerId = customerDelegate.getCustomer(customer.getEmailId()).getCustomerId();

			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			if (AppSysParamsUtil.isLMSEnabled()) {
				if (optLMS) {
					if (!LmsCommonUtil.isNonMandAccountAvailable(lmsMember.getHeadOFEmailId(), loyaltyManagementBD)) {
						familyHead = "N";
					}
					if (!LmsCommonUtil.isNonMandAccountAvailable(lmsMember.getRefferedEmail(), loyaltyManagementBD)) {
						refferedEmail = "N";
					}
				}
			}

			if (AppSysParamsUtil.isLMSEnabled()) {
				if (optLMS && familyHead.equals("Y") && refferedEmail.equals("Y")) {
					Locale locale = LocaleContextHolder.getLocale();
					LocaleContextHolder.getLocale();
					lmsMember.setCustomerId(customerId);
					LmsCommonUtil.lmsEnroll(lmsMember, getTrackInfo().getCarrierCode(), loyaltyManagementBD, lmsMemberDelegate,
							locale, true);
					lmsDetails.setEmailStatus("N");
				}
			}

		} catch (ModuleException e) {

			if (e.getExceptionCode().equals("aircustomer.logic.loginid.already.exist")) {
				messages.add("Given Email Already Exists");
			} else {
				messages.add("System error.");
			}

			customerSaveOrUpdateResponse.setSuccess(false);
			customerSaveOrUpdateResponse.setMessages(messages);
			customerSaveOrUpdateResponse.setErrors(null);
			customerSaveOrUpdateResponse.setWarnings(null);
			return customerSaveOrUpdateResponse;
		}

		if (!AppSysParamsUtil.isCustomerRegiterConfirmation() && AppSysParamsUtil.isAutoLoginAfterRegistration()) {
			Customer registeredCustomer = null;
			try {
				registeredCustomer = customerDelegate.getCustomer(customer.getEmailId());
				if (registeredCustomer != null) {
					customerSession.storeLoggedInCustomerID(String.valueOf(registeredCustomer.getCustomerId()));
					if (registeredCustomer.getLMSMemberDetails() != null) {
						customerSession.storeFFID(registeredCustomer.getLMSMemberDetails().getFfid());
					}

					customerSaveOrUpdateResponse.setAutoLogin(true);
				}
			} catch (ModuleException e) {
				e.printStackTrace();
			}
		}

		customerSaveOrUpdateResponse.setSuccess(true);
		customerSaveOrUpdateResponse.setMessages(null);
		customerSaveOrUpdateResponse.setErrors(null);
		customerSaveOrUpdateResponse.setWarnings(null);

		return customerSaveOrUpdateResponse;

	}

	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public CustomerSaveOrUpdateRS updateCustomer(@RequestBody CustomerSaveOrUpdateRQ customerSaveOrUpdateReq) {

		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();
		customerValidation.validateRegisterUpdateRQ(customerSaveOrUpdateReq);

		boolean success = true;
		ArrayList<String> messageTxt = new ArrayList<String>();

		CustomerSaveOrUpdateRS customerSaveOrUpdateResponse = new CustomerSaveOrUpdateRS();
		CustomerReqAdaptor customerReqAdaptor = new CustomerReqAdaptor();

		CustomerDetails customerDetails = customerSaveOrUpdateReq.getCustomer();
		LMSDetails lmsDetails = customerSaveOrUpdateReq.getLmsDetails();

		CustomerSessionStore customerSession = getCustomerSession();

		customerValidation.validateProfileUpdate(customerDetails, Integer.valueOf(customerSession.getLoggedInCustomerID()));

		if (lmsDetails != null) {
			customerValidation.validateLMSDetails(lmsDetails);
		}
		customerDetails.setCustomerID(customerSession.getLoggedInCustomerID());
		int customerId = Integer.valueOf(customerSession.getLoggedInCustomerID());

		Customer customer = customerReqAdaptor.adapt(customerDetails);

		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
		LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();

		try {

			customer.setPassword(
					customerDelegate.getCustomer(Integer.valueOf(customerSession.getLoggedInCustomerID())).getPassword());

			if (AppSysParamsUtil.isLMSEnabled()) {
				if (customer.getLMSMemberDetails() != null && lmsDetails != null) {
					LmsMember lmsMember = lmsMemberDelegate.getLmsMember(customer.getEmailId());
					lmsMember.setResidencyCode(customer.getCountryCode());
					lmsMember.setNationalityCode(customer.getNationalityCode().toString());
					lmsMember.setMobileNumber(customer.getMobile());
					lmsMember.setPhoneNumber(customer.getTelephone());
					lmsMember.setLanguage(lmsDetails.getLanguage());
					lmsMember.setHeadOFEmailId(lmsDetails.getHeadOFEmailId());
					lmsMember.setRefferedEmail(lmsDetails.getRefferedEmail());
					lmsMember.setPassportNum(lmsDetails.getPassportNum());
					lmsMember.setDateOfBirth(lmsDetails.getDateOfBirth());
					lmsMember.setGenderTypeId(getGenderTypeId(customer));

					customer.setCustomerId(Integer.valueOf(customerSession.getLoggedInCustomerID()));
					if (customer.getCustomerId() == 0) {
						Customer tempCustomer = customerDelegate.getCustomer(customer.getEmailId());
						customer.setCustomerId(tempCustomer.getCustomerId());
					}

					if (success) {
						Set<LmsMember> lmsMembers = new HashSet<LmsMember>();
						lmsMembers.add(lmsMember);
						customer.setCustomerLmsDetails(lmsMembers);
						customer.setIsLmsMember('Y');
						loyaltyManagementBD.updateMemberProfile(customer);
					}
				}
			}

			if (success) {
				customerDelegate.saveOrUpdate(customer);
			}

		} catch (Exception e) {
			messageTxt.add("System error.");
			customerSaveOrUpdateResponse.setSuccess(false);
			customerSaveOrUpdateResponse.setMessages(messageTxt);
			customerSaveOrUpdateResponse.setErrors(null);
			customerSaveOrUpdateResponse.setWarnings(null);
			return customerSaveOrUpdateResponse;
		}
		customerSaveOrUpdateResponse.setSuccess(true);
		customerSaveOrUpdateResponse.setMessages(messageTxt);
		customerSaveOrUpdateResponse.setErrors(null);
		customerSaveOrUpdateResponse.setWarnings(null);
		customerSaveOrUpdateResponse.setTransactionId(customerSaveOrUpdateReq.getTransactionId());

		return customerSaveOrUpdateResponse;

	}

	private Integer getGenderTypeId(Customer customer) {

		Integer genderTypeId = null;

		if (!StringUtil.isEmpty(customer.getTitle())) {

			String title = customer.getTitle();

			if (title.equalsIgnoreCase("MR")) {
				genderTypeId = 1;
			} else if (title.equalsIgnoreCase("MS") || title.equalsIgnoreCase("MRS")) {
				genderTypeId = 2;
			} else {
				genderTypeId = 0;
			}
		}
		return genderTypeId;
	}

	@ResponseBody
	@RequestMapping(value = "/updateSeatTypePreference", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public AncillaryPreferenceRS updateSeatTypePreference(@RequestBody AncillaryPreferenceRQ ancillaryPreferenceReq) {
		AncillaryPreferenceRS ancillaryPreferenceRS = new AncillaryPreferenceRS();
		if (AppSysParamsUtil.isSeatMealPreferenceEnabled()) {
			CustomerSessionStore customerSession = getCustomerSession();

			int intCustomerID = Integer.valueOf(customerSession.getLoggedInCustomerID());

			updatePreferredSeatType(ancillaryPreferenceReq.getCustomerPreferredSeatType(), intCustomerID,
					ModuleServiceLocator.getCustomerBD());
		}
		ancillaryPreferenceRS.setSuccess(true);
		return ancillaryPreferenceRS;
	}

	@ResponseBody
	@RequestMapping(value = "/updateMealListPreference", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public AncillaryPreferenceRS updateMealListPreference(@RequestBody AncillaryPreferenceRQ ancillaryPreferenceReq) {
		AncillaryPreferenceRS ancillaryPreferenceRS = new AncillaryPreferenceRS();
		if (AppSysParamsUtil.isSeatMealPreferenceEnabled()) {
			CustomerSessionStore customerSession = getCustomerSession();

			int intCustomerID = Integer.valueOf(customerSession.getLoggedInCustomerID());

			updatePreferredMealList(ancillaryPreferenceReq.getCustomerPreferredMealList(), intCustomerID,
					ModuleServiceLocator.getCustomerBD());
		}
		ancillaryPreferenceRS.setSuccess(true);
		return ancillaryPreferenceRS;
	}

	private void updatePreferredMealList(List<CustomerPreferredMealDTO> customerPreferredMealDTOList, int customerId,
			AirCustomerServiceBD customerDelegate) {
		if (customerPreferredMealDTOList != null && !customerPreferredMealDTOList.isEmpty()) {

			List<CustomerPreferredMeal> customerPreferredMealList = new ArrayList<CustomerPreferredMeal>();

			for (CustomerPreferredMealDTO meal : customerPreferredMealDTOList) {
				CustomerPreferredMeal customerPreferredMeal = new CustomerPreferredMeal();
				CustomerPreferredMealPK customerPreferredMealPK = new CustomerPreferredMealPK(customerId, meal.getMealCode());
				customerPreferredMeal.setId(customerPreferredMealPK);
				customerPreferredMeal.setQuantity(meal.getQuantity());
				customerPreferredMealList.add(customerPreferredMeal);
			}

			customerDelegate.setCustomerPreferredMeals(customerPreferredMealList);
		}
	}

	private void updatePreferredSeatType(String preferredSeatType, int customerId, AirCustomerServiceBD customerDelegate) {
		if (preferredSeatType != null && !preferredSeatType.equals("")) {
			CustomerPreferredSeat customerPreferredSeat = new CustomerPreferredSeat();
			customerPreferredSeat.setCustomerId(customerId);
			customerPreferredSeat.setSeatType(!preferredSeatType.equals("-") ? preferredSeatType : null);
			customerDelegate.setCustomerPreferredSeat(customerPreferredSeat);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/passwordRecovery", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ForgotPasswordRS pLoggedInCustomerasswordRecovery(@RequestBody ForgotPasswordRQ forgotPasswordReq) {

		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();
		customerValidation.validateForgotPasswordRQ(forgotPasswordReq);
		ArrayList<String> messageTxt = new ArrayList<String>();

		ForgotPasswordRS forgotPasswordResponse = new ForgotPasswordRS();
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();

		String secretQuestion = StringUtil.nullConvertToString(forgotPasswordReq.getSecretQuestion());
		String secretAnswer = StringUtil.nullConvertToString(forgotPasswordReq.getSecretAnswer());

		try {
			boolean blnStatus = false;

			blnStatus = customerDelegate.sendPasswordResetLink(forgotPasswordReq.getCustomerID(), secretQuestion, secretAnswer,
					getTrackInfo().getCarrierCode(), AppSysParamsUtil.getSecureServiceAppIBEUrl());
			if (blnStatus) {
				messageTxt.add("Email sent with a link to reset password");
			} else {
				messageTxt.add("Couldn't find an account for given email");
			}

		} catch (ModuleException me) {
			messageTxt.add("System error.");
			forgotPasswordResponse.setSuccess(false);
			forgotPasswordResponse.setMessages(messageTxt);
			return forgotPasswordResponse;
		}

		forgotPasswordResponse.setSuccess(true);
		forgotPasswordResponse.setMessages(messageTxt);

		return forgotPasswordResponse;

	}

	@ResponseBody
	@RequestMapping(value = "/lmsValidation", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public LMSValidationRS lmsValidation(@RequestBody LMSValidationRQ lmsValidationReq) throws ModuleException {

		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();

		boolean isLoggedCustomer = getCustomerSession().getLoggedInCustomerID() != null;
		customerValidation.validateLMSValidationRQ(lmsValidationReq, isLoggedCustomer);

		LMSValidationRS lmsValidationResponse = new LMSValidationRS();

		if (AppSysParamsUtil.isLMSEnabled()) {

			if (lmsValidationReq.getRefferedEmailId() != null && !lmsValidationReq.getRefferedEmailId().equals("")) {
				customerValidation.validateExistingLMSAccount(lmsValidationReq.getRefferedEmailId(), true);
				lmsValidationResponse.setExistingRefferdEmail(true);
			}

			if (lmsValidationReq.getHeadOfEmailId() != null && !lmsValidationReq.getHeadOfEmailId().equals("")) {
				customerValidation.validateExistingLMSAccount(lmsValidationReq.getHeadOfEmailId(), false);
				lmsValidationResponse.setExistingFamilyHeadEmail(true);
			}

			if (lmsValidationReq.getEmailId() != null && !lmsValidationReq.getEmailId().equals("")) {

				if (lmsValidationReq.isRegistration()) {
					customerValidation.validateNewRequestedLMSAccount(lmsValidationReq.getEmailId());
				}

				customerValidation.validateExistingName(lmsValidationReq);
				lmsValidationResponse.setLmsNameMismatch(false);

			}
		}

		lmsValidationResponse.setTransactionId(lmsValidationReq.getTransactionId());
		lmsValidationResponse.setSuccess(true);

		return lmsValidationResponse;

	}

	@ResponseBody
	@RequestMapping(value = "/lmsRegister", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public LMSRegisterRS lmsRegister(@RequestBody LMSRegisterRQ lmsRegisterReq) throws NumberFormatException, ModuleException {

		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();

		lmsRegisterReq.setCustomerID(getCustomerSession().getLoggedInCustomerID());
		Customer customer = ModuleServiceLocator.getCustomerBD()
				.getCustomer(Integer.valueOf(getCustomerSession().getLoggedInCustomerID()));
		lmsRegisterReq.getLmsDetails().setFfid(customer.getEmailId());

		customerValidation.validateLMSRegisterRQ(lmsRegisterReq);

		LMSDetailsReqAdaptor lmsDetailsReqAdaptor = new LMSDetailsReqAdaptor();
		LmsMember lmsMember = lmsDetailsReqAdaptor.adapt(lmsRegisterReq);
		Set<LmsMember> lmsMemberSet = new HashSet<>();
		lmsMemberSet.add(lmsMember);
		customer.setCustomerLmsDetails(lmsMemberSet);
		LoyaltyCustomerProfile loyaltyCustomerProfile = null;
		if (lmsRegisterReq.getLoyaltyProfileDetails() != null) {
			loyaltyCustomerProfile = adaptLoyaltyCustomerProfile(lmsRegisterReq.getLoyaltyProfileDetails(), customer);
			loyaltyCustomerProfile = customerValidation.validateLoyaltyCustomerProfile(loyaltyCustomerProfile, true);
			loyaltyCustomerProfile.setCustomerId(Integer.valueOf(getCustomerSession().getLoggedInCustomerID()));
		}

		LMSRegisterRS lmsRegisterResponse = new LMSRegisterRS();
		ArrayList<String> messages = new ArrayList<>();

		LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
		LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
		Locale locale = new Locale("en");

		if (AppSysParamsUtil.isLMSEnabled()) {
			try {

				LmsCommonUtil.lmsEnroll(lmsMember, getTrackInfo().getCarrierCode(), loyaltyManagementBD, lmsMemberDelegate,
						locale, true);

			} catch (ModuleException me) {
				messages.add("System error.");
				lmsRegisterResponse.setSuccess(false);
				lmsRegisterResponse.setMessages(messages);
				lmsRegisterResponse.setErrors(null);
				lmsRegisterResponse.setWarnings(null);
				return lmsRegisterResponse;
			}
		}

		if (loyaltyCustomerProfile != null) {
			customerService.saveOrUpdateLoyaltyCustomerProfile(loyaltyCustomerProfile);
		}

		lmsRegisterResponse.setSuccess(true);
		lmsRegisterResponse.setMessages(null);
		lmsRegisterResponse.setErrors(null);
		lmsRegisterResponse.setWarnings(null);

		return lmsRegisterResponse;

	}

	@ResponseBody
	@RequestMapping(value = "/signOut", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public TransactionalBaseRS signOut(@RequestBody TransactionalBaseRQ baseRequest) {

		TransactionalBaseRS baseResponse = new TransactionalBaseRS();
		CustomerSessionStore customerSession = getCustomerSession();
		customerSession.clearSession();

		baseResponse.setTransactionId(baseRequest.getTransactionId());
		baseResponse.setSuccess(true);

		return baseResponse;
	}

	@ResponseBody
	@RequestMapping(value = "/crossPortalLogin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public LMSCrossPortalLoginRS crossPortalLogin(@RequestBody LMSCrossPortalLoginRQ lmsCrossPortalLoginRQ)
			throws ModuleException {

		CustomerValidationService validationService = new CustomerValidationServiceImpl();

		CustomerSessionStore customerSession = getCustomerSession();

		String ffid = customerSession.getFFID();

		if (ffid != null) {
			validationService.validateLMSCrossPortalLoginReq(lmsCrossPortalLoginRQ, ffid);

			LMSCrossPortalLoginRS lmsCrossPortalLoginRS = new LMSCrossPortalLoginRS();
			lmsCrossPortalLoginRS.setTransactionId(lmsCrossPortalLoginRQ.getTransactionId());
			lmsCrossPortalLoginRS.setSuccess(true);

			LmsMemberServiceBD lmsMemberServiceBD = ModuleServiceLocator.getLmsMemberServiceBD();
			String crossProtalLoginUrl = lmsMemberServiceBD.getCrossProtalLoginUrl(ffid, lmsCrossPortalLoginRQ.getUrlRef());

			if (crossProtalLoginUrl != null && !crossProtalLoginUrl.isEmpty()) {
				lmsCrossPortalLoginRS.setMessages(null);
				lmsCrossPortalLoginRS.setUrl(crossProtalLoginUrl);
			} else {
				throw new ValidationException(ValidationException.Code.LMS_CROSS_PORTAL_LOGIN_ERROR);
			}

			return lmsCrossPortalLoginRS;
		} else {
			throw new ValidationException(ValidationException.Code.FFID_INVALID);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/sendLMSConfEmail", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TransactionalBaseRS sendlmsConfEmail(@RequestBody TransactionalBaseRQ baseRQ) {

		TransactionalBaseRS baseRS = new TransactionalBaseRS();
		baseRS.setSuccess(true);
		baseRS.setTransactionId(baseRQ.getTransactionId());
		CustomerSessionStore customerSession = getCustomerSession();
		try {
			Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(customerSession.getFFID());
			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			if (customer.getLMSMemberDetails() != null) {
				Locale locale = new Locale(customer.getLMSMemberDetails().getLanguage());
				LmsMember lmsMember = customer.getLMSMemberDetails();
				String validationString = lmsMemberDelegate.getVerificationString(lmsMember.getFfid());
				LmsCommonUtil.resendLmsConfEmail(lmsMember, AppSysParamsUtil.getCarrierCode(), validationString,
						lmsMemberDelegate, loyaltyManagementBD, locale, true);
			}
		} catch (ModuleException me) {
			baseRS.setSuccess(false);
			log.error("CustomerController.sendlmsConfEmail==>", me);
			return baseRS;
		}
		return baseRS;
	}

	@ResponseBody
	@RequestMapping(value = "/getLMSPoints", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TransactionalBaseRS getLMSPoints(@RequestBody TransactionalBaseRQ baseRQ) {
		LMSPointBalanceRS lmsPointBalanceRS = new LMSPointBalanceRS();
		lmsPointBalanceRS.setTransactionId(baseRQ.getTransactionId());
		lmsPointBalanceRS.setSuccess(true);
		String ffid = getCustomerSession().getFFID();
		String memberExternalId = getCustomerSession().getLMSRemoteId();
		if (ffid != null) {
			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			LmsMember lmsMember;
			try {
				lmsMember = lmsMemberDelegate.getLmsMember(ffid);
			} catch (ModuleException me) {
				lmsPointBalanceRS.setSuccess(false);
				log.error("CustomerController.getLMSPonts ==> getLmsMember", me);
				return lmsPointBalanceRS;
			}
			if (lmsMember.getEmailConfirmed() == 'Y') {
				LoyaltyPointDetailsDTO loyaltyPointDetailsDTO;
				try {
					loyaltyPointDetailsDTO = loyaltyManagementBD.getLoyaltyMemberPointBalances(ffid, memberExternalId);
					lmsPointBalanceRS.setPointBalance(loyaltyPointDetailsDTO.getMemberPointsAvailable());
				} catch (ModuleException me) {
					lmsPointBalanceRS.setSuccess(false);
					log.error("CustomerController.getLMSPonts ==> getLoyaltyMemberPointBalances", me);
					return lmsPointBalanceRS;
				}
			} else {
				lmsPointBalanceRS.setPointBalance(null);
			}
		} else {
			lmsPointBalanceRS.setPointBalance(null);
		}
		return lmsPointBalanceRS;
	}

	@ResponseBody
	@RequestMapping(value = "/customerLoggedInCheck", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerLoginCheckRS getLMSPonts(@RequestBody CustomerLoginCheckRQ customerLoginCheckRQ) throws ModuleException {
		String customerID = getCustomerSession().getLoggedInCustomerID();
		CustomerLoginCheckRS customerLoginCheckRS = new CustomerLoginCheckRS();
		customerLoginCheckRS.setTransactionId(customerLoginCheckRQ.getTransactionId());
		customerLoginCheckRS.setSuccess(true);
		if (customerID != null && !Integer.valueOf(customerID).equals(new Integer(0)) && !"".equals(customerID)) {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			Customer customer = customerDelegate.getCustomer(Integer.valueOf(customerID));
			customerLoginCheckRS.setCustomerLoggedIn(customer.getEmailId().equalsIgnoreCase(customerLoginCheckRQ.getLoginId()));
		}
		return customerLoginCheckRS;
	}

	@ResponseBody
	@RequestMapping(value = "/confirmLMSMember", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public LMSConfirmationRS confirmLMSMember(@RequestBody LMSConfirmationRQ lmsConfirmationRQ) throws ModuleException {
		return customerService.confirmLMSMember(lmsConfirmationRQ, getTrackInfo());
	}

	@ResponseBody
	@RequestMapping(value = "/confirmCustomer", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ConfirmCustomerRS confirmCustomer(@RequestBody ConfirmCustomerRQ confirmCustomerRQ) throws ModuleException {
		return customerService.confirmCustomer(confirmCustomerRQ, getTrackInfo());
	}

	@ResponseBody
	@RequestMapping(value = "/loyaltyRegister", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TransactionalBaseRS confirmLMSMember(@RequestBody LoyaltyProfileDetailsRQ loyaltyProfileDetails)
			throws ModuleException {
		TransactionalBaseRS baseRS = new TransactionalBaseRS();
		baseRS.setSuccess(registerLoyaltyCustomer(loyaltyProfileDetails));
		baseRS.setTransactionId(loyaltyProfileDetails.getTransactionId());
		return baseRS;
	}

	@ResponseBody
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TransactionalBaseRS updatePassword(@RequestBody PasswordChangeRQ passwordUpdateRQ) throws ModuleException {

		TransactionalBaseRS baseRS = new TransactionalBaseRS();
		CustomerValidationService customerValidation = new CustomerValidationServiceImpl();
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		ArrayList<String> messageTxt = new ArrayList<String>();
		Customer customer;

		if (!StringUtil.isEmpty(passwordUpdateRQ.getPasswordResetToken())) {
			customerValidation.validatePasswordChangeRQ(passwordUpdateRQ, null);
			CryptoServiceBD cryptoServiceBD = ModuleServiceLocator.getCryptoServiceBD();
			String decryptedData = cryptoServiceBD.decrypt(passwordUpdateRQ.getPasswordResetToken());
			String emailAddress = decryptedData.split("email=")[1];
			customer = customerDelegate.getCustomer(emailAddress);
		} else {
			customerValidation.validatePasswordChangeRQ(passwordUpdateRQ,
					Integer.valueOf(getCustomerSession().getLoggedInCustomerID()));
			customer = customerDelegate.getCustomer(Integer.valueOf(getCustomerSession().getLoggedInCustomerID()));
		}

		try {
			customer.setPassword(passwordUpdateRQ.getNewPassword());
			customerDelegate.saveOrUpdate(customer);
		} catch (Exception exception) {
			messageTxt.add("System error.");
			baseRS.setSuccess(false);
			baseRS.setMessages(messageTxt);
			return baseRS;
		}

		if (!StringUtil.isEmpty(passwordUpdateRQ.getPasswordResetToken())) {
			messageTxt.add("Password reset successful");
		} else {
			messageTxt.add("Password change successful");
		}
		baseRS.setSuccess(true);
		baseRS.setMessages(messageTxt);
		baseRS.setTransactionId(passwordUpdateRQ.getTransactionId());

		return baseRS;
	}

	// Retrive the email address from encrypted data
	@ResponseBody
	@RequestMapping(value = "/getEmailFromToken", produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerPasswordResetRS getEmailFromToken(@RequestParam("token") String token) throws ModuleException {

		CryptoServiceBD cryptoServiceBD = ModuleServiceLocator.getCryptoServiceBD();
		CustomerPasswordResetRS customerPasswordResetRS = new CustomerPasswordResetRS();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String decryptedData = cryptoServiceBD.decrypt(token);
		Date urlDate;
		try {
			urlDate = simpleDateFormat.parse(decryptedData.split("email=")[0].split("date=")[1]);
		} catch (ParseException e) {
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		String emailAddress = decryptedData.split("email=")[1];
		int timeoutHours = AppSysParamsUtil.getIBEPasswordResetTimeOut();
		int hours = (int) (new Date().getTime() - urlDate.getTime()) / DateUtils.MILLIS_IN_HOUR;

		if (hours <= timeoutHours) {
			customerPasswordResetRS.setSuccess(true);
			customerPasswordResetRS.setEmail(emailAddress);
			return customerPasswordResetRS;
		} else {
			throw new ValidationException(ValidationException.Code.PASSWORD_RESET_LINK_EXPIRED);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/getFamilyMembersDetails", method = RequestMethod.GET, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public FamilyMemberDetailsRS getFamilyMembersDetails() {

		FamilyMemberDetailsRS familyMemberDetailsRS = new FamilyMemberDetailsRS();

		CustomerSessionStore customerSession = getCustomerSession();
		if (customerSession != null && customerSession.getLoggedInCustomerID() != null) {
			int intCustomerID = Integer.valueOf(customerSession.getLoggedInCustomerID());

			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();

			try {
				setFamilyMemberDTOList(familyMemberDetailsRS, intCustomerID, customerDelegate);

			} catch (ModuleException moduleException) {
				ArrayList<String> messages = new ArrayList<>();
				messages.add("System error in Retreiving Family Members Details.");
				familyMemberDetailsRS.setSuccess(false);
				familyMemberDetailsRS.setMessages(messages);
				return familyMemberDetailsRS;
			}
		}

		familyMemberDetailsRS.setSuccess(true);
		return familyMemberDetailsRS;
	}

	@ResponseBody
	@RequestMapping(value = "/saveOrUpdateFamilyMember", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public FamilyMemberDetailsRS saveOrUpdateFamilyMember(@RequestBody FamilyMemberDetailsRQ familyMemberDetailsReq) {
		FamilyMemberDetailsRS familyMemberDetailsRS = new FamilyMemberDetailsRS();
		if (AppSysParamsUtil.isAddFamilyMemberEnabled()) {
			CustomerSessionStore customerSession = getCustomerSession();
			ArrayList<String> messages = new ArrayList<>();

			int intCustomerID = Integer.valueOf(customerSession.getLoggedInCustomerID());
			RPHGenerator familyMemberIDHolder = getCustomerSession().getFamilyMemberIDHolder();
			FamilyMemberRQAdaptor familyMemberRQAdaptor = new FamilyMemberRQAdaptor(familyMemberIDHolder);
			FamilyMember familyMember = familyMemberRQAdaptor.adapt(familyMemberDetailsReq.getFamilyMemberDTOList().get(0));
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			FamilyMember oldFamilyMember;
			try {
				if (familyMember != null) {
					if (familyMember.getFamilyMemberId() != null) {
						oldFamilyMember = customerDelegate.getFamilyMember(familyMember.getFamilyMemberId());
						oldFamilyMember.setTitle(familyMember.getTitle());
						oldFamilyMember.setFirstName(familyMember.getFirstName());
						oldFamilyMember.setLastName(familyMember.getLastName());
						oldFamilyMember.setRelationshipId(familyMember.getRelationshipId());
						oldFamilyMember.setNationalityCode(familyMember.getNationalityCode());
						oldFamilyMember.setDateOfBirth(familyMember.getDateOfBirth());
						customerDelegate.saveOrUpdate(oldFamilyMember);
					} else {
						familyMember.setCustomerId(intCustomerID);
						customerDelegate.saveOrUpdate(familyMember);
					}
					setFamilyMemberDTOList(familyMemberDetailsRS, intCustomerID, customerDelegate);
				}
			} catch (ModuleException moduleException) {
				messages.add("Module Exception is SaveOrUpdate family Member");
				familyMemberDetailsRS.setSuccess(false);
				familyMemberDetailsRS.setMessages(messages);
				return familyMemberDetailsRS;
			}
			familyMemberDetailsRS.setSuccess(true);
			familyMemberDetailsRS.setMessages(messages);
		}
		return familyMemberDetailsRS;
	}

	private void setFamilyMemberDTOList(FamilyMemberDetailsRS familyMemberDetailsRS, int intCustomerID,
			AirCustomerServiceBD customerDelegate) throws ModuleException {
		List<FamilyMember> familyMemberListFromDB = customerDelegate.getFamilyMembers(intCustomerID);
		RPHGenerator familyMemberIDHolder = getCustomerSession().getFamilyMemberIDHolder();
		FamilyMemberRSAdaptor familyMemberRSAdaptor = new FamilyMemberRSAdaptor(familyMemberIDHolder);
		for (FamilyMember updateFamilyMember : familyMemberListFromDB) {
			familyMemberDetailsRS.getFamilyMemberDTOList().add(familyMemberRSAdaptor.adapt(updateFamilyMember));
		}
	}

	@ResponseBody
	@RequestMapping(value = "/removeFamilyMember/{familyMemberId}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseRS removeFamilyMember(@PathVariable String familyMemberId) {

		BaseRS removeFamilyMemberRS = new BaseRS();
		ArrayList<String> messages = new ArrayList<>();
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		FamilyMember familyMember = null;
		try {
			if (familyMemberId != null) {
				RPHGenerator familyMemberIDHolder = getCustomerSession().getFamilyMemberIDHolder();
				familyMember = customerDelegate
						.getFamilyMember(Integer.valueOf(familyMemberIDHolder.getUniqueIdentifier(familyMemberId)));
				customerDelegate.removeFamilyMember(familyMember);
				removeFamilyMemberRS.setSuccess(true);
				removeFamilyMemberRS.setMessages(messages);
			}
		} catch (ModuleException moduleException) {
			messages.add("Module Exception in Removing Family Member.");
			removeFamilyMemberRS.setSuccess(false);
			removeFamilyMemberRS.setMessages(messages);
			return removeFamilyMemberRS;
		}
		return removeFamilyMemberRS;

	}

	private boolean registerLoyaltyCustomer(LoyaltyProfileDetailsRQ loyaltyProfileDetails) throws ModuleException {
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		Customer customer = customerDelegate.getCustomer(Integer.valueOf(getCustomerSession().getLoggedInCustomerID()));
		LoyaltyCustomerProfile loyaltyCustomerProfile = adaptLoyaltyCustomerProfile(loyaltyProfileDetails, customer);
		CustomerValidationService customerValidationService = new CustomerValidationServiceImpl();
		loyaltyCustomerProfile = customerValidationService.validateLoyaltyCustomerProfile(loyaltyCustomerProfile, true);
		loyaltyCustomerProfile.setCustomerId(customer.getCustomerId());
		return customerService.registerLoyaltyCustomer(loyaltyCustomerProfile);
	}

	private LoyaltyCustomerProfile adaptLoyaltyCustomerProfile(LoyaltyProfileDetailsRQ loyaltyProfileDetails, Customer customer) {
		String customerID = null;
		LoyaltyProfileAdaptor loyaltyProfileAdaptor = null;
		try {
			Customer customerModel = null;
			if (customer != null) {
				customerModel = customer;
			} else if (getCustomerSession().getLoggedInCustomerID() != null) {
				customerID = getCustomerSession().getLoggedInCustomerID();
				customerModel = ModuleServiceLocator.getCustomerBD().getCustomer(Integer.valueOf(customerID));
			}
			loyaltyProfileAdaptor = new LoyaltyProfileAdaptor(customerModel);
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		return loyaltyProfileAdaptor.adapt(loyaltyProfileDetails);
	}

	private CustomerLoginRS tokenBasedCustomerloginResponse(CustomerLoginRQ customerLoginReq) {
		Customer customerModel = null;
		CustomerLoginRS customerLoginRS = new CustomerLoginRS();
		boolean loyaltyManagmentEnabled = false;
		double totalCustomerCredit = 0;
		LmsMember lmsMember = null;
		String nationalityName = null;
		Collection<ReservationDetails> reservationDetailsList = null;
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		ArrayList<String> messages = new ArrayList<>();
		CustomerSessionStore customerSession = getCustomerSession();
		customerLoginRS.setTransactionId(customerLoginReq.getTransactionId());
		try {
			Integer customerId = customerDelegate.loadCustomerSession(customerLoginReq.getUserToken()).getCustomerId();
			if (customerId != null) {
				customerModel = ModuleServiceLocator.getCustomerBD().getCustomer(Integer.valueOf(customerId));
				if (customerModel != null) {
					customerSession.storeLoggedInCustomerID(String.valueOf(customerModel.getCustomerId()));
					int intCustomerID = customerModel.getCustomerId();
					Collection<CustomerCredit> customerCreditList = CustomerUtil.getCustomerCreditFromAllCarriers(intCustomerID,
							getTrackInfo());
					totalCustomerCredit = Double.valueOf(CustomerUtil.calculateCustomerTotalCredit(customerCreditList));

					if (customerModel.getNationalityCode() != null) {
						nationalityName = ModuleServiceLocator.getCommoMasterBD()
								.getNationality(customerModel.getNationalityCode()).getDescription();

					}
					customerLoginRS.setHasMashreqLoyalty(customerService.isLoyaltyCustomer(customerModel.getCustomerId()));
					if (loyaltyManagmentEnabled) {
						if (!customerModel.getCustomerLmsDetails().isEmpty()) {
							lmsMember = customerModel.getCustomerLmsDetails().iterator().next();
						}
					}

					LoggedInLMSDetailsAdaptor loggedInLMSDetailsAdaptor = new LoggedInLMSDetailsAdaptor();
					CustomerDetailsAdaptor customerDetailsAdaptor = new CustomerDetailsAdaptor();
					customerSession.storeAvailableCredit(totalCustomerCredit);
					if (lmsMember != null) {
						LoggedInLmsDetails loggedInLmsDetails = loggedInLMSDetailsAdaptor.adapt(lmsMember);
						customerSession.storeAvailableLMSPoints(loggedInLmsDetails.getAvailablePoints());
						customerSession.storeFFID(lmsMember.getFfid());
						customerLoginRS.setLoggedInLmsDetails(loggedInLmsDetails);
					}

					LoggedInCustomerDetails loggedInCustomerDetails = customerDetailsAdaptor.adapt(customerModel);
					loggedInCustomerDetails.setNationalityName(nationalityName);
					customerLoginRS.setLoggedInCustomerDetails(loggedInCustomerDetails);
					customerLoginRS.setTotalCustomerCredit(totalCustomerCredit);
					customerLoginRS.setLoyaltyManagmentEnabled(loyaltyManagmentEnabled);
					reservationDetailsList = CustomerUtil.getAfterReservationsList(customerModel.getCustomerId(), new Date(),
							getTrackInfo());
					customerLoginRS.setReservationList(new HashSet<ReservationDetails>(reservationDetailsList));
					customerLoginRS.setSuccess(true);
				}
			} else {
				messages.add(errorMessageSource.getMessage("ibe.customer.login.token.notfound", null,
						LocaleContextHolder.getLocale()));
				customerLoginRS.setSuccess(false);
				customerLoginRS.setMessages(messages);
			}
		} catch (Exception e) {
			messages.add(errorMessageSource.getMessage("system.error", null, LocaleContextHolder.getLocale()));
			customerLoginRS.setSuccess(false);
			customerLoginRS.setMessages(messages);
			return customerLoginRS;
		}
		return customerLoginRS;
	}

	@ResponseBody
	@RequestMapping(value = "alias/customerAliasList", method = RequestMethod.GET, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerAliasRS getCustomerAliasList() throws ModuleException {
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		CustomerAliasRS customerAliasRS = new CustomerAliasRS();
		boolean isLoggedCustomer = getCustomerSession().getLoggedInCustomerID() != null;
		if (isLoggedCustomer) {
			long customerId = Long.parseLong(getCustomerSession().getLoggedInCustomerID());
			List<CustomerAlias> customerAliasList = customerDelegate.getCustomerAliasList(customerId);
			if (customerAliasList.size() > 0) {
				customerAliasRS.setCustomerAliasList(customerAliasList);
				customerAliasRS.setSuccess(true);
			} else {
				customerAliasRS.setMessage(CustomAliasConstants.NO_CARD_STORED);
				customerAliasRS.setSuccess(false);
			}
		} else {
			customerAliasRS.setSuccess(false);
		}
		return customerAliasRS;

	}

	@ResponseBody
	@RequestMapping(value = "alias/deleteAlias", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CustomerAliasRS deleteCustomerAlias(@RequestBody CustomerAliasRQ customerAliasRQ) throws ModuleException {
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		CustomerAliasRS customerAliasRS = new CustomerAliasRS();
		boolean isLoggedCustomer = getCustomerSession().getLoggedInCustomerID() != null;
		if (isLoggedCustomer) {
			long customerId = Long.parseLong(getCustomerSession().getLoggedInCustomerID());
			CustomerAlias customerAlias = customerDelegate.getCustomerAliasByCustomerAliasId(customerAliasRQ.getCustomerAliasId(),
					customerId);
			customerAlias.setStatus(CustomAliasConstants.ALIAS_INACTIVE);
			try {
				customerDelegate.saveOrUpdate(customerAlias);
				customerAliasRS.setSuccess(true);
				customerAliasRS.setMessage(CustomAliasConstants.SUCCESSFULLY_DELETED_THE_STORED_CARD);
			} catch (ModuleException e) {
				customerAliasRS.setSuccess(false);
				customerAliasRS.setMessage(CustomAliasConstants.UNABLE_TO_DELETE_THE_STORED_CARD);
				log.error("CustomerController.deleteCustomerAlias-->", e);
			}
		} else {
			customerAliasRS.setSuccess(false);
		}

		return customerAliasRS;
	}

	public void setErrorMessageSource(MessageSource errorMessageSource) {
		this.errorMessageSource = errorMessageSource;
	}

}
