package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.PaxModificationParams;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;

public class PaxModificationParamsAdaptor implements Adaptor<LCCClientReservationPax, PaxModificationParams> {

	private Integer nameChangeCount;
	private List<String> interlineModificationParams;
	private boolean isGroupPnr;
	private boolean isUnFlownReservation;
	private boolean isCutOverTimeExceeded;

	public PaxModificationParamsAdaptor(Integer nameChangeCount, boolean isCutOverTimeExceeded, boolean isUnFlownReservation,
			List<String> interlineModificationParams, boolean isGroupPnr) {
		this.nameChangeCount = nameChangeCount;
		this.interlineModificationParams = interlineModificationParams;
		this.isUnFlownReservation = isUnFlownReservation;
		this.isCutOverTimeExceeded = isCutOverTimeExceeded;
		this.isGroupPnr = isGroupPnr;
	}

	@Override
	public PaxModificationParams adapt(LCCClientReservationPax passenger) {
		PaxModificationParams paxModificationParams = new PaxModificationParams();

		paxModificationParams.setPaxSequence(passenger.getPaxSequence());
		paxModificationParams.setNameEditable(isNameEditable());
		if (AppSysParamsUtil.isLMSEnabled()) {
			paxModificationParams.setFfidEditable(!isCutOverTimeExceeded);
		}
		/*
		 * This parameter is added for android/ios apps to enable ffid in namechange flow in future. TODO add backend
		 * validation for change ffid in namechange flow.
		 */
		paxModificationParams.setNameChangeFfid(false);
		return paxModificationParams;
	}

	private boolean isNameEditable() {
		boolean nameEditable = false;
		if (isGroupPnr) {
			// Name change count validation consider @ aaseervices when loading
			if (isUnFlownReservation) {
				nameEditable = interlineModificationParams.contains(ModifcationParamTypes.FARE_RULE_NCC_ENABLED);
			} else {
				nameEditable = interlineModificationParams.contains(ModifcationParamTypes.FARE_RULE_NCC_FOR_FLOWN_ENABLED);
			}
		} else {
			nameEditable = nameChangeCount < AppSysParamsUtil.getNameChangeMaxCount(ApplicationEngine.IBE);
			if (isUnFlownReservation) {
				nameEditable = nameEditable && AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.IBE);
			} else {
				nameEditable = nameEditable && AppSysParamsUtil.isEnableFareRuleLevelNCCForFlown(ApplicationEngine.IBE);
			}
		}
		return nameEditable;
	}
}
