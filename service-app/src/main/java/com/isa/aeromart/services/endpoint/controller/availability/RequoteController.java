package com.isa.aeromart.services.endpoint.controller.availability;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.InterlineFareSegChargeAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.FareQuoteRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationInfoAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.FareReqouteAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.PassengerInfoSummaryAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.RequoteSessionONDListAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.dto.availability.FareQuoteRS;
import com.isa.aeromart.services.endpoint.dto.availability.RequoteRQ;
import com.isa.aeromart.services.endpoint.dto.common.SpecificFlight;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccResSegmentInfo;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionExternalChargeSummary;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.BalanceQueryRequiredData;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AvailabilityRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.service.ValidationService;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

@Controller
@RequestMapping("modification")
public class RequoteController extends StatefulController {

	@Autowired
	public ValidationService validationService;
	@Autowired
	private BookingService bookingService;

	@ResponseBody
	@RequestMapping(value = "/requote", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			FareQuoteRS selectedFlightFareSearch(@Validated @RequestBody RequoteRQ requoteRQ) throws ModuleException {
		LCCClientReservation reservation = loadProxyReservation(requoteRQ.getModificationInfo().getPnr(), requoteRQ.isGroupPnr());
		Map<String, List<LccResSegmentInfo>> ondWiseResSegMap = ModificationUtils.getOndWiseSegmentMap(reservation.getSegments());
		LccReservationInfo lightReservation = ModificationUtils.getLccReservationInfo(reservation);
		validationService.validateRequoteRQ(requoteRQ, lightReservation.getLccSegmentInfos(),
				getRPHGenerator(requoteRQ.getTransactionId()), ondWiseResSegMap, reservation);
		FlightPriceRQ flightPriceRQ = createFlightPriceRQ(requoteRQ, lightReservation, ondWiseResSegMap,
				requoteRQ.getTransactionId());
		validationService.validateRequoteRQForModification(lightReservation.getLccSegmentInfos(), flightPriceRQ, reservation);
		FlightPriceRS flightPriceRS = fareRequote(flightPriceRQ);
		validationService.validateRequoteRS(flightPriceRS, requoteRQ.isModifySegment());
		String transactionId = initiateTransaction(requoteRQ.getTransactionId(), flightPriceRS.getTransactionIdentifier(),
				requoteRQ.isCancelSegment());
		FareQuoteRS response = getFareRequoteResponse(flightPriceRQ, flightPriceRS, transactionId);
		storeRequoteTransaction(requoteRQ, flightPriceRQ, flightPriceRS, reservation, transactionId);
		return response;
	}

	private FlightPriceRQ createFlightPriceRQ(RequoteRQ requoteRequest, LccReservationInfo reservation,
			Map<String, List<LccResSegmentInfo>> ondWiseResSegMap, String transactionId) throws ModuleException {
		String system = null;
		if (transactionId != null) {
			system = getRequoteSessionStore(transactionId).getPreferences().getSelectedSystem();
		}
		FareReqouteAdaptor adaptor = new FareReqouteAdaptor(requoteRequest, ondWiseResSegMap, getRPHGenerator(transactionId),
				getTrackInfo(), transactionId, SYSTEM.getEnum(system));
		FlightPriceRQ flightPriceRQ = adaptor.adapt(reservation);
		return flightPriceRQ;
	}

	private RPHGenerator getRPHGenerator(String transactionId) {
		if (transactionId != null) {
			AvailabilityRequoteSessionStore sessionStore = getTrasactionStore(transactionId);
			return sessionStore.getRPHGenerator();
		}
		return null;
	}

	private LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR) throws ModuleException {
		return bookingService.loadProxyReservation(pnr, isGroupPNR, getTrackInfo(), false, false, null, getTrackInfo()
				.getCarrierCode(), false);
	}

	private FlightPriceRS fareRequote(FlightPriceRQ flightPriceRQ) throws ModuleException {
		return ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ, getTrackInfo());
	}

	private FareQuoteRS getFareRequoteResponse(FlightPriceRQ flightPriceRQ, FlightPriceRS flightPriceRS, String transactionId) {
		PassengerInfoSummaryAdaptor pISAdaptor = new PassengerInfoSummaryAdaptor();
		FareQuoteRSAdaptor fQRSadaptor = new FareQuoteRSAdaptor(pISAdaptor.adapt(flightPriceRQ.getTravelerInfoSummary()),
				getTrackInfo(), flightPriceRQ.getAvailPreferences().getSearchSystem(), getRPHGenerator(transactionId),
				flightPriceRQ.getAvailPreferences().getOndFlexiSelected(), null, transactionId, true);

		return fQRSadaptor.adapt(flightPriceRS);
	}

	private void storeRequoteTransaction(RequoteRQ requoteRequest, FlightPriceRQ flightPriceRQ, FlightPriceRS flightPriceRS,
			LCCClientReservation reservation, String transactionId) {
		RequoteSessionStore sessionStore = getRequoteSessionStore(transactionId);
		FareInfo fareInfo = sessionStore.getFareInfo();

		FareSegChargeTO fareSegChargeTO = flightPriceRS.getSelectedPriceFlightInfo().getFareSegChargeTO();

		InterlineFareSegChargeAdaptor fareSegAdaptor = new InterlineFareSegChargeAdaptor(flightPriceRQ);
		fareSegChargeTO = fareSegAdaptor.adapt(flightPriceRS);

		fareInfo.setFareSegmentChargeTO(fareSegChargeTO);
		fareInfo.setLastFareQuotedDate(flightPriceRS.getSelectedPriceFlightInfo().getLastFareQuotedDate());
		fareInfo.setFqWithinValidity(flightPriceRS.getSelectedPriceFlightInfo().isFQWithinValidity());
		fareInfo.setOndSelectedFlexi(getOndFlexiSelection(flightPriceRS.getSelectedPriceFlightInfo()));
		fareInfo.setPerPaxPriceInfo(flightPriceRS.getSelectedPriceFlightInfo().getPerPaxPriceInfo());
		sessionStore.storeFareInfo(fareInfo);

		ReservationInfoAdaptor reservationInfoAdaptor = new ReservationInfoAdaptor(flightPriceRS.getTransactionIdentifier());
		ReservationInfo sessionResInfo = reservationInfoAdaptor.adapt(reservation);
		sessionStore.storeResInfo(sessionResInfo);

		if (requoteRequest.isModifySegment()) {
			sessionStore.storeModifiedFlightsInfo(requoteRequest.getModificationInfo().getModifiedFlights());
		}

		Set<String> removedResSegmentRPHlist = ModificationUtils.getRemovedResRPHList(requoteRequest, reservation);
		BalanceQueryRequiredData sessionBalanceQueryInfo = sessionStore.getRequoteBalanceReqiredParams();
		sessionBalanceQueryInfo.setRemovedReservationSegIds(removedResSegmentRPHlist);
		sessionBalanceQueryInfo.setCancelSegment(requoteRequest.isCancelSegment());
		sessionBalanceQueryInfo.setModifySegment(requoteRequest.isModifySegment());
		sessionBalanceQueryInfo.setGroundFltSegByFltSegId(ModificationUtils.getGroundSegmentByFlightSegmentIDMap(flightPriceRS,
				reservation.getSegments()));
		sessionStore.storeRequoteBalanceReqiredParams(sessionBalanceQueryInfo);

		PreferenceInfo sessionPreferences = sessionStore.getPreferences();
		sessionPreferences.setBookingCode(flightPriceRQ.getTravelPreferences().getBookingClassCode());
		sessionPreferences.setSeatType(flightPriceRQ.getTravelPreferences().getBookingType());
		sessionPreferences.setSelectedSystem(flightPriceRQ.getAvailPreferences().getSearchSystem().toString());
		sessionStore.storePreferences(sessionPreferences);

		RequoteSessionONDListAdaptor requoteOndAptor = new RequoteSessionONDListAdaptor(requoteRequest.getModificationInfo()
				.getModifiedFlights(), getRPHGenerator(transactionId), reservation);
		sessionStore.storeOndInfo(requoteOndAptor.adapt(flightPriceRQ.getOriginDestinationInformationList()));
		sessionStore.storeApplicableServiceTaxes(flightPriceRS.getApplicableServiceTaxes());
		storeSegRphInfo(reservation.getSegments(), sessionStore);
		storeSelectedFlightSegmentTOList(transactionId, flightPriceRS);
		storeSessionFlexiDetails(transactionId, requoteRequest, flightPriceRS);
		storeExistingFltSegUnqIds(reservation.getSegments(), sessionStore);
	}

	private void storeExistingFltSegUnqIds(Set<LCCClientReservationSegment> segments,RequoteSessionStore sessionStore) {
		for(LCCClientReservationSegment segment : segments){
			sessionStore.getExistingFlightSegunqIdList().add(segment.getFlightSegmentRefNumber());
		}
	}

	private void storeSegRphInfo(Set<LCCClientReservationSegment> resSegList, RequoteSessionStore sessionStore) {
		RPHGenerator rphGenerator = sessionStore.getRPHGenerator();

		for (LCCClientReservationSegment resSeg : resSegList) {
			String fltSegRph = rphGenerator.getRPH(resSeg.getFlightSegmentRefNumber());
			String resSegRph = ModificationUtils.createResSegRph(resSeg.getCarrierCode(), resSeg.getSegmentSeq());
			sessionStore.storeResSegRphToFlightSegMap(resSegRph, fltSegRph);
		}
	}

	private void storeSelectedFlightSegmentTOList(String transactionId, FlightPriceRS flightPriceRS) {
		if (flightPriceRS.getSelectedPriceFlightInfo() != null) {
			List<FlightSegmentTO> allSegments = new ArrayList<FlightSegmentTO>();
			for (OriginDestinationInformationTO ondInfo : flightPriceRS.getOriginDestinationInformationList()) {
				for (OriginDestinationOptionTO ondOption : ondInfo.getOrignDestinationOptions()) {
					if (ondOption.isSelected()) {
						fillSelSegmentLogicalCabinClass(ondOption.getFlightSegmentList(), flightPriceRS
								.getSelectedPriceFlightInfo().getAvailableLogicalCCList());
						allSegments.addAll(ondOption.getFlightSegmentList());
						break;
					}
				}
			}
			getRequoteAvailabilitySessionStore(transactionId).storeSelectedSegments(allSegments);
		}
	}

	private RequoteSessionStore getRequoteSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private AvailabilityRequoteSessionStore getRequoteAvailabilitySessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private String initiateTransaction(String rqTransactionId, String rsTransactionId, boolean isCancelSegment) {
		String transactionId = getTransactionId(rqTransactionId, rsTransactionId);
		if (isCancelSegment) {
			transactionId = initTransaction(AvailabilityRequoteSessionStore.SESSION_KEY, transactionId);
		}
		return transactionId;
	}

	private String getTransactionId(String rqTransactionId, String rsTransactionId) {
		if (rsTransactionId != null) {
			return rsTransactionId;
		} else {
			return rqTransactionId;
		}
	}

	private void storeSessionFlexiDetails(String transactionId, RequoteRQ requoteRequest, FlightPriceRS flightPriceRS) {

		List<SessionFlexiDetail> sessionFlexiDetailList = getSessionFlexiDetailsList(flightPriceRS,
				requoteRequest.getFlightSegments(), transactionId);
		if (sessionFlexiDetailList != null && !sessionFlexiDetailList.isEmpty() && requoteRequest.getModificationInfo() != null) {
			getRequoteAvailabilitySessionStore(transactionId).storeSessionFlexiDetails(sessionFlexiDetailList);			
		}
	}

	private List<SessionFlexiDetail> getSessionFlexiDetailsList(BaseAvailRS baseAvailRS, List<SpecificFlight> flightSegments,
			String transactionId) {
		List<SessionFlexiDetail> sessionFlexiDetailList = new ArrayList<SessionFlexiDetail>();
		Map<Integer, Boolean> ondWiseFlexiAvailabilityMap = getONDWiseFlexiAvailable(baseAvailRS.getSelectedPriceFlightInfo()
				.getAvailableLogicalCCList());
		int selectedOndSeq = getSelectedOndSequence(flightSegments, transactionId);
		Collections.sort(baseAvailRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList());
		for (ONDExternalChargeTO ondExternalChargeTO : baseAvailRS.getSelectedPriceFlightInfo().getFareTypeTO()
				.getOndExternalCharges()) {
			SessionFlexiDetail sessionFlexiDetail = new SessionFlexiDetail();
			if (selectedOndSeq == ondExternalChargeTO.getOndSequence()) {
				for (ExternalChargeTO externalChargeTO : ondExternalChargeTO.getExternalCharges()) {
					if (externalChargeTO.getType() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
						SessionExternalChargeSummary externalCharge = new SessionExternalChargeSummary();
						externalCharge.setChargeRateId(externalChargeTO.getChargeRateId());
						externalCharge.setSurchargeCode(externalChargeTO.getSurchargeCode());
						externalCharge.setCarrierCode(externalChargeTO.getCarrierCode());
						externalCharge.setFlexiCharge(externalChargeTO.getAmount());
						externalCharge.setSegmentCode(externalChargeTO.getSegmentCode());
						externalCharge.setAdditionalDetails(externalChargeTO.getAdditionalDetails());
						sessionFlexiDetail.getExternalChargeSummary().add(externalCharge);

						sessionFlexiDetail
								.setFlexiAvailable(ondWiseFlexiAvailabilityMap.get(ondExternalChargeTO.getOndSequence()));
						sessionFlexiDetail.setOndSequence(ondExternalChargeTO.getOndSequence());
						sessionFlexiDetail.setTotalFlexiCharge(ondExternalChargeTO.getTotalOndExternalCharges());
						sessionFlexiDetail.setFlexiSelected(isFlexiSelected(baseAvailRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList().get(selectedOndSeq))); // in availability search no fare class selected
						sessionFlexiDetailList.add(sessionFlexiDetail);
					}
				}
			}
		}
		return sessionFlexiDetailList;
	}

	private boolean isFlexiSelected(OndClassOfServiceSummeryTO ondClassOfServiceSummeryTO) {
		for(LogicalCabinClassInfoTO logicalCC : ondClassOfServiceSummeryTO.getAvailableLogicalCCList()){
			if(logicalCC.isSelected()){
				return logicalCC.isWithFlexi();
			}
		}
		return false;
	}

	private Map<Integer, Boolean> getOndFlexiSelection(PriceInfoTO priceInfoTO) {
		Map<Integer, Boolean> ondFlexiSelection = new HashMap<Integer, Boolean>();
		if (priceInfoTO != null) {
			for (OndClassOfServiceSummeryTO ondClassOfService : priceInfoTO.getAvailableLogicalCCList()) {
				for (LogicalCabinClassInfoTO lcc : ondClassOfService.getAvailableLogicalCCList()) {
					if (lcc.isSelected()) {
						ondFlexiSelection.put(ondClassOfService.getSequence(), lcc.isWithFlexi());
					}
				}
			}
		}
		return ondFlexiSelection;
	}

	private Map<Integer, Boolean> getONDWiseFlexiAvailable(List<OndClassOfServiceSummeryTO> availableLogicalCCList) {
		Map<Integer, Boolean> ondWiseFlexiAvailabilityMap = new TreeMap<Integer, Boolean>();
		for (OndClassOfServiceSummeryTO ondClassOfServiceSummeryTO : availableLogicalCCList) {
			boolean isFlexiAvailable = false;
			for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : ondClassOfServiceSummeryTO.getAvailableLogicalCCList()) {
				if (logicalCabinClassInfoTO.isFlexiAvailable() && logicalCabinClassInfoTO.getBundledFarePeriodId() == null) {
					isFlexiAvailable = true;
					break;
				}
			}
			ondWiseFlexiAvailabilityMap.put(ondClassOfServiceSummeryTO.getSequence(), isFlexiAvailable);
		}
		return ondWiseFlexiAvailabilityMap;
	}

	private int getSelectedOndSequence(List<SpecificFlight> flightSegments, String transactionId) {
		if (flightSegments != null && getRequoteSessionStore(transactionId).getFlightSegments() != null) {
			for (SessionFlightSegment sessionFltSeg : getRequoteSessionStore(transactionId).getFlightSegments()) {
				for (SpecificFlight specFlt : flightSegments) {
					if (specFlt.getDepartureDateTime().equals(sessionFltSeg.getDepartureDateTime())
							&& specFlt.getFlightDesignator().equals(sessionFltSeg.getFlightDesignator())) {
						return sessionFltSeg.getOndSequence();
					}

				}
			}
		}
		return -1;
	}

	private void fillSelSegmentLogicalCabinClass(List<FlightSegmentTO> selectedSegments,
			List<OndClassOfServiceSummeryTO> fareSummaryList) {
		for (FlightSegmentTO segment : selectedSegments) {
			LOGICAL_CC_LOOP: for (OndClassOfServiceSummeryTO cosSummary : fareSummaryList) {
				for (LogicalCabinClassInfoTO lccInfo : cosSummary.getAvailableLogicalCCList()) {
					if (cosSummary.getOndCode().contains(segment.getSegmentCode()) && lccInfo.isSelected()) {
						segment.setLogicalCabinClassCode(lccInfo.getLogicalCCCode());
						break LOGICAL_CC_LOOP;
					}
				}
			}
		}
	}

}
