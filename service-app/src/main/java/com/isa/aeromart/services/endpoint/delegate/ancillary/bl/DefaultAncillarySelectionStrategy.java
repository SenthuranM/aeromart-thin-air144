package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemComponent;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.seat.SeatItem;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public abstract class DefaultAncillarySelectionStrategy {

    public abstract PricedAncillaryType getDefaultPricedAncillaryType (PricedAncillaryType prevPricedAncillaryType,  AncillaryType availableAncillaryType, Transition<List<Scope>> transition);
    
    protected AncillaryItem getEquivalentItem(AncillariesConstants.AncillaryType ancillaryType, SelectedItem selectedItem, AncillaryItemComponent availableItems) {
        AncillaryItem equivalentItem = null;

        String itemId = selectedItem.getId();

        AncillaryItemLeaf itemsLeaf = (AncillaryItemLeaf) availableItems;
        String selectedBaggageName, itemBaggageName;

        switch (ancillaryType) {
            case BAGGAGE:
                for (AncillaryItem ancillaryItem : itemsLeaf.getItems()) {
                	itemBaggageName = AnciCommon.getBaggageName(ancillaryItem.getItemId());
                	selectedBaggageName = AnciCommon.getBaggageName(itemId);
                    if (selectedBaggageName.equals(itemBaggageName)) {
                        equivalentItem = ancillaryItem;
                        break;
                    }
                }
                break;
            case MEAL:
            	 for (AncillaryItem ancillaryItem : itemsLeaf.getItems()) {
                     if (itemId.equals(ancillaryItem.getItemId())) {
                         equivalentItem = ancillaryItem;
                         break;
                     }
                 }
                 break;
            case SEAT:
            	for (AncillaryItem ancillaryItem : itemsLeaf.getItems()) {
            		if (itemId.equals(ancillaryItem.getItemId())) {
            			SeatItem seatItem = (SeatItem) ancillaryItem;
            			if(seatItem.isAvailability()){
            		         equivalentItem = ancillaryItem;
            			}
                        break;
                    }
                }
            	break;
            case SSR_IN_FLIGHT:
            case SSR_AIRPORT:
            case INSURANCE:
            case FLEXIBILITY:
            case AIRPORT_TRANSFER:
                for (AncillaryItem ancillaryItem : itemsLeaf.getItems()) {
                    if (itemId.equals(ancillaryItem.getItemId())) {
                        equivalentItem = ancillaryItem;
                        break;
                    }
                }
                break;
        }

        return equivalentItem;
    }
    

    protected List<PricedSelectedItem> getPreviousAncillaryItems(PricedAncillaryType prevPricedAncillaryType, Scope scope) {
        List<PricedSelectedItem> prevAncillaryItems = new ArrayList<>();

        for (AncillaryScope ancillaryScope : prevPricedAncillaryType.getAncillaryScopes()) {
            if (AnciCommon.areEqual(scope, ancillaryScope.getScope())) {
                prevAncillaryItems = ancillaryScope.getAncillaries();
            }
        }

        return prevAncillaryItems;
    }

    protected AncillaryItemComponent getAvailableAncillaryItems(AncillaryType availableAncillaryType, Scope scope) {
        AncillaryItemComponent ancillaryItemComponent = null;

        for (Provider provider : availableAncillaryType.getProviders()) {
            for (AvailableAncillaryUnit availableAncillaryUnit : provider.getAvailableAncillaries().getAvailableUnits()) {
                if (AnciCommon.areEqual(scope, availableAncillaryUnit.getScope())) {
                    ancillaryItemComponent = availableAncillaryUnit.getItemsGroup();
                }
            }
        }

        return ancillaryItemComponent;
    }

    protected List<Transition<Scope>> prepareScopes(Transition<List<Scope>> transition) {

        List<Transition<Scope>> sortedScopes = new ArrayList<>();
        Transition<Scope> transitionScope;
        Iterator<Scope> scopeIte = transition.getNew().iterator();
        for (Scope prevScope : transition.getOld()) {
        	Scope newScope = scopeIte.next();
            transitionScope = new Transition<>();
            transitionScope.setNew(newScope);
            transitionScope.setOld(prevScope);

            sortedScopes.add(transitionScope);
        }

        return sortedScopes;
    }
    
}
