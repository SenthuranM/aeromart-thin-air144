package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.Tax;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;

public class TaxAdaptor implements Adaptor<TaxTO, Tax> {

	@Override
	public Tax adapt(TaxTO source) {
		Tax target = new Tax();
		target.setTaxCode(source.getTaxCode());
		target.setAmount(source.getAmount());
		target.setApplicablePaxTypes(source.getApplicablePassengerTypeCode());
		target.setCarrierCode(source.getCarrierCode());
		return target;
	}

}
