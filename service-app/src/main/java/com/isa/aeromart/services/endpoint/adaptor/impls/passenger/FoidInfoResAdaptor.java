package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.FoidInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;

public class FoidInfoResAdaptor implements Adaptor<LCCClientReservationAdditionalPax, FoidInfo> {

	@Override
	public FoidInfo adapt(LCCClientReservationAdditionalPax lccClientReservationAdditionalPax) {
		FoidInfo foidInfo = new FoidInfo();
		foidInfo.setFoidExpiry(lccClientReservationAdditionalPax.getPassportExpiry());
		foidInfo.setFoidNumber(lccClientReservationAdditionalPax.getPassportNo());
		foidInfo.setFoidPlace(lccClientReservationAdditionalPax.getPassportIssuedCntry());
		foidInfo.setPlaceOfBirth(lccClientReservationAdditionalPax.getPlaceOfBirth());
		return foidInfo;
	}

}
