package com.isa.aeromart.services.endpoint.dto.session;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherInformation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySession;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.AvailabilitySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PassengerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;

public class BookingTransaction extends BaseTransaction implements Serializable, AvailabilitySessionStore, PassengerSessionStore,
		AncillarySessionStore, PaymentSessionStore, BookingSessionStore {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String SESSION_KEY = "BKG_TRNX";

	private FareSegChargeTO pricingInformation;
	private List<SessionFlightSegment> segments;
	private RPHGenerator rphGenerator = null;
	private TravellerQuantity travellerQuantity;
	private List<ServiceTaxContainer> applicableServiceTaxes;
	private PreferenceInfo preferenceInfo;
	private List<SessionFlexiDetail> sessionFlexiDetailList;
	private ReservationInfo reservationInfo;

	// anci session data bundle
	private AncillarySession ancillarySession;

	// payment related
	private PostPaymentDTO postPaymentDTO;
	private LoyaltyInformation loyaltyInformation = null;
	private DiscountedFareDetails discountedFareDetails;
	private BigDecimal discountAmount;
	private BigDecimal totalAvailableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
	private ReservationDiscountDTO reservationDiscountDTO;
	private PaymentStatus paymentStatus;
	private OperationTypes operationTypes;
	private BinPromoDiscountInfo binPromoDiscountInfo;
	private BigDecimal serviceTaxForTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	// booking related
	private BookingRQ BookingRQ;
	private boolean onHoldCreated;
	private String pnr;
	
	// otp related
	private String voucherOTP;

	// financial data
	private FinancialStore financialStore;

	private TotalPaymentInfo totalPaymentInfo;

	private String captchaText;
	private String originCountryCodeForTest;
	private int paymentAttempts;
	private String onholdReleaseTime;
	private String lastName;
	private Date firstDepartureDate;
	private PayByVoucherInfo payByVoucherInfo;
	private boolean adminFeeInitiated;
	private VoucherPaymentInfo voucherPaymentInfo;
	private Map<String, VoucherPaymentInfo> voucherPaymentInfoMap;
	private VoucherInformation voucherInformation;
	private Map<String, String> voucherOTPMap;
	
	

	public BookingTransaction() {
		super.addInitHook(() -> {
			ancillarySession = new AncillarySession();
			ancillarySession.init();
			financialStore = new FinancialStore();
		});

		super.init();
	}

	@Override
	public void storePriceInventoryInfo(FareSegChargeTO pricingInformation) {
		this.pricingInformation = pricingInformation;
	}

	@Override
	public void storeSelectedSegments(List<FlightSegmentTO> segments) {
		this.segments = new ArrayList<SessionFlightSegment>();
		for (FlightSegmentTO segmentTO : segments) {
			this.segments.add(new SessionFlightSegment(segmentTO));
		}
	}

	@Override
	public void storePreferences(PreferenceInfo preferenceInfo) {
		this.preferenceInfo = preferenceInfo;
	}

	@Override
	public List<SessionFlightSegment> getSegments() {
		return segments;
	}

	@Override
	public FareSegChargeTO getFarePricingInformation() {
		return pricingInformation;
	}

	@Override
	public TotalPaymentInfo getTotalPaymentInformation() {
		buildPaymentInfo();
		return totalPaymentInfo;
	}

	@Override
	public RPHGenerator getRPHGenerator() {
		if (rphGenerator == null) {
			rphGenerator = new RPHGenerator();
		}
		return rphGenerator;
	}

	@Override
	public void storePostPaymentInformation(PostPaymentDTO postPaymentDTO) {
		this.postPaymentDTO = postPaymentDTO;

	}

	@Override
	public PostPaymentDTO getPostPaymentInformation() {
		return postPaymentDTO;
	}

	// anci impl ------------

	public SessionBasicReservation getReservationData() {
		return ancillarySession.getReservation();
	}

	public AncillarySession getAncillarySession() {
		return ancillarySession;
	}

	public List<SessionFlightSegment> getSegmentWithInventory() {
		return segments;
	}

	public FinancialStore getFinancialStore() {
		return financialStore;
	}

	public void addPaymentCharge(ExternalChgDTO ccChgDTO) {
		financialStore.getPaymentExternalCharges().getReservationCharges().add(new LCCClientExternalChgDTO(ccChgDTO));
	}

	public void removePaymentCharge(EXTERNAL_CHARGES extChgEnum) {
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getReservationCharges() != null
				&& !financialStore.getPaymentExternalCharges().getReservationCharges().isEmpty()) {
			Iterator<LCCClientExternalChgDTO> paymentChargeItr = financialStore.getPaymentExternalCharges()
					.getReservationCharges().iterator();
			while (paymentChargeItr.hasNext()) {
				LCCClientExternalChgDTO charge = paymentChargeItr.next();
				if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
					paymentChargeItr.remove();
				}
			}
		}
	}
	
	public void removePaymentPaxCharge(EXTERNAL_CHARGES extChgEnum) {
		// Assuming Service Tax is populated pax wise in payment session store we only remove Pax wise Charges
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getPassengers() != null
				&& !financialStore.getPaymentExternalCharges().getPassengers().isEmpty()) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> passengerCharges = financialStore.getPaymentExternalCharges().getPassengers();
			for(PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : passengerCharges){
				Iterator<LCCClientExternalChgDTO> paymentChargeItr= passengerChargeTo.getGetPassengerCharges().iterator();
				while (paymentChargeItr.hasNext()) {
					LCCClientExternalChgDTO charge = paymentChargeItr.next();
					if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
						paymentChargeItr.remove();
					}
				}
			}		
		}
	}

	@Override
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;

	}

	@Override
	public BigDecimal getDiscountAmount() {
		// FIXME temp hack
		if (discountAmount != null) {
			return discountAmount;
		}

		if (reservationDiscountDTO != null) {
			return reservationDiscountDTO.getTotalDiscount();
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	@Override
	public void setDiscountedFareDetails(DiscountedFareDetails discountedFareDetails) {
		this.discountedFareDetails = discountedFareDetails;

	}

	@Override
	public DiscountedFareDetails getDiscountedFareDetails() {
		return discountedFareDetails;
	}

	@Override
	public List<SessionFlightSegment> getSelectedSegmentsForPayment() {
		return segments;
	}

	@Override
	public PreferenceInfo getPreferencesForPayment() {
		return preferenceInfo;
	}

	@Override
	public PreferenceInfo getPreferencesForAncillary() {
		return preferenceInfo;
	}

	@Override
	public FareSegChargeTO getFareSegChargeTOForPayment() {
		return pricingInformation;
	}

	@Override
	public LoyaltyInformation getLoyaltyInformation() {
		if (loyaltyInformation == null) {
			loyaltyInformation = new LoyaltyInformation();
			loyaltyInformation.setLoyaltyPaymentOption(LoyaltyPaymentOption.NONE);
		}
		return loyaltyInformation;
	}

	@Override
	public void storeLoyaltyInformation(LoyaltyInformation loyaltyInformation) {
		this.loyaltyInformation = loyaltyInformation;
	}

	@Override
	public void storeBookingRQ(BookingRQ BookingRQ) {
		this.BookingRQ = BookingRQ;

	}

	@Override
	public void storeOnHoldCreated(boolean flag) {
		this.onHoldCreated = flag;

	}

	@Override
	public BookingRQ getbookingRQForPayment() {
		return BookingRQ;
	}

	@Override
	public boolean isOnHoldCreatedForPayment() {
		return onHoldCreated;
	}

	@Override
	public boolean isOnHoldCreated() {
		return onHoldCreated;
	}

	@Override
	public void storeTotalAvailableBalance(BigDecimal totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;

	}

	@Override
	public BigDecimal getTotalAvailableBalance() {
		// TODO Auto-generated method stub
		return totalAvailableBalance;
	}

	@Override
	public void storeTravellerQuantity(TravellerQuantity travellerQuantity) {
		this.travellerQuantity = travellerQuantity;
	}

	@Override
	public TravellerQuantity getTravellerQuantityForPayment() {
		return travellerQuantity;
	}

	@Override
	public TravellerQuantity getTravellerQuantityForAncillary() {
		return travellerQuantity;
	}

	@Override
	public TravellerQuantity getTravellerQuantity() {
		return travellerQuantity;
	}

	private void buildPaymentInfo() {
		totalPaymentInfo = new TotalPaymentInfo(pricingInformation, financialStore, travellerQuantity);
	}

	@Override
	public void storeDiscountDetails(DiscountedFareDetails discountDetails) {
		this.discountedFareDetails = discountDetails;
	}

	@Override
	public void storeReservationDiscountDTO(ReservationDiscountDTO reservationDiscountDTO) {
		this.reservationDiscountDTO = reservationDiscountDTO;
	}

	@Override
	public Collection<LCCClientExternalChgDTO> getExternalCharges() {
		return financialStore.getAllExternalCharges();
	}

	@Override
	public List<ServiceTaxContainer> getApplicableServiceTaxes() {
		if (applicableServiceTaxes == null) {
			applicableServiceTaxes = new ArrayList<ServiceTaxContainer>();
		}
		return applicableServiceTaxes;
	}

	@Override
	public void storeApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		getApplicableServiceTaxes().clear();
		getApplicableServiceTaxes().addAll(applicableServiceTaxes);
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	@Override
	public void storePaymentStatus(PaymentStatus actionStatus) {
		this.paymentStatus = actionStatus;
	}

	@Override
	public void clearPaymentInformation() {
		this.paymentStatus = null;
		this.postPaymentDTO = null;
		this.loyaltyInformation = null;
		removeAllPaymentCharge();

	}

	private void removeAllPaymentCharge() {
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getReservationCharges() != null
				&& !financialStore.getPaymentExternalCharges().getReservationCharges().isEmpty()) {
			financialStore.getPaymentExternalCharges().getReservationCharges().clear();
			financialStore.getPaymentExternalCharges().getPassengers().clear();

		}
	}

	@Override
	public void storeOperationType(OperationTypes operationTypes) {
		this.operationTypes = operationTypes;

	}

	@Override
	public OperationTypes getOperationType() {
		return operationTypes;
	}

	// TODO check naming as we include Availability level FLEXI charges
	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalCharges() {

		List<PassengerChargeTo<LCCClientExternalChgDTO>> paxwiseAnciExternalCharges = new ArrayList<PassengerChargeTo<LCCClientExternalChgDTO>>();

		// if only availability available
		if (financialStore.getAncillaryExternalCharges() == null && financialStore.getAvailabilityExternalCharges() != null) {

			paxwiseAnciExternalCharges.addAll(financialStore.getAvailabilityExternalCharges().getPassengers());

			return paxwiseAnciExternalCharges;

		}

		if (financialStore.getAncillaryExternalCharges() != null) {
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeToAnci : financialStore.getAncillaryExternalCharges()
					.getPassengers()) {
				PassengerChargeTo<LCCClientExternalChgDTO> newPassengerChargeToAnci = new PassengerChargeTo<LCCClientExternalChgDTO>();
				List<LCCClientExternalChgDTO> passengerCharges = new ArrayList<LCCClientExternalChgDTO>();
				newPassengerChargeToAnci.setPassengerRph(new String(passengerChargeToAnci.getPassengerRph()));
				for (LCCClientExternalChgDTO newExternalChg : passengerChargeToAnci.getGetPassengerCharges()) {
					if (newExternalChg.getExternalCharges() != null
							&& newExternalChg.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)) {
						passengerCharges.add(((LCCClientFlexiExternalChgDTO) newExternalChg).cloneWithFlexibilities());
					} else {
						passengerCharges.add(newExternalChg.clone());
					}
				}
				newPassengerChargeToAnci.setGetPassengerCharges(passengerCharges);
				paxwiseAnciExternalCharges.add(newPassengerChargeToAnci);
			}
			// paxwiseAnciExternalCharges.addAll((financialStore.getAncillaryExternalCharges().getPassengers()));
		}

		// merge availability passenger charges (FLEXI)
		if (financialStore.getAvailabilityExternalCharges() != null) {
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeToAnci : paxwiseAnciExternalCharges) {
				for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeToAvai : financialStore
						.getAvailabilityExternalCharges().getPassengers()) {
					if (passengerChargeToAnci.getPassengerRph().contentEquals(passengerChargeToAvai.getPassengerRph())) {
						for (LCCClientExternalChgDTO passengerAvailChg : passengerChargeToAvai.getGetPassengerCharges()) {
							if (passengerAvailChg.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)) {
								boolean isFlexiChgAvail = false;
								for (LCCClientExternalChgDTO passengerAnciChg : passengerChargeToAnci.getGetPassengerCharges()) {
									if (passengerAnciChg.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)
											&& passengerAnciChg.getFlightRefNumber().equalsIgnoreCase(
													passengerAvailChg.getFlightRefNumber())) {
										isFlexiChgAvail = true;
										break;
									}
								}
								if (!isFlexiChgAvail) {
									passengerChargeToAnci.getGetPassengerCharges().add(
											((LCCClientFlexiExternalChgDTO) passengerAvailChg).cloneWithFlexibilities());
								}
							}
						}
						// passengerChargeToAnci.getGetPassengerCharges().addAll(passengerChargeToAvai.getGetPassengerCharges());
					}
				}
			}

		}

		return paxwiseAnciExternalCharges;

	}

	@Override
	public Collection<LCCClientExternalChgDTO> getPaymentExternalCharges() {
		return financialStore.getPaymentExternalCharges().getReservationCharges();
	}
	
	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxWisePaymentExternalCharges(){
		return financialStore.getPaymentExternalCharges().getPassengers();
	}

	@Override
	public ReservationInfo getResInfo() {
		return reservationInfo;
	}

	@Override
	public void storeBalancePaymentRequiredInfo(List<FlightSegmentTO> fltSegments, PreferenceInfo prefInfo,
			List<ServiceTaxContainer> applicableServiceTaxes, TravellerQuantity travellerQuantity) {

		this.storeSelectedSegments(fltSegments);
		this.preferenceInfo = prefInfo;
		this.applicableServiceTaxes = applicableServiceTaxes;
		this.travellerQuantity = travellerQuantity;

	}

	@Override
	public List<ModifiedFlight> getModifiedFlightsInfo() {
		return null;
	}

	@Override
	public ReservationDiscountDTO getReservationDiscountDTO() {
		return reservationDiscountDTO;
	}

	@Override
	public Map<String, String> getResSegRphToFlightSegMap() {
		return null;
	}

	@Override
	public void storePnr(String pnr) {
		this.pnr = pnr;
	}

	@Override
	public String getPnr() {
		return pnr;
	}

	@Override
	public void clearBookingDetails() {
		pnr = null;
		pricingInformation = null;
		travellerQuantity = null;
		reservationInfo = null;
		ancillarySession = null;
		postPaymentDTO = null;
		loyaltyInformation = null;
		discountedFareDetails = null;
		discountAmount = null;
		totalAvailableBalance = null;
		reservationDiscountDTO = null;
		paymentStatus = null;
		operationTypes = null;
		binPromoDiscountInfo = null;
		onHoldCreated = false;
		totalPaymentInfo = null;
		captchaText = null;
		segments = null;
		rphGenerator = null;
		applicableServiceTaxes = null;
		preferenceInfo = null;
		BookingRQ = null;
		sessionFlexiDetailList = null;
		financialStore = null;
	}

	@Override
	public void storeRPHGeneratorForAncillaryModification(RPHGenerator rphGenerator) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storeReservationDataForAncillaryModification(ReservationInfo resInfo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storeSessionFlightsForAncillaryModification(List<FlightSegmentTO> segments) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storePreferenceInfoForAncillaryModification(PreferenceInfo preferenceInfo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void storePricingInformationForAncillaryModification(FareSegChargeTO pricingInformation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isGroupPNR() {

		return ProxyConstants.SYSTEM.getEnum(getPreferencesForPayment().getSelectedSystem()) == SYSTEM.INT;
	}

	@Override
	public PreferenceInfo getPreferenceInfoForBooking() {
		return preferenceInfo;
	}

	public void storeResInfo(ReservationInfo resInfo) {
		reservationInfo = resInfo;
	}

	@Override
	public void storeSessionFlexiDetails(List<SessionFlexiDetail> sessionFlexiDetailList) {
		this.sessionFlexiDetailList = sessionFlexiDetailList;

	}

	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetailForAncillary() {
		return sessionFlexiDetailList;
	}
	
	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetailForPayment() {
		return sessionFlexiDetailList;
	}


	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetails() {
		return sessionFlexiDetailList;
	}

	@Override
	public void storeCaptchaText(String captcha) {
		this.captchaText = captcha;
	}

	@Override
	public String getCaptchaText() {
		return captchaText;
	}

	@Override
	public BinPromoDiscountInfo getBinPromoDiscountInfo() {
		return binPromoDiscountInfo;
	}

	@Override
	public void storeBinPromoDiscountInfo(BinPromoDiscountInfo binPromoDiscountInfo) {
		this.binPromoDiscountInfo = binPromoDiscountInfo;
	}

	@Override
	public DiscountedFareDetails getDiscountedFareDetailsForAncillary() {
		return discountedFareDetails;
	}

	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalChargesForAncillary() {
		if (financialStore.getAncillaryExternalCharges() != null) {
			return financialStore.getAncillaryExternalCharges().getPassengers();
		}
		return null;
	}

	@Override
	public List<SessionFlightSegment> getSelectedSegmentsForAncillary() {
		return segments;
	}

	@Override
	public void storeUpdatedSessionFlightSegment(List<SessionFlightSegment> segments) {
		this.segments = segments;
	}

	// TODO remove and add directly to financial store
	@Override
	public void addAvailabilityFlexiChargesToFinancialStore(List<Passenger> passengers) {
		financialStore.buildAvailabilityFlexiCharges(sessionFlexiDetailList, passengers, segments, travellerQuantity);

	}

	@Override
	public DiscountedFareDetails getDiscountedFareDetailsForBooking(boolean isBinPromotion) {
		if (isBinPromotion) {
			return binPromoDiscountInfo.getDiscountedFareDetails();
		}
		return discountedFareDetails;
	}

	@Override
	public BigDecimal getDiscountAmountForBooking(boolean isBinPromotion) {
		if (isBinPromotion) {
			return binPromoDiscountInfo.getDiscountAmount();
		}
		return discountAmount;
	}

	@Override
	public ReservationDiscountDTO getReservationDiscountDTOForBooking(boolean isBinPromotion) {
		if (isBinPromotion) {
			return binPromoDiscountInfo.getReservationDiscountDTO();
		}
		return reservationDiscountDTO;
	}

	@Override
	public void setOriginCountryCodeForTest(String originCountryCodeForTest) {
		this.originCountryCodeForTest = originCountryCodeForTest;

	}

	@Override
	public String getOriginCountryCodeForTest() {
		return this.originCountryCodeForTest;
	}

	@Override
	public List<String> getInvolvingCarrierCodes() {
		List<String> carrierList = new ArrayList<>();
		if (segments != null) {
			for (SessionFlightSegment segment : segments) {
				if (!carrierList.contains(segment.getOperatingAirline())) {
					carrierList.add(segment.getOperatingAirline());
				}
			}
		}
		return carrierList;
	}

	@Override
	public void storeServiceCount(Map<String, Integer> serviceCount) {
		// TODO Auto-generated method stub

	}

	@Override
	public Map<String, Integer> getServiceCount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void trackPaymentAttempts() throws ModuleException {
		paymentAttempts++;
		if (paymentAttempts > PaymentConsts.PAYMENT_ATTEMPT_LIMIT) {
			throw new ModuleException("payment.api.payment.attempt.limit.exceeded");
		}

	}

	@Override
	public void storeOnholdReleaseTime(String onholdReleaseTime) {
		this.onholdReleaseTime = onholdReleaseTime;

	}

	@Override
	public String getOnholdReleaseTime() {
		return onholdReleaseTime;
	}

	@Override
	public void storeLastName(String lastName) {
		this.lastName = lastName;

	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public void storeFirstDepatureDate(Date firstDepartureDate) {
		this.firstDepartureDate = firstDepartureDate;
	}

	@Override
	public Date getFirstDepatureDate() {
		return firstDepartureDate;
	}

	public boolean isAdminFeeInitiated() throws ModuleException {
		if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			throw new ModuleException("payment.api.invalid.administration.fee.access");
		}
		return this.adminFeeInitiated;
	}

	public void setAdminFeeInitiated() throws ModuleException {
		if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			throw new ModuleException("payment.api.invalid.administration.fee.access");
		}
		this.adminFeeInitiated = true;
	}

	public BigDecimal getServiceTaxForTransactionFee() {
		return serviceTaxForTransactionFee;
	}

	public void setServiceTaxForTransactionFee(BigDecimal serviceTaxForTransactionFee) {
		this.serviceTaxForTransactionFee = serviceTaxForTransactionFee;
	}

	@Override
	public List<SessionFlightSegment> getAllSegmentWithInventory() {
		return getSegmentWithInventory();
	}

	@Override
	public String getVoucherOTP() {
		return voucherOTP;
	}

	@Override
	public void setVoucherOTP(String voucherOTP) {
		this.voucherOTP = voucherOTP;
	}
	
	@Override
	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	@Override
	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	@Override public VoucherInformation getVoucherInformation() {
		if (voucherInformation == null) {
			voucherInformation = new VoucherInformation();
			voucherInformation.setIsTotalAmountPaidFromVoucher(false);
		}
		return voucherInformation;
	}

	@Override
	public Map<String, String> getVoucherOTPMap() {
		if (this.voucherOTPMap == null) {
			this.voucherOTPMap = new HashMap<>();
		}
		return voucherOTPMap;
	}
}
