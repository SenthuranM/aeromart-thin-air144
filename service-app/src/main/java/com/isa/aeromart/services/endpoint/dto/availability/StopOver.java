package com.isa.aeromart.services.endpoint.dto.availability;


public class StopOver {

	// airport code
	private String airport;

	private String period;

	public String getAirport() {
		return airport;
	}

	public void setAirport(String airport) {
		this.airport = airport;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

}
