package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class CustomerDetailsResponse extends BaseRS{

	private CustomerDetails customer;
	
    private LMSDetails LmsDetails;

	public CustomerDetails getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDetails customer) {
		this.customer = customer;
	}

	public LMSDetails getLmsDetails() {
		return LmsDetails;
	}

	public void setLmsDetails(LMSDetails lmsDetails) {
		LmsDetails = lmsDetails;
	}
	
}
