package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class ConfirmCustomerRS extends TransactionalBaseRS {

	private boolean customerConfirmed;

	public boolean getCustomerConfirmed() {
		return customerConfirmed;
	}

	public void setCustomerConfirmed(boolean customerConfirmed) {
		this.customerConfirmed = customerConfirmed;
	}

}
