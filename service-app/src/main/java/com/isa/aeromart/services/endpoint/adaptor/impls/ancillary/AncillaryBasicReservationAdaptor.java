package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.ContactInformation;
import com.isa.aeromart.services.endpoint.dto.ancillary.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.Address;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class AncillaryBasicReservationAdaptor implements Adaptor<LCCClientReservation, BasicReservation>{

	@Override
	public BasicReservation adapt(LCCClientReservation source) {
		
		CommonReservationContactInfo lccContactInfo = source.getContactInfo();
		BasicReservation basicReservation = new BasicReservation();
		List<Passenger> passengerList = new ArrayList<Passenger>();
		ContactInformation contactInfo = new ContactInformation();
		Address address = new Address();
		PhoneNumber mobileNumber =  new PhoneNumber();
		PhoneNumber landNumber =  new PhoneNumber();
		
		Passenger passenger;
		
		address.setCity(lccContactInfo.getCity());
		address.setZipCode(lccContactInfo.getZipCode());
		address.setStreetAddress1(lccContactInfo.getStreetAddress1());
		address.setStreetAddress2(lccContactInfo.getStreetAddress2());
		
		mobileNumber.setNumber(lccContactInfo.getMobileNo());
		mobileNumber.setCountryCode(lccContactInfo.getCountryCode());
		
		landNumber.setNumber(lccContactInfo.getPhoneNo());
		landNumber.setCountryCode(lccContactInfo.getCountryCode());
		
		contactInfo.setFirstName(lccContactInfo.getFirstName());
		contactInfo.setLastName(lccContactInfo.getLastName());		
		contactInfo.setAddress(address);
		contactInfo.setCountry(lccContactInfo.getCountryCode());
		contactInfo.setEmailAddress(lccContactInfo.getEmail());
		contactInfo.setMobileNumber(mobileNumber);
		contactInfo.setLandNumber(landNumber);
		contactInfo.setNationality(lccContactInfo.getNationality());
		contactInfo.setState(lccContactInfo.getState());
		contactInfo.setTitle(lccContactInfo.getTitle());
		
		for(LCCClientReservationPax lccClientReservationPax : source.getPassengers()){
			passenger = new Passenger();
			passenger.setDateOfBirth(lccClientReservationPax.getDateOfBirth());
			passenger.setFirstName(lccClientReservationPax.getFirstName());
			passenger.setLastName(lccClientReservationPax.getLastName());
			passenger.setNationality(lccClientReservationPax.getNationality());
			passenger.setPassengerRph(lccClientReservationPax.getPaxSequence().toString());
			passenger.setTitle(lccClientReservationPax.getTitle());
			passenger.setType(lccClientReservationPax.getPaxType());
			passenger.setTravelerRefNumber(lccClientReservationPax.getTravelerRefNumber());
			if (PaxTypeTO.INFANT.equals(lccClientReservationPax.getPaxType()) && lccClientReservationPax.getParent() != null) {
				passenger.setTravelWith(lccClientReservationPax.getParent().getPaxSequence());
			}
			passengerList.add(passenger);
		}
		
		basicReservation.setContactInfo(contactInfo);
		basicReservation.setPassengers(passengerList);
		
		return basicReservation;
	}

	
}
