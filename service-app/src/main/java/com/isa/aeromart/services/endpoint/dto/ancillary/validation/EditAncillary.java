package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

public class EditAncillary extends BasicRuleValidation {

	private boolean editAncillary;

	public EditAncillary() {
		setRuleCode(RuleCode.EDIT_ANCILLARY);
		
	}

	public boolean isEditAncillary() {
		return editAncillary;
	}

	public void setEditAncillary(boolean editAncillary) {
		this.editAncillary = editAncillary;
	}
	 
}
