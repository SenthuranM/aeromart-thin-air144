package com.isa.aeromart.services.endpoint.dto.ancillary.provider;

import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;

public class SystemProvided extends Provider {

    public SystemProvided() {
        setProviderType(ProviderType.SYSTEM);
    }

}
