package com.isa.aeromart.services.endpoint.dto.ancillary;

public class ReservationAncillarySelection {

	private MetaData metaData;

	private AncillaryReservation ancillaryReservation;

	public MetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	public AncillaryReservation getAncillaryReservation() {
		return ancillaryReservation;
	}

	public void setAncillaryReservation(AncillaryReservation ancillaryReservation) {
		this.ancillaryReservation = ancillaryReservation;
	}
}
