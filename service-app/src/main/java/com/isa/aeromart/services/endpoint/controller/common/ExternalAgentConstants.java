package com.isa.aeromart.services.endpoint.controller.common;

public class ExternalAgentConstants {

	// query parameter types
	public static final String USER_NAME = "username";
	public static final String PASSWORD = "password";
	public static final String HASH_VALUE = "hashvalue";

	public static final String TOKEN = "token";
	public static final String STATUS = "status";
	public static final String MESSAGE = "message";
	public static final String EXPIRY_TIME_STAMP = "expiryTimeStamp";
	public static final String TOKEN_ID = "tokenID";

	public static final String AUTH_TOKEN = "auth-token";

	// query parameter values
	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAIL = "FAIL";
	public static final String YES = "Y";
	public static final String NO = "N";

	public static final String MERCHANT_ID = "goquoHoliday";
	
	public static final Long TIME_GAP = new Long(7*60*1000);
	
	public static final String ERROR_PAGE_URL = "http://www.google.lk";
}
