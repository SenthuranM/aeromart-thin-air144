package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class EffectivePaymentRS extends TransactionalBaseRS {

	private PaymentOptions paymentOptions;


	public PaymentOptions getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(PaymentOptions paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

}
