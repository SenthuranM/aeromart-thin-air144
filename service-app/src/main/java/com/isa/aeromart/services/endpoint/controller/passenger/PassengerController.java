package com.isa.aeromart.services.endpoint.controller.passenger;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.DuplicateCheckAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.passenger.PassengerService;
import com.isa.aeromart.services.endpoint.delegate.passenger.PaxDOBValidator;
import com.isa.aeromart.services.endpoint.dto.passenger.DuplicateCheckRQ;
import com.isa.aeromart.services.endpoint.dto.passenger.DuplicateCheckRS;
import com.isa.aeromart.services.endpoint.dto.passenger.PaxDOBValidationRequest;
import com.isa.aeromart.services.endpoint.dto.passenger.PaxDOBValidationResponse;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRQ;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRS;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

@Controller
@RequestMapping("passenger")
public class PassengerController extends StatefulController {

	private static Log log = LogFactory.getLog(PassengerController.class);

	@Autowired
	private PassengerService passengerService;
	
	@Autowired
	private PaxDOBValidator paxValidator;

	@ResponseBody
	@RequestMapping(value = "/paxDOBValidator", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
	PaxDOBValidationResponse doPaxValidation(@RequestBody PaxDOBValidationRequest paxDOBValidationRequest) {
		
		PaxDOBValidationResponse paxDOBValidationResponse = paxValidator.getPaxValidationResponse(paxDOBValidationRequest);
		return paxDOBValidationResponse;
	}
	
	@ResponseBody
	@RequestMapping(value = "/duplicateCheck", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			DuplicateCheckRS checkDuplicates(@RequestBody DuplicateCheckRQ duplicateCheckRQ) {
		DuplicateValidatorAssembler duplicateValidatorAssembler = getDuplicateCheckAssembler(duplicateCheckRQ);
		boolean hasDuplicates = passengerService.checkDuplicates(getTrackInfo(), duplicateValidatorAssembler);
		return getDuplicateCheckRS(hasDuplicates, duplicateCheckRQ.getTransactionId());
	}

	@ResponseBody
	@RequestMapping(value = "/lmsPaxValidation", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			ValidatePaxFFIDsRS validateLMSPax(@RequestBody ValidatePaxFFIDsRQ validatePaxFFIDsRQ) throws ModuleException {
		ValidatePaxFFIDsRS validatePaxFFIDsRS = passengerService.validateLMSPax(validatePaxFFIDsRQ);
		validatePaxFFIDsRS.setTransactionId(validatePaxFFIDsRQ.getTransactionId());
		return validatePaxFFIDsRS;
	}

	private DuplicateValidatorAssembler getDuplicateCheckAssembler(DuplicateCheckRQ duplicateCheckRQ) {
		List<SessionFlightSegment> flightSegments;
		String selectedSystem;
		if (getTrasactionStore(duplicateCheckRQ.getTransactionId()) instanceof BookingSessionStore) {
			BookingSessionStore sessionStore = (BookingSessionStore) getTrasactionStore(duplicateCheckRQ.getTransactionId());
			flightSegments = sessionStore.getSegments();
			selectedSystem = sessionStore.getPreferenceInfoForBooking().getSelectedSystem();
		} else {
			RequoteSessionStore sessionStore = (RequoteSessionStore) getTrasactionStore(duplicateCheckRQ.getTransactionId());
			flightSegments = getNewFlightSegments(sessionStore);
			selectedSystem = sessionStore.getPreferences().getSelectedSystem();
		}
		DuplicateCheckAdaptor duplicateCheckAdaptor = new DuplicateCheckAdaptor(removeBusSegments(flightSegments), selectedSystem);
		return duplicateCheckAdaptor.adapt(duplicateCheckRQ);
	}
	
	private List<SessionFlightSegment> getNewFlightSegments(RequoteSessionStore sessionStore) {
		List<SessionFlightSegment> fltSegList = new ArrayList<SessionFlightSegment>();
		for (SessionFlightSegment fltSeg : sessionStore.getFlightSegments()) {
			if (!sessionStore.getExistingFlightSegunqIdList().contains(fltSeg.getFlightRefNumber())) {
				fltSegList.add(fltSeg);
			}
		}
		return fltSegList;
	}

	private DuplicateCheckRS getDuplicateCheckRS(boolean hasDuplicates, String transactionId) {
		DuplicateCheckRS duplicateCheckRS = new DuplicateCheckRS();
		duplicateCheckRS.setHasDuplicates(hasDuplicates);
		duplicateCheckRS.setTransactionId(transactionId);
		return duplicateCheckRS;
	}

	private List<SessionFlightSegment> removeBusSegments(List<SessionFlightSegment> allSegments) {
		List<SessionFlightSegment> flightSegments = new ArrayList<SessionFlightSegment>();
		for (SessionFlightSegment flightSegment : allSegments) {
			try {
				if (flightSegment.getSegmentCode() != null
						&& !ModuleServiceLocator.getAirportBD().isBusSegment(flightSegment.getSegmentCode())) {
					flightSegments.add(flightSegment);
				}
			} catch (ModuleException e) {
				log.debug(" Passenger Controller ==> removeBusSegments :" + e);
			}
		}
		return flightSegments;
	}

}
