package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.airporttransfer.AirportTransferItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.baggage.BaggageItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.meal.MealItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.seat.SeatItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.airport.AirportSsrItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.inflight.InFlightSsrItems;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = BaggageItems.class, name = AncillariesConstants.Type.BAGGAGE),
        @JsonSubTypes.Type(value = MealItems.class, name = AncillariesConstants.Type.MEAL),
        @JsonSubTypes.Type(value = SeatItems.class, name = AncillariesConstants.Type.SEAT),
        @JsonSubTypes.Type(value = InFlightSsrItems.class, name = AncillariesConstants.Type.SSR_IN_FLIGHT),
        @JsonSubTypes.Type(value = AirportSsrItems.class, name = AncillariesConstants.Type.SSR_AIRPORT),
        @JsonSubTypes.Type(value = InsuranceItems.class, name = AncillariesConstants.Type.INSURANCE),
        @JsonSubTypes.Type(value = FlexiItems.class, name = AncillariesConstants.Type.FLEXIBILITY),
        @JsonSubTypes.Type(value = AirportTransferItems.class, name = AncillariesConstants.Type.AIRPORT_TRANSFER) })
public abstract class AncillaryItemComponent {

	private String type;

	private List<Integer> validations;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Integer> getValidations() {
		return validations;
	}

	public void setValidations(List<Integer> validations) {
		this.validations = validations;
	}
}
