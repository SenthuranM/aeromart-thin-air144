package com.isa.aeromart.services.endpoint.controller.payment;

import com.isa.aeromart.services.endpoint.delegate.payment.ConfirmPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "payFortOffline")
public class PayFortOfflineConfirmationController {

    @Autowired
    private ConfirmPaymentService confirmPaymentService;

    @ResponseBody
    @RequestMapping(value = "/payment/confirm", method = RequestMethod.POST)
    public Map<String, String> confirmPayment(@RequestParam Map<String, String> requestParams) {
        return confirmPaymentService.confirmPayFortOfflinePayment(requestParams);
    }
}
