package com.isa.aeromart.services.endpoint.adaptor.impls.customer;


import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.LoyaltyProfileDetailsRQ;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;

public class LoyaltyProfileAdaptor implements Adaptor<LoyaltyProfileDetailsRQ, LoyaltyCustomerProfile> {

	private Customer customer;

	public LoyaltyProfileAdaptor(Customer customer) {
		this.customer = customer;
	}

	@Override
	public LoyaltyCustomerProfile adapt(LoyaltyProfileDetailsRQ loyaltyProfileDetails) {
		LoyaltyCustomerProfile loyaltyCustomerProfile = new LoyaltyCustomerProfile();
		loyaltyCustomerProfile.setCity(loyaltyProfileDetails.getCity());
		loyaltyCustomerProfile.setDateOfBirth(loyaltyProfileDetails.getDateOfBirth());
		loyaltyCustomerProfile.setLoyaltyAccountNo(loyaltyProfileDetails.getLoyaltyAccountNo());
		if (customer != null) {
			loyaltyCustomerProfile.setCountryCode(customer.getCountryCode());
			loyaltyCustomerProfile.setNationalityCode(customer.getNationalityCode());
			loyaltyCustomerProfile.setMobile(customer.getMobile());
			loyaltyCustomerProfile.setCustomerId(customer.getCustomerId());
			if (customer.getLMSMemberDetails() != null) {
				loyaltyCustomerProfile.setDateOfBirth(customer.getLMSMemberDetails().getDateOfBirth());
				loyaltyCustomerProfile.setEmail(customer.getLMSMemberDetails().getFfid());
			}
		}
		return loyaltyCustomerProfile;
	}

}
