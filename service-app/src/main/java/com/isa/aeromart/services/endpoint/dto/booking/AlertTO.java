package com.isa.aeromart.services.endpoint.dto.booking;

public class AlertTO {

	private int alertId;

	private String content;

	public int getAlertId() {
		return alertId;
	}

	public void setAlertId(int alertId) {
		this.alertId = alertId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
