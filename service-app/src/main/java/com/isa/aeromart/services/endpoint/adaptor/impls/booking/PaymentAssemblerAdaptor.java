package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.webplatform.api.v2.util.PaxPaymentUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;

public class PaymentAssemblerAdaptor implements Adaptor<MakePaymentRQ, LCCClientPaymentAssembler> {

	private BookingSessionStore bookingSessionStore;

	private PaxPaymentUtil paxPayUtil;

	private Passenger passenger;

	private List<LCCClientExternalChgDTO> externalCharges;

	private boolean isOnhold;

	private PayByVoucherInfo sessionPayByVoucherInfo;


	public PaymentAssemblerAdaptor(BookingSessionStore bookingSessionStore, PaxPaymentUtil paxPayUtil, Passenger passenger,
			List<LCCClientExternalChgDTO> externalCharges, boolean isOnhold, PayByVoucherInfo sessionPayByVoucherInfo) {
		this.bookingSessionStore = bookingSessionStore;
		this.paxPayUtil = paxPayUtil;
		this.passenger = passenger;
		this.externalCharges = externalCharges;
		this.isOnhold = isOnhold;
		this.sessionPayByVoucherInfo = sessionPayByVoucherInfo;
	}

	@Override
	public LCCClientPaymentAssembler adapt(MakePaymentRQ source) {

		PaymentAssemblerComposer paymentComposer = new PaymentAssemblerComposer(this.externalCharges);
		paymentComposer.getPaymentAssembler().setPaxType(passenger.getPaxType());
		LCCClientPaymentAssembler lccClientPaymentAssembler;
		if (isOnhold) {
			lccClientPaymentAssembler = paymentComposer.getPaymentAssembler();
		} else {
			PaymentOptionsAdaptor paymentOptionsAdaptor = new PaymentOptionsAdaptor(paymentComposer, paxPayUtil,
					bookingSessionStore, passenger, sessionPayByVoucherInfo);
			lccClientPaymentAssembler = paymentOptionsAdaptor.adapt(source.getPaymentOptions())
				.getPaymentAssembler();
		}
		return lccClientPaymentAssembler;
	}

}
