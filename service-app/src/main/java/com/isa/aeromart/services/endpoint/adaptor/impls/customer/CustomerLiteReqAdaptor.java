package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import java.util.HashSet;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.common.PhoneNumberReqAdaptor;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerSaveOrUpdateRQ;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;

public class CustomerLiteReqAdaptor implements Adaptor<CustomerSaveOrUpdateRQ, Customer> {

	@Override
	public Customer adapt(CustomerSaveOrUpdateRQ source) {
		Customer customer = new Customer();
		customer.setCountryCode(source.getCustomer().getCountryCode());
		customer.setNationalityCode(source.getCustomer().getNationalityCode());
		PhoneNumberReqAdaptor phoneNumberReqAdaptor = new PhoneNumberReqAdaptor();
		customer.setMobile(phoneNumberReqAdaptor.adapt(source.getCustomer().getCustomerContact().getMobileNumber()));
		if (source.getLmsDetails() != null) {
			LmsMember lmsMember = new LmsMember();
			Set<LmsMember> lmsMemberDetails = new HashSet<>();
			lmsMember.setFfid(source.getLmsDetails().getFfid());
			lmsMember.setDateOfBirth(source.getLmsDetails().getDateOfBirth());
			lmsMemberDetails.add(lmsMember);
			customer.setCustomerLmsDetails(lmsMemberDetails);
		}
		return customer;
	}

}
