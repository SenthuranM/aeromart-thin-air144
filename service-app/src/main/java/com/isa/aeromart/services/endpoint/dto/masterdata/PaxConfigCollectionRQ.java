package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class PaxConfigCollectionRQ extends TransactionalBaseRQ {

	private List<PaxDetailsConfigRQ> configDetailsRQs;

	public List<PaxDetailsConfigRQ> getConfigDetailsRQs() {
		return configDetailsRQs;
	}

	public void setConfigDetailsRQs(List<PaxDetailsConfigRQ> configDetailsRQs) {
		this.configDetailsRQs = configDetailsRQs;
	}

}
