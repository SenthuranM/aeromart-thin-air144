package com.isa.aeromart.services.endpoint.utils.masterdata;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

public class MasterDataUtil {
	
	private static Log log = LogFactory.getLog(MasterDataUtil.class);

	public static String createTermsNCondFromXML(String strXMLFilePath){
		StringBuffer strbData = new StringBuffer();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilderFactory.setCoalescing(true);
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(strXMLFilePath);

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("terms");
			int intRecords = listOfRecords.getLength();
			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				strbData.append(menuNode.getFirstChild().getNodeValue());
			}
		} catch (SAXParseException err) {
			log.error("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId(), err);
		} catch (Throwable t) {
			log.error(t);
		}
		return strbData.toString();
	}
	
}
