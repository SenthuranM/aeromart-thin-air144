package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class ReservationDescriptor {

    private List<Passenger> passengers;

    private List<Segment> segments;

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }
}
