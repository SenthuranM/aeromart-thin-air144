package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

import java.util.List;

/**
 * Created by degorov on 6/20/17.
 */
public class ComposePaymentDTO {
    private String pnr; 
    private LmsCredits lmsCredits;
	private VoucherOption voucherOption;
    private PaymentSessionStore paymentSessionStore;
    private String contactEmail;
    private List<Passenger> paxInfo; 
    private DiscountRQ discountRQ;
    private TrackInfoDTO trackInfoDTO;
    private String transactionId;
    private ServiceTaxRQ serviceTaxRQ;
    private boolean isBalancePayment;
    private PaymentOptionsRQ paymentOptionsRQ;
    

    public String getPnr() {
        return pnr;
    }

    public LmsCredits getLmsCredits() {
        return lmsCredits;
    }

    public PaymentSessionStore getPaymentSessionStore() {
        return paymentSessionStore;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public List<Passenger> getPaxInfo() {
        return paxInfo;
    }

    public DiscountRQ getDiscountRQ() {
        return discountRQ;
    }

    public TrackInfoDTO getTrackInfoDTO() {
        return trackInfoDTO;
    }

    public String getTransactionId() {
        return transactionId;
    }      

    public ServiceTaxRQ getServiceTaxRQ() {
		return serviceTaxRQ;
	}	

	public boolean isBalancePayment() {
		return isBalancePayment;
	}	

	public PaymentOptionsRQ getPaymentOptionsRQ() {
		return paymentOptionsRQ;
	}

	

	public ComposePaymentDTO() {}

    public ComposePaymentDTO setPnr(String pnr) {
        this.pnr = pnr;
        return this;
    }

    public ComposePaymentDTO setLmsCredits(LmsCredits lmsCredits) {
        this.lmsCredits = lmsCredits;
        return this;
    }

    public ComposePaymentDTO setPaymentSessionStore(PaymentSessionStore paymentSessionStore) {
        this.paymentSessionStore = paymentSessionStore;
        return this;
    }

    public ComposePaymentDTO setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
        return this;
    }

    public ComposePaymentDTO setPaxInfo(List<Passenger> paxInfo) {
        this.paxInfo = paxInfo;
        return this;
    }

    public ComposePaymentDTO setDiscountRQ(DiscountRQ discountRQ) {
        this.discountRQ = discountRQ;
        return this;
    }

    public ComposePaymentDTO setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
        this.trackInfoDTO = trackInfoDTO;
        return this;
    }

    public ComposePaymentDTO setTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }
    
    public ComposePaymentDTO setServiceTaxRQ(ServiceTaxRQ serviceTaxRQ) {
		this.serviceTaxRQ = serviceTaxRQ;
		return this;
	}
    
    public ComposePaymentDTO setBalancePayment(boolean isBalancePayment) {
		this.isBalancePayment = isBalancePayment;
		return this;
	}
    
    public ComposePaymentDTO setPaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) {
		this.paymentOptionsRQ = paymentOptionsRQ;
		return this;
	}

	public VoucherOption getVoucherOption() {
		return voucherOption;
	}

	public ComposePaymentDTO setVoucherOption(VoucherOption voucherOption) {
		this.voucherOption = voucherOption;
		return this;
	}
}
