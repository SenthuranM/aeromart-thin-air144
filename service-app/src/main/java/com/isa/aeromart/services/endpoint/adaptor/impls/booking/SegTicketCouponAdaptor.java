package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.booking.PaxSegmentTicketCoupons;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;

public class SegTicketCouponAdaptor implements Adaptor<LccClientPassengerEticketInfoTO, PaxSegmentTicketCoupons> {

	private RPHGenerator rphGenerator;

	public SegTicketCouponAdaptor(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;
	}

	@Override
	public PaxSegmentTicketCoupons adapt(LccClientPassengerEticketInfoTO eticketInfoTO) {

		PaxSegmentTicketCoupons paxSegmentTicketCoupons = new PaxSegmentTicketCoupons();

		paxSegmentTicketCoupons.setFlightSegmentRPH(rphGenerator.getRPH(eticketInfoTO.getFlightSegmentRef()));
		paxSegmentTicketCoupons.seteTicketNo(eticketInfoTO.getPaxETicketNo());
		paxSegmentTicketCoupons.seteTicketStatus(eticketInfoTO.getPaxETicketStatus());
		paxSegmentTicketCoupons.setExternalCouponControl(eticketInfoTO.getExternalCouponControl());
		paxSegmentTicketCoupons.setExternalCouponStatus(eticketInfoTO.getExternalCouponStatus());
		paxSegmentTicketCoupons.setExternalETicketNo(eticketInfoTO.getExternalPaxETicketNo());
		paxSegmentTicketCoupons.setPaxStatus(eticketInfoTO.getPaxStatus());

		if (eticketInfoTO.getExternalCouponNo() != null) {
			paxSegmentTicketCoupons.setExternalCouponNo(eticketInfoTO.getExternalCouponNo().toString());
		}

		if (eticketInfoTO.getExternalCouponNo() != null) {
			paxSegmentTicketCoupons.setCouponNo(eticketInfoTO.getCouponNo().toString());
		}

		return paxSegmentTicketCoupons;
	}

}
