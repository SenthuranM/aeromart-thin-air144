package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;

public class AncillaryQuotation {

	private SessionMetaData metaData;

	private List<AncillaryType> quotedAncillaries;

	public SessionMetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(SessionMetaData metaData) {
		this.metaData = metaData;
	}

	public List<AncillaryType> getQuotedAncillaries() {
		return quotedAncillaries;
	}

	public void setQuotedAncillaries(List<AncillaryType> quotedAncillaries) {
		this.quotedAncillaries = quotedAncillaries;
	}
}
