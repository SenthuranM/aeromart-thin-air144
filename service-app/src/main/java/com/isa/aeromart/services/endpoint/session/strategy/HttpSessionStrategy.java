package com.isa.aeromart.services.endpoint.session.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.session.TransactionFactory;

@Service("httpSessionStrategy")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class HttpSessionStrategy {

	@Autowired
	private TransactionFactory transactionFactory;

	public TransactionFactory getTransactionFactory(String transactionId) {
		return transactionFactory;
	}

	public void setTransactionFactory(TransactionFactory transactionFactory) {
		this.transactionFactory = transactionFactory;
	}
}
