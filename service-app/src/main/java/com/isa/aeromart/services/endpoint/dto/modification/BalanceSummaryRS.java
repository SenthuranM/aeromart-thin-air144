package com.isa.aeromart.services.endpoint.dto.modification;

import java.util.List;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;


public class BalanceSummaryRS extends TransactionalBaseRS {

	private List<PassengerSummary> paxList;

	private CommonSummaryInfo reservationBalanceInfo;

	public List<PassengerSummary> getPaxList() {
		return paxList;
	}

	public void setPaxList(List<PassengerSummary> paxList) {
		this.paxList = paxList;
	}

	public CommonSummaryInfo getReservationBalanceInfo() {
		return reservationBalanceInfo;
	}

	public void setReservationBalanceInfo(CommonSummaryInfo reservationBalanceInfo) {
		this.reservationBalanceInfo = reservationBalanceInfo;
	}

}
