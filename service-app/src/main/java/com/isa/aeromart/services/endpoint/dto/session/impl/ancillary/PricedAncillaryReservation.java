package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

public class PricedAncillaryReservation {

	private SessionMetaData metaData;

	private List<PricedAncillaryType> ancillaryTypes;

	private List<PricedAncillaryPassenger> ancillaryPassengers;

	private List<MonetaryAmendment> amendments;

	public SessionMetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(SessionMetaData metaData) {
		this.metaData = metaData;
	}

	public List<PricedAncillaryType> getAncillaryTypes() {
		return ancillaryTypes;
	}

	public void setAncillaryTypes(List<PricedAncillaryType> ancillaryTypes) {
		this.ancillaryTypes = ancillaryTypes;
	}

	public List<PricedAncillaryPassenger> getAncillaryPassengers() {
		return ancillaryPassengers;
	}

	public void setAncillaryPassengers(List<PricedAncillaryPassenger> ancillaryPassengers) {
		this.ancillaryPassengers = ancillaryPassengers;
	}

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}
}
