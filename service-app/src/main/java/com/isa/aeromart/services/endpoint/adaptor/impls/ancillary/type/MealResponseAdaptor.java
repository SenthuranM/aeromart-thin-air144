package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability.ApplicableType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Rule;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.meal.MealItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.meal.MealItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.BasicRuleValidation;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.MultipleSelections;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentMealsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class MealResponseAdaptor extends AncillariesResponseAdaptor {
	@Override
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability, LCCAncillaryAvailabilityInDTO.LCCAncillaryType.MEALS);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	@Override
	public AncillaryType toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		AncillaryType ancillaryType = new AncillaryType();
		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.MEAL.name());

		List<Provider> providers = new ArrayList<>();
		SystemProvided provider = new SystemProvided();
		provider.setProviderType(Provider.ProviderType.SYSTEM);

		List<AvailableAncillary> availableAncillaries = new ArrayList<AvailableAncillary>();
		AvailableAncillary availableAncillary = new AvailableAncillary();

		List<AvailableAncillaryUnit> availableUnits = new ArrayList<AvailableAncillaryUnit>();
		LCCMealResponceDTO source = (LCCMealResponceDTO) ancillaries;
		FlightSegmentTO flightSegmentTO;
		String flightSegmentRPH;

		AvailableAncillaryUnit availableAncillaryUnit;

		SegmentScope segmentScope;
		Applicability applicability;
		MealItems mealItems;
		List<MealItem> mealItemList;
		MealItem mealItem;
		List<Charge> charges;
		Charge charge;
		MultipleSelections multipleSelections;
		int ruleIndex = 0;
		List<Rule> rules = new ArrayList<>();
		boolean multiSelect = source.isMultipleMealSelectionEnabled();
		List<LCCFlightSegmentMealsDTO> lccFlightSegmentMealsDTOList = source.getFlightSegmentMeals();
		Collections.sort(lccFlightSegmentMealsDTOList);

		for (LCCFlightSegmentMealsDTO lccMealsDTO : lccFlightSegmentMealsDTOList) {

			if (lccMealsDTO.getMeals() != null && !lccMealsDTO.getMeals().isEmpty()) {
				// multiple select rule validation
				multipleSelections = new MultipleSelections();
				if (multiSelect) {
					multipleSelections.setMultipleSelect(lccMealsDTO.isMultiMealEnabledForLogicalCabinClass());
				} else {
					multipleSelections.setMultipleSelect(false);
				}
				multipleSelections.setRuleValidationId(++ruleIndex);
				multipleSelections.setApplicability(BasicRuleValidation.ApplicabilityCode.CABIN_CLASS_WISE);
				// since multiple selection doesn't has to show a message it should be empty
				// multipleSelections.setRuleMessage(ruleMessage);
				rules.add(multipleSelections);

				availableAncillaryUnit = new AvailableAncillaryUnit();

				flightSegmentTO = lccMealsDTO.getFlightSegmentTO();
				flightSegmentRPH = rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber());

				segmentScope = new SegmentScope();
				segmentScope.setScopeType(ScopeType.SEGMENT);
				segmentScope.setFlightSegmentRPH(flightSegmentRPH);
				availableAncillaryUnit.setScope(segmentScope);

				applicability = new Applicability();
				applicability.setApplicableType(ApplicableType.PASSENGER.name());
				availableAncillaryUnit.setApplicability(applicability);

				mealItems = new MealItems();
				mealItemList = new ArrayList<>();

				for (LCCMealDTO lccMealDTO : lccMealsDTO.getMeals()) {

					mealItem = new MealItem();
					mealItem.setValidations(new ArrayList<>());
					mealItem.setItemId(String.valueOf(lccMealDTO.getMealCode()));
					mealItem.setMealCategoryCode(lccMealDTO.getMealCategoryCode());
					mealItem.setMealImagePath(lccMealDTO.getMealImagePath());
					mealItem.setMealThumbnailImagePath(lccMealDTO.getMealThumbnailImagePath());
					mealItem.setAvailable(lccMealDTO.getAvailableMeals());
					mealItem.setAllocated(lccMealDTO.getAllocatedMeals());
					mealItem.setDescription(lccMealDTO.getMealDescription());
					mealItem.setItemName(lccMealDTO.getMealName());
					mealItem.getValidations().add(multipleSelections.getRuleValidationId());
					mealItem.setPopularity(lccMealDTO.getPopularity());
					mealItem.setCategoryRestricted(lccMealDTO.isCategoryRestricted());

					charges = new ArrayList<>();
					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.PER_PASSENGER.name());
					charge.setAmount(lccMealDTO.getMealCharge());
					charges.add(charge);

					mealItem.setCharges(charges);
					mealItemList.add(mealItem);
				}

				mealItems.setItems(mealItemList);

				availableAncillaryUnit.setItemsGroup(mealItems);
				availableUnits.add(availableAncillaryUnit);
			}
		}

		availableAncillary.setAvailableUnits(availableUnits);
		availableAncillaries.add(availableAncillary);
		availableAncillary.setValidationDefinitions(rules);
		provider.setAvailableAncillaries(availableAncillary);
		providers.add(provider);
		ancillaryType.setProviders(providers);

		return ancillaryType;

	}

	@Override
	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {
		LCCMealDTO selectedMeal = new LCCMealDTO();
		selectedMeal.setMealCode(item.getId());
		selectedMeal.setAllocatedMeals(item.getQuantity());
		selectedMeal.setMealCharge(item.getAmount());

		return selectedMeal;
	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

	}
}
