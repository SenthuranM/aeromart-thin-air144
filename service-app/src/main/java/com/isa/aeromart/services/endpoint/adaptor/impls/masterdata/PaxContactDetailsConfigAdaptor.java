package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsField;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;

public class PaxContactDetailsConfigAdaptor implements Adaptor<IBEContactConfigDTO, PaxContactDetailsField>{

	@Override
	public PaxContactDetailsField adapt(IBEContactConfigDTO source) {
		
		PaxContactDetailsField paxContactDetailsField = new PaxContactDetailsField();
		
		paxContactDetailsField.setFieldName(source.getFieldName());
		paxContactDetailsField.setVisibility(source.isIbeVisibility());
		paxContactDetailsField.setMandatory(source.isMandatory());
		paxContactDetailsField.setValidation("");
		
		return paxContactDetailsField;
		
	}

}
