package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class PayfortCheckRQ extends TransactionalBaseRQ {

	private String pnr;
	private String transactionFee;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTransactionFee() {
		return transactionFee;
	}

	public void setTransactionFee(String transactionFee) {
		this.transactionFee = transactionFee;
	}

}
