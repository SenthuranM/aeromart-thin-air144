package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.LMSDetails;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;


public class LMSDetailsResAdaptor implements Adaptor<LmsMember, LMSDetails>{

	@Override
	public LMSDetails adapt(LmsMember lmsMember) {
		
		LMSDetails lmsDetails = new LMSDetails();
		
		lmsDetails.setDateOfBirth(lmsMember.getDateOfBirth());
		lmsDetails.setEmailStatus(String.valueOf(lmsMember.getEmailConfirmed()));
		lmsDetails.setFfid(lmsMember.getFfid());
		lmsDetails.setHeadOFEmailId(lmsMember.getHeadOFEmailId());
		lmsDetails.setLanguage(lmsMember.getLanguage());
		lmsDetails.setPassportNum(lmsMember.getPassportNum());
		lmsDetails.setRefferedEmail(lmsMember.getRefferedEmail());
		
		if(lmsMember.getEmailConfirmed()=='Y'){
	    	LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
	    	try {
				LoyaltyPointDetailsDTO existingMember = lmsManagementBD.getLoyaltyMemberPointBalances(lmsMember.getFfid(),
						lmsMember.getMemberExternalId());
				lmsDetails.setAvailablePoints(String.valueOf(existingMember.getMemberPointsAvailable()));
			} catch (ModuleException e) {
				// TODO handle exception log.error("Error in getLmsMemberDTO ", e);
			}
	    }
		
		return lmsDetails;
	}

}
