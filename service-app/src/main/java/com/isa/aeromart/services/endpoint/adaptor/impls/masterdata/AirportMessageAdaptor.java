package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessage;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;

public class AirportMessageAdaptor implements Adaptor<AirportMessageDisplayDTO, AirportMessage> {

	@Override
	public AirportMessage adapt(AirportMessageDisplayDTO source) {
		AirportMessage target = new AirportMessage();
		target.setAirportCode(source.getAirportCode());
		target.setAirportMessage(source.getAirportMessage());
		target.setAirportMsgId(source.getAirportMsgId());
		return target;
	}

}
