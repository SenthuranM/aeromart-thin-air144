package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PayfortCheckRS extends TransactionalBaseRS {
	private String voucherID;
	private String requestID;
	private boolean voucherAvailable;
	private String popupMessage;
	private boolean trackingNumberAvailable;

	public String getVoucherID() {
		return voucherID;
	}

	public void setVoucherID(String voucherID) {
		this.voucherID = voucherID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public boolean isVoucherAvailable() {
		return voucherAvailable;
	}

	public void setVoucherAvailable(boolean voucherAvailable) {
		this.voucherAvailable = voucherAvailable;
	}

	public String getPopupMessage() {
		return popupMessage;
	}

	public void setPopupMessage(String popupMessage) {
		this.popupMessage = popupMessage;
	}

	public boolean isTrackingNumberAvailable() {
		return trackingNumberAvailable;
	}

	public void setTrackingNumberAvailable(boolean trackingNumberAvailable) {
		this.trackingNumberAvailable = trackingNumberAvailable;
	}

}
