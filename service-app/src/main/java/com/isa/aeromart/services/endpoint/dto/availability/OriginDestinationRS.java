package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.ArrayList;
import java.util.List;

/*
 * Response for origin destination information request
 */

public class OriginDestinationRS {

	private String origin;

	private String destination;

	private int ondSequence;

	private List<AvailableOption> availableOptions;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public List<AvailableOption> getAvailableOptions() {
		if (this.availableOptions == null) {
			this.availableOptions = new ArrayList<AvailableOption>();
		}
		return availableOptions;
	}

	public void setAvailableOptions(List<AvailableOption> availableOptions) {
		this.availableOptions = availableOptions;
	}

}
