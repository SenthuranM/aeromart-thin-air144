package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.List;

public class FlightCalendarSelectedRS extends FlightCalendarRS {

	private SelectedFlightPricing selectedFlightPricing;
	private String currency;
	private List<String> paymentOptions;
	private List<FareClass> fareClasses;

	public List<FareClass> getFareClasses() {
		return fareClasses;
	}

	public void setFareClasses(List<FareClass> fareClass) {
		this.fareClasses = fareClass;
	}

	public FlightCalendarSelectedRS() {
	}

	public FlightCalendarSelectedRS(FlightCalendarRS flightCalendarResponse) {
		super(flightCalendarResponse);
	}

	public SelectedFlightPricing getSelectedFlightPricing() {
		return selectedFlightPricing;
	}

	public void setSelectedFlightPricing(SelectedFlightPricing selectedFlightPricing) {
		this.selectedFlightPricing = selectedFlightPricing;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<String> getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(List<String> paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

}
