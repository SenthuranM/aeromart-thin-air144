package com.isa.aeromart.services.endpoint.adaptor.utils;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;

public class VoucherPaymentDTOAdapter implements Adaptor<VoucherDTO, VoucherPaymentDTO> {

	@Override
	public VoucherPaymentDTO adapt(VoucherDTO vdto) {
		VoucherPaymentDTO voucherPaymentDTO = new VoucherPaymentDTO();
		voucherPaymentDTO.setAmountLocal(vdto.getAmountInLocal());
		voucherPaymentDTO.setCardCVV(vdto.getVoucherPaymentDTO().getCardCVV());
		voucherPaymentDTO.setCardExpiry(vdto.getVoucherPaymentDTO().getCardExpiry());
		voucherPaymentDTO.setCardHolderName(vdto.getVoucherPaymentDTO().getCardHolderName());
		voucherPaymentDTO.setCardNumber(vdto.getVoucherPaymentDTO().getCardNumber());
		voucherPaymentDTO.setCardTxnFeeLocal(vdto.getVoucherPaymentDTO().getCardTxnFeeLocal());
		voucherPaymentDTO.setCardType(vdto.getVoucherPaymentDTO().getCardType());
		voucherPaymentDTO.setIpgId(vdto.getVoucherPaymentDTO().getIpgId());
		voucherPaymentDTO.setPaymentMethod(vdto.getVoucherPaymentDTO().getPaymentMethod());
		return voucherPaymentDTO;
	}
}

