package com.isa.aeromart.services.endpoint.adaptor.utils;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.voucher.GiftVoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;

public class VoucherDTOAdapter implements Adaptor<GiftVoucherDTO, VoucherDTO> {

	@Override
	public VoucherDTO adapt(GiftVoucherDTO vdto) {
		VoucherDTO voucherDTO = new VoucherDTO();
		voucherDTO.setAmountInBase(vdto.getAmountInBase());
		voucherDTO.setAmountInLocal(vdto.getAmountInLocal());
		voucherDTO.setCurrencyCode(vdto.getCurrencyCode());
		voucherDTO.setEmail(vdto.getEmail());
		voucherDTO.setMobileNumber(vdto.getMobileNumber());
		voucherDTO.setPaxFirstName(vdto.getPaxFirstName());
		voucherDTO.setPaxLastName(vdto.getPaxLastName());
		voucherDTO.setRemarks(vdto.getRemarks());
		voucherDTO.setTemplateId(vdto.getTemplateId());
		voucherDTO.setValidFrom(vdto.getValidFrom());
		voucherDTO.setValidTo(vdto.getValidTo());
		voucherDTO.setVoucherGroupId(vdto.getVoucherGroupId());
		if (vdto.getVoucherPaymentDTO() != null) {
			voucherDTO.setVoucherPaymentDTO(vdto.getVoucherPaymentDTO().clone());
			voucherDTO.getVoucherPaymentDTO().setAmountLocal(vdto.getAmountInLocal());
			voucherDTO.getVoucherPaymentDTO().setAmount(vdto.getAmountInBase());
		}
		voucherDTO.setVoucherId(vdto.getVoucherId());
		return voucherDTO;
	}
}

