package com.isa.aeromart.services.endpoint.delegate.ancillary.bl.promotion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.AnciPassengerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.MonetaryAmendment;
import com.isa.aeromart.services.endpoint.dto.ancillary.Passenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.PaxChargesTO;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class AncillaryFreeServicePromotionImpl implements AncillaryPromotion {

	private TransactionalAncillaryService transactionalAncillaryService;
	
	private String transactionId;
	
	private TrackInfoDTO trackInfoDTO;
	
	@Override
	public void setTransactionalAncillaryService(TransactionalAncillaryService transactionalAncillaryService) {
		this.transactionalAncillaryService = transactionalAncillaryService;
	}

	@Override
	public void setTransactionalId(String transactionId) {
		this.transactionId = transactionId;		
	}
	

	@Override
	public void setTrackInfo(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;		
	}
	
	@Override
	public ReservationDiscountDTO calculatePromotion() throws ModuleException {
		DiscountRQ discountRQ;
		DiscountedFareDetails discountedFareDetails = transactionalAncillaryService.getDiscountedFareDetails(transactionId);
		List<Passenger> paxInfo = AnciCommon.toPassengers(transactionalAncillaryService.getReservationData(transactionId).getPassengers());
		
		discountRQ = buildDiscountRQ(paxInfo, discountedFareDetails);

		ReservationDiscountDTO resDiscountDTO = ReservationModuleUtils.getAirproxyReservationBD().calculatePromotionDiscount(discountRQ,
				trackInfoDTO);
		if(resDiscountDTO != null && resDiscountDTO.getDiscountAs() == null){
			resDiscountDTO.setDiscountAs(discountRQ.getDiscountInfoDTO().getDiscountAs());
		}
		
		return resDiscountDTO;
		
	}

	@Override
	public PriceQuoteAncillaryRS applyPromotion(PriceQuoteAncillaryRS priceQuoteAncillaryRS, ReservationDiscountDTO resDiscountDTO, Integer definitionId) {
		MonetaryAmendment monetaryAmendment;
		Integer passengerRPH;
		PaxDiscountDetailTO paxDiscountDetailTO;
		BigDecimal totalAnciDiscount;
		for(AncillaryPassenger ancillaryPassenger : priceQuoteAncillaryRS.getAncillaryReservation().getPassengers()){
			totalAnciDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
			passengerRPH = Integer.parseInt(ancillaryPassenger.getPassengerRph());
			if(resDiscountDTO.getPaxDiscountDetails().containsKey(passengerRPH)){
				paxDiscountDetailTO = resDiscountDTO.getPaxDiscountDetails().get(passengerRPH);
				for(DiscountChargeTO discountChargeTO : paxDiscountDetailTO.getPaxDiscountChargeTOs()){
					totalAnciDiscount = AccelAeroCalculator.add(totalAnciDiscount, discountChargeTO.getDiscountAmount());
				}
				for(DiscountChargeTO discountChargeTO : paxDiscountDetailTO.getInfantDiscountChargeTOs()){
					totalAnciDiscount = AccelAeroCalculator.add(totalAnciDiscount, discountChargeTO.getDiscountAmount());
				}
				if(AccelAeroCalculator.isGreaterThan(totalAnciDiscount, AccelAeroCalculator.getDefaultBigDecimalZero())){
					monetaryAmendment = new MonetaryAmendment();
					monetaryAmendment.setDefinitionId(definitionId);
					monetaryAmendment.setAmount(totalAnciDiscount);
					if(ancillaryPassenger.getAmendments() == null){
						ancillaryPassenger.setAmendments(new ArrayList<MonetaryAmendment>());
					}
					ancillaryPassenger.getAmendments().add(monetaryAmendment);
				}
			}
		}
		return priceQuoteAncillaryRS;
	}

	public DiscountRQ getDiscountCalculatorRQ(Collection<PaxChargesTO> paxChargesList,
			DiscountedFareDetails discountedFareDetails, PreferenceInfo preferenceInfo, String transactionIdentifier) {
		DiscountRQ discountRQ = null;
		IPaxCountAssembler paxAssm;
		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();
		QuotedFareRebuildDTO quotedFareRebuildDTO;

		TravellerQuantity travellerQuantity = transactionalAncillaryService.getTravellerQuantityFromSession(transactionId);
		FareSegChargeTO fareSegChargeTO = transactionalAncillaryService.getPricingInformation(transactionId);
		
		if (discountedFareDetails != null && paxChargesList != null && !paxChargesList.isEmpty()) {

			DISCOUNT_METHOD discountMethod = null;
			if (discountedFareDetails.getPromotionId() == null) {
				if (discountedFareDetails.getDomesticSegmentCodeList().size() > 0
						&& AppSysParamsUtil.isDomesticFareDiscountEnabled()) {
					discountMethod = DISCOUNT_METHOD.DOM_FARE_DISCOUNT;

				} else if (discountedFareDetails.getFarePercentage() > 0) {
					discountMethod = DISCOUNT_METHOD.FARE_DISCOUNT;

				}
			} else {
				discountMethod = DISCOUNT_METHOD.PROMOTION;
			}

			
//			setFareDiscountWith(paxQtyList, applicablePromotionDetailsTO, newBinDiscountedFareDetails);
			fareSegChargeTOs.add(fareSegChargeTO);

			paxAssm = getIPaxCountAssembler(travellerQuantity);

			quotedFareRebuildDTO = new QuotedFareRebuildDTO(fareSegChargeTOs, paxAssm,
					discountedFareDetails, preferenceInfo.getSeatType());

			discountRQ = new DiscountRQ(discountMethod);
			discountRQ.setCalculateTotalOnly(true);
			discountRQ.setPaxQtyList(getPaxQtyList(travellerQuantity));
			discountRQ.setQuotedFareRebuildDTO(quotedFareRebuildDTO);			
			discountRQ.setDiscountInfoDTO(discountedFareDetails);
			discountRQ.setPaxChargesList(paxChargesList);
			discountRQ.setSystem(ProxyConstants.SYSTEM.getEnum(preferenceInfo.getSelectedSystem()));
			discountRQ.setTransactionIdentifier(transactionIdentifier);

		}

		return discountRQ;

	}
	
	public List<PassengerTypeQuantityTO> getPaxQtyList(TravellerQuantity travellerQuantity) {

		List<PassengerTypeQuantityTO> paxQtyList = new ArrayList<PassengerTypeQuantityTO>();

		PassengerTypeQuantityTO adultPassenger = new PassengerTypeQuantityTO();
		adultPassenger.setPassengerType(PaxTypeTO.ADULT);
		adultPassenger.setQuantity(travellerQuantity.getAdultCount());
		paxQtyList.add(adultPassenger);

		PassengerTypeQuantityTO childPassenger = new PassengerTypeQuantityTO();
		childPassenger.setPassengerType(PaxTypeTO.CHILD);
		childPassenger.setQuantity(travellerQuantity.getChildCount());
		paxQtyList.add(childPassenger);

		PassengerTypeQuantityTO infantPassenger = new PassengerTypeQuantityTO();
		infantPassenger.setPassengerType(PaxTypeTO.INFANT);
		infantPassenger.setQuantity(travellerQuantity.getInfantCount());
		paxQtyList.add(infantPassenger);

		return paxQtyList;
	}
	
	public IPaxCountAssembler getIPaxCountAssembler(TravellerQuantity travellerQuantity) {
		IPaxCountAssembler paxAssm = new PaxCountAssembler(travellerQuantity.getAdultCount(), travellerQuantity.getChildCount(),
				travellerQuantity.getInfantCount());
		return paxAssm;
	}
	
	private DiscountRQ buildDiscountRQ(List<Passenger> paxInfo, DiscountedFareDetails discountedFareDetails) {

		
		Collection<ReservationPaxTO> reservationPaxTOCollection = new ArrayList<ReservationPaxTO>();
		
		PreferenceInfo preferenceInfo = transactionalAncillaryService.getPreferencesForAncillary(transactionId);
		List<PassengerChargeTo<LCCClientExternalChgDTO>> anciSessionPaxList = transactionalAncillaryService.getPaxAncillaryExternalCharges(transactionId);
	   
		if (anciSessionPaxList != null && anciSessionPaxList.size() > 0) {
			AdaptorUtils.adaptCollection(anciSessionPaxList, reservationPaxTOCollection, new AnciPassengerAdaptor(null));
		}

		Collection<PaxChargesTO> paxChargesList = transformPaxList(reservationPaxTOCollection, paxInfo);

		DiscountRQ discountRQ = getDiscountCalculatorRQ(paxChargesList, discountedFareDetails,
				preferenceInfo, transactionId);

		return discountRQ;
	}
	
	public Collection<PaxChargesTO> transformPaxList(Collection<ReservationPaxTO> reservationPaxTOCollection,
			List<Passenger> paxInfo) {

		Collection<PaxChargesTO> paxExtChargesList = new ArrayList<PaxChargesTO>();
		int paxSequence;
		
		if (reservationPaxTOCollection != null && !reservationPaxTOCollection.isEmpty()) {

			for (ReservationPaxTO resPaxTO : reservationPaxTOCollection) {

				for (Passenger passenger : paxInfo) {
					paxSequence = Integer.parseInt(passenger.getPassengerRph());
					if (Integer.parseInt(passenger.getPassengerRph()) == resPaxTO.getSeqNumber()) {

						PaxChargesTO paxExternalChargesTO = new PaxChargesTO();

						String paxType = passenger.getType();
						boolean isParent = isParent(paxInfo, paxSequence);
						if (passenger.getTravelWith() > 0) {
							continue;
						}

						if (PaxTypeTO.PARENT.equals(paxType)) {
							paxType = PaxTypeTO.ADULT;
						}

						paxExternalChargesTO.setPaxTypeCode(paxType);
						paxExternalChargesTO.setPaxSequence(paxSequence);
						paxExternalChargesTO.setExtChgList(resPaxTO.getExternalCharges());
						paxExternalChargesTO.setParent(isParent);

						paxExtChargesList.add(paxExternalChargesTO);

					}
				}

			}
		}

		return paxExtChargesList;
	}
	
	public static boolean isParent(List<Passenger> passengers, int paxSequenceID) {
		boolean isParent = false;
		for (Passenger passenger : passengers) {
			if (passenger.getTravelWith() == paxSequenceID) {
				isParent = true;
				break;
			}
		}
		return isParent;
	}

}
