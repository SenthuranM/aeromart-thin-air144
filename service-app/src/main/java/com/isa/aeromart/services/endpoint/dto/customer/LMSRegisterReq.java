package com.isa.aeromart.services.endpoint.dto.customer;

public class LMSRegisterReq {

	private String customerID;
    
    private LMSDetails lmsDetails;
       
    private boolean registration;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public LMSDetails getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LMSDetails lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean isRegistration() {
		return registration;
	}

	public void setRegistration(boolean registration) {
		this.registration = registration;
	}

}
