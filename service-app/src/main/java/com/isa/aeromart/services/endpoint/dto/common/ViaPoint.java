package com.isa.aeromart.services.endpoint.dto.common;

public class ViaPoint {

	private String airportCode;
	
	private String duration;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}


}
