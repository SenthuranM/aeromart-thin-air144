package com.isa.aeromart.services.endpoint.dto.ancillary.internal;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;

public class AncillaryReservationTo {

    private MetaData metaData;

    private AncillaryReservation ancillaryReservation;

    public void mergeAncillaryReservationTo(AncillaryReservationTo ancillaryReservationTo) {

    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public AncillaryReservation getAncillaryReservation() {
        return ancillaryReservation;
    }

    public void setAncillaryReservation(AncillaryReservation ancillaryReservation) {
        this.ancillaryReservation = ancillaryReservation;
    }
}
