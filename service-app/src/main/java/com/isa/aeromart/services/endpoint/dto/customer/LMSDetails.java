package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Date;

public class LMSDetails {

	private String availablePoints;

	private Date dateOfBirth;

	private String ffid;

	private String emailStatus;

	private String headOFEmailId;

	private String language;

	private String passportNumber;

	private String passportNum;
	
	private String refferedEmail;
	
	private String appCode;


	public String getAvailablePoints() {
		return availablePoints;
	}

	public void setAvailablePoints(String availablePoints) {
		this.availablePoints = availablePoints;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getHeadOFEmailId() {
		return headOFEmailId;
	}

	public void setHeadOFEmailId(String headOFEmailId) {
		this.headOFEmailId = headOFEmailId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public String getRefferedEmail() {
		return refferedEmail;
	}

	public void setRefferedEmail(String refferedEmail) {
		this.refferedEmail = refferedEmail;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getPassportNum() {
		return passportNum;
	}

	public void setPassportNum(String passportNum) {
		this.passportNum = passportNum;
	}
	
}

