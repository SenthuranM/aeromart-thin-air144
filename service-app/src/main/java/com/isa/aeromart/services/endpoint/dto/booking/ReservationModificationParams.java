package com.isa.aeromart.services.endpoint.dto.booking;

public class ReservationModificationParams {

	private Boolean cancellable;

	private Boolean modifiable;

	public Boolean getCancellable() {
		return cancellable;
	}

	public void setCancellable(Boolean cancellable) {
		this.cancellable = cancellable;
	}

	public Boolean getModifiable() {
		return modifiable;
	}

	public void setModifiable(Boolean modifiable) {
		this.modifiable = modifiable;
	}
}
