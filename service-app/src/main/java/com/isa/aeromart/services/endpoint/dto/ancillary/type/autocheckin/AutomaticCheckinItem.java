package com.isa.aeromart.services.endpoint.dto.ancillary.type.autocheckin;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;

/**
 * @author aravinth.r
 *
 */
public class AutomaticCheckinItem extends AncillaryItem {

	private Charge chargePerPax;

	private Boolean isavailable;

	/**
	 * @return the chargePerPax
	 */
	public Charge getChargePerPax() {
		return chargePerPax;
	}

	/**
	 * @param chargePerPax
	 *            the chargePerPax to set
	 */
	public void setChargePerPax(Charge chargePerPax) {
		this.chargePerPax = chargePerPax;
	}

	/**
	 * @return the isavailable
	 */
	public Boolean getIsavailable() {
		return isavailable;
	}

	/**
	 * @param isavailable
	 *            the isavailable to set
	 */
	public void setIsavailable(Boolean isavailable) {
		this.isavailable = isavailable;
	}

}
