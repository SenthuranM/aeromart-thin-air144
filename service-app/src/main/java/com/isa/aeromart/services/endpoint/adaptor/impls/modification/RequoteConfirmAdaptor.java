package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.FlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.modification.RequoteBalanceInfo;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.aeromart.services.endpoint.utils.payment.PaymentUtils;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.*;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaxPaymentUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;

import java.math.BigDecimal;
import java.util.*;

public class RequoteConfirmAdaptor {

	public static RequoteModifyRQ getRequoteModifyRQ(RequoteSessionStore session, TrackInfoDTO trackInfo,
			List<LCCClientReservationInsurance> reservationInsurances, List<ReservationPaxTO> anciIntegrtedPaxList)
			throws ModuleException {

		RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
		ReservationInfo resInfo = session.getResInfo();
		PreferenceInfo prefInfo = session.getPreferences();
		PostPaymentDTO postPayDTO = session.getRequotePostPaymentInformation();
		requoteModifyRQ.setSystem(SYSTEM.AA);
		requoteModifyRQ.setPnr(resInfo.getPnr());
		requoteModifyRQ.setVersion(resInfo.getVersion());
		requoteModifyRQ.setContactInfo(resInfo.getContactInfo());

		if (prefInfo.getSelectedSystem() == SYSTEM.INT.toString()) {
			requoteModifyRQ.setSystem(SYSTEM.INT);
			requoteModifyRQ.setTransactionIdentifier(resInfo.getTransactionId());
			requoteModifyRQ.setCarrierWiseFlightRPHMap(ModificationUtils.getCarrierWiseRPHs(session.getOndInfo()));
			requoteModifyRQ.setGroupPnr(resInfo.getPnr());
			requoteModifyRQ.setVersion(AdaptorUtils.getCorrectInterlineVersion(requoteModifyRQ.getVersion()));
		}

		FlightPriceRQ flightPriceRQ = ModificationAdaptor.createFlightPriceRQ(resInfo.getPaxTypeWiseCount(),
				prefInfo.getSeatType());

		QuotedFareRebuildDTO fareInfo = null;
		if (!session.getRequoteBalanceReqiredParams().isCancelReservation()) {
			fareInfo = new QuotedFareRebuildDTO(session.getFareInfo().getFareSegmentChargeTO(), flightPriceRQ, null);
		}
		requoteModifyRQ.setFareInfo(fareInfo);
		requoteModifyRQ.setLastFareQuoteDate(session.getFareInfo().getLastFareQuotedDate());
		requoteModifyRQ.setFQWithinValidity(session.getFareInfo().isFqWithinValidity());
		requoteModifyRQ.setRequoteSegmentMap(ModificationUtils.getReQuoteResSegmentMap(session.getOndInfo()));
		requoteModifyRQ.setRemovedSegmentIds(session.getRequoteBalanceReqiredParams().getRemovedReservationSegIds());

		if (AppSysParamsUtil.isLMSEnabled())
			requoteModifyRQ.setRemainingLoyaltyPoints(CustomerUtil.getRemainingLoyaltypoints(resInfo.getCustomerId()));

		requoteModifyRQ.setOndFareTypeByFareIdMap(ModificationUtils.getONDFareTypeByFareIdMap(session.getOndInfo()));
		requoteModifyRQ.setOldFareIdByFltSegIdMap(ModificationUtils.getOldFareIdByFltSegIdMap(session.getOndInfo()));

		if (reservationInsurances != null) {
			requoteModifyRQ.setInsurances(ModificationUtils.populateInsurance(reservationInsurances, resInfo.getPnr(),
					resInfo.getPaxTypeWiseCount().getAdultCount(), trackInfo));
		}

		List<ReservationPaxTO> paxListWithExtChgs = ModificationUtils.getPaxDetailsWithAnciModification(
				session.getFinancialStore(), session.getRequoteBalanceInfo().getPassengerSummaryList());
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		if (paxListWithExtChgs != null && !paxListWithExtChgs.isEmpty()) {
			for (ReservationPaxTO resPax : paxListWithExtChgs) {
				List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
				extChgList.addAll(resPax.getExternalCharges());
				paxExtChgMap.put(resPax.getSeqNumber(), extChgList);
			}
		}

		requoteModifyRQ.setPaxExtChgMap(paxExtChgMap);

		requoteModifyRQ.setLastCurrencyCode(resInfo.getSelectedCurrency());
		requoteModifyRQ.setReservationStatus(resInfo.getReservationStatus());
		// requoteModifyRQ.setExcludedSegFarePnrSegIds(resInfo.getExcludedSegFarePnrSegIds());

		RequoteBalanceInfo requoteBalanceInfo = session.getRequoteBalanceInfo();
		if (requoteBalanceInfo.getBalanceToPay().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);
			requoteModifyRQ.setLccTemporaryTnxMap(postPayDTO.getTemporyPaymentMap());
			requoteModifyRQ.setActualPayment(true);

			IPGResponseDTO creditInfo = postPayDTO.getIpgResponseDTO();
			IPGIdentificationParamsDTO ipgDTO = postPayDTO.getIpgRequestDTO() != null
					? postPayDTO.getIpgRequestDTO().getIpgIdentificationParamsDTO()
					: null;

			if (session.getRequoteBalanceReqiredParams().isAnciModification()) {
				if (anciIntegrtedPaxList != null) {
					mergeReservationAnciPax(paxListWithExtChgs, anciIntegrtedPaxList);
					List<FlightSegmentTO> fltSegTOs = new ArrayList<FlightSegmentTO>();
					AdaptorUtils.adaptCollection(session.getSegmentWithInventory(), fltSegTOs, new FlightSegmentAdaptor());
					if (fltSegTOs != null || fltSegTOs.size() != 0) {
						SSRServicesUtil.setSegmentSeqForSSR(paxListWithExtChgs, fltSegTOs, fltSegTOs.size());
					}
				}
			}
			// poplate service Tax related Info
			paxListWithExtChgs = ModificationUtils.includePassangerExternalCharges(session.getResInfo().getPaxList(),
					paxListWithExtChgs);
			if (session.getFinancialStore().getPaymentExternalCharges() != null
					&& !session.getFinancialStore().getPaymentExternalCharges().getPassengers().isEmpty()) {
				boolean paxWiseServiceTaxForCCFeeExits = false;
				for (PassengerChargeTo<LCCClientExternalChgDTO> paxCharge : session.getFinancialStore()
						.getPaymentExternalCharges().getPassengers()) {
					for (ReservationPaxTO pax : paxListWithExtChgs) {
						if (pax.getSeqNumber().toString().equals(paxCharge.getPassengerRph())
								&& pax.getExternalCharges() != null) {
							for (LCCClientExternalChgDTO extCharge : paxCharge.getGetPassengerCharges()) {
								pax.addExternalCharges(extCharge);
								paxWiseServiceTaxForCCFeeExits = true;
							}

						}
					}
				}
				if (paxWiseServiceTaxForCCFeeExits) {
					if (!session.getRequotePostPaymentInformation().getChargeCodeWiseServiceTaxExtCharges().isEmpty()) {
						ServiceTaxExtCharges serviceTaxExtCharges = new ServiceTaxExtCharges();
						serviceTaxExtCharges.setChgGrpCode(ReservationInternalConstants.ChargeGroup.TAX);
						serviceTaxExtCharges.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
						for (String chargeCode : session.getRequotePostPaymentInformation()
								.getChargeCodeWiseServiceTaxExtCharges().keySet()) {
							serviceTaxExtCharges.addServiceTaxes(session.getRequotePostPaymentInformation()
									.getChargeCodeWiseServiceTaxExtCharges().get(chargeCode));

						}
						requoteModifyRQ.getExternalChargesMap().put(EXTERNAL_CHARGES.SERVICE_TAX, serviceTaxExtCharges);
					}
				}
			}
			requoteModifyRQ
					.setExternalChargesMap(getExternalChargesMap(paxListWithExtChgs, postPayDTO.getSelectedExternalCharges()));

			CommonCreditCardPaymentInfo cardPaymentInfo = PaymentUtils.getCommonCreditCardPaymentInfo(
					postPayDTO.getTemporyPaymentMap(), creditInfo == null ? false : creditInfo.isSuccess());

			// loyaltyAgent, loyaltyAccount parse actual loyalty info
			requoteModifyRQ.setLccPassengerPayments(getPaxPaymentMapWithTravelRefNo(postPayDTO, resInfo, creditInfo, ipgDTO,
					cardPaymentInfo != null ? cardPaymentInfo.getTxnDateTime() : null, null, null,
					cardPaymentInfo != null ? cardPaymentInfo.getPayCurrencyDTO() : null, null,
					postPayDTO.getPaymentGateWay() != null ? postPayDTO.getPaymentGateWay().getBrokerType() : null,
					cardPaymentInfo, requoteBalanceInfo.getPassengerSummaryList(),
					session.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo(), session.getPayByVoucherInfo()));

		} else {
			requoteModifyRQ.setPaxWiseAdjustmentAmountMap(requoteBalanceInfo.getPaxWiseAdjustmentAmountMap());
			requoteModifyRQ.setNoBalanceToPay(true);
			requoteModifyRQ.setExternalChargesMap(getExternalChargesMap(paxListWithExtChgs,
					new HashMap<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO>()));
			addExtChargesWithDummyPayments(requoteModifyRQ, paxListWithExtChgs);
			requoteModifyRQ.setPaymentType(
					ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE);
		}
		requoteModifyRQ.setCarrierWiseLoyaltyPaymentInfo(session.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo());
		if (session.getRequoteBalanceReqiredParams().isNameChange()) {
			requoteModifyRQ.setNameChangedPaxMap(session.getNameChangedPaxMap());
		}

		return requoteModifyRQ;
	}

	private static Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap(List<ReservationPaxTO> paxList,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> extPaymentChgMap) throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();

		for (ReservationPaxTO pax : paxList) {
			for (LCCClientExternalChgDTO extChg : pax.getExternalCharges()) {
				colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
			}
		}

		Set<EXTERNAL_CHARGES> tmpKeySet = extPaymentChgMap.keySet();
		for (EXTERNAL_CHARGES key : tmpKeySet) {
			colEXTERNAL_CHARGES.add(key);
		}

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap = AirproxyModuleUtils.getReservationBD()
				.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MODIFY_ONLY);
		return extExternalChgDTOMap;
	}

	private static void addExtChargesWithDummyPayments(RequoteModifyRQ requoteModifyRQ, List<ReservationPaxTO> resPaxTos) {
		if (resPaxTos != null && !resPaxTos.isEmpty()) {
			for (ReservationPaxTO resPaxTo : resPaxTos) {
				if (!resPaxTo.getExternalCharges().isEmpty()) {
					LCCClientPaymentAssembler payAsm = new LCCClientPaymentAssembler();
					for (LCCClientExternalChgDTO externalCharge : resPaxTo.getExternalCharges()) {
						externalCharge.setAmountConsumedForPayment(true);
					}
					payAsm.getPerPaxExternalCharges().addAll(resPaxTo.getExternalCharges());
					payAsm.addCashPayment(BigDecimal.ZERO, null, new Date(), null, null, null, null);
					requoteModifyRQ.getLccPassengerPayments().put(resPaxTo.getTravelerRefNumber(), payAsm);
				}
			}
		}
	}

	private static Map<String, LCCClientPaymentAssembler> getPaxPaymentMapWithTravelRefNo(PostPaymentDTO postPayDTO,
			ReservationInfo resInfo, IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO, Date paymentTimestamp,
			String loyaltyAgentCode, String loyaltyAccount, PayCurrencyDTO payCurrencyDTO, String lccUniqueKey, String brokerType,
			CommonCreditCardPaymentInfo cardPaymentInfo, Collection<LCCClientPassengerSummaryTO> passengerSummaryList,
			Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments, PayByVoucherInfo payByVoucherInfo)
			throws ModuleException {

		Map<String, LCCClientPaymentAssembler> passengerPaymentMap = new HashMap<String, LCCClientPaymentAssembler>();
		Date currentDatetime = new Date();

		List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) passengerSummaryList;
		Collection<ReservationPaxTO> paxList = resInfo.getPaxList();

		int adultChildCount = 0;
		int infantCount = 0;
		boolean isInfantPaymentSeparated = resInfo.isInfantPaymentSeparated();
		for (LCCClientPassengerSummaryTO lccClientPassengerSummaryTO : colPassengerSummary) {
			BigDecimal amountDue = lccClientPassengerSummaryTO.getTotalAmountDue();
			if (!PaxTypeTO.INFANT.equals(lccClientPassengerSummaryTO.getPaxType())
					&& amountDue.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				adultChildCount++;
			} else if (isInfantPaymentSeparated && PaxTypeTO.INFANT.equals(lccClientPassengerSummaryTO.getPaxType())
					&& amountDue.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				infantCount++;
			}
		}

		boolean isCCPayment = !postPayDTO.hasNoCCPayment() && !postPayDTO.isNoPay();
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
				postPayDTO.getSelectedExternalCharges(), isCCPayment, false);
		externalChargesMediator.setCalculateJNTaxForCCCharge(true);
		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(adultChildCount,
				infantCount);

		Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
				.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);

		String loyaltyMemberAccountId = null;
		String[] rewardIDs = null;

		BigDecimal voucherRedeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (payByVoucherInfo != null) {
			voucherRedeemedAmount = voucherRedeemedAmount.add(payByVoucherInfo.getRedeemedTotal());
		}

		BigDecimal loyaltyPayAmount = postPayDTO.getLoyaltyCredit();
		boolean isExtCardPay = true;
		if (brokerType != null
				&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)
				&& cardPaymentInfo != null) {
			isExtCardPay = false;
		}
		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {

			PaymentAssemblerComposer paymentComposer = null;
			List<LCCClientExternalChgDTO> extCharges = new ArrayList<LCCClientExternalChgDTO>();

			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT) || isInfantPaymentSeparated) {
				BigDecimal amountDue = AccelAeroCalculator.add(paxSummaryTo.getTotalCreditAmount().negate(),
						paxSummaryTo.getTotalAmountDue());

				for (ReservationPaxTO pax : paxList) {
					if (PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()) == pax.getSeqNumber()) {
						BigDecimal paxAmountDue = paxSummaryTo.getTotalAmountDue();
						if (paxAmountDue.compareTo(BigDecimal.ZERO) > 0) {
							if (perPaxExternalCharges.size() > 0) {
								extCharges = PaymentUtils
										.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
							}
						}
						extCharges.addAll(pax.getExternalCharges());
						break;
					}
				}

				paymentComposer = new PaymentAssemblerComposer(extCharges);
				if (!postPayDTO.isNoPay()) {

					Integer paxSequence = PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber());
					BigDecimal lmsMemberPayment = reconcileLmsPayAmount(paxWiseRedeemedAmounts.get(paxSequence), amountDue);

					BigDecimal totalWithAncillary = amountDue;
					BigDecimal totalExtChgs = getTotalExtCharge(extCharges);

					// deduct external charge amount from amount due
					List<EXTERNAL_CHARGES> skipCharges = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
					skipCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
					skipCharges.add(EXTERNAL_CHARGES.JN_OTHER);
					skipCharges.add(EXTERNAL_CHARGES.SERVICE_TAX);
					amountDue = AccelAeroCalculator.subtract(amountDue, getTotalExtCharge(extCharges, skipCharges));
					totalWithAncillary = AccelAeroCalculator.add(amountDue, totalExtChgs);

					PaxPaymentUtil paxPayUtil = new PaxPaymentUtil(totalWithAncillary, totalExtChgs, loyaltyPayAmount,
							lmsMemberPayment, voucherRedeemedAmount, AccelAeroCalculator.getDefaultBigDecimalZero());

					loyaltyPayAmount = paxPayUtil.getRemainingLoyalty();
					while (paxPayUtil.hasConsumablePayments()) {
						PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
						BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
						if (payMethod == PaymentMethod.LMS) {
							for (String operatingCarrierCode : carrierWiseLoyaltyPayments.keySet()) {
								LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = carrierWiseLoyaltyPayments
										.get(operatingCarrierCode);
								Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
										.getPaxProductPaymentBreakdown();
								BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil
										.getPaxRedeemedTotal(carrierPaxProductRedeemed, paxSequence);

								loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
								rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
								if (payCurrencyDTO != null) {
									payCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
								}
								if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
									paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs,
											carrierPaxProductRedeemed, carrierLmsMemberPayment, payCurrencyDTO, currentDatetime,
											operatingCarrierCode);
								}
							}
						} else if (payMethod == PaymentMethod.VOUCHER) {
							if (payByVoucherInfo != null) {
								paymentComposer.addVoucherPayment(payByVoucherInfo, paxPayAmt, payCurrencyDTO,
										currentDatetime);
								voucherRedeemedAmount = paxPayUtil.getRemainingVoucherAmount();
							}
						} else {
							if (isExtCardPay) {
								paymentComposer.addCreditCardPayment(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
										paymentTimestamp, lccUniqueKey);
							} else {
								paymentComposer.addCreditCardPaymentInternal(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
										paymentTimestamp, cardPaymentInfo.getNo(), cardPaymentInfo.geteDate(),
										cardPaymentInfo.getSecurityCode(), cardPaymentInfo.getName(), lccUniqueKey);
							}
						}
					}
				}

				LCCClientPaymentAssembler payment = paymentComposer.getPaymentAssembler();

				if (PaxTypeTO.ADULT.equals(paxSummaryTo.getPaxType()) && paxSummaryTo.getInfantName() != null
						&& !paxSummaryTo.getInfantName().equals("")) {
					payment.setPaxType(PaxTypeTO.PARENT);
				} else {
					payment.setPaxType(paxSummaryTo.getPaxType());
				}
				passengerPaymentMap.put(paxSummaryTo.getTravelerRefNumber(), payment);
			}
		}
		return passengerPaymentMap;
	}

	private static BigDecimal reconcileLmsPayAmount(BigDecimal lmsPayAmount, BigDecimal amountDue) {
		if ((lmsPayAmount != null && amountDue != null) && (lmsPayAmount.equals(amountDue) || AccelAeroCalculator
				.subtract(amountDue, lmsPayAmount).abs().compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0)) {
			return amountDue;
		}
		return lmsPayAmount;
	}

	private static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	private static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList, List<EXTERNAL_CHARGES> skipCharges) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			if (skipCharges == null || (skipCharges != null && !skipCharges.contains(chg.getExternalCharges()))) {
				total = AccelAeroCalculator.add(total, chg.getAmount());
			}
		}
		return total;
	}

	private static void mergeReservationAnciPax(List<ReservationPaxTO> externalChargesIntegratedPaxList,
			List<ReservationPaxTO> anciIntegratedPaxList) {
		for (ReservationPaxTO extChgPax : externalChargesIntegratedPaxList) {
			for (ReservationPaxTO anciPax : anciIntegratedPaxList) {
				if (extChgPax.getSeqNumber() == anciPax.getSeqNumber()) {
					for (LCCSelectedSegmentAncillaryDTO anciDTO : anciPax.getSelectedAncillaries()) {
						extChgPax.addSelectedAncillaries(anciDTO);
					}
				}
			}
		}
	}
}
