package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

public class OnAccountCredits {
	private String agentCode;// makePayReq
	private String userId;// makePayReq
	private String accountNo;// makePayReq
	private String authorizationCode;// makePayReq
	private BigDecimal amount;// makePayReq

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
