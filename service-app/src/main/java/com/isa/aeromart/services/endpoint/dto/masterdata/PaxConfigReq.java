package com.isa.aeromart.services.endpoint.dto.masterdata;

import com.isa.aeromart.services.endpoint.dto.common.BaseRQ;

public class PaxConfigReq extends BaseRQ {
	
	private String origin;
	
	private String destination;
	
	private String appCode;
	
	private String carriers;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getCarriers() {
		return carriers;
	}

	public void setCarriers(String carriers) {
		this.carriers = carriers;
	}

}
