package com.isa.aeromart.services.endpoint.dto.masterdata;

public class AirportMessage {

	private Integer airportMsgId;
	private String airportCode;
	private String airportMessage;

	public Integer getAirportMsgId() {
		return airportMsgId;
	}

	public void setAirportMsgId(Integer airportMsgId) {
		this.airportMsgId = airportMsgId;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

}
