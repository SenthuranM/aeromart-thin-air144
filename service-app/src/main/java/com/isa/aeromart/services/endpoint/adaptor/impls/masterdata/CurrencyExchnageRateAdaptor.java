package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.dto.CurrencyExchangeRateDTO;

public class CurrencyExchnageRateAdaptor implements Adaptor<CurrencyExchangeRateDTO, CurrencyExchangeRate>{

	@Override
	public CurrencyExchangeRate adapt(CurrencyExchangeRateDTO source) {
		
		CurrencyExchangeRate currencyExchangeRate = new CurrencyExchangeRate();
		
		currencyExchangeRate.setBaseToCurrExRate(source.getBaseToCurrExRate());
		currencyExchangeRate.setCurrencyCode(source.getCurrencyCode());
		currencyExchangeRate.setDecimalPlaces(source.getDecimalPlaces());
		
		return currencyExchangeRate;
		
	}

}
