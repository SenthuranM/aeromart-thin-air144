package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AvailableAnci;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;

public class MealRequestAdaptor  extends AncillaryRequestAdaptor {
    @Override
    public String getLCCAncillaryType() {
        return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.MEALS;
    }

    @Override
	public Object toAvailableAncillaries(TrackInfoDTO trackInfo, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {
        AvailableMealRequestWrapper wrapper = new AvailableMealRequestWrapper();

        List<FlightSegmentTO> filteredFlightSegmentTOs = new ArrayList<>();
	    List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
	    flightSegmentTOs = AnciCommon.populateAnciOfferData(flightSegmentTOs, pricingInformation, travellerQuantity);
	    
	    try {
			for(FlightSegmentTO flightSegmentTO : flightSegmentTOs){
				if(!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())){
					filteredFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.ANCI_AVAILABLE_GENERIC_ERROR, e);
		}

        wrapper.setLccMealRequestDTOs(AvailableAnci.composeMealRequest(filteredFlightSegmentTOs, lccTransactionId != null ? lccTransactionId : ""));
        wrapper.setTransactionIdentifier(isModifyAnci? null : lccTransactionId != null ? lccTransactionId : "");
        wrapper.setBundledFareDTOs(pricingInformation.getOndBundledFareDTOs());
        wrapper.setSystem(system);
        wrapper.setSelectedLanguage(LocaleContextHolder.getLocale().toString());
        wrapper.setTrackInfo(trackInfo);
        wrapper.setPnr(pnr);
		wrapper.setAppEngine(CommonServiceUtil.getApplicationEngine(trackInfo.getAppIndicator()));

        return wrapper;
    }

	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {

		List<SelectedItem> selectedItems;
		SegmentScope segmentScope;
		String flightSegmentRPH;

		LCCMealDTO selectedMeal;
		List<LCCMealDTO> selectedMeals;
		Map<String, List<LCCMealDTO>> selectedMealsMapping = new HashMap<>();

		for(Preference preference : preferences){
			List<Selection> selections = preference.getSelections();
			for(Selection selection : selections){
				segmentScope = (SegmentScope) selection.getScope();
				flightSegmentRPH = segmentScope.getFlightSegmentRPH();

				if (!selectedMealsMapping.containsKey(flightSegmentRPH)) {
					selectedMealsMapping.put(flightSegmentRPH, new ArrayList<>());
				}

				selectedMeals = selectedMealsMapping.get(flightSegmentRPH);

				selectedItems = selection.getSelectedItems();
				for(SelectedItem selectedItem : selectedItems){
					selectedMeal = new LCCMealDTO();
					selectedMeal.setAllocatedMeals(selectedItem.getQuantity());
					selectedMeal.setMealCode(selectedItem.getId());
					selectedMeals.add(selectedMeal);
				}

			}
		}

		return selectedMealsMapping;

	}

	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {

		return null;

	}

	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;

	}

	public class AvailableMealRequestWrapper {
        List<LCCMealRequestDTO> lccMealRequestDTOs;
        String transactionIdentifier;
        List<BundledFareDTO> bundledFareDTOs;
        ProxyConstants.SYSTEM system;
        String selectedLanguage;
        BasicTrackInfo trackInfo;
        ApplicationEngine appEngine;
        String pnr;

        public List<LCCMealRequestDTO> getLccMealRequestDTOs() {
            return lccMealRequestDTOs;
        }

        public void setLccMealRequestDTOs(List<LCCMealRequestDTO> lccMealRequestDTOs) {
            this.lccMealRequestDTOs = lccMealRequestDTOs;
        }

        public String getTransactionIdentifier() {
            return transactionIdentifier;
        }

        public void setTransactionIdentifier(String transactionIdentifier) {
            this.transactionIdentifier = transactionIdentifier;
        }

        public List<BundledFareDTO> getBundledFareDTOs() {
            return bundledFareDTOs;
        }

        public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
            this.bundledFareDTOs = bundledFareDTOs;
        }

        public ProxyConstants.SYSTEM getSystem() {
            return system;
        }

        public void setSystem(ProxyConstants.SYSTEM system) {
            this.system = system;
        }

        public String getSelectedLanguage() {
            return selectedLanguage;
        }

        public void setSelectedLanguage(String selectedLanguage) {
            this.selectedLanguage = selectedLanguage;
        }

        public BasicTrackInfo getTrackInfo() {
            return trackInfo;
        }

        public void setTrackInfo(BasicTrackInfo trackInfo) {
            this.trackInfo = trackInfo;
        }

        public ApplicationEngine getAppEngine() {
            return appEngine;
        }

        public void setAppEngine(ApplicationEngine appEngine) {
            this.appEngine = appEngine;
        }

        public String getPnr() {
            return pnr;
        }

        public void setPnr(String pnr) {
            this.pnr = pnr;
        }
    }
}
