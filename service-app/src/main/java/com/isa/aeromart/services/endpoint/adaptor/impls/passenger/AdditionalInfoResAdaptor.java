package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.AdditionalInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;

public class AdditionalInfoResAdaptor implements Adaptor<LCCClientReservationAdditionalPax, AdditionalInfo> {

	@Override
	public AdditionalInfo adapt(LCCClientReservationAdditionalPax lccClientReservationAdditionalPax) {
		AdditionalInfo additionalInfo = new AdditionalInfo();
		additionalInfo.setFfid(lccClientReservationAdditionalPax.getFfid());
		additionalInfo.setNic(lccClientReservationAdditionalPax.getNationalIDNo());

		FoidInfoResAdaptor foidInfoResAdaptor = new FoidInfoResAdaptor();
		additionalInfo.setFoidInfo(foidInfoResAdaptor.adapt(lccClientReservationAdditionalPax));

		DocInfoResAdaptor docInfoResAdaptor = new DocInfoResAdaptor();
		additionalInfo.setDocInfo(docInfoResAdaptor.adapt(lccClientReservationAdditionalPax));

		return additionalInfo;
	}

}
