package com.isa.aeromart.services.endpoint.dto.session.impl.financial;

import java.util.ArrayList;
import java.util.List;

public class ReservationChargeTo<T> {

	private List<T> reservationCharges;

	private List<PassengerChargeTo<T>> passengers;

	public ReservationChargeTo() {
		reservationCharges = new ArrayList<T>();
		passengers = new ArrayList<PassengerChargeTo<T>>();
	}

	public List<T> getReservationCharges() {
		return reservationCharges;
	}

	public void setReservationCharges(List<T> reservationCharges) {
		this.reservationCharges = reservationCharges;
	}

	public List<PassengerChargeTo<T>> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<PassengerChargeTo<T>> passengers) {
		this.passengers = passengers;
	}
}
