package com.isa.aeromart.services.endpoint.dto.ancillary.scope;

import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;

public class SegmentScope extends Scope {

    private String flightSegmentRPH;

    public SegmentScope() {
        setScopeType(ScopeType.SEGMENT);
    }

    public String getFlightSegmentRPH() {
        return flightSegmentRPH;
    }

    public void setFlightSegmentRPH(String flightSegmentRPH) {
        this.flightSegmentRPH = flightSegmentRPH;
    }
    
    @Override
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (getClass() != obj.getClass()) {
	        return false;
	    }
	    final SegmentScope other = (SegmentScope) obj;

	    if ((this.getFlightSegmentRPH() == null) ? (other.getFlightSegmentRPH() != null) : !this.getFlightSegmentRPH().equals(other.getFlightSegmentRPH())) {
	        return false;
	    }
	    return true;
	}
    
    @Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + (this.flightSegmentRPH != null ? this.flightSegmentRPH.hashCode() : 0);
	    return hash;
	}
}
