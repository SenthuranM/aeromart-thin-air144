package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.SegmentDetails;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;

public class SegmentDetailsAdaptor implements Adaptor<FlightInfoTO , SegmentDetails >{

	@Override
	public SegmentDetails adapt(FlightInfoTO flightInfoTO) {
		
		SegmentDetails segmentDetails = new SegmentDetails();
		
		String date = flightInfoTO.getArrivalDate();
		String time = flightInfoTO.getArrivalTime();
		
		segmentDetails.setArrivalDateTime(date + " " + time);
		
		date = flightInfoTO.getDepartureDate();
		time = flightInfoTO.getDepartureTime();
		
		segmentDetails.setDepartureDateTime(date + " " + time);
		
		segmentDetails.setOrignNDest(flightInfoTO.getOrignNDest());
		segmentDetails.setStatus(flightInfoTO.getStatus());
		
		return segmentDetails;
	}

}
