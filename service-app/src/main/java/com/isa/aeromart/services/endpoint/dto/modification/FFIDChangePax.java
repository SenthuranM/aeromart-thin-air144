package com.isa.aeromart.services.endpoint.dto.modification;

public class FFIDChangePax {

	private int paxSeqNo;

	private String ffid;

	public int getPaxSeqNo() {
		return paxSeqNo;
	}

	public void setPaxSeqNo(int paxSeqNo) {
		this.paxSeqNo = paxSeqNo;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}
}
