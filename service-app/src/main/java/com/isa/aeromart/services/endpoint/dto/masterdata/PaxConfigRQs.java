package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class PaxConfigRQs extends TransactionalBaseRQ {

	private List<PaxConfigRQ> paxConfigRQs;

	public List<PaxConfigRQ> getPaxConfigRQs() {
		return paxConfigRQs;
	}

	public void setPaxConfigRQs(List<PaxConfigRQ> paxConfigRQs) {
		this.paxConfigRQs = paxConfigRQs;
	}

}
