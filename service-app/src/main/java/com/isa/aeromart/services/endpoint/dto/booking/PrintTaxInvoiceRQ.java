package com.isa.aeromart.services.endpoint.dto.booking;

public class PrintTaxInvoiceRQ extends TaxInvoiceRQ {

	private String viewMode;

	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}

}
