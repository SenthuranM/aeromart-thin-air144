package com.isa.aeromart.services.endpoint.dto.common;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.TravelModeAdaptor.TravelMode;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.JourneyType;

public class FlightSegment {

	private DateTime departureDateTime;

	private DateTime arrivalDateTime;

	// Departure terminal code
	private String departureTerminal;

	// Arrival terminal code
	private String arrivalTerminal;

	private String carrierCode;

	// carrier code of operating carrier
	private String operatingCarrier;

	// domestic journey or international journey
	private JourneyType journeyType;

	// segment duration
	private String duration;

	private TravelMode travelMode;

	private String filghtDesignator;

	// equipment model eg: A320-211
	private String equipmentModelNumber;

	private String equipmentModelInfo;

	private String flightSegmentRPH;

	private String segmentCode;

	private String routeRefNumber;
	
	private String originCountryCode;

	public DateTime getDepartureDateTime() {
		if (departureDateTime == null) {
			departureDateTime = new DateTime();
		}
		return departureDateTime;
	}

	public void setDepartureDateTime(DateTime departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public DateTime getArrivalDateTime() {
		if (arrivalDateTime == null) {
			arrivalDateTime = new DateTime();
		}
		return arrivalDateTime;
	}

	public void setArrivalDateTime(DateTime arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getDepartureTerminal() {
		return departureTerminal;
	}

	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	public String getArrivalTerminal() {
		return arrivalTerminal;
	}

	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public JourneyType getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(JourneyType journeyType) {
		this.journeyType = journeyType;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public TravelMode getTravelMode() {
		return travelMode;
	}

	public void setTravelMode(TravelMode travelMode) {
		this.travelMode = travelMode;
	}

	public String getFilghtDesignator() {
		return filghtDesignator;
	}

	public void setFilghtDesignator(String filghtDesignator) {
		this.filghtDesignator = filghtDesignator;
	}

	public String getEquipmentModelNumber() {
		return equipmentModelNumber;
	}

	public void setEquipmentModelNumber(String equipmentModelNumber) {
		this.equipmentModelNumber = equipmentModelNumber;
	}

	public String getEquipmentModelInfo() {
		return equipmentModelInfo;
	}

	public void setEquipmentModelInfo(String equipmentModelInfo) {
		this.equipmentModelInfo = equipmentModelInfo;
	}

	public String getFlightSegmentRPH() {
		return flightSegmentRPH;
	}

	public void setFlightSegmentRPH(String segmentRPH) {
		this.flightSegmentRPH = segmentRPH;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getRouteRefNumber() {
		return routeRefNumber;
	}

	public void setRouteRefNumber(String routeRefNumber) {
		this.routeRefNumber = routeRefNumber;
	}
	
	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}


}
