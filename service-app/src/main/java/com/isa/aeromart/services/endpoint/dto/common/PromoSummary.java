package com.isa.aeromart.services.endpoint.dto.common;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class PromoSummary extends Promotion {

	private String notes;

	private String discountType;

	@CurrencyValue
	private BigDecimal discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String discountAs;

	private String discountCode;

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getDiscountAs() {
		return discountAs;
	}

	public void setDiscountAs(String discountAs) {
		this.discountAs = discountAs;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

}
