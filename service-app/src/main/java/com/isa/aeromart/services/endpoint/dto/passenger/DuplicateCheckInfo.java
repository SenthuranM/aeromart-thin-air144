package com.isa.aeromart.services.endpoint.dto.passenger;

public class DuplicateCheckInfo {

	private String title;

	private String firstName;

	private String lastName;

	private String ffid;

	private String foidType;

	private String foidNumber;

	private String nationality;

	private String paxType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public String getFoidType() {
		return foidType;
	}

	public void setFoidType(String foidType) {
		this.foidType = foidType;
	}

	public String getFoidNumber() {
		return foidNumber;
	}

	public void setFoidNumber(String foidNumber) {
		this.foidNumber = foidNumber;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

}
