package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class DuplicateCheckInfoAdapter implements Adaptor<Passenger, LCCClientReservationPax> {

	@Override
	public LCCClientReservationPax adapt(Passenger passenger) {

		LCCClientReservationPax lccClientReservationPax = new LCCClientReservationPax();

		lccClientReservationPax.setFirstName(passenger.getFirstName());
		lccClientReservationPax.setLastName(passenger.getLastName());
		lccClientReservationPax.setNationalityCode(passenger.getNationality());
		lccClientReservationPax.setPaxType(passenger.getPaxType());
		lccClientReservationPax.setTitle(passenger.getTitle());
		if (passenger.getAdditionalInfo() != null) {
			LCCClientReservationAdditionalPax lccClientReservationAdditionalPax = new LCCClientReservationAdditionalPax();
			lccClientReservationAdditionalPax.setFfid(passenger.getAdditionalInfo().getFfid());
			if (passenger.getAdditionalInfo().getFoidInfo() != null) {
				lccClientReservationAdditionalPax.setPassportNo(passenger.getAdditionalInfo().getFoidInfo().getFoidNumber());
			}
			lccClientReservationPax.setLccClientAdditionPax(lccClientReservationAdditionalPax);
		}
		return lccClientReservationPax;
	}

}
