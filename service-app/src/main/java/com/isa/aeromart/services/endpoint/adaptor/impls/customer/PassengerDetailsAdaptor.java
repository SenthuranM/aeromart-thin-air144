package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;
import com.isa.aeromart.services.endpoint.dto.customer.PassengerDetails;
import com.isa.thinair.aircustomer.api.model.Customer;

public class PassengerDetailsAdaptor implements Adaptor<Customer, PassengerDetails>{

	@Override
	public PassengerDetails adapt(Customer customerModel) {
		
		PassengerDetails passengerDetails = new PassengerDetails();
		String[] mobile;
		PhoneNumber mobileNo = new PhoneNumber();
		
		passengerDetails.setTitle(customerModel.getTitle());
		passengerDetails.setFirstName(customerModel.getFirstName());
		passengerDetails.setLastName(customerModel.getLastName());
		passengerDetails.setEmailId(customerModel.getEmailId());		
		passengerDetails.setDateOfBirth(customerModel.getDateOfBirth());
		mobile = customerModel.getMobile().split("-");
		mobileNo.setCountryCode(mobile[0]);
		mobileNo.setAreaCode(mobile[1]);
		mobileNo.setNumber(mobile[2]);
		passengerDetails.setMobile(mobileNo);
		passengerDetails.setNationality(customerModel.getNationalityCode().toString());
		passengerDetails.setCountry(customerModel.getCountryCode());
		
		return passengerDetails;
	}

	
	
}
