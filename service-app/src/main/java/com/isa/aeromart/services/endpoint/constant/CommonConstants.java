package com.isa.aeromart.services.endpoint.constant;

public class CommonConstants {

	public enum JourneyType {
		DOMESTIC, INTERNATIONAL
	}

	public enum FareClassType {
		FLEXI("F"), BUNDLE("B"), DEFAULT("D");
		private String code;

		FareClassType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public enum PromoType {
		PROMO_CODE, RETURN_DISCOUNT
	}

	public enum ReservationFlow {
		CREATE_RESERVATION, MODIFY_RESERVATION, MODIFY_ANCILLARIES
	}
	
	public final static String XML_FILE_PATH = "/templates/web/";

	public enum AirportMessageStages {
		SEARCH_RESULT("SEARCH_RESULT"), CARD_PAYMENT("CARD_PAYMENT"), ITINERARY("ITINERARY");
		private String code;

		AirportMessageStages(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

	}

	public enum AirportMessageSalesChannel {
		XBE("3"), IBE("4"), BOTH("5");
		private String code;

		AirportMessageSalesChannel(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

}
