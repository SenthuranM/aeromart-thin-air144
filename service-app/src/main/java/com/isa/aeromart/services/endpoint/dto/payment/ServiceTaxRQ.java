package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ServiceTaxRQ  extends TransactionalBaseRQ {

	private String countryCode;
	private String stateCode;
	private String taxRegNo;
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getTaxRegNo() {
		return taxRegNo;
	}
	public void setTaxRegNo(String taxRegNo) {
		this.taxRegNo = taxRegNo;
	}
	
}
