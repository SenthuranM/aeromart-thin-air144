package com.isa.aeromart.services.endpoint.utils.common;

import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class RPHBuilder {

	private final static String UNDERSCORE = "_";

	/**
	 * Return the fare class code for fare classes in new ibe
	 */
	public static String FareClassCodeBuilder(LogicalCabinClassInfoTO fareClassTo) {
		if (fareClassTo.getBundledFarePeriodId() != null) {
			return fareClassTo.getLogicalCCCode() + UNDERSCORE + FareClassType.BUNDLE.getCode() + UNDERSCORE
					+ fareClassTo.getBundledFarePeriodId();
		} else if (fareClassTo.isWithFlexi()) {
			return fareClassTo.getLogicalCCCode() + UNDERSCORE + FareClassType.FLEXI.getCode();
		}
		return fareClassTo.getLogicalCCCode() + UNDERSCORE + FareClassType.DEFAULT.getCode();
	}

	public static String FareClassCodeBuilder(FlightFareSummaryTO fareSummaryTo) {
		if (fareSummaryTo.getBundledFarePeriodId() != null) {
			return fareSummaryTo.getLogicalCCCode() + UNDERSCORE + FareClassType.BUNDLE.getCode() + UNDERSCORE
					+ fareSummaryTo.getBundledFarePeriodId();
		} else if (fareSummaryTo.isWithFlexi()) {
			return fareSummaryTo.getLogicalCCCode() + UNDERSCORE + FareClassType.FLEXI.getCode();
		}
		return fareSummaryTo.getLogicalCCCode() + UNDERSCORE + FareClassType.DEFAULT.getCode();
	}

	public static String PassengerRPHBuilder(int paxSequence, String paxType, int travelWith) {
		String travelerReference = "";
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			travelerReference = "A" + paxSequence;
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			travelerReference = "C" + paxSequence;
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			travelerReference = "I" + paxSequence + "/A" + travelWith;
		}
		return travelerReference + "$";
	}
}
