package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Date;


public class LoggedInCustomer {

	private String title;

	private String firstName;

	private String lastName;

	private Date dateOfBirth;

	private String gender;

	private boolean loyaltyAccountExist;

	private String loyaltyAccountNumber;

	private int nationalityCode;

	private String profilePicUrl;

	private int socialCustomerTypeId;

	private String socialSiteCustId;

	private CustomerContact customerContactDetails;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLoyaltyAccountNumber() {
		return loyaltyAccountNumber;
	}

	public void setLoyaltyAccountNumber(String loyaltyAccountNumber) {
		this.loyaltyAccountNumber = loyaltyAccountNumber;
	}

	public int getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public int getSocialCustomerTypeId() {
		return socialCustomerTypeId;
	}

	public void setSocialCustomerTypeId(int socialCustomerTypeId) {
		this.socialCustomerTypeId = socialCustomerTypeId;
	}

	public String getSocialSiteCustId() {
		return socialSiteCustId;
	}

	public void setSocialSiteCustId(String socialSiteCustId) {
		this.socialSiteCustId = socialSiteCustId;
	}

	public CustomerContact getCustomerContactDetails() {
		return customerContactDetails;
	}

	public void setCustomerContactDetails(CustomerContact customerContactDetails) {
		this.customerContactDetails = customerContactDetails;
	}

	public boolean isLoyaltyAccountExist() {
		return loyaltyAccountExist;
	}

	public void setLoyaltyAccountExist(boolean loyaltyAccountExist) {
		this.loyaltyAccountExist = loyaltyAccountExist;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
