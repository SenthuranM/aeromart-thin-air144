package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public abstract class AncillaryItemComposite extends AncillaryItemComponent {

	private List<AncillaryItemComponent> ancillaryItems;

	public List<AncillaryItemComponent> getAncillaryItems() {
		return ancillaryItems;
	}

	public void setAncillaryItems(List<AncillaryItemComponent> ancillaryItems) {
		this.ancillaryItems = ancillaryItems;
	}
}
