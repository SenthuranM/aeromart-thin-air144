package com.isa.aeromart.services.endpoint.dto.ancillary;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AirportTransferInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AutoCheckinInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.InFlightSsrInput;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "ancillaryType", visible = true)
@JsonSubTypes({
		@JsonSubTypes.Type(value = InFlightSsrInput.class, name = AncillariesConstants.Type.SSR_IN_FLIGHT),
		@JsonSubTypes.Type(value = AirportTransferInput.class, name = AncillariesConstants.Type.AIRPORT_TRANSFER),
		@JsonSubTypes.Type(value = AutoCheckinInput.class, name = AncillariesConstants.Type.AUTOMATIC_CHECKIN)
})
public abstract class AncillaryInput {
	
    private String ancillaryType;

    public String getAncillaryType() {
        return ancillaryType;
    }

    public void setAncillaryType(String ancillaryType) {
        this.ancillaryType = ancillaryType;
    }
}
