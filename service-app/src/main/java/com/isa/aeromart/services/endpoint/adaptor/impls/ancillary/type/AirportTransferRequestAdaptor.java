package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public class AirportTransferRequestAdaptor extends AncillaryRequestAdaptor{

	@Override
	public String getLCCAncillaryType() {
		return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AIRPORT_TRANSFER;
	}

	@Override
	public Object toAvailableAncillaries(TrackInfoDTO trackInfo, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {
		AvailableAirportTransferRequestWrapper wrapper = new AvailableAirportTransferRequestWrapper();

        String txnId = lccTransactionId != null ? lccTransactionId : "";
		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
		Collections.sort(flightSegmentTOs);

		wrapper.setFlightSegmentTOs(flightSegmentTOs);
		wrapper.setAppIndicator(trackInfo.getAppIndicator());
		wrapper.setModifyAnci(isModifyAnci);
		wrapper.setServiceCount(serviceCount == null ? new HashMap<>() : serviceCount);
		wrapper.setSystem(system);

		return wrapper;
	}

	@Override
	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {
		List<SelectedItem> selectedItems;
		AirportScope airportScope;
		String flightSegmentRPH;
		String airportCode;
		String applyOn;
		boolean alreadyExists;

		LCCAirportServiceDTO selectedAirportTransfer;
		List<LCCAirportServiceDTO> selectedAirportTransfers, airportWiseSelectedAirportTransfer;
		Map<AirportScope, List<LCCAirportServiceDTO>> airportWiseSelectedAirportTransferMapping = new HashMap<>();
		Map<String, List<LCCAirportServiceDTO>> selectedAirportTransferMapping = new HashMap<>();

		for (Preference preference : preferences) {
			List<Selection> selections = preference.getSelections();
			for (Selection selection : selections) {
				airportScope = (AirportScope) selection.getScope();
				flightSegmentRPH = airportScope.getFlightSegmentRPH();
				airportCode = airportScope.getAirportCode();
				applyOn = airportScope.getPoint();
				
				if (!airportWiseSelectedAirportTransferMapping.containsKey(airportScope)) {
					airportWiseSelectedAirportTransferMapping.put(airportScope, new ArrayList<>());
				}

				airportWiseSelectedAirportTransfer = airportWiseSelectedAirportTransferMapping.get(airportScope);

				selectedItems = selection.getSelectedItems();
				for (SelectedItem selectedItem : selectedItems) {
					alreadyExists = false;
					selectedAirportTransfer = new LCCAirportServiceDTO();
					selectedAirportTransfer.setAirportTransferId(Integer.parseInt(selectedItem.getId()));
					selectedAirportTransfer.setAirportCode(airportCode);
					selectedAirportTransfer.setApplyOn(applyOn);
					if (airportWiseSelectedAirportTransfer.isEmpty()) {
						airportWiseSelectedAirportTransfer.add(selectedAirportTransfer);
					} else {
						for (LCCAirportServiceDTO lccAirportServiceDTO : airportWiseSelectedAirportTransfer) {
							if (lccAirportServiceDTO.getAirportTransferId().equals(selectedItem.getId())) {
								alreadyExists = true;
							}
						}
						if (!alreadyExists) {
							airportWiseSelectedAirportTransfer.add(selectedAirportTransfer);
						}
					}
				}

			}
		}
		if (!airportWiseSelectedAirportTransferMapping.isEmpty()) {
			for (AirportScope airportScopeRef : airportWiseSelectedAirportTransferMapping.keySet()) {
				if (!selectedAirportTransferMapping.containsKey(airportScopeRef.getFlightSegmentRPH())) {
					selectedAirportTransferMapping.put(airportScopeRef.getFlightSegmentRPH(), new ArrayList<>());
				}
				selectedAirportTransfers = selectedAirportTransferMapping.get(airportScopeRef.getFlightSegmentRPH());
				selectedAirportTransfers.addAll(airportWiseSelectedAirportTransferMapping.get(airportScopeRef));
			}

		}

		return selectedAirportTransferMapping;
	}

	@Override
	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;

	}

	@Override
	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;

	}

	public class AvailableAirportTransferRequestWrapper {

		 private List<FlightSegmentTO> flightSegmentTOs;
		 private AppIndicatorEnum appIndicator;
		 private ProxyConstants.SYSTEM system;
		 private boolean isModifyAnci;
		 private Map<String, Integer> serviceCount;
		 
		public List<FlightSegmentTO> getFlightSegmentTOs() {
			return flightSegmentTOs;
		}
		public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
			this.flightSegmentTOs = flightSegmentTOs;
		}
		public AppIndicatorEnum getAppIndicator() {
			return appIndicator;
		}
		public void setAppIndicator(AppIndicatorEnum appIndicator) {
			this.appIndicator = appIndicator;
		}
		public ProxyConstants.SYSTEM getSystem() {
			return system;
		}
		public void setSystem(ProxyConstants.SYSTEM system) {
			this.system = system;
		}
		public boolean isModifyAnci() {
			return isModifyAnci;
		}
		public void setModifyAnci(boolean isModifyAnci) {
			this.isModifyAnci = isModifyAnci;
		}
		public Map<String, Integer> getServiceCount() {
			return serviceCount;
		}
		public void setServiceCount(Map<String, Integer> serviceCount) {
			this.serviceCount = serviceCount;
		}
	        
	}

}
