package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.AirportTransferRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.AirportTransferResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.AutomaticCheckinRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.AutomaticCheckinResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.BaggageRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.BaggageResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.FlexibilityRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.FlexibilityResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.InsuranseRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.InsuranseResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.MealRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.MealResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SeatRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SeatResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrAirportServiceRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrAirportServiceResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrInFlightServiceRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrInFlightServiceResponseAdaptor;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class AncillaryAdaptorFactory {

	private AncillaryAdaptorFactory() {
	}

	public static AncillaryRequestAdaptor getRequestAdaptor(String type) {
		AncillariesConstants.AncillaryType ancillaryType = AncillariesConstants.AncillaryType.valueOf(type);
		AncillaryRequestAdaptor adaptor = null;

		switch (ancillaryType) {
		case BAGGAGE:
			adaptor = new BaggageRequestAdaptor();
			break;
		case SEAT:
			adaptor = new SeatRequestAdaptor();
			break;
		case MEAL:
			adaptor = new MealRequestAdaptor();
			break;
		case INSURANCE:
			adaptor = new InsuranseRequestAdaptor();
			break;
		case SSR_IN_FLIGHT:
			adaptor = new SsrInFlightServiceRequestAdaptor();
			break;
		case SSR_AIRPORT:
			adaptor = new SsrAirportServiceRequestAdaptor();
			break;
		case FLEXIBILITY:
			adaptor = new FlexibilityRequestAdaptor();
			break;
		case AIRPORT_TRANSFER:
			adaptor = new AirportTransferRequestAdaptor();
			break;
		case AUTOMATIC_CHECKIN:
			adaptor = new AutomaticCheckinRequestAdaptor();
			break;
		default:
			break;
		}

		return adaptor;
	}

	public static AncillariesResponseAdaptor getResponseAdaptor(String type) {
		AncillariesConstants.AncillaryType ancillaryType = AncillariesConstants.AncillaryType.valueOf(type);
		AncillariesResponseAdaptor adaptor = null;

		switch (ancillaryType) {
		case BAGGAGE:
			adaptor = new BaggageResponseAdaptor();
			break;
		case SEAT:
			adaptor = new SeatResponseAdaptor();
			break;
		case MEAL:
			adaptor = new MealResponseAdaptor();
			break;
		case INSURANCE:
			adaptor = new InsuranseResponseAdaptor();
			break;
		case SSR_IN_FLIGHT:
			adaptor = new SsrInFlightServiceResponseAdaptor();
			break;
		case SSR_AIRPORT:
			adaptor = new SsrAirportServiceResponseAdaptor();
			break;
		case FLEXIBILITY:
			adaptor = new FlexibilityResponseAdaptor();
			break;
		case AIRPORT_TRANSFER:
			adaptor = new AirportTransferResponseAdaptor();
			break;
		case AUTOMATIC_CHECKIN:
			adaptor = new AutomaticCheckinResponseAdaptor();
			break;
		default:
			break;
		}

		return adaptor;
	}
}
