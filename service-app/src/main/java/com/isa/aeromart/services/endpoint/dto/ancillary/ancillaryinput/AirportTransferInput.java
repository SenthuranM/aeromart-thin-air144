package com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryInput;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class AirportTransferInput extends AncillaryInput {

	public enum TransferType {
		AIRPORT, HOME
	}	
	
	private String applyOn;
	
	private Date tranferDate;
	
	private String transferAddress;
	
	private String transferContact;
	
	private String transferType;
	
	private boolean existing;
	
    public AirportTransferInput() {
        setAncillaryType(AncillariesConstants.Type.AIRPORT_TRANSFER);
    }
    
	public String getApplyOn() {
		return applyOn;
	}

	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	public Date getTranferDate() {
		return tranferDate;
	}

	public void setTranferDate(Date tranferDate) {
		this.tranferDate = tranferDate;
	}

	public String getTransferAddress() {
		return transferAddress;
	}

	public void setTransferAddress(String transferAddress) {
		this.transferAddress = transferAddress;
	}

	public String getTransferContact() {
		return transferContact;
	}

	public void setTransferContact(String transferContact) {
		this.transferContact = transferContact;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public boolean isExisting() {
		return existing;
	}

	public void setExisting(boolean existing) {
		this.existing = existing;
	}
}
