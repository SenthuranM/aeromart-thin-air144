package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

class ONDSurchargeAdaptor extends ONDBaseChargeAdaptor implements ONDChargeAdaptor<SurchargeTO> {

	public ONDSurchargeAdaptor(ONDPriceAdaptor ondAdaptor) {
		super(ondAdaptor);
	}

	@Override
	public void addCharge(String paxType, SurchargeTO surcharge) {
		PaxPrice paxPrice = getPaxPrice(paxType, surcharge);
		paxPrice.setSurcharge(AccelAeroCalculator.add(paxPrice.getSurcharge(), surcharge.getAmount()));
		paxPrice.setTotal(AccelAeroCalculator.add(paxPrice.getTotal(), surcharge.getAmount()));
		OndOWPricing ondPrice = getOndPrice(surcharge.getOndSequence());
		ondPrice.setTotalSurcharges(AccelAeroCalculator.add(ondPrice.getTotalSurcharges(), surcharge.getAmount()));
		ondPrice.setTotalPrice(AccelAeroCalculator.add(ondPrice.getTotalPrice(), surcharge.getAmount()));
	}

}