package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.SelectedFlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.common.SpecificFlight;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccResSegmentInfo;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class OriginDestinationInfoAdaptor implements Adaptor<OriginDestinationInformationTO, OriginDestinationInformationTO> {

	private static Log log = LogFactory.getLog(OriginDestinationInfoAdaptor.class);
	private RPHGenerator rphGenerator;
	private ModifiedFlight modifiedData;
	private int departureVariance = 0;
	private boolean isFlownOND = false;
	private List<SpecificFlight> newFltSegList;
	private boolean isModifiedOND;
	private List<LccResSegmentInfo> ondResSegmentInfos;
	private List<LccResSegmentInfo> lccResSegmentInfos;

	public OriginDestinationInfoAdaptor(RPHGenerator rphGenerator, ModifiedFlight modifiedData,
			List<SpecificFlight> newFltSegList, boolean isModifiedOND, List<LccResSegmentInfo> ondResSegmentInfos, Set<LccResSegmentInfo> lccResSegmentInfos) {
		this.rphGenerator = rphGenerator;
		this.modifiedData = modifiedData;
		this.newFltSegList = newFltSegList;
		this.isModifiedOND = isModifiedOND;
		this.ondResSegmentInfos = ondResSegmentInfos;
		this.lccResSegmentInfos = new ArrayList<LccResSegmentInfo>(lccResSegmentInfos);
	}

	@Override
	public OriginDestinationInformationTO adapt(OriginDestinationInformationTO ondInfoTO) {

		String origin;
		String destination;
		Date depatureDate;
		boolean isFlexiSelected = false;

		List<FlightSegmentTO> flightSegementTOList = new ArrayList<FlightSegmentTO>();
		List<FareTO> perPaxFareTOs = getPerPaxFareTOs(ondResSegmentInfos);
		List<String> fltSegUnqIdList = getFltSegUnqIdList(ondResSegmentInfos);
		List<String> resSegRPHList = getResSegRPH(ondResSegmentInfos);
		List<Integer> flightSegmentIds = getFlightSegmentIds(ondResSegmentInfos);
		isFlownOND = isFlownOND(ondResSegmentInfos);

		if (isModifiedOND) {

			origin = getOriginBy(getFirstSegment(newFltSegList));
			destination = getDestinationBy(getLastSegment(newFltSegList));
			depatureDate = getDepartureDateBy(getFirstSegment(newFltSegList));
			flightSegementTOList.addAll(getFlightSegmentTOList(newFltSegList));
			isFlexiSelected = FareClassType.FLEXI.toString().equals(
					modifiedData.getPreferences().getAdditionalPreferences().getFareClassType());

			getModifiedONDTO(ondInfoTO, modifiedData, ondResSegmentInfos, fltSegUnqIdList, perPaxFareTOs, origin, destination,
					depatureDate, departureVariance, isFlownOND, flightSegementTOList, getFirstSegment(ondResSegmentInfos)
							.getBookingType());

		} else {

			LccResSegmentInfo firstSegment = getFirstSegment(ondResSegmentInfos);
			LccResSegmentInfo lastSegment = getLastSegment(ondResSegmentInfos);

			origin = SegmentUtil.getFromAirport(firstSegment.getSegmentCode());
			destination = SegmentUtil.getToAirport(lastSegment.getSegmentCode());
			depatureDate = firstSegment.getDepartureDate();

			getExistingFlightSegmentTOList(flightSegementTOList, ondResSegmentInfos);
			isFlexiSelected = firstSegment.getFlexiRuleID() != null ? true : false;

			getUnModifiedONDTO(firstSegment, flightSegmentIds, resSegRPHList, ondInfoTO, perPaxFareTOs, origin, destination,
					depatureDate, departureVariance, isFlownOND, flightSegementTOList, firstSegment.getBookingType());
		}
		ondInfoTO.setFlexiSelected(isFlexiSelected);
		return ondInfoTO;
	}

	private List<FlightSegmentTO> getFlightSegmentTOList(List<SpecificFlight> reqSpecFltList) {
		List<FlightSegmentTO> fltSegToLst = new ArrayList<FlightSegmentTO>();
		SelectedFlightSegmentAdaptor adaptor = new SelectedFlightSegmentAdaptor(rphGenerator);
		AdaptorUtils.adaptCollection(reqSpecFltList, fltSegToLst, adaptor);
		return fltSegToLst;
	}

	private void getModifiedONDTO(OriginDestinationInformationTO ondInfoTO, ModifiedFlight modifiedData,
			List<LccResSegmentInfo> ondResSegList, List<String> fltSegUnqIdList, List<FareTO> perPaxFareTOs, String origin,
			String destination, Date departureDate, int departureVariance, boolean isFlownOND,
			List<FlightSegmentTO> flightSegementTOList, String bookingType) {

		ondInfoTO.setPreferredLogicalCabin(modifiedData.getPreferences().getLogicalCabinClass());
		ondInfoTO.setPreferredClassOfService(modifiedData.getPreferences().getLogicalCabinClass());
		ondInfoTO.setPreferredBookingClass(modifiedData.getPreferences().getBookingCode());

		if (modifiedData.getPreferences().getAdditionalPreferences().getFareClassType().equals(FareClassType.BUNDLE.toString())) {
			ondInfoTO.setPreferredBundleFarePeriodId(modifiedData.getPreferences().getAdditionalPreferences().getFareClassId());
		}

		ondInfoTO.setSameFlightModification(ModificationUtils.isSameFlightModification(fltSegUnqIdList,
				modifiedData.getToFlightSegRPH(), rphGenerator));

		if (AppSysParamsUtil.isSameFareModificationEnabled() || AppSysParamsUtil.isEnforceSameHigher()) {
			ondInfoTO.setDateChangedResSegList(dateChangedPnrSegIds(ondResSegList, newFltSegList, lccResSegmentInfos));
			ondInfoTO.setOldPerPaxFareTOList(perPaxFareTOs);
		}
		populateONDTOBy(origin, destination, departureDate, ondInfoTO, isFlownOND, flightSegementTOList, bookingType);
		populateONDTOBy(departureDate, departureVariance, ondInfoTO);
	}

	private void getUnModifiedONDTO(LccResSegmentInfo firstSegment, List<Integer> flightSegmentIds, List<String> resSegRPHList,
			OriginDestinationInformationTO ondInfoTO, List<FareTO> perPaxFareTOs, String origin, String destination,
			Date departureDate, int departureVariance, boolean isFlownOND, List<FlightSegmentTO> flightSegementTOList,
			String bookingType) {

		ondInfoTO.setPreferredLogicalCabin(firstSegment.getLogicalCabinClass());
		ondInfoTO.setPreferredClassOfService(firstSegment.getCabinClass());
		ondInfoTO.setPreferredBundleFarePeriodId(firstSegment.getBundleFarePeriodId());
		ondInfoTO.setSameFlightModification(false);
		ondInfoTO.getExistingFlightSegIds().addAll(flightSegmentIds);
		ondInfoTO.getExistingPnrSegRPHs().addAll(resSegRPHList);

		if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs()) {
			ondInfoTO.setOldPerPaxFareTOList(perPaxFareTOs);
			ondInfoTO.setUnTouchedResSegList(resSegRPHList);
			ondInfoTO.setUnTouchedOnd(true);
		}
		populateONDTOBy(origin, destination, departureDate, ondInfoTO, isFlownOND, flightSegementTOList, bookingType);
		populateONDTOBy(departureDate, departureVariance, ondInfoTO);
	}

	private void populateONDTOBy(String origin, String destination, Date departureDate, OriginDestinationInformationTO ondInfoTO,
			boolean isFlownOND, List<FlightSegmentTO> flightSegementTOList, String bookingType) {
		ondInfoTO.setOrigin(origin);
		ondInfoTO.setDestination(destination);
		ondInfoTO.setPreferredDate(departureDate);
		ondInfoTO.setPreferredBookingType(bookingType);
		OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
		depOndOptionTO.getFlightSegmentList().addAll(flightSegementTOList);
		ondInfoTO.setFlownOnd(isFlownOND);
		ondInfoTO.getOrignDestinationOptions().add(depOndOptionTO);
	}

	private void populateONDTOBy(Date depatureDate, int departureVariance, OriginDestinationInformationTO ondInfoTO) {

		Date depatureDateTimeStart;
		Date depatureDateTimeEnd;

		depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(CalendarUtil.getStartTimeOfDate(depatureDate), departureVariance
				* -1);
		depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(CalendarUtil.getEndTimeOfDate(depatureDate), departureVariance);
		ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
		ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);
	}

	private List<Integer> dateChangedPnrSegIds(List<LccResSegmentInfo> resSegments, List<SpecificFlight> specFltLst,
			List<LccResSegmentInfo> segments) {
		List<Integer> dateChngPnrSegLst = new ArrayList<Integer>();
		if (resSegments.size() == specFltLst.size()) {
			int key = 0;
			for (LccResSegmentInfo segmentInfo : resSegments) {
				if (segmentInfo.getDepartureDate().equals(specFltLst.get(key).getDepartureDateTime())
						|| !segmentInfo.getSegmentCode().equals(specFltLst.get(key).getSegmentCode())) {
					return null;
				} else if (isSwapOnd(segmentInfo.getDepartureDate(), specFltLst.get(key).getDepartureDateTime(), segments)) {
					return null;
				} else {
					dateChngPnrSegLst.add(segmentInfo.getPnrSegID());
				}
				key++;
			}
		} else {
			return null;
		}

		return dateChngPnrSegLst;
	}

	private boolean isSwapOnd(Date previousDepDate, Date newDepDate, List<LccResSegmentInfo> segments) {
		for (LccResSegmentInfo segment : segments) {
			if (!LccResSegmentInfo.CANCEL.equals(segment.getStatus()) && (segment.getDepartureDate().after(previousDepDate) && segment.getDepartureDate().before(newDepDate)
					|| segment.getDepartureDate().after(newDepDate) && segment.getDepartureDate().before(previousDepDate))) {
				return true;
			}
		}
		return false;
	}

	private String getOriginBy(SpecificFlight firstSegment) {
		return SegmentUtil.getFromAirport(firstSegment.getSegmentCode());
	}

	private String getDestinationBy(SpecificFlight lastSegment) {
		return SegmentUtil.getToAirport(lastSegment.getSegmentCode());
	}

	private Date getDepartureDateBy(SpecificFlight firstSegment) {
		return firstSegment.getDepartureDateTime();
	}

	private <T> T getFirstSegment(List<T> newFltSegList) {
		T flight = null;
		if (newFltSegList != null && !newFltSegList.isEmpty()) {
			flight = newFltSegList.get(0);
		}
		return flight;
	}

	private <T> T getFirstSegment(Set<T> newFltSegList) {
		T flight = null;
		if (newFltSegList != null && !newFltSegList.isEmpty()) {
			flight = newFltSegList.iterator().next();
		}
		return flight;
	}

	private <T> T getLastSegment(List<T> newFltSegList) {
		T flight = null;
		if (newFltSegList != null && !newFltSegList.isEmpty()) {
			flight = newFltSegList.get(newFltSegList.size() - 1);
		}
		return flight;
	}

	private <T> T getLastSegment(Set<T> newFltSegList) {
		T flight = null;
		if (newFltSegList != null && !newFltSegList.isEmpty()) {
			for (T t : newFltSegList)
				flight = t;
		}
		return flight;
	}

	private List<FareTO> getPerPaxFareTOs(List<LccResSegmentInfo> ondResSegments) {
		List<FareTO> perPaxFareTOs = new ArrayList<FareTO>();
		for (LccResSegmentInfo resSeg : ondResSegments) {
			perPaxFareTOs.add(resSeg.getFareTO());
		}
		return perPaxFareTOs;
	}

	private List<String> getFltSegUnqIdList(List<LccResSegmentInfo> ondResSegments) {
		List<String> fltSegUnqIdList = new ArrayList<String>();
		for (LccResSegmentInfo resSeg : ondResSegments) {
			fltSegUnqIdList.add(resSeg.getFlightSegmentRefNumber());
		}
		return fltSegUnqIdList;
	}

	private List<String> getResSegRPH(List<LccResSegmentInfo> ondResSegments) {
		List<String> resSegRPHList = new ArrayList<String>();
		for (LccResSegmentInfo resSeg : ondResSegments) {
			resSegRPHList
					.add(ModificationUtils.createResSegUnqIdatifier(resSeg.getFlightSegmentRefNumber(), resSeg.getPnrSegID()));
		}
		return resSegRPHList;
	}

	private List<Integer> getFlightSegmentIds(List<LccResSegmentInfo> ondResSegments) {
		List<Integer> flightSegmentIds = new ArrayList<Integer>();
		for (LccResSegmentInfo resSeg : ondResSegments) {
			flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));
		}
		return flightSegmentIds;
	}

	private boolean isFlownOND(List<LccResSegmentInfo> ondResSegments) {
		boolean isFlownOND = false;
		for (LccResSegmentInfo resSeg : ondResSegments) {
			if (resSeg.isFlownSegment()) {
				isFlownOND = true;
			}
		}
		return isFlownOND;
	}

	private static void getExistingFlightSegmentTOList(List<FlightSegmentTO> fltSegToLst, List<LccResSegmentInfo> resSegs) {
		for (LccResSegmentInfo resSeg : resSegs) {
			FlightSegmentTO flightSegementTO = new FlightSegmentTO();
			flightSegementTO.setFlightRefNumber(resSeg.getFlightSegmentRefNumber());
			flightSegementTO.setSegmentCode(resSeg.getSegmentCode());
			flightSegementTO.setFlightNumber(resSeg.getFlightNo());
			flightSegementTO.setRouteRefNumber(resSeg.getRouteRefNumber());
			fltSegToLst.add(flightSegementTO);
		}
	}

}
