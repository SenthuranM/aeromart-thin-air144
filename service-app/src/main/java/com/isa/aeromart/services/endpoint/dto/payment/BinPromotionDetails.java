package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.List;

public class BinPromotionDetails {

	private boolean promotionApplied;
	private boolean alreadyApplied;
	private boolean hasExistingPromotion;
	private boolean noPromotion;
	private boolean discountAsCredit;

	private BigDecimal existingPromotionAmount;
	private BigDecimal discountAmount;
	private BigDecimal lmsApplicableAmount;
	private AmountObject effectivePaymentAmount;
	private AmountObject transactionFee;

	private LmsCredits lmsCredits;

	private int paymentGatewayId;
	private String cardNo;
	private int cardType;

	private List<String> messages;

	public boolean isPromotionApplied() {
		return promotionApplied;
	}

	public void setPromotionApplied(boolean promotionApplied) {
		this.promotionApplied = promotionApplied;
	}

	public boolean isAlreadyApplied() {
		return alreadyApplied;
	}

	public void setAlreadyApplied(boolean alreadyApplied) {
		this.alreadyApplied = alreadyApplied;
	}

	public boolean isHasExistingPromotion() {
		return hasExistingPromotion;
	}

	public void setHasExistingPromotion(boolean hasExistingPromotion) {
		this.hasExistingPromotion = hasExistingPromotion;
	}

	public boolean isNoPromotion() {
		return noPromotion;
	}

	public void setNoPromotion(boolean noPromotion) {
		this.noPromotion = noPromotion;
	}

	public LmsCredits getLmsCredits() {
		return lmsCredits;
	}

	public void setLmsCredits(LmsCredits lmsCredits) {
		this.lmsCredits = lmsCredits;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public int getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(int paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public AmountObject getEffectivePaymentAmount() {
		return effectivePaymentAmount;
	}

	public void setEffectivePaymentAmount(AmountObject effectivePaymentAmount) {
		this.effectivePaymentAmount = effectivePaymentAmount;
	}

	public AmountObject getTransactionFee() {
		return transactionFee;
	}

	public void setTransactionFee(AmountObject transactionFee) {
		this.transactionFee = transactionFee;
	}

	public BigDecimal getExistingPromotionAmount() {
		return existingPromotionAmount;
	}

	public void setExistingPromotionAmount(BigDecimal existingPromotionAmount) {
		this.existingPromotionAmount = existingPromotionAmount;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public BigDecimal getLmsApplicableAmount() {
		return lmsApplicableAmount;
	}

	public void setLmsApplicableAmount(BigDecimal lmsApplicableAmount) {
		this.lmsApplicableAmount = lmsApplicableAmount;
	}

	public boolean isDiscountAsCredit() {
		return discountAsCredit;
	}

	public void setDiscountAsCredit(boolean discountAsCredit) {
		this.discountAsCredit = discountAsCredit;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

}
