package com.isa.aeromart.services.endpoint.controller.payment;

import java.awt.PageAttributes.MediaType;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.controller.common.StatefulRequestAwareController;
import com.isa.aeromart.services.endpoint.dto.payment.FlightDeatsilsForPaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.FlightDeatsilsForPaymentRS;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.common.LableServiceUtil;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

@Controller
@RequestMapping(value = "postPayment")
public class PostPaymentRedirectionController extends StatefulRequestAwareController {

	private static Log log = LogFactory.getLog(PostPaymentRedirectionController.class);

	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	public String redirectGET() {
		log.debug("Handle Web redirect GET");
		return handleGetRedirect(PaymentConsts.WEB_FORWARD_URL_SUFFIX);
	}

	@RequestMapping(value = "/redirect", method = RequestMethod.POST)
	public String redirectPOST() throws UnsupportedEncodingException {
		log.debug("Handle Web redirect POST");
		return handlePostRedirect(PaymentConsts.WEB_FORWARD_URL_SUFFIX);
	}

	@ResponseBody
	@RequestMapping(value = "/getAirportSpecialMessages", method = RequestMethod.POST)
	public FlightDeatsilsForPaymentRS getSpecialMessages(@RequestBody FlightDeatsilsForPaymentRQ details) {
		GlobalConfig globalConfig = new GlobalConfig();
		Map<String, ArrayList<String>> mapSpecialMsg = globalConfig.getAirportMessages();
		List<String> msgs = mapSpecialMsg.get(details.getOriginCode());
		if (msgs == null || msgs.size() == 0) {
			msgs = mapSpecialMsg.get(details.getDestinationCode());
		}
		List<String> specialMessages = new ArrayList<String>();
		FlightDeatsilsForPaymentRS rs = new FlightDeatsilsForPaymentRS();
		Properties propertyMap = new Properties();
		propertyMap.put("msg.fltSearch.spMsg", "okToBoardMsg");
		propertyMap.put("msg.fltSearch.spMsgCC", "creditCardMsg");
		if (msgs != null) {
			for (String key : msgs) {
				propertyMap.contains(key);
				specialMessages.add(LableServiceUtil.getMessage(key, details.getLanguageCode()));
			}
		}
		rs.setMessages(specialMessages);
		return rs;
	}
	
	@RequestMapping(value = "/mibe/redirect", method = RequestMethod.GET)
	public String mibeRedirectGET() {
		log.debug("Handle Mobile redirect GET");
		return handleGetRedirect(PaymentConsts.MOBILE_FORWARD_URL_SUFFIX);

	}

	@RequestMapping(value = "/mibe/redirect", method = RequestMethod.POST)
	public String mibeRedirectPOST() throws UnsupportedEncodingException {
		log.debug("Handle Mobile redirect POST");
		return handlePostRedirect(PaymentConsts.MOBILE_FORWARD_URL_SUFFIX);
	}

	

	@RequestMapping(value = "/redirect/cmi", method = RequestMethod.POST)
	@ResponseBody
	public String redirectFromCMI() {

		String intermediateResponse = "";
		String returncode = getRequest().getParameter(
				PaymentConstants.RETURN_CODE);
		if (PaymentConstants.RETURN_CODE_SUCCESS.equals(returncode)) {
			intermediateResponse = PaymentConstants.APPROVED_POST;

		} else {
			intermediateResponse = PaymentConstants.APPROVED_FOR_VOID;
		}

		log.debug("Handling CMI intermediate response=" + intermediateResponse);

		return intermediateResponse;

	}

	@RequestMapping(value = "/knet/redirect", method = RequestMethod.POST)
	public String knetRedirect() {

		if (log.isDebugEnabled())
			log.debug("###Start.." + getRequest().getRequestedSessionId());

		StringBuffer responseURL = new StringBuffer();

		getResponse().setContentType("text/html;charset=UTF-8");
		String paymentId = getRequest().getParameter("paymentid");
		String result = getRequest().getParameter("result");
		String trackid = getRequest().getParameter("trackid");
		Map receiptyMap = getReceiptMap(getRequest());
		try {

			PrintWriter out = getResponse().getWriter();
			// Get message details sent from Commerce Gateway
			String ErrorNo = getRequest().getParameter("Error");
			String udf1 = getRequest().getParameter("udf1");
			String udf2 = getRequest().getParameter("udf2");
			String udf3 = getRequest().getParameter("udf3");
			String udf4 = getRequest().getParameter("udf4");
			String udf5 = getRequest().getParameter("udf5");
			String ErrorText = getRequest().getParameter("ErrorText");
			String postdate = getRequest().getParameter("postdate");
			String tranid = getRequest().getParameter("tranid");
			String auth = getRequest().getParameter("auth");
			String ref = getRequest().getParameter("ref");

			StringBuilder strLog = new StringBuilder();
			strLog.append("paymentid:").append(paymentId).append(",");
			strLog.append("ErrorNo:").append(ErrorNo).append(",");
			strLog.append("udf1:").append(udf1).append(",");
			strLog.append("udf2:").append(udf2).append(",");
			strLog.append("udf3:").append(udf3).append(",");
			strLog.append("udf4:").append(udf4).append(",");
			strLog.append("udf5:").append(udf5).append(",");
			strLog.append("ErrorText:").append(ErrorText).append(",");
			strLog.append("postdate:").append(postdate).append(",");
			strLog.append("tranid:").append(tranid).append(",");
			strLog.append("trackid:").append(trackid).append(",");
			strLog.append("ref:").append(ref).append(",");
			strLog.append("result:").append(result).append(",");
			strLog.append("auth:").append(auth).append(",");
			strLog.append("SessionID:").append(getRequest().getRequestedSessionId()).append(",");
			log.info(strLog.toString());

			responseURL.append("REDIRECT=").append(AppSysParamsUtil.getSecureServiceAppIBEUrl())
					.append(PaymentConsts.WEB_FORWARD_URL_SUFFIX).append("?");
			responseURL.append("paymentid=").append(paymentId).append("&");
			if (!StringUtil.isNullOrEmpty(result))
				responseURL.append("result=").append(result).append("&");
			if (!StringUtil.isNullOrEmpty(auth))
				responseURL.append("auth=").append(auth).append("&");
			if (!StringUtil.isNullOrEmpty(ref))
				responseURL.append("ref=").append(ref).append("&");
			if (!StringUtil.isNullOrEmpty(postdate))
				responseURL.append("postdate=").append(postdate).append("&");
			if (!StringUtil.isNullOrEmpty(trackid))
				responseURL.append("trackid=").append(trackid).append("&");
			if (!StringUtil.isNullOrEmpty(tranid))
				responseURL.append("tranid=").append(tranid).append("&");
			if (!StringUtil.isNullOrEmpty(udf1))
				responseURL.append("udf1=").append(udf1).append("&");
			if (!StringUtil.isNullOrEmpty(udf2))
				responseURL.append("udf2=").append(udf2).append("&");
			if (!StringUtil.isNullOrEmpty(udf3))
				responseURL.append("udf3=").append(udf3).append("&");
			if (!StringUtil.isNullOrEmpty(udf4))
				responseURL.append("udf4=").append(udf4).append("&");
			if (!StringUtil.isNullOrEmpty(udf5))
				responseURL.append("udf5=").append(udf5);
			// Save Intermediate response
			// This action is executed by payment gateway repeatedly

			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			int paymentBrokerRefNo = Integer.parseInt(udf4);
			int tempPayId = Integer.parseInt(getTempID(trackid));
			Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);

			ipgResponseDTO.setRequestTimsStamp(requestTime);
			ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
			ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
			ipgResponseDTO.setTemporyPaymentId(tempPayId);

			String[] udf3Arr = udf3.split("_");
			String strPayCurCode = udf3Arr[1];
			int ipgId = Integer.parseInt(udf3Arr[0]);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					new Integer(ipgId), strPayCurCode);
			// Update receipt map for as intermediate response
			receiptyMap.put("responseType", "INTERMEDIATE");
			ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);

			out.print(responseURL.toString());
			out.flush();
			out.close();

			if (log.isDebugEnabled())
				log.debug("###END.." + getRequest().getRequestedSessionId());

		} catch (Exception ex) {
			log.error("###Error...##" + getRequest().getRequestedSessionId() + "paymentid" + paymentId + "Result" + result
					+ trackid, ex);
			try {
				PrintWriter out = getResponse().getWriter();
				responseURL = new StringBuffer();
				responseURL.append("REDIRECT=").append(AppSysParamsUtil.getSecureServiceAppIBEUrl())
						.append(PaymentConsts.WEB_FORWARD_URL_SUFFIX).append("?");
				responseURL.append("paymentid=").append(paymentId).append("&");
				responseURL.append("result=").append(result).append("&");
				responseURL.append("trackid=").append(trackid);
				out.print(responseURL.toString());
				out.flush();
				out.close();
			} catch (Exception exp) {
				log.error("###Error on data##" + getRequest().getRequestedSessionId(), exp);
			}

		}

		return null;
	}

	private Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
				log.info("***" + fieldName + "==" + fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	private String getReceiptMapString(HttpServletRequest request) throws UnsupportedEncodingException {
		StringBuffer postURLattributes = new StringBuffer();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);
			fieldValue=URLEncoder.encode(fieldValue,PaymentConstants.UTF_8);
					
			//fieldValue=fieldValue.replaceAll(PaymentConstants.HASH, PaymentConstants.HASH_TEXT);

			if (fieldValue != null && fieldValue.length() > 0) {
				postURLattributes.append(fieldName).append("=").append(fieldValue).append("&");
			}
		}
		postURLattributes.append(PaymentConstants.IPG_SESSION_ID).append("=").append(request.getSession().getId());
		return postURLattributes.toString();
	}

	private String getTempID(String trackID) {
		String tempPayID = "";
		if (trackID != null) {
			tempPayID = trackID.substring(1);
		}
		return tempPayID;
	}

	public static String getOnHoldDisplayTime(OnHold appOnHold, Date pnrReleaseDate) {
		long displayTimelong = getTotalMinutes(AppSysParamsUtil.getOnHoldDisplayTime(appOnHold)) * 60 * 1000;
		Calendar currentDate = Calendar.getInstance();
		long onHoldDuration = 0;

		if (pnrReleaseDate != null) {
			onHoldDuration = pnrReleaseDate.getTime() - currentDate.getTimeInMillis();
		}

		if (!AppSysParamsUtil.isApplyRouteBookingClassLevelOnholdReleaseTime() && onHoldDuration > displayTimelong) {
			return AppSysParamsUtil.getOnHoldDisplayTime(appOnHold);
		} else {
			return getTimeDDHHMM(onHoldDuration);
		}
	}

	/**
	 * Get Total Minutes
	 * 
	 * @param duration
	 * @return
	 */
	protected static int getTotalMinutes(String duration) {

		if (duration == null || duration.equals("") || duration.length() < 5) {
			return 0;
		}
		String[] arrDuration = duration.split(":");
		return (new Integer(arrDuration[0]).intValue() * 60) + new Integer(arrDuration[1]).intValue();
	}

	/**
	 * Support Months:days:hours:minuts or hours : minutes
	 * 
	 * @param hhmm
	 * @return
	 */
	public static String wrapWithDDHHMM(String hhmm) {
		StringBuilder wrapTextHM = new StringBuilder();
		if (hhmm != null && !hhmm.isEmpty()) {
			String[] textArray = hhmm.split(":");
			if (textArray.length == 3) {

				if (textArray[0] != null && !textArray[0].equals("0")) {
					wrapTextHM.append(textArray[0]);
					wrapTextHM.append(" Day(s) : ");
				}

				wrapTextHM.append(textArray[1]);
				wrapTextHM.append(" Hour(s) : ");

				wrapTextHM.append(textArray[2]);
				wrapTextHM.append(" Minute(s) ");

			}

			if (textArray.length == 2) {
				wrapTextHM.append(textArray[0]);
				wrapTextHM.append(" Hour(s) : ");

				wrapTextHM.append(textArray[1]);
				wrapTextHM.append(" Minute(s) ");
			}
		}

		return wrapTextHM.toString();
	}

	public static String getTimeDDHHMM(long time) {
		String dddhhmm = "";

		long days = TimeUnit.MILLISECONDS.toDays(time);
		long hours = TimeUnit.MILLISECONDS.toHours(time - TimeUnit.DAYS.toMillis(days));
		long minutes = TimeUnit.MILLISECONDS.toMinutes(time - (TimeUnit.DAYS.toMillis(days) + TimeUnit.HOURS.toMillis(hours)));

		dddhhmm = days + ":" + hours + ":" + minutes;

		return dddhhmm;
	}

	/**
	 * Is Kiosk User
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isKioskUser(HttpServletRequest request) {
		boolean isKioskUser = false;
		HttpSession reqsession = request.getSession();
		String accessPoint = (String) reqsession.getAttribute(PaymentConsts.SYS_ACCESS_POINT);
		if (PaymentConsts.SYS_ACCESS_POINT_KSK.equals(accessPoint)) {
			isKioskUser = true;

			User user = (User) reqsession.getAttribute(PaymentConsts.REQ_KSK_USER);

			if (user == null) {
				isKioskUser = false;
			}

		}

		return isKioskUser;
	}

	private String handleGetRedirect(String redirectSuffix) {
		log.debug("GET /redirect: received/n" + getRequest().getQueryString() + PaymentConstants.IPG_SESSION_ID.toString() + ":" + getRequest().getSession().getId());
		String RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + redirectSuffix + '?' + getRequest().getQueryString();

		String returnString = "redirect:" + RETURN_URL;

		log.debug("GET /redirect: redirecting\n" + returnString);

		return returnString;
	}

	private String handlePostRedirect(String redirectSuffix) throws UnsupportedEncodingException {

		log.debug("POST /redirect: received message, content length: " + getRequest().getContentLength());

		String postURLattributes = getReceiptMapString(getRequest());

		log.debug("POST /redirect: received message, content\n" + postURLattributes + PaymentConstants.IPG_SESSION_ID.toString() + ":" + getRequest().getSession().getId());

		String RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + redirectSuffix + '?' + postURLattributes;

		String returnString = "redirect:" + RETURN_URL;

		log.debug("POST /redirect: redirecting\n" + returnString);
		return returnString;
	}
	
}
