package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.common.SpecificFlight;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class SelectedFlightSegmentAdaptor implements Adaptor<SpecificFlight, FlightSegmentTO> {

	private RPHGenerator rphGenerator;

	public SelectedFlightSegmentAdaptor(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;
	}

	@Override
	public FlightSegmentTO adapt(SpecificFlight source) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		if (rphGenerator != null) {
			flightSegmentTO.setFlightRefNumber(rphGenerator.getUniqueIdentifier(source.getFlightSegmentRPH()));
		} else {
			flightSegmentTO.setFlightRefNumber(source.getFlightSegmentRPH());
		}
		flightSegmentTO.setRouteRefNumber(source.getRouteRefNumber());
		flightSegmentTO.setDepartureDateTime(source.getDepartureDateTime());
		flightSegmentTO.setSegmentCode(source.getSegmentCode());
		flightSegmentTO.setFlightNumber(source.getFlightDesignator());
		return flightSegmentTO;
	}
}
