package com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class InsuranceItems extends AncillaryItemLeaf {
	
	private List<InsuranceItem> items;

    public InsuranceItems() {
        setType(AncillariesConstants.Type.INSURANCE);
    }

    public List<InsuranceItem> getItems() {
		return items;
	}

	public void setItems(List<InsuranceItem> items) {
		this.items = items;
	}
	
}
