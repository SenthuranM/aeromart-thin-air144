package com.isa.aeromart.services.endpoint.dto.session;

import java.math.BigDecimal;

import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;

public class BinPromoDiscountInfo {

	private DiscountedFareDetails discountedFareDetails;
	private BigDecimal discountAmount;
	private boolean discountAsCredit;
	private String cardNumber;
	private int paymentGatewayId;
	private int cardType;
	// TODO remove
	private ReservationDiscountDTO reservationDiscountDTO;

	public DiscountedFareDetails getDiscountedFareDetails() {
		return discountedFareDetails;
	}

	public void setDiscountedFareDetails(DiscountedFareDetails discountedFareDetails) {
		this.discountedFareDetails = discountedFareDetails;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(int paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public boolean isDiscountAsCredit() {
		return discountAsCredit;
	}

	public void setDiscountAsCredit(boolean discountAsCredit) {
		this.discountAsCredit = discountAsCredit;
	}

	public ReservationDiscountDTO getReservationDiscountDTO() {
		return reservationDiscountDTO;
	}

	public void setReservationDiscountDTO(ReservationDiscountDTO reservationDiscountDTO) {
		this.reservationDiscountDTO = reservationDiscountDTO;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

}
