package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.math.BigDecimal;


public class AncillarySummary {

	private String anciType;
	
	private String scopeType;
	
	private boolean selected;
	
	private boolean available;
	
	private boolean editable;
	
	private BigDecimal totalAmount;

	public String getAnciType() {
		return anciType;
	}

	public void setAnciType(String ancyType) {
		this.anciType = ancyType;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getScopeType() {
		return scopeType;
	}

	public void setScopeType(String scopeType) {
		this.scopeType = scopeType;
	}

}
