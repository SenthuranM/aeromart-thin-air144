package com.isa.aeromart.services.endpoint.controller.common;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.isa.aeromart.services.endpoint.delegate.common.CaptchaVerifyServiceImpl;
import com.isa.aeromart.services.endpoint.delegate.common.RecaptchaService;
import com.isa.aeromart.services.endpoint.dto.common.CaptchaRequest;
import com.isa.aeromart.services.endpoint.dto.common.CaptchaResponse;
import com.isa.aeromart.services.endpoint.dto.common.CaptchaVerifyRQ;
import com.isa.aeromart.services.endpoint.dto.common.CaptchaVerifyRS;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;

@RestController
public class CaptchaController extends StatefulController {

	@Autowired
	private RecaptchaService reCaptchaService;

	/**
	 * 
	 * This controller is used for only captcha verification. Please dot use
	 * request in the method input parameters like this in any other controller
	 * class methods. This need to be removed and get the request in appropriate
	 * way.
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/captcha/verify", method = RequestMethod.POST)
	public CaptchaResponse verifyCaptcha(@RequestBody CaptchaRequest captchaReq, HttpServletRequest request) {

		ArrayList<String> messages = new ArrayList<String>();
		CaptchaResponse captchaResponse = new CaptchaResponse();
		BookingSessionStore bookingSessionStore = captchaReq.getTransactionId() != null ?
				(BookingSessionStore) getSessionStore(captchaReq.getTransactionId()) : null;
		CaptchaVerifyServiceImpl captchaVerify = new CaptchaVerifyServiceImpl();
		boolean captchaVerified = captchaVerify.verifyCaptcha(captchaReq.getCaptcha(), request);

		if (captchaVerified) {
			if (bookingSessionStore != null) {
				bookingSessionStore.storeCaptchaText(captchaReq.getCaptcha());
			}
			messages.add("verified");
		} else {
			messages.add("Captcha verification incorrect. Please try again"); // TODO
																				// get
																				// the
																				// translations
		}

		captchaResponse.setTransactionId(captchaReq.getTransactionId());
		captchaResponse.setSuccess(true);
		captchaResponse.setCaptchaTrue(captchaVerified);
		captchaResponse.setMessages(messages);

		return captchaResponse;
	}

	@RequestMapping(value = "/recaptcha/verify", method = RequestMethod.POST)
	public CaptchaVerifyRS verifyReCaptcha(@RequestBody CaptchaVerifyRQ captchaReq, HttpServletRequest request) {
		CaptchaVerifyRS response = new CaptchaVerifyRS();
		if (reCaptchaService.verifyRequest(captchaReq)) {
			request.getSession().setAttribute("CAPTCHA_CHALLENGE_VALIDATED", true);
			response.setSuccess(true);
		}
		return response;
	}

}
