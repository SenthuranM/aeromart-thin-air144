package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.PaxWiseTotal;
import com.isa.aeromart.services.endpoint.dto.common.PromoSummary;
import com.isa.aeromart.services.endpoint.dto.common.ReservationChargeDetail;

public class PaymentSummaryRS extends AdministrationFeeRS {

	private double totalAncillary;
	private double taxAndSurcharges;
	private List<PaxWiseTotal> paxWiseTotalFare;
	private PromoSummary promoSummary;
	private List<ReservationChargeDetail> reservationChargeDetails;

	public double getTotalAncillary() {
		return totalAncillary;
	}

	public void setTotalAncillary(double totalAncillary) {
		this.totalAncillary = totalAncillary;
	}

	public double getTaxAndSurcharges() {
		return taxAndSurcharges;
	}

	public void setTaxAndSurcharges(double taxAndSurcharges) {
		this.taxAndSurcharges = taxAndSurcharges;
	}

	public List<PaxWiseTotal> getPaxWiseTotalFare() {
		return paxWiseTotalFare;
	}

	public void setPaxWiseTotalFare(List<PaxWiseTotal> paxWiseTotalFare) {
		this.paxWiseTotalFare = paxWiseTotalFare;
	}

	public PromoSummary getPromoSummary() {
		return promoSummary;
	}

	public void setPromoSummary(PromoSummary promoSummary) {
		this.promoSummary = promoSummary;
	}


	public List<ReservationChargeDetail> getReservationChargeDetails() {
		return reservationChargeDetails;
	}

	public void setReservationChargeDetails(List<ReservationChargeDetail> reservationChargeDetails) {
		this.reservationChargeDetails = reservationChargeDetails;
	}

}
