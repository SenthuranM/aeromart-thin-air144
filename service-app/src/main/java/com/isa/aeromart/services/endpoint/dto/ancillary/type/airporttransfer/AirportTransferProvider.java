package com.isa.aeromart.services.endpoint.dto.ancillary.type.airporttransfer;

import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;

public class AirportTransferProvider extends Provider{
	
	private String address;
	
	private String airportTransferId;
	
	private String emailId;
	
	private String name;
	
	private String number;
	
	private String airportCode;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAirportTransferId() {
		return airportTransferId;
	}

	public void setAirportTransferId(String airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

}
