package com.isa.aeromart.services.endpoint.adaptor.impls.availability.response;

import java.util.HashMap;
import java.util.Map;

public class RPHGenerator {
	private int currentSequence = 0;
	private Map<String, String> uniqueIdRPHMap = new HashMap<String, String>();
	private Map<String, String> rphUniqueIdMap = new HashMap<String, String>();

	public String getRPH(String uniqueIdentifier) {
		if (!uniqueIdRPHMap.keySet().contains(uniqueIdentifier)) {
			String rph = getNextRPH();
			uniqueIdRPHMap.put(uniqueIdentifier, rph);
			rphUniqueIdMap.put(rph, uniqueIdentifier);
		}
		return uniqueIdRPHMap.get(uniqueIdentifier) + "";
	}

	private String getNextRPH() {
		return ("" + currentSequence++);
	}

	public String getUniqueIdentifier(String rph) {
		return rphUniqueIdMap.get(rph);
	}
}
