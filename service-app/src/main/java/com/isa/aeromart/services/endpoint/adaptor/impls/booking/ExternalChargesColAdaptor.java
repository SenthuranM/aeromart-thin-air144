package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airinventory.api.util.LccAdaptor;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ExternalChargesColAdaptor implements
		Adaptor<Collection<LCCClientExternalChgDTO>, Map<EXTERNAL_CHARGES, ExternalChgDTO>> {

	public Map<EXTERNAL_CHARGES, ExternalChgDTO> adapt(Collection<LCCClientExternalChgDTO> externalChgDTOs) {
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO>();
		if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
			java.util.Iterator<LCCClientExternalChgDTO> paymentChargeItr = externalChgDTOs.iterator();
			while (paymentChargeItr.hasNext()) {
				LCCClientExternalChgDTO charge = paymentChargeItr.next();
				if (EXTERNAL_CHARGES.JN_OTHER == charge.getExternalCharges()) {
					ServiceTaxExtChgDTO externalChgDTO = null;
					if (extChgMap.get(EXTERNAL_CHARGES.JN_OTHER) != null) {
						externalChgDTO = (ServiceTaxExtChgDTO) extChgMap.get(EXTERNAL_CHARGES.JN_OTHER);
						externalChgDTO.setAmount(AccelAeroCalculator.add(externalChgDTO.getAmount(), charge.getAmount()));
					} else {
						externalChgDTO = new ServiceTaxExtChgDTO();
						externalChgDTO.setAmount(charge.getAmount());
						externalChgDTO.setJourneyType(charge.getJourneyType());
						externalChgDTO.setExternalChargesEnum(charge.getExternalCharges());
						externalChgDTO.setFlightRefNumber(charge.getFlightRefNumber());
						if (charge.getFlightRefNumber() != null) {
							externalChgDTO.setFlightSegId(Integer.parseInt(LccAdaptor.extractFlightSegmentId(charge
									.getFlightRefNumber())));
						}
						externalChgDTO.setChgGrpCode(PricingConstants.ChargeGroups.TAX);
						externalChgDTO.setValid(true);
					}
					externalChgDTO.setChargeCode(charge.getCode());
					externalChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.JN_OTHER);
					extChgMap.put(EXTERNAL_CHARGES.JN_OTHER, externalChgDTO);
				} else if (EXTERNAL_CHARGES.CREDIT_CARD == charge.getExternalCharges()) {
					ExternalChgDTO externalChgDTO = null;
					if (extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD) != null) {
						externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
						externalChgDTO.setAmount(AccelAeroCalculator.add(externalChgDTO.getAmount(), charge.getAmount()));
					} else {
						externalChgDTO = new ExternalChgDTO();
						externalChgDTO.setAmount(charge.getAmount());
						externalChgDTO.setJourneyType(charge.getJourneyType());
						externalChgDTO.setExternalChargesEnum(charge.getExternalCharges());
						if (charge.getFlightRefNumber() != null) {
							externalChgDTO.setFlightSegId(Integer.parseInt(LccAdaptor.extractFlightSegmentId(charge
									.getFlightRefNumber())));
						}
						externalChgDTO.setChgGrpCode(PricingConstants.ChargeGroups.THIRDPARTY_SURCHARGES);
						externalChgDTO.setValid(true);
					}
					externalChgDTO.setChargeCode(charge.getCode());
					externalChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.CREDIT_CARD);
					extChgMap.put(EXTERNAL_CHARGES.CREDIT_CARD, externalChgDTO);
				}
			}
		}
		return extChgMap;
	}
}
