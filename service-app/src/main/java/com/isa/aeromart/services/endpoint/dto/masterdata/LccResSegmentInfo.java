package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.Date;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;

public class LccResSegmentInfo {

	private Integer pnrSegID;
	private Integer groundStationPnrSegmentID;
	private Integer segmentSeq;
	private String carrierCode;
	private Date departureDate;
	private String flightSegmentRefNumber;
	private boolean isFlownSegment;
	private FareTO fareTO;
	private String segmentCode;
	private String bookingType;
	private String cabinClass;
	private String logicalCabinClass;
	private Integer bundleFarePeriodId;
	private String flightNo;
	private String routeRefNumber;
	private Integer FlexiRuleID;
	private boolean unSegment;
	private String status;

	public static final String CANCEL = "CNX";

	public Integer getPnrSegID() {
		return pnrSegID;
	}

	public void setPnrSegID(Integer pnrSegID) {
		this.pnrSegID = pnrSegID;
	}

	public Integer getGroundStationPnrSegmentID() {
		return groundStationPnrSegmentID;
	}

	public void setGroundStationPnrSegmentID(Integer groundStationPnrSegmentID) {
		this.groundStationPnrSegmentID = groundStationPnrSegmentID;
	}

	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getFlightSegmentRefNumber() {
		return flightSegmentRefNumber;
	}

	public void setFlightSegmentRefNumber(String flightSegmentRefNumber) {
		this.flightSegmentRefNumber = flightSegmentRefNumber;
	}

	public boolean isFlownSegment() {
		return isFlownSegment;
	}

	public void setFlownSegment(boolean isFlownSegment) {
		this.isFlownSegment = isFlownSegment;
	}

	public FareTO getFareTO() {
		return fareTO;
	}

	public void setFareTO(FareTO fareTO) {
		this.fareTO = fareTO;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}
	
	public Integer getBundleFarePeriodId() {
		return bundleFarePeriodId;
	}

	public void setBundleFarePeriodId(Integer bundleFarePeriodId) {
		this.bundleFarePeriodId = bundleFarePeriodId;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getRouteRefNumber() {
		return routeRefNumber;
	}

	public void setRouteRefNumber(String routeRefNumber) {
		this.routeRefNumber = routeRefNumber;
	}

	public Integer getFlexiRuleID() {
		return FlexiRuleID;
	}

	public void setFlexiRuleID(Integer flexiRuleID) {
		FlexiRuleID = flexiRuleID;
	}

	public boolean isUnSegment() {
		return unSegment;
	}

	public void setUnSegment(boolean unSegment) {
		this.unSegment = unSegment;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
