package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.AirportMessageFilter;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.AirportFlightDetail;

public class AirportMessageFlightDetailsAdaptor {

	public List<AirportFlightDetail> getAirportFlightDetails(List<AirportMessageFilter> airportMessageFilterList) {
		List<AirportFlightDetail> airportMessageFlightDetailList = new ArrayList<AirportFlightDetail>();

		for (AirportMessageFilter airportMessageFilter : airportMessageFilterList) {
			airportMessageFlightDetailList.add(getAirportDetail(airportMessageFilter,
					ReservationInternalConstants.AirportMessageTravelWay.DEP));
			airportMessageFlightDetailList.add(getAirportDetail(airportMessageFilter,
					ReservationInternalConstants.AirportMessageTravelWay.ARR));
		}
		return airportMessageFlightDetailList;
	}

	private AirportFlightDetail getAirportDetail(AirportMessageFilter airportMessageFilter, String travelWay) {
		AirportFlightDetail airportFlightDetail = new AirportFlightDetail();
		if (ReservationInternalConstants.AirportMessageTravelWay.DEP.equalsIgnoreCase(travelWay)) {
			airportFlightDetail.setAirportCode(airportMessageFilter.getDepAirport());
		} else if (ReservationInternalConstants.AirportMessageTravelWay.ARR.equalsIgnoreCase(travelWay)) {
			airportFlightDetail.setAirportCode(airportMessageFilter.getArrAirport());
		}
		airportFlightDetail.setTravelDate(airportMessageFilter.getFromDate());
		airportFlightDetail.setFlightDepDate(airportMessageFilter.getFlightDepDate());
		airportFlightDetail.setFlightNumber(airportMessageFilter.getFlightNumber());
		airportFlightDetail.setFlightIds(airportMessageFilter.getFlightId());
		airportFlightDetail.setSalesChannel(airportMessageFilter.getSalesChanel());
		airportFlightDetail.setStage(airportMessageFilter.getStage());
		airportFlightDetail.setTravelWay(travelWay);
		airportFlightDetail.setOnd(airportMessageFilter.getDepAirport() + "/" + airportMessageFilter.getArrAirport());
		return airportFlightDetail;
	}
}
