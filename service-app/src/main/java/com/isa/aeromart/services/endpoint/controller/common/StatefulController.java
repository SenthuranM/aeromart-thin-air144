package com.isa.aeromart.services.endpoint.controller.common;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.isa.aeromart.services.endpoint.dto.common.ClientCommonInfoDTO;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.validation.GenericValidator;
import com.isa.aeromart.services.endpoint.session.TransactionFactory;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

public abstract class StatefulController extends StatefulRequestAwareController {

	@Autowired
	private TransactionFactory transactionFactory;

	private static final String X_FORWARDED_FOR_HEADER = "X-Forwarded-For";
	private static final String USER_AGENT_HEADER = "User-Agent";
	private static final String APPLICATION_INDICATOR_PARAM = "applicationIndicator";

	public <T> T getTrasactionStore(String transactionId) {
		return getTransactionFactory().getTransactionStore(transactionId);
	}

	public String initTransaction(String sessionKey, String transactionId) {
		return getTransactionFactory().initTransaction(sessionKey, transactionId);
	}

	public void terminateTransaction(String transactionId) {
		getTransactionFactory().terminateTransaction(transactionId);
	}

	public <T> T getSessionStore(String sessionKey) {
		return getTransactionFactory().getSessionStore(sessionKey);
	}

	private TransactionFactory getTransactionFactory() {
		return transactionFactory;
	}

	public void setTransactionFactory(TransactionFactory transactionFactory) {
		this.transactionFactory = transactionFactory;
	}

	protected ClientCommonInfoDTO getClientCommonInfoDTO() {

		ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();
		// clientIps = clientIP, proxyIP1, proxyIP2
		String clientIps = getRequest().getHeader(X_FORWARDED_FOR_HEADER);
		String clientIp = null;

		if (clientIps != null && !clientIps.trim().isEmpty()) {
			String[] clientIpArray = clientIps.split(Constants.COMMA_SEPARATOR);
			if (clientIpArray.length > 0) {
				for (String ipAddress : clientIpArray) {
					if (ipAddress != null && !ipAddress.trim().isEmpty()) {
						clientIp = ipAddress.trim();
						break;
					}
				}
			}

		}

		if (clientIp == null || clientIp.equals("")) {
			clientIp = getRequest().getRemoteAddr();
		}
		clientInfoDTO.setIpAddress(clientIp);
		clientInfoDTO.setFwdIpAddress(getRequest().getHeader(""));
		clientInfoDTO.setUrl(getRequest().getRequestURL().toString());
		clientInfoDTO.setBrowser(getRequest().getHeader(USER_AGENT_HEADER));
		UserPrincipal userPrincipal = (UserPrincipal) getRequest().getUserPrincipal();
		// DO not pass the defaultCarrier directly here. It will affect the dry
		// user functionalities.Insted use the
		// userPrincipal.getDefaultCarrierCode
		// if it's not null
		String defaultCArrier = (userPrincipal == null) ? AppSysParamsUtil.getDefaultCarrierCode()
				: userPrincipal.getDefaultCarrierCode();
		clientInfoDTO.setCarrierCode(defaultCArrier);
		return clientInfoDTO;
	}

	protected TrackInfoDTO getTrackInfo() {
		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		
		TrackInfoDTO trackInfo = new TrackInfoDTO();
		trackInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		trackInfo.setIpAddress(WebplatformUtil.getClientIpAddress(getRequest()));

		BasicTrackInfo bt = TrackInfoUtil.getBasicTrackInfo(getRequest());
		trackInfo.setSessionId(bt.getSessionId());
		trackInfo.setCustomerId(customerSessionStore.getLoggedInCustomerID());
		trackInfo.setOriginChannelId(TrackInfoUtil.getWebOriginSalesChannel(getRequest()));
		trackInfo.setAppIndicator(TrackInfoUtil.getAppIndicatorEnum(trackInfo.getOriginChannelId()));
		trackInfo.setCallingInstanceId(bt.getCallingInstanceId());
		return trackInfo;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(new GenericValidator());
	}

}
