package com.isa.aeromart.services.endpoint.controller.modification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ConfirmationResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.RequoteConfirmAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.ancillary.AncillaryService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.delegate.modification.ModificationService;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.booking.BookingConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.modification.ReQuoteConfirmRQ;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.booking.AdvertiesmentUtil;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

@Controller
@RequestMapping("modification")
public class ModificationController extends StatefulController {

	@Autowired
	private AncillaryService ancillaryService;

	@Autowired
	private ModificationService modificationService;

	@ResponseBody
	@RequestMapping(value = "/requoteConfirmation", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			BookingConfirmationRS confirmRequoteModification(@RequestBody ReQuoteConfirmRQ reQuoteConfirmRequest)
					throws ModuleException {
		LCCClientReservation modifyreservation;
		BookingConfirmationRS bookingConfirmationRS = new BookingConfirmationRS();
		RequoteSessionStore requoteSessionStore = getRequoteSessionStore(reQuoteConfirmRequest.getTransactionId());		
		List<ReservationPaxTO> anciIntegratedPaxList = getPaxListWithSelectedAncillaryDTOs(requoteSessionStore
				.getRequoteBalanceReqiredParams().isModifySegment(), reQuoteConfirmRequest);
		RequoteModifyRQ requoteModifyRQ = RequoteConfirmAdaptor.getRequoteModifyRQ(
				requoteSessionStore,
				getTrackInfo(),
				getReservationInsurances(requoteSessionStore.getRequoteBalanceReqiredParams().isModifySegment(),
						reQuoteConfirmRequest), anciIntegratedPaxList);		
		try {
			modifyreservation = ModuleServiceLocator.getAirproxyReservationBD()
					.requoteModifySegmentsWithAutoRefundForIBE(requoteModifyRQ, getTrackInfo());
		} catch (ModuleException ex) {

			modifyreservation = new LCCClientReservation();
			modifyreservation.setSuccessfulRefund(false);
		}

		modifyreservation = loadReservationWithSessionParams(requoteModifyRQ.getPnr(), requoteModifyRQ.getGroupPnr(),
				requoteSessionStore.getResInfo().getCustomerId());

		modificationService.modifyFFIDInfo(requoteSessionStore.getFFIDChangeRQ(), modifyreservation, getTrackInfo());
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		ConfirmationResAdaptor confirmationResAdaptor = new ConfirmationResAdaptor(
				AdvertiesmentUtil.getCarURLForAdvertiesment(modifyreservation),
				AdvertiesmentUtil.getHotelURLForAdvertiesment(modifyreservation),
				masterDataService.getCurrencyExchangeRate(modifyreservation.getLastCurrencyCode()));
		bookingConfirmationRS = confirmationResAdaptor.adapt(modifyreservation);

		bookingConfirmationRS.setSuccess(true);
		terminateTransaction(requoteModifyRQ.getTransactionIdentifier());

		boolean voidAutoRefundEnabled = AppSysParamsUtil.isVoidAutoRefundEnabled();
		boolean ibePaxCreditAutoRefundEnabled = AppSysParamsUtil.isIBEPaxCreditAutoRefundEnabled();

		if (!modifyreservation.isSuccessfulRefund() && (voidAutoRefundEnabled || ibePaxCreditAutoRefundEnabled)) {
			List<String> errors = new ArrayList<String>();
			errors.add("ERROR :: AUTOMATIC REFUND FAILED");
			bookingConfirmationRS.setErrors(errors);
		}
		// send medical ssr email
		SSRServicesUtil.sendMedicalSsrEmail(modifyreservation);
		
		return bookingConfirmationRS;
	}

	private RequoteSessionStore getRequoteSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private LCCClientReservation loadReservationWithSessionParams(String pnr, String groupPnr, String customerId) throws ModuleException {

		boolean isGroupPnr = false;
		if (groupPnr != null && !groupPnr.equals("")) {
			isGroupPnr = true;
		}
		
		LCCClientPnrModesDTO pnrModesDTO = ModificationUtils.getPnrModesDTO(pnr, isGroupPnr, getTrackInfo().getCarrierCode(), "");
		ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
		modificationParamRQInfo.setAppIndicator(AppIndicatorEnum.APP_IBE);
		modificationParamRQInfo.setIsRegisteredUser((customerId != null && !customerId.isEmpty()) ? true : false);

		return ModuleServiceLocator.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, modificationParamRQInfo,
				getTrackInfo());
	}

	private List<LCCClientReservationInsurance> getReservationInsurances(boolean isModifySegment,
			ReQuoteConfirmRQ reQuoteConfirmRequest) throws ModuleException {
		List<LCCClientReservationInsurance> reservationInsurances = null;
		if (isModifySegment) {
			if (ancillaryService != null) {
				AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(getTrackInfo(),
						reQuoteConfirmRequest);
				if (ancillaryIntegrateTo != null && ancillaryIntegrateTo.getReservation() != null) {
					reservationInsurances = ancillaryIntegrateTo.getReservation().getReservationInsurances();
				}
			}
		}
		return reservationInsurances;
	}

	private List<ReservationPaxTO> getPaxListWithSelectedAncillaryDTOs(boolean isModifySegment,
			ReQuoteConfirmRQ reQuoteConfirmRequest) throws ModuleException {
		List<ReservationPaxTO> paxList = null;
		if (isModifySegment) {
			if (ancillaryService != null) {
				AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(getTrackInfo(),
						reQuoteConfirmRequest);
				if (ancillaryIntegrateTo != null && ancillaryIntegrateTo.getReservation() != null) {
					paxList = new ArrayList<ReservationPaxTO>();
					List<AncillaryIntegratePassengerTO> anciPassengers = ancillaryIntegrateTo.getReservation().getPassengers();
					for (AncillaryIntegratePassengerTO anciPax : anciPassengers) {
						ReservationPaxTO resPax = new ReservationPaxTO();
						resPax.setSeqNumber(Integer.parseInt(anciPax.getPassengerRph()));
						for (LCCSelectedSegmentAncillaryDTO anciDTO : anciPax.getSelectedAncillaries()) {
							resPax.addSelectedAncillaries(anciDTO);
						}
						paxList.add(resPax);
					}
				}
			}
		}
		return paxList;
	}

}