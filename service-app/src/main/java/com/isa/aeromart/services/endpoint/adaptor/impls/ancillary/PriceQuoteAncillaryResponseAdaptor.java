package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciIntegration;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AutoCheckinInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.InFlightSsrInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItems;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class PriceQuoteAncillaryResponseAdaptor extends
		TransactionAwareAdaptor<PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper, PriceQuoteAncillaryRS> {

	private PriceQuoteAncillaryRQ priceQuoteAncillariesRequest;

	private Map<String, AncillaryPassenger> anciPassengerMap;

	private String flightSegmentRef;

	private AncillariesConstants.AncillaryType ancyType;

	private BigDecimal quotedAdultCharge;

	private BigDecimal quotedChildCharge;

	private BigDecimal quotedInfantCharge;

	private String anciId;

	private MetaData metaData;
	
	private Map<AirportScope, List<String>> airportWiseAirportServiceForPassenger;
	
	private Map<String, Map<AirportScope, List<String>>> paxWiseAirportServiceAllocation;
	
	private String anciCategoryCode;

	@Override
	public PriceQuoteAncillaryRS adapt(PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper source) {

		String flightSegmentRph;

		PriceQuoteAncillaryRS priceQuoteAncillariesResponse = new PriceQuoteAncillaryRS();
		priceQuoteAncillariesResponse.setTransactionId(getTransactionId());

		AncillaryReservation ancillaryReservation = new AncillaryReservation();
		anciPassengerMap = new HashMap<String, AncillaryPassenger>();
		paxWiseAirportServiceAllocation = new HashMap<String, Map<AirportScope,List<String>>>();

		LCCAncillaryQuotation lccAncillaryQuotation = source.getLccAncillaryQuotation();
		priceQuoteAncillariesRequest = source.getPriceQuoteAncillariesRequest();

		priceQuoteAncillariesResponse.setReservationData(priceQuoteAncillariesRequest.getReservationData());
		priceQuoteAncillariesResponse.setMetaData(priceQuoteAncillariesRequest.getMetaData());
		metaData = priceQuoteAncillariesRequest.getMetaData();

		if (lccAncillaryQuotation.getSegmentQuotations() != null) {
			for (LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO : lccAncillaryQuotation.getSegmentQuotations()) {
				flightSegmentRph = lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber();
				flightSegmentRef = getTransactionalAncillaryService().getRphGenerator(getTransactionId())
						.getRPH(flightSegmentRph);

				if (lccSelectedSegmentAncillaryDTO.getAirportServiceDTOs() != null) {
					for (LCCAirportServiceDTO lccAirportServiceDTO : lccSelectedSegmentAncillaryDTO.getAirportServiceDTOs()) {
						ancyType = AncillariesConstants.AncillaryType.SSR_AIRPORT;
						anciId = lccAirportServiceDTO.getSsrCode();
						if (LCCAirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(lccAirportServiceDTO.getApplicabilityType())) {
							BigDecimal perPaxCharge = getPerPaxAirportServiceChargesForReservation(lccAirportServiceDTO
									.getReservationAmount());
							quotedAdultCharge = perPaxCharge;
							quotedChildCharge = perPaxCharge;
							quotedInfantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						} else {
							quotedAdultCharge = lccAirportServiceDTO.getAdultAmount();
							quotedChildCharge = lccAirportServiceDTO.getChildAmount();
							quotedInfantCharge = lccAirportServiceDTO.getInfantAmount();
						}
						updateAnciExistsForPassenger(null, lccAirportServiceDTO.getAirportCode());
					}
				}
				if (lccSelectedSegmentAncillaryDTO.getBaggageDTOs() != null) {
					for (LCCBaggageDTO lccBaggageDTO : lccSelectedSegmentAncillaryDTO.getBaggageDTOs()) {
						ancyType = AncillariesConstants.AncillaryType.BAGGAGE;
						quotedAdultCharge = lccBaggageDTO.getBaggageCharge();
						quotedChildCharge = lccBaggageDTO.getBaggageCharge();
						quotedInfantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						anciId = lccBaggageDTO.getOndBaggageChargeId() + "|" + lccBaggageDTO.getBaggageName() + "|";
						updateAnciExistsForPassenger(resolveOndId(flightSegmentRef), null);
					}
				}
				if (lccSelectedSegmentAncillaryDTO.getExtraSeatDTOs() != null) {
					for (LCCAirSeatDTO lccAirSeatDTO : lccSelectedSegmentAncillaryDTO.getExtraSeatDTOs()) {
						ancyType = AncillariesConstants.AncillaryType.SEAT;
						quotedAdultCharge = lccAirSeatDTO.getSeatCharge();
						quotedChildCharge = lccAirSeatDTO.getSeatCharge();
						quotedInfantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						anciId = lccAirSeatDTO.getSeatNumber();
						updateAnciExistsForPassenger(null, null);
					}
				}
				if (lccSelectedSegmentAncillaryDTO.getMealDTOs() != null) {
					for (LCCMealDTO lccMealDTO : lccSelectedSegmentAncillaryDTO.getMealDTOs()) {
						ancyType = AncillariesConstants.AncillaryType.MEAL;
						quotedAdultCharge = lccMealDTO.getMealCharge();
						quotedChildCharge = lccMealDTO.getMealCharge();
						quotedInfantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						anciId = lccMealDTO.getMealCode();
						anciCategoryCode = lccMealDTO.getMealCategoryCode();
						updateAnciExistsForPassenger(null, null);
					}
				}
				if (lccSelectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs() != null) {
					ancyType = AncillariesConstants.AncillaryType.SSR_IN_FLIGHT;
					for (LCCSpecialServiceRequestDTO lccSpecialServiceRequestDTO : lccSelectedSegmentAncillaryDTO
							.getSpecialServiceRequestDTOs()) {
						quotedAdultCharge = lccSpecialServiceRequestDTO.getCharge();
						quotedChildCharge = lccSpecialServiceRequestDTO.getCharge();
						quotedInfantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						anciId = lccSpecialServiceRequestDTO.getSsrCode();
						updateAnciExistsForPassenger(null, null);
					}
				}
				if (lccSelectedSegmentAncillaryDTO.getAirportTransferDTOs() != null) {
					for (LCCAirportServiceDTO lccAirportTransfer : lccSelectedSegmentAncillaryDTO.getAirportTransferDTOs()) {
						ancyType = AncillariesConstants.AncillaryType.AIRPORT_TRANSFER;
						quotedAdultCharge = lccAirportTransfer.getAdultAmount();
						quotedChildCharge = lccAirportTransfer.getChildAmount();
						quotedInfantCharge = lccAirportTransfer.getInfantAmount();
						anciId = lccAirportTransfer.getSsrCode();
						updateAnciExistsForPassenger(null, lccAirportTransfer.getAirportCode());
					}
				}
				if (lccSelectedSegmentAncillaryDTO.getAutomaticCheckinDTOs() != null) {
					lccSelectedSegmentAncillaryDTO.getAutomaticCheckinDTOs().forEach(lccAutomaticCheckinDTO -> {
						ancyType = AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN;
						quotedAdultCharge = lccAutomaticCheckinDTO.getAutomaticCheckinCharge();
						quotedChildCharge = lccAutomaticCheckinDTO.getAutomaticCheckinCharge();
						quotedInfantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						anciId = lccAutomaticCheckinDTO.getAutoCheckinId().toString();
						updateAnciExistsForPassenger(null, lccAutomaticCheckinDTO.getAirportCode());
					});
				}
			}
		}

		ancillaryReservation.setPassengers(new ArrayList<AncillaryPassenger>(anciPassengerMap.values()));
		priceQuoteAncillariesResponse.setAncillaryReservation(ancillaryReservation);
		if (priceQuoteAncillariesRequest.getAncillaries() != null && !priceQuoteAncillariesRequest.getAncillaries().isEmpty()) {
			updateInsuranceDetails(priceQuoteAncillariesResponse);
			updateFlexiDetails(priceQuoteAncillariesResponse);
		}
		priceQuoteAncillariesResponse.setSuccess(true);
		return priceQuoteAncillariesResponse;
	}

	private void updateAnciExistsForPassenger(List<String> ondIds, String airport) {

		for (SelectedAncillary selectedAncillary : priceQuoteAncillariesRequest.getAncillaries()) {
			if (selectedAncillary.getType().equals(ancyType.name())) {

				for (Preference preference : selectedAncillary.getPreferences()) {
					if (preference.getAssignee() instanceof PassengerAssignee) {
						airportWiseAirportServiceForPassenger = new HashMap<AirportScope, List<String>>();
						PassengerAssignee pasenger = (PassengerAssignee) preference.getAssignee();
						if(!paxWiseAirportServiceAllocation.containsKey(pasenger.getPassengerRph())){
							paxWiseAirportServiceAllocation.put(pasenger.getPassengerRph(), new HashMap<AirportScope, List<String>>());
						}
						airportWiseAirportServiceForPassenger = paxWiseAirportServiceAllocation.get(pasenger.getPassengerRph());
						for (Selection selection : preference.getSelections()) {
							if (selection.getScope().getScopeType().equals(Scope.ScopeType.SEGMENT)) {
								SegmentScope segmentScope = (SegmentScope) selection.getScope();
								if (segmentScope.getFlightSegmentRPH().equals(flightSegmentRef)) {
									populateAnciMapWithData(segmentScope, selection, pasenger);
								}
							} else if (selection.getScope().getScopeType().equals(Scope.ScopeType.AIRPORT)) {
								AirportScope airportScope = (AirportScope) selection.getScope();
								if (airportScope.getFlightSegmentRPH().equals(flightSegmentRef) && (airportScope.getAirportCode().equals(airport) || airport.equals("***"))) {
									populateAnciMapWithData(airportScope, selection, pasenger);
								}
							} else if (selection.getScope().getScopeType().equals(Scope.ScopeType.OND)) {
								OndScope ondScope = (OndScope) selection.getScope();
								if (ondIds.contains(ondScope.getOndId())) {
									populateAnciMapWithData(ondScope, selection, pasenger);
								}
							}
						}
					}
				}
			}
		}
	}

	private void populateAnciMapWithData(Scope scope, Selection selection, PassengerAssignee pasenger) {

		AncillaryPassenger ancillaryPassenger;
		PricedAncillaryType pricedAncillaryType;
		AncillaryScope ancillaryScope;

		for (SelectedItem selectedItem : selection.getSelectedItems()) {
			if (selectedItem.getId().equals(anciId)) {
				if (!anciPassengerMap.containsKey(pasenger.getPassengerRph())) {
					ancillaryPassenger = new AncillaryPassenger();
					ancillaryPassenger.setPassengerRph(pasenger.getPassengerRph());
					anciPassengerMap.put(pasenger.getPassengerRph(), ancillaryPassenger);
				}
				ancillaryPassenger = anciPassengerMap.get(pasenger.getPassengerRph());
				PricedSelectedItem pricedAnciItem = new PricedSelectedItem();
				pricedAnciItem.setId(selectedItem.getId());
				pricedAnciItem.setName(selectedItem.getName());
				pricedAnciItem.setQuantity(selectedItem.getQuantity());
				pricedAnciItem.setMealCategoryCode(anciCategoryCode);
				if (selectedItem.getInput() != null && selectedItem.getInput() instanceof InFlightSsrInput) {
					InFlightSsrInput previousInput = (InFlightSsrInput) selectedItem.getInput();
					InFlightSsrInput input = new InFlightSsrInput();
					input.setComment(previousInput.getComment());
					pricedAnciItem.setInput(input);
				}
				if (selectedItem.getInput() != null && selectedItem.getInput() instanceof AutoCheckinInput) {
					AutoCheckinInput previousInput = (AutoCheckinInput) selectedItem.getInput();
					AutoCheckinInput input = new AutoCheckinInput();
					input.setEmail(previousInput.getEmail());
					input.setSeatCode(previousInput.getSeatCode());
					pricedAnciItem.setInput(input);
				}
				pricedAnciItem.setAmount(getQuotedPaxAnciCharge(pasenger.getPassengerRph()));

				if (ancillaryPassenger.getAncillaryTypes() == null) {
					ancillaryPassenger.setAncillaryTypes(new ArrayList<PricedAncillaryType>());
				}
				if (!pricedAncillaryTypeExistsInList(ancillaryPassenger.getAncillaryTypes())) {
					pricedAncillaryType = new PricedAncillaryType();
					pricedAncillaryType.setAncillaryType(ancyType.name());
					pricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());
					ancillaryPassenger.getAncillaryTypes().add(pricedAncillaryType);
				}
				for (PricedAncillaryType pricedAncillaryTypeRef : ancillaryPassenger.getAncillaryTypes()) {
					if (pricedAncillaryTypeRef.getAncillaryType().equals(ancyType.name())) {
						if (!ancillaryScopeExistsInList(pricedAncillaryTypeRef.getAncillaryScopes(), scope)) {
							ancillaryScope = new AncillaryScope();
							ancillaryScope.setScope(scope);
							ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());
							pricedAncillaryTypeRef.getAncillaryScopes().add(ancillaryScope);
						}
						for (AncillaryScope ancillaryScopeRef : pricedAncillaryTypeRef.getAncillaryScopes()) {
							if (ancillaryScopeRef.getScope() instanceof SegmentScope && !(ancillaryScopeRef.getScope() instanceof AirportScope)) {
								SegmentScope segmentScope = (SegmentScope) ancillaryScopeRef.getScope();
								SegmentScope lccRSSegmentScope = (SegmentScope) scope;
								if (segmentScope.getFlightSegmentRPH().equals(lccRSSegmentScope.getFlightSegmentRPH())) {
									ancillaryScopeRef.getAncillaries().add(pricedAnciItem);
								}
							} else if (ancillaryScopeRef.getScope() instanceof AirportScope) {
								AirportScope airportScope = (AirportScope) ancillaryScopeRef.getScope();
								AirportScope lccRSAirportScope = (AirportScope) scope;
								if (airportScope.getFlightSegmentRPH().equals(lccRSAirportScope.getFlightSegmentRPH())
										&& (airportScope.getAirportCode().equals(lccRSAirportScope.getAirportCode()) || airportScope
												.getAirportCode().equals("***"))) {
									if(!airportWiseAirportServiceForPassenger.containsKey(airportScope)){
										airportWiseAirportServiceForPassenger.put(airportScope, new  ArrayList<String>());
									}
									List<String> airportServiceList = airportWiseAirportServiceForPassenger.get(airportScope);
									if(!airportServiceList.contains(pricedAnciItem.getId())){
										ancillaryScopeRef.getAncillaries().add(pricedAnciItem);
										airportServiceList.add(pricedAnciItem.getId());
									}
								}
							} else if (ancillaryScopeRef.getScope() instanceof OndScope) {
								OndScope ondScope = (OndScope) ancillaryScopeRef.getScope();
								OndScope lccRSOndScope = (OndScope) scope;
								if (ondScope.getOndId().equals(lccRSOndScope.getOndId())) {
									if (ancillaryScopeRef.getAncillaries().size() > 0) {
										for (PricedSelectedItem pricedSelectedOndItem : ancillaryScopeRef.getAncillaries()) {
											if (!pricedSelectedOndItem.getId().equals(pricedAnciItem.getId())) {
												ancillaryScopeRef.getAncillaries().add(pricedAnciItem);
											} else if (pricedSelectedOndItem.getId().equals(pricedAnciItem.getId())
													&& pricedSelectedOndItem.getAmount().equals(pricedAnciItem.getAmount())) {
												if (pricedSelectedOndItem.getQuantity() == null) {
													pricedSelectedOndItem.setQuantity(1);
												}
												if (pricedAnciItem.getQuantity() == null) {
													pricedAnciItem.setQuantity(1);
												}
												pricedSelectedOndItem.setQuantity(
														pricedSelectedOndItem.getQuantity() + pricedAnciItem.getQuantity());
												pricedAnciItem.setQuantity(
														pricedSelectedOndItem.getQuantity() + pricedAnciItem.getQuantity());
											}
										}
									} else {
										ancillaryScopeRef.getAncillaries().add(pricedAnciItem);
									}
								}
							}
						}
					}
				}

				ancillaryPassenger.setAmount(AnciIntegration.getAncillarySelectionAmount(ancillaryPassenger.getAncillaryTypes()));
				anciPassengerMap.put(pasenger.getPassengerRph(), ancillaryPassenger);
			}
		}
	}

	private BigDecimal getQuotedPaxAnciCharge(String passengerRph) {
		BigDecimal paxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		List<SessionPassenger> paxList = getTransactionalAncillaryService().getReservationData(getTransactionId()).getPassengers();
		for (SessionPassenger pax : paxList) {
			if (pax.getPassengerRph().equals(passengerRph)) {
				if (PaxTypeTO.ADULT.equals(pax.getType())) {
					if (pax.isParent()) {
						paxCharge = AccelAeroCalculator.add(quotedAdultCharge, quotedInfantCharge);
					} else {
						paxCharge = quotedAdultCharge;
					}
				} else if (PaxTypeTO.CHILD.equals(pax.getType())) {
					paxCharge = quotedChildCharge;
				}
				break;
			}
		}
		return paxCharge;
	}

	private void updateInsuranceDetails(PriceQuoteAncillaryRS priceQuoteAncillariesResponse) {
		List<String> selectedInsuranceRefList = new ArrayList<String>();
		PricedAncillaryType pricedAncillaryType;
		AncillaryScope ancillaryScope; // since insurance are allocated for ALL_SEGMENT only one ancillaryScope needed
		Scope scope;
		PricedSelectedItem pricedSelectedItem;

		AncillaryQuotation ancillaryQuotation = getTransactionalAncillaryService().getAncillaryQuotation(getTransactionId());
		List<SelectedAncillary> selectedAncillaries = priceQuoteAncillariesRequest.getAncillaries();
		pricedAncillaryType = new PricedAncillaryType();
		pricedAncillaryType.setAncillaryType(AncillariesConstants.AncillaryType.INSURANCE.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());
		ancillaryScope = new AncillaryScope();
		scope = new Scope();
		scope.setScopeType(Scope.ScopeType.ALL_SEGMENTS);
		ancillaryScope.setScope(scope);
		ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());

		if (ancillaryQuotation.getQuotedAncillaries() != null) {

			for (SelectedAncillary selectedAncillary : selectedAncillaries) {
				if (selectedAncillary.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())) {
					if (selectedAncillary.getPreferences() != null && selectedAncillary.getPreferences().size() > 0) {
						for (Preference Preference : selectedAncillary.getPreferences()) {
							for (Selection selection : Preference.getSelections()) {
								for (SelectedItem selectedItem : selection.getSelectedItems()) {
									selectedInsuranceRefList.add(selectedItem.getId());
								}
							}
						}
					}
				}
			}
			if (selectedInsuranceRefList.size() > 0) {
				for (AncillaryType ancillaryType : ancillaryQuotation.getQuotedAncillaries()) {
					if (ancillaryType.getAncillaryType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())) {
						for (Provider provider : ancillaryType.getProviders()) {
							for (AvailableAncillaryUnit availableAncillaryUnit : provider.getAvailableAncillaries()
									.getAvailableUnits()) {
								InsuranceItems insuranceItems = (InsuranceItems) availableAncillaryUnit.getItemsGroup();
								for (InsuranceItem insuranceItem : insuranceItems.getItems()) {
									if (selectedInsuranceRefList.contains(insuranceItem.getItemId())) {
										pricedSelectedItem = new PricedSelectedItem();
										pricedSelectedItem.setId(insuranceItem.getItemId());
										pricedSelectedItem.setAmount(insuranceItem.getCharges().get(0).getAmount());
										ancillaryScope.getAncillaries().add(pricedSelectedItem);
									}
								}
							}
						}
					}
				}
				pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
				if (priceQuoteAncillariesResponse.getAncillaryReservation().getAncillaryTypes() == null) {
					priceQuoteAncillariesResponse.getAncillaryReservation().setAncillaryTypes(
							new ArrayList<PricedAncillaryType>());
				}
				priceQuoteAncillariesResponse.getAncillaryReservation().getAncillaryTypes().add(pricedAncillaryType);
			}
		}
	}

	private void updateFlexiDetails(PriceQuoteAncillaryRS priceQuoteAncillariesResponse) {
		List<String> selectedFlexiRefList = new ArrayList<String>();
		Map<String, List<String>> ondFlexiSelecionMap = new HashMap<String, List<String>>();
		PricedAncillaryType pricedAncillaryType;
		AncillaryScope ancillaryScope = null;
		OndScope ondScope;
		PricedSelectedItem pricedSelectedItem;

		AncillaryQuotation ancillaryQuotation = getTransactionalAncillaryService().getAncillaryQuotation(getTransactionId());
		List<SelectedAncillary> selectedAncillaries = priceQuoteAncillariesRequest.getAncillaries();
		pricedAncillaryType = new PricedAncillaryType();
		pricedAncillaryType.setAncillaryType(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());

		if (ancillaryQuotation.getQuotedAncillaries() != null) {

			for (SelectedAncillary selectedAncillary : selectedAncillaries) {
				if (selectedAncillary.getType().equals(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode())) {
					if (selectedAncillary.getPreferences() != null && selectedAncillary.getPreferences().size() > 0) {
						for (Preference Preference : selectedAncillary.getPreferences()) {
							for (Selection selection : Preference.getSelections()) {
								ondScope = (OndScope) selection.getScope();
								selectedFlexiRefList = new ArrayList<String>();
								for (SelectedItem selectedItem : selection.getSelectedItems()) {
									selectedFlexiRefList.add(selectedItem.getId());
								}
								ondFlexiSelecionMap.put(ondScope.getOndId(), selectedFlexiRefList);
							}
						}
					}
				}
			}
			if (ondFlexiSelecionMap.size() > 0) {
				for (AncillaryType ancillaryType : ancillaryQuotation.getQuotedAncillaries()) {
					if (ancillaryType.getAncillaryType().equals(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode())) {
						for (Provider provider : ancillaryType.getProviders()) {
							for (AvailableAncillaryUnit availableAncillaryUnit : provider.getAvailableAncillaries()
									.getAvailableUnits()) {
								ondScope = (OndScope) availableAncillaryUnit.getScope();
								ancillaryScope = new AncillaryScope();
								ancillaryScope.setScope(ondScope);
								ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());
								FlexiItems flexiItems = (FlexiItems) availableAncillaryUnit.getItemsGroup();
								for (FlexiItem flexiItem : flexiItems.getItems()) {
									if (ondFlexiSelecionMap.get(ondScope.getOndId()) != null
											&& ondFlexiSelecionMap.get(ondScope.getOndId()).contains(flexiItem.getItemId())
											&& !flexiItem.getDefaultSelected()) {
										pricedSelectedItem = new PricedSelectedItem();
										pricedSelectedItem.setId(flexiItem.getItemId());
										pricedSelectedItem.setName(flexiItem.getItemName());
										pricedSelectedItem.setAmount(flexiItem.getCharges().get(0).getAmount());
										ancillaryScope.getAncillaries().add(pricedSelectedItem);
									}
								}
								if (ancillaryScope != null && ancillaryScope.getAncillaries().size() > 0) {
									pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
								}
							}
						}
					}
				}

				if (priceQuoteAncillariesResponse.getAncillaryReservation().getAncillaryTypes() == null) {
					priceQuoteAncillariesResponse.getAncillaryReservation().setAncillaryTypes(
							new ArrayList<PricedAncillaryType>());
				}
				priceQuoteAncillariesResponse.getAncillaryReservation().getAncillaryTypes().add(pricedAncillaryType);
			}
		}
	}

	private boolean ancillaryScopeExistsInList(List<AncillaryScope> anciScope, Scope scope) {

		for (AncillaryScope ancillaryScope : anciScope) {
			if (ancillaryScope.getScope() instanceof SegmentScope && !(ancillaryScope.getScope() instanceof AirportScope)) {
				SegmentScope segmentScope = (SegmentScope) ancillaryScope.getScope();
				SegmentScope lccRSSegmentScope = (SegmentScope) scope;
				if (segmentScope.getFlightSegmentRPH().equals(lccRSSegmentScope.getFlightSegmentRPH())) {
					return true;
				}
			} else if (ancillaryScope.getScope() instanceof AirportScope) {
				AirportScope airportScope = (AirportScope) ancillaryScope.getScope();
				AirportScope lccRSAirportScope = (AirportScope) scope;
				if (airportScope.getFlightSegmentRPH().equals(lccRSAirportScope.getFlightSegmentRPH())
						&& (airportScope.getAirportCode().equals(lccRSAirportScope.getAirportCode()) || airportScope
								.getAirportCode().equals("***"))) {
					return true;
				}
			} else if (ancillaryScope.getScope() instanceof OndScope) {
				OndScope ondScope = (OndScope) ancillaryScope.getScope();
				OndScope lccRSOndScope = (OndScope) scope;
				if (ondScope.getOndId().equals(lccRSOndScope.getOndId())) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean pricedAncillaryTypeExistsInList(List<PricedAncillaryType> anciTypeList) {

		for (PricedAncillaryType pricedAncillaryType : anciTypeList) {
			if (pricedAncillaryType.getAncillaryType().equals(ancyType.name())) {
				return true;
			}
		}
		return false;
	}

	private List<String> resolveOndId(String fltegRef) {
		List<String> ondIds = new ArrayList<String>();
		if (metaData != null && metaData.getOndPreferences() != null && metaData.getOndPreferences().size() > 0) {
			for (OndUnit OndUnit : metaData.getOndPreferences()) {
				if (OndUnit.getFlightSegmentRPH() != null && OndUnit.getFlightSegmentRPH().size() > 0) {
					for (String fltSegRefId : OndUnit.getFlightSegmentRPH()) {
						if (fltegRef.equals(fltSegRefId)) {
							ondIds.add(OndUnit.getOndId());
						}
					}
				}
			}
		}
		return ondIds;
	}
	
	private BigDecimal getPerPaxAirportServiceChargesForReservation(BigDecimal totalCharges) {
		List<SessionPassenger> paxList = getTransactionalAncillaryService().getReservationData(getTransactionId())
				.getPassengers();
		int paxTypes = 0;
		for (SessionPassenger pax : paxList) {
			if (PaxTypeTO.ADULT.equals(pax.getType())) {
				paxTypes++;

			} else if (PaxTypeTO.CHILD.equals(pax.getType())) {
				paxTypes++;
			}
		}
		BigDecimal perPaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxTypes > 0) {
			perPaxCharge = AccelAeroCalculator.divide(totalCharges, paxTypes);
		}
		return perPaxCharge;
	}

	public static class AvailablePriceQuoteResponseWrapper {

		private LCCAncillaryQuotation lccAncillaryQuotation;

		private PriceQuoteAncillaryRQ priceQuoteAncillariesRequest;

		public LCCAncillaryQuotation getLccAncillaryQuotation() {
			return lccAncillaryQuotation;
		}

		public void setLccAncillaryQuotation(LCCAncillaryQuotation lccAncillaryQuotation) {
			this.lccAncillaryQuotation = lccAncillaryQuotation;
		}

		public PriceQuoteAncillaryRQ getPriceQuoteAncillariesRequest() {
			return priceQuoteAncillariesRequest;
		}

		public void setPriceQuoteAncillariesRequest(PriceQuoteAncillaryRQ priceQuoteAncillariesRequest) {
			this.priceQuoteAncillariesRequest = priceQuoteAncillariesRequest;
		}

	}

}
