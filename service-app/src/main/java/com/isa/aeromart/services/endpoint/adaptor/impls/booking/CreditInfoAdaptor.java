package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.CreditInfo;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CreditInfoAdaptor implements Adaptor<CreditInfoDTO, CreditInfo> {

	@Override
	public CreditInfo adapt(CreditInfoDTO creditInfo) {
		CreditInfo credits = new CreditInfo();
		credits.setAmount(AccelAeroCalculator.formatAsDecimal(creditInfo.getAmount()));
		credits.setExpireDate(creditInfo.getExpireDate());
		credits.setCredRef(String.valueOf(creditInfo.getTxnId()));
		credits.setOperatingCarrier(creditInfo.getCarrierCode());
		return credits;
	}

}
