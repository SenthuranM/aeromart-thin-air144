package com.isa.aeromart.services.endpoint.controller.payment;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.ancillary.AncillaryService;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.payment.PaymentService;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRQ;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRS;
import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionRQ;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRS;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRS;
import com.isa.aeromart.services.endpoint.dto.payment.ReservationChargesRQ;
import com.isa.aeromart.services.endpoint.dto.payment.ReservationChargesRS;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.thinair.commons.api.exception.ModuleException;

@Controller
@RequestMapping(value = "payment", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
public class PaymentController extends StatefulController {

	private static Log log = LogFactory.getLog(PaymentController.class);

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private AncillaryService ancillaryService;

	@ResponseBody
	@RequestMapping(value = "/paymentMethods")
	public PaymentMethodsRS getPaymentMethods(@RequestBody PaymentMethodsRQ paymentMethodsRQ) throws ModuleException {

		return paymentService.getPaymentMethodsRS(paymentMethodsRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "create/paymentSummary")
	public PaymentSummaryRS getPaymentSummary(@RequestBody PaymentSummaryRQ paymentSummaryRQ) throws ModuleException {

		return paymentService.getPaymentSummaryRS(paymentSummaryRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "balance/adminFee")
	public AdministrationFeeRS getBalanceAdminFee(@RequestBody AdministrationFeeRQ adminFeeRQ) throws ModuleException {
		// session initiated
		String transactionId = initTransaction(BookingSessionStore.SESSION_KEY, adminFeeRQ.getTransactionId());
		adminFeeRQ.setTransactionId(transactionId);
		return paymentService.getBalanceAdminFee(adminFeeRQ, getTrackInfo(), transactionId);
	}

	@ResponseBody
	@RequestMapping(value = "requote/adminFee")
	public AdministrationFeeRS getRequoteAdminFee(@RequestBody AdministrationFeeRQ adminFeeRQ) throws ModuleException {
		return paymentService.getRequoteAdminFee(adminFeeRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "anci/modification/adminFee")
	public AdministrationFeeRS getAnciModificationAdminFee(@RequestBody AdministrationFeeRQ adminFeeRQ) throws ModuleException {
		return paymentService.getAnciModificationAdminFee(adminFeeRQ, getTrackInfo());
	}

	@ResponseBody
	@RequestMapping(value = "payfort/check")
	public PayfortCheckRS checkPayfort(@RequestBody PayfortCheckRQ payfortCheckRQ) throws ModuleException {

		return paymentService.checkPayfort(payfortCheckRQ, getTrackInfo());
	}

	@ResponseBody
	@RequestMapping(value = "/paymentOptions")
	public PaymentOptionsRS getPaymentOptions(@RequestBody PaymentOptionsRQ paymentOptionsRQ) throws ModuleException {
		
		return paymentService.getPaymentOptionsRS(paymentOptionsRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "balance/paymentOptions")
	public PaymentOptionsRS getBalancePaymentOptions(@RequestBody PaymentOptionsRQ paymentOptionsRQ) throws ModuleException {

		// session initiated
		String bookingTransactionId = initTransaction(BookingSessionStore.SESSION_KEY, paymentOptionsRQ.getTransactionId());

		return paymentService.getBalancePaymentOptionsRS(paymentOptionsRQ, getTrackInfo(), bookingTransactionId);

	}

	@ResponseBody
	@RequestMapping(value = "requote/paymentOptions")
	public PaymentOptionsRS getRequotePaymentOptions(@RequestBody PaymentOptionsRQ paymentOptionsRQ) throws ModuleException {
		
		return paymentService.getRequotePaymentOptionsRS(paymentOptionsRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "anci/modification/paymentOptions")
	public PaymentOptionsRS getAnciModificationPaymentOptions(@RequestBody PaymentOptionsRQ paymentOptionsRQ)
			throws ModuleException {

		return paymentService.getAnciModificationPaymentOptionsRS(paymentOptionsRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "/effectivePayment")
	public PaymentOptionsRS getEffectivePayment(@RequestBody EffectivePaymentRQ effectivePaymentRQ) throws ModuleException {
		
		return paymentService.getEffectivePaymentRS(effectivePaymentRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "balance/effectivePayment")
	public PaymentOptionsRS getEffectiveBalancePaymentOptions(@RequestBody EffectivePaymentRQ effectivePaymentRQ)
			throws ModuleException {
		
		return paymentService.getEffectiveBalancePaymentRS(effectivePaymentRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "requote/effectivePayment")
	public PaymentOptionsRS getEffectiveRequotePaymentOptions(@RequestBody EffectivePaymentRQ effectivePaymentRQ)
			throws ModuleException {
		
		return paymentService.getEffectiveRequotePaymentRS(effectivePaymentRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "anci/modification/effectivePayment")
	public PaymentOptionsRS getEffectiveAnciModificationPaymentOptions(@RequestBody EffectivePaymentRQ effectivePaymentRQ)
			throws ModuleException {

		return paymentService.getEffectiveAnciModificationPaymentRS(effectivePaymentRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "/create/booking/makePayment")
	public MakePaymentRS makePaymentCreateBooking(@RequestBody MakePaymentRQ makePaymentRQ) throws ModuleException {
		
		return paymentService.processCreateBookingPayment(makePaymentRQ, getTrackInfo(), getClientCommonInfoDTO());

	}

	@ResponseBody
	@RequestMapping(value = "/requote/booking/makePayment")
	public MakePaymentRS makePaymentModifyBooking(@RequestBody MakePaymentRQ makePaymentRQ) throws ModuleException {
		
		return paymentService.processModifyBookingPayment(makePaymentRQ, getTrackInfo(), getClientCommonInfoDTO());

	}

	@ResponseBody
	@RequestMapping(value = "/anci/modification/makePayment")
	public MakePaymentRS makePaymentAnciModification(@RequestBody MakePaymentRQ makePaymentRQ) throws ModuleException {
		
		return paymentService.processModifyAnciPayment(makePaymentRQ, getTrackInfo(), getClientCommonInfoDTO());

	}

	@ResponseBody
	@RequestMapping(value = "/balance/booking/makePayment")
	public TransactionalBaseRS makePaymentBalance(@RequestBody MakePaymentRQ makePaymentRQ) throws ModuleException {
		
		return paymentService.processBalancePayment(makePaymentRQ, getTrackInfo(), getClientCommonInfoDTO());

	}

	@ResponseBody
	@RequestMapping(value = "/binPromotion")
	public PaymentOptionsRS getBinPromotion(@RequestBody BinPromotionRQ binPromotionRQ) throws ModuleException {
		return paymentService.getBinPromotionRS(binPromotionRQ, getTrackInfo());

	}
	
	@ResponseBody
	@RequestMapping(value = "requote/chargeDetails")
	public ReservationChargesRS getReservationChargeDetails(
			@RequestBody ReservationChargesRQ reservationChargesRQ) throws ModuleException {
		return paymentService.getReservationChargeDetailsRS(reservationChargesRQ, getTrackInfo());

	}

	@ResponseBody
	@RequestMapping(value = "/createAlias", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			MakePaymentRS createAlias(@RequestBody MakePaymentRQ makePaymentRQ) throws ModuleException {
		return paymentService.createAlias(makePaymentRQ, getTrackInfo());
	}
	
	@ResponseBody
	@RequestMapping(value = "/issueVoucherPaymentOptions")
	public PaymentOptionsRS getIssueVoucherPaymentOptions(@RequestBody PaymentOptionsRQ paymentOptionsRQ) throws ModuleException {
		return paymentService.getIssueVoucherPaymentOptionsRS(getTrackInfo(), paymentOptionsRQ);
	}

}