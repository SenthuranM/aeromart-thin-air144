package com.isa.aeromart.services.endpoint.dto.availability;

public class FareCalendarSelectedRS extends FareCalendarRS {

	private SelectedFlightPricing selectedFlightPricing;

	public FareCalendarSelectedRS() {

	}

	public FareCalendarSelectedRS(FareCalendarRS fareCalendarResponse) {
		super(fareCalendarResponse);
	}

	public SelectedFlightPricing getSelectedFlightPricing() {
		return selectedFlightPricing;
	}

	public void setSelectedFlightPricing(SelectedFlightPricing selectedFlightPricing) {
		this.selectedFlightPricing = selectedFlightPricing;
	}

}
