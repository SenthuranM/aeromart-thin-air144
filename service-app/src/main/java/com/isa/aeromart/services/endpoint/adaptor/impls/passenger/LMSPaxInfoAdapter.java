package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.passenger.LMSPassenger;

public class LMSPaxInfoAdapter implements Adaptor<Passenger, LMSPassenger> {

	@Override
	public LMSPassenger adapt(Passenger passenger) {
		LMSPassenger lmsPassenger = new LMSPassenger();
		lmsPassenger.setFirstName(passenger.getFirstName());
		lmsPassenger.setLastName(passenger.getLastName());
		lmsPassenger.setPaxType(passenger.getPaxType());
		lmsPassenger.setPassengerSequence(passenger.getPaxSequence());
		if (passenger.getAdditionalInfo() != null) {
			lmsPassenger.setFfid(passenger.getAdditionalInfo().getFfid());
		}
		return lmsPassenger;
	}

}
