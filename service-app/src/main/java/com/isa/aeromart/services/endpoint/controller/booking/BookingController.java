package com.isa.aeromart.services.endpoint.controller.booking;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.IPGTransactionRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.PassengerTypeQuantityAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationAssemblerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerAliasAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.DuplicatePaxCheckAssemblerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.ValidatePaxFFIDAdapter;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.BookingType;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.ancillary.AncillaryService;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.delegate.passenger.PassengerService;
import com.isa.aeromart.services.endpoint.delegate.payment.PaymentService;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.booking.OnholdPnrRS;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.passenger.PaxValidationDetails;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRQ;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRS;
import com.isa.aeromart.services.endpoint.dto.payment.Card;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptions;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.ServiceTaxRQ;
import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.utils.booking.BookingUtil;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.aeromart.services.endpoint.utils.passenger.PassengerUtil;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.utils.CustomerAliasGenerator;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;

@Controller
@RequestMapping("reservation")
public class BookingController extends StatefulController {

	private static Log log = LogFactory.getLog(BookingController.class);
	private static Log log2 = LogFactory.getLog("com.isa.aeromart.services.analytics.issue");

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private AncillaryService ancillaryService;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private PassengerService passengerService;

	private Set<String> errorSet = new HashSet<>();

	@ResponseBody
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public TransactionalBaseRS createBooking(@RequestBody BookingRQ bookingRQ) throws ValidationException, ModuleException {

		BookingSessionStore bookingSessionStore = getBookingSession(bookingRQ.getTransactionId());
		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		String customerID = customerSessionStore.getLoggedInCustomerID();

		BookingRS response = doPrePaymentValidations(bookingRQ);

		if (!response.isSuccess()) {
			return response;
		}

		AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(getTrackInfo(), bookingRQ);

		ReservationAssemblerAdaptor reservationAssemblerAdaptor = new ReservationAssemblerAdaptor(getTrackInfo(),
				bookingSessionStore, ancillaryIntegrateTo, customerID);

		boolean onholdCreated = bookingSessionStore.isOnHoldCreated();
		Date onholdReleaseZuluTimeStamp = getOnHoldReleaseTime(bookingRQ);
		boolean isAllowOnhold = isAllowOnhold(bookingRQ, onholdReleaseZuluTimeStamp);

		CommonReservationAssembler commonReservationAssembler;
		checkSeatAvailability(bookingRQ);
		if (isAllowOnhold && !onholdCreated) { // either offline OHD or cash OHD
			if (onholdReleaseZuluTimeStamp != null) {
				bookingRQ.setOnholdReleaseZuluTimeStamp(onholdReleaseZuluTimeStamp);
			}
			commonReservationAssembler = reservationAssemblerAdaptor.adapt(bookingRQ);
			BookingRS bookingRS = bookingService.createBooking(commonReservationAssembler, ancillaryIntegrateTo, getTrackInfo());
			if (bookingRS.getPnr() != null) {
				String onholdReleaseTime = getOnholdReleaseDuration(onholdReleaseZuluTimeStamp);
				response.setOnholdReleaseDuration(onholdReleaseTime);
				bookingSessionStore.storePnr(bookingRS.getPnr());
				bookingSessionStore.storeOnholdReleaseTime(onholdReleaseTime);
				bookingSessionStore.storeLastName(bookingRQ.getReservationContact().getLastName());
				bookingSessionStore.storeFirstDepatureDate(getFirstDepartureDateFromSession(bookingRQ.getTransactionId()));
				onholdCreated = true;
			} else if (!bookingRS.isSuccess()) {
				errorSet.addAll(bookingRS.getErrors());
				response.setSuccess(bookingRS.isSuccess());
				response.getErrors().addAll(errorSet);
				response.getMessages().addAll(errorSet);
				return response;
			}
		}

		bookingSessionStore.storeOnHoldCreated(onholdCreated);

		PaymentOptions paymentOptions = bookingRQ.getMakePaymentRQ().getPaymentOptions();
		
		CustomerUtil.overrideAndInjectAlias(customerID, paymentOptions);

		boolean paymentSuccess = makePayment(bookingRQ, response, onholdCreated);

		response.setPnr(getPnrFromSession(bookingRQ.getTransactionId()));

		if (bookingRQ.getMakePaymentRQ() != null && !BookingType.ONHOLD.equals(bookingRQ.getMakePaymentRQ().getBookingType())
				&& !BookingRS.ActionStatus.REDIRET_EXTERNAL.equals(response.getActionStatus())) {
			validatePaxPayment(bookingRQ);
		}

		response.setFirstDepartureDate(getFirstDepartureDateFromSession(bookingRQ.getTransactionId()));
		response.setContactLastName(bookingRQ.getReservationContact().getLastName());

		if (paymentSuccess) {
			if (onholdCreated) {
				makeBalancePayment(bookingSessionStore, response);
			} else {
				makeReservation(bookingRQ, bookingSessionStore, response, ancillaryIntegrateTo, reservationAssemblerAdaptor);
			}
			response.setOnholdReleaseDuration(null);
			response.setPnr(getPnrFromSession(bookingRQ.getTransactionId()));
			setBookingIPGTransactionResult(response, bookingRQ.getTransactionId());

			if (BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS.equals(response.getActionStatus())) {

				CommonServiceUtil commonServices = new CommonServiceUtil();
				commonServices.updateBlockedLmsCreditStatusForCreateReservation(response, bookingSessionStore);
			}
			
			PostPaymentDTO postPaymentDTO = bookingSessionStore.getPostPaymentInformation();
			CustomerUtil.saveCustomerAlias(customerID, postPaymentDTO);	

			bookingSessionStore.clearBookingDetails();
			terminateTransaction(bookingRQ.getTransactionId());

		}

		if (onholdCreated && response.getActionStatus().equals(BookingRS.ActionStatus.ONHOLD_CREATED_CASH)) {
			terminateTransaction(bookingRQ.getTransactionId());
		}

		response.setTransactionId(bookingRQ.getTransactionId());
		response.getErrors().addAll(errorSet);
		response.getMessages().addAll(errorSet);

		log2.info("At Booking Controller #create ### " + " --> tid: " + bookingRQ.getTransactionId() + " --> pnr: "
				+ response.getPnr() + "GATESTBC");

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/get/onhold/pnr", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public OnholdPnrRS getOnholdPNR(@RequestBody TransactionalBaseRQ baseRQ) throws ModuleException {

		if (baseRQ == null || baseRQ.getTransactionId() == null) {
			throw new ModuleException("booking.api.invalid.onhold.pnr.request");
		}

		BookingSessionStore bookingSessionStore = getBookingSession(baseRQ.getTransactionId());

		if (bookingSessionStore == null || !bookingSessionStore.isOnHoldCreated()) {
			throw new ModuleException("booking.api.onhold.not.created");
		}

		OnholdPnrRS onholdPnrRS = new OnholdPnrRS();
		if (bookingSessionStore.getLastName() == null) {
			BookingTransaction txn = (BookingTransaction) bookingSessionStore;
			String lastName = txn.getAncillarySession().getReservation().getContactInformation().getLastName();
			onholdPnrRS.setLastName(lastName);
		} else {
			onholdPnrRS.setLastName(bookingSessionStore.getLastName());
		}

		onholdPnrRS.setTransactionId(baseRQ.getTransactionId());
		onholdPnrRS.setPnr(bookingSessionStore.getPnr());
		onholdPnrRS.setOnholdReleaseDuration(bookingSessionStore.getOnholdReleaseTime());
		onholdPnrRS.setLastName(bookingSessionStore.getLastName());
		onholdPnrRS.setFirstDepartureDate(bookingSessionStore.getFirstDepatureDate());
		onholdPnrRS.setSuccess(true);

		log2.info("At Booking Controller #onhold#pnr ### " + " --> tid: " + baseRQ.getTransactionId() + " --> pnr: "
				+ bookingSessionStore.getPnr() + "GATESTBC");

		return onholdPnrRS;
	}

	private String getOnholdReleaseDuration(Date onholdReleaseZuluTimeStamp) {
		if (onholdReleaseZuluTimeStamp != null) {
			LocalDateTime onholdReleaseDate = onholdReleaseZuluTimeStamp.toInstant().atZone(ZoneId.systemDefault())
					.toLocalDateTime();
			LocalDateTime now = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			Long durationInMinutes = ChronoUnit.MINUTES.between(now, onholdReleaseDate);
			Long durationInDays = ChronoUnit.DAYS.between(now, onholdReleaseDate);
			StringBuilder ohdTime = new StringBuilder();
			if (durationInDays > 0) {
				ohdTime.append(durationInDays + " Day(s) : ");
			}
			ohdTime.append((durationInMinutes / 60) % 24 + " Hour(s) : ");
			ohdTime.append(durationInMinutes % 60 + " Minute(s) ");
			return ohdTime.toString();
		}
		return null;
	}

	private void makeReservation(BookingRQ bookingRQ, BookingSessionStore bookingSessionStore, BookingRS response,
			AncillaryIntegrateTO ancillaryIntegrateTo, ReservationAssemblerAdaptor reservationAssemblerAdaptor) {
		try {
			CommonReservationAssembler commonReservationAssembler;
			bookingRQ.setCreateOnhold(false);
			bookingRQ.setOnholdReleaseZuluTimeStamp(null);
			commonReservationAssembler = reservationAssemblerAdaptor.adapt(bookingRQ);
			ServiceTaxRQ servTxRQ = new ServiceTaxRQ();
			servTxRQ.setCountryCode(bookingRQ.getReservationContact().getCountry());
			servTxRQ.setStateCode(bookingRQ.getReservationContact().getState());
			servTxRQ.setTaxRegNo(bookingRQ.getReservationContact().getTaxRegNo());
			servTxRQ.setTransactionId(bookingRQ.getTransactionId());
			paymentService.applyServiceTax(bookingRQ.getTransactionId(), servTxRQ, getTrackInfo());
			BookingUtil.populatePaxInformation(bookingRQ, commonReservationAssembler, ancillaryIntegrateTo, bookingSessionStore);
			BookingUtil.populateResPaxWithSelAnciDtls(ancillaryIntegrateTo.getReservation().getPassengers(),
					commonReservationAssembler);
			BookingRS bookingRS = bookingService.createBooking(commonReservationAssembler, ancillaryIntegrateTo, getTrackInfo());
			bookingSessionStore.storePnr(bookingRS.getPnr());
			response.setSuccess(bookingRS.isSuccess());
			if (response.isSuccess() && bookingRS.getPnr() != null) {
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS);
			} else {
				errorSet.addAll(bookingRS.getErrors());
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_FAILED);
			}

		} catch (Exception e) {
			response.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_PAYMENT_FAILED);
			log.error("Error in confirmation", e);
		}

	}

	private void makeBalancePayment(BookingSessionStore bookingSessionStore, BookingRS response) {
		try {
			CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
			String customerID = customerSessionStore.getLoggedInCustomerID();
			LCCClientReservation reservation = bookingService.balancePayment(bookingSessionStore, getTrackInfo(), customerID);
			if (reservation != null) {
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS);
			} else {
				response.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_PAYMENT_FAILED);
			}
		} catch (Exception e) {
			response.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_PAYMENT_FAILED);
			log.error("Error in confirmation", e);
		}
	}

	private BookingRS doPrePaymentValidations(BookingRQ bookingRQ) throws ModuleException {
		checkReservationDataIntergrity(bookingRQ.getTransactionId());
		validateCaptchaWithBookingType(bookingRQ);
		return validatePassengerInfo(bookingRQ);
	}

	private void validateCaptchaWithBookingType(BookingRQ bookingRQ) {
		BookingSessionStore bookingSessionStore = (BookingSessionStore) getTrasactionStore(bookingRQ.getTransactionId());
		if (bookingRQ.getMakePaymentRQ().getBookingType() != null
				&& bookingRQ.getMakePaymentRQ().getBookingType().equals(BookingType.ONHOLD)
				&& AppSysParamsUtil.isCaptchaEnabledForUnregisterdUser()
				&& !bookingSessionStore.getCaptchaText().equals(bookingRQ.getCaptcha())) {
			throw new ValidationException(ValidationException.Code.ONHOLD_WRONG_CAPTCHA);
		}
	}

	private boolean makePayment(BookingRQ bookingRQ, BookingRS response, boolean onholdCreated) throws ModuleException {
		MakePaymentRS makePaymentResponse;
		PaymentSessionStore paymentSessionStore = (PaymentSessionStore) getTrasactionStore(bookingRQ.getTransactionId());
		boolean paymentSuccess = false;
		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		String customerID = customerSessionStore.getLoggedInCustomerID();
		if (paymentService != null) {
			MakePaymentRQ makePaymentRequest = bookingRQ.getMakePaymentRQ();
			makePaymentRequest.setContactInfo(bookingRQ.getReservationContact());
			if (onholdCreated) {
				makePaymentRequest.setPnr(paymentSessionStore.getPnr());
			}

			MakePaymentRQ makePaymentRQ = bookingRQ.getMakePaymentRQ();
			makePaymentRQ.setTransactionId(bookingRQ.getTransactionId());
			if (makePaymentRQ.getContactInfo() == null) {
				makePaymentRQ.setContactInfo(new ReservationContact());
			}
			
			makePaymentRQ.getContactInfo().setCountry(bookingRQ.getReservationContact().getCountry());
			makePaymentRQ.getContactInfo().setState(bookingRQ.getReservationContact().getState());
			makePaymentRQ.getContactInfo().setTaxRegNo(bookingRQ.getReservationContact().getTaxRegNo());
			makePaymentResponse = paymentService.processCreateBookingPayment(makePaymentRQ, getTrackInfo(),
					getClientCommonInfoDTO());

			if (makePaymentResponse != null && makePaymentResponse.isSuccess()) {
				paymentSuccess = handleSuccessPaymentResponse(bookingRQ, response, onholdCreated, makePaymentResponse);
			} else {
				response.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
			}
		}

		return paymentSuccess;
	}

	private boolean handleSuccessPaymentResponse(BookingRQ bookingRQ, BookingRS response, boolean onholdCreated,
			MakePaymentRS makePaymentResponse) {
		boolean paymentSuccess = false;
		if (BookingRS.PaymentStatus.SUCCESS.equals(makePaymentResponse.getPaymentStatus())) {
			response.setPaymentStatus(BookingRS.PaymentStatus.SUCCESS);
			paymentSuccess = true;
		}

		// offline payment
		else if (BookingRS.PaymentStatus.PENDING.equals(makePaymentResponse.getPaymentStatus())
				&& makePaymentResponse.isOfflinePayment()) {
			response.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
			response.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_OFFLINE);
			response.setExternalPGDetails(makePaymentResponse.getExternalPGDetails());
			response.setMessages(makePaymentResponse.getMessages());

		}

		// cash payment
		else if (BookingRS.PaymentStatus.PENDING.equals(makePaymentResponse.getPaymentStatus())
				&& makePaymentResponse.isCashPayment()) {
			response.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
			response.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_CASH);

		} else if (makePaymentResponse.isSwitchToExternal()) {

			if (!onholdCreated) {
				getBookingSession(bookingRQ.getTransactionId()).storeBookingRQ(bookingRQ);
			}

			response.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
			response.setActionStatus(BookingRS.ActionStatus.REDIRET_EXTERNAL);
			response.setExternalPGDetails(makePaymentResponse.getExternalPGDetails());
		} else if (BookingRS.PaymentStatus.ERROR.equals(makePaymentResponse.getPaymentStatus())) {
			response.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
		}
		return paymentSuccess;
	}

	private BookingRS validatePassengerInfo(BookingRQ bookingRQ) throws ValidationException, ModuleException {
		BookingRS response = new BookingRS();
		response.setSuccess(true);
		BookingSessionStore bookingStore = (BookingSessionStore) getTrasactionStore(bookingRQ.getTransactionId());

		PassengerUtil passengerUtil = new PassengerUtil();
		passengerUtil.validateContactDetails(bookingRQ, bookingStore, getTrackInfo());
		passengerUtil.validatePaxInfo(bookingRQ, bookingStore, getTrackInfo());

		DuplicatePaxCheckAssemblerAdaptor duplicatePaxCheckAdaptor = new DuplicatePaxCheckAssemblerAdaptor(bookingStore);
		DuplicateValidatorAssembler duplicateValidatorAssembler = duplicatePaxCheckAdaptor.adapt(bookingRQ.getPaxInfo());
		boolean hasDuplicates = passengerService.checkDuplicates(getTrackInfo(), duplicateValidatorAssembler);
		if (hasDuplicates) {
			response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_FAILED);
			response.setSuccess(false);
			return response;
		}

		ValidatePaxFFIDAdapter validatePaxFFIDAdapter = new ValidatePaxFFIDAdapter();
		ValidatePaxFFIDsRQ validatePaxFFIDsRQ = validatePaxFFIDAdapter.adapt(bookingRQ.getPaxInfo());
		ValidatePaxFFIDsRS validatePaxFFIDsRS = passengerService.validateLMSPax(validatePaxFFIDsRQ);

		for (PaxValidationDetails paxValidationDetails : validatePaxFFIDsRS.getPaxValidationList()) {
			if (!paxValidationDetails.isValidFFID() || !paxValidationDetails.isValidName()
					|| !paxValidationDetails.isValidPaxType()) {
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_FAILED);
				response.setSuccess(false);
				return response;
			}
		}

		return response;
	}

	private Date getOnHoldReleaseTime(BookingRQ bookingRQ) throws ModuleException {
		Date onholdReleaseZuluTimeStamp = null;
		if (bookingRQ.getMakePaymentRQ() != null) {
			bookingRQ.getMakePaymentRQ().setTransactionId(bookingRQ.getTransactionId());
		}
		onholdReleaseZuluTimeStamp = paymentService.calculateOnHoldReleaseTime(bookingRQ.getMakePaymentRQ(), getTrackInfo());
		return onholdReleaseZuluTimeStamp;
	}

	private boolean isAllowOnhold(BookingRQ bookingRQ, Date onholdReleaseZuluTimeStamp) {
		boolean isAllowOnhold = false;
		BookingSessionStore bookingSessionStore = (BookingSessionStore) getTrasactionStore(bookingRQ.getTransactionId());
		if (onholdReleaseZuluTimeStamp != null && bookingSessionStore.getFarePricingInformation() != null
				&& AppSysParamsUtil.isOnHoldEnable(OnHold.IBEPAYMENT)) {
			isAllowOnhold = bookingSessionStore.getFarePricingInformation().isAllowOnhold();
		}
		bookingRQ.setCreateOnhold(isAllowOnhold);
		return isAllowOnhold;
	}

	/*
	 * Validate reservation data - As we are keeping price quote in session, user can change data using separate browser
	 * session
	 */
	private void checkReservationDataIntergrity(String transactionId) throws ModuleException {
		BookingUtil.validatePassengerFlightDetails(getBookingSession(transactionId).getSegments(),
				getBookingSession(transactionId).getFarePricingInformation());
	}

	private void validatePaxPayment(BookingRQ bookingRQ) throws ModuleException {

		PaymentSessionStore paymentSessionStore = (PaymentSessionStore) getTrasactionStore(bookingRQ.getTransactionId());

		BigDecimal reservationAmountDue = paymentSessionStore.getTotalPaymentInformation().getTotalWithAncillaryCharges();

		BigDecimal loyaltyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal lmsPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditCardPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal externalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal redeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal adminFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (paymentSessionStore.getLoyaltyInformation().getTotalRedeemedAmount() != null) {
			lmsPaymentAmount = paymentSessionStore.getLoyaltyInformation().getTotalRedeemedAmount();
		}

		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		if (paymentSessionStore.getPostPaymentInformation() != null
				&& paymentSessionStore.getPostPaymentInformation().getIpgRequestDTO() != null
				&& paymentSessionStore.getPostPaymentInformation().getIpgRequestDTO().getAmount() != null) {
			creditCardPayAmount = AccelAeroCalculator.scaleValueDefault(
					new BigDecimal(paymentSessionStore.getPostPaymentInformation().getIpgRequestDTO().getAmount()));

			if (!AppSysParamsUtil.getBaseCurrency().equals(paymentSessionStore.getPostPaymentInformation().getIpgRequestDTO()
					.getIpgIdentificationParamsDTO().getPaymentCurrencyCode())) {
				CurrencyExchangeRate exchangeRate = masterDataService.getCurrencyExchangeRate(paymentSessionStore
						.getPostPaymentInformation().getIpgRequestDTO().getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
				creditCardPayAmount = AccelAeroCalculator.multiplyDefaultScale(creditCardPayAmount,
						exchangeRate.getExrateBaseToCurNumber());

			}

			externalChargeAmount = getExternalChargeAmount(paymentSessionStore);
		}
		
		if (paymentSessionStore.getPayByVoucherInfo() != null) {
			redeemedAmount = paymentSessionStore.getPayByVoucherInfo().getRedeemedTotal();
			if (paymentSessionStore.getVoucherInformation().isTotalAmountPaidFromVoucher()
					|| paymentSessionStore.getVoucherInformation().isTotalPaidFromVoucherLMS()) {
				creditCardPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				adminFeeAmount = paymentSessionStore.getTotalPaymentInformation().getTotalPaymentCharges();
			}
		}
		

		BigDecimal discountTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (bookingRQ.getMakePaymentRQ().isBinPromotion()) {
			if (DiscountApplyAsTypes.MONEY
					.equals(paymentSessionStore.getBinPromoDiscountInfo().getDiscountedFareDetails().getDiscountAs())) {
				discountTotal = paymentSessionStore.getBinPromoDiscountInfo().getDiscountAmount();
			}
		} else if (paymentSessionStore.getDiscountAmount() != null && paymentSessionStore.getDiscountedFareDetails() != null
				&& DiscountApplyAsTypes.MONEY.equals(paymentSessionStore.getDiscountedFareDetails().getDiscountAs())) {
			discountTotal = paymentSessionStore.getDiscountAmount();
		}

		BigDecimal totalTicketPrice = AccelAeroCalculator
				.add(reservationAmountDue, externalChargeAmount, discountTotal.negate(), adminFeeAmount);
		if (paymentSessionStore.getPayByVoucherInfo() != null
				&& paymentSessionStore.getVoucherInformation().isTotalAmountPaidFromVoucher()) {
			if (redeemedAmount.compareTo(totalTicketPrice) > 0) {
				redeemedAmount = totalTicketPrice;
			}
		}
		BigDecimal totalPaxPayment = AccelAeroCalculator.add(creditCardPayAmount, loyaltyAmount, lmsPaymentAmount,
				redeemedAmount);
		BigDecimal paymentDifference = AccelAeroCalculator.subtract(totalTicketPrice, totalPaxPayment);
		// Fix me
		// Need to improve payment difference
		if (paymentDifference.abs().compareTo(BigDecimal.valueOf(0.5)) > 0
				&& paymentSessionStore.getPostPaymentInformation() != null) {

			if (log.isErrorEnabled()) {
				log.error("### System can not process the payment.Payment diffrence : " + paymentDifference);
			}

			throw new ModuleException("msg.invalid.booking");
		}
	}

	private BigDecimal getExternalChargeAmount(PaymentSessionStore paymentSessionStore) {
		Collection<LCCClientExternalChgDTO> externalCharges = paymentSessionStore.getExternalCharges();

		BigDecimal externalChargeAmount = BigDecimal.ZERO;

		for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalCharges) {
			if (EXTERNAL_CHARGES.JN_OTHER.equals(lccClientExternalChgDTO.getExternalCharges())) {
				externalChargeAmount = AccelAeroCalculator.add(externalChargeAmount, lccClientExternalChgDTO.getAmount());
			}

			else if (EXTERNAL_CHARGES.CREDIT_CARD.equals(lccClientExternalChgDTO.getExternalCharges())) {
				externalChargeAmount = AccelAeroCalculator.add(externalChargeAmount, lccClientExternalChgDTO.getAmount());
			}
		}

		return externalChargeAmount;
	}

	private BigDecimal getTotalServiceTaxChargeAmount(PaymentSessionStore paymentSessionStore) {
		Collection<LCCClientExternalChgDTO> externalCharges = paymentSessionStore.getExternalCharges();

		BigDecimal externalChargeAmount = BigDecimal.ZERO;

		for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalCharges) {
			if (EXTERNAL_CHARGES.SERVICE_TAX.equals(lccClientExternalChgDTO.getExternalCharges())) {
				externalChargeAmount = AccelAeroCalculator.add(externalChargeAmount, lccClientExternalChgDTO.getAmount());
			}
		}

		return externalChargeAmount;
	}

	private void checkSeatAvailability(BookingRQ bookingRQ) throws ValidationException, ModuleException {
		FareSegChargeTO fareSegChargeTO = getBookingSession(bookingRQ.getTransactionId()).getFarePricingInformation();
		PassengerTypeQuantityAdaptor passengerTypeQuantityAdaptor = new PassengerTypeQuantityAdaptor();
		IPaxCountAssembler paxCountAssembler = new PaxCountAssembler(passengerTypeQuantityAdaptor.adapt(bookingRQ.getPaxInfo()));
		QuotedFareRebuildDTO quotedFareRBuilder = new QuotedFareRebuildDTO(fareSegChargeTO, paxCountAssembler, null,
				BookingClass.BookingClassType.NORMAL, false);
		if (ProxyConstants.SYSTEM.getEnum(
				getBookingSession(bookingRQ.getTransactionId()).getPreferenceInfoForBooking().getSelectedSystem()) == SYSTEM.AA) {
			if (AppSysParamsUtil.isIBEBlockSeatEnabled()) {
				Collection<OndFareDTO> collFares;
				collFares = AirproxyModuleUtils.getAirReservationQueryBD()
						.recreateFareSegCharges(quotedFareRBuilder.getFirstOndRebuildCriteria());
				Collection<TempSegBcAlloc> blockSeats = ModuleServiceLocator.getReservationBD().blockSeats(collFares, null);
				fareSegChargeTO.setBlockSeatIds(blockSeats);
			} else {
				if (!ModuleServiceLocator.getReservationQueryBD()
						.checkInventoryAvailability(quotedFareRBuilder.getFirstOndRebuildCriteria())) {
					throw new ValidationException(ValidationException.Code.REQUESTED_SEATS_NOT_AVAILABLE);
				}
			}
		}
	}

	private void setBookingIPGTransactionResult(BookingRS bookingRS, String transactionID) {
		BookingSessionStore sessionStore = getBookingSession(transactionID);
		if (sessionStore.getPostPaymentInformation() != null
				&& sessionStore.getPostPaymentInformation().getIpgResponseDTO() != null
				&& sessionStore.getPostPaymentInformation().getIpgResponseDTO().getIpgTransactionResultDTO() != null) {
			IPGTransactionRSAdaptor ipgTransactionRSAdaptor = new IPGTransactionRSAdaptor();
			bookingRS.setIpgTransactionResult(ipgTransactionRSAdaptor
					.adapt(sessionStore.getPostPaymentInformation().getIpgResponseDTO().getIpgTransactionResultDTO()));
		}
	}

	private String getPnrFromSession(String transactionId) {
		BookingSessionStore bookingSessionStore = getBookingSession(transactionId);
		if (bookingSessionStore.getPostPaymentInformation() != null
				&& bookingSessionStore.getPostPaymentInformation().getPnr() != null) {
			return bookingSessionStore.getPostPaymentInformation().getPnr();
		} else {
			return bookingSessionStore.getPnr();
		}
	}

	private Date getFirstDepartureDateFromSession(String transactionID) {
		BookingSessionStore bookingSessionStore = getBookingSession(transactionID);
		return bookingSessionStore.getSegments().get(0).getDepartureDateTime();
	}

	private BookingSessionStore getBookingSession(String transactionId) {
		return getTrasactionStore(transactionId);
	}
	
}