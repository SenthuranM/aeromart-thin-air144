package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ContactConfigCollectionRQ extends TransactionalBaseRQ {

	private List<PaxContactDetailsRQ> configDetailsRQs;

	public List<PaxContactDetailsRQ> getConfigDetailsRQs() {
		return configDetailsRQs;
	}

	public void setConfigDetailsRQs(List<PaxContactDetailsRQ> configDetailsRQs) {
		this.configDetailsRQs = configDetailsRQs;
	}
}
