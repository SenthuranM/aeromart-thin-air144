package com.isa.aeromart.services.endpoint.dto.voucher;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.commons.api.dto.VoucherDTO;


public class IssueVoucherRS extends TransactionalBaseRS{
	
	
	private List<VoucherDTO> voucherDTOList = new ArrayList<VoucherDTO>();

	public List<VoucherDTO> getVoucherDTOList() {
		return voucherDTOList;
	}

	public void setVoucherDTOList(List<VoucherDTO> voucherDTOList) {
		this.voucherDTOList = voucherDTOList;
	}
	
}
