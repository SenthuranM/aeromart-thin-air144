package com.isa.aeromart.services.endpoint.dto.session.impl.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SessionFlexiDetail {

	private Integer ondSequence;
	private boolean flexiAvailable;
	private boolean flexiSelected;
	private BigDecimal totalFlexiCharge;
	private List<SessionExternalChargeSummary> externalChargeSummary;

	public Integer getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(Integer ondSequence) {
		this.ondSequence = ondSequence;
	}

	public boolean isFlexiAvailable() {
		return flexiAvailable;
	}

	public void setFlexiAvailable(boolean flexiAvailable) {
		this.flexiAvailable = flexiAvailable;
	}

	public boolean isFlexiSelected() {
		return flexiSelected;
	}

	public void setFlexiSelected(boolean flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	public void setTotalFlexiCharge(BigDecimal totalFlexiCharge) {
		this.totalFlexiCharge = totalFlexiCharge;
	}

	public List<SessionExternalChargeSummary> getExternalChargeSummary() {
		if (externalChargeSummary == null) {
			externalChargeSummary = new ArrayList<SessionExternalChargeSummary>();
		}
		return externalChargeSummary;
	}

	public void setExternalChargeSummary(List<SessionExternalChargeSummary> externalChargeSummary) {
		this.externalChargeSummary = externalChargeSummary;
	}

	public BigDecimal getTotalFlexiCharge() {
		return totalFlexiCharge;
	}
}