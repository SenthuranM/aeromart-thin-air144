package com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati;

public class CmiFatouratiOfflinePaymentConfirmationResponse {

	private boolean success;
	private String message;
	private String detailErrorMessage;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetailErrorMessage() {
		return detailErrorMessage;
	}

	public void setDetailErrorMessage(String detailErrorMessage) {
		this.detailErrorMessage = detailErrorMessage;
	}
	

}
