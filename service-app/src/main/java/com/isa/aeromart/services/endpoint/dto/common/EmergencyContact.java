package com.isa.aeromart.services.endpoint.dto.common;

public class EmergencyContact {

	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private PhoneNumber emgnPhoneNumber;

	private String emgnEmail;

	public String getEmgnTitle() {
		return emgnTitle;
	}

	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	public String getEmgnLastName() {
		return emgnLastName;
	}

	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	public PhoneNumber getEmgnPhoneNumber() {
		return emgnPhoneNumber;
	}

	public void setEmgnPhoneNumber(PhoneNumber emgnPhoneNumber) {
		this.emgnPhoneNumber = emgnPhoneNumber;
	}

	public String getEmgnEmail() {
		return emgnEmail;
	}

	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}

}
