package com.isa.aeromart.services.endpoint.dto.session.impl.modification;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class ReservationInfo {
	
	private String pnr;
	
	private boolean groupPNR;	

	private String version;

	private String transactionId;

	private String customerId;
	
	private String language;
	
	private String selectedCurrency;
	
	private List<ReservationPaxTO> paxList;	
	
	private TravellerQuantity paxTypeWiseCount;

	private Map<String, Map<String, BigDecimal>> pnrPaxCreditMap = new HashMap<String, Map<String, BigDecimal>>();
	
	private CommonReservationContactInfo contactInfo;
	
	private String reservationStatus;
	
	private Collection<LCCClientReservationSegment> existingAllLccSegments;
	
	private boolean infantPaymentSeparated;

	private boolean dummyInfo = false;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public List<ReservationPaxTO> getPaxList() {
		return paxList;
	}

	public void setPaxList(List<ReservationPaxTO> paxList) {
		this.paxList = paxList;
	}

	public Map<String, Map<String, BigDecimal>> getPnrPaxCreditMap() {
		return pnrPaxCreditMap;
	}

	public void setPnrPaxCreditMap(Map<String, Map<String, BigDecimal>> pnrPaxCreditMap) {
		this.pnrPaxCreditMap = pnrPaxCreditMap;
	}

	public void addPnrPaxCreditMap(String pnr, Map<String, BigDecimal> paxCreditMap) {
		this.pnrPaxCreditMap.put(pnr, paxCreditMap);
	}

	public Map<String, BigDecimal> getPaxCreditMap(String CreditPnr) {
		return pnrPaxCreditMap.get(CreditPnr);
	}

	public TravellerQuantity getPaxTypeWiseCount() {
		return paxTypeWiseCount;
	}

	public void setPaxTypeWiseCount(TravellerQuantity paxTypeWiseCount) {
		this.paxTypeWiseCount = paxTypeWiseCount;
	}

	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Collection<LCCClientReservationSegment> getExistingAllLccSegments() {
		return existingAllLccSegments;
	}

	public void setExistingAllLccSegments(Collection<LCCClientReservationSegment> existingAllLccSegments) {
		this.existingAllLccSegments = existingAllLccSegments;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}

	public boolean isDummyInfo() {
		return dummyInfo;
	}

	public void setDummyInfo(boolean dummyInfo) {
		this.dummyInfo = dummyInfo;
	}
	
}
