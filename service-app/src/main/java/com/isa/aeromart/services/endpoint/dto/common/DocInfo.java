package com.isa.aeromart.services.endpoint.dto.common;

import java.util.Date;

public class DocInfo {

	private String travelDocumentType;

	private String docNumber;

	private String docPlaceOfIssue;

	private Date docIssueDate;

	private String applicableCountry;

	public String getTravelDocumentType() {
		return travelDocumentType;
	}

	public void setTravelDocumentType(String travelDocumentType) {
		this.travelDocumentType = travelDocumentType;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String visaDocNumber) {
		this.docNumber = visaDocNumber;
	}

	public String getDocPlaceOfIssue() {
		return docPlaceOfIssue;
	}

	public void setDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.docPlaceOfIssue = visaDocPlaceOfIssue;
	}

	public Date getDocIssueDate() {
		return docIssueDate;
	}

	public void setDocIssueDate(Date visaDocIssueDate) {
		this.docIssueDate = visaDocIssueDate;
	}

	public String getApplicableCountry() {
		return applicableCountry;
	}

	public void setApplicableCountry(String visaApplicableCountry) {
		this.applicableCountry = visaApplicableCountry;
	}

}
