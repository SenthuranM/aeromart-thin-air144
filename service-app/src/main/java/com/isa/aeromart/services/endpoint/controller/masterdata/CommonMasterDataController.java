package com.isa.aeromart.services.endpoint.controller.masterdata;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.dto.common.MealDetailsRS;
import com.isa.aeromart.services.endpoint.dto.common.SeatTypeDetailsRS;
import com.isa.aeromart.services.endpoint.dto.customer.GenericDropdownDTO;
import com.isa.aeromart.services.endpoint.dto.customer.RelationshipListRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessageRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessagesRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.CountryListRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.CurrencyExRatesRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.DropDownListRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.GoogleAnalyticsTestRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.TermsAndConditionsRS;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.service.ValidationService;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.aeromart.services.endpoint.utils.masterdata.MasterDataCacheUtil;
import com.isa.aeromart.services.endpoint.utils.masterdata.MasterDataUtil;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Controller
@RequestMapping("masterData")
public class CommonMasterDataController extends StatefulController {

	private static Log log = LogFactory.getLog(CommonMasterDataController.class);
	private static Log log2 = LogFactory.getLog("com.isa.aeromart.services.analytics.issue");
	private static final String DEFAULT_LANGUAGE = "en";

	@Autowired
	private ValidationService validationService;

	@ResponseBody
	@RequestMapping(value = "/countryList", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CountryListRS getCountryList() {

		CountryListRS countryListResponse = null;

		try {
			countryListResponse = MasterDataCacheUtil.getInstance().getCountryListResponse();
		} catch (Exception e) {
			log.error("Error in retriveing currency exchange rates", e);

		}

		return countryListResponse;

	}

	@ResponseBody
	@RequestMapping(value = "/currencyExRates", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CurrencyExRatesRS getCurrencyExRates() {

		CurrencyExRatesRS currencyExRatesResponse = null;
		try {
			currencyExRatesResponse = MasterDataCacheUtil.getInstance().getCachedCurrencyExchangeRatesResponse();
		} catch (Exception e) {
			log.error("Error in retriveing currency exchange rates", e);

		}

		return currencyExRatesResponse;
	}

	@ResponseBody
	@RequestMapping(value = "/dropDownLists", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public DropDownListRS getDropDownLists() {

		DropDownListRS dropDownLists = null;
		try {
			dropDownLists = MasterDataCacheUtil.getInstance().getDropDownLists();
		} catch (Exception e) {
			log.error("Error in retriveing currency exchange rates", e);

		}
		return dropDownLists;

	}

	@ResponseBody
	@RequestMapping(value = "/termsNConditions", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TermsAndConditionsRS getTermsAndConditions() {

		TermsAndConditionsRS termsAndConditions = new TermsAndConditionsRS();
		
		String termsSummary = "", termsFull = "";
		
		Locale locale = LocaleContextHolder.getLocale();
		if (locale == null) {
			locale = new Locale("en");
		}

		try {
			termsSummary = ReservationTemplateUtils.getTermsNConditions("TermsSummary_IBE", locale);
			termsFull = ReservationTemplateUtils.getTermsNConditions("TermsFull_IBE", locale);
		} catch (ModuleException e) {

		}

		if (termsFull.equals("")) {

			String strFileName = "TermsFull_IBE_en.xml";
			String language = locale.getLanguage();

			if (language != null) {
				strFileName = "TermsFull_IBE_" + language + ".xml";
				String strXMLFilePath = PlatformConstants.getConfigRootAbsPath() + CommonConstants.XML_FILE_PATH + strFileName;
				termsFull = MasterDataUtil.createTermsNCondFromXML(strXMLFilePath);
			}

		}

		termsAndConditions.setTermsNConditionsSummary(termsSummary);
		termsAndConditions.setTermsNConditionsFull(termsFull);

		termsAndConditions.setSuccess(true);

		return termsAndConditions;
	}

	@ResponseBody
	@RequestMapping(value = "/airportMessages", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			AirportMessageRS getAirportMessages(@RequestBody AirportMessagesRQ airportMessageRQ) {
		validationService.validateAirportMessageRQ(airportMessageRQ);
		AirportMessageRS airportMessageRS = null;
		try {
			airportMessageRS = MasterDataCacheUtil.getInstance().getAirportMessages(airportMessageRQ,
					getTrackInfo().getAppIndicator());
			airportMessageRS.setSuccess(true);
		} catch (Exception e) {
			log.error("Error in retriveing airport messages", e);
			airportMessageRS.setSuccess(false);

		}

		return airportMessageRS;
	}

	@ResponseBody
	@RequestMapping(value = "/addGALogs", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public void addGooleAnalyticsLogs(@RequestBody GoogleAnalyticsTestRQ googleAnalyticsTestRQ) {
		log2.info("-> pnr: " + googleAnalyticsTestRQ.getPnr() + "-> revanue: " + googleAnalyticsTestRQ.getRevanue()
				+ "-> exception: " + googleAnalyticsTestRQ.getException() + "-> flow: " + googleAnalyticsTestRQ.getFlow()
				+ "-> path: " + googleAnalyticsTestRQ.getPath() + "-> fullPath: " + googleAnalyticsTestRQ.getFullPath()
				+ "-> tid: " + googleAnalyticsTestRQ.getTransactionId() + " GATESTLOG");
	}
	
	@ResponseBody
	@RequestMapping(value = "/getAllSeatTypes", method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			SeatTypeDetailsRS getAllSeatTypes() {
		SeatTypeDetailsRS seatTypeDetailsRS = new SeatTypeDetailsRS();
		List<SeatType> seatPreferencesList = null;
		seatPreferencesList = CustomerUtil.getPreferenceSeatTypes();

		seatTypeDetailsRS.setSeatPreferencesList(seatPreferencesList);
		seatTypeDetailsRS.setSuccess(true);
		return seatTypeDetailsRS;
	}

	@ResponseBody
	@RequestMapping(value = "/getAllMealDetails", method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			MealDetailsRS getAllMealDetails() {
		MealDetailsRS mealDetailsRS = new MealDetailsRS();
		ArrayList<String> messages = new ArrayList<>();
		List<LCCMealDTO> mealList = null;

		try {
			mealList = ModuleServiceLocator.getAirproxyAncillaryBD().getMealsForPreference(getLanguage());
		} catch (ModuleException e) {
			messages.add("System error.");
			mealDetailsRS.setSuccess(false);
			mealDetailsRS.setMessages(messages);
			return mealDetailsRS;
		}
		mealDetailsRS.setMealList(mealList);
		mealDetailsRS.setSuccess(true);
		mealDetailsRS.setMessages(messages);
		return mealDetailsRS;
	}

	private String getLanguage() {
		return (getRequest().getLocale() != null && getRequest().getLocale().getLanguage() != null) ? getRequest().getLocale()
				.getLanguage() : DEFAULT_LANGUAGE;
	}
	
	

	private void setToDTOList(List<String[]> arrayList,DropDownListRS dropDownLists ,CountryListRS countryListResponse ) {
		if (arrayList == null || arrayList.isEmpty()) {
			return;
		}

		for (String[] array : arrayList) {
			GenericDropdownDTO relationshipDTO = new GenericDropdownDTO();
			relationshipDTO.setId(array[0]);
			relationshipDTO.setName(array[1]);
			//dtoList.add(relationshipDTO);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/getRelationshipDropdownList", method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			RelationshipListRS getRelationshipDropdownList() {

		RelationshipListRS relationshipDropdownListRS = new RelationshipListRS();

		try {
			relationshipDropdownListRS = MasterDataCacheUtil.getInstance().getRelationsResponse();
			relationshipDropdownListRS.setSuccess(true);
		} catch (Exception e) {
			log.error("Error in retriveing airport messages", e);
			relationshipDropdownListRS.setSuccess(false);

		}
		return relationshipDropdownListRS;
	}

}
