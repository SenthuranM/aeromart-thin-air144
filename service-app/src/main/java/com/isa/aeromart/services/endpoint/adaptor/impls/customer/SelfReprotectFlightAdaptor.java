package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;

public class SelfReprotectFlightAdaptor implements Adaptor<FlightSegmentDTO, SelfReprotectFlightDTO> {

	@Override
	public SelfReprotectFlightDTO adapt(FlightSegmentDTO flightSegmentDTO) {
		SelfReprotectFlightDTO selfReprotectFlightDTO = new SelfReprotectFlightDTO();
		selfReprotectFlightDTO.setFlightId(flightSegmentDTO.getFlightId());
		selfReprotectFlightDTO.setFlightNumber(flightSegmentDTO.getFlightNumber());
		selfReprotectFlightDTO.setFltSegId(flightSegmentDTO.getSegmentId().toString());
		selfReprotectFlightDTO.setSegmentCode(flightSegmentDTO.getSegmentCode());
		selfReprotectFlightDTO.setArrivalDate(flightSegmentDTO.getArrivalDateTime());
		selfReprotectFlightDTO.setDepartureDate(flightSegmentDTO.getDepartureDateTime());
		selfReprotectFlightDTO.setFlightDuration(getDuration(flightSegmentDTO));
		return selfReprotectFlightDTO;
	}

	private String getDuration(FlightSegmentDTO flightSegmentDTO) {
		String duration = "";
		if(flightSegmentDTO.getArrivalDateTimeZulu() != null && flightSegmentDTO.getDepartureDateTimeZulu() != null) {
			long diff = flightSegmentDTO.getArrivalDateTimeZulu().getTime() - flightSegmentDTO.getDepartureDateTimeZulu().getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			duration = ((diffHours < 9) ? "0" + diffHours : diffHours) + ":" + ((diffMinutes < 9) ? "0" + diffMinutes : diffMinutes);
		}
		return duration;
	}
}
