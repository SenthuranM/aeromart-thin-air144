package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Set;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class CustomerLoginResponse extends BaseRS{
	
	private LoggedInCustomerDetails loggedInCustomerDetails = null;
        
    private LoggedInLmsDetails loggedInLmsDetails = null;
    
    private Set<ReservationDetails> reservationList = null;
      
    private boolean loyaltyManagmentEnabled = false;
    
    private double totalCustomerCredit;

	public LoggedInCustomerDetails getLoggedInCustomerDetails() {
		return loggedInCustomerDetails;
	}

	public void setLoggedInCustomerDetails(
			LoggedInCustomerDetails loggedInCustomerDetails) {
		this.loggedInCustomerDetails = loggedInCustomerDetails;
	}

	public LoggedInLmsDetails getLoggedInLmsDetails() {
		return loggedInLmsDetails;
	}

	public void setLoggedInLmsDetails(LoggedInLmsDetails loggedInLmsDetails) {
		this.loggedInLmsDetails = loggedInLmsDetails;
	}

	public Set<ReservationDetails> getReservationList() {
		return reservationList;
	}

	public void setReservationList(Set<ReservationDetails> reservationList) {
		this.reservationList = reservationList;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

	public void setTotalCustomerCredit(double totalCustomerCredit) {
		this.totalCustomerCredit = totalCustomerCredit;
	}
	
	public double getTotalCustomerCredit() {
		return totalCustomerCredit;
	}
}
