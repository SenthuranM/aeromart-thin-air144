package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class Selection {
	
	private Scope scope;
	
	private List<SelectedItem> selectedItems;

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public List<SelectedItem> getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(List<SelectedItem> selectedItems) {
		this.selectedItems = selectedItems;
	}
}
