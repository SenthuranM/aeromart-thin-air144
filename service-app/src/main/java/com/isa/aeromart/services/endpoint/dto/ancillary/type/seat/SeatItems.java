package com.isa.aeromart.services.endpoint.dto.ancillary.type.seat;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class SeatItems extends AncillaryItemLeaf {

	private List<SeatItem> items;

    public SeatItems() {
        setType(AncillariesConstants.Type.SEAT);
    }

    public List<SeatItem> getItems() {
		return items;
	}

	public void setItems(List<SeatItem> items) {
		this.items = items;
	}
		
}
