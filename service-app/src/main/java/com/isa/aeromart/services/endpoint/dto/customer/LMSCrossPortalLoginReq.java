package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRQ;

public class LMSCrossPortalLoginReq extends BaseRQ{

	private String urlRef;

	public String getUrlRef() {
		return urlRef;
	}

	public void setUrlRef(String urlRef) {
		this.urlRef = urlRef;
	}
	
}
