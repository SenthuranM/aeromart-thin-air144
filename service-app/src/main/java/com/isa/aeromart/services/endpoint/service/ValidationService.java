package com.isa.aeromart.services.endpoint.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AvailableAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BaseAncillarySelectionsRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.RequoteRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessagesRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccResSegmentInfo;
import com.isa.aeromart.services.endpoint.dto.modification.FFIDUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.modification.NameChangePassenger;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRQ;
import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionRQ;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRQ;
import com.isa.aeromart.services.endpoint.dto.ping.PingParams;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface ValidationService {

	public void isValidPingRequest(PingParams pingParams) throws ValidationException;

	public void nameChangeValidate(LCCClientReservation reservation, List<NameChangePassenger> nameChangePassengerList,
			SYSTEM system, TrackInfoDTO trackInfo) throws ValidationException, ModuleException;

	public void ffidAddEditValidate(FFIDUpdateRQ ffidUpdateRQ, LCCClientReservation reservation,
			Map<Integer, NameDTO> nameChangeMap)
			throws ValidationException, ModuleException;

	public void checkDuplicateNames(List<LCCClientReservationPax> nameChangedpaxList, Set<LCCClientReservationSegment> segList,
			SYSTEM system, TrackInfoDTO trackInfo) throws ModuleException;

	public void validateAvailabilityRQ(AvailabilitySearchRQ availabilitySearchRq, TrackInfoDTO trackInfoDTO)
			throws ValidationException, ModuleException;

	public void validateAncillaryAvailabilityRQ(AncillaryAvailabilityRQ ancillaryAvailabilityRq) throws ValidationException;

	public void validateAvailableAncillaryRQ(AvailableAncillaryRQ availableAncillariesRq) throws ValidationException;

	public void validateBlockAncillaryRQ(BlockAncillaryRQ blockAncillariesRequest) throws ValidationException;

	public void
			validateAllocateAncillaryRQ(BaseAncillarySelectionsRQ baseAncillaryRQ, SessionBasicReservation sessionReserveData)
					throws ValidationException;

	public void validateRequoteRQ(RequoteRQ requoteRQ, Set<LccResSegmentInfo> resSegs, RPHGenerator rphGenerator,
			Map<String, List<LccResSegmentInfo>> ondWiseResSegMap, LCCClientReservation reservation);

	public void validateRequoteRQForModification(Set<LccResSegmentInfo> resSegs, FlightPriceRQ flightPriceRQ,
			LCCClientReservation reservation);

	public void validateRequoteRS(FlightPriceRS flightPriceRS, boolean isModifysegment);

	public void validateAncillary(AncillaryAvailabilityRQ ancillariesAvailabilityRequest) throws ValidationException;

	public void validateAirportMessageRQ(AirportMessagesRQ airportMessageRQ) throws ValidationException;

	public void validatePaymentMethodsRQ(PaymentMethodsRQ paymentMethodsRQ);

	public void validatatePaymentSummaryRQ(PaymentSummaryRQ paymentSummaryRQ);

	public void validateAdminFeeRQ(AdministrationFeeRQ adminFeeRQ);

	public void validatePayfortCheckRQ(PayfortCheckRQ payfortCheckRQ);

	public void validateBinPromotionRQ(BinPromotionRQ binPromotionRQ);

	public void validatePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException;

	public void validateBalancePaymentOptionsRQ(PaymentOptionsRQ balancePaymentOptionsRQ, String bookingTransactionId);

	public void validateRequotePaymentOptions(PaymentOptionsRQ requotePaymentOptionsRQ);

	public void validateAnciModificationPaymentOptionsRQ(PaymentOptionsRQ anciModificationPaymentOptionsRQ);

	public void validateEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ, CustomerSessionStore customerSessionStore);

	public void
			validateEffectiveBalancePaymentRQ(EffectivePaymentRQ effectivePaymentRQ, CustomerSessionStore customerSessionStore);

	public void
			validateRequoteEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ, CustomerSessionStore customerSessionStore);

	public void validateEffectiveAnciModificationRQ(EffectivePaymentRQ effectiveAnciModificationPaymentRQ,
			CustomerSessionStore customerSessionStore);

	public void preValidateCommonPaymentRQ(MakePaymentRQ makePaymentRQ);

	public void validateCreateBookingPaymentRQ(MakePaymentRQ makePaymentRQ, PaymentBaseSessionStore paymentBaseSessionStore)
			throws ModuleException;

	public void validateProcessBalancePayment(MakePaymentRQ balancePaymentRQ, PaymentBaseSessionStore paymentSessionStore)
			throws ModuleException;

	public void validateProcessRequotePayment(MakePaymentRQ requotePaymentRQ, PaymentBaseSessionStore paymentSessionStore)
			throws ModuleException;

	public void validateProcessAnciModificationPayment(MakePaymentRQ anciModificationPaymentRQ,
			PaymentBaseSessionStore paymentSessionStore) throws ModuleException;
	
	public void validateReqouteFlightSearchRQ(AvailabilitySearchRQ availabilitySearchRq, LCCClientReservation reservation) throws ValidationException;
	
	public void validateCreateAliasMakePaymentRQ(MakePaymentRQ makePaymentRQ, String customerId) throws ModuleException;

}
