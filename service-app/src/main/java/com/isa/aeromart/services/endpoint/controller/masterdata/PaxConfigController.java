package com.isa.aeromart.services.endpoint.controller.masterdata;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.ContactConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.UserRegistrationConfigRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.UserRegistrationConfigRS;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;


@Controller
@RequestMapping("paxConfig")
public class PaxConfigController extends StatefulController{

	@ResponseBody
	@RequestMapping(value = "/paxDetails", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public PaxDetailsConfigRS paxDetails(@RequestBody PaxDetailsConfigRQ paxDetailsConfigReq) {
		
		//PaxConfigUtil paxUtil = new PaxConfigUtil();
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		BookingSessionStore sessionStore = getTrasactionStore(paxDetailsConfigReq.getTransactionId());
		PaxDetailsConfigRS paxDetailsConfigResponse = masterDataService.paxDetails(paxDetailsConfigReq, getTrackInfo(),
				sessionStore.getPreferenceInfoForBooking().getSelectedSystem(), sessionStore.getInvolvingCarrierCodes(), sessionStore.getSegments());
		
		paxDetailsConfigResponse.setTransactionId(paxDetailsConfigReq.getTransactionId());
		return paxDetailsConfigResponse;
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/paxContactDetails", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public PaxContactDetailsRS paxContactDetails(@RequestBody PaxContactDetailsRQ paxContactDetailsReq) {
		
		//PaxConfigUtil paxUtil = new PaxConfigUtil();	
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		List<String> carrierCodes = null;
		if (paxContactDetailsReq.getTransactionId() != null) {
			BookingSessionStore sessionStore = getTrasactionStore(paxContactDetailsReq.getTransactionId());
			carrierCodes = sessionStore.getInvolvingCarrierCodes();
		}
		PaxContactDetailsRS paxContactDetailsResponse = masterDataService.paxContactDetails(paxContactDetailsReq,
				carrierCodes, getTrackInfo());

		paxContactDetailsResponse.setTransactionId(paxContactDetailsReq.getTransactionId());
		return paxContactDetailsResponse;
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/v2/paxDetails", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public PaxDetailsConfigRS paxDetailsV2(@RequestBody PaxConfigCollectionRQ paxDetailsConfigReq) {

		// PaxConfigUtil paxUtil = new PaxConfigUtil();
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		BookingSessionStore sessionStore = getTrasactionStore(paxDetailsConfigReq.getTransactionId());
		PaxDetailsConfigRS paxDetailsConfigResponse = masterDataService.unifiedPaxDetails(paxDetailsConfigReq, getTrackInfo(),
				sessionStore.getPreferenceInfoForBooking().getSelectedSystem(), sessionStore.getInvolvingCarrierCodes(),
				sessionStore.getSegments());

		paxDetailsConfigResponse.setTransactionId(paxDetailsConfigReq.getTransactionId());
		return paxDetailsConfigResponse;

	}

	@ResponseBody
	@RequestMapping(value = "/v2/paxContactDetails", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public PaxContactDetailsRS paxContactDetailsV2(@RequestBody ContactConfigCollectionRQ contactDetailsReq) {

		// PaxConfigUtil paxUtil = new PaxConfigUtil();
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		List<String> carrierCodes = null;
		if (contactDetailsReq.getTransactionId() != null) {
			BookingSessionStore sessionStore = getTrasactionStore(contactDetailsReq.getTransactionId());
			carrierCodes = sessionStore.getInvolvingCarrierCodes();
		}
		PaxContactDetailsRS paxContactDetailsResponse = masterDataService.unifiedContactDetails(contactDetailsReq, carrierCodes,
				getTrackInfo());

		paxContactDetailsResponse.setTransactionId(contactDetailsReq.getTransactionId());
		return paxContactDetailsResponse;

	}

	@ResponseBody
	@RequestMapping(value = "/usrRegDetails", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public UserRegistrationConfigRS userRegistraionConfig(@RequestBody UserRegistrationConfigRQ usrRegRQ) throws ModuleException {
		
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		
		UserRegistrationConfigRS usrRegRs = masterDataService.getUserRegistrationConfig(usrRegRQ, getTrackInfo());
		//TODO Change this to be fetch by DB column	
		makeMandatory(usrRegRs);
		
		usrRegRs.setSuccess(true);
		return usrRegRs;
		
	}
	
	private void makeMandatory(UserRegistrationConfigRS usrRegRs) {
		for (UserRegConfigDTO userRegConfigDTO : usrRegRs.getConfigList()) {			
			if (UserRegConfigDTO.FIELD_EMAIL.equals(userRegConfigDTO.getFieldName())  ||
					UserRegConfigDTO.FIELD_MOBILE.equals(userRegConfigDTO.getFieldName())) {
				userRegConfigDTO.setMandatory(true);
			}			
		}
	}
		
}
