package com.isa.aeromart.services.endpoint.delegate.payment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.isa.aeromart.services.endpoint.dto.session.store.GiftVoucherSessionStore;
import com.isa.thinair.paymentbroker.api.dto.payFort.PayFortOnlineParam;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.constant.TapPaymentConfirmConstants;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.payment.IPGHandlerRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati.CmiFatouratiOfflinePaymentConfirmationRequest;
import com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati.CmiFatouratiOfflinePaymentConfirmationResponse;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmRequest;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmResponse;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.session.TransactionFactory;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.TempPaymentTnxTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.bl.tapOffline.TapOfflinePaymentUtils;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

@Service("confirmPaymentService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ConfirmPaymentServiceAdapter implements ConfirmPaymentService {

	private static Log log = LogFactory.getLog(ConfirmPaymentServiceAdapter.class);

	@Autowired
	private TransactionFactory transactionFactory;

	@Autowired
	private PaymentService paymentService;

	@Override
	public PaymentConfirmationRS processPaymentReceipt(String transactionId, IPGHandlerRQ ipgHandlerRequest,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		// TODO validation
		// 1.duplicate request & Session
		// 2.DOS attack & other possible checks
		PaymentConfirmationRS paymentConfirmationResponse = new PaymentConfirmationRS();

		try {

			log.info("Inside ConfirmPaymentServiceAdapter continuePayment");
			if (ipgHandlerRequest == null || ipgHandlerRequest.getPostPaymentDTO() == null) {
				throw new ModuleException("paymentbroker.session.expired");
			}

			PaymentBaseSessionStore sessionStore;
			if (OperationTypes.MODIFY_ONLY == ipgHandlerRequest.getOperationType()) {
				sessionStore = getRequotePaymentSession(transactionId);
			} else {
				sessionStore = getPaymentSessionStore(transactionId);
			}

			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			PostPaymentDTO postPaymentData = null;
			boolean isPaymentSuccess = false;
			// && AppParamUtil.isInDevMode()
			if (SystemPropertyUtil.isPaymentBokerDisabled()) { // only in dev mode

				ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
				ipgResponseDTO.setCardType(2);

			} else {
				paymentConfirmationResponse.setSuccess(true);
				postPaymentData = ipgHandlerRequest.getPostPaymentDTO();
				Map<String, String> receiptyMap = postPaymentData.getReceiptyMap();

				int paymentBrokerRefNo = LCCClientApiUtils.getLccPaymentBrokerRefNo(postPaymentData.getTemporyPaymentMap());
				int tempPayId = LCCClientApiUtils.getTmpPayIdFromTmpPayMap(postPaymentData.getTemporyPaymentMap());

				synchronized (postPaymentData) {

					TempPaymentTnx tempPaymentTnx = ModuleServiceLocator.getReservationBD().loadTempPayment(tempPayId);

					if (TempPaymentTnxTypes.RESERVATION_SUCCESS.equals(tempPaymentTnx.getStatus())
							|| TempPaymentTnxTypes.PAYMENT_SUCCESS.equals(tempPaymentTnx.getStatus())) {
						log.info("Multiple payments detected for PNR :" + postPaymentData.getPnr());
						paymentConfirmationResponse.setPaymentStatus(PaymentStatus.ALREADY_COMPLETED);
						return paymentConfirmationResponse;
					}

					PaymentGateway paymentGateway = postPaymentData.getPaymentGateWay();

					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = postPaymentData.getIpgRequestDTO()
							.getIpgIdentificationParamsDTO();
					Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
					String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);
					String borkerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);

					if (responseTypeXML != null && !"".equals(responseTypeXML) && "true".equals(responseTypeXML)) {
						log.debug("### Respons is of type XML... ");
						XMLResponseDTO xmlResponse = postPaymentData.getXmlResponse();
						if (xmlResponse != null) {
							receiptyMap = getReceiptMap(xmlResponse, "");
						}
					}

					Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
					ipgResponseDTO.setRequestTimsStamp(requestTime);
					ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
					ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
					ipgResponseDTO.setTemporyPaymentId(tempPayId);
					ipgResponseDTO.setPaypalResponse(postPaymentData.getPaypalResponse());
					ipgResponseDTO.setOnholdCreated(postPaymentData.isOnHoldCreated());
					if(postPaymentData.getIpgRequestDTO().getCardType()!=null)
					{
					ipgResponseDTO.setCardType(Integer.parseInt(postPaymentData.getIpgRequestDTO().getCardType()));
					}
					
					if (postPaymentData.getAmeriaBankSessionInfo() != null) {
						ipgResponseDTO.setAmeriaBankSessionInfo(postPaymentData.getAmeriaBankSessionInfo());
					}
					ipgResponseDTO.setBrokerType(borkerType);

					log.debug("### Getting response data... ###");

					ipgResponseDTO = ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap, ipgResponseDTO,
							ipgIdentificationParamsDTO);

					addPaymentDetail(paymentGateway, ipgResponseDTO);
					
					if (ipgHandlerRequest.isCardToBeSaved()) {
						ipgResponseDTO.setAlias(ipgHandlerRequest.getAlias());
						ipgResponseDTO.setSaveCreditCard(ipgHandlerRequest.isCardToBeSaved());
					}
					

					if (IPGResponseDTO.STATUS_REJECTED.equals(ipgResponseDTO.getStatus())
							&& ipgResponseDTO.getErrorCode().equals("payfort.payment.user.backToMerchant")) {
						// PAY_AT_STORE_BACK_TO_MERCHANT;
					}

					isPaymentSuccess = ipgResponseDTO.isSuccess();

					if (!postPaymentData.isResponseReceived()) {
						postPaymentData.setResponseReceived(true);
						ModuleServiceLocator.getReservationBD().updateTempPaymentEntryPaymentStatus(
								postPaymentData.getTemporyPaymentMap(), ipgResponseDTO);
						injectCreditCardInfo(sessionStore, borkerType, ipgResponseDTO);							
					} else {
						log.info("Multiple payments handling for PNR :" + postPaymentData.getPnr());
						processMultiplePaymentResponses(ipgResponseDTO, postPaymentData.getIpgRefenceNo(), trackInfoDTO,
								transactionId);
					}
				}

				if (isPaymentSuccess) {
					log.info("payment completed successfully for PNR :" + postPaymentData.getPnr());
					((CommonCreditCardPaymentInfo) postPaymentData.getTemporyPaymentMap().get(tempPayId))
							.setPaymentSuccess(isPaymentSuccess);
					paymentConfirmationResponse.setPaymentStatus(PaymentStatus.SUCCESS);
					// paymentConfirmationResponse.setActionStatus(ActionStatus.PAYMENT_COMPLETED);
					sessionStore.storePaymentStatus(PaymentStatus.SUCCESS);
				} else {
					log.info("payment failed for PNR :" + postPaymentData.getPnr());
					paymentConfirmationResponse.setPaymentStatus(PaymentStatus.ERROR);
					// paymentConfirmationResponse.setActionStatus(ActionStatus.Er);
					sessionStore.storePaymentStatus(PaymentStatus.ERROR);
					if (ipgResponseDTO != null) {
						IPGTransactionResultDTO ipgTransactionResultDTO = ipgResponseDTO.getIpgTransactionResultDTO();
						// if (!postPayDTO.getIpgResponseDTO().isSuccess()) {
						if (ipgResponseDTO != null) {
							String ipgDTOlog = "[status:" + ipgResponseDTO.getStatus() + ",tmpPayId:"
									+ ipgResponseDTO.getTemporyPaymentId() + ",payBkrRefNo:"
									+ ipgResponseDTO.getPaymentBrokerRefNo() + "]";

							log.info(ipgDTOlog);
						}
						String paymentErrorCode = ipgResponseDTO.getErrorCode();
						if (paymentErrorCode != null && paymentErrorCode.trim().length() > 0) {
							// TODO : revert loyaltyRedemption
							// CustomerUtil.revertLoyaltyRedemption(request);

							ModuleException me = getStandardPaymentBrokerError(paymentErrorCode);

							throw me;

						} else {
							log.warn("Possible Intruder IP Detected : ");

							throw new ModuleException("paymentbroker.invalid.errorcode.possible.intruder");
						}
						// }
					}

				}

				// }

				postPaymentData.setIpgResponseDTO(ipgResponseDTO);
				if (IPGResponseDTO.STATUS_ACCEPTED.equals(ipgResponseDTO.getStatus())) {
					postPaymentData.setInvoiceStatus(QiwiPRequest.PAID);
				}

			}

		} catch (Exception me) {
			// revertLoyaltyRedemption();
			log.error("processPaymentReceipt ", me);
			/* If successful reservation exist. Load the reservation. */
			boolean isPaymentBrokerError = false;

			if (me instanceof ModuleException) {
				ModuleException moduleException = (ModuleException) me;
				String errorCode = moduleException.getExceptionCode();
				/**
				 * When credit card payment fail , the system recreate the payment page for any payment gateway.
				 *
				 */
				if (errorCode.equals(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR)) {
					log.error("processPaymentReceipt error @ payment gateway");
					isPaymentBrokerError = true;
					String strErrMsg = "";
					try {
						Map<String, String> errorMap = (Map) moduleException.getExceptionDetails();
						Iterator<String> itErrorMap = errorMap.keySet().iterator();

						if (itErrorMap.hasNext()) {
							errorCode = (String) itErrorMap.next();
						}

						strErrMsg = PaymentConsts.REQ_PAYMENT_GATEWAY_ERROR + errorCode;
						if (errorCode == null || "".equals(errorCode) || strErrMsg == null || "".equals(strErrMsg)) {
							strErrMsg = "pay.gateway.error.less.0";
						}
						throw new ModuleException(strErrMsg);
					} catch (Exception ex) {
						log.error("processPaymentReceipt", ex);
						errorCode = "pay.gateway.error.less.0";
					}
					ModuleException modExcption = new ModuleException(strErrMsg);

					modExcption.setErrorType(ErrorType.PAYMENT_ERROR);

					throw modExcption;

				} else {
					isPaymentBrokerError = false;
				}
			} else {
				isPaymentBrokerError = false;
			}

			if (isPaymentBrokerError == false) {
				// TODO:
				// if (isPaymentSuccess) {
				// msg = CommonUtil.getRefundMessage(language);
				// } else {
				// msg = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL,
				// SessionUtil.getLanguage(request));
				// }

				// request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
				// SessionUtil.resetSesionDataInError(request);
			}

		}

		log.info("end processing ............................");
		return paymentConfirmationResponse;
	}

   // NEEDS TO BE REFACTORED
	@Override
	public PaymentConfirmationRS processPaymentReceiptForGiftVoucher(String transactionId, IPGHandlerRQ ipgHandlerRequest,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		// TODO validation
		// 1.duplicate request & Session
		// 2.DOS attack & other possible checks
		PaymentConfirmationRS paymentConfirmationResponse = new PaymentConfirmationRS();

		try {

			log.info("Inside ConfirmPaymentServiceAdapter continuePayment");
			if (ipgHandlerRequest == null || ipgHandlerRequest.getPostPaymentDTO() == null) {
				throw new ModuleException("paymentbroker.session.expired");
			}

			GiftVoucherSessionStore sessionStore = getGiftVoucherSessionStore(transactionId);

			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			PostPaymentDTO postPaymentData = null;
			boolean isPaymentSuccess = false;
			// && AppParamUtil.isInDevMode()
			if (SystemPropertyUtil.isPaymentBokerDisabled()) { // only in dev mode

				ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
				ipgResponseDTO.setCardType(2);

			} else {
				paymentConfirmationResponse.setSuccess(true);
				postPaymentData = ipgHandlerRequest.getPostPaymentDTO();
				Map<String, String> receiptyMap = postPaymentData.getReceiptyMap();

				int paymentBrokerRefNo = LCCClientApiUtils.getLccPaymentBrokerRefNo(postPaymentData.getTemporyPaymentMap());
				int tempPayId = LCCClientApiUtils.getTmpPayIdFromTmpPayMap(postPaymentData.getTemporyPaymentMap());

				synchronized (postPaymentData) {

					TempPaymentTnx tempPaymentTnx = ModuleServiceLocator.getReservationBD().loadTempPayment(tempPayId);

					if (TempPaymentTnxTypes.RESERVATION_SUCCESS.equals(tempPaymentTnx.getStatus())
							|| TempPaymentTnxTypes.PAYMENT_SUCCESS.equals(tempPaymentTnx.getStatus())) {
						log.info("Multiple payments detected for PNR :" + postPaymentData.getPnr());
						paymentConfirmationResponse.setPaymentStatus(PaymentStatus.ALREADY_COMPLETED);
						return paymentConfirmationResponse;
					}

					PaymentGateway paymentGateway = postPaymentData.getPaymentGateWay();

					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = postPaymentData.getIpgRequestDTO()
							.getIpgIdentificationParamsDTO();
					Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
					String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);
					String borkerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);

					if (responseTypeXML != null && !"".equals(responseTypeXML) && "true".equals(responseTypeXML)) {
						log.debug("### Respons is of type XML... ");
						XMLResponseDTO xmlResponse = postPaymentData.getXmlResponse();
						if (xmlResponse != null) {
							receiptyMap = getReceiptMap(xmlResponse, "");
						}
					}

					Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
					ipgResponseDTO.setRequestTimsStamp(requestTime);
					ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
					ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
					ipgResponseDTO.setTemporyPaymentId(tempPayId);
					ipgResponseDTO.setPaypalResponse(postPaymentData.getPaypalResponse());
					ipgResponseDTO.setOnholdCreated(postPaymentData.isOnHoldCreated());

					if(postPaymentData.getIpgRequestDTO().getCardType()!=null) {
						ipgResponseDTO.setCardType(Integer.parseInt(postPaymentData.getIpgRequestDTO().getCardType()));
					}

					if (postPaymentData.getAmeriaBankSessionInfo() != null) {
						ipgResponseDTO.setAmeriaBankSessionInfo(postPaymentData.getAmeriaBankSessionInfo());
					}

					ipgResponseDTO.setBrokerType(borkerType);

					log.debug("### Getting response data... ###");

					ipgResponseDTO = ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap, ipgResponseDTO,
							ipgIdentificationParamsDTO);

					addPaymentDetail(paymentGateway, ipgResponseDTO);

					if (ipgHandlerRequest.isCardToBeSaved()) {
						ipgResponseDTO.setAlias(ipgHandlerRequest.getAlias());
						ipgResponseDTO.setSaveCreditCard(ipgHandlerRequest.isCardToBeSaved());
					}


					if (IPGResponseDTO.STATUS_REJECTED.equals(ipgResponseDTO.getStatus())
							&& ipgResponseDTO.getErrorCode().equals("payfort.payment.user.backToMerchant")) {
						// PAY_AT_STORE_BACK_TO_MERCHANT;
					}

					isPaymentSuccess = ipgResponseDTO.isSuccess();

					if (!postPaymentData.isResponseReceived()) {
						postPaymentData.setResponseReceived(true);
						ModuleServiceLocator.getReservationBD().updateTempPaymentEntryPaymentStatus(
								postPaymentData.getTemporyPaymentMap(), ipgResponseDTO);
						//injectCreditCardInfo(sessionStore, borkerType, ipgResponseDTO);
					} else {
						log.info("Multiple payments handling for PNR :" + postPaymentData.getPnr());
						processMultiplePaymentResponses(ipgResponseDTO, postPaymentData.getIpgRefenceNo(), trackInfoDTO,
								transactionId);
					}
				}

				if (isPaymentSuccess) {
					log.info("payment completed successfully for PNR :" + postPaymentData.getPnr());
					((CommonCreditCardPaymentInfo) postPaymentData.getTemporyPaymentMap().get(tempPayId))
							.setPaymentSuccess(isPaymentSuccess);
					paymentConfirmationResponse.setPaymentStatus(PaymentStatus.SUCCESS);
					// paymentConfirmationResponse.setActionStatus(ActionStatus.PAYMENT_COMPLETED);
					sessionStore.setPaymentStatus(PaymentStatus.SUCCESS);
				} else {
					log.info("payment failed for PNR :" + postPaymentData.getPnr());
					paymentConfirmationResponse.setPaymentStatus(PaymentStatus.ERROR);
					// paymentConfirmationResponse.setActionStatus(ActionStatus.Er);
					sessionStore.setPaymentStatus(PaymentStatus.ERROR);
					if (ipgResponseDTO != null) {
						IPGTransactionResultDTO ipgTransactionResultDTO = ipgResponseDTO.getIpgTransactionResultDTO();
						// if (!postPayDTO.getIpgResponseDTO().isSuccess()) {
						if (ipgResponseDTO != null) {
							String ipgDTOlog = "[status:" + ipgResponseDTO.getStatus() + ",tmpPayId:"
									+ ipgResponseDTO.getTemporyPaymentId() + ",payBkrRefNo:"
									+ ipgResponseDTO.getPaymentBrokerRefNo() + "]";

							log.info(ipgDTOlog);
						}
						String paymentErrorCode = ipgResponseDTO.getErrorCode();
						if (paymentErrorCode != null && paymentErrorCode.trim().length() > 0) {
							// TODO : revert loyaltyRedemption
							// CustomerUtil.revertLoyaltyRedemption(request);

							ModuleException me = getStandardPaymentBrokerError(paymentErrorCode);

							throw me;

						} else {
							log.warn("Possible Intruder IP Detected : ");

							throw new ModuleException("paymentbroker.invalid.errorcode.possible.intruder");
						}
						// }
					}

				}

				// }

				postPaymentData.setIpgResponseDTO(ipgResponseDTO);
				if (IPGResponseDTO.STATUS_ACCEPTED.equals(ipgResponseDTO.getStatus())) {
					postPaymentData.setInvoiceStatus(QiwiPRequest.PAID);
				}

			}

		} catch (Exception me) {
			// revertLoyaltyRedemption();
			log.error("processPaymentReceipt ", me);
			/* If successful reservation exist. Load the reservation. */
			boolean isPaymentBrokerError = false;

			if (me instanceof ModuleException) {
				ModuleException moduleException = (ModuleException) me;
				String errorCode = moduleException.getExceptionCode();
				/**
				 * When credit card payment fail , the system recreate the payment page for any payment gateway.
				 *
				 */
				if (errorCode.equals(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR)) {
					log.error("processPaymentReceipt error @ payment gateway");
					isPaymentBrokerError = true;
					String strErrMsg = "";
					try {
						Map<String, String> errorMap = (Map) moduleException.getExceptionDetails();
						Iterator<String> itErrorMap = errorMap.keySet().iterator();

						if (itErrorMap.hasNext()) {
							errorCode = (String) itErrorMap.next();
						}

						strErrMsg = PaymentConsts.REQ_PAYMENT_GATEWAY_ERROR + errorCode;
						if (errorCode == null || "".equals(errorCode) || strErrMsg == null || "".equals(strErrMsg)) {
							strErrMsg = "pay.gateway.error.less.0";
						}
						throw new ModuleException(strErrMsg);
					} catch (Exception ex) {
						log.error("processPaymentReceipt", ex);
						errorCode = "pay.gateway.error.less.0";
					}
					ModuleException modExcption = new ModuleException(strErrMsg);

					modExcption.setErrorType(ErrorType.PAYMENT_ERROR);

					throw modExcption;

				} else {
					isPaymentBrokerError = false;
				}
			} else {
				isPaymentBrokerError = false;
			}

			if (isPaymentBrokerError == false) {
				// TODO:
				// if (isPaymentSuccess) {
				// msg = CommonUtil.getRefundMessage(language);
				// } else {
				// msg = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL,
				// SessionUtil.getLanguage(request));
				// }

				// request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
				// SessionUtil.resetSesionDataInError(request);
			}

		}

		log.info("end processing ............................");
		return paymentConfirmationResponse;
	}


	private void injectCreditCardInfo(PaymentBaseSessionStore sessionStore, String borkerType, IPGResponseDTO ipgResponseDTO) {
		
		Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap = sessionStore.getPostPaymentInformation().getTemporyPaymentMap();		
		
		if (temporyPaymentMap != null && !BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(borkerType)) {
			for (CommonCreditCardPaymentInfo ccPayInfo : temporyPaymentMap.values()) {
				ccPayInfo.setNo(ipgResponseDTO.getCcLast4Digits());
			}
		}		
	}

	private void processMultiplePaymentResponses(IPGResponseDTO ipgResponseDTO, String merchantTxnRef, TrackInfoDTO trackInfo,
			String transactionId) {

		List<LCCSegmentSeatDTO> blockSeatDTOs = new ArrayList<LCCSegmentSeatDTO>();

		PaymentSessionStore paymentStore = getPaymentSessionStore(transactionId);

		SYSTEM selectedSystem = ProxyConstants.SYSTEM.getEnum(paymentStore.getPreferencesForPayment().getSelectedSystem());

		StringBuilder logInfo = new StringBuilder();
		logInfo.append("### Multiple Response process start.. ###").append("\n");
		logInfo.append("& PNR :").append(paymentStore.getPnr());
		logInfo.append("& merchantTxnRef :").append(merchantTxnRef);
		logInfo.append("& Payment Status :").append(ipgResponseDTO.isSuccess());

		if (ipgResponseDTO.isSuccess()) {
			boolean isUpdate = false;
			try {
				isUpdate = ModuleServiceLocator.getReservationBD().processMultiplePaymentResponses(
						ipgResponseDTO.getTemporyPaymentId());
			} catch (Exception ex) {
				isUpdate = false;
				log.error("Couldn't update payment status:", ex);
			}
			logInfo = new StringBuilder();
			logInfo.append("Multiple response process status : ").append(isUpdate);
			logInfo.append("& merchantTxnRef : ").append(merchantTxnRef);

			log.info(logInfo.toString());

		}

		if (!blockSeatDTOs.isEmpty()) {
			try {
				ModuleServiceLocator.getAirproxyAncillaryBD().releaseSeats(blockSeatDTOs, transactionId, selectedSystem,
						trackInfo);
			} catch (ModuleException e) {
				log.error("Error ==> ", e);
			}
		}

	}

	private Map<String, String> getReceiptMap(XMLResponseDTO xmlResponse, String sessionId) {
		Map<String, String> fields = new LinkedHashMap<String, String>();

		log.debug("### Started generating receipt map...");
		log.debug("### Order ID : " + StringUtil.getNotNullString(xmlResponse.getOrderId()));

		fields.put(PaymentConstants.XML_RESPONSE.ORDERID.getValue(), StringUtil.getNotNullString(xmlResponse.getOrderId()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYID.getValue(), StringUtil.getNotNullString(xmlResponse.getPayId()));
		fields.put(PaymentConstants.XML_RESPONSE.NCSTATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getNcStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERROR.getValue(), StringUtil.getNotNullString(xmlResponse.getNcError()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERRORPLUS.getValue(),
				StringUtil.getNotNullString(xmlResponse.getNcErrorPlus()));
		fields.put(PaymentConstants.XML_RESPONSE.ACCEPTANCE.getValue(), StringUtil.getNotNullString(xmlResponse.getAcceptance()));
		fields.put(PaymentConstants.XML_RESPONSE.STATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.ECI.getValue(), StringUtil.getNotNullString(xmlResponse.getEci()));
		fields.put(PaymentConstants.XML_RESPONSE.AMOUNT.getValue(), StringUtil.getNotNullString(xmlResponse.getAmount()));
		fields.put(PaymentConstants.XML_RESPONSE.CURRENCY.getValue(), StringUtil.getNotNullString(xmlResponse.getCurrency()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYMENT_METHOD.getValue(),
				StringUtil.getNotNullString(xmlResponse.getPaymentMethod()));
		fields.put(PaymentConstants.XML_RESPONSE.BRAND.getValue(), StringUtil.getNotNullString(xmlResponse.getBrand()));
		fields.put(PaymentConstants.XML_RESPONSE.SHAREQUIRED.getValue(), "N");

		fields.put(PaymentConstants.IPG_SESSION_ID, sessionId);

		log.debug("### Returning the receipt map...");
		return fields;
	}

	private void addPaymentDetail(PaymentGateway gateWayInfoDTO, IPGResponseDTO ipgResponseDTO) {
		if (ipgResponseDTO != null && ipgResponseDTO.getIpgTransactionResultDTO() != null && gateWayInfoDTO != null) {
			IPGTransactionResultDTO ipgTransactionResultDTO = ipgResponseDTO.getIpgTransactionResultDTO();
			ipgTransactionResultDTO.setAmount(gateWayInfoDTO.getEffectivePaymentAmount().getPaymentCurrencyValue().toString());
			ipgTransactionResultDTO.setCurrency(gateWayInfoDTO.getPaymentCurrency());
		}
	}

	private ModuleException getStandardPaymentBrokerError(String paymentErrorCode) {
		ModuleException me = new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR);
		Map<String, String> errorMap = new HashMap<String, String>();
		errorMap.put(paymentErrorCode, "External payment broker error:" + paymentErrorCode);
		me.setExceptionDetails(errorMap);
		return me;
	}

	private PaymentSessionStore getPaymentSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	private GiftVoucherSessionStore getGiftVoucherSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	private PaymentRequoteSessionStore getRequotePaymentSession(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	@Override
	public TapOfflinePaymentConfirmResponse confirmTapOfflinePayment(TapOfflinePaymentConfirmRequest request,
			TrackInfoDTO trackInfoDTO) {

		TapOfflinePaymentConfirmResponse response = new TapOfflinePaymentConfirmResponse();
		log.info("Inside confirmTapOfflinePayment method.");
		if (log.isDebugEnabled()) {
			log.debug("### Start.. confirmPayment request for TAP Ref ID " + request.toString());
		}
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		try {
			// load payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					PaymentConsts.TAP_OFFLINE_PG);

			Integer tempPayId = Integer.parseInt(getTempTnxID(request.getOrderNumber()));
			String pgName = paymentBrokerBD.getPaymentGatewayNameFromCCTransaction(tempPayId);

			IPGPaymentOptionDTO tapPgwConfigs = pgwList.stream()
					.filter(pg -> pg.getPaymentGateway() == Integer.parseInt(pgName.split("_")[0])).findFirst().get();

			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					tapPgwConfigs.getPaymentGateway(), tapPgwConfigs.getBaseCurrency());

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			TempPaymentTnx tempPaymentTnx = null;

			if (ipgProps != null) {

				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
				Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);
				ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);

				tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);
				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(tempPaymentTnx.getPnr());
				modes.setRecordAudit(false);

				requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);

				Map<String, String> receiptMap = createReceiptMap(request);
				receiptMap.put(TapPaymentConfirmConstants.ORDER_DONE_BY, tapPgwConfigs.getModuleCode());

				ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(receiptMap, ipgResponseDTO,
						ipgIdentificationParamsDTO);

				response.setSuccess(receiptMap.get(TapPaymentConfirmConstants.RESPONSE_MESSAGE).equals(
						TapOfflinePaymentUtils.SUCCESS) ? true : false);
				if (ipgResponseDTO.getStatus().equals(ipgResponseDTO.STATUS_ACCEPTED)) {
					log.info("TAP OFFLINE Payment Response is success for tapRefId: +" + request.getTapRefId());
					response.setMessage(TapOfflinePaymentUtils.SUCCESS);
				} else {
					log.info("TAP OFFLINE Payment Response is failed for tapRefId: +" + request.getTapRefId());
					response.setMessage(TapOfflinePaymentUtils.ERROR);
				}

				return response;

			} else {
				log.info("confirmTapOfflinePayment : ipg config internal error");
				return null;
			}

		} catch (NumberFormatException e) {
			log.error("confirmTapOfflinePayment invalid order id ", e);
			response.setMessage(TapOfflinePaymentUtils.ERROR);
			return response;
		} catch (Exception e) {
			log.error("confirmTapOfflinePayment: Failed Confirm the Booking", e);
			response.setMessage(TapOfflinePaymentUtils.ERROR);
			return response;
		}

	}

	@Override
	public Map<String, String> confirmPayFortOfflinePayment(Map<String, String> requestData) {
		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
		String status = IPGResponseDTO.STATUS_REJECTED;
		log.info("Inside confirmPayFortOfflinePayment method.");
		if (log.isDebugEnabled()) {
			log.debug("### Start.. confirmPayment request for PAYFORT Merchant Reference" + requestData.toString());
		}
		String merchantRefID = requestData.get(PayFortOnlineParam.MERCHANT_REFERENCE.getName());
		if (StringUtils.isNotEmpty(merchantRefID)) {
			PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
			int tempPayID = Integer.parseInt(merchantRefID.substring(1));
			try {
				// load payment related properties and information
				List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
						PaymentConsts.PAY_FORT_OFFLINE);
				String pgName = paymentBrokerBD.getPaymentGatewayNameFromCCTransaction(tempPayID);

				IPGPaymentOptionDTO pgwConfigs = pgwList.stream()
						.filter(pg -> pg.getPaymentGateway() == Integer.parseInt(pgName.split("_")[0])).findFirst().orElse(null);

				if (pgwConfigs != null) {
					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
							pgwConfigs.getPaymentGateway(), pgwConfigs.getBaseCurrency());
					Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);

					if (ipgProps != null) {
						Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayID);
						ipgResponseDTO.setRequestTimsStamp(requestTime);
						ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
						ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
						ipgResponseDTO.setTemporyPaymentId(tempPayID);

						try {
							ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(requestData, ipgResponseDTO,
									ipgIdentificationParamsDTO);
							status = IPGResponseDTO.STATUS_ACCEPTED;
						} catch (ModuleException exception) {
							log.info("Error occured executing deferred response", exception);
						}

					} else {
						log.info("confirmPayFortOfflinePayment : ipg config internal error");
					}
				} else {
					// This is because some of the updates are coming from pay fort online payments too.
				}
			} catch (NumberFormatException e) {
				log.error("confirmPayFortOfflinePayment invalid order id ", e);
			} catch (Exception e) {
				log.error("confirmPayFortOfflinePayment: Failed Confirm the Booking", e);
			}
		}
		return createResponseForPayFortOffline(status, merchantRefID);
	}

	private Map<String, String> createResponseForPayFortOffline(String status, String merchantRefID) {
		Map<String, String> response = new HashMap<>();
		if (IPGResponseDTO.STATUS_ACCEPTED.equals(status)) {
			log.info("PAY FORT OFFLINE Payment Response is success for merchant reference: " + merchantRefID);
			response.put(PayFortOnlineParam.STATUS.getName(), "success");
		} else {
			log.info("PAY FORT Payment Response is failed for merchant reference: " + merchantRefID);
			response.put(PayFortOnlineParam.STATUS.getName(), "error");
		}
		return response;
	}

	private String getTempTnxID(String orderID) {
		String tempPayID = "";
		if (orderID != null && orderID.length() > 1) {
			tempPayID = orderID.substring(1);
		}
		return tempPayID;
	}

	private Map<String, String> createReceiptMap(TapOfflinePaymentConfirmRequest request) {
		Map<String, String> receiptMap = new HashMap<String, String>();
		receiptMap.put(TapPaymentConfirmConstants.ORDER_NUMBER, request.getOrderNumber());
		receiptMap.put(TapPaymentConfirmConstants.PAY_MODE, request.getPayMode());
		receiptMap.put(TapPaymentConfirmConstants.PAY_REFERENCE_ID, request.getPayRefId());
		receiptMap.put(TapPaymentConfirmConstants.RESPONSE_CODE, request.getResCode());
		receiptMap.put(TapPaymentConfirmConstants.RESPONSE_MESSAGE, request.getResMessage());
		receiptMap.put(TapPaymentConfirmConstants.TAP_REFERENCE_ID, request.getTapRefId());
		return receiptMap;
	}

	@Override
	public CmiFatouratiOfflinePaymentConfirmationResponse confirmCmiFatouratiOfflinePayment(
			CmiFatouratiOfflinePaymentConfirmationRequest confirmRequest,
			TrackInfoDTO trackInfo) {

		CmiFatouratiOfflinePaymentConfirmationResponse response = new CmiFatouratiOfflinePaymentConfirmationResponse();
		log.info("Inside confirmfflinePayment method.");
		if (log.isDebugEnabled()) {
			log.debug("### Start.. confirmPayment request for CMI Fatourati Ref ID " + confirmRequest.toString());
		}
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		try {
			CreditCardTransaction cCTxn=PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionByReference(confirmRequest.getPnr());
			// load payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					PaymentConsts.CMI_FATOURATI);

			Integer tempPayId = cCTxn.getTemporyPaymentId();//Integer.parseInt(getTempTnxID(confirmRequest.getPnr();
			String pgName = paymentBrokerBD.getPaymentGatewayNameFromCCTransaction(tempPayId);

			IPGPaymentOptionDTO cmiFatouratiPgwConfigs = pgwList.stream()
					.filter(pg -> pg.getPaymentGateway() == Integer.parseInt(pgName.split("_")[0])).findFirst().get();

			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					cmiFatouratiPgwConfigs.getPaymentGateway(), cmiFatouratiPgwConfigs.getBaseCurrency());

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			TempPaymentTnx tempPaymentTnx = null;

			if (ipgProps != null) {

				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
				Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);
				ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);

				tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);
				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(tempPaymentTnx.getPnr());
				modes.setRecordAudit(false);

				requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);

				Map<String, String> receiptMap = createReceiptMapCMIFatourati(confirmRequest);
				receiptMap.put(TapPaymentConfirmConstants.ORDER_DONE_BY, cmiFatouratiPgwConfigs.getModuleCode());

				ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(receiptMap, ipgResponseDTO,
						ipgIdentificationParamsDTO);

				response.setSuccess(receiptMap.get(TapPaymentConfirmConstants.RESPONSE_MESSAGE).equals(
						TapOfflinePaymentUtils.SUCCESS) ? true : false);
				if (ipgResponseDTO.getStatus().equals(ipgResponseDTO.STATUS_ACCEPTED)) {
					log.info("CMI fatourati Payment Response is success for tapRefId: +" + confirmRequest.getOrderNumber());
					response.setSuccess(true);
					response.setMessage(TapOfflinePaymentUtils.SUCCESS);
				} else {
					log.info("CMI Fatourati Payment Response is failed for Fatourati: +" + confirmRequest.getOrderNumber());
					response.setMessage(TapOfflinePaymentUtils.ERROR);
					response.setDetailErrorMessage("CMI Fatourati Payment confirmation Response is failed for Fatourati: +" + confirmRequest.getOrderNumber());
				}

				return response;

			} else {
				log.info("CMI Fatourati Payment: ipg config internal error");
				response.setMessage(TapOfflinePaymentUtils.ERROR);
				response.setDetailErrorMessage("CMI Fatourati Payment confirmation Response is failed for Fatourati: +" + confirmRequest.getOrderNumber());
			return response;
			}

		} catch (NumberFormatException e) {
			log.error("CMI Fatourati Payment invalid order id ", e);
			response.setMessage(TapOfflinePaymentUtils.ERROR);
			response.setDetailErrorMessage(e.getMessage());
			return response;
		} catch (Exception e) {
			log.error("CMI Fatourati Payment: Failed Confirm the Booking", e);
			response.setMessage(TapOfflinePaymentUtils.ERROR);
			response.setDetailErrorMessage(e.getMessage());
			return response;
		}

	
	}

	private Map<String, String> createReceiptMapCMIFatourati(
			CmiFatouratiOfflinePaymentConfirmationRequest confirmRequest) {
		Map<String, String>recieptMap=new HashMap<>();
		recieptMap.put("pnr", confirmRequest.getPnr());
		recieptMap.put("orderNumber", confirmRequest.getOrderNumber());
		recieptMap.put("resCode", confirmRequest.getResCode());
		recieptMap.put("resCode", confirmRequest.getResMessage());
		return recieptMap;
	}

}
