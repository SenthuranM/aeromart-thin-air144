package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.ReservationDetails;
import com.isa.aeromart.services.endpoint.dto.customer.SegmentDetails;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;


public class ReservationDetailsAdaptor implements Adaptor<ReservationListTO , ReservationDetails >{

	@Override
	public ReservationDetails adapt(ReservationListTO reservationListTO) {
		
		ReservationDetails reservationDetails = new ReservationDetails();
		
		reservationDetails.setAirlineCode(reservationListTO.getAirlineCode());
		reservationDetails.setMarketingAirlineCode(reservationListTO.getMarketingAirlineCode());
		reservationDetails.setPaxName(reservationListTO.getPaxName());
		reservationDetails.setPnr(reservationListTO.getPnrNo());
		reservationDetails.setPnrStatus(reservationListTO.getPnrStatus());
		
		SegmentDetailsAdaptor segmentDetailsTransformer = new SegmentDetailsAdaptor();
		List<SegmentDetails> segmentDetails = new ArrayList<>();
		
		for(FlightInfoTO flightInfoTO : reservationListTO.getFlightInfo()){
			segmentDetails.add(segmentDetailsTransformer.adapt(flightInfoTO));
		}
		
		reservationDetails.setSegmentDetails(segmentDetails);
		
		return reservationDetails;
	}

}
