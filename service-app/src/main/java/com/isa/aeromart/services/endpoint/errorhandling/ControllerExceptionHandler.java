package com.isa.aeromart.services.endpoint.errorhandling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;
import com.isa.thinair.commons.api.exception.ModuleException;

@ControllerAdvice
public class ControllerExceptionHandler {

	private static final String UNKNOWN_ERROR = "Internal system error occurred.";
	private static Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);
	@Autowired
	private MessageSource errorMessageSource;

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Error> handleUnknownException(Exception exp) {

		log.error("UNKNOWN ERROR CAUGHT @ ControllerExceptionHandler", exp);

		return new ResponseEntity<Error>(new Error(UNKNOWN_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ ValidationException.class })
	public ResponseEntity<Error> handleValidationException(ValidationException exp) {

		log.error("Error caught @ ControllerExceptionHandler", exp);

		String errorMessage = errorMessageSource.getMessage(exp.getCode().getMessageCode(), null,
				LocaleContextHolder.getLocale());
		if ((exp.getCause() != null && exp.getCause().getMessage() != null) && !exp.getCause().getMessage().isEmpty()) {
			errorMessage += exp.getCause().getMessage();
		}
		if (exp.getErrorType().equals(ErrorType.OTHER_ERROR)) {
			return new ResponseEntity<Error>(new Error(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			return new ResponseEntity<Error>(new Error(errorMessage, exp.getErrorType()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * @param exp
	 * @return
	 * 
	 */
	@ExceptionHandler({ ModuleException.class })
	public ResponseEntity<Error> handleModuleException(ModuleException exp) {

		log.error("Error caught @ ControllerExceptionHandler", exp);

		while (exp.getCause() != null) {
			exp = (ModuleException) exp.getCause();
		}
		String errorMessage = "";

		try {
			errorMessage = errorMessageSource.getMessage(exp.getExceptionCode(), null, LocaleContextHolder.getLocale());
		} catch (NoSuchMessageException ex) {
			log.error("Error NoSuchMessageException : ", exp);
			errorMessage = errorMessageSource.getMessage("generic.system.error", null, LocaleContextHolder.getLocale());
		}
		return new ResponseEntity<Error>(new Error(errorMessage, exp.getErrorType()), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public void setErrorMessageSource(MessageSource errorMessageSource) {
		this.errorMessageSource = errorMessageSource;
	}
}
