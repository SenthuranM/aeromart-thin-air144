package com.isa.aeromart.services.endpoint.controller.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.BaseAvailRQAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.DiscountedFareDetailAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.InterlineFareSegChargeAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.ReservationDiscountDTOAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.CalendarRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.FareCalendarRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.FareCalendarSelectedRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.FareQuoteRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.FlightCalendarSelectedRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.ReturnFareCalendarRSAdaptor;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.ancillary.AncillaryService;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.payment.PaymentService;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BaggageRateRS;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.FareCalendarRS;
import com.isa.aeromart.services.endpoint.dto.availability.FareCalendarSelectedRS;
import com.isa.aeromart.services.endpoint.dto.availability.FareQuoteRS;
import com.isa.aeromart.services.endpoint.dto.availability.FlightCalendarRS;
import com.isa.aeromart.services.endpoint.dto.availability.FlightCalendarSelectedRS;
import com.isa.aeromart.services.endpoint.dto.availability.OriginDestinationInfo;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRQ;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRQ;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionExternalChargeSummary;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AvailabilityRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.AvailabilitySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.service.ValidationService;
import com.isa.aeromart.services.endpoint.utils.common.CurrencyDecorator;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

@Controller
@RequestMapping(value = "availability", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
		MediaType.APPLICATION_JSON_VALUE })
public class AvailabilityController extends StatefulController {

	private static Log log = LogFactory.getLog(AvailabilityController.class);

	@Autowired
	private ValidationService validationService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private AncillaryService ancillaryService;

	@ResponseBody
	@RequestMapping(value = "/search/fare/flight/calendar")
	public FareCalendarRS fareCalendarSearch(@RequestBody AvailabilitySearchRQ availabilitySearchReq) throws ModuleException {
		validationService.validateAvailabilityRQ(availabilitySearchReq, getTrackInfo());
		FlightAvailRQ flightAvailRQ = adaptRequestForCalendar(availabilitySearchReq);
		FlightAvailRS flightAvailRS = searchAvailableFlightsWithFare(flightAvailRQ);
		setTransaction(availabilitySearchReq, flightAvailRS);
		initiateTransaction(flightAvailRS.getTransactionIdentifier());
		FareCalendarRS response = adaptFareCalendarResponse(availabilitySearchReq, flightAvailRS);

		CurrencyDecorator currencyDecorator = new CurrencyDecorator(availabilitySearchReq.getPreferences().getCurrency(),
				getTrackInfo());
		currencyDecorator.decorate(response);
		response.setCurrency(currencyDecorator.getCurrency());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/requote/search/fare/flight/calendar")
	public FareCalendarRS requoteFareCalendarSearch(@RequestBody AvailabilitySearchRQ availabilitySearchReq)
			throws ModuleException {
		LCCClientReservation reservation = loadProxyReservation(availabilitySearchReq.getPnr(),
				availabilitySearchReq.isGroupPnr());
		validationService.validateReqouteFlightSearchRQ(availabilitySearchReq, reservation);
		FlightAvailRQ flightAvailRQ = adaptRequestForRequoteFlightSearch(availabilitySearchReq);
		FlightAvailRS flightAvailRS = searchAvailableFlightsForRequote(flightAvailRQ);
		initializeRequoteTransaction(availabilitySearchReq, flightAvailRS);
		FareCalendarRS response = adaptRequoteFareCalendarResponse(availabilitySearchReq, flightAvailRS);
		storePreferredSystem(flightAvailRQ.getAvailPreferences(), response.getTransactionId());
		CurrencyDecorator currencyDecorator = new CurrencyDecorator(availabilitySearchReq.getPreferences().getCurrency(),
				getTrackInfo());
		currencyDecorator.decorate(response);
		response.setCurrency(currencyDecorator.getCurrency());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/search/fare/flight/calendar/minimum")
	public FareCalendarSelectedRS minimumFareCalendarSearch(@RequestBody AvailabilitySearchRQ availabilitySearchReq)
			throws ModuleException {
		validationService.validateAvailabilityRQ(availabilitySearchReq, getTrackInfo());
		FlightAvailRQ flightAvailRQ = adaptRequestForCalendarMinimum(availabilitySearchReq);
		FlightAvailRS flightAvailRS = searchAvailableFlightsWithFare(flightAvailRQ);
		setTransaction(availabilitySearchReq, flightAvailRS);
		initiateTransaction(flightAvailRS.getTransactionIdentifier());
		FareCalendarSelectedRS response = adaptMinimumFareCalendarResponse(availabilitySearchReq, flightAvailRS, flightAvailRQ);
		storePricingInfo(flightAvailRS, availabilitySearchReq.getTravellerQuantity(), flightAvailRQ, response.getTransactionId());
		setPaymentOptions(response);

		CurrencyDecorator currencyDecorator = new CurrencyDecorator(availabilitySearchReq.getPreferences().getCurrency(),
				getTrackInfo());
		currencyDecorator.decorate(response);
		response.setCurrency(currencyDecorator.getCurrency());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/search/fare/flight/calendar/return")
	public FareCalendarRS returnDiscountCalendarSearch(@RequestBody AvailabilitySearchRQ availabilitySearchReq)
			throws ModuleException {
		// TODO do a specific validation for this case
		validationService.validateAvailabilityRQ(availabilitySearchReq, getTrackInfo());
		FlightAvailRQ flightAvailRQ = adaptRequestForCalendarReturnMinimum(availabilitySearchReq);
		FlightAvailRS flightAvailRS = searchAvailableFlightsWithFare(flightAvailRQ);
		setTransaction(availabilitySearchReq, flightAvailRS);
		FareCalendarRS response = adaptReturnFareCalendarResponse(availabilitySearchReq, flightAvailRS);

		CurrencyDecorator currencyDecorator = new CurrencyDecorator(availabilitySearchReq.getPreferences().getCurrency(),
				getTrackInfo());
		currencyDecorator.decorate(response);
		response.setCurrency(currencyDecorator.getCurrency());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/search/flight/calendar")
	public FlightCalendarRS flightCalendarSearch(@RequestBody AvailabilitySearchRQ availabilitySearchReq) throws ModuleException {
		validationService.validateAvailabilityRQ(availabilitySearchReq, getTrackInfo());
		FlightAvailRQ flightAvailRQ = adaptRequestForFlights(availabilitySearchReq);
		FlightAvailRS flightAvailRS = searchAvailableFlightsWithFare(flightAvailRQ);
		setTransaction(availabilitySearchReq, flightAvailRS);
		initiateTransaction(flightAvailRS.getTransactionIdentifier());
		FlightCalendarRS response = adaptFlightCalendarResponse(availabilitySearchReq, flightAvailRS);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/search/flight/calendar/minimum")
	public FlightCalendarSelectedRS minimumFlightCalendarSearch(@RequestBody AvailabilitySearchRQ availabilitySearchReq)
			throws ModuleException {
		validationService.validateAvailabilityRQ(availabilitySearchReq, getTrackInfo());
		FlightAvailRQ flightAvailRQ = adaptRequestForMinimum(availabilitySearchReq);
		FlightAvailRS flightAvailRS = searchAvailableFlightsWithFare(flightAvailRQ);
		setTransaction(availabilitySearchReq, flightAvailRS);
		initiateTransaction(flightAvailRS.getTransactionIdentifier());
		FlightCalendarSelectedRS response = adaptMinimumFlightCalendarResponse(availabilitySearchReq, flightAvailRS,
				flightAvailRQ);
		storePricingInfo(flightAvailRS, availabilitySearchReq.getTravellerQuantity(), flightAvailRQ, response.getTransactionId());
		setPaymentOptions(response);

		CurrencyDecorator currencyDecorator = new CurrencyDecorator(availabilitySearchReq.getPreferences().getCurrency(),
				getTrackInfo());
		currencyDecorator.decorate(response);
		response.setCurrency(currencyDecorator.getCurrency());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/selected/pricing/minimum")
	public FareQuoteRS selectedFlightFareSearch(@RequestBody AvailabilitySearchRQ availabilitySearchReq) throws ModuleException {
		validationService.validateAvailabilityRQ(availabilitySearchReq, getTrackInfo());
		FlightPriceRQ priceQuoteRQ = adaptPriceRequest(availabilitySearchReq);
		FlightPriceRS flightPriceRS = getPriceQuote(priceQuoteRQ);
		setTransaction(availabilitySearchReq, flightPriceRS);
		FareQuoteRS response = adaptPriceQuoteResponse(availabilitySearchReq, flightPriceRS, priceQuoteRQ);
		storeFlexiInfoToSession(response.getTransactionId(), flightPriceRS);
		storePricingInfo(flightPriceRS, availabilitySearchReq.getTravellerQuantity(), priceQuoteRQ, response.getTransactionId());

		setPaymentOptions(response, availabilitySearchReq);

		TravellerQuantity travellerQuantity = availabilitySearchReq.getTravellerQuantity();
		List<Passenger> paxInfo = populatePaxInfo(travellerQuantity);

		PaymentSummaryRQ paymentSummaryRO = new PaymentSummaryRQ();
		paymentSummaryRO.setPaxInfo(paxInfo);
		paymentSummaryRO.setTransactionId(response.getTransactionId());

		AdministrationFeeRS adminFeeRS = getAdminFee(response);
		if (adminFeeRS != null) {
			response.getSelectedFlightPricing()
					.setAdministrationFee(AccelAeroCalculator.parseBigDecimal(adminFeeRS.getAdministrationFee()));
		}

		CurrencyDecorator currencyDecorator = new CurrencyDecorator(availabilitySearchReq.getPreferences().getCurrency(),
				getTrackInfo());
		currencyDecorator.decorate(response);
		response.setCurrency(currencyDecorator.getCurrency());
		return response;

	}

	private AdministrationFeeRS getAdminFee(FareQuoteRS response) throws ModuleException {
		AdministrationFeeRS adminFeeRS = null;
		if (response != null && response.getSelectedFlightPricing() != null) {
			AdministrationFeeRQ adminFeeRQ = new AdministrationFeeRQ();
			adminFeeRQ.setTransactionId(response.getTransactionId());
			BigDecimal totalAmount = response.getSelectedFlightPricing().getTotal().getPrice();
			adminFeeRS = paymentService.getAdminFeeRS(totalAmount, adminFeeRQ, getTrackInfo());
		}
		return adminFeeRS;
	}

	@ResponseBody
	@RequestMapping(value = "/baggage/rates")
	public BaggageRateRS getBaggageRates(@RequestBody AvailabilitySearchRQ availabilitySearchReq) throws ModuleException {
		validationService.validateAvailabilityRQ(availabilitySearchReq, getTrackInfo());
		FlightPriceRQ priceQuoteRQ = adaptPriceRequest(availabilitySearchReq);
		FlightPriceRS flightPriceRS = getPriceQuote(priceQuoteRQ);
		BaggageRateRS baggageRateRS = ancillaryService.getBaggageRates(getTrackInfo(), flightPriceRS, availabilitySearchReq);
		baggageRateRS.setTransactionId(availabilitySearchReq.getTransactionId());
		return baggageRateRS;
	}

	private PaymentMethodsRQ createPaymentsMethodsRs(String transactionId) {
		PaymentMethodsRQ paymentRequest = new PaymentMethodsRQ();
		paymentRequest.setOperationTypes(OperationTypes.MAKE_ONLY);
		paymentRequest.setTransactionId(transactionId);
		return paymentRequest;
	}

	private PaymentMethodsRQ createPaymentsMethodsRs(String transactionId, String originCountryCode) {
		PaymentMethodsRQ paymentRequest = createPaymentsMethodsRs(transactionId);
		paymentRequest.setOriginCountryCode(originCountryCode);
		return paymentRequest;
	}

	private PaymentMethodsRQ createPaymentsMethodsRs(AvailabilitySearchRQ availabilitySearchRQ) throws ModuleException {
		PaymentMethodsRQ paymentRequest = createPaymentsMethodsRs(availabilitySearchRQ.getTransactionId(),
				availabilitySearchRQ.getPreferences().getOriginCountryCode());
		OriginDestinationInfo firstJourney = availabilitySearchRQ.getJourneyInfo().get(0);
		boolean journeyInfoIsNull = (firstJourney == null);

		if (!journeyInfoIsNull) {
			paymentRequest.setFirstSegmentONDCode(firstJourney.getOrigin() + "/" + firstJourney.getDestination());
		}

		return paymentRequest;
	}

	private PaymentMethodsRS getPaymentmethodsRS(PaymentMethodsRQ paymentRequest) throws ModuleException {
		return paymentService.getPaymentMethodsRS(paymentRequest, getTrackInfo());
	}

	private void setPaymentOptions(FareQuoteRS selectedRS, String originCountryCode) throws ModuleException {
		PaymentMethodsRS paymentMethods = getPaymentmethodsRS(
				createPaymentsMethodsRs(selectedRS.getTransactionId(), originCountryCode));
		selectedRS.setPaymentOptions(paymentMethods.getPaymentMethods());
	}

	private void setPaymentOptions(FareQuoteRS selectedRS, AvailabilitySearchRQ availabilitySearchReq) throws ModuleException {
		PaymentMethodsRS paymentMethods = getPaymentmethodsRS(createPaymentsMethodsRs(availabilitySearchReq));
		selectedRS.setPaymentOptions(paymentMethods.getPaymentMethods());
	}

	private void setPaymentOptions(FlightCalendarSelectedRS selectedRS) throws ModuleException {
		PaymentMethodsRS paymentMethods = getPaymentmethodsRS(createPaymentsMethodsRs(selectedRS.getTransactionId()));
		selectedRS.setPaymentOptions(paymentMethods.getPaymentMethods());
	}

	private void setPaymentOptions(FareCalendarRS selectedRS) throws ModuleException {
		PaymentMethodsRS paymentMethods = getPaymentmethodsRS(createPaymentsMethodsRs(selectedRS.getTransactionId()));
		selectedRS.setPaymentOptions(paymentMethods.getPaymentMethods());
	}

	private String initiateTransaction(String transactionId) {
		// terminateTransaction(transactionId);
		return initTransaction(AvailabilitySessionStore.SESSION_KEY, transactionId);
	}

	private AvailabilitySessionStore getAvailabilitySessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private void storePricingInfo(BaseAvailRS availRS, TravellerQuantity travellerQuantity, BaseAvailRQ availRQ,
			String transactionId) {

		AvailabilitySessionStore sessionStore = getAvailabilitySessionStore(transactionId);
		PreferenceInfo preferenceInfo = new PreferenceInfo();
		preferenceInfo.setSeatType(availRQ.getTravelPreferences().getBookingType());
		preferenceInfo.setSelectedSystem(availRQ.getAvailPreferences().getSearchSystem().toString());
		sessionStore.storePreferences(preferenceInfo);

		if (availRS.getSelectedPriceFlightInfo() != null) {
			List<FlightSegmentTO> allSegments = getSelectedFlightSegmentsInfo(availRS.getOriginDestinationInformationList(),
					availRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList());
			InterlineFareSegChargeAdaptor fareSegAdaptor = new InterlineFareSegChargeAdaptor(availRQ);
			FareSegChargeTO fareSegChargeTO = fareSegAdaptor.adapt(availRS);
			sessionStore.storePriceInventoryInfo(fareSegChargeTO);

			sessionStore.storeSelectedSegments(allSegments);
			sessionStore.storeTravellerQuantity(travellerQuantity);
			storeSessionPromotionInfo(availRQ, availRS, sessionStore);
			sessionStore.storeApplicableServiceTaxes(availRS.getApplicableServiceTaxes());
		}
	}

	private FareQuoteRS adaptPriceQuoteResponse(AvailabilitySearchRQ availabilitySearchReq, FlightPriceRS flightPriceRS,
			BaseAvailRQ baseAvailRQ) {
		FareQuoteRSAdaptor adaptor = new FareQuoteRSAdaptor(availabilitySearchReq.getTravellerQuantity(), getTrackInfo(),
				baseAvailRQ.getAvailPreferences().getSearchSystem(), getRPHGenerator(flightPriceRS),
				baseAvailRQ.getAvailPreferences().getOndFlexiSelected(), availabilitySearchReq.getPreferences().getCurrency(),
				availabilitySearchReq.getTransactionId(), false);
		FareQuoteRS response = adaptor.adapt(flightPriceRS);
		return response;
	}

	private FlightPriceRS getPriceQuote(FlightPriceRQ priceQuoteRQ) throws ModuleException {
		return ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(priceQuoteRQ, getTrackInfo());
	}

	private void setTransaction(AvailabilitySearchRQ baseAvail, BaseAvailRS baseResponse) {
		if (baseResponse.getTransactionIdentifier() == null && baseAvail.getTransactionId() != null) {
			baseResponse.setTransactionIdentifier(baseAvail.getTransactionId());
		} else if (baseAvail.getTransactionId() == null && baseResponse.getTransactionIdentifier() == null) {
			String transactionId = UUID.randomUUID().toString();
			baseResponse.setTransactionIdentifier(transactionId);
		}
	}

	private RPHGenerator getRPHGenerator(BaseAvailRS baseResponse) {
		return getRPHGenerator(baseResponse.getTransactionIdentifier());
	}

	private RPHGenerator getRPHGenerator(String transactionId) {
		if (transactionId != null) {
			return getAvailabilitySessionStore(transactionId).getRPHGenerator();
		}
		return null;
	}

	private FlightPriceRQ adaptPriceRequest(AvailabilitySearchRQ availabilitySearchReq) {
		BaseAvailRQAdaptor baseAdaptor = new BaseAvailRQAdaptor(true, false,
				getRPHGenerator(availabilitySearchReq.getTransactionId()), getTrackInfo());
		FlightPriceRQ priceQuoteRQ = new FlightPriceRQ(baseAdaptor.adapt(availabilitySearchReq));
		priceQuoteRQ.setTransactionIdentifier(availabilitySearchReq.getTransactionId());
		return priceQuoteRQ;
	}

	private FlightCalendarSelectedRS adaptMinimumFlightCalendarResponse(AvailabilitySearchRQ availabilitySearchReq,
			FlightAvailRS flightAvailRS, BaseAvailRQ baseAvailRQ) {
		FlightCalendarSelectedRSAdaptor adaptor = new FlightCalendarSelectedRSAdaptor(availabilitySearchReq,
				getRPHGenerator(flightAvailRS), getTrackInfo(), baseAvailRQ.getAvailPreferences().getSearchSystem());
		FlightCalendarSelectedRS response = adaptor.adapt(flightAvailRS);
		return response;
	}

	private FlightCalendarRS adaptFlightCalendarResponse(AvailabilitySearchRQ availabilitySearchReq,
			FlightAvailRS flightAvailRS) {
		CalendarRSAdaptor adaptor = CalendarRSAdaptor
				.getAdaptorForFlightResponse(getRPHGenerator(flightAvailRS.getTransactionIdentifier()));
		return adaptor.adapt(flightAvailRS);
	}

	private FareCalendarRS adaptFareCalendarResponse(AvailabilitySearchRQ availabilitySearchReq, FlightAvailRS flightAvailRS) {
		FareCalendarRSAdaptor adaptor = new FareCalendarRSAdaptor(availabilitySearchReq, getRPHGenerator(flightAvailRS));
		FareCalendarRS response = adaptor.adapt(flightAvailRS);
		return response;
	}

	private FareCalendarRS adaptReturnFareCalendarResponse(AvailabilitySearchRQ availabilitySearchReq,
			FlightAvailRS flightAvailRS) {
		ReturnFareCalendarRSAdaptor adaptor = new ReturnFareCalendarRSAdaptor(availabilitySearchReq,
				getRPHGenerator(flightAvailRS));
		FareCalendarRS response = adaptor.adapt(flightAvailRS);
		return response;
	}

	private FareCalendarSelectedRS adaptMinimumFareCalendarResponse(AvailabilitySearchRQ availabilitySearchReq,
			FlightAvailRS flightAvailRS, BaseAvailRQ baseAvailRQ) {
		FareCalendarSelectedRSAdaptor adaptor = new FareCalendarSelectedRSAdaptor(availabilitySearchReq,
				getRPHGenerator(flightAvailRS), getTrackInfo(), baseAvailRQ.getAvailPreferences().getSearchSystem());
		FareCalendarSelectedRS response = adaptor.adapt(flightAvailRS);
		return response;
	}

	private FlightAvailRQ adaptRequestForMinimum(AvailabilitySearchRQ availabilitySearchReq) {
		BaseAvailRQAdaptor baseAdaptor = new BaseAvailRQAdaptor(true, false, false, getTrackInfo());
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ(baseAdaptor.adapt(availabilitySearchReq));
		return flightAvailRQ;
	}

	private FlightAvailRQ adaptRequestForCalendarMinimum(AvailabilitySearchRQ availabilitySearchReq) {
		BaseAvailRQAdaptor baseAdaptor = new BaseAvailRQAdaptor(true, true, false, getTrackInfo());
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ(baseAdaptor.adapt(availabilitySearchReq));
		return flightAvailRQ;
	}

	private FlightAvailRQ adaptRequestForCalendarReturnMinimum(AvailabilitySearchRQ availabilitySearchReq) {
		BaseAvailRQAdaptor baseAdaptor = BaseAvailRQAdaptor.getOrderPreservedMinimumCalendarAdaptor(
				getRPHGenerator(availabilitySearchReq.getTransactionId()), getTrackInfo());
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ(baseAdaptor.adapt(availabilitySearchReq));
		flightAvailRQ.setTransactionIdentifier(availabilitySearchReq.getTransactionId());
		return flightAvailRQ;
	}

	private FlightAvailRQ adaptRequestForCalendar(AvailabilitySearchRQ availabilitySearchReq) {
		// BaseAvailRQAdaptor baseAdaptor = new BaseAvailRQAdaptor(false, true,false, getTrackInfo());
		BaseAvailRQAdaptor baseAdaptor = new BaseAvailRQAdaptor(false, true,
				getRPHGenerator(availabilitySearchReq.getTransactionId()), getTrackInfo());
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ(baseAdaptor.adapt(availabilitySearchReq));
		return flightAvailRQ;
	}

	private FlightAvailRQ adaptRequestForRequoteFlightSearch(AvailabilitySearchRQ availabilitySearchReq) {
		BaseAvailRQAdaptor baseAdaptor = new BaseAvailRQAdaptor(false, true, true, getTrackInfo());
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ(baseAdaptor.adapt(availabilitySearchReq));
		return flightAvailRQ;
	}

	private FlightAvailRQ adaptRequestForFlights(AvailabilitySearchRQ availabilitySearchReq) {
		BaseAvailRQAdaptor baseAdaptor = new BaseAvailRQAdaptor(false, false, false, getTrackInfo());
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ(baseAdaptor.adapt(availabilitySearchReq));
		return flightAvailRQ;
	}

	private FlightAvailRS searchAvailableFlightsWithFare(FlightAvailRQ flightAvailRQ) throws ModuleException {
		return ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlightsWithAllFares(flightAvailRQ,
				getTrackInfo());
	}

	private FlightAvailRS searchAvailableFlightsForRequote(FlightAvailRQ flightAvailRQ) throws ModuleException {
		return ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ, getTrackInfo(), false);
	}

	private void initializeRequoteTransaction(TransactionalBaseRQ baseAvail, BaseAvailRS baseResponse) {
		String transactionId = null;
		if (baseResponse.getTransactionIdentifier() != null) {
			transactionId = baseResponse.getTransactionIdentifier();
		} else if (baseAvail.getTransactionId() != null) {
			transactionId = baseAvail.getTransactionId();
		}
		transactionId = initTransaction(RequoteSessionStore.SESSION_KEY, transactionId);
		baseResponse.setTransactionIdentifier(transactionId);
	}

	private FareCalendarRS adaptRequoteFareCalendarResponse(AvailabilitySearchRQ availabilitySearchReq,
			FlightAvailRS flightAvailRS) {
		FareCalendarRSAdaptor adaptor = new FareCalendarRSAdaptor(availabilitySearchReq, getRPHGenerator(flightAvailRS));
		return adaptor.adapt(flightAvailRS);
	}

	private void storePreferredSystem(AvailPreferencesTO availPreferences, String transactionId) {
		AvailabilityRequoteSessionStore availabilityRequoteSessionStore = getAvailabilityRequoteSessionStore(transactionId);
		PreferenceInfo preferenceInfo = new PreferenceInfo();
		preferenceInfo.setSelectedSystem(availPreferences.getSearchSystem().toString());
		availabilityRequoteSessionStore.storePreferences(preferenceInfo);
	}

	private AvailabilityRequoteSessionStore getAvailabilityRequoteSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private List<SessionFlexiDetail> getSessionFlexiDetailsList(BaseAvailRS baseAvailRS) {
		List<SessionFlexiDetail> sessionFlexiDetailList = new ArrayList<SessionFlexiDetail>();
		Map<Integer, Boolean> ondWiseFlexiAvailabilityMap = getONDWiseFlexiAvailable(
				baseAvailRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList());
		for (ONDExternalChargeTO ondExternalChargeTO : baseAvailRS.getSelectedPriceFlightInfo().getFareTypeTO()
				.getOndExternalCharges()) {
			SessionFlexiDetail sessionFlexiDetail = new SessionFlexiDetail();
			for (ExternalChargeTO externalChargeTO : ondExternalChargeTO.getExternalCharges()) {
				if (externalChargeTO.getType() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
					SessionExternalChargeSummary externalCharge = new SessionExternalChargeSummary();
					externalCharge.setChargeRateId(externalChargeTO.getChargeRateId());
					externalCharge.setSurchargeCode(externalChargeTO.getSurchargeCode());
					externalCharge.setCarrierCode(externalChargeTO.getCarrierCode());
					externalCharge.setFlexiCharge(externalChargeTO.getAmount());
					externalCharge.setSegmentCode(externalChargeTO.getSegmentCode());
					externalCharge.setAdditionalDetails(externalChargeTO.getAdditionalDetails());
					sessionFlexiDetail.getExternalChargeSummary().add(externalCharge);
					sessionFlexiDetail.setFlexiAvailable(ondWiseFlexiAvailabilityMap.get(ondExternalChargeTO.getOndSequence()));
				}
			}
			// sessionFlexiDetail.setFlexiAvailable(ondWiseFlexiAvailabilityMap.get(ondExternalChargeTO.getOndSequence()));
			sessionFlexiDetail.setOndSequence(ondExternalChargeTO.getOndSequence());
			sessionFlexiDetail.setTotalFlexiCharge(ondExternalChargeTO.getTotalOndExternalCharges());
			sessionFlexiDetail.setFlexiSelected(false); // in availability search no fare class selected
			sessionFlexiDetailList.add(sessionFlexiDetail);
		}
		return sessionFlexiDetailList;
	}

	private Map<Integer, Boolean> getONDWiseFlexiAvailable(List<OndClassOfServiceSummeryTO> availableLogicalCCList) {
		Map<Integer, Boolean> ondWiseFlexiAvailabilityMap = new TreeMap<Integer, Boolean>();
		for (OndClassOfServiceSummeryTO ondClassOfServiceSummeryTO : availableLogicalCCList) {
			boolean isFlexiAvailable = false;
			for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : ondClassOfServiceSummeryTO.getAvailableLogicalCCList()) {
				if (isFlexiBundleAvailable(logicalCabinClassInfoTO) || isBundleWithFlexiSelected(logicalCabinClassInfoTO)) {
					isFlexiAvailable = true;
					break;
				}
			}
			ondWiseFlexiAvailabilityMap.put(ondClassOfServiceSummeryTO.getSequence(), isFlexiAvailable);
		}
		return ondWiseFlexiAvailabilityMap;
	}

	private boolean isBundleWithFlexiSelected(LogicalCabinClassInfoTO logicalCabinClassInfoTO) {
		return logicalCabinClassInfoTO.isSelected() && logicalCabinClassInfoTO.isWithFlexi()
				&& logicalCabinClassInfoTO.getBundledFarePeriodId() != null;
	}

	private boolean isFlexiBundleAvailable(LogicalCabinClassInfoTO logicalCabinClassInfoTO) {
		return logicalCabinClassInfoTO.isFlexiAvailable() && logicalCabinClassInfoTO.getBundledFarePeriodId() == null;
	}

	private void storeFlexiInfoToSession(String transactionId, BaseAvailRS baseAvailRS) {
		if (baseAvailRS != null && baseAvailRS.getSelectedPriceFlightInfo() != null
				&& baseAvailRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList() != null) {
			AvailabilitySessionStore sessionStore = getAvailabilitySessionStore(transactionId);
			List<SessionFlexiDetail> sessionFlexiDetailList = getSessionFlexiDetailsList(baseAvailRS);
			Map<Integer, Boolean> ondFlexiSelection = baseAvailRS.getSelectedPriceFlightInfo().getFareTypeTO()
					.getOndFlexiSelection();
			updateOndFlexiSelectionWithBundle(ondFlexiSelection,
					baseAvailRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList());
			setOndWiseFlexiSelectedFlag(sessionFlexiDetailList, ondFlexiSelection);
			sessionStore.storeSessionFlexiDetails(sessionFlexiDetailList);
		}
	}

	private void setOndWiseFlexiSelectedFlag(List<SessionFlexiDetail> sessionFlexiDetailList,
			Map<Integer, Boolean> ondFlexiSelection) {
		for (Integer ondSeq : ondFlexiSelection.keySet()) {
			for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiDetailList) {
				if (sessionFlexiDetail.getOndSequence() == ondSeq.intValue()) {
					sessionFlexiDetail.setFlexiSelected(ondFlexiSelection.get(ondSeq));
					break;
				}
			}
		}
	}

	private void updateOndFlexiSelectionWithBundle(Map<Integer, Boolean> ondFlexiSelection,
			List<OndClassOfServiceSummeryTO> availableLogicalCCList) {
		if (!ondFlexiSelection.isEmpty() && !availableLogicalCCList.isEmpty()) {
			for (Integer ondSeq : ondFlexiSelection.keySet()) {
				for (OndClassOfServiceSummeryTO availOndFareSummary : availableLogicalCCList) {
					if (availOndFareSummary.getSequence().equals(ondSeq)) {
						for (LogicalCabinClassInfoTO fareSummary : availOndFareSummary.getAvailableLogicalCCList()) {
							if (fareSummary.isSelected() && fareSummary.isWithFlexi()
									&& fareSummary.getBundledFarePeriodId() != null) {
								// selected bundle fare includes flexi
								ondFlexiSelection.put(ondSeq, fareSummary.isSelected());
								break;
							}
						}
					}
				}
			}
		}
	}

	private List<FlightSegmentTO> getSelectedFlightSegmentsInfo(List<OriginDestinationInformationTO> ondInfoList,
			List<OndClassOfServiceSummeryTO> fareSummaryList) {
		List<FlightSegmentTO> allSegments = new ArrayList<FlightSegmentTO>();
		for (OriginDestinationInformationTO ondInfo : ondInfoList) {
			for (OriginDestinationOptionTO ondOption : ondInfo.getOrignDestinationOptions()) {
				if (ondOption.isSelected()) {
					fillSelSegmentFlexiId(ondOption.getFlightSegmentList(), fareSummaryList);
					fillSelSegmentLogicalCabinClass(ondOption.getFlightSegmentList(), fareSummaryList);
					allSegments.addAll(ondOption.getFlightSegmentList());
					break;
				}
			}
		}
		return allSegments;
	}

	private void fillSelSegmentFlexiId(List<FlightSegmentTO> selectedSegments, List<OndClassOfServiceSummeryTO> fareSummaryList) {
		for (FlightSegmentTO segment : selectedSegments) {
			FLEXI_ID_LOOP: for (OndClassOfServiceSummeryTO cosSummary : fareSummaryList) {
				for (LogicalCabinClassInfoTO lccInfo : cosSummary.getAvailableLogicalCCList()) {
					if (cosSummary.getOndCode().contains(segment.getSegmentCode()) && lccInfo.isWithFlexi()
							&& lccInfo.getBundledFarePeriodId() == null) {
						segment.setFlexiID(lccInfo.getFlexiRuleID());
						break FLEXI_ID_LOOP;
					}
				}
			}
		}
	}

	private void fillSelSegmentLogicalCabinClass(List<FlightSegmentTO> selectedSegments,
			List<OndClassOfServiceSummeryTO> fareSummaryList) {
		for (FlightSegmentTO segment : selectedSegments) {
			LOGICAL_CC_LOOP: for (OndClassOfServiceSummeryTO cosSummary : fareSummaryList) {
				for (LogicalCabinClassInfoTO lccInfo : cosSummary.getAvailableLogicalCCList()) {
					if (cosSummary.getOndCode().contains(segment.getSegmentCode()) && lccInfo.isSelected()) {
						segment.setLogicalCabinClassCode(lccInfo.getLogicalCCCode());
						break LOGICAL_CC_LOOP;
					}
				}
			}
		}
	}

	private void storeSessionPromotionInfo(BaseAvailRQ availRQ, BaseAvailRS availRS, AvailabilitySessionStore sessionStore) {
		DiscountedFareDetails discountDetails = new DiscountedFareDetails();
		if (availRS.getSelectedPriceFlightInfo().getFareTypeTO() != null) {
			if (availRS.getSelectedPriceFlightInfo().getFareTypeTO().getPromotionTO() != null) {
				ApplicablePromotionDetailsTO promoInfo = availRS.getSelectedPriceFlightInfo().getFareTypeTO().getPromotionTO();
				DiscountedFareDetailAdaptor discountDetailsAdaptor = new DiscountedFareDetailAdaptor();
				ReservationDiscountDTOAdaptor resDiscAdapt = new ReservationDiscountDTOAdaptor(availRQ, getTrackInfo(), true,
						availRS.getSelectedPriceFlightInfo(), availRS.getTransactionIdentifier(), false);
				discountDetails = discountDetailsAdaptor.adapt(promoInfo);
				sessionStore.storeDiscountDetails(discountDetails);
				if (!PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(discountDetails.getPromotionType())) {
					sessionStore.storeReservationDiscountDTO(resDiscAdapt.adapt(discountDetails));
				}
			} else {
				sessionStore.storeDiscountDetails(null);
				sessionStore.storeReservationDiscountDTO(null);
			}
		}
	}

	private LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR) throws ModuleException {
		return bookingService.loadProxyReservation(pnr, isGroupPNR, getTrackInfo(), false, false, null,
				getTrackInfo().getCarrierCode(), false);
	}

	private void populatePassengrs(List<Passenger> paxInfo, int paxCount, String paxType) {
		if (paxCount > 0) {
			for (int i = 0; i < paxCount; i++) {

				Passenger passsenger = new Passenger();
				passsenger.setPaxSequence(paxInfo.size() + 1);
				passsenger.setLastName("P" + (paxInfo.size() + 1));
				passsenger.setPaxType(paxType);
				paxInfo.add(passsenger);
			}
		}
	}

	private List<Passenger> populatePaxInfo(TravellerQuantity travellerQuantity) {
		List<Passenger> paxInfo = new ArrayList<Passenger>();
		if (travellerQuantity == null)
			return paxInfo;

		int adultCount = travellerQuantity.getAdultCount();
		int childCount = travellerQuantity.getChildCount();
		int infantCount = travellerQuantity.getInfantCount();

		populatePassengrs(paxInfo, adultCount, PaxTypeTO.ADULT);
		populatePassengrs(paxInfo, childCount, PaxTypeTO.CHILD);
		populatePassengrs(paxInfo, infantCount, PaxTypeTO.INFANT);
		return paxInfo;

	}
}