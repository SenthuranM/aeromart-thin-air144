package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.Advertisement;
import com.isa.aeromart.services.endpoint.dto.booking.AdvertisementTemplate;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public abstract class LoadReservationAdaptor<T> implements Adaptor<LCCClientReservation, T> {

	protected static List<Advertisement> getAdvertisements(String carLink, String hotelLink) {

		List<Advertisement> advertisements = new ArrayList<>();

		if (carLink != null && !carLink.isEmpty()) {
			Advertisement carCategory = new Advertisement();
			List<AdvertisementTemplate> carAdvertisements = new ArrayList<>();
			AdvertisementTemplate carAdTemplate = new AdvertisementTemplate();

			boolean linkDynamic = AppSysParamsUtil.isRentACarLinkDynamic();

			carCategory.setCategory(Advertisement.Category.CAR);

			carAdTemplate.setUrl(carLink);
			carAdTemplate.setLinkDynamic(linkDynamic);
			carAdTemplate.setLinkLoadWithinFrame(true);
			carAdvertisements.add(carAdTemplate);
			carCategory.setAdvertisements(carAdvertisements);
			advertisements.add(carCategory);
		}

		if (hotelLink != null && !hotelLink.isEmpty()) {
			Advertisement hotelCategory = new Advertisement();
			List<AdvertisementTemplate> hotelAdvertisements = new ArrayList<>();
			AdvertisementTemplate hotelAdTemplate = new AdvertisementTemplate();

			boolean linkLoadWithinFrame = AppSysParamsUtil.isHotelLinkLoadWithinFrame();

			hotelCategory.setCategory(Advertisement.Category.HOTEL);

			hotelAdTemplate.setUrl(hotelLink);
			hotelAdTemplate.setLinkDynamic(true);
			hotelAdTemplate.setLinkLoadWithinFrame(linkLoadWithinFrame);
			hotelAdvertisements.add(hotelAdTemplate);
			hotelCategory.setAdvertisements(hotelAdvertisements);
			advertisements.add(hotelCategory);
		}

		return advertisements;
	}
}
