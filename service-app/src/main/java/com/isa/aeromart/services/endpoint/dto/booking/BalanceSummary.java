package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Collection;
import java.util.List;

public class BalanceSummary {

	private List<ChargeTemplate> chargeBreakdown;

	private String totalCharges;

	private String totalPayment;

	private String totalCredits;

	private Collection<PaxPayments> paxWisePayments;

	private Collection<PaxCredits> paxWiseCredits;

	public List<ChargeTemplate> getChargeBreakdown() {
		return chargeBreakdown;
	}

	public void setChargeBreakdown(List<ChargeTemplate> chargeBreakdown) {
		this.chargeBreakdown = chargeBreakdown;
	}

	public String getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(String totalCharges) {
		this.totalCharges = totalCharges;
	}

	public String getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(String totalPayment) {
		this.totalPayment = totalPayment;
	}

	public String getTotalCredits() {
		return totalCredits;
	}

	public void setTotalCredits(String totalCredits) {
		this.totalCredits = totalCredits;
	}

	public Collection<PaxPayments> getPaxWisePayments() {
		return paxWisePayments;
	}

	public void setPaxWisePayments(Collection<PaxPayments> paxWisePayments) {
		this.paxWisePayments = paxWisePayments;
	}

	public Collection<PaxCredits> getPaxWiseCredits() {
		return paxWiseCredits;
	}

	public void setPaxWiseCredits(Collection<PaxCredits> paxWiseCredits) {
		this.paxWiseCredits = paxWiseCredits;
	}

}
