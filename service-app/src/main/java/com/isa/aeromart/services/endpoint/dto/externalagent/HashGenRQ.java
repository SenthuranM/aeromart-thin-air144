package com.isa.aeromart.services.endpoint.dto.externalagent;

public class HashGenRQ {
	private String amount;

	private String currencyCode;

	private String token;

	private String orderId;
	
	private String merchantId;

	public String getAmount() {
		return amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getToken() {
		return token;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
}
