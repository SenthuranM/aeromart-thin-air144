package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Rule;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AvailableAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.CustomerPreference;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.AncillaryReservationTo;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.seat.SeatItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.seat.SeatItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.RowSeatPassengerCount;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.SeatsPassengerTypes;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

public class DefaultAncillarySelectorOnCreateReservation extends DefaultAncillarySelector {

	private AncillaryRS availableAncillariesOnCreate;

	private List<SessionPassenger> sessionPassengers;

	Map<String, Map<String, Integer>> rowNumWisePaxTypeCount = new HashMap<String, Map<String, Integer>>();

	private List<String> selectedSeatList = new ArrayList<String>();

	private CustomerPreference customerPreference;

	@Override
	public AncillaryReservationTo toDefaultAncillarySelection(List<Transition<List<String>>> modifications) {
		return null;
	}

	public AvailableAncillaryRQ getAvailableAncillaryRequest() {

		AvailableAncillaryRQ availablieRQ = new AvailableAncillaryRQ();
		availablieRQ.setAncillaries(new ArrayList<Ancillary>());
		Ancillary seat = new Ancillary();
		// for the moment only seat is needed , since getting all the available ancillaries will get lot of processing
		// time we only load needed ancillary types
		seat.setType(AncillariesConstants.AncillaryType.SEAT.getCode());
		availablieRQ.getAncillaries().add(seat);

		return availablieRQ;
	}

	public AncillaryReservationTo toDefaultAncillarySelection() {

		AncillaryReservationTo AncillaryReservationTo = new AncillaryReservationTo();
		AncillaryReservationTo.setAncillaryReservation(new AncillaryReservation());

		List<AncillaryPassenger> passengers = new ArrayList<AncillaryPassenger>();
		AncillaryPassenger ancillaryPassenger;
		PricedAncillaryType pricedAncillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;

		for (SessionPassenger sessionPassenger : sessionPassengers) {
			if (!sessionPassenger.getType().equals(PaxTypeTO.INFANT)) {
				ancillaryPassenger = new AncillaryPassenger();
				ancillaryPassenger.setPassengerRph(sessionPassenger.getPassengerRph());
				ancillaryPassenger.setAncillaryTypes(new ArrayList<PricedAncillaryType>());
				pricedAncillaryType = new PricedAncillaryType();
				pricedAncillaryType.setAncillaryType(AncillariesConstants.AncillaryType.SEAT.getCode());
				pricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());

				List<AncillaryType> anciTypeList = availableAncillariesOnCreate.getAncillaries();
				for (AncillaryType ancillaryType : anciTypeList) {
					if (ancillaryType.getAncillaryType().equals(AncillariesConstants.AncillaryType.SEAT.getCode())
							&& isSeatAssignmentEnabled()) {
						for (Provider provider : ancillaryType.getProviders()) {
							if (provider.getProviderType().equals(Provider.ProviderType.SYSTEM)) {
								List<Rule> validationDefinitions = provider.getAvailableAncillaries().getValidationDefinitions();
								for (AvailableAncillaryUnit availableAncillaryUnit : provider.getAvailableAncillaries()
										.getAvailableUnits()) {
									ancillaryScope = new AncillaryScope();
									ancillaryScope.setScope(availableAncillaryUnit.getScope());
									ancillaryScope.setAncillaries(new ArrayList<PricedSelectedItem>());
									pricedSelectedItem = null;
									if (getCustomerPreferredSeatType() != null) {
										pricedSelectedItem = selectSeatForPax(validationDefinitions, availableAncillaryUnit,
												sessionPassengers, sessionPassenger, true);
									}
									if (pricedSelectedItem == null) {
										pricedSelectedItem = selectSeatForPax(validationDefinitions, availableAncillaryUnit,
												sessionPassengers, sessionPassenger, false);
									}
									if (pricedSelectedItem != null) {
										ancillaryScope.getAncillaries().add(pricedSelectedItem);
									}
									// TODO validate the else condition whether default value need to be set to avoid
									// segment wise issues
									pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
								}

							}
						}
					}
				}
				ancillaryPassenger.getAncillaryTypes().add(pricedAncillaryType);
				passengers.add(ancillaryPassenger);
			}
		}

		AncillaryReservationTo.getAncillaryReservation().setPassengers(passengers);
		return AncillaryReservationTo;
	}

	private boolean isSeatAssignmentEnabled() {
		return AppSysParamsUtil.isAutomaticSeatAssignmentEnabled() || getCustomerPreferredSeatType() != null;
	}

	private PricedSelectedItem selectSeatForPax(List<Rule> validationDefinitions, AvailableAncillaryUnit availableAncillaryUnit,
			List<SessionPassenger> sessionPassengerList, SessionPassenger sessionPassenger, boolean considerPreference) {
		// TODO need to reduce the cyclic redundancy
		SeatsPassengerTypes seatsPassengerTypes;
		RowSeatPassengerCount rowSeatPassengerCount;
		PricedSelectedItem pricedSelectedItem = null;
		boolean seatEligible;
		Integer startingRowNumber = null;
		// for paxTypeRowSeat count rule
		Map<String, Integer> updatedPaxTypeRowSeatCount = new HashMap<String, Integer>();

		String passengerType = AnciCommon.resolvePaxType(sessionPassenger.getType(), sessionPassenger.isParent());
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(getAppIndicator())) {
			startingRowNumber = Integer.parseInt(AppSysParamsUtil.getAutoSeatAssignmentStartingRowNumberForIBE());
		} else if (getAppIndicator().equals(AppIndicatorEnum.APP_XBE)) {
			startingRowNumber = Integer.parseInt(AppSysParamsUtil.getAutoSeatAssignmentStartingRowNumberForXBE());
		}
		SegmentScope segmentScope = (SegmentScope) availableAncillaryUnit.getScope();
		SeatItems seatItems = (com.isa.aeromart.services.endpoint.dto.ancillary.type.seat.SeatItems) availableAncillaryUnit
				.getItemsGroup();
		List<SeatItem> sortedSeatItemList = seatItems.getItems();
		Collections.sort(sortedSeatItemList);
		for (SeatItem seatItem : sortedSeatItemList) {
			seatEligible = false;
			if (seatItem.isAvailability() && (considerPreference || seatItem.getRowNumber() >= startingRowNumber)
					&& !passengerType.equals("")
					&& !selectedSeatList.contains(segmentScope.getFlightSegmentRPH() + "$" + seatItem.getItemId())) {
				List<Integer> validations = seatItem.getValidations();
				String seatPreference = getCustomerPreferredSeatType();
				if (seatPreference == null || (considerPreference && seatPreference.equals(seatItem.getSeatLocationType()))) {
					for (Rule rule : validationDefinitions) {
						if (validations.contains(rule.getRuleValidationId())) {
							if (rule instanceof SeatsPassengerTypes) {
								seatsPassengerTypes = (SeatsPassengerTypes) rule;
								List<String> paxTypeList = seatsPassengerTypes.getPaxTypes();
								if (paxTypeList.contains(passengerType)) {
									seatEligible = false;
								} else {
									seatEligible = true;
								}
							} else if (rule instanceof RowSeatPassengerCount) {
								rowSeatPassengerCount = (RowSeatPassengerCount) rule;
								Map<String, Integer> rowSeatCountMap = rowSeatPassengerCount.getRowSeatPassengetCountMap();
								if (rowSeatCountMap.containsKey(passengerType) && rowSeatCountMap.get(passengerType) > 0) {
									if (rowNumWisePaxTypeCount
											.containsKey(segmentScope.getFlightSegmentRPH() + "$" + seatItem.getRowNumber())) {
										updatedPaxTypeRowSeatCount = rowNumWisePaxTypeCount
												.get(segmentScope.getFlightSegmentRPH() + "$" + seatItem.getRowNumber());
										if (updatedPaxTypeRowSeatCount.containsKey(passengerType)
												&& updatedPaxTypeRowSeatCount.get(passengerType) > 0) {
											seatEligible = true;
											updatedPaxTypeRowSeatCount.put(passengerType,
													updatedPaxTypeRowSeatCount.get(passengerType) - 1);
										} else if (!updatedPaxTypeRowSeatCount.containsKey(passengerType)) {
											seatEligible = true;
											updatedPaxTypeRowSeatCount.put(passengerType, rowSeatCountMap.get(passengerType) - 1);
										} else {
											seatEligible = false;
										}
									} else {
										seatEligible = true;
										updatedPaxTypeRowSeatCount = new HashMap<String, Integer>();
										updatedPaxTypeRowSeatCount.put(passengerType, rowSeatCountMap.get(passengerType) - 1);
									}
								} else {
									seatEligible = false;
								}
							}
						}
					}
				}
			}
			if (seatEligible) {
				pricedSelectedItem = new PricedSelectedItem();
				pricedSelectedItem.setId(seatItem.getItemId());
				pricedSelectedItem.setName(seatItem.getItemName());
				pricedSelectedItem.setAmount(seatItem.getCharges().get(0).getAmount());
				selectedSeatList.add(segmentScope.getFlightSegmentRPH() + "$" + seatItem.getItemId());
				rowNumWisePaxTypeCount.put(segmentScope.getFlightSegmentRPH() + "$" + seatItem.getRowNumber(),
						updatedPaxTypeRowSeatCount);
				break;
			}
		}

		return pricedSelectedItem;
	}

	private String getCustomerPreferredSeatType() {
		if (customerPreference != null) {
			return customerPreference.getSeatType();
		}
		return null;
	}

	public AncillaryRS getAvailableAncillariesOnCreate() {
		return availableAncillariesOnCreate;
	}

	public void setAvailableAncillariesOnCreate(AncillaryRS availableAncillariesOnCreate) {
		this.availableAncillariesOnCreate = availableAncillariesOnCreate;
	}

	public List<SessionPassenger> getSessionPassengers() {
		return sessionPassengers;
	}

	public void setSessionPassengers(List<SessionPassenger> sessionPassengers) {
		this.sessionPassengers = sessionPassengers;
	}

	public void setCustomerPreference(CustomerPreference customerPreference) {
		this.customerPreference = customerPreference;
	}

}
