package com.isa.aeromart.services.endpoint.dto.common;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class BalanceTo {
	
	private BigDecimal totalCreditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();

	public BigDecimal getTotalCreditBalance() {
		return totalCreditBalance;
	}

	public void setTotalCreditBalance(BigDecimal totalCreditBalance) {
		this.totalCreditBalance = totalCreditBalance;
	}

	public BigDecimal getBalanceToPay() {
		return balanceToPay;
	}

	public void setBalanceToPay(BigDecimal balanceToPay) {
		this.balanceToPay = balanceToPay;
	}

}
