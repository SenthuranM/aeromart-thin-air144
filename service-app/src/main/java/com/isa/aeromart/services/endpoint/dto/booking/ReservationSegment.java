package com.isa.aeromart.services.endpoint.dto.booking;

import com.isa.aeromart.services.endpoint.dto.common.FlightSegment;

public class ReservationSegment implements Comparable<ReservationSegment> {

	private String seatType;

	private String cabinClass;

	private int checkInClosingTime;

	private int checkInTimeGap;

	private String reservationSegmentRPH;

	private FlightSegment segment;

	private String fareGroupID;

	private String journeySequence;

	private String logicalCabinClass;

	private String openReturnConfirmBefore;

	private boolean openReturnSegment;

	private String openReturnTravelExpiry;

	private int segmentSequence;

	private int operationType;

	private String status;

	private String subStatus;

	private String standardStatus;// Whether UNSEGMENT or WAITLISTED

	private boolean flownSegment;
	
	private boolean returnSegment;

	public static final String UNSEGMENT = "UN";
	public static final String WAITLISTED = "WL";

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public int getCheckInClosingTime() {
		return checkInClosingTime;
	}

	public void setCheckInClosingTime(int checkInClosingTime) {
		this.checkInClosingTime = checkInClosingTime;
	}

	public int getCheckInTimeGap() {
		return checkInTimeGap;
	}

	public void setCheckInTimeGap(int checkInTimeGap) {
		this.checkInTimeGap = checkInTimeGap;
	}

	public String getReservationSegmentRPH() {
		return reservationSegmentRPH;
	}

	public void setReservationSegmentRPH(String reservationSegmentRPH) {
		this.reservationSegmentRPH = reservationSegmentRPH;
	}

	public FlightSegment getSegment() {
		return segment;
	}

	public void setSegment(FlightSegment segment) {
		this.segment = segment;
	}

	public String getFareGroupID() {
		return fareGroupID;
	}

	public void setFareGroupID(String fareGroupID) {
		this.fareGroupID = fareGroupID;
	}

	public String getJourneySequence() {
		return journeySequence;
	}

	public void setJourneySequence(String journeySequence) {
		this.journeySequence = journeySequence;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public String getOpenReturnConfirmBefore() {
		return openReturnConfirmBefore;
	}

	public void setOpenReturnConfirmBefore(String openReturnConfirmBefore) {
		this.openReturnConfirmBefore = openReturnConfirmBefore;
	}

	public boolean isOpenReturnSegment() {
		return openReturnSegment;
	}

	public void setOpenReturnSegment(boolean openReturnSegment) {
		this.openReturnSegment = openReturnSegment;
	}

	public String getOpenReturnTravelExpiry() {
		return openReturnTravelExpiry;
	}

	public void setOpenReturnTravelExpiry(String openReturnTravelExpiry) {
		this.openReturnTravelExpiry = openReturnTravelExpiry;
	}

	public int getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getStandardStatus() {
		return standardStatus;
	}

	public void setStandardStatus(String standardStatus) {
		this.standardStatus = standardStatus;
	}

	public boolean isFlownSegment() {
		return flownSegment;
	}

	public void setFlownSegment(boolean flownSegment) {
		this.flownSegment = flownSegment;
	}

	public boolean isReturnSegment() {
		return returnSegment;
	}

	public void setReturnSegment(boolean returnSegment) {
		this.returnSegment = returnSegment;
	}

	@Override
	public int compareTo(ReservationSegment o) {
		if (this.getSegment() != null && this.getSegment().getDepartureDateTime().getZulu() != null && o.getSegment() != null
				&& o.getSegment().getDepartureDateTime().getZulu() != null) {
			return this.getSegment().getDepartureDateTime().getZulu().compareTo(o.getSegment().getDepartureDateTime().getZulu());
		}
		return new Integer(this.getSegmentSequence()).compareTo(new Integer(o.getSegmentSequence()));
	}

}
