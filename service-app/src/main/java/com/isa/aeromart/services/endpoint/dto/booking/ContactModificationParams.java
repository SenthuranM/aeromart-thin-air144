package com.isa.aeromart.services.endpoint.dto.booking;

public class ContactModificationParams {

	private boolean contactModifiable;

	public boolean isContactModifiable() {
		return contactModifiable;
	}

	public void setContactModifiable(boolean contactModifiable) {
		this.contactModifiable = contactModifiable;
	}

}
