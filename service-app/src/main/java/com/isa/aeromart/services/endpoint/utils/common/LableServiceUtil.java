package com.isa.aeromart.services.endpoint.utils.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class LableServiceUtil{

	private static final String IBE_COMMON_MESSAGES = "resources/lables";
	private static final String defaultLanguage = "en";

	private static Map<String, ResourceBundle> lables = new HashMap<String, ResourceBundle>();
	
	static {
		loadResourceBundles();
	}

	private static void loadResourceBundles() {
		Map<String, String> suppotedLanguagesMap = AppSysParamsUtil.getIBESupportedLanguages();
		Set<String> lanKeySet = suppotedLanguagesMap.keySet();

		for (Iterator<String> iterator = lanKeySet.iterator(); iterator.hasNext();) {
			String lan = (String) iterator.next();
			Locale locale = new Locale(lan);
			lables.put(lan, ResourceBundle.getBundle(IBE_COMMON_MESSAGES, locale));
		}
	}


	public static String getBalanceSummaryDescription(String lableName, String description) {		
		return getMessage(lableName, description);		
	}
	
	public static String getMessage(String messageCode, String langauge) {
		if (langauge == null || langauge.equals("")){
			langauge = defaultLanguage;
		}
		ResourceBundle rb = lables.get(langauge);
		if(rb == null){
			langauge = defaultLanguage;
			rb = lables.get(langauge);
		}
		String message = "";
		if (messageCode != null && !messageCode.equals("")) {
			try {
				message = rb.getString(messageCode);
			} catch (Exception ibeClintEx) {
				try {
					rb = lables.get(langauge);
					message = rb.getString(messageCode);
				} catch (Exception ibeCommonEx) {
					try {
						message = MessagesUtil.getMessage(messageCode);
					} catch (Exception commonEx) {
						message = "";
					}
				}
			}
			if (message == null) {
				message = "";
			}
		}
		return message;
	}
}
