package com.isa.aeromart.services.endpoint.dto.booking;

public class SocialTO {

	private int marketingPopupDelay;

	public int getMarketingPopupDelay() {
		return marketingPopupDelay;
	}

	public void setMarketingPopupDelay(int marketingPopupDelay) {
		this.marketingPopupDelay = marketingPopupDelay;
	}

}
