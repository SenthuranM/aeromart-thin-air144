package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public abstract class AncillaryRequestAdaptor {

	public abstract String getLCCAncillaryType();

	public abstract Object toAvailableAncillaries(TrackInfoDTO trackInfoDTO, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount);

	public boolean blockSupported() {
		return false;
	}

	public Object toBlockAncillaries(List<SessionPassenger> passengers, RPHGenerator rphGenerator, List<InventoryWrapper> inventory,
			MetaData metaData, List<Preference> preferences, SYSTEM system, String lccTransactionId, TrackInfoDTO trackInfo) {
		return null;
	}

	public Object toReleaseAncillaries(List<SessionPassenger> passengers, RPHGenerator rphGenerator, List<InventoryWrapper> inventory,
			MetaData metaData, List<Preference> preferences, SYSTEM system, String lccTransactionId, TrackInfoDTO trackInfo) {
		return null;
	}

	public abstract Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData);

	public abstract Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData);

	public abstract Object toSaveAncillaries(AncillaryReservation ancillaryReservation);
}
