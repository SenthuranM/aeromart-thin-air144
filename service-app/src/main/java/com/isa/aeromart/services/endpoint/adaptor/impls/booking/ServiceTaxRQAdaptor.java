package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.ServiceTaxRQ;

public class ServiceTaxRQAdaptor implements Adaptor<Object, ServiceTaxRQ> {

	@Override
	public ServiceTaxRQ adapt(Object source) {
		ServiceTaxRQ serviceTaxRQ = new ServiceTaxRQ();

		if (source instanceof MakePaymentRQ) {
			serviceTaxRQ.setCountryCode(((MakePaymentRQ) source).getContactInfo().getCountry());
			serviceTaxRQ.setStateCode(((MakePaymentRQ) source).getContactInfo().getState());
			serviceTaxRQ.setTaxRegNo(((MakePaymentRQ) source).getContactInfo().getTaxRegNo());
			serviceTaxRQ.setTransactionId(((TransactionalBaseRQ) source).getTransactionId());
		}

		if (source instanceof EffectivePaymentRQ) {
			serviceTaxRQ.setCountryCode(((EffectivePaymentRQ) source).getReservationContact().getCountry());
			serviceTaxRQ.setStateCode(((EffectivePaymentRQ) source).getReservationContact().getState());
			serviceTaxRQ.setTaxRegNo(((EffectivePaymentRQ) source).getReservationContact().getTaxRegNo());
			serviceTaxRQ.setTransactionId(((TransactionalBaseRQ) source).getTransactionId());
		}
		
		if (source instanceof PaymentOptionsRQ) {
			if (((PaymentOptionsRQ) source).getReservationContact() != null) {
				serviceTaxRQ.setCountryCode(((PaymentOptionsRQ) source).getReservationContact().getCountry());
				serviceTaxRQ.setStateCode(((PaymentOptionsRQ) source).getReservationContact().getState());
				serviceTaxRQ.setTaxRegNo(((PaymentOptionsRQ) source).getReservationContact().getTaxRegNo());
				serviceTaxRQ.setTransactionId(((PaymentOptionsRQ) source).getTransactionId());
			}
		}

		return serviceTaxRQ;
	}

}
