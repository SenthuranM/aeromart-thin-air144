package com.isa.aeromart.services.endpoint.dto.ancillary.internal;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class InventoryWrapper {

	private FlightSegmentTO cachedFlightSegmentTO;

	private final SessionFlightSegment sessionFlightSegment;

	public InventoryWrapper(SessionFlightSegment sessionFlightSegment) {
		this.sessionFlightSegment = sessionFlightSegment;
	}

	public FlightSegmentTO getFlightSegmentTO() {

		if (cachedFlightSegmentTO == null) {
			cachedFlightSegmentTO = AnciCommon.toFlightSegmentTO(sessionFlightSegment);
		}

		return cachedFlightSegmentTO;
	}

}
