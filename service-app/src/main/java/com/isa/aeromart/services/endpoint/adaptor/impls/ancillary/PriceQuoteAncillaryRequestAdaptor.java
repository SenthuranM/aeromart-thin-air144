package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class PriceQuoteAncillaryRequestAdaptor extends
		TransactionAwareAdaptor<PriceQuoteAncillaryRQ, LCCAncillaryQuotation> {

	public LCCAncillaryQuotation adapt(PriceQuoteAncillaryRQ source) {

		LCCAncillaryQuotation transformed = new LCCAncillaryQuotation();

        RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());
        boolean isAnciModify = getTransactionalAncillaryService().isAncillaryTransaction(getTransactionId());
        transformed.setSystem(ProxyConstants.SYSTEM.getEnum(getTransactionalAncillaryService().getPreferencesForAncillary(getTransactionId()).getSelectedSystem()));
        transformed.setTransactionId(getTransactionId());
        
		MetaData metaData = source.getMetaData();
		List<OndUnit> ondUnits = metaData.getOndPreferences();

		List<SelectedAncillary> ancillaries = source.getAncillaries();

		AncillaryRequestAdaptor transformer;
		AncillariesConstants.AncillaryType ancillaryType;
		Object priceQuoteAnciTo;
		Map<String, ?> priceQuoteAnciSegs;

		List<LCCInsuranceQuotationDTO> insurance;
		LCCSelectedSegmentAncillaryDTO segmentSelection;
		Map<String, LCCSelectedSegmentAncillaryDTO> anciSelections = new HashMap<>();

		Map<String, String> flightSegmentRefToOndIdMapping = AnciCommon.toFlightSegmentRefToOndIdMapping(ondUnits);
		List<InventoryWrapper> inventory = getTransactionalAncillaryService().getFlightSegments(getTransactionId());
		InventoryWrapper inventoryWrapper;
		FlightSegmentTO flightSegmentTO;
        String flightSegmentRph;

        if(ancillaries != null){
			for (SelectedAncillary ancillary : ancillaries) {
				ancillaryType = AncillariesConstants.AncillaryType.resolveAncillaryType(ancillary.getType());

				transformer = AncillaryAdaptorFactory.getRequestAdaptor(ancillary.getType());
				priceQuoteAnciTo = transformer.toPriceQuoteAncillaries(ancillary.getPreferences(), metaData);

				if (ancillaryType != AncillariesConstants.AncillaryType.INSURANCE
						&& ancillaryType != AncillariesConstants.AncillaryType.FLEXIBILITY) {

					priceQuoteAnciSegs = (Map<String, ?>) priceQuoteAnciTo;

					for (String segmentRef : priceQuoteAnciSegs.keySet()) {

						flightSegmentRph = rphGenerator.getUniqueIdentifier(segmentRef);

						if (!anciSelections.containsKey(segmentRef)) {
							inventoryWrapper = AnciCommon.resolveInventory(inventory, flightSegmentRph);
							flightSegmentTO = inventoryWrapper.getFlightSegmentTO();

							segmentSelection = new LCCSelectedSegmentAncillaryDTO();
							segmentSelection.setFlightSegmentTO(flightSegmentTO);
							anciSelections.put(segmentRef, segmentSelection);
						}

						segmentSelection = anciSelections.get(segmentRef);

						switch (ancillaryType) {
						case BAGGAGE:
							segmentSelection.setBaggageDTOs((List<LCCBaggageDTO>) priceQuoteAnciSegs.get(segmentRef));
							segmentSelection.getFlightSegmentTO().setBaggageONDGroupId(
									flightSegmentRefToOndIdMapping.get(segmentRef));
							break;
						case SEAT:
							segmentSelection.setExtraSeatDTOs((List<LCCAirSeatDTO>) priceQuoteAnciSegs.get(segmentRef));
							break;
						case MEAL:
							segmentSelection.setMealDTOs((List<LCCMealDTO>) priceQuoteAnciSegs.get(segmentRef));
							break;
						case SSR_IN_FLIGHT:
							segmentSelection.setSpecialServiceRequestDTOs((List<LCCSpecialServiceRequestDTO>) priceQuoteAnciSegs
									.get(segmentRef));
							break;
						case SSR_AIRPORT:
							segmentSelection.setAirportServiceDTOs((List<LCCAirportServiceDTO>) priceQuoteAnciSegs
									.get(segmentRef));
							break;
						case AIRPORT_TRANSFER:
							segmentSelection.setAirportTransferDTOs((List<LCCAirportServiceDTO>) priceQuoteAnciSegs
									.get(segmentRef));
							break;
						case FLEXIBILITY:
							break;
						case AUTOMATIC_CHECKIN:
							segmentSelection.setAutomaticCheckinDTOs((List<LCCAutomaticCheckinDTO>) priceQuoteAnciSegs
									.get(segmentRef));
							break;
						}
					}

				} else if (ancillaryType == AncillariesConstants.AncillaryType.INSURANCE) {
					insurance = new ArrayList<>();
					insurance.add((LCCInsuranceQuotationDTO) priceQuoteAnciTo);
					transformed.setInsuranceQuotations(insurance);
				}

			}
        }

		transformed.setSegmentQuotations(new ArrayList<>(anciSelections.values()));
		transformed.setTransactionId(isAnciModify ? null : getTransactionId());
		return transformed;
	}
}
