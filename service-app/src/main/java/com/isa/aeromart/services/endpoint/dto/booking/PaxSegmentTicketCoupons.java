package com.isa.aeromart.services.endpoint.dto.booking;


public class PaxSegmentTicketCoupons {

	private String flightSegmentRPH;

	private String eTicketNo;

	private String couponNo;

	private String eTicketStatus;

	private String externalETicketNo;

	private String externalCouponNo;

	private String externalCouponStatus;

	private String externalCouponControl;

	private String paxStatus;

	public String geteTicketNo() {
		return eTicketNo;
	}

	public void seteTicketNo(String eTicketNo) {
		this.eTicketNo = eTicketNo;
	}

	public String getCouponNo() {
		return couponNo;
	}

	public void setCouponNo(String couponNo) {
		this.couponNo = couponNo;
	}

	public String geteTicketStatus() {
		return eTicketStatus;
	}

	public void seteTicketStatus(String eTicketStatus) {
		this.eTicketStatus = eTicketStatus;
	}

	public String getExternalETicketNo() {
		return externalETicketNo;
	}

	public void setExternalETicketNo(String externalETicketNo) {
		this.externalETicketNo = externalETicketNo;
	}

	public String getExternalCouponNo() {
		return externalCouponNo;
	}

	public void setExternalCouponNo(String externalCouponNo) {
		this.externalCouponNo = externalCouponNo;
	}

	public String getExternalCouponStatus() {
		return externalCouponStatus;
	}

	public void setExternalCouponStatus(String externalCouponStatus) {
		this.externalCouponStatus = externalCouponStatus;
	}

	public String getExternalCouponControl() {
		return externalCouponControl;
	}

	public void setExternalCouponControl(String externalCouponControl) {
		this.externalCouponControl = externalCouponControl;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getFlightSegmentRPH() {
		return flightSegmentRPH;
	}

	public void setFlightSegmentRPH(String flightSegmentRPH) {
		this.flightSegmentRPH = flightSegmentRPH;
	}
}
