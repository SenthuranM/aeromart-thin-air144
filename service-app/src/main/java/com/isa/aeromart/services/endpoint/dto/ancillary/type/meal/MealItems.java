package com.isa.aeromart.services.endpoint.dto.ancillary.type.meal;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class MealItems extends AncillaryItemLeaf{

	private List<MealItem> items;

    public MealItems() {
        setType(AncillariesConstants.Type.MEAL);
    }

    public List<MealItem> getItems() {
		return items;
	}

	public void setItems(List<MealItem> items) {
		this.items = items;
	}

}
