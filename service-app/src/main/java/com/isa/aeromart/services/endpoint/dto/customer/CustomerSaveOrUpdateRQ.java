package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class CustomerSaveOrUpdateRQ extends TransactionalBaseRQ{

	private CustomerDetails customer;
	
	private LMSDetails lmsDetails;

	private LoyaltyProfileDetailsRQ loyaltyProfileDetails;

    private boolean optLMS;

	public CustomerDetails getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDetails customer) {
		this.customer = customer;
	}

	public LMSDetails getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LMSDetails lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean isOptLMS() {
		return optLMS;
	}

	public void setOptLMS(boolean optLMS) {
		this.optLMS = optLMS;
	}

	public LoyaltyProfileDetailsRQ getLoyaltyProfileDetails() {
		return loyaltyProfileDetails;
	}

	public void setLoyaltyProfileDetails(LoyaltyProfileDetailsRQ loyaltyProfileDetails) {
		this.loyaltyProfileDetails = loyaltyProfileDetails;
	}

}
