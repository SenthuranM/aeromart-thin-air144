package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.math.BigDecimal;
import java.util.List;

public class AncillaryReservation {

    private List<AncillaryPassenger> passengers;

    private List<PricedAncillaryType> ancillaryTypes;

    private BigDecimal amount;

	private List<MonetaryAmendment> amendments;

	public List<AncillaryPassenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<AncillaryPassenger> passengers) {
        this.passengers = passengers;
    }

    public List<PricedAncillaryType> getAncillaryTypes() {
        return ancillaryTypes;
    }

    public void setAncillaryTypes(List<PricedAncillaryType> ancillaryTypes) {
        this.ancillaryTypes = ancillaryTypes;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}
}
