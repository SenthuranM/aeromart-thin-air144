package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class VoucherOption extends TransactionalBaseRQ {

	private boolean voidRedemption;

	private String lastRedeemedVoucherId;

	private BigDecimal totalRedeemedAmount;

	private boolean totalAmountRedeem;

	public boolean isVoidRedemption() {
		return voidRedemption;
	}

	public void setVoidRedemption(boolean voidRedemption) {
		this.voidRedemption = voidRedemption;
	}

	public BigDecimal getTotalRedeemedAmount() {
		return totalRedeemedAmount;
	}

	public void setTotalRedeemedAmount(BigDecimal totalRedeemedAmount) {
		this.totalRedeemedAmount = totalRedeemedAmount;
	}

	public boolean isTotalAmountRedeem() {
		return totalAmountRedeem;
	}

	public void setTotalAmountRedeem(boolean totalAmountRedeem) {
		this.totalAmountRedeem = totalAmountRedeem;
	}

	public String getLastRedeemedVoucherId() {
		return lastRedeemedVoucherId;
	}

	public void setLastRedeemedVoucherId(String lastRedeemdVoucherId) {
		this.lastRedeemedVoucherId = lastRedeemdVoucherId;
	}
}
