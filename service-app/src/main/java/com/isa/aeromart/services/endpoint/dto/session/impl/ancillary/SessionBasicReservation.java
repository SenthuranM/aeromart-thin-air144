package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

public class SessionBasicReservation {

	private List<SessionPassenger> passengers;

	private SessionContactInformation contactInformation;
	
	private String pnr;
	
	private boolean isPostPaymentExist;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isPostPaymentExist() {
		return isPostPaymentExist;
	}

	public void setPostPaymentExist(boolean isPostPaymentExist) {
		this.isPostPaymentExist = isPostPaymentExist;
	}

	public List<SessionPassenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<SessionPassenger> passengers) {
		this.passengers = passengers;
	}

	public SessionContactInformation getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(SessionContactInformation contactInformation) {
		this.contactInformation = contactInformation;
	}
}
