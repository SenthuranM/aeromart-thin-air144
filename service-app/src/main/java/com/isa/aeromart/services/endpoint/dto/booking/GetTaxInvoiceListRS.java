package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.airreservation.api.model.TaxInvoice;

public class GetTaxInvoiceListRS extends TransactionalBaseRS {

	private String taxInvoice;
	
	private List<TaxInvoice> taxInvoicesList;

	public String getTaxInvoice() {
		return taxInvoice;
	}

	public void setTaxInvoice(String taxInvoice) {
		this.taxInvoice = taxInvoice;
	}

	public List<TaxInvoice> getTaxInvoicesList() {
		return taxInvoicesList;
	}

	public void setTaxInvoicesList(List<TaxInvoice> taxInvoicesList) {
		this.taxInvoicesList = taxInvoicesList;
	}

}
