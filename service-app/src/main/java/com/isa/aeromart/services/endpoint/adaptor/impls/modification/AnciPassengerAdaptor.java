package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.util.Collection;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class AnciPassengerAdaptor implements Adaptor<PassengerChargeTo<LCCClientExternalChgDTO>, ReservationPaxTO> {
	
	private Collection<LCCClientPassengerSummaryTO> passengerSummaryList;
	
	public AnciPassengerAdaptor(Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		this.passengerSummaryList = passengerSummaryList;
	}
	
	@Override
	public ReservationPaxTO adapt(PassengerChargeTo<LCCClientExternalChgDTO> source) {
		ReservationPaxTO resPax = new ReservationPaxTO();
		if (source != null) {
			resPax.addExternalCharges(source.getGetPassengerCharges());
			resPax.setSeqNumber(Integer.parseInt(source.getPassengerRph()));
			if (passengerSummaryList != null) {
				resPax.setTravelerRefNumber(getPassengerRphToSave(passengerSummaryList,
						Integer.parseInt(source.getPassengerRph())));
			}
		}
		return resPax;
	}

	private String getPassengerRphToSave(Collection<LCCClientPassengerSummaryTO> passengerSummaryList, int paxSeqNumber) {
		for (LCCClientPassengerSummaryTO paxSummeryTo : passengerSummaryList) {
			String travelerRefNumber = paxSummeryTo.getTravelerRefNumber();
			if (PaxTypeUtils.getPaxSeq(travelerRefNumber) == paxSeqNumber) {
				return travelerRefNumber;
			}
		}
		return null;
	}

}
