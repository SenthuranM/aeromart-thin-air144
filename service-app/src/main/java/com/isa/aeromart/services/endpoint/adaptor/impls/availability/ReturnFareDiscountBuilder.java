package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airinventory.api.dto.seatavailability.FarePriceOnd;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ReturnFareDiscountBuilder {
	private int currentSequence = -1;
	private BigDecimal currentTotal = null;
	private BigDecimal inboundPerPaxFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private TravellerQuantity travellerQuantity;

	public ReturnFareDiscountBuilder() {

	}
	public ReturnFareDiscountBuilder(BigDecimal inboundPerPaxFlexiCharge, TravellerQuantity travellerQuantity) {
		this.inboundPerPaxFlexiCharge = inboundPerPaxFlexiCharge;
		this.travellerQuantity = travellerQuantity;
	}
	public void setSequence(int ondSequence) {

		if (ondSequence != currentSequence + 1) {
			throw new RuntimeException("Invalid ond iteration");
		}
		currentSequence = ondSequence;
	}

	public boolean isDiscountable() {
		return (currentSequence == OndSequence.IN_BOUND && currentTotal != null);
	}

	public BigDecimal getDiscount(FlightFareSummaryTO flightFareSummaryTO, boolean isFlexiFare) {

		if (flightFareSummaryTO.getMinimumFareByPriceOnd() != null && flightFareSummaryTO.getMinimumFareByPriceOnd().get(FarePriceOnd.RETURN) != null) {
			int totalFareApplicablePaxCount = 0;
			BigDecimal totalPrice = AccelAeroCalculator.add(currentTotal, flightFareSummaryTO.getTotalPrice());
			if (travellerQuantity != null) {
				// assume fare price will be apply to child same as adult
				totalFareApplicablePaxCount = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();
			}
			if (totalFareApplicablePaxCount > 1) {
				totalPrice = AccelAeroCalculator.multiply(totalPrice, totalFareApplicablePaxCount);
			}
			BigDecimal returnTotal = flightFareSummaryTO.getMinimumFareByPriceOnd().get(FarePriceOnd.RETURN);
			// reduce bundle fare
			if (flightFareSummaryTO.getBundleFareFee() != null) {
				if (totalFareApplicablePaxCount > 1) {
					returnTotal = AccelAeroCalculator.add(returnTotal,
							AccelAeroCalculator.multiply(flightFareSummaryTO.getBundleFareFee(), totalFareApplicablePaxCount));
				} else {
					returnTotal = AccelAeroCalculator.add(returnTotal, flightFareSummaryTO.getBundleFareFee());
				}
			}
			// reduce flexi charge
			if (isFlexiFare) {
				if (totalFareApplicablePaxCount > 1) {
					returnTotal = AccelAeroCalculator.add(returnTotal,
							AccelAeroCalculator.multiply(inboundPerPaxFlexiCharge, totalFareApplicablePaxCount));
				} else {
					returnTotal = AccelAeroCalculator.add(returnTotal, inboundPerPaxFlexiCharge);
				}
			}

			// Add the service taxes for the bundled fare charges and flexi charges
			returnTotal = AccelAeroCalculator.add(returnTotal, AccelAeroCalculator.multiply(AccelAeroCalculator.subtract(returnTotal,
					flightFareSummaryTO.getMinimumFareByPriceOnd().get(FarePriceOnd.RETURN)),
					AccelAeroCalculator.divide(flightFareSummaryTO.getServiceTaxPercentageForFares() == null ? AccelAeroCalculator.getDefaultBigDecimalZero() : flightFareSummaryTO.getServiceTaxPercentageForFares(), 100)));

			if (AccelAeroCalculator.isGreaterThan(totalPrice, AccelAeroCalculator.getTwoScaledBigDecimalFromString(returnTotal.toPlainString()))) {
				BigDecimal discount = AccelAeroCalculator.subtract(totalPrice, returnTotal);
				return discount;
			}
		}
		return null;

	}

	public void setTotal(BigDecimal totalPrice) {
		if (currentSequence == OndSequence.OUT_BOUND) {
			if (totalPrice != null) {
				if (currentTotal == null) {
					currentTotal = totalPrice;
				} else if (AccelAeroCalculator.isLessThan(totalPrice, currentTotal)) {
					currentTotal = totalPrice;
				}
			}
		} 
	}

	public int getCurrentSequence() {
		return currentSequence;
	}

	public void setCurrentSequence(int currentSequence) {
		this.currentSequence = currentSequence;
	}

}
