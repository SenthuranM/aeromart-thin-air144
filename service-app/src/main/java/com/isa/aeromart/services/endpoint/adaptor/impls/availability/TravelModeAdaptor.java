package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

public class TravelModeAdaptor {
	public enum TravelMode {
		FLIGHT, BUS
	}

	public static TravelMode adapt(int operationType) {
		if (AirScheduleCustomConstants.OperationTypes.BUS_SERVICE == operationType) {
			return TravelMode.BUS;
		}
		return TravelMode.FLIGHT;
	}
	
	public static TravelMode adapt(String segmentCode){
		try {
			if (ModuleServiceLocator.getAirportBD().isBusSegment(segmentCode)) {
				return TravelMode.BUS;
			}
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		return TravelMode.FLIGHT;
	}
	
	
}