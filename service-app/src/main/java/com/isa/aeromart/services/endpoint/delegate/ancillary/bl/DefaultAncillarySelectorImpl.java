package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.AncillaryReservationTo;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class DefaultAncillarySelectorImpl extends DefaultAncillarySelector {

    private List<OndUnit> prevOndUnits;
    private List<OndUnit> newOndUnits;

    private AncillaryReservation defaultSelections;
    private AncillaryPassenger defaultPassengerSelections;
    private List<PricedAncillaryType> defaultPricedAncillaryTypes;
    private PricedAncillaryType defaultPricedAncillaryType;
    private Map<String , Transition<List<Scope>>> fltSegTransitionMap;

    public AncillaryReservationTo toDefaultAncillarySelection(List<Transition<List<String>>> modifications) {

        AncillaryReservationTo ancillaryReservationTo = new AncillaryReservationTo();
        AncillariesConstants.AncillaryType ancillaryType;
        List<Scope> previousScopes;
        List<Transition<List<Scope>>> transitions;
        MetaData metaData;

        AncillaryType availableAncillaryType;
        List<Scope> availableAncillaryScopes;

        prevOndUnits = getPreviousSelections().getMetaData().getOndPreferences();
        newOndUnits = getAvailableAncillaries().getMetaData().getOndPreferences();
        setAllAnciReportected(true);

        defaultSelections = new AncillaryReservation();
        defaultSelections.setPassengers(new ArrayList<>());
        defaultSelections.setAncillaryTypes(new ArrayList<>());

        AncillaryReservation prevSelections = getPreviousSelections().getAncillaryReservation();
        for (AncillaryPassenger previousPassenger : prevSelections.getPassengers()) {

            defaultPassengerSelections = new AncillaryPassenger();
            defaultPassengerSelections.setPassengerRph(previousPassenger.getPassengerRph());

            defaultPricedAncillaryTypes = new ArrayList<>();

            for (PricedAncillaryType previousPricedAncillaryType : previousPassenger.getAncillaryTypes()) {

                ancillaryType = AncillariesConstants.AncillaryType.resolveAncillaryType(previousPricedAncillaryType.getAncillaryType());
                previousScopes = new ArrayList<>();
                for (AncillaryScope prevAncillaryScope : previousPricedAncillaryType.getAncillaryScopes()) {
                    previousScopes.add(prevAncillaryScope.getScope());
                }

                availableAncillaryType = getAvailableAncillaryType(ancillaryType);
                availableAncillaryScopes = getAvailableAncillaryScopes(ancillaryType);
                transitions = getScopeTransitions(modifications, previousScopes, availableAncillaryScopes);
                defaultPricedAncillaryType = new PricedAncillaryType();
                defaultPricedAncillaryType.setAncillaryType(availableAncillaryType.getAncillaryType());
                defaultPricedAncillaryType.setAncillaryScopes(new ArrayList<>());
                defaultPricedAncillaryType.setAmendments(new ArrayList<>());

                for (Transition<List<Scope>> transition : transitions) {
                    boolean isReprotected = setDefaultSelections(previousPricedAncillaryType, availableAncillaryType, transition);
                    if(!isReprotected && isAllAnciReportected()){
                    	setAllAnciReportected(false);
                    }
                }
                if(!defaultPricedAncillaryType.getAncillaryScopes().isEmpty()){
                	defaultPricedAncillaryTypes.add(defaultPricedAncillaryType);
                }
            }
            defaultPassengerSelections.setAncillaryTypes(defaultPricedAncillaryTypes);
            defaultSelections.getPassengers().add(defaultPassengerSelections);
        }

        ancillaryReservationTo.setAncillaryReservation(defaultSelections);

        metaData = new MetaData();
        metaData.setOndPreferences(newOndUnits);
        ancillaryReservationTo.setMetaData(metaData);

        return ancillaryReservationTo;
    }

    private AncillaryType getAvailableAncillaryType(AncillariesConstants.AncillaryType ancillaryType) {

        List<AncillaryType> availableAncillaryTypes = getAvailableAncillaries().getAncillaries();
        AncillaryType anciType = new AncillaryType();
        anciType.setAncillaryType(ancillaryType.getCode());

        for (AncillaryType availableAncillaryType : availableAncillaryTypes) {
            if (AncillariesConstants.AncillaryType.resolveAncillaryType(availableAncillaryType.getAncillaryType()) == ancillaryType) {
                anciType = availableAncillaryType;
                break;
            }
        }

        return anciType;
    }

    private List<Scope> getAvailableAncillaryScopes(AncillariesConstants.AncillaryType ancillaryType) {

        List<Scope> scopes = new ArrayList<>();
        AvailableAncillary availableAncillary;

        AncillaryType availableAncillaryType = getAvailableAncillaryType(ancillaryType);

        for (Provider provider : availableAncillaryType.getProviders()) {
            availableAncillary = provider.getAvailableAncillaries();

            for (AvailableAncillaryUnit availableAncillaryUnit : availableAncillary.getAvailableUnits()) {
                scopes.add(availableAncillaryUnit.getScope());
            }
        }

        return scopes;
    }

    private List<Transition<List<Scope>>> getScopeTransitions(List<Transition<List<String>>> modifications, List<Scope> prevScopes, List<Scope> newScopes) {

    	Transition<List<Scope>> transition;
    	 
        List<Transition<List<Scope>>> transitions = new ArrayList<>();
        fltSegTransitionMap = new HashMap<String, Transition<List<Scope>>>();
        List<Scope> unchangedScopes = new ArrayList<Scope>();
        List<Scope> changedNewScopes = newScopes;
        List<Scope> changedOldScopes = prevScopes;

        //unchanged scopes update
        for(Scope newScope : newScopes){
        	for(Scope prevScope : prevScopes){
        		if(newScope.equals(prevScope)){
        			transition = new Transition<>();
        			transition.setOld(new ArrayList<>());
        			transition.setNew(new ArrayList<>());
        			transition.getOld().add(prevScope);
        			transition.getNew().add(newScope);
        			transitions.add(transition);
        			unchangedScopes.add(prevScope);
        		}
        	}
        }
        changedNewScopes.removeAll(unchangedScopes);
        changedOldScopes.removeAll(unchangedScopes);

		for (Scope newScope : changedNewScopes) {

			transition = new Transition<>();
			transition.setOld(new ArrayList<>());
			transition.setNew(new ArrayList<>());
			
			// handle for two airport in the same segment to put in the same transition
			if(newScope instanceof AirportScope){
				AirportScope newAirportScope = (AirportScope) newScope;
				if(!fltSegTransitionMap.containsKey(newAirportScope.getFlightSegmentRPH())){
					fltSegTransitionMap.put(newAirportScope.getFlightSegmentRPH(), transition);
				}
				transition = fltSegTransitionMap.get(newAirportScope.getFlightSegmentRPH());
			}

			transition.getNew().add(newScope);

			for (Scope prevScope : changedOldScopes) {

				if (scopesMatch(modifications, prevScope, newScope) && !transition.getOld().contains(prevScope)) {
					transition.getOld().add(prevScope);
				}
			}
			if(!transitions.contains(transition) && !transition.getOld().isEmpty()){
				transitions.add(transition);
			}			
		}

        return transitions;
    }

    private boolean scopesMatch(List<Transition<List<String>>> modifications, Scope prevScope, Scope newScope) {
        boolean scopesMatch = false;

        List<String> prevFlightSegments;
        List<String> newFlightSegments;
        String scopeType;

        OndScope prevOndScope;
        OndScope newOndScope;

        SegmentScope prevSegmentScope;
        SegmentScope newSegmentScope;

        AirportScope prevAirportScope;
        AirportScope newAirportScope;

        scopeType = newScope.getScopeType();

        if (newScope.getScopeType().equals(prevScope.getScopeType())) {
            if (scopeType.equals(Scope.ScopeType.ALL_SEGMENTS)) {
                scopesMatch = true;
            } else if (scopeType.equals(Scope.ScopeType.OND)) {
                prevOndScope = (OndScope)prevScope;
                newOndScope = (OndScope)newScope;

                prevFlightSegments = getSegments(prevOndUnits, prevOndScope.getOndId());
                newFlightSegments = getSegments(newOndUnits, newOndScope.getOndId());

                for (Transition<List<String>> modification : modifications) {
                    if (hasIntersection(modification.getOld(), prevFlightSegments)
                            && hasIntersection(modification.getNew(), newFlightSegments)) {
                        scopesMatch = true;
                        break;
                    }
                }
            } else if (scopeType.equals(Scope.ScopeType.SEGMENT)) {
                prevSegmentScope = (SegmentScope)prevScope;
                newSegmentScope = (SegmentScope)newScope;

                for (Transition<List<String>> modification : modifications) {
                    if (hasIntersection(modification.getOld(), prevSegmentScope.getFlightSegmentRPH())
                            && hasIntersection(modification.getNew(), newSegmentScope.getFlightSegmentRPH())) {
                        scopesMatch = true;
                        break;
                    }
                }
            } else if (scopeType.equals(Scope.ScopeType.AIRPORT)) {
                prevAirportScope = (AirportScope) prevScope;
                newAirportScope = (AirportScope) newScope;

                for (Transition<List<String>> modification : modifications) {
                    if (hasIntersection(modification.getOld(), prevAirportScope.getFlightSegmentRPH())
                            && hasIntersection(modification.getNew(), newAirportScope.getFlightSegmentRPH())) {
                        scopesMatch = true;
                        break;
                    }
                }
            } else {
                scopesMatch = false;
            }
        } else {
            scopesMatch = false;
        }

        return scopesMatch;
    }

    private List<String> getSegments(List<OndUnit> ondUnits, String ondId) {
        List<String> segments = new ArrayList<>();

        for (OndUnit ondUnit : ondUnits) {
            if (ondId.equals(ondUnit.getOndId())) {
                segments = ondUnit.getFlightSegmentRPH();
                break;
            }
        }

        return segments;
    }

	private boolean setDefaultSelections(PricedAncillaryType previousPricedAncillaryType, AncillaryType availableAncillaryType,
			Transition<List<Scope>> transition) {

		DefaultAncillarySelectionStrategy defaultAncillarySelectionStrategy = DefaultAncillarySelectorFactory
				.getDefaultAncillarySelectionStrategy(transition);
		PricedAncillaryType tempPricedAncillaryType = defaultAncillarySelectionStrategy.getDefaultPricedAncillaryType(
				previousPricedAncillaryType, availableAncillaryType, transition);
		boolean isReprotected = false;

		if (tempPricedAncillaryType.getAncillaryScopes() != null && !tempPricedAncillaryType.getAncillaryScopes().isEmpty()) {
			defaultPricedAncillaryType.getAncillaryScopes().addAll(tempPricedAncillaryType.getAncillaryScopes());
			isReprotected = true;
		}
		if (tempPricedAncillaryType.getAmendments() != null) {
			defaultPricedAncillaryType.getAmendments().addAll(tempPricedAncillaryType.getAmendments());
		}

		return isReprotected;
	}

    private <T> boolean hasIntersection(Collection<T> c1, Collection<T> c2) {
        boolean hasIntersection = false;

        for (T t : c1) {
            if (c2.contains(t)) {
                hasIntersection = true;
                break;
            }
        }

        return hasIntersection;
    }

    private <T> boolean hasIntersection(Collection<T> c, T t) {
        boolean hasIntersection = false;

        if (c.contains(t)) {
            hasIntersection = true;
        }

        return hasIntersection;
    }
	
}
