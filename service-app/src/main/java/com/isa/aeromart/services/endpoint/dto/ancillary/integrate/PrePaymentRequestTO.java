package com.isa.aeromart.services.endpoint.dto.ancillary.integrate;

import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;

public class PrePaymentRequestTO {

    private AncillaryIntegrateTO ancillaryIntegrateTo;

    private ReservationInfo reservationInfo;

    public AncillaryIntegrateTO getAncillaryIntegrateTo() {
        return ancillaryIntegrateTo;
    }

    public void setAncillaryIntegrateTo(AncillaryIntegrateTO ancillaryIntegrateTo) {
        this.ancillaryIntegrateTo = ancillaryIntegrateTo;
    }

    public ReservationInfo getReservationInfo() {
        return reservationInfo;
    }

    public void setReservationInfo(ReservationInfo reservationInfo) {
        this.reservationInfo = reservationInfo;
    }
}
