package com.isa.aeromart.services.endpoint.dto.payment;

public class PaymentParams {

	private String echoToken;

	private Integer counter;

	public String getEchoToken() {
		return echoToken;
	}

	public void setEchoToken(String echoToken) {
		this.echoToken = echoToken;
	}

	public Integer getCounter() {
		return counter;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}
}
