package com.isa.aeromart.services.endpoint.dto.session.impl.modification;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;

public class FareInfo {
	
	private FareSegChargeTO fareSegmentChargeTO;
	
	private BigDecimal creditCardFee;

	private String modifyCharge;
	
	private String cancelReservationCharge;

	private String creditableAmount;
	
	private boolean flexiExistsForSelectedFare;	
	
	private Date lastFareQuotedDate;

	private boolean fqWithinValidity;
	
	private Map<Integer, Boolean> ondSelectedFlexi = new HashMap<Integer, Boolean>();
	
	private List<PerPaxPriceInfoTO> perPaxPriceInfo;

	public FareSegChargeTO getFareSegmentChargeTO() {
		return fareSegmentChargeTO;
	}

	public void setFareSegmentChargeTO(FareSegChargeTO fareSegmentChargeTO) {
		this.fareSegmentChargeTO = fareSegmentChargeTO;
	}

	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public String getModifyCharge() {
		return modifyCharge;
	}

	public void setModifyCharge(String modifyCharge) {
		this.modifyCharge = modifyCharge;
	}

	public String getCreditableAmount() {
		return creditableAmount;
	}

	public void setCreditableAmount(String creditableAmount) {
		this.creditableAmount = creditableAmount;
	}

	public boolean isFlexiExistsForSelectedFare() {
		return flexiExistsForSelectedFare;
	}

	public void setFlexiExistsForSelectedFare(boolean flexiExistsForSelectedFare) {
		this.flexiExistsForSelectedFare = flexiExistsForSelectedFare;
	}
	
	public String getCancelReservationCharge() {
		return cancelReservationCharge;
	}

	public void setCancelReservationCharge(String cancelReservationCharge) {
		this.cancelReservationCharge = cancelReservationCharge;
	}

	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public void setLastFareQuotedDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	public boolean isFqWithinValidity() {
		return fqWithinValidity;
	}

	public void setFqWithinValidity(boolean fqWithinValidity) {
		this.fqWithinValidity = fqWithinValidity;
	}

	public Map<Integer, Boolean> getOndSelectedFlexi() {
		return ondSelectedFlexi;
	}

	public void setOndSelectedFlexi(Map<Integer, Boolean> ondSelectedFlexi) {
		this.ondSelectedFlexi = ondSelectedFlexi;
	}

	public List<PerPaxPriceInfoTO> getPerPaxPriceInfo() {
		return perPaxPriceInfo;
	}

	public void setPerPaxPriceInfo(List<PerPaxPriceInfoTO> perPaxPriceInfo) {
		this.perPaxPriceInfo = perPaxPriceInfo;
	}
}
