package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import java.util.Date;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.aircustomer.api.constants.CustomAliasConstants;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;

public class CustomerAliasAdaptor implements Adaptor<IPGRequestDTO, CustomerAlias> {

	@Override
	public CustomerAlias adapt(IPGRequestDTO iPGRequestDTO) {

		CustomerAlias customerAlias = new CustomerAlias();
		customerAlias.setCustomerId(StringUtil.isNullOrEmpty(iPGRequestDTO.getCustomerId()) ? null : Long.parseLong(iPGRequestDTO
				.getCustomerId()));
		customerAlias.setAlias(iPGRequestDTO.getAlias());
		customerAlias.setCcName(StringUtil.isNullOrEmpty(iPGRequestDTO.getCardName()) ? null : iPGRequestDTO.getCardName());
		
		//TODO this is not correct, this should be the alias expiry, not the credit card expiry, fix it
		customerAlias.setExpiryDate(iPGRequestDTO.getExpDate());
//		Date aliasExpiry = CalendarUtil.addDateVarience(new Date(), 90); //TODO
//		customerAlias.setExpirygit Date(aliasExpiry); //TODO
		customerAlias.setHolderName(StringUtil.isNullOrEmpty(iPGRequestDTO.getHolderName()) ? null : iPGRequestDTO
				.getHolderName());
		customerAlias.setNoLastDigits(StringUtil.isNullOrEmpty(iPGRequestDTO.getCardNo()) ? null : BeanUtils
				.getLast4Digits(iPGRequestDTO.getCardNo()));
		customerAlias.setCreateDate(CalendarUtil.getCurrentSystemTimeInZulu());
		customerAlias.setStatus(CustomAliasConstants.ALIAS_ACTIVE);

		return customerAlias;
	}

}
