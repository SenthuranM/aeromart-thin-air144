package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.CreditInfo;
import com.isa.aeromart.services.endpoint.dto.booking.PaxCredits;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class PaxCreditAdaptor implements Adaptor<LCCClientReservationPax, PaxCredits>{
	
	public PaxCredits adapt(LCCClientReservationPax pax){

		PaxCredits paxCredits = new PaxCredits();

		CreditInfoAdaptor creditInfoAdaptor = new CreditInfoAdaptor();

		Collection<CreditInfo> colCreditInfo = new ArrayList<>();

		if (pax.getLccClientPaymentHolder().getCredits() != null && !pax.getLccClientPaymentHolder().getCredits().isEmpty()) {
			AdaptorUtils.adaptCollection(pax.getLccClientPaymentHolder().getCredits(), colCreditInfo, creditInfoAdaptor);
		}
		paxCredits.setPaxSequence(pax.getPaxSequence());
		paxCredits.setCreditInfo(colCreditInfo);

		return paxCredits;
	}

}
