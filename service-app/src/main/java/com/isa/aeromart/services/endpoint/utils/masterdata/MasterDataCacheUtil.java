package com.isa.aeromart.services.endpoint.utils.masterdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.AirportMessageAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.AirportMessageFilterAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.AirportMessageFlightDetailsAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.CountryDetailsAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.CountryWiseStatesAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.CurrencyExchnageRateAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.LanguageAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.PaxTitleAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.PaxTypeWiseTitleAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.RelationshipsAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.dto.customer.FamilyRelationshipDTO;
import com.isa.aeromart.services.endpoint.dto.customer.GenericDropdownDTO;
import com.isa.aeromart.services.endpoint.dto.customer.RelationshipListRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessage;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessageRQParams;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessageRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessagesRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.CountryDetails;
import com.isa.aeromart.services.endpoint.dto.masterdata.CountryListRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.CurrencyExRatesRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.CurrencyExchangeRate;
import com.isa.aeromart.services.endpoint.dto.masterdata.DropDownListRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.IBELanguage;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxTitle;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;
import com.isa.thinair.airmaster.api.dto.CurrencyExchangeRateDTO;
import com.isa.thinair.airmaster.api.dto.PaxTitleDTO;
import com.isa.thinair.airmaster.api.model.FamilyRelationship;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airreservation.api.dto.AirportMessageFilter;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;


public class MasterDataCacheUtil {
	
	private static MasterDataCacheUtil masterDataCacheUtil = null;
	
	private static CurrencyExRatesRS currencyExRatesResponse = new CurrencyExRatesRS();
	
	private static CountryListRS countryListResponse = new CountryListRS();
	
	private static DropDownListRS dropDownLists = new DropDownListRS();
	
	private static RelationshipListRS relationships = new RelationshipListRS();
	
	private static final long CASH_TIMEOUT = AppSysParamsUtil.getCurrencyExchangeRateCacheTimout();

	private volatile Date recordAge = new Date(System.currentTimeMillis() - CASH_TIMEOUT);
	
	private volatile boolean refreshingExchangeRateList = false;
	
	private static Log log = LogFactory.getLog(MasterDataCacheUtil.class);

	
	private MasterDataCacheUtil() {
		super();
	}

	public static MasterDataCacheUtil getInstance() {
		if (masterDataCacheUtil == null) {
			synchronized (MasterDataCacheUtil.class) {
				if (masterDataCacheUtil == null) {
					masterDataCacheUtil = new MasterDataCacheUtil();
				}
			}
		}
		return masterDataCacheUtil;
	}
	
	private void refreshExchangeRateList() throws ModuleException {
		if (System.currentTimeMillis() - recordAge.getTime() >= CASH_TIMEOUT) {
			synchronized (MasterDataCacheUtil.class) {
				if (System.currentTimeMillis() - recordAge.getTime() >= CASH_TIMEOUT) {
					refreshingExchangeRateList = true;
										
					List<CurrencyExchangeRateDTO> currencyExRateDTOList = null;
					List<CurrencyExchangeRate> currencyExRates = new ArrayList<CurrencyExchangeRate>();
					
					try {
						currencyExRateDTOList = ModuleServiceLocator.getCommoMasterBD().getExchangeRates();
						
						AdaptorUtils.adaptCollection(currencyExRateDTOList, currencyExRates, new CurrencyExchnageRateAdaptor());
						
						currencyExRatesResponse.setCurrencyExRates(currencyExRates);
						
						recordAge.setTime(System.currentTimeMillis());
						
					}
					catch (CommonsDataAccessException e) {
						log.error("exchange rate refresh fails", e);
						throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
					} catch (Exception e) {
						log.error("exchange rate refresh fails", e);
						throw new ModuleException("airmaster.technical.error", "airmaster.desc");
					} finally {
						refreshingExchangeRateList = false;
					}
				}
			}
		}
	}
	
	public CurrencyExRatesRS getCachedCurrencyExchangeRatesResponse() throws ModuleException {

		if ((System.currentTimeMillis() - recordAge.getTime()) >= CASH_TIMEOUT) {
			refreshExchangeRateList();
		}

		if (!refreshingExchangeRateList) {
			return currencyExRatesResponse;
		} else {
			synchronized (MasterDataCacheUtil.class) {
				return currencyExRatesResponse;
			}
		}
	}
	
	public CountryListRS getCountryListResponse() throws ModuleException {

		if( countryListResponse.getCountryList() == null || countryListResponse.getCountryList().isEmpty()) {
			try {
				List<CountryDetailsDTO> countryDetailsDTOList = null;
				List<CountryDetails> countryDetailsList = new ArrayList<CountryDetails>();
				
				countryDetailsDTOList = ModuleServiceLocator.getLocationBD().getCountryDetailsList();
				AdaptorUtils.adaptCollection(countryDetailsDTOList, countryDetailsList, new CountryDetailsAdaptor());

				countryListResponse.setCountryList(countryDetailsList);
			}catch (CommonsDataAccessException e) {
				log.error("country list retrieve fails", e);
				throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
			} catch (Exception e) {
				log.error("country list retrieve fails", e);
				throw new ModuleException("airmaster.technical.error", "airmaster.desc");
			}
		} 
		return countryListResponse;
	}
	
	public DropDownListRS getDropDownLists() throws ModuleException{

		if((dropDownLists.getPaxTitels() == null || dropDownLists.getPaxTitels().isEmpty()) 
				|| (dropDownLists.getLanguages() == null || dropDownLists.getLanguages().isEmpty())
				|| (dropDownLists.getPaxTypeWiseTitles() == null || dropDownLists.getPaxTypeWiseTitles().isEmpty())
				|| (dropDownLists.getCountryWiseStates() == null || dropDownLists.getCountryWiseStates().isEmpty())) {
			try{

				List<PaxTitleDTO> paxTitleDTOList = null;
				List<PaxTitle> paxTitleList = new ArrayList<PaxTitle>();
				List<Language> languages = null;
				List<IBELanguage> languageList = new ArrayList<IBELanguage>();
				Map<String, List<PaxTitleDTO>> paxTypeWiseTitleMap = null;
				Map<String, List<State>> countryWiseStatesMap = null;
				paxTitleDTOList = ModuleServiceLocator.getCommoMasterBD().getPaxTitleList();
				languages = (ArrayList<Language>) ModuleServiceLocator.getCommoMasterBD().getLanguages();
				paxTypeWiseTitleMap = ModuleServiceLocator.getCommoMasterBD().getPaxTypeWiseTitleList();
				countryWiseStatesMap = ModuleServiceLocator.getCommoMasterBD().getCountryWiseStates();

				AdaptorUtils.adaptCollection(paxTitleDTOList , paxTitleList , new PaxTitleAdaptor());
				AdaptorUtils.adaptCollection(languages, languageList, new LanguageAdaptor());

				dropDownLists.setPaxTypeWiseTitles((new PaxTypeWiseTitleAdaptor()).adapt(paxTypeWiseTitleMap));
				dropDownLists.setPaxTitels(paxTitleList);
				dropDownLists.setLanguages(languageList);	
				dropDownLists.setCountryWiseStates((new CountryWiseStatesAdaptor()).adapt(countryWiseStatesMap));
				dropDownLists.setSuccess(true);

			}catch (CommonsDataAccessException e) {
				log.error("dropdown list retrieve fails", e);
				throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
			} catch (Exception e) {
				log.error("dropdown list retrieve fails", e);
				throw new ModuleException("airmaster.technical.error", "airmaster.desc");
			}
		}

		return dropDownLists;		
	}

	public AirportMessageRS getAirportMessages(AirportMessagesRQ airportMessageRQ, AppIndicatorEnum appIndicator) throws ModuleException {

		AirportMessageRS airportMessagesRS = new AirportMessageRS();
		List<AirportMessage> airportMessages = new ArrayList<AirportMessage>();
		airportMessagesRS.setAirportMessages(airportMessages);

		List<AirportMessageFilter> airportFlightDetails = new ArrayList<AirportMessageFilter>();
		AirportMessageRQParams airportMessageRQParams = new AirportMessageRQParams(getSalesChannelByAppIndicator(appIndicator),
				airportMessageRQ.getStage(), airportMessageRQ.getLanguageCode());
		AdaptorUtils.adaptCollection(airportMessageRQ.getFlightSegInfo(), airportFlightDetails, new AirportMessageFilterAdaptor(
				airportMessageRQParams));

		List<AirportMessageDisplayDTO> airportMessageDisplayDTOList = ModuleServiceLocator.getCommoMasterBD().airportMessages(
				new AirportMessageFlightDetailsAdaptor().getAirportFlightDetails(airportFlightDetails));
		
		if (!"en".equals(airportMessageRQ.getLanguageCode())) {
			translateAirportMessages(airportMessageDisplayDTOList, airportMessageRQ.getLanguageCode());
		}

		AdaptorUtils.adaptCollection(airportMessageDisplayDTOList, airportMessages, new AirportMessageAdaptor());

		if (airportMessageRQ.getTransactionId() != null && !airportMessageRQ.getTransactionId().isEmpty()) {
			airportMessagesRS.setTransactionId(airportMessageRQ.getTransactionId());
		}

		return airportMessagesRS;
	}

	public String getSalesChannelByAppIndicator(AppIndicatorEnum appIndicator) {
		String salesChannel = CommonConstants.AirportMessageSalesChannel.BOTH.getCode();
		if (appIndicator.getCode() == AppIndicatorEnum.APP_IBE.getCode()) {
			salesChannel = CommonConstants.AirportMessageSalesChannel.IBE.getCode();
		} else if (appIndicator.getCode() == AppIndicatorEnum.APP_XBE.getCode()) {
			salesChannel = CommonConstants.AirportMessageSalesChannel.XBE.getCode();
		}
		return salesChannel;
	}
	
	private static void
			translateAirportMessages(List<AirportMessageDisplayDTO> airportMessageDisplayDTOList, String languageCode)
					throws ModuleException {
		for (AirportMessageDisplayDTO airportMessage : airportMessageDisplayDTOList) {
			String translatedMessageText = loadAirportMessagesTranslation(airportMessage.getAirportMsgId(),
					airportMessage.getI18MessageKey(), languageCode);
			if (translatedMessageText != null && !translatedMessageText.equals("")) {
				String unicodeTrnText = StringUtil.getUnicode(translatedMessageText).replace('^', ',');
				airportMessage.setAirportMessage(unicodeTrnText);
			}
		}
	}

	private static String loadAirportMessagesTranslation(Integer airportMsgId, String i18MessageKey, String slectedLang) throws ModuleException {
		ReservationAuxilliaryBD reservationAuxilliaryBD = ReservationModuleUtils.getReservationAuxilliaryBD();
		String translation = "";
		if (i18MessageKey != null && !i18MessageKey.isEmpty()) {
			translation = reservationAuxilliaryBD.loadI18AirportMessagesTranslation(i18MessageKey, slectedLang);
		} else {
			translation = reservationAuxilliaryBD.loadAirportMessagesTranslation(airportMsgId, slectedLang);
		}
		return translation;
	}
	
	public RelationshipListRS getRelationsResponse() throws ModuleException {

		RelationshipListRS relationshipDropdownListRS = new RelationshipListRS();
		if (relationships.getRelationshipList() == null || relationships.getRelationshipList().isEmpty()) {
			try {
				List<FamilyRelationshipDTO> relationshipDtoList = new ArrayList<FamilyRelationshipDTO>();

				Collection<FamilyRelationship> relationshipList = ModuleServiceLocator.getCommoMasterBD()
						.getFamilyRelationships();
				AdaptorUtils.adaptCollection(relationshipList, relationshipDtoList, new RelationshipsAdaptor());

				relationshipDropdownListRS.setRelationshipList(relationshipDtoList);
			} catch (CommonsDataAccessException e) {
				log.error("country list retrieve fails", e);
				throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
			} catch (Exception e) {
				log.error("country list retrieve fails", e);
				throw new ModuleException("airmaster.technical.error", "airmaster.desc");
			}
		}
		return relationshipDropdownListRS;
	}

}
