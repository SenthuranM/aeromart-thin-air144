package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.ArrayList;
import java.util.List;

public class OndBookingClassInfo {

	private List<String> bookingClasses = new ArrayList<String>();
	private List<String> cabinClasses = new ArrayList<String>();
	private List<String> logicalCabinClasses = new ArrayList<String>();
	private List<String> ondCodes = new ArrayList<>();

	public List<String> getBookingClasses() {
		return bookingClasses;
	}

	public List<String> getCabinClasses() {
		return cabinClasses;
	}

	public List<String> getLogicalCabinClasses() {
		return logicalCabinClasses;
	}

	public void setBookingClasses(List<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public void setCabinClasses(List<String> cabinClasses) {
		this.cabinClasses = cabinClasses;
	}

	public void setLogicalCabinClasses(List<String> logicalCabinClasses) {
		this.logicalCabinClasses = logicalCabinClasses;
	}

	public List<String> getOndCodes() {
		return ondCodes;
	}

	public void setOndCodes(List<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

}
