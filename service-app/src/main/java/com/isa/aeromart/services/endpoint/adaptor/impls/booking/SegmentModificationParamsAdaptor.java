package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.SegmentModificationParams;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;

public class SegmentModificationParamsAdaptor implements Adaptor<LCCClientReservationSegment, SegmentModificationParams> {

	private  boolean promoModifyAllow = true;
	private  boolean promoCancelAllow = true;
	private boolean hasCNFUNSegment;
	private boolean isGroupPnr;
	private boolean singleSegExist;
	private boolean isRegUser;
	private boolean reservationModifiable;
	private List<String> interlineModificationParams;
	private Map<String, Boolean> groundSegRef;

	SegmentModificationParamsAdaptor(boolean isGroupPnr, boolean isRegUser, LCCPromotionInfoTO promoInfo,
			boolean hasCNFUNSegment, List<String> interlineModificationParams, boolean reservationModifiable,
			boolean singleSegExist, Map<String, Boolean> groundSegRef) {
		this.hasCNFUNSegment = hasCNFUNSegment;
		this.isGroupPnr = isGroupPnr;
		this.isRegUser = isRegUser;
		this.interlineModificationParams = interlineModificationParams;
		this.reservationModifiable = reservationModifiable;
		this.singleSegExist = singleSegExist;
		this.groundSegRef = groundSegRef;
		if (promoInfo != null) {
			this.promoCancelAllow = promoInfo.isCancellationAllowed();
			this.promoModifyAllow = promoInfo.isModificationAllowed();
		}
	}

	@Override
	public SegmentModificationParams adapt(LCCClientReservationSegment segment) {
		SegmentModificationParams segmentModificationParams = new SegmentModificationParams();
		String newResSegRPH = ModificationUtils.createResSegRph(segment.getCarrierCode(), segment.getSegmentSeq());
		segmentModificationParams.setReservationSegmentRPH(newResSegRPH);

		boolean modifible = reservationModifiable && segment.isModifible()
				&& new Date().before(segment.getModifyTillBufferDateTime()) && !segment.isFlownSegment();
		boolean modifibleByDate = reservationModifiable && segment.isModifyByDate();
		boolean modifibleByRoute = reservationModifiable && segment.isModifyByRoute();
		boolean cancellable = reservationModifiable && segment.isCancellable();

		if (!modifible && segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
			cancellable = false;
		} else {
			cancellable = (!singleSegExist || isGroundSegment(segment)) && new Date().before(segment.getCancelTillBufferDateTime()) && promoCancelAllow && !segment.isFlownSegment()
					&& segment.isSegmentModifiableAsPerETicketStatus();
		}

		if (isGroupPnr) {
			modifibleByDate = modifibleByDate && interlineModificationParams.contains(ModifcationParamTypes.ALLOW_MODIFY_DATE);
			modifibleByRoute = modifibleByRoute && interlineModificationParams.contains(ModifcationParamTypes.ALLOW_MODIFY_ROUTE);
			cancellable = cancellable && interlineModificationParams.contains(ModifcationParamTypes.CANCEL_ALLOW);
		} else {
			if (isRegUser) {
				modifibleByDate = modifible && modifibleByDate && AppSysParamsUtil.isAllowModifySegmentDateIBE();
				modifibleByRoute = modifible && modifibleByRoute && AppSysParamsUtil.isAllowModifySegmentRouteIBE();
				cancellable = cancellable && AppSysParamsUtil.isAllowIBECancellationServicesReservation();
			} else {
				modifible = modifible && AppSysParamsUtil.isUnregisterdUserCanMofidyReservation();
				modifibleByDate = modifible && modifibleByDate && AppSysParamsUtil.isAllowModifySegmentDateIBE();
				modifibleByRoute = modifible && modifibleByRoute && AppSysParamsUtil.isAllowModifySegmentRouteIBE();
				cancellable = cancellable && AppSysParamsUtil.isUnregisterdUserCanCancelSegment();
			}
		}

		segmentModificationParams.setModifible(modifible && promoModifyAllow && !isGroundSegment(segment));
		segmentModificationParams.setCancellable(cancellable);

		if (modifible) {
			segmentModificationParams.setModifibleByDate(modifibleByDate && promoModifyAllow);
			segmentModificationParams.setModifibleByRoute(modifibleByRoute && promoModifyAllow);
		} else {
			segmentModificationParams.setModifibleByDate(false);
			segmentModificationParams.setModifibleByRoute(false);
		}

		if (hasCNFUNSegment && !segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
				&& !segment.isFlownSegment()) {
			if (segment.isUnSegment()) {
				segmentModificationParams.setCancellable(true);
				segmentModificationParams.setModifible(false);
			} else {
				segmentModificationParams.setCancellable(false);
				segmentModificationParams.setModifible(false);
			}
		}
		return segmentModificationParams;
	}

	private boolean isGroundSegment(LCCClientReservationSegment segment) {
		return groundSegRef.get(segment.getFlightSegmentRefNumber());
	}

}
