package com.isa.aeromart.services.endpoint.session;

import java.io.Serializable;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import com.isa.aeromart.services.endpoint.dto.session.GiftVoucherTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.isa.aeromart.services.endpoint.dto.session.AncillaryTransaction;
import com.isa.aeromart.services.endpoint.dto.session.BaseTransaction;
import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.RequoteTransaction;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;

@Component
public class TransactionFactory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private SessionDataStore sessionDataStore;

	private TransactionPoolManager trnxPoolManager;
	
	@Autowired 
	private HttpSession httpSession;

	public void setSessionDataStore(SessionDataStore sessionDataStore) {
		this.sessionDataStore = sessionDataStore;
	}

	public <T> T getTransactionStore(String transactionId) {
		if (transactionId == null) {
			throw new ValidationException(ValidationException.Code.NULL_TRANSACTION_ID);
		}
		if (!sessionDataStore.containSessionKey(transactionId)) {
			ValidationException ex = new ValidationException(ValidationException.Code.INVALID_TRANSACTION_ID);
			ex.setErrorType(ErrorType.TRANSACTION_ID_ERROR);
			throw ex;
		}
		return sessionDataStore.getData(transactionId);
	}

	public <T> T getSessionStore(String sessionKey) {
		if (!sessionDataStore.containSessionKey(sessionKey)) {
			Object data = null;
			if (CustomerSession.SESSION_KEY.equals(sessionKey)) {
				data = new CustomerSession();
			}
			if (data != null) {
				setSessionStore(sessionKey, data);
			}
		}
		return sessionDataStore.getData(sessionKey);
	}

	private <T> void setSessionStore(String sessionKey, T sessionObject) {
		sessionDataStore.putData(sessionKey, sessionObject);
	}

	public void terminateTransaction(String transactionId) {
		if (sessionDataStore.containSessionKey(transactionId)) {
			BaseTransaction base = getTransactionStore(transactionId);
			base.cleanup();
			sessionDataStore.remove(transactionId);
			getTransactionPoolManager().removeTransaction(transactionId);
		}
	}

	private String generateUniqueId() {
		return UUID.randomUUID().toString();
	}

	private boolean isTransactionExists(String transactionId) {
		if (transactionId != null && sessionDataStore.containSessionKey(transactionId)) {
			return true;
		}
		return false;
	}

	public String initTransaction(String sessionKey, String transactionId) {
		if (isTransactionExists(transactionId)) {
			return transactionId;
		}
		if (transactionId == null) {
			transactionId = generateUniqueId();
		}
		Object data = null;
		if (BookingTransaction.SESSION_KEY.equals(sessionKey)) {
			data = new BookingTransaction();
		} else if (RequoteTransaction.SESSION_KEY.equals(sessionKey)) {
			data = new RequoteTransaction();
		} else if (AncillaryTransaction.SESSION_KEY.equals(sessionKey)) {
			data = new AncillaryTransaction();
		} else if (GiftVoucherTransaction.SESSION_KEY.equals(sessionKey)) {
			data = new GiftVoucherTransaction();
		}

		if (data != null) {
			sessionDataStore.putData(transactionId, data);
			getTransactionPoolManager().addTransaction(transactionId);
		}
		return transactionId;
	}

	private TransactionPoolManager getTransactionPoolManager() {
		if (trnxPoolManager == null) {
			trnxPoolManager = new TransactionPoolManager(this);
		}
		return trnxPoolManager;
	}

	public HttpSession getHttpSession() {
		return httpSession;
	}

}
