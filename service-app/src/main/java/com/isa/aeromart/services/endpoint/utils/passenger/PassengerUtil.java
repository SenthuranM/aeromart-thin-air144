package com.isa.aeromart.services.endpoint.utils.passenger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.masterdata.ContactConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRS;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException.Code;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class PassengerUtil {

	private BookingSessionStore bookingSessionStore;

	public void validateContactDetails(BookingRQ BookingRQ, BookingSessionStore bookingSessionStore, TrackInfoDTO trackInfo)
			throws ValidationException {

		this.bookingSessionStore = bookingSessionStore;
		ContactConfigCollectionRQ contactConfigCollectionRQ = getContactConfigReqList();
		MasterDataService masterDataService = new MasterDataServiceAdaptor();

		PaxContactDetailsRS paxContactDetail = masterDataService.unifiedContactDetails(contactConfigCollectionRQ,
				bookingSessionStore.getInvolvingCarrierCodes(), trackInfo);

		for (Field field : BookingRQ.getReservationContact().getClass().getDeclaredFields()) {

			if (paxContactDetail.getContactDetails().containsKey(field.getName())) {

				if (paxContactDetail.getContactDetails().get(field.getName()).isMandatory()) {
					try {
						if (BeanUtils.getProperty(BookingRQ.getReservationContact(), field.getName()).isEmpty()) {
							throw new ValidationException(Code.CONTACT_DETAILS_VALIDATION_ERROR , new Throwable(" in " + field.getName()));
						} else {
							continue;
						}
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

		}

	}

	public void validatePaxInfo(BookingRQ BookingRQ, BookingSessionStore bookingSessionStore, TrackInfoDTO trackInfo)
			throws ValidationException {

		this.bookingSessionStore = bookingSessionStore;
		PaxConfigCollectionRQ paxConfigCollectionRQ = getPaxConfigReqList();

		MasterDataService masterDataService = new MasterDataServiceAdaptor();

		PaxDetailsConfigRS paxDetailsConfig = masterDataService.unifiedPaxDetails(paxConfigCollectionRQ, trackInfo,
				bookingSessionStore.getPreferenceInfoForBooking().getSelectedSystem(),
				bookingSessionStore.getInvolvingCarrierCodes(), bookingSessionStore.getSegments());

		for (Passenger pax : BookingRQ.getPaxInfo()) {
			for (Field field : pax.getClass().getDeclaredFields()) {
				if (paxDetailsConfig.getPaxDetailsConfig().containsKey(field.getName())) {
					try {
						if (pax.getPaxType().equals(PaxTypeTO.ADULT)) {
							if (paxDetailsConfig.getPaxDetailsConfig().get(field.getName()).getMandatory().isAdult()) {
								if (BeanUtils.getProperty(pax, field.getName()).isEmpty()) {
									throw new ValidationException(Code.PAXINFO_VALIDATION_ERROR, new Throwable(" in "+field.getName() + " for pax " + pax.getPaxSequence()));
								} else {
									continue;
								}
							}
						} else if (pax.getPaxType().equals(PaxTypeTO.CHILD)) {
							if (paxDetailsConfig.getPaxDetailsConfig().get(field.getName()).getMandatory().isChild()) {
								if (BeanUtils.getProperty(pax, field.getName()).isEmpty()) {
									throw new ValidationException(Code.PAXINFO_VALIDATION_ERROR, new Throwable(" in "+field.getName() + " for pax " + pax.getPaxSequence()));
								} else {
									continue;
								}
							}
						} else if (pax.getPaxType().equals(PaxTypeTO.INFANT)) {
							if (paxDetailsConfig.getPaxDetailsConfig().get(field.getName()).getMandatory().isInfant()) {
								if (BeanUtils.getProperty(pax, field.getName()).isEmpty()) {
									throw new ValidationException(Code.PAXINFO_VALIDATION_ERROR, new Throwable(" in "+field.getName() + " for pax " + pax.getPaxSequence()));
								} else {
									continue;
								}
							}
						}
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

	}

	private ContactConfigCollectionRQ getContactConfigReqList() {

		ContactConfigCollectionRQ contactConfigCollectionRQ = new ContactConfigCollectionRQ();
		contactConfigCollectionRQ.setConfigDetailsRQs(new ArrayList<>());

		List<SessionFlightSegment> segments = bookingSessionStore.getSegments();

		for (SessionFlightSegment segment : segments) {
			PaxContactDetailsRQ contactConfigRQ = new PaxContactDetailsRQ();
			String[] ondCodes = segment.getSegmentCode().split("/");
			contactConfigRQ.setOrigin(ondCodes[0]);
			contactConfigRQ.setDestination(ondCodes[ondCodes.length - 1]);
			contactConfigRQ.setCarriers(segment.getOperatingAirline());
			contactConfigCollectionRQ.getConfigDetailsRQs().add(contactConfigRQ);
		}

		return contactConfigCollectionRQ;
	}

	private PaxConfigCollectionRQ getPaxConfigReqList() {

		PaxConfigCollectionRQ paxConfigCollectionRQ = new PaxConfigCollectionRQ();
		paxConfigCollectionRQ.setConfigDetailsRQs(new ArrayList<>());

		List<SessionFlightSegment> segments = bookingSessionStore.getSegments();

		for (SessionFlightSegment segment : segments) {
			PaxDetailsConfigRQ paxConfigRQ = new PaxDetailsConfigRQ();
			String [] ondCodes = segment.getSegmentCode().split("/");
			paxConfigRQ.setOrigin(ondCodes[0]);
			paxConfigRQ.setDestination(ondCodes[ondCodes.length - 1]);
			paxConfigRQ.setCarriers(segment.getOperatingAirline());
			paxConfigCollectionRQ.getConfigDetailsRQs().add(paxConfigRQ);
		}

		return paxConfigCollectionRQ;
	}

}
