package com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.inflight;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;

public class InFlightSsrItem extends AncillaryItem {

	private Integer availableQty;
	
	private String serviceQuantity;
	
	private String ssrCode;
	
	private String ssrName;

	public Integer getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}

	public String getServiceQuantity() {
		return serviceQuantity;
	}

	public void setServiceQuantity(String serviceQuantity) {
		this.serviceQuantity = serviceQuantity;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrName() {
		return ssrName;
	}

	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}
	
}
