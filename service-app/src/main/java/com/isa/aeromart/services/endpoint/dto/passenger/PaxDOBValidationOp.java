package com.isa.aeromart.services.endpoint.dto.passenger;

public class PaxDOBValidationOp {

	private boolean isValidPaxType;

	private String firstName;

	private String paxType;

	public boolean getIsValidPaxType() {
		return isValidPaxType;
	}

	public void setIsValidPaxType(boolean isValidPaxType) {
		this.isValidPaxType = isValidPaxType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

}
