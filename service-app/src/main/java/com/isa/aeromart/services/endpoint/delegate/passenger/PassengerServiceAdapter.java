package com.isa.aeromart.services.endpoint.delegate.passenger;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.dto.passenger.DuplicateCheckRS;
import com.isa.aeromart.services.endpoint.dto.passenger.LMSPassenger;
import com.isa.aeromart.services.endpoint.dto.passenger.PaxValidationDetails;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRQ;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRS;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.LoyaltyNameValidator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;

@Service("passengerService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PassengerServiceAdapter implements PassengerService {

	private static Log log = LogFactory.getLog(PassengerServiceAdapter.class);

	@Override
	public boolean checkDuplicates(TrackInfoDTO trackInfoDTO, DuplicateValidatorAssembler duplicateValidatorAssembler) {
		DuplicateCheckRS duplicateCheckRS = new DuplicateCheckRS();

		boolean hasDuplicates = false;

		try {
			hasDuplicates = ModuleServiceLocator.getAirproxyReservationBD().checkForDuplicates(duplicateValidatorAssembler, null,
					trackInfoDTO);
		} catch (ModuleException e) {
			log.error("Error in checking duplicates ==> DuplicateCheckController.checkDuplicates(DuplicateCheckReq)");
			duplicateCheckRS.setSuccess(false);
		}

		if (hasDuplicates) {
			throw new ValidationException(ValidationException.Code.DUPLICATE_PAX_NAMES_FOUND);
		}

		return hasDuplicates;
	}

	@Override
	public ValidatePaxFFIDsRS validateLMSPax(ValidatePaxFFIDsRQ validatePaxFFIDsRQ) throws ModuleException {
		ValidatePaxFFIDsRS validatePaxFFIDsRS = new ValidatePaxFFIDsRS();
		List<PaxValidationDetails> paxValidationDetailsList = new ArrayList<>();

		List<LMSPassenger> lmsPassengers = validatePaxFFIDsRQ.getLmsPaxDetails();
		validatePaxFFIDsRS.setSuccess(true);
		LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
		LmsMemberServiceBD lmsMemberServiceBD = ModuleServiceLocator.getLmsMemberServiceBD();

		for (LMSPassenger lmsPassenger : lmsPassengers) {
			PaxValidationDetails paxValidationDetails = new PaxValidationDetails();
			if (AppSysParamsUtil.isLMSEnabled() && lmsPassenger.getFfid() != null && lmsPassenger.getFfid() != "") {
				validateLMSDetails(loyaltyManagementBD, lmsMemberServiceBD, lmsPassenger, paxValidationDetails);
			} else {
				paxValidationDetails.setValidFFID(true);
				paxValidationDetails.setValidName(true);
				paxValidationDetails.setValidPaxType(true);
				paxValidationDetails.setEmailConfirmed(true);
			}
			paxValidationDetails.setPaxSequence(lmsPassenger.getPassengerSequence());
			paxValidationDetailsList.add(paxValidationDetails);
		}

		validatePaxFFIDsRS.setPaxValidationList(paxValidationDetailsList);
		return validatePaxFFIDsRS;
	}

	private void validateLMSDetails(LoyaltyManagementBD loyaltyManagementBD, LmsMemberServiceBD lmsMemberServiceBD,
			LMSPassenger lmsPassenger, PaxValidationDetails paxValidationDetails) throws ModuleException {

		if (LmsCommonUtil.availableForCarrier(lmsPassenger.getFfid(), lmsMemberServiceBD)) {

			paxValidationDetails.setValidFFID(true);

			LmsMember lmsMember = lmsMemberServiceBD.getLmsMember(lmsPassenger.getFfid());
			
			boolean isEmailConfirmed = false;

			isEmailConfirmed = (lmsMember.getEmailConfirmed() == 'N') ? false : true;
			paxValidationDetails.setEmailConfirmed(isEmailConfirmed);

			LocalDate dateOfBirth = lmsMember.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			
			paxValidationDetails.setValidName(LoyaltyNameValidator.compareNames(lmsPassenger.getFirstName(), lmsPassenger.getLastName(),
							lmsMember.getFirstName(), lmsMember.getLastName()));
			paxValidationDetails.setValidPaxType(lmsPassenger.getPaxType().equals(getPaxType(dateOfBirth)));

		} else {
			paxValidationDetails.setValidFFID(false);
		}
	}

	private String getPaxType(LocalDate dateOfBirth) {
		LocalDate now = LocalDate.now();
		long numberOfYears = ChronoUnit.YEARS.between(dateOfBirth, now);
		long numberOfDays = ChronoUnit.DAYS.between(dateOfBirth, now);
		Collection<PaxTypeTO> paxTypes = CommonsServices.getGlobalConfig().getPaxTypes();
		for (PaxTypeTO paxType : paxTypes) {
			if (numberOfYears < paxType.getCutOffAgeInYears() && numberOfDays > getDaysLowerCutover(paxType)) {
				return paxType.getPaxTypeCode();
			}
		}
		return "INVALID";
	}

	private int getDaysLowerCutover(PaxTypeTO paxtype) {
		if (paxtype.getAgeLowerBoundaryInDays() > 0) {
			return paxtype.getAgeLowerBoundaryInDays();
		} else {
			return paxtype.getAgeLowerBoundaryInMonths() * 356 / 12;
		}
	}

}
