package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

import java.util.List;

public class SeatsPassengerTypes extends BasicRuleValidation {

    private List<String> paxTypes;

    public SeatsPassengerTypes() {
    	setApplicability(ApplicabilityCode.SEAT_WISE);
        setRuleCode(RuleCode.RESTRICTED_PAX_TYPES_FOR_SEAT);
    }

    public List<String> getPaxTypes() {
        return paxTypes;
    }

    public void setPaxTypes(List<String> paxTypes) {
        this.paxTypes = paxTypes;
    }
}
