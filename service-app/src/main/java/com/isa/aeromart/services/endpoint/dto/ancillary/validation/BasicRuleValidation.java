package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

import com.isa.aeromart.services.endpoint.dto.ancillary.Rule;

public class BasicRuleValidation extends Rule {

	public interface RuleCode {
		String MULTIPLE_SELECTIONS = "common-multiple-selections";
		//String MULTIPLE_SELECTIONS_DESCROIPTION = "common-multiple-selections"
		String PAX_TYPE_COUNT_FOR_ROW_SEATS = "pax-type-count-for-row-seat";
		String PAX_TYPE_COUNT_FOR_ROW_SEATS_DESCRIPTION = "ibe.ancillary.validation.seat.paxTypeCountForRow";
		String RESTRICTED_PAX_TYPES_FOR_SEAT = "seat-pax-types-restricted";
		String RESTRICTED_PAX_TYPES_FOR_SEAT_DESCRIPTION = "ibe.ancillary.validation.seat.restrictedPaxType";
		String INSURANCE_PLANS_COMBINATIONS = "insurance-plans-combinations";
		String INSURANCE_PLANS_COMBINATIONS_DESCRIPTION = "ibe.ancillary.validation.insurance";
		String EDIT_ANCILLARY= "ibe.ancillary.rule.edit";
		String CALENDAR_DATE_RESTRICT= "ibe.ancillary.rule.calander.date.restrict";
	}
	
	public interface ApplicabilityCode {
		String SEAT_ROW_WISE = "SEAT_ROW";
		String SEAT_WISE = "SEAT";
		String CABIN_CLASS_WISE = "CABIN_CLASS";
	}
	

	private String ruleMessage;
	
	private String applicability;
	

	public String getRuleMessage() {
		return ruleMessage;
	}

	public void setRuleMessage(String ruleMessage) {
		this.ruleMessage = ruleMessage;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

}
