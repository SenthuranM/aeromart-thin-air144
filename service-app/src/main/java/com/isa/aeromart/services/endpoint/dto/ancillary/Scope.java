package com.isa.aeromart.services.endpoint.dto.ancillary;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "scopeType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Scope.class, name = Scope.ScopeType.ALL_SEGMENTS),
        @JsonSubTypes.Type(value = OndScope.class, name = Scope.ScopeType.OND),
		@JsonSubTypes.Type(value = SegmentScope.class, name = Scope.ScopeType.SEGMENT),
		@JsonSubTypes.Type(value = AirportScope.class, name = Scope.ScopeType.AIRPORT)})
public class Scope {
	
	public interface ScopeType {
		String ALL_PASSENGERS = "ALL_PASSENGERS";
		String ALL_SEGMENTS = "ALL_SEGMENTS";
		String OND = "OND";
		String SEGMENT = "SEGMENT";
		String AIRPORT = "AIRPORT";
	}
	
	private String scopeType;

	public String getScopeType() {
		return scopeType;
	}

	public void setScopeType(String scopeType) {
		this.scopeType = scopeType;
	}
}