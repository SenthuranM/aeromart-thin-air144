package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.LoggedInCustomerDetails;
import com.isa.thinair.aircustomer.api.model.Customer;

import java.math.BigInteger;
import java.security.*;



public class CustomerDetailsAdaptor implements Adaptor<Customer , LoggedInCustomerDetails >{

	@Override
	public LoggedInCustomerDetails adapt(Customer customerModel) {
		
		PhoneNumberResAdaptor phoneNumberAdaptor = new PhoneNumberResAdaptor();
		LoggedInCustomerDetails loggedInCustomerDetails = new LoggedInCustomerDetails();
		
		loggedInCustomerDetails.setCustomerID(customerModel.getCustomerId());
		loggedInCustomerDetails.setTitle(customerModel.getTitle());
		loggedInCustomerDetails.setFirstName(customerModel.getFirstName());
		loggedInCustomerDetails.setLastName(customerModel.getLastName());
		loggedInCustomerDetails.setEmailId(customerModel.getEmailId());
		loggedInCustomerDetails.setCountry(customerModel.getCountryCode());
		loggedInCustomerDetails.setNationality(String.valueOf(customerModel.getNationalityCode()));
		if (customerModel.getLMSMemberDetails() != null) {
			loggedInCustomerDetails.setDateOfBirth(customerModel.getLMSMemberDetails().getDateOfBirth());
		}
		loggedInCustomerDetails.setMobile(phoneNumberAdaptor.adapt(customerModel.getMobile()));
		loggedInCustomerDetails.setSendPromoEmail(customerModel.getSendPromoEmail());
	
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(customerModel.getEmailId().getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);

			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
		loggedInCustomerDetails.setMd5EncryptedEmailId(hashtext);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return loggedInCustomerDetails;
	}
	
}
