package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.SpecificFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

public class OriginDestinationInfo extends BasicOndInfo implements Comparable<OriginDestinationInfo> {
	private List<StopOver> stopOvers;

	private BasePreferences preferences;

	private List<SpecificFlight> specificFlights;

	public List<StopOver> getStopOvers() {
		return stopOvers;
	}

	public void setStopOvers(List<StopOver> stopOvers) {
		this.stopOvers = stopOvers;
	}

	public BasePreferences getPreferences() {
		if (preferences == null) {
			preferences = new BasePreferences();
		}
		return preferences;
	}

	public void setPreferences(BasePreferences preferences) {
		this.preferences = preferences;
	}

	public List<SpecificFlight> getSpecificFlights() {
		return specificFlights;
	}

	public void setSpecificFlights(List<SpecificFlight> specificFlights) {
		this.specificFlights = specificFlights;
	}

	@Override
	public int compareTo(OriginDestinationInfo o) {
		if (o.getDepartureDateTime() != null) {
			if (this.getDepartureDateTime().compareTo(o.getDepartureDateTime()) == 0) {
				if (this.getDestination().equals(o.getOrigin())) {
					return -1;
				} else if (this.getOrigin().equals(o.getDestination())) {
					return 1;
				}
				return 0;
			}
			return this.getDepartureDateTime().compareTo(o.getDepartureDateTime());
		}
		return 0;
	}

	public static boolean isReturnSearch(List<OriginDestinationInfo> ondInfo) {
		return !CollectionUtils.isEmpty(ondInfo) && ondInfo.size() == OndSequence.RETURN_OND_SIZE &&
				StringUtils.equals(ondInfo.get(OndSequence.OUT_BOUND).getOrigin(),
						ondInfo.get(OndSequence.IN_BOUND).getDestination()) && StringUtils
				.equals(ondInfo.get(OndSequence.OUT_BOUND).getDestination(), ondInfo.get(OndSequence.IN_BOUND).getOrigin());
	}

}
