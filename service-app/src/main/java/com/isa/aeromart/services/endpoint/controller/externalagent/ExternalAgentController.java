package com.isa.aeromart.services.endpoint.controller.externalagent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.ExternalAgentConstants;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.dto.common.BaseRS;
import com.isa.aeromart.services.endpoint.dto.externalagent.AgentInfoRS;
import com.isa.aeromart.services.endpoint.dto.externalagent.ExternalAgentDTO;
import com.isa.aeromart.services.endpoint.dto.externalagent.ExternalAgentLogOutRS;
import com.isa.aeromart.services.endpoint.dto.externalagent.ExternalAgentLoginRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.HashGenRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.TokenRS;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.externalagent.ExternalAgentUtil;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

@Controller
@RequestMapping("externalagent")
public class ExternalAgentController extends StatefulController {

	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseRS loginRedirect(@RequestParam(name = "merchantID", required = false) String merchantId,
			@RequestParam(name = "timeStamp", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date timestamp,
			@RequestParam(name = "secureHash", required = false) String secureHash,
			@RequestParam(name = "returnUrl", required = false) String returnUrl) {

		BaseRS response = new BaseRS();
		//TEMP FIX
		if(secureHash != null){
			secureHash = secureHash.replaceAll(" ", "+");
		}
		
		if (validateLoginRedirection(timestamp, secureHash, merchantId)) {
			response.setSuccess(true);
		} else {
			response.setSuccess(false);
		}
		

		return response;
	}

	/**
	 * This method is used to create login response.
	 * 
	 * @param externalAgentLoginReq
	 * @return externalAgentLoginRS
	 */
	@ResponseBody
	@RequestMapping(value = "/auth/login", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TokenRS externalAgentLogin(@RequestBody ExternalAgentLoginRQ externalAgentLoginReq) {

		User externalAgentModel = null;

		ArrayList<String> messages = new ArrayList<>();

		TokenRS externalAgentLoginRS = new TokenRS();

		try {
			externalAgentModel = ModuleServiceLocator.getSecurityBD().authenticate(externalAgentLoginReq.getUsername(),
					externalAgentLoginReq.getPassword());

			if (externalAgentModel != null && externalAgentModel.getStatus().equals("ACT")) {
				String agentCode = externalAgentModel.getAgentCode();
				AgentToken agentToken = generateToken(agentCode, externalAgentLoginReq.getUsername(),
						externalAgentLoginReq.getMerchantId());
				saveAgentToken(agentToken);
				messages.add("login success");
				externalAgentLoginRS.setToken(agentToken.getToken());
				externalAgentLoginRS.setExpiryTimeStamp(agentToken.getTokenExpiryTimeStamp());
				setResponseSuccess(externalAgentLoginRS, messages);
				return externalAgentLoginRS;
			} else {
				messages.add("login failed.");
				setResponseFail(externalAgentLoginRS, messages);
				return externalAgentLoginRS;
			}

		} catch (Exception e) {
			messages.add("Login failed.");
			setResponseFail(externalAgentLoginRS, messages);
			return externalAgentLoginRS;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/information", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public AgentInfoRS getAgentDetails(@RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken)
			throws ModuleException {
		AgentInfoRS response = new AgentInfoRS();
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgentDetailsByTokenId(authToken);
		if (agent != null) {
			ExternalAgentDTO externalAgentDTO = new ExternalAgentDTO(agent);
			response.setAgent(externalAgentDTO);
			response.setSuccess(true);
		} else {
			response.setSuccess(false);
		}
		return response;
	}

	/**
	 * This method is used to create refresh token response.
	 * 
	 * @param externalAgentRefreshTokenRQ
	 * @return externalAgentRefreshTokenRS
	 */
	@ResponseBody
	@RequestMapping(value = "/token/refresh", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TokenRS refreshToken(@RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken) {
		ArrayList<String> messages = new ArrayList<>();

		AgentToken newAgentToken = null;
		AgentToken oldAgentToken = null;

		TokenRS externalAgentRefreshTokenRS = new TokenRS();

		try {
			oldAgentToken = getAgentToken(authToken);

			if (oldAgentToken != null && !oldAgentToken.isExpiredToken()) {
				newAgentToken = generateToken(oldAgentToken.getAgentCode(), oldAgentToken.getUserName(),
						getMerchantIdFromToken(authToken));
				saveAgentToken(newAgentToken);
				expireAgentToken(oldAgentToken);
				externalAgentRefreshTokenRS.setToken(newAgentToken.getToken());
				externalAgentRefreshTokenRS.setExpiryTimeStamp(newAgentToken.getTokenExpiryTimeStamp());
				messages.add("refresh success");
				setResponseSuccess(externalAgentRefreshTokenRS, messages);
				return externalAgentRefreshTokenRS;
			} else {
				messages.add("refresh failed.");
				setResponseFail(externalAgentRefreshTokenRS, messages);
				return externalAgentRefreshTokenRS;
			}
		} catch (Exception e) {
			messages.add("refresh failed.");
			setResponseFail(externalAgentRefreshTokenRS, messages);
			return externalAgentRefreshTokenRS;
		}

	}

	/**
	 * This method is used to create validate token response.
	 * 
	 * @param externalAgentValidateTokenRQ
	 * @return externalAgentLogOutRS
	 */
	@ResponseBody
	@RequestMapping(value = "/token/validate", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public TokenRS validateToken(@RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken) {

		TokenRS externalAgentValidateTokenRS = new TokenRS();
		ArrayList<String> messages = new ArrayList<>();

		try {
			AgentToken tokenToBeValidate = getAgentToken(authToken);

			if (tokenToBeValidate != null && !tokenToBeValidate.isExpiredToken()) {
				externalAgentValidateTokenRS.setToken(authToken);
				externalAgentValidateTokenRS.setExpiryTimeStamp(tokenToBeValidate.getTokenExpiryTimeStamp());
				externalAgentValidateTokenRS.setMessages(messages);
				setResponseSuccess(externalAgentValidateTokenRS, messages);
				return externalAgentValidateTokenRS;
			} else {
				messages.add("validate failed.");
				setResponseFail(externalAgentValidateTokenRS, messages);
				return externalAgentValidateTokenRS;
			}
		} catch (Exception e) {
			messages.add("validate failed.");
			setResponseFail(externalAgentValidateTokenRS, messages);
			return externalAgentValidateTokenRS;
		}
	}

	/**
	 * This method is used to create logout response.
	 * 
	 * @param externalAgentLogOutRQ
	 * @return externalAgentLogOutRS
	 */
	@ResponseBody
	@RequestMapping(value = "/logout", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ExternalAgentLogOutRS clearToken(@RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken) {

		ExternalAgentLogOutRS externalAgentLogOutRS = new ExternalAgentLogOutRS();
		ArrayList<String> messages = new ArrayList<>();

		try {
			AgentToken token = getAgentToken(authToken);
			if (token != null) {
				expireAgentToken(token);
				setResponseSuccess(externalAgentLogOutRS, messages);
				return externalAgentLogOutRS;
			} else {
				messages.add("logout failed.");
				setResponseFail(externalAgentLogOutRS, messages);
				return externalAgentLogOutRS;
			}
		} catch (Exception e) {
			messages.add("logout failed.");
			setResponseFail(externalAgentLogOutRS, messages);
			return externalAgentLogOutRS;
		}
	}

	/**
	 * This method is used to set basic values for Response when it is passed
	 * 
	 * @param baseResonse
	 *            , messages
	 */
	private void setResponseSuccess(BaseRS resonse, ArrayList<String> messages) {
		resonse.setSuccess(true);
		resonse.setMessages(messages);
		resonse.setErrors(null);
		resonse.setWarnings(null);
	}

	/**
	 * This method is used to set basic values for Response when it is failed
	 * 
	 * @param baseResonse
	 *            , messages
	 */
	private void setResponseFail(BaseRS resonse, ArrayList<String> messages) {
		resonse.setSuccess(false);
		resonse.setMessages(messages);
		resonse.setErrors(null);
		resonse.setWarnings(null);
	}

	/**
	 * This method is used to generate agentToken.
	 * 
	 * @param agentCode
	 *            , userName
	 * @return AgentToken
	 */
	private AgentToken generateToken(String agentCode, String userName , String merchantId) {
		UUID uuid;
		String strUuid;
		String token;

		uuid = UUID.randomUUID();
		strUuid = uuid.toString().substring(0, 16);
		token = merchantId + "-" + strUuid + agentCode;

		AgentToken agentToken = new AgentToken();
		agentToken.setAgentCode(agentCode);
		agentToken.setExpired(ExternalAgentConstants.NO);
		agentToken.setToken(token);
		agentToken.setTokenIssueTimeStamp(CalendarUtil.getCurrentZuluDateTime());
		agentToken.setTokenExpiryTimeStamp(calculateTokenExpiryTimeStamp(AppSysParamsUtil.getAeroMartPayTokenTimeOut()));
		agentToken.setUserName(userName);

		return agentToken;
	}
	
	private String getMerchantIdFromToken(String token){
		String merchantId="";
		if(token != null){
			merchantId = token.split("-")[0];
			if(merchantId==null || merchantId.isEmpty()){
				merchantId = ExternalAgentConstants.MERCHANT_ID;
			}
		}
		return merchantId;
	}

	/**
	 * This method is used to expire token. It gets particular token object and set its expired value as "Y". finally
	 * update that object in DB.
	 * 
	 * @param agentToken
	 */
	private void expireAgentToken(AgentToken agentToken) throws ModuleException {
		// agentToken.setExpired(ExternalAgentConstants.YES);
		// agentToken.setVersion(agentToken.getVersion() + new Long(1));
		// saveAgentToken(agentToken);
		removeAgentToken(agentToken.getTokenId());
	}

	/**
	 * This method is calling a service call which is used to get Token from DB
	 * 
	 * @param strToken
	 * @return AgentToken
	 */
	private AgentToken getAgentToken(String strToken) throws ModuleException {
		return ModuleServiceLocator.getTravelAgentBD().getAgentToken(strToken);
	}

	/**
	 * This method is calling a service call which is used to save Token in DB
	 * 
	 * @param agentToken
	 */
	private void saveAgentToken(AgentToken agentToken) throws ModuleException {
		ModuleServiceLocator.getTravelAgentBD().setAgentToken(agentToken);
	}

	/**
	 * This method is calling a service call which is used to delete Token in DB
	 * 
	 * @param agentToken
	 */
	private void removeAgentToken(int agentTokenId) throws ModuleException {
		ModuleServiceLocator.getTravelAgentBD().removeAgentToken(agentTokenId);
	}

	/**
	 * This is used to calculate expire time for the given token and convert that string type time to Java.Util.Date
	 * 
	 * @param stringTime
	 * @return Date
	 */
	private Date calculateTokenExpiryTimeStamp(String strExpiryTimeLimit) {
		Date expiryTimeStampDt = new Date();
		Long expiryTimeStampLong = new Long(strExpiryTimeLimit);
		expiryTimeStampDt.setTime(CalendarUtil.getCurrentZuluDateTime().getTime() + expiryTimeStampLong);
		return expiryTimeStampDt;
	}

	/**
	 * This is used to validate time stamp and secureHash value, if success returns to login page otherwise return to a
	 * error page
	 * 
	 * @param timestamp
	 *            , secureHash
	 * @return boolean
	 */
	private boolean validateLoginRedirection(Date timestamp, String secureHash, String merchantId) {
		boolean valid = false;

		if (ExternalAgentUtil.validateTimeStamp(timestamp) && ExternalAgentUtil.validateHash(timestamp, secureHash, merchantId)) {
			valid = true;
		}

		return valid;
	}

	@ResponseBody
	@RequestMapping(value = "/generateHash", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseRS generateHash(@RequestBody HashGenRQ externalAgentRefreshTokenRQ) {

		BaseRS externalAgentRefreshTokenRS = new BaseRS();

		String hashvalue = ExternalAgentUtil.createHashvalue(externalAgentRefreshTokenRQ);
		externalAgentRefreshTokenRS.setMessages(Arrays.asList(hashvalue));

		return externalAgentRefreshTokenRS;
	}

	@ResponseBody
	@RequestMapping(value = "/generateHashTS", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseRS generateHashTS(@RequestBody HashGenRQ externalAgentRefreshTokenRQ) {

		BaseRS externalAgentRefreshTokenRS = new BaseRS();

		String hashvalue = ExternalAgentUtil.createHashvalueForTimestamp(externalAgentRefreshTokenRQ);
		externalAgentRefreshTokenRS.setMessages(Arrays.asList(hashvalue));

		return externalAgentRefreshTokenRS;
	}

}
