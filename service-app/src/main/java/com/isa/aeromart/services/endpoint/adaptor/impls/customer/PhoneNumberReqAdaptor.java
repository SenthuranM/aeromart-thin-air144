package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;


public class PhoneNumberReqAdaptor implements Adaptor<PhoneNumber , String>{

	@Override
	public String adapt(PhoneNumber phoneNumber) {
		
		String phoneNo = "";
		
		if(phoneNumber == null){
			return "--";
		}
		phoneNo += phoneNumber.getCountryCode();
		phoneNo += "-" + phoneNumber.getAreaCode();
		phoneNo += "-" + phoneNumber.getNumber();
		
		return phoneNo;
	}

}
