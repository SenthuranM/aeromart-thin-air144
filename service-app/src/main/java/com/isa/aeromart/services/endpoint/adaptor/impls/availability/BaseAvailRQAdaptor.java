package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import static com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils.adaptCollection;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.OriginDestinationInfo;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException.Code;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class BaseAvailRQAdaptor implements Adaptor<AvailabilitySearchRQ, BaseAvailRQ> {

	private boolean findMininum;
	private boolean fareCalendarSearch;
	private RPHGenerator rphGenerator;
	private TrackInfoDTO trackInfo;
	private boolean preserveOndOrder;
	private boolean requoteFlightSerach;
	private Log log = LogFactory.getLog(BaseAvailRQAdaptor.class);

	public BaseAvailRQAdaptor(boolean findMininum, boolean fareCalendarSearch, boolean requoteFlightSearch, TrackInfoDTO trackInfo) {
		this.findMininum = findMininum;
		this.fareCalendarSearch = fareCalendarSearch;
		this.trackInfo = trackInfo;
		this.requoteFlightSerach = requoteFlightSearch;
	}

	public BaseAvailRQAdaptor(boolean findMininum, boolean fareCalendarSearch, RPHGenerator rphGenerator, TrackInfoDTO trackInfo) {
		this(findMininum, fareCalendarSearch, false,trackInfo);
		this.rphGenerator = rphGenerator;
		this.trackInfo = trackInfo;
	}

	private BaseAvailRQAdaptor(boolean findMininum, boolean fareCalendarSearch, boolean preserveOndOrder,
			RPHGenerator rphGenerator, TrackInfoDTO trackInfo) {
		this(findMininum, fareCalendarSearch, rphGenerator, trackInfo);
		this.preserveOndOrder = preserveOndOrder;
	}

	public static BaseAvailRQAdaptor getOrderPreservedMinimumCalendarAdaptor(RPHGenerator rphGenerator, TrackInfoDTO trackInfo) {
		return new BaseAvailRQAdaptor(true, true, true, rphGenerator, trackInfo);
	}

	@Override
	public BaseAvailRQ adapt(AvailabilitySearchRQ source) {

		BaseAvailRQ target = new BaseAvailRQ();

		// origin destination info
		List<OriginDestinationInformationTO> ondInfoTOList = new ArrayList<OriginDestinationInformationTO>();
		List<OriginDestinationInfo> ondInfo = source.getJourneyInfo();
		adaptCollection(ondInfo, ondInfoTOList, new OriginDestinationInfoAdaptor(source.getPreferences(), rphGenerator));
		target.setOriginDestinationInformationList(ondInfoTOList);

		// set available preferences info
		AvailablePreferenceAdaptor availablePreferenceTransformer = new AvailablePreferenceAdaptor(target.getAvailPreferences(),
				source.getJourneyInfo(), findMininum, fareCalendarSearch, preserveOndOrder,requoteFlightSerach, trackInfo);
		availablePreferenceTransformer.adapt(source.getPreferences());

		target.getAvailPreferences()
				.setSearchSystem(getSearchSystem(ondInfoTOList, source.isGroupPnr(), trackInfo.getCarrierCode()));
		target.getAvailPreferences().setIncludeHRTFares(includeHRTFares(source));

		// set traveler quantity info
		TravelerQuantityAdaptor travelerQuantityTransformer = new TravelerQuantityAdaptor(target.getTravelerInfoSummary());
		travelerQuantityTransformer.adapt(source.getTravellerQuantity());

		// set traveler preferences
		TravelerPreferenceAdaptor travelerPrefTransformer = new TravelerPreferenceAdaptor(target.getTravelPreferences());
		travelerPrefTransformer.adapt(source.getPreferences());
		
		target.setCustomerId(trackInfo.getCustomerId());
		
		return target;
	}

	private SYSTEM getSearchSystem(List<OriginDestinationInformationTO> ondInfoTOList, boolean groupPnr, String carrierCode) {
		if (requoteFlightSerach && groupPnr) {
			return SYSTEM.INT;
		}
		try {
			return CommonServiceUtil.getSearchSystem(ondInfoTOList, trackInfo.getCarrierCode());
		} catch (ModuleException e) {
			throw new ValidationException(Code.WRONG_OND_CODE);
		}
	}

	// This method should be revisited when multi city search is implemented for new IBE
	// THIS IS UNDER THE ASSUMPTION THAT BECAUSE RETURN JOURNEY IS THE AVAILABLE SEARCH FOR THE MOMENT,
	// IF OND SIZE IS 2 ALL THE RETURN JOURNEYS BECOME OPEN JAW
	// TODO
	// When multi city is implemented replace the following code AppSysParamsUtil.isAllowHalfReturnFaresForOpenJawJourney()
	//&& searchRQ.getJourneyInfo().size() == OndSequence.RETURN_OND_SIZE)
	// with AppSysParamsUtil.isAllowHalfReturnFaresForOpenJawJourney()
	//&& ISOPENJAW()
	private boolean includeHRTFares(AvailabilitySearchRQ searchRQ) {
		return AppSysParamsUtil.showHalfReturnFaresInIBECalendar() && (
				OriginDestinationInfo.isReturnSearch(searchRQ.getJourneyInfo()) || (
						AppSysParamsUtil.isAllowHalfReturnFaresForOpenJawJourney()
								&& searchRQ.getJourneyInfo().size() == OndSequence.RETURN_OND_SIZE));
	}
}
