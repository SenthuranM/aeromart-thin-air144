package com.isa.aeromart.services.endpoint.dto.common;

import java.util.Date;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.booking.ExtraDetails;


public class Passenger implements Comparable<Passenger> {

	private int paxSequence;

	private String title;

	private String firstName;

	private String lastName;

	private Integer nationality;

	private String paxType;

	private Date dateOfBirth;

	private int travelWith;

	private String parentName;

	private AdditionalInfo additionalInfo;
	
	private List<ExtraDetails> extras;
	
	private List<PricedAncillaryType> passengerWiseSelectedAncillaries;

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getNationality() {
		return nationality;
	}

	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	public int getTravelWith() {
		return travelWith;
	}

	public void setTravelWith(int travelWith) {
		this.travelWith = travelWith;
	}

	public AdditionalInfo getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(AdditionalInfo additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public List<ExtraDetails> getExtras() {
		return extras;
	}

	public void setExtras(List<ExtraDetails> extras) {
		this.extras = extras;
	}

	public List<PricedAncillaryType> getPassengerWiseSelectedAncillaries() {
		return passengerWiseSelectedAncillaries;
	}

	public void setPassengerWiseSelectedAncillaries(List<PricedAncillaryType> passengerWiseSelectedAncillaries) {
		this.passengerWiseSelectedAncillaries = passengerWiseSelectedAncillaries;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	@Override
	public int compareTo(Passenger o) {
		if (this.getPaxSequence() == 0 || o.getPaxSequence() == 0) {
			return (this.getPaxSequence() == 0) ? -1 : 1;
		}
		if (this.getPaxSequence() == 0 && o.getPaxSequence() == 0) {
			return 0;
		}
		return (new Integer(this.getPaxSequence())).compareTo(new Integer(o.getPaxSequence()));
	}

}
