package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class PassengerPageLoginResponse extends BaseRS{

	private PassengerDetails passengerDetails = null;
    
    private LoggedInLmsDetails loggedInLmsDetails = null;
      
    private boolean loyaltyManagmentEnabled = false;

	public PassengerDetails getPassengerDetails() {
		return passengerDetails;
	}

	public void setPassengerDetails(PassengerDetails passengerDetails) {
		this.passengerDetails = passengerDetails;
	}

	public LoggedInLmsDetails getLoggedInLmsDetails() {
		return loggedInLmsDetails;
	}

	public void setLoggedInLmsDetails(LoggedInLmsDetails loggedInLmsDetails) {
		this.loggedInLmsDetails = loggedInLmsDetails;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}
    
}
