package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public abstract class AncillaryItemLeaf extends AncillaryItemComponent {

    public abstract List<? extends AncillaryItem> getItems();

}
