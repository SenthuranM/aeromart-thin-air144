package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;


public class PhoneNumberResAdaptor implements Adaptor<String , PhoneNumber>{

	@Override
	public PhoneNumber adapt(String phoneNo) {
		
		if(phoneNo == null){
			return null;
		}
		PhoneNumber phoneNumber = new PhoneNumber();
		String[] phoneNoDecoded;
		
		phoneNoDecoded = phoneNo.split("-");
		
		if(phoneNoDecoded.length != 3){
			return null;
		}
		
		phoneNumber.setCountryCode(phoneNoDecoded[0]);
		phoneNumber.setAreaCode(phoneNoDecoded[1]);
		phoneNumber.setNumber(phoneNoDecoded[2]);
		
		return phoneNumber;
	}
	
}
