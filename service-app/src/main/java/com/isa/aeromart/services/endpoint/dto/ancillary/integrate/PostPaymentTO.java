package com.isa.aeromart.services.endpoint.dto.ancillary.integrate;

import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;

import java.util.List;

public class PostPaymentTO {

    private PostPaymentDTO postPaymentDto;

    private LoyaltyInformation loyaltyInformation;

    private SessionBasicReservation basicReservation;

    private ReservationInfo reservationInfo;

    private AncillaryIntegrateTO ancillaryIntegrateTo;

	private PayByVoucherInfo payByVoucherInfo;

    private Boolean isTotalAmountPaidFromVoucher;

	private Boolean totalPaidFromVoucherLMS;

    public PostPaymentDTO getPostPaymentDto() {
        return postPaymentDto;
    }

    public void setPostPaymentDto(PostPaymentDTO postPaymentDto) {
        this.postPaymentDto = postPaymentDto;
    }

    public LoyaltyInformation getLoyaltyInformation() {
        return loyaltyInformation;
    }

    public void setLoyaltyInformation(LoyaltyInformation loyaltyInformation) {
        this.loyaltyInformation = loyaltyInformation;
    }

    public SessionBasicReservation getBasicReservation() {
        return basicReservation;
    }

    public void setBasicReservation(SessionBasicReservation basicReservation) {
        this.basicReservation = basicReservation;
    }

    public ReservationInfo getReservationInfo() {
        return reservationInfo;
    }

    public void setReservationInfo(ReservationInfo reservationInfo) {
        this.reservationInfo = reservationInfo;
    }

    public AncillaryIntegrateTO getAncillaryIntegrateTo() {
        return ancillaryIntegrateTo;
    }

    public void setAncillaryIntegrateTo(AncillaryIntegrateTO ancillaryIntegrateTo) {
        this.ancillaryIntegrateTo = ancillaryIntegrateTo;
    }

	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
    }

	public void setVoucherInfos(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
    }

    public Boolean getIsTotalAmountPaidFromVoucher() {
        return isTotalAmountPaidFromVoucher;
    }

    public void setIsTotalAmountPaidFromVoucher(Boolean isTotalAmountPaidFromVoucher) {
        this.isTotalAmountPaidFromVoucher = isTotalAmountPaidFromVoucher;
    }
    
    public Boolean getTotalPaidFromVoucherLMS() {
		return totalPaidFromVoucherLMS;
	}

	public void setTotalPaidFromVoucherLMS(Boolean totalPaidFromVoucherLMS) {
		this.totalPaidFromVoucherLMS = totalPaidFromVoucherLMS;
	}

}
