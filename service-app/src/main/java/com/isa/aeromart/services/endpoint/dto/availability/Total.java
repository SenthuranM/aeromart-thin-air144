package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.List;

public class Total {

	private BigDecimal fare = BigDecimal.ZERO;

	private BigDecimal surcharge = BigDecimal.ZERO;

	private BigDecimal tax = BigDecimal.ZERO;

	private BigDecimal price = BigDecimal.ZERO;

	private Discount discount;

	private List<PaxPrice> paxWise;

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	public BigDecimal getSurcharge() {
		return surcharge;
	}

	public void setSurcharge(BigDecimal surcharge) {
		this.surcharge = surcharge;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Discount getDiscount() {
		if (discount == null) {
			discount = new Discount();
		}
		return discount;
	}

	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

	public List<PaxPrice> getPaxWise() {
		return paxWise;
	}

	public void setPaxWise(List<PaxPrice> paxWise) {
		this.paxWise = paxWise;
	}

}
