package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Date;

public class PaymentInfo {

	private Date date;

	private String paidCurrency;

	private String paymentAmount;

	private String amountInPaidCurrency;

	private String paymentMethod;

	public String getPaidCurrency() {
		return paidCurrency;
	}

	public void setPaidCurrency(String paidCurrency) {
		this.paidCurrency = paidCurrency;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getAmountInPaidCurrency() {
		return amountInPaidCurrency;
	}

	public void setAmountInPaidCurrency(String amountInPaidCurrency) {
		this.amountInPaidCurrency = amountInPaidCurrency;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
