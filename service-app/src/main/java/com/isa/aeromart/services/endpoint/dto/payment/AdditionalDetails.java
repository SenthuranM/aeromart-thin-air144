package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

public class AdditionalDetails {

	private String param1;// makePayReq
	private String param2;// makePayReq
	private List<String> param3;// makePayReq

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public List<String> getParam3() {
		return param3;
	}

	public void setParam3(List<String> param3) {
		this.param3 = param3;
	}

}
