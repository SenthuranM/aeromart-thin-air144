package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.decorator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.DecoratedAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.bl.promotion.AncillaryFreeServicePromotionImpl;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.MonetaryAmendment;
import com.isa.aeromart.services.endpoint.dto.ancillary.MonetaryAmendmentDefinition;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.flexi.FlexiItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItems;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySelection;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

public class MonetaryAmendmentDecorator<S>
		extends DecoratedAdaptor<MonetaryAmendmentDecorator.MonetaryAmendmentWrapper, S, PriceQuoteAncillaryRS> {

	private Map<AncillariesConstants.MonetaryAmendment, MonetaryAmendmentDefinition> monetaryAmendmentMap;

	private int counter = 1;

	public PriceQuoteAncillaryRS adapt(S source) {

		PriceQuoteAncillaryRS response;

		monetaryAmendmentMap = new HashMap<AncillariesConstants.MonetaryAmendment, MonetaryAmendmentDefinition>();
		setAddedAncillary(getBaseAdaptor().adapt(source));
		setRefundedAncillaries();
		setPromotions();
		setJnTax(getDecorate().getTaxApplicability());
		setAncillaryModificationPenalty(getDecorate().isApplyAnciPenalty());

		response = getAddedAncillary();
		// populate monetaryAmendmentDefinitions for priceQuoteRS
		if (monetaryAmendmentMap.size() > 0) {
			if (response.getMetaData() == null) {
				response.setMetaData(new MetaData());
				response.getMetaData().setMonetaryAmendmentDefinitions(new ArrayList<MonetaryAmendmentDefinition>());
			} else if (response.getMetaData().getMonetaryAmendmentDefinitions() == null) {
				response.getMetaData().setMonetaryAmendmentDefinitions(new ArrayList<MonetaryAmendmentDefinition>());
			}
			for (MonetaryAmendmentDefinition monetaryAmendmentDefinition : monetaryAmendmentMap.values()) {
				response.getMetaData().getMonetaryAmendmentDefinitions().add(monetaryAmendmentDefinition);
			}
		}
		return response;
	}

	private void setRefundedAncillaries() {

		/*
		 * TODO check whether each removed ancillary charges are eligible for refunding at the moment all remove
		 * ancillary charges are refunded
		 */
		MonetaryAmendmentDefinition monetaryAmendmentDefinition;
		BigDecimal paxWiseRemovedAnciTotal;
		MonetaryAmendment monetaryAmendment;

		if (getRemoveAncillary() != null && getRemoveAncillary().getAncillaryReservation().getPassengers().size() > 0) {
			for (AncillaryPassenger ancillaryPassenger : getAddedAncillary().getAncillaryReservation().getPassengers()) {
				for (AncillaryPassenger removeAncillaryPassenger : getRemoveAncillary().getAncillaryReservation()
						.getPassengers()) {
					if (ancillaryPassenger.getPassengerRph().equals(removeAncillaryPassenger.getPassengerRph())) {
						monetaryAmendmentDefinition = resolveMonetaryAmendmentDefinition(
								AncillariesConstants.MonetaryAmendment.PREVIOUS_SELECTION);
						paxWiseRemovedAnciTotal = AnciCommon.getAncillaryPassengerTotal(removeAncillaryPassenger);
						monetaryAmendment = new MonetaryAmendment();
						monetaryAmendment.setAmount(paxWiseRemovedAnciTotal);
						monetaryAmendment.setDefinitionId(monetaryAmendmentDefinition.getDefinitionId());
						if (ancillaryPassenger.getAmendments() == null) {
							ancillaryPassenger.setAmendments(new ArrayList<MonetaryAmendment>());
						}
						ancillaryPassenger.getAmendments().add(monetaryAmendment);
					}
				}
			}
		}
	}

	private void setPromotions() {
		// implement all the anci related promotions to a common anci promotion delegate
		MonetaryAmendmentDefinition monetaryAmendmentDefinition;

		DiscountedFareDetails discountedFareDetails = getTransactionalAncillaryService()
				.getDiscountedFareDetails(getTransactionId());
		if (discountedFareDetails != null && discountedFareDetails.getPromotionType()
				.equals(PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE)) {
			AncillaryFreeServicePromotionImpl freeServiceImpl = new AncillaryFreeServicePromotionImpl();
			freeServiceImpl.setTransactionalAncillaryService(getTransactionalAncillaryService());
			freeServiceImpl.setTransactionalId(getTransactionId());
			freeServiceImpl.setTrackInfo(getTrackInfoDTO());
			try {
				ReservationDiscountDTO reservationDiscountDTO = freeServiceImpl.calculatePromotion();
				if (reservationDiscountDTO != null && AccelAeroCalculator.isGreaterThan(reservationDiscountDTO.getTotalDiscount(),
						AccelAeroCalculator.getDefaultBigDecimalZero())) {
					monetaryAmendmentDefinition = resolveMonetaryAmendmentDefinition(
							AncillariesConstants.MonetaryAmendment.FREESERVICE);
					monetaryAmendmentDefinition
							.setCategory(AnciCommon.resolveMonetaryAmendmentCategory(reservationDiscountDTO.getDiscountAs()));
					setAddedAncillary(freeServiceImpl.applyPromotion(getAddedAncillary(), reservationDiscountDTO,
							monetaryAmendmentDefinition.getDefinitionId()));
					getTransactionalAncillaryService().setDiscounAmount(getTransactionId(),
							reservationDiscountDTO.getTotalDiscount());
				}
			} catch (ModuleException e) {
				throw new ValidationException(ValidationException.Code.ANCI_GENERIC_ERROR, e);
			}

		}

	}

	private void setJnTax(AnciAvailabilityRS taxApplicability) {

		BigDecimal paxWiseAnciTotal, anciTaxCharge;
		MonetaryAmendmentDefinition monetaryAmendmentDefinition;
		MonetaryAmendment monetaryAmendment;

		List<InventoryWrapper> inventory = getTransactionalAncillaryService().getFlightSegments(getTransactionId());
		InventoryWrapper firstDepartingSegment = AnciCommon.getFirstDepartingSegment(inventory);
		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());

		if (taxApplicability != null && taxApplicability.isJnTaxApplicable()
				&& getAddedAncillary().getAncillaryReservation().getPassengers() != null
				&& getAddedAncillary().getAncillaryReservation().getPassengers().size() > 0) {
			for (AncillaryPassenger ancillaryPassenger : getAddedAncillary().getAncillaryReservation().getPassengers()) {

				paxWiseAnciTotal = AnciCommon.getAncillaryPassengerTotal(AnciCommon.filterJNTaxRelatedAncillary(rphGenerator,
						ancillaryPassenger, getAddedAncillary().getMetaData(),
						firstDepartingSegment.getFlightSegmentTO().getFlightRefNumber()));
				paxWiseAnciTotal = updateFlexiCharge(rphGenerator, paxWiseAnciTotal,
						firstDepartingSegment.getFlightSegmentTO().getFlightRefNumber());
				paxWiseAnciTotal = updateInsuranceCharge(paxWiseAnciTotal);
				if (getRemoveAncillary() != null && getRemoveAncillary().getAncillaryReservation().getPassengers().size() > 0) {
					for (AncillaryPassenger removeAncillaryPassenger : getRemoveAncillary().getAncillaryReservation()
							.getPassengers()) {
						if (ancillaryPassenger.getPassengerRph().equals(removeAncillaryPassenger.getPassengerRph())) {
							paxWiseAnciTotal = AccelAeroCalculator.subtract(paxWiseAnciTotal,
									AnciCommon.getAncillaryPassengerTotal(AnciCommon.filterJNTaxRelatedAncillary(rphGenerator,
											removeAncillaryPassenger, getRemoveAncillary().getMetaData(), firstDepartingSegment
													.getFlightSegmentTO().getFlightRefNumber())));
						}
					}
				}

				anciTaxCharge = AnciCommon.calculateTaxCharge(paxWiseAnciTotal, taxApplicability.getTaxRatio());
				if (AccelAeroCalculator.isGreaterThan(anciTaxCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
					monetaryAmendmentDefinition = resolveMonetaryAmendmentDefinition(
							AncillariesConstants.MonetaryAmendment.JN_TAX);
					monetaryAmendment = new MonetaryAmendment();
					monetaryAmendment.setAmount(anciTaxCharge);
					monetaryAmendment.setDefinitionId(monetaryAmendmentDefinition.getDefinitionId());
					if (ancillaryPassenger.getAmendments() == null) {
						ancillaryPassenger.setAmendments(new ArrayList<MonetaryAmendment>());
					}
					ancillaryPassenger.getAmendments().add(monetaryAmendment);
				}
			}
		}
	}

	private void setAncillaryModificationPenalty(boolean ancillaryModificationPenaltyApplicability) {

		BigDecimal paxWiseAnciTotal;
		MonetaryAmendmentDefinition monetaryAmendmentDefinition;
		MonetaryAmendment monetaryAmendment;
		boolean anciModifyPenaltyForSystem;

		if (ProxyConstants.SYSTEM.AA.equals(ProxyConstants.SYSTEM.getEnum(
				getTransactionalAncillaryService().getPreferencesForAncillary(getTransactionId()).getSelectedSystem()))) {
			anciModifyPenaltyForSystem = !AppSysParamsUtil.isRefundIBEAnciModifyCredit();
		} else {
			anciModifyPenaltyForSystem = false;
		}
		if (ancillaryModificationPenaltyApplicability && anciModifyPenaltyForSystem
				&& getAddedAncillary().getAncillaryReservation().getPassengers() != null
				&& getAddedAncillary().getAncillaryReservation().getPassengers().size() > 0) {

			for (AncillaryPassenger ancillaryPassenger : getAddedAncillary().getAncillaryReservation().getPassengers()) {
				paxWiseAnciTotal = AnciCommon.getAncillaryPassengerTotal(ancillaryPassenger);
				paxWiseAnciTotal = updateInsuranceCharge(paxWiseAnciTotal);
				if (getRemoveAncillary() != null && getRemoveAncillary().getAncillaryReservation().getPassengers().size() > 0) {
					for (AncillaryPassenger removeAncillaryPassenger : getRemoveAncillary().getAncillaryReservation()
							.getPassengers()) {
						if (ancillaryPassenger.getPassengerRph().equals(removeAncillaryPassenger.getPassengerRph())) {
							paxWiseAnciTotal = AccelAeroCalculator.subtract(paxWiseAnciTotal,
									AnciCommon.getAncillaryPassengerTotal(removeAncillaryPassenger));
						}
					}
				}

				if (AccelAeroCalculator.isLessThan(paxWiseAnciTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
					monetaryAmendmentDefinition = resolveMonetaryAmendmentDefinition(
							AncillariesConstants.MonetaryAmendment.ANCILLARY_PENALTY);
					monetaryAmendment = new MonetaryAmendment();
					monetaryAmendment.setAmount(paxWiseAnciTotal);
					monetaryAmendment.setDefinitionId(monetaryAmendmentDefinition.getDefinitionId());
					if (ancillaryPassenger.getAmendments() == null) {
						ancillaryPassenger.setAmendments(new ArrayList<MonetaryAmendment>());
					}
					ancillaryPassenger.getAmendments().add(monetaryAmendment);
				}
			}
		}
	}

	private BigDecimal updateInsuranceCharge(BigDecimal anciTotal) {

		int nonInfants = 0;
		int paxCount = 0;

		TravellerQuantity travellerQuantity = getTransactionalAncillaryService()
				.getTravellerQuantityFromSession(getTransactionId());
		nonInfants = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();
		AncillarySelection ancillarySelection = getTransactionalAncillaryService()
				.getAncillarySelectionsFromSession(getTransactionId());
		AncillaryQuotation ancillaryQuotation = getTransactionalAncillaryService().getAncillaryQuotation(getTransactionId());

		if (ancillaryQuotation.getQuotedAncillaries() != null) {

			paxCount = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount() + travellerQuantity.getInfantCount();
			nonInfants = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();

			for (SelectedAncillary selectedAncillary : ancillarySelection.getAncillaryPreferences()) {
				if (selectedAncillary.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())) {
					for (Preference preference : selectedAncillary.getPreferences()) {
						// since flexibility is for all passengers no need to refer it as passenger wise
						for (Selection selection : preference.getSelections()) {

							for (AncillaryType ancillaryType : ancillaryQuotation.getQuotedAncillaries()) {
								if (ancillaryType.getAncillaryType()
										.equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())) {
									for (Provider provider : ancillaryType.getProviders()) {
										// since at the moment flexi is provided by the system provider
										for (AvailableAncillaryUnit availableAncillaryUnit : provider.getAvailableAncillaries()
												.getAvailableUnits()) {
											InsuranceItems insuranceItems = (InsuranceItems) availableAncillaryUnit
													.getItemsGroup();
											for (SelectedItem selectedItem : selection.getSelectedItems()) {
												for (InsuranceItem insuranceItem : insuranceItems.getItems()) {
													if (selectedItem.getId().equals(insuranceItem.getItemId())) {
														if (AppSysParamsUtil.allowAddInsurnaceForInfants()) {
															anciTotal = AccelAeroCalculator.add(anciTotal,
																	AccelAeroCalculator.divide(
																			insuranceItem.getCharges().get(0).getAmount(),
																			paxCount));
														} else {
															anciTotal = AccelAeroCalculator.add(anciTotal,
																	AccelAeroCalculator.divide(
																			insuranceItem.getCharges().get(0).getAmount(),
																			nonInfants));
														}

													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return anciTotal;
	}

	private BigDecimal updateFlexiCharge(RPHGenerator rphGenerator, BigDecimal anciTotal, String flightSegRPH) {

		int nonInfants = 0;

		TravellerQuantity travellerQuantity = getTransactionalAncillaryService()
				.getTravellerQuantityFromSession(getTransactionId());
		nonInfants = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();
		AncillarySelection ancillarySelection = getTransactionalAncillaryService()
				.getAncillarySelectionsFromSession(getTransactionId());
		AncillaryQuotation ancillaryQuotation = getTransactionalAncillaryService().getAncillaryQuotation(getTransactionId());
		List<SessionFlexiDetail> sessionFlexiDetailList = getTransactionalAncillaryService()
				.getSessionFlexiDetail(getTransactionId());
		String taxApplicableCarrier = FlightRefNumberUtil.getOperatingAirline(flightSegRPH);

		// update Ancillary selected flexibility total
		for (SelectedAncillary selectedAncillary : ancillarySelection.getAncillaryPreferences()) {
			if (selectedAncillary.getType().equals(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode())) {
				for (Preference preference : selectedAncillary.getPreferences()) {
					// since flexibility is for all passengers no need to refer it as passenger wise
					for (Selection selection : preference.getSelections()) {
						OndScope ondScope = (OndScope) selection.getScope();
						for (OndUnit OndUnit : getAddedAncillary().getMetaData().getOndPreferences()) {
							if (OndUnit.getOndId().equals(ondScope.getOndId())) {
								if (flightSegRPH != null && !flightSegRPH.equals("") && AnciCommon.isOndContainsGivenCarrierCode(
										rphGenerator, OndUnit.getFlightSegmentRPH(), taxApplicableCarrier)) {
									for (AncillaryType ancillaryType : ancillaryQuotation.getQuotedAncillaries()) {
										if (ancillaryType.getAncillaryType()
												.equals(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode())) {
											for (Provider provider : ancillaryType.getProviders()) {
												// since at the moment flexi is provided by the system provider
												for (AvailableAncillaryUnit availableAncillaryUnit : provider
														.getAvailableAncillaries().getAvailableUnits()) {
													OndScope quotedOndScope = (OndScope) availableAncillaryUnit.getScope();
													if (quotedOndScope.getOndId().equals(ondScope.getOndId())) {
														FlexiItems flexiItems = (FlexiItems) availableAncillaryUnit
																.getItemsGroup();
														for (SelectedItem selectedItem : selection.getSelectedItems()) {
															for (FlexiItem flexiItem : flexiItems.getItems()) {
																if (selectedItem.getId().equals(flexiItem.getItemId())) {
																	anciTotal = AccelAeroCalculator.add(anciTotal,
																			AccelAeroCalculator.divide(
																					flexiItem.getCharges().get(0).getAmount(),
																					nonInfants));
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		// update user availability selected flexibility total passenger wise
		if (sessionFlexiDetailList != null) {
			for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiDetailList) {
				if (sessionFlexiDetail.isFlexiAvailable() && sessionFlexiDetail.isFlexiSelected()) {
					anciTotal = AccelAeroCalculator.add(anciTotal,
							AccelAeroCalculator.divide(sessionFlexiDetail.getTotalFlexiCharge(), nonInfants));
				}
			}
		}

		return anciTotal;
	}

	private MonetaryAmendmentDefinition
			resolveMonetaryAmendmentDefinition(AncillariesConstants.MonetaryAmendment monetaryAmendment) {
		MonetaryAmendmentDefinition monetaryAmendmentDefinition = null;
		int definitionId = counter;
		if (!monetaryAmendmentMap.containsKey(monetaryAmendment)) {
			monetaryAmendmentDefinition = new MonetaryAmendmentDefinition();
			counter++;
			if (monetaryAmendment.equals(AncillariesConstants.MonetaryAmendment.JN_TAX)) {
				monetaryAmendmentDefinition.setCategory(MonetaryAmendmentDefinition.AmendmentCategory.DEBIT);
				monetaryAmendmentDefinition.setType(monetaryAmendment);
				monetaryAmendmentDefinition.setDefinitionId(definitionId);
				monetaryAmendmentDefinition.setDescription("Applicable JN Tax");
			} else if (monetaryAmendment.equals(AncillariesConstants.MonetaryAmendment.ANCILLARY_PENALTY)) {
				monetaryAmendmentDefinition.setCategory(MonetaryAmendmentDefinition.AmendmentCategory.DEBIT);
				monetaryAmendmentDefinition.setType(monetaryAmendment);
				monetaryAmendmentDefinition.setDefinitionId(definitionId);
				monetaryAmendmentDefinition.setDescription("Applicable Ancillary Panalty");
			} else if (monetaryAmendment.equals(AncillariesConstants.MonetaryAmendment.PREVIOUS_SELECTION)) {
				monetaryAmendmentDefinition.setCategory(MonetaryAmendmentDefinition.AmendmentCategory.CREDIT);
				monetaryAmendmentDefinition.setType(monetaryAmendment);
				monetaryAmendmentDefinition.setDefinitionId(definitionId);
				monetaryAmendmentDefinition.setDescription("Previous ancillary selection");
			} else if (monetaryAmendment.equals(AncillariesConstants.MonetaryAmendment.FREESERVICE)) {
				// free services may be money or credit in the back end but for front end we use it as credit
				monetaryAmendmentDefinition.setCategory(MonetaryAmendmentDefinition.AmendmentCategory.MONEY);
				monetaryAmendmentDefinition.setType(monetaryAmendment);
				monetaryAmendmentDefinition.setDefinitionId(definitionId);
				monetaryAmendmentDefinition.setDescription("Ancillary Free Services Discount");
			}
			monetaryAmendmentMap.put(monetaryAmendment, monetaryAmendmentDefinition);
		}
		return monetaryAmendmentMap.get(monetaryAmendment);
	}

	public static class MonetaryAmendmentWrapper {

		private AnciAvailabilityRS taxApplicability;

		private boolean applyAnciPenalty;

		public AnciAvailabilityRS getTaxApplicability() {
			return taxApplicability;
		}

		public void setTaxApplicability(AnciAvailabilityRS taxApplicability) {
			this.taxApplicability = taxApplicability;
		}

		public boolean isApplyAnciPenalty() {
			return applyAnciPenalty;
		}

		public void setApplyAnciPenalty(boolean applyAnciPenalty) {
			this.applyAnciPenalty = applyAnciPenalty;
		}

	}
}
