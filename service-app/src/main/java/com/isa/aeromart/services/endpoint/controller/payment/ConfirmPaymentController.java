package com.isa.aeromart.services.endpoint.controller.payment;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.IPGTransactionRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationAssemblerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.RequoteConfirmAdaptor;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.ancillary.AncillaryService;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.modification.ModificationService;
import com.isa.aeromart.services.endpoint.delegate.payment.ConfirmPaymentServiceAdapter;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.booking.IPGTransactionResult;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.payment.IPGHandlerRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentConfirmationRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentAncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.booking.BookingUtil;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

@Controller
@RequestMapping(value = "payment", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
public class ConfirmPaymentController extends StatefulController {

	private static Log log = LogFactory.getLog(ConfirmPaymentController.class);

	@Autowired
	private ConfirmPaymentServiceAdapter confirmPaymentService;

	@Autowired
	private AncillaryService ancillaryService;

	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private ModificationService modificationService;

	private static final String URL_ENCODING = "UTF-8";
	
	@ResponseBody
	@RequestMapping(value = "/receipt/confirmation/create")
	public TransactionalBaseRS confirmPaymentCreate(@RequestBody PaymentConfirmationRQ paymentConfirmationRQ) throws ModuleException,
			UnsupportedEncodingException {
		log.debug("inside payment receipt confirmation, create flow");
		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(paymentConfirmationRQ.getTransactionId());

		PaymentConfirmationRS paymentConfirmationResponse = null;
		try {
			paymentConfirmationResponse = validatePaymentReceipt(paymentConfirmationRQ, paymentSessionStore);
		} catch (ModuleException me) {
			if (ErrorType.PAYMENT_ERROR == me.getErrorType()) {
				if (paymentSessionStore.isOnHoldCreatedForPayment()) {
					me.setErrorType(ErrorType.PAYMENT_ERROR_ONHOLD);
				}
			}

			throw me;
		}

		BookingRS response = new BookingRS();
		PostPaymentDTO postPaymentDTO = null;
		BookingSessionStore bookingSessionStore = getTrasactionStore(paymentConfirmationRQ.getTransactionId());
		if (paymentSessionStore != null) {
			postPaymentDTO = paymentSessionStore.getPostPaymentInformation();
		}

		if (paymentConfirmationResponse != null && paymentConfirmationResponse.isSuccess()) {
			response.setSuccess(true);
			if (PaymentStatus.SUCCESS.equals(paymentConfirmationResponse.getPaymentStatus())) {

				response.setPaymentStatus(BookingRS.PaymentStatus.SUCCESS);
				log.info("payment success confirm the booking");

				boolean onholdCreated = paymentSessionStore.isOnHoldCreatedForPayment();

				BookingRQ bookingRQ = paymentSessionStore.getbookingRQForPayment();

				if (postPaymentDTO.isBalancePayment() || onholdCreated) {
					try {
						CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
						String customerID = customerSessionStore.getLoggedInCustomerID();
						LCCClientReservation reservation = bookingService.balancePayment(
								getTrasactionStore(paymentConfirmationRQ.getTransactionId()), getTrackInfo(), customerID);
						response.setFirstDepartureDate(BookingUtil.getFirstDeparture(reservation));
						response.setContactLastName(BookingUtil.getContactLastName(reservation));
						
						// TODO: on balance payment BOOKING_UPDATED_SUCCESS
						response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS);
						CustomerUtil.saveCustomerAlias(customerID, postPaymentDTO);
						// TODO:need to update the PNR
						response.setSuccess(true);
					} catch (Exception e) {
						log.error("Error in confirmation", e);
						if (onholdCreated) {
							response.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_PAYMENT_FAILED);
						}

					}
				} else if (bookingRQ != null) {
					BookingRS bookingRS = null;
					bookingRQ.setCreateOnhold(false);
					bookingRQ.setOnholdReleaseZuluTimeStamp(null);
					AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(getTrackInfo(),
							paymentConfirmationRQ);
					CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
					String customerID = customerSessionStore.getLoggedInCustomerID();
					ReservationAssemblerAdaptor reservationAssemblerAdaptor = new ReservationAssemblerAdaptor(getTrackInfo(),
							(BookingSessionStore) getTrasactionStore(paymentConfirmationRQ.getTransactionId()),
							ancillaryIntegrateTo, customerID);
					CommonReservationAssembler commonReservationAssembler = reservationAssemblerAdaptor.adapt(bookingRQ);
					response.setContactLastName(bookingRQ.getReservationContact().getLastName());
					response.setFirstDepartureDate(getFirstDepartureDateFromSession(paymentConfirmationRQ.getTransactionId()));
					bookingRS = (BookingRS) bookingService.createBooking(commonReservationAssembler, ancillaryIntegrateTo,
							getTrackInfo());
					if (bookingRS != null && bookingRS.isSuccess()) {
						response.setPnr(bookingRS.getPnr());
						response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS);
						CustomerUtil.saveCustomerAlias(customerID, postPaymentDTO);
					} else {
						response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_FAILED);
					}
				}

			} else if (PaymentStatus.ALREADY_COMPLETED.equals(paymentConfirmationResponse.getPaymentStatus())) {
				response.setPaymentStatus(BookingRS.PaymentStatus.ALREADY_COMPLETED);
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS);
			} else {
				response.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_FAILED);
			}

			if (BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS.equals(response.getActionStatus())) {

				CommonServiceUtil commonServices = new CommonServiceUtil();
				commonServices.updateBlockedLmsCreditStatusForCreateReservation(response, bookingSessionStore);
			}
			
			if (StringUtil.isNullOrEmpty(response.getPnr()) && postPaymentDTO != null
					&& !StringUtil.isNullOrEmpty(postPaymentDTO.getPnr())) {
				response.setPnr(paymentSessionStore.getPostPaymentInformation().getPnr());
			}
		}

		response.setIpgTransactionResult(getIPGTransactionResult(getPaymentSessionStore(paymentConfirmationRQ.getTransactionId())
				.getPostPaymentInformation()));

		bookingSessionStore.clearBookingDetails();
		terminateTransaction(paymentConfirmationRQ.getTransactionId());

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/receipt/confirmation/modify")
	public TransactionalBaseRS confirmPaymentModify(@RequestBody PaymentConfirmationRQ paymentConfirmationRQ) throws ModuleException {
		log.debug("inside payment receipt confirmation, modification flow ");
		PaymentRequoteSessionStore sessionStore = getRequotePaymentSession(paymentConfirmationRQ.getTransactionId());
		PaymentConfirmationRS paymentConfirmationResponse = validatePaymentReceipt(paymentConfirmationRQ, sessionStore);

		PostPaymentDTO postPaymentDTO = null;
		if (sessionStore != null) {
			postPaymentDTO = sessionStore.getPostPaymentInformation();
		}
		BookingRS response = new BookingRS();

		if (paymentConfirmationResponse != null && paymentConfirmationResponse.isSuccess()) {
			response.setSuccess(true);
			if (PaymentStatus.SUCCESS.equals(paymentConfirmationResponse.getPaymentStatus())) {

				response.setPaymentStatus(BookingRS.PaymentStatus.SUCCESS);
				CustomerUtil.saveCustomerAlias(getTrackInfo().getCustomerId(), postPaymentDTO);
				try {

					RequoteSessionStore requoteSessionStore = getTrasactionStore(paymentConfirmationRQ.getTransactionId());
					List<ReservationPaxTO> anciIntegratedPaxList = getPaxListWithSelectedAncillaryDTOs(requoteSessionStore
							.getRequoteBalanceReqiredParams().isModifySegment(), paymentConfirmationRQ);
					RequoteModifyRQ requoteModifyRQ = RequoteConfirmAdaptor.getRequoteModifyRQ(
							requoteSessionStore,
							getTrackInfo(),
							getReservationInsurances(requoteSessionStore.getRequoteBalanceReqiredParams().isModifySegment(),
									paymentConfirmationRQ), anciIntegratedPaxList);
					// TODO for IBE?
					LCCClientReservation reservation = ModuleServiceLocator.getAirproxyReservationBD()
							.requoteModifySegmentsWithAutoRefundForIBE(requoteModifyRQ, getTrackInfo());
					reservation = bookingService.loadProxyReservation(requoteSessionStore.getFFIDChangeRQ().getPnr(),
							requoteSessionStore.getFFIDChangeRQ().isGroupPnr(), getTrackInfo(), true, false, null, null,
							AppSysParamsUtil.isPromoCodeEnabled());
					modificationService.modifyFFIDInfo(requoteSessionStore.getFFIDChangeRQ(), reservation, getTrackInfo());
					response.setActionStatus(BookingRS.ActionStatus.BOOKING_MODIFICATION_SUCCESS);
					// requoteSessionStore.clearRequoteSessionTansaction();
					log.info("payment success confirm the booking");
					
					CommonServiceUtil commonServices = new CommonServiceUtil();
					commonServices.updateBlockedLmsCreditStatusForModifyReservation(requoteSessionStore);

					// send medical ssr email
					SSRServicesUtil.sendMedicalSsrEmail(reservation.getPNR(), reservation.getPassengers(), anciIntegratedPaxList,
							reservation.getContactInfo());

				} catch (Exception e) {
					log.error("error while saving requote payment", e);
					response.setActionStatus(BookingRS.ActionStatus.BOOKING_MODIFICATION_FAILED);
				}

			} else {
				response.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_FAILED);
			}

			if (StringUtil.isNullOrEmpty(response.getPnr()) && postPaymentDTO != null
					&& !StringUtil.isNullOrEmpty(postPaymentDTO.getPnr())) {
				response.setPnr(sessionStore.getPostPaymentInformation().getPnr());
			}
		}
		response.setPnr(sessionStore.getResInfo().getPnr());
		response.setFirstDepartureDate(sessionStore.getSegments().get(0).getDepartureDateTime());
		if (sessionStore.getResInfo().getContactInfo() != null) {
			response.setContactLastName(sessionStore.getResInfo().getContactInfo().getLastName());
		}

		response.setIpgTransactionResult(getIPGTransactionResult(getRequotePaymentSession(
				paymentConfirmationRQ.getTransactionId()).getPostPaymentInformation()));
		terminateTransaction(paymentConfirmationRQ.getTransactionId());
		return response;

	}

	@ResponseBody
	@RequestMapping(value = "/receipt/confirmation/ancillary")
	public PaymentConfirmationRS confirmPaymentModifyAncillary(@RequestBody PaymentConfirmationRQ paymentConfirmationRQ)
			throws ModuleException {

		log.debug("inside payment receipt confirmation, ancillary modification flow ");

		PaymentAncillarySessionStore sessionStore = getAnciPaymentSession(paymentConfirmationRQ.getTransactionId());

		PaymentConfirmationRS paymentConfirmationResponse = validatePaymentReceipt(paymentConfirmationRQ, sessionStore);

		if (paymentConfirmationResponse.getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
			try {
				ancillaryService.updateAncillary(getTrackInfo(), paymentConfirmationRQ);
			} catch (Exception e) {
				log.error("Ancillary modification payment confirmation has failed", e);
				throw new ModuleException("payment.api.anci.modification.update.failed");

			}
		}
		paymentConfirmationResponse.setPnr(sessionStore.getResInfo().getPnr());
		paymentConfirmationResponse.setFirstDepartureDate(sessionStore.getSegments().get(0).getDepartureDateTime());
		if (sessionStore.getResInfo().getContactInfo() != null) {
			paymentConfirmationResponse.setContactLastName(sessionStore.getResInfo().getContactInfo().getLastName());
		}

		paymentConfirmationResponse.setIpgTransactionResult(getIPGTransactionResult(getAnciPaymentSession(
				paymentConfirmationRQ.getTransactionId()).getPostPaymentInformation()));
		terminateTransaction(paymentConfirmationRQ.getTransactionId());
		
		if (PaymentStatus.SUCCESS.equals(paymentConfirmationResponse.getPaymentStatus())) {
			CommonServiceUtil commonServices = new CommonServiceUtil();
			commonServices.updateBlockedLmsCreditStatusForModifyAncillary(sessionStore);

		}

		return paymentConfirmationResponse;
	}

	private Map<String, String> splitPaymentReceiptMap(String uriParam) throws UnsupportedEncodingException {
		Map<String, String> paymentReceiptMap = new HashMap<String, String>();
		if (!StringUtil.isNullOrEmpty(uriParam)) {
			String[] pairs = uriParam.split("&");
			for (String pair : pairs) {
				int idx = pair.indexOf("=");
				paymentReceiptMap.put(URLDecoder.decode(pair.substring(0, idx), URL_ENCODING),
						URLDecoder.decode(pair.substring(idx + 1), URL_ENCODING));

			}
		} else {
			log.info("PostParam is null or empty");
		}
		return paymentReceiptMap;
	}

	private PaymentConfirmationRS validatePaymentReceipt(PaymentConfirmationRQ paymentConfirmationRQ,
			PaymentBaseSessionStore sessionStore) throws ModuleException {
		IPGHandlerRQ ipgHandlerRequest = new IPGHandlerRQ();
		try {
			if (paymentConfirmationRQ.getPaymentReceiptMap() == null || paymentConfirmationRQ.getPaymentReceiptMap().isEmpty()) {
				log.debug("Building Receipt map from PostParam");
				paymentConfirmationRQ.setPaymentReceiptMap(splitPaymentReceiptMap(paymentConfirmationRQ.getPostParam()));

			}
		} catch (UnsupportedEncodingException ux) {
			log.error("Invalid post payment parameters", ux);
			throw new ModuleException("payment.api.invalid.post.param.data");
		}

		if (paymentConfirmationRQ.getPaymentReceiptMap() == null || paymentConfirmationRQ.getPaymentReceiptMap().isEmpty()) {
			log.error("Invalid receipt map and PostParam, both empty or null");
			throw new ModuleException("payment.api.invalid.post.param.data");
		}

		PostPaymentDTO postPaymentDTO = sessionStore.getPostPaymentInformation();

		postPaymentDTO.setReceiptyMap(paymentConfirmationRQ.getPaymentReceiptMap());

		ipgHandlerRequest.setPostPaymentDTO(postPaymentDTO);
		ipgHandlerRequest.setOperationType(sessionStore.getOperationType());
		PaymentConfirmationRS paymentConfirmationRS = confirmPaymentService.processPaymentReceipt(
				paymentConfirmationRQ.getTransactionId(), ipgHandlerRequest, getTrackInfo());
		return paymentConfirmationRS;
	}

	private PaymentSessionStore getPaymentSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private PaymentRequoteSessionStore getRequotePaymentSession(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private PaymentAncillarySessionStore getAnciPaymentSession(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private List<LCCClientReservationInsurance> getReservationInsurances(boolean isModifySegment, TransactionalBaseRQ baseRQ)
			throws ModuleException {
		List<LCCClientReservationInsurance> reservationInsurances = null;
		if (isModifySegment) {
			if (ancillaryService != null) {
				AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(getTrackInfo(), baseRQ);
				if (ancillaryIntegrateTo != null && ancillaryIntegrateTo.getReservation() != null) {
					reservationInsurances = ancillaryIntegrateTo.getReservation().getReservationInsurances();
				}
			}
		}
		return reservationInsurances;
	}

	private List<ReservationPaxTO> getPaxListWithSelectedAncillaryDTOs(boolean isModifySegment, TransactionalBaseRQ baseRQ)
			throws ModuleException {
		List<ReservationPaxTO> paxList = null;
		if (isModifySegment) {
			if (ancillaryService != null) {
				AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(getTrackInfo(), baseRQ);
				if (ancillaryIntegrateTo != null && ancillaryIntegrateTo.getReservation() != null) {
					paxList = new ArrayList<ReservationPaxTO>();
					List<AncillaryIntegratePassengerTO> anciPassengers = ancillaryIntegrateTo.getReservation().getPassengers();
					for (AncillaryIntegratePassengerTO anciPax : anciPassengers) {
						ReservationPaxTO resPax = new ReservationPaxTO();
						resPax.setSeqNumber(Integer.parseInt(anciPax.getPassengerRph()));
						for (LCCSelectedSegmentAncillaryDTO anciDTO : anciPax.getSelectedAncillaries()) {
							resPax.addSelectedAncillaries(anciDTO);
						}
						paxList.add(resPax);
					}
				}
			}
		}
		return paxList;
	}

	private IPGTransactionResult getIPGTransactionResult(PostPaymentDTO postPaymentInformation) {

		if (postPaymentInformation != null && postPaymentInformation.getIpgResponseDTO() != null
				&& postPaymentInformation.getIpgResponseDTO().getIpgTransactionResultDTO() != null) {
			IPGTransactionRSAdaptor ipgTransactionRSAdaptor = new IPGTransactionRSAdaptor();
			return ipgTransactionRSAdaptor.adapt(postPaymentInformation.getIpgResponseDTO().getIpgTransactionResultDTO());
		}
		return null;

	}

	private Date getFirstDepartureDateFromSession(String transactionID) {
		BookingSessionStore bookingSessionStore = getTrasactionStore(transactionID);
		return bookingSessionStore.getSegments().get(0).getDepartureDateTime();
	}
}
