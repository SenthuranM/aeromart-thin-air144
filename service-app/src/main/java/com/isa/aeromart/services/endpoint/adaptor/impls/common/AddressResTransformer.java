package com.isa.aeromart.services.endpoint.adaptor.impls.common;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Address;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;

public class AddressResTransformer implements Adaptor<CommonReservationContactInfo, Address>{

	@Override
	public Address adapt(CommonReservationContactInfo commonReservationContactInfo) {
		Address address = new Address();
		address.setCity(commonReservationContactInfo.getCity());
		address.setStreetAddress1(commonReservationContactInfo.getStreetAddress1());
		address.setStreetAddress2(commonReservationContactInfo.getStreetAddress2());
		return address;
	}

}
