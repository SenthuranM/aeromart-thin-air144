package com.isa.aeromart.services.endpoint.dto.ancillary.type.seat;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.CompareToBuilder;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;

public class SeatItem extends AncillaryItem implements Comparable<SeatItem> {

	private String cabinType;

	private String rowGroupId;

	private String columnGroupId;

	private Integer rowNumber;

	private Integer columnNumber;

	private String seatVisibility;

	private boolean availability;

	private String seatType;

	private List<String> notAllowedPassengerType;

	private String seatLocationType;

	public String getCabinType() {
		return cabinType;
	}

	public void setCabinType(String cabinType) {
		this.cabinType = cabinType;
	}

	public String getRowGroupId() {
		return rowGroupId;
	}

	public void setRowGroupId(String rowGroupId) {
		this.rowGroupId = rowGroupId;
	}

	public String getColumnGroupId() {
		return columnGroupId;
	}

	public void setColumnGroupId(String columnGroupId) {
		this.columnGroupId = columnGroupId;
	}

	public Integer getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}

	public Integer getColumnNumber() {
		return columnNumber;
	}

	public void setColumnNumber(Integer columnNumber) {
		this.columnNumber = columnNumber;
	}

	public String getSeatVisibility() {
		return seatVisibility;
	}

	public void setSeatVisibility(String seatVisibility) {
		this.seatVisibility = seatVisibility;
	}

	public boolean isAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	public List<String> getNotAllowedPassengerType() {
		if (notAllowedPassengerType == null)
			notAllowedPassengerType = new ArrayList<String>();
		return notAllowedPassengerType;
	}

	public void setNotAllowedPassengerType(List<String> notAllowedPassengerType) {
		this.notAllowedPassengerType = notAllowedPassengerType;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	@Override
	public int compareTo(SeatItem comparingSeat) {
		return new CompareToBuilder().append(this.getRowGroupId(), comparingSeat.getRowGroupId())
				.append(this.getColumnGroupId(), comparingSeat.getColumnGroupId())
				.append(this.getRowNumber(), comparingSeat.getRowNumber()).toComparison();
	}

	public void setSeatLocationType(String seatLocationType) {
		this.seatLocationType = seatLocationType;
	}

	public String getSeatLocationType() {
		return seatLocationType;
	}

}
