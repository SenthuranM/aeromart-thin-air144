package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;

public class LoggedInCustomerDetails {
	
	private String title;
	
	private String firstName;
	
	private String lastName;
	
	private int customerID;
	
    private String emailId;
    
    private Date dateOfBirth;
    
    private PhoneNumber mobile; 
    
    private String nationality;
    
    private String country;
    
    private String nationalityName;
    
    private String md5EncryptedEmailId;
    
    private Boolean sendPromoEmail;


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public PhoneNumber getMobile() {
		return mobile;
	}

	public void setMobile(PhoneNumber mobile) {
		this.mobile = mobile;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNationalityName() {
		return nationalityName;
	}

	public void setNationalityName(String nationalityName) {
		this.nationalityName = nationalityName;
	}

	public String getMd5EncryptedEmailId() {
		return md5EncryptedEmailId;
	}

	public void setMd5EncryptedEmailId(String md5EncryptedEmailId) {
		this.md5EncryptedEmailId = md5EncryptedEmailId;
	}

	public Boolean getSendPromoEmail() {
		return sendPromoEmail;
	}

	public void setSendPromoEmail(Boolean sendPromoEmail) {
		this.sendPromoEmail = sendPromoEmail;
	}
    
}
