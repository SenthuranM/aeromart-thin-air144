package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;


public class CountryListResponse {

	private List<CountryDetails> countryList;

	public List<CountryDetails> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<CountryDetails> countryList) {
		this.countryList = countryList;
	}
	
}
