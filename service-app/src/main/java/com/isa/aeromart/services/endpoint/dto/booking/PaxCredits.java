package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Collection;

public class PaxCredits {

	private Collection<CreditInfo> creditInfo;

	private int paxSequence;

	public Collection<CreditInfo> getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(Collection<CreditInfo> creditInfo) {
		this.creditInfo = creditInfo;
	}

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

}
