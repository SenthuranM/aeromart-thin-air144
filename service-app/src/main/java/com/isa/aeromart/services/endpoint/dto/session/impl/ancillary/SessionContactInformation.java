package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import com.isa.aeromart.services.endpoint.dto.common.Address;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;

public class SessionContactInformation extends SessionPerson {

	private Address address;

	private PhoneNumber landNumber;

	private PhoneNumber mobileNumber;

	private String emailAddress;

	private String country;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public PhoneNumber getLandNumber() {
		return landNumber;
	}

	public void setLandNumber(PhoneNumber landNumber) {
		this.landNumber = landNumber;
	}

	public PhoneNumber getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(PhoneNumber mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
