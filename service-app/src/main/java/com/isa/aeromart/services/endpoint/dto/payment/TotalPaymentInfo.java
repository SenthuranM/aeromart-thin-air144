package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class TotalPaymentInfo {

	private BigDecimal totalWithAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalWithoutAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAvailabilityCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPaymentCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private FareSegChargeTO pricingInformation;

	private FinancialStore financialStore;

	private TravellerQuantity travellerQuantity;

	public TotalPaymentInfo(FareSegChargeTO pricingInformation, FinancialStore financialStore, TravellerQuantity travellerQuantity) {
		this.pricingInformation = pricingInformation;
		this.financialStore = financialStore;
		this.travellerQuantity = travellerQuantity;
		buildPaymentInfo();
	}

	private void buildPaymentInfo() {

		if (pricingInformation != null && travellerQuantity != null) {
			Map<String, Double> paxWiseTotalChargesMap = pricingInformation.getPaxWiseTotalCharges();
			Map<String, Double> totalJourneyFareMap = pricingInformation.getTotalJourneyFare();

			BigDecimal totalAdult = AccelAeroCalculator.add(new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.ADULT)),
					new BigDecimal(totalJourneyFareMap.get(PaxTypeTO.ADULT)));
			totalAdult = AccelAeroCalculator.multiply(totalAdult, travellerQuantity.getAdultCount());

			BigDecimal totalChild = BigDecimal.ZERO;
			if (travellerQuantity.getChildCount() > 0) {
				totalChild = AccelAeroCalculator.add(new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.CHILD)), new BigDecimal(
						totalJourneyFareMap.get(PaxTypeTO.CHILD)));
				totalChild = AccelAeroCalculator.multiply(totalChild, travellerQuantity.getChildCount());
			}

			BigDecimal totalInfant = BigDecimal.ZERO;
			if (travellerQuantity.getInfantCount() > 0) {
				totalInfant = AccelAeroCalculator.add(new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.INFANT)),
						new BigDecimal(totalJourneyFareMap.get(PaxTypeTO.INFANT)));

				totalInfant = AccelAeroCalculator.multiply(totalInfant, travellerQuantity.getInfantCount());
			}
			totalWithoutAncillaryCharges = AccelAeroCalculator.scaleValueDefault(AccelAeroCalculator.add(totalAdult, totalChild,
					totalInfant));

			if (financialStore != null) {
				totalAncillaryCharges = financialStore.getTotalAnicillaryCharges();
				totalPaymentCharges = financialStore.getTotalPaymentCharges();
				totalAvailabilityCharges = financialStore.getTotalAvailabilityCharges();

			}

			// add totalAvailabilityCharges (flexi at availability) as payment scope doesn't need to differentiate
			totalWithAncillaryCharges = AccelAeroCalculator.scaleValueDefault(AccelAeroCalculator.add(
					totalWithoutAncillaryCharges, totalAncillaryCharges, totalAvailabilityCharges));

			totalTicketPrice = AccelAeroCalculator.scaleValueDefault(AccelAeroCalculator.add(totalWithAncillaryCharges,
					totalPaymentCharges));

		} else {
			throw new IllegalArgumentException("missing.data.to.build.payment.info");
		}
	}

	public BigDecimal getTotalWithAncillaryCharges() {
		return totalWithAncillaryCharges;
	}

	public BigDecimal getTotalWithoutAncillaryCharges() {
		return totalWithoutAncillaryCharges;
	}

	public BigDecimal getTotalAncillaryCharges() {
		return totalAncillaryCharges;
	}

	public BigDecimal getTotalPaymentCharges() {
		return totalPaymentCharges;
	}

	public BigDecimal getTotalTicketPrice() {
		return totalTicketPrice;
	}

}
