package com.isa.aeromart.services.endpoint.dto.booking;

public class AdvertisementTemplate {

	private String url;

	private boolean isLinkDynamic;

	private boolean isLinkLoadWithinFrame;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isLinkDynamic() {
		return isLinkDynamic;
	}

	public void setLinkDynamic(boolean isLinkDynamic) {
		this.isLinkDynamic = isLinkDynamic;
	}

	public boolean isLinkLoadWithinFrame() {
		return isLinkLoadWithinFrame;
	}

	public void setLinkLoadWithinFrame(boolean isLinkLoadWithinFrame) {
		this.isLinkLoadWithinFrame = isLinkLoadWithinFrame;
	}

}
