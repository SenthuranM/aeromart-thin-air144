package com.isa.aeromart.services.endpoint.dto.booking;

public class PrintItineraryRQ extends ItineraryRQ {

	private String viewMode;

	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}

}
