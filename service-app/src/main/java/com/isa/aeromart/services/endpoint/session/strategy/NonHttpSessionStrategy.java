package com.isa.aeromart.services.endpoint.session.strategy;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.session.TransactionFactory;

@Service("nonHttpSessionStrategy")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NonHttpSessionStrategy {

	public TransactionFactory getTransactionFactory(String transactionId) {
		return null;
	}
}
