package com.isa.aeromart.services.endpoint.controller.modification;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.LoadReservationRSAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRS;
import com.isa.aeromart.services.endpoint.dto.modification.CancelReservationRQ;
import com.isa.aeromart.services.endpoint.dto.modification.CancelReservationRS;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

@Controller
@RequestMapping("modification")
public class CancelReseravationController extends StatefulController {

	private static Log log = LogFactory.getLog(CancelReseravationController.class);

	@Autowired
	private BookingService bookingService;

	@ResponseBody
	@RequestMapping(value = "/cancelReservation", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			LoadReservationRS cancelReservation(@RequestBody CancelReservationRQ cancelReservationRequest)
					throws ModuleException {
		LoadReservationRS response = new CancelReservationRS();

		/**
		 * TODO : customAdultCancelCharge, customChildCancelCharge, customInfantCancelCharge, userNotes are not used in
		 * IBE. Need to refactor when doing BE
		 */
		LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO.composeCancelReservationRequest(
				cancelReservationRequest.getPnr(), null, null, null, cancelReservationRequest.getVersion(), null);

		lccClientResAlterModesTO.setGroupPnr(cancelReservationRequest.isGroupPnr());
		ModuleServiceLocator.getAirproxyReservationBD().cancelReservation(
				lccClientResAlterModesTO, getClientInfoDTO(), getTrackInfo(), false, false);
		LCCClientReservation reservation = bookingService.loadProxyReservation(cancelReservationRequest.getPnr(),
				cancelReservationRequest.isGroupPnr(), getTrackInfo(), false, false, null, getTrackInfo().getCarrierCode(),
				false);

		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		LoadReservationRSAdaptor loadReservationRSAdaptor = new LoadReservationRSAdaptor(cancelReservationRequest.isGroupPnr(),
				null, customerSessionStore.getLoggedInCustomerID(), bookingService.getGroundSegments(reservation.getSegments()),
				masterDataService.getCurrencyExchangeRate(reservation.getLastCurrencyCode()));
		response = loadReservationRSAdaptor.adapt(reservation);
		response.setSuccess(true);
		return response;
	}

	public ClientCommonInfoDTO getClientInfoDTO() {
		ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();
		clientInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		return clientInfoDTO;
	}

}
