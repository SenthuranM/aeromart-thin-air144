package com.isa.aeromart.services.endpoint.dto.ancillary;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "mode", visible = true)
@JsonSubTypes({ @JsonSubTypes.Type(value = BasicReservation.class, name = ReservationData.Mode.CREATE),
		@JsonSubTypes.Type(value = ModifyReservation.class, name = ReservationData.Mode.MODIFY) })
public abstract class ReservationData {

	public interface Mode {
		String CREATE = "CREATE";
		String MODIFY = "MODIFY";
	}

	private String mode;

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

}
