package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;

public class SessionSegmentAdaptor implements Adaptor<SessionFlightSegment, LCCClientReservationSegment> {

	private Integer segmentSequence;
	@Override
	public LCCClientReservationSegment adapt(SessionFlightSegment sessionFlightSegment) {

		LCCClientReservationSegment lccReservationSegment = new LCCClientReservationSegment();
		lccReservationSegment.setArrivalDate(sessionFlightSegment.getArrivalDateTime());
		lccReservationSegment.setArrivalDateZulu(sessionFlightSegment.getArrivalDateTimeZulu());
		lccReservationSegment.setBookingFlightSegmentRefNumber(sessionFlightSegment.getLCCFlightReference());
		lccReservationSegment.setCabinClassCode(sessionFlightSegment.getCabinClass());
		lccReservationSegment.setDepartureDate(sessionFlightSegment.getDepartureDateTime());
		lccReservationSegment.setDepartureDateZulu(sessionFlightSegment.getDepartureDateTimeZulu());
		lccReservationSegment.setFlightNo(sessionFlightSegment.getFlightDesignator());
		lccReservationSegment.setFlightSegmentRefNumber(sessionFlightSegment.getFlightSegmentId().toString());
		lccReservationSegment.setLogicalCabinClass(sessionFlightSegment.getLogicalCabinClass());
		lccReservationSegment.setSegmentCode(sessionFlightSegment.getSegmentCode());
		lccReservationSegment.setSegmentSeq(segmentSequence);
		lccReservationSegment.setBaggageONDGroupId(sessionFlightSegment.getBaggageONDGroupId());
		lccReservationSegment.setStatus("CNF");
		lccReservationSegment.setReturnFlag("N");

		return lccReservationSegment;
	}

	public Integer getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(Integer segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

}
