package com.isa.aeromart.services.endpoint.adaptor;

public interface Adaptor<S, T> {

	public T adapt(S source);
}
