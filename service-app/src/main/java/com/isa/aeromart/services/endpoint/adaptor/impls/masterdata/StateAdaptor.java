package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.dto.masterdata.StateDetails;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airmaster.api.model.State;


public class StateAdaptor implements Adaptor<State, StateDetails>{

	@Override
	public StateDetails adapt(State source) {
		
		StateDetails stateDetails = new StateDetails();
		
		stateDetails.setStateCode(source.getStateCode());
		stateDetails.setStateName(source.getStateName());
		
		return stateDetails;
	}

}