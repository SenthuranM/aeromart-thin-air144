package com.isa.aeromart.services.endpoint.delegate.booking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.booking.ItineraryRQ;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRQ;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationValidateRes;
import com.isa.aeromart.services.endpoint.dto.booking.TaxInvoiceRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.booking.BookingUtil;
import com.isa.aeromart.services.endpoint.utils.common.LableServiceUtil;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAdminInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.CustomizedItineraryUtil;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Service("bookingService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BookingServiceImpl implements BookingService {

	private static Log log = LogFactory.getLog(BookingServiceImpl.class);

	private static final String ACTION_PRINT = "PRINT";

	private static final String ACTION_EMAIL = "EMAIL";

	@Override
	public BookingRS createBooking(CommonReservationAssembler commonReservationAssembler,
			AncillaryIntegrateTO ancillaryIntegrateTo, TrackInfoDTO trackInfoDTO) {

		BookingRS bookingRS = new BookingRS();

		try {
			ServiceResponce responce = ModuleServiceLocator.getAirproxyReservationBD().makeReservation(
					commonReservationAssembler, trackInfoDTO);
			LCCClientReservation commonReservation = (LCCClientReservation) responce
					.getResponseParam(CommandParamNames.RESERVATION);
			bookingRS.setPnr(commonReservation.getPNR());
			bookingRS.setSuccess(true);

			sendEmail(commonReservation.getPNR(), commonReservation.isGroupPNR(), commonReservation.getContactInfo(),
					commonReservation, trackInfoDTO);
		} catch (ModuleException me) {
			String language = LocaleContextHolder.getLocale().getLanguage();
			String errorMsg = LableServiceUtil.getMessage(me.getExceptionCode(), language);
			bookingRS.setSuccess(false);
			bookingRS.addError(errorMsg);
			bookingRS.getMessages().add(errorMsg);
			log.error("CreateReservation ==> error in create reservation.", me);
		} catch (Exception e) {
			bookingRS.setSuccess(false);
			log.error("CreateReservation ==> error in create reservation.", e);
		}

		return bookingRS;

	}

	@Override
	public LCCClientReservation balancePayment(BookingSessionStore bookingSessionStore, TrackInfoDTO trackInfoDTO,
			String customerID) throws Exception {

		boolean isGroupPNR = ProxyConstants.SYSTEM.getEnum(bookingSessionStore.getPreferenceInfoForBooking().getSelectedSystem()) == SYSTEM.INT;

		PostPaymentDTO postPaymentDTO = bookingSessionStore.getPostPaymentInformation();
		String pnr = bookingSessionStore.getPnr();

		LCCClientReservation lccReservation = this.loadProxyReservation(pnr, isGroupPNR, trackInfoDTO, true,
				(customerID != null && !customerID.isEmpty()), null, null, false);

		@SuppressWarnings("unchecked")
		Map<Integer, CommonCreditCardPaymentInfo> mapTnxIds = postPaymentDTO.getTemporyPaymentMap();

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		if (postPaymentDTO != null && postPaymentDTO.getIpgRequestDTO() != null
				&& postPaymentDTO.getIpgRequestDTO().getAmount() != null) {
			ipgIdentificationParamsDTO = postPaymentDTO.getIpgRequestDTO().getIpgIdentificationParamsDTO();
		}

		LCCClientBalancePayment balancePayment = BookingUtil.getBalancePayment(ipgIdentificationParamsDTO, lccReservation,
				bookingSessionStore, mapTnxIds);

		ClientCommonInfoDTO clientCommonInfoDTO = new ClientCommonInfoDTO();


		ServiceResponce responce = ModuleServiceLocator.getAirproxyReservationBD().balancePayment(balancePayment,
				lccReservation.getVersion(), isGroupPNR,
				AppSysParamsUtil.isFraudCheckEnabled(trackInfoDTO.getOriginChannelId()), clientCommonInfoDTO, trackInfoDTO,
				false, false);
		lccReservation = (LCCClientReservation) responce.getResponseParam(CommandParamNames.RESERVATION);

		sendEmail(pnr, isGroupPNR, lccReservation.getContactInfo(), lccReservation, trackInfoDTO);

		// send medical ssr email
		SSRServicesUtil.sendMedicalSsrEmail(lccReservation);

		return lccReservation;
	}

	@Override
	public LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR, TrackInfoDTO trackInfo, boolean recordAudit,
			boolean isRegisteredUser, String airlineCode, String marketingAirlineCode, boolean skipPromoAdjustment)
			throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = BookingUtil.getPnrModesDTO(pnr, isGroupPNR, recordAudit, airlineCode,
				marketingAirlineCode, skipPromoAdjustment);

		ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
		modificationParamRQInfo.setAppIndicator(AppIndicatorEnum.APP_IBE);
		modificationParamRQInfo.setIsRegisteredUser(isRegisteredUser);

		if (log.isDebugEnabled())
			log.debug(" Search reservation initiated for loadProxyReservation. " + pnrModesDTO.toString());
		return ModuleServiceLocator.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, modificationParamRQInfo,
				trackInfo);
	}

	@Override
	public LoadReservationValidateRes validateLoadReservationRQ(LoadReservationRQ loadReservationRQ, TrackInfoDTO trackInfoDTO,
			String customerID) {

		LoadReservationValidateRes loadReservationValidateRes = new LoadReservationValidateRes();
		loadReservationValidateRes.setValidLoadRequest(true);
		loadReservationValidateRes.setSuccess(true);
		loadReservationValidateRes.setGroupPnr(false);

		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();

		reservationSearchDTO.setPnr(loadReservationRQ.getPnr());
		reservationSearchDTO.setLastName(loadReservationRQ.getLastName());
		reservationSearchDTO.setDepartureDate(loadReservationRQ.getDepartureDateTime());

		// with dry reservation changes we can't guarantee where the reservation is going to be.
		// so we have to do a search all operation if LCC connectivity is enabled.
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			reservationSearchDTO.setSearchAll(true);
		} else {
			reservationSearchDTO.setSearchAll(false);
		}

		try {
			Collection<ReservationListTO> reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(
					reservationSearchDTO, null, trackInfoDTO);

			boolean customerIDUsed = false;

			if (customerID != null && !customerID.isEmpty() && loadReservationRQ.isValidateRQ()) {
				customerIDUsed = this.isCustomerIDUsed(loadReservationRQ.getPnr(), customerID, trackInfoDTO);
			}

			if (reservations.isEmpty()) {
				throw new ValidationException(ValidationException.Code.INVALID_COMBINATION);
			} else {

				if (reservations.size() > 1) {
					throw new ValidationException(ValidationException.Code.INVALID_COMBINATION);
				} else {
					validateResLoadRQ(loadReservationRQ, loadReservationValidateRes, reservations, customerIDUsed);
				}
			}
		} catch (ModuleException | ParseException e) {
			loadReservationValidateRes.setValidLoadRequest(false);
			loadReservationValidateRes.setSuccess(false);
			log.error("Load reservation request validation failed ==> BookingService", e);
		}
		return loadReservationValidateRes;
	}

	private void validateResLoadRQ(LoadReservationRQ loadReservationRQ, LoadReservationValidateRes loadReservationValidateRes,
			Collection<ReservationListTO> reservations, boolean customerIDUsed) throws ModuleException {
		ReservationListTO reservationItem = BeanUtils.getFirstElement(reservations);
		if (!customerIDUsed) {
			String foundLastName = reservationItem.getPaxName().split(",")[1].trim();
			if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservationItem.getPnrStatus())) {
				throw new ValidationException(ValidationException.Code.CNX_RES_LOAD);

			} else if (!isValidBookingData(reservationItem, loadReservationRQ.getDepartureDateTime(), loadReservationRQ.getPnr(),
					loadReservationRQ.isValidateRQ())) {
				throw new ValidationException(ValidationException.Code.INVALID_COMBINATION);

			} else if (loadReservationRQ.isValidateRQ()) {

				validateLoadRequestLastName(loadReservationRQ, loadReservationValidateRes, reservationItem, foundLastName);
			}

			loadReservationValidateRes.setAirlineCode(reservationItem.getAirlineCode());
			loadReservationValidateRes.setSellingAirlineCode(reservationItem.getMarketingAirlineCode());
		}
		String groupPnr = extractGroupPNR(reservationItem);
		if (("").equals(groupPnr) || groupPnr == null) {
			loadReservationValidateRes.setGroupPnr(false);
		} else {
			loadReservationValidateRes.setGroupPnr(true);
		}
	}

	private void validateLoadRequestLastName(LoadReservationRQ loadReservationRQ,
			LoadReservationValidateRes loadReservationValidateRes, ReservationListTO reservationItem, String foundLastName)
			throws ModuleException {
		boolean valid = loadReservationRQ.getLastName().equalsIgnoreCase(foundLastName);

		if (loadReservationRQ.isValidateRQ() && AppSysParamsUtil.isAllowLoadReservationsUsingPaxNameInIBE()) {
			valid = false;
			LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
			modes.setPnr(reservationItem.getPnrNo());
			Reservation reservation = ModuleServiceLocator.getReservationBD().getReservation(modes, null);
			// Allow load reservation using passenger last name.
			for (ReservationPax pax : reservation.getPassengers()) {
				if (pax.getLastName().equalsIgnoreCase(loadReservationRQ.getLastName())) {
					valid = true;
					break;
				}
			}
		}

		if (!valid) {
			throw new ValidationException(ValidationException.Code.INVALID_COMBINATION);
		}

		loadReservationValidateRes.setValidLoadRequest(valid);
	}

	public boolean isCustomerIDUsed(String pnr, String customerID, TrackInfoDTO trackInfoDTO) throws ModuleException, ValidationException {
		boolean customerIDUsed = false;
		Date nowDate = new Date();
		Collection<ReservationDTO> reservationDTOs = ModuleServiceLocator.getReservationQueryBD().getAfterReservations(
				Integer.valueOf(customerID), nowDate, null, trackInfoDTO);
		reservationDTOs.addAll(ModuleServiceLocator.getReservationQueryBD().getEarlyReservations(Integer.valueOf(customerID),
				nowDate, null, trackInfoDTO));
		for (ReservationDTO reservation : reservationDTOs) {
			if (reservation.getPnr().equals(pnr)) {
				customerIDUsed = true;
				break;
			}
		}
		return customerIDUsed;
	}

	public void sendItineraryEmail(ItineraryRQ printItineraryRQ, TrackInfoDTO trackInfo) throws ModuleException {
		performItineraryAction(printItineraryRQ, trackInfo, ACTION_EMAIL);
	}

	public String getItineraryInfo(ItineraryRQ printItineraryRQ, TrackInfoDTO trackInfo) throws ModuleException {
		return performItineraryAction(printItineraryRQ, trackInfo, ACTION_PRINT);
	}

	public String getTaxInvoiceInfo(TaxInvoiceRQ printTaxInvoiceRQ, TrackInfoDTO trackInfo) throws ModuleException {
		return performTaxInvoiceAction(printTaxInvoiceRQ, trackInfo, ACTION_PRINT);
	}
	
	public List<TaxInvoice> getTaxInvoiceList(TaxInvoiceRQ printTaxInvoiceRQ, TrackInfoDTO trackInfo) throws ModuleException {
		return getTaxInvoicesList(printTaxInvoiceRQ, trackInfo, ACTION_PRINT);
	}

	public boolean isTaxInvoiceAvailable(LoadReservationRQ loadReservationRQ) throws ModuleException {
		boolean isTaxInvoiceAvailable = false;
		String pnr = loadReservationRQ.getPnr();
		List<TaxInvoice> taxInvoices = ModuleServiceLocator.getReservationBD().getTaxInvoiceForPrintInvoice(pnr);
		if (taxInvoices != null && !taxInvoices.isEmpty()) {
			isTaxInvoiceAvailable = true;
		}
		return isTaxInvoiceAvailable;
	}

	public String performItineraryAction(ItineraryRQ printItineraryRQ, TrackInfoDTO trackInfo, String action)
			throws ModuleException {

		CommonItineraryParamDTO commonItineraryParam = getItineraryParams(printItineraryRQ);
		LCCClientPnrModesDTO pnrModesDTO = BookingUtil.getPnrModesDTO(printItineraryRQ.getPnr(), printItineraryRQ.isGroupPnr(),
				false, null, null, false);

		String itinerary = "";
		CommonItineraryParamDTO commonItineraryParamDTO = null;
		List<CommonItineraryParamDTO> itineraryDtoList = null;
		if (isShowCustomizedItinerary()) {
			// TODO: FIXME : For this, the back end needs to be refactored UserPrincipal would be enough for this
			itineraryDtoList = CustomizedItineraryUtil.itineraryCompose(getDummyHttpRQ(), action, commonItineraryParam, null,
					pnrModesDTO);
		} else {
			commonItineraryParamDTO = commonItineraryParam;
		}

		if (action.equals(ACTION_PRINT)) {
			if (itineraryDtoList != null) {
				for (CommonItineraryParamDTO itineraryParamDTO : itineraryDtoList) {
					itinerary = getItinerary(itineraryParamDTO, pnrModesDTO, trackInfo, false, "");
				}
			} else {
				itinerary = getItinerary(commonItineraryParamDTO, pnrModesDTO, trackInfo, false, "");
			}
		} else if (action.equals(ACTION_EMAIL)) {
			if (itineraryDtoList != null) {
				for (CommonItineraryParamDTO itineraryParamDTO : itineraryDtoList) {
					sendItinerary(itineraryParamDTO, pnrModesDTO, trackInfo, false, "");
				}
			} else {
				sendItinerary(commonItineraryParamDTO, pnrModesDTO, trackInfo, false, "");
			}
		}

		return itinerary;
	}

	public String performTaxInvoiceAction(TaxInvoiceRQ printTaxInvoiceRQ, TrackInfoDTO trackInfo, String action)
			throws ModuleException {
		String taxInvoiceHTML = "";
		String pnr = printTaxInvoiceRQ.getPnr();
		String selectedInvoiceIdsStr = printTaxInvoiceRQ.getSelectedInvoiceIdsStr();
		boolean isGroupPNR = printTaxInvoiceRQ.isGroupPnr();

		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPNR, trackInfo, false, false, null,
				trackInfo.getCarrierCode(), false);

		List<TaxInvoice> taxInvoices = new ArrayList<TaxInvoice>();
		taxInvoices = getTaxInvoiceByPnr(pnr, selectedInvoiceIdsStr, reservation);

		if (action.equals(ACTION_PRINT))
			taxInvoiceHTML = getTaxInvoiceInfo(taxInvoices, reservation);
		return taxInvoiceHTML;
	}
	
	public List<TaxInvoice> getTaxInvoiceByPnr(String pnr, String taxInvoiceNumbers, LCCClientReservation reservation)
			throws ModuleException {
		List<String> selectedInvoiceNumbersStrList = Arrays.asList(taxInvoiceNumbers.split("\\s*,\\s*"));
		List<TaxInvoice> selectedTaxInvoicesList = getSelectedTaxInvoicesList(selectedInvoiceNumbersStrList,
				reservation.getTaxInvoicesList());
		return selectedTaxInvoicesList;
	}
	
	private List<TaxInvoice> getSelectedTaxInvoicesList(List<String> selectedInvoiceNumbersStrList,
			List<TaxInvoice> allTaxInvoicesList) {
		List<TaxInvoice> selectedTaxInvoices = new ArrayList<>();
		for (String taxInvoiceNum : selectedInvoiceNumbersStrList) {
			for (TaxInvoice taxInvoice : allTaxInvoicesList) {
				if (taxInvoiceNum.equals(taxInvoice.getTaxInvoiceId().toString())) {
					selectedTaxInvoices.add(taxInvoice);
				}
			}
		}
		return selectedTaxInvoices;
	}
	
	public List<TaxInvoice> getTaxInvoicesList(TaxInvoiceRQ printTaxInvoiceRQ, TrackInfoDTO trackInfo, String action)
			throws ModuleException {
		String pnr = printTaxInvoiceRQ.getPnr();
		boolean isGroupPNR = printTaxInvoiceRQ.isGroupPnr();

		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPNR, trackInfo, false, false, null,
				trackInfo.getCarrierCode(), false);

		return reservation.getTaxInvoicesList();
	}
	
	private String getTaxInvoiceInfo(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation) throws ModuleException {
		String taxInvoiceHtml = "";
		taxInvoiceHtml = getTaxInvoice(taxInvoicesList, reservation);
		return taxInvoiceHtml;
		
	}
	
	private String getTaxInvoice(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation) throws ModuleException {
		String taxInvoice = "";
		taxInvoice += ModuleServiceLocator.getReservationBD().getTaxInvoiceForPrint(taxInvoicesList, reservation);
		return taxInvoice;
	}

	public String getTaxInvoiceForPrint(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation)
			throws ModuleException {
		String taxInvoice = "";
		taxInvoice += ModuleServiceLocator.getReservationBD().getTaxInvoiceForPrint(taxInvoicesList, reservation);
		return taxInvoice;
	}

	private CommonItineraryParamDTO getItineraryParams(ItineraryRQ printItineraryRQ) throws ModuleException {

		String pnr = printItineraryRQ.getPnr();
		String station = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		boolean includeBaggageAllowance = false;
		boolean isbaggageEnable = false;
		boolean isbaggageAllowanceRemarksEnable = false;

		LCCClientPnrModesDTO pnrModesDTO = BookingUtil.getPnrModesDTO(pnr, printItineraryRQ.isGroupPnr(), false, null, null,
				false);
		/*
		 * Need the origin country for itinerary. It is necessary to determine whether additional charge details will be
		 * printed based on the country's configuration.
		 */
		pnrModesDTO.setLoadOriginCountry(true);

		String strBaggageAllowanceRemarks = ModuleServiceLocator.getGlobalConfig().getBizParam(
				SystemParamKeys.SHOW_BAGGAGE_ALLOWANCE_REMARKS);
		String strBaggage = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE);

		if (strBaggageAllowanceRemarks != null && ("Y").equals(strBaggageAllowanceRemarks.trim())) {
			isbaggageAllowanceRemarksEnable = true;
		}
		if (strBaggage != null && ("Y").equals(strBaggage.trim())) {
			isbaggageEnable = true;
		}
		if (isbaggageAllowanceRemarksEnable && !isbaggageEnable) {
			includeBaggageAllowance = true;
		}

		CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
		commonItineraryParam.setItineraryLanguage(printItineraryRQ.getItineraryLanguage());
		commonItineraryParam.setIncludePaxFinancials(true);
		commonItineraryParam.setIncludePaymentDetails(true);
		if (AppSysParamsUtil.showChargesPaymentsInItinerary()) {
			commonItineraryParam.setIncludeTicketCharges(true);
		} else {
			commonItineraryParam.setIncludeTicketCharges(false);
		}
		commonItineraryParam.setIncludeBaggageAllowance(includeBaggageAllowance);
		commonItineraryParam.setStation(station);
		commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
		commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(printItineraryRQ.isGroupPnr()));
		commonItineraryParam.setIncludeTermsAndConditions(true);
		commonItineraryParam.setIncludeStationContactDetails(AppSysParamsUtil.showStationContactDetailsInItinerary());
		commonItineraryParam.setOperationType(printItineraryRQ.getOperationType());

		return commonItineraryParam;
	}

	public String getItinerary(CommonItineraryParamDTO commonItineraryParam, LCCClientPnrModesDTO pnrModesDTO,
			TrackInfoDTO trackInfoDTO, boolean individualPax, String selectedPax) throws ModuleException {
		StringBuilder itineraryBuilder = new StringBuilder();
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(pnrModesDTO.getPnr());
		if (individualPax) {
			String paxs[] = selectedPax.split(",");
			for (String pax : paxs) {
				if (pax != null || !("").equals(pax)) {
					commonItineraryParam.setSelectedPaxDetails(pax);
					itineraryBuilder.append(ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
							commonItineraryParam, reservationSearchDTO, trackInfoDTO));
				}
			}
		} else {
			commonItineraryParam.setSelectedPaxDetails(selectedPax);
			itineraryBuilder.append(ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
					commonItineraryParam, reservationSearchDTO, trackInfoDTO));
		}

		return itineraryBuilder.toString();
	}

	public String getTaxInvoice(CommonItineraryParamDTO commonItineraryParam, LCCClientPnrModesDTO pnrModesDTO,
			TrackInfoDTO trackInfoDTO, boolean individualPax, String selectedPax) throws ModuleException {
		StringBuilder itineraryBuilder = new StringBuilder();
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(pnrModesDTO.getPnr());
		if (individualPax) {
			String paxs[] = selectedPax.split(",");
			for (String pax : paxs) {
				if (pax != null || !("").equals(pax)) {
					commonItineraryParam.setSelectedPaxDetails(pax);
					itineraryBuilder.append(ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
							commonItineraryParam, reservationSearchDTO, trackInfoDTO));
				}
			}
		} else {
			commonItineraryParam.setSelectedPaxDetails(selectedPax);
			itineraryBuilder.append(ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
					commonItineraryParam, reservationSearchDTO, trackInfoDTO));
		}

		return itineraryBuilder.toString();
	}

	public void sendItinerary(CommonItineraryParamDTO commonItineraryParam, LCCClientPnrModesDTO pnrModesDTO,
			TrackInfoDTO trackInfoDTO, boolean individualPax, String selectedPax) throws ModuleException {

		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(pnrModesDTO.getPnr());
		if (individualPax) {
			String paxs[] = selectedPax.split(",");
			for (String pax : paxs) {
				if (pax != null || !("").equals(pax)) {
					commonItineraryParam.setSelectedPaxDetails(pax);
					ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
							trackInfoDTO);
				}
			}
		} else {
			commonItineraryParam.setSelectedPaxDetails(selectedPax);
			ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam, trackInfoDTO);
		}
	}

	public boolean isShowCustomizedItinerary() {
		return AppSysParamsUtil.getShowCustomizedItinerary();

	}

	private void sendEmail(String pnr, boolean isGroupPnr, CommonReservationContactInfo contactInfo,
			LCCClientReservation commonReservation, TrackInfoDTO trackInfoDTO) throws ModuleException {
		// Email Itinerary
		try {
			if (commonReservation.getStatus() != null
					&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(commonReservation.getStatus())
					&& BeanUtils.nullHandler(contactInfo.getEmail()).length() > 0) {
				String station = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);

				LCCClientPnrModesDTO pnrModesDTO = BookingUtil.getPnrModesDTO(pnr, isGroupPnr, false, null, null, false);

				/*
				 * Need to set this to true to load the originCountry.
				 */
				pnrModesDTO.setLoadOriginCountry(true);

				CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
				commonItineraryParam.setItineraryLanguage(contactInfo.getPreferredLanguage());
				commonItineraryParam.setIncludePaxFinancials(true);
				commonItineraryParam.setIncludePaymentDetails(true);
				commonItineraryParam.setIncludeTicketCharges(false);
				commonItineraryParam.setIncludeTermsAndConditions(true);
				commonItineraryParam.setStation(station);
				commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
				commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(isGroupPnr));
				commonItineraryParam.setIncludePaxContactDetails(AppSysParamsUtil.showPassenegerContactDetailsInItinerary());
				commonItineraryParam.setIncludeStationContactDetails(AppSysParamsUtil.showStationContactDetailsInItinerary());

				if (AppSysParamsUtil.isPrintEmailIndItinerary()) {
					String selectedPax = "";
					int nonInfants = commonReservation.getTotalPaxAdultCount() + commonReservation.getTotalPaxChildCount();
					for (int i = 1; i <= nonInfants; i++)
						selectedPax += i + ",";

					String paxs[] = selectedPax.split(",");
					for (String pax : paxs) {
						if (pax != null || !("").equals(pax)) {
							commonItineraryParam.setSelectedPaxDetails(pax);
							ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
									trackInfoDTO);
						}
					}
				} else {
					ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
							trackInfoDTO);
				}
			}

		} catch (Exception e) {
			log.error("Error while trying to send itinerary by email after payment", e);
		}
	}

	private boolean isValidBookingData(ReservationListTO reservationListTO, Date depDate, String pnr, boolean validate) {
		boolean isValid = false;
		if (validate) {
			if (reservationListTO != null && reservationListTO.getPnrNo().equals(pnr)) {

				List<FlightInfoTO> flightInfo = new ArrayList<>(reservationListTO.getFlightInfo());
				isValid = validateDepartureDate(depDate, flightInfo);
			}
		} else {
			isValid = true;
		}
		return isValid;
	}

	private boolean validateDepartureDate(Date depDate, List<FlightInfoTO> flightInfo) {
		Collections.sort(flightInfo);
		boolean isValid = false;
		for (FlightInfoTO flightInfoTO : flightInfo) {
			if (flightInfoTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				Date flightDepartureDate = new Date(flightInfoTO.getDepartureDateLong());
				SimpleDateFormat departureDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				if (flightDepartureDate.compareTo(CalendarUtil.getCurrentSystemTimeInZulu()) > 0 && flightInfo.size() > 1) {
					if (departureDateFormat.format(flightDepartureDate).equals(departureDateFormat.format(depDate))) {
						isValid = true;
					} else {
						isValid = false;
					}
					break;
				} else {
					if (departureDateFormat.format(flightDepartureDate).equals(departureDateFormat.format(depDate))) {
						isValid = true;
						break;
					}
				}
			}
		}
		return isValid;
	}

	private String extractGroupPNR(ReservationListTO reservationItem) {
		String groupPNR = "";
		groupPNR = reservationItem.getOriginatorPnr();
		if (StringUtils.isEmpty(groupPNR)) {
			// if this condition is true, it implies that the reservation is a dry reservation.
			if (StringUtils.isNotEmpty(reservationItem.getAirlineCode())
					&& StringUtils.isNotEmpty(reservationItem.getMarketingAirlineCode())) {
				groupPNR = reservationItem.getPnrNo();
			}
		}
		return groupPNR;
	}

	private HttpServletRequest getDummyHttpRQ() {
		return new HttpServletRequest() {
			@Override
			public void setCharacterEncoding(String arg0) throws UnsupportedEncodingException {
			}

			@Override
			public void setAttribute(String arg0, Object arg1) {
			}

			@Override
			public void removeAttribute(String arg0) {
			}

			@Override
			public boolean isSecure() {
				return false;
			}

			@Override
			public int getServerPort() {
				return 0;
			}

			@Override
			public String getServerName() {
				return null;
			}

			@Override
			public String getScheme() {
				return null;
			}

			@Override
			public RequestDispatcher getRequestDispatcher(String arg0) {
				return null;
			}

			@Override
			public String getRemoteHost() {
				return null;
			}

			@Override
			public String getRemoteAddr() {
				return null;
			}

			@Override
			public String getRealPath(String arg0) {
				return null;
			}

			@Override
			public BufferedReader getReader() throws IOException {
				return null;
			}

			@Override
			public String getProtocol() {
				return null;
			}

			@Override
			public String[] getParameterValues(String arg0) {
				return null;
			}

			@Override
			public Enumeration getParameterNames() {
				return null;
			}

			@Override
			public Map getParameterMap() {
				return null;
			}

			@Override
			public String getParameter(String arg0) {
				return null;
			}

			@Override
			public Enumeration getLocales() {
				return null;
			}

			@Override
			public Locale getLocale() {
				return null;
			}

			@Override
			public ServletInputStream getInputStream() throws IOException {
				return null;
			}

			@Override
			public String getContentType() {
				return null;
			}

			@Override
			public int getContentLength() {
				return 0;
			}

			@Override
			public String getCharacterEncoding() {
				return null;
			}

			@Override
			public Enumeration getAttributeNames() {
				return null;
			}

			@Override
			public Object getAttribute(String arg0) {
				return null;
			}

			@Override
			public boolean isUserInRole(String arg0) {
				return false;
			}

			@Override
			public boolean isRequestedSessionIdValid() {
				return false;
			}

			@Override
			public boolean isRequestedSessionIdFromUrl() {
				return false;
			}

			@Override
			public boolean isRequestedSessionIdFromURL() {
				return false;
			}

			@Override
			public boolean isRequestedSessionIdFromCookie() {
				return false;
			}

			@Override
			public Principal getUserPrincipal() {
				return null;
			}

			@Override
			public HttpSession getSession(boolean arg0) {
				return null;
			}

			@Override
			public HttpSession getSession() {
				return null;
			}

			@Override
			public String getServletPath() {
				return null;
			}

			@Override
			public String getRequestedSessionId() {
				return null;
			}

			@Override
			public StringBuffer getRequestURL() {
				return null;
			}

			@Override
			public String getRequestURI() {
				return null;
			}

			@Override
			public String getRemoteUser() {
				return null;
			}

			@Override
			public String getQueryString() {
				return null;
			}

			@Override
			public String getPathTranslated() {
				return null;
			}

			@Override
			public String getPathInfo() {
				return null;
			}

			@Override
			public String getMethod() {
				return null;
			}

			@Override
			public int getIntHeader(String arg0) {
				return 0;
			}

			@Override
			public Enumeration getHeaders(String arg0) {
				return null;
			}

			@Override
			public Enumeration getHeaderNames() {
				return null;
			}

			@Override
			public String getHeader(String arg0) {
				return null;
			}

			@Override
			public long getDateHeader(String arg0) {
				return 0;
			}

			@Override
			public Cookie[] getCookies() {
				return null;
			}

			@Override
			public String getContextPath() {
				return null;
			}

			@Override
			public String getAuthType() {
				return null;
			}
		};
	}

	@Override
	public void checkLoadingConstraint(LCCClientReservation reservation) throws ValidationException {
		boolean isError = false;

		if ((reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount()) > AppSysParamsUtil
				.getMaxChildAdultCount()) {
			isError = true;
		}

		for (LCCClientReservationSegment segment : reservation.getSegments()) {
			if (segment.isOpenReturnSegment()) {
				isError = true;
				break;
			}
		}
		CommonReservationAdminInfo adminInfo = reservation.getAdminInfo();
		if (AppSysParamsUtil.isIBEModificationRestrictionEnabled() && adminInfo != null && adminInfo.getOriginChannelId() != null
				&& !("").equals(adminInfo.getOriginChannelId())) {

			/*
			 * Check if only one airline is participating in the reservation. If this is true it's either a dry
			 * reservation or an normal one.
			 */
			isError = isAllowToWebModification(adminInfo);
		}

		if (isError) {
			throw new ValidationException(ValidationException.Code.BOOKING_NOT_ALLOWED_TO_MODIFY_THROUGH_WEB);
		}

	}

	private boolean isAllowToWebModification(CommonReservationAdminInfo adminInfo) {

		boolean allowToWebModify = false;

		String AIRLINE_SEPERATOR = ",";
		String CARRIER_SEPERATOR = "|";
		String LCC_CHANNEL_CODE = String.valueOf(ReservationInternalConstants.SalesChannel.LCC);

		if (adminInfo.getOriginChannelId().lastIndexOf(AIRLINE_SEPERATOR) == -1) {

			/*
			 * Check if the carrier code is present in the channel id. If it's not then it's a normal reservation.
			 * Otherwise it's a dry reservation.
			 */
			if (adminInfo.getOriginChannelId().lastIndexOf(CARRIER_SEPERATOR) == -1) {
				if (!AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(adminInfo.getOriginChannelId())) {
					allowToWebModify = true;
				}
			} else {
				String[] channelCode = StringUtils.split(adminInfo.getOriginChannelId(), CARRIER_SEPERATOR);
				if (!AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(channelCode[1])) {
					allowToWebModify = true;
				} else if (LCC_CHANNEL_CODE.equals(channelCode[1]) && !AppSysParamsUtil.isIBEModificationAllowedForDry()) {
					allowToWebModify = true;
				}
			}
		} else {
			boolean isOneCarrierOwn = false;
			boolean isViaLCC = false;
			for (String code : adminInfo.getOriginChannelId().split(AIRLINE_SEPERATOR)) {
				String[] channelCode = StringUtils.split(code, CARRIER_SEPERATOR);

				if (LCC_CHANNEL_CODE.equals(channelCode[1])) {
					isViaLCC = true;
				}

				if (channelCode[0].equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					isOneCarrierOwn = true;
				} else if (channelCode[0].equals(AppSysParamsUtil.getDefaultCarrierCode())
						&& !AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(channelCode[1])) {
					allowToWebModify = true;
					isOneCarrierOwn = true;
					break;
				}
			}

			/*
			 * If the reservation is done through LCC then check if it has at least one own carrier segment, if it has
			 * that then the reservation is an interline reservation.
			 * 
			 * Else it's a Dry reservation.
			 */
			if (isViaLCC && isOneCarrierOwn && !AppSysParamsUtil.isIBEModificationAllowedForInterline()) {
				allowToWebModify = true;
			} else if (isViaLCC && !AppSysParamsUtil.isIBEModificationAllowedForDry()) {
				allowToWebModify = true;
			}
		}
		return allowToWebModify;
	}

	public Map<String, Boolean> getGroundSegments(Set<LCCClientReservationSegment> segments) throws ModuleException {
		Map<String, Boolean> groundSegMap = new HashMap<>();
		for (LCCClientReservationSegment segment : segments) {
			groundSegMap.put(segment.getFlightSegmentRefNumber(),
					ModuleServiceLocator.getAirportBD().isBusSegment(segment.getSegmentCode()));
		}
		return groundSegMap;
	}

}