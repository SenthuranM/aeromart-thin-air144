package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;


public class LMSValidationResponse extends BaseRS{
	
	private boolean accountExists = false;

	private boolean lmsNameMismatch = false;
	
	private boolean existingFamilyHeadEmail = false;

	private boolean existingRefferdEmail = false;

	public boolean isAccountExists() {
		return accountExists;
	}

	public void setAccountExists(boolean accountExists) {
		this.accountExists = accountExists;
	}

	public boolean isLmsNameMismatch() {
		return lmsNameMismatch;
	}

	public void setLmsNameMismatch(boolean lmsNameMismatch) {
		this.lmsNameMismatch = lmsNameMismatch;
	}

	public boolean isExistingFamilyHeadEmail() {
		return existingFamilyHeadEmail;
	}

	public void setExistingFamilyHeadEmail(boolean existingFamilyHeadEmail) {
		this.existingFamilyHeadEmail = existingFamilyHeadEmail;
	}

	public boolean isExistingRefferdEmail() {
		return existingRefferdEmail;
	}

	public void setExistingRefferdEmail(boolean existingRefferdEmail) {
		this.existingRefferdEmail = existingRefferdEmail;
	}

}
