package com.isa.aeromart.services.endpoint.dto.common;

import java.util.ArrayList;
import java.util.List;

public class BaseRS {

	private boolean success;
	private List<String> messages;
	private List<String> errors;
	private List<String> warnings;

	public BaseRS() {
		super();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<String> getMessages() {
		if (this.messages == null) {
			this.messages = new ArrayList<>();
		}
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public void addMessage(String message) {
		getMessages().add(message);
	}

	public void addError(String error) {
		getErrors().add(error);
	}

	public List<String> getErrors() {
		if (errors == null) {
			errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getWarnings() {
		if (this.warnings == null) {
			this.warnings = new ArrayList<>();
		}
		return warnings;
	}

	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}

}