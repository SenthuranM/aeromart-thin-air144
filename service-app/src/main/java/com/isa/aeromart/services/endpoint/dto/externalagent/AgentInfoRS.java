package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class AgentInfoRS extends BaseRS {

	private ExternalAgentDTO externalAgentDTO;

	public void setAgent(ExternalAgentDTO externalAgentDTO) {
		this.externalAgentDTO = externalAgentDTO;
	}

	public ExternalAgentDTO getExternalAgentDTO() {
		return externalAgentDTO;
	}

	public void setExternalAgentDTO(ExternalAgentDTO externalAgentDTO) {
		this.externalAgentDTO = externalAgentDTO;
	}

}
