package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.FareRule;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FareRuleRSAdaptor implements Adaptor<FareRuleDTO, FareRule> {

	@Override
	public FareRule adapt(FareRuleDTO source) {
		FareRule target = new FareRule();
		target.setBookingClass(source.getBookingClassCode());
		target.setOndSequence(source.getOndSequence());
		target.setOndCode(source.getOrignNDest());
		if (source.getFareRuleCommentInSelectedLocal() != null && AppSysParamsUtil.isEnableFareRuleComments()) {
			target.setComment(source.getFareRuleCommentInSelectedLocal());
		} else {
			target.setComment(source.getComments());
		}

		return target;
	}

}
