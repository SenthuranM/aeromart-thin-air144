package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;

public class InterlineFareSegChargeAdaptor implements Adaptor<BaseAvailRS, FareSegChargeTO> {
	private BaseAvailRQ baseAvailRQ;

	public InterlineFareSegChargeAdaptor(BaseAvailRQ baseAvailRQ) {
		this.baseAvailRQ = baseAvailRQ;
	}

	@Override
	public FareSegChargeTO adapt(BaseAvailRS source) {
		FareSegChargeTO target = new FareSegChargeTO();
		if (source.getSelectedPriceFlightInfo() != null && source.getSelectedPriceFlightInfo().getFareSegChargeTO() != null) {
			target = source.getSelectedPriceFlightInfo().getFareSegChargeTO();
			// In case of Own search done with search system as 'INT'
			if (target.getPaxWiseTotalCharges().isEmpty()) {
				target.setPaxWiseTotalCharges(retrievePaxWiseTotalCharges(source));
			}
			if (target.getTotalJourneyFare().isEmpty()) {
				target.setTotalJourneyFare(retrieveTotalJourneyFare(source));
			}
			if (target.getOndBundledFareDTOs() == null) {
				target.setOndBundledFareDTOs(retrieveONDBundleFares(baseAvailRQ, source));
			}
			if (target.isAllowOnhold() && AdaptorUtils.isReservationCabinClassOnholdable(source)) {
				target.setAllowOnhold(true);
			} else {
				target.setAllowOnhold(false);
			}
			if (target.getOndFareSegChargeTOs().isEmpty()) {
				target.setOndFareSegChargeTOs(retrieveOndFareSegCharges(source));
			}
		} else if (source.getSelectedPriceFlightInfo() != null
				&& source.getSelectedPriceFlightInfo().getFareSegChargeTO() == null) {
			target.setAllowOnhold(AdaptorUtils.isReservationCabinClassOnholdable(source));
			target.setOndBundledFareDTOs(retrieveONDBundleFares(baseAvailRQ, source));
			target.setPaxWiseTotalCharges(retrievePaxWiseTotalCharges(source));
			target.setTotalJourneyFare(retrieveTotalJourneyFare(source));
			target.setOndFareSegChargeTOs(retrieveOndFareSegCharges(source));
		}
		return target;
	}

	public Map<String, Double> retrievePaxWiseTotalCharges(BaseAvailRS baseAvailRS) {
		Map<String, Double> paxWiseTotalCharges = new HashMap<String, Double>();
		BigDecimal adultCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal childCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		FareTypeTO perAdult;
		FareTypeTO perChild;
		FareTypeTO perInfant;

		if (baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.ADULT) != null) {
			perAdult = baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.ADULT).getPassengerPrice();
			adultCharges = AccelAeroCalculator.add(perAdult.getTotalSurcharges(), perAdult.getTotalTaxes(),
					perAdult.getTotalFees());
			paxWiseTotalCharges.put(PaxTypeTO.ADULT, adultCharges.doubleValue());
		}
		if (baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.CHILD) != null) {
			perChild = baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.CHILD).getPassengerPrice();
			childCharges = AccelAeroCalculator.add(perChild.getTotalSurcharges(), perChild.getTotalTaxes(),
					perChild.getTotalFees());
			paxWiseTotalCharges.put(PaxTypeTO.CHILD, childCharges.doubleValue());
		}
		if (baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.INFANT) != null) {
			perInfant = baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.INFANT).getPassengerPrice();
			infantCharge = AccelAeroCalculator.add(perInfant.getTotalSurcharges(), perInfant.getTotalTaxes(),
					perInfant.getTotalFees());
			paxWiseTotalCharges.put(PaxTypeTO.INFANT, infantCharge.doubleValue());

		}
		return paxWiseTotalCharges;
	}

	public List<BundledFareDTO> retrieveONDBundleFares(BaseAvailRQ baseAvailRQ, BaseAvailRS baseAvailRS) {
		List<BundledFareDTO> bundleFares = new ArrayList<BundledFareDTO>();
		if (baseAvailRQ.getOrderedOriginDestinationInformationList().size() > 0) {
			for (OriginDestinationInformationTO ondInfo : baseAvailRQ.getOrderedOriginDestinationInformationList()) {
				if (ondInfo.getPreferredBundleFarePeriodId() != null) {
					BundledFareDTO bundledFareDTO = new BundledFareDTO();
					Integer ondBundleFarePeriodId = ondInfo.getPreferredBundleFarePeriodId();
					bundledFareDTO.setBundledFarePeriodId(ondBundleFarePeriodId);
					for (OndClassOfServiceSummeryTO ondClassOfService : baseAvailRS.getSelectedPriceFlightInfo()
							.getAvailableLogicalCCList()) {
						for (LogicalCabinClassInfoTO lccList : ondClassOfService.getAvailableLogicalCCList()) {
							if (lccList.getBundledFarePeriodId() != null
									&& lccList.getBundledFarePeriodId().equals(ondBundleFarePeriodId)) {
								bundledFareDTO.setBookingClasses(lccList.getBookingClasses());
								if (lccList.getBundledFareFreeServiceName() != null
										&& lccList.getBundledFareFreeServiceName().size() > 0) {
									Set<BundledFareFreeServiceTO> freeServiceList = new HashSet<BundledFareFreeServiceTO>();
									for (String freeServiceName : lccList.getBundledFareFreeServiceName()) {
										BundledFareFreeServiceTO freeService = new BundledFareFreeServiceTO();
										freeService.setServiceName(freeServiceName);
										freeServiceList.add(freeService);
									}
									bundledFareDTO.setApplicableServices(freeServiceList);
								}
							}
						}
					}
					bundleFares.add(bundledFareDTO);
				}
			}
		}
		return bundleFares;
	}

	public Map<String, Double> retrieveTotalJourneyFare(BaseAvailRS baseAvailRS) {
		Map<String, Double> totalJourneyFare = new HashMap<String, Double>();
		BigDecimal adultCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal childCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		FareTypeTO perAdult;
		FareTypeTO perChild;
		FareTypeTO perInfant;

		if (baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.ADULT) != null) {
			perAdult = baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.ADULT).getPassengerPrice();
			adultCharges = perAdult.getTotalBaseFare();
			totalJourneyFare.put(PaxTypeTO.ADULT, adultCharges.doubleValue());
		}
		if (baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.CHILD) != null) {
			perChild = baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.CHILD).getPassengerPrice();
			childCharges = perChild.getTotalBaseFare();
			totalJourneyFare.put(PaxTypeTO.CHILD, childCharges.doubleValue());
		}
		if (baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.INFANT) != null) {
			perInfant = baseAvailRS.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.INFANT).getPassengerPrice();
			infantCharge = perInfant.getTotalBaseFare();
			totalJourneyFare.put(PaxTypeTO.INFANT, infantCharge.doubleValue());
		}

		return totalJourneyFare;
	}

	public Collection<OndFareSegChargeTO> retrieveOndFareSegCharges(BaseAvailRS baseAvailRS) {
		Collection<OndFareSegChargeTO> ondFareSegChargeTOs = new ArrayList<OndFareSegChargeTO>();
		List<FareRuleDTO> applicableFareRules = baseAvailRS.getSelectedPriceFlightInfo().getFareTypeTO().getApplicableFareRules();
		for (FareRuleDTO fareRule : applicableFareRules) {
			OndFareSegChargeTO ondFareRule = new OndFareSegChargeTO();
			LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc = new LinkedHashMap<Integer, LinkedList<BookingClassAlloc>>();
			LinkedList<BookingClassAlloc> bcAllocList = new LinkedList<BookingClassAlloc>();
			ondFareRule.setFareBasisCode(fareRule.getFareBasisCode());
			ondFareRule.setOndSequence(fareRule.getOndSequence());
			ondFareRule.setOndCode(fareRule.getOrignNDest());
			ondFareRule.setFareID(fareRule.getFareId());
			ondFareRule.setFlexiChargeIds(fareRule.getFlexiCodes());
			BookingClassAlloc bcAlloc = new BookingClassAlloc(fareRule.getBookingClassCode(), fareRule.getCabinClassCode(), null,
					0, null, false, false, false, false, false, false);
			bcAllocList.add(bcAlloc);
			OriginDestinationInformationTO ondInfo = baseAvailRS.getOrderedOriginDestinationInformationList().get(
					fareRule.getOndSequence());
			for (OriginDestinationOptionTO ondOption : ondInfo.getOrignDestinationOptions()) {
				for (FlightSegmentTO segment : ondOption.getFlightSegmentList()) {
					if (fareRule.getOrignNDest().equals(segment.getSegmentCode())) {
						fsegBCAlloc.put(segment.getFlightSegId(), bcAllocList);
					}
				}
			}
			ondFareRule.setFsegBCAlloc(fsegBCAlloc);
			ondFareSegChargeTOs.add(ondFareRule);
		}
		return ondFareSegChargeTOs;
	}
}
