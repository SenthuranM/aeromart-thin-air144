package com.isa.aeromart.services.endpoint.adaptor.impls.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.dto.common.FlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;

public class SegmentReqTransformer implements Adaptor<List<FlightSegment>, List<FlightSegmentTO>> {

	@Override
	public List<FlightSegmentTO> adapt(List<FlightSegment> source) {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		Iterator<FlightSegment> iteSegments = source.iterator();
		while (iteSegments.hasNext()) {
			FlightSegment segment = iteSegments.next();

			FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
			flightSegmentTO.setArrivalDateTime(segment.getArrivalDateTime().getLocal());
			flightSegmentTO.setArrivalDateTimeZulu(segment.getArrivalDateTime().getZulu());
			flightSegmentTO.setDepartureDateTime(segment.getDepartureDateTime().getLocal());
			flightSegmentTO.setDepartureDateTimeZulu(segment.getDepartureDateTime().getZulu());
			flightSegmentTO.setArrivalTerminalName(segment.getArrivalTerminal());
			flightSegmentTO.setDepartureTerminalName(segment.getDepartureTerminal());
			flightSegmentTO.setFlightModelNumber(segment.getEquipmentModelNumber());
			flightSegmentTO.setFlightModelDescription(segment.getEquipmentModelInfo());
//			flightSegmentTO.setOperationType(segment.getTravelMode());
			flightSegmentTO.setFlightNumber(segment.getFilghtDesignator());
			flightSegmentTO.setOperatingAirline(segment.getOperatingCarrier());
			if (segment.getJourneyType().equals(CommonConstants.JourneyType.DOMESTIC)) {
				flightSegmentTO.setDomesticFlight(true);
			} else {
				flightSegmentTO.setDomesticFlight(false);
			}
			flightSegmentTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(segment.getFlightSegmentRPH()));
			flightSegmentTO.setFlightDuration(segment.getDuration());
			flightSegmentTO.setCabinClassCode("Y"); // FIXME
			flightSegmentTO.setLogicalCabinClassCode("Y"); // FIXME

			flightSegmentTO.setFlightRefNumber(segment.getFlightSegmentRPH());
			flightSegmentTO.setSegmentCode(segment.getSegmentCode());
			flightSegmentTOs.add(flightSegmentTO);
		}

		return flightSegmentTOs;
	}

}
