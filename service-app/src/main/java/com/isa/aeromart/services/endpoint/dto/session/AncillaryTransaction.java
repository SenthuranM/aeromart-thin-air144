package com.isa.aeromart.services.endpoint.dto.session;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherInformation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySession;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AncillaryFlowSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.AncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentAncillarySessionStore;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class AncillaryTransaction extends BaseTransaction
		implements Serializable, AncillaryFlowSessionStore, AncillarySessionStore, PaymentAncillarySessionStore {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String SESSION_KEY = "ANCI_TRNX";

	private RPHGenerator rphGenerator;

	private CommonConstants.ReservationFlow reservationFlow;

	private ReservationInfo resInfo;

	private List<SessionFlightSegment> segments;

	private FareSegChargeTO pricingInformation;

	// anci session data bundle
	private AncillarySession ancillarySession;

	private PreferenceInfo preferenceInfo;

	private LoyaltyInformation loyaltyInformation;

	private FinancialStore financialStore;

	private List<ServiceTaxContainer> applicableServiceTaxes;

	private PaymentStatus paymentStatus;

	private PostPaymentDTO postPaymentDTO;

	private OperationTypes operationTypes;

	private DiscountedFareDetails discountedFareDetails;

	private String originCountryCodeForTest;

	private Map<String, Integer> serviceCount;

	private boolean adminFeeInitiated;

	private int paymentAttempts;

	private BigDecimal serviceTaxForTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String voucherOTP;

	private VoucherPaymentInfo voucherPaymentInfo;

	private PayByVoucherInfo payByVoucherInfo;

	private Map<String, VoucherPaymentInfo> voucherPaymentInfoMap;

	private VoucherInformation voucherInformation;

	private Map<String, String> voucherOTPMap;

	public AncillaryTransaction() {
		super.addInitHook(() -> {
			ancillarySession = new AncillarySession();
			ancillarySession.init();
			financialStore = new FinancialStore();
		});
		super.init();
	}

	public FareSegChargeTO getPricingInformation() {
		return pricingInformation;
	}

	public void setPricingInformation(FareSegChargeTO pricingInformation) {
		this.pricingInformation = pricingInformation;
	}

	public void setReservationFlow(CommonConstants.ReservationFlow reservationFlow) {
		this.reservationFlow = reservationFlow;
	}

	public CommonConstants.ReservationFlow getReservationFlow() {
		return reservationFlow;
	}

	@Deprecated
	public void setResInfo(ReservationInfo resInfo) {
		this.resInfo = resInfo;
	}

	public List<SessionFlightSegment> getSegments() {
		return segments;
	}

	public void setSegments(List<SessionFlightSegment> segments) {
		this.segments = segments;
	}

	public AncillarySession getAncillarySession() {
		return ancillarySession;
	}

	public void setAncillarySession(AncillarySession ancillarySession) {
		this.ancillarySession = ancillarySession;
	}

	public PreferenceInfo getPreferenceInfo() {
		return preferenceInfo;
	}

	public void setPreferenceInfo(PreferenceInfo preferenceInfo) {
		this.preferenceInfo = preferenceInfo;
	}

	@Override
	public RPHGenerator getRPHGenerator() {
		if (rphGenerator == null) {
			rphGenerator = new RPHGenerator();
		}
		return rphGenerator;
	}

	@Override
	public SessionBasicReservation getReservationData() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<SessionFlightSegment> getSegmentWithInventory() {
		return segments;
	}

	@Override
	public FinancialStore getFinancialStore() {
		if (financialStore == null) {
			return new FinancialStore();
		}
		return financialStore;
	}

	@Override
	public FareSegChargeTO getFarePricingInformation() {
		return pricingInformation;
	}

	@Override
	public TravellerQuantity getTravellerQuantityForAncillary() {
		return resInfo.getPaxTypeWiseCount();
	}

	@Override
	public PreferenceInfo getPreferencesForAncillary() {
		return preferenceInfo;
	}

	@Override
	public ReservationInfo getResInfo() {
		if (resInfo == null) {
			return new ReservationInfo();
		}
		return resInfo;
	}

	@Override
	public List<ModifiedFlight> getModifiedFlightsInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> getResSegRphToFlightSegMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PostPaymentDTO getPostPaymentInformation() {
		return postPaymentDTO;

	}

	@Override
	public void storeRPHGeneratorForAncillaryModification(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;

	}

	@Deprecated
	public void storeReservationDataForAncillaryModification(ReservationInfo resInfo) {
		setResInfo(resInfo);

	}

	@Override
	public void storeSessionFlightsForAncillaryModification(List<FlightSegmentTO> segments) {
		List<SessionFlightSegment> sessionSegments = new ArrayList<SessionFlightSegment>();
		SessionFlightSegment sessionFlightSegment;
		for (FlightSegmentTO flightSegment : segments) {
			sessionFlightSegment = new SessionFlightSegment(flightSegment);
			sessionSegments.add(sessionFlightSegment);
		}
		setSegments(sessionSegments);

	}

	@Override
	public void storePreferenceInfoForAncillaryModification(PreferenceInfo preferenceInfo) {
		setPreferenceInfo(preferenceInfo);

	}

	@Override
	public void storePricingInformationForAncillaryModification(FareSegChargeTO pricingInformation) {
		setPricingInformation(pricingInformation);

	}

	@Override
	public BigDecimal getBalanceToPayAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	// FIXME: verify with existing IBE, anci modi case
	@Override
	public Collection<LCCClientPassengerSummaryTO> getPassengerSummaryList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void storePostPaymentInformation(PostPaymentDTO postPaymentDTO) {
		this.postPaymentDTO = postPaymentDTO;
	}

	@Override
	public LoyaltyInformation getLoyaltyInformation() {
		if (loyaltyInformation == null) {
			loyaltyInformation = new LoyaltyInformation();
			loyaltyInformation.setLoyaltyPaymentOption(LoyaltyPaymentOption.NONE);
		}
		return loyaltyInformation;
	}

	@Override
	public void storeLoyaltyInformation(LoyaltyInformation loyaltyInformation) {
		this.loyaltyInformation = loyaltyInformation;

	}

	@Override
	public void addPaymentCharge(ExternalChgDTO ccChgDTO) {
		financialStore.getPaymentExternalCharges().getReservationCharges().add(new LCCClientExternalChgDTO(ccChgDTO));

	}

	@Override
	public Collection<LCCClientExternalChgDTO> getExternalCharges() {
		return financialStore.getAllExternalCharges();
	}

	@Override
	public void removePaymentCharge(EXTERNAL_CHARGES extChgEnum) {
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getReservationCharges() != null
				&& !financialStore.getPaymentExternalCharges().getReservationCharges().isEmpty()) {
			Iterator<LCCClientExternalChgDTO> paymentChargeItr = financialStore.getPaymentExternalCharges()
					.getReservationCharges().iterator();
			while (paymentChargeItr.hasNext()) {
				LCCClientExternalChgDTO charge = paymentChargeItr.next();
				if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
					paymentChargeItr.remove();
				}
			}
		}

	}

	@Override
	public FareSegChargeTO getFareSegChargeTOForPayment() {
		return pricingInformation;
	}

	@Override
	public List<SessionFlightSegment> getSelectedSegmentsForPayment() {
		return segments;
	}

	@Override
	public PreferenceInfo getPreferencesForPayment() {
		return preferenceInfo;
	}

	@Override
	public TravellerQuantity getTravellerQuantityForPayment() {
		return resInfo.getPaxTypeWiseCount();
	}

	@Override
	public List<ServiceTaxContainer> getApplicableServiceTaxes() {
		if (applicableServiceTaxes == null) {
			applicableServiceTaxes = new ArrayList<ServiceTaxContainer>();
		}
		return applicableServiceTaxes;
	}

	@Override
	public void storeApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		getApplicableServiceTaxes().clear();
		getApplicableServiceTaxes().addAll(applicableServiceTaxes);
	}

	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalCharges() {
		if (financialStore.getAncillaryExternalCharges() != null) {
			return financialStore.getAncillaryExternalCharges().getPassengers();
		}
		return null;
	}

	@Override
	public void storePaymentStatus(PaymentStatus actionStatus) {
		this.paymentStatus = actionStatus;

	}

	@Override
	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	@Override
	public void clearPaymentInformation() {
		this.paymentStatus = null;
		this.postPaymentDTO = null;
		this.loyaltyInformation = null;
		removeAllPaymentCharge();

	}

	private void removeAllPaymentCharge() {
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getReservationCharges() != null
				&& !financialStore.getPaymentExternalCharges().getReservationCharges().isEmpty()) {
			financialStore.getPaymentExternalCharges().getReservationCharges().clear();
			financialStore.getPaymentExternalCharges().getPassengers().clear();

		}
	}

	@Override
	public void storeOperationType(OperationTypes operationTypes) {
		this.operationTypes = operationTypes;

	}

	@Override
	public OperationTypes getOperationType() {
		return operationTypes;
	}

	@Override
	public boolean isGroupPNR() {
		return getResInfo().isGroupPNR();
	}

	public void storeResInfo(ReservationInfo resInfo) {
		this.resInfo = resInfo;
	}

	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetailForAncillary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DiscountedFareDetails getDiscountedFareDetailsForAncillary() {
		return discountedFareDetails;
	}

	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalChargesForAncillary() {
		if (financialStore.getAncillaryExternalCharges() != null) {
			return financialStore.getAncillaryExternalCharges().getPassengers();
		}
		return null;
	}

	@Override
	public List<SessionFlightSegment> getSelectedSegmentsForAncillary() {
		return segments;
	}

	@Override
	public void storeUpdatedSessionFlightSegment(List<SessionFlightSegment> segments) {
		this.segments = segments;
	}

	@Override
	public void setDiscountAmount(BigDecimal discountAmount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setOriginCountryCodeForTest(String originCountryCodeForTest) {
		this.originCountryCodeForTest = originCountryCodeForTest;

	}

	@Override
	public String getOriginCountryCodeForTest() {
		return this.originCountryCodeForTest;
	}

	@Override
	public void storeServiceCount(Map<String, Integer> serviceCount) {
		this.serviceCount = serviceCount;
	}

	@Override
	public Map<String, Integer> getServiceCount() {
		return this.serviceCount;
	}

	@Override
	public boolean isOnHoldCreated() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void trackPaymentAttempts() throws ModuleException {
		paymentAttempts++;
		if (paymentAttempts > PaymentConsts.PAYMENT_ATTEMPT_LIMIT) {
			throw new ModuleException("payment.api.payment.attempt.limit.exceeded");
		}

	}

	public boolean isAdminFeeInitiated() throws ModuleException {
		if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			throw new ModuleException("payment.api.invalid.administration.fee.access");
		}
		return this.adminFeeInitiated;
	}

	public void setAdminFeeInitiated() throws ModuleException {
		if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			throw new ModuleException("payment.api.invalid.administration.fee.access");
		}
		this.adminFeeInitiated = true;
	}

	@Override
	public FareInfo getFareInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removePaymentPaxCharge(EXTERNAL_CHARGES extChgEnum) {
		if (resInfo.getPaxList() != null && !resInfo.getPaxList().isEmpty()) {
			for (ReservationPaxTO pax : resInfo.getPaxList()) {
				Iterator<LCCClientExternalChgDTO> paymentChargeItr = pax.getExternalCharges().iterator();
				while (paymentChargeItr.hasNext()) {
					LCCClientExternalChgDTO charge = paymentChargeItr.next();
					if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
						paymentChargeItr.remove();
					}
				}
			}
		}

		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getPassengers() != null
				&& !financialStore.getPaymentExternalCharges().getPassengers().isEmpty()) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> passengerCharges = financialStore.getPaymentExternalCharges()
					.getPassengers();
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : passengerCharges) {
				Iterator<LCCClientExternalChgDTO> paymentChargeItr = passengerChargeTo.getGetPassengerCharges().iterator();
				while (paymentChargeItr.hasNext()) {
					LCCClientExternalChgDTO charge = paymentChargeItr.next();
					if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
						paymentChargeItr.remove();
					}
				}
			}
		}

		if (financialStore.getAncillaryExternalCharges() != null
				&& financialStore.getAncillaryExternalCharges().getPassengers() != null
				&& !financialStore.getAncillaryExternalCharges().getPassengers().isEmpty()) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> passengerCharges = financialStore.getAncillaryExternalCharges()
					.getPassengers();
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : passengerCharges) {
				Iterator<LCCClientExternalChgDTO> paymentChargeItr = passengerChargeTo.getGetPassengerCharges().iterator();
				while (paymentChargeItr.hasNext()) {
					LCCClientExternalChgDTO charge = paymentChargeItr.next();
					if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
						paymentChargeItr.remove();
					}
				}
			}
		}

	}

	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetailForPayment() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal getServiceTaxForTransactionFee() {
		return serviceTaxForTransactionFee;
	}

	public void setServiceTaxForTransactionFee(BigDecimal serviceTaxForTransactionFee) {
		this.serviceTaxForTransactionFee = serviceTaxForTransactionFee;
	}

	@Override
	public List<SessionFlightSegment> getAllSegmentWithInventory() {
		return getSegmentWithInventory();
	}

	/**
	 * existing removePaymentPaxCharge has been overridden by some other logic therefore only in ancillary transaction
	 * following method is required when GST is enabled in order to avoid the conflict of ancillary charge removal while
	 * calculating service tax
	 * 
	 */
	public void removePaymentExternalChargesOnly(EXTERNAL_CHARGES extChgEnum) {

		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getPassengers() != null
				&& !financialStore.getPaymentExternalCharges().getPassengers().isEmpty()) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> passengerCharges = financialStore.getPaymentExternalCharges()
					.getPassengers();
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : passengerCharges) {
				Iterator<LCCClientExternalChgDTO> paymentChargeItr = passengerChargeTo.getGetPassengerCharges().iterator();
				while (paymentChargeItr.hasNext()) {
					LCCClientExternalChgDTO charge = paymentChargeItr.next();
					if (extChgEnum != null && extChgEnum == charge.getExternalCharges()) {
						paymentChargeItr.remove();
					}
				}
			}
		}

	}

	@Override
	public String getVoucherOTP() {
		return voucherOTP;
	}

	@Override
	public void setVoucherOTP(String voucherOTP) {
		this.voucherOTP = voucherOTP;
	}


	@Override
	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	@Override
	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	@Override
	public VoucherInformation getVoucherInformation() {
		if (voucherInformation == null) {
			voucherInformation = new VoucherInformation();
			voucherInformation.setIsTotalAmountPaidFromVoucher(false);
		}
		return voucherInformation;
	}

	@Override
	public Map<String, String> getVoucherOTPMap() {
		if (this.voucherOTPMap == null) {
			this.voucherOTPMap = new HashMap<>();
		}
		return voucherOTPMap;
	}

}
