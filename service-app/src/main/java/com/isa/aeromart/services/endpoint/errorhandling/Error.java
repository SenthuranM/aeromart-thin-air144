package com.isa.aeromart.services.endpoint.errorhandling;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;

public class Error extends TransactionalBaseRS {

	private String error;

	private ErrorType errorType;

	public Error(String error, ErrorType errorType) {
		setSuccess(false);
		addError(error);
		this.error = error;
		this.errorType = errorType;
	}

	public Error(String error) {
		this(error, ErrorType.OTHER_ERROR);
	}

	public String getError() {
		return error;
	}

	public ErrorType getErrorType() {
		return errorType;
	}
}
