package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import org.javacc.parser.RSequence;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.common.PhoneNumberReqAdaptor;
import com.isa.aeromart.services.endpoint.dto.common.Address;
import com.isa.aeromart.services.endpoint.dto.common.EmergencyContact;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;

public class ReservationContactRQAdaptor implements Adaptor<ReservationContact, CommonReservationContactInfo> {

	private String customerID;

	public ReservationContactRQAdaptor(String customerID) {
		this.customerID = customerID;
	}

	@Override
	public CommonReservationContactInfo adapt(ReservationContact reservationContact) {
		CommonReservationContactInfo resContactInfo = new CommonReservationContactInfo();
		PhoneNumberReqAdaptor phoneNumberAdaptor = new PhoneNumberReqAdaptor();

		resContactInfo.setTitle(reservationContact.getTitle());
		resContactInfo.setFirstName(reservationContact.getFirstName());
		resContactInfo.setLastName(reservationContact.getLastName());
		resContactInfo.setEmail(reservationContact.getEmailAddress());
		resContactInfo.setCountryCode(reservationContact.getCountry());
		resContactInfo.setNationalityCode(reservationContact.getNationality());
		resContactInfo.setPreferredLanguage(reservationContact.getPreferredLangauge());
		resContactInfo.setZipCode(reservationContact.getZipCode());
		resContactInfo.setTaxRegNo(reservationContact.getTaxRegNo());
		resContactInfo.setState(reservationContact.getState());
		
		resContactInfo.setState(reservationContact.getState());
		resContactInfo.setTaxRegNo(reservationContact.getTaxRegNo());
		
		resContactInfo.setSendPromoEmail(reservationContact.getSendPromoEmail());
		
		if (customerID != null) {
			resContactInfo.setCustomerId(Integer.valueOf(customerID));
		}

		if (reservationContact.getFax() != null) {
			resContactInfo.setFax(phoneNumberAdaptor.adapt(reservationContact.getFax()));
		}

		if (reservationContact.getMobileNumber() != null) {
			resContactInfo.setMobileNo(phoneNumberAdaptor.adapt(reservationContact.getMobileNumber()));
		}

		if (reservationContact.getTelephoneNumber() != null) {
			resContactInfo.setPhoneNo(phoneNumberAdaptor.adapt(reservationContact.getTelephoneNumber()));
		}

		if (reservationContact.getAddress() != null) {
			Address address = reservationContact.getAddress();
			resContactInfo.setCity(address.getCity());
			resContactInfo.setStreetAddress1(address.getStreetAddress1());
			resContactInfo.setStreetAddress2(address.getStreetAddress2());
		}
		if (reservationContact.getEmergencyContact() != null) {
			EmergencyContact emergencyContact = reservationContact.getEmergencyContact();
			resContactInfo.setEmgnEmail(emergencyContact.getEmgnEmail());
			resContactInfo.setEmgnFirstName(emergencyContact.getEmgnFirstName());
			resContactInfo.setEmgnLastName(emergencyContact.getEmgnLastName());
			resContactInfo.setEmgnPhoneNo(phoneNumberAdaptor.adapt(emergencyContact.getEmgnPhoneNumber()));
			resContactInfo.setEmgnTitle(emergencyContact.getEmgnTitle());
		}

		return resContactInfo;
	}

}
