package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.AlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;

public class AlertAdaptor implements Adaptor<LCCClientAlertTO, AlertTO> {

	@Override
	public AlertTO adapt(LCCClientAlertTO lccClientAlertTO) {
		AlertTO alertTO = new AlertTO();
		alertTO.setAlertId(lccClientAlertTO.getAlertId());
		alertTO.setContent(lccClientAlertTO.getContent());
		return alertTO;
	}

}
