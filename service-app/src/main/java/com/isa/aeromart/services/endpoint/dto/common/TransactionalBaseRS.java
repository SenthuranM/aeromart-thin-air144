package com.isa.aeromart.services.endpoint.dto.common;

public class TransactionalBaseRS extends BaseRS {

	private String transactionId;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
