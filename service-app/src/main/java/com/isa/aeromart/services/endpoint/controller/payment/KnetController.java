package com.isa.aeromart.services.endpoint.controller.payment;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.thinair.commons.api.exception.ModuleException;

@Controller
@RequestMapping(value = "knet", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
public class KnetController extends StatefulController {

	private static Log log = LogFactory.getLog(KnetController.class);

	@ResponseBody
	@RequestMapping(value = "knet/notification")
	public void payAtStoreNotificationCall(@RequestBody HttpServletRequest request) throws ModuleException {

	}

}