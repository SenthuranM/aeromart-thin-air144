package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

public class PaymentOptions {

	private List<PaymentGateway> paymentGateways;
	private LmsCredits LmsCredits;
	private LoyaltyCredits loyaltyCredits; // payOptRes
	private Cash cash; // payOptRes
	private OnAccountCredits onAccountCredits; // makePayReq
	private AdditionalDetails additionalDetails; // makePayReq
	private VoucherOption voucherOption;

	public List<PaymentGateway> getPaymentGateways() {
		return paymentGateways;
	}

	public void setPaymentGateways(List<PaymentGateway> paymentGateways) {
		this.paymentGateways = paymentGateways;
	}

	public LmsCredits getLmsCredits() {
		return LmsCredits;
	}

	public void setLmsCredits(LmsCredits lmsCredits) {
		LmsCredits = lmsCredits;
	}

	public LoyaltyCredits getLoyaltyCredits() {
		return loyaltyCredits;
	}

	public void setLoyaltyCredits(LoyaltyCredits loyaltyCredits) {
		this.loyaltyCredits = loyaltyCredits;
	}

	public Cash getCash() {
		return cash;
	}

	public void setCash(Cash cash) {
		this.cash = cash;
	}

	public OnAccountCredits getOnAccountCredits() {
		return onAccountCredits;
	}

	public void setOnAccountCredits(OnAccountCredits onAccountCredit) {
		this.onAccountCredits = onAccountCredit;
	}

	public AdditionalDetails getAdditionalDetails() {
		return additionalDetails;
	}

	public void setAdditionalDetails(AdditionalDetails additionalDetails) {
		this.additionalDetails = additionalDetails;
	}

	public VoucherOption getVoucherOption() {
		return voucherOption;
	}

	public void setVoucherOption(VoucherOption voucherOption) {
		this.voucherOption = voucherOption;
	}

}
