package com.isa.aeromart.services.endpoint.delegate.customer;

import com.isa.aeromart.services.endpoint.dto.customer.ConfirmCustomerRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ConfirmCustomerRS;
import com.isa.aeromart.services.endpoint.dto.customer.LMSConfirmationRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSConfirmationRS;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface CustomerService {

	public LMSConfirmationRS confirmLMSMember(LMSConfirmationRQ lmsConfirmationRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public boolean registerLoyaltyCustomer(LoyaltyCustomerProfile loyaltyProfile) throws ModuleException;

	public void saveOrUpdateLoyaltyCustomerProfile(LoyaltyCustomerProfile loyaltyCustomerProfile) throws ModuleException;

	public LoyaltyCustomerProfile getCustomerProfileByCustomerID(int customerID) throws ModuleException;

	public boolean isLoyaltyCustomer(int customerID) throws ModuleException;

	public ConfirmCustomerRS confirmCustomer(ConfirmCustomerRQ confirmCustomerRQ, TrackInfoDTO trackInfo)
			throws ModuleException;
}
