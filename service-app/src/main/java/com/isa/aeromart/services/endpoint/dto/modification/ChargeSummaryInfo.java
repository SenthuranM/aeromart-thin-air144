package com.isa.aeromart.services.endpoint.dto.modification;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ChargeSummaryInfo {

	private BigDecimal modificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal cancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal penaltyCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	public BigDecimal getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(BigDecimal modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public BigDecimal getCancellationCharge() {
		return cancellationCharge;
	}

	public void setCancellationCharge(BigDecimal cancellationCharge) {
		this.cancellationCharge = cancellationCharge;
	}

	public BigDecimal getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(BigDecimal penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

}
