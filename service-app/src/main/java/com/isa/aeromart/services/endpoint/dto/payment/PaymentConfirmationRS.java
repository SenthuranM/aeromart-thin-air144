package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.ActionStatus;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.booking.IPGTransactionResult;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PaymentConfirmationRS extends TransactionalBaseRS {

	private String pnr;
	private Date firstDepartureDate;
	private String contactLastName;
	private PaymentStatus paymentStatus;
	private ActionStatus actionStatus;
	private IPGTransactionResult ipgTransactionResult;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Date getFirstDepartureDate() {
		return firstDepartureDate;
	}

	public void setFirstDepartureDate(Date firstDepartureDate) {
		this.firstDepartureDate = firstDepartureDate;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public IPGTransactionResult getIpgTransactionResult() {
		return ipgTransactionResult;
	}

	public void setIpgTransactionResult(IPGTransactionResult ipgTransactionResult) {
		this.ipgTransactionResult = ipgTransactionResult;
	}
}
