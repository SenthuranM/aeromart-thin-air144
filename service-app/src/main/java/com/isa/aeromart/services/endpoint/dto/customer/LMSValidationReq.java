package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRQ;

public class LMSValidationReq extends BaseRQ{

	private String headOfEmailId;
	
	private String refferedEmailId;
	
	private String emailId;
	
	private String firstName;
	
	private String lastName;
	
	private boolean registration;

	public String getHeadOfEmailId() {
		return headOfEmailId;
	}

	public void setHeadOfEmailId(String headOfEmailId) {
		this.headOfEmailId = headOfEmailId;
	}

	public String getRefferedEmailId() {
		return refferedEmailId;
	}

	public void setRefferedEmailId(String refferedEmailId) {
		this.refferedEmailId = refferedEmailId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isRegistration() {
		return registration;
	}

	public void setRegistration(boolean registration) {
		this.registration = registration;
	}
	
}
