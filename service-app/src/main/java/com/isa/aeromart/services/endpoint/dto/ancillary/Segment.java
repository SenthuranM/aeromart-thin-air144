package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.Date;
import java.util.List;

public class Segment {
	
	private String flightSegmentRPH;
	
	private String flightNumber;
	
	private String departureLocation;

	private List<String> stopovers;
	
	private String arrivalLocation;
	
	private Date departureDateTime;

	public String getFlightSegmentRPH() {
		return flightSegmentRPH;
	}

	public void setFlightSegmentRPH(String flightSegmentRPH) {
		this.flightSegmentRPH = flightSegmentRPH;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDepartureLocation() {
		return departureLocation;
	}

	public void setDepartureLocation(String departureLocation) {
		this.departureLocation = departureLocation;
	}

	public List<String> getStopovers() {
		return stopovers;
	}

	public void setStopovers(List<String> stopovers) {
		this.stopovers = stopovers;
	}

	public String getArrivalLocation() {
		return arrivalLocation;
	}

	public void setArrivalLocation(String arrivalLocation) {
		this.arrivalLocation = arrivalLocation;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

}
