package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.AnciModificationParams;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class AnciModificationParamsAdaptor implements Adaptor<LCCClientReservation, AnciModificationParams> {

	public AnciModificationParamsAdaptor() {

	}

	@Override
	public AnciModificationParams adapt(LCCClientReservation reservation) {
		AnciModificationParams anciModificationParams = new AnciModificationParams();
		anciModificationParams.setAnciModifiable(isAnciModifiable(reservation.getStatus()));
		return anciModificationParams;
	}

	private boolean isAnciModifiable(String reservationStatus) {
		boolean anciModifiable = true;
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)
				|| ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservationStatus)) {
			anciModifiable = false;
		}
		return anciModifiable;
	}
}
