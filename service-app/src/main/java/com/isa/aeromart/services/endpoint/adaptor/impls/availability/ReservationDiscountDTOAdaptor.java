package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;

public class ReservationDiscountDTOAdaptor implements Adaptor<DiscountedFareDetails, ReservationDiscountDTO> {
	Log log = LogFactory.getLog(ReservationDiscountDTOAdaptor.class);

	private BaseAvailRQ baseAvailRQ;
	private TrackInfoDTO trackInfo;
	private boolean calculateTotalOnly;
	private PriceInfoTO priceInfoTo;
	private String transactionId;
	private boolean isSkipDiscountQuote;
	
	public ReservationDiscountDTOAdaptor(BaseAvailRQ baseAvailRQ, TrackInfoDTO trackInfo, boolean calculateTotalOnly,
			PriceInfoTO priceInfoTo, String transactionId, boolean isSkipDiscountQuote) {
		this.baseAvailRQ = baseAvailRQ;
		this.trackInfo = trackInfo;
		this.calculateTotalOnly = calculateTotalOnly;
		this.priceInfoTo = priceInfoTo;
		this.transactionId = transactionId;
		this.isSkipDiscountQuote = isSkipDiscountQuote;
	}

	@Override
	public ReservationDiscountDTO adapt(DiscountedFareDetails source) {
		ReservationDiscountDTO target = new ReservationDiscountDTO();

		if (source != null && priceInfoTo != null && baseAvailRQ != null) {

			if (isSkipDiscountQuote && PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(source.getDiscountAs())) {
				return target;
			}
			DiscountRQ promotionRQ = ReservationBeanUtil.getPromotionCalculatorRQ(source, null, baseAvailRQ
					.getTravelerInfoSummary().getPassengerTypeQuantityList());
			promotionRQ.setCalculateTotalOnly(calculateTotalOnly);
			SYSTEM searchSystem = baseAvailRQ.getAvailPreferences().getSearchSystem();

			if (searchSystem != SYSTEM.AA && searchSystem != SYSTEM.INT
					&& baseAvailRQ.getAvailPreferences().getSearchSystem() != null) {
				searchSystem = baseAvailRQ.getAvailPreferences().getSearchSystem();
			}

			if (promotionRQ != null) {
				try {
					target = ReservationApiUtils.getApplicableDiscount(promotionRQ, priceInfoTo.getFareSegChargeTO(),
							baseAvailRQ, searchSystem, transactionId, trackInfo);
				} catch (ModuleException e) {
					log.error("ReservationDiscountDTOAdaptor ===> getApplicableDiscount" + e);
				}
			}

		}
		return target;
	}

}
