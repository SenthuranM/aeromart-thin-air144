package com.isa.aeromart.services.endpoint.dto.modification;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ChargeBreakdownTemplate {

	private boolean isAnci;

	private String chargeType;

	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public boolean isAnci() {
		return isAnci;
	}

	public void setAnci(boolean isAnci) {
		this.isAnci = isAnci;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

}
