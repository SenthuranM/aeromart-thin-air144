package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.List;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;

public class Surcharge {

	@CurrencyValue
	private BigDecimal amount;

	// surcharge applicable pax types.
	private List<String> applicablePaxTypes;

	private String carrierCode;

	private String surchargeCode;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public List<String> getApplicablePaxTypes() {
		return applicablePaxTypes;
	}

	public void setApplicablePaxTypes(List<String> applicablePaxTypes) {
		this.applicablePaxTypes = applicablePaxTypes;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getSurchargeCode() {
		return surchargeCode;
	}

	public void setSurchargeCode(String surchargeCode) {
		this.surchargeCode = surchargeCode;
	}

}
