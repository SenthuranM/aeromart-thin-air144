package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotEmpty;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class BasePaymentRQ extends TransactionalBaseRQ {

	@NotNull
	private PaymentInfo paymentInfo;
	@NotEmpty
	private String hashValue;

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

}
