package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

public class LoyaltyCredits {

	private BigDecimal maxRedeemable;// payOptRes"
	private BigDecimal minRedeemable;// payOptRes
	private String currency;// -- payOptRes

	public BigDecimal getMaxRedeemable() {
		return maxRedeemable;
	}

	public void setMaxRedeemable(BigDecimal maxRedeemable) {
		this.maxRedeemable = maxRedeemable;
	}

	public BigDecimal getMinRedeemable() {
		return minRedeemable;
	}

	public void setMinRedeemable(BigDecimal minRedeemable) {
		this.minRedeemable = minRedeemable;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
