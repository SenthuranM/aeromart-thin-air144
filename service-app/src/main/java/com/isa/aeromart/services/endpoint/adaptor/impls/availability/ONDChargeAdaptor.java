package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.thinair.airproxy.api.model.reservation.commons.BasicChargeTO;

interface ONDChargeAdaptor<T extends BasicChargeTO> {
	abstract void addCharge(String paxType, T surcharge);

	abstract PaxPrice getPaxPrice(String paxType, BasicChargeTO basicCharge);

	abstract OndOWPricing getOndPrice(int ondSequence);
}