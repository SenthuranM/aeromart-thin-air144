package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AvailableAnci;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;

public class BaggageRequestAdaptor extends AncillaryRequestAdaptor {

	public String getLCCAncillaryType() {
		return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.BAGGAGE;
	}

	public Object toAvailableAncillaries(TrackInfoDTO trackInfo, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {
		AvailableBaggageRequestWrapper wrapper = new AvailableBaggageRequestWrapper();

		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
		flightSegmentTOs = AnciCommon.populateAnciOfferData(flightSegmentTOs, pricingInformation, travellerQuantity);
		
		Collections.sort(flightSegmentTOs);

		List<LCCBaggageRequestDTO> lccBaggageRequestDTOs = AvailableAnci.composeBaggageRequest(flightSegmentTOs, lccTransactionId != null ? lccTransactionId : "");
		LCCReservationBaggageSummaryTo baggageSummaryTo = AnciCommon.toBaggageSummaryTo(flightSegmentTOs);

		wrapper.setLccBaggageRequestDTOs(lccBaggageRequestDTOs);
		wrapper.setTransactionIdentifier(isModifyAnci? null : lccTransactionId != null ? lccTransactionId : "");
		wrapper.setSystem(system); 
		wrapper.setSelectedLanguage(LocaleContextHolder.getLocale().toString()); 
		wrapper.setModifyOperation(isModifyAnci);
		wrapper.setHasFinalCutOverPrivilege(false);
		wrapper.setRequote(false);
		wrapper.setBaggageSummaryTo(baggageSummaryTo);
		wrapper.setBundledFareDTOs(pricingInformation.getOndBundledFareDTOs());
		wrapper.setPnr(pnr); 
		wrapper.setTrackInfo(trackInfo);
		wrapper.setAppEngine(CommonServiceUtil.getApplicationEngine(trackInfo.getAppIndicator()));

		return wrapper;
	}

	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {

		List<SelectedItem> selectedItems;
		OndScope ondScope;
		String ondId;
		OndUnit ondUnit;

		LCCBaggageDTO selectedBaggage;
		List<LCCBaggageDTO> selectedBaggageList;
		Map<String, List<LCCBaggageDTO>> selectedBaggageMapping = new HashMap<>();

		List<OndUnit> ondUnits = metaData.getOndPreferences();

		for (Preference preference : preferences) {
			List<Selection> selections = preference.getSelections();
			for (Selection selection : selections) {
				ondScope = (OndScope) selection.getScope();
				ondId = ondScope.getOndId();
				ondUnit = null;

				for (OndUnit o : ondUnits) {
					if (o.getOndId().equals(ondId)) {
						ondUnit = o;
						break;
					}
				}

				for (String flightSegmentRPH : ondUnit.getFlightSegmentRPH()) {

					if (!selectedBaggageMapping.containsKey(flightSegmentRPH)) {
						selectedBaggageMapping.put(flightSegmentRPH, new ArrayList<>());
					}

					selectedBaggageList = selectedBaggageMapping.get(flightSegmentRPH);

					selectedItems = selection.getSelectedItems();
					for (SelectedItem selectedItem : selectedItems) {
							selectedBaggage = new LCCBaggageDTO();
							selectedBaggage.setBaggageName(AnciCommon.getBaggageName(selectedItem.getId()));
							selectedBaggage.setOndBaggageChargeId(AnciCommon.getBaggageOndChargeId(selectedItem.getId()));
							selectedBaggage.setSoldPieces(selectedItem.getQuantity());
							selectedBaggageList.add(selectedBaggage);
					}
				}

			}
		}

		return selectedBaggageMapping;

	}

	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;

	}

	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;

	}

	public class AvailableBaggageRequestWrapper {

		private List<LCCBaggageRequestDTO> lccBaggageRequestDTOs;
		private String transactionIdentifier;
		private ProxyConstants.SYSTEM system;
		private String selectedLanguage;
		private boolean isModifyOperation;
		private boolean hasFinalCutOverPrivilege;
		private boolean isRequote;
		private ApplicationEngine appEngine;
		private LCCReservationBaggageSummaryTo baggageSummaryTo;
		private List<BundledFareDTO> bundledFareDTOs;
		private String pnr;
		private BasicTrackInfo trackInfo;

		public List<LCCBaggageRequestDTO> getLccBaggageRequestDTOs() {
			return lccBaggageRequestDTOs;
		}

		public void setLccBaggageRequestDTOs(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs) {
			this.lccBaggageRequestDTOs = lccBaggageRequestDTOs;
		}

		public String getTransactionIdentifier() {
			return transactionIdentifier;
		}

		public void setTransactionIdentifier(String transactionIdentifier) {
			this.transactionIdentifier = transactionIdentifier;
		}

		public ProxyConstants.SYSTEM getSystem() {
			return system;
		}

		public void setSystem(ProxyConstants.SYSTEM system) {
			this.system = system;
		}

		public String getSelectedLanguage() {
			return selectedLanguage;
		}

		public void setSelectedLanguage(String selectedLanguage) {
			this.selectedLanguage = selectedLanguage;
		}

		public boolean isModifyOperation() {
			return isModifyOperation;
		}

		public void setModifyOperation(boolean isModifyOperation) {
			this.isModifyOperation = isModifyOperation;
		}

		public boolean isHasFinalCutOverPrivilege() {
			return hasFinalCutOverPrivilege;
		}

		public void setHasFinalCutOverPrivilege(boolean hasFinalCutOverPrivilege) {
			this.hasFinalCutOverPrivilege = hasFinalCutOverPrivilege;
		}

		public boolean isRequote() {
			return isRequote;
		}

		public void setRequote(boolean isRequote) {
			this.isRequote = isRequote;
		}

		public ApplicationEngine getAppEngine() {
			return appEngine;
		}

		public void setAppEngine(ApplicationEngine appEngine) {
			this.appEngine = appEngine;
		}

		public LCCReservationBaggageSummaryTo getBaggageSummaryTo() {
			return baggageSummaryTo;
		}

		public void setBaggageSummaryTo(LCCReservationBaggageSummaryTo baggageSummaryTo) {
			this.baggageSummaryTo = baggageSummaryTo;
		}

		public List<BundledFareDTO> getBundledFareDTOs() {
			return bundledFareDTOs;
		}

		public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
			this.bundledFareDTOs = bundledFareDTOs;
		}

		public String getPnr() {
			return pnr;
		}

		public void setPnr(String pnr) {
			this.pnr = pnr;
		}

		public BasicTrackInfo getTrackInfo() {
			return trackInfo;
		}

		public void setTrackInfo(BasicTrackInfo trackInfo) {
			this.trackInfo = trackInfo;
		}
	}
}
