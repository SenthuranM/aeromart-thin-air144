package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class QueryRS extends BaseRS {
	
	private String blockReferenceId;
	
	private String status;

	public String getBlockReferenceId() {
		return blockReferenceId;
	}

	public String getStatus() {
		return status;
	}

	public void setBlockReferenceId(String blockReferenceId) {
		this.blockReferenceId = blockReferenceId;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
