package com.isa.aeromart.services.endpoint.utils.common;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentAncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class CommonServiceUtil {
	
	private static Log log = LogFactory.getLog(CommonServiceUtil.class);

	public static SYSTEM getSearchSystem(List<OriginDestinationInformationTO> journeyInfo, String requestingCarrier) throws ModuleException  {
		return ModuleServiceLocator.getCommoMasterBD().getSearchSystem(journeyInfo, requestingCarrier);
	}

	public static String getCountryByIpAddress(String ip) throws ModuleException {
		String countryCode = "OT";
		if (!StringUtil.isNullOrEmpty(ip)) {
			countryCode = ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(ip);
		}
		return countryCode;
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public static ApplicationEngine getApplicationEngine(AppIndicatorEnum appIndicatorEnum) {

		if (AppIndicatorEnum.APP_IBE.equals(appIndicatorEnum)) {
			return ApplicationEngine.IBE;
		} else if (AppIndicatorEnum.APP_XBE.equals(appIndicatorEnum)) {
			return ApplicationEngine.XBE;
		} else if (AppIndicatorEnum.APP_WS.equals(appIndicatorEnum)) {
			return ApplicationEngine.WS;
		} else if (AppIndicatorEnum.APP_IOS.equals(appIndicatorEnum)) {
			return ApplicationEngine.IOS;
		} else if (AppIndicatorEnum.APP_ANDROID.equals(appIndicatorEnum)) {
			return ApplicationEngine.ANDROID;
		} else {
			throw new ValidationException(ValidationException.Code.INVALID_CHANNEL);
		}

	}

	public static Integer getChannelId(AppIndicatorEnum appIndicatorEnum) {

		if (AppIndicatorEnum.APP_IBE.equals(appIndicatorEnum)) {
			return ReservationInternalConstants.SalesChannel.WEB;
		} else if (AppIndicatorEnum.APP_XBE.equals(appIndicatorEnum)) {
			return ReservationInternalConstants.SalesChannel.TRAVEL_AGENT;
		} else if (AppIndicatorEnum.APP_WS.equals(appIndicatorEnum)) {
			return ReservationInternalConstants.SalesChannel.DNATA;
		} else if (AppIndicatorEnum.APP_IOS.equals(appIndicatorEnum)) {
			return ReservationInternalConstants.SalesChannel.IOS;
		} else if (AppIndicatorEnum.APP_ANDROID.equals(appIndicatorEnum)) {
			return ReservationInternalConstants.SalesChannel.ANDROID;
		} else {
			throw new ValidationException(ValidationException.Code.INVALID_CHANNEL);
		}

	}

	public static Integer getDefaultPGId(Currency currency, AppIndicatorEnum appIndicatorEnum) {
		if (AppIndicatorEnum.APP_IBE.equals(appIndicatorEnum)) {
			return currency.getDefaultIbePGId();
		} else if (AppIndicatorEnum.APP_XBE.equals(appIndicatorEnum)) {
			return currency.getDefaultXbePGId();
		} else if (AppIndicatorEnum.APP_IOS.equals(appIndicatorEnum)) {
			return currency.getDefaultIbePGId();
		} else if (AppIndicatorEnum.APP_ANDROID.equals(appIndicatorEnum)) {
			return currency.getDefaultIbePGId();
		} else {
			throw new ValidationException(ValidationException.Code.INVALID_CHANNEL);
		}

	}

	public static ApplicationEngine getSysParamKeyApplicationEngine(AppIndicatorEnum appIndicator) {
		if (AppIndicatorEnum.APP_IBE.equals(appIndicator)) {
			return ApplicationEngine.IBE;
		} else if (AppIndicatorEnum.APP_XBE.equals(appIndicator)) {
			return ApplicationEngine.XBE;
		} else if (AppIndicatorEnum.APP_WS.equals(appIndicator)) {
			return ApplicationEngine.WS;
		} else if (AppIndicatorEnum.APP_IOS.equals(appIndicator)) {
			return ApplicationEngine.WS;
		} else if (AppIndicatorEnum.APP_ANDROID.equals(appIndicator)) {
			return ApplicationEngine.ANDROID;
		} else {
			throw new ValidationException(ValidationException.Code.INVALID_CHANNEL);
		}

	}
	
	public void updateBlockedLmsCreditStatusForCreateReservation(BookingRS bookingRS, BookingSessionStore bookingSessionStore) {

		if (bookingSessionStore.getLoyaltyInformation() != null && bookingSessionStore.getPostPaymentInformation() != null) {

			PostPaymentDTO postPaymentData = bookingSessionStore.getPostPaymentInformation();
			if (postPaymentData.getTemporyPaymentMap() != null && !postPaymentData.getTemporyPaymentMap().isEmpty()) {
				try {
					int tempPayId = getTmpPayIdFromTmpPayMap(postPaymentData.getTemporyPaymentMap());
					updateLmsBlockedCreditStatus(tempPayId);
				} catch (ModuleException e) {
					log.error("ERROR :: UPDATING LMS CREDIT STATUS | CREATE BOOKING");
				}
			} else if (bookingSessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo() != null
					&& !bookingSessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo().isEmpty()) {
				String[] rewardIDs = getRewardIds(bookingSessionStore.getLoyaltyInformation());
				updateLmsBlockedCreditStatus(rewardIDs);
			}
		}

	}

	public void updateBlockedLmsCreditStatusForModifyReservation(RequoteSessionStore requoteSessionStore) {

		if (requoteSessionStore.getLoyaltyInformation() != null
				&& requoteSessionStore.getRequotePostPaymentInformation() != null) {

			PostPaymentDTO postPaymentData = requoteSessionStore.getRequotePostPaymentInformation();
			if (postPaymentData.getTemporyPaymentMap() != null && !postPaymentData.getTemporyPaymentMap().isEmpty()) {
				try {
					int tempPayId = getTmpPayIdFromTmpPayMap(postPaymentData.getTemporyPaymentMap());
					updateLmsBlockedCreditStatus(tempPayId);
				} catch (ModuleException e) {
					log.error("ERROR :: UPDATING LMS CREDIT STATUS | MODIFY RESERVATION");
				}
			} else if (requoteSessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo() != null
					&& !requoteSessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo().isEmpty()) {
				String[] rewardIDs = getRewardIds(requoteSessionStore.getLoyaltyInformation());
				updateLmsBlockedCreditStatus(rewardIDs);
			}
		}

	}

	public void updateBlockedLmsCreditStatusForModifyAncillary(PaymentAncillarySessionStore paymentAncillarySessionStore) {

		if (paymentAncillarySessionStore.getLoyaltyInformation() != null
				&& paymentAncillarySessionStore.getPostPaymentInformation() != null) {

			PostPaymentDTO postPaymentData = paymentAncillarySessionStore.getPostPaymentInformation();
			if (postPaymentData.getTemporyPaymentMap() != null && !postPaymentData.getTemporyPaymentMap().isEmpty()) {
				try {
					int tempPayId = getTmpPayIdFromTmpPayMap(postPaymentData.getTemporyPaymentMap());
					updateLmsBlockedCreditStatus(tempPayId);
				} catch (ModuleException e) {
					log.error("ERROR :: UPDATING LMS CREDIT STATUS | MODIFY ANCILLARY");
				}
			} else if (paymentAncillarySessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo() != null
					&& !paymentAncillarySessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo().isEmpty()){
				String[] rewardIDs = getRewardIds(paymentAncillarySessionStore.getLoyaltyInformation());
				updateLmsBlockedCreditStatus(rewardIDs);
			}
		}

	}

	private void updateLmsBlockedCreditStatus(int tempPayId) {

		try {

			ModuleServiceLocator.getReservationBD().updateLMSCreditUtilizationSuccess(tempPayId);
			log.info("SUCCESSFULLY UPDATED LMS CREDIT STATUS >> PNR CREATED ");

		} catch (Exception e) {
			log.error("ERROR :: UPDATING LMS CREDIT STATUS");
		}
	}

	private void updateLmsBlockedCreditStatus(String [] rewardIDs) {

		try {
			ModuleServiceLocator.getReservationBD().updateLMSCreditUtilizationSuccess(rewardIDs);
			log.info("SUCCESSFULLY UPDATED LMS CREDIT STATUS >>  SUCCESS BY REWARD IDS ");

		} catch (Exception e) {
			log.error("ERROR :: UPDATING LMS CREDIT STATUS TO SUCCESS BY REWARD IDS");
		}
	}

	private String[] getRewardIds(LoyaltyInformation loyaltyInfo) {
		Set<String> rewardIds = new HashSet<String>();
		for (LoyaltyPaymentInfo loyaltyInformation : loyaltyInfo.getCarrierWiseLoyaltyPaymentInfo().values()) {
			rewardIds.addAll(Arrays.asList(loyaltyInformation.getLoyaltyRewardIds()));
		}
		return  rewardIds.stream().toArray(String[] ::new);
	}

	private static int getTmpPayIdFromTmpPayMap(Map mapTempPayMap) throws ModuleException {
		if (mapTempPayMap.size() == 1) {
			return (Integer) BeanUtils.getFirstElement(mapTempPayMap.keySet());
		}
		log.error("Error :: Retrieving temp pay id");
		throw new ModuleException("Error :: Retrieving temp pay id");
	}
}