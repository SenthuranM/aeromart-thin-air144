package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

public class AncillarySession {

	public void init() {
		AncillarySelection ancillarySelection;
		AncillaryQuotation ancillaryQuotation;

		setReservation(null);

		ancillarySelection = new AncillarySelection();
		setAncillarySelection(ancillarySelection);

		ancillaryQuotation = new AncillaryQuotation();
		setAncillaryQuotation(ancillaryQuotation);
	}

	private SessionBasicReservation reservation;

	private AncillarySelection ancillarySelection;

	private AncillaryQuotation ancillaryQuotation;

	public SessionBasicReservation getReservation() {
		return reservation;
	}

	public void setReservation(SessionBasicReservation reservation) {
		this.reservation = reservation;
	}

	public AncillarySelection getAncillarySelection() {
		return ancillarySelection;
	}

	public void setAncillarySelection(AncillarySelection ancillarySelection) {
		this.ancillarySelection = ancillarySelection;
	}

	public AncillaryQuotation getAncillaryQuotation() {
		return ancillaryQuotation;
	}

	public void setAncillaryQuotation(AncillaryQuotation ancillaryQuotation) {
		this.ancillaryQuotation = ancillaryQuotation;
	}
}
