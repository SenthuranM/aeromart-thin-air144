package com.isa.aeromart.services.endpoint.dto.booking;

public class PaymentDetails {

	private String currency;
	
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
