package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.AdditionalInfo;
import com.isa.aeromart.services.endpoint.dto.common.DocInfo;
import com.isa.aeromart.services.endpoint.dto.common.FoidInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;

public class AdditionalInfoReqAdaptor implements Adaptor<AdditionalInfo, LCCClientReservationAdditionalPax> {

	@Override
	public LCCClientReservationAdditionalPax adapt(AdditionalInfo additionalInfo) {
		LCCClientReservationAdditionalPax lccClientReservationAdditionalPax = new LCCClientReservationAdditionalPax();
		lccClientReservationAdditionalPax.setFfid(additionalInfo.getFfid());
		lccClientReservationAdditionalPax.setNationalIDNo(additionalInfo.getNic());
		if (additionalInfo.getFoidInfo() != null) {
			FoidInfo foidInfo = additionalInfo.getFoidInfo();
			lccClientReservationAdditionalPax.setPassportExpiry(foidInfo.getFoidExpiry());
			lccClientReservationAdditionalPax.setPassportNo(foidInfo.getFoidNumber());
			lccClientReservationAdditionalPax.setPassportIssuedCntry(foidInfo.getFoidPlace());
			lccClientReservationAdditionalPax.setPlaceOfBirth(foidInfo.getPlaceOfBirth());
		}

		if (additionalInfo.getDocInfo() != null) {
			DocInfo docInfo = additionalInfo.getDocInfo();
			lccClientReservationAdditionalPax.setVisaDocIssueDate(docInfo.getDocIssueDate());
			lccClientReservationAdditionalPax.setTravelDocumentType(docInfo.getTravelDocumentType());
			lccClientReservationAdditionalPax.setVisaApplicableCountry(docInfo.getApplicableCountry());
			lccClientReservationAdditionalPax.setVisaDocNumber(docInfo.getDocNumber());
			lccClientReservationAdditionalPax.setVisaDocPlaceOfIssue(docInfo.getDocPlaceOfIssue());
		}
		return lccClientReservationAdditionalPax;
	}

}
