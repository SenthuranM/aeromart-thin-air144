package com.isa.aeromart.services.endpoint.dto.ancillary;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.ThirdPartyProvider;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "providerType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = SystemProvided.class, name = Provider.ProviderType.SYSTEM),
        @JsonSubTypes.Type(value = ThirdPartyProvider.class, name = Provider.ProviderType.THIRD_PARTY) })
public abstract class Provider {

	public interface ProviderType {
		String SYSTEM = "SYSTEM";
        String THIRD_PARTY = "THIRD_PARTY";
	}

	private String providerType;
	
	private AvailableAncillary availableAncillaries;

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public AvailableAncillary getAvailableAncillaries() {
		return availableAncillaries;
	}

	public void setAvailableAncillaries(AvailableAncillary availableAncillaries) {
		this.availableAncillaries = availableAncillaries;
	}
}
