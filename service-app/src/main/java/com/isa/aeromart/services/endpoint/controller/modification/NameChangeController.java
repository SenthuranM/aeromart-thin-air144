package com.isa.aeromart.services.endpoint.controller.modification;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.InterlineFareSegChargeAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationInfoAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.ModificationAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.NameChangeFareRequoteAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.NameChangePassengerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.RequoteBalanceAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.RequoteSessionONDListAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRS;
import com.isa.aeromart.services.endpoint.dto.modification.NameChangeRQ;
import com.isa.aeromart.services.endpoint.dto.modification.RequoteBalanceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.BalanceQueryRequiredData;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AvailabilityRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ValidationService;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

@Controller
@RequestMapping("modification")
public class NameChangeController extends StatefulController {

	@Autowired
	private ValidationService validationService;

	@Autowired
	private BookingService bookingService;

	@ResponseBody
	@RequestMapping(value = "/nameChange", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			TransactionalBaseRS nameChangeRequote(@Validated @RequestBody NameChangeRQ nameChangeReq) throws ParseException, ModuleException {
		TransactionalBaseRS response = new BalanceSummaryRS();

		LCCClientReservation reservation = loadProxyReservation(nameChangeReq.getPnr(), nameChangeReq.isGroupPnr());
		List<LCCClientReservationPax> nameChangedpaxList = getValidatedNameChangedPaxList(nameChangeReq, reservation);
		FlightPriceRQ flightPriceRQ = getFlightPriceRQ(nameChangeReq.isGroupPnr(), reservation);
		FlightPriceRS flightPriceRS = fareRequote(flightPriceRQ);
		String transactionId = initiateTransaction(flightPriceRS.getTransactionIdentifier());
		reStoreRequoteSessionWithFareRequotedData(transactionId, flightPriceRS, flightPriceRQ, reservation, nameChangedpaxList);
		RequoteBalanceQueryRQ balanceQueryRQ = geteBalanceQueryRQ(transactionId);
		ReservationBalanceTO lCCClientReservationBalanceTO = getRequoteBalanceSummary(balanceQueryRQ);
		response = getBalanceSummaryResponse(transactionId, lCCClientReservationBalanceTO);
		reStoreRequoteSessionWithRequotBalanceSummaryData(transactionId, lCCClientReservationBalanceTO,
				(BalanceSummaryRS) response);
		response.setSuccess(true);
		response.setTransactionId(transactionId);

		return response;
	}

	private LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR) throws ModuleException {
		return bookingService.loadProxyReservation(pnr, isGroupPNR, getTrackInfo(), false, false, null, getTrackInfo()
				.getCarrierCode(), false);
	}

	private List<LCCClientReservationPax> getValidatedNameChangedPaxList(NameChangeRQ nameChangeReq,
			LCCClientReservation reservation) throws ValidationException, ModuleException {
		SYSTEM system = getSystem(nameChangeReq.isGroupPnr());
		validationService.nameChangeValidate(reservation, nameChangeReq.getNameChangePassengerList(), system, getTrackInfo());
		List<LCCClientReservationPax> nameChangedpaxList = getNamechangePaxList(nameChangeReq, reservation);

		if (AppSysParamsUtil.isDuplicateNameCheckEnabled())
			validationService.checkDuplicateNames(nameChangedpaxList, reservation.getSegments(), system, getTrackInfo());
		return nameChangedpaxList;
	}

	private List<LCCClientReservationPax> getNamechangePaxList(NameChangeRQ nameChangeReq, LCCClientReservation reservation) {
		NameChangePassengerAdaptor nameChangeAdaptor = new NameChangePassengerAdaptor(reservation.getPassengers());
		List<LCCClientReservationPax> nameChangedpaxList = new ArrayList<LCCClientReservationPax>();
		AdaptorUtils.adaptCollection(nameChangeReq.getNameChangePassengerList(), nameChangedpaxList, nameChangeAdaptor);
		return nameChangedpaxList;
	}

	private FlightPriceRQ getFlightPriceRQ(boolean isGroupPnr, LCCClientReservation reservation) {
		NameChangeFareRequoteAdaptor nameChangeFareReqoteAdaptor = new NameChangeFareRequoteAdaptor(getSystem(isGroupPnr));
		return nameChangeFareReqoteAdaptor.adapt(reservation);
	}

	private FlightPriceRS fareRequote(FlightPriceRQ flightPriceRQ) throws ModuleException {
		return ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ, getTrackInfo());
	}

	private void reStoreRequoteSessionWithFareRequotedData(String transactionId, FlightPriceRS flightPriceRS,
			FlightPriceRQ flightPriceRQ, LCCClientReservation reservation, List<LCCClientReservationPax> nameChangedpaxList)
			throws ModuleException, ParseException {

		RequoteSessionStore sessionStore = getRequoteSessionStore(transactionId);

		ReservationInfoAdaptor reservationInfoAdaptor = new ReservationInfoAdaptor(flightPriceRS.getTransactionIdentifier());
		ReservationInfo sessionResInfo = reservationInfoAdaptor.adapt(reservation);
		sessionStore.storeResInfo(sessionResInfo);

		FareInfo fareInfo = sessionStore.getFareInfo();
		FareSegChargeTO fareSegChargeTO = flightPriceRS.getSelectedPriceFlightInfo().getFareSegChargeTO();
		if (fareSegChargeTO == null) {
			InterlineFareSegChargeAdaptor fareSegAdaptor = new InterlineFareSegChargeAdaptor(flightPriceRQ);
			fareSegChargeTO = fareSegAdaptor.adapt(flightPriceRS);
		}
		fareInfo.setFareSegmentChargeTO(fareSegChargeTO);
		sessionStore.storeFareInfo(fareInfo);

		BalanceQueryRequiredData sessionBalanceQueryInfo = sessionStore.getRequoteBalanceReqiredParams();
		sessionBalanceQueryInfo.setRemovedReservationSegIds(new HashSet<String>());
		sessionBalanceQueryInfo.setCancelSegment(false);
		sessionBalanceQueryInfo.setModifySegment(false);
		sessionBalanceQueryInfo.setNameChange(true);
		sessionBalanceQueryInfo.setGroundFltSegByFltSegId(ModificationUtils.getGroundSegmentByFlightSegmentIDMap(flightPriceRS,
				reservation.getSegments()));
		sessionStore.storeRequoteBalanceReqiredParams(sessionBalanceQueryInfo);

		PreferenceInfo sessionPreferences = sessionStore.getPreferences();
		sessionPreferences.setBookingCode(flightPriceRQ.getTravelPreferences().getBookingClassCode());
		sessionPreferences.setSeatType(flightPriceRQ.getTravelPreferences().getBookingType());
		sessionPreferences.setSelectedSystem(getSystem(reservation.isGroupPNR()).toString());
		sessionStore.storePreferences(sessionPreferences);

		RequoteSessionONDListAdaptor requoteOndAptor = new RequoteSessionONDListAdaptor(null, null, null);
		sessionStore.storeOndInfo(requoteOndAptor.adapt(flightPriceRQ.getOriginDestinationInformationList()));

		sessionStore.storeNameChangedPaxMap(transformToPaxMap(nameChangedpaxList));
		sessionStore.storeApplicableServiceTaxes(flightPriceRS.getApplicableServiceTaxes());
		storeSelectedFlightSegmentTOList(transactionId, flightPriceRS);
	}

	private void storeSelectedFlightSegmentTOList(String transactionId, FlightPriceRS flightPriceRS) {
		if (flightPriceRS.getSelectedPriceFlightInfo() != null) {
			List<FlightSegmentTO> allSegments = new ArrayList<FlightSegmentTO>();
			for (OriginDestinationInformationTO ondInfo : flightPriceRS.getOriginDestinationInformationList()) {
				for (OriginDestinationOptionTO ondOption : ondInfo.getOrignDestinationOptions()) {
					if (ondOption.isSelected()) {
						allSegments.addAll(ondOption.getFlightSegmentList());
						break;
					}
				}
			}
			getRequoteAvailabilitySessionStore(transactionId).storeSelectedSegments(allSegments);
		}
	}

	private RequoteBalanceQueryRQ geteBalanceQueryRQ(String transactionId) {
		RequoteSessionStore sessionStore = getRequoteSessionStore(transactionId);
		RequoteBalanceQueryRQ balanceQueryRQ = ModificationAdaptor.getBalanceQueryRQ(sessionStore);
		return balanceQueryRQ;
	}

	private void reStoreRequoteSessionWithRequotBalanceSummaryData(String transactionId,
			ReservationBalanceTO lCCClientReservationBalanceTO, BalanceSummaryRS response) {
		RequoteSessionStore sessionStore = getRequoteSessionStore(transactionId);
		sessionStore.getFareInfo().setCreditableAmount(
				AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getSegmentSummary().getCurrentRefunds()));
		sessionStore.getFareInfo().setModifyCharge(
				AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getSegmentSummary().getNewModAmount()));
		sessionStore.storeBalanceInfo(getBalanceInfo(sessionStore, lCCClientReservationBalanceTO));
	}

	private RequoteBalanceInfo getBalanceInfo(RequoteSessionStore session, ReservationBalanceTO reservationBalanceTo){
		RequoteBalanceAdaptor requoteBalanceAdaptor = new RequoteBalanceAdaptor(session.getResInfo().getPaxCreditMap(
				session.getResInfo().getPnr()));
		return requoteBalanceAdaptor.adapt(reservationBalanceTo);
	}

	private ReservationBalanceTO getRequoteBalanceSummary(RequoteBalanceQueryRQ balanceQueryRQ) throws ModuleException {
		return ModuleServiceLocator.getAirproxyReservationBD().getRequoteBalanceSummary(balanceQueryRQ, getTrackInfo());
	}

	private BalanceSummaryRS getBalanceSummaryResponse(String transactionId, ReservationBalanceTO lCCClientReservationBalanceTO) {
		return ModificationAdaptor.createBalanceSummaryReseponse(getRequoteSessionStore(transactionId),
				lCCClientReservationBalanceTO);
	}

	public static Map<Integer, NameDTO> transformToPaxMap(List<LCCClientReservationPax> nameChangePassengerList) {

		Map<Integer, NameDTO> paxMap = new HashMap<Integer, NameDTO>();

		for (LCCClientReservationPax pax : nameChangePassengerList) {
			NameDTO nDTO = new NameDTO();
			nDTO.setFirstname(pax.getFirstName());
			nDTO.setLastName(pax.getLastName());
			nDTO.setTitle(pax.getTitle());
			nDTO.setPnrPaxId(PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber()));
			nDTO.setPaxReference(pax.getTravelerRefNumber());
			paxMap.put(nDTO.getPnrPaxId(), nDTO);
		}
		return paxMap;
	}

	private RequoteSessionStore getRequoteSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private AvailabilityRequoteSessionStore getRequoteAvailabilitySessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private SYSTEM getSystem(boolean isGroupPnr) {
		SYSTEM system = SYSTEM.AA;
		if (isGroupPnr) {
			system = SYSTEM.INT;
		}
		return system;
	}

	private String initiateTransaction(String transactionId) {
		return initTransaction(AvailabilityRequoteSessionStore.SESSION_KEY,transactionId);
	}
}
