package com.isa.aeromart.services.endpoint.delegate.masterdata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.PaxContactDetailsConfigAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.PaxCutOffYearsAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.PaxDetailsConfigAdaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.ContactConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxConfigCollectionRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsField;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxContactDetailsRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfig;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfigRS;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsField;
import com.isa.aeromart.services.endpoint.dto.masterdata.UserRegistrationConfigRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.UserRegistrationConfigRS;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class MasterDataServiceAdaptor implements MasterDataService {

	private static final Log log = LogFactory.getLog(MasterDataServiceAdaptor.class);
	private static final String AUTO_CHECKIN_VISIBILITY = "_autoCheckinVisibility";
	private static final String AUTO_CHECKIN_MANDATORY = "_autoCheckinMandatory";
	
	private Map<String , PaxDetailsField> paxDetailsFields;
	
	public PaxDetailsConfigRS paxDetails(PaxDetailsConfigRQ paxDetailsConfigReq, TrackInfoDTO trackInfo,
			String searchSystem, List<String> carrierList, List<SessionFlightSegment> segments) {

		boolean success = true;
		PaxDetailsConfigRS paxDetailsConfigResponse = new PaxDetailsConfigRS();
		PaxDetailsConfigAdaptor paxDetailsConfigAdaptor = new PaxDetailsConfigAdaptor();
		PaxCutOffYearsAdaptor paxCutOffYearsAdaptor = new PaxCutOffYearsAdaptor();
		paxDetailsFields = new HashMap<String, PaxDetailsField>();
		
		try {
			
			PaxContactConfigDTO paxContactDTO = populatePaxConfig(trackInfo, searchSystem, carrierList, paxDetailsConfigAdaptor);

			List<String> originDestinations = new ArrayList<String>();
			if (BeanUtils.nullHandler(paxDetailsConfigReq.getOrigin()).length() > 0
					&& BeanUtils.nullHandler(paxDetailsConfigReq.getDestination()).length() > 0) {
				originDestinations.add(paxDetailsConfigReq.getOrigin());
				originDestinations.add(paxDetailsConfigReq.getDestination());
			}
			populateCountryConfig(originDestinations, trackInfo);
			populateNICConfig(segments);

			paxDetailsConfigResponse.setPaxCutOffYears(paxCutOffYearsAdaptor.adapt(paxContactDTO.getPaxValidation()));
			
		} catch (ModuleException e) {
			success = false;
			log.error("ERROR in loading pax & contact config", e);
		}

		paxDetailsConfigResponse.setPaxDetailsConfig(paxDetailsFields);
		paxDetailsConfigResponse.setSuccess(success);
		paxDetailsConfigResponse.setErrors(null);
		paxDetailsConfigResponse.setMessages(null);
		paxDetailsConfigResponse.setWarnings(null);

		return paxDetailsConfigResponse;
	}

	public PaxDetailsConfigRS unifiedPaxDetails(PaxConfigCollectionRQ paxConfigCollectionRQ, TrackInfoDTO trackInfo,
			String searchSystem, List<String> carrierList, List<SessionFlightSegment> segments) {

		boolean success = true;
		PaxDetailsConfigRS paxDetailsConfigResponse = new PaxDetailsConfigRS();
		PaxDetailsConfigAdaptor paxDetailsConfigAdaptor = new PaxDetailsConfigAdaptor();
		PaxCutOffYearsAdaptor paxCutOffYearsAdaptor = new PaxCutOffYearsAdaptor();
		paxDetailsFields = new HashMap<String, PaxDetailsField>();
		
		try {
			
			PaxContactConfigDTO paxContactDTO = populatePaxConfig(trackInfo, searchSystem, carrierList, paxDetailsConfigAdaptor);

			List<String> originDestinations = new ArrayList<String>();
			for (PaxDetailsConfigRQ paxDetailsConfigReq : paxConfigCollectionRQ.getConfigDetailsRQs()) {
				if (BeanUtils.nullHandler(paxDetailsConfigReq.getOrigin()).length() > 0
						&& BeanUtils.nullHandler(paxDetailsConfigReq.getDestination()).length() > 0) {
					originDestinations.add(paxDetailsConfigReq.getOrigin());
					originDestinations.add(paxDetailsConfigReq.getDestination());
				}
			}
			populateCountryConfig(originDestinations, trackInfo);
			populateNICConfig(segments);
	
			paxDetailsConfigResponse.setPaxCutOffYears(paxCutOffYearsAdaptor.adapt(paxContactDTO.getPaxValidation()));
			
		}catch (ModuleException e) {
			success = false;
			log.error("ERROR in loading pax & contact config", e);
		}
	
		paxDetailsConfigResponse.setPaxDetailsConfig(paxDetailsFields);
		paxDetailsConfigResponse.setSuccess(success);
		paxDetailsConfigResponse.setErrors(null);
		paxDetailsConfigResponse.setMessages(null);
		paxDetailsConfigResponse.setWarnings(null);
		
		return paxDetailsConfigResponse;
	}
	
	public PaxContactDetailsRS paxContactDetails(PaxContactDetailsRQ paxContactDetailsReq, List<String> carrierList,
			TrackInfoDTO trackInfo) {
		
		boolean success = true;
		boolean visibility , mandatory;
		PaxContactDetailsRS paxContactDetailsResponse = new PaxContactDetailsRS();
		paxContactDetailsResponse.setTransactionId(paxContactDetailsReq.getTransactionId());
		PaxContactDetailsConfigAdaptor paxContactDetailsConfigAdaptor = new PaxContactDetailsConfigAdaptor();
		Map<String , PaxContactDetailsField> contactDetailsFields = new HashMap<String, PaxContactDetailsField>();
		
		try {

			if (carrierList == null || carrierList.isEmpty()) {
				if (carrierList == null) {
					carrierList = new ArrayList<>();
				}
				carrierList.add(trackInfo.getCarrierCode());
			}

			PaxContactConfigDTO paxContactDTO = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxContactConfig("AA",
					AppIndicatorEnum.APP_IBE.toString(), carrierList, trackInfo);
			
			for (Object obj : paxContactDTO.getContactConfigList()) {
				if (obj instanceof IBEContactConfigDTO) {
					IBEContactConfigDTO ibeConfig = (IBEContactConfigDTO) obj;
					PaxContactDetailsField paxContactDetailsField = paxContactDetailsConfigAdaptor.adapt(ibeConfig);
					if(contactDetailsFields.get(ibeConfig.getFieldName()) == null){
						contactDetailsFields.put(ibeConfig.getFieldName() , paxContactDetailsField);
					}else{
						visibility = contactDetailsFields.get(ibeConfig.getFieldName()).isVisibility();
						mandatory = contactDetailsFields.get(ibeConfig.getFieldName()).isMandatory();
						contactDetailsFields.get(ibeConfig.getFieldName()).setVisibility(visibility || paxContactDetailsField.isVisibility());
						contactDetailsFields.get(ibeConfig.getFieldName()).setMandatory(mandatory || paxContactDetailsField.isMandatory());
						
						if(contactDetailsFields.get(ibeConfig.getFieldName()).isMandatory() && !contactDetailsFields.get(ibeConfig.getFieldName()).isVisibility()){
							contactDetailsFields.get(ibeConfig.getFieldName()).setVisibility(false);
						}
					}
				}
			}
			
		} catch (ModuleException e) {
			success = false;
			log.error("ERROR in loading pax & contact config", e);
		}
		
		paxContactDetailsResponse.setContactDetails(contactDetailsFields);
		paxContactDetailsResponse.setServiceTaxApplicable(isServiceTaxApplicable(paxContactDetailsReq.getOrigin(), carrierList, trackInfo));
		paxContactDetailsResponse.setSuccess(success);
		paxContactDetailsResponse.setErrors(null);
		paxContactDetailsResponse.setMessages(null);
		paxContactDetailsResponse.setWarnings(null);
		
		return paxContactDetailsResponse;
		
	}
	
	public PaxContactDetailsRS unifiedContactDetails(ContactConfigCollectionRQ contactDetailsReq, List<String> carrierList,
			TrackInfoDTO trackInfo) {

		boolean success = true;
		boolean visibility, mandatory;
		PaxContactDetailsRS paxContactDetailsResponse = new PaxContactDetailsRS();
		paxContactDetailsResponse.setTransactionId(contactDetailsReq.getTransactionId());
		PaxContactDetailsConfigAdaptor paxContactDetailsConfigAdaptor = new PaxContactDetailsConfigAdaptor();
		Map<String, PaxContactDetailsField> contactDetailsFields = new HashMap<String, PaxContactDetailsField>();

		try {

			if (carrierList == null || carrierList.isEmpty()) {
				if (carrierList == null) {
					carrierList = new ArrayList<>();
				}
				carrierList.add(trackInfo.getCarrierCode());
			}

			PaxContactConfigDTO paxContactDTO = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxContactConfig("AA",
					AppIndicatorEnum.APP_IBE.toString(), carrierList, trackInfo);

			for (Object obj : paxContactDTO.getContactConfigList()) {
				if (obj instanceof IBEContactConfigDTO) {
					IBEContactConfigDTO ibeConfig = (IBEContactConfigDTO) obj;
					PaxContactDetailsField paxContactDetailsField = paxContactDetailsConfigAdaptor.adapt(ibeConfig);
					if (contactDetailsFields.get(ibeConfig.getFieldName()) == null) {
						contactDetailsFields.put(ibeConfig.getFieldName(), paxContactDetailsField);
					} else {
						visibility = contactDetailsFields.get(ibeConfig.getFieldName()).isVisibility();
						mandatory = contactDetailsFields.get(ibeConfig.getFieldName()).isMandatory();
						contactDetailsFields.get(ibeConfig.getFieldName())
								.setVisibility(visibility || paxContactDetailsField.isVisibility());
						contactDetailsFields.get(ibeConfig.getFieldName())
								.setMandatory(mandatory || paxContactDetailsField.isMandatory());

						if (contactDetailsFields.get(ibeConfig.getFieldName()).isMandatory()
								&& !contactDetailsFields.get(ibeConfig.getFieldName()).isVisibility()) {
							contactDetailsFields.get(ibeConfig.getFieldName()).setVisibility(false);
						}
					}
				}
			}

		} catch (ModuleException e) {
			success = false;
			log.error("ERROR in loading pax & contact config", e);
		}

		paxContactDetailsResponse.setContactDetails(contactDetailsFields);
		for (PaxContactDetailsRQ contactDetailsRQ : contactDetailsReq.getConfigDetailsRQs()) {
			paxContactDetailsResponse.setServiceTaxApplicable(isServiceTaxApplicable(contactDetailsRQ.getOrigin(), carrierList, trackInfo));
			break;
		}
		paxContactDetailsResponse.setSuccess(success);
		paxContactDetailsResponse.setErrors(null);
		paxContactDetailsResponse.setMessages(null);
		paxContactDetailsResponse.setWarnings(null);

		return paxContactDetailsResponse;

	}
	
	private void populateCountryWisePaxConfigs(Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs,
			Map<String, Boolean> countryPaxConfigAD, Map<String, Boolean> countryPaxConfigCH,
			Map<String, Boolean> countryPaxConfigIN) {

		for (List<PaxCountryConfigDTO> paxCountryConfigs : countryWiseConfigs.values()) {
			for (PaxCountryConfigDTO paxCountryConfig : paxCountryConfigs) {

				if (ReservationInternalConstants.PassengerType.ADULT.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigAD);
				}else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigCH);
				}else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigIN);
				}
				
			}
		}
	}
	
	private void fillPaxCountryConfigMap(PaxCountryConfigDTO paxCountryConfig, Map<String, Boolean> countryPaxConfig) {
		String fieldName = paxCountryConfig.getFieldName();
		if (countryPaxConfig.get(fieldName) == null) {
			countryPaxConfig.put(fieldName, paxCountryConfig.isMandatory());

			// This is only for autocheckin ancillary page
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_VISIBILITY, paxCountryConfig.getAutoCheckinVisibility());
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_MANDATORY, paxCountryConfig.getAutoCheckinMandatory());
		} else {
			Boolean currentValue = countryPaxConfig.get(fieldName);
			countryPaxConfig.put(fieldName, (currentValue || paxCountryConfig.isMandatory()));

			// This is only for autocheckin ancillary page
			Boolean autoCheckinVisibility = countryPaxConfig.get(fieldName + AUTO_CHECKIN_VISIBILITY);
			Boolean autoCheckinMandatory = countryPaxConfig.get(fieldName + AUTO_CHECKIN_MANDATORY);
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_VISIBILITY,
					(autoCheckinVisibility || paxCountryConfig.getAutoCheckinVisibility()));
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_MANDATORY,
					(autoCheckinMandatory || paxCountryConfig.getAutoCheckinMandatory()));
		}
	}
	
	private void overrideWithCountryConfigs(Map<String, Boolean> countryPaxConfig, String paxCategoryType) {
		for (String fieldName : countryPaxConfig.keySet()) {
			if (fieldName.contains("_"))
				continue;
			if (fieldName.equals(BasePaxConfigDTO.FIELD_TRAVELWITH)
					&& (paxCategoryType.equals(ReservationInternalConstants.PassengerType.ADULT) || paxCategoryType
							.equals(ReservationInternalConstants.PassengerType.CHILD))) {
				// travel with field is not available with adults and children
				continue;
			} else if (fieldName.equals(BasePaxConfigDTO.FIELD_TITLE)
					&& paxCategoryType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
				// title is not available with infants
				continue;
			} else {
				if (paxCategoryType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
					paxDetailsFields.get(fieldName).getMandatory().setAdult(countryPaxConfig.get(fieldName));
					paxDetailsFields.get(fieldName).getAutoCheckinVisibility()
							.setAdult(countryPaxConfig.get(fieldName + AUTO_CHECKIN_VISIBILITY));
					paxDetailsFields.get(fieldName).getAutoCheckinMandatory()
							.setAdult(countryPaxConfig.get(fieldName + AUTO_CHECKIN_MANDATORY));
				} else if (paxCategoryType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
					paxDetailsFields.get(fieldName).getMandatory().setChild(countryPaxConfig.get(fieldName));
					paxDetailsFields.get(fieldName).getAutoCheckinVisibility()
							.setChild(countryPaxConfig.get(fieldName + AUTO_CHECKIN_VISIBILITY));
					paxDetailsFields.get(fieldName).getAutoCheckinMandatory()
							.setChild(countryPaxConfig.get(fieldName + AUTO_CHECKIN_MANDATORY));
				} else if (paxCategoryType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
					paxDetailsFields.get(fieldName).getMandatory().setInfant(countryPaxConfig.get(fieldName));
					paxDetailsFields.get(fieldName).getAutoCheckinVisibility()
							.setInfant(countryPaxConfig.get(fieldName + AUTO_CHECKIN_VISIBILITY));
					paxDetailsFields.get(fieldName).getAutoCheckinMandatory()
							.setInfant(countryPaxConfig.get(fieldName + AUTO_CHECKIN_MANDATORY));
				}
			}
		}
	}

	
	public UserRegistrationConfigRS getUserRegistrationConfig(UserRegistrationConfigRQ paxContactDetailsReq,  TrackInfoDTO trackInfo) throws ModuleException{
		
		List<UserRegConfigDTO> configList = ModuleServiceLocator.getGlobalConfig().getUserRegConfig();
		
		List<String[]> questionInfo = SelectListGenerator.getSecreatQuestionList();
		
		UserRegistrationConfigRS registrationsConfig = new UserRegistrationConfigRS();
		registrationsConfig.setQuestionInfo(questionInfo);
		registrationsConfig.setConfigList(configList);
		return registrationsConfig;
		
		
	}
	
	private boolean isIncludeDomesticFlightSegment(List<SessionFlightSegment> segments) {
		if (segments != null && !segments.isEmpty()) {
			return segments.stream().anyMatch(segment -> segment.isDomesticFlight());
		}
		return false;
	}
	
	public CurrencyExchangeRate getCurrencyExchangeRate(String currency) {
		CurrencyExchangeRate currencyExchangeRate = null;
		if (currency != null && !currency.isEmpty()) {
			try {
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(currency, ApplicationEngine.IBE);
			} catch (ModuleException e) {
				log.error("ERROR in loading currencyExchangeRate", e);
			}
		}
		return currencyExchangeRate;
	}
	
	private boolean isServiceTaxApplicable(String originCode, List<String> carrires, TrackInfoDTO trackInfo) {

		if (originCode != null) {
			String originCountry;
			String[] countryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");

			if (carrires.size() > 1 || (carrires.size() == 1 && !carrires.get(0).equals(trackInfo.getCarrierCode()))) {
				try {
					Map<String, CachedAirportDTO> mapAirports = ModuleServiceLocator.getAirportBD()
							.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(originCode)));
					originCountry = mapAirports.get(originCode) != null ? mapAirports.get(originCode).getCountryCode() : "";
				} catch (ModuleException e) {
					log.error("ERROR in loading country code for the origin airport", e);
					return false;
				}
			} else {
				originCountry = (String) ModuleServiceLocator.getGlobalConfig().retrieveAirportInfo(originCode)[3];
			}

			for (String countryCode : countryCodes) {
				if (originCountry.equals(countryCode)) {
					return true;
				}
			}

		}

		return false;

	}

	private PaxContactConfigDTO populatePaxConfig(TrackInfoDTO trackInfo, String searchSystem, List<String> carrierList,
			PaxDetailsConfigAdaptor paxDetailsConfigAdaptor) throws ModuleException {
		boolean visibility;
		boolean mandatory;
		PaxContactConfigDTO paxContactDTO = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxContactConfig(searchSystem,
				AppIndicatorEnum.APP_IBE.toString(), carrierList, trackInfo);
		
		for (Object obj : paxContactDTO.getPaxConfigList()) {
			if (obj instanceof IBEPaxConfigDTO) {
				IBEPaxConfigDTO ibeConfig = (IBEPaxConfigDTO) obj;
				PaxDetailsField paxDetailsField = paxDetailsConfigAdaptor.adapt(ibeConfig);
				if(paxDetailsFields.get(ibeConfig.getFieldName()) == null){
					paxDetailsFields.put(ibeConfig.getFieldName() , paxDetailsField);
				}else{
											
					if (ReservationInternalConstants.PassengerType.ADULT.equals(ibeConfig.getPaxTypeCode())) {
						
						visibility = paxDetailsFields.get(ibeConfig.getFieldName()).getVisibility().isAdult();
						mandatory = paxDetailsFields.get(ibeConfig.getFieldName()).getMandatory().isAdult();
						paxDetailsFields.get(ibeConfig.getFieldName()).getVisibility().setAdult(visibility || ibeConfig.isIbeVisibility());
						paxDetailsFields.get(ibeConfig.getFieldName()).getMandatory().setAdult(mandatory || ibeConfig.isIbeMandatory());
					
					} else if (ReservationInternalConstants.PassengerType.CHILD.equals(ibeConfig.getPaxTypeCode())) {

						visibility = paxDetailsFields.get(ibeConfig.getFieldName()).getVisibility().isChild();
						mandatory = paxDetailsFields.get(ibeConfig.getFieldName()).getMandatory().isChild();
						paxDetailsFields.get(ibeConfig.getFieldName()).getVisibility().setChild(visibility || ibeConfig.isIbeVisibility());
						paxDetailsFields.get(ibeConfig.getFieldName()).getMandatory().setChild(mandatory || ibeConfig.isIbeMandatory());
					
					} else if (ReservationInternalConstants.PassengerType.INFANT.equals(ibeConfig.getPaxTypeCode())) {

						visibility = paxDetailsFields.get(ibeConfig.getFieldName()).getVisibility().isInfant();
						mandatory = paxDetailsFields.get(ibeConfig.getFieldName()).getMandatory().isInfant();
						paxDetailsFields.get(ibeConfig.getFieldName()).getVisibility().setInfant(visibility || ibeConfig.isIbeVisibility());
						paxDetailsFields.get(ibeConfig.getFieldName()).getMandatory().setInfant(mandatory || ibeConfig.isIbeMandatory());
					
					}
				}
			}
		}
		return paxContactDTO;
	}

	private void populateCountryConfig(List<String> originDestinations, TrackInfoDTO trackInfo) throws ModuleException {

		if (AppSysParamsUtil.isCountryPaxConfigEnabled()) {

			// loading country wise configs for above origin and destination
			Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs = ModuleServiceLocator.getAirproxyPassengerBD()
					.loadPaxCountryConfig("AA", AppIndicatorEnum.APP_IBE.toString(), originDestinations, trackInfo);

			if (countryWiseConfigs != null && countryWiseConfigs.size() > 0) {
				Map<String, Boolean> countryPaxConfigAD = new HashMap<String, Boolean>();
				Map<String, Boolean> countryPaxConfigCH = new HashMap<String, Boolean>();
				Map<String, Boolean> countryPaxConfigIN = new HashMap<String, Boolean>();

				// generating pax category type configs according to the most restricted rule
				populateCountryWisePaxConfigs(countryWiseConfigs, countryPaxConfigAD, countryPaxConfigCH,
						countryPaxConfigIN);
				// overriding the configs in action level in IBE since for each reservation this action is calling
				if (countryPaxConfigAD.size() > 0 && countryPaxConfigCH.size() > 0 && countryPaxConfigIN.size() > 0) {
					overrideWithCountryConfigs(countryPaxConfigAD, ReservationInternalConstants.PassengerType.ADULT);
					overrideWithCountryConfigs(countryPaxConfigCH, ReservationInternalConstants.PassengerType.CHILD);
					overrideWithCountryConfigs(countryPaxConfigIN, ReservationInternalConstants.PassengerType.INFANT);
				}
			}
		}
	}

	private void populateNICConfig(List<SessionFlightSegment> segments) {
		if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
			PaxDetailsField nationalIdField = paxDetailsFields.get(BasePaxConfigDTO.FIELD_NATIONAL_ID);
			if (nationalIdField != null) {
				PaxDetailsConfig mandatoryProp = nationalIdField.getMandatory();
				if (isIncludeDomesticFlightSegment(segments)) {
					mandatoryProp.setAdult(true);
				} else {
					mandatoryProp.setAdult(false);
				}
				mandatoryProp.setChild(false);
				mandatoryProp.setInfant(false);
			}
		}
	}

}
