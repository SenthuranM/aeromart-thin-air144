package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;


public class TravelerQuantityAdaptor implements Adaptor<TravellerQuantity, TravelerInfoSummaryTO> {

	TravelerInfoSummaryTO travelerInfoSummaryTO;

	public TravelerQuantityAdaptor(TravelerInfoSummaryTO target) {
		this.travelerInfoSummaryTO = target;
	}

	@Override
	public TravelerInfoSummaryTO adapt(TravellerQuantity source) {
		
		PassengerTypeQuantityTO adultsQuantity = travelerInfoSummaryTO.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		if (source.getAdultCount() > 0) {
			adultsQuantity.setQuantity(source.getAdultCount());
		}

		PassengerTypeQuantityTO childQuantity = travelerInfoSummaryTO.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		if (source.getChildCount() > 0) {
			childQuantity.setQuantity(source.getChildCount());
		}

		PassengerTypeQuantityTO infantQuantity = travelerInfoSummaryTO.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		if (source.getInfantCount() > 0) {
			infantQuantity.setQuantity(source.getInfantCount());
		}
		
		return travelerInfoSummaryTO;
	}

}
