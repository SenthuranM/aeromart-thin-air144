package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;

public class NullAdaptor<S, T> implements Adaptor<S, T> {

	@Override
	public T adapt(S source) {
		return null;
	}

}
