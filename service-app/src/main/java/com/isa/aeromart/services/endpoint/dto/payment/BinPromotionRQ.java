package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;

public class BinPromotionRQ extends TransactionalBaseRQ {

	private List<Passenger> paxInfo;
	private ReservationContact reservationContact;

	private String promoCode;
	private int paymentGatewayId;
	private String cardNo;
	private int cardType;
	private boolean overrideExistingPromotion;

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public int getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(int paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public boolean isOverrideExistingPromotion() {
		return overrideExistingPromotion;
	}

	public void setOverrideExistingPromotion(boolean overrideExistingPromotion) {
		this.overrideExistingPromotion = overrideExistingPromotion;
	}

	public List<Passenger> getPaxInfo() {
		return paxInfo;
	}

	public void setPaxInfo(List<Passenger> paxInfo) {
		this.paxInfo = paxInfo;
	}

	public ReservationContact getReservationContact() {
		return reservationContact;
	}

	public void setReservationContact(ReservationContact reservationContact) {
		this.reservationContact = reservationContact;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

}
