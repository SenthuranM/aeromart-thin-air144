package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.FlexiInfo;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationSegment;
import com.isa.aeromart.services.endpoint.utils.common.LableServiceUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class FlexiInfoAdaptor implements Adaptor<LCCClientAlertInfoTO, FlexiInfo> {
	private String language = LocaleContextHolder.getLocale().getLanguage();
	private final String FLEXI_MSG = "PgFlexi_lblFlexiMessage";
	private final String FLEXI_CNX_MSG = "PgFlexi_lblFlexiCnxMessage";
	private final String FLEXI_MOD_CNX_MSG = "PgFlexi_lblFlexiModCnxMessage";

	private Map<String, String> resSegIdToResSegPHPMap;
	private List<ReservationSegment> reservationSegments;

	private String flexiMessage;
	private String flexiCnxMessage;
	private String flexiModCnxMessage;

	FlexiInfoAdaptor(Map<String, String> resSegIdToResSegPHPMap, List<ReservationSegment> reservationSegments) {
		this.resSegIdToResSegPHPMap = resSegIdToResSegPHPMap;
		this.reservationSegments = reservationSegments;

		this.flexiMessage = LableServiceUtil.getBalanceSummaryDescription(FLEXI_MSG, language);
		this.flexiCnxMessage = LableServiceUtil.getBalanceSummaryDescription(FLEXI_CNX_MSG, language);
		this.flexiModCnxMessage = LableServiceUtil.getBalanceSummaryDescription(FLEXI_MOD_CNX_MSG, language);
	}

	@Override
	public FlexiInfo adapt(LCCClientAlertInfoTO flexiAlert) {
		FlexiInfo flexiInfo = new FlexiInfo();
		for (ReservationSegment resSeg : reservationSegments) {
			if (resSegIdToResSegPHPMap.get(flexiAlert.getFlightSegmantRefNumber()).equals(resSeg.getReservationSegmentRPH())
					&& !resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				List<LCCClientAlertTO> alertsList = flexiAlert.getAlertTO();
				for (LCCClientAlertTO alertTO : alertsList) {
					if (alertTO.getAlertId() == 1) {
						flexiMessage = flexiMessage.replace("#1", alertTO.getContent());
					} else if (alertTO.getAlertId() == 2) {
						flexiMessage = flexiMessage.replace("#3", flexiModCnxMessage);
					} else if (alertTO.getAlertId() == 0) {
						flexiMessage = flexiMessage.replace("#2", alertTO.getContent());
						flexiCnxMessage = flexiCnxMessage.replace("#2", alertTO.getContent());
					}
				}
				if (flexiMessage.contains("#1")) {
					flexiMessage = flexiCnxMessage;
				}
				if (flexiMessage.contains("#3")) {
					flexiMessage = flexiMessage.replace("#3", "");
				}

				flexiInfo.setSegmentCode(resSeg.getSegment().getSegmentCode());
				flexiInfo.setFlexiDetails(flexiMessage);
			}
		}
		return flexiInfo;
	}
}
