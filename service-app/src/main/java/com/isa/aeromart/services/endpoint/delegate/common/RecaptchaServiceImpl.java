package com.isa.aeromart.services.endpoint.delegate.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isa.aeromart.services.endpoint.dto.common.CaptchaVerifyRQ;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;

@Service
public class RecaptchaServiceImpl implements RecaptchaService {

	private static Log log = LogFactory.getLog(RecaptchaServiceImpl.class);
	private static final String USER_AGENT = "Mozilla/5.0";

	private boolean checkStatus(String gRecaptchaResponse) throws ClientProtocolException, IOException {
		if (!AppSysParamsUtil.isRecaptchaEnabled()) {
			return false;
		}
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

		HttpClient client = null;
		if (globalConfig.getUseProxy()) {
			HttpHost proxy = new HttpHost(globalConfig.getHttpProxy(), globalConfig.getHttpPort());
			client = HttpClientBuilder.create().setProxy(proxy).build();
		} else {
			client = HttpClientBuilder.create().build();
		}

		HttpPost post = new HttpPost(AppSysParamsUtil.getRecaptchaURL());

		post.setHeader("User-Agent", USER_AGENT);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("secret", AppSysParamsUtil.getRecaptchaSecretKey()));
		urlParameters.add(new BasicNameValuePair("response", gRecaptchaResponse));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);

		if (response.getStatusLine().getStatusCode() == 200) {
			ObjectMapper objectMapper = new ObjectMapper();
			GRecaptchaVerifyRS responseObject = objectMapper.readValue(response.getEntity().getContent(),
					GRecaptchaVerifyRS.class);
			if (!responseObject.isSuccess()) {
				log.debug("Captcha validation fails :" + responseObject.toString());
			}
			return responseObject.isSuccess();
		} else {
			return false;
		}
	}

	@Override
	public boolean verifyRequest(CaptchaVerifyRQ captchaReq) {
		try {
			return checkStatus(captchaReq.getUserReponseToken());
		} catch (IOException e) {
			log.error(e);
		}
		return false;
	}
}
