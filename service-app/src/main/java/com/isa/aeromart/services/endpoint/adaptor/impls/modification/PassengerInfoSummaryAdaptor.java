package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class PassengerInfoSummaryAdaptor implements Adaptor<TravelerInfoSummaryTO,TravellerQuantity>{

	@Override
	public TravellerQuantity adapt(TravelerInfoSummaryTO source) {
		TravellerQuantity travellerQuantity = new TravellerQuantity();
		if (source.getPassengerTypeQuantityList() != null && source.getPassengerTypeQuantityList().size() != 0) {
			for (PassengerTypeQuantityTO pax : source.getPassengerTypeQuantityList()) {
				if (pax.getPassengerType().equals(PaxTypeTO.ADULT)) {
					travellerQuantity.setAdultCount(pax.getQuantity());
				} else if (pax.getPassengerType().equals(PaxTypeTO.CHILD)) {
					travellerQuantity.setChildCount(pax.getQuantity());
				} else if (pax.getPassengerType().equals(PaxTypeTO.INFANT)) {
					travellerQuantity.setInfantCount(pax.getQuantity());
				}
			}
		}
		return travellerQuantity;
	}
}
