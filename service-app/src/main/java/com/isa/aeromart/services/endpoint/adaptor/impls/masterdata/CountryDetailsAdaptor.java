package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.CountryDetails;
import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;

public class CountryDetailsAdaptor implements Adaptor<CountryDetailsDTO, CountryDetails>{

	@Override
	public CountryDetails adapt(CountryDetailsDTO source) {
		
		CountryDetails countryDetails = new CountryDetails();
		
		countryDetails.setCountryCode(source.getCountryCode());
		countryDetails.setCountryName(source.getCountryName());
		countryDetails.setNationalityCode(source.getNationalityCode());
		countryDetails.setNationalityDes(source.getNationalityDes());
		countryDetails.setPhoneCode(source.getPhoneCode());
		
		return countryDetails;
		
	}

}
