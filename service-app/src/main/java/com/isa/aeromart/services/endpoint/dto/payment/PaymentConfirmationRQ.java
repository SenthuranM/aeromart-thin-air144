package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class PaymentConfirmationRQ extends TransactionalBaseRQ {

	private String postParam;

	private Map<String, String> paymentReceiptMap = null;
	
	private String paymode;

	public String getPostParam() {
		return postParam;
	}

	public void setPostParam(String postParam) {
		this.postParam = postParam;
	}

	public Map<String, String> getPaymentReceiptMap() {
		return paymentReceiptMap;
	}

	public String getPaymode() {
		return paymode;
	}

	public void setPaymode(String paymode) {
		this.paymode = paymode;
	}

	public void setPaymentReceiptMap(Map<String, String> paymentReceiptMap) {
		this.paymentReceiptMap = paymentReceiptMap;
	}
}
