package com.isa.aeromart.services.endpoint.delegate.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.dto.payment.VoucherInformation;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.ContactInformation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Passenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.AncillaryTransaction;
import com.isa.aeromart.services.endpoint.dto.session.RequoteTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySelection;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySession;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionContactInformation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionMetaData;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.ReservationChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.session.TransactionFactory;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

@Service("transactionalAncillaryService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TransactionalAncillaryServiceImpl implements TransactionalAncillaryService {

	@Autowired
	private TransactionFactory transactionFactory;

	public void clearSession(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);

		if (ancillarySessionStore != null) {
			AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();
			ancillarySession.init();
		}
	}

	public List<InventoryWrapper> getFlightSegments(String transactionId) {
		List<InventoryWrapper> inventory = new ArrayList<>();
		InventoryWrapper inventoryWrapper;

		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		List<SessionFlightSegment> flightSegments = ancillarySessionStore.getSegmentWithInventory();

		if (flightSegments != null) {
			for (SessionFlightSegment flightSegment : flightSegments) {
				inventoryWrapper = new InventoryWrapper(flightSegment);
				inventory.add(inventoryWrapper);
			}
		}

		return inventory;
	}

	public TravellerQuantity getTravellerQuantityFromSession(String transactionId) {

		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		TravellerQuantity travellerQuantity = ancillarySessionStore.getTravellerQuantityForAncillary();

		return travellerQuantity;
	}

	public PreferenceInfo getPreferencesForAncillary(String transactionId) {

		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		PreferenceInfo preferenceInfo = ancillarySessionStore.getPreferencesForAncillary();

		if (preferenceInfo == null) {
			preferenceInfo = new PreferenceInfo();
			preferenceInfo.setSelectedSystem(ProxyConstants.SYSTEM.AA.name());

		} else {
			if (preferenceInfo.getSelectedSystem() == null) {
				preferenceInfo.setSelectedSystem(ProxyConstants.SYSTEM.AA.name());
			}
		}

		return preferenceInfo;
	}

	public FareSegChargeTO getPricingInformation(String transactionId) {

		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		FareSegChargeTO fareSegChargeTO = ancillarySessionStore.getFarePricingInformation();

		return fareSegChargeTO;
	}

	public ReservationInfo getReservationInfo(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ReservationInfo reservationInfo = ancillarySessionStore.getResInfo();

		return reservationInfo;
	}

	public RPHGenerator getRphGenerator(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getRPHGenerator();
	}

	public void storeReservationData(String transactionId, BasicReservation basicReservation) {

		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();

		ContactInformation contactInformation;

		List<SessionPassenger> sessionPassengers;
		SessionPassenger sessionPassenger;
		SessionContactInformation sessionContactInformation;

		SessionBasicReservation sessionBasicReservation = new SessionBasicReservation();
		sessionPassengers = new ArrayList<>();

		ReservationInfo reservationInfo = null;
		if (ancillarySessionStore.getResInfo() != null) {
			reservationInfo = ancillarySessionStore.getResInfo();
		} else {
			reservationInfo = new ReservationInfo();
		}
		List<ReservationPaxTO> paxList = new ArrayList<>();
		ReservationPaxTO pax;

		for (Passenger passenger : basicReservation.getPassengers()) {
			sessionPassenger = new SessionPassenger();
			sessionPassenger.setTitle(passenger.getTitle());
			sessionPassenger.setFirstName(passenger.getFirstName());
			sessionPassenger.setLastName(passenger.getLastName());
			sessionPassenger.setPassengerRph(passenger.getPassengerRph());
			sessionPassenger.setType(passenger.getType());
			sessionPassenger.setParent(AnciCommon.isParent(passenger, basicReservation.getPassengers()));
			sessionPassenger.setDateOfBirth(passenger.getDateOfBirth());

			sessionPassengers.add(sessionPassenger);

			pax = new ReservationPaxTO();
			pax.setIsParent(false);
			pax.setPaxType(passenger.getType());
			pax.setTitle(passenger.getTitle());
			pax.setFirstName(passenger.getFirstName());
			pax.setLastName(passenger.getLastName());
			pax.setIsParent(sessionPassenger.isParent());
			pax.setSeqNumber(Integer.valueOf(passenger.getPassengerRph()));
			pax.setTravelerRefNumber(passenger.getTravelerRefNumber());
			pax.setInfantWith(AnciCommon.getInfantWith(passenger, basicReservation.getPassengers()));
			paxList.add(pax);
		}
		sessionBasicReservation.setPassengers(sessionPassengers);
        reservationInfo.setPaxList(paxList);

		contactInformation = basicReservation.getContactInfo();
		if (contactInformation != null) {
			sessionContactInformation = new SessionContactInformation();
			sessionContactInformation.setTitle(contactInformation.getTitle());
			sessionContactInformation.setFirstName(contactInformation.getFirstName());
			sessionContactInformation.setLastName(contactInformation.getLastName());
			sessionContactInformation.setAddress(contactInformation.getAddress());
			sessionContactInformation.setLandNumber(contactInformation.getLandNumber());
			sessionContactInformation.setMobileNumber(contactInformation.getMobileNumber());
			sessionContactInformation.setEmailAddress(contactInformation.getEmailAddress());
			sessionContactInformation.setCountry(contactInformation.getCountry());

			sessionBasicReservation.setContactInformation(sessionContactInformation);
		}

		ancillarySession.setReservation(sessionBasicReservation);

		ancillarySessionStore.storeResInfo(reservationInfo);

	}

	@Override
	public SessionBasicReservation getReservationData(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);

		AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();
		if (ancillarySessionStore != null && ancillarySessionStore.getPostPaymentInformation() != null
				&& ancillarySession != null && ancillarySession.getReservation() != null
				&& ancillarySessionStore.isOnHoldCreated()) {
			ancillarySession.getReservation().setPostPaymentExist(true);
			ancillarySession.getReservation().setPnr(ancillarySessionStore.getPostPaymentInformation().getPnr());
		}
		return ancillarySession.getReservation();
	}

	public void addBlockedAncillaries(String transactionId, MetaData metaData, List<SelectedAncillary> ancillaries) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);

		AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();
		AncillarySelection ancillarySelection = ancillarySession.getAncillarySelection();

		ancillarySelection.setBlockedAncillaries(ancillaries);

	}

	public void storeAncillaryPreferences(String transactionId, MetaData metaData, List<SelectedAncillary> ancillaries) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);

		AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();
		AncillarySelection ancillarySelection = ancillarySession.getAncillarySelection();

		ancillarySelection.setAncillaryPreferences(ancillaries);
		ancillarySelection.setMetaData(new SessionMetaData());
		ancillarySelection.getMetaData().setOndUnits(metaData.getOndPreferences());

	}

	public void storeAncillaryQuotations(String transactionId, MetaData metaData, List<AncillaryType> ancillaryQuotations) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);

		AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();
		AncillaryQuotation ancillaryQuotation = ancillarySession.getAncillaryQuotation();

		ancillaryQuotation.setQuotedAncillaries(ancillaryQuotations);
	}

	@Override
	public AncillaryQuotation getAncillaryQuotation(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);

		AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();
		return ancillarySession.getAncillaryQuotation();
	}

	public AncillarySelection getAncillarySelectionsFromSession(String transactionId) {

		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);

		AncillarySession ancillarySession = ancillarySessionStore.getAncillarySession();
		return ancillarySession.getAncillarySelection();
	}

	@Override
	public void storeAncillaryCharges(String transactionId, ReservationChargeTo<LCCClientExternalChgDTO> reservationCharges) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		if(ancillarySessionStore instanceof RequoteTransaction){
			mergePassengerChargesWithExisting(ancillarySessionStore, reservationCharges);
		}
		ancillarySessionStore.getFinancialStore().setAncillaryExternalCharges(reservationCharges);
	}

	@Override
	public List<ModifiedFlight> getModifications(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getModifiedFlightsInfo();
	}

	@Override
	public Map<String, String> getResSegRphToFlightSegMap(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getResSegRphToFlightSegMap();
	}

	@Override
	public PostPaymentDTO getPaymentData(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getPostPaymentInformation();
	}

    @Override
    public LoyaltyInformation getLoyaltyInformation(String transactionId) {
        AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
        return ancillarySessionStore.getLoyaltyInformation();
    }

    // ----------------------------------------

	private AncillarySessionStore getAncillarySessionStore(String transactionId) {
		AncillarySessionStore ancillarySessionStore = null;
		ancillarySessionStore = transactionFactory.getTransactionStore(transactionId);
		return ancillarySessionStore;
	}

	@Override
	public void storeReservationInfo(String transactionId, ReservationInfo resInfo) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storeReservationDataForAncillaryModification(resInfo);

	}

	@Override
	public void storeRPHGenerator(String transactionId, RPHGenerator rphGenerator) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storeRPHGeneratorForAncillaryModification(rphGenerator);

	}

	@Override
	public void storePreferenceInfo(String transactionId, PreferenceInfo preferenceInfo) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storePreferenceInfoForAncillaryModification(preferenceInfo);

	}

	@Override
	public void storeSessionSegments(String transactionId, List<FlightSegmentTO> segments) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storeSessionFlightsForAncillaryModification(segments);

	}

	@Override
	public void storePricingInformtation(String transactionId, FareSegChargeTO pricingInformation) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storePricingInformationForAncillaryModification(pricingInformation);

	}

	@Override
	public String initializeAnciModifyTransaction(String transactionId) {
		return transactionFactory.initTransaction(AncillaryTransaction.SESSION_KEY, transactionId);
	}

	@Override
	public List<SessionFlexiDetail> getSessionFlexiDetail(String transactionId) {
		
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		
		return ancillarySessionStore.getSessionFlexiDetailForAncillary();
	}
	
	private void mergePassengerChargesWithExisting(AncillarySessionStore ancillarySessionStore,
			ReservationChargeTo<LCCClientExternalChgDTO> reservationCharges) {
		FinancialStore financialStore = ancillarySessionStore.getFinancialStore();
		List<PassengerChargeTo<LCCClientExternalChgDTO>> existingPaxCharges = financialStore.getAncillaryExternalCharges()
				.getPassengers();
		if (existingPaxCharges != null && existingPaxCharges.size() != 0) {
			for(PassengerChargeTo<LCCClientExternalChgDTO> existingPaxCharge : existingPaxCharges){
				for(LCCClientExternalChgDTO lccClientExternalChgDTO : existingPaxCharge.getGetPassengerCharges()){
					if(lccClientExternalChgDTO.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)){
						for(PassengerChargeTo<LCCClientExternalChgDTO> modifiedPaxCharge : reservationCharges.getPassengers()){
							boolean flexiSelectedOnAncillary = false;
							for(LCCClientExternalChgDTO modifiedExtCharge : modifiedPaxCharge.getGetPassengerCharges()){
								if(modifiedExtCharge.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)){
									flexiSelectedOnAncillary = true;
								}
							}
							if(!flexiSelectedOnAncillary && modifiedPaxCharge.getPassengerRph().equals(existingPaxCharge.getPassengerRph())){
								modifiedPaxCharge.getGetPassengerCharges().add(lccClientExternalChgDTO);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public boolean isAncillaryTransaction(String transactionId) {
		
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		if(ancillarySessionStore instanceof AncillaryTransaction){
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isRequoteTransaction(String transactionId) {
		
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		if(ancillarySessionStore instanceof RequoteTransaction){
			return true;
		} else {
			return false;
		}
	}

	@Override
	public DiscountedFareDetails getDiscountedFareDetails(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getDiscountedFareDetailsForAncillary();
	}

	@Override
	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalCharges(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getPaxAncillaryExternalChargesForAncillary();
	}

	@Override
	public List<SessionFlightSegment> getSelectedSegments(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getSelectedSegmentsForAncillary();
	}

	@Override
	public void storeUpdatedSessionFlightSegment(String transactionId, List<SessionFlightSegment> segments) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storeUpdatedSessionFlightSegment(segments);
	}

	@Override
	public void setDiscounAmount(String transactionId, BigDecimal discountAmount) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.setDiscountAmount(discountAmount);
	}

	@Override
	public void storeServiceCount(String transactionId, Map<String, Integer> serviceCount) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storeServiceCount(serviceCount);
		
	}
	
	@Override
	public void storeApplicableServiceTaxes(String transactionId, Set<ServiceTaxContainer> applicableServiceTaxes) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		ancillarySessionStore.storeApplicableServiceTaxes(applicableServiceTaxes);		
	}


	@Override
	public Map<String, Integer> getServiceCount(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getServiceCount();
	}

	public boolean isRegUser() {
		
		boolean isRegUser = false;
		CustomerSessionStore customerSessionStore = null;
		
		customerSessionStore = transactionFactory.getSessionStore(CustomerSessionStore.SESSION_KEY);
		if(customerSessionStore != null && customerSessionStore.getLoggedInCustomerID() != null && !customerSessionStore.getLoggedInCustomerID().isEmpty()){
			isRegUser = true;
		}
		return isRegUser;
	}

	@Override
	public boolean isOnHoldCreated(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		
		return ancillarySessionStore.isOnHoldCreated();
	}

	@Override
	public PaymentBaseSessionStore getPaymentBaseSessionStore(
			String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	@Override
	public List<InventoryWrapper> getAllFlightSegments(String transactionId) {
		List<InventoryWrapper> inventory = new ArrayList<>();
		InventoryWrapper inventoryWrapper;

		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		List<SessionFlightSegment> flightSegments = ancillarySessionStore.getAllSegmentWithInventory();

		if (flightSegments != null) {
			for (SessionFlightSegment flightSegment : flightSegments) {
				inventoryWrapper = new InventoryWrapper(flightSegment);
				inventory.add(inventoryWrapper);
			}
		}

		return inventory;
	}

	@Override
	public PayByVoucherInfo getPayByVoucherInfo(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getPayByVoucherInfo();
	}

	@Override
	public VoucherInformation getVoucherInformation(String transactionId) {
		AncillarySessionStore ancillarySessionStore = getAncillarySessionStore(transactionId);
		return ancillarySessionStore.getVoucherInformation();
	}

}
