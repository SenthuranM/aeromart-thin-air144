package com.isa.aeromart.services.endpoint.dto.customer;

public class FamilyRelationshipDTO {
	
	private String id;

	private String name;
	
	private int minimumAgeInDays;
	
	private int maximumAgeInDays;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMinimumAgeInDays() {
		return minimumAgeInDays;
	}

	public void setMinimumAgeInDays(int minimumAgeInDays) {
		this.minimumAgeInDays = minimumAgeInDays;
	}

	public int getMaximumAgeInDays() {
		return maximumAgeInDays;
	}

	public void setMaximumAgeInDays(int maximumAgeInDays) {
		this.maximumAgeInDays = maximumAgeInDays;
	}

}
