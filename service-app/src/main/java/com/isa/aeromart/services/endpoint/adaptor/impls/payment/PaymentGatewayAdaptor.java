package com.isa.aeromart.services.endpoint.adaptor.impls.payment;

import java.util.Properties;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;

public class PaymentGatewayAdaptor implements Adaptor<IPGPaymentOptionDTO, PaymentGateway> {

	private Properties ipgProps;

	public PaymentGatewayAdaptor(Properties ipgProps) {
		this.ipgProps = ipgProps;
	}

	@Override
	public PaymentGateway adapt(IPGPaymentOptionDTO tmpIPGPaymentOptionDTO) {
		PaymentGateway paymentGateway = null;

		paymentGateway = new PaymentGateway();
		boolean isViewPaymentInIframe = Boolean.parseBoolean(ipgProps
				.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_VIEW_PAYMENT_IFRAME));
		boolean isSwitchToExternalURL = Boolean.parseBoolean(ipgProps
				.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SWITCH_TO_EXTERNAL_URL));
		String brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);

		paymentGateway.setGatewayId(tmpIPGPaymentOptionDTO.getPaymentGateway());
		paymentGateway.setPaymentCurrency(tmpIPGPaymentOptionDTO.getBaseCurrency());
		paymentGateway.setProviderCode(tmpIPGPaymentOptionDTO.getProviderCode());
		paymentGateway.setSwitchToExternalUrl(isSwitchToExternalURL);
		paymentGateway.setViewPaymentInIframe(isViewPaymentInIframe);
		paymentGateway.setProviderName(tmpIPGPaymentOptionDTO.getProviderName());
		paymentGateway.setBrokerType(brokerType);
		paymentGateway.setCvvOption(tmpIPGPaymentOptionDTO.getCvvOption());
		paymentGateway.setSaveCard(tmpIPGPaymentOptionDTO.isSaveCard());

		return paymentGateway;
	}

}
