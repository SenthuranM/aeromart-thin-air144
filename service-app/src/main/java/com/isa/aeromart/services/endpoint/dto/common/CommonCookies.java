package com.isa.aeromart.services.endpoint.dto.common;

public interface CommonCookies {

	public static final String AUTH_TOKEN = "authToken";
}
