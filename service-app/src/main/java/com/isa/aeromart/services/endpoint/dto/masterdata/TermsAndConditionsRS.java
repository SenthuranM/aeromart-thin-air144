package com.isa.aeromart.services.endpoint.dto.masterdata;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class TermsAndConditionsRS extends TransactionalBaseRS{

	private String termsNConditionsSummary;
	
	private String termsNConditionsFull;

	public String getTermsNConditionsSummary() {
		return termsNConditionsSummary;
	}

	public void setTermsNConditionsSummary(String termsNConditionsSummary) {
		this.termsNConditionsSummary = termsNConditionsSummary;
	}

	public String getTermsNConditionsFull() {
		return termsNConditionsFull;
	}

	public void setTermsNConditionsFull(String termsNConditionsFull) {
		this.termsNConditionsFull = termsNConditionsFull;
	}
	
}
