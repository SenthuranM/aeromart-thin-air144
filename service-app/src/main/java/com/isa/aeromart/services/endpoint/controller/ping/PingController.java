package com.isa.aeromart.services.endpoint.controller.ping;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.dto.common.CommonCookies;
import com.isa.aeromart.services.endpoint.dto.common.SimpleResponse;
import com.isa.aeromart.services.endpoint.dto.ping.PingParams;
import com.isa.aeromart.services.endpoint.dto.ping.PingResponse;
import com.isa.aeromart.services.endpoint.service.ValidationService;

@Controller
@RequestMapping("ping")
public class PingController extends StatefulController {

	@Autowired
	private ValidationService validationService;

	@ResponseBody
	@RequestMapping(value = "/ping/{echoToken}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public SimpleResponse<String> pingGet(@PathVariable String echoToken) {

		return new SimpleResponse<String>(echoToken);
	}

	@ResponseBody
	@RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			SimpleResponse<PingResponse> pingPost(@Validated @RequestBody PingParams pingParams) {

		validationService.isValidPingRequest(pingParams);

		PingResponse response = new PingResponse();
		response.setCounter(pingParams.getCounter());
		response.setResponseTime(new Date());
		return new SimpleResponse<PingResponse>(response);

	}

	@ResponseBody
	@RequestMapping(value = "/pingToken", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			SimpleResponse<PingResponse> pingWithToken(@CookieValue(CommonCookies.AUTH_TOKEN) String authToken,
					@RequestBody PingParams pingParams) {

		PingResponse response = new PingResponse();
		response.setCounter(pingParams.getCounter());
		response.setEchoToken(pingParams.getEchoToken());
		return new SimpleResponse<PingResponse>(response);

	}
}
