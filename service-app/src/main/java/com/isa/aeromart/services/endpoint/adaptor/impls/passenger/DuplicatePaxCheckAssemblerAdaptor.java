package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class DuplicatePaxCheckAssemblerAdaptor implements Adaptor<List<Passenger>, DuplicateValidatorAssembler> {

	private BookingSessionStore bookingStore;

	public DuplicatePaxCheckAssemblerAdaptor(BookingSessionStore bookingStore) {
		this.bookingStore = bookingStore;
	}

	@Override
	public DuplicateValidatorAssembler adapt(List<Passenger> passengers) {

		DuplicateValidatorAssembler duplicateValidatorAssembler = new DuplicateValidatorAssembler();

		DuplicateCheckInfoAdapter duplicateCheckInfoAdapter = new DuplicateCheckInfoAdapter();
		DuplicateCheckFlightSegAdaptor duplicateCheckFlightSegAdaptor = new DuplicateCheckFlightSegAdaptor();

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<>();

		List<LCCClientReservationPax> duplicateCheckPaxList = new ArrayList<>();

		AdaptorUtils.adaptCollection(passengers, duplicateCheckPaxList, duplicateCheckInfoAdapter);
		AdaptorUtils.adaptCollection(bookingStore.getSegments(), flightSegmentTOs, duplicateCheckFlightSegAdaptor);

		duplicateValidatorAssembler.setPaxList(duplicateCheckPaxList);
		duplicateValidatorAssembler.setFlightSegments(flightSegmentTOs);

		return duplicateValidatorAssembler;
	}

}
