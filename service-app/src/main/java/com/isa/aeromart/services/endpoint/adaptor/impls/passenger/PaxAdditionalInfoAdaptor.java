package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.AdditionalInfo;
import com.isa.aeromart.services.endpoint.dto.common.DocInfo;
import com.isa.aeromart.services.endpoint.dto.common.FoidInfo;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class PaxAdditionalInfoAdaptor implements Adaptor<AdditionalInfo, PaxAdditionalInfoDTO> {

	@Override
	public PaxAdditionalInfoDTO adapt(AdditionalInfo additionalInfo) {
		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
		paxAdditionalInfoDTO.setFfid(additionalInfo.getFfid());

		if (AppSysParamsUtil.isLMSEnabled()) {
			paxAdditionalInfoDTO.setFfid(additionalInfo.getFfid());
		}

		paxAdditionalInfoDTO.setNationalIDNo(additionalInfo.getNic());
		if (additionalInfo.getFoidInfo() != null) {
			FoidInfo foidInfo = additionalInfo.getFoidInfo();
			paxAdditionalInfoDTO.setPassportExpiry(foidInfo.getFoidExpiry());
			paxAdditionalInfoDTO.setPassportNo(foidInfo.getFoidNumber());
			paxAdditionalInfoDTO.setPassportIssuedCntry(foidInfo.getFoidPlace());
			paxAdditionalInfoDTO.setPlaceOfBirth(foidInfo.getPlaceOfBirth());
		}

		if (additionalInfo.getDocInfo() != null) {
			DocInfo docInfo = additionalInfo.getDocInfo();
			paxAdditionalInfoDTO.setVisaDocIssueDate(docInfo.getDocIssueDate());
			paxAdditionalInfoDTO.setTravelDocumentType(docInfo.getTravelDocumentType());
			paxAdditionalInfoDTO.setVisaApplicableCountry(docInfo.getApplicableCountry());
			paxAdditionalInfoDTO.setVisaDocNumber(docInfo.getDocNumber());
			paxAdditionalInfoDTO.setVisaDocPlaceOfIssue(docInfo.getDocPlaceOfIssue());
		}

		return paxAdditionalInfoDTO;
	}

}
