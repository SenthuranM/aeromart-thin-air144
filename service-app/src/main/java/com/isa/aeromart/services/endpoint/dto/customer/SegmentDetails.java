package com.isa.aeromart.services.endpoint.dto.customer;

public class SegmentDetails implements Comparable<SegmentDetails> {

	private String arrivalDateTime;

	private String departureDateTime;

	private String filghtDesignator;

	private String orignNDest;

	private String status;

	private long departureDateLong; // added to sort SegmentDetails list
	
	private String segmentCode;
	
	private boolean returnFlag;
	
	/** Holds reservation segment id */
	private Integer pnrSegId;
	
	/** Holds the Ground Segment Code if the segment is a Ground Segment */
	private String subStationShortName;

	public String getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(String arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getFilghtDesignator() {
		return filghtDesignator;
	}

	public void setFilghtDesignator(String filghtDesignator) {
		this.filghtDesignator = filghtDesignator;
	}

	public String getOrignNDest() {
		return orignNDest;
	}

	public void setOrignNDest(String orignNDest) {
		this.orignNDest = orignNDest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getDepartureDateLong() {
		return departureDateLong;
	}

	public void setDepartureDateLong(long departureDateLong) {
		this.departureDateLong = departureDateLong;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
	
	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * Compare departure date time using date's long value.
	 * 
	 * @param segmentDetails
	 *            SegmentDetails object to be compared with
	 * @return 0 if the argument Date is equal to this Date; -1 if this Date is before the passed Date; and 1 if this
	 *         Date is after the Date argument
	 */
	@Override
	public int compareTo(SegmentDetails segmentDetails) {
		if (this.departureDateLong > segmentDetails.getDepartureDateLong()) {
			return 1; // After
		} else if (this.departureDateLong < segmentDetails.getDepartureDateLong()) {
			return -1; // Before
		} else {
			return 0; // Same Date
		}
	}
	
	/**
	 * @return the pnrSegId
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return the subStationShortName
	 */
	public String getSubStationShortName() {
		return subStationShortName;
	}

	/**
	 * @param subStationShortName the subStationShortName to set
	 */
	public void setSubStationShortName(String subStationShortName) {
		this.subStationShortName = subStationShortName;
	}
}
