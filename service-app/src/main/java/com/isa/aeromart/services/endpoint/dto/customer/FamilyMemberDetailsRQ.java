package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class FamilyMemberDetailsRQ extends TransactionalBaseRQ {

	private List<FamilyMemberDTO> familyMemberDTOList = new ArrayList<FamilyMemberDTO>();

	public List<FamilyMemberDTO> getFamilyMemberDTOList() {
		return familyMemberDTOList;
	}

	public void setFamilyMemberDTOList(List<FamilyMemberDTO> familyMemberDTOList) {
		this.familyMemberDTOList = familyMemberDTOList;
	}

}
