package com.isa.aeromart.services.endpoint.dto.externalagent;

public class ExternalAgentValidationRQ {

	private String hashKey;

	private String merchantID;

	private String encryptedDate;

	private String date;

	public String getHashKey() {
		return hashKey;
	}

	public void setHashKey(String hashKey) {
		this.hashKey = hashKey;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getEncryptedDate() {
		return encryptedDate;
	}

	public void setEncryptedDate(String encryptedDate) {
		this.encryptedDate = encryptedDate;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
