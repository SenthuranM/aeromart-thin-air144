package com.isa.aeromart.services.endpoint.dto.session.store;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;

public interface CustomerSessionStore {

	public static String SESSION_KEY = CustomerSession.SESSION_KEY;

	public String getLoggedInCustomerID();

	public void storeLoggedInCustomerID(String customerID);

	public double getAvailableCredit();

	public void storeAvailableCredit(double availableCredit);

	public double getAvailableLMSPoints();

	public void storeAvailableLMSPoints(double availableCredit);

	public void storeFFID(String ffid);

	public String getMemberAuth();
	
	public void storeMemberAuth(String authToken);
	
	public String getLMSRemoteId();
	
	public void storeMemberEnrollimgCarrier(String memberEnrollingCarrier);
	
	public String getMemberEnrollingCarrier();
	
	public void storeLMSRemoteId(String lmsRemoteId);
	
	public String getFFID();
	
	public void clearSession();

	public RPHGenerator getFamilyMemberIDHolder();
	
}
