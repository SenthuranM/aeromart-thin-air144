package com.isa.aeromart.services.endpoint.utils.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.FlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.dto.availability.OndBookingClassInfo;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.ServiceTaxRQ;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionExternalChargeSummary;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.ReservationChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.booking.BookingUtil;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class PaymentUtils {

	private static Log log = LogFactory.getLog(PaymentUtils.class);

	private static List<EXTERNAL_CHARGES> excludingChargeCodesForServiceTaxes = Arrays.asList(EXTERNAL_CHARGES.JN_OTHER);

	public IPGPaymentOptionDTO getIpgPaymentOptionDTO(String selectedCurrency, boolean offLineEnabled, String moduleCode) {
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		ipgPaymentOptionDTO.setSelCurrency(selectedCurrency);
		ipgPaymentOptionDTO.setOfflinePayment(offLineEnabled);
		ipgPaymentOptionDTO.setModuleCode(moduleCode);
		return ipgPaymentOptionDTO;
	}

	public IPGIdentificationParamsDTO validateAndPrepareIPGConfigurationParamsDTO(Integer ipgId, String paymentCurrencyCode)
			throws ModuleException {

		if (ipgId == null && paymentCurrencyCode == null) {
			log.warn("IPG identification params with carrier or pay currency unspecified found " + "[ipgId=" + ipgId
					+ ",payCurCode=" + paymentCurrencyCode + "]");
		}

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, paymentCurrencyCode);
		ipgIdentificationParamsDTO.setFQIPGConfigurationName(ipgId + "_" + paymentCurrencyCode);
		ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
		return ipgIdentificationParamsDTO;
	}

	public Collection<Integer> getPaymentGatewayIds(Collection<IPGPaymentOptionDTO> paymentGateways) {
		Set<Integer> gatewayIDs = new HashSet<Integer>();
		if (paymentGateways != null) {
			for (IPGPaymentOptionDTO iPGPaymentOptionDTO : paymentGateways) {
				gatewayIDs.add(iPGPaymentOptionDTO.getPaymentGateway());
			}
		}
		return gatewayIDs;
	}

	private ServiceTaxContainer getServiceTax(EXTERNAL_CHARGES taxChargeCode, List<ServiceTaxContainer> applicableServiceTaxes) {
		if (taxChargeCode != null) {
			for (ServiceTaxContainer serviceTaxContainer : applicableServiceTaxes) {
				if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == taxChargeCode) {
					return serviceTaxContainer;
				}
			}
		}
		return null;
	}

	private BigDecimal getServiceCharge(EXTERNAL_CHARGES externalCharge,
			Collection<LCCClientExternalChgDTO> lccClientExternalChgDTOs) {
		BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (lccClientExternalChgDTOs != null && !lccClientExternalChgDTOs.isEmpty()) {
			for (LCCClientExternalChgDTO externalChgDTO : lccClientExternalChgDTOs) {
				if (externalChgDTO.getExternalCharges() == externalCharge) {
					serviceCharge = externalChgDTO.getAmount();
				}
			}
		}
		return serviceCharge;
	}

	private BigDecimal getServiceTaxRatio(EXTERNAL_CHARGES externalCharge, List<ServiceTaxContainer> applicableServiceTaxes) {
		for (ServiceTaxContainer serviceTaxContainer : applicableServiceTaxes) {
			if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == externalCharge) {
				return serviceTaxContainer.getTaxRatio();
			}
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ExternalChgDTO getApplicableServiceTax(EXTERNAL_CHARGES taxChargeCode, Integer operationalMode,
			Collection<LCCClientExternalChgDTO> lccClientExternalChgDTOs, List<ServiceTaxContainer> applicableServiceTaxes)
			throws ModuleException {

		ExternalChgDTO externalChgDTO = null;

		if (applicableServiceTaxes != null && !applicableServiceTaxes.isEmpty()) {
			ServiceTaxContainer serviceTaxObj = getServiceTax(taxChargeCode, applicableServiceTaxes);

			if (serviceTaxObj != null && serviceTaxObj.isTaxApplicable()) {
				BigDecimal totalJnTax = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (EXTERNAL_CHARGES taxableService : serviceTaxObj.getTaxableExternalCharges()) {
					BigDecimal serviceCharge = getServiceCharge(taxableService, lccClientExternalChgDTOs);
					if (AccelAeroCalculator.isGreaterThan(serviceCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						BigDecimal serviceJNTax = AccelAeroCalculator.multiplyDefaultScale(serviceCharge,
								getServiceTaxRatio(taxChargeCode, applicableServiceTaxes));

						totalJnTax = AccelAeroCalculator.add(totalJnTax, serviceJNTax);
					}
				}

				if (AccelAeroCalculator.isGreaterThan(totalJnTax, AccelAeroCalculator.getDefaultBigDecimalZero())) {
					Collection colEXTERNAL_CHARGES = new ArrayList();
					colEXTERNAL_CHARGES.add(taxChargeCode);
					Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD()
							.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, operationalMode);
					externalChgDTO = extChgMap.get(taxChargeCode);

					if (externalChgDTO != null) {
						externalChgDTO.setTotalAmount(AccelAeroCalculator.parseBigDecimal(totalJnTax.doubleValue()));
						externalChgDTO.setAmount(externalChgDTO.getTotalAmount());
						((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(serviceTaxObj.getTaxApplyingFlightRefNumber());
					}
				}
			}
		}

		return externalChgDTO;
	}

	public int getTmpPayIdFromTmpPayMap(Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap) throws ModuleException {
		if (mapTempPayMap != null && mapTempPayMap.size() == 1) {
			return (Integer) BeanUtils.getFirstElement(mapTempPayMap.keySet());
		}

		throw new ModuleException("temporyPayment.multipleCCTmpPayNotSupported");
	}

	public String getPNRFromLccTmpPayMap(Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap) throws ModuleException {
		if (mapTempPayMap.size() == 1) {
			CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils
					.getFirstElement(mapTempPayMap.values());
			return lccClientCreditCardPaymentInfo.getGroupPNR();
		}

		throw new ModuleException("temporyPayment.multipleCCTmpPayNotSupported");
	}

	public static List<LCCClientExternalChgDTO> converToProxyExtCharges(Collection<ExternalChgDTO> extChgs) {
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
		if (extChgs != null) {
			for (ExternalChgDTO chg : extChgs) {
				extChgList.add(new LCCClientExternalChgDTO(chg));
			}
		}
		return extChgList;
	}

	public static CommonCreditCardPaymentInfo getCommonCreditCardPaymentInfo(Map<Integer, CommonCreditCardPaymentInfo> tempPayMap,
			boolean isPaymentSuccess) {
		CommonCreditCardPaymentInfo cardPaymentInfo = null;

		if (tempPayMap != null && !tempPayMap.isEmpty()) {
			Set<Integer> tmpKeySet = tempPayMap.keySet();

			for (Integer key : tmpKeySet) {
				if (key != null) {
					cardPaymentInfo = tempPayMap.get(key);
					cardPaymentInfo.setPaymentSuccess(isPaymentSuccess);
					if (cardPaymentInfo != null) {
						break;
					}
				}
			}
		}
		return cardPaymentInfo;
	}

	public Properties getIPGProperties(int paymentGatewayID, String baseCurrency) throws ModuleException {
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = validateAndPrepareIPGConfigurationParamsDTO(paymentGatewayID,
				baseCurrency);
		return ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
	}

	public String getIPGBehavior(int paymentGatewayID) throws ModuleException {
		IPGPaymentOptionDTO pgOptionsDto = getPaymentGatewayOptions(paymentGatewayID);
		if (pgOptionsDto == null) {
			// payment gateway is in-active/invalid
			log.error("invalid payment gateway selected");
			throw new ModuleException("payment.api.invalid.payment.gateway.selected");
		}

		Properties ipgProps = getIPGProperties(paymentGatewayID, pgOptionsDto.getBaseCurrency());
		if (ipgProps != null) {
			return ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);

		}

		throw new ModuleException("payment.api.missing.payment.gateway.configs");

	}

	public IPGPaymentOptionDTO getPaymentGatewayOptions(Integer ipgId) throws ModuleException {
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		ipgPaymentOptionDTO.setPaymentGateway(ipgId);
		ipgPaymentOptionDTO.setOfflinePayment(true);
		List<IPGPaymentOptionDTO> ipgPaymentGateways = paymentBrokerBD.getPaymentGateways(ipgPaymentOptionDTO);

		if (ipgPaymentGateways != null && !ipgPaymentGateways.isEmpty()) {
			return ipgPaymentGateways.get(0);

		}
		return null;
	}

	// duplicated ReservationUtil,CommonUtil,SortUtil - IBE
	protected Collection<FlightSegmentTO> sortFlightSegmentTO(List<FlightSegmentTO> availableFlightInfo) {
		Collections.sort(availableFlightInfo, new Comparator<FlightSegmentTO>() {
			public int compare(FlightSegmentTO obj1, FlightSegmentTO obj2) {
				return (obj1.getDepartureDateTime()).compareTo(obj2.getDepartureDateTime());
			}
		});
		return availableFlightInfo;
	}

	public Date getDepartureTimeZulu(List<FlightSegmentTO> flights) {
		Date depTimeZulu = null;

		if (flights != null && !flights.isEmpty()) {
			sortFlightSegmentTO(flights);
			FlightSegmentTO flightSegmentTO = flights.get(0);
			depTimeZulu = flightSegmentTO.getDepartureDateTimeZulu();
		}

		return depTimeZulu;
	}

	private int getTotalMinutes(String duration) {

		if (duration == null || duration.equals("") || duration.length() < 5) {
			return 0;
		}
		String[] arrDuration = duration.split(":");
		return (new Integer(arrDuration[0]).intValue() * 60) + new Integer(arrDuration[1]).intValue();
	}

	public boolean isOnHoldPaymentEnable(OnHold appOnHold, Date zuluDepDateTime) {

		boolean isOnHoldEnable = false;
		String strOnHoldHHMM = AppSysParamsUtil.getOnHoldBufferStartCutOver(appOnHold);

		long onholdStartCutOver = getTotalMinutes(strOnHoldHHMM) * 60 * 1000;

		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(CalendarUtil.getCurrentZuluDateTime());

		if (currentDate.getTimeInMillis() < (zuluDepDateTime.getTime() - onholdStartCutOver)) {
			isOnHoldEnable = true;
		}

		return isOnHoldEnable;

	}

	public String wrapWithDDHHMM(String hhmm) {
		StringBuilder wrapTextHM = new StringBuilder();
		if (hhmm != null && !hhmm.isEmpty()) {
			String[] textArray = hhmm.split(":");
			if (textArray.length == 3) {

				if (textArray[0] != null && !textArray[0].equals("0")) {
					wrapTextHM.append(textArray[0]);
					wrapTextHM.append(" Day(s) : ");
				}

				wrapTextHM.append(textArray[1]);
				wrapTextHM.append(" Hour(s) : ");

				wrapTextHM.append(textArray[2]);
				wrapTextHM.append(" Minute(s) ");

			}

			if (textArray.length == 2) {
				wrapTextHM.append(textArray[0]);
				wrapTextHM.append(" Hour(s) : ");

				wrapTextHM.append(textArray[1]);
				wrapTextHM.append(" Minute(s) ");
			}
		}

		return wrapTextHM.toString();
	}

	public String getOnHoldDisplayTime(OnHold appOnHold, Date pnrReleaseDate) {
		long displayTimelong = getTotalMinutes(AppSysParamsUtil.getOnHoldDisplayTime(appOnHold)) * 60 * 1000;
		Calendar currentDate = Calendar.getInstance();
		long onHoldDuration = 0;

		if (pnrReleaseDate != null) {
			onHoldDuration = pnrReleaseDate.getTime() - currentDate.getTimeInMillis();
		}

		if (!AppSysParamsUtil.isApplyRouteBookingClassLevelOnholdReleaseTime() && onHoldDuration > displayTimelong) {
			return AppSysParamsUtil.getOnHoldDisplayTime(appOnHold);
		} else {
			return getTimeDDHHMM(onHoldDuration);
		}
	}

	public String getTimeDDHHMM(long time) {
		String dddhhmm = "";

		long days = TimeUnit.MILLISECONDS.toDays(time);
		long hours = TimeUnit.MILLISECONDS.toHours(time - TimeUnit.DAYS.toMillis(days));
		long minutes = TimeUnit.MILLISECONDS.toMinutes(time - (TimeUnit.DAYS.toMillis(days) + TimeUnit.HOURS.toMillis(hours)));

		dddhhmm = days + ":" + hours + ":" + minutes;

		return dddhhmm;
	}

	public String timeDiffInStr(long diff) {

		StringBuffer str = new StringBuffer();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffDays > 0) {
			str.append(diffDays + " days, ");
		}
		if (diffHours > 0) {
			str.append(diffHours + " hours, ");
		}
		if (diffMinutes > 0) {
			str.append(diffMinutes + " minutes, ");
		}
		if (diffSeconds > 0) {
			str.append(diffSeconds + " seconds.");
		}
		return str.toString();
	}

	public int getSegmentCount(List<SessionFlightSegment> segment) {
		if (segment != null && !segment.isEmpty()) {
			return segment.size();
		}
		return 0;
	}

	public int getPayablePaxCount(TravellerQuantity travellerQuantity) {
		if (travellerQuantity != null) {
			return travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();
		}
		return 0;
	}

	public boolean isCabinClassAllowOnHold(FareSegChargeTO pricingInformation) {
		if (pricingInformation != null && pricingInformation.isAllowOnhold()) {
			return pricingInformation.isAllowOnhold();
		}
		return false;
	}

	public List<PassengerTypeQuantityTO> getPaxQtyList(TravellerQuantity travellerQuantity) {

		List<PassengerTypeQuantityTO> paxQtyList = new ArrayList<PassengerTypeQuantityTO>();

		PassengerTypeQuantityTO adultPassenger = new PassengerTypeQuantityTO();
		adultPassenger.setPassengerType(PaxTypeTO.ADULT);
		adultPassenger.setQuantity(travellerQuantity.getAdultCount());
		paxQtyList.add(adultPassenger);

		PassengerTypeQuantityTO childPassenger = new PassengerTypeQuantityTO();
		childPassenger.setPassengerType(PaxTypeTO.CHILD);
		childPassenger.setQuantity(travellerQuantity.getChildCount());
		paxQtyList.add(childPassenger);

		PassengerTypeQuantityTO infantPassenger = new PassengerTypeQuantityTO();
		infantPassenger.setPassengerType(PaxTypeTO.INFANT);
		infantPassenger.setQuantity(travellerQuantity.getInfantCount());
		paxQtyList.add(infantPassenger);

		return paxQtyList;
	}

	public OndBookingClassInfo getOndBookingClassInfo(FareSegChargeTO fareSegChargeTO) {
		Collection<OndFareSegChargeTO> ondFareSegChargeTOs = fareSegChargeTO.getOndFareSegChargeTOs();

		List<String> bookingClasses = new ArrayList<String>();
		List<String> cabinClasses = new ArrayList<String>();
		List<String> logicalCabinClasses = new ArrayList<String>();
		List<String> ondCodes = new ArrayList<String>();
		OndBookingClassInfo ondBookingClassInfo = new OndBookingClassInfo();

		if (fareSegChargeTO != null && ondFareSegChargeTOs != null && !ondFareSegChargeTOs.isEmpty()) {
			for (OndFareSegChargeTO ondFareSegChargeTO : ondFareSegChargeTOs) {
				ondCodes.add(ondFareSegChargeTO.getOndCode());
				LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc = ondFareSegChargeTO.getFsegBCAlloc();
				if (fsegBCAlloc != null && !fsegBCAlloc.isEmpty() && fsegBCAlloc.values() != null
						&& !fsegBCAlloc.values().isEmpty()) {
					for (LinkedList<BookingClassAlloc> bookingClassAllocList : fsegBCAlloc.values()) {
						for (BookingClassAlloc bookingClassAlloc : bookingClassAllocList) {
							bookingClasses.add(bookingClassAlloc.getBookingClassCode());
							cabinClasses.add(bookingClassAlloc.getCabinClassCode());
							logicalCabinClasses.add(bookingClassAlloc.getLogicaCabinClassCode());
						}
					}
				}

			}
		}
		ondBookingClassInfo.setBookingClasses(bookingClasses);
		ondBookingClassInfo.setCabinClasses(cabinClasses);
		ondBookingClassInfo.setLogicalCabinClasses(logicalCabinClasses);
		ondBookingClassInfo.setOndCodes(ondCodes);

		return ondBookingClassInfo;
	}

	public IPaxCountAssembler getIPaxCountAssembler(TravellerQuantity travellerQuantity) {
		IPaxCountAssembler paxAssm = new PaxCountAssembler(travellerQuantity.getAdultCount(), travellerQuantity.getChildCount(),
				travellerQuantity.getInfantCount());
		return paxAssm;
	}

	public List<FlightSegmentTO> adaptFlightSegment(List<SessionFlightSegment> segments) {
		FlightSegmentAdaptor flightSegmentAdaptor = new FlightSegmentAdaptor();
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		AdaptorUtils.adaptCollection(segments, flightSegmentTOs, flightSegmentAdaptor);
		return flightSegmentTOs;
	}

	public BaseAvailRQ adaptBaseAvailRQ(List<PassengerTypeQuantityTO> passengerTypeList, PreferenceInfo preferenceInfo,
			ApplicationEngine appEngine) {
		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		baseAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList().addAll(passengerTypeList);
		baseAvailRQ.getAvailPreferences().setSearchSystem(ProxyConstants.SYSTEM.getEnum(preferenceInfo.getSelectedSystem()));
		baseAvailRQ.getAvailPreferences().setAppIndicator(appEngine);
		baseAvailRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(false); // TODO need to get this from the
																					// session
		baseAvailRQ.getTravelPreferences().setBookingType(null);// TODO for IBE booking doesn't allow to select booking
																// type, if service-App is exposed to XBE need to store
																// this in the transaction

		return baseAvailRQ;
	}

	public ServiceTaxQuoteCriteriaForTktDTO adaptServiceTaxQuoteCriteriaDTO(BaseAvailRQ baseAvailRQ,
			FareSegChargeTO fareSegChargeTO, List<FlightSegmentTO> flightSegments, ServiceTaxRQ serviceTaxRQ,
			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges, Map<Integer, String> paxWisePaxTypes) {

		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
		serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
		serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(fareSegChargeTO);
		serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegments);
		serviceTaxQuoteCriteriaDTO.setPaxState(serviceTaxRQ.getStateCode());
		serviceTaxQuoteCriteriaDTO.setPaxCountryCode(serviceTaxRQ.getCountryCode());
		serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(
				(serviceTaxRQ.getTaxRegNo() != null && !"".equals(serviceTaxRQ.getTaxRegNo())) ? true : false);
		serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
		serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
		serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(serviceTaxRQ.getTransactionId());

		return serviceTaxQuoteCriteriaDTO;
	}

	public Map<Integer, List<LCCClientExternalChgDTO>> perPaxExternalCharges(
			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges, FinancialStore finacialStore,
			List<SessionFlightSegment> sessionFlightSegments, TravellerQuantity travellerQuantity,
			List<SessionFlexiDetail> sessionFlexiDetailList, List<ReservationPaxTO> passengers, int rateApplicableType)
			throws ModuleException {

		// Ancillary related charges
		Map<Integer, List<LCCClientExternalChgDTO>> ancillaryExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		if (finacialStore.getAncillaryExternalCharges() != null
				&& finacialStore.getAncillaryExternalCharges().getPassengers() != null
				&& !finacialStore.getAncillaryExternalCharges().getPassengers().isEmpty()) {
			for (PassengerChargeTo<LCCClientExternalChgDTO> anciPassenger : finacialStore.getAncillaryExternalCharges()
					.getPassengers()) {
				if (anciPassenger.getGetPassengerCharges() != null && !anciPassenger.getGetPassengerCharges().isEmpty()) {
					ancillaryExternalCharges.put(Integer.parseInt(anciPassenger.getPassengerRph()),
							anciPassenger.getGetPassengerCharges());
				}
			}
		}

		// payment related external charges
		Map<Integer, List<LCCClientExternalChgDTO>> paxWisePaymentCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		ReservationChargeTo<LCCClientExternalChgDTO> paymentExtCharges = finacialStore.getPaymentExternalCharges();
		if (paymentExtCharges != null) {
			if (paymentExtCharges.getReservationCharges() != null && !paymentExtCharges.getReservationCharges().isEmpty()) {
				// paxwisePaymentExternalCharges =
				// getPaxwisePaymentExternalCharges(paymentExtCharges.getReservationCharges(),
				// adultChildCount);
				for (LCCClientExternalChgDTO LCCClientExternalChgDTO : paymentExtCharges.getReservationCharges()) {
					if (LCCClientExternalChgDTO.getExternalCharges().equals(EXTERNAL_CHARGES.CREDIT_CARD)) {
						constructPaxWiseCCChargeMap(rateApplicableType, sessionFlightSegments, passengers, paxWisePaymentCharges,
								LCCClientExternalChgDTO.getAmount());
					}
				}
			}
			// need a better logic to calculate payment external charges
			// int count = 1;
			// Iterator<List<LCCClientExternalChgDTO>> paxwisePaymentExternalChargesItre =
			// paxwisePaymentExternalCharges.iterator();
			// while (paxwisePaymentExternalChargesItre.hasNext()) {
			// paxWisePaymentCharges.put(count, paxwisePaymentExternalChargesItre.next());
			// count++;
			// }
		}

		// flexi related external charges, basically availability external charges
		// TODO add flexi related logics to payment
		Map<Integer, List<LCCClientFlexiExternalChgDTO>> paxWiseFlexiExternalCharges = getPaxWiseSegmentWiseFlexiExternalCharges(
				sessionFlightSegments, travellerQuantity, sessionFlexiDetailList, passengers);

		// merge external charges
		Collections.sort(passengers);
		for (ReservationPaxTO resPassenger : passengers) {
			if (!paxWiseExternalCharges.containsKey(resPassenger.getSeqNumber())) {
				paxWiseExternalCharges.put(resPassenger.getSeqNumber(), new ArrayList<LCCClientExternalChgDTO>());
			}
			if (ancillaryExternalCharges.containsKey(resPassenger.getSeqNumber())) {
				paxWiseExternalCharges.get(resPassenger.getSeqNumber())
						.addAll(ancillaryExternalCharges.get(resPassenger.getSeqNumber()));
			}

			if (paxWisePaymentCharges.containsKey(resPassenger.getSeqNumber())) {
				paxWiseExternalCharges.get(resPassenger.getSeqNumber())
						.addAll(paxWisePaymentCharges.get(resPassenger.getSeqNumber()));
			}

			if (paxWiseFlexiExternalCharges.containsKey(resPassenger.getSeqNumber())) {
				paxWiseExternalCharges.get(resPassenger.getSeqNumber())
						.addAll(paxWiseFlexiExternalCharges.get(resPassenger.getSeqNumber()));
			}
		}

		return paxWiseExternalCharges;
	}

	// populate credit card details into paxWisePaymentCharges
	public void constructPaxWiseCCChargeMap(int rateApplicableType, List<SessionFlightSegment> sessionFlightSegments,
			List<ReservationPaxTO> passengers, Map<Integer, List<LCCClientExternalChgDTO>> paxWisePaymentCharges,
			BigDecimal calculatedAmount) throws ModuleException {
		ExternalChgDTO externalChgDTO = null;
		Collection<EXTERNAL_CHARGES> externalChargesCollection = new ArrayList<>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>(); // only used to pass into util method
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();

		externalChargesCollection.add(EXTERNAL_CHARGES.CREDIT_CARD);
		try {
			extChgMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(externalChargesCollection, null,
					rateApplicableType);
		} catch (ModuleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);

		ServiceTaxCalculatorUtil.populatePaxWiseCreditCardCharge(passengers, externalChgDTO, calculatedAmount,
				paxWisePaymentCharges, paxWisePaxTypes, adaptFlightSegment(sessionFlightSegments), null, null);
	}

	public static Map<Integer, List<LCCClientFlexiExternalChgDTO>> getPaxWiseSegmentWiseFlexiExternalCharges(
			List<SessionFlightSegment> sessionFlightSegments, TravellerQuantity travellerQuantity,
			List<SessionFlexiDetail> sessionFlexiDetailList, List<ReservationPaxTO> passengers) {
		Map<Integer, List<LCCClientFlexiExternalChgDTO>> flexiExtChgMap = new HashMap<>();

		int nonInfants = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();

		for (ReservationPaxTO pax : passengers) {
			if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				List<LCCClientFlexiExternalChgDTO> externalChgDTOList = new ArrayList<>();
				if (sessionFlexiDetailList != null && !sessionFlexiDetailList.isEmpty()) {
					for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiDetailList) {
						externalChgDTOList
								.addAll(getPaxFlexiExternalCharges(sessionFlightSegments, nonInfants, pax, sessionFlexiDetail));
					}
				}
				flexiExtChgMap.put(pax.getSeqNumber(), externalChgDTOList);
			}
		}

		return flexiExtChgMap;

	}

	private static List<LCCClientFlexiExternalChgDTO> getPaxFlexiExternalCharges(List<SessionFlightSegment> sessionFlightSegments,
			int nonInfants, ReservationPaxTO pax, SessionFlexiDetail sessionFlexiDetail) {
		List<LCCClientFlexiExternalChgDTO> externalChgDTOList = new ArrayList<>();
		if (sessionFlexiDetail.isFlexiAvailable() && sessionFlexiDetail.isFlexiSelected()) {
			for (SessionExternalChargeSummary sessionExternalChargeSummary : sessionFlexiDetail.getExternalChargeSummary()) {
				List<String> fltSegRefList = getFlightSegmentRPHList(sessionExternalChargeSummary.getSegmentCode(),
						sessionFlightSegments);
				int segmentCount = fltSegRefList.size();
				BigDecimal[] flexiPassengerQuota = AccelAeroCalculator
						.roundAndSplit(sessionExternalChargeSummary.getFlexiCharge(), nonInfants);
				BigDecimal[] flexiSegmentQuota = AccelAeroCalculator.roundAndSplit(flexiPassengerQuota[pax.getSeqNumber() - 1],
						segmentCount);

				populateSegmentWiseFlexiExtCharges(pax, externalChgDTOList, sessionExternalChargeSummary, fltSegRefList,
						flexiSegmentQuota);
			}
		}
		return externalChgDTOList;
	}

	private static void populateSegmentWiseFlexiExtCharges(ReservationPaxTO pax,
			List<LCCClientFlexiExternalChgDTO> externalChgDTOList, SessionExternalChargeSummary sessionExternalChargeSummary,
			List<String> fltSegRefList, BigDecimal[] flexiSegmentQuota) {
		int segmentIndex = 0;
		for (String flightRefNumber : fltSegRefList) {
			if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				LCCClientFlexiExternalChgDTO tmpChgDTO = new LCCClientFlexiExternalChgDTO();
				tmpChgDTO.setExternalCharges(AnciCommon.getExternalCharge(AncillariesConstants.AncillaryType.FLEXIBILITY));
				tmpChgDTO.setCarrierCode(sessionExternalChargeSummary.getCarrierCode());
				tmpChgDTO.setCode(sessionExternalChargeSummary.getSurchargeCode());
				tmpChgDTO.setFlightRefNumber(flightRefNumber);
				tmpChgDTO.setSegmentCode(sessionExternalChargeSummary.getSegmentCode());
				tmpChgDTO.setAmount(flexiSegmentQuota[segmentIndex]);
				tmpChgDTO.setFlexiBilities(sessionExternalChargeSummary.getAdditionalDetails());
				externalChgDTOList.add(tmpChgDTO);
			}
			segmentIndex++;
		}
	}

	private static List<String> getFlightSegmentRPHList(String segmentCode, List<SessionFlightSegment> sessionFlightSegments) {
		List<String> fltSegRefList = new ArrayList<>();
		for (SessionFlightSegment flightSegment : sessionFlightSegments) {
			if (segmentCode.contains(flightSegment.getSegmentCode())) {
				fltSegRefList.add(flightSegment.getFlightRefNumber());
			}
		}
		return fltSegRefList;
	}

	public ReservationPaxTO findParent(ReservationPaxTO infant, List<ReservationPaxTO> paxList) {
		ReservationPaxTO parent = null;
		for (ReservationPaxTO pax : paxList) {
			if (pax.getPaxType().equals(PaxTypeTO.ADULT) && infant.getInfantWith() != null
					&& infant.getInfantWith().equals(pax.getSeqNumber())) {
				parent = pax;
				break;
			}
		}
		return parent;
	}

	public Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> populatePaxExternalCharges(
			List<PassengerChargeTo<LCCClientExternalChgDTO>> paxAncillaryExternalCharges, List<Passenger> paxInfo,
			BigDecimal removedCharges) {

		Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges = null;

		if (paxAncillaryExternalCharges != null && !paxAncillaryExternalCharges.isEmpty()) {
			paxCarrierExternalCharges = new HashMap<String, TreeMap<String, List<LCCClientExternalChgDTO>>>();

			if (paxInfo != null && !paxInfo.isEmpty()) {
				for (Passenger passenger : paxInfo) {

					String paxKey = null;
					if (BookingUtil.isParent(paxInfo, passenger.getPaxSequence())) {
						paxKey = passenger.getPaxSequence() + "_" + PaxTypeTO.PARENT + "_"
								+ ((BookingUtil.isParent(paxInfo, passenger.getPaxSequence())) ? "Y" : "N");

					} else {
						paxKey = passenger.getPaxSequence() + "_" + passenger.getPaxType() + "_"
								+ ((BookingUtil.isParent(paxInfo, passenger.getPaxSequence())) ? "Y" : "N");
					}

					List<LCCClientExternalChgDTO> externalChgDTOs = new ArrayList<LCCClientExternalChgDTO>();

					if (paxAncillaryExternalCharges != null && !paxAncillaryExternalCharges.isEmpty()) {
						for (PassengerChargeTo<LCCClientExternalChgDTO> paxAnciCharges : paxAncillaryExternalCharges) {
							if (passenger.getPaxSequence() == Integer.parseInt(paxAnciCharges.getPassengerRph())) {
								externalChgDTOs.addAll(paxAnciCharges.getGetPassengerCharges());
								break;
							}
						}
					}
					Map<EXTERNAL_CHARGES, LCCClientExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, LCCClientExternalChgDTO>();
					Map<String, Map<EXTERNAL_CHARGES, LCCClientExternalChgDTO>> carrierExtChgMap = new HashMap<String, Map<EXTERNAL_CHARGES, LCCClientExternalChgDTO>>();
					TreeMap<String, List<LCCClientExternalChgDTO>> carrierExtCharges = new TreeMap<String, List<LCCClientExternalChgDTO>>();

					if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {

						Comparator<LCCClientExternalChgDTO> exChargeComparator = new Comparator<LCCClientExternalChgDTO>() {
							public int compare(LCCClientExternalChgDTO c1, LCCClientExternalChgDTO c2) {
								return c1.getExternalCharges().toString().compareTo(c2.getExternalCharges().toString());
							}
						};

						Collections.sort(externalChgDTOs, exChargeComparator);

						for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalChgDTOs) {
							String chargeCarrierCode = lccClientExternalChgDTO.getCarrierCode();
							if (!carrierExtChgMap.containsKey(chargeCarrierCode)) {
								extChgMap = new HashMap<EXTERNAL_CHARGES, LCCClientExternalChgDTO>();
							} else {
								extChgMap = carrierExtChgMap.get(chargeCarrierCode);
							}

							if (!extChgMap.containsKey(lccClientExternalChgDTO.getExternalCharges())) {
								LCCClientExternalChgDTO extChg = null;
								if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
									extChg = (LCCClientFlexiExternalChgDTO) lccClientExternalChgDTO;
								} else {
									extChg = lccClientExternalChgDTO.clone();
								}

								extChg.setAmount(getCarrierAncillaryCharge(externalChgDTOs,
										lccClientExternalChgDTO.getExternalCharges(), chargeCarrierCode));

								if (removedCharges != null && removedCharges.doubleValue() > 0) {

									if (AccelAeroCalculator.isGreaterThan(removedCharges,
											AccelAeroCalculator.getDefaultBigDecimalZero())) {
										BigDecimal addedCharges = extChg.getAmount();
										if (removedCharges.compareTo(addedCharges) == 0) {
											removedCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
											extChg.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
										} else if (removedCharges.compareTo(addedCharges) > 0) {
											removedCharges = AccelAeroCalculator.subtract(removedCharges, addedCharges);
											extChg.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
										} else if (removedCharges.compareTo(addedCharges) < 0) {
											BigDecimal remainingExgCharge = AccelAeroCalculator.subtract(addedCharges,
													removedCharges);
											extChg.setAmount(remainingExgCharge);
											removedCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
										}
									}
								}

								extChgMap.put(extChg.getExternalCharges(), extChg);
							}
							carrierExtChgMap.put(chargeCarrierCode, extChgMap);
						}
					}

					for (String carrierCode : carrierExtChgMap.keySet()) {
						carrierExtCharges.put(carrierCode,
								new ArrayList<LCCClientExternalChgDTO>(carrierExtChgMap.get(carrierCode).values()));
					}

					paxCarrierExternalCharges.put(paxKey, carrierExtCharges);

				}
			}
		}
		return paxCarrierExternalCharges;
	}

	private BigDecimal getCarrierAncillaryCharge(List<LCCClientExternalChgDTO> externalChgDTOs, EXTERNAL_CHARGES externalCharge,
			String carrierCode) {
		BigDecimal total = BigDecimal.ZERO;
		if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
			for (LCCClientExternalChgDTO charge : externalChgDTOs) {
				if (charge.getCarrierCode().equals(carrierCode) && charge.getExternalCharges() == externalCharge) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		return total;
	}

	public LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, boolean recordAudit, String airlineCode,
			String marketingAirlineCode, boolean skipPromoAdjustment, ApplicationEngine applicationEngine) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		boolean loadFares = true; // Set True
		boolean loadLastUserNote = false;
		boolean loadOndChargesView = true;
		boolean loadPaxAvaBalance = true;
		boolean loadSegView = true;

		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegView(loadSegView);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(applicationEngine);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setSkipPromoAdjustment(skipPromoAdjustment);
		pnrModesDTO.setLoadAirportTransferInfo(true);

		if (StringUtils.isNotEmpty(marketingAirlineCode)) {
			if (marketingAirlineCode.trim().length() == 2) {
				pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
			}
		}

		if (StringUtils.isNotEmpty(airlineCode)) {
			if (airlineCode.trim().length() == 2) {
				pnrModesDTO.setAirlineCode(airlineCode);
			}
		}

		return pnrModesDTO;
	}

	public static Merchant getMerchant(String merchantID) {
		Merchant merchant = null;
		try {
			merchant = CommonsServices.getGlobalConfig().getMerchantByID(merchantID);
		} catch (Exception e) {
			log.error("Errorwhile retrieveing merchant, merchantID: " + merchantID, e);
		}
		return merchant;
	}

	public static String buildHashString(List<String> hashStringParamsInOrder, String secureHashKey) {
		String hashString = "";

		try {

			StringBuilder messageBuilder = new StringBuilder();

			for (String param : hashStringParamsInOrder) {
				messageBuilder.append(param);
			}

			Mac sha256HMAC = Mac.getInstance(PaymentConsts.HMAC_ALGO);
			SecretKeySpec secretKey = new SecretKeySpec(secureHashKey.getBytes(), PaymentConsts.HMAC_ALGO);
			sha256HMAC.init(secretKey);

			hashString = Base64.getEncoder().encodeToString(sha256HMAC.doFinal(messageBuilder.toString().getBytes()));

		} catch (Exception e) {
			log.error("Error while creating hashString", e);
		}

		return hashString;
	}

	@SuppressWarnings("unchecked")
	public static List<IPGPaymentOptionDTO> getPaymentGatewayList(String moduleCode) throws ModuleException {
		List<IPGPaymentOptionDTO> iPGPaymentOptionDTO = new ArrayList<>();
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		iPGPaymentOptionDTO = (List<IPGPaymentOptionDTO>) paymentBrokerBD.getPaymentGateways(moduleCode);
		return iPGPaymentOptionDTO;
	}
}
