package com.isa.aeromart.services.endpoint.utils.externalagent;

import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.aeromart.services.endpoint.controller.common.ExternalAgentConstants;
import com.isa.aeromart.services.endpoint.dto.externalagent.HashGenRQ;
import com.isa.aeromart.services.endpoint.utils.payment.PaymentUtils;
import com.isa.thinair.airtravelagents.api.model.AgentCreditBlock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class ExternalAgentUtil {

	public static String createHashvalue(HashGenRQ paymentInfo) {
		Merchant merchant = PaymentUtils.getMerchant(paymentInfo.getMerchantId());
		if (merchant != null) {
			List<String> responseParams = new ArrayList<String>();
			responseParams.add(paymentInfo.getToken());
			responseParams.add(paymentInfo.getAmount());
			responseParams.add(paymentInfo.getCurrencyCode());
			if (paymentInfo.getOrderId() != null && !"".equals(paymentInfo.getOrderId())) {
				responseParams.add(paymentInfo.getOrderId());
			}
			return PaymentUtils.buildHashString(responseParams, merchant.getSecureHashKey());
		} else {
			return "";
		}
	}

	public static String createHashvalueForTimestamp(HashGenRQ paymentInfo) {
		Merchant merchant = PaymentUtils.getMerchant(ExternalAgentConstants.MERCHANT_ID);
		if (merchant != null) {
			List<String> responseParams = new ArrayList<String>();
			responseParams.add(paymentInfo.getToken());

			return PaymentUtils.buildHashString(responseParams, merchant.getSecureHashKey());
		} else {
			return "";
		}
	}

	public static boolean validateTimeStamp(Date timeStamp) {
		boolean valid = false;

		Date currentTimeStamp = CalendarUtil.getCurrentZuluDateTime();
		Long timeGapInMillis = ExternalAgentConstants.TIME_GAP;

		if ((currentTimeStamp.getTime() - timeGapInMillis < timeStamp.getTime())
				&& (currentTimeStamp.getTime() > timeStamp.getTime())) {
			valid = true;
		}

		return valid;
	}

	public static boolean validateHash(Date timeStamp, String secureHash, String merchantId) {
		boolean valid = false;

		Merchant merchant = PaymentUtils.getMerchant(merchantId);
		if (merchant != null) {
			List<String> responseParams = new ArrayList<String>();
			if (timeStamp != null) {
				Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				String timeStampStr = formatter.format(timeStamp);
				responseParams.add(timeStampStr);
			}

			if (secureHash.equals(PaymentUtils.buildHashString(responseParams, merchant.getSecureHashKey()))) {
				valid = true;
			}
		}

		return valid;
	}

	public static AgentCreditBlock getAgentCreditBlock(AgentToken agentToken, String status, BigDecimal amount) {
		AgentCreditBlock agentCreditBlock = new AgentCreditBlock();
		agentCreditBlock.setAgentCode(agentToken.getAgentCode());
		agentCreditBlock.setCreditBlockedTimeStamp(new Date());
		agentCreditBlock.setStatus(status);
		agentCreditBlock.setToken(agentToken.getToken());
		agentCreditBlock.setLocalAmount(amount);
		agentCreditBlock.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		agentCreditBlock.setBaseAmount(amount);// ASSUMING THIS SUPPORTS ONLY FOR BASE CURRENCY
		agentCreditBlock.setBlockedCreditReleaseTimeStamp(CalendarUtil.addMilliSeconds(
				agentCreditBlock.getCreditBlockedTimeStamp(), AppSysParamsUtil.getExternalAgentCreditBlockDuration()));
		agentCreditBlock.setCreditBlockedTimeStamp(new Date());

		return agentCreditBlock;
	}

}
