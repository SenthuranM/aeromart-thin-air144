package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillarySummary;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillarySummaryRS;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

public class AncillarySummaryResponseAdaptor extends
		TransactionAwareAdaptor<AncillarySummaryResponseAdaptor.AncillarySummaryResponseAdaptorWrapper, AncillarySummaryRS> {

	private Map<String, AncillarySummary> anciTypeWiseSummary;
	
	protected static final String CONFIRM = "CNF";
	
	protected static final String CANCLE = "CNX";
	
	protected static final String ON_HOLD = "OHD";

	@Override
	public AncillarySummaryRS adapt(AncillarySummaryResponseAdaptorWrapper wrapper) {

		LCCClientReservation lccClientReservation = wrapper.getLccClientReservation();
		AncillaryAvailabilityRS request = wrapper.getRequest();

		AncillarySummaryRS ancillarySummaryRS = new AncillarySummaryRS();
		ancillarySummaryRS.setTransactionId(getTransactionId());

		List<AncillarySummary> ancillarySummaryList = new ArrayList<AncillarySummary>();
		AncillarySummary ancillarySummary;
		BigDecimal totalAmount;
		String anciType;

		anciTypeWiseSummary = new HashMap<String, AncillarySummary>();

		// set anci summary amount and selection

		// set insurance anci data
		if (lccClientReservation.getReservationInsurances() != null && lccClientReservation.getReservationInsurances().size() > 0) {
			totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			anciType = AncillariesConstants.AncillaryType.INSURANCE.getCode();
			ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
			ancillarySummary.setSelected(true);
			ancillarySummary.setScopeType(resolveAnciScope(anciType));
			if (ancillarySummary.getTotalAmount() == null) {
				ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}

			for (LCCClientReservationInsurance lccClientReservationInsurance : lccClientReservation.getReservationInsurances()) {
				totalAmount = AccelAeroCalculator.add(totalAmount, lccClientReservationInsurance.getQuotedTotalPremium());
			}
			ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(), totalAmount));
			anciTypeWiseSummary.put(anciType, ancillarySummary);
		}

		for (LCCClientReservationPax pax : lccClientReservation.getPassengers()) {
			for (LCCSelectedSegmentAncillaryDTO selectedSegmentAncillaryDTO : pax.getSelectedAncillaries()) {
				if (selectedSegmentAncillaryDTO.getAirportServiceDTOs() != null
						&& !selectedSegmentAncillaryDTO.getAirportServiceDTOs().isEmpty()) {
					totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					anciType = AncillariesConstants.AncillaryType.SSR_AIRPORT.getCode();
					ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
					ancillarySummary.setSelected(true);
					ancillarySummary.setScopeType(resolveAnciScope(anciType));
					if (ancillarySummary.getTotalAmount() == null) {
						ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}

					for (LCCAirportServiceDTO lccAirportServiceDTO : selectedSegmentAncillaryDTO.getAirportServiceDTOs()) {
						totalAmount = AccelAeroCalculator.add(totalAmount, lccAirportServiceDTO.getServiceCharge());
					}
					ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(), totalAmount));
					anciTypeWiseSummary.put(anciType, ancillarySummary);
				}

				if (selectedSegmentAncillaryDTO.getAirportTransferDTOs() != null
						&& !selectedSegmentAncillaryDTO.getAirportTransferDTOs().isEmpty()) {
					totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					anciType = AncillariesConstants.AncillaryType.AIRPORT_TRANSFER.getCode();
					ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
					ancillarySummary.setSelected(true);
					ancillarySummary.setScopeType(resolveAnciScope(anciType));
					if (ancillarySummary.getTotalAmount() == null) {
						ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}

					for (LCCAirportServiceDTO lccAirportServiceDTO : selectedSegmentAncillaryDTO.getAirportTransferDTOs()) {
						totalAmount = AccelAeroCalculator.add(totalAmount, lccAirportServiceDTO.getServiceCharge());
					}
					ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(), totalAmount));
					anciTypeWiseSummary.put(anciType, ancillarySummary);
				}

				if (selectedSegmentAncillaryDTO.getAutomaticCheckinDTOs() != null
						&& !selectedSegmentAncillaryDTO.getAutomaticCheckinDTOs().isEmpty()) {
					totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					anciType = AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN.getCode();
					ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
					ancillarySummary.setSelected(true);
					ancillarySummary.setScopeType(resolveAnciScope(anciType));
					if (ancillarySummary.getTotalAmount() == null) {
						ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}

					for (LCCAutomaticCheckinDTO lccAutomaticCheckinDTO : selectedSegmentAncillaryDTO.getAutomaticCheckinDTOs()) {
						totalAmount = AccelAeroCalculator.add(totalAmount, lccAutomaticCheckinDTO.getAutomaticCheckinCharge());
					}
					ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(), totalAmount));
					anciTypeWiseSummary.put(anciType, ancillarySummary);
				}

				if (selectedSegmentAncillaryDTO.getAirSeatDTO() != null
						&& selectedSegmentAncillaryDTO.getAirSeatDTO().getSeatNumber() != null) {
					totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					anciType = AncillariesConstants.AncillaryType.SEAT.getCode();
					ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
					ancillarySummary.setSelected(true);
					ancillarySummary.setScopeType(resolveAnciScope(anciType));
					if (ancillarySummary.getTotalAmount() == null) {
						ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}

					ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(),
							selectedSegmentAncillaryDTO.getAirSeatDTO().getSeatCharge()));
					anciTypeWiseSummary.put(anciType, ancillarySummary);
				}

				if (selectedSegmentAncillaryDTO.getBaggageDTOs() != null
						&& !selectedSegmentAncillaryDTO.getBaggageDTOs().isEmpty()) {
					totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					anciType = AncillariesConstants.AncillaryType.BAGGAGE.getCode();
					ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
					ancillarySummary.setSelected(true);
					ancillarySummary.setScopeType(resolveAnciScope(anciType));
					if (ancillarySummary.getTotalAmount() == null) {
						ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}

					for (LCCBaggageDTO lccBaggageDTO : selectedSegmentAncillaryDTO.getBaggageDTOs()) {
						totalAmount = AccelAeroCalculator.add(totalAmount, lccBaggageDTO.getBaggageCharge());
					}
					ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(), totalAmount));
					anciTypeWiseSummary.put(anciType, ancillarySummary);
				}

				if (selectedSegmentAncillaryDTO.getMealDTOs() != null && !selectedSegmentAncillaryDTO.getMealDTOs().isEmpty()) {
					totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					anciType = AncillariesConstants.AncillaryType.MEAL.getCode();
					ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
					ancillarySummary.setSelected(true);
					ancillarySummary.setScopeType(resolveAnciScope(anciType));
					if (ancillarySummary.getTotalAmount() == null) {
						ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}

					for (LCCMealDTO lccMealDTO : selectedSegmentAncillaryDTO.getMealDTOs()) {
						totalAmount = AccelAeroCalculator.add(totalAmount, new BigDecimal(lccMealDTO.getTotalPrice()));
					}
					ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(), totalAmount));
					anciTypeWiseSummary.put(anciType, ancillarySummary);
				}

				if (selectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs() != null
						&& !selectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs().isEmpty()) {
					totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					anciType = AncillariesConstants.AncillaryType.SSR_IN_FLIGHT.getCode();
					ancillarySummary = resolveAnciTypeAncillarySummary(anciType);
					ancillarySummary.setSelected(true);
					ancillarySummary.setScopeType(resolveAnciScope(anciType));
					if (ancillarySummary.getTotalAmount() == null) {
						ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
					}

					for (LCCSpecialServiceRequestDTO lccSpecialServiceRequestDTO : selectedSegmentAncillaryDTO
							.getSpecialServiceRequestDTOs()) {
						totalAmount = AccelAeroCalculator.add(totalAmount, lccSpecialServiceRequestDTO.getCharge());
					}
					ancillarySummary.setTotalAmount(AccelAeroCalculator.add(ancillarySummary.getTotalAmount(), totalAmount));
					anciTypeWiseSummary.put(anciType, ancillarySummary);
				}

			}
		}

		// set anci summary availability
		for (AncillaryAvailability ancillaryAvailability : request.getAvailability()) {
			ancillarySummary = resolveAnciTypeAncillarySummary(ancillaryAvailability.getType());
			ancillarySummary.setAvailable(ancillaryAvailability.isAvailable());
		}

		for (AncillarySummary ancillarySummaryRef : anciTypeWiseSummary.values()) {

			if (SalesChannelsUtil.isAppIndicatorWebOrMobile(getTrackInfoDTO().getAppIndicator())) {
				ancillarySummaryRef.setEditable(AnciCommon.isAncillaryEditableForModificationIBE(lccClientReservation,
						ancillarySummaryRef.getAnciType(), ancillarySummaryRef.isAvailable(), ancillarySummaryRef.isSelected(),
						wrapper.isRegUser()));
			}
			ancillarySummaryList.add(ancillarySummaryRef);
		}

		ancillarySummaryRS.setAncillarySummeryList(ancillarySummaryList);

		return updateAnciSummaryAccordingToReservationStatus(ancillarySummaryRS, lccClientReservation.getStatus(),
				lccClientReservation.getTotalAvailableBalance() != null ? lccClientReservation.getTotalAvailableBalance()
						.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0 : false);
	}

	private AncillarySummaryRS updateAnciSummaryAccordingToReservationStatus(AncillarySummaryRS ancillarySummaryRS,
			String reservationStatus, boolean forceConfirm) {
		if (reservationStatus.equals(CANCLE)) {
			for (AncillarySummary ancillarySummary : ancillarySummaryRS.getAncillarySummeryList()) {
				ancillarySummary.setEditable(false);
				ancillarySummary.setSelected(false);
				ancillarySummary.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}
		} else if (reservationStatus.equals(ON_HOLD)) {
			for (AncillarySummary ancillarySummary : ancillarySummaryRS.getAncillarySummeryList()) {
				ancillarySummary.setEditable(false);
			}
		} else if (reservationStatus.equals(CONFIRM) && forceConfirm) {
			for (AncillarySummary ancillarySummary : ancillarySummaryRS.getAncillarySummeryList()) {
				ancillarySummary.setEditable(false);
			}
		}
		ancillarySummaryRS.setSuccess(true);
		return ancillarySummaryRS;
	}

	private AncillarySummary resolveAnciTypeAncillarySummary(String anciType) {
		AncillarySummary ancillarySummary;
		if (!anciTypeWiseSummary.containsKey(anciType)) {
			ancillarySummary = new AncillarySummary();
			ancillarySummary.setAnciType(anciType);
			ancillarySummary.setScopeType(resolveAnciScope(anciType));
			anciTypeWiseSummary.put(anciType, ancillarySummary);
		}

		return anciTypeWiseSummary.get(anciType);
	}

	private String resolveAnciScope(String anciType) {
		String ScopeType = null;
		switch (anciType) {
		case AncillariesConstants.Type.AIRPORT_TRANSFER:
			ScopeType = Scope.ScopeType.AIRPORT;
			break;
		case AncillariesConstants.Type.BAGGAGE:
			ScopeType = Scope.ScopeType.OND;
			break;
		case AncillariesConstants.Type.FLEXIBILITY:
			ScopeType = Scope.ScopeType.OND;
			break;
		case AncillariesConstants.Type.INSURANCE:
			ScopeType = Scope.ScopeType.ALL_SEGMENTS;
			break;
		case AncillariesConstants.Type.MEAL:
			ScopeType = Scope.ScopeType.SEGMENT;
			break;
		case AncillariesConstants.Type.SEAT:
			ScopeType = Scope.ScopeType.SEGMENT;
			break;
		case AncillariesConstants.Type.SSR_AIRPORT:
			ScopeType = Scope.ScopeType.AIRPORT;
			break;
		case AncillariesConstants.Type.SSR_IN_FLIGHT:
			ScopeType = Scope.ScopeType.SEGMENT;
			break;
		case AncillariesConstants.Type.AUTOMATIC_CHECKIN:
			ScopeType = Scope.ScopeType.SEGMENT;
			break;
		}

		return ScopeType;
	}

	public static class AncillarySummaryResponseAdaptorWrapper {

		private LCCClientReservation lccClientReservation;

		private AncillaryAvailabilityRS request;
		
		private boolean regUser;

		public LCCClientReservation getLccClientReservation() {
			return lccClientReservation;
		}

		public void setLccClientReservation(LCCClientReservation lccClientReservation) {
			this.lccClientReservation = lccClientReservation;
		}

		public AncillaryAvailabilityRS getRequest() {
			return request;
		}

		public void setRequest(AncillaryAvailabilityRS request) {
			this.request = request;
		}

		public boolean isRegUser() {
			return regUser;
		}

		public void setRegUser(boolean regUser) {
			this.regUser = regUser;
		}

	}

}
