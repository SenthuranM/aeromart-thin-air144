package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.session.store.PaymentRequoteSessionStore;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;

import java.util.List;

/**
 * Created by degorov on 6/20/17.
 */
public class ComposeRequotePaymentDTO {
    private String pnr;
    private LmsCredits lmsCredits;
    private PaymentRequoteSessionStore paymentRequoteSessionStore;
    private boolean isLmsBlocking;
    private List<Passenger> paxInfo;
    private TrackInfoDTO trackInfoDTO;
    private String transactionId;

    public ComposeRequotePaymentDTO(){}

    public String getPnr() {
        return pnr;
    }

    public LmsCredits getLmsCredits() {
        return lmsCredits;
    }

    public PaymentRequoteSessionStore getPaymentRequoteSessionStore() {
        return paymentRequoteSessionStore;
    }

    public boolean isLmsBlocking() {
        return isLmsBlocking;
    }

    public List<Passenger> getPaxInfo() {
        return paxInfo;
    }

    public TrackInfoDTO getTrackInfoDTO() {
        return trackInfoDTO;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public ComposeRequotePaymentDTO setPnr(String pnr) {
        this.pnr = pnr;
        return this;
    }

    public ComposeRequotePaymentDTO setLmsCredits(LmsCredits lmsCredits) {
        this.lmsCredits = lmsCredits;
        return this;
    }

    public ComposeRequotePaymentDTO setPaymentRequoteSessionStore(PaymentRequoteSessionStore paymentRequoteSessionStore) {
        this.paymentRequoteSessionStore = paymentRequoteSessionStore;
        return this;
    }

    public ComposeRequotePaymentDTO setLmsBlocking(boolean lmsBlocking) {
        isLmsBlocking = lmsBlocking;
        return this;
    }

    public ComposeRequotePaymentDTO setPaxInfo(List<Passenger> paxInfo) {
        this.paxInfo = paxInfo;
        return this;
    }

    public ComposeRequotePaymentDTO setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
        this.trackInfoDTO = trackInfoDTO;
        return this;
    }

    public ComposeRequotePaymentDTO setTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }
}
