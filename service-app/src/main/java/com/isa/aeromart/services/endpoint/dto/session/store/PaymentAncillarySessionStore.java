package com.isa.aeromart.services.endpoint.dto.session.store;

import com.isa.aeromart.services.endpoint.dto.session.AncillaryTransaction;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;

public interface PaymentAncillarySessionStore extends PaymentRequoteSessionStore {
	public static String SESSION_KEY = AncillaryTransaction.SESSION_KEY;
	public void removePaymentExternalChargesOnly(EXTERNAL_CHARGES extChgEnum);
}
