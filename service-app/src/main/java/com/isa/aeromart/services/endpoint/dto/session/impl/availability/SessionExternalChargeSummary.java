package com.isa.aeromart.services.endpoint.dto.session.impl.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class SessionExternalChargeSummary {

	private Collection<FlexiInfoTO> additionalDetails = new ArrayList<FlexiInfoTO>();
	private BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String chargeRateId;
	private String surchargeCode;
	private String carrierCode;
	private String segmentCode;

	public BigDecimal getFlexiCharge() {
		return flexiCharge;
	}

	public void setFlexiCharge(BigDecimal flexiCharge) {
		this.flexiCharge = flexiCharge;
	}

	public String getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(String chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	public String getSurchargeCode() {
		return surchargeCode;
	}

	public void setSurchargeCode(String surchargeCode) {
		this.surchargeCode = surchargeCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Collection<FlexiInfoTO> getAdditionalDetails() {
		return additionalDetails;
	}

	public void setAdditionalDetails(Collection<FlexiInfoTO> additionalDetails) {
		this.additionalDetails = additionalDetails;
	}
	
}
