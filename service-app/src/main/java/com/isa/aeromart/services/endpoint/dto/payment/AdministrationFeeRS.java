package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class AdministrationFeeRS extends TransactionalBaseRS {
	private boolean displayAdministrationFee;
	private double administrationFee;
	private double totalAllInclusive;
	private String currency;
	private double totalServiceTaxes;
	private double serviceTaxForAdminFee;

	public double getTotalAllInclusive() {
		return totalAllInclusive;
	}

	public void setTotalAllInclusive(double totalAllInclusive) {
		this.totalAllInclusive = totalAllInclusive;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public boolean isDisplayAdministrationFee() {
		return displayAdministrationFee;
	}

	public void setDisplayAdministrationFee(boolean displayAdministrationFee) {
		this.displayAdministrationFee = displayAdministrationFee;
	}

	public double getAdministrationFee() {
		return administrationFee;
	}

	public void setAdministrationFee(double administrationFee) {
		this.administrationFee = administrationFee;
	}

	public double getTotalServiceTaxes() {
		return totalServiceTaxes;
	}

	public void setTotalServiceTaxes(double totalServiceTaxes) {
		this.totalServiceTaxes = totalServiceTaxes;
	}

	public double getServiceTaxForAdminFee() {
		return serviceTaxForAdminFee;
	}

	public void setServiceTaxForAdminFee(double serviceTaxForAdminFee) {
		this.serviceTaxForAdminFee = serviceTaxForAdminFee;
	}

}
