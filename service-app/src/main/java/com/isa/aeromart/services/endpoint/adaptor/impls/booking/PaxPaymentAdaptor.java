package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.PaxPayments;
import com.isa.aeromart.services.endpoint.dto.booking.PaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class PaxPaymentAdaptor implements Adaptor<LCCClientReservationPax, PaxPayments> {

	public PaxPayments adapt(LCCClientReservationPax pax) {

		PaxPayments paxPayments = new PaxPayments();

		PaymentInfoAdaptor paymentInfoAdaptor = new PaymentInfoAdaptor();

		Collection<PaymentInfo> colPaymentInfo = new ArrayList<>();

		if (pax.getLccClientPaymentHolder().getPayments() != null && !pax.getLccClientPaymentHolder().getPayments().isEmpty()) {
			AdaptorUtils.adaptCollection(pax.getLccClientPaymentHolder().getPayments(), colPaymentInfo, paymentInfoAdaptor);
		}
		paxPayments.setPaxSequence(pax.getPaxSequence());
		paxPayments.setPaymantInfo(colPaymentInfo);

		return paxPayments;
	}

}
