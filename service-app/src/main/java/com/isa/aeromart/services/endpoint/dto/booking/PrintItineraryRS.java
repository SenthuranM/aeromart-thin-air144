package com.isa.aeromart.services.endpoint.dto.booking;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PrintItineraryRS extends TransactionalBaseRS {

	private String itinerary;

	public String getItinerary() {
		return itinerary;
	}

	public void setItinerary(String itinerary) {
		this.itinerary = itinerary;
	}

}
