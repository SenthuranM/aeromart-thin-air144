package com.isa.aeromart.services.endpoint.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StringUtils;

import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.commons.api.constants.CommonsConstants.HumanVerificationConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class AuthenticationFilter implements Filter {
	private static final String MODULE_BASE_PATH = "/service-app/controller/";
	private static final String PARAM_EXCLUDE_PATHS = "excludePaths";
	private static final String AUTH_TOKEN = "auth-token";
	private String[] paths;

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		if (!skipAuthentication(request)) {
			boolean chainFilter = true;
			if (isTokenPresent(request)) {
				chainFilter = tokenLogin(request, response);
			} else {
				ForceLoginInvoker.defaultLogin();
			}
			if (chainFilter) {
				chainFilter = captchaFilter(request, response);
			}

			if (chainFilter) {
				chain.doFilter(request, response);
			}
			ForceLoginInvoker.close();
		} else {
			chain.doFilter(request, response);
		}

	}

	private boolean skipAuthentication(ServletRequest request) {
		String requestURI = ((HttpServletRequest) request).getRequestURI();
		if (!requestURI.startsWith(MODULE_BASE_PATH)) {
			return true;
		} else {
			if (paths != null && paths.length > 0) {
				for (String path : paths) {
					if (!"".equals(path.trim()) && !"".equals(requestURI) && requestURI.startsWith(path.trim())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean isTokenPresent(ServletRequest request) {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		String token = httpReq.getHeader(AUTH_TOKEN);

		return token != null && !"".equals(token.trim());
	}

	private boolean tokenLogin(ServletRequest request, ServletResponse response) throws IOException {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		String token = httpReq.getHeader(AUTH_TOKEN);

		try {
			// FIXME generalize the token implementation for any token validation
			ForceLoginInvoker.defaultLogin();
			AgentToken agentToken = ModuleServiceLocator.getTravelAgentBD().getAgentTokenById(token);
			
			if (agentToken != null && !agentToken.isExpiredToken()) {
				User user = ModuleServiceLocator.getSecurityBD().getUser(agentToken.getUserName());
				String password = ModuleServiceLocator.getCryptoServiceBD().decrypt(user.getPassword());
				ForceLoginInvoker.close();
				ForceLoginInvoker.login(user.getUserId(), password);
			} else {
				ForceLoginInvoker.close();
				throw new ModuleException("Invalid token");
			}
		} catch (ModuleException e) {
			HttpServletResponse httpRes = (HttpServletResponse) response;
			httpRes.sendError(HttpServletResponse.SC_FORBIDDEN);
			httpRes.setContentType("text/plain");
			return false;
		}

		return true;
	}

	private boolean captchaFilter(ServletRequest request, ServletResponse response) {
		if (AppSysParamsUtil.isIBEHumanVerficationEnabled() && AppSysParamsUtil.isRecaptchaEnabled()) {
			try {
				HttpServletRequest httpReq = (HttpServletRequest) request;
				String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());

				boolean isCaptchaValidationAction = (requestURI.indexOf("controller/") > -1)
						&& !(requestURI.indexOf("/recaptcha/verify") > -1);

				if (isCaptchaValidationAction) {
					// Header name has to be configured in lower case in app
					// param
					String ipHitsCount = httpReq.getHeader(AppSysParamsUtil.getIPHitCountHeaderName());

					if (ipHitsCount == null) {
						ipHitsCount = httpReq.getHeader(AppSysParamsUtil.getIPHitCountHeaderName().toUpperCase());
					}

					if (ipHitsCount != null) {
						if (Integer.parseInt(ipHitsCount) > AppSysParamsUtil.maxPerIPHitCount()) {
							httpReq.setAttribute(HumanVerificationConstants.VALIDATE_CAPTCHA, true);

							Boolean captchaChallengeCleared = (Boolean) httpReq.getSession()
									.getAttribute("CAPTCHA_CHALLENGE_VALIDATED");
							if (captchaChallengeCleared == null || !captchaChallengeCleared) {
								HttpServletResponse httpRes = (HttpServletResponse) response;
								httpRes.setHeader("captcha-challenge", "true");
								httpRes.setHeader("grecaptcha-site-key", AppSysParamsUtil.getRecaptchaSiteKey());
								httpRes.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
								httpRes.setContentType("text/plain");
								return false;
							}

						}
					}
				}

			} catch (Exception ex) {

			}
		}
		return true;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String pathList = filterConfig.getInitParameter(PARAM_EXCLUDE_PATHS);
		if (pathList != null) {
			this.paths = StringUtils.tokenizeToStringArray(pathList, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);
		}
	}

}
