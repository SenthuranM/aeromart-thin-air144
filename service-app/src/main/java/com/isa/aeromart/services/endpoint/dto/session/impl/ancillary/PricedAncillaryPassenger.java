package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

public class PricedAncillaryPassenger {
	
	private String passengerRPH;

	private List<PricedAncillaryType> ancillaryTypes;

	private List<MonetaryAmendment> amendments;

	public List<PricedAncillaryType> getAncillaryTypes() {
		return ancillaryTypes;
	}

	public void setAncillaryTypes(List<PricedAncillaryType> ancillaryTypes) {
		this.ancillaryTypes = ancillaryTypes;
	}

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}

	public String getPassengerRPH() {
		return passengerRPH;
	}

	public void setPassengerRPH(String passengerRPH) {
		this.passengerRPH = passengerRPH;
	}
}
