package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRS;
import com.isa.aeromart.services.endpoint.dto.modification.CommonSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.PassengerSummary;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.BalanceQueryRequiredData;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModificationAdaptor {

	public static RequoteBalanceQueryRQ getBalanceQueryRQ(RequoteSessionStore session) {

		RequoteBalanceQueryRQ balanceQueryRQ = new RequoteBalanceQueryRQ();
		ReservationInfo resInfo = session.getResInfo();
		BalanceQueryRequiredData balanceQuerydata = session.getRequoteBalanceReqiredParams();
		balanceQueryRQ.setRemovedSegmentIds(balanceQuerydata.getRemovedReservationSegIds());
		balanceQueryRQ.setGroundFltSegByFltSegId(balanceQuerydata.getGroundFltSegByFltSegId());
		balanceQueryRQ.setPnr(resInfo.getPnr());
		balanceQueryRQ.setRequoteSegmentMap(ModificationUtils.getReQuoteResSegmentMap(session.getOndInfo()));
		FlightPriceRQ flightPriceRQ = createFlightPriceRQ(session.getResInfo().getPaxTypeWiseCount(), session.getPreferences()
				.getSeatType());
		
		
		String system = session.getPreferences().getSelectedSystem();
		String version = resInfo.getVersion();

		if (system == SYSTEM.INT.toString() || resInfo.isGroupPNR()) {
			version = ReservationCommonUtil.getCorrectInterlineVersion(version);
			balanceQueryRQ.setSystem(SYSTEM.INT);
			balanceQueryRQ.setVersion(version);
			balanceQueryRQ.setTransactionIdentifier(resInfo.getTransactionId());
			balanceQueryRQ.setCarrierWiseFlightRPHMap(ModificationUtils.getCarrierWiseRPHs(session.getOndInfo()));
			balanceQueryRQ.setGroupPnr(resInfo.getPnr());
		}
		if (session.getNameChangedPaxMap() != null)
			balanceQueryRQ.setNameChangedPaxMap(session.getNameChangedPaxMap()); // set nameChangeData

		if (AppSysParamsUtil.isLMSEnabled()) {
			Double remainingLoyaltyPoints = CustomerUtil.getRemainingLoyaltypoints(resInfo.getCustomerId());
			balanceQueryRQ.setRemainingLoyaltyPoints(remainingLoyaltyPoints);
		}

		balanceQueryRQ.setOndFareTypeByFareIdMap(ModificationUtils.getONDFareTypeByFareIdMap(session.getOndInfo()));
		balanceQueryRQ.setOldFareIdByFltSegIdMap(ModificationUtils.getOldFareIdByFltSegIdMap(session.getOndInfo()));

		QuotedFareRebuildDTO fareInfo = null;
		fareInfo = new QuotedFareRebuildDTO(session.getFareInfo().getFareSegmentChargeTO(), flightPriceRQ, null);
		balanceQueryRQ.setFareInfo(fareInfo);

		if (balanceQuerydata.isAnciModification()) {
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			if (resInfo.getPaxList() != null && !resInfo.getPaxList().isEmpty()) {
				for (ReservationPaxTO resPax : resInfo.getPaxList()) {
					List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
					extChgList.addAll(resPax.getExternalCharges());
					paxExtChgMap.put(resPax.getSeqNumber(), extChgList);
				}
				balanceQueryRQ.setPaxExtChgMap(paxExtChgMap);
			}
		}
		return balanceQueryRQ;
	}

	@Deprecated
	public static BalanceSummaryRS createBalanceSummaryReseponse(RequoteSessionStore session,
			ReservationBalanceTO reservationBalanceTo) {

		CommonBalanceSummaryInfoAdaptor summaryInfoAdaptor = new CommonBalanceSummaryInfoAdaptor(session.getResInfo());

		CommonSummaryInfo summaryInfo = summaryInfoAdaptor.adapt(reservationBalanceTo);

		PassengerSummaryAdaptar adaptor = new PassengerSummaryAdaptar();
		List<PassengerSummary> paxList = new ArrayList<PassengerSummary>();
		AdaptorUtils.adaptCollection(reservationBalanceTo.getPassengerSummaryList(), paxList, adaptor);

		BalanceSummaryRS response = new BalanceSummaryRS();
//TODO Remove Comments
/*		UpdatedChargeForDisplay updatedCharge;
		UpdatedChargesAdaptor updatedChargesAdaptor = new UpdatedChargesAdaptor(session.getResInfo(), session.getExtChargeInfo(),
				session.getFareInfo().getCreditCardFee());
		updatedCharge = updatedChargesAdaptor.adapt(reservationBalanceTo);
		RequoteBalanceInfo requoteBalanceInfo;
		RequoteBalanceAdaptor requoteBalanceAdaptor = new RequoteBalanceAdaptor(session.getResInfo().getPaxCreditMap(
				session.getResInfo().getPnr()));
		requoteBalanceInfo = requoteBalanceAdaptor.adapt(reservationBalanceTo);
		
		response.setModifiedBalanceInfo(requoteBalanceInfo);
		response.setUpdatedChargeForDisplay(updatedCharge);*/

		response.setReservationBalanceInfo(summaryInfo);
		response.setPaxList(paxList);
		return response;

	}

	public static FlightPriceRQ createFlightPriceRQ(TravellerQuantity travellerQuantity, String seatType) {

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();
		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(travellerQuantity.getAdultCount());
		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(travellerQuantity.getChildCount());
		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(travellerQuantity.getInfantCount());
		// setting traveler preferences
		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		travelerPref.setBookingType(seatType);
		return flightPriceRQ;
	}

	@Deprecated
	public static BalanceSummaryRS getCancelBalanceSummary(ReservationBalanceTO lCCClientReservationBalanceTO,
			Map<String, BigDecimal> paxCreditMap, ReservationInfo resInfo) {
		BalanceSummaryRS response = new BalanceSummaryRS();
		
		CommonBalanceSummaryInfoAdaptor summaryInfoAdaptor = new CommonBalanceSummaryInfoAdaptor(resInfo);
		
		CommonSummaryInfo summaryInfo = summaryInfoAdaptor.adapt(lCCClientReservationBalanceTO);
		
		PassengerSummaryAdaptar adaptor = new PassengerSummaryAdaptar();
		List<PassengerSummary> paxList = new ArrayList<PassengerSummary>();
		AdaptorUtils.adaptCollection(lCCClientReservationBalanceTO.getPassengerSummaryList(), paxList, adaptor);
		
		response.setPaxList(paxList);
		response.setReservationBalanceInfo(summaryInfo);
		
//TODO Remove Comments
/*		CancelBalanceSummaryDTO cancelBalanceSummary = new CancelBalanceSummaryDTO();
		if (lCCClientReservationBalanceTO != null) {

			LCCClientSegmentSummaryTO lCCClientSegmentSummaryTO = lCCClientReservationBalanceTO.getSegmentSummary();
			cancelBalanceSummary.setAirFare(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentFareAmount()));
			cancelBalanceSummary.setTax(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentTaxAmount(), lCCClientSegmentSummaryTO.getCurrentSurchargeAmount())));
			cancelBalanceSummary.setDiscount(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentDiscount()));
			cancelBalanceSummary.setCancelChrage(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO
					.getTotalCnxCharge()));

			if (lCCClientReservationBalanceTO.getPassengerSummaryList() != null) {
				for (LCCClientPassengerSummaryTO pax : lCCClientReservationBalanceTO.getPassengerSummaryList()) {
					if (!PaxTypeTO.INFANT.equalsIgnoreCase(pax.getPaxType())) {
						if (PaxTypeTO.ADULT.equalsIgnoreCase(pax.getPaxType())) {
							cancelBalanceSummary.setPerPaxCancelCharge(AccelAeroCalculator.formatAsDecimal(pax
									.getTotalCnxCharge()));
							break;
						}
					}
				}
			}
			cancelBalanceSummary.setCreditableAmount(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentRefunds()));
			cancelBalanceSummary.setAvailableCredit(AccelAeroCalculator.formatAsDecimal(ModificationUtils.getTotalPaxCredit(
					paxCreditMap).negate()));
			cancelBalanceSummary.setFinalCredit(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO
					.getTotalCreditAmount()));
		}
		response.setCancelBalanceSummary(cancelBalanceSummary);*/
		response.setSuccess(true);

		return response;
	}
}
