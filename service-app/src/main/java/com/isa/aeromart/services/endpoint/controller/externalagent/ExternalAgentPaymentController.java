package com.isa.aeromart.services.endpoint.controller.externalagent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.externalagent.ExternalAgentAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.ExternalAgentConstants;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.dto.externalagent.BasePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.BlockRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.BlockRS;
import com.isa.aeromart.services.endpoint.dto.externalagent.ConfirmRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.ConfirmRS;
import com.isa.aeromart.services.endpoint.dto.externalagent.CreditPaymentRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.PaymentInfo;
import com.isa.aeromart.services.endpoint.dto.externalagent.QueryRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.QueryRS;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.externalagent.ExternalAgentUtil;
import com.isa.aeromart.services.endpoint.utils.payment.PaymentUtils;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airtravelagents.api.dto.ExternalAgentRQDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentCreditBlock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

@Controller
@RequestMapping(value = "externalagent", method = RequestMethod.POST, produces = {
		MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
public class ExternalAgentPaymentController extends StatefulController {

	@ResponseBody
	@RequestMapping(value = "/credit/block", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BlockRS blockCredit(@RequestBody BlockRQ blockRQ, @RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken)
			throws ModuleException {
		BlockRS response = new BlockRS();

		if (validateHash(blockRQ.getHashValue(), authToken, blockRQ.getPaymentInfo(), null)) {
			AgentToken agentToken = ModuleServiceLocator.getTravelAgentBD().getAgentTokenById(authToken);
			if (agentToken != null) {
				validatePaymentInfo(blockRQ.getPaymentInfo(), agentToken.getAgent());
				BigDecimal amount = new BigDecimal(blockRQ.getPaymentInfo().getAmount());
				Integer blockId = ModuleServiceLocator.getTravelAgentFinanceBD()
						.blockAgentCredit(ExternalAgentUtil.getAgentCreditBlock(agentToken, AgentCreditBlock.BLOCKED, amount));
				response.setBlockReferenceId(blockId.toString());
				response.setPaymentInfo(blockRQ.getPaymentInfo());
				response.setPaymentInfoInAgentCurrency(
						getPaymentInfoInAgentCurrency(agentToken.getAgent().getCurrencyCode(), amount));
				response.setSuccess(true);
			} else {
				response.setErrors(Arrays.asList("external.agent.no.active.users=No active users"));
				response.setSuccess(false);
			}
		} else {
			response.setErrors(Arrays.asList("external.agent.invalid.hash"));
			response.setSuccess(false);
		}
		return response;
	}

	private void validatePaymentInfo(PaymentInfo paymentInfo, Agent agent) throws ModuleException {

		if (!Agent.PAYMENT_MODE_ONACCOUNT.equals(paymentInfo.getPaymentMethod())
				&& !Agent.PAYMENT_MODE_BSP.equals(paymentInfo.getPaymentMethod())) {
			throw new ModuleException("external.agent.invalid.paymentmethod");
		}
		if (!AppSysParamsUtil.getBaseCurrency().equals(paymentInfo.getCurrencyCode())) {
			throw new ModuleException("external.agent.invalid.paymentcurrency");
		}
		if (!validateCurrency(paymentInfo.getAmount())) {
			throw new ModuleException("external.agent.invalid.paymentcurrency.format");
		}
		BigDecimal amount = new BigDecimal(paymentInfo.getAmount().trim());
		if (amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new ModuleException("external.agent.negative.pay.amount");
		}
		if (Agent.PAYMENT_MODE_ONACCOUNT.equals(paymentInfo.getPaymentMethod())) {
			if (agent.getAgentSummary().getAvailableCredit().compareTo(amount) < 0) {
				throw new ModuleException("external.agent.logic.creditbalance.exceeds");
			}
		}
		if (Agent.PAYMENT_MODE_BSP.equals(paymentInfo.getPaymentMethod())) {
			if ((agent.getPaymentMethod() != null && !agent.getPaymentMethod().contains(Agent.PAYMENT_MODE_BSP))
					|| AppSysParamsUtil.getRestrictedPaymentModes(String.valueOf(PromotionCriteriaConstants.ProductType.GOQUO))
							.contains(Agent.PAYMENT_MODE_BSP)) {
				throw new ModuleException("external.agent.bsp.not.enabled");
			}
			if (agent.getAgentSummary().getAvailableBSPCredit().compareTo(amount) < 0) {
				throw new ModuleException("external.agent.logic.creditbalance.exceeds");
			}
		}
	}

	private String getMerchantIdFromToken(String token) {
		String merchantId = "";
		if (token != null) {
			merchantId = token.split("-")[0];
			if (merchantId == null || merchantId.isEmpty()) {
				merchantId = ExternalAgentConstants.MERCHANT_ID;
			}
		}
		return merchantId;
	}

	private boolean validateCurrency(String money) {
		boolean isValied = true;

		if (money == null || money.isEmpty()) {
			isValied = false;
		}

		Pattern p = Pattern.compile("[0-9]+([,.][0-9]{1,2})?");
		Matcher m = p.matcher(money);
		if (!m.matches()) {
			isValied = false;
		}
		return isValied;
	}

	private boolean validateHash(String secureHash, String token, PaymentInfo paymentInfo, String orderID) {
		boolean valid = false;
		Merchant merchant = PaymentUtils.getMerchant(getMerchantIdFromToken(token));
		if (merchant != null) {
			List<String> responseParams = new ArrayList<String>();
			responseParams.add(token);
			responseParams.add(paymentInfo.getAmount());
			responseParams.add(paymentInfo.getCurrencyCode());
			if (orderID != null && !"".equals(orderID)) {
				responseParams.add(orderID);
			}
			if (secureHash.equals(PaymentUtils.buildHashString(responseParams, merchant.getSecureHashKey()))) {
				valid = true;
			}
		}
		return valid;
	}

	private boolean validateOrderID(String orderID) {
		boolean valid = false;
		if (orderID != null && !"".equals(orderID)) {
			valid = true;
		}
		return valid;
	}

	private PaymentInfo getPaymentInfoInAgentCurrency(String currencyCode, BigDecimal amount) throws ModuleException {
		PaymentInfo paymnetInfo = new PaymentInfo();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		paymnetInfo.setAmount(AccelAeroCalculator
				.parseBigDecimal(BLUtil.getAmountInLocal(amount, currencyCode, exchangeRateProxy).doubleValue()).toString());
		paymnetInfo.setCurrencyCode(currencyCode);
		paymnetInfo.setPaymentMethod(Agent.PAYMENT_MODE_ONACCOUNT);
		return paymnetInfo;
	}

	private PaymentInfo getPaymentInfoInAgentCurrencyForBSP(String currencyCode, BigDecimal amount) throws ModuleException {
		PaymentInfo paymnetInfo = new PaymentInfo();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		paymnetInfo.setAmount(AccelAeroCalculator
				.parseBigDecimal(BLUtil.getAmountInLocal(amount, currencyCode, exchangeRateProxy).doubleValue()).toString());
		paymnetInfo.setCurrencyCode(currencyCode);
		paymnetInfo.setPaymentMethod(Agent.PAYMENT_MODE_BSP);
		return paymnetInfo;
	}

	@ResponseBody
	@RequestMapping(value = "/credit/confirm", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ConfirmRS confirm(@RequestBody ConfirmRQ confirmRQ, @RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken)
			throws ModuleException {
		ConfirmRS response = new ConfirmRS();
		if (validateHash(confirmRQ.getHashValue(), authToken, confirmRQ.getPaymentInfo(), confirmRQ.getOrderId())
				&& validateOrderID(confirmRQ.getOrderId())) {
			AgentToken agentToken = ModuleServiceLocator.getTravelAgentBD().getAgentTokenById(authToken);
			if (agentToken != null) {
				validatePaymentInfo(confirmRQ.getPaymentInfo(), agentToken.getAgent());
				BigDecimal amount = new BigDecimal(confirmRQ.getPaymentInfo().getAmount());
				PayCurrencyDTO payCurrencyDTO = ExternalAgentAdaptor
						.getPayCurrencyDTO(confirmRQ.getPaymentInfo().getCurrencyCode(), amount);
				PaymentReferenceTO paymentReferenceTO = ExternalAgentAdaptor.getPaymentReferenceTO(confirmRQ);
				ExternalAgentRQDTO externalAgentRQDTO = ExternalAgentAdaptor.adaptExternalAgentRQDTO(confirmRQ);
				AgentTransaction agentTransaction = ModuleServiceLocator.getTravelAgentFinanceBD()
						.confirmBlockedCredit(agentToken, externalAgentRQDTO, payCurrencyDTO, paymentReferenceTO);
				response.setPaymentReferenceId(agentTransaction.getTnxId() + "");
				response.setOrderId(confirmRQ.getOrderId());
				response.setPaymentInfo(confirmRQ.getPaymentInfo());
				response.setPaymentInfoInAgentCurrency(
						getPaymentInfoInAgentCurrency(agentToken.getAgent().getCurrencyCode(), amount));
				if (agentTransaction != null) {
					response.setSuccess(true);
				}
			} else {
				response.setErrors(Arrays.asList("external.agent.no.active.users=No active users"));
				response.setSuccess(false);
			}
		} else {
			response.setErrors(Arrays.asList("external.agent.invalid.hash=Invalid hash value"));
			response.setSuccess(false);
		}

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/credit/payment", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ConfirmRS payment(@RequestBody CreditPaymentRQ paymentRQ,
			@RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken) throws ModuleException {
		ConfirmRS response = new ConfirmRS();

		if (validateHash(paymentRQ.getHashValue(), authToken, paymentRQ.getPaymentInfo(), paymentRQ.getOrderId())
				&& validateOrderID(paymentRQ.getOrderId())) {
			AgentToken agentToken = ModuleServiceLocator.getTravelAgentBD().getAgentTokenById(authToken);
			if (agentToken != null) {
				validatePaymentInfo(paymentRQ.getPaymentInfo(), agentToken.getAgent());
				BigDecimal amount = new BigDecimal(paymentRQ.getPaymentInfo().getAmount());
				PayCurrencyDTO payCurrencyDTO = null;

				if (Agent.PAYMENT_MODE_BSP.equals(paymentRQ.getPaymentInfo().getPaymentMethod())) {
					BigDecimal amountInLocalCurrency = new BigDecimal(
							getPaymentInfoInAgentCurrencyForBSP(agentToken.getAgent().getCurrencyCode(), amount).getAmount());
					String agentStationCurrencyCode = ModuleServiceLocator.getTravelAgentBD()
							.getCurrencyCodeByStationCode(agentToken.getAgent().getStationCode());
					payCurrencyDTO = ExternalAgentAdaptor.getPayCurrencyDTO(agentStationCurrencyCode, amountInLocalCurrency);
				} else {
					payCurrencyDTO = ExternalAgentAdaptor.getPayCurrencyDTO(paymentRQ.getPaymentInfo().getCurrencyCode(), amount);
				}
				PaymentReferenceTO paymentReferenceTO = ExternalAgentAdaptor.getPaymentReferenceTO(paymentRQ);
				ExternalAgentRQDTO externalAgentRQDTO = ExternalAgentAdaptor.adaptExternalAgentRQDTO(paymentRQ);
				AgentTransaction agentTransaction = ModuleServiceLocator.getTravelAgentFinanceBD().externalPayment(agentToken,
						externalAgentRQDTO, payCurrencyDTO, paymentReferenceTO);
				response.setPaymentReferenceId(agentTransaction.getTnxId() + "");
				response.setOrderId(paymentRQ.getOrderId());
				response.setPaymentInfo(paymentRQ.getPaymentInfo());
				if (agentTransaction != null) {
					response.setSuccess(true);
				}
			} else {
				response.setErrors(Arrays.asList("external.agent.no.active.users=No active users"));
				response.setSuccess(false);
			}
		} else {
			response.setErrors(Arrays.asList("external.agent.invalid.hash=Invalid hash value"));
			response.setSuccess(false);
		}

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/credit/query", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public QueryRS query(@RequestBody QueryRQ externalAgentRQ, @RequestHeader(ExternalAgentConstants.AUTH_TOKEN) String authToken)
			throws ModuleException {

		QueryRS response = new QueryRS();
		AgentToken agentToken = ModuleServiceLocator.getTravelAgentBD().getAgentTokenById(authToken);
		if (agentToken != null) {
			AgentCreditBlock agentCreditBlock = ModuleServiceLocator.getTravelAgentFinanceBD()
					.getAgentCreditBlock(new Integer(externalAgentRQ.getBlockReferenceId()), null);
			if (agentCreditBlock != null) {
				response.setBlockReferenceId(agentCreditBlock.getAgentCreditBlockId() + "");
				response.setStatus(agentCreditBlock.getStatus());
				response.setSuccess(true);
			} else {
				response.setErrors(Arrays.asList("external.agent.invalid.blockId=Invalid Block ID"));
				response.setSuccess(false);
			}
		} else {
			response.setErrors(Arrays.asList("external.agent.no.active.users=No active users"));
			response.setSuccess(false);
		}
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/credit/void", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ConfirmRS reverse(@RequestBody BasePaymentRQ externalAgentRQ) throws ModuleException {

		// TODO : Need to do completely
		ConfirmRS response = new ConfirmRS();
		response.setSuccess(true);
		return response;
	}
}
