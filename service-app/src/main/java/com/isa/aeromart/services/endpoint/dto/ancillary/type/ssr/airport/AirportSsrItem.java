package com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.airport;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;

public class AirportSsrItem extends AncillaryItem{

	private String airportCode;
	
	private String ssrCode;
	
	private String ssrImagePath;
	
	private String ssrThumbnailImagePath;
	
	private String ssrName;
	
	private String applicablityType;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrImagePath() {
		return ssrImagePath;
	}

	public void setSsrImagePath(String ssrImagePath) {
		this.ssrImagePath = ssrImagePath;
	}

	public String getSsrThumbnailImagePath() {
		return ssrThumbnailImagePath;
	}

	public void setSsrThumbnailImagePath(String ssrThumbnailImagePath) {
		this.ssrThumbnailImagePath = ssrThumbnailImagePath;
	}

	public String getSsrName() {
		return ssrName;
	}

	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}
	
	public String getApplicablityType() {
		return applicablityType;
	}

	public void setApplicablityType(String applicablityType) {
		this.applicablityType = applicablityType;
	}
	
}
