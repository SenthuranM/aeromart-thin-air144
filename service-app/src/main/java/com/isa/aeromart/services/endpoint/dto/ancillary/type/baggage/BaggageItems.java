package com.isa.aeromart.services.endpoint.dto.ancillary.type.baggage;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class BaggageItems extends AncillaryItemLeaf {

    private List<BaggageItem> items;

    public BaggageItems() {
        setType(AncillariesConstants.Type.BAGGAGE);
    }

    public List<BaggageItem> getItems() {
		return items;
	}

	public void setItems(List<BaggageItem> items) {
		this.items = items;
	}
}
