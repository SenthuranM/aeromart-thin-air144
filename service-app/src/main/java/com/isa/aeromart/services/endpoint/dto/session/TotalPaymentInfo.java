package com.isa.aeromart.services.endpoint.dto.session;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.PaxWiseTotal;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class TotalPaymentInfo {

	private BigDecimal totalWithAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalWithoutAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAvailabilityCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPaymentCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private FareSegChargeTO pricingInformation;

	private FinancialStore financialStore;

	private TravellerQuantity travellerQuantity;

	public TotalPaymentInfo(FareSegChargeTO pricingInformation, FinancialStore financialStore, TravellerQuantity travellerQuantity) {
		this.pricingInformation = pricingInformation;
		this.financialStore = financialStore;
		this.travellerQuantity = travellerQuantity;
		buildPaymentInfo();
	}

	private void buildPaymentInfo() {

		if (pricingInformation != null && travellerQuantity != null) {
			Map<String, Double> paxWiseTotalChargesMap = pricingInformation.getPaxWiseTotalCharges();
			Map<String, Double> totalJourneyFareMap = pricingInformation.getTotalJourneyFare();

			BigDecimal totalAdult = AccelAeroCalculator.add(new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.ADULT)),
					new BigDecimal(totalJourneyFareMap.get(PaxTypeTO.ADULT)));
			totalAdult = AccelAeroCalculator.multiply(totalAdult, travellerQuantity.getAdultCount());

			BigDecimal totalChild = BigDecimal.ZERO;
			if (travellerQuantity.getChildCount() > 0) {
				totalChild = AccelAeroCalculator.add(new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.CHILD)), new BigDecimal(
						totalJourneyFareMap.get(PaxTypeTO.CHILD)));
				totalChild = AccelAeroCalculator.multiply(totalChild, travellerQuantity.getChildCount());
			}

			BigDecimal totalInfant = BigDecimal.ZERO;
			if (travellerQuantity.getInfantCount() > 0) {
				totalInfant = AccelAeroCalculator.add(new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.INFANT)),
						new BigDecimal(totalJourneyFareMap.get(PaxTypeTO.INFANT)));

				totalInfant = AccelAeroCalculator.multiply(totalInfant, travellerQuantity.getInfantCount());
			}
			totalWithoutAncillaryCharges = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.add(totalAdult, totalChild,
					totalInfant).doubleValue());

			if (financialStore != null) {

				totalPaymentCharges = financialStore.getTotalPaymentCharges();
				
				// at the moment service tax is only calculated on PaxWisePaymentCharges
				totalServiceTaxAmount = financialStore.getTotalPaxWisePaymentCharges();
				
				totalAvailabilityCharges = financialStore.getTotalAvailabilityCharges();

				totalAncillaryCharges = financialStore.getTotalAnicillaryCharges();

				// add flexi if it's added at availability
				totalAncillaryCharges = AccelAeroCalculator.add(totalAncillaryCharges, totalAvailabilityCharges);

			}

			// flexi at availability is included in totalAncillaryCharges
			totalWithAncillaryCharges = AccelAeroCalculator.add(totalWithoutAncillaryCharges, totalAncillaryCharges,
					totalServiceTaxAmount);

			totalTicketPrice = AccelAeroCalculator.add(totalWithAncillaryCharges, totalPaymentCharges);

		} else {
			throw new IllegalArgumentException("missing.data.to.build.payment.info");
		}
	}

	public BigDecimal getTotalWithAncillaryCharges() {
		return totalWithAncillaryCharges;
	}

	public BigDecimal getTotalWithoutAncillaryCharges() {
		return totalWithoutAncillaryCharges;
	}

	public BigDecimal getTotalAncillaryCharges() {
		return totalAncillaryCharges;
	}

	public BigDecimal getTotalPaymentCharges() {
		return totalPaymentCharges;
	}

	public BigDecimal getTotalTicketPrice() {
		return totalTicketPrice;
	}

	public BigDecimal getTotalServiceTaxAmount() {
		return totalServiceTaxAmount;
	}

	public void setTotalServiceTaxAmount(BigDecimal totalServiceTaxAmount) {
		this.totalServiceTaxAmount = totalServiceTaxAmount;
	}

	public List<PaxWiseTotal> getPaxWiseTotalList() {

		List<PaxWiseTotal> paxWiseTotalList = new ArrayList<PaxWiseTotal>();

		Map<String, Double> totalJourneyFareMap = pricingInformation.getTotalJourneyFare();

		BigDecimal totalAdult = new BigDecimal(totalJourneyFareMap.get(PaxTypeTO.ADULT));
		totalAdult = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(totalAdult,
				travellerQuantity.getAdultCount()).doubleValue());
		buildPaxWiseFareList(paxWiseTotalList, totalAdult.doubleValue(), PaxTypeTO.ADULT, travellerQuantity.getAdultCount());

		BigDecimal totalChild = BigDecimal.ZERO;
		if (travellerQuantity.getChildCount() > 0) {
			totalChild = new BigDecimal(totalJourneyFareMap.get(PaxTypeTO.CHILD));
			totalChild = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(totalChild,
					travellerQuantity.getChildCount()).doubleValue());
			buildPaxWiseFareList(paxWiseTotalList, totalChild.doubleValue(), PaxTypeTO.CHILD, travellerQuantity.getChildCount());

		}

		BigDecimal totalInfant = BigDecimal.ZERO;
		if (travellerQuantity.getInfantCount() > 0) {
			totalInfant = new BigDecimal(totalJourneyFareMap.get(PaxTypeTO.INFANT));
			totalInfant = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(totalInfant,
					travellerQuantity.getInfantCount()).doubleValue());
			buildPaxWiseFareList(paxWiseTotalList, totalInfant.doubleValue(), PaxTypeTO.INFANT,
					travellerQuantity.getInfantCount());

		}

		return paxWiseTotalList;
	}

	public double getTotalCharges() {
		Map<String, Double> paxWiseTotalChargesMap = pricingInformation.getPaxWiseTotalCharges();

		BigDecimal totalAdult = new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.ADULT));
		totalAdult = AccelAeroCalculator.multiply(totalAdult, travellerQuantity.getAdultCount());

		BigDecimal totalChild = BigDecimal.ZERO;
		if (travellerQuantity.getChildCount() > 0) {
			totalChild = new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.CHILD));
			totalChild = AccelAeroCalculator.multiply(totalChild, travellerQuantity.getChildCount());
		}

		BigDecimal totalInfant = BigDecimal.ZERO;
		if (travellerQuantity.getInfantCount() > 0) {
			totalInfant = new BigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.INFANT));
			totalInfant = AccelAeroCalculator.multiply(totalInfant, travellerQuantity.getInfantCount());
		}

		BigDecimal totalCharges = AccelAeroCalculator.add(totalAdult, totalChild, totalInfant);

		return AccelAeroCalculator.parseBigDecimal(totalCharges.doubleValue()).doubleValue();
	}

	private void buildPaxWiseFareList(List<PaxWiseTotal> paxWiseTotalList, double paxTypeTotal, String paxType, int noOfPax) {
		PaxWiseTotal paxWiseTotal = new PaxWiseTotal();
		paxWiseTotal.setPaxType(paxType);
		paxWiseTotal.setNoOfPax(noOfPax);
		paxWiseTotal.setTotalFare(paxTypeTotal);
		paxWiseTotalList.add(paxWiseTotal);
	}

	public FinancialStore getFinancialStore() {
		return financialStore;
	}
	
	

}
