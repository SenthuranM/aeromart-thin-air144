package com.isa.aeromart.services.endpoint.dto.passenger;

import java.util.List;

public class PaxDOBValidationRequest {

	private List<PaxDOBValidationDetails> paxDOBValidationDetailsList;

	public List<PaxDOBValidationDetails> getPaxDOBValidationDetailsList() {
		return paxDOBValidationDetailsList;
	}

	public void setPaxDOBValidationDetailsList(
			List<PaxDOBValidationDetails> paxDOBValidationDetailsList) {
		this.paxDOBValidationDetailsList = paxDOBValidationDetailsList;
	}

}
