package com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.inflight;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class InFlightSsrItems extends AncillaryItemLeaf {
	
	private List<InFlightSsrItem> items;

    public InFlightSsrItems() {
        setType(AncillariesConstants.Type.SSR_IN_FLIGHT);
    }

    public List<InFlightSsrItem> getItems() {
		return items;
	}

	public void setItems(List<InFlightSsrItem> items) {
		this.items = items;
	}
	
}
