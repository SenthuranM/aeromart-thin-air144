package com.isa.aeromart.services.endpoint.dto.customer;


public class UserTokenDetails {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
