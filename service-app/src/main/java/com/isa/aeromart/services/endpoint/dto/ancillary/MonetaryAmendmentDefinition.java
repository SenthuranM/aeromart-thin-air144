package com.isa.aeromart.services.endpoint.dto.ancillary;

import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class MonetaryAmendmentDefinition {

	public enum AmendmentCategory {
		CREDIT,
		DEBIT,
		MONEY
	}

	private Integer definitionId;

	private AmendmentCategory category;

	private AncillariesConstants.MonetaryAmendment type;

	private String description;

	public Integer getDefinitionId() {
		return definitionId;
	}

	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}

	public AmendmentCategory getCategory() {
		return category;
	}

	public void setCategory(AmendmentCategory category) {
		this.category = category;
	}

	public AncillariesConstants.MonetaryAmendment getType() {
		return type;
	}

	public void setType(AncillariesConstants.MonetaryAmendment type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
