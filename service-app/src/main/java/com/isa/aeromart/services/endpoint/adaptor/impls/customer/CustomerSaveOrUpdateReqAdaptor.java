package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerContact;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerSaveOrUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSDetails;
import com.isa.aeromart.services.endpoint.utils.common.StringUtil;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

public class CustomerSaveOrUpdateReqAdaptor implements Adaptor<CustomerSaveOrUpdateRQ , LmsMember>{

	@Override
	public LmsMember adapt(CustomerSaveOrUpdateRQ customerSaveOrUpdateReq) {
		
		LmsMember lmsMember = new LmsMember();
		LMSDetails lmsDetails = customerSaveOrUpdateReq.getLmsDetails();
		CustomerDetails customer = customerSaveOrUpdateReq.getCustomer();
		PhoneNumberReqAdaptor phoneNoAdaptor = new PhoneNumberReqAdaptor();
		CustomerContact customerContact = customer.getCustomerContact();
		
		lmsMember.setDateOfBirth(lmsDetails.getDateOfBirth());
		if(lmsDetails.getEmailStatus() != null){
			lmsMember.setEmailConfirmed(lmsDetails.getEmailStatus().toCharArray()[0]);
		}
		lmsMember.setFfid(lmsDetails.getFfid());
		lmsMember.setHeadOFEmailId(StringUtil.nullConvertToString(lmsDetails.getHeadOFEmailId()));
		lmsMember.setLanguage(lmsDetails.getLanguage());
		lmsMember.setPassportNum(StringUtil.nullConvertToString(lmsDetails.getPassportNum()));
		lmsMember.setRefferedEmail(StringUtil.nullConvertToString(lmsDetails.getRefferedEmail()));
		
		lmsMember.setFirstName(customer.getFirstName());
		lmsMember.setLastName(customer.getLastName());
		lmsMember.setMobileNumber(phoneNoAdaptor.adapt(customerContact.getMobileNumber()));
		lmsMember.setPhoneNumber(phoneNoAdaptor.adapt(customerContact.getTelephoneNumber()));
		lmsMember.setRegDate(new Date());
		lmsMember.setEmailConfSent('N');
	    lmsMember.setEmailConfirmed('N');
	    lmsMember.setNationalityCode(String.valueOf(customer.getNationalityCode()));
	    lmsMember.setResidencyCode(customer.getCountryCode());
	    lmsMember.setEnrollingCarrier(AppSysParamsUtil.getDefaultCarrierCode());
	    
	    String title = customer.getTitle();
		
		if(title.equalsIgnoreCase("MR")){
			lmsMember.setGenderTypeId(1);
		}
		else if(title.equalsIgnoreCase("MS")){
			lmsMember.setGenderTypeId(2);
		}
		else{
			lmsMember.setGenderTypeId(0);
		}	
		
		Map<String, String> locationRefMap = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyLocationExtReferences();
		Map<String, String> enrollingChannelMap = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyEnrollingChannelExtReferences();
		
		String enrollmentLocExtRef = lmsDetails.getAppCode();
		
		if (enrollmentLocExtRef.equals("APP_IBE")){
			lmsMember.setEnrollmentLocExtRef(locationRefMap.get(AppIndicatorEnum.APP_IBE.toString()));
			lmsMember.setEnrollingChannelExtRef(enrollingChannelMap.get(AppIndicatorEnum.APP_IBE.toString()));
		}
		else if (enrollmentLocExtRef.equals("APP_XBE")){
			lmsMember.setEnrollmentLocExtRef(locationRefMap.get(AppIndicatorEnum.APP_XBE.toString()));
			lmsMember.setEnrollingChannelExtRef(enrollingChannelMap.get(AppIndicatorEnum.APP_XBE.toString()));
		}
		lmsMember.setEnrollingChannelIntRef(enrollingChannelMap.get(AppIndicatorEnum.APP_IBE.toString()));
		String password = UUID.randomUUID().toString().replace("-", "").substring(0,20);
		
		lmsMember.setPassword(password.toUpperCase());

		return lmsMember;
	}
	
}
