package com.isa.aeromart.services.endpoint.controller.masterdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.masterdata.AirportTransformer;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportDTO;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.commons.api.exception.ModuleException;

@Controller
@RequestMapping("airports")
public class AirportController {

	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<AirportDTO> listAirports() {

		List<AirportDTO> airportList = null;
		try {
			Collection<Airport> airports = ModuleServiceLocator.getAirportBD().getAirports();
			AirportTransformer transformer = new AirportTransformer();
			
			if(airports != null && !airports.isEmpty()){
				airportList = new ArrayList<AirportDTO>();
				for(Airport airport:airports){
					airportList.add(transformer.adapt(airport));
				}
			}	
		} catch (ModuleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return airportList;	
	}
}
