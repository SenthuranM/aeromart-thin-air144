package com.isa.aeromart.services.endpoint.utils.common;

public class StringUtil {

	protected static String toInitCap(String str) {
		String intCap = str;
		if (str != null && !"".equals(str.trim())) {
			intCap = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
		}
		return intCap;
	}

	public static String nullConvertToString(Object obj) {
		String strReturn = "";

		if (obj == null) {
			strReturn = "";
		} else {
			strReturn = obj.toString().trim();
		}
		return strReturn;
	}

	public static boolean isEmpty(String value) {
		if (value == null || value.trim().isEmpty())
			return true;
		else
			return false;
	}
	
}
