package com.isa.aeromart.services.endpoint.dto.booking;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PrintTaxInvoiceRS extends TransactionalBaseRS {

	private String taxInvoice;

	public String getTaxInvoice() {
		return taxInvoice;
	}

	public void setTaxInvoice(String taxInvoice) {
		this.taxInvoice = taxInvoice;
	}

}
