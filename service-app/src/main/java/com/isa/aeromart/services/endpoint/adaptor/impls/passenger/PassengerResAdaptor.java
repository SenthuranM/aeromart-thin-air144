package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ExtraDetailsAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.ExtraDetails;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class PassengerResAdaptor implements Adaptor<LCCClientReservationPax, Passenger> {

	@Override
	public Passenger adapt(LCCClientReservationPax lccClientReservationPax) {
		Passenger passenger = new Passenger();
		List<ExtraDetails> extras = new ArrayList<>();
		String paxType;
		passenger.setDateOfBirth(lccClientReservationPax.getDateOfBirth());
		passenger.setFirstName(lccClientReservationPax.getFirstName());
		passenger.setLastName(lccClientReservationPax.getLastName());
		passenger.setNationality(lccClientReservationPax.getNationalityCode());
		passenger.setPaxSequence(lccClientReservationPax.getPaxSequence());
		passenger.setTitle(lccClientReservationPax.getTitle());
		if (lccClientReservationPax.getParent() != null) {
			LCCClientReservationPax parent = lccClientReservationPax.getParent();
			passenger.setTravelWith(parent.getPaxSequence());
			passenger.setParentName(parent.getTitle() + " " + parent.getFirstName() + " " + parent.getLastName());
		}

		AdditionalInfoResAdaptor additionalInfoResAdaptor = new AdditionalInfoResAdaptor();
		LCCClientReservationAdditionalPax lccClientReservationAdditionalPax = lccClientReservationPax.getLccClientAdditionPax();
		
		if (lccClientReservationAdditionalPax != null) {
			passenger.setAdditionalInfo(additionalInfoResAdaptor.adapt(lccClientReservationAdditionalPax));
		}
		
		AdaptorUtils.adaptCollection(lccClientReservationPax.getSelectedAncillaries(), extras, new ExtraDetailsAdaptor());
		passenger.setExtras(extras);
		
		paxType = lccClientReservationPax.getPaxType();
		if(paxType.equals(PaxTypeTO.ADULT)){
			passenger.setPaxType("adult");
		}else if(paxType.equals(PaxTypeTO.CHILD)){
			passenger.setPaxType("child");
		}else if(paxType.equals(PaxTypeTO.INFANT)){
			passenger.setPaxType("infant");
		}
		
		return passenger;
	}

}
