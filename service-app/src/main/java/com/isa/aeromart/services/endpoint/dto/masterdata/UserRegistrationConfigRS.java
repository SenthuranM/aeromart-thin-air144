package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;

public class UserRegistrationConfigRS extends TransactionalBaseRS {

	private List<UserRegConfigDTO> configList;
	private List<String[]> questionInfo;
	
	public List<UserRegConfigDTO> getConfigList() {
		return configList;
	}
	
	public void setConfigList(List<UserRegConfigDTO> configList) {
		this.configList = configList;
	}
	
	public List<String[]> getQuestionInfo() {
		return questionInfo;
	}
	
	public void setQuestionInfo(List<String[]> questionInfo) {
		this.questionInfo = questionInfo;
	}
	
	
	
}
