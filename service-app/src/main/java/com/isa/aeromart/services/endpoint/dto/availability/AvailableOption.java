package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.FlightSegment;
import com.isa.aeromart.services.endpoint.dto.common.ViaPoint;

public class AvailableOption {

	private boolean seatAvailable;

	private boolean selected;
	
	private String originAirportCode;
	
	private String destinationAirportCode;

	private List<ViaPoint> viaPoints;

	private String totalDuration;

	private List<FlightSegment> segments;

	private List<AvailableFareClass> availableFareClasses;

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public List<ViaPoint> getViaPoints() {
		return viaPoints;
	}

	public void setViaPoints(List<ViaPoint> viaPoints) {
		this.viaPoints = viaPoints;
	}

	public String getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}

	public List<FlightSegment> getSegments() {
		if (segments == null) {
			segments = new ArrayList<FlightSegment>();
		}
		return segments;
	}

	public void setSegments(List<FlightSegment> segments) {
		this.segments = segments;
	}

	public List<AvailableFareClass> getAvailableFareClasses() {
		if (availableFareClasses == null) {
			availableFareClasses = new ArrayList<AvailableFareClass>();
		}
		return availableFareClasses;
	}

	public void setAvailableFareClasses(List<AvailableFareClass> availableFareClasses) {
		this.availableFareClasses = availableFareClasses;
	}

	public String getOriginAirportCode() {
		return originAirportCode;
	}

	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}


}
