package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Assignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.DefaultAncillarySelectionRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;

public class DefaultSelectionToBlcokRequestAdaptor implements Adaptor<DefaultAncillarySelectionRS, BlockAncillaryRQ> {

	private Map<String, SelectedAncillary> anciWiseSelectedAncillaries;
	
	@Override
	public BlockAncillaryRQ adapt(DefaultAncillarySelectionRS source) {
		
		SelectedAncillary selectedAncillary;
		Preference preference;
		PassengerAssignee passengerAssignee;
		Selection selection;
		
		anciWiseSelectedAncillaries = new HashMap<String, SelectedAncillary>();
		BlockAncillaryRQ blockAncillaryRQ = new BlockAncillaryRQ();
		blockAncillaryRQ.setAncillaries(new ArrayList<SelectedAncillary>());
		blockAncillaryRQ.setTransactionId(source.getTransactionId());
		
		for(AncillaryPassenger ancillaryPassenger : source.getAncillaryReservation().getPassengers()){
			preference = new Preference();
			passengerAssignee = new PassengerAssignee();
			passengerAssignee.setAssignType(Assignee.AssignType.PASSENGER);
			passengerAssignee.setPassengerRph(ancillaryPassenger.getPassengerRph());
			preference.setAssignee(passengerAssignee);
			
			for(PricedAncillaryType pricedAncillaryType : ancillaryPassenger.getAncillaryTypes()){
				preference.setSelections(new ArrayList<Selection>());
				
				for(AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()){
					selection = new Selection();
					selection.setScope(ancillaryScope.getScope());
					selection.setSelectedItems((List<SelectedItem>)(List<?>) ancillaryScope.getAncillaries());
					preference.getSelections().add(selection);
				}
				
				if(!anciWiseSelectedAncillaries.containsKey(pricedAncillaryType.getAncillaryType())){
					selectedAncillary = new SelectedAncillary();
					selectedAncillary.setType(pricedAncillaryType.getAncillaryType());
					selectedAncillary.setPreferences(new ArrayList<Preference>());
				} else {
					selectedAncillary = anciWiseSelectedAncillaries.get(pricedAncillaryType.getAncillaryType());
				}
				selectedAncillary.getPreferences().add(preference); 
				anciWiseSelectedAncillaries.put(pricedAncillaryType.getAncillaryType(), selectedAncillary);
			}
		}
		for(SelectedAncillary selectedAncillaryRef : anciWiseSelectedAncillaries.values()){
			blockAncillaryRQ.getAncillaries().add(selectedAncillaryRef);
		}
		
		return blockAncillaryRQ;
	}

}
