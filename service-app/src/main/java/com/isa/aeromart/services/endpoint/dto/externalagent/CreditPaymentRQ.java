package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotEmpty;

public class CreditPaymentRQ extends BasePaymentRQ {

	@NotEmpty
	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
