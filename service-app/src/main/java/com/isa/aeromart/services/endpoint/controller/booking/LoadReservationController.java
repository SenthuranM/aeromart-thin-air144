package com.isa.aeromart.services.endpoint.controller.booking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ConfirmationResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.LoadReservationRSAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.dto.booking.BookingConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRQ;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRS;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationValidateRes;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.utils.booking.AdvertiesmentUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

@Controller
@RequestMapping(value = "reservation", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
public class LoadReservationController extends StatefulController {

	@Autowired
	BookingService bookingService;

	@ResponseBody
	@RequestMapping(value = "/load")
	public LoadReservationRS loadReservation(@RequestBody LoadReservationRQ loadReservationRQ) throws ModuleException {
		LoadReservationRS loadReservationRS = new LoadReservationRS();
		loadReservationRS.setSuccess(true);
		LCCClientReservation reservation;
		loadReservationRQ.setValidateRQ(true);
		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		String customerID = customerSessionStore.getLoggedInCustomerID();
		LoadReservationValidateRes loadReservationValidateRes = bookingService.validateLoadReservationRQ(loadReservationRQ,
				getTrackInfo(), customerID);

		boolean isGroupPnr;
		isGroupPnr = loadReservationValidateRes.isGroupPnr();
		reservation = bookingService.loadProxyReservation(loadReservationRQ.getPnr(), isGroupPnr, getTrackInfo(), true,
				(customerID != null && !customerID.isEmpty()) ? true : false, null, null, AppSysParamsUtil.isPromoCodeEnabled());

		bookingService.checkLoadingConstraint(reservation);
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		LoadReservationRSAdaptor loadReservationRSAdaptor = new LoadReservationRSAdaptor(isGroupPnr,
				AdvertiesmentUtil.getCarURLForAdvertiesment(reservation), customerID,
				bookingService.getGroundSegments(reservation.getSegments()),
				masterDataService.getCurrencyExchangeRate(reservation.getLastCurrencyCode()));
		loadReservationRS = loadReservationRSAdaptor.adapt(reservation);
		loadReservationRS.setSuccess(true);

		return loadReservationRS;
	}

	@ResponseBody
	@RequestMapping(value = "/loadConfirmationPage")
	public BookingConfirmationRS getReservationDetails(@RequestBody LoadReservationRQ loadReservationRQ) throws ModuleException {

		BookingConfirmationRS bookingConfirmationRS = new BookingConfirmationRS();
		bookingConfirmationRS.setSuccess(true);
		TrackInfoDTO trackInfo = getTrackInfo();
		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		loadReservationRQ.setValidateRQ(true);
		String customerID = customerSessionStore.getLoggedInCustomerID();
		LCCClientReservation reservation;
		LoadReservationValidateRes loadReservationValidateRes = bookingService.validateLoadReservationRQ(loadReservationRQ,
				getTrackInfo(), customerID);

		boolean isGroupPnr;
		isGroupPnr = loadReservationValidateRes.isGroupPnr();
		reservation = bookingService.loadProxyReservation(loadReservationRQ.getPnr(), isGroupPnr, trackInfo, true,
				(customerID != null && !customerID.isEmpty()) ? true : false, null, null, AppSysParamsUtil.isPromoCodeEnabled());
		
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		ConfirmationResAdaptor confirmationResAdaptor = new ConfirmationResAdaptor(
				AdvertiesmentUtil.getCarURLForAdvertiesment(reservation),
				AdvertiesmentUtil.getHotelURLForAdvertiesment(reservation),
				masterDataService.getCurrencyExchangeRate(reservation.getLastCurrencyCode()));
		bookingService.checkLoadingConstraint(reservation);
		bookingConfirmationRS = confirmationResAdaptor.adapt(reservation);
		bookingConfirmationRS.setTaxInvoiceGenerated(bookingService.isTaxInvoiceAvailable(loadReservationRQ));
		bookingConfirmationRS.setSuccess(true);

		return bookingConfirmationRS;
	}

}