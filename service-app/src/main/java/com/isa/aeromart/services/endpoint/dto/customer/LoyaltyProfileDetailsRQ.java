package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class LoyaltyProfileDetailsRQ extends TransactionalBaseRQ {

	@NotNull
	private String loyaltyAccountNo;

	@NotNull
	private Date dateOfBirth;

	@NotNull
	private String city;

	public String getLoyaltyAccountNo() {
		return loyaltyAccountNo;
	}

	public void setLoyaltyAccountNo(String loyaltyAccountNo) {
		this.loyaltyAccountNo = loyaltyAccountNo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
