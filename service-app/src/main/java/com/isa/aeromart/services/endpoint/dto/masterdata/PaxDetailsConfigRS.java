package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PaxDetailsConfigRS extends TransactionalBaseRS{
	
	private Map<String , PaxDetailsField> paxDetailsConfig;
	
	private PaxCutOffYears paxCutOffYears;

	public Map<String, PaxDetailsField> getPaxDetailsConfig() {
		return paxDetailsConfig;
	}

	public void setPaxDetailsConfig(Map<String, PaxDetailsField> paxDetailsConfig) {
		this.paxDetailsConfig = paxDetailsConfig;
	}

	public PaxCutOffYears getPaxCutOffYears() {
		return paxCutOffYears;
	}

	public void setPaxCutOffYears(PaxCutOffYears paxCutOffYears) {
		this.paxCutOffYears = paxCutOffYears;
	}
	
}
