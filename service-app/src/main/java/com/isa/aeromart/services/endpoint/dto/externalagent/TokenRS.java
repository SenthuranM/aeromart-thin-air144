package com.isa.aeromart.services.endpoint.dto.externalagent;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class TokenRS extends BaseRS {

	private String token;
	private Date expiryTimeStamp;

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiryTimeStamp() {
		return expiryTimeStamp;
	}

	public void setExpiryTimeStamp(Date expiryTimeStamp) {
		this.expiryTimeStamp = expiryTimeStamp;
	}
}
