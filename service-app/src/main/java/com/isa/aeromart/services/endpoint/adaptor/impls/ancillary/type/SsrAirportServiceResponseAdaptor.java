package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability.ApplicableType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.airport.AirportSsrItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.airport.AirportSsrItems;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class SsrAirportServiceResponseAdaptor extends AncillariesResponseAdaptor {
	@Override
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability,
				LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AIRPORT_SERVICE);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	@Override
	public AncillaryType toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		AncillaryType ancillaryType = new AncillaryType();
		List<Provider> providers = new ArrayList<Provider>();
		SystemProvided provider = new SystemProvided();
		List<AvailableAncillary> availableAncillay = new ArrayList<AvailableAncillary>();
		AvailableAncillary availableAncillary = new AvailableAncillary();
		List<AvailableAncillaryUnit> availableUnits = new ArrayList<AvailableAncillaryUnit>();

		String flightSegmentRPH;
		FlightSegmentTO flightSegmentTO;

		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.SSR_AIRPORT.name());
		provider.setProviderType(Provider.ProviderType.SYSTEM);
		Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> source = (Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO>) ancillaries;

		for (AirportServiceKeyTO airportServiceKeyTO : source.keySet()) {

			AvailableAncillaryUnit availableAncillaryUnit = new AvailableAncillaryUnit();
			AirportSsrItems airportSsrItems = new AirportSsrItems();
			List<AirportSsrItem> airportSsrItemsList = new ArrayList<AirportSsrItem>();
			AirportScope airportScope = new AirportScope();

			LCCFlightSegmentAirportServiceDTO lCCFlightSegmentAirportServiceDTO = source.get(airportServiceKeyTO);

			flightSegmentTO = lCCFlightSegmentAirportServiceDTO.getFlightSegmentTO();
			flightSegmentRPH = rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber());

			airportScope.setScopeType(ScopeType.AIRPORT);
			airportScope.setFlightSegmentRPH(flightSegmentRPH);
			airportScope.setAirportCode(airportServiceKeyTO.getAirport());
			
			switch (airportServiceKeyTO.getAirportType()) {
			case "A":
				airportScope.setPoint(AirportScope.Point.ARRIVAL.name());
				break;
			case "D":
				airportScope.setPoint(AirportScope.Point.DEPARTURE.name());
				break;
			case "T":
				airportScope.setPoint(AirportScope.Point.TRANSIT.name());
				break;
			}

			Applicability applicability = new Applicability();
			applicability.setApplicableType(ApplicableType.PASSENGER.name());
			availableAncillaryUnit.setApplicability(applicability);
			availableAncillaryUnit.setScope(airportScope);

			for (LCCAirportServiceDTO lccAirportServiceDTO : lCCFlightSegmentAirportServiceDTO.getAirportServices()) {

				AirportSsrItem airportSsrItem = new AirportSsrItem();
				List<Charge> charges = new ArrayList<Charge>();
				Charge charge;

				airportSsrItem.setAirportCode(lccAirportServiceDTO.getAirportCode());
				airportSsrItem.setDescription(lccAirportServiceDTO.getSsrDescription());
				airportSsrItem.setItemId(lccAirportServiceDTO.getSsrCode());
				airportSsrItem.setSsrCode(lccAirportServiceDTO.getSsrCode());
				airportSsrItem.setSsrImagePath(lccAirportServiceDTO.getSsrImagePath());
				airportSsrItem.setSsrThumbnailImagePath(lccAirportServiceDTO.getSsrThumbnailImagePath());
				airportSsrItem.setSsrName(lccAirportServiceDTO.getSsrName());
				airportSsrItem.setApplicablityType(lccAirportServiceDTO.getApplicabilityType());

				if (lccAirportServiceDTO.getApplicabilityType().equals(LCCAirportServiceDTO.APPLICABILITY_TYPE_PAX)) {

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.ADULT.name());
					charge.setAmount(lccAirportServiceDTO.getAdultAmount());
					charges.add(charge);

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.CHILD.name());
					charge.setAmount(lccAirportServiceDTO.getChildAmount());
					charges.add(charge);

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.INFANT.name());
					charge.setAmount(lccAirportServiceDTO.getInfantAmount());
					charges.add(charge);

				} else if (lccAirportServiceDTO.getApplicabilityType().equals(LCCAirportServiceDTO.APPLICABILITY_TYPE_RESERVATION)) {

					charge = new Charge();
					charge.setChargeBasis(Charge.ChargeBasis.ALL_PASSENGERS.name());
					charge.setAmount(lccAirportServiceDTO.getReservationAmount());
					charges.add(charge);
				}

				airportSsrItem.setCharges(charges);
				airportSsrItemsList.add(airportSsrItem);
			}

			airportSsrItems.setItems(airportSsrItemsList);
			availableAncillaryUnit.setItemsGroup(airportSsrItems);
			availableUnits.add(availableAncillaryUnit);

		}

		availableAncillary.setAvailableUnits(availableUnits);
		availableAncillay.add(availableAncillary);
		provider.setAvailableAncillaries(availableAncillary);
		providers.add(provider);
		ancillaryType.setProviders(providers);

		return ancillaryType;
	}


	@Override
	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {

		AirportScope airportScope = (AirportScope)scope;

		LCCAirportServiceDTO selectedAirportService = new LCCAirportServiceDTO();
		selectedAirportService.setSsrCode(item.getId());
		selectedAirportService.setAirportCode(airportScope.getAirportCode());
		selectedAirportService.setAdultAmount(item.getAmount());
		selectedAirportService.setServiceCharge(item.getAmount());
		//FIXME set ammount for child
		return selectedAirportService;
	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

	}
}
