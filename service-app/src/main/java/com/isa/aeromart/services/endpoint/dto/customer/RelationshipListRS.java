package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class RelationshipListRS extends BaseRS {
	
	List<FamilyRelationshipDTO> relationshipList;

	public List<FamilyRelationshipDTO> getRelationshipList() {
		return relationshipList;
	}

	public void setRelationshipList(List<FamilyRelationshipDTO> relationshipList) {
		this.relationshipList = relationshipList;
	}

}
