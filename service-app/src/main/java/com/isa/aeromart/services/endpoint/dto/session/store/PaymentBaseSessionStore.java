package com.isa.aeromart.services.endpoint.dto.session.store;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherInformation;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface PaymentBaseSessionStore {

	public enum LoyaltyPaymentOption {
		TOTAL, PART, NONE;
	}

	public boolean isGroupPNR();

	public boolean isAdminFeeInitiated() throws ModuleException;

	public void setAdminFeeInitiated() throws ModuleException;

	public void storePostPaymentInformation(PostPaymentDTO postPaymentDTO);

	public PostPaymentDTO getPostPaymentInformation();

	public LoyaltyInformation getLoyaltyInformation();

	public void storeLoyaltyInformation(LoyaltyInformation loyaltyInformation);

	public void addPaymentCharge(ExternalChgDTO ccChgDTO);

	public Collection<LCCClientExternalChgDTO> getExternalCharges();

	public void removePaymentCharge(EXTERNAL_CHARGES extChgEnum);

	// other module implementation - AvailabilitySessionStore
	public FareSegChargeTO getFareSegChargeTOForPayment();

	public List<SessionFlightSegment> getSelectedSegmentsForPayment();

	public PreferenceInfo getPreferencesForPayment();

	public TravellerQuantity getTravellerQuantityForPayment();

	public List<ServiceTaxContainer> getApplicableServiceTaxes();

	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalCharges();

	public void storePaymentStatus(PaymentStatus actionStatus);

	public PaymentStatus getPaymentStatus();

	public void clearPaymentInformation();

	public void storeOperationType(OperationTypes operationTypes);

	public OperationTypes getOperationType();

	public void setOriginCountryCodeForTest(String originCountryCodeForTest);

	public String getOriginCountryCodeForTest();

	public void trackPaymentAttempts() throws ModuleException;

	public void removePaymentPaxCharge(EXTERNAL_CHARGES extChgEnum);

	FinancialStore getFinancialStore();

	ReservationInfo getResInfo();

	List<SessionFlexiDetail> getSessionFlexiDetailForPayment();

	public BigDecimal getServiceTaxForTransactionFee();

	public void setServiceTaxForTransactionFee(BigDecimal serviceTaxForTransactionFee);

	public String getVoucherOTP();

	public void setVoucherOTP(String voucherOTP);

	public VoucherInformation getVoucherInformation();

	public PayByVoucherInfo getPayByVoucherInfo();
	
	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo);
	
	public Map<String, String> getVoucherOTPMap();

}
