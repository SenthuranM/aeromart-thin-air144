package com.isa.aeromart.services.endpoint.utils.customer;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerAliasAdaptor;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerCredit;
import com.isa.aeromart.services.endpoint.dto.customer.ReservationDetails;
import com.isa.aeromart.services.endpoint.dto.customer.SegmentDetails;
import com.isa.aeromart.services.endpoint.dto.payment.Card;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptions;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.common.StringUtil;
import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.utils.CustomerAliasGenerator;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.webplatform.core.commons.MapGenerator;

public class CustomerUtil {
	
	private final static String YES = "Y";
	
	private final static String NO = "N";

	public static Collection<CustomerCredit> getCustomerCreditFromAllCarriers(
			int intCustomerID, TrackInfoDTO trackInfo) throws ModuleException {
		List<String> pnrList = getPnrList(intCustomerID);

		Collection<PaxCreditDTO> colPaxCredit = ModuleServiceLocator
				.getReservationQueryBD().getPaxCreditForPnrs(pnrList);

		Collection<PaxCreditDTO> colOtherCarriersPaxCredit = null;
		if (AppSysParamsUtil.isLCCConnectivityEnabled() && hasOtherCarrierReservation(intCustomerID, trackInfo)) {
			try {
				colOtherCarriersPaxCredit = ModuleServiceLocator
						.getLCCPaxCreditBD().getPaxCreditAccrossCarriers(
								pnrList, trackInfo);
				fillOriginatorPnr(colOtherCarriersPaxCredit);
			} catch (ModuleException e) {
				// WS access Issue. Skip other carrier pax credit retrieval
			}
		}

		if (colOtherCarriersPaxCredit != null
				&& !colOtherCarriersPaxCredit.isEmpty()) {
			colPaxCredit.addAll(colOtherCarriersPaxCredit);
		}

		Collection<CustomerCredit> colCustCreditDto = mergeCustomerCreditFromCarriers(colPaxCredit);
		return sortCustomerCreditByExpiryDate(colCustCreditDto);
	}
	
	private static boolean hasOtherCarrierReservation(int customerID, TrackInfoDTO trackInfoDTO) throws ModuleException{
		Collection<ReservationDTO> reservationDetailsList = new ArrayList<>();
		reservationDetailsList.addAll(ModuleServiceLocator.getReservationQueryBD().getAfterReservations(customerID, new Date(), null, trackInfoDTO));
		reservationDetailsList.addAll(ModuleServiceLocator.getReservationQueryBD().getEarlyReservations(customerID, new Date(), null, trackInfoDTO));
		for (ReservationDTO reservation : reservationDetailsList) {
			if (StringUtils.isNotEmpty(reservation.getOriginatorPnr())) {
				return true;
			}
		}
		return false;
	}


	private static void fillOriginatorPnr(
			Collection<PaxCreditDTO> colOtherCarriersPaxCredit) {
		if (colOtherCarriersPaxCredit != null
				&& !colOtherCarriersPaxCredit.isEmpty()) {
			for (PaxCreditDTO paxCreditDTO : colOtherCarriersPaxCredit) {
				paxCreditDTO.setOriginatorPnr(paxCreditDTO.getPnr());
			}
		}
	}

	private static List<String> getPnrList(int customerID)
			throws ModuleException {
		return ModuleServiceLocator.getReservationQueryBD().getPnrList(
				customerID);
	}

	private static Collection<CustomerCredit> mergeCustomerCreditFromCarriers(
			Collection<PaxCreditDTO> coll) {

		Collection<CustomerCredit> customerCreditList = new ArrayList<CustomerCredit>();
		Map<String, CustomerCredit> custCreditMap = new HashMap<String, CustomerCredit>();
		if (coll != null) {
			for (PaxCreditDTO paxCreditDTO : coll) {
				CustomerCredit customerCredit;
				String key = paxCreditDTO.getPnr() + "#"
						+ paxCreditDTO.getPaxSequence();
				SimpleDateFormat smpdtFormat = new SimpleDateFormat(
						"dd MMM yyyy");
				DecimalFormat decimalFormat = new DecimalFormat("#########.00");

				if (custCreditMap.containsKey(key)) {
					customerCredit = custCreditMap.get(key);
					BigDecimal bal = AccelAeroCalculator.add(new BigDecimal(
							customerCredit.getBalance()), paxCreditDTO
							.getBalance());
					customerCredit.setBalance(decimalFormat.format(bal));
				} else {
					customerCredit = new CustomerCredit();
					customerCredit.setPnr(paxCreditDTO.getPnr());
					customerCredit.setGroupPnr(paxCreditDTO.getOriginatorPnr());
					customerCredit.setFullName(StringUtil
							.nullConvertToString(paxCreditDTO.getTitle())
							+ " "
							+ StringUtil.nullConvertToString(paxCreditDTO
									.getFirstName())
							+ " "
							+ StringUtil.nullConvertToString(paxCreditDTO
									.getLastName()));

					BigDecimal bal = new BigDecimal(
							(paxCreditDTO.getBalance() != null) ? paxCreditDTO
									.getBalance().toString() : "0");

					customerCredit.setBalance(decimalFormat.format(bal));
					customerCredit.setDateOfExpiry(smpdtFormat
							.format(paxCreditDTO.getDateOfExpiry()));
				}

				custCreditMap.put(key, customerCredit);
			}
		}

		if (!custCreditMap.isEmpty()) {
			for (Iterator mapIt = custCreditMap.entrySet().iterator(); mapIt
					.hasNext();) {
				Map.Entry<String, CustomerCredit> entry = (Map.Entry<String, CustomerCredit>) mapIt
						.next();
				customerCreditList.add(entry.getValue());
			}
		}

		return customerCreditList;
	}

	protected static Collection<CustomerCredit> sortCustomerCreditByExpiryDate(
			Collection<CustomerCredit> colCustDto) {

		List<CustomerCredit> custCreditList = new ArrayList<CustomerCredit>(
				colCustDto);

		Collections.sort(custCreditList, new Comparator<CustomerCredit>() {

			public int compare(CustomerCredit obj1, CustomerCredit obj2) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
				Date d1 = null;
				Date d2 = null;
				try {
					d1 = (Date) formatter.parse(obj1.getDateOfExpiry());
					d2 = (Date) formatter.parse(obj2.getDateOfExpiry());
				} catch (ParseException e) {
					/*
					 * this should be avoid
					 */
				}

				if (d1 != null & d2 != null) {
					return d1.compareTo(d2);
				} else {
					return 0;
				}

			}

		});

		return custCreditList;
	}

	public static String calculateCustomerTotalCredit(
			Collection<CustomerCredit> col) {
		BigDecimal dblTotal = null;

		if (col != null) {
			dblTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (CustomerCredit custCredit : col) {

				BigDecimal val = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (custCredit.getBalance() != null
						&& !custCredit.getBalance().equals("")) {
					val = new BigDecimal(custCredit.getBalance());
				}

				dblTotal = AccelAeroCalculator.add(dblTotal, val);

			}
		}

		return dblTotal == null ? "" : dblTotal.toString();

	}
	
	/**
	 * Get Customer Reservation list from today
	 * 
	 * @param customerID
	 * @param dateNow
	 * @param trackInfoDTO TODO
	 * @return
	 * @throws ModuleException
	 */
	public static List<ReservationDetails> getAfterReservationsList(int customerID, Date dateNow, TrackInfoDTO trackInfoDTO) throws ModuleException {
		Collection<ReservationDTO> reservationDTOs = ModuleServiceLocator.getReservationQueryBD()
				.getAfterReservations(customerID, dateNow, null, trackInfoDTO);
		return composeReservationsList(reservationDTOs, false, false);
	}

	/**
	 * Get All Customer Reservations
	 * 
	 * @param customerID
	 * @param dateNow
	 * @param trackInfoDTO TODO
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ReservationDetails> getEarlyReservationsList(
			int customerID, Date dateNow, TrackInfoDTO trackInfoDTO) throws ModuleException {
		Collection<ReservationDTO> reservationDTOs = ModuleServiceLocator.getReservationQueryBD()
				.getEarlyReservations(customerID,dateNow, null, trackInfoDTO);
		return composeReservationsList(reservationDTOs, false, true);
	}

	/**
	 * 
	 * @param colReservationDTO
	 * @param includeExchangedSegment
	 *            TODO
	 * @param isFlown TODO
	 * @return
	 * @throws ModuleException
	 */
	private static List<ReservationDetails> composeReservationsList(Collection<ReservationDTO> colReservationDTO,
			boolean includeExchangedSegment, boolean isFlown) throws ModuleException {
		//Collection<ReservationListTO> colReservationListTO = new ArrayList<ReservationListTO>();
		
		List<ReservationDetails> reservationDetailsList = new ArrayList<ReservationDetails>();
		
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd MMM yyyy");
        //SimpleDateFormat smpdtValDate = new SimpleDateFormat("yyMMddHHmm");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");

		if (colReservationDTO != null && colReservationDTO.size() > 0) {
			Iterator<ReservationDTO> iterReservation = colReservationDTO.iterator();

			// Reservation Information
			while (iterReservation.hasNext()) {
				ReservationDTO reservationDTO = iterReservation.next();
				ReservationContactInfo reservationContactInfo = reservationDTO.getReservationContactInfo();

				
				ReservationDetails reservationDetails = new ReservationDetails();
				
				reservationDetails.setPnr(StringUtil.nullConvertToString(reservationDTO.getPnr()));
				reservationDetails.setBookingDate(reservationDTO.getPnrDate());
				reservationDetails.setPaxName(StringUtil.nullConvertToString(reservationContactInfo.getFirstName()) + ", "
						+ StringUtil.nullConvertToString(reservationContactInfo.getLastName()));
				reservationDetails.setPnrStatus(reservationDTO.getStatus());
				reservationDetails.setFlown(isFlown);
				reservationDetails.setContactLastName(reservationDTO.getReservationContactInfo().getLastName());
	
				List<SegmentDetails> segmentDetailsList = new ArrayList<SegmentDetails>();
				
				//ReservationListTO reservationListTO = new ReservationListTO();

				for (Iterator iterator = reservationDTO.getSegments().iterator(); iterator.hasNext();) {
					ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) iterator.next();

					if (!includeExchangedSegment
							&& ReservationSegmentSubStatus.EXCHANGED.equals(reservationSegmentDTO.getSubStatus())) {
						continue;
					}

					SegmentDetails segmentDetails = new SegmentDetails();
					
					String dateTime = smpdtDate.format(reservationSegmentDTO.getArrivalDate()) + " " 
							+ smpdtTime.format(reservationSegmentDTO.getArrivalDate());

					segmentDetails.setArrivalDateTime(dateTime);
					
					dateTime = smpdtDate.format(reservationSegmentDTO.getDepartureDate()) + " " 
						+ smpdtTime.format(reservationSegmentDTO.getDepartureDate());
					
					segmentDetails.setDepartureDateTime(dateTime);

					boolean isOpenReturn = reservationSegmentDTO.isOpenReturnSegment();

					String segmentStatus = reservationSegmentDTO.getStatus();
					if (isOpenReturn && !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segmentStatus)) {
						segmentStatus = "";
					}
					
					segmentDetails.setStatus(segmentStatus);

					String segmentCode = BeanUtils.nullHandler(reservationSegmentDTO.getSegmentCode());

					if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
						segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
					}

					segmentDetails.setOrignNDest(getONDDetails(segmentCode));
					segmentDetails.setSegmentCode(segmentCode);
					segmentDetails.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(reservationSegmentDTO.getReturnFlag()));
					segmentDetails.setFilghtDesignator(reservationSegmentDTO.getFlightNo());
					segmentDetails.setDepartureDateLong(reservationSegmentDTO.getZuluDepartureDate().getTime());
					segmentDetails.setPnrSegId(reservationSegmentDTO.getPnrSegId());
					segmentDetails.setSubStationShortName(reservationSegmentDTO.getSubStationShortName());

					segmentDetailsList.add(segmentDetails);
				}

				if (reservationDTO.getColExternalSegments() != null && reservationDTO.getColExternalSegments().size() > 0) {
					for (ReservationExternalSegmentTO reservationExternalSegmentTO : reservationDTO.getColExternalSegments()) {
						String segmentCode = reservationExternalSegmentTO.getSegmentCode();

						if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
							segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
						}
						
						SegmentDetails segmentDetails = new SegmentDetails();
						
						String dateTime = smpdtDate.format(reservationExternalSegmentTO.getArrivalDate()) + " " 
								+ smpdtTime.format(reservationExternalSegmentTO.getArrivalDate());

						segmentDetails.setArrivalDateTime(dateTime);
						
						dateTime = smpdtDate.format(reservationExternalSegmentTO.getDepartureDate()) + " " 
							+ smpdtTime.format(reservationExternalSegmentTO.getDepartureDate());
						
						segmentDetails.setDepartureDateTime(dateTime);
						segmentDetails.setOrignNDest(getONDDetails(segmentCode));
						segmentDetails.setSegmentCode(segmentCode);
						segmentDetails.setStatus(reservationExternalSegmentTO.getStatus());
						segmentDetails.setDepartureDateLong(reservationExternalSegmentTO.getDepartureDate().getTime());
						segmentDetails.setPnrSegId(reservationExternalSegmentTO.getPnrExtSegId());
						segmentDetails.setSubStationShortName(null);
						
						segmentDetailsList.add(segmentDetails);
					}
				}

				Collections.sort(segmentDetailsList);
				setReturnFlagForSegments(segmentDetailsList);
				sortByReservationStatus(segmentDetailsList);
				reservationDetails.setSegmentDetails(segmentDetailsList);
				reservationDetails.setReservationType(ReservationApiUtils
						.getReservationType(convertToLccClientResSegmentList(segmentDetailsList)));
				reservationDetailsList.add(reservationDetails);
			}
		}

		return reservationDetailsList;
	}
	
	private static TravellerQuantity getTravellerQuantity(ReservationDTO reservationDTO) {
		TravellerQuantity travellerQuantity = new TravellerQuantity();
		travellerQuantity.setAdultCount(reservationDTO.getAdultCount());
		travellerQuantity.setChildCount(reservationDTO.getChildCount());
		travellerQuantity.setInfantCount(reservationDTO.getInfantCount());

		return travellerQuantity;
	}

	private static String getONDDetails(String strOND) {
		Map mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strOND.split("/");
		strSegements = "";
		for (int i = 0; i < strArrSegment.length; i++) {
			if (AppSysParamsUtil.isHideStopOverEnabled() && strArrSegment.length > 2 && i > 0 && i < (strArrSegment.length - 1))
				continue;
			if (strSegements != "") {
				strSegements += " / ";
			}
			if (mapAirports.get(strArrSegment[i]) != null) {
				strSegements += mapAirports.get(strArrSegment[i]);
			} else {
				strSegements += strArrSegment[i];
			}
		}
		return strSegements;
	}
	
	protected static Collection<SegmentDetails> sortByReservationStatus(List<SegmentDetails> segmentDetailsList) {
		Collections.sort(segmentDetailsList, new Comparator<SegmentDetails>() {
			
			public int compare(SegmentDetails obj1, SegmentDetails obj2) {
				return (obj1.getStatus()).compareTo(obj2.getStatus());
			}

		});
		return segmentDetailsList;
	}
	@Deprecated
	public static Double getRemainingLoyaltypoints(String customerId) {
		Double remainingLoyaltyPoints =null ;
		if (customerId != null && !"".equals(customerId)) {
			
			/*
			 * Important :  method Depricated (Wrong functionality)
			 * Calling lms provider for fetch point balance using customer id but should change this to customer email id
			 * Remote call returns return code as 5000 (member not found) and remaining point balance as zero
			 * Still Service app doesn't get error due to this.
			 * But keeping this is getting low performance so remove remote call and return hard coded zero due to time frame
			 * and maintain current scenario 
			 *TODO Fix this and test whether any failures.
			 * 
			 * */
			
			remainingLoyaltyPoints = 0D;
			/*LoyaltyPointDetailsDTO loyaltyPointDetailsDTO;
			try {
				loyaltyPointDetailsDTO = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyMemberPointBalances(customerId);
				if (loyaltyPointDetailsDTO != null) {
					remainingLoyaltyPoints = loyaltyPointDetailsDTO.getMemberPointsAvailable();
				}
			} catch (ModuleException e) {
				//TODO return 0 points while handling 
			}*/
		}
		return remainingLoyaltyPoints;
	}
	
	public static List<SeatType> getPreferenceSeatTypes() {
		return ModuleServiceLocator.getCustomerBD().getPreferenceSeatTypes();
	}

	public static String getCustomerPreferredSeatType(int customerId) {
		return ModuleServiceLocator.getCustomerBD().getCustomerPreferredSeatType(customerId);
	}

	public static List<CustomerPreferredMealDTO> getCustomerPreferredMeals(int customerId) {
		return ModuleServiceLocator.getCustomerBD().getCustomerPreferredMeals(customerId);
	}
	
	/**
	 * TODO : Refactor to a BE
	 */
	public static void saveCustomerAlias(String customerID, PostPaymentDTO postPaymentDTO) throws ModuleException {

		if (postPaymentDTO != null && !com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(customerID)) {
			IPGRequestDTO ipgRequestDTO = postPaymentDTO.getIpgRequestDTO();

			if (ipgRequestDTO != null) {
				String alias = ipgRequestDTO.getAlias();
				boolean saveAlias = ipgRequestDTO.isSaveCreditCard();

				if (saveAlias && !com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(alias)) {
					AirCustomerServiceBD airCustomerDelegate = ModuleServiceLocator.getCustomerBD();
					CustomerAliasAdaptor customerAliasAdaptor = new CustomerAliasAdaptor();
					CustomerAlias customerAlias = customerAliasAdaptor.adapt(ipgRequestDTO);
					airCustomerDelegate.save(customerAlias);

				}
			}
		}
	}
	
	public static void overrideAndInjectAlias(String customerID, PaymentOptions paymentOptions) throws ModuleException {
		Card card = null;
		if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(customerID)) {
			if (paymentOptions != null) {
				List<PaymentGateway> paymentGateways = paymentOptions.getPaymentGateways();
				if (paymentGateways != null && !paymentGateways.isEmpty()) {
					List<Card> cards = paymentGateways.get(0).getCards();
					if (cards != null && !cards.isEmpty()) {
						card = cards.get(0);
					}
				}
			}

			if (card != null) {
				if (card.isSaveCreditCard()) {
					String generatedAlias = CustomerAliasGenerator.generateAlias(card.getCardNo());
					card.setAlias(generatedAlias);
				}
			}
		}
	}

	private static void setReturnFlagForSegments(List<SegmentDetails> segmentDetailsList) {
		List<LCCClientReservationSegment> resSegmentList = convertToLccClientResSegmentList(segmentDetailsList);

		Map<String, String> segmentReturnMap = ReservationApiUtils.getSegmentReturnMap(resSegmentList);

		for (SegmentDetails segDetail : segmentDetailsList) {
			boolean eligibleForReturn = segDetail.getPnrSegId() != null
					&& segmentReturnMap.get(segDetail.getPnrSegId().toString()) != null
					&& !ReservationConstants.SegmentStatus.CANCELED.equals(segDetail.getStatus());
			boolean returnFlag = eligibleForReturn ? YES.equals(segmentReturnMap.get(segDetail.getPnrSegId().toString())) : false;
			segDetail.setReturnFlag(returnFlag);
		}
	}

	private static List<LCCClientReservationSegment> convertToLccClientResSegmentList(List<SegmentDetails> segmentDetailsList) {
		List<LCCClientReservationSegment> resSegmentList = new ArrayList<>();

		for (SegmentDetails segDetail : segmentDetailsList) {
			LCCClientReservationSegment resSegment = new LCCClientReservationSegment();
			resSegment.setDepartureDateZulu(new Date(segDetail.getDepartureDateLong() * 1000));
			resSegment.setSubStationShortName(segDetail.getSubStationShortName());
			resSegment.setPnrSegID(segDetail.getPnrSegId());
			resSegment.setSegmentCode(segDetail.getSegmentCode());
			resSegment.setStatus(segDetail.getStatus());

			resSegmentList.add(resSegment);
		}

		return resSegmentList;
	}

}
