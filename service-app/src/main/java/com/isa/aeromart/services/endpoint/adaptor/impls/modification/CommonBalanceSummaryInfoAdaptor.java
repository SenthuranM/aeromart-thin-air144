package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.modification.ChargeBreakdownTemplate;
import com.isa.aeromart.services.endpoint.dto.modification.ChargeSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.CommonSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.StateSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class CommonBalanceSummaryInfoAdaptor implements Adaptor<ReservationBalanceTO, CommonSummaryInfo> {

	private ReservationInfo resInfo;
	private BigDecimal preModifiedSurchargetotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal postModifiedSurchargetotal = AccelAeroCalculator.getDefaultBigDecimalZero();

	//private ReservationExternalChargeInfo extCharge;

	//private BigDecimal ccFee;

	public CommonBalanceSummaryInfoAdaptor(ReservationInfo resInfo) {
		this.resInfo = resInfo;
		//this.extCharge = extCharge;
		//this.ccFee = ccFee;
	}

	@Override
	public CommonSummaryInfo adapt(ReservationBalanceTO source) {

		CommonSummaryInfo balanceSummaryInfo = new CommonSummaryInfo();

		balanceSummaryInfo.setAmountDue(source.getTotalAmountDue());
		balanceSummaryInfo.setCreditAmount(source.getTotalCreditAmount());
		balanceSummaryInfo.setFareAmount(getFareAmountWithFlexi(source.getSegmentSummary()));
		balanceSummaryInfo.setNonRefundableAmount(source.getSegmentSummary().getCurrentNonRefunds());
		balanceSummaryInfo.setPaidAmount(source.getTotalPaidAmount());
		balanceSummaryInfo.setRefundableAmount(source.getSegmentSummary().getCurrentRefunds());
		balanceSummaryInfo.setChargeInfo(getReservationChargeInfo(source, false));
		balanceSummaryInfo.setPostModifiedSummary(getPosetModifySummary(source));
		balanceSummaryInfo.setPreModifiedSummary(getPreModifiedSummary(source));

		Object[] chargeBreakDownListArray = getPrePostChargeBreakDownList(source);

		List<ChargeBreakdownTemplate> premodifiedChgBreakdown = (List<ChargeBreakdownTemplate>) chargeBreakDownListArray[0];
		List<ChargeBreakdownTemplate> postmodifiedChgBreakdown = (List<ChargeBreakdownTemplate>) chargeBreakDownListArray[1];

		if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
			balanceSummaryInfo.getPreModifiedSummary().setSurchargeAmount(
					AccelAeroCalculator.add(balanceSummaryInfo.getPreModifiedSummary().getSurchargeAmount(),
							preModifiedSurchargetotal));
			balanceSummaryInfo.getPostModifiedSummary().setSurchargeAmount(
					AccelAeroCalculator.add(balanceSummaryInfo.getPostModifiedSummary().getSurchargeAmount(),
							postModifiedSurchargetotal));
		}
		
		balanceSummaryInfo.getPreModifiedSummary().getChargeBreakDown().addAll(premodifiedChgBreakdown);
		balanceSummaryInfo.getPostModifiedSummary().getChargeBreakDown().addAll(postmodifiedChgBreakdown);
		return balanceSummaryInfo;
	}

	private Object[] getPrePostChargeBreakDownList(ReservationBalanceTO source) {
		List<ChargeBreakdownTemplate> preModifiedBreakDown = new ArrayList<ChargeBreakdownTemplate>();
		List<ChargeBreakdownTemplate> postModifiedBreakDown = new ArrayList<ChargeBreakdownTemplate>();

		LCCClientSegmentSummaryTO lCCClientSegmentSummaryTO = source.getSegmentSummary();
		// Add Ancillary
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
		if (resInfo.getPaxList() != null) {
			for (ReservationPaxTO pax : resInfo.getPaxList()) {
				extChgList.addAll(pax.getExternalCharges());
			}
		}

		BigDecimal seatChg = BigDecimal.ZERO;
		BigDecimal mealChg = BigDecimal.ZERO;
		BigDecimal baggageChg = BigDecimal.ZERO;
		BigDecimal ssrChg = BigDecimal.ZERO;
		BigDecimal airportServiceChg = BigDecimal.ZERO;
		BigDecimal airportTransferChg = BigDecimal.ZERO;
		BigDecimal insuranceChg = BigDecimal.ZERO;
		BigDecimal autoCheckinChg = BigDecimal.ZERO;
		BigDecimal totalAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (LCCClientExternalChgDTO chgDTO : extChgList) {
			if (chgDTO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
				if (!chgDTO.isAmountConsumedForPayment()) {
					totalAncillaryCharges = AccelAeroCalculator.add(totalAncillaryCharges, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.SEAT_MAP) {
					seatChg = AccelAeroCalculator.add(seatChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.MEAL) {
					mealChg = AccelAeroCalculator.add(mealChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.BAGGAGE) {
					baggageChg = AccelAeroCalculator.add(baggageChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
					ssrChg = AccelAeroCalculator.add(ssrChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
					airportServiceChg = AccelAeroCalculator.add(airportServiceChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_TRANSFER) {
					airportTransferChg = AccelAeroCalculator.add(airportTransferChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INSURANCE) {
					insuranceChg = AccelAeroCalculator.add(insuranceChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AUTOMATIC_CHECKIN) {
					autoCheckinChg = AccelAeroCalculator.add(autoCheckinChg, chgDTO.getAmount());
				}
			}
		}

		if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
			if (seatChg.compareTo(BigDecimal.ZERO) != 0
					|| lCCClientSegmentSummaryTO.getCurrentSeatAmount().compareTo(BigDecimal.ZERO) != 0) {
				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.SEAT_MAP.toString(),
						lCCClientSegmentSummaryTO.getCurrentSeatAmount());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.SEAT_MAP.toString(),
						seatChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);
			}

			if (mealChg.compareTo(BigDecimal.ZERO) != 0
					|| lCCClientSegmentSummaryTO.getCurrentMealAmount().compareTo(BigDecimal.ZERO) != 0) {

				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.MEAL.toString(),
						lCCClientSegmentSummaryTO.getCurrentMealAmount());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.MEAL.toString(), mealChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);

			}

			if (baggageChg.compareTo(BigDecimal.ZERO) != 0
					|| lCCClientSegmentSummaryTO.getCurrentBaggageAmount().compareTo(BigDecimal.ZERO) != 0) {

				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.BAGGAGE.toString(),
						lCCClientSegmentSummaryTO.getCurrentBaggageAmount());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.BAGGAGE.toString(),
						baggageChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);

			}

			if (ssrChg.compareTo(BigDecimal.ZERO) != 0
					|| lCCClientSegmentSummaryTO.getCurrentSSRAmount().compareTo(BigDecimal.ZERO) != 0) {

				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString(), lCCClientSegmentSummaryTO.getCurrentSSRAmount());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString(), baggageChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);

			}

			if (airportServiceChg.compareTo(BigDecimal.ZERO) > 0) {

				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.AIRPORT_SERVICE.toString(), AccelAeroCalculator.getDefaultBigDecimalZero());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.AIRPORT_SERVICE.toString(), airportServiceChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);

			}

			if (airportTransferChg.compareTo(BigDecimal.ZERO) > 0) {

				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString(), AccelAeroCalculator.getDefaultBigDecimalZero());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString(), airportTransferChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);

			}

			if (insuranceChg.compareTo(BigDecimal.ZERO) != 0
					|| lCCClientSegmentSummaryTO.getCurrentInsuranceAmount().compareTo(BigDecimal.ZERO) != 0) {

				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.INSURANCE.toString(),
						lCCClientSegmentSummaryTO.getCurrentInsuranceAmount());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true, EXTERNAL_CHARGES.INSURANCE.toString(),
						insuranceChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);

			}
			
			if (autoCheckinChg.compareTo(BigDecimal.ZERO) > 0) {

				ChargeBreakdownTemplate preUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString(), AccelAeroCalculator.getDefaultBigDecimalZero());
				ChargeBreakdownTemplate postUpdated = getChargeBreakdownTemplate(true,
						EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString(), autoCheckinChg);

				preModifiedBreakDown.add(preUpdated);
				postModifiedBreakDown.add(postUpdated);

			}

			this.preModifiedSurchargetotal = AccelAeroCalculator.add(lCCClientSegmentSummaryTO.getCurrentSeatAmount(),
					lCCClientSegmentSummaryTO.getCurrentMealAmount(), lCCClientSegmentSummaryTO.getCurrentSSRAmount(),
					lCCClientSegmentSummaryTO.getCurrentBaggageAmount(), lCCClientSegmentSummaryTO.getCurrentAutoCheckinAmount());
			this.postModifiedSurchargetotal = AccelAeroCalculator.add(seatChg, mealChg, baggageChg, ssrChg, airportServiceChg,
					airportTransferChg, autoCheckinChg);
		} 

		Object[] breakdownlistArray = { preModifiedBreakDown, postModifiedBreakDown };

		return breakdownlistArray;
	}

	private ChargeBreakdownTemplate getChargeBreakdownTemplate(boolean isAnci, String chrgeType, BigDecimal amount) {

		ChargeBreakdownTemplate chargeTemplate = new ChargeBreakdownTemplate();
		chargeTemplate.setAnci(isAnci);
		chargeTemplate.setChargeAmount(amount);
		chargeTemplate.setChargeType(chrgeType);

		return chargeTemplate;
	}

	private StateSummaryInfo getPreModifiedSummary(ReservationBalanceTO source) {

		StateSummaryInfo preModifiedSummary = new StateSummaryInfo();

		preModifiedSummary.setAdjAmount(source.getSegmentSummary().getCurrentAdjAmount());
		preModifiedSummary.setDiscount(source.getSegmentSummary().getCurrentDiscount());
		preModifiedSummary.setFareAmount(source.getSegmentSummary().getCurrentFareAmount());
		preModifiedSummary.setSurchargeAmount(AccelAeroCalculator.add(source.getSegmentSummary().getCurrentSurchargeAmount(),
				source.getSegmentSummary().getCurrentAdjAmount()));
		preModifiedSummary.setTaxAmount(source.getSegmentSummary().getCurrentTaxAmount());
		preModifiedSummary.setTotalPrice(source.getSegmentSummary().getCurrentTotalPrice());
		preModifiedSummary.setChargeInfo(getReservationChargeInfo(source, true));
		// preModifiedSummary.setChargeBrakDown();

		return preModifiedSummary;
	}

	private StateSummaryInfo getPosetModifySummary(ReservationBalanceTO source) {

		StateSummaryInfo postModifiedSummary = new StateSummaryInfo();

		postModifiedSummary.setAdjAmount(source.getSegmentSummary().getNewAdjAmount());
		postModifiedSummary.setDiscount(source.getSegmentSummary().getNewDiscount());
		postModifiedSummary.setFareAmount(getFareAmountWithFlexi(source.getSegmentSummary()));
		postModifiedSummary.setSurchargeAmount(AccelAeroCalculator.add(source.getSegmentSummary().getNewSurchargeAmount(), source
				.getSegmentSummary().getNewExtraFeeAmount()));
		postModifiedSummary.setTaxAmount(source.getSegmentSummary().getNewTaxAmount());
		postModifiedSummary.setTotalPrice(source.getTotalPrice());
		postModifiedSummary.setChargeInfo(getReservationChargeInfo(source, false));
		// postModifiedSummary.setChargeBrakDown();

		return postModifiedSummary;
	}

	private ChargeSummaryInfo getReservationChargeInfo(ReservationBalanceTO source, boolean isPreModifiedValues) {

		ChargeSummaryInfo chargeInfo = new ChargeSummaryInfo();

		if (isPreModifiedValues) {

			chargeInfo.setCancellationCharge(source.getSegmentSummary().getCurrentCnxAmount());
			chargeInfo.setModificationCharge(source.getSegmentSummary().getCurrentModAmount());
			chargeInfo.setPenaltyCharge(source.getSegmentSummary().getCurrentModificationPenatly());

		} else {
			chargeInfo.setCancellationCharge(source.getTotalCnxCharge());
			chargeInfo.setModificationCharge(source.getTotalModCharge());
			chargeInfo.setPenaltyCharge(source.getSegmentSummary().getModificationPenalty());
		}

		return chargeInfo;
	}

	private BigDecimal getFareAmountWithFlexi(LCCClientSegmentSummaryTO lCCClientSegmentSummaryTO) {
		BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (lCCClientSegmentSummaryTO.getOutBoundExternalCharge().compareTo(BigDecimal.ZERO) > 0) {
			flexiCharge = lCCClientSegmentSummaryTO.getOutBoundExternalCharge();
		} else if (lCCClientSegmentSummaryTO.getInBoundExternalCharge().compareTo(BigDecimal.ZERO) > 0) {
			flexiCharge = lCCClientSegmentSummaryTO.getInBoundExternalCharge();
		}
		BigDecimal newFareWithFlexi = AccelAeroCalculator.add(lCCClientSegmentSummaryTO.getNewFareAmount(), flexiCharge);
		return newFareWithFlexi;
	}
}
