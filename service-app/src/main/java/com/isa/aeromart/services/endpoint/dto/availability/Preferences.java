package com.isa.aeromart.services.endpoint.dto.availability;

import com.isa.aeromart.services.endpoint.dto.common.Promotion;

public class Preferences extends BasePreferences {

	private String currency;

	private Promotion promotion;

	private String originCountryCode;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Promotion getPromotion() {
		if (promotion == null) {
			promotion = new Promotion();
		}
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}

}
