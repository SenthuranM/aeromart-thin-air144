package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemComponent;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class OneToManyModification extends DefaultAncillarySelectionStrategy {

	@Override
	public PricedAncillaryType getDefaultPricedAncillaryType(PricedAncillaryType prevPricedAncillaryType,
			AncillaryType availableAncillaryType, Transition<List<Scope>> transition) {

		Map<Scope, AncillaryScope> anciScopeMap = new HashMap<>();
		PricedAncillaryType pricedAncillaryType = new PricedAncillaryType();		
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		AncillaryScope ancillaryScope;
		PricedSelectedItem defaultItem;

//		List<Transition<Scope>> sortedScope = prepareScopes(transition);
		AncillariesConstants.AncillaryType ancillaryType = AncillariesConstants.AncillaryType
				.resolveAncillaryType(availableAncillaryType.getAncillaryType());

		Scope prevScope;

		List<PricedSelectedItem> prevItems;
		AncillaryItemComponent availableItems;
		AncillaryItem equivalentItem;

		prevScope = transition.getOld().get(0);
		prevItems = getPreviousAncillaryItems(prevPricedAncillaryType, prevScope);	

		for (PricedSelectedItem prevItem : prevItems) {
			for (Scope newScope : transition.getNew()) {
				availableItems = getAvailableAncillaryItems(availableAncillaryType, newScope);
				equivalentItem = getEquivalentItem(ancillaryType, prevItem, availableItems);
				if (equivalentItem != null) {
					
					if(!anciScopeMap.containsKey(newScope)){
						ancillaryScope = new AncillaryScope();
						ancillaryScope.setScope(newScope);
						ancillaryScope.setAncillaries(new ArrayList<>());
						anciScopeMap.put(newScope, ancillaryScope);
					}
					ancillaryScope = anciScopeMap.get(newScope);
					defaultItem = new PricedSelectedItem();
					defaultItem.setId(equivalentItem.getItemId());
					defaultItem.setName((equivalentItem.getItemName() != null) ? equivalentItem.getItemName() : prevItem.getName());
					defaultItem.setQuantity(prevItem.getQuantity()); // TODO -- inventory validation
					defaultItem.setMealCategoryCode(prevItem.getMealCategoryCode());
					// TODO -- get relevant charge
					if (!equivalentItem.getCharges().isEmpty()) {
						defaultItem.setAmount(equivalentItem.getCharges().iterator().next().getAmount());
					} else {
						defaultItem.setAmount(new BigDecimal(0));
					}

					ancillaryScope.getAncillaries().add(defaultItem);
				}
			}
		}

		if(anciScopeMap != null && !anciScopeMap.isEmpty()){
			for(AncillaryScope ancillaryScopeRef : anciScopeMap.values()){
				pricedAncillaryType.getAncillaryScopes().add(ancillaryScopeRef);
			}
		}
		return pricedAncillaryType;
	}

}
