package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;

public class OndOWPricing {

	// Ond sequence number
	private int ondSequence;

	// ond wise total price
	@CurrencyValue
	private BigDecimal totalPrice;

	// ond wise total surcharge
	@CurrencyValue
	private BigDecimal totalSurcharges = BigDecimal.ZERO;

	// Ond wise total taxes
	@CurrencyValue
	private BigDecimal totalTaxes = BigDecimal.ZERO;

	// pax wise price details for given OND
	private List<PaxPrice> paxWise;

	public OndOWPricing() {
		totalPrice = BigDecimal.ZERO;
		totalSurcharges = BigDecimal.ZERO;
		totalTaxes = BigDecimal.ZERO;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getTotalSurcharges() {
		return totalSurcharges;
	}

	public void setTotalSurcharges(BigDecimal totalSurcharges) {
		this.totalSurcharges = totalSurcharges;
	}

	public List<PaxPrice> getPaxWise() {
		if (paxWise == null) {
			paxWise = new ArrayList<PaxPrice>();
		}
		return paxWise;
	}

	public BigDecimal getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(BigDecimal totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	public void setPaxWise(List<PaxPrice> paxWise) {
		this.paxWise = paxWise;
	}

}
