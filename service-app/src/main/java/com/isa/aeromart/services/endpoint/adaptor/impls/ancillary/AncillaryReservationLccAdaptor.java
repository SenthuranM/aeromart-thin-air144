package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciIntegration;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationAncillarySelection;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AutoCheckinInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.InFlightSsrInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class AncillaryReservationLccAdaptor extends TransactionAwareAdaptor<LCCClientReservation, ReservationAncillarySelection> {

	private Map<String, OndUnit> reservationOndUnitsMap;
	private List<OndUnit> ondUnits;

	private AncillaryReservation ancillaryReservation;
	private List<AncillaryPassenger> ancillaryPassengers;
	private AncillaryPassenger ancillaryPassenger;
	private List<PricedAncillaryType> pricedAncillaryTypes;
	private PricedAncillaryType pricedAncillaryType;

	public ReservationAncillarySelection adapt(LCCClientReservation source) {

		ReservationAncillarySelection transformedResponse = new ReservationAncillarySelection();

		reservationOndUnitsMap = new HashMap<>();
		ondUnits = new ArrayList<>();

		ancillaryReservation = new AncillaryReservation();
		ancillaryReservation.setAncillaryTypes(new ArrayList<>());
		ancillaryPassengers = new ArrayList<>();

		for (LCCClientReservationPax pax : source.getPassengers()) {
			if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
				processAncillaryPassenger(pax, source.getSegments());
			}
		}

		pricedAncillaryTypes = new ArrayList<>();
		processAncillaryInsurance(source.getReservationInsurances());
		ancillaryReservation.getAncillaryTypes().addAll(pricedAncillaryTypes);
		ancillaryReservation.setAmount(AnciIntegration.getAncillarySelectionAmount(pricedAncillaryTypes));

		ancillaryReservation.setPassengers(ancillaryPassengers);
		transformedResponse.setAncillaryReservation(ancillaryReservation);

		for (OndUnit o : reservationOndUnitsMap.values()) {
			ondUnits.add(o);
		}

		MetaData metaData = new MetaData();
		metaData.setOndPreferences(ondUnits);
		transformedResponse.setMetaData(metaData);

		return transformedResponse;
	}

	private void processAncillaryPassenger(LCCClientReservationPax pax, Set<LCCClientReservationSegment> segments) {
		ancillaryPassenger = new AncillaryPassenger();
		ancillaryPassenger.setPassengerRph(AnciCommon.toPassengerRph(pax));

		List<LCCSelectedSegmentAncillaryDTO> anciSegments = pax.getSelectedAncillaries();

		pricedAncillaryTypes = new ArrayList<>();

		processAncillaryBaggage(anciSegments, segments);
		processAncillarySeat(anciSegments);
		processAncillaryMeal(anciSegments);
		processAncillarySsrInFlight(anciSegments);
		processAncillarySsrAirport(anciSegments);
		processAncillaryFlexibility(anciSegments);
		processAncillaryAirportTransfer(anciSegments);
		processAncillaryAutoCheckin(anciSegments);

		ancillaryPassenger.setAncillaryTypes(pricedAncillaryTypes);
		ancillaryPassenger.setAmount(AnciIntegration.getAncillarySelectionAmount(pricedAncillaryTypes));
		ancillaryPassengers.add(ancillaryPassenger);
	}

	// TODO -- move following methods to anci-type wise adaptors

	private void processAncillaryBaggage(List<LCCSelectedSegmentAncillaryDTO> anciSegments, Set<LCCClientReservationSegment> segments) {

		AncillariesConstants.AncillaryType ancillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		OndScope ondScope;

		FlightSegmentTO flightSegmentTO = null;
		String ondId, fltSegRefNo;

		Map<String, OndUnit> ondUnitsMap = new HashMap<>();
		OndUnit ondUnit;

		pricedAncillaryType = new PricedAncillaryType();
		ancillaryType = AncillariesConstants.AncillaryType.BAGGAGE;
		pricedAncillaryType.setAncillaryType(ancillaryType.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());

		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {

			if (anciSegment.getBaggageDTOs() != null && anciSegment.getBaggageDTOs().size() > 0) {
				ondId = null;
				if (anciSegment.getBaggageDTOs().get(0).getOndBaggageGroupId() != null) {
					ondId = anciSegment.getBaggageDTOs().get(0).getOndBaggageGroupId();
				} else {
					ondId = AnciCommon.resolveResSegmentBaggageOndId(segments, anciSegment.getFlightSegmentTO()
							.getFlightRefNumber());
				}
				
				if (ondId != null) {
					if (!ondUnitsMap.containsKey(ondId)) {
						ondUnit = new OndUnit();
						ondUnit.setOndId(ondId);
						ondUnit.setFlightSegmentRPH(new ArrayList<>());

						ancillaryScope = new AncillaryScope();
						ancillaryScope.setAncillaries(new ArrayList<>());

						ondScope = new OndScope();
						ondScope.setOndId(ondId);
						ancillaryScope.setScope(ondScope);

						for (LCCBaggageDTO baggage : anciSegment.getBaggageDTOs()) {
							pricedSelectedItem = new PricedSelectedItem();
							pricedSelectedItem.setId(AnciCommon.getBaggageId(baggage.getOndBaggageChargeId(),
									baggage.getBaggageName()));
							pricedSelectedItem.setName(baggage.getBaggageName());
							pricedSelectedItem.setAmount(baggage.getBaggageCharge());
							pricedSelectedItem.setQuantity(1);
							ancillaryScope.getAncillaries().add(pricedSelectedItem);
						}

						pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);

						ondUnitsMap.put(ondId, ondUnit);
					} else {
						for (AncillaryScope anciScope : pricedAncillaryType.getAncillaryScopes()) {
							if (Scope.ScopeType.OND.equals(anciScope.getScope().getScopeType())
									&& ondId.equals(((OndScope) anciScope.getScope()).getOndId())) {
								for (PricedSelectedItem selectedItem : anciScope.getAncillaries()) {
									selectedItem.setQuantity(selectedItem.getQuantity() + 1);
								}
							}
						}
					}

					ondUnit = ondUnitsMap.get(ondId);
					ondUnit.getFlightSegmentRPH().add(rphGenerator.getRPH(anciSegment.getFlightSegmentTO().getFlightRefNumber()));
				}
			}

		}
		if (!pricedAncillaryType.getAncillaryScopes().isEmpty()) {
			pricedAncillaryTypes.add(pricedAncillaryType);
		}

		for (String ondKey : ondUnitsMap.keySet()) {
			if (!reservationOndUnitsMap.containsKey(ondKey)) {
				reservationOndUnitsMap.put(ondKey, ondUnitsMap.get(ondKey));
			}
		}

	}

	private void processAncillarySeat(List<LCCSelectedSegmentAncillaryDTO> anciSegments) {

		AncillariesConstants.AncillaryType ancillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		SegmentScope segmentScope;

		FlightSegmentTO flightSegmentTO;
		LCCAirSeatDTO airSeatDTO;

		pricedAncillaryType = new PricedAncillaryType();
		ancillaryType = AncillariesConstants.AncillaryType.SEAT;
		pricedAncillaryType.setAncillaryType(ancillaryType.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());

		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {

			flightSegmentTO = anciSegment.getFlightSegmentTO();

			ancillaryScope = new AncillaryScope();
			segmentScope = new SegmentScope();
			segmentScope.setScopeType(Scope.ScopeType.SEGMENT);
			segmentScope.setFlightSegmentRPH(rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber()));
			ancillaryScope.setScope(segmentScope);
			ancillaryScope.setAncillaries(new ArrayList<>());

			if (anciSegment.getAirSeatDTO() != null && anciSegment.getAirSeatDTO().getSeatNumber() != null
					&& anciSegment.getAirSeatDTO().getSeatCharge() != null) {
				airSeatDTO = anciSegment.getAirSeatDTO();

				pricedSelectedItem = new PricedSelectedItem();
				pricedSelectedItem.setId(airSeatDTO.getSeatNumber());
				pricedSelectedItem.setAmount(airSeatDTO.getSeatCharge());
				pricedSelectedItem.setQuantity(1);
				ancillaryScope.getAncillaries().add(pricedSelectedItem);
			}

			if (anciSegment.getExtraSeatDTOs() != null && anciSegment.getExtraSeatDTOs().size() > 0) {
				for (LCCAirSeatDTO seat : anciSegment.getExtraSeatDTOs()) {
					pricedSelectedItem = new PricedSelectedItem();
					pricedSelectedItem.setId(seat.getSeatNumber());
					pricedSelectedItem.setAmount(seat.getSeatCharge());
					pricedSelectedItem.setQuantity(1);
					ancillaryScope.getAncillaries().add(pricedSelectedItem);
				}
			}
			if (!ancillaryScope.getAncillaries().isEmpty()) {
				pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
			}
		}

		if (!pricedAncillaryType.getAncillaryScopes().isEmpty()) {
			pricedAncillaryTypes.add(pricedAncillaryType);
		}
	}

	/**
	 * This method is used to populate autocheckin details from given segment
	 * 
	 * @param anciSegments
	 */
	private void processAncillaryAutoCheckin(List<LCCSelectedSegmentAncillaryDTO> anciSegments) {
		AncillariesConstants.AncillaryType ancillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		SegmentScope segmentScope;
		FlightSegmentTO flightSegmentTO;
		pricedAncillaryType = new PricedAncillaryType();
		ancillaryType = AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN;
		pricedAncillaryType.setAncillaryType(ancillaryType.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());
		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {
			flightSegmentTO = anciSegment.getFlightSegmentTO();
			if (anciSegment.getAutomaticCheckinDTOs() != null && anciSegment.getAutomaticCheckinDTOs().size() > 0) {
				ancillaryScope = new AncillaryScope();
				segmentScope = new SegmentScope();
				segmentScope.setScopeType(Scope.ScopeType.SEGMENT);
				segmentScope.setFlightSegmentRPH(rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber()));
				ancillaryScope.setScope(segmentScope);
				ancillaryScope.setAncillaries(new ArrayList<>());
				for (LCCAutomaticCheckinDTO autoCheckin : anciSegment.getAutomaticCheckinDTOs()) {
					pricedSelectedItem = new PricedSelectedItem();
					pricedSelectedItem.setId(autoCheckin.getAutoCheckinId().toString());
					pricedSelectedItem.setAmount(autoCheckin.getAutomaticCheckinCharge());
					pricedSelectedItem.setName(autoCheckin.getSeatPref());
					pricedSelectedItem.setQuantity(1);
					AutoCheckinInput autoCheckinInput = new AutoCheckinInput();
					autoCheckinInput.setEmail(autoCheckin.getEmail());
					autoCheckinInput.setSeatCode(autoCheckin.getSeatCode());
					pricedSelectedItem.setInput(autoCheckinInput);
					ancillaryScope.getAncillaries().add(pricedSelectedItem);
				}
				if (!ancillaryScope.getAncillaries().isEmpty()) {
					pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
				}
			}
		}
		if (!pricedAncillaryType.getAncillaryScopes().isEmpty()) {
			pricedAncillaryTypes.add(pricedAncillaryType);
		}
	}

	private void processAncillaryMeal(List<LCCSelectedSegmentAncillaryDTO> anciSegments) {

		AncillariesConstants.AncillaryType ancillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		SegmentScope segmentScope;

		FlightSegmentTO flightSegmentTO;

		pricedAncillaryType = new PricedAncillaryType();
		ancillaryType = AncillariesConstants.AncillaryType.MEAL;
		pricedAncillaryType.setAncillaryType(ancillaryType.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());

		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {

			flightSegmentTO = anciSegment.getFlightSegmentTO();

			if (anciSegment.getMealDTOs() != null && anciSegment.getMealDTOs().size() > 0) {

				ancillaryScope = new AncillaryScope();
				segmentScope = new SegmentScope();
				segmentScope.setScopeType(Scope.ScopeType.SEGMENT);
				segmentScope.setFlightSegmentRPH(rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber()));
				ancillaryScope.setScope(segmentScope);
				ancillaryScope.setAncillaries(new ArrayList<>());

				for (LCCMealDTO meal : anciSegment.getMealDTOs()) {
					pricedSelectedItem = new PricedSelectedItem();
					pricedSelectedItem.setId(meal.getMealCode());
					pricedSelectedItem.setName(meal.getMealName());
					pricedSelectedItem.setAmount(meal.getMealCharge());
					pricedSelectedItem.setQuantity(meal.getSoldMeals());
					pricedSelectedItem.setMealCategoryCode(meal.getMealCategoryCode());
					ancillaryScope.getAncillaries().add(pricedSelectedItem);
				}

				pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
			}
		}

		if (!pricedAncillaryType.getAncillaryScopes().isEmpty()) {
			pricedAncillaryTypes.add(pricedAncillaryType);
		}
	}

	private void processAncillarySsrInFlight(List<LCCSelectedSegmentAncillaryDTO> anciSegments) {
		AncillariesConstants.AncillaryType ancillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		SegmentScope segmentScope;

		FlightSegmentTO flightSegmentTO;

		pricedAncillaryType = new PricedAncillaryType();
		ancillaryType = AncillariesConstants.AncillaryType.SSR_IN_FLIGHT;
		pricedAncillaryType.setAncillaryType(ancillaryType.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());

		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {

			flightSegmentTO = anciSegment.getFlightSegmentTO();

			if (anciSegment.getSpecialServiceRequestDTOs() != null && anciSegment.getSpecialServiceRequestDTOs().size() > 0) {

				ancillaryScope = new AncillaryScope();
				segmentScope = new SegmentScope();
				segmentScope.setScopeType(Scope.ScopeType.SEGMENT);
				segmentScope.setFlightSegmentRPH(rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber()));
				ancillaryScope.setScope(segmentScope);
				ancillaryScope.setAncillaries(new ArrayList<>());

				for (LCCSpecialServiceRequestDTO inFlightService : anciSegment.getSpecialServiceRequestDTOs()) {
					pricedSelectedItem = new PricedSelectedItem();
					pricedSelectedItem.setId(inFlightService.getSsrCode());
					pricedSelectedItem.setName(inFlightService.getSsrName());
					pricedSelectedItem.setAmount(inFlightService.getCharge());
					pricedSelectedItem.setQuantity(1);
					InFlightSsrInput input = new InFlightSsrInput();
					input.setComment(inFlightService.getText());
					pricedSelectedItem.setInput(input);
					ancillaryScope.getAncillaries().add(pricedSelectedItem);
				}

				pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);
			}
		}

		if (!pricedAncillaryType.getAncillaryScopes().isEmpty()) {
			pricedAncillaryTypes.add(pricedAncillaryType);
		}
	}

	private void processAncillarySsrAirport(List<LCCSelectedSegmentAncillaryDTO> anciSegments) {
		AncillariesConstants.AncillaryType ancillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		AirportScope airportScope;

		FlightSegmentTO flightSegmentTO;

		pricedAncillaryType = new PricedAncillaryType();
		ancillaryType = AncillariesConstants.AncillaryType.SSR_AIRPORT;
		pricedAncillaryType.setAncillaryType(ancillaryType.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		Map<String, AncillaryScope> ancillaryScopeMap;

		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(getTransactionId());

		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {

			flightSegmentTO = anciSegment.getFlightSegmentTO();
			ancillaryScopeMap = new HashMap<>();

			if (anciSegment.getAirportServiceDTOs() != null && anciSegment.getAirportServiceDTOs().size() > 0) {

				for (LCCAirportServiceDTO airportServiceDTO : anciSegment.getAirportServiceDTOs()) {

					if (!ancillaryScopeMap.containsKey(airportServiceDTO.getAirportCode())) {
						ancillaryScope = new AncillaryScope();
						airportScope = new AirportScope();
						airportScope.setScopeType(Scope.ScopeType.AIRPORT);
						airportScope.setFlightSegmentRPH(rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber()));
						airportScope.setAirportCode(airportServiceDTO.getAirportCode());
						ancillaryScope.setScope(airportScope);
						ancillaryScope.setAncillaries(new ArrayList<>());
						ancillaryScopeMap.put(airportServiceDTO.getAirportCode(), ancillaryScope);
					}

					ancillaryScope = ancillaryScopeMap.get(airportServiceDTO.getAirportCode());

					pricedSelectedItem = new PricedSelectedItem();
					pricedSelectedItem.setId(airportServiceDTO.getSsrCode());
					pricedSelectedItem.setName(airportServiceDTO.getSsrName());
					pricedSelectedItem.setQuantity(1);
					pricedSelectedItem.setAmount(airportServiceDTO.getServiceCharge());
					ancillaryScope.getAncillaries().add(pricedSelectedItem);
				}

			}
			for (AncillaryScope ancillaryScopeRef : ancillaryScopeMap.values()) {
				pricedAncillaryType.getAncillaryScopes().add(ancillaryScopeRef);
			}
		}

		if (!pricedAncillaryType.getAncillaryScopes().isEmpty()) {
			pricedAncillaryTypes.add(pricedAncillaryType);
		}
	}

	private void processAncillaryInsurance(List<LCCClientReservationInsurance> insurances) {
		AncillariesConstants.AncillaryType ancillaryType;
		AncillaryScope ancillaryScope;
		PricedSelectedItem pricedSelectedItem;
		SegmentScope segmentScope;

		FlightSegmentTO flightSegmentTO;

		pricedAncillaryType = new PricedAncillaryType();
		ancillaryType = AncillariesConstants.AncillaryType.INSURANCE;
		pricedAncillaryType.setAncillaryType(ancillaryType.getCode());
		pricedAncillaryType.setAncillaryScopes(new ArrayList<>());

		Map<String, AncillaryScope> ancillaryScopeMap;

		ancillaryScope = new AncillaryScope();
		Scope scope = new Scope();
		scope.setScopeType(Scope.ScopeType.ALL_SEGMENTS);
		ancillaryScope.setScope(scope);
		ancillaryScope.setAncillaries(new ArrayList<>());

		for (LCCClientReservationInsurance lccInsurance : insurances) {

			pricedSelectedItem = new PricedSelectedItem();
			pricedSelectedItem.setId(lccInsurance.getInsuranceQuoteRefNumber());
			pricedSelectedItem.setAmount(lccInsurance.getQuotedTotalPremium());
			ancillaryScope.getAncillaries().add(pricedSelectedItem);

		}

		pricedAncillaryType.getAncillaryScopes().add(ancillaryScope);

	}

	private void processAncillaryFlexibility(List<LCCSelectedSegmentAncillaryDTO> anciSegments) {
		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {

		}
	}

	private void processAncillaryAirportTransfer(List<LCCSelectedSegmentAncillaryDTO> anciSegments) {
		for (LCCSelectedSegmentAncillaryDTO anciSegment : anciSegments) {

		}
	}

}
