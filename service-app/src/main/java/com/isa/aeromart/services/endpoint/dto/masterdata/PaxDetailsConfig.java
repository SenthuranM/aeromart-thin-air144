package com.isa.aeromart.services.endpoint.dto.masterdata;

public class PaxDetailsConfig {

	private boolean adult;
	
	private boolean child;
	
	private boolean infant;

	public boolean isAdult() {
		return adult;
	}

	public void setAdult(boolean adult) {
		this.adult = adult;
	}

	public boolean isChild() {
		return child;
	}

	public void setChild(boolean child) {
		this.child = child;
	}

	public boolean isInfant() {
		return infant;
	}

	public void setInfant(boolean infant) {
		this.infant = infant;
	}
	
}
