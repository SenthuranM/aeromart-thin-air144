package com.isa.aeromart.services.endpoint.controller.voucher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRS;
import com.isa.aeromart.services.endpoint.dto.session.store.GiftVoucherSessionStore;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.adaptor.utils.PaybyVoucherInfoAdapter;
import com.isa.aeromart.services.endpoint.adaptor.utils.VoucherDTOAdapter;
import com.isa.aeromart.services.endpoint.controller.availability.AvailabilityController;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.delegate.payment.PaymentService;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherOption;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.aeromart.services.endpoint.dto.voucher.GiftVoucherDTO;
import com.isa.aeromart.services.endpoint.dto.voucher.IssueVoucherRQ;
import com.isa.aeromart.services.endpoint.dto.voucher.IssueVoucherRS;
import com.isa.aeromart.services.endpoint.dto.voucher.VoucherRedeemRQ;
import com.isa.aeromart.services.endpoint.dto.voucher.VoucherRedeemRS;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.service.VoucherTemplateBD;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;

@Controller
@RequestMapping(value = "voucher", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
		MediaType.APPLICATION_JSON_VALUE })
public class VoucherController extends StatefulController {

	private static Log log = LogFactory.getLog(AvailabilityController.class);

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private MessageSource errorMessageSource;

	@ResponseBody
	@RequestMapping(value = "/getVouchers")
	public List<VoucherTemplateTo> getVoucherTemplates() throws ModuleException {
		List<VoucherTemplateTo> voucherList = null;
		try {

			VoucherTemplateBD voucherDelegate = ModuleServiceLocator.getVoucherTemplateBD();
			voucherList = voucherDelegate.getVoucherInSalesPeriod();
			paymentService.setCreditCardFeesToVoucherTemplates(voucherList);

		} catch (ModuleException me) {
			log.error("VoucherController ==>  failed", me);

		}
		return voucherList;
	}

	@ResponseBody
	@RequestMapping(value = "/getVouchersByGroupId")
	public IssueVoucherRS getVouchersByGroupId(@RequestBody Map<String, String> params) throws ModuleException {
		IssueVoucherRS issueVoucherRS = new IssueVoucherRS();
		try {
			VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
			issueVoucherRS.setVoucherDTOList(voucherDelegate.getVouchersFromGroupId(params.get("groupId")));

		} catch (ModuleException me) {
			log.error("VoucherController ==>  failed", me);
		}
		GiftVoucherSessionStore giftVoucherSessionStore = paymentService.getGiftVoucherStore(params.get("voucher_transactionId"));
		VoucherPaymentDTO voucherPaymentDTO = new VoucherPaymentDTO();
		voucherPaymentDTO.setCardHolderName(giftVoucherSessionStore.getCardHolderName());
		issueVoucherRS.getVoucherDTOList().get(0).setVoucherPaymentDTO(voucherPaymentDTO);
		return issueVoucherRS;
	}

	@ResponseBody
	@RequestMapping(value = "/issueVouchers")
	public MakePaymentRS issueVoucherForIBE(@RequestBody IssueVoucherRQ voucherRQ)
			throws ModuleException {
		String transactionId = initTransaction(GiftVoucherSessionStore.SESSION_KEY, voucherRQ.getTransactionId());
		MakePaymentRS voucherRS = new MakePaymentRS();
		voucherRQ.setTransactionId(transactionId);
		List<String> messages = new ArrayList<>();

		if (AppSysParamsUtil.isVoucherEnabled()) {
			try {
				voucherRS = paymentService.processGiftVoucherPayment(getTrackInfo(), voucherRQ, getClientCommonInfoDTO());

			} catch (ModuleException me) {
				log.error("IssueGiftVoucherAction ==> execute() generating voucher failed", me);
				if (me.getExceptionCode().equals("promotions.voucher.amount.local.null")) {
					//voucherRS.getVoucherDTOList().get(0).setRemarks("Voucher notIssued");
					voucherRS.setSuccess(false);
					messages.add(errorMessageSource
							.getMessage("gift.voucher.notissued.payment.failed", null, LocaleContextHolder.getLocale()));
				}
				if (me.getExceptionCode().equals("airreservations.arg.cc.error")) {
					voucherRS.setSuccess(false);
					messages.add(errorMessageSource.getMessage("gift.voucher.notissued", null, LocaleContextHolder.getLocale()));
				}
			}
		}
		voucherRS.setTransactionId(transactionId);
		voucherRS.setMessages(messages);
		return voucherRS;
	}

	@ResponseBody
	@RequestMapping(value = "/emailVouchers")
	public IssueVoucherRS sendVoucherEmail(@RequestBody IssueVoucherRQ voucherRQ) throws ModuleException {
		List<GiftVoucherDTO> giftVoucherList = voucherRQ.getVoucherDTOList();
		List<String> messages = new ArrayList<String>();
		IssueVoucherRS voucherRS = new IssueVoucherRS();
		if (AppSysParamsUtil.isVoucherEnabled()) {
			try {
				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				List<VoucherDTO> voucherList = new ArrayList<VoucherDTO>();
				AdaptorUtils.adaptCollection(giftVoucherList, voucherList, new VoucherDTOAdapter());

				for (VoucherDTO vdto : voucherList) {
					if (vdto.getEmail() == null || vdto.getEmail().isEmpty()) {
						throw new Exception("email.required");
					} else {
						VoucherPaymentDTO voucherPaymentDTO = new VoucherPaymentDTO();
						voucherPaymentDTO.setCardHolderName(voucherRQ.getCardHolderName());
						vdto.setVoucherPaymentDTO(voucherPaymentDTO);
						voucherDelegate.emailVoucher(vdto);
					}
				}
				messages.add(
						errorMessageSource.getMessage("gift.voucher.email.sent.success", null, LocaleContextHolder.getLocale()));
				voucherRS.setSuccess(true);
				voucherRS.setMessages(messages);
			} catch (ModuleException me) {
				messages.add(
						errorMessageSource.getMessage("gift.voucher.email.sent.failed", null, LocaleContextHolder.getLocale()));
				voucherRS.setSuccess(false);
				voucherRS.setMessages(messages);
				log.error("Email Gift Voucher Error");
			} catch (Exception e) {
				messages.add(
						errorMessageSource.getMessage("gift.voucher.email.sent.failed", null, LocaleContextHolder.getLocale()));
				voucherRS.setSuccess(false);
				voucherRS.setMessages(messages);
				log.error("Email address not found", e);
			}
		}
		return voucherRS;
	}

	@ResponseBody
	@RequestMapping(value = "/printVouchers")
	public String printVoucher(@RequestBody IssueVoucherRQ voucherRQ) throws ModuleException {
		List<GiftVoucherDTO> giftVoucherList = voucherRQ.getVoucherDTOList();
		String printHtmlContent = "";
		if (AppSysParamsUtil.isVoucherEnabled()) {
			List<VoucherDTO> voucherList = new ArrayList<VoucherDTO>();
			AdaptorUtils.adaptCollection(giftVoucherList, voucherList, new VoucherDTOAdapter());
			for (VoucherDTO vdto : voucherList) {
				try {
					VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
					printHtmlContent += voucherDelegate.printVoucher(vdto);
				} catch (ModuleException me) {
					log.error("printing voucher failed", me);
					if (me.getExceptionCode().equals("promotions.voucher.amount.local.null")) {
						me.getExceptionDetails();
					}
				}
			}
		}
		return printHtmlContent;
	}

	@ResponseBody
	@RequestMapping(value = "/redeemVouchers")
	public VoucherRedeemRS redeemVoucher(@RequestBody VoucherRedeemRQ voucherRedeemRQ) throws ModuleException {
		VoucherRedeemRS voucherRedeemRS = paymentService.redeemVouchers(voucherRedeemRQ, getTrackInfo());
		return voucherRedeemRS;
	}

	@ResponseBody
	@RequestMapping(value = "/redeem/void")
	public TransactionalBaseRS voidVoucherRedeem(@RequestBody VoucherOption voucherOption) {
		TransactionalBaseRS voucherVoidRS = new TransactionalBaseRS();
		try {
			paymentService.voidRedeemVoucher(voucherOption, voucherOption.getTransactionId());
			voucherVoidRS.setTransactionId(voucherOption.getTransactionId());
			voucherVoidRS.setSuccess(true);
		} catch (ModuleException e) {
			log.error("Voucher unblocking failed => ", e);
			voucherVoidRS.setSuccess(false);
		}
		return voucherVoidRS;
	}

	@ResponseBody
	@RequestMapping(value = "/getsession")
	public String getSession() {
		String transactionId = UUID.randomUUID().toString();
		return initTransaction(BookingSessionStore.SESSION_KEY, transactionId);

	}

}
