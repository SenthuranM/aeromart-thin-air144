package com.isa.aeromart.services.endpoint.dto.session.impl.modification;

import java.util.Map;
import java.util.Set;

public class BalanceQueryRequiredData {
	
	private Set<String> removedReservationSegIds;
	
	private Map<String, String> groundFltSegByFltSegId;
	
	private boolean cancelSegment;
	
	private boolean modifySegment;
	
	private boolean cancelReservation;
	
	private boolean anciModification;
	
	private boolean chargeOveride;
	
	private boolean nameChange;
	
	public boolean isCancelSegment() {
		return cancelSegment;
	}

	public void setCancelSegment(boolean cancelSegment) {
		this.cancelSegment = cancelSegment;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public boolean isCancelReservation() {
		return cancelReservation;
	}

	public void setCancelReservation(boolean cancelReservation) {
		this.cancelReservation = cancelReservation;
	}

	public Set<String> getRemovedReservationSegIds() {
		return removedReservationSegIds;
	}

	public void setRemovedReservationSegIds(Set<String> removedReservationSegIds) {
		this.removedReservationSegIds = removedReservationSegIds;
	}

	public Map<String, String> getGroundFltSegByFltSegId() {
		return groundFltSegByFltSegId;
	}

	public void setGroundFltSegByFltSegId(Map<String, String> groundFltSegByFltSegId) {
		this.groundFltSegByFltSegId = groundFltSegByFltSegId;
	}

	public boolean isAnciModification() {
		return anciModification;
	}

	public void setAnciModification(boolean anciModification) {
		this.anciModification = anciModification;
	}

	public boolean isChargeOveride() {
		return chargeOveride;
	}

	public void setChargeOveride(boolean chargeOveride) {
		this.chargeOveride = chargeOveride;
	}

	public boolean isNameChange() {
		return nameChange;
	}

	public void setNameChange(boolean nameChange) {
		this.nameChange = nameChange;
	}

}
