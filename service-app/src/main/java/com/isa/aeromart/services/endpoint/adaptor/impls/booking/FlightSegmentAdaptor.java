package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.utils.common.StringUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class FlightSegmentAdaptor implements Adaptor<SessionFlightSegment, FlightSegmentTO> {

	private final static String SPLIT_DELIM = "\\$";

	@Override
	public FlightSegmentTO adapt(SessionFlightSegment sessionFlightSegment) {

		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();

		flightSegmentTO.setSegmentCode(sessionFlightSegment.getSegmentCode());
		flightSegmentTO.setDepartureDateTime(sessionFlightSegment.getDepartureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(sessionFlightSegment.getDepartureDateTimeZulu());
		flightSegmentTO.setArrivalDateTime(sessionFlightSegment.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(sessionFlightSegment.getArrivalDateTimeZulu());
		flightSegmentTO.setFlightNumber(sessionFlightSegment.getFlightDesignator());
		flightSegmentTO.setFlightRefNumber(sessionFlightSegment.getLCCFlightReference());
		flightSegmentTO.setCabinClassCode(sessionFlightSegment.getCabinClass());
		flightSegmentTO.setLogicalCabinClassCode(sessionFlightSegment.getLogicalCabinClass());
		flightSegmentTO.setFlightSegId(sessionFlightSegment.getFlightSegmentId());
		flightSegmentTO.setSegmentSequence(sessionFlightSegment.getSegmentSequence());
		flightSegmentTO.setOndSequence(sessionFlightSegment.getOndSequence());
		flightSegmentTO.setBookingClass(sessionFlightSegment.getBookingClass());
		flightSegmentTO.setReturnFlag(sessionFlightSegment.getReturnFlag());

		String operatingAirline = sessionFlightSegment.getOperatingAirline();
		if (StringUtil.isEmpty(operatingAirline) && !StringUtil.isEmpty(sessionFlightSegment.getLCCFlightReference())) {
			operatingAirline = getOperatingAirline(sessionFlightSegment.getLCCFlightReference());
		}
		flightSegmentTO.setOperatingAirline(operatingAirline);

		return flightSegmentTO;

	}

	private String getOperatingAirline(String flightRPH) {
		String arr[] = flightRPH.split(SPLIT_DELIM);
		String airlineCode = null;
		if (arr.length > 0) {
			airlineCode = arr[0];
		}
		return airlineCode;
	}

}
