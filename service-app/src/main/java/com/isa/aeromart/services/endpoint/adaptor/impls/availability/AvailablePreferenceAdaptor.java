package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.PromoType;
import com.isa.aeromart.services.endpoint.dto.availability.OriginDestinationInfo;
import com.isa.aeromart.services.endpoint.dto.availability.Preferences;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AvailablePreferenceAdaptor implements Adaptor<Preferences, AvailPreferencesTO> {
	private static Log log = LogFactory.getLog(AvailablePreferenceAdaptor.class);
	private AvailPreferencesTO availPreferencesTO;
	private int ondCount;
	private boolean findMinimum;
	private boolean fareCalendarSearch;
	private List<OriginDestinationInfo> journeyInfos;
	private TrackInfoDTO trackInfo;
	private boolean preserveOndOrder;
	private boolean requoteFlightSerach;

	public AvailablePreferenceAdaptor(AvailPreferencesTO availPreferencesTO, List<OriginDestinationInfo> journeyInfos,
			boolean findMinimum, boolean fareCalendarSearch, boolean preserveOndOrder, boolean requoteFlightSerach, TrackInfoDTO trackInfo) {
		this.availPreferencesTO = availPreferencesTO;
		this.ondCount = journeyInfos.size();
		this.findMinimum = findMinimum;
		this.fareCalendarSearch = fareCalendarSearch;
		this.preserveOndOrder = preserveOndOrder;
		this.journeyInfos = journeyInfos;
		this.trackInfo = trackInfo;
		this.requoteFlightSerach =requoteFlightSerach;

	}

	public AvailPreferencesTO adapt(Preferences source) {

		availPreferencesTO.setFareCalendarSearch(fareCalendarSearch);
		availPreferencesTO.setQuoteFares(findMinimum);
		availPreferencesTO.setModifyBooking(false);
		availPreferencesTO.setPreserveOndOrder(preserveOndOrder);
		String promoType = source.getPromotion().getType();
		if (promoType != null && !promoType.isEmpty()) {
			if (PromoType.PROMO_CODE.toString().equals(promoType)) {
				availPreferencesTO.setPromoCode(source.getPromotion().getCode());
			}
		}
		try {
			availPreferencesTO.setPointOfSale(CommonServiceUtil.getCountryByIpAddress(trackInfo.getIpAddress()));
		} catch (ModuleException e) {
			log.error("retrieve country code error : AvailablePreferenceAdaptor ==> adapt ");
		}

		availPreferencesTO.setAppIndicator(CommonUtil.getAppIndicator(trackInfo.getAppIndicator()));

		Map<Integer, Boolean> quoteOndFlexi = new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> ondFlexiSelected = new HashMap<Integer, Boolean>();
		for (int ondSequence = 0; ondSequence < journeyInfos.size(); ondSequence++) {
			OriginDestinationInfo ondInfo = journeyInfos.get(ondSequence);
			quoteOndFlexi.put(ondSequence, true);
			// if (ondInfo.getSpecificFlights() == null) {
			// ondFlexiSelected.put(ondSequence, true);
			// } else {
			String fareClassType = ondInfo.getPreferences().getAdditionalPreferences().getFareClassType();
			if (fareClassType != null) {
				if (fareClassType.equals(FareClassType.FLEXI.toString())) {
					ondFlexiSelected.put(ondSequence, true);
				} else {
					ondFlexiSelected.put(ondSequence, false);
				}
			} else {
				ondFlexiSelected.put(ondSequence, false);
			}
			// }
		}
		availPreferencesTO.setQuoteOndFlexi(quoteOndFlexi);
		availPreferencesTO.setOndFlexiSelected(ondFlexiSelected);
		// set multi city flag
		if (ondCount > 2) {
			availPreferencesTO.setMultiCitySearch(true);
		}
		availPreferencesTO.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
		availPreferencesTO.setRequoteFlightSearch(requoteFlightSerach);
		availPreferencesTO.setPreferredLanguage(LocaleContextHolder.getLocale().toString());
		return availPreferencesTO;
	}

	public boolean quoteOndFlexi() {
		return true;
	}
}
