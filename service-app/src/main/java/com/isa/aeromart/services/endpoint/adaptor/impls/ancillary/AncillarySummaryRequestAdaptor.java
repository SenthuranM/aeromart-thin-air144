package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRQ;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;

public class AncillarySummaryRequestAdaptor extends
		TransactionAwareAdaptor<AncillarySummaryRequestAdaptor.AncillarySummaryRequestAdaptorWrapper, LCCAncillaryAvailabilityInDTO> {

	@Override
	public LCCAncillaryAvailabilityInDTO adapt(
			AncillarySummaryRequestAdaptorWrapper wrapper) {
		
		LCCAncillaryAvailabilityInDTO transformed;
		AncillaryRequestAdaptor transformer;
		
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		List<FlightSegmentTO> filteredFlightSegmentTOs = new ArrayList<>();
		InsuranceFltSegBuilder insFltSegBldr = null;
		
		LCCClientReservation lccClientReservation = wrapper.getLccClientReservation();
		
		AncillaryAvailabilityRQ source = wrapper.getRequest();

		List<Ancillary> ancillaries = source.getAncillaries();

		transformed = new LCCAncillaryAvailabilityInDTO();
		flightSegmentTOs = AnciCommon.getFlightSegments(lccClientReservation.getSegments());
		
		// getting insurance origin airport
		try {
			insFltSegBldr = new InsuranceFltSegBuilder(flightSegmentTOs);
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())) {
					filteredFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		
		for (Ancillary ancillary : ancillaries) {
			transformer = AncillaryAdaptorFactory.getRequestAdaptor(ancillary.getType());
			transformed.addAncillaryType(transformer.getLCCAncillaryType());
		}


		transformed.addAllFlightSegmentTOs(filteredFlightSegmentTOs);
		transformed.setTransactionIdentifier(null); // for anci modification set as null
		transformed.setQueryingSystem(lccClientReservation.isGroupPNR() ? ProxyConstants.SYSTEM.INT : ProxyConstants.SYSTEM.AA);
		transformed.setInsuranceOrigin(insFltSegBldr.getOriginAirport()); 
		transformed.setDepartureSegmentCode(flightSegmentTOs.get(0).getSegmentCode()); 
		transformed.setAppIndicator(getTrackInfoDTO().getAppIndicator());
		/*
		 * setAirportServiceCountMap
		 */

		transformed.setBaggageSummaryTo(AnciCommon.toBaggageSummaryTo(flightSegmentTOs));

		return transformed;
	}
		
	
	public static class AncillarySummaryRequestAdaptorWrapper {

		private LCCClientReservation lccClientReservation;
		
		private AncillaryAvailabilityRQ request;

		public LCCClientReservation getLccClientReservation() {
			return lccClientReservation;
		}

		public void setLccClientReservation(LCCClientReservation lccClientReservation) {
			this.lccClientReservation = lccClientReservation;
		}

		public AncillaryAvailabilityRQ getRequest() {
			return request;
		}

		public void setRequest(AncillaryAvailabilityRQ request) {
			this.request = request;
		}

	}

}
