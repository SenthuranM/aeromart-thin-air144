package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class AvailableAncillary {

	private List<AvailableAncillaryUnit> availableUnits;

	private List<Rule> validationDefinitions;

	public List<AvailableAncillaryUnit> getAvailableUnits() {
		return availableUnits;
	}

	public void setAvailableUnits(List<AvailableAncillaryUnit> availableUnits) {
		this.availableUnits = availableUnits;
	}

	public List<Rule> getValidationDefinitions() {
		return validationDefinitions;
	}

	public void setValidationDefinitions(List<Rule> validationDefinitions) {
		this.validationDefinitions = validationDefinitions;
	}

}
