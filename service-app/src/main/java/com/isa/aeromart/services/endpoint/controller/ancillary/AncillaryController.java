package com.isa.aeromart.services.endpoint.controller.ancillary;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.ancillary.AncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AllocateAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AllocateAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillarySummaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AvailableAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.CustomerPreference;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.DefaultAncillarySelectionRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.DefaultAncillarySelectionsRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRS;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.thinair.commons.api.exception.ModuleException;

@Controller
@RequestMapping(value = "ancillary", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
		MediaType.APPLICATION_JSON_VALUE })
public class AncillaryController extends StatefulController {

	private static final Log log = LogFactory.getLog(AncillaryController.class);

	@Autowired
	private AncillaryService ancillaryService;

	@ResponseBody
	@RequestMapping(value = "/availability/reservation/create")
	public AncillaryAvailabilityRS checkAncillariesAvailabilityOnCreateReservation(
			@RequestBody AncillaryAvailabilityRQ ancillariesAvailabilityRequest) throws Exception {

		clearBookingSessionData(ancillariesAvailabilityRequest.getTransactionId());

		AncillaryAvailabilityRS response = ancillaryService.checkAncillaryAvailabilityOnCreateReservation(getTrackInfo(),
				ancillariesAvailabilityRequest);

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/availability/reservation/modify")
	public AncillaryAvailabilityRS checkAncillariesAvailabilityOnModifyReservation(
			@RequestBody AncillaryAvailabilityRQ ancillariesAvailabilityRequest) throws Exception {

		AncillaryAvailabilityRS response = ancillaryService.checkAncillaryAvailabilityOnModifyReservation(getTrackInfo(),
				ancillariesAvailabilityRequest);

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/availability/ancillaries/modify/summary")
	public AncillarySummaryRS ancillarySummary(@RequestBody AncillaryAvailabilityRQ ancillariesAvailabilityRequest)
			throws Exception {

		AncillarySummaryRS response = ancillaryService.getAncillarySummary(getTrackInfo(), ancillariesAvailabilityRequest);
		response.setTransactionId(ancillariesAvailabilityRequest.getTransactionId());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/availability/ancillaries/modify")
	public AncillaryAvailabilityRS checkAncillariesAvailabilityOnModifyAncillaries(
			@RequestBody AncillaryAvailabilityRQ ancillariesAvailabilityRequest) throws Exception {

		AncillaryAvailabilityRS response = ancillaryService.checkAncillaryAvailabilityOnModifyAncillary(getTrackInfo(),
				ancillariesAvailabilityRequest);

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/available/reservation/create")
	public AncillaryRS getAvailableAncillariesOnCreateReservation(@RequestBody AvailableAncillaryRQ availableAncillariesRequest)
			throws Exception {

		AncillaryRS response = ancillaryService.getAncillaryOnCreateReservation(getTrackInfo(), availableAncillariesRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/available/reservation/modify")
	public AncillaryRS getAvailableAncillariesOnModifyReservation(@RequestBody AvailableAncillaryRQ availableAncillariesRequest)
			throws Exception {

		AncillaryRS response = ancillaryService.getAncillaryOnModifyReservation(getTrackInfo(), availableAncillariesRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/available/ancillaries/modify")
	public AncillaryRS getAvailableAncillariesOnModifyAncillaries(@RequestBody AvailableAncillaryRQ availableAncillariesRequest)
			throws Exception {

		AncillaryRS response = ancillaryService.getAncillaryOnModifyAncillary(getTrackInfo(), availableAncillariesRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/default/reservation/create")
	public DefaultAncillarySelectionRS getDefaultAncillariesOnCreateReservation(
			@RequestBody DefaultAncillarySelectionsRQ defaultAncillarySelectionsRequest) throws Exception {
		CustomerPreference customerPrefences = getCustomerPreferences(defaultAncillarySelectionsRequest);
		DefaultAncillarySelectionRS response = ancillaryService.getDefaultAncillaryOnCreateReservation(getTrackInfo(),
				defaultAncillarySelectionsRequest, customerPrefences);
		return response;
	}

	private CustomerPreference getCustomerPreferences(TransactionalBaseRQ baseRQ) {
		CustomerSessionStore customerSession = getSessionStore(CustomerSession.SESSION_KEY);
		CustomerPreference pref = null;
		if (customerSession != null && customerSession.getLoggedInCustomerID() != null) {
			pref = new CustomerPreference();
			pref.setSeatType(
					CustomerUtil.getCustomerPreferredSeatType(Integer.parseInt(customerSession.getLoggedInCustomerID())));
		}
		return pref;
	}

	@ResponseBody
	@RequestMapping(value = "/default/reservation/modify")
	public DefaultAncillarySelectionRS getDefaultAncillariesOnModifyReservation(
			@RequestBody DefaultAncillarySelectionsRQ defaultAncillarySelectionsRequest) throws Exception {

		DefaultAncillarySelectionRS response = ancillaryService.getDefaultAncillaryOnModifyReservation(getTrackInfo(),
				defaultAncillarySelectionsRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/default/ancillaries/modify")
	public DefaultAncillarySelectionRS getDefaultAncillariesOnModifyAncillaries(
			@RequestBody DefaultAncillarySelectionsRQ defaultAncillarySelectionsRequest) throws Exception {

		DefaultAncillarySelectionRS response = ancillaryService.getDefaultAncillaryOnModifyAncillary(getTrackInfo(),
				defaultAncillarySelectionsRequest);

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/block")
	public BlockAncillaryRS blockAncillaries(@RequestBody BlockAncillaryRQ blockAncillariesRequest) throws Exception {

		BlockAncillaryRS response = ancillaryService.blockAncillary(getTrackInfo(), blockAncillariesRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/set/reservation/create")
	public AllocateAncillaryRS setAncillariesOnCreateReservation(@RequestBody AllocateAncillaryRQ allocateAncillariesRequest)
			throws Exception {
		AllocateAncillaryRS response = ancillaryService.allocateAncillaryOnCreateReservation(getTrackInfo(),
				allocateAncillariesRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/set/reservation/modify")
	public AllocateAncillaryRS setAncillariesOnModifyReservation(@RequestBody AllocateAncillaryRQ allocateAncillariesRequest)
			throws ModuleException {

		AllocateAncillaryRS response = ancillaryService.allocateAncillaryOnModifyReservation(getTrackInfo(),
				allocateAncillariesRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/set/ancillaries/modify")
	public AllocateAncillaryRS setAncillariesOnModifyAncillaries(@RequestBody AllocateAncillaryRQ allocateAncillariesRequest)
			throws Exception {

		AllocateAncillaryRS response;
		response = ancillaryService.allocateAncillaryOnModifyAncillary(getTrackInfo(), allocateAncillariesRequest);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/price/reservation/create")
	PriceQuoteAncillaryRS getAncillaryPriceQuotationOnCreateReservation(@RequestBody PriceQuoteAncillaryRQ request)
			throws ModuleException {

		PriceQuoteAncillaryRS response;
		response = ancillaryService.getAncillaryPriceQuotationOnCreateReservation(getTrackInfo(), request);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/price/reservation/modify")
	PriceQuoteAncillaryRS getAncillaryPriceQuotationOnModifyReservation(@RequestBody PriceQuoteAncillaryRQ request)
			throws ModuleException {

		PriceQuoteAncillaryRS response;

		response = ancillaryService.getAncillaryPriceQuotationOnModifyReservation(getTrackInfo(), request);

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/price/ancillaries/modify")
	PriceQuoteAncillaryRS getAncillaryPriceQuotationOnModifyAncillary(@RequestBody PriceQuoteAncillaryRQ request)
			throws ModuleException {

		PriceQuoteAncillaryRS response;

		response = ancillaryService.getAncillaryPriceQuotationOnModifyAncillary(getTrackInfo(), request);

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/update/ancillaries/modify")
	public TransactionalBaseRS updateAncillary(@RequestBody TransactionalBaseRQ baseRequest) throws Exception {

		TransactionalBaseRS response;

		response = ancillaryService.updateAncillary(getTrackInfo(), baseRequest);
		terminateTransaction(baseRequest.getTransactionId());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/ancillaries/modify/balance", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public BalanceSummaryRS setAncillaryBalanceSummery(@RequestBody TransactionalBaseRQ baseRQ) throws Exception {

		BalanceSummaryRS balanceSummery = ancillaryService.getAncillaryBalanceSummery(getTrackInfo(), baseRQ);

		return balanceSummery;

	}

	private void clearBookingSessionData(String transactionId) {
		BookingSessionStore bookingSessionStore = getTrasactionStore(transactionId);
		bookingSessionStore.storeOnHoldCreated(false);
	}

}
