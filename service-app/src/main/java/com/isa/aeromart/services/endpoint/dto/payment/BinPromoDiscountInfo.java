package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;

public class BinPromoDiscountInfo {

	private DiscountedFareDetails discountedFareDetails;
	private BigDecimal discountAmount;
	private boolean discountAsCredit;
	private String cardNumber;
	private int paymentGatewayId;

	public DiscountedFareDetails getDiscountedFareDetails() {
		return discountedFareDetails;
	}

	public void setDiscountedFareDetails(DiscountedFareDetails discountedFareDetails) {
		this.discountedFareDetails = discountedFareDetails;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(int paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public boolean isDiscountAsCredit() {
		return discountAsCredit;
	}

	public void setDiscountAsCredit(boolean discountAsCredit) {
		this.discountAsCredit = discountAsCredit;
	}

}
