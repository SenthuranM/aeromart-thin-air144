package com.isa.aeromart.services.endpoint.dto.common;

public class CaptchaRequest extends TransactionalBaseRQ{
	
	private String captcha;

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
		
}
