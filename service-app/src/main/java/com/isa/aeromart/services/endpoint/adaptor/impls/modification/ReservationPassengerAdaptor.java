package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class ReservationPassengerAdaptor  implements Adaptor<LCCClientReservationPax, ReservationPaxTO> { 	
	
	@Override
	public ReservationPaxTO adapt(LCCClientReservationPax lccClientReservationPax) {
		ReservationPaxTO reservationPaxTO = new ReservationPaxTO();
		reservationPaxTO.setSeqNumber(lccClientReservationPax.getPaxSequence());
		reservationPaxTO.setTravelerRefNumber(lccClientReservationPax.getTravelerRefNumber());
		return reservationPaxTO;
	}	
}
