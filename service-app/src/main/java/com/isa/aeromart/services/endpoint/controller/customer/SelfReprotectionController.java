package com.isa.aeromart.services.endpoint.controller.customer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.customer.SelfReprotectFlightAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.dto.common.BaseRS;
import com.isa.aeromart.services.endpoint.dto.customer.ReservationDetailsGridDataRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ReservationDetailsGridDataRS;
import com.isa.aeromart.services.endpoint.dto.customer.TransferReservationSegmentsRQ;
import com.isa.aeromart.services.endpoint.dto.customer.TransferReservationSegmentsRS;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.common.StringUtil;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.dto.ReservationDetailsDTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO.SegmentDetails;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.util.AlertConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

@Controller
@RequestMapping("selfReprotection")
public class SelfReprotectionController extends StatefulController {

	private static Log log = LogFactory.getLog(SelfReprotectionController.class);

	@Autowired
	BookingService bookingService;

	@Autowired
	private MessageSource errorMessageSource;

	@ResponseBody
	@RequestMapping(value = "/getReservationDetailsGridData")
	public ReservationDetailsGridDataRS getReservationDetailsGridData(
			@RequestBody ReservationDetailsGridDataRQ reservationDetailsGridDataReq) throws ModuleException {

		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		String customerID = customerSessionStore.getLoggedInCustomerID();
		boolean isGroupPnr;
		String originChannel;
		LCCClientReservation reservation = null;

		ReservationDetailsGridDataRS reservationDetailsGridDataRS = new ReservationDetailsGridDataRS();
		try {
			
			Integer alertId = Integer.parseInt(reservationDetailsGridDataReq.getAlertId());

			ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
			reservationSearchDTO.setPnr(reservationDetailsGridDataReq.getPnr());

			Collection<ReservationListTO> reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(
					reservationSearchDTO, null, TrackInfoUtil.getBasicTrackInfo(getRequest()));

			isGroupPnr = isGroupPNR(reservations);
			reservation = bookingService.loadProxyReservation(reservationDetailsGridDataReq.getPnr(), isGroupPnr, getTrackInfo(),
					true, (customerID != null && !customerID.isEmpty()) ? true : false, null, null,
					AppSysParamsUtil.isPromoCodeEnabled());

			if (!validateLinkDetails(reservation, alertId, reservationDetailsGridDataReq.getPnrSegmentId())) {
				throw new ModuleException("pax.reprotect.error.link.details.wrong");
			}
			
			validateIBEActionedAlerts(reservation, alertId);

			if (isGroupPnr) {
				originChannel = reservation.getAdminInfo().getOriginChannelId().split(",")[0].split("\\|")[1];
			} else {
				originChannel = reservation.getAdminInfo().getOriginChannelId();
			}

			if (!AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(originChannel)) {	
				throw new ModuleException("pax.reprotect.error.not.allowed.app.parameter");

			}

			reservationDetailsGridDataRS.setReservationDetailsList(getReservationDetailsDTOList(reservation,
					reservationDetailsGridDataReq.getPnrSegmentId()));
			
			reservationDetailsGridDataRS.setFlightsList(getSelfReprotectionFlights(reservationDetailsGridDataReq.getAlertId(),
					isGroupPnr, getReservationSegment(reservation, reservationDetailsGridDataReq.getPnrSegmentId())));
			reservationDetailsGridDataRS.setOldFlightSegment(getOldFlightSegment(reservationDetailsGridDataReq.getOldFlightSegmentId()));

			setRSCommonDataFromReq(reservationDetailsGridDataReq, reservationDetailsGridDataRS);
			reservationDetailsGridDataRS.setSuccess(true);

		} catch (ParseException pe) {
			log.error("ParseException in Self Reprotection " + pe);
			composeError(reservationDetailsGridDataRS, "pax.reprotect.error.parse.exception");

		}catch (ModuleException me) {
			log.error("ModuleException in Self Reprotection " + me);
			composeError(reservationDetailsGridDataRS, me.getExceptionCode());
		}
		
		return reservationDetailsGridDataRS;
	}

	private SelfReprotectFlightDTO getOldFlightSegment(String oldFlightSegmentId) throws NumberFormatException, ModuleException {
		Collection<FlightSegmentDTO> flightSegmentDTOs = ModuleServiceLocator.getFlightBD().getFlightSegments(
				Arrays.asList(Integer.parseInt(oldFlightSegmentId)));
		return new SelfReprotectFlightAdaptor().adapt(flightSegmentDTOs.stream().filter(Objects::nonNull).findFirst()
				.orElse(new FlightSegmentDTO()));
	}

	private void setRSCommonDataFromReq(ReservationDetailsGridDataRQ reservationDetailsGridDataReq,
			ReservationDetailsGridDataRS reservationDetailsGridDataRS) {
		reservationDetailsGridDataRS.setLanguage(reservationDetailsGridDataReq.getLanguage());
		reservationDetailsGridDataRS.setType(reservationDetailsGridDataReq.getType());
		reservationDetailsGridDataRS.setPnr(reservationDetailsGridDataReq.getPnr());
		reservationDetailsGridDataRS.setAlertId(reservationDetailsGridDataReq.getAlertId());
		reservationDetailsGridDataRS.setPnrSegmentId(reservationDetailsGridDataReq.getPnrSegmentId());
	}

	private ArrayList<Map<String, Object>> getReservationDetailsDTOList(LCCClientReservation reservation, String pnrSegmentId) {

		ArrayList<Map<String, Object>> reservationDetailsDTOList = new ArrayList<Map<String, Object>>();
		ReservationDetailsDTO reservationDetailsDTO = null;
		LCCClientReservationSegment reservationSegment = getReservationSegment(reservation, pnrSegmentId);
		int i = 0;

		for (LCCClientReservationPax lCCClientReservationPax : reservation.getPassengers()) {
			reservationDetailsDTO = new ReservationDetailsDTO();
			
			String title = StringUtil.isEmpty(lCCClientReservationPax.getTitle()) ? "" :lCCClientReservationPax.getTitle()+ ". ";
			reservationDetailsDTO.setPaxName( title + lCCClientReservationPax.getFirstName()
					+ " " + lCCClientReservationPax.getLastName());
			reservationDetailsDTO.setPaxType(lCCClientReservationPax.getPaxType());
			reservationDetailsDTO.setPnr(reservation.getPNR());
			reservationDetailsDTO.setStatus(lCCClientReservationPax.getStatus());
			reservationDetailsDTO.setDepartureDateTime(reservationSegment.getDepartureDate());
			reservationDetailsDTO.setArrivalDateTime(reservationSegment.getArrivalDate());
			reservationDetailsDTO.setFrom(reservationSegment.getDepartureAirportName());
			reservationDetailsDTO.setTo(reservationSegment.getArrivalAirportName());
			reservationDetailsDTO.setFlightSegId(extractFlightSegmentId(reservationSegment.getFlightSegmentRefNumber()));

			Map<String, Object> row = new HashMap<String, Object>();
			row.put("reservationDetailsDTO", reservationDetailsDTO);
			row.put("id", i++);
			reservationDetailsDTOList.add(row);
		}

		return reservationDetailsDTOList;
	}

	private String extractFlightSegmentId(String flightSegmentRefNumber) {
		String flightSegId = "";
		if (flightSegmentRefNumber == null) {
			return flightSegId;
		}
		String[] flightSegRefArray = flightSegmentRefNumber.split("\\$");
		return flightSegRefArray.length > 2 ? flightSegRefArray[2] : flightSegId;
	}

	private LCCClientReservationSegment getReservationSegment(LCCClientReservation reservation, String pnrSegmentId) {
		return reservation.getSegments().stream().filter(Objects::nonNull)
				.filter(lccReservationSegment -> pnrSegmentId.equalsIgnoreCase(lccReservationSegment.getPnrSegID().toString()))
				.findFirst().orElse(new LCCClientReservationSegment());
	}

	private List<SelfReprotectFlightDTO> getSelfReprotectionFlights(String alertId, boolean isGroupPNR,
			LCCClientReservationSegment lccClientReservationSegment) throws ModuleException {
		List<SelfReprotectFlightDTO> flightsList = null;
		flightsList = (List<SelfReprotectFlightDTO>) ModuleServiceLocator.getAirproxyReservationBD().getSelfReprotectFlights(
				alertId, getTrackInfo(), isGroupPNR, lccClientReservationSegment.getCarrierCode());
		if (flightsList == null || flightsList.size() == 0) {
			throw new ModuleException("module.no.reprotect.flights");
		}

		return flightsList;
	}



	private boolean isGroupPNR(Collection<ReservationListTO> reservations) {
		ReservationListTO reservationItem = reservations.stream().filter(Objects::nonNull).findFirst()
				.orElse(new ReservationListTO());

		return !StringUtil.isEmpty(reservationItem.getOriginatorPnr())
				|| (!StringUtil.isEmpty(reservationItem.getAirlineCode()) && !StringUtil.isEmpty(reservationItem
						.getMarketingAirlineCode()));
	}

	@ResponseBody
	@RequestMapping(value = "/transferReservationSegments")
	public TransferReservationSegmentsRS transferReservationSegments(
			@RequestBody TransferReservationSegmentsRQ transferReservationSegmentsReq) throws ModuleException {

		CustomerSessionStore customerSessionStore = getSessionStore(CustomerSession.SESSION_KEY);
		String customerID = customerSessionStore.getLoggedInCustomerID();
		TransferReservationSegmentsRS transferReservationSegmentsRS = new TransferReservationSegmentsRS();
		LCCClientReservation reservation = null;
		boolean isGroupPnr;

		try {
			Integer alertId = Integer.parseInt(transferReservationSegmentsReq.getAlertId());
			
			ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
			reservationSearchDTO.setPnr(transferReservationSegmentsReq.getPnr());

			Collection<ReservationListTO> reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(
					reservationSearchDTO, null, TrackInfoUtil.getBasicTrackInfo(getRequest()));

			isGroupPnr = isGroupPNR(reservations);

			reservation = bookingService.loadProxyReservation(transferReservationSegmentsReq.getPnr(), isGroupPnr,
					getTrackInfo(), true, (customerID != null && !customerID.isEmpty()) ? true : false, null, null,
					AppSysParamsUtil.isPromoCodeEnabled());

			if (!validateLinkDetails(reservation, alertId, transferReservationSegmentsReq.getPnrSegmentId())) {
				throw new ModuleException("pax.reprotect.error.link.details.wrong");
			}

			LCCClientReservationSegment reservationSegment = getReservationSegment(reservation,
					transferReservationSegmentsReq.getPnrSegmentId());

			if (reservationSegment == null) {
				log.error("Requesting reservation segment is not found for the pnr :" + transferReservationSegmentsReq.getPnr() );
				throw new ModuleException("pax.reprotect.error.reservation.segment.not.found");
			}
			
			validateIBEActionedAlerts(reservation, alertId);

			FlightSearchDTO flightSearchDTO = getFlightSearchDTO(transferReservationSegmentsReq, reservation, reservationSegment);

			FlightAvailRS flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(
					getFlightAvailRQ(reservationSegment, flightSearchDTO), getTrackInfo(), false);

			boolean areSeatsAvailable = ReservationBeanUtil.hasInventoryForFlight(flightAvailRS, flightSearchDTO.getOndList()
					.get(0).getFlightRPHList().get(0), reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount(),
					reservation.getTotalPaxInfantCount());

			if (areSeatsAvailable) {
				transferSegments(transferReservationSegmentsReq, isGroupPnr, reservation, reservationSegment, flightAvailRS);
				
				updateAlertToActioned(alertId);
				
				
			} else {
			    log.error("Seats not available for the pnr :" + transferReservationSegmentsReq.getPnr() );
				throw new ModuleException("pax.reprotect.error.airinventory.transfer.flight.inventory.unavilable");
			}
			
			transferReservationSegmentsRS.setSuccess(true);
			transferReservationSegmentsRS.setPnr(transferReservationSegmentsReq.getPnr());
			transferReservationSegmentsRS.setGroupPnr(isGroupPnr);
			

		} catch (ParseException pe) {
			log.error("ParseException in Self Reprotection " + pe);
			composeError(transferReservationSegmentsRS, "pax.reprotect.error.parse.exception");
		} catch (ModuleException me) {
			log.error("ModuleException in Self Reprotection " + me);
			composeError(transferReservationSegmentsRS, me.getExceptionCode());
		}
		
		return transferReservationSegmentsRS;
		
	}

	
	private FlightAvailRQ getFlightAvailRQ(LCCClientReservationSegment reservationSegment, FlightSearchDTO flightSearchDTO)
			throws ParseException, ModuleException {
		FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createMultiCityAvailSearchRQ(flightSearchDTO, AppIndicatorEnum.APP_IBE);

		flightAvailRQ.getAvailPreferences().setQuoteFares(false);
		flightAvailRQ.getAvailPreferences().setMultiCitySearch(false);
		flightAvailRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.IBE);
		flightAvailRQ.getAvailPreferences().setSearchSystem(reservationSegment.getSystem());
		return flightAvailRQ;
	}

	private void transferSegments(TransferReservationSegmentsRQ transferReservationSegmentsReq, boolean isGroupPNR,
			LCCClientReservation reservation, LCCClientReservationSegment reservationSegment, FlightAvailRS flightAvailRS)
			throws ModuleException {
		LCCClientTransferSegmentTO transferSegmentTO = new LCCClientTransferSegmentTO();

		if (isGroupPNR) {

			Map<String, SegmentDetails> oldSegmentDetails = new HashMap<String, SegmentDetails>();

			/* Create Old Segment Details */
			SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
			segmentDetails.setDepartureDate(reservationSegment.getDepartureDate());
			segmentDetails.setDepartureDateZulu(reservationSegment.getDepartureDateZulu());
			segmentDetails.setArrivalDate(reservationSegment.getArrivalDate());
			segmentDetails.setArrivalDateZulu(reservationSegment.getArrivalDateZulu());

			segmentDetails.setPnrSegId(new Integer(reservationSegment.getBookingFlightSegmentRefNumber()));
			segmentDetails.setSegmentCode(reservationSegment.getSegmentCode());
			segmentDetails.setCarrier(AppSysParamsUtil.extractCarrierCode(reservationSegment.getFlightNo()));
			segmentDetails.setCabinClass(reservationSegment.getCabinClassCode());
			segmentDetails.setLogicalCabinClass(reservationSegment.getLogicalCabinClass());
			segmentDetails.setFlightNumber(reservationSegment.getFlightNo());
			segmentDetails.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
			segmentDetails.setSegmentSeq(reservationSegment.getSegmentSeq());
			oldSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

			transferSegmentTO.setOldSegmentDetails(oldSegmentDetails);

			/* Create New Segment Details */
			Map<String, SegmentDetails> newSegmentDetails = new HashMap<String, SegmentDetails>();

			FlightSegmentTO newFlightSegment = flightAvailRS.getOriginDestinationInformationList().get(0)
					.getOrignDestinationOptions().get(0).getFlightSegmentList().get(0);

			segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
			segmentDetails.setDepartureDate(newFlightSegment.getDepartureDateTime());
			segmentDetails.setDepartureDateZulu(newFlightSegment.getDepartureDateTimeZulu());
			segmentDetails.setArrivalDate(newFlightSegment.getArrivalDateTime());
			segmentDetails.setArrivalDateZulu(newFlightSegment.getArrivalDateTimeZulu());
			segmentDetails.setFlightSegId(newFlightSegment.getFlightSegId());
			segmentDetails.setSegmentCode(newFlightSegment.getSegmentCode());
			segmentDetails.setCarrier(AppSysParamsUtil.extractCarrierCode(newFlightSegment.getFlightNumber()));
			segmentDetails.setCabinClass(newFlightSegment.getCabinClassCode());
			segmentDetails.setLogicalCabinClass(newFlightSegment.getLogicalCabinClassCode());
			segmentDetails.setFlightNumber(newFlightSegment.getFlightNumber());
			segmentDetails.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
			segmentDetails.setSegmentSeq(newFlightSegment.getSegmentSequence());
			newSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

			transferSegmentTO.setNewSegmentDetails(newSegmentDetails);

			if (oldSegmentDetails.size() != newSegmentDetails.size()) {
				throw new ModuleException("airreservations.transfersegment.segment.notmatch");
			}

		} else {

			Map<String, Integer> oldNewFlightSegId = new HashMap<String, Integer>();
			oldNewFlightSegId.put(reservationSegment.getFlightSegmentRefNumber(),
					Integer.parseInt(transferReservationSegmentsReq.getFltSegId()));
			transferSegmentTO.setOldNewFlightSegId(oldNewFlightSegId);
		}

		transferSegmentTO.setPnr(transferReservationSegmentsReq.getPnr());
		transferSegmentTO.setGroupPNR(reservation.isGroupPNR());
		transferSegmentTO.setReservationVersion(reservation.getVersion());
		transferSegmentTO.setAlert(true);

		ModuleServiceLocator.getAirproxySegmentBD().transferSegments(transferSegmentTO,
				TrackInfoUtil.getBasicTrackInfo(getRequest()), SalesChannelsUtil.SALES_CHANNEL_WEB_KEY);

	}

	private FlightSearchDTO getFlightSearchDTO(TransferReservationSegmentsRQ transferReservationSegmentsReq,
			LCCClientReservation reservation, LCCClientReservationSegment reservationSegment) throws ParseException {

		FlightSearchDTO flightSearchDTO = new FlightSearchDTO();
		flightSearchDTO.setAdultCount(reservation.getTotalPaxAdultCount());
		flightSearchDTO.setChildCount(reservation.getTotalPaxChildCount());
		flightSearchDTO.setInfantCount(reservation.getTotalPaxInfantCount());

		flightSearchDTO.setClassOfService(reservationSegment.getCabinClassCode());
		String segmentCode = reservationSegment.getSegmentCode();
		flightSearchDTO.setFromAirport(SegmentUtil.getFromAirport(segmentCode));
		flightSearchDTO.setToAirport(SegmentUtil.getToAirport(segmentCode));

		flightSearchDTO.setDepartureDate(DateUtil.formatDate(transferReservationSegmentsReq.getDepartureDate(), "dd/MM/yyyy"));

		ONDSearchDTO ondSearchDTO = new ONDSearchDTO();

		ondSearchDTO.setFromAirport(SegmentUtil.getFromAirport(segmentCode));
		ondSearchDTO.setToAirport(SegmentUtil.getToAirport(segmentCode));
		ondSearchDTO.setClassOfService(reservationSegment.getCabinClassCode());
		ondSearchDTO.setDepartureDate(transferReservationSegmentsReq.getDepartureDate());

		FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
		flightSegmentDTO.setFlightNumber(reservationSegment.getFlightNo());
		flightSegmentDTO.setSegmentCode(segmentCode);

		flightSegmentDTO.setDepartureDateTime(transferReservationSegmentsReq.getDepartureDate());
		flightSegmentDTO.setArrivalDateTime(transferReservationSegmentsReq.getArrivalDate());
		flightSegmentDTO.setSegmentId(Integer.parseInt(transferReservationSegmentsReq.getFltSegId()));

		List<String> flightRPHList = new ArrayList<String>();
		String flightRPH = FlightRefNumberUtil.composeFlightRPH(flightSegmentDTO);
		flightRPHList.add(flightRPH);
		ondSearchDTO.setFlightRPHList(flightRPHList);

		flightSearchDTO.getOndList().add(ondSearchDTO);
		return flightSearchDTO;
	}
	
	private boolean validateLinkDetails(LCCClientReservation reservation, Integer alertId, String pnrSegId) {
		return reservation
				.getAlertInfoTOs()
				.stream()
				.filter(Objects::nonNull)
				.filter(lccClientAlertInfoTO -> pnrSegId.equals(lccClientAlertInfoTO.getFlightSegmantRefNumber()))
				.map(lccClientAlertInfoTO -> lccClientAlertInfoTO.getAlertTO().stream().filter(Objects::nonNull)
						.map(lccClientAlertTO -> lccClientAlertTO.getAlertId() == alertId)).collect(Collectors.toList()).size() > 0;
	}
	
	private void validateIBEActionedAlerts(LCCClientReservation reservation, Integer alertId) throws ModuleException {
		List<LCCClientAlertInfoTO> alerts = reservation.getAlertInfoTOs();

		if (CollectionUtils.isEmpty(alerts)) {
			log.error("No alerts found on the reservation for the PNR: " + reservation.getPNR());
			throw new ModuleException("pax.reprotect.error.link.details.wrong");
		}

		boolean valid = false;
		for (LCCClientAlertInfoTO alert : alerts) {
			List<LCCClientAlertTO> alertTOs = alert.getAlertTO();
			if (alertTOs != null) {
				for (LCCClientAlertTO alertTO : alertTOs) {
					Integer resAlertId = alertTO.getAlertId();
					if (resAlertId.equals(alertId)) {
						String alreadyActionedAlert = alertTO.getIbeActioned();
						if (!StringUtil.isEmpty(alreadyActionedAlert)) {
							if (AlertConstants.IBEOperations.IBE_ACTIONED_YES.equals(alreadyActionedAlert)) {
								log.error("Rejecting since alert is already actioned for the alert id: " + alertId);
								throw new ModuleException("pax.reprotect.error.reprotection.done.once");
							} else {
								valid = true;
							}
							break;
						}

					}
				}
			}
		}

		if (!valid) {
			log.error("Requested alert is not avaible in the reservation for the alert ID: " + alertId);
			throw new ModuleException("pax.reprotect.error.link.details.wrong");
		}
	}
	

	private void updateAlertToActioned(Integer alertId) throws ModuleException {
		List<Integer> alertIds = new ArrayList<Integer>();
		alertIds.add(alertId);

		//alert id will be always from the operating carrier
		List<Alert> alerts = ModuleServiceLocator.getAlertingBD().retrieveAlerts(alertIds);
		if (CollectionUtils.isEmpty(alerts) || alerts.size() > 1) {
			throw new ModuleException("pax.reprotect.error.link.details.wrong");
		}
		Alert alert = alerts.get(0);
		alert.setIbeActoined(AlertConstants.IBEOperations.IBE_ACTIONED_YES);
		ModuleServiceLocator.getAlertingBD().updateAlert(alert);

	}

	private void composeError(BaseRS baseRS, String errorCode) {
		Locale locale = LocaleContextHolder.getLocale();
		String message = errorMessageSource.getMessage(errorCode, null, locale);
		baseRS.addMessage(message);
		baseRS.setSuccess(false);
	}
}
