package com.isa.aeromart.services.endpoint.dto.common;

import java.util.Date;

public class FoidInfo {

	private String foidNumber;

	private Date foidExpiry;

	private String foidPlace;

	private String placeOfBirth;

	public String getFoidPlace() {
		return foidPlace;
	}

	public void setFoidPlace(String foidPlace) {
		this.foidPlace = foidPlace;
	}

	public String getFoidNumber() {
		return foidNumber;
	}

	public void setFoidNumber(String foidNumber) {
		this.foidNumber = foidNumber;
	}

	public Date getFoidExpiry() {
		return foidExpiry;
	}

	public void setFoidExpiry(Date foidExpiry) {
		this.foidExpiry = foidExpiry;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

}
