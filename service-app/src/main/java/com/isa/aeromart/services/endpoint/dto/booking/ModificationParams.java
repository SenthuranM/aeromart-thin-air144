package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.List;

public class ModificationParams {

	private List<SegmentModificationParams> segmentModificationParams;

	private List<PaxModificationParams> paxModificationParams;

	private ReservationModificationParams reservationModificationParams;

	private ContactModificationParams contactModificationParams;

	private AnciModificationParams anciModificationParams;

	public List<SegmentModificationParams> getSegmentModificationParams() {
		return segmentModificationParams;
	}

	public void setSegmentModificationParams(List<SegmentModificationParams> segmentModificationParams) {
		this.segmentModificationParams = segmentModificationParams;
	}

	public List<PaxModificationParams> getPaxModificationParams() {
		return paxModificationParams;
	}

	public void setPaxModificationParams(List<PaxModificationParams> paxModificationParams) {
		this.paxModificationParams = paxModificationParams;
	}

	public ReservationModificationParams getReservationModificationParams() {
		return reservationModificationParams;
	}

	public void setReservationModificationParams(ReservationModificationParams reservationModificationParams) {
		this.reservationModificationParams = reservationModificationParams;
	}

	public ContactModificationParams getContactModificationParams() {
		return contactModificationParams;
	}

	public void setContactModificationParams(ContactModificationParams contactModificationParams) {
		this.contactModificationParams = contactModificationParams;
	}

	public AnciModificationParams getAnciModificationParams() {
		return anciModificationParams;
	}

	public void setAnciModificationParams(AnciModificationParams anciModificationParams) {
		this.anciModificationParams = anciModificationParams;
	}
}
