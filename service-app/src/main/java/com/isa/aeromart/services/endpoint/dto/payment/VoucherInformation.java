package com.isa.aeromart.services.endpoint.dto.payment;

public class VoucherInformation {

	private boolean isTotalAmountPaidFromVoucher;
	
	private boolean totalPaidFromVoucherLMS;

	public boolean isTotalAmountPaidFromVoucher() {
		return isTotalAmountPaidFromVoucher;
	}

	public void setIsTotalAmountPaidFromVoucher(boolean isTotalAmountPaidFromVoucher) {
		this.isTotalAmountPaidFromVoucher = isTotalAmountPaidFromVoucher;
	}

	public boolean isTotalPaidFromVoucherLMS() {
		return totalPaidFromVoucherLMS;
	}

	public void setTotalPaidFromVoucherLMS(boolean totalPaidFromVoucherLMS) {
		this.totalPaidFromVoucherLMS = totalPaidFromVoucherLMS;
	}
}