package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.FamilyRelationshipDTO;
import com.isa.thinair.airmaster.api.model.FamilyRelationship;

public class RelationshipsAdaptor implements Adaptor<FamilyRelationship, FamilyRelationshipDTO>{

	@Override
	public FamilyRelationshipDTO adapt(FamilyRelationship familyRelationship) {
		FamilyRelationshipDTO familyRelationshipDTO = new FamilyRelationshipDTO();
		
		familyRelationshipDTO.setId(familyRelationship.getFamilyRelationshipId());
		familyRelationshipDTO.setName(familyRelationship.getDescription());
		familyRelationshipDTO.setMinimumAgeInDays(familyRelationship.getMinimumAgeInDays());
		familyRelationshipDTO.setMaximumAgeInDays(familyRelationship.getMaximumAgeInDays());
		
		return familyRelationshipDTO;
	}

}
