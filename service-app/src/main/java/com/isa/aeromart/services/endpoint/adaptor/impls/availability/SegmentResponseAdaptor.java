package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.dto.common.FlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class SegmentResponseAdaptor implements Adaptor<FlightSegmentTO, FlightSegment> {

	private RPHGenerator rphGenerator;

	public SegmentResponseAdaptor(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;
	}

	@Override
	public FlightSegment adapt(FlightSegmentTO source) {

		FlightSegment target = new FlightSegment();
		target.getArrivalDateTime().setLocal(source.getArrivalDateTime());
		target.getArrivalDateTime().setZulu(source.getArrivalDateTimeZulu());
		target.getDepartureDateTime().setLocal(source.getDepartureDateTime());
		target.getDepartureDateTime().setZulu(source.getDepartureDateTimeZulu());
		target.setArrivalTerminal(source.getArrivalTerminalName());
		target.setDepartureTerminal(source.getDepartureTerminalName());
		target.setEquipmentModelNumber(source.getFlightModelNumber());
		target.setEquipmentModelInfo(source.getFlightModelDescription());
		target.setTravelMode(TravelModeAdaptor.adapt(source.getOperationType()));
		target.setFilghtDesignator(source.getFlightNumber());
		target.setCarrierCode(source.getOperatingAirline());
		target.setJourneyType((source.isDomesticFlight()
				? CommonConstants.JourneyType.DOMESTIC
				: CommonConstants.JourneyType.INTERNATIONAL));
		target.setDuration(source.getFlightDuration());
		target.setFlightSegmentRPH(rphGenerator.getRPH(source.getFlightRefNumber()));
		target.setSegmentCode(source.getSegmentCode());
		target.setRouteRefNumber(source.getRouteRefNumber());
		target.setOriginCountryCode(source.getOriginCountryCode());	
		return target;
	}

}