package com.isa.aeromart.services.endpoint.controller.common;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.delegate.common.CaptchaVerifyServiceImpl;
import com.isa.aeromart.services.endpoint.dto.common.CaptchaRequest;
import com.isa.aeromart.services.endpoint.dto.common.CaptchaResponse;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;

@Controller
@RequestMapping("captchaVerify")
public class CaptchaVerificationController extends StatefulController{
	
	/**
	* 
	* This controller is used for only captcha verification. Please dot use request in the method input parameters like this
	* in any other controller class methods. This need to be removed and get the request in appropriate way.
	* 
	*/
	@ResponseBody
	@RequestMapping(value = "/verify", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public CaptchaResponse verifyCaptcha(@RequestBody CaptchaRequest captchaReq , HttpServletRequest request) {
		
		ArrayList<String> messages = new ArrayList<String>();
		CaptchaResponse captchaResponse = new CaptchaResponse();
		BookingSessionStore bookingSessionStore = (BookingSessionStore) getSessionStore(captchaReq.getTransactionId());
		CaptchaVerifyServiceImpl captchaVerify = new CaptchaVerifyServiceImpl();
		boolean captchaVerified = captchaVerify.verifyCaptcha(captchaReq.getCaptcha(), request);
		
		if(captchaVerified){
			bookingSessionStore.storeCaptchaText(captchaReq.getCaptcha());
			messages.add("verified");
		}else{
			messages.add("Captcha verification incorrect. Please try again"); // TODO get the translations
		}
		
		captchaResponse.setTransactionId(captchaReq.getTransactionId());
		captchaResponse.setSuccess(true);
		captchaResponse.setCaptchaTrue(captchaVerified);
		captchaResponse.setMessages(messages);
			
		return captchaResponse;
	}
	
}
