package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;

public class Discount {

	// discount type
	private String type;

	// discount amount
	private BigDecimal amount = BigDecimal.ZERO;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
