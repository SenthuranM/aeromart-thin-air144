package com.isa.aeromart.services.endpoint.dto.session.impl.ancillary;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;

public class PricedAncillaryType extends Ancillary {

	private List<PricedAncillaryScope> scopes;

	private List<MonetaryAmendment> amendments;

	public List<PricedAncillaryScope> getScopes() {
		return scopes;
	}

	public void setScopes(List<PricedAncillaryScope> scopes) {
		this.scopes = scopes;
	}

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}
}
