package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.TravelModeAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.dto.common.DateTime;
import com.isa.aeromart.services.endpoint.dto.common.FlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;

public class FlightSegmentResAdaptor implements Adaptor<LCCClientReservationSegment, FlightSegment> {

	private RPHGenerator rphGenerator;

	public FlightSegmentResAdaptor(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;
	}
	@Override
	public FlightSegment adapt(LCCClientReservationSegment lccClientReservationSegment) {

		FlightSegment flightSegment = new FlightSegment();

		String newFlightSegRPH = rphGenerator.getRPH(lccClientReservationSegment.getFlightSegmentRefNumber());

		DateTime arrivalDateTime = new DateTime();
		arrivalDateTime.setLocal(lccClientReservationSegment.getArrivalDate());
		arrivalDateTime.setZulu(lccClientReservationSegment.getArrivalDateZulu());

		DateTime departureDateTime = new DateTime();
		departureDateTime.setLocal(lccClientReservationSegment.getDepartureDate());
		departureDateTime.setZulu(lccClientReservationSegment.getDepartureDateZulu());
		flightSegment.setArrivalDateTime(arrivalDateTime);
		flightSegment.setArrivalTerminal(lccClientReservationSegment.getArrivalTerminalName());
		flightSegment.setCarrierCode(lccClientReservationSegment.getCarrierCode());
		flightSegment.setDepartureDateTime(departureDateTime);
		flightSegment.setDuration(lccClientReservationSegment.getFlightDuration());
		flightSegment.setTravelMode(TravelModeAdaptor.adapt(lccClientReservationSegment.getSegmentCode()));
		flightSegment.setEquipmentModelInfo(lccClientReservationSegment.getFlightModelDescription());
		flightSegment.setEquipmentModelNumber(lccClientReservationSegment.getFlightModelNumber());
		flightSegment.setFilghtDesignator(lccClientReservationSegment.getFlightNo());
		flightSegment.setJourneyType(lccClientReservationSegment.isDomesticFlight()
				? CommonConstants.JourneyType.DOMESTIC
				: CommonConstants.JourneyType.INTERNATIONAL);
		flightSegment.setSegmentCode(lccClientReservationSegment.getSegmentCode());
		flightSegment.setFlightSegmentRPH(newFlightSegRPH);
		return flightSegment;
	}

}
