package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.ContactModificationParams;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class ContactModificationParamsAdaptor implements Adaptor<LCCClientReservation, ContactModificationParams> {

	public ContactModificationParamsAdaptor() {

	}

	@Override
	public ContactModificationParams adapt(LCCClientReservation reservation) {
		ContactModificationParams contactModificationParams = new ContactModificationParams();
		contactModificationParams.setContactModifiable(isContactModifiable(reservation.getStatus()));
		return contactModificationParams;
	}

	private boolean isContactModifiable(String reservationStatus) {
		boolean contactModifiable = true;
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)
				|| ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservationStatus)) {
			contactModifiable = false;
		}
		return contactModifiable;
	}
}
