package com.isa.aeromart.services.endpoint.delegate.ancillary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryAvailabilityRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryAvailabilityResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryBasicReservationAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryIntegrationAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryIntegrationAdaptor.AncillaryIntegrationAdaptorWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryMonetaryAmendmentRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryReservationLccAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillarySummaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillarySummaryRequestAdaptor.AncillarySummaryRequestAdaptorWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillarySummaryResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillarySummaryResponseAdaptor.AncillarySummaryResponseAdaptorWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AvailableAncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AvailableAncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.BaggageRateRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.BlockAncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.DecoratedAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.PriceQuoteAncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.PriceQuoteAncillaryResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.ReleaseAncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.TransactionAwareAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.decorator.MonetaryAmendmentDecorator;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.decorator.MonetaryAmendmentDecorator1;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciIntegration;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.AirportTransferRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.AirportTransferRequestAdaptor.AvailableAirportTransferRequestWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.AutomaticCheckinRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.BaggageRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.FlexibilityResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.FlexibilityResponseAdaptor.AvailableFlexiResponseWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.InsuranseRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.InsuranseRequestAdaptor.AvailableInsuranceRequestWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.MealRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.MealRequestAdaptor.AvailableMealRequestWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SeatRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SeatRequestAdaptor.AvailableSeatRequestWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SeatRequestAdaptor.OccupancySeatRequestWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrAirportServiceRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrAirportServiceRequestAdaptor.AvailableAncillariesSsrAirportServiceRequestWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrInFlightServiceRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type.SsrInFlightServiceRequestAdaptor.AvailableSsrInFlightServiceRequestWrapper;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.FlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationInfoAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.delegate.ancillary.bl.AncillaryModificationClassifier;
import com.isa.aeromart.services.endpoint.delegate.ancillary.bl.DefaultAncillarySelector;
import com.isa.aeromart.services.endpoint.delegate.ancillary.bl.DefaultAncillarySelectorImpl;
import com.isa.aeromart.services.endpoint.delegate.ancillary.bl.DefaultAncillarySelectorOnCreateReservation;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.dto.UpdateAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReProtect;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillarySummary;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.ModifyReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationAncillarySelection;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AllocateAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AllocateAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillarySummaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AvailableAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BaggageRateRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.CustomerPreference;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.DefaultAncillarySelectionRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.DefaultAncillarySelectionsRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.ReleaseAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryUpdateRequestTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.PostPaymentTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.PrePaymentRequestTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.PrePaymentTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.AncillaryReservationTo;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.AvailableAncillaryTo;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRQ;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationValidateRes;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.Transition;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRS;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySelection;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.ReservationChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.service.ValidationService;
import com.isa.aeromart.services.endpoint.session.TransactionFactory;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

@Service("ancillaryService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AncillaryServiceAdaptor implements AncillaryService {

	@Autowired
	private TransactionalAncillaryService transactionalAncillaryService;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private MessageSource errorMessageSource;

	@Autowired
	private ValidationService validationService;

	@Autowired
	private TransactionFactory transactionFactory;

	public AncillaryAvailabilityRS checkAncillaryAvailabilityOnCreateReservation(TrackInfoDTO trackInfo,
			AncillaryAvailabilityRQ request) throws ModuleException {

		validationService.validateAncillaryAvailabilityRQ(request);
		transactionalAncillaryService.clearSession(request.getTransactionId());

		AncillaryAvailabilityRS ancillaryAvailabilityRS = new AncillaryAvailabilityRS();
		ancillaryAvailabilityRS = checkAncillariesAvailability(trackInfo, request);
		saveReservationData(trackInfo, request);
		if (!insuranceQuotation(trackInfo, request)) {
			for (AncillaryAvailability ancillaryAvailability : ancillaryAvailabilityRS.getAvailability()) {
				if (ancillaryAvailability.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())
						&& ancillaryAvailability.isAvailable()) {
					ancillaryAvailability.setAvailable(false);
				}
			}
		}
		flexiQuotation(trackInfo, request);
		ancillaryAvailabilityRS.setTransactionId(request.getTransactionId());
		ancillaryAvailabilityRS.setSuccess(true);
		return ancillaryAvailabilityRS;

	}

	public AncillaryAvailabilityRS checkAncillaryAvailabilityOnModifyReservation(TrackInfoDTO trackInfo,
			AncillaryAvailabilityRQ request) throws ModuleException {
		validationService.validateAncillary(request);
		transactionalAncillaryService.clearSession(request.getTransactionId());

		ReservationInfo reservationInfo = getTransactionalAncillaryService().getReservationInfo(request.getTransactionId());

		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(),
				reservationInfo.isGroupPNR(), trackInfo, false, false, null, null, false);
		// store service count for ancillary airport services
		transactionalAncillaryService.storeServiceCount(request.getTransactionId(),
				AnciCommon.extractAiportServiceCount(lccClientReservation.getPassengers()));

		AncillaryAvailabilityRS ancillaryAvailabilityRS = new AncillaryAvailabilityRS();
		ancillaryAvailabilityRS = checkAncillariesAvailability(trackInfo, request);
		if (lccClientReservation.getReservationInsurances() == null
				|| !(lccClientReservation.getReservationInsurances().size() > 0)) {
			for (AncillaryAvailability ancillaryAvailability : ancillaryAvailabilityRS.getAvailability()) {
				if (ancillaryAvailability.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())
						&& ancillaryAvailability.isAvailable() && !insuranceQuotation(trackInfo, request)) {
					ancillaryAvailability.setAvailable(false);
				}
			}
		} else {
			for (AncillaryAvailability ancillaryAvailability : ancillaryAvailabilityRS.getAvailability()) {
				if (ancillaryAvailability.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())
						&& ancillaryAvailability.isAvailable()) {
					ancillaryAvailability.setAvailable(false);
				}
			}
		}
		flexiQuotation(trackInfo, request);
		ancillaryAvailabilityRS.setTransactionId(request.getTransactionId());
		ancillaryAvailabilityRS.setSuccess(true);
		return ancillaryAvailabilityRS;
	}

	public AncillaryAvailabilityRS checkAncillaryAvailabilityOnModifyAncillary(TrackInfoDTO trackInfo,
			AncillaryAvailabilityRQ request) throws ModuleException {
		validationService.validateAncillary(request);
		ModifyReservation modifyReservationData = (ModifyReservation) request.getReservationData();

		// validate Pnr and capture groupPnr value
		LoadReservationRQ loadReservationRQ = new LoadReservationRQ();
		loadReservationRQ.setPnr(modifyReservationData.getPnr());
		loadReservationRQ.setTransactionId(request.getTransactionId());
		loadReservationRQ.setValidateRQ(false);
		LoadReservationValidateRes loadReservationValidateRes = bookingService.validateLoadReservationRQ(loadReservationRQ,
				trackInfo, null);

		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(modifyReservationData.getPnr(),
				loadReservationValidateRes.isGroupPnr(), trackInfo, false, false, null, null, false);

		// create basic reservation from reservation data
		AncillaryBasicReservationAdaptor basicReservationAdaptor = new AncillaryBasicReservationAdaptor();
		BasicReservation basicReservation = basicReservationAdaptor.adapt(lccClientReservation);

		String transactionId = initiateSessionInformationForAncillaryModification(lccClientReservation,
				request.getTransactionId());

		request.setTransactionId(transactionId);

		transactionalAncillaryService.storeReservationData(transactionId, basicReservation);

		AncillaryAvailabilityRS ancillaryAvailabilityRS = new AncillaryAvailabilityRS();
		ancillaryAvailabilityRS = checkAncillariesAvailability(trackInfo, request);
		ancillaryAvailabilityRS.setTransactionId(transactionId);

		TransactionAwareAdaptor<AncillarySummaryResponseAdaptor.AncillarySummaryResponseAdaptorWrapper, AncillarySummaryRS> responseSummaryTransformer = new AncillarySummaryResponseAdaptor();
		responseSummaryTransformer.setTrackInfoDTO(trackInfo);
		AncillarySummaryResponseAdaptor.AncillarySummaryResponseAdaptorWrapper responseWrapper = new AncillarySummaryResponseAdaptorWrapper();
		responseWrapper.setLccClientReservation(lccClientReservation);
		responseWrapper.setRequest(ancillaryAvailabilityRS);
		responseWrapper.setRegUser(transactionalAncillaryService.isRegUser());

		AncillarySummaryRS ancillarySummaryRS = responseSummaryTransformer.adapt(responseWrapper);
		for (AncillaryAvailability ancillaryAvailability : ancillaryAvailabilityRS.getAvailability()) {
			for (AncillarySummary ancillarySummary : ancillarySummaryRS.getAncillarySummeryList()) {
				if (ancillaryAvailability.getType().equals(ancillarySummary.getAnciType()) && ancillaryAvailability.isAvailable()
						&& !ancillarySummary.isEditable()) {
					ancillaryAvailability.setAvailable(false);

				}
			}
		}
		if (lccClientReservation.getReservationInsurances() == null
				|| !(lccClientReservation.getReservationInsurances().size() > 0)) {
			for (AncillaryAvailability ancillaryAvailability : ancillaryAvailabilityRS.getAvailability()) {
				if (ancillaryAvailability.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())
						&& ancillaryAvailability.isAvailable() && !insuranceQuotation(trackInfo, request)) {
					ancillaryAvailability.setAvailable(false);
				}
			}
		} else {
			for (AncillaryAvailability ancillaryAvailability : ancillaryAvailabilityRS.getAvailability()) {
				if (ancillaryAvailability.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())
						&& ancillaryAvailability.isAvailable()) {
					ancillaryAvailability.setAvailable(false);
				}
			}
		}
		ancillaryAvailabilityRS.setSuccess(true);
		return ancillaryAvailabilityRS;

	}

	public AncillarySummaryRS getAncillarySummary(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request)
			throws ModuleException {

		ModifyReservation modifyReservationData = (ModifyReservation) request.getReservationData();

		// validate Pnr and capture groupPnr value
		LoadReservationRQ loadReservationRQ = new LoadReservationRQ();
		loadReservationRQ.setPnr(modifyReservationData.getPnr());
		loadReservationRQ.setTransactionId(request.getTransactionId());
		loadReservationRQ.setValidateRQ(false);
		LoadReservationValidateRes loadReservationValidateRes = bookingService.validateLoadReservationRQ(loadReservationRQ,
				trackInfo, null);

		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(modifyReservationData.getPnr(),
				loadReservationValidateRes.isGroupPnr(), trackInfo, false, false, null, null, false);

		TransactionAwareAdaptor<AncillarySummaryRequestAdaptor.AncillarySummaryRequestAdaptorWrapper, LCCAncillaryAvailabilityInDTO> requestTransformer = new AncillarySummaryRequestAdaptor();
		requestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		requestTransformer.setTrackInfoDTO(trackInfo);
		AncillarySummaryRequestAdaptorWrapper wrapper = new AncillarySummaryRequestAdaptorWrapper();
		wrapper.setLccClientReservation(lccClientReservation);
		wrapper.setRequest(request);

		LCCAncillaryAvailabilityInDTO transformedRequest = requestTransformer.adapt(wrapper);

		AnciAvailabilityRS response = ModuleServiceLocator.getAirproxyAncillaryBD().getAncillaryAvailability(transformedRequest,
				trackInfo);

		TransactionAwareAdaptor<AnciAvailabilityRS, AncillaryAvailabilityRS> responseTransformer = new AncillaryAvailabilityResponseAdaptor();
		responseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		responseTransformer.setTrackInfoDTO(trackInfo);
		responseTransformer.setTransactionId(request.getTransactionId());

		AncillaryAvailabilityRS ancillaryAvailabilityRS = responseTransformer.adapt(response);

		TransactionAwareAdaptor<AncillarySummaryResponseAdaptor.AncillarySummaryResponseAdaptorWrapper, AncillarySummaryRS> responseSummaryTransformer = new AncillarySummaryResponseAdaptor();
		responseSummaryTransformer.setTrackInfoDTO(trackInfo);
		AncillarySummaryResponseAdaptor.AncillarySummaryResponseAdaptorWrapper responseWrapper = new AncillarySummaryResponseAdaptorWrapper();
		responseWrapper.setLccClientReservation(lccClientReservation);
		responseWrapper.setRequest(ancillaryAvailabilityRS);
		responseWrapper.setRegUser(transactionalAncillaryService.isRegUser());
		AncillarySummaryRS ancillarySummaryRS = responseSummaryTransformer.adapt(responseWrapper);
		ancillarySummaryRS = validateInsuaranceQuotation(request, lccClientReservation, ancillarySummaryRS, trackInfo);
		return ancillarySummaryRS;

	}

	public BaggageRateRS getBaggageRates(TrackInfoDTO trackInfoDTO, FlightPriceRS flightPriceRS,
			AvailabilitySearchRQ availabilitySearchReq) throws ModuleException {

		String selectedSystem = transactionalAncillaryService.getPreferencesForAncillary(availabilitySearchReq.getTransactionId())
				.getSelectedSystem();
		BaggageRatesDTO baggageRatesDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getBaggageRates(trackInfoDTO,
				flightPriceRS, availabilitySearchReq.getPreferences().getCabinClass(),
				availabilitySearchReq.getPreferences().getLogicalCabinClass(), selectedSystem, ApplicationEngine.IBE,
				LocaleContextHolder.getLocale().toString());
		BaggageRateRSAdaptor baggageRateRSAdaptor = new BaggageRateRSAdaptor(
				transactionalAncillaryService.getRphGenerator(availabilitySearchReq.getTransactionId()));
		BaggageRateRS baggageRateRS = baggageRateRSAdaptor.adapt(baggageRatesDTO);
		baggageRateRS.setSuccess(true);
		return baggageRateRS;
	}

	public AncillaryRS getAncillaryOnCreateReservation(TrackInfoDTO trackInfo, AvailableAncillaryRQ request)
			throws ModuleException {

		validationService.validateAvailableAncillaryRQ(request);

		TransactionAwareAdaptor<AvailableAncillaryRQ, Map<AncillariesConstants.AncillaryType, Object>> requestTransformer = new AvailableAncillaryRequestAdaptor();
		requestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		requestTransformer.setTransactionId(request.getTransactionId());
		requestTransformer.setTrackInfoDTO(trackInfo);

		Map<AncillariesConstants.AncillaryType, Object> transformedRequest = requestTransformer.adapt(request);
		Object singleResponse;
		Object responseMap;
		Map<AncillariesConstants.AncillaryType, Object> response = new HashMap<>();

		AncillaryQuotation ancillaryQuotation = transactionalAncillaryService.getAncillaryQuotation(request.getTransactionId());

		AncillaryRS availableAncillariesResponse;

		for (AncillariesConstants.AncillaryType ancillaryType : transformedRequest.keySet()) {

			switch (ancillaryType) {
			case BAGGAGE:
				BaggageRequestAdaptor.AvailableBaggageRequestWrapper baggageRequestWrapper = (BaggageRequestAdaptor.AvailableBaggageRequestWrapper) transformedRequest
						.get(ancillaryType);

				singleResponse = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableBaggages(
						baggageRequestWrapper.getLccBaggageRequestDTOs(), baggageRequestWrapper.getTransactionIdentifier(),
						baggageRequestWrapper.getSystem(), baggageRequestWrapper.getSelectedLanguage(),
						baggageRequestWrapper.isModifyOperation(), baggageRequestWrapper.isHasFinalCutOverPrivilege(),
						baggageRequestWrapper.isRequote(), baggageRequestWrapper.getAppEngine(),
						baggageRequestWrapper.getBaggageSummaryTo(), baggageRequestWrapper.getBundledFareDTOs(),
						baggageRequestWrapper.getPnr(), baggageRequestWrapper.getTrackInfo());

				response.put(ancillaryType, singleResponse);
				break;
			case SSR_AIRPORT:
				SsrAirportServiceRequestAdaptor.AvailableAncillariesSsrAirportServiceRequestWrapper airportServiceRequestWrapper = (AvailableAncillariesSsrAirportServiceRequestWrapper) transformedRequest
						.get(ancillaryType);

				responseMap = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableAiportServices(
						airportServiceRequestWrapper.getFlightSegmentTOs(), airportServiceRequestWrapper.getAppIndicator(),
						airportServiceRequestWrapper.getSsrCategory(), airportServiceRequestWrapper.getSystem(),
						airportServiceRequestWrapper.getSelectedLanguage(),
						airportServiceRequestWrapper.getTransactionIdentifier(), airportServiceRequestWrapper.getTrackInfo(),
						airportServiceRequestWrapper.getServiceCount(), airportServiceRequestWrapper.isModifyAnci(),
						airportServiceRequestWrapper.getBundledFareDTOs(), airportServiceRequestWrapper.getPnr());

				response.put(ancillaryType, responseMap);
				break;
			case AIRPORT_TRANSFER:
				AirportTransferRequestAdaptor.AvailableAirportTransferRequestWrapper airportTransferRequestWrapper = (AvailableAirportTransferRequestWrapper) transformedRequest
						.get(ancillaryType);

				responseMap = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableAiportTransfers(
						airportTransferRequestWrapper.getFlightSegmentTOs(), airportTransferRequestWrapper.getAppIndicator(),
						airportTransferRequestWrapper.getSystem(), airportTransferRequestWrapper.getServiceCount(),
						airportTransferRequestWrapper.isModifyAnci());

				response.put(ancillaryType, responseMap);
				break;
			case MEAL:
				MealRequestAdaptor.AvailableMealRequestWrapper mealRequestWrapper = (AvailableMealRequestWrapper) transformedRequest
						.get(ancillaryType);

				singleResponse = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableMealsWithTranslations(
						mealRequestWrapper.getLccMealRequestDTOs(), mealRequestWrapper.getTransactionIdentifier(),
						mealRequestWrapper.getBundledFareDTOs(), mealRequestWrapper.getSystem(),
						mealRequestWrapper.getSelectedLanguage(), mealRequestWrapper.getTrackInfo(),
						mealRequestWrapper.getAppEngine(), mealRequestWrapper.getPnr());

				response.put(ancillaryType, singleResponse);
				break;
			case SEAT:
				SeatRequestAdaptor.AvailableSeatRequestWrapper seatRequestWrapper = (AvailableSeatRequestWrapper) transformedRequest
						.get(ancillaryType);

				singleResponse = ModuleServiceLocator.getAirproxyAncillaryBD().getSeatMap(seatRequestWrapper.getLccSeatMapDTO(),
						seatRequestWrapper.getSelectedLocale(), seatRequestWrapper.getTrackInfo(),
						seatRequestWrapper.getAppEngine());

				response.put(ancillaryType, singleResponse);
				break;
			case SSR_IN_FLIGHT:
				if (trackInfo.getAppIndicator().equals(AppIndicatorEnum.APP_XBE)
						|| (AppSysParamsUtil.isSSREnabled() && SalesChannelsUtil.isAppIndicatorWebOrMobile(trackInfo
								.getAppIndicator()))) {
					SsrInFlightServiceRequestAdaptor.AvailableSsrInFlightServiceRequestWrapper inFlightRequestWrapper = (AvailableSsrInFlightServiceRequestWrapper) transformedRequest
							.get(ancillaryType);

					singleResponse = ModuleServiceLocator.getAirproxyAncillaryBD().getSpecialServiceRequests(
							inFlightRequestWrapper.getFlightSegmentTOs(), inFlightRequestWrapper.getTransactionIdentifier(),
							inFlightRequestWrapper.getSystem(), inFlightRequestWrapper.getSelectedLanguage(),
							inFlightRequestWrapper.getTrackInfo(), inFlightRequestWrapper.getFltRefWiseSelectedSSR(),
							inFlightRequestWrapper.isModifyAnci(), inFlightRequestWrapper.isGdsPnr(), false, false);

					response.put(ancillaryType, singleResponse);
				}
				break;
			case INSURANCE:

				if (!AnciCommon.isAncillaryQuoted(ancillaryQuotation, AncillariesConstants.AncillaryType.INSURANCE)) {
					InsuranseRequestAdaptor.AvailableInsuranceRequestWrapper insuranceRequestWrapper = (AvailableInsuranceRequestWrapper) transformedRequest
							.get(ancillaryType);
					if (insuranceRequestWrapper != null) {
						singleResponse = ModuleServiceLocator.getAirproxyAncillaryBD().getInsuranceQuotation(
								insuranceRequestWrapper.getFlightSegmentTOs(), insuranceRequestWrapper.getInsuredPassengerDTOs(),
								insuranceRequestWrapper.getInsuredJourneyDTO(),
								insuranceRequestWrapper.getTransactionIdentifier(), insuranceRequestWrapper.getSystem(),
								insuranceRequestWrapper.getPnr(), insuranceRequestWrapper.getClientInfoDto(),
								insuranceRequestWrapper.getTotalTicketPriceWithoutExternal(),
								insuranceRequestWrapper.getTrackInfo(), insuranceRequestWrapper.getAppEngine(),
								insuranceRequestWrapper.getContactInfo());

						response.put(ancillaryType, singleResponse);
					} else {
						response.put(ancillaryType, new ArrayList<LCCInsuranceQuotationDTO>());
					}
				} else {
					response.put(ancillaryType, new ArrayList<LCCInsuranceQuotationDTO>());
				}

				break;

			case FLEXIBILITY:

				if (AppSysParamsUtil.isFlexiEnabledInAnci()
						&& !AnciCommon.isAncillaryQuoted(ancillaryQuotation, AncillariesConstants.AncillaryType.FLEXIBILITY)
						&& transactionalAncillaryService.getSessionFlexiDetail(request.getTransactionId()) != null) {
					FlexibilityResponseAdaptor.AvailableFlexiResponseWrapper responseWrapper = new AvailableFlexiResponseWrapper();
					responseWrapper.setInventory(transactionalAncillaryService.getFlightSegments(request.getTransactionId()));
					responseWrapper.setSessionFlexiDetailList(
							transactionalAncillaryService.getSessionFlexiDetail(request.getTransactionId()));
					singleResponse = responseWrapper;
					response.put(ancillaryType, singleResponse);
				} else if (transactionalAncillaryService.getSessionFlexiDetail(request.getTransactionId()) != null) {
					FlexibilityResponseAdaptor.AvailableFlexiResponseWrapper responseWrapper = new AvailableFlexiResponseWrapper();
					responseWrapper.setInventory(transactionalAncillaryService.getFlightSegments(request.getTransactionId()));
					responseWrapper.setSessionFlexiDetailList(
							transactionalAncillaryService.getSessionFlexiDetail(request.getTransactionId()));
					response.put(ancillaryType, responseWrapper);
				}
				break;

			case AUTOMATIC_CHECKIN:
				AutomaticCheckinRequestAdaptor.AvailableAutomaticCheckinRequestWrapper automaticCheckinRequestWrapper = (AutomaticCheckinRequestAdaptor.AvailableAutomaticCheckinRequestWrapper) transformedRequest
						.get(ancillaryType);
				responseMap = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableAutomaticCheckins(
						automaticCheckinRequestWrapper.getFlightSegmentTOs(), automaticCheckinRequestWrapper.getAppIndicator(),
						automaticCheckinRequestWrapper.getSystem(), automaticCheckinRequestWrapper.getServiceCount(),
						automaticCheckinRequestWrapper.isModifyAnci());
				response.put(ancillaryType, responseMap);
				break;
			}

		}

		TransactionAwareAdaptor<Map<AncillariesConstants.AncillaryType, Object>, AncillaryRS> responseTransformer = new AvailableAncillariesResponseAdaptor();
		responseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		responseTransformer.setErrorMessageSource(errorMessageSource);
		responseTransformer.setTransactionId(request.getTransactionId());

		availableAncillariesResponse = responseTransformer.adapt(response);
		availableAncillariesResponse.setSuccess(true);
		return availableAncillariesResponse;

	}

	public TransactionFactory getTransactionFactory() {
		return transactionFactory;
	}

	public void setTransactionFactory(TransactionFactory transactionFactory) {
		this.transactionFactory = transactionFactory;
	}

	public AncillaryRS getAncillaryOnModifyReservation(TrackInfoDTO trackInfo, AvailableAncillaryRQ request)
			throws ModuleException {
		validationService.validateAvailableAncillaryRQ(request);
		ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(request.getTransactionId());
		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(),
				reservationInfo.isGroupPNR(), trackInfo, false, false, null, null, false);

		AncillaryBasicReservationAdaptor basicReservationAdaptor = new AncillaryBasicReservationAdaptor();
		BasicReservation basicReservation = basicReservationAdaptor.adapt(lccClientReservation);

		transactionalAncillaryService.storeReservationData(request.getTransactionId(), basicReservation);
		// since get Ancillary call is same for create and modify flow we can reuse the
		// getAncillariesOnCreateReservation method to avoid code duplication
		AncillaryRS availableAncillaryRS = new AncillaryRS();
		availableAncillaryRS = getAncillaryOnCreateReservation(trackInfo, request);
		availableAncillaryRS.setTransactionId(request.getTransactionId());
		availableAncillaryRS.setSuccess(true);
		return availableAncillaryRS;
	}

	public AncillaryRS getAncillaryOnModifyAncillary(TrackInfoDTO trackInfo, AvailableAncillaryRQ request)
			throws ModuleException {

		AncillaryRS availableAncillaryRS = new AncillaryRS();
		availableAncillaryRS = getAncillaryOnCreateReservation(trackInfo, request);
		availableAncillaryRS.setTransactionId(request.getTransactionId());
		availableAncillaryRS.setSuccess(true);
		return availableAncillaryRS;

	}

	public DefaultAncillarySelectionRS getDefaultAncillaryOnCreateReservation(TrackInfoDTO trackInfo,
			DefaultAncillarySelectionsRQ request, CustomerPreference customerPreference) throws Exception {

		AvailableAncillaryRQ availableAncillaryRQ;
		AncillaryReservationTo ancillaryReservationTo;

		DefaultAncillarySelectionRS defaultAncillarySelectionRS = new DefaultAncillarySelectionRS();
		DefaultAncillarySelectorOnCreateReservation defaultAncillaryAdaptor = new DefaultAncillarySelectorOnCreateReservation();
		defaultAncillaryAdaptor.setAppIndicator(trackInfo.getAppIndicator());
		defaultAncillaryAdaptor.setCustomerPreference(customerPreference);
		availableAncillaryRQ = defaultAncillaryAdaptor.getAvailableAncillaryRequest();
		availableAncillaryRQ.setTransactionId(request.getTransactionId());

		AncillaryRS availableAncillaryResponse = getAncillaryOnCreateReservation(trackInfo, availableAncillaryRQ);

		List<SessionPassenger> sessionPassengerList = getTransactionalAncillaryService()
				.getReservationData(request.getTransactionId()).getPassengers();

		defaultAncillaryAdaptor.setAvailableAncillariesOnCreate(availableAncillaryResponse);
		defaultAncillaryAdaptor.setSessionPassengers(sessionPassengerList);

		ancillaryReservationTo = defaultAncillaryAdaptor.toDefaultAncillarySelection();
		defaultAncillarySelectionRS.setAncillaryReservation(ancillaryReservationTo.getAncillaryReservation());
		defaultAncillarySelectionRS.setTransactionId(request.getTransactionId());
		defaultAncillarySelectionRS.setSuccess(true);
		return defaultAncillarySelectionRS;
	}

	public DefaultAncillarySelectionRS getDefaultAncillaryOnModifyReservation(TrackInfoDTO trackInfo,
			DefaultAncillarySelectionsRQ request) throws Exception {
		DefaultAncillarySelectionRS response;

		ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(request.getTransactionId());

		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(),
				reservationInfo.isGroupPNR(), trackInfo, false, false, null, null, false);
		// store service count for ancillary airport services
		transactionalAncillaryService.storeServiceCount(request.getTransactionId(),
				AnciCommon.extractAiportServiceCount(lccClientReservation.getPassengers()));

		ReservationAncillarySelection reservationAncillarySelection = getAncillarySelection(request, lccClientReservation);

		AvailableAncillaryRQ availableAncillaryRequest = new AvailableAncillaryRQ();
		List<Ancillary> ancillaries = new ArrayList<>();
		Ancillary ancillary;
		for (AncillariesConstants.AncillaryType ancillaryType : AncillariesConstants.AncillaryType.getAllAncillaryTypes()) {
			if (!ancillaryType.equals(AncillariesConstants.AncillaryType.INSURANCE)) {
				ancillary = new Ancillary();
				ancillary.setType(ancillaryType.getCode());
				ancillaries.add(ancillary);
			}
		}
		availableAncillaryRequest.setTransactionId(request.getTransactionId());
		availableAncillaryRequest.setAncillaries(ancillaries);

		AncillaryRS availableAncillaryResponse = getAncillaryOnModifyReservation(trackInfo, availableAncillaryRequest);

		AncillaryReservationTo ancillaryReservationTo = new AncillaryReservationTo();
		ancillaryReservationTo.setMetaData(reservationAncillarySelection.getMetaData());
		ancillaryReservationTo.setAncillaryReservation(reservationAncillarySelection.getAncillaryReservation());

		AvailableAncillaryTo availableAncillaryTo = new AvailableAncillaryTo();
		availableAncillaryTo.setMetaData(availableAncillaryResponse.getMetaData());
		availableAncillaryTo.setAncillaries(availableAncillaryResponse.getAncillaries());

		DefaultAncillarySelector defaultAncillarySelector = new DefaultAncillarySelectorImpl();
		defaultAncillarySelector.setPreviousSelections(ancillaryReservationTo);
		defaultAncillarySelector.setAvailableAncillaries(availableAncillaryTo);

		List<ModifiedFlight> modifiedFlights = transactionalAncillaryService.getModifications(request.getTransactionId());
		Map<String, String> resSegRphToFlightSegMap = transactionalAncillaryService
				.getResSegRphToFlightSegMap(request.getTransactionId());

		List<Transition<List<String>>> modifications = AnciCommon.toModificationTransitions(modifiedFlights,
				resSegRphToFlightSegMap);
		AncillaryReservationTo defaultSelections = defaultAncillarySelector.toDefaultAncillarySelection(modifications);

		AncillaryReProtect ancillaryReProtect = new AncillaryReProtect();
		ancillaryReProtect.setAllAncillariesReProtected(defaultAncillarySelector.isAllAnciReportected());

		response = new DefaultAncillarySelectionRS();
		response.setMetaData(availableAncillaryTo.getMetaData());
		response.setAncillaryReservation(defaultSelections.getAncillaryReservation());
		response.setAncillaryReProtect(ancillaryReProtect);
		response.setTransactionId(request.getTransactionId());
		response.setSuccess(true);
		return response;
	}

	public DefaultAncillarySelectionRS getDefaultAncillaryOnModifyAncillary(TrackInfoDTO trackInfo,
			DefaultAncillarySelectionsRQ request) throws Exception {
		DefaultAncillarySelectionRS response = new DefaultAncillarySelectionRS();
		response.setTransactionId(request.getTransactionId());
		response.setSuccess(true);
		return response;
	}

	public void releaseAncillary(TrackInfoDTO trackInfo, ReleaseAncillaryRQ request) throws ModuleException {

		boolean releaseSuccess = false;
		ReleaseAncillaryRQ releaseAncillaryRQ;
		List<SelectedAncillary> releaseAncillarySelection;
		Map<AncillariesConstants.AncillaryType, Object> transformedReleaseRequest;
		TransactionAwareAdaptor<ReleaseAncillaryRQ, Map<AncillariesConstants.AncillaryType, Object>> requestReleaseTransformer = new ReleaseAncillaryRequestAdaptor();
		requestReleaseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		requestReleaseTransformer.setTrackInfoDTO(trackInfo);

		releaseAncillarySelection = transactionalAncillaryService.getAncillarySelectionsFromSession(request.getTransactionId())
				.getBlockedAncillaries();
		if (releaseAncillarySelection != null && !releaseAncillarySelection.isEmpty()) {
			for (SelectedAncillary selectedAncillary : releaseAncillarySelection) {
				if (selectedAncillary.getType().equals(AncillariesConstants.AncillaryType.SEAT.getCode())) {
					releaseAncillaryRQ = new ReleaseAncillaryRQ();
					releaseAncillaryRQ.setTransactionId(request.getTransactionId());
					releaseAncillaryRQ.setAncillaries(new ArrayList<>());
					releaseAncillaryRQ.getAncillaries().add(selectedAncillary);

					transformedReleaseRequest = requestReleaseTransformer.adapt(releaseAncillaryRQ);
					SeatRequestAdaptor.OccupancySeatRequestWrapper seatReleaseWrapper = (OccupancySeatRequestWrapper) transformedReleaseRequest
							.get(AncillariesConstants.AncillaryType.SEAT);

					// release block seats
					releaseSuccess = ModuleServiceLocator.getAirproxyAncillaryBD().releaseSeats(
							seatReleaseWrapper.getBlockSeatDTOs(), seatReleaseWrapper.getTransactionIdentifier(),
							seatReleaseWrapper.getSystem(), seatReleaseWrapper.getTrackInfo());
				}
			}
			if (releaseSuccess) {
				transactionalAncillaryService.addBlockedAncillaries(request.getTransactionId(), request.getMetaData(),
						new ArrayList<SelectedAncillary>());
			}
		}
	}

	public BlockAncillaryRS blockAncillary(TrackInfoDTO trackInfo, BlockAncillaryRQ request) throws ModuleException {

		boolean success = false;
		ReleaseAncillaryRQ releaseAncillaryRQ;
		ReservationAncillarySelection reservationAncillarySelection = null;

		validationService.validateBlockAncillaryRQ(request);

		// handle when reserved seat is trying to block again for requote and anicllary modification flow
		if (transactionalAncillaryService.isRequoteTransaction(request.getTransactionId())
				|| transactionalAncillaryService.isAncillaryTransaction(request.getTransactionId())) {
			ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(request.getTransactionId());

			LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(),
					reservationInfo.isGroupPNR(), trackInfo, false, false, null, null, false);
			// store service count for ancillary airport services
			transactionalAncillaryService.storeServiceCount(request.getTransactionId(),
					AnciCommon.extractAiportServiceCount(lccClientReservation.getPassengers()));

			reservationAncillarySelection = getAncillarySelection(request, lccClientReservation);
		}

		// compare reservation and front end seat selection and remove previous seats
		if (reservationAncillarySelection != null && reservationAncillarySelection.getAncillaryReservation() != null
				&& !reservationAncillarySelection.getAncillaryReservation().getPassengers().isEmpty()) {

			for (AncillaryPassenger ancillaryPassenger : reservationAncillarySelection.getAncillaryReservation()
					.getPassengers()) {
				for (PricedAncillaryType pricedAncillaryType : ancillaryPassenger.getAncillaryTypes()) {
					if (AncillariesConstants.Type.SEAT.equals(pricedAncillaryType.getAncillaryType())) {
						for (SelectedAncillary selectedAncillary : request.getAncillaries()) {
							if (AncillariesConstants.Type.SEAT.equals(selectedAncillary.getType())) {
								for (Preference preference : selectedAncillary.getPreferences()) {
									if (preference.getAssignee() instanceof PassengerAssignee) {
										PassengerAssignee passengerAssignee = (PassengerAssignee) preference.getAssignee();
										if (passengerAssignee.getPassengerRph().equals(ancillaryPassenger.getPassengerRph())) {
											for (Selection selection : preference.getSelections()) {
												SegmentScope SelectionScope = (SegmentScope) selection.getScope();
												for (AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()) {
													SegmentScope ReservationScope = (SegmentScope) ancillaryScope.getScope();
													if (ReservationScope.getFlightSegmentRPH()
															.equals(SelectionScope.getFlightSegmentRPH())) {
														List<SelectedItem> removalItems = new ArrayList<SelectedItem>();
														for (SelectedItem selectedItem : selection.getSelectedItems()) {
															for (PricedSelectedItem pricedSelectedItem : ancillaryScope
																	.getAncillaries()) {
																if (pricedSelectedItem.getId().equals(selectedItem.getId())) {
																	removalItems.add(selectedItem);
																}
															}
														}
														if (!removalItems.isEmpty()) {
															for (SelectedItem removeSelectedItem : removalItems) {
																selection.getSelectedItems().remove(removeSelectedItem);
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		TransactionAwareAdaptor<BlockAncillaryRQ, Map<AncillariesConstants.AncillaryType, Object>> requestBlockTransformer = new BlockAncillaryRequestAdaptor();
		requestBlockTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		requestBlockTransformer.setTrackInfoDTO(trackInfo);
		Map<AncillariesConstants.AncillaryType, Object> transformedBlockRequest = requestBlockTransformer.adapt(request);
		BlockAncillaryRS blockAncillariesResponse = new BlockAncillaryRS();

		boolean callBlockSeat = false;

		// handle IBE block seat call with IBE constrains
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(trackInfo.getAppIndicator())) {
			if (transactionalAncillaryService.isRequoteTransaction(request.getTransactionId())
					|| (!transactionalAncillaryService.isAncillaryTransaction(request.getTransactionId())
							&& !transactionalAncillaryService.isOnHoldCreated(request.getTransactionId()))) {
				callBlockSeat = true;
			}
		}

		if (callBlockSeat) {

			for (AncillariesConstants.AncillaryType ancillaryType : transformedBlockRequest.keySet()) {
				switch (ancillaryType) {
				case SEAT:
					releaseAncillaryRQ = new ReleaseAncillaryRQ();
					releaseAncillaryRQ.setTransactionId(request.getTransactionId());
					releaseAncillary(trackInfo, releaseAncillaryRQ);
					SeatRequestAdaptor.OccupancySeatRequestWrapper seatBlockWrapper = (OccupancySeatRequestWrapper) transformedBlockRequest
							.get(ancillaryType);
					success = ModuleServiceLocator.getAirproxyAncillaryBD().blockSeats(seatBlockWrapper.getBlockSeatDTOs(),
							seatBlockWrapper.getTransactionIdentifier(), seatBlockWrapper.getSystem(),
							seatBlockWrapper.getTrackInfo());

					blockAncillariesResponse.setSuccess(success);
					break;

				}
			}
		} else {
			success = true;
			blockAncillariesResponse.setSuccess(success);
		}

		if (success) {
			transactionalAncillaryService.addBlockedAncillaries(request.getTransactionId(), request.getMetaData(),
					request.getAncillaries());
		}

		blockAncillariesResponse.setTransactionId(request.getTransactionId());
		return blockAncillariesResponse;
	}

	public PriceQuoteAncillaryRS getAncillaryPriceQuotationOnCreateReservation(TrackInfoDTO trackInfo,
			TransactionalBaseRQ request) throws ModuleException {

		PriceQuoteAncillaryRS response;
		MetaData metaData;
		AnciAvailabilityRS taxApplicability = null;

		/*
		 * if(request instanceof PriceQuoteAncillaryRQ){ validationService.validateAllocateAncillaryRQ(request,
		 * transactionalAncillaryService.getReservationData(request.getTransactionId())); }
		 */

		AncillarySelection ancillarySelection = transactionalAncillaryService
				.getAncillarySelectionsFromSession(request.getTransactionId());
		metaData = new MetaData();
		metaData.setOndPreferences(ancillarySelection.getMetaData().getOndUnits());

		PriceQuoteAncillaryRQ priceQuoteAncillaryRequest = new PriceQuoteAncillaryRQ();
		priceQuoteAncillaryRequest.setMetaData(metaData);
		priceQuoteAncillaryRequest.setAncillaries(ancillarySelection.getAncillaryPreferences());

		TransactionAwareAdaptor<PriceQuoteAncillaryRQ, LCCAncillaryQuotation> priceQuoteRequestTransformer;
		priceQuoteRequestTransformer = new PriceQuoteAncillaryRequestAdaptor();
		priceQuoteRequestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		priceQuoteRequestTransformer.setTransactionId(request.getTransactionId());
		LCCAncillaryQuotation lccAncillaryQuotationRq = priceQuoteRequestTransformer.adapt(priceQuoteAncillaryRequest);
		lccAncillaryQuotationRq.setPnr(transactionalAncillaryService.getReservationInfo(request.getTransactionId()).getPnr());
		lccAncillaryQuotationRq.setModifyAnci(transactionalAncillaryService.isAncillaryTransaction(request.getTransactionId()));

		// bundle fare setting for own
		FareSegChargeTO fareSegChargeTO = transactionalAncillaryService.getPricingInformation(request.getTransactionId());
		List<BundledFareDTO> bundleFareList = fareSegChargeTO.getOndBundledFareDTOs();
		List<FlightSegmentTO> flightSegList = AnciCommon
				.toFlightSegmentTOs(transactionalAncillaryService.getFlightSegments(request.getTransactionId()));
		Map<String, BundledFareDTO> segmentBundledFareMap = new HashMap<String, BundledFareDTO>();
		int i = 0;
		if (bundleFareList != null) {
			for (BundledFareDTO bundledFareDTO : bundleFareList) {
				for (FlightSegmentTO flightSegTO : flightSegList) {
					if (flightSegTO.getOndSequence() == i) {
						segmentBundledFareMap.put(flightSegTO.getSegmentCode(), bundledFareDTO);
					}
				}
				i++;
			}
		}

		LCCAncillaryQuotation lccAncillaryQuotationRs = ModuleServiceLocator.getAirproxyAncillaryBD()
				.getSelectedAncillaryDetails(lccAncillaryQuotationRq, segmentBundledFareMap, null, trackInfo);

		TransactionAwareAdaptor<PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper, PriceQuoteAncillaryRS> responseTransformer = new PriceQuoteAncillaryResponseAdaptor();
		responseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		responseTransformer.setTransactionId(request.getTransactionId());

		AvailablePriceQuoteResponseWrapper wrapper = new AvailablePriceQuoteResponseWrapper();
		wrapper.setLccAncillaryQuotation(lccAncillaryQuotationRs);
		wrapper.setPriceQuoteAncillariesRequest(priceQuoteAncillaryRequest);

		TransactionAwareAdaptor<TransactionalBaseRQ, AncillaryMonetaryAmendmentRequestAdaptor.MonetaryAmendmentWrapper> monetaryAmendmentRequestTransformer;
		monetaryAmendmentRequestTransformer = new AncillaryMonetaryAmendmentRequestAdaptor();
		monetaryAmendmentRequestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		monetaryAmendmentRequestTransformer.setTransactionId(request.getTransactionId());

		AncillaryMonetaryAmendmentRequestAdaptor.MonetaryAmendmentWrapper monetaryAmendmentWrapper = monetaryAmendmentRequestTransformer
				.adapt(priceQuoteAncillaryRequest);

		if (!ModuleServiceLocator.getAirportBD().isBusSegment(monetaryAmendmentWrapper.getDepartingSegmentCode())) {
			taxApplicability = ModuleServiceLocator.getAirproxySearchAndQuoteBD().getTaxApplicabilityForAncillaries(
					monetaryAmendmentWrapper.getCarrierCode(), monetaryAmendmentWrapper.getDepartingSegmentCode(),
					monetaryAmendmentWrapper.getExternalChargeType(), trackInfo);
		}
		MonetaryAmendmentDecorator.MonetaryAmendmentWrapper monetaryAmendmentDecoratorWrapper = new MonetaryAmendmentDecorator.MonetaryAmendmentWrapper();
		monetaryAmendmentDecoratorWrapper.setTaxApplicability(taxApplicability);
		monetaryAmendmentDecoratorWrapper.setApplyAnciPenalty(false);

		DecoratedAdaptor<MonetaryAmendmentDecorator.MonetaryAmendmentWrapper, PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper, PriceQuoteAncillaryRS> decoratedResponseTransformer = new MonetaryAmendmentDecorator<>();
		decoratedResponseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		decoratedResponseTransformer.setTransactionId(request.getTransactionId());
		decoratedResponseTransformer.setBaseAdaptor(responseTransformer);
		decoratedResponseTransformer.setDecorate(monetaryAmendmentDecoratorWrapper);

		response = decoratedResponseTransformer.adapt(wrapper);
		response.setSuccess(true);
		return response;
	}

	public PriceQuoteAncillaryRS getAncillaryPriceQuotationOnModifyReservation(TrackInfoDTO trackInfo,
			TransactionalBaseRQ request) throws ModuleException {

		/*
		 * if(request instanceof PriceQuoteAncillaryRQ){ validationService.validateAllocateAncillaryRQ(request,
		 * transactionalAncillaryService.getReservationData(request.getTransactionId())); }
		 */

		PriceQuoteAncillaryRS response;
		response = getAncillaryPriceQuotationOnCreateReservation(trackInfo, request);
		response.setSuccess(true);
		return response;
	}

	public PriceQuoteAncillaryRS getAncillaryPriceQuotationOnModifyAncillary(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException {

		/*
		 * if(request instanceof PriceQuoteAncillaryRQ){ validationService.validateAllocateAncillaryRQ(request,
		 * transactionalAncillaryService.getReservationData(request.getTransactionId())); }
		 */

		PriceQuoteAncillaryRS response = null;
		MetaData metaData;
		AnciAvailabilityRS taxApplicability = null;

		ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(request.getTransactionId());
		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(),
				reservationInfo.isGroupPNR(), (TrackInfoDTO) trackInfo, false, false, null, null, false);
		ReservationAncillarySelection reservationAncillarySelection = getAncillarySelection(request, lccClientReservation);

		AncillarySelection ancillarySelection = transactionalAncillaryService
				.getAncillarySelectionsFromSession(request.getTransactionId());

		AncillaryModificationClassifier ancillaryModificationClassifier = new AncillaryModificationClassifier();
		ancillaryModificationClassifier.setPreviousSelection(reservationAncillarySelection);
		ancillaryModificationClassifier.setNewSelection(ancillarySelection);

		AncillarySelection ancillariesToAdd = ancillaryModificationClassifier
				.getPassengerAncillarySelection(AncillaryModificationClassifier.Classification.TO_ADD);

		// price quotation for added ancillaries
		metaData = new MetaData();
		metaData.setOndPreferences(ancillarySelection.getMetaData().getOndUnits());
		PriceQuoteAncillaryRQ priceQuoteAncillaryRequest = new PriceQuoteAncillaryRQ();
		priceQuoteAncillaryRequest.setMetaData(metaData);
		priceQuoteAncillaryRequest.setAncillaries(ancillariesToAdd.getAncillaryPreferences());

		TransactionAwareAdaptor<PriceQuoteAncillaryRQ, LCCAncillaryQuotation> priceQuoteRequestTransformer;
		priceQuoteRequestTransformer = new PriceQuoteAncillaryRequestAdaptor();
		priceQuoteRequestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		priceQuoteRequestTransformer.setTransactionId(request.getTransactionId());
		LCCAncillaryQuotation lccAncillaryQuotationRq = priceQuoteRequestTransformer.adapt(priceQuoteAncillaryRequest);
		lccAncillaryQuotationRq.setPnr(transactionalAncillaryService.getReservationInfo(request.getTransactionId()).getPnr());
		lccAncillaryQuotationRq.setModifyAnci(transactionalAncillaryService.isAncillaryTransaction(request.getTransactionId()));

		// bundle fare setting for own
		FareSegChargeTO fareSegChargeTO = transactionalAncillaryService.getPricingInformation(request.getTransactionId());
		List<BundledFareDTO> bundleFareList = fareSegChargeTO.getOndBundledFareDTOs();
		List<FlightSegmentTO> flightSegList = AnciCommon
				.toFlightSegmentTOs(transactionalAncillaryService.getFlightSegments(request.getTransactionId()));
		Map<String, BundledFareDTO> segmentBundledFareMap = new HashMap<String, BundledFareDTO>();
		int i = 0;
		if (bundleFareList != null) {
			for (BundledFareDTO bundledFareDTO : bundleFareList) {
				for (FlightSegmentTO flightSegTO : flightSegList) {
					if (flightSegTO.getOndSequence() == i) {
						segmentBundledFareMap.put(flightSegTO.getSegmentCode(), bundledFareDTO);
					}
				}
				i++;
			}
		}

		LCCAncillaryQuotation lccAncillaryQuotationRs = ModuleServiceLocator.getAirproxyAncillaryBD()
				.getSelectedAncillaryDetails(lccAncillaryQuotationRq, segmentBundledFareMap, null, trackInfo);

		TransactionAwareAdaptor<PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper, PriceQuoteAncillaryRS> responseTransformer = new PriceQuoteAncillaryResponseAdaptor();
		responseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		responseTransformer.setTransactionId(request.getTransactionId());

		AvailablePriceQuoteResponseWrapper wrapper = new AvailablePriceQuoteResponseWrapper();
		wrapper.setLccAncillaryQuotation(lccAncillaryQuotationRs);
		wrapper.setPriceQuoteAncillariesRequest(priceQuoteAncillaryRequest);

		// get removed ancillary priceQuatations
		PriceQuoteAncillaryRS removeAncillaryPriceQuotation = ancillaryModificationClassifier
				.getRemovedAncillaryPriceQuotation(reservationAncillarySelection);

		TransactionAwareAdaptor<TransactionalBaseRQ, AncillaryMonetaryAmendmentRequestAdaptor.MonetaryAmendmentWrapper> monetaryAmendmentRequestTransformer;
		monetaryAmendmentRequestTransformer = new AncillaryMonetaryAmendmentRequestAdaptor();
		monetaryAmendmentRequestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		monetaryAmendmentRequestTransformer.setTransactionId(request.getTransactionId());

		AncillaryMonetaryAmendmentRequestAdaptor.MonetaryAmendmentWrapper monetaryAmendmentWrapper = monetaryAmendmentRequestTransformer
				.adapt(priceQuoteAncillaryRequest);

		if (!ModuleServiceLocator.getAirportBD().isBusSegment(monetaryAmendmentWrapper.getDepartingSegmentCode())) {
			taxApplicability = ModuleServiceLocator.getAirproxySearchAndQuoteBD().getTaxApplicabilityForAncillaries(
					monetaryAmendmentWrapper.getCarrierCode(), monetaryAmendmentWrapper.getDepartingSegmentCode(),
					monetaryAmendmentWrapper.getExternalChargeType(), trackInfo);
		}
		MonetaryAmendmentDecorator.MonetaryAmendmentWrapper monetaryAmendmentDecoratorWrapper = new MonetaryAmendmentDecorator.MonetaryAmendmentWrapper();
		monetaryAmendmentDecoratorWrapper.setTaxApplicability(taxApplicability);
		monetaryAmendmentDecoratorWrapper.setApplyAnciPenalty(true);

		DecoratedAdaptor<MonetaryAmendmentDecorator.MonetaryAmendmentWrapper, PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper, PriceQuoteAncillaryRS> decoratedResponseTransformer = new MonetaryAmendmentDecorator<>();
		decoratedResponseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		decoratedResponseTransformer.setTransactionId(request.getTransactionId());
		decoratedResponseTransformer.setTrackInfoDTO(trackInfo);
		decoratedResponseTransformer.setBaseAdaptor(responseTransformer);
		decoratedResponseTransformer.setDecorate(monetaryAmendmentDecoratorWrapper);
		decoratedResponseTransformer.setRemoveAncillary(removeAncillaryPriceQuotation);
		response = decoratedResponseTransformer.adapt(wrapper);
		response.setSuccess(true);
		return response;
	}

	public AllocateAncillaryRS allocateAncillaryOnCreateReservation(TrackInfoDTO trackInfo, AllocateAncillaryRQ request)
			throws ModuleException {

		AllocateAncillaryRS response = new AllocateAncillaryRS();
		if (request.getMetaData() != null) {
			validationService.validateAllocateAncillaryRQ(request,
					transactionalAncillaryService.getReservationData(request.getTransactionId()));
			transactionalAncillaryService.storeAncillaryPreferences(request.getTransactionId(), request.getMetaData(),
					request.getAncillaries());

			getAncillaryIntegrateTO(trackInfo, request);
		}
		response.setTransactionId(request.getTransactionId());
		response.setSuccess(true);
		return response;
	}

	public AllocateAncillaryRS allocateAncillaryOnModifyReservation(TrackInfoDTO trackInfo, AllocateAncillaryRQ request)
			throws ModuleException {
		AllocateAncillaryRS response = new AllocateAncillaryRS();
		if (request.getMetaData() != null) {
			validationService.validateAllocateAncillaryRQ(request,
					transactionalAncillaryService.getReservationData(request.getTransactionId()));
			transactionalAncillaryService.storeAncillaryPreferences(request.getTransactionId(), request.getMetaData(),
					request.getAncillaries());

			getAncillaryIntegrateTO(trackInfo, request);
		}
		response.setTransactionId(request.getTransactionId());
		response.setSuccess(true);
		return response;
	}

	public AllocateAncillaryRS allocateAncillaryOnModifyAncillary(TrackInfoDTO trackInfo, AllocateAncillaryRQ request)
			throws ModuleException {

		AllocateAncillaryRS response = new AllocateAncillaryRS();
		if (request.getMetaData() != null) {
			validationService.validateAllocateAncillaryRQ(request,
					transactionalAncillaryService.getReservationData(request.getTransactionId()));
			transactionalAncillaryService.storeAncillaryPreferences(request.getTransactionId(), request.getMetaData(),
					request.getAncillaries());

			transactionalAncillaryService.storeAncillaryPreferences(request.getTransactionId(), request.getMetaData(),
					request.getAncillaries());

			getAncillaryIntegrateTO(trackInfo, request);
		}
		response.setSuccess(true);
		response.setTransactionId(request.getTransactionId());
		return response;
	}

	public UpdateAncillaryRS updateAncillary(TrackInfoDTO trackInfo, TransactionalBaseRQ baseRequest) throws Exception {

		PostPaymentTO postPaymentTo = new PostPaymentTO();
		ReservationInfo resInfo = transactionalAncillaryService.getReservationInfo(baseRequest.getTransactionId());

		postPaymentTo.setReservationInfo(resInfo);
		postPaymentTo.setAncillaryIntegrateTo(getExistingAncillaryIntegrateTO(trackInfo, baseRequest));
		postPaymentTo.setBasicReservation(transactionalAncillaryService.getReservationData(baseRequest.getTransactionId()));
		postPaymentTo.setPostPaymentDto(transactionalAncillaryService.getPaymentData(baseRequest.getTransactionId()));
		postPaymentTo.setVoucherInfos(transactionalAncillaryService.getPayByVoucherInfo(baseRequest.getTransactionId()));
		postPaymentTo.setIsTotalAmountPaidFromVoucher(transactionalAncillaryService
				.getVoucherInformation(baseRequest.getTransactionId()).isTotalAmountPaidFromVoucher());

		PaymentBaseSessionStore paymentSession = transactionalAncillaryService
				.getPaymentBaseSessionStore(baseRequest.getTransactionId());
		if (paymentSession != null && paymentSession.getFinancialStore() != null) {
			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseServiceTaxCCFee = paxWisePaymentServiceTaxes(
					paymentSession.getFinancialStore());
			if (postPaymentTo.getPostPaymentDto() != null && !paxWiseServiceTaxCCFee.isEmpty()) {
				postPaymentTo.getPostPaymentDto().setPaxWiseServiceTaxCCFee(paxWiseServiceTaxCCFee);
			}
		}
		postPaymentTo.setLoyaltyInformation(transactionalAncillaryService.getLoyaltyInformation(baseRequest.getTransactionId()));
		postPaymentTo.setTotalPaidFromVoucherLMS(transactionalAncillaryService.getVoucherInformation(baseRequest.getTransactionId()).isTotalPaidFromVoucherLMS());
		AncillaryUpdateRequestTO ancillaryUpdateRequestTo = AnciIntegration.getAncillaryUpdateRequestTo(postPaymentTo, trackInfo);

		ModuleServiceLocator.getAirproxyReservationBD().updateAncillary(ancillaryUpdateRequestTo.getAnciAssembler(),
				ancillaryUpdateRequestTo.isEnableFraudCheck(), ancillaryUpdateRequestTo.getContactInfo(),
				ancillaryUpdateRequestTo.isInterlinePaymentAllowed(), ancillaryUpdateRequestTo.getFlightSegWiseSelectedMeals(),
				trackInfo);

		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(resInfo.getPnr(), resInfo.isGroupPNR(),
				trackInfo, false, false, null, null, false);

		UpdateAncillaryRS updateAncillaryRS = new UpdateAncillaryRS();
		updateAncillaryRS.setContactLastName(
				transactionalAncillaryService.getReservationInfo(baseRequest.getTransactionId()).getContactInfo().getLastName());
		updateAncillaryRS.setFirstDepartureDate(transactionalAncillaryService.getFlightSegments(baseRequest.getTransactionId())
				.get(0).getFlightSegmentTO().getDepartureDateTime());
		updateAncillaryRS.setTransactionId(baseRequest.getTransactionId());
		updateAncillaryRS.setSuccess(true);

		// send medical ssr email
		SSRServicesUtil.sendMedicalSsrEmail(lccClientReservation);

		return updateAncillaryRS;
	}

	public AncillaryIntegrateTO getAncillaryIntegrateToOnCreateReservation(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException {
		return null;
	}

	public AncillaryIntegrateTO getAncillaryIntegrateToOnModifyReservation(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException {
		return null;
	}

	public AncillaryIntegrateTO getAncillaryIntegrateToOnModifyAncillary(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException {
		return null;
	}

	/**
	 * FIXME temp hack to fix the issue TODO refactor whole flow
	 * 
	 * @param trackInfo
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public AncillaryIntegrateTO getExistingAncillaryIntegrateTO(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException {
		AncillarySelection ancillarySelection = transactionalAncillaryService
				.getAncillarySelectionsFromSession(request.getTransactionId());
		if (ancillarySelection.getIntegrateTO() == null) {
			throw new ModuleException("Invalid state");
		}

		return ancillarySelection.getIntegrateTO();
	}

	public AncillaryIntegrateTO getAncillaryIntegrateTO(TrackInfoDTO trackInfo, TransactionalBaseRQ request)
			throws ModuleException {

		AncillaryIntegrateTO ancillaryIntegrateTo;
		boolean anciModify = transactionalAncillaryService.isAncillaryTransaction(request.getTransactionId());
		boolean segmentModify = transactionalAncillaryService.isRequoteTransaction(request.getTransactionId());

		TransactionAwareAdaptor<AncillaryIntegrationAdaptor.AncillaryIntegrationAdaptorWrapper, AncillaryIntegrateTO> modifyAncillaryTransformer = null;
		ReservationAncillarySelection baseAncillarySelectionsResponse = null;
		LCCClientReservation lccClientReservation = null;
		AnciAvailabilityRS taxApplicability = null;

		if (anciModify || segmentModify) {
			ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(request.getTransactionId());
			lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(), reservationInfo.isGroupPNR(),
					(TrackInfoDTO) trackInfo, false, false, null, null, false);
		}

		AncillarySelection ancillarySelection = transactionalAncillaryService
				.getAncillarySelectionsFromSession(request.getTransactionId());

		// price quote for selected ancillaries
		PriceQuoteAncillaryRQ priceQuoteAncillariesRequest = new PriceQuoteAncillaryRQ();
		priceQuoteAncillariesRequest.setMetaData(new MetaData());
		priceQuoteAncillariesRequest.getMetaData()
				.setOndPreferences(ancillarySelection.getMetaData() != null
						? ancillarySelection.getMetaData().getOndUnits()
						: new ArrayList<OndUnit>());
		priceQuoteAncillariesRequest.setAncillaries(ancillarySelection.getAncillaryPreferences() != null
				? ancillarySelection.getAncillaryPreferences()
				: new ArrayList<SelectedAncillary>());
		priceQuoteAncillariesRequest.setTransactionId(request.getTransactionId());

		PriceQuoteAncillaryRS priceQuoteAncillariesResponse = getAncillaryPriceQuotation(trackInfo, priceQuoteAncillariesRequest);

		modifyAncillaryTransformer = new AncillaryIntegrationAdaptor();
		modifyAncillaryTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		modifyAncillaryTransformer.setTransactionId(request.getTransactionId());
		modifyAncillaryTransformer.setTrackInfoDTO(trackInfo);

		TransactionAwareAdaptor<TransactionalBaseRQ, AncillaryMonetaryAmendmentRequestAdaptor.MonetaryAmendmentWrapper> monetaryAmendmentRequestTransformer;
		monetaryAmendmentRequestTransformer = new AncillaryMonetaryAmendmentRequestAdaptor();
		monetaryAmendmentRequestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		monetaryAmendmentRequestTransformer.setTransactionId(priceQuoteAncillariesRequest.getTransactionId());

		AncillaryMonetaryAmendmentRequestAdaptor.MonetaryAmendmentWrapper monetaryAmendmentWrapper = monetaryAmendmentRequestTransformer
				.adapt(priceQuoteAncillariesRequest);

		if (!ModuleServiceLocator.getAirportBD().isBusSegment(monetaryAmendmentWrapper.getDepartingSegmentCode())) {
			taxApplicability = ModuleServiceLocator.getAirproxySearchAndQuoteBD().getTaxApplicabilityForAncillaries(
					monetaryAmendmentWrapper.getCarrierCode(), monetaryAmendmentWrapper.getDepartingSegmentCode(),
					monetaryAmendmentWrapper.getExternalChargeType(), trackInfo);
		}
		MonetaryAmendmentDecorator1.MonetaryAmendmentWrapper monetaryAmendmentDecoratorWrapper = new MonetaryAmendmentDecorator1.MonetaryAmendmentWrapper();
		monetaryAmendmentDecoratorWrapper.setTaxApplicability(taxApplicability);
		if (anciModify) {
			monetaryAmendmentDecoratorWrapper.setApplyAnciPenalty(true);
			baseAncillarySelectionsResponse = getAncillarySelection(request, lccClientReservation);
			// TODO remove when back end support to pick up meal segments even meals are not available for selected
			// passenger
			AnciCommon.updatePriceQuoteForUnavailableMealSegments(ancillarySelection, priceQuoteAncillariesResponse,
					baseAncillarySelectionsResponse);
		} else if (segmentModify) {
			monetaryAmendmentDecoratorWrapper.setLccClientReservation(lccClientReservation);
		}
		DecoratedAdaptor<MonetaryAmendmentDecorator1.MonetaryAmendmentWrapper, AncillaryIntegrationAdaptor.AncillaryIntegrationAdaptorWrapper, AncillaryIntegrateTO> decoratedResponseTransformer = new MonetaryAmendmentDecorator1();
		decoratedResponseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		decoratedResponseTransformer.setTransactionId(priceQuoteAncillariesRequest.getTransactionId());
		decoratedResponseTransformer.setDecorate(monetaryAmendmentDecoratorWrapper);
		decoratedResponseTransformer.setBaseAdaptor(modifyAncillaryTransformer);
		decoratedResponseTransformer.setTrackInfoDTO(trackInfo);

		AncillaryIntegrationAdaptorWrapper wrapper = new AncillaryIntegrationAdaptorWrapper();
		wrapper.setReservationAncillarySelection(baseAncillarySelectionsResponse);
		wrapper.setPriceQuatedSelectdAncillary(priceQuoteAncillariesResponse);
		wrapper.setReservation(lccClientReservation);
		wrapper.setAncillaryModification(anciModify);

		ancillaryIntegrateTo = decoratedResponseTransformer.adapt(wrapper);
		ancillarySelection.setIntegrateTO(ancillaryIntegrateTo);
		return ancillaryIntegrateTo;

	}

	public PrePaymentTO getPrePaymentTO(TrackInfoDTO trackInfo, TransactionalBaseRQ baseRequest) throws Exception {

		ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(baseRequest.getTransactionId());

		PrePaymentRequestTO prePaymentRequestTo = new PrePaymentRequestTO();
		prePaymentRequestTo.setAncillaryIntegrateTo(getExistingAncillaryIntegrateTO(trackInfo, baseRequest));
		prePaymentRequestTo.setReservationInfo(reservationInfo);

		return AnciIntegration.getPrePaymentTo(prePaymentRequestTo);
	}

	public ReservationAncillarySelection getAncillarySelection(TransactionalBaseRQ baseRQ,
			LCCClientReservation lccClientReservation) {

		ReservationAncillarySelection response;

		TransactionAwareAdaptor<LCCClientReservation, ReservationAncillarySelection> adaptor = new AncillaryReservationLccAdaptor();
		adaptor.setTransactionalAncillaryService(transactionalAncillaryService);
		adaptor.setTransactionId(baseRQ.getTransactionId());

		response = adaptor.adapt(lccClientReservation);
		return response;
	}

	public BalanceSummaryRS getAncillaryBalanceSummary(TrackInfoDTO trackInfo, TransactionalBaseRQ request) throws Exception {

		ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(request.getTransactionId());

		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(),
				reservationInfo.isGroupPNR(), trackInfo, false, false, null, null, false);

		AncillaryIntegrateTO ancillaryIntergrationTo = getAncillaryIntegrateTO(trackInfo, request);
		return AnciIntegration.genarateAncillaryBalanceSummary(ancillaryIntergrationTo, request.getTransactionId(),
				reservationInfo, lccClientReservation);

	}

	public TransactionalAncillaryService getTransactionalAncillaryService() {
		return transactionalAncillaryService;
	}

	public void setTransactionalAncillaryService(TransactionalAncillaryService transactionalAncillaryService) {
		this.transactionalAncillaryService = transactionalAncillaryService;
	}

	public BookingService getBookingService() {
		return bookingService;
	}

	public void setBookingService(BookingService bookingService) {
		this.bookingService = bookingService;
	}

	public ValidationService getValidationService() {
		return validationService;
	}

	public MessageSource getErrorMessageSource() {
		return errorMessageSource;
	}

	public void setErrorMessageSource(MessageSource errorMessageSource) {
		this.errorMessageSource = errorMessageSource;
	}

	private AncillaryAvailabilityRS checkAncillariesAvailability(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request)
			throws ModuleException {

		TransactionAwareAdaptor<AncillaryAvailabilityRQ, LCCAncillaryAvailabilityInDTO> requestTransformer = new AncillaryAvailabilityRequestAdaptor();
		requestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		requestTransformer.setTrackInfoDTO(trackInfo);

		LCCAncillaryAvailabilityInDTO transformedRequest = requestTransformer.adapt(request);

		AnciAvailabilityRS response = ModuleServiceLocator.getAirproxyAncillaryBD().getAncillaryAvailability(transformedRequest,
				trackInfo);

		TransactionAwareAdaptor<AnciAvailabilityRS, AncillaryAvailabilityRS> responseTransformer = new AncillaryAvailabilityResponseAdaptor();
		responseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		responseTransformer.setTrackInfoDTO(trackInfo);
		responseTransformer.setTransactionId(request.getTransactionId());

		return responseTransformer.adapt(response);
	}

	private void saveReservationData(BasicTrackInfo trackInfo, AncillaryAvailabilityRQ request) throws ModuleException {
		BasicReservation basicReservation;

		if (request.getReservationData().getMode().equals(BasicReservation.Mode.CREATE)) {
			basicReservation = (BasicReservation) request.getReservationData();
			transactionalAncillaryService.storeReservationData(request.getTransactionId(), basicReservation);
		}
	}

	private boolean insuranceQuotation(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request) throws ModuleException {
		Ancillary ancillary;
		AncillaryRS availableAncillariesResponse;

		for (Ancillary ancillaryRef : request.getAncillaries()) {
			if (ancillaryRef.getType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())) {
				AvailableAncillaryRQ availableAncillaryRequest = new AvailableAncillaryRQ();
				availableAncillaryRequest.setTransactionId(request.getTransactionId());
				availableAncillaryRequest.setMetaData(request.getMetaData());
				List<Ancillary> ancillaries = new ArrayList<>();
				ancillary = new Ancillary();
				ancillary.setType(AncillariesConstants.AncillaryType.INSURANCE.getCode());
				ancillaries.add(ancillary);
				availableAncillaryRequest.setAncillaries(ancillaries);

				if (request.getReservationData().getMode().equals(BasicReservation.Mode.CREATE)) {
					availableAncillariesResponse = getAncillaryOnCreateReservation(trackInfo, availableAncillaryRequest);
				} else {
					availableAncillariesResponse = getAncillaryOnModifyReservation(trackInfo, availableAncillaryRequest);
				}

				if (!availableAncillariesResponse.getAncillaries().get(0).getProviders().isEmpty()) {
					AncillaryQuotation ancillaryQuotation = transactionalAncillaryService
							.getAncillaryQuotation(request.getTransactionId());
					if (!AnciCommon.isAncillaryQuoted(ancillaryQuotation, AncillariesConstants.AncillaryType.INSURANCE)) {
						if (ancillaryQuotation != null && ancillaryQuotation.getQuotedAncillaries() != null) {
							ancillaryQuotation.getQuotedAncillaries().addAll(availableAncillariesResponse.getAncillaries());
							transactionalAncillaryService.storeAncillaryQuotations(request.getTransactionId(),
									availableAncillariesResponse.getMetaData(), ancillaryQuotation.getQuotedAncillaries());
						} else {
							transactionalAncillaryService.storeAncillaryQuotations(request.getTransactionId(),
									availableAncillariesResponse.getMetaData(), availableAncillariesResponse.getAncillaries());
						}
					}
					break;
				} else {
					return false;
				}
			}
		}
		return true;
	}

	private void flexiQuotation(TrackInfoDTO trackInfo, AncillaryAvailabilityRQ request) throws ModuleException {
		Ancillary ancillary;

		for (Ancillary ancillaryRef : request.getAncillaries()) {
			if (ancillaryRef.getType().equals(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode())
					&& transactionalAncillaryService.getSessionFlexiDetail(request.getTransactionId()) != null) {
				AvailableAncillaryRQ availableAncillaryRequest = new AvailableAncillaryRQ();
				availableAncillaryRequest.setTransactionId(request.getTransactionId());
				availableAncillaryRequest.setMetaData(request.getMetaData());
				List<Ancillary> ancillaries = new ArrayList<>();
				ancillary = new Ancillary();
				ancillary.setType(AncillariesConstants.AncillaryType.FLEXIBILITY.getCode());
				ancillaries.add(ancillary);
				availableAncillaryRequest.setAncillaries(ancillaries);

				AncillaryRS availableAncillariesResponse = getAncillaryOnCreateReservation(trackInfo, availableAncillaryRequest);

				AncillaryQuotation ancillaryQuotation = transactionalAncillaryService
						.getAncillaryQuotation(request.getTransactionId());
				if (ancillaryQuotation != null && ancillaryQuotation.getQuotedAncillaries() != null) {
					ancillaryQuotation.getQuotedAncillaries().addAll(availableAncillariesResponse.getAncillaries());
					transactionalAncillaryService.storeAncillaryQuotations(request.getTransactionId(),
							availableAncillariesResponse.getMetaData(), ancillaryQuotation.getQuotedAncillaries());
				} else {
					transactionalAncillaryService.storeAncillaryQuotations(request.getTransactionId(),
							availableAncillariesResponse.getMetaData(), availableAncillariesResponse.getAncillaries());
				}
			}
		}

	}

	private String initiateSessionInformationForAncillaryModification(LCCClientReservation reservation, String transactionId) {
		ReservationInfoAdaptor reservationInfoAdaptor = new ReservationInfoAdaptor(transactionId);
		ReservationInfo resInfo = reservationInfoAdaptor.adapt(reservation);
		RPHGenerator rphGenerator;

		PreferenceInfo prefInfo = new PreferenceInfo();
		prefInfo.setSelectedSystem(reservation.isGroupPNR() ? SYSTEM.INT.toString() : SYSTEM.AA.toString());

		FareSegChargeTO pricingInformation = new FareSegChargeTO();
		pricingInformation.setOndBundledFareDTOs(
				reservation.getBundledFareDTOs() == null ? new ArrayList<BundledFareDTO>() : reservation.getBundledFareDTOs());
		transactionId = transactionalAncillaryService.initializeAnciModifyTransaction(transactionId);
		transactionalAncillaryService.storePricingInformtation(transactionId, pricingInformation);
		transactionalAncillaryService.storeReservationInfo(transactionId, resInfo);
		transactionalAncillaryService.storePreferenceInfo(transactionId, prefInfo);
		transactionalAncillaryService.storeSessionSegments(transactionId, AnciCommon
				.updateOndSequence(reservation.getCarrierOndGrouping(), AnciCommon.getFlightSegments(reservation.getSegments())));
		rphGenerator = transactionalAncillaryService.getRphGenerator(transactionId);
		// initiate rphGenerator
		List<LCCClientReservationSegment> sortedResSegList = new ArrayList<>(reservation.getSegments());
		Collections.sort(sortedResSegList);
		for (LCCClientReservationSegment segment : sortedResSegList) {
			rphGenerator.getRPH(segment.getFlightSegmentRefNumber());
		}
		transactionalAncillaryService.storeApplicableServiceTaxes(transactionId, reservation.getApplicableServiceTaxes());
		transactionalAncillaryService.storeServiceCount(transactionId,
				AnciCommon.extractAiportServiceCount(reservation.getPassengers()));
		return transactionId;
	}

	@Override
	public BalanceSummaryRS getAncillaryBalanceSummery(TrackInfoDTO trackInfo, TransactionalBaseRQ request) throws Exception {

		ReservationInfo reservationInfo = transactionalAncillaryService.getReservationInfo(request.getTransactionId());

		LCCClientReservation lccClientReservation = bookingService.loadProxyReservation(reservationInfo.getPnr(),
				reservationInfo.isGroupPNR(), trackInfo, false, false, null, null, false);

		AncillaryIntegrateTO ancillaryIntergrationTo = getAncillaryIntegrateTO(trackInfo, request);

		// applyServiceTax(request.getTransactionId(), trackInfo, reservationInfo, ancillaryIntergrationTo);

		return AnciIntegration.genarateAncillaryBalanceSummary(ancillaryIntergrationTo, request.getTransactionId(),
				reservationInfo, lccClientReservation);

	}

	public PriceQuoteAncillaryRS getAncillaryPriceQuotation(TrackInfoDTO trackInfo,
			PriceQuoteAncillaryRQ priceQuoteAncillariesRequest) throws ModuleException {

		TransactionAwareAdaptor<PriceQuoteAncillaryRQ, LCCAncillaryQuotation> priceQuoteRequestTransformer;
		priceQuoteRequestTransformer = new PriceQuoteAncillaryRequestAdaptor();
		priceQuoteRequestTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		priceQuoteRequestTransformer.setTransactionId(priceQuoteAncillariesRequest.getTransactionId());
		LCCAncillaryQuotation lccAncillaryQuotationRq = priceQuoteRequestTransformer.adapt(priceQuoteAncillariesRequest);
		lccAncillaryQuotationRq.setPnr(
				transactionalAncillaryService.getReservationInfo(priceQuoteAncillariesRequest.getTransactionId()).getPnr());
		lccAncillaryQuotationRq.setModifyAnci(
				transactionalAncillaryService.isAncillaryTransaction(priceQuoteAncillariesRequest.getTransactionId()));

		// bundle fare setting for own
		FareSegChargeTO fareSegChargeTO = transactionalAncillaryService
				.getPricingInformation(priceQuoteAncillariesRequest.getTransactionId());
		List<BundledFareDTO> bundleFareList = fareSegChargeTO.getOndBundledFareDTOs();
		List<FlightSegmentTO> flightSegList = AnciCommon.toFlightSegmentTOs(
				transactionalAncillaryService.getFlightSegments(priceQuoteAncillariesRequest.getTransactionId()));
		Map<String, BundledFareDTO> segmentBundledFareMap = new HashMap<String, BundledFareDTO>();
		int i = 0;
		if (bundleFareList != null) {
			for (BundledFareDTO bundledFareDTO : bundleFareList) {
				for (FlightSegmentTO flightSegTO : flightSegList) {
					if (flightSegTO.getOndSequence() == i) {
						segmentBundledFareMap.put(flightSegTO.getSegmentCode(), bundledFareDTO);
					}
				}
				i++;
			}
		}

		LCCAncillaryQuotation lccAncillaryQuotationRs = ModuleServiceLocator.getAirproxyAncillaryBD()
				.getSelectedAncillaryDetails(lccAncillaryQuotationRq, segmentBundledFareMap, null, trackInfo);

		TransactionAwareAdaptor<PriceQuoteAncillaryResponseAdaptor.AvailablePriceQuoteResponseWrapper, PriceQuoteAncillaryRS> responseTransformer = new PriceQuoteAncillaryResponseAdaptor();
		responseTransformer.setTransactionalAncillaryService(transactionalAncillaryService);
		responseTransformer.setTransactionId(priceQuoteAncillariesRequest.getTransactionId());

		AvailablePriceQuoteResponseWrapper wrapper = new AvailablePriceQuoteResponseWrapper();
		wrapper.setLccAncillaryQuotation(lccAncillaryQuotationRs);
		wrapper.setPriceQuoteAncillariesRequest(priceQuoteAncillariesRequest);

		return responseTransformer.adapt(wrapper);
	}

	private void applyServiceTax(String transactionId, TrackInfoDTO trackInfoDTO, ReservationInfo resInfo,
			AncillaryIntegrateTO ancillaryIntergrationTo) throws ModuleException {

		List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
		ReservationPaxTO resPaxTO = null;

		PaymentBaseSessionStore paymentSessionStore = transactionFactory.getTransactionStore(transactionId);

		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		baseAvailRQ.getAvailPreferences().setSearchSystem(
				ProxyConstants.SYSTEM.getEnum(paymentSessionStore.getPreferencesForPayment().getSelectedSystem()));

		List<FlightSegmentTO> flightSegments = adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());

		// Removing Service Tax from PaymentSessionStore in payment external charge
		paymentSessionStore.removePaymentPaxCharge(EXTERNAL_CHARGES.SERVICE_TAX);

		for (AncillaryIntegratePassengerTO anciPassenger : ancillaryIntergrationTo.getReservation().getPassengers()) {
			if (anciPassenger.getExternalCharges() != null && !anciPassenger.getExternalCharges().isEmpty()) {
				paxWiseExternalCharges.put(Integer.parseInt(anciPassenger.getPassengerRph()), anciPassenger.getExternalCharges());
			}
		}

		for (ReservationPaxTO reservationPaxTO : resInfo.getPaxList()) {

			resPaxTO = new ReservationPaxTO();
			resPaxTO.setSeqNumber(reservationPaxTO.getSeqNumber());
			resPaxTO.setIsParent(reservationPaxTO.getIsParent());
			resPaxTO.setPaxType(reservationPaxTO.getPaxType());
			resPaxTO.setInfantWith(reservationPaxTO.getInfantWith());
			paxList.add(resPaxTO);

			paxWisePaxTypes.put(reservationPaxTO.getSeqNumber(), reservationPaxTO.getPaxType());
		}

		// Constructing ServiceTaxQuoteCriteriaDTO obj
		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = createServiceTaxQuoteCriteriaForTktDTO(baseAvailRQ, null,
				flightSegments, resInfo.getContactInfo(), paxWiseExternalCharges, paxWisePaxTypes, transactionId);

		// Calling Service Tax Call
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator.getAirproxyReservationBD()
				.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfoDTO);

		// Applying Service Tax RS to Pax wise Charges
		ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(paxList, serviceTaxQuoteRS, false, ApplicationEngine.IBE, false);

		// Store Service Taxes
		// storeServiceTaxChargesToSession(transactionId, paxList);

	}

	private List<FlightSegmentTO> adaptFlightSegment(List<SessionFlightSegment> segments) {
		FlightSegmentAdaptor flightSegmentAdaptor = new FlightSegmentAdaptor();
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		AdaptorUtils.adaptCollection(segments, flightSegmentTOs, flightSegmentAdaptor);
		return flightSegmentTOs;
	}

	private ServiceTaxQuoteCriteriaForTktDTO createServiceTaxQuoteCriteriaForTktDTO(BaseAvailRQ baseAvailRQ,
			FareSegChargeTO fareSegChargeTO, List<FlightSegmentTO> flightSegments, CommonReservationContactInfo contactInfo,
			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges, Map<Integer, String> paxWisePaxTypes,
			String transactionId) {

		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();

		serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
		serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(fareSegChargeTO);
		serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegments);
		serviceTaxQuoteCriteriaDTO.setPaxState(contactInfo.getState());
		serviceTaxQuoteCriteriaDTO.setPaxCountryCode(contactInfo.getCountryCode());
		serviceTaxQuoteCriteriaDTO
				.setPaxTaxRegistered((contactInfo.getTaxRegNo() != null && !"".equals(contactInfo.getTaxRegNo())) ? true : false);
		serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
		serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
		serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(transactionId);

		return serviceTaxQuoteCriteriaDTO;
	}

	private void storeServiceTaxChargesToSession(String transactionId, List<ReservationPaxTO> paxList) {
		PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo;

		AncillarySessionStore ancillarySessionStore = transactionFactory.getTransactionStore(transactionId);
		ReservationChargeTo<LCCClientExternalChgDTO> reservationChargeTo = ancillarySessionStore.getFinancialStore()
				.getPaymentExternalCharges();

		// Adding newly quoted service taxes pax wise into payment session store
		if (reservationChargeTo.getPassengers() == null || reservationChargeTo.getPassengers().isEmpty()) {
			reservationChargeTo.setPassengers(new ArrayList<PassengerChargeTo<LCCClientExternalChgDTO>>());
			for (ReservationPaxTO reservationPaxTO : paxList) {
				passengerChargeTo = new PassengerChargeTo<>();
				passengerChargeTo.setPassengerRph(reservationPaxTO.getSeqNumber().toString());
				passengerChargeTo.setGetPassengerCharges(reservationPaxTO.getExternalCharges());
				reservationChargeTo.getPassengers().add(passengerChargeTo);

			}
		} else {
			for (ReservationPaxTO reservationPaxTO : paxList) {
				boolean paxExists = false;
				for (PassengerChargeTo<LCCClientExternalChgDTO> existingPassengerChargeTo : reservationChargeTo.getPassengers()) {
					if (existingPassengerChargeTo.getPassengerRph().equals(reservationPaxTO.getSeqNumber().toString())) {
						paxExists = true;
						for (LCCClientExternalChgDTO modifiedLCCClientExternalChgDTO : reservationPaxTO.getExternalCharges()) {
							if (modifiedLCCClientExternalChgDTO.getExternalCharges()
									.equals(ReservationInternalConstants.EXTERNAL_CHARGES.SERVICE_TAX)) {
								existingPassengerChargeTo.getGetPassengerCharges().add(modifiedLCCClientExternalChgDTO);
							}
						}

					}
				}
				if (!paxExists) {
					passengerChargeTo = new PassengerChargeTo<>();
					passengerChargeTo.setPassengerRph(reservationPaxTO.getSeqNumber().toString());
					passengerChargeTo.setGetPassengerCharges(reservationPaxTO.getExternalCharges());
					reservationChargeTo.getPassengers().add(passengerChargeTo);
				}
			}
		}
		// update infant charges added to parent
		for (PassengerChargeTo<LCCClientExternalChgDTO> sessionPax : reservationChargeTo.getPassengers()) {
			for (ReservationPaxTO pax : paxList) {
				if (PaxTypeTO.INFANT.equals(pax.getPaxType())
						&& pax.getSeqNumber().equals(Integer.parseInt(sessionPax.getPassengerRph()))) {
					ReservationPaxTO parent = findParent(pax, paxList);
					if (parent != null && parent.getInfantExternalCharges() != null) {
						sessionPax.getGetPassengerCharges().addAll(parent.getInfantExternalCharges());
					}
				}
			}
		}
	}

	public ReservationPaxTO findParent(ReservationPaxTO infant, List<ReservationPaxTO> paxList) {
		ReservationPaxTO parent = null;
		for (ReservationPaxTO pax : paxList) {
			if (pax.getPaxType().equals(PaxTypeTO.ADULT) && infant.getInfantWith().equals(pax.getSeqNumber())) {
				parent = pax;
				break;
			}
		}
		return parent;
	}

	private Map<Integer, List<LCCClientExternalChgDTO>> paxWisePaymentServiceTaxes(FinancialStore financialStore) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseServiceTaxCCFee = new HashMap<>();
		if (financialStore.getPaymentExternalCharges() != null
				&& financialStore.getPaymentExternalCharges().getPassengers() != null
				&& !financialStore.getPaymentExternalCharges().getPassengers().isEmpty()) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> passengerCharges = financialStore.getPaymentExternalCharges()
					.getPassengers();
			if (passengerCharges != null && !passengerCharges.isEmpty()) {
				for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : passengerCharges) {

					if (passengerChargeTo.getPassengerRph() != null) {
						Integer paxSequence = new Integer(passengerChargeTo.getPassengerRph());
						Iterator<LCCClientExternalChgDTO> paymentChargeItr = passengerChargeTo.getGetPassengerCharges()
								.iterator();
						while (paymentChargeItr.hasNext()) {
							LCCClientExternalChgDTO charge = paymentChargeItr.next();
							if (EXTERNAL_CHARGES.SERVICE_TAX == charge.getExternalCharges()) {
								if (paxWiseServiceTaxCCFee.get(paxSequence) == null) {
									paxWiseServiceTaxCCFee.put(paxSequence, new ArrayList<>());
								}
								paxWiseServiceTaxCCFee.get(paxSequence).add(charge);

							}
						}
					}

				}
			}

		}
		return paxWiseServiceTaxCCFee;
	}

	private AncillarySummaryRS validateInsuaranceQuotation(AncillaryAvailabilityRQ request,
			LCCClientReservation lccClientReservation, AncillarySummaryRS ancillarySummaryRS, TrackInfoDTO trackInfo)
			throws ModuleException {
		boolean noTransactionIdAvailable = false;
		if (StringUtil.isNullOrEmpty(request.getTransactionId())) {
			request.setTransactionId(
					initiateSessionInformationForAncillaryModification(lccClientReservation, request.getTransactionId()));
			noTransactionIdAvailable = true;
		}
		if (!insuranceQuotation(trackInfo, request)) {
			for (AncillarySummary ancillarySummary : ancillarySummaryRS.getAncillarySummeryList()) {
				if (ancillarySummary.getAnciType().equals(AncillariesConstants.AncillaryType.INSURANCE.getCode())
						&& ancillarySummary.isAvailable()) {
					ancillarySummary.setAvailable(false);
					ancillarySummary.setEditable(false);
				}
			}
		}
		if (noTransactionIdAvailable) {
			transactionalAncillaryService.clearSession(request.getTransactionId());
			request.setTransactionId(null);
		}
		return ancillarySummaryRS;

	}

}
