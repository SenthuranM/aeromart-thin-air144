package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankSessionInfo;
import com.isa.thinair.paymentbroker.api.dto.paypal.PAYPALResponse;

/**
 * Contain reservation data from payment page and post card details page to pages after the payment gw response is
 * received
 * 
 * @author M.Rikaz
 * 
 */
public class PostPaymentDTO {

	// TODO required for payment retry, verify
	private boolean isGroupPnr = false;

	private Map temporyPaymentMap;

	private String ipgRefenceNo;

	private IPGResponseDTO ipgResponseDTO;

	private String pnr;

	private String selectedCurrency;

	private boolean responceReceived;

	private BigDecimal reservationCredit;

	private boolean noPay = false;

	private BigDecimal creditCardFee;

	@Deprecated
	private LoyaltyPaymentOption loyaltyPayOption;

	private BigDecimal loyaltyCredit;

	@Deprecated
	private Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtCharges;

	private String transactionId;

	private PAYPALResponse paypalResponse;

	private String balanceQueryData;

	private boolean securePayment3D;

	private String invoiceStatus;

	private String billId;

	private long timeToSpare;

	private boolean onHoldCreated;

	private String errorCode;

	private boolean retryingWithAnotherIPG;

	@Deprecated
	private Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo;

	private boolean hasBalanceToPay = true;

	private boolean payFortStatus;

	private boolean payFortStatusVoucher;

	private IPGRequestDTO ipgRequestDTO;

	private PaymentGateway paymentGateWay;

	private XMLResponseDTO xmlResponse;

	private Map<String, String> receiptyMap;

	private PayCurrencyDTO payCurrencyDTO;
	
	private Map<String, VoucherPaymentInfo> voucherPaymentInfoAudit;

	// TODO: temp storing when ONHOLD pnr not generated & payment collection happens @ external or 3D verification
	/*
	 * on such cases if we dont allow to create the booking then no need for this. - external -ok but 3D secure ???
	 */
	private BookingRQ bookingRQ;

	private boolean balancePayment;

	private AmeriaBankSessionInfo ameriaBankSessionInfo;
	
	private Map<Integer, List<LCCClientExternalChgDTO>> paxWiseServiceTaxCCFee;
	
	private Map<String, ServiceTaxExtChgDTO> chargeCodeWiseServiceTaxExtCharges;

	public PostPaymentDTO() {
		this.selectedExtCharges = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
	}

	public boolean isPayFortStatus() {
		return payFortStatus;
	}

	public void setPayFortStatus(boolean payFortStatus) {
		this.payFortStatus = payFortStatus;
	}

	public boolean isPayFortStatusVoucher() {
		return payFortStatusVoucher;
	}

	public void setPayFortStatusVoucher(boolean payFortStatusVoucher) {
		this.payFortStatusVoucher = payFortStatusVoucher;
	}

	/**
	 * @return the retryingWithAnotherIPG
	 */
	public boolean isRetryingWithAnotherIPG() {
		return retryingWithAnotherIPG;
	}

	/**
	 * @param retryingWithAnotherIPG
	 *            the retryingWithAnotherIPG to set
	 */
	public void setRetryingWithAnotherIPG(boolean retryingWithAnotherIPG) {
		this.retryingWithAnotherIPG = retryingWithAnotherIPG;
	}

	public Map getTemporyPaymentMap() {
		return temporyPaymentMap;
	}

	public void setTemporyPaymentMap(Map temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	public String getIpgRefenceNo() {
		return ipgRefenceNo;
	}

	public void setIpgRefenceNo(String ipgRefenceNo) {
		this.ipgRefenceNo = ipgRefenceNo;
	}

	public IPGResponseDTO getIpgResponseDTO() {
		return ipgResponseDTO;
	}

	public void setIpgResponseDTO(IPGResponseDTO ipgResponseDTO) {
		this.ipgResponseDTO = ipgResponseDTO;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPnr() {
		return this.pnr;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public boolean isResponseReceived() {
		return responceReceived;
	}

	public void setResponseReceived(boolean responseReceived) {
		this.responceReceived = responseReceived;
	}

	public BigDecimal getReservationCredit() {
		return reservationCredit;
	}

	public void setReservationCredit(BigDecimal reservationCredit) {
		this.reservationCredit = reservationCredit;
	}

	public boolean isNoPay() {
		return noPay;
	}

	public void setNoPay(boolean noPay) {
		this.noPay = noPay;
	}

	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public LoyaltyPaymentOption getLoyaltyPayOption() {
		return loyaltyPayOption;
	}

	public void setLoyaltyPayOption(LoyaltyPaymentOption loyaltyPayOption) {
		this.loyaltyPayOption = loyaltyPayOption;
	}

	public BigDecimal getLoyaltyCredit() {
		return loyaltyCredit;
	}

	public void setLoyaltyCredit(BigDecimal loyaltyCredit) {
		this.loyaltyCredit = loyaltyCredit;
	}

	public void addExternalCharge(ExternalChgDTO ccChgDTO) {
		this.selectedExtCharges.put(ccChgDTO.getExternalChargesEnum(), ccChgDTO);
	}

	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getSelectedExternalCharges() {
		return this.selectedExtCharges;
	}

	public void removeExternalCharge(EXTERNAL_CHARGES extChgEnum) {
		this.selectedExtCharges.remove(extChgEnum);
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the paypalResponse
	 */
	public PAYPALResponse getPaypalResponse() {
		return paypalResponse;
	}

	/**
	 * @param paypalResponse
	 *            the paypalResponse to set
	 */
	public void setPaypalResponse(PAYPALResponse paypalResponse) {
		this.paypalResponse = paypalResponse;
	}

	public boolean isSecurePayment3D() {
		return securePayment3D;
	}

	public void setSecurePayment3D(boolean securePayment3D) {
		this.securePayment3D = securePayment3D;
	}

	public String getBalanceQueryData() {
		return balanceQueryData;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	/**
	 * @return the timeToSpare
	 */
	public long getTimeToSpare() {
		return timeToSpare;
	}

	/**
	 * @param timeToSpare
	 *            the timeToSpare to set
	 */
	public void setTimeToSpare(long timeToSpare) {
		this.timeToSpare = timeToSpare;
	}

	/**
	 * @return the invoiceStatus
	 */
	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	/**
	 * @param invoiceStatus
	 *            the invoiceStatus to set
	 */
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	/**
	 * @return the billId
	 */
	public String getBillId() {
		return billId;
	}

	/**
	 * @param billId
	 *            the billId to set
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	/**
	 * @return the onHoldCreated
	 */
	public boolean isOnHoldCreated() {
		return onHoldCreated;
	}

	/**
	 * @param onHoldCreated
	 *            the onHoldCreated to set
	 */
	public void setOnHoldCreated(boolean onHoldCreated) {
		this.onHoldCreated = onHoldCreated;
	}

	public void setBalanceToPay(boolean hasBalanceToPay) {
		this.hasBalanceToPay = hasBalanceToPay;
	}

	public boolean hasNoCCPayment() {
		return getLoyaltyPayOption() == LoyaltyPaymentOption.TOTAL || !hasBalanceToPay;
	}

	public Map<String, LoyaltyPaymentInfo> getCarrierWiseLoyaltyPaymentInfo() {
		return carrierWiseLoyaltyPaymentInfo;
	}

	public void setCarrierWiseLoyaltyPaymentInfo(Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo) {
		this.carrierWiseLoyaltyPaymentInfo = carrierWiseLoyaltyPaymentInfo;
	}

	public LoyaltyPaymentInfo getCarrierLoyaltyPaymentInfo(String carrierCode) {
		if (carrierWiseLoyaltyPaymentInfo != null && carrierWiseLoyaltyPaymentInfo.containsKey(carrierCode)) {
			return carrierWiseLoyaltyPaymentInfo.get(carrierCode);
		}

		return null;
	}

	public Map<String, String> getReceiptyMap() {
		return receiptyMap;
	}

	public void setReceiptyMap(Map<String, String> receiptyMap) {
		this.receiptyMap = receiptyMap;
	}

	public IPGRequestDTO getIpgRequestDTO() {
		return ipgRequestDTO;
	}

	public void setIpgRequestDTO(IPGRequestDTO ipgRequestDTO) {
		this.ipgRequestDTO = ipgRequestDTO;
	}

	public PaymentGateway getPaymentGateWay() {
		return paymentGateWay;
	}

	public void setPaymentGateWay(PaymentGateway paymentGateWay) {
		this.paymentGateWay = paymentGateWay;
	}

	public XMLResponseDTO getXmlResponse() {
		return xmlResponse;
	}

	public void setXmlResponse(XMLResponseDTO xmlResponse) {
		this.xmlResponse = xmlResponse;
	}

	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	public BookingRQ getBookingRQ() {
		return bookingRQ;
	}

	public void setBookingRQ(BookingRQ bookingRQ) {
		this.bookingRQ = bookingRQ;
	}

	public boolean isBalancePayment() {
		return balancePayment;
	}

	public void setBalancePayment(boolean balancePayment) {
		this.balancePayment = balancePayment;
	}

	public boolean isGroupPnr() {
		return isGroupPnr;
	}

	public void setGroupPnr(boolean isGroupPnr) {
		this.isGroupPnr = isGroupPnr;
	}

	public AmeriaBankSessionInfo getAmeriaBankSessionInfo() {
		return ameriaBankSessionInfo;
	}

	public void setAmeriaBankSessionInfo(AmeriaBankSessionInfo ameriaBankSessionInfo) {
		this.ameriaBankSessionInfo = ameriaBankSessionInfo;
	}

	public Map<Integer, List<LCCClientExternalChgDTO>> getPaxWiseServiceTaxCCFee() {
		if(paxWiseServiceTaxCCFee == null){
			paxWiseServiceTaxCCFee = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		}
		return paxWiseServiceTaxCCFee;
	}

	public void setPaxWiseServiceTaxCCFee(Map<Integer, List<LCCClientExternalChgDTO>> paxWiseServiceTaxCCFee) {
		this.paxWiseServiceTaxCCFee = paxWiseServiceTaxCCFee;
	}

	public Map<String, ServiceTaxExtChgDTO> getChargeCodeWiseServiceTaxExtCharges() {
		if(chargeCodeWiseServiceTaxExtCharges == null){
			chargeCodeWiseServiceTaxExtCharges = new HashMap<String, ServiceTaxExtChgDTO>();
		}
		return chargeCodeWiseServiceTaxExtCharges;
	}

	public void setChargeCodeWiseServiceTaxExtCharges(Map<String, ServiceTaxExtChgDTO> chargeCodeWiseServiceTaxExtCharges) {
		this.chargeCodeWiseServiceTaxExtCharges = chargeCodeWiseServiceTaxExtCharges;
	}

	public Map<String, VoucherPaymentInfo> getVoucherPaymentInfoAudit() {
		return voucherPaymentInfoAudit;
	}

	public void setVoucherPaymentInfoAudit(Map<String, VoucherPaymentInfo> voucherPaymentInfoAudit) {
		this.voucherPaymentInfoAudit = voucherPaymentInfoAudit;
	}
}