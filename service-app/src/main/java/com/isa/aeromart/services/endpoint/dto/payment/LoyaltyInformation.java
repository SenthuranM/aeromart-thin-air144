package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;

//FIXME: consider renaming to LMSInformation
public class LoyaltyInformation {
	private LoyaltyPaymentOption loyaltyPaymentOption = LoyaltyPaymentOption.NONE;

	private Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo;

	private boolean redeemLoyaltyPoints;

	private BigDecimal totalRedeemedAmount;
	
	private boolean binPromotion;

	public LoyaltyPaymentOption getLoyaltyPaymentOption() {
		return loyaltyPaymentOption;
	}

	public Map<String, LoyaltyPaymentInfo> getCarrierWiseLoyaltyPaymentInfo() {
		return carrierWiseLoyaltyPaymentInfo;
	}

	public boolean isRedeemLoyaltyPoints() {
		return redeemLoyaltyPoints;
	}

	public BigDecimal getTotalRedeemedAmount() {
		return totalRedeemedAmount;
	}

	public void setLoyaltyPaymentOption(LoyaltyPaymentOption loyaltyPaymentOption) {
		this.loyaltyPaymentOption = loyaltyPaymentOption;
	}

	public void setCarrierWiseLoyaltyPaymentInfo(Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo) {
		this.carrierWiseLoyaltyPaymentInfo = carrierWiseLoyaltyPaymentInfo;
	}

	public void setRedeemLoyaltyPoints(boolean redeemLoyaltyPoints) {
		this.redeemLoyaltyPoints = redeemLoyaltyPoints;
	}

	public void setTotalRedeemedAmount(BigDecimal totalRedeemedAmount) {
		this.totalRedeemedAmount = totalRedeemedAmount;
	}

	public boolean isBinPromotion() {
		return binPromotion;
	}

	public void setBinPromotion(boolean binPromotion) {
		this.binPromotion = binPromotion;
	}

	}
