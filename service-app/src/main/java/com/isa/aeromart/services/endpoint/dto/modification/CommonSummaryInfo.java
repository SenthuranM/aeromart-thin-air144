package com.isa.aeromart.services.endpoint.dto.modification;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CommonSummaryInfo {

	private BigDecimal amountDue = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal creditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal paidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal refundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal nonRefundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private ChargeSummaryInfo chargeInfo;

	private StateSummaryInfo preModifiedSummary;

	private StateSummaryInfo postModifiedSummary;

	public BigDecimal getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(BigDecimal amountDue) {
		this.amountDue = amountDue;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getRefundableAmount() {
		return refundableAmount;
	}

	public void setRefundableAmount(BigDecimal refundableAmount) {
		this.refundableAmount = refundableAmount;
	}

	public BigDecimal getNonRefundableAmount() {
		return nonRefundableAmount;
	}

	public void setNonRefundableAmount(BigDecimal nonRefundableAmount) {
		this.nonRefundableAmount = nonRefundableAmount;
	}

	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public ChargeSummaryInfo getChargeInfo() {
		return chargeInfo;
	}

	public void setChargeInfo(ChargeSummaryInfo chargeinfo) {
		this.chargeInfo = chargeinfo;
	}

	public StateSummaryInfo getPreModifiedSummary() {
		return preModifiedSummary;
	}

	public void setPreModifiedSummary(StateSummaryInfo preModifiedSummary) {
		this.preModifiedSummary = preModifiedSummary;
	}

	public StateSummaryInfo getPostModifiedSummary() {
		return postModifiedSummary;
	}

	public void setPostModifiedSummary(StateSummaryInfo postModifiedSummary) {
		this.postModifiedSummary = postModifiedSummary;
	}

}
