package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.DocInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;

public class DocInfoResAdaptor implements Adaptor<LCCClientReservationAdditionalPax, DocInfo> {

	@Override
	public DocInfo adapt(LCCClientReservationAdditionalPax lccClientReservationAdditionalPax) {
		DocInfo visaInfo = new DocInfo();
		visaInfo.setTravelDocumentType(lccClientReservationAdditionalPax.getTravelDocumentType());
		visaInfo.setApplicableCountry(lccClientReservationAdditionalPax.getVisaApplicableCountry());
		visaInfo.setDocIssueDate(lccClientReservationAdditionalPax.getVisaDocIssueDate());
		visaInfo.setDocNumber(lccClientReservationAdditionalPax.getVisaDocNumber());
		visaInfo.setDocPlaceOfIssue(lccClientReservationAdditionalPax.getVisaDocPlaceOfIssue());
		return visaInfo;
	}

}
