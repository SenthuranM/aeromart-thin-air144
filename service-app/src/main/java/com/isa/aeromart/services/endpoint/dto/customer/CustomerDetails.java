package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;

public class CustomerDetails {
	
	private String title;
	
	private String firstName;
	
	private String lastName;
	
	private String customerID;
    
    private String password; 

    private String gender;  

    private Date dateOfBirth;
    
    private String countryCode;
    
    private int nationalityCode;
    
    private String status = null;
    
    private String secretQuestion;

    private String secretAnswer; 

    private Date registrationDate = null;

    private Date confirmationDate = null;

    private Set<CustomerQuestionaireMap> customerQuestionaireMap;

    private Set<SocialCustomer> socialCustomer;

    private CustomerContact customerContact;
    
    private Boolean sendPromoEmail;

    private List<CustomerPreferredMealDTO> cutomerPreferredMealList;
    
    private String customerPreferredSeatType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSecretQuestion() {
		return secretQuestion;
	}

	public void setSecretQuestion(String secretQuestion) {
		this.secretQuestion = secretQuestion;
	}

	public String getSecretAnswer() {
		return secretAnswer;
	}

	public void setSecretAnswer(String secretAnswer) {
		this.secretAnswer = secretAnswer;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public Set<CustomerQuestionaireMap> getCustomerQuestionaire() {
		return customerQuestionaireMap;
	}

	public void setCustomerQuestionaire(Set<CustomerQuestionaireMap> customerQuestionaire) {
		this.customerQuestionaireMap = customerQuestionaire;
	}

	public Set<SocialCustomer> getSocialCustomer() {
		return socialCustomer;
	}

	public void setSocialCustomer(Set<SocialCustomer> socialCustomer) {
		this.socialCustomer = socialCustomer;
	}

	public CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContact customerContact) {
		this.customerContact = customerContact;
	}

	public Boolean getSendPromoEmail() {
		return sendPromoEmail;
	}

	public void setSendPromoEmail(Boolean sendPromoEmail) {
		this.sendPromoEmail = sendPromoEmail;
	}

	public List<CustomerPreferredMealDTO> getCutomerPreferredMealList() {
		return cutomerPreferredMealList;
	}

	public void setCutomerPreferredMealList(
			List<CustomerPreferredMealDTO> cutomerPreferredMealList) {
		this.cutomerPreferredMealList = cutomerPreferredMealList;
	}

	public String getCustomerPreferredSeatType() {
		return customerPreferredSeatType;
	}

	public void setCustomerPreferredSeatType(String customerPreferredSeatType) {
		this.customerPreferredSeatType = customerPreferredSeatType;
	}

}
