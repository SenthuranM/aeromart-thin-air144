package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.List;

public class PaymentGateway {

	private int gatewayId;
	private String providerName;// payOptRes
	private String paymentCurrency;// payOptRes
	private String providerCode;// payOptRes
	private String description;// payOptRes
	private String brokerType;// payOptRes
	private boolean saveCard;
	private boolean switchToExternalUrl;// payOptRes
	private boolean viewPaymentInIframe;// payOptRes
	private int cvvOption;// payOptRes
	private AmountObject effectivePaymentAmount;// payOptRes, effMulPayRes
	private AmountObject transactionFee;// payOptRes, effMulPayRes, effPayRes
	private List<Card> cards;
	private BigDecimal paymentAmount;// effPayReq
	private BigDecimal transactionFeeValue;
	private boolean transactionFeeInPercentage;

	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrokerType() {
		return brokerType;
	}

	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	public boolean isSwitchToExternalUrl() {
		return switchToExternalUrl;
	}

	public void setSwitchToExternalUrl(boolean switchToExternalUrl) {
		this.switchToExternalUrl = switchToExternalUrl;
	}

	public boolean isViewPaymentInIframe() {
		return viewPaymentInIframe;
	}

	public void setViewPaymentInIframe(boolean viewPaymentInIframe) {
		this.viewPaymentInIframe = viewPaymentInIframe;
	}

	public int isCvvOption() {
		return cvvOption;
	}

	public void setCvvOption(int cvvOption) {
		this.cvvOption = cvvOption;
	}

	public AmountObject getEffectivePaymentAmount() {
		return effectivePaymentAmount;
	}

	public void setEffectivePaymentAmount(AmountObject effectivePaymentAmount) {
		this.effectivePaymentAmount = effectivePaymentAmount;
	}

	public AmountObject getTransactionFee() {
		return transactionFee;
	}

	public void setTransactionFee(AmountObject transactionFee) {
		this.transactionFee = transactionFee;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}


	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public boolean isSaveCard() {
		return saveCard;
	}

	public void setSaveCard(boolean saveCard) {
		this.saveCard = saveCard;
	}

	public boolean isTransactionFeeInPercentage() {
		return transactionFeeInPercentage;
	}

	public void setIsTransactionFeeInPercentage(boolean isTransactionFeeInPercentage) {
		this.transactionFeeInPercentage = isTransactionFeeInPercentage;
	}

	public BigDecimal getTransactionFeeValue() {
		return transactionFeeValue;
	}

	public void setTransactionFeeValue(BigDecimal transactionFeeValue) {
		this.transactionFeeValue = transactionFeeValue;
	}
}
