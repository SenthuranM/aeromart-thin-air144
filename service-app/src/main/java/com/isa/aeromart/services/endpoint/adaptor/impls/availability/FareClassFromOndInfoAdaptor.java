package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.dto.availability.FareClass;
import com.isa.aeromart.services.endpoint.utils.common.RPHBuilder;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FareClassFromOndInfoAdaptor implements Adaptor<FlightFareSummaryTO, FareClass> {

	@Override
	public FareClass adapt(FlightFareSummaryTO source) {
		FareClass target = new FareClass();
		target.setRank(source.getRank());
		target.setComment(source.getComment());
		target.setDescription(source.getLogicalCCDesc());
		target.setTemplateDTO(source.getBundleFareDescriptionTemplateDTO());
		if (source.getBundledFarePeriodId() != null) {
			target.setFareClassId(source.getBundledFarePeriodId());
			target.setFareClassType(FareClassType.BUNDLE);
			target.setAdditionalFareFee(source.getBundleFareFee());
		} else if (source.isWithFlexi() && AppSysParamsUtil.isDisplayFlexiAsSeparteBundle()) {
			target.setFareClassId(source.getFlexiRuleId());
			target.setFareClassType(FareClassType.FLEXI);
		} else {
			target.setFareClassType(FareClassType.DEFAULT);
		}
		target.setImageUrl(source.getImageUrl());
		target.setFareClassCode(RPHBuilder.FareClassCodeBuilder(source));
		target.setLogicalCabinClass(source.getLogicalCCCode());
		target.setFreeServices(source.getBundledFareFreeServiceNames());
		return target;
	}

}
