package com.isa.aeromart.services.endpoint.dto.payment.tapOffline;

public class TapOfflinePaymentConfirmResponse {
	
	private boolean success;
	
	private String message;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
	
	
}
