package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;


public class BlockAncillaryRS extends TransactionalBaseRS{
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
