package com.isa.aeromart.services.endpoint.controller.payment;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.IPGTransactionRSAdaptor;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.payment.ConfirmPaymentServiceAdapter;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.booking.IPGTransactionResult;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.payment.IPGHandlerRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentConfirmationRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.store.GiftVoucherSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.promotion.api.service.VoucherBD;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "voucher/payment")
public class ConfirmVoucherPaymentController extends StatefulController {

	// This class needs to be refactored
	// Most of the code is duplicated with the confirm payment controller of the booking flow

	private static final String URL_ENCODING = "UTF-8";

	private static Log log = LogFactory.getLog(ConfirmVoucherPaymentController.class);

	@Autowired private ConfirmPaymentServiceAdapter confirmPaymentService;

	@RequestMapping(value = "/redirect", method = RequestMethod.POST)
	public String redirectPOST()
			throws UnsupportedEncodingException {
		log.debug("Handle Web redirect Post");
		return handlePostRedirect(PaymentConsts.WEB_VOUCHER_PURCHASE_FORWARD_URL_SUFFIX);
	}

	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	public String redirectGet()
			throws UnsupportedEncodingException {
		log.debug("Handle Web redirect get");
		return handleGetRedirect(PaymentConsts.WEB_VOUCHER_PURCHASE_FORWARD_URL_SUFFIX);
	}

	private String handlePostRedirect(String redirectSuffix) throws UnsupportedEncodingException {

		log.debug("POST /redirect: received message, content length: " + getRequest().getContentLength());

		String postURLattributes = getReceiptMapString(getRequest());

		log.debug("POST /redirect: received message, content\n" + postURLattributes +
				PaymentConstants.IPG_SESSION_ID.toString() + ":" + getRequest().getSession().getId());

		String RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + redirectSuffix + '?' + postURLattributes;

		String returnString = "redirect:" + RETURN_URL;

		log.debug("POST /redirect: redirecting\n" + returnString);
		return returnString;
	}

	private String handleGetRedirect(String redirectSuffix) {
		log.debug("GET /redirect: received/n" + getRequest().getQueryString() + PaymentConstants.IPG_SESSION_ID.toString() + ":"
				+ getRequest().getSession().getId());
		String RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + redirectSuffix + '?' + getRequest().getQueryString();

		String returnString = "redirect:" + RETURN_URL;

		log.debug("GET /redirect: redirecting\n" + returnString);

		return returnString;
	}

	private String getReceiptMapString(HttpServletRequest request) throws UnsupportedEncodingException {
		StringBuffer postURLattributes = new StringBuffer();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements(); ) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);
			fieldValue = URLEncoder.encode(fieldValue, PaymentConstants.UTF_8);

			//fieldValue=fieldValue.replaceAll(PaymentConstants.HASH, PaymentConstants.HASH_TEXT);

			if (fieldValue != null && fieldValue.length() > 0) {
				postURLattributes.append(fieldName).append("=").append(fieldValue).append("&");
			}
		}
		postURLattributes.append(PaymentConstants.IPG_SESSION_ID).append("=").append(request.getSession().getId());
		return postURLattributes.toString();
	}

	@ResponseBody @RequestMapping(value = "/confirmation",
			consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public TransactionalBaseRS confirmVoucherPurchase(
			@RequestBody PaymentConfirmationRQ paymentConfirmationRQ) throws ModuleException, UnsupportedEncodingException {
		log.debug("inside payment receipt confirmation, create flow");
		GiftVoucherSessionStore voucherSessionStore = getPaymentSessionStore(paymentConfirmationRQ.getTransactionId());

		PaymentConfirmationRS paymentConfirmationResponse = null;
		try {
			paymentConfirmationResponse = validatePaymentReceipt(paymentConfirmationRQ, voucherSessionStore);
		} catch (ModuleException me) {
			log.error("Error occured for voucher payment confirmation", me);
			me.setErrorType(CommonsConstants.ErrorType.PAYMENT_ERROR);
			throw me;
		}

		MakePaymentRS response = new MakePaymentRS();
		PostPaymentDTO postPaymentDTO = null;
		if (voucherSessionStore != null) {
			postPaymentDTO = voucherSessionStore.getPostPaymentInformation();
		}

		if (paymentConfirmationResponse != null && paymentConfirmationResponse.isSuccess()) {
			response.setSuccess(true);
			if (BookingRS.PaymentStatus.SUCCESS.equals(paymentConfirmationResponse.getPaymentStatus())) {
				response.setPaymentStatus(BookingRS.PaymentStatus.SUCCESS);
				log.info("payment success confirm the vouchers");

				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();

				try {
					voucherDelegate.issueGiftVouchersFromIBE(voucherSessionStore.getGiftVouchers());
				} catch (ModuleException exception) {
					log.error("Error occurred creating Vouchers", exception);
					exception.setErrorType(CommonsConstants.ErrorType.VOUCHER_CREATION_FAILED);
					throw exception;
				}

			} else if (BookingRS.PaymentStatus.ALREADY_COMPLETED.equals(paymentConfirmationResponse.getPaymentStatus())) {
				response.setPaymentStatus(BookingRS.PaymentStatus.ALREADY_COMPLETED);
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS);
			} else {
				response.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
				response.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_FAILED);
			}
			response.setVoucherGroupId(voucherSessionStore.getGiftVouchers().get(0).getVoucherGroupId());
		}

		response.setIpgTransactionResult(getIPGTransactionResult(
				getPaymentSessionStore(paymentConfirmationRQ.getTransactionId()).getPostPaymentInformation()));

		voucherSessionStore.clearSessionStore();
		terminateTransaction(paymentConfirmationRQ.getTransactionId());

		return response;
	}

	private IPGTransactionResult getIPGTransactionResult(PostPaymentDTO postPaymentInformation) {

		if (postPaymentInformation != null && postPaymentInformation.getIpgResponseDTO() != null
				&& postPaymentInformation.getIpgResponseDTO().getIpgTransactionResultDTO() != null) {
			IPGTransactionRSAdaptor ipgTransactionRSAdaptor = new IPGTransactionRSAdaptor();
			return ipgTransactionRSAdaptor.adapt(postPaymentInformation.getIpgResponseDTO().getIpgTransactionResultDTO());
		}
		return null;

	}

	private PaymentConfirmationRS validatePaymentReceipt(PaymentConfirmationRQ paymentConfirmationRQ,
			GiftVoucherSessionStore sessionStore) throws ModuleException {
		IPGHandlerRQ ipgHandlerRequest = new IPGHandlerRQ();
		try {
			if (paymentConfirmationRQ.getPaymentReceiptMap() == null || paymentConfirmationRQ.getPaymentReceiptMap().isEmpty()) {
				log.debug("Building Receipt map from PostParam");
				paymentConfirmationRQ.setPaymentReceiptMap(splitPaymentReceiptMap(paymentConfirmationRQ.getPostParam()));

			}
		} catch (UnsupportedEncodingException ux) {
			log.error("Invalid post payment parameters", ux);
			throw new ModuleException("payment.api.invalid.post.param.data");
		}

		if (paymentConfirmationRQ.getPaymentReceiptMap() == null || paymentConfirmationRQ.getPaymentReceiptMap().isEmpty()) {
			log.error("Invalid receipt map and PostParam, both empty or null");
			throw new ModuleException("payment.api.invalid.post.param.data");
		}

		PostPaymentDTO postPaymentDTO = sessionStore.getPostPaymentInformation();

		postPaymentDTO.setReceiptyMap(paymentConfirmationRQ.getPaymentReceiptMap());

		ipgHandlerRequest.setPostPaymentDTO(postPaymentDTO);
		PaymentConfirmationRS paymentConfirmationRS = confirmPaymentService
				.processPaymentReceiptForGiftVoucher(paymentConfirmationRQ.getTransactionId(), ipgHandlerRequest, getTrackInfo());
		return paymentConfirmationRS;
	}

	private GiftVoucherSessionStore getPaymentSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private Map<String, String> splitPaymentReceiptMap(String uriParam) throws UnsupportedEncodingException {
		Map<String, String> paymentReceiptMap = new HashMap<>();
		if (!StringUtil.isNullOrEmpty(uriParam)) {
			String[] pairs = uriParam.split("&");
			for (String pair : pairs) {
				int idx = pair.indexOf("=");
				paymentReceiptMap.put(URLDecoder.decode(pair.substring(0, idx), URL_ENCODING),
						URLDecoder.decode(pair.substring(idx + 1), URL_ENCODING));

			}
		} else {
			log.info("PostParam is null or empty");
		}
		return paymentReceiptMap;
	}

}
