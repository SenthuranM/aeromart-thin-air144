package com.isa.aeromart.services.endpoint.dto;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class UpdateAncillaryRS extends TransactionalBaseRS {

	private Date firstDepartureDate;

	private String contactLastName;

	public Date getFirstDepartureDate() {
		return firstDepartureDate;
	}

	public void setFirstDepartureDate(Date firstDepartureDate) {
		this.firstDepartureDate = firstDepartureDate;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

}
