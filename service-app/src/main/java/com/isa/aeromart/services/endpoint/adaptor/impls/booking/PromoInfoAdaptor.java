package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.PromoInfoRS;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;

public class PromoInfoAdaptor implements Adaptor<LCCPromotionInfoTO, PromoInfoRS> {

	private LCCClientReservation reservation;

	public PromoInfoAdaptor(LCCClientReservation reservation) {
		this.reservation = reservation;
	}

	@Override
	public PromoInfoRS adapt(LCCPromotionInfoTO lccPromoInfoTO) {
		PromoInfoRS promoInfo = new PromoInfoRS();
		promoInfo.setCancellable(lccPromoInfoTO.isCancellationAllowed());
		promoInfo.setModifiable(lccPromoInfoTO.isModificationAllowed());
		promoInfo.setPromoType(lccPromoInfoTO.getPromoType());
		promoInfo.setValue(lccPromoInfoTO.getDiscountValue());
		promoInfo.setAppliedTo(lccPromoInfoTO.getDiscountApplyTo());
		promoInfo.setDiscountType(lccPromoInfoTO.getDiscountType());
		promoInfo.setAppliedChilds(lccPromoInfoTO.getAppliedChilds());
		promoInfo.setAppliedAdults(lccPromoInfoTO.getAppliedAdults());
		promoInfo.setAppliedInfants(lccPromoInfoTO.getAppliedInfants());
		promoInfo.setApplicableBINs(lccPromoInfoTO.getApplicableBINs());
		promoInfo.setAppliedAncis(lccPromoInfoTO.getAppliedAncis());
		promoInfo.setAppliedTotal(getDiscountAmount(reservation, lccPromoInfoTO).floatValue());
		if (DiscountApplyAsTypes.CREDIT.equals(lccPromoInfoTO.getDiscountAs())) {
			promoInfo.setCreditPromo(true);
		} else if (DiscountApplyAsTypes.MONEY.equals(lccPromoInfoTO.getDiscountAs())) {
			promoInfo.setMoneyPromo(true);
		}
		return promoInfo;
	}

	private static BigDecimal getDiscountAmount(LCCClientReservation lccClientReservation,
			LCCPromotionInfoTO lccPromotionInfoTO) {
		BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (lccClientReservation.getTotalDiscount() != null && !AccelAeroCalculator
				.isEqual(lccClientReservation.getTotalDiscount(), AccelAeroCalculator.getDefaultBigDecimalZero())) {
			BigDecimal dispTotalDiscount = lccClientReservation.getTotalDiscount();
			if (AccelAeroCalculator.isLessThan(dispTotalDiscount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				dispTotalDiscount = dispTotalDiscount.negate();
			}

			if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
				discount = dispTotalDiscount;
			} else {
				discount = dispTotalDiscount.negate();
			}
		} else if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())
				&& lccPromotionInfoTO.getCreditDiscountAmount() != null
				&& !lccPromotionInfoTO.getCreditDiscountAmount().equals(BigDecimal.ZERO)) {

			BigDecimal dispTotalDiscount = lccPromotionInfoTO.getCreditDiscountAmount();
			if (AccelAeroCalculator.isLessThan(dispTotalDiscount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				dispTotalDiscount = dispTotalDiscount.negate();
			}

			discount = dispTotalDiscount;
		}
		return discount.abs();
	}

}
