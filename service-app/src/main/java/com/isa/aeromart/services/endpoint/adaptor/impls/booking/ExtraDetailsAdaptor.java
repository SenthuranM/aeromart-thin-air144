package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.ExtraDetails;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.commons.core.util.StringUtil;


public class ExtraDetailsAdaptor implements Adaptor<LCCSelectedSegmentAncillaryDTO, ExtraDetails>{

	@Override
	public ExtraDetails adapt(LCCSelectedSegmentAncillaryDTO source) {
		
		ExtraDetails extraDetails = new ExtraDetails(); 
		String meals = "";

		extraDetails.setFlightSegmentCode(source.getFlightSegmentTO().getSegmentCode());
		extraDetails.setFlightDesignator(source.getFlightSegmentTO().getFlightNumber());
		
		if (source.getMealDTOs() != null && !source.getMealDTOs().isEmpty()) {
			for(LCCMealDTO meal : source.getMealDTOs()){
				meals += meal.getSoldMeals() + "X " + meal.getMealName() + " ";
			}
			
			extraDetails.setMeal(meals);
		}
		
		if (source.getBaggageDTOs() != null && !source.getBaggageDTOs().isEmpty()) {
			extraDetails.setBaggage(source.getBaggageDTOs().get(0).getBaggageDescription());
		}
		
		if(source.getAirSeatDTO() != null){
			extraDetails.setSeat(source.getAirSeatDTO().getSeatNumber());
		}
		
		populateSpecialServices(source, extraDetails);
		populateAirportServices(source, extraDetails);
		populateAirportTransfer(source, extraDetails);
		populateAutoCheckin(source, extraDetails);
		return extraDetails;
	}

	/**
	 * This is used to populate selected auto checkin details
	 * 
	 * @param source
	 * @param extraDetails
	 */
	private void populateAutoCheckin(LCCSelectedSegmentAncillaryDTO source, ExtraDetails extraDetails) {
		if (source.getAutomaticCheckinDTOs() != null && !source.getAutomaticCheckinDTOs().isEmpty()) {
			for (LCCAutomaticCheckinDTO automaticCheckin : source.getAutomaticCheckinDTOs()) {
				if (StringUtil.isNullOrEmpty(automaticCheckin.getSeatPref())) {
					extraDetails.setAutomaticCheckin(automaticCheckin.getSeatCode());
				} else {
					extraDetails.setAutomaticCheckin(automaticCheckin.getSeatPref());
				}
			}
		}

	}

	private void populateAirportServices(LCCSelectedSegmentAncillaryDTO source, ExtraDetails extraDetails) {
		if (source.getAirportServiceDTOs() != null && !source.getAirportServiceDTOs().isEmpty()) {
			for (LCCAirportServiceDTO airportService : source.getAirportServiceDTOs()) {
				extraDetails.setAirprotServices(extraDetails.getAirprotServices() + airportService.getSsrName() + ", ");
			}
			if (extraDetails.getAirprotServices() != null && !("").equals(extraDetails.getAirprotServices())) {
				extraDetails.setAirprotServices(
						extraDetails.getAirprotServices().substring(0, extraDetails.getAirprotServices().length() - 2));
			}
		}
	}

	private void populateSpecialServices(LCCSelectedSegmentAncillaryDTO source, ExtraDetails extraDetails) {
		if (source.getSpecialServiceRequestDTOs() != null && !source.getSpecialServiceRequestDTOs().isEmpty()) {
			for (LCCSpecialServiceRequestDTO specialService : source.getSpecialServiceRequestDTOs()) {
				extraDetails.setSpecialServices(extraDetails.getSpecialServices() + specialService.getDescription() + ", ");
			}
			if (extraDetails.getSpecialServices() != null && !("").equals(extraDetails.getSpecialServices())) {
				extraDetails.setSpecialServices(
						extraDetails.getSpecialServices().substring(0, extraDetails.getSpecialServices().length() - 2));
			}
		}
	}
	
	private void populateAirportTransfer(LCCSelectedSegmentAncillaryDTO source, ExtraDetails extraDetails) {
		if (source.getAirportTransferDTOs() != null && !source.getAirportTransferDTOs().isEmpty()) {
			for (LCCAirportServiceDTO airportTransfer : source.getAirportTransferDTOs()) {
				extraDetails.setAirprotTransfer(extraDetails.getAirprotTransfer() + airportTransfer.getAirportTransferId() + ":" + airportTransfer.getTranferDate() + ":" + airportTransfer.getTransferAddress() + ":" + airportTransfer.getTransferContact() + ", ");
			}
			if (extraDetails.getAirprotTransfer() != null && !("").equals(extraDetails.getAirprotTransfer())) {
				extraDetails.setAirprotTransfer(
						extraDetails.getAirprotTransfer().substring(0, extraDetails.getAirprotTransfer().length() - 2));
			}
		}
	}

}
