package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.BookingInfoTO;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.StringUtil;

public class BookingInfoResAdaptor implements Adaptor<LCCClientReservation, BookingInfoTO> {

	@Override
	public BookingInfoTO adapt(LCCClientReservation reservation) {
		BookingInfoTO bookingInfoTO = new BookingInfoTO();
		StringBuilder insurancePolicies = new StringBuilder();
		bookingInfoTO.setBookingDate(reservation.getZuluBookingTimestamp());
		// bookingInfoTO.setExpiryDays(reservation.getAutoCancellationInfo().get);AvailabilityController
		bookingInfoTO.setPnr(reservation.getPNR());
		if (reservation.getPreferrenceInfo() != null) {
			bookingInfoTO.setPrefferedLanguage(reservation.getPreferrenceInfo().getPreferredLanguage());
		}

		if (reservation.getTotalAvailableBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			bookingInfoTO.setBalanceAvailable(true);
		}

		bookingInfoTO.setGroupPnr(reservation.isGroupPNR());
		bookingInfoTO.setReleaseDate(reservation.getZuluReleaseTimeStamp());
		bookingInfoTO.setStatus(reservation.getStatus());
		TravellerQuantity travellerQuantity = new TravellerQuantity();
		travellerQuantity.setAdultCount(reservation.getTotalPaxAdultCount());
		travellerQuantity.setChildCount(reservation.getTotalPaxChildCount());
		travellerQuantity.setInfantCount(reservation.getTotalPaxInfantCount());
		bookingInfoTO.setTravelerQuantity(travellerQuantity);
		bookingInfoTO.setVersion(reservation.getVersion());

		for (LCCClientReservationInsurance insurance : reservation.getReservationInsurances()) {
			if (!StringUtil.isNullOrEmpty(insurance.getPolicyCode())) {
				insurancePolicies.append(insurance.getPolicyCode());
				insurancePolicies.append(" , ");
			}
		}

		if (!StringUtil.isNullOrEmpty(insurancePolicies.toString())) {
			bookingInfoTO.setPolicyCode(insurancePolicies.substring(0, insurancePolicies.length() - 3));
		}

		return bookingInfoTO;
	}
}
