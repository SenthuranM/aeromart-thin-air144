package com.isa.aeromart.services.endpoint.dto.modification;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class NameChangeRQ extends TransactionalBaseRQ {

	private boolean nameChangeRequote;

	private boolean groupPnr;

	@NotNull
	private String pnr;
	@NotNull
	private List<NameChangePassenger> nameChangePassengerList;

	/**
	 * @return the nameChangeRequote
	 */
	public boolean isNameChangeRequote() {
		return nameChangeRequote;
	}

	/**
	 * @param nameChangeRequote
	 *            the nameChangeRequote to set
	 */
	public void setNameChangeRequote(boolean nameChangeRequote) {
		this.nameChangeRequote = nameChangeRequote;
	}

	/**
	 * @return the nameChangePassengerList
	 */
	public List<NameChangePassenger> getNameChangePassengerList() {
		return nameChangePassengerList;
	}

	/**
	 * @param nameChangePassengerList
	 *            the nameChangePassengerList to set
	 */
	public void setNameChangePassengerList(List<NameChangePassenger> nameChangePassengerList) {
		this.nameChangePassengerList = nameChangePassengerList;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

}
