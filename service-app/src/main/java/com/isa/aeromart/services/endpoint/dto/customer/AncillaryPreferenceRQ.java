package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.List;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;

public class AncillaryPreferenceRQ {

	private String customerPreferredSeatType;

	private List<CustomerPreferredMealDTO> customerPreferredMealList;

	public String getCustomerPreferredSeatType() {
		return customerPreferredSeatType;
	}

	public void setCustomerPreferredSeatType(String customerPreferredSeatType) {
		this.customerPreferredSeatType = customerPreferredSeatType;
	}

	public List<CustomerPreferredMealDTO> getCustomerPreferredMealList() {
		return customerPreferredMealList;
	}

	public void setCustomerPreferredMealList(List<CustomerPreferredMealDTO> customerPreferredMealList) {
		this.customerPreferredMealList = customerPreferredMealList;
	}

}
