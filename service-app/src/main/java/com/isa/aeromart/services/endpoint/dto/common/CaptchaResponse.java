package com.isa.aeromart.services.endpoint.dto.common;

public class CaptchaResponse extends TransactionalBaseRS{

	private boolean captchaTrue = false;

	public boolean isCaptchaTrue() {
		return captchaTrue;
	}

	public void setCaptchaTrue(boolean captchaTrue) {
		this.captchaTrue = captchaTrue;
	}
	
}
