package com.isa.aeromart.services.endpoint.dto.passenger;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class DuplicateCheckRS extends TransactionalBaseRS {

	private boolean hasDuplicates = false;

	public boolean isHasDuplicates() {
		return hasDuplicates;
	}

	public void setHasDuplicates(boolean hasDuplicates) {
		this.hasDuplicates = hasDuplicates;
	}

}
