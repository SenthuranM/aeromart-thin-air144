package com.isa.aeromart.services.endpoint.dto.ancillary;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "assignType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = PassengerAssignee.class, name = Assignee.AssignType.PASSENGER),
		@JsonSubTypes.Type(value = Assignee.class, name = Assignee.AssignType.ALL_PASSENGERS)})
public class Assignee {

    public interface AssignType {
       String ALL_PASSENGERS = "ALL_PASSENGERS";
       String ALL_SEGMENTS = "ALL_SEGMENTS";
       String PASSENGER = "PASSENGER";
    }

    private String assignType;

    public String getAssignType() {
        return assignType;
    }

    public void setAssignType(String assignType) {
        this.assignType = assignType;
    }
}
