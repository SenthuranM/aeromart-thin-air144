package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.Date;

/** only used by payment summary response */
public class FlightSegment {

	private String flightNo;
	private String segmentCode;
	private Date departueDateTime;
	private Date arrivalDateTime;
	private Charges charges;

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public Charges getCharges() {
		return charges;
	}

	public void setCharges(Charges charges) {
		this.charges = charges;
	}

	public Date getDepartueDateTime() {
		return departueDateTime;
	}

	public void setDepartueDateTime(Date departueDateTime) {
		this.departueDateTime = departueDateTime;
	}

}
