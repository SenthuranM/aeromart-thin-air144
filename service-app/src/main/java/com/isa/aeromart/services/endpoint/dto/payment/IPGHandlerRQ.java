package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;

public class IPGHandlerRQ {

	private String transactionId;

	private String ipgResponse;

	// FIXME : until session is enabled this will be available temp
	private PostPaymentDTO postPaymentDTO;

	private OperationTypes operationType;

	private String alias;

	private boolean cardToBeSaved;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getIpgResponse() {
		return ipgResponse;
	}

	public void setIpgResponse(String ipgResponse) {
		this.ipgResponse = ipgResponse;
	}

	public PostPaymentDTO getPostPaymentDTO() {
		return postPaymentDTO;
	}

	public void setPostPaymentDTO(PostPaymentDTO postPaymentDTO) {
		this.postPaymentDTO = postPaymentDTO;
	}

	public OperationTypes getOperationType() {
		return operationType;
	}

	public void setOperationType(OperationTypes operationType) {
		this.operationType = operationType;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public boolean isCardToBeSaved() {
		return cardToBeSaved;
	}

	public void setCardToBeSaved(boolean cardToBeSaved) {
		this.cardToBeSaved = cardToBeSaved;
	}

}
