package com.isa.aeromart.services.endpoint.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import com.isa.aeromart.services.endpoint.delegate.payment.PaymentServiceAdapter;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.aeromart.services.endpoint.dto.payment.Card;
import com.isa.aeromart.services.endpoint.dto.payment.LmsCredits;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptions;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore.LoyaltyPaymentOption;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.utils.payment.PaymentUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;

public class ValidationServiceUtils {

	private static Log log = LogFactory.getLog(PaymentServiceAdapter.class);

	public static void checkRequest(TransactionalBaseRQ baseRQ) {
		if (baseRQ == null) {
			throw new ValidationException(
					ValidationException.Code.EMPTY_REQUEST);
		}

	}

	public static void checkTransactionId(String transactionId) {
		if (StringUtil.isNullOrEmpty(transactionId)) {
			throw new ValidationException(
					ValidationException.Code.TRANSACTION_ID_EMPTY);
		}
	}

	public static void checkPaxListNullOrEmptyOrNoAdults(
			List<com.isa.aeromart.services.endpoint.dto.common.Passenger> paxList) {
		if (paxList != null && !paxList.isEmpty()) {
			for (com.isa.aeromart.services.endpoint.dto.common.Passenger passenger : paxList) {
				if (PaxTypeTO.ADULT.contentEquals(passenger.getPaxType())) {
					return;
				}
			}
		}
		throw new ValidationException(
				ValidationException.Code.INVALID_PASSENGER_INFORMATION);
	}

	public static void checkReservationContact(
			ReservationContact reservationContact) {
		if (!(reservationContact != null && !StringUtil
				.isNullOrEmpty(reservationContact.getEmailAddress()))) {
			throw new ValidationException(
					ValidationException.Code.INVALID_RESERVATION_CONTACT_DETAILS);
		}
	}

	public static void checkPnr(String pnr) {
		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ValidationException(ValidationException.Code.PNR_EMPTY);

		}

	}

	public static void checkOriginCountryCode(String originCountryCode) {
		if (StringUtil.isNullOrEmpty(originCountryCode)) {
			throw new ValidationException(
					ValidationException.Code.ORIGIN_COUNTRY_CODE_EMPTY);
		}

	}

	public static void checkPaymentOptions(PaymentOptions paymentOptions) {
		if (paymentOptions == null) {
			throw new ValidationException(
					ValidationException.Code.PAYMENT_OPTIONS_EMPTY);

		}

	}

	public static void checkLmsOption(LmsCredits lmsCredits) {
		if (lmsCredits == null) {
			throw new ValidationException(
					ValidationException.Code.LMS_OPTION_EMPTY);

		}

	}

	public static void checkLMSAmountEmptyOrNegative(BigDecimal lmsRequestAmount) {
		if (!checkAmountNotNegative(lmsRequestAmount)) {
			throw new ValidationException(
					ValidationException.Code.LMS_REQUEST_AMOUNT_INVALID);

		}

	}

	public static void checkCardAmountEmptyOrNegative(
			BigDecimal cardRequestAmount) {
		if (!checkAmountNotNegative(cardRequestAmount)) {
			throw new ValidationException(
					ValidationException.Code.CARD_REQUEST_AMOUNT_INVALID);

		}

	}

	private static boolean checkAmountNotNegative(BigDecimal cardRequestAmount) {
		return cardRequestAmount != null
				&& cardRequestAmount.doubleValue() >= 0;
	}

	public static void checkPaymentBaseSession(
			PaymentBaseSessionStore paymentBaseSessionStore) {
		if (paymentBaseSessionStore == null) {
			throw new ValidationException(
					ValidationException.Code.INVALID_SESSION);
		}

	}

	public static LoyaltyPaymentOption getLoyaltyPaymentOption(
			LoyaltyInformation loyaltyInformation) {
		LoyaltyPaymentOption loyaltyPaymentOption = LoyaltyPaymentOption.NONE;

		if (loyaltyInformation != null) {
			loyaltyPaymentOption = loyaltyInformation.getLoyaltyPaymentOption();
		}

		return loyaltyPaymentOption;
	}

	public static void checkPaymentGateways(List<PaymentGateway> paymentGateways) {
		if (!(paymentGateways != null && !paymentGateways.isEmpty())) {
			throw new ValidationException(
					ValidationException.Code.PAYMENT_GATEWAYS_EMPTY);

		}

	}

	public static void checkIPGId(int paymentGatewayId) {
		if (paymentGatewayId <= 0) {
			throw new ValidationException(
					ValidationException.Code.INVALID_IPG_ID);
		}

	}

	public static void checkIPGCardId(int ipgCardId) {
		if (ipgCardId <= 0) {
			throw new ValidationException(
					ValidationException.Code.INVALID_IPG_CARD_ID);
		}

	}

	public static void checkIPGCardNumber(String cardNumber) {
		if (StringUtil.isNullOrEmpty(cardNumber)) {
			throw new ValidationException(
					ValidationException.Code.CARD_NUMBER_EMPTY);
		}

	}

	public static void checkIPGCardHolderName(String cardHolderName) {
		if (StringUtil.isNullOrEmpty(cardHolderName)) {
			throw new ValidationException(
					ValidationException.Code.CARD_HOLDER_NAME_EMPTY);
		}

	}

	public static void checkIPGCardCvv(String cvv) {
		int cvvInt = 0;

		try {
			cvvInt = Integer.valueOf(cvv);
		} catch (Exception e) {// catch (NumberFormatException e) {
			log.error("CVV Number format exception", e);
			throw new ValidationException(
					ValidationException.Code.INVALID_IPG_CARD_CVV);
		}
		if (cvvInt <= 0) {
			throw new ValidationException(
					ValidationException.Code.EMPTY_IPG_CARD_CVV);
		}

	}

	public static void checkPaymentGateway(PaymentGateway paymentGateway) {

		if (paymentGateway == null) { // not necessary as paymentGateways non
										// empty checked
			throw new ValidationException(
					ValidationException.Code.PAYMENT_GATEWAY_EMPTY);

		}

		checkIPGId(paymentGateway.getGatewayId());

	}

	public static void checkIPGCards(List<Card> cards) {
		if (!(cards != null && !cards.isEmpty())) {
			throw new ValidationException(
					ValidationException.Code.PAYMENT_CARDS_EMPTY);

		}

	}

	public static void checkIPGCard(Card card) {
		if (card == null) {
			throw new ValidationException(
					ValidationException.Code.PAYMENT_CARD_EMPTY);

		}

	}

	public static void checkCardExpiryDate(Date expiryDate) {
		if (expiryDate == null) {
			throw new ValidationException(
					ValidationException.Code.CARD_EXPIRY_DATE_EMPTY);

		}

		if (expiryDate.before(new Date())) {
			throw new ValidationException(
					ValidationException.Code.CARD_EXPIRY_DATE_INVALID);

		}

	}

	public static void commonPaymentValidation(MakePaymentRQ makePaymentRQ,
			PaymentBaseSessionStore paymentBaseSessionStore)
			throws ModuleException {

		// removed if condition for escalating below code for payment through
		// LMS full payment.

		if (isRequiredPaymentValidation(paymentBaseSessionStore)) {
			ValidationServiceUtils.checkPaymentOptions(makePaymentRQ.getPaymentOptions());
			ValidationServiceUtils.checkPaymentGateways(makePaymentRQ.getPaymentOptions().getPaymentGateways());
			ValidationServiceUtils.checkPaymentGateway(makePaymentRQ.getPaymentOptions().getPaymentGateways().get(0));

			PaymentGateway paymentGateway = makePaymentRQ.getPaymentOptions().getPaymentGateways().get(0);

			ValidationServiceUtils.checkIPGCards(paymentGateway.getCards());
			ValidationServiceUtils.checkIPGCard(paymentGateway.getCards().get(0));

			Card card = makePaymentRQ.getPaymentOptions().getPaymentGateways().get(0).getCards().get(0);

			ValidationServiceUtils.checkIPGCardId(card.getCardType());
			ValidationServiceUtils.checkCardAmountEmptyOrNegative(card.getPaymentAmount());

			// the following card info are optional for EXTERNAL PGWs
			PaymentUtils paymentUtils = new PaymentUtils();
			if (!PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL.equals(paymentUtils
					.getIPGBehavior(paymentGateway.getGatewayId()))) {

				ValidationServiceUtils.checkCardExpiryDate(card.getExpiryDate());
				ValidationServiceUtils.checkIPGCardNumber(card.getCardNo());

				ValidationServiceUtils.checkIPGCardHolderName(card.getCardHoldersName());

				if (!card.getCardCvv().equals("000")) {
					ValidationServiceUtils.checkIPGCardCvv(card.getCardCvv());
				}

			}
		}
	}

	private static boolean isRequiredPaymentValidation(PaymentBaseSessionStore paymentBaseSessionStore) {

		if (paymentBaseSessionStore == null) {
			return true;
		}
		
		BigDecimal lmsAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal voucherBalanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();

		LoyaltyInformation loyaltyInformation = paymentBaseSessionStore.getLoyaltyInformation();
		if (AppSysParamsUtil.isLMSEnabled() && loyaltyInformation != null && loyaltyInformation.isRedeemLoyaltyPoints()) {
			if (LoyaltyPaymentOption.TOTAL.equals(loyaltyInformation.getLoyaltyPaymentOption())) {
				return false;
			}
			lmsAmount = loyaltyInformation.getTotalRedeemedAmount();
		}

		if (AppSysParamsUtil.isVoucherEnabled() && paymentBaseSessionStore.getPayByVoucherInfo() != null) {
			if (paymentBaseSessionStore.getVoucherInformation().isTotalAmountPaidFromVoucher()) {
				return false;
			}
			voucherBalanceToPay = new BigDecimal(paymentBaseSessionStore.getPayByVoucherInfo().getBalTotalPay());
		}

		return !lmsAmount.equals(voucherBalanceToPay);
	}

	public static void checkLMSUserLogin(
			CustomerSessionStore customerSessionStore) {
		if (customerSessionStore == null) {
			throw new ValidationException(
					ValidationException.Code.LMS_CUSTOMER_NOT_LOGGED_IN);
		}
		if (StringUtil.isNullOrEmpty(customerSessionStore.getFFID())) {
			throw new ValidationException(
					ValidationException.Code.LMS_CUSTOMER_ID_EMPTY);
		}

	}

	public static void checkAlasPaymentAmount(PaymentGateway selectedPaymentGateway) throws ModuleException {
		Integer paymentGatewayId = selectedPaymentGateway.getGatewayId();
		Card selectedCard = selectedPaymentGateway.getCards().get(0);
		BigDecimal paymentAmount = selectedCard.getPaymentAmount();
		BigDecimal aliasPayment = null;

		List<IPGPaymentOptionDTO> gateways = PaymentUtils.getPaymentGatewayList(ApplicationEngine.IBE.toString());
		if (!CollectionUtils.isEmpty(gateways)) {
			IPGPaymentOptionDTO selectedGateway = gateways.stream()
					.filter(gateway -> gateway.isSaveCard() && gateway.getPaymentGateway() == paymentGatewayId).findFirst()
					.orElse(null);
			if (selectedGateway != null) {
				aliasPayment = selectedGateway.getAliasBasePaymentAmount();
			}
		}

		if (aliasPayment == null || !AccelAeroCalculator.isEqual(paymentAmount, aliasPayment)) {
			throw new ValidationException(ValidationException.Code.INVALID_PAYMENT_AMOUNT);
		}

	}
	
	public static void checkCustomerId(String customerId) throws ValidationException{
		if(StringUtil.isNullOrEmpty(customerId)){
			throw new ValidationException(ValidationException.Code.LOYALTY_CUSTOMER_INVALID);
		}
	}
}
