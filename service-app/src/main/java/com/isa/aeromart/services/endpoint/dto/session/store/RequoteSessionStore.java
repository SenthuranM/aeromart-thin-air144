package com.isa.aeromart.services.endpoint.dto.session.store;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.modification.FFIDUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.modification.RequoteBalanceInfo;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.RequoteTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.BalanceQueryRequiredData;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.RequoteONDInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationExternalChargeInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationSegmentInfo;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airreservation.api.dto.NameDTO;

public interface RequoteSessionStore {
	
	public static String SESSION_KEY = RequoteTransaction.SESSION_KEY;
	
	public abstract ReservationSegmentInfo getResSegmentInfo();
	
	public abstract void storeResSegmentInfo(ReservationSegmentInfo reservationSegmentInfo);

	public abstract FareInfo getFareInfo();
	
	public abstract void storeFareInfo(FareInfo fareInfo);

	public abstract BalanceQueryRequiredData getRequoteBalanceReqiredParams();

	public abstract void storeRequoteBalanceReqiredParams(BalanceQueryRequiredData balanceQueryRequiredData);
	
	public abstract List<RequoteONDInfo> getOndInfo ();
	
	public abstract void storeOndInfo (List<RequoteONDInfo> ondInfo);
	
	public abstract PreferenceInfo getPreferences();
	
	public abstract void storePreferences(PreferenceInfo preferenceInfo);
	
	public abstract ReservationInfo getResInfo();

	public abstract void storeResInfo(ReservationInfo resInfo);
	
	public abstract ReservationExternalChargeInfo getExtChargeInfo();	
	
	public abstract RequoteBalanceInfo getRequoteBalanceInfo();
	
	public abstract void storeBalanceInfo(RequoteBalanceInfo requoteBalanceInfo);
	
	public abstract Double getRemainingLoyaltyPoints();
	
	public abstract void storeRemainingAvailableLoyaltyPoints(Double loyaltyPoints);
	
	public abstract Map<Integer, NameDTO> getNameChangedPaxMap();
	
	public abstract void storeNameChangedPaxMap(Map<Integer, NameDTO> nameChangedPaxMap);

	public abstract PostPaymentDTO getRequotePostPaymentInformation();

	public abstract FinancialStore getFinancialStore();
	
	public abstract void storeModifiedFlightsInfo(List<ModifiedFlight> modifiedFlightsInfo);
	
	public abstract RPHGenerator getRPHGenerator();

	public abstract void storeResSegRphToFlightSegMap(String resSegmentRPH, String flightSegmentRPH);
	
	public abstract List<SessionFlightSegment> getSegmentWithInventory();
	
	public abstract LoyaltyInformation getLoyaltyInformation();
	
	public abstract List<SessionFlexiDetail> getSessionFlexiDetails();
	
	public abstract void clearRequoteSessionTansaction();
	
	public abstract List<SessionFlightSegment> getFlightSegments();

	public void storeApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes);

	public List<String> getExistingFlightSegunqIdList();

	public abstract void storeFFIDChangeRQ(FFIDUpdateRQ ffidUpdateRQ);

	public abstract FFIDUpdateRQ getFFIDChangeRQ();

	public PayByVoucherInfo getPayByVoucherInfo();

	public Map<String, String> getVoucherOTPMap();

}