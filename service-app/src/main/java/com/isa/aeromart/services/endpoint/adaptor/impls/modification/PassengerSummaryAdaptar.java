package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.modification.ChargeSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.CommonSummaryInfo;
import com.isa.aeromart.services.endpoint.dto.modification.PassengerInfo;
import com.isa.aeromart.services.endpoint.dto.modification.PassengerSummary;
import com.isa.aeromart.services.endpoint.dto.modification.StateSummaryInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;

public class PassengerSummaryAdaptar implements Adaptor<LCCClientPassengerSummaryTO, PassengerSummary> {

	@Override
	public PassengerSummary adapt(LCCClientPassengerSummaryTO source) {

		PassengerSummary paxSummary = new PassengerSummary();

		paxSummary.setPaxInfo(getPaxInfo(source));
		paxSummary.setPaxCharges(gtePaxCharges(source));

		return paxSummary;
	}

	private CommonSummaryInfo gtePaxCharges(LCCClientPassengerSummaryTO source) {
		CommonSummaryInfo paxCharges = new CommonSummaryInfo();

		paxCharges.setAmountDue(source.getTotalAmountDue());
		paxCharges.setCreditAmount(source.getTotalCreditAmount());
		paxCharges.setFareAmount(source.getSegmentSummary().getNewFareAmount());
		paxCharges.setChargeInfo(getChargeSummaryInfo(source, true));
		paxCharges.setNonRefundableAmount(source.getSegmentSummary().getCurrentNonRefunds());
		paxCharges.setPaidAmount(source.getTotalPaidAmount());
		paxCharges.setRefundableAmount(source.getSegmentSummary().getCurrentRefunds());
		paxCharges.setPostModifiedSummary(getPostModifiedSummary(source));
		paxCharges.setPreModifiedSummary(getPreModifiedSummary(source));

		return paxCharges;
	}

	private StateSummaryInfo getPreModifiedSummary(LCCClientPassengerSummaryTO source) {
		StateSummaryInfo preModifiedSummary = new StateSummaryInfo();

		preModifiedSummary.setAdjAmount(source.getSegmentSummary().getCurrentAdjAmount());
		preModifiedSummary.setChargeInfo(getChargeSummaryInfo(source, false));
		preModifiedSummary.setDiscount(source.getSegmentSummary().getCurrentDiscount());
		preModifiedSummary.setFareAmount(source.getSegmentSummary().getCurrentFareAmount());
		preModifiedSummary.setSurchargeAmount(source.getSegmentSummary().getCurrentSurchargeAmount());
		preModifiedSummary.setTaxAmount(source.getSegmentSummary().getCurrentTaxAmount());
		preModifiedSummary.setTotalPrice(source.getSegmentSummary().getCurrentTotalPrice());
		// preModifiedSummary.setChargeBrakDown(chargeBrakDown);
		return preModifiedSummary;
	}

	private StateSummaryInfo getPostModifiedSummary(LCCClientPassengerSummaryTO source) {
		StateSummaryInfo postModifiedSummary = new StateSummaryInfo();

		postModifiedSummary.setAdjAmount(source.getSegmentSummary().getNewAdjAmount());
		postModifiedSummary.setChargeInfo(getChargeSummaryInfo(source, true));
		postModifiedSummary.setDiscount(source.getSegmentSummary().getNewDiscount());
		postModifiedSummary.setFareAmount(source.getSegmentSummary().getNewFareAmount());
		postModifiedSummary.setSurchargeAmount(source.getSegmentSummary().getNewSurchargeAmount());
		postModifiedSummary.setTaxAmount(source.getSegmentSummary().getNewTaxAmount());
		postModifiedSummary.setTotalPrice(source.getSegmentSummary().getNewTotalPrice());
		// postModifiedSummary.setChargeBrakDown(chargeBrakDown);
		return postModifiedSummary;
	}

	private ChargeSummaryInfo getChargeSummaryInfo(LCCClientPassengerSummaryTO source, boolean isPostModifyAmouts) {

		ChargeSummaryInfo chargeInfo = new ChargeSummaryInfo();
		if (isPostModifyAmouts) {
			chargeInfo.setCancellationCharge(source.getTotalCnxCharge());
			chargeInfo.setModificationCharge(source.getTotalModCharge());
			chargeInfo.setPenaltyCharge(source.getSegmentSummary().getModificationPenalty());
		} else {
			chargeInfo.setCancellationCharge(source.getSegmentSummary().getCurrentCnxAmount());
			chargeInfo.setModificationCharge(source.getSegmentSummary().getCurrentModAmount());
			chargeInfo.setPenaltyCharge(source.getSegmentSummary().getCurrentModificationPenatly());
		}
		return chargeInfo;
	}

	private PassengerInfo getPaxInfo(LCCClientPassengerSummaryTO source) {
		PassengerInfo paxInfo = new PassengerInfo();
		paxInfo.setPaxType(source.getPaxType());
		paxInfo.setPassengerName(source.getPaxName());
		paxInfo.setAccompaniedPaxRPH(source.getAccompaniedTravellerRef());
		paxInfo.setRph(source.getTravelerRefNumber());
		return paxInfo;
	}

}
