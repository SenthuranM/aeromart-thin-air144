package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class AvailableAncillaryUnit {
	
	private Scope scope;
	
	private Applicability applicability;

	private List<Integer> validations;
	
	private AncillaryItemComponent itemsGroup;

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public Applicability getApplicability() {
		return applicability;
	}

	public void setApplicability(Applicability applicability) {
		this.applicability = applicability;
	}

	public List<Integer> getValidations() {
		return validations;
	}

	public void setValidations(List<Integer> validations) {
		this.validations = validations;
	}

	public AncillaryItemComponent getItemsGroup() {
		return itemsGroup;
	}

	public void setItemsGroup(AncillaryItemComponent itemsGroup) {
		this.itemsGroup = itemsGroup;
	}
}
