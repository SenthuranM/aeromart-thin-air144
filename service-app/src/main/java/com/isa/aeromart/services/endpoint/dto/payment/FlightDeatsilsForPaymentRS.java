package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;


/** only used by payment summary request */
public class FlightDeatsilsForPaymentRS {

	private List<String> messages;

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

}
