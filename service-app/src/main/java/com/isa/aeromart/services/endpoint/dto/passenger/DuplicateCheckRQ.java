package com.isa.aeromart.services.endpoint.dto.passenger;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class DuplicateCheckRQ extends TransactionalBaseRQ{

	private List<DuplicateCheckInfo> duplicateCheckList;

	public List<DuplicateCheckInfo> getDuplicateCheckList() {
		return duplicateCheckList;
	}

	public void setDuplicateCheckList(List<DuplicateCheckInfo> duplicateCheckList) {
		this.duplicateCheckList = duplicateCheckList;
	}

}
