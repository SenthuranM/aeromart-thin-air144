package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class DropDownListResponse extends BaseRS{

	private List<PaxTitle> paxTitels;
	
	private List<IBELanguage> languages;

	public List<PaxTitle> getPaxTitels() {
		return paxTitels;
	}

	public void setPaxTitels(List<PaxTitle> paxTitels) {
		this.paxTitels = paxTitels;
	}

	public List<IBELanguage> getLanguages() {
		return languages;
	}

	public void setLanguages(List<IBELanguage> languages) {
		this.languages = languages;
	}
		
}
