package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.common.Transition;

public class DefaultAncillarySelectorFactory {

    private DefaultAncillarySelectorFactory() {
    }

	public static DefaultAncillarySelectionStrategy getDefaultAncillarySelectionStrategy(Transition<List<Scope>> transition) {

		DefaultAncillarySelectionStrategy defaultAncillarySelectionStrategy;

		if (transition.getOld().size() == transition.getNew().size()) {
			defaultAncillarySelectionStrategy = new OneToOneModification();
		} else if ((transition.getOld().size() == 1 && transition.getNew().size() > 1)
				|| (transition.getOld().size() > 1 && transition.getNew().size() == 1)) {
			defaultAncillarySelectionStrategy = new OneToManyModification();
		} else {
			defaultAncillarySelectionStrategy = new IncompatibleModification();
		}

		return defaultAncillarySelectionStrategy;
	}
}
