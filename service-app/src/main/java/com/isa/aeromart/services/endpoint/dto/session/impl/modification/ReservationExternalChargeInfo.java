package com.isa.aeromart.services.endpoint.dto.session.impl.modification;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ReservationExternalChargeInfo {
	
	private Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtCharges = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
	
	public void addExternalCharge(ExternalChgDTO ccChgDTO) {
		this.selectedExtCharges.put(ccChgDTO.getExternalChargesEnum(), ccChgDTO);
	}

	public void removeExternalCharge(EXTERNAL_CHARGES extChgEnum) {
		this.selectedExtCharges.remove(extChgEnum);
	}
	
	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getSelectedExtCharges() {
		return selectedExtCharges;
	}

	public void setSelectedExtCharges(Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtCharges) {
		this.selectedExtCharges = selectedExtCharges;
	}
	
	public BigDecimal getServiceChargeTax(EXTERNAL_CHARGES externalCharge) {
		BigDecimal serviceChargeTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.getSelectedExtCharges().get(externalCharge) != null) {
			serviceChargeTax = this.getSelectedExtCharges().get(externalCharge).getTotalAmount();
		}
		return serviceChargeTax;
	}

	public BigDecimal getServiceCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.getSelectedExtCharges().get(externalCharge) != null) {
			serviceCharge = this.getSelectedExtCharges().get(externalCharge).getAmount();
		}
		return serviceCharge;
	}

}
