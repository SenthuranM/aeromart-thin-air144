package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

import java.util.List;

public class InsurancePlansCombinations extends BasicRuleValidation {

    private List<InsurancePlansGroup> insurancePlanGroups;

    public InsurancePlansCombinations() {
        setRuleCode(RuleCode.INSURANCE_PLANS_COMBINATIONS);
    }

    public List<InsurancePlansGroup> getInsurancePlanGroups() {
        return insurancePlanGroups;
    }

    public void setInsurancePlanGroups(List<InsurancePlansGroup> insurancePlanGroups) {
        this.insurancePlanGroups = insurancePlanGroups;
    }
}
