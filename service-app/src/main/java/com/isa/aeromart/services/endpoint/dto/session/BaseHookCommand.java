package com.isa.aeromart.services.endpoint.dto.session;


public interface BaseHookCommand {

	public void execute();

}
