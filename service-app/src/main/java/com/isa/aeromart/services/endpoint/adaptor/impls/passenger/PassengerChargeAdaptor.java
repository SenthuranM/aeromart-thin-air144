package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class PassengerChargeAdaptor implements Adaptor<ReservationPaxTO, PassengerChargeTo<LCCClientExternalChgDTO>> {

	@Override
	public PassengerChargeTo<LCCClientExternalChgDTO> adapt(ReservationPaxTO source) {
		PassengerChargeTo<LCCClientExternalChgDTO> paxChargeTo = new PassengerChargeTo<>();
		paxChargeTo.setPassengerRph(source.getSeqNumber().toString());
		paxChargeTo.setGetPassengerCharges(source.getExternalCharges());
		return paxChargeTo;		
	}

}
