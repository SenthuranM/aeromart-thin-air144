package com.isa.aeromart.services.endpoint.dto.customer;


public class LMSRegValidationInfo {

	private String headFFID;
	
	private String refferedFFID;
	
	private String ffid;
	
	private String firstName;
	
	private String lastName;

	public String getHeadFFID() {
		return headFFID;
	}

	public void setHeadFFID(String headFFID) {
		this.headFFID = headFFID;
	}

	public String getRefferedFFID() {
		return refferedFFID;
	}

	public void setRefferedFFID(String refferedFFID) {
		this.refferedFFID = refferedFFID;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
