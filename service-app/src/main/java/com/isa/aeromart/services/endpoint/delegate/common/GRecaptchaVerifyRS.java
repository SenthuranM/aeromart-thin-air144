package com.isa.aeromart.services.endpoint.delegate.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GRecaptchaVerifyRS {
	private boolean success;
	private String challenge_ts;
	private String hostname;
	@JsonProperty(value = "error-codes")
	private List<String> errorcodes = new ArrayList();

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getChallenge_ts() {
		return challenge_ts;
	}

	public void setChallenge_ts(String challenge_ts) {
		this.challenge_ts = challenge_ts;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public List<String> getErrorcodes() {
		return errorcodes;
	}

	public void setErrorcodes(List<String> errorcodes) {
		this.errorcodes = errorcodes;
	}

	@Override
	public String toString() {
		return "GRecaptchaVerifyRS [success=" + success + ", challenge_ts=" + challenge_ts + ", hostname=" + hostname
				+ ", errorcodes=" + errorcodes + "]";
	}

}