<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<title><fmt:message key="msg.Page.Title"/></title>
<link rel="stylesheet" type="text/css" href="../css/myStyle_no_cache.css"/>
<%@ include file='../common/interlinePgHD.jsp' %>
<c:if test="${not empty(requestScope.sysOndSource)}">
<script src="<c:out value='${requestScope.sysOndSource}' escapeXml='false'/>" type="text/javascript"></script>		
</c:if>	
<script src="../js/v2/common/flightSearch.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
	    var GLOBALS = <c:out value="${requestScope.systemDefaultParam}" escapeXml="false" />;
		//TODO Refactor-don't add more
		var objCWindow = "";
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}

		var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
		var strToDate	= strSysDate;
		if (top.GLOBALS.currentDate != null && top.GLOBALS.currentDate == "") {
			var date = top.GLOBALS.currentDate;
			var dtTempSysDate = new Date(date.substr(6,4), (Number(date.substr(3,2)) - 1), date.substr(0,2));
			strSysDate  	= DateToString(addDays(dtTempSysDate, 0));
			dtSysDate		= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
			strToDate       = dtSysDate; 
		}	
</script>

</head>

<body>
<c:import url="../common/pageLoading.jsp"/>
<div id="divLoadBg">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' height="700">
	<tr>
	    <td class="kiosk-header" valign="top">
	    <table width='100%' border='0' cellpadding='0' cellspacing='0'>
	                <tr>
	                    <td class="k-header-left"></td>
	                    <td colspan='3'class="k-header-mid" class="alignLeft">
	                        <table width='100%' border='0' cellpadding='0' cellspacing='0' align="center">
	                            <tr>
	                              <td width='185' style='height:70px;'>
	                                	<img src='../images/LIbe005_no_cache.gif' alt='logo' id="lnkHomeBanner" class="cursorPointer" width="185"/>
	                              </td>
	                                <td class="alignRight">
	                                	<a href="javascript:void(0)" class="lanuage"><label class="fntWhite" id="lblLanguage">Arabic</label></a>
	                                </td>
	                            </tr>
	                        </table>
	                    </td>
	                    <td class="k-header-right"></td>
	                </tr>    
	    </table>
	    </td></tr>
	    <tr>
	    <td class="kiosk-body" align="center">
	    <table width='830' border='0' cellpadding='0' cellspacing='0' height="100%">
	    	<tr><td class="rowGap">&nbsp;</td></tr>
	  		<tr>
	  			<td colspan="2" ><label id="lblWelcometoKiosk" class="kisokwelcome"></label></td>
	  		</tr>
	  		<tr><td class="rowGap">&nbsp;</td></tr>
	        <tr>
	            <td valign="top" width='260'>
	            	<%@ include file='../../v2/common/includeFlightSearch.jsp' %> 
	             </td>	            
	             <td align="center">
	                <table width='50%' border='0' cellpadding='2' cellspacing='3'>
	                    <tr>
	                        <td align="center" width="257px">
	                            <a href="javascript:void(0)" class="kiosk-button" id="signIn"><label id="lblSignInKisok"></label></a>
	                        </td>
	                        <td align="center" width="257px">
	                             <a href="javascript:void(0)" class="kiosk-button" id="register"><label id="lblNewUserKisok"></label></a>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td colspan="2" align="center" width="257px">
	                             <a href="javascript:void(0)" class="kiosk-button" id="destinations"><label id="lblDestinations"></label></a>
	                        </td>
	                     </tr>
	                     <tr> 
	                        <td colspan="2" align="center" width="257px">
	                             <a href="javascript:void(0)" class="kiosk-button" id="baggage"><label id="lblBaggageAllowance"></label></a>
	                        </td>
	                     </tr>
	                     <tr>
	                        <td colspan="2" align="center" width="257px">
	                             <a href="javascript:void(0)" class="kiosk-button" id="rentCar"><label id="lblRentCar"></label></a>
	                        </td>
	                     </tr>
	                     <tr>
	                        <td colspan="2" align="center" width="257px">
	                             <a href="javascript:void(0)" class="kiosk-button" id="bookHotel"><label id="lblBookHotel">Rent a Car</label></a>
	                        </td>
	                    </tr>
	                 </table>             
	             </td>
	         </tr>
	    </table>
		</td></tr>
	    <tr>
	    <td class="kiosk-footer" align="center">
	    	 <table width='830' border='0' cellpadding='0' cellspacing='0'>
	            <tr>
	           		<td width="50"></td>
	                <td><img src="../images/footer-img3_no_cache.jpg" width="210" height="232" alt=""/></td>
	                <td width="50"></td>
	                <td><img src="../images/footer-img2_no_cache.jpg" width="210" height="232" alt=""/></td>
	                <td width="50"></td>
	                <td><img src="../images/footer-img1_no_cache.jpg" width="210" height="232" alt=""/></td>
	                <td width="50"></td>
	            </tr>
	         </table>
	    </td>
	    </tr>
	    </table>
	    <form id="submitFrom"  method="post">
	    	<input type="hidden" id="hdnParamData" name="hdnParamData"/>
			<input type="hidden" id="hdnLanguage" name="hdnLanguage"/>
	    </form>
	</div>
	<%@ include file='../common/iBECommonParam.jsp'%>
	<script src="../js/v2/kiosk/kioskHome.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<div id='divLoadMsg' class="mainPageLoader">
		<%@ include file='../common/includeLoadingMsg.jsp' %>
	</div>	
</body>	
</html>
			