<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<title><fmt:message key="msg.Page.Title"/></title>
<link rel="stylesheet" type="text/css" href="../css/myStyle_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"/>
<%@ include file='../common/interlinePgHD.jsp' %>
<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
<script src="../js/v2/kiosk/login.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</head>

<body>
<table width='100%' border='0' cellpadding='0' cellspacing='0' height="700">
<tr>
    <td class="kiosk-header" valign="top">
    <table width='100%' border='0' cellpadding='0' cellspacing='0'>
                <tr>
                    <td class="k-header-left"></td>
                    <td colspan='3'class="k-header-mid" class="alignLeft">
                        <table width='100%' border='0' cellpadding='0' cellspacing='0' align="center">
                            <tr>
                              <td width='185' style='height:70px;'>
                                	<img src='../images/LIbe005_no_cache.gif' alt='logo' id="lnkHomeBanner" class="cursorPointer" width="185"/>
                              </td>
                                <td class="alignRight">
                                	<!--<a href="javascript:void(0)" ><label class="fntWhite">Arabic</label></a>-->
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="k-header-right"></td>
                </tr>    
    </table>
    </td></tr>
    <tr>
    <td class="kiosk-body">
    <table width='100%' border='0' cellpadding='0' cellspacing='0' height="100%">
        <tr>
            <td align='center' valign="middle">
            	<form name="frmKisokLogin" method="post" id="frmKisokLogin">
	                <table width='589' border='0' align="center" cellpadding='0' cellspacing='0' style="width:589px;">
	                    <tr><td class="kiosk-top">
	                        <table width='100%' border='0' cellpadding='0' cellspacing='0'>
	                            <tr><td class="kiosk-title"><label class="fntDefaultWhite">Login</label></td></tr>
	                        </table>
	                    </td></tr>
	                    <tr><td align="center" class="kiosk-mid" valign="middle">
	                        <table width='90%' border='0' cellpadding='3' cellspacing='0'>
	                            <tr><td colspan="2" class="kiosk-welcomnote"><font>Welcome !<br />
										To access the&nbsp;<label id="carrierName" id="carrierName"></label>&nbsp;online site, please enter your User ID and Password and click Login.</font>
								</td></tr>
	                            <tr>
	                            <td class="k-login-label" width="40%"><label>User ID :</label></td>
	                            <td class="k-login-control">
	                            <input type="text" id="username" name="username" size="35"/>
	                            </td></tr>
	                            <tr><td class="k-login-label" width="40%"><label>Password :</label></td>
	                            <td class="k-login-control"><input type="password" id="password" name="password" size="35"/></td>
	                            </tr>
	                            <tr><td width="40%"></td>
	                            <td class="k-login-control">
	                             <input type="button" class="button" value="Close" id="btnCancel"/>
	                             <input type="button" class="button" value="Login" id="btnLogin"/>	                           
	                            </td></tr>
	                        </table>
	                    </td></tr>
	                    <tr><td class="kiosk-bottom">&nbsp;</td></tr>
	                </table>
	                <input type='hidden' id="hdnParamData" name='hdnParamData' value=""/>
                </form>
             </td>
         </tr>
    </table>
	</td></tr>
    <tr>
    <td class="kiosk-footer" align="center">
    	 <table width='830' border='0' cellpadding='0' cellspacing='0'>
            <tr>
           		<td width="50"></td>
                <td><img src="../images/footer-img3_no_cache.jpg" width="210" height="232" alt=""/></td>
                <td width="50"></td>
                <td><img src="../images/footer-img2_no_cache.jpg" width="210" height="232" alt=""/></td>
                <td width="50"></td>
                <td><img src="../images/footer-img1_no_cache.jpg" width="210" height="232" alt=""/></td>
                <td width="50"></td>
            </tr>
         </table>
    </td>
    </tr>
    </table>
    <div id='divLoadMsg' class="mainPageLoader">
    	<%@ include file='../common/includeLoadingMsg.jsp' %>
		<!--<iframe src="LoadMsg" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:200; height:70;'></iframe>
	--></div>    
</body>
</html>