<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
	<%@ page pageEncoding="UTF-8" %> 
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<head>
  
<%@ include file='../common/interlinePgHD.jsp' %>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v3/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
	var strConfData = <c:out value="${requestScope.sesKioskConfData}" escapeXml="false" />;
	var strOHDLabels = <c:out value="${requestScope.ibeOHDLabels}" escapeXml="false" />;
	var timeOut = <c:out value="${requestScope.timeOut}" escapeXml="false" />;
</script>
<style>
html, body{
	background:#fff;
}
</style>
<c:import url="../../../../ext_html/headerExtra.jsp" />
</head>
  <body>
	<c:import url="../common/pageLoading.jsp"/>
    <div id="divLoadBg" style="width:auto;display: none;" class="rightColumn">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' id="PgOHDconfirm">	
		<!-- Top Banner -->
		<c:import url="../../../../ext_html/header.jsp" />    
		<tr>
		<td align='center' class="outerPageBackGround">
			<table border='0' style='width:940px;' cellpadding='2' cellspacing='0' class='PageBackGround'>
			<%--<tr>
				<td colspan='2' align='center'>
					<table width='97%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class="alignRight">
								<img border='0' class="linkImage cursorPointer" src="../images/home_no_cache.gif" id="linkHome"/>
							</td>
						</tr>
						<tr>
							<td class='rowSingleGap'></td>
						</tr>			
					</table>
				</td>
			 </tr> 
 			--%>
			<tr>
				<td class="pageName alignLeft paddingCalss"><label id="lblBookyouFlight"></label></td>
			</tr>
			<tr>
				<td colspan='2'>
					<div class="" style="width: 100%;" class="stepsContainer">
						<c:import url="../../../../ext_html/booking_process_v3.jsp" />
					</div>
				</td>
			</tr>
			<tr>
				<td class='rowGap'></td>
			</tr>
			<tr>
				<td align="left" >
				<div class="rightColumn">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<%@ include file="../common/includeFrameTop.html"%>
							<table width='100%' border='0' cellpadding='2' cellspacing='0'>
										<!--<tr>
											<td align='center'>
												<br/>
													<img src="../images/LogoAni<c:out value="${sessionScope.sysCarrier}" escapeXml="false" />_no_cache.gif" />
												<br/>
											</td>
										</tr>
										-->
										<tr>
											<td class="alignLeft">
												<label id="lblMsgThank"></label>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class="alignLeft">
												<label id="lblSummary" class='fntBold hdFont hdFontColor'>Reservation Summary</label>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class="alignLeft">
												<font class="fntBold">									
														<br/>
															<label id="lblOHDResNo">Your Reservation number is:</label> &nbsp;
															<label id="resNo"></label>
														<br/><br/>							
														<label id="lblOnHoldTime">Dear Customer, your booking is placed ON-HOLD for</label>&nbsp;
														<label id="time"></label>&nbsp;
														<label id="lblOHDHours">hour(s)</label>
														<br/>
														<label id="lblOHDCollectItinerary">please make the payment & confirm your booking.</label>
														<br/>
														<span id="lblOHDExpiryMessage">Bookings not paid for within the time limit will automatically be cancelled.</span>	
														<br/>													
													<br/>
												</font>								
											</td>
										</tr>
									</table>
						<%@ include file="../common/includeFrameBottom.html"%>
						</table>
						</div>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='rowGap'></td>
						</tr>
						<tr id="trButtonPannel">
							<td>
							<div class="buttonset">
								<!-- Button -->
								<table border='0' cellpadding='0' cellspacing='0' width="100%">
									<tr>
										<td class="alignLeft">
											<u:hButton name="btnFinish" id="btnFinish" value="Finish"  cssClass="backPrevious"/>
											<!--<input type="button" id="linkHome"  value="Finish" class="Button"/>-->
										</td>
										<td class="alignRight">
											<!--<u:hButton name="btnPrint" id="btnPrint" value="Print"  cssClass="backPrevious"/>
											<input type="button" id="btnPrint"  value="Print" class="Button" />-->
										</td>
									</tr>
								</table>
							</div>
							</td>
						</tr>
					</table>
					<br/>
				</td>
			</tr>
				<!-- Bottom AA Logo -->
				<tr>
				<td class='appLogo' colspan="2"></td>
				</tr>
				<c:import url="../../../../ext_html/cfooter.jsp" />
			</table>
				</td>
			</tr>
		</table>
			<div id='divLoadMsg' class="mainPageLoader">
				<%@ include file='../common/includeLoadingMsg.jsp' %>
			</div>
		<form method="post" action="" name="formHidden" id="formHidden">
			<input type="hidden" id="hdnMode" name="hdnMode"/>
		</form>	
		<%@ include file='../common/iBECommonParam.jsp'%>
	</div>	
  	<script src="../js/v2/reservation/OHDConfirm.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
  	<div id="accelAeroIBETrack" style="display: none">
		<iframe name="frmTracking" id="frmTracking" src="showBlank" frameborder="0">
		</iframe>	
	</div>
	<div id="accelAeroIBETrackYandex" style="display: none"></div>
  </body>   
  <%@ include file='../common/inline_Tracking.jsp' %>
  <%@ include file='../common/thirdPartyTracking.jsp' %>
</html>