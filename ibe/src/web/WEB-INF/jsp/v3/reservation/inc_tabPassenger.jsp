<table width='100%' border='0' cellpadding='0' cellspacing='0' style="display: ;" id="passengerMainTable">
  <tr style="display: none;">
    <td colspan="2" class="passengerGadient paddingL5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="rowSingleGap"></td>
        </tr>
        <tr>
          <td colspan='2' class='rowGap'></td>
        </tr>
        <tr style="display: none;">
          <td class="alignLeft"><img src="../images/AA045_no_cache.jpg"/>&nbsp;
            <label id="lblPaxEmailContactIsCorrect"></label>
            <br/>
            <img src="../images/AA045_no_cache.jpg"/>&nbsp;
            <label id="lblPaxBookingNameIsCorrect" class='lineHeight fntEnglish'></label></td>
        </tr>
      </table></td>
  </tr>
  <tr id="trModSearch">
    <td class="alignLeft paddingCalss" colspan="2"><%@ include file="../common/modifySearch.jsp"%></td>
  </tr>
  <tr>
    <td colspan='2' class='rowGap'></td>
  </tr>

  <%--  Pax Options to Continue --%>
  <tr id="paxPageOptions" style="display: none;">
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
        <%@ include file="../common/includeFrameTop.html" %>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
          <tr>
            <td align="left" colspan="4">
                <img border='0' src="../images/myAAlogo-red_no_cache.gif" width="130" height="35" alt="My Airarabia"/>
                <img border="0" src="../images/StatusBar_no_cache.jpg" width="1" height="32" alt="" style="margin: 0 23px" id="imgSeperater"/>
                <img border='0' src="../images/air-rewards_no_cache.png" height="31" id="imgAirewards" alt="Air Rewards" />
            </td>
          </tr>
          <tr>
              <td class="rowSingleGap" colspan="6"></td>
          </tr>
          <tr>
            <td width="1%"></td>
            <td width="35%" class="alignLeft bdRight" height="60" valign="top"><table border="0" width="100%">
                <tr>
                  <td align="center" colspan="2"><img border='0' src="../images/Icon_Login_no_cache.gif" width="48" height="27" alt="Login & Continiue"/></td>
                </tr>
                <tr>
                  <td width="5%"><input type="radio" name="howContinue" id="loginContinue" /></td>
                  <td width="90%" class="alignLeft"><label class="fntBold hdFontColor" id="lblLoginContinue">Log in and load details</label></td>
                </tr>
                <tr>
                  <td width="5%"></td>
                  <td width="90%" class="alignLeft"><label id="lblLoginContinue1">I am already registered with 'My airarabia'</label></td>
                </tr>
                <tr>
                  <td class="rowGap" colspan="2"></td>
                </tr>
                <tr id="trRegisterdUser" style="display: none;">
                  <td colspan="2"><%--  Registerd User login --%>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
						<tr>
							<td>
							     <table>
							     	<tr>
							      		<td id="tdFBSignInP" style="display: none;">
											<div class="soclaiButton soCommonBtn">
												<a href="javascript:void(0)" title="Login With Facebook" id="btnFBSignInP" name="btnFBSignInP" class="buttonFB"> 
										          <span class="logo"></span> <label id="lblFBSignIn">Sign In</label>
												</a>
											</div>							      		
							      		</td>
							      		<td id="tdLnSignInP" style="display: none;">
											<div class="soclaiButton soCommonBtn">
												<a href="javascript:void(0)" title="Login With LinkedIn" id="btnLNSignInP" name="btnLNSignInP" class="buttonLN"> 
										          <span class="logo"></span> <label id="lblLNSignIn">Sign In</label>
												</a>
											</div>							      		
							      		</td>
							     	</tr>
							     </table>
							</td>
						</tr>
						<tr>
                        	<td class="rowSingleGap"></td>
                       </tr>
					   <tr>
                        <td><label id="lblSignindSavetime" class="fntBold">Sign in and Save time</label></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>
                      <tr>
                        <td><label id="lblMsgForRegisteredUser"></label></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>
                      <tr>
                        <td><label id="lblLoginID" class="fntBold"> </label></td>
                      </tr>
                      <tr>
                        <td><input type="text" id="txtUID" name="txtUID" maxlength="100" style="width:180px"/></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>
                      <tr>
                        <td><label id="lblPassword" class="fntBold"></label></td>
                      </tr>
                      <tr>
                        <td><input type="password" id="txtPWD" name="txtPWD" maxlength="12" style="width:180px"/></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>
                      <tr>
                        <td><!--<input type="button" id="btnSignIn" title="Click here to login" class="Button ButtonMedium" value="Sign In"/>-->
                          
                          <u:hButton name="btnSignIn" value="Sign In"  cssClass="whitebutton"/></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
            <td width="1%"></td>
            <td width="35%" class="alignLeft bdRight usrReg" valign="top" style="display: none"><table border="0" width="100%">
                <tr>
                  <td align="center" colspan="2"><img border='0' src="../images/Icon_CreateNew_no_cache.gif" width="48" height="27" alt="Login & Continiue"/></td>
                </tr>
                <tr>
                  <td width="5%"><input type="radio" name="howContinue" id="registerContinue"/></td>
                  <td width="90%" class="alignLeft"><label class="fntBold hdFontColor" id="lblRegisterContinue">Register now</label></td>
                </tr>
                <tr>
                  <td width="5%"></td>
                  <td width="90%" class="alignLeft"><label id="lblRegisterContinue1">I would like to register with 'My airarabia'</label></td>
                </tr>
                <tr>
                  <td class="rowGap" colspan="2"></td>
                </tr>
                <tr id="trNewdUser" style="display: none;">
                  <td colspan="2"><%--  Register New User --%>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
                    	<tr>
							<td>
							     <table>
							     	<tr>
							      		<td id="tdFBRegisterP" style="display: none;">
											<div class="soclaiButton soCommonBtn">
												<a href="javascript:void(0)" title="Register With Facebook" id="btnFBRegisterP" name="btnFBRegisterP" class="buttonFB"> 
										          <span class="logo"></span> <label id="lblFBRegister">Register</label>
												</a>
											</div>							      		
							      		</td>
							      		<td id="tdLnRegisterP" style="display: none;">
											<div class="soclaiButton soCommonBtn">
												<a href="javascript:void(0)" title="Register With LinkedIn" id="btnLNRegisterP" name="btnLNRegisterP" class="buttonLN"> 
										          <span class="logo"></span> <label id="lblLnRegister">Register</label>
												</a>
											</div>							      		
							      		</td>
							     	</tr>
							     </table>
							</td>
						</tr>
						<tr>
                        	<td class="rowSingleGap"></td>
                       </tr>
                      <tr>
                        <td ><label class="fntBold" id="lblLoginInfo"></label></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>
                      <tr id="trLoginEmail">
                        <td ><label id="lblLoginID" class="fntBold"> </label></td>
                      </tr>
                      <tr>
                        <td ><input type="text" name="txtLoginEmail" title="login ID" maxlength="100" style="width: 180px"  id="txtLoginEmail"/>
                          <label class="mandatory" id="lblPaxEmailMand">*</label></td>
                      </tr>
                      <tr>
                        <td><label class="fntSmall" id="lblInfoLoginID"></label></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>
                      <tr>
                        <td><label id="lblPassword" class="fntBold"></label></td>
                      </tr>
                      <tr>
                        <td><input type="password" maxlength="12" size="10" name="txtPassword" id="txtPassword" style="width: 180px"/>
                          <label id="lblPaxPassowrdMand"><font class="mandatory">*</font></label></td>
                      </tr>
                      <tr>
                        <td><label id="lblInfoPassword" class="fntSmall"></label></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>
                      <tr>
                        <td><label id="lblConfirmPwd" class="fntBold"></label></td>
                      </tr>
                      <tr>
                        <td><input type="password" maxlength="12" size="10" name="txtConPassword" id="txtConPassword" style="width: 180px"/>
                          <label id="lblPaxPassowrdConfMand"><font class="mandatory">*</font></label></td>
                      </tr>
                      <tr>
                        <td class="rowSingleGap"></td>
                      </tr>                      
					  <tr>
						<td>
						  <input type="checkbox" id="isLMS" name="joinLms"/>
						  <label id="lmsJoin" class="fntBold"></label>
						</td>
					  </tr>
					  	<tr>
							<td>
								<input type="hidden" name="lmsDetails.mobileLMS" id="hdnMobileLMS"/>
								<input type="hidden" name="lmsDetails.EmailHLMS" id="hdnEmailHLMS"/>
								<input type="hidden" name="lmsDetails.DOBLMS" id="hdnDOBLMS"/>
								<input type="hidden" name="lmsDetails.FirstNameLMS" id="hdnFirstNameLMS"/>
								<input type="hidden" name="lmsDetails.MiddleNameLMS" id="hdnMiddleNameLMS"/>
								<input type="hidden" name="lmsDetails.LastNameLMS" id="hdnLastNameLMS"/>
								<input type="hidden" name="lmsDetails.EmailLMS" id="hdnEmailLMS"/>
								<label id="hdnPopulateMsg"></label>
								<label id="hdnPopulateTopic"></label>
							</td>
						</tr>				  
                      <tr>
                        <td height="10">&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
            <td width="1%" class="usrReg" style="display: none"></td>
            <td width="25%" class="alignLeft" valign="top"><table border="0" width="100%">
                <tr>
                  <td align="center" colspan="2"><img border='0' src="../images/Icon_Guest_no_cache.gif" width="48" height="27" alt="Login & Continiue"/></td>
                </tr>
                <tr>
                  <td width="5%"><input type="radio" name="howContinue" id="guestContinue" checked="checked"/></td>
                  <td width="90%" class="alignLeft"><label class="fntBold hdFontColor" id="lblContinueGuest">Continue as Guest</label></td>
                </tr>
                <tr>
                  <td width="5%"></td>
                  <td width="90%" class="alignLeft"><label id="lblContinueGuest1">Book without log in not Register</label></td>
                </tr>
              </table></td>
            <td width="1%"></td>
          </tr>
        </table>
        <%@ include file="../common/includeFrameBottom.html" %>
        </table>
        <div class='rowGap'></div>
    </td>
  </tr>
  <%-- Pax Option to Continue --%>

  <%--  Pax Deatil section --%>
  <tr id="paxSection">
  <td class="alignLeft">
  <table width="100%" border="0" cellspacing="1" cellpadding="1">
      <tr>
          <td style='width:25px;' class="alignLeft"></td>
          <td class="alignLeft"><span id="spnError"></span></td>
          <td style='width:25px;' class="alignLeft"></td>
      </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <%@ include file="../common/includeFrameTop.html" %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td class="alignLeft" colspan="2"><label id="lblPassengerInformation" class="hdFontColor fntBold"></label></td>
  </tr>
  <tr>
      <td><span id="lblPaxBookingNameIsCorrect" class='lineHeight fntEnglish divfont'></span></td>
  </tr>
  <tr>
      <td class="rowGap"></td>
  </tr>
  <tr id="adlGrid" style="display:none">
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <thead>
          <tr>
              <td colspan="6">
                  <span id="clearPax" class="paddingL5 clearLabel clearAutoGenPax ui-helper-hidden">&nbsp;</span>
                  <label id="lblClearAutoGenPax" class="hdFontColor cursorPointer clearAutoGenPax ui-helper-hidden textDec" >Change the passenger names and details</label>
              </td>
          </tr>
          <tr>
              <td colspan='6' class="alignLeft paddingL5"><label id="lblAdultHD" class='fntBold hdFontColor'></label>
              </td>
          </tr>
          <tr class="v3PaxTitle">
              <td width="40"  class=''><label id="lblPaxNo" class='fntBold'></label></td>
              <td width="50"  class=''><label id="lblPaxTitle" class='fntBold'></label>
                  <label id="lblADTitleMand" class="mandatory">*</label></td>
              <td width="150"  class=''><label id="lblPaxFirstName" class='fntBold'></label>
                  <label id="lblADFNameMand" class="mandatory">*</label></td>
              <td width="160"  class=''><label id="lblPaxLastName" class='fntBold'></label>
                  <label id="lblAdLNameMand" class="mandatory">*</label></td>
              <td width="160"  class=''><label id="lblPaxNationality" class='fntBold'></label>
                  <label id="lblADNationalityMand" class="mandatory">*</label></td>
              <td width="100"  class=''><label id="lblPaxDOB" class='fntBold'></label>
                  <label  id="lblADDOBMand" class="mandatory">*</label></td>

          </tr>
          </thead>
          <tr id="paxAdTemplate" class="paxDetailsGrid">
              <td colspan="6"><table border="0" cellspacing="0" cellpadding="0" width="auto">
                  <tr>
                      <td width="40" class="alignLeft" rowspan="2"><label id="paxSequence"></label></td>
                      <td width="50" class="alignLeft"><select id='title' style="width: 45px;" size='1' name='title'>
                      </select></td>
                      <td width="150" class="alignLeft"><input type='text' id='firstName' name="firstName" style='width:130px;' maxlength='50' class="fontCapitalize" onfocus="setDetailsForPax()" /></td>
                      <td width="160" class="alignLeft"><input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize" onfocus="setDetailsForPax()"/></td>
                      <td width="160" class="alignLeft"><select id='nationality' size='1' name='nationality' style='width:150px;'>
                      </select></td>
                      <td width="100" class="alignLeft"><input type='text' id='dateOfBirth' name="dateOfBirth" style='width:70px;' maxlength='50' readonly="readonly"/></td>

                  </tr>
                  <tr>
                      <td class='rowSingleGap'></td>
                  </tr>
                  <tr id="trLoadFamilyDetails">
					<td colspan="6" class="alignRight" > 
					  <font class="fntBold fntLarge mandatory fntEnglish">
						<input type="button" id="loadFamilyDetails" class="Button ButtonLargeMedium loadFamilyDetails" value="Load Famliy Details" autocomplete="off" >
					  </font>
					</td>
				  </tr>
                  <tr class="psptDetails">
                      <td colspan="6"><table width="auto" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                              <td width="90" >&nbsp;</td>
                              <td width="160"  class="alignLeft gridHDsub"><label id="lblFFidAd" class="fntBold adFFID">Airewards ID (Email)</label></td>
                              <td width="100" class="gridHDsub" >
									<label id="lblPaxNationalIDNo" class="gridHDFont fntBold adPSPTExpiry"></label>
									<label id="lblADTNationalIDNoMand" class="MandatoryGrid lblADTNationalIDNoMand">&nbsp;*</label>
							  </td>
                              <td width="160" class='gridHDsub' >
                              		<label id="lblPSPT" class="gridHDFont fntBold adPSPT"></label>
                                  	<label class="MandatoryGrid lblADPSPTMand">&nbsp;*</label>
                              </td>
                              <td width="150" class="gridHDsub" >
                              		<label id="lblPSPTExpiry" class="gridHDFont fntBold adPSPTExpiry"></label>
                                  	<label class="MandatoryGrid lblADExpiryMand">&nbsp;*</label>
                              </td>
                              <td width="150" class="gridHDsub" >
                              		<label id="lblPSPTPlace" class="gridHDFont fntBold adPSPTPlace"></label>
                                  	<label class="MandatoryGrid lblADIssuedCntryMand">&nbsp;*</label>
							  </td>
                          </tr>
                          <tr>
                              <td width="40" >&nbsp;</td>
                              <td width="160" >
                                  <textarea id="ffidTxt" cols="30" rows="1"
                                            style="overflow:hidden;width:130px;height:12px;padding: 2px;resize:none" class="paxffid"></textarea>
                                  <input type="hidden" id='ffid' name="ffid" maxlength="30"/>
                              </td>
                              <td width="90">
									<textarea id="nationalIDNoTxt" cols="30" rows="1" style="overflow:hidden;width:70px;height:12px;padding: 2px;resize:none"></textarea>
									<input type="hidden" id='displayNationalIDNo' name="displayNationalIDNo" maxlength="30"/>					
							  </td>
                              <td width="160" ><!-- <input type='text' id='foidNumber' name="foidNumber" maxlength="30"/>-->

                                  <textarea id="foidNumberTxt" cols="30" rows="1"
                                            style="overflow:hidden;width:130px;height:12px;padding: 2px;resize:none"></textarea>
                                  <input type="hidden" id='foidNumber' name="foidNumber" maxlength="30"/></td>
                              <td width="150" ><input type="text" id="foidExpiry" name="foidExpiry" readonly="readonly" style='width:60px;'/></td>
                              <td width="150" ><select id="foidPlace" size="1" name="foidPlace" style="width:150px">
                              </select></td>

                          </tr>
                      </table></td>
                  </tr>

                  <tr class="docoDetails">
                      <td colspan="6"><table width="auto" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td width="60" class="gridHDsub" ><label id="lblTravelDocumentType" class="gridHDFont fntBold"></label></td>
                              <td width="160" class="gridHDsub" ><label id="lblVisaDocNumber" class="gridHDFont fntBold"></label></td>
                              <td width="160" class='gridHDsub' ><label id="lblPlaceOfBirth" class="gridHDFont fntBold"></label></td>
                              <td width="160" class='gridHDsub' ><label id="lblVisaDocPlaceOfIssue" class="gridHDFont fntBold"></label></td>
                              <td width="160" class="gridHDsub" ><label id="lblVisaDocIssueDate" class="gridHDFont fntBold"></label></td>
                              <td width="160" class="gridHDsub" ><label id="lblVisaApplicableCountry" class="gridHDFont fntBold"></label></td>
                            </tr>
                            <tr>
								<td width="60" ><select id="travelDocumentType" size="1" name="travelDocumentType" style="width:50px"></select></td>
								<td width="160" >
                                    <textarea id="visaDocNumber" cols="30" rows="1" style="overflow:hidden;width:100px;height:12px;padding: 2px;resize:none"></textarea>
                                    <input type="hidden" id='visaDocNumber' name="visaDocNumber" maxlength="30"/></td>
								<td width="160" ><input type="text" id="placeOfBirth" name="placeOfBirth" style='width:60px;'/></td>
								<td width="160" ><input type="text" id="visaDocPlaceOfIssue" name="visaDocPlaceOfIssue" style='width:60px;'/></td>
								<td width="160" ><input type="text" id="visaDocIssueDate" name="visaDocIssueDate" readonly="readonly" style='width:60px;'/></td>
								<td width="160" ><select id="visaApplicableCountry" size="1" name="visaApplicableCountry" style="width:150px"></select></td>
                            </tr>
                      </table></td>
                  </tr>
                  <tr>
                      <td class="rowSingleGap" colspan="6"></td>
                  </tr>
              </table></td>
          </tr>
      </table></td>
  </tr>
  <%--  Child Grid --%>
  <tr>
      <td class="rowGap"></td>
  </tr>
  <tr id="chdGrid" style="display:none">
      <td><div class="table-Row one-Col" style="height: 10px">
          <div class="dottedLine"><img src="../images/spacer_no_cache.gif"/> </div>
      </div>
          <table width="auto" border="0" cellspacing="0" cellpadding="0">
              <thead>
              <tr>
                  <td colspan='6' class="alignLeft paddingL5"><label id="lblChildHD" class='fntBold hdFontColor'></label></td>
              </tr>
              <tr class="v3PaxTitle">
                  <td width="40"  class=''><label id="lblPaxNo" class='fntBold'></label></td>
                  <td width="50"  class=''><label id="lblPaxTitle" class='fntBold'></label>
                      <label id="lblCHTitleMand" class="mandatory">*</label></td>
                  <td width="150"  class=''><label id="lblPaxFirstName" class='fntBold'></label>
                      <label  id="lblCHFNameMand" class="mandatory">*</label></td>
                  <td width="160"  class=''><label id="lblPaxLastName" class='fntBold'></label>
                      <label id="lblCHLNameMand" class="mandatory">*</label></td>
                  <td width="160" class=''><label id="lblPaxNationality" class='fntBold'></label>
                      <label  id="lblCHNationalityMand" class="mandatory">*</label></td>
                  <td width="100" class=''><label id="lblPaxDOB" class='fntBold'></label>
                      <label id="lblCHDOBMand" class="mandatory">*</label></td>
              </tr>
              </thead>
              <tr id="paxChTemplate" class="paxDetailsGrid">
                  <td colspan="6"><table border="0" cellspacing="0" cellpadding="0" width="auto">
                      <tr>
                          <td width="40" class="alignLeft" rowspan="2"><label id="paxSequence"></label></td>
                          <td width="50"><select id='chdTitle' style="width: 45px;" size='1' name='chdTitle'>
                          </select></td>
                          <td width="150"><input type='text' id='firstName' name="firstName" style='width:130px;' maxlength='50' class="fontCapitalize"/></td>
                          <td width="160"><input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize"/></td>
                          <td width="160"><select id='nationality' size='1' name='nationality' style='width:150px;'>
                          </select></td>
                          <td width="100"><input type='text' id='dateOfBirth' name="dateOfBirth" style='width:70px;' maxlength='50' readonly="readonly"/></td>
                      </tr>
                      <tr>
                          <td class='rowSingleGap'></td>
                      </tr>
                      <tr id="trLoadFamilyDetails">
						<td colspan="6" class="alignRight" > 
						  <font class="fntBold fntLarge mandatory fntEnglish">
							<input type="button" id="loadFamilyDetails" class="Button ButtonLargeMedium loadFamilyDetails" value="Load Famliy Details" autocomplete="off" >
						  </font>
						</td>
					  </tr>
                      <tr class="psptDetails">
                          <td colspan="6"><table width="auto" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                  <td width="90">&nbsp;</td>
                                  <td width="160" class='alignLeft gridHDsub'><label class="fntBold chFFID" id="lblFFidCh">Airewards ID (Email)</label></td>
                                  <td width="100" class="gridHDsub" >
										<label id="lblPaxNationalIDNo" class="gridHDFont fntBold adPSPTExpiry"></label>
										<label id="lblADTNationalIDNoMand" class="mandatory lblCHNationalIDNoMand">&nbsp;*</label>
								  </td>
                                  <td width="160" class='gridHDsub'>
                                  		<label id="lblPSPT" class='gridHDFont fntBold chPSPT'></label>
                                  		<label class="MandatoryGrid lblCHPSPTMand">&nbsp;*</label>
                                  </td>
                                  <td width="150" class='gridHDsub'>
                                  		<label id="lblPSPTExpiry" class='gridHDFont fntBold chPSPTExpiry'></label>
                                  		<label class="MandatoryGrid lblCHExpiryMand">&nbsp;*</label>
                                  </td>
                                  <td width="150" class='gridHDsub'>
                                  		<label id="lblPSPTPlace" class='gridHDFont fntBold chPSPTPlace'></label>
                                  		<label class="MandatoryGrid lblCHIssuedCntryMand">&nbsp;*</label>
										<label id="lblADTNationalIDNoMand" class="mandatory lblCHNationalIDNoMand">*</label>
								  </td>
                              </tr>
                              <tr>
                                  <td width="40">&nbsp;</td>
                                  <td width="160" >
                                      <textarea id="ffidTxt" cols="30" rows="1"
                                                style="overflow:hidden;width:130px;height:12px;padding: 2px;resize:none" class="paxffid"></textarea>
                                      <input type="hidden" id='ffid' name="ffid" maxlength="30"/>
                                  </td>
                                  <td width="90">
									<textarea id="nationalIDNoTxt" cols="30" rows="1" style="overflow:hidden;width:70px;height:12px;padding: 2px;resize:none"></textarea>
									<input type="hidden" id='displayNationalIDNo' name="displayNationalIDNo" maxlength="30"/>					
							      </td>    
                                  <td width="160"><textarea id="foidNumberTxt" cols="30" rows="1"
                                                            style="overflow:hidden;width:140px;height:12px;padding: 2px;resize:none"></textarea>
                                      <input type="hidden" id='foidNumber' name="foidNumber" maxlength="30"/></td>
                                  <td width="150"><input type='text' id='foidExpiry' name="foidExpiry" readonly="readonly" style='width:60px;'/></td>
                                  <td width="150"><select id='foidPlace' size='1' name='foidPlace' style='width:150px'>
                                  </select></td>
                              </tr>
                          </table></td>
                      </tr>
                      <tr class="docoDetails">
                          <td colspan="6"><table width="auto" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="60" class="gridHDsub" ><label id="lblTravelDocumentType" class="gridHDFont fntBold"></label><label class="MandatoryGrid lblCHTravelDocumentTypeMand">*</label></td>
                              <td width="160" class="gridHDsub" ><label id="lblVisaDocNumber" class="gridHDFont fntBold"></label><label class="MandatoryGrid lblCHVisaDocNumberMand">*</label></td>
                              <td width="160" class='gridHDsub' ><label id="lblPlaceOfBirth" class="gridHDFont fntBold"></label><label class="MandatoryGrid lblCHPlaceOfBirthMand">*</label></td>
                              <td width="160" class='gridHDsub' ><label id="lblVisaDocPlaceOfIssue" class="gridHDFont fntBold"></label><label class="MandatoryGrid lblCHVisaDocPlaceOfIssueMand">*</label></td>
                              <td width="160" class="gridHDsub" ><label id="lblVisaDocIssueDate" class="gridHDFont fntBold"></label><label class="MandatoryGrid lblCHVisaDocIssueDateMand">*</label></td>
                              <td width="160" class="gridHDsub" ><label id="lblVisaApplicableCountry" class="gridHDFont fntBold"></label><label class="MandatoryGrid lblCHVisaApplicableCountryMand">*</label></td>
                            </tr>
                            <tr>
								<td width="60" ><select id="travelDocumentType" size="1" name="travelDocumentType" style="width:50px"></select></td>
								<td width="160" >
                                    <textarea id="visaDocNumber" cols="30" rows="1"
                                              style="overflow:hidden;width:100px;height:12px;padding: 2px;resize:none"></textarea>
                                    <input type="hidden" id='visaDocNumber' name="visaDocNumber" maxlength="30"/>
								</td>
                                <td width="160" ><input type="text" id="placeOfBirth" name="placeOfBirth" style='width:60px;'/></td>
								<td width="160" ><input type="text" id="visaDocPlaceOfIssue" name="visaDocPlaceOfIssue" style='width:60px;'/></td>
								<td width="160" ><input type="text" id="visaDocIssueDate" name="visaDocIssueDate" readonly="readonly" style='width:60px;'/></td>
								<td width="160" ><select id="visaApplicableCountry" size="1" name="visaApplicableCountry" style="width:150px"></select></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                          <td class="rowSingleGap" colspan="6"></td>
                      </tr>
                  </table></td>
              </tr>
          </table></td>
  </tr>
  <%--  Infant Grid --%>
  <tr>
      <td class="rowGap"></td>
  </tr>
  <tr id="infGrid" style="display:none">
      <td><div class="table-Row one-Col" style="height: 10px">
          <div class="dottedLine"><img src="../images/spacer_no_cache.gif"/> </div>
      </div>
          <table width="auto" border="0" cellspacing="0" cellpadding="0">
              <thead>
              <tr>
                  <td colspan='8' class="alignLeft paddingL5"><label id="lblInfantHD"  class='fntBold hdFontColor'></label></td>
              </tr>
              <tr class="v3PaxTitle">
               <td colspan="7">
                      <table width="auto" border="0" cellspacing="4" cellpadding="0">
                          <tr>
			                  <td width="40"  class=''><label id="lblPaxNo" class='fntBold'></label></td>
			                  <td id="tdInfTitle" width="50"  class=''><label id="lblPaxTitle" class='fntBold'></label>
								  <label id="lblINTitleMand" class="mandatory">*</label></td>
			                  <td width="100"  class='' ><label id="lblPaxDOB" class='fntBold'></label>
			                      <label  id="lblINDOBMand" class="mandatory">*</label></td>
			                  <td width="140"  class=''><label id="lblPaxFirstName" class='fntBold'></label>
			                      <label id="lblINFNameMand" class="mandatory">*</label></td>
			                  <td width="140"  class=''><label id="lblPaxLastName" class='fntBold'></label>
			                      <label id="lblINLNameMand" class="mandatory">*</label></td>
			                  <td width="140" class=''><label id="lblPaxNationality" class='fntBold'></label>
			                      <label  id="lblINNationalityMand" class="mandatory">*</label></td>
			                  <td colspan='4' width=""  class=''><label id="lblPaxTravlingWith" class='fntBold'></label>
			                      <label id="lblINTravelWithMand" class="mandatory">*</label></td>
                      	</tr>
                    </table>
              </tr>
              <tr id="paxInTemplate" class="paxDetailsGrid">
                  <td colspan="7">
                      <table width="auto" border="0" cellspacing="5" cellpadding="0">
                          <tr>
                              <td class="alignLeft" width="40" ><label id="paxSequence"></label></td>
                              <td width="50"><select id='infTitle' style="width: 45px;" size='1' name='infTitle'></select></td>
                              <td width="100" colspan=''><input type='text' id='dateOfBirth' name="dateOfBirth" style='width:60px;' maxlength='50' readonly="readonly"/>
                                  <a href="javascript:void(0)"></a></td>
                              <td width="140"><input type='text' id='firstName' name="firstName" style='width:130px;' maxlength='50' class="fontCapitalize"/></td>
                              <td width="140"><input type='text' id='lastName' name="lastName" style='width:130px;' maxlength='50' class="fontCapitalize"/></td>
                              <td width="140" class="alignLeft"><select id='nationality' size='1' name='nationality' style='width:130px;'>
                      			</select></td>                      
							  <td  colspan='' width="60"  class=''><select id='travelWith' size='1' style='width:50px;' name='travelWith'>
                              </select></td>
                          </tr>
                          <tr id="trLoadFamilyDetails">
							<td colspan="6" class="alignRight" > 
							  <font class="fntBold fntLarge mandatory fntEnglish">
								<input type="button" id="loadFamilyDetails" class="Button ButtonLargeMedium loadFamilyDetails" value="Load Famliy Details" autocomplete="off" >
							  </font>
							</td>
						  </tr>
                          <tr class="psptDetails">
                              <td colspan="5"><table width="auto" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td width="40">&nbsp;</td>
                                      <td width="100" class="gridHDsub" ><label id="lblPaxNationalIDNo" class="gridHDFont fntBold adPSPTExpiry"></label></td>
                                      <td width="180" class='gridHDsub' colspan="2"><label id="lblPSPT" class='gridHDFont fntBold inPSPT'></label><label class="MandatoryGrid lblADPSPTMand">&nbsp;*</label></td>
 		                              <td width="100" class='gridHDsub'><label id="lblPSPTExpiry" class='gridHDFont fntBold inPSPTExpiry'></label><label class="MandatoryGrid lblINExpiryMand">&nbsp;*</label></td>
 	                                  <td width="160" class='gridHDsub'><label id="lblPSPTPlace" class='gridHDFont fntBold inPSPTPlace'></label><label class="MandatoryGrid lblINIssuedCntryMand">&nbsp;*</label></td>
									  <td width="50" class='gridHDsub' colspan='4'>&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td >&nbsp;</td>
                                      <td width="100"><textarea id="nationalIDNoTxt" cols="30" rows="1" style="overflow:hidden;width:70px;height:12px;padding: 2px;resize:none"></textarea></td>
                                      <td colspan="2" ><textarea id="foidNumberTxt" cols="30" rows="1" style="overflow:hidden;width:140px;height:12px;padding: 2px;resize:none"></textarea>
                                      		<input type="hidden" id='foidNumber' name="foidNumber" maxlength="30"/></td>
                                      <td ><input type='text' id='foidExpiry' name="foidExpiry" readonly="readonly" style='width:60px;'/></td>
                                      <td ><select id='foidPlace' size='1' name='foidPlace' style='width:150px'>
                                      </select></td>


                                  </tr>
                              </table></td>
                          </tr>
                          <tr class="docoDetails" style="display: none">
                              <td colspan="6"><table width="auto" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                      				<td width="160" class='gridHDsub' ><label id="lblPlaceOfBirth" class="gridHDFont fntBold"></label></td>
                      				<td width="60" class="gridHDsub" ><label id="lblTravelDocumentType" class="gridHDFont fntBold"></label></td>
                      				<td width="160" class="gridHDsub" ><label id="lblVisaDocNumber" class="gridHDFont fntBold"></label></td>
                      				<td width="160" class='gridHDsub' ><label id="lblPlaceOfBirth" class="gridHDFont fntBold"></label></td>
                      				<td width="160" class='gridHDsub' ><label id="lblVisaDocPlaceOfIssue" class="gridHDFont fntBold"></label></td>
                      				<td width="160" class="gridHDsub" ><label id="lblVisaDocIssueDate" class="gridHDFont fntBold"></label></td>
                      				<td width="160" class="gridHDsub" ><label id="lblVisaApplicableCountry" class="gridHDFont fntBold"></label></td>
                    			  </tr>
                    			  <tr>
									<td width="160" ><input type="text" id="placeOfBirth" name="placeOfBirth" readonly="readonly" style='width:60px;'/></td>
									<td width="60" ><select id="travelDocumentType" size="1" name="travelDocumentType" style="width:50px"></select></td>
									<td width="160" ><input type="text" id="visaDocNumber" name="visaDocNumber" readonly="readonly" style='width:60px;'/></td>
                        			<td width="160" ><input type="text" id="placeOfBirth" name="placeOfBirth" readonly="readonly" style='width:60px;'/></td>
									<td width="160" ><input type="text" id="visaDocPlaceOfIssue" name="visaDocPlaceOfIssue" readonly="readonly" style='width:60px;'/></td>
									<td width="160" ><input type="text" id="visaDocIssueDate" name="visaDocIssueDate" readonly="readonly" style='width:60px;'/></td>
									<td width="160" ><select id="visaApplicableCountry" size="1" name="visaApplicableCountry" style="width:150px"></select></td>
              					  </tr>
                              </table></td>
                          </tr>
                      <!--<tr>
          					     <td width="50" class="gridHDsub" ><label id="lblPaxNationalIDNo" class="gridHDFont fntBold adPSPTExpiry"></label></td> 
		  				        </tr>-->
		  				  <tr>
		  					 <!-- <td width="50">
                  <textarea id="nationalIDNoTxt" cols="30" rows="1" 
																style="overflow:hidden;width:40px;height:12px;padding: 2px;resize:none"></textarea>
                  </td>  -->            
		  				  </tr>
		  					<input type="hidden" id='displayNationalIDNo' name="displayNationalIDNo" maxlength="30"/>					
		  				  </td>	
                      </table>

                  </td>
              </tr>
              </thead>
          </table></td>
  </tr>
  <tr>
      <td class="rowGap"></td>
  </tr>
  <tr>
      <td><label class='mandatory'>&nbsp;*&nbsp;</label>
          <label id="lblAreMandatory"></label>
      </td>
  </tr>
  <tr>
      <td class='rowGap'></td>
  </tr>
  <%-- <tr>
     <td class="alignLeft paddingCalss"><i>
       <label id="lblPaxYemenMassage">Passengers traveling from Yemen are required to enter their nationality. </label>
       </i></td>
   </tr>--%>
  </table>
  <%@ include file="../common/includeFrameBottom.html" %>
  </table>
  <div class='rowGap'></div>
  </td>
  </tr>
  <%--  Pax Deatil section --%>

  <%-- Contact informations --%>
  <tr id="contactSection">
    <td class="alignLeft">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
        <%@ include file="../common/includeFrameTop.html" %>
        <div class="div-Table" id="tblContactDetails">
          <div class="table-Row one-Col">
            <div class="table-Data one-Col">
              <label class='fntBold hdFontColor' id="lblcntInformation"></label>
            </div>
          </div>
          <div class="table-Row three-Col" id="trTitle">
            <div class="table-Data left-TD">
              <label id="lblTitle" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <select id='selTitle' size='1' style="width: 45px;" tabindex="1" name='contactInfo.title'>
              </select>
              <label id="lblTitleMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblFirstName">
            <div class="table-Data left-TD">
              <label id="lblFirstName" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtFName' name="contactInfo.firstName" maxlength='50'  class="fontCapitalize"  tabindex="2" />
              <label id="lblFnameMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblLastName">
            <div class="table-Data left-TD">
              <label id="lblLastName" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtLName' name="contactInfo.lastName" maxlength='50'  class="fontCapitalize"  tabindex="3" />
              <label id="lblLNameMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row one-Col" id="tblLastNameNotification">
		  	<div class="table-Data one-Col">
				<label id="lblLastNameNotification"></label>
			</div>
		  </div>
          <div class="table-Row three-Col" id="tblNationality">
            <div class="table-Data left-TD">
              <label id="lblNationality" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <select id='selNationality' size='1' style='width:135px;' tabindex="6" name='contactInfo.nationality'>
              </select>
              <label id="lblNationalityMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblAddr1">
            <div class="table-Data left-TD">
              <label id="lblAddress" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtStreet' name='contactInfo.addresStreet' maxlength="100" tabindex="4"/>
              <label id="lblAddrMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblAddr2">
            <div class="table-Data left-TD">
              <label id="lblAddress1" class="fntBold">&nbsp;</label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtAddress' name='contactInfo.addresline' maxlength="100" tabindex="5"/>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblCity">
            <div class="table-Data left-TD">
              <label id="lblCity" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtCity'  name="contactInfo.city"  maxlength='20'  tabindex="7" />
              <label id="lblCityMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblZipCode">
            <div class="table-Data left-TD">
              <label id="lblZipCode" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtZipCode' name='contactInfo.zipCode' maxlength="10" tabindex="8"/>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblCountry">
            <div class="table-Data left-TD">
              <label id="lblCountry" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <select id='selCountry' size='1' style='width:135px;'  tabindex="9" name='contactInfo.country'>
              </select>
              <input id='countryName' type="hidden" name='contactInfo.countryName' value="" />
              <label id="lblCountryMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblPreferredLang">
            <div class="table-Data left-TD">
              <label id="lblPreferredLanguage" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <select id='selPrefLang' size='1' style='width:135px;'  tabindex="10" name='contactInfo.preferredLangauge'>
              </select>
              <label id="lblPreferredLangMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row one-Col" style="height: 10px">
            <div class="dottedLine"><img src="../images/spacer_no_cache.gif"/> </div>
          </div>
          <!-- Contact Details -->
          
          <div class="table-Row one-Col thin-row trPhoneNoHeader">
            <div class="table-Data one-Col">
              <label class="fntBold hdFontColor fntBold" id="lblContactDetails"></label>
            </div>
          </div>
          <div id="tblContactNo">
            <div class="table-Row two-Col" id="trMobileNo">
              <div class="table-Data left-TD">
                <label id="lblMobileNo" class="hdFontColor fntBold"></label>
              </div>
              <div class="table-Row one-Col thin-row trPhoneNoLabel">
                <div class="table-Data left-TD auto-height">
                  <div id="atLeasetOne" class="" style=""><font class="mandatory" >*&nbsp;</font>
                    <label class="" id="lblAtleaseOneText">Please provide at least one phone Number</label>
                  </div>
                </div>
                <div class="table-Data right-TD">
                  <label id="lblCountryCode" class='fntSmall fntBold'></label>
                  <label id="lblAreaCode" class='fntSmall areaCode fntBold'></label>
                  <label id="lblNumber" class='fntSmall fntBold'></label>
                </div>
              </div>
                <!-- Mobile No Help for W5 -->
                <% if (com.isa.thinair.commons.core.util.AppSysParamsUtil.addMobileAreaCodePrefix()) { %>
                <div id="mobileNoHelpDiv" class="table-Row one-Col thin-row trPhoneNoLabel">
                    <div id="mobileNoHelp"  class="table-Data right-TD">
                        <label id="lblCountryCodeHelp" class='fntSmall fntBold'>98</label>
                        <label id="lblAreaCodeHelp" class='fntSmall areaCode fntBold'>9XX</label>
                        <label id="lblNumberHelp" class='fntSmall fntBold'>XXXXXXX</label>
                    </div>
                </div>
                <% } %>
              <div class="table-Data right-TD"> <font>:</font>
                <input type='text' id='txtMCountry' name="contactInfo.mCountry" style='width:50px;' maxlength='4'  tabindex="11" class="cunCode"/>
                <input type='text' id='txtMArea' name="contactInfo.mArea" style='width:50px;' maxlength='4'  tabindex="12" class="areaCode"/>
                <input type='text' id='txtMobile' name="contactInfo.mNumber" style='width:100px;' maxlength='11' tabindex="13" class="pNumber"/>
                <label id="lblMobileNoMand" class='mandatory'>*</label>
                <label class='mandatory'>*</label>
              </div>
            </div>
            <div class="table-Row two-Col" id="trphoneNo">
              <div class="table-Data left-TD auto-height">
                <label id="lblPhoneNo" class="hdFontColor fntBold"></label>
              </div>
              <div class="table-Row one-Col thin-row trPhoneNoLabel">
                <div class="table-Data left-TD auto-height">
                  <label>
                  <input type="checkbox" name="chkSameApplyNumber" id="chkSameApplyNumber"/>
                  <label id="lblUseSameNumber">I will use the same number during travel</label>
                  </label>
                  
                </div>
                <div class="table-Data right-TD">
                  <label id="lblCountryCode" class='fntSmall fntBold'></label>
                  <label id="lblAreaCode" class='fntSmall areaCode fntBold'></label>
                  <label id="lblNumber" class='fntSmall fntBold'></label>
                </div>
              </div>
              <div class="table-Data right-TD"> <font>:</font>
                <input type='text' id='txtPCountry' name="contactInfo.lCountry" style='width:50px;' maxlength='4'  tabindex="14" class="cunCode"/>
                <input type='text' id='txtPArea' name="contactInfo.lArea" style='width:50px;' maxlength='4'  tabindex="15" class="areaCode"/>
                <input type='text' id='txtPhone' name="contactInfo.lNumber" style='width:100px;' maxlength='11'  tabindex="16" class="pNumber"/>
                <label id="lblPhoneNoMand" class='mandatory'>*</label>
              </div>
            </div>
            <div class="table-Row two-Col" id="trFaxNo">
              <div class="table-Data left-TD">
                <label id="lblFaxNo" class="fntBold"></label>
              </div>
              <div class="table-Row one-Col thin-row trPhoneNoLabel">
                <div class="table-Data left-TD">
                  <label>Please provide the Fax Number</label>
                </div>
                <div class="table-Data right-TD">
                  <label id="lblCountryCode" class='fntSmall fntBold'></label>
                  <label id="lblAreaCode" class='fntSmall areaCode fntBold'></label>
                  <label id="lblNumber" class='fntSmall fntBold'></label>
                </div>
              </div>
              <div class="table-Data right-TD"> <font>:</font>
                <input type='text' id='txtFCountry' name="contactInfo.fCountry" style='width:50px;' maxlength='4'  tabindex="17" class="cunCode"/>
                <input type='text' id='txtFArea' name="contactInfo.fArea" style='width:50px;' maxlength='4'  tabindex="18" class="areaCode"/>
                <input type='text' id='txtFax' name="contactInfo.fNo" style='width:100px;' maxlength='11'  tabindex="19" class="pNumber"/>
                <label id="lblFaxMand" class='mandatory'>*</label>
              </div>
            </div>
          </div>
          <div class="table-Row one-Col" style="height: 10px">
            <div class="dottedLine"><img src="../images/spacer_no_cache.gif"/> </div>
          </div>
          <div id="tblEmail">
            <div class="table-Row one-Col auto-height">
              <div class="table-Data one-Col">
                <label id="lblEmailAddress" class="hdFontColor fntBold"></label>
              </div>
            </div>
            <div class="table-Row one-Col auto-height">
              <div class="table-Data one-Col">
                <label id="lblMsgValideEmail"></label>
              </div>
            </div>
            <div class="table-Row two-Col" id="trEmail">
              <div class="table-Data left-TD">
                <label id="lblEmailAddress" class="fntBold"></label>
              </div>
              <div class="table-Data right-TD"> <label class="font">:</label>
                <input type='text' id='txtEmail' name="contactInfo.emailAddress" style='width:165px;' maxlength='100'  tabindex="20" />
                <label id="lblEmailMand" class='mandatory'>*</label>
              </div>
            </div>
            <div class="table-Row two-Col">
              <div class="table-Data left-TD auto-height">
                <label id="lblVerifyEmail" class="fntBold"></label>
              </div>
              <div class="table-Data right-TD"> <label class="font">:</label>
                <input type='text' id='txtVerifyEmail' name="txtVerifyEmail" style='width:165px;' maxlength='100' tabindex="21" />
                <label id="lblVerifyEmailMand" class='mandatory'>*</label>
              </div>
            </div>
          </div>
          <div id="tblFFPField" style="display:none">
              <div class="table-Row one-Col" style="height: 5px">
                  <div class="dottedLine"><img src="../images/spacer_no_cache.gif"/> </div>
              </div>
              <div class="table-Row one-Col auto-height">
                  <div class="table-Data one-Col">
                      <label id="lblLMSInfo" class="hdFontColor fntBold"></label>
                  </div>
              </div>
              <div class="table-Row three-Col" id="tblDOBLMS">
                  <div class="table-Data left-TD">
                      <label id="lblDOBLms" class="fntBold"></label>
                  </div>
                  <div class="table-Data right-TD"> <label class="font">:</label>
                      <input type="text" maxlength="50" name="DOBLMS" id="textDOBLMS" />
                      <label id="lblDOBLMSMand" class="mandatory">*</label>
                  </div>
              </div>
              <div class="table-Row three-Col" id="tblComLanguage">
                  <div class="table-Data left-TD">
                      <label id="lblComLanguage" class="fntBold"></label>
                  </div>
                  <div class="table-Data right-TD"> <label class="font">:</label>
                      	<select name="comLangLMS" title="Language" id="textComLangLMS">
							<option value="EN">English</option>
							<option value="AR">Arabic</option>
						</select>
                      <label id="lblComLanguageMand" class='mandatory' style="display: none">*</label>
                  </div>
              </div>
              <div class="table-Row three-Col" id="tblPPNumberLms">
                  <div class="table-Data left-TD">
                      <label id="lblPPNumberLms" class="fntBold"></label>
                  </div>
                  <div class="table-Data right-TD"> <label class="font">:</label>
                      <input type="text" maxlength="50" name="pPNumberLMS" id="textPPNumberLMS" />
                      <label id="lblPPNumberLmsMand" class='mandatory' style="display: none">*</label>
                  </div>
              </div>
            <div class="table-Row one-Col auto-height">
                  <div class="table-Data one-Col">
                      <label id="lblLMSRefferdEMail"></label>
                  </div>
              </div>
              <div class="table-Row one-Col" id="tblRefferdEmailLms">
                  <div class="table-Data one-Col">
                      <label id="lblRefferedEmailLms" class="fntBold"></label>
                  </div>
                  <div class="table-Data one-Col"> <label class="font">:</label>
                      <input type="text" maxlength="50" name="RefferedEmailLMS" id="textRefferedEmailLMS" />
                      <label id="lblRefferedEmailLmsMand" class='mandatory' style="display: none">*</label>
                  </div>
              </div>

              <div class="table-Row one-Col auto-height">
                  <div class="table-Data one-Col">
                      <label id="lblLMSFamilyHeadEMail"></label>
                  </div>
              </div>
              <div class="table-Row one-Col" id="tblFamilHeadEmailLms">
                  <div class="table-Data one-Col">
                      <label id="lblFamilHeadEmailLms" class="fntBold"></label>
                  </div>
                  <div class="table-Data one-Col"> <label class="font">:</label>
                      <input type="text" maxlength="50" name="FamilHeadEmailLMS" id="textFamilHeadEmailLMS" />
                      <label id="lblFamilHeadEmailLmsMand" class='mandatory' style="display: none">*</label>
                  </div>
              </div>
          </div>
          <!-- AARESAA-5454 - Issue 1		
								<div class="table-Row one-Col" id="trItinearyLans">
									<div class="table-Data left-TD">
										<label id="lblLanguage"></label>
									</div>
									<div class="table-Data right-TD">
										<font>:</font>
										<select id="selLanguage" name="contactInfo.emailLanguage"  style="width: 90px;" size="1" tabindex="22"></select>
									</div>
								</div>
								
								-->
          <input type="hidden" id="hdnItiLanguage" name="contactInfo.emailLanguage"/>
          <div class="table-Row  one-Col">
            <div class="table-Data one-Col">
              <label class="fntSmall">&nbsp;</label>
            </div>
          </div>
          <div class="end-Table"></div>
        </div>
        <%@ include file="../common/includeFrameDivideTop.html" %>
      </table>
    </td>
  </tr>
  <%-- Emergency Contact Information --%>
  <tr id="trEmgnContactInfo">
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
        <%@ include file="../common/includeFrameDivideBottom.html" %>
        <div class="div-Table alignLeft" >
          <div class="table-Row one-Col">
            <div class="table-Data one-Col">
              <label id="lblEmgnCntInformation"  class='fntBold hdFontColor'></label>
            </div>
          </div>
          <div class="table-Row three-Col" id="trEmgnTitle">
            <div class="table-Data left-TD">
              <label id="lblTitle" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <select id='selEmgnTitle' size='1' style="width: 45px;" tabindex="23" name='contactInfo.emgnTitle'>
              </select>
              <label id="lblEmgnTitleMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblEmgnFirstName">
            <div class="table-Data left-TD">
              <label id="lblFirstName" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtEmgnFName' name="contactInfo.emgnFirstName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="24" />
              <label id="lblEmgnFNameMand" class='mandatory'>*</label>
            </div>
          </div>
          <div class="table-Row three-Col" id="tblEmgnLastName">
            <div class="table-Data left-TD">
              <label id="lblLastName" class="fntBold"></label>
            </div>
            <div class="table-Data right-TD"> <font>:</font>
              <input type='text' id='txtEmgnLName' name="contactInfo.emgnLastName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="25" />
              <label id="lblEmgnLNameMand" class='mandatory'>*</label>
            </div>
          </div>
          <div id="trEmgnPhoneNo">
            <div class="table-Row two-Col">
              <div class="table-Data left-TD">
                <label id="lblPhoneNo" class="fntBold"></label>
              </div>
              <div class="table-Row one-Col thin-row trPhoneNoLabel">
                <div class="table-Data left-TD">
                  <label>&nbsp;</label>
                </div>
                <div class="table-Data right-TD">
                  <label id="lblCountryCode" class='fntSmall fntBold'></label>
                  <label id="lblAreaCode" class='fntSmall areaCode fntBold'></label>
                  <label id="lblNumber" class='fntSmall fntBold'></label>
                </div>
              </div>
              <div class="table-Data right-TD"> <font>:</font>
                <input type='text' id='txtEmgnPCountry' name="contactInfo.emgnLCountry" style='width:50px;' maxlength='4'  tabindex="26" class="cunCode"/>
                <input type='text' id='txtEmgnPArea' name="contactInfo.emgnLArea" style='width:50px;' maxlength='4'  tabindex="27" class="areaCode"/>
                <input type='text' id='txtEmgnPhone' name="contactInfo.emgnLNumber" style='width:100px;' maxlength='11'  tabindex="28" class="pNumber"/>
                <label id="lblEmgnPhoneNoMand" class='mandatory'>*</label>
              </div>
            </div>
          </div>
          <div id="tblEmgnEmail">
            <div class="table-Row two-Col">
              <div class="table-Data left-TD">
                <label id="lblEmailAddress" class="fntBold"></label>
              </div>
              <div class="table-Data right-TD"> <font>:</font>
                <input type='text' id='txtEmgnEmail' name="contactInfo.emgnEmail" style='width:165px;' maxlength='100'  tabindex="29" />
                <label id="lblEmgnEmailMand" class='mandatory'>*</label>
              </div>
            </div>
            <div class="table-Row two-Col">
              <div class="table-Data left-TD">
                <label id="lblVerifyEmail" class="fntBold"></label>
              </div>
              <div class="table-Data right-TD"> <font>:</font>
                <input type='text' id='txtVerifyEmgnEmail' name="txtVerifyEmgnEmail" style='width:165px;' maxlength='100' tabindex="30" />
              </div>
            </div>
          </div>
          <div class="table-Row  one-Col">
            <div class="table-Data one-Col">
              <label class="fntSmall">&nbsp;</label>
            </div>
          </div>
          <div class="end-Table"></div>
        </div>
        <%@ include file="../common/includeFrameDivideTop.html" %>
      </table></td>
  </tr>
  <!-- Update customer profile -->
  <tr id="trUpdateCustProfile">
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
        <%@ include file="../common/includeFrameDivideBottom.html" %>
        <table width='100%' border='0' cellpadding='0' cellspacing='0' >
          <tr>
            <td colspan="2" class='rowSingleGap'></td>
          </tr>
          <tr>
            <td width="5%"><input type="checkbox" name="contactInfo.updateCustomerProf" id="chkUpdateCustProf" value="true"/>
              &nbsp;</td>
            <td class="alignLeft"><label id="lblUpdateCustProfile"></label></td>
          </tr>
        </table>
        <%@ include file="../common/includeFrameDivideTop.html" %>
      </table></td>
  </tr>
  <tr>
    <td class="alignLeft"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td class='pnlMidL'></td>
          <td class='pnlMid'><label class='mandatory'>&nbsp;*&nbsp;</label>
            <label id="lblAreMandatory"></label></td>
          <td class='pnlMidR'></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><%@ include file="../common/includeFrameGroupBottom.html" %></td>
  </tr>
  <tr>
    <td class='rowSingleGap'></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="6" class="alignLeft"><div class="buttonset"> 
              <!-- Button -->
              <table border='0' cellpadding='0' cellspacing='0'>
                <tr>
                  <td class="alignLeft">
                    <u:hButton name="btnPrevious" id="btnPrevious" tabIndex="31" value="Previous Step"  cssClass="backPrevious"/></td>
                  <td style="width:10px;">&nbsp;</td>
                  <td>
                    <u:hButton name="btnSOver" id="btnSOver" value="Start Over" tabIndex="31" cssClass="blackStOver"/>
                  <td style="width:10px;">&nbsp;</td>
                  <td class="alignRight">
                    <u:hButton name="btnNext" id="btnNext" value="Next" tabIndex="30" cssClass="redContinue"/></td>
                </tr>
              </table>
            </div></td>
        </tr>
      </table></td>
  </tr>
</table>

<div id="familyMemberDetails" style="display: none;min-height: 260px">
	<%@ include file='familyMemberDetails.jsp' %>
</div>
