<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%--
	<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" }'>
		<!-- Google Website Optimizer Tracking Script -->
			<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
			  _gaq.push(['gwo._trackPageview', '/0528702568/goal']);
			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
			</script>
		<!-- End of Google Website Optimizer Tracking Script -->
	</c:if>
	<c:if test='${sessionScope.sessionDataDTO.carrier == "3O" }'>
			<!-- Google Website Optimizer Tracking Script -->
			<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
			  _gaq.push(['gwo._trackPageview', '/2980512962/goal']);
			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
			</script>
		<!-- End of Google Website Optimizer Tracking Script -->
	</c:if>
	<c:if test='${sessionScope.sessionDataDTO.carrier == "E5" }'>
		<!-- Google Website Optimizer Tracking Script -->
			<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
			  _gaq.push(['gwo._trackPageview', '/3403576409/goal']);
			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
			</script>
		<!-- End of Google Website Optimizer Tracking Script -->
	</c:if> 
 --%>
	<%@ include file='../common/interlinePgHD.jsp' %>
	<link rel='stylesheet' type='text/css' href='../css/table_layoutV3_EN.css'/>	
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v3/reservation/reservationConfirmatioSupport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript">
		var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
		var GLOBALS = {};
		GLOBALS.skipPersianDateChange = <c:out value="${requestScope.commonParams.skipPersianCalendarChange}" escapeXml="false"/>;
	</script>
	<c:import url="../../../../ext_html/headerExtra.jsp" />
</head>
<body>
<c:import url="../common/pageLoading.jsp"/>
<div id="processingRes" style="display: none">
	<div class="spMsg" align="center">
		<label id="lblBookingProcessing">Your booking is processing, Please wait...</label>
	</div>
</div>
<div id="divLoadBg">
<form action="" id="frmConfirmation" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgInterlineConfirm">
		<%-- Top Banner --%>
		<c:import url="../../../../ext_html/header.jsp" />
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<tr>
						<td>
							<table width="0" height="0">
								<tr><td><span id='spnSpotAdd'></span></td></tr>
							</table>
						</td>
					</tr>
					<%-- Content holder --%>
					<%-- Left Column --%>
					<tr>
					<td colspan="2" class="mainbody" class="alignLeft">
						<div id="sortable-ul">
							<div class="sortable-child">
							<%-- Left Column --%>
							<div style="width: 250px;background: transparent;visibility: hidden;"  class="floater">
							
							<div id="divSummaryPane" style="display: none;">
							<div class="bookingSummary">
								<div class="Page-heading">
								<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tbody>
										<tr>
											<td colspan="3">
											<table cellspacing="0" cellpadding="0" border="0" width="100%"
												class="headerRow">
												<tbody>
													<tr>
														<td class="headerBG hLeft"></td>
														<td class="headerBG hCenter alignLeft"><label
															class="fntBold gridHDFont" id="lblBookingSummary">Booking
														Summary</label></td>
														<td class="headerBG hRight"></td>
													</tr>
												</tbody>
											</table>
											</td>
										</tr>
										<tr>
											<td class="mLeft"></td>
											<td class="mCenter">
											<div class="pane-body">
											<table cellspacing="0" cellpadding="0" border="0" width="100%">
												<tbody>
													<tr>
														<td class="pnlTop"></td>
													</tr>
													<tr>
														<td style="height: 52px;" class="pnlBottom add-padding alignLeft">
														<table cellspacing="1" cellpadding="1" border="0" width="">
															<tbody>
																<tr>
																	<td><label id="adultCount">1 Adult(s)</label><label
																		id="childCount"></label> <label id="infantCount"></label></td>
																</tr>
															</tbody>
														</table>
														</td>
													</tr>
													<tr>
														<td>
														<div style="display: block;" id="outFlightsDiv">
														<table cellspacing="0" cellpadding="0" border="0" width="100%">
															<tbody>
																<tr>
																	<td style="height: 32px;background-color: #666666"
																		class="pnlBottom colourGreavy alignLeft">
																	<table cellspacing="1" cellpadding="1" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td>&nbsp;<label class="fntBold gridHDFont"
																					id="outFlightDirection">Outbound</label></td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="pnlTop"></td>
																</tr>
																<tr>
																	<td class="rowGap"></td>
																</tr>
																<tr id="outFlightTemplate" style="display: none;">
																	<td class="pnlBottom add-padding alignLeft">
																	<table cellspacing="1" cellpadding="1" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td><label class="fntBold" id="flightSeqNo"></label><label
																					class="fntBold" id="segmentCode"></label></td>
																			</tr>
																			<tr>
																				<td><label id="lblDepature">Departure</label><label>:</label>
																				&nbsp; <label id="departureDate" class="date-disp"></label>
                                                                                    <label id="departureDateValue" class="date-hid" style="display:none"></label>,&nbsp; <label
																					id="departureTime"></label></td>
																			</tr>
																			<tr>
																				<td><label id="lblArrival">Arrival</label><label>:</label>
																				&nbsp; <label id="arrivalDate" class="date-disp"></label>
                                                                                    <label id="arrivalDateValue" class="date-hid" style="display:none"></label>,&nbsp; <label
																					id="arrivalTime"></label></td>
																			</tr>
																			<tr>
																				<td class="summarySperator"></td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>								
															</tbody>
														</table>
														<div style="display: none;" class="fntBold add-padding"
															id="totalOutDiv">
														<table cellspacing="0" cellpadding="0" border="0" width="100%">
															<tbody>
																<tr>
																	<td width="70%"><label class="fntBold"
																		id="lblTotalOutFlight">Total outbound flight</label></td>
																	<td width="10%" class="alignRight"><label
																		class="fntDefault fntBold" name="valBaseCurrCode"></label></td>
																	<td width="20%" class="alignRight"><label class="fntBold"
																		id="TotalOutFlight"></label></td>
																</tr>
															</tbody>
														</table>
														</div>
														</div>
														</td>
													</tr>
													<tr>
														<td class="rowGap"></td>
													</tr>
													<tr>
														<td>
														<div style="display: none;" id="inFlightsDiv">
														<table cellspacing="0" cellpadding="0" border="0" width="100%">
															<tbody>
																<tr>
																	<td style="height: 32px;"
																		class="pnlBottom colourGreavy alignLeft">
																	<table cellspacing="1" cellpadding="1" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td>&nbsp;<label class="fntBold gridHDFont"
																					id="inFlightDirection"> </label></td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="pnlTop"></td>
																</tr>
																<tr>
																	<td class="rowGap"></td>
																</tr>
																<tr id="inFlightTemplate" style="display: none;">
																	<td class="pnlBottom add-padding alignLeft">
																	<table cellspacing="1" cellpadding="1" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td><label class="fntBold" id="flightSeqNo"></label><label
																					class="fntBold" id="segmentCode"></label></td>
																			</tr>
																			<tr>
																				<td><label id="lblDepature">Departure</label><label>:</label>
																				&nbsp; <label id="departureDate"></label>,&nbsp; <label
																					id="departureTime"></label></td>
																			</tr>
																			<tr>
																				<td><label id="lblArrival">Arrival</label><label>:</label>
																				&nbsp; <label id="arrivalDate"></label>,&nbsp; <label
																					id="arrivalTime"></label></td>
																			</tr>
																			<tr>
																				<td class="summarySperator"></td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>
															</tbody>
														</table>
														<div style="display: none;" class="fntBold add-padding"
															id="totalInDiv">
														<table cellspacing="0" cellpadding="0" border="0" width="100%">
															<tbody>
																<tr>
																	<td width="70%"><label class="fntBold"
																		id="lblTotalInFlight">Total inbound flight</label></td>
																	<td width="10%" class="alignRight"><label
																		class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																	<td width="20%" class="alignRight"><label class="fntBold"
																		id="TotalInFlight"></label></td>
																</tr>
															</tbody>
														</table>
														</div>
														</div>
														</td>
													</tr>
													<tr>
														<td class="rowGap"></td>
													</tr>
													<tr>
														<td>
														<div style="display: none;" id="addServiceDiv">
														<table cellspacing="0" cellpadding="0" border="0" width="100%">
															<tbody>
																<tr>
																	<td style="height: 52px;"
																		class="pnlBottom colourGreavy alignLeft">
																	<table cellspacing="1" cellpadding="1" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td>&nbsp;<label class="fntBold gridHDFont"
																					id="lblAditionalServices">Additional Services</label></td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="pnlTop"></td>
																</tr>
																<tr>
																	<td class="rowGap"></td>
																</tr>
																<tr>
																	<td class="pnlBottom alignLeft">
																	<div class="add-padding" style="display: none;"
																		id="trFlexiCharges" name="trFlexiCharges">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold"
																					id="lblFlexiCharges">Flexi Charges</label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntBold" id="valFlexiCharges" name="valFlexiCharges"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
																	<div class="add-padding" style="display: none;"
																		id="trSeatCharges" name="trSeatCharges">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold"
																					id="lblSeatSelection">undefined</label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntBold" id="valSeatCharges" name="valSeatCharges"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
																	<div class="add-padding" style="display: none;"
																		id="trInsuranceCharges" name="trInsuranceCharges">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold"
																					id="lblTravelSecure">undefined</label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntBold" id="valInsuranceCharges" name="valInsuranceCharges"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
																	<div class="add-padding" style="display: none;"
																		id="trMealCharges">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold"
																					id="lblMealSelection" name="lblMealSelection">undefined</label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntBold" id="valMealCharges" name="valMealCharges"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
																	<div class="add-padding" style="display: none;"
																		id="trBaggageCharges">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold"
																					id="lblBaggageSelection" name="lblBaggageSelection">undefined</label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntBold" id="valBaggageCharges" name="valBaggageCharges"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
                                                                    <div class="add-padding" style="display: none;"
																		id="trSsrCharges">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold"
																					id="lblSsrSelection" name="lblSsrSelection">undefined</label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntBold" id="valSsrCharges" name="valSsrCharges"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
																	<div class="add-padding" style="display: none;"
																		id="trAptCharges">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold"
																					id="lblAptSelection" name="lblAptSelection">undefined</label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntBold" id="valAptCharges" name="valAptCharges"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
																	<div class="add-padding" style="display: none;" id="trTxnFee" name="trTxnFee">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tbody>
																			<tr>
																				<td width="70%"><label class="fntBold" id="lblTNXFee"></label></td>
																				<td width="10%" class="alignRight"><label
																					class="fntDefault fntBold" name="valBaseCurrCode">AED</label></td>
																				<td width="20%" class="alignRight"><label
																					class="fntDefault fntBold" id="valTxnFee" name="valTxnFee"></label></td>
																			</tr>
																		</tbody>
																	</table>
																	</div>
																	</td>
																</tr>
																<tr>
																	<td class="rowGap"></td>
																</tr>
															</tbody>
														</table>
														</div>
														</td>
													</tr>
												</tbody>
											</table>
											</div>
											</td>
											<td class="mRight"></td>
										</tr>
										<tr>
											<td colspan="3">
											<table cellspacing="0" cellpadding="0" border="0" width="100%"
												class="footerRow">
												<tbody>
													<tr>
														<td class="headerBG bLeft"></td>
														<td class="headerBG bCenter">
														<div style="display: block;" class="fntBold" id="totalPriceDiv">
														<table cellspacing="0" cellpadding="0" border="0" width="100%">
															<tbody>
																<tr>
																	<td width="70%"><label class="fntBold gridHDFont"
																		id="lblTotalFlightServices">Total flights + services</label></td>
																	<td width="10%" class="alignRight"><label
																		class="gridHDFont fntBold" name="valBaseCurrCode"></label></td>
																	<td width="20%" class="alignRight"><label
																		class="fntBold gridHDFont" id="valTotal" name="valTotal"></label></td>
																</tr>
															</tbody>
														</table>
														</div>
														</td>
														<td class="headerBG bRight"></td>
													</tr>
												</tbody>
											</table>
											</td>
										</tr>
									</tbody>
								</table>
								</div>
								</div>
								<%-- <div class="bookingSummary"> 
								<div class="Page-heading">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="headerRow">
										<tr>
											<td class='headerBG hLeft'></td>
											<td class='headerBG hCenter'><label id="lblBookingSummary" class='fntBold gridHDFont'></label></td>
											<td class='headerBG hRight'></td>
										</tr>
									</table>
									</td></tr>
									<tr>
											<td class='mLeft'></td>
											<td class='mCenter'>
													<div class="pane-body">
													<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgPassenger">
														<tr>
															<td class='pnlTop'></td>
														</tr>
														<tr>
															<td class='pnlBottom add-padding alignLeft'>
																<table width="100%" border="0" cellspacing="1" cellpadding="1">
																	<tr>
																		<td>
																		<label id="adultCount"></label>
																		<label id="childCount"></label>
																		<label id="infantCount"></label>
																		</td>            
																	</tr>
																	</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate">
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom add-padding alignLeft' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																				<tr>
																					<td colspan="3"><label id="segmentCode"  class='fntBold hdFontColor'/></td>            
																				</tr>
																				<tr>
																					<td width="50%"><label id="lblflightNoHD"></label></td>
																					<td width="1%"><label>:</label></td>
																					<td width="49%"><label id="flightNumber"/></td>
																				</tr>
																				<tr>
																					<td><label id="lbldepatureDate"></label> </td>
																					<td><label>:</label></td>
																					<td><label id="departureDate"/></td>
																				</tr>
																				<tr>
																					<td><label id="lbldepatureTime"></label></td>
																					<td><label>:</label></td>
																					<td><label id="departureTime"/></td>
																				</tr>
																				<tr>
																					<td><label id="lblarrivalTime"></label></td>
																					<td><label>:</label></td>
																					<td><label id="arrivalTime"/></td>
																				</tr>
																				<tr>
																					<td><label id="lblDuration"></label></td>
																					<td><label>:</label></td>
																					<td><label id="duration"/></td>
																				</tr>
																				<tr><td colspan="3" ></td></tr>
    																			<tr><td colspan="3" ><span name="anciDetails" >
    																			</span></td></tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													</div>
												</td>
												<td class='mRight'></td>
											</tr>
											<tr><td colspan="3">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerRow">
											<tr>
											<td class="headerBG bLeft"></td>
											<td class="headerBG bCenter">&nbsp;</td>
											<td class="headerBG bRight"></td>
											</tr></table></td>
											</tr>
											
										</table>
									  </div>
								</div> --%>
								
								<%-- Payment Summary --%>
								<%--<div class="paymentSummary"> 
									<div class="Page-heading">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="headerRow">
											<tr>
												<td class='headerBG hLeft'></td>
												<td class='headerBG hCenter'><label id="lblPaymentSummary" class='fntBold gridHDFont'></label></td>
												<td class='headerBG hRight'></td>
											</tr>
										</table>
										</td></tr>
										<tr>
												<td class='mLeft'></td>
												<td class='mCenter'>
													<div class="pane-body">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" >
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																		<td class='pnlTopR'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom alignLeft' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																				<tr name='trAirFare'>
																					<td width="55%"><label id="lblAirFare"></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valAirFare' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr name='trCharges'>
																					<td width="55%"><label id="lblCharges"> &amp;
																					</label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valCharges' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr name='trFlexiCharges'>
																					<td width="55%"><label id="lblFlexiCharges"></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valFlexiCharges' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr name='trSeatCharges'>
																					<td><label id='lblSeatSelection' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valSeatCharges' class='fntDefault txtBold'></label></td>
																				</tr>																				
																				<tr name='trInsuranceCharges'>
																					<td><label id='lblTravelSecure' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valInsuranceCharges' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr name='trMealCharges'>
																					<td><label id='lblMealSelection' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valMealCharges' class='fntDefault txtBold'></label></td>
																				</tr>									
																				<tr name='trTxnFee'>
																					<td width="55%"><label id="lblTNXFee"></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>	
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valTxnFee' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr>
																					<td colspan="4" height="20px"></td>
																				</tr>
																				<tr>
																					<td width="55%"><label id="lblTotal" class='fntBold'></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valTotal' class='fntDefault fntBold hdFontColor' ></label></td>
																				</tr>
																				<tr style="display: none;" name="trSelectedCurr">
																					<td width="55%"></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valSelectedCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valSelectedTotal' class='fntDefault txtBold hdFontColor'></label></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													</div>
												</td>
												<td class='mRight'></td>
											</tr>
											<tr><td colspan="3">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerRow">
											<tr>
											<td class="headerBG bLeft"></td>
											<td class="headerBG bCenter">&nbsp;</td>
											<td class="headerBG bRight"></td>
											</tr></table></td>
											</tr>
										</table>
									</div>
								</div> --%>
							</div>
							</div>
						&nbsp;
						</div> 
						<div class="sortable-child">
						<%-- Right Column --%>
						<div class="rightColumn">
							<table width='100%' border='0' cellpadding='0' cellspacing='0' class="alignRight">
								<tr id="trBookingSteps" style="display: none">
									<td align='center'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td colspan='2' class='pnlBottom' valign='top' align='center' style="height: 38px">
													<table width='98%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<tr>
															<td class="pageName alignLeft paddingCalss">
																<label id="lblBookyouFlight"></label>
															</tr>
														<tr><td class="alignLeft">
															<div style="width: 100%;" class="stepsContainer">
																<c:import url="../../../../ext_html/booking_process_v3.jsp" />
															</div>
														</td></tr>
														<tr>
															<td class="rowSingleGap">
															</td>
														</tr>
														<%--<tr>
															<td class="alignLeft" class="">
															<label id="lblItinerary"></label>
															</td>
														</tr> --%>
														
														<!-- <tr>
															<td class="alignLeft">
																<label id="lblMsgThank"></label>
															</td>
														</tr> -->
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr id="trCreditcardProof">
															<td class="alignLeft">
																<div class="spMsg">
																	<label id="lblCreditcardProof"></label>
																</div>															
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr id="transactionResultPanel" style="display: none">
															<td class="alignLeft">
																<div class="spMsg">
																	<label class="fntBold" id="transaction.header"></label>
																	<table border="0">
																		<tr>
																			<td width="150px" class="alignLeft"><label id="transaction.time"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionTime"></label></td>
																			<td width="150px" class="alignLeft"><label id="transaction.status"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionStatus"></label></td>
																		</tr>
																		<tr>
																			<td width="150px" class="alignLeft"><label id="transaction.amount"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionAmount"></label></td>
																			<td width="150px" class="alignLeft"><label id="transaction.referenceID"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionReferenceID"></label></td>
																		</tr>
																		<tr>
																			<td width="150px" class="alignLeft"><label id="transaction.merchanttrackID"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionMerchanttrackID"></label></td>																			
																		</tr>
																	</table>
																</div>															
															</td>
														</tr>
																												
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td colspan='2' class='' valign='top' align='center'>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td class="alignLeft">
																<label id="lblSummary" class='fntBold hdFont hdFontColor'>Reservation Summary</label>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td align='center'>
																<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																	<td colspan="2">
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td>
																					<img src='../images/n058_no_cache.gif' alt='Confirm'/>
																				</td>
																				<td colspan='2' class="alignLeft">
																					<label id="lblMsgThank" class='fntBold'></label></br>
																					<label id="lblMsgSentMail" class='fntBold'> Your booking has been successfully completed and your itinerary has been sent to you by email </label>																					
																				</td>
																			</tr>
																		</table>
																	</td>
																	</tr>
																	<tr>
																		<td class='rowGap' colspan="2"></td>
																	</tr>
																	<tr>
																		<td colspan='2' class='rowGap'></td>
																	</tr>
																	<tr id='trInsuranceSellFailed' style='display:none;'>
																		<td colspan='2' class='alignLeft'>
																			<label id='msgInsuranceSellFailed'>&nbsp;</label> 
																		</td>																		
																	</tr>
																	<tr>																
																		<td colspan="4">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<%@ include file="../common/includeFrameTop.html"%>
																			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td colspan="2" class="alignLeft">
																						<label  id="lblBookinDetails" class='hdFontColor fntBold'>Booking Details</label>
																					</td>
																				</tr>
																				<tr>
																					<td class="alignLeft" width="55%"> 
																						<label  id="lblPnr">Reservation Number</label><label class="paddingL3">:</label>	
																						<label id="lblPnrValue" class="fntBold"> <c:out value='${sessionScope.sesPNRNO}' escapeXml='false' /></label>
																					</td>
																					<td  width="45%" class="alignLeft" > <span id="spnPolicy">
																						<label  id="lblPolicy"></label><label class="paddingL3" >:</label>	
																						<label id="lblPolicyValue" class="fntBold"> </label></span>
																					</td>
																				</tr>
																				<tr>
																					<td  width="55%" class="alignLeft" >
																						<label id="lblPnrDate"></label><label class="paddingL3">:</label>
                                                                                        <label id="lblPnrDateValue" class="fntBold date-disp"></label>
                                                                                        <label id="lblPnrDateValueHid" class="fntBold date-hid" style="display:none"></label>
																					</td>
																					<td width='45%'  id="tdInsTypeDisplay" class="alignLeft" style="display: none;" >
																						<label id="lblInsuranceType"></label>  
																						<label class="padding3"> : </label>
																						<label id="lblInsuranceTypeValue" class="fntBold"></label>
																					</td>
																				</tr>
																			</table>
																			<%@ include file="../common/includeFrameBottom.html"%>
																			</table>																		
																		</td>																		
																	</tr>
																	<tr>
																		<td colspan='2' class='rowGap'></td>
																	</tr>											
																	<tr id="insPanel" style="display:none">																
																		<td colspan="4">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<%@ include file="../common/includeFrameTop.html"%>
																			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td colspan="2" class="alignLeft">
																						<label  id="lblInsuranceHD" class='hdFontColor fntBold'>Buy Travel Insurance</label>
																						<span class="ttip" id="insuranceAnciOfferDescription" title="">
																							<label id="insuranceAnciOfferTitle"></label>
																						</span>
																					</td>
																				</tr>
																				<tr>
																					<td colspan='2' class='rowGap'></td>
																				</tr>
																				<tr>
																					<td colspan="2" class="alignLeft"> 
																						<label class='hdFontColor' id="lblPolicy">Confirmation Number</label><label class="paddingL3">:</label>	
																						<label  id="lblPolicyValue1" class="fntBold"> </label>
																					</td>
																				</tr>
																				<tr>
																					<td colspan='2' class='rowGap'></td>
																				</tr>
																				<tr>
																					<td  class="alignLeft" colspan="2"> 
																						<label id="lblInsThank">Thank you for including Travel Insurance in your itinerary! Your application was successfully processed.
																						</label><br/><br/>
																							<label id="lblInsThank1">You will receive a Thank You email with confirmation details via your listed email address.
																						</label>
																					</td>
																				</tr>
																				<tr>
																					<td colspan='2' class='rowGap'></td>
																				</tr>
																				<tr>
																					<td colspan="2" align="center"> 
																						<div class="alignLeft insCoverNote">
																							<label id="lblInsNoteConfirmation">If you would have any inquires regarding your travel insurance, please contact AEC Insurance via email at {0} or by calling the ACE Customer Care Hotline at (+632) 849-6000
</label><br/><br/>
<label id="lblInsNoteConfirmation1">For emergency or assistance service, please call ACE Assistance 24-Hour Hotline (+632) 864-0865.
</label>
<br/><br/>
<label id="lblInsNoteConfirmation2">Contact the local telephone operator and ask for a reverse charge call to ACE Assistance.</label>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td colspan='2' class='rowGap'></td>
																				</tr>
																				<tr>
																					<td  class="alignLeft" colspan="2"> 
																						<label id="lblsafeFlightMsg">We wish you a happy and safe travel.
																						</label>
																					</td>
																				</tr>
																				<tr>
																					<td  class="alignRight" colspan="2"> 
																						<img src='../images/v3/ibeInsLogo.jpg' alt='ACE Insurance' />
																					</td>
																				</tr>
																			</table>
																			<%@ include file="../common/includeFrameBottom.html"%>
																			</table>																		
																		</td>																		
																	</tr>	
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="alignLeft">
										<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
											<%@ include file="../common/includeFrameTop.html"%>
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td colspan="3">
														<label id="lblItinerary" class='fntBold hdFontColor'>Your Itinerary</label>
													</td>
												</tr>
												<tr><td colspan="3" class="rowGap"></td></tr>
												<tr>
													<td id="depFlightPanel" style="display: none;">
														<table width='100%' border='0' cellpadding='0' cellspacing='0'>
															<tr>
																<td>
																	<!--<label id="lblDeparting" class='fntBold hdFontColor'></label> -->
																</td>
															</tr>
															<tr><td class="rowGap"></td></tr>
															<tr id="departueFlight">
																<td>
																	<table width='100%' border='0' cellpadding='3' cellspacing='0'>
																		<tr>
																		<td colspan="3" class='gridHD defaultRowGap'><label id="segmentCode" class='gridHDFont fntBold'></label></td></tr>
																		<tr>
																			<td width="50%" class='defaultRowGap '>
                                                                                <label id="lblDeparture"></label>
                                                                            </td>
																			<td width="30%" class='defaultRowGap '>
                                                                                <label id="departureDate" class="date-disp"></label>
                                                                                <label id="departureDateValue" class="date-hid" style="display:none"></label>
                                                                            </td>
																			<td width="10%" class='defaultRowGap '><label id="departureTime"></label></td>
																		</tr>
																		<tr>
																			<td width="50%" class='defaultRowGap GridTable'><label id="lblArrival"></label></td>
																			<td width="30%" class='defaultRowGap GridTable'>
                                                                                <label id="arrivalDate" class="date-disp"></label>
                                                                                <label id="arrivalDateValue" class="date-hid" style="display:none"></label>
                                                                            </td>
																			<td width="10%" class='defaultRowGap GridTable'><label id="arrivalTime"></label></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
													<td id="trReturnFltGap" style="display: none;">&nbsp;</td>
													<td id="trReturnFlt" style="display: none;">
														<table width='100%' border='0' cellpadding='0' cellspacing='0'>
															<tr>
																<td>
																	<label id="lblReturning" class='fntBold hdFontColor'></label>
																</td>
															</tr>
															<tr><td class="rowGap"></td></tr>
															<tr id="returnFlight">
																<td>
																	<table width='100%' border='0' cellpadding='3' cellspacing='0'>
																		<tr>
																		<td colspan="3" class='gridHD defaultRowGap'><label id="segmentCode" class='gridHDFont fntBold'></label></td></tr>
																		<tr>
																			<td width="50%" class='defaultRowGap '><label id="lblDeparture"></label></td>
																			<td width="30%" class='defaultRowGap '>
                                                                                <label id="departureDate" class="date-disp"></label>
                                                                                <label id="departureDateValue" class="date-hid" style="display:none"></label>
                                                                            </td>
																			<td width="10%" class='defaultRowGap '><label id="departureTime"></label></td>
																		</tr>
																		<tr>
																			<td width="50%" class='defaultRowGap GridTable'><label id="lblArrival"></label></td>
																			<td width="30%" class='defaultRowGap GridTable'>
                                                                                <label id="arrivalDate" class="date-disp"></label>
                                                                                <label id="arrivalDateValue" class="date-hid" style="display:none"></label>
                                                                            </td>
																			<td width="10%" class='defaultRowGap GridTable'><label id="arrivalTime"></label></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<%--
													<tr id="depFlightPanel" style="display: none;">
														<td>
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>											
																<tr>
																	<td colspan='2' valign='top' align='center'>
																		<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">
																			<tr>
																				<td class="Gridwt alignLeft" colspan="7"> <label id="lblDeparting" class='fntBold hdFontColor'></label></td>
																			</tr>
																			<tr>
																				<td rowspan="2" class='gridHD' align="center"><label id="lblSegment" class='gridHDFont fntBold'></label></td>
																				<td colspan="2" class='gridHD' align="center"><label id="lblDeparture" class='gridHDFont fntBold'> </label></td>
																				<td colspan="2" class='gridHD' align="center"><label id="lblArrival" class='gridHDFont fntBold'></label></td>
																				<td rowspan="2" class='gridHD' align="center"><label id="lblDFlightNo" class='gridHDFont fntBold'></label></td>
																				<td rowspan="2" class='gridHD' align="center"><label id="lblDuration" class='gridHDFont fntBold'></label></td>
																			</tr> 
																			<tr>            
																				<td class='gridHDDark' align="center"><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
																				<td class='gridHDDark' align="center"><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
																				<td class='gridHDDark' align="center"><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
																				<td class='gridHDDark' align="center"><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
																			</tr>
																			<tr id="departueFlight">
																				<td width='35%' class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="segmentCode"></label></td>
																				<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="departureDate"></label></td>
																				<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="departureTime"></label></td>
																				<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
																				<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
																				<td width='10%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
																				<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>															
																			</tr>            
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='2' class='rowGap'></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr id="trReturnFlt" style="display: none;">
														<td>
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>											
																<tr>
																	<td colspan='2'  valign='top' align='center'>
																		<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">
																			<tr>
																				<td class="Gridwt alignLeft" colspan="7"><label id="lblReturning" class='fntBold hdFontColor'> </label></td>
																			</tr>
																			<tr>
																				<td rowspan="2" class='gridHD' align="center"><label id="lblRSegment" class='gridHDFont fntBold'></label></td>
																				<td colspan="2" class='gridHD' align="center"><label id="lblRDeparture" class='gridHDFont fntBold'> </label></td>
																				<td colspan="2" class='gridHD' align="center"><label id="lblRArrival" class='gridHDFont fntBold'></label></td>
																				<td rowspan="2" class='gridHD' align="center"><label id="lblRFlightNo" class='gridHDFont fntBold'></label></td>
																				<td rowspan="2" class='gridHD' align="center"><label id="lblDuration" class='gridHDFont fntBold'></label></td>
																			</tr> 
																			<tr>            
																				<td class='gridHDDark' align="center"><label id="lblRDepartureDate" class='gridHDFont fntBold'></label></td>
																				<td class='gridHDDark' align="center"><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
																				<td class='gridHDDark' align="center"><label id="lblRArrivalDate" class='gridHDFont fntBold'></label></td>
																				<td class='gridHDDark' align="center"><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
																			</tr>
																			<tr id="returnFlight">
																				<td width='35%' class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="segmentCode"></label></td>
																				<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="departureDate"></label></td>
																				<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="departureTime"></label></td>
																				<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
																				<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
																				<td width='10%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
																				<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>															
																			</tr>                
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='2' class='rowGap'></td>
																</tr>
																<tr>
																	<td colspan='2' class='rowGap'></td>
																</tr>
															</table>
														</td>
													</tr>
													 --%>
													</table>
											<%@ include file="../common/includeFrameBottom.html"%>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan='2' class='rowGap'></td>
								</tr>
								<tr id="trPaxDetail" style="display: none;">
									<td class="alignLeft">
										<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
											<%@ include file="../common/includeFrameTop.html"%>
												<table width="100%" border="0" cellspacing="0" cellpadding="3">
														<tr>
															<td>
																<label id="lblPaxInfo" class='hdFontColor fntBold'></label>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr id="tradultfGrid">
															<td class='noPadding gridHD'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>				
																		<td class='gridHDDark' width="1%"></td>																
																		<td class='gridHDDark alignLeft'  width='59%' ><label id="lblAdultHD" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='40%'><label id="lblServices" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr id="adultGrid">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'  width="1%"><label id="itnSeqNo"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'><label id="itnPaxName"></label></td>
																		<td  class='defaultRowGap rowColor bdBottom bdRight'><span id="additionalInfo" class="fntDefault"></span></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr id="trChildGrid">
															<td class='noPadding'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>		
																		<td class='gridHDDark' width="1%"></td>																
																		<td class='gridHDDark alignLeft' width='59%' ><label id="lblChildHD" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='40%'><label id="lblServices" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr id="childGrid">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight' width="1%"><label id="itnSeqNo"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight' width='59%'><label id="itnPaxName"></label></td>
																		<td class="defaultRowGap rowColor bdBottom bdRight" width='60%'><span id="additionalInfo" class="fntDefault" ></span></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr id="trInfGrid">
															<td class='noPadding'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>					
																		<td class='gridHDDark' width="1%"></td>																
																		<td class='gridHDDark alignLeft'  width='59%'><label id="lblInfantHD" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='40%'><label id="lbltravelwith" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr id="infantGrid">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'  width="1%"><label id="itnSeqNo"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'  width='59%'><label id="itnPaxName"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'  width='40%'><label id="itnTravellingWith"></label></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														</table>
														<%@ include file="../common/includeFrameDivideTop.html" %>
														<tr><td colspan="3">
														<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="display: none;" id="tblFlexiDetails">
														<%@ include file="../common/includeFrameDivideBottom.html" %>
														<table width="100%" border="0" cellspacing="0" cellpadding="3">
														<tr>
															<td class="gridHD">
																<label id="lblFlexiHeader" class='gridHDFont fntBold'></label>
															</td>
														</tr>
														<tr id="trFlexiGrid">
															<td class='noPadding'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>																	
																		<td class='gridHDDark alignLeft'  width='30%'><label id="lblOriginDestination" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='70%'><label id="lblFlexiInfo" class='gridHDFont fntBold'></label></td>
																	</tr>																
																	<tr id="flexiInfo">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'  width='30%'><label id="originDestination"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'  width='70%'><label id="flexiDetails" class="fntDefault"></label></td>
																	</tr>
																</table>
															</td>
														</tr>
														</table>
														</table>
														</td></tr>
														<%@ include file="../common/includeFrameBottom.html"%>
													</table>
												</td></tr>
												<tr>
													<td class='rowGap'></td>
												</tr>
												<tr><td class="alignLeft">
													<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<%@ include file="../common/includeFrameTop.html"%>
																<table border='0' cellspacing='0' cellpadding='1' width='100%' id="payDetail">	
																	<tr>
																		<td class='' colspan="2" class="alignLeft">
																			<label name="lblPaymentInfo" id="lblPaymentInfo" class='hdFontColor fntBold'></label>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap' colspan="2"></td>
																	</tr>
																	<tr id="paymentDetails">
																		<td class='defaultRowGap alignLeft rowColor paddingL5'><label id="labelName" ></label></td>
																		<td class="alignRight rowColor paddingR5"><label id="value"></label></td>
																	</tr>	
																	<%--														
																	<tr name="trAirFare">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft' ><label id="lblTotAirFare">T</label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valAirFare' ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr>																	
																	<tr name="trCharges">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft'  ><label id="lblTotTaxSurcharge"></label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valCharges' ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr>
																	<tr name="trFlexiCharges">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft'  ><label id="lblFlexiCharges"></label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valFlexiCharges' ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr>
																	<tr name="trSeatCharges">
																		<td class=' bdLeft bdBottom bdRight alignLeft' ><label id='lblSeatSelection'></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name="valSeatCharges" ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr> 
																	<tr name="trInsuranceCharges">
																		<td class=' bdLeft bdBottom bdRight alignLeft'><label id='lblTravelSecure' ></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name="valInsuranceCharges" ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr> 																				
																	<tr name="trMealCharges">
																		<td class=' bdLeft bdBottom bdRight alignLeft'><label id='lblMealSelection'></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name="valMealCharges" ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr> 																	 
																	<tr name="trTxnFee">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft' ><label id="lblTNXFee"> </label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valTxnFee' ></label>&nbsp;<label name='valBaseCurrCode'></label></td>
																	</tr>   																	 
																	<tr>
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft' ><label id="lblTotal" class='fntBold'></label></td>
																		<td class=' bdRight bdBottom' align='right'><label name='valTotal' class='fntBold' ></label>&nbsp;<label name='valBaseCurrCode'  class='fntBold'></label></td>
																	</tr> 
																	<tr id="paymentSelectedCurrCode" style="display: none;">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft'><label id="lblTotal" class='fntBold'></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name='valSelectedTotal' class='fntBold'></label>&nbsp;<label name='valSelectedCurrCode' class='fntBold'></label></td>
																	</tr>  --%>
																	<tr>
																		<td colspan='2' class='rowGap'></td>
																	</tr>
															</table>
													<%@ include file="../common/includeFrameBottom.html"%>     			
												</table>
										</td>
								</tr>
								<tr>
									<td class='rowGap'></td>
								</tr>
								<tr id="trContactDetail" style="display: none">
									<td colspan='2'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
											<%@ include file="../common/includeFrameTop.html"%>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">														
														<tr>															
															<td>
																<div class="div-Table alignLeft">
																		<div class="table-Row one-Col">
																			<div class="table-Data one-Col">
																				<label id="lblCotactInfo" class='hdFontColor fntBold'></label>
																			</div>
																		</div>
																		<div class="table-Row one-Col">
																			<div class="table-Data one-Col">
																				<label id="lblName" class="fntBold">Contact Person Name</label>
																			</div>
																			<div class="table-Data one-Col">
																				<font>:</font>
																				<label id="paxTitle" class=""></label>&nbsp;
																				<label id="firstname" class="fontCapitalize  "></label>&nbsp;
																				<label id="lasttname" class="fontCapitalize "></label>
																			</div>
																		</div>
																		
																		<div class="table-Row three-Col">
																			<div class="table-Data left-TD"><label id="lblNationality" class="fntBold"></label></div>
																			<div class="table-Data one-Col">
																				<font>:</font>
																				<label id="nationality" class=""></label>
																			</div>
																		</div>
																		
																		<div class="table-Row three-Col" style="display: none">
																			<div class="table-Data left-TD"><label id="lblcity" class="fntBold"></label></div>
																			<div class="table-Data right-TD">
																				<font>:</font>
																				<label id="city" class=""></label>
																			</div>
																		</div>
																		
																		<div class="table-Row three-Col">
																			<div class="table-Data left-TD"><label id="lblCountry" class="fntBold"></label></div>
																			<div class="table-Data right-TD">
																				<font>:</font>
																				<label id="Country" class=""></label>
																			</div>
																		</div>
																		
																		<div class="table-Row three-Col">
																			<div class="table-Data left-TD"><label id="lblMobile" class="fntBold"></label></div>
																			<div class="table-Data right-TD">
																				<font>:</font>
																				<label id="mobile" class=""></label>
																			</div>
																		</div>
																		
																		<div class="table-Row three-Col">
																			<div class="table-Data left-TD"><label id="lblPhone" class="fntBold"></label></div>
																			<div class="table-Data right-TD">
																				<font>:</font>
																				<label id="phone" class=""></label>
																			</div>
																		</div>
																		
																		<div class="table-Row one-Col">
																			<div class="table-Data one-Col"><label id="lblEmailAddress" class="fntBold"></label></div>
																			<div class="table-Data one-Col">
																				<label id="lblEmailLable">
																					A copy of a your reservation has been sent your at the following email address :
																				</label>
																			</div>
																			<div class="table-Data one-Col">
																				<font>:</font>
																				<label id="emailAddress" class=""></label>
																			</div>
																		</div>
																	</div>
														
															<!-- 
																<table border='0' cellpadding='3' cellspacing='0' width='100%' class="">
																	<tr>
																		<td class='gridHD alignLeft' colspan="4"><label id="lblCotactInfo" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr>
																		<td width="25%" class='defaultRowGap rowColor alignLeft'><label id="lblTitle"></label></td> 
																		<td width="25%" class='rowColor alignLeft'>:<label id="paxTitle" class="fntBold paddingL5"></label></td>        				
																		<td class='rowColor'></td>      				
																		<td class='rowColor'></td>    
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate alignLeft' ><label id="lblFirstname"></label></td>
																		<td class='rowColorAlternate alignLeft'>:<label id="firstname" class="fontCapitalize fntBold paddingL5"></label></td>
																		<td class='rowColorAlternate alignLeft' width="25%" ><label id="lblLasttname"></label></td>
																		<td class='rowColorAlternate alignLeft' >:<label id="lasttname" class="fontCapitalize fntBold paddingL5"></label></td>
																	</tr>
																	<tr>																		
																		<td class='defaultRowGap rowColor alignLeft'><label id="lblNationality"></label></td>
																		<td class='rowColor alignLeft'>:<label id="nationality" class="fntBold paddingL5"></label></td>
																		<td class='rowColor alignLeft'><label id="lblcity"></label></td>
																		<td class='rowColor alignLeft'>:<label id="city" class="fontCapitalize fntBold paddingL5"></label></td>
																	</tr>																	
																	<tr>
																		<td class='defaultRowGap rowColorAlternate alignLeft'><label id="lblCountry"></label></td>
																		<td class='rowColorAlternate alignLeft'>:<label id="Country" class="fntBold paddingL5"></label></td>
																		<td class='rowColorAlternate alignLeft'><label id="lblMobile"></label></td>
																		<td class='rowColorAlternate alignLeft' >:<label id="mobile" class="fntBold paddingL5"></label></td>
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColor alignLeft'><label id="lblPhone"></label></td>
																		<td class='rowColor alignLeft'>:<label id="phone" class="fntBold paddingL5"></label></td>
																		<td class='rowColor alignLeft'>&nbsp;</td>
																		<td class='rowColor alignLeft'>&nbsp;</td>																		
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate alignLeft' class="alignLeft"><label id="lblEmailAddress"></label></td>
																		<td class='rowColorAlternate alignLeft'>:<label id="emailAddress" class="fntBold paddingL5"></label></td>
																		<td class='rowColorAlternate'></td>
																		<td class='rowColorAlternate'></td>
																	</tr>																	
																</table>
																 -->
															</td>															
														</tr>
													</table>
												<%@ include file="../common/includeFrameBottom.html"%>
												</table>
												</td>
											</tr>
											<tr>
												<td class='rowGap'></td>
											</tr>
											<%-- This should be enable only airmed, Temp disable for tag build --%>
											<tr id="trDispEmgnContact" style="display: none;">												
												<td colspan='2' class='' valign='top' class="alignLeft">
												<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
													<%@ include file="../common/includeFrameTop.html"%>
														<table width="100%" border="0" cellspacing="0" cellpadding="0">														
															<tr>															
																<td>
																	<div class="div-Table alignLeft">
																		<div class="table-Row one-Col">
																			<div class="table-Data one-Col">
																				<label id="lblEmgnCntInformation" class='hdFontColor fntBold'></label>
																			</div>
																		</div>
																		<div class="table-Row one-Col">
																			<div class="table-Data one-Col">
																				<label id="lblEmgngName" class="fntBold">Emergency  Contact Person Name</label>
																			</div>
																			<div class="table-Data one-Col">
																				<font>:</font>
																				<label id="emgnPaxTitle" class=""></label>&nbsp;
																				<label id="emgnFirstName" class="fontCapitalize "></label>&nbsp;
																				<label id="emgnLastName" class="fontCapitalize "></label>
																			</div>
																		</div>
																		<div class="table-Row three-Col">
																			<div class="table-Data left-TD"><label id="lblPhone" class="fntBold"></label></div>
																			<div class="table-Data one-Col">
																				<font>:</font>
																				<label id="emgnPhoneNo" class=""></label>
																			</div>
																		</div>
																		<div class="table-Row three-Col">
																			<div class="table-Data left-TD"><label id="lblEmailAddress" class="fntBold"></label></div>
																			<div class="table-Data right-TD">
																				<font>:</font>
																				<label id="emgnEmail" class=""></label>
																			</div>
																		</div>
																	</div>
																</td>															
															</tr>
															<tr>
																<td class='rowGap'></td>
															</tr>
													</table>
													<%@ include file="../common/includeFrameBottom.html"%>
													</table>
												</td>												
								</tr>
							
								
								<tr class="bookhotelRow">
									<td colspan='2'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
											<%@ include file="../common/includeFrameTop.html"%>
												<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
												<tr id="rowBookHotelOrCarSeperator" style="display:none;">
												</tr>
												<tr id="rowBookHotelOrCar" style="display:none;"><td class='pnlBottom' valign='top' align="center">
													<table width='98%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<%--
															<tr>
																<td class="alignLeft">
																<table><tr>
																<td width="35%">
																	<a id="linkBookHotel" target="_blank" href="">
																		<span class="fntBig" id="lblBookaHotel">Book A Hotel</span><br>
																		<span class="fntSmall" id="lblBookaHotelDesc">for confertable stay at you destination</span>
																	</a>
																</td>
																<td width="15%"></td>
																<td width="35%">
																	<a id="linkRentCar" target="_blank" href="">
																		<span class="fntBig" id="lblRentaCar">Rent a Car</span><br>
																		<span class="fntSmall" id="lblRentaCarDesc">and enjoy the comfort on arival</span>
																	</a>
																</td>
																</tr></table>
																	
																	
																</td>
															</tr>
														--%>
														
														<tr>
															<td align='center'>
																<a id="linkBookHotel" target="_blank" href="">
																	<img src='../images/Hotel_booking_button_no_cache.gif' alt='Hotel Booking'/>
																</a>
																<a id="linkRentCar" target="_blank" href="">
																	<img src='../images/Car_booking_button_no_cache.gif' alt='Cate Booking'/>
																</a>
															</td>
														</tr>
														<tr>
															<td class="alignLeft">
																<label id="lblMsgBookAHotel" class='fntBold hdFontColor'></label>
															</td>
														</tr>
														<tr>
														   <td>
																<div id="divHotel">																			
																			<iframe name="frmhotel" id="frmhotel" src="showBlank" frameborder="0" width="650px" height="250px">
																			</iframe>		
																</div>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td class="alignLeft">
																<label id="lblMsgBookACar" class='fntBold hdFontColor'></label>
															</td>
														</tr>
														<tr>
														   <td>
																<div id="divRentCar">																			
																		<iframe name="frmrentcar" id="frmrentcar" src="showBlank" frameborder="0" width="650px" height="300px">
																		</iframe>		
																</div>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														 
													</table>
												</td></tr></table>
								<%@ include file="../common/includeFrameBottom.html"%>
								</table>
								</td>
								</tr>

								<tr>
									<td class='rowGap'></td>
								</tr>
								
								<tr class="inlineSocialPanel" style="display: none;" >
									<td colspan='2'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
											<%@ include file="../common/includeFrameTop.html"%>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td>
																<div class="div-Table alignLeft">
																		<div class="">
																				<label id="lblShareInSocialMedia" class='hdFontColor fntBold'>Share on Social Media</label>
																		</div>
																		<div class="socialBtnArea" style="top:0px;margin: 0 auto;height: 75px;">
																			<div class="refFacebook btnSocial fbook">&nbsp;</div>
																			<div class="refTwitter btnSocial twiter">&nbsp;</div>
																			<div class="refGoogle btnSocial googlep">&nbsp;</div>
																			<div class="refLinkedIn btnSocial linkIn">&nbsp;</div>
																	   </div>
															   </div>
															</td>
														</tr>
												</table>
											<%@ include file="../common/includeFrameBottom.html"%>
										</table>
									</td>
							    </tr>
								<tr class="inlineSocialPanel" style="display: none;" >
									<td class='rowGap'></td>
							   </tr>

								<tr id="trButtonPannel" style="display: none;">
									<td class="alignLeft" colspan='2'>
									<div class="buttonset">
										<!-- Button -->
										<table border='0' cellpadding='0' cellspacing='0' width="100%">
											<tr>
												<td class="alignLeft"><%--<input type="button" id="btnFinish"  value="Finish" class="Button"/>--%>
												    <u:hButton name="btnFinish" id="btnFinish" value="Finish"  cssClass="redNormal"/>
												</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td align="center">
                                                    <u:hButton name="btnPrint" id="btnPrint" value="Print"  cssClass="redNormal"/>
                                                </td>
                                                <td style="width:10px;">&nbsp;</td>
												<td class="alignRight">
												<u:hButton name="btnEmailMe" id="btnEmailMe" value="Email me"  cssClass="redNormal"/>
												<%--<input type="button" id="btnPrint"  value="Print" class="Button" /></td>--%>
                                                </td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
									<tr>
										<td class='rowGap' colspan='2'></td>
									</tr>
									<tr>
										<td class='rowGap' colspan='2'></td>
									</tr>
							</table>
						</div>
					</div>
					</div>
					</td>
					</tr>
					<!-- Bottom AA Logo -->
					<tr>
						<td class='appLogo' colspan="2"></td>
					</tr>
				</table>
			</td>
		</tr>
		<c:import url="../../../../ext_html/cfooter.jsp" />
	</table>
	<input type="hidden" id="hdnMode" name="hdnMode"/>
	<%@ include file='../common/iBECommonParam.jsp'%>		
 </form> 
 </div>
 
<div id='divLoadMsg' class="mainPageLoader">
	<%@ include file='../common/includeLoadingMsg.jsp' %>
</div>
<form id="submitForm" method="post">
	<input type="hidden" name="pnr" value="<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />" />
</form>
<script src="../js/v2/reservation/interLineConfirmation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<!-- <div id='fb-root' style="display: none"></div>  -->
<div id="newPopItem" style="display: none;" >
	<%@ include file='../../common/includePopup.jsp' %>
</div>
<div style="display:none">
	<c:import url="../../../../ext_html/inc_ConfPromo.jsp" />
</div>
<%-- Use to load third party tools and tracking codes--%>
<div id="accelAeroIBETrack" style="display: none">
	<iframe name="frmTracking" id="frmTracking" src="showBlank" frameborder="0">
	</iframe>	
</div>
<div id="pixelTracker"></div>
<div id="accelAeroIBETrackYandex" style="display: none;"></div>
</body>
<%@ include file='../common/inline_Tracking.jsp' %>
<%@ include file='../common/thirdPartyTracking.jsp' %>
</html>