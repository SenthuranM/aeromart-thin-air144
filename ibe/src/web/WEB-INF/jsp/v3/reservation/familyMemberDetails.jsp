<table>
	<tr>
		<td>
			<label class="fntBold hdFontColor" id="lblSelectFamilyMembers"> Select Family Member Details </label>
		</td>
	</tr>
	<tr id="trFamilyMembersList" style="background-color: gray;">
		<td colspan="4">
			<div id="familyMembersList">
				<table border="0" cellpadding="0" cellspacing="0" align="center"
					class="GridTable" id="familyMembersTbl">
					<tr>
						<td class="segcodebar bdRight" align="left" style="width: 150px"><label
							class="gridHDFont fntBold" id="lblFMFirstNameHD">First
								Name</label></td>
						<td class="segcodebar bdRight" align="left" style="width: 150px"><label
							class="gridHDFont fntBold" id="lblFMLastNameHD">Last Name</label></td>
						<td class="segcodebar bdRight" align="left" style="width: 60px"><label
							class="gridHDFont fntBold" id="lblFMRelationshipHD">Relationship</label></td>
						<td class="segcodebar bdRight" align="left" style="width: 82px"><label
							class="gridHDFont fntBold" id="lblFMNationalityHD">Nationality</label></td>
						<td class="segcodebar" align="left" style="width: 85px"><label
							class="gridHDFont fntBold" id="lblFMDOBHD">Date of Birth</label></td>
					</tr>
					<tbody id="familyMembersTable">
					</tbody>
				</table>
			</div>
		</td>
	</tr>
	
	<tr>
    	<td class="rowGap" colspan="2"></td>
    </tr>
    
    <tr>
    	<td colspan="2">
    		<input type="button" id="loadFamilyDetailsOK" class="Button" value="Ok" autocomplete="off" name="loadFamilyDetails_popup_ok_button">
    		<input type="button" id="loadFamilyDetailsCancel" class="Button" value="Cancel" autocomplete="off" name="loadFamilyDetails_popup_cancel_button">
    	</td>
    </tr>
</table>