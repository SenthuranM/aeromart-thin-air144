
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script type="text/javascript">
		$(function() {
			$( "#sliderLoyaltyRedeem" ).slider({
				range: "min",
				value: 0,
				min: 0,
				max: 1500,
				slide: function( event, ui ) {
					$( "#amount" ).html( "$" + ui.value );
				}
			});
			$( "#amount" ).html( "$" + $( "#sliderLoyaltyRedeem" ).slider( "value" ) );
		});
	</script>

<table width='100%' border='0' cellpadding='0' cellspacing='0'>
<tr>
	<td colspan="2" class="paymentGadient">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td class="rowSingleGap">
				</td>
				</tr>
				<tr>
				<td class="alignLeft" colspan="2" style="padding-left: 5px;">
					<label id="lblPayment" class="hdFontColor fntBold"></label>
				</td>
				</tr>
				<tr>
			<td colspan='2' class='rowGap'></td>
		</tr>
		<tr>
		<td colspan='2'>
			<div id="specialMsg">
				<div class="spMsg alignLeft">
					<span ></span>
				</div>
			</div>
			<div id="trpayPageAirportMsg" style="margin: 6px 0 0">
				<div class="spMsg alignLeft">
					<span id="payPageAirportMsg"></span>
				</div>
			</div>
				<div id="spnPayType" style="display: none;margin: 6px 0 0">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<%@ include file="../common/includeFrameTop.html"%>
					<div class="alignLeft" style="padding-left: 50px;">
						<table cellspacing="0" cellpadding="2" border="0" width="60%">
							<tr>
								<td width="40%" class="alignLeft">
									<label id="lblPaymentMode"></label>
								</td>
							</tr>
							<tr><td colspan="4" class="rowSingleGap">
							
							</td></tr>
							<tr> 
								<td id="tdPayLater">		
									<input type="radio" checked="checked"  value="ONHOLD" class="noBorder" name="selPayType" id="selPayType"/>
									<label id="lblPayAtCashier">  </label>&nbsp;
								</td> 
								<td id="tdPayByCC" width="35%"><div id="payByCard">
									<input type="radio"  value="NORMAL" class="noBorder" name="selPayType" id="selPayType1">
									<label id="lblPayByCreditCard"> </label> </div>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="rowSingleGap">				
								</td>
							</tr>
							<tr id="trOnholdDuration">
								<td colspan="2" class="alignLeft rowSingleGap">
									<div style="z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);">
										<label class="fntBold" id="lblOnHoldState"> On-Hold Duration :</label> <label style="display:none" class="fntBold" id="onHoldDisplayTime"></label>	
									</div>	
								</td>
							</tr>
						</table>
					</div>
					<%@ include file="../common/includeFrameBottom.html"%>
				</table>
				</div>
		</td>
		</tr>	
		
	<tr>
		<td class='rowGap'></td>
	</tr>
	
	<tr id="userLoginPanel" style="display: none">
		<td colspan="2">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<%@ include file="../common/includeFrameTop.html"%>
						<div class="div-Table" >
							<div class="table-Row one-Col" style="height: auto">
                                <div class="table-Data one-Col">
                                    <img src="../images/air-rewards_no_cache.png" alt="Air Rewards" height="25" style="padding:0px 0px 0px 0px;"/>
                                </div>
                            </div>
							<div class="table-Row one-Col" style="height: auto">
                                <div class="table-Data one-Col">
                                    <label class="fntBold" id="lblLMSLoginUser">Are you an existing member of Airewards? Login now and use your points to pay.</label>
                                </div>
                            </div>
                            <div class="table-Row one-Col" style="height: auto">
                                <div class="table-Data one-Col">
                                    <label class="fntBold" id="lblLMSLoginEmail">Email</label>&nbsp;&nbsp;
                                    <input type="text" id="lmsLoginEmail" name="lmsLoginEmail" maxlength="100" style="width: 180px;height: 16px;" autocomplete="off"/>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="fntBold" id="lblLMSLoginPassword">Password</label>&nbsp;&nbsp;
                                    <input type="password" id="lmsLoginPassword" name="lmsLoginPassword" maxlength="12" style="width: 180px;height: 16px" autocomplete="off"/>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="button" id="btnLmsLogin" name="btnLmsLogin" class="Button ButtonMedium" >Log In</button>
                                </div>
                            </div>
						</div>
						<%@ include file="../common/includeFrameBottom.html"%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr id="payByLMSPointsPanel" style="display: none">
		<td colspan="2">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<%@ include file="../common/includeFrameTop.html"%>
                        <div class="div-Table" >

                            <div class="table-Row one-Col" style="height: auto">
                                <div class="table-Data one-Col">
                                    <img src="../images/air-rewards_no_cache.png" alt="Air Rewards" height="25" style="padding:0px 0px 0px 0px;"/>
                                </div>
                            </div>
                            <div class="table-Row one-Col" style="height: auto">
                                <div class="table-Data one-Col">
                                    <label class="fntBold" id="lblLMSPayReq">Do you want to pay using your Airewards points?</label>
                                    <input type="radio" id="payByLoyalty_yes" name="payByLoyalty" value="yes">&nbsp;
                                    <label id="lblYes">Yes</label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="payByLoyalty_no" name="payByLoyalty" value="no">&nbsp;
                                    <label id="lblNo">No</label>
                                </div>
                            </div>
                            <div id="payByAirewardPoints" class="alignLeft table-Row one-Col" style="display: none">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                   <tr>
                                       <td><label class="fntBold" id="lblAirewardsID">Airewards ID</label></td>
                                       <td><label>:</label><label id='lmsFFID' class="fntBold">xxxx@yyyy.zz</label></td>
                                   </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                    <tr>
                                        <td><label id="lblLMSAvailablePoints">Available points</label></td>
                                        <td><label>:</label><label id='lmsTotalAvailablePoints' class="fntBold"></label></td>
                                    </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                    <tr>
                                        <td><label id="lblLMSEqvAmount">Equivalent Amount</label></td>
                                        <td><label>:</label><label id='lmsTotalAvailablePointsAmount' class="fntBold"></label></td>
                                    </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                    <tr>
                                        <td><label id="lblMaxRedeemAmount">Maximum Redeemable Amount</label></td>
                                        <td><label>:</label><label id='lmsMaxRedeemablePointsAmount' class="fntBold"></label></td>
                                    </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                    <tr>
                                        <td colspan="2"><label id="lblLMSSliderLabel">How much you would like to redeem?</label></td>
                                    </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <table width="75%" cellpadding="10"><tr>
                                                <td width="75%"><div id="sliderLoyaltyRedeem"></div></td>
                                                <td class="alignLeft"><label id="amount" class="fntBold"></label></td>
                                            </tr></table>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                    <tr>
                                        <td colspan="2" >
                                            <ul>
                                                <li><label id="lblLMSCondt1">to redeem on flights & baggage you need at least 4,000 points.</label></li>
                                                <li><label id="lblLMSCondt2">to redeem on seats, meals, insurance and airport services there are no threshold.</label></li>
                                                <li><label id="lblLMSCondt3">Taxes cannot be redeemed.</label></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                    <tr>
                                        <td colspan="2" class="alignRight">
                                            <button type="button" id="redeemPts" name="redeemPts"  class="Button ButtonLargeMedium" >Confirm Redemption</button>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="rowGap"></td></tr>
                                </table>
                                <div id="LMS_Redeemed_Panel" style="display: none" class="table-Row one-Col">
                                    <table width="100%">
                                        <tr>
                                            <td width="50%" align="center">
                                            <span class="loader-points" style="display: none">
                                            <img src="../images/loading-bar-small_no_cache.gif" /><br/>
                                            <label id="lblLmsRedemptionComplete" class="fntExSmall fntBold">Redemption Complete...</label>
                                            </span>
                                            </td>
                                            <td width="50%">
                                                <label id="confirmdRedeem" class="hdFontColor fntBold"> </label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>


                        </div>
						<%@ include file="../common/includeFrameBottom.html"%>
					</td>
				</tr>
			</table>
		</td>		
	</tr>
	<tr><td colspan="2" class="rowGap"></td></tr>
	<tr id="payByVoucherPanel" >
		<td colspan="2">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<%@ include file="../common/includeFrameTop.html"%>
                        <div class="div-Table" >

                            <div class="table-Row one-Col" style="height: auto">
                                <div class="table-Data one-Col">
                                    <label class="fntBold" id="lblVoucherPayReq"></label>
                                    <input type="radio" id="payByVoucher_yes" name="payByVoucher" value="yes">&nbsp;
                                    <label id="lblVoucherYes"></label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="payByVoucher_no" name="payByVoucher" value="no">&nbsp;
                                    <label id="lblVoucherNo"></label>
                                </div>
                            </div>
                            <div id="payByVouchers" class="alignLeft table-Row one-Col" style="display:none">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">                                   
                                   <tr id="trVoucher">
										<td valign="top" width="30%">
											<label id="lblVoucherIDInput">Insert voucher IDs to redeem</label>
										</td>
										<td width="70%">
											<table id="tblVoucherRedeem">
												<tr>
													<td>
														<table id="tblVoucherIDs">
															<tr id='voucherSet_0'>
																<td><input type="text" id="voucherID_0"></td>
																<td valign="top">														
																	<input type="button" name="btnRemoveVoucher" class="Button" id="btnRemoveVoucher_0" value="-" onclick="UI_Payment.removeVoucher(this.id)"/>
																	<input type="button" name="btnAddVoucher" class="Button" id="btnAddVoucher_0" value="+" onclick="UI_Payment.addVoucher(this.id)"/>
																</td>
															</tr>
														</table>
													</td>													
												</tr>
											</table>
										</td>					
									</tr>
									<tr>
                                        <td colspan="2" class="alignRight">
                                            <button type="button" id=redeemVouchers name="redeemVouchers"  class="Button ButtonLargeMedium" ></button>
                                        </td>
                                    </tr>							
                                </table>
                                <div id="Voucher_Redeemed_Panel" class="table-Row one-Col" style="display:none">
                                    <table width="100%">
                                        <tr id="trVoucherProgressBar" style="display:none">
                                            <td width="50%" align="center" id="tdVoucherProgressBar">
	                                            <img src="../images/loading-bar-small_no_cache.gif" /><br/>
	                                            <label id="lblVoucherRedemptionComplete" class="fntExSmall fntBold"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50%">
                                                <label id="confirmdVoucherRedeem" class="hdFontColor fntBold"> </label>
                                            </td>
                                            <td width="50%">
                                                <label id="voucherIDList" class="hdFontColor fntBold"> </label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>                                	
                            </div>
                        </div>
						<%@ include file="../common/includeFrameBottom.html"%>
					</td>
				</tr>
			</table>
		</td>		
	</tr>		
	
			
	<tr>
		<td class='rowGap'></td>
	</tr>
	<tr id="payByCreditCardPanel" style="display: none">
	  <td>
	  
		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td>
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='tblLoyalty' class="alignLeft" style="display: none;">
					<tr>
						<td>
							<label id="lblLoyaltyCredit_1"></label>
							 <strong><span id="spnloyaltyCredit"></span></strong> <label id="lblLoyaltyCredit_2"></label>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr id="trLoyalPayFull">
									<td  width="25">
										<input type="radio" name="loyaltyOption" value="total" />
									</td>
									<td>
										<label id="lblLoyaltyUseAll"></label>
									</td>
								</tr>
								<tr>
									<td>
										<input type="radio" name="loyaltyOption" value="part" />
									</td>
									<td>
										<label id="lblLoyaltyUseAmt_1"></label>
											<input type="text" name="loyaltySetteledAmount" id="loyaltySetteledAmount" size="9" maxlength="9" />
										<label id="lblLoyaltyUseAmt_2"></label>
									</td>
								</tr>
								<tr>
									<td>
										<input type="radio" name="loyaltyOption" value="none" />
									</td>
									<td>
										<label id="lblLoyaltyNo"></label>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="100%" >
					<tr>
						<td class='rowGap'></td>
					</tr>
					<tr>
						<td align="left"><input type="button" name="btnRecalculate" id="btnRecalculate"  tabindex="23"  value="Recalculate" class="Button ButtonMedium"/></td>
					</tr>
				</table>
			</td>
		</tr>				
		<!-- Credit place holder -->
		<tr>
			<td class='rowGap' colspan='2'></td>
		</tr>
		
		<tr>
			<td class="" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<%@ include file="../common/includeFrameTop.html"%>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 5px;">
					<tr>
						<td class="alignLeft">
							<label id="lblMsgReviewPayment"></label>
						</td>
					</tr>
					<tr><td class='rowGap'></td></tr>
				</table>
			<%@ include file="../common/includeFrameDivideTop.html"%>
			
			<tr>
			<td class='pnlMid' colspan="3">
				<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable" id="modifyBal" style="display: none">										
						<tr>
							<td colspan="3" class="alignLeft">
							<label id="lblBalancePayment" class="fntBold fntRed"></label></td>
						</tr>
						<tr>
							<td
								class="gridHD"><font>&nbsp;</font></td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblModifingFlight" class="fntBold fntWhite">Modifying Flight(s)&nbsp;</label>
							</td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblNewFlight" class="fntBold fntWhite">New Flight(s)&nbsp;</label></td>
						</tr>						
						<tr id="balanceSummaryTemplate">
							<td
								class="SeperateorBGAlt rowDataHeight GridItems alignLeft">
								<label id="displayDesc"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayOldCharges" class="fntRed"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayNewCharges" class="fntRed"></label>
							</td>
						</tr>
				</table>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="3" id="tblPriceBreakdown" class="totalCol">
					<tr>
						<td style="background: transparent;"><label id="lblTotalDue" class='fntBold fntWhite'></label></td>
						<td style="background: transparent;"><label id="lblTotalDueAmount" name="lblTotalDueAmount" class='fntBold fntWhite'></label>  <label name='valBaseCurrCode' class='fntBold fntWhite'></label></td>
					</tr> 
					<tr id="paymentSelectedCurrCode" style="display: none;">
						<td style="background: transparent;"><label id="lblTotalDue" class='fntBold fntWhite'></label></td>
						<td style="background: transparent;"><label id="lblTotalDueAmountSelected" class='fntBold fntWhite'></label> <label id="lblSelectedCurrCode"  class='fntBold fntWhite'></label></td>
					</tr>
					<tr>
					<td class='bLeft'></td>
					<td class='bRight'></td>
					</tr>  
				</table>
				</td>
			</tr></table>
			</td>
		</tr>
		<tr>
			<td class='rowGap' colspan='2'></td>
		</tr>
		<tr>
			<td id="flexiInfo" style="display: none;" class="alignLeft" colspan="2"><font class="fntEnglish">Remaining Flexibilities: <span id="spnRemainingFlexi"><font class="fntRed">No more flexibilities available for the cancelled segment</font></span></font>
			</td>
		</tr>
		<tr>
			<td class='rowGap' colspan='2'></td>
		</tr>																	
		<tr>
			<td colspan="2" class="alignLeft paddingL5" width="65%">
				<table  border="0" cellspacing="0" cellpadding="0">
					<tr>
						 <td class="alignLeft paddingL5" width="175px"><label id="lblPG"></label></td>
						 <td class="alignLeft">
							<select id='selPG' size='1' style='width:200px;' name='strPaymentGateway'></select>
						 </td>
					</tr>
					<%--<tr>
						 <td>
							<select id='selPG' size='1' style='width:200px;' name='strPaymentGateway'></select>
						 </td>
					</tr> --%>
				</table>
			</td>
		</tr>
		<tr>
			<td class='rowGap'></td>
		</tr>		
		<tr>
			<td colspan="2" class="alignLeft">
				<%@include file="../common/cardDetail.jsp" %>
			</td>
		</tr>
		
				
			
		<tr>
			<td class="alignLeft" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">		
				<%@ include file="../common/includeFrameDivideBottom.html" %>
					<label id="lblNofication" class='fntBold'> </label><label class="paddingL3 fntBold">:</label><label class="paddingL3" id="lblMsgRecordIP"></label><br>
					<label id="lblYourIPIs" class='fntBold'> </label><label class="paddingL3 fntBold">:</label><label class="paddingL3" id='lblYourIP'></label>
				<%@ include file="../common/includeFrameBottom.html"%>
			</table>
			</td>
		</tr>
		<tr>
		</tr>
			<tr>
			<td class='rowGap'>				
			</td>
		</tr>		
		<tr style="display: none;">
			<td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">														
					<tr>
						<td colspan='2'  valign='top'>													
							<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
								<tr>
									<td colspan="7" class="Gridwt alignLeft"><label id="lblDeparting"  class='fntBold hdFontColor'></label></td>
								</tr>	
								<tr>
									<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
								</tr> 
								<tr>            
									<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
								</tr>
								<tr id="departueFlight">
									<td width='35%' class='defaultRowGapNew GridItems alignLeft'><label id="segmentCode"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="departureDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="departureTime"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalTime"></label></td>
									<td width='10%' class='defaultRowGapNew GridItems' align='center'><label id="flightNumber"></label></td>
									<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>															
								</tr>               
							</table>																																
						</td>
					</tr>
					<tr>
						<td colspan='7' class='rowGap'></td>
					</tr>
				</table>
			</td>
		</tr>											
		<tr style="display: none">
			<td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
					<tr id="trRetrunGrid">
						<td colspan='2'  valign='top'>
							<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">
								<tr>
									<td colspan="7" class="Gridwt alignLeft"> <label id="lblReturning"  class='fntBold hdFontColor'></label></td>
								</tr>
								<tr>
									<td rowspan="2" align='center' class='gridHD'><label id="lblRSegment" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align='center' class='gridHD'><label id="lblRDeparture" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align='center' class='gridHD'><label id="lblRArrival" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblRFlightNo" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
								</tr>
								<tr>           
									<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>
								</tr> 
								<tr id="returnFlight">
									<td width='35%' class='defaultRowGapNew GridItems alignLeft'><label id="segmentCode"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="departureDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="departureTime"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalTime"></label></td>
									<td width='10%' class='defaultRowGapNew GridItems' align='center'><label id="flightNumber"></label></td>
									<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>								
								</tr>                
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr> 
	  </table>
	 </td>
	</tr>	
	<tr id="intExtProcessing" style="display: none">
		<td>
			<div style="text-align: center;" class="fntBold, fntRed">
				<img  src="../images/tr-processing_en_no_cache.gif">
			</div>
		</td>
	</tr>																
	<tr>
		<td class='rowGap' id="paymentPannelPosition">
			<%-- <div id="cardInputPannelInternalExternal">
				<iframe name="cardInputPannelInternalExternal" id="cardInputPannelInternalExternal" class="paymentCardInputs"  style="" frameborder="0" marginwidth="0" marginheight="0" height="0%" width="0%" src="showBlank">
				</iframe>
			</div> --%>
		</td>
	</tr>
	
	<tr id="trTermsCondition" style="display:none">
		<td class="alignRight">
			<table border="0" width="100%">
					<tr>
						<td class="alignRight"><label id="lblAcceptMsg1"></label>&nbsp;
						<a href="#" id="linkTerms"><u> <label class="hdFontColor"
							id="lblAcceptMsg2"></label></u> </a>
						<label id="lblAcceptMsg3"></label>	
						</td>
						<td width="2%"><label><input type="checkbox"
							title="Click here if you agree with terms and conditions"
							name="chkTerms" id="chkTerms" /></label></td>
					</tr>
				</table>
		</td>
	</tr>
		
	
	<tr>
		<td height="15px" colspan="2">
		</td>
	</tr>
	
	<tr>
		<td class="alignLeft" colspan="2">
		<div class="buttonset" id="paymentButtonset">
			<!-- Button -->
			<table border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td class="alignLeft">
						<!--<input type="button" name="btnPrevious" id="btnPaymentPrevious" tabindex="23"  value="Previous" class="Button"/>
						<input type="button" name="butEditAnci" id="butEditAnci" tabindex="23"  value="Previous" class="Button" style="display: none"/>-->
						<u:hButton name="btnPrevious" id="btnPaymentPrevious" value="Previous Step" tabIndex="23" cssClass="backPrevious"/>
						<u:hButton name="btnPrevious" id="butEditAnci" value="Previous Step" tabIndex="23" cssClass="backPrevious ui-state-default ui-corner-all" inLineStyles="display:none"/>
					</td>
					<td style="width:10px;">&nbsp;</td>	
					<td><!--<input type="button" name="btnSOver"  tabindex="23"  value="Start Over" class="Button ButtonMedium"/>-->
						<u:hButton name="btnSOver" id="btnSOver" value="Start Over" tabIndex="23" cssClass="blackStOver"/>	
						<!--<input type="button" name="btnRecalculate" id="btnRecalculate"  tabindex="23"  value="Recalculate" class="Button ButtonMedium"/>
					--></td>	
					<td style="width:10px;">&nbsp;</td>
					<td class="alignRight">
						<!--<input type="button" name="btnNext" id="btnPurchase"  value="Purchase" class="Button" />
						<input type="button" name="btnPurchaseOnAnci" id="btnPurchaseOnAnci"  value="Purchase" 
						class="Button" style="display: none;"/>	-->
						<u:hButton name="btnNext" id="btnPurchase" value="Next" tabIndex="22" cssClass="redContinue"/>
						<u:hButton name="anciBtnPayment" id="btnPurchaseOnAnci" value="Proceed to Payment" tabIndex="22" cssClass="redContinue ui-state-default ui-corner-all" inLineStyles="display:none"/>
					</td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
</table></td></tr></table>
<input type="hidden" id="payCurrency" name="pgw.payCurrency"/>
<input type="hidden" id="providerName" name="pgw.providerName"/>
<input type="hidden" id="providerCode" name="pgw.providerCode"/>
<input type="hidden" id="description" name="pgw.description"/>
<input type="hidden" id="paymentGateway" name="pgw.paymentGateway"/>
<input type="hidden" id="payCurrencyAmount" name="pgw.payCurrencyAmount"/>
<input type="hidden" id="brokerType" name="pgw.brokerType"/>
<input type="hidden" id="switchToExternalURL" name="pgw.switchToExternalURL"/>
<input type="hidden" id="viewPaymentInIframe" name="pgw.viewPaymentInIframe"/>
<input type="hidden" id="onholdReleaseTime" name="pgw.onholdReleaseTime"/>
<input type="hidden" id="hdnMode" name="hdnMode"/>
<input type="hidden" id="jsonInsurance" name="jsonInsurance"/>
<input type="hidden" id="jsonPaxWiseAnci" name="jsonPaxWiseAnci"/>
<input type="hidden" id="jsonSubStations" name="jsonSubStations"/>
<input type="hidden" id="jsonPaxInfo" name="jsonPaxInfo"/>
<input type="hidden" id="hdnQiwiMobileNumber" name="hdnQiwiMobileNumber"/>
<input type="hidden" id="hdnPayFortMobileNumber" name="hdnPayFortMobileNumber"/>
<input type="hidden" id="hdnPayFortEmail" name="hdnPayFortEmail"/>
<input type="hidden" id="hdnPGWPaymentMobileNumber" name="hdnPGWPaymentMobileNumber"/>
<input type="hidden" id="hdnPGWPaymentEmail" name="hdnPGWPaymentEmail"/>
<input type="hidden" id="hdnPGWPaymentCustomerName" name="hdnPGWPaymentCustomerName"/>
<input type="hidden" id="isQiwiOnlineMode" name="isQiwiOnlineMode" />
