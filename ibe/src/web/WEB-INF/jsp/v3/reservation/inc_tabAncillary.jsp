<table width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none " id="anciMainTable">
<tr>
	<td colspan="2" class="anciGadient">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 5px;">
				<tr>
				<td class="rowSingleGap">
				</td>
				</tr>
				<tr>
				<td class="alignLeft" colspan="2" >
					<label id="lblPersonalizeTrip" class="hdFontColor fntBold"></label>
				</td>
				</tr>
				<tr>
					<td colspan='2' class='rowGap'></td>
				</tr>				
				<tr>
					<td id='tblMainTrSelectAnciText' class='alignLeft'>
						<label id='lblPleaseSelectAnci'></label>
					</td>
				</tr>
                <tr id='tblMainTrNoAnciMsg' style="display:none;">
					<td class='alignLeft' >
						<label class='fntBold' id='lblNoAnciAvailable'></label>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan='2' class='rowGap'></td>
	</tr>
	<tr>
	<td colspan='2'>
	<table cellpadding="0" cellspacing="0" border="0" width="100%" id="anciConfigTable">
	<%-- Seat --%>
	<tr id="seatTr" class="anciRow"> <!-- do not change this tr Id, it relates with the order config  --> 
		<td colspan='2'>
			<div  class='rowGap'></div>
			<div id='tblSeat' style="display: none">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
					<%@ include file="../common/includeFrameTop.html"%>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<tr>
								<td class='alignLeft'><label class='fntBold hdFontColor' id='lblSelectSeat'></label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
						</table>
					<%@ include file="../common/includeFrameDivideTop.html" %>
					<tr id="seatBannerRow" class="BannerRow"  style="display:none"><td colspan="3"><img src="<fmt:message key="msg.ancillary.banner.seat"/>" border="0" width="680"/>
					</td></tr>
					<%@ include file="../common/includeFrameDivideBottom.html" %>
						<table width='650' border='0' cellpadding='0' cellspacing='0' align='center' class="ancipanelBody">
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr><td><div class="seatPromoOffer alignLeft"></div><div class="rowGap clear"></div></td></tr>
							<tr>
								<td class="alignLeft">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tbody><tr>
											<td width="50%">
												<table><tr>
													<td width="2%"><input type="radio" name="radSeat" id="radSeat_Y" value="Y" class="noBorder" checked="checked"></td>
													<td width="98%"><label id="lblSeatYes"></label></td>
												</tr></table>
											</td>
											<td width="50%">
												<table><tr>
													<td><input type="radio" name="radSeat" id="radSeat_N" value="N" class="noBorder"></td>
													<td><label id="lblSeatNo"></label></td>
												</tr></table>
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>							
							<tr>
								<td class='alignLeft'>
								<div class="divSearAnici">
									
								</div>
								<div class="disabledLinks" style="display: none; "></div>
								</td>
							</tr>
						</table>
			<%@ include file="../common/includeFrameBottom.html"%>
			</table>
			</div>
			<!-- </table> -->
			</td>
	</tr>
	<%-- Baggage v3 --%>
	<tr id="baggageTr" class="anciRow"> <!-- do not change this tr Id, it relates with the order config  --> 
		<td colspan='2'>
			<div class="rowGap"></div>
			<div id='tblBaggage' style="display: none">
			<div>
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
					<%@ include file="../common/includeFrameTop.html"%>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<tr>
								<td class='alignLeft'><label class='fntBold hdFontColor' id='lblSelectBaggage'></label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
						</table>
					<%@ include file="../common/includeFrameDivideTop.html" %>
					<tr id="bagBannerRow" class="BannerRow"  style="display:none"><td colspan="3"><img src="<fmt:message key="msg.ancillary.banner.baggage"/>" border="0" width="680"/></td></tr>
					<%@ include file="../common/includeFrameDivideBottom.html" %>
						<table width='650' border='0' cellpadding='0' cellspacing='0' align='center' class="ancipanelBody">
							
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr><td><div class="baggageOffer alignLeft"></div><div class="rowGap clear"></div></td></tr>						
							<tr>
								<td class='alignLeft'>
								<div class="divBaggageAnci">
									
								</div>
								<div class="disabledLinks" style="display: none; "></div>
								</td>
							</tr>
						</table>
			<%@ include file="../common/includeFrameBottom.html"%>
			</table>
			</div>
			</div>
			<!-- </table> -->
			</td>
	</tr>
	<%-- Meal --%>
	<tr id="mealTr" class="anciRow"><!-- do not change this tr Id, it relates with the order config -->
		<td colspan='2'>
			<!--<table id='tblMeal' width='100%' border='0' cellpadding='0' cellspacing='0' style="display: none">-->
			<div id='tblMeal' style="display: none">
			<div  class='rowGap'></div>
					<div id="viewMeal">
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
						<%@ include file="../common/includeFrameTop.html"%>
							<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
								<tr>
									<td class='alignLeft'><label id='lblSelectMeal' class='fntBold hdFontColor'></label></td>
								</tr>
								<tr>
									<td class='rowGap'></td>
								</tr>
							</table>
						<%@ include file="../common/includeFrameDivideTop.html" %>
							<tr id="mealBannerRow" class="BannerRow" style="display:none"><td colspan=3"><img src="<fmt:message key="msg.ancillary.banner.meal"/>" border="0" width="680"/></td></tr>
						<%@ include file="../common/includeFrameDivideBottom.html" %>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class="ancipanelBody">
							<tr>
								<td class='rowGap'></td>
							</tr>
							
							<tr>
								<td class='alignLeft'>
									<table id='tblMealSel' border='0' cellpadding='1' cellspacing='1' width='100%'>
										<tr>
											<td colspan="2">
											<label id='lblMealWhy'>
											</label>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr id="mealMenuDisplay">
											
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td>
												<table border='0' cellpadding='0' cellspacing='0' width='100%'>
												<tr><td width="50%">
													<table><tr>
														<td width='2%'><input type='radio' id='radMeal_Y'  name='radMeal' value='Y' class='noBorder' ></td>
														<td width='98%'><label id='lblMealYes'></label></td>
													</tr></table>
													</td>
													<td width="50%">
														<table><tr>
														<td><input type='radio' name='radMeal' id='radMeal_N' value='N' class='noBorder'></td>
														<td><label id='lblMealNo'></label></td>
													</tr></table></td>
												</tr>
												<tr>
													<td class='rowGap' colspan="2"></td>
												</tr>
												</table>
											</td>
										</tr>										
									</table>
								</td>
							</tr>
							<tr>
								<td class='alignLeft'>
								<div id='tblMealDetails'>
								</div>
								<div class="disabledLinks" style="display: none;"></div>
								</td>
							</tr>
						</table>
					<%@ include file="../common/includeFrameBottom.html"%>
					</table>
					</div>
				</div>
			<!-- </table>  -->
		</td>
	</tr>
	<%-- Insurance --%>
	<tr id="insTr" class="anciRow"><!-- do not change this tr Id, it relates with the order config -->
		<td colspan='2'>
			<!--<table id='tblIns' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">-->
				<div id='tblIns' style="display: none">
				<div  class='rowGap'></div>
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
					<%@ include file="../common/includeFrameTop.html"%>
					<c:import url="../../../../ext_html/ins_IBEContent.jsp?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" />
					<%@ include file="../common/includeFrameBottom.html"%>
					</table>
				</div>
			<!-- </table> -->
			<div class="disabledLinks" style="display: none;"></div>
		</td>
	</tr>

	<%-- Flexi --%>
	<tr id="flexiTr" class="anciRow"><!-- do not change this tr Id, it relates with the order config -->
		<td colspan='2'>
				<div id='tblFlexi' style="display: none">
				<div  class='rowGap'></div>
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
					<%@ include file="../common/includeFrameTop.html"%>
					<table border='0' cellpadding='1' cellspacing='1' width='100%'>
						<tr>
							<td class='alignLeft'><label id='lblFlexiHD' class='fntBold hdFontColor'>Add Flexi Fare</label></td>
						</tr>
						<tr>
							<td class='rowGap' colspan="2"></td>
						</tr>
						<tr>
							<td class='alignLeft'><label id='lblFlexiInfo'>You can add flexible fare facility for your trip. </label></td>
						</tr>
						<tr>
							<td class='rowGap' colspan="2"></td>
						</tr>
						<tr>
							<td>
								<table border='0' cellpadding='0' cellspacing='0' width='100%'>
								<tr><td width="50%">
									<table><tr>
										<td width='2%'><input type='radio' id='radFlexi_Y'  name='radFlexi' value='Y' class='noBorder' ></td>
										<td width='98%' class="alignLeft"><label id='lblFlexiYes'>Yes, I would like to add flexi fare to my trip</label></td>
									</tr></table>
									</td>
									<td width="50%">
										<table><tr>
										<td><input type='radio' name='radFlexi' id='radFlexi_N' value='N' class='noBorder'></td>
										<td><label id='lblFlexiNo'>	No thanks</label></td>
									</tr></table></td>
								</tr>
								<tr>
									<td class='rowGap' colspan="2"></td>
								</tr>
								</table>
							</td>
						</tr>										
						<tr>
							<td class='alignLeft'>
								<div id='tblFlexiDetails'>
								</div>
								<div class="disabledLinks" style="display: none;"></div>
							</td>
						</tr>
					</table>
					<%@ include file="../common/includeFrameBottom.html"%>
					</table>
				</div>
		</td>
	</tr>
	
	<!-- SSR -->
	<tr id="ssrTr" class="anciRow"><!-- do not change this tr Id, it relates with the order config -->
		<td >
			<!--<table id='tblSSR' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">-->
				<div id='tblSSR' style="display: none">
					<div  class='rowGap'></div>
					<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
					<%@ include file="../common/includeFrameTop.html"%>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<tr>
								<td class='alignLeft'><label id='lblSelectSSR' class='fntBold hdFontColor'></label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr>
								<td class='alignLeft'>
									<table id='tblSSRSel' border='0' cellpadding='1' cellspacing='1' width='100%'>
										<tr>
											<td><label id='lblSSRWhy'></label></td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td>
												<table border='0' cellpadding='1' cellspacing='1' width='100%'>
													<tr>
														<td width='2%'><input type='radio' name='radSSR' value='Y' class='noBorder' id='radSSR_Y'></td>
														<td width='98%'><label id='lblSSRYes'></label></td>
													</tr>
													<tr>
														<td><input type='radio'  name='radSSR' value='N' class='noBorder' id='radSSR_N'></td>
														<td><label id='lblSSRNo'></label></td>
													</tr>
													<tr>
														<td colspan='2' class='rowGap'></td>
													</tr>													
												</table>
											</td>
										</tr>
									</table>
									
								</td>
							</tr>
							<tr>
								<td class='alignLeft'>
								<div id='divSSRContainer'>
									<table id='tblSSRDetails' border='0' cellpadding='1' cellspacing='1' width='100%' style='display:block;'>
										<tr>
											<td><label id='lblSSRHowTo'></label></td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class='tabBG noPadding' style="padding:0 4px">
												<div class="newTabStyle">
												<table border='0' cellpadding='1' cellspacing='0'>
													<tbody id='tbdySSRSeg' name='tbdySSRSeg'></tbody>
												</table>
												</div>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class="alignLeft">
												<table width='95%' border='0' cellpadding='1' cellspacing='1' align='center' class="anciTable">
													<thead>
														<tr>
															<td width='35%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSSRPaxHD'></label></td>
															<td width='35%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSSRSSRHD'></label></td>
															<td width='30%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSSRCommentHD'></label></td>
														</tr>
													</thead>
													<tr id="paxSSRTemplate">
														<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label name="paxName" ></label></td>
														<td class='rowColor bdRight bdBottom' align='center'><select name='ssrList' size='1' style='width:150px;'></select></td>
														<td class='rowColor bdRight bdBottom' align='center'><input name="ssrText" maxlength='50'></label></td>
													</tr>        
												</table>
											</td>
										</tr>
									</table>
									</div>
									<div class="disabledLinks" style="display: none;"></div>
								</td>
							</tr>
						</table>
					<%@ include file="../common/includeFrameBottom.html"%>
					</table>
				</div>
			<!-- </table> -->
		</td>
	</tr>
	<%-- Hala --%>
	<tr id="halaTr" class="anciRow"><!-- do not change this tr Id & class, it relates with the order config -->
		<td colspan='2'>
					<div id='tblHala' style="display: none">
					<div  class='rowGap'></div>
					<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
					<%@ include file="../common/includeFrameTop.html"%>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<tr>
								<td class='alignLeft'><label id='lblHalaHD' class='fntBold hdFontColor'></label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr><td><div class='halaPromoOffer alignLeft'></div><div class='rowGap clear'></div></td></tr>
							<tr>
								<td class='alignLeft'><label id='lblHalaInfo' class=''></label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr><td class="alignLeft"> 
								<table border='0' cellpadding='0' cellspacing='0' width='100%'>
									<tr>
										<td width="50%"><table>
											<tr>
												<td width='2%' ><input type='radio' id='radHala_Y'  name='radHala' value='Y' class='noBorder' ></td>
												<td width='98%' class='alignLeft'><label id='lblHalaYes'>Yes, I want to add Airport Services</label></td>
											</tr>
										</table></td>
										<td><table>
											<tr>
												<td width='2%'><input type='radio' name='radHala' id='radHala_N' value='N' class='noBorder'></td>
												<td width='98%' class='alignLeft'><label id='lblHalaNo'>No thanks</label></td>	
											</tr>
										</table>
										</td>
									</tr>
								</table></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr id='tblHalaDetails'>
								<td class='alignLeft'>
									<div class="divHalaAnci">
									
									</div>
									<div class="disabledLinks" style="display: none; "></div>
								</td>
							</tr>
							</table>
					<%@ include file="../common/includeFrameBottom.html"%>
					</table>
				</div>
			<!-- </table> -->
		</td>
	</tr>
	<tr id="apTransferTr" class="anciRow"><!-- do not change this tr Id & class, it relates with the order config -->
		<td colspan='2'>
            <div  class='rowGap'></div>
			<table id='tblApTransfer' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">
                <%@ include file="../common/includeFrameTop.html"%>
						<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<tr>
								<td class='alignLeft'><label id='lblAPTransferHD' class='fntBold hdFontColor'>Select Airport Transfer</label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr>
								<td class='alignLeft'><label id='lblAirportTransferInfo' class=''>Upgrade to a better travel experience with Airport Transfer cabs. You can reserve cabs in your destination(s).</label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr>
								<td class="alignLeft">
									<table border='0' cellpadding='1' cellspacing='1' width='100%'>
									<tr>
										<td width='2%'><input type='radio' id='radAPT_Y'  name='radAPT' value='Y' class='noBorder' ></td>
										<td width='98%'><label id='lblAPTYes'>Yes, I want to add Airport Transfers</label></td>
									</tr>
									<tr>
										<td><input type='radio' name='radAPT' id='radAPT_N' value='N' class='noBorder'></td>
										<td><label id='lblAPTNo'>No thanks</label></td>
									</tr>
									<tr>
										<td class='rowGap' colspan="2"></td>
									</tr>
									</table>
								</td>
							</tr>	
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr id='tblApTransferDetails'>
								<td class='alignLeft'>
									<div class="divApTransferAnci"></div>
									<div class="disabledLinks" style="display: none; "></div>
							   </td>
							</tr>
							</table>
                <%@ include file="../common/includeFrameBottom.html"%>
			</table>
		</td>
	 </tr>	
	</table>
	</td>
	</tr>
	<tr>
		<td height="30px" colspan='2'>
		</td>
	</tr>
	<tr>
		<td class="alignLeft" colspan='2'>
		<div class="buttonset">
			<%-- Button --%>
			<table border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td class="alignLeft">
					<!--<input type="button" name="btnPrevious" id='anciBtnPrevious'  tabindex="23"  value="Previous" class="Button"/>-->
					<u:hButton name="btnPrevious" id="anciBtnPrevious" value="Previous Step" tabIndex="23" cssClass="backPrevious"/>
					</td>
					<td style="width:10px;">&nbsp;</td>		
					<td><!--<input type="button" name="btnSOver"  tabindex="23"  value="Start Over" class="Button ButtonMedium"/>-->
					<u:hButton name="btnSOver" id="btnSOver" value="Start Over" tabIndex="23" cssClass="blackStOver"/>
					</td>	
					<td style="width:10px;">&nbsp;</td>				
					<td class="alignRight">
						<!--<input type="button" name="btnNext" id="anciBtnNext" tabindex="24" value="Next" 
						class="Button"/>
						<input type="button" name="anciBtnPayment" id="anciBtnPayment" tabindex="24" value="Proceed to Payment" 
						class="Button ButtonLarge" style="display: none"/>
						-->
						<u:hButton name="btnNext" id="anciBtnNext" value="Next" tabIndex="24" cssClass="redContinue"/>
						<u:hButton name="anciBtnPayment" id="anciBtnPayment" tabIndex="24" value="Proceed to Payment"  cssClass="redContinue" inLineStyles="display:none"/>
					</td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
</table>

<span id='spnSeatInfo' style='position:absolute;background-color:#FF2200;width:80px;height:50px;display:none;' class='seatTT ui-corner-all'>
	<table width='100%' border='0' cellpadding='0' cellspacing='1' >
		<tr>
			<td align='center'><span id='spnTTSeatNo' class='fntBold gridHDFont fntSmaller'></span></td>
		</tr>
		<tr>
			<td align='center'><span id='spnTTSeatChg' class='fntBold gridHDFont fntSmaller'></span></td>
		</tr>
	</table>
</span>

 <span id='spnSocial' style='position:absolute; display:none;' class='seatTT ui-corner-all'>
 	<table width='100%' border='0' cellpadding='0' cellspacing='1' >
		<tr>
			<td align='center'><span id='spnSocialSeating'></span></td>
		</tr>
	</table>
 </span>
