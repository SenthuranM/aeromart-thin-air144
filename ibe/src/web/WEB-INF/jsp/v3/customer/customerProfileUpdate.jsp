<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 	
<script type="text/javascript">
</script>
<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr id="trProfile">
	<td>
		<div id="profilePage" class="">
			 <form name="frmCutUpdate" id="frmCutUpdate" method="post" action="">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"> 
						<tr>
							<td  colspan='4' class="rowGap">
							</td>
						</tr>
						<tr>
							<td colspan='4' class="alignLeft">
								<label class="fntBold  hdFont hdFontColor" id="lblChangeDetails">  </label>											 
							</td>
						</tr>
						<tr>
							<td  colspan='4' class="rowGap">
							</td>
						</tr>
						<tr>
                            <td colspan="4" valign="top" class="alignLeft">
                              <div id="spnError" class="mandatory" style=""></div>	                                          
                            </td>
                        </tr>
						<tr>
							<td colspan="4" class="alignLeft">
								<label id="lblInfoChangeDetails"></label>
							</td>
						</tr>
						<tr>
							<td  colspan='4' class="rowGap">
							</td>
						</tr>
						<tr>
							<td colspan="4" class="alignLeft">
								<label class="fntSmall" id="lblInfoFillReg"></label>
								<label class="fntSmall" id="lblHighlightWith"> </label> <font class="mandatory">*</font> <label id="lblAreMandatory"></label>
							</td>
						</tr>	
						<tr>
							<td  colspan='4' class="rowGap">
							</td>
						</tr>
						<tr id="trEmail">
							<td  colspan='4'>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td class='pnlTopL' style="width:10px"></td>
								<td class='pnlTopM pnlWidth'></td>
								<td class='pnlTopR'></td>
							</tr>
							<tr>
							<td class="pnlMidL"></td>
							<td class="pnlMid">
								<table width="100%" border="0" cellpadding="2" cellspacing="0">
								<tr>
									<td colspan="4" class="alignLeft">
										<label class="fntBold hdFontColor" id="lblLoginInfo"></label>
									</td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								<tr>
									<td width="25%" class="alignLeft">
										<label id="lblEmail"> </label>
									</td>
									<td width="1%">
										<label>:</label>
									</td>
									<td width="33%" class="alignLeft">
										<input type="text" style="width: 190px;" readonly="readonly" title="login ID" maxlength="100" size="25" name="customer.emailId" id="txtEmail"/>
									</td>
									<td width="43%" class="alignLeft">	
										<label class="fntSmall" id="lblInfoLoginID"></label>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<table border="0" cellpadding="1" cellspacing="0" width="100%" id="tblPassword">
										<tr>
											<td width="25%" class="alignLeft">
												<label id="lblPassword"></label>
											</td>
											<td width="1%">
												<label>:</label>
											</td>
											<td width="33%" class="alignLeft">
												<input type="password" style="width: 150px;" maxlength="12" size="10" name="customer.password" id="txtPassword"/> 
											    <label class="mandatory" id="lblPassowrdMand">*</label>
											</td>
											<td width="43%" valign="top" rowspan="3" class="alignLeft">
												<label id="lblInfoPassword" class="fntSmall"></label>
											</td>
										</tr>
										<tr>
											<td class="alignLeft"><label id="lblConfirmPwd"></label></td>
											<td><label>:</label></td>
											<td class="alignLeft">
												<input type="password" style="width: 150px;" maxlength="12" size="10" name="txtConPassword" id="txtConPassword"/> 
												<label class="mandatory" id="lblPassowrdConfMand">*</label>
											</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>
								</td>
								<td class="pnlMidR"></td>
								</tr>
								<tr>
									<td class="pnlBottomL"></td>
									<td class="pnlBottomM"></td>
									<td class="pnlBottomR"></td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
							</table>
							</td>
						</tr>	
						<tr id="tblSecurityQuestion">
							<td  colspan='4'>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td class='pnlTopL' style="width:10px"></td>
										<td class='pnlTopM pnlWidth'></td>
										<td class='pnlTopR'></td>
									</tr>
									<tr>
									<td class="pnlMidL"></td>
									<td class="pnlMid">
										<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%">
											<tbody>
												<tr>
													<td colspan="4">
														<table border="0" cellpadding="1" cellspacing="0" width="100%" >
															<tr>
																<td colspan="4" class="alignLeft">
																	<label id="lblForgotPwd" class="fntBold hdFontColor"></label>
																</td>
															</tr>
															<tr>
																<td class="alignLeft">
																	<label id="lblSecurityQues"></label>
																</td>
																<td><label>:</label></td>
																<td colspan="2" class="alignLeft">
																	<select  name="customer.secretQuestion" id="selSecQ" style="width:300px" size="1">	
																		<option value="">[Please Select a Question]</option>													
																	</select>
																	<label class="mandatory" id="lblSecQuesMand">*</label>
																</td>
															</tr>	
															<tr>
																<td width="25%" class="alignLeft">
																	<label id="lblYourAnswer"></label>
																</td>
																<td width="1%">
																	<label>:</label>
																</td>
																<td width="33%" class="alignLeft">					
																	<input type="text" maxlength="100" size="30" name="customer.secretAnswer" id="txtAns"/> <label class="mandatory" id="lblSecAnswerMand">*</label>
																</td>
																<td width="44%" rowspan="2" class="alignLeft">
																	<label class="fntSmall" id="lblInfoSecurityQues"></label>
																</td>
															</tr>
														</table>
													</td>
												</tr>											
												<tr id="trAlternateEmail">
													<td width="25%"  class="alignLeft"><label id="lblAltEmail"></label></td>
													<td width="1%"><label>:</label></td>
													<td class="alignLeft" colspan="2">					
														<input type="text" maxlength="100" name="customer.alternativeEmailId" size="30" id="txtAlternateEmail"/>
														<label class="mandatory" id="lblAlternateEmailMand">*</label>
													</td>
												</tr>	
												<tr>
													<td class="rowGap" colspan="4">
													</td>
												</tr>													
											</tbody>
										</table>
									</td>
									<td class="pnlMidR"></td>
									</tr>
									<tr>
										<td class="pnlBottomL"></td>
										<td class="pnlBottomM"></td>
										<td class="pnlBottomR"></td>
									</tr>
									<tr>
										<td  colspan='4' class="rowGap">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr id="tblContactDetails">
						<td  colspan='4'>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td class='pnlTopL' style="width:10px"></td>
										<td class='pnlTopM pnlWidth'></td>
										<td class='pnlTopR'></td>
									</tr>
									<tr>
									<td class="pnlMidL"></td>
									<td class="pnlMid">
										<table width="100%" border="0" cellpadding="2" cellspacing="0" align='center'>
											<tr>
												<td colspan="4" class="alignLeft">
													<label class="fntBold hdFontColor" id="lblAboutU"></label>
												</td>
											</tr>
											<tr>
												<td width="25%" class="alignLeft">
													<label id="lblTitle"></label>
												</td>
												<td width="1%">
													<label>:</label>
												</td>
												<td width="33%" class="alignLeft">
													<select  style="width: 50px;" size="1" name="customer.title" id="selTitle">
													</select>
													<label class="mandatory" id="lblTitleMand"> *</label>
												</td>
												<td width="44%"><label> </label></td>
											</tr>
											<tr id="trFirstName">
												<td class="alignLeft">
													<label id="lblFirstName"> </label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="text" readonly="" class="fontCapitalize" maxlength="50" size="50" name="customer.firstName" id="txtFirstName"/> 
													<label class="mandatory" id="lblFirstNameMand">*</label>
												</td>
											</tr>
											<tr id="trLastName">
												<td class="alignLeft">
													<label id="lblLastName"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="text" readonly="" class="fontCapitalize" maxlength="50" size="50" name="customer.lastName" id="txtLastName"/> 
													<label class="mandatory" id="lblLastNameMand">*</label>
											</td></tr>
											<tr id="trGender">
												<td class="alignLeft">
													<label id="lblGender"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="radio" class="NoBorder" name="customer.gender" id="radGender" value="M" checked="checked"/>
													<label id="lblMale"></label>
													<input type="radio" class="NoBorder" name="customer.gender" id="radFemale" value="F"/>
													<label id="lblFemale"></label>
													<label class="mandatory" id="lblGennderMand"> *</label>
												</td>
											</tr>
											<tr id="trDOB">
												<td class="alignLeft">
													<label id="lblDOB"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<table cellspacing="2" cellpadding="0" border="0">
														<tbody>
															<tr>
															<td class="alignLeft">
																<input type="text" invalidtext="true"  size="10" maxlength="10" name="customer.dateOfBirth" id="txtDateOfBirth"/> 
															</td>
															<td valign="baseline" class="alignLeft">
																<img border="0" src="../images/Calendar2_no_cache.gif" id="imgCalendar" class="cursorPointer"/>
																<label class="mandatory" id="lblDOBMand"> *</label>
															</td>														
														</tr>
													   </tbody>
												  </table>
												</td>
											</tr>
											<tr id="trNationality">
												<td class="alignLeft">
													<label id="lblNationality"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<select style="width: 140px;" size="1" name="customer.nationalityCode" id="selNationality">
													</select>
													<label class="mandatory" id="lblNationalityMand">*</label>
												</td>
											</tr>
											<tr id="trAddr1">
												<td>
													<label id="lblAddress"></label>
												</td>
												<td><label>:</label>
												</td>
												<td colspan="2">
													<input type="text" maxlength="100" size="50" name="customer.addressStreet" id="txtAddr1"/> 
													<label class="mandatory" id="lblAddrMand">*</label>
												</td>
											</tr>
											<tr id="trAddr2">
												<td></td>
												<td><label>:</label></td>
												<td colspan="2">
													<input type="text" maxlength="100" size="50" name="customer.addressLine" id="txtAddr2"/>
												</td>
											</tr>
											<tr id="trCity">
												<td class="alignLeft">
													<label id="lblCity"></label>
												</td>
												<td><label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="text" class="fontCapitalize" maxlength="50" size="50" name="customer.city" id="txtCity"/> 
													<label class="mandatory" id="lblCityMand">*</label>
												</td>
											</tr>
											<tr id="trZipCode">
												<td>
													<label id="lblZipCode"></label>
												</td>
												<td><label>:</label>
												</td>
												<td colspan="2">
													<input type="text" maxlength="10" size="10" name="customer.zipCode" id="txtZipCode"/> 
													<label class="mandatory" id="lblZipCodeMand">*</label>
												</td>
											</tr>
											<tr id="trCountry">
												<td class="alignLeft">
													<label class="" id="lblCountryResidence"></label>
												</td>
												<td><label>:</label></td>
												<td colspan="2" class="alignLeft">
													<select style="width: 140px;"  size="1" name="customer.countryCode" id="selCountry">												
													</select>
													<label class="mandatory" id="lblCountryMand">*</label>
												</td>
											</tr>
											<!-- Contact Details -->
											<tr class="trPhoneNoHeader">
												<td colspan="3">
													<label class="fntBold hdFontColor" id="lblContactDetails"></label>
												</td>
											</tr>															
											<tr class="trPhoneNoLabel">
												<td valign="top" class="fntSmall" width="25%"></td>
												<td width="1%"><label>:</label></td>
												<td width="74%">
													<table cellspacing="0" cellpadding="2" border="0">
														<tbody>
														  <tr>
															<td class="alignLeft"><label class="fntSmall " id="lblCountryCode"></label></td>
															<td class="alignLeft"><label class="fntSmall areaCode" id="lblAreaCode"></label></td>
															<td class="alignLeft"><label class="fntSmall " id="lblNumber"></label></td>
															<td class="alignLeft"><label class="fntSmall "> </label></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="trMobileNo">
												<td valign="top" class="fntSmall"><label id="lblMobileNo"></label></td>
												<td><label>:</label></td>
												<td colspan="2">
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtMCountry" id="txtMCountry"/></td>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtMArea" id="txtMArea" class="areaCode"/></td>
															<td class="alignLeft"><input type="text"  maxlength="10" style="width: 100px;" name="txtMobile" id="txtMobile"/></td>
															<td class="alignLeft"><label class="mandatory" id="lblMobileMand">*</label></td>
															<td>&nbsp;</td>
															<td><span id="atLeasetOne"><font class="mandatory" >*&nbsp;</font> <label class="fntSmall" id="lblAtleaseOneText">Please provide at least one phone Number</label></span></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="trPhoneNo">
												<td valign="top" class="fntSmall">
													<label id="lblTelNo"></label>
												</td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtPCountry" id="txtPCountry"/></td>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtPArea" id="txtPArea" class="areaCode"/></td>
															<td class="alignLeft"><input type="text"  maxlength="10" style="width: 100px;" name="txtTelephone" id="txtTelephone"/></td>
															<td class="alignLeft">
															<label class="mandatory" id="lblPhoneMand">*</label>
															</td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>								
											<tr id="trHomeOfficeNo">
												<td valign="top" class="fntSmall"><label id="lblOfficeOHomeNo"></label></td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtOCountry" id="txtOCountry"/></td>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtOArea" id="txtOArea" class="areaCode"/></td>
															<td class="alignLeft"><input type="text"  maxlength="10" style="width: 100px;" name="txtOfficeTel" id="txtOfficeTel"/></td>
															<td class="alignLeft"><label class="mandatory" id="lblHomeOfficeMand">*</label></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>		
											<tr id="trFaxNo">
												<td valign="top" class="fntSmall"><label id="lblFaxNo"></label></td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" id="txtFCountry"/></td>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" id="txtFArea" class="areaCode"/></td>
															<td class="alignLeft"><input type="text" maxlength="10" style="width: 100px;" id="txFax"/></td>
															<td class="alignLeft"></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>										
										</table>
									</td>
									<td class="pnlMidR"></td>
									</tr>
									<tr>
									<td class="pnlBottomL"></td>
									<td class="pnlBottomM"></td>
									<td class="pnlBottomR"></td>
								</tr>
									<tr>
										<td  colspan='4' class="rowGap">
										</td>
									</tr>
								</table>
						</td>
						</tr>
						
						<tr id="trMealSeatPreference">
							<td  colspan='4'>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td class='pnlTopL' style="width:10px"></td>
								<td class='pnlTopM pnlWidth'></td>
								<td class='pnlTopR'></td>
							</tr>
							<tr>
							<td class="pnlMidL"></td>
							<td class="pnlMid">
								<table width="100%" border="0" cellpadding="2" cellspacing="0">
								<tr>
									<td colspan="4" class="alignLeft">
										<label class="fntBold hdFontColor" id="lblMealSeatPreference"></label>
									</td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								<tr>
									<td width="25%" class="alignLeft">
										<label id="lblSelectMeals"> </label>
										<label>&nbsp;&nbsp;:&nbsp;&nbsp;</label>
										<a href="javascript:void(0)" class="selectAici selectMeal" id="meal_0_0">
											<label id="lblSelectMeal">Select Meals</label>
										</a>
									</td>
									
								</tr>
								
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
									<td>
										<div id="newPopItem" style="position: fixed; z-index: 1001; top: -187px; left: 310px; width: 645px; display: none;">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tbody><tr>
												<td class="popBG popTopLeft"></td>
												<td class="popBG popTopMid"></td>
												<td class="popBG popTopRight"></td>
											</tr>
											<tr>
												<td class="popBG1 popMidLeft"></td>
												<td class="popBG1 popMidMid">
													<div id="popupHeader"><span class="headeritems"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="redBar"><label>Menu</label></td><td class="redCorner"></td></tr></tbody></table></span><span class="close"></span></div>
														<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
															<tbody><tr>
																<td class="popBG popTopLeft"></td>
																<td class="popBG popTopMid"></td>
																<td class="popBG popTopRight"></td>
															</tr>
															<tr>
																<td class="popBG1 popMidLeft"></td>
																<td class="popBG1 popMidMid">
																	<div id="bodyitems" style="overflow-y: auto; height: 420px;">
																	<div id="pan_0_0" class="mealCatDiv">
																	<div class="catBlock firstCat"></div>
																	<a href="javascript:void(0)" class="mealCatClick calClass1" id="cat_1">
																	<div class="catBlock">&nbsp;</div>
																	</a>
																	<a href="javascript:void(0)" class="mealCatClick calClass2" id="cat_2">
																	<div class="catBlock">&nbsp;</div>
																	</a>
																	<a href="javascript:void(0)" class="mealCatClick calClass3" id="cat_3">
																	<div class="catBlock">&nbsp;</div>
																	</a>
																	<a href="javascript:void(0)" class="mealCatClick calClass4" id="cat_4">
																	<div class="catBlock">&nbsp;</div>
																	</a>
																	<a href="javascript:void(0)" class="mealCatClick calClass5" id="cat_5">
																	<div class="catBlock">&nbsp;</div>
																	</a>
																	<a href="javascript:void(0)" class="mealCatClick calClass6" id="cat_6">
																	<div class="catBlock">&nbsp;</div>
																	</a>
																	<a href="javascript:void(0)" class="mealCatClick calClass7" id="cat_7">
																	<div class="catBlock">&nbsp;</div>
																	</a>
																	</div>
																	</div>
																</td>
																<td class="popBG1 popMidRight"></td>
															</tr>
															<tr>
																<td class="popBG popBottomLeft"></td>
																<td class="popBG popBottomMid"></td>
																<td class="popBG popBottomRight"></td>
															</tr>
														</tbody></table>
													<div id="footeritems"><input type="button" value="Confirm" class="Button mealConfirm"></div>
												</td>
												<td class="popBG1 popMidRight"></td>
											</tr>
											<tr>
												<td class="popBG popBottomLeft"></td>
												<td class="popBG popBottomMid"></td>
												<td class="popBG popBottomRight"></td>
											</tr>
										</tbody></table>
										</div>
									</td>
								</tr>
								
								<tr id="trSelectedMealsList">
									<td colspan="3">
										<div id="selectedMealsList">
											<table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" class="GridTable" id="selectedMealsTbl">
												<tr>
													<td class="segcodebar bdRight" align="left" style="width: 110px"><label
														class="gridHDFont fntBold" id="lblMealNameHD">Meal Name</label></td>
													<td class="segcodebar bdRight" align="left" style="width: 385px"><label
														class="gridHDFont fntBold" id="lblMealDesHD">Description</label></td>
													<td class="segcodebar bdRight" align="left" colspan="2" style="width: 60px"><label
														class="gridHDFont fntBold " id="lblMealQuntityHD">Quantity</label></td>
												</tr>
												<tbody id="selectedMealsTable">																	
												</tbody>
											</table>
										</div>
									</td>
								</tr>
								
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								
								<tr>
									<td width="30%" class="alignLeft">
										<label id="lblSelectSeat"></label>
										<label>:</label>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<div id="seatPreferences" style="color: #737373; padding-left:180px">
										</div>
									</td>
								</tr>
									
								</table>
								</td>
								<td class="pnlMidR"></td>
								</tr>
								<tr>
									<td class="pnlBottomL"></td>
									<td class="pnlBottomM"></td>
									<td class="pnlBottomR"></td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
							</table>
							</td>
						</tr>	
						
						<tr id="trFamilyMemberDetails">
							<td  colspan='4'>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td class='pnlTopL' style="width:10px"></td>
								<td class='pnlTopM pnlWidth'></td>
								<td class='pnlTopR'></td>
							</tr>
							<tr>
							<td class="pnlMidL"></td>
							<td class="pnlMid">
								<table width="100%" border="0" cellpadding="2" cellspacing="0">
								<tr>
									<td colspan="4" class="alignLeft">
										<label class="fntBold hdFontColor" id="lblFamilyMembers"> Family Members </label>
									</td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								
								<tr id="trFamilyMembersList">
									<td colspan="4">
										<div id="familyMembersList">
											<table border="0" cellpadding="0" cellspacing="0" align="center" class="GridTable" id="familyMembersTbl">
												<tr>
													<td class="segcodebar bdRight" align="left" style="width: 150px"><label
														class="gridHDFont fntBold" id="lblFMFirstName">First Name</label></td>
													<td class="segcodebar bdRight" align="left" style="width: 150px"><label
														class="gridHDFont fntBold" id="lblFMLastName">Last Name</label></td>
													<td class="segcodebar bdRight" align="left" style="width: 60px"><label
														class="gridHDFont fntBold" id="lblFMRelationship">Relationship</label></td>
													<td class="segcodebar bdRight" align="left" style="width: 82px"><label
														class="gridHDFont fntBold" id="lblFMNationality">Nationality</label></td>
													<td class="segcodebar" align="left" style="width: 85px"><label
														class="gridHDFont fntBold" id="lblFMDOB">Date of Birth</label></td>
												</tr>
												<tbody id="familyMembersTable">																	
												</tbody>
											</table>
										</div>
									</td>
								</tr>
								
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								<tr>
									<td>
										<div class="alignLeft">
											<input type="button" id="btnAddFamilyMember" class="Button" value="Add" title="Click here to Add a New Family Member" name="btnAddFamilyMember" autocomplete="off">
											<input type="button" id="btnEditFamilyMember" class="Button" value="Edit" title="Click here to Edit a Family Member" name="btnEditFamilyMember" autocomplete="off">
											<input type="button" id="btnRemoveFamilyMember" class="Button" value="Remove" title="Click here to Remove a Family Member" name="btnRemoveFamilyMember" autocomplete="off">
										</div>
									</td>
									
								</tr>
								
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								
								<tr>
									<td>
										<div id="familyMemberDetails">
											<fieldset style="border: solid 1px #C0C0C0;">
										    	<legend class="fntBold hdFontColor">Add/Edit Family Members</legend>
										    <table style="padding: 10px;">
											    <tr>
											    	<td>
											    		<label id="lblFMFirstName"> First Name </label>
													</td>
													<td>
														<label>:</label>
													</td>
											    	<td>
											    		<input type="text" style="width: 190px;" title="Family Member First Name" maxlength="100" size="25" name="familyMember.firstName" id="FMFirstName" class="fontCapitalize"/>
														<label class="mandatory" >*</label>
													</td>
													
													<td style="padding-left: 30px;">
											    		<label id="lblFMLastName"> Last Name </label>
													</td>
													<td>
														<label>:</label>
													</td>
											    	<td>
											    		<input type="text" style="width: 190px;" title="Family Member Last Name" maxlength="100" size="25" name="familyMember.lastName" id="FMLastName" class="fontCapitalize"/>
														<label class="mandatory" >*</label>
													</td>
											    </tr>
										
												<tr>
											    	<td>
											    		<label id="lblFMRelationship"> Relationship </label>
													</td>
											    	<td>
														<label>:</label>
													</td>
													<td class="alignLeft">
														<select style="width: 194px;" size="1" name="familyMember.reationshipCode" id="FMRelationship">
														</select>
														<label class="mandatory" >*</label>
													</td>
													
													<td style="padding-left: 30px;">
											    		<label id="lblFMNationality"> Nationality </label>
													</td>
											    	<td>
														<label>:</label>
													</td>
													<td class="alignLeft">
														<select style="width: 194px;" size="1" name="familyMember.nationalityCode" id="FMNationality">
														</select>
														<label class="mandatory" id="lblNationalityMand">*</label>
													</td>
											    </tr>
											    
											    <tr>
											    	<td>
											    		<label id="lblFMDOB"> Date of Birth </label>
													</td>
											    	<td>
														<label>:</label>
													</td>
													<td class="alignLeft">
														<input type="text" id="FMDOB" name="FMDateOfBirth" style="width: 90px;">
													</td>
													
											    </tr>
											    
											    <tr>
											    	<td>
											    	<div class="alignRight">
														<input type="button" id="btnSaveFamilyMember" class="Button" value="Save" name="btnSaveFamilyMember" autocomplete="off">
													</div>
													</td>													
											    </tr>
											    
			
										    </table>
										   </fieldset>
										</div>
									</td>
								</tr>	
									
								</table>
								</td>
								<td class="pnlMidR"></td>
								</tr>
								<tr>
									<td class="pnlBottomL"></td>
									<td class="pnlBottomM"></td>
									<td class="pnlBottomR"></td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
							</table>
							</td>
						</tr>	
						
						<tr>
						<td  colspan='4'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr id="tblLmsDetails">
								<td colspan="4">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
										<tr>
											<td class='pnlTopL' style="width:10px"></td>
											<td class='pnlTopM pnlWidth'></td>
											<td class='pnlTopR'></td>
										</tr>
										<tr>
											<td class="pnlMidL"></td>
											<td class="pnlMid">
											<table width="100%" border="0" cellpadding="2" cellspacing="0" align='center'>
												<tr>
													<td colspan="4" class="alignLeft">
														<label class="fntBold hdFontColor" id="lblLMSInfo">Loyalty Account Details</label>
													</td>
												</tr>
												
												<tr id="trEmailLMS">
													<td class="alignLeft">
														<label id="lblEmailLMS"> Email for Frequent Flyer ID (FFID)</label>
													</td>
													<td>
														<label>:</label>
													</td>
													<td colspan="2" class="alignLeft">
														<input type="text" readonly="readonly" class="fontCapitalize" maxlength="50" size="50" name="emailLMS" id="txtEmailLMS"/> 
														<label class="mandatory" id="lblEmailMandLms">*</label>
													</td>
												</tr>
	
												<tr id="trDOBLMS">
													<td class="alignLeft">
														<label id="lblDOBLMS"> Date of Birth</label>
													</td>
													<td>
														<label>:</label>
													</td>
													<td colspan="2" class="alignLeft">
														<input type="text" readonly="readonly" maxlength="50" size="50" name="lmsDetails.dateOfBirth" id="txtDOBLMS" style="width: 130px"/> 
														<label class="mandatory" id="lblDOBMandLms">*</label>
													</td>
												</tr>
	
												<tr id="trLanguageLMS">
													<td class="alignLeft">
														<label id="lblLanguageLMS">Communication Language</label>
													</td>
													<td>
														<label>:</label>
													</td>
													<td colspan="2" class="alignLeft">
	
														<select title="Language" style="width: 130px"  name="lmsDetails.language" id="txtLanguageLMS">
															<option value="en">English</option>
															<option value="ar">Arabic</option>
														</select>
													</td>
												</tr>
	
												<tr id="trPpnLMS">
													<td class="alignLeft">
														<label id="lblPPLMS"> Passport Number</label>
													</td>
													<td>
														<label>:</label>
													</td>
													<td colspan="2" class="alignLeft">
	
														<input type="text" class="fontCapitalize" maxlength="50" size="50" name="lmsDetails.passportNum" id="txtPpnLMS"/>
													</td>
												</tr>
												
												<tr id="trEmailRefLMS">
													<td class="alignLeft">
														<label id="lblReferringEmailLms">Referring Member Email</label>
													</td>
													<td>
														<label>:</label>
													</td>
													<td colspan="2" class="alignLeft">
														<input type="text" id="txtRefferedEmailLMS" name="lmsDetails.refferedEmail" maxlength="100" size="50" autocomplete="off"/>
													</td>
												</tr>
												
												<tr id="trEmailHLMS">
													<td class="alignLeft">
														<label id="lblFamilyHeadLMS">Family Head Email</label>
													</td>
													<td>
														<label>:</label>
													</td>
													<td colspan="2" class="alignLeft">
														<input type="text" maxlength="100" size="50" name="lmsDetails.headOFEmailId" id="txtEmailHLMS"/> 
														<label class="mandatory" id="lblEmailHMandLMS" style="display: none">*</label>
													</td>
												</tr>
												</table>
											</td>
										<td class="pnlMidR"></td>
										</tr>
										<tr>
										<td class="pnlBottomL"></td>
										<td class="pnlBottomM"></td>
										<td class="pnlBottomR"></td>
										</tr>
								</table>							
							</td>
						</tr>	
						
						
						<tr id="trEmgnContactInfo">
							<td  colspan='4'>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
								<tr>
									<td class='pnlTopL' style="width:10px"></td>
									<td class='pnlTopM pnlWidth'></td>
									<td class='pnlTopR'></td>
								</tr>
								<tr>
								<td class="pnlMidL"></td>
								<td class="pnlMid">
									<table width="100%" border="0" cellpadding="2" cellspacing="0" align='center'>
										<tbody>
											<tr>
												<td colspan="4">
													<label class="fntBold hdFontColor" id="lblEmergencyContactInfo"></label>
												</td>
											</tr>
											<tr id="trEmgnTitle">
												<td width="25%">
													<label id="lblTitle"></label>
												</td>
												<td width="1%">
													<label>:</label>
												</td>
												<td width="33%">
													<select  style="width: 50px;" size="1" name="customer.emgnTitle" id="selEmgnTitle">
													</select>
													<label class="mandatory" id="lblEmgnTitleMand"> *</label>
												</td>
												<td width="44%"><label> </label></td>
											</tr>
											<tr id="trEmgnFirstName">
												<td>
													<label id="lblFirstName"> </label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2">
													<input type="text" class="fontCapitalize" maxlength="50" size="50" name="customer.emgnFirstName" id="txtEmgnFirstName"/> 
													<label class="mandatory" id="lblEmgnFirstNameMand">*</label>
												</td>
											</tr>
											<tr id="trEmgnLastName">
												<td>
													<label id="lblLastName"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2">
													<input type="text"  class="fontCapitalize" maxlength="50" size="50" name="customer.emgnLastName" id="txtEmgnLastName"/> 
													<label class="mandatory" id="lblEmgnLastNameMand">*</label>
												</td>
											</tr>
											<tr id="emgnPhone1">
												<td valign="top" class="fntSmall"></td>
												<td></td>
												<td>
													<table cellspacing="0" cellpadding="2" border="0">
														<tbody>
														  <tr>
															<td class="alignLeft"><label class="fntSmall " id="lblCountryCode"></label></td>
															<td class="alignLeft"><label class="fntSmall areaCode" id="lblAreaCode"></label></td>
															<td class="alignLeft"><label class="fntSmall " id="lblNumber"></label></td>
															<td class="alignLeft"><label class="fntSmall "></label> </td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="emgnPhone2">
												<td valign="top" class="fntSmall">
													<label id="lblTelNo"></label>
												</td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>																			
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPCountry" id="txtEmgnPCountry"/></td>
															<td class="alignLeft"><input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPArea" id="txtEmgnPArea" class="areaCode"/></td>
															<td class="alignLeft"><input type="text"  maxlength="10" style="width: 100px;" name="txtEmgnTelephone" id="txtEmgnTelephone"/></td>
															<td class="alignLeft">
																<label class="mandatory" id="lblEmgnPhoneMand">*</label>
															</td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="trEmgnEmail">
												<td>
													<label id="lblEmail"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2">
													<input type="text" maxlength="100" size="25" name="customer.emgnEmail" id="txtEmgnEmail"/> 
													<label class="mandatory" id="lblEmgnEmailMand">*</label>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td class="pnlMidR"></td>
								</tr>
								<tr>
									<td class="pnlBottomL"></td>
									<td class="pnlBottomM"></td>
									<td class="pnlBottomR"></td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
							</table>
						</td>
						</tr>
						<tr id="tblPreference">
							<td colspan='4' valign='top'>
								<table cellspacing="0" cellpadding="2" border="0" align="center" width="99%" >
									<tbody>
									<tr>
											<td colspan="4" class="alignLeft">
												<label class="fntBold hdFontColor" id="lblYourPref"></label>
											</td>
										</tr>
										<tr id="trPromotionPref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%">
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="radio" name="questionaire[1].questionAnswer" id="chkQ1" value="N" class="NoBorder"/>
															<input type="hidden" name="questionaire[1].questionKey" value="2" />
															<label id="lblUnsubcribe">  </label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="radio" name="questionaire[1].questionAnswer" id="chkQ1" value="Y" class="NoBorder"/>
															&nbsp;<label id="lblInfoPromotional"></label>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr id="tblEmailPref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%" >
													<tr>
														<td colspan="4" class="alignLeft">
															<label id="lblInfoEmailFormat"></label>
														</td>
													</tr>										
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="hidden" name="questionaire[2].questionKey" value="3" />
															<input type="radio" checked="" value="HTML" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
															<label id="lblHtml"></label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">											
															<input type="radio" value="PLAIN" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
															<label id="lblText"> </label>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr id="tblSmsPref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%">
													<tr>
														<td colspan="4" class="alignLeft">
															<label id="lblInfoSMS"></label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="hidden" name="questionaire[3].questionKey" value="4" />
															<input type="radio" value="Yes" name="questionaire[3].questionAnswer" id="radQ4"  class="NoBorder"/>
															<label id="lblInfoMobile"> </label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="radio" checked="" value="No" name="questionaire[3].questionAnswer" id="radQ4" class="NoBorder"/>
															<label id="lblNo"> </label>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr id="tblLanguagePref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%" >
													<tr>
														<td colspan="4">
															<label id="lblInfoLan"></label>
															<input type="hidden" name="questionaire[4].questionKey" value="5" />
														</td>
													</tr>	
													<tr id="templateLanguages">
														<td colspan="4" class="alignLeft">																		
															<input type="radio" tabindex="21" value="" name="questionaire[4].questionAnswer" id="radQ5" class="NoBorder"/>
																<label id="lblLanName">  </label>
														</td>
													</tr>
													<tr>
														<td class="Dots" colspan="4"><label > </label></td>
													</tr>
													<tr>
														<td colspan="4">
															<label id="lblInfoContactLan"> </label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="hidden" name="questionaire[5].questionKey" value="6" />
															<input type="text" size="50" name="questionaire[5].questionAnswer" maxlength="50" value="" id="txtQ2"/>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr>
											<td colspan="4" class="alignLeft">
													<label id="lblInfoAgree1" hidden> </label>
													<br/><br/>	
													<label id="lblInfoAgree2"> </label>	
													<br/><br/>
													<div class="divfont" id="lblInfoAgree3"> </div>	
											</td>
										</tr>
										</table>
									</td>
								</tr>																																
								<tr id="aggrementInfo">
									<td colspan="4">	
										<table border="0" cellpadding="2" cellspacing="0" width="100%" >
										<tr>
											<td class="rowGap" colspan="4">
											</td>
										</tr>
										<tr>
											<td class="rowGap" colspan="4">
											</td>
										</tr>												
									</tbody></table>
								</td>
								</tr>
								<tr id="trButtons">
									<td colspan="4">
										<table cellspacing="0" cellpadding="0" border="0" align="center" width="99%" >
											<tr>
												<td class="alignLeft">
													<u:hButton name="btnCancel" id="btnCancel" value="Back" tabIndex="23" 
													cssClass="redNormal"/>
													<%-- <input type="button" title="Cancel"  class="Button" name="btnCancel" id="btnCancel"/>--%>
												</td><td class="alignRight">
													<u:hButton name="btnUpdate" id="btnUpdate" value="Back" tabIndex="24" 
													cssClass="redNormal"/>
													<%--<input type="button" title="Click here to Update the Profile"  class="Button" name="btnUpdate" id="btnUpdate"/> --%>
												</td>
											</tr>
										</table>
									</td>
								</tr>
						</table>					
				<input type="hidden" name="customer.version" id="customer.version"/>				
				<input type="hidden" name="customer.telephone" id="hdnTelNo"/>
				<input type="hidden" name="customer.mobile" id="hdnMobile"/>
				<input type="hidden" name="customer.officeTelephone" id="hdnOffice"/>
				<input type="hidden" name="customer.fax" id="hdnFax"/>			
				<input type="hidden" name="customer.emgnPhoneNo" id="hdnEmgnPhoneNo"/>				
				<input type="hidden" name="lmsDetails.mobile" id="hdnMobileLMS"/>
				<input type="hidden" name="preferredMealsList" id="selectedMeals"/>	
			</form>
			</div>
		</td>
	</tr>								
	<tr id="trMsg">
		<td>
			<table width='99%' border='0' cellpadding='0' cellspacing='0' align='center'>
				<tr>
					<td>
						<table width='100%' border='0' cellpadding='2' cellspacing='0'>
							<tr>
								<td valign='top'>
									<br>
									<br>
								</td>
							</tr>
							<tr>
								<td valign='top'>
									<table width='100%' border='0' cellpadding='2' cellspacing='0'>
										<tr>
											<td><img src="../images/n058_no_cache.gif" id="imgConfirm"></td>
											<td class="alignLeft">&nbsp;<label class="fntBold" id="lblMsgUpdateSucess"></label></td>				
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
								</td>
							</tr>							 
						</table>					 										
						</td>
		 			 </tr>	       			
		    </table>
		</td>
	</tr>
</table>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/customer/customerProfileUpdate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/v2/customer/customerValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</c:if>



