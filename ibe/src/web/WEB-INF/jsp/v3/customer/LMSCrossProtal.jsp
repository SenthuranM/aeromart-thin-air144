<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/>	
</head>
<body>
	<p id="errorMsg" style="height:100px;padding-left: 10px;" class="alignLeft" width="85%" valign="middle" ></p>

	<script type="text/javascript">
		var url = "<c:out value='${requestScope.crossProtalUrl}' escapeXml='false' />";
		var loginSuccess = "<c:out value='${requestScope.loginSuccess}' escapeXml='false' />";
		var errorMsg = "<c:out value='${requestScope.error}' escapeXml='false'/>";
		if(loginSuccess){
			window.location.href= url;
		} else {
			document.getElementById('errorMsg').innerHTML = errorMsg;
		}
	</script>
</body>
</html>