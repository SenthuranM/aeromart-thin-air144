<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html class="no-js" lang="">
	<head>
	    <meta charset="utf-8">
	    <title></title>
	    <meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
		<link rel="stylesheet" href="../css/mobile/bootstrap_no_cache.css">
		<link rel="stylesheet" href="../css/mobile/datepicker_no_cache.css">
	    <link rel="stylesheet" href="../css/mobile/main_no_cache.css">
	    <!-- build:css(.) styles/vendor.css -->
	    <!-- bower:css -->
	    <!-- endbower -->
	    <!-- endbuild -->
	    <!-- build:css(.tmp) styles/main.css -->

	    <!-- endbuild -->
	    <!-- build:js scripts/vendor/modernizr.js -->

	    <!-- endbuild -->

	</head>
	<body>
	    <!--[if lt IE 10]>
	    <![endif]-->
		<div class="row" >
			<div class="header col-xs-12 col-sm-12">
				<div id="bar" class="bar" style="display: none"><span></span><div class="loading_div" ></div></div>
			</div>
			<div class="header col-xs-12 col-sm-12" style="display: none">
				<div class="col-xs-2 col-sm-2" ><br><h4><a onclick="goBack()"><i class="glyphicon glyphicon-menu-left"></i>Back</a></h4></div>
				<div class="col-xs-8 col-sm-8" style="text-align: center"><h3 data-i18n="MobileLoyaltyReg_lblRegister">Register</h3></div>
				<div class="col-xs-2 col-sm-2" ></div>
			</div>
		</div>
		<br>

		<div class="content" id="regContent">
			<form id="customerRegForm" action="">
				<div class="row" id="accountDetails">
					<div class="form-detail col-xs-12 col-sm-12">
						<div class="col-xs-12 col-sm-12" style="margin-top: 10px;margin-bottom: 10px;">
							<h4 data-i18n="MobileLoyaltyReg_lblAccount">Account</h4>
                        </div>
						<div class="col-xs-12 col-sm-12"><input type="email"  class="form-control" id="txtEmail" name="customer.emailId" placeholder="Email" data-i18n-ph="MobileLoyaltyReg_lblEmail"></div>
						<div class="col-xs-12 col-sm-12"><input type="password"  class="form-control" id="password" name="customer.password" placeholder="Password" data-i18n-ph="MobileLoyaltyReg_lblPassword"></div>
						<div class="col-xs-12 col-sm-12"><input type="password"  class="form-control" id="confirmPassword" placeholder="Confirm Password" data-i18n-ph="MobileLoyaltyReg_lblConfirmPassword"></div>
						<br/><div class=" col-xs-12 col-sm-12"><div class="col-xs-12 col-sm-12" id="accountAlert"></div></div>
					</div>
				</div>
				<br>
				<div class="row" id="personalDetails">
					<div class="form-detail col-xs-12 col-sm-12">
						<div class="col-xs-12 col-sm-12" style="margin-top: 10px;margin-bottom: 10px;"><h4 data-i18n="MobileLoyaltyReg_lblAboutYou">About you</h4></div>
						<div class="form-input-left col-xs-4 col-sm-4">
							<select class="form-control" id="selTitle" name="customer.title">
								<option value="" id="translateTitle" data-i18n="MobileLoyaltyReg_lblTitle">Title</option>
							</select>
						</div>
						<div class="form-input-right col-xs-8 col-sm-8"><input type="text"  class="form-control" id="txtFirstName" name="customer.firstName" placeholder="First Name" data-i18n-ph="MobileLoyaltyReg_lblFirstName"></div>
						<div class=" col-xs-12 col-sm-12"><input type="text"  class="form-control" id="txtLastLame" name="customer.lastName" placeholder="Last Name" data-i18n-ph="MobileLoyaltyReg_lblLastName"></div>
						<div class=" col-xs-12 col-sm-12">
							<select class="form-control col-xs-12 col-sm-12" name="customer.nationalityCode"  id="selNationality">
								<option value="" data-i18n="MobileLoyaltyReg_lblNationality">Nationality</option>
							</select>
						</div>
						<div class=" col-xs-12 col-sm-12">
							<select class="form-control" name="customer.countryCode" id="selCountry">
								<option value="" data-i18n="MobileLoyaltyReg_lblCountry">Country of Residence</option>
							</select>
						</div>
						<div class=" col-xs-12 col-sm-12">
							<input type="text"  class="form-control" id="txtMobile"  name="customerMobile" placeholder="Mobile Number" maxlength="15" data-i18n-ph="MobileLoyaltyReg_lblMobileNo">
						</div>
						<br/>
						<div class=" col-xs-12 col-sm-12">
							<div class="col-xs-12 col-sm-12" id="detailsAlert"></div>
						</div>
			        </div>
				</div>
				<br>
				<div class="row" id="lmsDetails">
					<div class="form-detail col-xs-12 col-sm-12">
						<div class="col-xs-12 col-sm-12"><img src="../images/air-rewards_no_cache.png" alt="Air Rewards" style="padding-bottom: 6px ;padding-top: 10px"></div>
						<div class="col-xs-12 col-sm-12" style="margin-top: 10px;margin-bottom: 10px;">
                                <h5> <span data-i18n="MobileLoyaltyReg_lblWouldEnroll">Would you like to enroll with AIREWARDS</span> &nbsp;&nbsp; <input type="checkbox" id="optLMS" name="optLMS" checked> </h5>
                        </div>
						<div class=" col-xs-12 col-sm-12" id="lmsForm">
							<div class=" col-xs-12 col-sm-12"><input type="text" id="txtLMSDOB" name="lmsDetails.dateOfBirth" placeholder="Date of Birth" style="width:100%;padding-top: 9px" data-i18n-ph="MobileLoyaltyReg_lblDob"></p></div>
							<div class=" col-xs-12 col-sm-12"><input type="text"  class="form-control" id="txtLMSPPN" name="lmsDetails.passportNum" placeholder="Passport number" data-i18n-ph="MobileLoyaltyReg_lblPassportNo"></div>
							<br/><div class=" col-xs-12 col-sm-12"><div class=" col-xs-12 col-sm-12" id="lmsAlert"></div></div>
						</div>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="row">
						<div class="col-xs-12 col-sm-12" style=" padding-left: 15px;padding-right: 15px;">
							<button type="button" class="btn btn-danger btn-lg btn-block" id="submitRegistration" data-i18n="MobileLoyaltyReg_lblRegisterNow">Register Now</button></div>
						</div>
					</div>
			        <br>
			    </div>

			    <input type="hidden"  id="joinLMS" name="joinLMS"/>
				<input type="hidden"  id="lmsEmailId" name="lmsDetails.emailId"/>
				<input type="hidden"  id="lmsFirstName" name="lmsDetails.firstName"/>
				<input type="hidden"  id="lmsLastName" name="lmsDetails.lastName"/>
				<input type="hidden"  id="lmsGenderCode" name="lmsDetails.genderCode"/>
				<input type="hidden"  id="lmsNationalityCode" name="lmsDetails.nationalityCode"/>
				<input type="hidden"  id="lmsResidencyCode" name="lmsDetails.residencyCode"/>
				<input type="hidden"  id="lmsMobileNumber" name="lmsDetails.mobileNumber"/>

				<input type="hidden"  id="customerMobile" name="customer.mobile"/>
			</form>




			<script src="../js/mobile/modernizr.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/mobile/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/mobile/bootstrap.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<%-- 			<script src="../js/mobile/jquery-ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> --%>
			<script src="../js/globalConfig.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/mobile/bootstrap-datepicker.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<!-- 			<link rel="stylesheet" href="../css/mobile/jquery-ui_no_cache.css"> -->

		    <!-- endbuild -->

			<script>
				var enableGoogleAnalytics = <c:out value='${requestScope.enableGoogleAnalytics}' escapeXml='false' />;
				if (enableGoogleAnalytics) {
					(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
					function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
					e=o.createElement(i);r=o.getElementsByTagName(i)[0];
					e.src='https://www.google-analytics.com/analytics.js';
					r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
					ga('create','<c:out value='${requestScope.googleAnalyticsKey}' escapeXml='false' />');ga('send','pageview');
				}
		    </script>
			<script>

				function goBack() {
				    window.history.back();
				}
			</script>
			<script type="text/javascript">
				function changeMe(sel) {
					sel.style.color = "#555";
				}
			</script>
			<script src="../js/v2/customer/registerMobileCustomer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
			<script src="../js/v2/common/jquery.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		</div>
		<div class="content" id="successContent" hidden="hidden">
			<div class="row" >
				<div class="form-detail col-xs-12 col-sm-12">
					<div class="col-xs-12 col-sm-12"><h3 data-i18n="MobileLoyaltyReg_lblCongratulations">Congratulations!</h3><br/></div>
					<div class="col-xs-12 col-sm-12"><p data-i18n="MobileLoyaltyReg_lblYourAccountCreated">Your account was created successfully</p></div>
					<div class="col-xs-12 col-sm-12" id="cnfEmailMsg" hidden="hidden">
                        <p data-i18n="MobileLoyaltyReg_lblActivateAccount">To activate your account, please check your email and click on the confirmation link that was sent to you.</p>
                    </div>
				</div>
			</div>
			<br/>
		</div>
		<!-- Button trigger modal -->

		<div class="modal fade" id="lmsMergeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Merge Accounts</h4>
					</div>
					<div>
						<div class="modal-body">
							<p data-i18n="MobileLoyaltyReg_lblAccountExists">The account with this Airewards ID already exists. Would you like to merge your account?</p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="mergeOk" data-i18n="MobileLoyaltyReg_lblYes">Yes</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" data-i18n="MobileLoyaltyReg_lblNo">No</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="nameModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel" data-i18n="MobileLoyaltyReg_lblNameMissmatch">Name Mismatch</h4>
					</div>
					<div>
						<div class="modal-body">
							<p data-i18n="MobileLoyaltyReg_lblAccountMerge">The account with this Airewards ID already exists with a differrent "First name" and a "Last name". Please add the existing "First name" and "Last name" to continue with the merge.</p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" data-i18n="MobileLoyaltyReg_lblOk">Ok</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>