<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../../common/topHolder.jsp' %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
    <tr>
        <td>
            <div class="rowGap"></div>
            <table border='0' cellpadding='0' cellspacing='0' align='center' width='100%'>
                <%@ include file="../common/includeFrameTop.html" %>
                <div class="div-Table" >
                    <div class="table-Row one-Col" style="height: auto" id="Airewards_Panel">
                        <div class="table-Data one-Col">
                            <img src="../images/air-rewards_no_cache.png" height="25" alt="Air Rewards" style="padding:0px 0px 0px 0px;" />
                        </div>
                    </div>
                    <div class="table-Row one-Col" style="height: auto;display: none" id="userNotAirewards">
                        <div class="table-Data one-Col"><label>
                            <ul style="padding: 0">
                                <li><label id="earnPtsIDLMS"></label></li>
                                <li><label id="redeemPtsIDLMS"></label></li>
                                <li><label id="msgIDLMS"></label></li>
                            </ul></label>
                        </div>
                        <div class="table-Data one-Col alignRight">
                            <u:hButton name="btnActivateLMS1" id="btnActivateLMS1" value="Join Now" tabIndex="20" cssClass="redContinue" />
                        </div>
                    </div>
                    <div class="table-Row one-Col" style="height: auto;display: none" id="userAirewards">
                        <div class="table-Data one-Col">
                            <label id="lblEarnPtsID" class="fntBold hdFontColor"></label><br/>
                            <label id="lblEarnPtsIDLMS"></label>&nbsp;
                            	<a id="earnPtsLnk" target="_blank" href="lmsCrossPortalLogin.action?menuIntRef=EARN" style="cursor: pointer;">
                            		<u><label id="lblEarnPtsLnk"></label></u>
                            	</a><br/><br/>

                            <label id="lblRedeemPtsID" class="fntBold hdFontColor"></label><br/>
                            <label id="lblRedeemPtsIDLMS"></label>&nbsp;<a id="redeemPtsLnk" target="_blank" href="lmsCrossPortalLogin.action?menuIntRef=REDEEM" style="cursor: pointer;"><u><label id="lblRedeemPtsLnk"></label></u></a><br/><br/>

                            <label id="lblLinkID" class="fntBold hdFontColor"></label><br/>
                            <label id="lblLinkIDLMS"></label>&nbsp;<a id="linkLnk" target="_blank" href="lmsCrossPortalLogin.action?menuIntRef=RETRO" style="cursor: pointer;"><u><label id="lblLinkLnk"></label></u></a><br/><br/>

                            <%-- label id="lblAccountMgt" class="fntBold hdFontColor"></label><br/>
                            <label id="lblAccountMgtLMS"></label>&nbsp;<a target="_blank" href="lmsCrossPortalLogin.action?menuIntRef=PROFILE" ><u><label id="lblAccountMgtLnk"></label></u></a><br/><br/--%>
                        </div>
                    </div>

                </div>
                <%@ include file="../common/includeFrameBottom.html" %>
            </table>
        </td>
   </tr>
</table>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
    <script src="../js/v2/customer/lmsDetails.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</c:if>