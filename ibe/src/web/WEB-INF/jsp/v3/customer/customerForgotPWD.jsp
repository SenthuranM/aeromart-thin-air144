<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">		
		<title></title>
		<script src="../js/common/Common.js" type="text/javascript"></script> 
		<script src="../js/common/validations.js" type="text/javascript"></script> 		
		<%@ include file='../common/interlinePgHD.jsp' %>		 
	</head>
	<body id="direction">
		<div id="divLoadBg">
			<form id="frmPWD" name="frmPWD" method="post" action="">
				<table width="562px" border="0" cellpadding="0" cellspacing="0">
				  <tbody>
						<tr>
							<td colspan="2" class="tblBGB" class="alignLeft">
								<table width="500px" border="0" cellpadding="2" cellspacing="0">
								  <tbody>
								   <tr>
										<td colspan="2" class="rowGap"></td>							
									</tr>
									<tr>
										<td colspan="2">
										<label class="fntBold hdFontColor hdFont" id="lblForgotPwd"></label>
										</td>							
									</tr>
									<tr>
										<td colspan="2" class="rowGap"></td>							
									</tr>
									<tr>
										<td colspan="2">
											<label id="lblForgotPwdMsg"></label>
										</td>							
									</tr>
									<tr>
										<td colspan="4">
											<div id="spnError"></div>																
										</td>
									</tr>
									<tr>
										<td colspan="2">									
										</td>							
									</tr>
									<!-- <tr>
										<td colspan="2">	
											<label id="lblHighlightWith"></label><label class='mandatory'>&nbsp;*&nbsp;</label><label id="lblAreMandatory"> </label>			
										</td>							
									</tr> -->
									<tr>
										<td width="30%">
											<label class="fntEnglish" id="lblEmailAddress"></label>
										</td>							
										<td>
											<div dir="ltr">											
												<font class="mandatory">&nbsp;*&nbsp;</font>
												<input type="text" id="txtEmail" name="customer.emailId" maxlength="100" style="width: 120px;" /> 
											</div>
										</td>
									</tr>
									<tr id="securityQuestion">
										<td>
											<label id="lblSecurityQuestion"></label>
										</td>							
										<td>											
											<div dir="ltr">											
											<font class="mandatory">&nbsp;*&nbsp;</font>
											<select id="SelSecurityQuestion" name="customer.secretQuestion" size="1" style="width: 300px;position: relative;z-index: 1"> 
												<option value="" selected="selected">[Please Select a Question]</option>
											</select>
											</div>						
										</td>
									</tr>
									<tr id="securityAnswer">
										<td>
											<label id="lblAnswer"></label>
										</td>							
										<td>
											<div dir="ltr">											
												<font class="mandatory">&nbsp;*&nbsp;</font>
											<input type="text" id="txtAnswer" name="customer.secretAnswer" maxlength="100" style="width: 120px;"  />
											</div>											
										</td>
									</tr>
									<tr>
										<td colspan="2" class="rowGap">
										</td>					 
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td class="alignLeft">
											<input id="btnGetPWD" class="Button ButtonLarge"  title="Click here to get your forgotten password by mail" type="button"/>
											<input id="btnClose" class="Button"   type="button"/>
										</td>							
									</tr>
								</tbody>
						</table>
					</td>
					<td class="tblBGBRIGHT" width="1%"></td>
				</tr>
				<tr>
					<td class="tblBGBTML" width="1%"></td>
					<td class="tblBGBTM" width="98%"></td>
					<td class="tblBGTMR" width="1%"></td>
				</tr>
			</tbody>
		</table>			
	  </form>			 
	 <script src="../js/v2/customer/customerForgotPassword.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 		
	</div>
	</body>
</html>