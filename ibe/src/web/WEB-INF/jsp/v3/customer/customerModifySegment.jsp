<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<form id="frmCusModifySegment" method="post">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td valign="top" style="height: 200px;" class="tblBG">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="99%">
				  <tr>
					<td class="rowGap">
					</td>
			     </tr>
				 <tr>
						<td class="rowGap">
						</td>
				</tr>
				<tr>
						<td>
							<font class="fntBold hdFont hdFontColor">Modify Segment</font>
						</td>
				</tr>
				<tr>
						<td class="rowGrap">
				</td>
				</tr>
				<tr>
					<td valign="bottom">
								<font class="fntRed">Reservation Number : <font class="fntBold fntLarge"><span id="spnPNR"></span></font></font>
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="1" cellpadding="2" border="0" width="100%">										
						    <tr>
								<td valign='top'>													
									<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
											<tr>
												<td colspan="7" class="alignLeft Gridwt"><label id="lblDeparting"  class='fntBold hdFontColor'>Modifying Flight(s)</label></td>
											</tr>	
											<tr>
												<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'>Origin / Destination</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'>Departure</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'>Arrival</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'>Flight No</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblCarrier" class='gridHDFont fntBold'>Carrier</label></td>												
											</tr> 
											<tr>            
												<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'>Time</label></td>
												<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'>Time</label></td>            
											</tr>
											<tr id="departueFlightMS">
												<td width='35%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
												<td width='15%' class='rowColor' align='center'>
                                                    <label id="departureDate"  class="date-disp"></label>
                                                    <label id="departureDateValue" class="date-hid" style="display:none"></label>
                                                </td>
												<td width='8%' class='rowColor' align='center'><label id="departureTime"></label></td>
												<td width='15%' class='rowColor' align='center'>
                                                    <label id="arrivalDate"  class="date-disp"></label>
                                                    <label id="arrivalDateValue" class="date-hid" style="display:none"></label>
                                                 </td>
												<td width='8%' class='rowColor' align='center'><label id="arrivalTime"></label></td>
												<td width='10%' class='rowColor' align='center'><label id="flightNo"></label></td>
												<td width='9%' class='rowColor' align='center'><img id="carrierImage" src=""/></td>												
											</tr>               
										</table>																																
									</td>
								</tr>
								<tr>
									<td align="center">
									<input type="button"  value="Back" class="Button ui-state-default ui-corner-all" title="Click here to go to the previous page" id="btnBackModifySegment">
								</td>
							</tr>
							<tr>
								<td>
									<font>Please use the Booking Form on the left to search for your new flight(s) and modify according to your new requirements.</font>
								</td>
							</tr>
						</table>
					</td>
				</tr>					
			</table>
		 </td>
	</tr>
  </table>
  <script src="../js/v2/customer/customerReservationModifySegment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</form>