<%@ include file="/WEB-INF/jsp/common/directives.jsp"%>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<form id="frmResList">
	<table width='100%' border='0' cellpadding='0' cellspacing='0'
		align='center'>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="0" border="0" align="center"
					width="100%">
					<tbody>
						<tr>
							<td>
								<div id="trAirwards" style="display: none">
									<div class="rowGap"></div>
									<div id="errorLms"></div>
									<table border='0' cellpadding='0' cellspacing='0'
										align='center' width='100%'>
										<%@ include file="../common/includeFrameTop.html"%>
										<div>
											<img src="../images/air-rewards_no_cache.png"
												alt="Air Rewards" style="padding-bottom: 10px" />
										</div>
										<div class="div-Table" id="lmsJoinNow_reservationList">
											<table border='0' cellpadding='0' cellspacing='0'
												align='center' width='100%'>
												<tr>
													<td colspan="2"><label id="lblAirewardsJoinNow"></label><label>:</label>
                                                                        <input type="radio" name="cusOptLMS" id="cusOptLMS_yes" value="Y" />
                                                                        <label id="lblYes">Yes</label>
                                                                        <input type="radio" name="cusOptLMS" id="cusOptLMS_No" value="N" checked="checked"/>
                                                                        <label id="lblNo">No</label>
                                                    </td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
											</table>
										</div>
										<div class="table-Row one-Col"
											style="height: auto; display: none"
											id="lmsActivate_reservationList">
											<table border='0' cellpadding='0' cellspacing='0'
												align='center' width='100%'>											
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
												<tr>
													<td width="40%"><label id="lblDOBLms"></label></td>
													<td><input type="text" class="fontCapitalize"
														maxlength="50" size="50" name="dobLMS" id="textDOBLMS"
														style="width: 130px" /> <label class="mandatory"
														id="lblDOBLmsMand">*</label></td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
												<tr>
													<td width="40%"><label id="lblLanguageLms"></label></td>
													<td><select title="Language" style="width: 130px"
														name="languageLMS" id="textLanguageLMS">
															<option value="EN">English</option>
															<option value="AR">Arabic</option>
													</select> <label class="mandatory" id="lblLanguageLmsMand"
														style="display: none">*</label></td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
												<tr>
													<td width="40%"><label id="lblPpnLms"></label></td>
													<td><input type="text" class="fontCapitalize"
														maxlength="50" size="50" name="ppnLMS" id="textPpnLMS" />
													</td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
												<tr>
													<td colspan="2"><label id="lblLMSRefferdEMail"></label>
													</td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
												<tr>
													<td width="40%"><label id="lblRefferedEmailLms"></label></td>
													<td><input type="text" maxlength="50"
														name="RefferedEmailLMS" id="textRefferedEmailLMS" /></td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
												<tr>
													<td colspan="2"><label id="lblLMSFamilyHeadEMail"></label>
													</td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
												<tr>
													<td width="40%"><label id="lblEmailHLms"></label></td>
													<td><input type="text" maxlength="50" size="50"
														name="emailHLMS" id="textEmailHLMS" /> <label
														class="mandatory" id="lblEmailHMandLMS">*</label></td>
												</tr>
												<tr>
													<td colspan="2" class="rowGap"></td>
												</tr>
											</table>
										</div>
										<%@ include file="../common/includeFrameBottom.html"%>
									</table>
								</div>
								<div class="rowGap"></div>
								</td>
								</tr>
								<tr>
								<td>
								<!-- Loyalty Program -->
								<div class="div-Table" id="trActivateLoyalty" style="display: false">
									<table border='0' cellpadding='0' cellspacing='0'
										align='center' width='100%'>
										<%@ include file="../common/includeFrameTop.html"%>
										<div class="div-Table" >
                                        <div class="table-Row one-Col" style="height: auto">
                                            <div class="table-Data one-Col">
                                                <label class="fntBold hdFont hdFontColor">Mashreq Credit Card Account</label>
                                            </div>
                                        </div>
                                        <div class="table-Row one-Col" style="height: auto">
                                            <div class="table-Data one-Col">
                                                <label id="lblMashreqWelcome">Activate Mashreq Credit Card Account and start flying for free!</label>
                                            </div>
                                        </div>
                                        </div>
										<div class="table-Row one-Col">
											<div class="table-Data two-Col">
												<label id="lblInfoDoYouHaveAccount">Do you have Air
													Arabia Mashreq Card Loyality Account?</label>
											</div>
											<div class="table-Data left-TD">
												<font>:</font> <input type="radio" name="optCardAccLoyalty"
													id="optCardAcc_yes" value="Y" /> <label id="lblYes">Yes</label>
												<input type="radio" name="optCardAccLoyalty" id="optCardAcc_No"
													value="N" checked="checked" /> <label id="lblNo">No</label>
											</div>
										</div>
										<div class="extractor">
											<div class="table-Row one-Col thin-row">
												<div class="table-Data one-Col">
													<label id="lblInfoLoyaltyNo"></label>
												</div>
											</div>

											<div class="table-Row one-Col" id="trloyalatyAccount">
												<div class="table-Data left-TD">
													<label id="lblAccountNo"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font> <input type="text" maxlength="25" size="25"
														name="customer.loyaltyAccountNumber" id="txtAccountNoLoyalty" />
													<label class="mandatory">*</label>
												</div>
											</div>
											
												<div class="table-Row two-Col" id="trDOBLoyalty">
																<div class="table-Data left-TD">
																	<label id="lblDOBLoyalty"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" class="fontCapitalize" maxlength="50" size="50" name="customer.dateOfBirth" id="textDOBLoyalty"
														style="width: 100px" /> <label class="mandatory"
														id="lblDOBLoyaltyMand">*</label>
																	
																</div>
												</div>
												<div class="table-Row two-Col" id="trCityLoyalty">
																<div class="table-Data left-TD">
																	<label id="lblCity"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" class="fontCapitalize" maxlength="50" name="customer.city" id="txtCityLoyalty"/> 
																	<label class="mandatory" id="lblCityMand">*</label>
																</div>
												</div>
											
											<div class="table-Row one-Col thin-row">
												<div class="table-Data one-Col">
													<label class="fntSmall" id="lblInfoAccontValidate"></label>
												</div>
											</div>
										</div>
										<div class="table-Row  one-Col">
											<div class="table-Data one-Col">
												<label class="fntSmall">&nbsp;</label>
											</div>
										</div>
										<div class="end-Table"></div>
										<%@ include file="../common/includeFrameBottom.html"%>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td class="rowGap"></td>
						</tr>
						<tr id="trButtons">
								<td colspan="4">
										<table cellspacing="0" cellpadding="0" border="0" align="center" width="99%" >
												<tr>
													<td align="right" colspan="4" >
														<u:hButton name="btnRegisterCommon" id="btnRegisterCommon" value="Registration" tabIndex="20" cssClass="redContinue" />		 																		
												    </td>
											    </tr>
										</table>
								 </td>
						</tr>
						<tr>
							<td class="rowGap"></td>
						</tr>
						<tr>
							<td class="alignLeft paddingCalss"><label
								class="fntBold hdFont hdFontColor" id="lblReservations"></label>
							</td>
						</tr>
						<tr>
							<td class="rowGap"></td>
						</tr>
						<tr>
							<td class="alignLeft paddingCalss"><label id="lblMsgResNo">
							</label> <label id="lblMsgClickRes"> </label></td>
						</tr>
						<tr id="trResList">
							<td>
								<div id="spnReservations">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<%@ include file="../common/includeFrameTop.html"%>
										<table cellspacing="1" cellpadding="2" border="0" width="100%"
											class="GridTable">
											<tr>
												<td align="center" width="14%" class="GridHeader"><label
													class="fntBold fntWhite" id="lblResNo"></label></td>
												<td align="center" class="GridHeader"><label
													class="fntBold fntWhite" id="lblTrip"></label></td>
												<td align="center" width="23%" class="GridHeader"><label
													class="fntBold fntWhite" id="lblDeparture"></label></td>
												<td align="center" width="23%" class="GridHeader"><label
													class="fntBold fntWhite" id="lblArrival"></label></td>
												<td align="center" width="10%" class="GridHeader"
													colspan="2"><label class="fntBold fntWhite"
													id="status"></label></td>
											</tr>
											<tbody id="resTable"></tbody>
										</table>
										<%@ include file="../common/includeFrameBottom.html"%>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
</form>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	
</c:if>
