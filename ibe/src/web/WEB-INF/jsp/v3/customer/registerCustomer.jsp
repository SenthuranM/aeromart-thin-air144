<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="-1"/>
		<title><fmt:message key="msg.Page.Title" /></title>		
		<link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css"/>
		<script type="text/javascript" src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">
			//TODO Refactor - do not add more
			var GLOBALS = <c:out value="${requestScope.systemDefaultParam}" escapeXml="false" />;
			//TODO Refactor-don't add more
			var objCWindow = "";
			var dtC = new Date();
			var dtCM = dtC.getMonth() + 1;
			var dtCD = dtC.getDate();
			if (dtCM < 10){dtCM = "0" + dtCM}
			if (dtCD < 10){dtCD = "0" + dtCD}
	
			var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
			var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
			var strToDate	= strSysDate;
			if (UI_Top.holder().GLOBALS.currentDate != null && UI_Top.holder().GLOBALS.currentDate == "") {
				var date = UI_Top.holder().GLOBALS.currentDate;
				var dtTempSysDate = new Date(date.substr(6,4), (Number(date.substr(3,2)) - 1), date.substr(0,2));
				strSysDate  	= DateToString(addDays(dtTempSysDate, 0));
				dtSysDate		= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
				strToDate       = dtSysDate; 
			}
			var dataFromSocialSite = '<c:out value="${requestScope.dataFromSocialSite}" escapeXml="false" />';			
			
		</script>
		
		<link rel="stylesheet" href="../css/table_layout_EN.css" type="text/css" /> 	
	</head>
	<body>
		<c:import url="../common/pageLoading.jsp"/>
		<div id="divLoadBg" style="display: none;">
			<a href="#"  id="linkFocus"> </a>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgRegister">
				<!-- Top Banner -->
				<c:import url="../../../../ext_html/header.jsp" />
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<!-- Content holder -->
							<tr id="trForm">
							 <td colspan="2" class="mainbody alignLeft">
								<div id="sortable-ul">
								<div class="sortable-child">
									<div style="width:250px">
										<%@ include file='../../v3/common/includeLeftPanel.jsp' %>
									</div>
								</div>
								<div class="sortable-child">
								<form id="frmRegister" name="frmRegister" method="post" action="customerRegister.action">
								<div class="rightColumn">
									<div class="page-body" style="width: 100%;">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
										<tr>
											<td>
												<table width="100%" border="0" cellpadding="2" cellspacing="1" align='center'>
                                                    <tr id="lmsIntegrates">
                                                        <td valign="top">
                                                            <div class="ui-corner-TL ui-corner-TR" style="background: #ee0000">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td width="36%" style="padding: 15px 20px">
                                                                            <label style="color: white;font-size: 17px;line-height: 25px;">Get great benefits when you become a member of My Airarabia or Airewards program</label>
                                                                        </td>
                                                                        <td class="alignRight"><img src="../images/air-rewards-banner_no_cache.png" alt="Air Rewards"></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="rowGap"></div>
                                                        </td>
                                                    </tr>
													<tr>
														<td valign="top">
															<label id="lblRegisterNow" class="fntBold hdFontColor hdFont"></label>
														</td>
													</tr>
													<tr>
														<td>
															<div id="spnError" class="mandatory"></div>																
														</td>
													</tr>
													<tr>
														<td valign="top">
															<label id="lblSignUp"></label><br/>
															<label class="fntSmall" id="lblInfoFillReg"></label>
															<label class="fntSmall" id="lblHighlightWith"> </label> <font class="mandatory">*</font> <label id="lblAreMandatory"></label>
														</td>
													</tr>
													<tr>
														<td>
														 <!-- social register panel -->
														<div id="socialRegister" style="display:none;">
															<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																<%@ include file="../common/includeFrameTop.html"%> 
																   <div id="tdFBSignIn" style="display: none" class="soclaiButton soCommonBtn">
																		<a href="javascript:void(0)" title="Register With Facebook" id="btnFBSignIn" name="btnFBSignIn" class="buttonFB">
																			<span class="logo"></span>
																		    <span><label id="lblRegisterWithFacebook">Register With Facebook</label></span>
																	  </a>
																  </div>
																  <div id="tdLinkdinSignIn" style="display: none" class="soclaiButton soCommonBtn">
																		<a href="javascript:void(0)" title="Register With LinkedIn" id="btnLinkdinSignIn" name="btnLinkdinSignIn" class="buttonLN">
																			<span class="logo"></span>
																		    <span><label id="lblRegisterWithLinkedIn">Register With LinkedIn</label></span>
																	  </a>
																  </div>
													 		     <div class="end-Table"></div> 
													 		<%@ include file="../common/includeFrameBottom.html"%> 
															</table>
														</div>
														<div class="rowGap"></div>
														<!-- Login info Table -->
														<div class="div-Table" id="loginInfo">
															<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
															<%@ include file="../common/includeFrameTop.html"%>
															<div class="table-Row one-Col">
																<div class="table-Data one-Col">
																	<label class="fntBold hdFontColor" id="lblLoginInfo"></label>
																</div>
															</div>
															<div class="table-Row two-Col" id="trEmail">
																<div class="table-Data left-TD">
																	<label id="lblEmail"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" name="customer.emailId" title="login ID" maxlength="100" style="width: 130px"  id="txtEmail"/>
																	<label class="mandatory" id="lblEmailMand">*</label>
																</div>
															</div>
															<div class="table-Row two-Col">
																<div class="table-Data one-Col">
																	<label class="fntSmall" id="lblInfoLoginID"></label>
																</div>
															</div>
															
															<div class="table-Row two-Col skipPass">
																<div class="table-Data left-TD">
																	<label id="lblPassword"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="password" maxlength="12" size="10" name="customer.password" id="txtPassword"/>  
																	<label id="lblPassowrdMand"><font class="mandatory">*</font></label>
																</div>
															</div>
															<div class="table-Row two-Col skipPass">
																<div class="table-Data one-Col">
																	<label id="lblInfoPassword" class="fntSmall"></label>
																</div>
															</div>
															
															<div class="table-Row two-Col skipPass">
																<div class="table-Data left-TD">
																	<label id="lblConfirmPwd"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="password" maxlength="12" size="10" name="txtConPassword" id="txtConPassword"/> 
																	<label id="lblPassowrdConfMand"><font class="mandatory">*</font></label>
																</div>
															</div>
															<div class="table-Row  one-Col">
																<div class="table-Data one-Col">
																	<label class="fntSmall">&nbsp;</label>
																</div>
															</div>
															<div class="end-Table"></div>
															<%@ include file="../common/includeFrameBottom.html"%>
															</table>
														</div>
														<div class="rowGap"></div>
														<!-- Security question Table -->
														<div class="div-Table" id="tblSecurityQuestion">
															<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
															<%@ include file="../common/includeFrameTop.html"%>
															<div class="table-Row one-Col">
																<div class="table-Data one-Col">
																	<label class="fntBold hdFontColor" id="lblForgotPwd"></label>
																</div>
															</div>
															
															<div class="table-Row one-Col">
																<div class="table-Data left-TD">
																	<label id="lblSecurityQues"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<select style="width: 300px;" size="1" name="customer.secretQuestion" id="selSecQ">	
																		<option value="">[Please Select a Question]</option>												
																	</select>																		
																	<label class="mandatory" id="lblSecQuesMand">*</label>
																</div>
															</div>
															
															<div class="table-Row two-Col">
																<div class="table-Data left-TD">
																	<label id="lblYourAnswer"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" maxlength="100" name="customer.secretAnswer" id="txtAns"/>
																	<label class="mandatory" id="lblSecAnswerMand">*</label>
																</div>
															</div>
															<div class="table-Row two-Col">
																<div class="table-Data one-Col">
																	<label class="fntSmall" id="lblInfoSecurityQues"></label>
																</div>
															</div>
															<div class="table-Row two-Col" id="trAlternateEmail">
																<div class="table-Data left-TD">
																	<label id="lblAltEmail"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" maxlength="100" name="customer.alternativeEmailId" id="txtAlternateEmail"/>
																	<label class="mandatory" id="lblAlternateEmailMand">*</label>
																</div>
															</div>
															<div class="table-Row  one-Col">
																<div class="table-Data one-Col">
																	<label class="fntSmall">&nbsp;</label>
																</div>
															</div>
															<div class="end-Table"></div>
															<%@ include file="../common/includeFrameBottom.html"%>
															</table>
														</div>
														<div class="rowGap"></div>
														<!-- About you Table -->
														<div class="div-Table" id="tblContactDetails">
															<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
															<%@ include file="../common/includeFrameTop.html"%>
															<div class="table-Row one-Col">
																<div class="table-Data one-Col">
																	<label class="fntBold hdFontColor" id="lblAboutU"></label>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="trTitle">
																<div class="table-Data left-TD">
																	<label id="lblTitle"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<select  style="width: 50px;" size="1" name="customer.title" id="selTitle">
																	</select>
																	<label class="mandatory" id="lblTitleMand"> *</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trFirstName">
																<div class="table-Data left-TD">
																	<label id="lblFirstName"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" style="width:130px" class="fontCapitalize" maxlength="50" name="customer.firstName" id="txtFirstName"/>
																	<label class="mandatory" id="lblFirstNameMand">*</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trLastName">
																<div class="table-Data left-TD">
																	<label id="lblLastName"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" style="width:130px" class="fontCapitalize" maxlength="50" name="customer.lastName" id="txtLastName"/>
																	<label class="mandatory" id="lblLastNameMand">*</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trGender">
																<div class="table-Data left-TD">
																	<label id="lblGender"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input checked="checked"  type="radio" class="NoBorder" name="customer.gender" id="radGender" value="M"/>
																	<label id="lblMale"></label>
																	<input type="radio" class="NoBorder" name="customer.gender" id="radFemale" value="F"/>
																	<label id="lblFemale"></label>	
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trDOB">
																<div class="table-Data left-TD">
																	<label id="lblDOB"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text"  size="10" maxlength="10" name="customer.dateOfBirth" id="txtDateOfBirth"/>
																	<img border="0" src="../images/Calendar2_no_cache.gif" id="imgCalendar" class="cursorPointer"/>
																	<label class="mandatory" id="lblDOBMand"> *</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trNationality">
															<div class="table-Data one-Col">
																<div class="table-Data left-TD">
																	<label id="lblNationality"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<select style="width: 135px;" size="1" name="customer.nationalityCode" id="selNationality">																			
																	</select>
																	<label class="mandatory" id="lblNationalityMand">*</label>
																</div>
															</div>
															</div>
															
															<div class="table-Row two-Col" id="trAddr1">
																<div class="table-Data left-TD">
																	<label id="lblAddress"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" maxlength="100" name="customer.addressStreet" id="txtAddr1"/> 
																	<label class="mandatory" id="lblAddrMand">*</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trCity">
																<div class="table-Data left-TD">
																	<label id="lblCity"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" class="fontCapitalize" maxlength="50" name="customer.city" id="txtCity"/> 
																	<label class="mandatory" id="lblCityMand">*</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trAddr2">
																<div class="table-Data left-TD">
																	<label >&nbsp; </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" maxlength="100" name="customer.addressLine" id="txtAddr2"/>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trZipCode">
																<div class="table-Data left-TD">
																	<label id="lblZipCode"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" maxlength="10" size="10" name="customer.zipCode" id="txtZipCode"/> 
																	<label class="mandatory" id="lblZipCodeMand">*</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trCountry">
																<div class="table-Data left-TD">
																	<label id="lblCountryResidence"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<select style="width: 135px;"  size="1" name="customer.countryCode" id="selCountry">																														
																	</select>
																	<label class="mandatory" id="lblCountryMand">*</label>
																</div>
															</div>
															<!-- Contact Details -->
															<div class="table-Row one-Col thin-row trPhoneNoHeader">
																<div class="table-Data one-Col">
																	<label class="fntBold hdFontColor" id="lblContactDetails"></label>
																</div>
															</div>
															
															<div class="table-Row one-Col thin-row trPhoneNoLabel">
																<div class="table-Data left-TD">
																	<label>&nbsp;</label>
																</div>
																<div class="table-Data right-TD">
																	<label class="fntSmall " id="lblCountryCode"></label>
																	<label class="fntSmall areaCode" id="lblAreaCode"></label>
																	<label class="fntSmall " id="lblNumber"></label>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="trMobileNo">
																<div class="table-Data left-TD">
																	<label id="lblMobileNo"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtMCountry" id="txtMCountry"/>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtMArea" id="txtMArea" class="areaCode"/>
																	<input type="text"  maxlength="10" style="width: 100px;" name="txtMobile" id="txtMobile"/>
																	<label class="mandatory" id="lblMobileMand">*</label>
																	<div id="atLeasetOne" class="floatItem" style="width:300px"><font class="mandatory" >*&nbsp;</font> <label class="fntSmall" id="lblAtleaseOneText">Please provide at least one phone Number</label></div>
																	<div style="clear:both"></div>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="trPhoneNo">
																<div class="table-Data left-TD">
																	<label id="lblTelNo"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtPCountry" id="txtPCountry"/>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtPArea" id="txtPArea" class="areaCode"/>
																	<input type="text"  maxlength="10" style="width: 100px;" name="txtTelephone" id="txtTelephone"/>
																	<label class="mandatory" id="lblPhoneMand">*</label>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="trHomeOfficeNo">
																<div class="table-Data left-TD">
																	<label id="lblOfficeOHomeNo"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtOCountry" id="txtOCountry"/>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtOArea" id="txtOArea" class="areaCode"/>
																	<input type="text"  maxlength="10" style="width: 100px;" name="txtOfficeTel" id="txtOfficeTel"/>
																	<label class="mandatory" id="lblHomeOfficeMand">*</label>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="trFaxNo">
																<div class="table-Data left-TD">
																	<label id="lblFaxNo"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text"  maxlength="4" style="width: 50px;" id="txtFCountry"/>
																	<input type="text"  maxlength="4" style="width: 50px;" id="txtFArea" class="areaCode"/>
																	<input type="text" maxlength="10" style="width: 100px;" id="txFax"/>
																	<label class="mandatory" id="lblFaxMand">*</label>
																</div>
															</div>
															
															<div class="table-Row  one-Col">
																<div class="table-Data one-Col">
																	<label class="fntSmall">&nbsp;</label>
																</div>
															</div>
															<div class="end-Table"></div>
															<%@ include file="../common/includeFrameBottom.html"%>
															</table>
														</div>
														<div class="rowGap"></div>
														<!-- Emergency Contact Details -->
														<div class="div-Table " id="trEmgnContactInfo">
															<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
															<%@ include file="../common/includeFrameTop.html"%>
															<div class="table-Row one-Col">
																<div class="table-Data one-Col">
																	<label class="fntBold hdFontColor" id="lblEmergencyContactInfo"></label>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="trEmgnTitle">
																<div class="table-Data left-TD">
																	<label id="lblTitle"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<select  style="width: 50px;" size="1" name="customer.emgnTitle" id="selEmgnTitle">
																	</select>
																	<label class="mandatory" id="lblEmgnTitleMand"> *</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trEmgnFirstName">
																<div class="table-Data left-TD">
																	<label id="lblFirstName"> </label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" class="fontCapitalize" maxlength="50" name="customer.emgnFirstName" id="txtEmgnFirstName"/> 
																	<label class="mandatory" id="lblEmgnFirstNameMand">*</label>
																</div>
															</div>
															
															<div class="table-Row two-Col" id="trEmgnLastName">
																<div class="table-Data left-TD">
																	<label id="lblLastName"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text"  class="fontCapitalize" maxlength="50" name="customer.emgnLastName" id="txtEmgnLastName"/> 
																	<label class="mandatory" id="lblEmgnLastNameMand">*</label>
																</div>
															</div>
															
															<div class="table-Row one-Col thin-row" id="emgnPhone1">
																<div class="table-Data left-TD">
																	<label>&nbsp;</label>
																</div>
																<div class="table-Data right-TD">
																	<label class="fntSmall " id="lblCountryCode"></label>
																	<label class="fntSmall areaCode" id="lblAreaCode"></label>
																	<label class="fntSmall " id="lblNumber"></label>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="emgnPhone2">
																<div class="table-Data left-TD">
																	<label id="lblTelNo"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPCountry" id="txtEmgnPCountry"/>
																	<input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPArea" id="txtEmgnPArea" class="areaCode"/>
																	<input type="text"  maxlength="10" style="width: 100px;" name="txtEmgnTelephone" id="txtEmgnTelephone"/>
																	<label class="mandatory" id="lblEmgnPhoneMand">*</label>
																</div>
															</div>
															
															<div class="table-Row one-Col" id="trEmgnEmail">
																<div class="table-Data left-TD">
																	<label id="lblEmail"></label>
																</div>
																<div class="table-Data right-TD">
																	<font>:</font>
																	<input type="text" maxlength="100" size="25" name="customer.emgnEmail" id="txtEmgnEmail"/> 
																	<label class="mandatory" id="lblEmgnEmailMand">*</label>
																</div>
															</div>
															
															<div class="table-Row  one-Col">
																<div class="table-Data one-Col">
																	<label class="fntSmall">&nbsp;</label>
																</div>
															</div>
															<div class="end-Table"></div>
															<%@ include file="../common/includeFrameBottom.html"%>
															</table>
                                                            <div class="rowGap"></div>
														</div>


														<!-- Air Rewards -->
														<div class="div-Table" id="trLMSLoyalty" >
															<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
															<%@ include file="../common/includeFrameTop.html"%>
                                                                <div class="table-Row one-Col" style="height: auto">
                                                                    <div class="table-Data one-Col">
                                                                        <img src="../images/air-rewards_no_cache.png" alt="Air Rewards" style="padding-bottom: 6px" />
                                                                    </div>
                                                                </div>

                                                                <div class="table-Row one-Col">
                                                                    <div class="table-Data one-Col">
                                                                        <label id="lblInfoDoYouHaveAirewards"></label>
                                                                    <%--</div>
                                                                    <div class="table-Data left-TD">--%>
                                                                        <label>:</label>
                                                                        <input type="radio" name="optLMS" id="optLMS_yes" value="Y" />
                                                                        <label id="lblYes">Yes</label>
                                                                        <input type="radio" name="optLMS" id="optLMS_No" value="N" checked="checked"/>
                                                                        <label id="lblNo">No</label>
                                                                    </div>
                                                                </div>
                                                                <div class="extractor">
																<div class="table-Row one-Col">
																	<div class="table-Data one-Col">
																		<label class="fntBold hdFontColor" id="lblLMSInfo"></label>
																	</div>
																</div>

																<div class="table-Row one-Col" id="trDOBLMS">
                                                                    <div class="table-Data left-TD">
                                                                        <label id="lblDOBLMS" class="fntBold"></label>
                                                                    </div>
																	<div class="table-Data two-col">
																		<label>:</label>
																		<input type="text" name="lmsDetails.dateOfBirth" readonly="readonly" title="Date of Birth" maxlength="100" style="width:66%"  id="txtDOBLMS"/>
																		<label class="mandatory" id="lblDOBMandLMS">*</label>
																	</div>
																</div>
																<div class="table-Row one-Col auto-height" id="trLanguageLMS">
                                                                    <div class="table-Data left-TD">
                                                                        <label id="lblLanguageLMS" class="fntBold"></label>
                                                                    </div>
																	<div class="table-Data two-col">
																		<label>:</label>
																		<select name="lmsDetails.language" title="Language" id="txtLanguageLMS">
																			<option value="EN">English</option>
																			<option value="AR">Arabic</option>
																		</select>
																	</div>
																</div>
																<div class="table-Row one-Col" id="trPPLMS">
																	<div class="table-Data left-TD">
																		<label id="lblPPLMS" class="fntBold"></label>
																	</div>
																	<div class="table-Data two-col">
																		<font>:</font>
																		<input type="text" name="lmsDetails.passportNum" title="Passport Number" maxlength="100" style="width: 130px"  id="txtPpnLMS"/>
                                                                        <label class="mandatory" id="lblPPLMSMand" style="display: none">*</label>
																	</div>
																</div>
                                                                <div class="table-Row one-Col auto-height">
                                                                    <div class="table-Data one-Col">
                                                                        <label id="lblLMSRefferdEMail"></label>
                                                                    </div>
                                                                </div>
                                                                <div id="tblRefferdEmailLms" class="table-Row one-Col" style=" height: auto;">
                                                                    <div class="table-Data left-TD">
                                                                        <label id="lblReferringEmailLms" class="fntBold"></label>
                                                                    </div>
                                                                    <div class="table-Data two-col"> <label class="font">:</label>
                                                                        <input type="text" id="txtRefferedEmailLMS" name="lmsDetails.refferedEmail" maxlength="50" autocomplete="off">
                                                                        <label class="mandatory" id="lblRefferedEmailLmsMand" style="display: none">*</label>
                                                                    </div>
                                                                </div>

                                                                <div class="table-Row one-Col auto-height">
                                                                    <div class="table-Data one-Col">
                                                                        <label id="lblLMSFamilyHeadEMail" ></label>
                                                                    </div>
                                                                </div>
																<div class="table-Row two-Col" id="trEmailHLMS">
                                                                    <div class="table-Data left-TD">
                                                                        <label id="lblFamilyHeadLMS" class="fntBold"></label>
                                                                    </div>
																	<div class="table-Data two-col">
																		<font>:</font>
																		<input type="text" name="lmsDetails.headOFEmailId" title="Family Head Email" maxlength="100" style="width: 130px"  id="txtEmailHLMS"/>
																		<label class="mandatory" id="lblEmailHMandLMS" style="display: none">*</label>
																	</div>
																</div>
				                                                </div>
															<%@ include file="../common/includeFrameBottom.html"%>
															</table>
                                                            <div class="rowGap"></div>
														</div>


                                                         <!-- Loyalty Program -->
                                                         <div class="div-Table" id="trLoyalty">
                                                             <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                 <%@ include file="../common/includeFrameTop.html"%>
                                                                 <div class="table-Row one-Col">
                                                                     <div class="table-Data one-Col">
                                                                         <label id="lblInfoLoyaltyProgram" class="fntBold hdFontColor"></label>
                                                                     </div>
                                                                 </div>
                                                                 <div class="table-Row one-Col">
                                                                     <div class="table-Data two-Col">
                                                                         <label id="lblInfoDoYouHaveAccount">Do you have Air Arabia Mashreq Card Loyality Account?</label>
                                                                     </div>
                                                                     <div class="table-Data left-TD">
                                                                         <font>:</font>
                                                                         <input type="radio" name="optCardAcc" id="optCardAcc_yes" value="Y"/>
                                                                         <label id="lblYes">Yes</label>
                                                                         <input type="radio" name="optCardAcc" id="optCardAcc_No" value="N" checked="checked"/>
                                                                         <label id="lblNo">No</label>
                                                                     </div>
                                                                 </div>
                                                                 <div class="extractor">
                                                                     <div class="table-Row one-Col thin-row">
                                                                         <div class="table-Data one-Col">
                                                                             <label id="lblInfoLoyaltyNo"></label>
                                                                         </div>
                                                                     </div>

                                                                     <div class="table-Row one-Col" id="trloyalatyAccount">
                                                                         <div class="table-Data left-TD">
                                                                             <label id="lblAccountNo"></label>
                                                                         </div>
                                                                         <div class="table-Data right-TD">
                                                                             <font>:</font>
                                                                             <input type="text" maxlength="25" size="25" name="customer.loyaltyAccountNumber" id="txtAccountNo"/>
                                                                             <label class="mandatory">*</label>
                                                                         </div>
                                                                     </div>
                                                                     <div id="dobloyalatyAccount"></div>

                                                                     <div class="table-Row one-Col thin-row">
                                                                         <div class="table-Data one-Col">
                                                                             <label class="fntSmall" id="lblInfoAccontValidate"></label>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="table-Row  one-Col">
                                                                     <div class="table-Data one-Col">
                                                                         <label class="fntSmall">&nbsp;</label>
                                                                     </div>
                                                                 </div>

                                                                 <div class="end-Table"></div>
                                                                 <%@ include file="../common/includeFrameBottom.html"%>
                                                             </table>
                                                         </div>
                                                         <div class="rowGap" id="LMSDetails"></div>
                                                         <!-- Loyalty Program -->
													</td>
												</tr>
												<tr id="tblPreference">
													<td colspan='4'>
														<table cellspacing="0" cellpadding="0" border="0" align="center" width="99%" >
															<tbody>
																<tr id="trPromotionPref">
																	<td>
																		<table cellspacing="0" cellpadding="2" border="0" class="alignLeft" width="100%">
																		<tr>
																			<td colspan="4">
																				<label class="fntBold hdFontColor" id="lblYourPref"></label>
																				<input type="hidden" name="questionaire[0].questionKey" value="1" />
																				<input type="hidden" name="questionaire[0].questionAnswer" value="" />
																			</td>
																		</tr>
																		<tr id="trPromotionPref">
																			<td colspan="4">
																				<input type="hidden" name="questionaire[1].questionKey" value="2" />
																				<input type="hidden" name="questionaire[1].questionAnswer" value="Y" id="promotional" />
																				<input type="checkbox"  checked="checked" value="Y" id="chkQ1" class="NoBorder" />
																				&nbsp;<label id="lblInfoPromotional"></label>
																			</td>
																		</tr>
																		<tr><td class="Dots" colspan="4"><label > </label></td></tr>
																		</table>
																	</td>
																</tr>
																<tr id="tblEmailPref">
																	<td>
																		<table border="0" cellpadding="2" cellspacing="0" width="100%" >
																			<tr>
																				<td>
																					<label id="lblInfoEmailFormat"></label>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<input type="hidden" name="questionaire[2].questionKey" value="3" />
																					<input type="radio" checked="checked" value="HTML" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
																					<label id="lblHtml"></label>
																				</td>
																			</tr>
																			<tr>
																				<td>											
																					<input type="radio" value="PLAIN" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
																					<label id="lblText"> </label>
																				</td>
																			</tr>
																			<tr><td class="Dots" colspan="4"><label > </label></td></tr>
																		</table>
																	</td>
																</tr>
																
																<tr id="tblSmsPref">
																	<td>
																		<table border="0" cellpadding="2" cellspacing="0" width="100%" >
																			<tr>
																				<td>
																					<label id="lblInfoSMS"></label>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<input type="hidden" name="questionaire[3].questionKey" value="4" />
																					<input type="radio" value="Yes" name="questionaire[3].questionAnswer" id="radQ4"  class="NoBorder"/>
																					<label id="lblInfoMobile"> </label>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<input type="radio"  checked="checked"  value="No" name="questionaire[3].questionAnswer" id="radQ4" class="NoBorder"/><label id="lblNo"> </label>
																				</td>
																			</tr>
																			<tr><td class="Dots" colspan="4"><label > </label></td></tr>
																		</table>
																	</td>
																</tr>
																
																<tr id="tblLanguagePref">
																	<td>
																		<table border="0" cellpadding="2" cellspacing="0" width="100%" >
																			<tr>
																				<td colspan="4">
																					<label id="lblInfoLan"></label>
																					<input type="hidden" name="questionaire[4].questionKey" value="5" />
																				</td>
																			</tr>	
																			<tr id="templateLanguages">
																				<td colspan="4">																		
																					<input type="radio" tabindex="21" value="" name="questionaire[4].questionAnswer" id="radQ5" class="NoBorder"/>
																						<label id="lblLanName">  </label>
																				</td>
																			</tr>															
																			<tr>
																				<td colspan="4">
																					<label id="lblInfoContactLan"> </label>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="4">
																					<input type="hidden" name="questionaire[5].questionKey" value="6" />
																					<input type="text" size="50" name="questionaire[5].questionAnswer" maxlength="50" value="" id="txtQ2"/>
																				</td>
																			</tr>
																			<tr><td class="Dots" colspan="4"><label > </label></td></tr>
																		</table>
																	</td>
																</tr> 
															</tbody></table>
													</td>
											</tr>
											<tr id="aggrementInfo">
												<td colspan="4">	
													<table border="0" cellpadding="2" cellspacing="0" width="100%" >
														<tr><td>														
														<label id="lblInfoAgree1" hidden> </label>
														<br/><br/>	
														<label id="lblInfoAgree2"> </label>	
														<br/><br/>
														<div class="divfont" id="lblInfoAgree3"> </div>	
														</td></tr>
													</table>																															
												</td>
											</tr>
											<tr id="trButtons">
												<td colspan="4">
													<table cellspacing="0" cellpadding="0" border="0" align="center" width="99%" >
														<tr>
															<td align="center" colspan="4" >
																<u:hButton  value="Previous" cssClass="backPrevious" name="btnPrevious" id="btnPrevious"/>
																<u:hButton  value="Reset" cssClass="redNormal" name="btnReset" id="btnReset"/>
																<u:hButton name="btnRegister" id="btnRegister" value="Registration" tabIndex="20" cssClass="redContinue" />		 
																<%--<input type="button" title="Click here for Registration" class="Button ButtonLarge" name="btnRegister" id="btnRegister"/> --%>																		
															</td>
														</tr>
													</table>
												</td>
											</tr>											
										</table>
										</td></tr></table>
										</div>
										</div>
										<%-- Handle common parameters --%>
										<%@ include file='../common/iBECommonParam.jsp'%>
										<input type="hidden" name="customer.telephone" id="hdnTelNo"/>
										<input type="hidden" name="customer.mobile" id="hdnMobile"/>
										<input type="hidden" name="customer.officeTelephone" id="hdnOffice"/>
										<input type="hidden" name="customer.fax" id="hdnFax"/>	
										<input type="hidden" name="customer.emgnPhoneNo" id="hdnEmgnPhoneNo"/>
										
										<%--Social Customer attributes --%>	
										<input type="hidden" name="customer.socialCustomerId" id="socialCustomerId"/>
										<input type="hidden" name="customer.socialSiteCustId" id="socialSiteCustId"/>	
										<input type="hidden" name="customer.socialCustomerTypeId" id="socialCustomerTypeId"/>
										<input type="hidden" name="customer.profilePicUrl" id="profilePicUrl"/>	
										
										<%-- Handle LMS Mobile  --%>
										<input type="hidden" name="lmsDetails.emailId" id="hdnEmailLMS"/>
										<input type="hidden" name="lmsDetails.firstName" id="hdnFirstNameLMS"/>
										<input type="hidden" name="lmsDetails.lastName" id="hdnLastNameLMS"/>
										<input type="hidden" name="lmsDetails.mobileNumber" id="hdnMobileLMS"/>
										<input type="hidden" name="lmsDetails.nationalityCode" id="hdnNationalityCodeLMS"/>
										<input type="hidden" name="lmsDetails.residencyCode" id="hdnResidencyCodeLMS"/>
										<input type="hidden" name="lmsDetails.genderCode" id="hdnGenderCodeLMS"/>
										<input type="hidden" name="lmsDetails.appCode" id="hdnAppCode" readonly="readonly" value="APP_IBE"/>
									</form>
									</div>
									</div>
								  </td>
							 </tr>
							 <%-- Success Message --%>
							 <tr id="trMsg">
							 	<td>							 	
									<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
										<tr>
											<td class="pageBody">
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td valign="top" align="center" class="alignLeft">
																<table width='95%' border='0' cellpadding='2' cellspacing='0'>																
																	<tr>
																		<td valign="top" class="spMsg">
																			<img src="../images/n058_no_cache.gif" vspace="3" hspace="3" align="absmiddle" />
																			<label id="lblInfoRegSuccess" class="fntBold"> </label>	
																		</td>
																	</tr>
																	<tr id="trLmsReg">
																		<td valign="top" class="spMsg">
																			<img src="../images/n058_no_cache.gif" vspace="3" hspace="3" align="absmiddle" />
																			<label id="lblLmsEmail" class="fntBold"> </label>	
																		</td>
																	</tr>
																	<tr><td><br/><br/></td> </tr>																
																	<tr>
																		<td  align="center">
																			<u:hButton name="btnSignIN" id="btnSignIN" value="Sign In" tabIndex="20" cssClass="redContinue" />
																			<%--<input type="button"  value="Sign In" class="Button" name="btnSignIN" id="btnSignIN"/> --%>		
																		</td>
																	</tr>																	
																	<tr><td><br/><br/></td> </tr>																																		
																</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>									
							 </tr>
							 <tr>
								<td class='appLogo' colspan="2"></td>
							</tr>
						</table>
					</td>
				</tr>
				<c:import url="../../../../ext_html/cfooter.jsp" />
			</table>
		</div>
		<form id="frmLogin" method="post" name="frmLogin" action="">
			<%-- Handle social site related responses--%>
			<%@ include file='../../v2/customer/socialCustomerParam.jsp'%>	
			<%@ include file='../common/iBECommonParam.jsp'%>
		</form>
		<form id="submitForm" method="post">
		</form>
		
		<script type="text/javascript">
			var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
		</script>
		<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />		
		<script src="../js/v2/customer/socialCustomer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
		<script src="../js/v2/customer/registerCustomer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
		<script src="../js/v2/customer/customerValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<div id='divLoadMsg' class="mainPageLoader">
			<%@ include file='../common/includeLoadingMsg.jsp' %>
		</div>
	</body>
	<%@ include file='../common/inline_Tracking.jsp' %>
</html>