<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="-1"/>	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><fmt:message key="msg.Page.Title"/></title>	
		<%-- Handle common parameters --%>
		<%@ include file='../common/iBECommonParam.jsp'%>		
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script type="text/javascript">
			 var strReqParam = '<c:out value="${requestScope.sysReqParamAA}" escapeXml="false" />';
		</script>
	</head>
	<body>		
		<div id="divLoadBg" style="display: none">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgLogin">
				<!-- Top Banner -->
				<c:import url="../../../../ext_html/header.jsp" />
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<!-- Content holder -->
							<tr>
								<td colspan="2" class="mainbody alignLeft">
									<div id="sortable-ul">
									<div class="sortable-child">
										<div style="width:250px">
											<%@ include file='../../v3/common/includeLeftPanel.jsp' %>
										</div>
									</div>
									<div class="sortable-child">
										<div class="rightColumn">
											<form id="frmLogin" method="post" name="frmLogin" action="">
											<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" >
                                                <tr id="lmsIntegrates">
                                                    <td valign="top">
                                                        <div class="ui-corner-TL ui-corner-TR" style="background: #ee0000">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td width="36%" style="padding: 15px 20px">
                                                                        <label style="color: white;font-size: 17px;line-height: 25px;">Get great benefits when you become a member of My Airarabia or Airewards program</label>
                                                                    </td>
                                                                    <td class="alignRight"><img src="../images/air-rewards-banner_no_cache.png" alt="Air Rewards"></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="rowGap"></div>
                                                    </td>
                                                </tr>
												<tr>
													<td colspan="3"><label class="fntBold hdFontColor hdFont" id="lblLoginTitle"></label></td>
												</tr>
												<tr>
													<td colspan="3" class="rowGap">
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<%@ include file="../common/includeFrameTop.html"%>
																<table cellspacing="2" cellpadding="0" border="0" align="center" width="100%" class="paddingL5">
																	<tbody>
																	<tr>
																		<td colspan="2" class="rowGap">
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2">
																			<label class="mandatory fntBold" id="lblLoginStatus"><b></b></label>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" class="rowGap">
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<label id="lblName"></label>
																		</td>
																		<td ><input type="text" style="width:150px;" maxlength="100" name="emailId" id="txtUID"/></td>
																	</tr>
																	<tr>
																		<td><label id="lblPassword"></label>
																		</td>
																		<td>
																			<input type="password" style="width: 150px;" maxlength="12" name="password" id="txtPWD"/>
																			&nbsp;&nbsp;<a href="javascript:void(0)" ><u><label id="btnforgetPwd" style="cursor:pointer ;"></label></u></a>
																		</td>
																	</tr>
																	<tr>
																		<td class="rowGap" colspan="2"></td>
																	</tr>
																	<tr>
																		<td></td>
																		<td>
																			<u:hButton name="btnSignIn" id="btnSignIn" value="Sign In" tabIndex="20" cssClass="redContinue" />
																		</td>
																		
																	</tr>
																	
																	<tr><td colspan="3">
																		<div class="socailBtnWrapper">
																			<div id="tdFBSignIn" style="display: none" class="soclaiButton soCommonBtn">
																				<a href="javascript:void(0)" title="Sign In With Facebook" id="btnFBSignIn" name="btnFBSignIn" class="buttonFB">
																					<span class="logo"></span>
																					<span><label id="lblSignInFacebook">Sign In With Facebook</label></span>
																				</a>
																			</div>
																			
																		<div id="tdLinkdinSignIn" style="display: none" class="soclaiButton soCommonBtn">
																				<a href="javascript:void(0)" title="Sign In With LinkedIn" id="btnLinkdinSignIn" name="btnLinkdinSignIn" class="buttonLN">
																					<span class="logo"></span>
																					<span><label id="lblSignInLinkedIn">Sign In With LinkedIn</label></span>
																				</a>
																		</div>
																		 	   		<div class="clear"></div>
																		</div>
																	</td></tr>
																	<tr>
																		<td class="rowGap" colspan="2"></td>
																	</tr>
																	<tr>
																		<td>
																		</td>
																		<td>
																			<u:hButton name="btnRegister" cssClass="redNormal" title="Click here for new Registration " id="btnRegisgter"/>
																		</td>
																	</tr>
																</tbody></table>
															<%@ include file="../common/includeFrameBottom.html"%>
														</table>
													</td>
												</tr>
												<tr>
														<td class="rowGap" colspan="3"></td>
													</tr>
													<tr>
														<td colspan="3">
															<div id="msgRegister" class="divfont"></div><br/>
															<div id="msgRegister1" class="divfont"></div>
														</td>
													</tr>
													<tr>
														<td class="rowGap" colspan="3"></td>
													</tr>
												</table>
												<input type="hidden" value="" name="hdnVersion"/><input type="hidden" value="Login" name="hdnMode"/>
												
												<%-- Handle social site related responses--%>
												<%@ include file='../../v2/customer/socialCustomerParam.jsp'%>	
											</form>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class='appLogo' colspan="2"></td>
							</tr>
						</table>
					</td>
				</tr>
				<c:import url="../../../../ext_html/cfooter.jsp" />	
		   </table>
		</div>
		<script type="text/javascript">
			var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
			var httpsEnabled = "<c:out value="${requestScope.httpsEnable}" escapeXml="false"/>";	
		</script>
		<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />		
		<script src="../js/v2/customer/socialCustomer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
		<script src="../js/v2/customer/customerLogin.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
		<div id='divLoadMsg' class="mainPageLoader">
			<%@ include file='../common/includeLoadingMsg.jsp' %>
		</div>
	</body>
	<%@ include file='../common/inline_Tracking.jsp' %>
</html>
