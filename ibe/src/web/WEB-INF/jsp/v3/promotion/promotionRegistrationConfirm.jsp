<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ include file="/WEB-INF/jsp/common/directives.jsp"%>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<title><fmt:message key="msg.Page.Title" /></title>
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<%@ include file='../common/interlinePgHD.jsp'%>
<script
	src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">	
var promoPage = "<c:out value="${sessionScope.promoPage}" escapeXml="false"/>"; 
var promoMessages = <c:out value="${requestScope.promoMessages}" escapeXml="false" />;
</script>	

</head>

<body>
<div id="divLoadBg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align='center' class="outerPageBackGround">
		<table style='width: 940px;' border='0' cellpadding='0'
			cellspacing='0' align='center' class='PageBackGround'>
			<!-- Top Banner -->
			<c:import url="../../../../ext_html/header.jsp" />
			<tr>
				<td colspan="2" class="mainbody">
				<div class="page-body" style="width: 100%;">
				</div>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td class="alignLeft paddingCalss pageName"><label id="promoHeading" class="hdFontColor fntBold">Congratulations!</label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr id ="trSuccees">
								<td class="alignLeft paddingCalss"><label id="lblPromoSuccessText"
									class=""></label>
									<br />
									<br />
									</td>
							</tr>
							<tr id ="trTerms">
							   <td class="alignLeft paddingCalss"><label id="lblPromoTermsText"
									class=""></label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
						</table>
					</td>
					<td id="doubleSeatSubcription"  width="300" valign="top">
						<img src="../images/v3/double-seat-en.jpg" width="300" alt="Double Seat"/>
					</td>
					<td id="flexiDateSubcription"  width="300" valign="top">
						<img src="../images/promo/flexi_date_subscribed.jpg" width="300" alt="Flexi Date"/>
					</td>	
					</tr></table>
				</td>			
				</tr>
				<%-- Bottom AA Logo --%>
			<tr>
				<td class='appLogo' colspan="2"></td>
			</tr>
			<c:import url="../../../../ext_html/cfooter.jsp" />
		</table>
		</td>
		
			<%--	<div >
				<table id="tblContinue" border='0' cellpadding='0' cellspacing='0' width="100%">
					<tr>
						<td class="alignLeft"></td>
						<td style="width: 10px;">&nbsp;</td>
						<td class="alignRight">
						<u:hButton name="btnContinue" id="btnContinue" value="Close"
							tabIndex="20" cssClass="redContinue" /></td>
					</tr>
					<tr>
						<td colspan="3" class='rowGap'></td>
					</tr>
					<tr>
						<td colspan="3" class="alignLeft">
							<label id="lblAdd" style="display: none">Please Keep in touch with E-Mails</label>
						</td>
					</tr>
				</table>
				</div>
				 --%>
	   </td>
		</tr>
</table>
</div>

<script type="text/javascript">
$("#promoHeading").text(promoMessages.promoHeading);
if(promoPage != null && promoPage == 'FD') {
	$("#doubleSeatSubcription").remove();
	$("#trSuccees").find("#lblPromoSuccessText").text(promoMessages.flexiSubcribed);
	$("#trTerms").find("#lblPromoTermsText").text(promoMessages.flexiTerms);
} else if(promoPage != null && promoPage == 'NS'){
	$("#flexiDateSubcription").remove();
	$("#trSuccees").find("#lblPromoSuccessText").text(promoMessages.nextSubcribed);
	$("#trTerms").find("#lblPromoTermsText").text(promoMessages.nextTerms);
}

$(document).ready(function() {
	$("#btnContinue").click(function() {
		window.close();
	});
});
</script>

<% request.getSession().setAttribute("promoPage", null); %>
<div id='divLoadMsg' class="mainPageLoader" style="display: none">
<%@ include file='../common/includeLoadingMsg.jsp'%>
</div>

</body>
</html>