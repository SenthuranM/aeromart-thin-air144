<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.isa.thinair.promotion.api.model.PromotionRequest"%>
<%@ page import="com.isa.thinair.promotion.api.to.constants.PromoRequestParam" %> 
<%@ page import="com.isa.thinair.promotion.api.to.constants.PromoTemplateParam" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp"%>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<title><fmt:message key="msg.Page.Title" /></title>
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<%@ include file='../common/interlinePgHD.jsp'%>
<script	src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v3/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
	var reqString = null;
</script>
</head>
<body>
<div id="divLoadBg" style="display: none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align='center' class="outerPageBackGround">
		<table style='width: 940px;' border='0' cellpadding='0'
			cellspacing='0' align='center' class='PageBackGround'>
			<!-- Top Banner -->
			<c:import url="../../../../ext_html/header.jsp" />
			<tr>
				<td colspan="2" class="mainbody">
				<div class="page-body" style="width: 100%;">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td class="alignLeft paddingCalss pageName"><label
							id="lblPromoHeading" class="hdFontColor fntBold">Double Seat</label></td>
					</tr>
					<tr>
						<td class='rowGap'></td>
					</tr>
					<tr>
						<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="promoNextSeatOnly"><tr>
							<td class="alignLeft paddingCalss"><label id="lblPromoText" >
							Enjoy double the space when travelling on Air Arabia and keep the seat next to you empty.<br/>
<br/><b>How it works:</b><br/>
<ol>
<li>You select Double Seat option for the flights where you would like to enjoy extra comfort.</li>
<li>You confirm your selection and pay for the service.</li>
<li>You receive an email confirming your subscription for the service.</li>
<li>Within 24h-12h before the flight departure and subject to seats availability, Air Arabia will do the seats allocation and send you a confirmation:
	<ul>
		<li> If there are seats available on the flight, Air Arabia will block a Double Seat for you and send you a new booking confirmation with the seat numbers.</li>
		<li> If there are no seats available on the flight, Air Arabia will refund<sup>(1)</sup> the amount of your subscription. No changes will be done on your reservation.</li>
	</ul>
</li>
</ol>
</label>
							</td>
							<td width="300" valign="top" >
								<img src="../images/v3/double-seat-en.jpg" width="300" alt="Double Seat"/>
							</td>
                            <%--<td width="300" valign="top" >
								<img src="../images/v3/flexi-date-en.jpg" width="300" alt="Flexi Date"/>
							</td>--%>
						</tr></table></td>
					</tr>
					<tr>
						<td class='rowGap'></td>
					</tr>
					<tr>
						<td>
						<table cellspacing="0" cellpadding="2" border="0">
							<tbody>
								<tr>
									<td width="140px" class="alignLeft"><label class="fntBold"
										id="lblPnrNo">PNR Number</label></td>
									<td width="5">:</td>
									<td width="250" class="alignLeft"><span id="pnr"></span></td>
								</tr>
								<tr>
									<td width="140" class="alignLeft"><label class="fntBold"
										id="lblContactName">Contact Person Name</label></td>
									<td width="5">:</td>
									<td class="alignLeft" width="250"><span id="contactName"></span>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td class='rowGap'></td>
					</tr>
					<tr>
						<td>
							<table cellspacing="0" cellpadding="2" border="0" width="96%">
							<tr>
								<td width="48%">
									<div class="outBoundFlight" style="float:none;margin:0 10px;width:90%">
										<label id="lblOutBound" class="hdFontColor fntBold">Outbound</label>
										<label class="hdFontColor fntBold">:</label>
										<label id="lbloutBoundSegs"></label>
										<div id="outBoundPax">
											<table cellspacing="0" cellpadding="2" border="0" align="center" width="99%" class="emptySeat seat-details">
											<thead>
											<tr>
												<td align="center" width="40%" class="gridHD bdBottom ">
													<label id="lblPrompPaxNameHD" class="gridHDFont fntBold">Name</label>
												</td>
												<td align="center" width="25%" class="gridHD bdBottom promoNextSeatOnly">
													<label id="lblEmptySeatHD" class="gridHDFont fntBold">Additional Seat</label>
												</td>
												<td align="center" width="15%" class="gridHD bdBottom promoNextSeatOnly">
													<label id="lblEmptySeatPriceHD" class="gridHDFont fntBold">Price &nbsp;(AED)</label>
												</td>
												<td  id="tdMoveSeat_0" align="center" width="20%" class="gridHD bdBottom move-seat" style="display: none">
													<label id="lblMoveSeat" class="gridHDFont fntBold">I accept to be moved <sup>(2)</sup></label>
												</td>
											</tr></thead>
											<tbody id="PaxBody_0"></tbody>
											</table>
											<div class='rowGap'></div>
										</div>
									</div>
								</td>
								<td width="48%">
									<div class="inBoundFlight" style="float:none;margin:0 10px;width:90%">
										<label id="lblInBound"  class="hdFontColor fntBold">Inbound</label>
										<label class="hdFontColor fntBold">:</label>
										<label id="lblinBoundSegs"></label>
										<div id="inBoundPax">
											<table cellspacing="0" cellpadding="2" border="0" align="center" width="99%" class="emptySeat seat-details">
											<thead>
											<tr>
												<td align="center" width="40%" class="gridHD bdBottom ">
													<label id="lblSeatPaxNameHD" class="gridHDFont fntBold">Name</label>
												</td>
												<td align="center" width="25%" class="gridHD bdBottom">
													<label id="lblEmptySeatHD" class="gridHDFont fntBold promoNextSeatOnly">Additional Seat</label>
												</td>
												<td align="center" width="15%" class="gridHD bdBottom promoNextSeatOnly">
													<label id="lblBaggagePriceHD" class="gridHDFont fntBold">Price &nbsp;(AED)</label>
												</td>
												<td id="tdMoveSeat_1" align="center" width="20%" class="gridHD bdBottom move-seat" style="display: none">
													<label id="lblMoveSeat" class="gridHDFont fntBold">I accept to be moved <sup>(2)</sup></label>
												</td>
											</tr></thead>
											<tbody id="PaxBody_1"></tbody>
											</table>
											<div class='rowGap'></div>
										</div>
									</div>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class='rowGap'></td>
					</tr>
					<tr class="flexiDateSpecific">
						<td>
							<table cellspacing="0" cellpadding="2" border="0" width="95%">
							<tr>
								<td>
									<div class="FlexiFlight_0" style="float:none;margin:0 auto">
										<label id="lblFlexiOutbound" class="hdFontColor fntBold">Outbound</label>
										<label class="hdFontColor fntBold">:</label>
										<label id="lblFlexiSegments_0"></label> <br />
										<label id="lblFlexiDeparture" class="fntBold">Departure Date</label> 
										<label id="lblFlexiDepartureDate_0"></label>
										<div id="boundFexi_0">
										   <table cellspacing="0" cellpadding="2" border="0" align="center" width="99%" class="emptySeat seat-details">
												<thead>
													<tr>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</td>
								
								<td>
									<div class="FlexiFlight_1" style="float:none;margin:0 auto">
										<label id="lblFlexiInbound" class="hdFontColor fntBold">Inbound</label> 
										<label class="hdFontColor fntBold">:</label>
										<label id="lblFlexiSegments_1"></label> <br />
										<label id="lblFlexiDeparture" class="fntBold">Departure Date</label>
										<label id="lblFlexiDepartureDate_1"></label>
										<div id="boundFexi_1">
										   <table cellspacing="0" cellpadding="2" border="0" align="center" width="99%" class="emptySeat seat-details">
												<thead>
													<tr>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</td>	
					</tr>
					
				</table>
				</td>
				</tr>
				
				<tr class="flexiDateSpecific">
					<td class='rowGap'></td>
				</tr>
				
				
				<tr class="flexiDateSpecific">
				 <td>
				<div id="divReward" style="display: none">
				<table border='0' cellpadding='0' cellspacing='0' width="40%">
					<tbody>
						<tr>
							<td class="alignLeft" width="50%"><label id="lblRewardText" class="fntBold">Entitled Reward will be </label></td>
							<td class="alignLeft" width="5%">:</td>
							<td class="alignLeft, fntBold" width="35%"><span id="reward"></span></td>
						</tr>
					</tbody>
				</table>
				</div>
				</td>
				</tr>
				
				<tr>
					<td class='rowGap'></td>
				</tr>
				<tr><td>
				<div id="trAccept">
				<table border='0' cellpadding='0' cellspacing='0' width="100%">
					<tbody>
						<tr>
							<td class="alignRight"><label id="lblAcceptMsg1">I
							accept the airline's</label>&nbsp; <a href="#" id="linkTerms"><u>
							<label class="hdFontColor" id="lblAcceptMsg2">Terms and
							Conditions</label></u> </a> <label id="lblAcceptMsg3"></label></td>
							<td width="2%"><label><input type="checkbox"
								title="Click here if you agree with terms and conditions"
								name="chkTerms" id="chkTerms" autocomplete="off"></input></label></td>
						</tr>
					</tbody>
				</table>
				</div>
				<div class='rowGap'></div>
				<div ><%-- Button --%>
				<table id="tblContinue" border='0' cellpadding='0' cellspacing='0' width="100%">
					<tr>
						<td class="alignLeft"></td>
						<td style="width: 10px;">&nbsp;</td>
						<td class="alignRight">
						<u:hButton name="btnContinue" id="btnContinue" value="Continue"
							tabIndex="20" cssClass="redContinue" /></td>
					</tr>
					<tr>
						<td colspan="3" class='rowGap'></td>
					</tr>
					<tr class="promoNextSeatOnly">
						<td colspan="3" class="alignLeft">
							<label id="termsOFPromo">(*): Non-refundable fee of AED xx when you subscribe for Double Seat service.</label>
						</td>
					</tr>
					<tr class="promoNextSeatOnly">
						<td class="alignLeft">
							<label id="termsForMoveSeat">(*): In case there are no seats available next to the seat number I pre-booked earlier, I accept to be moved to the first available row where I can be offered a double seat. </label>
						</td>
					</tr>
				</table>

				</div>
				<div class='rowGap'></div>
				</td>
			</tr>
			<%-- Bottom AA Logo --%>
			<tr>
				<td class='appLogo' colspan="2"></td>
			</tr>
			<c:import url="../../../../ext_html/cfooter.jsp" />
		</table>
		</div>
		</td>
	</tr>
</table></td></tr></table>
</div>
<%@ include file='../common/iBECommonParam.jsp'%>
<div id='divLoadMsg' class="mainPageLoader" style="display: none">
<%@ include file='../common/includeLoadingMsg.jsp'%>
</div>

<c:if test="${not empty(requestScope.promReqAttr)}">
	<script type="text/javascript">
	reqString = '<c:out value="${requestScope.promReqAttr}" escapeXml="false" />';
</script>
</c:if>
<form id="submitForm" method="post"></form>
	
	<form id="frmManageBooking" style="display: none;">
	  <td><span id="submitPnr"></span></td>
	  <td><span id="txtLastName"></span></td>
	  <td><span id="txtDateOfDep"></span></td>
	  <td><span id="groupPNR"></span></td>
	  <td><span id="airlineCode"></span></td>
	  <td><span id="marketingAirlineCode"></span></td>
	</form>
<script>
		// Promotion request params
		var PromoRequestParams = {};
		PromoRequestParams['FreeSeat.SeatsFree'] = "<%= PromoRequestParam.FreeSeat.SEATS_TO_FREE%>";
		PromoRequestParams['FlexiDate.DaysLowerBound'] = "<%= PromoRequestParam.FlexiDate.FLEXI_DATE_LOWER_BOUND %>";
		PromoRequestParams['FlexiDate.DaysUpperBound'] = "<%= PromoRequestParam.FlexiDate.FLEXI_DATE_UPPER_BOUND %>";
		// Promotion charges/rewards params
		var PromoRewardsAndCharges = {};
	    PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays'] = "<%= PromoTemplateParam.ChargeAndReward.FLEXI_DATE_DAYS_REWARD.getPrefix()%>" ;
			
</script>


</body>
<script	src="../js/v2/promotion/promotionRegistration.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<!-- <script	src="../js/v2/promotion/manageBooking.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>  -->
</html>