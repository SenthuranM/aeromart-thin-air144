<table width="auto" border="0" cellspacing="0" cellpadding="0" id="tableModSearch">
	<tr>
		<td colspan="3"></td>
	</tr>
	<%@ include file="../common/includeFrameTop.html" %>
				<table width='auto' border='0' cellpadding='0' cellspacing='0'>			
					<tr>
						<td  valign='top'>
							<div id="btnModSeach" class="cursorPointer fntBold">
								<span class="imgMod ">&nbsp;</span>
								<label id="lblModifysearch" class="cursorPointer">Modify Search</label>
							</div>
							<div style="width:660px;height:225px;display:none" id="divModSeach">
									<div class='rowGap'></div>
									<div class="alignLeft">
											<table width='auto' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td class="alignLeft" width="auto"> <label id="lblSelectaFlight" class="fntBold" > Select a Flight </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
													<td class="alignLeft" width="auto"> <input type="radio" id="roundTrip" name="tripDirection" value="R" /> &nbsp;<label id="lblRoundTrip">Round Trip</label> </td>
													<td class="alignLeft" width="auto"> <input type="radio" id="onewayTrip" name="tripDirection" value="O" /> &nbsp;<label id="lblOneWayTrip">One Way </label> </td>
													<td></td>
												</tr>
											</table>									
									</div>
									<div class='rowGap'></div>
									<div class="alignLeft">
										<table width='auto' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td class="alignLeft"  width="100"> <label id="lblBoarding" class="fntBold" style="white-space: nowrap;"> Boarding: </label> </td>
													<td class="alignLeft"  width ="230"> 
														<select id='selFromLoc' size='1'  NAME='selFromLoc' style="width:220px">
														</select>
													</td>
													<td  class="alignLeft"  width ="25"> <label id="lblTo" class="fntBold" style="white-space: nowrap;"> To:</label> </td>
													<td class="alignLeft" width ="230" >
														<select id='selToLoc' size='1'  NAME='selToLoc' style="width:220px">
														</select>
													</td>
													<td></td>
												</tr>
										</table>								
									</div>
									<div id="trCheckStopOver" style="display: none">
									<div class='rowGap'></div>
										<div style="width:100px;float:left;">
											<label id="lblTransitTime" class="fntBold" style="white-space: nowrap;">Transit Time</label>
										</div>
										<div class="stopOverTimeInner" style="width:350px;float:left;"></div>
										<div><input type="checkbox" id="chkStopOver" checked="checked"/> <label id="lblSearchWithTransit">Apply Transit Time</label></div>
										<div class="clear"></div>
									</div>
									<div class='rowGap'></div>						
									<div class="alignLeft" >
										<table width='auto' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td width ="100" class="alignLeft" > <label id="lblDepartOn" class="fntBold" style="white-space: nowrap;"> Depart on: </label> </td>
													<td width ="230" class="alignLeft" > 
														<input type="text" id="departureDateBox" name="departureDateBox" style="width: 125px;height: 15px" readonly="readonly"/>
													 </td>
												    <td width ="70" class="alignLeft" > <label id="lblReturnOn" class="fntBold" style="white-space: nowrap;"> Return on: </label> </td>
													<td width ="auto" class="alignLeft" > 
														<input type="text" id="returnDateBox" name="returnDateBox" style="width: 125px;height: 15px" readonly="readonly"/>
													 </td>
													 <td></td>											 
												</tr>
										</table>
									</div>									
								    <div class='rowGap'></div>
									<div class="alignLeft">
										<table width='auto' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td style='width:100px;' class="alignLeft" ><label id="lblAdult" class="fntBold"> Adults</label></td>
											<td class="alignLeft" style='width: 70px;'>
												<select  name='selAdults' id='selAdults' size='1' style='width: 45px;'>
												</select>
											</td>
											<td style='width: 60px;' class="alignLeft" ><label id="lblChild" class="fntBold"> Children</label></td>
											<td class="alignLeft" style='width: 70px;' >
												<select name='selChild' id='selChild' size='1' style='width: 45px;'>
												</select>
											</td>									
											<td style='width: 80px;' class="alignLeft" ><label id="lblInfant" class="fntBold">Infant(s)</label></td>
											<td class="alignLeft" style='width: 65px;' >
												<select name='selInfants' id='selInfants' size='1' style='width: 45px;'>
												</select>
											</td>
											<td style='width: 70px;' class="alignLeft" ><label id="lblCabinClass" class="fntBold">Cabin Class</label></td>
											<td class="alignLeft" >
												<select name='selCOS' id='selCOS' size='1' style='width: 110px;'>
												</select>
											</td>
										</tr>							
									</table>
								</div>
								<div class='rowGap'></div>
								<div class="alignLeft" id="divPromoCode">
									<table width='auto' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td width ="100" class="alignLeft" > <label id="lblPromoCode" class="fntBold" style="white-space: nowrap;">Promo Code </label> </td>
											<td width ="150" class="alignLeft" > 
												<input type="text" id="promoCode" name="promoCode" style="width: 100px;height: 15px" maxlength="40"/>
											 </td>

											<td style='width: 100px;' class="alignLeft" style="display: none;"><label id="lblPaxType" class="fntBold" style="display: none;">Pax Type</label></td>
											<td class="alignLeft" style="display: none;" >
												<select id='selPaxType' size='1' style='width:130px;' NAME='selPaxType' >
													<option value="A" selected="selected">All</option>
													<option value="L">Local</option>
													<option value="F">Foreign</option>
												</select>
											</td>
											 <td></td>											 
										</tr>
									</table>
								</div>
								<div class='rowGap'></div>
								<div>
									<table width='auto' border='0' cellpadding='0' cellspacing='0'>
										<tr>
								 			<td width ="190" class="alignLeft"  style="display: none;"> <label id="lblBin" class="fntBold" style="white-space: nowrap;">Bank Identification Number </label> </td>

											<td width ="auto" class="alignLeft" style="display: none;"> 
												<input type="text" id="bankIdNo" name="bankIdNo" style="width: 100px;height: 15px" maxlength="6"/>
											 </td>
											 <td></td>											 
										</tr>
									</table>
								</div>
						<div id="trClass1" class="rowGap">
						
						</div>						
							<div class="alignRight" id="tdSearch">
								<u:hButton name="btnSearch" id="btnSearch" value="Search Flights" tabIndex="24" 
									cssClass="whitebutton" title='Click here to Search for flights'/>
								<%--<input type='button' id='btnSearch' class='Button' 	value='Search Flights'
									title='Click here to Search for flights' NAME='btnSearch'> --%>
							</div>
					</div>			
					
					<input type="hidden" id="msDepartureDate"/> 
					<input type="hidden" id="msReturnDate"/>					
					</td>				  
				</tr>		
			</table>	
	<%@ include file="../common/includeFrameBottom.html" %>
</table>							
