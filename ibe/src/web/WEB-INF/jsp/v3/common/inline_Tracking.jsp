<!-- Added on 02/11/2015 -->

<script type="text/javascript">
	
	var carrier = '<c:out value="${sessionScope.sessionDataDTO.carrier}" escapeXml="false" />';
	var pixelSessionID = '<c:out value="${sessionScope.pixelTrackerSesID}" escapeXml="false" />';

	if(carrier == "G9" || carrier == "3O" || carrier == "E5" || carrier == "9P"){
		try{

			window.$.getScript(('https:' == document.location.protocol ? 'https://' : 'http://') + 'airarabia.api.sociaplus.com/partner.js');
			
		}catch(i){
			
		}
	}
	
	<c:if test = "${sessionScope.externalPartyID == '2b04253a-33aa-11e7-a919-92ebcb67fe33'}">
	
	if (carrier == "RM") {
	    // -------------------------for pixel tracking----------------------
	    var pnr = '<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />' || 0;
	    var currencyCode = '<c:out value="${sessionScope.sesPayCurrencyCode}" escapeXml="false" />' || 'AMD';
	    var totalAmount = '<c:out value="${sessionScope.sesPayAmount}" escapeXml="false" />' || 0;
	
		if(pnr != '' && pnr != 0){
	    	var pix = '<img src="https://pixel.aviasales.ru/armenia?CurrencySymbol=' + currencyCode + '&TotalPrice='+totalAmount+'&PNR='+pnr+'" border="0" width="1" height="1" />'
	    	document.getElementById("pixelTracker").innerHTML = pix;
		}
	    //-----------------------------------------------------------------
	}
	</c:if>
</script>