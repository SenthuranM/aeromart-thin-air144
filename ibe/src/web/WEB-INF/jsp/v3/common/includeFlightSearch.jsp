<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<form method="post" action="showFlightSearch.action" id="frmFlightSearch" name="fromFlightSearch">
	<table width='100%' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td colspan='3'>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class='pTopL'><img border='0' src="../images/spacer_no_cache.gif"/></td>
						<td class='pTopM' align='center'><label id="lblBook" class="fntWhite fntBold">  </label></td>
						<td class='pTopDM' align='center'>
						<c:if test='${requestScope.enableMultiCity == "true"}'>
							<a href='javascript:void(0)' onclick='UI_FlightSearch.flightSearchBtnOnClick(2)'>
								<label style="cursor: pointer;" id="lblMultiCitySearch" class="fntWhite fntBold">MultiCity Search</label>
							</a>
						</c:if>
						<c:if test='${requestScope.enableMultiCity != "true"}'>
							<img border='0' src="../images/spacer_no_cache.gif"/>
						</c:if>
						</td>
						<td class='pTopDR'><img border='0' src="../images/spacer_no_cache.gif"/></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class='pMidL'><img border='0' src="../images/spacer_no_cache.gif"/></td>
			<td class='pMidM' valign='top'>
				<div  class="searchCritira">
						<div class='rowGap'></div>

						<div class="alignLeft">
							<select id='selFromLoc' size='1'  NAME='searchParams.fromAirport' style="width:220px">
							</select>
						</div>
						<div class="alignLeft">
							<select id='selToLoc' size='1'  NAME='searchParams.toAirport' style="width:220px">
							</select>
						</div>
						<div id="trCheckStopOver" style="display: none">
							<div style="width:50;float:left;">
								<label id="lblTransitTime" class="fntBold" style="white-space: nowrap;">Transit Time</label>
							</div>
							<div style="width:50;float:right;text-align: right">
								<label id="lblSearchWithTransit">Search with Transit Time</label>
								<input type="checkbox" id="chkStopOver" checked="checked"/> 
							</div>
							<div class="clear"></div>
							<div class="stopOverTimeInner"></div>
						</div>
						<div class="alignLeft" >
							<label id="lblDepatureDate"></label>
						</div>
						<div class="alignLeft">
							<input type="text" id="departureDateBox" name="departureDateBox" style="width: 200px;height: 15px" readonly="readonly"/>
						</div>

						<div>
						<%--<select id='selDVariance' size='1' style='width: 57px;' NAME='searchParams.departureVariance' title=' No of days before or after departure date'>
							</select>						
							  <b><label>+/- </label> </b>
							 <label id="lblDays">days</label> --%>
						</div>
						<div class="alignLeft">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class="alignLeft"><label id="lblReturnDate"></label></td>
									<td class="alignRight" style="padding: 0 8px" valign="top">
										<label id="lblReturnTrip"></label>
										<input type="checkbox" id="chkReturnTrip" name="chkReturnTrip" checked="checked" class="noBorder">
									</td>
								</tr>
							</table>
						</div>
					<div class="alignLeft">
						<input type="text" id="returnDateBox" name="returnDateBox" style="width: 200px;height: 15px" readonly="readonly"/>
					</div>
					<div>
						<%-- <select id='selRVariance' size='1' style='width: 57px;' NAME='selRVariance' title='No of days before or after return date'>
						</select> 
						 <b><label>+/- </label> </b>
							<label id="lblDays">days</label> --%>
					</div>
					<div class="alignLeft">
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td style='width: 60px;'><label id="lblAdults" class="fntsmall"></label></td>
								<td style='width: 60px;'><label id="lblChildren" class="fntsmall"></label></td>
								<td style='width: 75px;'><label id="lblnfants" class="fntsmall"></label>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><label id="lblChildrenRange" class="fntSmallSm fntEnglish"></label></td>
								<td><label id="lblInfantRange" class="fntSmallSm fntEnglish"></label></td>
							</tr>													
							<tr>
								<td class="alignLeft" style='width: 50px;'>
									<select  name='searchParams.adultCount' id='selAdults' size='1' style='width: 45px;'>
									</select>
								</td>
								<td class="alignLeft" style='width: 50px;' >
									<select name='searchParams.childCount' id='selChild' size='1' style='width: 45px;'>
									</select>
								</td>
								<td class="alignLeft" style='width: 65px;' >
									<select name='searchParams.infantCount' id='selInfants' size='1' style='width: 45px;'>
									</select>
								</td>
							</tr>
						</table>
					</div>
			<div id="trClass1" class="rowGap">
				
			</div>
			<div id="trClass2" >
				<label id="lblClass">Cabin Class</label>
			</div>
			
			<div id="trClass3"  class="alignLeft">
					<select id='selCOS' size='1' style='width: 190px;' name='searchParams.classOfService'>
					</select>
			</div>
			<div id="trPaxCategoryRow" class="rowGap">
			
			</div>
			
			<div id='trPaxCategoryLabel' style="display: none" class="alignLeft">
				<label id="lblPaxCategory"></label>
			</div>
			<div id='trPaxCategoryList' style="display: none" class="alignLeft">

					<select id='selType' size='1'style='width: 190px;' NAME='selType'>
					</select>
			</div>
			<div id="divPromoCode">
				<div class="rowGap">
				</div>
				<div >
				<label id="lblPromoCode">Promo Code</label>
				</div>
				
				<div class="alignLeft">
					<input type="text" id="promoCode" name="searchParams.promoCode" style="width: 100px;height: 15px" maxlength="40" onkeyUp="return ValidateFlagText(this,event);" onkeyPress="return ValidateFlagText(this,event);"/>
				</div>
				<div class="rowGap">
				</div>
				<div style="display: none;">
				<label id="lblBin">Bank Identification Number</label>
				</div>
				
				<div class="alignLeft" style="display: none;">
					<input type="text" id="bankIdNo" name="searchParams.bankIdentificationNo" style="width: 100px;height: 15px" maxlength="6"/>
				</div>
			</div>		
			<div class="rowGap">
			</div>
			<div class="alignLeft">				
					<label id="lblCurrency"></label>
			</div>
			<div class="alignLeft">
					<select id='selCurrency' size='1' style='width: 190px;' NAME='searchParams.selectedCurrency'>
					</select>
			</div>
			
			<div class="rowGap">
			</div>
	
			<div id="trFareTypeRow" class="rowGap">
			</div>
			<div id='trFareTypeLabel' style="display: none" class="alignLeft">
				<label id="lblFareCategory"></label>
			</div>
			<div id='trFareTypeList' style="display: none" class="alignLeft">
				<select id='selFareType' size='1' style='width: 190px;' NAME='searchParams.fareType'>
				</select>
			</div> 
			
			
			<div class="alignRight" id="tdSearch">
				<input type='button' id='btnSearch' class='Button ui-state-default ui-corner-all' 	value='Search'
					title='Click here to Search for flights' NAME='btnSearch'>
			</div>
		</div>
		<input type="hidden" id="departureDate" name="searchParams.departureDate"/> 
		<input type="hidden" id="returnDate" name="searchParams.returnDate"/>
		<input type="hidden" id="resDepartureVariance" name="searchParams.departureVariance"/>
		<input type="hidden" id="resReturnVariance" name="searchParams.returnVariance"/>
		<input type="hidden" id="resFlexibleDates" name="searchParams.flexibleDates" value='0' />
		<input type="hidden" id="resReturnFlag" name="searchParams.returnFlag"/>
		<input type="hidden" id="resModifyPNR" name="pnr"/>
		<input type="hidden" id="resGroupPNR" name="groupPNR"/>	
		<input type="hidden" id="resIsModifySegment" name="modifySegment" />
		<input type="hidden" id="resModifySegments" name="oldAllSegments" />
		<input type="hidden" id="resModifySegmentRefNos" name="modifySegmentRefNos" />
		<input type="hidden" id="resOldFareID" 	    name="oldFareID" />
		<input type="hidden" id="resOldFareType" 	name="oldFareType" />
		<input type="hidden" id="resOldFareAmount" 	name="oldFareAmount" />
		<input type="hidden" id="resOldFareCarrierCode" 	name="oldFareCarrierCode" />
		<input type="hidden" id="resVersion" 	name="version" />	
		<input type="hidden" id="resModifingFlightInfo" 	name="modifingFlightInfo" />
		<input type="hidden" id="resContactInfoJson" name="contactInfoJson" />
		<input type="hidden" id="resPaxJson" name="paxJson">	
		<input type="hidden" id="hndFromAirport" name='searchParams.fromAirportName'/>	
		<input type="hidden" id="hndToAirport" name='searchParams.toAirportName'/>
		<input type="hidden" id="resFirstDeparture" 	name="searchParams.firstDeparture" />
		<input type="hidden" id="resLastArrival" 	name="searchParams.lastArrival" />
		<input type="hidden" id="hasInsurance" 	name="hasInsurance" />
		<input type="hidden" id="resQuoteOutboundFlexi" name="searchParams.quoteOutboundFlexi"/>
		<input type="hidden" id="resQuoteInboundFlexi" name="searchParams.quoteInboundFlexi"/>
		<input type="hidden" id="resOndQuoteFlexi" name="searchParams.ondQuoteFlexiStr"/>
		<input type="hidden" id="resFlexiSelected" name="searchParams.flexiSelected"/>
		<input type="hidden" id="resModFlexiSelected" name="searchParams.modifyFlexiSelected"/>
		<input type="hidden" id="resPaxType" 			name="searchParams.paxType" />
		<input type="hidden" name="fromSecure" id="fromSecure" value="true"/>
		
		<input type="hidden" 	id="resNonSecurePath" 		name="commonParams.nonSecurePath" />
		<input type="hidden" 	id="resSecurePath" 		name="commonParams.securePath" />
		<input type="hidden" 	id="resHomeURL" 		name="commonParams.homeURL" />
		<input type="hidden" 	id="resRegCustomer" 		name="commonParams.regCustomer"/>
		<input type="hidden" 	id="resLocale" 		name="commonParams.locale" />
		<input type="hidden" 	id="resSearchSystem" 		name="searchParams.searchSystem" value="<c:out value="${searchParams.searchSystem}" escapeXml="false"/>">	
		<input type="hidden" 	id="kioskAccessPoint" 		name="commonParams.kioskAccessPoint" value='<c:out value="${commonParams.kioskAccessPoint}" escapeXml="false"/>' />	
		<input type="hidden" 	id="skipPersianCalendarChange" 		name="commonParams.skipPersianCalendarChange" value='<c:out value="${commonParams.skipPersianCalendarChange}" escapeXml="false"/>' />
		<input type="hidden" 	id="resPointOfSale" 			name="searchParams.pointOfSale" value='<c:out value="${commonParams.pointOfSale}" escapeXml="false"/>'/>
		<input type="hidden" id="resFlexiAlerts" name="resFlexiAlerts" />
		<input type="hidden" id="resQuoteFlexi" name="searchParams.quoteFlexi" value='<c:out value="${searchParams.quoteFlexi}" escapeXml="false"/>'/>		
		<input type="hidden" id="hdnFromAirportSubStation" name='searchParams.fromAirportSubStation'/>
		<input type="hidden" id="hdnToAirportSubStation" name='searchParams.toAirportSubStation'/>
		
		<!-- //segment modification IBE-->
<input type="hidden" id="modifyByRoute" name="modifyByRoute"/>
<input type="hidden" id="modifyByDate" name="modifyByDate"/>
<input type="hidden" id="resAddGroundSegment" name="addGroundSegment"/>
<input type="hidden" id="totalSegmentCount" name="totalSegmentCount"/>
<input type="hidden" id="requoteFlightSearch" name="requoteFlightSearch"/>
<input type="hidden" id="cancelSegmentRequote" name="cancelSegmentRequote"/>
<input type="hidden" id="balanceQueryData" name="balanceQueryData"/>
<input type="hidden" id="availableCOSRankMap" name="availableCOSRankMap" />
<input type="hidden" id="nameChangeRequote" name="nameChangeRequote"/>
<input type="hidden" id="nameChangePaxData" name="nameChangePaxData"/>
		
		</td>
		<td class='pMidR'><img border='0' src="../images/spacer_no_cache.gif"/></td>
	</tr>
	<tr>
		<td colspan='3'>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="footerRow">
				<tr>
					<td class='pBotL'><img border='0' src="../images/spacer_no_cache.gif"/></td>
					<td class='pBotM'><img border='0' src="../images/spacer_no_cache.gif"/></td>
					<td class='pBotR'><img border='0' src="../images/spacer_no_cache.gif"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>										