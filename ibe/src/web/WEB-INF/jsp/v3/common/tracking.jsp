<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../../../jsp/common/topHolder.jsp' %>
<html>
	<head>	
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>				
		<script type="text/javascript">
			var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';		
		</script>
	</head>
	<body style="display: none">
		<span id='spnSpotAdd'></span>
		 
		 <%-- Common tracking data --%>
		 <c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />	
		 <script type="text/javascript">
		 		var carrier = '<c:out value="${sessionScope.sessionDataDTO.carrier}" escapeXml="false" />';	
		 </script>		
		 
		<%-- Confirmation page tracking code --%>
		<c:if test='${param.pageID == "CONFIRM" }'>			
					
			     <c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
					<script type="text/javascript">
					 	var google_conversion_id = 1028750054;
			            var google_conversion_language = "en";
			            var google_conversion_format = "2";
			            var google_conversion_color = "ffffff";
			            var google_conversion_label = "uQ21CJb9ugEQ5vXF6gM";
			            var google_conversion_value = 0;
			        </script>
			
			        <noscript>
				        <div style="display:inline;">
				        <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1028750054/?label=uQ21CJb9ugEQ5vXF6gM&amp;guid=ON&amp;script=0"/>
				        </div>
			        </noscript>	
			        <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>	       
			        
			        <!-- Shoogloo Start-->
			        <script src="https://trackin.affiliserve.com/142815/transaction.asp?APPID=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&MID=142815&PID=7503&status="></script>
						<noscript><img src="https://trackin.affiliserve.com/apptag.asp?APPID=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&MID=142815&PID=7503&status=" border="0" height="1" width="1"/>
					</noscript>
			        <!-- Shoogloo End-->
			        
			        <!-- Offer Conversion: Air Arabia tracking pixel -->
					<iframe src="https://vcommission.go2jump.org/SL1cS?adv_sub=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&amount=AMOUNT" scrolling="no" frameborder="0" width="1" height="1"></iframe>
					<!-- // End Offer Conversion -->
			        <!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/19/2013 -->
			        <SCRIPT SRC='https://bsrv.adohana.com/ohana/conversion.js?oh_price=<c:out value="${param.price}" escapeXml="false"/>&oh_id=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&id=268&r=1.0' type="text/javascript"></script>
			        
			        <!-- Added as per Airarabia request on 08/04/2013 -->
			        <img src="https://app.salecycle.com/Import/PixelCapture.aspx?c=17460&e=no-reply@airarabia.com&sfs=orderNumber^<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" />
			        <!-- Added as per Airarabia request on 08/04/2013 -->
				</c:if>
				
				<c:if test='${sessionScope.sessionDataDTO.carrier == "3O"}'>
					<script type="text/javascript">
					 	var google_conversion_id = 1007654460;
			            var google_conversion_language = "en";
			            var google_conversion_format = "2";
			            var google_conversion_color = "ffffff";
			            var google_conversion_label = "vEJpCNyo3gEQvKy-4AM";
			            var google_conversion_value = 0;
			        </script>
			
			        <noscript>
			        <div style="display:inline;">
			        <img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1007654460/?label=vEJpCNyo3gEQvKy-4AM&amp;guid=ON&amp;script=0"/>
			        </div>
			        </noscript>	
			        <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>	
			        
			        <!-- Cirteo Pixel tracking Start-->
			        <script type="text/javascript" src="../js/criteo_ld.js"></script>
					<script type="text/javascript">
					CRITEO.Load([{
					    pageType: 'confirmation',
					    'Transaction ID': '<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />',
					    'Product IDs': ['<c:out value="${param.productID}" escapeXml="false"/>'],
					    'Prices': ['<c:out value="${param.price}" escapeXml="false"/>'],
					    'Quantities': ['1', '1']
					}], [3966,'pac','','100',[[7715686, 7715687], [7715682, 7715683], [7715684, 7715685]],{'Transaction ID':['t',0],'Product IDs':['i',1],'Prices':['p',1],'Quantities':['q',1]}]);
					</script>
			         <!-- Cirteo Pixel tracking End-->
               
				</c:if>
				
				<script type="text/javascript">			
						// Take reference from main js 
						var UI_Confirm = UI_Top.holder().UI_Confirm;
	
						function confirmPageTrack() {
							
							if (UI_Confirm.tempResponse != null && UI_Confirm.tempResponse != "") {
								var response = UI_Confirm.tempResponse;
								var flightDepDetails = UI_Confirm.flightDepDetails;
								var flightRetDetails = UI_Confirm.flightRetDetails;
								
								if(response.mode == "CRE") {
									addSpotAdvert(response, flightDepDetails, flightRetDetails);
								}			
								
								if(response.flightRetDetails != null && response.flightRetDetails.length > 0){
									var way = "RT";
								} else {
									var way = "OW";
								}
								
								var path = flightDepDetails[0].segmentShortCode;
								var arr = new Array();
								arr = flightDepDetails[0].segmentShortCode.split("/");
								path = arr[0];
								var arr1 = new Array();
								arr1 = flightDepDetails[flightDepDetails.length - 1].segmentShortCode.split("/");
								path += arr1[1];
						
								var strAnalyticPage = "/funnel_G1/Confirmation/" + response.mode + "/" + response.flightDepDetails[0].carrierCode + "/" + path + "/" + way;
	
								try {
									if(strAnalyticEnable == "true") {			
										pageTracker._trackPageview(strAnalyticPage);
									}
								} catch(ex) {}	
								<!-- Postclick Tracking Tag Start -->
								if (response.carrierCode == "G9") {
									$('#spnSpotAdd').html('<img src="http://adserver.adtech.de/pcsale/3.0/1289/0/0/0/BeaconId=16167;rettype=img;subnid=1;SalesValue=1;custom1='+ UI_Confirm.strPNR +'" width="1" height="1">' + 
															'<img src="http://marketing.airarabia.ae/comms/action/conv/" width="1" height="1">');
								} else if(response.carrierCode == "3O") {
									$('#spnSpotAdd').html('<img src="http://adserver.adtech.de/pcsale/3.0/875/0/0/0/BeaconId=7497;rettype=img;subnid=1;SalesValue=1;;custom1='+ UI_Confirm.strPNR +'" width="1" height="1">' +
															'<img src="http://clic.reussissonsensemble.fr/registersale.asp?site=7545&mode=ppl&ltype=1&order='+ UI_Confirm.strPNR +'"  width="1" height="1"/>' + 
															'<img src="http://marketing.airarabia.ae/comms/action/conv/" width="1" height="1">');
								} else if(response.carrierCode == "E5") {
									$('#spnSpotAdd').html('<img src="http://marketing.airarabia.ae/comms/action/conv/" width="1" height="1">');
								}
								<!-- Postclick Tracking Tag End -->
							  }
						   }
	
	
							function addSpotAdvert(response, flightDepDetails, flightRetDetails) {
								
								var strPNR = response.bookingTO.PNR;
								if(response.mode == "CRE") {
									var prodid = strPNR;
								} else {
									var prodid = strPNR + "/" + response.strRandom;			
								}
								var axel = Math.random()+"";
								var a = axel * 10000000000000;		
								var arrData ;
								var strAirLine = response.flightDepDetails[0].carrierCode;		
								var airline = null;
								
								if(response.flightDepDetails != null && response.flightDepDetails.length > 0){
									var arr = new Array();
									arr = flightDepDetails[0].segmentCode.split("/");
									arrData = arr[0];
									var arr1 = new Array();
									arr1 = flightDepDetails[flightDepDetails.length - 1].segmentCode.split("/");
									arrData += "-" + arr1[1];
								}	 
								
								if(response.flightRetDetails != null && response.flightRetDetails.length > 0){
									var arr2 = new Array();
									arr2 = flightRetDetails[0].segmentCode.split("/");
									arrData += " - " + arr2[0];
									var arr3 = new Array();
									arr3 = flightRetDetails[flightRetDetails.length - 1].segmentCode.split("/");
									arrData += "-" + arr3[1];
								}
	
								if (strAirLine == '3O') {
									airline = "AirArabia - Morocco";
								} else if (strAirLine == 'GA' || strAirLine == 'AA' || strAirLine == 'G9'){
									airline = "AirArabia - Sharjah";
								} else{
									airline = strAirLine;
								}
								
								var amountsInGoogleAnlCurrency = new Array();
								amountsInGoogleAnlCurrency = response.amountsInGoogleAnlCurrency;		
								var faresAmount = amountsInGoogleAnlCurrency[0];
								var taxesAmount = amountsInGoogleAnlCurrency[1];
								var insrAmount = amountsInGoogleAnlCurrency[2];
								var transactionFeeAmount = amountsInGoogleAnlCurrency[3];
								var cost = amountsInGoogleAnlCurrency[4];
								var surchargesAmount = amountsInGoogleAnlCurrency[5];
								//var strTravelerCount = response.colAdultCollection.length + response.colChildCollection.length;
								var strPolicyNo = response.policyNo;
								var arrGoogleInfo = {};
								arrGoogleInfo = response.reservationTransItemInfo;
								
								if(strAnalyticEnable == 'true' && cost != null && faresAmount != null) {
									
									pageTracker._addTrans(
						    			prodid,                                     // Order ID
						    			airline, 	                        		// Affiliation
						    			cost,                   					// Total
						    			taxesAmount,            					// Tax
						    			"",                                   		// Shipping
						    			"",                                 		// City
						    			"",                               			// State
						    			""                                       	// Country
						  			);
									
									// FARE AMOUNT
									var fareSKU = strPNR + "/FARE";
						  			pageTracker._addItem(
						    			prodid,                                     // Order ID
						    			fareSKU,									// SKU
						    			arrData,    								// Product Name
						    			"Airfare",									// Category
								    	faresAmount,        						// Price    			
								    	"1"				                            // Quantity
								    	//strTravelerCount
						  			);
						  			
									// TAX
									var taxSKU = strPNR + "/TAX";
						  			pageTracker._addItem(
						    			prodid,                                     // Order ID
						    			taxSKU,										// SKU
						    			arrData,    								// Product Name
						    			"Tax",										// Category
						    			taxesAmount,        						// Price    			
								    	"1"				                            // Quantity
						  			); 
						  			
									// SURCHARGES
									var surchargesSKU = strPNR + "/SURCHARGES";
						  			pageTracker._addItem(
						    			prodid,                                     // Order ID
						    			surchargesSKU,								// SKU
						    			arrData,    								// Product Name
						    			"Surcharges",								// Category
						    			surchargesAmount,      						// Price    			
								    	"1"				                            // Quantity
						  			); 
	
						  			// INSURANCE
						  			var insSKU = strPNR + "/INSURANCE/" + strPolicyNo;
									if(insrAmount != '0'){
										pageTracker._addItem(
									    	prodid,                            		// Order ID
									    	insSKU,									// SKU
									    	arrData,								// Product Name	
									    	"Insurance",       		    			// Category
									    	insrAmount,						        // Price
									    	"1"                                     // Quantity
									  	);
									}
									
									// TRANSACTION FEES
									if(transactionFeeAmount != null || transactionFeeAmount != '0'){
										var transFeeSKU = strPNR + "/TRANSACTION_FEES";
										pageTracker._addItem(
									    	prodid,									// Order ID
									    	transFeeSKU,							// SKU
									    	arrData,			            		// Product Name
									    	"Transaction Fees",						// Category
									    	transactionFeeAmount,					// Price
									    	"1"                                     // Quantity
									  	);
									}
	
									for(x in arrGoogleInfo){
										var sku = strPNR + "/" + arrGoogleInfo[x][0];
										var segmentDetails = arrGoogleInfo[x][1];
										var productType = arrGoogleInfo[x][2];
										var amount = arrGoogleInfo[x][3];
										var quantity = arrGoogleInfo[x][4];
										
										if(amount != null || amount != ''){
											pageTracker._addItem(
									    			prodid,							// Order ID
									    			sku,							// SKU
									    			arrData, 						// Product Name
									    			productType,					// Category
									    			amount,							// Price
									    			quantity                        // Quantity
									  		);
										}
									}
									
									pageTracker._trackTrans();		
								}		
							}
							// Update Tracking Data
							$(document).ready(function() {	
								confirmPageTrack();		
							});					
							
									
				</script>
				<%-- Al waseet tracking data --%> 
				<script type='text/javascript'>
					<!--//<![CDATA[
					    var OA_p=location.protocol=='https:'?'https:':'http:';
					    var OA_r=Math.floor(Math.random()*999999);
					    document.write ("<" + "script language='JavaScript' ");
					    document.write ("type='text/javascript' src='"+OA_p);
					    document.write ("//ads.waseet.net/www/delivery/tjs.php");
					    document.write ("?trackerid=1&amp;r="+OA_r+"'><" + "\/script>");
				//]]>-->
				</script>
				<noscript>
					<div id='m3_tracker_1' style='position: absolute; left: 0px; top: 0px; visibility: hidden;'>
					<img src='http://ads.waseet.net/www/delivery/ti.php?trackerid=1&amp;UI_Confirm.strPNR=%%UI_CONFIRM.STRPNR_VALUE%%&amp;cb=%%RANDOM_NUMBER%%' width='0' height='0' alt='' />
					</div>
				</noscript>	

		</c:if>
		
		<%-- Availability search tracking data --%>
		<c:if test='${ param.pageID == "FLIGHT_SEARCH" }'>
			<script type="text/javascript">	
					var UI_AvailabilitySearch = UI_Top.holder().UI_AvailabilitySearch;
					function sendGoogleTrackingDataAS() {
							if (UI_AvailabilitySearch.isModifySegment == true) {
								var mode = "MOD";
							} else {
								var mode = "CRE";			
							}
							if(UI_AvailabilitySearch.returnFlag == "true") {
								var way = "RT";
							} else {
								var way = "OW";			
							}	
							var strAnalyticPage = "/funnel_G1/FlightSelect/" + mode + "/" + carrier + "/" + UI_AvailabilitySearch.fromAirport + UI_AvailabilitySearch.toAirport + "/" + way;
							if(strAnalyticEnable == "true") {			
								pageTracker._trackPageview(strAnalyticPage);
							}
					}
					$(document).ready(function() {	
						sendGoogleTrackingDataAS();
					});					
			</script>			
		</c:if>
		
		<%--TODO: Pax page tracking data --%>
		<c:if test='${ param.pageID == "PAX"}'>
		
		</c:if>
		<!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/25/2013 -->
        <!-- tag were modified as requiest of Ariarabia on  May 26, 2015 -->
         <c:if test='${sessionScope.sessionDataDTO.carrier == "G9" || sessionScope.sessionDataDTO.carrier == "3O" || sessionScope.sessionDataDTO.carrier == "E5" || sessionScope.sessionDataDTO.carrier == "9P"}'>
         	  <script type="text/javascript">
                (function() {
                    try {
                        var viz = document.createElement("script");
                        viz.type = "text/javascript";
                        viz.async = true;
                        viz.src = ("https:" == document.location.protocol ?"https://mea-tags.vizury.com" : "http://mea-tags.vizury.com")+ "/analyze/pixel.php?account_id=VIZVRM3429";

                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(viz, s);
                        viz.onload = function() {
                            try {
                                pixel.parse();
                            } catch (i) {
                            }
                        };
                        viz.onreadystatechange = function() {
                            if (viz.readyState == "complete" || viz.readyState == "loaded") {
                                try {
                                    pixel.parse();
                                } catch (i) {
                                }
                            }
                        };
                    } catch (i) {
                    }
                })();
            </script>
         </c:if>
        
		<c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
           <script type='text/javascript'>
				var _spapi = _spapi || [];
				_spapi.push(['_partner', 'airarabia']);
				(
				function()
				{
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'airarabia.api.sociaplus.com/partner.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
				}
				)();
			</script>
		
		</c:if>
		 <!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/25/2013 -->
	</body>
</html>
