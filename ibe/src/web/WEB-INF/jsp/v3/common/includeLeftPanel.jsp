<div id="RewardsPanel-0">
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td colspan='3' class="Page-heading">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="headerRow">
					<tr>
						<td class='headerBG hLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
						<td class='headerBG hCenter alignLeft' align='left'>
							<img border='0' src="../images/myAAlogo-white_no_cache.gif"/></label>
						</td>
						<td class='headerBG hRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
				</table>
			</td>
		</tr>
		<tr>
			<td class='mLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
			<td class='mCenter' valign='top'>
				<div  class="searchCritira">
					<div class='rowGap'></div>
					<div class="alignLeft hideMe-if-empty">
						<label id="lblMyAirarabDiscovBenifits">							
						</label>
					</div>
					<div class='rowGap'></div>
					<div class="alignLeft hideMe-if-empty">
						<b><label id="lblMyAirarabiheading1" class="hdFontColor textDec"></label></b>
					</div>
					<div class="alignLeft hideMe-if-empty" >
						<div id="lblMyAirarabiPara1" class="divfont ul-list">
						</div>
					</div>
					<div class="alignLeft hideMe-if-empty">
						<b><label id="lblMyAirarabiheading6" class="hdFontColor textDec"></label></b>
					</div>
					<div class="alignLeft hideMe-if-empty" >
						<label id="lblMyAirarabiPara6">							
						</label>
					</div>
					<div class="alignLeft">
						<label id="lblMyAirarabiPara1 hideMe-if-empty">
						</label>
					</div>
					<div class="alignLeft hideMe-if-empty">
						<b><label id="lblMyAirarabiheading3" class="hdFontColor textDec"></label></b>
					</div>
					<div class="alignLeft hideMe-if-empty">
						<label id="lblMyAirarabiPara3">							
						</label>
					</div>
					<div class="alignLeft hideMe-if-empty">
						<b><label id="lblMyAirarabiheading4" class="hdFontColor textDec"></label></b>
					</div>
					<div class="alignLeft hideMe-if-empty">
						<label id="lblMyAirarabiPara4" >							
						</label>
					</div>
				</div>
		</td>
		<td class='mRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
	</tr>
	<tr class="hideMe-if-empty">
		<td class='mLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
		<td class='mCenter' valign='top'>
			<div  class="left-panel-header" style="background: #666666">
				<div class="searchCritira alignLeft"><label id="lblMyAirarabiaPPolicy" class="divfont fntWhite fntBold"></label></div>	
			</div>
		</td>
		<td class='mRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
	</tr>
	<tr>
		<td class='mLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
		<td class='mCenter' valign='top'>
			<div  class="searchCritira hideMe-if-empty">
				<b><div class="divfont fntSmall" id="lblInfoAgree3"> </div></b>
			</div>
		</td>
		<td class='mRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
	</tr>
	<tr>
		<td colspan='3'>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="footerRow">
				<tr>
					<td class='pBotL'><img border='0' src="../images/spacer_no_cache.gif"/></td>
					<td class='pBotM'><img border='0' src="../images/spacer_no_cache.gif"/></td>
					<td class='pBotR'><img border='0' src="../images/spacer_no_cache.gif"/></td>
				</tr>
			</table>
		</td>
	</tr>
    </table>
    <div class="rowGap"></div>
</div>

<div id="RewardsPanel-1">
    <table width='100%' border='0' cellpadding='0' cellspacing='0'>
    <tr>
        <td colspan='3' class="Page-heading">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="headerRow">
                <tr>
                    <td class='headerBG hLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                    <td class='headerBG hCenter alignLeft' align='left'>
                        <img border='0' src="../images/air-rewards-w_no_cache.png"/>
                    </td>
                    <td class='headerBG hRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
            </table>
        </td>
    </tr>
    <tr>
        <td class='mLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
        <td class='mCenter' valign='top'>
            <div class="rowGap"></div>
            <div  class="searchCritira">
                <div class="alignLeft hideMe-if-empty">
                    <b><label id="lblMyAirarabiheading2" class="hdFontColor textDec"></label></b>
                </div>
                <div class="alignLeft hideMe-if-empty">
                    <label id="lblMyAirarabiPara2" >
                    </label>
                </div>
                <div class="alignLeft hideMe-if-empty">
                    <div id="lblMyAirarabiPara7" class="divfont ul-list">
                    </div>
                </div>
            </div>
        </td>
        <td class='mRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
    </tr>
    <tr>
        <td colspan='3'>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="footerRow">
                <tr>
                    <td class='pBotL'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                    <td class='pBotM'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                    <td class='pBotR'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                </tr>
            </table>
        </td>
    </tr>
    </table>
</div>
<div id="RewardsPanel-2">
    <table width='100%' border='0' cellpadding='0' cellspacing='0'>
    <tr><td colspan="3">
        <div class="rowGap"></div>
    </td></tr>
    <tr class="airarabiaMashReq" style="display:none">
        <td colspan='3' class="Page-heading">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="headerRow">
                <tr>
                    <td class='headerBG hLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                    <td class='headerBG hCenter alignLeft' align='left'>
                        <b><label id="lblMyAirarabiheading5" class="gridHDFont" style="font-size: 14px"></label></b>
                    </td>
                    <td class='headerBG hRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
            </table>
        </td>
    </tr>
    <tr class="airarabiaMashReq" >
        <td class='mLeft'><img border='0' src="../images/spacer_no_cache.gif"/></td>
        <td class='mCenter' valign='top'>
            <div class="rowGap"></div>
            <div  class="searchCritira">
                <label id="lblMyAirarabiPara5">
                </label>
            </div>
        </td>
        <td class='mRight'><img border='0' src="../images/spacer_no_cache.gif"/></td>
    </tr>
    <tr class="airarabiaMashReq" >
        <td colspan='3'>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="footerRow">
                <tr>
                    <td class='pBotL'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                    <td class='pBotM'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                    <td class='pBotR'><img border='0' src="../images/spacer_no_cache.gif"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>