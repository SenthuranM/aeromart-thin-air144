<script type="text/javascript">
	// Yandex Pixel Tracking
	
	try {
		<c:if test = "${sessionScope.sesPNRNO != null}">
		if (carrier == "RM") {
			var pnr = '<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />';
			var markerID = '<c:out value="${sessionScope.marker}" escapeXml="false" />';
			var mode = '<c:out value="${sessionScope.sessionDataDTO.ibeReservationPostDTO.mode}" escapeXml="false" />';
			var reservationMode = '<c:out value="${sessionScope.sesMode}" escapeXml="false" />';

			var onhold = (reservationMode == "onhold" ? true : false);
			var cancel = (reservationMode == "cancel" ? true : false);
			
			if (pnr != null && pnr != "" && markerID != null && markerID != ""){
				// This is to filter status based on yandex requirements
				var status = "";
				
				if (onhold) {
					status = "booking";
				} else if (cancel){
					status = "cancel";
				} else if (mode == "CREATE_RESERVATION"){
					status = "paid";
				}
				var pix = '<img src = https://avia.yandex.ru/order/pixel/' + markerID + '/' + status + '.gif>';
				document.getElementById("accelAeroIBETrackYandex").innerHTML = pix;
			}
		}
		</c:if>
	} catch(e){
		
	}
	
</script>