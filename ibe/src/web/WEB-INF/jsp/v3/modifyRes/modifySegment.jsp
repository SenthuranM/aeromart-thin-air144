<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<form id="frmCusModifySegment" method="post">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td valign="top" style="height: 200px;" class="alignLeft">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" >
				<tr>
					<td>
						<label id="lblHDModifySegment" class="fntBold hdFont hdFontColor paddingCalss">Modify Segment</label>
					</td>
				</tr>
				<tr>
					<td class="rowGrap">
				</td>
				</tr>
				<tr>
					<td valign="bottom">
						<label id="lblResNo" class="paddingCalss fntBold">Reservation Number :</label> <label class="fntBold fntLarge" id="spnPNR"></label>
					</td>
				</tr>
				 <tr>
						<td class="rowGap">
						</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">										
						    <tr>
								<td valign='top'>
								<table cellspacing="0" cellpadding="0" border="0" width="100%">		
								<%@ include file="../common/includeFrameTop.html"%>		
									<div><label id="lblModifingFlight"  class='fntBold hdFontColor'>Modifying Flight(s)</label></div>												
										<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
											<tr>
												<td rowspan="2" align='center' class='gridHD'><label id="lblOnd" class='gridHDFont fntBold'>Origin / Destination</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'>Departure</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'>Arrival</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'>Flight No</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'>Duration</label></td>												
											</tr> 
											<tr>            
												<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'>Time</label></td>
												<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'>Time</label></td>            
											</tr>
											<tr id="departueFlightMS">
												<td width='35%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
												<td width='15%' class='rowColor' align='center'><label id="departureDate"></label></td>
												<td width='8%' class='rowColor' align='center'><label id="departureTime"></label></td>
												<td width='15%' class='rowColor' align='center'><label id="arrivalDate"></label></td>
												<td width='8%' class='rowColor' align='center'><label id="arrivalTime"></label></td>
												<td width='10%' class='rowColor' align='center'><label id="flightNo"></label></td>
												<td width='9%' class='rowColor'  align='center'><label id="duration"></label></td>										
											</tr>               
										</table>
										<%@ include file="../common/includeFrameBottom.html"%>	
										</table>																																	
									</td>
								</tr>
								<tr>
									<td class="rowGrap"><br/></td>
								</tr>
								<tr>
								<td valign='top'>
								<table cellspacing="0" cellpadding="0" border="0" width="100%">		
								<%@ include file="../common/includeFrameTop.html"%>		
									<label id="lblModifingFlight"  class='fntBold hdFontColor'>Search here to modify the journey</label>											
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tbody>
												<tr>
													<td width="60" >
														<label id="lblFrom" >From</label>
													</td>
													<td>
														<select id="selFromLoc_dummy" name="selFromLoc_dummy" style="width:150px;" tabindex="0" ></select>
													</td>
													<td width="10"></td>
													<td width="80" >
														<label  id="lblDepartOn" >Departure Date</label>
													</td>
													<td width="110">
														<input name="departureDate_dummy" type="text" id="departureDate_dummy" class="formField" size="11" tabindex="2"/>
													</td>
													<td width="10"></td>
													<td width="60" class="formLabelSpacer" ><label id="lblCabin" >Cabin</label></td>
													<td>
														<select name="selCOS_dummy" id="selCOS_dummy" style="width:125px" tabindex="3">
														</select>
													</td>
													<td  width="10"></td>
												</tr>
												<tr>
												<td>
													<label id="lblTo">To</label>
												</td>
												<td>
													<select id="selToLoc_dummy" name="selToLoc_dummy" class="formField" style="width:150px;" tabindex="1" ></select>
												</td>
												<td colspan="8"></td>
												</tr>
											</tbody>
										</table>
										<%@ include file="../common/includeFrameBottom.html"%>	
										</table>																																	
									</td>
								</tr>
								<tr>
									<td class="rowGrap"><br/></td>
								</tr>
								<tr>
									<td>
									<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
										<td class="alignLeft">
											<u:hButton name="btnBackModifySegment" id="btnBackModifySegment" value="Back" tabIndex="23" title="Click here to go to the previous page"/></td>
										<td class="alignRight">
											<u:hButton name="btnSearchModifySegment" id="btnSearchModifySegment" value="Search flights" tabIndex="24" 
											cssClass="redContinue" title="Click here to search flights"/></td>
										</td>
									</tr></table>
									<%--<input type="button"  value="Back" class="Button" title="Click here to go to the previous page" id="btnBackModifySegment"> --%>
								</td>
							</tr>
							<tr style="display: none">
								<td>
									<label id="lblMsgUseSearchV3">Please use the Booking Form on the right to search for your new flight(s) and modify according to your new requirements.</label>
								</td>
							</tr>
						</table>
					</td>
				</tr>					
			</table>
		 </td>
	</tr>
  </table>
  
</form>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/modifyRes/modifySegment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</c:if>