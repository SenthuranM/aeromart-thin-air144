<div id="balanceSummary">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan='2' class='rowGap'></td>
	</tr>
	<%@ include file="../common/includeFrameTop.html"%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class='pnlBottomTall'  colspan='2' valign="top">
				<table cellspacing="0" cellpadding="2" border="0" width="100%" class="" id="modifyBal">										
						<tr>
							<td colspan="3" class="alignLeft paddingL5">
							<label id="lblPaymentDetails" class="fntBold fntRed">Payment Details</label></td>
						</tr>
						<tr>
							<td
								class="gridHDDark"><font>&nbsp;</font></td>
							<td align="center" width="25%"
								class="gridHDDark">
								<label id="lblModifingFlight" class="fntBold fntWhite">Modifying Flight(s)&nbsp;</label>
							</td>
							<td align="center" width="25%"
								class="gridHDDark">
								<label id="lblNewFlight" class="fntBold fntWhite">New Flight(s)&nbsp;</label></td>
						</tr>
						<tr id="balanceSummaryTemplate">
							<td
								class="SeperateorBGAlt rowDataHeight GridItems alignLeft">
								<label id="displayDesc"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight fntRed">
								<label id="displayOldCharges"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight fntRed">
								<label id="displayNewCharges" ></label>
							</td>
						</tr>
				</table>
			</td>
		</tr>
	</table>
	<%@ include file="../common/includeFrameBottom.html"%>
	</table>
</div>