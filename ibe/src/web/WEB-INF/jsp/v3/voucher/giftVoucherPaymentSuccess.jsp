

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="-1"/>
        <title>
            <fmt:message key="msg.Page.Title" />
        </title>
        <%@ include file='../common/iBECommonParam.jsp'%>
        <link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css"/>
        <script type="text/javascript" src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
        <%@ include file='../common/interlinePgHD.jsp' %>
        <script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
        <script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
        <script src="../js/v3/voucher/giftVoucherPostPay.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false'/>" type="text/javascript"></script>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <!-- Top Banner -->
            <c:import url="../../../../ext_html/header.jsp" />
            <tr>
                <td width="10%"></td>
                <td width="90%" align='left' class="outerPageBackGround">
                    <table style='width: 700px;' border='0' cellpadding='0'
                        cellspacing='0' align='center' class='PageBackGround'>
                        <tr>
                            <td class="alignLeft paddingCalss pageName"><label
                                id="lblMainhd" class="hdFontColor fntBold">Gift Vouchers</label></td>
                        </tr>
                        <tr>
                            <td><br/></td>
                        </tr>
                        <tr>
                            <td class="alignLeft">
                                <div class="" style="width: 100%;" class="stepsContainer">
                                    <c:import url="giftVoucherTabs.jsp" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="mainbody">
                                <table width='95%' border='0' cellpadding='2' cellspacing='0'>
                                    <tr>
                                        <td valign="top" class="spMsg">
                                            <img src="../images/n058_no_cache.gif" id="success" vspace="3" hspace="3" align="absmiddle" />
                                            <label id="lblVoucherMessage" class="fntBold">
                                            Payment has been successfully completed and Gift Voucher generated.
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><br/><br/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <%@ include file="../common/includeFrameTop.html"%>
                                    <table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                        <tr>
                                            <td colspan="2" class="alignLeft">
                                                <label id=lblCustomerDetailsHd class='fntBold hdFont hdFontColor'>Customer Details</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><br/></td>
                                        </tr>
                                        <tr>
                                            <td  width="50%" class="alignLeft" >
                                                <table width="100%">
                                                    <tr>
                                                        <td width ="30%">
                                                            <label id="lblCustomerName">Customer Name</label>
                                                        </td>
                                                        <td width ="50%"><label class="paddingL3">:</label>
                                                            <label id="customerName" class="fntBold"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="50%"></td>
                                        </tr>
                                        <tr>
                                            <td  width="50%" class="alignLeft" >
                                                <table width="100%">
                                                    <tr>
                                                        <td width ="30%">
                                                            <label id="lblEmail">Email</label>
                                                        </td>
                                                        <td width ="50%"><label class="paddingL3">:</label>
                                                            <label id="email" class="fntBold"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="50%"></td>
                                        </tr>
                                    </table>
                                    <%@ include file="../common/includeFrameBottom.html"%>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="rowGap"></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <%@ include file="../common/includeFrameTop.html"%>
                                    <table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                        <tr id="voucherDetails">
                                            <td>
                                                <table id="voucherListPurchased" width="100%" border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td colspan="5" class="gridHD defaultRowGap"><label id="lblVoucherConfirmationSummary" class="gridHDFont fntBold">Voucher(s) Summary
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" class="defaultRowGap GridTable "><label class="hdFontColor fntBold" id="lblVoucherId">Voucher ID</label></td>
                                                        <td width="20%" class="defaultRowGap GridTable "><label class="hdFontColor fntBold" id="lblAmountSrch">Amount</label></td>
                                                        <td width="20%" class="defaultRowGap GridTable "><label class="hdFontColor fntBold" id="lblValidFrom">Valid From</label></td>
                                                        <td width="20%" class="defaultRowGap GridTable "><label class="hdFontColor fntBold" id="lblValidTo">Valid To</label></td>
                                                        <td width="20%" class="defaultRowGap GridTable "><label class="hdFontColor fntBold" id="lblPrintEmail">Print/Email</label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <%@ include file="../common/includeFrameBottom.html"%>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="rowGap"></td>
                        </tr>
                       	<tr id="trButtonPannel">
			                <td class="alignLeft" colspan='2'>
			                    <div class="buttonset">
			                        <!-- Button -->
			                        <table border='0' cellpadding='0' cellspacing='0' width="100%">
			                            <tr>
			                                <td class="alignLeft">
			                                    <%--<input type="button" id="btnFinish"  value="Finish" class="Button"/>--%>
			                                    <u:hButton name="btnFinish" id="btnFinish" value="Finish"  cssClass="redNormal"/>
			                                </td>
			                                <td align="center" id="tdEmail">
			                                </td>
			                                <td class="alignRight">
			                                    <u:hButton name="btnPrintConf" id="btnPrintConf" value="Print"  cssClass="redNormal"/>
			                                    <u:hButton name="btnEmailConf" id="btnEmailConf" value="Email me"  cssClass="redNormal"/>
			                                </td>
			                            </tr>
			                        </table>
			                    </div>
			                </td>
			            </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <div id='divLoadMsg' class="mainPageLoader">
                        <%@ include file='../common/includeLoadingMsg.jsp'%>
                    </div>
                </td>
            </tr>
             <%-- Bottom AA Logo --%>
	        <tr>
	            <td class='appLogo' colspan="2"></td>
	        </tr>
        </table>
        <c:import url="../../../../ext_html/cfooter.jsp" />
    </body>
</html>

