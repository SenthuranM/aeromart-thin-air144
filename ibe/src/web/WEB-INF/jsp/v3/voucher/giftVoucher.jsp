

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><%@ include file="/WEB-INF/jsp/common/directives.jsp"%><%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html
    xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="-1" />
        <title>
            <fmt:message key="msg.Page.Title" />
        </title>
        <link rel='stylesheet' type='text/css' href='../css/table_layout_EN.css' />
        <link rel='stylesheet' type='text/css' href='../css/myStyleV3_no_cache.css' />
        <link rel="stylesheet" type="text/css" href="../css/remove-jquery_no_cache.css" />
        <link rel="stylesheet" type="text/css" href="../css/jquery.calendarview_no_cache.css?
            <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" />
            <link rel="stylesheet" type="text/css" href="../css/jquery.bt_no_cache.css?
                <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" />
                <script	src="../js/common/Common.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script><%@ include file='../common/interlinePgHD.jsp'%>
                <script	src="../js/common/autoDate.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/v2/jquery/jquery.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/v2/jquery/jquery-migrate.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/v2/jquery/jquery.ui.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script type="text/javascript"	src="../js/v2/jquery/i18n/grid.locale-en.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />">
                </script>
                <script	src="../js/v2/jquery/jquery.jqGrid.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/v2/common/jQuery.common.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/v2/common/jQuery.commonSystem.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/v2/isalibs/isa.jquery.decorator.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/v2/common/autoDate.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script	src="../js/common/validations.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript">
                </script>
                <script src="../js/v3/voucher/giftVoucher.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false'/>" type="text/javascript">
                </script>
                <script src="../js/v2/common/cardValidator.js?
                    <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false'/>" type="text/javascript">
                </script>
            </head>
            <body>
                <form id="frmVoucherPurchase">
                    <div id="divLoadBg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <!-- Top Banner -->
                            <c:import url="../../../../ext_html/header.jsp" />
                            <tr>
                                <td width="10%"></td>
                                <td width="60%" align='left' class="outerPageBackGround">
                                    <table style='width: 680px;' border='0' cellpadding='0'
                            cellspacing='0' align='center' class='PageBackGround'>
                                        <tr>
                                            <td colspan="2" class="mainbody">
                                                <div class="page-body" style="width: 100%;"></div>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td class="alignLeft paddingCalss pageName">
                                                            <label
                                                id="lblMainhd" class="hdFontColor fntBold">Gift Vouchers</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="setHeight" colspan="4"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="alignLeft">
                                                            <div class="" style="width: 100%;" class="stepsContainer">
                                                                <c:import url="giftVoucherTabs.jsp" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="700px" border="0" cellspacing="0" cellpadding="0"	id="tableModSearch">
                                                                <tr>
                                                                    <td colspan="3"></td>
                                                                </tr><%@ include file="../common/includeFrameTop.html"%>
                                                                <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                    <tr>
                                                                        <td valign='top'>
                                                                            <div style="width: 660px; height:auto" id="divModSeach">
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                        <tr>
                                                                                            <td class="alignLeft" width="auto">
                                                                                                <label
                                                                                    class="fntBold hdFontColor" id="lblGiftVoucherSearch">Gift Voucher Search</label>
                                                                                            </td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                     
												                                        <tr>
                                            											<td><br/></td>
                                        												</tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label id="lblAboutSearch" class="lineHeight fntEnglish">Search Gift Voucher Templates here.</label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0'
                                                                            cellspacing='0'>
                                                                                        <tr>
                                                                                            <td class="alignLeft" width="10%">
                                                                                                <label id="lblAmountSrch" class="fntBold" style="white-space: nowrap;">Amount</label>
                                                                                            </td>
                                                                                            <td class="alignLeft" width="25%">
                                                                                                <font>:</font>&nbsp;
                                                                                                <select id='amountSrch' size='1' name='amountSrch' style="width: 100px"></select>
                                                                                    &nbsp;
                                                                                                <label id="lblBsCurrency" class="fntBold" style="white-space: nowrap;"></label>
                                                                                            </td>
                                                                                            <td class="alignLeft" width="15%">
                                                                                                <label id="lblCurrencySrch" class="fntBold" style="white-space: nowrap;">Sell Currency</label>
                                                                                    &nbsp;
                                                                                                <label id="lblSelCurrency" class="fntBold" style="white-space: nowrap;"></label>
                                                                                            </td>
                                                                                            <td class="alignLeft" width="18%">
                                                                                                <font>:</font>&nbsp;
                                                                                                <select id='currencySrch' size='1' name='currencySrch' style="width: 100px"></select>
                                                                                            </td>
                                                                                            <td align="center" width="12%">
                                                                                                <label id="lblValiditySrch" class="fntBold" style="white-space: nowrap;">Validity</label>
                                                                                            </td>
                                                                                            <td class="alignLeft" width="18%">
                                                                                                <font>:</font>&nbsp;
                                                                                                <select id='validitySrch' size='1' name='validitySrch' style="width: 100px"></select>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                        <tr>
                                                                                            <td class="alignLeft" width="auto">
                                                                                                <label
                                                                                    class="fntBold hdFontColor" id="lblGiftVoucherDetailsHd">Gift Voucher Details</label>
                                                                                            </td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0'
                                                                            cellspacing='0'>
                                                                                        <tr>
                                                                                            <td width="100%">
                                                                                                <label id="lblAboutSelection" class="lineHeight fntEnglish">Select Gift Voucher according to above preference.</label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0'
                                                                            cellspacing='0'>
                                                                                        <tr>
                                                                                            <td class="alignLeft" width="100">
                                                                                                <label id="lblVoucherName" class="fntBold" style="white-space: nowrap;">Voucher Name</label>
                                                                                            </td>
                                                                                            <td class="alignLeft" width="240">
                                                                                                <font>:</font>&nbsp;
                                                                                                <select id='voucherName' size='1' name='voucherName' style="width: 220px"></select>
                                                                                            </td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0'
                                                                            cellspacing='0'>
                                                                                        <tr>
                                                                                            <td width="100" class="alignLeft">
                                                                                                <label id="lblAmount" class="fntBold">Amount</label>
                                                                                            </td>
                                                                                            <td class="alignLeft" style='width: 100'>
                                                                                                <font>:</font>&nbsp;
                                                                                    
                                                                                                <input type="text" id="amountInLocal" name="amountInLocal" style="width: 125px; height: 15px" />
                                                                                            </td>
                                                                                            <td width="10" class="alignLeft">
                                                                                                <label id="lblSpace" class="fntBold">&nbsp;&nbsp;</label>
                                                                                            </td>
                                                                                            <td style='width: 60px;' class="alignLeft">
                                                                                                <input type="text" id="currencyCode" name="currencyCode" style="width: 75px; height: 15px" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0'
                                                                            cellspacing='0'>
                                                                                        <tr>
                                                                                            <td width="100" class="alignLeft">
                                                                                                <label id="lblValidFrom" class="fntBold" style="white-space: nowrap;">Valid From</label>
                                                                                            </td>
                                                                                            <td width="230" class="alignLeft">
                                                                                                <font>:</font>&nbsp;
                                                                                                <input type="text" id="validFrom" name="validFrom" style="width: 125px; height: 15px" />
                                                                                            </td>
                                                                                            <td width="70" class="alignLeft">
                                                                                                <label id="lblValidTo" class="fntBold" style="white-space: nowrap;">Valid To</label>
                                                                                            </td>
                                                                                            <td width="auto" class="alignLeft">
                                                                                                <font>:</font>&nbsp;
                                                                                    
                                                                                                <input type="text" id="validTo" name="validTo" style="width: 125px; height: 15px" />
                                                                                            </td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0'
                                                                            cellspacing='0'>
                                                                                        <tr>
                                                                                            <td width="100" class="alignLeft">
                                                                                                <label id="lblQuantity" class="fntBold" style="white-space: nowrap;">Quantity</label>
                                                                                            </td>
                                                                                            <td width="230" class="alignLeft">
                                                                                                <font>:</font>&nbsp;
                                                                                                <input  type="number" name="quantity"  id="quantity" size="10" maxlength="5" min="1" style="width:50px" value="1"/>
                                                                                                
                                                                                            </td>                                                                                            
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class='rowGap'></div>
                                                                                <div class="alignRight" id="tdSearch">
    																				<u:hButton name="btnAdd" id="btnAdd" value="Add Another" tabIndex="10" 
																						cssClass="whitebutton" title='Add Another Voucher'/>
																					<u:hButton name="btnAddToBuy" id="btnAddToBuy" value="Add To Cart" tabIndex="10" 
																						cssClass="whitebutton" title='Add To Cart'/>			    																	
    																			</div>
                                                                            </table><%@ include file="../common/includeFrameBottom.html"%>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <br/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="700px" border="0" cellspacing="0" cellpadding="0"	id="tableModDetails">
                                                                            <tr>
                                                                                <td colspan="3"></td>
                                                                            </tr><%@ include file="../common/includeFrameTop.html"%>
                                                                            <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                <tr>
                                                                                    <td valign='top'>
                                                                                        <div style="width: 660px; height:auto" id="divModDetails">
                                                                                            <div class="alignLeft">
                                                                                                <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                                    <tr>
                                                                                                        <td class="alignLeft" width="auto">
                                                                                                            <label class="fntBold hdFontColor" id="lblCustomerDetailsHd">Customer Details</label>
                                                                                                        </td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div class='rowGap'></div>
                                                                                <div class="alignLeft">
                                                                                    <table width='auto' border='0' cellpadding='0'
                                                                            cellspacing='0'>
                                                                                        <tr>
                                                                                            <td width="100%">
                                                                                                <label id="lblAboutCustomer" class="lineHeight fntEnglish">Enter the Customer Details here.</label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                            <div class='rowGap'></div>
                                                                                            <div class="alignLeft">
                                                                                                <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                                    <tr>
                                                                                                        <td width="100" class="alignLeft">
                                                                                                            <label id="lblFirstName" class="fntBold">First Name</label>
                                                                                                        </td>
                                                                                                        <td class="alignLeft" style='width: 100;'>
                                                                                                            <font>:</font>&nbsp;
														                    
                                                                                                            <input type="text" id="paxFirstName" name="voucherDTO.paxFirstName" style="width: 125px; height: 15px" />
                                                                                                            <font class="mandatory">*</font>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div class='rowGap'></div>
                                                                                            <div class="alignLeft">
                                                                                                <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                                    <tr>
                                                                                                        <td width="100" class="alignLeft">
                                                                                                            <label id="lblLastName" class="fntBold">Last Name</label>
                                                                                                        </td>
                                                                                                        <td class="alignLeft" style='width: 100;'>
                                                                                                            <font>:</font>&nbsp;
														                    
                                                                                                            <input type="text" id="paxLastName" name="voucherDTO.paxLastName" style="width: 125px; height: 15px" />
                                                                                                            <font class="mandatory">*</font>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div class='rowGap'></div>
                                                                                            <div class="alignLeft">
                                                                                                <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                                    <tr>
                                                                                                        <td width="100" class="alignLeft">
                                                                                                            <label id="lblEmail" class="fntBold">Email</label>
                                                                                                        </td>
                                                                                                        <td class="alignLeft" style='width: 100;'>
                                                                                                            <font>:</font>&nbsp;
														                    
                                                                                                            <input type="text" id="email" name="voucherDTO.email" style="width: 125px; height: 15px" />
                                                                                                            <font class="mandatory">*</font>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div class='rowGap'></div>
                                                                                            <div class="alignLeft">
                                                                                                <table width='auto' border='0' cellpadding='0' cellspacing='0'>
                                                                                                    <tr>
                                                                                                        <td width="100" class="alignLeft">
                                                                                                            <label id="lblRemarks" class="fntBold">Remarks</label>
                                                                                                        </td>
                                                                                                        <td class="alignLeft" style='width: 100;'>
                                                                                                            <font>:</font>&nbsp;
														                    
                                                                                                            <textarea id="remarks" name="voucherDTO.remarks" style="width: 225px; height: 50px" maxlength="500"></textarea>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div class='rowGap'></div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table><%@ include file="../common/includeFrameBottom.html"%>
                                                                        </table>
                                                                        <div class='rowGap'></div>
                                                                        <div class="alignLeft" id="btnSet1">
                                                                            <table style="width: 700px" border='0' cellpadding='0'
                                                    cellspacing='0'>
                                                                                <tr>
                                                                                    <td class="alignLeft">
                                                                                        <u:hButton name="btnReset" id="btnReset" tabIndex="15" value="Reset" cssClass="blackStOver" />
                                                                                    </td>
                                                                                    <td class="alignRight">
                                                                                        <u:hButton name="btnNext" id="btnBuy" tabIndex="16" value="Checkout" cssClass="redContinue"/>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                       <%--Payment options --%>
                                                                        <div id="trPaymentMethods">
                                                                            <div id="divPayment">
                                                                                <div class='tabBody'><%@ include file="cardDetailVoucher.jsp"%>
                                                                                    <div class="alignLeft">
                                                                                        <table width="100%" border='0' cellpadding='0'
                                                    cellspacing='0'>
                                                                                            <tr id="IPNotification">
                                                                                                <td class="alignLeft" colspan="2">
                                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"><%@ include file="../common/includeFrameDivideBottom.html" %>
                                                                                                        <label id="lblNofication" class='fntBold'>Notification</label>
                                                                                                        <label class="paddingL3 fntBold">:</label>
                                                                                                        <label class="paddingL3" id="lblMsgRecordIP"></label>
                                                                                                        <br>
                                                                                                            <label id="lblYourIPIs" class='fntBold'>Your IP address is</label>
                                                                                                            <label class="paddingL3 fntBold">:</label>
                                                                                                            <label class="paddingL3" id='lblYourIP'></label><%@ include file="../common/includeFrameBottom.html"%>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class='rowGap'></div>
                                                                                    <div class="alignRight" id="paymentButtonset">
                                                                                        <table width="100%" border='0' cellpadding='0' cellspacing='0'>
                                                                                            <tr>
                                                                                                <td width="100" class="alignLeft">
                                                                                                    <u:hButton name="btnBack" id="btnBack" value="Previous Step" tabIndex="22" cssClass="backPrevious"/>
                                                                                                </td>
                                                                                                <td width="100" class="alignRight">
                                                                                                    <u:hButton name="btnPurchase" id="btnPurchase" value="Proceed to Payment" tabIndex="23" cssClass="redContinue"/>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class='rowGap'></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <c:import url="voucherSummary.jsp" />
                                            </tr><%-- Bottom AA Logo --%>
                                            <tr>
                                                <td class='appLogo' colspan="3"></td>
                                            </tr>
                                        </table>
                                        <c:import url="../../../../ext_html/cfooter.jsp" />
                                    </div>
                                    <div id='divLoadMsg' class="mainPageLoader"><%@ include file='../common/includeLoadingMsg.jsp'%>
                                    </div>
                                    <input type="hidden" id="hdnTnxFee" />
                                    <input type="hidden" id="amountInLocal" name="amountInLocal" />
                                    <input type="hidden" id="payCurrency" name="pgw.payCurrency" />
                                    <input type="hidden" id="providerName" name="pgw.providerName" />
                                    <input type="hidden" id="providerCode" name="pgw.providerCode" />
                                    <input type="hidden" id="description" name="pgw.description" />
                                    <input type="hidden" id="paymentGateway" name="pgw.paymentGateway" />
                                    <input type="hidden" id="payCurrencyAmount"	name="pgw.payCurrencyAmount" />
                                    <input type="hidden" id="brokerType" name="pgw.brokerType" />
                                    <input type="hidden" id="switchToExternalURL" name="pgw.switchToExternalURL" />
                                    <input type="hidden" id="hdnMode" name="hdnMode" />
                                    <input type="hidden" id="vcardType" name="voucherCCDetailsDTO.cardType" />
                                    <input type="hidden" id="vcardNumber" name="voucherCCDetailsDTO.cardNumber" />
                                    <input type="hidden" id="vcardExpiry" name="voucherCCDetailsDTO.cardExpiry" />
                                    <input type="hidden" id="vcardCVV" name="voucherCCDetailsDTO.cardCVV" />
                                    <input type="hidden" id="vcardHolderName" name="voucherCCDetailsDTO.cardHolderName" />
                                    <input type="hidden" id="vpaymentMethod" name="voucherCCDetailsDTO.paymentMethod" />                               
                                    <input type="hidden" id="vipgId" name="voucherCCDetailsDTO.ipgId" />
                                    <input type="hidden" id="camountInLocal" name="voucherDTO.amountInLocal" />
                                    <input type="hidden" id="templateId"  name="voucherDTO.templateId" />
                                    <input type="hidden" id="vcurrencyCode"  name="voucherDTO.currencyCode" />
                                    <input type="hidden" id="vvalidFrom" name="voucherDTO.validFrom" />
                                    <input type="hidden" id="vvalidTo" name="voucherDTO.validTo" />
                                    <input type="hidden" id="vList" name="selectedList" />
                                    
                                </form>
                            </body>
                        </html>

