

<td width="30%">
    <div class="bookingSummary" id="voucherListSummery">
        <div class="Page-heading">
            <table cellspacing="0" cellpadding="0" border="0" width="80%">
                <tr>
                    <td colspan="3">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" class="headerRow">
                            <tr>
                                <td class="headerBG hLeft"></td>
                                <td class="headerBG hCenter alignLeft">
                                    <label class="fntBold gridHDFont" id="lblVoucherListSummary">Vouchers To Buy
                                    </label>
                                </td>
                                <td class="headerBG hRight"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="mLeft"></td>
                    <td class="mCenter">
                        <div class="pane-body">
                            <table id="vDetails" cellspacing="0" cellpadding="0" border="0" width="100%">                                                                   
                                        </table>
                                    </div>
                                    </td>
                                </tr>
                            </table>                            
                        </div>
           
    </div>
    <div class="bookingSummary" id="voucherSummery">
        <div class="Page-heading">
            <table cellspacing="0" cellpadding="0" border="0" width="80%">
                <tr>
                    <td colspan="3">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" class="headerRow">
                            <tr>
                                <td class="headerBG hLeft"></td>
                                <td class="headerBG hCenter alignLeft">
                                    <label class="fntBold gridHDFont" id="lblVoucherSummary">Summary of Purchase
                                    </label>
                                </td>
                                <td class="headerBG hRight"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="mLeft"></td>
                    <td class="mCenter">
                        <div class="pane-body">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td class="pnlTop"></td>
                                </tr>
                                <tr>
                                    <td style="height: 52px;" class="pnlBottom add-padding alignLeft">
                                        <table id="paymentPgSummary" cellspacing="4" cellpadding="2" border="0">
                                                                                 
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="display: block;" id="outFlightsDiv">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td style="height:10px" class="pnlBottom colourGreavy alignLeft">
                                                        <table cellspacing="1" cellpadding="1" border="0" width="100%">
                                                            <tr>
                                                                <td>&nbsp;
                                                                    <label class="fntBold gridHDFont" id="lblCustomer Summary">Customer Summary</label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pnlTop"></td>
                                                </tr>
                                                <tr>
                                                    <td class="rowGap"></td>
                                                </tr>
                                                <tr>
                                                    <td class="pnlBottom add-padding alignLeft">
                                                        <table cellspacing="1" cellpadding="1" border="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <label class="fntBold" id="lblCusName">Customer Name</label>
                                                                    <label id="cusName"></label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="fntBold" id="lblCusEmail">Customer Email</label>
                                                                    <label id="cusEmail"></label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="summarySperator"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="fntBold add-padding" id="totalOutDiv">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="60%">
                                                            <label class="fntBold" id="lblVoucherPrice">Voucher Price</label>
                                                        </td>
                                                        <td width="25%" class="alignRight">
                                                            <label id="baseAmount" class="fntDefault fntBold" name="baseAmount"></label>
                                                        </td>
                                                        <td width="15%" align="center">
                                                            <label class="fntBold" id="baseCurrency"></label>
                                                        </td>
                                                    </tr>
                                                    <tr id="ccTaxVoucher">
                                                        <td width="60%">
                                                            <label class="fntBold" id="lblAdministrationFee">Administration Fee</label>
                                                        </td>
                                                        <td width="25%" class="alignRight">
                                                            <label id="ccTaxAmount" class="fntDefault fntBold" name="ccTaxAmount"></label>
                                                        </td>
                                                        <td width="15%" align="center">
                                                            <label class="fntBold" id="ccTaxCurrency"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rowGap"></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" class="footerRow">
                                            <tr>
                                                <td class="headerBG bLeft"></td>
                                                <td class="headerBG bCenter">
                                                    <div style="display: block;" class="fntBold" id="totalPriceDiv">
                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                            <tr>
                                                                <td width="60%">
                                                                    <label class="fntBold gridHDFont" id="lblTotalPrice">Total Price</label>
                                                                </td>
                                                                <td width="25%" class="alignRight">
                                                                    <label id="baseTotalAmount" class="gridHDFont fntBold" name="valTotal"></label>
                                                                </td>
                                                                <td width="15%" class="alignRight">
                                                                    <label class="fntBold gridHDFont" id="baseTotalCurrency" name="valBaseCurrCode"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td class="headerBG bRight"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</td>

