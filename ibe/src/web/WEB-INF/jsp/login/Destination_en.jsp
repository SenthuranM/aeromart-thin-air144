<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %> 
	<fmt:setLocale value="en" scope="session"/> 
<%@ include file='../common/pageHD.jsp'%>
<head>   
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
   	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/StyleKsk_no_cache.css'>		
  </head>
  <body>
  <div id="content">
    <div id="toolBar">
      <!-- START: Page Header -->
	  <h1>
                	 &raquo; Destinations  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           </h1>
	  <!-- END: Page Header -->
		<!-- Start: Page Tools -->
      <div id="tools"><a onclick="window.print()" style="cursor:hand" id="tools-print"><img src="../images/spacer_no_cache.gif" border="0" alt="Print this page" title="Print this page"/></a></div>
		<!-- End: Page Tools -->
    </div>
    <!-- START: Content Body -->
    <div id="content-body">
													<div class="postimagecenter"><img src="../images/map_eng_no_cache.jpg" alt="Air Arabia destinations" /></div>
	<p>Our fast-growing network includes:</p>
	<ul>
	<li>Aleppo (Syria) </li>
	<li>Alexandria (Egypt) </li>
	<li>Almaty (Kazakhstan)</li>
	<li>Amman (Jordan)</li>
	<li>Assiut (Egypt) </li>
	<li>Astana (Kazakhstan)</li>
	<li>Bahrain </li>
	<li>Beirut (Lebanon) </li>
	<li>Colombo (Sri Lanka) </li>
	<li>Damascus (Syria) </li>
	<li>Dammam (Saudi Arabia) </li>
	<li>Doha (Qatar) </li>
	<li>Istanbul (Turkey) </li>
	<li>Jaipur (India) </li>
	<li>Jeddah (Saudi Arabia) </li>
	<li>Kabul (Afghanistan) </li>
	<li>Khartoum (Sudan) </li>
	<li>Kochi (India) </li>
	<li>Kuwait </li>
	<li>Luxor (Egypt) </li>
	<li>Mumbai (India) </li>
	<li>Nagpur (India)</li>
	<li>Muscat (Oman) </li>
	<li>Riyadh (Saudi Arabia) </li>
	<li>Sana&#8217;a (Yemen) </li>
	<li>Sharjah (U.A.E) </li>
	<li>Sharm El Sheikh (Egypt)</li>
	<li>Tehran (Iran)</li>
	</ul>
	<p>&nbsp;</p>
				<div id="page-links"></div>
					
							</div>
			</div>
     <!-- END: Content Body -->
  </div>
  <!-- START: Content Body -->
  <div id="separator" style="clear:both;">&nbsp;</div>
</div>
</body>
<script type="text/javascript">	
	<!--
		opener.childfocus(this);
			
	//-->
	</script>