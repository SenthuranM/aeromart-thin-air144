﻿<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %> 
	<fmt:setLocale value="ar" scope="session"/> 
<%@ include file='../common/pageHD.jsp'%>
<head>   
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
   	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/StyleKsk_ar_no_cache.css'>		
  </head>
  <body>
  <div id="content">
    <div id="toolBar">
      <!-- START: Page Header -->
	   <h1>
                	 &raquo; وجهاتنا  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;            </h1>
	  <!-- END: Page Header -->
		<!-- Start: Page Tools -->
      <div id="tools"><a onclick="window.print()" style="cursor:hand" id="tools-print"><img src="../images/spacer_no_cache.gif" border="0" alt="Print this page" title="Print this page"/></a></div>
		<!-- End: Page Tools -->
    </div>
    <!-- START: Content Body -->
    <div id="content-body">
													<div class="postimagecenter"><img src="../images/map_ar_no_cache.jpg" alt="Air Arabia destinations" /></div>
	<p>معلومات عن  محطات رحلاتنا:- تحلق العربية للطيران إلى 27 محطة في17 بلدا عبر منطقة الشرق الأوسط وشمال أفريقيا. تشمل شبكة محطاتنا سريعة التوسع على:  </p>
	<ul>
	<li>الأقصر (مصر)</li>
	<li>الإسكندريه (مصر) </li>
	<li>استانا (كازاخستان)</li>
	<li>الماتي(كازاخستان)</li>
	<li>أسيوط (مصر)</li>
	<li>	إسطنبول (تركيّة )</li>
	<li>البحرين</li>
	<li>بيروت (لبنان)</li>
	<li>جده (المملكه العربيه السعوديه)</li>
	<li>	جايبور (الهند)</li>
	<li>حلب (سوريا)</li>
	<li>الخرطوم (السودان)</li>
	<li>الدمام (المملكه العربيه السعوديه)</li>
	<li>دمشق (سوريا)</li>
	<li>الدوحة (قطر)</li>
	<li>الرياض (المملكه العربيه السعوديه)</li>
	<li>الشارقة(أ.ع.م)</li>
	<li>شرم الشيخ (مصر)</li>
	<li>صنعاء (اليمن)</li>
	<li>عمّان (الاردن)</li>
	<li>كولومبو (سريلانكا)</li>
	<li>كابول (افغانستان)</li>
	<li>الكويت</li>
	<li>كوتشي (الهند )</li>
	<li>مسقط (سلطنة عمان</li>
	<li>مومباي (الهند)</li>
	<li>ناجبور (الهند)</li>
	</ul>
	<p>&nbsp;</p>
				<div id="page-links"></div>
					
							</div>
			</div>
     <!-- END: Content Body -->
  </div>
  <!-- START: Content Body -->
  <div id="separator" style="clear:both;">&nbsp;</div>
</div>
</body>
<script type="text/javascript">	
	<!--
		opener.childfocus(this);
			
	//-->
	</script>