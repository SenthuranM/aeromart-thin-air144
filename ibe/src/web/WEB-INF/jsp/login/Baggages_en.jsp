<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %> 
	<fmt:setLocale value="en" scope="session"/> 
<%@ include file='../common/pageHD.jsp'%>
<head>   
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
   	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/StyleKsk_no_cache.css'>	
  </head>
  <body>
  <div id="content">
    <div id="toolBar">
      <!-- START: Page Header -->
	  <h1>           	 &raquo; Baggage Allowance              </h1>
	  <!-- END: Page Header -->
		<!-- Start: Page Tools -->
      <div id="tools"><a onclick="window.print()" style="cursor:hand" id="tools-print"><img src="../images/spacer_no_cache.gif" border="0" alt="Print this page" title="Print this page"/></a></div>
		<!-- End: Page Tools -->
    </div>
    <!-- START: Content Body -->
    <div id="content-body">
	
	<p>At <strong>Air Arabia</strong>, our aim is to make your travel experience as easy and convenient as possible and we?d like to ensure that you are aware of our baggage policy when you plan your travel. It is important that you are familiar with the allowance that you are entitled to, so that you avoid any inconvenience at the airport. </p>
	<p>Our Baggage Allowance policy is outlined below:</p>
	<p><strong>Checked Baggage Allowance</strong> is the amount of free baggage you are permitted to check-in to be loaded into the aircraft hold.</p>
	<p><strong>Cabin Baggage Allowance</strong> is the amount of cabin baggage you are permitted to carry onto the aircraft. </p>
	<p>You are entitled to the following allowances as based on your destination:</p>

	<table cellspacing="0" cellpadding="0">
	<tbody>
	<tr>
	<td valign="top" width="243"><font class='tablefont'><strong>Almaty &amp; Astana (Kazakastan)</strong></font></td>
	<td valign="top" width="330"><font class='tablefont'>
	<p>Free Checked Baggage allowance 20kgs </p>
	<p>Cabin Baggage <strong>1</strong> piece with a maximum weight not exceeding 6kgs. The total dimensions of which should not exceed 55 x 40 x 20cm.</p>
	</font></td>
	</tr>
	<tr>
	<td valign="top" width="243"><font class='tablefont'><strong>Within GCC (except Jeddah) </strong></font></td>
	<td valign="top" width="330"><font class='tablefont'>
	<p>Free Checked Baggage allowance 25kgs </p>
	<p>Cabin Baggage <strong>1 </strong>piece with a total maximum weight not exceeding 7kgs. The total dimensions of which should not exceed 55 x 40 x 20cm </p>
	</font></td>
	</tr>
	<tr>
	<td valign="top" width="243"><font class='tablefont'><strong>All other flights </strong></font></td>
	<td valign="top" width="330"><font class='tablefont'>
	<p>Free Checked Baggage allowance 30kgs </p>
	<p>Cabin Baggage <strong>1 </strong>piece with a maximum weight not exceeding 6kgs. The total dimensions of which should not exceed 55 x 40 x 20cm </p>
	</font></td>
	</tr>
	</tbody>
	</table>
	<p>The maximum weight permitted per individual piece of baggage is <strong>32kgs</strong> with total dimensions of <strong>160cms</strong> (W+D+L). No single piece of baggage may exceed this allowance. </p>
	<p>Only <strong>one box</strong> is permitted as part of free baggage allowance. The box must be the original manufacture?s box containing the original item.</p>
	<p>All items weighing 32kgs or more and with dimensions of 160cms or more can only be sent as cargo. Air Arabia does not offer cargo facilities for outsized baggage.</p>
	<p>In addition to the above free allowances you may carry the following items as cabin baggage; a coat, a handbag/clutch bag, umbrella, small items of tax free goods and a laptop computer. </p>
	<p>Excess Baggage will be subject to the following charges:</p>
		<table>
			<tr>
			<td><font class='tablefont'><b>Applicable on flights</b></font></td>		
			<td><font class='tablefont'><b>Flights to and from Sharjah International Airport</b></font></td>
			<td><font class='tablefont'><b>All Connecting Flights</b></font></td>
			</tr>
			<tr>
			<td><font class='tablefont'>Within GCC (except Jeddah)</font></td>		
			<td><font class='tablefont'>AED 10 per kg</font></td>
			<td><font class='tablefont'>AED 10 per kg</font></td>
			</tr>
			<tr>
			<td><font class='tablefont'>India & Colombo</font></td>		
			<td><font class='tablefont'>AED 15 per kg</font></td>
			<td><font class='tablefont'>AED 15 per kg</font></td>
			</tr>
			<tr>
			<td><font class='tablefont'>All others (including Jeddah)</font></td>		
			<td><font class='tablefont'>AED 20 per kg</font></td>
			<td><font class='tablefont'>AED 15 per kg</font></td>
			</tr>
		</table>




	<p>Please read our <a href="www.aiarabia.com/travel-regulation.html">Travel Regulations</a> for further detailed information.</p>
	
				<div id="page-links"></div>
					
							</div>
			</div>
     <!-- END: Content Body -->
  </div>
  <!-- START: Content Body -->
  <div id="separator" style="clear:both;">&nbsp;</div>
</div>
</body>
<script type="text/javascript">	
	<!--
		opener.childfocus(this);
			
	//-->
	</script>
								