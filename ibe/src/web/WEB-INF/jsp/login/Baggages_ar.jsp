﻿<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %> 
	<fmt:setLocale value="ar" scope="session"/> 
<%@ include file='../common/pageHD.jsp'%>
<head>   
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
   	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/StyleKsk_ar_no_cache.css'>	
  </head>
  <body>
  <div id="content">
    <div id="toolBar">
      <!-- START: Page Header -->
	  <h1>
                	 &raquo;  الوزن المسموح             </h1>
	  <!-- END: Page Header -->
		<!-- Start: Page Tools -->
      <div id="tools"><a onclick="window.print()" style="cursor:hand" id="tools-print"><img src="../images/spacer_no_cache.gif" border="0" alt="Print this page" title="Print this page"/></a></div>
		<!-- End: Page Tools -->
    </div>
    <!-- START: Content Body -->
    <div id="content-body">
													<p>عند العربية للطيران ، هدفنا هو جعل خبرة سفرك سهلة و مريحة بقدر الإمكان و نحن نريد التأكيد بأنك على علم بقانون الوزن عندما تخطط للسفر. و هو من المهم أن تكون على معرفة مسبقة بالوزن المسموح به حتى لا تعرض نفسك للاحراج فى المطار.</p>
	
	<table cellspacing="0" cellpadding="0">
	<tr>
	<td width="243" valign="top"><font class='tablefont'>
	<p><b>الأمتعة المسجلة المسموح بها: </b></p>
</font></td>
	<td width="330" valign="top"><font class='tablefont'>
<p>وزن الأمتعة المسموح تسجيلها ونقلها مجاناً في جسم الطائرة.</p>
	</font></td>
	</tr>
	<tr>
	<td width="243" valign="top"><font class='tablefont'><b>الأمتعة المحمولة المسموح بها: </b><p></font></td>
	<td width="330" valign="top"><font class='tablefont'>
الوزن المسموح حمله إلى داخل مقصورة الطائرة. 
	</font></td>
	</tr>
	<tr>
	<td width="243" valign="top"><font class='tablefont'>
<p><b>داخل دول مجلس التعاون الخليجي (باستثناء جدة)  : </b></p>
	</font></td>
	<td width="330" valign="top"><font class='tablefont'>
<p>يسمح بتسجيل 25 كغ. حقيبة واحدة من الأمتعة المحمولة على ألا يزيد وزنها عن 7 كغ وألا تتجاوز أبعادها 55 x 40 x 20 سم </p>
	</font></td>
	</tr>
	<tr>
	<td width="243" valign="top"><font class='tablefont'>
<p><b>جميع الرحلات الأخرى </b></p>
</font></td>
	<td width="330" valign="top"><font class='tablefont'>
<p>يسمح بتسجيل 30 كغ. حقيبة محمولة واحدة على ألا يزيد وزنها عن 6 كغ وألا تتجاوز أبعادها 55 x 40 x 20 سم </p>
	</font></td>
	</tr>
	</table>
	<p> الحد الأقصى للوزن المسموح به هو 32  كجم  لحقيبة السفر الواحدة  بشرط أن يكون مجموع قياساتها 160 سم</p>
	<p>يسمح باقتناء صندوق واحد فقط للشحن يحتوي على البضائع الأصلية للمنتج المراد شحنه مع الأمتعة  بحيث يكون الوزن الإجمالي 32 كجم لجميع الامتعة.</p>
	<p>جميع الصناديق المخصصة للشحن والتي يبلغ وزنها 32 كجم أو أكثر ومجموع قياساتها 160 سم أو أكثر سوف يتم شحنها ، حيث لا تتوفر خدمة الشحن الجوي على  العربية للطيران.  للبضائع التي يتجاوز حجمها الحد الاقصى المسموح به.</p>
	<p>إضافة لما سبق، يسمح للراكب بحمل التالي إلى مقصورة الطائرة مجاناً: معطف، حقيبة يد، مظلة، بضائع غير خاضعة للجمارك وكمبيوتر محمول. </p>
	<p>يجب تخزين كافة الأمتعة الشخصية في الخزائن المخصصة فوق الرأس أو تحت المقعد أمام الراكب. مع المحافظة على مخارج الطوارئ سالكة. </p>

			<ul>
			<li>دول الخليج العربي (باستثناء جدة):	10 درهم/كيلو</li>
			<li>الدول الأخرى:		      	15 درهم/كيلو</li>
		</ul>
	<p>الرجاء قراءة <a href="www.airarabia.com/arabic/travel-regulation.html"><u>تعليماتنا للسفر </u></a> للمزيد من المعلومات</p>
	
				<div id="page-links"></div>
					
						</div>
     <!-- END: Content Body -->
  </div>
  <!-- START: Content Body -->
  <div id="separator" style="clear:both;">&nbsp;</div>
</div>
</body>
<script type="text/javascript">	
	<!--
		opener.childfocus(this);
			
	//-->
	</script>
								