<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <link rel="shortcut icon" href="../images/AA.ico"> 
    <link rel="stylesheet" type="text/css" href="../css/<fmt:message key="msg.pg.css.name"/>">
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   
	<script src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>   	
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>			
	</head>
 <body onkeydown='return Body_onKeyDown(event)' oncontextmenu="return showContextMenu()" ondrag='return false' class='PageBackGround' onUnload="childWindowClosed()" scroll="no">
 <form id='formPWD' name='formPWD' method='post' action=''>
	<br>
  	<table width="500" border="5" cellpadding="0" cellspacing="0" align="center" class="outLine">
		<tr>
			<td valign="middle" class="outLine" align="center">
				<%@ include file="../common/includeFrameTop.jsp" %>
					<table width='98%' border='0' cellpadding='2' cellspacing='0'>
						<tr>
							<td>
								<font>
									<fmt:message key="msg.login.why.register"/><c:out value='${sessionScope.appAirlineHomeUrlStr}' escapeXml='false' /> <fmt:message key="msg.login.why.register1"/>
								</font>
							</td>							
						</tr>
						<tr>
							<td align="<fmt:message key='msg.pg.align.right'/>">
								<input type='button' id='btnClose' class='Button' value='<fmt:message key="msg.res.btn.Close"/>' onclick='window.close()'>
							</td>							
						</tr>
				  	</table>
				  <%@ include file="../common/includeFrameBottom.jsp" %>
			</td>
		</tr>
	</table>
	<input type="hidden" id="hdnMode" name="hdnMode">
	</form>
  </body>
</html>
