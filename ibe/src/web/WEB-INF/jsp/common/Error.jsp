<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='-1'>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href="../images/AA.ico?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
    <link rel='stylesheet' type='text/css' href='../css/<fmt:message key="msg.pg.css.name"/>'>
	<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>        
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    		
	<script src="../js/common/cardValidator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 	
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>		
  </head>
  <body onkeydown='return Body_onKeyDown(event)' ondrag='return false' oncontextmenu="return showContextMenu()">
	<%@ include file='../common/includeTop.jsp'%>
		<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
				<tr>
					<td class="pageBody">
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<tr>
								<td class="pageBody">
										<%@ include file='../common/includeFrameTop.jsp'%>
											<table width="100%" border="0" cellpadding="2" cellspacing="2">
												<tr>
													<td align="center">
													<img src="<fmt:message key="msg.pg.img.folder"/>n057_no_cache.gif">
													</td>
												</tr>
												<tr>
													<td align="center">
														<font class="mandatory fntBold"><fmt:message key="msg.error.hd"/></font>
													</td>
												</tr>
												
												<tr>
													<td style="height:200px" align="center" valign="top">
														<font><c:out value='${requestScope.error}' escapeXml='false'/></font>
													</td>
												</tr>
											</table>
										<%@ include file='../common/includeFrameBottom.jsp'%>	
									<br>
								</td>
							</tr>
						</table>									
					</td>
				</tr>
			</table>
	<%@ include file='../common/includeBottom.jsp'%>
	<script type="text/javascript" >
	<!--
		strPageID = "ERROR"
		var strHomeURL = "<c:out value='${requestScope.strHomeURL}' escapeXml='false'/>";
		function onLoad(){
			try{
				if (typeof(parent.document.getElementById("divLoadMsgPayment")) == "object"){
					parent.document.getElementById("divLoadMsgPayment").style.visibility = "hidden";
				}
				top[0].LoadingCompleted();
			}catch(e){}
		}
		
		onLoad();
	//-->	
	</script>
	<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />		
  </body>
</html>
			