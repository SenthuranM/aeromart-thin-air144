<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.res.flt.TermsNCond"/></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
  </head>
    <body onkeydown='return Body_onKeyDown(event)' scroll="auto" oncontextmenu="return showContextMenu()"  ondrag='return false'  onUnload="childWindowClosed()">
    	<table width="95%" border="0" cellpadding="2" cellspacing" align="center">
    		<tr>
    			<td>
					<span id="spnTerms">
					<c:out value="${requestScope.termsNCondFull}" escapeXml="false" />
					</span>
				</td>
			</tr>
		</table>
</body>
<script type="text/javascript">
<!--
	var blnTerms = true;
	opener.top[0].blnChildOpen = true;
//-->
</script>
</html>
</html>