<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='-1'>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/<fmt:message key="msg.pg.css.name"/>'>
    <style type='text/css'>
    	<!-- 
    	.GridItemRow
		{
			border-width:1px 2px 2px 1px;  
			height:18px; 
			border-style:solid; 
			border-color:#CCCCCC gray #D4D0C8 #CCCCCC;
			}
			
		.GridItem
		{
			background-color:white;
			}	
		.BodyColor{
			background-color:transparent;		
		}
		
		
    	-->
    </style>
  </head>
	<body class='BodyColor'>
	<table align='center' class="prograssBarTable">
		<tr>
			<td class='' align='center' style='<fmt:message key="msg.res.progress.color"/>'>
				<font class='fntBold <fmt:message key="msg.pg.font.Page"/>  fntDefaultWhite'><fmt:message key="msg.res.progress.payment"/></font>				 
				<IMG SRC='<fmt:message key="msg.res.progress.banner"/>'>
			</td>
		</tr>
	</table>	
</html>