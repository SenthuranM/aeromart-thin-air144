														<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
														<form method="post" action="showFlightSearch.action" id="fromFlightSearch" name="fromFlightSearch">
														<table width="100%" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td style='background-image:url(../images/<fmt:message key="msg.kiosk.bookbtn"/>) ;height:339px;width:235px;' align='center'>
																<br><br>
																<table width='80%' border='0' cellpadding='0' cellspacing='2' ID='Table7' align="center">
																<!-- 
																<tr>
																	<td colspan='4'><font class="<fmt:message key="msg.pg.font.Page"/>"><b><fmt:message key="msg.fltSearch.title"/></b></font></td>
																</tr>
																-->
																<!--
																<tr>
																	<td colspan='4'><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.From"/></font></td>
																</tr>
																-->
																<tr>
																	<td colspan='4'>
																		<select id='selFromLoc' size='1' style='width:175px;' NAME='selFromLoc' onchange='selFromLocOnChange()'>
																		</select>
																	</td>
																</tr>
																<!--
																<tr>
																	<td colspan='4'><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.To"/></font></td>
																</tr>
																-->
																<tr>
																	<td colspan='4'>
																		<select id='selToLoc' size='1' style='width:175px;' NAME='selToLoc' onchange='selToLocOnChange()'>
																		</select>
																	</td>
																</tr>
																<tr>
																	<td colspan='4'><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.depDt"/></font></td>
																</tr>
																<tr>
																	<td colspan='4'>
																		<table width="100%" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																			 	<td width="95%">
																					<select id='selDtDept' size='1' style='width:50px;' NAME='selDtDept' onchange="selDtDeptOnChange()">
																						<option></option>
																					</select>
																					<select id='selYrDept' size='1' style='width:100px;' NAME='selYrDept' onchange="selYrDeptOnChange()">
																						<option></option>
																					</select>
																				</td>
																				<td  align='<fmt:message key="msg.pg.align.right"/>'>
																					<a href='javascript:void(0)' onclick='LoadCalendar(0,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='<fmt:message key="msg.fltSearch.tt.calendar"/>'></a>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='4'>
																		<select id='selDVariance' size='1' style='width:50px;' NAME='selDVariance' title='<fmt:message key="msg.fltSearch.tt.deptVariance"/>'>
																		</select>
																		<font class="<fmt:message key="msg.pg.font.Page"/>"><b>+/-</b> <fmt:message key="msg.fltSearch.Days"/></font>
																	</td>
																</tr>
																<tr>
																	<td><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.retDt"/></font></td>
																	<td colspan="3" align='<fmt:message key="msg.pg.align.right"/>'>
																		<table width="100%" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																				<td align='<fmt:message key="msg.pg.align.right"/>'>
																					<font class="<fmt:message key="msg.pg.font.Page"/>">&nbsp;<fmt:message key="msg.fltSearch.ReturnTrip"/>&nbsp;</font>
																				</td>
																				<td align='<fmt:message key="msg.pg.align.right"/>'>
																					<input type="checkbox" id="chkReturnTrip" name="chkReturnTrip" onclick="chkReturnTrip_click()" class="noBorder">
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='4'>
																		<table width="100%" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																			 	<td width="95%">
																					<select id='selDtRetu' size='1' style='width:50px;' NAME='selDtRetu' onchange="selDtRetuOnChange()">
																						<option></option>
																					</select>
																					<select id='selYrRetu' size='1' style='width:100px;' NAME='selYrRetu' onchange="selYrRetuOnChange()">
																						<option></option>
																					</select>
																				</td>
																				<td  align='<fmt:message key="msg.pg.align.right"/>'>
																					<a href='javascript:void(0)' onclick='LoadCalendar(1,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='<fmt:message key="msg.fltSearch.tt.calendar"/>'></a>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>		
																<tr>
																	<td colspan='4'>
																		<select id='selRVariance' size='1' style='width:50px;' NAME='selRVariance' title='<fmt:message key="msg.fltSearch.tt.retVariance"/>'>
																		</select>
																		<font class="<fmt:message key="msg.pg.font.Page"/>"><b>+/-</b> <fmt:message key="msg.fltSearch.Days"/></font>
																	</td>
																</tr>
																<tr>
																	<td colspan='4'>
																		<table width='100%' border='0' cellpadding='0' cellspacing='0' ID='Table8'>
																			<tr>
																				<td width='30%'><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.Adults"/></font></td>
																				<td width='33%'><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.Child"/></font></td>
																				<td width='37%'><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.Infants"/>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
																			</tr>
																			<tr>
																				<td><font class="<fmt:message key="msg.pg.font.Page"/>">&nbsp;</font></td>
																				<td><font class='fntsmall <fmt:message key="msg.pg.font.Page"/>'><fmt:message key="msg.fltSearch.Child.detail"/></font></td>
																				<td><font class='fntsmall <fmt:message key="msg.pg.font.Page"/>'><fmt:message key="msg.fltSearch.Infants.detail"/></font></td>
																			</tr>
																			<tr>
																				<td>
																					<select id='selAdults' size='1' style='width:45px;' NAME='selAdults' onChange='selAdults_onChange()'>
																					</select>														
																				</td>			
																				<td>
																					<select id='selChild' size='1' style='width:50px;' NAME='selChild'>
																					</select>														
																				</td>
																				<td align='right'>
																					<select id='selInfants' size='1' style='width:65px;' NAME='selInfants'>
																					</select>														
																				</td>			
																			</tr>	
																		</table>
																	</td>
																</tr>																
																<tr>
																	<td colspan='4'><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.fltSearch.Currency"/></font></td>
																</tr>
																<tr>
																	<td colspan='4'>
																		<select id='selCurrency' size='1' style='width:175px;' NAME='selCurrency'>
																		</select>
																	</td>
																</tr>
																<tr>
																	<td colspan='4' align='<fmt:message key="msg.pg.align.right"/>' valign="top" id="tdSearch">
																		<input type='button' id='btnSearch' class='Button' value='<fmt:message key="msg.res.btn.Search"/>' title='<fmt:message key="msg.fltSearch.tt.Search"/>' NAME='btnSearch' onclick='flightSearchBtnOnClick(0)'>
																	</td>
																</tr>
															</table><input type="hidden" id="hdnFromLoc" name="hdnFromLoc"><input type="hidden" id="hdnToLoc" name="hdnToLoc"><input type="hidden" id="hdnDeptDate" name="hdnDeptDate"><input type="hidden" id="hdnRetDate" name="hdnRetDate"><input type="hidden" id="hdnOutFlightID" name="hdnOutFlightID"><input type="hidden" id="hdnRetFlightID" name="hdnRetFlightID"><input type="hidden" id="hdnAdults" name="hdnAdults"><input type="hidden" id="hdnChild" name="hdnChild"><input type="hidden" id="hdnInfants" name="hdnInfants"><input type="hidden" id="hdnCurrency" name="hdnCurrency"><input type="hidden" id="hdnMode" name="hdnMode"><input type="hidden" id="hdnOVar" name="hdnOVar"><input type="hidden" id="hdnRVar" name="hdnRVar">

																</td>
															</tr>
														</table>

														</form><!-- <script type="text/javascript"><c:out value="${requestScope.reqClientMessages}" escapeXml="false" /></script>-->
																									