<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <link rel="shortcut icon" href="../images/AA.ico"> 
    <link rel="stylesheet" type="text/css" href="../css/<fmt:message key="msg.pg.css.name"/>">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>       
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	  	
	<script language="JavaScript" src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
  </head>
  <body onkeydown='return Body_onKeyDown(event)' oncontextmenu="return showContextMenu()">
  	<br><br><br>
	<table width="500" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td valign="top">
				<%@ include file="../common/includeFrameTop.jsp" %>
					<table width='100%' border='0' cellpadding='2' cellspacing='0'>
				  		<tr>
				  			<td align='center'>
				  				<br><br><br>
				  				<img src="../images/LogoAni<c:out value="${sessionScope.sysCarrier}" escapeXml="false" />_no_cache.gif">
				  				<br><br><br>
				  			</td>
				  		</tr>
						<tr>
				  			<td align='center'>
								<font class='fntBold fntLarge mandatory <fmt:message key="msg.pg.font.Page"/>'>
									<fmt:message key="msg.res.pay.success.msg"/>
								</font>												
				  			</td>
				  		</tr>
				  		<tr>
				  			<td align='center'>
				  			  	<br><br>
								<a href="<c:out value='${sessionScope.appAirlineHomeUrlStr}' escapeXml='false' />" target="_top"><font class="fntBold fntLarge"><u><c:out value='${sessionScope.appAirlineHomeTextStr}' escapeXml='false' /></u></font></a>
				  			  	<br><br>								
				  			</td>
				  		</tr>
				  	</table>
				  <%@ include file="../common/includeFrameBottom.jsp" %>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	<!--
	strPageID = "ERROR"
	var strHomeURL = "<c:out value='${requestScope.strHomeURL}' escapeXml='false'/>";
	function onLoad(){
		try{
			if (strHomeURL == ""){
				//setVisible("btnSignIn", false)
			}
			if (typeof(parent.document.getElementById("divLoadMsgPayment")) == "object"){
				parent.document.getElementById("divLoadMsgPayment").style.visibility = "hidden";
			}
			top[0].LoadingCompleted();
		}catch(e){
		}
	}
	onLoad();
	//-->
	</script>
  </body>
</html>
