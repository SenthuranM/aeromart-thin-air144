<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<title></title>
<link rel="stylesheet" type="text/css" href="../css/myStyle_no_cache.css"/>
<%@ include file='../common/interlinePgHD.jsp' %>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
	var strConfData = <c:out value="${requestScope.sesKioskConfData}" escapeXml="false" />;
	var timeOut = <c:out value="${requestScope.timeOut}" escapeXml="false" />;
</script>
</head>
  <body>
	<c:import url="../common/pageLoading.jsp"/>
    <div id="divLoadBg" style="display: none;">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' >	   
		<tr>
		<td align='center' class="outerPageBackGround">
			<table border='0' style='width:940px;' cellpadding='2' cellspacing='0' class='PageBackGround'>	
			<!-- Top Banner -->
			<c:import url="../../../../ext_html/header.jsp" />
			<tr>
				<td colspan='2' align='center'>
					<table width='97%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class="alignRight">
								<img border='0' class="linkImage cursorPointer" src="../images/home_no_cache.gif" id="linkHome"/>
							</td>
						</tr>
						<tr>
							<td class='rowSingleGap'></td>
						</tr>			
					</table>
				</td>
			 </tr> 
			<tr>
				<td align="center">
					<table width='100%' border='0' cellpadding='2' cellspacing='0'>
						
						<tr>
							<td>
								<table width='100%' border='0' cellpadding='2' cellspacing='0'>
										<tr>
											<td align='center'>
												<br/>
													<img src="../images/LogoAni<c:out value="${sessionScope.sysCarrier}" escapeXml="false" />_no_cache.gif" />
												<br/>
											</td>
										</tr>
										<tr>
											<td align='center'>
												<font class="fntBold">									
														<br/><br/>
															<label id="lblResNo"> Your Reservation number is :</label> &nbsp;
															<label id="resNo"> </label>
														<br/><br/><br/>													
														<label id="lblOnHoldTime"> Dear Customer your booking is placed ON-HOLD for</label> 
														<label id="time"></label>
														<label id="lblHours"> hour(s), </label> 
														<br/>
														<label id="lblCollectItinerary">
															please proceed to the cashier to make the payment &amp; collect your itinerary.
														</label>
														<br/>
														<label id="lblExpiryMessage">
															Bookings not paid for within the time limit will automatically expire.
														</label>	
														<br/>													
													<br/>
												</font>								
											</td>
										</tr>
									</table>
							</td>
						</tr>
					</table>
					<br/>
				</td>
			</tr>
			<!-- Bottom AA Logo -->
				<tr>
				<td class='appLogo' colspan="2"></td>
				</tr>
				<c:import url="../../../../ext_html/cfooter.jsp" />
			</table>
				</td>
			</tr>
		</table>
			<div id='divLoadMsg' class="mainPageLoader">
				<%@ include file='../common/includeLoadingMsg.jsp' %>
			</div>
		<form method="post" action="" name="formHidden" id="formHidden">
			<input type="hidden" id="hdnMode" name="hdnMode"/>
		</form>	
		<%@ include file='../common/iBECommonParam.jsp'%>
	</div>	
  	<script src="../js/v2/kiosk/kioskOHDConfirm.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
  </body>   	
</html>