<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<div id="pageLoading" style='position:absolute;left: 40%;top: 50%;z-index: 100;display:block' class="mainPageLoaderFreame">
	<table align='center' class="prograssBarTable" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" border="0">
		<tr>
			<td class='GridItemRowLoadMsg GridItemMsg progressBG' align='center' style='<fmt:message key="msg.res.progress.color"/>'>
				<font class='fntBold <fmt:message key="msg.pg.font.Page"/>  fntDefaultWhite' style="<fmt:message key="msg.res.progress.forecolor"/>"><fmt:message key="msg.res.progress"/></font>			
				<IMG SRC='<fmt:message key="msg.res.progress.banner"/>'>
			</td>
		</tr>
	</table>
</div>