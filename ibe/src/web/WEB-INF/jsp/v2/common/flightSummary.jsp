	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<!-- Booking Summary -->
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class='lpaneTopL'></td>
						<td class='lpaneTopM'><label id="lblBookingSummary" class='fntBold gridHDFont'></label></td>
						<td class='lpaneTopR'></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class='lpaneMidL'></td>
						<td class='lpaneMidM'>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">								
								<tr>
									<td class='pnlBottom'>
										<table width="100%" border="0" cellspacing="1" cellpadding="1">
											<tr>
												<td>
													<label id="lblFlightDetail" class='fntBold hdFontColor'></label>
													<br/>
													<label name="lblNoFlightSelect" id="lblNoFlightSelect" style="display: none"></label>
												</td>
											</tr>
											<tr>
												<td class='rowGap'></td>
											</tr>
											<tr>
												<td><label id="lblReturn"/></td>           
											</tr>
											<tr>
												<td><label id="lblAdultCount"/></td>            
											</tr>
											<tr>            
												<td><label id="lblChildCount"/></td>
											</tr>
											<tr>
												<td><label id="lblInfantCount"/></td>
											</tr>         
										</table>
									</td>
								</tr>
								<tr>
									<td class='rowGap'></td>
								</tr>
								<tr id="trBookingSFlightPanel">
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate">
											<tr>
												<td class='pnlTop pnlWidthLeft'></td>
												<td class='pnlTopR'></td>
											</tr>
											<tr>
												<td class='pnlBottom' colspan='2'>
													<table width='100%' border='0' cellpadding='1' cellspacing='1'>
														<tr>
															<td colspan="3"><label id="segmentCode"  class='fntBold hdFontColor'/></td>            
														</tr>
														<tr>
															<td width="50%"><label id="lblFlightNo"></label></td>
															<td width="1%"><label>:</label></td>
															<td width="49%"><label id="flightNumber"/></td>
														</tr>
														<tr>
															<td><label  id="lblDate"></label> </td>
															<td><label>:</label></td>
															<td><label id="depatureDate"/></td>
														</tr>
														<tr>
															<td><label id="lblDepature"></label></td>
															<td><label>:</label></td>
															<td><label id="depatureTime"/></td>
														</tr>
														<tr>
															<td><label id="lblArrival"></label></td>
															<td><label>:</label></td>
															<td><label id="arrivalTime"/></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td class='lpaneMidR'></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class='lpaneBotL'></td>
						<td class='lpaneBotM'></td>
						<td class='lpaneBotR'></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class='rowGap'></td>
		</tr>
		
		<!-- Payment Summary -->
		<tr id="trPaymentPannel">
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class='lpaneTopL'></td>
									<td class='lpaneTopM'><label id="lblPaymentSummary" class='fntBold gridHDFont'></label></td>
									<td class='lpaneTopR'></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class='lpaneMidL'></td>
									<td class='lpaneMidM'>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">								
											<tr>
												<td class='pnlBottom'>
													<table width="100%" border="0" cellspacing="1" cellpadding="1">
														<tr>
															<td>
																<label id="lblBookingDetail" class='fntBold hdFontColor'></label>
																<br/>
																<label name="lblNoFlightSelect" id="lblNoFlightSelect" style="display: none"></label>
																<label id="lblRetFlight" class='fntBold'/>
															</td>
														</tr>
														<tr>
															<td><label id="lblAdults" class='fntBold'/></td>
														</tr>
														<tr>
															<td><label id="lblChildren" class='fntBold'/></td>
														</tr>
														<tr>
															<td><label id="lblInfants" class='fntBold'/></td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr id="trPaymentSPanel">
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate">
														<tr>
															<td class='pnlTop pnlWidthLeft'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td class='pnlBottom' colspan='2'>
																<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																	<tr>
																		<td width="50%"><label id="lblAirFare"></label></td>
																		<td width="2%" class="hdFontColor fntBold">:</td>																				
																		<td width="10%" id="airFareCurrCode" class='fntDefault txtBold' align="left"></td>
																		<td width="38%" id="airFare" class='fntDefault txtBold' align="right"></td>																					
																	</tr>
																 	<tr>
																		<td width="50%"><label id="lblCharges"></label></td>
																		<td width="2%" class="hdFontColor fntBold">:</td>	
																		<td width="10%" id="chargesCurrCode" class='fntDefault txtBold' align="left"></td>
																		<td width="38%" id="charges"  class='fntDefault txtBold' align="right"></td>
																	</tr>																				
																	<tr id='trSeatCharges'>
																		<td><label id='lblSeatSelection' >Seat Selection</label></td>
																		<td class="hdFontColor fntBold">:</td>
																		<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																		<td align="right"><div id='divSeatCharges' class='fntDefault txtBold'></div></td>
																	</tr>																				
																	<tr id='trInsuranceCharges'>
																		<td><label id='lblTravelSecure' >Travel Insurance</label></td>
																		<td class="hdFontColor fntBold">:</td>
																		<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																		<td align="right"><div id='divInsuranceCharges' class='fntDefault txtBold'></div></td>
																	</tr>
																	<tr id='trMealCharges'>
																		<td><label id='lblMealSelection' >Meal Selection</label></td>
																		<td class="hdFontColor fntBold">:</td>
																		<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																		<td align="right"><div id='divMealCharges' class='fntDefault txtBold'></div></td>
																	</tr>
																	<tr id='trHalaCharges'>
																		<td><label id='lblHalaService' >Hala Service</label></td>
																		<td class="hdFontColor fntBold">:</td>
																		<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																		<td align="right"><div id='divHalaCharges' class='fntDefault txtBold'></div></td>
																	</tr>
																	<tr id='trAptCharges'>
																		<td><label id='lblAirportTransfer' >Airport Transfer</label></td>
																		<td class="hdFontColor fntBold">:</td>
																		<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																		<td align="right"><div id='divHalaCharges' class='fntDefault txtBold'></div></td>
																	</tr>
																	<tr>
																		<td width="50%"><label id="lblTNXFee">Transaction Fees</label></td>
																		<td width="2%" class="hdFontColor fntBold">:</td>	
																		<td width="10%" id="txnFeeCurrCode" class='fntDefault txtBold' align="left"></td>
																		<td width="38%" id="txnFee"  class='fntDefault txtBold' align="right"></td>
																	</tr>
																	<tr>
																		<td colspan="4" height="10px"></td>
																	</tr>
																	<tr>
																		<td width="50%"><label id="lblTotal" class='fntBold'>Total</label></td>
																		<td width="2%" class="hdFontColor fntBold">:</td>
																		<td width="10%" id="totalCurrCode"  class='fntDefault fntBold hdFontColor' align="left"></td>
																		<td width="38%" id="total" class='fntDefault fntBold hdFontColor' align="right"></td>
																	</tr> 
																	<tr style="display: none;" id="selectedCurr">
																		<td width="50%"></td>
																		<td width="2%" class="hdFontColor fntBold hdFontColor">:</td>
																		<td width="10%" id="totalSelectedCurrCode"  class='fntDefault fntBold hdFontColor' align="left"></td>
																		<td width="38%" id="selectedTotal" class='fntDefault fntBold hdFontColor' align="right"></td>
																	</tr> 
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td class='lpaneMidR'></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class='lpaneBotL'></td>
									<td class='lpaneBotM'></td>
									<td class='lpaneBotR'></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>		
	</table>
