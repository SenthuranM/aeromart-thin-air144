<%-- TODO: Refactor for V2--%>
<%@ page isErrorPage="true" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='/WEB-INF/jsp/common/topHolder.jsp' %>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>

<%
String url = AppParamUtil.getSecureIBEUrl() + "showReservation.action";
String carrier = AppParamUtil.getDefaultCarrier();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
		<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['gwo._setAccount', 'UA-19350552-2']);
		  _gaq.push(['gwo._trackPageview', '/1698405054/goal']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
		<!-- End of Google Website Optimizer Tracking Script -->
	</c:if>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'/>
    <meta http-equiv='cache-control' content='no-cache'/>
    <meta http-equiv='expires' content='-1'/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel='shortcut icon' href="../images/AA.ico?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/>	
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		UI_commonSystem.setPageFullView();
		if(opener!=null){	
            // Should Execute Page all Scripts - Track Error
			try {		
				if(opener.UI_commonSystem){
					opener.UI_commonSystem.setErrorStatus();
				}
			}catch(e){};
			setTimeout("window.close();self.close();",2000);
		}		
		UI_Top.holder().UI_commonSystem.loadingCompleted();	
	});

	
	</script>
	</head>
  <body>
  <div id="mainContainer">
  <input type="hidden" id="hdnURL" name="hdnURL" value="<%=url %>"/>
  	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' >
  		   
		<tr>
		<td align='center' class="outerPageBackGround">
			<table border='0' style='width:940px;' cellpadding='0' cellspacing='0' class='PageBackGround'>
			<!-- Top Banner -->
			<c:import url="../../../../ext_html/header.jsp" />
			<tr>
			<td valign="top" class="SesionOverBg tapMainbody">
					<table width='100%' border='0' cellpadding='0' cellspacing='0' class="innderTable">
				  		<tr>
				  			<td align='center'><span id="spnHome"></span></td>
				  		</tr>
						<tr>
				  			<td align='center'>
								<font class='fntBold fntLarge mandatory <fmt:message key="msg.pg.font.Page"/>'>
									<fmt:message key="msg.res.session.msg"/>
								</font>												
				  			</td>
				  		</tr>
				  		
				  		<tr>
				  			<td align='center'>
				  				<span id="spnSearch">
									<font class='fntBold fntLarge mandatory <fmt:message key="msg.pg.font.Page"/>'>
										<!--<input type="button" id="homePage" class="Button ButtonLarge" value="Search for a new flight"/>
									--></font>		
								</span>										
				  			</td>
				  		</tr>
				  		<tr>
				  			<td align='center'>
				  				<form id="formSignIn" name="formSignIn" method="post">
				  				<input type="hidden" id="hdnParamData" name="hdnParamData" value="en^SI^"/>
				  				<input type="hidden" id="hdnCarrier" name="hdnCarrier" value="<%=carrier %>"/>
								<font class='fntBold fntLarge mandatory <fmt:message key="msg.pg.font.Page"/>'>
									<input type="button" id="signIn" class="Button ButtonLarge" value="<fmt:message key='msg.login.button.signIn'/>"/>
								</font>	
								
								<input type="hidden" 	id="homeURL" 		name="commonParams.homeURL" value='<c:out value="${commonParams.homeURL}" escapeXml="false"/>' />
								</form>											
				  			</td>
				  		</tr>
				  		
				  		<tr>
				  			<td align='center'>
				  			  	<br><br>
									<input type="button" id="homePage1" class="Button ButtonLarge" value="<fmt:message key='msg.res.session.start'/>"  />
				  			  	<br><br>								
				  			</td>
				  		</tr>
				  	<!-- Bottom AA Logo -->
				<tr>
				<td class='appLogo' colspan="2"></td>
				</tr>
				<c:import url="../../../../ext_html/cfooter.jsp" />
				</table>
				</td>
			</tr>
		</table>
		<input type= "hidden" id="pageID" value="SESSION_PAGE" />
		</td></tr></table>
	<script type="text/javascript">
	<!--
	$(document).ready(function(){		
		strPageID = "ERROR";		
		var strHomeURL = "<c:out value='${requestScope.strHomeURL}' escapeXml='false'/>";		
		var arrCarriers = new Array();
		<c:out value='${applicationScope.appCarrierCode}' escapeXml='false'/>	
		
		function writeSpan() {
			var stml = '';
			stml+= '<table width="100%" border="0" cellpadding="2" cellspacing="0">';
			
			for(var cl=0;cl<arrCarriers.length; cl++) {	
					if(arrCarriers[cl][2] == "true"){				
						stml+= '  <tr>';
						stml+= '    <td align="center">';		
						stml+= '		<br><br><br> ';
						stml+= '		<a href="'+arrCarriers[cl][1]+'" target=_top>	<img alt="'+arrCarriers[cl][1]+'" src="../images/LogoAni'+arrCarriers[cl][0]+'_no_cache.gif" border="0"> ';
						stml+= '		<br><br><br> ';
						stml+= '	</td>';
						stml+= '  </tr>';						
					}
			 }	
			
			stml+= '</table>';
			
			DivWrite("spnHome", stml);
		}
		var caller = "";
			caller = "<c:out value="${requestScope.caller}" escapeXml="false" />";
		
		 	 
		
		function login_click(){
			try{
			
			caller = "ibe";
			caller = "<c:out value="${requestScope.caller}" escapeXml="false" />";
			var homePage = "<c:out value="${requestScope.homePage}" escapeXml="false" />";  
			if(caller == "kiosk"){
				UI_Top.holder().location.replace(strHomePageURL);
			}else if(caller == "ibe"){
				UI_Top.holder().location.replace(strHomePageURL);
			}else if(caller == "cus"){
			    window.location(homePage);
			}else{
				UI_Top.holder().location.replace(strHomePageURL);
			}
			
			}catch(e){}
		}
		
		function onLoad(){
			try{
				if (strHomeURL == ""){
					setVisible("btnSignIn", false)
				}
				writeSpan();
				top[0].LoadingCompleted();
			}catch(e){
			}
		}
		
		function signIn(){
		 
		var strAction= $("#hdnURL").val();
						var objFrm = document.forms["formSignIn"];
						objFrm.action=strAction;
						objFrm.target="_top";
			 			objFrm.submit();
		}
		
		$("#signIn").click(function(){
			signIn();
		})
		$("#homePage").click(function(){
			goHome()
		});
		$("#homePage1").click(function(){
			//alert(arrCarriers[0][1])
			goHome(arrCarriers[0][1])
		});
		function goHome(){
			UI_Top.holder().location.replace(arrCarriers[0][1])
		}
		$("#homeURL").val(arrCarriers[0][1]);
		UI_commonSystem.loadingCompleted();
	});
	//onLoad();
	//-->	
	</script>	
	<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
 	<%-- Remove Existing  JSESSIONID if Session expired to prevent http https page transitions--%>
 	<%
	    Cookie cookie = new Cookie("JSESSIONID", null);
	    cookie.setPath(request.getContextPath());
	    cookie.setMaxAge(0);
	    response.addCookie(cookie);
	%>	
  </div>
  </body>
</html>
