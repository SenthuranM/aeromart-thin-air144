<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/cacheClear.jsp'%>
<%@ include file='../../../jsp/common/topHolder.jsp' %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="-1"/> 	
</head>
<body>
	<form id='frmRedirect' action='<c:out value="${redirectUrl}" escapeXml='false' />' method="post">
		<c:forEach var="hdnParamData" items="${formHiddenDataMap}"> 
			<input type="hidden" name="<c:out value="${hdnParamData.key}" escapeXml='false' />" value='<c:out value="${hdnParamData.value}" escapeXml='false' />' />
		</c:forEach>		
	</form>
	
	<script type="text/javascript">
		var isTargetTop = "<c:out value="${isTargetTop}" escapeXml='false' />";
		
		if (isTargetTop == "true" || isTargetTop == true) {
			if(fromSocialSites != undefined && fromSocialSites != "" && fromSocialSites =='true'){
				document.forms["frmRedirect"].target= "_self";
			}else{
				document.forms["frmRedirect"].target= "_top";
			}
		}
		document.getElementById("frmRedirect").submit();
	</script>
</body>
</html>