<input type="hidden" id="resModifySegment" name="modifySegment" value='<c:out value="${param.modifySegment}" escapeXml="false"/>'/>
<input type="hidden" id="resModifySegmentRefNos" name="modifySegmentRefNos" value='<c:out value="${param.modifySegmentRefNos}" escapeXml="false"/>'/>
<input type="hidden" id="resPNR" 	name="pnr" value='<c:out value="${param.pnr}" escapeXml="false"/>'/>
<input type="hidden" id="resVersion" name="version" value='<c:out value="${param.version}" escapeXml="false"/>' />
<input type="hidden" id="resGroupPNR" name="groupPNR" value='<c:out value="${param.groupPNR}" escapeXml="false"/>' />
<input type="hidden" id="resOldAllSegments" name="oldAllSegments" value='<c:out value="${param.oldAllSegments}" escapeXml="false"/>' />
<input type="hidden" id="resOldFareID" 		name="oldFareID" value='<c:out value="${param.oldFareID}" escapeXml="false"/>' />
<input type="hidden" id="resOldFareType" 	name="oldFareType" value='<c:out value="${param.oldFareType}" escapeXml="false"/>' />
<input type="hidden" id="resOldFareAmount" 	name="oldFareAmount" value='<c:out value="${param.oldFareAmount}" escapeXml="false"/>' />
<input type="hidden" id="resOldFareCarrierCode" 	name="oldFareCarrierCode" value='<c:out value="${param.oldFareCarrierCode}" escapeXml="false"/>' />
<input type="hidden" id="resModifingFlightInfo" 	name="modifingFlightInfo" value='<c:out value="${param.modifingFlightInfo}" escapeXml="false"/>' />	
<input type="hidden" id="contactInfoJson" name="contactInfoJson" value='<c:out value="${param.contactInfoJson}" escapeXml="false"/>'/>
<input type="hidden" id="paxJson" name="paxJson" value='<c:out value="${param.paxJson}" escapeXml="false"/>'/>
<input type="hidden" id="hasInsurance" name="hasInsurance" 	value='<c:out value="${param.hasInsurance}" escapeXml="false"/>'/>
<!-- //segment modification IBE -->
<input type="hidden" id="modifyByRoute" name="modifyByRoute" 	value='<c:out value="${param.modifyByRoute}" escapeXml="false"/>'/>
<input type="hidden" id="modifyByDate" name="modifyByDate" 	value='<c:out value="${param.modifyByDate}" escapeXml="false"/>'/>
<input type="hidden" id="addGroundSegment" name="addGroundSegment" 	value='<c:out value="${param.addGroundSegment}" escapeXml="false"/>'/>
<input type="hidden" id="busForDeparture" name="busForDeparture" value='<c:out value="${param.busForDeparture}" escapeXml="false"/>'/>
<input type="hidden" id="busConValidTimeFrom" name="busConValidTimeFrom" value='<c:out value="${param.busConValidTimeFrom}" escapeXml="false"/>'/>
<input type="hidden" id="busConValidTimeTo" name="busConValidTimeTo" value='<c:out value="${param.busConValidTimeTo}" escapeXml="false"/>'/>
<input type="hidden" id="totalSegmentCount" name="totalSegmentCount" value='<c:out value="${param.totalSegmentCount}" escapeXml="false"/>'/>
<input type="hidden" id="requoteFlightSearch" name="requoteFlightSearch" value='<c:out value="${param.requoteFlightSearch}" escapeXml="false"/>'/>
<input type="hidden" id="cancelSegmentRequote" name="cancelSegmentRequote" value='<c:out value="${param.cancelSegmentRequote}" escapeXml="false"/>'/>
<input type="hidden" id="nameChangeRequote" name="nameChangeRequote" value='<c:out value="${param.nameChangeRequote}" escapeXml="false"/>'/>
<input type="hidden" id="nameChangePaxData" name="nameChangePaxData" value='<c:out value="${param.nameChangePaxData}" escapeXml="false"/>'/>
<input type="hidden" id="balanceQueryData" name="balanceQueryData" value='<c:out value="${balanceQueryData}" escapeXml="false"/>'/>
<input type="hidden" id="availableCOSRankMap" name="availableCOSRankMap" value='<c:out value="${availableCOSRankMap}" escapeXml="false"/>'/>