<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	
	</head>
	<body>
		<form id="aeroMartPayResponse" name="aeroMartPayResponse" method="post" action='<c:out value='${requestScope.returnURL}' escapeXml='false' />'>
			<input type="hidden" name="orderID" id="orderID" value='<c:out value='${requestScope.orderID}' escapeXml='false' />' /> 
			<input type="hidden" name="responseCode" id="responseCode"  value='<c:out value='${requestScope.responseCode}' escapeXml='false' />' /> 
			<input type="hidden" name="responseMessage" id="responseMessage" value='<c:out value='${requestScope.responseMessage}' escapeXml='false' />' />
			<input type="hidden" id="hashString" name="hashString" value='<c:out value='${requestScope.hashString}' escapeXml='false' />' />
			<input type="hidden" name="referenceID" id="referenceID" value='<c:out value='${requestScope.referenceID}' escapeXml='false' />' />
		</form>
		<script type="text/javascript">
			window.onload = function() {
				document.forms["aeroMartPayResponse"].submit()
			}
		</script>
	
	</body>
</html>