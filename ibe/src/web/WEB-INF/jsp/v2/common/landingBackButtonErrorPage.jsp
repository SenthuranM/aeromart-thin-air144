<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/cacheClear.jsp'%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="-1"/> 	
</head>
<body>	
	<form name="redirectForm" action="showLoadPage!loadBackButtonHandleErrorPage.action" method="post">
		<input type="hidden" name="errorCode" value="<c:out value='${requestScope.errorCode}' escapeXml='false' />" />
		<input type="hidden" name="redirectAttempt" value="<c:out value='${requestScope.redirectAttempt}' escapeXml='false' />" />
	</form>
	<script type="text/javascript">		
		document.forms["redirectForm"].action = "showLoadPage!loadBackButtonHandleErrorPage.action?id=" + Math.floor(Math.random()*1000000);			
		document.forms["redirectForm"].target= "_top";
		document.forms["redirectForm"].submit();
	</script>	
</body>
</html>
