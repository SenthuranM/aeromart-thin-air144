<%@ page isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp"%>
<%@ include file='../common/cacheClear.jsp'%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" }'>
	<c:if test='${param.version=="1"}'>
		<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push([ 'gwo._setAccount', 'UA-19350552-2' ]);
			_gaq.push([ 'gwo._trackPageview', '/1698405054/test' ]);
			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl'
						: 'http://www')
						+ '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<!-- End of Google Website Optimizer Tracking Script -->
	</c:if>
	<c:if test='${param.version!="1"}'>
		<!-- Google Website Optimizer Control Script -->
		<script>
			function utmx_section() {
			}
			function utmx() {
			}
			(function() {
				var k = '1698405054', d = document, l = d.location, c = d.cookie;
				function f(n) {
					if (c) {
						var i = c.indexOf(n + '=');
						if (i > -1) {
							var j = c.indexOf(';', i);
							return escape(c.substring(i + n.length + 1,
									j < 0 ? c.length : j))
						}
					}
				}
				var x = f('__utmx'), xx = f('__utmxx'), h = l.hash;
				d
						.write('<sc'
								+ 'ript src="'
								+ 'http'
								+ (l.protocol == 'https:' ? 's://ssl'
										: '://www')
								+ '.google-analytics.com'
								+ '/siteopt.js?v=1&utmxkey='
								+ k
								+ '&utmx='
								+ (x ? x : '')
								+ '&utmxx='
								+ (xx ? xx : '')
								+ '&utmxtime='
								+ new Date().valueOf()
								+ (h ? '&utmxhash=' + escape(h.substr(1)) : '')
								+ '" type="text/javascript" charset="utf-8"></sc'+'ript>')
			})();
		</script>
		<script>
			utmx("url", 'A/B');
		</script>
		<!-- End of Google Website Optimizer Control Script -->
		<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push([ 'gwo._setAccount', 'UA-19350552-2' ]);
			_gaq.push([ 'gwo._trackPageview', '/1698405054/test' ]);
			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl'
						: 'http://www')
						+ '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<!-- End of Google Website Optimizer Tracking Script -->
	</c:if>
</c:if>
<%@ include file='../common/interlinePgHD.jsp'%>
<link rel='stylesheet' type='text/css'
	href='../css/myStyle_no_cache.css' />
<script type="text/javascript">
	$(document).ready(function() {
		UI_commonSystem.setPageFullView();
		$('#btnLoadReservation').click(function() {
			var prefLan = "<c:out value='${sessionScope.sessionDataDTO.language}' escapeXml='false'/>";
			if (prefLan == null || $.trim(prefLan) == "") {
				prefLan = "EN";
			}
			
			$('#hdnParamData').val(prefLan.toUpperCase() +"^MB^"+$('#loadReservationPnr').val()+"^"+
					$('#loadReservationLastName').val()+"^"+
					$('#loadReservationDepartureDate').val());
			$('#frmLoadReservation').submit();
		});		
	});
	UI_Top.holder().UI_commonSystem.loadingCompleted();
</script>
</head>
<body>
	<%-- Re-enter card details --%>
	<div id="divLoadBg">
		<c:if test="${requestScope.invalidCard != true}">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width: 940px;' border='0' cellpadding='0'
							cellspacing='0' align='center' class='PageBackGround'>
							<!-- Top Banner -->
							<c:if test="${requestScope.displayBanner == true}">
								<c:import url="../../../../ext_html/header.jsp" />
							</c:if>
							<tr>
								<td class="mainbody">
									<div class="differError"
										style="display: block; margin: 20px auto; background: #FDFDE8; width: 85%; border: 1px solid #AAA; -moz-border-radius: 6px 6px 6px 6px;">
										<table width="100%" border="0" cellpadding="2" cellspacing="2">
											<tr>
												<td width="15%" align="right" valign="middle"><img
													src="../images/n057_no_cache.gif" /></td>
												<td style="height: 100px; padding-left: 10px;"
													class="alignLeft" width="85%" valign="middle"><font
													class="mandatory"> <c:choose>
															<c:when test="${not empty(requestScope.error)}">
																<c:out value='${requestScope.error}' escapeXml='false' />
																Your Booking was put onhold. You can load it using pnr <c:out value="${sessionScope.sessionDataDTO.putOnHoldBeforePaymentDTO.putOnHoldBeforePaymentPnr}" escapeXml="false"/> and pay later.
																	You can also load the reservation and pay by clicking the button below.
															</c:when>
															<c:otherwise>
																	Your Booking was put onhold. You can load it using pnr <c:out value="${sessionScope.sessionDataDTO.putOnHoldBeforePaymentDTO.putOnHoldBeforePaymentPnr}" escapeXml="false"/> and pay later.
																	You can also load the reservation and pay by clicking the button below.
																</c:otherwise>
														</c:choose> </font></td>
											</tr>
											<%--<tr>
														<td align="center">
															<font class="mandatory fntBold"><fmt:message key="msg.error.hd"/></font>
														</td>
													</tr>
													
													<tr>
														
													</tr>
													 --%>
										</table>
									</div>
									<div class='rowGap'></div> <br />
									<center>
										<input type="button" id="btnLoadReservation"
											value="Load Reservation" class="Button ButtonMedium" />
									</center></td>
							</tr>
							<c:if test="${requestScope.displayBanner == true}">
								<tr>
									<td class='appLogo' colspan="2"></td>
								</tr>
								<c:import url="../../../../ext_html/cfooter.jsp" />
							</c:if>
						</table></td>
				</tr>
			</table>
		</c:if>
		<%@ include file='../common/iBECommonParam.jsp'%>
	</div>
	<form action="showReservation.action" id="frmLoadReservation" target="_top">
		<input type="hidden" id="loadReservationPnr" name="loadReservationPnr" value='<c:out value="${sessionScope.sessionDataDTO.putOnHoldBeforePaymentDTO.putOnHoldBeforePaymentPnr}" escapeXml="false"/>'/>
		<input type="hidden" id="loadReservationLastName" name="loadReservationLastName" value='<c:out value="${sessionScope.sessionDataDTO.putOnHoldBeforePaymentDTO.lastName}" escapeXml="false"/>'/>
		<jsp:useBean id="today" class="java.util.Date" scope="page" />
		<input type="hidden" id="loadReservationDepartureDate" name="loadReservationDepartureDate" value='<fmt:formatDate value="${sessionScope.sessionDataDTO.putOnHoldBeforePaymentDTO.departureDate}" pattern="dd/MM/yyyy"/>'/>
		<input type="hidden" id="hdnParamData" name="hdnParamData"/>
		<input type="hidden" id="hdnMode" name="hdnMode" value="MAKEPAYMENT"/>
	</form>
</body>
</html>
