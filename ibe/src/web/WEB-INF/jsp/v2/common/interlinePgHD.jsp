  	<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
 	
 	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="-1"/>    
	<title><fmt:message key="msg.Page.Title"/></title>	
	<link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<link rel="stylesheet" type="text/css" href="../css/commonSocial_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<link rel='shortcut icon' href="../images/AA.ico?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	
	<script type="text/javascript">
		var fromSocialSites = '<c:out value="${sessionScope.fromSocialSites}" escapeXml="false" />'; 
	</script>
	
	<!-- common javascript libs  -->
	<script src="../js/globalConfig.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/JQuery.ibe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jquery.readonly.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
	<script src="../js/v2/common/jquery.alerts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jquery.tooltip.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/topHolder.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<c:if test='${commonParams.locale != "en" && !commonParams.skipPersianCalendarChange}'>
		<script src="../js/v2/jquery/i18n/jquery.ui.datepicker-<c:out value='${commonParams.locale}' escapeXml='false'/>.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</c:if>
	<c:if test='${commonParams.locale == "fa" && commonParams.skipPersianCalendarChange}'>
		<script src="../js/v2/jquery/i18n/jquery.ui.datepicker_defaultCalendar-<c:out value='${commonParams.locale}' escapeXml='false'/>.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</c:if>
	
