<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../../../jsp/common/topHolder.jsp' %>
<html>
	<head>	
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">
			var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';		
		</script>
	</head>
	<body style="display: none" >
		<span id='spnSpotAdd'></span>
		 
		 <%-- Common tracking data --%>
		 <c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />	
		 <script type="text/javascript">
		 		var carrier = '<c:out value="${sessionScope.sessionDataDTO.carrier}" escapeXml="false" />';	
		 </script>		
		 
		<%-- Confirmation page tracking code --%>
		<c:if test='${param.pageID == "CONFIRM" }'>
			  <c:if test='${sessionScope.sessionDataDTO.carrier == "C7"}'>
			  <!-- for Cinnamon Air C7 -->
			 	<!-- Google Code for Booking Successful Conversion Page -->
				<script type="text/javascript">
				/* <![CDATA[ */
				var google_conversion_id = 983444668;
				var google_conversion_language = "en";
				var google_conversion_format = "2";
				var google_conversion_color = "ffffff";
				var google_conversion_label = "RUXJCOzhyAUQvNn41AM";
				var google_conversion_value = "<c:out value='${param.price}' escapeXml='false'/>";
				/* ]]> */
				</script>
				<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
				</script>
				<noscript>
				<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/983444668/?value=<c:out value='${param.price}' escapeXml='false'/>&amp;label=RUXJCOzhyAUQvNn41AM&amp;guid=ON&amp;script=0"/>
				</div>
				</noscript>
			  	<!-- Google Code for Booking Successful Conversion Page -->
			  </c:if>	
			  
			  			 			 
			     <c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
					<%--<script type="text/javascript">
					 	var google_conversion_id = 1028750054;
			            var google_conversion_language = "en";
			            var google_conversion_format = "2";
			            var google_conversion_color = "ffffff";
			            var google_conversion_label = "uQ21CJb9ugEQ5vXF6gM";
			            var google_conversion_value = 0;
			        </script>
			
			        <noscript>
				        <div style="display:inline;">
				        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1028750054/?label=uQ21CJb9ugEQ5vXF6gM&amp;guid=ON&amp;script=0"/>
				        </div>
			        </noscript>	
			        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>--%>
			        
			        
			        <!-- Yandex.Metrika counter -->
			         <script type="text/javascript">
			         (function (d, w, c) {
			        	    (w[c] = w[c] || []).push(function() {
			        	        try {
			        	            w.syaCounter30540667 = new Ya.Metrika({id:30540667,
			        	                    webvisor:true,
			        	                    clickmap:true,
			        	                    trackLinks:true,
			        	                    accurateTrackBounce:true});
			        	        } catch(e) { }
			        	    });

			        	    var n = d.getElementsByTagName("script")[0],
			        	        s = d.createElement("script"),
			        	        f = function () { n.parentNode.insertBefore(s, n); };
			        	    s.type = "text/javascript";
			        	    s.async = true;
			        	    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			        	    if (w.opera == "[object Opera]") {
			        	        d.addEventListener("DOMContentLoaded", f, false);
			        	    } else { f(); }
			        	})(document, window, "yandex_metrika_callbacks");
                     </script>
           			 <noscript><div><img src="//mc.yandex.ru/watch/30540667" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                     <!-- /Yandex.Metrika counter -->
             
			    
			    	<!-- Google Code for &#1047;&#1072;&#1082;&#1072;&#1079; Conversion Page -->
					<script type="text/javascript">
							/* <![CDATA[ */
							var google_conversion_id = 955424500;
							var google_conversion_language = "en";
							var google_conversion_format = "3";
							var google_conversion_color = "ffffff";
							var google_conversion_label = "jJ9PCP37ymIQ9L3KxwM";
							var google_remarketing_only = false;
							/* ]]> */
					</script>
					<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
					</script>
					<noscript>
						<div style="display:inline;">
							 <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/955424500/?label=jJ9PCP37ymIQ9L3KxwM&amp;guid=ON&amp;script=0"/>
						</div>
					</noscript>
			    	        
			        <!-- Shoogloo Start-->
			        <script src="https://trackin.affiliserve.com/142815/transaction.asp?APPID=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&MID=142815&PID=7503&status="></script>

						<noscript><img src="https://trackin.affiliserve.com/apptag.asp?APPID=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&MID=142815&PID=7503&status=" border="0" height="1" width="1"/>
					</noscript>
			        <!-- Shoogloo End-->
			        
			        <!-- Offer Conversion: Air Arabia tracking pixel -->
					<iframe src="https://vcommission.go2jump.org/SL1cS?adv_sub=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&amount=AMOUNT" scrolling="no" frameborder="0" width="1" height="1"></iframe>
					<!-- // End Offer Conversion -->

			        <!-- Added as per Airarabia request on 08/04/2013 -->
			        <script type="text/javascript">
						try {var __scP=(document.location.protocol=="https:")?"https://":"http://";
						var __scS=document.createElement("script");__scS.type="text/javascript";
						__scS.async=true;__scS.src=__scP+"app.salecycle.com/capture/AIRARABIA.js";
						document.getElementsByTagName("head")[0].appendChild(__scS);}catch(e){}
						//for Production the URL should be chnage in the above script -------- app.salecycle.com/capture/AIRARABIA.js -------
					</script>
			        
			        <img src="https://app.salecycle.com/Import/PixelCapture.aspx?c=17460&e=<c:out value="${param.c_email}" escapeXml="false"/>&sfs=orderNumber^<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" />
			        <!-- Added as per Airarabia request on 08/04/2013 -->

                     <!-- Wego's tracking tag start Jira: AARESAA-21019 on 31/03/2015-->

                     <script type="text/javascript">
                         var _wego = _wego || [];
                         _wego.push(
                                 [ 'conversionId', '50c16a51-b99d-49d3-8fa3-77303c061f8e' ],
                                 [ 'transactionId', '<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />' ],//<transaction ID>
                                 [ 'currencyCode', 'AED' ],//<ISO 4217 3-letter currency code>
                                 [ 'commission', 0 ],//<commission amount -- 0-9 only, DO NOT use commas, use period "." for decimal point>
                                 [ 'totalBookingValue', <c:out value='${param.priceRound}' escapeXml='false'/>]//<total booking value amount -- 0-9 only, DO   NOT use commas, use period "." for decimal point>
                         );
                         (function () {
                             var s = document.getElementsByTagName('script')[0];
                             var wg = document.createElement('script');
                             wg.type = 'text/javascript';
                             wg.src = 'https://s.wego.com/conversion.js';
                             s.parentNode.insertBefore(wg, s);
                         })();
                     </script>
                     <!-- Wego's tracking tag end-->
                     <!--­ Avia Sales start Jira: AARESAA-21019 on 31/03/2015 -->
                     <img src="https://pixel.aviasales.ru/airarabia?CurrencySymbol=AED&TotalPrice=<c:out value='${param.price}' escapeXml='false'/>&PNR=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />" border="0" width="1" height="1" />
                     <!--­ Avia Sales end-->
				</c:if>
				
				<c:if test='${sessionScope.sessionDataDTO.carrier == "3O"}'>
				<%--<script type="text/javascript">
					 	var google_conversion_id = 1007654460;
			            var google_conversion_language = "en";
			            var google_conversion_format = "2";
			            var google_conversion_color = "ffffff";
			            var google_conversion_label = "vEJpCNyo3gEQvKy-4AM";
			            var google_conversion_value = 0;
			        </script>
			
			        <noscript>
			        <div style="display:inline;">
			        <img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1007654460/?label=vEJpCNyo3gEQvKy-4AM&amp;guid=ON&amp;script=0"/>
			        </div>
			        </noscript>	
			        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>	--%>
			       <%--
			        <!-- Cirteo Pixel tracking Start-->
			        <script type="text/javascript" src="../js/criteo_ld.js"></script>
					<script type="text/javascript">
					CRITEO.Load([{
					    pageType: 'confirmation',
					    'Transaction ID': '<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />',
					    'Product IDs': ['<c:out value="${param.productID}" escapeXml="false"/>'],
					    'Prices': ['<c:out value="${param.price}" escapeXml="false"/>'],
					    'Quantities': ['1', '1']
					}], [3966,'pac','','100',[[7715686, 7715687], [7715682, 7715683], [7715684, 7715685]],{'Transaction ID':['t',0],'Product IDs':['i',1],'Prices':['p',1],'Quantities':['q',1]}]);
					</script>
					
			         <!-- Cirteo Pixel tracking End-->
                	--%> 
                	
                	
                	<!-- Added as per Airarabia request on 05/03/2014 -->
			        <script type="text/javascript">
						try {var __scP=(document.location.protocol=="https:")?"https://":"http://";
						var __scS=document.createElement("script");__scS.type="text/javascript";
						__scS.async=true;__scS.src=__scP+"app.salecycle.com/capture/AIRARABIA.js";
						document.getElementsByTagName("head")[0].appendChild(__scS);}catch(e){}
						//for Production the URL should be chnage in the above script -------- app.salecycle.com/capture/AIRARABIA.js -------
					</script>
			        
			        <img src="https://app.salecycle.com/Import/PixelCapture.aspx?c=17460&e=<c:out value="${param.c_email}" escapeXml="false"/>&sfs=orderNumber^<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" />
			        <!-- Added as per Airarabia request on 05/03/2014 -->
			        
               		<!-- Added as per Airarabia request on 05/05/2014 -->
               		<!-- Netaffiliation -->
					<script type="text/javascript" src="http://action.metaffiliation.com/trk.php?mclic=P4A1ED1011&argtemp=<c:out value="${sessionScope.sessionDataDTO.requestSessionIdentifier}" escapeXml="false" />&argann=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" ></script>
					<noscript>
						<img src="http://action.metaffiliation.com/trk.php?mclic=P4A1ED1011&argtemp=<c:out value="${sessionScope.sessionDataDTO.requestSessionIdentifier}" escapeXml="false" />&argann=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" width="1" height="1" border="0" />
					</noscript>
					<!-- Netaffiliation -->
               		<!-- Affilinet -->
               		<iframe src="http://4006456.fls.doubleclick.net/activityi;src=4006456;type=abcde123;cat=fghij456;u1=<c:out value="${sessionScope.sessionDataDTO.customerId}" escapeXml="false" />;u2=<c:out value="${sessionScope.sessionDataDTO.customerEmail}" escapeXml="false" />;qty=<c:out value="${param.fpq}" escapeXml="false"/>;cost=<c:out value="${param.price}" escapeXml="false"/>;ord=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />? 
               		"width="1" height="1" frameborder="0" style="display:none"></iframe>
					<!-- Affilinet -->
               		<!-- criteo -->
               		<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
					<script type="text/javascript"> 
					window.criteo_q = window.criteo_q || []; 
					window.criteo_q.push( 
					 { event: "setAccount", account: 13722}, 
					 { event: "manualDising" }, 
					 { event: "setCustomerId", id: "Customer ID" }, 
					 { event: "setSiteType", type: "m for mobile or t for tablet or d for desktop" }, 
					 { event: "trackTransaction" , id: '<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />', new_customer: 1, 
					 deduplication: 0, item: [ 
					 { id: "Flight ID", price: <c:out value="${param.price}" escapeXml="false"/>, quantity:<c:out value="${param.fpq}" escapeXml="false"/> } 
					]}); 
					
					</script>
					<!-- criteo -->
               		<!-- Added as per Airarabia request on 05/05/2014 -->
				</c:if>
				
				<script type="text/javascript"><!--			
						// Take reference from main js 

						var UI_Confirm = UI_Top.holder().UI_Confirm;
	
						function confirmPageTrack() {

							if (UI_Confirm.tempResponse != null && UI_Confirm.tempResponse != "") {
								var response = UI_Confirm.tempResponse;
								var flightDepDetails = UI_Confirm.flightDepDetails;
								var flightRetDetails = UI_Confirm.flightRetDetails;
								
								if(response.mode == "CRE") {
									addSpotAdvert(response, flightDepDetails, flightRetDetails);
								}			
								
								if(response.flightRetDetails != null && response.flightRetDetails.length > 0){
									var way = "RT";
								} else {
									var way = "OW";
								}
								
								var path = flightDepDetails[0].segmentShortCode;
								var arr = new Array();
								arr = flightDepDetails[0].segmentShortCode.split("/");
								path = arr[0];
								if( way == "RT"){
									var arr1 = new Array();
									var segCount = flightDepDetails.length/2;
									arr1 = flightDepDetails[segCount].segmentShortCode.split("/");
									path += arr1[0];
								}else{
									path += arr[arr.length-1];
								}

								//var arr1 = new Array();
								//arr1 = flightDepDetails[flightDepDetails.length - 1].segmentShortCode.split("/");
								//path += arr1[0];
								//var arr1 = new Array();
								//arr1 = flightDepDetails[flightDepDetails.length - 1].segmentShortCode.split("/");
								//path += arr1[1];
						
								var strAnalyticPage = "/funnel_G1/Confirmation/" + response.mode + "/" + response.flightDepDetails[0].carrierCode + "/" + path + "/" + way;
	
								try {
									if(strAnalyticEnable == "true") {			
										//pageTracker._trackPageview(strAnalyticPage);

										_gaq.push(['_trackPageview', strAnalyticPage ]);
									}
								} catch(ex) {}	
								<!-- Postclick Tracking Tag Start -->
								if (response.carrierCode == "G9") {
									$('#spnSpotAdd').html('<img src="http://adserver.adtech.de/pcsale/3.0/1289/0/0/0/BeaconId=16167;rettype=img;subnid=1;SalesValue=1;custom1=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" width="1" height="1">' + 
															'<img src="http://marketing.airarabia.ae/comms/action/conv/" width="1" height="1">');
		
								} else if(response.carrierCode == "3O") {
									$('#spnSpotAdd').html('<img src="http://adserver.adtech.de/pcsale/3.0/875/0/0/0/BeaconId=7497;rettype=img;subnid=1;SalesValue=1;custom1=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" width="1" height="1">' +
															'<img src="http://clic.reussissonsensemble.fr/registersale.asp?site=7545&mode=ppl&ltype=1&order=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />"  width="1" height="1">' + 
															'<img src="http://marketing.airarabia.ae/comms/action/conv/" width="1" height="1">');
								} else if(response.carrierCode == "E5") {
									$('#spnSpotAdd').html('<img src="http://marketing.airarabia.ae/comms/action/conv/" width="1" height="1">');
								}
								<!-- Postclick Tracking Tag End -->
							}
						   }
	
	
							function addSpotAdvert(response, flightDepDetails, flightRetDetails) {
								
								var strPNR = response.bookingTO.PNR;
								if(response.mode == "CRE") {
									var prodid = strPNR;
								} else {
									var prodid = strPNR + "/" + response.strRandom;			
								}
								var axel = Math.random()+"";
								var a = axel * 10000000000000;		
								var arrData ;
								var strAirLine = response.flightDepDetails[0].carrierCode;		
								var airline = null;
								
								if(response.flightDepDetails != null && response.flightDepDetails.length > 0){
									var arr = new Array();
									arr = flightDepDetails[0].segmentCode.split("/");
									arrData = arr[0];
									var arr1 = new Array();
									arr1 = flightDepDetails[flightDepDetails.length - 1].segmentCode.split("/");
									arrData += "-" + arr1[1];
								}	 
								
								if(response.flightRetDetails != null && response.flightRetDetails.length > 0){
									var arr2 = new Array();
									arr2 = flightRetDetails[0].segmentCode.split("/");
									arrData += " - " + arr2[0];
									var arr3 = new Array();
									arr3 = flightRetDetails[flightRetDetails.length - 1].segmentCode.split("/");
									arrData += "-" + arr3[1];
								}
	
								if (strAirLine == '3O') {
									airline = "AirArabia - Morocco";
								} else if (strAirLine == 'GA' || strAirLine == 'AA' || strAirLine == 'G9'){
									airline = "AirArabia - Sharjah";
								} else{
									airline = strAirLine;
								}
								
								var amountsInGoogleAnlCurrency = new Array();
								amountsInGoogleAnlCurrency = response.amountsInGoogleAnlCurrency;		
								var faresAmount = amountsInGoogleAnlCurrency[0];
								var taxesAmount = amountsInGoogleAnlCurrency[1];
								var insrAmount = amountsInGoogleAnlCurrency[2];
								var transactionFeeAmount = amountsInGoogleAnlCurrency[3];
								var cost = amountsInGoogleAnlCurrency[4];
								var surchargesAmount = amountsInGoogleAnlCurrency[5];
								//var strTravelerCount = response.colAdultCollection.length + response.colChildCollection.length;
								var strPolicyNo = response.policyNo;
								var arrGoogleInfo = {};
								arrGoogleInfo = response.reservationTransItemInfo;
								
								if(strAnalyticEnable == 'true' && cost != null && faresAmount != null) {
									
									//Visitor level variable 
									_gaq.push(['_setCustomVar',
									          1,		 	// This custom var is set to slot #1.
									          'Converted', 	// The name acts as a category for the user activity.
									          'Yes',		// This is the value of the custom variable.
									          1 			// Sets the scope to visitor-level.
									          ]);
									
									var userType = (UI_Confirm.blnRegUser)?"Registered":"Visitor";							
									//Session level variable 
									_gaq.push(['_setCustomVar',
											2,         		// This custom var is set to slot #2.
											'User Type',   	// The name of the custom variable to identify the user.
											userType,  		// Sets the value of "User Type" to "Registered" or "Visitor" depending on status.
											2               // Sets the scope to session-level.
											]);
									
									
									_gaq.push(['_addTrans',
									           prodid,           	// order ID - required
									           airline,  			// affiliation or store name
									           cost,         		// total - required
									           taxesAmount,     	// tax
									           '',              	// shipping
									           '',       			// city
									           '',    			 	// state or province
									           ''         		   	// country
									         ]);
									
									// FARE AMOUNT
									var fareSKU = strPNR + "/FARE";
						  			
						  			
						  			 _gaq.push(['_addItem',
						  			    prodid,           							// order ID - required
									    fareSKU,           							// SKU/code - required
									    arrData,        							// product name
									    'Airfare',   								// category or variation
									    faresAmount,        						// unit price - required
									    '1'               							// quantity - required
									  ]);
						  			
									// TAX
									var taxSKU = strPNR + "/TAX";
						  			 
						  			
						  			_gaq.push(['_addItem',
						  			    prodid,           							// order ID - required
						  			  	taxSKU,           							// SKU/code - required
									    arrData,        							// product name
									    'Tax',   									// category or variation
									    taxesAmount,        						// unit price - required
									    '1'               							// quantity - required
									  ]);
						  			
									// SURCHARGES
									var surchargesSKU = strPNR + "/SURCHARGES";
						  			
						  			
						  			_gaq.push(['_addItem',
						  			    prodid,           							// order ID - required
						  			  	surchargesSKU,           					// SKU/code - required
									    arrData,        							// product name
									    'Surcharges',   							// category or variation
									    surchargesAmount,        					// unit price - required
									    '1'               							// quantity - required
									  ]);
	
						  			// INSURANCE
						  			var insSKU = strPNR + "/INSURANCE/" + strPolicyNo;
									if(insrAmount != '0'){
										
										_gaq.push(['_addItem',
							  			    prodid,           							// order ID - required
							  			  	insSKU,           							// SKU/code - required
										    arrData,        							// product name
										    'Insurance',   								// category or variation
										    insrAmount,        							// unit price - required
										    '1'               							// quantity - required
										  ]);
									}
									
									// TRANSACTION FEES
									if(transactionFeeAmount != null || transactionFeeAmount != '0'){
										var transFeeSKU = strPNR + "/TRANSACTION_FEES";
										
										_gaq.push(['_addItem',
							  			    prodid,           							// order ID - required
							  			  	transFeeSKU,           						// SKU/code - required
										    arrData,        							// product name
										    'Transaction Fees',   						// category or variation
										    transactionFeeAmount,      					// unit price - required
										    '1'               							// quantity - required
										  ]);
									}
	
									for(x in arrGoogleInfo){
										var sku = strPNR + "/" + arrGoogleInfo[x][0];
										var segmentDetails = arrGoogleInfo[x][1];
										var productType = arrGoogleInfo[x][2];
										var amount = arrGoogleInfo[x][3];
										var quantity = arrGoogleInfo[x][4];
										
										if(amount != null || amount != ''){
											_gaq.push(['_addItem',
								  			    prodid,           					// order ID - required
								  			 	sku,           						// SKU/code - required
											    arrData,        					// product name
											    productType,   						// category or variation
											    amount,      						// unit price - required
											    quantity   							// quantity - required
											  ]);
										}
									}
									
									//pageTracker._trackTrans();	
									 _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
								}		
							}
							// Update Tracking Data
							$(document).ready(function() {	
								confirmPageTrack();
		
							});					
							
									
				--></script>
				<%-- Al waseet tracking data --%> 
				<script type='text/javascript'>
					<!--//<![CDATA[
					    var OA_p=location.protocol=='https:'?'https:':'http:';
					    var OA_r=Math.floor(Math.random()*999999);
					    document.write ("<" + "script language='JavaScript' ");
					    document.write ("type='text/javascript' src='"+OA_p);
					    document.write ("//ads.waseet.net/www/delivery/tjs.php");
					    document.write ("?trackerid=1&amp;r="+OA_r+"'><" + "\/script>");
				//]]>-->
				</script>
				<noscript>
					<div id='m3_tracker_1' style='position: absolute; left: 0px; top: 0px; visibility: hidden;'>
					<img src='//ads.waseet.net/www/delivery/ti.php?trackerid=1&amp;UI_Confirm.strPNR=%%UI_CONFIRM.STRPNR_VALUE%%&amp;cb=%%RANDOM_NUMBER%%' width='0' height='0' alt='' />
					</div>
				</noscript>
				
				<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" || sessionScope.sessionDataDTO.carrier == "3O" || sessionScope.sessionDataDTO.carrier == "E5"}'>

			<iframe name="Switch"
				src="//ssl.hurra.com/Trackit?tid=0C3673PPS&sale=<c:out value="${param.price}" escapeXml="false"/>&orderid=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />&currency=<c:out value="${param.cur}" escapeXml="false"/>&s_metric=<c:out value="${param.s_m}" escapeXml="false"/>&s_category=<c:out value="${param.s_c}" escapeXml="false"/>&fv_order_article=<c:out value="${param.fvoa}" escapeXml="false"/>&fv_order_price=<c:out value="${param.price}" escapeXml="false"/>&fv_order_quantity=<c:out value="${param.fpq}" escapeXml="false"/>&departure_date=<c:out value="${param.fddate}" escapeXml="false"/>&return_date=<c:out value="${param.frdate}" escapeXml="false"/>&ifrsw=1"
				marginwidth="1" marginheight="1" height="1" width="1" border="0"
				frameborder="0"></iframe>
				
				<!-- Added as per request on [AARESAA-16880] Tracking pixel IAS on 03/04/2014 all airarbia hubs -->
				<!-- Facebook Conversion Code for Air Arabia_Conversions -->
				<script type="text/javascript">
				var fb_param = {};
				fb_param.pixel_id = '6011375617138';
				fb_param.value = '<c:out value="${param.price}" escapeXml="false"/>';
				fb_param.currency = '<c:out value="${param.cur}" escapeXml="false"/>';
				(function(){
				  var fpw = document.createElement('script');
				  fpw.async = true;
				  fpw.src = '//connect.facebook.net/en_US/fp.js';
				  var ref = document.getElementsByTagName('script')[0];
				  ref.parentNode.insertBefore(fpw, ref);
				})();
				</script>
				<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6011375617138&amp;value=<c:out value="${param.price}" escapeXml="false"/>&amp;currency=<c:out value="${param.cur}" escapeXml="false"/>" /></noscript>
				<!-- Added as per request on [AARESAA-16880] Tracking pixel IAS on 03/04/2014 -->
				
				
				<!-- Added as per request on [AARESAA-16680] Tracking pixel IAS on 03/04/2014 -Start -->
				<!--Start ConversionTrack Code AirArabiaOnlineBooking --> 
				<script> 
				var s,f,c=document.cookie;s=c.indexOf("ed399898069");if(s==-1) {s=c.indexOf("eds399898069")}; 
				if (s>-1) { c=c.substring(s+1);c=c.substring(c.indexOf("=")+1); e=c.indexOf(";");f=c.substring(0,e==-1? c.length:e); 
				document.write('<img src="https://pd.ed10.net/p/0G/RL5PT8X/U1?CEDID='+f+'&m=<c:out value="${param.price}" escapeXml="false"/>&pk=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />" height=1 width=1>');} 
				</script> 
				<!--End ConversionTrack Code AirArabiaOnlineBooking -->
				<!-- Added as per request on [AARESAA-16680] Tracking pixel IAS on 03/04/2014 -End -->

		</c:if>
		
		<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" || sessionScope.sessionDataDTO.carrier == "3O" || sessionScope.sessionDataDTO.carrier == "E5" || sessionScope.sessionDataDTO.carrier == "9P"}'>
					<!-- Added as per request on [AARESAA-12696] Tracking pixel IAS on 16/04/2013 -->
            <script type="text/javascript">
                (function() {
                    try {
                        var viz = document.createElement("script");
                        viz.type = "text/javascript";
                        viz.async = true;
                        viz.src = ("https:" == document.location.protocol ?"https://mea-tags.vizury.com" : "http://mea-tags.vizury.com")+ "/analyze/pixel.php?account_id=VIZVRM3429";

                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(viz, s);
                        viz.onload = function() {
                            try {
                                pixel.parse();
                            } catch (i) {
                            }
                        };
                        viz.onreadystatechange = function() {
                            if (viz.readyState == "complete" || viz.readyState == "loaded") {
                                try {
                                    pixel.parse();
                                } catch (i) {
                                }
                            }
                        };
                    } catch (i) {
                    }
                })();
            </script>
		</c:if>

		
		<c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
					    <%--<script type="text/javascript">
							var baseURL = "https://ssl.vizury.com/analyze/analyze.php?account_id=VIZVRM3429&param=t400&oid=<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />&op=&curr=&src=&dest=&doj=&rd=&ad=&ch=&inf=&section=1&level=1";
							var analyze = document.createElement("iframe");
							analyze.src = baseURL;
							analyze.scrolling = "no";
							analyze.width = 1;
							analyze.height = 1;
							analyze.marginheight = 0;
							analyze.marginwidth = 0;
							analyze.frameborder = 0;
							
							var node = document.getElementsByTagName("script")[0];
							node.parentNode.insertBefore(analyze, node);
						
						</script>--%>
						<!-- Added as per request on [AARESAA-12696] Tracking pixel IAS on 16/04/2013 -->
		 				<!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/19/2013 ****** This should goes last ************-->
					        <script src='https://bsrv.adohana.com/ohana/conversion.js?oh_price=<c:out value="${param.price}" escapeXml="false"/>&oh_id=<c:out value="${sessionScope.sesPNRNO}" escapeXml="false" />&id=268&r=1.0' type="text/javascript"></script>
					    <!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/19/2013 -->
		</c:if>
		</c:if>
		
		<%-- Availability search tracking data --%>
		<c:if test='${ param.pageID == "FLIGHT_SEARCH" }'>
			<script type="text/javascript">
					var UI_AvailabilitySearch = UI_Top.holder().UI_AvailabilitySearch;
					function sendGoogleTrackingDataAS() {
							if (UI_AvailabilitySearch.isModifySegment == true) {
								var mode = "MOD";
							} else {
								var mode = "CRE";			
							}
							if(UI_AvailabilitySearch.returnFlag == "true") {
								var way = "RT";
							} else {
								var way = "OW";			
							}	
							var strAnalyticPage = "/funnel_G1/FlightSelect/" + mode + "/" + carrier + "/" + UI_AvailabilitySearch.fromAirport + UI_AvailabilitySearch.toAirport + "/" + way;
							if(strAnalyticEnable == "true") {			
								//pageTracker._trackPageview(strAnalyticPage);
								_gaq.push(['_trackPageview', strAnalyticPage ]);
							}
					}
					$(document).ready(function() {	
						sendGoogleTrackingDataAS();
					});					
			</script>
			
			<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" || sessionScope.sessionDataDTO.carrier == "3O" || sessionScope.sessionDataDTO.carrier == "E5"}'>
					<!-- Added as per request on [AARESAA-16880] Tracking pixel IAS on 03/04/2014 all airarbia hubs -->
					<!-- Facebook Conversion Code for Air Arabia_Leads --> 
					<script type="text/javascript"> 
					var fb_param = {}; 
					fb_param.pixel_id = '6011375509138'; 
					fb_param.value = '0.00'; 
					fb_param.currency = '<c:out value="${param.cur}" escapeXml="false"/>'; 
					(function(){ 
					  var fpw = document.createElement('script'); 
					  fpw.async = true; 
					  fpw.src = '//connect.facebook.net/en_US/fp.js'; 
					  var ref = document.getElementsByTagName('script')[0]; 
					  ref.parentNode.insertBefore(fpw, ref); 
					})(); 
					</script> 
					<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6011375509138&amp;value=0&amp;currency=<c:out value="${param.cur}" escapeXml="false"/>" /></noscript>
			</c:if>

		</c:if>
		
		<%--TODO: Pax page tracking data --%>
		<c:if test='${ param.pageID == "PAX"}'>
			<c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
			
			</c:if>

            <c:if test='${sessionScope.sessionDataDTO.carrier == "C7"}'>
                <script type="text/javascript">

                    function changeDateFormat(date){
                        var dt='';
                        if(date){
                            var temp =date.split('/');
                            dt = temp[2]+'-'+temp[1]+'-'+temp[0];
                        }
                        return dt;

                    }
                    var google_tag_params = {};
                    google_tag_params.flight_originid= '<c:out value="${param.src}" escapeXml="false"/>';
                    google_tag_params.flight_destid= '<c:out value="${param.dest}" escapeXml="false"/>';
                    google_tag_params.flight_startdate=changeDateFormat('<c:out value="${param.dt}" escapeXml="false"/>');
                    google_tag_params.flight_enddate=changeDateFormat('<c:out value="${param.returnDate}" escapeXml="false"/>');
                    google_tag_params.flight_pagetype= 'home';
                    google_tag_params.flight_totalvalue= '<c:out value="${param.price}" escapeXml="false"/>';

                </script>
                <script type="text/javascript">
                    /* <![CDATA[ */
                    var google_conversion_id = 968520397;
                    var google_custom_params = window.google_tag_params;
                    var google_remarketing_only = true;
                    /* ]]> */

                </script>
                <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                </script>
                <noscript>
                    <div style="display:inline;">
                        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/968520397/?value=0&amp;guid=ON&amp;script=0"/>
                    </div>
                </noscript>

            </c:if>
		</c:if>
		<!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/25/2013 -->
        <!-- tag were modified as requiest of Ariarabia on  May 26, 2015 -->
        <c:if test='${sessionScope.sessionDataDTO.carrier == "G9" || sessionScope.sessionDataDTO.carrier == "3O" || sessionScope.sessionDataDTO.carrier == "E5" || sessionScope.sessionDataDTO.carrier == "9P"}'>
        	  <script type="text/javascript">
                (function() {
                    try {
                        var viz = document.createElement("script");
                        viz.type = "text/javascript";
                        viz.async = true;
                        viz.src = ("https:" == document.location.protocol ?"https://mea-tags.vizury.com" : "http://mea-tags.vizury.com")+ "/analyze/pixel.php?account_id=VIZVRM3429";

                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(viz, s);
                        viz.onload = function() {
                            try {
                                pixel.parse();
                            } catch (i) {
                            }
                        };
                        viz.onreadystatechange = function() {
                            if (viz.readyState == "complete" || viz.readyState == "loaded") {
                                try {
                                    pixel.parse();
                                } catch (i) {
                                }
                            }
                        };
                    } catch (i) {
                    }
                })();
            </script>
        
        </c:if>
        
 		<c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
 
            <script type='text/javascript'>
				var _spapi = _spapi || [];
				_spapi.push(['_partner', 'airarabia']);
				(
				function()
				{
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'airarabia.api.sociaplus.com/partner.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
				}
				)();
			</script>
		
		</c:if>
		 <!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/25/2013 -->
	</body>
</html>