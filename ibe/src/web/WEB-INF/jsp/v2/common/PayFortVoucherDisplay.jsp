<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/cacheClear.jsp'%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>PayFort Pay@Store Voucher Display</title>
<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/>	
<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	


<script type="text/javascript">

</script>
</head>
<body>
	<script type="text/javascript">
	var voucher_number = "<c:out value="${requestScope.voucherID}" escapeXml="false" />";
	var request_id = "<c:out value="${requestScope.requestID}" escapeXml="false" />";
	var script_URL = "<c:out value="${requestScope.payAtstoreScriptUrl}" escapeXml="false" />";
	var lang = 'en';
	var _rnd = Math.random() ;
	var _proto = (("https:" == document.location.protocol) ? "https:" : "http:");
	document.write(unescape("%3Cscript src='" + _proto + script_URL +"?" + _rnd
			+ "' type='text/javascript'%3E%3C/script%3E"));
	</script>
</body>
</html>