<div id="divSummaryPane">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<!-- Booking Summary -->
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class='lpaneTopL'></td>
				<td class='lpaneTopM'><label id="lblBookingSummary"
					class='fntBold gridHDFont'>Booking Summary</label></td>
				<td class='lpaneTopR'></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class='lpaneMidL'></td>
				<td class='lpaneMidM'>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class='pnlTop'></td>
					</tr>
					<tr>
						<td class='pnlBottom'>
						<table width="100%" border="0" cellspacing="1" cellpadding="1">
							<tr>
								<td><label id="lblFlightDetails" class='fntBold hdFontColor'>Flight
								Details</label></td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
							<tr>
								<td><label id="lblReturn" /></td>
							</tr>
							<tr>
								<td><label id="adultCount" /></td>
							</tr>
							<tr>
								<td><label id="childCount" /></td>
							</tr>
							<tr>
								<td><label id="infantCount" /></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td class='rowGap'></td>
					</tr>
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							id="flightTemplate">
							<tr>
								<td class='pnlTop pnlWidthLeft'></td>
								<td class='pnlTopR'></td>
							</tr>
							<tr>
								<td class='pnlBottom' colspan='2'>
								<table width='100%' border='0' cellpadding='1' cellspacing='1'>
									<tr>
										<td colspan="3"><label id="segmentCode"
											class='fntBold hdFontColor' /></td>
									</tr>
									<tr>
										<td width="50%"><label id="lblflightNo">Flight No</label></td>
										<td width="1%"><label>:</label></td>
										<td width="49%"><label id="flightNumber" /></td>
									</tr>
									<tr>
										<td><label id="lbldepatureDate">Date</label></td>
										<td><label>:</label></td>
										<td><label id="depatureDate" /></td>
									</tr>
									<tr>
										<td><label id="lbldepatureTime">Departure</label></td>
										<td><label>:</label></td>
										<td><label id="depatureTime" /></td>
									</tr>
									<tr>
										<td><label id="lblarrivalTime">Arrival</label></td>
										<td><label>:</label></td>
										<td><label id="arrivalTime" /></td>
									</tr>
									<tr>
										<td colspan="3" ></td>
									</tr>
									<tr>
										<td colspan="3" ><span name="anciDetails" ></span></td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				<td class='lpaneMidR'></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class='lpaneBotL'></td>
				<td class='lpaneBotM'></td>
				<td class='lpaneBotR'></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class='rowGap'></td>
	</tr>

	<!-- Payment Summary -->
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class='lpaneTopL'></td>
				<td class='lpaneTopM'><label id="lblPaymentSummary"
					class='fntBold gridHDFont'>Payment Summary</label></td>
				<td class='lpaneTopR'></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class='lpaneMidL'></td>
				<td class='lpaneMidM'>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							id="flightTemplate">
							<tr>
								<td class='pnlTop pnlWidthLeft'></td>
								<td class='pnlTopR'></td>
							</tr>
							<tr>
								<td class='pnlBottom' colspan='2'>
								<table width='100%' border='0' cellpadding='1' cellspacing='1'>
									<tr id='trAirFare' style="display:none;">
										<td width="50%"><label id="lblAirFare">Air Fare</label></td>
										<td width="2%" class="hdFontColor fntBold">:</td>
										<td width="10%" align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td width="38%" align="right"><label id='valAirFare' class='fntDefault txtBold'></label></td>
									</tr>
									<tr id='trCharges' style="display:none;">
										<td width="50%"><label id="lblCharges">Tax &amp;
										Surcharges</label></td>
										<td width="2%" class="hdFontColor fntBold">:</td>
										<td width="10%" align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td width="38%" align="right"><label id='valCharges' class='fntDefault txtBold'></label></td>
									</tr>
									<tr id='trSeatCharges'>
										<td><label id='lblSeatSelection' style="display:none;">Seat Selection</label></td>
										<td class="hdFontColor fntBold">:</td>
										<td align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td align="right"><label id='valSeatCharges' class='fntDefault txtBold'></label></td>
									</tr>																				
									<tr id='trInsuranceCharges' style="display:none;">
										<td><label id='lblTravelSecure' >Travel Insurance</label></td>
										<td class="hdFontColor fntBold">:</td>
										<td align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td align="right"><label id='valInsuranceCharges' class='fntDefault txtBold'></label></td>
									</tr>
									<tr id='trMealCharges' style="display:none;">
										<td><label id='lblMealSelection' >Meal Selection</label></td>
										<td class="hdFontColor fntBold">:</td>
										<td align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td align="right"><label id='valMealCharges' class='fntDefault txtBold'></label></td>
									</tr>									
									<tr id='trTxnFee' style="display:none;">
										<td width="50%"><label id="lblTNXFee">Transaction Fees</label></td>
										<td width="2%" class="hdFontColor fntBold">:</td>	
										<td width="10%" align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td width="38%" align="right"><label id='valTxnFee' class='fntDefault txtBold'></label></td>
									</tr>
									<tr id='trUtilCredit' style="display:none;">
										<td width="50%"><label id="lblUtilizedCredit">Utilized Credit</label></td>
										<td width="2%" class="hdFontColor fntBold">:</td>	
										<td width="10%" align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td width="38%" align="right"><label id='valUtilCredit' class='fntDefault txtBold'></label></td>
									</tr>
									<tr>
										<td colspan="4" height="20px"></td>
									</tr>
									<tr>
										<td width="50%"><label id="lblTotal" class='fntBold'>Total</label></td>
										<td width="2%" class="hdFontColor fntBold">:</td>
										<td width="10%" align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
										<td width="38%" align="right"><label id='valTotal' class='fntDefault fntBold hdFontColor' ></label></td>
									</tr>
									<tr style="display: none;" id="trSelectedCurr">
										<td width="50%"></td>
										<td width="2%" class="hdFontColor fntBold hdFontColor">:</td>
										<td width="10%" align="left"><label name='valSelectedCurrCode' class='fntDefault txtBold'></label></td>
										<td width="38%" align="right"><label id='valSelectedTotal' class='fntDefault txtBold hdFontColor'></label></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				<td class='lpaneMidR'></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class='lpaneBotL'></td>
				<td class='lpaneBotM'></td>
				<td class='lpaneBotR'></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>