<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp"%>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/interlinePgHD.jsp'%>
<link rel="stylesheet" type="text/css" href="../css/jquery.calendarview_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" />
<link rel="stylesheet" type="text/css" href="../css/remove-jquery_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" />
<link rel="stylesheet" type="text/css" href="../css/jquery.bt_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" />
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/common/jquery.combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/common/jquery.summaryPanel.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/common/jquery.stiky.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>


<script type="text/javascript">
	var strReqParam = '<c:out value="${requestScope.sysReqParamAA}" escapeXml="false" />';
	var GLOBALS = <c:out value="${requestScope.systemDefaultParam}" escapeXml="false" />;
	var fareType_IMG_PATH = '<c:out value="${requestScope.sysImagePath}" escapeXml="false" />';
	var currencyList = [['IRR','Iran Riyal'],['USD','US Dollar']];
	var paxJason = null;
	//TODO Refactor-don't add more
	var objCWindow = "";
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();
	var dtSysDate = new Date(strSysDate.substr(6, 4), (Number(strSysDate
			.substr(3, 2)) - 1), strSysDate.substr(0, 2));
	var strToDate = strSysDate;
	if (UI_Top.holder().GLOBALS.currentDate != null && UI_Top.holder().GLOBALS.currentDate == "") {
		var date = UI_Top.holder().GLOBALS.currentDate;
		var dtTempSysDate = new Date(date.substr(6, 4), (Number(date.substr(3,
				2)) - 1), date.substr(0, 2));
		strSysDate = DateToString(addDays(dtTempSysDate, 0));
		dtSysDate = new Date(strSysDate.substr(6, 4), (Number(strSysDate
				.substr(3, 2)) - 1), strSysDate.substr(0, 2));
		strToDate = dtSysDate;
	}
	var bookingStepsArray = [ "Select a Flight", "Passenger details",
			"Personalise ", "Payment", "Print & Fly" ];

	var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
	<!--
	//Design pesific labels are set ti the js variables and overrite the originals
	var btnContinuelbl = '<fmt:message key="msg.common.button.continue"/>';
	var btnContinuelblPass = '<fmt:message key="msg.common.button.continue.passenger"/>';
	var flexiLblModification ='<fmt:message key="msg.flext.label.modofication"/>';
	var flexiLblCancelation ='<fmt:message key="msg.flext.label.cancelation"/>';
	var flexiLblCondition ='<fmt:message key="msg.flext.label.confition"/>';
	
	var ondListStr = null;
	var isDrySearch = null;
	var paxCountStr = null;
	-->
</script>

    <c:if test="${not empty(requestScope.paxJason)}">
	    <script type="text/javascript">
	       paxJason = <c:out value="${requestScope.paxJason}" escapeXml="false" />;
	    </script>	
    </c:if>	
    
    <c:if test="${not empty(requestScope.ondListString)}">
	    <script type="text/javascript">
	    	ondListStr = <c:out value="${requestScope.ondListString}" escapeXml="false" />;
	    </script>	
    </c:if>	
    
    <c:if test="${not empty(requestScope.drySearch)}">
	    <script type="text/javascript">
	    isDrySearch = <c:out value="${requestScope.drySearch}" escapeXml="false" />;
	    </script>	
    </c:if>
    <c:if test="${not empty(requestScope.paxCountStr)}">
	    <script type="text/javascript">	    	
	    	paxCountStr = "<c:out value="${requestScope.paxCountStr}" escapeXml="false" />";
	    </script>	
    </c:if>        	
	<style>
		td.peocess-text{
			width:144px! important;
		}
	</style>
</head>
<body>
<c:import url="../common/pageLoading.jsp" />
<form id="operCarrierFrm" name="operCarrierFrm" action=""
 method="post" >
 <input type="hidden" name="ondListString" id="ondListString" value=""/>
 <input type="hidden" name="hdnParamData" id="hdnParamData" value="EN^MC^AED"/>
 <input type="hidden" name="drySearch" id="drySearch" value="true"/>
 <input type="hidden" name="paxCountStr" id="paxCountStr" value=""/>
 <input type="hidden" name="hdnPromoCode" id="hdnPromoCode" value=""/> 
</form>
<div id="divLoadBg" style="display: none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	id="PgFares">
	<tr>
		<td align='center' class="outerPageBackGround">
		<table style='width: 940px;' border='0' cellpadding='0'
			cellspacing='0' align='center' class='PageBackGround'>
			<!-- Top Banner -->
			<c:import url="../../../../ext_html/header.jsp" />
			<!-- Registered user menu place holder -->
			<%@ include file="../common/includeRegUserBar.jsp"%>
			<!-- Content holder -->
			<tr>
				<td colspan="2" class="mainbody">
				<div id="sortable-ul">
				<div class="sortable-child"><!-- Left Column -->
				<div style="background: #fff;display:none" class="floater" >
					<div id="divSummaryPane">
						<div class="bookingSummary"></div>
						<div class="paymentSummary"></div>
					</div>
				</div>
				&nbsp;</div>
				<div class="sortable-child" style="float:left"><!-- Right Column -->
				<div class="rightColumn">
				<div class="page-body" style="width: 100%;">
				<div id="SearchPane">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
						<td valign="top" align="center">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="rowSingleGap"></td>
								</tr>
								<tr>
									<td class="paddingCalss">
										<label id="lblEnterTravel">Enter your travel details and select your search preferences below. Click Find Flights when you are done.</label>
									</td>
								</tr>
								<tr>
									<td class="rowSingleGap"></td>
								</tr>
								<tr>
									<td valign="top" align="center">
										<c:import url="../../../../ext_html/inc_mcFlightSearch.jsp" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</div>
				<div id="resultsPane">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr >
						<td valign="top" align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="alignLeft">
								<div class="" style="width: 100%;" class="stepsContainer">
								<c:import url="../../../../ext_html/booking_process_v3.jsp" />
								</div>
								</td>
							</tr>
							<tr>
								<td class="alignLeft paddingCalss"><label
									id="lblSelectFlights" class="hdFontColor fntBold">Select Flights</label></td>
							</tr>
							<tr>
								<td class="rowSingleGap"></td>
							</tr>
												
							<tr id="browserMsgTr">
								<td class="alignLeft paddingCalss">
									<div class="spMsg" id="browserMsgTxt"></div>
									<br/>
								</td>
							</tr>
							
							<tr style="display: none">
								<td class="alignLeft paddingCalss"><font> <label
									id="lblSelectFlightMsg"> </label> </font></td>
							</tr>
							<tr style="display: none">
								<td class='rowGap'></td>
							</tr>
							<tr>
								<td class="alignLeft paddingCalss">
									<div class="spMsg" id="searchPageAirportMsg"></div>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				<div id="FlightDetailsPanel">
				<%-- Flight Details --%>
					<div id="flightTempl" style="display:none">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class='pnlTop pnlWidth'></td>
							<td class='pnlTopR'></td>
						</tr>	
						<tr>
							<td class='pnlBottom'  colspan='2' valign="top">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class="alignLeft">																		
										<label id="lblFlightOptions" class="fntBold hdFontColor paddingL5">Flight Options for: <span id="hOND"></span></label>
									</td>
								</tr>
								<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="20%" class="alignLeft">
												<button id="ln_Prev" class="Button ButtonMedium"> &lt; Previous Day </button>
											</td>
											<td align="center"><label><span id="hDepDateP" class="date-disp"></span><span id="hDepDate" class="date-hid" style="display:none"></span></label></td>
											<td width="20%" class="alignRight">
												<button id="ln_Next" class="Button ButtonMedium"> Next Day &gt; </button>
											</td>
										</tr>
									</table>
								</td>
								</tr>
								<tr>
									<td id="flightTable">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="GridTable" >
										<thead>
											<tr>
												<td colspan="7" class='gridHD bdBottom bdLeft bdRight paddingL5'>
													<label class='gridHDFont fntBold' id="optTxt"></label>
												</td>
											</tr> 
											<tr class="tbHead">
												<td align='center' width='15%' class='gridHD bdLeft bdRight'><label id="lblFlightNo" class='gridHDFont fntBold'></label></td>
												<td align="center" width='15%' class='gridHD bdRight'><label id="lblDepature" class='gridHDFont fntBold'></label></td>
												<td align="center" width='15%' class='gridHD bdRight'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
												<td align='center' width='15%' class='gridHD bdRight'><label id="lblStops" class='gridHDFont fntBold'></label></td>
											</tr>
										</thead>
										<tbody>
											<tr class="tbbody">
												<td class='rowColor bdRight bdBottom bdLeft' align='center'><label  id="flightNumber"></label></td>
												<td class='rowColor bdRight bdBottom' align='center'><label id="departureDate" class="date-disp"></label>
												<label id="departureDateLong" class="date-hid" style="display:none"></label>
												<label id="departureTime" class="date-disp"></label></td>
												<td class='rowColor bdRight bdBottom' align='center'><label id="arrivalDate" class="date-disp"></label>
												<label id="arrivalDateLong" class="date-hid" style="display:none"></label>
												<label id="arrivalTime" class="date-disp"></label></td>
												<td class='rowColor bdRight bdBottom' align='center'><label id="stops"></label></td>
											</tr>
										</tbody>
          								</table>
									</td>												
								</tr>
							</table>
						</td>
						</tr>
						<tr class="trAddFlight">
							<td colspan='2'>
								<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
									<td width="80%"><label id="lblLocalTimeAirport"></label></td>
									<td class="alignRight paddingCalss">
										<button id="btnCont" class="Button"> Add Flight </button>
									</td>
								</tr></table>
							</td>
						</tr>
					</table>
					</div>
				</div>
				<div id="farequoteArea">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class='rowGap'></td>
						</tr>
						<tr><td>
							<div id="shoping_Cart" class='pnlBottom' style='height:auto'>
								<div><label class='fntBold hdFontColor paddingL5' id="lblSeletedFlight">Selected Flights List</label></div>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="GridTable" >
									<thead>
										<tr>
											<td align='center' width='25%' class='gridHD bdLeft bdRight'><label id="lblOnd" class='gridHDFont fntBold'></label></td>
											<td align='center' width='12%' class='gridHD bdRight'><label id="lblFlightNo" class='gridHDFont fntBold'></label></td>
											<td align='center' width='8%' class='gridHD bdRight'><label id="lblCabin" class='gridHDFont fntBold'>Cabin</label></td>
											<td align="center" width='15%' class='gridHD bdRight'><label id="lblDepature" class='gridHDFont fntBold'></label></td>
											<td align="center" width='15%' class='gridHD bdRight'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
											<td align='center' width='10%' class='gridHD bdRight'><label id="lblStops" class='gridHDFont fntBold'>Stops</label></td>
											<td align='center' width='15%' class='gridHD bdRight'></td>
										</tr>
									</thead>
									<tbody id="SelectedItemsFL">
										<tr>
										<td colspan="7">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td class='rowColor bdRight bdBottom bdLeft' width='25%' align='center'><label id="segments"></label></td>
												<td class='rowColor bdRight bdBottom' width='12%' align='center'><label id="flightNumber"></label></td>
												<td class='rowColor bdRight bdBottom' width='8%' align='center'><label id="cabinClass"></label></td>
												<td class='rowColor bdRight bdBottom' width='15%' align='center'><label id="departureDate" class="date-disp"></label>
												<label id="departureDateLong" class="date-hid" style="display:none"></label>
												<label id="departureTime" class="date-disp"></label></td>
												<td class='rowColor bdRight bdBottom' width='15%' align='center'><label id="arrivalDate" class="date-disp"></label>
												<label id="arrivalDateLong" class="date-hid" style="display:none"></label>
												<label id="arrivalTime" class="date-disp"></label></td>
												<td class='rowColor bdRight bdBottom' width='10%' align='center'><label id="stops"></label></td>
												<td class='rowColor bdRight bdBottom' width='15%' align='center' ><label id="moD"></label></td>
											</tr>
											<tr>
												<td colspan="7">
													<div class="clsLcc" id="ondLogicalCC"></div>
												</td>
											</tr>
										</table>
										</td>
										</tr>
									</tbody>
          						</table>
							</div>
						</td></tr>
						<tr>
							<td class="alignRight paddingCalss" ><button id="btnfareQuate" class="Button ButtonLarge">Complete Flight Selection</button></td>
						</tr>
					</table>
				</div>
				<div id="trPriceBDPannel" style="display:none">
					<%-- Price Break Down --%>
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class='pnlTop pnlWidth'></td>
										<td class='pnlTopR'></td>
									</tr>	
									<tr>
										<td class='pnlBottomTall'  colspan='2' valign="top">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
												<tr>
													<td class="alignLeft">																		
														<label id="lblPriceBreakDown" class="fntBold hdFontColor paddingL5"></label>&nbsp;<label id="lblCurrencySupportMessage"></label>
													</td>
												</tr>
												<tr>
													<td>
														<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">	
															<tr>		
																<td align="center" width="200px" class="gridHD">
																	<label id="lblOnd" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" class="gridHD">
																	<label id="lblPassengerType" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" width="100" class="gridHD">
																	<label id="lblFare" class='gridHDFont fntBold'></label>																							
																</td>		
																<td align="center" width="100" class="gridHD">
																	<label id="lblCharges" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" width="50" class="gridHD">
																	<label id="lblNoOfPax" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" width="100" class="gridHD">
																	<label id="lblTotal" class='gridHDFont fntBold'></label>																						
																</td>	
															</tr>	
															<tr id="priceBreakDownTemplate">		
																<td rowspan="1" width="200" class="GridItems alignLeft">
																	<label id="ond">	</label>
																</td>
																<td class="GridItems" align='center'>
																	<label id="paxType"></label>
																</td>		
																<td class="GridItems alignRight">
																	<label id="fare"></label>
																</td>		
																<td class="GridItems alignRight">
																	<label id="sur"></label>
																</td>		
																<td align="center" class="GridItems">
																	<label id="noPax"></label>
																</td>		
																<td class="GridItems alignRight">
																	<label id="total"></label>
																</td>	
															</tr>																		
																																																																					
													</table>
													<table cellspacing="0" cellpadding="2" border="0" width="100%" class="GridTable">
														<tr style="display: none;" id="promoDiscountPanel">
															<td colspan="3" style="padding:0px">
																<div id="promoDiscountPanel_OB" class="floatleft">
																	<table style="width: 100% ">
																		<tr>
																			<td class="alignRight"><label id="lblPromoDiscount" class="fntBold gridHDFont lblPromoDiscount"></label></td>
																			<td width="115" class="alignRight"><label id="promoDiscountAmount" class="fntBold gridHDFont promoDiscountAmount"></label></td>
																		</tr>
																	</table>
																</div>
																<div id="promoDiscountPanel_IB" class="floatright">
																	<table style="width: 100% ">
																		<tr>
																			<td class="alignRight"><label id="lblPromoDiscount" class="fntBold gridHDFont lblPromoDiscount"></label></td>
																			<td width="115" class="alignRight"><label id="promoDiscountAmount" class="fntBold gridHDFont promoDiscountAmount"></label></td>
																		</tr>
																	</table>
																</div>
															</td>
														</tr>
														<tr>		
																<td width="125px" class="GridHighlight alignLeft">
																	<a href="javascript:void(0);">
																		<u><label class="fntLink hdFontColor" id="lblFareRules"></label></u>
																	</a>																						
																</td>																																										
																<td  class="GridHighlight alignRight" style="border-right:1px solid #e4e4e4">
																	<label id="lblTotal" class="fntBold uppercase"></label>
																</td>		
																<td  width="100" class="GridHighlight alignRight">
																	<label class="fntBold" id="totalAmount"></label>
																</td>	
														</tr>
														<tr style="display: none;" id="selectedTotalPanel">		
																<td width="125px" class="GridHighlight alignLeft">																																												
																</td>			
																
																<td   class="GridHighlight alignRight" style="border-right:1px solid #e4e4e4">
																	<label id="lblTotal" class="fntBold uppercase"></label>
																</td>		
																<td  width="100" class="GridHighlight alignRight">
																	<label class="fntBold" id="totalAmountSel"></label>
																</td>	
														</tr>
														<tr style="display: none;" id="promoDiscountPanelCredit">
															<td colspan="2" class="alignRight"><label id="lblPromoDiscount" class="fntBold gridHDFont lblPromoDiscount"></label></td>
															<td width="115" class="alignRight"><label id="promoDiscountAmount" class="fntBold gridHDFont promoDiscountAmount"></label></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>	
											
											
											<%--tax and surcharge bd data --%>

											
											<tr id="taxBDDisplayLinkRow" class="taxBDDisplayLinkRows">
													<td class="alignLeft">	
														<a id="taxBDDisplayLink" href="javascript:void(0);">																	
															<u><label id="lblTaxBreakDown" class="fntLink hdFontColor"></label></u>
														</a>
													</td>
											</tr>
											
											<tr class="taxBDDisplayRows">
													<td>
														<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">	
															<tr>		
																<td align="center" class="gridHD">
																	<label id="lblTaxBDApp" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" class="gridHD">
																	<label id="lblTaxBDOpCarrier" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" width="150" class="gridHD">
																	<label id="lblTaxBDCode" class='gridHDFont fntBold'></label>																							
																</td>		
																<td align="center" width="100" class="gridHD">
																	<label id="lblTaxBDCharge" class='gridHDFont fntBold'></label>																						
																</td>	
															</tr>	
															<tr id="taxBreakDownTemplate">		
																<td rowspan="1" align="center" class="GridItems">
																	<label id="applicableToDisplay">	</label>
																</td>
																<td class="GridItems" align="center">
																	<label id="carrierCode"></label>
																</td>		
																<td align="center" class="GridItems">
																	<label id="taxCode"></label>
																</td>		
																<td align="right" class="GridItems">
																	<label id="amount"></label>
																</td>	
															</tr>																																					
														</table>																		
													</td>
											</tr>
											
											
											
											<tr class="surchargeBDDisplayRows">
												<td colspan='2' class='rowGap'></td>
											</tr>
											
											<tr id="surchargeBDDisplayLinkRow" class="surchargeBDDisplayLinkRows">
													<td class="alignLeft">																							
														<a id="surchargeBDDisplayLink" href="javascript:void(0);">																	
															<u><label id="lblSurchargeBreakDown" class="fntLink hdFontColor"></label></u>
														</a>	
													</td>
											</tr>
											
											<tr class="surchargeBDDisplayRows">
													<td>
														<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">	
															<tr>		
																<td align="center" class="gridHD">
																	<label id="lblSurchargeBDApp" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" class="gridHD">
																	<label id="lblSurchargeBDOpCarrier" class='gridHDFont fntBold'></label>																						
																</td>		
																<td align="center" width="150" class="gridHD">
																	<label id="lblSurchargeBDCode" class='gridHDFont fntBold'></label>																							
																</td>		
																<td align="center" width="100" class="gridHD">
																	<label id="lblSurchargeBDCharge" class='gridHDFont fntBold'></label>																						
																</td>	
															</tr>	
															<tr id="surchargeBreakDownTemplate">		
																<td rowspan="1" align="center" class="GridItems">
																	<label id="applicableToDisplay"></label>
																</td>
																<td class="GridItems" align="center">
																	<label id="carrierCode"></label>
																</td>		
																<td align="center" class="GridItems">
																	<label id="surchargeCode"></label>
																</td>		
																<td align="right" class="GridItems">
																	<label id="amount"></label>
																</td>	
															</tr>																																						
													</table>																		
												</td>
											</tr>

											</table>
										</td>
									</tr>
									<tr>
										<td colspan='2' class='rowGap'></td>
									</tr>													
								</table>
							</td>
						</tr>										
							
					</table>
			</div>

				<%--Modify Balance Summary  --%> 
				<c:if test="${param.modifySegment == true}">
					<c:import url="../modifyRes/modifySegmentBalance.jsp"></c:import>
				</c:if> <%-- Flexi Message --%>
				<div id="divFlexiMessage" style="display: none;">
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class='rowGap'></td>
					</tr>
					<tr>
						<td class="alignLeft paddingCalss"><font
							class="fntEnglish fntBold">Remaining Flexibilities: <span
							id="spnFlexibilities" style="color: red;">No more
						flexibilities available for the cancelled segment</span></font></td>
					</tr>
					<tr>
						<td class='rowGap'></td>
					</tr>
				</table>
				</div>
				
				<%-- Onhold Booking State Message --%>
				
				               <div id="onholdBookingStateMessage" style="display: none;">
				                 <table width='100%' border='0' cellpadding='0' cellspacing='0'>
					               <tr>
						            <td class='rowGap'></td>
					                </tr>
					                <tr>
						              <td class="alignLeft paddingCalss">
						                <div class="alignLeft spMsg">
							             <label id="lblOnHoldState">
								          
							             </label>
						               </div>
						              </td>
					                </tr>
					               <tr>
						             <td class='rowGap'></td>
					               </tr>
				                </table>
				              </div>		
				
				<%-- Importance Notice --%>
				<div id="trTermsNCond">
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="alignLeft paddingCalss"><label
									class="fntBold hdFontColor paddingL5" id="lblImportantMsg"></label>
								</td>
							</tr>
							<tr>
								<td colspan='2' class='rowGap'></td>
							</tr>
							<tr>
								<td class="paddingR5"><label id="termsNCond"></label></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>

				</div>
				<div id="trAccept">
				<table>
					<tr>
						<td class="alignRight"><label id="lblAcceptMsg1"></label>&nbsp;
						<a href="#" id="linkTerms"><u> <label class="hdFontColor"
							id="lblAcceptMsg2"></label></u> </a>
						<label id="lblAcceptMsg3"></label>	
						</td>
						<td width="2%"><label><input type="checkbox"
							title="Click here if you agree with terms and conditions"
							name="chkTerms" id="chkTerms" /></label></td>
					</tr>
				</table>
				</div>
				<%-- TODO According Application parameter load from separate jsp --%>
				<div id="trPanelImageCaptcha" style="display: none">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<div>
						<table cellpadding="2" cellspacing="0" width="100%">
							<tr>
								<td>&nbsp;</td>
								<td><img src="../jcaptcha.jpg" align="top"
									style="cursor: pointer;" name="imgCPT" id="imgCPT" /> <br />
								<label id="lblChangeImage">Please Click on the image to
								change</label></td>
								<td>&nbsp; : &nbsp; <input type="text"
									style="width: 220px;" name="captcha" id="txtCaptcha" /> <font
									class="mandatory"> *</font></td>
							</tr>
						</table>
						</div>
						</td>
					</tr>
				</table>
				</div>
				<div class='rowGap'></div>
				<div class="buttonset"><%-- Button --%>

				<table border='0' cellpadding='0' cellspacing='0' width="100%">
					<tr>
						<td class="alignLeft"><%--<input type="button" id="btnSOver"  class="Button <fmt:message key='msg.res.startover.class'/>"/>
												--%> 
						<u:hButton name="btnSOver" id="btnSOver" value="Start Over" tabIndex="20" cssClass="blackStOver"/>
						</td>
						<td style="width: 10px;">&nbsp;</td>
						<td class="alignRight"><%--<input type="button" id="btnContinue"  class="Button" />--%>
						<u:hButton name="btnContinue" id="btnContinue" value="Continue" tabIndex="20" cssClass="redContinue" /></td>
					</tr>
				</table>

				</div>
				<div class='rowGap'></div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="alignLeft paddingCalss"><label id="lblReCalMsg"></label></td>	
				</tr>
				</table>	
				</div>
				</div></div>
				</div>
				</div>
				</td>
			</tr>

			<%-- Bottom AA Logo --%>
			<tr>
				<td class='appLogo' colspan="2"></td>
			</tr>
			<c:import url="../../../../ext_html/cfooter.jsp" />
		</table>
		</td>
	</tr>
</table>
<div class="loadingContainer">
<div class="newPogressLoading">
<table width="100%" border="0" height="100%" style="background: #fff">
	<tr>
		<td valign="middle" align="center"><img
			src="../images/Loading_no_cache.gif" alt="loading.." /></td>
	</tr>
</table>
</div>
</div>

<form action="" id="frmFare" method="post">
	<%@ include file='../common/iBECommonParam.jsp'%>
	<div id="searchSubmitParams" name="searchSubmitParams">	
		<input type="hidden" id="resFareQuoteLogicalCCSelection" name="searchParams.fareQuoteLogicalCCSelection"/>
		<input type="hidden" id="resOndQuoteFlexi" name="searchParams.ondQuoteFlexiStr"/>
		<input type="hidden" id="resOndAvailbleFlexiStr" name="searchParams.ondAvailbleFlexiStr"/>
		<input type="hidden" id="resflightSearchOndFlexiSelection" name="searchParams.flightSearchOndFlexiSelectionStr"/>
		<input type="hidden" id="resOndSelectedFlexiStr" name="searchParams.ondSelectedFlexiStr"/>
		<input type="hidden" id="resOndwiseFlexiAvilableForAnci" name="searchParams.resOndwiseFlexiAvilableForAnci"/>
		<input type="hidden" id="resOndwiseFlexiChargesForAnci" name="searchParams.resOndwiseFlexiChargesForAnci"/>
		<input type="hidden" id="resOndBundleFareStr" name="searchParams.preferredBundledFares"/>
		<input type="hidden" id="resOndBookingClassStr" name="searchParams.preferredBookingCodes"/>
		<input type="hidden" id="resOndSegBookingClassStr" name="searchParams.ondSegBookingClassStr"/>
		<input type="hidden" id="resAvailableBundleFareLCClassStr" 	name="searchParams.availableBundleFareLCClass"/>
		<input type="hidden" 	id="resPointOfSale" name="searchParams.pointOfSale" value='<c:out value="${commonParams.pointOfSale}" escapeXml="false"/>'/>
		
		<input type="hidden" id="resFareType" name="searchParams.fareType" value="ALL"/>
		<input type="hidden" id="resSearchSystem" name="searchParams.searchSystem" value="AA"/>
		<input type="hidden" id="resBookingType" name="searchParams.bookingType" value="NORMAL"/>
			
		<input type="hidden" id="resSelectedCurrency" name="searchParams.selectedCurrency"  value='<c:out value="${searchParams.selectedCurrency}" escapeXml="false"/>' />
		<input type="hidden" id="resAdultCount" name="searchParams.adultCount"  value="" />
		<input type="hidden" id="resChildCount" name="searchParams.childCount"  value="" />
		<input type="hidden" id="resInfantCount" name="searchParams.infantCount"  value="" />
		
		<input type="hidden" id="resPromoCode" name="searchParams.promoCode"/>
		<input type="hidden" id="respromotionInfo" name="promoInfoJson"/>
		<input type="hidden" id="resOndListStr" name="searchParams.ondListString" value="" />
		
		
	</div>
	<div id="pgwDetails" name="pgwDetails">
		<input type="hidden" name="pgwPaymentMobileNumber"  id="pgwPaymentMobileNumber" escapeXml="false"/>
		<input type="hidden" name="pgwPaymentEmail"  id="pgwPaymentEmail" escapeXml="false"/>
		<input type="hidden" name="pgwPaymentCustomerName"  id="pgwPaymentCustomerName" escapeXml="false"/>
	</div>
	<input type="hidden" name="blnNextPrevious" id="blnNextPrevious" value="false" /> 
	<input type="hidden" id="resFlexibleDates" 	name="flexibleDates" value='<c:out value="${param.flexibleDates}" escapeXml="false"/>' /> 
	<input 	type="hidden" id="fromSecure" name="fromSecure" value='<c:out value="${param.fromSecure}" escapeXml="false"/>' /> 	
	<input 	type="hidden" id="resFlexiAlerts" name="resFlexiAlerts" value='<c:out value="${param.resFlexiAlerts}" escapeXml="false"/>' /> 
	<input 	type="hidden" id="requestSessionIdentifier" name="requestSessionIdentifier" value='<c:out value="${param.requestSessionIdentifier}" escapeXml="false"/>' />
	<input type="hidden" 	id="resFareQuote" 			name="fareQuoteJson" value='<c:out value="${fareQuoteJson}" escapeXml="false"/>' />
	<input type="hidden" 	id="resSelectedFlights" 	name="selectedFlightJson" value='<c:out value="${selectedFlightJson}" escapeXml="false"/>' />
</form>

 <form id="frmTemp" method="post"> 
 </form>
<script src="../js/calDisplayConfig.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
<c:if test="${not empty(requestScope.sysOndSource)}">
<script src="<c:out value='${requestScope.sysOndSource}' escapeXml='false'/>" type="text/javascript"></script>		
</c:if>
<script src="../js/v2/reservation/availabilitySearchSupportV2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
<script src="../js/v2/reservation/mcAvailabilitySearch.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>


</div>
<div id='divLoadMsg' class="mainPageLoader" style="display: none">
<%@ include file='../common/includeLoadingMsg.jsp'%>
</div>
<%-- Use to load third party tools and tracking codes--%>
<div id="accelAeroIBETrack" style="display: none"><iframe
	name="frmTracking" id="frmTracking" src="showBlank" frameborder="0">
</iframe></div>
</body>
<%@ include file='../common/inline_Tracking.jsp' %>
</html>
