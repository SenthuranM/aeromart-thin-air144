<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<link rel='shortcut icon' href='../images/AA.ico'/>
<title></title>
	<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/>
	<script src="../js/v2/jquery/jquery.js" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>	
	<script src="../js/v2/isalibs/isa.jquery.templete.js" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.form.js" type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js" type="text/javascript"></script>
	<script src="../js/v2/common/JQuery.ibe.i18n.js" type="text/javascript"></script>
	<script src="../js/v2/common/JQuery.flash.js" type="text/javascript"></script>
</head>

<body id="direction">
<div id="dvPaymentPage">
<form action="" id="frmPayment" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgPayment">
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<!-- Top Banner -->
					<tr>
						<td colspan='2'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td colspan='3' class='topBanner' align='center'>
										<table width='97%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td width='5%' style='height:70px;'><a href='javascript:UI_Payment.startOverClick();' id="lnkHome" name="lnkHome" title=""><img src='../images/LIbe005_no_cache.gif' alt='logo'/></a></td>
												<td class="alignRight">
													<script src="../js/common/activeContentLoader.js" type="text/javascript"></script>
													<div id="flashObj">									   
													    	<noscript>
														    	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="491" height="72">
															      <param name="movie" value="http://www.airarabia.com/userfiles/header_banner/en/sharjah.swf">
															      <param name="quality" value="high">
															      <param name="menu" value="false">
															      <param name="wmode" value="transparent">
															      <embed src="http://www.airarabia.com/userfiles/header_banner/en/sharjah.swf" width="491" height="72" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" menu="false" wmode="transparent"></embed>
															    </object>
													    </noscript>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class='topBannerLB'></td>
									<td class='topBannerMB'><label>&nbsp;</label></td>
									<td class='topBannerRB'></td>
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- Home Link row -->
					<tr>
						<td colspan='2' align='center'>
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class="alignRight">
										<a href='javascript:UI_Payment.startOverClick();' id="lnkHome" name="lnkHome" title="Click here to go to the Home page"><img border='0' class="linkImage" src="../images/home_no_cache.gif"></a>
									</td>
								</tr>
								<tr>
									<td class='rowSingleGap'></td>
								</tr>			
							</table>
						</td>
					</tr>
					
					<!-- Content holder -->
					<tr>
						<!-- Left Column -->
						<td width='260px' valign='top' class='alignLeft'>  
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<!-- Booking Summary -->
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneTopL'></td>
												<td class='lpaneTopM'><label id="lblBookingSummary" class='fntBold gridHDFont'>Booking Summary</label></td>
												<td class='lpaneTopR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneMidL'></td>
												<td class='lpaneMidM'>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgPassenger">
														<tr>
															<td class='pnlTop'></td>
														</tr>
														<tr>
															<td class='pnlBottom'>
																<table width="100%" border="0" cellspacing="1" cellpadding="1">
																	<tr>
																		<td><label id="lblFlightDetails" class='fntBold hdFontColor'>Flight Details</label></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td><label id="lblReturn"/></td>           
																	</tr>
																	<tr>
																		<td><label id="adultCount"/></td>            
																	</tr>
																	<tr>            
																		<td><label id="childCount"/></td>
																	</tr>
																	<tr>
																		<td><label id="infantCount"/></td>
																	</tr>         
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate">
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																		<td class='pnlTopR'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																				<tr>
																					<td colspan="3"><label id="segmentCode"  class='fntBold hdFontColor'/></td>            
																				</tr>
																				<tr>
																					<td width="50%"><label id="lblflightNo">Flight No</label></td>
																					<td width="1%"><label>:</label></td>
																					<td width="49%"><label id="flightNumber"/></td>
																				</tr>
																				<tr>
																					<td><label  id="lbldepatureDate">Date</label> </td>
																					<td><label>:</label></td>
																					<td><label id="depatureDate"/></td>
																				</tr>
																				<tr>
																					<td><label id="lbldepatureTime">Departure</label></td>
																					<td><label>:</label></td>
																					<td><label id="depatureTime"/></td>
																				</tr>
																				<tr>
																					<td><label id="lblarrivalTime">Arrival</label></td>
																					<td><label>:</label></td>
																					<td><label id="arrivalTime"/></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td class='lpaneMidR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneBotL'></td>
												<td class='lpaneBotM'></td>
												<td class='lpaneBotR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class='rowGap'></td>
								</tr>
								
								<!-- Payment Summary -->
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneTopL'></td>
												<td class='lpaneTopM'><label id="lblPaymentSummary" class='fntBold gridHDFont'>Payment Summary</label></td>
												<td class='lpaneTopR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneMidL'></td>
												<td class='lpaneMidM'>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop'></td>
														</tr>
														<tr>
															<td class='pnlBottom'>
																<table width="100%" border="0" cellspacing="1" cellpadding="1">
																	<tr>
																		<td><label id="lblRetFlight" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td><label id="lblAdults" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td><label id="lblChildren" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td><label id="lblInfants" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate">
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																		<td class='pnlTopR'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																				<tr>
																					<td width="50%"><label id="lblAirFare">Air Fare</label></td>
																					<td width="2%" class="hdFontColor fntBold">:</td>																				
																					<td width="10%" id="airFareCurrCode" class='fntDefault txtBold' align="left"></td>
																					<td width="38%" id="airFare" class='fntDefault txtBold' align="right"></td>																					
																				</tr>
																			 	<tr>
																					<td width="50%"><label id="lblCharges">Tax &amp; surcharges</label></td>
																					<td width="2%" class="hdFontColor fntBold">:</td>	
																					<td width="10%" id="chargesCurrCode" class='fntDefault txtBold' align="left"></td>
																					<td width="38%" id="charges"  class='fntDefault txtBold' align="right"></td>
																				</tr>																				
																				<tr id='trSeatCharges'>
																					<td><label id='lblSeatSelection' >Seat Selection</label></td>
																					<td class="hdFontColor fntBold">:</td>
																					<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																					<td align="right"><div id='divSeatCharges' class='fntDefault txtBold'></div></td>
																				</tr>																				
																				<tr id='trInsuranceCharges'>
																					<td><label id='lblTravelSecure' >Travel Insurance</label></td>
																					<td class="hdFontColor fntBold">:</td>
																					<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																					<td align="right"><div id='divInsuranceCharges' class='fntDefault txtBold'></div></td>
																				</tr>
																				<tr id='trMealCharges'>
																					<td><label id='lblMealSelection' >Meal Selection</label></td>
																					<td class="hdFontColor fntBold">:</td>
																					<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																					<td align="right"><div id='divMealCharges' class='fntDefault txtBold'></div></td>
																				</tr>
																				<tr id='trHalaCharges'>
																					<td><label id='lblHalaService' >Hala Service</label></td>
																					<td class="hdFontColor fntBold">:</td>
																					<td align="left"><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																					<td align="right"><div id='divHalaCharges' class='fntDefault txtBold'></div></td>
																				</tr>
																				<tr>
																					<td width="50%"><label id="lblTNXFee">Transaction Fees</label></td>
																					<td width="2%" class="hdFontColor fntBold">:</td>	
																					<td width="10%" id="txnFeeCurrCode" class='fntDefault txtBold' align="left"></td>
																					<td width="38%" id="txnFee"  class='fntDefault txtBold' align="right"></td>
																				</tr>
																				<tr>
																					<td colspan="4" height="10px"></td>
																				</tr>
																				<tr>
																					<td width="50%"><label id="lblTotal" class='fntBold'>Total</label></td>
																					<td width="2%" class="hdFontColor fntBold">:</td>
																					<td width="10%" id="totalCurrCode"  class='fntDefault fntBold hdFontColor' align="left"></td>
																					<td width="38%" id="total" class='fntDefault fntBold hdFontColor' align="right"></td>
																				</tr> 
																				<tr style="display: none;" id="selectedCurr">
																					<td width="50%"></td>
																					<td width="2%" class="hdFontColor fntBold hdFontColor">:</td>
																					<td width="10%" id="totalSelectedCurrCode"  class='fntDefault fntBold hdFontColor' align="left"></td>
																					<td width="38%" id="selectedTotal" class='fntDefault fntBold hdFontColor' align="right"></td>
																				</tr> 
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td class='lpaneMidR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneBotL'></td>
												<td class='lpaneBotM'></td>
												<td class='lpaneBotR'></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						
						<!-- Right Column -->
						<td valign='top' id="rightPanel" rowspan='2' align="right">
							<table width='98%' border='0' cellpadding='0' cellspacing='0' class="alignRight">
								<tr>
									<td align='left'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='pnlTop pnlWidth'></td>
												<td class='pnlTopR'></td>
											</tr>
											<tr>
												<td colspan='2' class='pnlBottomTall' valign='top' align='center'>
													<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<tr>
															<td>
																<table border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td class='tab1A' align='center'>
																			<table border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td><label class='fntBold navFontBig'>1</label></td>
																					<td style='width:10px;'></td>
																					<td><label  id="lblSearch&Choose" class='fntBold navFont'>Search & Choose</label></td>
																				</tr>
																			</table>
																		</td>
																		<td class='tab2' align='center'>
																			<table border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td><label class='fntBold navFontBig'>2</label></td>
																					<td style='width:10px;'></td>
																					<td><label id="lblSelect&Pay" class='fntBold navFont'>Select & Pay</label></td>
																				</tr>
																			</table>
																		</td>
																		<td class='tab3A' align='center'>
																			<table border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td><label class='fntBold navFontBig'>3</label></td>
																					<td style='width:10px;'></td>
																					<td><label id="lblPrint&Fly" class='fntBold navFont'>Print & Fly</label></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td class="alignLeft">
																<label id="lblPayment" class='fntBold hdFont hdFontColor'>Payment</label>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td class="alignLeft">
																<label id="lblMsgReviewPayment"></label>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
									
										<!-- Grid -->
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottom' valign='top'>
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="1%" class="tblBgLeft"></td>
																		<td>
																			<table width="100%" border="0" cellspacing="1" cellpadding="1" id="tblPriceBreakdown">
																				<tr>
																					<th colspan="2" class="alignLeft"><label id="lblPriceBreakdown" class='fntBold'>Price Breakdown </label></th>            
																				</tr>
																				<tr>
																					<th width='50%' class='gridHD bdLeft bdBottom bdTop' align='center'><label id="lblDescription" class='gridHDFont fntBold'>Description</label></th>
																					<th width='50%' class='gridHD  bdBottom bdRight bdTop' align='center'><label id="lblAmount" class='gridHDFont fntBold'>Amount</label></th>
																				</tr>
																				<tr>
																					<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblTotAirFare">Total Air fare</label></td>
																					<td align='right' class='rowColor  bdBottom bdRight'><label id="lblTotAirFareAmount" class='hdFontColor'></label></td>
																				</tr>
																				<tr> 
																					<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id="lblTotTaxCharge">Total Taxes & Surcharges </label></td>
																					<td align='right' class='rowColorAlternate  bdBottom bdRight'><label id="lblTotTaxChargeAmount" class='hdFontColor'></label></td>
																				</tr>
																				<tr> 
																					<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id="lblFlexiCharges">Flexi Charges </label></td>
																					<td align='right' class='rowColorAlternate  bdBottom bdRight'><label id="lblFlexiChargeAmount" class='hdFontColor'></label></td>
																				</tr>
																				<tr id="trSeatChargesBD">
																					<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft' ><label id='lblSeatSelection'>Seat Selection </label></td>
																					<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblSeatCharges" class='hdFontColor'></label></td>
																				</tr> 
																				<tr id="trInsuranceChargesBD">
																					<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id='lblTravelSecure' >Travel Insurance </label></td>
																					<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblInsuranceCharges" class='hdFontColor'></label></td>
																				</tr> 																				
																				<tr id="trMealChargesBD">
																					<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id='lblMealSelection'>Meal Selection </label></td>
																					<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblMealCharges" class='hdFontColor'></label></td>
																				</tr> 
																				<tr id="trHalaChargesBD">
																					<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id='lblHalService'>Hala Service </label></td>
																					<td align='right' class='rowColorAlternate bdBottom bdRight' ><label id="lblHalaCharges" class='hdFontColor'></label></td>
																				</tr> 
																				<tr id="trAirportTransferChargesBD">
																					<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id='lblAirportTransfer'>Airport Transfer </label></td>
																					<td align='right' class='rowColorAlternate bdBottom bdRight' ><label id="lblAirportTransferCharges" class='hdFontColor'></label></td>
																				</tr> 
																				<tr>
																					<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblTNXFee">Transaction Fees </label></td>
																					<td align='right' class='rowColor  bdBottom bdRight'><label id="lblTNXFeeAmount" class='hdFontColor'></label></td>
																				</tr>   																				
																				<tr>
																					<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '><label id="lblTotalDue" class='fntBold'>Total Due Amount</label></td>
																					<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblTotalDueAmount" class='fntBold hdFontColor'></label></td>
																				</tr> 
																				<tr id="paymentSelectedCurrCode" style="display: none;">
																					<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="lblTotalDue" class='fntBold'>Total Due Amount</label></td>
																					<td align='right' class='rowColor bdBottom bdRight'><label id="lblTotalDueAmountSelected" class='fntBold hdFontColor'></label></td>
																				</tr>           
																			</table>
																		</td>
																		<td width="1%" class="tblBgRight"></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td id="flexiInfo" style="display: none;" align="left" colspan="2"><font class="fntEnglish">Remaining Flexibilities: <span id="spnRemainingFlexi"><font class="fntRed">No more flexibilities available for the cancelled segment</font></span></font>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap' colspan='2'></td>
																	</tr>
																	<tr>
																		<td colspan="3">
																			<table width="100%" border="0" cellspacing="1" cellpadding="1">
																				<tr>
																					 <td class="alignLeft"><label id="lblPG">Payment Gateway</label></td>
																				</tr>
																				<tr>
																					 <td>
																						<select id='selPG' size='1' style='width:200px;' name='strPaymentGateway'></select>
																					 </td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class="alignLeft" colspan="3">
																			<label id="lblNofication" class='fntBold'>Notification </label><label class="paddingL3 fntBold">:</label><label class="paddingL3" id="lblMsgRecordIP">Air Arabia records IP address for all web bookings for security reasons. </label><br>
																			<label id="lblYourIPIs" class='fntBold'>Your IP address is </label><label class="paddingL3 fntBold">:</label><label class="paddingL3" id='lblYourIP'>127.0.0.1</label>
																		</td>
																	</tr>																	
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align='left'>
									<div class="buttonset">
										<!-- Button -->
										<table border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td><input type="button" id="btnCancel"  value="Cancel" class="Button"/></td>					
												<td><input type="button" id="btnPurchase"  value="Purchase" class="Button" /></td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- Bottom AA Logo -->
					<tr>
						<td class='appLogo'></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<input type="hidden" id="payCurrency" name="pgw.payCurrency"/>
	<input type="hidden" id="providerName" name="pgw.providerName"/>
	<input type="hidden" id="providerCode" name="pgw.providerCode"/>
	<input type="hidden" id="description" name="pgw.description"/>
	<input type="hidden" id="paymentGateway" name="pgw.paymentGateway"/>
	<input type="hidden" id="switchToExternalURL" name="pgw.switchToExternalURL"/>
	<input type="hidden" id="payCurrencyAmount" name="pgw.payCurrencyAmount"/>
	<input type="hidden" id="viewPaymentInIframe" name="pgw.viewPaymentInIframe"/>
	<input type="hidden" id="onholdReleaseTime" name="pgw.onholdReleaseTime"/>
	<input type="hidden" id="hdnMode" name="hdnMode"/>
 </form>
<script src="../js/v2/reservation/interLinePayment.js" type="text/javascript"></script>	
</div>
	<div id='divLoadMsg' class="mainPageLoader">
		<%@ include file='../common/includeLoadingMsg.jsp' %>
	</div>
 </body>
</html>