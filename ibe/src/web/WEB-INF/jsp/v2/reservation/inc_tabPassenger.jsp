<table width='100%' border='0' cellpadding='0' cellspacing='0' style="display: none;" id="passengerMainTable">
<tr>
	<td colspan="2" class="passengerGadient paddingL5">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="pageTopHeader">
				<tr>
				<td class="rowSingleGap">
				</td>
				</tr>
				<tr>
				<td class="pageName alignLeft" colspan="2">
					<label id="lblPassengerInformation"></label>
				</td>
				</tr>
				<tr>
					<td colspan='2' class='rowGap'></td>
				</tr>
				<tr>
					<td class="alignLeft">
						<div id="mandatoryText"><img src="../images/AA045_no_cache.jpg"/>&nbsp;<label id="lblFillInEnglish"></label><label id="lblHighlightWith"></label><label class='mandatory'>&nbsp;*&nbsp;</label><label id="lblAreMandatory"></label><br/> </div>
						<img src="../images/AA045_no_cache.jpg"/>&nbsp;<label id="lblPaxEmailContactIsCorrect"></label><br/>
						<img src="../images/AA045_no_cache.jpg"/>&nbsp;<label id="lblPaxBookingNameIsCorrect" class='lineHeight fntEnglish'></label>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan='2' class='rowGap'></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="1" cellpadding="1">
				<tr>
					<td style='width:25px;' class="alignLeft"></td>
					<td class="alignLeft"><span id="spnError"></span></td>
					<td style='width:25px;' class="alignLeft"></td>
				</tr>
			</table>
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class='pnlTop pnlWidth'></td>
					<td class='pnlTopR'></td>
				</tr>
				<tr>
					<td colspan='2' class='pnlBottom' valign='top'>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr id="adlGrid" style="display:none">
								<td>							
									
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<thead>
										<tr>
											<td colspan='6' class="alignLeft paddingL5">
												<label id="lblAdultHD" class='fntBold hdFontColor'></label>
											</td>
										</tr>
										<tr>
											<td width="7%" align="center" class='gridHD'>
												<label id="lblPaxNo" class='gridHDFont fntBold'></label>
											</td>
											<td width="9%" align="center" class='gridHD'>
												<label id="lblPaxTitle" class='gridHDFont fntBold'></label>
												<label id="lblADTitleMand" class="MandatoryGrid">*</label>
											</td>
											<td width="25%" align="center" class='gridHD'>
												<label id="lblPaxFirstName" class='gridHDFont fntBold'></label>
												<label id="lblADFNameMand" class="MandatoryGrid">*</label>
											</td>
											<td width="25%" align="center" class='gridHD'>
												<label id="lblPaxLastName" class='gridHDFont fntBold'></label>
												<label id="lblAdLNameMand" class="MandatoryGrid">*</label>
											</td>
											<td width="16%" align="center" class='gridHD'>
												<label id="lblPaxNationality" class='gridHDFont fntBold'></label>
												<label id="lblADNationalityMand" class="MandatoryGrid">*</label>
											</td>
											<td width="18%" align="center" class='gridHD'>
												<label id="lblPaxDOB" class='gridHDFont fntBold'></label>
												<label id="lblADDOBMand" class="MandatoryGrid">*</label>
											</td>
										</tr>
										<tr>
											<td colspan="6" class="alignLeft paddingL5">
												<label id="lblMSGforOtherLang">&nbsp;</label>
											</td>
										</tr>
										</thead>
										<tr id="paxAdTemplate">
											<td colspan="6">
												<table border="0" cellspacing="1" cellpadding="0" width="100%">
												<tr>
													<td width="7%" align='right' rowspan="2">
														<label id="paxSequence"></label>
													</td>
													<td width="9%" align='center'>
														<select id='title' style="width: 45px;" size='1' name='title'>
														</select>
													</td>
													<td width="25%" align='center'>
														<input type='text' id='firstName' name="firstName" style='width:140px;' maxlength='50' class="fontCapitalize"/>
													</td>
													<td width="25%" align='center'>
														<input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize"/>
													</td>
													<td width="16%" align='center'>
														<select id='nationality' size='1' name='nationality' style='width:150px;'>
														</select>
													</td>
													<td width="18%" align='center'>
														<input type='text' id='dateOfBirth' name="dateOfBirth" style='width:60px;' maxlength='50' readonly="readonly"/>
													</td>
												</tr>
												<tr class="psptDetails">
													<td colspan="5">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
										                	<td width="9%" class="gridHDsub" ><label id="lblPaxNationalIDNo" class="gridHDFont fntBold adPSPTExpiry"></label></td>
													  <td width="27%" class='gridHDsub' align="center">
													    <label id="lblPSPT" class="gridHDFont fntBold adPSPT"></label>
													    <label class="MandatoryGrid lblADPSPTMand">*</label>
													  </td>
										  		     <td width="27%" class="gridHDsub" align="center">
										   			    <label id="lblPSPTExpiry" class="gridHDFont fntBold adPSPTExpiry"></label>
										   			 	<label class="MandatoryGrid lblADExpiryMand">*</label>   
													 </td>
										       		  <td width="17%" class="gridHDsub" align="center">
										    		    <label id="lblPSPTPlace" class="gridHDFont fntBold adPSPTPlace"></label>
										    		    <label class="MandatoryGrid lblADIssuedCntryMand">*</label>
													  </td>
										                <td width="19%" align="center" class="gridHD">&nbsp;</td>
														</tr>
														<tr>
										                	<td width="9%"><textarea id="nationalIDNoTxt" cols="30" rows="1" 
																style="overflow:hidden;width:40px;height:12px;padding: 2px;resize:none"></textarea>
																<input type="hidden" id='displayNationalIDNo' name="displayNationalIDNo" maxlength="30"/>					
															</td>						
										           	    <td width="27%" align="center">
										           	     <!-- <input type='text' id='foidNumber' name="foidNumber" maxlength="30"/>-->
													     <textarea id="foidNumberTxt" cols="30" rows="1" 
													     style="overflow:hidden;width:140px;height:12px;padding: 2px;resize:none"></textarea>
													     <input type="hidden" id='foidNumber' name="foidNumber" maxlength="30"/> 
														</td>
										           	  <td width="27%" align="center">
										    		    <input type="text" id="foidExpiry" name="foidExpiry" readonly="readonly" style='width:60px;'/>
														</td>
										              <td width="17%" align="center">
										  			  	<select id="foidPlace" size="1" name="foidPlace" style="width:150px"></select>
														</td>
										                    <td width="19%" align="center">
															</td>
										                </tr>
														</table>
													</td>
												</tr>
													<tr class="docoDetails">
														<td colspan="5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
                                                             <td width="10%" class="gridHDsub" align="center">
                                                                <label id="lblTravelDocumentType" class="gridHDFont fntBold"></label>
                                                                <label class="MandatoryGrid lblADTravelDocumentTypeMand">*</label>
                                                             </td>
                                                              <td width="20%" class="gridHDsub" align="center">
                                                                <label id="lblVisaDocNumber" class="gridHDFont fntBold"></label>
                                                                <label class="MandatoryGrid lblADVisaDocNumberMand">*</label>
														        </td>
														  <td width="20%" class='gridHDsub' align="center">
														    <label id="lblPlaceOfBirth" class="gridHDFont fntBold"></label>
														    <label class="MandatoryGrid lblADPlaceOfBirthMand">*</label>
														  </td>
														  <td width="20%" class='gridHDsub' align="center">
														    <label id="lblVisaDocPlaceOfIssue" class="gridHDFont fntBold"></label>
														    <label class="MandatoryGrid lblADVisaDocPlaceOfIssueMand">*</label>
														  </td>
													     <td width="15%" class="gridHDsub" align="center">
														    <label id="lblVisaDocIssueDate" class="gridHDFont fntBold"></label>
														 	<label class="MandatoryGrid lblADVisaDocIssueDateMand">*</label>   
														 </td>
													  <td width="15%" class="gridHDsub" align="center">
													    <label id="lblVisaApplicableCountry" class="gridHDFont fntBold"></label>
													    <label class="MandatoryGrid lblADVisaApplicableCountryMand">*</label>
														  </td>
															</tr>
															<tr>
												      <td width="10%" align="center">
														  	<select id="travelDocumentType" size="1" name="travelDocumentType" style="width:50px"></select>
															</td>
												   	    <td width="19%" align="center">
														     <textarea id="visaDocNumber" cols="30" rows="1" 
														     style="overflow:hidden;width:100px;height:12px;padding: 2px;resize:none"></textarea>
														     <input type="hidden" id='visaDocNumber' name="visaDocNumber" maxlength="30"/> 
															</td>
													<td width="17%" align="center">
													    <input type="text" id="placeOfBirth" name="placeOfBirth" style='width:100px;'/>
															</td>
												   	  <td width="15%" align="center">
													    <input type="text" id="visaDocPlaceOfIssue" name="visaDocPlaceOfIssue" style='width:90px;'/>
															</td>
												   	  <td width="20%" align="center">
													    <input type="text" id="visaDocIssueDate" name="visaDocIssueDate" readonly="readonly" style='width:60px;'/>
															</td>
												      <td width="18%" align="center">
														  	<select id="visaApplicableCountry" size="1" name="visaApplicableCountry" style="width:110px"></select>
															</td>												
												        </tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										</table>		
								</td>
							</tr> 
							<%--  Child Grid --%>
							 <tr id="chdGrid" style="display:none">
								<td>									
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<thead>
										<tr>
											<td colspan='6' class="alignLeft paddingL5">
												<label id="lblChildHD" class='fntBold hdFontColor'></label>
											</td>
										</tr>
										<tr>
											<td width="7%" align="center" class='gridHD'>
												<label id="lblPaxNo" class='gridHDFont fntBold'></label>
											</td>
											<td width="9%" align="center" class='gridHD'>
												<label id="lblPaxTitle" class='gridHDFont fntBold'></label>
												<label id="lblCHTitleMand" class="MandatoryGrid">*</label>
											</td>
											<td width="25%" align="center" class='gridHD'>
												<label id="lblPaxFirstName" class='gridHDFont fntBold'></label>
												<label id="lblCHFNameMand" class="MandatoryGrid">*</label>
											</td>
											<td width="25%" align="center" class='gridHD'>
												<label id="lblPaxLastName" class='gridHDFont fntBold'></label>
												<label id="lblCHLNameMand" class="MandatoryGrid">*</label>
											</td>
											<td width="16%" align="center" class='gridHD'>
												<label id="lblPaxNationality" class='gridHDFont fntBold'></label>
												<label id="lblCHNationalityMand" class="MandatoryGrid">*</label>
											</td>
											<td width="18%" align="center" class='gridHD'>
												<label id="lblPaxDOB" class='gridHDFont fntBold'></label>
												<label id="lblCHDOBMand" class="MandatoryGrid">*</label>
											</td>
										</tr>
										</thead>
										<tr id="paxChTemplate">
											<td colspan="6">
												<table border="0" cellspacing="1" cellpadding="0" width="100%">
												<tr>
													<td width="7%" align='right' rowspan="2">
														<label id="paxSequence"></label>
													</td>
													<td width="9%" align='center'>
														<select id='chdTitle' style="width: 45px;" size='1' name='chdTitle'>
														</select>
													</td>
													<td width="25%" align='center'>
														<input type='text' id='firstName' name="firstName" style='width:140px;' maxlength='50' class="fontCapitalize"/>
													</td>
													<td width="25%" align='center'>
														<input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize"/>
													</td>
													<td width="16%" align='center'>
														<select id='nationality' size='1' name='nationality' style='width:150px;'>
														</select>
													</td>
													<td width="18%" align='center'>
														<input type='text' id='dateOfBirth' name="dateOfBirth" style='width:60px;' maxlength='50' readonly="readonly"/>
													</td>
												</tr>
												<tr class="psptDetails">
													<td colspan="5">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="9%" class="gridHDsub" ><label id="lblPaxNationalIDNo" class="gridHDFont fntBold adPSPTExpiry"></label></td>
															<td width="27%" class='gridHDsub' align='center'>
																<label id="lblPSPT" class='gridHDFont fntBold chPSPT'></label>
																<label class="MandatoryGrid lblCHPSPTMand">*</label>
															</td>
															<td width="27%" class='gridHDsub' align='center'>
																<label id="lblPSPTExpiry" class='gridHDFont fntBold chPSPTExpiry'></label>
																<label class="MandatoryGrid lblCHExpiryMand">*</label>
															</td>
															<td width="17%" class='gridHDsub' align='center'>
																<label id="lblPSPTPlace" class='gridHDFont fntBold chPSPTPlace'></label>
																<label class="MandatoryGrid lblCHIssuedCntryMand">*</label>
															</td>
															<td width="19%" align='center' class='gridHD'>
																&nbsp;
															</td>
														</tr>
														<tr>
															<td width="9%"><textarea id="nationalIDNoTxt" cols="30" rows="1" 
																style="overflow:hidden;width:40px;height:12px;padding: 2px;resize:none"></textarea>
																<input type="hidden" id='displayNationalIDNo' name="displayNationalIDNo" maxlength="30"/>					
															</td>
															<td width="27%" align='center'>
																<textarea id="foidNumberTxt" cols="30" rows="1" 
																style="overflow:hidden;width:140px;height:12px;padding: 2px;resize:none"></textarea>
																<input type="hidden" id='foidNumber' name="foidNumber" maxlength="30"/> 
															</td>
															<td width="27%" align='center'>
																<input type='text' id='foidExpiry' name="foidExpiry" readonly="readonly" style='width:60px;'/>
															</td>
															<td width="17%" align='center'>
																<select id='foidPlace' size='1' name='foidPlace' style='width:150px'>
																</select>
															</td>
															<td width="19%" align='center'>
																&nbsp;
															</td>
														</tr>
														</table>
													</td>
												</tr>
												<tr class="docoDetails">
														<td colspan="5">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
														  <td width="27%" class='gridHDsub' align="center">
														    <label id="lblPlaceOfBirth" class="gridHDFont fntBold"></label>
														    <label class="MandatoryGrid lblCHPlaceOfBirthMand">*</label>
														  </td>
													     <td width="27%" class="gridHDsub" align="center">
														    <label id="lblTravelDocumentType" class="gridHDFont fntBold"></label>
														 	<label class="MandatoryGrid lblCHTravelDocumentTypeMand">*</label>   
														 </td>
													  <td width="17%" class="gridHDsub" align="center">
													    <label id="lblVisaDocNumber" class="gridHDFont fntBold"></label>
													    <label class="MandatoryGrid lblCHVisaDocNumberMand">*</label>
														  </td>
														  <td width="27%" class='gridHDsub' align="center">
														    <label id="lblVisaDocPlaceOfIssue" class="gridHDFont fntBold"></label>
														    <label class="MandatoryGrid lblCHVisaDocPlaceOfIssueMand">*</label>
														  </td>
													     <td width="27%" class="gridHDsub" align="center">
														    <label id="lblVisaDocIssueDate" class="gridHDFont fntBold"></label>
														 	<label class="MandatoryGrid lblCHVisaDocIssueDateMand">*</label>   
														 </td>
													  <td width="17%" class="gridHDsub" align="center">
													    <label id="lblVisaApplicableCountry" class="gridHDFont fntBold"></label>
													    <label class="MandatoryGrid lblCHVisaApplicableCountryMand">*</label>
														  </td>
															</tr>
															<tr>
												   	  <td width="15%" align="center">
													    <input type="text" id="placeOfBirth" name="placeOfBirth" style='width:60px;'/>
															</td>
												      <td width="15%" align="center">
														  	<select id="travelDocumentType" size="1" name="travelDocumentType" style="width:150px"></select>
															</td>
												   	    <td width="20%" align="center">
														     <textarea id="visaDocNumber" cols="30" rows="1" 
														     style="overflow:hidden;width:140px;height:12px;padding: 2px;resize:none"></textarea>
														     <input type="hidden" id='visaDocNumber' name="visaDocNumber" maxlength="30"/> 
															</td>
												   	  <td width="20%" align="center">
													    <input type="text" id="visaDocPlaceOfIssue" name="visaDocPlaceOfIssue" style='width:60px;'/>
															</td>
												   	  <td width="15%" align="center">
													    <input type="text" id="visaDocIssueDate" name="visaDocIssueDate" readonly="readonly" style='width:60px;'/>
															</td>
												      <td width="15%" align="center">
														  	<select id="visaApplicableCountry" size="1" name="visaApplicableCountry" style="width:150px"></select>
															</td>												
												        </tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>			
								</td>
							</tr>
							<%--  Infant Grid --%>
							<tr id="infGrid" style="display:none">
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="1">
										<thead>
											<tr>
												<td colspan='8' class="alignLeft paddingL5"><label id="lblInfantHD"  class='fntBold hdFontColor'></label></td>
											</tr>
											<tr>
												<td width="7%" align="center" class='gridHD'><label id="lblPaxNo" class='gridHDFont fntBold'></label></td>
												<td id="infTitleHeader" width="7%" align="center" class='gridHD'>
													<label id="lblPaxTitle" class='gridHDFont fntBold'></label>
													<label id="lblInfTitleMand" class="MandatoryGrid">*</label>
												</td>
												<td width="18%" align="center" class='gridHD' colspan="2">
													<label id="lblPaxDOB" class='gridHDFont fntBold'></label> 
													<label id="lblINDOBMand" class="MandatoryGrid">*</label>
												</td>																						
												<td width="28%" align="center" class='gridHD'>
													<label id="lblPaxFirstName" class='gridHDFont fntBold'></label> 
													<label id="lblINFNameMand" class="MandatoryGrid">*</label> 
												</td>
												<td width="26%" align="center" class='gridHD'>
													<label id="lblPaxLastName" class='gridHDFont fntBold'></label> 
													<label id="lblINLNameMand" class="MandatoryGrid">*</label> 
												</td>
												<td colspan='4' width="20%" align="center" class='gridHD'>
													<label id="lblPaxTravlingWith" class='gridHDFont fntBold'></label> 
													<label id="lblINTravelWithMand" class="MandatoryGrid">*</label>
												</td>
											</tr>
										</thead>        
										<tr id="paxInTemplate" >
											<td colspan="6">
												<table border="0" cellspacing="1" cellpadding="0" width="100%">
													<tr>
														<td align='right'  rowspan="2" width="7%"><label id="paxSequence" ></label></td>
														<td id='tdInfTitle' align='center' width="7%"><select id='infTitle' style="width: 45px;" size='1' name='infTitle'></select></td>
														<td align='center'><input type='text' id='dateOfBirth' name="dateOfBirth" style='width:60px;' maxlength='50' readonly="readonly"/></td>
			  											<td align='center'><a href="javascript:void(0)"></a></td>
														<td align='center'><input type='text' id='firstName' name="firstName" style='width:140px;' maxlength='50' class="fontCapitalize"/></td>
														<td align='center'><input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize"/></td>
														<td align='center'><select id='travelWith' size='1' style='width:50px;' name='travelWith'></select></td>
														<td align='center'><input type='text' id='foidType' name="foidType" style='display:none' /></td>
														<td align='center'><input type='text' id='foidNumber' name="foidNumber" style='display:none' /></td>										
													</tr>
													<tr class="psptDetails">
														<td colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
												          		<tr>
																	<td width="9%" class="gridHDsub" >
																		<label id="lblPaxNationalIDNo" class="gridHDFont fntBold adPSPTExpiry"></label>
																	</td>
																</tr>
																<tr>
																	<td width="9%">
																		<textarea id="nationalIDNoTxt" cols="30" rows="1"  style="overflow:hidden;width:40px;height:12px;padding: 2px;resize:none"></textarea>
															    		<input type="hidden" id='displayNationalIDNo' name="displayNationalIDNo" maxlength="30"/>
												    				</td>
																</tr>
															</table>
														</td>
												    </tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class='rowGap'></td>
				</tr>
				<%-- <tr>
					<td class="alignLeft paddingCalss">
						<i><label id="lblPaxYemenMassage">Passengers traveling from Yemen are required to enter their nationality. </label></i>
					</td>
				</tr>--%>
			</table>
		</td>
	</tr>
	<tr>
		<td class='rowGap'></td>
	</tr>
	<%--  Pax Options to Continue --%>
	<tr id="paxPageOptions" style="display: none;">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class='pnlTop pnlWidth'></td>
					<td class='pnlTopR'></td>
				</tr>
				<tr>
				<td colspan='2' class='pnlUnderBottom paddingCalss' valign="middle" style="height:136px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
						<tr>
							<td align="left" colspan="2">
								<img border='0' src="../images/myAAlogo-red_no_cache.gif" width="130" height="35" alt="Login & Continiue"/>
							</td>
						</tr>
						<tr>
							<td width="2%"></td>
							<td width="30%" align="left" height="60">
								<table border="0" width="100%">
									<tr>
										<td align="center" colspan="2">
											<img border='0' src="../images/Icon_Login_no_cache.gif" width="48" height="27" alt="Login & Continiue"/>
										</td>
									</tr>
									<tr>
										<td width="5%"><input type="radio" name="howContinue" id="loginContinue" /></td>
										<td width="90%" class="alignLeft"><label class="fntBold hdFontColor" id="lblLoginContinue">Log in and load details</label></td>
									</tr>
									<tr>
										<td width="5%"></td>
										<td width="90%" class="alignLeft"><label id="lblLoginContinue1">I am already registered with 'My airarabia'</label></td>
									</tr>
								</table>
							</td>
							<td width="3%"></td>
							<td width="30%" align="left" >
								<table border="0" width="100%">
									<tr>
										<td align="center" colspan="2">
											<img border='0' src="../images/Icon_CreateNew_no_cache.gif" width="48" height="27" alt="Login & Continiue"/>
										</td>
									</tr>
									<tr>
										<td width="5%"><input type="radio" name="howContinue" id="registerContinue"/></td>
										<td width="90%" class="alignLeft"><label class="fntBold hdFontColor" id="lblRegisterContinue">Register now</label></td>
									</tr>
									<tr>
										<td width="5%"></td>
										<td width="90%" class="alignLeft"><label id="lblRegisterContinue1">I would like to register with 'My airarabia'</label> </td>
									</tr>
								</table>
							</td>
							<td width="3%"></td>
							<td width="30%" align="left">
								<table border="0" width="100%">
									<tr>
										<td align="center" colspan="2">
											<img border='0' src="../images/Icon_Guest_no_cache.gif" width="48" height="27" alt="Login & Continiue"/>		
										</td>
									</tr>
									<tr>
										<td width="5%"><input type="radio" name="howContinue" id="guestContinue" checked="checked"/></td>
										<td width="90%" class="alignLeft"><label class="fntBold hdFontColor" id="lblContinueGuest">Continue as Guest</label></td>
									</tr>
									<tr>
										<td width="5%"></td>
										<td width="90%" class="alignLeft"><label id="lblContinueGuest1">Book without log in not Register</label></td>
									</tr>
								</table>
							</td>
							<td width="2%"></td>
						</tr>
					</table>
				</td>
				</tr>
				<tr>
					<td class='pnlUnderDown pnlWidth'></td>
					<td class='pnlUnderBottomR'></td>
				</tr>
				<tr>
					<td class='rowGap'></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<%--  Registerd User login --%>
			<div id="trRegisterdUser" style="display: none;height:140px" class="div-Table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class='pnlTop pnlWidth'></td>
						<td class='pnlTopR'></td>
					</tr>
					
					<tr>
						<td>
							<table>
							    <tr>
							      	<td id="tdFBSignInP" style="display: none;">
										<div class="soclaiButton soCommonBtn">
											<a href="javascript:void(0)" title="Login With Facebook" id="btnFBSignInP" name="btnFBSignInP" class="buttonFB"> 
										       <span class="logo"></span> <label id="lblFBSignIn">Sign In</label>
											</a>
										</div>							      		
							      	</td>
							      	<td id="tdLnSignInP" style="display: none;">
										<div class="soclaiButton soCommonBtn">
											<a href="javascript:void(0)" title="Login With LinkedIn" id="btnLNSignInP" name="btnLNSignInP" class="buttonLN"> 
										       <span class="logo"></span> <label id="lblLNSignIn">Sign In</label>
											</a>
										</div>							      		
							      	</td>
							  </tr>
						 </table>btnSignIn
					 </td>
					</tr>
					 <tr>
                        <td class="rowSingleGap"></td>
                    </tr>
					
					<tr>
					<td colspan='2' class='pnlBottom paddingCalss' valign='top'>
		           		<div class="table-Row one-Col">
		           			<div class="table-Data one-Col">
							 	<label id="lblSignindSavetime" class="fntBold hdFontColor">Sign in and Save time</label>
							 </div>
						</div>
		           		<div class="table-Row one-Col">
		           			<div class="table-Data one-Col">
							 	<label id="lblMsgForRegisteredUser"></label>
							 </div>
						</div>
			            <div class="table-Row one-Col">
							<div class="table-Data left-TD">
								<label id="lblLoginID">  </label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type="text" id="txtUID" name="txtUID" maxlength="100" style="width:120px"/>
							</div>
						</div>
						<div class="table-Row one-Col">
							<div class="table-Data left-TD">
								<label id="lblPassword"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type="password" id="txtPWD" name="txtPWD" maxlength="12" style="width:120px"/>
							</div>
						</div>
						<div class="table-Row one-Col">
							<div class="table-Data left-TD">
								<label>&nbsp;</label>
							</div>
							<div class="table-Data right-TD">
								<font>&nbsp;</font>
								<input type="button" id="btnSignIn" title="Click here to login" class="Button ButtonMedium" value="Sign In"/>
							</div>
						</div>
						<div class="table-Row  one-Col">
							<div class="table-Data one-Col">
								<label class="fntSmall">&nbsp;</label>
							</div>
						</div>
		           	<div class="end-Table"></div>
				</td>
				</tr>
		     </table>
		</div>
			<%--  Register New User --%>
			<div id="trNewdUser" style="display: none;height:120px" class="div-Table">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class='pnlTop pnlWidth'></td>
						<td class='pnlTopR'></td>
					</tr>
					
					<tr>
						<td>
							<table>
							    <tr>
							      	<td id="tdFBRegisterP" style="display: none;">
										<div class="soclaiButton soCommonBtn">
											<a href="javascript:void(0)" title="Register With Facebook" id="btnFBRegisterP" name="btnFBRegisterP" class="buttonFB"> 
										       <span class="logo"></span> <label id="lblFBRegister">Register</label>
											</a>
										</div>							      		
							       </td>
							       <td id="tdLnRegisterP" style="display: none;">
										<div class="soclaiButton soCommonBtn">
											<a href="javascript:void(0)" title="Register With LinkedIn" id="btnLNRegisterP" name="btnLNRegisterP" class="buttonLN"> 
										       <span class="logo"></span> <label id="lblLnRegister">Register</label>
											</a>
										</div>							      		
							      </td>
							   </tr>
							 </table>
						</td>
					</tr>
					<tr>
                        	<td class="rowSingleGap"></td>
                     </tr>
							
					<tr>
					<td colspan='2' class='pnlBottom paddingCalss' valign='top'>
						<div class="table-Row one-Col">
							<div class="table-Data one-Col">
								<label class="fntBold hdFontColor" id="lblLoginInfo"></label>
							</div>
						</div>
						<div class="table-Row two-Col" id="trLoginEmail">
							<div class="table-Data left-TD">
								<label id="lblLoginID"> </label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type="text" name="txtLoginEmail" title="login ID" maxlength="100" style="width: 130px"  id="txtLoginEmail"/>
								<label class="mandatory" id="lblPaxEmailMand">*</label>
							</div>
						</div>
						<div class="table-Row two-Col">
							<div class="table-Data one-Col">
								<label class="fntSmall" id="lblInfoLoginID"></label>
							</div>
						</div>
						
						<div class="table-Row two-Col">
							<div class="table-Data left-TD">
								<label id="lblPassword"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type="password" maxlength="12" size="10" name="txtPassword" id="txtPassword"/>  
								<label id="lblPaxPassowrdMand"><font class="mandatory">*</font></label>
							</div>
						</div>
						<div class="table-Row two-Col">
							<div class="table-Data one-Col">
								<label id="lblInfoPassword" class="fntSmall"></label>
							</div>
						</div>
						
						<div class="table-Row two-Col">
							<div class="table-Data left-TD">
								<label id="lblConfirmPwd"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type="password" maxlength="12" size="10" name="txtConPassword" id="txtConPassword"/> 
								<label id="lblPaxPassowrdConfMand"><font class="mandatory">*</font></label>
							</div>
						</div>
						<div class="table-Row  one-Col">
							<div class="table-Data one-Col">
								<label class="fntSmall">&nbsp;</label>
							</div>
						</div>
						<div class="end-Table"></div>
					</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
                     
	<%-- Contact informations --%>
	<tr>
		<td class="alignLeft">
		<div class="div-Table" id="tblContactDetails">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class='pnlTop' style="width:10px"></td>
					<td class='pnlTop pnlWidth'></td>
					<td class='pnlTopR'></td>
				</tr>
				<tr>
					<td class="pnlBottomTall"></td>
					<td class='pnlBottomTall' valign='top'>
						<div class="table-Row one-Col">
							<div class="table-Data one-Col">
								<label class='fntBold hdFontColor' id="lblcntInformation"></label>
							</div>
						</div>
						
						<div class="table-Row one-Col" id="trTitle">
							<div class="table-Data left-TD">
								<label id="lblTitle"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<select id='selTitle' size='1' style="width: 45px;" tabindex="1" name='contactInfo.title'></select>
								<label id="lblTitleMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<div class="table-Row two-Col" id="tblFirstName">
							<div class="table-Data left-TD">
								<label id="lblFirstName"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtFName' name="contactInfo.firstName" maxlength='50'  class="fontCapitalize"  tabindex="2" />
								<label id="lblFnameMand" class='mandatory'>*</label> 
							</div>
						</div>
						<div class="table-Row two-Col" id="tblLastName">
							<div class="table-Data left-TD">
								<label id="lblLastName"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtLName' name="contactInfo.lastName" maxlength='50'  class="fontCapitalize"  tabindex="3" />
								<label id="lblLNameMand" class='mandatory'>*</label> 
							</div>
						</div>
						<div class="table-Row one-Col" id="tblLastNameNotification">
							<div class="table-Data one-Col">
								<label id="lblLastNameNotification"></label>
							</div>
						</div>
						
						<div class="table-Row two-Col" id="tblAddr1">
							<div class="table-Data left-TD">
								<label id="lblAddress"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtStreet' name='contactInfo.addresStreet' maxlength="100" tabindex="4"/>
								<label id="lblAddrMand" class='mandatory'>*</label>
							</div>
						</div>
						<div class="table-Row two-Col" id="tblNationality">
							<div class="table-Data left-TD">
								<label id="lblNationality"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<select id='selNationality' size='1' style='width:135px;' tabindex="6" name='contactInfo.nationality'></select>
								<label id="lblNationalityMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<div class="table-Row two-Col" id="tblAddr2">
							<div class="table-Data left-TD">
								<label>&nbsp;</label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtAddress' name='contactInfo.addresline' maxlength="100" tabindex="5"/>
							</div>
						</div>
						<div class="table-Row two-Col" id="tblCity">
							<div class="table-Data left-TD">
								<label id="lblCity"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtCity'  name="contactInfo.city"  maxlength='20'  tabindex="7" />
								<label id="lblCityMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<div class="table-Row two-Col" id="tblZipCode">
							<div class="table-Data left-TD">
								<label id="lblZipCode"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtZipCode' name='contactInfo.zipCode' maxlength="10" tabindex="8"/>
							</div>
						</div>
						<div class="table-Row two-Col" id="tblCountry">
							<div class="table-Data left-TD">
								<label id="lblCountry"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<select id='selCountry' size='1' style='width:135px;'  tabindex="9" name='contactInfo.country'>														
								</select><label id="lblCountryMand" class='mandatory'>*</label>
								<input id='countryName' type="hidden" name='contactInfo.countryName' value="" />
							</div>
						</div>
						
						<div class="table-Row two-Col" id="tblPreferredLang">
							<div class="table-Data left-TD">
								<label id="lblPreferredLanguage"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<select id='selPrefLang' size='1' style='width:135px;'  tabindex="10" name='contactInfo.preferredLangauge'>														
								</select><label id="lblPreferredLangMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<!-- Contact Details -->
						<div class="table-Row one-Col thin-row trPhoneNoHeader">
							<div class="table-Data one-Col">
								<label class="fntBold hdFontColor" id="lblContactDetails"></label>
							</div>
						</div>
						
						<div class="table-Row one-Col thin-row trPhoneNoLabel">
							<div class="table-Data left-TD">
								<label>&nbsp;</label>
							</div>
							<div class="table-Data right-TD">
								<label id="lblCountryCode" class='fntSmall'></label>
								<label id="lblAreaCode" class='fntSmall areaCode'></label>
								<label id="lblNumber" class='fntSmall'></label>
							</div>
						</div>
						<!-- Mobile No Help for W5 -->
					<% if (com.isa.thinair.commons.core.util.AppSysParamsUtil.addMobileAreaCodePrefix()) { %>
						<div id="mobileNoHelpDiv" class="table-Row one-Col thin-row trPhoneNoLabel">
							<div class="table-Data left-TD">
								<label id="lblMobileNoExample" class='fntSmall fntBold'>Example</label>
							</div>
							<div id="mobileNoHelp"  class="table-Data right-TD">
			                	<label id="lblCountryCodeHelp" class='fntSmall fntBold'>98</label>
			                  	<label id="lblAreaCodeHelp" class='fntSmall areaCode fntBold'>9XX</label>
			                  	<label id="lblNumberHelp" class='fntSmall fntBold'>XXXXXXX</label>
			                </div>
						</div>
					<% } %>
						<div id="tblContactNo">
						<div class="table-Row one-Col" id="trMobileNo">
							<div class="table-Data left-TD">
								<label id="lblMobileNo"></label>
							</div>
							<div class="table-Data right-TD">
								<font class="changeOverFont">:</font>
								<div class="changeOver">
									<input type='text' id='txtMCountry' name="contactInfo.mCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="11"/>
									<input type='text' id='txtMArea' name="contactInfo.mArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="12" class="areaCode"/> 
									<input type='text' id='txtMobile' name="contactInfo.mNumber" style='width:100px;text-align:right;' maxlength='10' tabindex="13" />
								</div>
								<label id="lblMobileNoMand" class='mandatory'>*</label>
								<div id="atLeasetOne" class="floatItem" style="width:223px"><font class="mandatory" >*&nbsp;</font> <label class="fntSmall" id="lblAtleaseOneText">Please provide at least one phone Number</label></div>
							</div>
						</div>
						
						<div class="table-Row one-Col" id="trphoneNo">
							<div class="table-Data left-TD">
								<label id="lblPhoneNo"></label>
							</div>
							<div class="table-Data right-TD" >
								<font class="changeOverFont">:</font>
								<div class="changeOver">
								<input type='text' id='txtPCountry' name="contactInfo.lCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="14" />
								<input type='text' id='txtPArea' name="contactInfo.lArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="15" class="areaCode"/> 
								<input type='text' id='txtPhone' name="contactInfo.lNumber" style='width:100px;text-align:right;' maxlength='10'  tabindex="16" />
								</div>
								<label id="lblPhoneNoMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<div class="table-Row one-Col" id="trFaxNo">
							<div class="table-Data left-TD">
								<label id="lblFaxNo"></label>
							</div>
							<div class="table-Data right-TD">
								<font class="changeOverFont">:</font>
								<div class="changeOver">
								<input type='text' id='txtFCountry' name="contactInfo.fCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="17" />
								<input type='text' id='txtFArea' name="contactInfo.fArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="18" class="areaCode"/> 
								<input type='text' id='txtFax' name="contactInfo.fNo" style='width:100px;text-align:right;' maxlength='10'  tabindex="19" />
								</div>
								<label id="lblFaxMand" class='mandatory'>*</label>
							</div>
						</div>
						</div>
						<div id="tblEmail">
						<div class="table-Row one-Col auto-height">
							<div class="table-Data one-Col">
								<label id="lblMsgValideEmail"></label>
							</div>
						</div>
						<div class="table-Row one-Col" id="trEmail">
							<div class="table-Data left-TD">
								<label id="lblEmailAddress"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtEmail' name="contactInfo.emailAddress" style='width:165px;' maxlength='100'  tabindex="20" />
								<label id="lblEmailMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<div class="table-Row one-Col">
							<div class="table-Data left-TD">
								<label id="lblVerifyEmail"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtVerifyEmail' name="txtVerifyEmail" style='width:165px;' maxlength='100' tabindex="21" />
								<label id="lblVerifyEmailMand" class='mandatory' style="display:none">*</label>
							</div>
						</div>
						</div>
						<!-- AARESAA-5454 - Issue 1		
						<div class="table-Row one-Col" id="trItinearyLans">
							<div class="table-Data left-TD">
								<label id="lblLanguage"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<select id="selLanguage" name="contactInfo.emailLanguage"  style="width: 90px;" size="1" tabindex="22"></select>
							</div>
						</div>
						
						-->	
						<input type="hidden" id="hdnItiLanguage" name="contactInfo.emailLanguage"/>
						
						<div class="table-Row  one-Col">
							<div class="table-Data one-Col">
								<label class="fntSmall">&nbsp;</label>
							</div>
						</div>
						<div class="end-Table"></div>
					</td>
					<td class="pnlBottomTall"></td>
				</tr>
			</table>
			</div>
		</td>
	</tr>
	<%-- Emergency Contact Information --%>
	<tr id="trEmgnContactInfo">
		<td class="alignLeft">
		<div class="div-Table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class='pnlTop pnlWidth'></td>
					<td class='pnlTopR'></td>
				</tr>
				<tr>
					<td colspan='2' class='pnlBottomTall paddingCalss' valign='top'>
					
					<div class="table-Row one-Col">
						<div class="table-Data one-Col">
							<label id="lblEmgnCntInformation"  class='fntBold hdFontColor'></label>
						</div>
					</div>
						
						<div class="table-Row one-Col" id="trEmgnTitle">
							<div class="table-Data left-TD">
								<label id="lblTitle"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<select id='selEmgnTitle' size='1' style="width: 45px;" tabindex="23" name='contactInfo.emgnTitle'></select>
								<label id="lblEmgnTitleMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<div class="table-Row two-Col" id="tblEmgnFirstName">
							<div class="table-Data left-TD">
								<label id="lblFirstName"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtEmgnFName' name="contactInfo.emgnFirstName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="24" />
								<label id="lblEmgnFNameMand" class='mandatory'>*</label>
							</div>
						</div>
						<div class="table-Row two-Col" id="tblEmgnLastName">
							<div class="table-Data left-TD">
								<label id="lblLastName"></label>
							</div>
							<div class="table-Data right-TD">
								<font>:</font>
								<input type='text' id='txtEmgnLName' name="contactInfo.emgnLastName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="25" />
								<label id="lblEmgnLNameMand" class='mandatory'>*</label>
							</div>
						</div>
						
						<div id="trEmgnPhoneNo">
							<div class="table-Row one-Col thin-row trPhoneNoLabel">
								<div class="table-Data left-TD">
									<label>&nbsp;</label>
								</div>
								<div class="table-Data right-TD">
									<label id="lblCountryCode" class='fntSmall'></label>
									<label id="lblAreaCode" class='fntSmall areaCode'></label>
									<label id="lblNumber" class='fntSmall'></label>
								</div>
							</div>
							
							<div class="table-Row one-Col">
								<div class="table-Data left-TD">
									<label id="lblPhoneNo"></label>
								</div>
								<div class="table-Data right-TD">
									<font class="changeOverFont">:</font>
									<div class="changeOver">
									<input type='text' id='txtEmgnPCountry' name="contactInfo.emgnLCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="26" />
									<input type='text' id='txtEmgnPArea' name="contactInfo.emgnLArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="27" class="areaCode"/> 
									<input type='text' id='txtEmgnPhone' name="contactInfo.emgnLNumber" style='width:100px;text-align:right;' maxlength='10'  tabindex="28" />
									</div>
									<label id="lblEmgnPhoneNoMand" class='mandatory'>*</label>
								</div>
							</div>
						</div>
						<div id="tblEmgnEmail">
							<div class="table-Row one-Col">
								<div class="table-Data left-TD">
									<label id="lblEmailAddress"></label>
								</div>
								<div class="table-Data right-TD">
									<font>:</font>
									<input type='text' id='txtEmgnEmail' name="contactInfo.emgnEmail" style='width:165px;' maxlength='100'  tabindex="29" />
									<label id="lblEmgnEmailMand" class='mandatory'>*</label>
								</div>
							</div>
							
							<div class="table-Row one-Col">
								<div class="table-Data left-TD">
									<label id="lblVerifyEmail"></label>
								</div>
								<div class="table-Data right-TD">
									<font>:</font>
									<input type='text' id='txtVerifyEmgnEmail' name="txtVerifyEmgnEmail" style='width:165px;' maxlength='100' tabindex="30" />
								</div>
							</div>
						</div>
						
						<div class="table-Row  one-Col">
							<div class="table-Data one-Col">
								<label class="fntSmall">&nbsp;</label>
							</div>
						</div>
						<div class="end-Table"></div>
						
					</td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td class='rowSingleGap'></td>
	</tr>
	<tr id="trUpdateCustProfile">
		<td class="alignLeft">
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td colspan="2" class='rowSingleGap'></td>
				</tr>
				<tr>
					<td width="5%"><input type="checkbox" name="contactInfo.updateCustomerProf" id="chkUpdateCustProf" value="true"/>&nbsp;</td>
					<td class="alignLeft"><label id="lblUpdateCustProfile"></label></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class='rowSingleGap'></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="6" class="alignLeft">
					<div class="buttonset">
						<!-- Button -->
						<table border='0' cellpadding='0' cellspacing='0'  align="center">
							<tr>
								<td class="alignLeft"><button type="button" name="btnPrevious" tabindex="31"  value="Previous" class="Button ui-state-default ui-corner-all"></button></td>
								<td style="width:10px;">&nbsp;</td>		
								<td><button type="button" name="btnSOver"  tabindex="32"  value="Start Over" class="Button ButtonMedium ui-state-default ui-corner-all"></button></td>	
								<td style="width:10px;">&nbsp;</td>				
								<td class="alignRight"><button type="button" name="btnNext" tabindex="30" value="Next" class="Button ui-state-default ui-corner-all" ></button></td>
							</tr>
						</table>
					</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>