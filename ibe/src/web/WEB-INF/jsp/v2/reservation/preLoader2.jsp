<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<c:if test='${requestScope.defaultCCODE == "G9" }'>
<!-- Google Website Optimizer Control Script -->
<!-- G9 -->
	<script>
	function utmx_section(){}function utmx(){}
	(function(){var k='0528702568',d=document,l=d.location,c=d.cookie;function f(n){
	if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.indexOf(';',i);return escape(c.substring(i+n.
	length+1,j<0?c.length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;
	d.write('<sc'+'ript src="'+
	'http'+(l.protocol=='https:'?'s://ssl':'://www')+'.google-analytics.com'
	+'/siteopt.js?v=1&utmxkey='+k+'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='
	+new Date().valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
	'" type="text/javascript" charset="utf-8"></sc'+'ript>')})();
	</script>
	<!-- End of Google Website Optimizer Control Script -->
	<!-- Google Website Optimizer Tracking Script -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
	  _gaq.push(['gwo._trackPageview', '/0528702568/test']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
<!-- End of Google Website Optimizer Tracking Script -->
</c:if>
<c:if test='${requestScope.defaultCCODE == "3O" }'>
<!-- 3O -->
<!-- Google Website Optimizer Control Script -->
	<script>
	function utmx_section(){}function utmx(){}
	(function(){var k='2980512962',d=document,l=d.location,c=d.cookie;function f(n){
	if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.indexOf(';',i);return escape(c.substring(i+n.
	length+1,j<0?c.length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;
	d.write('<sc'+'ript src="'+
	'http'+(l.protocol=='https:'?'s://ssl':'://www')+'.google-analytics.com'
	+'/siteopt.js?v=1&utmxkey='+k+'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='
	+new Date().valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
	'" type="text/javascript" charset="utf-8"></sc'+'ript>')})();
	</script>
	<!-- End of Google Website Optimizer Control Script -->
	<!-- Google Website Optimizer Tracking Script -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
	  _gaq.push(['gwo._trackPageview', '/2980512962/test']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
<!-- End of Google Website Optimizer Tracking Script -->
</c:if>
<c:if test='${requestScope.defaultCCODE == "E5" }'>
<!-- E5 -->
<!-- Google Website Optimizer Control Script -->
	<script>
	function utmx_section(){}function utmx(){}
	(function(){var k='3403576409',d=document,l=d.location,c=d.cookie;function f(n){
	if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.indexOf(';',i);return escape(c.substring(i+n.
	length+1,j<0?c.length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;
	d.write('<sc'+'ript src="'+
	'http'+(l.protocol=='https:'?'s://ssl':'://www')+'.google-analytics.com'
	+'/siteopt.js?v=1&utmxkey='+k+'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='
	+new Date().valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
	'" type="text/javascript" charset="utf-8"></sc'+'ript>')})();
	</script>
	<!-- End of Google Website Optimizer Control Script -->
	<!-- Google Website Optimizer Tracking Script -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
	  _gaq.push(['gwo._trackPageview', '/3403576409/test']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
<!-- End of Google Website Optimizer Tracking Script -->
</c:if>
</head>
<body>
<c:import url="../common/pageLoading.jsp"/>
<!-- 
<input type="radio" id="radDesign_old" name="radDesign" value="0" onclick="selectFlow(this)"/> Old Flow <br/>
<input type="radio" id="radDesign_old" name="radDesign" value="1" onclick="selectFlow(this)"/> New Flow
 -->
<form id="myform" name="myform" action="showReservation.action" method="post">
	<input type="hidden" id="hdnParamData" name="hdnParamData" value="<c:out value="${requestScope.hdnParamData}" escapeXml="false" />"/>
	<input type="hidden" id="hdnCarrier" name="hdnCarrier" value="<c:out value="${requestScope.hdnCarrier}" escapeXml="false" />"/>
	<input type="hidden" id="hdnParamUrl" name="hdnParamUrl" value="<c:out value="${requestScope.hdnParamUrl}" escapeXml="false" />"/>
	<input type="hidden" id="hdnDesign" name="hdnDesign" value="0"/>
	<input type="hidden" id="hdnLanguage" name="hdnLanguage" value="<c:out value="${param.hdnLanguage}" escapeXml="false" />"/> 
</form>
<script type="text/javascript">
<!--
function selectFlow(obj){
	document.forms[0].hdnDesign.value = obj.value;
	document.forms[0].submit()
}
-->
</script>
	<script>utmx_section("versionID")</script>
		<script type="text/javascript">
			document.forms[0].hdnDesign.value = 0;
			document.forms[0].submit();
		</script>
	</noscript>
</body>
</html>