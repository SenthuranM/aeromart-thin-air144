<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%--
<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" }'>
	<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
		  _gaq.push(['gwo._trackPageview', '/1390862417/goal']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	<!-- End of Google Website Optimizer Tracking Script -->
</c:if>
<c:if test='${sessionScope.sessionDataDTO.carrier == "3O" }'>
		<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
		  _gaq.push(['gwo._trackPageview', '/1390862417/goal']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	<!-- End of Google Website Optimizer Tracking Script -->
</c:if>
<c:if test='${sessionScope.sessionDataDTO.carrier == "EG" }'>
	<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
		  _gaq.push(['gwo._trackPageview', '/1390862417/goal']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	<!-- End of Google Website Optimizer Tracking Script -->
</c:if> 
--%>
	<%@ include file='../common/interlinePgHD.jsp' %>
	<link rel='stylesheet' type='text/css' href='../css/table_layout_EN.css'/>		
	<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/> 	
	<link rel="stylesheet" type="text/css" href="../css/remove-jquery_no_cache.css"/>	
	
	<script src="../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/common/jquery.msAccordion.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/jquery.resizecrop-1.0.3.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
	<script type="text/javascript" src="../js/v2/common/jquery.stiky.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/common/jquery.summaryPanel.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/anciConfig.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/anciLayouts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/anciStructures.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/socialSeat.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/interLinePax.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/interLineAnci.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/interLinePayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/interLineContainer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/modifyRes/modifyConfirm.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jquery.readonly.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
	<script src="../js/v2/common/cardValidator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
 
	<script type="text/javascript">
		var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
		var sysDefLang = <c:out value="${requestScope.sysDefLang}" escapeXml="false"/>
		var paxCat = <c:out value="${requestScope.paxCat}" escapeXml="false"/>;
		var errorMessage = "<c:out value='${requestScope.error}' escapeXml='false'/>";	
		var fareType_IMG_PATH = '<c:out value="${requestScope.sysImagePath}" escapeXml="false" />';
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear();
		var GLOBALS = <c:out value="${requestScope.systemDefaultParam}" escapeXml="false" />;

	</script>
</head>

<body>
<c:import url="../common/pageLoading.jsp"/>
<div id="divLoadBg">
<form action="" id="frmReservation" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgContainer">
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<!-- Top Banner -->
					<c:import url="../../../../ext_html/header.jsp" />
					<!-- Content holder -->
					
					<!-- Registered user menu place holder -->

					<!-- Content holder -->
					<tr>
					<td colspan="2" class="mainbody" class="alignLeft">
					<div id="sortable-ul">
					<div class="sortable-child">
					<!-- Left Column -->
					<div style="width: 250px;background: #fff"  class="floater">
							<div id="divSummaryPane">
								<div class="bookingSummary"></div>
								<div class="paymentSummary"></div>
								
							</div> 
								<%-- <%@ include file="inc_tabSummaryPane.jsp" %> --%>
					</div>
					&nbsp;
					</div>
					<div class="sortable-child">
					<!-- Right Column -->
					<div class="rightColumn">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td colspan="2">
							<div id="regUser" style="display: none">
								<%-- Logged User --%>
								<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tbody>
										<tr>
											<td width="50%" class="GridHeaderHighlight alignLeft">
											<span id="spnUN">
												<label id="lblWelcome"	class="fntWhite paddingL5">  </label>&nbsp;
												<label id="lblTxtName" class="fntBold fntWhite fontCapitalize">
												</label>
											</span>
											</td>
											<td align="center" width="39%" class="logUserBG">
											</td>
											<td width="1%" class="logUserCorT"></td>
										</tr>
										<tr style="display: none">
											<td>
										   	   <div id="linkdinWrapper">
          									         <script type="IN/Login" ></script>
          				                      </div>
          				                   </td>
										</tr>
									</tbody>
								</table>
								
								<div style="padding-bottom: 10px"></div>
								 </div>
								<%-- Logged User --%>
						</td>
					</tr>
					
					<tr>
						<td class='pnlTop pnlWidth'></td>
						<td class='pnlTopR'></td>
					</tr>
					<tr>
					<td class='pnlBottomTall'  colspan='2' valign="top" align="center">
						<table width='100%' border='0' cellpadding='0' cellspacing='0' class='alignRight'>
						<tr>
							<td align='left'>
								<div style="width: 100%;" class="stepsContainer">
									<c:import url="../../../../ext_html/booking_process.jsp" />
								</div>
							</td>
						</tr>
						<tr>
							<td align='left'>
								<div id='divAnciPromoBanner' style="position: relative;"></div>
							</td>
						</tr>
						
						<tr>
							<td>
								<div id="page_1"></div>										
								<div id="page_2"></div>
								<div id="page_3"></div>
								<!-- Grid -->
								<div id="divPassenger" style="display:none;">
									<div class='tabBody'>										
									  <c:choose>
										  <c:when test="${param.paymentFlow =='PAYMENT_RETRY' || modifyAncillary || param.modifySegment || param.addGroundSegment || makePayment}">
										  </c:when>
										  <c:otherwise>
										  	<%@ include file="inc_tabPassenger.jsp" %>
										  </c:otherwise>
									  </c:choose>
									</div>											
								</div>
								<div id="divAncillary" style="display:none;">
									<div class='tabBody'>										
										 <c:choose>
											 <c:when test="${param.paymentFlow =='PAYMENT_RETRY' || makePayment}">											
											 </c:when>
											 <c:otherwise>
										  		<%@ include file="inc_tabAncillary.jsp" %>
										 	 </c:otherwise>
										 </c:choose>
									</div> 
								</div>
								<div id="divPayment" style="display:none;">
									<div class='tabBody'>
										<%@ include file="inc_tabPayment.jsp" %> 
									</div>
								</div>
								<%--<div id="divModifyConfirm" style="display:none;">									
									<div class='tabBody'>
										<%@ include file="inc_tabModifyConfirm.jsp" --%> 
								<%-->	</div>
								</div> --%>
						</td></tr>
					</table>
					</td></tr>
					</table>
					</div>
					</div>
					</div>
						</td>
					</tr>
					
					<!-- Bottom AA Logo -->
					<tr>
					<td class='appLogo' colspan="2"></td>
					</tr>
					<c:import url="../../../../ext_html/cfooter.jsp" />
				</table>
			</td>
		</tr>
	</table>
	<%@ include file='../common/iBECommonParam.jsp'%>
	<%@ include file='../common/reservationParam.jsp'%>
	<input type="hidden" name="paymentType" id="paymentType" value="NORMAL"/>
	<input type="hidden" id="fromSecure" name="fromSecure" value='<c:out value="${param.fromSecure}" escapeXml="false"/>'/>
	<input type="hidden" id="mode" name="mode" value='<c:out value="${param.mode}" escapeXml="false"/>'/>
	<input type="hidden" id="resFlexiAlerts" name="resFlexiAlerts" value='<c:out value="${param.resFlexiAlerts}" escapeXml="false"/>'/>	
	<input type="hidden" id="requestSessionIdentifier" name="requestSessionIdentifier" value='<c:out value="${param.requestSessionIdentifier}" escapeXml="false"/>'/> 
	
	<c:if test="${param.modifySegment == true || param.requoteFlightSearch == true|| modifyAncillary || param.addGroundSegment  || makePayment}">
		<input type="hidden" id="pnr" name="pnr" value='<c:out value="${param.pnr}" escapeXml="false"/>'/>
		<input type="hidden" id="groupPNR" name="groupPNR" value='<c:out value="${param.groupPNR}" escapeXml="false"/>' />
		<input type="hidden" id="version" name="version" value='<c:out value="${param.version}" escapeXml="false"/>'/>
		<input type="hidden" id="modifingFlightInfo" 	name="modifingFlightInfo" value='<c:out value="${param.modifingFlightInfo}" escapeXml="false"/>' />	
		<input type="hidden" id="resModifySegmentRefNos" name="modifySegmentRefNos" value='<c:out value="${param.modifySegmentRefNos}" escapeXml="false"/>'/>
		<input type="hidden" id="oldAllSegments" name="oldAllSegments" value='<c:out value="${param.oldAllSegments}" escapeXml="false"/>' />
		<input type="hidden" id="modifySegment" name="modifySegment" value='<c:out value="${param.modifySegment}" escapeXml="false"/>'/>
		<input type="hidden" id="modifyAncillary" name="modifyAncillary" value='<c:out value="${modifyAncillary}" escapeXml="false"/>'/>
		<input type="hidden" id="selectedAncillary" name="selectedAncillary" value='<c:out value="${selectedAncillary}" escapeXml="false"/>'/>
		<input type="hidden" id="paxJson" name="paxJson" value='<c:out value="${paxJson}" escapeXml="false"/>'/>
		<input type="hidden" id="contactInfoJson" name="contactInfoJson" value='<c:out value="${contactInfoJson}" escapeXml="false"/>'/>	
		<input type="hidden" id="resOldFareID" 		name="oldFareID" value='<c:out value="${param.oldFareID}" escapeXml="false"/>' />
		<input type="hidden" id="resOldFareType" 	name="oldFareType" value='<c:out value="${param.oldFareType}" escapeXml="false"/>' />
		<input type="hidden" id="resOldFareAmount" 	name="oldFareAmount" value='<c:out value="${param.oldFareAmount}" escapeXml="false"/>' />
		<input type="hidden" id="resOldFareCarrierCode" 	name="oldFareCarrierCode" value='<c:out value="${param.oldFareCarrierCode}" escapeXml="false"/>' />
		<input type="hidden" id="hasInsurance" name="hasInsurance" 	value='<c:out value="${param.hasInsurance}" escapeXml="false"/>'/>
		<input type="hidden" id="addGroundSegment" name="addGroundSegment" 	value='<c:out value="${param.addGroundSegment}" escapeXml="false"/>'/>
		<input type="hidden" id="totalSegmentCount" name="totalSegmentCount" value='<c:out value="${param.totalSegmentCount}" escapeXml="false"/>'/>
		<input type="hidden" id="makePayment" name="makePayment" value='<c:out value="${makePayment}" escapeXml="false"/>'/>
		<input type="hidden" id="requoteFlightSearch" name="requoteFlightSearch" value='<c:out value="${param.requoteFlightSearch}" escapeXml="false"/>'/>
		<input type="hidden" id="cancelSegmentRequote" name="cancelSegmentRequote" value='<c:out value="${param.cancelSegmentRequote}" escapeXml="false"/>'/>
		<input type="hidden" id="nameChangeRequote" name="nameChangeRequote" value='<c:out value="${param.nameChangeRequote}" escapeXml="false"/>'/>
		<input type="hidden" id="nameChangePaxData" name="nameChangePaxData" value='<c:out value="${param.nameChangePaxData}" escapeXml="false"/>'/>
		<input type="hidden" id="balanceQueryData" name="balanceQueryData" value='<c:out value="${balanceQueryData}" escapeXml="false"/>'/>	
		<input type="hidden" id="availableCOSRankMap" name="availableCOSRankMap" value='<c:out value="${availableCOSRankMap}" escapeXml="false"/>'/>
		<input type="hidden" id="jsonOnds" name="jsonOnds" value='<c:out value="${jsonOnds}" escapeXml="false"/>'/>
		<input type="hidden" id="onPreviousClick" name="onPreviousClick" />		
	</c:if>
	<input type="hidden" id="paymentFlow" name="paymentFlow" value='<c:out value="${param.paymentFlow}" escapeXml="false"/>' />
</form>
</div>
<div class="zoomer" style="display:none;position:absolute;"><table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0"><tr>
<td valign="middle" align="center" class="loadImage"><img src="../images/Loading_no_cache.gif"/></td></tr><tr><td valign="bottom"><div class="imgDescription"></div></td></tr></table></div>
<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />
<div id='divLoadMsg' class="mainPageLoader">
	<%@ include file='../common/includeLoadingMsg.jsp' %>
</div>
<div id="newPopItem" style="display: none;" >
	<%@ include file='../../common/includePopup.jsp' %>
</div>
<form id="submitForm">
	<input type="hidden" name="sessionPNR" id="sessionPNR" value="" />
</form>
<div id="cardInputPannel">
	<iframe name="cardInputs" id="cardInputs" class="paymentCardInputs"  style="position: absolute;top: 0px;left: 0px" frameborder="0" marginwidth="0" marginheight="0" height="0%" width="0%" src="showBlank">
	</iframe>
</div>		

</body>
<!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/25/2013 -->
<!-- tag were modified as requiest of Ariarabia on  May 26, 2015 -->

 <c:if test='${sessionScope.sessionDataDTO.carrier == "G9" || sessionScope.sessionDataDTO.carrier == "3O" || sessionScope.sessionDataDTO.carrier == "E5" || sessionScope.sessionDataDTO.carrier == "9P"}'>
 	<script type="text/javascript">
        (function() {
            try {
                var viz = document.createElement("script");
                viz.type = "text/javascript";
                viz.async = true;
                viz.src = ("https:" == document.location.protocol ?"https://mea-tags.vizury.com" : "http://mea-tags.vizury.com")+ "/analyze/pixel.php?account_id=VIZVRM3429";

                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(viz, s);
                viz.onload = function() {
                    try {
                        pixel.parse();
                    } catch (i) {
                    }
                };
                viz.onreadystatechange = function() {
                    if (viz.readyState == "complete" || viz.readyState == "loaded") {
                        try {
                            pixel.parse();
                        } catch (i) {
                        }
                    }
                };
            } catch (i) {
            }
        })();
    </script>
 </c:if>
 
<c:if test='${sessionScope.sessionDataDTO.carrier == "G9"}'>
      <script type='text/javascript'>
		var _spapi = _spapi || [];
		_spapi.push(['_partner', 'airarabia']);
		(
		function()
		{
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'airarabia.api.sociaplus.com/partner.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
		}
		)();
	</script>
			
</c:if>
<%@ include file='../common/inline_Tracking.jsp' %>
<!-- Added as per request on sub: Fwd: [ISA-#1195] Tracking pixel IAS on 03/25/2013 -->
</html>