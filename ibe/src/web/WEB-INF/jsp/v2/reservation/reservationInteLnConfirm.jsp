<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%--
<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" }'>
	<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
		  _gaq.push(['gwo._trackPageview', '/0528702568/goal']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	<!-- End of Google Website Optimizer Tracking Script -->
</c:if>
<c:if test='${sessionScope.sessionDataDTO.carrier == "3O" }'>
		<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
		  _gaq.push(['gwo._trackPageview', '/2980512962/goal']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	<!-- End of Google Website Optimizer Tracking Script -->
</c:if>
<c:if test='${sessionScope.sessionDataDTO.carrier == "E5" }'>
	<!-- Google Website Optimizer Tracking Script -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['gwo._setAccount', 'UA-10278800-1']);
		  _gaq.push(['gwo._trackPageview', '/3403576409/goal']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	<!-- End of Google Website Optimizer Tracking Script -->
</c:if> 
 --%>
	<%@ include file='../common/interlinePgHD.jsp' %>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<link rel='stylesheet' type='text/css' href='../css/table_layoutV3_EN.css'/>
	<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/>
	<script src="../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/reservation/reservationConfirmatioSupport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script type="text/javascript">
		var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';		
	</script>
</head>
<body>
<c:import url="../common/pageLoading.jsp"/>
<div id="processingRes" style="display: none">
	<div class="spMsg" align="center">
		<label id="lblBookingProcessing">Your booking is processing, Please wait...</label>
	</div>
</div>
<div id="divLoadBg" style="display:none">
<form action="" id="frmConfirmation" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgInterlineConfirm">
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<%-- Top Banner --%>
					<c:import url="../../../../ext_html/header.jsp" />
					<tr>
						<td>
							<table width="0" height="0">
								<tr><td><span id='spnSpotAdd'></span></td></tr>
							</table>
						</td>
					</tr>
					<%-- Content holder --%>
					<%-- Left Column --%>
					<tr>
					<td colspan="2" class="mainbody" class="alignLeft" >
						<div id="sortable-ul">
							<div class="sortable-child">
							<%-- Left Column --%>
							<div style="width: 250px;background: transparent;"  class="floater">
							
							<div id="divSummaryPane" style="display: none;">
							<div class="bookingSummary"> 
								<div class="Page-heading">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="headerRow">
										<tr>
											<td class='headerBG hLeft'></td>
											<td class='headerBG hCenter'><label id="lblBookingSummary" class='fntBold gridHDFont'></label></td>
											<td class='headerBG hRight'></td>
										</tr>
									</table>
									</td></tr>
									<tr>
											<td class='mLeft'></td>
											<td class='mCenter'>
													<div class="pane-body">
													<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgPassenger">
														<tr>
															<td class='pnlTop'></td>
														</tr>
														<tr>
															<td class='pnlBottom add-padding alignLeft'>
																<table width="100%" border="0" cellspacing="1" cellpadding="1">
																	<tr>
																		<td>
																		<label id="adultCount"></label>
																		<label id="childCount"></label>
																		<label id="infantCount"></label>
																		</td>            
																	</tr>
																	</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate" class="flightTemplateC">
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom add-padding alignLeft' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																				<tr>
																					<td colspan="3"><label id="segmentCode"  class='fntBold hdFontColor'/></td>            
																				</tr>
																				<tr>
																					<td width="50%"><label id="lblflightNoHD"></label></td>
																					<td width="1%"><label>:</label></td>
																					<td width="49%"><label id="flightNumber"/></td>
																				</tr>
																				<tr>
																					<td><label id="lbldepatureDate"></label> </td>
																					<td><label>:</label></td>
																					<td>
																						<label id="departureDate" class="date-disp"></label>
																						<label id="departureDateValue" class="date-hid" style="display:none"></label>
																					</td>
																				</tr>
																				<tr>
																					<td><label id="lbldepatureTime"></label></td>
																					<td><label>:</label></td>
																					<td><label id="departureTime"/></td>
																				</tr>
																				<tr>
																					<td><label id="lblarrivalTime"></label></td>
																					<td><label>:</label></td>
																					<td><label id="arrivalTime"/></td>
																				</tr>
																				<tr>
																					<td><label id="lblDuration"></label></td>
																					<td><label>:</label></td>
																					<td><label id="duration"/></td>
																				</tr>
																				<tr><td colspan="3" ></td></tr>
    																			<tr><td colspan="3" ><span name="anciDetails" >
    																			</span></td></tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													</div>
												</td>
												<td class='mRight'></td>
											</tr>
											<tr><td colspan="3">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerRow">
											<tr>
											<td class="headerBG bLeft"></td>
											<td class="headerBG bCenter">&nbsp;</td>
											<td class="headerBG bRight"></td>
											</tr></table></td>
											</tr>
											
										</table>
									  </div>
								</div>
								
								<%-- Payment Summary --%>
								<div class="paymentSummary"> 
									<div class="Page-heading">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="headerRow">
											<tr>
												<td class='headerBG hLeft'></td>
												<td class='headerBG hCenter'><label id="lblPaymentSummary" class='fntBold gridHDFont'></label></td>
												<td class='headerBG hRight'></td>
											</tr>
										</table>
										</td></tr>
										<tr>
												<td class='mLeft'></td>
												<td class='mCenter'>
													<div class="pane-body">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" >
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																		<td class='pnlTopR'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom alignLeft' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1' class="flightTemplateC">
																				<tr name='trAirFare'>
																					<td width="55%"><label id="lblAirFare"></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" align="left"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valAirFare' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr name='trCharges'>
																					<td width="55%"><label id="lblCharges"> &amp;
																					</label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valCharges' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr id='trFlexiCharges' style="display: none;">
																					<td width="55%"><label id="lblFlexiCharges"></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valFlexiCharges' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr id='trSeatCharges'>
																					<td><label id='lblSeatSelection' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valSeatCharges' class='fntDefault txtBold'></label></td>
																				</tr>																				
																				<tr id='trInsuranceCharges'>
																					<td><label id='lblTravelSecure' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valInsuranceCharges' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr id='trMealCharges'>
																					<td><label id='lblMealSelection' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valMealCharges' class='fntDefault txtBold'></label></td>
																				</tr>	
																				<tr name='trBaggageCharges'>
																					<td><label id='lblBaggageSelection' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valBaggageCharges' class='fntDefault txtBold'></label></td>
																				</tr>
                                                                                <tr name='trSsrCharges'>
																					<td><label id='lblSsrSelection' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valSsrCharges' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr id='trAptCharges'>
																					<td><label id='lblAptSelection' ></label></td>
																					<td><label>:&nbsp;</label></td>
																					<td class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td align="right"><label name='valAptCharges' class='fntDefault txtBold'></label></td>
																				</tr>									
																				<tr id='trTxnFee'>
																					<td width="55%"><label id="lblTNXFee"></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>	
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valTxnFee' class='fntDefault txtBold'></label></td>
																				</tr>
																				<tr>
																					<td colspan="4" height="20px"></td>
																				</tr>
																				<tr>
																					<td width="55%"><label id="lblTotal" class='fntBold'></label></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valBaseCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valTotal' class='fntDefault fntBold hdFontColor' ></label></td>
																				</tr>
																				<tr style="display: none;" name="trSelectedCurr">
																					<td width="55%"></td>
																					<td width="2%"><label>:&nbsp;</label></td>
																					<td width="10%" class="alignLeft"><label name='valSelectedCurrCode' class='fntDefault txtBold'></label></td>
																					<td width="33%" align="right"><label name='valSelectedTotal' class='fntDefault txtBold hdFontColor'></label></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													</div>
												</td>
												<td class='mRight'></td>
											</tr>
											<tr><td colspan="3">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerRow">
											<tr>
											<td class="headerBG bLeft"></td>
											<td class="headerBG bCenter">&nbsp;</td>
											<td class="headerBG bRight"></td>
											</tr></table></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							</div>
						&nbsp;
						</div>
						<div class="sortable-child">
						<%-- Right Column --%>
						<div class="rightColumn">
							<table width='100%' border='0' cellpadding='0' cellspacing='0' class="alignRight">
								<tr id="trBookingSteps" style="display: none">
									<td align='center'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='pnlTop pnlWidth'></td>
												<td class='pnlTopR'></td>
											</tr>
											<tr>
												<td colspan='2' class='pnlBottom' valign='top' align='center' style="height: 38px">
													<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<tr><td class="alignLeft">
															<div style="width: 100%;" class="stepsContainer">
																<c:import url="../../../../ext_html/booking_process.jsp" />
															</div>
														</td></tr>
														<tr>
															<td class="rowSingleGap">
																	</td>
														</tr>
																												
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='pnlTop pnlWidth'></td>
												<td class='pnlTopR'></td>
											</tr>
											<tr>
												<td colspan='2' class='pnlBottom' valign='top' align='center'>
													<table width='98%' border='0' cellpadding='1' cellspacing='1'>
														<tr>
															<td class="alignLeft" class="pageName">
															<label id="lblItinerary"></label>
															</td>
														</tr>
														
														<tr>
															<td class="alignLeft">
																<label id="lblMsgThank"></label>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td class="alignLeft">
																<div class="spMsg">
																	<div id="lblCreditcardProof"></div>
																</div>															
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr id="transactionResultPanel" style="display: none">
															<td class="alignLeft">
																<div class="spMsg">
																	<label class="fntBold" id="transaction.header"></label>
																	<table border="0">
																		<tr>
																			<td width="150px" class="alignLeft"><label id="transaction.time"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionTime"></label></td>
																			<td width="150px" class="alignLeft"><label id="transaction.status"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionStatus"></label></td>
																		</tr>
																		<tr>
																			<td width="150px" class="alignLeft"><label id="transaction.amount"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionAmount"></label></td>
																			<td width="150px" class="alignLeft"><label id="transaction.referenceID"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionReferenceID"></label></td>
																		</tr>
																		<tr>
																			<td width="150px" class="alignLeft"><label id="transaction.merchanttrackID"></label></td>
																			<td width="150px" class="alignLeft">:<label id="transactionMerchanttrackID"></label></td>																			
																		</tr>
																	</table>
																</div>															
															</td>
														</tr>
													
														<tr>
															<td class="alignLeft">
																<label id="lblSummary" class='fntBold hdFont hdFontColor'>Reservation Summary</label>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td align='center'>
																<table width='95%' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																	<td colspan="2">
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td>
																					<img src='../images/n058_no_cache.gif' alt='Confirm'/>
																				</td>
																				<td colspan='2' class="alignLeft">
																					<label id="lblMsgSentMail" class='fntBold'> Your booking has been successfully completed and your itinerary has been sent to you by email </label>
																					<label id="lblMsgBookingSuccess" class='fntBold' style="display: none;"> Your booking has been successfully completed</label>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="3" class="alignLeft"><div class="spMsg" id="divRefundMessage" style="display: none;">
																				<label id="lblRefundMessage"></label>
																				</div></td>
																			</tr>
																		</table>
																	</td>
																	</tr>
																	<tr>
																		<td class='rowGap' colspan="2"></td>
																	</tr>
																	<tr>
																		<td colspan='2' class='rowGap'></td>
																	</tr>
																	<tr id='trInsuranceSellFailed' style='display:none;'>
																		<td colspan='2' class='alignLeft'>
																			<label id='msgInsuranceSellFailed'>&nbsp;</label> 
																		</td>																		
																	</tr>
																	<tr>																
																		<td colspan="4">
																			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td class="alignLeft" width="55%"> 
																						<label  id="lblPnr">Reservation Number</label><label class="paddingL3">:</label>	
																						<label id="lblPnrValue" class="fntBold"> <c:out value='${sessionScope.sesPNRNO}' escapeXml='false' /></label>
																					</td>
																					<td  width="45%" class="alignLeft" > <span id="spnPolicy">
																						<label  id="lblPolicy"></label><label class="paddingL3" >:</label>	
																						<label id="lblPolicyValue" class="fntBold"> </label></span>
																					</td>
																				</tr>
																				<tr>
																					<td  width="55%" class="alignLeft" >
																						<label id="lblPnrDate"></label><label class="paddingL3">:</label>
																						<label id="lblPnrDateValue" class="fntBold date-disp"></label>
																						<label id="lblPnrDateValueHid" class="fntBold date-hid" style="display:none"></label>
																					</td>
																					<td width='45%'  id="tdInsTypeDisplay" class="alignLeft" style="display: none;" >
																						<label id="lblInsuranceType"></label>  
																						<label class="padding3"> : </label>
																						<label id="lblInsuranceTypeValue" class="fntBold"></label>
																					</td>
																				</tr>
																			</table>																		
																		</td>																		
																	</tr>												
																	
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr id="depFlightPanel" style="display: none;">
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' >											
											<tr>
												<td colspan='2' valign='top' align='center'>
													<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable trconFPanel">
														<tr>
															<td class="Gridwt alignLeft" colspan="7"> <label id="lblDeparting" class='fntBold hdFontColor'></label></td>
														</tr>
														<tr>
															<td rowspan="2" class='gridHD' align="center"><label id="lblSegment" class='gridHDFont fntBold'></label></td>
															<td colspan="2" class='gridHD' align="center"><label id="lblDeparture" class='gridHDFont fntBold'> </label></td>
															<td colspan="2" class='gridHD' align="center"><label id="lblArrival" class='gridHDFont fntBold'></label></td>
															<td rowspan="2" class='gridHD' align="center"><label id="lblDFlightNo" class='gridHDFont fntBold'></label></td>
															<td rowspan="2" class='gridHD' align="center"><label id="lblDuration" class='gridHDFont fntBold'></label></td>
														</tr> 
														<tr>            
															<td class='gridHDDark' align="center"><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align="center"><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align="center"><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align="center"><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
														</tr>
														<tr id="departueFlight">
															<td width='35%' class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="segmentCode"></label></td>
															<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'>
																<label id="departureDate" class="date-disp"></label>
																<label id="departureDateValue" class="date-hid" style="display:none"></label>
															</td>
															<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="departureTime"></label></td>
															<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'>
																<label id="arrivalDate" class="date-disp"></label>
																<label id="arrivalDateValue" class="date-hid" style="display:none"></label>
															</td>
															<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
															<td width='10%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
															<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>															
														</tr>            
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr id="trReturnFlt" style="display: none;">
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>											
											<tr>
												<td colspan='2'  valign='top' align='center'>
													<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable trconFPanel">
														<tr>
															<td class="Gridwt alignLeft" colspan="7"><label id="lblReturning" class='fntBold hdFontColor'> </label></td>
														</tr>
														<tr>
															<td rowspan="2" class='gridHD' align="center"><label id="lblRSegment" class='gridHDFont fntBold'></label></td>
															<td colspan="2" class='gridHD' align="center"><label id="lblRDeparture" class='gridHDFont fntBold'> </label></td>
															<td colspan="2" class='gridHD' align="center"><label id="lblRArrival" class='gridHDFont fntBold'></label></td>
															<td rowspan="2" class='gridHD' align="center"><label id="lblRFlightNo" class='gridHDFont fntBold'></label></td>
															<td rowspan="2" class='gridHD' align="center"><label id="lblDuration" class='gridHDFont fntBold'></label></td>
														</tr> 
														<tr>            
															<td class='gridHDDark' align="center"><label id="lblRDepartureDate" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align="center"><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align="center"><label id="lblRArrivalDate" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align="center"><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
														</tr>
														<tr id="returnFlight">
															<td width='35%' class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="segmentCode"></label></td>
															<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="departureDate"></label></td>
															<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="departureTime"></label></td>
															<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
															<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
															<td width='10%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
															<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>															
														</tr>                
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr id="trPaxDetail" style="display: none;">
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>											
											<tr>
												<td colspan='2'  valign='top' class="alignLeft">
													<table width="100%" border="0" cellspacing="0" cellpadding="3" class="GridTable trconFPanel">
														<tr>
															<td class="gridHD">
																<label id="lblPaxInfo" class='gridHDFont fntBold'></label>
															</td>
														</tr>
														<tr id="tradultfGrid">
															<td class='noPadding'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>				
																		<td class='gridHDDark' width="7px"></td>																
																		<td class='gridHDDark alignLeft'  width='60%' ><label id="lblAdultHD" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='40%'><label id="lblServices" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr id="adultGrid">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'  width="7px"><label id="itnSeqNo"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'><label id="itnPaxName"></label></td>
																		<td  class='defaultRowGap rowColor bdBottom bdRight'><span id="additionalInfo" class="fntDefault"></span></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr id="trChildGrid">
															<td class='noPadding'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>		
																		<td class='gridHDDark' width="7px"></td>																
																		<td class='gridHDDark alignLeft' width='60%' ><label id="lblChildHD" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='40%'><label id="lblServices" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr id="childGrid">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight' width="7px"><label id="itnSeqNo"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'><label id="itnPaxName"></label></td>
																		<td class="defaultRowGap rowColor bdBottom bdRight"><span id="additionalInfo" class="fntDefault" ></span></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr id="trInfGrid">
															<td class='noPadding'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>					
																		<td class='gridHDDark' width="7px"></td>																
																		<td class='gridHDDark alignLeft'  width='60%'><label id="lblInfantHD" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='40%'><label id="lbltravelwith" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr id="infantGrid">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'  width="7px"><label id="itnSeqNo"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'><label id="itnPaxName"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'><label id="itnTravellingWith"></label></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<br/>
													<table width="100%" border="0" cellspacing="0" cellpadding="3" class="GridTable" style="display: none;" id="tblFlexiDetails">
														<tr>
															<td class="gridHD">
																<label id="lblFlexiHeader" class='gridHDFont fntBold'></label>
															</td>
														</tr>
														<tr id="trFlexiGrid">
															<td class='noPadding'>
																<table border='0' cellspacing='0' cellpadding='3' width='100%'>
																	<tr>																	
																		<td class='gridHDDark alignLeft'  width='30%'><label id="lblOriginDestination" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark alignLeft'  width='70%'><label id="lblFlexiInfo" class='gridHDFont fntBold'></label></td>
																	</tr>																
																	<tr id="flexiInfo">
																		<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="originDestination"></label></td>
																		<td class='defaultRowGap rowColor bdBottom bdRight'><label id="flexiDetails" class="fntDefault"></label></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<br/>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='noPadding'>
																<table border='0' cellspacing='1' cellpadding='1' width='100%' id="payDetail" class="GridTable trconFPanel">	
																	<tr>
																		<td class='gridHD' colspan="2" class="alignLeft"><label name="lblPaymentInfo" id="lblPaymentInfo" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr id="paymentDetails">
																		<td class='defaultRowGap alignLeft rowColor paddingL5'><label id="labelName" ></label></td>
																		<td align='right' class='rowColor'><label id="value"></label></td>
																	</tr>	
																	<%--														
																	<tr name="trAirFare">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft' ><label id="lblTotAirFare">T</label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valAirFare' ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr>																	
																	<tr name="trCharges">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft'  ><label id="lblTotTaxSurcharge"></label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valCharges' ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr>
																	<tr name="trFlexiCharges">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft'  ><label id="lblFlexiCharges"></label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valFlexiCharges' ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr>
																	<tr name="trSeatCharges">
																		<td class=' bdLeft bdBottom bdRight alignLeft' ><label id='lblSeatSelection'></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name="valSeatCharges" ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr> 
																	<tr name="trInsuranceCharges">
																		<td class=' bdLeft bdBottom bdRight alignLeft'><label id='lblTravelSecure' ></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name="valInsuranceCharges" ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr> 																				
																	<tr name="trMealCharges">
																		<td class=' bdLeft bdBottom bdRight alignLeft'><label id='lblMealSelection'></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name="valMealCharges" ></label>&nbsp;<label name='valBaseCurrCode' ></label></td>
																	</tr> 																	 
																	<tr name="trTxnFee">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft' ><label id="lblTNXFee"> </label></td>
																		<td align='right' class=' bdRight bdBottom'><label name='valTxnFee' ></label>&nbsp;<label name='valBaseCurrCode'></label></td>
																	</tr>   																	 
																	<tr>
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft' ><label id="lblTotal" class='fntBold'></label></td>
																		<td class=' bdRight bdBottom' align='right'><label name='valTotal' class='fntBold' ></label>&nbsp;<label name='valBaseCurrCode'  class='fntBold'></label></td>
																	</tr> 
																	<tr id="paymentSelectedCurrCode" style="display: none;">
																		<td class='defaultRowGap  bdLeft bdBottom bdRight alignLeft'><label id="lblTotal" class='fntBold'></label></td>
																		<td align='right' class=' bdBottom bdRight'><label name='valSelectedTotal' class='fntBold'></label>&nbsp;<label name='valSelectedCurrCode' class='fntBold'></label></td>
																	</tr>  --%>     			
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr id="trContactDetail" style="display: none">
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>											
											<tr>												
												<td colspan='2' class='pnlBottom' valign='top' class="alignLeft">
													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="trconFPanel">														
														<tr>															
															<td style="border:1px solid #DFDFDF;">
																<table border='0' cellpadding='3' cellspacing='0' width='100%' class="">
																	<tr>
																		<td class='gridHD alignLeft' colspan="4"><label id="lblCotactInfo" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr>
																		<td width="25%" class='defaultRowGap rowColor alignLeft'><label id="lblTitle"></label></td> 
																		<td width="25%" class='rowColor alignLeft'>:<label id="paxTitle" class="fntBold paddingL5"></label></td>        				
																		<td class='rowColor'></td>      				
																		<td class='rowColor'></td>    
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate alignLeft' ><label id="lblFirstname"></label></td>
																		<td class='rowColorAlternate alignLeft'>:<label id="firstname" class="fontCapitalize fntBold paddingL5"></label></td>
																		<td class='rowColorAlternate alignLeft' width="25%" ><label id="lblLasttname"></label></td>
																		<td class='rowColorAlternate alignLeft' >:<label id="lasttname" class="fontCapitalize fntBold paddingL5"></label></td>
																	</tr>
																	<tr>																		
																		<td class='defaultRowGap rowColor alignLeft'><label id="lblNationality"></label></td>
																		<td class='rowColor alignLeft'>:<label id="nationality" class="fntBold paddingL5"></label></td>
																		<td class='rowColor alignLeft'><label id="lblcity"></label></td>
																		<td class='rowColor alignLeft'>:<label id="city" class="fontCapitalize fntBold paddingL5"></label></td>
																	</tr>																	
																	<tr>
																		<td class='defaultRowGap rowColorAlternate alignLeft'><label id="lblCountry"></label></td>
																		<td class='rowColorAlternate alignLeft'>:<label id="Country" class="fntBold paddingL5"></label></td>
																		<td class='rowColorAlternate alignLeft'><label id="lblMobile"></label></td>
																		<td class='rowColorAlternate alignLeft' >:<label id="mobile" class="fntBold paddingL5"></label></td>
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColor alignLeft'><label id="lblPhone"></label></td>
																		<td class='rowColor alignLeft'>:<label id="phone" class="fntBold paddingL5"></label></td>
																		<td class='rowColor alignLeft'>&nbsp;</td>
																		<td class='rowColor alignLeft'>&nbsp;</td>																		
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate alignLeft' class="alignLeft"><label id="lblEmailAddress"></label></td>
																		<td class='rowColorAlternate alignLeft'>:<label id="emailAddress" class="fntBold paddingL5"></label></td>
																		<td class='rowColorAlternate'></td>
																		<td class='rowColorAlternate'></td>
																	</tr>																	
																</table>
															</td>															
														</tr>
													</table>
												</td>												
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											<%-- This should be enable only airmed, Temp disable for tag build --%>
											<tr id="trDispEmgnContact" style="display: none;">												
												<td colspan='2' class='pnlBottom' valign='top' class="alignLeft">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">														
														<tr>															
															<td style="border:1px solid #DFDFDF;">
																<table border='0' cellpadding='3' cellspacing='0' width='100%' class="">
																	<tr>
																		<td class='gridHD alignLeft' colspan="4"><label id="lblEmgnCntInformation" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr>
																		<td width="25%" class='defaultRowGap rowColor alignLeft'><label id="lblTitle"></label></td> 
																		<td width="25%" class='rowColor alignLeft'>:<label id="emgnPaxTitle" class="fntBold paddingL5"></label></td>        				
																		<td class='rowColor'></td>      				
																		<td class='rowColor'></td>    
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate alignLeft' ><label id="lblFirstname"></label></td>
																		<td class='rowColorAlternate alignLeft'>:<label id="emgnFirstName" class="fontCapitalize fntBold paddingL5"></label></td>
																		<td class='rowColorAlternate alignLeft' width="25%" ><label id="lblLasttname"></label></td>
																		<td class='rowColorAlternate alignLeft' >:<label id="emgnLastName" class="fontCapitalize fntBold paddingL5"></label></td>
																	</tr>
																	<tr>
																		<td class='defaultRowGap rowColor alignLeft'><label id="lblPhone"></label></td>
																		<td class='rowColor alignLeft'>:<label id="emgnPhoneNo" class="fntBold paddingL5"></label></td>
																		<td class='rowColor alignLeft'><label id="lblEmailAddress"></label></td>
																		<td class='rowColor alignLeft'>:<label id="emgnEmail" class="fntBold paddingL5"></label></td>																		
																	</tr>																	
																</table>
															</td>															
														</tr>
													</table>
												</td>												
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								
								<tr id="rowBookHotelOrCarSeperator" style="display:none;">
									<td class='pnlTop pnlWidth'></td>
									<td class='pnlTopR'></td>
								</tr>
								<tr id="rowBookHotelOrCar" style="display:none;"><td class='pnlBottom' valign='top' align="center" colspan='2'>
									<table width='98%' border='0' cellpadding='0' cellspacing='0' align='center' class="trconFPanel">
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td align='center'>
												<a id="linkBookHotel" target="_blank" href="">
													<img src='../images/Hotel_booking_button_no_cache.gif' alt='Hotel Booking'/>
												</a>
												<a id="linkRentCar" target="_blank" href="">
													<img src='../images/Car_booking_button_no_cache.gif' alt='Cate Booking'/>
												</a>
											</td>
										</tr>
										<tr>
											<td class="alignLeft">
												<label id="lblMsgBookAHotel" class='fntBold hdFontColor'></label>
											</td>
										</tr>
										<tr>
										   <td>
												<div id="divHotel">																			
															<iframe name="frmhotel" id="frmhotel" src="showBlank" frameborder="0" width="675" height="250" style="width:675px;height:250px" >
															</iframe>		
												</div>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class="alignLeft">
												<label id="lblMsgBookACar" class='fntBold hdFontColor'></label>
											</td>
										</tr>
										<tr>
										   <td>
												<div id="divRentCar">																			
															<iframe name="frmrentcar" id="frmrentcar" src="showBlank" frameborder="0" width="675" height="300" style="width:675px;height:300px">
															</iframe>		
												</div>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
									</table>
								</td></tr>
								<tr class="inlineSocialPanel" style="display: none;" >
									<td colspan='2'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
											<%@ include file="../common/includeFrameTop.html"%>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td>
																<div class="div-Table alignLeft">
																		<div class="table-Row one-Col">
																				<label id="lblShareInSocialMedia" class='hdFontColor fntBold'>Share on Social Media</label>
																		</div>
																		<div class="table-Row one-Col">
																			<div class="refFacebook">Facebook LOGO</div>
																			<div class="refTwitter">|Twitter LOGO</div>
																			<div class="refGoogle">|Google Plus LOGO</div>
																			<div class="refLinkedIn">LinkedIn LOGO</div>
																	   </div>
															   </div>
															</td>
														</tr>
												</table>
											<%@ include file="../common/includeFrameBottom.html"%>
										</table>
									</td>
							    </tr>
								<tr class="inlineSocialPanel" style="display: none;" >
									<td class='rowGap'></td>
							   </tr>
								<tr id="trButtonPannel" style="display: none;">
									<td align='left' colspan='2'>
									<div class="buttonset">
										<!-- Button -->
										<table border='0' cellpadding='2' cellspacing='0' width="100%">
											<tr>
												<td class="alignLeft"><input type="button" id="btnFinish"  value="Finish" class="Button ui-state-default ui-corner-all"/></td>
												<td align="center" id="tdEmail"><input type="button"  id="btnEmailMe"  class="Button ButtonLarge ui-state-default ui-corner-all" value="Email me" title="Click here to Email me the itinerary"/></td>					
												<td class="alignRight"><input type="button" id="btnPrint"  value="Print" class="Button ui-state-default ui-corner-all" /></td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
									<tr>
										<td class='rowGap' colspan='2'></td>
									</tr>
									<tr>
										<td class='rowGap' colspan='2'></td>
									</tr>
							</table>
						</div>
					</div>
					</div>
					</td>
					</tr>
					<!-- Bottom AA Logo -->
					<tr>
						<td class='appLogo' colspan="2"></td>
					</tr>
					<c:import url="../../../../ext_html/cfooter.jsp" />
				</table>
			</td>
		</tr>
	</table>
	<input type="hidden" id="hdnMode" name="hdnMode"/>
	<%@ include file='../common/iBECommonParam.jsp'%>		
 </form> 
 </div>
 
<div id='divLoadMsg' class="mainPageLoader">
	<%@ include file='../common/includeLoadingMsg.jsp' %>
</div>
<form id="submitForm" method="post">
	<input type="hidden" name="pnr" value="<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />" />
</form>
<script src="../js/v2/reservation/interLineConfirmation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<!-- <div id='fb-root' style="display: none"></div> -->
<div id="newPopItem" style="display: none;" >
	<%@ include file='../../common/includePopup.jsp' %>
</div>
<div style="display:none">
	<c:import url="../../../../ext_html/inc_ConfPromo.jsp" />
</div>

<%-- Use to load third party tools and tracking codes--%>
<div id="accelAeroIBETrack" style="display: none">
	<iframe name="frmTracking" id="frmTracking" src="showBlank" frameborder="0">
	</iframe>	
</div>

</body>
<%@ include file='../common/inline_Tracking.jsp' %>
</html>