<table width='100%' border='0' cellpadding='0' cellspacing='0'>
<tr>
	<td colspan="2" class="paymentGadient">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td class="rowSingleGap">
				</td>
				</tr>
				<tr>
				<td class="pageName alignLeft" colspan="2" style="padding-left: 5px;">
					<label id="lblPayment"></label>
				</td>
				</tr>
				<tr>
			<td colspan='2' class='rowGap'></td>
		</tr>
		<tr>
		<td colspan='2'>
			<div id="specialMsg">
				<div class="spMsg alignLeft">
					<span ></span>
				</div>
			</div>
			<div id="trpayPageAirportMsg" style="margin: 6px 0 0">
				<div class="spMsg alignLeft">
					<span id="payPageAirportMsg"></span>
				</div>
			</div>
			<div id="spnPayType" style="display: none;margin: 6px 0 0">
				<div class="alignLeft" style="padding-left: 50px;">
					<table cellspacing="0" cellpadding="2" border="0" width="60%">
						<tr>
							<td width="40%" class="alignLeft">
								<label id="lblPaymentMode"></label>
							</td>
						</tr>
						<tr><td colspan="4" class="rowSingleGap">
						
						</td></tr>
						<tr> 
							<td id="tdPayLater">	
								<input type="radio" checked="checked"  value="ONHOLD" class="noBorder" name="selPayType" id="selPayType"/>
								<label id="lblPayAtCashier">  </label>&nbsp;
							</td> 
							<td width="35%">	
								<input type="radio"  value="NORMAL" class="noBorder" name="selPayType" id="selPayType1">
								<label id="lblPayByCreditCard"> </label> 
							</td>
						</tr>
						<tr>
							<td colspan="2" class="rowSingleGap">				
							</td>
						</tr>
						<tr id="trOnholdDuration">
							<td colspan="2" class="alignLeft rowSingleGap">
								<div style="z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);">
									<label class="fntBold" id="lblOnHoldState"> On-Hold Duration :</label> <label class="fntBold" id="onHoldDisplayTime"></label>	
								</div>	
							</td>
						</tr>
					</table>
				</div>
			</div>
		</td>
		</tr>	
		
	<tr>
		<td class='rowGap'></td>
	</tr>
	<tr id="payByCreditCardPanel"  style="display: none">
	  <td>
		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td>
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='tblLoyalty' class="alignLeft" style="display: none;">
					<tr>
						<td>
							<label id="lblLoyaltyCredit_1"></label>
							 <strong><span id="spnloyaltyCredit"></span></strong> <label id="lblLoyaltyCredit_2"></label>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr id="trLoyalPayFull">
									<td  width="25">
										<input type="radio" name="loyaltyOption" value="total" />
									</td>
									<td>
										<label id="lblLoyaltyUseAll"></label>
									</td>
								</tr>
								<tr>
									<td>
										<input type="radio" name="loyaltyOption" value="part" />
									</td>
									<td>
										<label id="lblLoyaltyUseAmt_1"></label>
											<input type="text" name="loyaltySetteledAmount" id="loyaltySetteledAmount" size="9" maxlength="9" />
										<label id="lblLoyaltyUseAmt_2"></label>
									</td>
								</tr>
								<tr>
									<td>
										<input type="radio" name="loyaltyOption" value="none" />
									</td>
									<td>
										<label id="lblLoyaltyNo"></label>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
				</table>
			</td>
		</tr>
	
		<!-- Credit place holder -->
		<tr>
			<td class="pnlBottom" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 5px;">
					<tr>
						<td class="alignLeft">
							<label id="lblMsgReviewPayment"></label>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan='2' class='rowGap'></td>
		</tr>
		<tr>
			<td class='' colspan='2'>
				<table cellspacing="0" cellpadding="0" border="0" width="100%" >
					<tr>
						<td align="left"><input type="button" name="btnRecalculate" id="btnRecalculate"  tabindex="23"  value="Recalculate" class="Button ButtonMedium"/></td>
					</tr>
				</table>
				<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable" id="modifyBal" style="display: none">										
						<tr>
							<td colspan="3" class="alignLeft">
							<label id="lblBalancePayment" class="fntBold fntRed"></label></td>
						</tr>
						<tr>
							<td
								class="gridHD"><font>&nbsp;</font></td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblModifingFlight" class="fntBold fntWhite">Modifying Flight(s)&nbsp;</label>
							</td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblNewFlight" class="fntBold fntWhite">New Flight(s)&nbsp;</label></td>
						</tr>						
						<tr id="balanceSummaryTemplate">
							<td
								class="SeperateorBGAlt rowDataHeight GridItems alignLeft">
								<label id="displayDesc"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayOldCharges" class="fntRed"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayNewCharges" class="fntRed"></label>
							</td>
						</tr>
				</table>
				<%--						
				<table width="100%" border="0" cellspacing="0" cellpadding="3" id="tblPriceBreakdown">
					<tr>
						<th colspan="2" class="Gridwt bdTop bdRight bdLeft alignLeft"><label id="lblPriceBreakdown" class='fntBold hdFontColor'></label></th>            
					</tr>
					<tr>
						<th width='50%' class='gridHD bdLeft bdBottom bdTop' align='center'><label id="lblDescription" class='gridHDFont fntBold'></label></th>
						<th width='50%' class='gridHD  bdBottom bdRight bdTop' align='center'><label id="lblAmount" class='gridHDFont fntBold'></label></th>
					</tr>
					<tr id='trAirFareBD'>
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblTotAirFare"></label></td>
						<td align='right' class='rowColor  bdBottom bdRight'><label id="lblTotAirFareAmount" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr>
					<tr id='trChargesBD'> 
						<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id="lblTotTaxCharge"></label></td>
						<td align='right' class='rowColorAlternate  bdBottom bdRight'><label id="lblTotTaxChargeAmount" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 
					<tr id='trFlexiChargesBD'> 
						<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id="lblFlexiCharges"> </label></td>
						<td align='right' class='rowColorAlternate  bdBottom bdRight'><label id="lblFlexiChargeAmount" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr>
					<tr id="trSeatChargesBD">
						<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft' ><label id='lblSeatSelection'> </label></td>
						<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblSeatCharges" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 
					<tr id="trInsuranceChargesBD">
						<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id='lblTravelSecure' ></label></td>
						<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblInsuranceCharges" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 																				
					<tr id="trMealChargesBD">
						<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id='lblMealSelection'> </label></td>
						<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblMealCharges" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 
					<tr id="trHalaChargesBD">
						<td class='rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id='lblHalService'> </label></td>
						<td align='right' class='rowColorAlternate bdBottom bdRight' ><label id="lblHalaCharges" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 
					 <tr id="trModCanlCharge">
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblModCanlCharge"> </label></td>
						<td align='right' class='rowColor  bdBottom bdRight'> <label id="lblModCanlChargeAmount" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 
					<tr id="trTNXFee">
						<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight alignLeft'><label id="lblTNXFee"></label></td>
						<td align='right' class='rowColor  bdBottom bdRight'> <label id="lblTNXFeeAmount" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr>   
					<tr id='trUtilizedCredit' style="display:none;">
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblUtilzedCredit"></label></td>
						<td align='right' class='rowColor  bdBottom bdRight'><label id="valUtilizedCredit" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 
					<tr id='trReservationCredit' style="display:none;">
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblReservationCredit"></label></td>
						<td align='right' class='rowColor  bdBottom bdRight'><label id="valReservationCredit" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 
					<tr id='trBalanceToPay' style="display:none;">
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblBalanceToPay"></label></td>
						<td align='right' class='rowColor  bdBottom bdRight'><label id="valBalanceToPay" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 					
					<tr id='trTotalPaid' style="display:none;">
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight alignLeft'><label id="lblTotalPaid"></label></td>
						<td align='right' class='rowColor  bdBottom bdRight'><label id="lblTotalPaidAmount" class='hdFontColor'></label> <label name='valBaseCurrCode' class='hdFontColor'></label></td>
					</tr> 																			
					<tr>
						<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '><label id="lblTotalDue" class='fntBold'></label></td>
						<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblTotalDueAmount" class='fntBold hdFontColor'></label>  <label name='valBaseCurrCode' class='fntBold hdFontColor'></label></td>
					</tr> 
					<tr id="paymentSelectedCurrCode" style="display: none;">
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="lblTotalDue" class='fntBold'></label></td>
						<td align='right' class='rowColor bdBottom bdRight'><label id="lblTotalDueAmountSelected" class='fntBold hdFontColor'></label> <label name='valSelectedCurrCode' id="lblSelectedCurrCode" class='fntBold hdFontColor'></label></td>
					</tr>           
				</table> --%>
				<table width="100%" border="0" cellspacing="0" cellpadding="3" id="tblPriceBreakdown">
					<tr>
						<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '><label id="lblTotalDue" class='fntBold'></label></td>
						<td align='right' class='rowColorAlternate bdBottom bdRight'><label id="lblTotalDueAmount" name="lblTotalDueAmount" class='fntBold hdFontColor'></label>  <label name='valBaseCurrCode' class='fntBold hdFontColor'></label></td>
					</tr> 
					<tr id="paymentSelectedCurrCode" style="display: none;">
						<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="lblTotalDue" class='fntBold'></label></td>
						<td align='right' class='rowColor bdBottom bdRight'><label id="lblTotalDueAmountSelected" class='fntBold hdFontColor'></label> <label id="lblSelectedCurrCode" class='fntBold hdFontColor'></label></td>
					</tr>   
				</table>
			</td>
		</tr> 
		<tr>
			<td class='rowGap' colspan='2'></td>
		</tr>
		<tr>
			<td id="flexiInfo" style="display: none;" align="left" colspan="2"><font class="fntEnglish">Remaining Flexibilities: <span id="spnRemainingFlexi"><font class="fntRed">No more flexibilities available for the cancelled segment</font></span></font>
			</td>
		</tr>
		<tr>
			<td class='rowGap' colspan='2'></td>
		</tr>																	
		<tr>
			<td colspan="2" class="alignLeft paddingL5" width="65%" style="display: none">
				<table  border="0" cellspacing="0" cellpadding="0">
					<tr>
						 <td class="alignLeft paddingL5" width="175px"><label id="lblPG"></label></td>
						 <td class="alignLeft">
							<select id='selPG' size='1' style='width:200px;' name='strPaymentGateway'></select>
						 </td>
					</tr>
					<%-- <tr>
						 <td>
							<select id='selPG' size='1' style='width:200px;' name='strPaymentGateway'></select>
						 </td>
					</tr> --%>
				</table>
			</td>
		</tr>
		<tr>
			<td class='rowGap'></td>
		</tr>		
		<tr>
			<td colspan="2" class="alignLeft">
				<%@include file="../../v2/common/cardDetail.jsp" %>
			</td>
		</tr>	
		<tr>
			<td class='rowGap'>				
			</td>
		</tr>
		<tr id="IPNotification">
			<td class="alignLeft paddingL5" colspan="2">
				<label id="lblNofication" class='fntBold'> </label><label class="paddingL3 fntBold">:</label><label class="paddingL3" id="lblMsgRecordIP"></label><br>
				<label id="lblYourIPIs" class='fntBold'> </label><label class="paddingL3 fntBold">:</label><label class="paddingL3" id='lblYourIP'></label>
			</td>
		</tr>
			<tr>
			<td class='rowGap'>				
			</td>
		</tr>		
		<tr style="display: none;">
			<td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">														
					<tr>
						<td colspan='2'  valign='top'>													
							<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
								<tr>
									<td colspan="7" class="Gridwt alignLeft"><label id="lblDeparting"  class='fntBold hdFontColor'></label></td>
								</tr>	
								<tr>
									<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
								</tr> 
								<tr>            
									<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
								</tr>
								<tr id="departueFlight">
									<td width='35%' class='defaultRowGapNew GridItems alignLeft'><label id="segmentCode"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="departureDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="departureTime"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalTime"></label></td>
									<td width='10%' class='defaultRowGapNew GridItems' align='center'><label id="flightNumber"></label></td>
									<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>															
								</tr>               
							</table>																																
						</td>
					</tr>
					<tr>
						<td colspan='7' class='rowGap'></td>
					</tr>
				</table>
			</td>
		</tr>											
		<tr style="display: none">
			<td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
					<tr id="trRetrunGrid">
						<td colspan='2'  valign='top'>
							<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">
								<tr>
									<td colspan="7" class="Gridwt alignLeft"> <label id="lblReturning"  class='fntBold hdFontColor'></label></td>
								</tr>
								<tr>
									<td rowspan="2" align='center' class='gridHD'><label id="lblRSegment" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align='center' class='gridHD'><label id="lblRDeparture" class='gridHDFont fntBold'></label></td>
									<td colspan="2" align='center' class='gridHD'><label id="lblRArrival" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblRFlightNo" class='gridHDFont fntBold'></label></td>
									<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
								</tr>
								<tr>           
									<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
									<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>
								</tr> 
								<tr id="returnFlight">
									<td width='35%' class='defaultRowGapNew GridItems alignLeft'><label id="segmentCode"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="departureDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="departureTime"></label></td>
									<td width='15%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalDate"></label></td>
									<td width='8%' class='defaultRowGapNew GridItems' align='center'><label id="arrivalTime"></label></td>
									<td width='10%' class='defaultRowGapNew GridItems' align='center'><label id="flightNumber"></label></td>
									<td width='9%' class='defaultRowGapNew GridItems' align='center'><label id="duration"></label></td>								
								</tr>                
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr> 
	  </table>
	 </td>
	</tr>	
	<tr id="intExtProcessing" style="display: none">
		<td>
			<div style="text-align: center;" class="fntBold, fntRed">
				<img  src="../images/tr-processing_en_no_cache.gif">
			</div>
		</td>
	</tr>																
	<tr>
		<td class='rowGap' id="paymentPannelPosition">
			<%-- <div id="cardInputPannelInternalExternal">
				<iframe name="cardInputPannelInternalExternal" id="cardInputPannelInternalExternal" class="paymentCardInputs"  style="" frameborder="0" marginwidth="0" marginheight="0" height="0%" width="0%" src="showBlank">
				</iframe>
			</div> --%>
		</td>
	</tr>
	
	
		
	
	<tr>
		<td height="30px" colspan="2">
		</td>
	</tr>
	<tr>
		<td align='left' colspan="2">
		<div class="buttonset" id="paymentButtonset">
			<!-- Button -->
			<table border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td class="alignLeft">
						<button type="button" name="btnPrevious" id="btnPaymentPrevious" tabindex="23"  value="Previous" class="Button ui-state-default ui-corner-all">Previous</button>
						<button type="button" name="butEditAnci" id="butEditAnci" tabindex="23"  value="Previous" class="Button ui-state-default ui-corner-all" style="display: none">Edit</button>
					</td>
					<td style="width:10px;">&nbsp;</td>	
					<td>
						<button type="button" name="btnSOver"  tabindex="23"  value="Start Over" class="Button ui-state-default ui-corner-all">Start Over</button>
						<!--<input type="button" name="btnRecalculate" id="btnRecalculate"  tabindex="23"  value="Recalculate" class="Button ButtonMedium"/>
					--></td>	
					<td style="width:10px;">&nbsp;</td>
					<td class="alignRight">
						<button type="button" name="btnNext" id="btnPurchase"  value="Purchase" class="Button ui-state-default ui-corner-all" >Purchase</button>
						<button type="button" name="btnPurchaseOnAnci" id="btnPurchaseOnAnci"  value="Purchase" 
						class="Button" style="display: none;">Purchase</button>
					</td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
</table></td></tr></table>
<input type="hidden" id="payCurrency" name="pgw.payCurrency"/>
<input type="hidden" id="providerName" name="pgw.providerName"/>
<input type="hidden" id="providerCode" name="pgw.providerCode"/>
<input type="hidden" id="description" name="pgw.description"/>
<input type="hidden" id="paymentGateway" name="pgw.paymentGateway"/>
<input type="hidden" id="payCurrencyAmount" name="pgw.payCurrencyAmount"/>
<input type="hidden" id="brokerType" name="pgw.brokerType"/>
<input type="hidden" id="switchToExternalURL" name="pgw.switchToExternalURL"/>
<input type="hidden" id="onholdReleaseTime" name="pgw.onholdReleaseTime"/>
<input type="hidden" id="hdnMode" name="hdnMode"/>
<input type="hidden" id="jsonInsurance" name="jsonInsurance"/>
<input type="hidden" id="jsonPaxWiseAnci" name="jsonPaxWiseAnci"/>
<input type="hidden" id="jsonSubStations" name="jsonSubStations"/>
<input type="hidden" id="jsonPaxInfo" name="jsonPaxInfo"/>
