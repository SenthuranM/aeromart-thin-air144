<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<link rel='shortcut icon' href='../images/AA.ico'/>
<title></title>
	<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/>
	<script src="../js/v2/jquery/jquery.js" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
	<script src="../js/common/Common.js" type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.templete.js" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.form.js" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.json.js" type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.common.js" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js" type="text/javascript"></script>
	<script src="../js/v2/common/JQuery.ibe.i18n.js" type="text/javascript"></script>
	<script src="../js/v2/common/JQuery.flash.js" type="text/javascript"></script>
</head>

<body id="direction">
<div id="divAnciPage">
<form action="" id="frmAnci" method="post">
	<input type="hidden" name="hdnMode" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id='PgInterlineAncillary'>
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<!-- Top Banner -->
					<tr>
						<td colspan='2'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td colspan='3' class='topBanner' align='center'>
										<table width='97%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td width='5%' style='height:70px;'><a href='#' name="lnkHome"  title=""><img src='../images/LIbe005_no_cache.gif' alt='logo'/></a></td>
												<td class="alignRight">
													<script src="../js/common/activeContentLoader.js" type="text/javascript"></script>
													<div id="flashObj">									   
													    	<noscript>
														    	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="491" height="72">
															      <param name="movie" value="http://www.airarabia.com/userfiles/header_banner/en/sharjah.swf">
															      <param name="quality" value="high">
															      <param name="menu" value="false">
															      <param name="wmode" value="transparent">
															      <embed src="http://www.airarabia.com/userfiles/header_banner/en/sharjah.swf" width="491" height="72" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" menu="false" wmode="transparent"></embed>
															    </object>
													    </noscript>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class='topBannerLB'></td>
									<td class='topBannerMB'><label>&nbsp;</label></td>
									<td class='topBannerRB'></td>
								</tr>
							</table>
						</td>
					</tr>					
					<!-- Home Link row -->
					<tr>
						<td colspan='2' align='center'>
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td align='right'>
										<a href='#' name="lnkHome" title="Click here to go to the Home page"><img border='0' class="linkImage" src="../images/home_no_cache.gif" /></a>
									</td>
								</tr>
								<tr>
									<td class='rowSingleGap'></td>
								</tr>			
							</table>
						</td>
					</tr>
					
					<!-- Content holder -->
					<tr>
						<!-- Left Column -->
						<td width='260px' valign='top' class='alignLeft'>  
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<!-- Booking Summary -->
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneTopL'></td>
												<td class='lpaneTopM'><label class='fntBold gridHDFont' id='lblBookingSummary'>Booking Summary</label></td>
												<td class='lpaneTopR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneMidL'></td>
												<td class='lpaneMidM'>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgPassenger">
														<tr>
															<td class='pnlTop'></td>
														</tr>
														<tr>
															<td class='pnlBottom'>
																<table width="100%" border="0" cellspacing="1" cellpadding="1">
																	<tr>
																		<td><label class='fntBold hdFontColor' id='lblFlightDetails'>Flight Details</label></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td><label id="lblReturn"/></td>           
																	</tr>
																	<tr>
																		<td><label id="adultCount"/></td>            
																	</tr>
																	<tr>            
																		<td><label id="childCount"/></td>
																	</tr>
																	<tr>
																		<td><label id="infantCount"/></td>
																	</tr>         
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate">
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																		<td class='pnlTopR'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																				<tr>
																					<td colspan="3"><label name="segmentName"  class='fntBold hdFontColor'/></td>            
																				</tr>
																				<tr>
																					<td width="50%"><label id='lblFlightNumber'>Flight No</label></td>
																					<td width="1%"><label>:</label></td>
																					<td width="49%"><label name="flightNumber"/></td>
																				</tr>
																				<tr>
																					<td><label id='lblDepartureDate'>Date</label> </td>
																					<td><label>:</label></td>
																					<td><label name="depatureDate"/></td>
																				</tr>
																				<tr>
																					<td><label id='lblDeparture'>Departure</label></td>
																					<td><label>:</label></td>
																					<td><label name="depatureTime"/></td>
																				</tr>
																				<tr>
																					<td><label id='lblArrivalTime'>Arrival</label></td>
																					<td><label>:</label></td>
																					<td><label name="arrivalTime"/></td>
																				</tr>
																				<tr>
																				<td colspan="3" ><span name="anciDetails" ></span></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>																
															</td>
														</tr>
													</table>
												</td>
												<td class='lpaneMidR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneBotL'></td>
												<td class='lpaneBotM'></td>
												<td class='lpaneBotR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class='rowGap'></td>
								</tr>
								
								<!-- Payment Summary -->
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneTopL'></td>
												<td class='lpaneTopM'><label class='fntBold gridHDFont' id='lblPaymentSummary'>Payment Summary</label></td>
												<td class='lpaneTopR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneMidL'></td>
												<td class='lpaneMidM'>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop'></td>
														</tr>
														<tr>
															<td class='pnlBottom'>
																<table width="100%" border="0" cellspacing="1" cellpadding="1">
																	<tr>
																		<td><label id="lblRetFlight" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td><label id="lblAdults" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td><label id="lblChildren" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td><label id="lblInfants" class='fntBold'/></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate">
																	<tr>
																		<td class='pnlTop pnlWidthLeft'></td>
																		<td class='pnlTopR'></td>
																	</tr>
																	<tr>
																		<td class='pnlBottom' colspan='2'>
																			<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																				<tr>
																					<td width="50%"><label id="lblAirfare">Air Fare</label></td>
																					<td width="2%" class="hdFontColor fntBold">:</td>
																					<td width='10%' align='left'><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																					<td width="38%" align='right'><div id='divAirFare' class='fntDefault txtBold'></div></td>
																				</tr>
																				<tr id="trChargesSummary">
																					<td><label id="lblCharges">Tax & surcharges</label></td>
																					<td class="hdFontColor fntBold">:</td>
																					<td align='left'><div name='anciCurrCode' class='fntDefault txtBold'></div></td>
																					<td align='right'><div id='divCharges' class='fntDefault txtBold'></div></td>
																				</tr>																				
																				<tr>
																					<td><label id="lblTotal" class='fntBold'>Total</label></td>
																					<td class="hdFontColor fntBold">:</td>
																					<td align='left'><div name='anciCurrCode' class='fntDefault fntBold '></div></td>
																					<td align='right'><div id='divTotal' class='fntDefault fntBold hdFontColor'></div></td>
																				</tr>
																				<tr id='trSelCurTotal'>
																					<td><label id="lblSelCurTotal" class='fntBold'></label></td>
																					<td class="hdFontColor fntBold">:</td>
																					<td align='left'><div id='divSelCur' class='fntDefault fntBold'></div></td>
																					<td align='right'><div id='divSelCurTotal' class='fntDefault fntBold hdFontColor'></div></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td class='lpaneMidR'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class='lpaneBotL'></td>
												<td class='lpaneBotM'></td>
												<td class='lpaneBotR'></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						
						<!-- Right Column -->
						<td id="rightPanel" valign='top' align='right' rowspan='2'>
							<table width='98%' border='0' cellpadding='0' cellspacing='0' align='right'>
								<tr>
									<td align='left'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='pnlTop pnlWidth'></td>
												<td class='pnlTopR'></td>
											</tr>
											<tr>
												<td colspan='2' class='pnlBottom' valign='top' align='center'>
													<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<tr>
															<td>
																<table border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td class='tab1A' align='center'>
																			<table border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td><label class='fntBold navFontBig'>1</label></td>
																					<td style='width:10px;'></td>
																					<td><label class='fntBold navFont' id='lblsearchNChoose'>Search &amp; Choose</label></td>
																				</tr>
																			</table>
																		</td>
																		<td class='tab2' align='center'>
																			<table border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td><label class='fntBold navFontBig'>2</label></td>
																					<td style='width:10px;'></td>
																					<td><label class='fntBold navFont' id='lblselectNPay'>Select &amp; Pay</label></td>
																				</tr>
																			</table>
																		</td>
																		<td class='tab3A' align='center'>
																			<table border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td><label class='fntBold navFontBig'>3</label></td>
																					<td style='width:10px;'></td>
																					<td><label class='fntBold navFont' id='lblprintNFly'>Print &amp; Fly</label></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td class='alignLeft'>
																<label class='fntBold hdFont hdFontColor' id='lblPersonalizeTrip'>Personalise Your Trip</label>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td id='tblMainTrSelectAnciText' class='alignLeft'>
																<label id='lblPleaseSelectAnci'>Please Select Air Arabia Add-On Services and enjoy the extra comfort during travel.</label>
															</td>
														</tr>
                                                        <tr id='tblMainTrNoAnciMsg'>
															<td class='alignLeft'>
																<label class='fntBold' id='lblNoAnciAvailable'>No Add-On Services available. You are being forwarded to the next step...</label>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>

												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<!-- Seat -->
											<tr>
												<td>
													<table id='tblSeat' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottom' valign='top' align='center'>
																<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
																	<tr>
																		<td class='alignLeft'><label class='fntBold hdFontColor' id='lblSelectSeat'>Select Your Seat</label></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblSeatSel' border='0' cellpadding='1' cellspacing='1' width='100%'>
																				<tr>
																					<td><label id='lblSeatSelectWhy'>Click on traveler name & choose a seat on the map.</label></td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td>
																						<table border='0' cellpadding='1' cellspacing='1' width='100%'>
																							<tr>
																								<td width='2%'><input type='radio' id='radSeat' name='radSeat' value='Y' class='noBorder' checked="checked"></td>
																								<td width='98%'><label id='lblSeatYes'>Yes, I would like to select my seat.</label></td>
																							</tr>
																							<tr>
																								<td><input type='radio' id='radSeat' name='radSeat' value='N' class='noBorder'></td>
																								<td><label id='lblSeatNo'>No thanks</label></td>
																							</tr>
																							<tr>
																								<td colspan='2' class='rowGap'></td>
																							</tr>
																							<tr>
																								<td colspan='2' align='center'>
																									<input type="button" id="btnSeatAddNow"  value="Add to my trip Now" class="ButtonAncilary"/>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblSeatDetails' border='0' cellpadding='1' cellspacing='1' width='100%' style='display:none;'>
																				<tr>
																					<td><label id='lblSeatMapHowto'>Click on traveler name & choose a seat on the map.</label></td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td class='tabBG noPadding'>
																						<table border='0' cellpadding='1' cellspacing='0' >
																							<tbody id='tbdySeatSeg' name='tbdySeatSeg'></tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td>
																					<table border='0' cellpadding='1' cellspacing='0'>
																						<tr>
																							<td class='availSeat'></td>
																							<td><label id='lblAvailableSeat'>Available Seat</label></td>
																							<td class='notAvailSeat'></td>
																							<td><label id='lblOccupiedSeat'>Occupied Seat</label></td>
																							<td class='SltdSeat'></td>
																							<td><label id='lblSelectedSeat'>Selected Seat</label></td>
																						</tr>
																					</table>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td align='center'>
																						<table border='0' cellpadding='0' cellspacing='0' id="tblSeatMap" class="tblSeatMapContainer">
																							<tr>
																								<td class='wingRight' valign='bottom' align='center'>
																									<table border='0' cellpadding='0' cellspacing='0'>
																										<tbody id='tbdyWingRight'></tbody>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<td align='center'>
																									<table border='0' cellpadding='0' cellspacing='0'>
																										<tbody id='tbdySeatMap'></tbody>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<td class='wingLeft' valign='top' align='center'>
																									<table border='0' cellpadding='0' cellspacing='0'>
																										<tbody id='tbdyWingLeft'></tbody>
																									</table>
																								</td>
																							</tr>
																						</table>
																						<span id='spnAnciNoSeat'></span>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td align='left'>
																						<table width='95%' border='0' cellpadding='1' cellspacing='1' align='center'>
																							<tr>
																								<td width='5%' class='gridHD' align='center'><label class='gridHDFont fntBold'>&nbsp;</label></td>
																								<td width='40%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSeatPaxNameHD'>Name</label></td>
																								<td width='20%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSeatNumberHD'>Seat No</label></td>
																								<td width='20%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSeatPriceHD'>Price</label></td>
																								<td width='20%' class='gridHD' align='center'><label class='gridHDFont fntBold'>&nbsp;</label></td>
																							</tr>
																							<tr id="seatPaxTemplate">
																								<td class='defaultRowGap rowColor bdLeft bdBottom bdRight' align='center'><input type='radio' id='radSeatPax' name='radSeatPax' class='noBorder'></td>
																								<td class='rowColor bdRight bdBottom'><label name="paxName"></label></td>
																								<td class='rowColor bdRight bdBottom' align='center'><label name="seatNo"></label></td>
																								<td class='rowColor bdRight bdBottom' align='center'><label name="seatCharge"></label></td>
																								<td class='rowColor bdRight bdBottom' align='center'><span name="clear"></span></td>
																							</tr>        
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr id="seatTermsRow">
																					<td align="left">
																						<table><tr>
																							<td width="2%"><label><input type="checkbox" title="Click here if you agree with terms and conditions v1" name="chkSeatTerms" id="chkSeatTerms"/><label></td>
																							<td width="98%" align="left"><label id="lblSeatAcceptMsg">Accept Seat Terms and Conditions.</label>&nbsp;&nbsp;</td>
																						</tr></table>		   
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
													</table>
												</td>
											</tr>
											
											<!-- Insurance -->
											<tr>
												<td>
													<table id='tblIns' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottom' valign='top' align='center'>
																<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
																	<tr>
																		<td class='alignLeft'><label id='lblInsuranceHD' class='fntBold hdFontColor'>Secure Your Trip with Travel Insurance</label>
																			<span class="ttip" id="insuranceAnciOfferDescription" title="">
																				<label id="insuranceAnciOfferTitle"></label>
																			</span>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'><label id='lblInsuranceDesc'>For your extra comfort during travel we have added Travel Insurance travel insurance to your booking. Underwritten by Chartis it covers you for trip cancellation, baggage loss, flight delay personal accident and much more. <a>Click here</a> to learn more about Travel Insurance</label> </td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'><label id='lblInsuranceChDesc'>Travel Insurance by Air Arabia for this trip costs </label><label><span id="spnInsCost" > </span></label></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table border='0' cellpadding='1' cellspacing='1' width='100%'>
																				<tr>
																					<td width='2%'><input type='checkbox' id='chkAnciIns' name='chkIns' class='noBorder' /></td>
																					<td width='98%'><label id='lblInsuranceYes'>Yes, I would like to purchase travel insurance</label></td>
																				</tr>
																				<tr>
																					<td></td>
																					<td><label  id='lblInsuranceUncheckNotice' class='fntSmall'>Uncheck the box if you do not wish to purchase.</label></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td colspan='2' class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<label id='lblInsuranceTNC'>Click here to view the Air Arabia Travel Insurance <a >Terms &amp; Condition</a> </label>
																		</td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
													</table>
												</td>
											</tr>
											
											<!-- Meal -->
											<tr>
												<td>
													<table id='tblMeal' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottom' valign='top' align='center'>
																<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
																	<tr>
																		<td class='alignLeft'><label id='lblSelectMeal' class='fntBold hdFontColor'>Select a Meal</label></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblMealSel' border='0' cellpadding='1' cellspacing='1' width='100%'>
																				<tr>
																					<td><label id='lblMealWhy'>Enjoy more personalized journey with Air Arabia. Select the meal of your choice offered by our Sky Cafe menu prior your flight.</label></td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td>
																						<table border='0' cellpadding='1' cellspacing='1' width='100%'>
																							<tr>
																								<td width='2%'><input type='radio' id='radMeal' name='radMeal' value='Y' class='noBorder' checked="checked"></td>
																								<td width='98%'><label id='lblMealYes'>Yes, I would like to select a meal.</label></td>
																							</tr>
																							<tr>
																								<td><input type='radio' id='radMeal' name='radMeal' value='N' class='noBorder'></td>
																								<td><label id='lblMealNo'>No thanks</label></td>
																							</tr>
																							<tr>
																								<td colspan='2' class='rowGap'></td>
																							</tr>
																							<tr>
																								<td colspan='2' align='center'>
																									<input type="button" id="btnMealAddNow"  value="Add to my trip Now" class="ButtonAncilary"/>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblMealDetails' border='0' cellpadding='1' cellspacing='1' width='100%' style='display:none'>
																				<tr>
																					<td><label id='lblMealHowTo'>To choose the meal/snack, select passenger name and desired meal/snack.</label></td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td class='tabBG noPadding'>
																						<table border='0' cellpadding='1' cellspacing='0'>
																							<tbody id='tbdyMealSeg' name='tbdyMealSeg'></tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td align='center'>
																						<input type="button" id="btnViewSkyCafe"  value="View Sky Cafe Menu" class="ButtonMeal"/>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td align='left'>
																						<table width='95%' border='0' cellpadding='1' cellspacing='1' align='center'>
																							<thead>
																								<tr>
																									<td width='50%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblMealPaxHD'>Name</label></td>
																									<td width='30%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblMealMenuHD'>Menu</label></td>
																									<td width='20%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblMealPriceHD'>Price</label></td>
																								</tr>
																							</thead>
																							<tr id="paxMealTemplate">
																								<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label name="paxName" ></label></td>
																								<td class='rowColor bdRight bdBottom' align='center'><select name='mealList' size='1' style='width:150px;'></select></td>
																								<td class='rowColor bdRight bdBottom' align='center'><label name="mealCharge"></label></td>
																							</tr>        
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
													</table>
												</td>
											</tr>
											
											
											<!-- SSR -->
											<tr>
												<td>
													<table id='tblSSR' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottom' valign='top' align='center'>
																<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
																	<tr>
																		<td class='alignLeft'><label id='lblSelectSSR' class='fntBold hdFontColor'>Select SSR</label></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblSSRSel' border='0' cellpadding='1' cellspacing='1' width='100%'>
																				<tr>
																					<td><label id='lblSSRWhy'>Enjoy more personalized journey with Air Arabia. Select special services required for you on flight.</label></td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td>
																						<table border='0' cellpadding='1' cellspacing='1' width='100%'>
																							<tr>
																								<td width='2%'><input type='radio' id='radSSR' name='radSSR' value='Y' class='noBorder' checked="checked"></td>
																								<td width='98%'><label id='lblMealYes'>Yes, I would like to select a special services.</label></td>
																							</tr>
																							<tr>
																								<td><input type='radio' id='radSSR' name='radSSR' value='N' class='noBorder'></td>
																								<td><label id='lblSSRNo'>No thanks</label></td>
																							</tr>
																							<tr>
																								<td colspan='2' class='rowGap'></td>
																							</tr>
																							<tr>
																								<td colspan='2' align='center'>
																									<input type="button" id="btnSSRAddNow"  value="Add to my trip Now" class="ButtonAncilary"/>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblSSRDetails' border='0' cellpadding='1' cellspacing='1' width='100%' style='display:none'>
																				<tr>
																					<td><label id='lblSSRHowTo'>To choose SSR, select passenger name and desired ssr and provide any additional comments in the input box.</label></td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td class='tabBG noPadding'>
																						<table border='0' cellpadding='1' cellspacing='0'>
																							<tbody id='tbdySSRSeg' name='tbdySSRSeg'></tbody>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td align='left'>
																						<table width='95%' border='0' cellpadding='1' cellspacing='1' align='center'>
																							<thead>
																								<tr>
																									<td width='35%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSSRPaxHD'>Name</label></td>
																									<td width='35%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSSRSSRHD'>SSR</label></td>
																									<td width='30%' class='gridHD' align='center'><label class='gridHDFont fntBold' id='lblSSRCommentHD'>Comment</label></td>
																								</tr>
																							</thead>
																							<tr id="paxSSRTemplate">
																								<td class='defaultRowGap rowColor bdLeft bdBottom bdRight'><label name="paxName" ></label></td>
																								<td class='rowColor bdRight bdBottom' align='center'><select name='ssrList' size='1' style='width:150px;'></select></td>
																								<td class='rowColor bdRight bdBottom' align='center'><input name="ssrText" maxlength='50'></label></td>
																							</tr>        
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
													</table>
												</td>
											</tr>
											
											<!-- Hala -->
											<tr>
												<td>
													<table id='tblHala' width='100%' border='0' cellpadding='0' cellspacing='0' style="display:none;">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottom' valign='top' align='center'>
																<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
																	<tr>
																		<td class='alignLeft'><label id='lblHalaHD' class='fntBold hdFontColor'>Select a Hala Service</label></td>
																	</tr>
																	<tr>
																		<td class='rowGap'></td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblHalaSel' border='0' cellpadding='1' cellspacing='1' width='100%' style="display:none;">
																				<tr>
																					<td><label id='lblAnHalaDesc'></label></td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																				<tr>
																					<td>
																						<table border='0' cellpadding='1' cellspacing='1' width='100%'>
																							<tr>
																								<td width='2%'><input type='radio' id='radHala' name='radHala' value='Y' class='noBorder' checked="checked" /></td>
																								<td width='98%'><label id='lblHalaYes'>Yes, Hala Service.</label></td>
																							</tr>
																							<tr>
																								<td><input type='radio' id='radHala' name='radHala' value='N' class='noBorder' /></td>
																								<td><label id='lblHalaNo'>No thanks</label></td>
																							</tr>
																							<tr>
																								<td colspan='2' class='rowGap'></td>
																							</tr>
																							<tr>
																								<td colspan='2' align='center'>
																									<input type="button" id="btnHala"  value="Add to my trip Now" class="ButtonAncilary"/>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='alignLeft'>
																			<table id='tblHalaDetails' border='0' cellpadding='1' cellspacing='1' width='100%' style='display:none;'>
																				<tr>
																					<td>
																						<label></label>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowGap'></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'>
															<input type="hidden" name="paxWiseAnci" id="paxWiseAnci" />
															<input type="hidden" name="insurance" id="insurance" />
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align='letf'>
									<div class="buttonset">
										<!-- Button -->
										<table border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td><input type="button" id="btnCancel"  value="Start Over" class="Button ui-state-default ui-corner-all"/></td>					
												<td><input type="button" id="btnContinue"  value="Continue" class="Button ui-state-default ui-corner-all" /></td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- Bottom AA Logo -->
					<tr>
						<td class='appLogo'></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
 </form>

 <span id='spnSeatInfo' style='position:absolute;background-color:#FF2200;width:70px;height:30px;display:none;' class='seatTT ui-corner-all'>
	<table width='100%' border='0' cellpadding='0' cellspacing='1' >
		<tr>
			<td align='center'><span id='spnTTSeatNo' class='fntBold gridHDFont fntSmaller'></span></td>
		</tr>
		<tr>
			<td align='center'><span id='spnTTSeatChg' class='fntBold gridHDFont fntSmaller'></span></td>
		</tr>
	</table>
</span>

 <span id='spnSocial' style='position:absolute;display:none;' class='seatTT ui-corner-all'>
 	<table width='100%' border='0' cellpadding='0' cellspacing='1' >
		<tr>
			<td align='center'><span id='spnSocialSeating'></span></td>
		</tr>
	</table>
 </span>
<script src="../js/v2/reservation/anciStructures.js" type="text/javascript"></script>
 <script src="../js/v2/reservation/interLineAnci.js" type="text/javascript"></script>	
 </div>
 </body>
</html>