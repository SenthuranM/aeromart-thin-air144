<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<%@ page pageEncoding="UTF-8" %>   
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/interlinePgHD.jsp' %>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
	var strConfData = <c:out value="${requestScope.sesKioskConfData}" escapeXml="false" />;
	var strOHDLabels = <c:out value="${requestScope.ibeOHDLabels}" escapeXml="false" />;
	var timeOut = <c:out value="${requestScope.timeOut}" escapeXml="false" />;
</script>
</head>
  <body>
	<c:import url="../common/pageLoading.jsp"/>
    <div id="divLoadBg" style="width:auto;display: none;" class="rightColumn">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' id="PgOHDconfirm">	   
		<tr>
		<td align='center' class="outerPageBackGround">
			<table border='0' cellpadding='2' cellspacing='0' class='PageBackGround' width="100%">	
			<!--<table border='0' style='width:940px;' cellpadding='2' cellspacing='0' class='PageBackGround'>-->
			<!-- Top Banner -->
			<%--<c:import url="../../../../ext_html/header.jsp" /> --%> 
			<!--<tr>
				<td colspan='2' align='center'>
					<table width='97%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class="alignRight">
								<img border='0' class="linkImage cursorPointer" src="../images/home_no_cache.gif" id="linkHome"/>
							</td>
						</tr>
						<tr>
							<td class='rowSingleGap'></td>
						</tr>			
					</table>
				</td>
			 </tr> 
			-->
			<tr>
				<td colspan='2'>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td class='pnlTop pnlWidth'></td>
						<td class='pnlTopR'></td>
					</tr>
					<tr>
						<td class='pnlBottom'  colspan='2' valign="top" align="center" >
							<div class="" style="width: 100%;" class="stepsContainer">
								<c:import url="../../../../ext_html/booking_process.jsp" />
							</div>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class='rowGap'></td>
			</tr>
			<tr>
				<td align="center">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='pnlTop pnlWidth'></td>
							<td class='pnlTopR'></td>
						</tr>
						<tr>
							<td colspan="2" class="pnlBottom">
								<table width='100%' border='0' cellpadding='2' cellspacing='0'>
										<!--<tr>
											<td align='center'>
												<br/>
													<img src="../images/LogoAni<c:out value="${sessionScope.sysCarrier}" escapeXml="false" />_no_cache.gif" />
												<br/>
											</td>
										</tr>
										-->
										<tr>
											<td class="alignLeft paddingCalss">
												<label id="lblMsgThank"></label>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class="alignLeft paddingCalss">
												<label id="lblSummary" class='fntBold hdFont hdFontColor'>Reservation Summary</label>
											</td>
										</tr>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class="alignLeft paddingCalss">
												<font class="fntBold">									
														<br/>
															<label id="lblOHDResNo">Your Reservation number is:</label> &nbsp;
															<label id="resNo"></label>
														<br/><br/>							
														<label id="lblOnHoldTime">Dear Customer, your booking is placed ON-HOLD for</label>&nbsp;
														<label id="time"></label>&nbsp;
														<label id="lblOHDHours">hour(s)</label>
														<br/>
														<label id="lblOHDCollectItinerary">please make the payment & confirm your booking.</label>
														<br/>
														<span id="lblOHDExpiryMessage">Bookings not paid for within the time limit will automatically be cancelled.</span>	
														<br/>													
													<br/>
												</font>								
											</td>
										</tr>
									</table>
							</td>
						</tr>
						<tr id="trButtonPannel">
							<td colspan='2' class="alignLeft paddingCalss">
							<div class="buttonset">
								<!-- Button -->
								<table border='0' cellpadding='0' cellspacing='0' width="100%">
									<tr>
										<td class="alignLeft"><input type="button" id="btnFinish" name="btnFinish"  value="Finish" class="Button"/></td>
										<td class="alignRight"><!--<input type="button" id="btnPrint"  value="Print" class="Button" />--></td>
									</tr>
								</table>
							</div>
							</td>
						</tr>
					</table>
					<br/>
				</td>
			</tr>
				<!-- Bottom AA Logo -->
				<%-- <tr>
				<td class='appLogo' colspan="2"></td>
				</tr>
				<c:import url="../../../../ext_html/cfooter.jsp" />
				 --%>
			</table>
				</td>
			</tr>
		</table>
			<div id='divLoadMsg' class="mainPageLoader">
				<%@ include file='../common/includeLoadingMsg.jsp' %>
			</div>
		<form method="post" action="" name="formHidden" id="formHidden">
			<input type="hidden" id="hdnMode" name="hdnMode"/>
		</form>	
		<%@ include file='../common/iBECommonParam.jsp'%>
	</div>	
  	<script src="../js/v2/reservation/OHDConfirm.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
  </body>   
  <%@ include file='../common/inline_Tracking.jsp' %>	
</html>