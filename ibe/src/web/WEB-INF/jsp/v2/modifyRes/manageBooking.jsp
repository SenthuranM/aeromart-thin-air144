<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<title><fmt:message key="msg.Page.Title"/></title>
		<link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css"></link>
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		
		<script type="text/javascript">
	    var promoPnr = null;								
		var promoLastName = null;
		var promoDepDate = null;
		var GLOBALS = {};
		GLOBALS.skipPersianDateChange = <c:out value="${requestScope.commonParams.skipPersianCalendarChange}" escapeXml="false" />;
       </script>  	
	</head>
	<body>
		<c:import url="../common/pageLoading.jsp"/>
		<div id="divLoadBg" style="display: none;">		
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgManageBooking">
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<!-- Top Banner -->
							<c:import url="../../../../ext_html/header.jsp" />
							<!-- Content holder -->
							<tr id="trForm">
							  <td>
								<form id="frmManageBooking" name="frmManageBooking" method="post" action="">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td class="alignLeft mainbody">
												<div id="sortable-ul">
												<div class="sortable-child" style="width: 100%; ">
													<div class="rightColumn rightColumFix" style="width: 98%; ">
													<table width="100%" border="0" cellpadding="2" cellspacing="0" align='center'>
															<tr>
																<td class="alignLeft paddingCalss" align="left"><label class="fntBold hdFontColor hdFont" id="lblHdManageRes"></label></td>
															</tr>
															<tr>
																<td class="alignLeft paddingCalss" align="left"><label id="lblMsgEnterInfo"></label></td>
															</tr>
															<tr>
																<td></td>
															</tr>
															<tr>
																<td valign="top"  class="alignLeft paddingCalss">																
																	<table cellspacing="0" cellpadding="2" border="0" width="100%">																		
																		<tr>
																			<td colspan="4" class="alignLeft" align="left">
																				<span id="spnError" class="mandatory fntBold">
																				
																				</span>
																				<br/>
																			</td>
																		</tr>											
																		<tr>
																			<td width="50">&nbsp;</td>
																			<td width="140px" class="alignLeft" align="left">
																				<label class="fntBold" id="lblPnrNo"></label>
																			</td>
																			<td width="5">:</td>
																			<td width="250" class="alignLeft" align="left">
																				<input type="text" name="pnr" id="pnr"/>
																				<font class="mandatory"> *</font>
																			</td>
																		</tr>
																		<tr>
																			<td width="50">&nbsp;</td>
																			<td id="contactLastName" width="140" class="alignLeft" align="left"><label class="fntBold" id="lblContactLastName"></label></td>
																			<td id="contactOrPaxLastName" width="140" class="alignLeft" align="left"><label class="fntBold" id="lblPaxOrContactLastName"></label></td>
																			<td width="5">:</td>
																			<td class="alignLeft" width="250" align="left">
																				<input type="text" style="width: 220px;" name="lastName" id="txtLastName"/>
																				<font class="mandatory"> *</font></td>
																		</tr>
																		<tr>
																			<td width="50">&nbsp;</td>
																			<td width="140"class="alignLeft" align="left"><label class="fntBold" id="lblDepDate"></label></td>
																			<td width="5">:</td>
																			<td class="alignLeft" width="250" align="left">
																				<input type="text" style="width: 80px;" name="txtDateOfDep" id="txtDateOfDep"/>
																				<input type="hidden" name="dateOfDep" id="dateOfDep"/>
																				<input type="hidden" name="preDateOfDep" id="preDateOfDep"/>
																				<img border="0" src="../images/Calendar2_no_cache.gif" id="imgCalendar" align="top" class="cursorPointer" title="Click here to view calendar" />
																				<label class="mandatory"> *</label>																				
																			</td>
																		</tr>
																		
																		<tr id="trPanelImageCaptcha">
																			<td width="50">&nbsp;</td>
																			<td width="140"class="alignLeft" align="left">
																				<img src="../jcaptcha.jpg" align="top" style="cursor: pointer;" name="imgCPT" id="imgCPT"/>
																				<br/>
																				<label id="lblTextImagecaptura">Please Click on the image to change</label>
																			</td>
																			<td width="5">:</td>
																			<td class="alignLeft" width="250" align="left">														
																				<input type="text" style="width: 220px;" name="captcha" id="txtCaptcha"/>
																				<font class="mandatory"> *</font>
																			</td>
																		</tr>																		
																	  </table>																	
																</td>
															</tr>
															<tr>
																<td align="center">
																	 <br/>
																	 <input type="button" class="Button ui-state-default ui-corner-all"  title="Refresh the Word" tabindex="23" id="btnRefresh" style="display: none;"/> 
																	 <input type="button"  value="Previous" class="Button ui-state-default ui-corner-all" name="btnPrevious" id="btnPrevious"/>
																	 <input type="button" class="Button ui-state-default ui-corner-all"  title="Continue" tabindex="24" id="btnContinue"/></td>
															</tr>								
														</table>
														</div>
														</div>
														</div>
													</td>
												</tr>
											</table>									
										<%@ include file='../common/iBECommonParam.jsp'%>	
										<input type="hidden" id="groupPNR" name="groupPNR" />	
										<input type="hidden" id="airlineCode" name="airlineCode" />
										<input type="hidden" id="marketingAirlineCode" name="marketingAirlineCode" />	
										
										<script type="text/javascript">
													var pnr = "<c:out value="${requestScope.pnr}" escapeXml="false"/>";													
													var lname = "<c:out value="${requestScope.lname}" escapeXml="false"/>";
													var ddate = "<c:out value="${requestScope.ddate}" escapeXml="false"/>";
													var linkType = "<c:out value="${requestScope.link}" escapeXml="false"/>";
													var httpsEnabled = "<c:out value="${requestScope.httpsEnable}" escapeXml="false"/>";	
										</script>	
										</form>
								  </td>
							 </tr>
							 <tr>
								<td class='appLogo' colspan="2"></td>
							</tr>
							<c:import url="../../../../ext_html/cfooter.jsp" />														
						</table>
					</td>
				</tr>
			</table>
		</div>
		
		<script type="text/javascript">
			promoPnr = "<c:out value="${requestScope.promoPnr}" escapeXml="false"/>";									
			promoLastName = "<c:out value="${requestScope.promoLastName}" escapeXml="false"/>";
			promoDepDate = "<c:out value="${requestScope.promoDepDate}" escapeXml="false"/>";
	   </script>
		
		<script type="text/javascript">
			var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
		</script>
		<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />					
		<script src="../js/v2/modifyRes/manageBooking.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
		<form id="submitForm" method="post">
		</form>
		<div id='divLoadMsg' class="mainPageLoader">
			<%@ include file='../common/includeLoadingMsg.jsp' %>
		</div>
	</body>
	<%@ include file='../common/inline_Tracking.jsp' %>
</html>