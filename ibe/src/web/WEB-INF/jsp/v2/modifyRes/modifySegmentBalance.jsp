<div id="balanceSummary">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class='pnlTop pnlWidth'></td>
			<td class='pnlTopR'></td>
		</tr>	
		<tr>
			<td class='pnlBottomTall'  colspan='2' valign="top">
				<table cellspacing="1" cellpadding="2" border="0" width="100%" class="" id="modifyBal">										
						<tr>
							<td colspan="3" class="alignLeft paddingL5">
							<label id="lblPaymentDetails" class="fntBold fntRed">Payment Details</label></td>
						</tr>
						<tr>
							<td
								class="gridHD"><font>&nbsp;</font></td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblModifingFlight" class="fntBold fntWhite">Modifying Flight(s)&nbsp;</label>
								<label id="lblCurrentCharges" class="fntBold fntWhite" style="display: none;">Current OND Charges&nbsp;</label>
							</td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblNewFlight" class="fntBold fntWhite">New Flight(s)&nbsp;</label>
								<label id="lblNewCharges" class="fntBold fntWhite" style="display: none;">New OND Charges&nbsp;</label></td>
						</tr>
						<tr id="balanceSummaryTemplate">
							<td
								class="SeperateorBGAlt rowDataHeight GridItems alignLeft">
								<label id="displayDesc"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayOldCharges" class="fntRed"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayNewCharges" class="fntRed"></label>
							</td>
						</tr>
				</table>
				</td>
			</tr>
		</table>
</div>