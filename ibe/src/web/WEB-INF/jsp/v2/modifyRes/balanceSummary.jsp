<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<form id="frmBalanceSummay" method="post">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td valign="top" style="height: 200px;" class="tblBG alignLeft">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">				  
				 <tr>
						<td class="defaultRowGap">
						</td>
				</tr>
				<tr>
						<td class="alignLeft">
							<label id="lblConfirmCancel" class="fntBold hdFont hdFontColor paddingCalss">Confirm Cancellation</label>
						</td>
				</tr>
				<tr>
						<td class="defaultRowGap">
				</td>
				</tr>				
				<tr>
					<td valign="bottom" colspan="2" class="alignLeft paddingCalss">						
						<label  id="lblCancelMsg1">Cancellation charges of </label> <label id="cancelAmount" class="fntBold"> </label>&nbsp; <label  id="lblCancelMsg2"> (or equivalent) will be applied per passenger.</label>						
					</td>
				</tr>
				<tr>
						<td class="defaultRowGap"> 	</td>
				</tr>
				
				<tr>
					<td valign="bottom" class="alignLeft paddingCalss">
							<label id="lblResNo" class="fntRed">Reservation Number : </label> <label class="fntBold fntLarge" id="reservationNum"></label>
					</td>
				</tr>
				<tr>
						<td class="rowGrap"> </td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">										
						    <tr>
								<td valign='top'>													
									<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
											<tr>
												<td colspan="7" class="alignLeft GridHeader"><label id="lblCancelFlight"  class='fntBold gridHDFont'>Cancelling Flight(s)</label></td>
											</tr>	
											<tr>
												<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'>Origin / Destination</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblDepature" class='gridHDFont fntBold'>Departure</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'>Arrival</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'>Flight No</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'>Duration</label></td>												
											</tr> 
											<tr>            
												<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'>Time</label></td>
												<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'>Time</label></td>            
											</tr>											
											<tr id="departueFlightCancels">
												<td width='35%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
												<td width='15%' class='rowColor' align='center'>
													<label id="departureDate"  class="date-disp"></label>
													<label id="departureDateValue" class="date-hid" style="display:none"></label>
												</td>
												<td width='8%' class='rowColor' align='center'><label id="departureTime"></label></td>
												<td width='15%' class='rowColor' align='center'>
													<label id="arrivalDate"  class="date-disp"></label>
													<label id="arrivalDateValue" class="date-hid" style="display:none"></label>
												</td>
												<td width='8%' class='rowColor' align='center'><label id="arrivalTime"></label></td>
												<td width='10%' class='rowColor' align='center'><label id="flightNo"></label></td>
												<td width='9%' class='rowColor'  align='center'><label id="duration"></label></td>												
											</tr> 
											<tr style="display: none;" id="separator">
												<td style="height: 10px;" class="GridItems SeperateorBGColor" colspan="7">
												</td>
											</tr> 											            
										</table>																																
									</td>
								</tr>
								<tr>
										<td class="rowGrap">&nbsp; </td>
								</tr>
								<tr>
									<td colspan="2">									
										<table cellspacing="1" cellpadding="2" border="0" width="100%" 	class="GridTable">											
												<tr>
													<td class="GridHeader alignLeft" colspan="2">
													<label id="lblPaymentDetails" class="fntBold fntWhite">Payment Details</label></td>
												</tr>
												<tr>
													<td class="GridItems alignLeft"><label id="lblAirFare">&nbsp;Airfare</label></td>
													<td align="right" width="20%" class="GridItems">
														<label id="airFareAmount"></label>
													</td>
												</tr>
												<tr>
													<td class="GridItems alignLeft">
														<label id="lblTaxNSurcharges">&nbsp;Taxes &amp; Surcharges</label></td>
														<td align="right" class="GridItems">
														<label id="surchargesAmount"></label></td>
												</tr>
												<tr>
													<td style="height: 5px;" class="GridItems SeperateorBGColor" colspan="2"></td>
												</tr>
												<tr>
													<td class="GridItems alignLeft">
														<label id="lblDiscount">&nbsp;Discount</label></td>
														<td align="right" class="GridItems">
														<label id="discountAmount"></label>
													</td>
												</tr>
												<tr>
													<td style="height: 5px;" class="GridItems SeperateorBGColor" colspan="2"></td>
												</tr>												
												<tr>
													<td class="GridItems alignLeft">
													<label class="fntBold fntEnglish" id="lblCancelCharge">&nbsp;Cancellation Charge</label></td>													
													<td align="right" class="GridItems">
														<label class="fntBold" id="cancelChargeAmount"> </label>
													</td>													
												</tr>
												<tr>
													<td style="height: 5px;" class="GridItems SeperateorBGColor"
														colspan="2"></td>
												</tr>
												<tr>
													<td class="GridItems alignLeft">
														<label class="fntBold" id="lblCreditableAmount">&nbsp;Creditable Amount</label></td>													
														<td align="right" class="GridItems"><label class="fntBold" id="creditableAmount"></label></td>
												</tr>
												<tr id="trSeperator">
													<td style="height: 5px;" class="GridItems SeperateorBGColor"
														colspan="2"></td>
												</tr>
												<tr id="trReservationCredit">
													<td class="GridItems alignLeft">
													<label class="fntBold fntEnglish" id="lblAvailableCredit">&nbsp;Available Credit</label></td>													
													<td align="right" class="GridItems"><label class="fntBold" id="availableCredit"></label>
													</td>													
												</tr>
												<tr>
													<td style="height: 5px;" class="GridItems SeperateorBGColor"
														colspan="2"></td>
												</tr>
												<tr>
													<td class="GridItems alignLeft">
													<label class="fntBold fntEnglish" id="lblFinalCredit">&nbsp;Final Credit</label></td>													
													<td align="right" class="GridItems">
														<label class="fntBold" id="finalCreditAmount"></label>
													</td>													
												</tr>											
										</table>										
									</td>
								</tr>
								<tr>
									<td class="defaultRowGap"></td>
								</tr>
								<tr id="flexiMessage" style="display: none;">
									<td colspan="2" class="alignLeft paddingCalss">
										<label id="lblRemainFlexi" class="fntEnglish fntBold">Remaining Flexibilities: </label><label id="spnFlexibilities" style="color: red;">No more flexibilities available for the cancelled segment</label>
									</td>
								</tr>
									
								<tr>
										<td class="defaultRowGap"> 	</td>
								</tr>
								<tr>
									<td colspan="2" class="alignLeft paddingCalss">
										<label id="lablMsgCredit" class="fntEnglish">Click on
									'Confirm' to continue with the cancellation. Applicable cancellation
									charges will be deducted and the balance amount will be credited to
									your account. </label>
									<div id="divRedeemCancel" class="divfont">
									To use your credit for a new reservation, please
									contact our <a target="_top"
										href=""><u>Call
									Centre</u></a> in your region.</div>
									</td>
								</tr>
								<tr>
									<td valign="bottom" class="rowGap" colspan="2"></td>
								</tr>
								<tr>
									<td align="center" colspan="2">
										<input type="button"  value="Back" title="Click here to go to the previous page" class="Button" id="btnBackBalanceSummary">
										<input type="button"  value="Confirm" title="Click here to cancel the segment" class="Button" id="btnCancelConfirm">
									</td>
								</tr>
						</table>
					</td>
				</tr>					
			</table>
		 </td>
	</tr>
  </table>
</form>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/modifyRes/cancelBalanceSummary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</c:if>