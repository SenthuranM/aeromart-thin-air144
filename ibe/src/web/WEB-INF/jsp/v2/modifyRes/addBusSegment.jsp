<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<form id="frmCusAddBusSegment" method="post">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td valign="top" style="height: 200px;" class="tblBG alignLeft">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" >
				  <tr>
					<td class="rowGap">
					</td>
			     </tr>
				 <tr>
						<td class="rowGap">
						</td>
				</tr>
				<tr>
						<td>
							<label id="lblHDAddBusSegment" class="fntBold hdFont hdFontColor paddingCalss">Add Bus Segment</label>
						</td>
				</tr>
				<tr>
						<td class="rowGrap">
				</td>
				</tr>
				<tr>
					<td valign="bottom">
								<label id="lblResNo" class="fntRed paddingCalss">Reservation Number :</label> <label class="fntBold fntLarge" id="spnPNR"></label>
					</td>
				</tr>
				 <tr>
						<td class="rowGap">
						</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%">										
						    <tr>
								<td valign='top'>													
									<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
											<tr>
												<td colspan="7" class="alignLeft Gridwt"><label id="lblBusConnectingFlight"  class='fntBold hdFontColor'>Bus Connecting Flight</label></td>
											</tr>	
											<tr>
												<td rowspan="2" align='center' class='gridHD'><label id="lblOnd" class='gridHDFont fntBold'>Origin / Destination</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'>Departure</label></td>
												<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'>Arrival</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'>Flight No</label></td>
												<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'>Duration</label></td>												
											</tr> 
											<tr>            
												<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'>Time</label></td>
												<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'>Date</label></td>
												<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'>Time</label></td>            
											</tr>
											<tr id="departueFlightMS">
												<td width='35%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
												<td width='15%' class='rowColor' align='center'><label id="departureDate"></label></td>
												<td width='8%' class='rowColor' align='center'><label id="departureTime"></label></td>
												<td width='15%' class='rowColor' align='center'><label id="arrivalDate"></label></td>
												<td width='8%' class='rowColor' align='center'><label id="arrivalTime"></label></td>
												<td width='10%' class='rowColor' align='center'><label id="flightNo"></label></td>
												<td width='9%' class='rowColor'  align='center'><label id="duration"></label></td>										
											</tr>               
										</table>																																
									</td>
								</tr>
								<tr>
									<td align="center">
									<input type="button"  value="Back" class="Button" title="Click here to go to the previous page" id="btnBackAddBusSegment">
								</td>
							</tr>
							<tr>
								<td>
									<label id="lblMsgUseSearch">Please use the Booking Form on the left to search for buses connecting to this flight</label>
								</td>
							</tr>
						</table>
					</td>
				</tr>					
			</table>
		 </td>
	</tr>
  </table>
  
</form>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/modifyRes/addBusSegment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</c:if>