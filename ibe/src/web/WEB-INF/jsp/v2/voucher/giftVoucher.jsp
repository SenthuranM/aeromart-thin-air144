<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ include file="/WEB-INF/jsp/common/directives.jsp"%>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<title><fmt:message key="msg.Page.Title" /></title>

<link rel='stylesheet' type='text/css' href='../css/table_layout_EN.css' />
<link rel='stylesheet' type='text/css' href='../css/myStyleV3_no_cache.css' />
<link rel="stylesheet" type="text/css" href="../css/remove-jquery_no_cache.css" />
<script	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<%@ include file='../common/interlinePgHD.jsp'%>
<script	src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script type="text/javascript"	src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script	src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script src="../js/v2/voucher/giftVoucher.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false'/>" type="text/javascript"></script>
<script src="../js/v2/common/cardValidator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false'/>" type="text/javascript"></script>
</head>

<body>
	<form id="frmVoucherPurchase">	
		<div id="divLoadBg">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Top Banner -->
				<c:import url="../../../../ext_html/header.jsp" />
				<tr>
					<td width="10%"></td>
					<td width="90%" align='left' class="outerPageBackGround">
						<table style='width: 680px;' border='0' cellpadding='0'
							cellspacing='0' align='center' class='PageBackGround'>
	
							<tr>
								<td colspan="2" class="mainbody">
									<div class="page-body" style="width: 100%;"></div>
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td class="alignLeft paddingCalss pageName"><label
												id="promoHeading" class="hdFontColor fntBold">Gift
													Vouchers</label></td>
										</tr>
										<tr>
											<td class="setHeight" colspan="4"></td>
										</tr>
										<table width="660px" border="0" cellspacing="0" cellpadding="0"	id="tableModSearch">
											<tr>
												<td colspan="3"></td>
											</tr>
											<%@ include file="../common/includeFrameTop.html"%>
											<table width='auto' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td valign='top'>
														<div style="width: 660px; height: 300px" ; id="divModSeach">
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td class="alignLeft" width="auto"><label
																			class="fntBold hdFontColor" id="lblVoucherDetails">Gift Voucher details</label></td>
																		<td></td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td class="alignLeft" width="100">
																			<label class="fntBold" style="white-space: nowrap;"> Voucher Name </label>
																		</td>
																		<td class="alignLeft" width="230">
																			<font>:</font>&nbsp;<select id='voucherName' size='1' name='voucherName' style="width: 220px"></select>
																		</td>
																		<td></td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td width="100" class="alignLeft">
																			<label id="lblAmount" class="fntBold"> Amount</label>
																		</td>
																		<td class="alignLeft" style='width: 100'><font>:</font>&nbsp;
																			<input type="text" id="amountInLocal" name="amountInLocal" style="width: 125px; height: 15px" />
																		</td>
																		<td width="15" class="alignLeft">
																			<label id="lblSpace" class="fntBold">&nbsp;&nbsp;</label>
																		</td>
																		<td style='width: 60px;' class="alignLeft">
																			<input type="text" id="currencyCode" name="currencyCode" style="width: 75px; height: 15px" />
																		</td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td width="100" class="alignLeft">
																			<label id="lblDepartOn" class="fntBold" style="white-space: nowrap;"> Valid From </label>
																		</td>
																		<td width="230" class="alignLeft">
																			<font>:</font>&nbsp;<input type="text" id="validFrom" name="validFrom" style="width: 125px; height: 15px" />
																		</td>
																		<td width="70" class="alignLeft">
																			<label id="lblReturnOn" class="fntBold" style="white-space: nowrap;"> Valid To </label>
																		</td>
																		<td width="auto" class="alignLeft">
																			<font>:</font>&nbsp;
																			<input type="text" id="validTo" name="validTo" style="width: 125px; height: 15px" />
																		</td>
																		<td></td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td class="alignLeft" width="auto">
																			<label class="fntBold hdFontColor" id="lblPassengerDetails">Passenger Details</label>
																		</td>
																		<td></td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td width="100" class="alignLeft">
																			<label id="lblFirstName" class="fntBold"> First Name</label>
																		</td>
																		<td class="alignLeft" style='width: 100;'>
																			<font>:</font>&nbsp;
																			<input type="text" id="paxFirstName" name="voucherDTO.paxFirstName" style="width: 125px; height: 15px" />
																			<font class="mandatory">*</font>
																		</td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td width="100" class="alignLeft">
																			<label id="lblLastName" class="fntBold"> Last Name</label>
																		</td>
																		<td class="alignLeft" style='width: 100;'>
																			<font>:</font>&nbsp;
																			<input type="text" id="paxLastName" name="voucherDTO.paxLastName" style="width: 125px; height: 15px" />
																			<font class="mandatory">*</font>
																		</td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td width="100" class="alignLeft">
																			<label id="lblEmail" class="fntBold"> Email</label>
																		</td>
																		<td class="alignLeft" style='width: 100;'>
																			<font>:</font>&nbsp;
																			<input type="text" id="email" name="voucherDTO.email" style="width: 125px; height: 15px" />
																			<font class="mandatory">*</font>
																		</td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
																<div class="alignLeft">
																	<table width='auto' border='0' cellpadding='0' cellspacing='0'>
																		<tr>
																			<td width="100" class="alignLeft">
																				<label id="lblRemarks" class="fntBold"> Remarks</label>
																			</td>
																			<td class="alignLeft" style='width: 100;'>
																				<font>:</font>&nbsp;
																				<textarea id="remarks" name="voucherDTO.remarks" style="width: 225px; height: 50px" maxlength="500"></textarea>
																			</td>
																		</tr>
																	</table>
																</div>
															<div class='rowGap'></div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td width="100" class="alignLeft">
																			<input type="button" id="btnBuy" name="btnBuy" class="Button" value="Buy">
																		</td>
																		<td width="100" class="alignLeft">
																			<input type="button" id="btnReset" name="btnReset" class="Button" value="Reset">
																		</td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
															<div class='rowGap'></div>
															<div class="alignLeft" style="display: none"
																id="divTotalAmount">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="3" id="tblPriceBreakdown">
																	<tr>
																		<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '>
																			<label id="lblAmount" class='fntBold'>Voucher Price</label>
																		</td>
																		<td align='right' class='rowColorAlternate bdBottom bdRight'>
																			<label id="lblVoucherAmount" name="lblAmount" class='fntBold hdFontColor' align="left"></label>
																		</td>
																		<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '>
																			<label id="lblTxDue" class='fntBold'>Transaction Fee</label>
																		</td>
																		<td align='right' class='rowColorAlternate bdBottom bdRight'>
																			<label id="lblTnxAmount" name="lblTnxAmount" class='fntBold hdFontColor' align="left"></label>
																		</td>
																		<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '>
																			<label id="lblTotalDue" class='fntBold'>Total Amount</label>
																		</td>
																		<td align='right' class='rowColorAlternate bdBottom bdRight'>
																			<label id="lblTotalDueAmount" name="lblTotalDueAmount" class='fntBold hdFontColor' align="left"></label>
																		</td>
																	</tr>
																</table>
															</div>
															<div class='rowGap'></div>
	
															<div class='rowGap'></div>
	
	
															<div class="alignLeft" id="postPaymentSuccess"
																style="display: none">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '>
																			<label id="lblMsg" class='fntBold'>Your Payment is Success</label>
																		</td>
																</table>
															</div>
															<div class="alignLeft" id="postPaymentDetails"
																style="display: none">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '>
																			<label id="lblPostPaymentDetails" class='fntBold'></label>
																		</td>
																</table>
															</div>
															<div class="alignLeft" id="emailSuccess"
																style="display: none">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td class='defaultRowGap rowColorAlternate bdLeft bdBottom bdRight '>
																			<label id="lblMsg" class='fntBold'>Email is sent successfully</label>
																		</td>
																	</tr>
																</table>
															</div>
	
															<div class='rowGap'></div>
	
	
															<%--Payment options --%>
	
															<div id="trPaymentMethods">
																<div id="divPayment">
																	<div class='tabBody'>
																		<%@ include file="../common/cardDetail.jsp"%>
																	</div>
																</div>
															</div>
															<div class="alignLeft">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr id="IPNotification">
																		<td class="alignLeft paddingL5" colspan="2">
																		<label id="lblNofication" class='fntBold'>Notification</label>
																		<label class="paddingL3 fntBold">:</label>
																		<label class="paddingL3" id="lblMsgRecordIP">Air Arabia records IP address for all web bookings for security reasons.</label>
																		<br/>
																		<label id="lblYourIPIs" class='fntBold'>Your IP address is </label>
																		<label class="paddingL3 fntBold">:</label>
																		<label class="paddingL3" id='lblYourIP'></label></td>
																	</tr>
	
																</table>
															</div>
															<div class='rowGap'></div>
															<div class='rowGap'></div>
															<div class='rowGap'></div>
															<div class="alignRight" id="paymentButtonset">
																<table width='auto' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td width="100" class="alignLeft">
																			<input type="button" id="btnPurchase" name="btnPurchase" class="Button" value="Purchase"/>
																		</td>
																	</tr>
																</table>
															</div>
	
															<div class='rowGap'></div>
														</div>
													</td>
												</tr>
	
											</table>
	
	
											<%@ include file="../common/includeFrameBottom.html"%>
										</table>
									</table>
								</td>
							</tr>
	
							<td class='appLogo' colspan="2"></td>
							</tr>
							<c:import url="../../../../ext_html/cfooter.jsp" />
						</table>
					</td>
	
					</td>
				</tr>
			</table>
		</div>
		<div id='divLoadMsg' class="mainPageLoader">
			<%@ include file='../common/includeLoadingMsg.jsp'%>
		</div>
			<input type="hidden" id="hdnTnxFee" />
			<input type="hidden" id="amountInLocal" name="amountInLocal" />
			<input type="hidden" id="payCurrency" name="pgw.payCurrency" />
			<input type="hidden" id="providerName" name="pgw.providerName" />
			<input type="hidden" id="providerCode" name="pgw.providerCode" />
			<input type="hidden" id="description" name="pgw.description" />
			<input type="hidden" id="paymentGateway" name="pgw.paymentGateway" />
			<input type="hidden" id="payCurrencyAmount"	name="pgw.payCurrencyAmount" />
			<input type="hidden" id="brokerType" name="pgw.brokerType" />
			<input type="hidden" id="switchToExternalURL" name="pgw.switchToExternalURL" />
			<input type="hidden" id="hdnMode" name="hdnMode" />
			<input type="hidden" id="vcardType" name="voucherDTO.voucherPaymentDTO.cardType" />
			<input type="hidden" id="vcardNumber" name="voucherDTO.voucherPaymentDTO.cardNumber" />
			<input type="hidden" id="vcardExpiry" name="voucherDTO.voucherPaymentDTO.cardExpiry" />
			<input type="hidden" id="vcardCVV" name="voucherDTO.voucherPaymentDTO.cardCVV" />
			<input type="hidden" id="vcardHolderName" name="voucherDTO.voucherPaymentDTO.cardHolderName" />
			<input type="hidden" id="vpaymentMethod" name="voucherDTO.voucherPaymentDTO.paymentMethod" />
			<input type="hidden" id="vamountLocal" name="voucherDTO.voucherPaymentDTO.amountLocal" />
			<input type="hidden" id="vcardTxnFeeLocal" name="voucherDTO.voucherPaymentDTO.cardTxnFeeLocal" />
			<input type="hidden" id="vipgId" name="voucherDTO.voucherPaymentDTO.ipgId" />
			<input type="hidden" id="camountInLocal" name="voucherDTO.amountInLocal" />
			<input type="hidden" id="templateId"  name="voucherDTO.templateId" />
			<input type="hidden" id="vcurrencyCode"  name="voucherDTO.currencyCode" />
			<input type="hidden" id="vvalidFrom" name="voucherDTO.validFrom" />
			<input type="hidden" id="vvalidTo" name="voucherDTO.validTo" />
	</form>
</body>

</html>