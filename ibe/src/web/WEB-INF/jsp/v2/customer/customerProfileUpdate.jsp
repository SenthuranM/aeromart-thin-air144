<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='/WEB-INF/jsp/common/topHolder.jsp' %>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr id="trProfile">
	<td>
		<div id="profilePage" class="">
			 <form name="frmCutUpdate" id="frmCutUpdate" method="post" action="">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"> 
						<tr>
							<td colspan='4' class="alignLeft">
								<label class="fntBold  hdFont hdFontColor" id="lblChangeDetails">  </label>											 
							</td>
						</tr>
						<tr>
							<td  colspan='4' class="rowGap">
							</td>
						</tr>
						<tr>
                            <td colspan="4" valign="top" class="alignLeft">
                              <div id="spnError" class="mandatory" style=""></div>	                                          
                            </td>
                        </tr>
						<tr>
							<td colspan="4" class="alignLeft">
								<label id="lblInfoChangeDetails"></label>
							</td>
						</tr>
						<tr>
							<td  colspan='4' class="rowGap">
							</td>
						</tr>
						<tr>
							<td colspan="4" class="alignLeft">
								<label class="fntSmall" id="lblInfoFillReg"></label>
								<label class="fntSmall" id="lblHighlightWith"> </label> <font class="mandatory">*</font> <label id="lblAreMandatory"></label>
							</td>
						</tr>	
						<tr>
							<td  colspan='4' class="rowGap">
							</td>
						</tr>
						<tr id="trEmail">
							<td  colspan='4'>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td class='pnlTop' style="width:10px"></td>
								<td class='pnlTop pnlWidth'></td>
								<td class='pnlTopR'></td>
							</tr>
							<tr>
							<td class="pnlBottom"></td>
							<td class="pnlBottom">
								<table width="100%" border="0" cellpadding="2" cellspacing="0">
								<tr>
									<td colspan="4" class="alignLeft">
										<label class="fntBold hdFontColor" id="lblLoginInfo"></label>
									</td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
								<tr>
									<td width="25%" class="alignLeft">
										<label id="lblEmail"> </label>
									</td>
									<td width="1%">
										<label>:</label>
									</td>
									<td width="33%" class="alignLeft">
										<input type="text" style="width: 190px;" readonly="readonly" title="login ID" maxlength="100" size="25" name="customer.emailId" id="txtEmail"/>
									</td>
									<td width="43%" class="alignLeft">	
										<label class="fntSmall" id="lblInfoLoginID"></label>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<table border="0" cellpadding="1" cellspacing="0" width="100%" id="tblPassword">
										<tr>
											<td width="25%" class="alignLeft">
												<label id="lblPassword"></label>
											</td>
											<td width="1%">
												<label>:</label>
											</td>
											<td width="33%" class="alignLeft">
												<input type="password" style="width: 150px;" maxlength="12" size="10" name="customer.password" id="txtPassword"/> 
											    <label class="mandatory" id="lblPassowrdMand">*</label>
											</td>
											<td width="43%" valign="top" rowspan="3" class="alignLeft">
												<label id="lblInfoPassword" class="fntSmall"></label>
											</td>
										</tr>
										<tr>
											<td class="alignLeft"><label id="lblConfirmPwd"></label></td>
											<td><label>:</label></td>
											<td class="alignLeft">
												<input type="password" style="width: 150px;" maxlength="12" size="10" name="txtConPassword" id="txtConPassword"/> 
												<label class="mandatory" id="lblPassowrdConfMand">*</label>
											</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>
								</td>
								<td class="pnlBottom"></td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
							</table>
							</td>
						</tr>	
						<tr id="tblSecurityQuestion">
							<td  colspan='4'>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td class='pnlTop' style="width:10px"></td>
										<td class='pnlTop pnlWidth'></td>
										<td class='pnlTopR'></td>
									</tr>
									<tr>
									<td class="pnlBottom"></td>
									<td class="pnlBottom">
										<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%">
											<tbody>
												<tr>
													<td colspan="4">
														<table border="0" cellpadding="1" cellspacing="0" width="100%" >
															<tr>
																<td colspan="4" class="alignLeft">
																	<label id="lblForgotPwd" class="fntBold hdFontColor"></label>
																</td>
															</tr>
															<tr>
																<td class="alignLeft">
																	<label id="lblSecurityQues"></label>
																</td>
																<td><label>:</label></td>
																<td colspan="2" class="alignLeft">
																	<select  name="customer.secretQuestion" id="selSecQ" style="width:300px" size="1">	
																		<option value="">[Please Select a Question]</option>													
																	</select>
																	<label class="mandatory" id="lblSecQuesMand">*</label>
																</td>
															</tr>	
															<tr>
																<td width="25%" class="alignLeft">
																	<label id="lblYourAnswer"></label>
																</td>
																<td width="1%">
																	<label>:</label>
																</td>
																<td width="33%" class="alignLeft">					
																	<input type="text" maxlength="100" size="30" name="customer.secretAnswer" id="txtAns"/> <label class="mandatory" id="lblSecAnswerMand">*</label>
																</td>
																<td width="44%" rowspan="2" class="alignLeft">
																	<label class="fntSmall" id="lblInfoSecurityQues"></label>
																</td>
															</tr>
														</table>
													</td>
												</tr>											
												<tr id="trAlternateEmail">
													<td width="25%"  class="alignLeft"><label id="lblAltEmail"></label></td>
													<td width="1%"><label>:</label></td>
													<td class="alignLeft" colspan="2">					
														<input type="text" maxlength="100" name="customer.alternativeEmailId" size="30" id="txtAlternateEmail"/>
														<label class="mandatory" id="lblAlternateEmailMand">*</label>
													</td>
												</tr>	
												<tr>
													<td class="rowGap" colspan="4">
													</td>
												</tr>													
											</tbody>
										</table>
									</td>
									<td class="pnlBottom"></td>
									</tr>
									<tr>
										<td  colspan='4' class="rowGap">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr id="tblContactDetails">
						<td  colspan='4'>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td class='pnlTop' style="width:10px"></td>
										<td class='pnlTop pnlWidth'></td>
										<td class='pnlTopR'></td>
									</tr>
									<tr>
									<td class="pnlBottom"></td>
									<td class="pnlBottom">
										<table width="100%" border="0" cellpadding="2" cellspacing="0" align='center'>
											<tr>
												<td colspan="4" class="alignLeft">
													<label class="fntBold hdFontColor" id="lblAboutU"></label>
												</td>
											</tr>
											<tr>
												<td width="25%" class="alignLeft">
													<label id="lblTitle"></label>
												</td>
												<td width="1%">
													<label>:</label>
												</td>
												<td width="33%" class="alignLeft">
													<select  style="width: 50px;" size="1" name="customer.title" id="selTitle">
													</select>
													<label class="mandatory" id="lblTitleMand"> *</label>
												</td>
												<td width="44%"><label> </label></td>
											</tr>
											<tr id="trFirstName">
												<td class="alignLeft">
													<label id="lblFirstName"> </label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="text" readonly="" class="fontCapitalize" maxlength="50" size="50" name="customer.firstName" id="txtFirstName"/> 
													<label class="mandatory" id="lblFirstNameMand">*</label>
												</td>
											</tr>
											<tr id="trLastName">
												<td class="alignLeft">
													<label id="lblLastName"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="text" readonly="" class="fontCapitalize" maxlength="50" size="50" name="customer.lastName" id="txtLastName"/> 
													<label class="mandatory" id="lblLastNameMand">*</label>
											</td></tr>
											<tr id="trGender">
												<td class="alignLeft">
													<label id="lblGender"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="radio" class="NoBorder" name="customer.gender" id="radGender" value="M" checked="checked"/>
													<label id="lblMale"></label>
													<input type="radio" class="NoBorder" name="customer.gender" id="radFemale" value="F"/>
													<label id="lblFemale"></label>
													<label class="mandatory" id="lblGennderMand"> *</label>
												</td>
											</tr>
											<tr id="trDOB">
												<td class="alignLeft">
													<label id="lblDOB"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<table cellspacing="2" cellpadding="0" border="0">
														<tbody>
															<tr>
															<td class="alignLeft">
																<input type="text" invalidtext="true"  size="10" maxlength="10" name="customer.dateOfBirth" id="txtDateOfBirth"/> 
															</td>
															<td valign="baseline" class="alignLeft">
																<img border="0" src="../images/Calendar2_no_cache.gif" id="imgCalendar" class="cursorPointer"/>
																<label class="mandatory" id="lblDOBMand"> *</label>
															</td>														
														</tr>
													   </tbody>
												  </table>
												</td>
											</tr>
											<tr id="trNationality">
												<td class="alignLeft">
													<label id="lblNationality"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<select style="width: 140px;" size="1" name="customer.nationalityCode" id="selNationality">
													</select>
													<label class="mandatory" id="lblNationalityMand">*</label>
												</td>
											</tr>
											<tr id="trAddr1">
												<td>
													<label id="lblAddress"></label>
												</td>
												<td><label>:</label>
												</td>
												<td colspan="2">
													<input type="text" maxlength="100" size="50" name="customer.addressStreet" id="txtAddr1"/> 
													<label class="mandatory" id="lblAddrMand">*</label>
												</td>
											</tr>
											<tr id="trAddr2">
												<td></td>
												<td><label>:</label></td>
												<td colspan="2">
													<input type="text" maxlength="100" size="50" name="customer.addressLine" id="txtAddr2"/>
												</td>
											</tr>
											<tr id="trCity">
												<td class="alignLeft">
													<label id="lblCity"></label>
												</td>
												<td><label>:</label>
												</td>
												<td colspan="2" class="alignLeft">
													<input type="text" class="fontCapitalize" maxlength="50" size="50" name="customer.city" id="txtCity"/> 
													<label class="mandatory" id="lblCityMand">*</label>
												</td>
											</tr>
											<tr id="trZipCode">
												<td>
													<label id="lblZipCode"></label>
												</td>
												<td><label>:</label>
												</td>
												<td colspan="2">
													<input type="text" maxlength="10" size="10" name="customer.zipCode" id="txtZipCode"/> 
													<label class="mandatory" id="lblZipCodeMand">*</label>
												</td>
											</tr>
											<tr id="trCountry">
												<td class="alignLeft">
													<label class="" id="lblCountryResidence"></label>
												</td>
												<td><label>:</label></td>
												<td colspan="2" class="alignLeft">
													<select style="width: 140px;"  size="1" name="customer.countryCode" id="selCountry">												
													</select>
													<label class="mandatory" id="lblCountryMand">*</label>
												</td>
											</tr>
											<!-- Contact Details -->
											<tr class="trPhoneNoHeader">
												<td colspan="3">
													<label class="fntBold hdFontColor" id="lblContactDetails"></label>
												</td>
											</tr>															
											<tr class="trPhoneNoLabel">
												<td valign="top" class="fntSmall" width="25%"></td>
												<td width="1%"><label>:</label></td>
												<td width="74%">
													<table cellspacing="0" cellpadding="2" border="0">
														<tbody>
														  <tr>
															<td align="left"><label class="fntSmall " id="lblCountryCode"></label></td>
															<td align="left"><label class="fntSmall areaCode" id="lblAreaCode"></label></td>
															<td align="left"><label class="fntSmall " id="lblNumber"></label></td>
															<td align="left"><label class="fntSmall "> </label></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="trMobileNo">
												<td valign="top" class="fntSmall"><label id="lblMobileNo"></label></td>
												<td><label>:</label></td>
												<td colspan="2">
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtMCountry" id="txtMCountry"/></td>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtMArea" id="txtMArea" class="areaCode"/></td>
															<td align="left"><input type="text"  maxlength="10" style="width: 100px;" name="txtMobile" id="txtMobile"/></td>
															<td align="left"><label class="mandatory" id="lblMobileMand">*</label></td>
															<td>&nbsp;</td>
															<td><span id="atLeasetOne"><font class="mandatory" >*&nbsp;</font> <label class="fntSmall" id="lblAtleaseOneText">Please provide at least one phone Number</label></span></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="trPhoneNo">
												<td valign="top" class="fntSmall">
													<label id="lblTelNo"></label>
												</td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtPCountry" id="txtPCountry"/></td>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtPArea" id="txtPArea" class="areaCode"/></td>
															<td align="left"><input type="text"  maxlength="10" style="width: 100px;" name="txtTelephone" id="txtTelephone"/></td>
															<td align="left">
															<label class="mandatory" id="lblPhoneMand">*</label>
															</td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>								
											<tr id="trHomeOfficeNo">
												<td valign="top" class="fntSmall"><label id="lblOfficeOHomeNo"></label></td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtOCountry" id="txtOCountry"/></td>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtOArea" id="txtOArea" class="areaCode"/></td>
															<td align="left"><input type="text"  maxlength="10" style="width: 100px;" name="txtOfficeTel" id="txtOfficeTel"/></td>
															<td align="left"><label class="mandatory" id="lblHomeOfficeMand">*</label></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>		
											<tr id="trFaxNo">
												<td valign="top" class="fntSmall"><label id="lblFaxNo"></label></td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" id="txtFCountry"/></td>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" id="txtFArea" class="areaCode"/></td>
															<td align="left"><input type="text" maxlength="10" style="width: 100px;" id="txFax"/></td>
															<td align="left"></td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>										
										</table>
									</td>
									<td class="pnlBottom"></td>
									</tr>
									<tr>
										<td  colspan='4' class="rowGap">
										</td>
									</tr>
								</table>
						</td>
						</tr>
						<tr id="trEmgnContactInfo">
							<td  colspan='4'>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
								<tr>
									<td class='pnlTop' style="width:10px"></td>
									<td class='pnlTop pnlWidth'></td>
									<td class='pnlTopR'></td>
								</tr>
								<tr>
								<td class="pnlBottom"></td>
								<td class="pnlBottom">
									<table width="100%" border="0" cellpadding="2" cellspacing="0" align='center'>
										<tbody>
											<tr>
												<td colspan="4">
													<label class="fntBold hdFontColor" id="lblEmergencyContactInfo"></label>
												</td>
											</tr>
											<tr id="trEmgnTitle">
												<td width="25%">
													<label id="lblTitle"></label>
												</td>
												<td width="1%">
													<label>:</label>
												</td>
												<td width="33%">
													<select  style="width: 50px;" size="1" name="customer.emgnTitle" id="selEmgnTitle">
													</select>
													<label class="mandatory" id="lblEmgnTitleMand"> *</label>
												</td>
												<td width="44%"><label> </label></td>
											</tr>
											<tr id="trEmgnFirstName">
												<td>
													<label id="lblFirstName"> </label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2">
													<input type="text" class="fontCapitalize" maxlength="50" size="50" name="customer.emgnFirstName" id="txtEmgnFirstName"/> 
													<label class="mandatory" id="lblEmgnFirstNameMand">*</label>
												</td>
											</tr>
											<tr id="trEmgnLastName">
												<td>
													<label id="lblLastName"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2">
													<input type="text"  class="fontCapitalize" maxlength="50" size="50" name="customer.emgnLastName" id="txtEmgnLastName"/> 
													<label class="mandatory" id="lblEmgnLastNameMand">*</label>
												</td>
											</tr>
											<tr id="emgnPhone1">
												<td valign="top" class="fntSmall"></td>
												<td></td>
												<td>
													<table cellspacing="0" cellpadding="2" border="0">
														<tbody>
														  <tr>
															<td align="left"><label class="fntSmall " id="lblCountryCode"></label></td>
															<td align="left"><label class="fntSmall areaCode" id="lblAreaCode"></label></td>
															<td align="left"><label class="fntSmall " id="lblNumber"></label></td>
															<td align="left"><label class="fntSmall "></label> </td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="emgnPhone2">
												<td valign="top" class="fntSmall">
													<label id="lblTelNo"></label>
												</td>
												<td><label>:</label></td>
												<td>
													<table cellspacing="0" cellpadding="1" border="0">
														<tbody>
														  <tr>																			
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPCountry" id="txtEmgnPCountry"/></td>
															<td align="left"><input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPArea" id="txtEmgnPArea" class="areaCode"/></td>
															<td align="left"><input type="text"  maxlength="10" style="width: 100px;" name="txtEmgnTelephone" id="txtEmgnTelephone"/></td>
															<td align="left">
																<label class="mandatory" id="lblEmgnPhoneMand">*</label>
															</td>
														  </tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="trEmgnEmail">
												<td>
													<label id="lblEmail"></label>
												</td>
												<td>
													<label>:</label>
												</td>
												<td colspan="2">
													<input type="text" maxlength="100" size="25" name="customer.emgnEmail" id="txtEmgnEmail"/> 
													<label class="mandatory" id="lblEmgnEmailMand">*</label>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td class="pnlBottom"></td>
								</tr>
								<tr>
									<td  colspan='4' class="rowGap">
									</td>
								</tr>
							</table>
						</td>
						</tr>
						<tr id="tblPreference">
							<td colspan='4' valign='top'>
								<table cellspacing="0" cellpadding="2" border="0" align="center" width="99%" >
									<tbody>
									<tr>
											<td colspan="4" class="alignLeft">
												<label class="fntBold hdFontColor" id="lblYourPref"></label>
											</td>
										</tr>
										<tr id="trPromotionPref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%">
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="radio" name="questionaire[1].questionAnswer" id="chkQ1" value="N" class="NoBorder"/>
															<input type="hidden" name="questionaire[1].questionKey" value="2" />
															<label id="lblUnsubcribe">  </label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="radio" name="questionaire[1].questionAnswer" id="chkQ1" value="Y" class="NoBorder"/>
															&nbsp;<label id="lblInfoPromotional"></label>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr id="tblEmailPref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%" >
													<tr>
														<td colspan="4" class="alignLeft">
															<label id="lblInfoEmailFormat"></label>
														</td>
													</tr>										
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="hidden" name="questionaire[2].questionKey" value="3" />
															<input type="radio" checked="" value="HTML" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
															<label id="lblHtml"></label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">											
															<input type="radio" value="PLAIN" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
															<label id="lblText"> </label>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr id="tblSmsPref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%">
													<tr>
														<td colspan="4" class="alignLeft">
															<label id="lblInfoSMS"></label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="hidden" name="questionaire[3].questionKey" value="4" />
															<input type="radio" value="Yes" name="questionaire[3].questionAnswer" id="radQ4"  class="NoBorder"/>
															<label id="lblInfoMobile"> </label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="radio" checked="" value="No" name="questionaire[3].questionAnswer" id="radQ4" class="NoBorder"/>
															<label id="lblNo"> </label>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr id="tblLanguagePref">
											<td colspan="4" class="alignLeft">
												<table cellspacing="0" cellpadding="2" border="0" width="100%" >
													<tr>
														<td colspan="4">
															<label id="lblInfoLan"></label>
															<input type="hidden" name="questionaire[4].questionKey" value="5" />
														</td>
													</tr>	
													<tr id="templateLanguages">
														<td colspan="4" class="alignLeft">																		
															<input type="radio" tabindex="21" value="" name="questionaire[4].questionAnswer" id="radQ5" class="NoBorder"/>
																<label id="lblLanName">  </label>
														</td>
													</tr>
													<tr>
														<td class="Dots" colspan="4"><label > </label></td>
													</tr>
													<tr>
														<td colspan="4">
															<label id="lblInfoContactLan"> </label>
														</td>
													</tr>
													<tr>
														<td colspan="4" class="alignLeft">
															<input type="hidden" name="questionaire[5].questionKey" value="6" />
															<input type="text" size="50" name="questionaire[5].questionAnswer" maxlength="50" value="" id="txtQ2"/>
														</td>
													</tr>
													<tr><td class="Dots" colspan="4"><label > </label></td></tr>
												</table>
											</td>
										</tr>										
										<tr>
											<td colspan="4" class="alignLeft">
													<label id="lblInfoAgree1" hidden> </label>
													<br/><br/>	
													<label id="lblInfoAgree2"> </label>	
													<br/><br/>
													<div class="divfont" id="lblInfoAgree3"> </div>	
											</td>
										</tr>
										</table>
									</td>
								</tr>
								<tr id="aggrementInfo">
									<td colspan="4">	
										<table border="0" cellpadding="2" cellspacing="0" width="100%" >
										<tr>
											<td class="rowGap" colspan="4">
											</td>
										</tr>
										<tr>
											<td class="rowGap" colspan="4">
											</td>
										</tr>												
									</tbody></table>
								</td>
								</tr>
								<tr id="trButtons">
									<td colspan="4">
										<table cellspacing="0" cellpadding="0" border="0" align="center" width="99%" >
											<tr>
												<td align="center" colspan="4">
													<input type="button" title="Cancel"  class="Button" name="btnCancel" id="btnCancel"/>
													<input type="button" title="Click here to Update the Profile"  class="Button" name="btnUpdate" id="btnUpdate"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
						</table>					
				<input type="hidden" name="customer.version" id="customer.version"/>				
				<input type="hidden" name="customer.telephone" id="hdnTelNo"/>
				<input type="hidden" name="customer.mobile" id="hdnMobile"/>
				<input type="hidden" name="customer.officeTelephone" id="hdnOffice"/>
				<input type="hidden" name="customer.fax" id="hdnFax"/>			
				<input type="hidden" name="customer.emgnPhoneNo" id="hdnEmgnPhoneNo"/>		
			</form>
			</div>
		</td>
	</tr>
	<tr id="trMsg">
		<td>
			<table width='99%' border='0' cellpadding='0' cellspacing='0' align='center'>
				<tr>
					<td>
						<table width='100%' border='0' cellpadding='2' cellspacing='0'>
							<tr>
								<td valign='top'>
									<br>
									<br>
								</td>
							</tr>
							<tr>
								<td valign='top'>
									<table width='100%' border='0' cellpadding='2' cellspacing='0'>
										<tr>
											<td><img src="../images/n058_no_cache.gif" id="imgConfirm"></td>
											<td align="left">&nbsp;<label class="fntBold" id="lblMsgUpdateSucess"></label></td>				
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
								</td>
							</tr>							 
						</table>					 										
						</td>
		 			 </tr>	       			
		    </table>
		</td>
	</tr>
</table>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/customer/customerProfileUpdate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/v2/customer/customerValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</c:if>

