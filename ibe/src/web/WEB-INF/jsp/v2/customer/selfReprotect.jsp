<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<!--  <html xmlns="http://www.w3.org/1999/xhtml"> -->
<html>
	<head>
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="-1"/>	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><fmt:message key="msg.Page.Title"/></title>		
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
		<link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<link rel="stylesheet" type="text/css" href="../css/commonSocial_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<link rel='shortcut icon' href="../images/AA.ico?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	
	<script type="text/javascript">
		var fromSocialSites = '<c:out value="${sessionScope.fromSocialSites}" escapeXml="false" />'; 
		var strReqParam = '<c:out value="${requestScope.sysReqParamAA}" escapeXml="false" />';
		var pnr = '<c:out value="${requestScope.pnr}" escapeXml="false" />';
		var alertId = '<c:out value="${requestScope.alertId}" escapeXml="false" />';
		var pnrSegId = '<c:out value="${requestScope.pnrSegId}" escapeXml="false" />';
	</script>
	
	<!-- common javascript libs  -->
	<script src="../js/globalConfig.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
<%-- 	<script src="../js/v2/common/jquery.tooltip.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	 --%>
	<script src="../js/common/topHolder.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/JQuery.ibe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jquery.readonly.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
	<script src="../js/v2/common/jquery.alerts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	</head>
	<!--  -->
	<body>
		<form id="loadReservationForm" method="post" action="showCustomerLoadPage!loadCustomerHomePage.action">
			<input type="hidden" id="groupPNR" name="groupPNR" autocomplete="off"/>
			<input type="hidden" id="airlineCode" name="airlineCode" autocomplete="off"/>
			<input type="hidden" id="marketingAirlineCode" name="marketingAirlineCode" autocomplete="off"/>
			<input type="hidden" id="pnr" name="pnr" autocomplete="off"/>
			<input type="hidden" id="isTransferPage" name="isTransferPage" autocomplete="off"/>
			<input type="hidden" id="depDate" name="depDate" autocomplete="off"/>
		</form>		
		<c:import url="../common/pageLoading.jsp" />
		<div id="loadingMessage"></div>	
		<div id="divLoadBg">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgLogin">
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<!-- Top Banner -->
							<c:import url="../../../../ext_html/header.jsp" />
							<!-- Content holder -->
							
							<tr>
								<td style="height: 30px;">
								</td>
							</tr>
							
<!-- 							<tr> -->
<!-- 								<td id="reservationDetailsGridContainer"> -->
								
<!-- 								</td> -->
<!-- 							</tr> -->
							
							<tr id="trResDetailList">
								<td>
									<div id="reservationDetails">
										<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">										
												<tr>		
													<td align="center" width="14%" class="GridHeader">
														<label class="fntBold fntWhite" id="lblReservationNumber"> Reservation Number</label>
													</td>		
													<td align="center" width="24%" class="GridHeader">
														<label class="fntBold fntWhite" id="paxName"> Passenger Name</label>
													</td>		
													<td align="center" width="12%" class="GridHeader">
														<label class="fntBold fntWhite" id="from">From</label>
													</td>		
													<td align="center" width="12%" class="GridHeader">
														<label class="fntBold fntWhite" id="to">To</label>
													</td>		
													<td align="center" width="14%" class="GridHeader">
														<label class="fntBold fntWhite" id="departureDate">Departure</label>
													</td>
													<td align="center" width="14%" class="GridHeader" >
														<label class="fntBold fntWhite" id="arrivalDate">Arrival</label>
													</td>
													<td align="center" width="10%" class="GridHeader">
														<label class="fntBold fntWhite" id="status">Status</label>
													</td>	
												</tr>										
										  		<tbody id="resDetailTable">										
										 		</tbody>
										</table>
									</div>
								</td>
							</tr>
	
							<tr>
								<td style="height: 30px;">
								</td>
							</tr>
							
							<tr>
								<td>
									<div>
										<font>Following are the available flights for reprotection/ Transfer, please select one and confirm.</font>
									</div>
								</td>
							</tr>
							
							<tr>
								<td style="height: 30px;">
								</td>
							</tr>
							
							<tr>
								<td>
									<div id="flights">
									</div>
								</td>
															
							</tr>
							
							<tr>
								<td style="height: 30px;">
								</td>
							</tr>
														
							<tr>
								<td>
									<div>
										<input type="button" id="confirmTransfer" value="Confirm Transfer" class='Button ButtonLarge'/>							
									</div>
								</td>
								
								<td>
									
								</td>
								
								<td>
									<div>
										<input type="button" id="modifyBooking" value="Modify Booking" class='Button ButtonLarge'/>							
									</div>
								</td>
							</tr>
							
							<tr>
								<td style="height: 30px;">
								</td>
							</tr>
							
							<tr>
								<td class='appLogo' colspan="2"></td>
							</tr>
							<c:import url="../../../../ext_html/cfooter.jsp" />							
						</table>
					</td>
				</tr>
		   </table>
		</div>
		<script src="../js/v2/customer/selfReprotection.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</body>
</html>
