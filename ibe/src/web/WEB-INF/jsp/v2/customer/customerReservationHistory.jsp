<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='/WEB-INF/jsp/common/topHolder.jsp' %>
<form id="frmResHistory">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td class='tblBG'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
					<tr>
						<td class="rowHeight">
						</td>
					</tr>
					<tr>
						<td valign="top" class="alignLeft paddingCalss">
							<label class="fntBold  hdFont hdFontColor" id="lblYourReservations">Your Reservations</label>
						</td>
					</tr>
					<tr>
						<td class="setHeight">
						</td>
					</tr>
					<tr>
						<td class="alignLeft paddingCalss">
							<label id="lblSubHeadHisResNo"> No Reservations </label>
							<label id="lblSubHeadHisRes">Click the Reservation Number to view details of your bookings</label>												
						</td>
					</tr>
					<tr id="trResHisList">
						<td>
							<div id="spnReservations">
								<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">									
										<tr>		
											<td align="center" width="14%" class="GridHeader">
												<label class="fntBold fntWhite" id="lblReservationNumber">Reservation Number</label>
											</td>		
											<td align="center" class="GridHeader">
												<label class="fntBold fntWhite" id="lblTrip">Trip</label>
											</td>		
											<td align="center" width="23%" class="GridHeader">
												<label class="fntBold fntWhite" id="lblDeparture">Departure</label>
											</td>		
											<td align="center" width="23%" class="GridHeader">
												<label class="fntBold fntWhite" id="lblArrival">Arrival</label>
											</td>		
											<td align="center" width="10%" class="GridHeader" colspan="2">
												<label class="fntBold fntWhite" id="lblStatus">Status</label>
											</td>	
										</tr>
								 <tbody id="resTableHistory">							
							</table>
						</div>
						   </td>									
					</tr>
				</table>
		</td>
	</tr>
  </table>
</form>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/customer/customerReservationHistoryV2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</c:if>
