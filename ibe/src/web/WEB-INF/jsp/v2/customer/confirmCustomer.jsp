<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">		
		<title><fmt:message key="msg.Page.Title"/></title>
		<link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css"/>
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
		
		<script type="text/javascript">
			function loadSignIn() {
				var formHtml =  "<form id='dynamicForm' method='post'> <input type='hidden' name='hdnParamData' value="+ SYS_IBECommonParam.locale +"^SI></form>";
				$("body").append(formHtml);
				$("#dynamicForm").attr("action", SYS_IBECommonParam.securePath + "showReservation.action");
				$("#dynamicForm").attr("target", "_top");	
				$("#dynamicForm").submit();	
			}
		</script>
					
	</head>
	<body>		
		<div id="divLoadBg" style="display: block;">
			<a href="#"  id="linkFocus"> </a>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgRegisterConfirm">
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<!-- Top Banner -->
							<c:import url="../../../../ext_html/header.jsp" />																	
							 <%-- Success Message --%>
							 <tr id="trMsg">
							 	<td>							 	
									<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
										<tr>
											<td class="pageBody tapMainbody">
												<table width='100%' border='0' cellpadding='0' cellspacing='0' class="innderTable">
													<tr>
														<td valign="top">
															<%@ include file='../../common/includeFrameTop.jsp' %>						
																<table width='100%' border='0' cellpadding='2' cellspacing='0'>
																	<tr>
																		<td width="30"><img id="imgConfirm" src='../images/n058_no_cache.gif'/></td>
																		<td align="left">&nbsp;<font class="fntBold <fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.register.success"/><br/></font></td>				
																	</tr>
																	<tr><td></td> </tr>	
																	<tr><td></td> </tr>	
																	<tr>
																		<td colspan="2" align="center">
																			<input type="button"  value="Sign In" class="Button" name="btnSignIN" id="btnSignIN" onclick="loadSignIn()"/>		
																		</td>
																	</tr>																	
																	<tr><td></td> </tr>	
																	<tr><td></td> </tr>																	
																																	
																</table>
															<%@ include file='../common/includeFrameBottom.html'%>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>									
							 </tr>
							 <tr>
								<td class='appLogo' colspan="2"></td>
							</tr>
							<c:import url="../../../../ext_html/cfooter.jsp" />									
						</table>
					</td>
				</tr>
			</table>
		</div>	
		<%@ include file='../common/iBECommonParam.jsp'%>	
	</body>
</html>