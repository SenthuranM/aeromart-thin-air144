<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<form id="frmResList">
<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
 <tr>
	<td class="tblBG">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
				<tbody>
				  <tr>
					<td class="rowGap">
						</td>
					</tr>
					<tr>
					<td class="alignLeft paddingCalss">
						<label class="fntBold hdFont hdFontColor" id="lblReservations"></label>
						</td>
					</tr>
					<tr>
					<td class="rowGap">
						</td>
					</tr>
					<tr>
						<td class="alignLeft paddingCalss">
							<label id="lblMsgResNo">  </label>
							<label id="lblMsgClickRes"> </label>												
						</td>
					</tr>
					<tr id="trResList">
						<td>
							<div id="spnReservations">
								<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">										
										<tr>		
											<td align="center" width="14%" class="GridHeader">
												<label class="fntBold fntWhite" id="lblReservationNumber"></label>
											</td>		
											<td align="center" class="GridHeader">
												<label class="fntBold fntWhite" id="lblTrip"></label>
											</td>		
											<td align="center" width="23%" class="GridHeader">
												<label class="fntBold fntWhite" id="lblDeparture"></label>
											</td>		
											<td align="center" width="23%" class="GridHeader">
												<label class="fntBold fntWhite" id="lblArrival"></label>
											</td>		
											<td align="center" width="10%" class="GridHeader" colspan="2">
												<label class="fntBold fntWhite" id="lblStatus"></label>
											</td>	
										</tr>										
								  		<tbody id="resTable">										
								 </tbody>
								</table>
							</div>
						</td>
					</tr>
		     </tbody>
		</table>
	  </td>
	</tr>
</table>
</form>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/customer/reservationList.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</c:if>
