<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='/WEB-INF/jsp/common/topHolder.jsp' %>

<form id="frmResCredit" method="post">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td valign="top" style="height: 200px;" class="tblBG" >
		<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
			<tbody>
				<tr>
					<td class="rowGap">
						</td>
				</tr>
				<tr>
					<td>
					<table width="100%">
						<tbody>
							<tr>
								<td align="center" width="90%">
									<input type="button"  class="Button btnCredit btnCDisable" id="btnCustomerCredit" disabled="" /> 
									<input type="button"  class="Button ButtonCreditLarge"    id="btnMashCre" />
									<input  type="button" class="Button ButtonCreditLarge"   id="btnMashCreUse" />
							   </td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
				<tr>
					<td>
					    <div id="CustomerCreditHD" style="display: none;">
							<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
								<tbody>
									<tr>
										<td class="alignLeft paddingCalss">
										<label class="fntBold  hdFont hdFontColor" id="lblAirlineCredit">
											
										</label>												
										</td>
									</tr>
									<tr>
										<td class="rowGap">
										</td>
									</tr>
									<tr>
										<td>
										<table cellspacing="0" cellpadding="0" border="0"
											width="100%">
											<tbody>
												<tr>
													<td class="alignLeft paddingCalss">
														<label id="lblCrditAmtMsg"></label> 
														<label id="lblCreditAmt" class="fntBold"></label>
														
													</td>
												</tr>
												<tr>
													<td class="alignLeft paddingCalss">
														<div id="lblSubHead1"> 
															<label id="lblNoRes" style="display: none;">														 		
															</label>
														</div>
													 	<div id="lblSubHead2">
													 		<label id="lblClickRes"></label>
													 	</div>
														
													</td>
												</tr>
												<tr>
													<td class="alignLeft paddingCalss">
														<div id="divRedeem" class="divfont">
															
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="loyaltyCreditHD" style="display: none;">
							<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">								
								<tr>
									<td class="alignLeft paddingCalss">
										<label class="fntBold  hdFont hdFontColor" id="lblInfoMashreqCredit">										
										</label>										
									</td>
									</tr>										
									<tr id="tdSubHeadLoyaltyCreNo" style="display: none;">
										<td class="alignLeft paddingCalss">
											<label id="lblSubHeadLoyaltyCreNo"></label>
										</td>
									</tr>
									<tr>
										<td class="rowGap">
										</td>
								</tr>			
							</table>
						</div>
						<div id="creditUsageHD" style="display: none;">
							<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
								<tr>
									<td class="alignLeft paddingCalss">
										<label class="fntBold  hdFont hdFontColor" id="lblCreditUsage"></label>
									</td>
								</tr>
								<tr id="trSubHeadCreditUsage" style="display: none;">
									<td class="alignLeft paddingCalss">
										<label id="lblNoRes">										
										</label>
									</td>
								</tr>
								<tr>
									<td class="rowGap">
									</td>						
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="setHeight"></td>
				</tr>
				<tr>
					<td>
						<div id="CustomerCreditTable" style="display: none;">
							<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
								<tbody>
									<tr>
										<td>
											<div id="spnCredits">
											  <table cellspacing="1" cellpadding="0" border="0" width="100%" class="GridTable">	
												<tbody>
													<tr>		
														<td align="center" width="14%" title="Click here to get details of Reservation" class="GridHeader">
															<label class="fntBold fntWhite" id="lblResNo"> </label>
														</td>		
														<td align="center" width="50%" class="GridHeader">
															<label class="fntBold  fntWhite" id="lblPassenger"></label>
														</td>		
														<td align="center" width="21%" class="GridHeader">
															<label class="fntBold f fntWhite" id="creditAmount"></label>
														</td>		
														<td align="center" width="15%" class="GridHeader">
															<label class="fntBold fntWhite" id="lblBeforeUse"></label>
														</td>	
													</tr>	
													<tr id="creditDetailsTemplate">	
														<td align="center" valign="top" class="GridItems">													
															<u><label class="cursorPointer" id="pnr"></label></u>												
														</td>	
														<td class="GridItems paddingL5">
															<label id="fullName"> </label>
														</td>	
														<td align="right" class="GridItems paddingR5">
															<label id="balance"> </label> <label name="currencyCode"> </label>
														</td>	
														<td align="center" class="GridItems">
															<label id="dateOfExpiry">  </label>
														</td>
													</tr>	
												</tbody>
											</table>
										</div>
									</td>
									</tr>
									<tr>
										<td class="alignLeft">
											<span id="spnCreditsMsg" /> 
													
											</span>
										</td>
									</tr>
								</tbody>
							</table>
					   </div>
					   <div id="loyaltyCreditTable" style="display: none;">
							<table cellspacing="1" cellpadding="0" border="0" width="60%" class="GridTable">	
								<tr>		
									<td align="center" width="40%" class="GridHeader">
										<label  class="fntBold fntWhite" id="lblAvailableCredit"></label>
									</td>		
									<td align="center" width="30%" class="GridHeader">
										<label id="lblDateOfEarn" class="fntBold fntWhite"></label>
									</td>		
									<td align="center" width="30%" class="GridHeader">
										<label id="lblDateOfExpiry"  class="fntBold fntWhite"></label>
									</td>	
								</tr>	
								<tr id="loyaltyCreditTemplate">	
									<td align="right" class="GridItems paddingR5">
										<label id="credit">  </label> <label name="currencyCode"> </label>
									</td>	
									<td align="right" class="GridItems paddingR5">
										<label id="dateOfEarn">  </label>
									</td>	
									<td align="right" class="GridItems">
										<label id="dateOfExpiry">  </label>
									</td>
								</tr>
							</table>
						</div>
						<div id="creditUsageTable" style="display: none;">
							<table cellspacing="1" cellpadding="0" border="0" width="60%" class="GridTable">	
								<tr>		
									<td align="center" width="14%" title="Click here to get details of Reservation" class="GridHeader">
										<label class="fntBold  fntWhite" id="lblResNo"> </label>
									</td>		
									<td align="center" width="30%" class="GridHeader">
										<label class="fntBold  fntWhite" id="lblAmount"></label>
									</td>		
									<td align="center" width="30%" class="GridHeader">
										<label class="fntBold  fntWhite" id="lblPaymentDate"></label>
									</td>	
								</tr>	
								<tr id="creditUsageTemplate">	
									<td align="center" valign="top" class="GridItems">										
										<u><label class="cursorPointer" id="pnr"></label></u>											
									</td>	
									<td align="right" class="GridItems paddingR5">
										<label id="paidAmount">  </label>  <label name="currencyCode"> </label>
									</td>	
									<td align="right" class="GridItems">
										<label id="paidDate"> </label>
									</td>
								</tr>	
							</table>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
</form>
<c:if test='${applicationScope.isDevModeOn == "false"}'>
	<script src="../js/v2/customer/customerReservationCredit.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</c:if>