<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<link rel="stylesheet" href="../css/table_layout_EN.css" type="text/css" /> 	
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
<script src="../js/v2/isalibs/isa.jquery.airutil.js" type="text/javascript"></script>	
<form action="" id="frmCustReservation" method="post" class="">
	<div id="reservationPage" style="display: none;">
		<a href="#"  id="linkFocus"> </a>
		<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
		<tr>
			<td class='pnlTop pnlWidth'></td>
			<td class='pnlTopR'></td>
		</tr>
			<tr>
		    	<td class='tblBG' colspan="2">
					<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">				
					  <tr>
							<td class="rowSingleGap">
							</td>
					  </tr>
					  <tr>
							<td class="alignLeft paddingCalss">
								<label id="lblSummary" class="fntBold  hdFont hdFontColor"></label>
							</td>
					  </tr>
				      <tr>
					      <td valign="top" colspan="2" id="tdSuccessMsg" style="display: none;">
					          <table cellspacing="0" cellpadding="2" border="0" width="100%">
							  	<tr>
								   <td width="38">
											<img src="../images/n058_no_cache.gif" id="imgConfirm">
								   </td>
								   <td class="alignLeft paddingCalss">
									  <label class="fntBold" id="lblMsgSuccess"></label>
								   </td>
							    </tr>						    
						     </table>
					      </td>
			         </tr>      
		      		   <tr id="anciButtonHdPannel">
					      <td class="alignLeft paddingCalss">
								<span style="display: block;" id="spnPernMsg">									
									<label id="lblMsgAncilary" class="fntBold hdFontColor">	</label>
									<label id="lblMsgOnhold" class="fntBold hdFont hdFontColor">	Pay for your booking</label>									
								</span>
								<br/>
					      </td>
				       </tr>
				        <tr id="onHoldInfoPannel">
					      <td class="alignLeft paddingCalss">
								<span style="display: block;padding-top: 5px;padding-bottom: 5px;">
									<label id="lblMsgOnholdInfo1" class="fntBold hdFontColor">Your booking is onhold, please press the "Make payment" button to finish the payment before it expires.	</label>
									<br/>
									<label id="lblMsgOnholdInfo2" class="fntBold hdFontColor">Your booking is not confirmed until you do a successful payment and receive a confirmation.</label>									
								</span>								
					      </td>
				       </tr>
				       <tr id="forcedCFMInfoPannel">
					      <td class="alignLeft paddingCalss">
								<span style="display: block;padding-top: 5px;padding-bottom: 5px;">
									<label id="lblMsgForcedCFMInfo1" class="fntBold hdFontColor">Your booking is Forced-Confirm, please press the "Make payment" button to finish the payment.	</label>									
								</span>															
					      </td>
				       </tr>
				       
				       <tr>
					          <td class="rowHeight">
					          </td>
				       </tr>
				       <tr>
				       		<td class="alignLeft paddingCalss">
								 <table border='0' align='center' id="anciButtonPannel" style="display: none;padding-bottom: 5px;">
									 <tr>
										    <td align='left' class="anciButtons">
											    <input type='button' id='btnSeatUpdate'  class='btnAuxButton' value='Select Your Seat' disabled="disabled">
											    <input type='button' id='btnInsUpdate'  class='btnAuxButton' value='Travel Insurance' disabled="disabled">
											    <input type='button' id='btnmealUpdate'  class='btnAuxButton' value='Select Your Meal' disabled="disabled">
											    <input type='button' id='btnBaggageUpdate'  class='btnAuxButton' value='Select Your Baggage' disabled="disabled">
											    <input type='button' id='btnHalaUpdate'  class='btnAuxButton' value='Select Your Airport Service' disabled="disabled">
											    <input type='button' id='btnAPTUpdate'  class='btnAuxButton' value='Select Your Airport Transfer' disabled="disabled">
											    <input type='button' id='btnAddSrv4'   class='btnAuxButton' value="Select Your SSR" disabled="disabled">
											    <input type='button' id='btnMakePayment'   class='btnAuxButton' value="Make Payment" disabled="disabled">
										   </td>																				 
									 </tr>
									 <tr>
									 </tr>
								 </table>
							</td>
						</tr>
						<tr>
							<td class="rowHeight">
							</td>
						</tr>								       
			     </table>
			</td>
		</tr>
		<tr>
		<td class='rowGapNone'></td>
	</tr>
	<tr>
		<td class='pnlTop pnlWidth'></td>
		<td class='pnlTopR'></td>
	</tr>
	<tr id="trDataPanel">
		<td colspan="2" class='pnlBottomTall'>
		<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
				<tr>
					<td valign='bottom' class="alignLeft paddingCalss" width='55%'>
						<label id="lblPnr" class="fntBold hdFontColor"></label> 
						<label class="fntBold hdFontColor padding3"> : </label> 
						<label id="lblPnrNo" class="fntBold"></label>
					</td>
					<td valign='bottom' width='45%' class="alignLeft paddingCalss" > 
						<div id="policyPannel"> 
							<label id="lblPolicy" class="fntBold hdFontColor"></label> 
							<label class="fntBold hdFontColor padding3"> : </label>
							<label id="policyCode" class="fntBold"></label> 
						</div>
					</td>
				</tr>
				<tr>
					<td valign='bottom' width='55%' class="alignLeft paddingCalss">
						<label id="lblStatus" class="fntBold hdFontColor"></label> 
						<label class="fntBold hdFontColor padding3"> : </label>
						<label id="lblStrStatus" class="fntBold">&nbsp;</label>
						<label id="lblStrZuluReleaseDate" class="fntBold"></label>
					</td>
					<td width='45%' class="alignLeft paddingCalss" id="tdInsTypeDisplay"  style="display: none;">
						<label id="lblInsuranceType"  class="fntBold hdFontColor"></label>  
						<label class="fntBold hdFontColor padding3"> : </label>
						<label id="lblInsuranceTypeValue" class="fntBold"></label>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="alignLeft paddingCalss">
						<div class="spMsg" id="divRefundMessage" style="display: none;">
							<label id="lblRefundMessage"></label>
					</div>
					</td>
				</tr>
				<tr>
					<td valign='bottom' width='55%' class="alignLeft paddingCalss">
						<label id="lblBkgDate"  class="fntBold hdFontColor"></label>  
						<label class="fntBold hdFontColor padding3"> : </label>
						<label id="lblStrBkgDate" class="fntBold date-disp"></label>
						<label id="lblStrBkgDateHid" class="date-hid" style="display:none"></label>
					</td>
					<td width='45%' class="alignLeft paddingCalss">						
					</td>
				</tr>
				<tr>
					<td colspan='2'>											
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>										
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">														
										<tr>
											<td valign='top'>													
												<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
													<tr>
														<td colspan="10" class="alignLeft Gridwt"><label id="lblDeparting"  class='fntBold hdFontColor'></label></td>
													</tr>	
													<tr>
														<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDisplayStatus" class='gridHDFont fntBold'></label></td>
														<td align="center" width="5%" rowspan="2" class="gridHD" id="tdDepModifyImage" style="display: none;" id="tdHdDepatureResCheck">		
															<img title="Select for changes" src="../images/AA174_no_cache.gif" name="imgHD1" id="imgHD1">		
														</td>
													</tr> 
													<tr>            
														<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
													</tr>
													<tr id="departueFlight">
														<td width='30%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
														<td width='15%' class='rowColor' align='center'><label id="departureDate" class="date-disp"></label><label id="departureDateValue" class="date-hid" style="display:none"></label></td>
														<td width='8%' class='rowColor' align='center'><label id="departureTime"></label></td>
														<td width='15%' class='rowColor' align='center'><label id="arrivalDate" class="date-disp"></label><label id="arrivalDateValue" class="date-hid" style="display:none"></label></td>
														<td width='8%' class='rowColor' align='center'><label id="arrivalTime"></label></td>
														<td width='10%' class='rowColor' align='center'><label id="flightNo"></label></td>
														<td width='9%' class='rowColor'  align='center'><label id="duration"></label></td>														
														<td width='5%' class='rowColor' align='center' id="displayStatus"><label id="displayStatus" onmouseout="hideToolTip()" onmouseover="displayToolTip(this.id);"></label></td>
														<td align="center" class="GridItems" id="tdDepatureResCheck" style="display: none;">
															<input type="checkbox"  value="" class="noBorder" name="chkSegment0" disabled="disabled"  id="flightSegmentRefNumber">
															<img title="Cancelled" src="../images/AA163_no_cache.gif">
													    </td>
													</tr>               
												</table>																																
											</td>
										</tr>
										<tr>
											<td colspan='1' class='rowGap'></td>
										</tr>									
									</table>
								</td>
							</tr>				
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
										<tr id="trRetrunGrid">
											<td valign='top'>
												<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">
													<tr>
														<td colspan="8" class="alignLeft Gridwt"> <label id="lblReturning"  class='fntBold hdFontColor'></label></td>
													</tr>
													<tr>
														<td rowspan="2" align='center' class='gridHD'><label id="lblRSegment" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align='center' class='gridHD'><label id="lblRDeparture" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align='center' class='gridHD'><label id="lblRArrival" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblRFlightNo" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDisplayStatus" class='gridHDFont fntBold'></label></td>
														<td align="center" width="5%" rowspan="2" class="gridHD"  id="tdHdReturnResCheck" style="display: none;">		
															<img title="Select for changes" src="../images/AA174_no_cache.gif" name="imgHD1" id="imgHD1">		
														</td>
													</tr>
													<tr>           
														<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>
													</tr> 
													<tr id="returnFlight">
														<td width='30%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
														<td width='15%' class='defaultRowGap rowColor' align='center'><label id="departureDate"></label><label id="departureDateValue" class="date-hid" style="display:none"></label></td>
														<td width='8%' class='defaultRowGap rowColor' align='center'><label id="departureTime"></label></td>
														<td width='15%' class='defaultRowGap rowColor' align='center'><label id="arrivalDate"></label><label id="arrivalDateValue" class="date-hid" style="display:none"></label></td>
														<td width='8%' class='defaultRowGap rowColor' align='center'><label id="arrivalTime"></label></td>
														<td width='10%' class='defaultRowGap rowColor' align='center'><label id="flightNo"></label></td>
														<td width='9%' class='rowColor'  align='center'><label id="duration"></label></td>
														<td width='5%' class='rowColor' align='center' id="displayStatus"><label id="displayStatus"  onmouseout="hideToolTip()" onmouseover="displayToolTip(this.id);"></label>
													    </td>
														<td align="center" class="GridItems" id="tdReturnResCheck" style="display: none;">
															<input type="checkbox"  value="" class="noBorder" name="chkSegment0" disabled="disabled" id="flightSegmentRefNumber">
															<img title="Cancelled" src="../images/AA163_no_cache.gif">
													    </td>
													    
													</tr>                
												</table>
											</td>
										</tr>
										<tr>
											<td colspan='1' class='rowGap'></td>
										</tr>	
										<tr id="departureButtonsPannel">
											<td align="center">
												<input type="button" class="Button ButtonLarge ui-state-default ui-corner-all" title="Click here to cancel the reservation" id="btnCancelRes"> 
												<input type="button"  class="Button ButtonLargeDisable ui-state-default ui-corner-all" title="Click here to cancel a segment" id="btnCancelSegment"> 
												<input type="button"  class="Button ButtonLargeDisable ui-state-default ui-corner-all" title="Click here to modify a segment" id="btnModify">	
												<input type="button"  class="Button ButtonLargeDisable ui-state-default ui-corner-all" title="Click here to add a bus segment" id="btnAddBusSegment">											
											</td>
										</tr>
								</table>
							</td>
						</tr>													
						<tr>
							<td class='rowGap'></td>
						</tr>														
						<tr>
							<td>
								<table width='100%' border='0' cellpadding='0' cellspacing='0'>											
									<tr>
										<td  valign='top' class="alignLeft">
											<table width="100%" border="0" cellspacing="0" cellpadding="1" class="GridTable">
												<tr>
													<td class='gridHD alignLeft paddingL5'><label id="lblPaxInfo" class='gridHDFont fntBold'></label></td>
												</tr>
												<tr id="tradultfGrid">
													<td class='noPadding'>
														<table border='0' cellspacing='1' cellpadding='1' width='100%'>
															<tr>
																<td class='gridHDDark'>&nbsp;</td>																
																<td class='gridHDDark alignLeft'  width='60%'><label id="lblAdultHD" class='gridHDFont fntBold'></label></td>
																<td class='gridHDDark alignLeft'  width='40%'><label id="lblServices" class='gridHDFont fntBold'></label></td>
															</tr>
															<tr id="adultGrid">
																<td class='defaultRowGap bdBottom rowColor'  width="3%"><label id="itnSeqNo"></label></td>
																<td class='defaultRowGap bdBottom rowColor' width="47%"><label id="itnPaxName"></label></td>
																<td  class='defaultRowGap rowColor bdBottom bdRight' width="50%"><span id="additionalInfo" class="fntDefault"></span></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="trChildGrid">
													<td class='noPadding'>
														<table border='0' cellspacing='1' cellpadding='1' width='100%'>
															<tr>																		
																<td class='lightGrayHD alignLeft' width='60%' colspan="2"><label id="lblChildHD" class='hdFontColor fntBold'></label></td>
																<td class='lightGrayHD alignLeft'  width='40%'><label id="lblServices" class='hdFontColor fntBold'></label></td>
															</tr>
															<tr id="childGrid">
																<td class='defaultRowGap bdBottom rowColor' width="3%"><label id="itnSeqNo"></label></td>
																<td class='defaultRowGap bdBottom rowColor' width="47%"><label id="itnPaxName"></label></td>
																<td  class='defaultRowGap rowColor bdBottom bdRight' width="50%"><span id="additionalInfo" class="fntDefault"></span></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="trInfGrid">
													<td class='noPadding'>
														<table border='0' cellspacing='1' cellpadding='1' width='100%'>
															<tr>																		
																<td class='lightGrayHD alignLeft'  width='60%' colspan="2"><label id="lblInfantHD" class='hdFontColor fntBold'></label></td>
																<td class='lightGrayHD alignLeft'  width='40%'><label id="lbltravelwith" class='hdFontColor fntBold'></label></td>
															</tr>
															<tr id="infantGrid">
																<td class='defaultRowGap bdBottom rowColor'  width="3%"><label id="itnSeqNo"></label></td>
																<td class='defaultRowGap bdBottom rowColor' width="47%"><label id="itnPaxName"></label></td>
																<td class='defaultRowGap rowColor bdBottom bdRight' width="50%"><label id="itnTravellingWith"></label></td>
															</tr>
														</table>
													</td>
												</tr>																
											</table>
											<br/>
											<table width="100%" border="0" cellspacing="1" cellpadding="1" style="display: none;" id="flexiDetails">
												<tr>
													<td class='noPadding'>
														<table border='0' cellspacing='1' cellpadding='1' width='100%' class="GridTable">
															<tr>
																<td class='gridHD alignLeft paddingL5' colspan="2"><label id="lblFlexiHeader" class='gridHDFont fntBold'>Reservation Flexibilities</label></td>
															</tr>
															<tr>
																<td class='lightGrayHD alignLeft'  width='30%'><label id="lblOriginDestination" class='hdFontColor fntBold'>Origin / Destination</label></td>
																<td class='lightGrayHD alignLeft'  width='70%'><label id="lblFlexiInfo" class='hdFontColor fntBold'>Flexibilities</label></td>
															</tr>
															<tr id="flexiInfo">
																<td class='defaultRowGap rowColor'><label id="originDestination"></label></td>
																<td class='rowColor'><label id="flexiDetails"></label></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
											<br/>
											<table width="100%" border="0" cellspacing="1" cellpadding="1" id="priceBreakDown">
												<tr>
													<td class='noPadding'>
														<table border='0' cellspacing='1' cellpadding='1' width='100%' id="payDetail" class="GridTable">	
															<tr>
																<td class='gridHD alignLeft paddingL5' colspan="2"><label id="lblPaymentInfo" class='gridHDFont fntBold'></label></td>
															</tr>															
															<tr id="paymentDetails">
																<td class='defaultRowGap alignLeft rowColor paddingL5'><label id="labelName" ></label></td>
																<td align='right' class='rowColor'><label id="value"></label></td>
															</tr>																					     			
														</table>
													</td>
												</tr>
											</table>
												
											<div id="cancelBalanceSummary">												
												<div id="divRedeemReservation" class="divfont">															
												</div>
											</div>									
										</td>
									</tr>
									<tr>
										<td colspan='2' class='rowGap'></td>
									</tr>
									<tr>
										<td colspan='2' class='rowGap'></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<div id="spnErrorRes" class="mandatory"> </div>
							</td>
						</tr>
						<tr>
							<td align='left' id="contactInfomation">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class='gridHD alignLeft paddingL5' colspan="6"><label id="lblCotactInfo" class='gridHDFont fntBold'></label></td>
									</tr>
									<tr>
										<td class='pnlBottomTall' valign='top' colspan="2">
											<div class="div-Table" id="tblContactDetails">
												<div class="table-Row  one-Col">
													<div class="table-Data one-Col">
														<label class="fntSmall">&nbsp;</label>
													</div>
												</div>
												<div class="table-Row one-Col" id="trTitle">
													<div class="table-Data left-TD">
														<label id="lblTitle"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selCTitleRes' size='1' tabindex="1" name='contactInfo.title'></select>
														<label id="lblTitleMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblFirstName">
													<div class="table-Data left-TD">
														<label id="lblFirstname"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtFNameRes' name="contactInfo.firstName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="2" />
														<label id="lblFnameMand" class='mandatory'>*</label> 
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblLastName">
													<div class="table-Data left-TD">
														<label id="lblLasttname"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtLNameRes' name="contactInfo.lastName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="3" />
														<label id="lblLNameMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblAddr1">
													<div class="table-Data left-TD">
														<label id="lblAddress"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtStreetRes' name='contactInfo.addresStreet' style="width:130px;" maxlength="100" tabindex="4"/>
														<label id="lblAddrMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblNationality">
													<div class="table-Data left-TD">
														<label id="lblNationality"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selNationalityRes' size='1' style='width:135px;' tabindex="18" name='contactInfo.nationality'></select>
														<label id="lblNationalityMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblAddr2">
													<div class="table-Data left-TD">
														<label>&nbsp;</label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtAddressRes' name='contactInfo.addresline' style="width:130px;" maxlength="100" tabindex="5"/>
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblCity">
													<div class="table-Data left-TD">
														<label id="lblcity"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtCityRes'  name="contactInfo.city" style='width:130px;'  maxlength='20'  tabindex="6" />
														<label id="lblCityMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblZipCode">
													<div class="table-Data left-TD">
														<label id="lblZipCode"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtZipCodeRes' name="contactInfo.zipCode" style='width:130px;' maxlength='10'  tabindex="8" />
														<label id="lblZipCodeMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblCountry">
													<div class="table-Data left-TD">
														<label id="lblCountry"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selCountryRes' size='1' style='width:135px;'  tabindex="9" name='contactInfo.country'>	
															<option value=""></option>						
														</select><label id="lblCountryMand" class='mandatory'>&nbsp;*</label>
														<input id='countryName' type="hidden" name='contactInfo.countryName' value="" />
													</div>
												</div>
												
												<div class="table-Row two-Col" id="tblPreferredLang">
													<div class="table-Data left-TD">
														<label id="lblPreferredLanguage"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selPrefLang' size='1' style='width:135px;'  tabindex="10" name='contactInfo.preferredLangauge'>	
															<option value=""></option>						
														</select><label id="lblPreferredLangMand" class='mandatory'>&nbsp;*</label>
													</div>
												</div>
												
												<div id="tblContactNo">
													<!-- Contact Details -->
													<div class="table-Row one-Col thin-row trPhoneNoHeader">
														<div class="table-Data one-Col">
															<label class="fntBold hdFontColor" id="lblContactDetails"></label>
														</div>
													</div>
													
													<div class="table-Row one-Col thin-row trPhoneNoLabel">
														<div class="table-Data left-TD">
															<label>&nbsp;</label>
														</div>
														<div class="table-Data right-TD">
															<label id="lblCountryCode" class='fntSmall'></label>
															<label id="lblAreaCode" class='fntSmall areaCode'></label>
															<label id="lblNumber" class='fntSmall'></label>
														</div>
													</div>
													
													<div class="table-Row one-Col" id="trMobileNo">
														<div class="table-Data left-TD">
															<label id="lblMobileNo"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtMCountryRes' name="contactInfo.mCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="11"/>
															<input type='text' id='txtMAreaRes' name="contactInfo.mArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="12" class="areaCode"/>
															<input type='text' id='txtMobileRes' name="contactInfo.mNumber" style='width:100px;text-align:right;' maxlength='10' tabindex="13" />
															<label id="lblMobileNoMand" class='mandatory'>*</label>
															<span id="atLeasetOne"><font class="mandatory" >*&nbsp;</font> <label class="fntSmall" id="lblAtleaseOneText">Please provide at least one phone Number</label></span>
														</div>
													</div>
													
													<div class="table-Row one-Col" id="trphoneNo">
														<div class="table-Data left-TD">
															<label id="lblPhoneNo"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtPCountryRes' name="contactInfo.lCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="14" />
															<input type='text' id='txtPAreaRes' name="contactInfo.lArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="15" class="areaCode"/>
															<input type='text' id='txtPhoneRes' name="contactInfo.lNumber" style='width:100px;text-align:right;' maxlength='10'  tabindex="16" />
															<label id="lblPhoneNoMand" class='mandatory'>*</label>
														</div>
													</div>
													
													<div class="table-Row one-Col" id="trFaxNo">
														<div class="table-Data left-TD">
															<label id="lblFaxNo"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtFCountryRes' name="contactInfo.fCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="17" />
															<input type='text' id='txtFAreaRes' name="contactInfo.fArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="18" class="areaCode"/>
															<input type='text' id='txtFaxRes' name="contactInfo.fNo" style='width:100px;text-align:right;' maxlength='10'  tabindex="19" />
															<label id="lblFaxMand" class='mandatory'>&nbsp;*</label>
														</div>
													</div>
												</div>
												<div id="tblEmail">
													<div class="table-Row one-Col" >
														<div class="table-Data one-Col">
															<label id="lblMsgValideEmail"></label>
														</div>
													</div>
													
													<div class="table-Row one-Col" >
														<div class="table-Data left-TD">
															<label id="lblEmailAddress"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtEmailRes' name="contactInfo.emailAddress" style='width:165px;' maxlength='100'  tabindex="20" />
															<label id="lblEmailMand" class='mandatory'>*</label>
														</div>
													</div>
													
													<div class="table-Row one-Col">
														<div class="table-Data left-TD">
															<label id="lblVerifyEmail"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtVerifyEmailRes' name="txtVerifyEmail" style='width:165px;' maxlength='100' tabindex="21" />
														</div>
													</div>
													<!--  AARESAA-5454 - Issue 1 
													<div class="table-Row one-Col" id="trItinearyLans">
														<div class="table-Data left-TD">
															<label id="lblLanguage"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<select id="selLanguageRes" name="contactInfo.emailLanguage"  style="width: 90px;" size="1" tabindex="22"></select>
														</div>
													</div>
													-->
												</div>
												<div class="table-Row  one-Col">
													<div class="table-Data one-Col">
														<label class="fntSmall">&nbsp;</label>
													</div>
												</div>
												<div class="end-Table"></div>
											</div>
										</td>
									</tr>
							  </table>
						</td>	
					</tr>	
					<%-- Emergency Contact Information --%>
					<tr id="trEmgnContactInfo">
						<td class="alignLeft">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class='pnlTop pnlWidth'></td>
									<td class='pnlTopR'></td>
								</tr>
								<tr>
									<td colspan='2' class='pnlBottomTall paddingCalss' valign='top'>
										<div class="div-Table">
											<div class="table-Row  one-Col">
												<div class="table-Data one-Col">
													<label id="lblEmgnCntInformation"  class='fntBold hdFontColor'></label>
												</div>
											</div>
											
											<div class="table-Row one-Col" id="trEmgnTitle">
												<div class="table-Data left-TD">
													<label id="lblTitle"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<select id='selEmgnTitleRes' size='1' style="width: 45px;" tabindex="23" name='contactInfo.emgnTitle'></select>
													<label id="lblEmgnTitleMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div class="table-Row two-Col" id="tblEmgnFirstName">
												<div class="table-Data left-TD">
													<label id="lblFirstname"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<input type='text' id='txtEmgnFNameRes' name="contactInfo.emgnFirstName" style='width:140px;' maxlength='50'  class="fontCapitalize"  tabindex="24" />
													<label id="lblEmgnFNameMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div class="table-Row two-Col" id="tblEmgnLastName">
												<div class="table-Data left-TD">
													<label id="lblLasttname"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<input type='text' id='txtEmgnLNameRes' name="contactInfo.emgnLastName" style='width:150px;' maxlength='50'  class="fontCapitalize"  tabindex="25" />
													<label id="lblEmgnLNameMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div id="trEmgnPhoneNo">
												<!-- Contact Details -->
												<div class="table-Row one-Col thin-row" id="trPhoneNoHeader">
													<div class="table-Data one-Col">
														<label class="fntBold hdFontColor" id="lblContactDetails"></label>
													</div>
												</div>
												
												<div class="table-Row one-Col thin-row trPhoneNoLabel">
													<div class="table-Data left-TD">
														<label>&nbsp;</label>
													</div>
													<div class="table-Data right-TD">
														<label id="lblCountryCode" class='fntSmall'></label>
														<label id="lblAreaCode" class='fntSmall areaCode'></label>
														<label id="lblNumber" class='fntSmall'></label>
													</div>
												</div>
												
												<div class="table-Row one-Col" id="trMobileNo">
													<div class="table-Data left-TD">
														<label id="lblPhoneNo"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtEmgnPCountryRes' name="contactInfo.emgnLCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="26" />
														<input type='text' id='txtEmgnPAreaRes' name="contactInfo.emgnLArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="27" class="areaCode"/>
														<input type='text' id='txtEmgnPhoneRes' name="contactInfo.emgnLNumber" style='width:100px;text-align:right;' maxlength='10'  tabindex="28" />
														<label id="lblEmgnPhoneNoMand" class='mandatory'>*</label>
													</div>
												</div>
											</div>
											
											<div class="table-Row two-Col" id="tblEmgnEmail">
												<div class="table-Data left-TD">
													<label id="lblEmailAddress"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<input type='text' id='txtEmgnEmailRes' name="contactInfo.emgnEmail" style='width:165px;' maxlength='100'  tabindex="29" />
													<label id="lblEmgnEmailMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div class="table-Row  one-Col">
												<div class="table-Data one-Col">
													<label class="fntSmall">&nbsp;</label>
												</div>
											</div>
											<div class="end-Table"></div>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr>
						<td>
							<table width='100%' border="0" cellspacing="0" cellpadding="1">
								<tr id="pageBottomBtnPannel">
									<td colspan='6' align='center'>
										<input type='button' id='btnUpdateRes' class='Button ui-state-default ui-corner-all ButtonLarge' title='Click here to update contact details'  tabindex="23">
										<input type='button' id='btnEmailRes' class='Button ui-state-default ui-corner-all ButtonLarge' title='Click here to email itinerary'  tabindex="24">
										<input type='button' id='btnItineraryRes' class='Button ui-state-default ui-corner-all ButtonLarge' title='Click here to view itinerary'   tabindex="25">
									</td>																				
								</tr>
							</table>
						</td>
					</tr>		
					<tr>
					   <td>
							<div id="divRentCar">																			
										<iframe name="frmrentcar" id="frmrentcar" src="showBlank" frameborder="0" width="500px" height="300px">
										</iframe>		
							</div>
						</td>
						<td>
							<div id="toolTip" style="display:none;position: absolute;width:200px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
								<table>
									<tr>
										<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
											<font size="6px" style="font-weight:bold"> Alerts </font>
										</td>
									</tr>
									<tr>
										<td style="padding-top:10px;" id="toolTipBody"></td>
									</tr> 
								</table>
							</div>
						</td>
					</tr>																							
					</table>																						
				</td>
			</tr>
		</table>								
	</td>
</tr>
</table>
	<c:if test='${applicationScope.isDevModeOn == "false"}'>
		<script src="../js/v2/customer/reservationDetail.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</c:if>
	<input type="hidden" id="versionRes" name="version" />
	<input type="hidden" id="selectedAncillary" name="selectedAncillary" />
	<input type="hidden" id="modifyAncillary" name="modifyAncillary" />
	<input type="hidden" id="paxJson" name="paxJson" />
	<input type="hidden" id="contactInfoJson" name="contactInfoJson" />
	<input type="hidden" id="pnr" name="pnr" />
	<input type="hidden" id="groupPNR" name="groupPNR" />
	<input type="hidden" id="oldAllSegments" name="oldAllSegments" />	
	<input type="hidden" id="makePayment" name="makePayment" />
	<input type="hidden" 	id="resRegCustomer" name="commonParams.regCustomer" value='<c:out value="${commonParams.regCustomer}" escapeXml="false"/>' />
	<input type="hidden" id="jsonOnds" name="jsonOnds"/>
	<%@ include file='../../v2/common/reservationParam.jsp'%>	
	</div>
</form>