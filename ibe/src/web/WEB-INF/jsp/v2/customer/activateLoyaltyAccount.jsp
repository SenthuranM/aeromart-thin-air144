<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<title><fmt:message key="msg.Page.Title"/></title>				
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 		
	</head>
	<body>
		<c:import url="../common/pageLoading.jsp"/>
		<div id="divLoadBg" style="display: none;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgLoyalty">
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<!-- Top Banner -->
							<c:import url="../../../../ext_html/header.jsp" />
							<!-- Content holder -->
							<tr>
								<td valign="top" align="left">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
										<tr>
											<td>
												<form id="frmLoyalty" method="post"  action="">
														<table cellspacing="0" cellpadding="2" border="0" width="100%">
										                  <tr>
																<td colspan="2">
																	<span id="spnError"></span>
																	<font class="mandatory fntEnglish"><b></b></font>
																</td>
															</tr>

															<tr>
																<td valign="top" colspan="2">
																	<label class="fntBold" id="lblHdActivate">Activate Air Arabia Mashreq Credit Card Loyalty Account</label>
																</td>
															</tr>											
															<tr>
																<td class="setHeight">
																</td>
															</tr>
															<tr>
																<td width="60%">
																	<table cellspacing="0" cellpadding="2" border="0" width="100%">
																		<tr>
																			<td valign="top"><label id="lblAccountNo">Account Number</label>:</td>
																			<td valign="top">
																				<table cellspacing="0" cellpadding="2" border="0" width="100%">
																					<tr>
																						<td>
																							<input type="text" value="" maxlength="25" size="25" name="account.loyaltyAccountNo" id="txtAccountNo"/>
																							<font class="mandatory">&nbsp;*</font>
																						</td>
																						</tr>
																				</table>
																			</td>																											
																		</tr>
				                                                          <tr>
																		  <td class="fntSmall"><label id="lblMobileNo">Mobile No :</label>:</td>
																			<td colspan="2">
																				<table cellspacing="0" cellpadding="2" border="0" width="33%" dir="ltr">
																					<tr>																	
																						<td align="left">
																							<input type="text" value="" maxlength="4" style="width: 50px;" name="txtMCountry" id="txtMCountry"/>
																						</td>
																						<td align="left">
																							<input type="text" value=""  maxlength="4" style="width: 50px;" name="txtMArea" id="txtMArea"/>
																						</td>
																						<td align="left">
																							<input type="text" value=""  maxlength="10" style="width: 100px;" name="txtMobile" id="txtMobile"/>
																						</td>																																								
																						<td align="left"><font class="fntSmall fntEnglish"><font class="MandSecure Your Trip with Travel Insurance">*</font></font></td>
																					</tr>
																				</table>
																			</td>
																		  </tr>
																			<tr>
																				<td><label id="lblDOB">Date of Birth :</label>:</td>
																				<td colspan="2">
																					<table cellspacing="0" cellpadding="2" border="0" width="100%">
																						<tr>
																							<td width="24%">
																								<input type="text" value=""  size="10" maxlength="10" name="account.dateOfBirth" id="txtDateOfBirth"/> 
																							</td>
																							<td valign="baseline">
																								<img border="0" src="../images/Calendar2_no_cache.gif" id="imgCalendar" class="cursorPointer"/><label class="mandatory"> *</label>
																							</td>																
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td><label id="lblNationality">Nationality</label>:</td>
																				<td colspan="3">
																					<table cellspacing="0" cellpadding="2" border="0" width="100%">
																						<tr>
																							<td>
																								<select style="width: 140px;" size="1" name="account.nationalityCode" id="selNationality">																									
																								</select>
																									<font class="mandatory">*</font>
																							</td>
																						</tr>
																					</table>
																				 </td>
																			</tr>
																			<tr>
																				<td><label id="lblCity">City:</label>:</td>
																				<td colspan="3">
																				<table cellspacing="0" cellpadding="2" border="0" width="100%">
																				<tbody><tr>
																					<td>
																						<input type="text" value="" class="fontCapitalize" maxlength="50" size="50" name="account.city" id="txtCity"/> 
																						<font class="mandatory">*</font>
																					</td>
																				</tr>
																				</tbody></table>
																				</td>
																			</tr>													
																			<tr>
																				<td><label id="lblCountryRes">Country of Residence :</label></td>
																				<td colspan="3">
																				<table cellspacing="0" cellpadding="2" border="0" width="100%">
																					<tr>
																						<td>
																							<select style="width: 140px;" size="1" name="account.countryCode" id="selCountry">																								
																							</select>
																							<font class="mandatory">*</font>
																						</td>
																					</tr>
																				</table>
																				</td>
																			</tr>
																			<tr><td>&nbsp;</td></tr>
																			<tr>
																				<td align="right">
																					<input type="button" title="Click here for Registration" value="Activate" class="Button" name="btnActivate" id="btnActivate"/>
																					
																				</td>
																				<td align="left" colspan="2">
																					<input type="button"  value="Previous" class="Button" name="btnPrevious" id="btnPrevious"/>
																				</td>
																			</tr>			
																	</table>
																</td>
																<td valign="top">
																	<table cellspacing="0" cellpadding="0" border="0" width="100%">
																		<tr>
																			<td width="55%">	
																					<label class="fntSmall" id="lblInfoAccontValidate">Your loyalty account number will be validated against your email ID, date of birth, mobile no., city, country and nationality. Please make sure that the information you are entering for these fields is the same as that filled in your Air Arabia Mashreq Credit Card application form.</label>
																					<br/>
																					<br/>
																				  <label class="fntSmall" id="PgLoyalty_lblInfoImportant">Important: If your user ID is different from the email ID filled in your card application form, please register as a new user with the correct email ID as per your application form</label>	
																			</td>	
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<input type="hidden" name="account.mobile" id="mobileNo"/>	
														<%@ include file='../common/iBECommonParam.jsp'%>	
													</form>
										    </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class='appLogo' colspan="2"></td>
							</tr>
							<c:import url="../../../../ext_html/cfooter.jsp" />									
						</table>
					</td>
				</tr>
		   </table>
		</div>
					
		<c:if test='${applicationScope.isDevModeOn == "false"}'>
			<script src="../js/v2/customer/activateLoyaltyAccount.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		</c:if>	
		<form id="submitForm" method="post">
		</form>
		<div id='divLoadMsg' class="mainPageLoader">
			<%@ include file='../common/includeLoadingMsg.jsp' %>
		</div>
	</body>
</html>
