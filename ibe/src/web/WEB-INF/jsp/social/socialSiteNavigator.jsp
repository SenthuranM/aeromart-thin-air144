<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
</head>
<body>
	<%
	    session.setAttribute("fromSocialSites", true);
	%>


	<script type="text/javascript">
		var ibeConfig = new Array();
		<c:out value="${applicationScope.appIBEConfigJS}" escapeXml="false" />
		location.replace("../../js/indexSocialAero_" + ibeConfig[5] + ".jsp");
	</script>
</body>
</html>
