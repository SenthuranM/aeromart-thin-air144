﻿﻿﻿﻿<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>

<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <META HTTP-EQUIV="Expires" CONTENT="-1">
    <link rel='stylesheet' type='text/css' href='../css/Style_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'/>	
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	  	
	<script language="JavaScript" src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<c:if test='${lang!="en"}'>
		<script src="../js/v2/jquery/i18n/jquery.ui.datepicker-<c:out value='${lang}' escapeXml='false'/>.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</c:if>
<script type="text/javascript">
  	var displayAdvancedScheduleDetails = "<c:out value='${displayAdvancedScheduleDetails}' escapeXml='false'/>";
	var specialFltFromDate = "<c:out value='${sffd}' escapeXml='false'/>"	
	var specialFltToDate = "<c:out value='${sftd}' escapeXml='false'/>"
	var specialFltNo = "<c:out value='${sfno}' escapeXml='false'/>"
	var specialFltDest = "<c:out value='${sfd}' escapeXml='false'/>"
	
	var startDate = "<c:out value='${selStartDate}' escapeXml='false'/>";
	var stopDate = "<c:out value='${selStopDate}' escapeXml='false'/>";
	var lang = "<c:out value='${lang}' escapeXml='false'/>";
	var specialNote = '<fmt:message key="msg.flightschedule.specialflt"/>';
	<c:out value='${mainData}' escapeXml='false'/>
	<c:out value='${schedule}' escapeXml='false'/>

	function onLoadPage(){
		getUniqueSchedule();
	}
	var sheTitle = '<fmt:message key="msg.flightschedule.title"/>';
	var noFlight = '<fmt:message key="msg.flightschedule.noflight"/>';
	var flight = '<fmt:message key="msg.flightschedule.flight"/>';
	var dep = '<fmt:message key="msg.flightschedule.dep"/>';
	var arr = '<fmt:message key="msg.flightschedule.arr"/>';
	var modelNo = '<fmt:message key="msg.flightschedule.modelNo" />';
	var duration = '<fmt:message key="msg.flightschedule.duration"/>';
	var daysOfOps = '<fmt:message key="msg.flightschedule.daysofops"/>';
	var fromDate = '<fmt:message key="msg.flightschedule.fromdate"/>';
	var to = '<fmt:message key="msg.flightschedule.to"/>';
	var toDate = '<fmt:message key="msg.flightschedule.todate"/>';
	var daily = '<fmt:message key="msg.flightschedule.daily"/>';
	var su = '<fmt:message key="msg.flightschedule.su"/>';
	var mo = '<fmt:message key="msg.flightschedule.mo"/>';
	var tu = '<fmt:message key="msg.flightschedule.tu"/>';
	var we = '<fmt:message key="msg.flightschedule.we"/>';
	var th = '<fmt:message key="msg.flightschedule.th"/>';
	var fr = '<fmt:message key="msg.flightschedule.fr"/>';
	var sa = '<fmt:message key="msg.flightschedule.sa"/>';
	var align = '<fmt:message key="msg.pg.align.left"/>';
	var hAlign = 'center';
	var recordtop = '<fmt:message key="msg.flightschedule.recordtop"/>';
	var pgDir=(lang=='ar'||lang=='fa')?"RTL":"LTR";

	var tableTag ='<br /><table class="recordlhdr '+pgDir+'" border="0" cellpadding="0" cellspacing="0" align="center" >';
	var tableTagHeader ='<br /><table class="recordtopHeader '+pgDir+'" cellpadding="0" cellspacing="0" align="center" >';

	var advancedScheduleDetails = '';
	if(displayAdvancedScheduleDetails == "true"){
		advancedScheduleDetails = '<td align="'+hAlign+'" style="width:100px;border-bottom:1 solid black;" class="hdRow">'+modelNo+'</td><td align="'+hAlign+'" style="width:75px;border-bottom:1 solid black;" class="hdRow">'+duration+'</td>';
	}
	var table   = '<tr class="header fntWhite fntBold thinBorderB thinBorderL thinBorderR"> <td align="'+hAlign+'" style="width:75px;border-bottom:1 solid black;" class="hdRow">'+flight+'</td> <td align="'+hAlign+'" style="width:75px;border-bottom:1 solid black;" class="hdRow">'+dep+'</td> <td align="'+hAlign+'" style="width:75px;border-bottom:1 solid black;" class="hdRow">'+arr+'</td> <td align="'+hAlign+'" style="width:150px;border-bottom:1 solid black;" class="hdRow">'+daysOfOps+'</td><td align="'+hAlign+'" style="width:100px;border-bottom:1 solid black;" class="hdRow">'+fromDate+'</td><td align="'+hAlign+'" style="width:100px;border-bottom:1 solid black;" class="hdRow">'+toDate+'</td>'+ advancedScheduleDetails +'</tr>';
	var noFlightHTML = '<table width="100%"><tr class="'+recordtop+' fntWhite fntBold"><td align="center" >'+noFlight+'</td></tr></table>';

	var tr = new Array('<tr class="recordd">', '<tr class="recordl">');
	var tr2 = '</tr>';
	var td1 = '<td class="GridItems" align="'+align+'" style="vertical-align : middle">'; 
	var td2 = '</td>';
	
	var row1 = '<tr class="'+recordtop+ ' fntBold fntRed"><td colspan="10" align="'+align+'">';

	var row2 = '<tr class="'+recordtop+ ' fntBold fntRed row2" style="background-color:#D4CFD0;"><td colspan="6" align="'+align+'">';
	
	var row = '<tr class="'+recordtop+ ' fntBold fntRed"><td colspan="10" align="'+align+'" class="align">';
	var comma = '<fmt:message key="msg.flightschedule.comma"/>';
	
	
	
	
</script>
<script src="../js/reservation/schedule.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	
  </head>
  
 <body  onLoad="onLoadPage();" scroll='no' onkeydown='return Body_onKeyDown(event)' >
<span id="scheduleTbl">
</span>	
 </body>

</html>
			