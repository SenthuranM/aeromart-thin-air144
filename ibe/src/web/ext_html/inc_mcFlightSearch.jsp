 <%@ include file="/WEB-INF/jsp/common/directives.jsp"%>
         <%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class='pnlBottomTall'>
	<tr>
		<td class="rowSingleGap"></td>
	</tr>
	<tr>
		<td class="pageName alignLeft paddingCalss"><label
			id="lblBookyouFlight">Book Your Air Arabia
				Flight</label></td>
	</tr>
	<tr>
		<td class="rowSingleGap"><br/></td>
	</tr>
	<tr><td>
	<div class="pnlMid">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class=''>
	<tr>
		<td class="pageName alignLeft paddingCalss">
			<label id="lblWhenWhere" >Where and when</label><br/>
		</td>
	</tr>
	<tr><td class="alignLeft paddingCalss">
		<label id="lblmcDecs">You are going to initiate a multiple destinations trip</label>
	</td></tr>
	<tr>
		<td class="rowSingleGap"></td>
	</tr>
	<tr>
		<td class="alignLeft paddingCalss" id="errorDiv">
		
		</td>
	</tr>
	<tr>
		<td id="multiOND">
			<div id="ONDTeml" class="formContainer divMultiOND" style="border-top:1px solid #d1d1d1;padding:10px">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="60" >
								<label id="lblBoarding" >From</label>
							</td>
							<td width="150" >
								<select id="selFromLoc" name="selFromLoc" style="width:150px;" tabindex="0" ></select>
								<input type="hidden" id="hndFromAirport" name="hndFromAirport" tabindex="-1"/> 
							</td>
							<td width="10"></td>
							<td width="60" >
								<label  id="lblDepature" >Depart</label>
							</td>
							<td width="125">
								<input name="departureDate" type="text" id="departureDate" class="formField" size="11" tabindex="2"/>
							</td>
							<td width="10"></td>
							<td width="60" class="formLabelSpacer" ><label id="lblCabin" >Cabin</label></td>
							<td>
								<select name="selCOS" id="selCOS" style="width:130px" tabindex="3">
								</select>
							</td>
							<td  width="10"></td>
							<td width="100" align="right" rowspan="2">
								<!-- <button id="btnDelDest" class="Button">Delete</button> -->
								<u:hButton name="btnDelDest" id="btnDelDest" tabIndex="31" value="Delete"  cssClass="redNormal" />
							</td>
						</tr>
						<tr>
						<td>
							<label id="lblTo">To</label>
						</td>
						<td>
							<select id="selToLoc" name="selToLoc" class="formField" style="width:150px;" tabindex="1" ></select>
							<input type="hidden" id="hndToAirport" name="hndToAirport" tabindex="-1"/> 
						</td>
						<td colspan="8" >
						<span style="display:none">
							<select id="selCurrency" name="selCurrency"></select>
							<select id="selType" name="selType"></select>
							<select id="selFareType" name="selFareType"></select>
						</span>
						</td>
						</tr>
					</tbody>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td class="rowSingleGap"></td>
	</tr>
	<tr id="trAddDest">
		<td class="alignRight paddingCalss" style="padding:10px">
				<!-- <button id="btnAddDestination" class="Button ButtonLarge">Add Destination</button> -->
				<u:hButton name="btnAddDestination" id="btnAddDestination" tabIndex="31" value="Add Destination"  cssClass="redNormal"/>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class='pnlBottomTall'>
	<tr>
		<td class="rowSingleGap"></td>
	</tr>
	<tr>
		<td class="pageName alignLeft paddingCalss">
			<label id="lblTravelers" class="pageName alignLeft">Travellers Info</label><br/>
		</td>
	</tr>
	<tr>
		<td class="rowSingleGap"></td>
	</tr>
	<tr>
		<td>
			<div class="formContainer" style="border-top:1px solid #d1d1d1;padding:10px">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="50" >
								<label id="lblAdult" >Adults</label><br>
								<label id="lblAdultAgeRange" >12+</label>
							</td>
							<td width="65">
								<select id="selAdults" name="selAdults"  style="width: 50px;" tabindex="0" >
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</td>
							<td width="10"></td>
							<td width="45" >
								<label id="lblChild" >Children</label><br>
								<label id="lblChildAgeRange" >2-12</label>
							</td>
							<td width="65">
								<select id="selChild" name="selChild"  style="width: 50px;" tabindex="0" >
									<option value="0">0</option>
								</select>
							</td>
							<td width="10"></td>
							<td width="45" >
								<label id="lblInfant" >Infants</label>
							</td>
							<td width="70">
								<select id="selInfants" name="selInfants"  style="width: 50px;" tabindex="0" >
									<option value="0">0</option>
								</select>
							</td>
							<td width="60" >
								<label id="lblCurrency" >Currency </label>
							</td>
							<td width="90">
								<select id="selSelectedCurrency" name="selSelectedCurrency"  style="width: 80px;" tabindex="0" >									
								</select>
							</td>
							<td class="alignRight" width="auto">
								<!-- <input type="button" id="btnFindFlight"  class="Button" value="Search for flights" /> -->
								<u:hButton name="btnFindFlight" id="btnFindFlight" tabIndex="31" value="Find Flights"  cssClass="redNormal"/>
							</td>
						</tr>
						<tr>
							<td colspan="11">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr id="trPromoCode">
										<td width="80" ><label id="lblPromoCode">Promo Code </label></td>
										<td width ="150">
											<input type="text" id="promoCode" name="promoCode" style="width: 100px;height: 15px" maxlength="40"/>
										</td>
										<td colspan="9"></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</td>
	</tr></table>
	</div>
	</td></tr>
	<tr>
		<td class="rowSingleGap"></td>
	</tr>
</table>