<%--
Author: Baladewa
// ******** Changing the Insurance Content to display in XBE Ancillary **** //
1. Get a copy of the "ibe/src/web/ext_html/ins_IBEContent.jsp" file and place in 
	to the XBE clients ext_html folder (if not exist create a folder in Client folder).
2. Be sure not change any of the ID in the existing elements and change the only the 
	HTML as client expect.
3. what ever additional (client specific) functionalities included in the client's ins_content.jsp
	with out breaking the flow
 --%>
<div id="tblInsMain">
<table border='0' cellpadding='1' cellspacing='1' width='100%'>
<tr>
	<td class='alignLeft'>
		<label id='lblInsuranceHD' class='fntBold hdFontColor'>
		</label>
	</td>
</tr>
<tr>
	<td class='rowGap' colspan="2"></td>
</tr>
<tr><td><div class='insPromoOffer alignLeft'></div><div class='rowGap clear'></div></td></tr>
<tr>
	<td class='alignLeft'><label id='lblInsuranceDesc'></label></td>
</tr>
<tr>
	<td class='rowGap' colspan="2"></td>
</tr>
<tr>
	<td class='alignLeft'><label id='lblInsuranceChDesc'></label>&nbsp;<label id='spnInsCost'></label>
</tr>
<tr>
	<td class='rowGap' colspan="2"></td>
</tr>
<tr>
	<td width='100%' valign='top' class='alignLeft' colspan="2"><table border='0' cellpadding='5' cellspacing='0' class='insCoverInforTable'>
	<tr><td class='alignLeft'><label id='insCoverInfo1'></label></td></tr>
	<tr><td class='withIcon'><span>&nbsp;</span><label id='insCoverInfo2'></label></td></tr>
	<tr><td class='withIcon'><span>&nbsp;</span><label id='insCoverInfo3'></label></td></tr>
	<tr><td class='withIcon'><span>&nbsp;</span><label id='insCoverInfo4'></label></td></tr>
	</table>
	</td>
</tr>
<tr><td class='rowGap'  colspan="2"></td></tr>
</table>
</div>

<!-- JUST for check -->


<div id="tblInsRider"></div>
<div id="tblInsTnC" align="left"></div>



<div id="tblInsRider">

</div>
	
	

