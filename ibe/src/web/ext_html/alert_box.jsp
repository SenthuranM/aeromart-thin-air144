<html>
<head>
<style type="text/css">
/* jAlert */  
body{margin: 0px;padding: 0px;overflow: hidden;font-family: Arial, sans-serif;font-size: 13px}
input{
	background:#a3a3a3;
	border:1px solid #888888;
	color:#333;
	cursor:pointer;
	font-family:tahoma;
	font-size:11px;
	font-weight:bold;
	height:22px;
	text-align:center;
	text-transform:capitalize;
	margin: 1px 0;
}
#popup_container {
	font-family: Arial, sans-serif;
	font-size: 12px;
	min-width: 300px; /* Dialog will be no smaller than this */
	max-width: 600px; /* Dialog will wrap after this width */
	background: #FFF;
	border: solid 2px #d5241e;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	color: #fff;
}
.popuo_title_bg{
	background:url("../images/panelBGall_no_cache.jpg") repeat scroll 0 -30px transparent;
	border-bottom:1px solid #999999;
	padding:0px 5px;
	height: 25px;
}

.popuo_title_bg #popup_title {
	color:#FFFFFF;
	cursor:default;
	font-size:14px;
	font-weight:bold;
	line-height:1.75em;
	margin:0;
	text-align:left;
	position: relative;
	display: inline;
	width:300px;
}

.rtl .popuo_title_bg #popup_title {
	color:#FFFFFF;
	cursor:default;
	font-size:14px;
	font-weight:bold;
	line-height:1.75em;
	margin:0;
	text-align: right;
	position: relative;
	display: inline;
	width:100px;
}

.popuo_title_bg span.popuo_close{
	background: url("../images/popupClose_no_cache.gif") no-repeat;
	height: 18px;width: 15px;display: block;
	position: relative;top:4px;
}

.rtl .popuo_title_bg span.popuo_close{
	background: url("../images/popupClose_no_cache.gif") no-repeat;
	height: 18px;width: 15px;display: block;
	position: relative;top:4px;
}

#popup_content {
	background: 16px 16px no-repeat url(../images/info_no_cache.gif);
	padding: 1.5em 1.75em 1em 1.75em;
	margin: 0em;
	color: #000;
}

#popup_content.alert {
	background-image: url(../images/important_no_cache.gif);
}

.rtl #popup_content.alert {
	 background-position: 95% 1em;
}

#popup_content.confirm {
	background-image: url(../images/help_no_cache.gif);
}

.rtl #popup_content.confirm {
    background-position: 95% 1em;
}

#popup_content.prompt {
	background-image: url(../images/info_no_cache.gif);
}

.rtl #popup_content.prompt {
	    background-position: 95% 1em;
}

#popup_message {
	padding-left: 48px;
}

.rtl #popup_message {
	padding-right: 48px;
}

#popup_panel {
	text-align: center;
	margin: 1em 0em 0em 1em;
	position: relative;
}

#popup_prompt {
	margin: .5em 0em;
}
</style>
	
</head>
<body style="margin: 0;padding: 0;height: 100%">
	<div class="popuo_title_bg">
		<table style="border-collapse: collapse;width:100%"><tr>
		<td width="95%"><h1 id="popup_title"></h1></td>
		<td width="5%"><span class="popuo_close"></span></td>
		</tr></table>
	</div>
	<div style="clear:both;height: 1px"></div>
	<div id="popup_content">
		  <div id="popup_message"></div>
	</div>
	<div id="popup_panel"></div>
</body>
</html>
