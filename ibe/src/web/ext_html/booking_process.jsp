<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
		<tr>
			<td class="stepsItem">
				<div class="<fmt:message key="msg.pg.process1"/>">
					&nbsp;
				</div>
			</td>
			<td class="gap"></td>
			<td class="stepsItem">
				<div class="<fmt:message key="msg.pg.process2"/>">
					&nbsp;
				</div>
			</td>
			<td class="gap"></td>
			<td class="stepsItem">
				<div class="<fmt:message key="msg.pg.process3"/>">
					&nbsp;
				</div>
			</td>
			<td class="gap"></td>
			<td class="stepsItem">
				<div class="<fmt:message key="msg.pg.process4"/>">
					&nbsp;
				</div>
			</td>
		</tr>
	</table>
