//indexCommon.js

var dtC = new Date();
var dtCM = dtC.getMonth() + 1;
var dtCD = dtC.getDate();
if (dtCM < 10){dtCM = "0" + dtCM}
if (dtCD < 10){dtCD = "0" + dtCD}

var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); //"25/08/2005";
var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));

	function buildYearDropDown(strName, intYears){
		var dtDepDate	= addDays(dtSysDate, 0);
		var intMonth	= dtDepDate.getMonth();
		var intYear		= dtDepDate.getFullYear();
		var arrMonths	= new Array("January","February","March","April","May","June","July","August","September","October","November","December");

		var objDrop		= getFieldByID(strName);
		var strValue    = "";
		var intMonths	= 12 * intYears
		for (var i = 0 ; i < intMonths; i++){

			if ((intMonth + 1) < 10){
				strValue = "0" + (intMonth + 1);
			}else{
				strValue = (intMonth + 1);
			}

			objDrop.length =  objDrop.length + 1
			objDrop.options[objDrop.length - 1].text = arrMonths[intMonth] + " " + intYear;
			objDrop.options[objDrop.length - 1].value = strValue + "/" + intYear;
			intEndMonth = strValue;
			intEndYear  = intYear;
			intMonth++

			if (intMonth > 11){
				intMonth = 0 ;
				intYear++;
			}
		}
	}

	function faqClick(){
		var intHeight = (window.screen.height - 200);
		var intWidth = 795;
		var intX = ((window.screen.height - intHeight) / 2);
		var intY = ((window.screen.width - intWidth) / 2);
		var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
		if ((objCW)  && (!objCW.closed)){objCW.close();}
		objCW = window.open(strFAQLink,'ToolChild',strProp)

	}

	function FillCalenderDay(objDName, strValue){
		var objdrop = getFieldByID(objDName)
		var strCD	= strSysDate
		var strDMonth = Number(strSysDate.substr(3,2));
		var strDYear = Number(strSysDate.substr(6,4));

		var strMonth = Number(strValue.substr(0,2));
		var strYear = Number(strValue.substr(3,4));

		var strDays = getDaysInMonth(strMonth, strYear);
		var strDay = ""
		var strStartDate = 1;
		if ((strDMonth == strMonth) && (strYear == strDYear)){
			strStartDate = Number(strSysDate.substr(0,2))
		}

		objdrop.length = 1;
		for (var i = strStartDate; i <= strDays ; i ++){
			strDay = i
			if (strDay < 10){
				strDay = "0" + strDay
			}
			objdrop.length = objdrop.length + 1
			objdrop.options[objdrop.length -1].text =  strDay;
			objdrop.options[objdrop.length -1].value =  strDay;
		}
	}

	function buildDropDowns(){
		var intCount = arrAirports.length ;
		var objDF 	 = getFieldByID("selFromLoc");
		var objDT 	 = getFieldByID("selToLoc");

		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] == strBaseAirport){

				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strFromHD;
				objDF.options[objDF.length - 1].value = "";

				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";

				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];

				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";

				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strToHD;
				objDT.options[objDT.length - 1].value = "";

				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";

				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];

				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";
				break;
			}
		}

		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] != strBaseAirport){
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];

				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];
			}
		}

		buildYearDropDown("selYrDept",1);
		buildYearDropDown("selYrRetu",1);
		FillCalenderDay("selDtDept", getFieldByID("selYrDept").options[1].value);

		defaultSetYearDay();

		var objLB = new listBox();
		
		objLB.dataArray = arrClass;
		objLB.id = "selCOS";
		objLB.blnMergeTextValue = false;
		objLB.firstTextValue = strDDDefValue;
		objLB.blnMergeStyle = "-";
		objLB.blnFirstEmpty = true;
		objLB.fillListBox();

		objLB.dataArray = arrCurrency;
		objLB.id = "selCurrency";
		objLB.blnMergeTextValue = true;
		objLB.firstTextValue = strDDDefValue;
		objLB.blnMergeStyle = "-";
		objLB.blnFirstEmpty = true;
		objLB.fillListBox();

		objLB.dataArray=arrLan;
		objLB.id="selLangunage";
		objLB.blnMergeTextValue=true;
		objLB.firstTextValue=strDDDefValue;
		objLB.blnMergeStyle="-";
		objLB.blnFirstEmpty=false;
		objLB.fillListBox();

		objLB.dataArray = arrCusType;
		objLB.id = "selType";
		objLB.blnMergeTextValue = false;
		objLB.firstTextValue = strDDDefValue;
		objLB.blnMergeStyle = "";
		objLB.blnFirstEmpty = true;
		objLB.fillListBox();
	}


	function defaultSetYearDay(){
		FillCalenderDay("selDtRetu", getFieldByID("selYrRetu").options[1].value);
	}

	function selYrDeptOnChange(){
		FillCalenderDay("selDtDept", getValue("selYrDept"));
		setField("selDtDept", strSeleDDay);
	}

	function selYrRetuOnChange(){
		FillCalenderDay("selDtRetu", getValue("selYrRetu"));
		setField("selDtRetu", strSeleRDay);
	}

	function selDtDeptOnChange(){
		strSeleDDay = getValue("selDtDept");
	}

	function selDtRetuOnChange(){
		strSeleRDay = getValue("selDtRetu");
	}

	function selToLocOnChange(){
		if (getText("selToLoc") == strDash){
			getFieldByID("selToLoc").options[0].selected = true;
		}
	}

	function selFromLocOnChange(){
		if (getText("selFromLoc") == strDash){
			getFieldByID("selFromLoc").options[0].selected = true;
		}
	}

	function setDate(strDate, strID){
		var arrStrValue = strDate.split("/");
		switch (strID){
			case "0" :
				setField("selYrDept", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrDeptOnChange();
				setField("selDtDept", arrStrValue[0]);
				break ;
			case "1" :
				setField("selYrRetu", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrRetuOnChange();
				setField("selDtRetu", arrStrValue[0]);
				break ;
		}
	}

	function chkReturnTrip_click(){
		if (getFieldByID("chkReturnTrip").checked){
			setField("selRVariance", intRDaysDef);
			Disable("selDtRetu", false);
			Disable("selYrRetu", false);
			Disable("selRVariance", false);
		}else{
			setField("selRVariance", "0");
			setField("selDtRetu", "");
			setField("selYrRetu", "");
			Disable("selDtRetu", true);
			Disable("selYrRetu", true);
			Disable("selRVariance", true);
			defaultSetYearDay();
		}
	}

	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 0 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.currentDate = strSysDate;
		switch (strID){
			case 0 :
				if (getValue("selDtDept") != "" && getValue("selYrDept")){
					objCal1.currentDate = getValue("selDtDept") + "/" + getValue("selYrDept");
				}
				break;
			case 1 :
			if (getFieldByID("chkReturnTrip").checked){
				if (getValue("selDtRetu") != "" && getValue("selYrRetu")){
					objCal1.currentDate = getValue("selDtRetu") + "/" + getValue("selYrRetu");
				}
			}else{
				return;
			}
			break;
		}
		objCal1.showCalendar(objEvent);
	}

	// --------------------- Maximum Passengers
	function selAdults_onChange(){
		var arrInfants = new Array();
		var intAdults = Number(getValue("selAdults"))// + Number(getValue("selChild"));
		var strCInfants = getValue("selInfants");
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrInfants;
		objLB.textIndex = "0";
		objLB.id = "selInfants";
		objLB.fillListBox();

		setField("selInfants",  strCInfants);
	}

	function flightSearchBtnOnClick(intID){
		switch (intID){
			case 0 :
				if (clientValidateFS()){
					var objFrm = getFieldByID("form1");
					objFrm.target = "_top";
					objFrm.action = "../public/showReservation.action";
					objFrm.submit();
				}
				break;
			case 1 :
				var strPostData =  getValue("selLangunage") + "^RE"
				setField("hdnParamData", strPostData);
				var objFrm = getFieldByID("form1");
				objFrm.action = secureUrl+"showReservation.action";
				objFrm.target = "_top";
				objFrm.submit();
				break;
			case 2 :
				var strPostData = getValue("selLangunage") + "^SI"
				setField("hdnParamData", strPostData);
				var objFrm = getFieldByID("form1");
				objFrm.action = secureUrl+"showReservation.action";
				objFrm.target = "_top";
				objFrm.submit();
				break;
		}
	}


	function clientValidateFS(){
		if (getValue("selFromLoc") == ""){
			alert(arrError["ERR001"]);
			getFieldByID("selFromLoc").focus();
			return false;
		}

		if (getValue("selToLoc") == ""){
			alert(arrError["ERR002"]);
			getFieldByID("selToLoc").focus();
			return false;
		}

		if (getValue("selCOS") == ""){
			alert(arrError["ERR010"]);
			getFieldByID("selCOS").focus();
			return false;
		}
		
		if (getValue("selType") == "") {
			alert(arrError["ERR011"]);
			getFieldByID("selType").focus();			
			return false;
		}

		if (getValue("selCurrency") == ""){
			alert(arrError["ERR006"]);
			getFieldByID("selCurrency").focus();
			return false;
		}

		if (getValue("selFromLoc") == getValue("selToLoc")){
			alert(arrError["ERR003"]);
			getFieldByID("selToLoc").focus();
			return false;
		}

		if ((getValue("selDtDept") == "") || (getValue("selYrDept") == "")){
			alert(arrError["ERR007"]);
			getFieldByID("selDtDept").focus();
			return false;
		}else{
			if (!CheckDates(strSysDate, getValue("selDtDept") + "/" + getValue("selYrDept"))){
				alert(arrError["ERR009"] + strSysDate + ".");
				getFieldByID("selDtDept").focus();
				return false;
			}
		}

		if (getFieldByID("chkReturnTrip").checked){
			if ((getValue("selDtRetu") == "") || (getValue("selYrRetu") == "")){
				alert(arrError["ERR008"]);
				getFieldByID("selDtRetu").focus();
				return false;
			}

			if (!CheckDates(getValue("selDtDept") + "/" + getValue("selYrDept"), getValue("selDtRetu") + "/" + getValue("selYrRetu"))){
				alert(arrError["ERR004"]);
				getFieldByID("selDtRetu").focus();
				return false;
			}
		}

		strRDate = "";
		blnReturn = false;
		if (getValue("selDtRetu") != "" && getValue("selYrRetu") != ""){
			strRDate	= getValue("selDtRetu") + "/" + getValue("selYrRetu");
		}
		if (getFieldByID("chkReturnTrip").checked){blnReturn	= true;}

		var strPostData = getValue("selLangunage") + "^" +
						  "AS^" +
					   getValue("selFromLoc") + "^" +
					   getValue("selToLoc") + "^" +
					   getValue("selDtDept") + "/" + getValue("selYrDept") + "^" +
					   strRDate + "^" +
					   getValue("selDVariance") + "^" +
					   getValue("selRVariance") + "^" +
					   getValue("selCurrency") + "^" +
					   getValue("selAdults") + "^" +
					   getValue("selChild") + "^" +
					   getValue("selInfants") + "^" +
					   getText("selFromLoc") + "^" +
					   getText("selToLoc") + "^" +
					   blnReturn  + "^" +
				       getValue("selCOS") + "^" +
				       getValue("selType");
		setField("hdnParamData", strPostData);
		return true;
	}

	function defaultDataLoad(){
		var dtDepDate = addDays(dtSysDate, Number(strDefDayDept));
		strDDate = dateChk(dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear());
		strSeleDDay = dtDepDate.getDate();

		var dtRetuDate = "";
		strRDate = "";
		strSeleRDay = "  /  /    ";
		if (String(strDefDayRetu) != ""){
			var dtRetuDate = addDays(dtSysDate, Number(strDefDayRetu));
			strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
			strSeleRDay = dtRetuDate.getDate();
			setField("chkReturnTrip", true);
		}

		setField("selDVariance", strDDays);
		setField("selRVariance", strRDays);
		chkReturnTrip_click();
	}

	function dateOnBlur(strID, objC){
		var blnDateEntered = false;
		if (objC.value != ""){blnDateEntered = true;}
		dateChk(strID);
		if (objC.value == "" && blnDateEntered){
			alert(arrError["ERR005"]);
		}
	}

	function cacheData(){
		setField("selFromLoc", strFrom);
		setField("selToLoc", strTo);

		var arrDtDept = strDDate.split("/");
		setField("selYrDept", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrDeptOnChange();
		setField("selDtDept", arrDtDept[0]);

		setField("selYrRetu", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrRetuOnChange();
		setField("selDtRetu", arrDtDept[0]);

		if (getValue("selDtRetu") == ""){
			defaultSetYearDay();
		}

		setField("selDVariance", strDDays);
		setField("selRVariance", strRDays);
		setField("selCOS", strSelCOS);
		setField("selCurrency", strSelCurr);
		setField("selType", strSelDefType);
		getFieldByID("selFromLoc").focus();
	}

	// ------------------------
	function flightSearchOnLoad(){
		buildDropDowns();

		var arrAdults = new Array();
		var arrChild  = new Array();
		arrChild[0] = new Array();
		arrChild[0][0] = "0";
		for (var i = 1 ; i <= intMaxAdults ; i++){
			arrAdults[i - 1] = new Array();
			arrAdults[i - 1][0] = i;

			arrChild[i] = new Array();
			arrChild[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrAdults;
		objLB.textIndex = "0";
		objLB.id = "selAdults";
		objLB.fillListBox();

		objLB = new listBox();
		objLB.dataArray = arrChild;
		objLB.textIndex = "0";
		objLB.id = "selChild";
		objLB.fillListBox();
		selAdults_onChange();

		// ------------------------
		var arrDays = new Array();
		for (var i = 0 ; i <= intDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;
		}

		var objLB2 = new listBox();
		objLB2.dataArray = arrDays;
		objLB2.id = "selDVariance";
		objLB2.fillListBox();

		arrDays.length = 0
		for (var i = 0 ; i <= intRDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;
		}
		objLB2 = new listBox();
		objLB2.dataArray = arrDays;
		objLB2.id = "selRVariance";
		objLB2.fillListBox();

		Disable("selDtRetu", true);
		Disable("selYrRetu", true);
		Disable("selRVariance", true);

		defaultDataLoad()
		cacheData();
		
		if(typeof ibeConfig != 'undefined') {
			if(ibeConfig[4]!='true'){
				display('trCOS1',false);
				display('trCOS2',false);
				display('trCOS3',false);
			}
		}
	}
	flightSearchOnLoad();

	// ---------------- Calendar
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.borderColor = "#FE0000";
	objCal1.headerTextColor = "white";
	objCal1.noYears = "1";
	objCal1.blnArabic = false;
	objCal1.disableUpto = strSysDate;
	objCal1.disableFrom = getDaysInMonth(intEndMonth, intEndYear) + "/" + intEndMonth + "/" + intEndYear;
	objCal1.buildCalendar();
