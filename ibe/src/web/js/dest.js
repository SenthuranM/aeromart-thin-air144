<!--

var arrAirports 	= new Array();
var arrCurrency = new Array();
var arrCurrencyMA = new Array();
var arrError = new Array();

arrAirports[0] = new Array("AMD","Ahmedabad","G9");
arrAirports[1] = new Array("ALP","Aleppo","G9");
arrAirports[2] = new Array("ALY","Alexandria","G9,MA");
arrAirports[3] = new Array("ALA","Almaty","G9");
arrAirports[4] = new Array("AMM","Amman Queen Alia int","G9");
arrAirports[5] = new Array("AMS","Amsterdam - Schiphol","MA");
arrAirports[6] = new Array("ATZ","Assiut","G9");
arrAirports[7] = new Array("ATH","Athens","G9");
arrAirports[8] = new Array("BAH","Bahrain","G9");
arrAirports[9] = new Array("BLR","Bangalore","G9");
arrAirports[10] = new Array("BCN","Barcelona - Barcelona International","MA");
arrAirports[11] = new Array("BEY","Beirut","G9");
arrAirports[12] = new Array("CRL","Brussels South - Charleroi","MA");
arrAirports[13] = new Array("CMN","Casablanca","MA");
arrAirports[14] = new Array("MAA","Chennai","G9");
arrAirports[15] = new Array("CGP","Chittagong","G9");
arrAirports[16] = new Array("CJB","Coimbatore","G9");
arrAirports[17] = new Array("CMB","Colombo","G9");
arrAirports[18] = new Array("DAM","Damascus","G9");
arrAirports[19] = new Array("DMM","Dammam King Fahad","G9");
arrAirports[20] = new Array("DEL","Delhi","G9");
arrAirports[21] = new Array("DAC","Dhaka","G9");
arrAirports[22] = new Array("DOH","Doha","G9");
arrAirports[23] = new Array("HHN","Frankfurt - Hahn","MA");
arrAirports[24] = new Array("GVA","Geneva - Cointrin International","MA");
arrAirports[25] = new Array("GOI ","Goa","G9");
arrAirports[26] = new Array("HYD","Hyderabad","G9");
arrAirports[27] = new Array("SAW","Istanbul Sabiha Int Airport","G9");
arrAirports[28] = new Array("JAI","Jaipur","G9");
arrAirports[29] = new Array("JED","Jeddah King A Aziz","G9");
arrAirports[30] = new Array("KHI","Karachi","G9");
arrAirports[31] = new Array("KTM","Kathmandu","G9");
arrAirports[32] = new Array("KRT","Khartoum","G9");
arrAirports[33] = new Array("KBP","Kiev","G9");
arrAirports[34] = new Array("COK","Kochi","G9");
arrAirports[35] = new Array("CCJ","Kozhikode (Calicut)","G9");
arrAirports[36] = new Array("KWI","Kuwait","G9");
arrAirports[37] = new Array("LTK","Latakia","G9");
arrAirports[38] = new Array("LIS","Lisbon - Portela","MA");
arrAirports[39] = new Array("STN","London - Stansted","MA");
arrAirports[40] = new Array("LXR","Luxor International","G9");
arrAirports[41] = new Array("LYS","Lyon - Saint Exupery","MA");
arrAirports[42] = new Array("MAD","Madrid - Barajas International","MA");
arrAirports[43] = new Array("MRS","Marseille - Provence","MA");
arrAirports[44] = new Array("BGY","Milan - Bergamo","MA");
arrAirports[45] = new Array("MPL","Montpelier - Mediterranee","MA");
arrAirports[46] = new Array("BOM","Mumbai","G9");
arrAirports[47] = new Array("MCT","Muscat","G9");
arrAirports[48] = new Array("NAG","Nagpur","G9");
arrAirports[49] = new Array("NBO","Nairobi","G9");
arrAirports[50] = new Array("CDG","Paris - Charles De Gaulle","MA");
arrAirports[51] = new Array("ORY","Paris - Orly","MA");
arrAirports[52] = new Array("PEW","Peshawar","G9");
arrAirports[53] = new Array("RUH","Riyadh King Khaled","G9");
arrAirports[54] = new Array("SAH","Sanaa","G9");
arrAirports[55] = new Array("SHJ","Sharjah","G9");
arrAirports[56] = new Array("SYZ","Shiraz","G9");
arrAirports[57] = new Array("TNG","Tangier","MA");
arrAirports[58] = new Array("IKA","Tehran","G9");
arrAirports[59] = new Array("TFN","Tenerife - Tenerife North","MA");
arrAirports[60] = new Array("TRV","Thiruvananthapuram","G9");
arrAirports[61] = new Array("TUN","Tunis - Carthage","MA");


//the last value (false/true) specify the default currency (true for default)
arrCurrency[0] = new Array('BHD','Bahrani Dinar',false);
arrCurrency[1] = new Array('EUR','Euro',false); 
arrCurrency[2] = new Array('INR','Indian Rupee',false); 
arrCurrency[3] = new Array('QAR','Qatari Riyal',false); 
arrCurrency[4] = new Array('OMR','Rial Omani',false); 
arrCurrency[5] = new Array('AED','UAE Dirham',true); 
arrCurrency[6] = new Array('USD','US Dollar',false);
arrCurrency[7] = new Array('UAH','Ukrainian Hryvnia',false);

//the following array should be changed
arrCurrencyMA[0] = new Array('EGP','Egyptiyan Pound',false);
arrCurrencyMA[1] = new Array('EUR','Euro',false); 
arrCurrencyMA[2] = new Array('GBP','British Pound',false); 
arrCurrencyMA[3] = new Array('MAD','Moroccan Dirham',true); 
arrCurrencyMA[4] = new Array('TND','Tunisia, Dinars',false); 
arrCurrencyMA[5] = new Array('USD','US Dollar',false);

arrError['ERR009'] = 'Your Departure Date cannot be before the current date. Please check and enter again.';arrError['ERR008'] = 'Please select your Returning Date.';arrError['ERR002'] = 'Please select your destination.';arrError['ERR006'] = 'Please select a Currency.';arrError['ERR004'] = 'Your Returning Date cannot be before your Departing Date. Please check your dates and enter again. ';arrError['ERR001'] = 'Please select the city you will be departing from.';arrError['ERR005'] = 'Please enter a valid Date.';arrError['ERR007'] = 'Please select your Departing Date.';arrError['ERR003'] = 'The From & To locations cannot be the same. Please check and enter again';;


var strDash = "----------------------"
var strSeleDDay = "";
var strSeleRDay = "";
var intEndMonth  = "";
var intEndYear = "";	
var objCW ;
var strRDate = "";
var strDDate = "";
var blnReturn = false;
var strLanguage = "EN";

// Default values
var intMaxAdults = 9
var strFromHD = 'Where are you flying from'
var strToHD = 'Where are you flying to'
var strHD = '-- select --';//added by Haider 18-08-08
var strDDDefValue = 'Please Select'
var intRDaysDef = 0;
var strDefDayRetu = "0";
var strBaseAirport1 = "SHJ"
var G9URL = "http://reservations.airarabia.com/ibe/public/";
var strBaseAirport2 = "CMN" //this should be replaced according to the destination code
var MAURL = "http://reservationsma.airarabia.com/ibe/public/"; //new url should be here
var shjHub = 'G9';
var maHub = 'MA';//this should replaced according to new hub code
var strFrom = "";
var strTo = "";
var strCurr = "AED";
var strSelCurr = "AED";
var strDDays = "0";
var strRDays = "0";
var intDays = "3";
var intRDays = "3";
var strDefDayDept = "0";
var firstSelected = null;
var firstSelected2 = null;

var dtC = new Date();
var dtCM = dtC.getMonth() + 1;
var dtCD = dtC.getDate();
if (dtCM < 10){dtCM = "0" + dtCM}
if (dtCD < 10){dtCD = "0" + dtCD}

var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); //"25/08/2005";
var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));

	function buildYearDropDown(strName, intYears){
		var dtDepDate	= addDays(dtSysDate, 0);
		var intMonth	= dtDepDate.getMonth();
		var intYear		= dtDepDate.getFullYear();
		var arrMonths	= new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		
		var objDrop		= getFieldByID(strName);
		if(objDrop == null)
			return ;
		var strValue    = "";
		var intMonths	= 12 * intYears
		for (var i = 0 ; i < intMonths; i++){
			
			if ((intMonth + 1) < 10){
				strValue = "0" + (intMonth + 1);
			}else{
				strValue = (intMonth + 1);
			}
			
			objDrop.length =  objDrop.length + 1
			objDrop.options[objDrop.length - 1].text = arrMonths[intMonth] + " " + intYear;
			objDrop.options[objDrop.length - 1].value = strValue + "/" + intYear;
			intEndMonth = strValue;
			intEndYear  = intYear;
			intMonth++
			
			if (intMonth > 11){
				intMonth = 0 ;
				intYear++;
			}
		}
	}
	
	function faqClick(){
		var intHeight = (window.screen.height - 200);
		var intWidth = 795;
		var intX = ((window.screen.height - intHeight) / 2);
		var intY = ((window.screen.width - intWidth) / 2);
		var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
		if ((objCW)  && (!objCW.closed)){objCW.close();}
		objCW = window.open(strFAQLink,'ToolChild',strProp)
		
	}
	
	function FillCalenderDay(objDName, strValue){
		var objdrop = getFieldByID(objDName)
		var strCD	= strSysDate
		var strDMonth = Number(strSysDate.substr(3,2));
		var strDYear = Number(strSysDate.substr(6,4));
		
		var strMonth = Number(strValue.substr(0,2));
		var strYear = Number(strValue.substr(3,4));
		
		var strDays = getDaysInMonth(strMonth, strYear);
		var strDay = ""
		var strStartDate = 1;
		if ((strDMonth == strMonth) && (strYear == strDYear)){
			strStartDate = Number(strSysDate.substr(0,2))
		}
		
		objdrop.length = 1;
		for (var i = strStartDate; i <= strDays ; i ++){
			strDay = i 
			if (strDay < 10){
				strDay = "0" + strDay
			}
			objdrop.length = objdrop.length + 1
			objdrop.options[objdrop.length -1].text =  strDay;
			objdrop.options[objdrop.length -1].value =  strDay;
		}
	}
	function isHubMatch(destHub, hubCodes){
		var n=hubCodes.length;
		for(i=0;i<n;i++){
			if(destHub.match(hubCodes[i])!=null){
				return true;
			}
		}
		return false;
	}
	function buildDestList(obj, hub, dText){
		var intCount = arrAirports.length ; 
		
		obj.length =  obj.length + 1
		obj.options[obj.length - 1].text = dText;
		obj.options[obj.length - 1].value = "";
		
		obj.length =  obj.length + 1
		obj.options[obj.length - 1].text = strDash;
		obj.options[obj.length - 1].value = "";
		var hubCodes = hub.split(",");		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] == strBaseAirport1 || arrAirports[i][0] == strBaseAirport2){
				
				if(isHubMatch(arrAirports[i][2], hubCodes) == true){
					obj.length =  obj.length + 1
					obj.options[obj.length - 1].text = arrAirports[i][1];
					obj.options[obj.length - 1].value = i;
					
					obj.length =  obj.length + 1
					obj.options[obj.length - 1].text = strDash;
					obj.options[obj.length - 1].value = "";
				}
			}
		}
			

		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] != strBaseAirport1 && arrAirports[i][0] != strBaseAirport2){
				if(isHubMatch(arrAirports[i][2], hubCodes) == true){
					obj.length =  obj.length + 1
					obj.options[obj.length - 1].text = arrAirports[i][1];
					obj.options[obj.length - 1].value = i;
				}
			}
		}
	}
	
	function buildCurrencyList(arr){
		var intCount = arr.length;
		var obj = document.getElementById('selCurrency');
		obj.length = 0;
		for (var i = 0 ; i < intCount ; i++){
			obj.length =  obj.length + 1
			obj.options[obj.length - 1].text = arr[i][1];
			obj.options[obj.length - 1].value = arr[i][0];
			obj.options[obj.length - 1].selected = arr[i][2];
		}
	}
	function buildDropDowns(){
			var objDF 	 = getFieldByID("selFromLoc");
			var objDT 	 = getFieldByID("selToLoc");
			//added by Haider 18-08-08
			var objDF2 	 = getFieldByID("selFromLoc2");
			var objDT2 	 = getFieldByID("selToLoc2");
			var hubs = shjHub+","+maHub;
			buildDestList(objDF, hubs, strFromHD);
			buildDestList(objDT, hubs, strToHD);
			if(objDF2!=null && objDT2!=null){
				buildDestList(objDF2, hubs,strHD);
				buildDestList(objDT2, hubs,strHD);
			}
			buildYearDropDown("selYrDept",1);
			buildYearDropDown("selYrRetu",1);
			buildYearDropDown("selYrDept2",1);//added by Haider 18-08-08
			FillCalenderDay("selDtDept", getFieldByID("selYrDept").options[1].value);
			
			defaultSetYearDay();
			
			var objLB = new listBox();
			objLB.dataArray = arrCurrency; 
			objLB.id = "selCurrency";
			objLB.blnMergeTextValue = true;
			objLB.firstTextValue = strDDDefValue;
			objLB.blnMergeStyle = "-";
			objLB.blnFirstEmpty = true;
			objLB.fillListBox();
			document.getElementById('selCurrency').disabled = true;
		}

		
	
	
	function defaultSetYearDay(){
		FillCalenderDay("selDtRetu", getFieldByID("selYrRetu").options[1].value);
	}
	
	function selYrDeptOnChange(){
		FillCalenderDay("selDtDept", getValue("selYrDept"));
		setField("selDtDept", strSeleDDay);
	}
	
	function selYrRetuOnChange(){
		FillCalenderDay("selDtRetu", getValue("selYrRetu"));
		setField("selDtRetu", strSeleRDay);
	}
	
	function selDtDeptOnChange(){
		strSeleDDay = getValue("selDtDept");
	}

	function selDtRetuOnChange(){
		strSeleRDay = getValue("selDtRetu");
	}	
	
	function selToLocOnChange(){
		var objDF 	 = getFieldByID("selFromLoc");
		var objDT = getFieldByID("selToLoc");
		if (getText("selToLoc") == strDash || getValue("selToLoc") == ""){
			getFieldByID("selToLoc").options[0].selected = true;
			document.getElementById('selCurrency').disabled = true;
			if(objDF.value == ""){
				objDT.length = 0;
				objDF.length = 0;
				firstSelected = null;
				var hubs = shjHub+","+maHub;
				buildDestList(objDF, hubs, strFromHD);
				buildDestList(objDT, hubs, strToHD);
			}

			return ;			
		}
		if(firstSelected == null)
			firstSelected = objDT;
		var selValueDF = getValue("selFromLoc");
		var selValueDT = getValue("selToLoc");
		var hubDT = arrAirports[selValueDT][2];
		var hubDF = "NULL";
		if(selValueDF != "")
			hubDF = arrAirports[selValueDF][2];
		if(firstSelected == objDT){	
			objDF.length = 0;
			buildDestList(objDF, hubDT, strFromHD);
		}
		if(hubDF.match(hubDT) != null || hubDT.match(hubDF) != null){
			setField("selFromLoc", selValueDF);
		}
		selValueDF = getValue("selFromLoc");
		if(selValueDF != ""){
			hubDF = arrAirports[selValueDF][2];
			var hub = hubDT;
			if(hubDT.match(maHub)!=null && hubDT.match(shjHub)!=null)
				hub = hubDF;
			if(hub.match(shjHub) != null){
				buildCurrencyList(arrCurrency);
			}else if(hub.match(maHub) != null){
				buildCurrencyList(arrCurrencyMA);
			}
			document.getElementById('selCurrency').disabled = false;
		}else
			document.getElementById('selCurrency').disabled = true;
		
	}
	
	function selFromLocOnChange(){
		var objDT 	 = getFieldByID("selToLoc");
		var objDF = getFieldByID("selFromLoc");
		if (getText("selFromLoc") == strDash || getValue("selFromLoc") == ""){
			getFieldByID("selFromLoc").options[0].selected = true;
			document.getElementById('selCurrency').disabled = true;
			if(objDT.value == ""){
				firstSelected = null;
				var hubs = shjHub+","+maHub;
				objDT.length = 0;
				objDF.length = 0;
				buildDestList(objDF, hubs, strFromHD);
				buildDestList(objDT, hubs, strToHD);
			}

			return;			
		}
		if(firstSelected == null)
			firstSelected = objDF;
		var selValueDF = getValue("selFromLoc");
		var selValueDT = getValue("selToLoc");
		var hubDF = arrAirports[selValueDF][2];
		var hubDT="NULL";
		if(selValueDT != "")
			hubDT = arrAirports[selValueDT][2];
		if(firstSelected == objDF){	
			objDT.length = 0;
			buildDestList(objDT, hubDF, strToHD);
		}
		if(hubDF.match(hubDT) != null || hubDT.match(hubDF) != null){
			setField("selToLoc", selValueDT);		
		}
		selValueDT = getValue("selToLoc");
		if(selValueDT != ""){
			hubDT = arrAirports[selValueDT][2];
			var hub = hubDF;
			if(hubDF.match(maHub)!=null && hubDF.match(shjHub)!=null)
				hub = hubDT;
			if(hub.match(shjHub) != null){
				buildCurrencyList(arrCurrency);
			}else if(hub.match(maHub) != null){
				buildCurrencyList(arrCurrencyMA);
			}
			document.getElementById('selCurrency').disabled = false;
		}else
			document.getElementById('selCurrency').disabled = true;
		
	}
	// schedule page
	function selToLoc2OnChange(){
		var objDF2 	 = getFieldByID("selFromLoc2");
		var objDT2 = getFieldByID("selToLoc2");
		if (getText("selToLoc2") == strDash){
			getFieldByID("selToLoc2").options[0].selected = true;
			if(objDF2.value == ""){
				objDF2.length = 0;
				objDT2.length = 0;
				firstSelected2 = null;
				var hubs = shjHub+","+maHub;
				buildDestList(objDF2, hubs, strHD);
				buildDestList(objD2T, hubs, strHD);
			}
			return ;			
		}
		if(firstSelected2 == null)
			firstSelected2 = objDT2;
		var selValueDF2 = getValue("selFromLoc2");
		var selValueDT2 = getValue("selToLoc2");
		var hubDT2 = arrAirports[selValueDT2][2];
		var hubDF2 = "NULL";
		if(selValueDF2 != "")
			hubDF2 = arrAirports[selValueDF2][2];
		if(firstSelected2 == objDT2){	
			objDF2.length = 0;
			buildDestList(objDF2, hubDT2, strHD);
		}
		if(hubDF2.match(hubDT2) != null || hubDT2.match(hubDF2) != null){
			setField("selFromLoc2", selValueDF2);
		}		
	}
	
	function selFromLoc2OnChange(){
		var objDT2	 = getFieldByID("selToLoc2");
		var objDF2 = getFieldByID("selFromLoc2");
		if (getText("selFromLoc2") == strDash){
			getFieldByID("selFromLoc2").options[0].selected = true;
			if(objDT2.value == ""){
				firstSelected2 = null;
				var hubs = shjHub+","+maHub;
				objDT2.length = 0;
				objDF2.length = 0;
				buildDestList(objDF2, hubs, strHD);
				buildDestList(objDT2, hubs, strHD);
			}
			return;			
		}
		if(firstSelected2 == null)
			firstSelected2 = objDF2;
		var selValueDF2 = getValue("selFromLoc2");
		var selValueDT2 = getValue("selToLoc2");
		var hubDF2 = arrAirports[selValueDF2][2];
		var hubDT2="NULL";
		if(selValueDT2 != "")
			hubDT2 = arrAirports[selValueDT2][2];
		if(firstSelected2 == objDF2){	
			objDT2.length = 0;
			buildDestList(objDT2, hubDF2, strHD);
		}
		if(hubDF2.match(hubDT2) != null || hubDT2.match(hubDF2) != null){
			setField("selToLoc2", selValueDT2);		
		}
						
	}


	function setDate(strDate, strID){
		var arrStrValue = strDate.split("/");
		switch (strID){
			case "0" : 
				setField("selYrDept", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrDeptOnChange();
				setField("selDtDept", arrStrValue[0]);
				break ;
			case "1" : 
				setField("selYrRetu", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrRetuOnChange();
				setField("selDtRetu", arrStrValue[0]);
				break ;
		}
	}
	
	function chkReturnTrip_click(){
		if (getFieldByID("chkReturnTrip").checked){
			setField("selRVariance", intRDaysDef);
			Disable("selDtRetu", false);
			Disable("selYrRetu", false);
			Disable("selRVariance", false);
		}else{
			setField("selRVariance", "0");
			setField("selDtRetu", "");
			setField("selYrRetu", "");
			Disable("selDtRetu", true);
			Disable("selYrRetu", true);
			Disable("selRVariance", true);
			defaultSetYearDay();
		}
	}
	
	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 0 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.currentDate = strSysDate;
		switch (strID){
			case 0 : 
				if (getValue("selDtDept") != "" && getValue("selYrDept")){
					objCal1.currentDate = getValue("selDtDept") + "/" + getValue("selYrDept");
				}
				break;
			case 1 : 
			if (getFieldByID("chkReturnTrip").checked){
				if (getValue("selDtRetu") != "" && getValue("selYrRetu")){
					objCal1.currentDate = getValue("selDtRetu") + "/" + getValue("selYrRetu");
				}
			}else{
				return;
			}
			break;			
		}		
		objCal1.showCalendar(objEvent);
	}
	
	// --------------------- Maximum Passengers
	function selAdults_onChange(){
		var arrInfants = new Array();
		var intAdults = Number(getValue("selAdults"))// + Number(getValue("selChild"));
		var strCInfants = getValue("selInfants");
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrInfants; 
		objLB.textIndex = "0";
		objLB.id = "selInfants";
		objLB.fillListBox();
		
		setField("selInfants",  strCInfants);
	}
	
	function flightSearchBtnOnClick(intID){
		var hub = "G9";
		switch (intID){
			case 0 : 
				if (clientValidateFS()){
					var objFrm = getFieldByID("form1");
					objFrm.target = "_top";
					var selValueDF = getValue("selFromLoc");
					var selValueDT = getValue("selToLoc");
					if(arrAirports[selValueDF][2] == arrAirports[selValueDT][2]){
						hub = arrAirports[selValueDF][2];
					}else{
						if(arrAirports[selValueDF][2].length > arrAirports[selValueDT][2].length)
							hub = arrAirports[selValueDT][2];
						else
							hub = arrAirports[selValueDF][2];
						
					}
					if(hub == shjHub)
						objFrm.action = G9URL+"showReservation.action";
					else
						objFrm.action = MAURL+"showReservation.action";
					objFrm.submit();
				}
				break;
			case 1 : 
				var strPostData = "EN^RE"
				setField("hdnParamData", strPostData);
				var objFrm = getFieldByID("form1");
				objFrm.target = "_top";
				objFrm.submit();
				break;
			case 2 :
				var strPostData = "EN^SI"
				setField("hdnParamData", strPostData);			
				var objFrm = getFieldByID("form1");
				objFrm.target = "_top";
				objFrm.submit();
				break;
		}
	}	
	
	
	function clientValidateFS(){
		if (getValue("selFromLoc") == ""){
			alert(arrError["ERR001"]);		
			getFieldByID("selFromLoc").focus();		
			return false;
		}
		
		if (getValue("selToLoc") == ""){
			alert(arrError["ERR002"]);
			getFieldByID("selToLoc").focus();
			return false;
		}		
		
		if (getValue("selCurrency") == ""){
			alert(arrError["ERR006"]);
			getFieldByID("selCurrency").focus();
			return false;
		}				
		
		if (getValue("selFromLoc") == getValue("selToLoc")){
			alert(arrError["ERR003"]);
			getFieldByID("selToLoc").focus();			
			return false;
		}
		
		if ((getValue("selDtDept") == "") || (getValue("selYrDept") == "")){
			alert(arrError["ERR007"]); 	
			getFieldByID("selDtDept").focus();		
			return false;
		}else{
			if (!CheckDates(strSysDate, getValue("selDtDept") + "/" + getValue("selYrDept"))){
				alert(arrError["ERR009"] + strSysDate + ".");
				getFieldByID("selDtDept").focus();	
				return false;
			}			
		}
		
		if (getFieldByID("chkReturnTrip").checked){
			if ((getValue("selDtRetu") == "") || (getValue("selYrRetu") == "")){
				alert(arrError["ERR008"]); 	
				getFieldByID("selDtRetu").focus();		
				return false;
			}
		
			if (!CheckDates(getValue("selDtDept") + "/" + getValue("selYrDept"), getValue("selDtRetu") + "/" + getValue("selYrRetu"))){
				alert(arrError["ERR004"]);
				getFieldByID("selDtRetu").focus();
				return false;
			}
		}
		
		strRDate = "";
		blnReturn = false;
		if (getValue("selDtRetu") != "" && getValue("selYrRetu") != ""){
			strRDate	= getValue("selDtRetu") + "/" + getValue("selYrRetu");
		}
		if (getFieldByID("chkReturnTrip").checked){blnReturn	= true;}
		var selValueDF = getValue("selFromLoc");
		var selValueDT = getValue("selToLoc");
				
		var strPostData = "EN^" + 
						  "AS^" +
					   arrAirports[selValueDF][0] + "^" +
					   arrAirports[selValueDT][0] + "^" +
					   getValue("selDtDept") + "/" + getValue("selYrDept") + "^" +
					   strRDate + "^" +
					   getValue("selDVariance") + "^" +
					   getValue("selRVariance") + "^" +
					   getValue("selCurrency") + "^" +
					   getValue("selAdults") + "^" +
					   getValue("selChild") + "^" +
					   getValue("selInfants") + "^" +
					   arrAirports[selValueDF][1] + "^" +
					   arrAirports[selValueDT][1] + "^" +
					   blnReturn;

		setField("hdnParamData", strPostData);
		return true;
	}
	
	function defaultDataLoad(){
		var dtDepDate = addDays(dtSysDate, Number(strDefDayDept));
		strDDate = dateChk(dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear());
		strSeleDDay = dtDepDate.getDate();
		
		var dtRetuDate = "";
		strRDate = "";
		strSeleRDay = "  /  /    ";
		if (String(strDefDayRetu) != ""){
			var dtRetuDate = addDays(dtSysDate, Number(strDefDayRetu));
			strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
			strSeleRDay = dtRetuDate.getDate();
			setField("chkReturnTrip", true);
		}
		
		setField("selDVariance", strDDays); 
		setField("selRVariance", strRDays);
		chkReturnTrip_click();
	}
	
	function dateOnBlur(strID, objC){
		var blnDateEntered = false;
		if (objC.value != ""){blnDateEntered = true;}
		dateChk(strID);
		if (objC.value == "" && blnDateEntered){
			alert(arrError["ERR005"]); 
		}
	}
	
	function cacheData(){
		//setField("selFromLoc", strFrom);
		//setField("selToLoc", strTo);
		
		var arrDtDept = strDDate.split("/");
		setField("selYrDept", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrDeptOnChange();
		setField("selDtDept", arrDtDept[0]);
		
		setField("selYrRetu", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrRetuOnChange();
		setField("selDtRetu", arrDtDept[0]);
		
		if (getValue("selDtRetu") == ""){
			defaultSetYearDay();
		}
				
		setField("selDVariance", strDDays);
		setField("selRVariance", strRDays);
		//setField("selCurrency", strSelCurr);
		
		getFieldByID("selFromLoc").focus();

	}
	
	// ------------------------ 
	function flightSearchOnLoad(){
		buildDropDowns();
		
		var arrAdults = new Array();
		var arrChild  = new Array();
		arrChild[0] = new Array();
		arrChild[0][0] = "0";
		for (var i = 1 ; i <= intMaxAdults ; i++){
			arrAdults[i - 1] = new Array();
			arrAdults[i - 1][0] = i;
			
			arrChild[i] = new Array();
			arrChild[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrAdults; 
		objLB.textIndex = "0";
		objLB.id = "selAdults";
		objLB.fillListBox();
		
		objLB = new listBox();
		objLB.dataArray = arrChild; 
		objLB.textIndex = "0";
		objLB.id = "selChild";
		objLB.fillListBox();
		selAdults_onChange();
		
		// ------------------------ 
		var arrDays = new Array();
		for (var i = 0 ; i <= intDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		
		var objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selDVariance";
		objLB2.fillListBox();
		
		arrDays.length = 0
		for (var i = 0 ; i <= intRDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selRVariance";
		objLB2.fillListBox();

		Disable("selDtRetu", true);
		Disable("selYrRetu", true);
		Disable("selRVariance", true);
		
		defaultDataLoad()
		cacheData();

	}	
	flightSearchOnLoad();
	function myOnload(){
		setField("selFromLoc", "");
		setField("selToLoc", "");
	}
	
	
	// ---------------- Calendar
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.borderColor = "#FE0000";
	objCal1.headerTextColor = "white";
	objCal1.noYears = "1";
	objCal1.blnArabic = false;
	objCal1.disableUpto = strSysDate;
	objCal1.disableFrom = getDaysInMonth(intEndMonth, intEndYear) + "/" + intEndMonth + "/" + intEndYear;
	objCal1.buildCalendar();
	// Special Flight Setting 

	if(document.getElementById('specialFltFromDate') !=null){
		setField("specialFltFromDate", ""); //starting date of format mm/dd/yyyy
		setField("specialFltToDate", ""); //Ending date	of format mm/dd/yyyy	
		setField("specialFltNo", ""); //Flights number: it should be comma seperated and without G9 (for ex 2107,2121)
		setField("specialFltDst", ""); //3 letters Flight destination (for ex. JED)  	 
	}

//-->