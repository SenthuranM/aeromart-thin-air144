<%@ page language="java"%>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title><%=AppParamUtil.getDefaultCarrier()%></title>
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
<meta name=ProgId content=VisualStudio.HTML>
<meta name=Originator content="Microsoft Visual Studio .NET 7.1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<%! 
String url = AppParamUtil.getSecureIBEUrl() + "showReservation.action";
String kioskUrl = AppParamUtil.getSecureIBEUrl()+"showKiosk.action"; 
//String kioskUrl = AppParamUtil.getSecureIBEUrl()+"showLoadPage!kioskLogin.action";
String scheduleUrl =  AppParamUtil.getNonsecureIBEUrl()+"js/testSchedule.jsp";
String agentsURL =  AppParamUtil.getNonsecureIBEUrl()+"showAgentsInfo.action";
String dummyAgentURL = "retrieveAgentDummy_GA.jsp";
%>
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../js/common/commonLoad.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../js/SSTbl.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	

<style type="text/css">
.clFont {
        font-family: Verdana,Arial,Helvetica,sans-serif;;
		color: #737373;
		font-size:9px;
      }

	
.ssThinBorder{
	BORDER-RIGHT: #a6a6a6 1px solid; 
	BORDER-TOP: #a6a6a6 1px solid; 
	BORDER-LEFT: #a6a6a6 1px solid; 
	BORDER-BOTTOM: #a6a6a6 1px solid; 
	}

.ssThinBG{
	background-color:#EFEFEF;
	}
.ssHref:link
{
	font-family:Verdana,Arial,Helvetica,sans-serif;
	font-size:12px;
	color:Black;
}	
.ssHref:HOVER
{
	color:Red;
	text-decoration:underline;}

.ssThinBGSltd
{
	background-color:#CFCFCF;
	}	
	body{
	overflow-x:hidden;
}
</style>
    

</head>
<body style="background-image: url(../images/Testing_no_cache.jpg);">
<!-- ClickTale Top part --> 
<script type="text/javascript"> 
	var WRInitTime=(new Date()).getTime(); 
</script> 
<!-- ClickTale end of Top part --> 
<table width="100%" cellpadding="2" cellspacing="0" border="0" align="top">
	<tr>
		<td colspan="3" width="100%" style="border-left-width: 1; border-right-width: 1; border-top-width: 1; border-bottom: 1px solid #808080">
			<font class="clFont">Server Time : <span id="spnClock" name ="spnClock"></span></font>
		</td>
	</tr>
</table>

<table width="775" cellpadding="2" cellspacing="0" border="0" align="center">
	<tr>
		<td width="180" valign="top">
			<!-- Flight Search Goes Here -->
			<table width='100%' border='0' cellpadding='0' cellspacing='2' ID='Table7' bgcolor="white" align="top">
				<tr>
					<td colspan='4'><font><b>Book Flights</b></font></td>
					<td> <font><b><a href='javascript:void(0)' onclick='flightSearchBtnOnClick(7)'>MultiCity Search</a></b></font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<!-- <input type = 'hidden' id='selFromLoc'    NAME='selFromLoc' onChange="selFromLocOnChange()">
							<span id='spnOrg' style="z-index: 4"></span> <br>
							 -->
						<select id='selFromLoc' size='1' style='width:163px;' NAME='selFromLoc' onChange="selFromLocOnChange()">
										<option></option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<!-- <input type = 'hidden'  id='selToLoc'   NAME='selToLoc' onChange="selToLocOnChange()">
						<span id='spnDes' style="z-index: 2"></span> -->
						
						<select id='selToLoc' size='1' style='width:163px;' NAME='selToLoc' onChange="selToLocOnChange()">
										<option></option>
						</select>
					</td>

				</tr>
				<tr>
					<td colspan='4'><font>Departing Date</font></td>
				</tr>
				<tr>
					<td colspan='1'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
							<tr>

								<td width="95%">
									<select id='selDtDept' size='1' style='width:50px;' NAME='selDtDept' onChange="selDtDeptOnChange()">
										<option></option>
									</select>
									<select id='selYrDept' size='1' style='width:100px;' NAME='selYrDept' onChange="selYrDeptOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>

									<a href='javascript:void(0)' onclick='javascript:loadIBECal(0,event);'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selDVariance' size='1' style='width:50px;' NAME='selDVariance' title='No of days before or after departure date'>

						</select>
						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
				<td>
				<table>
				<tr>
					<td colspan='1'><font>Returning Date</font></td>
					<td align='right'>

						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td align='right'>
									<font>&nbsp;Return Trip&nbsp;</font>
								</td>
								<td align='right'>
									<input type="checkbox" id="chkReturnTrip" name="chkReturnTrip" onClick="chkReturnTrip_click()" class="noBorder">
								</td>

							</tr>
						</table>
					</td>
					</tr>
				</table>
				</td>
				</tr>
				<tr>
					<td colspan='1'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table3">
							<tr>
								<td width="95%">

									<select id='selDtRetu' size='1' style='width:50px;' NAME='selDtRetu' onChange="selDtRetuOnChange()">
										<option></option>
									</select>
									<select id='selYrRetu' size='1' style='width:100px;' NAME='selYrRetu' onChange="selYrRetuOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>
									<a href='javascript:void(0)' onclick='javascript:loadIBECal(1,event);'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>

								</td>
							</tr>
						</table>
					</td>
				</tr>		
				<tr>
					<td colspan='4'>
						<select id='selRVariance' size='1' style='width:50px;' NAME='selRVariance' title='No of days before or after return date'>
						</select>

						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' ID='Table8'>
							<tr>

								<td width='30%'><font>Adults</font></td>
								<td width='33%'><font>Children</font></td>
								<td width='37%'><font>Infants&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
							</tr>
							<tr>
								<td><font>&nbsp;</font></td>
								<td><font class='fntsmall'>(2-14 years)</font></td>

								<td><font class='fntsmall'>(under 2 years)</font></td>
							</tr>
							<tr>
								<td>
									<select id='selAdults' size='1' style='width:45px;' NAME='selAdults' onChange='selAdults_onChange()'>
									</select>														
								</td>			
								<td>
									<select id='selChild' size='1' style='width:50px;' NAME='selChild'>

									</select>														
								</td>
								<td align='right'>
									<select id='selInfants' size='1' style='width:65px;' NAME='selInfants'>
									</select>														
								</td>			
							</tr>	
						</table>
					</td>
				</tr>
				<tr>

					<td colspan='4'>
						<a href="#" onClick="faqClick()"><font class='fntsmall'><u>Important information on Child and Infant bookings</u></font></a>
					</td>
				</tr>
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				</tr>
				<tr id="trClass2">
					<td colspan='4'><font>Class</font></td>
				</tr>
				<tr id="trClass3">
						<td colspan='4'>
						<select id='selCOS' size='1' style='width:175px;' NAME='selCOS'>
							<option value="Y">Economy Class</option>
							<option value="C">Business Class</option>
						</select>
					</td>
				</tr>	
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				<tr>

					<td colspan='4'><font>Currency</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selCurrency' size='1' style='width:175px;' NAME='selCurrency'>
						</select>
					</td>
				</tr>
				
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				<tr>

					<td colspan='4'><font>Origin Country</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selOriginCountry' size='1' style='width:175px;' NAME='selOriginCountry'>
						</select>
					</td>
				</tr>
				
				<tr>

					<td colspan='4'><font>Promo Code</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<input type="text" id='txtPromoCode' size='6' style='width:175px;' NAME='txtPromoCode'>
					</td>
				</tr>

				<tr>
					<td colspan='4' align='right'>
						<input type='button' id='btnSearch' class='Button' value='Search' title='Click here to Search for flights' NAME='btnSearch' onclick='flightSearchBtnOnClick(0)'>
					</td>
				</tr>
				<tr>
		<td>
			<input type="button" id="btn1" value="Register" class='Button' onclick='flightSearchBtnOnClick(1)'>
			<input type="button" id="btnMobileReg" value="Register Mobile" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(8)'>
			<input type="button" id="btnAgent" value="RegisterAgent" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(3)'>
			<input type="button" id="btnCorporate" value="RegisterCorporate" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(5)'>
			<input type="button" id="btnGiftVoucher" value="Gift Voucher" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(8)'>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="Button1" value="Sign In" class='Button' onclick='flightSearchBtnOnClick(2)'>
			<input type="button" id="Button2" value="Manage Booking Link" class='Button ButtonLarge' onclick='flightSearchBtnOnClick(4)'>			
		</td>
		<td>
			 
		</td>
	</tr>
	<tr>
		<td>
			<font>Language</font>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<select id='selLangunage' size='1' style='width:175px;' NAME='selCurrency'>
				<option value="EN">English</option>
				<option value="SI">Sinhalese</option>
				<option value="AR">Arabic</option>
				<option value="RU">Russian</option>
				<option value="FR">French</option>
				<option value="ES">Spanish</option>	
				<option value="IT">Italian</option>	
				<option value="TR">Turkish</option>
                <option value="ZH">Chinese</option>
				<option value="DE">German(not support)</option>
				<option value="FA">Farsi</option>
			</select>
		</td>
		<td>
		</td>
	</tr>
	
	<tr>
		<td>
			<font>Origin Static Website</font>
		</td>
		<td>
		</td>
	</tr>
	
	<tr>
		<td>
			<select id='selOriginCarrier' size='1' style='width:175px;' NAME='selOriginCarrier'>
				<option value="G9">airarabia.com</option>
				<option value="0Y">flyyeti.com</option>
			</select>
		</td>
		<td>
		<br>
		</td>
		
	</tr>
	<tr>
		<td>
		 
		 
		</td>
	</tr>
				
			</table>
			
		</td>
		<td rowspan="3" valign="top" halign="left"><table width="200px"><tr>

			<td>
			<a href="<%=kioskUrl%>"><font class="fntBold fntRed">Test Kiosk</font></a><br>
			<a href="javascript:updateTitle()"><font class="fntBold">Set Time to Title</font></a><br>
			<a href="<%=dummyAgentURL%>"><font class="fntBold">Test Agent Retrieval</font></a><br>
			</td>	 	
			</tr>		 
			 
			<tr>

			<tr><td><br><font class="fntBold">Configurations</font></td></tr>
				<tr> 
					<td align="left">
						<font>Load Configs from DB : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isUsingDBConfigs()%></font>
					</td></tr>
				<tr> 
						<td align="left">
							<font>Default PGW Currency: </font>
						</td>
						<td align="right">
							<font><%=AppParamUtil.getDefaultPGW()%></font>
						</td></tr>
				<tr> 
					<td align="left">
						<font>Apply CC Charge: </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isApplyCreditCardCharge()%></font>
					</td></tr>
					<tr> 
					<td align="left">
						<font>Show Travel Guard : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isTravelGuardEnabled()%></font>
					</td></tr>
					<tr> 
					<td align="left">
						<font>Is In Dev Mode : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isInDevMode()%></font>
					</td></tr>
					
					
					<tr>&nbsp;</tr>
					<tr>&nbsp;</tr>
					<tr>&nbsp;</tr>
					<tr>&nbsp;</tr>
					<tr>&nbsp;</tr>
					<tr>
					<td align='right'>
						<font>&nbsp; Hub Transit Times&nbsp;</font>
					</td>
					<td>
						<input type="checkbox" id="chkHubTransit" name="chkHubTransit" onClick="chkHubTransit_click()" class="noBorder"  disabled>
					</td>
					</tr>
					<tr>
					<td>
					<input type="text" id='txtHubCode' size='3' style='width:50px;' NAME='txtHubCode' disabled>
					</td>
					<td>
					<font>&nbsp; Outbound Transit Time &nbsp;</font>
					<input type="text" id='txtOutboundTransitTime' size='6' style='width:50px;' NAME='txtOutboundTransitTime' maxlength='2'  disabled>
					</td>
					<td>
					<font>&nbsp; Inbound Transit Time &nbsp;</font>
					<input type="text" id='txtInboundTransitTime' size='6' style='width:50px;' NAME='txtInboundTransitTime' maxlength='2' disabled>
					</td>
					<td>
						<font>(in Days)&nbsp;</font>
					</td>
					</tr>
					
			 
		</table>
		</td>
		<td valign="top" align="right"> 
			<input type="hidden" name="schurl" id="schurl" value="<%=AppParamUtil.getNonsecureIBEUrl()%>">
			<input type="hidden" id="specialFltFromDate" name="specialFltFromDate" value="">
			<input type="hidden" id="specialFltToDate" name="specialFltToDate" value="">
			<input type="hidden" id="specialFltNo" name="specialFltNo" value="">
			<input type="hidden" id="specialFltDst" name="specialFltDst" value="JED">
		<table border="0" width="300" >
			<tr>
			<td  valign="top" align="right">
			<table border="0" width="300" style="border-collapse: collapse; border-style: inset; border-width: 1" bordercolor="#111111" cellpadding="7" cellspacing="5">
						 <tr>
        				<td width="16%" align="left" colspan="2" style="padding-right:3px;border:0;vertical-align:middle; "><font class="fntBold fntRed">Test Schedule</font></td>
      				</tr>
					<tr>
        				<td width="16%" align="left" style="padding-right:3px;border:0;vertical-align:middle; "><font>Round Trip:</font></td>
	
      			 		 <td width="91%" style="padding-right:3px;border:0; "><input type="checkbox" id="chkReturnTrip2" name="chkReturnTrip2"  class="noBorder"></td>
      				</tr>	
					 <tr>
				        <td width="5%" style="padding-right:3px;border:0; "><font>From*</font></td>
				
				        <!-- <td width="20%" style="padding-right:3px;border:0; "><select id='selFromLoc2' class="form_destinations" style="width:150px;" name='selFromLoc2' onChange='selFromLoc2OnChange()'> -->
				        <td width="20%" style="padding-right:3px;border:0; "><select id='selFromLoc2' class="form_destinations" style="width:150px;" name='selFromLoc2'>
				        </select></td>
 						<td style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>				        		      		
				    </tr>				        
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>To*</font></td>
				        <!-- <td style="padding-right:3px;border:0; "><select id='selToLoc2' class="form_destinations" style="width:150px;" name='selToLoc2' onChange='selToLoc2OnChange()'> -->				        
				        <td style="padding-right:3px;border:0; "><select id='selToLoc2' class="form_destinations" style="width:150px;" name='selToLoc2'>
				
				        </select></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      		</tr>
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>Month*</font></td>
				        <td style="padding-right:3px;border:0;width:180px "><select id='selYrDept2' class="form_date" style="width:150px;" name='selYrDept2' >
				        </select></td>
				
				        <td style="padding-right:3px;border:0; " align="left"><input name="go" type="button" id="go" value="Search" class='Button' onClick="onGo()"></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      </tr>
		      <tr>
		      		<td colspan="3"><iframe  id="sch_container" name="sch_container" width="99%" height="200px" src="" scrolling="auto" frameborder="0" style="width:99%;height:200px;"></iframe></td>				
		      </tr>
		      </table>
		      </td>
		      </tr>
		      <!-- 
		      <tr> 
		      <td valign="top" align="right"> 
		 		<table border="0" width="300" style="border-collapse: collapse; border-style: inset; border-width: 1" bordercolor="#111111">
					 <tr>
        				<td width="16%" align="left" colspan="2" style="padding-right:3px;border:0;vertical-align:middle; "><font class="fntBold fntRed">Test Agents</font></td>
      				</tr>
						
					 <tr>
				        <td width="5%" style="padding-right:3px;border:0; "><font>Country*</font></td>
				
				        <td width="20%" style="padding-right:3px;border:0; "><select id='selCountry' class="form_destinations" style="width:150px;" name='selCountry' onChange='selCountryOnChange()'>
				        </select></td>
				        <td width="12%" style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td width="63%" style="padding-right:3px;border:0; ">&nbsp;</td>
		      		</tr>
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>City*</font></td>
				        <td style="padding-right:3px;border:0; "><select id='selStation' class="form_destinations" style="width:150px;" name='selStation'>
				
				        </select></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      		</tr>
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>Agent Type*</font></td>
				        <td style="padding-right:3px;border:0; "><select id='selAgentType' class="form_destinations" style="width:150px;" name='selAgentType' >
				        <option value="ALL">All</option>
				        <option value="GSASO">Sales Shops and GSA</option>
				        <option value="TA">Travel Agents</option>
				        </select></td>
				
				        <td style="padding-right:3px;border:0; " align="left"><input name="continue" type="button" id="continue" value="Continue" class='Button' onClick="onContinue()"></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      </tr>
		      <tr>
		      		<td colspan="4"><iframe  id="sch_agents" name="sch_agents" width="99%" height="350" src=""  scrolling="yes" frameborder="0"></iframe></td>				
		      </tr>
		      -->
			</table>
			<br/>
			<br/>
			<table>
				<tr>
					<td>
					<form id="manageBookingWithData" method="post">
					<fieldset>
						<legend style="text-decoration: blink">
						<font style="color: red; font-weight: bold;"> Test Manage booking with External Data <font></legend>
						<table>
							<tr>
								<td><font>PNR </font></td>
								<td width="5">:</td>
								<td width="250" class="alignLeft" align="left"><input
									type="text" name="pnr" id="pnr" /> <font class="mandatory">
								*</font></td>
							</tr>
							<tr>
								<td><font>Last Name </font></td>
								<td width="5">:</td>
								<td class="alignLeft" width="250" align="left"><input
									type="text" style="width: 220px;" name="lastName"
									id="txtLastName" /> <font class="mandatory"> *</font></td>
							</tr>
							<tr>
								<td><font>Departure Date </font></td>
								<td width="5">:</td>
								<td class="alignLeft" width="250" align="left"><input
									type="text" style="width: 80px;" name="dateOfDep"
									id="txtDateOfDep" /><font>(dd/mm/yyyy) </font> <label
									class="mandatory"> *</label> <br />
								</td>
							</tr>
							<tr>
								<td align="center" colspan="3"><input type="button"
									class="Button" value="Continue" title="Continue" tabindex="24"
									id="btnContinue" onclick="flightSearchBtnOnClick(6)" /></td>
							</tr>
						</table>
					</fieldset>
					</form>
					</td>
				</tr>
			</table>
		</td>
		</tr>
		
		</table>
		</td>
		</tr>
<!-- -->		
	

</table>
<form id="form1" method="post" action="../public/showReservation.action">
	<input type="hidden" id="hdnParamData" name="hdnParamData">
	<input type="hidden" id="hdnCarrier" name="hdnCarrier">
	<input type="hidden" id="hdnOriginCountry" name="hdnOriginCountry"/>
	<input type="hidden" id="hdnParamUrl" name="hdnParamUrl" value="<%=url%>">
</form>

<form method="post" name="formRetrieveAgents" id="formRetrieveAgents">
	<input type="hidden" id="hdnMode" name="hdnMode" value="">
	<input type="hidden" id="hdnLanguage" name="hdnLanguage" value="en">
	<input type="hidden" id="hdnurl" name="hdnurl" value="<%=agentsURL%>">
 </form>
	<input type="hidden" id="specialFltFromDate" name="specialFltFromDate" value="">
	<input type="hidden" id="specialFltToDate" name="specialFltToDate" value="">
	<input type="hidden" id="specialFltNo" name="specialFltNo" value="">
	<input type="hidden" id="specialFltDst" name="specialFltDst" value="JED">
	
	<input type="hidden" name="sysDate" id="sysDate" value="<%=AppParamUtil.getSysDate() %>">	
	
	<input type="hidden" name="defaultCarrierCode" id="defaultCarrierCode" value="<%=AppParamUtil.getDefaultCarrier() %>">
	<input type="hidden" name="dynamicOndListEnabled" id="dynamicOndListEnabled" value="<%=AppParamUtil.isDynamicOndListPopulationEnabled() %>">
	<input type="hidden" name="userDefineTransitTimeEnabled" id="userDefineTransitTimeEnabled" value="<%=AppParamUtil.isUserDefineTransitTimeEnabled() %>">
		
<script language="javascript" type="text/javascript"><!--

var arrCurrency = new Array();
var arrError = new Array();
var arrAirports = new Array();

<%@ include file="automationAirportsList.jsp" %>
//End of airport list


arrCurrency[0] = new Array('BHD','Bahrani Dinar'); 
arrCurrency[1] = new Array('EGP','Egyptian Pound'); 
arrCurrency[2] = new Array('INR','Indian Rupee'); 
arrCurrency[3] = new Array('JOD','Jordanian Dinar'); 
arrCurrency[4] = new Array('LBP','Lebanese Pound'); 
arrCurrency[5] = new Array('MAD','Moroccan Dhiram'); 
arrCurrency[6] = new Array('QAR','Qatari Riyal'); 
arrCurrency[7] = new Array('OMR','Rial Omani'); 
arrCurrency[8] = new Array('RUB','Russian Rouble'); 
arrCurrency[9] = new Array('SAR','Saudi Arabian Riyal'); 
arrCurrency[10] = new Array('LKR','Sri Lanka Rupee'); 
arrCurrency[11] = new Array('AED','UAE Dirham'); 
arrCurrency[12] = new Array('USD','US Dollar');
arrCurrency[13] = new Array('EUR','EURO');
arrCurrency[14] = new Array('KWD','Kuwait Dinner');

//--------- Country list to populate agent search & IBE marketing carrier identification --------------------------//
var arrCountry = new Array();
arrCountry[0] = new Array('OL', 'COUNTRY FOR OFFLINE AGENT', 'G9');
arrCountry[1] = new Array('AE', 'UAE', 'G9|3O');
arrCountry[2] = new Array('AF','Afghanistan','G9');
arrCountry[3] = new Array('AM','Armenia','G9'); 	
arrCountry[4] = new Array('BD','Bangladesh','G9'); 	
arrCountry[5] = new Array('BH','Bahrain','G9'); 	
arrCountry[6] = new Array('DJ','Djibouti','G9');
arrCountry[7] = new Array('EG','Egypt','G9'); 	
arrCountry[8] = new Array('FR','France','G9'); 	
arrCountry[9] = new Array('GR','Greece','G9'); 	
arrCountry[10] = new Array('IN','India','G9'); 	
arrCountry[11] = new Array('IR','Iran','G9'); 	
arrCountry[12] = new Array('JO','Jordan','G9'); 	
arrCountry[13] = new Array('KE','Kenya','G9'); 	
arrCountry[14] = new Array('KW','Kuwait','G9');	 	
arrCountry[15] = new Array('KZ','Kazakhstan','G9'); 	
arrCountry[16] = new Array('LB','Lebanon','G9'); 
arrCountry[17] = new Array('LK','Sri Lanka','G9'); 	
arrCountry[18] = new Array('MA','Morocco','G9'); 	
arrCountry[19] = new Array('MV','Maldives','G9'); 	
arrCountry[20] = new Array('MY ','Malaysia','G9');	
arrCountry[21] = new Array('NP','Nepal','G9'); 	
arrCountry[22] = new Array('OM','Oman','G9');
arrCountry[23] = new Array('PK','Pakistan','G9'); 	 	
arrCountry[24] = new Array('PN','Palestine','G9'); 	
arrCountry[25] = new Array('QA','Qatar','G9'); 	
arrCountry[26] = new Array('RU','Russia','G9');	
arrCountry[27] = new Array('SA','Saudi Arabia','G9');
arrCountry[28] = new Array('SD','Sudan','G9'); 	
arrCountry[29] = new Array('SG ','Singapore','G9');	
arrCountry[30] = new Array('SP','Spain','G9');	 	
arrCountry[31] = new Array('SY','Syria','G9'); 	
arrCountry[32] = new Array('TR','Turkey','G9'); 	
arrCountry[33] = new Array('UA','Ukraine','G9'); 	
arrCountry[34] = new Array('YE','Yemen','G9');	
arrCountry[35] = new Array("GE","Georgia", 'G9');	
arrCountry[36] = new Array("ID","Indonesia", 'G9');
arrCountry[37] = new Array("LI","Liechtenstein", 'G9');
arrCountry[38] = new Array("MR","Mauritania", 'G9');	
arrCountry[39] = new Array("MD","Moldova", 'G9');	
arrCountry[40] = new Array("MM","Myanmar", 'G9');	
arrCountry[41] = new Array("NG","Nigeria", 'G9');	
arrCountry[42] = new Array("ST","Sao Tome and Principe", 'G9');
arrCountry[43] = new Array("GB","United Kingdom", 'G9');	
arrCountry[44] = new Array("TZ","Tanzania", 'G9');	
arrCountry[45] = new Array("TM","Turkmenistan", 'G9');	
arrCountry[46] = new Array("AZ","Azerbaijan", 'G9');	
arrCountry[47] = new Array("BS","Bahamas", 'G9');	
arrCountry[48] = new Array("BB","Barbados", 'G9');	
arrCountry[49] = new Array("GA","Gabon", 'G9');	
arrCountry[50] = new Array("CF","Central African Republic", 'G9');
arrCountry[51] = new Array("CL","Chile", 'G9');	
arrCountry[52] = new Array("CU","Cuba", 'G9');	
arrCountry[53] = new Array("DK","Denmark", 'G9');	
arrCountry[54] = new Array("GL","Greenland", 'G9');	
arrCountry[55] = new Array("FO","Faroe Islands", 'G9');	
arrCountry[56] = new Array("EE","Estonia", 'G9');	
arrCountry[57] = new Array("PF","French Polynesia", 'G9');
arrCountry[58] = new Array("WF","Wallis and Futuna Islands", 'G9');	
arrCountry[59] = new Array("GH","Ghana", 'G9');
arrCountry[60] = new Array("GI","Gibraltar", 'G9');	
arrCountry[61] = new Array("GY","Guyana", 'G9');	
arrCountry[62] = new Array("HT","Haiti", 'G9');	
arrCountry[63] = new Array("HN","Honduras", 'G9');	
arrCountry[64] = new Array("LT","Lithuania", 'G9');	
arrCountry[65] = new Array("MU","Mauritius", 'G9');	
arrCountry[66] = new Array("MX","Mexico", 'G9');	
arrCountry[67] = new Array("NA","Namibia");	
arrCountry[68] = new Array("WS","Western Samoa", 'G9');	
arrCountry[69] = new Array("VN","Vietnam", 'G9');	
arrCountry[70] = new Array("ZW","Zimbabwe", 'G9');	
arrCountry[71] = new Array("BT","Bhutan", 'G9');	
arrCountry[72] = new Array("NO","Norway", 'G9');	
arrCountry[73] = new Array("BN","Brunei", 'G9');	
arrCountry[74] = new Array("ML","Mali", 'G9');
arrCountry[75] = new Array("NE","Niger", 'G9');	
arrCountry[76] = new Array("SN","Senegal", 'G9');	
arrCountry[77] = new Array("CI","Ivory Coast", 'G9');	
arrCountry[78] = new Array("BF","Burkina Faso", 'G9');	
arrCountry[79] = new Array("KH","Cambodia", 'G9');	
arrCountry[80] = new Array("PA","Panama", 'G9');	
arrCountry[81] = new Array("SZ","Swaziland", 'G9');	
arrCountry[82] = new Array("TJ","Tajikistan", 'G9');	
arrCountry[83] = new Array("TN","Tunisia", 'G9');	
arrCountry[84] = new Array("VE","Venezuela", 'G9');	
arrCountry[85] = new Array("IT","Italy", 'G9');	
arrCountry[86] = new Array("MC","Monaco", 'G9');	
arrCountry[87] = new Array("AD","Andorra", 'G9');	
arrCountry[88] = new Array("AT","Austria", 'G9');
arrCountry[89] = new Array("BE","Belgium", 'G9');
arrCountry[90] = new Array("FI","Finland", 'G9');
arrCountry[91] = new Array("PT","Portugal", 'G9');
arrCountry[92] = new Array("LU","Luxembourg", 'G9');	
arrCountry[93] = new Array("MQ","Martinique", 'G9');	
arrCountry[94] = new Array("SM","San Marino", 'G9');	
arrCountry[95] = new Array("NL","Netherlands", 'G9');	
arrCountry[96] = new Array("GF","French Guiana", 'G9');
arrCountry[97] = new Array("AR","Argentina", 'G9');	
arrCountry[98] = new Array("KR","Korea, South", 'G9');
arrCountry[99] = new Array("GP","Guadeloupe", 'G9');	
arrCountry[100] = new Array("JM","Jamaica", 'G9');	
arrCountry[101] = new Array("BM","Bermuda", 'G9');	
arrCountry[102] = new Array("MO","Macau", 'G9');	
arrCountry[103] = new Array("CH","Switzerland", 'G9');	
arrCountry[104] = new Array("BO","Bolivia", 'G9');	
arrCountry[105] = new Array("BW","Botswana", 'G9');	
arrCountry[106] = new Array("ET","Ethiopia", 'G9');
arrCountry[107] = new Array("HU","Hungary", 'G9');
arrCountry[108] = new Array("LA","Laos", 'G9');
arrCountry[109] = new Array("MK","Macedonia", 'G9');
arrCountry[110] = new Array("MN","Mongolia", 'G9');
arrCountry[111] = new Array("NI","Nicaragua", 'G9');
arrCountry[112] = new Array("SI","Slovenia", 'G9');
arrCountry[113] = new Array("SE","Sweden", 'G9');
arrCountry[114] = new Array("VU","Vanuatu", 'G9');
arrCountry[115] = new Array("AO","Angola", 'G9');
arrCountry[116] = new Array("AW","Aruba", 'G9');
arrCountry[117] = new Array("NR","Nauru", 'G9');
arrCountry[118] = new Array("KI","Kiribati", 'G9');
arrCountry[119] = new Array("BI","Burundi", 'G9');
arrCountry[120] = new Array("CV","Cape Verde", 'G9');
arrCountry[121] = new Array("KY","Cayman Islands", 'G9');
arrCountry[122] = new Array("CR","Costa Rica", 'G9');
arrCountry[123] = new Array("ER","Eritrea", 'G9');
arrCountry[124] = new Array("FJ","Fiji", 'G9');
arrCountry[125] = new Array("GM","Gambia", 'G9');
arrCountry[126] = new Array("LR","Liberia", 'G9');
arrCountry[127] = new Array("MT","Malta", 'G9');
arrCountry[128] = new Array("SC","Seychelles", 'G9');
arrCountry[129] = new Array("SB","Solomon Islands", 'G9');
arrCountry[130] = new Array("TO","Tonga", 'G9');
arrCountry[131] = new Array("ZM","Zambia", 'G9');
arrCountry[132] = new Array("GU","Guam", 'G9');
arrCountry[133] = new Array("PW","Palau", 'G9');
arrCountry[134] = new Array("EC","Ecuador", 'G9');
arrCountry[135] = new Array("PR","Puerto Rico", 'G9');
arrCountry[136] = new Array("AS","American Samoa", 'G9');
arrCountry[137] = new Array("MH","Marshall Islands", 'G9');
arrCountry[138] = new Array("VG","British Virgin Islands", 'G9');
arrCountry[139] = new Array("MP","Northern Mariana Islands", 'G9');
arrCountry[140] = new Array("BZ","Belize", 'G9');
arrCountry[141] = new Array("NU","Niue", 'G9');
arrCountry[142] = new Array("NZ","New Zealand", 'G9');
arrCountry[143] = new Array("CK","Cook Islands", 'G9');
arrCountry[144] = new Array("CY","Cyprus", 'G9');
arrCountry[145] = new Array("GT","Guatemala", 'G9');
arrCountry[146] = new Array("JP","Japan", 'G9');
arrCountry[147] = new Array("LS","Lesotho", 'G9');
arrCountry[148] = new Array("MW","Malawi", 'G9');
arrCountry[149] = new Array("PG","Papua New Guinea", 'G9');
arrCountry[150] = new Array("PY","Paraguay", 'G9');
arrCountry[151] = new Array("TW","Taiwan", 'G9');
arrCountry[152] = new Array("UZ","Uzbekistan", 'G9');
arrCountry[153] = new Array("BR","Brazil", 'G9');
arrCountry[154] = new Array("BG","Bulgaria", 'G9');
arrCountry[155] = new Array("DO","Dominican Republic", 'G9');
arrCountry[156] = new Array("GW","Guinea", 'G9');
arrCountry[157] = new Array("IS","Iceland", 'G9');
arrCountry[158] = new Array("LV","Latvia", 'G9');
arrCountry[159] = new Array("MZ","Mozambique", 'G9');
arrCountry[160] = new Array("PE","Peru", 'G9');
arrCountry[161] = new Array("SR","Suriname", 'G9');
arrCountry[162] = new Array("UG","Uganda", 'G9');
arrCountry[163] = new Array("GD","Grenada", 'G9');
arrCountry[164] = new Array("AI","Anguilla", 'G9');
arrCountry[165] = new Array("MS","Montserrat", 'G9');
arrCountry[166] = new Array("AG","Antigua and Barbuda", 'G9');
arrCountry[167] = new Array("CO","Colombia", 'G9');
arrCountry[168] = new Array("HR","Croatia", 'G9');
arrCountry[169] = new Array("CZ","Czech Republic", 'G9');
arrCountry[170] = new Array("HK","Hong Kong", 'G9');
arrCountry[171] = new Array("PL","Poland", 'G9');
arrCountry[172] = new Array("RO","Romania", 'G9');
arrCountry[173] = new Array("RW","Rwanda", 'G9');
arrCountry[174] = new Array("SL","Sierra Leone", 'G9');
arrCountry[175] = new Array("UY","Uruguay", 'G9');
arrCountry[176] = new Array("AU","Australia", 'G9');
arrCountry[177] = new Array("CA","Canada", 'G9');
arrCountry[178] = new Array("CN","China", 'G9');
arrCountry[179] = new Array("DE","Germany", 'G9');
arrCountry[180] = new Array("DZ","Algeria", 'G9');
arrCountry[181] = new Array("IE","Ireland", 'G9');
arrCountry[182] = new Array("LY","Libya", 'G9');
arrCountry[183] = new Array("NB","Northern Ireland", 'G9');
arrCountry[184] = new Array("PH","Philippines", 'G9');
arrCountry[185] = new Array("SO","Somalia", 'G9');
arrCountry[186] = new Array("SV","El Salvador", 'G9');
arrCountry[187] = new Array("TH","Thailand", 'G9');
arrCountry[188] = new Array("UK","United Kingdom", 'G9');
arrCountry[189] = new Array("US","United States", 'G9');
arrCountry[190] = new Array("ZA","South Africa", 'G9');
arrCountry[191] = new Array("IQ","Iraq", 'G9');
arrCountry[192] = new Array("OT","Others", 'G9');
arrCountry[193] = new Array("BA","Bank Misr", 'G9');
arrCountry[194] = new Array("LC","Saint Lucia", 'G9');
	
//----------- End of country list -----------------------------//

//----------------- Station List to populate agent search screen -------------- //
var arrStation = new Array();
arrStation[0] = new Array ('TBD ', 'TBD', 'AE', 'G9');
arrStation[1] = new Array ('OLA', 'STATION FOR OFFLINE AGENTS', 'OL', 'G9|3O');
arrStation[2] = new Array ('DXB', 'Dubai', 'AE', 'G9|3O');
arrStation[3] = new Array ('UAQ', 'Umm Al Qaiwan', 'AE', 'G9|3O'); 	              
arrStation[4] = new Array ('AAN', 'Al Ain', 'AE', 'G9|3O'); 	
arrStation[5] = new Array ('AJM', 'Ajman', 'AE', 'G9|3O'); 	
arrStation[6] = new Array ('FJR', 'Fujairah', 'AE', 'G9|3O'); 	
arrStation[7] = new Array ('AUH', 'Abu Dhabi', 'AE', 'G9|3O'); 	 
arrStation[8] = new Array ('SHJ', 'Sharjah', 'AE', 'G9|3O'); 	
arrStation[9] = new Array ('RKT', 'Ras Al Khaimah', 'AE', 'G9|3O'); 	
arrStation[10] = new Array ('KBL', 'Kabul', 'AF', 'G9'); 	                      	
arrStation[11] = new Array ('EVN', 'Yerevan', 'AM', 'G9'); 	                    	
arrStation[12] = new Array ('DAC', 'DHAKA', 'BD', 'G9'); 	                      	 
arrStation[13] = new Array ('CGP', 'Chittagong', 'BD', 'G9'); 	                 	
arrStation[14] = new Array ('BAH', 'Bahrain', 'BH', 'G9'); 	                    	
arrStation[15] = new Array ('JIB', 'DJibouti', 'DJ', 'G9');  	                   	
arrStation[16] = new Array ('SSH', 'Sharm El Sheik', 'EG', 'G9'); 	             	 
arrStation[17] = new Array ('CAI', 'Cairo', 'EG', 'G9'); 	
arrStation[18] = new Array ('ATZ', 'Assiut', 'EG', 'G9|3O'); 	
arrStation[19] = new Array ('ALY', 'Alexandria', 'EG', 'G9|3O'); 	 
arrStation[20] = new Array ('CMB', 'Colombo', 'LK', 'G9');
arrStation[21] = new Array ('CMN', 'Casablanca', 'MA', 'G9|3O');
arrStation[22] = new Array ('MLE', 'Maldives', 'MV', 'G9');  	                   	 	
arrStation[23] = new Array ('KUL', 'Kuala Lumpur', 'MY', 'G9|3O'); 	               	 	
arrStation[24] = new Array ('KTM', 'Nepal', 'NP', 'G9|3O');  	                     		                      	
arrStation[25] = new Array ('MCT', 'Muscat', 'OM', 'G9|3O');
arrStation[26] = new Array ('KHI', 'Karachi', 'PK', 'G9|3O'); 	                    	
arrStation[27] = new Array ('PEW', 'Peshawar', 'PK', 'G9|3O'); 	                   	
arrStation[28] = new Array ('GAZ', 'GAZA', 'PN', 'G9|3O'); 
arrStation[29] = new Array ('NBL', 'NABOLUS', 'PN', 'G9|3O');  		                       	
arrStation[30] = new Array ('DOH', 'Doha', 'QA', 'G9|3O'); 		                       	                       	
arrStation[31] = new Array ('VKO', 'Moscow', 'RU', 'G9|3O'); 	    	                     	
arrStation[32] = new Array ('MED', 'Medinah', 'SA', 'G9|3O'); 	 	                    	                       	
arrStation[33] = new Array ('KSA', 'Saudi Arabia', 'SA', 'G9|3O'); 	                       		                       	
arrStation[34] = new Array ('RUH', 'Riyadh', 'SA', 'G9|3O'); 	
arrStation[35] = new Array ('JED', 'Jeddah', 'SA', 'G9|3O');  	                     	
arrStation[36] = new Array ('DMM', 'Dammam', 'SA', 'G9|3O');  	
arrStation[37] = new Array ('KSC', 'KSA Call Centre', 'SA', 'G9|3O');  	 
arrStation[38] = new Array ('KRT', 'Kartoum', 'SD', 'G9|3O');  	             	
arrStation[39] = new Array ('SIN', 'SINGAPORE', 'SG', 'G9|3O');  	                  	
arrStation[40] = new Array ('BCN', 'Barcelona', 'SP', 'G9|3O');   	                  	 		               	
arrStation[41] = new Array ('ALP', 'Aleppo', 'SY', 'G9|3O');  	                     	
arrStation[42] = new Array ('DAM', 'Damascus', 'SY', 'G9|3O');  	                    	                   	
arrStation[43] = new Array ('LTK', 'Latakia', 'SY', 'G9|3O'); 	                   	
arrStation[44] = new Array ('SAW', 'Istanbul', 'TR', 'G9|3O');
arrStation[45] = new Array ('IST', 'Istanbul', 'TR ', 'G9|3O');	 	                   	                       	
arrStation[46] = new Array ('KBP', 'KIEV', 'UA ', 'G9|3O');	 	                       	
arrStation[47] = new Array ('SAH', 'Sanaa', 'YE ', 'G9|3O');
arrStation[48] = new Array ('NBO', 'Nairobi', 'KE ', 'G9|3O');
arrStation[48] = new Array ('BRU', 'Brussels - Terminal', '', '');



//----------------- End of Station List -------------- //

arrError['ERR009'] = 'Your Departure Date cannot be before the current date. Please check and enter again.';
arrError['ERR008'] = 'Please select your Returning Date.';
arrError['ERR002'] = 'Please select your destination.';
arrError['ERR006'] = 'Please select a Currency.';
arrError['ERR004'] = 'Your Returning Date cannot be before your Departing Date. Please check your dates and enter again. ';
arrError['ERR001'] = 'Please select the city you will be departing from.';
arrError['ERR005'] = 'Please enter a valid Date.';
arrError['ERR007'] = 'Please select your Departing Date.';
arrError['ERR003'] = 'The From & To locations cannot be the same. Please check and enter again';
arrError['ERR010'] = 'Invalid value for inbound transit time';
arrError['ERR011'] = 'Invalid value for outbound transit time';

var voucherEnable = <%=AppParamUtil.isVoucherEnabled()%>

if (!voucherEnable) {
	document.getElementById("btnGiftVoucher").style.display = 'none';
}

var appURL = getValue("hdnParamUrl");

var strDash = "----------------------"
var strSeleDDay = "";
var strSeleRDay = "";
var intEndMonth  = "";
var intEndYear = "";	
var objCW ;
var strRDate = "";
var strDDate = "";
var blnReturn = false;

// Default values
var intMaxAdults = 9
var strFromHD = 'Where are you flying from'
var strToHD = 'Where are you flying to'
var strDDDefValue = 'Please Select'
var intRDaysDef = 0;
var strDefDayRetu = "0";
var strBaseAirport = "SHJ"
var strBaseAirport2 = "CMN"
var strFrom = "";
var strTo = "";
var strCurr = "AED";
var strSelCurr = "AED";
var strDDays = "0";
var strRDays = "0";
var intDays = "3";
var intRDays = "3";
var strDefDayDept = "0";
var strLanguage = "EN";
var strOriginCarrier = "G9";
var shjHub = 'G9';
var maHub = 'MA';//this should replaced according to new hub code

var dtC = new Date();
var dtCM = dtC.getMonth() + 1;
var dtCD = dtC.getDate();
if (dtCM < 10){dtCM = "0" + dtCM}
if (dtCD < 10){dtCD = "0" + dtCD}

var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); //"25/08/2005";
var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
var today = new Date();
var expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000); // plus 30 days

var ondDataMap = [];
var originList = [];

var dynamicOndListEnabled = document.getElementById('dynamicOndListEnabled').value;

	function buildYearDropDown(strName, intYears){
		var dtDepDate	= addDays(dtSysDate, 0);
		var intMonth	= dtDepDate.getMonth();
		var intYear		= dtDepDate.getFullYear();
		var arrMonths	= new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		
		var objDrop		= getFieldByID(strName);
		if(objDrop == null)
			return ;
		var strValue    = "";
		var intMonths	= 12 * intYears
		for (var i = 0 ; i < intMonths; i++){
			
			if ((intMonth + 1) < 10){
				strValue = "0" + (intMonth + 1);
			}else{
				strValue = (intMonth + 1);
			}
			
			objDrop.length =  objDrop.length + 1
			objDrop.options[objDrop.length - 1].text = arrMonths[intMonth] + " " + intYear;
			objDrop.options[objDrop.length - 1].value = strValue + "/" + intYear;
			intEndMonth = strValue;
			intEndYear  = intYear;
			intMonth++
			
			if (intMonth > 11){
				intMonth = 0 ;
				intYear++;
			}
		}
	}
	
	function faqClick(){
		var intHeight = (window.screen.height - 200);
		var intWidth = 795;
		var intX = ((window.screen.height - intHeight) / 2);
		var intY = ((window.screen.width - intWidth) / 2);
		var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
		if ((objCW)  && (!objCW.closed)){objCW.close();}
		objCW = window.open(strFAQLink,'ToolChild',strProp)
		
	}
	
	function FillCalenderDay(objDName, strValue){
		var objdrop = getFieldByID(objDName)
		var strCD	= strSysDate
		var strDMonth = Number(strSysDate.substr(3,2));
		var strDYear = Number(strSysDate.substr(6,4));
		
		var strMonth = Number(strValue.substr(0,2));
		var strYear = Number(strValue.substr(3,4));
		
		var strDays = getDaysInMonth(strMonth, strYear);
		var strDay = ""
		var strStartDate = 1;
		if ((strDMonth == strMonth) && (strYear == strDYear)){
			strStartDate = Number(strSysDate.substr(0,2))
		}
		
		objdrop.length = 1;
		for (var i = strStartDate; i <= strDays ; i ++){
			strDay = i 
			if (strDay < 10){
				strDay = "0" + strDay
			}
			objdrop.length = objdrop.length + 1
			objdrop.options[objdrop.length -1].text =  strDay;
			objdrop.options[objdrop.length -1].value =  strDay;
		}
	}
	
	
	function onGo(){
	 
	 
		
	var selValueDF = getValue("selFromLoc2");
	var selValueDT = getValue("selToLoc2");
		
	var roundTrip = document.getElementById('chkReturnTrip2').checked;
	var from = "";
	var to = "";
	 
		from = arrAirports[selValueDF][0];
		to = arrAirports[selValueDT][0];
	 
	var date = document.getElementById('selYrDept2').value;
	var specFltFromDate = document.getElementById('specialFltFromDate').value;
	var specFltToDate = document.getElementById('specialFltToDate').value;
	var specFltNo = document.getElementById('specialFltNo').value;
	var specFltDst = document.getElementById('specialFltDst').value;
	var surl = "";
	var hub ="";
	 
	
		surl = document.getElementById('schurl').value +"showFlightSchedule.action";
	 
		 
	
	surl = surl+"?startDate="+date+"&fromDst="+from+"&toDst="+to+"&roundTrip="+roundTrip+"&lang=en"+"&sffd="+specFltFromDate+"&sftd="+specFltToDate+"&sfno="+specFltNo+"&sfd="+specFltDst;
	var frm = document.getElementById('sch_container');
	frm.src = surl;


	//var agentFrm =document.getElementById('sch_agents'); 
	//agentFrm.src = document.getElementById('schurl').value+"showAgentsInfo.action";
	}
	
	function ondListLoaded(){
		if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
			return false;
		} else {
			return true;
		}
	}
	
	function customSort(a, b){
		a = a[1];
		b = b[1];
		return a == b ? 0 : (a < b ? -1 : 1)
	}
		
	function initDynamicOndList(){		
		
		for ( var originIndex in origins) {

			var tempDestArray = [];
			for ( var destIndex in origins[originIndex]) {
				var curDest = origins[originIndex][destIndex];
				
				if(curDest[0] != null){							
					var currentSize = tempDestArray.length;
					tempDestArray[currentSize] = [];
					tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
					tempDestArray[currentSize][1] = airports[curDest[0]]['en'];
				}				
			}

			// Add entry if at least one destination is there to final ond map
			if (tempDestArray.length > 0) {
		
				tempDestArray.sort(customSort);
				ondDataMap[airports[originIndex]['code']] = tempDestArray;

				var currentOriginSize = originList.length;
				originList[currentOriginSize] = [];
				originList[currentOriginSize][0] = airports[originIndex]['code'];
				originList[currentOriginSize][1] = airports[originIndex]['en'];

				originList.sort(customSort);

			}			
			
		}
	}
	
	/* var objFD = new filterDropDown();
	objFD.id  = "spnOrg";
	objFD.width = "163px";
	objFD.listWidth = "280px";
	objFD.dataArray = arrAirports;
	 
	objFD.onClick = "pgOnClickFr";
	objFD.onSelect = "selFromLocOnChange";
	objFD.wrapText = false;
	objFD.defaultText = "--From--";
	objFD.tabIndex = 0;
	//objFD.displayFilterDropDown();

	 var objTD = new filterDropDown();
	objTD.id  = "spnDes";
	objTD.width = "163px";
	objTD.listWidth = "280px";
	objTD.dataArray = arrAirports;
	 
	objTD.onClick = "pgOnClickT";
	objTD.onSelect = "selToLocOnChange";
	objTD.wrapText = false;
	objTD.defaultText = "--To--";
	objTD.tabIndex = 0;
	objTD.displayFilterDropDown(); */
	
	function buildAirportList(){	
		var fromAirpotList;
		if(dynamicOndListEnabled && ondListLoaded()){
			initDynamicOndList();
			fromAirpotList = originList;
			
		} else {
			fromAirpotList = arrAirports;
		}
		
		var objFD		= getFieldByID("selFromLoc");
		var objTD		= getFieldByID("selToLoc");
		//Intially toairportlist same as from airport
		
		objFD.options[0].text = '--From--';
		objFD.options[0].value = '--From--';
		
		objTD.options[0].text = '--To--';
		objTD.options[0].value = '--To--';
		
		for (var i = 0 ; i < fromAirpotList.length; i++){
			objFD.length =  objFD.length + 1
			objFD.options[objFD.length - 1].text = fromAirpotList[i][1];
			objFD.options[objFD.length - 1].value = fromAirpotList[i][0];
			
			objTD.length =  objTD.length + 1
			objTD.options[objTD.length - 1].text = fromAirpotList[i][1];
			objTD.options[objTD.length - 1].value = fromAirpotList[i][0];
		}			
	}
	
	function buildDropDowns(){
		
		buildAirportList();
	
		buildYearDropDown("selYrDept",1);
		buildYearDropDown("selYrRetu",1);
		FillCalenderDay("selDtDept", getFieldByID("selYrDept").options[1].value);
		
		defaultSetYearDay();
		
		var objLB = new listBox();
		objLB.dataArray = arrCurrency; 
		objLB.id = "selCurrency";
		objLB.blnMergeTextValue = true;
		objLB.firstTextValue = strDDDefValue;
		objLB.blnMergeStyle = "-";
		objLB.blnFirstEmpty = true;
		objLB.fillListBox();		
		
		var objOriginLB = new listBox();
		objOriginLB.dataArray = arrCountry;
		objOriginLB.id = "selOriginCountry";
		objOriginLB.blnMergeTextValue = false;
		objOriginLB.firstTextValue = "UAE";
		objOriginLB.blnMergeStyle = "-";
		objOriginLB.blnFirstEmpty = true;
		objOriginLB.fillListBox();
		
	}

	//**************** Related to Agents search ****************//
	/** Populates Country list*/
	 

	function selCountryOnChange(){
		var country = getValue("selCountry");
		buildStationDropDown(country);
	}

	function onContinue(){
		setField("hdnMode", "SEARCH");
		setField("hdnLanguage", "en"); // Language
		//var object = document.getElementById('sch_agents');
		var url = getValue("hdnurl");

		var country=getValue("selCountry");
		var station = getValue("selStation");
		var agentType=getValue("selAgentType");
			
		url = url+"?selCountry="+country+"&selStation="+station+"&selAgentType="+agentType;
		//object.src =url+'&hdnMode=SEARCH';
		
		getFieldByID("formRetrieveAgents").action = url+'&hdnMode=SEARCH' ;
		getFieldByID("formRetrieveAgents").submit();
		
	}
	
	
	function defaultSetYearDay(){
		FillCalenderDay("selDtRetu", getFieldByID("selYrRetu").options[1].value);
	}
	
	function selYrDeptOnChange(){
		FillCalenderDay("selDtDept", getValue("selYrDept"));
		setField("selDtDept", strSeleDDay);
	}
	
	function selYrRetuOnChange(){
		FillCalenderDay("selDtRetu", getValue("selYrRetu"));
		setField("selDtRetu", strSeleRDay);
	}
	
	function selDtDeptOnChange(){
		strSeleDDay = getValue("selDtDept");
	}

	function selDtRetuOnChange(){
		strSeleRDay = getValue("selDtRetu");
	}	
	
	function selToLocOnChange(){
		
		getFieldByID("txtHubCode").value = "";
		getFieldByID("chkHubTransit").disabled = true;
		getFieldByID("txtOutboundTransitTime").disabled = true;
		getFieldByID("txtInboundTransitTime").disabled = true;
		
		if (getText("selToLoc") == strDash){
			getFieldByID("selToLoc").options[0].selected = true;
		}
		
		if(getValue("selToLoc") != '--To--' && getValue("selFromLoc") != '--From--'){
			if(dynamicOndListEnabled && ondListLoaded() && userDefineTransitTimeEnabled){
				
				var fromIndex = -1;
				var toIndex = -1;
				// get from, to indexes
				var from = getValue("selFromLoc");
				var to = getValue("selToLoc");
				var airportsLength = airports.length;
				for(var i=0;i<airportsLength;i++){
					if(from == airports[i].code){
						fromIndex = i;
					} else if(to == airports[i].code) {
						toIndex = i;
					}
					
					if(fromIndex != -1 && toIndex != -1){
						break;
					}
				}
				
				if(fromIndex != -1){			
					var availDestOptLength = origins[fromIndex].length;
					for(var i=0;i<availDestOptLength;i++){
						var destOptArr = origins[fromIndex][i];
						if(destOptArr[0] == toIndex){							
							//prepare HUB stopover time option 
							var hubCodeList = destOptArr[5];
							if(hubCodeList != null && hubCodeList.length > 0){
								for(var k = 0; k < hubCodeList.length; k++){
									var hubCodes = hubCodeList[k];
									if(hubCodes != null && hubCodes.length > 0){
										//Need to show fields to define hub stopovertimes hubTimeDetails									
										for(var l = 0; l < hubCodes.length ; l++){											
											getFieldByID("txtHubCode").value = hubCodes[l];
											getFieldByID("chkHubTransit").disabled = false;
										}
									}
								}
							}						
							break;
						}
					}
				} 
			}
		}
	}
	
	function selFromLocOnChange(){
		if (getValue("selFromLoc") == '--From--'){
			//getFieldByID("selFromLoc").options[0].selected = true;
		} else {
			var toAirportList;
			
			if(dynamicOndListEnabled && ondListLoaded()){				
				toAirportList = originList;
				
			} else {
				toAirportList = arrAirports;
			}
			var objTD		= getFieldByID("selToLoc");
			var toAirportList = ondDataMap[getValue("selFromLoc")];	
			objTD.options[0].text = '--To--';
			objTD.options[0].value = '--To--';
			for (var i = 0 ; i < toAirportList.length; i++){				
				objTD.length =  objTD.length + 1
				objTD.options[objTD.length - 1].text = toAirportList[i][1];
				objTD.options[objTD.length - 1].value = toAirportList[i][0];
			}			
		}
		
		getFieldByID("txtHubCode").value = "";
		getFieldByID("chkHubTransit").disabled = true;
		getFieldByID("txtOutboundTransitTime").disabled = true;
		getFieldByID("txtInboundTransitTime").disabled = true;
	}
	
	function setIBEDate(strDate, strID){
		var arrStrValue = strDate.split("/");
		switch (strID){
			case "0" : 
				setField("selYrDept", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrDeptOnChange();
				setField("selDtDept", arrStrValue[0]);
				break ;
			case "1" : 
				setField("selYrRetu", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrRetuOnChange();
				setField("selDtRetu", arrStrValue[0]);
				break ;
		}
	}
	
	function chkReturnTrip_click(){
		if (getFieldByID("chkReturnTrip").checked){
			setField("selRVariance", intRDaysDef);
			Disable("selDtRetu", false);
			Disable("selYrRetu", false);
			Disable("selRVariance", false);
		}else{
			setField("selRVariance", "0");
			setField("selDtRetu", "");
			setField("selYrRetu", "");
			Disable("selDtRetu", true);
			Disable("selYrRetu", true);
			Disable("selRVariance", true);
			defaultSetYearDay();
		}
	}
	
	function chkHubTransit_click(){
		getFieldByID("txtOutboundTransitTime").disabled = false;
		getFieldByID("txtInboundTransitTime").disabled = false;
	}
	
	function loadIBECal(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 0 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setIBEDate";
		objCal1.currentDate = strSysDate;
		switch (strID){
			case 0 : 
				if (getValue("selDtDept") != "" && getValue("selYrDept")){
					objCal1.currentDate = getValue("selDtDept") + "/" + getValue("selYrDept");
				}
				break;
			case 1 : 
			if (getFieldByID("chkReturnTrip").checked){
				if (getValue("selDtRetu") != "" && getValue("selYrRetu")){
					objCal1.currentDate = getValue("selDtRetu") + "/" + getValue("selYrRetu");
				}
			}else{
				return;
			}
			break;			
		}		
		objCal1.showCalendar(objEvent);
	}
	
	// --------------------- Maximum Passengers
	function selAdults_onChange(){
		var arrInfants = new Array();
		var intAdults = Number(getValue("selAdults"))// + Number(getValue("selChild"));
		var strCInfants = getValue("selInfants");
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrInfants; 
		objLB.textIndex = "0";
		objLB.id = "selInfants";
		objLB.fillListBox();
		setPaxArrays();
		setField("selInfants",  strCInfants);
	}

	function flightSearchBtnOnClick(intID){
		switch (intID){
			case 0 : 
				if (getValue("selFromLoc")=="--From--"){
					alert("Please select the location of your departure");
					return false;
				}
				else if (getValue("selToLoc")=="--To--"){
					alert("Please select the location of your arrival");
					return false;
				}
				if (clientValidateFS()){
					var objFrm = getFieldByID("form1");
					setField("hdnCarrier",  getValue("selOriginCarrier"));	
					setField("hdnOriginCountry", getValue("selOriginCountry"));
					objFrm.target = "_top";
					objFrm.action = "../public/showReservation.action";
					objFrm.submit();
				}
				break;
			case 1 : 
				var strPostData =  getValue("selLangunage") + "^RE^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
			case 2 :
				var strPostData = getValue("selLangunage") + "^SI^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));				
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 3 : 
				var strPostData =  getValue("selLangunage") + "^REA^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 4 : 
				var strPostData =  getValue("selLangunage") + "^MAN^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 5 : 
				var strPostData =  getValue("selLangunage") + "^REC^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 6 : 
				var strPostData =  getValue("selLangunage") + "^MB^" + getValue("pnr") + "^" + getValue("txtLastName") + "^" + getValue("txtDateOfDep");
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 7 : 
			    var strPostData =  getValue("selLangunage") + "^MC^" + getValue("selCurrency") ;
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 8 :
				var strPostData =  getValue("selLangunage") + "^MRE^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 9 : 
			    var strPostData =  getValue("selLangunage") + "^GV^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
		}
	}	
	
	function getHubTimeDetails(){
		
	}
	
	
	function clientValidateFS(){
		if (getValue("selFromLoc") == ""){
			alert(arrError["ERR001"]);		
			//getFieldByID("selFromLoc").focus();		
			return false;
		}
		
		if (getValue("selToLoc") == ""){
			alert(arrError["ERR002"]);
			//getFieldByID("selToLoc").focus();
			return false;
		}		
		
		if (getValue("selCurrency") == ""){
			alert(arrError["ERR006"]);
			getFieldByID("selCurrency").focus();
			return false;
		}				
		
		if (getValue("selFromLoc") == getValue("selToLoc")){
			alert(arrError["ERR003"]);
			//getFieldByID("selToLoc").focus();			
			return false;
		}
		
		if ((getValue("selDtDept") == "") || (getValue("selYrDept") == "")){
			alert(arrError["ERR007"]); 	
			getFieldByID("selDtDept").focus();		
			return false;
		}else{
			if (!CheckDates(strSysDate, getValue("selDtDept") + "/" + getValue("selYrDept"))){
				alert(arrError["ERR009"] + strSysDate + ".");
				getFieldByID("selDtDept").focus();	
				return false;
			}			
		}
		
		if (getFieldByID("chkReturnTrip").checked){
			if ((getValue("selDtRetu") == "") || (getValue("selYrRetu") == "")){
				alert(arrError["ERR008"]); 	
				getFieldByID("selDtRetu").focus();		
				return false;
			}
		
			if (!CheckDates(getValue("selDtDept") + "/" + getValue("selYrDept"), getValue("selDtRetu") + "/" + getValue("selYrRetu"))){
				alert(arrError["ERR004"]);
				getFieldByID("selDtRetu").focus();
				return false;
			}
		}
		
		strRDate = "";
		blnReturn = false;
		if (getValue("selDtRetu") != "" && getValue("selYrRetu") != ""){
			strRDate	= getValue("selDtRetu") + "/" + getValue("selYrRetu");
		}
		if (getFieldByID("chkReturnTrip").checked){blnReturn	= true;}
		
		var hubTimeDetails = "";
 
		if(getFieldByID("chkHubTransit").checked){
			if(getFieldByID("txtOutboundTransitTime").value != ""){
				var regEx=/^\d+$/;
				if(!regEx.test(getFieldByID("txtOutboundTransitTime").value)){
					alert(arrError["ERR010"]);
					return false;
				}
				
				if(getFieldByID("txtInboundTransitTime").value!=""){
					if(!regEx.test(getFieldByID("txtInboundTransitTime").value)){
						alert(arrError["ERR011"]);
						return false;
					}
				}
				hubTimeDetails = getFieldByID("txtHubCode").value + "_" + getFieldByID("txtOutboundTransitTime").value + "_" + getFieldByID("txtInboundTransitTime").value;

			}else{
				hubTimeDetails = getFieldByID("txtHubCode").value +"_0_0";
			}
		}
		
		var strPostData = getValue("selLangunage") + "^" + 
				  "AS^" +
			   getValue("selFromLoc") + "^" +
			   getValue("selToLoc") + "^" +
			   getValue("selDtDept") + "/" + getValue("selYrDept") + "^" +
			   strRDate + "^" +
			   getValue("selDVariance") + "^" +
			   getValue("selRVariance") + "^" +
			   getValue("selCurrency") + "^" +
			   getValue("selAdults") + "^" +
			   getValue("selChild") + "^" +
			   getValue("selInfants") + "^" +
			   getText("selFromLoc").replace(/ *\([^)]*\) */g, "") + "^" +
			   getText("selToLoc").replace(/ *\([^)]*\) */g, "") + "^" +
			   blnReturn + "^" + getValue("selCOS") +
			   "^" + 
			   "^" +
			   "^" +
			   getText("txtPromoCode") + "^" + "^" + "^" + hubTimeDetails;

		setField("hdnParamData", strPostData);		
		return true;
	}
	
	function defaultDataLoad(){
		var dtDepDate = addDays(dtSysDate, Number(strDefDayDept));
		strDDate = dateChk(dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear());
		strSeleDDay = dtDepDate.getDate();
		
		var dtRetuDate = "";
		strRDate = "";
		strSeleRDay = "  /  /    ";
		if (String(strDefDayRetu) != ""){
			var dtRetuDate = addDays(dtSysDate, Number(strDefDayRetu));
			strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
			strSeleRDay = dtRetuDate.getDate();
			setField("chkReturnTrip", true);
		}
		
		setField("selDVariance", strDDays); 
		setField("selRVariance", strRDays);
		chkReturnTrip_click();
	}
	
	function dateOnBlur(strID, objC){
		var blnDateEntered = false;
		if (objC.value != ""){blnDateEntered = true;}
		dateChk(strID);
		if (objC.value == "" && blnDateEntered){
			alert(arrError["ERR005"]); 
		}
	}
	
	function cacheData(){
		setField("selFromLoc", strFrom);
		setField("selToLoc", strTo);
		
		var arrDtDept = strDDate.split("/");
		setField("selYrDept", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrDeptOnChange();
		setField("selDtDept", arrDtDept[0]);
		
		setField("selYrRetu", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrRetuOnChange();
		setField("selDtRetu", arrDtDept[0]);
		
		if (getValue("selDtRetu") == ""){
			defaultSetYearDay();
		}
				
		setField("selDVariance", strDDays);
		setField("selRVariance", strRDays);
		setField("selCurrency", strSelCurr);
		setField("selOriginCarrier", strOriginCarrier);
		
		//getFieldByID("selFromLoc").focus();
	}
	
	
	// ------------------------ 
	function flightSearchOnLoad(){
		buildDropDowns();

		
		// ------------------------ 
		var arrDays = new Array();
		for (var i = 0 ; i <= intDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		
		var objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selDVariance";
		objLB2.fillListBox();
		
		arrDays.length = 0
		for (var i = 0 ; i <= intRDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selRVariance";
		objLB2.fillListBox();

		Disable("selDtRetu", true);
		Disable("selYrRetu", true);
		Disable("selRVariance", true);
		
		defaultDataLoad()
		cacheData();
	}	
	flightSearchOnLoad();
	
	 
	// ---------------- Calendar
	var objCal1 = new Calendar("ibeCalendar1");
	objCal1.onClick = "setIBEDate";
	objCal1.borderColor = "#FE0000";
	objCal1.borderClass = "calendarBorder"
	objCal1.headerTextColor = "white";
	objCal1.noYears = "1";
	objCal1.blnArabic = false;
	objCal1.disableUpto = strSysDate;
	objCal1.disableFrom = getDaysInMonth(intEndMonth, intEndYear) + "/" + intEndMonth + "/" + intEndYear;
	objCal1.buildCalendar();
	
	var objSecs;
	function startTime() {
		var sysDate = ""
		sysDate = document.getElementById('sysDate').value
		var arrdt = sysDate.split(" ");
		var arryear = arrdt[0].split("/");
		var arrhrs = arrdt[1].split(":");
		vrSyaDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
				Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
				Number(arrhrs[2]));
		objSecs = setInterval("addSeconds()", 1000);
	}

	function addSeconds() {
		vrSyaDate.setSeconds(vrSyaDate.getSeconds() + 1);
		var clDate = vrSyaDate;
		getFieldByID("spnClock").innerHTML = clDate.toLocaleString();
	}
	
	try{
		startTime();
	}catch(e){ getFieldByID("spnClock").innerHTML = "error" }
	
	var objTSecs;
	function startStatusTime() {
		var sysDate = ""
		sysDate = document.getElementById('sysDate').value
		var arrdt = sysDate.split(" ");
		var arryear = arrdt[0].split("/");
		var arrhrs = arrdt[1].split(":");
		vrSyaDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
				Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
				Number(arrhrs[2]));
		objSecs = setInterval("addSeconds()", 1000);
	}
	
	
	function updateTitle(){
	try{
		var sysTDate = ""
			sysTDate = document.getElementById('sysDate').value
			var arrdt = sysTDate.split(" ");
			var arryear = arrdt[0].split("/");
			var arrhrs = arrdt[1].split(":");
			vrSyTDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
					Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
					Number(arrhrs[2]));
			objTSecs = setInterval("addSecondsToTitle()", 1000);
		}catch(e){ }
	}
	
	function addSecondsToTitle() {
		try{
			vrSyTDate.setSeconds(vrSyaDate.getSeconds() + 1);
			var clDate = vrSyTDate.getHours()+": "+ vrSyTDate.getMinutes()+": "+vrSyTDate.getSeconds();
			document.title = clDate.toLocaleString();
			}catch(e){ }
	} 
	
	
		  
	 
//
--></script>
<script src="../js/CommonLoader.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../js/dummydest.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
<script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script> 
<script type="text/javascript"> 
    try {
    	var pageTracker = _gat._getTracker("UA-1319699-8"); 
        pageTracker._setDomainName(".isaaviations.com");
        pageTracker._setAllowHash("false");
        pageTracker._initData();
		pageTracker._trackPageview("/Landing_G9");
    } catch(err) {}
</script>
<!-- ClickTale Bottom part --> 
<div id="ClickTaleDiv" style="display: none;"></div> 
<script type='text/javascript'> 
document.write(unescape("%3Cscript%20src='"+ 
 (document.location.protocol=='https:'? 
  'https://clicktale.pantherssl.com/': 
  'http://s.clicktale.net/')+ 
 "WRb6.js'%20type='text/javascript'%3E%3C/script%3E")); 
</script> 
<script type="text/javascript"> 
var ClickTaleSSL=1; 
if(typeof ClickTale=='function') ClickTale(4194,0.0085,"www07"); 
</script> 
<!-- ClickTale end of Bottom part --> 
</body>
</html>
