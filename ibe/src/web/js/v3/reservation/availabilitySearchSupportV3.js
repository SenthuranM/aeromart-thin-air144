var dirctionPB = "H";
$.fn.iterateTempletePB = function(params) {
	var templeteName = "";	
	if (dirctionPB.toUpperCase() == "V"){
		var temp = $("#" + params.templeteName)
		if (temp.get(0).tagName == "TD"){
			temp.parent().attr("id", params.templeteName);
			temp.attr("id","");
		}
	}
	templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;
    var segmentCode = "";   
	var dtoRecord;	
	for (var f = 0; f < params.data.length; f++) {	
		segmentCode = params.data[f].segmentName;
		clonedTemplateId = templeteName + "_" + f;
		var	clone = $( "#" + templeteName + "").clone();
		clone.attr("id", clonedTemplateId );
		clone.appendTo($("#" + templeteName).parent());
		$("#"+clonedTemplateId).find("label#segmentName").text(segmentCode);
		if (dirctionPB.toUpperCase() == "H"){
			$( "#" + clonedTemplateId).css({"width":"49%"});
			$( "#" + clonedTemplateId).addClass("floatleft");
			if ( params.data.length > 1){
				if (f == 0){
					$("<div width='1%' style='width:1%;float:left'>&nbsp;</div>").appendTo($( "#" + clonedTemplateId).parent());
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" + UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}else{
					$( "#" + clonedTemplateId).css({"width":"49%"});
					$( "#" + clonedTemplateId).addClass("floatright");
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" + UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}
				$( "#" + clonedTemplateId).parent().next().css({"width":"100%","clear":"both"});
			}else{
				$( "#" + clonedTemplateId).parent().next().css({"width":"49%","clear":"both"});
				if (f == 0){
					//$("<tr ><td class='rowSingleGap'></td></tr>").appendTo($( "#" + clonedTemplateId).parent());
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" + UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}else{
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" + UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}
			}
		}else{
			if ( params.data.length > 1){
				$( "#" + clonedTemplateId).children().attr("width","100%");
		
				if (f == 0){
					$("<tr ><td class='rowSingleGap'></td></tr>").appendTo($( "#" + clonedTemplateId).parent());
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" +UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}else{
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" + UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}
			}else{
				if (f == 0){
					$("<tr ><td class='rowSingleGap'></td></tr>").appendTo($( "#" + clonedTemplateId).parent());
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" + UI_AvailabilitySearch.outSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}else{
					//clone.find(".FareTypes").addClass(UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale);
                    clone.find(".FareTypes").css({
                        "backgroundImage": "url(" + UI_AvailabilitySearch.retSelectedFareType + "_" + SYS_IBECommonParam.locale + ".png)"
                    });
				}
			}
		}
		reSetPaxWiseData(params.data[f].paxWise);
		for(var i = 0; i < params.data[f].paxWise.length ; i++) {
			if (UI_AvailabilitySearch.showPriceBDInSelCurr){
				$( "#" + clonedTemplateId + "").find("#"+params.dtoName).find("#totalPaxFare").attr("id", "selCurTotalPaxFare");
			}
			var paxWiseRowsClone = $( "#" + clonedTemplateId + "").find("#"+params.dtoName).clone();
			var paxClonedTemplateId = templeteName + "_" + f + "_" + i;			
			paxWiseRowsClone.attr("id", paxClonedTemplateId );
			paxWiseRowsClone.addClass("paymenBdown");
			paxWiseRowsClone.appendTo($( "#" + clonedTemplateId + "").find("#"+params.dtoName).parent());			
			dtoRecord =  params.data[f].paxWise[i];			
			elementIdAppend = params.dtoName + "[" + i + "].";
			$("#"+paxClonedTemplateId).find("label").each(function( intIndex ){
					$(this).text(dtoRecord[this.id]);
					$(this).attr("id",  elementIdAppend +  this.id);
			});						
			$( "#" + paxClonedTemplateId).show();
			$( "#" + clonedTemplateId + "").find("#"+params.dtoName).hide();
		}
		var tTax = params.data[f].totalTaxSurcharges;
		var tCCode = UI_AvailabilitySearch.currencyCode;
		var tTotel = params.data[f].totalPrice;
		if (UI_AvailabilitySearch.showPriceBDInSelCurr){
			tTax = params.data[f].totalTaxSurchargesInSelectedCurr;
			tCCode = UI_AvailabilitySearch.selectedCurrency;
			tTotel = params.data[f].totalPriceInSelectedCurr;
		}
		if (calDisplayConfig.design.totelinSelectedWhileBase){
			tTotel = params.data[f].totalPriceInSelectedCurr;
		}
		
		var totalChargesperFlightRow = "<tr class='paymenBdown'><td class='GridItems alignLeft'><label>" + UI_AvailabilitySearch.labels.lblCharges+ "</label></td><td class='GridItems alignRight'><label>"+
		tTax + " " + tCCode +"</label></td></tr>" +
									"<tr class='paymenBdown'><td class='GridHighlight alignLeft'><label class='fntBold'>"+inoutLabels(f)+"</label></td><td class='GridHighlight alignRight'><label class='fntBold'>"+
									tTotel + " " + ((calDisplayConfig.design.totelinSelectedWhileBase)?UI_AvailabilitySearch.selectedCurrency:tCCode) +"</label></td></tr>";
		$(totalChargesperFlightRow).appendTo($( "#" + clonedTemplateId).find("tbody"));		
		$( "#" + clonedTemplateId).show();
	}
	$( "#" + templeteName).hide();
	$( "#priceBreakDownTemplate_0").find(".paymenBdown").filter(':odd').find(" td").removeClass("rowColor").addClass("GridItems");
	$( "#priceBreakDownTemplate_1").find(".paymenBdown").filter(':odd').find(" td").removeClass("rowColor").addClass("GridItems");
}

/**
 * Reset Paxwise data
 */

reSetPaxWiseData = function(paxWise) {
	var showPriceBDInSelCurr = false;
	var currencyCode = "";
	var selectedCurrency = "";
	if (UI_AvailabilitySearch != null){
		showPriceBDInSelCurr = UI_AvailabilitySearch.showPriceBDInSelCurr;
		currencyCode = UI_AvailabilitySearch.currencyCode;
		selectedCurrency = UI_AvailabilitySearch.selectedCurrency;
	}else{
		showPriceBDInSelCurr = UI_mcAvailabilitySearch.showPriceBDInSelCurr
		currencyCode = UI_mcAvailabilitySearch.currencyCode
		selectedCurrency = UI_mcAvailabilitySearch.selectedCurrency
	}
	
	var tempPax = "";
	for (var i = 0; i < paxWise.length; i++) {
		tempPax = paxWise[i];		
		tempPax.paxType = getPaxType({paxCode:tempPax.paxType});		
		
		var currCode = '';
		var fareObj = {sur:null, fare:null, total:null, totalPaxFare : null};
		if(showPriceBDInSelCurr && (currencyCode != selectedCurrency)){
			curCode = selectedCurrency;
			if (tempPax.sur) {
				fareObj.sur = tempPax.selCurSur  + " " + curCode;			
			}
			if (tempPax.fare) {
				fareObj.fare = tempPax.selCurFare + " "+ curCode;
			}
			
			if (tempPax.selCurtotal) {
				fareObj.total = tempPax.selCurtotal + " " + curCode;
			}
			if (tempPax.totalPaxFare) {
				fareObj.totalPaxFare = tempPax.selCurTotalPaxFare + " "+ curCode;
			}
			if (tempPax.selCurTotalPaxFare) {
				fareObj.selCurTotalPaxFare = tempPax.selCurTotalPaxFare + " "+ curCode;
			}
		}else{
			curCode = currencyCode;
			
			if (tempPax.sur) {
				fareObj.sur = tempPax.sur + " " + curCode;
			}
			if (tempPax.fare) {
				fareObj.fare = tempPax.fare + " "+ curCode;
			}
			
			if (tempPax.total) {
				fareObj.total = tempPax.total + " " + curCode;
			}
			if (tempPax.totalPaxFare) {
				fareObj.totalPaxFare = tempPax.totalPaxFare + " "+ curCode;
			}
			if (tempPax.selCurTotalPaxFare) {
				fareObj.selCurTotalPaxFare = tempPax.selCurTotalPaxFare + " "+ curCode;
			}
		}
		
		tempPax.fare = fareObj.fare;
		tempPax.sur = fareObj.sur;
		tempPax.total = fareObj.total;
		tempPax.totalPaxFare = fareObj.totalPaxFare;
		tempPax.selCurTotalPaxFare = fareObj.selCurTotalPaxFare;
		delete paxWise[i];
		paxWise[i] = tempPax;
	}
	return paxWise;
};

$.fn.iterateTempleteTaxBD = function(params) {
	var templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;  
	var dtoRecord;	
	reSetTaxBDData(params.data);
	for (var f = 0; f < params.data.length; f++) {	
		clonedTemplateId = templeteName + "_" + f;
		var clone = $( "#" + templeteName + "").clone();	
		clone.attr("id", clonedTemplateId );
		clone.appendTo($("#" + templeteName).parent());			
		dtoRecord =  params.data[f];	
		elementIdAppend = params.dtoName + "[" + f + "].";
		
		$("#"+clonedTemplateId).find("label").each(function( intIndex ){
			$(this).text(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});						
		$( "#" + clonedTemplateId).show();	
	}
	$( "#" + templeteName).hide();
}

/**
 * Set the currency codes based on view options.
 */
reSetTaxBDData = function(taxBDData) {
	var showPriceBDInSelCurr = false;
	var currencyCode = "";
	var selectedCurrency = "";
	if (UI_AvailabilitySearch != null){
		showPriceBDInSelCurr = UI_AvailabilitySearch.showPriceBDInSelCurr;
		currencyCode = UI_AvailabilitySearch.currencyCode;
		selectedCurrency = UI_AvailabilitySearch.selectedCurrency;
	}else{
		showPriceBDInSelCurr = UI_mcAvailabilitySearch.showPriceBDInSelCurr;
		currencyCode = UI_mcAvailabilitySearch.currencyCode;
		selectedCurrency = UI_mcAvailabilitySearch.selectedCurrency;
	}
	
	var tempBD = "";	
	for (var i = 0; i < taxBDData.length; i++) {		
		tempBD = taxBDData[i];
		// we have only one type per row. so getting the first element.
		
 		var paxTypeString = getPaxType({paxCode:tempBD.applicablePassengerTypeCode[0]});
		if (paxTypeString != undefined && paxTypeString!=null && paxTypeString.length>0){
		    tempBD.applicableToDisplay = paxTypeString;
		}
		var currCode = '';
		var amount = null;
		
		if(showPriceBDInSelCurr && (currencyCode != selectedCurrency)){
			currCode = selectedCurrency;
			if (tempBD.amountInSelectedCurrency){
				amount = tempBD.amountInSelectedCurrency +  " " +  currCode;
			}						
		} else {
			currCode = currencyCode;
			if (tempBD.amount){
				amount = tempBD.amount +  " " +  currCode;
			}		
		}
		//set the formated amount value with currency code.
		tempBD.amount = amount;		
		delete taxBDData[i];
		taxBDData[i] =  tempBD;
	}
	return taxBDData;
};

/**
 * Set Total Fare
 */
setTotalFare = function(fareInCurrency) {
	var totalPriceInBase = fareInCurrency.inBaseCurr.totalPrice;
	$("#totalAmount").text(totalPriceInBase + " " + fareInCurrency.inBaseCurr.currency);	
	
	if (fareInCurrency.inBaseCurr.currency != fareInCurrency.inSelectedCurr.currency) {
		var totalPriceInSelCur = fareInCurrency.inSelectedCurr.totalPrice;
		$("#totalAmountSel").text(totalPriceInSelCur + " " + fareInCurrency.inSelectedCurr.currency);
		$("#selectedTotalPanel").show();	
		$("#unselectedTotalPanel").removeClass('baseTotalPanel');
	}
}

getPaxType = function(params) {
	var lblAdult = "";
	var lblChild = "";
	var lblInfant = "";
	if (UI_AvailabilitySearch != null){
		lblAdult = UI_AvailabilitySearch.lblAdult;
		lblChild = UI_AvailabilitySearch.lblChild;
		lblInfant = UI_AvailabilitySearch.lblInfant;
	}else{
		lblAdult = UI_mcAvailabilitySearch.lblAdult;
		lblChild = UI_mcAvailabilitySearch.lblChild;
		lblInfant = UI_mcAvailabilitySearch.lblInfant;
	}
	switch(params.paxCode) {
		case "AD": return lblAdult;break;
		case "CH": return lblChild;break;
		case "IN": return lblInfant;break;		
	}
};


function  setDesignSpecLabels(){
	if (UI_AvailabilitySearch.isModifySegment){
		$("#btnContinue").find("td.buttonMiddle").text(btnContinuelbl);
	}else{
		$("#btnContinue").find("td.buttonMiddle").text(btnContinuelblPass);
	}
	//$("#trTermsNCond").hide();
}

function inoutLabels(ind){
	var lbl = null;
	if (ind == 0){
		lbl = UI_AvailabilitySearch.labels.lblTotalOutFlight;
	}else{
		lbl = UI_AvailabilitySearch.labels.lblTotalInFlight;
	}
	return lbl;
}