/**
* Author : Baladewa Welathanthri
 * 
 */
(function($) {
	var func = null;
	var dayWidth = null;
	var dayGap = null;
	var loading = new Image();
	var calID = null;
	var dat = null;
	this.departureLowerFare = null;
	this.arrivalLowerFare = null
	this.newdayFare = null;
	var selectedDepartureDate = '';
	var selectedReturnDate = '';
	var selectedHtmlToAdd = null;
	var selectedwidth = 155;
	var intInterval = 800;
	showTCF = false;
	//var defaultflightLabels = [{"comment": null, "fareBasis": null, "logicalCCCode": "Y",	"logicalCCDesc": "Economy Class", "rank": 1, "selected": false, "withFlexi": false},
	//                    {"comment": null, "fareBasis": null, "logicalCCCode": "Y",	"logicalCCDesc": "Economy Class", "rank": 1, "selected": false, "withFlexi": true},
	//                    {"comment": null, "fareBasis": null, "logicalCCCode": "J",	"logicalCCDesc": "Economy Class", "rank": 1, "selected": false, "withFlexi": false}];
	
	var defaultflightLabels = [{"logicalCCCode": "", "logicalCCDesc":" ", "seatAvailable":false, "selected":false, "withFlexi":false}];
	
	$.fn.calendarview = function(settings) {
		 var jsDaysFull = UI_AvailabilitySearch.weekdaysFull || ["Sunday","Monday","Tuesday","Wednsday","Thursday","Friday","Saturday"];
		 var jsDays = UI_AvailabilitySearch.weekdays || ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
		 var jsMonths = UI_AvailabilitySearch.months || ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	     var config = {
		 noOfDays : 7,
		 gapOfDays : 0,
		 dispalyItems : {arrivalTime:false,departureTime:false,radio:true,flightNum:false,stopOverCount:true, stopOverDisplayBy:"default", displayNonstop:true},
		 loadingImage : "",
		 displayHeadeFormat : "ddd dd MMM",
		 topLabel : "",
		 travelType : "dep",
		 currencyCode : '',
		 data : {}, // json obj to send
		 flightLabels:defaultflightLabels,
		 getDataForAllDays : function(){			 
		 },
		 selectFlight:function(event, val){
			 
		 },
		 getAvailabilityForSelectedDate: function(htmlElement, CalendarDateId){
		 	
		 }
	     };

	    if (settings)
	    	settings = $.extend(config, settings);
	    
	    
	    var borderWidth = 1;
		loading.src = settings.loadingImage;
	    return this.each(function() {
	    	func = settings;
	    	var	el = $(this);
	    	var calWidth = parseInt(el.css("width"),10);
	    	var scrollSpace = 15;
	    	var bordegap = settings.gapOfDays + borderWidth;
	    	var vewDatesInCal = (settings.noOfDays > 7) ? 7 : settings.noOfDays;
	    	dayWidth = Math.round(((calWidth-50) - ((bordegap * settings.noOfDays * 2)+borderWidth))/vewDatesInCal);
	    	calWidth = (dayWidth * vewDatesInCal)+(bordegap * vewDatesInCal) + borderWidth + 60;
	    	dayGap = "margin:0px "+settings.gapOfDays+"px 0px "+settings.gapOfDays+"px";
	    	headerdayGap = "style='margin:"+settings.gapOfDays+"px 0px 0px "+settings.gapOfDays+"px'";
	    	   	
	    	var beforText = '<table cellpadding="0" cellspacing="0" width="99%" style="border-collapse:collapse"><tr><td colspan="3"><h3 class="label">'+settings.topLabel+'</h3></td></tr><tr><td valign="bottom" width="40" class="newCal" align="left">';
			beforText +='<div class="leftNavigation">';
			
			if (calDisplayConfig.layout.additionalNavigationEnabled){
				beforText +='<div class="sub-div-additional cal_previous"><a href="javascript:void(0)" style="padding:0 3px;display: none;" title="Previous Week">&lt;&lt;</a></div>';
				$('.flightCalendar').css("top","0px");
			} else{
				$('.flightCalendar').css("top","0px");
			}
			
			beforText +='<div class="sub-div cal_previous"><a href="javascript:void(0)" title="'+UI_AvailabilitySearch.labels.lblPreviousDay+'" id="lblPreviousDay">'+UI_AvailabilitySearch.labels.lblPreviousDay+'</a></div>'+
							'</div></td><td class="aligend" style="overflow:hidden;" valign="bottom">&nbsp;</td><td valign="bottom" width="40" class="newCal" align="right"><div class="rightNavigation">';
				
			if (calDisplayConfig.layout.additionalNavigationEnabled){
				beforText +='<div class="sub-div-additional cal_next"><a href="javascript:void(0)" style="padding:0 3px;display: none;" title="Next Week">&gt;&gt;</a></div>';
			}
			
			beforText +='<div class="sub-div cal_next"><a href="javascript:void(0)" title="'+UI_AvailabilitySearch.labels.lblNextDay+'" id="lblNextDay">'+UI_AvailabilitySearch.labels.lblNextDay+'</a></div>';
	    	$(this).before(beforText);
  	
	    	
	    	var htmlToAdd ='';
	    	htmlToAdd += '<div style="width:'+Number(calWidth )+'px" class="c-scroller"><ul class="calendar-ul" style="width:'+Number(calWidth+100)+'px">';
		    	 for (var i = 0; i<vewDatesInCal; i++){
			    		//htmlToAdd += addChild();
			     };
	    	htmlToAdd +='</ul></div>';
	    	$(this).html(htmlToAdd);
	    	$(this).css("width",calWidth);
	    	$(this).prev("table").find(".aligend").html($(this));
	    	var tT = $('<tr class="AllFlight"><td colspan="3" class="TDthisFls"><div class="pnlBottom nonAirarab" style="display:none;height:23px;line-height:25px;"><label id="lblAvailFl" class="fntBold hdFontColor paddingL5">'+UI_AvailabilitySearch.labels.lblAvailFl+'</label> - <label id="lblAvailFl1">'+UI_AvailabilitySearch.labels.lblAvailFl1+' </label></div><div class="thisFls" style="display:none"><table cellpadding="0" cellspacing="0" width="100%" class="GridTable"><thead></thead><tbody id="'+this.id+'_FL"></tbody></table></div></td></tr>');
	    	var tTCF = $('<tr><td colspan="3" style="border:0px! important;"></td></tr>');
	    	if (showTCF){
	    		tTCF = $('<tr><td colspan="3" style="border:0px! important;"><div style="text-align:right"><a href="javascript:void(0)" class="CompareFare"><label id="lblCompareFare" class="hdFontColor"><b>COMPARE</b> FARE OPTION</label></a></div></td></tr>');
	    	}
	    	$(this).parent().parent().parent().append(tT,tTCF);
	    	
	    	var dispalyFlightCount = (el.data('maxChildCount') == null) ? 1 : el.data('maxChildCount');
	    	$.each($(".correction"), function(){
	    		this.innerHTML = this.innerHTML.replace(/ \/ /gi, " " + UI_AvailabilitySearch.labels.lblTo + " ");
	    	});
    		createDatesInClander(settings.travelType, settings.data, null, settings.noOfDays,"#"+this.id+" .calendar-ul");
    		
	    	//setNavigationPosistion(el);
	    	dispalyFlightperDate($.data($("#"+this.id)[0], "selectedID"), this.id);
	    	
	    	function createDatesInClander(type, dataArray, current, nod, o){
	   		 var htmlToAdd = "";
	   		 if (dataArray == undefined || dataArray == undefined || dataArray == ""){
	   			 var htmlToAdd = "No flight data found for this Route";
	   			 $(o).html(htmlToAdd);
	   			 
	   			 if (type == 'dep'){
	   				 $("#fCalOut").next(".sub-div").hide();
	   			 	 $("#fCalOut").prev(".sub-div").hide();
	   			 }
	   		 	 if (type == 'arr'){
	   		 		$("#fCalIn").next(".sub-div").hide();
   			 	    $("#fCalIn").prev(".sub-div").hide();
	   		 	 }
	   		 }else{
		   		 if (nod == 1){
	   					 htmlToAdd = careteOneDay(dataArray,null, null);
		   		 }else{
		   			 if (type == 'dep'){
		   				 for (var i = 0; i<nod; i++){
		   					$(o).append(addChild(dataArray[i],'dep',nod));
		   			     };
		   			 }else{
		   				 for (var i = 0; i<nod; i++){
		   					$(o).append(addChild(dataArray[i],'arr',nod));
		   			     };
		   			 }
		   		 }
	   		 }

	    	};
	   	 
	   
	    function setDateFormat(date, selected){
	   		var temdate = date.split("/");
	   		var newDate = new Date(temdate[1]+"/"+temdate[0]+"/"+temdate[2]);
	   		
	   		if (settings.displayHeadeFormat = "ddd dd mmm"){
	   			return  "<div class='daySmall'>"+ (selected ? jsDaysFull[newDate.getDay()] : jsDays[newDate.getDay()])+"</div><div class='dateSmall'>"+newDate.getDate()+" "+jsMonths[newDate.getMonth()]+"</div>";
	   		}
	   	};
	 	
	   	 //create one day for the calendar
	   	 function careteOneDay(day,way,count){
	   		var lowerstAdded = false;
	   		//segment modification IBE
	   		//var isModifyDate = getModifyDateStaus();
			 if (day != undefined && day.availableFlightInfo != null ){
				var calOBJ = (way == "dep")? $("#fCalOut"):$("#fCalIn");
				if (day.availableFlightInfo.length != 0){
					if (dispalyFlightCount < day.availableFlightInfo.length) {
						dispalyFlightCount = day.availableFlightInfo.length;
						calOBJ.data('maxChildCount', dispalyFlightCount);
					};
				}else{
					if (way == "dep"){
						if ( day.departureDate == $("#resDepartureDate").val() && count != 1 ){
							day.selected = true;
						}
					}else{
						if ( day.departureDate == $("#resReturnDate").val() && count != 1 ){
							day.selected = true;
						}
					}
				}	
						var segmentTootltip = '';
						var flightuniqeID = '';
						var gradientID = "";
						var seletedClass= "";
						var selWidth = dayWidth;
						var top = 15;
						var ddVal = day.departureDate.split("/");
						var temFLID = "date_" + way + "_" + ddVal[0] +  ddVal[1] +  ddVal[2];
						selWidth = dayWidth;
						if (day.selected){
							calOBJ.find(".selected").css({"top":top, "width":selWidth});
							calOBJ.find(".getFlightDay").removeClass("selected");
							seletedClass = "selected";
							selWidth = calDisplayConfig.design.selectedWidth;
							top = 0;
						}
						var flightNumber = '';
						var htmlToAdd = $("<a></a>").attr({
							"id":temFLID,
							"class":seletedClass + " getFlightDay",
							"style":"width:"+selWidth+"px;top:"+top+"px;"
						});
						
						if(way == "dep" && day.selected){						
							selectedDepartureDate = day.departureDate;
						} else if(way != "dep" && day.selected) {
							selectedReturnDate = day.departureDate;
						}
						
						var lb=UI_AvailabilitySearch.labels;
						var sData = $("<div>"+setDateFormat(day.departureDate, false)+"</div>").attr("class","sData");
						var sDataFull = $("<div>"+setDateFormat(day.departureDate, true)+"</div>").attr("class","sDataFull");
						var fltAvailability = $("<div><span > "+UI_AvailabilitySearch.labels.lblNone+" </span></div>").attr("class","flightAvailability");
						var sFares = $("<div><span > - </span></div>").attr("class","minFareValue");
						if (day.minFare==null && day.availableFlightInfo.length>0 && !UI_AvailabilitySearch.isRequoteFlightSearch){
							var sFares = $("<div><span>"+UI_AvailabilitySearch.labels.fullFlights+"</span></div>").attr("class","minFareValue");
                            fltAvailability = $("<div><span >&nbsp;</span></div>").attr("class","flightAvailability");
						}else if (day.minFare!=null){
							var sFares = $("<div><span class='from'>"+UI_AvailabilitySearch.labels.lblBoarding+"</span> <span>"+ settings.currencyCode +"</span> <label>"+ day.minFare +"</label></div>").attr("class","minFareValue");
						}
						if(day.availableFlightInfo.length > 0 && day.minFare!=null && !UI_AvailabilitySearch.isRequoteFlightSearch){
							fltAvailability = $("<div><span > "+UI_AvailabilitySearch.labels.available+" </span></div>").attr("class","flightAvailability");
						} 
						
						var hspan = $("<span class='calCurrdate'>"+day.departureDate+"</span>").hide();
						sData.append(hspan)
						htmlToAdd.append($("<div class='cal-indBg cal-base'></div>").append(sData, sDataFull,sFares,fltAvailability));
						$.data(htmlToAdd[0], "availFlights", day.availableFlightInfo);
						//setting the selected date ID 
						if (day.selected){
							$.data(calOBJ[0], "selectedID", temFLID);
						}
						//change dateformats in calendara
						chnageDateElements = function(){
							if (SYS_IBECommonParam.locale=='fa' && !UI_Top.holder().GLOBALS.skipPersianDateChange){
								htmlToAdd.find(".sData, .sDataFull").find(".daySmall, .dateSmall").remove();
								htmlToAdd.find(".sData, .sDataFull").
								append("<div>"+dateFromGregorian(htmlToAdd.find(".calCurrdate").text(),SYS_IBECommonParam.locale)+"</div>");
							}
						}
						chnageDateElements();
						htmlToAdd.click(function(){
							UI_commonSystem.showProgress();
							oldSelectedObj = calOBJ.find(".selected");
							oldSelectedObj.removeClass("selected");
							htmlToAdd.addClass("selected");
							calOBJ.find(".getFlightDay").css({top:15, width:dayWidth});
							$(this).animate({width: calDisplayConfig.design.selectedWidth, top:0},{duration:500,
								complete: function(){
									if(func.getAvailabilityForSelectedDate(htmlToAdd,oldSelectedObj, calOBJ.attr("id"))){
										//func.flightLabels = $.extend(func.flightLabels, $.data(htmlToAdd[0], "availLogicalCC"));	
										var currentAvailLCC = $.data(htmlToAdd[0], "availLogicalCC");
										if(currentAvailLCC != null && currentAvailLCC.length > 0){											
											func.flightLabels = $.data(htmlToAdd[0], "availLogicalCC");
										} else {
											//Sends a dummy logical cabin class if no lcc is available
											func.flightLabels = defaultflightLabels;
										}
										dispalyFlightperDate(this.id, null);
										$("#"+calOBJ.attr("id")+"_FL").find("input[type='radio']").attr("checked",false);
										//setting the selected date ID 
										$.data(calOBJ[0], "selectedID", this.id);
								   		$("#trPriceBDPannel").slideUp("slow");
								   		setLowasetFare(calOBJ.attr("id")+"_FL", null);
								   		$.fn.calendarview.callTooltip();
								   		$("#tempDiscountBanner").remove();
								   		$("#tempPromoBanner").remove();
									}else{
										$(this).removeClass("selected");
										$(this).css({top:15, width:dayWidth});
										oldSelectedObj.addClass("selected");
										oldSelectedObj.animate({width: selectedwidth, top:0},{duration:500});
									}
									dateChangerjobforLanguage(SYS_IBECommonParam.locale);
								}
							});
							
						});
					/*}	else{
					var selectClass = "";
					top = 0;
					selWidth = dayWidth;
					if ( day.departureDate == $("#resDepartureDate").val() && count != 1 ){
						selectClass = "selected";
						selWidth = selectedwidth;
						top = 0;
					}
					var lblNoFl = $('<div class="totalValue noFlight">'+UI_AvailabilitySearch.labels.noFlights+'</div>');
					var tNoflight = $('<div class="cal-indBg cal-base flightNo zeromargin" style="width:'+dayWidth+'px"></div>');
					var sData = $("<div>"+setDateFormat(day.departureDate, false)+"</div>").attr("class","sData");
					var hspan = $("<span class='calCurrdate'>"+day.departureDate+"</span>").hide();
					sData.append(hspan);
					//var divBase = $('<div class="cal-base flightNo "></div>').append();
					var htmlToAdd = $("<a></a>").attr({
						"class":selectClass + " getFlightDay",
						"style":"width:"+selWidth+"px;top:"+top+"px;"
					});
					htmlToAdd.append(tNoflight.append(sData, lblNoFl));
				}*/
			 }
			 /* not happending */
			 /*else{
				 	var selectClass = "";
				 	top = 0;
					selWidth = dayWidth;
					if ( day.departureDate == $("#resDepartureDate").val() && count != 1){
						selectClass = "selected";
						selWidth = selectedwidth;
						top = 0;
					}
					var lblNoFl = $('<div class="totalValue noFlight">'+UI_AvailabilitySearch.labels.noFlights+'</div>');
					var tNoflight = $('<div class="cal-indBg cal-base flightNo zeromargin" style="width:'+dayWidth+'px"></div>');
					var sData = $("<div>"+setDateFormat(day.departureDate, false)+"</div>").attr("class","sData");
					var hspan = $("<span class='calCurrdate'>"+day.departureDate+"</span>").hide();
					sData.append(hspan);
					//var divBase = $('<div class="cal-base flightNo "></div>').append();
					var htmlToAdd = $("<a></a>").attr({
						"class":selectClass + " getFlightDay",
						"style":"width:"+selWidth+"px;top:"+top+"px;"
					});
					htmlToAdd.append(tNoflight.append(sData, lblNoFl));
			 }*/
			 return htmlToAdd;
		 };
		 
		 /**
		  * 
		  * flightsArrayForAllDays : The flight infomation for the whole period. 
		  * 						(dto: AvailableFlightInfoByDateDTO.java)	
		  * depOrReturn : flag indicating whether flights are departures of arrivals ["dep" || "arr"]
		  * calDOMObjs : New calendar DOM objects containing. Structure :  { body=, header=} 
		  * numOfDays : The number of days : TODO - verify whether this is really wanted.
		  * dateArray : The date array containing the dates of the period in which flights are created.
		  */
		 function createNewCalendarContent(flightsArrayForAllDays, depOrReturn,calDOMObjs, numOfDays,dateArray, moveDirection){			 
			 //iterate over the date array and set the html contents for bodies and headers.
			 for(i=0; i<dateArray.length; i++){				 
				 var matchingFlightInfoFound = false;	

				 if (flightsArrayForAllDays != undefined && flightsArrayForAllDays != null ){
					 for(j=0; j<flightsArrayForAllDays.length ; j++ ){
						 if(dateArray[i] == flightsArrayForAllDays[j].departureDate){							 
							 $(calDOMObjs[i].body).html(careteOneDay(flightsArrayForAllDays[j],depOrReturn,numOfDays));
							 //$(calDOMObjs[i].header).html(careteOneDayHeader(flightsArrayForAllDays[j], depOrReturn, numOfDays, dateArray[i]));
							 matchingFlightInfoFound = true;
							 break;
						 } 
					 }
				 }
				 //if no day is matched that means we have no flights!
				 if (!matchingFlightInfoFound){
					 //pass null and no flights representation would be generated by respective functions
					 $(calDOMObjs[i].body).html(careteOneDay(null,depOrReturn,numOfDays));
					 //$(calDOMObjs[i].header).html(careteOneDayHeader(null, depOrReturn, numOfDays, dateArray[i]));
				 }
			 }
			 /**AARESAA-13367 fix.
			  * Because the timeout of dB.animate function some times at this point 
			  * there can be 8 li elements in calendar-ul so that below click function triger will
			  * not work as expected
			  */
			 
			 /**var calOBJ = (depOrReturn == "dep")? "fCalOut":"fCalIn";
			 if( $("#"+calOBJ).find("a.selected").length<=0){
				 if (moveDirection == "next"){ 
					 $("#"+calOBJ).find("ul.calendar-ul li:first-child").find("a").trigger("click");
				 }else{
					 $("#"+calOBJ).find("ul.calendar-ul li:last-child").find("a").trigger("click");
				 }
			 }else{
				 bindEventsNextPrev();
			 }*/
			 removeSelectedFromCal(calID);
		 };
		 		 		
		 function createRoot(seg,current,segLength){
			 var strSEG = "";
			 if(current == (segLength-1)){
				 strSEG = seg.split("/")[0]+ " / " + seg.split("/")[1];
			 }else{
				 strSEG = seg.split("/")[0]+ " / "; 
			 }
			return strSEG;
		 }
		 
		 $.fn.calendarview.callTooltip = function(){			 
			 $(".ttip").tooltip({
				    bodyHandler: function() { 
				    	var text = $(this).attr('role').split("|");
				    	var html = '';
				    	if(text[1] != undefined && text[1] != null 
				    			&& text[1] != '' && text[1] != 'null'){
				    		html += "<img src='"+text[1]+"'/><br/>";
				    	}
				 		return html; 
				    }, 
				    showURL: false,
				    delay: 0,
				    track: true,
                    top: 10,
                    extraClass: "caltooltip"
			 });
  	     	var sHeagth = 0;
	   		if ($(".cal-base").css("height") == "NaN"){ sHeagth = 1}else {
	   			sHeagth = parseInt($(".cal-base").css("height"))+parseInt($(".cal-base").css("margin"))+parseInt($(".cal-base").parent().css("padding"), 10);
	   		}
	   		if (calDisplayConfig.layout.moreThanTwoFlight == "SCROLL"){
	   			var total = $("#"+calID).find(".flight-body").children();
	   			var nok = (total.length>7)? dispalyFlightCount: 1;
	   			$("#"+calID).find(".c-scroller").css("height",Number(nok*sHeagth + 8));
	   			
	   			if (!$.browser.msie && $("body").css("direction") == "rtl")
		   	    	$("#"+calID).find(".c-scroller").css({"position":"relative", "left":"17px"});
	   		}
		 };
		 
		 function fallDecimals(count,value){
			 var falls = ( String(value).indexOf(".") >= count ) ? falls = parseInt(value,10) : value;
			 return falls;
		 }
		 
		 function getEgdeFare(calIDVal,maxMin){
			 var FareCollection = [];
			 var ind = 0;
			 $.each($("#"+calIDVal).find(".totalValueSelected > label.currValueTotal"),function(i,j){
				 if($(j).parent().parent().find("span.full").length == 0){
					 FareCollection[ind++] = parseFloat(j.innerHTML,10);
				 }
			 });
			 var MinMaxFire = 0;
			 if (maxMin.toUpperCase() == "MIN"){
				 MinMaxFire = (FareCollection == "" || FareCollection == undefined)? 0 : Math.min.apply( Math, FareCollection );
			 }else if (maxMin.toUpperCase() == "MAX"){
				 MinMaxFire = (FareCollection == "" || FareCollection == undefined)? 0 : Math.max.apply( Math, FareCollection );
			 }
			 return MinMaxFire;
		 } 
		 
	 
		 /*function sendStopsCount(index){
			 var nomArr = UI_AvailabilitySearch.stopovers  || ["Non","One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];
			 if (index == 0)
				 return "<span class=''>" + nomArr[index] + "</span> <span class='lblStopover'>stop</span>";
			 else if (index == 1)
				 return "<span  class='tootTip'>" + nomArr[index] + "</span> <span class='lblStopover'>stop</span>";
			 else
				 return "<span class='tootTip'>" + nomArr[index] + "</span> <span class='lblStopovers'>stops</span>"; 
		 }*/
		 
		 /*function getFarelavel(flevel){
			 var str = null;
			 if (flevel != null || flevel != undefined){
				 switch (parseInt(flevel, 10))
				 {
				 case 2:
					 str = "FareGridBorder02"
					  break;
				 case 3:
					 str = "FareGridBorder03"
					  break;
				 case 4:
					 str = "FareGridBorder04"
					  break;
				 case 5:
					 str = "FareGridBorder05"
					  break;
				 default:
					 str = "FareGridBorder01";
				 }
			 }
			return str;
		 };*/
		 
		 function removeSelectedFromCal(cal){
			if ( $("#"+cal).find(".selected").length == 0){
				 $("#"+cal).find(".getFlightDay").animate({width: 90},{duration:500});
			}
		 }
		 
		 function addChild (d,w,c){
			 if (c == 1){
				 	var tLi = $('<li><div class="getDay cal-day flight-body"><div style="width:0px;"><img src="'+loading.src+'" align="center" style="visibility:hidden"/></div></div></li>').attr("style",dayGap);
			 	}else{
			 		var tLi = $('<li></li>').attr("style",dayGap);
			 		var divCalDay = $("<div></div>").attr({"class": "getDay cal-day flight-body"});
			 		//creating Flight
			 		tLi.append(divCalDay.append(careteOneDay(d,w,c)));
			 	}
			 return tLi;
		 };
		 
		 
		 function getDayofDate(d){
			 var day = null;
			 var temdate = d.split("/");
		   		var newDate = new Date(temdate[1]+"/"+temdate[0]+"/"+temdate[2]);
		   		day =  jsDays[newDate.getDay()];
			 return day
		 }

		 function dataformater(txt){
			 if (txt != null || txt != undefined) 
				 text = txt.split("/");
			 var dat = null;
			 if ($.browser.msie)
				 dat = Number(text[2])+"/"+Number(text[1])+"/"+Number(text[0]);
			 else
				 dat = Number(text[1])+"/"+Number(text[0])+"/"+Number(text[2]);
			 
			 return dat;
		 };
		 
		 bindEventsNextPrev();
		 $(document).on("click",".sub-div-additional", function(event){
			 //the length of the navigation is the calendar length.
			 nextPrevBuilderFn(this, event,$(this).parent().parent().next().find("div.cal-header ul li").length);
		 });
		 
		 function bindEventsNextPrev(){
			 $(".sub-div").unbind().bind("click", function(event){
				 nextPrevBuilderFn(this, event,1); 
			 });
		 }
		 
		 function nextPrevBuilderFn(xthis, event, noOfDays){
			 calID = $(xthis).parent().parent().next().find("div").attr("id") || $(xthis).parent().parent().prev().find("div").attr("id");
			 var dat = (calID == "fCalOut")? "dep":"arr";
			 var current = null
			 //The animation duration for appearing and disappearing the calendar day pages.
			 var animationDuration = 250;
			 //to overcome the display issues for several days.
			 if (noOfDays >1){
				 animationDuration = 0;
			 }
			 
			 
			 if ($(xthis).hasClass("cal_previous") == true){
				 if(func.getDataForAllDays != undefined){
					 if ($(xthis).parent().parent().parent().find(".calendar-ul").children().length == func.noOfDays &&
							 $(xthis).parent().parent().parent().find("ul.calendar-ul>li:first-child").find(".sData").html() != "&nbsp;"){
					 				 						
						 // direction - previous						 
						 var dBs = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li");
						 
						 var toDayStr = new Date();						 
						 var todayIntVal =  dateVal(dataformater(toDayStr.getDate() + "/" + Number(toDayStr.getMonth()+1) + "/" + toDayStr.getFullYear())).d;
						 						 
						 var currentFirstDateStr = $(dBs[0]).find(".sData > span").html();
						 var navigateToDateStr = addDaysAsString(currentFirstDateStr , -noOfDays);
						 var navigateToDateIntVal = dateVal(dataformater(navigateToDateStr)).d;

						 // date validation - the navigated first date should be greater than the 
						 // current date.
						 if (navigateToDateIntVal >= todayIntVal){
							 var length = noOfDays;// $(dHs).length;	
							 var calendarLength = $(dBs).length;

							 //the middle date of the calendar.
							 var pivotalDate = null;
							 // var pivotalDateNumber = ((length -1)/2)+1;
							 //get the pivital date index to get the date value.
							 var pivotalDateIndex = (calendarLength -1) - ((((length -1)/2)+1) -1);							

							 //we need to get the array of dates for the new calendar pages.
							 //this needs to be done before dates are removed from the dom.
							 var dateArrayForNewCalendar = [];						 

							 //calculate the departure and arrival variance.
							 var departureVariance = null;
							 var arrivalVariance = null;

							 if (dat == "dep"){
								 departureVariance = (length -1)/2;
								 // NOTE : There's no difference between arrival variance and departure variance as of now.
								 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
								 arrivalVariance = departureVariance;
							 }						 						 
							 if (dat == "arr"){
								 arrivalVariance = (length -1)/2;		
								 // NOTE : There's no difference between arrival variance and departuer variance as of now.
								 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
								 departureVariance = arrivalVariance;
							 }

							 //array to hold new headers and bodies.
							 var calSearchDataArr = [];
							 var date = null;

							 var loopStartIndex = calendarLength - 1; // start from the end of the calendar.
							 var loopEndIndex = pivotalDateIndex - arrivalVariance;

							 //temp -previous click
							 for (i=loopStartIndex; i>=loopEndIndex; i--){

								 var bBody = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li:first-child");

								 var dBody = $(dBs[i]);
							
								 if(date == null){
									 date =  bBody.find(".sData span").html();
								 }
								 date = addDaysAsString(date , -1);

								 dateArrayForNewCalendar[dateArrayForNewCalendar.length] = date;

								 if(i == pivotalDateIndex){
									 pivotalDate = date;
								 }

								 current = dataformater(bBody.find(".sData span").html());
								 current = dateVal(current).d + 1;	

								 bBody.before(addChild(null,null,1));

								 bBody = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li:first-child").children().children();

								 //to resolve scoping issues after animation time outs.
								 (function(dB,  bB){
									 dB.animate({width: 1},{duration:animationDuration,
										 complete: function() {
											 //AARESAA-13367 fix
											 if (dB.find("a").hasClass("selected")){
												 dB.prev().find("a").trigger("click");
											 }
											 dB.remove();
										 }});

									 (function(bBody){
										 bBody.animate({width: dayWidth},{duration:animationDuration,
											 complete: function() {
												 bBody.children().css("visibility","visible");
												 //alert('working');
												 //func.getData(event, nextDate,dat,7,bBody,"next",careteOneDay,bHeader,careteOneDayHeader);
											 }});
									 })(bB);	 
								 })(dBody, bBody);

								 // get the data to an array.
								 var obj = {									
										 body:bBody.parent()
								 };
								 calSearchDataArr[calSearchDataArr.length] = obj;							 
							 }

							 //collect all the data required for the action.
							 var nextPrevCalSearchParams = {
									 calDataArray : calSearchDataArr,
									 calNextDatesArray : dateArrayForNewCalendar,
									 noOfDays : 1,
									 depVar : departureVariance,
									 arrVar : arrivalVariance,
									 pivotalDate : pivotalDate,
									 depOrReturn : dat,
									 calCreateContentCB : createNewCalendarContent, 
									 moveDirection : "prev",
									 calendarLength : calendarLength
							 };
							 func.getDataForAllDays(nextPrevCalSearchParams);
						 }
 
					 }
				 }
			 }else{
				 if(func.getDataForAllDays != undefined){
					 
					 if ($(xthis).parent().parent().parent().find(".calendar-ul").children().length == func.noOfDays &&
							 $(xthis).parent().parent().parent().find("ul.calendar-ul>li:last-child").find(".sData").html() != "&nbsp;"){
						 
						 
						 var dBs = $(xthis).parent().parent().prev().find("div.c-scroller ul.calendar-ul li");
						 
						 var calendarLength = $(dBs).length;						 
						 var length = noOfDays;// $(dHs).length;				
						 
						 //the middle date of the calendar.
						 var pivotalDate = null;

						 //get the pivital date index to get the date value.
						 var pivotalDateIndex = (((length -1)/2)+1) -1;
						 
						 //we need to get the array of dates for the new calendar pages.
						 //this needs to be done before dates are removed from the dom in the next for loop.
						 var dateArrayForNewCalendar = [];						 

						 //calculate the departure and arrival variance.
						 var departureVariance = null;
						 var arrivalVariance = null;
						 if (dat == "dep"){
							 departureVariance = (length -1)/2;
							 // NOTE : There's no difference between arrival variance and departure variance as of now.
							 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
							 arrivalVariance = departureVariance;
						 }						 
						 
						 if (dat == "arr"){
							 arrivalVariance = (length -1)/2;		
							 // NOTE : There's no difference between arrival variance and departuer variance as of now.
							 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
							 departureVariance = arrivalVariance;
						 }
						 
						 //array to hold new headers and bodies.
						 var calSearchDataArr = [];
						 var date = null;
						 for (i=0; i<length; i++){
							 
							 var dBody = $(dBs[i]); 
							 var bBody = $(xthis).parent().parent().prev().find("div.c-scroller ul.calendar-ul li:last-child");
							 			
							 if(date == null){
								 date =  bBody.find(".sData span").html();
							 }
							 date = addDaysAsString(date , 1);
							 
							 dateArrayForNewCalendar[dateArrayForNewCalendar.length] = date;
							 
							 if(i == pivotalDateIndex){
								 pivotalDate = date;
							 }
							 
							 current = dataformater(bBody.find(".sData span").html());
							 current = dateVal(current).d + 1;	
							 
							 bBody.after(addChild(null,null,1));
							 
							 bBody = $(xthis).parent().parent().prev().find("div.c-scroller ul.calendar-ul li:last-child").children().children();
							 
							 //to resolve scoping issues after animation time outs.
							 (function(dB, bB){
								 dB.animate({width: 1},{duration:animationDuration,
									 complete: function() {
										 //AARESAA-13367 fix
										if (dB.find("a").hasClass("selected")){
											 dB.next().find("a").trigger("click");
										 }
										 dB.remove();
									}});

								 (function(bBody){
									 bBody.animate({width: dayWidth},{duration:animationDuration,
										 complete: function() {
											 bBody.children().css("visibility","visible");
											 	//alert('working');
										 }});
								 })(bB);	 
							 })(dBody, bBody);
							 
							 // get the data to an array.
							 var obj = {									
									body:bBody.parent()
							 };
							 calSearchDataArr[calSearchDataArr.length] = obj;							 
						 }
						 
						 //collect all the data required for the action.
						 var nextPrevCalSearchParams = {
								 calDataArray : calSearchDataArr,
								 calNextDatesArray : dateArrayForNewCalendar,
								 noOfDays : 1,
								 depVar : departureVariance,
								 arrVar : arrivalVariance,
								 pivotalDate : pivotalDate,
								 depOrReturn : dat,
								 calCreateContentCB : createNewCalendarContent, 
								 moveDirection : "next",
								 calendarLength : calendarLength									
						 };
						 
						 func.getDataForAllDays(nextPrevCalSearchParams);
					 }	 
				 }
			 }
			 
		 };
		 
		 function addDaysAsString(strDate, intDays){
			 if (strDate != null){
				 var dtDate = new Date(Number(strDate.substring(6,10)), Number(strDate.substring(3,5))-1, Number(strDate.substring(0,2)));
				 var gmtOffsetOri = dtDate.getTimezoneOffset();
				 var calNewDate = new Date(dtDate.getTime() + Number(intDays) *24*60*60*1000);
				 var gmtOffsetNew = calNewDate.getTimezoneOffset();
				 //Assumption they won't be multiple dst changes with these days
				 if (gmtOffsetOri != gmtOffsetNew){
					 var offsetDifference = gmtOffsetOri - gmtOffsetNew;
					 calNewDate = new Date(calNewDate.getTime() - (offsetDifference*60*1000));	
				 } 
				 return DateToString(calNewDate);
			 }
		};
		
		/*function setNavigationPosistion(obj){
			var w = obj.width();
			var h = obj.height();
			obj.parent().parent().find(".sub-div").css({
				paddingTop:h/2 - 10
			});
			obj.parent().parent().find(".sub-div").find("a").css({
				height:h/2 + 10
			});
		}*/
		
	
		function createDummyFareSummaryList(smList, availFT){
			smnList =[];
			$.each(availFT , function(ind , key){
				var tBool = false;
				for (var i=0; i<smList.length; i++){
					if (key.logicalCCCode == smList[i].logicalCCCode && key.withFlexi == smList[i].withFlexi && key.bundledFarePeriodId == smList[i].bundledFarePeriodId){
						tBool = true;
						smnList.push(smList[i]);
						break;
					}
				}
				if (!tBool){
					var tFare = {"logicalCCCode":key.logicalCCCode, "logicalCCDesc":key.logicalCCDesc, "seatAvailable":false, "selected":false, "withFlexi":key.withFlexi};
					smnList.push(tFare);
				}
			});
			return smnList
		}
		
		function dispalyFlightperDate(dateId, calid){
			var getViaPointsText = function(viaPoint){
				var nomArr = UI_AvailabilitySearch.stopovers  || ["No","1", "2", "3", "4", "5", "6", "7", "8", "9"];
				var text = "";
				if (settings.dispalyItems.stopOverDisplayBy != undefined && settings.dispalyItems.stopOverDisplayBy.toLowerCase() == "number"){
					if (viaPoint.length == 0){
						text += viaPoint.length;
					}else{
						text += nomArr[viaPoint.length];
					}
				}else{
					text = nomArr[viaPoint.length] + "&nbsp;";
					text += (viaPoint.length==1)?UI_AvailabilitySearch.labels.shortover:UI_AvailabilitySearch.labels.shortovers;					
					if (viaPoint.length > 0){
						text += "<br>(";
						for (var i=0;i<viaPoint.length; i++){
							if (i == viaPoint.length-1){
								text += viaPoint[i];
							}else{
								text += viaPoint[i] + " | ";
							}
						}
						text += ")";
					}
				}
				
				return text;
			};
			var flGridHeader = function(flsBody){
				if (func.flightLabels.length == 0){func.flightLabels = defaultflightLabels};
				var noFareTypes = getObjlength(func.flightLabels);
				var noOfCols = calDisplayConfig.design.noStopColumn?4:3;
				var widthOfCol = (100-calDisplayConfig.design.fareColumnWidth)/noOfCols;
				var trH = $("<tr></tr>");
				var tdFLNo = $("<td width='"+widthOfCol+"%' class='gridHD NotFare' align='center'></td>").append("<label class='gridHDFont'>"+UI_AvailabilitySearch.labels.lblFlightNo+"</label>");
				var tdDep = $("<td width='"+widthOfCol+"%' class='gridHD NotFare' align='center'></td>").append("<label class='gridHDFont'>"+UI_AvailabilitySearch.labels.lblDepature+"</label>");
				var tdArr = $("<td width='"+widthOfCol+"%' class='gridHD NotFare' align='center'></td>").append("<label class='gridHDFont'>"+UI_AvailabilitySearch.labels.lblArrival+"</label>");
				var tdStops = null;
				if (calDisplayConfig.design.noStopColumn){
					tdStops = $("<td width='"+widthOfCol+"%' class='gridHD NotFare' align='center'></td>").append("<label class='gridHDFont' style='text-transform: capitalize'>"+UI_AvailabilitySearch.labels.shortovers+"</label>");
				}
				trH.append(tdFLNo,tdDep, tdArr, tdStops);
				if ( noFareTypes> 0){
						var widthFare = calDisplayConfig.design.fareColumnWidth/noFareTypes + "%";
						$.each(func.flightLabels, function(i, obj){
							if (obj.logicalCCCode != ""){
								var objKey = obj.logicalCCCode + '_' + (obj.withFlexi ? 'Y' : 'N');
								var tdFareType = $("<td class='gridHD fareClass thinBorderL "+objKey+"_class' align='center' width='"+widthFare+"'></td>")
								.append("<label id='"+objKey+"' class='gridHDFont'>"+obj.logicalCCDesc+" <span role='"+obj.comment+ "|" + obj.imageUrl +"' class='ttip'>&nbsp;<img src='../images/spacer_no_cache.gif' width='7' height='13' alt=''/></span></label>");
								trH.append(tdFareType);
							}else{
								var tdFareType = $("<td class='gridHD' align='center' width='60%'></td>").append("<label  class='gridHDFont'>&nbsp;</label>");
								trH.append(tdFareType);
							}
						});
				}else{
					var tdFareType = $("<td class='gridHD' align='center' width='60%'></td>").append("<label  class='gridHDFont'>&nbsp;</label>");
					trH.append(tdFareType);
				}
				flsBody.find("thead").empty();
				flsBody.find("thead").append(trH);
				flsBody.find("tbody").empty();
				var getFLDetails = function(j,flg){
					var str = '';
					$.each(j, function(){
						str += this[flg] + "<br/>"; 
					})
					return str
				}
				
				var getFLDetailsWithoutBreak = function(j,flg){
					var str = '';
			
					$.each(j, function(){
						if(this['csOcCarrierCode']!=null) {
							str += this[flg]+" "; 
						}
					})
					return str == "null" ? "" : str
				} 
				
				var getTransitDuration = function(flight){
					var str = '';
					if (flight.transitDuration != null){
						str = flight.transitDuration;
					}				
					return str;
				} 
				
				var getFlightDetails = function(flight,lblFlightDuration,lblAircraftModel,lblOperatedBy) {
					var string = '';
					$.each(flight.segments, function(){
						if (this['csOcCarrierCode'] != null) {
							string += this['flightNumber'] + " "
								+ lblFlightDuration + "="
								+ this['duration'] + ","
								+ lblAircraftModel + "="
								+ this['flightModelNumber'] + ","
								+ lblOperatedBy + "="
								+ this['csOcCarrierCode'] + "; "
							}
					})
					return string.slice(0,-2);
				}
				
				
				if (avaiFlight != null && avaiFlight.length > 0){
					$.each(avaiFlight, function(k, j){
						var toolTipForCSFlight = "";
						if(getFLDetailsWithoutBreak(j.segments, 'csOcCarrierCode') != "") {
							var lblFlightDuration = UI_AvailabilitySearch.labels.lblFlightDuration;
							var lblTransitDuration = UI_AvailabilitySearch.labels.lblTransitDuration;
							var lblAircraftModel = UI_AvailabilitySearch.labels.lblAircraftModel;
							var lblOperatedBy = UI_AvailabilitySearch.labels.lblOperatedBy;			
							var transitDuration = getTransitDuration(j);						
							if(transitDuration != "") {
								toolTipForCSFlight = "title = '"+getFlightDetails(j,lblFlightDuration,lblAircraftModel,lblOperatedBy)+" : "+lblTransitDuration+" = "+transitDuration+"'";
							} else {
								toolTipForCSFlight = "title = '"+getFlightDetails(j,lblFlightDuration,lblAircraftModel,lblOperatedBy)+"'";
							}
						}
						var evenClass = (k%2==0)?"even":"";
						var trF = $("<tr class='"+evenClass+"'></tr>");
						var tdFNO = $("<td class='GridItems ' "+toolTipForCSFlight+" align='center'></td>").append("<label>"+getFLDetails(j.segments, 'flightNumber')+"</label>");
						var tdFDep = $("<td class='GridItems ' align='center'></td>").append("<label>"+getFLDetails(j.segments, 'departureTime')+"</label>");
						var tdFArr = $("<td class='GridItems ' align='center'></td>").append("<label>"+getFLDetails(j.segments, 'arrivalTime')+"</label>");
						var tdFStops = null;
						if (calDisplayConfig.design.noStopColumn){
							tdFStops = $("<td class='GridItems ' align='center'></td>").append("<label>"+getViaPointsText(j.viaPoints)+"</label>");
						}
						trF.append(tdFNO, tdFDep, tdFArr, tdFStops);
						if (j.flightFareSummaryList.length > 0 && noFareTypes > 0){
							var fareTypesArr = createDummyFareSummaryList(j.flightFareSummaryList, func.flightLabels);
							for (var d=0; d<noFareTypes; d++){
                                var tClass = getFLDetails(j.segments, 'flightNumber').replace(/<br\/>/g,"|");
								if (fareTypesArr[d].seatAvailable){
									var flID = dateId + "_" + k + "_" + fareTypesArr[d].logicalCCCode + "_" + (fareTypesArr[d].withFlexi ? 'Y' : 'N') + "_" + (fareTypesArr[d].freeFlexi ? 'Y' : 'N')+"_"+fareTypesArr[d].bundledFarePeriodId+"_"+$.toJSON(fareTypesArr[d].bookingCodes)+"_"+$.toJSON(fareTypesArr[d].segmentBookingClasses);
                                    tClass += "_"+fareTypesArr[d].logicalCCCode + "_" + (fareTypesArr[d].withFlexi ? 'Y' : 'N') + "_" + (fareTypesArr[d].freeFlexi ? 'Y' : 'N') + "_"+ fareTypesArr[d].bundledFarePeriodId;
									var inputItems = $("<input type='radio'/>").attr({
										"id":flID + "_CHK",
										"name":dateId+"_CHK",
										"checked": ( j.selected && fareTypesArr[d].selected )? true:false,
                                        "class":tClass
									});
									var lblItem =  $("<span><span class='currLabel'>"+ settings.currencyCode +"</span> <label class='currValueTotal'>"+fareTypesArr[d].calDisplayAmout+"</label></span>").attr("class","totalValueSelected");
									var flLCOS = $("<span>"+fareTypesArr[d].logicalCCCode+"</span>").attr({"class":"flightLCOS", "style":"display:none"});
									var flRefID = $("<span>"+getFlightRefIds(j.segments)+"</span>").attr({"class":"flightRefId", "style":"display:none"});
									var lblAvailSeat = $("<span></span>").css({"display":"none"});
									if (fareTypesArr[d].noOfAvailableSeats > -1){
										var blClass = (calDisplayConfig.design.blinks.onlyNSeat)?"blinkItem":"";
										lblAvailSeat = $("<span class='"+blClass+"'> " + UI_AvailabilitySearch.labels.availableSeats.replace("{0}", fareTypesArr[d].noOfAvailableSeats) + "</span>").css({"display":"block", "fontSize":"10px", "color":"#737373"});
									}
									var webonly = "";
									var webOnlyImg = null;
									if (fareTypesArr[d].visibleChannelName != null && fareTypesArr[d].visibleChannelName.toUpperCase() == "WEB"){
										webonly = "web-only";
										var blClass = (calDisplayConfig.design.blinks.webonly)?"blinkItem":"";
										webOnlyImg = $("<span class='"+blClass+"'>"+UI_AvailabilitySearch.labels.webOnly+"</span>").css({
											"color":"#238906",
											"font-size":"10px"
										});
									}
									var lowerstCon = $("<span class='lowerstCon "+webonly+"'></span>").append(webOnlyImg);
									var tdFFareType = $("<td align='center' class='GridItems thinBorderL tdFareColumn' id='"+flID+"_TD'></td>").append(inputItems, lblItem, flRefID, flLCOS, lblAvailSeat, lowerstCon);
									tdFFareType.click(function(event){
											$(this).find("input[type='radio']").attr("checked", true);
											var __selctedItem = $(this).find(".flightRefId").text();
											var __duration = $(this).find(".totalDuration").text();
											if (func.selectFlight!= undefined){
												func.selectFlight(event,__selctedItem,"departure",__duration);
										}
									});
									tdFFareType.mouseover(function(){
										$(this).addClass("selected");
									});
									tdFFareType.mouseout(function(){
										$(this).removeClass("selected");
									});
								}else{
									var lowerstCon = $("<span class='lowerstCon'></span>");
									var tdFFareType = $("<td class='GridItems  thinBorderL tdFareColumn' align='center' ></td>").append("<label>"+UI_AvailabilitySearch.labels.notAvailable+"</label>", lowerstCon);
								}
								trF.append(tdFFareType);
							}
						}else{
							if (UI_AvailabilitySearch.isRequoteFlightSearch && j.seatAvailable){
								var flID = dateId + "_" + k ;
								var inputItems = $("<input type='radio'/>").attr({
									"id":flID + "_CHK",
									"name":dateId+"_CHK",
									"checked": ( j.selected && fareTypesArr[d].selected )? true:false
								});
								var lblItem = $("<span><span class='currLabel'></span> <label class='lblNotAvailable'>Available</label></span>").attr("class","totalValueSelected");
								var fareTypesArr = createDummyFareSummaryList(j.flightFareSummaryList, func.flightLabels);
								var flLCOS = '';
								if (noFareTypes > 0) {
									for (var d=0; d<noFareTypes; d++){
										flLCOS = $("<span>"+fareTypesArr[d].logicalCCCode+"</span>").attr({"class":"flightLCOS", "style":"display:none"});
									}
								} else {
									flLCOS = $("<span></span>").attr({"class":"flightLCOS", "style":"display:none"});
								}
								var flRefID = $("<span>"+getFlightRefIds(j.segments)+"</span>").attr({"class":"flightRefId", "style":"display:none"});
								var lowerstCon = $("<span class='lowerstCon'></span>");
								var tdFFareType = $("<td align='center' class='GridItems thinBorderL tdFareColumn' id='"+flID+"_TD'></td>").append(inputItems, lblItem, flRefID, flLCOS, lowerstCon);
								tdFFareType.click(function(event){
                  $(this).find("input[type='radio']").attr("checked", true);
                  var __selctedItem = $(this).find(".flightRefId").text();
                  var __duration = $(this).find(".totalDuration").text();
                  if (func.selectFlight!= undefined){
                    func.selectFlight(event,__selctedItem,"departure",__duration);
                }
              });
							}else{
								var lowerstCon = $("<span class='lowerstCon'></span>");
								var tdFFareType = $("<td class='GridItems  thinBorderL tdFareColumn' align='center' colspan='"+noFareTypes+"'></td>").append("<label id='lblFlightFull'>"+UI_AvailabilitySearch.labels.fullFlights+"</label>", lowerstCon);
							}
							trF.append(tdFFareType);
						}
						flsBody.find("tbody").append(trF);
					});
				}else{
					var trNF = $("<tr class='even'></tr>");
					var tdFNO = $("<td class='GridItems ' align='center'></td>").append("<label> - </label>");
					var tdFDep = $("<td class='GridItems ' align='center'></td>").append("<label> - </label>");
					var tdFArr = $("<td class='GridItems ' align='center'></td>").append("<label> - </label>");
					var tdFStops = null
					if (calDisplayConfig.design.noStopColumn){
						 tdFStops = $("<td class='GridItems ' align='center'></td>").append("<label> - </label>");
					}
					//var tdNF = $("<td class='GridItems  thinBorderL tdFareColumn' colspan='"+ noFareTypes +"' align='center'></td>").append("<label>"+UI_AvailabilitySearch.labels.notAvailable+"</label>");
					trNF.append(tdFNO, tdFDep, tdFArr, tdFStops);
					for (var d=0; d<noFareTypes; d++){
						var tdFFareType = $("<td class='GridItems  thinBorderL tdFareColumn' align='center'></td>").append("<label>"+UI_AvailabilitySearch.labels.notAvailable+"</label>");
						trNF.append(tdFFareType);
					}
		
					flsBody.find("tbody").append(trNF);
				}
			}
			if (dateId !=null){
				var getFlightRefIds = function(segs){
					var refIDs = "";
					if (segs.length > 0){
						$.each(segs, function(dt, at){
							if (dt == segs.length-1){
								refIDs += this.flightRefNumber;
							}else{
								refIDs += this.flightRefNumber + ":";
							}
						});
					}
					return refIDs;
				};
				

							
				var avaiFlight = $.data($("#"+dateId)[0], "availFlights");
				var calObj = (dateId.split("_")[1]=="dep")?$("#fCalOut"):$("#fCalIn");
				var flsBody = calObj.parent().parent().next(".AllFlight").children().children(".thisFls");
				flGridHeader(flsBody);
				flsBody.show();
				setLowasetFare(null, dateId.split("_")[1]);
			}else{
				var flsBody = $("#"+calid).parent().parent().next(".AllFlight").children().children();
				flGridHeader(flsBody);
				flsBody.show();
			}
			//set blink
			clearInterval(intInterval);
			$(".blinkItem").css("visibility", "visible");
	    	intInterval = setInterval(function(){
				 if ($(".blinkItem").css("visibility")=="visible"){
					 $(".blinkItem").css("visibility", "hidden");
				 }else{
					 $(".blinkItem").css("visibility", "visible");
				 }
			 }, 800);
			//set blink
			clearInterval(intInterval);
			$(".blinkItem").css("visibility", "visible");
	    	intInterval = setInterval(function(){
				 if ($(".blinkItem").css("visibility")=="visible"){
					 $(".blinkItem").css("visibility", "hidden");
				 }else{
					 $(".blinkItem").css("visibility", "visible");
				 }
			 }, 800);
	    	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
		}
		
		
		
		function setLowasetFare(id,way){
			 if (id == null){
				 var calIDVal = (way == "dep")? "fCalOut_FL":"fCalIn_FL";
			 	 var calHeadIDVal = (way == "dep")? "fCalOut":"fCalIn";
			 }else{
				 calIDVal = id;
			 }
			 var lowerst = getEgdeFare(calIDVal, "Min");
			 $.each($("#"+calIDVal).find(".totalValueSelected > label.currValueTotal"),function(i,j){
				 if (parseFloat(j.innerHTML,10) == lowerst){
					 if (!$(j).parent().parent().find(".lowerstCon").hasClass("web-only")){
						 $(j).parent().parent().find(".lowerstCon").html("<span class='lowerst'><img src='../images/v3/Bullet_no_cache.gif' alt=''/> "+UI_AvailabilitySearch.labels.lowerstFare+"</span>");
					 }
				 }else{
					 if (!$(j).parent().parent().find(".lowerstCon").hasClass("web-only")){
						 $(j).parent().parent().find(".lowerstCon").html("");
					 }
					 
				 }

			 });
			 var headerObj = $("#"+calHeadIDVal).find(".selected").find(".minFareValue>label");
			 if(headerObj.length==0 && lowerst > 0){
				 var sFares = $("<span class='from'>"+UI_AvailabilitySearch.labels.lblBoarding+"</span> <span>"+ settings.currencyCode +"</span> <label>"+ lowerst +"</label><span> Available </span>").attr("class","minFareValue");
				 var headerObj2 = $("#"+calHeadIDVal).find(".selected").find(".minFareValue>span");
				 headerObj2.replaceWith(sFares);
			 }else{
				 headerObj.text(lowerst);
			 }
		 }
		
		function dateVal( str1 ) {
			    var diff = Date.parse( str1 );
			    return isNaN( diff ) ? NaN : {
			    	diff : diff,
			    	ms : Math.floor( diff            % 1000 ),
			    	s  : Math.floor( diff /     1000 %   60 ),
			    	m  : Math.floor( diff /    60000 %   60 ),
			    	h  : Math.floor( diff /  3600000 %   24 ),
			    	d  : Math.floor( diff / 86400000        )
			    };
		};
		 
		 
		 $(document).on("mouseover",".getDay", function(){
				$(this).addClass("mout");
		 });
		 $(document).on("mouseout",".getDay",function(){
				$(this).removeClass("mout");
		 });
		 
		 $(document).on("click",".flight-header a",function(){
				$(this).parent().parent().children().removeClass("_selected");
				$(this).parent().addClass("_selected");
				$(this).parent().parent().next().children().addClass("notshow");
				$("#b-"+this.id).removeClass("notshow");
		});
		
	 
		 
		$.fn.calendarview.selectItem = function(event){
			//setting selected object
			UI_AvailabilitySearch.lastSelected = $(event.currentTarget).parent().parent().parent().parent().parent().find("a.selected");
			$(event.currentTarget).parent().parent().parent().parent().parent().find("a").removeClass("selected");
			$(event.currentTarget).addClass("selected")
			$(event.currentTarget).find("input[type='radio']").attr("checked",true);
			
			var t = $(event.currentTarget).parent().find(".dipDate").html();
			var headArr = $(event.currentTarget).parent().parent().parent().parent().parent().parent().prev(".cal-header").find("li");
			$.each(headArr, function(i, j){
				$(j).find(".cal-one-day").removeClass("headSelected")
				if ($(j).find("span").html() == t){
					$(j).find(".cal-one-day").addClass("headSelected")
				}
			});
		};
		
		$.fn.calendarview.changeSelectedFare = function(values){
			var defaults = {inbound:null, outbound:null}
			var values = $.extend(defaults,values);
			var maxWholeNumWithDecimal = 0;			
				if(values.outbound!=null ){
					var bodyObj_Out = $("#fCalOut_FL").find("input[type='radio']:checked").parent().find(".totalValueSelected").find('.currValueTotal');
					bodyObj_Out.text(values.outbound);
					setLowasetFare(null, 'dep');
				}
				if(values.inbound != null){
					var bodyObj_In = $("#fCalIn_FL").find("input[type='radio']:checked").parent().find(".totalValueSelected").find('.currValueTotal');
					bodyObj_In.text(values.inbound);
					setLowasetFare(null, 'arr');
				}
		}
		 
//end of plugin
	    });
	 };

	 function ulSelectAll(obj){
		 obj.parent().parent().parent().parent().parent().find("a").removeClass("selected");
	 };
	 
	 
	 function setCalLabels(){
		 if (UI_AvailabilitySearch.labels != "" || UI_AvailabilitySearch.labels != null){
			 $(".lowerst").text(UI_AvailabilitySearch.labels.lowerstFare);
			 $(".lowerst").attr("title",UI_AvailabilitySearch.labels.lowerstFare);
			 $(".FlightFull").text(UI_AvailabilitySearch.labels.fullFlights);
			 $(".departureTime span").text(UI_AvailabilitySearch.labels.shortDeparture);
			 $(".arivalTime span").text(UI_AvailabilitySearch.labels.shortArrival);
			 $(".noFlight").text(UI_AvailabilitySearch.labels.noFlights);
			 $(".lblStopover").text(UI_AvailabilitySearch.labels.shortover);
			 $(".lblStopovers").text(UI_AvailabilitySearch.labels.shortovers);
			 $(".CompareFare").find("label").text(UI_AvailabilitySearch.labels.lblCompareFare);
			 $(".getDay").css({ "height":"auto","background":"transparent"});
			
		 }
	 }
	 
	 function getModifyDateStaus(){
		 var isModifyDate = true;
		 if(UI_AvailabilitySearch.isModifySegment == true){
			 if(UI_AvailabilitySearch.modifyByDate == false){
				 isModifyDate = false;
			 }
		 }
		 return isModifyDate;
	 }
	 /**
	 * TODO delete 
	 * add Flexi Itrms to Cal this was moved from availabilitySearch.js
	  */
	 
	 $.fn.calendarview.includeFlexiInCal = function(){
		//$("#fCalOut").parent().append($("#outFlightFlexi").parent().parent());
		//$("#fCalIn").parent().append($("#inFlightFlexi").parent().parent());
	 }
	 
	 /**
	  * Setting in Place Flexi box in Calendar moved from availabilitySearch.js
	  */
	 $.fn.calendarview.settingInPlaceFlexiInCal = function(obj){
	 	//var itemsSelected = obj.parent().find(".selected").find(".stopover");
	 	//var leftval = ($("html").attr("dir") == "rtl")? 5 : 3;
	 	obj.parent().parent().css({
	 		width:$(".flightCalendar").width() - 7,
	 		margin:"0 auto"
	 	})

	 	obj.show();
	 };
	 
	 	 
	 /**
	  * to create tabs html when selecting the tabs
	  */
	 $.fn.calendarview.createTabData = function(ind){
	 	var str ="";
	 		str += '<div class="flight-oriented">';
	 		str +='<table border="0" width="100%" cellpadding="0" cellspacing="0" style="width:100%;margin:0 auto">';
	 		str +='<tr><td valign="top" class=""></td><td class=""></td></tr>'+
	 			'<tr>'+
	 				'<td width="100%" valign="top" colspan="2" class=""><div class="outerCal">'+
	 					'<div class="flightCalendar" style="width:'+calDisplayConfig.layout.calWidth+'px;" id="fCalOut"></div></div>';
	 					str +='</td></tr>';
	 					if (calDisplayConfig.layout.displayFlightDetailsInCal){}
	 			if ($("#resReturnFlag").val() == "true"){
	 				str +='<tr><td colspan="2" class="rowGap" style="height:25px"></td></tr><tr><td valign="top" class=""></td><td class=""></td></tr>';
	 				str +='<td width="100%" valign="top" colspan="2" class=""><div class="outerCal">'+
	 					'<div class="flightCalendar" style="width:'+calDisplayConfig.layout.calWidth+'px;" id="fCalIn"></div></div>';
	 				str +='</td>';
	 					if (calDisplayConfig.layout.displayFlightDetailsInCal){}
	 			}
	 		str +='</tr><tr><td colspan="2" class="rowGap"></td></tr>';
	 	str +='</table></div>';

	 	return str;
		}
	 
	 	function getObjlength(obj) {
		    return obj.length;
		}
})(jQuery);