(function($) {
	
	var designVersion = 1;
	$.fn.summaryPanel = function(o) {
		var config = {
				id:"",
				data:null,
				labels:null
		};

	if (o)
    	settings = $.extend(config, o);
	//Start of plugin
    return this.each(function(){
    	var	el = $(this);
    	var objid = this.className;
    	
    	var totalChargesLabelHtmlRaw = '<td width="70%" class="alignLeft"><label id="lblTotalFlightServices" class="fntBold gridHDFont" title="'+ o.labels.lblTotalFlightServices +'">'+ o.labels.lblTotalFlightServices +'</label></td>';
    	if(typeof UI_Container !='undefined' && UI_Container.isModifyAncillary()){
    		totalChargesLabelHtmlRaw = '<td width="70%" class="alignLeft"><label id="lblAdditionalAnciCharges" class="fntBold gridHDFont" title="'+ o.labels.lblAdditionalAnciCharges +'">'+ o.labels.lblAdditionalAnciCharges +'</label></td>';
    	}    	
    	
    	var addHTML = '<div class="Page-heading"><table width="100%" border="0" cellspacing="0" cellpadding="0">'+
    						'<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="headerRow">'+
								'<tr>'+
									'<td class="headerBG hLeft"><img src="../images/spacer_no_cache.gif"/></td>'+
									'<td class="headerBG hCenter alignLeft"><label id="lbl'+o.id+'" class="fntBold gridHDFont">Payment Summary</label></td>'+
									'<td class="headerBG hRight"><img src="../images/spacer_no_cache.gif"/></td>'+
								'</tr></table></td></tr>'+
								'<tr>'+
									'<td class="mLeft"></td>'+
									'<td class="mCenter"><div class="pane-body"></div></td>'+
									'<td class="mRight"></td>'+
								'</tr>'+
								'<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerRow">'+
								'<tr>'+
								'<td class="headerBG bLeft"><img src="../images/spacer_no_cache.gif"/></td>'+
								'<td class="headerBG bCenter">'+
								'<div id="totalPriceDiv" class="fntBold" style="display:none">'+															
									'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
										totalChargesLabelHtmlRaw +
										'<td width="20%" class="alignRight" valign="top"><label id="valTotal" class="baseTotalPanel fntBold gridHDFont">0.00</label> <br> <div class="fntBold trSelectedCurrClass" style="display:none"> <label id="valSelectedTotal" class="fntBold gridHDFont">0.00</label> </div></td>'+
										'<td width="10%" class="alignRight" valign="top">&nbsp;<label id="valTotal_tg" name=\'valBaseCurrCode\' class=\'baseTotalPanel gridHDFont fntBold\'>  </label><br>'+
										'<div class="fntBold trSelectedCurrClass" style="display:none"> &nbsp;<label name=\'valSelectedCurrCode\' class=\'gridHDFont fntBold\'>  </label></div></td>'+
									'</tr></table>'+							
								'</div>'+
								'<div id="trResCredit" class="fntBold" style="display:none">'+															
									'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
									'<td width="70%" class="alignLeft"><label id="lblTotalReservationCredit" class="fntBold gridHDFont">'+o.labels.lblTotalReservationCredit+'</label></td>'+	
									'<td width="20%" class="alignRight"><label id="valTotalReservationCredit" class="fntBold gridHDFont">0.00</label></td>'+
									'<td width="10%" class="alignRight">&nbsp;<label name=\'valBaseCurrCode\' class=\'gridHDFont fntBold\'>  </label></td>'+
									'</tr></table>'+							
								'</div>'+
								'<div id="trResBalanceToPay" class="fntBold" style="display:none">'+															
									'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
									'<td width="70%" class="alignLeft"><label id="lblBalanceToPay" class="fntBold gridHDFont">'+o.labels.lblBalanceToPay+'</label></td>'+	
									'<td width="20%" class="alignRight"><label id="valModifyBalanceToPay" class="fntBold gridHDFont">0.00</label></td>'+
									'<td width="10%" class="alignRight">&nbsp;<label name=\'valBaseCurrCode\' class=\'gridHDFont fntBold\'>  </label></td>'+
									'</tr></table>'+							
								'</div>'+
								'<div id="trCreditBalance" class="fntBold" style="display:none">'+															
									'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
									'<td width="70%" class="alignLeft"><label id="lblCreditBalance" class="fntBold gridHDFont">'+o.labels.lblCreditBalance+'</label></td>'+	
									'<td width="20%" class="alignRight"><label id="valResCreditBalance" class="fntBold gridHDFont">0.00</label></td>'+
									'<td width="10%" class="alignRight">&nbsp;<label name=\'valBaseCurrCode\' class=\'gridHDFont fntBold\'>  </label></td>'+
									'</tr></table>'+							
								'</div>'+
								'</td>'+
								'<td class="headerBG bRight"><img src="../images/spacer_no_cache.gif"/></td>'+
								'</tr></table></td></tr>'+
								'<tr id="totalPromoDiscountDivCredit" style="display:none">'+
									'<td class="mLeft" style="border-bottom:1px solid #ddd"></td>'+
									'<td class="mCenter" style="border-bottom:1px solid #ddd">'+
										'<table width="98%" border="0" cellspacing="0" cellpadding="0" style="width:96%;margin:0 auto;padding:5px;">'+
										'<tr>'+
											'<td width="50%" class="alignLeft" >'+
												'<label  class="fntBold lblSumPromoDiscount" id="lblSumPromoDiscount" title="'+o.labels.lblPromoDiscountCredit+'">'+o.labels.lblPromoDiscountCredit+'</label>'+
											'</td>'+	
											'<td width="40%" class="alignRight" valign="top">'+
												'<label id="valPromoDiscount1" class="fntBold valPromoDiscount">0.00</label>'+
											'</td>'+
											'<td width="10%" class="alignRight" valign="top">&nbsp;'+
												'<label name=\'valSelectedCurrCode\' class=\'fntBold\'></label>'+
											'</td>'+
										'</tr>'+
										'</table>'+
									'</td>'+
									'<td class="mRight" style="border-bottom:1px solid #ddd"></td>'+
								'</tr>'+
							'</table>'+
						'</div>';
    	if (objid != "paymentSummary") {
    		el.html(addHTML);
    	}
    	
    	displayItems = function(p){     		
			//lak currency selector class - p.inSelectedCurr.currency == null(this is for modify flow currency check)   		
			if (p.inSelectedCurr.currency == null || (p.inSelectedCurr.currency == p.inBaseCurr.currency)) {				
				$("#valTotal,#valTotal_tg").removeClass("baseTotalPanel");
				//$("#valTotal_tg").removeClass("baseTotalPanel");
				$("#valTotal,#valTotal_tg").addClass("baseTot_prc");
				//$("#valTotal_tg").addClass("baseTot_prc");
			} else {
				$('#valTotal,#valTotal_tg').removeClass("baseTot_prc");
				//$('#valTotal_tg').removeClass("baseTot_prc");
				$('#valTotal,#valTotal_tg').addClass("baseTotalPanel");	
				//$('#valTotal_tg').addClass("baseTotalPanel");
			}
    		if(parseFloat(p.totalFare)>0)
    			$('#trAirFare').show();
    		else
    			$('#trAirFare').hide();
    		
    		if(parseFloat(p.totalTaxSurcharge)>0)
    			$('#trCharges').show();
    		else
    			$('#trCharges').hide();
    		
    		if(p.hasFee)
    			$('#trTxnFee').show();
    		else
    			$('#trTxnFee').hide();
    		
    		if(p.hasReservationCredit)
    			$('#trUtilCredit').show();
    		else
    			$('#trUtilCredit').hide();
    		
    		if(p.hasReservationBalance)
    			$('#trResBalance').show();
    		else
    			$('#trResBalance').hide();
    		
    		if(p.hasSeatCharge)
    			$('#trSeatCharges').show();
    		else
    			$('#trSeatCharges').hide();
    		
    		if(p.hasMealCharge)
    			$('#trMealCharges').show();
    		else
    			$('#trMealCharges').hide();
    		
    		if(p.hasBaggageCharge)
    			$('#trBaggageCharges').show();
    		else
    			$('#trBaggageCharges').hide();    		
    		
			if(p.hasAirportServiceCharge)
				$("#trHalaCharges").show();
			else
				$("#trHalaCharges").hide();
			
    		if(p.hasInsurance)
    			$('#trInsuranceCharges').show();
    		else
    			$('#trInsuranceCharges').hide();
    		
    		if(p.hasAirportTransferCharge)
				$("#trAptCharges").show();
			else
				$("#trAptCharges").hide();
    		
    		if (p.inSelectedCurr!= null && p.inSelectedCurr.currency != p.inBaseCurr.currency
    				&& p.inSelectedCurr.currency != null  && 
    				parseFloat(p.inSelectedCurr.totalPrice)>= 0) {
    			$('label[name="valSelectedCurrCode"]').text(
    					p.inSelectedCurr.currency);
    			$("#valSelectedTotal").text(p.inSelectedCurr.totalPrice);
    			$('.trSelectedCurrClass').show();    			
    		} else {
    			$('.trSelectedCurrClass').hide();
    		}
    		
    		
    		if(p.hasModifySegment)
    			$('#trModificationCharge').show();
    		else
    			$('#trModificationCharge').hide();
    		
    		if(p.hasTotalReservationCredit)
    			$('#trResCredit').show();
    		else
    			$('#trResCredit').hide();
    		
    		if(p.hasBalanceToPay)
    			$('#trResBalanceToPay').show();
    		else
    			$('#trResBalanceToPay').hide();
    		
    		if(p.hasCreditBalance)
    			$('#trCreditBalance').show();
    		else
    			$('#trCreditBalance').hide();
    		
    		if(p.hasIbeDiscount){
    			$("#totalDiscountDiv").show();
    		}
    		else{
    			$("#totalDiscountDiv").hide();
    		}
    		
    		if(p.hasPromoDiscount && parseFloat(p.inBaseCurr.totalIbePromoDiscount) > 0){
    			var isOutBoundPromo = UI_Container.discountInfo.applicability =="OB" ? true:false;
    			if(p.deductFromTotal){
    				if(isOutBoundPromo){
    					$("#totalPromoDiscount_OB_Div").show();
    				}else{
    					$("#totalPromoDiscountDiv").show();
    				}
    				$("#totalPromoDiscountDivCredit").hide();
    			} else {
    				$("#totalPromoDiscountDivCredit").show();
    				$("#totalPromoDiscountDiv").hide();
    				$("#totalPromoDiscount_OB_Div").hide();
    			}
    			
    		} else {
    			$("#totalPromoDiscount_OB_Div").hide();
    			$("#totalPromoDiscountDiv").hide();
    			$("#totalPromoDiscountDivCredit").hide();
    		}
    		
    		$("#totalPriceDiv").show();
    		//$("#totalOutDiv").show();
			//$("#totalInDiv").show();
			
			if (p.hasFee || p.hasSeatCharge || p.hasMealCharge || p.hasInsurance || p.hasBaggageCharge) {
    			if (!UI_Container.paymentRetry) {
    				$("#addServiceDiv").slideDown("slow");
    			}
    		} else {
    			$("#addServiceDiv").slideUp("slow");
    		}
			if(UI_commonSystem.loyaltyManagmentEnabled){
				if($("#lblRewardsPanelDesc").html()=="" || $("#lblRewardsPanelDesc").html()==null){
					var lmsPane = '<div class="Page-heading"><table width="100%" border="0" cellspacing="0" cellpadding="0">'+
		                            '<tr>'+
		                            '<td colspan="3"><div style="background: #ffffdd;padding: 10px;border: 1px solid #f8f8a9" class="ui-corner-all">' +
		                                '<label id="lblRewardsPanelTitle" class="hdFontColor fntBold"></label><br/><br/>' +
		                                '<label id="lblRewardsRatioDesc"></label><br/>' +
		                            '</div></td></tr>'+
		                            '<tr>'+
									'</table>'+
								'</fieldset>'+
							'</form>'+
						'</div>';
			    	el.append(lmsPane);
				}
			}
    			
    	};
    	
    	//TODO move the global variables out and pass them as params
             function showSelectedFlightsSummary(selectedFlights){
    		
    			var labels = UI_Container.labels;
    			if(selectedFlights.isReturnFlight == true || selectedFlights.isReturnFlight == 'true') {
    				$("#lblReturn").text(labels['lblRoundTrip']);
    			}else {
    				$("#lblReturn").text(labels['lblOneWayTrip']);
    			}
    			var strMsgAdult = labels['lblAdult'];
    			var strMsgChild = labels['lblChild'];
    			var strMsgInfant = labels['lblInfant'];
    			
    			if(UI_Container.paxTypeCounts.adults > 0) {
    				$("#adultCount").text(UI_Container.paxTypeCounts.adults+ " " + strMsgAdult)
    			}
    			if(UI_Container.paxTypeCounts.children > 0) {
    				$("#childCount").text(", "+UI_Container.paxTypeCounts.children +" " + strMsgChild)
    			}
    			if(UI_Container.paxTypeCounts.infants > 0) {
    				$("#infantCount").text(", "+UI_Container.paxTypeCounts.infants+" " + strMsgInfant)
    			}
    			
    			var outFlights = new Array();
    			var inflights = new Array();
    			if (selectedFlights != null && selectedFlights.flightSegments != null) {
    				for (var i = 0; i < selectedFlights.flightSegments.length; i++) {
    					if (selectedFlights.flightSegments[i].returnFlag == false) {    						
    						outFlights = outFlights.concat(selectedFlights.flightSegments[i]);
    					} else if (selectedFlights.flightSegments[i].returnFlag == true) {    						
    						inflights = inflights.concat(selectedFlights.flightSegments[i]);
    					} else {
    						alert("Unknown flight : incorrect return flag");
    					}
    				}
    			}
    			updateFlightDetails(outFlights, inflights, labels);
    			
    		};
    		
    		function updateSummayPannelFareQuote(pName,processDate) {     			
    			if (pName == 'paymentSummary'){ 
					var curr = processDate.currency;   
					addCurrencyDescription(curr);
					addSelectedCurrencyDescription(processDate.selectedCurrency);
					$("#valFlexiCharges").text(processDate.flexiCharge);
					$("#valSeatCharges").text(processDate.seatMapCharge);
					$("#valTotal").text(processDate.totalPrice);
					var segmentFare = processDate.segmentFare;					
					if (segmentFare != null && segmentFare.length > 0) {
					
						if (isMcSelected=='true'){
							var tTotIn =0;
							var tTotOut =0;
							for (var k=0;k<processDate.segmentFare.length;k++){
								if(processDate.segmentFare[k].hasInbound)
								   tTotIn+= parseFloat(processDate.segmentFare[k].totalPriceInSelectedCurr,10);
								else
								   tTotOut+= parseFloat(processDate.segmentFare[k].totalPriceInSelectedCurr,10);
							}
							$("#TotalInFlight").text(Number(tTotIn).toFixed(2));
							$("#totalInDiv").show();
							$("#TotalOutFlight").text(Number(tTotOut).toFixed(2));
							$("#totalOutDiv").show();
						}else{
							$("#TotalOutFlight").text(Number(processDate.segmentFare[0].totalPriceInSelectedCurr).toFixed(2));
							$("#totalOutDiv").show();
							if(processDate.segmentFare.length > 1){
								$("#TotalInFlight").text(Number(processDate.segmentFare[1].totalPriceInSelectedCurr).toFixed(2));
								$("#totalInDiv").show();
							}							
						}

	    			}  else {
	    				$("#totalOutDiv").hide();
	    				$("#totalInDiv").hide();
	    			}  			

		    		$("#valTxnFee").text(processDate.totalFee);
		    		var discountAmount = processDate.inBaseCurr.currency != processDate.inSelectedCurr.currency ? 
		    				processDate.inSelectedCurr.totalIbeFareDiscount : processDate.inBaseCurr.totalIbeFareDiscount;
		    		if (parseFloat(discountAmount) > 0) {
		    			$("#valDiscount").text("-"+discountAmount);	    		
			    		$("#totalDiscountDiv").show(); 
		    		}else{
		    			$("#valDiscount").text("");	    		
			    		$("#totalDiscountDiv").hide(); 
		    		}
		    		
		    		var promoDiscount = "";
		    		if (processDate.currency != processDate.selectedCurrency) {
		    			promoDiscount = processDate.inSelectedCurr.totalIbePromoDiscount;
		    		} else {
		    			promoDiscount = processDate.inBaseCurr.totalIbePromoDiscount;
		    		}
		    		
		    		if(processDate.hasPromoDiscount && parseFloat(promoDiscount) > 0){	   
		    			var isOutBoundPromo = UI_Container.discountInfo.applicability =="OB" ? true:false;
		    			if(processDate.deductFromTotal){
		    				$(".lblSumPromoDiscount").text(o.labels.lblPromoDiscount); 
		    				$(".lblSumPromoDiscount").attr('title', o.labels.lblPromoDiscount); 
		    				$(".valPromoDiscount").text('-' + promoDiscount);
		    				if(isOutBoundPromo){
		    					$("#totalPromoDiscount_OB_Div").slideDown("slow"); 
		    				}else{
			    				$("#totalPromoDiscountDiv").slideDown("slow"); 
		    				}
		    				$("#totalPromoDiscountDivCredit").slideUp("slow"); 
		    			} else {
		    				$(".lblSumPromoDiscount").text(o.labels.lblPromoDiscountCredit); 
		    				$(".lblSumPromoDiscount").attr('title', o.labels.lblPromoDiscountCredit); 
		    				$(".valPromoDiscount").text(promoDiscount);
		    				if(isOutBoundPromo){
		    					$("#totalPromoDiscount_OB_Div").slideUp("slow"); 
		    				}else{
		    					$("#totalPromoDiscountDiv").slideUp("slow"); 
		    				}
		    				$("#totalPromoDiscountDivCredit").show(); 
		    			}	    		
		    		} else {
		    			$(".valPromoDiscount").text("");	    		
		    			$("#totalPromoDiscountDiv").slideUp("slow"); 
		    			$("#totalPromoDiscount_OB_Div").slideUp("slow"); 
		    			$("#totalPromoDiscountDivCredit").slideUp("slow"); 
		    		}
    			}
    		}
    		
    		 function createHTML(pName,processDate){
    			 	var OutFareType = "";var InFareType = "";
    			 	if ($("#resFlexiSelection").val() != ""){
	    			 	var tFlSelection = $.parseJSON( $("#resFlexiSelection").val() );
	    			 	if (tFlSelection.outSelectedFare == 'promoFare'){OutFareType = 'Promo Fare';
	    			 	}else if(tFlSelection.outSelectedFare == 'flexiFare'){OutFareType = 'Flexi Fare';
	    			 	}else{OutFareType = 'Primium Fare';}
	    			 	if (tFlSelection.inSelectedFare == 'promoFare'){InFareType = 'Promo Fare';
	    			 	}else if(tFlSelection.inSelectedFare == 'flexiFare'){InFareType = 'Flexi Fare';
	    			 	}else{InFareType = 'Primium Fare';}
    			 	}else{
    			 		var tFlSelection = {};
    			 		tFlSelection.outSelectedFare = '';
    			 		tFlSelection.inSelectedFare ='';
    			 	}
    			 	if(tFlSelection.inSelectedFare != ""){tFlSelection.inSelectedFare = "url(" + tFlSelection.inSelectedFare + "_en.png)"}; //TODO set the language, by SYS_IBECommonParam.locale
    			 	if(tFlSelection.outSelectedFare != ""){tFlSelection.outSelectedFare = "url(" + tFlSelection.outSelectedFare + "_en.png)"}; //TODO set the language, by SYS_IBECommonParam.locale
    				var str= null;
    				if (pName == 'bookingSummary'){    					 	
    					str = '<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
    							'<tr><td class="pnlTop"></td></tr>'+
    							'<tr><td class="pnlBottom add-padding alignLeft" style="height:52px">'+
    									'<table width=valSelectedTotal"100%" border="0" cellspacing="1" cellpadding="1">'+
    										'<tr><td><label id="adultCount" /><label id="childCount" /><label id="infantCount" /></td></tr>'+
    							'</table></td></tr>'+    								
    							'<tr><td> <div id="outFlightsDiv" style="display:none">'+
    							 		'<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
    							 			'<tr><td class="pnlBottom alignLeft colourGreavy" style="height:32px;background-color: #666666; background-image:'+tFlSelection.outSelectedFare+';" valign="top">'+
    							 					'<table width="100%" border="0" cellspacing="1" cellpadding="1">'+
    							 						'<tr><td>&nbsp;<label id="outFlightDirection" class="fntBold gridHDFont">  </label></td></tr>'+
    							 			'</table></td></tr>'+
    							 			'<tr><td class="pnlTop"></td></tr>'+
    							 			'<tr><td class="rowGap"></td></tr>'+
		    							 	'<tr id="outFlightTemplate">'+
		    							 		'<td class="pnlBottom add-padding alignLeft">'+
		    							 			'<table width="100%" border="0" cellpadding="1" cellspacing="1">'+
		    							 				'<tr><td><label id="flightSeqNo" class="fntBold"/><label id="segmentCode" class="fntBold" /></td></tr>'+
		    							 				'<tr>'+
			    							 				'<td><table width="99%" cellpadding="0" cellspacing="0"><tr>'+
			    							 				'<td width="30%"><label id="lblDepature" >'+o.labels.lblDepature+'</label></td>'+
			    							 				'<td width="59%"><label id="departureDate" class="date-disp"/><label class="date-hid" id="departureDateValue" style="display:none"/>,&nbsp;<label id="departureTime" /></td>'+
			    							 				'</tr></table></td>'+
		    							 				'</tr>'+
		    							 				'<tr>'+
			    							 				'<td><table width="99%" cellpadding="0" cellspacing="0"><tr>'+
			    							 				'<td width="30%"><label id="lblArrival" >'+o.labels.lblArrival+'</label></td>'+
			    							 				'<td width="59%"><label id="arrivalDate" class="date-disp"/><label class="date-hid" id="arrivalDateValue" style="display:none"/>,&nbsp;<label id="arrivalTime" /></td>'+
			    							 				'</tr></table></td>'+
		    							 				'</tr>'+
					    							 	'<tr>'+
					    							 		'<td class="summarySperator"></td>'+
														 '</tr>'+
														 
											'</table></td></tr>'+	
											
											
											'<tr><td class="rowGap"></td></tr>'+
				    				'</table><div id="totalOutDiv" class="baseTotalPanel fntBold add-padding" style="display:none">'+				    			
					    				'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td colspan="3"></td></tr><tr>'+
				    					'<td width="70%" class="alignLeft"><label id="lblTotalOutFlight" class="fntBold">'+o.labels.lblTotalOutFlight+'</label></td>'+	
				    					'<td width="20%" class="alignRight"><label id="TotalOutFlight" class="fntBold"></label></td>'+
				    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    					'</tr></table>'+
				    				'</div></div></td></tr>'+
				    				
				    				'<tr>'+
				    					'<td>'+
					    					'<div id="totalPromoDiscount_OB_Div" class="fntBold" style="display:none">'+															
											'<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
				    							 	'<tr>'+
				    							 		'<td class="pnlBottom alignLeft">'+
				    							 		'<div class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<tr><td class="summarySperator" colspan="3"></td></tr>'+
															'<tr>'+
																'<td width="60%" class="alignLeft" >'+
																	'<label  class="fntBold totService lblSumPromoDiscount" id="lblSumPromoDiscount" title="'+o.labels.lblPromoDiscount+'">'+o.labels.lblPromoDiscount+'</label>'+
																'</td>'+	
																'<td width="30%" class="alignRight" valign="top">'+
																	'<label id="valPromoDiscount2" class="fntBold valPromoDiscount">0.00</label>'+
																'</td>'+
																'<td width="10%" class="alignRight" valign="top">&nbsp;'+
																	'<label name=\'valSelectedCurrCode\' class=\' fntBold\'></label>'+
																'</td>'+
															'</tr>'+
														'</table></div>'+
													'</td></tr>'+
													'<tr><td class="rowGap"></td></tr>'+
												'</table>'+
											'</div>'+
				    					'</td>'+
				    				'</tr>' +
				    				
					 				'<tr><td class="rowGap"></td></tr>'+
				    				'<tr><td><div id="inFlightsDiv" style="display:none">'+
							 		'<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
							 			'<tr><td class="pnlBottom alignLeft colourGreavy" style="height:32px;background-color: #666666; background-image:'+tFlSelection.inSelectedFare+';" valign="top">'+
							 					'<table width="100%" border="0" cellspacing="1" cellpadding="1">'+
							 						'<tr><td>&nbsp;<label id="inFlightDirection" class="fntBold gridHDFont">  </label></td></tr>'+
							 			'</table></td></tr>'+
							 			'<tr><td class="pnlTop"></td></tr>'+
							 			'<tr><td class="rowGap"></td></tr>'+
	    							 	'<tr id="inFlightTemplate">'+
	    							 		'<td class="pnlBottom add-padding alignLeft">'+
	    							 			'<table width="100%" border="0" cellpadding="1" cellspacing="1">'+
	    							 				'<tr><td><label id="flightSeqNo" class="fntBold"/><label id="segmentCode" class="fntBold" /></td></tr>'+
	    							 				'<tr>'+
		    							 				'<td><table width="99%" cellpadding="0" cellspacing="0"><tr>'+
		    							 				'<td width="30%"><label id="lblDepature" >'+o.labels.lblDepature+'</label></td>'+
		    							 				'<td width="69%"><label id="departureDate" class="date-disp"/><label class="date-hid" id="departureDateValue" style="display:none"/>,&nbsp; <label id="departureTime" /></td>'+
		    							 				'</tr></table></td>'+
	    							 				'</tr>'+
	    							 				'<tr>'+
		    							 				'<td><table width="99%" cellpadding="0" cellspacing="0"><tr>'+
		    							 				'<td width="30%"><label id="lblArrival" >'+o.labels.lblArrival+'</label></td>'+
		    							 				'<td width="69%"><label id="arrivalDate" class="date-disp"/><label class="date-hid" id="arrivalDateValue" style="display:none"/>,&nbsp; <label id="arrivalTime" /></td>'+
		    							 				'</tr></table></td>'+
	    							 				'</tr>'+
				    							 	'<tr>'+
				    							 		'<td class="summarySperator"></td>'+
													 '</tr>'+													 
										'</table></td></tr>'+
										'<tr><td class="rowGap"></td></tr>'+
										'</table><div id="totalInDiv" class="baseTotalPanel fntBold add-padding" style="display:none">'+											
												'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td colspan="3"></td></tr><tr>'+
		    			    					'<td width="70%" class="alignLeft"><label id="lblTotalInFlight" class="fntBold">'+o.labels.lblTotalInFlight+'</label></td>'+	
		    			    					'<td width="20%" class="alignRight"><label id="TotalInFlight" class="fntBold"></label></td>'+
		    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
		    			    					'</tr></table>'+
											'</div></div></td></tr>'+
						 				'<tr><td class="rowGap"></td></tr>'+
						 				'<tr><td>'+
						 				'<div id="totalDiscountDiv" class="fntBold" style="display:none">'+															
											'<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
				    							 	'<tr>'+
				    							 		'<td class="pnlBottom alignLeft">'+
				    							 		'<div class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<tr><td class="summarySperator" colspan="3"></td></tr>'+
															'<tr>'+
																'<td width="60%" class="alignLeft" >'+
																	'<label  class="fntBold totService" title="'+o.labels.lblDiscount+'">'+o.labels.lblDiscount+'</label>'+
																'</td>'+	
																'<td width="30%" class="alignRight" valign="top">'+
																	'<label id="valDiscount" class="fntBold ">0.00</label>'+
																'</td>'+
																'<td width="10%" class="alignRight" valign="top">&nbsp;'+
																	'<label name=\'valSelectedCurrCode\' class=\' fntBold\'></label>'+
																'</td>'+
															'</tr>'+
														'</table></div>'+
													'</td></tr>'+
													'<tr><td class="rowGap"></td></tr>'+
												'</table>'+
										'</div>'+
						 				'<div id="addServiceDiv" style="display:none">'+
								 		'<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
								 			'<tr><td class="pnlBottom alignLeft" style="height:32px;background-color: #666666">'+
								 					'<table width="100%" border="0" cellspacing="1" cellpadding="1">'+
								 						'<tr><td class="alignLeft">&nbsp;<label id="lblAditionalServices" class="fntBold gridHDFont"> '+o.labels.lblAditionalServices+' </label></td></tr>'+
								 			'</table></td></tr>'+
								 			'<tr><td class="pnlTop"></td></tr>'+
								 			'<tr><td class="rowGap"></td></tr>'+
		    							 	'<tr>'+
		    							 		'<td class="pnlBottom alignLeft">'+
			    							 		'<div id="trFlexiCharges" style="display:none;" class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblFlexiCharges" class="fntBold">'+o.labels.lblFlexiCharges+'</label></td>'+
				    			    					'<td width="20%" class="alignRight"><label id="valFlexiCharges" class="fntBold"></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+
			    			    					'<div id="trSeatCharges" style="display:none;" class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblSeatSelection" class="fntBold">'+o.labels.lblSeatSelection+'</label></td>'+	
				    			    					'<td width="20%" class="alignRight"><label id="valSeatCharges" class="fntBold"></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+
			    			    					'<div id="trInsuranceCharges" style="display:none;" class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblTravelSecure" class="fntBold">'+o.labels.lblTravelSecure+'</label></td>'+
				    			    					'<td width="20%" class="alignRight"><label id="valInsuranceCharges" class="fntBold"></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+
			    			    					'<div id="trMealCharges" style="display:none;" class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblMealSelection" class="fntBold">'+o.labels.lblMealSelection+'</label></td>'+	
				    			    					'<td width="20%" class="alignRight"><label id="valMealCharges" class="fntBold"></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+
			    			    					'<div id="trBaggageCharges" style="display:none;" class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblBaggageSelection" class="fntBold">'+o.labels.lblBaggageSelection+'</label></td>'+	
				    			    					'<td width="20%" class="alignRight"><label id="valBaggageCharges" class="fntBold"></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+
                                                    '<div id="trSsrCharges" style="display:none;" class="add-padding">'+
                                                        '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
                                                        '<td width="70%" class="alignLeft"><label id="lblSsrSelection" class="fntBold">'+o.labels.lblSsrSelection+'</label></td>'+
                                                        '<td width="20%" class="alignRight"><label id="valSsrCharges" class="fntBold"></label></td>'+
                                                        '<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
                                                        '</tr></table>'+
                                                    '</div>'+
													'<div id="trHalaCharges" style="display:none;" class="add-padding">'+
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblHalaSelection" class="fntBold">'+o.labels.lblHalaSelection+'</label></td>'+	
				    			    					'<td width="20%" class="alignRight"><label id="valHalaCharges" class="fntBold"></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+
			    			    						'<div id="trAptCharges" style="display:none;" class="add-padding">'+		    							 		
			    			    						'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
			    			    						'<td width="70%" class="alignLeft"><label id="lblAptSelection" class="fntBold">'+ o.labels.lblAptSelection+'</label></td>'+	
			    			    						'<td width="20%" class="alignRight"><label id="valAptCharges" class="fntBold"></label></td>'+
			    			    						'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
			    			    						'</tr></table>'+
			    			    					'</div>'+
			    			    					'<div id="trPromoNextSeat" style="display:none;" class="add-padding">'+		 							 		
			    			    						'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
			    			    						'<td width="70%" class="alignLeft"><label id="lblNextSeatService" class="fntBold">'+ o.labels.lblNextSeatService +'</label></td>'+
			    			    						'<td width="20%" class="alignRight"><label id="valNextSeatPromoCharges" class="fntBold"></label></td>'+
			    			    						'<td width="10%" class="alignRight">&nbsp;<label name=\'valBaseCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
			    			    						'</tr></table>'+
			    			    					'</div>'+
				    			    				'<div id="trModificationCharge" style="display:none;" class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblModificationCharge" class="fntBold">'+o.labels.lblModificationCharge+'</label></td>'+	
				    			    					'<td width="20%" class="alignRight"><label id="valModificationCharge" class="fntBold"></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+
			    			    					'<div id="trTxnFee" style="display:none;" class="add-padding">'+
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<td width="70%" class="alignLeft"><label id="lblTNXFee" class="fntBold">'+o.labels.lblTNXFee+'</label></td>'+				    			    					
				    			    					'<td width="20%" class="alignRight"><label id=\'valTxnFee\' class=\'fntDefault fntBold\'></label></td>'+
				    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
				    			    					'</tr></table>'+
			    			    					'</div>'+  
			    			    					'<div id="trJnTaxAnci" style="display:none;" class="add-padding">'+
			    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
			    			    					'<td width="70%" class="alignLeft"><label id="lblJnTaxForAnci" class="fntBold">'+o.labels.lblJnTaxForAnci+'</label></td>'+				    			    					
			    			    					'<td width="20%" class="alignRight"><label id=\'valJnTaxForAnci\' class=\'fntDefault fntBold\'></label></td>'+
			    			    					'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
			    			    					'</tr></table>'+
		    			    					'</div>'+  
		    			    					
		    			    					'<div id="anciDowngradePenalty" style="display:none;" class="add-padding">'+
		    			    						'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
		    			    						'<td width="70%" class="alignLeft"><label id="lblAnciDowngradePenalty" class="fntBold">Ancillary Penalty</label></td>'+				    			    					
		    			    						'<td width="20%" class="alignRight"><label id=\'valAnciDowngradePenalty\' class=\'fntDefault fntBold\'></label></td>'+
		    			    						'<td width="10%" class="alignRight">&nbsp;<label name=\'valSelectedCurrCode\' class=\'fntDefault fntBold\'>  </label></td>'+
		    			    						'</tr></table>'+
		    			    					'</div>'+
	    			    					
			    			    				'</td></tr>'+
											'<tr><td class="rowGap"></td></tr>'+
											'</table> </div>'+
											'<div id="totalPromoDiscountDiv" class="fntBold" style="display:none">'+															
											'<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
				    							 	'<tr>'+
				    							 		'<td class="pnlBottom alignLeft">'+
				    							 		'<div class="add-padding">'+			    							 		
				    			    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
				    			    					'<tr><td class="summarySperator" colspan="3"></td></tr>'+
															'<tr>'+
																'<td width="60%" class="alignLeft" >'+
																	'<label  class="fntBold totService lblSumPromoDiscount" id="lblSumPromoDiscount" title="'+o.labels.lblPromoDiscount+'">'+o.labels.lblPromoDiscount+'</label>'+
																'</td>'+	
																'<td width="30%" class="alignRight" valign="top">'+
																	'<label id="valPromoDiscount" class="fntBold valPromoDiscount">0.00</label>'+
																'</td>'+
																'<td width="10%" class="alignRight" valign="top">&nbsp;'+
																	'<label name=\'valSelectedCurrCode\' class=\' fntBold\'></label>'+
																'</td>'+
															'</tr>'+
														'</table></div>'+
													'</td></tr>'+
													'<tr><td class="rowGap"></td></tr>'+
												'</table>'+
											'</div>'+
							'</td></tr>'+						 			
    				'</table>';
    					
    				}
    				
    				return str
    			};

        $.fn.summaryPanel.update = function(pName){
        	if (pName == 'bookingSummary') {
        		el.find(".pane-body").html(createHTML(pName,o.data));
        	} else {
        		updateSummayPannelFareQuote(pName, o.data);
        	}
    		if (o.data != null){
	    		if (pName == 'paymentSummary'){
	    			displayItems(o.data);
	    		}else if (pName == 'bookingSummary'){
	    			showSelectedFlightsSummary(o.data)
	    		}
    		}
    	};
    	$.fn.summaryPanel.update(objid);
   //end of plugin
    	$.fn.summaryPanel.firstLoad = function(){
    		var inFlights = UI_AvailabilitySearch.getSelectedFlightSegments("inbound");
    		var outFlights = UI_AvailabilitySearch.getSelectedFlightSegments("outbound");
    		var labels = UI_AvailabilitySearch.labels;
    		
    		updateFlightDetails(outFlights, inFlights, labels);
    		$.fn.summaryPanel.updateFlightPriceDetails();    		
    	}
    	
    	function updateFlightDetails(outFlights, inFlights, labels) {    		
    		$("#inFlightTemplate").nextAll().remove();
    		$("#inFlightTemplate").iterateTemplete({templeteName:"inFlightTemplate", data:inFlights, dtoName:"flightSegments"});
    		$("#outFlightTemplate").nextAll().remove();
    		$("#outFlightTemplate").iterateTemplete({templeteName:"outFlightTemplate", data:outFlights, dtoName:"flightSegments"});   		
    		
    		if (outFlights != null && outFlights.length >= 1) {
    			if (outFlights.length > 1) {
    				$("#outFlightDirection").html(labels.lblOutbound + " (" + labels.lblConnecting + ")");
    			} else {
    				$("#outFlightDirection").html(labels.lblOutbound);
    			}   			
    			$("#outFlightsDiv").slideDown("slow");
    		} else {
    			$("#outFlightsDiv").slideUp("slow");
    		}
    		
    		if (inFlights != null && inFlights.length >= 1) {
    			if (inFlights.length > 1) {
    				$("#inFlightDirection").html(labels.lblInbound + " (" + labels.lblConnecting + ")");
    			} else {
    				$("#inFlightDirection").html(labels.lblInbound);
    			}   			
    			$("#inFlightsDiv").slideDown("slow");    			
    		} else {
    			$("#inFlightsDiv").slideUp("slow");
    		}
    		
    	}
    	
    	function addCurrencyDescription(currencyCode) {
    		$("label[name='valBaseCurrCode']").text(currencyCode);
    	}
    	
    	function addSelectedCurrencyDescription(currencyCode) {
    		$("label[name='valSelectedCurrCode']").text(currencyCode);
    	}
    	
    	function convertAmountToSelectedCurrency(amount){
    		var exchangeRate=UI_Anci.currency.exchangeRate == null ? 1 : UI_Anci.currency.exchangeRate;
    		return $.airutil.format.currency(amount * exchangeRate);
    	}
    	
    	$.fn.summaryPanel.updateFlightPriceDetails = function() {    		
    		var fareQuote = UI_AvailabilitySearch.fareQuote; 
    		//lak currency selector class    
    		if(fareQuote != null && fareQuote != ""){    			
    			if (fareQuote.inSelectedCurr.currency == fareQuote.inBaseCurr.currency) {				
    				$("#valTotal,#valTotal_tg").removeClass("baseTotalPanel_prc");
    				//$("#valTotal_tg").removeClass("baseTotalPanel_prc");
    				$("#valTotal,#valTotal_tg").addClass("baseTot_prc");
    				//$("#valTotal_tg").addClass("baseTot_prc");
    			} else {
    				$('#valTotal,#valTotal_tg').removeClass("baseTot_prc");
    				//$('#valTotal_tg').removeClass("baseTot_prc");
    				$('#valTotal,#valTotal_tg').addClass("baseTotalPanel_prc");	
    				//$('#valTotal_tg').addClass("baseTotalPanel_prc");
    			}
    		}
    		
    		if (fareQuote != null && fareQuote.segmentFare != null) {
    			addCurrencyDescription(fareQuote.inBaseCurr.currency);    			
    			if (fareQuote.segmentFare.length > 0) {
    				$("#TotalOutFlight").text(Number(fareQuote.segmentFare[0].totalPriceInSelectedCurr).toFixed(2));
					$("#totalOutDiv").slideDown("slow");  
    			}    			
	    		if (fareQuote.segmentFare.length > 1) {
					$("#TotalInFlight").text(Number(fareQuote.segmentFare[1].totalPriceInSelectedCurr).toFixed(2));
					$("#totalInDiv").slideDown("slow");
				}	    		
	    		
	    		var totalPriceInBase = fareQuote.inBaseCurr.totalPrice;
	    		
	    		if (totalPriceInBase > 0 ){
		    		$("#valTotal").text(totalPriceInBase);	    		
		    		$("#totalPriceDiv").slideDown("slow"); 
	    		}else{
	    			$("#valTotal").text("");	    		
		    		$("#totalPriceDiv").slideUp("slow"); 
	    		}

	    		var discountAmount = fareQuote.inBaseCurr.currency != fareQuote.inSelectedCurr.currency ? 
	    								fareQuote.inSelectedCurr.totalIbeFareDiscount : fareQuote.inBaseCurr.totalIbeFareDiscount;
	    		if (parseFloat(discountAmount) > 0) {
	    			$("#valDiscount").text(discountAmount);	    		
		    		$("#totalDiscountDiv").slideDown("slow"); 
	    		}else{
	    			$("#valDiscount").text("");	    		
		    		$("#totalDiscountDiv").slideUp("slow"); 
	    		}
	    		
	    		var promoDiscount = "";
	    		if (fareQuote.inBaseCurr.currency != fareQuote.inSelectedCurr.currency) {
	    			promoDiscount = fareQuote.inSelectedCurr.totalIbePromoDiscount;	    			
	    			var totalPriceInSelCur = fareQuote.inSelectedCurr.totalPrice;
	    			$("#valSelectedTotal").text(totalPriceInSelCur);
	    			addSelectedCurrencyDescription(fareQuote.inSelectedCurr.currency);	    			
	    			$(".trSelectedCurrClass").slideDown("slow");
	    		} else {
	    			promoDiscount = fareQuote.inBaseCurr.totalIbePromoDiscount;
	    			$(".trSelectedCurrClass").slideUp("slow");
	    			
	    			/*
	    			 * If base currency is selected it will be set here. Summary panel currency names 
	    			 * were changed to selectedCurrency which is why this change is taking place.
	    			 */
	    			addSelectedCurrencyDescription(fareQuote.inSelectedCurr.currency);
	    		}
	    		
	    		if(fareQuote.hasPromoDiscount){	    
	    			var isOutBoundPromo = UI_AvailabilitySearch.promotionInfo.applicability =="OB" ? true:false;
	    			if(fareQuote.deductFromTotal){
	    				$("#lblSumPromoDiscount").text(o.labels.lblPromoDiscount); 
	    				$("#lblSumPromoDiscount").attr('title', o.labels.lblPromoDiscount); 
	    				$("#valPromoDiscount").text('-' + promoDiscount);
	    			} else {
	    				$("#lblSumPromoDiscount").text(o.labels.lblPromoDiscountCredit); 
	    				$("#lblSumPromoDiscount").attr('title', o.labels.lblPromoDiscountCredit); 
	    				$("#valPromoDiscount").text(promoDiscount);
	    			}
	    			if(isOutBoundPromo){
	    				$("#totalPromoDiscount_OB_Div").slideDown("slow"); 
	    			}else{
	    				$("#totalPromoDiscountDiv").slideDown("slow"); 
	    			}
	    			
	    		} else {
	    			$("#valPromoDiscount").text("");	    		
	    			$("#totalPromoDiscountDiv").slideUp("slow"); 
	    			$("#totalPromoDiscount_OB_Div").slideUp("slow"); 
	    		}
	    		
			} else {
				$("#totalOutDiv").slideUp("slow"); 
				$("#totalInDiv").slideUp("slow"); 
				$("#totalPriceDiv").slideUp("slow");
				$("#totalDiscountDiv").slideUp("slow");
			} 
	
    	
    	}
    });
    

	}
	    
})(jQuery);

