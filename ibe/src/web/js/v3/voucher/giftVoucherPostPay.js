function UI_giftVoucherPostPay() {
}

var voucherList = new Array();
UI_giftVoucherPostPay.errorInfo = "";
UI_giftVoucherPostPay.lables = "";

$(document).ready(function() {
	UI_giftVoucherPostPay.ready();
});

UI_giftVoucherPostPay.ready = function () {
	$("#btnEmailConf").click(function() {
		UI_giftVoucherPostPay.email();
	});
	$("#btnPrintConf").click(function() {
		UI_giftVoucherPostPay.print();
	});
	
	$("#btnFinish").click(function() {
		SYS_IBECommonParam.homeClick();
	});
	
	UI_giftVoucherPostPay.getVoucherInfo();
	$("#tabFirst").removeClass('stepsItem select');
	$("#tabFirst").addClass('stepsItem selected last-setected');
	$("#tabSecond").removeClass('stepsItem');
	$("#tabSecond").addClass('stepsItem select');
}

UI_giftVoucherPostPay.getVoucherInfo = function() {
	var data = {};
	data['getVoucherDTO'] = true; 
	$.ajax({
		url : 'issueVoucherConfirmation.action',
		dataType : "json",
		data : data,
		type : "POST",
		beforeSend : showProgress(),
		success : UI_giftVoucherPostPay.detailsSuccess
	});
}

UI_giftVoucherPostPay.detailsSuccess = function(response) {	
	
	UI_giftVoucherPostPay.errorInfo = response.errorInfo;
	UI_giftVoucherPostPay.lables = response.jsonLabel;
	UI_giftVoucherPostPay.setLables();
	voucherList = response.voucherList;	
	$("#customerName").text(voucherList[0].paxFirstName + " " + voucherList[0].paxLastName);
	$("#email").text(voucherList[0].email);
	
	for(var i = 0;i<voucherList.length;i++){
		var voucher = voucherList[i];
		var cls;
		if(i%2 == 0){
			cls="defaultRowGap"
		} else {
			cls = "defaultRowGap GridTable"
		}
		var voucherId = voucher.voucherId;
		var amount = voucher.amountInLocal + " " + voucher.currencyCode;
		var validFrom = voucher.validFrom;
		var validTo = voucher.validTo;
		var chkBox = JSON.stringify(voucher);
		
		var htmlToWrite =  "<tr><td width='20%' class='"+cls+"'><label>"+voucherId+"</label></td>"+
	     				   "<td width='20%' class='"+cls+"'><label>"+amount+"</label></td>"+
	     				   "<td width='20%' class='"+cls+"'><label>"+validFrom+"</label></td>"+
	     				   "<td width='20%' class='"+cls+"'><label>"+validTo+"</label></td>"+
	     				   "<td width='20%' class='"+cls+"'><input type='checkbox' name='vchkbx' id='chk_"+i+"' value='"+chkBox+"'></input></td></tr>"    				   
	    $("#voucherListPurchased").append(htmlToWrite); 
	}
	

//	voucher = response.voucherDTO;
//	$("#lblVoucherMessage").text(response.message);
//	$("#voucherId").text(voucher.voucherId);
//	$("#amount").text(voucher.amountInLocal + " " + voucher.currencyCode);
	
//	$("#validFrom").text(voucher.validFrom);
//	$("#validTo").text(voucher.validTo);	   
}

UI_giftVoucherPostPay.setLables = function() {
	var lables = UI_giftVoucherPostPay.lables;
	for ( var k in lables) {
		if (lables.hasOwnProperty(k)) {
			$("#" + k).html(lables[k]);
		}
	}
}

UI_giftVoucherPostPay.email = function() {	
	var vouchersToEmail = new Array();
	$("input:checkbox[name=vchkbx]:checked").each(function(){
		vouchersToEmail.push($(this).val());
	});	
	if(vouchersToEmail.length==0){
		jAlert(UI_giftVoucherPostPay.errorInfo["selectToEmail"], 'Alert');
	} else {
		var data = {};
		for(var i=0 ; i<vouchersToEmail.length; i++){
			var voucher  = JSON.parse(vouchersToEmail[i]);
			if (voucher != null) {				
				data["voucherDTOs["+i+"].validFrom"] = voucher.validFrom;
				data["voucherDTOs["+i+"].validTo"] = voucher.validTo;
				data["voucherDTOs["+i+"].amountInLocal"] = voucher.amountInLocal;
				data["voucherDTOs["+i+"].currencyCode"] = voucher.currencyCode;
				data["voucherDTOs["+i+"].paxFirstName"] = voucher.paxFirstName;
				data["voucherDTOs["+i+"].paxLastName"] = voucher.paxLastName;
				data["voucherDTOs["+i+"].email"] = voucher.email;
				data["voucherDTOs["+i+"].voucherId"] = voucher.voucherId;
				data["voucherDTOs["+i+"].templateId"] = voucher.templateId;
			}
		}
		$.ajax({
			url : 'emailVoucherIBE.action',
			dataType : "json",
			data : data,
			type : "POST",
			beforeSend : showProgress(),
			success : UI_giftVoucherPostPay.voucherEmailSuccess
		});		
	}
}

UI_giftVoucherPostPay.voucherEmailSuccess = function() {
	hideProgress();
//	$("#postPaymentSuccess").hide();
//	$("#postPaymentDetails").hide();
//	$("#emailSuccess").show();
}

UI_giftVoucherPostPay.print = function() {
	var vouchersToPrint = new Array();
	$("input:checkbox[name=vchkbx]:checked").each(function(){
		vouchersToPrint.push($(this).val());
	});	
	if(vouchersToPrint.length==0){
		jAlert(UI_giftVoucherPostPay.errorInfo["selectToPrint"], 'Alert');
	} else if (vouchersToPrint.length>1){
		jAlert(UI_giftVoucherPostPay.errorInfo["selectOne"], 'Alert');
	} else{
		var voucher = JSON.parse(vouchersToPrint[0]);
		if (voucher != null) {
			var validFrom = voucher.validFrom;
			var validTo = voucher.validTo;
			var amountInLocal = voucher.amountInLocal;
			var currencyCode = voucher.currencyCode;
			var paxFirstName = voucher.paxFirstName;
			var paxLastName = voucher.paxLastName;
			var voucherId = voucher.voucherId;
			var templateId = voucher.templateId;
	
			var strUrl = "printVoucherIBE.action" + "?validFrom=" + validFrom
					+ "&validTo=" + validTo + "&amountInLocal=" + amountInLocal
					+ "&currencyCode=" + currencyCode + "&paxFirstName="
					+ paxFirstName + "&paxLastName=" + paxLastName + "&voucherId="
					+ voucherId + "&templateId=" + templateId;
			showProgress();
			top.objCW = window.open(strUrl, "myWindow", $("#popWindow")
					.getPopWindowProp(630, 900, true));
			if (top.objCW) {
				top.objCW.focus()
			}
			UI_giftVoucherPostPay.voucherPrintSuccess();
		}
	}

}

UI_giftVoucherPostPay.voucherPrintSuccess = function() {
	hideProgress();
//	$("#postPaymentSuccess").hide();
//	$("#postPaymentDetails").hide();
//	$("#emailSuccess").hide();
}

showProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").show();
}

hideProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").hide();
}