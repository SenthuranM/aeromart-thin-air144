function UI_aeroMartPayment(){
}

UI_aeroMartPayment.paymentOptions = null;
UI_aeroMartPayment.selectedCard = null;
UI_aeroMartPayment.messeges = null;
UI_aeroMartPayment.validate = {};


document.ready = function () {
	UI_aeroMartPayment.ready();
}

UI_aeroMartPayment.ready = function() {
	UI_aeroMartPayment.paymentOptions = paymentOptions;
	UI_aeroMartPayment.messages = messages;
	UI_aeroMartPayment.resetValidations();
	UI_aeroMartPayment.resetDetailFields();
	UI_aeroMartPayment.initPaymentOptions(UI_aeroMartPayment.paymentOptions);
	UI_aeroMartPayment.setFieldTypes();
	UI_aeroMartPayment.resetValidations();
	UI_aeroMartPayment.resetErrors();
	$('[name="cardType"]').click(function(){
		UI_aeroMartPayment.setSelectedOptions();
	});
	UI_aeroMartPayment.setMessages();
	$("#submitPayment").click(function(){
		showLoading();
		UI_aeroMartPayment.submitPayment();
	});
	UI_aeroMartPayment.setSelectedOptions();
}

UI_aeroMartPayment.resetValidations = function () {
	UI_aeroMartPayment.validate.cardNumber = false;
	UI_aeroMartPayment.validate.cardCVV = false;
	UI_aeroMartPayment.validate.cardExpiry = false;
	UI_aeroMartPayment.validate.cardHolderName = false;
}

UI_aeroMartPayment.setMessages = function () {
	var errors = '';
	if (UI_aeroMartPayment.messages != null) {
		for (var i = 0; i < UI_aeroMartPayment.messages.length ; i++){
			errors += "<li class='li-error'><label class='li-label'>" + UI_aeroMartPayment.messages[i] + "</label></li>"
		}
		$("#ulError").html(errors);
	}
}

UI_aeroMartPayment.setSelectedOptions = function () {
	var paymentOption = $('input[name="cardType"]:checked').val();
	var cardType = paymentOption.split("_")[0];
	var gatewayId = paymentOption.split("_")[1];
	$("#paymentgatewayId").val(gatewayId);
	UI_aeroMartPayment.resetCCDetails();
	for (var j = 0 ; j < UI_aeroMartPayment.paymentOptions.length ; j++){
		var cards = UI_aeroMartPayment.paymentOptions[j].cards;
		var paymentGateway = UI_aeroMartPayment.paymentOptions[j];
		if (paymentGateway.gatewayId == gatewayId) {
			$("#pg_description").html(paymentGateway.description);
			$("#pg_curr").html(paymentGateway.paymentCurrency);
			$("#pg_amount").html(paymentGateway.amountInPgBaseCurrency);
			for (var i = 0 ; i < cards.length ; i++) {
				if(cardType == cards[i].cardType) {
					UI_aeroMartPayment.selectedCard = cards[i];
					UI_aeroMartPayment.enableDetails(cards[i].requiredUserInputs);
					UI_aeroMartPayment.limitFieldLengths(cards[i].cardType);
					$("#cardBehavior").val(cards[i].cardBehavior);
					$("#cardType").val(cards[i].cardType);
					break;
				}
			}
		}
	}
}

UI_aeroMartPayment.validateCard = function (cardType, cardNumber) {
	return ValidateCardNos(cardType.toString(), cardNumber);
}

UI_aeroMartPayment.enableDetails = function(requiredUserInputs){
	UI_aeroMartPayment.resetDetailFields();
	UI_aeroMartPayment.resetValidations();
	UI_aeroMartPayment.resetErrors();
	for (var i = 0 ; i < requiredUserInputs.length ; i++){
		var showField = "";
		var enableField = "";
		switch (trim(requiredUserInputs[i])){
			case "cardNumber":
				$("#div_card_no").show();
				UI_aeroMartPayment.validate.cardNumber = true;
				$("#cardNumber").removeAttr("disabled");
				break;
			case "cardCVV":
				$("#div_cvv").show();
				UI_aeroMartPayment.validate.cardCVV = true;
				$("#cvv").removeAttr("disabled");
				break;
			case "cardExpiry":
				$("#div_expiry_date").show();
				UI_aeroMartPayment.validate.cardExpiry = true;
				$("#expDateMonth").removeAttr("disabled");
				$("#expDateYear").removeAttr("disabled");
				break;
			case "cardHolderName":
				$("#div_card_holder").show();
				UI_aeroMartPayment.validate.cardHolderName = true;
				$("#cardHolderName").removeAttr("disabled");
				break;
		}
	}
}

UI_aeroMartPayment.resetDetailFields = function () {
	$("#div_card_no").hide();
	$("#divCardNumber").attr("disabled", "disabled");
	$("#div_expiry_date").hide();
	$("#expDateMonth").attr("disabled", "disabled");
	$("#expDateYear").attr("disabled", "disabled");
	$("#div_cvv").hide();
	$("#cvv").attr("disabled", "disabled");
	$("#div_card_holder").hide();
	$("#cardHolderName").attr("disabled", "disabled");
}

UI_aeroMartPayment.resetCCDetails = function () {
	$("#cardNumber").val("");
	$("#expDateMonth").val("");
	$("#expDateYear").val("");
	$("#cvv").val("");
	$("#cardHolderName").val("");
}

UI_aeroMartPayment.setFieldTypes = function () {
	$("#cardNumber").numeric();
	$("#cvv").numeric();
	$("#cardHolderName").alphaNumericWhiteSpace();
}


UI_aeroMartPayment.initPaymentOptions = function (paymentGateways) {

	var cardOptions='';
	for (var j = 0 ; j < paymentGateways.length ; j++){
		var cards = paymentGateways[j].cards;
		var paymentGateway = paymentGateways[j];
		$("#pg_description").html(paymentGateway.description);
		for (var i = 0 ; i < cards.length ; i++) {
			var card = cards[i];
			var payment_option = card.cardType + '_' + paymentGateway.gatewayId;
			var id = 'payment' + '_' + card.cssClassName + '_' + payment_option;
			cardOptions += '<span class="radio payment-logo">';
			if (i == 0 && j == 0) {
				cardOptions += '<input name ="cardType" type="radio" id="' + id + '" value="' + payment_option + '" checked>';
			} else {
				cardOptions += '<input name ="cardType" type="radio" id="' + id + '" value="' + payment_option + '">';
			}
			cardOptions += '<label for="' + id + '">';
			cardOptions += '<i class="ico">';
			cardOptions += '<div class="">';
			cardOptions += '<img ng-src="../images/aeroMARTPay/payment-logos/' + card.cssClassName.toLowerCase() + '.jpg" src="../images/aeroMARTPay/payment-logos/' + card.cssClassName.toLowerCase() + '.jpg">';
			cardOptions += '<div class="text truncate" title="' + card.cssClassName + '" style="line-height:16px; padding-left: 25px; width:160px">';
			cardOptions += card.cssClassName;
			cardOptions += '</div></div></i></label></span>';
		}
	}
	$(".payment-options").html(cardOptions);
}
UI_aeroMartPayment.submitPayment = function () {
	$("#expiryDate").val( $("#expDateMonth").val() + $("#expDateYear").val() );
	UI_aeroMartPayment.resetErrors();
	if (UI_aeroMartPayment.validatePaymentDetails()) {
		$("#payment-form").submit();
	} else {
		hideLoading();
	}
}

UI_aeroMartPayment.resetErrors = function (){
	$("#cardRequiredError").hide();
	$("#expiryRequiredError").hide();
	$("#cvvRequiredError").hide();
	$("#cardHolderRequiredError").hide();
	$("#cardInvalidError").hide();
	$("#expiryInvalidError").hide();
	$("#cvvInvalidError").hide();
	$("#cardHolderInvalidError").hide();
}

UI_aeroMartPayment.limitFieldLengths = function (cardType) {
	var maxLength = "";
	switch (cardType.toString()) {
		case "3" :
			maxLength = "15";
			break;
		case "4" :
			maxLength = "14";
			break;
		case "1" :
			maxLength = "16";
			break;
		case "2" :
			maxLength = "16";
			break;
		case "5" :
			maxLength = "16";
			break;
		case "6" :
			maxLength = "16";
			break;
		default	:
			maxLength = "16";
	}
	$("#cardNumber").attr("maxlength", maxLength.toString());
	$("#cvv").attr("maxlength", "4");
}

UI_aeroMartPayment.validatePaymentDetails = function () {
	var cardNumber = $("#cardNumber").val();
	var cvv = $("#cvv").val();
	var expiryDate = $("#expiryDate").val();
	var cardHolderName = $("#cardHolderName").val();
	
	var valid = true;
	
	if (UI_aeroMartPayment.validate.cardNumber) {
		if (cardNumber == null || trim(cardNumber)=="") {
			valid = false;
			$("#cardRequiredError").show();
		} else if (!UI_aeroMartPayment.validateCard(UI_aeroMartPayment.selectedCard.cardType, cardNumber)) {
			$("#cardInvalidError").show();
			valid = false;
		}
	}
	
	if (UI_aeroMartPayment.validate.cardCVV){
		if(cvv == null || trim(cvv)=="") {
			$("#cvvRequiredError").show();
			valid = false;
		} else if (trim(cvv).length < 3) {
			$("#cvvInvalidError").show();
			valid = false;
		}
	}
	
	if (UI_aeroMartPayment.validate.cardExpiry){
		if(expiryDate == null || trim(expiryDate)=="") {	
			$("#expiryRequiredError").show();
			valid = false;
		} else if (trim(expiryDate).length != 4) {
			$("#expiryInvalidError").show();
			valid = false;
		}
	}
	
	if (UI_aeroMartPayment.validate.cardHolderName && (cardHolderName == null || trim(cardHolderName)=="")) {
		$("#cardHolderRequiredError").show();
		valid = false;
	}
	
	return valid;
}

showLoading = function () {
	$("#overLayingMask").addClass("overlay");
	$("#loadImage").addClass("overlayImage");
}

hideLoading = function () {
	$("#overLayingMask").removeClass("overlay");
	$("#loadImage").removeClass("overlayImage");
}
