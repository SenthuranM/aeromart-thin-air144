    /*
	*********************************************************
		Description		: Common Routings to get the Control information
		Author			: Rilwan A. Latiff
		Version			: 1.1
		Created         : 1st June 2005
		Last Modified	: 22nd March 2009
    ---------------------------------------------------------
    Ver 1.0  - 01st June 2005 - 1st Release
    Ver 1.1  - 22nd March 2009 - Re orginize the funciton with the additional set of functions		
    *********************************************************	
	*/
	 
	/* Get the control as Object */
	function getFieldByID(strControlID){return document.getElementById(strControlID);}
	function getFieldByName(strControlName){return document.getElementsByName(strControlName);}
	
	/* Get the Field Type */
	function getFieldType(objControl){return objControl.type.toUpperCase();}
	
	/* Set values to a control  */
	function getText(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.options[objControl.selectedIndex].text;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].text;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	/* Set values to a control  */
	function getValue(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				if (intLength > 0)
				for (var i = 0 ; i < intLength ; i++){
					if (strReturn != ""){strReturn += ","; }
					strReturn += objControl[i].checked;
				}				
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				for (var i = 0 ; i < intLength ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.value ;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].value;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	/* Set values to a control  */
	function setField(strControlID, strControlValue){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		switch (strType){
			case "TEXT" : objControl.value = strControlValue ; break ;
			case "PASSWORD" : objControl.value = strControlValue ; break ;
			case "HIDDEN" : objControl.value = strControlValue ; break ;
			case "TEXTAREA" : objControl.value = strControlValue ; break ;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				if (strControlValue != true && strControlValue != false){
					var arrConValue = strControlValue.split(",");
					var intArrLength = arrConValue.length ;
					if (strControlValue == ""){
						for (var i = 0 ; i < intLength ; i++){
							objControl[i].checked = false;
						}				
					}
					
					for (var x = 0; x < intArrLength ; x++){
						for (var i = 0 ; i < intLength ; i++){
							if (objControl[i].value == arrConValue[x]){
								if (!objControl[i].disabled){
									objControl[i].checked = true;
								}
								break;
							}
						}
					}
				}else{
					for (var i = 0 ; i < intLength ; i++){
						if (!objControl[i].disabled){
							objControl[i].checked = strControlValue;
						}
						
					}
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				if (strControlValue == ""){
					for (var i = 0 ; i < intLengrh ; i++){
						objControl[i].checked = false;
					}				
				}
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].value == strControlValue){
						objControl[i].checked = true;
						break;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].value == strControlValue){
						objControl.options[i].selected = true;
						break;
					}
				}
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				var arrConValue = strControlValue.split(",");
				var intArrLength = arrConValue.length ;
				if (strControlValue == ""){
					for (var i = 0 ; i < intLengrh ; i++){
						objControl.options[i].selected = false;
					}				
				}
				for (var x = 0; x < intArrLength ; x++){
					for (var i = 0 ; i < intLengrh ; i++){
						if (objControl.options[i].value == arrConValue[x]){
							objControl.options[i].selected = true;
							break;
						}
					}				
				}
				break;
		}
	}
	
	/* Set the Background Color */
	function setBGColor(strControlID, strColor){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.backgroundColor = strColor;
	}
	
	/* Set the Navigate URL for a Anchor Tags */
	function setLinkUrl(strControlID, strNavigateURL){
		var objControl = getFieldByID(strControlID) ;
		objControl.href= strNavigateURL;
	}
	
	/* Set Style Sheet Class */
	function setStyleClass(strControlID, strClassName){
		var objControl = getFieldByID(strControlID) ;
		objControl.className = strClassName
	}
	
	/* get Style Sheet Class */
	function getStyleClass(strControlID){
		var objControl = getFieldByID(strControlID) ;
		return objControl.className;
	}
	
	/* Write to Divs / span */
	function divWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
	
	/* Read the div information */
	function divRead(strDivID){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			return objControl.innerHTML
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			return objControl.innerHTML;
		}
		else if (document.layers)
		{
			return "";
		}
	}
	
	/* Set Image */
	function setImage(strImageID, strImagePath){
		var objControl = getFieldByID(strImageID);
		objControl.src = strImagePath;
	}
	
	/* set Background Image */
	function setBGImage(strID, strImagePath){
		var objControl = getFieldByID(strID);
		objControl.style.backgroundImage = "url(" + strImagePath + ")";
	}
	
	/* Set Visibility */
	function setVisible(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if (!blnVisible){
			objControl.style.visibility = "hidden";
		}else{
			objControl.style.visibility = "visible";
		}
	}
	
	/* Get Visible */
	function getVisible(strControlID){
		var objControl = getFieldByID(strControlID);
		if (objControl.style.visibility == "visible"){
			return true;
		}else{
			return false;
		}
	}
	
	/* Set Display */
	function setDisplay(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if (!blnVisible){
			objControl.style.display = "none";
		}else{
			objControl.style.display = "block";
		}
	}
	
	/* Get Display */
	function getDisplay(strControlID){
		var objControl = getFieldByID(strControlID);
		if (objControl.style.display == "block"){
			return true;
		}else{
			return false;
		}
	}
	
	/* Set Checked status used for Radio Buttond / Check box */
	function setChecked(strControlID, blnStatus){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		var intLen = 0;
		var i = 0;
		switch (strType){
			case "CHECKBOX" : 
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				intLen = objControl.length;
				if (intLen > 0){
				    i = 0;
				    do{
				        objControl[i].checked = blnStatus;
				        i++;
				    }while(--intLen)
				}
				break;
		}	
	}
	
	/* Get Checked status */
	function getChecked(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var blnStatus = false;
		switch (strType){
			case "CHECKBOX" : 
				if (objControl.checked){
					blnStatus = true;
				}
				break;
		}	
		return blnStatus
	}
	
	/* Set Read Only */
	function readOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable; break;
			case "PASSWORD" : objControl.readOnly = blnEnable; break;
			case "TEXTAREA" : objControl.readOnly = blnEnable; break;
		}
	}
	
	/* Enable / Disable a Conrol */
	function setDisable(strControlID, blnStatus){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		var intLen = 0;
		var i = 0;
		
		switch (strType){
			case "CHECKBOX" : 
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				intLen = objControl.length;
				if (intLen > 0){
				    i = 0;
				    do{
				        objControl[i].disabled = blnStatus;
				        i++;
				    }while(--intLen)
				}
				break;
				
			default :
				objControl.disabled = blnStatus ; 
				break ;
		}	
	}
	
	/* get The Disabled Status */
	function getDisable(strID){
		return getFieldByID(strID).disabled;
	}
	
	/* set Z-index */
	function setZIndex(strControlID, intIndex){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.zIndex = intIndex;
	}
	
	/* Set the Cursor */
	function setCursor(strControlID, strCursor){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.cursor = strCursor;
	}
	
	/* Set the Maxlength used for text area keypress */
	function MaxLength(objControl, objEvent, intLen){
		if ((objEvent.keyCode == 9) || (objEvent.keyCode == 46) || (objEvent.keyCode == 8) || (objEvent.keyCode == 37) || (objEvent.keyCode == 38) || (objEvent.keyCode == 39) || (objEvent.keyCode == 40)){
			return true
		}else{
			if (objControl.value.length >= intLen){
				var strValue = objControl.value
				objControl.value = strValue.substr(0, intLen);
				return false;
			}
		}
		return true;
	}
	
	/* Number convert to Arabic */
	function numberConvertToArabic(intNumber){
		var arrNumbers = new Array("&#1632;","&#1633;","&#1634;","&#1635;","&#1636;","&#1637;","&#1638;","&#1639;","&#1640;","&#1641;")
		intNumber = String(intNumber);
		var intLength = intNumber.length;
		var strReturn = "" ;
		for (var i = 0 ; i < intLength ; i++){
			if (intNumber.substr(i,1) != " "){
				if (!isNaN(intNumber.substr(i,1))){
					strReturn += arrNumbers[intNumber.substr(i,1)];
				}else{
					strReturn += intNumber.substr(i,1);
				}
			}else{
				strReturn += intNumber.substr(i,1);
			}
		}
		
		return strReturn;
	}
	
	/* Currency Format */
	function currencyFormat(num, decimals){
		var blnDecimals = true;
		if (arguments.length > 2){
			blnDecimals = arguments[2];
		}
		
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = String(Math.floor(num/100));
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
		var strReturnValue = "";
		if (blnDecimals){
			strReturnValue = (((sign)?'':'-') + num + '.' + cents);
		}else{
			strReturnValue = (((sign)?'':'-') + num);
		}
		return strReturnValue;
	}
	
	/* Add Decimals */
	function addDecimals(strValue, intDecimals){
		var strReturn = "";
		var intDevide = "1"
		for (var i = 0 ; i < Number(intDecimals) ; i++){
			intDevide += "0";
		}
		strValue = String(Math.round(Number(strValue) * Number(intDevide))/Number(intDevide))
		var intIndex = strValue.indexOf(".")
		var strDecimals = "";
		if (intIndex !=  -1){
			var arrData = strValue.split(".");
			if (arrData.length > 1){
				strDecimals = arrData[1]
				for (var i = strDecimals.length ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
				strDecimals = strDecimals.substr(0, intDecimals);
			}else{
				for (var i = 0 ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
			}
			strReturn = arrData[0] + "." + strDecimals ;		
		}else{
			for (var i = 0 ; i < Number(intDecimals) ; i++){
				strDecimals += "0" ;
			}	
			strReturn = strValue + "." + strDecimals ;
		}
		return strReturn;
	}
	
	/* Comma Formated Numbers */
	function commaFormatted(amount){
	    var delimiter = ","; // replace comma if desired
	    var a = amount.split('.',2)
	    var d = a[1];
	    var i = parseInt(a[0]);
	    if(isNaN(i)) { return ''; }
	    var minus = '';
	    if(i < 0) { minus = '-'; }
	    i = Math.abs(i);
	    var n = new String(i);
	    var a = [];
	    while(n.length > 3)
	    {
		    var nn = n.substr(n.length-3);
		    a.unshift(nn);
		    n = n.substr(0,n.length-3);
	    }
	    if(n.length > 0) { a.unshift(n); }
	    n = a.join(delimiter);
	    if(d.length < 1) { amount = n; }
	    else { amount = n + '.' + d; }
	    amount = minus + amount;
	    return amount;
    }
	
	/* Round Number */
	function roundNumber(intNumber, intDecimals) {
    	intNumber += '';	/* CONVERT NUMBER TO STRING */
    	var arNUMS = intNumber.split('.');		/* GET DECIMAL NUMBERS */
    	/* IF THERE IS NO DECIMAL, THEN return THE NUMBER */


       	if( arNUMS.length < 2 ) {
       		return intNumber;
       	}
       	var intPower = Math.pow(10, arNUMS[1].length);
       	var intRound = intNumber;

        for( var i = 0; i <= arNUMS[1].length - intDecimals; i++ ) {
        	intRound = (Math.round((intRound * intPower)) / intPower);
        	intPower = intPower / 10;
        }
        return intRound;
	}

    /* Generate the Error */
	function raiseError(strErrNo){
		var strMsg = arrError[strErrNo];
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMsg;
	}
		
	/* Build Error Message */
	function buildError(strMessage){
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMessage = strMessage.replace("#" + (i+1), arguments[i+1]);
			}
			strMessage = strMessage
		}
		return strMessage;
	}
	
	/* Check Invalid Characters	 */
	function checkInvalidChar(strValue, strMessage, strControlText){
		var strChkEmpty = FindChar(strValue)
		if (strChkEmpty != "0"){
			return buildError(strMessage, strChkEmpty[0], strControlText);
		}else{
			return "";
		}
	}
	
	/* Find invalid characters */
	function FindChar(StringIn){
		// ------------- Check the standards characters
		/*
		\b 	Backspace
		\f 	Form feed
		\n 	New line
		\r 	Carriage return
		\t 	tab
		\" 	quotation mark
		\'  MS Office single quote 6
		\'  Ms Office singel Quote 9
		\\  Back Slash
		*/

		var CharInArray = new Array("'","<",">","^",'"',"~","--");
		var CharOutArray=new Array();
		for (var i=0;i<StringIn.length;i++){
			switch (StringIn.charCodeAt(i)){
				case 92 :
					CharOutArray[0]="\\ "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				case 8216 :
				case 8217 :
					CharOutArray[0]="' "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				default :
					for (var j=0;j<CharInArray.length;j++){
						if (StringIn.charAt(i)==CharInArray[j]){
							CharOutArray[0]=CharInArray[j]
							CharOutArray[1]=eval(i+1);
							return (CharOutArray);
						}
					}
					break;
			}
		}
		
		for (var j=0;j<CharInArray.length;j++){
			if (StringIn.indexOf(CharInArray[j]) != -1){
				CharOutArray[0]=CharInArray[j] + " ";
				CharOutArray[1]=StringIn.indexOf(CharInArray[j]);
				return (CharOutArray)
			}
		}
		return "0";
	}
	
	/* context */
	function disablePage(){
		return true;
	}
	
	/* Right Trim */
	function rightTrim(strValue){
		var objRegExp = /^([\w\W]*)(\b\s*)$/;
	
	      if(objRegExp.test(strValue)) {
	       /*remove trailing a whitespace characters*/
	       strValue = strValue.replace(objRegExp, '$1');
	    }
	  	return strValue;
	}
	
	/* left trim  */
	function leftTrim(strValue){
		var objRegExp = /^(\s*)(\b[\w\W]*)$/;
	
	  	if(objRegExp.test(strValue)) {
	        /*remove leading a whitespace characters*/
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	  	return strValue;
	}
	
	/* trim */
	function trim(strValue){
		var objRegExp = /^(\s*)$/;
	
	    /*check for all spaces */
	    if(objRegExp.test(strValue)) {
	       strValue = strValue.replace(objRegExp, '');
	       if( strValue.length == 0)
	          return strValue;
	    }
	
	   /*check for leading & trailing spaces */
	   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	   if(objRegExp.test(strValue)) {
	       /*remove leading and trailing whitespace characters */
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	
	  	return strValue;
	}
	
	/* Replace All */
	function replaceAll(strValue, strRepValue, strNValue){
        strRepValue = "/" + "\\" + strRepValue + "/" + "g";
        strValue = strValue.replace(eval(strRepValue) , strNValue);
	    return strValue;
    }
    
    /* clear cache */
	function clearCache(){
		try{
			window.clipboardData.clearData()
		}catch(ex){}
	}
	
	/* conver the text to Title Case */
	function converToTitleCase(strValue){
		var arrValues = strValue.split(" ");
		strValue = "";
		var strFC = ""
		var strOC = ""
		for (var i = 0; i < arrValues.length ; i++){
			strOC = "";
			if (strValue != ""){strValue += " ";}
			arrValues[i] = trim(arrValues[i]);
			arrValues[i] = arrValues[i].toLowerCase();
			strFC = arrValues[i].substr(0,1).toUpperCase();
			if (arrValues[i].length > 1){
				strOC = arrValues[i].substr(1,arrValues[i].length - 1).toLowerCase()
			}
			strValue += strFC + strOC;
		}
		return strValue
	}
	
	/* Copy array to a another array */
	function arrayClone(arrSource, arrTarget){
		arrTarget.length = 0 ; 
		for (var i = 0 ; i < arrSource.length ; i++){
			arrTarget[i] = new Array();
			for (var m = 0 ; m < arrSource[i].length ; m++){
				arrTarget[i][m] = arrSource[i][m];
			}
		}
	}
	
	/* copy key array to a another array */
	function keyArrayClone(objArray, objTarget){
		objTarget = new Array();
		for(var key in objArray){
			if(objArray[key] != null){
				objTarget[key] = objArray[key];
			}
		}
		return objTarget; 
	}
	
	/* focus to a control */
	function setFocus(id){
		var objControl = getFieldByName(id);
		var intLength = objControl.length ;
		if (intLength > 1){
			objControl[0].focus();
		}else{
			document.getElementById(id).focus();
		}
	}
	
	/* Convert the Ajax restriceted post values to correct value  */
	function convertAjaxData(strValue){
	    strValue = strValue.replace(/\+/g,'%2B'); /* + sign */
		return strValue;
	}
	
	/* Ajax  */
	/* ---------------- Remote scripting  */
	var reqObj;
  	function retrieveURL(url) {
		if (window.XMLHttpRequest) { // Non-IE browsers
      		reqObj = new XMLHttpRequest();
      		reqObj.onreadystatechange = processStateChange;
      		try {
        		reqObj.open("GET", url, true);
      		}catch (e) {
        		showERRMessage(e);
      		}
      		reqObj.send(null);
    	}else if (window.ActiveXObject) { // IE
      		reqObj = new ActiveXObject("Microsoft.XMLHTTP");
      		if (reqObj) {
        		reqObj.onreadystatechange = processStateChange;
        		reqObj.open("GET", url, true);
        		reqObj.send();
      		}
    	}
  	}
  	
	var httpReq=false;
	function makePOST(url,params) {
		httpReq=false;
	    if(window.XMLHttpRequest){ // Mozilla, Safari,...
			httpReq=new XMLHttpRequest();
	    }else if(window.ActiveXObject){ // IE
			try{
	              httpReq=new ActiveXObject("Microsoft.XMLHTTP");
	        } catch (e) {}
	    }
	
	    if (!httpReq) {
	        alert('Cannot create XMLHTTP instance');
	        return false;
	    }
	     
	    params = convertAjaxData(params); 
	    httpReq.onreadystatechange=processStateChange;
	    httpReq.open('POST', url, true);
	    httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//	    httpReq.setRequestHeader("Content-length",params.length);
//	    httpReq.setRequestHeader("Connection","close");
	    httpReq.send(params);
	    
	    try{
		    if (UI_Top.holder().strTopPage){
			    ResetTimeOut();
		   	}
		}catch(e){}
	}
	
	function makeGet(url) {
		if (window.XMLHttpRequest) { // Non-IE browsers
	   		httpReq=new XMLHttpRequest();
	  	}else if (window.ActiveXObject) { // IE
	   		httpReq=new ActiveXObject("Microsoft.XMLHTTP");
	 	}
	
		if (httpReq) {
			url = convertAjaxData(url);
	   		httpReq.onreadystatechange=processStateChange;
	   		httpReq.open("GET",url,true);
	   		httpReq.setRequestHeader( "If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT" );
	   		httpReq.send(null);
	   	}
	   	/*
	   	var arrData = url.split("?");
	   	makePOST(arrData[0], arrData[1]);
	   	*/
	   	try{
		    if (UI_Top.holder().strTopPage){
			    ResetTimeOut();
		   	}
		}catch(e){}
	}
	
	/* set the Child window Properties  */
	function getChildWindowProp(intHeigh, intWidth){
		var intTop 	= (window.screen.height - intHeigh) / 2;
		var intLeft = (window.screen.width -  intWidth) / 2;
		var strScroll = "no";
		if (arguments.length > 2){
			if (arguments[2] == true){
				strScroll = "yes";
			}
		}
		var strProp = 'toolbar=no,' +
		              'location=no,' +
		              'status=no,' +
		              'menubar=no,' +
		              'scrollbars=' + strScroll + ','+
		              'width=' + intWidth + ',' +
		              'height=' + intHeigh + ',' +
		              'resizable=no,' +
		              'top=' + intTop +',' +
		              'left=' + intLeft
		return strProp
	}
	
	/* Fill characters  */
	function fillChar(strValue, intLength, strChar){
		strValue = trim(String(strValue));
		var intVLength = strValue.length; 
		var strZero = "";
		for (var i = intVLength ; i < intLength ; i++){
			strZero += strChar;
		}
		return String(strZero) + String(strValue);
	}
	
	/* Array Short Client side */
	function sortArray(intColumn, strOrder, strArrayName){
		strArrayName = eval(strArrayName);
		var intCount = strArrayName.length
		for (var a = 1 ; a = 1;){
		//while (blnFound == false){
			var blnFound = false
			for (var i = 0 ; i < intCount ; i++){
				if ((i+1) < intCount){
					var strCompareValue = String(strArrayName[i][intColumn]);
					var strCompareTo = String(strArrayName[i+1][intColumn]);
					
					// Date Check 
					var arrMTemp;
					var arrTemp;
					var strLastValues = ""
					if (String(strCompareValue).indexOf('/') != -1){
						arrMTemp = strCompareValue.split(" ");
						arrTemp = arrMTemp[0].split("/");
						if (arrTemp.length == 3){
							if (arrTemp[2].length == 4 && arrTemp[1].length == 2 && arrTemp[0].length == 2){
								strLastValues = ""
								if (arrMTemp.length == 2){
									strLastValues = arrMTemp[1].replace(":","");
								}
								strCompareValue = String(arrTemp[2]) + String(arrTemp[1]) + String(arrTemp[0]) + strLastValues
							}
						}
					}
					
					if (strCompareValue.indexOf(',') != -1){
						strCompareValue = strCompareValue.replace(',',"");
					}
					
					if (strCompareTo.indexOf('/') != -1){
						arrMTemp = strCompareTo.split(" ");
						arrTemp = arrMTemp[0].split("/");
						if (arrTemp.length == 3){
							strLastValues = ""
								if (arrMTemp.length == 2){
									strLastValues = arrMTemp[1].replace(":","");
								}
								strCompareTo = String(arrTemp[2]) + String(arrTemp[1]) + String(arrTemp[0]) + strLastValues
						}
					}
					
					if (strCompareTo.indexOf(',') != -1){
						strCompareTo = strCompareTo.replace(',',"")
					}
					if (!isNaN(Number(strCompareValue))){
						strCompareValue = Number(strCompareValue);
					}else{
						strCompareValue = strArrayName[i][intColumn];
					}
					if (!isNaN(Number(strCompareTo))){
						strCompareTo = Number(strCompareTo);
					}else{
						strCompareTo = strArrayName[i+1][intColumn];
					}
					if (strOrder.toUpperCase() == "ASC"){
						if (strCompareValue > strCompareTo){
							var arrCurrData = new Array();
							var intarrColumns = strArrayName[i].length ;
							for (var x = 0 ; x < intarrColumns ; x++){
								arrCurrData[x] = strArrayName[i][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i][x] = strArrayName[i+1][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i+1][x] = arrCurrData[x];
							}
							blnFound = true
						}
					}else{
						if (strCompareValue < strCompareTo){
							var arrCurrData = new Array();
							var intarrColumns = strArrayName[i].length ;
							for (var x = 0 ; x < intarrColumns ; x++){
								arrCurrData[x] = strArrayName[i][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i][x] = strArrayName[i+1][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i+1][x] = arrCurrData[x];
							}
							blnFound = true
						}				
					}
				}
			}
			if (blnFound == false){
				break;
			}
		}
	}	
	
	/* Check Key Array is available*/
	function checkKeyArrayAvailable(objArray){
		var blnReturn = false;
		for(var key in objArray){
			blnReturn = true;
		}
		return blnReturn;
	}
	
	/* Get Array Code information */
	function getArrayCodes(objArray, intIndex,  strID, intElement){
		var strReturn = "";
		for (var i = 0 ; i < objArray.length ; i++){
			if (objArray[i][intIndex] == strID){
				strReturn = objArray[i][intElement];
				break;
			}
		}
		return strReturn;
	}
	
	/* Get Mouse X */
	function getMouseX(objEvent){
		var x = 0;
		if (browser.isIE) {
			x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				x = objEvent.clientX;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				x = objEvent.clientX;
			}else{
				x = objEvent.clientX + window.scrollX;
			}
		}	
		return x;
	}
	
	/* Get Mouse Y  */
	function getMouseY(objEvent){
		var y = 0;
		if (browser.isIE) {
			y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				y = objEvent.clientY;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				y = objEvent.clientY + document.documentElement.scrollTop;
			}else{
				y = objEvent.clientY + window.scrollY;
			}
		}	
		return y;
	}
	
	/* Select All in the Text */
	function selectAll(strID){
		getFieldByID(strID).select()
	}
	
	/* Window Screen Height  */
	function getPageHeight(){
		var y;
		if (self.innerHeight){
			// all except Explorer{
			y = self.innerHeight;
		}else if (document.documentElement && document.documentElement.clientHeight){
			// Explorer 6 Strict
			y = document.documentElement.clientHeight;
		}else if (document.body){
		 	// other Explorers
			y = document.body.clientHeight;
		}
		return y;
	}
	
	/* Window Screen Width */
	function getPageWidth(){
		var x
		if (self.innerWidth){
		 	// all except Explorer{
			x = self.innerWidth;
		}else if (document.documentElement && document.documentElement.clientWidth){
			// Explorer 6 Strict
			x = document.documentElement.clientWidth;
		}else if (document.body){
			// other Explorers{
			x = document.body.clientWidth;
		}
		return x;
	}
	
	/* Complete page height  */
    function getPageInnerHeight(){
        var intPgHt = null;
        // Firefox
        if( window.innerHeight && window.scrollMaxY ){
	        intPgHt = window.innerHeight + window.scrollMaxY;
        // all but Explorer Mac
        }else if( document.body.scrollHeight > document.body.offsetHeight ){
	        intPgHt = document.body.scrollHeight;
        // works in Explorer 6 Strict, Mozilla (not FF) and Safari
        }else{ 
	       intPgHt = document.body.offsetHeight + document.body.offsetTop; 
        }
        return intPgHt;
    }
    
    /* Complete page Width  */
    function getPageInnerWidth(){
        var intPgWt = null;
        // Firefox
        if( window.innerWidth && window.scrollMaxX ){
	        intPgWt = window.innerWidth + window.scrollMaxX;
        // all but Explorer Mac
        }else if( document.body.scrollWidth > document.body.offsetWidth ){
	        intPgWt = document.body.scrollWidth;
        // works in Explorer 6 Strict, Mozilla (not FF) and Safari
        }else{ 
	        intPgWt = document.body.offsetWidth + document.body.offsetLeft; 
        }
        return intPgWt;
    }
	
	/* Mouse Button Click */
	function getMouseButtonClick(objEvent){
		var intBtn = null;
		if (browser.isIE){
			objEvent = window.event;
		}
		intBtn = objEvent.button;
		return intBtn;
	}
	
	/* Set Tab Index Dynamically */
	function setTabIndex(strControlID, intIndex){
		getFieldByID(strControlID).tabIndex = intIndex;
	}
	
	/* Append Elements to page */
	function appendElement(strHolder, strID, strObjectString){
		if (String(strID) != ""){
			var newDiv = document.createElement('div');
			if (getFieldByID(strID) == null){
				newDiv.setAttribute("id", strID);
				newDiv.innerHTML = strObjectString
				getFieldByID(strHolder).appendChild(newDiv);
			}
		}
	}

	/* Remove elements from page */
	function removeElement(strHolder, strID) {
		var d = document.getElementById(strHolder);
		try{
			var olddiv = getFieldByID(strID);
			if (getFieldByID(strID) != null){
				d.removeChild(olddiv);
			}
		}catch(ex){}
	}
	
	/* Left Position */
	function getLeft(strID){
		return getFieldByID(strID).offsetLeft;
	}
	
	/*
	 * Top Position
	 */
	function getTop(strID){
		return getFieldByID(strID).offsetTop;
	}
	
	/* Inner Height */
	function getHeight(strID){
	    return getFieldByID(strID).offsetHeight;
	}
	
	/* Inner Width */
	function getWidth(strID){
	    return getFieldByID(strID).offsetWidth;
	}
	
	//------------------------------------------------
	// Deprecated
	//------------------------------------------------
	
	/* Write to Divs / span */
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
	
	/* Read the div information */
	function DivRead(strDivID){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			return objControl.innerHTML
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			return objControl.innerHTML;
		}
		else if (document.layers)
		{
			return "";
		}
	}
	
	/* Set ReadOnly	 */
	function ReadOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable ; break ;
			case "PASSWORD" : objControl.readOnly = blnEnable ; break ;
			case "TEXTAREA" : objControl.readOnly = blnEnable ; break ;
		}	
	}
	
	/* Enable / Disable a Conrol */
	function Disable(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].disabled = blnEnable;
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].disabled = blnEnable;
				}
				break;
			default :
				objControl.disabled = blnEnable ; break ;
		}	
	}
	
	// Currency Format
	function CurrencyFormat(num, decimals){
		/*
		if ((String(amount) == "") || (amount == ".00")){amount = 0;}
		amount = addDecimals(amount, decimals);
		var Num = amount.toString().replace(/\$|\,/g,'');
		var dec = Num.indexOf(".");
		var intCents = ((dec > -1) ? "" + Num.substring(dec,Num.length) : ".00");
		Num = "" + parseInt(Num);
		var intValue = "";
		var intReturnValue = "";
		if (intCents.length == 2) intCents += "0";
		if (intCents.length == 1) intCents += "00";
		if (intCents == "") intCents += ".00";
		var count = 0;
		for (var k = Num.length-1; k >= 0; k--) {
			var oneChar = Num.charAt(k);
			if (count == 3){
				intValue += ",";
				intValue += oneChar;
				count = 1;
				continue;
			}else {
				intValue += oneChar;
				count ++;
			}
		}
		for (var k = intValue.length-1; k >= 0; k--) {
			var oneChar = intValue.charAt(k);
			intReturnValue += oneChar;
		}
		intReturnValue = intReturnValue + intCents;
		if (intReturnValue.indexOf("-,") != -1)
		{
			intReturnValue = intReturnValue.replace("-,","-");
		}
		*/
		
		var blnDecimals = true;
		if (arguments.length > 2){
			blnDecimals = arguments[2];
		}
		
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = String(Math.floor(num/100));
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
		var strReturnValue = "";
		if (blnDecimals){
			strReturnValue = (((sign)?'':'-') + num + '.' + cents);
		}else{
			strReturnValue = (((sign)?'':'-') + num);
		}
		return strReturnValue;
	}
	
	/* Replace all  */
	function replaceall(strValue, strRepValue, strNValue){
		var i = strValue.indexOf(strRepValue);
		while(i > -1){
			strValue = strValue.replace(strRepValue, strNValue);
			i = strValue.indexOf(strRepValue);
		}
		return strValue
	}
	
	/* clear cache */
	function ClearCache(){
		try{
			window.clipboardData.clearData()
		}catch(ex){}
	}
	
	/* close child windows */
	function CloseChildWindow(){
		try{
			if ((UI_Top.holder().objCW) && (!UI_Top.holder().objCW.closed)){
				UI_Top.holder().objCW.close();
			}
		}catch(ex){}
	}
	
	/* get The Disabled Status */
	function getDisabled(strID){
		return getFieldByID(strID).disabled;
	}
	
	/*
	*********************************************************			
		NOTE    NOTE    NOTE    NOTE    NOTE    NOTE    NOTE
    ---------------------------------------------------------
        If you need to add any specific function please add to 
        commonsystem.js file 
	*********************************************************			
	*/
	/* --------------------------------------------------------- end of Page --------------------------------------------------------- */