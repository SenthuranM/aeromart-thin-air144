	/*
	*********************************************************
		Description		: Disable keys (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 1.1
		Careated on		: 04th June 2000
		Last Modified	: 07th October 2005
	*********************************************************	
	*/
	var strPageID = "";
	function Body_onKeyDown(objEvent){
		var strKeyValue = objEvent.keyCode ;
		var strDisable  = "N" ;
		if (objEvent.ctrlKey){ 
			switch (strKeyValue){
				case 69 : // E Search
				case 72 : // H History
				case 73 : // I Bookmarks
				case 76 : // N New window
				case 78 : // O URL	 
				case 79 : // L URL
				case 82 : // R Reload
				case 53 : // 5 select all
					strDisable = "Y"
					break ;
			}		
		}
		
		if (objEvent.altKey){
			switch (strKeyValue){
				case 37 : // Back Arrow
				case 39 : // Forward Arrow
					strDisable = "Y"
					break ;
				case 96 : // 0 NumberPad
				case 97 : // 1 NumberPad
				case 98 : // 2 NumberPad
				case 99 : // 3 NumberPad
				case 100 : // 4 NumberPad
				case 101 : // 5 NumberPad
				case 102 : // 6 NumberPad
				case 103 : // 7 NumberPad
				case 104 : // 8 NumberPad
				case 105 : // 9 NumberPad
					alert("Copyrights to AccelAero. All Rights Reserved");
					strDisable = "Y"
					break ;
			}						
		}
		
		switch (strKeyValue){
			case 122 : // F11
			case 222 : // '
			case 116 : // F5
				strDisable = "Y";
				break ;
			case 191 : // /
				strDisable = "Y";	
				if (browser.isIE){
					if (objEvent.srcElement.invalidText != null){
						if (objEvent.srcElement.invalidText.toUpperCase() == "TRUE"){
							strDisable = "N";					
						}
					}
				}	
			case 13 : // Enter Key
				if (browser.isIE){
					switch (objEvent.srcElement.tagName){
						case "TEXTAREA" :
					 		strDisable = "Y";
							break;
					}
				}else{
					switch (objEvent.target.type){
						case "textarea" :
			 			strDisable = "Y";
						break;
					}
				}
				break ;	
				
			case 8 :   // Backspace
				if (browser.isIE){
					strDisable = "Y";
					switch (objEvent.srcElement.type){
				 		case "text" :
						case "textarea" :
						case "password" :
					 		strDisable = "N";
							break;
					}
				}else{
					strDisable = "Y";
					switch (objEvent.target.type){
			 			case "text" :
						case "textarea" :
						case "password" :
				 			strDisable = "N";
							break;
					}	
				}
				break ;
		}
		
		if (strDisable == "Y"){
			if (browser.isIE){
				objEvent.keyCode = 0;
			}
			return false
		}else{
			return true
		}                       
	} 
	
	
	function Body_onKeyPress(objEvent){

		var strDisable  = "N" ;
		var charac;
		
		if (objEvent.which == null)
		 	charac= String.fromCharCode(objEvent.keyCode);    // IE
	  	else if (objEvent.which > 0)
		 	charac= String.fromCharCode(objEvent.which);
		
		switch (charac){
			case '<' : // <
			case '>' : // >
			case '"' : // "
			case '^' :  // ^	
			case '|' : // |				
				strDisable = "Y"
				break ;
		}		
		
		
		if (strDisable == "Y"){
			if (browser.isIE){
				objEvent.keyCode = 0;
			}
			return false
		}else{
			return true
		}                       
	} 	
	
	
	function PasteValidate(objEvent, objControl){
		var strKeyValue = objEvent.keyCode ;
		var strDisable  = "N" ;
		if (objEvent.ctrlKey){ 
			switch (strKeyValue){
				case 86 : // V
					strDisable = "Y"
					break ;
			}	
		}
		if (strDisable == "Y"){
			objControl.value = "";
			return false
		}else{
			return true
		} 
	}
	
	function homeClick(){
		try{
			switch (strPageID){
				case "PAYMENT" :
				case "CONFIRMATION" :
					document.getElementById("hdnMode").value = "";
					homePgClick();
					break;
				case "ERROR" :
					if (strHomeURL == ""){
						top[0].document.getElementById("hdnMode").value = "";
						top[0].homeClick();
					}else{
						UI_Top.holder().location.replace(strHomeURL);
					}
					break;	
				default :
					top[0].document.getElementById("hdnMode").value = "";
					top[0].homeClick();
					break;
			}
		}catch(e){
		}
	}
	
	// -------------------------------- End of File -----------------------------------