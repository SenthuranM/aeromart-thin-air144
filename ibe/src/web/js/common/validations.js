
//validations.js
//Sudheera Liyanage

function validateUSPhone( strValue ) {
/************************************************
Ex. (999) 999-9999 or (999)999-9999
*/
	var objRegExp  = /^\([1-9][0-9]{2}\)\s?[0-9]{3}\-[0-9]{4}$/;
	var re = new RegExp(objRegExp);
	var ve = re.exec(strValue);
	if (ve == null) {return false;}else{return true}

  	//check for valid us phone with or without space between
  	//area code
}

function showCommonError(msg) {
	alert(msg);
	return true;
}

// to show the error messages - default msgType is Error
function showERRMessage(strErrMessage) {
	alert(strErrMessage);
	return true;
}

function  isNumeric( strValue ) {
	var objRegExp  =  /(^-?[0-9][0-9]*\.[0-9]*$)|(^-?[0-9][0-9]*$)|(^-?\.[0-9][0-9]*$)/;
	var re = new RegExp(objRegExp);
	var ve = re.exec(strValue);
	if (ve == null) {return false;}else{return true}
  	//check for numeric characters
}

function validateInteger( strValue ) {
	var objRegExp  = /(^-?[0-9][0-9]*$)/;

  	//check for integer characters
  	return objRegExp.test(strValue);
}


function validateValue(strValue,strMatchPattern ){
	var re = new RegExp(strMatchPattern);
	var ve = re.exec(strValue);
	if (ve == null) {return false;}else{return true}
 	//check if string matches pattern
}


function rightTrim(strValue){
	var objRegExp = /^([\w\W]*)(\b\s*)$/;
	var re = new RegExp(objRegExp);
	var ve = re.exec(strValue);
	if (ve == null){
	}else{
	   //remove trailing a whitespace characters
       strValue = strValue.replace(objRegExp, '$1');
    }
  	return strValue;
}

function leftTrim(strValue){
	var objRegExp = /^(\s*)(\b[\w\W]*)$/;
	var re = new RegExp(objRegExp);
	var ve = re.exec(strValue);
	if (ve == null){
	}else{
	   	//remove leading a whitespace characters
       strValue = strValue.replace(objRegExp, '$2');
    }
  	return strValue;
}

function trim(strValue){
	var objRegExp = /^(\s*)$/;
	var re = new RegExp(objRegExp);
	var ve = re.exec(strValue);
	if (ve == null){
	}else{
       strValue = strValue.replace(objRegExp, '');
       if( strValue.length == 0)
          return strValue;
    }

    //check for leading & trailing spaces
    objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	var re = new RegExp(objRegExp);
	var ve = re.exec(strValue);
	if (ve == null){
	}else{
       //remove leading and trailing whitespace characters
       strValue = strValue.replace(objRegExp, '$2');
    }
  	return strValue;
}

function currencyValidate(strValue, nValue, nDecimal){
	var strNValue = "*"
	var strDValue = "*" ;
	var strPattern = "" ;
	
	if (nValue != ""){
		strNValue = "{0," + nValue + "}";
	}
	
	if (nDecimal != ""){
		strDValue = "{0," + nDecimal + "}";
	}
	strPattern = "(^-?[0-9]" + strNValue + "\\.[0-9]" + strDValue + "$)|(^-?[0-9]" + strNValue + "$)|(^-?\\.[0-9]" + strDValue + "$)";
	return validateValue(strValue, strPattern);
}

function isEmpty(s) {return (trim(s).length==0);}
function hasWSpace(s){
	var re = new RegExp("^\s*$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
}
function isAlpha(s){
	var re = new RegExp("^[a-zA-Z]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^[a-zA-Z]+$").test(s);
}
function isInt(s){
	var re = new RegExp("^[-+]?[0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^[-+]?[0-9]+$").test(n);
}
function isPositiveInt(s){
	var re = new RegExp("^[+]?[0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^[+]?[0-9]+$").test( n );
}
function isNegativeInt(s){
	var re = new RegExp("^-[0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^-[0-9]+$").test(n);
}
function isAlphaNumeric(s) {
	var re = new RegExp("^[a-zA-Z0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^[a-zA-Z0-9]+$").test(s);
} 
function isAlphaNumericWhiteSpace(s){
	var re = new RegExp("^[a-zA-Z0-9-/ \w\s]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^[a-zA-Z0-9 \w\s]+$").test(s);
}
function isAlphaWhiteSpace(s){
	var re = new RegExp("^[a-zA-Z-/ \w\s]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
}
function isDecimal(s){
	var re = new RegExp("^[-+]?[0-9]+[.][0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^[-+]?[0-9]+[.][0-9]+$").test(n);
}
function isLikeDecimal(n){
	window.status=isInt(n)+'||'+RegExp("^[-+]?[0-9]+[.]+$").test(n)+'||'+isDecimal(n);
	return isInt(n)||RegExp("^[-+]?[0-9]+[.]$").test(n)||isDecimal(n);
}
function isPositiveDecimal(s){
	var re = new RegExp("^[+]?[0-9]+[.][0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return (RegExp("^[+]?[0-9]+[.][0-9]+$").test(n));
}
function isNegativeDecimal(s){
	var re = new RegExp("^-[0-9]+[.][0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^-[0-9]+[.][0-9]+$").test(n);
}

function checkTime(s){
	var re = new RegExp("^[012][0-9]:[0-5][0-9]$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	 //return RegExp( "^[012][0-9]:[0-5][0-9]$" ).test( s );
}
function checkEmail(s){
	var re = new RegExp("^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp( "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$" ).test(s);
}

//**********************
function attachEvents(){
    document.onkeypress=checkOnKeyPress;
    //document.oncontextmenu=stopRightMouse;
	document.body.onpaste=validatePaste;
}	


//**********************
function checkOnKeyPress() {
    var e=event;
    var kCode=e.keyCode;
    var keys1=[34,39,60,62,94,126];
    //alert("validKeysOnly=" + kCode);

    if (kCode=="13"){
		onEnterKeyPressed();
		return;
	}
    keys1="_"+keys1.join("_")+"_";
    if(keys1.indexOf("_"+kCode+"_")!=-1)e.returnValue=false;
}


//**********************
function validatePaste() {
	
    var val=window.clipboardData.getData("Text");
    var obj=event.srcElement
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
			window.clipboardData.setData("Text",removeInvalids(val));
        }
    }
}

//**********************
function removeInvalids(val){
    var re1=/'|"/g;
    var re2=/<|>|~|\^/g;

    val=val.replace(re1,"");
    val=val.replace(re2,"-");
    return val;
}


//**********************
function stopRightMouse(){
    var e=event;
    var obj=e.srcElement;
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
            return true;
        }
    }
    return false;
}

//**********************
function doAfterLoad(){
	attachEvents();
}

//***********************************************
function isError(e,msg,id){
    var blnErr=false;
    var val=getVal(id);
    var obj=getField(id);

    if(e=="NULL"){
        if (isEmpty(val))blnErr=true;
    }else if(e=="NOT_ALPHA"){
        blnErr=!isAlpha(val);
    }else if(e=="NOT_ALPHANUMERIC"){
        blnErr=!isAlphaNumeric(val);
    }else if(e=="NaN"){
            blnErr=(isNaN(val));
    }else if(e=="NOT_FLOAT"){
            blnErr=(!isNaN(val))&&(val.indexOf(".")!=-1);
    }else if(e.substr(0,6)=="MAXVAL"){
        blnErr=(Number(val)>Number(e.substr(6)));
	}else if(e.substr(0,6)=="MINVAL"){
        blnErr=(Number(val)<Number(e.substr(6)));
    }else if(e.substr(0,6)=="MAXLEN"){
        blnErr=(val.length>Number(e.substr(6)));
    }else if(e.substr(0,6)=="MINLEN"){
        blnErr=(val.length<Number(e.substr(6)));
    }else if(e.substr(0,6)=="WHITESPACE"){
        blnErr=hasWSpace();
    }else{
        alert("isErr:"+e+":"+"invalid error type" );
    }

    if (blnErr){
        alert(msg);
		if(obj[0]){
			if (obj.tagName=="SELECT"){
				obj.focus();
			}else{
				obj[0].focus()
			}
		}else if(!(obj.readOnly||obj.disabled||obj.type=='hidden'||obj.style.visibility=='hidden'||obj.style.display=='none')){
			obj.focus();
		}
    }else{
        setField(id,trim(val));
    }

    return blnErr;
}


function onEnterKeyPressed(){

	//override this function if you need in you JS file
}

function removeChars(str,regX){
	return str.replace(new RegExp(regX),"");
}

function keyPressCheckEmail(){
	var obj=window.event.srcElement;
	if(!isEmpty(obj.value)&&!checkEmail(obj.value)){
		alert('Invalid Email Address');
		obj.focus();
	}
}

function ValidateFlagText(textBoxObj,evt) {

    //skip events for space and control keys
   var keyID = evt.charCode;
   if ((keyID==8)||(keyID==13||keyID==46||keyID==0)||(keyID >47 && keyID <58)||(keyID >64 && keyID <91)||(keyID >96 && keyID <123)) {
	   return true;
   }
   else{
	   return false;
   }
}