function doLogoffIfTImeout(){
	document.getElementById("sessionTimer").style.visibility = 'visible';
 	var folderPath = '../images/alert_no_cache.png';
  
	var html = '';
	html+='<table border="0" cellpadding="0" cellspacing="0" height="40px" width="440px" align="center" style="background-color: #F3E798;opacity: .8; filter: alpha(opacity=83); background-repeat:repeat;-moz-border-radius-bottomright:8px; -moz-border-radius-bottomleft:8px; border-bottom: 1px solid  #FFDD00; border-left: 1px solid #FFDD00; border-right: 1px solid #FFDD00;">';
	html+='<tr>';
	html+='<td width="12%" valign="middle">&nbsp;<img id="imgConfirm" src="'+folderPath+'"></td><td width="88%" valign="middle" halign="center"><font class="fntBold">'+ strMsgTimeout +'<span id="spnT">&nbsp;&nbsp;</span></font>'; 
	html+=' <font class="fntBold"> '+ strMsgSec +'  &nbsp;&nbsp;<a href="javascript:chkCancelSession()">&nbsp;&nbsp;<u>'+ strMsgCancel +'</u></a></font>';
	html+='';
	html+='<form method="GET" id="frmSession" name="frmSession"></form>';
	html+='</td>';
	html+='</tr>';
	html+='</table>';
								  					  
			DivWrite("sessionTimer",html);						    				  
			callHoldOn();	 		
 }
 
 function callHoldOn(){
	 setTimeout('timerCall()',1000);
 }
 var sec = 90;
 var isCancelSessionTimer = false;
 function timerCall(){
 	
  alignFloatLayers();
	sec = sec-1;
	document.title = sec + " Sec to Timeout";
	 
	if(sec > 0 && isSessionTimerCancel() != true){
		callHoldOn();
	}else if(sec > 0 && isSessionTimerCancel() == true){
 		document.title = "Session Expired";	
		actionCancelTimeout();
	}
	 	
	DivWrite("spnT", sec);	
	
	if(sec <= 0 && isSessionTimerCancel() == false){
		
		var strAction= UI_Top.holder().strVirtualPath+"showSessionExpire.action";
  					getFieldByID("frmSession").target="_self";;
					getFieldByID("frmSession").action=strAction;
		 			getFieldByID("frmSession").submit();
	}else if(sec == 0 && isSessionTimerCancel() == true){
	 	actionCancelTimeout();
	}else{
		
	}
 
 }
 
 function actionCancelTimeout(){
 		sec = 85;
		document.getElementById("sessionTimer").style.visibility = 'hidden';
		isCancelSessionTimer = false;
		startTimer();
 }
 
 function isSessionTimerCancel(){
 	return isCancelSessionTimer;
 }
 
 function chkCancelSession(){
	isCancelSessionTimer = true;
 }