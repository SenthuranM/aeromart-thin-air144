	/*
	*********************************************************
		Description		: Common Routings to get the Control information
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 1st June 2005
	*********************************************************	
	*/


	function setModel(model){
    	for(var key in model){
    		if(model[key].match('null')){
    			model[key]="";
    		}
     		setField(key,model[key]);
  		}
  	
	}	
	

	// Set the Navigate URL for a Anchor Tag
	function setLinkUrl(strControlID, strNavigateURL){
		var objControl = getFieldByID(strControlID) ;
		objControl.href= strNavigateURL;
	}
	
	// Set Style Sheet
	function setStyleClass(strControlID, strClassName){
		var objControl = getFieldByID(strControlID) ;
		objControl.className = strClassName
	}
	
	// get Style Sheet
	function getStyleClass(strControlID){
		var objControl = getFieldByID(strControlID) ;
		return objControl.className;
	}
		
	// Write to Divs / span
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
	
	// Read the div information
	function DivRead(strDivID){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			return objControl.innerHTML
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			return objControl.innerHTML;
		}
		else if (document.layers)
		{
			return "";
		}
	}
	
	// Set Image 	
	function setImage(strImageID, strImagePath){
		var objControl = getFieldByID(strImageID);
		objControl.src = strImagePath;
	}
	
	// Set Visibility
	function setVisible(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if (!blnVisible){
			objControl.style.visibility = "hidden";
		}else{
			objControl.style.visibility = "visible";
		}
	}
	
	// Get Visibility
	function getVisible(strControlID){
		var objControl = getFieldByID(strControlID);
		if (objControl.style.visibility == "visible"){
			return true;
		}else{
			return false;
		}
	}
	
	function display(id,bln){
		var obj=getFieldByID(id);
		var str=(bln)?"block":"none";
		obj.style.display=str;
	}
	
	// Get the Field Type
	function getFieldType(objControl){
		return objControl.type.toUpperCase();
	}
	
	// Get the control as Object
	function getFieldByID(strControlID){
		return document.getElementById(strControlID) ;
	}
	
	function getFieldByName(strControlName){
		return document.getElementsByName(strControlName) ;
	}
	
	
	// Set ReadOnly	
	function ReadOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable ; break ;
			case "PASSWORD" : objControl.readOnly = blnEnable ; break ;
			case "TEXTAREA" : objControl.readOnly = blnEnable ; break ;
		}	
	}
	
	// Enable / Disable a Conrol
	function Disable(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].disabled = blnEnable;
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].disabled = blnEnable;
				}
				break;
			default :
				objControl.disabled = blnEnable ; break ;
		}	
	}
	
	// Read Only
	function readOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable; break;
			case "PASSWORD" : objControl.readOnly = blnEnable; break;
			case "TEXTAREA" : objControl.readOnly = blnEnable; break;
		}
	}
	
	// Set values to a control 
	function getText(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.options[objControl.selectedIndex].text;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].text;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	// Set values to a control 
	function getValue(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				if (intLength > 0)
				for (var i = 0 ; i < intLength ; i++){
					if (strReturn != ""){strReturn += ","; }
					if (objControl[i].checked){
						strReturn += objControl[i].value + "^";
					}
					strReturn += objControl[i].checked;
				}				
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				for (var i = 0 ; i < intLength ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.value ;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].value;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	// Set values to a control 
	function setField(strControlID, strControlValue){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		switch (strType){
			case "TEXT" : objControl.value = strControlValue ; break ;
			case "PASSWORD" : objControl.value = strControlValue ; break ;
			case "HIDDEN" : objControl.value = strControlValue ; break ;
			case "TEXTAREA" : objControl.value = strControlValue ; break ;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				if (strControlValue != true && strControlValue != false){
					var arrConValue = strControlValue.split(",");
					var intArrLength = arrConValue.length ;
					if (strControlValue == ""){
						for (var i = 0 ; i < intLength ; i++){
							objControl[i].checked = false;
						}				
					}
					
					for (var x = 0; x < intArrLength ; x++){
						for (var i = 0 ; i < intLength ; i++){
							if (objControl[i].value == arrConValue[x]){
								if (!objControl[i].disabled){
									objControl[i].checked = true;
								}
								break;
							}
						}
					}
				}else{
					for (var i = 0 ; i < intLength ; i++){
						if (!objControl[i].disabled){
							objControl[i].checked = strControlValue;
						}
						
					}
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				if (strControlValue == ""){
					for (var i = 0 ; i < intLengrh ; i++){
						objControl[i].checked = false;
					}				
				}
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].value == strControlValue){
						objControl[i].checked = true;
						break;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].value == strControlValue){
						objControl.options[i].selected = true;
						break;
					}
				}
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				var arrConValue = strControlValue.split(",");
				var intArrLength = arrConValue.length ;
				if (strControlValue == ""){
					for (var i = 0 ; i < intLengrh ; i++){
						objControl.options[i].selected = false;
					}				
				}
				for (var x = 0; x < intArrLength ; x++){
					for (var i = 0 ; i < intLengrh ; i++){
						if (objControl.options[i].value == arrConValue[x]){
							objControl.options[i].selected = true;
							break;
						}
					}				
				}
				break;
		}
	}
	
	
	function numberConvertToArabic(intNumber){
		var arrNumbers = new Array("&#1632;","&#1633;","&#1634;","&#1635;","&#1636;","&#1637;","&#1638;","&#1639;","&#1640;","&#1641;")
		intNumber = String(intNumber);
		var intLength = intNumber.length;
		var strReturn = "" ;
		for (var i = 0 ; i < intLength ; i++){
			if (intNumber.substr(i,1) != " "){
				if (!isNaN(intNumber.substr(i,1))){
					strReturn += arrNumbers[intNumber.substr(i,1)];
				}else{
					strReturn += intNumber.substr(i,1);
				}
			}else{
				strReturn += intNumber.substr(i,1);
			}
		}
		
		return strReturn;
	}
	
	function CurrencyFormat(amount, decimals){
		if ((String(amount) == "") || (amount == ".00")){amount = 0;}
		amount = addDecimals(amount, decimals);
		var Num = amount.toString().replace(/\$|\,/g,'');
		var dec = Num.indexOf(".");
		var intCents = ((dec > -1) ? "" + Num.substring(dec,Num.length) : ".00");
		Num = "" + parseInt(Num);
		var intValue = "";
		var intReturnValue = "";
		if (intCents.length == 2) intCents += "0";
		if (intCents.length == 1) intCents += "00";
		if (intCents == "") intCents += ".00";
		var count = 0;
		for (var k = Num.length-1; k >= 0; k--) {
			var oneChar = Num.charAt(k);
			if (count == 3){
				intValue += ",";
				intValue += oneChar;
				count = 1;
				continue;
			}else {
				intValue += oneChar;
				count ++;
			}
		}
		for (var k = intValue.length-1; k >= 0; k--) {
			var oneChar = intValue.charAt(k);
			intReturnValue += oneChar;
		}
		intReturnValue = intReturnValue + intCents;
		if (intReturnValue.indexOf("-,") != -1)
		{
			intReturnValue = intReturnValue.replace("-,","-");
		}
		return intReturnValue;
	}

	function addDecimals(strValue, intDecimals){
		var strReturn = "";
		var intDevide = "1"
		for (var i = 0 ; i < Number(intDecimals) ; i++){
			intDevide += "0";
		}
		strValue = String(Math.round(Number(strValue) * Number(intDevide))/Number(intDevide))
		var intIndex = strValue.indexOf(".")
		var strDecimals = "";
		if (intIndex !=  -1){
			var arrData = strValue.split(".");
			if (arrData.length > 1){
				strDecimals = arrData[1]
				for (var i = strDecimals.length ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
				strDecimals = strDecimals.substr(0, intDecimals);
			}else{
				for (var i = 0 ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
			}
			strReturn = arrData[0] + "." + strDecimals ;		
		}else{
			for (var i = 0 ; i < Number(intDecimals) ; i++){
				strDecimals += "0" ;
			}	
			strReturn = strValue + "." + strDecimals ;
		}
		return strReturn;
	}
	
	function raiseError(strErrNo){
		var strMsg = arrError[strErrNo];
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMsg;
	}
	
	function getFormattedMessage(strMsg){
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMsg;
	}
	
	/*
	*
	*/
	function checkInvalidChar(strValue, strMessage, strControlText){
		var strChkEmpty = FindChar(strValue)
		if (strChkEmpty != "0"){
			return buildError(strMessage, strChkEmpty[0], strControlText);
		}else{
			return "";
		}
	}
	
	function buildError(strMessage){
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMessage = strMessage.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMessage;
	}
	
	function FindChar(StringIn){
		// ------------- Check the standards characters
		var CharInArray = new Array("'","<",">","^","/",'"',"~");
		var strLength = 0;
		if (StringIn != null && StringIn != "") {
			strLength = StringIn.length;
		}
		for (var i=0;i<strLength;i++){
			for (var j=0;j<CharInArray.length;j++){
				if (StringIn.charAt(i)==CharInArray[j]){
					var CharOutArray=new Array();
					CharOutArray[0]=CharInArray[j];
					CharOutArray[1]=eval(i+1);
					return (CharOutArray);
				}
			}
		}
		return "0";
	}
	
	function showContextMenu(){
		var bln=enableContextMenu();
		return bln;
	}
	
	function enableContextMenu(){
		var win;
		var bln=false;
		if(window.location.href.indexOf('https')!=-1){
			win=window;
		}else{
			win=top;
		}
		
		if(win.request){
			bln=(win.request['debugFrontend']=='true')?true:false;
		}		
		return bln;
	}	
	
	function rightTrim(strValue){
		var objRegExp = /^([\w\W]*)(\b\s*)$/;
	
	      if(objRegExp.test(strValue)) {
	       //remove trailing a whitespace characters
	       strValue = strValue.replace(objRegExp, '$1');
	    }
	  	return strValue;
	}
	
	function leftTrim(strValue){
		var objRegExp = /^(\s*)(\b[\w\W]*)$/;
	
	  	if(objRegExp.test(strValue)) {
	    	//remove leading a whitespace characters
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	  	return strValue;
	}
	
	function trim(strValue){
		if (strValue == undefined)
			return;
		var objRegExp = /^(\s*)$/;
	
	    //check for all spaces
	    if(objRegExp.test(strValue)) {
	       strValue = strValue.replace(objRegExp, '');
	       if( strValue.length == 0)
	          return strValue;
	    }
	
	   //check for leading & trailing spaces
	   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	   if(objRegExp.test(strValue)) {
	       //remove leading and trailing whitespace characters
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	
	  	return strValue;
	}
	
	function debug(){
		var args=debug.arguments;
		var str='';
		for(var i=0;i<args.length;i++){
			str+=args[i]+'\n';
		}
		alert(str);
	}
	function replaceall(strValue, strRepValue, strNValue){
		var i = strValue.indexOf(strRepValue);
		while(i > -1){
			strValue = strValue.replace(strRepValue, strNValue);
			i = strValue.indexOf(strRepValue);
		}
		return strValue
	}
	
	function ClearCache(){
		try{
			window.clipboardData.clearData()
		}catch(e){}
	}
	
	function CloseChildWindow(){
		try{
			if ((top[0].objCW) && (!top[0].objCW.closed)){
				if 	(top[0].blnChildOpen){
					top[0].objCW.close();
				}
			}
		}catch (ex){}
	}
	
	
	function childWindowClosed(){
		try{
			opener.top[0].blnChildOpen = false
		}catch (ex){}
	}
	
	function backClick(){
		top[0].backBClick();
	}
	
	function converToTitleCase(strValue){
		var arrValues = strValue.split(" ");
		strValue = "";
		var strFC = ""
		var strOC = ""
		for (var i = 0; i < arrValues.length ; i++){
			strOC = "";
			if (strValue != ""){strValue += " ";}
			arrValues[i] = trim(arrValues[i]);
			arrValues[i] = arrValues[i].toLowerCase();
			strFC = arrValues[i].substr(0,1).toUpperCase();
			if (arrValues[i].length > 1){
				strOC = arrValues[i].substr(1,arrValues[i].length - 1).toLowerCase()
			}
			strValue += strFC + strOC;
		}
		return strValue
	}

	function buttonDisable(strID, blnValue, strType){
		var strClass = "Button";
		 
		if (!blnValue){
			switch (strType){
				case "D" : strClass += " ButtonDefault"; break
				case "M" : strClass += " ButtonMedium"; break
				case "L" : strClass += " ButtonLarge"; break
				case "A" : strClass += " btnAuxButton"; break
				case "C" : strClass += " btnCredit"; break
				case "B" : strClass += " ButtonCreditLarge"; break
			}
		}else{
			switch (strType){
				case "D" : strClass += " ButtonDefaultDisable"; break
				case "M" : strClass += " ButtonMediumDisable"; break
				case "L" : strClass += " ButtonLargeDisable"; break
				case "A" : strClass += " btnAuxButton AuxButtonDisable"; break
				case "C" : strClass += " btnCredit btnCDisable"; break
				case "B" : strClass += " ButtonCreditLarge btnCLargeDisable"; break
			}
		}
		setStyleClass(strID, strClass);
		Disable(strID, blnValue)
	}
	
	function arrayClone(arrSource, arrTarget){
		arrTarget.length = 0 ; 
		for (var i = 0 ; i < arrSource.length ; i++){
			arrTarget[i] = new Array();
			for (var m = 0 ; m < arrSource[i].length ; m++){
				arrTarget[i][m] = arrSource[i][m];
			}
		}
	}
	
	function setFocus(id){
		document.getElementById(id).focus();
	}
	
	function refreshSessionTimer(){
		try{
			top[0].resetTimeOut();
		}catch(e){}
	}
	
	/**
	 * @deprecated use ISAMath.getRoundedValue int isa.common.math.js
	 */
	function getRoundedValue(input, boundry, breakPoint) {
		
		var output  = 0;
		if(boundry == 0) return output;
		var divValue = Math.floor(input / boundry);			
		var modValue = input % boundry;			
		var difference = 0;
		if(modValue != 0) {
			if(breakPoint == 0) {
				divValue = divValue +1;
			}else if(breakPoint != boundry) {
				difference = input - (divValue * boundry);					
				if(difference == breakPoint || difference > breakPoint) {
					divValue = divValue +1;
				}
			}		
		}
		output = divValue * boundry;		
		return output;
	}
	
	/**
	 * for new currency round up rules
	 * @author Lasantha Pambagoda
	 * @deprecated use ISAMath.getFormattedRoundedValue in isa.common.math.js
	 */
	function getRoundedValueWithFormatting(isRoundVisible, input, boundry, breakPoint){

		var roundedValue = null;
		var formattedRoundedValue = null;
		
		if(isRoundVisible == true) {
			
			if(boundry != null && boundry != "" && breakPoint != null && breakPoint != ""){
				roundedValue = getRoundedValue(input, boundry, breakPoint);
				if(boundry < 1) {
					// get the decimal points to format
					var fomatPlaces = getNoOfDecimalPlaces(boundry);
					formattedRoundedValue = CurrencyFormat(roundedValue, fomatPlaces);
				} else {
					formattedRoundedValue = CurrencyFormat(roundedValue, 2);
				}
			// parameters are missing, apply the two decimal round up 
			} else {
				roundedValue = twoDecimalRoundUp(input);
				formattedRoundedValue = CurrencyFormat(roundedValue, 2);
			}
		// round visibility is false, apply the two decimal round up
		} else {
			roundedValue = twoDecimalRoundUp(input);
			formattedRoundedValue = CurrencyFormat(roundedValue, 2);
		}
		return formattedRoundedValue;
	}

	/**
	 * get the no of decimal places for a given number
	 * @author Lasantha Pambagoda
	 * @deprecated use ISAMath.getNoOfDecimalPlaces in isa.common.math.js
	 */
	function getNoOfDecimalPlaces(value){
		var noOfPlaces = 2;
		if(value != null && !isNaN(value)) {
			var strValue = value + "";
			var indexDecimal = strValue.indexOf(".");
			if(indexDecimal != -1) {
				try {
					noOfPlaces = (strValue.substring(indexDecimal + 1)).length;
				}catch (e) {
					// do nothing noOfPlaces remains 2
				}
			}
		}
		return noOfPlaces;
	}

	/**
	 * Default rounding rule, two decimal round up
	 * @author Lasantha Pambagoda
	 * @deprecated use ISAMath.defaultRounding in isa.common.math.js
	 */	
	function twoDecimalRoundUp(input) {

		if(input != null && !isNaN(input)) {
			var roundUpValue = Math.ceil(input * 100)/100;
		}
		return roundUpValue;
	}
	

function showWindow(height,width,url,winName){
	var y=(window.screen.height-height)/2;
	var x=(window.screen.width-width)/2;
	var props='toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width='+width+',height='+height+',resizable=no,top='+y+',left='+x;
	var win=window[winName];

	if (win) {
        if(!win.closed) {
            win.focus();
            return;
        }
    }

	window[winName]=window.open(url,winName,props);
}
function getMouseX(objEvent){
		var x = 0;
		if (browser.isIE) {
			x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				x = objEvent.clientX;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				x = objEvent.clientX;
			}else{
				x = objEvent.clientX + window.scrollX;
			}
		}	
		return x;
	}
	
	function getMouseY(objEvent){
		var y = 0;
		if (browser.isIE) {
			y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				y = objEvent.clientY;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				y = objEvent.clientY + document.documentElement.scrollTop;
			}else{
				y = objEvent.clientY + window.scrollY;
			}
		}	
		return y;
	}
	
refreshSessionTimer();
	
//function to search an array for the existence of a value
function isValueExist(arr,value){
	var x;
	for (x in arr){
		if(value==arr[x]){
			return true;
		}
	}
	return false;
}

