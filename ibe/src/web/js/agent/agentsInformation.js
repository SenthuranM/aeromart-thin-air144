function onLoadPage() {
	
	if(arrAgents!= null && arrAgents.length>0){		
	 buildAgentsDetailList();
	}
	else{	
		//var htmlNoData = "";
		//htmlNoData='<table width="100%"><tr class="recordtop fntWhite fntBold"><td align="center" >No Display Data</td></tr></table>';
		DivWrite("tblAgentsInfo", htmlNoData);
	}
	top[0].LoadingCompleted();
}

function pageBtnOnClick(){
/*	var carriers = "G9";
	var strPostData = "EN^" +
						"RA^" +
						getValue("selCountry") + "^" + // Country selected
						getValue("selStation") + "^" + // Station selected
						getValue("selAgentType");// Agent types to show
						//carriers + "^" + // Comma separated list of carriers to which selected station belongs to
										// set this to "" (empty string) if carriers to which it belongs to is not known
						
	setField("hdnParamData", strPostData);
	setField("hdnMode", "SEARCH");


	setField("hdnLanguage", "en"); // Language
	var url = getValue("hdnurl");
//	var url = "https://airarabia.isaaviations.com/ibe/public/"+ "showAgentsInfo.action"; // Action URL
//	if (carriers == "3O") {
//		url = "https://airarabiama.isaaviations.com/ibe/public/"+ "showAgentsInfo.action"";
//	}
	//top[0].LoadingProgress(); 
	getFieldByID("formRetrieveAgents").action = url ;

	getFieldByID("formRetrieveAgents").submit();*/
}

function onLoadMapClick(url){
	//var map = document.getElementById('google_Map');
	//if(url!=null && url!=""){
	//	map.src =  url;
	//}
	mapURL = url;
	Show_Popup(url);
}

function buildAgentsDetailList(){
	var strHTMLText = "";
	strHTMLText += table +tablebody+trow;
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="GridTable">';
	strHTMLText += '	<tr class="header fntWhite fntBold thinBorderB thinBorderL thinBorderR">';	
	strHTMLText += ' 		<td style="border-bottom:1 solid black;" align="left" width="28%" >'+agentName+'<\/td>' ;	
	strHTMLText += ' 		<td style="border-bottom:1 solid black;" align="left" width="12%" >'+telephone+'<\/td>' ;
	//strHTMLText += ' 		<td style="border-bottom:1 solid black;" align="left" width="10%" >'+fax+'<\/td>' ;
	strHTMLText += ' 		<td style="border-bottom:1 solid black;" align="left" width="15%" >'+email+'<\/td>' ;
	strHTMLText += ' 		<td style="border-bottom:1 solid black;" align="left" width="25%" >'+address+'<\/td>' ;
	if(agentType=='GSASO'){
		strHTMLText += ' 		<td style="border-bottom:1 solid black;" align="left" width="10%" >'+map+'<\/td>' ;
	}
		strHTMLText += '	 <\/tr>'
	strHTMLText += '	<tr>';
	
	for (var q = 0 ; q < arrAgents.length ; q++){
			var googleMap = arrAgents[q][12];
			 if(q%2==0){
				strHTMLText += '<tr class="recordd">';
			 }else{
				strHTMLText += '<tr class="recordl">';
			 }
			strHTMLText += '<td class="GridItems firstCol" align="left">' + arrAgents[q][1] + '<\/td>';			
			strHTMLText += '<td class="GridItems" align="left">' + arrAgents[q][13] + '<\/td>';
			//strHTMLText += '<td class="GridItems" align="left">' + arrAgents[q][9] + '<\/td>';
			strHTMLText += '<td class="GridItems" align="left">' + arrAgents[q][8] + '<\/td>';
			strHTMLText += '<td class="GridItems" align="left">' + arrAgents[q][3] +'<\/td>';
			strHTMLText	+= '	<td class="GridItems" align="left">';
			if(agentType=='GSASO'){
				if(googleMap!="" && googleMap.length>0 ){
					strHTMLText += '		<a href="javascript:onLoadMapClick(' +  "'" +  arrAgents[q][12] + "'" + ')">'+viewmap+'<\/a>';
				}
			}
			strHTMLText	+= '	<\/td>';
			strHTMLText += '<\/tr>';
			
	}
	strHTMLText += '<\/table>';	
	DivWrite("tblAgentsInfo", strHTMLText);
	
}

onLoadPage();