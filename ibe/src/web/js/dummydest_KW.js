<!--

var arrAirports 	= new Array();
var arrCurrency = new Array();
var arrCurrencyMA = new Array();
var arrError = new Array();

arrAirports[0] = new Array("KWI","Kuwait","KW");
/*arrAirports[1] = new Array("BAH","Baharain","KW");*/
arrAirports[1] = new Array("CAI","Cairo","KW");
/*arrAirports[3] = new Array("CCJ","Calicut International","KW");
arrAirports[4] = new Array("DEL","Delhi","KW");
arrAirports[5] = new Array("DMM","King fahd International","KW");
arrAirports[6] = new Array("DWC","Dubai World","KW");
arrAirports[7] = new Array("ELQ","prince nayef bin","KW");
arrAirports[8] = new Array("HMB","Sohag airport","KW");
arrAirports[9] = new Array("JED","Jeddah airport","KW");
arrAirports[10] = new Array("KHI","Karachchi airport","KW");
arrAirports[11] = new Array("MHD","Mashad airport","KW");
arrAirports[12] = new Array("NJF","najaf airport","KW");
arrAirports[13] = new Array("RUH","riyadh international","KW");
arrAirports[14] = new Array("SKT","Sialkot airport","KW");*/

//the last value (false/true) specify the default currency (true for default)
arrCurrency[0] = new Array('BHD','Bahrani Dinar',false);
arrCurrency[1] = new Array('EUR','Euro',false); 
arrCurrency[2] = new Array('INR','Indian Rupee',false); 
arrCurrency[3] = new Array('QAR','Qatari Riyal',false); 
arrCurrency[4] = new Array('OMR','Rial Omani',false); 
arrCurrency[5] = new Array('AED','UAE Dirham',true); 
arrCurrency[6] = new Array('USD','US Dollar',false);
arrCurrency[7] = new Array('UAH','Ukrainian Hryvnia',false);

//the following array should be changed
arrCurrencyMA[0] = new Array('EGP','Egyptiyan Pound',false);
arrCurrencyMA[1] = new Array('EUR','Euro',false); 
arrCurrencyMA[2] = new Array('GBP','British Pound',false); 
arrCurrencyMA[3] = new Array('MAD','Moroccan Dirham',true); 
arrCurrencyMA[4] = new Array('TND','Tunisia, Dinars',false); 
arrCurrencyMA[5] = new Array('USD','US Dollar',false);

arrError['ERR009'] = 'Your Departure Date cannot be before the current date. Please check and enter again.';arrError['ERR008'] = 'Please select your Returning Date.';arrError['ERR002'] = 'Please select your destination.';arrError['ERR006'] = 'Please select a Currency.';arrError['ERR004'] = 'Your Returning Date cannot be before your Departing Date. Please check your dates and enter again. ';arrError['ERR001'] = 'Please select the city you will be departing from.';arrError['ERR005'] = 'Please enter a valid Date.';arrError['ERR007'] = 'Please select your Departing Date.';arrError['ERR003'] = 'The From & To locations cannot be the same. Please check and enter again';


var strDash = "----------------------"
var strSeleDDay = "";
var strSeleRDay = "";
var intEndMonth  = "";
var intEndYear = "";
var objCW ;
var strRDate = "";
var strDDate = "";
var blnReturn = false;
var strLanguage = "EN";

// Default values
var intMaxAdults = 9
var strFromHD = 'Where are you flying from'
var strToHD = 'Where are you flying to'
var strHD = '-- select --';//added by Haider 18-08-08
var strDDDefValue = 'Please Select'
var intRDaysDef = 0;
var strDefDayRetu = "0";
var strBaseAirport1 = (strBaseAirport1==undefined)?"SHJ":strBaseAirport1;
var G9URL = "http://reservations.airarabia.com/ibe/public/";
var strBaseAirport2 = (strBaseAirport2==undefined)?"CMN":strBaseAirport2; //this should be replaced according to the destination code
var MAURL = "http://reservationsma.airarabia.com/ibe/public/"; //new url should be here
var shjHub = (shjHub==undefined)?"G9":shjHub;
var maHub = (maHub==undefined)?"MA":maHub;//this should replaced according to new hub code
var egHub = (egHub==undefined)?"E5":egHub;
var strFrom = "";
var strTo = "";
var strCurr = "AED";
var strSelCurr = "AED";
var strDDays = "0";
var strRDays = "0";
var intDays = "3";
var intRDays = "3";
var strDefDayDept = "0";
var firstSelected = null;
var firstSelected2 = null;

var dtC = new Date();
var dtCM = dtC.getMonth() + 1;
var dtCD = dtC.getDate();
if (dtCM < 10){dtCM = "0" + dtCM}
if (dtCD < 10){dtCD = "0" + dtCD}

    var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); //"25/08/2005";
    var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));


    function faqClick(){
        var intHeight = (window.screen.height - 200);
        var intWidth = 795;
        var intX = ((window.screen.height - intHeight) / 2);
        var intY = ((window.screen.width - intWidth) / 2);
        var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
        if ((objCW)  && (!objCW.closed)){objCW.close();}
        objCW = window.open(strFAQLink,'ToolChild',strProp)

        }


    function isHubMatch(destHub, hubCodes){
        var n=hubCodes.length;
        for(i=0;i<n;i++){
        if(destHub.match(hubCodes[i])!=null){
        return true;
        }
        }
        return false;
        }
    function buildDestList(obj, hub, dText){
        var intCount = arrAirports.length ;

        obj.length =  obj.length + 1
        obj.options[obj.length - 1].text = dText;
        obj.options[obj.length - 1].value = "";

        obj.length =  obj.length + 1
        obj.options[obj.length - 1].text = strDash;
        obj.options[obj.length - 1].value = "";
        var hubCodes = hub.split(",");
        for (var i = 0 ; i < intCount ; i++)		{
        if (arrAirports[i][0] == strBaseAirport1 || arrAirports[i][0] == strBaseAirport2){

        if(isHubMatch(arrAirports[i][2], hubCodes) == true){
        obj.length =  obj.length + 1
        obj.options[obj.length - 1].text = arrAirports[i][1];
        obj.options[obj.length - 1].value = i;

        obj.length =  obj.length + 1
        obj.options[obj.length - 1].text = strDash;
        obj.options[obj.length - 1].value = "";
        }
        }
        }


        for (var i = 0 ; i < intCount ; i++)		{
        if (arrAirports[i][0] != strBaseAirport1 && arrAirports[i][0] != strBaseAirport2){
        if(isHubMatch(arrAirports[i][2], hubCodes) == true){
        obj.length =  obj.length + 1
        obj.options[obj.length - 1].text = arrAirports[i][1];
        obj.options[obj.length - 1].value = i;
        }
        }
        }
        }

    function buildCurrencyList(arr){
        var intCount = arr.length;
        var obj = document.getElementById('selCurrency');
        obj.length = 0;
        for (var i = 0 ; i < intCount ; i++){
        obj.length =  obj.length + 1
        obj.options[obj.length - 1].text = arr[i][1];
        obj.options[obj.length - 1].value = arr[i][0];
        obj.options[obj.length - 1].selected = arr[i][2];
        }
        }
    function buildDropDownsSch(){
        //var objDF 	 = getFieldByID("selFromLoc");
        //var objDT 	 = getFieldByID("selToLoc");
        //added by Haider 18-08-08
        var objDF2 	 = getFieldByID("selFromLoc2");
        var objDT2 	 = getFieldByID("selToLoc2");
        var hubs = shjHub+","+maHub;
        //buildDestList(objDF, hubs, strFromHD);
        //buildDestList(objDT, hubs, strToHD);
        if(objDF2!=null && objDT2!=null){
        buildDestList(objDF2, hubs,strHD);
        buildDestList(objDT2, hubs,strHD);
        }
        //buildYearDropDown("selYrDept",1);
        //buildYearDropDown("selYrRetu",1);
        buildYearDropDown("selYrDept2",1);//added by Haider 18-08-08
        //FillCalenderDay("selDtDept", getFieldByID("selYrDept").options[1].value);

        defaultSetYearDay();


        //	document.getElementById('selCurrency').disabled = true;
        }








    // schedule page
    function selToLoc2OnChange(){
        var objDF2 	 = getFieldByID("selFromLoc2");
        var objDT2 = getFieldByID("selToLoc2");
        if (getText("selToLoc2") == strDash){
        getFieldByID("selToLoc2").options[0].selected = true;
        if(objDF2.value == ""){
        objDF2.length = 0;
        objDT2.length = 0;
        firstSelected2 = null;
        var hubs = shjHub+","+maHub;
        buildDestList(objDF2, hubs, strHD);
        buildDestList(objD2T, hubs, strHD);
        }
        return ;
        }
        if(firstSelected2 == null)
        firstSelected2 = objDT2;
        var selValueDF2 = getValue("selFromLoc2");
        var selValueDT2 = getValue("selToLoc2");
        if(selValueDF2 != null && selValueDF2 != ""){
        var hubDT2 = arrAirports[selValueDT2][2];
        var hubDF2 = "NULL";
        if(selValueDF2 != "")
        hubDF2 = arrAirports[selValueDF2][2];
        if(firstSelected2 == objDT2){
        objDF2.length = 0;
        var hubs = shjHub+","+maHub+","+egHub;
        buildDestList(objDF2, hubs, strHD);
        }
        if(hubDF2.match(hubDT2) != null || hubDT2.match(hubDF2) != null){
        setField("selFromLoc2", selValueDF2);
        }
        }
        }

    function selFromLoc2OnChange(){
        var objDT2	 = getFieldByID("selToLoc2");
        var objDF2 = getFieldByID("selFromLoc2");
        if (getText("selFromLoc2") == strDash){
        getFieldByID("selFromLoc2").options[0].selected = true;
        if(objDT2.value == ""){
        firstSelected2 = null;
        var hubs = shjHub+","+maHub;
        objDT2.length = 0;
        objDF2.length = 0;
        buildDestList(objDF2, hubs, strHD);
        buildDestList(objDT2, hubs, strHD);
        }
        return;
        }
        if(firstSelected2 == null)
        firstSelected2 = objDF2;
        var selValueDF2 = getValue("selFromLoc2");
        var selValueDT2 = getValue("selToLoc2");
        if(selValueDF2 != null && selValueDF2 != ""){
        var hubDF2 = arrAirports[selValueDF2][2];
        var hubDT2="NULL";
        if(selValueDT2 != "")
        hubDT2 = arrAirports[selValueDT2][2];
        if(firstSelected2 == objDF2){
        objDT2.length = 0;
        var hubs = shjHub+","+maHub+","+egHub;
        buildDestList(objDT2, hubs, strHD);
        }
        if(hubDF2.match(hubDT2) != null || hubDT2.match(hubDF2) != null){
        setField("selToLoc2", selValueDT2);
        }
        }

        }


    function setDate(strDate, strID){
        var arrStrValue = strDate.split("/");
        switch (strID){
        case "0" :
        setField("selYrDept", arrStrValue[1] + "/" + arrStrValue[2]);
        selYrDeptOnChange();
        setField("selDtDept", arrStrValue[0]);
        break ;
        case "1" :
        setField("selYrRetu", arrStrValue[1] + "/" + arrStrValue[2]);
        selYrRetuOnChange();
        setField("selDtRetu", arrStrValue[0]);
        break ;
        }
        }

    function chkReturnTrip_click(){
        if (getFieldByID("chkReturnTrip").checked){
        setField("selRVariance", intRDaysDef);
        Disable("selDtRetu", false);
        Disable("selYrRetu", false);
        Disable("selRVariance", false);
        }else{
        setField("selRVariance", "0");
        setField("selDtRetu", "");
        setField("selYrRetu", "");
        Disable("selDtRetu", true);
        Disable("selYrRetu", true);
        Disable("selRVariance", true);
        defaultSetYearDay();
        }
        }

    function LoadCalendar(strID, objEvent){
        objCal1.ID = strID;
        objCal1.top = 0 ;
        objCal1.left = 0 ;
        objCal1.onClick = "setDate";
        objCal1.currentDate = strSysDate;
        switch (strID){
        case 0 :
        if (getValue("selDtDept") != "" && getValue("selYrDept")){
        objCal1.currentDate = getValue("selDtDept") + "/" + getValue("selYrDept");
        }
        break;
        case 1 :
        if (getFieldByID("chkReturnTrip").checked){
        if (getValue("selDtRetu") != "" && getValue("selYrRetu")){
        objCal1.currentDate = getValue("selDtRetu") + "/" + getValue("selYrRetu");
        }
        }else{
        return;
        }
        break;
        }
        objCal1.showCalendar(objEvent);
        }

    // --------------------- Maximum Passengers





    function defaultDataLoad(){
        var dtDepDate = addDays(dtSysDate, Number(strDefDayDept));
        strDDate = dateChk(dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear());
        strSeleDDay = dtDepDate.getDate();

        var dtRetuDate = "";
        strRDate = "";
        strSeleRDay = "  /  /    ";
        if (String(strDefDayRetu) != ""){
        var dtRetuDate = addDays(dtSysDate, Number(strDefDayRetu));
        strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
        strSeleRDay = dtRetuDate.getDate();
        setField("chkReturnTrip", true);
        }

        setField("selDVariance", strDDays);
        setField("selRVariance", strRDays);
        chkReturnTrip_click();
        }

    function dateOnBlur(strID, objC){
        var blnDateEntered = false;
        if (objC.value != ""){blnDateEntered = true;}
        dateChk(strID);
        if (objC.value == "" && blnDateEntered){
        alert(arrError["ERR005"]);
        }
        }

    function cacheData(){
        //setField("selFromLoc", strFrom);
        //setField("selToLoc", strTo);

        var arrDtDept = strDDate.split("/");
        setField("selYrDept", arrDtDept[1] + "/" + arrDtDept[2]);
        selYrDeptOnChange();
        setField("selDtDept", arrDtDept[0]);

        setField("selYrRetu", arrDtDept[1] + "/" + arrDtDept[2]);
        selYrRetuOnChange();
        setField("selDtRetu", arrDtDept[0]);

        if (getValue("selDtRetu") == ""){
        defaultSetYearDay();
        }

        setField("selDVariance", strDDays);
        setField("selRVariance", strRDays);
        //setField("selCurrency", strSelCurr);

        getFieldByID("selFromLoc").focus();

        }
    var arrAdults = new Array();

    function setPaxArrays(){
        var arrChild  = new Array();
        var adCount = getValue("selAdults");
        for (var i = 0 ; i <= (intMaxAdults - parseInt(adCount,10)) ; i++){
        arrChild[i] = new Array();
        arrChild[i][0] = i;
        }
        objLB = new listBox();
        objLB.dataArray = arrChild;
        objLB.textIndex = "0";
        objLB.id = "selChild";
        objLB.fillListBox();
        }
    // ------------------------
    function flightSchOnLoad(){
        buildDropDownsSch();
        for (var i = 1 ; i <= intMaxAdults ; i++){
        arrAdults[i - 1] = new Array();
        arrAdults[i - 1][0] = i;
        }
        var objLB = new listBox();
        objLB.dataArray = arrAdults;
        objLB.textIndex = "0";
        objLB.id = "selAdults";
        objLB.fillListBox();
        setPaxArrays();
        selAdults_onChange();

        // ------------------------
        var arrDays = new Array();
        for (var i = 0 ; i <= intDays ; i++){
        arrDays[i] = new Array();
        arrDays[i][0] = i;
        arrDays[i][1] =  "+/- " + i ;
        }

        var objLB2 = new listBox();
        objLB2.dataArray = arrDays;
        objLB2.id = "selDVariance";
        objLB2.fillListBox();

        arrDays.length = 0
        for (var i = 0 ; i <= intRDays ; i++){
        arrDays[i] = new Array();
        arrDays[i][0] = i;
        arrDays[i][1] =  "+/- " + i ;
        }
        objLB2 = new listBox();
        objLB2.dataArray = arrDays;
        objLB2.id = "selRVariance";
        objLB2.fillListBox();

        Disable("selDtRetu", true);
        Disable("selYrRetu", true);
        Disable("selRVariance", true);

        defaultDataLoad()
        cacheData();

        }
    flightSchOnLoad();
    function myOnload(){
        setField("selFromLoc", "");
        setField("selToLoc", "");
        }


    // ---------------- Calendar
    var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.borderColor = "#FE0000";
	objCal1.headerTextColor = "white";
	objCal1.noYears = "1";
    objCal1.blnArabic = false;
    objCal1.disableUpto = strSysDate;
    objCal1.disableFrom = getDaysInMonth(intEndMonth, intEndYear) + "/" + intEndMonth + "/" + intEndYear;
    objCal1.buildCalendar();
    // Special Flight Setting

    if(document.getElementById('specialFltFromDate') !=null){
		setField("specialFltFromDate", ""); //starting date of format mm/dd/yyyy
		setField("specialFltToDate", ""); //Ending date	of format mm/dd/yyyy	
		setField("specialFltNo", ""); //Flights number: it should be comma seperated and without G9 (for ex 2107,2121)
		setField("specialFltDst", ""); //3 letters Flight destination (for ex. JED)  	 
	}

//-->