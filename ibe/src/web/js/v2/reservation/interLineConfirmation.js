;(function($){
	$.label = function(id){
		var query='label[name="'+id+'"]';
		return $(query);
	}
	
	$.tr = function(id){
		var query='tr[name="'+id+'"]';
		return $(query);
	}
	$.fn.disableNav = function(){
		this.each(function(index,item){
			$(item).prop('disabled',true).addClass("ButtonDefaultDisable");
		});
		return this;
	}

	$.fn.enableNav = function(){
		this.each(function(index,item){
			$(item).prop('disabled',false).removeClass("ButtonDefaultDisable");
		});
		return this;
	}
})(jQuery);



function UI_Confirm(){}

$(document).ready(function(){
	UI_Confirm.ready();
});

;(function(UI_Confirm){
	UI_Confirm.strPNR = "";
	UI_Confirm.strLanguage = "";
	UI_Confirm.strEmailLanguage = "";
	UI_Confirm.strMsgEmailSent= "";
	UI_Confirm.blnRegUser = false;
	UI_Confirm.contactEmailAddress = "-";
	
	UI_Confirm.homeUrl = "";
	UI_Confirm.nonSecurePath = "";
	UI_Confirm.homeUrl = "";
	UI_Confirm.isGroupPNR = false;
	UI_Confirm.segmentNameMap ={};
	
	// Retry - Reloading Reservation
	UI_Confirm.isExistRes = false;
	UI_Confirm.maxReTry = 4;
	UI_Confirm.timeGap = 1000*60; // minute
	UI_Confirm.timer = null;
	UI_Confirm.isProcessing =  false;
	UI_Confirm.count = 1;
	
	UI_Confirm.adultCount = 1;
	UI_Confirm.childCount = 1;
	UI_Confirm.infantCount = 1;
	
	UI_Confirm.fadultCount = 0;
	UI_Confirm.fchildCount = 0;
	UI_Confirm.finfantCount = 0;
	
	UI_Confirm.allowIndivPrint = false;
	UI_Confirm.isCarLinkDynamic = false;
	UI_Confirm.isHotelLinkLoadWithinFrame = false;	
	UI_Confirm.showSocialFeeds = false;

	// Tracking data
	UI_Confirm.tempResponse =  null;
	UI_Confirm.flightDepDetails =  null;
	UI_Confirm.flightRetDetails =  null;
	
	//Tracking Cirteo pixel information
	UI_Confirm.selectedFlight =  null;
	UI_Confirm.fareQuote =  null;
	UI_Confirm.totalPriceBaseCurrency =  null;
	UI_Confirm.selectedCurrency = "";
	
	//Social Stream Data
	UI_Confirm.pageLabel = {};
	
	
	UI_Confirm.ready = function(){		   		   
			$("#btnFinish").click(function(){UI_Confirm.finishClick();});
			$("#btnPrint").click(function(){UI_Confirm.printClick();});
			$("#lnkHome").click(function(){UI_Confirm.finishClick();});
			$("#btnEmailMe").click(function(){UI_Confirm.EmailMeClick();});
			$('#btnFinish').disableNav();
			$('#btnPrint').disableNav();
			$('#lnkHome').disableNav();
			$('#btnEmailMe').disableNav();
			
			UI_commonSystem.stepsSettings("last","interLineConfurmtion");
			UI_commonSystem.setCommonButtonSettings();		
			UI_Confirm.loadConfimationPageData();
	}
	
	UI_Confirm.loadConfimationPageData = function() {		
		UI_Confirm.isProcessing = true;
		$("#submitForm").attr('action', 'interlineItinerary.action?rad=' + UI_commonSystem.getRandomNumber());			
		$("#submitForm").ajaxSubmit({dataType: 'json',			
				success: UI_Confirm.loadConfirmationPage, beforeSubmit:UI_commonSystem.loadingProgress, error:errorProcessing});			
		return false;
	}
	
	UI_Confirm.EmailMeClick = function() {	
		$("#btnEmailMe").attr("disabled","disabled").addClass("ButtonLargeDisable");
		var emailLanguage = $.trim(UI_Confirm.strEmailLanguage) == "" ? UI_Confirm.strLanguage : UI_Confirm.strEmailLanguage;
		var  data = new Object();
		data["hdnPNRNo"] = UI_Confirm.strPNR;
		data["groupPNR"] = UI_Confirm.isGroupPNR;
		data["itineraryLanguage"] = emailLanguage;
		$("#submitForm").attr('action', 'sendEmail.action?rad=' + UI_commonSystem.getRandomNumber());	
		$("#submitForm").ajaxSubmit({dataType: 'json', data:data, success: UI_Confirm.emailSucess});		
	}
	
	UI_Confirm.emailSucess = function(response) {
		if(response != null && response.success == true) {
			alert(UI_commonSystem.alertText({msg:UI_Confirm.strMsgEmailSent,language:UI_Confirm.strLanguage}));
		} else  {
			alert("Email sending fail.");
		}
	}
	
	UI_Confirm.finishClick = function() {
		SYS_IBECommonParam.homeClick();		
	}
	
	UI_Confirm.printClick = function() {
		var selectedPax = UI_Confirm.adultCount ; 
		var intHeigh = 630;
		var intWidth = 900
		var intTop 	= (window.screen.height - intHeigh) / 2;
		var intLeft = (window.screen.width -  intWidth) / 2;	
		var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=' + intWidth + ',height=' + intHeigh + ',resizable=no,top='+ intTop +',left=' + intLeft
		var printLanguage = $.trim(UI_Confirm.strEmailLanguage) == "" ? UI_Confirm.strLanguage : UI_Confirm.strEmailLanguage;	
		var formHtml =  "<form id='dynamicFormPrint' method='post'>" +
				    	"<input type='hidden' name='hdnPNRNo' value='"+ UI_Confirm.strPNR +"'/>" +
				    	"<input type='hidden' name='groupPNR' value='"+ UI_Confirm.isGroupPNR +"'/>" +
				    	"<input type='hidden' name='itineraryLanguage' value='"+ printLanguage +"'/>" +				    	
				    	"<input type='hidden' name='selectedPax' value='"+ selectedPax +"'/>" +
				    	"<input type='hidden' name='viewMode' value='confirmRes'/>" +
				    	"</form>";
		
		
		/*
		 ------------------------------------------------------------------
		  var strUrl = "printItinerary!printIndividualPaxItinerary.action"
		+ "?hdnPNRNo=" + jsonBkgInfo.PNR
		+ "&groupPNR=" + isGroupPNR
		+ "&includePaxFinancials=" + includePaxFinancials
		+ "&includePaymentDetails=" + includePaymentDetails
		+ "&includeTicketCharges=" + includeTicketCharges
		+ "&itineraryLanguage=" + itineraryLanguage
		+ "&selectedPax=" + selectedPax
		+ "&includeTermsAndConditions=" +includeTCDetails
		+ "&operationType='MODIFY_BOOKING'";		 
		
		*/
	
		$("body").append(formHtml);	
		if (UI_Confirm.allowIndivPrint == true)
			$("#dynamicFormPrint").attr("action", "printItinerary!printIndividualPaxItinerary.action");  
		else
			$("#dynamicFormPrint").attr("action", "printItinerary.action");
			
		$('#dynamicFormPrint').submit(function() {		
			objCW = window.open('', 'myWindow', strProp);
				this.target = 'myWindow';
		});
		$('#dynamicFormPrint').submit();
	}
	
	
	function loadErrorPage(response){
		var  message = "";
		if (response != null && response != "") {
			message = response.messageTxt;
		}
		window.location.href="showLoadPage!loadError.action?messageTxt="+message + "&rad=" + UI_commonSystem.getRandomNumber();
	}
	
	
	UI_Confirm.processReTry = function() {
			
		if (UI_Confirm.isExistRes == false && UI_Confirm.isProcessing == false &&  UI_Confirm.count <  UI_Confirm.maxReTry ) {
			UI_Confirm.count++;
			UI_Confirm.loadConfimationPageData();
		}
		
	}
	
	function errorProcessing(response) {
		UI_Confirm.isProcessing = false;
		$("#divLoadBg").hide();		
		$("#processingRes").show();
		
		UI_Confirm.timer = setTimeout('UI_Confirm.processReTry()', UI_Confirm.timeGap);
		
		if (UI_Confirm.isExistRes == true) {
			clearTimeout(UI_Confirm.timer);
		} else	if (UI_Confirm.count >= UI_Confirm.maxReTry) {
			// load Error Page
			clearTimeout(UI_Confirm.timer);
			loadErrorPage(response);
		}		
		
	}
	
	function buildSegmentNameMap(segments){
		var map = {};
		for(var i = 0 ; i < segments.length; i++){
			map[segments[i].segmentShortCode]= segments[i].segmentCode;
			if (i==0){
				var segArrOutboundStart= segments[i].segmentCode.split("/");
		    }
			if (i == (segments.length-1)){
				
				if( i==0){
					var segArrOutboundEnd= segments[i].segmentCode.split("/");
				} else{
					var segArrOutboundEnd= segments[i].segmentCode.split("/");
				}
				
		    }
		}
		UI_Confirm.segmentNameMap = map;
	}
	function getSegmentName(code){
		var name = UI_Confirm.segmentNameMap[code];
		return (name==null)?code:name;
	}
	
	UI_Confirm.loadConfirmationPage = function(response){
		$('#btnFinish').enableNav();		
		$('#lnkHome').enableNav();
				
		if(response != null && response.success == true) {
			if(response.contactInfo.emgnFirstName == "" && response.contactInfo.emgnLastName == ""
				&& response.contactInfo.emgnPhoneNo == ""){
				$("#trDispEmgnContact").hide();
			}
			UI_Confirm.tempResponse = response;
			
			
			UI_Confirm.displayPagePannels();
			// Ignore code structure and process very important data first
			$("#divLoadBg").show();			
			UI_Confirm.isGroupPNR = response.groupPNR;
			UI_Confirm.strPNR = response.bookingTO.PNR;
			// Set PNR server side using c:out
			$("#lblPnrValue").text(UI_Confirm.strPNR);
			$("#lblPnrDateValue").append(response.bookingTO.bookingDate);
			var t = response.bookingTO.bookingDate.split("-");
			var strDt = t[0]+"/"+t[1]+"/"+t[2];
			$("#lblPnrDateValueHid").append(strDt)
			$("#divRefundMessage").toggle(response.successfulRefund);
			
			if(response.itineraryEmailGenerated){
				$("#lblMsgBookingSuccess").hide();
				$("#lblMsgSentMail").show();				
			}else{
				$("#lblMsgSentMail").hide();
				$("#lblMsgBookingSuccess").show();				
			}
			
			UI_Confirm.strEmailLanguage = response.emailLanguage;
			UI_Confirm.strLanguage = response.language;
			UI_Confirm.strMsgEmailSent = response.jsonLabel["msgEmailSent"];
			UI_Confirm.isCarLinkDynamic = response.carLinkDynamic;
			UI_Confirm.isHotelLinkLoadWithinFrame = response.hotelLinkLoadWithinFrame;		
			
			strPolicy = response.jsonLabel["lblPolicyHD"];
			$("#divLoadBg, #divPromos").populateLanguage({messageList:response.jsonLabel});	
			UI_Confirm.pageLabel = response.jsonLabel;
			//AARESAA-12872 - Did this because label does not render html tags
			if(response.displayCreditCardProofLable == false){			
				$("#trCreditcardProof").hide();
			} else {
				$("#trCreditcardProof").show();
				$("#lblCreditcardProof").html(response.jsonLabel["lblCreditcardProof"]);				
			}
			
			//var flightSegments = $.airutil.sort.quickSort( response.flightSegments, fltSegmentComparator);
			var flightSegments = $.airutil.sort.quickSort(response.flightDepDetails.concat(response.flightRetDetails), fltSegmentComparator);
			UI_Confirm.flightDepDetails = $.airutil.sort.quickSort( response.flightDepDetails, fltSegmentComparator);
			buildSegmentNameMap(flightSegments);
			UI_Confirm.selectedFlight = flightSegments;
			
			var colAdultCollection = summarizeAnciData(response.colAdultCollection);
			var colChildCollection = summarizeAnciData(response.colChildCollection);
			var colInfantCollection = summarizeAnciData(response.colInfantCollection);
			
			if(response.saveCookieEnable){
				UI_Confirm.SearchCookie();

				var UserName = "saveOprtnDataCookie_";
				var today=new Date();
				var noOfCookieExpiryDaysExpiryDays = response.expiryDays;
				var expiry=new Date(today.getTime()+ noOfCookieExpiryDaysExpiryDays*24*3600*1000);	//UI_Confirm.noOfCookieExpiryDays
				document.cookie = UserName + "=" + JSON.stringify(response.reservationCookieDetails)+ "; path=/; expires=" + expiry.toGMTString();
			}
			
			var strMsgAdult = response.jsonLabel["lblAdultHD"];
			var strMsgChild = response.jsonLabel["lblChildHD"];
			var strMsgInfant = response.jsonLabel["lblInfantHD"];		
			var strMsgOneWayTrip = response.jsonLabel["lblOneWayTrip"];
			var strMsgRoundTrip = response.jsonLabel["lblRoundTrip"];
			var strMsgProgressBar = response.jsonLabel["lblProgressBar"];			
			
			$('body').append('<div id="divSessContainer"></div>');
			UI_commonSystem.initSessionTimeout('divSessContainer',response.timeoutDTO,function(){});
			// load external files
			loadCssFile(UI_Confirm.strLanguage);	
			$("#lblProgressBar").text(strMsgProgressBar);			
			
			buildInsuranceData(response.bookingTO);
			// Set links
			//TODO Remove
			//TODO merge with buildInsuranceData

			if(response.insurances!=null && response.insurances.length > 0 ){
				var policyCode = UI_Confirm.createInsurancePolicyCode(response.insurances);
				if(policyCode!=null && policyCode!= ''){
					$('#lblPolicyValue, #lblPolicyValue1').text(policyCode);
					if(strPolicy!=null)
						$('#lblPolicy').text(strPolicy);
					$('#spnPolicy').show();
					$('#trInsuranceSellFailed').hide();
				}else{
					$('#msgInsuranceSellFailed').text(response.jsonLabel["msgInsuranceSellFailed"]);
					$('#trInsuranceSellFailed').show();
					$('#spnPolicy').hide();
				}
				if (response.jsonLabel["showINSInPanel"] != undefined && 
						response.jsonLabel["showINSInPanel"].toUpperCase() == "TRUE"){
					$("#insPanel").show();
					$('#spnPolicy').hide();
				}else{
					$("#insPanel").hide();
					$('#spnPolicy').show();
				}
			}else{
				$('#trInsuranceSellFailed').hide();
				$('#lblPolicyValue, #lblPolicyValue1').text('');
				$('#spnPolicy').hide();
				$("#insPanel").hide();
			}
			
			if($.trim(response.hotelLink) != "" || $.trim(response.carLink) != "") {
			if ($.trim(response.hotelLink) != "") {
				if (UI_Confirm.isHotelLinkLoadWithinFrame == true) {
					$("#frmhotel").attr("src", response.hotelLink);
					$("#linkBookHotel").remove();
				} else {
					$("#linkBookHotel").attr("href", response.hotelLink);
					$("#divHotel").remove();					
				}
			} else {				
				$("#linkBookHotel").remove();
				$("#divHotel").remove();
			}
			
			if ($.trim(response.carLink) != "") {
				if (UI_Confirm.isCarLinkDynamic == true) {
					$("#frmrentcar").attr("src", response.carLink);
					$("#linkRentCar").remove();					
				} else {
					$("#linkRentCar").attr("href", response.carLink);
					$("#divRentCar").remove();
				}
			} else {
				$("#linkRentCar").remove();
				$("#divRentCar").remove();
			}			
			
				
				$("#rowBookHotelOrCarSeperator").show();
				$("#rowBookHotelOrCar").show();
			} 
			
			loadContactInfo(response.contactInfo);
			HideEmailButton(response.contactInfo.email);			

			UI_Confirm.fadultCount = response.adultCount;
			UI_Confirm.fchildCount = response.childCount;
			UI_Confirm.finfantCount = response.infantCount;
			
			$("#flightTemplate").iterateTemplete({templeteName:"flightTemplate", data:flightSegments, dtoName:"flightSegments"});
			updateFlightDetails(UI_Confirm.flightDepDetails, response.flightRetDetails, response.jsonLabel);
			if (UI_Confirm.flightDepDetails.length > 0) {
				$("#departueFlight").iterateTemplete({templeteName:"departueFlight", data:UI_Confirm.flightDepDetails, dtoName:"flightSegments"});
			} else {
				$("#depFlightPanel").hide();
			}		
			
			if(response.flexiInfo != null && response.flexiInfo.length > 0){
				$("#tblFlexiDetails").show();
				$("#flexiInfo").nextAll().remove();
				$("#flexiInfo").iterateTemplete({templeteName:"flexiInfo", data:response.flexiInfo, dtoName:"flexiInfo"});
			}		
			UI_Confirm.fareQuote = response.fareQuote;
			$("#airFareCurrCode").append(response.fareQuote.inBaseCurr.currency);
			$("#airFare").append(response.fareQuote.inBaseCurr.totalFare);
			$("#chargesCurrCode").append(response.fareQuote.inBaseCurr.currency);
			$("#charges").append(response.fareQuote.inBaseCurr.totalTaxSurcharge);
			$("#totalCurrCode").append(response.fareQuote.inBaseCurr.currency);
			$("#total").append(response.fareQuote.inBaseCurr.totalPrice);	
			$("#valTotal").text(response.fareQuote.inBaseCurr.totalPrice);
			UI_Confirm.totalPriceBaseCurrency = response.fareQuote.inBaseCurr.totalPrice;
			$("#addServiceDiv").hide();
			if(response.flightRetDetails != null && response.flightRetDetails.length > 0){
				UI_Confirm.flightRetDetails = $.airutil.sort.quickSort( response.flightRetDetails, fltSegmentComparator);
				$("#trReturnFlt").show();
				$("#trReturnFltGap").show();
				$("#returnFlight").iterateTemplete({templeteName:"returnFlight", data:UI_Confirm.flightRetDetails, dtoName:"flightSegments"});				
				$("#lblReturn").text(strMsgRoundTrip);
				$("#lblRetFlight").text(strMsgRoundTrip);				
			}else {
				$("#trReturnFlt").hide();
				$("#trReturnFltGap").hide();
				$("#lblReturn").text(strMsgOneWayTrip);
				$("#lblRetFlight").text(strMsgOneWayTrip);
			}
			
			if(colAdultCollection.length > 0) {
				$("#adultCount").text(colAdultCollection.length+ " " + strMsgAdult)
			}
			if(colChildCollection.length > 0) {
				$("#childCount").text(", "+colChildCollection.length +" " + strMsgChild)
			}
			if(colInfantCollection.length > 0) {
				$("#infantCount").text(", "+colInfantCollection.length+" " + strMsgInfant)
			}
			//show the pax details			
			if(colAdultCollection.length > 0){
				$("#adultGrid").iterateTemplete({templeteName:"adultGrid", data:colAdultCollection, dtoName:"colAdultCollection"});
			}else {
				$("#tradultfGrid").hide();
			}
			
			if(colChildCollection.length > 0){
				$("#childGrid").iterateTemplete({templeteName:"childGrid", data:colChildCollection, dtoName:"colChildCollection"});
			}else {
				$("#trChildGrid").hide();
			}
			
			if(colInfantCollection.length > 0){
				$("#infantGrid").iterateTemplete({templeteName:"infantGrid", data:colInfantCollection, dtoName:"colInfantCollection"});
			}else {
				$("#trInfGrid").hide();
			}		
			
			UI_Confirm.adultCount = 0;
			UI_Confirm.infantCount = 0;
			if(colAdultCollection.length > 0) {
				UI_Confirm.adultCount += colAdultCollection.length;
			}

			if(colChildCollection.length > 0) {
				UI_Confirm.adultCount += colChildCollection.length;
			}

			if(colInfantCollection.length > 0){
				UI_Confirm.infantCount = colInfantCollection.length;
			}
			
			UI_Confirm.allowIndivPrint = false;
			if (response.allowIndivPrint == true || response.allowIndivPrint == 'true')
				UI_Confirm.allowIndivPrint = true;			
			
			showPaymentSummary(response.fareQuote);
			buildPaymentDetails(response.balanceSummary, response.creditPromo);
			displayTransationDetail(response.ipgTransactionResultDTO, response.jsonLabel);
			
			/* styling alternative rows for payment breakdown table */
			var index = 0;
			$('#payDetail').find('tr').each(function(i){
				if($(this).css('display')!='none'){
					if(index >0){
						$(this).find('td').removeClass('rowColorAlternate');
						$(this).find('td').removeClass('rowColor');
						if( index%2 == 0 ) {
							$(this).find('td').addClass('rowColorAlternate');
						}else{
							$(this).find('td').addClass('rowColor');
						}
					}
					index ++;
				}
			});
			$("#dvConfirmPage").show();	
			$('#btnPrint').enableNav();
			$('#btnEmailMe').enableNav();	
			dateChangerjobforLanguage(SYS_IBECommonParam.locale);
			UI_commonSystem.loadingCompleted();		
			
			// Update Session details and log user received a itinerary
			UI_Confirm.updateItinearyStatus();	
			UI_Confirm.isExistRes = true;
			// Load tracking page
			UI_Confirm.loadTrackingPage();
			//set layout Adjustment
			pageLayoutSetting();
			//load social live feeds
			UI_Confirm.enableSocialLiveFeeds(response);
			
			UI_Confirm.marketingPopDelay = response.socialTo.marketingPopupDelay;
			if(UI_Confirm.marketingPopDelay != null && UI_Confirm.marketingPopDelay != "" &&  parseInt(UI_Confirm.marketingPopDelay, 10) > 0){
				var t = setTimeout(UI_Confirm.openPop, parseInt(UI_Confirm.marketingPopDelay, 10)*1000);
			}
			
		} else {	
			// Ensure User will receive confirmation page 
			errorProcessing(response);
		}
		
		
	}
	
	function displayTransationDetail(transactionResult, label) {
		if (transactionResult != null && transactionResult.showTransactionDetail == true) {
			$("#transactionTime").text(" " + transactionResult.transactionTime);
			if (transactionResult.transactionStatus == "ACCEPTED") {
				$("#transactionStatus").text(" " + label["transaction.status.success"]);
			} else {
				$("#transactionStatus").text(" " + label["transaction.status.reject"]);
			}			
			$("#transactionAmount").text(" " + transactionResult.currency + " " + transactionResult.amount);
			$("#transactionReferenceID").text(" " + transactionResult.referenceID);
			$("#transactionMerchanttrackID").text(" " + transactionResult.merchantTrackID);	
			$("#transactionResultPanel").show();
		} else {
			$("#transactionResultPanel").hide();
		}
		
	}
	function updateFlightDetails(outFlights, inFlights, labels) { 		
		
		if (outFlights != null && outFlights.length >= 1) {
			if (outFlights.length > 1) {
				$("#outFlightDirection").text(labels.lblOutbound + " (" + labels.lblConnecting + ")");
			} else {
				$("#outFlightDirection").text(labels.lblOutbound);
			}   
			$("#outFlightTemplate").nextAll().remove();
			$("#outFlightTemplate").iterateTemplete({templeteName:"outFlightTemplate", data:outFlights, dtoName:"flightSegments"});  
			$("#outFlightsDiv").slideDown("slow");
		} else {
			$("#outFlightsDiv").slideUp("slow");
		}
		
		if (inFlights != null && inFlights.length >= 1) {
			if (inFlights.length > 1) {
				$("#inFlightDirection").text(labels.lblInbound + " (" + labels.lblConnecting + ")");
			} else {
				$("#inFlightDirection").text(labels.lblInbound);
			}  
			$("#inFlightTemplate").nextAll().remove();
			$("#inFlightTemplate").iterateTemplete({templeteName:"inFlightTemplate", data:inFlights, dtoName:"flightSegments"});	
			$("#inFlightsDiv").slideDown("slow");    			
		} else {
			$("#inFlightsDiv").slideUp("slow");
		}
 	}
	
	/*
	 * Summarize Ancillary data to display
	 */
	function summarizeAnciData (objPDt) {
		for ( var i = 0; i < objPDt.length; i++) {
			var pax = objPDt[i];
			
			var segAnciCount = 0 ;
			var additionalInfo = '';
			pax.selectedAncillaries = $.airutil.sort.quickSort(pax.selectedAncillaries, segmentComparator);
			for ( var j = 0; j < pax.selectedAncillaries.length; j++) {
				var hasSeat = false;
				var hasMeal = false;
				var hasBaggage = false;
				var hasSSR = false;
				var hasAirportService = true;
				var hasAirportTransfer = false;
				
				var anci = pax.selectedAncillaries[j];
				anci.seat = '';
				if (anci.airSeatDTO != null && anci.airSeatDTO.seatNumber != null
						&& anci.airSeatDTO.seatNumber != '') {
					anci.seat = anci.airSeatDTO.seatNumber;
					hasSeat = true;
				}				
				var meals = '';
				var meal = null
				for ( var k = 0; k < anci.mealDTOs.length; k++) {
					meal = anci.mealDTOs[k];
					if (meal.mealName != null
							&& meal.mealName != '') {						
						meals += meal.soldMeals + " x " + meal.mealName;										 
						if (k < (anci.mealDTOs.length -1)) {
							 meals += " , ";
						}
						hasMeal = true;	
					}
				} 
				anci.meals = meals;
				
				var baggages = '';
				for ( var k = 0; k < anci.baggageDTOs.length; k++) {
					if (anci.baggageDTOs[k].baggageName != null
							&& anci.baggageDTOs[k].baggageName != '') {
						if (k > 0) {
							baggages += ', ';
						}
						baggages += anci.baggageDTOs[k].baggageName;
						hasBaggage = true;
					}
				}
				anci.baggages = baggages;
	
				var ssrs = '';
				for ( var k = 0; k < anci.specialServiceRequestDTOs.length; k++) {
					if (anci.specialServiceRequestDTOs[k].description != null
							&& anci.specialServiceRequestDTOs[k].description != '') {
						if (k > 0) {
							ssrs += ', ';
						}
						ssrs += anci.specialServiceRequestDTOs[k].description;
						if (anci.specialServiceRequestDTOs[k].text != null
								&& anci.specialServiceRequestDTOs[k].text != '') {
							ssrs += (' - ' + anci.specialServiceRequestDTOs[k].text);
						}
						hasSSR = true;
					}
				}
				anci.ssrs = ssrs;
				
				var apss = '';
				for(var k=0; k<anci.airportServiceDTOs.length; k++){
					if (anci.airportServiceDTOs[k].ssrName != null
							&& anci.airportServiceDTOs[k].ssrName != '') {
						if (k > 0) {
							apss += ', ';
						}
						apss += anci.airportServiceDTOs[k].ssrName;
						if (anci.airportServiceDTOs[k].airportCode != null
								&& anci.airportServiceDTOs[k].airportCode != '') {
							apss += (' - ' + anci.airportServiceDTOs[k].airportCode);
						}
						hasAirportService = true;
					}
				}
				anci.apss = apss;
				
				var apts = '';
				for(var k=0; k<anci.airportTransferDTOs.length; k++){
					if (anci.airportTransferDTOs[k].ssrName != null
							&& anci.airportTransferDTOs[k].ssrName != '') {
						if (k > 0) {
							apts += ', ';
						}
						apts += anci.airportTransferDTOs[k].ssrName;
						if (anci.airportTransferDTOs[k].airportCode != null
								&& anci.airportTransferDTOs[k].airportCode != '') {
							apts += (' - ' + anci.airportTransferDTOs[k].airportCode);
						}
						hasAirportTransfer = true;
					}
				}
				anci.apts = apts;
				
				anci.additionalInfo = '';
				anci.segCode = getSegmentName(anci.flightSegmentTO.segmentCode);
				if(segAnciCount >0){
					additionalInfo += '<br /><br />';
				}
				if(hasSSR || hasSeat || hasMeal || hasBaggage || hasAirportService || hasAirportTransfer){
					anci.additionalInfo = (anci.segCode + ' : ');
					var hasEntry = false;
					if(hasSeat){
						anci.additionalInfo += (anci.seat);
						hasEntry = true;
					}
					if(hasMeal){
						if(hasEntry) anci.additionalInfo += ', ';
						anci.additionalInfo += (anci.meals);
						hasEntry = true;
					}
					if(hasBaggage){
						if(hasEntry) anci.additionalInfo += ', ';
						anci.additionalInfo += (anci.baggages);
						hasEntry = true;
					}
					if(hasSSR){
						if(hasEntry) anci.additionalInfo += ', ';
						anci.additionalInfo += (anci.ssrs);
						hasEntry = true;
					}
					if(hasAirportService){
						if(hasEntry) anci.additionalInfo += ', ';
						anci.additionalInfo += (anci.apss);
						hasEntry = true;
					}
					if(hasAirportTransfer){
						if(hasEntry) anci.additionalInfo += ', ';
						anci.additionalInfo += (anci.apts);
						hasEntry = true;
					}
					segAnciCount ++;
				}
				
				additionalInfo += anci.additionalInfo;
			}
			pax.additionalInfo = additionalInfo;
		}
		return objPDt;
	}
	
	function loadContactInfo (contacInfo){
		$("#paxTitle").append(contacInfo.title);
		$("#firstname").append(contacInfo.firstName);
		$("#lasttname").append(contacInfo.lastName);
		$("#address").append(contacInfo.streetAddress1);
		$("#nationality").append(contacInfo.nationality);
		$("#addressstreet").append(contacInfo.streetAddress2);
		$("#city").append(contacInfo.city);
		$("#state").append(contacInfo.state);
		$("#Country").append(contacInfo.countryCode);
		$("#mobile").append(contacInfo.mobileNo);
		$("#phone").append(contacInfo.phoneNo);
		$("#Fax").append(contacInfo.fax);
		$("#emailAddress").append(contacInfo.email);
		UI_Confirm.contactEmailAddress = contacInfo.email;
		
		if(contacInfo.emgnFirstName!="" || contacInfo.emgnLastName!="" || 
				contacInfo.emgnPhoneNo != ""){
			$("#emgnPaxTitle").append(contacInfo.emgnTitle);
			$("#emgnFirstName").append(contacInfo.emgnFirstName);
			$("#emgnLastName").append(contacInfo.emgnLastName);
			$("#emgnPhoneNo").append(contacInfo.emgnPhoneNo);
			$("#emgnEmail").append(contacInfo.emgnEmail);
		}
		
	}
	
	//Set Flight logo using flight data
	function setImage (flightSegments,carrierImage, flightData) {	
		for (var i = 0; i < flightData.length; i++) {
			 $('#' +flightSegments+ '\\['+ i +'\\]\\.' +carrierImage).attr({  
			  src: flightData[i].carrierImagePath, 
			  title: flightData[i].carrierCode, 
			  alt: flightData[i].carrierCode 	 
			});		
		}
	}
	
	function HideEmailButton(emailAddress) {
		if (emailAddress == null || $.trim(emailAddress) == "") {
			$("#tdEmail").hide();
		}
	}
	
	//Load css file according to language
	//Override normal style sheet
	function loadCssFile (language) {	
		if(language == 'ar') {
			var href = "../css/myStyle_ar_no_cache.css";
			
			$("head").append("<link>");
		    css = $("head").children(":last");
		    css.attr({
		      rel:  "stylesheet",
		      type: "text/css",
		      href: href
		   });
		   // Set body dir attribute
		   $('#direction').attr("dir", "rtl");
		   $("#rightPanel").attr("align", "left");
		} 	
	}
	
	function fltSegmentComparator(first, second){
		return (first.departureTimeLong - second.departureTimeLong);
	}
	
	function segmentComparator(first ,second){
		var parser  = function(d){
			var a = d.split('T');
			var b = a[0].split('-');
			var c = a[1].split(':');
			return new Date(b[0],parseInt(b[1],10)-1,b[2],c[0],c[1],c[2]);
		}
		var d1 = parser(first.flightSegmentTO.departureDateTime);
		var d2 = parser(second.flightSegmentTO.departureDateTime);
		return (d1.getTime()-d2.getTime());
	}
	
	function showPaymentSummary(fareQuote) {
		$.label("valBaseCurrCode").text(fareQuote.inBaseCurr.currency);
		if(parseFloat(fareQuote.inBaseCurr.totalFare)>0){
			$.label("valAirFare").text(fareQuote.inBaseCurr.totalFare);
			$.tr('trAirFare').show();
		}else{
			$.tr('trAirFare').hide();			
		}
		if(parseFloat(fareQuote.inBaseCurr.totalTaxSurcharge)>0){
			$.label("valCharges").text(fareQuote.inBaseCurr.totalTaxSurcharge);
			$.tr('trCharges').show();
		}else{
			$.tr('trCharges').hide();
		}
		
		if(fareQuote.hasFlexi){
			$.label("valFlexiCharges").text(fareQuote.flexiCharge);
			$('#trFlexiCharges').show();
		}else{
			$('#trFlexiCharges').hide();
		}
		
		if (fareQuote.hasFee) {
			$('#trTxnFee').show();
			$.label("valTxnFee").text(fareQuote.inBaseCurr.totalFee);
		} else {
			$('#trTxnFee').hide();
		}
		
		var discElem = null;
		if($.tr('trDiscount')){
			discElem = "Discount";
		} else if($.tr('trDiscountCredit')) {
			discElem = "DiscountCredit";
		}
		
		if(parseFloat(fareQuote.inBaseCurr.totalIbePromoDiscount)>0){
			if(fareQuote.deductFromTotal){
				$.label('val' + discElem).text('-' + fareQuote.inBaseCurr.totalIbePromoDiscount);
			} else {
				$.label('val' + discElem).text(fareQuote.inBaseCurr.totalIbePromoDiscount);
			}
			
			$.tr('tr' + discElem).show();
		}else{
			$.tr('tr' + discElem).hide();
		}
		
		$.label("valTotal").text(fareQuote.inBaseCurr.totalPrice);
	
		if (fareQuote.hasSeatCharge) {
			$.label("valSeatCharges").text(fareQuote.seatMapCharge);
			$('#trSeatCharges').show();
		} else {
			$('#trSeatCharges').hide();
		}
	
		if (fareQuote.hasMealCharge) {
			$.label("valMealCharges").text(fareQuote.mealCharge);
			$('#trMealCharges').show();
		} else {
			$('#trMealCharges').hide();
		}
		
		if (fareQuote.hasBaggageCharge) {
			$.label("valBaggageCharges").text(fareQuote.baggageCharge);
			$.tr('trBaggageCharges').show();
		} else {
			$.tr('trBaggageCharges').hide();
		}

        if (fareQuote.hasSsrCharge) {
            $.label("valSsrCharges").text(fareQuote.ssrCharge);
            $.tr('trSsrCharges').show();
        } else {
            $.tr('trSsrCharges').hide();
        }

		if (fareQuote.hasAirportTransferCharge) {
			$.label("valAptCharges").text(fareQuote.airportTransferCharge);
			$('#trAptCharges').show();
		} else {
			$('#trAptCharges').hide();
		}
	
		if (fareQuote.hasInsurance) {
			$.label("valInsuranceCharges").text(fareQuote.insuranceCharge);
			$('#trInsuranceCharges').show();
		} else {
			$('#trInsuranceCharges').hide();
		}
		

		if (fareQuote.hasModifySegment) {
			$("#lblModCanlChargeAmount").text(fareQuote.inBaseCurr.modifyCharge);
			$('#trModCanlCharge').show();			
		} else {
			$('#trModCanlCharge').hide();			
		}
		UI_Confirm.selectedCurrency = fareQuote.inBaseCurr.currency;
		if (fareQuote.inSelectedCurr!= null && fareQuote.inSelectedCurr.currency !=null && 
				fareQuote.inSelectedCurr.currency != fareQuote.inBaseCurr.currency) {
			//UI_Confirm.selectedCurrency = fareQuote.inSelectedCurr.currency;
			$.label("valSelectedCurrCode").text(
					fareQuote.inSelectedCurr.currency);
			$.label("valSelectedTotal").text(fareQuote.inSelectedCurr.totalPrice);
			$.tr('trSelectedCurr').show();
		} else {
			$.tr('trSelectedCurr').hide();
		}
		
		var index = 0;
		
	
	}
	
	// Update viewed Itinerary Status
	UI_Confirm.updateItinearyStatus = function() {		
		$("#submitForm").ajaxSubmit({url:"iBEClientLog!updateUserReceivedItineary.action", dataType: 'json',success: UI_Confirm.updateItinearyStatusSuccess});		
	}
	
	UI_Confirm.updateItinearyStatusSuccess = function(response) {
		
	}
	
	UI_Confirm.displayPagePannels = function() {
		$("#divSummaryPane").show();
		$("#depFlightPanel").show();
		$("#trReturnFlt").show();
		$("#trReturnFltGap").show();
		$("#trPaxDetail").show();
		$("#trContactDetail").show();
		$("#trButtonPannel").show();
		$("#trBookingSteps").show();
		$("#processingRes").hide();
	}
	
	function buildPaymentDetails(paymentData, isCreditPromotion) {
		if (paymentData != null) {			
			$("#paymentDetails").nextAll().remove();
			$("#paymentDetails").iterateTemplete({templeteName:"paymentDetails", data:paymentData, dtoName:"balanceSummary"});		
			$("#paymentDetails").nextAll().filter(':odd').find(" td").removeClass("rowColor").addClass("rowColorAlternate");
			
			
			if(isCreditPromotion){
				$("#paymentDetails").nextAll().filter('tr:last').prev().find(" td").removeClass("rowColor").addClass("totalCol");
				$("#paymentDetails").nextAll().filter('tr:last').prev().find(" label").addClass("gridHDFont fntBold");
			} else {			
				$("#paymentDetails").nextAll().filter('tr:last').find(" td").removeClass("rowColor").addClass("totalCol");
				$("#paymentDetails").nextAll().filter('tr:last').find(" label").addClass("gridHDFont fntBold");
			}
		}
	}	
	
	UI_Confirm.loadTrackingPage = function() {
		var t = UI_Confirm.fareQuote.inBaseCurr;
		var productID = UI_commonSystem.genarateProductID({flightSegments: UI_Confirm.selectedFlight});
		var fvoa = productID;//fv_order_article
		var fpq = UI_Confirm.fadultCount;//fv_product_quantity
		var nop = parseInt(UI_Confirm.fadultCount,10)+parseInt(UI_Confirm.fchildCount)+parseInt(UI_Confirm.finfantCount); //number of pax
		if (UI_Confirm.fchildCount>0){fpq+=","+UI_Confirm.fchildCount;}
		if (UI_Confirm.finfantCount>0){fpq+=","+UI_Confirm.finfantCount;}
		var fddate = UI_Confirm.selectedFlight[0].flightRefNumber.split("$")[3].substr(0,8);
		fddate = fddate.substr(0,4)+"-"+fddate.substr(4,2)+"-"+fddate.substr(6,2);
		//s_metric
		var s_m = t.totalFare + "|" + t.totalFee + "|" + t.totalTaxSurcharge + "|" + t.totalPayable + "|" + nop +
		"adult|economy|" + (UI_Confirm.fareQuote.fareType==0?"normal":"flexi") + "|visa";
		var s_c = "one_way";

		var frdate = "open",returnBooking=false;
        for (var i=0;i<UI_Confirm.selectedFlight.length;i++){
            var j = UI_Confirm.selectedFlight[i];
            if (j.returnFlag){
                returnBooking=true;
                break;
            }
        }
        var origin = UI_Confirm.selectedFlight[0].segmentShortCode.split("/")[0];
        var destination = UI_Confirm.selectedFlight[UI_Confirm.selectedFlight.length-1].segmentShortCode.split("/")[1];
		if (returnBooking){
			if (UI_Confirm.selectedFlight.length>0){
				var tx = productID.split("-");
				fvoa += ","+tx[1]+"-"+tx[0];
                destination = UI_Confirm.selectedFlight[UI_Confirm.selectedFlight.length-1].segmentShortCode.split("/")[0];
				var frdate = UI_Confirm.selectedFlight[UI_Confirm.selectedFlight.length-1].flightRefNumber.split("$")[3].substr(0,8);
                frdate = frdate.substr(0,4)+"-"+frdate.substr(4,2)+"-"+frdate.substr(6,2);
				s_c = "round_trip";
			}
		}
		var paramData = "pageID=CONFIRM&rad="+UI_commonSystem.getRandomNumber()+"&productID="+productID+
            "&price="+UI_Confirm.totalPriceBaseCurrency+"&priceRound="+Math.round(UI_Confirm.totalPriceBaseCurrency)+
            "&cur="+UI_Confirm.selectedCurrency+"&s_m="+s_m+"&fvoa="+fvoa+"&fpq="+fpq+"&fddate="+fddate+
            "&frdate="+frdate+"&s_c="+s_c+"&c_email="+UI_Confirm.contactEmailAddress;
		$("#frmTracking").attr("src", "showLoadPage!loadTrackingPage.action?"+paramData);

        //Set Dyn Ads for Flights on search results custom tag
        if(ga_conversion_id===undefined){var ga_conversion_id=00000000;}
        if(ga_language===undefined){var ga_language="en";}
        if(ga_format===undefined){var ga_format="2";}
        if(ga_color===undefined){var ga_color="ffffff";}
        if(ga_label===undefined){var ga_label="vEJpCNyo3gEQvKy-4AM";}
        if( window.google_trackConversion !== undefined) {
            window.google_trackConversion({
                google_conversion_id: ga_conversion_id,
                google_conversion_language: ga_language,
                google_conversion_format: ga_format,
                google_conversion_color: ga_color,
                google_conversion_label: ga_label,
                google_conversion_value: UI_Confirm.totalPriceBaseCurrency,
                google_remarketing_only: true,
                google_tag_params: {
                    flight_originid: origin,
                    flight_destid: destination,
                    flight_startdate: fddate,
                    flight_enddate: (frdate == "open") ? "" : frdate,
                    flight_pagetype: 'purchase'
                }
            });
        }
	}
	
	function buildInsuranceData(bookingData) {
		var policyCode = bookingData.policyNo;
		if ( policyCode != null && $.trim(policyCode) != "") {			
			if (!UI_commonSystem.isEmpty(bookingData.insuranceType)) {
				$("#lblInsuranceTypeValue").text(bookingData.insuranceType);
				$("#tdInsTypeDisplay").show()
			} else {
				$("#tdInsTypeDisplay").hide();
			}
		} else {
			
		}
	}

})(UI_Confirm);


UI_Confirm.enableSocialLiveFeeds = function (response){
	if(response.socialTo.showFacebook!= null && response.socialTo.showFacebook){
		$(".refFacebook").show();
		UI_Confirm.fbTitle= response.jsonLabel["fbTitle"];
		UI_Confirm.fbCaption=response.jsonLabel["fbCaption"];
		
		try{
			(function(d){
				var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
				js = d.createElement('script'); js.id = id; js.async = true;
				js.src = "//connect.facebook.net/en_US/all.js";
				d.getElementsByTagName('head')[0].appendChild(js);
			}(document));
		
			window.fbAsyncInit = function (){
				FB.init({
					appId : response.socialTo.facebookAppId,
					status : true, 
					cookie : true
				});
			}
		}catch (e) { }
	}else{
		$(".refFacebook").remove();
	}
	
	if(response.socialTo.showTwitter!= null && response.socialTo.showTwitter){
		$('.refTwitter').show();
	}else{
		$(".refTwitter").remove();
	}
	
	if(response.socialTo.showGooglePlus!= null && response.socialTo.showGooglePlus){
		$('.refGoogle').show();
	}else{
		$(".refGoogle").remove();
	}
	
	if(response.socialTo.showLinkedln!= null && response.socialTo.showLinkedln){
		$('.refLinkedIn').show();
	}else{
		$(".refLinkedIn").remove();
	}
	
	if(response.socialTo.showFacebook || response.socialTo.showTwitter || response.socialTo.showGooglePlus || response.socialTo.showLinkedln ) {
		UI_Confirm.socialDescription = response.jsonLabel["socialDescription"];
		UI_Confirm.description=UI_Confirm.socialDescription.replace("{0}", response.socialTo.outbound.stationStartCode).replace("{1}", response.socialTo.outbound.stationEndCode).replace("{2}", response.socialTo.outbound.journeyDate);
		UI_Confirm.outboundDesc = UI_Confirm.description;
		UI_Confirm.detailedDescription=UI_Confirm.socialDescription.replace("{0}", response.socialTo.outbound.airportStartCode).replace("{1}", response.socialTo.outbound.airportEndCode).replace("{2}", response.socialTo.outbound.journeyDate);
		
		if(response.socialTo.inbound != null){
			var arrivalDate = response.socialTo.inbound.journeyDate;
			if(arrivalDate != null){
				var returnDesc =  " " + response.jsonLabel["returnDescription"].replace("{0}", arrivalDate);
				UI_Confirm.description += returnDesc;
				UI_Confirm.detailedDescription += returnDesc;
			}
		}
		
		$('.refFacebook').click(function() {UI_Confirm.postToFacebookFeed() });
		$('.refTwitter').click(function() {UI_Confirm.postToTwitterFeed() });
		$('.refGoogle').click(function() {UI_Confirm.postToGoogleFeed() });
		$('.refLinkedIn').click(function() {UI_Confirm.postToLinkInFeed() });
		
		$("#socialNet").show();
		if(response.socialTo.showInlinePanel){
			$(".inlineSocialPanel").show();
		}
		
		UI_Confirm.showSocialFeeds = true;
	}
}

UI_Confirm.openPop = function (){
	$("#newPopItem").openMyPopUp({
		width: globalConfig.promoPopupProp.width,
		height: UI_Confirm.dimentions().height, 
		topPoint: UI_Confirm.dimentions().topPoint,
		headerHTML:"<label class='hdFontColor'> </label>",
		bodyHTML: function(){
			return UI_Confirm.populatePromoContent();
		},
		footerHTML: function(){
			var passHTML = "<input type='button' value='"+UI_Confirm.pageLabel.lblCloseThis+"' class='Button close'/> &nbsp;  &nbsp;<label style='color:#999'>"+UI_Confirm.pageLabel.lblCloseKey+"</label>";
			return passHTML;
		}
	});
	$(".close").unbind("click").bind("click",function(){$("#newPopItem").closeMyPopUp();});
	$(document).bind("keypress", function(keyEvent){
		 if(keyEvent.keyCode == 27){
			 $("#newPopItem").closeMyPopUp();
			 $(document).unbind("keypress");
          }
	});
}

UI_Confirm.dimentions = function (){
    var height = 0;
    var topPoint = ($(window).height()/2) - 210;
	if($("#linkBookHotel").length > 0 || $("#divHotel").length >0 ){
		height += 210;
	}
	if($("#linkRentCar").length > 0 || $("#divRentCar").length >0 ){
		height += 210;
	}
	if(UI_Confirm.showSocialFeeds){
		height += 290;
		topPoint = 5;
	}
	if(parseInt(height, 10) <= parseInt(globalConfig.promoPopupProp.height, 10)){
		height = globalConfig.promoPopupProp.height;
	}
	return {height: height, topPoint:topPoint };
}

UI_Confirm.windows = null;
UI_Confirm.postToLinkInFeed = function(){
	if (UI_Confirm.windows!=null){UI_Confirm.windows.close();}
	
	var intHeigh = 400;
	var intWidth = 600;
	var intTop 	= (window.screen.height - intHeigh) / 2;
	var intLeft = (window.screen.width -  intWidth) / 2;	
	var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width='+ intWidth +',height='+ intHeigh +',resizable=no,top='+ intTop +',left=' + intLeft;
	var urlToDirect = 'http://www.linkedin.com/shareArticle?mini=true';
	var ibeUrl="&url="+SYS_IBECommonParam.homeURL;
  	var urlToDirect = urlToDirect + ibeUrl + '&title=' +UI_Confirm.outboundDesc;
	UI_Confirm.windows = window.open(urlToDirect, "Linkedln", strProp);
}

UI_Confirm.postToGoogleFeed = function(){
	if (UI_Confirm.windows!=null){UI_Confirm.windows.close();}
	var intHeigh = 400;
	var intWidth = 600;
	var intTop 	= (window.screen.height - intHeigh) / 2;
	var intLeft = (window.screen.width -  intWidth) / 2;	
	var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width='+ intWidth +',height='+ intHeigh +',resizable=no,top='+ intTop +',left=' + intLeft;
	var url="https://twitter.com/intent/tweet?original_referer=&related=test&source=tweetbutton&text=";
	var formHtml =  "<HTML>" +
	"<head><title>Google + Feed</title><head>" +
	"<body>" +
	"<h3>Comming soon...</h3>" +				    	
	"</body>" +
	"</HTML>";
	UI_Confirm.windows = window.open('', "Google +", strProp);
	UI_Confirm.windows.document.writeln(formHtml);
}

UI_Confirm.postToFacebookFeed = function () {
    var obj = {
      method: 'feed',
      link: SYS_IBECommonParam.homeURL,
      // picture: 'http://reservations.airarabia.com/ibe/images/top_seal_en.gif',: 
      picture: SYS_IBECommonParam.nonSecurePath + "../images/top_seal_en.gif",
      name: UI_Confirm.fbTitle,
      caption: UI_Confirm.fbCaption,
      description: UI_Confirm.detailedDescription,
      display: 'dialog'
   }
    
    function callback(){
    	
    }

    FB.ui(obj, callback);
}

UI_Confirm.postToTwitterFeed = function () {
	if (UI_Confirm.windows!=null){UI_Confirm.windows.close();}
	
		var intHeigh = 400;
		var intWidth = 600;
		var intTop 	= (window.screen.height - intHeigh) / 2;
		var intLeft = (window.screen.width -  intWidth) / 2;	
		var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width='+ intWidth +',height='+ intHeigh +',resizable=no,top='+ intTop +',left=' + intLeft;
		var url="https://twitter.com/intent/tweet?original_referer=&related=test&source=tweetbutton&text=";
	  	var ibeUrl="&url="+SYS_IBECommonParam.homeURL;
	  	var urlToDirect = url+UI_Confirm.description+ibeUrl;
	  	UI_Confirm.windows = window.open(urlToDirect, "Twitter", strProp);
}
UI_Confirm.populatePromoContent = function(){
	var t =$("#promoContent");
	if (t.find("#divSocial").children().length > 0){
		var chC = t.find("#divSocial").children().length;
		var sL = (chC * 75) + 18;
		if(sL < 105){
			sL = 105;
		}
		$(".socialBtnArea").css("width", sL);
	}
	return t;
}

UI_Confirm.SearchCookie = function(){
	var cookies=document.cookie.split(";");
	for(var i=0;i<cookies.length;i++){
		var cookie=cookies[i].split("=");
		if($.trim(cookie[0]).search("saveOprtnDataCookie_") != -1){
			document.cookie = cookies[i] + "; expires=Thu 07 Jan 2010 01:00:00 AM IST";
		}
	}	
}

UI_Confirm.createInsurancePolicyCode = function(insurances){
	var insurancePolicyCode =  "";
	$.each(insurances,function(key,insuraceObj){
		var policyCode =  insuraceObj.policyCode;
		if(insurancePolicyCode.indexOf(policyCode)== -1){
			if(key > 0 ){
				insurancePolicyCode += " , ";
			}
			insurancePolicyCode += policyCode;
		}
		
	});
	return insurancePolicyCode;
}
