function UI_Payment(){
}

var strMsgAdult = "";
var strMsgChild = "";
var strMsgInfant = "";
var strMsgOneWayTrip = "";
var strMsgRoundTrip = "";
var strMsgProgressBar = "";
var strPaymentInfo1 = "";
var strPaymentInfo2 = "";
var strPaymentInfo3 = "";
var blnRegUser = false;
var strErrorHTML = "";
var strSelectEmpty = "<option value=''> </option>";
var skipCardTypeCheck = false;
var arrError = new Array();
var voucherIDs = new Array();
var voucherHTMLID = new Array();
var redeemedOnce = false;

UI_Payment.securePath = "";
UI_Payment.nonSecurePath = "";
UI_Payment.homeUrl = "";
UI_Payment.objPGS = null;
UI_Payment.objAllPGS = null;
UI_Payment.loaded = false;
UI_Payment.initialized = false;
UI_Payment.isCardPanelBuild = false;
UI_Payment.loyalty = null;
UI_Payment.fareQuoteOriginal = null;
UI_Payment.accessPoint = null;
UI_Payment.onHoldEnable = false;
UI_Payment.onHoldCreated = false;
UI_Payment.airportMessage = "";
UI_Payment.onHoldRestrictedBC = false;
UI_Payment.brokerType = null;
UI_Payment.isNoPay = false;
UI_Payment.cardErrorInfo = null;
UI_Payment.timerPayment = null;
UI_Payment.payTimeGap = 30 * 1000 * 1;
UI_Payment.payTimeGapShort = 30 * 1000 * 1;
UI_Payment.paymentGatewayID = null;
UI_Payment.paymentGatewayProviderCd = null;
UI_Payment.paymentOptionsInitialized = false;
UI_Payment.PaymentErrorOccured = false;
UI_Payment.selectedPaymentMethodValStr = null;
UI_Payment.reloadedToRetry = true;
UI_Payment.skipCardTypePayment = false;

UI_Payment.tnxFeeInfo = null;
UI_Payment.tnxFeeInfoOriginal = null;
UI_Payment.isModifyAncillary = false;
UI_Payment.modifySegment = false;
UI_Payment.ispaymentforOHD = false;

var jsonCEObj = null;

UI_Payment.pickBINPromotionSucess = false;
UI_Payment.promotionBIN = null;

UI_Payment.loggedUser = false;
UI_Payment.isLoyaltyRedeemed = false;
UI_Payment.isVoucherRedeemed = false;
UI_Payment.isLoyaltyEnabled = false;
UI_Payment.isFullyRedeemedByLoyalty = false;
UI_Payment.payFortOfflinePayment = false;
UI_Payment.payFortPayATHomeOfflinePayment = false;
UI_Payment.payFortMobileNumber = "";
UI_Payment.payFortEmail = "";

//common PGW info
UI_Payment.pgwPaymentMobileNumber = "";
UI_Payment.pgwPaymentEmail = "";
UI_Payment.pgwPaymentCustomerName = "";


UI_Payment.maxRedeemableInBaseCurr = 0;
UI_Payment.loyaltySliderSelValue = 0;
UI_Payment.availableLoyaltyPoints = 0;
UI_Payment.totalLmsRedeemedAmount = 0;
UI_Payment.integrateMashreqWithLMSEnabled = false;
UI_Payment.adminFeeRegulationEnabled = false;

UI_Payment.mashreqPayOption = null;

UI_Payment.payAtStorePreIndex = 0;
UI_Payment.payAtStorepostIndex = 0;
UI_Payment.payAtHomePreIndex = 0;
UI_Payment.payAtHomepostIndex = 0;

UI_Payment.ready = function() {
	// Removing previous selected PGW
	UI_Payment.paymentOptionsInitialized = false;
	UI_Payment.paymentGatewayID = null;
	
	UI_Payment.paymentOptionsInitialized
	UI_Payment.paymentGatewayID 
    UI_Container.showLoading();
    $("#divAnciPromoBanner").hide();
    $("#divSummaryPane").css("top", "0px");
    if (!UI_Payment.initialized) {
        UI_Payment.hidePaymentPage();

        $("#btnCancel").click(function() {
            SYS_IBECommonParam.homeClick();
        });
        $("#lnkHome").click(function() {
            SYS_IBECommonParam.homeClick();
        });
                
        $('input[name="loyaltyOption"]').click(function(event) {
        	if($(this).val() == 'total' && UI_Payment.isLoyaltyRedeemed){
        		//reset loyalty selection
            	jConfirm(UI_Container.getLabels().lblAirewardsRemoveAlert, UI_Container.labels.lblAlertConfirm, function(res) {
                    if (!res) {
                    	if(UI_Payment.mashreqPayOption != null){
                    		UI_Payment.mashreqPayOption.prop('checked', true);
                    	}
                    	return false;
                    } else {
                        UI_Payment.resetLoyaltyRedeemOption();
                        UI_Payment.loyaltyOptionChanged(true);
                    }
            	}, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
        	} else {
        		UI_Payment.loyaltyOptionChanged(true);
        	}        	
        });

        $('#btnRecalculate').click(function() {
            if (UI_Payment.updateFareQuoteBasedOnLoyalty()) {
                $('#btnPurchase').enableNav();
                $('#btnPurchaseOnAnci').enableNav();
                $('#btnRecalculate').disableNav();
            } else {
                $('#btnPurchase').disableNav();
                $('#btnPurchaseOnAnci').disableNav();
                $('#btnRecalculate').enableNav();
            }
            UI_Payment.disablePreviousNavigation();
            UI_Payment.resetPaymentInfoPanel();

        });

        $("#payByLoyalty_yes").click(function() {
            $("#payByAirewardPoints").show();
        });
        $("#payByLoyalty_no").click(function() {
            UI_Payment.resetLoyaltyRedeemOption();
        });

        $('#redeemPts').click(function() {
            if ($('#payByLoyalty_yes').is(':checked')) {
                if ($("#cardRadio_cash").is(":checked")) {
                    jConfirm(UI_Container.labels.lblCashPayRemovalMessage, UI_Container.labels.lblAlertConfirm, function(res) {
                        if (res) {
                            UI_Payment.redeemLoyaltyPoints(false);
                            $("#cardDetailPannel").hide();
                            $("#paymentInfoPanel").hide();
                            $("#onHoldInfoPanel").hide();
                            $("#cardRadio_cash").attr('checked', false);
                            return false;
                        }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
                } else if(UI_Payment.mashreqPayOption != null && UI_Payment.mashreqPayOption.val() != "none"){
                	jConfirm(UI_Container.getLabels().lblMashreqRemoveAlert, UI_Container.labels.lblAlertConfirm, function(res) {
                        if (res) {
                        	$('input[name="loyaltyOption"][value="none"]').prop("checked", true);
                        	UI_Payment.mashreqPayOption = $('input[name="loyaltyOption"]:checked');
                        	$('#loyaltySetteledAmount').val('');
                            $('#loyaltySetteledAmount').attr('disabled', true);
                            UI_Payment.redeemLoyaltyPoints(false);
                            return false;
                        }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
                }  else if(UI_Payment.isPayFortPaymentSelectedBeforeLoyalty()) {
                	jConfirm(UI_Container.labels.lblLoyaltyRemovalMessageForPayFort, UI_Container.labels.lblAlertConfirm, function(res) {
                        if (res) {
                            UI_Payment.redeemLoyaltyPoints(false);
                            $("#cardDetailPannel").hide();
                            $("#paymentInfoPanel").hide();
                            $("#onHoldInfoPanel").hide();
                            $('input:radio[name=cardRadio]').each(function () { $(this).prop('checked', false); });
                        	UI_Payment.paymentGatewayID = null;
                        	UI_Payment.paymentGatewayTypeCode = 'undefined';
                            return false;
                        }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
                } else {
                    UI_Payment.redeemLoyaltyPoints(false);
                }
            }
        });
        
        $("#redeemVouchers").click(function(){
        	if ($("#cardRadio_cash").is(":checked")) {
	        	if ($('#payByVoucher_yes').is(':checked')) {
	        		jConfirm(UI_Container.labels.lblCashPayRemovalMessageVouchers, UI_Container.labels.lblAlertConfirm, function(res) {
	                    if (!res) {
	                        $("#payByVouchers").hide();
	                        $("#paymentInfoPanel").hide();
	                        $("#onHoldInfoPanel").hide();
	                        $("#cardRadio_cash").attr('checked', true);
	                        $('#payByVoucher_no').attr('checked', true);
	                        return false;
	                    } else {
	                    	$("#cardRadio_cash").attr('checked', false);
	                		UI_Payment.redeemVouchers(false);
	                    }
	                }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
	        	}
        	} else {
        		UI_Payment.redeemVouchers(false)
        	}
        });

        $(function() {
            $("#sliderLoyaltyRedeem").slider({
                range: "min",
                value: 50,
                min: 0,
                max: 100,
                slide: function(event, ui) {
                    var baseCurrency = UI_Payment.fareQuoteOriginal.inBaseCurr.currency;
                    var selCurrency = UI_Payment.fareQuoteOriginal.inSelectedCurr.currency;
                    var sliderVal = ui.value;
                    var sliderValInSelCurr = sliderVal;
                    var displayCurrency = baseCurrency;
                    var step = 1;
                    if (baseCurrency != selCurrency) {
                    	sliderValInSelCurr = sliderValInSelCurr * UI_Payment.fareQuoteOriginal.selectedEXRate;
                        displayCurrency = selCurrency;
                        step = step / UI_Payment.fareQuoteOriginal.selectedEXRate;
                    }

                    if (sliderVal > UI_Payment.maxRedeemableInBaseCurr) {
                        return false;
                    }

                    $(this).slider('option', 'step', step);
                    $("#amount").html($.airutil.format.currency(sliderValInSelCurr) + ' ' + displayCurrency);
                    UI_Payment.loyaltySliderSelValue = sliderValInSelCurr;
                }
            });
        });

        $("#payByLoyalty_no").prop("checked", true).trigger("click");

        var valCurrFn = function() {
            var val = $.trim($('#loyaltySetteledAmount').val());
            val = val.replace(/[^0-9\.]/g, "");
            var por = val.split('.');
            if (por.length > 1) {
                val = '';
                for (var i = 0; i < por.length; i++) {
                    if (por.length - 1 == i) {
                        val += '.';
                        if (por[i].length > 2) {
                            por[i] = por[i].substr(0, 2);
                        }
                    }
                    val += por[i]
                }
            }
            $('#loyaltySetteledAmount').val(val)
        }
        $("#expiryMonth").change(function() {
            UI_Payment.expiryDateChange();
        });
        $("#expiryYear").change(function() {
            UI_Payment.expiryDateChange();
        });
        $("#whatisthis").click(function() {
            UI_Payment.displayCCD($(this));
        });
        $("#qiwiMobileNumber").keyup(function(event) {
            UI_Payment.qiwiMobileKeyUpValidation();
        });
        $("#payFortMobileNumber").keyup(function(event) {
            UI_Payment.payFortMobileKeyUpValidation();
        });

        $("#pgwPaymentMobileNumber").keyup(function(event) {
            UI_Payment.pwgCommonMobileKeyUpValidation();
        });
        
        $('#loyaltySetteledAmount').blur(valCurrFn);
        $('#loyaltySetteledAmount').keyup(function() {
            valCurrFn();
            $('#btnPurchase').disableNav();
            $('#btnPurchaseOnAnci').disableNav();
            $('#btnRecalculate').enableNav();
        });
        $("#selPayType").click(function() {
            UI_Payment.payTypeChanged(1);
        });
        $("#selPayType1").click(function() {
            UI_Payment.payTypeChanged(2);
        });
        
        $("#btnLmsLogin").click(function(){UI_Payment.loginClick();UI_commonSystem.showLmsInBar();});

        if (!UI_Container.isKiosk) {
            $("#lblPayAtCashier").text(UI_Container.getLabels().lblPayLater);
        }
        $("#onhold_Partners").click(function() {
            UI_Payment.openPaymentPartners(SYS_IBECommonParam.locale);
        });
        if (anciConfig.termandConditiononPayment) {
            $("#linkTerms").click(function() {
                UI_Payment.linkTermsClick(globalConfig.termsLink);
            });
            $("#trTermsCondition").show();
        }
        if (typeof(UI_Top.holder().GLOBALS) != "undefined" && UI_Top.holder().GLOBALS.promoCodeEnabled && !UI_Container.modifySegment && !UI_Container.addModifyAncillary && (typeof $("#pnr").val() == 'undefined')) {
            $("#checkPromotionsForBIN").html(UI_Container.labels.lblCheckPromoBtn);
            $("#checkPromotionsForBIN").show();
            $("#checkPromotionsForBIN").decoButton('redContinue');
            $("#checkPromotionsForBIN").click(function(event) {
                event.preventDefault();
                UI_Payment.searchForPromotions(false);
            });
        }

        UI_Payment.initialized = true;
    }
    if (!UI_Payment.loaded) {
        UI_Payment.loadPaymentdata();
        if (UI_Container.isLoyaltyManagmentEnabled()) {
            UI_Payment.loadLoyaltyPointData();
        } 
    } else {
        setTimeout('UI_Container.hideLoading()', 1000);
    }
    if (anciConfig.paymentOnAnici == true) {
        $('#divPayment').find('.StepsTable').hide();
        if (UI_Anci.noAncillary) {
            $('#butEditAnci').hide();
            $('#btnPaymentPrevious').show();
        } else {
            $('#butEditAnci').show();
            $('#btnPaymentPrevious').hide();
        }
        $('#btnPurchaseOnAnci').show();
        $('#btnPurchase').hide();
    }
    UI_Payment.loadSpecialMessages();
    //set Trasaction images path with language
    var trImgPath = "../images/tr-processing_" + UI_Container.language + "_no_cache.gif";
    $("#intExtProcessing").find("img").attr("src", trImgPath);
    UI_commonSystem.setCommonButtonSettings();
    UI_Payment.reloadedToRetry = false;

    if ((typeof $('input:radio[name=cardRadio]:checked').val() == 'undefined') &&
    		($("#resSelectedCurrency").val() == null || $("#resSelectedCurrency").val() == '')) {
        $("#paymentSelectedCurrCode").hide();
    }    
	if(voucherHTMLID.length == 0){
		voucherHTMLID.push("voucherID_0");
	}
    $("#trVoucher").hide();
    $("#voucherID_0").numeric();
    
    $('#payByVoucher_yes').click (function(){
    	$("#trVoucher").show();
    	$("#payByVouchers").show();
    	$("#btnRedeem").prop('disabled', false);
    	$("#confirmdVoucherRedeem").text("");
		$("#voucherIDList").text("");
    	for (i = 0; i<voucherHTMLID.length; i++) {
			$("#"+voucherHTMLID[i]).prop('disabled', false);
		}
    });
    
    $('#payByVoucher_no').click(function() {
    	$("#trVoucher").hide();
    	$("#payByVouchers").hide();
    	$("#btnRedeem").prop('disabled', true);
    	for (i = 0; i<voucherHTMLID.length; i++) {
			$("#"+voucherHTMLID[i]).prop('disabled', true);
		}
    	UI_Payment.resetVoucherRedeemOption();
    });
   
//    $("input[name = 'btnAddVoucher']").click( function() {
//    	UI_Payment.addVoucher();
//    });
//    
//    $("input[name = 'btnRemoveVoucher']").click( function() {
//    	UI_Payment.removeVoucher();
//    });
    
    if(redeemedOnce){
    	UI_Payment.redeemVouchers (true);
    }
    
    window.onbeforeunload = function (event) {
    	UI_Payment.resetVoucherRedeemOption();
	};
}

UI_Payment.loginClick = function(){
	if(!loginValidation( $('#lmsLoginEmail'), $('#lmsLoginPassword') )) return;	
	UI_Container.showLoading();
	var url = "ajaxLogin.action";
	var  data = new Object();
	data["txtUID"] = $('#lmsLoginEmail').val().toUpperCase();
	data["txtPWD"] = $('#lmsLoginPassword').val();
	data["page"] = "interline";
	$("#btnLmsLogin").ajaxSubmit({ type: "POST", dataType: 'json', async: true, processData: false, data:data, url:url,
		success: UI_Payment.userLoginSucess, error:UI_commonSystem.setErrorStatus});
}

UI_Payment.userLoginSucess = function(response) {
	if(response.loginSuccess) {
		var isLmsUser = false;
		if(response.lmsDetails !=null && (response.lmsDetails.emailStatus!=null || response.lmsDetails.emailStatus!="")){
			UI_commonSystem.changeUserLmsStatus(response.lmsDetails);
			UI_commonSystem.showLmsInBar();
			isLmsUser = true;
		}
		UI_commonSystem.showLmsInBar();
		UI_Container.hideLoading();
		UI_Container.buildCustomer(response.customer, response.totalCustomerCredit, response.baseCurr);
		eval(response.custProfile);
		if(top!=null & top[0]!=null){
			top[0].strReturnPage = "CUSTOMER";
			top[0].strUN = response.txtUname;
			top[0].blnMadereg = true;
		}
		// Update Register Customer Login status
		SYS_IBECommonParam.setCustomerState(true);
		
		if(isLmsUser){
			UI_Payment.loggedUser = true;
			UI_Payment.loadLoyaltyPointData();
		} else {
			$("#userLoginPanel").hide();
		}
	} else {
		jAlert(UI_commonSystem.alertText({msg:arrError["ERR061"], language:strLanguage}));
		UI_Container.hideLoading();
	}		
}

UI_Payment.onPrevNavigation = function() {
    UI_Payment.brokerType = null;
    $('input:radio[name=cardRadio]:checked').attr("checked", false);
    if (UI_Payment.pickBINPromotionSucess) {
        UI_Payment.removeBINPromotion(true);
    }
    $("#payByLoyalty_no").click();
}

UI_Payment.disablePreviousNavigation = function() {
    if (UI_Container.isOnholdPaymentCreateFlow || UI_Container.paymentRetry) {
        $('button[name="btnPrevious"]').disableNav();
    }
}

UI_Payment.loadSpecialMessages = function() {
    var msgs = UI_Container.getSpecialMessages();
    var count = 0;
    $('#specialMsg').html('');
    for (var key in msgs) {
        count++;
        $('#specialMsg').append(msgs[key]);
    }
    if (count > 0) {
        if (anciConfig.displayCCMsgAlert)
            $('#specialMsg').hide();
        else
            $('#specialMsg').slideDown('slow');
    } else {
        $('#specialMsg').hide();
    }
    $('#specialMsg').addClass("spMsg alignLeft")
}



/*
 * Validate Card Details
 */
UI_Payment.validate = function() {
    var loyaltyOption = $('input[name="loyaltyOption"]:checked').val();
    var currentBIN = $("#cardNumber").val().slice(0, 6);
    var errorMsgs = UI_Payment.cardErrorInfo;
    if (anciConfig.termandConditiononPayment && !$("#chkTerms").is(':checked')) {
        jAlert(UI_Container.errorInfo["ERR042"], 'Alert');
        return false;
    } else if (UI_Payment.pickBINPromotionSucess && (currentBIN != UI_Payment.promotionBIN)) {
        UI_Payment.removeBINPromotion(false);
        return false;
    } else if (UI_Payment.isCardPay() && UI_Payment.brokerType == "internal-external" && loyaltyOption != 'total' &&
        !UI_Payment.isNoPay && !UI_Payment.isFullyRedeemedByLoyalty) {
        UI_Payment.removeClasses();
        if (UI_Payment.paymentGatewayProviderCd != "PAYPAL") {
            var myArray = new Array();
            var myArrayindex = 0;
            for (xobj in jsonCEObj) {
                if (!(jsonCEObj[xobj].id == "#billingAddress1" || jsonCEObj[xobj].id == "#city" || jsonCEObj[xobj].id == "#postalCode" || jsonCEObj[xobj].id == "#state")) {
                    myArray[myArrayindex] = jsonCEObj[xobj];
                    myArrayindex = myArrayindex + 1;
                }

            }
            if (!UI_Payment.validatePageControl(myArray)) {
                return false;
            }
        } else {
            if (!UI_Payment.validatePageControl(jsonCEObj)) {
                return false;
            }
        }
        strErrorHTML = "";
        var firstFocus = "";
        if (!ValidateCardNos($("#cardType").val(), $("#cardNumber").val())) {
            strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidCardNo);
            if (firstFocus == "") {
                firstFocus = "cardNumber";
            }
            UI_Payment.addErrorClass("#cardNumber");
        }


        if ($("#expiryDate").val().length < 4) {
            strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidExpiryDate);
            if (firstFocus == "") {
                firstFocus = "expiryMonth";
            }
            UI_Payment.addErrorClass("#expiryMonth");
        }
        var strSysDate = UI_Container.systemDate;
        var strFullYear = strSysDate.substr(6, 4);

        var strExpriyDateMM = $("#expiryDate").val().substr(0, 2);
        var strExpriyDateYY = $("#expiryDate").val().substr(2, 4);
        if (!CheckDates("01/" + strSysDate.substr(3, 2) + "/" + strFullYear.substr(2, 2), "01/" + strExpriyDateMM + "/" + strExpriyDateYY)) {
            strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidExpiryDate);
            if (firstFocus == "") {
                firstFocus = "expiryMonth";
            }
            UI_Payment.addErrorClass("#expiryMonth");
        }


        // Validate CVD
        if (!UI_Payment.validateCvvCode($("#cardType").val(), $("#cardCVV").val())) {
            UI_Payment.addErrorClass("#cardCVV");
            strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidCVV);
            if (firstFocus == "") {
                firstFocus = "cardCVV";
            }
        }

        var discountInfo = UI_Container.getDiscountInfo();
        if (discountInfo != null && discountInfo.promotionId != null &&
            !(UI_Container.isModifySegment() || UI_Container.isRequoteFlow())) {
            var applicableBINs = discountInfo.applicableBINs;
            var cardNo = $("#cardNumber").val();

            if (applicableBINs != null && applicableBINs.length > 0) {
                var matched = false;
                for (var i = 0; i < applicableBINs.length; i++) {
                    var bin = applicableBINs[i];
                    var intCardNo = parseInt(cardNo.substr(0, 6), 10);
                    if (bin == intCardNo) {
                        matched = true;
                    }
                }

                if (!matched) {
                    strErrorHTML += UI_Payment.wrapLi(errorMsgs.ccBinNotMatch);
                    if (firstFocus == "") {
                        firstFocus = "cardNumber";
                    }
                }
            }
        }

        if (strErrorHTML != "") {
            $("#" + firstFocus).focus();
            strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
            $("#lblError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
            return false;
        }

        return true;

    } else if (typeof UI_Payment.paymentGatewayTypeCode != 'undefined' && UI_Payment.paymentGatewayTypeCode.toUpperCase().indexOf('QIWI') >= 0) {

        var strErrorHTML = "";
        var qiwiMobile = "+" + $.trim($("#qiwiMobileNumber").val());
        var regex = /^\+[0-9]{5,15}$/;

        if (!regex.test(qiwiMobile)) {
            strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidQiwiMobileNo);
            if (firstFocus == "") {
                firstFocus = "qiwiMobileNumber";
            }

            $("#" + firstFocus).focus();
            strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
            $("#lblQiwiError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
            return false;
        } else {
            $("#hdnQiwiMobileNumber").val(qiwiMobile);
            var qiwiCardType = $('input:radio[name=cardRadio]:checked').val();

            if (qiwiCardType !== 'undefined' && qiwiCardType.split(':')[0]) {
                $("#isQiwiOnlineMode").val("true");
            } else {
                $("#isQiwiOnlineMode").val("false");
            }
        }
        return true;
    } else if (typeof UI_Payment.paymentGatewayTypeCode != 'undefined' && UI_Payment.paymentGatewayTypeCode.toUpperCase().indexOf('PAY@STORE') >= 0) {

        var strErrorHTML = "";
        var payfortMobile = "+" + $.trim($("#payFortMobileNumber").val());
        var payfortEmail = $.trim($("#payFortEmail").val());
        var regex = /^\+[0-9]{5,15}$/;
        var regexMail = /^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,7}$/;
        
        if (!regex.test(payfortMobile)) {
            strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidPayFortMobileNo);
            if (firstFocus == "") {
                firstFocus = "payFortMobileNumber";
            }

            $("#" + firstFocus).focus();
            strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
            $("#lblPayFortError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
            return false;
        } else if(UI_Payment.paymentGatewayID != null && !checkEmail(payfortEmail)){
        	strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidPayFortEmail);
        	if (firstFocus == "") {
                firstFocus = "payFortEmail";
            }

            $("#" + firstFocus).focus();
            strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
            $("#lblPayFortError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
            return false;
        } else {
            $("#hdnPayFortMobileNumber").val(payfortMobile);
            $("#hdnPayFortEmail").val(payfortEmail);
        }
        return true;
    } else if (typeof UI_Payment.paymentGatewayTypeCode != 'undefined' && $("#providerName").val().indexOf('GOSELL') >= 0) {

        var strErrorHTML = "";
        var pgwPaymentMobile = "+" + $.trim($("#cdPGWPaymentMobileNumber").val());
        var pgwPaymentEmail = $.trim($("#cdPGWPaymentEmail").val());
        var pgwPaymentCustomerName = $.trim($("#cdPGWPaymentCustomerName").val());
        var regex = /^\+[0-9]{5,15}$/;
        var regexMail = /^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,7}$/;
        
        if (!regex.test(pgwPaymentMobile)) {
            strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidPaymentMobileNo);
            if (firstFocus == "") {
                firstFocus = "cdPGWPaymentMobileNumber";
            }

            $("#" + firstFocus).focus();
            strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
            $("#lblPaymentCommonError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
            return false;
        } else if(UI_Payment.paymentGatewayID != null && !checkEmail(pgwPaymentEmail)){
        	strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidPaymentEmail);
        	if (firstFocus == "") {
                firstFocus = "cdPGWPaymentEmail";
            }

            $("#" + firstFocus).focus();
            strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
            $("#lblPaymentCommonError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
            return false;
        } else if(!pgwPaymentCustomerName){
        	strErrorHTML += UI_Payment.wrapLi(errorMsgs.invalidUserName);
        	if (firstFocus == "") {
                firstFocus = "cdPGWPaymentCustomerName";
            }

            $("#" + firstFocus).focus();
            strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
            $("#lblPaymentCommonError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
            return false;
        } else {
            $("#hdnPGWPaymentMobileNumber").val(pgwPaymentMobile);
            $("#hdnPGWPaymentEmail").val(pgwPaymentEmail);
            $("#hdnPGWPaymentCustomerName").val(pgwPaymentCustomerName);
        }
        return true;  
    } else {

        if (!UI_Payment.skipCardTypePayment) {
            if (UI_Payment.isCardPay() && !$("input[name='cardRadio']").is(':checked') && UI_Payment.loyaltyOptionValue() != 'total' && !UI_Payment.isNoPay) {
                var msg = UI_Container.labels.lblCardPaymentMethod;
                msg = (msg == null || msg == "") ? "Please select your payment method" : msg;
                jAlert(msg);
                return false;
            }
        }
        return true;
    }
}

UI_Payment.qiwiMobileKeyUpValidation = function() {
    var qiwiMobile = $.trim($("#qiwiMobileNumber").val());
    var regex = /^[0-9]{1,15}$/;

    if (!regex.test(qiwiMobile)) {
        $("#qiwiMobileNumber").val("");
    }
}

UI_Payment.payFortMobileKeyUpValidation = function() {
    var payFortMobile = $.trim($("#payFortMobileNumber").val());
    var regex = /^[0-9]{1,15}$/;

    if (!regex.test(payFortMobile)) {
        $("#payFortMobileNumber").val("");
    }
}

UI_Payment.pwgCommonMobileKeyUpValidation = function() {
    var payFortMobile = $.trim($("#hdnPGWPaymentMobileNumber").val());
    var regex = /^[0-9]{1,15}$/;

    if (!regex.test(payFortMobile)) {
        $("#hdnPGWPaymentMobileNumber").val("");
    }
}

function checkEmail(s){
	var re = new RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp( "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$" ).test(s);

}

/*
 * Validate Page Control
 */
UI_Payment.validatePageControl = function(p) {
    var blnReturn = true;
    var intLen = p.length;
    var strChkEmpty = "";
    strErrorHTML = "";
    var firstFocus = "";
    var errorMsgs = UI_Payment.cardErrorInfo;
    var label = UI_Container.labels;
    var tempLabel = "";
    if (intLen > 0) {
        var i = 0;
        for (var i = intLen - 1; i >= 0; i--) {
            p[i] = $.extend({
                required: false
            }, p[i]);
            if (p[i].required) {
                if ($(p[i].id).isFieldEmpty()) {
                    strErrorHTML += UI_Payment.wrapLi(p[i].desc);
                    if (firstFocus == "") {
                        firstFocus = p[i].id;
                    }
                    UI_Payment.addErrorClass(p[i].id);
                    blnReturn = false;
                }
            }

            /* Invalid Character */
            if (p[i].id == "#cardCVV") {
                tempLabel = label.lblSecurityCode;
            } else if (p[i].id == "#cardHoldersName") {
                tempLabel = label.lblCardHolderName;
            } else if (p[i].id == "#lblCardNumber") {
                tempLabel = label.lblCardHolderName;
            } else {
                tempLabel = p[i].desc;
            }
            strChkEmpty = checkInvalidChar($(p[i].id).val(), errorMsgs.invalidChr, tempLabel);
            if (strChkEmpty != "") {
                strErrorHTML += UI_Payment.wrapLi(strChkEmpty);
                if (firstFocus == "") {
                    firstFocus = p[i].id;
                }
                UI_Payment.addErrorClass(p[i].id);
                blnReturn = false;
            }
        }

    }

    if (strErrorHTML != "") {
        $(firstFocus).focus();
        strErrorHTML = "<br/>" + strErrorHTML + "<br/>";
        $("#lblError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
    }
    return blnReturn;
}

UI_Payment.loadAirportMessages = function() {
    var apMsg = UI_Payment.airportMessage;
    $('#payPageAirportMsg').html('');
    $('#payPageAirportMsg').html(apMsg);

    if (apMsg != null && apMsg != '') {
        $('#payPageAirportMsg').slideDown('slow');
    } else {
        $('#payPageAirportMsg').hide();
        $("#trpayPageAirportMsg").hide();
    }
}

UI_Payment.loadErrorPage = function(response) {
    window.location.href = "showLoadPage!loadError.action?messageTxt=" + response.messageTxt;
}

UI_Payment.hidePaymentPage = function() {
    $("#divPayment").hide();
}

UI_Payment.showPaymentPage = function() {
    $("#payByCreditCardPanel").show();
    $("#divPayment").slideDown('slow');
}

UI_Payment.resetLoyaltyRedeemOption = function() {
    $("#sliderLoyaltyRedeem").slider("option", "value", $("#sliderLoyaltyRedeem").slider("option", "min"));
    var currCode = "";
    if (UI_Payment.fareQuoteOriginal != undefined && UI_Payment.fareQuoteOriginal != null) {
        var baseCurrency = UI_Payment.fareQuoteOriginal.inBaseCurr.currency;
        var selCurrency = UI_Payment.fareQuoteOriginal.inSelectedCurr.currency;
        if (baseCurrency != selCurrency) {
            currCode = selCurrency;
        }
    }
    $("#amount").html($("#sliderLoyaltyRedeem").slider("option", "value") + ' ' + currCode);
    $("#payByLoyalty_no").prop("checked", true);
    $("#payByAirewardPoints").hide();
    $("#confirmdRedeem").text("");
    if (UI_Payment.isLoyaltyRedeemed) {
        UI_Payment.redeemLoyaltyPoints(true);
    }
}

UI_Payment.redeemLoyaltyPoints = function(isAllowZeroPayment) {
    var selectedRedeemValue = $("#sliderLoyaltyRedeem").slider("option", "value");

    if (selectedRedeemValue != undefined && selectedRedeemValue != null) {
        var redeemedAmount = parseFloat(selectedRedeemValue);
        if (isAllowZeroPayment || redeemedAmount > 0) {
            //update payment info view on loyalty option change
            UI_Container.showLoading();
            UI_Payment.pointRedeemLoaderShow();
            $("#paymentSelectedCurrCode").hide();
            $('#btnPurchase').disableNav();
            $('#btnPurchaseOnAnci').disableNav();
            $('#btnRecalculate').disableNav();

            var data = {};
            data['redeemLoyaltyPoints'] = true;
            data['redeemRequestAmount'] = redeemedAmount;
            data['paymentGatewayID'] = UI_Payment.paymentGatewayID;
            
            if(UI_Payment.mashreqPayOption!= null && UI_Payment.mashreqPayOption.val() != "total"){
            	data['loyaltyPayOption'] = UI_Payment.mashreqPayOption.val();
                data['loyaltyCreditAmount'] = $('#loyaltySetteledAmount').val();
                data['loyaltyBased'] = true;
            }

            if (UI_Container.paymentRetry) {
                data['paymentFlow'] = $('#paymentFlow').val();
                $.ajax({
                    url: "interlinePayment.action",
                    data: data,
                    cache: false,
                    dataType: 'json',
                    async: false,
                    success: UI_Payment.loadUpdatedFareQuote,
                    error: UI_commonSystem.setErrorStatus
                });
            } else {
                data['insurance'] = $.toJSON(UI_Container.getInsurance());
                data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
                if ($("#pnr").val() != undefined) {
                    data['resPnr'] = $("#pnr").val();
                }

                if (UI_Container.isModifySegment()) {
                    if (UI_Container.isAllowInfantInsurance) {
                        data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(true));
                    } else {
                        data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(false));
                    }
                }

                $("#frmReservation").ajaxSubmit({
                    dataType: 'json',
                    processData: false,
                    data: data,
                    async: false,
                    url: "interlinePayment.action",
                    success: UI_Payment.loadUpdatedFareQuote,
                    error: UI_commonSystem.setErrorStatus
                });
            }
        } else {
        	if(UI_Payment.availableLoyaltyPoints == 0){
        		jAlert(UI_Container.getLabels().lblNoLoyaltyPoints);
        	} else {        		
        		jAlert(UI_Container.getLabels().lblInvalidLoyaltyPoints);
        	}
            $("input[name='payByLoyalty']").prop("disabled", false);
        }
    } else {
        jAlert(UI_Container.getLabels().lblInvalidLoyaltyPoints);
        $("input[name='payByLoyalty']").prop("disabled", false);
    }

}

UI_Payment.pointRedeemLoaderShow = function() {
    $("#LMS_Redeemed_Panel").show();
    $(".loader-points").show();
}

UI_Payment.pointRedeemLoaderHide = function() {
    $(".loader-points").hide();
    UI_Container.hideLoading();
}

UI_Payment.manageLMSPaymentOption = function() {
    if (UI_Payment.isLoyaltyEnabled) {
    	if(UI_Payment.isNoPay){
        	$("#confirmdRedeem").text("");
        	$("#payByLMSPointsPanel").hide();
        	$("#userLoginPanel").hide();
        } else {
        	if(UI_Payment.loggedUser){      
        		$("#userLoginPanel").hide();
        		if(UI_commonSystem.lmsDetails.emailStatus=='N'){
        			$("#confirmdRedeem").text("");
        			$("#payByLMSPointsPanel").hide();
        		} else {
        			$("#payByLMSPointsPanel").show();
        		}
        	} else {        
        		$("#userLoginPanel").show();
        	}
        }
    } else {
        $("#sliderLoyaltyRedeem").slider("option", "max", '0');
        $('#lmsTotalAvailablePoints').html('0');
        $('#lmsTotalAvailablePointsAmount').html('0');
        $("#payByLMSPointsPanel").hide();
        $("#userLoginPanel").hide();
        
        if(!UI_Payment.loggedUser && UI_Container.isLoyaltyManagmentEnabled()){
        	$("#userLoginPanel").show();
        }
        
    }
}

UI_Payment.loadLoyaltyPointData = function() {
    var data = {};
    if (!UI_Container.paymentRetry) {
    	data['insurance'] = $.toJSON(UI_Container.getInsurance());
    	data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
    	data['freeServiceDiscount'] = UI_Container.getFreeServiceDiscount();
    	data['resPnr'] = $("#pnr").val();
    }
    data['redeemRequestPoints'] = null;

    $("#frmReservation").ajaxSubmit({
        dataType: 'json',
        processData: false,
        data: data,
        async: false,
        url: "loyaltyPaymentHandle!getRedeemBreakdown.action",
        beforeSubmit: UI_Container.showLoading,
        success: UI_Payment.setLoyaltyData,
        error: UI_commonSystem.setErrorStatus
    });
}

UI_Payment.setLoyaltyData = function(response) {
    if (response != null && response.success == true) {
        UI_Payment.isLoyaltyEnabled = response.lmsEnabled;
        $('#lmsTotalAvailablePoints').html(response.memberAvailablePoints);
        UI_Payment.availableLoyaltyPoints = response.memberAvailablePoints;
        var baseCurrency = UI_Payment.fareQuoteOriginal.inBaseCurr.currency;
        var selCurrency = UI_Payment.fareQuoteOriginal.inSelectedCurr.currency;
        var availAmountDisp = response.memberAvailableAmount;
        if(response.maxRedeemAmount > UI_Payment.fareQuoteOriginal.inBaseCurr.totalPayable){
        	response.maxRedeemAmount = UI_Payment.fareQuoteOriginal.inBaseCurr.totalPayable;
        }
        var maxRedeemAmountDisp = response.maxRedeemAmount;
        var displayCurrency = baseCurrency;
        if (baseCurrency != selCurrency) {
            availAmountDisp = availAmountDisp * UI_Payment.fareQuoteOriginal.selectedEXRate;
            maxRedeemAmountDisp = maxRedeemAmountDisp * UI_Payment.fareQuoteOriginal.selectedEXRate;
            displayCurrency = selCurrency;
        }
        UI_Payment.maxRedeemableInBaseCurr = response.maxRedeemAmount;
        $('#lmsTotalAvailablePointsAmount').html($.airutil.format.currency(availAmountDisp) + " " + displayCurrency);
        $('#lmsMaxRedeemablePointsAmount').html($.airutil.format.currency(maxRedeemAmountDisp) + " " + displayCurrency);
        if (UI_commonSystem.lmsDetails != null) {
            $('#lmsFFID').html(UI_commonSystem.lmsDetails.emailId);
        }
        $("#sliderLoyaltyRedeem").slider("option", "max", response.maxRedeemAmount);   

    } else {
        //disable loyalty payment option
        UI_Payment.isLoyaltyEnabled = false;
        UI_Payment.availableLoyaltyPoints = 0;
        $('#lmsTotalAvailablePoints').html('0');
        $('#lmsTotalAvailablePointsAmount').html('0');

    }
    UI_Payment.manageLMSPaymentOption();
    UI_Container.hideLoading();
}

UI_Payment.loadPaymentdata = function() {
    UI_Payment.hidePaymentPage();
    var data = {};
    data['paymentGatewayID'] = UI_Payment.paymentGatewayID;
    if(UI_Container.fareQuote != undefined && UI_Container.fareQuote != null 
    		&& UI_Container.fareQuote.inSelectedCurr != undefined && UI_Container.fareQuote.inSelectedCurr != null){
    	data['searchParams.selectedCurrency'] = UI_Container.fareQuote.inSelectedCurr.currency;
    }
    if (UI_Container.paymentRetry) {
        data['paymentFlow'] = $('#paymentFlow').val();
        $.ajax({
            url: "interlinePayment.action",
            data: data,
            cache: false,
            async: false,
            dataType: 'json',
            beforeSubmit: UI_Container.showLoading,
            success: UI_Payment.loadPaymentPage
        });
    } else {
        data['insurance'] = $.toJSON(UI_Container.getInsurance());
        data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
        data['freeServiceDiscount'] = UI_Container.getFreeServiceDiscount();
        data['nameChangeRequote'] = $("#nameChangeRequote").val();
        data['nameChangePaxData'] = $("#nameChangePaxData").val();
        data['resPnr'] = $("#pnr").val();

        var contactDetails = UI_Container.getContactDetails();
        if (contactDetails != null) {
            data['contactEmail'] = contactDetails.emailAddress;
        }

        if (UI_Container.isModifySegment()) {
            //$("#modifingFlightInfo").val()
            if (UI_Container.isAllowInfantInsurance) {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(true));
            } else {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(false));
            }
        }

        $("#frmReservation").ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            async: false,
            url: "interlinePayment.action",
            beforeSubmit: UI_Container.showLoading,
            success: UI_Payment.loadPaymentPage,
            error: UI_commonSystem.setErrorStatus
        });
    }

    return false;
}

UI_Payment.updateFareQuoteBasedOnLoyalty = function() {

    //update payment info view on loyalty option change
    $("#paymentSelectedCurrCode").hide();

    if (!UI_Payment.validateLoyaltyOption()) {
        return false;
    }
    UI_Payment.hidePaymentPage();
    $('#btnPurchase').disableNav();
    $('#btnPurchaseOnAnci').disableNav();
    $('#btnRecalculate').disableNav();
    UI_Container.showLoading();
    var data = {};
    data['loyaltyPayOption'] = $('input[name="loyaltyOption"]:checked').val();
    data['loyaltyCreditAmount'] = $('#loyaltySetteledAmount').val();
    data['loyaltyBased'] = true;
    data['paymentGatewayID'] = UI_Payment.paymentGatewayID;

    if (UI_Container.paymentRetry) {
        data['paymentFlow'] = $('#paymentFlow').val();
        $.ajax({
            url: "interlinePayment.action",
            data: data,
            cache: false,
            dataType: 'json',
            async: false,
            beforeSubmit: UI_Container.showLoading,
            success: UI_Payment.loadUpdatedFareQuote,
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        data['insurance'] = $.toJSON(UI_Container.getInsurance());
        data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
        if ($("#pnr").val() != undefined) {
            data['resPnr'] = $("#pnr").val();
        	data['nameChangeRequote'] = $("#nameChangeRequote").val();
        	data['nameChangePaxData'] = $("#nameChangePaxData").val();
        	if ($("#groupPNR").val() != null) {
            	var pnrNo = $("#groupPNR").val();
            	var arrPnrNo = pnrNo.split(",");
            	if (arrPnrNo[0] != "") {
                	data["pnr"] = arrPnrNo[0];
            	}
        	}
        }

        if (UI_Container.isModifySegment()) {
            if (UI_Container.isAllowInfantInsurance) {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(true));
            } else {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(false));
            }
        }

        $("#frmReservation").ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            async: false,
            url: "interlinePayment.action",
            success: UI_Payment.loadUpdatedFareQuote,
            error: UI_commonSystem.setErrorStatus
        });
    }


    return true;
}

UI_Payment.loadUpdatedFareQuote = function(response) {
    UI_Payment.showPaymentPage();
    UI_Payment.pointRedeemLoaderHide();
    if (response != null && response.success == true) {
        var loyalty = response.customerLoyalty;
        var fareQuote = response.fareQuote;

        if (response.modifyBalanceDisplayPayment == true) {
            $("#tblPriceBreakdown").hide();
            UI_ModifyConfirm.displaySummary(response.updateCharge.chargesList);
            $("#modifyBal").show();
        }
        
        if (response.voucherEnabled) {
        	$("#payByVoucherPanel").show();
        } else {
        	$("#payByVoucherPanel").hide();
        }
        
        //Store tnx fee info
        UI_Payment.tnxFeeInfo = {
            "totalPrice": fareQuote.totalPrice,
            "totalPayable": fareQuote.totalPayable,
            "totalFee": fareQuote.totalFee,
            "totalPriceInSel": fareQuote.inSelectedCurr.totalPrice,
            "totalPayableInSel": fareQuote.inSelectedCurr.totalPayable,
            "totalFeeInSel": fareQuote.inSelectedCurr.totalFee
        };
        UI_Payment.tnxFeeInfoOriginal = $.airutil.dom.cloneObject(UI_Payment.tnxFeeInfo);
        UI_Payment.fareQuoteOriginal = $.airutil.dom.cloneObject(fareQuote);
        //		UI_Payment.fareQuoteOriginal.flexiUCharge =  $('#resFlexiUserSelectionCharge').val();

        UI_Payment.setActualFareQuoteValues();

        UI_Container.updateFareQuote(UI_Payment.fareQuoteOriginal);
        UI_Payment.showPaymentBreakDown(UI_Payment.fareQuoteOriginal);
        UI_Payment.stylePriceBreakDown();
        UI_Payment.loyalty = loyalty;

        UI_Payment.airportMessage = response.airportMessage;
        UI_Payment.loadAirportMessages();
        UI_Payment.hideAddServiceSummary();

        if (response.redeemLoyaltyPoints) {
            UI_commonSystem.lmsDetails.availablePoints = response.remainingLoyaltyPoints;
            UI_commonSystem.showLmsInBar();
            
            var baseCurrency = UI_Payment.fareQuoteOriginal.inBaseCurr.currency;
            var selCurrency = UI_Payment.fareQuoteOriginal.inSelectedCurr.currency;
            var totRedeemedAmount = response.totalRedeemedAmount;
            var displayCurrency = baseCurrency;
            if (baseCurrency != selCurrency) {
            	totRedeemedAmount = totRedeemedAmount * UI_Payment.fareQuoteOriginal.selectedEXRate;
                displayCurrency = selCurrency;
            }
            
            if(totRedeemedAmount == 0){
            	UI_Payment.isLoyaltyRedeemed = false;
            } else {            	
            	UI_Payment.isLoyaltyRedeemed = true;
            }
            
            if(UI_Payment.isLoyaltyRedeemed){            	
            	if(totRedeemedAmount != UI_Payment.loyaltySliderSelValue){
            		totRedeemedAmount = UI_Payment.loyaltySliderSelValue;
            	}
            	
            	var str = "Congratulations! You have successfully redeemed : " + $.airutil.format.currency(totRedeemedAmount) + " " + displayCurrency + " " + " from your loyalty points. Your remaining balance is : " + UI_commonSystem.lmsDetails.availablePoints + " points";
            	$("#confirmdRedeem").text(str);
            }
            
            UI_Payment.totalLmsRedeemedAmount = totRedeemedAmount;
        } else if (response.voucherRedemption) {
        	UI_Payment.isVoucherRedeemed = true;
        	UI_Payment.redeemSuccess(response);
        }
        
        var payable = parseFloat(UI_Payment.fareQuoteOriginal.totalPayable);
        if (payable > 0) {
            UI_Payment.skipCardTypePayment = false;
            UI_Payment.isFullyRedeemedByLoyalty = false;
            $("#divCardInfoPannel").show();
        } else {
            UI_Payment.skipCardTypePayment = true;
            UI_Payment.isFullyRedeemedByLoyalty = true;
            $("#divCardInfoPannel").hide();
        }

    } else {
        if (response.redeemLoyaltyPoints) {
            UI_Payment.isLoyaltyRedeemed = false;
            UI_Payment.resetLoyaltyRedeemOption();
            jAlert(response.messageTxt);
        } else {        	
        	UI_Payment.loadErrorPage(response);
        }
    }
    UI_Payment.disablePreviousNavigation();
}

UI_Payment.loyaltyOptionChanged = function(isOnClick) {

    UI_Payment.mashreqPayOption = $('input[name="loyaltyOption"]:checked');
    var val = UI_Payment.mashreqPayOption.val();
    if (val != null) {
        switch (val) {
            case 'total':
                $('#loyaltySetteledAmount').val('');
                $('#loyaltySetteledAmount').attr('disabled', true);
                if (UI_Payment.updateFareQuoteBasedOnLoyalty()) {
                    $('#btnPurchase').enableNav();
                    $('#btnPurchaseOnAnci').enableNav();
                    $('#btnRecalculate').disableNav();
                }
                break;
            case 'part':

                if ($("#paymentType").val() == "ONHOLD") {
                    $('#btnRecalculate').disableNav();
                } else {
                    $('#loyaltySetteledAmount').val('0');
                    $('#loyaltySetteledAmount').attr('disabled', true);
                    if (UI_Payment.updateFareQuoteBasedOnLoyalty()) {
                        $('#loyaltySetteledAmount').val('');
                        $('#loyaltySetteledAmount').prop('disabled', false);
                        UI_Payment.disablePurchaseNav();
                        $('#btnRecalculate').enableNav();
                    }
                }
                break;

            case 'none':
                $('#loyaltySetteledAmount').val('');
                $('#loyaltySetteledAmount').attr('disabled', true);
                if (UI_Payment.updateFareQuoteBasedOnLoyalty()) {
                    $('#btnPurchase').enableNav();
                }
                break;
            default:
                jAlert('invalid option');
        }
    }
    UI_Payment.displayCardDetailPanel(isOnClick);
}

UI_Payment.disablePurchaseNav = function() {
    $('#btnPurchase').disableNav();
    $('#btnPurchaseOnAnci').disableNav();
}

UI_Payment.validateLoyaltyOption = function() {
    if(UI_Payment.integrateMashreqWithLMSEnabled){
    	$("input[name=loyaltyOption][value='none']").prop('checked', true);
    }
    var val = $('input[name="loyaltyOption"]:checked').val();
    var retVal = false;
    if (val != null) {
        var credit = 0;
        var totalAmt = 0;
        if (!UI_Payment.fareQuoteOriginal.hasFee) {
            totalAmt = parseFloat(UI_Payment.fareQuoteOriginal.inBaseCurr.totalPrice);
        } else {
            totalAmt = parseFloat(UI_Payment.fareQuoteOriginal.inBaseCurr.totalPrice) - parseFloat(UI_Payment.fareQuoteOriginal.inBaseCurr.totalFee);
        }
        
        if(UI_Payment.totalLmsRedeemedAmount != null){        	
        	totalAmt = totalAmt - UI_Payment.totalLmsRedeemedAmount;
        }
        
        var totalLoyalty = parseFloat(UI_Payment.loyalty.creditAmount);
        switch (val) {
            case 'total':
                if (totalAmt <= totalLoyalty) {
                    retVal = true;
                }
                break;
            case 'part':
                var amt = $.trim($('#loyaltySetteledAmount').val());
                var amtFlt = parseFloat(amt);
                var labels = UI_Container.getLabels();
                if (amt == '' || isNaN(amtFlt)) {
                    $('#loyaltySetteledAmount').attr('disabled', false);
                    $('#loyaltySetteledAmount').focus();
                    jAlert(labels.msgInvalidAmount);
                    retVal = false;
                } else if (amtFlt > totalLoyalty) {
                    $('#loyaltySetteledAmount').focus();
                    jAlert(labels.msgAmountGreateThanCredit);
                    retVal = false;
                } else if (totalAmt <= amtFlt) {
                	if(totalAmt == amtFlt && UI_Payment.isLoyaltyRedeemed){
                		$('#loyaltySetteledAmount').val($.airutil.format.currency(amt));
                        retVal = true;
                	} else {                		
                		var maxVal = $.airutil.format.currency(totalAmt) + ' ' + UI_Payment.fareQuoteOriginal.inBaseCurr.currency;
                		jConfirm(labels.msgPayTotalByLoyaltyConfirm.replace('{0}', maxVal),
                				labels.msgPayTotalHD,
                				function(response) {
                			if (response) {
                				UI_Payment.resetLoyaltyRedeemOption();
                				clickLoyalty('total');
                			} else {
                				$('#loyaltySetteledAmount').select().focus();
                			}
                		});
                		retVal = false;
                	}
                } else {
                    $('#loyaltySetteledAmount').val($.airutil.format.currency(amt));
                    retVal = true;
                }
                break;
            case 'none':
                retVal = true;
                break;
            default:
                jAlert('invalid option');
                retVal = false;
        }
    }
    return retVal;
}

UI_Payment.releaseSeats = function() {
    $("#frmReservation").attr('action', 'interlineAnciRelease.action');
    $("#frmReservation").ajaxSubmit({
        dataType: 'json',
        success: function() {},
        error: UI_commonSystem.setErrorStatus
    });
}

UI_Payment.showPaymentBreakDown = function(fareQuote) {
    //	This overrides payment gateway currency amount with selected amount in selected currency
    //	if (fareQuote.inSelectedCurr.currency != fareQuote.inBaseCurr.currency
    //			&& fareQuote.inSelectedCurr.currency != null) {
    //
    //		$("#paymentSelectedCurrCode").show();
    //		$("#lblTotalDueAmountSelected").text(
    //				fareQuote.inSelectedCurr.totalPayable);
    //	}

    if (fareQuote.hasSeatCharge) {
        $('#lblSeatCharges').html(fareQuote.seatMapCharge);
        $('#trSeatChargesBD').show();
    } else {
        $('#lblSeatCharges').html('');
        $('#trSeatCharges').hide();
        $('#trSeatChargesBD').hide();
    }
    if (fareQuote.hasMealCharge && (UI_Container.isFOCmealEnabled == false)) {
        $('#lblMealCharges').html(fareQuote.mealCharge);
        $('#trMealChargesBD').show();
    } else {
        $('#lblMealCharges').html('');
        $('#trMealChargesBD').hide();
    }
    if (fareQuote.hasInsurance) {
        $('#lblInsuranceCharges').html(fareQuote.insuranceCharge);
        $('#trInsuranceChargesBD').show();
    } else {
        $('#lblInsuranceCharges').html('');
        $('#trInsuranceChargesBD').hide();
    }
    if (fareQuote.hasAirportServiceCharge) {
        $('#lblHalaCharges').html(fareQuote.airportServiceCharge);
        $('#trHalaChargesBD').show();
    } else {
        $('#lblHalaCharges').html('');
        $('#trHalaChargesBD').hide();
    }

    if (fareQuote.hasAirportTransferCharge) {
        $('#lblAirportTransferCharges').html(fareQuote.airportTransferCharge);
        $('#trAirportTransferChargesBD').show();
    } else {
        $('#lblAirportTransferCharges').html('');
        $('#trAirportTransferChargesBD').hide();
    }


    if (fareQuote.hasLoyaltyCredit) {
        $('#valUtilizedCredit').html(fareQuote.loyaltyCredit);
        $('#trUtilizedCredit').show();
    } else {
        $('#valUtilizedCredit').html('');
        $('#trUtilizedCredit').hide();
    }

    if (fareQuote.hasFee) {
        $("#lblTNXFeeAmount").text(fareQuote.inBaseCurr.totalFee);
        $('#trTNXFee').show();
    } else {
        $('#lblTNXFeeAmount').html('');
        $('#trTNXFee').hide();
    }
    if (parseFloat(fareQuote.inBaseCurr.totalFare) > 0) {
        $("#lblTotAirFareAmount").text(fareQuote.inBaseCurr.totalFare);
        $('#trAirFareBD').show();
    } else {
        $('#lblTotAirFareAmount').html('');
        $('#trAirFareBD').hide();
    }
    if (parseFloat(fareQuote.inBaseCurr.totalTaxSurcharge) > 0) {
        $("#lblTotTaxChargeAmount")
            .text(fareQuote.inBaseCurr.totalTaxSurcharge);
        $('#trChargesBD').show();
    } else {
        $('#lblTotTaxChargeAmount').html('');
        $('#trChargesBD').hide();
    }

    if (fareQuote.hasReservationCredit) {
        $('#valReservationCredit').html(fareQuote.reservationCredit);
        $('#trReservationCredit').show();
    } else {
        $('#valReservationCredit').html('');
        $('#trReservationCredit').hide();
    }

    if (fareQuote.hasReservationBalance) {
        $('#valBalanceToPay').html(fareQuote.reservationBalance);
        $('#trBalanceToPay').show();
    } else {
        $('#valBalanceToPay').html('');
        $('#trBalanceToPay').hide();
    }

    if (fareQuote.hasModifySegment) {
        $("#lblModCanlChargeAmount").text(fareQuote.inBaseCurr.modifyCharge);
        $('#trModCanlCharge').show();

        $("#lblTotalPaidAmount").text(fareQuote.inBaseCurr.creditableAmount);
        $('#trTotalPaid').show();
    } else {
        $('#trModCanlCharge').hide();
        $('#trTotalPaid').hide();
    }

    $("label[name='lblTotalDueAmount']").text(fareQuote.inBaseCurr.totalPayable);
    if(fareQuote.inSelectedCurr.currency != null && fareQuote.inSelectedCurr.currency != ''){
    	$("#paymentSelectedCurrCode").show();
    	if(fareQuote.inSelectedCurr.totalPayable != '' && fareQuote.inSelectedCurr.totalPayable != '0.00'){
    		$("#lblTotalDueAmountSelected").text(fareQuote.inSelectedCurr.totalPayable);
    	}else{
    		$("#lblTotalDueAmountSelected").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.inBaseCurr.totalPayable));
    	}
    	$('#lblSelectedCurrCode').text(fareQuote.inSelectedCurr.currency);
    	$('#lblTotalDue').hide();
    	$('#lblTotalDueAmount').hide();
    	$("#tblPriceBreakdown").find('label[name="valBaseCurrCode"]').hide();
    }

}
UI_Payment.gridInit = false;

UI_Payment.loadPaymentPage = function(response) {
    if (response != null && response.success == true) {
        // Modify Balance Summary
        //TODO Merge with Old Price break Down
        if (response.modifyBalanceDisplayPayment == true) {
            $("#tblPriceBreakdown").hide();
            UI_ModifyConfirm.displaySummary(response.updateCharge.chargesList);
            $("#modifyBal").show();
        }
        UI_Payment.payFortOfflinePayment = response.payFortOfflinePayment;
        UI_Payment.payFortPayATHomeOfflinePayment = response.payFortPayATHomeOfflinePayment;
        UI_Payment.payFortMobileNumber = response.hdnPGWPaymentMobileNumber;
        UI_Payment.payFortEmail = response.hdnPGWPaymentEmail;
        UI_Payment.pgwPaymentMobileNumber = response.hdnPGWPaymentCustomerName;
        UI_Payment.pgwPaymentEmail = response.pgwPaymentEmail;
        UI_Payment.pgwPaymentCustomerName = response.pgwPaymentCustomerName;
        UI_Payment.integrateMashreqWithLMSEnabled = response.integrateMashreqWithLMSEnabled;
        UI_Payment.adminFeeRegulationEnabled = response.adminFeeRegulationEnabled;
        UI_Payment.objPGS = response.paymentGateways;
        UI_Payment.objAllPGS = response.allPaymentGateways;
        UI_Payment.isNoPay = response.hasNopay;
        UI_Payment.accessPoint = response.accessPoint;
        UI_Payment.onHoldEnable = response.onHoldEnable;
        UI_Payment.onHoldCreated = response.onHoldCreated;
        UI_Payment.onHoldRestrictedBC = response.onHoldRestrictedBC;
        UI_Payment.onHoldDisplayTime = response.onHoldDisplayTime;
        UI_Payment.onHoldImageCapcha = response.oNDImageCapcha
        UI_Payment.buildPaymentOption(UI_Payment.accessPoint,
            UI_Payment.onHoldEnable, response.onHoldDisplayTime,
            UI_Payment.onHoldRestrictedBC);
        UI_Payment.cardErrorInfo = response.cardErrorInfo;
        UI_Payment.paymentGatewayID = response.paymentGatewayID;
        UI_Payment.loggedUser = response.loggedUser;

        var labels = UI_Container.getLabels();
        var strPurchase = labels["btnPurchase"];
        var strRecalculate = labels['btnRecalculate']

        if (response.voucherEnabled) {
        	$("#payByVoucherPanel").show();
        } else {
        	$("#payByVoucherPanel").hide();
        }

        if ($('#btnPurchase').children().length > 0) {
            if ($("#paymentType").val() == 'ONHOLD') {
                $('#btnPurchase').find("td.buttonMiddle").html("Pay Later");
            } else {
                $('#btnPurchase').find("td.buttonMiddle").html(strPurchase);
            }
        } else {
            $('#btnPurchase').val(strPurchase);
        }
        $('#btnRecalculate').val(strRecalculate);
        strPaymentInfo1 = labels["lblPaymentInfo1"];
        strPaymentInfo2 = labels["lblPaymentInfo2"];
        strPaymentInfo3 = labels["lblPaymentInfo3"];

        $("#lblYourIP").text(response.userIp);
        var loyalty = response.customerLoyalty;
        var fareQuote = response.fareQuote;

        if (UI_Payment.adminFeeRegulationEnabled) {
        	UI_Payment.tnxFeeInfo = {
                    "totalPrice": fareQuote.totalPrice,
                    "totalPayable": fareQuote.totalPayable,
                    "totalFee": fareQuote.totalFee,
                    "totalPriceInSel": fareQuote.inSelectedCurr.totalPrice,
                    "totalPayableInSel": fareQuote.inSelectedCurr.totalPayable,
                    "totalFeeInSel": fareQuote.inSelectedCurr.totalFee
                };
        }else{
        	UI_Payment.tnxFeeInfo = {
                    "totalPrice": fareQuote.totalPrice,
                    "totalPayable": fareQuote.totalPayable,
                    "totalFee": fareQuote.totalFee,
                    "totalPriceInSel": fareQuote.inSelectedCurr.totalPrice,
                    "totalPayableInSel": fareQuote.inSelectedCurr.totalPayable,
                    "totalFeeInSel": fareQuote.inSelectedCurr.totalFee
                };
        }
        
        UI_Payment.tnxFeeInfoOriginal = $.airutil.dom.cloneObject(UI_Payment.tnxFeeInfo);
        UI_Payment.fareQuoteOriginal = $.airutil.dom.cloneObject(fareQuote);

        var element = $('input:radio[name=cardRadio]:checked');
        var effectiveFareQuote = UI_Payment.skipPromotionIfCashPayment(element); //UI_Payment.fareQuoteOriginal;

        if (response.promotionPayment != null && response.promotionPayment) {
            //Promotions payments, occur for confirmed reservations only. So no other payments to be made.
            var msg = UI_Container.labels.lblNextSeatServices;
            msg = (msg == null || msg == "") ? "Next Seat Charges + services." : msg;
            $('#lblTotalFlightServices').text(msg);
        }

        UI_Payment.loyalty = loyalty;
        UI_Payment.setupLayoutPaymentRetry(response, effectiveFareQuote);
        UI_Payment.setActualFareQuoteValues(effectiveFareQuote);

        UI_Container.updateFareQuote(effectiveFareQuote);
        UI_Payment.showPaymentBreakDown(effectiveFareQuote);
        UI_Payment.stylePriceBreakDown();
        UI_Payment.showLoyalty(loyalty, effectiveFareQuote);

        if (!UI_Payment.gridInit && !UI_Container.paymentRetry) {
            UI_Payment.loadFlightGirddata(UI_Container.getOutboundFlightSegs());
            if (UI_Container.getInboundFlightSegs().length > 0) {
                UI_Payment.loadFlightRetrunGirddata(UI_Container
                    .getInboundFlightSegs());
            } else {
                $("#trRetrunGrid").hide();
            }
            UI_Payment.gridInit = true;
        }

        $("#selPG").children().remove();
        $("#selPG").append(response.pgOptions);

        var isGWSet = false;
        var selPayGatawayVal = "";
        if (UI_Payment.objPGS != null) {
            // Build Payment Card List
            UI_Payment.isModifyAncillary = response.modifyAncillary;
            UI_Payment.modifySegment = response.modifySegment;
            UI_Payment.ispaymentforOHD = response.makePayment;
            UI_Payment.buildPaymentCardListPannel(response.cardsInfo);
        } else {
            $("#payByCard").hide();
        }
        UI_Payment.buildCardInitPannel();
        UI_Payment.setPaymentRetryData(response.selectedPaymentMethodDTO, loyalty);
        if (!ui_paymentGWManger.showPGS) { //this configs taken from globalConfig.js just see what are PGWs integrated when testing
            $("#selPG").hide();
            $("#lblPG").hide();
        }

        UI_Payment.showPaymentPage();

        //$("#divLoadMsg").hide();
        UI_Container.hideLoading();

        UI_Payment.airportMessage = response.airportMessage;
        UI_Payment.loadAirportMessages();

        if (UI_Payment.objPGS != null) {
            UI_Payment.hideCCPanel();
        }

        if(UI_Payment.modifySegment || UI_Payment.isModifyAncillary || UI_Payment.ispaymentforOHD){
        	try{
    			eval(response.errorMessageInfo);
    		}catch(e){}
        }
    } else {
        UI_Payment.loadErrorPage(response);
    }
    UI_Container.hideLoading();
    UI_Payment.disablePreviousNavigation();
}
UI_Payment.changeExternalBehavior = function() {
    $("#cardDetailPannel").hide();
    $("#paymentInfoPanel").hide();
    $("#onHoldInfoPanel").hide();
    $("#externalEmbaded").show();
    UI_Container.processPostCardSubmit();
}
UI_Payment.hideCCPanel = function() {
    if (UI_Payment.objPGS.length > 0) {
        if ($("#selPayType1").attr("checked") && ui_paymentGWManger.selectDefaultCard) {
            var allCardType = $("input[name='cardRadio']");
            $(allCardType[ui_paymentGWManger.defaultCardindex]).attr("checked", true);
            $(allCardType[ui_paymentGWManger.defaultCardindex]).trigger('click');
        }
        if (!ui_paymentGWManger.showCardList) {
            $("#customCCPanel").hide();
        }

    }

    /*for ( var pl = 0; pl < UI_Payment.objPGS.length; pl++) {
     if ( ($("#selPayType1").attr("checked")) &&
     UI_Payment.objPGS[pl].providerCode == "WIRECARD"){
     var allCardType = $("input[name='cardRadio']");
     for (var i=0;i<allCardType.length;i++){
     if (UI_Payment.objPGS[pl].paymentGateway = allCardType[i].value.split(":")[1]){
     $(allCardType[i]).attr("checked", true);
     $(allCardType[i]).trigger('click');
     skipCardTypeCheck = true;
     $("#customCCPanel").hide();
     break;
     }
     }
     }
     }*/
}

UI_Payment.setupLayoutPaymentRetry = function(response, fareQuoteOriginal) {
    if (UI_Container.paymentRetry) {
        if (response.modifySegment) {
            UI_Container.modifySegment = true;
        }
        UI_Container.buildSegmentNameMap(response.flightSegments);
        $(".bookingSummary").summaryPanel({
            id: "BookingSummary",
            data: response,
            labels: UI_Container.labels
        });
        $(".paymentSummary").summaryPanel({
            id: "PaymentSummary",
            data: fareQuoteOriginal,
            labels: UI_Container.labels
        });
        UI_Payment.hideAddServiceSummary();
    }
}

UI_Payment.setPaymentRetryData = function(selectedPaymentMethodDTO, loyalty) {
    if (UI_Container.paymentRetry) {
        UI_Payment.paymentRetryErrorMessage(selectedPaymentMethodDTO, loyalty);
    }
}

UI_Payment.hideAddServiceSummary = function() {
    if (UI_Container.paymentRetry) {
        $("#addServiceDiv").hide();
    }
}

UI_Payment.paymentRetryErrorMessage = function(selectedPaymentMethodInfo, loyalty) {
    UI_Payment.disablePreviousNavigation();
    if (UI_Container.paymentRetry) {
        if (selectedPaymentMethodInfo != null) {
            var buildRadioValue = selectedPaymentMethodInfo.cardType + ":" + selectedPaymentMethodInfo.gatewayID;
            $('input:radio[value^="' + buildRadioValue + '"]').attr('checked', 'checked');
            var params = {};
            params['fareQuotePgwCharge'] = false;
            UI_Payment.paymentMethodChange(params);
        }
        if (loyalty.enabled) {
            clickLoyalty(selectedPaymentMethodInfo.loyaltyOption.toLowerCase());
            if (selectedPaymentMethodInfo.loyaltyOption == 'PART') {
                $('#loyaltySetteledAmount').val($.airutil.format.currency(selectedPaymentMethodInfo.loyaltyCredit));
                $('#btnRecalculate').click();
            }
        }
        var message = "</br><font class='mandatory paddingL5'>" + UI_Payment.wrapLi(errorMessage) + "</font></br>";
        $("#lblError").html(message);
        $("#lblErrorExternalIPG").html(message);
        $("#divCardInfoPannel").show();
    }
}

UI_Payment.setActualFareQuoteValues = function(fareQuoteObj) {
    //selectCardVal ==> cash:null:onHold
    var element = $('input:radio[name=cardRadio]:checked');
    var loyaltyOption = $('input[name="loyaltyOption"]:checked').val();
    var selectCardVal = element.val();
    var shouldHaveCCFee = (typeof(selectCardVal) == "undefined" && (loyaltyOption == "part" || loyaltyOption == "none"));

    if (fareQuoteObj == undefined || fareQuoteObj == null) {
        fareQuoteObj = UI_Payment.fareQuoteOriginal;
    }

    if (typeof(selectCardVal) == "undefined" && !shouldHaveCCFee) {
    	
    	if (UI_Payment.adminFeeRegulationEnabled) {
    		var totPrice = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPrice));
            var totPayable = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPayable));
            var selTotPrice = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPriceInSel));
            var selTotPayable = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPayableInSel));

            fareQuoteObj.totalPrice = totPrice;
            fareQuoteObj.totalPayable = totPayable;
            fareQuoteObj.selectedtotalPrice = selTotPrice;
            fareQuoteObj.selectedTotalPayable = selTotPayable;
            fareQuoteObj.hasFee = true;
            fareQuoteObj.inBaseCurr.totalPrice = totPrice;
            fareQuoteObj.inBaseCurr.totalPayable = totPayable;
            fareQuoteObj.inBaseCurr.totalFee = parseFloat("0");
            fareQuoteObj.inSelectedCurr.totalPrice = selTotPrice;
            fareQuoteObj.inSelectedCurr.totalPayable = selTotPayable;
    	}
    	else {
    		var totPrice = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPrice) - parseFloat(UI_Payment.tnxFeeInfoOriginal.totalFee));
    		var totPayable = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPayable) - parseFloat(UI_Payment.tnxFeeInfoOriginal.totalFee));
    		var selTotPrice = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPriceInSel) - parseFloat(UI_Payment.tnxFeeInfoOriginal.totalFeeInSel));
    		var selTotPayable = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPayableInSel) - parseFloat(UI_Payment.tnxFeeInfoOriginal.totalFeeInSel));
    		
    		 fareQuoteObj.totalPrice = totPrice;
    	     fareQuoteObj.totalPayable = totPayable;
    	     fareQuoteObj.selectedtotalPrice = selTotPrice;
    	     fareQuoteObj.selectedTotalPayable = selTotPayable;
    	     fareQuoteObj.totalFee = parseFloat("0");
    	     fareQuoteObj.hasFee = false;
    	     fareQuoteObj.inBaseCurr.totalPrice = totPrice;
    	     fareQuoteObj.inBaseCurr.totalPayable = totPayable;
    	     fareQuoteObj.inBaseCurr.totalFee = parseFloat("0");
    	     fareQuoteObj.inSelectedCurr.totalPrice = selTotPrice;
    	     fareQuoteObj.inSelectedCurr.totalPayable = selTotPayable;
    	     fareQuoteObj.inSelectedCurr.totalFee = parseFloat("0");
    	}
    	
    } else {

        if (!shouldHaveCCFee && selectCardVal.split(":")[2] == "onHold") {
        	
        	if (UI_Payment.adminFeeRegulationEnabled) {
        		
        		var tnxFeeObj = null;
                if (UI_Payment.isCashPaymentWithPromotion(element)) {
                    tnxFeeObj = UI_Payment.tnxFeeInfo;
                } else {
                    tnxFeeObj = UI_Payment.tnxFeeInfoOriginal;
                }

                var totPrice = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPrice));
                var totPayable = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPayable));
                var selTotPrice = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPriceInSel));
                var selTotPayable = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPayableInSel));

                fareQuoteObj.totalPrice = totPrice;
                fareQuoteObj.totalPayable = totPayable;
                fareQuoteObj.selectedtotalPrice = selTotPrice;
                fareQuoteObj.selectedTotalPayable = selTotPayable;
                fareQuoteObj.hasFee = true;
                fareQuoteObj.inBaseCurr.totalPrice = totPrice;
                fareQuoteObj.inBaseCurr.totalPayable = totPayable;
                fareQuoteObj.inSelectedCurr.totalPrice = selTotPrice;
                fareQuoteObj.inSelectedCurr.totalPayable = selTotPayable;
        		
        	}else {
        		
        		var tnxFeeObj = null;
                if (UI_Payment.isCashPaymentWithPromotion(element)) {
                    tnxFeeObj = UI_Payment.tnxFeeInfo;
                } else {
                    tnxFeeObj = UI_Payment.tnxFeeInfoOriginal;
                }

                var totPrice = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPrice) - parseFloat(tnxFeeObj.totalFee));
                var totPayable = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPayable) - parseFloat(tnxFeeObj.totalFee));
                var selTotPrice = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPriceInSel) - parseFloat(tnxFeeObj.totalFeeInSel));
                var selTotPayable = $.airutil.format.currency(parseFloat(tnxFeeObj.totalPayableInSel) - parseFloat(tnxFeeObj.totalFeeInSel));

                fareQuoteObj.totalPrice = totPrice;
                fareQuoteObj.totalPayable = totPayable;
                fareQuoteObj.selectedtotalPrice = selTotPrice;
                fareQuoteObj.selectedTotalPayable = selTotPayable;
                fareQuoteObj.totalFee = parseFloat("0");
                fareQuoteObj.hasFee = false;
                fareQuoteObj.inBaseCurr.totalPrice = totPrice;
                fareQuoteObj.inBaseCurr.totalPayable = totPayable;
                fareQuoteObj.inBaseCurr.totalFee = parseFloat("0");
                fareQuoteObj.inSelectedCurr.totalPrice = selTotPrice;
                fareQuoteObj.inSelectedCurr.totalPayable = selTotPayable;
                fareQuoteObj.inSelectedCurr.totalFee = parseFloat("0");
        		
        	}

        } else if (!UI_Payment.fareQuoteOriginal.hasFee) {

            var totPrice = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPrice));
            var totPayable = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPayable));
            var totFee = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalFee));
            var selTotPrice = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPriceInSel));
            var selTotPayable = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalPayableInSel));

            fareQuoteObj.totalPrice = totPrice;
            fareQuoteObj.totalPayable = totPayable;
            fareQuoteObj.totalFee = totFee;
            fareQuoteObj.selectedtotalPrice = selTotPrice;
            fareQuoteObj.selectedTotalPayable = selTotPayable;
            fareQuoteObj.hasFee = true;
            fareQuoteObj.inBaseCurr.totalPrice = totPrice;
            fareQuoteObj.inBaseCurr.totalPayable = totPayable;
            fareQuoteObj.inBaseCurr.totalFee = totFee;
            fareQuoteObj.inSelectedCurr.totalPrice = selTotPrice;
            fareQuoteObj.inSelectedCurr.totalPayable = selTotPayable;
            fareQuoteObj.inSelectedCurr.totalFee = $.airutil.format.currency(parseFloat(UI_Payment.tnxFeeInfoOriginal.totalFeeInSel));
        }

    }
}

UI_Payment.buildPaymentCardListPannel = function(cardsInfo) {
    var labels = UI_Container.getLabels();
    if (!UI_Payment.onHoldEnable && (cardsInfo == null || cardsInfo == "" || cardsInfo.length == 0)) {
        $("#payByCreditCardPanel, #payByCard").hide();
        jAlert("Can not find payment method. Invalid payment gateway configuration. Please contact call center or administrator");
        return;
    }
    
    setClassName = function(className) {       
        return className.toUpperCase();
    };
    var cardInfo = null;

    if (UI_Payment.onHoldEnable && anciConfig.onHoldvisibility == 2 && !UI_Container.isBINPromotion()) {
        var onholdCash = {
            cardType: "cash",
            paymentGateWayID: "null",
            behaviour: "onHold",
            cardDisplayName: labels["lblPaymentOnHold"],
            cssClassName: "CASH",
            i18nMsgKey: null,
            status: "ACT"
        };
        cardsInfo.push(onholdCash);
    }

    if (!UI_Payment.isCardPanelBuild) {
        var cardImage = "";
        var cardCss = "";
        for (var y = 0; y < cardsInfo.length; y++) {
            cardCss = setClassName(cardsInfo[y].cssClassName);
            if (cardCss != "") {
                cardImage = "<span class='" + cardCss + "'></span>";
            } else {
                cardImage = "";
            }

            if ((!(UI_Payment.isModifyAncillary || UI_Payment.modifySegment || UI_Payment.ispaymentforOHD) || cardsInfo[y].modicifationAllowed || UI_Payment.payFortOfflinePayment || UI_Payment.payFortPayATHomeOfflinePayment) && ((cardCss != "FATOURATI") || cardsInfo[y].withinOnholdPeriod) && ((cardCss != "QIWI_OFFLINE") || cardsInfo[y].withinOnholdPeriod)) {

                $("#listCard").append("<li style='display: inline;'><div class='card-img'>" + cardImage + "</div><span class='card-desc'>" +
                    "<input type='RADIO' class='noBorder' name='cardRadio' id='cardRadio_" + cardsInfo[y].cardType + "' value='" + cardsInfo[y].cardType + ":" + cardsInfo[y].paymentGateWayID + ":" + cardsInfo[y].behaviour + ":" + cardsInfo[y].cardDisplayName + "'>" +
                    "<label class='fntBold'> " + cardsInfo[y].cardDisplayName + "</label></span></li>");
            }

        };

        $('input[name="cardRadio"]').click(function(e) {
            var isCashPayWithPromo = UI_Payment.isCashPaymentWithPromotion(this);
            var isCashPayWithLoyalty = UI_Payment.isCashPaymentWithLoyalty(this);
            var isCashPayWithVoucher = UI_Payment.isCashPaymentWithVoucher(this);
            var isPayFortPayWithLoyalty = UI_Payment.isPayFortPaymentWithLoyalty(this);
            var isPayFortPayWithMashreq = UI_Payment.isPayFortPaymentWithMashreq(this);
            if (isCashPayWithPromo || isCashPayWithLoyalty || isPayFortPayWithLoyalty || isPayFortPayWithMashreq || isCashPayWithVoucher) {
                var labels = UI_Container.getLabels();
                var element = this;
                if (isCashPayWithPromo) {
                    /*jConfirm(UI_Container.labels.lblPromoRemovalMessage, UI_Container.labels.lblAlertConfirm, function(res){
                        if(!res){
                            $("#cardDetailPannel").hide();
                            $("#paymentInfoPanel").hide();
                            $("#onHoldInfoPanel").hide();
                            $(element).attr('checked', false);
                            return false;
                        } else {*/
                    UI_Payment.removeAppliedPromotionInfo();
                    $("#paymentInfoPanel").hide();
                    $("#onHoldInfoPanel").show();
                    $("#cardDetailPannel").hide();
                    UI_Payment.paymentMethodChange(this);
                    /* }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);*/
                }

                if (isCashPayWithLoyalty || $('#payByLoyalty_yes').is(':checked')) {
                    jConfirm(UI_Container.labels.lblLoyaltyRemovalMessage, UI_Container.labels.lblAlertConfirm, function(res) {
                        if (!res) {
                            $("#cardDetailPannel").hide();
                            $("#paymentInfoPanel").hide();
                            $("#onHoldInfoPanel").hide();
                            $(element).attr('checked', false);
                            return false;
                        } else {
                            UI_Payment.resetLoyaltyRedeemOption();
                            UI_Payment.paymentMethodChange(this);
                        }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
                }
                
                if (isCashPayWithVoucher || $('#payByVoucher_yes').is(':checked')) {
                    jConfirm(UI_Container.labels.lblVoucherRemovalMessage, UI_Container.labels.lblAlertConfirm, function(res) {
                        if (!res) {
                        	$("#cardDetailPannel").hide();
                            $("#paymentInfoPanel").hide();
                            $("#onHoldInfoPanel").hide();
                            $(element).attr('checked', false);
                            return false;
                        } else {
                            UI_Payment.resetVoucherRedeemOption();
                            UI_Payment.paymentMethodChange(this);
                        }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
                }
                if (isPayFortPayWithLoyalty && $('#payByLoyalty_yes').is(':checked')) {
                    jConfirm(UI_Container.labels.lblLoyaltyRemovalMessageForPayFort, UI_Container.labels.lblAlertConfirm, function(res) {
                        if (!res) {
                            $("#cardDetailPannel").hide();
                            $("#paymentInfoPanel").hide();
                            $("#onHoldInfoPanel").hide();
                            $(element).attr('checked', false);
                        	UI_Payment.paymentGatewayID = null;
                        	UI_Payment.paymentGatewayTypeCode = 'undefined';
                            return false;
                        } else {
                            UI_Payment.resetLoyaltyRedeemOption();
                            UI_Payment.paymentMethodChange(this);
                        }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
                }
                
                if(isPayFortPayWithMashreq && UI_Payment.mashreqPayOption.val() == 'part' ){
                    jConfirm(UI_Container.labels.lblMashreqRemovalMessageForPayFort, UI_Container.labels.lblAlertConfirm, function(res) {
                        if (!res) {
                            $("#cardDetailPannel").hide();
                            $("#paymentInfoPanel").hide();
                            $("#onHoldInfoPanel").hide();
                            $(element).attr('checked', false);
                        	UI_Payment.paymentGatewayID = null;
                        	UI_Payment.paymentGatewayTypeCode = 'undefined';
                            return false;
                        } else {
                        	clickLoyalty("none");
                            $("#btnRecalculate").disableNav();
                            $('#loyaltySetteledAmount').attr('disabled', false);
                            if($('#payByLoyalty_yes').is(':checked')){
                            	 UI_Payment.resetLoyaltyRedeemOption();
                            }
                            UI_Payment.paymentMethodChange(this);
                            
                        }
                    }, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
                }

            } else {
                UI_Payment.paymentMethodChange(this);
            }
        });
    }
    UI_Payment.displayCardDetailPanel();
}

UI_Payment.skipPromotion = function() {
    var fareQuoteCopy = $.airutil.dom.cloneObject(UI_Payment.fareQuoteOriginal);
    if (fareQuoteCopy.deductFromTotal) {
        var promoDiscount = parseFloat(fareQuoteCopy.inBaseCurr.totalIbePromoDiscount);
        var promoDiscountInSelCurr = parseFloat(fareQuoteCopy.inSelectedCurr.totalIbePromoDiscount);
        var totalPrice = parseFloat(fareQuoteCopy.inBaseCurr.totalPrice)
        var totalPayable = parseFloat(fareQuoteCopy.inBaseCurr.totalPayable);
        var totalPriceInSelCurr = parseFloat(fareQuoteCopy.inSelectedCurr.selectedtotalPrice);
        var totalPayableInSelCurr = parseFloat(fareQuoteCopy.inSelectedCurr.selectedTotalPayable)

        totalPrice += promoDiscount;
        totalPayable += promoDiscount;
        totalPriceInSelCurr += promoDiscountInSelCurr;
        totalPayableInSelCurr += promoDiscountInSelCurr;

        totalPrice = $.airutil.format.currency(totalPrice);
        totalPayable = $.airutil.format.currency(totalPayable);
        totalPriceInSelCurr = $.airutil.format.currency(totalPriceInSelCurr);
        totalPayableInSelCurr = $.airutil.format.currency(totalPayableInSelCurr);

        fareQuoteCopy.inBaseCurr.totalPrice = totalPrice;
        fareQuoteCopy.inBaseCurr.totalPayable = totalPayable;
        fareQuoteCopy.totalPrice = totalPrice;
        fareQuoteCopy.totalPayable = totalPayable;
        fareQuoteCopy.inSelectedCurr.selectedtotalPrice = totalPriceInSelCurr;
        fareQuoteCopy.inSelectedCurr.selectedTotalPayable = totalPayableInSelCurr;
        fareQuoteCopy.selectedtotalPrice = totalPriceInSelCurr;
        fareQuoteCopy.selectedTotalPayable = totalPayableInSelCurr;

        if (UI_Payment.tnxFeeInfo != null) {
            var tnxFeeObj = $.airutil.dom.cloneObject(UI_Payment.tnxFeeInfoOriginal);
            var tnxTotPrice = parseFloat(tnxFeeObj.totalPrice);
            var tnxTotPriceInSelCurr = parseFloat(tnxFeeObj.totalPriceInSel);
            var tnxTotPayable = parseFloat(tnxFeeObj.totalPayable);
            var tnxTotPayableInSelCurr = parseFloat(tnxFeeObj.totalPayableInSel);

            tnxTotPrice += promoDiscount;
            tnxTotPayable += promoDiscount;
            tnxTotPriceInSelCurr += promoDiscountInSelCurr;
            tnxTotPayableInSelCurr += promoDiscountInSelCurr;

            UI_Payment.tnxFeeInfo.totalPrice = $.airutil.format.currency(tnxTotPrice);
            UI_Payment.tnxFeeInfo.totalPriceInSel = $.airutil.format.currency(tnxTotPriceInSelCurr);
            UI_Payment.tnxFeeInfo.totalPayable = $.airutil.format.currency(tnxTotPayable);
            UI_Payment.tnxFeeInfo.totalPayableInSel = $.airutil.format.currency(tnxTotPayableInSelCurr);
        }
    }
    fareQuoteCopy.inBaseCurr.totalIbePromoDiscount = "0.00";
    fareQuoteCopy.inSelectedCurr.totalSelectedIbePromoDiscount = "0.00";
    fareQuoteCopy.totalIbePromoDiscount = "0.00";
    fareQuoteCopy.totalSelectedIbePromoDiscount = "0.00";
    fareQuoteCopy.hasPromoDiscount = false;


    return fareQuoteCopy;
}

UI_Payment.skipPromotionIfCashPayment = function(element) {
    if (UI_Payment.isCashPaymentWithPromotion(element)) {
        return UI_Payment.skipPromotion();
    }

    return UI_Payment.fareQuoteOriginal;
}

UI_Payment.removeAppliedPromotionInfo = function() {
    if (UI_Payment.fareQuoteOriginal.deductFromTotal) {
        var fareQuoteWithoutPromo = UI_Payment.skipPromotion();

        UI_Payment.setActualFareQuoteValues(fareQuoteWithoutPromo);
        UI_Container.updateFareQuote(fareQuoteWithoutPromo);
        UI_Payment.showPaymentBreakDown(fareQuoteWithoutPromo);
        UI_Payment.stylePriceBreakDown();
        UI_Payment.hideAddServiceSummary();
    }
}

UI_Payment.isCashPaymentSelected = function(element) {
    var selectCardVal = $(element).val();
    if (selectCardVal == null || selectCardVal == "") {
        return false;
    }
    var selectCardValArr = selectCardVal.split(":");
    var paymentGatewayID = selectCardValArr[1];

    if (paymentGatewayID == "null") {
        return true;
    }

    return false;
}

UI_Payment.isPayFortPaymentSelected = function(element) {
    var selectCardVal = $(element).val();
    if (selectCardVal == null || selectCardVal == "") {
        return false;
    }
    var selectCardValArr = selectCardVal.split(":");
    var paymentGatewayDisplayName = selectCardValArr[3];

    if (paymentGatewayDisplayName.toUpperCase().indexOf('PAY@') >= 0) {
        return true;
    }

    return false;
}

UI_Payment.isPayFortPaymentSelectedBeforeLoyalty = function() {

    if (UI_Payment.paymentOptionsInitialized && UI_Payment.paymentGatewayID != null && UI_Payment.paymentGatewayID != "") {
        if(UI_Payment.selectedPaymentMethodValStr != null && UI_Payment.selectedPaymentMethodValStr != ""){
        	var selectCardValArr = UI_Payment.selectedPaymentMethodValStr.split(":");
        	var paymentGatewayDisplayName = selectCardValArr[3];
        	if (paymentGatewayDisplayName.toUpperCase().indexOf('PAY@') >= 0) {
                return true;
            }
        }
    }
    return false;
}

UI_Payment.isCashPaymentWithPromotion = function(element) {
    var discountInfo = UI_Container.getDiscountInfo();
    if (discountInfo != null && discountInfo.promotionId != null &&
        !(UI_Container.isModifySegment() || UI_Container.isRequoteFlow()) && UI_Payment.isCashPaymentSelected(element)) {
        return true;
    }

    return false;
}

UI_Payment.isCashPaymentWithLoyalty = function(element) {
    if (UI_Container.isLoyaltyManagmentEnabled() && UI_Payment.isLoyaltyRedeemed && UI_Payment.isCashPaymentSelected(element)) {
        return true;
    }

    return false;
}


UI_Payment.isCashPaymentWithVoucher = function(element) {
    if (UI_Payment.isVoucherRedeemed && UI_Payment.isCashPaymentSelected(element)) {
        return true;
    }
    return false;
}


UI_Payment.isPayFortPaymentWithLoyalty = function(element) {
    if (UI_Container.isLoyaltyManagmentEnabled() && UI_Payment.isLoyaltyRedeemed && UI_Payment.isPayFortPaymentSelected(element)) {
        return true;
    }

    return false;
}

UI_Payment.isPayFortPaymentWithMashreq = function(element) {
    if (UI_Payment.loyalty.enabled && UI_Payment.isPayFortPaymentSelected(element) && UI_Payment.mashreqPayOption != null && UI_Payment.mashreqPayOption.val() == 'part') {
        return true;
    }

    return false;
}

UI_Payment.stylePriceBreakDown = function() {
    var index = 0;
    /* styling alternative rows for price breakdown table */
    $('#tblPriceBreakdown').find('tr').each(function(i) {
        if ($(this).css('display') != 'none') {
            $(this).find('td').removeClass('rowColorAlternate');
            $(this).find('td').removeClass('rowColor');
            if (index % 2 == 0) {
                $(this).find('td').addClass('rowColorAlternate');
            } else {
                $(this).find('td').removeClass('rowColor');
            }
            index++;
        }
    });
}

function clickLoyalty(option) {
    $('input[name="loyaltyOption"]').each(function(index, val) {
        if ($(val).val() == option) {
            $(this).attr('checked', true);
        }
    });
    UI_Payment.loyaltyOptionChanged(false);
}

UI_Payment.showLoyalty = function(loyalty, fareQuote) {
	if(UI_Payment.integrateMashreqWithLMSEnabled){
		$('#tblLoyalty').hide();
        $('#btnRecalculate').hide();
	}else if (loyalty.enabled) {
        $('#spnloyaltyCredit').html(loyalty.creditAmount);
        $('#tblLoyalty').show();
        $('#btnRecalculate').show();
        $('#btnRecalculate').disableNav();
        var totalPrice = parseFloat(fareQuote.inBaseCurr.totalPrice);
        var totalCredit = parseFloat(loyalty.creditAmount);
        $('#loyaltySetteledAmount').attr('disabled', true);
        if (totalPrice <= totalCredit) {
            $('input[name="loyaltyOption"]').each(function(index, val) {
                if ($(val).val() == 'total') {
                    $(this).attr('checked', true);
                    $(this).prop('disabled', false);
                } else {
                    $(this).attr('checked', false);
                }
            });
        } else if (totalCredit > 0) {
            $('#loyaltySetteledAmount').prop('disabled', false);
            $('input[name="loyaltyOption"]').each(function(index, val) {
                if ($(val).val() == 'part') {
                    $(this).attr('checked', true);
                } else {
                    $(this).attr('checked', false);
                    if ($(val).val() == 'total') {
                        $(this).attr('disabled', true);
                        $('#trLoyalPayFull').find("input").attr('disabled', 'disabled');
                    }
                }
            });
        } else {
            $('#loyaltySetteledAmount').attr('disabled', true);
            $('input[name="loyaltyOption"]').each(function(index, val) {
                if ($(val).val() == 'total') {
                    $(this).attr('checked', false);
                    $(this).attr('disabled', true);
                } else if ($(val).val() == 'part') {
                    $(this).attr('checked', false);
                    $(this).attr('disabled', true);
                } else if ($(val).val() == 'none') {
                    $(this).attr('checked', true);
                }
            });
        }
        
        if(UI_Container.isLoyaltyManagmentEnabled() && UI_commonSystem.lmsDetails != null &&
        		UI_commonSystem.lmsDetails.emailId != null && UI_commonSystem.lmsDetails.emailId != ''){
        	$('input[name="loyaltyOption"]').each(function(index, val) {
        		if ($(val).val() == 'none') {
                    $(this).attr('checked', true);
                }
        	});
        }
        
        UI_Payment.loyaltyOptionChanged(false);
    } else {
        $('#tblLoyalty').hide();
        $('#btnRecalculate').hide();
    }
}

UI_Payment.pgSelected = function() {
    var pgId = UI_Payment.paymentGatewayID;
    if (pgId != null) {
        for (var pl = 0; pl < UI_Payment.objPGS.length; pl++) {
            if (UI_Payment.objPGS[pl].paymentGateway == pgId) {
                UI_Payment.fillPaymentGW(UI_Payment.objPGS[pl]);
                break;
            }
        }
    } else if (UI_Payment.brokerType == "onHold") {
        $("#paymentInfoPanel").hide();
        $("#onHoldInfoPanel").show();
        $("#cardDetailPannel").hide();
        if (UI_Payment.onHoldImageCapcha) {
            $("#imageCap").show();
            $("#imgCPT").unbind("click").bind("click", function() {
                UI_Payment.refresh();
            });
        } else {
            $("#imageCap").hide();
        }
    }
    //chnage payment gateway view
    if ($("#viewPaymentInIframe").val() == "true" && UI_Container.useIframeInPostingParams) {
        UI_Payment.changeExternalBehavior();
    }
}

UI_Payment.loadFlightdata = function(segments) {
    $("#flightTemplate").iterateTemplete({
        templeteName: "flightTemplate",
        data: segments,
        dtoName: "flightSegments"
    });
}

UI_Payment.loadFlightGirddata = function(segments) {
    $("#departueFlight").iterateTemplete({
        templeteName: "departueFlight",
        data: segments,
        dtoName: "flightSegments"
    });
}

UI_Payment.loadFlightRetrunGirddata = function(segments) {
    $("#returnFlight").iterateTemplete({
        templeteName: "returnFlight",
        data: segments,
        dtoName: "flightSegments"
    });
}

UI_Payment.loadPaymentGateway = function(securePath) {
    UI_Payment.disableButtons();
    //$("#divLoadMsg").show();
    UI_Container.showLoading();
    $("#lblProgressBar").text(strMsgProgressBar);
    $('#jsonInsurance').val($.toJSON(UI_Container.getInsurance()));
    $('#jsonPaxWiseAnci').val($.toJSON(UI_Container.getPaxWiseAnci()));
    $("#totalSegmentCount").val(UI_commonSystem.totalSegCount);
    $("#jsonPaxInfo").val($.toJSON(UI_Container.getPaxArray()));
    $("#frmReservation").attr('action',
        securePath + 'interlinePostCardDetails.action');
    $("#frmReservation").submit();
}

UI_Payment.fillPaymentGW = function(pg) {
    // TODO need to change the pg according to the selected pg
    UI_Payment.buildCardDetailPanel(pg);
    if (pg.payCurrency != null) {
        var farQCurrency = UI_Container.getCurrencies().base;
        $("#payCurrency").val(pg.payCurrency);
        $("#providerName").val(pg.providerName);
        $("#providerCode").val(pg.providerCode);
        $("#description").val(pg.description);
        $("#paymentGateway").val(pg.paymentGateway);
        $("#payCurrencyAmount").val(pg.payCurrencyAmount);
        $("#brokerType").val(UI_Payment.brokerType);
        $("#switchToExternalURL").val(pg.switchToExternalURL);
        $("#viewPaymentInIframe").val(pg.viewPaymentInIframe);
        $("#onholdReleaseTime").val(pg.onholdReleaseTime);

        if (pg.switchToExternalURL) {
            UI_Container.useIframeInPostingParams = false;
        }

        $("#lblMsgReviewPayment").text(
            strPaymentInfo1 + strPaymentInfo2 + " " + pg.payCurrency + strPaymentInfo3);
        if (UI_Payment.objAllPGS.length == 0 && pg.payCurrency != farQCurrency) {
            $("#paymentSelectedCurrCode").show();
            $("#lblTotalDueAmountSelected").text(pg.payCurrencyAmount);
            $('#lblSelectedCurrCode').text(pg.payCurrency)
            return;
        }
        if (pg.payCurrency == farQCurrency) {
            $("#paymentSelectedCurrCode").hide();
        }

        for (var pl = 0; pl < UI_Payment.objAllPGS.length; pl++) {
            if (pg.paymentGateway == UI_Payment.objAllPGS[pl].paymentGateway) {
                $("#payCurrency").val(UI_Payment.objAllPGS[pl].payCurrency);
                if (farQCurrency != UI_Payment.objAllPGS[pl].payCurrency) {
                    $("#paymentSelectedCurrCode").show();
                    $("#lblTotalDueAmountSelected").text(
                        UI_Payment.objAllPGS[pl].payCurrencyAmount);
                    $('#lblSelectedCurrCode').text(
                        UI_Payment.objAllPGS[pl].payCurrency)
                }
                $("#lblMsgReviewPayment").text(
                    strPaymentInfo1 + strPaymentInfo2 + " " + UI_Payment.objAllPGS[pl].payCurrency + strPaymentInfo3);
                return;
            }
        }
        if (UI_Payment.objAllPGS != null && UI_Payment.objAllPGS != "" && UI_Payment.objAllPGS.length != 0) {
            $("#paymentSelectedCurrCode").hide();
        }
    }
}

// Set Flight logo using flight data
UI_Payment.setImage = function(flightSegments, carrierImage, flightData) {
    for (var i = 0; i < flightData.length; i++) {
        $('#' + flightSegments + '\\[' + i + '\\]\\.' + carrierImage).attr({
            src: flightData[i].carrierImagePath,
            title: flightData[i].carrierCode,
            alt: flightData[i].carrierCode
        });
    }
}

UI_Payment.disableButtons = function() {
    $("#btnPurchase").attr("disabled", "disabled").addClass(
        "ButtonDefaultDisable");
    $('#btnPurchaseOnAnci').attr("disabled", "disabled").addClass(
        "ButtonDefaultDisable")
    $("#btnCancel").attr("disabled", "disabled").addClass(
        "ButtonDefaultDisable");
}
var payType = anciConfig.onHoldvisibility;
UI_Payment.buildPaymentOption = function(accessPoint, onHoldEnable, onHoldDisTime, restrictedOnHoldBC) {

    if (onHoldEnable == true) {
        if (UI_Payment.objPGS != null) {
            if (!UI_Payment.paymentOptionsInitialized) {
                $('#selPayType1').attr('checked', true);
                $('#selPayType').attr('checked', false);
                UI_Payment.hideCCPanel();
                $("#paymentType").val('NORMAL');
                $("#trOnholdDuration").hide();
            }
        } else {
            $('#selPayType').attr('checked', true);
            $("#paymentType").val('ONHOLD');
            $("#tdPayByCC").hide();
            $("#customCCPanel").hide();
        }

        if (payType == 1) {
            $("#spnPayType").show();
            //$("#onHoldDisplayTime").text(onHoldDisTime);
            if ($("#lblOnHoldState").text().indexOf("{0}") > -1) {
                $("#lblOnHoldState").text($("#lblOnHoldState").text().replace("{0}", onHoldDisTime));
            }
            if (restrictedOnHoldBC) {
                $("#paymentType").val('NORMAL');
                $('#selPayType').attr('disabled', 'disabled');
                $("#trOnholdDuration").hide();
                UI_Payment.payTypeChanged(2);
                $('#selPayType1').attr('checked', true);
                $('#selPayType').attr('checked', false);
            }
        } else {
            $("#paymentType").val('NORMAL');
            $('#selPayType1').attr('checked', true);
            $('#selPayType').attr('checked', false);
        }
    } else {
        $("#trOnholdDuration").hide();
        if (payType == 1) {
            $("#spnPayType").show();
        }
        $("#paymentType").val('NORMAL');
        $('#selPayType1').attr('checked', true);
        $("#tdPayLater").hide();
        $("#tdPayByCC").attr('align', 'left');
    }
    UI_Payment.paymentOptionsInitialized = true;
}

UI_Payment.getPaymentType = function() {
    return $("#paymentType").val();
}

UI_Payment.payTypeChanged = function(params) {
    payType = params;
    if(params == 1){
    	$('input:radio[name=cardRadio]:checked').attr("checked", false);
    	$("#paymentSelectedCurrCode").hide();
    	$("#selPG").hide();
    	$("#lblPG").hide();
    }
    var labels = UI_Container.getLabels();
    if (params != "" && params == 1 && $("#selPayType").val() == 'ONHOLD') {
        $("#paymentType").val('ONHOLD');
        $("#btnPurchaseOnAnci").text(labels['btnContinue']);
        $("#trOnholdDuration").show();
    } else if (params == 2 && $("#selPayType1").val() == 'NORMAL') {
        $("#btnPurchaseOnAnci").text(labels['btnPurchase']);
        $("#paymentType").val('NORMAL');
        $("#trOnholdDuration").hide();
        $("#selPG").show();
        $("#lblPG").show();
    }
    UI_Payment.loadPaymentdata();
}

UI_Payment.loyaltyOptionValue = function() {
    return $('input[name="loyaltyOption"]:checked').val();
}

UI_Payment.noCardPaymentTobeCollected = function() {
    var loyaltyOption = UI_Payment.loyaltyOptionValue();
    if (loyaltyOption == 'total' || UI_Payment.skipCardTypePayment) {
        return true;
    }
    return false;
}

UI_Payment.buildCardDetailPanel = function(paymentGatyeway) {
    var cardTypes = null;

    // Set Payment Gateway type
    if (UI_Payment.brokerType != null && UI_Payment.brokerType == "internal-external") {
        UI_Container.isExternalPaymentGateway = false;
    } else {
        UI_Container.isExternalPaymentGateway = true;
    }

    if (UI_Payment.isCardPay() && !UI_Payment.noCardPaymentTobeCollected() && !UI_Payment.isNoPay) {
        $("#divCardInfoPannel").show();
        if (UI_Payment.brokerType == "internal-external") {
            $("#paymentInfoPanel").hide();
            $("#onHoldInfoPanel").hide();
            $("#cardDetailPannel").show();
            UI_Payment.clearCardDetails();
            UI_Payment.paymentGatewayProviderCd = paymentGatyeway.providerCode;
            if (paymentGatyeway.providerCode != "PAYPAL") {
                $("#extraCardDetailForPayPalPannel").hide();
            } else {
                $("#extraCardDetailForPayPalPannel").show();
            }
            UI_Payment.clearErrorMessages();
            UI_Payment.removeClasses();
            $("#cardTypePics").empty();
            UI_Payment.setErrorData();
            $("#cardType").focus();
        } else if (UI_Payment.brokerType == "onHold") {
            $("#paymentInfoPanel").hide();
            $("#onHoldInfoPanel").show();
            $("#cardDetailPannel").hide();
        } else {
            $("#paymentInfoPanel").show();
            $("#onHoldInfoPanel").hide();
            $("#cardDetailPannel").hide();
            $("#paymentInfoPanel").find("#lblPaymentGeneralInfo3").html($("#lblPaymentGeneralInfo3").text());
        }

        if (paymentGatyeway.providerCode == "QIWI") {
        	
            $("#payfortDetailsPanel").hide();
            $("#pgwCommonDetailsPanel").hide();
        	  var qiwiMode = $('input:radio[name=cardRadio]:checked').parent().parent().find('span')[0]; //.className;

              if (typeof qiwiMode !== 'undefined') {
                  qiwiMode = qiwiMode.className;

                  if (qiwiMode !== 'QIWI_OFFLINE') {
                      // set labels for Qiwi online mode
                      if (UI_Container.labels.lblPaymentInfoQiwiHeading) {
                          $("#lblPaymentInfoHeading").text(UI_Container.labels.lblPaymentInfoQiwiHeading);
                      }
                      if (UI_Container.labels.lblPaymentQiwiGeneralInfo1) {
                          $("#lblPaymentGeneralInfo1").text(UI_Container.labels.lblPaymentQiwiGeneralInfo1);
                      }
                      if (UI_Container.labels.lblPaymentQiwiGeneralInfo2) {
                          $("#lblPaymentGeneralInfo2").text(UI_Container.labels.lblPaymentQiwiGeneralInfo2);
                      }
                  } else {
                      // set labels for Qiwi offline mode
                      if (UI_Container.labels.lblPaymentInfoQiwiHeading) {
                          $("#lblPaymentInfoHeading").text(UI_Container.labels.lblPaymentInfoQiwiOfflineHeading);
                      }
                      if (UI_Container.labels.lblPaymentQiwiGeneralInfo1) {
                          $("#lblPaymentGeneralInfo1").text(UI_Container.labels.lblPaymentQiwiOfflineGeneralInfo1);
                      }
                      if (UI_Container.labels.lblPaymentQiwiGeneralInfo2) {
                          $("#lblPaymentGeneralInfo2").text(UI_Container.labels.lblPaymentQiwiOfflineGeneralInfo2);
                      }
                  }

              }
              $("#qiwiMobileNumber").val($("#hdnQiwiMobileNumber").val().replace('+', ''));
              $("#qiwiMobileNumber").focus(function() {
                  $(this).select();
              });
              $("#qiwiDetailsPanel").show();

              if (UI_Payment.onHoldEnable || UI_Payment.onHoldCreated) {
                  $("#radQiwiOfflinePayment").attr("disabled", false);
                  $("#lblQiwiOfflineOption").show();
                  $("#radQiwiOfflinePayment").show();
              } else {
                  $("#radQiwiOfflinePayment").attr("disabled", true);
                  $("#radQiwiOfflinePayment").hide();
                  $("#lblQiwiOfflineOption").hide();
              }

        } else if( paymentGatyeway.providerName == "PAYFORT_PAY_AT_STORE" && UI_Payment.payAtStorepostIndex === 0) {
        	// set labels for Qiwi offline mode
        	UI_Payment.payAtStorepostIndex = 1;
        	var data = {};
        	data['pnr'] = $("#pnr").val();
        	var labelTxnFee = $('#valTxnFee');
        	data['valTxnFee'] = labelTxnFee.text();
        	if ($("#groupPNR").val() != null) {
        		data['groupPNR'] = $("#groupPNR").val();
        	}
        	$("#dummaryFrm").ajaxSubmit({url:"handleInterlinePayFort!checkPayFort.action",data:data, dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success:  UI_Payment.loadVoucherCheckForPayAtStore, error:UI_commonSystem.setErrorStatus});	

        	
        } else if( paymentGatyeway.providerName == "PAYFORT_PAY_AT_HOME" && UI_Payment.payAtHomepostIndex === 0) {
        	// set labels for Qiwi offline mode
        	UI_Payment.payAtHomepostIndex = 1;
        	var data = {};
        	data['pnr'] = $("#pnr").val();
        	var labelTxnFee = $('#valTxnFee');
        	data['valTxnFee'] = labelTxnFee.text();
        	if ($("#groupPNR").val() != null) {
        		data['groupPNR'] = $("#groupPNR").val();
        	}
        	$("#dummaryFrm").ajaxSubmit({url:"handleInterlinePayFort!checkPayFort.action",data:data, dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success:  UI_Payment.loadVoucherCheckForPayAtHome, error:UI_commonSystem.setErrorStatus});	

        	
        } else if (paymentGatyeway.providerCode === "MPG" ) {
        	if (UI_Container.labels.lblPaymentInfoMPGHeading) {
                $("#lblPaymentInfoHeading").html(UI_Container.labels.lblPaymentInfoMPGHeading);
        	}
        } else {
        	
            $("#qiwiDetailsPanel").hide();
            $("#payfortDetailsPanel").hide();
            $("#pgwCommonDetailsPanel").hide();
            $("#lblPaymentInfoHeading").text(UI_Container.labels.lblPaymentInfoHeading);
            $("#lblPaymentGeneralInfo1").text(UI_Container.labels.lblPaymentGeneralInfo1);
            $("#lblPaymentGeneralInfo2").text(UI_Container.labels.lblPaymentGeneralInfo2);
            
            // allowing common payment related info pannel show and pupulate data for Tap Gosell PGW
            if (paymentGatyeway.providerName == "TAP_GOSELL" ) {
            	$("#pgwCommonDetailsPanel").show();
				 if (UI_Payment.ispaymentforOHD || UI_Payment.isModifyAncillary || UI_Payment.modifySegment) {
					$("#cdPGWPaymentMobileNumber")
							.val(
									$("#pgwPaymentMobileNumber").val().replace(
											'+', ''));
					$("#cdPGWPaymentEmail").val($("#pgwPaymentEmail").val());
					$("#cdPGWPaymentCustomerName").val(
							$("#pgwPaymentCustomerName").val());
				} else {
					$("#cdPGWPaymentMobileNumber").val(
							$("#hdnQiwiMobileNumber").val().replace('+', ''));
					$("#cdPGWPaymentEmail").val($("#hdnPGWPaymentEmail").val());
					$("#cdPGWPaymentCustomerName").val(
							$("#hdnPGWPaymentCustomerName").val());
				}
            }
            
        }

    } else {
        $("#divCardInfoPannel").hide();
    }
    
    if(UI_Payment.payAtStorePreIndex === 1){
    	UI_Payment.payAtStorePreIndex = 0;
    	UI_Payment.payAtStorepostIndex = 0;
    } else if(UI_Payment.payAtStorePreIndex === 0){
    	UI_Payment.payAtStorePreIndex = 1;
    }
    
    if(UI_Payment.payAtHomePreIndex === 1){
    	UI_Payment.payAtHomePreIndex = 0;
    	UI_Payment.payAtHomepostIndex = 0;
    }else if(UI_Payment.payAtHomePreIndex === 0){
    	UI_Payment.payAtHomePreIndex = 1;
    }
}

UI_Payment.clearErrorMessages = function() {
    $("#lblError").html("");
    $("#lblErrorExternalIPG").html("");
}

UI_Payment.displayCCD = function(obj) {
    colseThisPopup = function() {
        UI_commonSystem.readOnly(false);
        $(".popuploader").hide();
    };
    colseThisPopup();
    UI_commonSystem.readOnly(true);
    var posobj = obj.position();
    $(".popuploader").css({
        "position": "absolute",
        "z-index": "1900",
        "top": posobj.top - 260,
        "left": posobj.left + 60
    });
    $(".closethisp").click(function() {
        colseThisPopup();
    });
    if ($("#cardType").val() != 3)
        $(".popupbody").html('<img src="../images/allCards_no_cache.gif" />');
    else
        $(".popupbody").html('<img src="../images/AmexCards_no_cache.gif" />');
    $(".popuploader").show();
    $("#page-mask").click(function() {
        colseThisPopup();
    });
}



UI_Payment.expiryDateChange = function() {
    $("#expiryDate").val($("#expiryMonth").val() + $("#expiryYear").val());
}

UI_Payment.displayAlert = function(msg) {
    jAlert(msg);
}

UI_Payment.buildMonths = function() {
    $("#expiryMonth").empty();
    var strMonths = strSelectEmpty;

    for (var i = 1; i <= 12; i++) {
        if (i < 10) {
            strMonths += "<option value='0" + i + "'> 0" + i + " </option>";
        } else {
            strMonths += "<option value='" + i + "'> " + i + " </option>";
        }
    }

    $("#expiryMonth").append(strMonths);
}

UI_Payment.buildYears = function() {
    $("#expiryYear").empty();
    var strYears = strSelectEmpty;
    var strSystemYear = UI_Container.systemDate.split("/")[2];

    if (strSystemYear == null || strSystemYear == "") {
        strSystemYear = (new Date()).getFullYear();
    }

    for (var i = Number(strSystemYear); i < Number(strSystemYear) + 15; i++) {
        strYears += "<option value='" + i.toString().substring(2) + "'> " + i + " </option>";
    }

    $("#expiryYear").append(strYears);
}

UI_Payment.addErrorClass = function(id) {
    $(id).addClass("errorControl");
}

UI_Payment.removeErrorClass = function(id) {
    $(id).removeClass("errorControl");
}

UI_Payment.removeClasses = function() {
    $("#cardDetailPannel").find(".errorControl").each(function(intIndex) {
        $("#" + this.id).removeClass("errorControl");
    });
}

UI_Payment.wrapLi = function(message) {
    return "<li>" + message + "</li>"
}

UI_Payment.setErrorData = function() {
    var errorMsgs = UI_Payment.cardErrorInfo;
    if (errorMsgs != null) {
        jsonCEObj = [{
            id: "#cardCVV",
            desc: errorMsgs.reqCVV,
            required: true
        }, {
            id: "#expiryYear",
            desc: errorMsgs.reqExpiryYear,
            required: true
        }, {
            id: "#expiryMonth",
            desc: errorMsgs.reqExpiryMonth,
            required: true
        }, {
            id: "#cardNumber",
            desc: errorMsgs.reqCardNo,
            required: true
        }, {
            id: "#cardHoldersName",
            desc: errorMsgs.reqCardHolName,
            required: true
        }, {
            id: "#cardType",
            desc: errorMsgs.reqCardTYpe,
            required: true
        }, {
            id: "#billingAddress1",
            desc: errorMsgs.reqBillingAddress1,
            required: true
        }, {
            id: "#city",
            desc: errorMsgs.reqCity,
            required: true
        }, {
            id: "#postalCode",
            desc: errorMsgs.reqPostalCode,
            required: true
        }, {
            id: "#state",
            desc: errorMsgs.reqState,
            required: true
        }];
    }
}

UI_Payment.clearCardDetails = function() {
    clearTimeout(UI_Payment.timerPayment);
    UI_Container.isPaymentProcessing = false;

    $("#payByCreditCardPanel").show();
    $("#paymentButtonset").show();
    $("#intExtProcessing").hide();
    $("#cardHoldersName").val("");
    $("#cardNumber").val("");
    $("#expiryMonth").val("");
    $("#expiryYear").val("");
    $("#cardCVV").val("");
}

UI_Payment.enableLoyaltySelect = function() {
    $('input[name="loyaltyOption"]').each(function(index, val) {
        $(this).attr('disabled', false);
    });
    UI_Payment.showLoyaltyCardPaymentError(UI_Payment.loyalty, UI_Payment.fareQuoteOriginal);
}

UI_Payment.showLoyaltyCardPaymentError = function(loyalty, fareQuote) {
	if(UI_Payment.integrateMashreqWithLMSEnabled){
		$('#tblLoyalty').hide();
        $('#btnRecalculate').hide();
	} else if(loyalty.enabled) {
        $('#spnloyaltyCredit').html(loyalty.creditAmount);
        var totalPrice = parseFloat(fareQuote.inBaseCurr.totalPrice);
        var totalCredit = parseFloat(loyalty.creditAmount);
        $('#loyaltySetteledAmount').attr('disabled', true);
        if (totalPrice <= totalCredit) {
            $('input[name="loyaltyOption"]').each(function(index, val) {
                if ($(val).val() == 'total') {
                    $(this).prop('disabled', false);
                }
            });
        } else if (totalCredit > 0) {
            $('#loyaltySetteledAmount').prop('disabled', false);
            $('input[name="loyaltyOption"]').each(function(index, val) {
                if ($(val).val() == 'total') {
                    $(this).attr('disabled', true);
                    $('#trLoyalPayFull').find("input").attr('disabled', 'disabled');
                }
            });
        } else {
            $('#loyaltySetteledAmount').attr('disabled', true);
            $('input[name="loyaltyOption"]').each(function(index, val) {
                if ($(val).val() == 'total') {
                    $(this).attr('checked', false);
                    $(this).attr('disabled', true);
                } else if ($(val).val() == 'part') {
                    $(this).attr('checked', false);
                    $(this).attr('disabled', true);
                } else if ($(val).val() == 'none') {
                    $(this).attr('checked', true);
                }
            });
        }
    } else {
        $('#tblLoyalty').hide();
        $('#btnRecalculate').hide();
    }
}

UI_Payment.validateCvvCode = function(cardType, cvvCode) {

    var digits = 0;
    //TODO Set Card Type
    switch (cardType) {
        case '1': //MASTERCARD
        case 'EUROCARD': //EUROCARD
        case 'EUROCARD/MASTERCARD':
        case '2': //VISA
        case '5': //CB
        case 'DISCOVER':
        case '6': //PAYPAL
            digits = 3;
            break;
        case '3':
        case 'AMERICANEXPRESS':
        case 'AMERICAN EXPRESS':
            digits = 4;
            break;
        default:
            return false;
    }

    var regExp = new RegExp('[0-9]{' + digits + '}');
    return (cvvCode.length == digits && regExp.test(cvvCode))
}

UI_Payment.displayCardDetailPanel = function(resetPaymentInfoPanel) {
    var loyaltyOption = $('input[name="loyaltyOption"]:checked').val();
    if (UI_Payment.isCardPay() && loyaltyOption != 'total' && !UI_Payment.isNoPay) {
        UI_Payment.pgSelected();
        $("#divCardInfoPannel").show();
        if (UI_Payment.brokerType == "internal-external") {
            $("#paymentInfoPanel").hide();
            $("#onHoldInfoPanel").hide();
            $("#cardDetailPannel").show();
            if ($("#cardType").val() != '6') {
                $("#extraCardDetailForPayPalPannel").hide();
            } else {
                $("#extraCardDetailForPayPalPannel").show();
            }
        } else if (UI_Payment.brokerType == "external") {
            $("#paymentInfoPanel").show();
            $("#onHoldInfoPanel").hide();
            $("#cardDetailPannel").hide();
        } else if (UI_Payment.brokerType == "onHold") {
            $("#paymentInfoPanel").hide();
            $("#cardDetailPannel").hide();
            $("#onHoldInfoPanel").show();
        } else {
            $("#cardDetailPannel").hide();
            $("#paymentInfoPanel").hide();
            $("#onHoldInfoPanel").hide();
        }
    } else {
        $("#divCardInfoPannel").hide();
    }

    if (resetPaymentInfoPanel) {
        UI_Payment.resetPaymentInfoPanel(resetPaymentInfoPanel);
    }
}

UI_Payment.resetPaymentInfoPanel = function(resetPaymentInfoPanel) {
    //	if(UI_Payment.reloadedToRetry || ){
    //		return false;
    //	}

    if (!resetPaymentInfoPanel) {
        return false;
    }
    $('input:radio[name=cardRadio]:checked').attr('checked', false);
    $("#paymentType").val("");
    UI_Payment.paymentGatewayID = null;
    $("#paymentInfoPanel").hide();
    $("#onHoldInfoPanel").hide();
    $("#cardDetailPannel").hide();
}

UI_Payment.isCardPay = function() {
    var cardPay = true;
    if ($("input[name='selPayType']:checked").val() != "NORMAL") {
        cardPay = false;
    }
    return cardPay;
}


UI_Payment.isTransationCompleted = function() {
    if (UI_Container.isPaymentProcessing == true) {
        $.ajax({
            url: "manageBookingV2!reservationExit.action",
            cache: false,
            dataType: 'json',
            success: UI_Payment.isTransationCompletedProcess
        });
        return false;
    }
}

UI_Payment.isTransationCompletedProcess = function(response) {
    if (response != null && response.success == true) {
        var pnr = response.pnr;
        if (UI_Container.isPaymentProcessing == true && (pnr != null && pnr != "")) {
            $('#submitForm').attr('action', 'showLoadPage!loadConfimationPage.action');
            $("#sessionPNR").val(pnr);
            $("#submitForm").attr('target', '_self');
            $('#submitForm').submit();
        }
    } else {
        UI_Payment.timerPayment = setTimeout('UI_Payment.isTransationCompleted()', UI_Payment.payTimeGapShort);
    }
}

UI_Payment.buildCardInitPannel = function() {
    if (!UI_Payment.isCardPanelBuild) {
        UI_Payment.buildMonths();
        UI_Payment.buildYears();
        UI_Payment.isCardPanelBuild = true;
    }
    $("#cardDetailPannel").hide();
}

UI_Payment.paymentMethodChange = function(params) {

    if (UI_Payment.loyalty.enabled) {
        if (!UI_Payment.validateLoyaltyOption()) {
            UI_Payment.resetPaymentInfoPanel(true);
            return false;
        }
    }

    UI_Container.showLoading();
    if(UI_Payment.fareQuoteOriginal.inSelectedCurr.currency == ""){
    	$("#paymentSelectedCurrCode").hide();
    }
    var element = $('input:radio[name=cardRadio]:checked');
    var selectCardVal = element.val();
    UI_Payment.selectedPaymentMethodValStr = selectCardVal;

    if (selectCardVal == null || selectCardVal == "") {
        alert("Can not process payment");
        return;
    }
    var selectCardValArr = selectCardVal.split(":");
    var cardTypeID = selectCardValArr[0];
    var paymentGatewayID = selectCardValArr[1];
    var paymentGatewayMode = selectCardValArr[2];
    var paymentMethod = selectCardValArr[3];

    UI_Payment.brokerType = paymentGatewayMode;
    if (paymentGatewayID == "null") {
        UI_Payment.paymentGatewayID = null;
        $("#paymentType").val('ONHOLD');
        $("#onHoldDisplayTime1").text(UI_Payment.onHoldDisplayTime);
    } else {
        $("#paymentType").val('NORMAL');
        UI_Payment.paymentGatewayID = paymentGatewayID;
        UI_Payment.paymentGatewayTypeCode = paymentMethod;
    }
    $("#cardType").val(cardTypeID);
    $("#paymentMethod").val(paymentMethod);

    //reload fare quote with payment gateway wise charges
    var fareQuoteIPGCharges = true;
    if (typeof params !== 'undefined' && params['fareQuotePgwCharge'] == false) {
        fareQuoteIPGCharges = false;
    }

    if (fareQuoteIPGCharges) {
        UI_Payment.getFareQuoteWithIPGCharge(paymentGatewayID);
    }

    UI_Payment.pgSelected();

    if (paymentGatewayID == "null" && UI_Payment.loyalty.enabled) {
        clickLoyalty("none");
        $("#btnRecalculate").disableNav();
        $('#loyaltySetteledAmount').attr('disabled', false);
    } else if (paymentGatewayID != "null" && UI_Payment.loyalty.enabled) {
        $("#btnRecalculate").enableNav();
        $('#loyaltySetteledAmount').prop('disabled', false);
    }

    var effectiveFareQuote = UI_Payment.skipPromotionIfCashPayment(element);

    UI_Payment.setActualFareQuoteValues(effectiveFareQuote);

    UI_Container.updateFareQuote(effectiveFareQuote);
    UI_Payment.showPaymentBreakDown(effectiveFareQuote);
    UI_Payment.stylePriceBreakDown();
    UI_Payment.hideAddServiceSummary();

    UI_Container.hideLoading();
}

/*
 * Fare Quote with payment gateway wise charges
 * 
 * */
UI_Payment.getFareQuoteWithIPGCharge = function(paymentGatewayID) {

    UI_Payment.clearErrorMessages();
    //UI_Payment.hidePaymentPage();
    $('#btnPurchase').disableNav();
    $('#btnPurchaseOnAnci').disableNav();
    $('#btnRecalculate').disableNav();
    UI_Container.showLoading();
    var data = {};
    data['loyaltyPayOption'] = $('input[name="loyaltyOption"]:checked').val();
    data['loyaltyCreditAmount'] = $('#loyaltySetteledAmount').val();
    data['paymentGatewayID'] = paymentGatewayID;
    data['resPnr'] = $("#pnr").val();
    data['freeServiceDiscount'] = UI_Container.getFreeServiceDiscount();
    data['insurance'] = $.toJSON(UI_Container.getInsurance());

    if (!UI_Container.paymentRetry) {
        data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
        if (UI_Container.isModifySegment()) {
            if (UI_Container.isAllowInfantInsurance) {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(true));
            } else {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(false));
            }
        }
    }
    var contactDetails = UI_Container.getContactDetails();
    if (contactDetails != null) {
        data['contactEmail'] = contactDetails.emailAddress;
    }
    if ($("#cardRadio_cash").is(":checked")) {
    	data['cashPayment'] = true;
    } else {
    	data['cashPayment'] = false;
    }

    $("#frmReservation").ajaxSubmit({
        dataType: 'json',
        processData: false,
        data: data,
        async: false,
        url: "interlinePayment.action",
        beforeSubmit: UI_Container.showLoading,
        success: UI_Payment.applyFareQuoteWithPaymentGatewayWiseCharges,
        error: UI_commonSystem.setErrorStatus
    });
    return true;
}

UI_Payment.applyFareQuoteWithPaymentGatewayWiseCharges = function(response) {

    if (response != null && response.success == true) {
    
		// Modify Balance Summary
		//TODO Merge with Old Price break Down
		if (response.modifyBalanceDisplayPayment == true) {	
			$("#tblPriceBreakdown").hide();
			UI_ModifyConfirm.displaySummary(response.updateCharge.chargesList);	
			$("#modifyBal").show();
		}	
		UI_Payment.objPGS = response.paymentGateways;
		UI_Payment.objAllPGS = response.allPaymentGateways;
		UI_Payment.isNoPay = response.hasNopay;	
		UI_Payment.accessPoint = response.accessPoint;
		UI_Payment.onHoldEnable = response.onHoldEnable;
		UI_Payment.onHoldCreated = response.onHoldCreated;
		UI_Payment.onHoldRestrictedBC = response.onHoldRestrictedBC;
		UI_Payment.onHoldDisplayTime = response.onHoldDisplayTime;
		UI_Payment.onHoldImageCapcha = response.oNDImageCapcha
		if (!$("#cardRadio_cash").is(":checked")){
			UI_Payment.buildPaymentOption(UI_Payment.accessPoint,
				UI_Payment.onHoldEnable, response.onHoldDisplayTime,
				UI_Payment.onHoldRestrictedBC);	
    	}
		UI_Payment.cardErrorInfo = response.cardErrorInfo;
		
		var labels = UI_Container.getLabels();

        var loyalty = response.customerLoyalty;
        var fareQuote = response.fareQuote;
        
        if (response.voucherEnabled) {
        	$("#payByVoucherPanel").show();
        } else {
        	$("#payByVoucherPanel").hide();
        }

        UI_Payment.tnxFeeInfo = {
            "totalPrice": fareQuote.totalPrice,
            "totalPayable": fareQuote.totalPayable,
            "totalFee": fareQuote.totalFee,
            "totalPriceInSel": fareQuote.inSelectedCurr.totalPrice,
            "totalPayableInSel": fareQuote.inSelectedCurr.totalPayable,
            "totalFeeInSel": fareQuote.inSelectedCurr.totalFee
        };
        UI_Payment.tnxFeeInfoOriginal = $.airutil.dom.cloneObject(UI_Payment.tnxFeeInfo);
        UI_Payment.fareQuoteOriginal = $.airutil.dom.cloneObject(fareQuote);

        var element = $('input:radio[name=cardRadio]:checked');
        var effectiveFareQuote = UI_Payment.skipPromotionIfCashPayment(element); //UI_Payment.fareQuoteOriginal;

        if (response.promotionPayment != null && response.promotionPayment) {
            //Promotions payments, occur for confirmed reservations only. So no other payments to be made.
            var msg = UI_Container.labels.lblNextSeatServices;
            msg = (msg == null || msg == "") ? "Next Seat Charges + services." : msg;
            $('#lblTotalFlightServices').text(msg);
        }

        UI_Payment.loyalty = loyalty;
        UI_Payment.setupLayoutPaymentRetry(response, effectiveFareQuote);
        UI_Payment.setActualFareQuoteValues(effectiveFareQuote);

        UI_Container.updateFareQuote(effectiveFareQuote);
        UI_Payment.showPaymentBreakDown(effectiveFareQuote);
        UI_Payment.stylePriceBreakDown();
        //		UI_Payment.showLoyalty(loyalty, effectiveFareQuote);	

        if (!UI_Payment.gridInit && !UI_Container.paymentRetry) {
            UI_Payment.loadFlightGirddata(UI_Container.getOutboundFlightSegs());
            if (UI_Container.getInboundFlightSegs().length > 0) {
                UI_Payment.loadFlightRetrunGirddata(UI_Container
                    .getInboundFlightSegs());
            } else {
                $("#trRetrunGrid").hide();
            }
            UI_Payment.gridInit = true;
        }

        $("#selPG").children().remove();
        $("#selPG").append(response.pgOptions);

        var isGWSet = false;
        var selPayGatawayVal = "";
        if (UI_Payment.objPGS != null) {
            // Build Payment Card List
            UI_Payment.isModifyAncillary = response.modifyAncillary;
            UI_Payment.modifySegment = response.modifySegment;
            UI_Payment.ispaymentforOHD = response.makePayment;
            UI_Payment.buildPaymentCardListPannel(response.cardsInfo);
        } else {
            $("#payByCard").hide();
        }
        UI_Payment.buildCardInitPannel();
        //		UI_Payment.setPaymentRetryData(response.selectedPaymentMethodDTO, loyalty);

        if (!ui_paymentGWManger.showPGS) { //this configs taken from globalConfig.js just see what are PGWs integrated when testing
            $("#selPG").hide();
            $("#lblPG").hide();
        }

        UI_Payment.showPaymentPage();
        //$("#divLoadMsg").hide();
        UI_Container.hideLoading();

        UI_Payment.airportMessage = response.airportMessage;
        UI_Payment.loadAirportMessages();

        if (UI_Payment.objPGS != null) {
            UI_Payment.hideCCPanel();
        }

        return true;
    } else {
        return false;
    }
}



UI_Payment.refresh = function() {
    $("#txtCaptcha").val("");
    document['imgCPT'].src = '../jcaptcha.jpg?relversion=' + (new Date()).getTime();
}

UI_Payment.isValideImageChapcha = function() {
    var data = {};
    //UI_Container.showLoading();
    data['captcha'] = $("#txtCaptcha").val();
    UI_commonSystem.getDummyFrm().ajaxSubmit({
        data: data,
        dataType: 'json',
        url: 'imageChapcha!onHoldBooking.action',
        success: UI_Payment.processImageCapCha,
        error: UI_commonSystem.setErrorStatus
    });
    return false;
}

UI_Payment.processImageCapCha = function(response) {
    var labels = UI_Container.getLabels();
    var msgTxt = response.messageTxt;
    if (response != null && response.success == true) {
        if (msgTxt != null && msgTxt == "valid") {
            $("#cardInputPannel").show();
            $('#frmReservation').attr('target', 'cardInputs');
            $('#frmReservation').submit();
        } else {
            UI_Payment.refresh();
            jAlert(labels['lblTypeCorrectImage'], 'Alert');
            UI_Container.hideLoading();
        }
    } else {
        UI_commonSystem.loadErrorPage({
            messageTxt: response.messageTxt
        });
    }
    UI_Container.hideLoading();
}

UI_Payment.CaptureSuccess = function(response) {
    var labels = UI_Container.getLabels();
    if (response.capturePass == true) {
        UI_Container.onNext();
    } else {
        UI_Payment.refresh()
        jAlert(labels['lblTypeCorrectImage'], 'Alert');
        UI_Container.hideLoading();
    }
};
UI_Payment.openPaymentPartners = function(lang) {
    var labels = UI_Container.getLabels();
    var url = "";
    //if (lang == "ar"){
    url = labels["linkPaymentPartner"];
    //}else{
    //url = "http://www.airarabia.com/payment-channels";
    //}
    var intHeight = (window.screen.height - 200);
    var intWidth = 950;
    var intX = ((window.screen.height - intHeight) / 2);
    var intY = ((window.screen.width - intWidth) / 2);
    var strProp = "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight + ",resizable=no,top=" + intX + ",left=" + intY;
    if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)) {
        UI_Top.holder().objCWindow.close();
    }
    UI_Top.holder().objCWindow = window.open(url, 'ToolChild', strProp);
}

/**
 * Link terms Click
 */
UI_Payment.linkTermsClick = function(extUrl) {
    var url = "";
    if (extUrl != null) {
        //set to external link
        url = extUrl;
    } else {
        url = SYS_IBECommonParam.nonSecurePath + "showTermsNCond.action";
    }
    var intHeight = (window.screen.height - 200);
    var intWidth = 795;
    var intX = ((window.screen.height - intHeight) / 2);
    var intY = ((window.screen.width - intWidth) / 2);
    var strProp = "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight + ",resizable=no,top=" + intX + ",left=" + intY;
    if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)) {
        UI_Top.holder().objCWindow.close();
    }
    UI_Top.holder().objCWindow = window.open(url, 'ToolChild', strProp)
};

UI_Payment.searchForPromotions = function(overrideExistinPromotion) {
    if ($("#cardNumber").val() == "") {
        $("#lblError").html("<font class='mandatory paddingL5'>Enter the credit card number<\/font>");
    } else if (!ValidateCardNos($("#cardType").val(), $("#cardNumber").val())) {
        $("#lblError").html("<font class='mandatory paddingL5'>Enter a valid credit card number<\/font>");
    } else {
        UI_commonSystem.getDummyFrm().ajaxSubmit({
            data: UI_Payment.getDataForPromotionReload(overrideExistinPromotion),
            dataType: 'json',
            url: 'promotionSearch.action',
            beforeSubmit: UI_Container.showLoading(),
            success: UI_Payment.loadPromotionsSuccess,
            error: UI_commonSystem.setErrorStatus
        });
    }
};

UI_Payment.loadPromotionsSuccess = function(response) {
    UI_Container.hideLoading();
    var promoCode = $('#promoCodeInPaymentPage').val();
    UI_Payment.clearReloadPromotionsDialog();

    var promotionDialog = '<div id="reloadPromotions">' +
        '<table  align="center">' +
        /*'<tr>'+
         '<td colspan="2"></td>'+
         '</tr>'+*/
        '<tr>' +
        '<td><label>' + UI_Container.labels.lblReloadPromoCode + '</label></td>' +
        '<td><input type="text" id="promoCodeInPaymentPage" name="promoCodeInPaymentPage"></td>' +
        '</tr>' +
        '<tr>' +
        '<td colspan="2" id="promocodeEmpty"></td>' +
        '</tr>' +
        '<tr>' +
        '<td colspan="2" align="right"><button name="button" id="pickPromotion">' + UI_Container.labels.lblReloadPromoBtn + '</button></td>' +
        '</tr>' +
        '</table>' +
        '</div>';

    var promotionSelectionDialog = '<div id="promoSelection">' +
        '<table  align="center">' +
        '<tr>' +
        '<td><label>' + UI_Container.labels.lblAlreadyAppliedPromo + '</label></td>' +
        '<td><label id="alreadyAppliedDiscount" style="font-weight:bold"></label></td>' +
        '</tr>' +
        '<tr>' +
        '<td><label>' + UI_Container.labels.lblCreditCardPromo + '</label></td>' +
        '<td><label id="binPromotionDiscount" style="font-weight:bold"></label></td>' +
        '</tr>' +
        '<tr>' +
        '<td align="right"><button name="button" id="existingPromotionBtn">' + UI_Container.labels.lblAlreadyAppliedPromoBtn + '</button></td>' +
        '<td align="right"><button name="button" id="binPromotionBtn">' + UI_Container.labels.lblCreditCardPromoBtn + '</button></td>' +
        '</tr>' +
        '</table>' +
        '</div>';

    if (response.success && response.hasBinPromotion) {
        if (response.hasExistingPromotion) {
            if ($('#promoSelection').is(":visible")) {
                $('#promoSelection').dialog({
                    "title": UI_Container.labels.promoSelectionDialogTitle
                });
            } else {
                $(promotionSelectionDialog).dialog({
                    "title": UI_Container.labels.promoSelectionDialogTitle
                });
            }

            $("#binPromotionDiscount").html(response.binPromotionDiscountAmount + " " + UI_Container.fareQuote.inBaseCurr.currency);
            $("#alreadyAppliedDiscount").html(response.existingDiscountAmount + " " + UI_Container.fareQuote.inBaseCurr.currency);
            $("#existingPromotionBtn").unbind("click").bind("click", function() {
                $("#promoSelection").dialog('close');
            });
            $("#binPromotionBtn").unbind("click").bind("click", function() {
                UI_Payment.searchForPromotions(true)
            });
        } else {
            UI_Payment.promotionBIN = $("#cardNumber").val().slice(0, 6);
            UI_Payment.pickBINPromotionSucess = true;
            UI_Container.discountInfo = response.discountInfo;
            UI_Container.updateFareQuote(response.fareQuote);
            UI_Payment.showPaymentBreakDown(response.fareQuote);
            UI_Payment.stylePriceBreakDown();
        }
    } else {
        if (response.checkPromotionWithPromoCode) {
            $('#promoCodeInPaymentPage').val("");
            jAlert(UI_Container.labels.msgNoPromotionFound + promoCode);
        } else if (response.hasExistingPromotion) {
            jAlert(UI_Container.labels.msgPromotionAlreadyApplied);
        } else {
            if ($('#reloadPromotions').length >= 1) {
                $('#reloadPromotions').dialog({
                    "title": UI_Container.labels.reloadPromoDialogTitle
                });
            } else {
                $(promotionDialog).dialog({
                    "title": UI_Container.labels.reloadPromoDialogTitle
                });
            }
            $("#pickPromotion").unbind("click").bind("click", UI_Payment.pickPromotionWithPromocodeAndBin);
        }
    }
};

UI_Payment.pickPromotionWithPromocodeAndBin = function() {
    $("#promocodeEmpty").html('');
    var promoCode = $('#promoCodeInPaymentPage').val();
    if (promoCode == "") {
        $("#promocodeEmpty").html('Enter Promocode');
        $("#promocodeEmpty").addClass("mandatory");

    } else {
        UI_Payment.searchForPromotions(false);
    }

};

UI_Payment.clearReloadPromotionsDialog = function() {
    if ($('#reloadPromotions').is(":visible")) {
        $("#reloadPromotions").dialog('close');
    }
    if ($('#promoSelection').is(":visible")) {
        $("#promoSelection").dialog('close');
    }
};

UI_Payment.removeBINPromotion = function(exitFromPaymentPage) {
    if (exitFromPaymentPage) {
        UI_Payment.removeBINPromotionCallBack(true);
    } else {
        var header = UI_Container.labels.remPromoDialogHeader;
        var message = UI_Container.labels.remPromoDialogMsg;
        jConfirm(message + '\n', header, function(res) {
            UI_Payment.removeBINPromotionCallBack(res);
        }, 'Yes', 'No');
    }
};

UI_Payment.removeBINPromotionCallBack = function(res) {
    if (res) {
        $("#lblError").html('');
        UI_commonSystem.getDummyFrm().ajaxSubmit({
            data: UI_Payment.getDataForPromotionReload(),
            dataType: 'json',
            url: 'promotionSearch!removeBinPromotion.action',
            beforeSubmit: UI_Container.showLoading(),
            success: UI_Payment.removeBINPromotionSuccess,
            error: UI_commonSystem.setErrorStatus
        });
        UI_Container.hideLoading();
    } else {
        $("#lblError").html("<font class='mandatory paddingL5'>" + UI_Container.labels.msgCreditcardMissMatch + " <\/font>");
    }
};
UI_Payment.removeBINPromotionSuccess = function(response) {
    if (response.success) {
        UI_Payment.pickBINPromotionSucess = false;
        UI_Container.updateFareQuote(response.fareQuote);
        UI_Payment.showPaymentBreakDown(response.fareQuote);
        UI_Payment.stylePriceBreakDown();
    }
};

UI_Payment.getDataForPromotionReload = function(overrideExistinPromotion) {
    var data = {};
    var searchParams = $("input[name*='searchParams']");
    for (var i = 0; i < searchParams.size(); i++) {
        data[searchParams[i].name] = searchParams[i].value;
    }
    data["searchParams.bankIdentificationNo"] = $("#cardNumber").val().slice(0, 6);
    //get the promo code val entered in popup
    data["promoCode"] = $('#promoCodeInPaymentPage').val();
    data["selectedFlightJson"] = $("#resSelectedFlights").val();
    data["preferedLang"] = $("#selPrefLang").val();
    data["addGroundSegment"] = UI_Container.addGroundSegment;
    data["modifySegment"] = UI_Container.modifySegment;
    data["modifyAncillary"] = UI_Container.addModifyAncillary;

    data['insurance'] = $.toJSON(UI_Container.getInsurance());
    data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
    data['makePayment'] = UI_Container.makePayment;
    if (typeof overrideExistinPromotion != 'undefined') {
        data['overrideExistingPromotion'] = overrideExistinPromotion;
    }
    return data;
};

function loginValidation(uidOBJ, pwOBJ){
	if (uidOBJ.val() == ""){
		jAlert(UI_commonSystem.alertText({msg:arrError["ERR056"],language:strLanguage}));
		uidOBJ.focus();
		return false;
	}
	
	if (pwOBJ.val() == ""){
		jAlert(UI_commonSystem.alertText({msg:arrError["ERR057"], language:strLanguage}));
		pwOBJ.focus();
		return false
	}
	
	var strChkEmpty = FindChar(uidOBJ.val())
	if (strChkEmpty != "0"){
		jAlert(UI_commonSystem.alertText({msg:buildError(arrError["ERR058"], strChkEmpty[0], arrError["ERR059"]), language:strLanguage}));
		uidOBJ.focus();
		return false
	}
	
	strChkEmpty = FindChar(pwOBJ.val())
	if (strChkEmpty != "0"){
		jAlert(UI_commonSystem.alertText({msg:buildError(arrError["ERR058"], strChkEmpty[0], arrError["ERR060"]), language:strLanguage}));
		pwOBJ.focus();
		return false
	}
	return true;
}	

/*
 * after checking an existing voucher available for pnr
 */
UI_Payment.loadVoucherCheckForPayAtStore = function(response) {
	if(response != null && response.voucherAvailable != false ){
		var requestID = response.requestID ;
		var voucherID = response.voucherID ;
		var pnr = $("#pnr").val();
		
		jConfirm(response.popupMessage, UI_Container.labels.lblVoucherExists, function(res) {
            if (!res) {
            	$("#qiwiDetailsPanel").hide();
    			$("#payfortDetailsPanel").hide();
    			$("#pgwCommonDetailsPanel").hide();
    			$("#paymentInfoPanel").hide();
    			$('input:radio[name=cardRadio]').each(function () { $(this).prop('checked', false); });
            } else {
            	
//            	Allow only if user can create multiple voucher for same payment 
////			$("#lblPaymentInfoHeading").text(UI_Container.labels.lblPaymentInfoPayFortOfflineHeading);
////            $("#lblPaymentGeneralInfo1").text(UI_Container.labels.lblPaymentPayFortOfflineGeneralInfo1);
////            $("#lblPaymentGeneralInfo2").text(UI_Container.labels.lblPaymentPayFortOfflineGeneralInfo2);
////            $("#payFortMobileNumber").val($("#hdnPayFortMobileNumber").val().replace('+', ''));
////            $("#payFortEmail").val($("#hdnPayFortEmail").val());
////            $("#qiwiDetailsPanel").hide();
////            $("#payfortDetailsPanel").show();
            	
            	var payFortVoucherUrl = "handleInterlinePayFort!showVoucherDisplay.action?voucherID="+voucherID+"&requestID="+requestID+"&pnr="+pnr;
    			$('#frmReservation').attr('action',payFortVoucherUrl);
    			$('#frmReservation').attr('target','_self');
    			$('#frmReservation').submit();
            }
    	}, UI_Container.labels.lblComYes, UI_Container.labels.lblComNo);
		
	} else {
		
		$("#lblPaymentInfoHeading").text(UI_Container.labels.lblPaymentInfoPayFortOfflineHeading);
        $("#lblPaymentGeneralInfo1").text(UI_Container.labels.lblPaymentPayFortOfflineGeneralInfo1);
        $("#lblPaymentGeneralInfo2").text(UI_Container.labels.lblPaymentPayFortOfflineGeneralInfo2);
        if( UI_Payment.payFortMobileNumber === "" ){
        	$("#payFortMobileNumber").val($("#hdnPayFortMobileNumber").val().replace('+', ''));
        } else {
        	var payAtStoreMobileNumber = UI_Payment.payFortMobileNumber.replace(', ', '');
        	$("#payFortMobileNumber").val(payAtStoreMobileNumber);
        } 
        if( UI_Payment.payFortEmail === "" ){
        	$("#payFortEmail").val($("#hdnPayFortEmail").val());
        } else {
        	$("#payFortEmail").val(UI_Payment.payFortEmail);
        }
        	
        
       
        $("#qiwiDetailsPanel").hide();
        $("#pgwCommonDetailsPanel").hide();
        $("#payfortDetailsPanel").show();
	}
}


/*
 * after checking an existing tracking number available for pnr
 */
UI_Payment.loadVoucherCheckForPayAtHome = function(response) {
	
	if(response != null && response.trackingNumberAvailable != false && response.popupMessage != null){
		alert(response.popupMessage);
		$('input:radio[name=cardRadio]').each(function () { $(this).prop('checked', false); });
		$("#paymentInfoPanel").hide();
	}  else {
		$("#qiwiDetailsPanel").hide();
		$("#payfortDetailsPanel").hide();
		$("#pgwCommonDetailsPanel").hide();
        $("#lblPaymentInfoHeading").text(UI_Container.labels.lblPaymentInfoHeading);
        $("#lblPaymentGeneralInfo1").text(UI_Container.labels.lblPaymentGeneralInfo1);
        $("#lblPaymentGeneralInfo2").text(UI_Container.labels.lblPaymentGeneralInfo2);
	}
}



UI_Payment.redeemSuccess = function(response){
	var success = response.voucherRedeemSuccess;
	if (success){
		redeemedOnce = true;
		UI_Payment.disableRedeemedVouchers();
		var vouchers = "";
		if(response.voucherRedeemResponse!=null){
			for (var i = 0 ;response.voucherRedeemResponse.redeemVoucherList.length > i; i++) {
				if (i != 0){
					vouchers += ", " ;
				}
				vouchers += response.voucherRedeemResponse.redeemVoucherList[i].voucherId;
			}
		
			$("#confirmdVoucherRedeem").text(response.messageTxt);
			$("#voucherIDList").text(vouchers);
		}	
	} else {
		$("#confirmdVoucherRedeem").text(response.messageTxt);
	} 
	
	$("#Voucher_Redeemed_Panel").show();
	
	if(response.disableRedeem){
		$("#redeemVouchers").prop('disabled', true);
	}
	
	UI_Payment.isVoucherRedeemed = true;
	
	UI_Payment.hideVoucherRedeemProgress();
}

UI_Payment.removeVoucher = function (id){
	var index = id.split("_")[1];
	var removeElement = "#voucherID_"+index;
	if(voucherHTMLID.length > 1){
		var position = voucherHTMLID.indexOf("voucherID_"+index);
		voucherHTMLID.splice(position, 1);
		if($(removeElement).is(':disabled')){
			UI_Payment.redeemVouchers(false);
		}
		$(removeElement).parent().parent().remove();

		var previous = voucherHTMLID[voucherHTMLID.length-1].split("_")[1];
		$("#btnAddVoucher_"+previous).show();
		
	} else {
		var previous = voucherHTMLID[voucherHTMLID.length-1].split("_")[1];
		$("#btnAddVoucher_"+previous).show();	
		$("#trVoucher").hide();
    	$("#payByVouchers").hide();    	
    	$('#payByVoucher_yes').attr('checked', false);
    	$('#payByVoucher_no').attr('checked', true);
		if($(removeElement).is(':disabled')){
			UI_Payment.resetVoucherRedeemOption();
		}
		$(removeElement).val("");	
		$(removeElement).prop('disabled', false);
	}
}

UI_Payment.addVoucher = function (id){
	var index = parseInt(id.split("_")[1])+(1);
	if (trim($("#voucherID_"+id.split("_")[1]).val())=="") {
		alert("Please add the voucher ID to the field..");
	} else {
		$("#"+id).hide();
		var voucherString = "<tr id='voucherSet_"+index+"'><td><input type=\"text\"id=\"voucherID_"+index+"\"></td>"+
							"<td valign='top'>"+
							"<input type='button' name='btnRemoveVoucher' class='Button' id='btnRemoveVoucher_"+index+"' value='-' onclick='UI_Payment.removeVoucher(this.id)'/>"+
							"<input type='button' name='btnAddVoucher' class='Button' id='btnAddVoucher_"+index+"' value='+' onclick='UI_Payment.addVoucher(this.id)'/></td></tr>";				
		voucherHTMLID.push("voucherID_"+index);
		$("#tblVoucherIDs").append(voucherString);
		$("#voucherID_"+index).numeric();
	}
}


UI_Payment.disableRedeemedVouchers = function () {
	for(var i = 0; i < voucherHTMLID.length ; i++) {
		var disableElement = "#voucherID_"+i;
		$(disableElement).prop('disabled', true);
	}
}

UI_Payment.unblockRedeemedVouchers = function (){
	$.ajax({
        url : 'redeemVouchers!unblockRedeemedVouchers.action',
        beforeSend : UI_commonSystem.showProgress,
        dataType:"json",
        type : "POST",		           
		success : UI_commonSystem.hideProgress,
	});	
}

UI_Payment.resetVoucherRedeemOption = function() {
	var data = {};
    data['voucherRedemption'] = true;
    data['removeVouchers'] = true;
    data['paymentGatewayID'] = UI_Payment.paymentGatewayID;
	data["onLoad"] = false;
	//data["selCurrencyCode"] = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
	UI_Payment.showVoucherRedeemProgress();
	if (UI_Container.paymentRetry) {
		$.ajax({
			dataType: 'json',
            processData: false,
            data: data,
            async: false,
            url: "interlinePayment.action",
            success: UI_Payment.loadUpdatedFareQuote,
            error: UI_commonSystem.setErrorStatus
		});
	} else {
        data['insurance'] = $.toJSON(UI_Container.getInsurance());
        data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
        if ($("#pnr").val() != undefined) {
            data['resPnr'] = $("#pnr").val();
        	data['nameChangeRequote'] = $("#nameChangeRequote").val();
        	data['nameChangePaxData'] = $("#nameChangePaxData").val();
        	if ($("#groupPNR").val() != null) {
            	var pnrNo = $("#groupPNR").val();
            	var arrPnrNo = pnrNo.split(",");
            	if (arrPnrNo[0] != "") {
                	data["pnr"] = arrPnrNo[0];
            	}
        	}
        }

        if (UI_Container.isModifySegment()) {
            if (UI_Container.isAllowInfantInsurance) {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(true));
            } else {
                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(false));
            }
        }

        $("#frmReservation").ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            async: false,
            url: "interlinePayment.action",
            success: UI_Payment.loadUpdatedFareQuote,
            error: UI_commonSystem.setErrorStatus
        });
	}
}

UI_Payment.redeemVouchers = function (onLoad){
	if(UI_Payment.checkVoucherEmptyFields()){
		var data = {};
		if (!onLoad){
			for (i = 0; i<voucherHTMLID.length; i++) {
				var voucherID = trim($("#"+voucherHTMLID[i]).val());
				var position = i +1;
				if(voucherID != null && voucherID != "") {
					data["voucherRedeemRequest.voucherIDList["+i+"]"]= $("#"+voucherHTMLID[i]).val();
				} else {
					alert("Please add the voucher ID of voucher ");
				}
			}
			if(voucherHTMLID.length==0){
				data["voucherRedeemRequest.voucherIDList"]=null;
			}
		}
	
	    data['voucherRedemption'] = true;
	    data['paymentGatewayID'] = UI_Payment.paymentGatewayID;
		data["onLoad"] = onLoad;
		//data["selCurrencyCode"] = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
		UI_Payment.showVoucherRedeemProgress();
		if (UI_Container.paymentRetry) {
			$.ajax({
				dataType: 'json',
	            processData: false,
	            data: data,
	            async: false,
	            url: "interlinePayment.action",
	            success: UI_Payment.loadUpdatedFareQuote,
	            error: UI_commonSystem.setErrorStatus
			});
		} else {
	        data['insurance'] = $.toJSON(UI_Container.getInsurance());
	        data['paxWiseAnci'] = $.toJSON(UI_Container.getPaxWiseAnci());
	        if ($("#pnr").val() != undefined) {
	            data['resPnr'] = $("#pnr").val();
	        	data['nameChangeRequote'] = $("#nameChangeRequote").val();
	        	data['nameChangePaxData'] = $("#nameChangePaxData").val();
	        	if ($("#groupPNR").val() != null) {
	            	var pnrNo = $("#groupPNR").val();
	            	var arrPnrNo = pnrNo.split(",");
	            	if (arrPnrNo[0] != "") {
	                	data["pnr"] = arrPnrNo[0];
	            	}
	        	}
	        }
	
	        if (UI_Container.isModifySegment()) {
	            if (UI_Container.isAllowInfantInsurance) {
	                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(true));
	            } else {
	                data['resPaxInfo'] = $.toJSON(UI_Container.getPaxArray(false));
	            }
	        }
	
	        $("#frmReservation").ajaxSubmit({
	            dataType: 'json',
	            processData: false,
	            data: data,
	            async: false,
	            url: "interlinePayment.action",
	            success: UI_Payment.loadUpdatedFareQuote,
	            error: UI_commonSystem.setErrorStatus
	        });
		}
	}
}

UI_Payment.showVoucherRedeemProgress = function(){
	$("#trVoucherProgressBar").show();
}

UI_Payment.hideVoucherRedeemProgress = function(){
	$("#trVoucherProgressBar").hide();
}

UI_Payment.checkVoucherEmptyFields = function() {
	var result = true;
	for (i = 0; i<voucherHTMLID.length; i++) {
		var voucherID = trim($("#"+voucherHTMLID[i]).val());
		if(voucherID == null || voucherID == "") {
			alert("Please remove empty Voucher ID fields...");
			result = false;
			break;
		}
	}
	return result;
}
