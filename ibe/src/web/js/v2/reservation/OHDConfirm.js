/** IBE OHD Confirm
 * 
 * @author Rumesh
 *  
 */

function UI_OHDConfirm(){};
UI_OHDConfirm.pnr = null;
;(function(UI_OHDConfirm) {
	
	$(document).ready(function() {
		UI_OHDConfirm.displayPage();
		//$("#divLoadBg").hide();
		$("#pageLoading").hide();
		$("#divLoadMsg").show();
		UI_OHDConfirm.ready();		
	});
	
	UI_OHDConfirm.ready = function() {
		$("#linkHome","#btnFinish").click(function(){ SYS_IBECommonParam.homeClick() });
		$("#btnFinish").unbind("click").click(function(){ SYS_IBECommonParam.homeClick() });
		$("#btnPrint").click(function(){ UI_OHDConfirm.printPage() });
		UI_commonSystem.stepsSettings("last","interLineConfurmtion");
		UI_OHDConfirm.setPageData();		
	}
	
	UI_OHDConfirm.setPageData = function() {
		var onHoldData = strConfData;
		$("#PgOHDconfirm").populateLanguage({messageList:strOHDLabels});
		$("#resNo").text(onHoldData.pnr);
		$("#time").text(onHoldData.releaseTime);
		$("#divLoadMsg").hide();
		$("#divLoadBg").show();
		UI_OHDConfirm.loadTrackingPage();
		$('body').append('<div id="divSessContainer"></div>');
		UI_commonSystem.initSessionTimeout('divSessContainer',timeOut,function(){});
	}
	
	UI_OHDConfirm.printPage = function(){
		window.print();
		return false;
	}
	
	UI_OHDConfirm.displayPage = function() {
		UI_Top.holder().UI_commonSystem.loadingCompleted();
		if (ui_paymentGWManger.showConfirmIframe){ //TODO Need to add a js configuration or remove by check other clients
			$("#divLoadBg", parent.document).css("visibility", "hidden");
			$("#divLoadBg", parent.document).show();	
			var p = $(".rightColumn", parent.document).position();
			$(".rightColumn", parent.document).hide();
			$("#cardInputs", parent.document).attr("width", parseInt($(".rightColumn", parent.document).css("width")) + 10 );
			$("#cardInputs", parent.document).attr("height", "400");
			$("#cardInputs", parent.document).css("top", "0px");
			$("#cardInputs", parent.document).css("left", "0px");
			$("#cardInputPannel", parent.document).css("position", "absolute");
			$("#cardInputPannel", parent.document).css("top", p.top );
			$("#cardInputPannel", parent.document).css("left", p.left);
			$("#divLoadBg", parent.document).css("visibility", "visible");
		}else{
			$("#divLoadBg", parent.document).hide();	
			$("#cardInputs", parent.document).attr("height", "100%");
			$("#cardInputs", parent.document).attr("width", "100%");
			$("#cardInputs", parent.document).css("top", "0px");
			$("#cardInputs", parent.document).css("left", "0px");
		}
	}
	UI_OHDConfirm.loadTrackingPage = function(){
		var paramData = "pageID=OHDCONFIRM&rad="+UI_commonSystem.getRandomNumber()+"&pnr="+UI_OHDConfirm.pnr;
		$("#frmTracking").attr("src", "showLoadPage!loadTrackingPage.action?"+paramData);
	}
	
	
})(UI_OHDConfirm);