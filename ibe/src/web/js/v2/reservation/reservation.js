
function UI_Reservation(){}

UI_Reservation.strWindowURL = window.location.href;

$(document).ready(function(){	
	UI_Reservation.ready();
});

UI_Reservation.ready = function(){
	UI_Reservation.onLoad();	
}

UI_Reservation.loadingProgress = function(){	
	$("#divLoadMsg").show();
}

UI_Reservation.loadingCompleted = function(){
	$("#divLoadMsg").hide();	
}

UI_Reservation.goBack = function(step){
	
	if(step == null) step = 0;
	
	switch(step) {
		case 2 :
			break;
		case 3 :
			break;
		default :
			UI_Reservation.loadingProgress();
			if (DATA['modifingRes'] == null){
				goHome()
			}else{
				showPNR();
			}			
			break;
	}
}

UI_Reservation.goHome = function(){
	UI_Reservation.loadingProgress();
	if (DATA['userInfo'] == null){
		location.replace(GLOBALS.homePageURL);		
	}else{
		
	}
}

UI_Reservation.showPNR = function(){	
	
}

UI_Reservation.checkHttps = function() {
	if (UI_Reservation.strWindowURL.indexOf("https:") == -1){
		alert("Invalid URL. Please type the correct URL.  \nSorry for the inconvenience.")
		UI_Top.holder().location.replace(GLOBALS.homePageURL);
		return false;
	} 
}

UI_Reservation.onLoad = function() {

	UI_Reservation.loadingProgress();
	var strSubmitUrl = '';

	//create params for from search string
	var arrParamsAA	= strReqParam.split('^');	

	switch (arrParamsAA[1]){
		case 'AS' :
			//validate parameters for Availability Search
			if (!(arrParamsAA.length == 15 || arrParamsAA.length == 16 || arrParamsAA.length == 17 || arrParamsAA.length == 18)){
				UI_Reservation.LoadingCompleted();
				alert('Unable to process your request. Invalid Data. \nSorry for the inconvenience.')
				UI_Top.holder().location.replace(GLOBALS.homePageURL);
				return false;
				break;
			}

			if (arrParamsAA[14] == 'true' || arrParamsAA[14] == true){
				arrParamsAA[14] = true;
			}else{
				arrParamsAA[14] = false;
			}				
			strSubmitUrl =  GLOBALS.nonsecureIBEUrl + 'showLoadPage!loadFlightSearch.action';
			frmMain.location.replace(strSubmitUrl);
			break;
		case 'RE' :
			strSubmitUrl = GLOBALS.secureIBEUrl + 'showCustomerLoadPage!loadCustomerRegisterPage.action';
			frmMain.location.replace(strSubmitUrl);
			break;
		case 'SI' :
			UI_Reservation.checkHttps();
			strSubmitUrl = GLOBALS.secureIBEUrl + 'showCustomerLoadPage!loadCustomerLoginPage.action';
			frmMain.location.replace(strSubmitUrl);
			break;
		case 'ASI' :
			UI_Reservation.checkHttps();
			strSubmitUrl = GLOBALS.secureIBEUrl + 'showCustomerLoadPage!loadCustomerHomePage.action';
			frmMain.location.replace(strSubmitUrl);
			break;
		case "MAN" :
			UI_Reservation.checkHttps();
			strSubmitUrl = GLOBALS.secureIBEUrl + 'showLoadPage!loadManageBooking.action';
			frmMain.location.replace(strSubmitUrl);
			break;
		case "KOS" :			
			strSubmitUrl = GLOBALS.nonsecureIBEUrl + 'showLoadPage!kioskHome.action';
			frmMain.location.replace(strSubmitUrl);
			break;	
		case "REA" :
			UI_Reservation.checkHttps();			
			strSubmitUrl  = GLOBALS.secureIBEUrl + 'showAgentLoadPage!loadAgentRegisterPage.action';
			frmMain.location.replace(strSubmitUrl);
			break;
		case "REC" :
			UI_Reservation.checkHttps();			
			strSubmitUrl  = GLOBALS.secureIBEUrl + 'showAgentLoadPage!loadCorporateRegisterPage.action';
			frmMain.location.replace(strSubmitUrl);
			break;
		case "ELINK" :			
			UI_Reservation.checkHttps();
			strSubmitUrl = GLOBALS.secureIBEUrl + 'showLoadPage!loadManageBooking.action?link=ELINK&pnr='+arrParamsAA[2]+'&lname='+arrParamsAA[3]+'&ddate='+arrParamsAA[4];
			frmMain.location.replace(strSubmitUrl);
			break;
		default :
			UI_Reservation.LoadingCompleted();
			alert('Unable to process your request. Invalid Data. \nSorry for the inconvenience.')
			UI_Top.holder().location.replace(GLOBALS.homePageURL);
			return false;
			break;
	}
}
/**
 *  Hold AirLine Parameters
 * 
 */
function AirLine(){}

AirLine.blnReturn = false;
AirLine.strFrom = "";



