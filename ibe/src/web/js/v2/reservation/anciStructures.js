/**
 * IBE Anci Inner Structure related builder/display functions
 * 
 */


//-----------------------------------Airport Transfer Related Inner Structures - Start -----------------------------------------------------------
function buildPerPaxSingleAiportTransfer (sIndex, airportCode, pIndex, paxType, transfer, modify){
		var p = transfer.provider;
		var ssrCode = transfer.ssrCode;
		if(ssrCode != null){
			ssrCode = transfer.ssrCode.replace(" ","_");
		}
		var formatedAmount = getPaxApplicableAptCharge(transfer, paxType) + " " + UI_Anci.currency.selected;
		
		var req = {date: '', hour: '', min:'', type:'', address:'', no: '' };
		var visibility = '';
		if(modify) {
			var dateArr = transfer.tranferDate.split(" ");
			var timeArr = dateArr[1].split(":");
			req.date = dateArr[0];
			req.hour = timeArr[0];
			req.min  = timeArr[1];
			req.type = transfer.transferType;
			req.address = transfer.transferAddress;
			req.no = transfer.transferContact;
			if(UI_Anci.allowModifyApts == false){
				visibility = ' disabled="disabled" '; 
			}
		}
		
		var paxAptHtml = "";
		var key = "aptPax_" + sIndex + '_' + airportCode + "_"+ pIndex + "_" + ssrCode +"_";
		var wrappedkey = "\'" + key +  "\'";
			
		paxAptHtml =
		  '<table>' + 
			'<tr>' + 
				'<td><button type="button" id="' + key + 'add"    onclick="addPaxAPTRequest('+wrappedkey + ', false)"   class="Button" style="' + (modify ? "display:none;" : "") + 'float: left; ">Add</button></td>' + 
				'<td><button type="button" id="' + key + 'edit"   onclick="editPaxAPTRequest('+wrappedkey + ', false)"  class="Button" style="' + (modify ? "" : "display:none;") + 'float: left; ">Edit</button></td>' + 
				'<td><button type="button" id="' + key + 'remove" onclick="removePaxAPTRequest('+wrappedkey + ', false)" class="Button" style="display:none; float: right; ">Remove</button></td>' + 
			'</tr>' + 
			'<tr>' + 
				'<td colspan="3">' + 
					'<div id="' + key + "div" + '" style="border-width: 1px; border-style: solid; padding: 5px 5px;display:none" >' + 
						'<table>' + 
							'<tr>' + 
								'<td><label id="lblProviderId">Id</label></td>' + 
								'<td colspan="2"><span class="providerId">' +p.airportTransferId + '</span></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblAirport">Airport</label></td>' + 
								'<td colspan="2"><span class="airportCode">' + airportCode + '</span></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblProviderName">Provider Name</label></td>' + 
								'<td colspan="2"><span class="providerName">' +p.name + '</span></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblProviderNo">Provider Number</label></td>' + 
								'<td colspan="2"><span class="providerNo">' +p.number + '</span></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblProviderAddress">Provider Address</label></td>' + 
								'<td colspan="2"><span class="providerAddress">'+ p.address + '</span></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lbltranferCharge">Price :</label></td>' + 
								'<td colspan="2"><span id="' + key + 'charge">' + formatedAmount + '</span></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblDate">Date</label></td>' + 
								'<td colspan="2"><input type="text" id="' + key + 'date" value="' + req.date + '"' + visibility + 'maxlength="10" onblur="dateOnBlur(this)"></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblTime">Time(HH/MM)</label></td>' + 
								'<td><select id="' +  key   + 'hour" name="hour" size="1" class="editable aa-input"' + visibility + '>' + getHourOptionList(req.hour) + '</select></td>' +
								'<td><select id="' +  key   + 'min" name="min" size="1" class="editable aa-input"' + visibility + '>' + getMinOptionList(req.min)  + '</select></td>' +
							'</tr>' +
							'<tr>' + 
								'<td><label id="lblType">Type</label></td>' + 
								'<td><select id="' +  key   + 'type" name="type" size="1" class="editable aa-input"' + visibility + '>' + getTypeOptionList(req.type)  + '</select></td>' +
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblAddress">Address</label></td>' + 
								'<td colspan="2"><input type="text" id="' + key + 'address" value="' + req.address + '" maxlength="80" onkeyUp="customAlphaNumeric(this)"' + visibility + '></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td><label id="lblContact">Contact No</label></td>' + 
								'<td colspan="2"><input type="text" id="' + key + 'no" value="' + req.no + '"maxlength="16" onkeyup="cusotmNumericWithSigns(this)"' + visibility + '></td>' + 
							'</tr>' + 
							'<tr>' + 
								'<td align="left"><button type="button" id="' + key + 'collapse" value="collapse" class="Button" style="float: left;" onclick="showHidePaxAPTPanel(' +wrappedkey + ', false)">Collapse</button></td>' + 
								'<td colspan="2" align="right"><button type="button" id="' + key + 'confirm" onclick="confirmPaxAptRequest(' +wrappedkey + ',' + modify +  ')" value="Confirm" class="Button" style="float: right; ">Confirm</button></td>' + 
							'</tr>' + 
						'</table>' + 
					'</div>' + 
				'</td>' + 
			'</tr>' + 
		'</table>';
		   
		return paxAptHtml;
 }


 function getHourOptionList (selected) {
	var optionList = '<option value=""></option>';
	for(var h=0; h<24; h++){
		var temp =h;
		if(h<10){
			temp = '0' + temp;
		}
		optionList = optionList + ' <option ' + ((selected == temp) ? 'selected="selected"' : "" ) +  'value="' + temp + '">' + temp + '</option>';
		
	}
	return optionList;
 };
 

 function getMinOptionList (selected) {
	var optionList = '<option value=""></option>';
	for(var h=0; h<60; h=h+15){
		var temp =h;
		if(h<10){
			temp = '0' + temp;
		}
		optionList = optionList + ' <option ' + ((selected == temp) ? 'selected="selected"' : "" ) +  'value="' + temp + '">' + temp + '</option>';
		
	}
	return optionList;
 };
 
 function getTypeOptionList (selected) {
		var optionList = '<option value=""></option>';
		optionList = optionList + ' <option ' + ((selected == 'HOME') ? 'selected="selected"' : "" ) +  'value="HOME">' + 'HOME' + '</option>';
		optionList = optionList + ' <option ' + ((selected == 'AIRPORT') ? 'selected="selected"' : "" ) +  'value="AIRPORT">' + 'AIRPORT' + '</option>';
		return optionList;
	 };
 
function addPaxAPTRequest(id){
	$('#' + id + 'edit').hide();
	showHidePaxAPTPanel(id, true);
}

function editPaxAPTRequest(id){
	getFieldByID(id + "date").focus();
	showHidePaxAPTPanel(id, true);
}

function removePaxAPTRequest(id){
	$('#' + id + 'add').show();
	$('#' + id + 'edit').hide();
	$('#' + id + 'remove').hide();
	
	var arr = id.split("_");
	var segId =arr[1]; var airPortCode=arr[2]; var paxId = arr[3]; var ssrCode = arr[4];
	
	clearPaxAptRequest(id, paxId);

	var aptData = UI_Anci.jsAnciSegModel[segId].pax[paxId].apt;
	for (var i = 0; i< aptData.apts.length; i++){
		if (airPortCode == aptData.apts[i].airport){
			var tempSbTot = aptData.apts[i].subTotal;
			aptData.apts.splice(i,1);
			UI_Anci.updateAnciTotals(UI_Anci.anciType.APTRANSFER, tempSbTot,'DEDUCT');
			UI_Anci.updateAnciCounts(UI_Anci.anciType.APTRANSFER,1,'DEDUCT');
			aptData.aptChargeTotal -= tempSbTot;
		}
	}
	UI_Anci.updateFlightAnciView();	

	var paxSegId = "aptPaxPrice_" + segId + '_' + airPortCode + "_"+ paxId;
	var formatedAmount = $.airutil.format.currency(aptData.aptChargeTotal);
	$("#" + paxSegId).text(formatedAmount);
}

function confirmPaxAptRequest(id, modify ){
   if(isValidPaxAptRequest(id, modify)){
		$('#' + id + 'add').hide();
		$('#' + id + 'edit').show();
		$('#' + id + 'remove').show();
		showHidePaxAPTPanel(id, false);
		
		var pArr= id.split("_");
		paxAptAdd(pArr[1], pArr[2], pArr[3], pArr[4], composePaxInfo(id)); //segId, airportCode, paxId, ssrCode, paxInfo
	}
}


function isValidPaxAptRequest(id, modify){
	var valid = false;
	if(modify && !UI_Anci.allowModifyApts){
		jAlert("To modify the reserved airport transfer, please contact the service provider");
		return false;
	}
	if (getFieldByID(id + "date").value == "") {
		jAlert(UI_Container.labels.lblDateCannotBeEmpty);
		getFieldByID(id + "date").focus();
		return false;
	}
	if (!dateChk(id + "date")) {
		jAlert(UI_Container.labels.lblInvalidateReqDate);
		getFieldByID(id + "date").focus();
		return false;
	}
	
	if (getFieldByID(id + "hour").value == "") {
		jAlert(UI_Container.labels.lblReqHourCannotBeEmpty);
		getFieldByID(id + "hour").focus();
		return false;
	}
	
	if (getFieldByID(id + "min").value == "") {
		jAlert(UI_Container.labels.lblReqMinCannotBeEmpty);
		getFieldByID(id + "min").focus();
		return false;
	}
	
	if (getFieldByID(id + "type").value == "") {
		jAlert(UI_Container.labels.lblPickupTypeCannotBeEmpty);
		getFieldByID(id + "type").focus();
		return false;
	}
	
	 var ref = id.split("_");
	 var segIndex  = UI_Anci.getAiportTransferSegIndex(ref[1], ref[2]);
	 var cuttOffTime = UI_Anci.getAirportTransferCutoff(segIndex);
		 
	 //"2013-12-27T15:55:00"
	var dateTime = cuttOffTime.split("T");
	var dateStr = dateTime[0];
	var dateArr = dateStr.split("-");
		
	var timeStr = dateTime[1];
	var timeArr = timeStr.split(":");
    var flightDate = dateArr[0] + dateArr[1] + dateArr[2] + timeArr[0] + timeArr[1];
	var formatedCutOff = timeArr[0] + ":" + timeArr[1] + " " + dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
		
	var requestDateArr = dateToGregorian(getFieldByID(id + "date").value, SYS_IBECommonParam.locale).split("/");
	var requestDate =  requestDateArr[2] + requestDateArr[1] + requestDateArr[0] + getFieldByID(id + "hour").value + getFieldByID(id + "min").value;
	
	var fltSeg = UI_Anci.jsApTransferModel[segIndex].flightSegmentTO;
	if(fltSeg.airportType == UI_Anci.AirportType.ARRIVAL){
		if(parseInt(requestDate, 10) <= parseInt(flightDate,10)){
			jAlert( UI_Container.labels.lblReqArrivalDateRange.replace('{0}', formatedCutOff));
			return false;
		}
	}else if(fltSeg.airportType == UI_Anci.AirportType.DEPARTURE || fltSeg.airportType == UI_tabAnci.AirportType.TRANSIT){
		if(parseInt(requestDate, 10) >= parseInt(flightDate,10)){
			jAlert(UI_Container.labels.lbReqDepartureDateRange.replace('{0}', formatedCutOff));
			return false;
		}
	}
	
	if (getFieldByID(id + "address").value == "") {
		jAlert(UI_Container.labels.lblAddressCannotBeEmpty);
		getFieldByID(id + "address").focus();
		return false;
	}
	
	if (getFieldByID(id + "no").value == "") {
		jAlert(UI_Container.labels.lblContactNotEmpty);
		getFieldByID(id + "no").focus();
		return false;
	}
	return true;
}

function dateOnBlur(domElement) {
	var pdDateId = domElement.id;
	if($.trim($("#" + pdDateId).val()) != ''){
		 dateChk(pdDateId);
	}else{
		$("#" + pdDateId).val('');
	}
} 

function customAlphaNumeric(domElement) {
	  var id =domElement.id;	 
	  var value = $.trim($("#" + id).val()); 
	  if (!/^[a-zA-Z0-9,.// ]*$/.test(value)) {
		  value  = value.replace(/[^a-zA-Z0-9,.// ]/g,"");
		  $("#" + id).val(value);
	  }
}


function cusotmNumericWithSigns (domElement) {
		  var id = domElement.id;
		  var value = $.trim($("#" + id).val());
		  if (!/^[0-9-+]*$/.test(value)) {
			  value  = value.replace(/[^0-9-+]/g,"");
			  $("#" + id).val(value);
		  }
 }

function showHidePaxAPTPanel(section, show){
	if(show){
		$('#' + section + 'div').show();
	}else{
		$('#' + section + 'div').hide();
	}
}

function clearPaxAptRequest(id, paxId){
	$("#" + id + "date").val("");
	$("#" + id + "hour").val("");
	$("#" + id + "min").val("");
	$("#" + id + "type").val("");	
	$("#" + id + "address").val("");
	$("#" + id + "no").val("");
	$("#" + id + paxId).val("");
}

function composePaxInfo(id){
    var info = {};
    var pickupDate = getFieldByID(id + "date").value;
    if(SYS_IBECommonParam.locale != "en"){
    	pickupDate = dateToGregorian(pickupDate,SYS_IBECommonParam.locale);
    }
    if(pickupDate.indexOf("/") != -1){
  	 var dateNewFormat = pickupDate.split("/");
  	 pickupDate = dateNewFormat[0] + "-" + dateNewFormat[1] + "-" + dateNewFormat[2];
    }
	info.pdDate = pickupDate;
	info.pdHour = getFieldByID(id + "hour").value;
	info.pdMin = getFieldByID(id + "min").value;
	info.pdType = getFieldByID(id + "type").value,
	info.pdAddress = getFieldByID(id + "address").value;
	info.pdContactNo = getFieldByID(id + "no").value;
	return info;
}

function paxAptAdd(segId, airportCode, paxId, ssrCode, paxInfo){
	var intSegRow = UI_Anci.getAiportTransferSegIndex(segId, airportCode);
	var selApt = getSelAptObj(UI_Anci.jsApTransferModel[intSegRow].airportServices);
	addAptForPaxObj(paxId, ssrCode, segId,selApt, intSegRow, paxInfo);
	UI_Anci.updateFlightAnciView();
}

addAptForPaxObj = function(paxId, ssrCode, segId, selApt, aptSegRowIndex, paxInfo){
	var fltSeg = UI_Anci.jsApTransferModel[aptSegRowIndex].flightSegmentTO;
	var aptData = UI_Anci.jsAnciSegModel[segId].pax[paxId].apt;
	var paxApts = aptData.apts;
	var paxAptId = null;
	var updateAmount = true;
	
	var amt = getPaxApplicableAptCharge(selApt, UI_Anci.jsAnciSegModel[segId].pax[paxId].paxType, paxId);
	for(var i = 0  ; i < paxApts.length; i ++){
		if(paxApts[i].ssrCode == selApt.ssrCode && fltSeg.airportCode == paxApts[i].airport){
			paxAptId = i; //verify segId
			updateAmount = false;
			break;
		}
	}
	paxAptId= (paxAptId == null) ? paxApts.length: paxAptId;
	paxApts[paxAptId] = {
			ssrCode:selApt.ssrCode,
			ssrName:selApt.ssrName,
			providerId:selApt.airportTransferId,
			ssrDescription:selApt.ssrDescription,
			charge:parseFloat(amt),
			subTotal:parseFloat(amt),
			applicableType:selApt.applicabilityType,
			airportType:fltSeg.airportType,
			airport:fltSeg.airportCode,
			info : paxInfo
	};
	
	if(updateAmount){
		aptData.aptChargeTotal += parseFloat(amt);
		var paxSegId = "aptPaxPrice_" + segId + '_' + fltSeg.airportCode + "_"+ paxId;
		//var formatedAmount = $.airutil.format.currency(aptData.aptChargeTotal);
		var formatedAmount = $.airutil.format.currency(parseFloat(amt));
		$("#" + paxSegId).text(formatedAmount);
		UI_Anci.updateAnciTotals(UI_Anci.anciType.APTRANSFER,parseFloat(amt),'ADD');
		UI_Anci.updateAnciCounts(UI_Anci.anciType.APTRANSFER,1,'ADD');
	}
}

getSelAptObj = function(aptList, ssrCode){
	var aptObj = {};
	for(var i=0;i<aptList.length;i++){
		if(aptList[i].ssrName == 'Airport Transfer'){
			aptObj = aptList[i];
			break;
		}
	}
	return aptObj;
}

getPaxApplicableAptCharge = function(aptObj, paxType){
	var serviceCharge = 0;
	if(paxType == UI_Anci.PaxType.PARENT){
		serviceCharge = parseFloat(aptObj.adultAmount) + parseFloat(aptObj.infantAmount);
	} else if(paxType == UI_Anci.PaxType.ADULT){
		serviceCharge = parseFloat(aptObj.adultAmount);
	} else if(paxType == UI_Anci.PaxType.CHILD){
		serviceCharge = parseFloat(aptObj.childAmount);
	}
	if(isNaN(serviceCharge)) {
	 	serviceCharge = aptObj.serviceCharge;
	}
	return $.airutil.format.currency(serviceCharge);
}

//-----------------------------------Airport Transfer Related Inner Structures - Start -----------------------------------------------------------