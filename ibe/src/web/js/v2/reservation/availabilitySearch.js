function UI_AvailabilitySearch(){}

UI_AvailabilitySearch.OND_SEQUENCE = {OUT_BOUND:0, IN_BOUND:1};

UI_AvailabilitySearch.fromAirport = "";
UI_AvailabilitySearch.toAirport = "";
UI_AvailabilitySearch.departureDate = "";
UI_AvailabilitySearch.returnDate = "";
UI_AvailabilitySearch.departureVariance = "";
UI_AvailabilitySearch.arrivalVariance = "";
UI_AvailabilitySearch.selectedCurrency = "";
UI_AvailabilitySearch.returnFlag = "";
UI_AvailabilitySearch.lblDepature = "";
UI_AvailabilitySearch.lblArrival = "";
UI_AvailabilitySearch.currencyCode = UI_Top.holder().GLOBALS.baseCurrency;
UI_AvailabilitySearch.sessionTimeOutDisplay =  UI_Top.holder().GLOBALS.sessionTimeOutDisplay;
// Set Flexible Date Status
UI_AvailabilitySearch.isFlexibleDates = 0;
// Set Class Of Services
UI_AvailabilitySearch.classOFService = "";

UI_AvailabilitySearch.depatureStatus = false;
UI_AvailabilitySearch.arrivalStatus = false;
UI_AvailabilitySearch.msgCanNotReserve = "";
UI_AvailabilitySearch.errorInfo = "";
UI_AvailabilitySearch.blnOFlt = false;
UI_AvailabilitySearch.blnRFlt = false;
UI_AvailabilitySearch.msgWithoutOutbound = "";
UI_AvailabilitySearch.msgWithoutInbound = "";
UI_AvailabilitySearch.flightType = "";
UI_AvailabilitySearch.lblAdult = "";
UI_AvailabilitySearch.lblChild = "";
UI_AvailabilitySearch.lblInfant = "";
UI_AvailabilitySearch.noAdult = null;
UI_AvailabilitySearch.noChild = null;
UI_AvailabilitySearch.noInfant = null;
UI_AvailabilitySearch.fareQuote = null;

UI_AvailabilitySearch.outboundFlights = null;
UI_AvailabilitySearch.inboundFlights = null;

UI_AvailabilitySearch.outFlightRPHList = "";
UI_AvailabilitySearch.retFlightRPHList = "";

UI_AvailabilitySearch.outSelectedFareType = "promoFare";
UI_AvailabilitySearch.retSelectedFareType = "promoFare";

UI_AvailabilitySearch.fareQuoteOutFlightRPHList = "";
UI_AvailabilitySearch.fareQuoteRetFlightRPHList = "";

UI_AvailabilitySearch.airportMessage = "";

UI_AvailabilitySearch.labels = "";
UI_AvailabilitySearch.weekdays = null;
UI_AvailabilitySearch.months = null;
UI_AvailabilitySearch.stopovers = null;
UI_AvailabilitySearch.lastSelected = null;
UI_AvailabilitySearch.blnPageLoad = false;
UI_AvailabilitySearch.minReturnTransitionTime = UI_Top.holder().GLOBALS.minReturnTransitionTime;
UI_AvailabilitySearch.isCaptchaEnabled = UI_Top.holder().GLOBALS.imageCapchaDisAvalSearch;
UI_AvailabilitySearch.modSegBalSummaryDisplay = UI_Top.holder().GLOBALS.modSegBalAvalSearch;
UI_AvailabilitySearch.calendarViews = 7;
UI_AvailabilitySearch.calendarType = UI_Top.holder().GLOBALS.calendarType;
UI_AvailabilitySearch.calendarGradianView = calDisplayConfig.design.displayGradientFare;

// Half return params
UI_AvailabilitySearch.halfReturnFareQuote= false;
UI_AvailabilitySearch.stayOverTimeInMillis = 0;

UI_AvailabilitySearch.outBoundDurations = null;
UI_AvailabilitySearch.inBoundDurations = null;
UI_AvailabilitySearch.updateChargeList = null;
UI_AvailabilitySearch.showFareRules = null;
UI_AvailabilitySearch.fareRules = null;
UI_AvailabilitySearch.hideTaxBDRows = true;
UI_AvailabilitySearch.hideSurchargeBDRows = true;

UI_AvailabilitySearch.isModifySegment = false;
UI_AvailabilitySearch.isKioskUser = false;
UI_AvailabilitySearch.isModifyBalanceSuccess = false;
UI_AvailabilitySearch.isRegUser = false;

// Handle multiple request in same session
UI_AvailabilitySearch.isFromPostCardPage = false;
UI_AvailabilitySearch.msgFromPostCardPage = null;

// segment modification IBE
UI_AvailabilitySearch.modifyByDate = false;
UI_AvailabilitySearch.modifyByRoute = false;

// for passenger DOB validation
UI_AvailabilitySearch.paxValidation = null;


UI_AvailabilitySearch.addGroundSegment = false;
// requote flow
UI_AvailabilitySearch.isRequoteFlightSearch = false;
UI_AvailabilitySearch.isCnxSegmentRequote = false;
UI_AvailabilitySearch.isNameChangeRequote = false;
// set the available payment methods
UI_AvailabilitySearch.showPaymentIcons = false;
UI_AvailabilitySearch.paymentMethods = [];
UI_AvailabilitySearch.ondWiseLogicalCCSelected = {};
// to mark that quote flexi charges
UI_AvailabilitySearch.ondWiseFlexiSelected = {};
//in Back-end Flexi Quote for all 
UI_AvailabilitySearch.ondWiseFlexiQuote = {0:true,1:true};
//ond wise flexi availability
UI_AvailabilitySearch.ondWiseFlexiAvailability = {};
//show Flexi selection on Anci page
UI_AvailabilitySearch.ondWiseFlexiSelectionInAnciArr = {0:true,1:true};
//show Flexi charge on Anci page
UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr = {0:0,1:0};
//to make the quote bundle charges
UI_AvailabilitySearch.ondWiseBundleFareSelected = {};
//
UI_AvailabilitySearch.ondWiseBookingClassSelected = {};

UI_AvailabilitySearch.segWiseBookingClassOverride={};

UI_AvailabilitySearch.availableBundleFareLCClass = {};


UI_AvailabilitySearch.promotionInfo = null;
UI_AvailabilitySearch.isBINPromotion = false;

UI_AvailabilitySearch.fromAirPortName ="";
UI_AvailabilitySearch.toAirPortName ="";

UI_AvailabilitySearch.isLogicalCabinClassEnabled = false;

UI_AvailabilitySearch.isCookieChargesApplicable = UI_Top.holder().GLOBALS.cookieChargesApplicable;
UI_AvailabilitySearch.noOfCookieExpiryDays = UI_Top.holder().GLOBALS.noOfCookieExpiryDays;
UI_AvailabilitySearch.promoDiscountType = {
		PERCENTAGE : "PERCENTAGE",
		VALUE : "VALUE"
}

var newONDSegmentList = [];
var   removedOndList = [];
// the array to contain the new calendar headers and bodies.
var newCalObjs = null;
// call back function to create new calendar headers and bodies.
var createNewCalContentCB = null;
var departureOrReturnFlag = null;
var tempFormDataStore = null;
var numberOfDays = null;
var newCalNextDatesArr = null;
var moveDirection = null;
var calendarLength = null;
var isLogicalCCSelected = false;


var dtODate 		= null;
var dtRDate 		= null;
var strODate 		= "";
var strRDate 		= "";
var carrier = "";


var slFareQuoteRow = $("#SelectedItemsFL_done>tr");
var slFareQuoteTable = $("#farequote_done_Area>table");

var today=new Date();
var expiry=new Date(today.getTime()+ UI_AvailabilitySearch.noOfCookieExpiryDays*24*3600*1000);
var expired=new Date(today.getTime()-24*3600*1000);

$(document).ready(function(){	
	UI_AvailabilitySearch.ready();
});

/**
 * Page Ready
 */
UI_AvailabilitySearch.ready = function(){
	insertParam("st", 'null');
	UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = false;
	UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = false;		
	
	$("#divLoadBg").hide();
	// Buttons
	UI_commonSystem.loadingProgress();
	$("#btnSOver").decorateButton();	
	$("#btnContinue").decorateButton();		
	$("#btnSOver").click(function(){UI_AvailabilitySearch.homeClick();});
	$("#btnBack").click(function(){UI_AvailabilitySearch.controleState();});	
	

	$("#btnContinue").click(function(){UI_commonSystem.pageOnChange();UI_AvailabilitySearch.loadPaxPage();});	
	$("#linkTerms").click(function(){UI_AvailabilitySearch.linkTermsClick(globalConfig.termsLink);});
	
	$("#btnModOndSeg").decorateButton();		
	$("#btnModOndSeg").click(function(){UI_AvailabilitySearch.loadRequoteSegment();});	
	
	$("#btnRequote").decorateButton();		
	$("#btnRequote").click(function(){UI_AvailabilitySearch.reCalculateFareRequote();});	
	
	$("#btnAvailableFlight").decorateButton();		
	$("#btnAvailableFlight").click(function(){UI_AvailabilitySearch.showHideAvailableFilghtsOnly(true);});	
	
	UI_AvailabilitySearch.setLayout();
	UI_AvailabilitySearch.setPageAirLineParameters();
	UI_AvailabilitySearch.setAirlineParameters();	
	UI_AvailabilitySearch.pageInit();	
	UI_AvailabilitySearch.setDates();
	UI_AvailabilitySearch.getFlygingDates();	
	
	// Check Kiosk User
	if ($("#kioskAccessPoint").val() != null && $("#kioskAccessPoint").val() == "true") {
		UI_AvailabilitySearch.isKioskUser = true;
	}
	
	// Modify Segment
	if ($.trim($("#resPNR").val()) != "" && $("#addGroundSegment").val() != "true") {
		isModify = true;
		$("#resModifySegment").val(true);
		UI_AvailabilitySearch.isModifySegment = true;
		$("#tableModSearch").hide();
	}

	// Add Bus Segment
	if ($.trim($("#resPNR").val()) != "" && $("#addGroundSegment").val() == "true") {
		UI_AvailabilitySearch.addGroundSegment = true;
		$("#tableModSearch").hide();
	}

	// Registered User
	if ($("#regCustomer").val() == "true") {
		UI_AvailabilitySearch.isRegUser = true;
	}	

	/**
	 * Build Steps
	 */	
	UI_commonSystem.stepsSettings(1,"availability");	
	/**
	 * Build Calendar tabs
	 */	
		
	
	UI_AvailabilitySearch.setCalendarDateVars();
	// Load Page Build Data
	UI_AvailabilitySearch.loadPageBuildData();
	UI_commonSystem.loadingCompleted();

	if($("#requoteFlightSearch").val()!=null && $("#requoteFlightSearch").val()!='' && $("#requoteFlightSearch").val()=="true" ){
		 if ($("#cancelSegmentRequote").val() != null && $("#requoteFlightSearch").val()!='' && $("#cancelSegmentRequote").val() == "true") {
			 UI_AvailabilitySearch.isRequoteFlightSearch = true;
			 UI_AvailabilitySearch.isCnxSegmentRequote = true;
			 UI_AvailabilitySearch.cancelSegmentRequote();	
				
		 } else if($("#nameChangeRequote").val() != null && $("#nameChangeRequote").val()!='' 
			 && $("#nameChangeRequote").val() == "true"){
			 
			 UI_AvailabilitySearch.isRequoteFlightSearch = true;
			 UI_AvailabilitySearch.isNameChangeRequote = true;
			 UI_AvailabilitySearch.nameChangeRequote();
			 
		 } else {
			 UI_AvailabilitySearch.isRequoteFlightSearch = true;
			 $("#resIsMcSelted").val(false);
			 UI_AvailabilitySearch.loadAvailabilitySearchData();
		 }
	}else{		
		UI_AvailabilitySearch.showHideRequoteData({isFareAvailable:false});
		UI_AvailabilitySearch.loadAvailabilitySearchData();
	}

	// UI_AvailabilitySearch.cleanAndBuildCalHolder();
	/*
	 * TODO Clean
	 */
	// $("#tabs1").tabs("option", "active",
	// Number($("#resFlexibleDates").val()));
	
	UI_commonSystem.setCommonButtonSettings();
	// UI_AvailabilitySearch.initFlexiActions();
	
	UI_commonSystem.dragableLI();
		if (UI_AvailabilitySearch.isCaptchaEnabled){
			// $("#trPanelImageCaptcha").show();
			$("#imgCPT").click(function(){UI_AvailabilitySearch.refresh()});
		}
		
	if (calDisplayConfig.layout.displayFlightDetailsInCal
			&& !UI_AvailabilitySearch.isCalendarViewDisabled()){
		$("#FlightDetailsPanel").remove();
	} else{
		$("#outFlightDetails").remove();
		$("#inFlightDetails").remove();
	}
	
	UI_AvailabilitySearch.setBalanceSummaryDisplayPage();

	$(".tempMore").tooltip({ 
	    bodyHandler: function() {
	    	return UI_AvailabilitySearch.commonFlexiMessage;
	 		// return UI_AvailabilitySearch.outBoundFlexiMessage;
	    }, 
	    showURL: false,
	    delay: 0,
	    track: true
	});

	$(document).on("click",".CompareFare",function(){
		UI_AvailabilitySearch.compareFareClick();
	});
	$("#browserMsgTr").hide();
};

UI_AvailabilitySearch.buildCalendarTabs = function() {
	UI_commonSystem.readOnly(true);
	$("#tabs1").empty();
	$("#tabs1").append($.fn.calendarview.createTabData(0));
	$("#resFlexibleDates").val(0);
	UI_AvailabilitySearch.setCalendarDateVars();
}

UI_AvailabilitySearch.initFlexiActions = function(){
	$(document).on("click","input[name='chkFlexiOutbound']",function(){
		UI_AvailabilitySearch.setFare();
		if(UI_AvailabilitySearch.updateChargeList != null){
			UI_AvailabilitySearch.displaySummary();
		}
	});
	$(document).on("click","input[name='chkFlexiInbound']",function(){
		UI_AvailabilitySearch.setFare();
		if(UI_AvailabilitySearch.updateChargeList != null){
			I_AvailabilitySearch.displaySummary();
		}
	});
}

UI_AvailabilitySearch.controleState = function(){
	if(readParam('st').indexOf("reQuoteFare")>-1){
		insertParam('st', 'mcAvail');
		UI_AvailabilitySearch.showHideAvailableFilghtsOnly(true);
	}else{
		insertParam('st', '');
		UI_AvailabilitySearch.homeClick();
	}
}


UI_AvailabilitySearch.homeClick = function() {
	if (UI_AvailabilitySearch.isModifySegment == true || UI_AvailabilitySearch.addGroundSegment) {
		UI_commonSystem.loadingProgress();	
		if(UI_AvailabilitySearch.isKioskUser != true ) {
			$("#frmFare").attr('action','showCustomerLoadPage!loadCustomerHomePage.action');
		} else {
			$("#frmFare").attr('action','showLoadPage!kioskHome.action');			
		}		
		$("#frmFare").submit();
	} else {
		SYS_IBECommonParam.homeClick();
	}
}

/*
 * TODO to be Clean UI_AvailabilitySearch.cleanAndBuildCalHolder = function(){
 * var isFlexible = Number($("#resFlexibleDates").val());
 * $("#tabs1-2").html(''); if (isFlexible == 0){
 * $("#tabs1-2").append($.fn.calendarview.createTabData(1)); }else{
 * $("#tabs1-2").append($.fn.calendarview.createTabData(0)); }
 * UI_AvailabilitySearch.initFlexiActions(); }
 */

/**
 * Load Page Build data
 */
UI_AvailabilitySearch.loadPageBuildData = function() {
	 $("#FlightDetailsPanel").hide();
	 $("#trPriceBDPannel").hide();
	 $("#balanceSummary").hide();	 
	 var url = "availabilitySearchPageDetail.action";
	 var data = {};
	 $.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,		
	 success: UI_AvailabilitySearch.loadPageBuildDataProcess, error:UI_commonSystem.setErrorStatus, cache : false});
	return false;
}

UI_AvailabilitySearch.loadPageBuildDataProcess = function(response) {
	if(response != null && response.success == true) {	
		$("#divLoadBg").slideDown(1000);

		if(UI_AvailabilitySearch.addGroundSegment){
		  response.jsonLabel.lblSelectFlight = 	response.jsonLabel.lblSelectBus;            
		  response.jsonLabel.lblSelectFlightMsg = response.jsonLabel.lblSelectBusMsg;
         	  response.jsonLabel.noFlights = response.jsonLabel.noBuses;
		  response.jsonLabel.fullFlights = response.jsonLabel.fullBuses;
		}

		$(".bookingSummary").summaryPanel({id:"BookingSummary", data:null, labels:response.jsonLabel});
		$("#PgFares").populateLanguage({messageList:response.jsonLabel});
        if ($.trim($("#lblSelectFlightMsg"))!=""){
            $("#lblSelectFlightMsg").parent().parent().show();
        }
		UI_AvailabilitySearch.labels = response.jsonLabel;
		UI_AvailabilitySearch.errorInfo = response.errorInfo;		
		UI_AvailabilitySearch.blnShowOneWay = response.blnShowOneWay;		
		UI_AvailabilitySearch.msgCanNotReserve = response.jsonLabel.msgCanNotReserve;
		UI_AvailabilitySearch.msgWithoutOutbound = response.jsonLabel.msgWithoutOutbound;
		UI_AvailabilitySearch.msgWithoutInbound = response.jsonLabel.msgWithoutInbound;
		UI_AvailabilitySearch.lblAdult = response.jsonLabel.lblAdult;
		UI_AvailabilitySearch.lblChild = response.jsonLabel.lblChild;
		UI_AvailabilitySearch.lblInfant = response.jsonLabel.lblInfant;
		UI_AvailabilitySearch.lblArrival = UI_AvailabilitySearch.labels.lblArrival;
		UI_AvailabilitySearch.lblDepature = UI_AvailabilitySearch.labels.lblDepature;
		UI_AvailabilitySearch.weekdays = response.jsonLabel.weekdaysArray.split(",");
		UI_AvailabilitySearch.weekdaysFull= response.jsonLabel.weekdaysFullArray.split(",");
		UI_AvailabilitySearch.months = response.jsonLabel.monthsArray.split(",");
		UI_AvailabilitySearch.stopovers = response.jsonLabel.stopoversArray.split(",");
		UI_AvailabilitySearch.showPriceBDInSelCurr = response.showPriceBDInSelCurr;
		UI_AvailabilitySearch.isFromPostCardPage = response.fromPostCardPage;
		UI_AvailabilitySearch.msgFromPostCardPage = response.msgFromPostCardPage;
		UI_AvailabilitySearch.commonFlexiMessage = response.jsonLabel.lblcommonFlexiMessage;
		UI_AvailabilitySearch.promoCodeNotFound =  response.promoCodeNotFound;
        UI_CaptchaValidation.labels = UI_AvailabilitySearch.labels;
		carrier = response.carrier;	
		$('input').change(function (){UI_commonSystem.pageOnChange();});
		UI_AvailabilitySearch.updateCurrencyMessage();
		if ($.trim(response.termsNCond) == ""){
			$("#trTermsNCond").hide();
		}else{
			$("#termsNCond").html(response.termsNCond);
		}
		// Set Search Parameters
		if (response.searchSystem!=undefined){
			$("#resSearchSystem").val(response.searchSystem);
		}
		UI_AvailabilitySearch.blnPageLoad = true;
		// button to set Previous or Start over labels
		// TODO Set Correct action to previous
		$("#btnSOver").show();
		if (UI_AvailabilitySearch.isModifySegment || UI_AvailabilitySearch.addGroundSegment){
			if ($("#btnSOver").children().length > 0){
				$("#btnSOver").find("td.buttonMiddle").html(response.jsonLabel.btnPrevious);
			}else{
				$("#btnSOver").val(response.jsonLabel.btnPrevious);
			}
						
		}
		
		if ( UI_AvailabilitySearch.isKioskUser || ((UI_AvailabilitySearch.isModifySegment || UI_AvailabilitySearch.isRegUser)
				&& UI_AvailabilitySearch.sessionTimeOutDisplay )){			
			$('body').append('<div id="divSessContainer"></div>');
			UI_commonSystem.initSessionTimeout('divSessContainer',response.timeoutDTO,function(){});
		}
		
		// Set Airport Message From response to show on screen
		UI_AvailabilitySearch.airportMessage = response.airportMessage;
		UI_AvailabilitySearch.loadAirportSearchMessages();
		UI_AvailabilitySearch.loadTrackingPage();
	}  else {
		// UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		wrapAndDebugError('001',response);
	}
	/*
	 * we used this when we had any issue with browser if( $.browser.mozilla &&
	 * parseInt($.browser.version,10) >= 10 ){
	 * $("#browserMsgTxt").html(UI_AvailabilitySearch.labels.msgFirefoxNotsupported);
	 * $("#browserMsgTr").show(); }
	 */
	setDesignSpecLabels();
	$("#tableModSearch").find("label").each(function(){
		$(this).attr("title", $(this).text());
	});
}

function wrapAndDebugError (id, response){
	var msg = (response==null) ? null:response.messageTxt;
	if(msg == null || msg == ''){
		if(msg == null) return ; // no errors to be shown as browser back
									// create a bug in ajaxSubmit plugin
		if(console !=null  && console.log !=null){
			console.log(id);
			console.log(response);
		}
		msg = 'REF:'+id + ' Error occured. Please report with reference';
	}
	UI_commonSystem.loadErrorPage({messageTxt:msg});
}

UI_AvailabilitySearch.sendGoogleTrackingData = function(carrier){
	if (UI_AvailabilitySearch.isModifySegment == true) {
		var mode = "MOD";
	} else {
		var mode = "CRE";			
	}
	if(UI_AvailabilitySearch.returnFlag == "true") {
		var way = "RT";
	} else {
		var way = "OW";			
	}	
	var strAnalyticPage = "/funnel_G1/FlightSelect/" + mode + "/" + carrier + "/" + UI_AvailabilitySearch.fromAirport + UI_AvailabilitySearch.toAirport + "/" + way;
	if(strAnalyticEnable == "true") {			
		// pageTracker._trackPageview(strAnalyticPage);
		_gaq.push(['_trackPageview', strAnalyticPage ]);
	}
}

/*
 * Set Page Layouts
 */
UI_AvailabilitySearch.setLayout = function(){
	if (calDisplayConfig.layout.termsConditionPos == "bottom"){
		var temp = $("#trTermsNCond").remove().clone();
		$(".buttonset").parent().append(temp);
	}
}

/**
 * Set Dates Departure/Arrival
 */
UI_AvailabilitySearch.setDates = function() {
	dtODate = new Date(UI_AvailabilitySearch.departureDate.substr(6,4), (Number(UI_AvailabilitySearch.departureDate.substr(3,2)) - 1), UI_AvailabilitySearch.departureDate.substr(0,2));
	if (UI_AvailabilitySearch.returnDate != ""){
		dtRDate = new Date(UI_AvailabilitySearch.returnDate.substr(6,4), (Number(UI_AvailabilitySearch.returnDate.substr(3,2)) - 1), UI_AvailabilitySearch.returnDate.substr(0,2));
	}
}


/**
 * Get Flying Dates
 * 
 */
UI_AvailabilitySearch.getFlygingDates =  function() {
	var strM = dtODate.getMonth() + 1;
	var strD = dtODate.getDate();
	if (strM < 10){strM = "0" + strM}
	if (strD < 10){strD = "0" + strD}
	strODate  = strD + "/" + strM + "/" + dtODate.getFullYear();
	
	if (dtRDate != null){
		strM = dtRDate.getMonth() + 1;
		strD = dtRDate.getDate();
		if (strM < 10){strM = "0" + strM}
		if (strD < 10){strD = "0" + strD}
		strRDate  = strD + "/" + strM + "/" + dtRDate.getFullYear();
	}
}

/**
 * Set Date Variance parameters
 * TODO : Baladewa plz remove hard coded locale
 */
UI_AvailabilitySearch.setCalendarDateVars = function(){
	if (Number($("#resFlexibleDates").val()) == 0){
		// change
		var calVarience = parseInt((UI_AvailabilitySearch.calendarViews / 2));
		var topDiff = parseInt((UI_AvailabilitySearch.calendarViews)) - 1;
		var diffO = (dateVal(dataformater(dateToGregorian(strODate,'en'))).d - dateVal(dataformater(UI_Top.holder().strSysDate)).d);
		var diffR = (dateVal(dataformater(dateToGregorian(strRDate,'en'))).d - dateVal(dataformater(UI_Top.holder().strSysDate)).d);
		if ( diffO < calVarience ) {
			$("#resDepartureVariance").val(topDiff - diffO);
		}else{
			$("#resDepartureVariance").val(calVarience);
		}
		if (diffR < calVarience) {
			$("#resReturnVariance").val(topDiff - diffR);
		}else{
			$("#resReturnVariance").val(calVarience);
		}
		
	}else{
		// change
		if (dateVal(dataformater(strODate)).d == dateVal(dataformater(UI_Top.holder().strSysDate)).d) {
			$("#resDepartureVariance").val(2);
		}else{
			$("#resDepartureVariance").val(1);
		}
		if (dateVal(dataformater(strRDate)).d == dateVal(dataformater(UI_Top.holder().strSysDate)).d) {
			$("#resReturnVariance").val(2);
		}else{
			$("#resReturnVariance").val(1);
		}
	}
}

/**
 * Set Air line parameters
 */
UI_AvailabilitySearch.setPageAirLineParameters = function() {
	 var arrParamsAA	= UI_Top.holder().strReqParam.split('^');	 
	 if ($.trim($("#resFromAirport").val()) == "") {
		 $("#resFromAirport").val(arrParamsAA[2]);	 
		 $("#resToAirport").val(arrParamsAA[3]);
		 $("#resDepartureDate").val(arrParamsAA[4]);
		 $("#resReturnDate").val(arrParamsAA[5]);
		 $("#resDepartureVariance").val(arrParamsAA[6]);
		 $("#resReturnVariance").val(arrParamsAA[7]);
		 $("#resSelectedCurrency").val(arrParamsAA[8]);
		 $("#resAdultCount").val(arrParamsAA[9]);
		 $("#resChildCount").val(arrParamsAA[10]);
		 $("#resInfantCount").val(arrParamsAA[11]);
		 $("#resFromAirportName").val(arrParamsAA[12]);
		 $("#resToAirportName").val(arrParamsAA[13]);

		 $("#resReturnFlag").val(arrParamsAA[14]);
		 if (arrParamsAA[16] != undefined && arrParamsAA[16] != "") {
			 $("#resPaxType").val(arrParamsAA[16]);	
		 } else {
			 $("#resPaxType").val($("#selPaxType").val());
		 }
		 

		 if (arrParamsAA[17] != undefined && arrParamsAA[17] != "") {			
			 $("#resFareType").val(arrParamsAA[17]);
		 } else {
			 // To Do: Set Fare type
			 $("#resFareType").val("A");
		 }
		 
		 // Cabin Class
		 if (arrParamsAA[15] != undefined && arrParamsAA[15] != "") {			
			 $("#resClassOfService").val(arrParamsAA[15]);
		 } else {			 
			 $("#resClassOfService").val(UI_Top.holder().GLOBALS.classOfService);
		 }	
		 
		 if (arrParamsAA[18] != undefined && arrParamsAA[18] != "") {
			 $("#resPromoCode").val(arrParamsAA[18]);
		 }
		 
		 if (arrParamsAA[19] != undefined && arrParamsAA[19] != "") {
			 $("#resBankIdNo").val(arrParamsAA[19]);
		 }
		 
		 if (arrParamsAA[20] != undefined && arrParamsAA[20] != "") {
			 $("#resPreviousSearch").val(arrParamsAA[20].split("_")[0]);
			 $("#resFlightSearchDates").val(arrParamsAA[20].split("_")[1]);
		 }
		 

		 if (arrParamsAA[21] != undefined && arrParamsAA[21] != "") {
			 $("#resHubTimeDetails").val(arrParamsAA[21]);
		 }
		 
		 // Set Airline Flexible date status
		 $("#resFlexibleDates").val(0);	
		 // $('#resQuoteInboundFlexi').val(true);
		 // $('#resQuoteOutboundFlexi').val(true);
	 }
	 
	
}

/**
 * Set AirLine parameters
 */

UI_AvailabilitySearch.setAirlineParameters =  function () {
	UI_AvailabilitySearch.fromAirport =  $("#resFromAirport").val();
	UI_AvailabilitySearch.toAirport =  $("#resToAirport").val();
	UI_AvailabilitySearch.departureDate =  $("#resDepartureDate").val();
	UI_AvailabilitySearch.returnDate =  $("#resReturnDate").val();
	UI_AvailabilitySearch.selectedCurrency =  $("#resSelectedCurrency").val();
	UI_AvailabilitySearch.returnFlag = $("#resReturnFlag").val();
	UI_AvailabilitySearch.isFlexibleDates = $("#resFlexibleDates").val();
	UI_AvailabilitySearch.classOFService = $("#resClassOfService").val();
}


/**
 * Page Setup
 */
UI_AvailabilitySearch.pageInit = function() {	
	$("#trPaymentSPanel").hide();	
	$("#FlightDetailsPanel").hide();
	$("#trPriceBDPannel").hide();
	$("#trPaymentPannel").remove();
	$("#balanceSummary").hide();
	
	slFareQuoteRow = $("#SelectedItemsFL_done>tr");
	slFareQuoteTable = $("#farequote_done_Area>table");
}

UI_AvailabilitySearch.searchPageClear = function() {
	$("#divSummaryPane").hide();	
}


/**
 * Request Page Init Data And Availability Search Data for 3 Day view
 */
// To Identify First Request OR Other One
var requestID = 1;
UI_AvailabilitySearch.loadAvailabilitySearchData = function(){
	 UI_AvailabilitySearch.buildCalendarTabs();
	 UI_AvailabilitySearch.searchPageClear();
	 $("#FlightDetailsPanel").hide();
	 $("#trPriceBDPannel").hide();
	 $("#balanceSummary").hide();
	 UI_AvailabilitySearch.setDefaultSearchOption($("#resFromAirport").val(), $("#resToAirport").val());
	 var url = 'availabilitySearchCalendar.action?requestID='+ requestID++;
	 var data = createSearchSubmitData();
	 
     setCookieData(data);
     
	 if ((!UI_AvailabilitySearch.isModifySegment && !UI_AvailabilitySearch.addGroundSegment)
			 || (UI_AvailabilitySearch.isModifySegment && $('#resModFlexiSelected').val() != "true")) {
		 $.each(UI_AvailabilitySearch.ondWiseFlexiSelected, function(key, value){
			 UI_AvailabilitySearch.ondWiseFlexiSelected[key] = false;
		 });
		 data['searchParams.ondSelectedFlexiStr'] = $.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected);
	 } else if(UI_AvailabilitySearch.isModifySegment && $('#resModFlexiSelected').val() == "true"){
		 $.each(UI_AvailabilitySearch.ondWiseFlexiSelected, function(key, value){
			 UI_AvailabilitySearch.ondWiseFlexiSelected[key] = true;
		 });
		 data['searchParams.ondSelectedFlexiStr'] = $.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected);
		 UI_AvailabilitySearch.ondWiseFlexiQuote = {0:false,1:false};
	 }
	 if (UI_AvailabilitySearch.isModifySegment){
		 UI_AvailabilitySearch.showHideAvailableFilghtsOnly(true);
	 } else {
		 UI_AvailabilitySearch.ondWiseBundleFareSelected = {};
		 UI_AvailabilitySearch.ondWiseBookingClassSelected = {};
		 UI_AvailabilitySearch.segWiseBookingClassOverride = {};
	 }
	 data['searchParams.ondQuoteFlexiStr'] = $.toJSON(UI_AvailabilitySearch.ondWiseFlexiQuote);
	 data['searchParams.ondAvailbleFlexiStr'] = $.toJSON(UI_AvailabilitySearch.ondWiseFlexiAvailability);
	 data['searchParams.preferredBundledFares'] = $.toJSON(UI_AvailabilitySearch.ondWiseBundleFareSelected);
	 data['searchParams.preferredBookingCodes'] = $.toJSON(UI_AvailabilitySearch.ondWiseBookingClassSelected);
	 data['searchParams.availableBundleFareLCClass'] = $.toJSON(UI_AvailabilitySearch.availableBundleFareLCClass);
	 data['searchParams.ondSegBookingClassStr'] = $.toJSON(UI_AvailabilitySearch.segWiseBookingClassOverride);
	 data['searchParams.resOndwiseFlexiChargesForAnci'] = $.toJSON(UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr);
	
	 if(UI_CaptchaValidation.isHumanVerificationEnabled) {
		 UI_CaptchaValidation.setDataToProcessAfterCaptchaValidation(data,url,'AVAIL', null);	 
	 }
	 
	 UI_commonSystem.loadingProgress();
	 $.ajax({type: "POST", dataType: 'json',data: data , url:url,		
	 success: UI_AvailabilitySearch.loadAvailabilitySearchDataProcess, error:UI_commonSystem.setErrorStatus, cache : false});
	return false;
}

function setCookieData(data){
    if(UI_AvailabilitySearch.isCookieChargesApplicable){
        var sector = data['searchParams.fromAirport'] + "/" + data['searchParams.toAirport'];
        var offset = new Date().getTimezoneOffset();
        data['searchParams.gmtOffset'] = offset;
        
        var searchParamCookie = data['searchParams.adultCount'] + "/" + data['searchParams.childCount'] + "/" + data['searchParams.infantCount'] + "_" +
           sector+ "_" + data['searchParams.departureDate'] + "_" + data['searchParams.returnDate'];
        var cookieData = getCookie(searchParamCookie);
        data['searchParams.isPreviousSearch'] = cookieData.split("_")[0];
        data['searchParams.flightSearchDates'] = cookieData.split("_")[1];      
    }
}

function getCookie(currentSearch){
	var currentSearchParams = currentSearch.split("_");
	var cookies=document.cookie.split(";");
	var isPreviousSearch = false;
	var flightSearchDates = "";
	var gmtOffSet = "";
	var searchDate, paxCount, segmentCode, depDate, retDate, offeredFare;
	
	for(var i=0;i<cookies.length;i++)
	{
		var cookie=cookies[i].split("=");
		if($.trim(cookie[0]).search("searchParamCookie_") != -1){
			
			searchDate = cookie[0].split("_")[1];
			paxCount = cookie[1].split('_')[0];
			segmentCode = cookie[1].split('_')[1];
			depDate = cookie[1].split('_')[2];
			retDate = cookie[1].split('_')[3];
			offeredFare = cookie[1].split('_')[4];
			
			if(($.trim(paxCount) == currentSearchParams[0]) &&
					($.trim(segmentCode) == currentSearchParams[1]) && ($.trim(depDate) == currentSearchParams[2])
					&& ($.trim(retDate) == currentSearchParams[3])){
				if(!isPreviousSearch){
					isPreviousSearch = true;	
				}
				
				if(flightSearchDates == ""){
					flightSearchDates += searchDate + "#" + offeredFare +  ",";
				} else {
					if(flightSearchDates.search(searchDate) == -1){
						flightSearchDates += searchDate + "#" + offeredFare + ",";
					}	
				}
			}	
		}			
	}
	return isPreviousSearch + "_" + flightSearchDates;
}

UI_AvailabilitySearch.searchCookie = function(details){
	var cookieData = details.split("_");
	var cookies=document.cookie.split(";");
	var dptflt, avlflt, ond, rtflg, fare, paxcnt;
	
	for(var i=0;i<cookies.length;i++){
		var cookie=cookies[i].split("=");
		if($.trim(cookie[0]).search("saveSearchCookie_") != -1){
				
			dptflt	= cookie[1].split("_")[0];
			avlflt 	= cookie[1].split("_")[1];
			ond 	= cookie[1].split("_")[2];
			rtflg	= cookie[1].split("_")[3];
			fare 	= cookie[1].split("_")[4];
			paxcnt 	= cookie[1].split("_")[5];
			
			if(($.trim(dptflt) == cookieData[0]) && ($.trim(avlflt) == cookieData[1])
					&& ($.trim(ond) == cookieData[2]) && ($.trim(rtflg) == cookieData[3])
					&& ($.trim(paxcnt) == cookieData[5])){
				document.cookie = cookies[i] + "; expires=Thu 07 Jan 2010 01:00:00 AM IST";
				
			}
			
		}
	}
}

function setCookieValue(response){
	//Flight Date and Flight numbers
	var endfill = false;
	var FlightInfo;
	if(response.searchParams.returnFlag == true){
		var dptFltInfo;
		var dptFltList = response.departueFlights;
		var dptFltIndex = dptFltList.length;
		for(var i=0;i<dptFltIndex;i++){
			if(dptFltList[i].selected == true){
					endfill = true;
				var avblFlts = dptFltList[i].availableFlightInfo;
				var avblFltsIndex = avblFlts.length;
				    dptFltInfo = dptFltList[i].departureDate;
				for(var j=0;j<avblFltsIndex;j++){
					if(avblFlts[j].selected == true){
						dptFltInfo = dptFltInfo + "/" + avblFlts[j].segments[0].flightNumber;
					}					
				}
			}
			if(endfill == false){
				dptFltInfo = dptFltList[i].departureDate;
			}
		}
		endfill = false;
		var avlFltInfo;
		var arvlFltList = response.arrivalFlights;
		var arvlFltIndex = arvlFltList.length;
		for(var i=0;i<arvlFltIndex;i++){
			if(arvlFltList[i].selected == true){
					endfill = true;
				var avblFlts = arvlFltList[i].availableFlightInfo;
				var avblFltsIndex = avblFlts.length;
					avlFltInfo = arvlFltList[i].departureDate;
				for(var j=0;j<avblFltsIndex;j++){
					if(avblFlts[j].selected == true){
						avlFltInfo = avlFltInfo + "/" + avblFlts[j].segments[0].flightNumber;
					}
				}
			}
			if(endfill == false){
				avlFltInfo = arvlFltList[i].departureDate;
			}
		}
		FlightInfo = dptFltInfo + "_" + avlFltInfo;
	
	}
	else{
		var endfill = false;
		var dptFltInfo;
		var dptFltList = response.departueFlights;
		var dptFltIndex = dptFltList.length;
		for(var i=0;i<dptFltIndex;i++){
			if(dptFltList[i].selected == true){
					endfill = true;
				var avblFlts = dptFltList[i].availableFlightInfo;
				var avblFltsIndex = avblFlts.length;
				    dptFltInfo = dptFltList[i].departureDate;
				for(var j=0;j<avblFltsIndex;j++){
					if(avblFlts[j].selected == true){
						dptFltInfo = dptFltInfo + "/" + avblFlts[j].segments[0].flightNumber;
					}
				}
			}
			if(endfill == false){
				dptFltInfo = dptFltList[i].departureDate;
			}
		}
		FlightInfo = dptFltInfo;
	}
	//Return flag
	var rtFlg;
	if(response.searchParams.returnFlag == true){
		rtFlg = "RT";	
	}
	else{
		rtFlg = "OW";
	}
	//Pax Count
	var passengers = response.searchParams.adultCount + "/" + response.searchParams.childCount + "/" + response.searchParams.infantCount; 
	
	//Search OND
	var OND = response.searchParams.fromAirport + "/" +	response.searchParams.toAirport;
	
	//Total Fare
	var Fare = parseFloat(response.fareQuote.totalFare);
	var Surcharge = parseFloat(response.fareQuote.totalTaxSurcharge);
	var Tax = parseFloat(response.fareQuote.totalTax);
	var TotalFare = String(Fare + Surcharge + Tax) + "/" + response.fareQuote.currency;
	
	//Setting Cookie value
	var CookieValue = FlightInfo + "_" + OND + "_" + rtFlg + "_" + TotalFare + "_" + passengers;
	
	return CookieValue;
}


function createSearchSubmitData() {
	var obj = {};	   
    $("#searchSubmitParams > input").each(function(i,o){
      var n = o.name, v = o.value;      	
      var jItm = $(o);
		if(jItm.attr('type') != 'radio' || jItm.attr('checked') ) {
			obj[n] = v;
		}	   			
    });
    return obj;
}

function populateOldPerPaxFareTOList(flights){
	if(flights.length > 0){
		var oldPerPaxFareTOList = [];
		for(var i = 0 ; i < flights.length ; i++){
			oldPerPaxFareTOList[i] = flights[i].fareTO;
		}
		return oldPerPaxFareTOList;
	} else {
		return null;		
	}		
	
	
}

/**
 * Process Request Data
 */
var pageLoadCount = 0;
var timeObj = "";
UI_AvailabilitySearch.loadAvailabilitySearchDataProcess = function(response) {
	
	if (UI_AvailabilitySearch.blnPageLoad == false) {
		if (pageLoadCount < 5) {
			timeObj = setTimeout('UI_AvailabilitySearch.loadAvailabilitySearchDataProcess('+ $.toJSON(response) +')',1000);
			pageLoadCount++;
		} else {
			clearTimeout(timeObj);
			window.location.reload();
		}
	} else if(response != null && response.success == true) {
		
		if(UI_CaptchaValidation.isHumanVerificationEnabled) {
	  	 	if(response.captchaValidationRequired) {
				UI_commonSystem.loadingCompleted();
				UI_CaptchaValidation.openCaptchaPopup();
				return false;
			}
			UI_CaptchaValidation.resetDataToProcessAfterCaptchaValidation(); 
	 	}

		var jsonOutboundLCClasses = "{";
        var index = 0 ;
        var outBoundAvailbleBundleFareArray = new Array();
        $.each(response.outboundAvailableLogicalCC,function(index,logicalCC){
            if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
                outBoundAvailbleBundleFareArray[index] = new Array();
                outBoundAvailbleBundleFareArray[index].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, "bundleFareName":logicalCC.logicalCCDesc, "imageUrl":setBundleFareImageURL(logicalCC)});

            }
        });

        var jsonINboundLCClasses = "{";
        var index = 0 ;
        var inBoundAvailbleBundleFareArray = new Array();
        $.each(response.inboundAvailableLogicalCC,function(index,logicalCC){
            if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
                inBoundAvailbleBundleFareArray[index] = new Array();
                inBoundAvailbleBundleFareArray[index].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, "bundleFareName":logicalCC.logicalCCDesc, "imageUrl": setBundleFareImageURL(logicalCC)});

            }
        });

        UI_AvailabilitySearch.availableBundleFareLCClass[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = outBoundAvailbleBundleFareArray;
        UI_AvailabilitySearch.availableBundleFareLCClass[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = inBoundAvailbleBundleFareArray;
        $("#resAvailableBundleFareLCClassStr").val($.toJSON(UI_AvailabilitySearch.availableBundleFareLCClass));

			
		if(UI_AvailabilitySearch.isCookieChargesApplicable){
			var offeredFare = "";
			if(response.fareQuote != null){
				offeredFare = response.fareQuote.totalFare;
			}
			var sector = response.searchParams.fromAirport + "/" +	response.searchParams.toAirport;
			var searchParamCookie = response.searchParams.adultCount + "/" + response.searchParams.childCount + "/" + response.searchParams.infantCount + "_"
									+ sector + "_"
									+ response.searchParams.departureDate + "_" + response.searchParams.returnDate + "_"
									+ offeredFare;
			
			var settingDetails = response.searchParams.sysDate + "_" + searchParamCookie;
			setCookie("searchParamCookie_" + settingDetails ,searchParamCookie);	
		}
		
		
		if(UI_Top.holder().GLOBALS.saveCookieEnable && !response.requoteFlightSearch ){
			var searchDetails = setCookieValue(response);
			UI_AvailabilitySearch.searchCookie(searchDetails);
			setCookie("saveSearchCookie" ,JSON.stringify(response.searchParamsCookieDetails));
		}
				
		// segment modification IBE
		UI_AvailabilitySearch.modifyByRoute = response.modifyByRoute;
		UI_AvailabilitySearch.modifyByDate = response.modifyByDate;
		
		UI_AvailabilitySearch.promotionInfo = response.promoInfo;
		if(response.promoInfo != null){
			var promo = response.promoInfo;
			UI_AvailabilitySearch.isBINPromotion = (promo.applicableBINs!= null && promo.applicableBINs.length > 0) ;
			$("#promoDiscountErrPanel").hide();
		}
		else{
			if(response.searchParams.promoCode!="" && response.fareQuote.hasFareQuote){
				$("#promoDiscountErrPanel").show();				
				$("#lblPromoError").text(UI_AvailabilitySearch.promoCodeNotFound);
			} else{
				$("#promoDiscountErrPanel").hide();
			}
		}
		// Add bus segment
		UI_AvailabilitySearch.addGroundSegment = response.addGroundSegment;
		if(UI_AvailabilitySearch.addGroundSegment){
			$("#busForDeparture").val(response.searchParams.busForDeparture);
			$("#busConValidTimeFrom").val(response.searchParams.busConValiedTimeFrom);
			$("#busConValidTimeTo").val(response.searchParams.busConValiedTimeTo);
		}
		
		UI_AvailabilitySearch.isRequoteFlightSearch = response.requoteFlightSearch;
		UI_AvailabilitySearch.fareQuote = response.fareQuote;		
		UI_AvailabilitySearch.outboundFlights = response.departueFlights;		
		UI_AvailabilitySearch.inboundFlights = response.arrivalFlights;	
		
		
		if(UI_AvailabilitySearch.isCalendarViewDisabled()){
			UI_mcAvailabilitySearch.ondString = [];
			var tempOnd = {};
			tempOnd.fromAirport = $("#resFromAirport").val();
			tempOnd.toAirport = $("#resToAirport").val();
			tempOnd.fromAirportName = UI_AvailabilitySearch.fromAirPortName;
			tempOnd.toAirportName = UI_AvailabilitySearch.toAirPortName;
			tempOnd.flightRPHList = [];
			tempOnd.flownOnd = false;
			tempOnd.existingFlightRPHList = [];
			tempOnd.existingResSegRPHList = [];
			tempOnd.departureDate = $("#resDepartureDate").val();
			tempOnd.depDate = dateToGregorian($("#resDepartureDate").val(),SYS_IBECommonParam.locale);
			tempOnd.departureVariance = 0;
			tempOnd.dateInteger = dateVal(dataformater(dateToGregorian($("#resDepartureDate").val(),SYS_IBECommonParam.locale))).d;
			tempOnd.classOfService = $("#resClassOfService").val();
			tempOnd.logicalCabinClass = null;
			tempOnd.bookingType = "NORMAL";
			tempOnd.bookingClass = null;
			UI_mcAvailabilitySearch.ondString[UI_mcAvailabilitySearch.ondString.length] = tempOnd;		
			UI_mcAvailabilitySearch.flightDetailsPanelHTML = "#divFlightDetailsRequote";
			UI_mcAvailabilitySearch.processFlightSearch(response);
			
			rebindClickOperation = function(selectorStr){
				for ( var i = 0; i < $(selectorStr).length; i++) {
					var prevBtnId = $(selectorStr)[i].id;
					$("#" + prevBtnId).unbind("click").click(function(e){UI_AvailabilitySearch.searchNextPrevDay(e)});
					
				}
			}
			
			rebindClickOperation("a[id^='ln_Prev_']");
			rebindClickOperation("a[id^='ln_Next_']");
			
		} else {
			UI_AvailabilitySearch.buildDays(response.departueFlights , response.arrivalFlights, response.calDisplayCurrency,
					response.outboundAvailableLogicalCC, response.inboundAvailableLogicalCC);
				$.fn.calendarview.includeFlexiInCal();
		}
		
		UI_AvailabilitySearch.setSelectedBookingClass(response);
		
		UI_AvailabilitySearch.setRPHList();
				
		UI_AvailabilitySearch.showFareRules = response.showFareRules;
		UI_AvailabilitySearch.fareRules = response.fareRules;
		
		UI_AvailabilitySearch.halfReturnFareQuote =  response.searchParams.halfReturnFareQuote;
		UI_AvailabilitySearch.stayOverTimeInMillis = response.searchParams.stayOverTimeInMillis;
		UI_AvailabilitySearch.noAdult = response.searchParams.adultCount;
		UI_AvailabilitySearch.noChild = response.searchParams.childCount;
		UI_AvailabilitySearch.noInfant = response.searchParams.infantCount;
		
		UI_AvailabilitySearch.ondWiseFlexiAvailability = response.ondWiseTotalFlexiAvailable;
		
		// Check Interline Selected Selected Flight Segments
		var isValid = UI_AvailabilitySearch.isValidSelectedFlightSegments();
		if (isValid) {
			UI_AvailabilitySearch.fareQuoteOutFlightRPHList = UI_AvailabilitySearch.getRPHArrayString(UI_AvailabilitySearch.outFlightRPHList);
			UI_AvailabilitySearch.fareQuoteRetFlightRPHList = UI_AvailabilitySearch.getRPHArrayString(UI_AvailabilitySearch.retFlightRPHList);		
			// Display Flight Details
			UI_AvailabilitySearch.displaySelectedFlightDetails();		
			UI_AvailabilitySearch.setFare();
		}
			
		if (response.paymentMethods!=null){
			UI_AvailabilitySearch.paymentMethods=response.paymentMethods;
		}
		
		if(response.showPaymentIcons != null){
			UI_AvailabilitySearch.showPaymentIcons = response.showPaymentIcons;
		}
		
		// display onholdbooking message
		UI_AvailabilitySearch.displayPaymentMethods(response.allowOnhold);
		$("#resHalfReturnFareQuote").val(UI_AvailabilitySearch.halfReturnFareQuote);
		$("#resStayOverTimeInMillis").val(UI_AvailabilitySearch.stayOverTimeInMillis);
		$("#requestSessionIdentifier").val(response.requestSessionIdentifier);		
		
		// Call loading message before balancesummaryaction
		UI_commonSystem.loadingCompleted();
	    // Modify Segment Balance Summary
		if (UI_AvailabilitySearch.isModifySegment == true  && UI_AvailabilitySearch.fareQuote != null &&
				UI_AvailabilitySearch.fareQuote.segmentFare != null) {
				UI_AvailabilitySearch.showBalcenceSummary();
		}

		if (UI_AvailabilitySearch.addGroundSegment == true  && UI_AvailabilitySearch.fareQuote != null &&
				UI_AvailabilitySearch.fareQuote.segmentFare != null) {
				UI_AvailabilitySearch.showBalcenceSummary();
		}
		
		// Set Airport Message From response to show on screen
		UI_AvailabilitySearch.airportMessage = response.airportMessage;
		UI_AvailabilitySearch.loadAirportSearchMessages();
		// TO Display imageCaptcha after the availability
		if (UI_AvailabilitySearch.isCaptchaEnabled){
			$("#trPanelImageCaptcha").show();
		}
		UI_AvailabilitySearch.isLogicalCabinClassEnabled = response.logicalCabinClassEnabled;
	} else {
		// UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		wrapAndDebugError('002',response);
	}	

		UI_AvailabilitySearch.ondWiseFlexiAvailability = response.ondWiseTotalFlexiAvailable;
		UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr = response.ondWiseTotalFlexiCharge;
}

UI_AvailabilitySearch.setSelectedBookingClass = function(response){
	
	if(response.fareRules != null){
		response.fareRules.forEach(function(item, index) {
		    
			UI_AvailabilitySearch.ondWiseBookingClassSelected[index] = {};
			UI_AvailabilitySearch.ondWiseBookingClassSelected[index][item.ondCode] = item.bookingClass;

		});
		
	}
	
	$("#resFareQuoteBookingCCSelection").val($.toJSON(UI_AvailabilitySearch.ondWiseBookingClassSelected));
	
}
/**
 * Display Flight Details
 * 
 */

UI_AvailabilitySearch.displaySelectedFlightDetails = function() {
	showTripPassengerSummary();	
	showSelectedFlightsSummary(); 
	
}

function showTripPassengerSummary() {
	var labels = UI_AvailabilitySearch.labels;
	if(UI_AvailabilitySearch.returnFlag == "true") {
		$("#lblReturn").text(labels['lblRoundTrip']);
	}else {
		$("#lblReturn").text(labels['lblOneWayTrip']);
	} 
	var strMsgAdult = labels['lblAdult'];
	var strMsgChild = labels['lblChild'];
	var strMsgInfant = labels['lblInfant'];
	
	if(Number($("#resAdultCount").val()) > 0) {
		$("#lblAdultCount").text($("#resAdultCount").val() + " " + strMsgAdult)
	}
	if(Number($("#resChildCount").val()) > 0) {
		$("#lblChildCount").text(Number($("#resChildCount").val()) +" " + strMsgChild)
	}
	if(Number($("#resInfantCount").val()) > 0) {
		$("#lblInfantCount").text(Number($("#resInfantCount").val())+" " + strMsgInfant)
	}
}

function showSelectedFlightsSummary(){
	$("#divSummaryPane").show();
	UI_AvailabilitySearch.setRPHList();	
	$.fn.summaryPanel.firstLoad();
	$("#tableOBIB").nextAll().remove();
	$("#tableOBIB").iterateTemplete({templeteName:"tableOBIB", data:UI_AvailabilitySearch.getSelectedFlightSegments("both"), dtoName:"flightSegments"});
	$("#tableOB").nextAll().remove();
	$("#tableOB").iterateTemplete({templeteName:"tableOB", data:UI_AvailabilitySearch.getSelectedFlightSegments("outbound"), dtoName:"flightSegments"});
	$("#tableIB").nextAll().remove();
	$("#tableIB").iterateTemplete({templeteName:"tableIB", data:UI_AvailabilitySearch.getSelectedFlightSegments("inbound"), dtoName:"flightSegments"});

	if(UI_AvailabilitySearch.noAdult > 0) {
		$("#adultCount").text(UI_AvailabilitySearch.noAdult+ " " + UI_AvailabilitySearch.lblAdult);
	}
	if(UI_AvailabilitySearch.noChild > 0) {
		$("#childCount").text(", "+UI_AvailabilitySearch.noChild+ " " + UI_AvailabilitySearch.lblChild);
	}
	if(UI_AvailabilitySearch.noInfant > 0) {
		$("#infantCount").text(", "+UI_AvailabilitySearch.noInfant+ " " + UI_AvailabilitySearch.lblInfant);
	}
	
	UI_AvailabilitySearch.showDuration();
	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
}


/**
 * Set Fare
 */

UI_AvailabilitySearch.setFare = function() {
	var fareQuote = UI_AvailabilitySearch.fareQuote;
	if(top.UI_Top.holder().GLOBALS.promoCodeEnabled){
		fareQuote.promoCodeEnabled = true;
	}	
	// Check Selected Flight Available
	if(!UI_AvailabilitySearch.isCnxSegmentRequote){
		if (fareQuote == null || UI_AvailabilitySearch.getSelectedFlightSegments("both").length == 0) {
			fareQuote =  null;
		}
	}
	if (fareQuote != null && fareQuote.segmentFare != null) {
				
		$("#priceBreakDownTemplate").nextAll().remove();
		$("#taxBreakDownTemplate").nextAll().remove();
		$("#surchargeBreakDownTemplate").nextAll().remove();
				
		var fareRuleMsg = '';
		if(UI_AvailabilitySearch.showFareRules && calDisplayConfig.layout.fareruleLinkDisplay){
            $("#lblFareRules").show().parent().parent().parent().parent().show();
			for(var i = 0; i < UI_AvailabilitySearch.fareRules.length; i++){
				if(UI_AvailabilitySearch.fareRules[i].comment!= null && UI_AvailabilitySearch.fareRules[i].comment!=''){
					fareRuleMsg += '<b>'+ UI_AvailabilitySearch.fareRules[i].ondCode + '</b><br /> &nbsp;&nbsp;'+ UI_AvailabilitySearch.fareRules[i].comment + '<br />' ;
				}
			}
			if(fareRuleMsg==''){
				$("#lblFareRules").hide();
			}
		} else {
			$("#lblFareRules").hide();
		}
		if (calDisplayConfig.layout.newWindowForFareRuleDetails){
			$("#lblFareRules").tooltip({ 
			    bodyHandler: function() { 
			 		return fareRuleMsg; 
			    }, 
			    showURL: false,
			    delay: 0,
			    track: true
			});
		}else{
			$("#lblFareRules").unbind("click").bind("click", function(){
				var intX = ((window.screen.height - 600) / 2);
				var intY = ((window.screen.width - 600) / 2);
				var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=600,height=600,resizable=no,top=" + intX + ",left=" + intY;
				var url = "";
				var consoleRef = window.open('', 'ToolChild',strProp);

				consoleRef.document.writeln(
						  '<html><head></head>'
						   +'<body bgcolor=white onLoad="self.focus()">'
						   +fareRuleMsg
						   +'</body></html>');
				
			});
		}
		
		// Clone the object as we need to get the original value if flexi is
		// deselected
		var segSegFareQuote = $.airutil.dom.cloneObject(fareQuote.segmentFare);
		$("#priceBreakDownTemplate").iterateTempletePB({templeteName:"priceBreakDownTemplate", data:segSegFareQuote, dtoName:"paxWise"});
		for (var pp=0; pp<segSegFareQuote.length;pp++){
			$(".pb-class").each(function(jj,ppOBJ){
				if(pp%2==0){
	                $("#priceBreakDownTemplate_"+pp+""+jj).addClass("even");
	            }
			});
		}
        
        
		if (fareQuote.perPaxTaxBD != null && fareQuote.perPaxTaxBD.length >0){
			var  perPaxTaxBD = $.airutil.dom.cloneObject(fareQuote.perPaxTaxBD);
			$("#taxBreakDownTemplate").iterateTempleteTaxBD({templeteName:"taxBreakDownTemplate", data:perPaxTaxBD, dtoName:"perPaxTaxBD"});			

			$('#taxBDDisplayLink').unbind();			
			$('#taxBDDisplayLink').click(function() {
				$(".taxBDDisplayRows").toggle();
				if( $(".taxBDDisplayRows").is(":visible") ){
				 	UI_AvailabilitySearch.hideTaxBDRows = false;
				}
			});	
			
			if (UI_AvailabilitySearch.hideTaxBDRows){
				$(".taxBDDisplayRows").hide();
			}

		} else {
			// hide the set of rows displaying tax break down information.
			$(".taxBDDisplayRows").hide();
			$(".taxBDDisplayLinkRows").hide();
		}
		
		if (fareQuote.perPaxSurchargeBD !=null && fareQuote.perPaxSurchargeBD.length >0 ) {
			var  perPaxSurchargeBD = $.airutil.dom.cloneObject(fareQuote.perPaxSurchargeBD);
			$("#surchargeBreakDownTemplate").iterateTempleteTaxBD({templeteName:"surchargeBreakDownTemplate", data:perPaxSurchargeBD, dtoName:"perPaxSurchargeBD"});			

			$('#surchargeBDDisplayLink').unbind();	
			
			$('#surchargeBDDisplayLink').click(function() {
				$(".surchargeBDDisplayRows").toggle();	
				if( $(".surchargeBDDisplayRows").is(":visible") ){
				 	UI_AvailabilitySearch.hideSurchargeBDRows = false;
				}			
			});

			if (UI_AvailabilitySearch.hideTaxBDRows){
				$(".surchargeBDDisplayRows").hide();
			}
			
		} else {
			// hide the set of rows displaying surcharge break down information.
			$(".surchargeBDDisplayRows").hide();
			$(".surchargeBDDisplayLinkRows").hide();
		}				
		
		UI_AvailabilitySearch.setTotalFare({inBaseCurr:fareQuote.inBaseCurr, inSelectedCurr:fareQuote.inSelectedCurr});
		UI_AvailabilitySearch.setDiscountAmount({inBaseCurr:fareQuote.inBaseCurr, inSelectedCurr:fareQuote.inSelectedCurr});
		UI_AvailabilitySearch.setPromoDiscountAmount(fareQuote);
		$("#FlightDetailsPanel").slideDown();
		$("#trPriceBDPannel").slideDown();
		$.fn.summaryPanel.updateFlightPriceDetails();
	} else {
		$("#FlightDetailsPanel").slideUp();
		$("#trPriceBDPannel").slideUp();
	}	

}

function setCookie(name, value)
{
   document.cookie=name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
}
/*
 * including Flexi box in to Calendar moved to jquery.calendarview.js as
 * $.fn.calendarview.includeFlexiInCal()
 */

/*
 * Setting in Place Flexi box in Calendar moved to jquery.calendarview.js as
 * $.fn.calendarview.settingInPlaceFlexiInCal()
 */

/**
 * Update currency message
 * 
 */
UI_AvailabilitySearch.updateCurrencyMessage = function() {	
	$("#lblCurrencySupportMessage").text($("#lblCurrencySupportMessage").text().replace("{0}", UI_AvailabilitySearch.currencyCode));
}

UI_AvailabilitySearch.getSegname = function(p){
	var retSegName = "";
	var fromSegCode = "";
	var toSegCode = "";
	var space = "&nbsp;";
	if(UI_Top.holder().GLOBALS.skipSegmentCodeInLabelsForOtherLanguages == "false"){
		fromSegCode = "(" + $("#resFromAirport").val() + ")";
		toSegCode = "(" + $("#resToAirport").val() + ")";
	}
	if(SYS_IBECommonParam.locale != "en"){
		space = "&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	if(p==0){
		// outbound
		retSegName = space + UI_AvailabilitySearch.fromAirPortName + fromSegCode + space + " / " + space + 
		UI_AvailabilitySearch.toAirPortName + toSegCode
	}else{
		// inbound
		retSegName = space + UI_AvailabilitySearch.toAirPortName + toSegCode + space + " / " + space + 
		UI_AvailabilitySearch.fromAirPortName  + fromSegCode
	}
	return retSegName;
}

/**
 * Set Total Fare
 */
UI_AvailabilitySearch.setTotalFare = function(fareInCurrency) {
	setTotalFare(fareInCurrency);
}

/**
 * Set Discount for booking a return flight
 */
UI_AvailabilitySearch.setDiscountAmount = function(fareInCurrency) {
	var discountAmount = fareInCurrency.inBaseCurr.totalIbeFareDiscount;
	var curr = fareInCurrency.inBaseCurr.currency;
	if (fareInCurrency.inBaseCurr.currency != fareInCurrency.inSelectedCurr.currency) {
		discountAmount = fareInCurrency.inSelectedCurr.totalIbeFareDiscount;
		curr = fareInCurrency.inSelectedCurr.currency;
	}
	$("#discountAmount").text("-" + discountAmount + " " + curr);
	if (parseFloat(discountAmount) > 0) {
		$("#tempDiscountBanner").remove();
		$("#returnDiscountPanel").show();
		var strMSG = UI_AvailabilitySearch.labels.msgDiscountBanner;
		strMSG = strMSG.replace("{0}", curr);
		strMSG = strMSG.replace("{1}", discountAmount);
		var tBanner = $("<span id='tempDiscountBanner' class='tbanner'>"+strMSG+"</span>").addClass("spMsg");
		var topVal = 125;
		$(".rightColumn").before(tBanner);
		UI_AvailabilitySearch.rePositionBanner(tBanner, topVal)
	} else {
		$("#tempDiscountBanner").remove();
		$("#returnDiscountPanel").hide();
	}
}

/**
 * Set discount for promotion
 */
UI_AvailabilitySearch.setPromoDiscountAmount = function(fareQuote){
	
	var discountAmount = fareQuote.inBaseCurr.totalIbePromoDiscount;

	if (fareQuote.hasPromoDiscount && parseFloat(discountAmount) > 0) {
		var curr = fareQuote.inBaseCurr.currency;
		if (fareQuote.inBaseCurr.currency != fareQuote.inSelectedCurr.currency) {
			discountAmount = fareQuote.inSelectedCurr.totalIbePromoDiscount;
			curr = fareQuote.inSelectedCurr.currency;
		}
		
		if(fareQuote.deductFromTotal){	
			var promoAppType = UI_AvailabilitySearch.promotionInfo.applicability;
			//var isOutBoundPromo = UI_AvailabilitySearch.promotionInfo.applicability =="OB" ? true:false;
			$("#promoDiscountPanel").show();
			if (promoAppType == "OB"){
				$(".lblPromoDiscount").text(UI_AvailabilitySearch.labels.lblPromoDiscount);
				$(".promoDiscountAmount").text("-" + discountAmount + " " + curr);
				$("#promoDiscountPanel_OB").css("width",(($("#resReturnFlag").val()=="true")?"49%":"100%")).show();
				$("#promoDiscountPanel_IB").css("width",(($("#resReturnFlag").val()=="true")?"49%":"0%")).hide();
			}else if(promoAppType == "IB"){
				$(".lblPromoDiscount").text(UI_AvailabilitySearch.labels.lblPromoDiscount);
				$(".promoDiscountAmount").text("-" + discountAmount + " " + curr);
				$("#promoDiscountPanel_IB").css("width","49%").show();
				$("#promoDiscountPanel_OB").css("width","49%").hide();
			}else{
				$(".lblPromoDiscount").text(UI_AvailabilitySearch.labels.lblPromoDiscount);
				$(".promoDiscountAmount").text("-" + discountAmount + " " + curr);
				$("#promoDiscountPanel_IB").css("width","99%").show();
				$("#promoDiscountPanel_OB").css("width","0%").hide();
			}
			//if(isOutBoundPromo){
				
			//}else{
				
			//}
			$("#promoDiscountPanelCredit").hide();
		} else {
			$(".lblPromoDiscount").text(UI_AvailabilitySearch.labels.lblPromoDiscountCredit);
			$(".promoDiscountAmount").text(discountAmount + " " + curr);
			$("#promoDiscountPanelCredit").show();
			$("#promoDiscountPanel").hide();
		}
		
		$("#tempPromoBanner").remove();
		if(fareQuote.deductFromTotal){
			$("#lblPromoDiscount").text(UI_AvailabilitySearch.labels.lblPromoDiscount);
		} else {
			$("#lblPromoDiscount").text(UI_AvailabilitySearch.labels.lblPromoDiscountCredit);
		}
	
		if( UI_AvailabilitySearch.promotionInfo != null){			
			var strMSG = UI_AvailabilitySearch.promotionInfo.description;
			if(strMSG != undefined && strMSG != null){				
				var discountType = UI_AvailabilitySearch.promotionInfo.discountType;
				var strDiscountBnr = '';
				if(discountType == UI_AvailabilitySearch.promoDiscountType.PERCENTAGE){
					strDiscountBnr = UI_AvailabilitySearch.promotionInfo.farePercentage + '%';
				} else if(discountType == UI_AvailabilitySearch.promoDiscountType.VALUE) {
					strDiscountBnr = curr + ' ' + discountAmount;
				}
				
				strMSG = strMSG.replace("{amount}", strDiscountBnr);
				strMSG = strMSG.replace("{Amount}", strDiscountBnr);
				
				
				var tBanner = $("<span id='tempPromoBanner' class='tbanner'>"+strMSG+"</span>").addClass("spMsg");
				
				var topPx = 125;
				if($("#tempDiscountBanner") != null && $("#tempDiscountBanner").length > 0){
					topPx = 220;
				}
				$(".rightColumn").before(tBanner);
				UI_AvailabilitySearch.rePositionBanner(tBanner,topPx);
			}
		}
		
	} else {
		$("#promoDiscountPanel").hide();
		$("#tempPromoBanner").remove();
	}
	
}
// *** set the banner posstion***/
UI_AvailabilitySearch.rePositionBanner = function(tBanner,topPx){

	var pos = $("#tabs1").position();
	if (tBanner==undefined){
		topPx = 125;
		$(".tbanner").each(function(){
			var tBanner = $(this);
			tBanner.css({
				"position":"absolute",
				"top":pos.top + topPx,
				"left": pos.left + 755,
				"borderRadius":8,
				"width":160
			});
			topPx += 75;
		});
	}else{
		tBanner.css({
			"position":"absolute",
			"top":pos.top + topPx,
			"left": pos.left + 755,
			"borderRadius":8,
			"width":160
		});
	}
}

/**
 * Price Break down Template temporally moved to the
 * availabilitySearchSupport.js this function is support to disply the Price
 * Break down UI all UI design specific functions in availabilitySearch.js can
 * be moved availabilitySearchSupport.js
 */


/**
 * Tax break down template also moved to availabilitySearchSupport.js.
 */


/**
 * Recalculate fare
 */
UI_AvailabilitySearch.userSelected = "";
UI_AvailabilitySearch.reCalculateFare = function(e, typ) {	
	 if(UI_AvailabilitySearch.clientValidateAS(e, "fareCal")) { 
		var rphData = UI_AvailabilitySearch.getRPHData(typ, e);
		UI_AvailabilitySearch.userSelected = typ;
		if(rphData){			 
			 UI_commonSystem.loadingProgress();
			 if (rphData.system!=undefined){
				 $("#resSearchSystem").val(rphData.system);	
			 }

			 var data = createSearchSubmitData();
			 var url = 'loadFareQuote.action';
			 if (UI_AvailabilitySearch.isRequoteFlightSearch) {
				 
				 var newFlightSegRef = UI_AvailabilitySearch.getNewFlightSegRef();	
					
				 if(newFlightSegRef!=null && newFlightSegRef!=""){		
						
					 var existingCnfSeg = jQuery.parseJSON($("#resOldAllSegments").val());		
						
					 for(var i=0;i < existingCnfSeg.length;i++){
						 existingCnfSeg[i].returnFlag = existingCnfSeg[i].returnFlag == "Y"?true:false;
					 }

					 var modifyFlightNos = $("#resModifySegmentRefNos").val();		
					 newONDSegmentList = UI_AvailabilitySearch.getRequoteOnds({existingFltSegList:existingCnfSeg,modFltSegNo:modifyFlightNos});
				 }
				 
				 data  =  $.airutil.dom.concatObjects(data, rphData.data);		
				 
				 var ondSearchDTOList = UI_AvailabilitySearch.getONDSearchDTOList();
				 UI_AvailabilitySearch.overrideBundledServicesForRequote(ondSearchDTOList);
				 
				 data['cancelSegment'] = UI_AvailabilitySearch.isCnxSegmentRequote;
				 data['nameChange'] = UI_AvailabilitySearch.isNameChangeRequote;
				 data['searchParams.ondListString'] = $.toJSON(ondSearchDTOList);
				 data['searchParams.ticketValidTillStr'] = UI_AvailabilitySearch.getTicketValidTill();
				 data['searchParams.lastFareQuoteDateStr'] = UI_AvailabilitySearch.getLastFareQuoteDate();		
				 
				 if(UI_AvailabilitySearch.isCalendarViewDisabled()){
					 data['searchParams.fareQuoteLogicalCCSelection'] = $("#resFareQuoteLogicalCCSelection").val();
					 data['searchParams.ondQuoteFlexiStr'] = $("#resOndQuoteFlexi").val();
					 data['searchParams.ondSelectedFlexiStr'] = $("#resOndSelectedFlexiStr").val();
					 data['searchParams.flightSearchOndFlexiSelectionStr'] = $("#resflightSearchOndFlexiSelection").val();
					 data['searchParams.preferredBundledFares'] = $("#resOndBundleFareStr").val();
					 data['searchParams.preferredBookingCodes'] = $("#resOndBookingClassStr").val();
					 data['searchParams.ondSegBookingClassStr'] = $("#resOndSegBookingClassStr").val();
				 }
				 
				 $("#resOndListStr").val(ondSearchDTOList);
				 // temp
				 data['searchParams.searchSystem']= UI_AvailabilitySearch.getSelectedSystem();

				 url = 'fareRequote.action';
			 }
			 setCookieData(data);
			 data  =  $.airutil.dom.concatObjects(data, rphData.data);
			 $.ajax({type: "POST", dataType: 'json',data: data , url:url, async: false,		
			 success: UI_AvailabilitySearch.reCalculateFareProcess, error:UI_commonSystem.setErrorStatus, cache : false, beforeSubmit:UI_commonSystem.loadingProgress});
			
		}else{
			$("#trPriceBDPannel").slideUp("slow");	
		}
		return false;
	 } else{
		 return "faild";
	 } 
	 
}

/**
 * Get RPH Data
 */
UI_AvailabilitySearch.getRPHData = function(typ, e) {
	 var  segmentsRPHListData = ""; UI_AvailabilitySearch.getSelectedFlight();
	 var data = {};
	 var systemOut = "";
	 var systemIn = "";
	 if (UI_AvailabilitySearch.outFlightRPHList != null && UI_AvailabilitySearch.outFlightRPHList != "") {
		 var systemRPHData = UI_AvailabilitySearch.outFlightRPHList.split("!");
		 systemOut = systemRPHData[1];
		 segmentsRPHListData = systemRPHData[0].split(":");
		 for(var i = 0; i < segmentsRPHListData.length; i++) {
			 data["outFlightRPHList[" + i + "]"] = segmentsRPHListData[i];
		 }	 
	 } else {
		 data["outFlightRPHList"] = "";
	 }
	 
	 if (UI_AvailabilitySearch.retFlightRPHList != null && UI_AvailabilitySearch.retFlightRPHList != "") {
		 var systemRPHData = UI_AvailabilitySearch.retFlightRPHList.split("!");
		 systemIn = systemRPHData[1];
		 segmentsRPHListData = systemRPHData[0].split(":");
		 for(var i = 0; i < segmentsRPHListData.length; i++) {
			 data["retFlightRPHList[" + i + "]"] = segmentsRPHListData[i];
		 }	 
	 } else {
		 data["retFlightRPHList"] = "";
	 }	
	 
	 if (systemIn != "" && systemOut != systemIn )  {
		 systemOut = 'INT';
	 }
	 
	 return {data:data,system:systemOut};
}


UI_AvailabilitySearch.setAvailableLogicalCabinClasses = function(ondLogicalCCList){
	setAvailableLogicalCabinClasses(ondLogicalCCList);
/*	if(ondLogicalCCList != undefined && ondLogicalCCList != null){
		for ( var i = 0; i < ondLogicalCCList.length; i++) {
			var ondLogicalCCObj = ondLogicalCCList[i][0];
			if(ondLogicalCCObj != undefined && ondLogicalCCObj != null){					
				var ondSeq = ondLogicalCCObj.sequence;
				var availableLogicalCCs = ondLogicalCCObj.availableLogicalCCList;
				var parentElem = $('#ondLogicalCC_' + ondSeq);
				
				if(availableLogicalCCs != undefined && availableLogicalCCs != null){
					parentElem.empty();	
					isLogicalCCSelected = false;
					for ( var j = 0; j < availableLogicalCCs.length; j++) {
						var logicalCCObj = availableLogicalCCs[j];
						var value = ondSeq + "_" + logicalCCObj.logicalCCCode;						
						value += (logicalCCObj.withFlexi) ? '_Y' : '_N';
						value += '_' + logicalCCObj.bundledFarePeriodId + '_' + logicalCCObj.bookingCode;
						
						var title = logicalCCObj.comment;
						var name = 'logical_' + ondSeq;
						
						var logicalRadio = $("<input>").attr({"type":"radio",
							"id":ondSeq + '_' + j,
							"value" : value,
							"title" :title,
							"name" : name,
							"checked": logicalCCObj.selected});
						
						//Set Default selection
						if(logicalCCObj.selected){																
							UI_AvailabilitySearch.setLogicalCCSelection({ondSeq: ondSeq, logicalCCCode: logicalCCObj.logicalCCCode,
								withFlexi: logicalCCObj.withFlexi, bundledFarePeriodId: logicalCCObj.bundledFarePeriodId, 
								bookingClass: logicalCCObj.bookingCode});
						}
							
						logicalRadio.unbind('click').click(function(e){
							isLogicalCCSelected = true;
							var arrVal = e.target.value.split("_");
							$("#trPriceBDPannel").slideUp("slow");	
							$("#balanceSummary").slideUp("slow");		
							$("#trAccept").hide();
							$("#btnContinue").hide();
							$("#requoteBtnSet").show();
							$("#btnRequote").show();
							
							var ondSeq = parseInt(arrVal[0], 10);
							var logicalCCCode = arrVal[1];
							var withFlexi = true;
							if(arrVal[2] != "Y"){
								withFlexi = false;
							}
							
							var bundledFarePeriodId = null;
							if(arrVal[3] != '' && arrVal[3] != 'null'){
								bundledFarePeriodId = arrVal[3];
							}
							
							var bookingClass = null;
							if(arrVal[4] != '' && arrVal[4] != 'null'){
								bookingClass = arrVal[4];
							}
							
							UI_AvailabilitySearch.setLogicalCCSelection({ondSeq: ondSeq, logicalCCCode: logicalCCCode,
								withFlexi: withFlexi, bundledFarePeriodId: bundledFarePeriodId, 
								bookingClass: bookingClass});
							
						});
						
						var labelElem = $("<label>"+logicalCCObj.logicalCCDesc+"</label>").attr({"title" : title}); 
						
						
						parentElem.append(logicalRadio);
						parentElem.append(labelElem);
						parentElem.append('&nbsp;&nbsp;');
						
					}
				}
			}
			
		}
		$(".clsLcc").show();
	}*/
}

UI_AvailabilitySearch.setLogicalCCSelection = function(params){	
	
	UI_AvailabilitySearch.segWiseBookingClassOverride[params.ondSeq] = null;
	UI_AvailabilitySearch.ondWiseBookingClassSelected[params.ondSeq] = null;
	if(params.bookingClasses != null){    		
		if(params.bookingClasses.length > 1){
			UI_AvailabilitySearch.segWiseBookingClassOverride[params.ondSeq] = params.segBookingClasses;
		} else {
			UI_AvailabilitySearch.ondWiseBookingClassSelected[params.ondSeq] = params.bookingClasses[0];
		}
	}
	
	UI_AvailabilitySearch.ondWiseLogicalCCSelected[params.ondSeq] =  params.logicalCCCode;
	UI_AvailabilitySearch.ondWiseFlexiSelected[params.ondSeq] = params.withFlexi;
	UI_AvailabilitySearch.ondWiseBundleFareSelected[params.ondSeq] = params.bundledFarePeriodId;
	
	$("#resFareQuoteLogicalCCSelection").val($.toJSON(UI_AvailabilitySearch.ondWiseLogicalCCSelected));
	$("#resOndSelectedFlexiStr").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected));
	//Keep track of flexi selection done in flight search page
	$("#resflightSearchOndFlexiSelection").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected));
	$("#resOndBundleFareStr").val($.toJSON(UI_AvailabilitySearch.ondWiseBundleFareSelected));
	$("#resOndBookingClassStr").val($.toJSON(UI_AvailabilitySearch.ondWiseBookingClassSelected));
	$("#resOndSegBookingClassStr").val($.toJSON(UI_AvailabilitySearch.segWiseBookingClassOverride));
}


/**
 * Process Recalculate Fare
 */
UI_AvailabilitySearch.reCalculateFareProcess = function(response){
	if(response != null && response.success == true) {
		UI_AvailabilitySearch.promotionInfo = response.promoInfo;
		if(response.promoInfo != null){
			var promo = response.promoInfo;
			UI_AvailabilitySearch.isBINPromotion = (promo.applicableBINs!= null && promo.applicableBINs.length > 0) ;
			$("#promoDiscountErrPanel").hide();
		} else{
			var data = createSearchSubmitData();
			if(data['searchParams.promoCode'] != ""){
				$("#promoDiscountErrPanel").show();
				
				$("#lblPromoError").text(UI_AvailabilitySearch.promoCodeNotFound);
			
			}
			else{
				$("#promoDiscountErrPanel").hide();
			}
		}
		if (response.paymentMethods!=null){
			UI_AvailabilitySearch.paymentMethods=response.paymentMethods;
		}
		// display onholdbooking message
		UI_AvailabilitySearch.displayPaymentMethods(response.allowOnhold);
		$("#requestSessionIdentifier").val(response.requestSessionIdentifier);
		if(response.fareQuote != null){
			// if (response.fareQuote.fareType != 0){
			if (true){
				
				if(!UI_AvailabilitySearch.isCalendarViewDisabled()){			
					var selFare;
					if(!UI_AvailabilitySearch.isCnxSegmentRequote && !UI_AvailabilitySearch.isNameChangeRequote){
						selFare = UI_AvailabilitySearch.getSelectedFlightFares();
						var preferredBundleAvailable = true;
						var submitData = createSearchSubmitData();
						$.each($.parseJSON(submitData['searchParams.preferredBundledFares']), function(key, value){
							if(value != null && value != 'null' && $.trim(value) != ''){
								var ondSequence = parseInt(key,10);
								var prefBundle = parseInt(value,10);
								var found = false;
								if(response.ondLogicalCCList.length > ondSequence){
									var availableLCCs = response.ondLogicalCCList[ondSequence];
									for( var i = 0 ; i < availableLCCs.length; i++){
										var bundles = availableLCCs[i].availableLogicalCCList;
										for ( var j = 0 ; j < bundles.length; j++){
											if(prefBundle == bundles[j].bundledFarePeriodId){
												found = true; 
												break;
											}
										}
									}
								}
								if(!found){
									preferredBundleAvailable = false;
									return false;
								}
							}
							return true;
						});
						if(!preferredBundleAvailable){
							response.fareQuote = null;
							UI_AvailabilitySearch.reCalculateFareProcess(response);
							return ;
						}
						if(selFare.outFare != response.outboundCalFare){				
							$.fn.calendarview.changeSelectedFare({outbound:response.outboundCalFare});
						}
						if(selFare.inFare!=null && selFare.inFare != response.inboundCalFare){				
							$.fn.calendarview.changeSelectedFare({inbound:response.inboundCalFare});
						}
						
					}
				} else {
					UI_AvailabilitySearch.setAvailableLogicalCabinClasses(response.ondLogicalCCList);	
					$.each(response.ondLogicalCCList,function(index,logicalCC){
						var ondLogicalCCObj = logicalCC[0];
			            if(ondLogicalCCObj != undefined && ondLogicalCCObj != null){
			            	var ondAvailbleBundleFareArray = new Array();
			                var availableLogicalCCs = ondLogicalCCObj.availableLogicalCCList;
			                for ( var i = 0; i < availableLogicalCCs.length; i++) {
								var lccObj = availableLogicalCCs[i];
			                    if(lccObj.bundledFarePeriodId !== undefined){
			                        if(lccObj.bundledFarePeriodId!== null){
			                            ondAvailbleBundleFareArray[i] = new Array();
			                            ondAvailbleBundleFareArray[i].push({"bundledFarePeriodId":lccObj.bundledFarePeriodId ,
			                                "bundleFareFee":lccObj.bundledFareFee ,
			                                "bundleFareFreeServices":lccObj.bundledFareFreeServiceName,
			                                "bundleFareName":lccObj.logicalCCDesc,
			                                "imageUrl":setBundleFareImageURL(lccObj)});
			                        }
			                    }
							}
			                UI_AvailabilitySearch.availableBundleFareLCClass[ondLogicalCCObj.sequence] = ondAvailbleBundleFareArray;
			            }
					});
					$("#resAvailableBundleFareLCClassStr").val($.toJSON(UI_AvailabilitySearch.availableBundleFareLCClass));
				}
				
				UI_AvailabilitySearch.setSelectedBookingClass(response);
				
				UI_AvailabilitySearch.fareQuote = response.fareQuote;
				UI_AvailabilitySearch.showFareRules = response.showFareRules;
				UI_AvailabilitySearch.fareRules = response.fareRules;
				UI_AvailabilitySearch.setFare();
			
				UI_AvailabilitySearch.fareQuoteOutFlightRPHList = response.outFlightRPHList;
				UI_AvailabilitySearch.fareQuoteRetFlightRPHList = response.retFlightRPHList;	
				UI_AvailabilitySearch.isRequoteFlightSearch = response.requoteFlightSearch;

				UI_AvailabilitySearch.showHideRequoteData({isFareAvailable:true});
				
				// Set Airport Message From response to show on screen
				UI_AvailabilitySearch.airportMessage = response.airportMessage;
				UI_AvailabilitySearch.loadAirportSearchMessages();
				UI_commonSystem.loadingCompleted();
				
				if (!UI_AvailabilitySearch.isCnxSegmentRequote && UI_AvailabilitySearch.isModifySegment  && UI_AvailabilitySearch.fareQuote != null &&
						UI_AvailabilitySearch.fareQuote.segmentFare != null && !UI_AvailabilitySearch.isNameChangeRequote) {
						UI_AvailabilitySearch.showBalcenceSummary();
				}
				
				UI_AvailabilitySearch.isFromPostCardPage = response.fromPostCardPage;
				UI_AvailabilitySearch.msgFromPostCardPage = response.msgFromPostCardPage;

				if(UI_AvailabilitySearch.isRequoteFlightSearch && 
						(UI_AvailabilitySearch.isCnxSegmentRequote || UI_AvailabilitySearch.isNameChangeRequote)){
		    		//for Airarabia no need to display flight details directed to balance summarry
					UI_AvailabilitySearch.calCNXAndNameChangeBalSummary(); 
				}
				
				if(UI_Top.holder().GLOBALS.saveCookieEnable && !UI_AvailabilitySearch.isCnxSegmentRequote && 
						!UI_AvailabilitySearch.isNameChangeRequote){
					setCookie("saveSearchCookie" ,JSON.stringify(response.searchParamsCookieDetails));
				}
				
			}else{
				UI_AvailabilitySearch.fareQuote = "";
				$("#trPriceBDPannel").slideUp("slow");
				jAlert(UI_AvailabilitySearch.labels.faresNotAvailable, 'Alert');
				if(response.forceRefresh){
					// UI_AvailabilitySearch.loadAvailabilitySearchData();
					if(!UI_AvailabilitySearch.isCalendarViewDisabled()){						
						if(UI_AvailabilitySearch.userSelected == "departure"){				
							$("#fCalIn_FL").find("input[type='radio']").attr("checked", false);
						}else{
							$("#fCalOut_FL").find("input[type='radio']").attr("checked", false);
						}
					}
				}
				UI_AvailabilitySearch.showHideRequoteData({isFareAvailable:false});
				UI_commonSystem.loadingCompleted();
			}
			
			UI_AvailabilitySearch.ondWiseFlexiAvailability = response.ondWiseTotalFlexiAvailable;
			UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr = response.ondWiseTotalFlexiCharge;
			
			$('#resOndAvailbleFlexiStr').val($.toJSON(response.ondWiseTotalFlexiAvailable));
			$("#resOndwiseFlexiChargesForAnci").val($.toJSON(response.ondWiseTotalFlexiCharge));
		}else{
			UI_AvailabilitySearch.fareQuote = "";
			$("#trPriceBDPannel").slideUp("slow");
			jAlert(UI_AvailabilitySearch.labels.faresNotAvailable, 'Alert');
			if(response.forceRefresh){
				if(UI_AvailabilitySearch.userSelected == "departure"){				
					$("#fCalIn_FL").find("input[type='radio']").attr("checked", false);
				}else{
					$("#fCalOut_FL").find("input[type='radio']").attr("checked", false);
				}
			}

			UI_AvailabilitySearch.showHideRequoteData({isFareAvailable:false});
			UI_commonSystem.loadingCompleted();
		}
	} else if (response != null) {
		UI_AvailabilitySearch.fareQuote = "";
		$("#trPriceBDPannel").slideUp("slow");
		var msg = UI_AvailabilitySearch.labels[response.messageTxt];
		msg = (msg==null?response.messageTxt:msg);
		jAlert(msg, 'Alert');
		if(response.forceRefresh){
			if(UI_AvailabilitySearch.userSelected == "departure"){				
				$("#fCalIn_FL").find("input[type='radio']").attr("checked", false);
			}else{
				$("#fCalOut_FL").find("input[type='radio']").attr("checked", false);
			}
		}

		UI_AvailabilitySearch.showHideRequoteData({isFareAvailable:false});
		UI_commonSystem.loadingCompleted();
	} else {
		// UI_commonSystem.loadErrorPage({messageTxt:""});
		wrapAndDebugError('003',response);
	}	
	
	
	
}

UI_AvailabilitySearch.loadPaxPage  = function() {
	// System Down Message
	if(UI_Top.holder().GLOBALS.isCreditCardPaymentsEnabled != true && UI_Top.holder().GLOBALS.isReservationOnholdEnabled != true){		
		jAlert(UI_AvailabilitySearch.msgCanNotReserve,'Alert');
		return;
	}	

	if(UI_AvailabilitySearch.isCnxSegmentRequote || UI_AvailabilitySearch.isNameChangeRequote) {
		
		if (UI_AvailabilitySearch.fareQuote == null || UI_AvailabilitySearch.fareQuote == "") {
			jAlert(UI_AvailabilitySearch.errorInfo["ERR039"],'Alert');
			return false;
		}	
		
		if (UI_AvailabilitySearch.isCaptchaEnabled == false){
			UI_AvailabilitySearch.CaptureSuccess({capturePass:true})
		}else {	
			UI_AvailabilitySearch.isValideImageChapcha();				
		}
		
	} else {
		// Validation
		if( UI_AvailabilitySearch.isModifySegment && !UI_AvailabilitySearch.paxDOBValidation()){
			return false;
		}	
		if (UI_AvailabilitySearch.clientValidateAS()) {
			if (!UI_AvailabilitySearch.fareQuoteValidation()){
				jAlert(UI_AvailabilitySearch.errorInfo["ERR039"],'Alert');
				return false;
			}	
			else if(!$("#chkTerms").is(':checked')){		
				jAlert(UI_AvailabilitySearch.errorInfo["ERR042"],'Alert');
				return false;
			}
			else if(UI_AvailabilitySearch.isCaptchaEnabled && ($("#txtCaptcha").val() == null || $("#txtCaptcha").val() == "")) {
				UI_AvailabilitySearch.refresh();
				jAlert(UI_AvailabilitySearch.labels.lblTypeCorrectImage,'Alert');
				return  false;
			} else {
				 // Modify Segment validation
				 if (UI_AvailabilitySearch.isModifySegment) {
					// Check Balance Summary Exist
					if (UI_AvailabilitySearch.isModifyBalanceSuccess == false) {
						jAlert("Error in modification, please try again",'Alert');
						return false;
					} else if(UI_AvailabilitySearch.isSameFlightModification() && UI_Top.holder().GLOBALS.allowSameFlightModificationInIBE == false){
						jAlert("You have selected same flight for modification. Please change your flight.",'Alert');
						return false;
					}
				 }
			
				 if (UI_AvailabilitySearch.isFromPostCardPage) {				 
					 var isConfim = confirm(UI_AvailabilitySearch.msgFromPostCardPage);
					 if (!isConfim) {
						 return false;
					 }				 
				 }
			
				if (UI_AvailabilitySearch.isCaptchaEnabled == false){
					UI_AvailabilitySearch.CaptureSuccess({capturePass:true})
				}else {	
					UI_AvailabilitySearch.isValideImageChapcha();				
				}
			}   
		
		}	
	}
	
}  


UI_AvailabilitySearch.paxDOBValidation = function() {	
	
	var adults = paxJason.adults ;
	var children = paxJason.children ;
	var infants = paxJason.infants ;	
	var selectedOutSegments = UI_AvailabilitySearch.getSelectedFlightSegments("outbound");
	var departureDate = new Date(selectedOutSegments[0].departureTimeLong);
	var strDepatureDate = DateToString(departureDate);
	UI_AvailabilitySearch.loadPaxValidation ();
	var paxValidation = UI_AvailabilitySearch.paxValidation;
	
	if ((paxJason != null) && (paxJason != "") && (paxValidation !=null)) {
		
		
		for ( var i = 0; i < adults.length; i++) {
			
			if( (adults[i].dateOfBirth != "") && (adults[i].dateOfBirth != null) ){
				if (!ageCompare(adults[i].dateOfBirth,
						strDepatureDate, paxValidation.adultAgeCutOverYears)) {
					jAlert("Adult "+adults[i].firstName+" does not fall into our Adult category");
					$("#departureDateBox").focus();	
					return false;
				}
				
				if (ageCompare(adults[i].dateOfBirth,
						strDepatureDate, paxValidation.childAgeCutOverYears)) {
					jAlert("Adult "+adults[i].firstName+" does not fall into our Adult category");
					$("#departureDateBox").focus();	
					return false;
				}
			}
			
			
		}
		
		for (var i = 0; i < children.length; i++) {
			
			if( (children[i].dateOfBirth != "") && (children[i].dateOfBirth != null) ){
				if (!ageCompare(children[i].dateOfBirth,
						strDepatureDate, paxValidation.childAgeCutOverYears)) {
					jAlert("Child "+children[i].firstName+" does not fall into our Child category");
					$("#departureDateBox").focus();	
					return false;
				}
				
				if (ageCompare(children[i].dateOfBirth,
						strDepatureDate, paxValidation.infantAgeCutOverYears)) {
					jAlert("Child "+children[i].firstName+" does not fall into our Child category");
					$("#departureDateBox").focus();	
					return false;
				}
			}
		}
		
		for ( var i = 0; i < infants.length; i++) {
			
			if( (infants[i].dateOfBirth != "") && (infants[i].dateOfBirth != null) ){
				if (!ageCompare(infants[i].dateOfBirth,
						strDepatureDate, paxValidation.infantAgeCutOverYears)) {
					jAlert("Infant "+infants[i].firstName+" does not fall into our Infant category");
					$("#departureDateBox").focus();	
					return false;
				}	
			}
		}
	}			
	
	
	return true;
}

UI_AvailabilitySearch.createCarrierList = function(){
	var filghtSegments = UI_AvailabilitySearch.getSelectedFlightSegments("both");
	var operatingCarriers = '';
	if(filghtSegments != []){
		for(var i=0;i<filghtSegments.length;i++){	
			if((operatingCarriers = '') || (operatingCarriers.search(filghtSegments[i].carrierCode < 0))){				
				if(i == (filghtSegments.length - 1)){
					operatingCarriers += filghtSegments[i].carrierCode;					
				} else {
					operatingCarriers += filghtSegments[i].carrierCode + ",";
				}				
			}			
		}
	}
	
	return operatingCarriers;
}


UI_AvailabilitySearch.loadPaxValidation = function(){
	
	var data = new Array();
	data['carriers'] = UI_AvailabilitySearch.createCarrierList();
	data['system'] = UI_AvailabilitySearch.getSelectedSystem();
	UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, 
			url:'loadPaxContactConfig!loadPaxValidation.action', async:false,
			success:
            function (response){
            UI_AvailabilitySearch.paxValidation = response.paxValidation;
            	}, 
			error:UI_commonSystem.setErrorStatus});
}

UI_AvailabilitySearch.CaptureSuccess = function (response){
	if (response.capturePass == true){
		var fareQuote = $.airutil.dom.cloneObject(UI_AvailabilitySearch.fareQuote);
		var promoInfo = $.airutil.dom.cloneObject(UI_AvailabilitySearch.promotionInfo);
		$('#respromotionInfo').val($.toJSON(promoInfo));
		UI_AvailabilitySearch.updateSearchParams();
		$('#resFareQuote').val($.toJSON(fareQuote));
		var tJosn = $.toJSON({flightSegments:UI_AvailabilitySearch.getSelectedFlightSegments("both"), 
			isReturnFlight:UI_AvailabilitySearch.returnFlag});
		
		// French translation issue fix for Arrival & Departure dates - Supports all languages  
		var nJson = jQuery.parseJSON(tJosn);
		
		if(typeof(nJson.flightSegments) !== "undefined" && nJson.flightSegments.length > 0){
			for(var i = 0; i < nJson.flightSegments.length;i++){
				if(typeof(nJson.flightSegments[i]) !== "undefined"){
					nJson.flightSegments[i].arrivalDate = escape(nJson.flightSegments[i].arrivalDate);
					nJson.flightSegments[i].departureDate = escape(nJson.flightSegments[i].departureDate);
				}
			}
		}
		
		tJosn = $.toJSON(nJson);

		if(UI_AvailabilitySearch.isRequoteFlightSearch){			
			$('#resSelectedFlights').val($.toJSON({flightSegments:UI_AvailabilitySearch.setSelectedFlightInfo(newONDSegmentList),isReturnFlight:false}));			
			$('#modifySegment').val(true);			
			$('#balanceQueryData').val($('#balanceQueryData').val());
			$('#resOndListStr').val($.toJSON(UI_AvailabilitySearch.getONDSearchDTOList()));
			$('#resTicketValidTill').val(UI_AvailabilitySearch.getTicketValidTill());
			$('#resLastFareQuoteDate').val(UI_AvailabilitySearch.getLastFareQuoteDate());

		} else {
			$('#resSelectedFlights').val(tJosn);
			// TODO:RW re-factor this to send ond sequence wise flexi selection
			var ondQuoteFlexiSelected = {};
			if($("#resFlexiSelected").val() != null && $("#resFlexiSelected").val() != ''){
				var ondQuoteFlexiSelected = $.parseJSON($("#resFlexiSelected").val());
			} else {
				ondQuoteFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = false;
				ondQuoteFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = false;
			}
			$('#resFlexiSelection').val($.toJSON({
				outBoundFlexiSelection:ondQuoteFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND], 
				outSelectedFare:UI_AvailabilitySearch.outSelectedFareType,
				inBoundFlexiSelection:ondQuoteFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND], 
				inSelectedFare:UI_AvailabilitySearch.retSelectedFareType}));
		} 
		
		if(($("#resPaxType").val() == '') && ( $("#selPaxType").val() != '')){
		 $("#resPaxType").val($("#selPaxType").val());
		}
		var hidParam = '';
		if(!UI_AvailabilitySearch.isCnxSegmentRequote && !UI_AvailabilitySearch.isNameChangeRequote){
			// added for cirteo pixel to send the product is genarated
					hidParam += $("<input type='hidden'></input>").attr({
						"id":"hidProductID",
						"name":"hidProductID",
						"value":UI_commonSystem.genarateProductID({flightSegments: UI_AvailabilitySearch.getSelectedFlightSegments("both")})
					});
		}
		
		// populate PGW related Data
		if(strPGWPaymentMobileNumber != undefined && strPGWPaymentEmail != undefined && strPGWPaymentCustomerName != undefined){
			$('#pgwPaymentMobileNumber').val(strPGWPaymentMobileNumber);
			$('#pgwPaymentEmail').val(strPGWPaymentEmail);
			$('#pgwPaymentCustomerName').val(strPGWPaymentCustomerName);	
		}
		
		$('#frmFare').attr('action',SYS_IBECommonParam.securePath+'loadContainer.action');
		$('#frmFare').append(hidParam);
		
		if(fromSocialSites != undefined && fromSocialSites != "" && fromSocialSites && fromSocialSites =='true'){
			$('#frmFare').attr('target','_self');
		}else{
			$('#frmFare').attr('target','_top');	
		}
		$('#frmFare').submit();	
	}else{
		UI_AvailabilitySearch.refresh();
		jAlert(UI_AvailabilitySearch.labels.lblTypeCorrectImage,'Alert');
	}
}

UI_AvailabilitySearch.updateSearchParams = function() {
	var outList = UI_AvailabilitySearch.outFlightRPHList;
	var retList = UI_AvailabilitySearch.retFlightRPHList;
	if (outList != null && outList != "") {	
		var dataArr = outList.split("$");
		if (dataArr.length >= 4) {
			var departureDate = outList.split("$")[3];
			if (departureDate.length >= 8) {			
				$("#resDepartureDate").val(departureDate.substring(6, 8) + "/" + departureDate.substring(4, 6) +  "/" + departureDate.substring(0, 4));
			}
		}	
	}
	
	if (retList != null && retList != "") {	
		var dataArr = retList.split("$");
		if (dataArr.length >= 4) {
			var returnDate = retList.split("$")[3];
			if (returnDate.length >= 8) {			
				$("#resReturnDate").val(returnDate.substring(6, 8) + "/" + returnDate.substring(4, 6) +  "/" + returnDate.substring(0, 4));
			}
		}	
	}
}


/**
 * Fare Quote Validation
 */
UI_AvailabilitySearch.fareQuoteValidation = function() {	
	if (UI_AvailabilitySearch.fareQuote == null || UI_AvailabilitySearch.fareQuote == "") {
		return false;
	}else {
		return true;
	}
}

/**
 * GetRPH Data Array
 */
UI_AvailabilitySearch.getRPHArrayString = function(flightRPHList) {
	 if (flightRPHList != null && flightRPHList != "") {		
		 return flightRPHList.split("-")[0];
	 } else 
		 return "";
}


/**
 * Combine Segments inbound and outbound
 */
UI_AvailabilitySearch.getSelectedFlightSegments = function(way) {	
	var selected = null
	if (way == "outbound")
		selected = UI_AvailabilitySearch.getSelectedFlight(UI_AvailabilitySearch.outFlightRPHList, UI_AvailabilitySearch.outboundFlights);
	else if(way == "inbound")
		selected = UI_AvailabilitySearch.getSelectedFlight(UI_AvailabilitySearch.retFlightRPHList, UI_AvailabilitySearch.inboundFlights)
	else{
		selected = UI_AvailabilitySearch.getSelectedFlight(UI_AvailabilitySearch.outFlightRPHList, UI_AvailabilitySearch.outboundFlights);
		selected = selected.concat(UI_AvailabilitySearch.getSelectedFlight(UI_AvailabilitySearch.retFlightRPHList, UI_AvailabilitySearch.inboundFlights));
	}
	return selected;
}
/**
 * create Logical cabin class map inbound and outbound
 */
UI_AvailabilitySearch.getSelectedFlightLogicalCCMap = function(){
	var _selectedflightLCOS = new Object();
	//Set BundleFareIDs
	UI_AvailabilitySearch.ondWiseBundleFareSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = "";
	UI_AvailabilitySearch.ondWiseBundleFareSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = "";
	//Setting BookingClass 
	UI_AvailabilitySearch.ondWiseBookingClassSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = null;
	UI_AvailabilitySearch.ondWiseBookingClassSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = null;
	var getFlightSeglist = function(CalObj,ondSequence){
		var lccid = null;
		flRefIds = [];
		selFlight = CalObj.find("input[type='radio']:checked").parent();
		lccid = selFlight.find(".flightLCOS").text()
		flRefIds = selFlight.find(".flightRefId").text().split(":");
		for (var t=0;t<flRefIds.length;t++){
			_selectedflightLCOS[ondSequence] = lccid;
		}
		/*
		 * Set WithFlexiParams
		 */
		if(CalObj.find("input[type='radio']:checked").attr("id") != undefined){			
			var wFlxi = CalObj.find("input[type='radio']:checked").attr("id").split("_")[5];
			var fFlxi = CalObj.find("input[type='radio']:checked").attr("id").split("_")[6];
			var isWithFlexiNotFree = (wFlxi == 'Y' && fFlxi != 'Y') ? true : false;
			var isWithFlexi = (wFlxi == 'Y') ? true : false;
			var bundledFarePeriodId =  CalObj.find("input[type='radio']:checked").attr("id").split("_")[7]	;
			var bookingClasses =  CalObj.find("input[type='radio']:checked").attr("id").split("_")[8];
			var segmentBookingClasses =  CalObj.find("input[type='radio']:checked").attr("id").split("_")[9];
			
			bookingClasses = $.parseJSON(bookingClasses);
			if(bookingClasses == undefined || bookingClasses == 'undefined' || bookingClasses == 'null' || bookingClasses == ''){
				bookingClasses = null;
			}
						
			var ondSeq = 0;
			
			if(CalObj.attr('id') == 'fCalOut_FL'){	
				ondSeq = UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND;
				if((UI_AvailabilitySearch.isRequoteFlightSearch || UI_AvailabilitySearch.isModifySegment)
						&& $("#resModFlexiSelected").val() == 'true'){
					UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = false;					
				} else {					
					UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = isWithFlexi;
				}

				if(bundledFarePeriodId != null && bundledFarePeriodId != '' && bundledFarePeriodId != 'null'){
					UI_AvailabilitySearch.outSelectedFareType = getBundleFareImageURL(0, bundledFarePeriodId);
				} else {
					if(UI_AvailabilitySearch.isLogicalCabinClassEnabled == true){
						if (isWithFlexiNotFree){
							UI_AvailabilitySearch.outSelectedFareType = fareType_IMG_PATH.replace("http:", "") + "flexiFare"; //"flexiFare"
						}else if(lccid == "Y"){
							UI_AvailabilitySearch.outSelectedFareType = fareType_IMG_PATH.replace("http:", "") + "promoFare"; //"promoFare"
						}else if(lccid == "J"){UI_AvailabilitySearch.outSelectedFareType = fareType_IMG_PATH.replace("http:", "") + "primiFare"; //"primiFare"
						}						
					}
				}
			} else if(CalObj.attr('id') == 'fCalIn_FL'){
				ondSeq = UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND;
				if((UI_AvailabilitySearch.isRequoteFlightSearch || UI_AvailabilitySearch.isModifySegment)
						&& $("#resModFlexiSelected").val() == 'true'){					
					UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = false;
				} else {
					UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = isWithFlexi;					
				}

				if(bundledFarePeriodId != null && bundledFarePeriodId != '' && bundledFarePeriodId != 'null'){
					UI_AvailabilitySearch.retSelectedFareType = getBundleFareImageURL(1, bundledFarePeriodId);
				} else {
					if(UI_AvailabilitySearch.isLogicalCabinClassEnabled == true){
						if (isWithFlexiNotFree){
							UI_AvailabilitySearch.retSelectedFareType = fareType_IMG_PATH.replace("http:", "") + "flexiFare"; //"flexiFare"
						}else if(lccid == "Y"){
							UI_AvailabilitySearch.retSelectedFareType = fareType_IMG_PATH.replace("http:", "") + "promoFare"; //"promoFare"
						}else if(lccid == "J"){UI_AvailabilitySearch.retSelectedFareType = fareType_IMG_PATH.replace("http:", "") + "primiFare"; //"primiFare"
						}
					}
				}
				
			}
			
			UI_AvailabilitySearch.ondWiseBundleFareSelected[ondSeq] = bundledFarePeriodId;
			UI_AvailabilitySearch.segWiseBookingClassOverride[ondSeq] = null;
			if(bookingClasses != null && bookingClasses.length > 0){
				var bkClassNum = bookingClasses.length;
				if(bkClassNum == 1){
					UI_AvailabilitySearch.ondWiseBookingClassSelected[ondSeq] = bookingClasses[0];
				} else {
					UI_AvailabilitySearch.segWiseBookingClassOverride[ondSeq] = $.parseJSON(segmentBookingClasses);
				}
			}
			
			$("#resOndSelectedFlexiStr").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected));
			//Keep track of flexi selection done in flight search page
			$("#resflightSearchOndFlexiSelection").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected));
			$("#resOndQuoteFlexi").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiQuote));
			$("#resOndBundleFareStr").val($.toJSON(UI_AvailabilitySearch.ondWiseBundleFareSelected));
			$("#resOndBookingClassStr").val($.toJSON(UI_AvailabilitySearch.ondWiseBookingClassSelected));
			$("#resOndSegBookingClassStr").val($.toJSON(UI_AvailabilitySearch.segWiseBookingClassOverride));
			$("#resOndAvailbleFlexiStr").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiAvailability));
			$("#resOndwiseFlexiChargesForAnci").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr));
			
			var objFlexiSelectionParam =  $("#resFlexiSelection").val();
			var outboundFareType = objFlexiSelectionParam.outSelectedFare;
			var inboundFareType = objFlexiSelectionParam.inSelectedFare;
			var outboundFlxiSelected =  false ;
			var inboundFlxiSelected =  false ;
			
			$('#resFlexiSelection').val($.toJSON({
				outBoundFlexiSelection:UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND], 
				outSelectedFare:outboundFareType,
				inBoundFlexiSelection:UI_AvailabilitySearch.ondWiseFlexiSelected[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND], 
				inSelectedFare:inboundFareType}));
			
		
		}
	}
	//send ond sequence with the flight
	getFlightSeglist( $("#fCalOut_FL"),0 );
	if (UI_AvailabilitySearch.returnFlag == "true"){
		getFlightSeglist( $("#fCalIn_FL"),1 );
	}
	return _selectedflightLCOS;
}

/**
 * Extract Selected Flights
 */

UI_AvailabilitySearch.getSelectedFlight = function(flightRPHList , flights) {
	 var segmentsRPHListData  = "";
	 var segments = [];
	 // Out bound Flights
	 if (flightRPHList != null && flightRPHList != "") {
		 var systemRPH = flightRPHList.split("!");
		 segmentsRPHListData = systemRPH[0].split(":");
		 
			 if (flights != null && flights != "") {
				 OuterLoop:
				 for (var f = 0; f < flights.length; f++) {	
					 if (flights[f].availableFlightInfo != null && flights[f].availableFlightInfo != "") {						 
						 for (var fn = 0; fn < flights[f].availableFlightInfo.length; fn++) {
							 if (flights[f].availableFlightInfo[fn].segments != null && flights[f].availableFlightInfo[fn].segments != "") {	
								 var segAsem = "";								 
								 for (var s = 0; s < flights[f].availableFlightInfo[fn].segments.length; s++) {								
									 if (s == 0) {
										 segAsem = flights[f].availableFlightInfo[fn].segments[s].flightRefNumber;
									 } else {
										 segAsem = segAsem + ":" + flights[f].availableFlightInfo[fn].segments[s].flightRefNumber;
									 }
									 if (systemRPH[0] == segAsem) {
										 segments =  flights[f].availableFlightInfo[fn].segments;
										 break OuterLoop;
									 }									
								 }
							 }
						 }
					 }
				 }		 
		    }	 
	 }
	 
	 return segments;
	
}

UI_AvailabilitySearch.getSelectedSystem = function() {
	 var selectedSystem = null;
	
	 var flightRPHList = [];
	 var flights = [];
	 if (UI_AvailabilitySearch.outFlightRPHList != null && UI_AvailabilitySearch.outFlightRPHList != "") {
		 var systemRPH = UI_AvailabilitySearch.outFlightRPHList.split("!");
		 flightRPHList[flightRPHList.length] = systemRPH[0];
		 flights = UI_AvailabilitySearch.outboundFlights;
	 }
	 
	 if (UI_AvailabilitySearch.retFlightRPHList != null && UI_AvailabilitySearch.retFlightRPHList != "") {
		 var systemRPH = UI_AvailabilitySearch.retFlightRPHList.split("!");
		 flightRPHList[flightRPHList.length] = systemRPH[0];
		 flights = flights.concat(UI_AvailabilitySearch.inboundFlights);
	 }
	 
	 // Out bound Flights
	 for (var rphI = 0; rphI < flightRPHList.length; rphI++) {
		 var systemRPH = flightRPHList[rphI];
		 
			 if (flights != null && flights != "") {
				 OuterLoop:
				 for (var f = 0; f < flights.length; f++) {	
					 if (flights[f].availableFlightInfo != null && flights[f].availableFlightInfo != "") {						 
						 for (var fn = 0; fn < flights[f].availableFlightInfo.length; fn++) {
							 if (flights[f].availableFlightInfo[fn].segments != null && flights[f].availableFlightInfo[fn].segments != "") {	
								 var segAsem = "";	
								 var intSystem = null;
								 for (var s = 0; s < flights[f].availableFlightInfo[fn].segments.length; s++) {								
									 if (s == 0) {
										 segAsem = flights[f].availableFlightInfo[fn].segments[s].flightRefNumber;
									 } else {
										 segAsem = segAsem + ":" + flights[f].availableFlightInfo[fn].segments[s].flightRefNumber;
									 }
									 intSystem = flights[f].availableFlightInfo[fn].segments[s].system;
									 if (systemRPH == segAsem) {
										 if(selectedSystem == null){
											 selectedSystem = intSystem;
										 } else if(selectedSystem != intSystem){
											return "INT";
										 }
										 break OuterLoop;
									 }									
								 }
							 }
						 }
					 }
				 }		 
		    }	 
	 }
	 
	 return selectedSystem;
	 
	
}


/**
 * Get Calendar Selected RPH List
 */
// TODO:Set Selected RPHList From Calendar js
UI_AvailabilitySearch.setRPHList  = function() {
	if(UI_AvailabilitySearch.isCnxSegmentRequote){
		UI_AvailabilitySearch.setFlightRPHList({existingFltSegList:newONDSegmentList});		
	} else if(UI_AvailabilitySearch.isCalendarViewDisabled()){
		UI_AvailabilitySearch.outFlightRPHList = UI_AvailabilitySearch.getNewFlightSegRef();
	} else {
		// var value =
		// $("#fCalOut").find(".selected").next(".flightRefID").text();
		var value = $("#fCalOut_FL").find("input[type='radio']:checked").parent().find(".flightRefId").text();
		UI_AvailabilitySearch.outFlightRPHList = UI_commonSystem.isEmpty(value) ? "" : value;
	
		value = $("#fCalIn_FL").find("input[type='radio']:checked").parent().find(".flightRefId").text();
		UI_AvailabilitySearch.retFlightRPHList = UI_commonSystem.isEmpty(value) ? "" : value;
	
		var durat = $("#fCalIn_FL").find("input[type='radio']:checked").parent().find(".totalDuration").text();
		UI_AvailabilitySearch.outBoundDurations = UI_commonSystem.isEmpty(durat) ? "" : durat;
		$("#outBoundDurations").val(UI_AvailabilitySearch.outBoundDurations);
	
		durat = $("#fCalIn_FL").find("input[type='radio']:checked").parent().find(".totalDuration").text();
		UI_AvailabilitySearch.inBoundDurations = UI_commonSystem.isEmpty(durat) ? "" : durat;
		$("#inBoundDurations").val(UI_AvailabilitySearch.inBoundDurations);
		$("#resFareQuoteLogicalCCSelection").val($.toJSON(UI_AvailabilitySearch.getSelectedFlightLogicalCCMap()));
	}	
}

UI_AvailabilitySearch.getSelectedFlightFares = function(){
	var outFare = $("#fCalOut_FL").find("input[type='radio']:checked").parent().find(".totalValueSelected").find('.currValueTotal').text();
	outFare = UI_commonSystem.isEmpty(outFare) ? null : outFare;
	var inFare = $("#fCalIn_FL").find("input[type='radio']:checked").parent().find(".totalValueSelected").find('.currValueTotal').text();
	inFare = UI_commonSystem.isEmpty(inFare) ? null : inFare;
	return {outFare:outFare,inFare: inFare};
}

// Took this function in to Calendar JS
/*
 * UI_AvailabilitySearch.changeSelectedFares = function(values){ }
 */
/**
 * Client Validate Availability Search
 */
// TODO: One way flight select
UI_AvailabilitySearch.clientValidateAS = function(e, method) {
	UI_AvailabilitySearch.setRPHList();	
	UI_AvailabilitySearch.returnFlag = $("#resReturnFlag").val();
	if(UI_AvailabilitySearch.returnFlag != "true") {
		if (UI_commonSystem.isEmpty(UI_AvailabilitySearch.outFlightRPHList)) {			
			jAlert(UI_AvailabilitySearch.errorInfo["ERR015"],'Alert');
			return false;
		}
	} else {		
		if (UI_commonSystem.isEmpty(UI_AvailabilitySearch.outFlightRPHList) && UI_commonSystem.isEmpty(UI_AvailabilitySearch.retFlightRPHList) && UI_AvailabilitySearch.blnShowOneWay == false) {			
			jAlert(UI_AvailabilitySearch.errorInfo["ERR015"] + "\n" + UI_AvailabilitySearch.errorInfo["ERR016"],'Alert');
			return false;
		} 
		
		if(UI_AvailabilitySearch.blnShowOneWay == false) {			
			if (UI_commonSystem.isEmpty(UI_AvailabilitySearch.outFlightRPHList)) {				
				if (method != "fareCal") {
					jAlert(UI_AvailabilitySearch.errorInfo["ERR015"],'Alert');	
				} else {
					UI_AvailabilitySearch.displaySelectedFlightDetails();
				}
				return false;
			} 
			if(UI_commonSystem.isEmpty(UI_AvailabilitySearch.retFlightRPHList)) {				
				if (method != "fareCal") {
					jAlert(UI_AvailabilitySearch.errorInfo["ERR016"],'Alert');
				} else {
					UI_AvailabilitySearch.displaySelectedFlightDetails();
				}
				return false;
			}	
			// Date Check
			var depSegArrival = UI_AvailabilitySearch.getSelectedFlight(UI_AvailabilitySearch.outFlightRPHList, UI_AvailabilitySearch.outboundFlights);			
			var depMs = Number(depSegArrival[0].departureTimeLong);
			var outbLastArrivalZulu = Number(depSegArrival[depSegArrival.length -1].arrivalTimeZuluLong);
			var arrSegArrival = UI_AvailabilitySearch.getSelectedFlight(UI_AvailabilitySearch.retFlightRPHList, UI_AvailabilitySearch.inboundFlights);			
			var arMs = Number(arrSegArrival[0].departureTimeLong);	
			var inFirstDepatureZulu = Number(arrSegArrival[0].departureTimeZuluLong);	
			if (!CheckDates(DateToString(new Date(depMs)), (DateToString(new Date(arMs))))){
				jAlert(UI_AvailabilitySearch.errorInfo["ERR004"],'Alert');
				$(e.currentTarget).removeClass("selected");
				$(e.currentTarget).find("input[type='radio']").attr("checked","");
				UI_AvailabilitySearch.lastSelected.addClass("selected");
				UI_AvailabilitySearch.lastSelected.find("input[type='radio']").attr("checked","checked");
				return false;
			}
			// Transfer Time
			if ((outbLastArrivalZulu + Number(UI_AvailabilitySearch.minReturnTransitionTime))  >= inFirstDepatureZulu) {
				jAlert(UI_AvailabilitySearch.errorInfo["ERR040"],'Alert');
				$(e.currentTarget).removeClass("selected");
				$(e.currentTarget).find("input[type='radio']").attr("checked","");
				UI_AvailabilitySearch.lastSelected.addClass("selected");
				UI_AvailabilitySearch.lastSelected.find("input[type='radio']").attr("checked","checked");
				return false;
			}
		
		} else {
			if (UI_commonSystem.isEmpty(UI_AvailabilitySearch.outFlightRPHList) && UI_commonSystem.isEmpty(UI_AvailabilitySearch.retFlightRPHList)) {				
				jAlert("Please select out bound or in bound flight",'Alert');
			}
			
			if (UI_commonSystem.isEmpty(UI_AvailabilitySearch.retFlightRPHList)) {
				if (!confirm(UI_AvailabilitySearch.msgWithoutInbound)){
					return  false;					
				} else {					
					UI_AvailabilitySearch.retFlightRPHList = "";
					$("#resReturnDate").val("");
					$("#resReturnFlag").val(false);
				}
			}
			
			if (UI_commonSystem.isEmpty(UI_AvailabilitySearch.outFlightRPHList)) {
				if (!confirm(UI_AvailabilitySearch.msgWithoutOutbound)){
					return  false;					
				} else {
					UI_AvailabilitySearch.outFlightRPHList = "";
				}
			}
			
	
		}
	}	
	UI_AvailabilitySearch.displaySelectedFlightDetails();
	return true;
}


// To Do: Move to common
function raiseError(strErrNo){
	var strMsg = UI_AvailabilitySearch.errorInfo[strErrNo];
	if (arguments.length >1) {
		for (var i = 0 ; i < arguments.length - 1 ; i++){
			strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
		}
	}
	return strMsg;
}

/**
 * to create tabs html when selecting the tabs has moved to
 * jquery.calendarview.js
 */



/**
 * to Build the 3 days view
 * 
 * @return
 */
UI_AvailabilitySearch.buildDays = function(dataSetDepature, dataSetArrival, calDisplayCurrency, outLCCs, inLCCs){
	// UI_AvailabilitySearch.cleanAndBuildCalHolder();
	var nod = (Number($("#resFlexibleDates").val()) == 0 ) ? UI_AvailabilitySearch.calendarViews : 3;// changes
																										// 3 -
																										// 7
	var flightSegLabel = '';
	
	if(UI_AvailabilitySearch.isRequoteFlightSearch) {
		flightSegLabel = "<span class='imgCode'>" + UI_AvailabilitySearch.labels.lblFlightDetail +":"+"</span> <span class='correction' id='lblOutbound'>"+UI_AvailabilitySearch.getSegname(0)+"</span>";
	} else {
		flightSegLabel = "<span class='imgCode'>" + UI_AvailabilitySearch.labels.lblOutbound +":"+"</span> <span class='correction' id='lblOutbound'>"+UI_AvailabilitySearch.getSegname(0)+"</span>";
	}
	
	$("#fCalOut").calendarview({
		noOfDays : nod,
		gapOfDays : calDisplayConfig.design.gapInDaysPx,
		dispalyItems : calDisplayConfig.design.ItemsToDispalyInCal,
		travelType : "dep",
		data : dataSetDepature,
		topLabel : flightSegLabel,
		loadingImage : calDisplayConfig.design.loadingImage,
		currencyCode:calDisplayCurrency,
		flightLabels:outLCCs,
		selectFlight:function(event, val,typ,tDuration){
			if (typ == "departure"){
				UI_AvailabilitySearch.outFlightRPHList = val;
				UI_AvailabilitySearch.outBoundDurations = tDuration;
			}else{
				UI_AvailabilitySearch.retFlightRPHList = val;
				UI_AvailabilitySearch.inBoundDurations = tDuration;
			}
		
			UI_AvailabilitySearch.reCalculateFare(event, typ);

	 	},
		getAvailabilityForSelectedDate: function(selectedHtmlToAdd, oldSeleted ,calId){
		    UI_commonSystem.loadingProgress();
			return UI_AvailabilitySearch.searchAvailabilityForSelectedDate(selectedHtmlToAdd, oldSeleted, calId);
		},
		getDataForAllDays:function(nextPrevCalSearchParams){
		    UI_commonSystem.loadingProgress();
			UI_AvailabilitySearch.setNextPreviousDataForAllDays(nextPrevCalSearchParams);
		}
	});
	if ($("#resReturnFlag").val() == "true"){
		$("#fCalIn").calendarview({
			noOfDays : nod,
			gapOfDays : calDisplayConfig.design.gapInDaysPx,
			dispalyItems : calDisplayConfig.design.ItemsToDispalyInCal,
			travelType : "arr",
			data:dataSetArrival,
			topLabel :"<span class='imgCode returnOND'>" + UI_AvailabilitySearch.labels.lblInbound +" </span>: <span class='correction' id='lblInBound'>"+
			UI_AvailabilitySearch.getSegname(1)+"</span>",
			loadingImage : calDisplayConfig.design.loadingImage,
			currencyCode:calDisplayCurrency,
			flightLabels:inLCCs,
			selectFlight:function(event, val ,typ,tDuration){
				if (typ == "departure"){
					UI_AvailabilitySearch.outFlightRPHList = val;
					UI_AvailabilitySearch.outBoundDurations = tDuration;
				}else{
					UI_AvailabilitySearch.retFlightRPHList = val;
					UI_AvailabilitySearch.inBoundDurations = tDuration;
				}
				
				UI_AvailabilitySearch.reCalculateFare(event, typ);
		 	},
			getAvailabilityForSelectedDate: function(selectedHtmlToAdd, oldSeleted, calId){
			    UI_commonSystem.loadingProgress();
				return UI_AvailabilitySearch.searchAvailabilityForSelectedDate(selectedHtmlToAdd, oldSeleted, calId);
			},
			getDataForAllDays:function(nextPrevCalSearchParams){
			    UI_commonSystem.loadingProgress();
				UI_AvailabilitySearch.setNextPreviousDataForAllDays(nextPrevCalSearchParams);
			}
		});

	}
	var hideObj = ($("#resFlexibleDates").val() == 0 ) ? $("#tabs1-2") : $("#tabs1-2"); // change
	$.fn.calendarview.callTooltip();	
};

UI_AvailabilitySearch.searchAvailabilityForSelectedDate = function(selectedHtmlToAdd, oldSeleted, calId){
	var currDate = $(selectedHtmlToAdd).find('.calCurrdate').text();
	if (calId == 'fCalOut') {
		var selectedDepartureDate = oldSeleted.find(".selected").find(".calCurrdate").text();
		var selectedReturnDate = $("#fCalIn").find(".selected").find(".calCurrdate").text();
	}else{
		var selectedDepartureDate = $("#fCalOut").find(".selected").find(".calCurrdate").text();
		var selectedReturnDate = oldSeleted.find(".selected").find(".calCurrdate").text();
	}
	selectedDate = selectedReturnDate;
	var isReturn = true;
	var selectedOndSequence = UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND;
	
	if (calId == 'fCalOut') {
		selectedOndSequence = UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND;
		selectedDate = selectedDepartureDate;
		isReturn = false;
	}
	
	if ((!UI_AvailabilitySearch.isModifySegment && !UI_AvailabilitySearch.addGroundSegment)
			 || (UI_AvailabilitySearch.isModifySegment && $('#resOndSelectedFlexiStr').val() != "true")) {
		UI_AvailabilitySearch.ondWiseFlexiSelected[selectedOndSequence] = true;
	} else if(UI_AvailabilitySearch.isModifySegment && $('#resOndSelectedFlexiStr').val() == "true"){
		UI_AvailabilitySearch.ondWiseFlexiSelected[selectedOndSequence] = false;
	}
	
	if(!UI_AvailabilitySearch.isModifySegment){
		 UI_AvailabilitySearch.ondWiseBundleFareSelected = {};
		 UI_AvailabilitySearch.ondWiseBookingClassSelected = {};
		 UI_AvailabilitySearch.segWiseBookingClassOverride = {};
	}
	
	$("#resFareQuoteLogicalCCSelection").val("");
	$("#resOndQuoteFlexi").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiQuote));	
	$("#resOndSelectedFlexiStr").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected));	
	//Keep track of flexi selection done in flight search page
	$("#resflightSearchOndFlexiSelection").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected));
	$("#resOndAvailbleFlexiStr").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiAvailability));
	$("#resOndwiseFlexiChargesForAnci").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr));
	
	$("#resOndBundleFareStr").val($.toJSON(UI_AvailabilitySearch.ondWiseBundleFareSelected));
	$("#resOndBookingClassStr").val($.toJSON(UI_AvailabilitySearch.ondWiseBookingClassSelected));
	$("#resOndSegBookingClassStr").val($.toJSON(UI_AvailabilitySearch.segWiseBookingClassOverride));
	
								
	if(selectedDate != currDate){
		var doSearch = true;								
		if(isReturn &&
				!CheckDates(selectedDepartureDate, currDate)){
			jAlert(UI_AvailabilitySearch.errorInfo["ERR004"],'Alert');
			UI_commonSystem.loadingCompleted();
			return false;
		} else if(!isReturn && selectedReturnDate != undefined && selectedReturnDate != '' &&
				!CheckDates(currDate, selectedReturnDate)) {
			jAlert(UI_AvailabilitySearch.errorInfo["ERR004"],'Alert');
			UI_commonSystem.loadingCompleted();
			return false;
		}
		

		if(!isReturn){									
			selectedDepartureDate = currDate;
			$("#resDepartureDate").val(currDate);
		} else {
			selectedReturnDate = currDate;
			$("#resReturnDate").val(currDate);
		}	
		
		$("#resDepartureVariance").val(0);
		$("#resReturnVariance").val(0);
		
		$("#FlightDetailsPanel").hide();
		$("#trPriceBDPannel").hide();
		$("#balanceSummary").hide();
		 
		 								 
		 var url = 'availabilitySearchCalendar.action';
		 var data = createSearchSubmitData();
		 if(UI_CaptchaValidation.isHumanVerificationEnabled) { 
		 	var additionalData = []
		 	additionalData['selectedHtmlToAdd'] = selectedHtmlToAdd;	
		 	UI_CaptchaValidation.setDataToProcessAfterCaptchaValidation(null,null,'SELDATE', additionalData);	
		 }
		 
		 $.ajax(
		 	{
				type: "POST",
				dataType: 'json',
				data: data,
				url: url,
				async: false,
				success: function(response){
					UI_AvailabilitySearch.searchAvailabilityForSelectedDateSuccess(response, calId, selectedHtmlToAdd); 
				},
				error: UI_commonSystem.setErrorStatus,
				cache: false
			});		
		return true;					
	}
}


UI_AvailabilitySearch.searchAvailabilityForSelectedDateSuccess = function(response, calId, selectedHtmlToAdd) {
	
	if(UI_CaptchaValidation.isHumanVerificationEnabled) {
		 	if(response.captchaValidationRequired) {
					UI_commonSystem.loadingCompleted();
					UI_CaptchaValidation.openCaptchaPopup();
					return false;
		 	}	 
		 	UI_CaptchaValidation.resetDataToProcessAfterCaptchaValidation(); 
	}
	
	var outBoundAvailbleBundleFareArray = new Array();
	var inBoundAvailbleBundleFareArray = new Array();
	if(calId == 'fCalOut'){
		if(response.departueFlights != null){
			$.data(selectedHtmlToAdd[0], "availFlights", response.departueFlights[0].availableFlightInfo);
			$.data(selectedHtmlToAdd[0], "availLogicalCC", response.outboundAvailableLogicalCC);
			UI_AvailabilitySearch.updateOBIBFlights(response.departueFlights[0].departureDate, 
					response.departueFlights[0].availableFlightInfo, 'out');
		}
	} else {
		if(response.arrivalFlights != null){
			$.data(selectedHtmlToAdd[0], "availFlights", response.arrivalFlights[0].availableFlightInfo);
			$.data(selectedHtmlToAdd[0], "availLogicalCC", response.inboundAvailableLogicalCC);
			UI_AvailabilitySearch.updateOBIBFlights(response.arrivalFlights[0].departureDate, 
					response.arrivalFlights[0].availableFlightInfo, 'in');				
		}
	}

	if(response.outboundAvailableLogicalCC != undefined && response.outboundAvailableLogicalCC != null){						
		$.each(response.outboundAvailableLogicalCC,function(index,logicalCC){
			if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
				outBoundAvailbleBundleFareArray[index] = new Array();
				outBoundAvailbleBundleFareArray[index].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, "bundleFareName":logicalCC.logicalCCDesc, "imageUrl":setBundleFareImageURL(logicalCC)});
				
			}
		});
	}
	
	if(response.inboundAvailableLogicalCC != undefined && response.inboundAvailableLogicalCC != null){						
		$.each(response.inboundAvailableLogicalCC,function(index,logicalCC){
			if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
				inBoundAvailbleBundleFareArray[index] = new Array();
				inBoundAvailbleBundleFareArray[index].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, "bundleFareName":logicalCC.logicalCCDesc, "imageUrl":setBundleFareImageURL(logicalCC)});
				
			}
		});
	}

	UI_commonSystem.loadingCompleted();
	UI_AvailabilitySearch.availableBundleFareLCClass[UI_AvailabilitySearch.OND_SEQUENCE.OUT_BOUND] = outBoundAvailbleBundleFareArray;
	UI_AvailabilitySearch.availableBundleFareLCClass[UI_AvailabilitySearch.OND_SEQUENCE.IN_BOUND] = inBoundAvailbleBundleFareArray;
	$("#resAvailableBundleFareLCClassStr").val($.toJSON(UI_AvailabilitySearch.availableBundleFareLCClass));
	
	//TODO:RW remove this
	//UI_AvailabilitySearch.ONDWiseFlexiSelectionInAnci(response.departueFlights,response.arrivalFlights);
	//UI_AvailabilitySearch.ONDWiseTotalFlexiChargeInAnci(response.fareQuote.strOndFlexiTotalChargeInbound,response.fareQuote.strOndFlexiTotalChargeOutbound);
	
	UI_AvailabilitySearch.ondWiseFlexiAvailability = response.ondWiseTotalFlexiAvailable;
	UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr = response.ondWiseTotalFlexiCharge;
}

UI_AvailabilitySearch.updateOBIBFlights = function(selectedDate, availFlight, way){
	if(way == 'out'){
		$.each(UI_AvailabilitySearch.outboundFlights, function(i, obj){
			if(selectedDate == obj.departureDate){
				obj.availableFlightInfo = availFlight;
			}
		});
	} else {
		$.each(UI_AvailabilitySearch.inboundFlights, function(i, obj){
			if(selectedDate == obj.departureDate){
				obj.availableFlightInfo = availFlight;
			}
		});
	}
}

/**
 * Set the next previous data for all days.
 * 
 */
UI_AvailabilitySearch.setNextPreviousDataForAllDays = function (nextPrevActionParams){	
	// copy the original form data to the temp object.
	tempFormDataStore = {	depDate:$("#resDepartureDate").val(), arrDate: $("#resReturnDate").val(),
			depVariance:$("#resDepartureVariance").val(), arrVariance: $("#resReturnVariance").val(),
			retFlag:$("#resReturnFlag").val(), flexibleDates: $("#resFlexibleDates").val()};
	if (nextPrevActionParams.depOrReturn == "dep"){
		$("#resDepartureDate").val(nextPrevActionParams.pivotalDate);
		$("#resDepartureVariance").val(nextPrevActionParams.depVar);
		$("#resReturnVariance").val(nextPrevActionParams.arrVar);
		$("#resReturnFlag").val(false);
	}else{
		$("#resReturnDate").val(nextPrevActionParams.pivotalDate);
		$("#resReturnVariance").val(nextPrevActionParams.arrVar);
		$("#resDepartureVariance").val(nextPrevActionParams.depVar);
		$("#resReturnFlag").val(true);
	}
	$("#resFlexibleDates").val(0);
	$("#blnNextPrevious").val(true);
	
	newCalObjs = nextPrevActionParams.calDataArray;
	createNewCalContentCB = nextPrevActionParams.calCreateContentCB;
	departureOrReturnFlag = nextPrevActionParams.depOrReturn;
	numberOfDays = nextPrevActionParams.noOfDays;
	newCalNextDatesArr = nextPrevActionParams.calNextDatesArray;	
	moveDirection = nextPrevActionParams.moveDirection;
	calendarLength = nextPrevActionParams.calendarLength;

	UI_AvailabilitySearch.loadNextPreviousFlightData();	
	return false;
};

/**
 * Load Next/Previous Flight Data
 */
UI_AvailabilitySearch.loadNextPreviousFlightData = function() {	 
	 UI_commonSystem.loadingProgress();
	 var data = {};
	 var FareCollection =[];
	 if( departureOrReturnFlag == "dep"){		
		 $.each($("#fCalOut_FL").find(".currValueTotal"),function(i,j){
			 FareCollection[i] = parseInt(j.innerHTML,10);
		 });
	 }else{		
		 $.each($("#fCalIn_FL").find(".currValueTotal"),function(i,j){
			 FareCollection[i] = parseInt(j.innerHTML,10);
		 });
	 }
	 data["maxAmount"] = (FareCollection == "" || FareCollection == undefined)? 0 : Math.max.apply( Math, FareCollection );
	 data["minAmount"] = (FareCollection == "" || FareCollection == undefined)? 0 : Math.min.apply( Math, FareCollection );
	/*
	 * $("#frmFare").attr('action', 'nextPreviousCalendar.action');
	 * $("#frmFare").ajaxSubmit({data:data, dataType: 'json',success:
	 * UI_AvailabilitySearch.loadNextPreviousFlightDataProcess,
	 * error:UI_commonSystem.setErrorStatus});
	 */
	 
	 var url = 'nextPreviousCalendar.action';
	 var data = createSearchSubmitData();
	 
	 $.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,
	 success: UI_AvailabilitySearch.loadNextPreviousFlightDataProcess, error:UI_commonSystem.setErrorStatus, cache : false, beforeSubmit:UI_commonSystem.loadingProgress});
		 	 	 
	 return false;
}

/**
 * Process Next/Previous data
 */ 
UI_AvailabilitySearch.loadNextPreviousFlightDataProcess = function(response){
	
	if(response != null && response.success == true) {
		 		 
		if ( departureOrReturnFlag == "dep") {
			
			createNewCalContentCB(response.departueFlights, departureOrReturnFlag, newCalObjs, numberOfDays, newCalNextDatesArr, moveDirection);
			
			if (moveDirection == "next"){			
				(UI_AvailabilitySearch.outboundFlights).splice(0,response.departueFlights.length);
				UI_AvailabilitySearch.outboundFlights = (UI_AvailabilitySearch.outboundFlights).concat(response.departueFlights);
			} else {
				var startIndex = calendarLength - response.departueFlights.length;
				(UI_AvailabilitySearch.outboundFlights).splice(startIndex,response.departueFlights.length);				
				UI_AvailabilitySearch.outboundFlights = (response.departueFlights).concat(UI_AvailabilitySearch.outboundFlights);
			}			
			if (UI_commonSystem.isEmpty($("#fCalOut").find(".selected").find(".sData").text())) {
				$("#trPriceBDPannel").slideUp("slow");
				$("#balanceSummary").slideUp("slow");
				// $("#fCalOut_FL").parent().parent().hide();
				showSelectedFlightsSummary();				
			}
		} else if ( departureOrReturnFlag == "arr") {
			
			createNewCalContentCB(response.arrivalFlights, departureOrReturnFlag, newCalObjs, numberOfDays, newCalNextDatesArr, moveDirection);
			
			if (moveDirection == "next"){				
				(UI_AvailabilitySearch.inboundFlights).splice(0,response.arrivalFlights.length);
				UI_AvailabilitySearch.inboundFlights = (UI_AvailabilitySearch.inboundFlights).concat(response.arrivalFlights);
			} else {
				// take off the last part of the flights to append the new
				// flight set.
				var startIndex = calendarLength - response.arrivalFlights;				
				(UI_AvailabilitySearch.inboundFlights).splice(startIndex,response.arrivalFlights.length);				
				UI_AvailabilitySearch.inboundFlights = (response.arrivalFlights).concat(UI_AvailabilitySearch.inboundFlights);
			}
			
			if (UI_commonSystem.isEmpty($("#fCalIn").find(".selected").find(".sData").text())) {
				$("#trPriceBDPannel").slideUp("slow");
				$("#balanceSummary").slideUp("slow");
				// $("#fCalIn_FL").parent().parent().parent().parent().hide();
				showSelectedFlightsSummary();
			}
		}
		UI_AvailabilitySearch.setAfterNextPreviousData();			
	} else {
		// UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		wrapAndDebugError('004',response);
	}
	UI_commonSystem.loadingCompleted();
	// $.fn.calendarview.callTooltip();
}

// reset the original form data after the data is filled.
UI_AvailabilitySearch.setAfterNextPreviousData = function(){
	$("#resDepartureDate").val(tempFormDataStore.depDate);
	$("#resDepartureVariance").val(tempFormDataStore.depVariance);
	$("#resFlexibleDates").val(tempFormDataStore.flexibleDates);
	if (tempFormDataStore.retFlag == "true"){
		$("#resReturnVariance").val(tempFormDataStore.arrVariance);
		$("#resReturnDate").val(tempFormDataStore.arrDate)
		$("#resReturnFlag").val(tempFormDataStore.retFlag);
	}
}

UI_AvailabilitySearch.newPreloaderShow = function (obj){
	var setW = parseInt(obj.parent().css("width")) -
	(parseInt(obj.parent().css("padding-left")) + parseInt(obj.parent().css("padding-right")) + 10);
	obj.html($(".loadingContainer").html());
	$(".newPogressLoading").css("position","absolute");
	$(".newPogressLoading").css("z-index",100);
	$(".newPogressLoading").css("width", setW);
		obj.animate({height:80},
				{duration:500,
				complete:function(){
					$(".newPogressLoading").show();
				}
			});
}

UI_AvailabilitySearch.newPreloaderHide = function(obj){
	var headerHeight = 40;
	var objheight = null;
	if (obj.attr("id") == "tabs1-1"){
		obj.css("height",80);
		var newheight = parseInt($("#" +obj.attr('id')+" .c-scroller").css("height")) + headerHeight;
		obj.animate({height:newheight},
				{duration:500,
				complete:function(){
					$("#" +obj.attr('id')+" .flight-oriented").show();
					obj.find("div.newPogressLoading").remove();
				}
			});
	}else{
		var newheight = parseInt($("#" +obj.attr('id')+" .c-scroller").css("height")) + Number(10);
		if (obj.find(".no-return").hasClass("the-one-cal")){
			newheight = newheight + headerHeight + 50;
		}else{
			// $(".the-one-cal").css("height", newheight);
			newheight = (newheight* 2) + (headerHeight* 2) + 10;
		}
		obj.css("height",80);
		obj.animate({height:newheight},
				{duration:500,
				complete:function(){
					$("#"+obj.attr('id')+" .flight-oriented").show();
					obj.find("div.newPogressLoading").remove();
				}
			});
	}
	
	UI_commonSystem.readOnly(false);

}

/**
 * Link terms Click
 */
UI_AvailabilitySearch.linkTermsClick = function(extUrl) {
	var url = "";
	if (extUrl != null){
		// set to external link
		url = extUrl;
	}else{
		url = SYS_IBECommonParam.nonSecurePath + "showTermsNCond.action";
	}
	var intHeight = (window.screen.height - 200);
	var intWidth = 795;
	var intX = ((window.screen.height - intHeight) / 2);
	var intY = ((window.screen.width - intWidth) / 2);
	var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
	if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)){UI_Top.holder().objCWindow.close();}
	UI_Top.holder().objCWindow = window.open(url,'ToolChild',strProp)
};

/**
 * Link terms Click
 */
UI_AvailabilitySearch.compareFareClick = function(extUrl) {
	var url = "";
	if (extUrl != null){
		// set to external link
		url = extUrl;
	}else{
		url = SYS_IBECommonParam.nonSecurePath + "showLoadPage!showCompareFare.action";
	}
	var intHeight = (window.screen.height - 400);
	var intWidth = 500;
	var intX = ((window.screen.height - intHeight) / 2);
	var intY = ((window.screen.width - intWidth) / 2);
	var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
	if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)){UI_Top.holder().objCWindow.close();}
	UI_Top.holder().objCWindow = window.open(url,'ToolChild',strProp)
};


/*
 * MISC functions
 */
function dateVal( str1 ) {
    var diff = Date.parse( str1 );
    return isNaN( diff ) ? NaN : {
    	diff : diff,
    	ms : Math.floor( diff            % 1000 ),
    	s  : Math.floor( diff /     1000 %   60 ),
    	m  : Math.floor( diff /    60000 %   60 ),
    	h  : Math.floor( diff /  3600000 %   24 ),
    	d  : Math.floor( diff / 86400000        )
    };
};
/**
 * format the date
 */
function dataformater(txt){
	 if (txt != null || txt != undefined) 
		 text = txt.split("/");
	 var dat = null;
	 if ($.browser.msie)
		 dat = Number(text[2])+"/"+Number(text[1])+"/"+Number(text[0]);
	 else
		 dat = Number(text[1])+"/"+Number(text[0])+"/"+Number(text[2]);
	 
	 return dat;
};
/**
 * get selected flights availability
 */
UI_AvailabilitySearch.isSelectedFlight = function (){
	
}


/**
 * Show Balance Summary
 */
UI_AvailabilitySearch.showBalcenceSummary = function() {	
	if (UI_AvailabilitySearch.getSelectedFlightSegments("both").length > 0) {
		var rphData = UI_AvailabilitySearch.getRPHData();	
		if(rphData){
			UI_commonSystem.loadingProgress();
			var url = '';
			
			var data = createSearchSubmitData();	
			
			if(UI_AvailabilitySearch.isRequoteFlightSearch){
				
				$('#resSelectedFlights').val($.toJSON({flightSegments:UI_AvailabilitySearch.setSelectedFlightInfo(newONDSegmentList),isReturnFlight:false}));
				
				 data['searchParams.ondListString'] = $.toJSON(UI_AvailabilitySearch.getONDSearchDTOList());
				 data['searchParams.ticketValidTillStr'] = UI_AvailabilitySearch.getTicketValidTill();
				 data['searchParams.lastFareQuoteDateStr'] = UI_AvailabilitySearch.getLastFareQuoteDate();
				 if(UI_AvailabilitySearch.getSelectedSystem() == "INT"){
					 data['groupPNR'] = true;
					 data['balanceQueryData'] = $.toJSON(getBalanceQueryData($('#resPNR').val(), true,$("#resVersion").val()));
					 $('#balanceQueryData').val($.toJSON(getBalanceQueryData($('#resPNR').val(), true,$("#resVersion").val()))); 
				 }else{
					 data['balanceQueryData'] = $.toJSON(getBalanceQueryData($('#resPNR').val(), false,$("#resVersion").val()));
					 $('#balanceQueryData').val($.toJSON(getBalanceQueryData($('#resPNR').val(), false,$("#resVersion").val()))); 
				 }
				
				if(UI_AvailabilitySearch.isCnxSegmentRequote) {
					data['cancelSegment'] = true;
					data['modifySegment'] = false;
				} else {
					data['cancelSegment'] = false;
					data['modifySegment'] = true;
				}
				
				url = 'requoteBalanceSummary.action';
			} else {
					$('#resSelectedFlights').val($.toJSON({flightSegments:UI_AvailabilitySearch.getSelectedFlightSegments("both"), 
				isReturnFlight:UI_AvailabilitySearch.returnFlag}));			
				url = 'balanceSummary.action';
			}


			 
			 data  =  $.airutil.dom.concatObjects(data, rphData.data);
			
			 data['selectedFlightJson'] = $('#resSelectedFlights').val();
			 if(UI_AvailabilitySearch.getSelectedSystem() == "INT"){
				 data['groupPNR'] = true;
			 }
			 $.ajax({type: "POST", dataType: 'json',data: data , url:url,		
			 success: UI_AvailabilitySearch.showBalcenceSummaryProcess, error:UI_commonSystem.setErrorStatus, cache : false});
			
		}
		return false;
	}
}

function getBalanceQueryData(pnr, isGroupPnr, version) {
	var balanceData = {
	    removedSegmentIds : getRemovedResSegmentRPHList(),
	    pnr : pnr,
	    groupPnr : isGroupPnr ? pnr:null ,
	    version : version,
	    groundFltSegByFltSegId : updateGroundFltSegByFltSegMap()
	};
	

	return balanceData;

}

function updateGroundFltSegByFltSegMap(){
	var groundFltSegByFltSegId = {};
	var segments = UI_AvailabilitySearch.getONDSearchDTOList();
	var existBusSegByPnrSegId ={};
	$.each(segments, function(key, flt) {
		if(flt.busSegment && flt.status == "CNF" ){
			var flightSegId = UI_AvailabilitySearch.getFlightSegIdFromRPH(flt.flightRPHList[0]);
			var pnrSegId = UI_AvailabilitySearch.getFlightSegIdFromRPH(flt.existingResSegRPHList[0]);
			existBusSegByPnrSegId[pnrSegId]=flightSegId;
		}			
	});
	
	
	$.each(segments, function(key, flt) {
		if(!flt.busSegment && flt.status == "CNF"){				
			var busPnrSegId =  flt.groundStationPnrSegmentID;				
			var flightSegId = UI_AvailabilitySearch.getFlightSegIdFromRPH(flt.flightRPHList[0]);
			
			if(existBusSegByPnrSegId!=null && existBusSegByPnrSegId[busPnrSegId]!=null){					
				if(groundFltSegByFltSegId[flightSegId]==undefined || groundFltSegByFltSegId[flightSegId]==null){
					groundFltSegByFltSegId[flightSegId] = existBusSegByPnrSegId[busPnrSegId];	
				}
			}			

		}			
	});
	return groundFltSegByFltSegId;
}

function getRemovedResSegmentRPHList() {
	var resSegmentRPHs = [];

	for ( var i = 0; i < removedOndList.length; i++) {
		resSegmentRPHs = resSegmentRPHs.concat(removedOndList[i]
		        .getResSegRPHList());
	}

	return resSegmentRPHs;
}

function decodeJsonDate(dstr) {
	var a = dstr.split('T');
	var d = a[0].split('-');
	var t = a[1].split(':');
	// new Date(year, month, day, hours, minutes, seconds,
	// milliseconds);
	var dtDate = new Date(d[0], parseInt(d[1], 10) - 1, d[2], t[0], t[1],
	        t[2]);
	
	return dtDate;
}

/**
 * Balance Summary Process
 */
UI_AvailabilitySearch.showBalcenceSummaryProcess = function(response) {		
	    if(response != null && response.success == true) {	    	
	    	if (response.modifyBalanceDTO != null) {
	    		if(response.groupPNR == true){
	    			// if default selected flight is interline a call is made to
					// lcc
		    		// to do the balance calculation making the booking an
					// interline one
	    			// after that the booking should be considered interline
	    			$("#resGroupPNR").val("true");
	    			$("#resVersion").val(response.version);
	    			
	    		}
	    		var balance = response.modifyBalanceDTO;
	    		var fareQuote = UI_AvailabilitySearch.fareQuote;
	    		var getValue = function(value){ if (value == null) return 0;  else return parseFloat(value)};
	    		
	    		fareQuote.hasModifySegment = true;   		
	    		fareQuote.inBaseCurr.modifyCharge = balance.modificationCharge;
	    		fareQuote.inBaseCurr.totalPrice = getValue(fareQuote.inBaseCurr.totalPrice) + getValue(balance.modificationCharge);
	    		if (balance.totalReservationCredit != null) {
	    			fareQuote.hasTotalReservationCredit = true;
	    			fareQuote.inBaseCurr.totalReservationCredit = balance.totalReservationCredit;
	    		}
	    		
	    		if (balance.balanceToPay != null && Number(balance.balanceToPay > 0)) {
	    			fareQuote.hasBalanceToPay = true;
	    			fareQuote.inBaseCurr.balaceToPay = balance.balanceToPay;
	    		} 
	    		if (balance.totalCreditBalace != null && Number(balance.totalCreditBalace) > 0){
	    			fareQuote.hasCreditBalance = true;
	    			fareQuote.inBaseCurr.creditBalnce = balance.totalCreditBalace;
	    		}
	    		
	    		if (fareQuote.hasBalanceToPay && fareQuote.hasCreditBalance) {
	    			fareQuote.hasBalanceToPay = false;
	    			fareQuote.hasCreditBalance = false;
	    		}
	    		
	    		
	    	}
	    	if (UI_AvailabilitySearch.modSegBalSummaryDisplay) {
	    		UI_AvailabilitySearch.updateChargeList = response.updateCharge.chargesList;
	    		$("#balanceSummary").populateLanguage({messageList:response.jsonLabel});
		    	UI_AvailabilitySearch.displaySummary();
		    	if(response.flexiReservation){
		    		$('#divFlexiMessage').show();
		    		if(response.flexiInfo != null && response.flexiInfo != ""){
		    			$('#spnFlexibilities').text(response.flexiInfo);
		    		}
		    	} else {
		    		$('#divFlexiMessage').hide();
		    	}
	    	} else {
	    		$("#balanceSummary").hide();
	    		//for Airarabia no need to display flight details directed to balance summarry
	    		if(UI_AvailabilitySearch.isCnxSegmentRequote || UI_AvailabilitySearch.isNameChangeRequote){
	    			UI_AvailabilitySearch.loadPaxPage();
	    		}
	    	}
	    	UI_AvailabilitySearch.isModifyBalanceSuccess = true;
            dateChangerjobforLanguage(SYS_IBECommonParam.locale,'yy/m/d h:mm');
		} else {
			UI_AvailabilitySearch.isModifyBalanceSuccess = false;
			$("#balanceSummary").hide();
			jAlert(response.messageTxt,'Alert');
			// UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}
		UI_commonSystem.loadingCompleted();	
}


UI_AvailabilitySearch.displayPaymentMethods = function(allowOnhold) {
	if (UI_AvailabilitySearch.showPaymentIcons){
		if (allowOnhold && !UI_AvailabilitySearch.isBINPromotion){
			if ($.inArray("CASH", UI_AvailabilitySearch.paymentMethods) == -1) {
				UI_AvailabilitySearch.paymentMethods[UI_AvailabilitySearch.paymentMethods.length] = "CASH";
			}
		}else{
			if ($.inArray("CASH", UI_AvailabilitySearch.paymentMethods)>-1){
				UI_AvailabilitySearch.paymentMethods.splice($.inArray("CASH", UI_AvailabilitySearch.paymentMethods), 1);
			}
		}
		if (UI_AvailabilitySearch.paymentMethods.length!=0){
			UI_AvailabilitySearch.createPaymentMethods();
		}
	}
}
/**
 * Create payment methos
 */
UI_AvailabilitySearch.createPaymentMethods = function() {
	$("#payMethods").empty();
	if (UI_AvailabilitySearch.paymentMethods.length>0){
		$.each(UI_AvailabilitySearch.paymentMethods, function(){
			var tSpan = $("<span></span>").attr("class", this);
			$("#payMethods").append(tSpan);
		});
	}
	var setWidth = function(){
		var twidth = UI_AvailabilitySearch.paymentMethods.length*47;
		var ttWidth = $.trim($("#lblPayingoptions").text()).length*6;
		if (ttWidth>twidth)	twidth = ttWidth;
		return twidth
	};
	$('#trPaymentMethods').css({
		width:setWidth(),
		margin:"0 auto"
	})
	$('#trPaymentMethods').show();
};


/**
 * Display Balance Summary
 */
UI_AvailabilitySearch.displaySummary = function() {
	
	if(UI_AvailabilitySearch.isRequoteFlightSearch) {
		$("#lblCurrentCharges").show();
		$("#lblNewCharges").show();
		$("#lblModifingFlight").hide();
		$("#lblNewFlight").hide();
	} else {
		$("#lblModifingFlight").show();
		$("#lblNewFlight").show();
		$("#lblCurrentCharges").hide();
		$("#lblNewCharges").hide();
	}
	
	var chargesList = $.airutil.dom.cloneObject(UI_AvailabilitySearch.updateChargeList);
	$("#balanceSummaryTemplate").nextAll().remove();
	chargesList = UI_AvailabilitySearch.addCurrencyCode(chargesList);
	$("#balanceSummaryTemplate").iterateTemplete({templeteName:"balanceSummaryTemplate", data:chargesList, dtoName:"chargesList"});
	$("#modifyBal tr:last td").addClass("SeperateorBGColor");
	// $("#modifyBal :contains('Balance To
	// Pay')").parent().parent().children().addClass("SeperateorBGColor");
	$("#balanceSummary").show();
}

/**
 * Set Currency Code
 */
UI_AvailabilitySearch.addCurrencyCode = function(chargesList) {	
	var tempPax = "";
	var shiftKey = 0;
	var currCode = UI_AvailabilitySearch.currencyCode;
	var selectedCurrency = false;
	if(UI_AvailabilitySearch.currencyCode!=UI_AvailabilitySearch.selectedCurrency){
		currCode = UI_AvailabilitySearch.selectedCurrency;
		selectedCurrency = true;
	}
	for (var i = 0; i < chargesList.length; i++) {
		tempPax = chargesList[i];
		var oldCharges = null;
		var newCharges = null;
		if(selectedCurrency){
			oldCharges = tempPax.selCurDisplayOldCharges + " "+ currCode;	
			newCharges = tempPax.selCurDisplayNewCharges + " " + currCode;
		}else{
			oldCharges = tempPax.displayOldCharges + " "+ currCode;	
			newCharges = tempPax.displayNewCharges + " " + currCode;			
		}
		
		if (tempPax.displayOldCharges) {
			tempPax.displayOldCharges = oldCharges;
		}
		if (tempPax.displayNewCharges) {
			tempPax.displayNewCharges = newCharges;		
		}
		
		delete chargesList[i];
		if(tempPax.enableDisplay){
			chargesList[i-shiftKey] = tempPax;
		} else {
			shiftKey = shiftKey +1
		}
	}
	chargesList.length = chargesList.length - shiftKey;
	return chargesList;
}

UI_AvailabilitySearch.refresh = function(){
	$("#txtCaptcha").val("");
	document['imgCPT'].src = '../jcaptcha.jpg?relversion=' + (new Date()).getTime();
}

UI_AvailabilitySearch.isValideImageChapcha = function() {
	var data = {};
	data['captcha'] = $("#txtCaptcha").val();
	$("#frmTemp").attr('action', 'imageChapcha.action');
	$("#frmTemp").ajaxSubmit({data:data,dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress, success: UI_AvailabilitySearch.processImageCapCha, error:UI_commonSystem.setErrorStatus});
	return  false;
}

UI_AvailabilitySearch.processImageCapCha = function(response) {		
	var msgTxt = response.messageTxt;
	if(response != null && response.success == true) {    	  	
    		if (msgTxt != null && msgTxt == "valid") {
    			UI_AvailabilitySearch.CaptureSuccess({capturePass:true})
    		} else {
    			UI_AvailabilitySearch.refresh();
    			jAlert(UI_AvailabilitySearch.labels.lblTypeCorrectImage,'Alert');    			
    		}
	} else {		
		// UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		wrapAndDebugError('005',response);
	}
	UI_commonSystem.loadingCompleted();	
}

UI_AvailabilitySearch.setBalanceSummaryDisplayPage = function() {
	$("#mode").val(UI_AvailabilitySearch.modSegBalSummaryDisplay);
}

UI_AvailabilitySearch.loadAirportSearchMessages = function() {	
	
	var airportMsg = UI_AvailabilitySearch.airportMessage;	
	$('#searchPageAirportMsg').html('');	
	$('#searchPageAirportMsg').append(airportMsg);
	
	if(airportMsg != null && airportMsg != ''){
		$('#searchPageAirportMsg').slideDown('slow');
	} else {
		$('#searchPageAirportMsg').hide();
	}	
}


UI_AvailabilitySearch.isValidSelectedFlightSegments = function() {
	var outRPH = UI_AvailabilitySearch.outFlightRPHList;
	var inRPH = UI_AvailabilitySearch.retFlightRPHList;
	
	if (UI_AvailabilitySearch.returnFlag == "true" && outRPH != "" && inRPH != "") {
		var depSegArrival = UI_AvailabilitySearch.getSelectedFlight(outRPH, UI_AvailabilitySearch.outboundFlights);			
		var depMs = Number(depSegArrival[0].departureTimeLong);
		var depLastSegArrMs = Number(depSegArrival[depSegArrival.length -1].arrivalTimeLong);
		var arrSegArrival = UI_AvailabilitySearch.getSelectedFlight(inRPH, UI_AvailabilitySearch.inboundFlights);			
		var arMs = Number(arrSegArrival[0].departureTimeLong);		
				
		if ((arMs + Number(UI_AvailabilitySearch.minReturnTransitionTime))  <= depLastSegArrMs) {		
			$("#fCalIn").find(".selected").removeClass("selected");			
			$("#fCalIn").find("input[type='radio']").attr("checked","");	
			UI_AvailabilitySearch.setRPHList();	
			UI_AvailabilitySearch.fareQuote = "";			
			$("#trPriceBDPannel").slideUp("slow");
			$("#balanceSummary").hide();
			$("#flightTemplate").hide();
			return false;
		}		
	}
	return true;
}

UI_AvailabilitySearch.showDuration = function(){
	var temp = $("#tableOB_0").parent().children();
	$(".outDuration").remove();
	$("#tableOB_0").append('<td align="center" width="9%" class="rowColor bdRight bdBottom outDuration"><label>'
			+UI_AvailabilitySearch.outBoundDurations+'</label></td>');
	$("#tableOB_0").find("td:last").attr("rowSpan",parseInt(temp.length - 2));
	
	if (UI_AvailabilitySearch.inBoundDurations != ""){
		var temp1 = $("#tableIB_0").parent().children();
		$(".inDuration").remove();
		$("#tableIB_0").append('<td align="center" width="9%" class="rowColor bdRight bdBottom inDuration"><label>'
				+UI_AvailabilitySearch.inBoundDurations+'</label></td>');
		$("#tableIB_0").find("td:last").attr("rowSpan",parseInt(temp1.length - 2));
	}
}

UI_AvailabilitySearch.isSameFlightModification = function() {
	var modifyFlightNos = $("#resModifySegmentRefNos").val();
	var flightDetails = jQuery.parseJSON($("#resModifingFlightInfo").val());
	var buildRPH = "";
	
	if ( modifyFlightNos != "" && flightDetails != "" && flightDetails.length > 0) {
		var modifyFlightNosArr = modifyFlightNos.split(":");
		
		for (var i = 0; i < modifyFlightNosArr.length; i++) {
			
			for (var f = 0; f < flightDetails.length; f++) {
				if (flightDetails[f].flightSegmentRefNumber == modifyFlightNosArr[i]) {
					if ( i == 0) {
						buildRPH = (flightDetails[f].flightRefNumber).split("-")[0];
					} else {
						buildRPH = buildRPH + ":" + ((flightDetails[f].flightRefNumber).split("-")[0]);
					}
					break;
				}
			}
		}
		
	} else {
		jAlert("Modifing flight has a invalid data",'Alert');    	
	}	
	// Get Selected Flight RPH
	var depRPHwithSystem = UI_AvailabilitySearch.outFlightRPHList;
	var depRPH = depRPHwithSystem.split("!")[0];	
	if ($.trim(buildRPH) == $.trim(depRPH)) {
		return  true;
	} else {
		return false;
	}
	
}

// Google analytics
UI_AvailabilitySearch.loadTrackingPage = function() {
	var sendParam="pageID=FLIGHT_SEARCH&rad="+UI_commonSystem.getRandomNumber()+"&src="+UI_AvailabilitySearch.fromAirport+"&dest="+UI_AvailabilitySearch.toAirport+
	"&dt="+UI_AvailabilitySearch.departureDate+"&lan="+SYS_IBECommonParam.locale.toUpperCase()+"&cur="+UI_AvailabilitySearch.selectedCurrency;
	$("#frmTracking").attr("src", "showLoadPage!loadTrackingPage.action?"+sendParam);
    var departDate = UI_AvailabilitySearch.departureDate.split("/");
    departDate = departDate[2] +"-"+ departDate[1] +"-"+ departDate[0];
    var returnDate = "";
    if(UI_AvailabilitySearch.returnDate!==""){
        returnDate = UI_AvailabilitySearch.returnDate.split("/");
        returnDate = returnDate[2] +"-"+ returnDate[1] +"-"+ returnDate[0];
    }
    //Set Dyn Ads for Flights on search results custom tag
    if(ga_conversion_id===undefined){var ga_conversion_id=00000000}
    if( window.google_trackConversion !== undefined){
        window.google_trackConversion({
            google_conversion_id: ga_conversion_id,
            google_remarketing_only: true,
            google_tag_params : {
                flight_originid : UI_AvailabilitySearch.fromAirport,
                flight_destid : UI_AvailabilitySearch.toAirport,
                flight_startdate : departDate,
                flight_enddate : returnDate,
                flight_pagetype : 'searchresults'
            }
        });
    }

}

UI_AvailabilitySearch.ondListLoaded = function(){
	if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
		return false;
	} else {
		return true;
	}
}


UI_AvailabilitySearch.setDefaultSearchOption = function (from, to){
	if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
			UI_AvailabilitySearch.ondListLoaded()) {
		var fromIndex = -1;
		var toIndex = -1;
		// get from, to indexes
		var airportsLength = airports.length;
		for(var i=0;i<airportsLength;i++){
			if(from == airports[i].code){
				fromIndex = i;			
			} else if(to == airports[i].code) {
				toIndex = i;			
			}
			
			if(fromIndex != -1 && toIndex != -1){
				var theLanguage = $('html').attr('lang');
				
				if(theLanguage == 'ar'){
					UI_AvailabilitySearch.fromAirPortName = airports[fromIndex].ar;
					UI_AvailabilitySearch.toAirPortName = airports[toIndex].ar;
					if(UI_AvailabilitySearch.fromAirPortName == undefined || UI_AvailabilitySearch.toAirPortName == undefined){
						var selectedLang = SYS_IBECommonParam.locale;
						var defaultLang = 'en';
						if(airports[fromIndex][selectedLang] != undefined && airports[toIndex][selectedLang] != undefined){
							UI_AvailabilitySearch.fromAirPortName = airports[fromIndex][selectedLang];
							UI_AvailabilitySearch.toAirPortName = airports[toIndex][selectedLang];
						} else if(airports[fromIndex][defaultLang] != undefined && airports[toIndex][defaultLang] != undefined) {
							UI_AvailabilitySearch.fromAirPortName = airports[fromIndex][defaultLang];
							UI_AvailabilitySearch.toAirPortName = airports[toIndex][defaultLang];
						} else {
							UI_AvailabilitySearch.fromAirPortName = $("#resFromAirportName").val();
							UI_AvailabilitySearch.toAirPortName = $("#resToAirportName").val();
						}
					}
				}else{
					var defaultLang = SYS_IBECommonParam.locale;
					if(airports[fromIndex][defaultLang] != undefined && airports[toIndex][defaultLang] != undefined) {
						UI_AvailabilitySearch.fromAirPortName = airports[fromIndex][defaultLang];
						UI_AvailabilitySearch.toAirPortName = airports[toIndex][defaultLang];
						$("#resFromAirportName").val(UI_AvailabilitySearch.fromAirPortName);
						$("#resToAirportName").val(UI_AvailabilitySearch.toAirPortName);
					} else {
						UI_AvailabilitySearch.fromAirPortName = $("#resFromAirportName").val()
						UI_AvailabilitySearch.toAirPortName = $("#resToAirportName").val();
					}
				}
				break;
			}
		}
		
		if(fromIndex != -1){			
			var availDestOptLength = origins[fromIndex].length;
			for(var i=0;i<availDestOptLength;i++){
				var destOptArr = origins[fromIndex][i];
				if(destOptArr[0] == toIndex){
					var isInt = false;
					var optCarriersArr = destOptArr[3].split(",");
					for(var j=0;j<optCarriersArr.length;j++){
						if(optCarriersArr[j] != GLOBALS.defaultCarrierCode){
							isInt = true;
							break;
						}
					}
					
					if(isInt){
						$('#resSearchSystem').val('INT');
					} else {
						$('#resSearchSystem').val('AA');
					}
					
					break;
				}
			}
		} else {			
			$('#resSearchSystem').val('INT');
		}
	} else {
		$('#resSearchSystem').val('AA');
        UI_AvailabilitySearch.fromAirPortName = ($("#resFromAirportName").val()=="")?from:$("#resFromAirportName").val();
        UI_AvailabilitySearch.toAirPortName = ($("#resToAirportName").val()=="")?to:$("#resToAirportName").val();
	}
	$('#resBookingType').val('NORMAL');
	
}


UI_AvailabilitySearch.loadRequoteSegment  = function() {
	
	var newFlightSegRef = UI_AvailabilitySearch.getNewFlightSegRef();	
	
	if(newFlightSegRef!=null && newFlightSegRef!=""){		
		
		var existingCnfSeg = jQuery.parseJSON($("#resOldAllSegments").val());		
		
		for(var i=0;i < existingCnfSeg.length;i++){
			existingCnfSeg[i].returnFlag = existingCnfSeg[i].returnFlag == "Y"?true:false;
		}
		
		var modifyFlightNos = $("#resModifySegmentRefNos").val();		
		
		newONDSegmentList = UI_AvailabilitySearch.getRequoteOnds({existingFltSegList:existingCnfSeg,modFltSegNo:modifyFlightNos});
		
		var valid = UI_AvailabilitySearch.validateNewOnd(newONDSegmentList);
		
		if(valid){
			UI_AvailabilitySearch.showHideAvailableFilghtsOnly(false);
			/*var displayOnds = [];
			for(var i = 0 ; i < newONDSegmentList.length; i++){
				displayOnds[displayOnds.length] = newONDSegmentList[i].getDisplayOnd();
			}*/
			
			//$("#departueFlight").nextAll().remove();
			//$("#departueFlight").iterateTempleteFS({templeteName:"departueFlight", data:displayOnds, dtoName:"displayOnd"});
			UI_AvailabilitySearch.reCalculateFareRequote();
			//dateChangerjobforLanguage(SYS_IBECommonParam.locale);
		}else{
			jAlert(UI_AvailabilitySearch.labels.lblOndOverlapWithExist, 'Alert');	
		}
		
	}else{		
		jAlert(UI_AvailabilitySearch.labels.lblSelectFlight, 'Alert');
	}	
}



getCommonLogicalClasses = function(ondLogicalCCList){
    var viewLCClist = [];
    var testLCClist = [];
    if(ondLogicalCCList != undefined && ondLogicalCCList != null){
        for ( var i = 0; i < ondLogicalCCList.length; i++) {
            var ondLogicalCCObj = ondLogicalCCList[i][0];
            if(ondLogicalCCObj != undefined && ondLogicalCCObj != null){
                var availableLogicalCCs = ondLogicalCCObj.availableLogicalCCList;
                var ondSeq = ondLogicalCCObj.sequence;
                for (var k =0;k<availableLogicalCCs.length;k++){
                    var lcCode = availableLogicalCCs[k].logicalCCCode + "_" + ((availableLogicalCCs[k].withFlexi)?"Y":"N") + "_" + (availableLogicalCCs[k].bundledFarePeriodId||"N");
                    if($.inArray(lcCode,testLCClist)<0){
                        testLCClist[testLCClist.length] = lcCode;
                        var rew = [];
                        rew[0] = lcCode;
                        var t = {
                            "logicalCCDesc": availableLogicalCCs[k].logicalCCDesc,
                            "comment": availableLogicalCCs[k].comment,
                            "ondSeq": ondSeq,
                            "bookingCodes": availableLogicalCCs[k].bookingClasses,
                            "logicalCCCode":availableLogicalCCs[k].logicalCCCode,
                            "bundledFarePeriodId":availableLogicalCCs[k].bundledFarePeriodId,
                            "withFlexi":availableLogicalCCs[k].withFlexi,
                            "selected":availableLogicalCCs[k].selected,
                            "imageUrl":availableLogicalCCs[k].imageUrl
                        }
                        rew[1] = t;
                        viewLCClist[viewLCClist.length] =rew;
                    }
                }
            }
        }
    }
    return viewLCClist;
}


setAvailabilityOfLogicalCabin = function(index, commonLCC, ondLogicalCCList, slFlight){
	isLogicalCCSelected = false;
    var tdWidth = ["20%", "8%", "10%", "10%", "8%"];
    var flHTMLROW = slFareQuoteRow.clone();
    flHTMLROW.attr("id", "SelectedItemsFL_done_"+index);
    var lcWidth =parseInt(40/commonLCC.length,10);
    if(commonLCC.length>0){
        for (var cc=0;cc<commonLCC.length;cc++){
            slFlight[commonLCC[cc][0]] = "N/A";
            var obj = commonLCC[cc][1];
            var tdHtml = $('<td class="GridItems bdRight tdFareColumn" align="center" width="'+lcWidth+'">'+
                '<label id="'+commonLCC[cc][0]+'" title="'+obj.comment+'">'+obj.logicalCCDesc+'</label>'+
                '</td>');
            flHTMLROW.append(tdHtml);
        }

        for ( var i = 0; i < ondLogicalCCList.length; i++) {
            var ondLogicalCCObj = ondLogicalCCList[i][0];
            if(ondLogicalCCObj != undefined && ondLogicalCCObj != null){
                var ondSeq = ondLogicalCCObj.sequence;
                if(ondSeq == index){
                    var availableLogicalCCs = ondLogicalCCObj.availableLogicalCCList;
                    if(availableLogicalCCs != undefined && availableLogicalCCs != null){
                        for ( var j = 0; j < availableLogicalCCs.length; j++) {
                            var lcCode = availableLogicalCCs[j].logicalCCCode + "_" + ((availableLogicalCCs[j].withFlexi)?"Y":"N") + "_" + (availableLogicalCCs[j].bundledFarePeriodId||"N");
                            for (var cc=0;cc<commonLCC.length;cc++){
                                if (lcCode===commonLCC[cc][0]){
                                    var obj = availableLogicalCCs[j];
                                    var value = "radio_"+ ondSeq + "_" + obj.logicalCCCode;
                                    value += obj.withFlexi ? '_Y' : '_N';
                                    value += '_' + obj.bundledFarePeriodId + '_' + $.toJSON(obj.bookingClasses) + '_';
                                    value += cc + '_' + obj.selected + '_' + $.toJSON(obj.segmentBookingClasses);
                                    slFlight[commonLCC[cc][0]] = value;
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
        flHTMLROW.find("label").each(function(indx){
            var tempData = ""
            if(typeof(slFlight[this.id])==="object"){
                tempData = (slFlight[this.id]).join("<br/>");
            }else{
                tempData = slFlight[this.id];
            }
            $(this).html(tempData);

            if(tempData.indexOf("radio_")>-1){
                var temdataArray = tempData.split("_");

                var name = 'logical_' + temdataArray[1];
                var value = temdataArray[1] + '_' + temdataArray[2];
                value += '_' +temdataArray[3] + '_' + temdataArray[4];
                value += '_' + temdataArray[5] + '_' + temdataArray[8];
                var logicalRadio = $("<input>").attr({"type":"radio",
                    "id":temdataArray[1] + '_' + temdataArray[6],
                    "value" : value,
                    "name" : name,
                    "checked": (temdataArray[7]=="true")?"checked":""});
                
                var withFlexi = true;
                if(temdataArray[3] != "Y"){
                    withFlexi = false;
                }
                
                var bundledFarePeriodId = null;
                if(temdataArray[4] != '' && temdataArray[4] != 'null'){
                    bundledFarePeriodId = temdataArray[4];
                }
                
                var bookingClasses = null;
                if(temdataArray[5] != '' && temdataArray[5] != 'null'){
                    bookingClasses = $.parseJSON(temdataArray[5]);
                }

                var segBookingClasses = null;
                if(temdataArray[8] != '' && temdataArray[8] != 'null'){
                	segBookingClasses = $.parseJSON(temdataArray[8]);
                }
                //Set Default selection
                if(temdataArray[7]=="true"){												
					UI_AvailabilitySearch.setLogicalCCSelection({"ondSeq": temdataArray[1], "logicalCCCode": temdataArray[2],
						"withFlexi": withFlexi, "bundledFarePeriodId": bundledFarePeriodId, 
						"bookingClasses": bookingClasses, "segBookingClasses" : segBookingClasses});
				}
                
                logicalRadio.off("click").on("click",function(e){
                	isLogicalCCSelected = true;
                    var arrVal = e.target.value.split("_");
                    var ondSeq = parseInt(arrVal[0], 10);
                    var logicalCCCode = arrVal[1];
                    var withFlexi = true;
                    if(arrVal[2] != "Y"){
                        withFlexi = false;
                    }

                    var bundledFarePeriodId = null;
                    if(arrVal[3] != '' && arrVal[3] != 'null'){
                        bundledFarePeriodId = arrVal[3];
                    }

                    var bookingClasses = null;
                    if(arrVal[4] != '' && arrVal[4] != 'null'){
                        bookingClasses = $.parseJSON(arrVal[4]);
                    }

                    var segBookingClasses = null;
                    if(arrVal[5] != '' && arrVal[5] != 'null'){
                    	segBookingClasses = $.parseJSON(arrVal[5]);
                    }

                    UI_AvailabilitySearch.setLogicalCCSelection({"ondSeq": ondSeq, "logicalCCCode": logicalCCCode,
                        "withFlexi": withFlexi, "bundledFarePeriodId": bundledFarePeriodId,
                        "bookingClasses": bookingClasses, "segBookingClasses" : segBookingClasses});
                    
                    UI_AvailabilitySearch.reCalculateFareRequote();
                });
                $(this).parent().empty().append(logicalRadio);
                logicalRadio.prop("checked", temdataArray[7]=="true");
            }

        });
        if(index%2==0){
            flHTMLROW.addClass("even")
        }
        $("#SelectedItemsFL_done").append(flHTMLROW);
    }
}

setAvailableLogicalCabinClasses = function(ondLogicalCCList){
    $("#farequote_done_Area")
        .empty()
        .append(slFareQuoteTable.clone());

    var commonLCC = getCommonLogicalClasses(ondLogicalCCList);
    var wd = (30/commonLCC.length);
    $.each(commonLCC, function(x,LC){
        var obj = commonLCC[x][1];
        var thHtml = $('<td class="gridHD fareClass bdRight Y_Y_class normal-table" align="center" width="'+wd+'%">' +
            '<label class="gridHDFont fntBold">'+obj.logicalCCDesc+
            ' <span role="' + obj.comment + '|' + obj.imageUrl + '" class="ttip">&nbsp;<img src="../images/spacer_no_cache.gif" width="7" height="13" alt=""></span>'+
            '</label></td>');
        $("#shoping_Cart_head").append(thHtml);
    });
    //$.data($("#SelectedItemsFL_"+cur)[0], "SLF", slFlight);
    var selectedFlightWithLogicalCCs = [];
    var displayOnds = [];
	for(var i = 0 ; i < newONDSegmentList.length; i++){
		displayOnds[displayOnds.length] = newONDSegmentList[i].getDisplayOnd();
	}
    $.each(displayOnds, function(index, j){
    //$("#SelectedItemsFL>tr").each(function(index){
        var slFlight = j;
        setAvailabilityOfLogicalCabin(index, commonLCC, ondLogicalCCList, slFlight);
    });
    insertParam("st", "reQuoteFare");
    
    $(".ttip").tooltip({
	    bodyHandler: function() {
	    	var text = $(this).attr('role').split("|");
	    	var html = '';
	    	if(text[1] != undefined && text[1] != null 
	    			&& text[1] != '' && text[1] != 'null'){
	    		html += "<img src='"+text[1]+"'/><br/>";
	    	}
	 		return html; 
	    }, 
	    showURL: false,
	    delay: 0,
	    track: true,
        top: 10,
        extraClass: "caltooltip"
    });
}



/**
 * Flight Segments Display template
 */
// TODO:Refactor

$.fn.iterateTempleteFS = function(params) {

	var templeteName = params.templeteName;
	var clonedTemplateId;
	var tempClonedTemplateId = "";
	var elementIdAppend;
    var selected = false;
    var flightRefNumbers = "";
    var flightRefNumberTemp = "";
    var tempID = "";
	var dtoRecord;
	var rowspan = 1;
	var tempElementID = "";
	var isModifable = true;
	var isCancellable = true;
	var isAddGroundSegmentAllowed = true;
	var isAddGroundSegmentButtonShow = false;

	// var reservationStatus = UI_ModifyReservationDetail.bookingInfo.status;
	var reservationStatus = "CNF";
	for(var i=0; i < params.data.length ; i++) {		
		var isNewSegment = false;		
		dtoRecord = params.data[i];
		
		clonedTemplateId = templeteName + "_" + i;		
		
		
		
		var clone = $( "#" + templeteName + "").clone();		
		clone.attr("id", clonedTemplateId );		
		clone.appendTo($("#" + templeteName).parent());
		elementIdAppend = params.dtoName + "[" + i + "].";	
		
		
		// setting the value and id of labels
		$("#"+clonedTemplateId).find("label").each(function( intIndex ){
			$(this).html(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});			
		
		// setting the value and id of labels
		$("#"+clonedTemplateId).find("img").each(function( intIndex ){			
			$(this).attr("src", dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		
		$("#"+clonedTemplateId).find("div").each(function( intIndex ){			
			$(this).attr("id",  this.id + '_' + i);
		});	
		
		if (tempID != dtoRecord.interLineGroupKey) {
			rowspan = 1;
			// Reset values
			isModifable = true;
			isCancellable = true;
		}
		
		// setting the value, id and name of inputs
		$("#"+clonedTemplateId).find("input").each(function( intIndex ){
			$(this).val(dtoRecord[this.id]);
			if (this.id == "flightSegmentRefNumber") {
				flightRefNumberTemp = dtoRecord[this.id];				
				if (rowspan == 1) {
					tempElementID =  elementIdAppend +  this.id;					
				} 
			}
			$(this).attr("id",  elementIdAppend +  this.id);
			$(this).attr("name",  this.id);
			
		});		
		
		if (dtoRecord.status == 'CNX') {
			$("#"+clonedTemplateId).find(" td:last input").remove();
			$( "#" + clonedTemplateId).find(" label").addClass("fntCnxSeg");
		} else if (reservationStatus == "ONHOLD") {
			$("#"+clonedTemplateId).find(" td:last input").remove();
			$("#"+clonedTemplateId).find(" td:last img").remove();
	    } else {
	    	if (!(isModifable && dtoRecord.isModifiable)) {
	    		isModifable = false;
	    	}
	    	if (!(isCancellable && dtoRecord.isCancellable)) {
	    		isCancellable = false;
	    	}
		}		

		tempID = dtoRecord.interLineGroupKey;			
		
		
		if (i%2 == 0) {
			$( "#" + clonedTemplateId).find(" td").addClass("SeperateorBGColor");
		}
		
		$("#" + clonedTemplateId).show();		
		}
	
	$( "#" + templeteName).hide();
}

UI_AvailabilitySearch.getModifiedSegmentInfo = function(params){
	
	var flightList = (UI_AvailabilitySearch.outboundFlights).concat(UI_AvailabilitySearch.inboundFlights);
	//var newFlightSegRef = $("#fCalOut_FL").find("input[type='radio']:checked").parent().find(".flightRefId").text();
	var newFlightSegRef = UI_AvailabilitySearch.getNewFlightSegRef();
	var selectedFlight =UI_AvailabilitySearch.getSelectedFlight(newFlightSegRef, flightList); 
	if(selectedFlight!=null && selectedFlight[0]!=null){
		
		var duration ;
		
		if(selectedFlight[0].duration!=null && selectedFlight[0].duration!=""){
			duration = $.trim(selectedFlight[0].duration).replace(/[a-zA-Z]/g, '').replace(' ',':');
		}			
		var orgin;
		var destin;
		if(selectedFlight[0].segmentShortCode!=null){
			segArr= selectedFlight[0].segmentShortCode.split("/");
			if(segArr.length>1){
				orgin =segArr[0];
				destin =segArr[segArr.length-1];	
			}
			
		}
		
	  modFlightSegInfo = {airLine: selectedFlight[0].carrierCode,
			  			arrival:destin,
			  			arrivalDate: selectedFlight[0].arrivalDate,
			  			arrivalDateLong: selectedFlight[0].arrivalTimeLong,
			  			arrivalText: "ARR",
			  			arrivalTime: selectedFlight[0].arrivalTime,
			  			arrivalTimeLong: selectedFlight[0].arrivalTime,
			  			arrivalTimeZuluLong: selectedFlight[0].arrivalTimeZuluLong,
			  			carrierImagePath: selectedFlight[0].carrierImagePath,	 
			  			departure: orgin,
			  			departureDate: selectedFlight[0].departureDate,
			  			departureDateLong: selectedFlight[0].departureTimeLong,	
			  			departureText: "DEP",
			  			departureTime: selectedFlight[0].departureTime,
			  			departureTimeZuluLong: selectedFlight[0].departureTimeZuluLong,	
			  			duration:duration ,
			  			flightNo: selectedFlight[0].flightNumber,
			  			flightRefNumber: selectedFlight[0].flightRefNumber,
			  			orignNDest: selectedFlight[0].segmentCode+" - (NEW)",
			  			segmentShortCode: selectedFlight[0].segmentShortCode,
			  			pnrSegID : null,
			  			status: "NEW",
			  			system: "AA",
			  			oldPerPaxFareTO : null};
	  
	  return modFlightSegInfo;
	}
	
	return null;
}


UI_AvailabilitySearch.showHideAvailableFilghtsOnly = function(showFlights){	
	if(showFlights){
		$("#tabs1").show();
		$("#modifyBtnSet").show();		
		$("#tabs2").hide();
		$("#trPriceBDPannel").slideUp("slow");	
		$("#balanceSummary").slideUp("slow");		
		$("#trAccept").hide();
		$("#btnContinue").hide();
		if(UI_AvailabilitySearch.isCalendarViewDisabled()){						
			$("#divFlightDetailsRequote").show();
		}
	}else{
		$("#tabs1").hide();
		$("#modifyBtnSet").hide();
		$("#tabs2").show();		
		$("#btnRequote").show();
		if(UI_AvailabilitySearch.isCalendarViewDisabled()){			
			$("#divFlightDetailsRequote").hide();
		}
	}
	
	UI_AvailabilitySearch.resetFlightOptionSelction();
	
}

UI_AvailabilitySearch.resetFlightOptionSelction = function(){	
	var lccMap = UI_AvailabilitySearch.ondWiseLogicalCCSelected;
	if(lccMap == null || lccMap == "" || $.isEmptyObject(lccMap)){
		lccMap = $.parseJSON($("#resFareQuoteLogicalCCSelection").val());
	}
	
	if(lccMap != null && lccMap != ""){		
		$.each(lccMap, function(key, value){
			UI_AvailabilitySearch.setLogicalCCSelection({'ondSeq': key, 'logicalCCCode': null,
				'withFlexi': false, 'bundledFarePeriodId': null, 'bookingClasses': null, 'segBookingClasses' : null});
		});
	}
}

UI_AvailabilitySearch.userSelected = "";
UI_AvailabilitySearch.reCalculateFareRequote = function(e, typ) {
		//Airarabia no need to display flight details directed to balance summarry on cancellation 
		if(UI_AvailabilitySearch.isCnxSegmentRequote || UI_AvailabilitySearch.isNameChangeRequote){
			$("#divLoadBg").hide();
		}
		UI_AvailabilitySearch.setRPHList();
			
		var rphData = UI_AvailabilitySearch.getRPHData(typ, e);
		UI_AvailabilitySearch.userSelected = typ;
		if(rphData){			 
			 UI_commonSystem.loadingProgress();
			 if (rphData.system!=undefined){
				 $("#resSearchSystem").val(rphData.system);
				 $("#resSearchSystem").val(rphData.system);	
			 }
			 var url = 'fareRequote.action';			 
			 var data = createSearchSubmitData();			 
			 data  =  $.airutil.dom.concatObjects(data, rphData.data);		
			 
			 var ondSearchDTOList = UI_AvailabilitySearch.getONDSearchDTOList();
			 UI_AvailabilitySearch.overrideBundledServicesForRequote(ondSearchDTOList);
			 
			 data['cancelSegment'] = UI_AvailabilitySearch.isCnxSegmentRequote;
			 data['nameChange'] = UI_AvailabilitySearch.isNameChangeRequote;
			 data['searchParams.ondListString'] = $.toJSON(ondSearchDTOList);
			 data['searchParams.ticketValidTillStr'] = UI_AvailabilitySearch.getTicketValidTill();
			 data['searchParams.lastFareQuoteDateStr'] = UI_AvailabilitySearch.getLastFareQuoteDate();		
			 
			 if(UI_AvailabilitySearch.isCalendarViewDisabled()){
				 data['searchParams.fareQuoteLogicalCCSelection'] = $("#resFareQuoteLogicalCCSelection").val();
				 data['searchParams.ondQuoteFlexiStr'] = $("#resOndQuoteFlexi").val();
				 data['searchParams.ondSelectedFlexiStr'] = $("#resOndSelectedFlexiStr").val();
				 data['searchParams.flightSearchOndFlexiSelectionStr'] = $("#resflightSearchOndFlexiSelection").val();
				 data['searchParams.preferredBundledFares'] = $("#resOndBundleFareStr").val();
				 data['searchParams.preferredBookingCodes'] = $("#resOndBookingClassStr").val();
				 data['searchParams.ondSegBookingClassStr'] = $("#resOndSegBookingClassStr").val();
			 }
			 
			 $("#resOndListStr").val(ondSearchDTOList);
			 // temp
			 data['searchParams.searchSystem']= UI_AvailabilitySearch.getSelectedSystem();
			 
			 $.ajax({type: "POST", dataType: 'json',data: data , url:url,		
			 success: UI_AvailabilitySearch.reCalculateFareProcess, error:UI_commonSystem.setErrorStatus, cache : false, beforeSubmit:UI_commonSystem.loadingProgress});
			
		}else{
			$("#trPriceBDPannel").slideUp("slow");	
		}
		return false;	 
}

UI_AvailabilitySearch.overrideBundledServicesForRequote = function(ondSearchDTOList){
	if(isLogicalCCSelected && UI_AvailabilitySearch.ondWiseBundleFareSelected != null &&
			!$.isEmptyObject(UI_AvailabilitySearch.ondWiseBundleFareSelected)){	
		for ( var ondIndex = 0; ondIndex < ondSearchDTOList.length; ondIndex++) {			
			ondSearchDTOList[ondIndex].bundledServicePeriodId = UI_AvailabilitySearch.ondWiseBundleFareSelected[ondIndex];
		}
	}
}

/** ******************************* */
/** * Requote Flow relate methods ** */
/** ******************************* */

var ondStatus = {
	    NEW : "NEW",
	    CNF : "CNF",
	    OPENRT : "OPENRT"
	}

UI_AvailabilitySearch.getRequoteOnds = function(inParams) {
	
	var allSegs = inParams.existingFltSegList;	
	var originalJourney = "";
	var fareONDMap = {};	
		
	function sortSegs(seg1, seg2) {
		return decodeJsonDate(seg1.departureDateZulu).getTime() > decodeJsonDate(seg2.departureDateZulu).getTime() ? 1 : -1;
	}	
	
	allSegs.sort(sortSegs);
	
	
	for ( var j = 0; j < allSegs.length; j++) {
		originalJourney += allSegs[j].flightSegmentRefNumber.split('$')[1];
		originalJourney += ",";	
		if (allSegs[j].status != 'CNX') {
			// out[out.length] = allSegs[i];
			if (allSegs[j].interlineGroupKey != null) {
				if (fareONDMap[allSegs[j].interlineGroupKey] == null) {
					fareONDMap[allSegs[j].interlineGroupKey] = {
						flights : []
					};
				}
				var arr = fareONDMap[allSegs[j].interlineGroupKey].flights;
				arr[arr.length] = allSegs[j];
			} else {
				alert(' grp key is [' + allSegs[j].interlineGroupKey
				        + ']');
			}
		}
	}
	
	function isModifyingFlight(flts){
		var modifyingRefNumbers = inParams.modFltSegNo.split(":");
		for(var i = 0 ; i < flts.length; i++){
			for(var j = 0 ; j < modifyingRefNumbers.length; j++){
				if(flts[i].bookingFlightSegmentRefNumber ==  modifyingRefNumbers[j]){
					modifyingRefNumbers.splice(j,1);
				}
			}
		}
		if(modifyingRefNumbers.length == 0){
			return true;
		} else if(modifyingRefNumbers.length > 0 && modifyingRefNumbers.length != inParams.modFltSegNo.split(":").length){
			alert("Invalid fare group Ond modification attempted");
			throw "Invalid fare group Ond modification attempted";
		}
		return false;
	}
	
	function isSameSegmentModifying(flts,selFlts){
		var modifyingRefNumbers = inParams.modFltSegNo.split(":");
		for(var i = 0 ; i < flts.length; i++){
			var fltRefNo = flts[i].bookingFlightSegmentRefNumber;
			var fltSegRefNo = flts[i].flightSegmentRefNumber;
			var cabinClass = flts[i].cabinClassCode;
			
			for(var j = 0 ; j < modifyingRefNumbers.length; j++){
				if(fltRefNo ==  modifyingRefNumbers[j]){
 					if (!containsSameSegment(selFlts,fltSegRefNo) || cabinClass!=UI_AvailabilitySearch.classOFService) {
						return false;
					}
				}
			}
		}		
		
		if(modifyingRefNumbers.length!=selFlts.length){
			return false;
		}
		
		return true;
	}
	
	function isBusNoMoreValid(flts){
		var modifyingRefNumbers = inParams.modFltSegNo.split(":");
		var groundSegmentId = null;
		for(var i=0;i< inParams.existingFltSegList.length;i++){
			var existFlt = inParams.existingFltSegList[i];
			for(var j = 0 ; j < modifyingRefNumbers.length; j++){
				if(existFlt.pnrSegID==modifyingRefNumbers[j]){
					groundSegmentId = existFlt.groundStationPnrSegmentID;
				}
			}
		}	
		
		for(var i = 0 ; i < flts.length; i++){
			if(groundSegmentId!=null && groundSegmentId== flts[i].pnrSegID){
				return true;
			} 
		}
		return false;
	}	
	
	function containsSameSegment(selFlts,fltSegRefNo){
		 
		for(var i = 0 ; i < selFlts.length; i++){						
			if(fltSegRefNo == selFlts[i].flightRefNumber.split("#")[0]){
				return true;
			} 		
		}	
		return false;
	}
	
	function sortOnds(ond1, ond2) {
		return ond1.getFirstDepartureDTZuluMillis() > ond2.getFirstDepartureDTZuluMillis() ? 1 : -1;
	}	
	
	function getTotalJourney(tmpOndList){
		var totalJourney = "";
		for ( var i = 0; i < tmpOndList.length; i++) {
			var fltRefList = tmpOndList[i].getFlightRPHList();
			for(var j=0; j < fltRefList.length; j++){
				totalJourney += fltRefList[j].split('$')[1];
				totalJourney += ",";				
			}
		}
		return totalJourney;
	} 
	
	function getReverseJourney(originalJourney){
		var originalSegments = originalJourney.split(',');
		var reverseSeg = "";
		var reverseJourney = "";
		for ( var i = 0; i < originalSegments.length; i++) {
			var elements = originalSegments[i].split("/");
			var count = elements.length - 1;
			reverseSeg = "";
			while (count >= 0) {
				if (reverseSeg == "") {
					reverseSeg = elements[count];
				} else {
					reverseSeg = reverseSeg + "/" + elements[count];
				}
				count--;
			}
			reverseJourney = reverseSeg + reverseJourney;
		}
		return reverseJourney;
	}
	
	function isEligibleToSameBCModification(originalJourney, currentJourney) {
		if (originalJourney.indexOf(currentJourney) >= 0
				|| currentJourney.indexOf(originalJourney) >= 0) {
			if (originalJourney.length >= currentJourney.length
					&& originalJourney.split(currentJourney).length >= 0) {
				var removingJourneyOnd = originalJourney.replace(currentJourney,
				'');
				if(removingJourneyOnd.replace(/,/g,'') == getReverseJourney(currentJourney)){
					return true;
				}else{
					return false;
				}				
			} else if (currentJourney.length >= originalJourney.length
					&& currentJourney.split(originalJourney).length >= 0) {
				var addingJourneyOnd = currentJourney.replace(originalJourney,
						'');
				if (addingJourneyOnd.replace(/,/g, '') == getReverseJourney(originalJourney)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
		
	var out = [];

	$.each(fareONDMap, function(grpKey, obj) {
		var ondInfo = new ONDInfo();
		if(!isModifyingFlight(obj.flights)){		
			if(!isBusNoMoreValid(obj.flights)){
				ondInfo.addExistingFlightSegments(obj.flights);
				ondInfo.setOldPerPaxFareTOList(populateOldPerPaxFareTOList(obj.flights));
				out[out.length] = ondInfo;	
			}			
		} else {			
			if(UI_AvailabilitySearch.isCnxSegmentRequote){
				var remOnd = new ONDInfo();
				remOnd.addExistingFlightSegments(obj.flights);
				removedOndList[removedOndList.length] = remOnd;
			}else{
				var flightList = (UI_AvailabilitySearch.outboundFlights);
				//var newFlightSegRef = $("#fCalOut_FL").find("input[type='radio']:checked").parent().find(".flightRefId").text();
				var newFlightSegRef = UI_AvailabilitySearch.getNewFlightSegRef();
				var selectedFlights =UI_AvailabilitySearch.getSelectedFlight(newFlightSegRef, flightList); 
				
				if(!isSameSegmentModifying(obj.flights,selectedFlights)) {
					if(obj.flights[0].returnFlag){
						for(var i=0;i < selectedFlights.length;i++){
							selectedFlights[i].returnFlag = true;
						}
					}
					// modfified to another segment
					ondInfo.addNewFlightSegments(selectedFlights);
					if(selectedFlights.length == obj.flights.length){						
						ondInfo.setOldPerPaxFareTOList(populateOldPerPaxFareTOList(obj.flights));
						ondInfo.setDateChangedResSegList(getONDFlightRPHList(obj.flights));
					}					
					
					// add the removing ond
					var remOnd = new ONDInfo();
					remOnd.addExistingFlightSegments(obj.flights);
					removedOndList[removedOndList.length] = remOnd;
					out[out.length] = ondInfo;			
					
					ondInfo.setModifiedResSegList(remOnd.getResSegRPHList());
				} else {
					// modfified to same flight segment
					ondInfo.addNewFlightSegments(selectedFlights);
					ondInfo.setOldPerPaxFareTOList(populateOldPerPaxFareTOList(obj.flights));
					ondInfo.setSameFlightModification(true);
					out[out.length] = ondInfo;
					
					// add the removing ond
					var remOnd = new ONDInfo();
					remOnd.addExistingFlightSegments(obj.flights);
					removedOndList[removedOndList.length] = remOnd;
					
					ondInfo.setModifiedResSegList(remOnd.getResSegRPHList());
				}
			}		
			
		}
	});
	
	out = out.sort(sortOnds);	
	var currentJourney = getTotalJourney(out);
	
	if (originalJourney != currentJourney
			&& isEligibleToSameBCModification(originalJourney,
					currentJourney)) {
		for ( var i = 0; i < out.length; i++) {
			out[i].setEligibleToSameBCMod(true);
		}
	} else if (originalJourney != currentJourney) {
		for ( var i = 0; i < out.length; i++) {
			out[i].dateChangedResSegList = null;
			out[i].setEligibleToSameBCMod(false);
		}
	}	
	return out;
}

function ONDInfo(){
	var flights = [];
	var origin = null;
	var destination = null;
	var status = null;
	var cabinClass = null;
	var bookingType = null;
	var bookingClass = null;
	var lastFareQuoteDate = null;
	var ticketValidTill = null;
	var duration = null;
	var segmentCodeFull = null;
	var logicalCabinClass = null;
	var oldPerPaxFareTOList = null;	
	var dateChangedResSegList = null;
	var eligibleToSameBCMod = false;
	var sameFlightModification = false;
	var modifiedResSegList = null;
	var bundledServicePeriodId = null;
	var busSegment = false;
	var groundStationPnrSegmentID = null;
	
	this.addExistingFlightSegments = function(flts){
		for(var i = 0 ; i < flts.length ; i++){
			addFlightSegment(convertExistingFltInfo(flts[i]));
		}
		deriveOndInfo();
		
	}
	
	this.addNewFlightSegments = function(flts){
		for(var i = 0 ; i < flts.length ; i++){
			addFlightSegment(convertNewFltInfo(flts[i]));
		}
		deriveOndInfo();
	}	
	
	function deriveOndInfo(){
		origin = flights[0].segmentCode.split('/')[0];
		var arr = flights[flights.length - 1].segmentCode.split('/');
		destination = arr[arr.length - 1];
		duration = flights[0].flightDuration;
		bookingType = flights[0].bookingType;
		bookingClass = null;
		status = flights[0].status;
		segmentCodeFull = flights[0].segmentCodeFull; 
		ticketValidTill = flights[0].ticketValidTill;
		lastFareQuoteDate = flights[0].lastFareQuoteDate;
		cabinClass = flights[0].cabinClass;
		logicalCabinClass = flights[0].logicalCabinClass;
		oldPerPaxFareTOList = populateOldPerPaxFareTOList(flights);
		bundledServicePeriodId = flights[0].bundledServicePeriodId;
		busSegment = flights[0].busSegment;
		groundStationPnrSegmentID = flights[0].groundStationPnrSegmentID;
	}
	
	
	function addFlightSegment(flight){
		flights[flights.length] = flight;
		flights.sort(sortFltSegs);
	}
	
	this.getFlightSegment = function(){		
		return flights;
	}
	
	function sortFltSegs(fltSeg1, fltSeg2) {
		return fltSeg1.departureDTZuluMillis > fltSeg2.departureDTZuluMillis ? 1 : -1;
	}
	
	// 2012-07-05T04:26:38
	function getJsonDate(timeInMillis){
		var now = new Date();
		var d = UI_AvailabilitySearch.nullyFyGMTOffsetAndGetDate(now.getTimezoneOffset(),timeInMillis);
		return d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+'T'+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
	}

	UI_AvailabilitySearch.nullyFyGMTOffsetAndGetDate = function(gmtOffset,timeInMillis){
		gmtOffset = gmtOffset*60*1000;
		var d = null;
		if(gmtOffset<0){
			d= new Date(timeInMillis + gmtOffset) 
		}else{
			d= new Date(timeInMillis - gmtOffset)
		}
		return d;
	}

	function leadingZero(val) {
		var str = val.toString();
            if(str.length == 1) {
			str = '0' + str;
		}
		 
	   return str;
	}
	
	function timeFmt(duration){		
		if(duration!=null && duration!=""){
			var timeArr = $.trim(duration).replace(/[a-zA-Z]/g, '').replace(' ',':').split(':')
			return leadingZero(timeArr[0])+':'+leadingZero(timeArr[1]);
		}	
	}
	
	// get the dateValue from date
	function getDateTimeFromJason(jsdate){
		var a = jsdate.split('T');
		var d = a[0].split('-');
		var t = a[1].split(':');
		return d[0].substr(2,2) +""+ d[1] +""+ d[2] +""+ t[0] +""+ t[1];
	}
	

	function convertExistingFltInfo(fltSeg){
		var busSegment=false;
		if(fltSeg.subStationShortName!=null &&  $.trim(fltSeg.subStationShortName)!=""){
			busSegment=true;
		}
		return {	    
			carrierCode: fltSeg.carrierCode,
			segmentCode: fltSeg.segmentCode,
		    flightNo :fltSeg.flightNo,
		    departureDate : fltSeg.departureDate , // json std
		    departureDateValue : (fltSeg.departureDateValue==undefined)?getDateTimeFromJason(fltSeg.departureDate):fltSeg.departureDateValue,
		    arrivalDate : fltSeg.arrivalDate,	    // json std
		    arrivalDateValue : (fltSeg.arrivalDateValue==undefined)?getDateTimeFromJason(fltSeg.arrivalDate):fltSeg.arrivalDateValue,
		    departureDateTimeZulu : fltSeg.departureDateZulu ,
		    arrivalDateTimeZulu : fltSeg.arrivalDateZulu ,
		    departureDTZuluMillis: decodeJsonDate(fltSeg.departureDateZulu).getTime(),
  			arrivalDTZuluMillis: decodeJsonDate(fltSeg.arrivalDateZulu).getTime(), 
		    segmentRPH : (fltSeg.routeRefNumber==undefined)?fltSeg.flightSegmentRefNumber:fltSeg.flightSegmentRefNumber+ "#" + fltSeg.routeRefNumber,
		    resSegmentRPH : fltSeg.bookingFlightSegmentRefNumber,
		    status : fltSeg.status ,
		    bookingType : fltSeg.bookingType,
		    flownSegment : fltSeg.flownSegment,
		    bookingClass : '',
		    segmentCodeFull :fltSeg.departureAirportName+' / '+fltSeg.arrivalAirportName,
		    flightDuration:fltSeg.flightDuration,
		    ticketValidTill:fltSeg.ticketValidTill,
		    lastFareQuoteDate:fltSeg.lastFareQuoteDate,
		    cabinClass : fltSeg.cabinClassCode,
		    logicalCabinClass : fltSeg.logicalCabinClass,
		    domesticFlight : fltSeg.domesticFlight,
		    oldPerPaxFareTO : fltSeg.fareTO,
		    isReturnFlight	: fltSeg.returnFlag,
		    bundledServicePeriodId : fltSeg.bundledServicePeriodId,
		    busSegment : busSegment,
		    groundStationPnrSegmentID : fltSeg.groundStationPnrSegmentID
		}
	}
	
	function convertNewFltInfo(fltSeg){
		return {
			carrierCode: fltSeg.carrierCode,
			segmentCode: fltSeg.segmentShortCode,
			flightNo: fltSeg.flightNumber,
			departureDateStr : fltSeg.departureDate , // "Sun, 12 Aug 12"
			departureDateValue : fltSeg.departureDateValue ,
			arrivalDateStr : fltSeg.arrivalDate,	   // "Sun, 12 Aug 12"
			arrivalDateValue : fltSeg.arrivalDateValue,
			departureTime : fltSeg.departureTime , // "20:40"
			arrivalTime : fltSeg.arrivalTime,	   // "20:40"
			departureDate : fltSeg.depLocalDateTimeJson , // json std
			arrivalDate : fltSeg.arrLocalDateTimeJson,	    // json std
			departureDTZuluMillis: fltSeg.departureTimeZuluLong,
	  		arrivalDTZuluMillis: fltSeg.arrivalTimeZuluLong, 
			departureDateTimeZulu : fltSeg.depZuluDateTimeJson , 
			arrivalDateTimeZulu : fltSeg.arrZuluDateTimeJson,
  			segmentRPH: fltSeg.flightRefNumber,
  			resSegmentRPH : null,
 		    status : 'NEW' ,
  			orignNDest: fltSeg.segmentCode,
  			segmentShortCode: fltSeg.segmentShortCode,
  			bookingType : 'NORMAL', // TODO
		    flownSegment : false,
		    bookingClass : '',
		    segmentCodeFull :fltSeg.segmentCode,
		    flightDuration:timeFmt(fltSeg.duration),
		    cabinClass : UI_AvailabilitySearch.classOFService,
		    logicalCabinClass : UI_AvailabilitySearch.classOFService,
		    domesticFlight : fltSeg.domesticFlight,
		    fareTO : null,
		    isReturnFlight	: fltSeg.returnFlag,
		    bundledServicePeriodId : null,
		    busSegment : false,
		    groundStationPnrSegmentID : null
  		};
	}
	
	this.getFirstDepartureDTZuluMillis = function() {
		return flights[0].departureDTZuluMillis;
	};

	this.getLastArrivalDTZuluMillis = function() {
		return flights[flights.length - 1].arrivalDTZuluMillis;
	};
	

	this.getFirstDepartureDate = function(){
		return flights[0].departureDate;
	}
	function getFirstDepartureDateTime(){
		return flights[0].departureDate;
	}	
	
	function getLastArrivalDateTime(){
		return flights[flights.length - 1].arrivalDate;
	}
	
	function getFirstDepartureDateStr(){
		return flights[0].departureDateStr;
	}
	
	function getFirstDepartureDateValue(){
		return flights[0].departureDateValue;
	}
	
	function getLastArrivalDateStr(){
		return flights[flights.length - 1].arrivalDateStr;
	}
	
	function getLastArrivalDateValue(){
		return flights[flights.length - 1].arrivalDateValue;
	}
	
	
	function getFirstDepartureTimeOnly(){
		return flights[0].departureTime;
	}
	
	function getLastArrivalTimeOnly(){
		return flights[flights.length - 1].arrivalTime;
	}
	
	this.setModifiedResSegList = function(resSegList){
		this.modifiedResSegList = resSegList;
	}
	
	this.getModifiedResSegList = function(){
		return this.modifiedResSegList;
	}
	
	function getOrignNDestFullStr(){
		var fullSegCode;
		/*if(flights.length > 1){
			fullSegCode = flights[0].segmentCodeFull.split("/")[0]+"/"+ flights[flights.length - 1].segmentCodeFull.split("/")[1];
		} else {
			fullSegCode = flights[0].segmentCodeFull
		}*/
		for(var i = 0 ; i < flights.length; i++){
			if(i > 0 ){
				fullSegCode = fullSegCode+ '<br />' +flights[i].segmentCodeFull;
			} else {
				fullSegCode = flights[i].segmentCodeFull;
			}
		}
		return fullSegCode;		
	}
	
	function getNumberOfStops(){
		var stops = "";
		stops = UI_AvailabilitySearch.stopovers[flights.length-1] + " " + UI_AvailabilitySearch.labels.shortover;
		return stops;		
	}
	
	function getFlightNumbers(){
		var fltNos = '';
		for(var i = 0 ; i < flights.length; i++){
			if(fltNos.length > 0 ){
				fltNos = fltNos+ '<br />' +flights[i].flightNo;
			} else {
				fltNos = flights[i].flightNo;
			}
		}
		return fltNos;
	}
	
	// Sun, 05 Aug 12
	function ddFmt(dst){
        $.datepicker.setDefaults( $.datepicker.regional[ (SYS_IBECommonParam.locale==="ar" || SYS_IBECommonParam.locale==="fa")?"en":SYS_IBECommonParam.locale ] );
		return $.datepicker.formatDate('D, dd M y', decodeJsonDate(dst));
	}
	
	// HH:MM
	function dtFmt(dst){
		var date = decodeJsonDate(dst);
		return leadingZero(date.getHours())+":" +leadingZero(date.getMinutes());			
	}
	
	function getFlightDateTimes(flg){
        //for persian conversion
        var changeDateWithLocal = function(date){
            var rte = $.datepicker.formatDate('ddMy', decodeJsonDate(date));
            if(SYS_IBECommonParam.locale===''){
                rte = dateFromGregorian(date,SYS_IBECommonParam.locale)
            }
            return rte;
        }
		var fltDT = '';
		for(var i = 0 ; i < flights.length; i++){
			if(i > 0 ){
				fltDT = fltDT+ '<br />';
				if(flg.toLowerCase()=='deparature'){
					fltDT+=changeDateWithLocal(flights[i].departureDate);
					fltDT+= " " + dtFmt(flights[i].departureDate);
				}else{
					fltDT+=changeDateWithLocal(flights[i].arrivalDate);
					fltDT+= " " + dtFmt(flights[i].arrivalDate);
				}
			} else {
				if(flg.toLowerCase()=='deparature'){
					fltDT=changeDateWithLocal(flights[i].departureDate);
					fltDT+= " " + dtFmt(flights[i].departureDate);
				}else{
					fltDT=changeDateWithLocal(flights[i].arrivalDate);
					fltDT+= " " + dtFmt(flights[i].arrivalDate);
				}
			}
		}
		return fltDT;
	}
	
	this.getSegmentCodeFull = function(){
		return segmentCodeFull;
	};
	
	this.getBookingType = function(){
		return bookingType;
	};
	
	this.getStatus = function(){
		return status;
	};
	
	this.getOrigin = function() {
		return origin;
	};

	this.getDestination = function() {
		return destination;
	};
	
	this.getBookingClass = function() {
		return bookingClass;
	}

	this.getCabinClass = function() {
		return cabinClass;
	}

	this.getLastFareQuoteDate = function() {
		return lastFareQuoteDate;
	}
	
	this.getLogicalCabinClass = function() {
		return logicalCabinClass;
	}

	this.getTicketValidTill = function() {
		return ticketValidTill;
	}
	
	this.setOldPerPaxFareTOList = function(oldPerPaxFareTOList) {
		this.oldPerPaxFareTOList = oldPerPaxFareTOList;
	}

	this.getOldPerPaxFareTOList = function() {
		return this.oldPerPaxFareTOList;
	}
	
	this.setDateChangedResSegList = function(resSegList){
		this.dateChangedResSegList = resSegList;
	}
	
	this.getDateChangedResSegList = function(){
		return this.dateChangedResSegList;
	}
	
	this.setSameFlightModification = function(sameFltModification){
		this.sameFlightModification = sameFltModification;
	}
	
	this.getSameFlightModification = function(){
		return this.sameFlightModification;
	}
	
	this.setEligibleToSameBCMod = function(eligibility) {
		this.eligibleToSameBCMod = eligibility;
	}

	this.getEligibleToSameBCMod = function() {
		return this.eligibleToSameBCMod;
	}
	
	this.getbundledServicePeriodId = function(){
		return bundledServicePeriodId;
	}
	
	this.getBusSegment = function(){
		return busSegment;
	}
	
	this.getGroundStationPnrSegmentID = function(){
		return groundStationPnrSegmentID;
	}	
	
	this.getFlightRPHList = function() {
		var list = [];
		for ( var i = 0; i < flights.length; i++) {
			list[list.length] = flights[i].segmentRPH;
		}
		return list;
	};

	this.isFlownOnd = function() {
		for ( var i = 0; i < flights.length; i++) {
			if (flights[i].flownSegment) {
				return true;
			}
		}
		return false;
	};

	this.getExistingFlightRPHList = function() {
		var list = [];
		if (status == ondStatus.CNF) {
			for ( var i = 0; i < flights.length; i++) {
				list[list.length] = flights[i].segmentRPH;
			}
		}
		return list;
	};

	this.getResSegRPHList = function() {
		var list = [];
		for ( var i = 0; i < flights.length; i++) {
			flights
			list[list.length]  = buildResSegRPH(flights[i]);;
		}
		return list;
	};
	
	function buildResSegRPH(flightObj){
	    var rph = flightObj.segmentRPH;
	    var arr = rph.split('$');
	    arr[2] = flightObj.resSegmentRPH;
	    return arr.join('$');
	}

	this.getAllOpenSegmentsForOHDRelCal = function() {
		var list = [];
		for ( var i = 0; i < flights.length; i++) {
			list[list.length] = {
			    flightNumber : flights[i].flightNo,
			    departureDateTimeZulu : flights[i].departureDateTimeZulu,
			    operatingAirline : flights[i].carrierCode,
			    segmentCode : flights[i].segmentCode
			}
		}
		return list;
	}
	
	this.getDisplayOnd = function(){
		
		// Sun, 05 Aug 12
		function ddFmt(dst){
            $.datepicker.setDefaults( $.datepicker.regional[ (SYS_IBECommonParam.locale==="ar" || SYS_IBECommonParam.locale==="fa")?"en":SYS_IBECommonParam.locale ] );
			return $.datepicker.formatDate('D, dd M y', decodeJsonDate(dst));
		}
		
		// HH:MM
		function dtFmt(dst){
			var date = decodeJsonDate(dst);
			return leadingZero(date.getHours())+":" +leadingZero(date.getMinutes());			
		}
		
		var depDateFrmt; 
		var arrDateFrmt;
		var depTimeFrmt; 
		var arrTimeFrmt;
		var dateDepVal;
		var dateArrVal;
		
		if(status == ondStatus.NEW){
			depDateFrmt = getFirstDepartureDateStr();
			arrDateFrmt = getLastArrivalDateStr();
			depTimeFrmt = getFirstDepartureTimeOnly();
			arrTimeFrmt = getLastArrivalTimeOnly();
			dateDepVal = getFirstDepartureDateValue();
			dateArrVal = getLastArrivalDateValue();
		}else{
			depDateFrmt = ddFmt(getFirstDepartureDateTime());
			arrDateFrmt = ddFmt(getLastArrivalDateTime());
			depTimeFrmt = dtFmt(getFirstDepartureDateTime());
			arrTimeFrmt = dtFmt(getLastArrivalDateTime());
			dateDepVal = getFirstDepartureDateValue();
			dateArrVal = getLastArrivalDateValue();
		}
		
		return {
			origin: origin,
			destination: destination,
			orignNDest: getOrignNDestFullStr(),
			departureDate: depDateFrmt,
			departureDateValue: dateDepVal,
			departureTime: depTimeFrmt,
			arrivalDate: arrDateFrmt,
			arrivalDateValue: dateArrVal,
			arrivalTime: arrTimeFrmt,
			flightNo: getFlightNumbers(),
			flightNumber: getFlightNumbers(),
			duration:duration,
			status: status,
			flDepartureDateTime: getFlightDateTimes('deparature'),
			flArrivalDateTime: getFlightDateTimes('arrival'),
			stops:getNumberOfStops()
		}
	}
	
	

}

UI_AvailabilitySearch.getFlightSegIdFromRPH = function (flightRPH){	
	if(flightRPH!=undefined && flightRPH!=null 
			&& flightRPH.split("$")[2]!=null){
		return flightRPH.split("$")[2];
	}
	return null;
}	

UI_AvailabilitySearch.getONDSearchDTOList = function() {
	
	var ondList = newONDSegmentList;
	var list = [];
	for ( var i = 0; i < ondList.length; i++) {
		var ond = ondList[i];

		if (ond.getBookingType() == ondStatus.OPENRT) {
			continue;
		}
		var searchOND = {
		    fromAirport : ond.getOrigin(),
		    toAirport : ond.getDestination(),
		    flightRPHList : ond.getFlightRPHList(),
		    status : ond.getStatus(),
		    flownOnd : ond.isFlownOnd(),
		    existingFlightRPHList : getExistingFlightRPHList(ond),
		    existingResSegRPHList : getExistingResSegRPHList(ond),
		    departureDate : ond.getFirstDepartureDate(),
		    classOfService : ond.getCabinClass(),
		    bookingType : ond.getBookingType(),
		    bookingClass : ond.getBookingClass(),
		    logicalCabinClass: ond.getLogicalCabinClass(),
		    oldPerPaxFareTOList : ond.getOldPerPaxFareTOList(),
		    dateChangedResSegList:ond.getDateChangedResSegList(),
		    modifiedResSegList: ond.getModifiedResSegList(),
		    sameFlightModification:ond.getSameFlightModification(),
		    eligibleToSameBCMod : ond.getEligibleToSameBCMod(),
		    bundledServicePeriodId : ond.getBundledServicePeriodId(),
		    busSegment : ond.getBusSegment(),
		    groundStationPnrSegmentID : ond.getGroundStationPnrSegmentID()
		};
		list[list.length] = searchOND;
	}
	return list;
}

function getExistingResSegRPHList(ondInfo) {

	function firstContainAll(first, second) {
		var count = 0;
		for ( var i = 0; i < first.length; i++) {
			var found = false;
			for ( var j = 0; j < second.length; j++) {
				if (second[j] == first[i]) {
					found = true;
					count++;
					break;
				}
			}
			if (found && count == second.length) {
				return true;
			}
		}
		return false;
	}

	if (ondInfo.getStatus() == 'NEW') {
		var rphList = ondInfo.getFlightRPHList();
		for ( var i = 0; i < removedOndList.length; i++) {
			var existnRPHs = removedOndList[i].getFlightRPHList();
			if (firstContainAll(existnRPHs, rphList)) {
				return removedOndList[i].getResSegRPHList();
			}
		}
	} else if (ondInfo.getStatus() == 'CNF') {
		return ondInfo.getResSegRPHList();
	}
	return [];
}

function getONDFlightRPHList(ondFlights){
	var list = [];
	for ( var i = 0; i < ondFlights.length; i++) {
		list[list.length] = ondFlights[i].pnrSegID;
	}
	return list;
}

function getExistingFlightRPHList(ondInfo) {

	function firstContainAll(first, second) {
		var count = 0;
		for ( var i = 0; i < first.length; i++) {
			var found = false;
			for ( var j = 0; j < second.length; j++) {
				if (second[j] == first[i]) {
					found = true;
					count++;
					break;
				}
			}
			if (found && count == second.length) {
				return true;
			}
		}
		return false;
	}

	if (ondInfo.getStatus() == 'NEW') {
		var rphList = ondInfo.getFlightRPHList();
		for ( var i = 0; i < removedOndList.length; i++) {
			var existnRPHs = removedOndList[i].getFlightRPHList();
			if (firstContainAll(existnRPHs, rphList)) {
				return existnRPHs;
			}
		}
	} else if (ondInfo.getStatus() == 'CNF') {
		return ondInfo.getFlightRPHList();
	}
	return [];
}

UI_AvailabilitySearch.setSelectedFlightInfo = function(flightSegList){	
	
	function decodeJsonDate(dstr) {
		var a = dstr.split('T');
		var d = a[0].split('-');
		var t = a[1].split(':');
		// new Date(year, month, day, hours, minutes, seconds,
		// milliseconds);
//		return new Date(d[0], parseInt(d[1], 10) - 1, d[2], t[0], t[1],
//		        t[2]);
        var utcDateMillis = Date.UTC(parseInt(d[0], 10),parseInt(d[1], 10) - 1,parseInt(d[2], 10),parseInt(t[0], 10),parseInt(t[1], 10),parseInt(t[2], 10));
        var now = new Date();
        var date = UI_AvailabilitySearch.nullyFyGMTOffsetAndGetDate(now.getTimezoneOffset(),utcDateMillis);
        return date;

    }
	
	function decodeJsonDateForGetTime(dstr) {
		var a = dstr.split('T');
		var d = a[0].split('-');
		var t = a[1].split(':');
        var utcDateMillis = Date.UTC(parseInt(d[0], 10),parseInt(d[1], 10) - 1,parseInt(d[2], 10),parseInt(t[0], 10),parseInt(t[1], 10),parseInt(t[2], 10));
       
        return new Date(utcDateMillis);           
        
    }

	function decodeJsonDateLocal(dstr) {
		var a = dstr.split('T');
		var d = a[0].split('-');
		var t = a[1].split(':');
		return new Date(d[0], parseInt(d[1], 10) - 1, d[2], t[0], t[1], t[2]);
    }
	
	function localddFmt(dst){
        $.datepicker.setDefaults( $.datepicker.regional[ (SYS_IBECommonParam.locale==="ar" || SYS_IBECommonParam.locale==="fa")?"":SYS_IBECommonParam.locale ] );
		return $.datepicker.formatDate('D, dd M y', decodeJsonDateLocal(dst));
	}
	
	function ddFmt(dst){
        $.datepicker.setDefaults( $.datepicker.regional[ (SYS_IBECommonParam.locale==="ar" || SYS_IBECommonParam.locale==="fa")?"":SYS_IBECommonParam.locale ] );
		return $.datepicker.formatDate('D, dd M y', decodeJsonDate(dst));
	}
	
	function dateFormat(dst){
        $.datepicker.setDefaults( $.datepicker.regional[ (SYS_IBECommonParam.locale==="ar" || SYS_IBECommonParam.locale==="fa")?"":SYS_IBECommonParam.locale ] );
		var date =  $.datepicker.formatDate('D, dd M y', decodeJsonDate(dst));
		var result = "";
		    for(var i = 0; i < date.length; i++){
		        result += "\\u" + ("000" + date[i].charCodeAt(0).toString(16)).substr(-4);
		    }
		return date;
	}
	
	function getTimeOnly(dstr) {
		var a = dstr.split('T');
		var d = a[0].split('-');
		var t = a[1].split(':');
	
		return  t[0]+':'+t[1];
	}
	
	function getDateTimeFromJason(jsdate){
		var a = jsdate.split('T');
		var d = a[0].split('-');
		var t = a[1].split(':');
		return d[0].substr(2,2) +""+ d[1] +""+ d[2] +""+ t[0] +""+ t[1];
	}
	
	function setOndValues(fltSegInfo){
		var arrivalDateStr = "";
		var depDateStr = "";
		
		if(SYS_IBECommonParam.locale != "en") {
			arrivalDateStr = dateFormat(fltSegInfo.arrivalDate);
			depDateStr = dateFormat(fltSegInfo.departureDate);
		} else {
			if((typeof fltSegInfo.arrivalDateStr != 'undefined')){
				arrivalDateStr = fltSegInfo.arrivalDateStr;
				depDateStr = fltSegInfo.departureDate;
			} else {
				arrivalDateStr = ddFmt(fltSegInfo.arrivalDate);
				depDateStr = ddFmt(fltSegInfo.departureDate);
			}			
		}
		
		return {
			arrivalDateValue : (typeof fltSegInfo.arrivalDateValue != 'undefined') ? fltSegInfo.arrivalDateValue : getDateTimeFromJason(fltSegInfo.arrivalDate),
			arrivalDate: (typeof fltSegInfo.arrivalDateStr != 'undefined') ? fltSegInfo.arrivalDateStr : ddFmt(fltSegInfo.arrivalDate),
			arrivalTerminalName: null,
			arrivalTime: (typeof fltSegInfo.arrivalTime != 'undefined') ? fltSegInfo.arrivalTime :getTimeOnly(fltSegInfo.arrivalDate),
			arrivalTimeLong: decodeJsonDate(fltSegInfo.arrivalDate).getTime(),
			arrivalTimeZuluLong: decodeJsonDateForGetTime(fltSegInfo.arrivalDateTimeZulu).getTime(),
			carrierCode: fltSegInfo.carrierCode,
			carrierImagePath: null,
			departureDateValue : (typeof fltSegInfo.departureDateValue != 'undefined') ? fltSegInfo.departureDateValue : getDateTimeFromJason(fltSegInfo.departureDate),
			departureDate: (typeof fltSegInfo.departureTime != 'undefined') ? fltSegInfo.departureDateStr: localddFmt(fltSegInfo.departureDate),
			departureTerminalName: null,
			departureTime: (typeof fltSegInfo.departureTime != 'undefined') ? fltSegInfo.departureTime:getTimeOnly(fltSegInfo.departureDate),
			departureTimeLong: decodeJsonDate(fltSegInfo.departureDate).getTime(),
			departureTimeZuluLong: decodeJsonDateForGetTime(fltSegInfo.departureDateTimeZulu).getTime(),
			domesticFlight: false,
			duration: fltSegInfo.flightDuration,
			flightNumber: fltSegInfo.flightNo,
			flightRefNumber: fltSegInfo.segmentRPH,
			domesticFlight : fltSegInfo.domesticFlight,
			flightSeqNo: '',
			returnFlag: fltSegInfo.isReturnFlight,
			segmentCode: fltSegInfo.segmentCodeFull,
			segmentId: null,
			segmentName: null,
			segmentShortCode: fltSegInfo.segmentCode,
			resSegmentRPH :fltSegInfo.resSegmentRPH,
			pnrSegID : fltSegInfo.resSegmentRPH,
			system: "AA",
			fareTO : fltSegInfo.fareTO};			 
	}
	
	var list= [];
	for(var i = 0 ; i < flightSegList.length; i++){
		var flights = flightSegList[i].getFlightSegment();
		for(var j = 0 ; j < flights.length; j++){
			list[list.length] = setOndValues(flights[j]);
		}		
	}	
	return list;

}

UI_AvailabilitySearch.cancelSegmentRequote = function(){

	var existingCnfSeg = jQuery.parseJSON($("#resOldAllSegments").val());
	var modifyFlightNos = $("#resModifySegmentRefNos").val();
	
	for(var i=0;i < existingCnfSeg.length;i++){
		existingCnfSeg[i].returnFlag = existingCnfSeg[i].returnFlag == "Y"?true:false;
	} 
	
	newONDSegmentList = UI_AvailabilitySearch.getRequoteOnds({existingFltSegList:existingCnfSeg,modFltSegNo:modifyFlightNos});
	
	var displayOnds = [];
	for(var i = 0 ; i < newONDSegmentList.length; i++){
		displayOnds[displayOnds.length] = newONDSegmentList[i].getDisplayOnd();
	}
	
	$("#departueFlight").nextAll().remove();
	$("#departueFlight").iterateTempleteFS({templeteName:"departueFlight", data:displayOnds, dtoName:"displayOnd"});
	$("#flightTemplate").nextAll().remove();
	$("#flightTemplate").iterateTempleteFS({templeteName:"flightTemplate", data:displayOnds, dtoName:"flightSegments"});
	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
	
	if($("#resAdultCount").val() > 0) {
		$("#adultCount").text($("#resAdultCount").val()+ " " + UI_AvailabilitySearch.lblAdult);
	}
	if($("#resChildCount").val() > 0) {
		$("#childCount").text(", "+$("#resChildCount").val()+ " " + UI_AvailabilitySearch.lblChild);
	}
	if($("#resInfantCount").val() > 0) {
		$("#infantCount").text(", "+$("#resInfantCount").val()+ " " + UI_AvailabilitySearch.lblInfant);
	}
	
	$("#tabs1").hide();
	$("#modifyBtnSet").hide();
	$("#btnAvailableFlight").hide();
	$("#btnRequote").hide();		
	$("#tabs2").show();
	UI_AvailabilitySearch.returnFlag = "false";
	$("#resReturnFlag").val("false");
	
	UI_AvailabilitySearch.reCalculateFareRequote();
}

UI_AvailabilitySearch.nameChangeRequote = function(){

	var existingCnfSeg = jQuery.parseJSON($("#resOldAllSegments").val());
	var modifyFlightNos = $("#resModifySegmentRefNos").val();
	
	for(var i=0;i < existingCnfSeg.length;i++){
		existingCnfSeg[i].returnFlag = existingCnfSeg[i].returnFlag == "Y"?true:false;
	} 
	
	newONDSegmentList = UI_AvailabilitySearch.getRequoteOnds({existingFltSegList:existingCnfSeg,modFltSegNo:''});
	
	var displayOnds = [];
	for(var i = 0 ; i < newONDSegmentList.length; i++){
		displayOnds[displayOnds.length] = newONDSegmentList[i].getDisplayOnd();
	}
	
	$("#departueFlight").nextAll().remove();
	$("#departueFlight").iterateTempleteFS({templeteName:"departueFlight", data:displayOnds, dtoName:"displayOnd"});
	$("#flightTemplate").nextAll().remove();
	$("#flightTemplate").iterateTempleteFS({templeteName:"flightTemplate", data:displayOnds, dtoName:"flightSegments"});
	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
	
	if($("#resAdultCount").val() > 0) {
		$("#adultCount").text($("#resAdultCount").val()+ " " + UI_AvailabilitySearch.lblAdult);
	}
	if($("#resChildCount").val() > 0) {
		$("#childCount").text(", "+$("#resChildCount").val()+ " " + UI_AvailabilitySearch.lblChild);
	}
	if($("#resInfantCount").val() > 0) {
		$("#infantCount").text(", "+$("#resInfantCount").val()+ " " + UI_AvailabilitySearch.lblInfant);
	}
	
	$("#tabs1").hide();
	$("#modifyBtnSet").hide();
	$("#btnAvailableFlight").hide();
	$("#btnRequote").hide();		
	$("#tabs2").show();
	UI_AvailabilitySearch.returnFlag = "false";
	$("#resReturnFlag").val("false");
	
	UI_AvailabilitySearch.reCalculateFareRequote();
}

UI_AvailabilitySearch.setFlightRPHList = function(inParams) {
	var list=[];
	var rphList = '';
	flightSegList = inParams.existingFltSegList;
	for(var i = 0 ; i < flightSegList.length; i++){
		var flights = flightSegList[i].getFlightSegment();
		for(var j = 0 ; j < flights.length; j++){			
				list[list.length] = flights[j].segmentRPH;			
				
				if(list.length > 1){
					rphList = rphList+':';
				}
				rphList = rphList+flights[j].segmentRPH
		}		
	}	
	UI_AvailabilitySearch.outFlightRPHList =rphList;	
}

UI_AvailabilitySearch.calCNXAndNameChangeBalSummary = function(){
	UI_commonSystem.loadingProgress();
	
	UI_AvailabilitySearch.setFlightRPHList({existingFltSegList:newONDSegmentList});
	
	var fareQuote = $.airutil.dom.cloneObject(UI_AvailabilitySearch.fareQuote);
	
	UI_AvailabilitySearch.updateSearchParams();	
	
	$('#resFareQuote').val($.toJSON(fareQuote));
	$('#paxJson').val($('#paxJson').val());	
	
	var rphData = UI_AvailabilitySearch.getRPHData();	
	
	var url = '';					
		
	$('#resSelectedFlights').val($.toJSON({flightSegments:UI_AvailabilitySearch.setSelectedFlightInfo(newONDSegmentList),isReturnFlight:false}));
	
	url = 'requoteBalanceSummary.action';

	 var data = createSearchSubmitData();	
	 data  =  $.airutil.dom.concatObjects(data, rphData.data);
	 data['searchParams.ondListString'] = $.toJSON(UI_AvailabilitySearch.getONDSearchDTOList());
	 data['searchParams.ticketValidTillStr'] = UI_AvailabilitySearch.getTicketValidTill();
	 data['searchParams.lastFareQuoteDateStr'] = UI_AvailabilitySearch.getLastFareQuoteDate();
	 
	if(UI_AvailabilitySearch.isCnxSegmentRequote) {
		data['cancelSegment'] = true;
		data['modifySegment'] = false;
	} else {
		data['cancelSegment'] = false;
		data['modifySegment'] = true;
	}
	 
	 data['balanceQueryData'] = $.toJSON(getBalanceQueryData($('#resPNR').val(), false,$("#resVersion").val()));
	 
	 $('#balanceQueryData').val($.toJSON(getBalanceQueryData($('#resPNR').val(), false,$("#resVersion").val()))); 
	 
	 $.ajax({type: "POST", dataType: 'json',data: data , url:url,		
	 success: UI_AvailabilitySearch.showBalcenceSummaryProcess, error:UI_commonSystem.setErrorStatus, cache : false});	
}

UI_AvailabilitySearch.getLastFareQuoteDate = function() {
	if (removedOndList != null && removedOndList.length > 0) {
		return removedOndList[0].getLastFareQuoteDate();
	}
	return null;
}

UI_AvailabilitySearch.getTicketValidTill = function() {
	if (removedOndList != null && removedOndList.length > 0) {
		return removedOndList[0].getTicketValidTill();
	}
	return null;
}

UI_AvailabilitySearch.showHideRequoteData =function(param){
	var isCancel = UI_AvailabilitySearch.isCnxSegmentRequote;
	if(UI_AvailabilitySearch.isRequoteFlightSearch){
		if(isCancel){
			$("#tabs1").hide();
			$("#modifyBtnSet").hide();
			$("#requoteBtnSet").hide();			
		} else {			
			$("#modifyBtnSet").hide();
			$("#requoteBtnSet").show();		
		}
		
		if(param.isFareAvailable) {
			$("#btnRequote").hide();			
			$("#trAccept").show();
			$("#btnContinue").show();
		} else {
			$("#trAccept").hide();
			$("#btnContinue").hide();
		}
	}else{
		$("#tabs2").hide();
		$("#modifyBtnSet").hide();
		$("#requoteBtnSet").hide();	
	}	
}

UI_AvailabilitySearch.validateNewOnd =function(flightSegList) {
	
	function getNewFltSegment(flightSegList){
		var newFltSegList = [];
		for(var i = 0 ; i < flightSegList.length; i++){
			var flights = flightSegList[i].getFlightSegment();
			for(var j = 0 ; j < flights.length; j++){
				if(flights[j].status == 'NEW'){
					newFltSegList[newFltSegList.length] = flights[j];
				}
			}	
			
		}
		return newFltSegList;		
	}
	
	var newFltSegList = getNewFltSegment(flightSegList);	
	
	for(var i = 0 ; i < flightSegList.length; i++){
		var flights = flightSegList[i].getFlightSegment();
		for(var j = 0 ; j < flights.length; j++){			
			if(flights[j].status != 'NEW'){
				for(var n=0;n < newFltSegList.length;n++){
					
					if(newFltSegList[n].arrivalDTZuluMillis <= flights[j].arrivalDTZuluMillis 
							&& newFltSegList[n].departureDTZuluMillis >= flights[j].departureDTZuluMillis){
						return false;
					}	
					
					if(newFltSegList[n].arrivalDTZuluMillis > flights[j].arrivalDTZuluMillis 
							&& newFltSegList[n].departureDTZuluMillis <= flights[j].departureDTZuluMillis){
						return false;
					}
					
				}
			}
		}		
	}
	return true;
}

UI_AvailabilitySearch.ONDWiseFlexiSelectionInAnci =function(departueFlights,arrivalFlights) {
	var selectedFlightOutboundFlexiAvailable =  false;
	var selectedFlightInboundFlexiAvailable =  false;
	if(departueFlights != undefined && departueFlights != null){
		$.each(departueFlights,function(index,departureFlight){
			if(departureFlight.selected){
				$.each(departureFlight.availableFlightInfo, function(flightInfoIndex,flightInfo) {
					if(flightInfo.selected){
						$.each(flightInfo.flightFareSummaryList,function(index,fareSumamry){
							if(fareSumamry.bundledFarePeriodId == null){
								if(!selectedFlightOutboundFlexiAvailable){
									selectedFlightOutboundFlexiAvailable =  fareSumamry.withFlexi;
								}
							}
							
						});
					}

				});
			}
		});
	}
	
	if(arrivalFlights != undefined && arrivalFlights != null){
		$.each(arrivalFlights,function(arrivalFlightIndex,arrivalFlight){
			if(arrivalFlight.selected){
				$.each(arrivalFlight.availableFlightInfo, function(flightInfoIndex,flightInfo) {
					if(flightInfo.selected){
						$.each(flightInfo.flightFareSummaryList,function(index,fareSumamry){
							if(fareSumamry.bundledFarePeriodId == null){
								if(!selectedFlightInboundFlexiAvailable){
									selectedFlightInboundFlexiAvailable =  fareSumamry.withFlexi;
								}
							}
						});
					}				
				});
			}
		});
	}	
	
	//UI_AvailabilitySearch.ondWiseFlexiSelectionInAnciArr = {0:selectedFlightOutboundFlexiAvailable,1:selectedFlightInboundFlexiAvailable};
	//$("#resOndwiseFlexiAvilableForAnci").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelectionInAnciArr));
//	UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr ={0:outboundFlexiCharge,1:inboundFlexiCharge};
//	$("#resOndwiseFlexiChargesForAnci").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr));
}

//TODO:RW remove this
UI_AvailabilitySearch.ONDWiseTotalFlexiChargeInAnci =function(strTotalFlexiChargeInbound,strTotalFlexiChargeOutbound) {
	UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr ={0:strTotalFlexiChargeOutbound,1:strTotalFlexiChargeInbound};
	$("#resOndwiseFlexiChargesForAnci").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexichargeInAnciArr));
}

UI_AvailabilitySearch.isCalendarViewDisabled = function(){
	if(UI_AvailabilitySearch.isRequoteFlightSearch && !isCalanderViewEnabledForRequote){
		return true;
	}
	
	return false;
}

UI_AvailabilitySearch.getNewFlightSegRef = function(){
	var newFlightSegRef = "";
	if(UI_AvailabilitySearch.isCalendarViewDisabled()){
		newFlightSegRef = $("input[type='radio'][name='radSel_0']:checked").val();
		if(newFlightSegRef != undefined && newFlightSegRef != null){
			//replace all occurrences 
			var regex = new RegExp("\\,","g");
			newFlightSegRef = newFlightSegRef.replace(regex, ':');
		}
		UI_AvailabilitySearch.outFlightRPHList = newFlightSegRef;
	} else {		
		newFlightSegRef = $("#fCalOut_FL").find("input[type='radio']:checked").parent().find(".flightRefId").text();	
	}	
	
	return newFlightSegRef;
}

UI_AvailabilitySearch.searchNextPrevDay = function(e){
	var flags = e.target.id.split("_");
	var index = flags[2];
	var flg = flags[1];
	var _ondListStr = [];
	_ondListStr[_ondListStr.length] = UI_mcAvailabilitySearch.ondString[index];
	_ondListStr[0].flightRPHList = [];
	var extDate = UI_mcAvailabilitySearch.ondString[index].depDate;
	var boolSearch = false;
	if (flg.toUpperCase()=='NEXT'){
		_ondListStr[0].depDate = addDaysAsString(extDate, 1);
		_ondListStr[0].departureDate = converAAToJson(addDaysAsString(extDate, 1));
		$('#resDepartureDate').val(_ondListStr[0].depDate);
		boolSearch = true;
	}else{
		if (dateVal(dataformater(UI_Top.holder().strSysDate)).d < dateVal(dataformater(extDate)).d){
			_ondListStr[0].depDate = addDaysAsString(extDate, -1);
			_ondListStr[0].departureDate = converAAToJson(addDaysAsString(extDate, -1));
			$('#resDepartureDate').val(_ondListStr[0].depDate);
			boolSearch = true;
		}
	}
	if (boolSearch){
		$.data($("#flightTempl_"+index), "OnDData",_ondListStr[index]);
		
		UI_AvailabilitySearch.loadAvailabilitySearchData();
	}
}

function setBundleFareImageURL(logicalCC){
	
	if(logicalCC.imageUrl != null){
		
	    var lIndex = logicalCC.imageUrl.lastIndexOf("/"),
	        url = logicalCC.imageUrl.substr(0, (lIndex+1));
	    return url + 'bundledFareStripe_' + logicalCC.bundledFarePeriodId;
	    
	}else{
		
		return "";
		
	}
}

function getBundleFareImageURL(index, bId){
    var wayBundelFares = UI_AvailabilitySearch.availableBundleFareLCClass[index], returnUrl = "";
    $.each(wayBundelFares, function(i,obj){
        if (obj!=undefined && String(obj[0].bundledFarePeriodId) === bId){
            returnUrl = obj[0].imageUrl;
        }
    })
    return returnUrl;
}

/** ************************************* */
/** End of Requote Flow relate methods * */
/** ************************************* */
