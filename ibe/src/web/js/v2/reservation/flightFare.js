function UI_FlightFare(){}

UI_FlightFare.fromAirport = "";
UI_FlightFare.toAirport = "";
UI_FlightFare.depatureDate = "";
UI_FlightFare.returnDate = "";
UI_FlightFare.departureVariance = "";
UI_FlightFare.arrivalVariance = "";
UI_FlightFare.selectedCurrency = "";
UI_FlightFare.returnFlag = "";
UI_FlightFare.currencyCode = UI_Top.holder().GLOBALS.baseCurrency;
UI_FlightFare.depatureStatus = false;
UI_FlightFare.arrivalStatus = false;
UI_FlightFare.msgCanNotReserve = "";
UI_FlightFare.errorInfo = "";
UI_FlightFare.blnOFlt = false;
UI_FlightFare.blnRFlt = false;
UI_FlightFare.msgWithoutOutbound = "";
UI_FlightFare.msgWithoutInbound = "";
UI_FlightFare.faresNotAvailable = "";
UI_FlightFare.flightType = "";
UI_FlightFare.lblAdult = "";
UI_FlightFare.lblChild = "";
UI_FlightFare.lblInfant = "";
UI_FlightFare.fareQuote = null;

// Hold Departure and Return Flight data
UI_FlightFare.depatureFlights = null;
UI_FlightFare.returnFlights = null;


var dtODate 		= null;
var dtRDate 		= null;
var strODate 		= "";
var strRDate 		= "";

$(document).ready(function(){	
	UI_FlightFare.ready();
});
/**
 * Page Ready 
 */
UI_FlightFare.ready = function(){
	$("#divLoadBg").hide();
	//Buttons
	$("#btnSOver").decorateButton();	
	$("#btnContinue").decorateButton();	
	
	$("#btnSOver").click(function(){SYS_IBECommonParam.homeClick();});	
	$("#btnContinue").click(function(){UI_commonSystem.pageOnChange();UI_FlightFare.loadPaxPage();});
	
	$("#trBookingSFlightPanel").hide();	
	$("#trPaymentSPanel").hide();
	$("label[name='lblNoFlightSelect']").show();
	$("#linkTerms").click(function(){UI_FlightFare.linkTermsClick();});
	
	// Next Previous Departure links 
	$("#linkPreviousOutgoing").click(function(){UI_FlightFare.changeDates("O", -1);});
	$("#linkNextOutgoing").click(function(){UI_FlightFare.changeDates("O", 1);});
	$("#linkCenterPreviousOutgoing").click(function(){UI_FlightFare.changeDates("O", -1);});
	$("#linkCenterNextOutgoing").click(function(){UI_FlightFare.changeDates("O", 1);});
	// Next Previous Return links 
	$("#linkPreviousReturn").click(function(){UI_FlightFare.changeDates("R", -1);});
	$("#linkNextReturn").click(function(){UI_FlightFare.changeDates("R", 1);});
	$("#linkCenterPreviousReturn").click(function(){UI_FlightFare.changeDates("R", -1);});
	$("#linkCenterNextReturn").click(function(){UI_FlightFare.changeDates("R", 1);});
	
	$("#btnReCalculate").click(function(){UI_FlightFare.reCalculateFare();});
	
	UI_FlightFare.setPageAirLineParameters();
	UI_FlightFare.setAirlineParameters();
	UI_FlightFare.pageInit();
	UI_FlightFare.setDates();
	// To Do: Review
	UI_FlightFare.getFlygingDates();
	UI_FlightFare.loadFareData();	
}


/**
 * Recalculate fare
 */
UI_FlightFare.reCalculateFare = function() {
	
	if (UI_FlightFare.clientValidate()) {
		 var data = {};
		 var flightData = "";
		 var segmentData = "";
		 var outFlightRPHList = "";
		 var retFlightRPHList = "";
		 if (UI_FlightFare.depatureStatus == true ) {
			 flightData = ($("input[id*='radOut']:checked").val()).split("-");
			 segmentData = (flightData[0]).split(",");
			 for(var i = 0; i < segmentData.length; i++) {
				 data["outFlightRPHList[" + i + "]"] = segmentData[i];
			 }	 
			
		 }
		 if (UI_FlightFare.arrivalStatus == true && blnOnewayDeparture == false) {
			 flightData = ($("input[id*='radIn']:checked").val()).split("-");
			 segmentData = flightData[0].split(",");
			 for(var i = 0; i < segmentData.length; i++) {
				 data["retFlightRPHList[" + i + "]"] = segmentData[i];
			 }				
		 }
		 $("#resSearchSystem").val(flightData[1]);
		 UI_commonSystem.loadingProgress();
		 $("#frmFare").attr('action', 'loadFareQuote.action');
		 $("#frmFare").ajaxSubmit({data:data, dataType: 'json', success: UI_FlightFare.reCalculateFareProcess});	 
		 return false;
	 }
}

/**
 * Process Recalculate Fare
 */
UI_FlightFare.reCalculateFareProcess = function(response){	
	if(response != null && response.success == true) {
		if(response.fareQuote!=null){
			UI_FlightFare.setFare(response.fareQuote);
			UI_FlightFare.setTotalFare(response.fareQuote.totalPrice);	
		}else{
			jAlert(UI_FlightFare.faresNotAvailable)
		}
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

/**
 * Set Dates Departure/Arrival
 */
UI_FlightFare.setDates = function() {
	dtODate = new Date(UI_FlightFare.depatureDate.substr(6,4), (Number(UI_FlightFare.depatureDate.substr(3,2)) - 1), UI_FlightFare.depatureDate.substr(0,2));
	if (UI_FlightFare.returnDate != ""){
		dtRDate = new Date(UI_FlightFare.returnDate.substr(6,4), (Number(UI_FlightFare.returnDate.substr(3,2)) - 1), UI_FlightFare.returnDate.substr(0,2));
	}
}

/**
 * Get Flying Dates
 *  
 */
UI_FlightFare.getFlygingDates =  function() {
	var strM = dtODate.getMonth() + 1;
	var strD = dtODate.getDate();
	if (strM < 10){strM = "0" + strM}
	if (strD < 10){strD = "0" + strD}
	strODate  = strD + "/" + strM + "/" + dtODate.getFullYear();
	
	if (dtRDate != null){
		strM = dtRDate.getMonth() + 1;
		strD = dtRDate.getDate();
		if (strM < 10){strM = "0" + strM}
		if (strD < 10){strD = "0" + strD}
		strRDate  = strD + "/" + strM + "/" + dtRDate.getFullYear();
	}
}

/**
 * Next/ Previous change date
 */
UI_FlightFare.changeDates = function(strType, intDays){
		switch (strType){
			case "O" : dtODate = addDays(dtODate, intDays); strSelectedOut = ""; break ;
			case "R" : dtRDate = addDays(dtRDate, intDays); strSelectedRet = ""; break ;			
		}
		UI_FlightFare.writeRouteDes();
		var blnTrue = true;
		
		switch (strType){
			case "O" : 
				if (!CheckDates(UI_Top.holder().strSysDate, strODate)){
					alert(raiseError("ERR043", UI_Top.holder().strSysDate + "."));
					//alert("From date cannot be less than " + top[0].strSysDate + ".");
					dtODate = addDays(dtODate, 1);
					blnTrue = false;
				}	
				break;
			case "R" :
				if (!CheckDates(UI_Top.holder().strSysDate, strRDate)){
					alert(raiseError("ERR044", UI_Top.holder().strSysDate + "."));
					//alert("Return date cannot be less than " + top[0].strSysDate + ".");
					dtRDate = addDays(dtRDate, 1);
					blnTrue = false;
				}	
				break;			
		}
		if (!blnTrue){
			UI_FlightFare.writeRouteDes();
		}else{			
			switch (strType){
				case "O" : $("#resDepartureDate").val(strODate);
						  // $("#resReturnDate").val(""); 
						   UI_FlightFare.flightType = "O";
						   // arrOFlt.length = 0;
						   $("#blnReturn").val(false);
						   break;
				case "R" : $("#resReturnDate").val(strRDate); 
				 		   UI_FlightFare.flightType = "R";
						  // arrRFlt.length = 0;
						   $("#blnReturn").val(true);
						   break;			
			}
			$("#blnNextPrevious").val(true);
			switch (strType){
				case "O" : UI_FlightFare.showSearchedDate("dateOB",dateConvertToMonthDD(strODate)); break;
				case "R" : UI_FlightFare.showSearchedDate("dateIB",dateConvertToMonthDD(strRDate)); break;
			} 
			
			UI_FlightFare.loadNextPreviousFlightData();			
		}
}

/**
 * Show Search Date
 */
UI_FlightFare.showSearchedDate = function(obj, strDate){
	$("#"+obj).text(strDate);
}


/**
 * 
 */
UI_FlightFare.writeRouteDes =  function(){
	UI_FlightFare.getFlygingDates();	
}

/**
 * Load Next/Previous Flight Data
 */
UI_FlightFare.loadNextPreviousFlightData = function(params) {	 
	 UI_commonSystem.loadingProgress();
	 $("#frmFare").attr('action', 'nextPreviousFlightSearch.action');
	 $("#frmFare").ajaxSubmit({dataType: 'json',success: UI_FlightFare.loadNextPreviousFlightDataProcess});	 
	 return false;
}

/**
 * Process Next/Previous data
 */ 
UI_FlightFare.loadNextPreviousFlightDataProcess = function(response){	
	if(response != null && response.success == true) {
		if ( UI_FlightFare.flightType == "O") {
			UI_FlightFare.loadFlightDepartureData(response.departueFlights);
		} else if ( UI_FlightFare.flightType == "R") {
			UI_FlightFare.loadFlightArrivalData(response.arrivalFlights);
		}
		UI_FlightFare.setFare(response.fareQuote);
		UI_FlightFare.setTotalFare(response.fareQuote.totalPrice);		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}



/**
 * Page Setup
 */
UI_FlightFare.pageInit = function() {
	$("#outDesc").text(UI_FlightFare.fromAirport + " - " + UI_FlightFare.toAirport);
	$("#dateOB").text(dateConvertToMonthDD(UI_FlightFare.depatureDate));
	if (UI_FlightFare.returnFlag == "true") {
		$("#inDesc").text(UI_FlightFare.toAirport + " - " + UI_FlightFare.fromAirport);
		$("#dateIB").text(dateConvertToMonthDD(UI_FlightFare.returnDate));
	} else {
		$("#tableReturnPanel").hide();
	}
	$("#lblSelectFlightMsg").hide();
	$("#tableOB").hide();
	$("#tableIB").hide();
	$("#trLocalTimeAirportMsgOB").hide();
	$("#tableOBNoFlight").hide();
	$("#tableIBNoFlight").hide();
	$("#trPriceBDPannel").hide();
	$("#trTermsNCond").hide();
	$("#trAccept").hide();
	
}

/**
 *  Display page panels
 */
UI_FlightFare.dispalyPanels = function(params) {
	$("#lblSelectFlightMsg").show();
	if (params.type == 'O') {		
		$("#tableOBNoFlight").hide();
		$("#trLocalTimeAirportMsgOB").show();
		$("#tableOB").show();
	} else if (params.type == 'R')	{
		$("#tableIBNoFlight").hide();		
		$("#trLocalTimeAirportMsgIB").show();
		$("#tableIB").show();
	}	
	$("#trTermsNCond").show();
	$("#trAccept").show();
}

/**
 * Set AirLine parameters
 */

UI_FlightFare.setAirlineParameters =  function () {
	UI_FlightFare.fromAirport =  $("#resFromAirport").val();
	UI_FlightFare.toAirport =  $("#resToAirport").val();
	UI_FlightFare.depatureDate =  $("#resDepartureDate").val();
	UI_FlightFare.returnDate =  $("#resReturnDate").val();
	UI_FlightFare.selectedCurrency =  $("#resSelectedCurrency").val();
	UI_FlightFare.returnFlag = $("#resReturnFlag").val();
}

/**
 * Set Air line parameters
 */
UI_FlightFare.setPageAirLineParameters = function() {
	 var arrParamsAA	= UI_Top.holder().strReqParam.split('^');
	 //$("#local").val(arrParamsAA[0]);
	 if ($.trim($("#resFromAirport").val()) == "") {
		 $("#resFromAirport").val(arrParamsAA[2]);	 
		 $("#resToAirport").val(arrParamsAA[3]);
		 $("#resDepartureDate").val(arrParamsAA[4]);
		 $("#resReturnDate").val(arrParamsAA[5]);
		 $("#resDepartureVariance").val(arrParamsAA[6]);
		 $("#resReturnVariance").val(arrParamsAA[7]);
		 $("#resSelectedCurrency").val(arrParamsAA[8]);
		 $("#resAdultCount").val(arrParamsAA[9]);
		 $("#resChildCount").val(arrParamsAA[10]);
		 $("#resInfantCount").val(arrParamsAA[11]);
		 $("#resFromAirportName").val(arrParamsAA[12]);
		 $("#resToAirportName").val(arrParamsAA[13]);
		 $("#resReturnFlag").val(arrParamsAA[14]);		
		 if (arrParamsAA[17] != undefined && arrParamsAA[17] != "") {			
			 $("#resFareType").val(arrParamsAA[17]);
		 } else {
			 //To Do: Set Fare type 
			 $("#resFareType").val("A");
		 }	 
		 
		 // Cabin Class
		 if (arrParamsAA[15] != undefined && arrParamsAA[15] != "") {			
			 $("#resCabinClassCode").val(arrParamsAA[15]);
		 } else {			 
			 $("#resCabinClassCode").val(UI_Top.holder().GLOBALS.classOfService);
		 }	
		 
	 }
}
UI_FlightFare.loadFareData = function(){	 
	 $("#frmFare").attr('action', 'interlineFare.action');
	 $("#frmFare").ajaxSubmit({dataType: 'json',success: UI_FlightFare.loadFares});			
	return false;
}

UI_FlightFare.loadPaxPage  = function() {
	// TO DO:Need to verify
	/*if(top.GLOBALS.isCreditCardPaymentsEnabled !='true' && top.GLOBALS.isReservationOnholdEnabled != 'true'){
		alert(UI_FlightFare_msgCanNotReserve);
		return;
	}	*/
	
	if (UI_FlightFare.clientValidate()) {
		if (!$("#chkTerms").is(':checked')){
			alert(UI_FlightFare.errorInfo["ERR042"]);
			return false;
		}
		
		// restricted fares validation confirmation
		/*if(strResFaresValidate == true){
			var value = window.confirm(strMsgConfirmRestrict);
				if(!value){
					return;
				}
		} */
		
		$('#resFareQuote').val($.toJSON(UI_FlightFare.fareQuote));
		$('#resSelectedFlights').val($.toJSON({flightSegments:UI_FlightFare.getSelectedFlightSegments(),isReturnFlight:UI_FlightFare.returnFlag}));
		$('#frmFare').attr('action','loadContainer.action');
		$('#frmFare').attr('target','_self');
		$('#frmFare').submit();
	}	
	
}
/**
 * Get Selected Flights
 */
/*UI_FlightFare.getSelectedFlightSegments = function() {
	var flightsOut = "";
	var flightsIn = "";
	var id = "";
	var flightID = "";
	var flights = {};
	if (UI_FlightFare.depatureStatus == true) {
		id = $("input[id*='radOut']:checked").attr("id");
		flightID = Number((id.split("_")[1]).split("S")[0]); // S - Segment
		flightsOut = UI_FlightFare.depatureFlights[flightID];
	}
	
	if (UI_FlightFare.arrivalStatus == true && blnOnewayDeparture == false) {
		id = $("input[id*='radIn']:checked").attr("id");
		flightID = Number((id.split("_")[1]).split("S")[0]);
		flightsIn = UI_FlightFare.returnFlights[flightID];
	}
	
	flights['outBound'] = flightsOut;
	flights['inBound']  = flightsIn;
	
	return flights;
} */

UI_FlightFare.getSelectedFlightSegments = function(){
	var getSelectedFlight = function(flightSegts,direction){
		var selected = [];
		for(var i = 0 ; i < flightSegts.length ; i ++ ){
			if(flightSegts[i].selected){
				selected = selected.concat(flightSegts[i].segments);
			}
		}
		for(var i = 0 ; i < selected.length ; i++ ){
			selected[i].direction = direction;
		}
		return selected;
	}
	var selected = getSelectedFlight(UI_FlightFare.depatureFlights,'out');
	selected = selected.concat(getSelectedFlight(UI_FlightFare.returnFlights,'in'));
	return selected;
}

/**
 * Client Validate
 */
UI_FlightFare.clientValidate = function() {	
	
	var msDepature = ""
	var msArrival = ""	
	
	var radIn  = "radIn";
	var radOut = "radOut";
	
	blnOnewayDeparture = false;
	blnOnewayRetrun = false;	
	if(UI_FlightFare.blnShowOneWay != "true") {
		if (!UI_FlightFare.blnOFlt){
			alert(UI_FlightFare.errorInfo["ERR010"]);
			return false;
		}
		
		if ($("input[id*='radOut']:checked").length == 0){
			alert(UI_FlightFare.errorInfo["ERR015"]);
			return false;
		}
	}		
	
	if($("input[id*='radOut']:checked").length == 0 && $("input[id*='radIn']:checked").length == 0){
		alert("Please select out bound or in bound flight");
		return false;
	}	
	
	if(!UI_FlightFare.blnOFlt || $("input[id*='radOut']:checked").length == 0) {
		if (confirm(UI_FlightFare.msgWithoutOutbound)){	
			var id = $("input[id*='radIn']:checked").attr("id");
			var departureFlightID = Number((id.split("_")[1]).split("S")[0]);
			
			if (UI_FlightFare.returnFlights != null && UI_FlightFare.returnFlights == "") {
				msArrival = UI_FlightFare.returnFlights[departureFlightID].segments[0].depatureTimeLong;
			}			
			blnOnewayRetrun = true;			
		}else {
			return false;
		}
	} else {
		var id = $("input[id*='radOut']:checked").attr("id");
		var departureFlightID = Number((id.split("_")[1]).split("S")[0]);
		
		if (UI_FlightFare.depatureFlights != null && UI_FlightFare.depatureFlights == "") {
			msDepature = UI_FlightFare.depatureFlights[departureFlightID].segments[UI_FlightFare.depatureFlights[departureFlightID].segments.length - 1].arrivalTimeLong;
		}			
	}	
	if (UI_FlightFare.returnFlag == "true"){
		if(UI_FlightFare.blnShowOneWay != "true") {
			if (!UI_FlightFare.blnRFlt){
				alert(UI_FlightFare.errorInfo["ERR012"]);
				return false;
			}
			
			if ($("input[id*='radIn']:checked").length == 0){
				alert(UI_FlightFare.errorInfo["ERR016"]);
				return false;
			}
		}
		
		if (!UI_FlightFare.blnRFlt || $("input[id*='radIn']:checked").length == 0){
			
			if (confirm(UI_FlightFare.msgWithoutInbound)){
				blnOnewayDeparture = true;
			}else {
				return false;
			}				
		} else if(!blnOnewayRetrun){
			// check the date range check 
			if (!CheckDates(strODate, strRDate)){
				alert(UI_FlightFare.errorInfo["ERR004"]);
				return false;
			}			
			
			var id = $("input[id*='radIn']:checked").attr("id");
			var departureFlightID = Number((id.split("_")[1]).split("S")[0]);
			
			if (UI_FlightFare.returnFlights != null && UI_FlightFare.returnFlights == "") {
				msArrival = UI_FlightFare.returnFlights[departureFlightID].segments[0].depatureTimeLong;
			}
			
			if (msArrival > msDepature){
				alert(arrError["ERR040"]);
				return false;
			}			
			
		}		
	}	
	return true;
}

UI_FlightFare.loadErrorPage = function(response){	
	window.location.href= SYS_IBECommonParam.nonSecurePath + "showLoadPage!loadError.action?messageTxt="+response.messageTxt;
}

UI_FlightFare.loadFares = function(response){		
	if(response != null && response.success == true) {	
		
		UI_FlightFare.fareQuote = response.fareQuote;
		UI_FlightFare.outboundFlights = response.departueFlights;
		UI_FlightFare.inboundFlights = response.arrivalFlights;
		
		$("#divLoadBg").slideDown("slow");
//		$('body').append('<div id="divSessContainer"></div>');
//		UI_commonSystem.initSessionTimeout('divSessContainer',response.timeoutDTO,function(){});
		$('input').change(function (){UI_commonSystem.pageOnChange();});
		$("#PgFares").populateLanguage({messageList:response.jsonLabel});
		UI_FlightFare.errorInfo = response.errorInfo;
		UI_FlightFare.blnShowOneWay = response.blnShowOneWay;
		UI_FlightFare.msgCanNotReserve = response.jsonLabel.msgCanNotReserve;
		UI_FlightFare.msgWithoutOutbound = response.jsonLabel.msgWithoutOutbound;
		UI_FlightFare.msgWithoutInbound = response.jsonLabel.msgWithoutInbound;
		UI_FlightFare.faresNotAvailable = response.jsonLabel.faresNotAvailable;
		UI_FlightFare.lblAdult = response.jsonLabel.lblAdult;
		UI_FlightFare.lblChild = response.jsonLabel.lblChild;
		UI_FlightFare.lblInfant = response.jsonLabel.lblInfant;
		UI_FlightFare.loadFlightDepartureData(response.departueFlights);
		UI_FlightFare.loadFlightArrivalData(response.arrivalFlights);
		UI_FlightFare.updateCurrencyMessage();
		UI_FlightFare.setFare(response.fareQuote);
		UI_FlightFare.setTotalFare(response.fareQuote.totalPrice);
		$("#termsNCond").html(response.termsNCond);
		// Set Search Parameters
		$("#resSearchSystem").val(response.searchSystem);
	} else {
		UI_FlightFare.loadErrorPage(response);
	}
	UI_commonSystem.loadingCompleted();
	
}

/**
 * Set Total Fare
 */
UI_FlightFare.setTotalFare = function(totalPrice) {
	$("#totalAmount").text(totalPrice + " " + UI_FlightFare.currencyCode);
}
/**
 * Update currency message
 * 
 */
UI_FlightFare.updateCurrencyMessage = function() {
	$("#lblCurrencySupportMessage").text();
	$("#lblCurrencySupportMessage").text($("#lblCurrencySupportMessage").text().replace("{0}", UI_FlightFare.currencyCode));
}

/**
 * Set Fare
 */

UI_FlightFare.setFare = function(fareQuote) {
	if (fareQuote != null ) {
		UI_FlightFare.fareQuote = fareQuote;
		if (UI_FlightFare.checkFareStatus() == true) {
			if (fareQuote.segmentFare != null) {	
				//UI_FlightFare.reSetPaxWiseData(fareQuote.paxWise);
				$("#priceBreakDownTemplate").nextAll().remove();				
				$("#priceBreakDownTemplate").iterateTempletePB({templeteName:"priceBreakDownTemplate", data:fareQuote.segmentFare, dtoName:"paxWise"});	
				//$("#priceBreakDownTemplate").iterateTemplete({templeteName:"priceBreakDownTemplate", data:fareQuote.paxWise, dtoName:"paxWise"});			
			} 
			$("#trPriceBDPannel").show();						
		} else {
			$("#trPriceBDPannel").hide();
		}
	} else {
		$("#trPriceBDPannel").hide();
	}
}

UI_FlightFare.checkFareStatus = function() {
	var fareDisplay = false;		
	if (UI_FlightFare.depatureStatus == true && UI_FlightFare.returnDate == "" ) {				
		if ($("input[id*='radOut']:checked").is(":checked")) {
			fareDisplay = true;
		}	else {
			fareDisplay = false;
		}
		
	}
	if (UI_FlightFare.depatureStatus == true && UI_FlightFare.arrivalStatus == true) {
		if ($("input[id*='radOut']:checked").is(":checked") && $("input[id*='radIn']:checked").is(":checked")) {
			fareDisplay = true;
		} else {
			fareDisplay = false;
		}			
	}
	
	if (fareDisplay == true) {
		$("#btnReCalculate").hide();
	} else {
		$("#btnReCalculate").show();
	}
	
	return fareDisplay;
}


UI_FlightFare.getPaxType = function(params) {
	switch(params.paxCode) {
		case "AD": return UI_FlightFare.lblAdult;break;
		case "CH": return UI_FlightFare.lblChild;break;
		case "IN": return UI_FlightFare.lblInfant;break;		
	}
}
/**
 * Reset Paxwise data
 */

UI_FlightFare.reSetPaxWiseData = function(paxWise) {	
	var tempPax = "";
	for (var i = 0; i < paxWise.length; i++) {
		tempPax = paxWise[i];		
		tempPax.paxType = UI_FlightFare.getPaxType({paxCode:tempPax.paxType});		
		
		if (tempPax.fare) {
			tempPax.fare = tempPax.fare + " "+ UI_FlightFare.currencyCode;
		}
		if (tempPax.tax) {
			tempPax.tax = tempPax.tax + " " + UI_FlightFare.currencyCode;
		}
		if (tempPax.total) {
			tempPax.total = tempPax.total + " " +UI_FlightFare.currencyCode;
		}
		delete paxWise[i];
		paxWise[i] = tempPax;
	}
	return paxWise;
}
/**
 * Set Departure Flight Data 
 */
UI_FlightFare.loadFlightDepartureData = function(depatureFlight) {
	UI_FlightFare.depatureFlights = depatureFlight;
	if (depatureFlight.length > 0) {		
		$("#departueFlightTemplate").nextAll().remove();
		$("#departueFlightTemplate").iterateTempleteAS({templeteName:"departueFlightTemplate", data:depatureFlight, dtoName:"segments"});		
		UI_FlightFare.depatureStatus = true;
		UI_FlightFare.blnOFlt = true;		
		UI_FlightFare.dispalyPanels({type:"O"});		
	} else {
		UI_FlightFare.depatureStatus = false;
		UI_FlightFare.blnOFlt = false;	
		$("#trLocalTimeAirportMsgOB").hide();
	}
}

/**
 * Set Arrival Flight Data 
 */
UI_FlightFare.loadFlightArrivalData = function(flights) {
	UI_FlightFare.returnFlights = flights; 
	if (flights.length > 0) {
		$("#arrivalFlightTemplate").nextAll().remove();
		$("#arrivalFlightTemplate").iterateTempleteAS({templeteName:"arrivalFlightTemplate", data:flights, dtoName:"segments"});
		
		UI_FlightFare.arrivalStatus = true;
		UI_FlightFare.blnRFlt = true;
		UI_FlightFare.dispalyPanels({type:"R"});		
	} else {
		UI_FlightFare.arrivalStatus = false;
		UI_FlightFare.blnRFlt = false;
		$("#trLocalTimeAirportMsgIB").hide();
	}
}

/**
 * Link terms Click
 */
//To Do: Implement new flow
UI_FlightFare.linkTermsClick = function() {
	var url = SYS_IBECommonParam.nonSecurePath + "showTermsNCond.action";	
	var intHeight = (window.screen.height - 200);
	var intWidth = 795;
	var intX = ((window.screen.height - intHeight) / 2);
	var intY = ((window.screen.width - intWidth) / 2);
	var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
	if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)){UI_Top.holder().objCWindow.close();}
	UI_Top.holder().objCWindow = window.open(url,'ToolChild',strProp)
}


/**
 * Flight Search Template
 */
$.fn.iterateTempleteAS = function(params) {

	var templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;
    var selected = false;
    var flightRefNumbers = "";
    var tempID = "";
	var dtoRecord ;	
	for (var f = 0; f < params.data.length; f++) {
		// Get Flight Selected status
		selected = params.data[f].selected;
		for(var i=0; i < params.data[f].segments.length ; i++) {			
			clonedTemplateId = templeteName + "_" + f + "" + i;			
			var clone = $( "#" + templeteName + "").clone();			
			clone.attr("id", clonedTemplateId );			
			clone.appendTo($("#" + templeteName).parent());			
			dtoRecord =  params.data[f].segments[i];			
			elementIdAppend = params.dtoName + "[" + i + "].";
			
			if (i == 0) {
				flightRefNumbers = dtoRecord.flightRefNumber;
				$("#"+clonedTemplateId + " td:first").attr("rowspan", params.data[f].segments.length);
				$("#"+clonedTemplateId + " td:first").find("input").each(function( intIndex ){						
					$(this).val("");					
					$(this).attr("id", this.id  + "_" + f + "S" + i);
					tempID = this.id;
					$(this).click(function(){UI_FlightFare.checkClick(this);});	
				});
				if (selected == true) {					
					$("#"+clonedTemplateId + " td:first").find("input").each(function( intIndex ){					
						$(this).attr("checked", true);												
					});
				} 
			} else {
				flightRefNumbers = flightRefNumbers + "," + dtoRecord.flightRefNumber;				
				$("#"+clonedTemplateId + " td:first").remove();
			}			
			$("#"+clonedTemplateId).find("label").each(function( intIndex ){
				$(this).text(dtoRecord[this.id]);
				$(this).attr("id",  elementIdAppend +  this.id);
			});					
			
			$("#"+clonedTemplateId).find("img").each(function( intIndex ){			
				$(this).attr("src", dtoRecord[this.id]);
				$(this).attr("id",  elementIdAppend +  this.id);
			});
			if (f%2 == 1) {
				$( "#" + clonedTemplateId +" td").addClass("SeperateorBGColor");
			}
			if (i == (params.data[f].segments.length - 1)) {
				$("#" + tempID).val(flightRefNumbers + "-" + dtoRecord.system);
			}
			$( "#" + clonedTemplateId).show();
			
		}
		selected = false;
	}

	$( "#" + templeteName).hide();
}


/**
 * Price Break down Template
 */
// To Do: Merge two template(AS and PB)
$.fn.iterateTempletePB = function(params) {
	var templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;
    var segmentCode = "";   
	var dtoRecord;	
	for (var f = 0; f < params.data.length; f++) {	
		segmentCode = params.data[f].segmentCode;
		UI_FlightFare.reSetPaxWiseData(params.data[f].paxWise);
		for(var i = 0; i < params.data[f].paxWise.length ; i++) {			
			clonedTemplateId = templeteName + "_" + f + "" + i;			
			var clone = $( "#" + templeteName + "").clone();			
			clone.attr("id", clonedTemplateId );			
			clone.appendTo($("#" + templeteName).parent());			
			dtoRecord =  params.data[f].paxWise[i];			
			elementIdAppend = params.dtoName + "[" + i + "].";
			
			if (i == 0) {
				$("#"+clonedTemplateId + " td:first").attr("rowspan", params.data[f].paxWise.length);
				$("#"+clonedTemplateId + " td:first").find("label").each(function( intIndex ){
					$(this).text(segmentCode);
					$(this).attr("id",  elementIdAppend +  this.id);
				});					
			} else {
				$("#"+clonedTemplateId + " td:first").remove();
			}			
			$("#"+clonedTemplateId).find("label").each(function( intIndex ){
				$(this).text(dtoRecord[this.id]);
				$(this).attr("id",  elementIdAppend +  this.id);
			});						
			$( "#" + clonedTemplateId).show();			
		}
	}
	$( "#" + templeteName).hide();
}

/**
 * Click check box
*/
UI_FlightFare.checkClick = function(obj) {
	var status = obj.checked;
	var type = (obj.id).split("_")[0];	
	$("input[id*='" + type + "']:checked").attr("checked", false);	
	if (status == true) {
		obj.checked = true;
	} else {
		obj.checked = false;
	} 
	$("#trPriceBDPannel").hide();
	$("#btnReCalculate").show();
}
// To Do: Move to common
function raiseError(strErrNo){
	var strMsg = UI_FlightFare.errorInfo[strErrNo];
	if (arguments.length >1){
		for (var i = 0 ; i < arguments.length - 1 ; i++){
			strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
		}
	}
	return strMsg;
}