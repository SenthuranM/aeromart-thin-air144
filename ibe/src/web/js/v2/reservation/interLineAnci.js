/**
 * Ancillary
 * 
 * @author Dilan Anuruddha
 * @author Ruwan Baladewa
 *  
 */

function UI_Anci(){};
	
;(function(UI_Anci){
	UI_Anci.anciType = {
			SEAT:'SEAT_MAP',
			MEAL:'MEALS',
			SSR:'SSR',
			INS:'INSURANCE',
			HALA:'AIRPORT_SERVICE',
			BAGGAGE:'BAGGAGES',
			APTRANSFER:'AIRPORT_TRANSFER',
			FLEXI: 'FLEXI'
	};
	
	UI_Anci.promoAnciType = {
			SEAT_MAP:'SEAT_MAP',
			MEALS:'MEAL',
			SSR:'INFLIGHT_SERVICES',
			INSURANCE:'INSURANCE',
			AIRPORT_SERVICE:'AIRPORT_SERVICE',
			BAGGAGES:'BAGGAGE'
	}
	UI_Anci.promoTypes = {
			SYSGEN : "SYSGEN",
			BUYNGET : "BUYNGET",
			DISCOUNT : "DISCOUNT",
			FREESERVICE : "FREESERVICE"
	}
	UI_Anci.PromoAppicablity = {
			RESERVATION : 3,
			PER_PAX : 4
	}
	UI_Anci.promoDiscountType = {
			PERCENTAGE : "PERCENTAGE",
			VALUE : "VALUE"
	}
	UI_Anci.promoDiscountAs = {
			MONEY : "Money",
			CREDIT : "Credit"
	}
	
	UI_Anci.infantTypeSuffix = "";//removed by Client request ' /INF'
	
	/* Segment data structues */
	UI_Anci.jsAnciSeg = null;
	UI_Anci.jsAnciAvail = null;
	UI_Anci.jsAnciSegModel  = null;
	UI_Anci.flightSegments = null;
	UI_Anci.pnr = null;
	UI_Anci.browser = $.browser;
	
	UI_Anci.blnRegUser = false;
	UI_Anci.hasAPSThumbnails= true;
	
	UI_Anci.isFQTVSelected = new Array();
	UI_Anci.applyToAllSegments = false;
	UI_Anci.globallyAppliedSsr = 'FQTV';
	
	/* Seat Map Segment Model */
	UI_Anci.jsSMModel = null;
	
	/* Array of seats against segments 
	 * occupied by the pax is contained here.
	 */
	UI_Anci.occupiedSeats = {};
	
	// seat re-protect status
	UI_Anci.isSeatReprotected = {};
	
	UI_Anci.fltWiseFreeSeatMap = {};
	UI_Anci.fltWiseFreeMealMap = {};
	
	//Seat navigation structure 
	UI_Anci.currentSegPax = 0;
	UI_Anci.segmentPaxUsedArray = new Array();
	//Baggage navigation structure 
	UI_Anci.currentBaggageSegPax = 0;
	UI_Anci.segmentPaxBaggageUsedArray = new Array();
	//Hala navigation structure 
	UI_Anci.currentHalaSegPax = 0;
	UI_Anci.segmentPaxHalaUsedArray = new Array();
	
	UI_Anci.multimealEnebled = false;
	UI_Anci.mealCategoryEnebled = false;
	
	UI_Anci.allowModifyApts= false;
	
	
	UI_Anci.isBaggagesSelectedForCurrentBooking = false;
	UI_Anci.isBaggagesAvailableToReprotect = false;
	UI_Anci.isPreviousBookingWithNoBaggages = false;
	UI_Anci.ondBaggageObj = {};
	UI_Anci.isONDBaggage = "N";

	
	UI_Anci.mealViewType  = anciConfig.mealview;
	UI_Anci.seatViewType  = anciConfig.seatview; 
	UI_Anci.insViewType   = anciConfig.insViewType; 	
	UI_Anci.showInsCharge = anciConfig.showInsCharge;
	UI_Anci.showMealMenu  = anciConfig.menuView;
	UI_Anci.baggageViewType  = anciConfig.baggageview;
	
	UI_Anci.ondAvailbleFlexi = {};
	UI_Anci.jsAnciFlexiAdded = {};
	//TODO:RW is this required???
	UI_Anci.jsAnciFlexiAvailable = {"0":true,"1":true};
	UI_Anci.jsAnciFlexiUAdded = {"0":false,"1":false};
	
	//AIRLINE_198 Show flexi selection in Anci page
	UI_Anci.flexiEnableInAnci = true;
	
	/* Meal data structures */
	UI_Anci.jsMealModel = null;
	/* Meal data structures */
	UI_Anci.jsMealCategoryModel = null;
	
	/* Baggage data structures */
	UI_Anci.jsBaggageModel = null;
	UI_Anci.jsBaggageFlightSegmentModel = null;
	
	/* SSR data structures */
	UI_Anci.jsSSRModel = null;
	
	/* Adult, Child PAX data */
	UI_Anci.jsonPaxAdults = null;
	/* Infant data */
	//UI_Anci.jsonPaxInfants = null;
	
	//to Load the logical cabin class map
	UI_Anci.lccMap = {};
	
	UI_Anci.busCarrierCodes = {};
	
	UI_Anci.jsonLabel = [];
	
	UI_Anci.seat = {
			currentPaxId: -1,
			currentSegId: -1
	};
	UI_Anci.meal = {
			currentPaxId: -1,
			currentSegId: -1
	};
	
	UI_Anci.ssr = {
			currentPaxId: -1,
			currentSegId: -1
	};
	UI_Anci.baggage = {
			currentPaxId: -1,
			currentSegId: -1
	};
	UI_Anci.hala = {
			currentPaxId: -1,
			currentSegId: -1
	};
	UI_Anci.anciSummary = [];
	UI_Anci.anciTotal= 0;
	UI_Anci.fareQuote = null;
	UI_Anci.journeySummary = null;
	UI_Anci.anciInsurance = null;
	UI_Anci.tabSltdSeat = null;
	UI_Anci.tabSltdMeal = null;
	UI_Anci.tabSltdSSR = null;
	UI_Anci.homeUrl = "";
	UI_Anci.securePath = "";
	UI_Anci.nonSecurePath = '';
	UI_Anci.availableBundleFares = {} ;
	UI_Anci.winO = null;
	UI_Anci.currency = {selected:'',base:'',exchangeRate:1};
	UI_Anci.loaded = false;
	UI_Anci.system = '';
	UI_Anci.direction = '';
	UI_Anci.noAncillary = false;
	UI_Anci.seatRefMap = {};
	UI_Anci.notAllAnciReprotect = false;
	UI_Anci.modifyRakInsurance = false;
	UI_Anci.isRakInsurance =  false;
	UI_Anci.formattedInsAmount ="";
	UI_Anci.formattedOptionalInsAmount ="";
	UI_Anci.navigateFromAnci = false;
	UI_Anci.INSURANCE_TYPE = {NONE:'0',GENERAL: "1", CNX:'2', MLTRISK:'3'};
	UI_Anci.selectedBaggages = null;
	UI_Anci.currAirportServiceApplicability = '';
	UI_Anci.insuranceViewChanged = false;//view changed according to the country
	UI_Anci.selectDefaultSeatsForPax = true;
	UI_Anci.isDateModification = false;
	UI_Anci.insuranceObj = {};
	UI_Anci.freeServiceDecorator = "Free";
	UI_Anci.selectedBundledFares = null;

	UI_Anci.freeServiceDiscount = 0;
	UI_Anci.insurancePopUpContent='';
	UI_Anci.avilableInsuranceRefNumbers='';
	UI_Anci.avilableInsuranceproductGroup='';
	UI_Anci.selectedInsuranceRefNumbers = [];
	UI_Anci.selectedInsuranceQuotes = [];
	UI_Anci.lastSelectIsDefault = true;
	UI_Anci.preferredMeals = null;
	UI_Anci.preferredSeatType = null;
	
	UI_Anci.reprotectLoaded = {
			meal: false,
			ssr: false,
			baggage: false
	};
	
	UI_Anci.loadingInProgress = {
			meal: false,
			seat: false,
			ssr: false,
			insurance: false,
			other: false,
			baggage:false,
			aps: false,
			apt: false
	};
	
	UI_Anci.ChargeType = {
		PER_PAX : 'P',
		PER_RESERVATION : 'R'
	};
	
	UI_Anci.AirportType = {
		DEPARTURE : 'D',
		ARRIVAL : 'A',
		TRANSIT: 'T'
	};
	
	UI_Anci.PaxType = {
		PARENT : 'PA',
		ADULT  : 'AD',
		CHILD  : 'CH'
	};
	
	UI_Anci.JNTax = {
		isApplicable : false,
		ratio: 0,
		taxAmount: 0
	}
	
	UI_Anci.totalFlexiCharge = 0;
	
	UI_Anci.seatsBlocked = false;
	
	UI_Anci.availableBundleFares = {} ;
	UI_Anci.OND_SEQUENCE = {OUT_BOUND:0, IN_BOUND:1};
    UI_Anci.paxWiseAnci = {};
    //used for seat selection
    UI_Anci.fltRefNo = undefined;
	
	UI_Anci.ready = function(direction){
		globalConfig.currentPage = "ANCI";
		UI_Anci.navigateFromAnci = false;
		UI_Anci.direction = direction;
		var freeServiceObj = UI_Anci.getPromoDiscountObject(UI_Anci.promoTypes.FREESERVICE);
		if(freeServiceObj != undefined && freeServiceObj != null ){			
			$("#divAnciPromoBanner").show();
		}
		$("#divSummaryPane").css("top","0px");
		if(UI_Container.isModifyAncillary()){
			UI_Anci.updatePriceSummary();
		}
		if(UI_Anci.noAncillary){
			setTimeout('UI_Anci.handleNavigation()',1000);
		}else{
			UI_Anci.showLoadingMsg()
			if(UI_Anci.loaded==false){
				setMealLayout();
				setSeatMapLayout();
				setInsuranceLayout();
				setBaggageLayout();
				setHalaLayout();
				setAirportTransferLayout();
				setFlexiLayout();
				UI_Anci.loadAnciData();
			}else{
				setTimeout('UI_Anci.hideLoadingMsg()',1000);
			}
		}
		if (anciConfig.paymentOnAnici == true){
			$('#anciBtnPayment').show();
			$('#anciBtnNext').hide();
		}
		if(UI_Anci.browser.webkit){
			$("#spnSeatInfo").css("position", "fixed");
			$("#spnSocial").css("position", "fixed");
		}
				
		UI_Anci.jsAnciFlexiAdded = _parseJSONStringToObject($("#resflightSearchOndFlexiSelection").val());		
		UI_Anci.ondAvailbleFlexi = _parseJSONStringToObject($('#resOndAvailbleFlexiStr').val());
		UI_Anci.availableBundleFares = _parseJSONStringToObject($("input#resAvailableBundleFareLCClassStr").val());

		UI_commonSystem.changeUserLmsStatus(UI_commonSystem.lmsDetails);
		UI_commonSystem.showLmsInBar();
	};
	
	UI_Anci.handleNavigation = function(){	
		if(UI_Anci.navigateFromAnci) return;
		if(UI_Anci.direction == 'next'){
			UI_Anci.anciBlockingResponse({success:true});
		}else{
			$('#anciBtnPrevious').click();
		}
	};
	
	UI_Anci.startOverClick = function() {
		top[0].homeClick();
	};
	
	UI_Anci.loadPageText = function(){
		var lbls = UI_Anci.jsonLabel;
		UI_Anci.infantTypeSuffix = " \/ " +lbls['lblInfantTypeSuffix'];
		/* Link texts */
		$('#lblInsuranceDesc').html(lbls['lblInsuranceDesc']);
		$('#lblInsuranceTNC').html(lbls['lblInsuranceTNC']);
		$('#insCoverInfo1').html(lbls['insCoverInfo1']);
		
		/* buttons */
		$('#btnViewSkyCafe').val(lbls['btnViewSkyCafe']);
		$('#btnSeatAddNow').val(lbls['btnSeatAddNow']);
		$('#btnMealAddNow').val(lbls['btnMealAddNow']);	
		$('#btnSSRAddNow').val(lbls['btnSSRAddNow']);
			
	};
	
	/* 
	 * Disable all buttons 
	 */
	UI_Anci.disableButtons = function(){
		$("#btnViewSkyCafe").attr("disabled","disabled");
		$("#btnSeatAddNow").attr("disabled","disabled");
		$("#btnMealAddNow").attr("disabled","disabled");
		$("#btnSSRAddNow").attr("disabled","disabled");
		$("#chkAnciIns").attr("disabled","disabled");
		
		$("#btnCancel").attr("disabled","disabled").addClass("ButtonDefaultDisable");
		$("#btnContinue").attr("disabled","disabled").addClass("ButtonDefaultDisable");
		
	};
	/*
	 * Enable buttons 
	 */
	UI_Anci.enableButtons = function(){
		$("#btnViewSkyCafe").attr("disabled","");
		$("#btnSeatAddNow").attr("disabled","");
		$("#btnMealAddNow").attr("disabled","");
		$("#btnSSRAddNow").attr("disabled","");
		$("#chkAnciIns").attr("disabled","");
		
		$("#btnCancel").attr("disabled","").removeClass("ButtonDefaultDisable");
		$("#btnContinue").attr("disabled","").removeClass("ButtonDefaultDisable");		
	};
	
	/*
	 * Ancillary availability data load...
	 */
	UI_Anci.loadAnciData = function(){
		var data = {};	
		data = $.extend(data,UI_Container.getPaxSubmitData());		
		
		if(UI_Container.isModifySegment() || UI_Container.isModifyAncillary ){
			if (UI_Container.isAllowInfantInsurance) {
				data['resPaxInfo'] = $.toJSON(UI_Anci.getPaxArray(true));
			} else {
				data['resPaxInfo'] = $.toJSON(UI_Anci.getPaxArray());
			}
		}
		
		//yes no Seats--------
		$("input[name='radSeat']").live("click", function(){
				if ($(this).val() == "Y"){
					$(".divSearAnici").slideDown(100);
				}else{
					if (isSeatesUsed()){
						UI_Anci.removeAllSelectedSeats();
					}else{
						$(".divSearAnici").hide();
					}
				}
		});
		//yes no Baggage--------
		$("input[name='radBaggage']").live("click", function(){
				if ($(this).val() == "Y"){
					$(".divBaggageAnci").slideDown(100);
				}else{
					if (isBaggagesUsed()){
						jAlert(UI_Container.labels.lblAnciRemoveBaggageAlert);
						$("#radBaggage_Y").trigger("click");
					}else{
						$(".divBaggageAnci").hide();
					}
				}
		});
		
		
		var fnClickTracker = function(){
			UI_Anci.navigateFromAnci = true;
		}
		$('#anciBtnNext').click(fnClickTracker);
		$('#anciBtnPrevious').click(fnClickTracker);	
		$('#anciBtnPayment').click(fnClickTracker);
		
		//yes no Meals--------
		$("input[name='radMeal']").live("click", function(){
				if ($(this).val() == "Y"){
					$("#tblMealDetails").slideDown(100);
					//$("#mealBannerRow").hide();
				}else{
					if (isMealUsed()){
						jAlert(UI_Container.labels.lblAnciRemoveMealAlert);
						$("#radMeal_Y").trigger("click");
					}else{
						$("#tblMealDetails").hide();
						//$("#mealBannerRow").show();
					}
				}
		});
		
		//yes no SSR--------
		$("input[name='radSSR']").live("click", function(){
			if ($(this).val() == "Y"){
				$("#divSSRContainer").slideDown(100);
			}else{
				if (isSSRUsed()){
					jAlert(UI_Container.labels.lblAnciRemoveSSRAlert);
					$("#radSSR_Y").trigger("click");
				}else{
					//$('html, body').scrollTop($('html, body').scrollTop() - 1);//solution for page flickering but not works in all browsers 
					//$("#divSSRContainer").slideUp(500); // can not fix the page flickering for jQuery slideUp till fixed page height fixed 
					$("#divSSRContainer").hide();
				}
			}
		});
		
		//yes no AP services--------
		$("input[name='radHala']").live("click", function(){
			if ($(this).val() == "Y"){
				$("#tblHalaDetails").slideDown(100);
			}else{
				if (isAPSUsed()){
					jAlert(UI_Container.labels.lblAnciRemoveSSRAlert);
					$("#radHala_Y").trigger("click");
				}else{
					$("#tblHalaDetails").hide();
				}
			}
		});
		
		//yes no Flexi--------
		$("input[name='radFlexi']").live("click", function(){
			if ($(this).val() == "Y"){
				$("#tblFlexiDetails").slideDown(100);
			}else{
				if (isFlexiUsed()){
					jAlert(UI_Container.labels.lblAnciRemoveFlexiAlert);
					$("#radFlexi_Y").trigger("click");
				}else{
					$("#tblFlexiDetails").hide();
				}
			}
		});
		
		$("input[name='radAPT']").live("click", function(){
			if ($(this).val() == "Y"){
				$("#tblApTransferDetails").slideDown(100);
			}else{
				if (isAPTUsed()){
					jAlert(UI_Container.labels.lblAnciRemoveAPTAlert);
					$("#radAPT_Y").trigger("click");
				}else{
					$("#tblApTransferDetails").hide();
				}
			}
		});
		
		
		UI_commonSystem.setCommonButtonSettings(); 
		
		 $("#frmReservation").attr('action', "interlineAnciInit.action");
		 data["searchParams.ondListString"] = $("#resOndListStr").val();
		 data["hasInsurance"] = UI_Container.hasInsurance;
		 $("#frmReservation").ajaxSubmit({dataType: 'json',processData: false,	data:data,		
				success: UI_Anci.loadAnciPage, error:UI_commonSystem.setErrorStatus});

	};
	

	
	function isSeatesUsed (type,item){
		var seatsUsed = false;
		$.each(UI_Anci.jsAnciSegModel, function(i,j){
			$.each(j.pax, function(k,l){
				if (l.seat.seatNumber != "")
					seatsUsed = true;
			});
		})
		return seatsUsed
	};
	function isBaggagesUsed (){
		var baggagesUsed = false;
		$.each(UI_Anci.jsAnciSegModel, function(i,j){
			$.each(j.pax, function(k,l){
				if (l.baggage.baggages != "")
					baggagesUsed = true;
			});
		})
		return baggagesUsed
	};
	function isMealUsed (){
		var mealUsed = false;
		$.each(UI_Anci.jsAnciSegModel, function(i,j){
			$.each(j.pax, function(k,l){
				if (l.meal.meals != "")
					mealUsed = true;
			});
		});
		return mealUsed
	};
	
	function isSSRUsed(){
		var mealUsed = false;
		$.each(UI_Anci.jsAnciSegModel, function(i,j){
			$.each(j.pax, function(k,l){
				if (l.ssr.ssrs != "")
					mealUsed = true;
			});
		});
		return mealUsed
	};
	
	function isAPSUsed(){
		var apsUsed = false;
		$.each(UI_Anci.jsAnciSegModel, function(i,j){
			$.each(j.pax, function(k,l){
				if (l.aps.apss != "")
					apsUsed = true;
			});
		});
		return apsUsed;
	};
	
	function isAPTUsed(){
		var aptUsed = false;
		$.each(UI_Anci.jsAnciSegModel, function(i,j){
			$.each(j.pax, function(k,l){
				if (l.apt.apts != "")
					aptUsed = true;
			});
		});
		return aptUsed;
	};

	function isFlexiUsed(){
		var flexiUsed = false;
		var temFlexObj = UI_Anci.jsAnciFlexiUAdded;
		for (var prop in temFlexObj) {
			if(temFlexObj.hasOwnProperty(prop)){
		        if (temFlexObj[prop]){
		        	flexiUsed = true;
		        	break;
		        }
		    }
		}
		return flexiUsed;
	};
	
	/*
	 * Return default currency
	 */
	UI_Anci.getDefaultCurrency = function(){
		return UI_Anci.currency.base;
	}
	
	/*
	 * Return the child and adult arrays
	 */
	UI_Anci.getPaxArray = function(includeInfants){
		var paxDetails = UI_Container.getPaxDetails();
		var paxArr = [];
		for(var i = 0 ; i < paxDetails.adults.length; i++){
			var index = paxArr.length;
			paxArr[index] = paxDetails.adults[i];
			if(paxArr[index].parent == 'true' || paxArr[index].parent === true){	
				paxArr[index].paxType= 'PA';
			}else{
				paxArr[index].paxType= 'AD';
			}
		}
		for(var i = 0 ; i < paxDetails.children.length; i++){
			var index = paxArr.length;
			paxArr[index] = paxDetails.children[i];
			paxArr[index].paxType='CH';
		
		}
		if((includeInfants||false)){
			for(var i = 0 ; i < paxDetails.infants.length; i++){
				var index = paxArr.length;
				paxArr[index] = paxDetails.infants[i];
				paxArr[index].paxType='IN';
			
			}
		}
		
		for(var i = 0 ; i < paxArr.length ; i++ ){
			paxArr[i].name = paxArr[i].firstName + ' ' + paxArr[i].lastName;	
			paxArr[i].anci = [];
			paxArr[i].removeAnci = [];
			paxArr[i].paxId = i;
		}
		return paxArr;
	}
	/*
	 * Populate Ancillary availability
	 */
	UI_Anci.populateStructures = function(p){
		
		var paxDetails = UI_Container.getPaxDetails();
		
		UI_Anci.jsonLabel = UI_Container.getLabels();
		if(UI_Anci.jsonLabel==null){
			UI_Anci.jsonLabel = [];
		}

		UI_Anci.loadPageText();
		UI_Anci.currency =  UI_Container.getCurrencies();
		
		var insLink = "";
		var insCondLink = "";
		if(UI_Anci.jsonLabel != null){
			insLink = UI_Anci.jsonLabel['linkInsurance'];
			insCondLink = UI_Anci.jsonLabel['linkInsuranceTermNCond'];
		}
		UI_Anci.anciInsurance = {total:0,selected:false,insLink: insLink,insCondLink:insCondLink,insuranceRefNumber:null,operatingAirline:null,quotedTotalPremiumAmount:0,ssrFeeCode:null,planCode:null,lastSelectIsDefault:true};
		UI_Anci.anciSummary.length = 0;	
		
		
		UI_Anci.insuranceViewChanged = p.insuranceViewChanged;
//		if(p.insuranceAvailable && !UI_Anci.insuranceViewChanged){
//			var htmlStr = "<table><tr>"+
//			"<td class='alignLeft' width='2%'><input type='checkbox' id='radAnciIns_Y' name='chkIns' checked='checked' /></td>"+
//			"<td width='98%' class='alignLeft'><label id='lblInsuranceYes' >"+UI_Container.labels.lblInsuranceYes+"</label></td>"+
//			"</tr></table>";
//			$(".insuranceViewChange").html(htmlStr);
//			$('#radAnciIns_Y').val(p.insuranceDTO.quotedTotalPremiumAmount);
//			$("#radAnciIns_Y").unbind("click").bind("click", function(){
//				if ($("#radAnciIns_Y").attr("checked") == true){
//					UI_Anci.insRadioOnClick('radAnciIns_Y');
//				}else{
//					UI_Anci.insRadioOnClick('radAnciIns_N');
//				}
//			});
//		}
		
		if(p.insuranceAvailable) {
			UI_Anci.insuranceObj = p.insuranceDTO;
			UI_Anci.avilableInsuranceproductGroup = UI_Anci.createInsuranceGroups(UI_Anci.insuranceObj);
			UI_Anci.buildInsuranceMainProduct(p.insuranceDTO, true);
			UI_Anci.buildInsuranceOptionalProduct(p.insuranceDTO);
			if (p.insuranceDTO[0].europianCountry) {
				UI_Anci.RemoveAllSelectedInsuranceQuotes();
			}
		}
		UI_Anci.jsonPaxAdults = UI_Anci.getPaxArray();
		
		UI_Anci.jsAnciSeg = p.availabilityOutDTO;
		
		UI_Anci.jsAnciSeg = $.airutil.sort.quickSort(UI_Anci.jsAnciSeg,segmentComparator);
		
		
		if(UI_Container.isModifyAncillary() ){
			
			var fixMissingSegs = function(paxSegs){
				for(var i = 0; i< UI_Anci.jsAnciSeg.length; i++){
					var found = false;
					for(var j = 0; j< paxSegs.length; j++){
						if(compareFltRefNos(paxSegs[j].flightSegmentTO.flightRefNumber, 
								UI_Anci.jsAnciSeg[i].flightSegmentTO.flightRefNumber)){
							found= true;
							break;
						}
					}
					if(!found){
						paxSegs[paxSegs.length]={flightSegmentTO:UI_Anci.jsAnciSeg[i].flightSegmentTO,
								airSeatDTO:null,mealDTOs:[], specialServiceRequestDTOs:[], insuranceQuotation:null, airportServiceRequestDTOs:[], airportTransferDTOs:[]};
					}
				}
				return paxSegs;
			}
			for(var j = 0 ; j < UI_Anci.jsonPaxAdults.length; j++){
				var paxAnci = UI_Anci.jsonPaxAdults[j].currentAncillaries;
				paxAnci =fixMissingSegs(paxAnci);
				UI_Anci.jsonPaxAdults[j].currentAncillaries = $.airutil.sort.quickSort(paxAnci,segmentComparator);
			}
		}
		
	
		UI_Anci.jsAnciAvail = {meals:[],seats:[],hala:[],ssrs:[],baggages:[], apts:[],flexi:[], segmentInfo:[]};
		
		
		
		UI_Anci.jsSMModel = [];
		UI_Anci.jsAnciSegModel = [];
		
		if( UI_Anci.jsonPaxAdults == null ){
			UI_Anci.jsonPaxAdults = [];
		}
		
		
		for(var i = 0 ; i < UI_Anci.jsAnciSeg.length ; i++ ){
			var seg = UI_Anci.jsAnciSeg[i];
			var ancAv = {meal: false, seat: false, hala: false, ssr:false, baggage:false, apt: false, flexi:false};
			seg = $.extend(seg, ancAv);
			for(var j = 0 ; j < seg.ancillaryStatusList.length; j++){
				if(seg.ancillaryStatusList[j].available){
					var segObj = {segCode: seg.flightSegmentTO.segmentCode,segInd: i, airportCode:seg.flightSegmentTO.airportCode};
					var anciType = seg.ancillaryStatusList[j].ancillaryType.ancillaryType;
										
					if( anciType == UI_Anci.anciType.MEAL){
						UI_Anci.jsAnciAvail.meals[UI_Anci.jsAnciAvail.meals.length] = segObj;
						seg.meal = true;
					}else if( anciType == UI_Anci.anciType.SEAT){
						//Added flight information with returnFlag attribute. This will use to check inbound/outbound flights
						//This segmentInfo is used in Segments for Seat maps section
						if(seg.flightSegmentTO.returnFlag){ 
							var returnFlag = {returnFlag : true};
							var seatMapSegObj = $.extend(segObj,returnFlag);
							UI_Anci.jsAnciAvail.segmentInfo[UI_Anci.jsAnciAvail.segmentInfo.length]=seatMapSegObj;
						}else{
							var returnFlag = {returnFlag : false};
							var seatMapSegObj = $.extend(segObj,returnFlag);
							UI_Anci.jsAnciAvail.segmentInfo[UI_Anci.jsAnciAvail.segmentInfo.length]=seatMapSegObj;
						}
						UI_Anci.jsAnciAvail.seats[UI_Anci.jsAnciAvail.seats.length] = segObj;
						seg.seat = true;
					}else if ( anciType == UI_Anci.anciType.HALA){
						UI_Anci.jsAnciAvail.hala[UI_Anci.jsAnciAvail.hala.length] = segObj;
						seg.hala = true;
					}else if( anciType == UI_Anci.anciType.SSR){
						UI_Anci.jsAnciAvail.ssrs[UI_Anci.jsAnciAvail.ssrs.length] = segObj;
						seg.ssr = true;
					}else if( anciType == UI_Anci.anciType.BAGGAGE){
						//segObj.segInd = UI_Anci.jsAnciAvail.baggages.length;
						UI_Anci.jsAnciAvail.baggages[UI_Anci.jsAnciAvail.baggages.length] = segObj;
						seg.baggage = true;
					}else if ( anciType == UI_Anci.anciType.APTRANSFER){
						UI_Anci.jsAnciAvail.apts[UI_Anci.jsAnciAvail.apts.length] = segObj;
						seg.apt = true;
					} else if(anciType == UI_Anci.anciType.FLEXI){
						UI_Anci.jsAnciAvail.flexi[UI_Anci.jsAnciAvail.flexi.length] = segObj;
						seg.flexi = true;
					}
				}
			}
			UI_Anci.jsAnciSegModel.push({segCode:seg.flightSegmentTO.segmentCode, returnFlag:seg.flightSegmentTO.returnFlag ,flightRefNumber: seg.flightSegmentTO.flightRefNumber, pax:[], pnrSegId : seg.flightSegmentTO.pnrSegId, flownSegment : seg.flightSegmentTO.flownSegment});
			for(var j = 0 ; j < UI_Anci.jsonPaxAdults.length; j++){				
				UI_Anci.jsAnciSegModel[UI_Anci.jsAnciSegModel.length-1].pax[j]={paxType:UI_Anci.jsonPaxAdults[j].paxType,refI: j, seat:{seatNumber:'',seatCharge:0},meal:{meals:[],mealChargeTotal:0,mealQtyTotal:0},ssr:{ssrs:[],ssrChargeTotal:0,ssrQtyTotal:0},baggage:{baggages:[],baggageChargeTotal:0,baggageQtyTotal:0,isOnd:UI_Anci.isONDBaggage},aps:{apss:[],apsChargeTotal:0}, apt:{apts:[],aptChargeTotal:0}};
			}
			UI_Anci.occupiedSeats[seg.flightSegmentTO.flightRefNumber]= {seats:{}}; // CONVERTED TO HASH
		}
	}
	
	/*
	 * Reset data on the block error
	 */
	UI_Anci.resetDataOnBlockError = function(){
		UI_commonSystem.pageOnChange();
		/* For Seats */
		$.each(UI_Anci.occupiedSeats, function(fltRefNo,obj){
			$.each(obj.seats,function(seatNumber, seatObj){
				if(seatObj!=null && parseInt(seatObj.paxId,10) >=0){
					UI_Anci.clearSeat(seatObj);
				}
			});
		});		
	}
	
	UI_Anci.getEnabledAncilllaries = function (availRS){
		var anciAvail={};
		var modFalg = UI_Container.isModifyAncillary();
		anciAvail.airportServicesAvailable = (modFalg?(availRS.airportServicesAvailable && UI_Container.isAnciModifyEnabled('AIRPORT_SERVICE')):
			availRS.airportServicesAvailable);
		anciAvail.mealsAvailable = (modFalg?(availRS.mealsAvailable && UI_Container.isAnciModifyEnabled('MEAL')):
			availRS.mealsAvailable);
		anciAvail.insuranceAvailable = (modFalg?(availRS.insuranceAvailable && UI_Container.isAnciModifyEnabled('INSURANCE')):
			availRS.insuranceAvailable);
		anciAvail.seatMapAvailable = (modFalg?(availRS.seatMapAvailable && UI_Container.isAnciModifyEnabled('SEAT')):
			availRS.seatMapAvailable);
		anciAvail.ssrAvailable = (modFalg?(availRS.ssrAvailable && UI_Container.isAnciModifyEnabled('SSR')):
			availRS.ssrAvailable);
		anciAvail.baggagesAvailable = (modFalg?(availRS.baggagesAvailable && UI_Container.isAnciModifyEnabled('BAGGAGE')):
			availRS.baggagesAvailable);
		anciAvail.airportTransferAvailable = (modFalg?(availRS.airportTransferAvailable && UI_Container.isAnciModifyEnabled('AIRPORT_TRANSFER')):
			availRS.airportTransferAvailable);
		anciAvail.flexiAvailable = (modFalg?(availRS.flexiAvailable && UI_Container.isAnciModifyEnabled('FLEXI')):
			availRS.flexiAvailable);
		return anciAvail;
	}
	/*
	 * setting up new ancillary order with configuration
	 */
	UI_Anci.anciLayoutConfig = function (){
		if (anciConfig.layout.anciOrder != undefined){
			var anciRowsArray = $("#anciConfigTable").find("tr.anciRow");
			$("#anciConfigTable").empty();
			for (var i = 0; i < anciConfig.layout.anciOrder.length; i++){
				for (var j = 0; j < anciRowsArray.length; j++){
					if (anciConfig.layout.anciOrder[i] == anciRowsArray[j].id){
						$("#anciConfigTable").append(anciRowsArray[j]);
						break;
					}
				}
			}UI_Anci
		}
	}
		
	/*
	 * Loading ancillary page
	 */
	UI_Anci.loadAnciPage = function(response){		
		UI_Anci.anciLayoutConfig();
		$("#anciMainTable").show();
		UI_Anci.hideLoadingMsg()
		if(response != null && response.success) {
			UI_Anci.lccMap = response.lccMap;
			UI_Anci.loaded = true;			
			UI_Anci.system = response.system;
			UI_Anci.isRakInsurance = response.rakEnabled;
			UI_SocialSeat.params = response.socialSeatingParams;
			UI_Anci.busCarrierCodes = response.busCarrierCodes;
			
			UI_Anci.populateStructures(response);
			UI_Anci.freeServiceDecorator = UI_Anci.jsonLabel['lblBundledFreeService'];
			UI_Anci.isDateModification = response.dateModification;
			UI_Anci.totalFlexiCharge = response.flexiCharge;
			UI_Anci.JNTax.isApplicable = response.jnTaxApplicable;
			if(response.jnTaxApplicable){
				UI_Anci.JNTax.ratio = response.jnTaxRatio;
			} else {
				UI_Anci.JNTax.ratio = 0;
			}
			UI_Container.applyAncillaryPenalty = response.applyAncillaryPenalty;
			UI_Anci.fltWiseFreeSeatMap = {};
			UI_Anci.fltWiseFreeMealMap = {};
			$.each(UI_Anci.jsAnciSeg, function(i, obj){
				var fltSeg = obj.flightSegmentTO;
				UI_Anci.fltWiseFreeSeatMap[fltSeg.flightRefNumber] = obj.seat && fltSeg.freeSeatEnabled;
				UI_Anci.fltWiseFreeMealMap[fltSeg.flightRefNumber] = obj.meal && fltSeg.freeMealEnabled;
			});
			
			UI_Anci.selectedBundledFares = response.selectedBundledFareDTOs;
			
			$("#tblMainTrSelectAnciText").hide();
			$("#tblMainTrNoAnciMsg").hide();
			$("#tblSeat").hide();
			$("#tblIns").hide();
			$("#tblMeal").hide();
			$("#tblHala").hide();
			$("#tblBaggage").hide();
			$("#tblApTransfer").hide();
			$("#tblFlexi").hide();

			
			var loadSync = (UI_Container.isModifySegment() || UI_Container.isModifyAncillary())
			
			var anciSummary = UI_Anci.anciSummary;
			var anciStatus = false;
			var anciAvail = UI_Anci.getEnabledAncilllaries(response);
			anciAvail.flexiAvailable = true;
			
			UI_Anci.flexiEnableInAnci =  UI_Anci.isFlexiSelectionEnablesInAnciPage(response.availabilityOutDTO);
			anciSummary[anciSummary.length]={type:UI_Anci.anciType.FLEXI,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblFlexiCharges'],rad:'radFlexi'};
			if(anciAvail.flexiAvailable && UI_Anci.flexiEnableInAnci){
				$("#tblFlexi").show();
				UI_Anci.buildONDFlexiTab({id:"tbdyFlexiSeg", tdID:"tdFlexiSeg_", click:null, segs: UI_Anci.jsAnciAvail.flexi});
				anciSummary[anciSummary.length-1].stat = 'ACT';
				anciStatus  = true;
			}

			anciSummary[anciSummary.length]={type:UI_Anci.anciType.HALA,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblHalaSelection'],rad:'radHala'};
			if(anciAvail.airportServicesAvailable){
				$("#tblHala").show();
				UI_Anci.loadHalaData(true);
				anciSummary[anciSummary.length-1].stat = 'ACT';
				anciStatus  = true;
			}
			
			anciSummary[anciSummary.length]={type:UI_Anci.anciType.MEAL,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblMealSelection'],rad:'radMeal'};
			if(anciAvail.mealsAvailable){
				anciSummary[anciSummary.length-1].stat = 'ACT';
				$("#tblMeal").show();
				anciStatus  = true;
				if (UI_Anci.mealViewType != "New-view"){//old one should load before the meal data is loaded		
					UI_Anci.buildSegmentsTab({id:"tbdyMealSeg", tdID:"tdMealSeg_", click:UI_Anci.tabClickMeal, segs: UI_Anci.jsAnciAvail.meals});
					UI_Anci.buildMealPax();
				}
				
				UI_Anci.loadMealData(loadSync);
			}
			
			anciSummary[anciSummary.length]={type:UI_Anci.anciType.INS,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblTravelSecure'],rad:null};
			
			
			if(anciAvail.insuranceAvailable ){
				anciSummary[anciSummary.length-1].stat = 'ACT';
				if(!UI_Anci.insuranceViewChanged){
					UI_Anci.insRadioOnClick('radAnciIns_Y');
				}
				$("#tblIns").show();
				anciStatus  = true;
			}
			
			anciSummary[anciSummary.length]={type:UI_Anci.anciType.SEAT,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblSeatSelection'],rad:'radSeat'};
			if(anciAvail.seatMapAvailable){
				anciSummary[anciSummary.length-1].stat = 'ACT';
				anciStatus  = true;
				if (UI_Anci.seatViewType != "New-view"){//old one should load before seat data is loaded					
					UI_Anci.buildSegmentsTab({id:"tbdySeatSeg", tdID:"tdSeatSeg_", click:UI_Anci.tabClickSeat, segs: UI_Anci.jsAnciAvail.seats});
					UI_Anci.buildSeatPax(UI_Anci.jsAnciAvail.seats);
				}				
				UI_Anci.loadSeatData(loadSync);
				if (UI_Anci.seatViewType == "New-view"){//new one should load after the seat data is loaded
					UI_Anci.buildSegmentsSeatsTab({id:"tbdySeatSeg", tdID:"tdSeatSeg_", click:null, segs: UI_Anci.jsAnciAvail.seats});
					UI_Anci.buildSeatPax(UI_Anci.jsAnciAvail.seats);
				}
				UI_Anci.trigSegPax();
			}
			
			anciSummary[anciSummary.length]={type:UI_Anci.anciType.SSR,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblSSRSelection'],rad:'radSSR'};
			if(anciAvail.ssrAvailable){
				anciSummary[anciSummary.length-1].stat = 'ACT';
				$("#tblSSR").show();
				anciStatus  = true;
				UI_Anci.buildSegmentsTab({id:"tbdySSRSeg", tdID:"tdSSRSeg_", click:UI_Anci.tabClickSSR, segs: UI_Anci.jsAnciAvail.ssrs});
				UI_Anci.buildSSRPax();
				UI_Anci.loadSSRData(loadSync);
			}
			anciSummary[anciSummary.length]={type:UI_Anci.anciType.BAGGAGE,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblBaggageSelection'],rad:'radBaggage'};
			if(anciAvail.baggagesAvailable){
				anciSummary[anciSummary.length-1].stat = 'ACT';
				anciStatus  = true;				
				UI_Anci.loadBaggageData(loadSync);
				if (UI_Anci.baggageViewType != "New-view"){	
					//TODO tab-view
					UI_Anci.buildSegmentsTab({id:"tbdyBaggageSeg", tdID:"tdBaggageSeg_", click:UI_Anci.tabClickBaggage, segs: UI_Anci.jsAnciAvail.baggages});
					UI_Anci.buildBaggagePax(UI_Anci.jsAnciAvail.baggages);
				}
//				if (UI_Anci.baggageViewType == "New-view"){
//					UI_Anci.buildSegmentsBaggageTab({id:"tbdyBaggageSeg", tdID:"tdBaggageSeg_", click:null, baggages: UI_Anci.jsAnciAvail.baggages});
//					UI_Anci.buildBaggagePax(UI_Anci.jsAnciAvail.baggages);
//				}
			}
			
			anciSummary[anciSummary.length]={type:UI_Anci.anciType.APTRANSFER,subTotal:0,qty:0,stat:'INA',name:UI_Anci.jsonLabel['lblAptSelection'],rad:'radAPT'};
			if(anciAvail.airportTransferAvailable){
				$("#tblApTransfer").show();
				UI_Anci.loadAirportTransferData(true);
				anciSummary[anciSummary.length-1].stat = 'ACT';
				anciStatus  = true;
			}
			
			
			var anciRadCheck = function(radId){
				$('input[name="'+radId+'"]').each(function(index, item){
					if($(item).val() == 'Y'){
						$(item).attr('checked',true);						
					}
					
				});
			}
			
			var ancRadNo = function(radId){
				$('input[name="'+radId+'"]').each(function(index, item){
					if($(item).val() == 'N'){
						$(item).attr('checked',true);						
					}
					
				});
			}
			
			var isSelectedSSR = function(paxWiseAnci){
				if(paxWiseAnci != null){
					for(var i = 0  ; i < paxWiseAnci.length ;i ++){
						for(var j = 0  ; j < paxWiseAnci[i].selectedAncillaries.length ;j ++){
							if(paxWiseAnci[i].selectedAncillaries[j].specialServiceRequestDTOs.length > 0){
								return true;
							}
						}					
					}					
				}
			}

			for(var i = 0  ; i < anciSummary.length ;i ++){
				var isSSRSelected = isSelectedSSR(response.paxWiseAnci);
				if(anciSummary[i].stat == 'ACT' && anciSummary[i].rad!=null){
					if(!(UI_Container.isModifyAncillary() && anciSummary.length ==1)
							&& anciSummary[i].type == UI_Anci.anciType.SSR && !response.showSSRByDefault && !isSSRSelected){
						ancRadNo(anciSummary[i].rad);
					}else{
						anciRadCheck(anciSummary[i].rad);
					}
				}
			}
			
			
			
			$('a[name="lnkHome"]').click(function (){UI_Anci.startOverClick();});
			
			$("#btnCancel").click(function(){UI_Anci.startOverClick();});
			
			
			$("#btnMealAddNow").enable();
			
			$("#btnMealAddNow").click(function(){
				anciRadCheck(UI_Anci.getAncillarySummary(UI_Anci.anciType.MEAL).rad);
				UI_Anci.loadMealData(loadSync);			
			});
			
			$("#btnViewSkyCafe").click(function(){UI_Anci.viewCafeMenu();});
			
			$("#btnSeatAddNow").enable();
			$("#btnSeatAddNow").click(function(){
				anciRadCheck(UI_Anci.getAncillarySummary(UI_Anci.anciType.SEAT).rad);
				UI_Anci.loadSeatData(loadSync);
			});
			$("#spnSeatInfo").hide();
			$("#spnSocial").hide();
			
			$("#btnSSRAddNow").enable();
			$("#btnSSRAddNow").click(function(){
				anciRadCheck(UI_Anci.getAncillarySummary(UI_Anci.anciType.SSR).rad);
				UI_Anci.loadSSRData(loadSync);
			});

			$("#btnHala").enable();
			$("#btnHala").click(function(){UI_Anci.viewHalaInfo();});
			
			if (UI_Anci.insViewType == "New-view"){
				$("input[name='radAnciIns']").click(function(){
						UI_Anci.insRadioOnClick(this.id);
				});
				$('#lblInsuranceTNC').find('a').addClass('fntDefault').addClass('fntBold');
				$('#lblInsuranceTNC').find('a').click(function (){UI_Anci.showPopupWindow('Terms and Conditions',UI_Anci.anciInsurance.insCondLink); return false;});
				$('#insCoverInfo5').click(function (){UI_Anci.showPopupWindow('Terms and Conditions',UI_Anci.anciInsurance.insCondLink); return false;});
			} else if (UI_Anci.insViewType == 'New-multiOption-view'){
				$("input[name='radAnciIns']").click(function(){
						UI_Anci.insRadioOnClick(this.id);
				});
				$("#lblInsuranceTNC_cancelation_link").click(function (){UI_Anci.showPopupWindow('Terms and Conditions','showLoadPage!loadInsuranceTermsCon.action?insType=CNX'); return false;});
				$("#lblInsuranceTNC_multirisk_link").click(function (){UI_Anci.showPopupWindow('Terms and Conditions','showLoadPage!loadInsuranceTermsCon.action?insType=MLTRISK'); return false;});
				$("#lblInsuranceTNC_cancelation_link").addClass('fntDefault').addClass('fntBold');
				$("#lblInsuranceTNC_multirisk_link").addClass('fntDefault').addClass('fntBold');
			}else {
				$("#chkAnciIns").click(function() { $("#chkAnciIns").blur();UI_Anci.insOnClick(); });
				$('#lblInsuranceDesc').find('a').addClass('fntDefault');
				$('#lblInsuranceDesc').find('a').click(function (){UI_Anci.showPopupWindow('Insurance Info',UI_Anci.anciInsurance.insLink); return false;});
				$('#lblInsuranceTNC').find('a').addClass('fntDefault').addClass('fntBold');
				$('#lblInsuranceTNC').find('a').click(function (){UI_Anci.showPopupWindow('Terms and Conditions',UI_Anci.anciInsurance.insCondLink); return false;});
			}

			// show hide the pax booked seat legend
			if(UI_Container.isModifyAncillary()){
				$('.tdOldSeatLegend').show();
			}else{
				$('.tdOldSeatLegend').hide();
			}
			
			var defaultInsSelection = Number(response.insuranceSelectedByDefault);
			if(anciAvail.insuranceAvailable && defaultInsSelection == 1 && UI_Anci.insViewType != 'New-multiOption-view'){
				if (UI_Anci.insViewType != "New-view"){
					$("#chkAnciIns").attr('checked',true);
					UI_Anci.insOnClick();
				} else {
					$("#radAnciIns_Y").attr('checked',true);
					UI_Anci.insRadioOnClick('radAnciIns_Y');
				}
			} else if(anciAvail.insuranceAvailable  && UI_Anci.insViewType == 'New-multiOption-view'){
				switch(defaultInsSelection) {					
					case 2: $("#radAnciIns_C").attr('checked',true);UI_Anci.insRadioOnClick("radAnciIns_C"); break;
					case 3: $("#radAnciIns_M").attr('checked',true);UI_Anci.insRadioOnClick("radAnciIns_M"); break;
					default : $("#radAnciIns_N").attr('checked',true);UI_Anci.insRadioOnClick("radAnciIns_N"); break;
				}
				
			} else if(anciAvail.insuranceAvailable){
				//Insurance default selection
				if (defaultInsSelection == -1) {
					$("#radAnciIns_N").attr('checked',false);
				} else if (defaultInsSelection == 0) {
					//$("#radAnciIns_N").attr('checked',true);
				}				
				//UI_Anci.insRadioOnClick("radAnciIns_N");
			}
			
			UI_Anci.updatePriceSummary();
			//UI_Anci.trigSegPax();
			UI_Anci.trigSegBaggagePax();
			//UI_Anci.trigSegHalaPax();
			if(!anciStatus){
				UI_Anci.noAncillary = true;
				if(UI_Anci.direction == 'next'){
					$("#tblMainTrNoAnciMsg").show();					
				}
				setTimeout('UI_Anci.handleNavigation()',5000);	
				
			} else {
				$("#tblMainTrNoAnciMsg").hide();
				$("#tblMainTrSelectAnciText").show();
			}		
			if(UI_Anci.notAllAnciReprotect){
				jAlert(UI_Anci.jsonLabel['msgReprotectFailed'],'Ancillary reprotect');
			}
			if(UI_Anci.isFreeSeatEnableForReservation()){
				$("#radSeat_N").disable();
			}
			
			
			if(UI_Anci.isFreeMealEnableForReservation()){
				$("#radMeal_N").disable();
			}
			
			for (var prop in UI_Anci.jsAnciFlexiAdded) {
				if(UI_Anci.jsAnciFlexiAdded.hasOwnProperty(prop)){
			        if (UI_Anci.jsAnciFlexiAdded[prop]){
			        	$("#radFlexi_N, #radFlexi_Y").disable();
			        	break;
			        }
			    }
			}
			UI_Anci.manipulateFlexiSelectionParam();

            UI_Anci.paxWiseAnci = response.paxWiseAnci;


            UI_Anci.ancillaryModifiability = response.ancillaryModifiability;

            UI_Anci.populateSelectedSeats();

            UI_Anci.populateSelectedMeals() ;

            UI_Anci.populateSelectedHala() ;

            UI_Anci.populateSelectedBaggage() ;
            
            if((UI_Container.isModifyAncillary() || UI_Container.isModifySegment()) && UI_Anci.preferredMeals != null){
            	UI_Anci.setSelectedMeals(UI_Anci.preferredMeals);
            }
            
            if(UI_Container.isModifyAncillary() || UI_Container.isModifySegment()){
            	var hasSeatsSelected = false;
				$.each(UI_Anci.jsonPaxAdults[0].currentAncillaries, function(i,j){
					if(j.airSeatDTO != null | j.airSeatDTO != undefined){
						hasSeatsSelected = true;
					}
				});
				
				if(!hasSeatsSelected){
					UI_Anci.setPreferredSeatType();
				}
            }

		}else{
			//jAlert(UI_Container.labels.lblAnciPageLoadFailAlert, 'Alert');
			//UI_commonSystem.setErrorStatus();		
			if (response != null)  {
				UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
			} else {
				UI_commonSystem.setErrorStatus();		
			}
		}
		
	}
	
	
	UI_Anci.seatReprotect = function(){
		var notAllocSeats = [];
		
		for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
			var currAnci = UI_Anci.jsonPaxAdults[i].currentAncillaries;
			var segId = 0;
			for(var k = 0 ; k < currAnci.length; k++){				
				if(!UI_Container.isOneOfModifyingSeg(currAnci[k].flightSegmentTO.flightRefNumber) || UI_Anci.jsSMModel[segId]== null){
					continue;
				}
				var seatDTO = currAnci[k].airSeatDTO;
				var selFltSeg = UI_Anci.jsSMModel[segId].flightSegmentDTO;
				if(seatDTO!=null && selFltSeg.logicalCabinClassCode == seatDTO.logicalCabinClassCode){
					var fltRefNo = selFltSeg.flightRefNumber;
					var paxType = UI_Anci.jsonPaxAdults[i].paxType;
					var refObj = {fltRefNo: fltRefNo, seatNumber: seatDTO.seatNumber};
					var seatObj = UI_Anci.getSeatObj(refObj);
					
					var seatMapObj =  UI_Anci.getSeatObj(refObj);
					
					if(seatMapObj != null && seatMapObj.avail == 'RES'){
					// previously selected seat is not available. cannot reprotect
						UI_Anci.isSeatReprotected[i] = false;
					}
					
					if(!UI_Anci.isSeatOccupied(refObj) && seatMapObj != null && seatMapObj.avail == 'VAC' ){
						if(UI_Anci.paxHasInfant(i) && !UI_Anci.canSeatInfant(refObj, i, segId)){
							notAllocSeats[notAllocSeats.length] = {paxId: i, seat: seatDTO.seatNumber};
						}else{
							var mObj= {paxId : i, paxType: paxType, refObj:refObj, segId: segId};
							UI_Anci.markSeat(mObj);
							UI_Anci.isSeatReprotected[i] = true;
						}
					}else{
						notAllocSeats[notAllocSeats.length] = {paxId: i, seat: seatDTO.seatNumber};
					}
					
				}
				segId ++;
			}
		}
		if(notAllocSeats.length>0){
			UI_Anci.notAllAnciReprotect = true;
		}
	}
	
	/*
	 * Build meal pax
	 */
	UI_Anci.buildMealPax =function(){
		UI_Anci.meal.currentPaxId = 0;
		
		var templateName = '#paxMealTemplate';
		
		for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
			var tmpltId = 'mealPaxTmplt_'+i;
			var clone = $('#paxMealTemplate').clone();
			clone.attr('id', tmpltId );
			clone.appendTo($('#paxMealTemplate').parent());  			
			
			var pax = $('#'+tmpltId+' label[name="paxName"]');
			pax.attr('id','mealPax_Name_'+i);			
			var infType = '';
			if(UI_Anci.jsonPaxAdults[i].paxType == 'PA'){
				infType = UI_Anci.infantTypeSuffix;
			}
			pax.text(UI_Anci.jsonPaxAdults[i].name+infType);
			
			var sel = $('#'+tmpltId+' select[name="mealList"]');
			sel.attr('id','mealPax_MealList_'+i);
			sel.change(function (e){ UI_Anci.mealPaxChanged(e);})
			$('#'+tmpltId+' label[name="mealCharge"]').attr('id','mealPax_MealCharge_'+i);
		}

		$('#paxMealTemplate').hide();
		if (UI_Anci.mealViewType != "New-view"){
			$("#tblMealSel tr:first td:last").remove();
			$("#tblMealSel tr:first td:first").attr("width","100%");
		}
	}
	
	/*
	 * Build ssr pax
	 */
	UI_Anci.buildSSRPax =function(){
		UI_Anci.ssr.currentPaxId = 0;
		
		var templateName = '#paxSSRTemplate';
		
		for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
			var tmpltId = 'ssrPaxTmplt_'+i;
			var clone = $('#paxSSRTemplate').clone();
			clone.attr('id', tmpltId );
			clone.appendTo($('#paxSSRTemplate').parent());  			
			
			var pax = $('#'+tmpltId+' label[name="paxName"]');
			pax.attr('id','ssrPax_Name_'+i);	
			var infType = '';
			if(UI_Anci.jsonPaxAdults[i].paxType == 'PA'){
				infType = UI_Anci.infantTypeSuffix;
			}
			pax.text(UI_Anci.jsonPaxAdults[i].name+infType);
			
			var sel = $('#'+tmpltId+' select[name="ssrList"]');
			sel.attr('id','ssrPax_SSRList_'+i);
			sel.change(function (e){ UI_Anci.ssrPaxChanged(e);})
			var ssrInput = $('#'+tmpltId+' input[name="ssrText"]');
			ssrInput.attr('id','ssrPax_SSRText_'+i);
			ssrInput.keyup(function(e){
				var target = e.target;	
				var value = $.trim(target.value);
				if (!/^[a-zA-Z ]*$/.test(value)) {
					value  = value.replace(/[^a-zA-Z0-9 ]/g,"");
					target.value = value;
				}
			});
			ssrInput.change(function (e){ UI_Anci.ssrTextChanged(e);})
			
			ssrInput.blur(function(e){
				var target = e.target;	
				var arr = target.id.split('_');
				var paxId = parseInt(arr[2],10);
				var val = parseInt($('#ssrPax_SSRList_'+paxId).val(),10);
				if(val>=0){
					UI_Anci.updateSSRTextValue(paxId,target.value);
				}
				
			});
		}

		$('#paxSSRTemplate').hide();
	}

	function clearSeatPaxStyle(paxId, parentId,segid){
		$(".segs-full").find('label,span').each(function (i){
			$(this).removeClass('fntRed');	
			$(this).addClass('fntGrey');	
		});
	}
	function clearBaggagePaxStyle(paxId, parentId,segid){
		$(".segs-full-baggage").find('label,span').each(function (i){
			$(this).removeClass('fntRed');	
			$(this).addClass('fntGrey');	
		});
	}
	function addSeatPaxStyle(paxId, parentId, segid){
		var tmpltId = $('#'+parentId).find('#seatPaxTmplt_'+segid+'_'+paxId);
		$(tmpltId).find('label,span').each(function (i){
			$(this).removeClass('fntGrey');	
			$(this).addClass('fntRed');	
		});
	}
	function addBaggagePaxStyle(paxId, parentId, segid){
		var tmpltId = $('#'+parentId).find('#baggagePaxTmplt_'+segid+'_'+paxId);
		$(tmpltId).find('label,span').each(function (i){
			$(this).removeClass('fntGrey');	
			$(this).addClass('fntRed');	
		});
	}
	function getIndex(obj,id){
		var r = 0;
		for (var i = 0; i < obj.length;i++){
			if (obj[i].id == id){
				r = i;
				break;
			}
		}
		return r;
	};

	/*
	 * Style the selected pax row
	 */
	UI_Anci.seatMarkSelectedPax = function(evt){
		if (UI_Anci.seatViewType != "New-view"){
			var arrData= evt.target.id.split("_");
			var id = parseInt(arrData[1],10);
			if(UI_Anci.seat.currentPaxId >=0 ){
				clearSeatPaxStyle(UI_Anci.seat.currentPaxId,null,null);
			}
			UI_Anci.seat.currentPaxId = id;
			addSeatPaxStyle(id,null,null);
		}else{
			UI_Anci.currentSegPax = getIndex($("input[name='radSeatPax']"),evt.target.id);
			var arrData1 = evt.target.id.split(":");
			UI_Anci.highlightSAegment(arrData1[1]);
			var tm = arrData1[1].split("_")
			var arrData = arrData1[0].split("-");
			var id = parseInt(arrData[1],10);
			if(UI_Anci.seat.currentPaxId >=0 ){
				for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
					clearSeatPaxStyle(i,arrData1[1],parseInt(tm[1],10));
				}
			}
			UI_Anci.seat.currentPaxId = id;
			addSeatPaxStyle(id,arrData1[1],parseInt(tm[1],10));
			var myArrData = arrData1[1].split("_");
			UI_Anci.clickSeatSegment({ind:parseInt(myArrData[1],10)});
		}
		UI_SocialSeat.markSelectedPax();
	}
	/*
	 * Style the selected pax row
	 */
	UI_Anci.baggageMarkSelectedPax = function(evt){
		if (UI_Anci.baggageViewType != "New-view"){
			var arrData= evt.target.id.split("_");
			var id = parseInt(arrData[1],10);
			if(UI_Anci.baggage.currentPaxId >=0 ){
				clearBaggagePaxStyle(UI_Anci.baggage.currentPaxId,null,null);
			}
			UI_Anci.baggage.currentPaxId = id;
			addBaggagePaxStyle(id,null,null);
		}else{
			UI_Anci.currentBaggageSegPax = getIndex($("input[name='radBaggagePax']"),evt.target.id);
			var arrData1 = evt.target.id.split(":");
			UI_Anci.highlightBaggageSegment(arrData1[1]);
			var tm = arrData1[1].split("_")
			var arrData = arrData1[0].split("-");
			var id = parseInt(arrData[1],10);
			if(UI_Anci.baggage.currentPaxId >=0 ){
				for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
					clearBaggagePaxStyle(i,arrData1[1],parseInt(tm[1],10));
				}
			}
			UI_Anci.baggage.currentPaxId = id;
			addBaggagePaxStyle(id,arrData1[1],parseInt(tm[1],10));
			var myArrData = arrData1[1].split("_");
			UI_Anci.clickBaggageSegment({ind:parseInt(myArrData[1],10)});
		}
	}
	UI_Anci.highlightSAegment = function(segmentID){
		$(".segs-full").removeClass("active");
		$("#"+segmentID).addClass("active");
	}
	UI_Anci.highlightBaggageSegment = function(segmentID){
		$(".segs-full-baggage").removeClass("active");
		$("#"+segmentID).addClass("active");
	}
	/*
	 * Build seat pax
	 */
	UI_Anci.buildSeatPax = function(seg){
		if (UI_Anci.seatViewType != "New-view"){
			for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
				var seatRef ='{paxId:'+i+'}';
				var tmpltId = 'seatPaxTmplt_'+i;
				var clone = $('#seatPaxTemplate').clone();
				clone.attr('id', tmpltId );
				clone.appendTo($('#seatPaxTemplate').parent());  
				
				var ch = $('#'+tmpltId+' input[name="radSeatPax"]');
				ch.attr('id','radSeatPax_'+i);
				ch.click(function (evt){UI_Anci.seatMarkSelectedPax(evt);});
				var pax = $('#'+tmpltId+' label[name="paxName"]');
				pax.attr('id','seatPax_Name_'+i);			
				var infType = '';
				if(UI_Anci.jsonPaxAdults[i].paxType == 'PA'){
					infType = UI_Anci.infantTypeSuffix;
				}
				pax.text(UI_Anci.jsonPaxAdults[i].name+infType);
				$('#'+tmpltId+' label[name="seatCharge"]').attr('id','seatPax_Charge_'+i);
				$('#'+tmpltId+' label[name="seatNo"]').attr('id','seatPax_SeatNo_'+i);
				var clr = $('#'+tmpltId+' span[name="clear"]');
				clr.attr('id','seatPax_Clear_'+i);
				clr.html("<a href='#' style='display:none;' class='noTextDec' onclick='UI_Anci.removeSeatPax(" + seatRef +  ");  return false;'>"+UI_Anci.jsonLabel['lblSeatClear']+"</a>");
				
			}
			$('#seatPaxTemplate').hide();
		}else{
			for (var k = 0 ;  k < seg.length; k++){
				for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
					var tmpltId = 'seatPaxTmplt_'+seg[k].segInd+'_'+i;
					var ch = $('#'+tmpltId+' input[name="radSeatPax"]');
					ch.click(function (evt){UI_Anci.seatMarkSelectedPax(evt);});
					var pax = $('#'+tmpltId+' label[name="paxName"]');
					pax.attr('id','seatPax_Name_'+i);		
					var infType = '';
					if(UI_Anci.jsonPaxAdults[i].paxType == 'PA'){
						infType = UI_Anci.infantTypeSuffix;
					}
					pax.text(UI_Anci.jsonPaxAdults[i].name+infType);
					$('#'+tmpltId+' label[name="seatCharge"]').attr('id','seatPax_Charge_'+i);
					$('#'+tmpltId+' label[name="seatNo"]').attr('id','seatPax_SeatNo_'+i);
					var clr = $('#'+tmpltId+' span[name="clear"]');
					clr.attr('id','seatPax_Clear_'+i);
					if(UI_Container.isModifySegment() && UI_Anci.jsAnciSegModel[k].pax[i].seat!= null &&
							UI_Anci.jsAnciSegModel[k].pax[i].seat.seatNumber != null && 
							UI_Anci.jsAnciSegModel[k].pax[i].seat.seatNumber != ''){
						clr.children("a").css("display","block");
					}else{
						clr.children("a").css("display","none");
					}
				}
			}
		}
	
	}
	
	/*
	 * Build baggage pax
	 */
	UI_Anci.buildBaggagePax = function(seg){
		if (UI_Anci.baggageViewType != "New-view"){
			for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
				var seatRef ='{paxId:'+i+'}';
				var tmpltId = 'baggagePaxTmplt_'+i;
				var clone = $('#baggagePaxTemplate').clone();
				clone.attr('id', tmpltId );
				clone.appendTo($('#baggagePaxTemplate').parent());  
				
				var ch = $('#'+tmpltId+' input[name="radBaggagePax"]');
				ch.attr('id','radBaggagePax_'+i);
				ch.click(function (evt){UI_Anci.baggageMarkSelectedPax(evt);});
				var pax = $('#'+tmpltId+' label[name="paxName"]');
				pax.attr('id','baggagePax_Name_'+i);			
				var infType = '';
				if(UI_Anci.jsonPaxAdults[i].paxType == 'PA'){
					infType = UI_Anci.infantTypeSuffix;
				}
				pax.text(UI_Anci.jsonPaxAdults[i].name+infType);

				var sel = $('#'+tmpltId+' select[name="selBaggageType"]');
				sel.attr('id','selBaggageType_'+i);
				sel.change(function (e){ UI_Anci.baggagePaxChanged(e);});

				$('#'+tmpltId+' label[name="baggageCharge"]').attr('id','baggagePax_Charge_'+i);
				$('#'+tmpltId+' label[name="baggageNo"]').attr('id','baggagePax_SeatNo_'+i);
				var clr = $('#'+tmpltId+' span[name="clear"]');
				clr.attr('id','baggagePax_Clear_'+i);
				clr.html("<a href='#' style='display:none;' class='noTextDec clearLabel' onclick='UI_Anci.removeSeatPax(" + seatRef +  ");  return false;' title='"+UI_Anci.jsonLabel['lblSeatClear']+"'>&nbsp;</a>");
				
			}
			
		}else{
			for (var k = 0 ;  k < seg.length; k++){
                var correctBaggageSedId = UI_Anci.getBaggageSegIndex(seg[k].segInd);

                if (correctBaggageSedId != null) {

                    for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){
                        var tmpltId = 'baggagePaxTmplt_'+seg[k].segInd+'_'+i;
                        var ch = $('#'+tmpltId+' input[name="radBaggagePax"]');
                        ch.click(function (evt){UI_Anci.baggageMarkSelectedPax(evt);});
                        if (UI_Anci.jsonPaxAdults.length == 1) {
                            ch.hide();
                        }
                        var sel = $('#'+tmpltId+' select[name="selBaggageType"]');
                        sel.attr('id','selBaggageType_'+ seg[k].segInd +'_' + i);
                        sel.change(function (e){ UI_Anci.baggagePaxChanged(e);});
                        if((UI_Container.isModifyAncillary() || UI_Container.isModifySegment()) && !UI_Anci.isPreviousBookingWithNoBaggages){
                            var lab = $('#'+tmpltId+' label[name="baggageType"]');
                            lab.attr('id','baggageType_'+ seg[k].segInd +'_' + i);
                            for(var ind=0; ind< UI_Anci.jsBaggageModel[correctBaggageSedId].baggages.length; ind++){
                                var baggage = UI_Anci.jsBaggageModel[correctBaggageSedId].baggages[ind];
                                $("#selBaggageType_" + seg[k].segInd + "_" +i ).append($("<option></option>").
                                attr("value",ind).
                                text(baggage.baggageDescription));

                            }
                            if(UI_Container.isModifySegment() && UI_Anci.selectedBaggages != null && UI_Anci.selectedBaggages.length > 0){
                                if(UI_Anci.selectedBaggages[k] != undefined) {
                                    $('#selBaggageType_'+ seg[k].segInd + '_'+ i).val(UI_Anci.selectedBaggages[k][i]);
                                }
                            }
                        }
                        var pax = $('#'+tmpltId+' label[name="paxName"]');
                        pax.attr('id','baggagePax_Name_'+i);
                        var infType = '';
                        if(UI_Anci.jsonPaxAdults[i].paxType == 'PA'){
                            infType = UI_Anci.infantTypeSuffix;
                        }
                        pax.text(UI_Anci.jsonPaxAdults[i].name+infType);
                        $('#'+tmpltId+' label[name="baggageCharge"]').attr('id','baggagePax_Charge_'+ seg[k].segInd + '_' +i);
                        $('#'+tmpltId+' label[name="baggageNo"]').attr('id','baggagePax_SeatNo_'+ seg[k].segInd + '_'+i);
                        var clr = $('#'+tmpltId+' span[name="clear"]');
                        clr.attr('id','baggagePax_Clear_'+ seg[k].segInd + '_'+i);
                        if(UI_Container.isModifySegment() && UI_Anci.jsAnciSegModel[k].pax[i].baggage!= null &&
                                UI_Anci.jsAnciSegModel[k].pax[i].baggage.baggageName != null &&
                                UI_Anci.jsAnciSegModel[k].pax[i].baggage.baggageName != ''){
                            clr.children("a").css("display","block");
                        }else{
                            clr.children("a").css("display","none");
                        }


                    }
                }
			}
			
		}
		
		
	}
	//Trigger pax
	var globalBaggageSegID = 0;
	UI_Anci.trigSegBaggagePax = function (){
		var radArra = $("input[name='radBaggagePax']");
		for (var i=UI_Anci.currentBaggageSegPax; i<UI_Anci.segmentPaxBaggageUsedArray.length; i++){
			if (UI_Anci.segmentPaxBaggageUsedArray[i].baggageUsed == false){
				globalBaggageSegID = UI_Anci.segmentPaxBaggageUsedArray[i].baggageSegId;
				var pid = UI_Anci.segmentPaxBaggageUsedArray[i].pax.paxId;
				$(radArra[i]).trigger('click');
				$("#selBaggageType_" + i + "_" +pid ).enable();
				break;
			}
		}
	}
	
	

	//Trigger pax
	//var globalHalaSegID = 0;
	/*UI_Anci.trigSegHalaPax = function (){
		var radArra = $("input[name='radHalaPax']");
		for (var i=UI_Anci.currentalaSegPax; i<UI_Anci.segmentPaxHalaUsedArray.length; i++){
			if (UI_Anci.segmentPaxHalaUsedArray[i].baggageUsed == false){
				globalBaggageSegID = UI_Anci.segmentPaxHalaUsedArray[i].halaSegId;
				var pid = UI_Anci.segmentPaxHalaUsedArray[i].pax.paxId;
				$(radArra[i]).trigger('click');
				$("#selHalaType_" + i + "_" +pid ).enable();
				break;
			}
		}
	}*/
	
	UI_Anci.showLoadingMsg = function(type){
		if(type == UI_Anci.anciType.SEAT){
			UI_Anci.loadingInProgress.seat = true;
		}else if (type == UI_Anci.anciType.MEAL){
			UI_Anci.loadingInProgress.meal = true;
		}else if (type == UI_Anci.anciType.SSR){
			UI_Anci.loadingInProgress.ssr = true;
		}else if (type == UI_Anci.anciType.INS){
			UI_Anci.loadingInProgress.insurance = true;
		}else if (type == UI_Anci.anciType.BAGGAGE){
			UI_Anci.loadingInProgress.baggage = true;
		}else if (type == UI_Anci.anciType.APTRANSFER){
			UI_Anci.loadingInProgress.apt = true;
		} else{
			UI_Anci.loadingInProgress.other = true;
		}
		
		UI_Container.showLoading();
	}
	/*
	 * Load seat map data
	 */
	UI_Anci.loadSeatData = function(sync){
		$("#tblSeat").show();
//		$("#tbdySeatSeg").hide();
		//UI_commonSystem.sectionLoaderShow("#tblSeatMap","#seatmapLoader");
		UI_Anci.showLoadingMsg(UI_Anci.anciType.SEAT);
		var data = {};
		UI_Anci.selectDefaultSeatsForPax = true; 
		$("#frmReservation").ajaxSubmit({dataType: 'json',url:'interlineAnciSeatMap.action', processData: false, async:!sync,	data:data,		
				success: UI_Anci.onSeatMapLoadSuccess, error:UI_commonSystem.setErrorStatus});
	}
	UI_Anci.hideLoadingMsg = function(type){
		if(type == UI_Anci.anciType.SEAT){
			UI_Anci.loadingInProgress.seat = false;
		}else if (type == UI_Anci.anciType.MEAL){
			UI_Anci.loadingInProgress.meal = false;
		}else if (type == UI_Anci.anciType.SSR){
			UI_Anci.loadingInProgress.ssr = false;
		}else if (type == UI_Anci.anciType.INS){
			UI_Anci.loadingInProgress.insurance = false;
		}else if (type == UI_Anci.anciType.BAGGAGE){
			UI_Anci.loadingInProgress.baggage = false;
		}else if(type == UI_Anci.anciType.APTRANSFER){
			UI_Anci.loadingInProgress.apt = false;
		}else{
			UI_Anci.loadingInProgress.other = false;
		}
		var loading = UI_Anci.loadingInProgress;
		if(!loading.seat && !loading.meal && !loading.ssr && !loading.insurance && !loading.apt && !loading.other ){
			UI_Container.hideLoading();
		}
	}
	/*
	 * On successful seat map loading..
	 */
	UI_Anci.onSeatMapLoadSuccess = function(response){
		
		if(response!=null){
			UI_Anci.jsSMModel = response.seatMapDTO.lccSegmentSeatMapDTOs;	
			UI_Anci.jsSMModel = $.airutil.sort.quickSort(UI_Anci.jsSMModel,segmentDTOComparator);
			if(UI_Container.isModifySegment()){
				UI_Anci.seatReprotect();
			}
			UI_Anci.viewSeatInfo();			
			if(typeof UI_Top.holder().GLOBALS != "undefined" && UI_Top.holder().GLOBALS.autoSeatAssignmentEnabled){				
				if(!UI_Container.isModifyAncillary() && !UI_Container.isModifySegment()){
					UI_Anci.setDefaultSeatSelection();
				}
			}
			
			UI_Anci.updateSeatChargeInView();
			
			//UI_commonSystem.sectionLoaderHide("#tblSeatMap","#seatmapLoader")
			$("#tbdySeatSeg").show();
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.SEAT);
			
			UI_SocialSeat.populateProfiles();
			
			$(".removeallSeats").unbind("click").bind("click", function(){
				UI_Anci.removeAllSelectedSeats();
			});
			UI_Anci.searPromoOffer(response);
			
			UI_Anci.preferredSeatType = response.seatType;
			
			if(UI_Container.isRegisteredUser & (UI_Anci.jsonPaxAdults.length == 1) & !UI_Container.isModifyAncillary() 
					& !UI_Container.isModifySegment()){
				UI_Anci.setPreferredSeatType();
			}
			
		}else{			
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.SEAT);
			jConfirm('Error occured while loading Seats', 'Ok', function(){});
		}
	}
	
	UI_Anci.searPromoOffer = function(p){
		if(p.anciOffer){
			//anciOfferName
			$(".seatPromoOffer").html(p.anciOfferDescription);
		}else{
			$(".seatPromoOffer").parent().parent().hide();
		}
	}
	
	
	/*
	 * Seat Info
	 */
	UI_Anci.viewSeatInfo = function(){
		if (UI_Anci.seatViewType != "New-view")
			var segId = UI_Anci.jsAnciAvail.seats[0].segInd;
		else
			var segId = UI_Anci.jsAnciAvail.seats[globalSegID].segInd;
		
		UI_Anci.clickSeatSegment({ind:segId});
	}
	/*
	 * Baggage Info
	 */
	UI_Anci.viewBaggageInfo = function(){
        var matchingSegIds;
		if (UI_Anci.baggageViewType != "New-view")
			var segId = UI_Anci.jsAnciAvail.baggages[0].segInd;
		else
			var segId = UI_Anci.jsAnciAvail.baggages[globalBaggageSegID].segInd;
		if((!UI_Container.isModifyAncillary() && !UI_Container.isModifySegment()) || UI_Anci.isPreviousBookingWithNoBaggages){
			var j =0;
			var k =0;
			var availableSegs = UI_Anci.jsAnciAvail.baggages;
            var defaultBaggageSelected;
			while (j < UI_Anci.jsAnciSeg.length){
				if (availableSegs[k] != undefined){
					if (availableSegs[k].segInd == j){
                        matchingSegIds = UI_Anci.getMatchingIOSegIds(j);
						for(var ind=0; ind< UI_Anci.jsBaggageModel[k].baggages.length; ind++){
							var baggage = UI_Anci.jsBaggageModel[k].baggages[ind];
							for (var i = 0 ;  i < UI_Anci.jsonPaxAdults.length; i++ ){				
								$("#selBaggageType_" + j + "_" + i ).append($("<option></option>").
								attr("value",ind).
								text(baggage.baggageDescription));								
														
								if(!UI_Container.isModifySegment() || UI_Anci.isPreviousBookingWithNoBaggages){

                                    if (baggage.defaultBaggage == 'Y' || ind == 0) {
                                        if (matchingSegIds[0] == j) {
                                            UI_Anci.paxBaggageRemove(j, i, true);
                                        } else {
                                            UI_Anci.paxBaggageRemove(j, i, false);
                                        }
                                        $('#selBaggageType_'+ j+ '_'+ i).val(ind);
                                        UI_Anci.paxBaggageAdd(i,ind, j, true);
                                    }
                                }

							}
						}
						k++;
					}
				}
				j++;
			}
		}
		UI_Anci.clickBaggageSegment({ind:segId});
	}
	/*
	 * Load meal data
	 */
	UI_Anci.loadMealData = function(sync){
		UI_Anci.showLoadingMsg(UI_Anci.anciType.MEAL);
		var data = {};	
		$("#frmReservation").ajaxSubmit({dataType: 'json',url:'interlineAnciMeal.action', processData: false, async: !sync,	data:data,		
			success: UI_Anci.onMealLoadSuccess, error:UI_commonSystem.setErrorStatus});
	}
	
	/*
	 * On successful meal loading..
	 */
	UI_Anci.onMealLoadSuccess = function(response){
		createMealCatModal = function(arraList){
			var objectList = [];
			for (var i = 0; i < arraList.length; i++){
				var objectCatList = [];
				var arr = [];
				$.each(arraList[i].meals, function(i, meal){
	                if ($.inArray(meal.mealCategoryID, arr) === -1) {
	                	arr.push(meal.mealCategoryID);
	                	var temp = { "catCode": meal.mealCategoryID, "catName": meal.mealCategoryCode };
	                	objectCatList.push(temp);
	                }
				});
				objectList.push(objectCatList);
			}
			return objectList;
		};
		
		if(response!=null){
			UI_Anci.jsMealModel = response.mealResponceDTO.flightSegmentMeals;
			UI_Anci.multimealEnebled = response.mulipleMealsSelectionEnabled;
			UI_Anci.mealCategoryEnebled = response.mealCategoryEnabled;
			UI_Anci.jsMealModel = $.airutil.sort.quickSort(UI_Anci.jsMealModel, segmentComparator);
			UI_Anci.jsMealCategoryModel = createMealCatModal(UI_Anci.jsMealModel);
			UI_Anci.mealPromoOffer(response);
			UI_Anci.viewMealInfo();
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.MEAL);
			
			if(UI_Container.isRegisteredUser && (UI_Anci.jsonPaxAdults.length == 1)){
				if(UI_Container.isModifyAncillary() || UI_Container.isModifySegment()){
					var hasMeals = false;
					$.each(UI_Anci.jsonPaxAdults[0].currentAncillaries, function(i,j){
						if(j.mealDTOs.length != 0){
							hasMeals = true;
						}
					});
					
					if(!hasMeals){
						UI_Anci.preferredMeals = response.preferredMealList;
					}
				}else{
					UI_Anci.setSelectedMeals(response.preferredMealList);
				}
			}
			
		}else{
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.MEAL);
			jConfirm('Error occured while loading Meals', 'Alert', function(){});
		}
	}
	
	/* Auto Select the preferred meals for the customer */
	UI_Anci.setSelectedMeals = function(mealList){
		
		var mealSelected = [];
		var meals;
		var segId = 0;
		var paxId = 0;
		if(UI_Anci.jsonPaxAdults.length == 1){
			
			for(i = 0 ; i < UI_Anci.jsAnciSeg.length ; i++){
				
				if(UI_Anci.jsAnciSeg[i].meal){
					mealSelected = [];
					segId = i;
					var intSegRow = UI_Anci.getMealSegIndex(segId);
					var str = '<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">';
					var isMultiMealEnabled = UI_Anci.isMultiMealEnabledForSegment(segId);
					
					if (mealList != null){
						//meals = mealList.split(",");
						
						$.each(mealList, function(i,customerPreferredMealDTO){
							$.each(UI_Anci.jsMealModel[intSegRow].meals, function(i , mealItem){
								if((mealItem.mealCode == customerPreferredMealDTO.mealCode) && (customerPreferredMealDTO.quantity <= mealItem.availableMeals)){
									data = {};
									data["selectedMeal"] = mealItem;
									data["count"] = customerPreferredMealDTO.quantity;
									data["mealIndex"] = i;
									data["mealCategoryID"] = mealItem.mealCategoryID;
									mealSelected.push(data);
								}
							});
						});
					}
					
					if (mealSelected.length<=0){
						str += '<tr>'+
						'<td class="mealCol defaultRowGap rowColor "><a href="javascript:void(0)" class="selectAici selectMeal" id="meal_'+segId+'_'+paxId+'"><label id="lblSelectMeal">'+UI_Anci.jsonLabel["lblSelectMeal"]+'</label></a></td>'+
						'<td class="priceCol defaultRowGap rowColor " align="center"><label id="price_'+segId+'_'+paxId+'"> - </label></td>'+
						'<td class="clearCol defaultRowGap rowColor " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_'+segId+'_'+paxId+'" style="display:none" title="'+UI_Anci.jsonLabel["lblSeatClear"]+'"></a></td>'+
						'</tr>';
					}else{
						var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.MEALS, null, UI_Anci.getOndSequence(intSegRow));
						
						$.each(mealSelected, function(i,j){
							var selectedMealItem = UI_Anci.jsMealModel[intSegRow].meals[j.mealIndex];
							var catId = j.selectedMeal.mealCategoryID;
							var qty = 1;
							if (isMultiMealEnabled){
								qty = j.count;
							} else if(catId == '-'){
								catId = selectedMealItem.mealCategoryID;
							}
							var mealCharge = UI_Anci.convertAmountToSelectedCurrency(selectedMealItem.mealCharge * qty);
							var applyFormatter = UI_Anci.isApplyDecorator(mealCharge,isFreeService);
			        		if(applyFormatter){
			        			mealCharge = UI_Anci.freeServiceDecorator;	
			        		}		
							
							if (isMultiMealEnabled){
								str += '<tr><td class="mealCol defaultRowGap rowColor ">'+
										'<a id="meal_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="selectAici selectMeal" href="javascript:void(0)"><label id="lblSelectMeal">'+qty+'x'+selectedMealItem.mealName+'</label></a>'+
										'</td><td align="center" class="priceCol defaultRowGap rowColor  "><label>'+mealCharge+'</label></td>'+
										'<td align="center" class="clearCol defaultRowGap rowColor  ">'+
										'<a title="Clear" style="display: block;" id="clear_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>'+
										'</td></tr>';
							}else{
								str += '<tr><td class="mealCol defaultRowGap rowColor ">'+
								'<a id="meal_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="selectAici selectMeal" href="javascript:void(0)"><label id="lblSelectMeal">'+selectedMealItem.mealName+'</label></a>'+
								'</td><td align="center" class="priceCol defaultRowGap rowColor  "><label>'+mealCharge+'</label></td>'+
								'<td align="center" class="clearCol defaultRowGap rowColor  ">'+
								'<a title="Clear" style="display: block;" id="clear_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>'+
								'</td></tr>';
							}
						});
					}
					str += '</table>';
					$("#multimeal_"+segId+'_'+paxId).html(str);
					
					UI_Anci.meal.currentPaxId = paxId;
					
					UI_Anci.meal.currentSegId = segId;
					
					if(UI_Container.isModifySegment() | UI_Container.isModifyAncillary()){
						UI_Anci.paxWiseAnci = null;
					}
					
					UI_Anci.mealPaxChanged({paxID:paxId,segID:segId,slMeals:mealSelected});
				}
					
			}
		
		}
		
	}
	
	/*
	* Meal Promotion info
	*/
	UI_Anci.mealPromoOffer = function(p){
		if (p.anciOffer){
			//anciOfferName
			$(".mealOfferDiv").html(p.anciOfferDescription);
		}else{
			$(".mealOfferDiv").hide();
		}
	}
	
	/*
	 * Meal Info
	 */
	UI_Anci.viewMealInfo = function(){
		if (UI_Anci.mealViewType != "New-view"){
			$("#tblMealSel").show(); //change to hide() to show()
			$("#tblMealDetails").show();
			var segId = UI_Anci.jsAnciAvail.meals[0].segInd;
			UI_Anci.clickMealSegment({ind:segId});
		}else{//new one should load after the meal data is loaded
			UI_Anci.buildSegmentsMealTab({id:"tbdyMealSeg", tdID:"tdMealSeg_", click:null, segs: UI_Anci.jsAnciAvail.meals});
			UI_Anci.buildMealPax();
		}
	}
	
	/*
	 * Cafe Menu
	 */
	UI_Anci.viewCafeMenu = function(){
		UI_Anci.showPopupWindow("Sky Cafe Menu","showFile!displayMeals.action",600,800,true);
	}
	
	/*
	 * Load SSR data
	 */
	UI_Anci.loadSSRData = function(sync){
		UI_Anci.showLoadingMsg(UI_Anci.anciType.SSR);
		var data = {};	
		$("#frmReservation").ajaxSubmit({dataType: 'json',url:'interlineAnciSSR.action', processData: false, async:!sync, data:data,		
			success: UI_Anci.onSSRLoadSuccess, error:UI_commonSystem.setErrorStatus});		
	}
	
	/*
	 * On successful meal loading..
	 */
	UI_Anci.onSSRLoadSuccess = function(response){		
		if(response!=null){
			UI_Anci.jsSSRModel = response.flightSegmentSSRs;
			if(UI_Anci.jsSSRModel != null){
				UI_Anci.jsSSRModel = $.airutil.sort.quickSort(UI_Anci.jsSSRModel,segmentComparator);
				UI_Anci.viewSSRInfo();
				UI_Anci.hideLoadingMsg(UI_Anci.anciType.SSR);
			}			
		}else{
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.SSR);
			jConfirm('Error occured while loading SSRs', 'Ok', function(){});
		}
	}
	
	
	
	/*
	 * Load baggage data
	 */
	UI_Anci.loadBaggageData = function(sync){
		$("#tblBaggage").show();
		$("#tbdyBaggageSeg").hide();
		UI_Anci.showLoadingMsg(UI_Anci.anciType.BAGGAGE);
		var data = {};	
		data['isModifyAnci'] = UI_Container.addModifyAncillary;
		$("#frmReservation").ajaxSubmit({dataType: 'json',url:'interlineAnciBaggage.action', processData: false, async:!sync,	data:data,		
				success: UI_Anci.onBaggageLoadSuccess, error:UI_commonSystem.setErrorStatus});
	}
	
	
	/*
	 * On successful baggage loading..
	 */
	UI_Anci.onBaggageLoadSuccess = function(response){
		
		var i = 0;
		var j = 0;
		
		var removeEmptyBaggageArr = function (arr){
			var retunArray = [];
			$.each(arr, function(){
				if (this.baggages.length != 0){
					retunArray[retunArray.length] = this;
				}
			})
			return retunArray;
		}
		if(response!=null){
			UI_Anci.isONDBaggage = response.baggageResponceDTO.ondBaggage;

			UI_Anci.jsBaggageModel = removeEmptyBaggageArr(response.baggageResponceDTO.flightSegmentBaggages);
			UI_Anci.jsBaggageFlightSegmentModel = response.flightSegmentTOs;
			UI_Anci.jsBaggageModel = $.airutil.sort.quickSort(UI_Anci.jsBaggageModel,segmentComparator);
			//if(UI_Container.isModifySegment()){
			//	UI_Anci.seatReprotect();
			//}
			if(UI_Container.isModifySegment() || UI_Container.isModifyAncillary()){
				isBaggageSelectedForCurrentBooking();
			}
			
			if(UI_Anci.jsBaggageModel.length > 0){
				if (UI_Anci.baggageViewType == "New-view"){
					UI_Anci.buildSegmentsBaggageTab({id:"tbdyBaggageSeg", tdID:"tdBaggageSeg_", click:null, baggages: UI_Anci.jsAnciAvail.baggages});
					UI_Anci.buildBaggagePax(UI_Anci.jsAnciAvail.baggages);
				}
				
				if(!UI_Container.isModifySegment() || !UI_Anci.isBaggagesSelectedForCurrentBooking){
					UI_Anci.viewBaggageInfo();
				}			
				//UI_commonSystem.sectionLoaderHide("#tblSeatMap","#seatmapLoader")
				UI_Anci.baggagePromoOffer(response);
				$("#tbdyBaggageSeg").show();
				UI_Anci.hideLoadingMsg(UI_Anci.anciType.BAGGAGE);
			} else {
				$("#tbdyBaggageSeg").hide();
				UI_Anci.hideLoadingMsg(UI_Anci.anciType.BAGGAGE);
			}			
			
		}else{			
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.BAGGAGE);
			jConfirm('Error occured while loading Baggage', 'Ok', function(){});
		}

		while(true){
	
			if($("#tdBaggageSeg_" + i).length){
				
				while(true){

					if($("#selBaggageType_" + i + "_" + j).length){

						$("#selBaggageType_" + i + "_" + j).trigger("change");
						j++;
		
					}else{

						break;
					
					}

				}
			
				i++;
				j = 0;
		
			}else{

				break;

			}

		}
		
	}
	
	UI_Anci.baggagePromoOffer = function(p){
		if(p.anciOffer){
			//anciOfferName
			$(".baggageOffer").html("<label>"+p.anciOfferDescription+"</label>");
		}else{
			$(".baggageOffer").parent().parent().hide();
		}
	}

	UI_Anci.viewSSRInfo = function(){
		$("#tblSSRSel").show();//changed hide() to show()
		$("#tblSSRDetails").show();
		var segId =UI_Anci.jsAnciAvail.ssrs[0].segInd;
		UI_Anci.clickSSRSegment({ind:segId});
		$("#radSSR_Y").trigger("click");
	}
	
	UI_Anci.contFn = function(){
		
		UI_Anci.disableButtons();
		var data = {};

		var seatsToBlock = UI_Anci.seatsForBlocking();
		if(seatsToBlock.length>0){
			data['selectedSeats'] = $.toJSON(seatsToBlock);
			data['system']= UI_Anci.system;
			$("#frmReservation").ajaxSubmit({dataType: 'json',async: true,url:'interlineAnciBlocking.action', processData: false,	data:data,		
				success: UI_Anci.anciBlockingResponse, error:UI_commonSystem.setErrorStatus});
		}else{
			UI_Anci.anciBlockingResponse({success:true});
		}
	}
	
	/*
	 * Release blocked seats synchronously 
	 */
	UI_Anci.releaseBlockedSeats = function(){
		var seatsReleased = true;
		if(UI_Anci.seatsBlocked){
			$("#frmReservation").ajaxSubmit({dataType: 'json',async: false,url:'interlineAnciRelease.action', processData: false,	data:{},		
				success: function (response){
					UI_Anci.seatsBlocked = !response.success;
					seatsReleased = response.success;
				}, error:UI_commonSystem.setErrorStatus
			});
		}
		return seatsReleased;
	}
	
	/*
	 * Block selected seat synchronously
	 */
	UI_Anci.blockSelectedSeats = function(){
		var seatsToBlock = UI_Anci.seatsForBlocking();
		var seatsBlocked = true;

		UI_Anci.releaseBlockedSeats();

		if(seatsToBlock.length>0){
			var data = {};
			data['selectedSeats'] = $.toJSON(seatsToBlock);
			data['system']= UI_Anci.system;
			$("#frmReservation").ajaxSubmit({dataType: 'json',async: false,url:'interlineAnciBlocking.action', processData: false,	data:data,		
				success: function (response){
					UI_Anci.seatsBlocked = response.success;
					seatsBlocked= response.success;
				}, error:UI_commonSystem.setErrorStatus
			});
		}
		
		return seatsBlocked;
	}
	
	UI_Anci.seatsForBlocking = function(){
		var outArr = [];
		$.each(UI_Anci.occupiedSeats, function(fltRefNo,obj){
			var seatArr = [];
			$.each(obj.seats,function(seatNumber, seatObj){
				if(seatObj!=null && parseInt(seatObj.paxId,10) >=0){
					seatArr[seatArr.length] = {seatNo: seatObj.refObj.seatNumber,paxType: seatObj.paxType};
				}
			});
			if(seatArr.length>0){
				outArr[outArr.length] = {fltRefNo:fltRefNo,seats:seatArr}; 
			}
		});
		
		return outArr;
	}
	
	UI_Anci.allSelectedCheck = function(){
		//UI_Anci.jsAnciSegModel.push({segCode:seg.flightSegmentTO.segmentCode, flightRefNumber: seg.flightSegmentTO.flightRefNumber, pax:[]});		
		//UI_Anci.jsAnciSegModel[].pax[j]={paxType:,refI: j, seat:{seatNumber:'',seatCharge:0},meal:{meals:[],mealChargeTotal:0,mealQtyTotal:0},
		//		ssr:{ssrs:[],ssrChargeTotal:0,ssrQtyTotal:0}};
		//UI_Anci.jsAnciSeg[] {meal: false, seat: false, hala: false, ssr:false};
		
		var anciNoSelect = {meals:[], seats:[], ssrs:[], apss:[], apts:[],flexi:[]};
		$.each(UI_Anci.jsAnciSegModel, function(segI, segModel){
			var meal = {segCode: segModel.segCode, segId: segI, paxCount: 0, paxIds:[]};
			var seat = {segCode: segModel.segCode, segId: segI, paxCount: 0, paxIds:[]};
			var ssr = {segCode: segModel.segCode, segId: segI, paxCount: 0, paxIds:[]};
			var aps = {segCode: segModel.segCode, segId: segI, paxCount: 0, paxIds:[]};
			var apt = {segCode: segModel.segCode, segId: segI, paxCount: 0, paxIds:[]};
			var flexi = {segCode: segModel.segCode, segId: segI, paxCount: 0, paxIds:[]};
			
			$.each(segModel.pax, function(paxId, paxAnci){				
				if(UI_Anci.jsAnciSeg[segI].meal && paxAnci.meal.mealQtyTotal == 0){
					meal.paxCount++;
					meal.paxIds[meal.paxIds] = paxId;
				}
				if(UI_Anci.jsAnciSeg[segI].seat && (paxAnci.seat.seatNumber == null || paxAnci.seat.seatNumber == "")){
					seat.paxCount++;
					seat.paxIds[seat.paxIds] = paxId;
				}
				if(UI_Anci.jsAnciSeg[segI].ssr && paxAnci.ssr.ssrQtyTotal == 0){
					ssr.paxCount++;
					ssr.paxIds[ssr.paxIds] = paxId;
				}
				if(UI_Anci.jsAnciSeg[segI].hala && (paxAnci.aps.apss == null || paxAnci.aps.apss.length == 0)){
					aps.paxCount++;
					aps.paxIds[aps.paxIds] = paxId;
				}
				if(UI_Anci.jsAnciSeg[segI].apt && (paxAnci.apt.apts == null || paxAnci.apt.apts.length == 0)){
					apt.paxCount++;
					apt.paxIds[apt.paxIds] = paxId;
				}
			});
			if(UI_Anci.jsAnciSeg[segI].meal && meal.paxCount> 0){
				anciNoSelect.meals[anciNoSelect.meals.length]= meal;
			}
			if(UI_Anci.jsAnciSeg[segI].seat && seat.paxCount> 0){
				anciNoSelect.seats[anciNoSelect.seats.length]= seat;
			}
			if(UI_Anci.jsAnciSeg[segI].ssr && ssr.paxCount > 0){
				anciNoSelect.ssrs[anciNoSelect.ssrs.length]= ssr;
			}
			if(UI_Anci.jsAnciSeg[segI].hala && aps.paxCount > 0){
				anciNoSelect.apss[anciNoSelect.apss.length]= aps;
			}
			if(UI_Anci.jsAnciSeg[segI].apt && apt.paxCount > 0){
				anciNoSelect.apts[anciNoSelect.apts.length]= apt;
			}
			
		});
		
		var anciSum = UI_Anci.anciSummary;
		var str = '';
		var count = 0;
		var allSelected = true;
		var atLeastOneSelected = false;
		for(var i=0; i < anciSum.length; i++){
			var selectedNoSelect = [];
			var userOptOut = false;
			if(anciSum[i].rad!=null && $('input[name="'+anciSum[i].rad+'"]:checked').val() == 'N'){
				userOptOut = true;
			}else if (anciSum[i].type ==  UI_Anci.anciType.INS && !UI_Anci.anciInsurance.selected){
				userOptOut = true;
			}			
			if (anciSum[i].type ==  UI_Anci.anciType.MEAL){
				selectedNoSelect = anciNoSelect.meals;
			}else if (anciSum[i].type ==  UI_Anci.anciType.SEAT){
				selectedNoSelect = anciNoSelect.seats;
			}else if (anciSum[i].type ==  UI_Anci.anciType.SSR){
				selectedNoSelect = anciNoSelect.ssrs;
			}else if(anciSum[i].type == UI_Anci.anciType.HALA){
				selectedNoSelect = anciNoSelect.apss;
			}else if(anciSum[i].type == UI_Anci.anciType.APTRANSFER){
				selectedNoSelect = anciNoSelect.apts;
			}
				
			
			if(anciSum[i].stat == 'ACT' && !userOptOut && selectedNoSelect.length > 0){
				if(count>0){
					str+='<br />';
				}
				str += '<b>'+anciSum[i].name + '</b> not selected for : ';
				var substr = '<div style="display:block; margin-left:5px;">'
				$.each(selectedNoSelect, function(ind, itm){
					if(ind>0) substr += ', ';
					substr += UI_Container.getSegmentName(itm.segCode);
				});
				substr += '</div>';
				str += substr;
				count ++;
				allSelected = false;
			}
			if(anciSum[i].qty > 0){
				atLeastOneSelected = true;
			}	
		}	
		
		// flexi handled by OND wise and needed a special filter
		if(UI_Anci.flexiEnableInAnci && $('input[name="radFlexi"]:checked').val() !== 'N'){
			var ondLsit = UI_Anci.getONDFormSeg(UI_Anci.jsAnciAvail.flexi);
			var noFlexiCount = 0;
			var noFlexiOnds = [];
			var ondCheckedList = $('[id^="chkFlexiAvail_"]');
			$.each(ondCheckedList, function(key,ondFlexiCheck) {
				if(ondFlexiCheck.checked !== true){
					ondFlexiRef = ondFlexiCheck.id.split("_");
					$.each(ondLsit,function(key,ondFlexi){
						if(ondFlexiRef[1] === ondFlexi.ondSeq.toString() ){
							noFlexiOnds[noFlexiCount] = ondFlexi.segCode;
							noFlexiCount++;
						}
					});
				}
			});
			if(noFlexiOnds.length > 0){
				str += '<b>'+ "Flexi" + '</b> not selected for : ';
				var substr = '<div style="display:block; margin-left:5px;">';
				$.each(noFlexiOnds, function(ind, flexOnd){
					if(ind>0) substr += ', ';
					substr += flexOnd;
				});
				substr += '</div>';
				str += substr;
			}
		}
		
		return {allSelected: allSelected, str: str, count:count, atLeastOneSelected: atLeastOneSelected};
	}
	
	UI_Anci.leastOneSelectedCheck = function(){
		var anciSum = UI_Anci.anciSummary;
		var str = '';
		var count = 0;
		var atLeastOneSelected = false;
		for(var i=0; i < anciSum.length; i++){
			var userOptOut = false;
			if(anciSum[i].rad!=null && $('input[name="'+anciSum[i].rad+'"]:checked').val() == 'N'){
				userOptOut = true;
			}else if (anciSum[i].type ==  UI_Anci.anciType.INS){
				if (anciSum[i].subTotal > 0 ) {
					userOptOut = true;
					anciSum[i].qty = 1;
			    } else  {
			    	userOptOut = false;
			    	anciSum[i].qty = 0;// Update ancilary qty
			    }
			}else if(anciSum[i].type ==  UI_Anci.anciType.HALA){
				if((anciSum[i].subTotal != null && anciSum[i].subTotal > 0)
						|| (anciSum[i].qty != null && anciSum[i].qty > 0)){
					userOptOut = false;
				}
			} else if(anciSum[i].type ==  UI_Anci.anciType.APTRANSFER){
				if(anciSum[i].subTotal != null && anciSum[i].subTotal > 0){
					userOptOut = false;
					anciSum[i].qty = 1;
				} else {
					anciSum[i].qty = 0;
				}
			} else if(anciSum[i].type ==  UI_Anci.anciType.FLEXI){
				if(!_isFlexiNotSelected()){
					userOptOut = true;
			    	anciSum[i].qty = 1;
				}else{
					userOptOut = false;
			    	anciSum[i].qty = 0;
				}
				
			}
			
			if(anciSum[i].stat == 'ACT' && !userOptOut && anciSum[i].qty==0){
				if(count>0){
					str+=', ';
				}
				str += anciSum[i].name;
				count ++;
			}
			
			if(anciSum[i].qty > 0){
				atLeastOneSelected = true;
			}	
		}
		
		return {atLeastOneSelected: atLeastOneSelected, str: str, count:count};
	}
	
	_isFlexiNotSelected = function(){
		var isFlexiNotSelected = true;
		var isFlexiAvailableForOnds = false;
		
		if(UI_Anci.ondAvailbleFlexi != null && UI_Anci.jsAnciSeg != null){
			for(i=0;i<UI_Anci.jsAnciSeg.length;i++){
				var ondSeq = UI_Anci.jsAnciSeg[i].flightSegmentTO.ondSequence;
				if(UI_Anci.ondAvailbleFlexi[ondSeq] != undefined && UI_Anci.ondAvailbleFlexi[ondSeq]){
					isFlexiAvailableForOnds = true;
					if((UI_Anci.jsAnciFlexiAdded[ondSeq] != undefined && UI_Anci.jsAnciFlexiAdded[ondSeq]) 
							|| (UI_Anci.jsAnciFlexiUAdded[ondSeq] != undefined && UI_Anci.jsAnciFlexiUAdded[ondSeq])){					
						isFlexiNotSelected = false;
						return;
					}
				}
			}
		}
		
		return isFlexiAvailableForOnds && isFlexiNotSelected;
	}
	
	UI_Anci.checkForFreeSeatSelection = function(){
		var response = {result:true, message:'', segIndex:0};		
		for(var anciSegIndex = 0;anciSegIndex < UI_Anci.jsAnciSegModel.length;anciSegIndex++){
			var anciSegObj = UI_Anci.jsAnciSegModel[anciSegIndex];
			var fltRefNo = anciSegObj.flightRefNumber;
			if(UI_Anci.fltWiseFreeSeatMap[fltRefNo]){
				for(var paxIndex=0;paxIndex<anciSegObj.pax.length;paxIndex++){
					var paxObj = anciSegObj.pax[paxIndex];
					if(paxObj.seat.seatNumber == null || paxObj.seat.seatNumber == ''){
						response.result = false;
						response.message = UI_Container.labels.lblFreeSeatSelectionNotification.replace('{0}', anciSegObj.segCode);
						response.segIndex = anciSegIndex;
						return response;	
					}
				}
			}
		}
		
		return response;
	}
	
	UI_Anci.checkForFreeMealSelection = function(){
		var response = {result:true, message:'', segIndex:0};
		for(var anciSegIndex = 0;anciSegIndex < UI_Anci.jsAnciSegModel.length;anciSegIndex++){
			var anciSegObj = UI_Anci.jsAnciSegModel[anciSegIndex];
			var fltRefNo = anciSegObj.flightRefNumber;
			if(UI_Anci.fltWiseFreeMealMap[fltRefNo]){
				for(var paxIndex=0;paxIndex<anciSegObj.pax.length;paxIndex++){
					var paxObj = anciSegObj.pax[paxIndex];
					if(paxObj.meal.mealQtyTotal == null || parseInt(paxObj.meal.mealQtyTotal) == 0){
						response.result = false;
						response.message = UI_Container.labels.lblFreeMealSelectionNotification.replace('{0}', anciSegObj.segCode);
						response.segIndex = anciSegIndex;
						return response;	
					}
				}
			}
		}
		
		return response;
	}
	
	UI_Anci.isFreeSeatEnableForReservation = function(){
		var isFreeSeatEnable = false;
		for(var anciSegIndex = 0;anciSegIndex < UI_Anci.jsAnciSegModel.length;anciSegIndex++){
			var anciSegObj = UI_Anci.jsAnciSegModel[anciSegIndex];
			var fltRefNo = anciSegObj.flightRefNumber;
			if(UI_Anci.fltWiseFreeSeatMap[fltRefNo]){
				isFreeSeatEnable = true;
				break;
			}
		}
		
		return isFreeSeatEnable;
	}
	
	UI_Anci.isFreeMealEnableForReservation = function(){
		var isFreeMealEnable = false;
		for(var anciSegIndex = 0;anciSegIndex < UI_Anci.jsAnciSegModel.length;anciSegIndex++){
			var anciSegObj = UI_Anci.jsAnciSegModel[anciSegIndex];
			var fltRefNo = anciSegObj.flightRefNumber;
			if(UI_Anci.fltWiseFreeMealMap[fltRefNo]){
				isFreeMealEnable = true;
				break;
			}
		}
		
		return isFreeMealEnable;
	}
	
	/*
	 * Continue On Click
	 */
	UI_Anci.continueOnClick = function(navCallBack){
		
		var seatSelectionResponse = UI_Anci.checkForFreeSeatSelection();		
		if(!seatSelectionResponse.result){
			jAlert(seatSelectionResponse.message);
			//UI_Anci.clickSeatSegment({ind:seatSelectionResponse.segIndex});
			return false;
		}
		
		var mealSelectionResponse = UI_Anci.checkForFreeMealSelection();
		if(!mealSelectionResponse.result){
			jAlert(mealSelectionResponse.message);
			//UI_Anci.clickMealSegment({ind:mealSelectionResponse.segIndex});
			return false;
		}
		
		//check for seat terms condition acceptence.
		var termsChecked = $("#chkSeatTerms").is(':checked');
		var termsCheckVisible = $('#chkSeatTerms').is(':visible')
		if(UI_Anci.getAncillarySummary(UI_Anci.anciType.SEAT).qty > 0 && !termsChecked && termsCheckVisible && anciConfig.showSeatMessageTermsCheckBox){
			  jAlert(UI_Container.labels.lblAnciSeatSelectionTermsAcceptAlert);
			  return false;
		}
		
		var message = UI_Anci.bundleFarePopUpDisplay();
		if(message != null) {
			jAlert(message);
			return false;
		}
		if (($("#radAnciIns_Y").attr("checked") == true) 
				&& anciConfig.insTCCheck && ($("#chkTC").attr("checked")==false)){
			jAlert(UI_Container.labels.insTCuncheckMsg);
			return false;
		}else{
			var paxWiseAnci = [];
			paxWiseAnci = UI_Anci.generatePaxWiseAnci();
			var validateMedicalSsrSegments = true;
			var message="";
			
			if (UI_Container.isModifySegment()){
				validateMedicalSsrSegments = UI_Anci.alertMedicalSsrForAllSegments(UI_Anci.paxWiseAnci, paxWiseAnci);
				message="M";
			}else{
				validateMedicalSsrSegments = UI_Anci.checkMedicalSsrInAllSegments(paxWiseAnci);
				message="C";
			}
				
			if (!validateMedicalSsrSegments){
				ssrSegmentValidationPopUp(message);
			}else{
				callMyPopUp(null, null, null, null, null, null, navCallBack);
			}
		}
		
		
	}
	

		
	UI_Anci.bundleFarePopUpDisplay = function() {

		var message = null;

		if (UI_Container.bundleFarePopupEnabled) {
//				&& UI_Anci.selectedBundledFares != null) {
			// return free anci list for selected fare bundles
			var bundledAvailableAnci = new Object();
//			bundledAvailableAnci = UI_Anci
//					.returnFareBundleFreeAnci(UI_Anci.selectedBundledFares);
			bundledAvailableAnci = UI_Anci.returnFareBundleFreeAnci();
			var strTemp = '';
			var bundleFareNotSelectedFreeAnci = new Object();

			var paxWiseAnci = [];
			paxWiseAnci = UI_Anci.generatePaxWiseAnci();
			var flexiChecked = false;
			var alertFareBundlePopup = false;

			if (!(Object.keys(bundledAvailableAnci).length === 0)) {
				for (i = 0; i < Object.keys(bundledAvailableAnci).length; i++) {
					if (bundledAvailableAnci[i] != null) {
						if (!(Object.keys(paxWiseAnci).length === 0)) {
							for (j = 0; j < Object.keys(paxWiseAnci).length; j++) {
								var pax = paxWiseAnci[j];
								for (segSeq = 0; segSeq < pax.anci.length; segSeq++) {
									var ondPaxAnciList = pax.anci[segSeq];
									if (ondPaxAnciList != undefined) {
										for (anci in bundledAvailableAnci[i]) {
											switch (bundledAvailableAnci[i][anci]) {

											case "MEAL":
												if (UI_Anci.jsAnciSeg[segSeq].meal == true
														&& ondPaxAnciList.meal.meals.length == 0
														&& UI_Anci.jsAnciSeg[segSeq].flightSegmentTO.ondSequence == i) {
													alertFareBundlePopup = true;
													bundleFareNotSelectedFreeAnci["MEAL"] = "Meals";
												}
												break;
											case "BAGGAGE":
												if (UI_Anci.jsAnciSeg[segSeq].baggage == true
														&& ondPaxAnciList.baggage.baggages.length == 0
														&& UI_Anci.jsAnciSeg[segSeq].flightSegmentTO.ondSequence == i) {
													alertFareBundlePopup = true;
													bundleFareNotSelectedFreeAnci["BAGGAGE"] = "Baggages";
												}
												break;
											case "SEAT_MAP":
												if (UI_Anci.jsAnciSeg[segSeq].seat == true
														&& ondPaxAnciList.seat.seatNumber == ""
														&& UI_Anci.jsAnciSeg[segSeq].flightSegmentTO.ondSequence == i) {
													alertFareBundlePopup = true;
													bundleFareNotSelectedFreeAnci["SEAT_MAP"] = "Seats";
												}
												break;
											case "AIRPORT_SERVICE":
												if (UI_Anci.jsAnciSeg[segSeq].hala == true
														&& ondPaxAnciList.aps.apss.length == 0
														&& UI_Anci.jsAnciSeg[segSeq].flightSegmentTO.ondSequence == i) {
													alertFareBundlePopup = true;
													bundleFareNotSelectedFreeAnci["AIRPORT_SERVICE"] = "Airport Services";
												}
												break;
											case "SSR":
												if (UI_Anci.jsAnciSeg[segSeq].ssr == true
														&& ondPaxAnciList.ssr.ssrs.length == 0
														&& UI_Anci.jsAnciSeg[segSeq].flightSegmentTO.ondSequence == i) {
													alertFareBundlePopup = true;
													bundleFareNotSelectedFreeAnci["SSR"] = "SSR";
												}
												break;
											case "AIRPORT_TRANSFER":
												if (UI_Anci.jsAnciSeg[segSeq].apt == true
														&& ondPaxAnciList.apt.apts.length == 0
														&& UI_Anci.jsAnciSeg[segSeq].flightSegmentTO.ondSequence == i) {
													alertFareBundlePopup = true;
													bundleFareNotSelectedFreeAnci["AIRPORT_TRANSFER"] = "Airport Transfer";
												}
												break;
											case "FLEXI_CHARGES":
												if (UI_Anci.jsAnciSeg[segSeq].flexi == true
														&& UI_Anci
																.isFlexiNotSelected()
														&& !flexiChecked) {
													alertFareBundlePopup = true;
													bundleFareNotSelectedFreeAnci["FLEXI_CHARGES"] = "Flexi";
													flexiChecked = true;
												}
												break;
											}
										}
									}
								}

							}
						}
					}
				}
			}

			if (alertFareBundlePopup) {
				var bundleFareNotSelectedFreeAnciArray = $.map(
						bundleFareNotSelectedFreeAnci, function(value, index) {
							return [ value ];
						});

				strTemp = bundleFareNotSelectedFreeAnciArray.join(", ");

				bundleFareNotSelectedFreeAnci = {};

				var fareYouSelected = UI_Anci.jsonLabel['lblFareYouSelected'];
				var makeSelection = UI_Anci.jsonLabel['lblMakeSelection'];

				if (alertFareBundlePopup) {
					message = fareYouSelected + strTemp.toString()
							+ makeSelection;
				}
			}
		}
		return message;
	}
	
	UI_Anci.isFlexiNotSelected = function(){
		var isFlexiNotSelected = true;
		var isFlexiAvailableForOnds = false;
		
		if(UI_Anci.ondAvailbleFlexi != null && UI_Anci.jsAnciSeg != null){
			for(k=0;k<UI_Anci.jsAnciSeg.length;k++){
				var ondSeq = UI_Anci.jsAnciSeg[k].flightSegmentTO.ondSequence;
				if(UI_Anci.ondAvailbleFlexi[ondSeq] != undefined && UI_Anci.ondAvailbleFlexi[ondSeq]){
					isFlexiAvailableForOnds = true;
					if((UI_Anci.jsAnciFlexiAdded[ondSeq] != undefined && UI_Anci.jsAnciFlexiAdded[ondSeq]) 
							|| (UI_Anci.jsAnciFlexiUAdded[ondSeq] != undefined && UI_Anci.jsAnciFlexiUAdded[ondSeq])){					
						isFlexiNotSelected = false;
						return;
					}
				}
			}
		}
		
		return isFlexiAvailableForOnds && isFlexiNotSelected;
	}
	 
	 /*
	  * Return free anci for fare bundle
	  */
//	 UI_Anci.returnFareBundleFreeAnci = function (ondWiseBundleFareSelected) {
	UI_Anci.returnFareBundleFreeAnci = function (){
	 	var map = new Object(); 
	 	var ondWiseBundleFareSelected = _parseJSONStringToObject($("#resOndBundleFareStr").val());
	 	var avilableBundleFares = UI_Anci.availableBundleFares;
		if (ondWiseBundleFareSelected != null) {
			for (ond = 0; ond < Object.keys(ondWiseBundleFareSelected).length; ond++) {
				if (ondWiseBundleFareSelected[ond] == ""
						|| ondWiseBundleFareSelected[ond] == "null" || ondWiseBundleFareSelected[ond] == null) {
					map[ond] = null;
				} else {
					var availableBundlesForOnd = avilableBundleFares[ond];
					for (fare = 0; fare < Object.keys(availableBundlesForOnd).length; fare++) {
						if (availableBundlesForOnd[fare] != null
								&& availableBundlesForOnd[fare][0].bundledFarePeriodId == ondWiseBundleFareSelected[ond]) {
							map[ond] = availableBundlesForOnd[fare][0].bundleFareFreeServices;
							break;
						}
					}
				}
			}
		}	 	
	 	
	 	return map;
	 	
	 	
	 	
	 	
	 	
	 	
//	 	if(ondWiseBundleFareSelected.length>0) {
//	 		for(i =0; i<ondWiseBundleFareSelected.length; i++) {
//	 			if(ondWiseBundleFareSelected[i] != null){
//	 				map[i] = ondWiseBundleFareSelected[i].freeServiceNames;
//	 			} else {
//	 				
//	 				map[i] = null;
//	 			}
//	 			
//	 		}
//	 	}
//	 	return map;
	 }
	
	UI_Anci.focusNotSelected = function(selectionDetails){
		//TODO focus to non selected ancillary
	}
	
	UI_Anci.getNumOfUnFlownUnBusSegs = function(pax){
		
		var numberOfUnFlownUnBusSegs = 0;
		
	    if (pax.anci != null){
	    	for (var j=0;j < pax.anci.length; j++){
	    		var anciSegment = pax.anci[j];
	    		if (!UI_Anci.isBusCarrierCode(anciSegment.segInfo.flightRefNumber) && !anciSegment.segInfo.flownSegment){
	    			numberOfUnFlownUnBusSegs++;
	    		}
	    	}
	    }else if (pax.selectedAncillaries != null){
	    	for (var j=0;j < pax.selectedAncillaries.length; j++){
	    		var anciSegment = pax.selectedAncillaries[j];
	    		if (!UI_Anci.isBusCarrierCode(anciSegment.flightSegmentTO.flightRefNumber) && !anciSegment.flightSegmentTO.flownSegment){
	    			numberOfUnFlownUnBusSegs++;
	    		}
	    	}
	    }
		
		return numberOfUnFlownUnBusSegs;
	}

	UI_Anci.alertMedicalSsrForAllSegments = function(previousPaxWiseAnci, modifiedPaxWiseAnci){
		var medicalSsrInAllSegmentsIfThere = true;
		var validateModifySegments = true;
		
		if (modifiedPaxWiseAnci != null){
			for (var i = 0; i < modifiedPaxWiseAnci.length; i++) {
				var pax = modifiedPaxWiseAnci[i];
				
				if (pax.paxType != "IN"){
					var validateSsrSegmentsInModify = UI_Anci.validateSsrSegmentsInModify(pax, "MEDA");
					if (!validateSsrSegmentsInModify) {
						validateModifySegments = false;
						break;
					}
				}
			}
		}
		
		if (modifiedPaxWiseAnci != null){
			for (var i = 0; i < modifiedPaxWiseAnci.length; i++) {
				var pax = modifiedPaxWiseAnci[i];
				
				if (pax.paxType != "IN"){
					var isModifySegmentPaxContainsSSR = UI_Anci.isPaxContainsSsr(pax, "MEDA");
					var paxInPreviousSegment = UI_Anci.getPaxFromPreviousSegments(pax, previousPaxWiseAnci);
					
					if (paxInPreviousSegment != null && paxInPreviousSegment.selectedAncillaries != null){
						var numberOfSegsWithoutBusSegs = UI_Anci.getNumOfUnFlownUnBusSegs(paxInPreviousSegment);
						if (numberOfSegsWithoutBusSegs > 1 && paxInPreviousSegment.selectedAncillaries.length != UI_Anci.getNumberOfModifyingSegments(paxInPreviousSegment)){
							var isPaxPreviouslyContainedSSR = UI_Anci.isPaxContainsSsrInPreviousSegments(paxInPreviousSegment, "MEDA");
							var isEqual = (isModifySegmentPaxContainsSSR == isPaxPreviouslyContainedSSR);

							if (!isEqual) {
								medicalSsrInAllSegmentsIfThere = false;
								break;
							}
						}
					}
				}
			}
		}
		
		return medicalSsrInAllSegmentsIfThere && validateModifySegments;
	}
	
	UI_Anci.getNumberOfModifyingSegments = function(pax){
		var numberOfModifyingSeg = 0;
		
		if (pax.selectedAncillaries != null){
	    	for (var j=0;j < pax.selectedAncillaries.length; j++){
	    		var anciSegment = pax.selectedAncillaries[j];
	    		if (UI_Container.isOneOfModifyingSeg(anciSegment.flightSegmentTO.flightRefNumber)){
	    			numberOfModifyingSeg++;
	    		}
	    	}
	    }
		
		return numberOfModifyingSeg;
	}
	
	UI_Anci.isPaxContainsSsr = function(pax, ssrCode){
		var isPaxContainsSsrInAllNewSegments = false;
		var numberOfsegmentsHasSSR = 0;
		
		if (pax.anci != null){
			var newSegmentSize = UI_Anci.getNumOfUnFlownUnBusSegs(pax);
			anciToUpdateList = pax.anci;
			
			for (var i = 0; i < anciToUpdateList.length; i++) {
				var selectedSegmentAnci = anciToUpdateList[i];
				if (selectedSegmentAnci != null && selectedSegmentAnci.ssr != null && selectedSegmentAnci.ssr.ssrs != null && !selectedSegmentAnci.segInfo.flownSegment){
					var ssrList = [];
					ssrList = selectedSegmentAnci.ssr.ssrs;
					for (var k = 0; k < ssrList.length; k++) {
						var ssr = ssrList[k];
						if (ssr != null && ssr.ssrCode == "MEDA"){
							numberOfsegmentsHasSSR++;
						}
					}
				}
			}
		}
			    
		if (newSegmentSize == numberOfsegmentsHasSSR) {
			isPaxContainsSsrInAllNewSegments = true;
		}

		return isPaxContainsSsrInAllNewSegments;
	}
	
	UI_Anci.validateSsrSegmentsInModify = function(pax, ssrCode){
		var isPaxContainsOrDoNotContainSsrInAllNewSegments = false;
		var numberOfsegmentsHasSSR = 0;
		
		if (pax.anci != null){
			var newSegmentSize = UI_Anci.getNumOfUnFlownUnBusSegs(pax);
			anciToUpdateList = pax.anci;
			
			for (var i = 0; i < anciToUpdateList.length; i++) {
				var selectedSegmentAnci = anciToUpdateList[i];
				if (selectedSegmentAnci != null && selectedSegmentAnci.ssr != null && selectedSegmentAnci.ssr.ssrs != null && !selectedSegmentAnci.segInfo.flownSegment){
					var ssrList = [];
					ssrList = selectedSegmentAnci.ssr.ssrs;
					for (var k = 0; k < ssrList.length; k++) {
						var ssr = ssrList[k];
						if (ssr != null && ssr.ssrCode == "MEDA"){
							numberOfsegmentsHasSSR++;
						}
					}
				}
			}
		}
			    
		if (newSegmentSize == numberOfsegmentsHasSSR || numberOfsegmentsHasSSR == 0) {
			isPaxContainsOrDoNotContainSsrInAllNewSegments = true;
		}

		return isPaxContainsOrDoNotContainSsrInAllNewSegments;
	}
	
	UI_Anci.isPaxContainsSsrInPreviousSegments = function(pax, ssrCode){
		
		var isPaxContainsSsrInAllNewSegments = false;
		var numberOfsegmentsHasSSR = 0;

		if (pax.selectedAncillaries != null){
	    	var segmentList = [];
	    	segmentList = pax.selectedAncillaries;
	    	var newSegmentSize = UI_Anci.getNumOfUnFlownUnBusSegs(pax);
	    	
	    	for (var j = 0; j < segmentList.length; j++) {
	    		if (segmentList[j].specialServiceRequestDTOs != null && !segmentList[j].flightSegmentTO.flownSegment){
	    			var ssrList = segmentList[j].specialServiceRequestDTOs;
	    			for (var k = 0; k < ssrList.length; k++) {
	    				var ssr = ssrList[k];
						if (ssr != null && ssr.ssrCode == "MEDA"){
							numberOfsegmentsHasSSR++;
						}
					}	
	    		}
	    	}	
	    }
		
		if (newSegmentSize == numberOfsegmentsHasSSR) {
			isPaxContainsSsrInAllNewSegments = true;
		}

		return isPaxContainsSsrInAllNewSegments;
	}
	
	UI_Anci.getPaxFromPreviousSegments = function(pax, totalPaxWiseAnci){
		var equalPax = null;
		
		for (var i = 0; i < totalPaxWiseAnci.length; i++) {
			var tempPax = totalPaxWiseAnci[i];
			if (tempPax.travellerRefNo == pax.travelerRefNumber){
				equalPax = tempPax;
				break;
			}
		}
		
		return equalPax;
	}
	
	UI_Anci.checkMedicalSsrInAllSegments = function(paxWiseAnci){
		var medicalSsrInAllSegmentsIfThere = true;
		
		if (paxWiseAnci != null){
			for (var i = 0; i < paxWiseAnci.length; i++) {
			    var pax = paxWiseAnci[i];
			    
			    if (pax.paxType != "IN"){
				    if (paxWiseAnci[i].anci != null){
				    	var segmentSize = UI_Anci.getNumOfUnFlownUnBusSegs(paxWiseAnci[i]);
						
					    numberOfSegmentsContainsMedicalSsr = UI_Anci.getNumberOfSegmentsContainsSsr(pax, "MEDA");
					    numberOfSegmentsDoNotContainMedicalSsr = UI_Anci.getNumberOfSegmentsDoNotContainSsr(pax, "MEDA");
					    
					    if ((numberOfSegmentsContainsMedicalSsr == segmentSize) || (numberOfSegmentsDoNotContainMedicalSsr == segmentSize)) {
							medicalSsrInAllSegmentsIfThere = true;
						} else if ((numberOfSegmentsContainsMedicalSsr == 0) && (numberOfSegmentsDoNotContainMedicalSsr == 0)) {
							medicalSsrInAllSegmentsIfThere = true;
						} else {
							medicalSsrInAllSegmentsIfThere = false;
							break;
						}
				    }
			    }
			    
			}
		}
		return medicalSsrInAllSegmentsIfThere;
	}
	
	UI_Anci.getNumberOfSegmentsContainsSsr = function(pax, ssrCode){
		var numberOfSegmentsContainsMedicalSsr = 0;
		var anciToUpdateList = [];
		if (pax.anci != null){
			anciToUpdateList = pax.anci;
			for (var i = 0; i < anciToUpdateList.length; i++) {
				var selectedSegmentAnci = anciToUpdateList[i];
				if (selectedSegmentAnci != null && selectedSegmentAnci.ssr != null && selectedSegmentAnci.ssr.ssrs != null && !selectedSegmentAnci.segInfo.flownSegment){
					var ssrList = [];
					ssrList = selectedSegmentAnci.ssr.ssrs;
					for (var k = 0; k < ssrList.length; k++) {
						var ssr = ssrList[k];
						if (ssr != null && ssr.ssrCode == "MEDA"){
							numberOfSegmentsContainsMedicalSsr++;
						}
					}
				}
			}
		}
		return numberOfSegmentsContainsMedicalSsr;
	}
	
	UI_Anci.getNumberOfSegmentsDoNotContainSsr = function(pax, ssrCode){
		var numberOfSegmentsDoNotContainMedicalSsr = 0;
		var anciToRemoveList = [];
		if (pax.removeAnci != null){
			anciToRemoveList = pax.removeAnci;
			for (var i = 0; i < anciToRemoveList.length; i++) {
				var selectedSegmentAnci = anciToRemoveList[i];
				if (selectedSegmentAnci != null && selectedSegmentAnci.ssr != null && selectedSegmentAnci.ssr.ssrs != null && !selectedSegmentAnci.segInfo.flownSegment) {
					var ssrList = [];
					ssrList = selectedSegmentAnci.ssr.ssrs;
					for (var k = 0; k < ssrList.length; k++) {
						var ssr = ssrList[k];
						if (ssr != null && ssr.ssrCode == "MEDA"){
							numberOfSegmentsDoNotContainMedicalSsr++;
						}
					}
				}
			}
		}
		return numberOfSegmentsDoNotContainMedicalSsr;
	}
	
	UI_Anci.isBusCarrierCode = function(flightRefNumber){
		
		var carrierCode = flightRefNumber.substring(0, 2);
		
		for(var i =0; i < UI_Anci.busCarrierCodes.length; i++){
			if(carrierCode == UI_Anci.busCarrierCodes[i] ){
				return true;
			}
		}
		return false;
	}
	
	UI_Anci.handleSeatBlockingError = function(){
		UI_Anci.resetDataOnBlockError();
		UI_Anci.loadSeatData(false);
		if (UI_Anci.seatViewType == "New-view"){
			UI_Anci.buildSegmentsSeatsTab({id:"tbdySeatSeg", tdID:"tdSeatSeg_", click:null, segs: UI_Anci.jsAnciAvail.seats});
			UI_Anci.buildSeatPax(UI_Anci.jsAnciAvail.seats);
			UI_Anci.buildSeatMap({segId:UI_Anci.seat.currentSegId});
			UI_Anci.populateSMData({segId:UI_Anci.seat.currentSegId});
			UI_Anci.trigSegPax();
		}else{
			UI_Anci.buildSeatMap({segId:UI_Anci.seat.currentSegId});
			UI_Anci.populateSMData({segId:UI_Anci.seat.currentSegId});
		}
		jAlert(UI_Container.labels.lblAnciTryFailAlert);
	}
	
	/* 
	 * On anci blocking return
	 */
	UI_Anci.anciBlockingResponse = function(response){
		//UI_Anci.hideLoadingMsg();
		UI_Anci.enableButtons();
		if(response.success){
			$('#anciBtnNext').click();
		}else{
			UI_Anci.resetDataOnBlockError();
			/* FIXME only seat blocking handled - make generic */
			UI_Anci.loadSeatData(false);
			jConfirm('Error occured while allocating requested seat(s). Please try again', 'Ok', function(){});
		}
	}
	
	/*
	 * Segments
	 */
	UI_Anci.buildSegmentsTab = function(p){
		$("#" + p.id).find("tr").remove();
		
		var objSegDt = p.segs;
		var intLen = objSegDt.length;
		var i = 0;
		var tblRow = null;
		if (intLen > 0){
			tblRow = document.createElement("TR");
			var x = 0;
			do{
				if (x == 4){
					$("#" + p.id).append(tblRow);
					tblRow = document.createElement("TR");
				}
				tblRow.appendChild(UI_commonSystem.createCell({desc:"<label id='lbl_" + p.tdID + objSegDt[i].segInd  + "' class='fntDefault cursorPointer' title='"+ UI_Container.getSegmentName(objSegDt[i].segCode) +"'>" + UI_Container.getSegmentName(objSegDt[i].segCode) + "<\/label>", 
																id:p.tdID + objSegDt[i].segInd, 
																click:p.click,
																css:"tabNotSltd cursorPointer",
																align:"center"}));
				i++;
				x++;
			}while(--intLen);
			$("#" + p.id).append(tblRow);
		}
	}
	//Trigger pax
	var globalSegID = 0;
	UI_Anci.trigSegPax = function (){
		var radArra = $("input[name='radSeatPax']");
		for (var i=UI_Anci.currentSegPax; i<UI_Anci.segmentPaxUsedArray.length; i++){
			if (UI_Anci.segmentPaxUsedArray[i].seatUsed == false){
				globalSegID = UI_Anci.segmentPaxUsedArray[i].seatSegId;
				var pid = UI_Anci.segmentPaxUsedArray[i].pax.paxId;
				$(radArra[i]).trigger('click');
				break;
			}
		}
	}
	/*
	 * Segments for Seat maps
	 */
	UI_Anci.buildSegmentsSeatsTab = function(p){
		var availableSegs = UI_Anci.jsAnciAvail.segmentInfo;
		var adultChild = UI_Anci.jsonPaxAdults;
		var occSeat = UI_Anci.occupiedSeats;
		var addHTML = "";
		var addInboundHTML = "";
		var addOutboundHTML = "";
		var ibExist = false;
		var obExist = false;
		var j = 0;
		while (j < UI_Anci.jsAnciSeg.length){			
			if(availableSegs[j] != undefined && !availableSegs[j].returnFlag){				
				addOutboundHTML += '<div id="tdSeatSeg_'+ availableSegs[j].segInd +'" class="segs-full">'+
				UI_Anci.buildOthers(availableSegs[j],adultChild,availableSegs[j].segInd)+'</div>';
				obExist = true;		
				addOutboundHTML += '<div class="segs-full NoData"></div>';
			}else{
				if (availableSegs[j] != undefined){
						addInboundHTML += '<div id="tdSeatSeg_'+ availableSegs[j].segInd +'" class="segs-full">'+
						UI_Anci.buildOthers(availableSegs[j],adultChild,availableSegs[j].segInd)+'</div>';
						ibExist = true;
						addInboundHTML += '<div  class="segs-full NoData"></div>';
				}else{
					addInboundHTML += '<div class="segs-full NoData"></div>';
				}
			}
			j++
		};
		
		addHTML += '<div class="outBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblOutbound ,UI_Anci.jsAnciSeg.length/2,obExist) + addOutboundHTML+'</div>'+
					'<div class="inBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblInbound,UI_Anci.jsAnciSeg.length/2,ibExist)+ addInboundHTML+'</div><div class="clear"></div>';
		
		$("#"+p.id).html(addHTML);
		
		var n = 0;
		for (var i = 0; i < availableSegs.length ; i++){
			for (var j = 0; j < adultChild.length; j++){
				var segmentPaxUsed = {};
				segmentPaxUsed.segment = availableSegs[i];
				segmentPaxUsed.seatSegId = i;
				segmentPaxUsed.pax = adultChild[j];
				segmentPaxUsed.seatUsed = false;
				if(UI_Container.isModifyAncillary()){
					if ( adultChild[j].currentAncillaries == undefined || adultChild[j].currentAncillaries == "")
						segmentPaxUsed.seatUsed = false;
					else if (adultChild[j].currentAncillaries[i].seat == undefined)
						segmentPaxUsed.seatUsed = false;
					else if (adultChild[j].currentAncillaries[i].seat == "")
						segmentPaxUsed.seatUsed = false;
					else
						segmentPaxUsed.seatUsed = true;
				}
					
				UI_Anci.segmentPaxUsedArray[n] = segmentPaxUsed;
				n++;
			}
		}

	}
	/*
	 * Build seat Inner parts
	 */
	UI_Anci.buildOthers = function (segData,innerData,parentIndex){
		setFreeSeats = function(p){
			var str = ' '
			if (UI_Anci.jsAnciSeg[p].flightSegmentTO.freeSeatEnabled){
				str += UI_Container.labels.freeSeatAvail;
			}
			return str;
		}
		
		
		var addHTML = "";
			addHTML += '<table cellspacing="0" cellpadding="1" border="0" align="center" width="99%" class="seat-details">'+
			'<tbody>'+
			'<tr><td colspan="5" class="segcodebar"><label>'+UI_Container.getSegmentName(segData.segCode) + '' + setFreeSeats(parentIndex) +' </label></td></tr>'+
			'<tr>'+
				'<td align="center" width="5%" class="gridHD bdBottom bdLeft"><label class="gridHDFont fntBold">&nbsp;</label></td>'+
				'<td align="center" width="40%" class="gridHD bdBottom"><label id="lblSeatPaxNameHD" class="gridHDFont fntBold">'+UI_Container.labels.lblSeatPaxNameHD+'</label></td>'+
				'<td align="center" width="20%" class="gridHD bdBottom"><label id="lblSeatNumberHD" class="gridHDFont fntBold">'+UI_Container.labels.lblSeatNumberHD+'</label></td>'+
				'<td align="center" width="30%" class="gridHD bdBottom bdRight" colspan="2"><label id="lblSeatPriceHD" class="gridHDFont fntBold">'+UI_Container.labels.lblSeatPriceHD+'&nbsp; ('+UI_Anci.currency.selected+')</label></td>'+
			'</tr>';

			$.each(innerData, function(j,w){
				var bgClass = (j%2 == 1) ? "odd":"even";
				var seatRef ='{paxId:'+j+',segId:'+parentIndex+'}';
				var infInd = '';
				if(w.paxType == 'PA'){
					infInd = UI_Anci.infantTypeSuffix;
				}
				if (UI_Container.isModifyAncillary() && !UI_Anci.modifyAnciAllowed(UI_Anci.anciType.SEAT) && w.currentAncillaries != null && w.currentAncillaries.length > 0 && w.currentAncillaries[parentIndex].airSeatDTO != null &&
					w.currentAncillaries[parentIndex].airSeatDTO.seatNumber != null && 
					w.currentAncillaries[parentIndex].airSeatDTO.seatNumber != ''){
					addHTML +='<tr id="seatPaxTmplt_'+parentIndex+'_'+j+'" class="'+bgClass+'">'+
					'<td align="center" class="defaultRowGap rowColor bdLeft bdBottom">'+
					'<input type="radio" name="radSeatPax" id="radSeatPax-'+ j +':tdSeatSeg_'+ parentIndex +'" disabled="disabled"></td>'+
					'<td class="rowColor bdBottom"><label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="seatNo">'+w.currentAncillaries[parentIndex].airSeatDTO.seatNumber+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="seatCharge">'+UI_Anci.convertAmountToSelectedCurrency(w.currentAncillaries[parentIndex].airSeatDTO.seatCharge)+'</label></td>'+
					'<td align="center" class="rowColor bdRight bdBottom"><span name="clear"><label> &nbsp; </label>'+
					'</span></td>'+
					'</tr>';
				}else if(UI_Container.isModifySegment() && UI_Anci.jsAnciSegModel[segData.segInd].pax[j].seat!= null &&
						UI_Anci.jsAnciSegModel[segData.segInd].pax[j].seat.seatNumber != null && 
						UI_Anci.jsAnciSegModel[segData.segInd].pax[j].seat.seatNumber != ''){
					var seatNo = UI_Anci.jsAnciSegModel[segData.segInd].pax[j].seat.seatNumber;
					var seatCharge = UI_Anci.jsAnciSegModel[segData.segInd].pax[j].seat.seatCharge;
					addHTML +='<tr id="seatPaxTmplt_'+parentIndex+'_'+j+'" class="'+bgClass+'">'+
					'<td align="center" class="defaultRowGap rowColor bdLeft bdBottom">'+
					'<input type="radio" name="radSeatPax" id="radSeatPax-'+ j +':tdSeatSeg_'+ parentIndex +'" ></td>'+
					'<td class="rowColor bdBottom"><label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="seatNo">'+seatNo+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="seatCharge">'+UI_Anci.convertAmountToSelectedCurrency(seatCharge)+'</label></td>'+
					'<td align="center" class="rowColor bdRight bdBottom"><span name="clear">'+
					'<a href="#" class="noTextDec clearLabel" onclick="UI_Anci.removeSeatPax(' + seatRef + ');  return false;" title='+UI_Anci.jsonLabel["lblSeatClear"]+'>&nbsp;</a>'+
					'</span></td>'+
					'</tr>';
				}else{
					addHTML +='<tr id="seatPaxTmplt_'+parentIndex+'_'+j+'" class="'+bgClass+'">'+
					'<td align="center" class="defaultRowGap rowColor bdLeft bdBottom">'+
					'<input type="radio" name="radSeatPax" id="radSeatPax-'+ j +':tdSeatSeg_'+ parentIndex +'"></td>'+
					'<td class="rowColor bdBottom"><label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="seatNo"> - </label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="seatCharge"> - </label></td>'+
					'<td align="center" class="rowColor bdRight bdBottom"><span name="clear">'+
					'<a href="#" class="noTextDec clearLabel" onclick="UI_Anci.removeSeatPax(' + seatRef + ');  return false;" title='+UI_Anci.jsonLabel["lblSeatClear"]+'>&nbsp;</a>'+
					'</span>  </td>'+
					'</tr>';
				}
			});
			addHTML += '</table>';
		return addHTML;
	};
	
	/*
	 * Segments for baggages
	 */
	UI_Anci.buildSegmentsBaggageTab = function(p){
		var segmentCodeString = "";
		var segmentCodeArray = "";
		var curOriginCode = "";
		var lastDestCode ="";
		var ond = "";
		var ondReturn="";
		var hasReturn = false;
		var availableSegs = UI_Anci.jsAnciAvail.baggages;
		if(UI_Anci.isONDBaggage == "Y"){
		
				for (var k = 0 ;  k < UI_Anci.jsAnciSeg.length; k++){		
					if(!UI_Anci.jsAnciSeg[k].flightSegmentTO.returnFlag ){
						segmentCodeString += availableSegs[k].segCode + " ";
						
					} else {
						hasReturn=true;
					}
				}
				segmentCodeArray = segmentCodeString.split(" ");
				for(var i=0;i<segmentCodeArray.length;i++){
					if(i==0){
						curOriginCode = segmentCodeArray[i].split("/")[0];
					}if(i==(segmentCodeArray.length-2)){
						lastDestCode = segmentCodeArray[i].split("/")[1];
					}
				}
				ond = curOriginCode + "/" + lastDestCode;
				
				availableSegs = jQuery.makeArray(UI_Anci.jsAnciAvail.baggages[0]);
				availableSegs[0].segCode=ond;
				
				if(hasReturn){
					ondReturn = lastDestCode + "/" + curOriginCode;
					var copyObj = jQuery.extend(true, {}, UI_Anci.jsAnciAvail.baggages[0]);
					copyObj.segCode=ondReturn;
					copyObj.segInd = 1;
					availableSegs.push(copyObj);
				}
		
		}
		var adultChild = UI_Anci.jsonPaxAdults;		
		var addHTML = "";
		var obExist = false;
		var ibExist = false;
		var addInboundHTML = "";
		var addOutboundHTML = "";
		var j = 0;
		var i = 0;
        var fltSegIndex;

		UI_Anci.selectedBaggages = new Array();	
		
		UI_Anci.ondBaggageObj = UI_Anci.getOndBaggageInfoObj();
		
		while (j < UI_Anci.jsBaggageModel.length){
            fltSegIndex = UI_Anci.getFlightSegIdOfBaggageSegId(UI_Anci.jsBaggageModel[j].flightSegmentTO.flightRefNumber);

			if (!UI_Anci.jsBaggageModel[j].flightSegmentTO.returnFlag){
				if (availableSegs[i] != undefined){
					//if (availableSegs[i].segInd == j){
					
						if (UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId != null) {
							if (j == UI_Anci.ondBaggageObj.firstSegId[UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId]) {
								addOutboundHTML += '<div id="tdBaggageSeg_'+ fltSegIndex +'" class="segs-full-baggage">'+
								UI_Anci.buildBaggageInner(UI_Anci.ondBaggageObj.ondCodes[UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId],adultChild,fltSegIndex)+'</div>';
							} else {
								addOutboundHTML += '<div id="tdBaggageSeg_'+ fltSegIndex +'" class="segs-full-baggage" style="display:none">'+
								UI_Anci.buildBaggageInner(UI_Anci.ondBaggageObj.ondCodes[UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId],adultChild,fltSegIndex)+'</div>';
							}
						} else {
							addOutboundHTML += '<div id="tdBaggageSeg_'+ fltSegIndex +'" class="segs-full-baggage">'+
							UI_Anci.buildBaggageInner(availableSegs[i].segCode + "",adultChild,fltSegIndex)+'</div>';
						}
						
						i++;
						obExist = true;
					//}else{
					//	addOutboundHTML += '<div  class="segs-full-baggage NoData">&nbsp;</div>';
					//}
				}else{
					addOutboundHTML += '<div class="segs-full-baggage NoData">&nbsp;</div>';
				}
			}else{
				if (availableSegs[i] != undefined){
					//if (availableSegs[i].segInd == j){
						if (UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId != null) {
							if (j == UI_Anci.ondBaggageObj.firstSegId[UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId]) {
								addInboundHTML += '<div id="tdBaggageSeg_'+ fltSegIndex +'" class="segs-full-baggage">'+
								UI_Anci.buildBaggageInner(UI_Anci.ondBaggageObj.ondCodes[UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId],adultChild,fltSegIndex)+'</div>';
							} else {
								addInboundHTML += '<div id="tdBaggageSeg_'+ fltSegIndex +'" class="segs-full-baggage" style="display:none">'+
								UI_Anci.buildBaggageInner(UI_Anci.ondBaggageObj.ondCodes[UI_Anci.jsBaggageModel[j].flightSegmentTO.baggageONDGroupId],adultChild,fltSegIndex)+'</div>';
							}
						} else {
							addInboundHTML += '<div id="tdBaggageSeg_'+ fltSegIndex +'" class="segs-full-baggage">'+
							UI_Anci.buildBaggageInner(availableSegs[i].segCode + "",adultChild,fltSegIndex)+'</div>';
						}
						
						i++;
						ibExist = true;
					//}else{
					//	addInboundHTML += '<div  class="segs-full-baggage NoData"></div>';
					//}
				}else{
					addInboundHTML += '<div class="segs-full NoData"></div>';
				}
			}
			j++
		};

		addHTML += '<div class="outBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblOutbound ,UI_Anci.jsAnciSeg.length/2,obExist) +addOutboundHTML+'</div>'+
					'<div class="inBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblInbound ,UI_Anci.jsAnciSeg.length/2,ibExist) + addInboundHTML+'</div><div class="clear"></div>';
		
		$("#"+p.id).html(addHTML);
		
		var n = 0;
		for (var i = 0; i < availableSegs.length ; i++){
			for (var j = 0; j < adultChild.length; j++){
				var segmentPaxUsed = {};
				segmentPaxUsed.segment = availableSegs[i];
				segmentPaxUsed.baggageSegId = i;
				segmentPaxUsed.pax = adultChild[j];
				segmentPaxUsed.baggageUsed = false;
				if(UI_Container.isModifyAncillary()){
					if ( adultChild[j].currentAncillaries == undefined || adultChild[j].currentAncillaries == "")
						segmentPaxUsed.baggageUsed = false;
					else if (adultChild[j].currentAncillaries[i].baggages == undefined)
						segmentPaxUsed.baggageUsed = false;
					else if (adultChild[j].currentAncillaries[i].baggages == "")
						segmentPaxUsed.baggageUsed = false;
					else
						segmentPaxUsed.baggageUsed = true;
				}
					
				UI_Anci.segmentPaxBaggageUsedArray[n] = segmentPaxUsed;
				n++;
			}
		}

	}
	
	/*
	 * Build Baggage Inner parts
	 */
	UI_Anci.buildBaggageInner = function (ondCode,innerData,parentIndex){
		//var tmpBaggageOptions = '<option value="-">-</option><option value="0">20 Kg</option><option value="1">25 Kg</option><option value="2">30 Kg</option>';
		var tmpArr = new Array();
		var correctBaggageSegID = null;
		
		var addHTML = "";
			addHTML += '<table cellspacing="0" cellpadding="1" border="0" align="center" width="99%" class="seat-details">'+
			'<tbody>'+
			'<tr><td colspan="5" class="segcodebar"><label>'+UI_Container.getSegmentName(ondCode)+'</label></td></tr>'+
			'<tr>'+
				'<td align="center" width="5%" class="gridHD bdLeft bdBottom"><label class="gridHDFont fntBold">&nbsp;</label></td>'+
				'<td align="center" width="40%" class="gridHD bdBottom "><label id="lblSeatPaxNameHD" class="gridHDFont fntBold">'+UI_Container.labels.lblSeatPaxNameHD+'</label></td>'+
				'<td align="center" width="20%" class="gridHD bdBottom"><label id="lblBaggageTypeHD" class="gridHDFont fntBold">'+UI_Container.labels.lblBaggageTypeHD+'</label></td>'+
				'<td align="center" width="35%" class="gridHD bdBottom" colspan="2"><label id="lblBaggagePriceHD" class="gridHDFont fntBold">'+UI_Container.labels.lblBaggagePriceHD+' &nbsp;('+UI_Anci.currency.selected+')</label></td>'+
	
			'</tr>';
			
			
			$.each(innerData, function(j,w){
				var seatRef ='{paxId:'+j+',segId:'+parentIndex+'}';
				var bgClass = (j%2 == 1) ? "odd":"even";
				var infInd = '';
				if(w.paxType == 'PA'){
					infInd = UI_Anci.infantTypeSuffix;
				}
				var currAnciSegIndex = -1;
				baggageParentIndex = UI_Anci.getBaggageSegIndex(parentIndex);
				if(UI_Container.isModifySegment() && w.currentAncillaries != null &&  w.currentAncillaries.length > 0){
					var modifyingSegIndex = 0;
					for(var p=0;p<w.currentAncillaries.length;p++){
						if(UI_Container.isOneOfModifyingSeg(w.currentAncillaries[p].flightSegmentTO.flightRefNumber)){
							if(modifyingSegIndex == baggageParentIndex){
								currAnciSegIndex = p;
								break;
							}
							modifyingSegIndex ++;
						}
					}
				}
				if (UI_Container.isModifyAncillary() && !UI_Anci.modifyAnciAllowed(UI_Anci.anciType.BAGGAGE) && w.currentAncillaries != null && w.currentAncillaries.length > 0
						&& w.currentAncillaries[parentIndex].baggageDTOs != null && w.currentAncillaries[parentIndex].baggageDTOs.length > 0){
					
					var baggageDes = w.currentAncillaries[parentIndex].baggageDTOs[0].baggageName;
					var baggageCharge = w.currentAncillaries[parentIndex].baggageDTOs[0].baggageCharge;
					
					var applyFormatter = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.BAGGAGES,baggageCharge, UI_Anci.getOndSequence(currAnciSegIndex));
					baggageCharge = $.airutil.format.currency(baggageCharge);
					if(applyFormatter){
						baggageCharge = UI_Anci.freeServiceDecorator;	
					}
					
					addHTML +='<tr id="baggagePaxTmplt_'+parentIndex+'_'+j+'" class="'+bgClass+'">'+
					'<td align="center" class="defaultRowGap rowColor bdLeft bdBottom">'+
					'<input type="radio" name="radBaggagePax" id="radBaggagePax-'+ j +':tdBaggageSeg_'+ parentIndex +'" disabled="disabled"></td>'+
					'<td class="rowColor bdRight bdBottom"><label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="baggageType">'+ baggageDes +'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="baggageCharge">'+ baggageCharge +'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><span name="clear"><label>  </label>'+
					'</span></td>'+
					'</tr>';
				}else if(UI_Container.isModifySegment() && UI_Anci.jsAnciSegModel[parentIndex].pax[j].baggage!= null &&
						UI_Anci.jsAnciSegModel[parentIndex].pax[j].baggage.baggageName != null && 
						UI_Anci.jsAnciSegModel[parentIndex].pax[j].baggage.baggageCharge != ''){
					var baggageDes = UI_Anci.jsAnciSegModel[parentIndex].pax[j].baggage.baggageName;
					var baggageCharge = UI_Anci.jsAnciSegModel[parentIndex].pax[j].baggage.baggageCharge;
					var applyFormatter = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.BAGGAGES,baggageCharge, UI_Anci.getOndSequence(currAnciSegIndex));
					baggageCharge = $.airutil.format.currency(baggageCharge);
					if(applyFormatter){
						baggageCharge = UI_Anci.freeServiceDecorator;	
					}
					addHTML +='<tr id="baggagePaxTmplt_'+parentIndex+'_'+j+'" class="'+bgClass+'">'+
					'<td align="center" class="defaultRowGap rowColor bdLeft bdBottom ">'+
					'<input type="radio" name="radBaggagePax" id="radBaggagePax-'+ j +':tdBaggageSeg_'+ parentIndex +'" ></td>'+
					'<td class="rowColor bdBottom"><label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="baggageType">'+baggageDes+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="baggageCharge">'+baggageCharge+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><span name="clear">'+
					'<a href="#" class="noTextDec clearLabel" onclick="UI_Anci.paxBaggageRemoveWrapper(' + seatRef + ');  return false;" title="'+UI_Anci.jsonLabel["lblBaggageClear"]+'">&nbsp;</a>'+
					'</span></td>'+
					'</tr>';
				}
				else if (UI_Container.isModifySegment() && w.currentAncillaries != null && w.currentAncillaries.length > 0 && currAnciSegIndex !=-1
						&& w.currentAncillaries[currAnciSegIndex].baggageDTOs != null && w.currentAncillaries[currAnciSegIndex].baggageDTOs.length > 0){
					
					var baggageName = w.currentAncillaries[currAnciSegIndex].baggageDTOs[0].baggageName;
					var baggageCharge = w.currentAncillaries[currAnciSegIndex].baggageDTOs[0].baggageCharge;
                    var baggageId = -1;

                    if (UI_Anci.isBaggagesSelectedForCurrentBooking) {
                        //correctBaggageSegID = UI_Anci.getBaggageSegIndexSegmentCodeWise(w.currentAncillaries[currAnciSegIndex].flightSegmentTO.segmentCode);
                        correctBaggageSegID = modifyingSegIndex;
                        baggageId = UI_Anci.getBaggageOptionId(correctBaggageSegID,baggageName)
                    }

                    if(baggageName != null && baggageCharge != null){
                    	for(var k=0;k<UI_Anci.jsBaggageModel[baggageParentIndex].baggages.length;k++){
							if(UI_Anci.jsBaggageModel[baggageParentIndex].baggages[k].baggageName == baggageName){
								UI_Anci.isBaggagesAvailableToReprotect = true;
							} 
						}
                    }
                    
                    // avoiding default baggage selection, if baggage is reprotectable
					if(!UI_Anci.isBaggagesAvailableToReprotect && ((!UI_Anci.isBaggagesSelectedForCurrentBooking) || (UI_Anci.isBaggagesSelectedForCurrentBooking && baggageId == -1))){

						correctBaggageSegID = UI_Anci.getBaggageSegIndex(parentIndex);
                        baggageName = UI_Anci.jsBaggageModel[correctBaggageSegID].baggages[0].baggageName;
                        baggageCharge = UI_Anci.jsBaggageModel[correctBaggageSegID].baggages[0].baggageCharge;

                        for(var k=0;k<UI_Anci.jsBaggageModel[correctBaggageSegID].baggages.length;k++){

							if(UI_Anci.jsBaggageModel[correctBaggageSegID].baggages[k].defaultBaggage == "Y"){
								baggageName = UI_Anci.jsBaggageModel[correctBaggageSegID].baggages[k].baggageName;
								baggageCharge = UI_Anci.jsBaggageModel[correctBaggageSegID].baggages[k].baggageCharge;
								break;
							} 
						}
					} else{
						//correctBaggageSegID = UI_Anci.getBaggageSegIndexSegmentCodeWise(w.currentAncillaries[currAnciSegIndex].flightSegmentTO.segmentCode);
						correctBaggageSegID = modifyingSegIndex;
						
						if(UI_Anci.jsBaggageModel[correctBaggageSegID].flightSegmentTO.baggageONDGroupId != null){
							for(var k=0;k<UI_Anci.jsBaggageModel[baggageParentIndex].baggages.length;k++){
								if(UI_Anci.jsBaggageModel[baggageParentIndex].baggages[k].baggageName == baggageName){
									baggageName = UI_Anci.jsBaggageModel[baggageParentIndex].baggages[k].baggageName;
									baggageCharge = UI_Anci.jsBaggageModel[baggageParentIndex].baggages[k].baggageCharge;
									break;
								} 
							}
						}
					}
					
					baggageId = UI_Anci.getBaggageOptionId(correctBaggageSegID,baggageName);
					UI_Anci.paxBaggageAdd(j,baggageId, parentIndex, true);
					
					var applyFormatter = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.BAGGAGES,baggageCharge, UI_Anci.getOndSequence(correctBaggageSegID));
					baggageCharge = $.airutil.format.currency(baggageCharge);
					if(applyFormatter){
						baggageCharge = UI_Anci.freeServiceDecorator;	
					}
									
					tmpArr[j] = baggageId;
					UI_Anci.selectedBaggages[baggageParentIndex] = tmpArr;
					addHTML +='<tr id="baggagePaxTmplt_'+parentIndex+'_'+j+'" class="'+bgClass+'">'+
					'<td align="center" class="defaultRowGap rowColor bdLeft bdBottom">'+
					'<input type="radio" name="radBaggagePax" id="radBaggagePax-'+ j +':tdBaggageSeg_'+ parentIndex +'" ></td>'+
					'<td class="rowColor bdBottom"><label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><select name="selBaggageType" id="selBaggageType"></select></td>'+
					'<td align="center" class="rowColor bdBottom"><label name="baggageCharge">'+ baggageCharge +'</label></td>'+
					'<td align="center" class="rowColor bdBottom"><span name="clear">'+
					'<a href="#" class="noTextDec clearLabel" onclick="UI_Anci.paxBaggageRemoveWrapper(' + parentIndex +','+ j + ');  return false;" title="'+UI_Anci.jsonLabel["lblBaggageClear"]+'">&nbsp;</a>'+
					'</span>  </td>'+
					'</tr>';
				}
				else{
					addHTML +='<tr id="baggagePaxTmplt_'+parentIndex+'_'+j+'" class="'+bgClass+'">'+
					'<td align="center" class="defaultRowGap rowColor bdLeft bdBottom ">'+
					'<input type="radio" name="radBaggagePax" id="radBaggagePax-'+ j +':tdBaggageSeg_'+ parentIndex +'"></td>'+
					'<td class="rowColor bdBottom"><label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
					'<td align="center" class="rowColor  bdBottom"><select name="selBaggageType" id="selBaggageType"></select></td>'+
					'<td align="center" class="rowColor  bdBottom"><label name="baggageCharge"> - </label></td>'+
					'<td align="center" class="rowColor  bdBottom"><span name="clear">'+
					'<a href="#" class="noTextDec clearLabel" onclick="UI_Anci.paxBaggageRemoveWrapper(' + parentIndex +','+ j + ');  return false;" title="'+UI_Anci.jsonLabel["lblBaggageClear"]+'">&nbsp;</a>'+
					'</span>  </td>'+
					'</tr>';
					
				}
			});
			addHTML += '</table>';
		return addHTML;
	};
	
	/*
	 * Build Meals View
	 */
	UI_Anci.buildSegmentsMealTab = function(o){
		
		setFreeMeals = function(p){
			var str = ' '
			if (UI_Anci.jsAnciSeg[p].flightSegmentTO.freeMealEnabled){
				str +=UI_Container.labels.freeMealAvail;
			}
			return str;
		}
		var avalMealsPax = o.segs;
		var htmlStr = '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="anciTable active">'+
						'<tr>'+
							'<td class="segcodebar flightCol" align="left"><label class="gridHDFont fntBold" id="lblMealFlightHD">'+UI_Container.labels.lblMealFlightHD+'</label></td>'+
							'<td class="segcodebar passengerCol" align="left"><label class="gridHDFont fntBold" id="lblMealPaxHD">'+UI_Container.labels.lblMealPaxHD+'</label></td>'+
							'<td class="segcodebar mealCol" align="left"><label class="gridHDFont fntBold" id="lblMealMenuHD" style="width:80px;display:block">'+UI_Container.labels.lblMealMenuHD+'</label></td>'+
							'<td class="segcodebar" align="right" colspan="2" style="width:110px"><label class="gridHDFont fntBold" id="lblMealPriceHD">'+UI_Container.labels.lblMealPriceHD+'</label>'+
							'<label class="gridHDFont fntBold">(</label><label class="gridHDFont fntBold" id="lblCurrency">'+UI_Anci.currency.selected+'</label><label class="gridHDFont fntBold">)</label></td>'+
						'</tr>';
		$.each(avalMealsPax, function(i,j){
			htmlStr +='<tr><td valign="middle" align="left" class="flightCol defaultRowGap rowColor bdLeft bdRight bdBottom"><label>'+
			UI_Container.getSegmentName(j.segCode)+ '' + setFreeMeals(j.segInd) + '</label></td><td class="allOtherColl bdRight bdBottom" colspan="4" valign="top" align="left">'+
			buildMealPaxIN(UI_Anci.jsonPaxAdults,j.segInd)+'</td></tr>';
		});
		htmlStr +='<tr id="mealTotalROW"></tr></table>';
		/*htmlStr +='</table><div class="mealloader" style="display:none">'+
		'<table cellspacing="0" cellpadding="0" border="0" width="100%">'+
		'<tr><td class="headerBG hLeft"></td><td class="headerBG hCenter">'+
		'<label class="fntBold gridHDFont" id="lblMealSelector">'+UI_Container.labels.lblMealSelector+'</label><a class="closethis" href="javascript:void(0)">x</a>'+
		'</td><td class="headerBG hRight"></td></tr><tr><td colspan="3" align="left">'+
		'<div class="mealbody"><img class="loadingImg" src="../images/Loading-cal_no_cache.gif"/></div></td></tr></table>'+
		'</div>'*/
				
		$("#"+o.id).html(htmlStr);
		/*$.each($(".allOtherColl"), function(j,k){
			if ($(k).height() < $(k).find("table").height())
				$(k).find("table").css("height","100%")
			else
				$(k).find("table").css("height",$(k).height())
		});*/
	};
	
	function buildMealPaxIN(paxArr, segIndex){
		var innerHtml = '<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">';
		var getValidMeal = function(mealCode){
			if(UI_Anci.jsMealModel[segIndex] == null){
				return null;
			}
			var meals = UI_Anci.jsMealModel[segIndex].meals;
			var meal = null;
			for(var i = 0 ; i < meals.length; i++){
				if(meals[i].mealCode == mealCode){
					meal = meals[i];
					meal.mealIndex = i;
					break;
				}
			}
			return meal;
		}
		
		var isMultiMealEnabled = UI_Anci.jsMealModel[UI_Anci.getMealSegIndex(segIndex)].multiMealEnabledForLogicalCabinClass;
		
		var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.MEALS, null, UI_Anci.getOndSequence(segIndex));
		
		$.each(paxArr, function(i,j){
			if (i%2 == 1)
				innerHtml += '<tr class="even">';
			else
				innerHtml += '<tr class="odd">';
			var infType = '';
			if(j.paxType == 'PA'){
				infType = UI_Anci.infantTypeSuffix;
			}
			var currAnciSegIndex = -1;
			if(UI_Container.isModifySegment() && j.currentAncillaries != null &&  j.currentAncillaries.length > 0){
				var modifyingSegIndex = 0;
				for(var p=0;p<j.currentAncillaries.length;p++){
					if(UI_Container.isOneOfModifyingSeg(j.currentAncillaries[p].flightSegmentTO.flightRefNumber)){
						if(modifyingSegIndex == segIndex){
							currAnciSegIndex = p;
							break;
						}
						modifyingSegIndex ++;
					}
				}
			}
			innerHtml += '<td class="passengerCol defaultRowGap rowColor"><label>'+j.firstName+' '+j.lastName +infType+'</label></td>'+
			'<td class="defaultRowGap rowColor" colspan="3" id="multimeal_'+segIndex+'_'+i+'" valign="top">'+
			'<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">';
			if (UI_Container.isModifyAncillary() && !UI_Anci.modifyAnciAllowed(UI_Anci.anciType.MEAL)  && j.currentAncillaries != null &&  j.currentAncillaries.length > 0 && j.currentAncillaries[segIndex].mealDTOs != ""){
				for (var selMeal = 0; selMeal < j.currentAncillaries[segIndex].mealDTOs.length; selMeal++) {					
					var mealTotalPrice = UI_Anci.convertAmountToSelectedCurrency(j.currentAncillaries[segIndex].mealDTOs[selMeal].totalPrice);
					var applyFormatter = UI_Anci.isApplyDecorator(mealTotalPrice,isFreeService);
            		if(applyFormatter){
            			mealTotalPrice = UI_Anci.freeServiceDecorator;	
            		}
            		
            		var mealNameLabel = j.currentAncillaries[segIndex].mealDTOs[selMeal].soldMeals +'x'+j.currentAncillaries[segIndex].mealDTOs[selMeal].mealName;
            		if(!isMultiMealEnabled){
            			mealNameLabel = j.currentAncillaries[segIndex].mealDTOs[selMeal].mealName
            		}
            		
					innerHtml += '<tr><td class="mealCol defaultRowGap rowColor  "><label id="lblSelectMeal">'+mealNameLabel+'</label></td>'+
					'<td align="center" class="priceCol defaultRowGap rowColor  "><label id="price_'+segIndex+'_'+i+'">'+mealTotalPrice+'</label></td>'+
					'<td align="center" class="clearCol defaultRowGap rowColor ">&nbsp;</td>'+
					'</tr>'
				}
				innerHtml +='</table></td></tr>';
			}else if (UI_Container.isModifySegment()  && currAnciSegIndex>=0 && j.currentAncillaries[currAnciSegIndex].mealDTOs != ""){	
				var mealName = UI_Anci.jsonLabel["lblSelectMeal"];
				var mealCharge = ' - ';
				var clearStyle = 'style="display:none"';
				var idText =  segIndex+ '_'+i+'_0';
				var clearText = '';
				var hasReprotectMeal = false;
				var mealReprotected = {};
				var setNewMealTemplate = false;
				
				for (var selMeal = 0; selMeal < j.currentAncillaries[currAnciSegIndex].mealDTOs.length; selMeal++) {
					var skipReprotect = false;
					var meal = getValidMeal(j.currentAncillaries[currAnciSegIndex].mealDTOs[selMeal].mealCode);
					if(meal == null) {
						UI_Anci.notAllAnciReprotect = true; 
						setNewMealTemplate = true;
						continue;
					}								
					hasReprotectMeal = true;
					setNewMealTemplate = false;
					
					if(meal!=null){
						var mealIndex = meal.mealIndex;
						idText = segIndex+'_'+i+'_'+meal.mealCategoryID+'_'+mealIndex;
						var soldMeals = j.currentAncillaries[currAnciSegIndex].mealDTOs[selMeal].soldMeals;	
						
						if(!isMultiMealEnabled && mealReprotected[currAnciSegIndex]){
                    		skipReprotect = true;
                    	} else if(!isMultiMealEnabled){
                    		soldMeals = 1;                    		
                    	}
						
						if(!skipReprotect){						
							UI_Anci.paxMealAdd(i, mealIndex,soldMeals, segIndex);
							mealName = soldMeals+'x'+meal.mealName;
		            		if(!isMultiMealEnabled){
		            			mealName = meal.mealName;
		            		}
		            		if($("#resSelectedCurrency").val() != null && $("#resSelectedCurrency").val() != ''){
		            			mealCharge = UI_Anci.convertAmountToSelectedCurrency(meal.mealCharge * soldMeals);		            			
		            		} else {
		            			mealCharge = $.airutil.format.currency(meal.mealCharge * soldMeals);		            			
		            		}
							clearStyle = '';
							mealReprotected[currAnciSegIndex] = true;
						} else {
							UI_Anci.notAllAnciReprotect = true;
						}
					}
					if (UI_Anci.notAllAnciReprotect == false) {
						clearText = '<a title="Clear" style="display: block;" id="clear_'+idText+'"  '+ clearStyle +' class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>';
					}
					
					if(!skipReprotect){						
						var applyFormatter = UI_Anci.isApplyDecorator(mealCharge,isFreeService);					
						if(applyFormatter){
							mealCharge = UI_Anci.freeServiceDecorator;	
						}	
						
						innerHtml += '<tr><td class="mealCol defaultRowGap rowColor ">'+
						'<a id="meal_'+idText+'" class="selectAici selectMeal" href="javascript:void(0)"><label id="lblSelectMeal">'+mealName+'</label></a>'+
						'</td><td align="center" class="priceCol defaultRowGap rowColor  "><label>'+mealCharge+'</label></td>'+
						'<td align="center" class="clearCol defaultRowGap rowColor  ">'+ clearText +						
						'</td></tr>';	
					}
					
				}
				if(setNewMealTemplate){					
					innerHtml += '<tr><td class="mealCol defaultRowGap rowColor "><a href="javascript:void(0)" class="selectAici selectMeal" id="meal_'+segIndex+'_'+i+'"><label id="lblSelectMeal">'+UI_Anci.jsonLabel["lblSelectMeal"]+'</label></a></td>'+
					'<td class="priceCol defaultRowGap rowColor  " align="center"><label id="price_'+segIndex+'_'+i+'"> - </label></td>'+
					'<td class="clearCol defaultRowGap rowColor  " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_'+segIndex+'_'+i+'" style="display:none" title="'+UI_Anci.jsonLabel["lblSeatClear"]+'"></a></td>'+
					'</tr>';	
				} else if (hasReprotectMeal == false) {					
					innerHtml += '<tr><td class="mealCol defaultRowGap rowColor ">'+
					'<a id="meal_'+idText+'" class="selectAici selectMeal" href="javascript:void(0)"><label id="lblSelectMeal">'+mealName+'</label></a>'+
					'</td><td align="center" class="priceCol defaultRowGap rowColor  "><label>'+mealCharge+'</label></td>'+
					'<td align="center" class="clearCol defaultRowGap rowColor  ">'+ clearText +						
					'</td></tr>';	
				}
				innerHtml +='</table></td></tr>';
			}else if (UI_Anci.jsMealModel != null && UI_Anci.jsMealModel[UI_Anci.getMealSegIndex(segIndex)].meals.length != 0){
				innerHtml += '<tr><td class="mealCol defaultRowGap rowColor "><a href="javascript:void(0)" class="selectAici selectMeal" id="meal_'+segIndex+'_'+i+'"><label id="lblSelectMeal">'+UI_Anci.jsonLabel["lblSelectMeal"]+'</label></a></td>'+
				'<td class="priceCol defaultRowGap rowColor  " align="center"><label id="price_'+segIndex+'_'+i+'"> - </label></td>'+
				'<td class="clearCol defaultRowGap rowColor  " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_'+segIndex+'_'+i+'" style="display:none" title="'+UI_Anci.jsonLabel["lblSeatClear"]+'"></a></td>'+
				'</tr></table></td></tr>';
			}else{
				innerHtml += '<tr><td class="mealCol defaultRowGap rowColor"><a href="javascript:void(0)">&nbsp;</a></td>'+
				'<td class="priceCol defaultRowGap rowColor  " align="center"><label id="price_'+segIndex+'_'+i+'"> - </label></td>'+
				'<td class="clearCol defaultRowGap rowColor  " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_'+segIndex+'_'+i+'" style="display:none" title="'+UI_Anci.jsonLabel["lblSeatClear"]+'"></a></td>'+
				'</tr></table></td></tr>';
			}
		});
		innerHtml += '</table>';
		return innerHtml;
	}
	

	/*
	 * Get availability model segment code for given index
	 */
	UI_Anci.getFltRefNoForIndex = function(index){
		var fltRefNo = UI_Anci.jsAnciSeg[index].flightSegmentTO.flightRefNumber;
		return fltRefNo;
	}
		
	UI_Anci.getCabinClassInfoForIndex = function(index){
		var cos = {'cc':UI_Anci.jsAnciSeg[index].flightSegmentTO.cabinClassCode, 'lcc':UI_Anci.jsAnciSeg[index].flightSegmentTO.logicalCabinClassCode};
		return cos;
	}
	
	UI_Anci.getAnciSegIndexFromAPIndex = function(index){
		var fltRefNo = UI_Anci.jsHalaFlightSegmentModel[index].flightRefNumber;
		fltRefNo = _getActualFlightReferenceNumber(fltRefNo);
		
		var anciInd = 0;
		for(var i=0;i<UI_Anci.jsAnciSeg.length;i++){
			if(fltRefNo == _getActualFlightReferenceNumber(UI_Anci.jsAnciSeg[i].flightSegmentTO.flightRefNumber)){
				return anciInd = i;
				break;
			}
		}
		
		return anciInd;
	}
	
	/**
	 * 
	 *########################################Flexi Related Methods ##########################################
	 * 
	 */
	
	
	UI_Anci.buildONDFlexiTab = function(p){
		var obExist = false, ibExist = false, addOutboundHTML = "", addInboundHTML = "",enable="" , checked = "";
		availableSegs = UI_Anci.getONDFormSeg(p.segs);
		if (UI_Anci.ondAvailbleFlexi != null) {
			var ondFlexiCharge = _parseJSONStringToObject($("#resOndwiseFlexiChargesForAnci").val());
			$.each(availableSegs, function(i, segObj){
				
				var flexiChgStr = '';
				if(ondFlexiCharge != null && ondFlexiCharge[segObj.ondSeq] != undefined 
						&& ondFlexiCharge[segObj.ondSeq] != null
						&& ondFlexiCharge[segObj.ondSeq] != ''){
					if(UI_Anci.currency.selected != null && UI_Anci.currency.selected != ''){
						flexiChgStr += UI_Anci.currency.selected + ' ' + UI_Anci.convertAmountToSelectedCurrency(ondFlexiCharge[segObj.ondSeq]);
					} else {
						flexiChgStr += UI_Anci.currency.base + ' ' + ondFlexiCharge[segObj.ondSeq];
					}					
				}

				var applyFormatter = UI_Anci.isApplyFreeServiceDecorator("FLEXI_CHARGES",flexiChgStr,segObj.ondSeq);                		
        		if(applyFormatter){
        			flexiChgStr = UI_Anci.freeServiceDecorator;	
        		}	
				
				if (UI_Anci.ondAvailbleFlexi[segObj.ondSeq] && !segObj.returnFlag){
					enable = (UI_Anci.jsAnciFlexiAdded[segObj.ondSeq])?"disabled='disabled'":"";
					if(enable == ""){
						enable = (!UI_Anci.ondAvailbleFlexi[segObj.ondSeq])?"disabled='disabled'":"";
					}
					checked = (UI_Anci.jsAnciFlexiAdded[segObj.ondSeq])?"checked='true'":"";
					addOutboundHTML += '<div id="tdFlexiSeg_'+ segObj.ondSeq +'" class="segs-full-baggage">';
					addOutboundHTML += '<div class="segcodebar"><input type="checkbox" id="chkFlexiAvail_'+segObj.ondSeq+'" '+enable+' '+checked+' class="addFlexiToOND"/><label>' + UI_Container.getSegmentName(segObj.segCode) + '&nbsp;&nbsp;&nbsp;&nbsp;' + flexiChgStr + '</label></div>';
					addOutboundHTML += '</div>';
					obExist = true;
				} if (UI_Anci.ondAvailbleFlexi[segObj.ondSeq] && segObj.returnFlag){
					enable = (UI_Anci.jsAnciFlexiAdded[segObj.ondSeq])?"disabled='disabled'":"";
					if(enable == ""){
						enable = (!UI_Anci.ondAvailbleFlexi[segObj.ondSeq])?"disabled='disabled'":"";
					}
					checked = (UI_Anci.jsAnciFlexiAdded[segObj.ondSeq])?"checked='true'":"";
					addInboundHTML += '<div id="tdFlexiSeg_'+ segObj.ondSeq +'" class="segs-full-baggage">';
					addInboundHTML += '<div class="segcodebar"><input type="checkbox" id="chkFlexiAvail_'+segObj.ondSeq+'" '+enable+' '+checked+' class="addFlexiToOND"/><label>' + UI_Container.getSegmentName(segObj.segCode) + '&nbsp;&nbsp;&nbsp;&nbsp;' + flexiChgStr + '</label></div>';
					addInboundHTML += '</div>';
					ibExist = true;
				}
			});
		}else{
			addOutboundHTML += '<div class="segs-full-baggage NoData">&nbsp;</div>';
		}
		
		if(!(ibExist || obExist)){
			$("#tblFlexi").hide();
		} else {
			var addHTML = '<div class="outBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblOutbound ,UI_Anci.jsAnciSeg.length/2,obExist) + addOutboundHTML +'</div>'+
			'<div class="inBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblInbound ,UI_Anci.jsAnciSeg.length/2,ibExist) + addInboundHTML+'</div><div class="clear"></div>';

			$("#"+p.id).html(addHTML);
			$("#tblFlexi").show();
			
			$(".addFlexiToOND").unbind('click').bind('click', function(){
				var temp = this.id.split("_")[1];
				if($(this).prop('checked')){
					UI_Anci.jsAnciFlexiUAdded[temp] = true;
					
				}else{
					UI_Anci.jsAnciFlexiUAdded[temp] = false;
				}
				UI_Anci.manipulateFlexiSelectionParam();
			});
			
			UI_Anci.extractFlexiChargesAndModifyPaymentBreakdown();
		}		
	};
	
	/**
	 * When Flexi is selected in anci page, all the flexi charges will be reduced
	 * from surcharges and displayed separately
	 * */
	UI_Anci.extractFlexiChargesAndModifyPaymentBreakdown = function(){
		
		var totalFlexi = 0;
		var fareQuote = UI_Container.fareQuote;
		var ondTotalFlexiChargeObj = _parseJSONStringToObject($("#resOndwiseFlexiChargesForAnci").val());

		// flexy for selected flexy fairs
		if(UI_Container.isModifySegment() == false){
			for(var i = 0; i < Object.keys(UI_Anci.jsAnciFlexiAdded).length; i++){
				if(UI_Anci.jsAnciFlexiAdded[i]){
					var flexiCharge = ondTotalFlexiChargeObj[i];
					totalFlexi = parseFloat(totalFlexi) + parseFloat(flexiCharge);
					
					fareQuote.segmentFare[i].totalTaxSurcharges = fareQuote.segmentFare[i].totalTaxSurcharges - flexiCharge;
					fareQuote.segmentFare[i].totalTaxSurchargesInSelectedCurr = UI_Anci.convertAmountToSelectedCurrency(fareQuote.segmentFare[i].totalTaxSurcharges);
					
					fareQuote.segmentFare[i].totalPrice = fareQuote.segmentFare[i].totalPrice - flexiCharge;
					fareQuote.segmentFare[i].totalPriceInSelectedCurr = UI_Anci.convertAmountToSelectedCurrency(fareQuote.segmentFare[i].totalPrice - flexiCharge);
				}
			}
		}
		
		// total fareQuote Flexi
		UI_Container.fareQuoteOndFlexiChargesTotal = totalFlexi;
		
		if(UI_Container.fareQuoteOndFlexiChargesTotal > 0){
			$.fn.summaryPanel.update('paymentSummary');
		}
	}
			
	UI_Anci.getONDFormSeg = function(getObj){
		
		var segmentCodeString = "";
		var segmentCodeArray = "";
		var curOriginCode = "";
		var lastDestCode ="";
		var ond = "";
		var ondReturn="";
		var hasReturn = false;
		var availableSegs = getObj;
		
		var ondFlexiSegString  = {};
		
		_isInboundModification = function(jsAnciSeg){
			for (var k = 0 ;  k < jsAnciSeg.length; k++){		
				if(!jsAnciSeg[k].flightSegmentTO.returnFlag){
					return false;
				}
			}
			
			return true;
		}
		
		if(availableSegs.length > 0) {
			var isInboundModification = _isInboundModification(UI_Anci.jsAnciSeg);
			var activeCounter = 0;
			for (var k = 0 ;  k < UI_Anci.jsAnciSeg.length; k++){
				if(!UI_Anci.jsAnciSeg[k].flexi){
					continue;
				}
				
				if(isInboundModification){
					hasReturn=true;
					var ondSeq = UI_Anci.jsAnciSeg[k].flightSegmentTO.ondSequence;
					var segmentCodeString = "";
					if(ondFlexiSegString[ondSeq] != undefined &&
							ondFlexiSegString[ondSeq] != null){
						segmentCodeString = ondFlexiSegString[ondSeq];
					}
					
					segmentCodeString += availableSegs[activeCounter].segCode + " ";
					
					ondFlexiSegString[ondSeq] = segmentCodeString;
				} else if(!UI_Anci.jsAnciSeg[k].flightSegmentTO.returnFlag ){					
					var ondSeq = UI_Anci.jsAnciSeg[k].flightSegmentTO.ondSequence;
					var segmentCodeString = "";
					if(ondFlexiSegString[ondSeq] != undefined &&
							ondFlexiSegString[ondSeq] != null){
						segmentCodeString = ondFlexiSegString[ondSeq];
					}
					
					segmentCodeString += availableSegs[activeCounter].segCode + " ";
					
					ondFlexiSegString[ondSeq] = segmentCodeString;
				} else {
					hasReturn=true;
				}
				
				activeCounter++;
			}
			
			var ondFlexiSegInfo = new Array();
			
			$.each(ondFlexiSegString, function(key, value){
				segmentCodeArray = $.trim(value).split(" ");
				for(var i=0;i<segmentCodeArray.length;i++){
					if(i==0){
						curOriginCode = segmentCodeArray[i].split("/")[0];
					}if(i==(segmentCodeArray.length-1)){
						lastDestCode = segmentCodeArray[i].split("/")[1];
					}
				}
				ond = curOriginCode + "/" + lastDestCode;
				
				if(!isInboundModification){					
					var flexiObj = {ondSeq: key, segCode : ond, returnFlag: false};
					ondFlexiSegInfo.push(flexiObj);
				}
				
				if(hasReturn){
					if(isInboundModification){
						ondReturn = ond;
					} else {						
						ondReturn = lastDestCode + "/" + curOriginCode;
					}
					flexiObj = {ondSeq: 1, segCode : ondReturn, returnFlag: true};
					ondFlexiSegInfo.push(flexiObj);
				}
				
			});
			
			
			
		}
			
		return ondFlexiSegInfo;
	}
	
	/**
	 * 
	 *######################################## Airport Service Related Methods ##########################################
	 * 
	 */
	
	/*
	 * Load hala data
	 */
	UI_Anci.loadHalaData = function(sync){
		
		$("#tblHala").show();
		$("#tbdyHalaSeg").hide();
		UI_Anci.showLoadingMsg(UI_Anci.anciType.HALA);
		
		var data = {};
		
		if(UI_Container.isModifyAncillary() || UI_Container.isModifySegment()){
			data['resPaxInfo'] = $.toJSON(UI_Anci.jsonPaxAdults);
		}
		
		$("#frmReservation").ajaxSubmit({dataType: 'json',url:'interlineAnciAirportService.action', processData: false, async:!sync, data:data,		
				success: UI_Anci.onHalaLoadSuccess, error:UI_commonSystem.setErrorStatus});
	}
	
	
	/*
	 * On successful hala loading..
	 */
	UI_Anci.jsHalaModifyAirportMap = [];
	UI_Anci.onHalaLoadSuccess = function(response){
		
		if(response!=null){
			UI_Anci.jsHalaModel = response.airportServicesList;
			UI_Anci.jsHalaFlightSegmentModel = response.flightSegmentTOs;
			UI_Anci.jsHalaModel = $.airutil.sort.quickSort(UI_Anci.jsHalaModel,segmentComparator);
			UI_Anci.viewHalaInfo();			
			
			
			$("#tbdyBaggageSeg").show();
			
			UI_Anci.buildSegmentsHalaTab({id:"tbdyHalaSeg", tdID:"tdHalaSeg_", click:null, segs: UI_Anci.jsHalaFlightSegmentModel});
			UI_Anci.buildHalaPax();
			
			
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.HALA);
			UI_Anci.halaPromoOffer(response)
		}else{			
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.HALA);
			jConfirm('Error occured while loading airport services', 'Alert', function(){});
		}
	}
	
	UI_Anci.halaPromoOffer = function(p){
		if(p.anciOffer){
			//anciOfferName
			$(".halaPromoOffer").html(p.anciOfferDescription);
		}else{
			$(".halaPromoOffer").parent().parent().hide();
		}
	}
	
	/*
	 * Hala Service
	 */
	UI_Anci.viewHalaInfo = function(){
		$("#tblHalaSel").hide();
		$("#tbdyHalaSeg").show();
	}
	
	/* 
	 * Get index of the hala model segment
	 */
	UI_Anci.getHalaSegIndex = function(segId, airportCode){
		var intSegRow = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
		intSegRow = UI_Anci.getHalaSegIndexByFltRefNo(fltRefNo, airportCode);
		return intSegRow;
	}

    UI_Anci.getHalaSegIndexByFltRefNo = function(fltRefNo, airportCode){
		var intSegRow = null;
        if (UI_Anci.jsHalaModel != null) {
            for(var i=0; i < UI_Anci.jsHalaModel.length; i++){
                if(compareFltRefNos(fltRefNo,UI_Anci.jsHalaModel[i].flightSegmentTO.flightRefNumber) &&
                    UI_Anci.jsHalaModel[i].flightSegmentTO.airportCode == airportCode){
                    intSegRow = i;
                    break;
                }
            }
        }
		return intSegRow;
	}
	
	/*
	 * Segments for HALA
	 */
	 
	UI_Anci.notPerResApServices = [];
	
	UI_Anci.mapModifiedSegments = function(segindex, adultChild){
		
		if ( UI_Container.isModifySegment() ){
			UI_Anci.jsHalaModifyAirportMap = []
			$.each(UI_Anci.jsAnciAvail.hala , function(i,j){
				var apcodeMap = {newApCode: j.airportCode, segindex:j.segInd, modifyApCode:""}
				UI_Anci.jsHalaModifyAirportMap[UI_Anci.jsHalaModifyAirportMap.length] = apcodeMap;
			});
			
			var currAnciSegIndex = -1;
			var modifyingSegIndex = 0;
			
			//Mapping end to end 
			var oldSelmentModel = [];
			for (var i = 0; i < adultChild[0].currentAncillaries.length; i++){
				var t = adultChild[0].currentAncillaries[i].flightSegmentTO.segmentCode.split("/");
				if(adultChild[0].currentAncillaries[i].airportServiceDTOs != null){
					for(var y = 0 ; y < adultChild[0].currentAncillaries[i].airportServiceDTOs.length; y++){
						var t = adultChild[0].currentAncillaries[i].airportServiceDTOs[y].airportCode;
						if ($.inArray( t , oldSelmentModel) <= -1){
							oldSelmentModel[oldSelmentModel.length] = t;
					}
					}
				}
			}
			
			$.each(UI_Anci.jsHalaModifyAirportMap , function(k,l){
				if($.inArray( l.newApCode , oldSelmentModel) >= 0){
					l.modifyApCode = l.newApCode;
				}
			});
			
		}
	}
	
	
	UI_Anci.buildSegmentsHalaTab = function(p){
		UI_Anci.jsAnciAvail.hala = [];
		$.each( UI_Anci.jsHalaFlightSegmentModel, function(intIndex, obj){
			var segObj = {segCode: obj.segmentCode,segInd: UI_Anci.getAnciSegIndexFromAPIndex(intIndex), airportCode:obj.airportCode, returnFlag:obj.returnFlag };
			UI_Anci.jsAnciAvail.hala[intIndex] = segObj;
		});
		var availableSegs = UI_Anci.jsAnciAvail.hala;
		var adultChild = UI_Anci.jsonPaxAdults;	
		
		
		var addHTML = "";
		var obExist = false;
		var ibExist = false;
		var addInboundHTML = "";
		var addOutboundHTML = "";
		var j = 0;
		var i = 0;

		//UI_Anci.selectedhalas = new Array();	
		addOutboundHTML += '<div class="segs-full-baggage NoData">&nbsp;</div>';
		addInboundHTML += '<div class="segs-full-baggage NoData">&nbsp;</div>';
		while (j < availableSegs.length ){
			if (availableSegs[j].returnFlag == false){
				addOutboundHTML += '<div id="tdHalaSeg_'+ availableSegs[j].segInd +'_'+availableSegs[j].airportCode+'" class="segs-full-baggage">'+
				UI_Anci.buildHalaInner(availableSegs[j],adultChild,availableSegs[j].segInd)+'</div>';
				obExist = true;
			}else{
				addInboundHTML += '<div id="tdHalaSeg_'+ availableSegs[j].segInd +'_'+availableSegs[j].airportCode+'" class="segs-full-baggage">'+
				UI_Anci.buildHalaInner(availableSegs[j],adultChild,availableSegs[j].segInd)+'</div>';
				ibExist = true;
			}
			j++
		};

		addHTML += '<div class="outBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblOutbound ,UI_Anci.jsAnciSeg.length/2,obExist) +addOutboundHTML+'</div>'+
					'<div class="inBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblInbound ,UI_Anci.jsAnciSeg.length/2,ibExist) + addInboundHTML+'</div><div class="clear"></div>';
		
		$("#"+p.id).html(addHTML);
		
		var n = 0;
		for (var i = 0; i < availableSegs.length ; i++){
			for (var j = 0; j < adultChild.length; j++){
				var segmentPaxUsed = {};
				segmentPaxUsed.segment = availableSegs[i];
				segmentPaxUsed.halaSegId = i;
				segmentPaxUsed.pax = adultChild[j];
				segmentPaxUsed.halaUsed = false;
				if(UI_Container.isModifyAncillary()){
					if ( adultChild[j].currentAncillaries == undefined || adultChild[j].currentAncillaries == "")
						segmentPaxUsed.halaUsed = false;
					else if (adultChild[j].currentAncillaries[segmentPaxUsed.segment.segInd].apss == undefined)
						segmentPaxUsed.halaUsed = false;
					else if (adultChild[j].currentAncillaries[segmentPaxUsed.segment.segInd].apss == "")
						segmentPaxUsed.halaUsed = false;
					else
						segmentPaxUsed.halaUsed = true;
				}
					
				UI_Anci.segmentPaxHalaUsedArray[n] = segmentPaxUsed;
				n++;
			}
		}

	}
	
	UI_Anci.getAirportNameFromSegment = function(segCode, segName, airportCode){
		var airportName = airportCode;
		
		var segCodeArr = segCode.split("/");
		var segNameIndex = null;
		for (var i = 0; i < segCodeArr.length; i++){
			if(airportCode == segCodeArr[i]){
				segNameIndex = i;
				break;
			}
		}
		
		if(segNameIndex != null){			
			airportName = segName.split("/")[segNameIndex];
		}
		
		return $.trim(airportName);
	}
	
	/*
	 * Build Hala Inner parts
	 */
	
	
	UI_Anci.buildHalaInner = function (segData,innerData,segIndex){
		var selectedVIPSer = []
		var notPerResHala =[]
		perResAvailability = function(){
			var t = false;
			for (var i=0; i < UI_Anci.jsHalaModel.length;i++){
				var apServices = UI_Anci.jsHalaModel[i].airportServices;
				for(var j=0; j< apServices.length; j++){
					if (UI_Anci.ChargeType.PER_RESERVATION == apServices[j].applicabilityType){
						t = true;
						break;
					}
				}
			}
			return t;
		};
		

		getSelectedPerResServices = function(serviceList, adCh, segIndex, segData){
			var bool = false;
			$.each(serviceList.apServiceList, function(m, ser){
				if ( ser.applicabilityType == UI_Anci.ChargeType.PER_RESERVATION ){
					$.each(adCh, function(n, pax){
						if(pax.currentAncillaries != null && pax.currentAncillaries.length != 0){
							var currAnciSegIndex = -1;
							var modifyingSegIndex = 0;
							
							for(var p=0;p<pax.currentAncillaries.length;p++){
								if(UI_Container.isOneOfModifyingSeg(pax.currentAncillaries[p].flightSegmentTO.flightRefNumber)){
									if(modifyingSegIndex == segIndex){
										currAnciSegIndex = p;
										break;
									}
									modifyingSegIndex ++;
								}
							}
							var corAPIndex = currAnciSegIndex;
							if (currAnciSegIndex != -1 && pax.currentAncillaries[corAPIndex].airportServiceDTOs != null && 
								pax.currentAncillaries[corAPIndex].airportServiceDTOs.length != 0){
										$.each(pax.currentAncillaries[corAPIndex].airportServiceDTOs, function(o, selSer){
											if ( UI_Anci.getModifingAPcode(segData.airportCode) == selSer.airportCode && ser.ssrCode == selSer.ssrCode && segData.airportCode == serviceList.airport){
												bool = true;
												selSer.reservationAmount = ser.reservationAmount;//getSelReservationAmount(adCh, currAnciSegIndex, selSer.ssrCode);
												selSer.applicabilityType = UI_Anci.ChargeType.PER_RESERVATION;
												if ( selectedVIPSer.length == 0 ){
													selectedVIPSer.push(selSer);
												}else if( selectedVIPSer[selectedVIPSer.length - 1].ssrCode != selSer.ssrCode){
													selectedVIPSer.push(selSer);
												}
											}
										});
							}
						}
					});
				}
			});
			return bool;
		};
		
		getPerReservationServicesForAddAnci = function(serviceList, adCh, segIndex, airportCode){
			var addAnciPerResService = []
			$.each(serviceList, function(m, ser){
				if ( ser.applicabilityType == UI_Anci.ChargeType.PER_RESERVATION ){
					$.each(adCh, function(j,w){						
						if (w.currentAncillaries != null && w.currentAncillaries.length != 0) {													
							var apServicesList = w.currentAncillaries[segIndex].airportServiceDTOs;
							if(apServicesList != null){
								$.each(apServicesList, function(o, selSer){
									if (ser.ssrCode == selSer.ssrCode && airportCode == selSer.airportCode){
										selSer.reservationAmount = getSelReservationAmount(adCh, segIndex, selSer.ssrCode, airportCode);
										selSer.applicabilityType = UI_Anci.ChargeType.PER_RESERVATION;
										if ( addAnciPerResService.length == 0 ){
											addAnciPerResService.push(selSer);
										}else if( addAnciPerResService[addAnciPerResService.length - 1].ssrCode != selSer.ssrCode){
											addAnciPerResService.push(selSer);
										}
									}
								});
							}
						}
						
					});
				}
				
			});
			
			
			return addAnciPerResService;
		};
		
		getPerPaxServicesForAddAnci = function(serviceList, paxServiceList, airportCode){
			var addAnciPerPaxService = []
			$.each(serviceList, function(m, ser){
				if (ser.applicabilityType == UI_Anci.ChargeType.PER_PAX) {
					$.each(paxServiceList, function(j, paxSer){
						if (ser.ssrCode == paxSer.ssrCode &&
								paxSer.airportCode == airportCode){
							paxSer.applicabilityType = UI_Anci.ChargeType.PER_PAX;
							addAnciPerPaxService.push(paxSer);
						}
					});
				}
			});
			
			return addAnciPerPaxService;
		}
		
		getSelReservationAmount = function(paxList, selSegIndex, ssrCode, airPortCode){
			var serviceAmount = 0;
			
			$.each(paxList, function(n, pax){
				if(pax.currentAncillaries != null && pax.currentAncillaries.length != 0){
					if (pax.currentAncillaries[selSegIndex].airportServiceDTOs != null && 
							pax.currentAncillaries[selSegIndex].airportServiceDTOs.length != 0){
						$.each(pax.currentAncillaries[selSegIndex].airportServiceDTOs, function(o, selSer){
							if (ssrCode == selSer.ssrCode && selSer.airportCode == airPortCode){
								serviceAmount += selSer.serviceCharge;
							}
						});
					}
				}
			});
			
			return serviceAmount;
		}
		
		var apServiceList1 = UI_Anci.getFltSegAvailAPServiceList(segData.segCode, segData.airportCode);

		//Mapping end to end 
		UI_Anci.mapModifiedSegments(segIndex, innerData);		

		var tmpArr = new Array();
		var addHTML = "";
			addHTML += '<table cellspacing="0" cellpadding="1" border="0" align="center" width="99%" class="seat-details">'+
			'<tbody>'+
			'<tr><td colspan="5" class="segcodebar"><label>'+
			UI_Anci.getAirportNameFromSegment(segData.segCode, UI_Container.getSegmentName(segData.segCode), segData.airportCode)+'</label></td></tr>'+
			'<tr>'+
				'<td align="center" width="40%" class="gridHD bdBottom "><label id="lblSeatPaxNameHD" class="gridHDFont fntBold">'+UI_Container.labels.lblSeatPaxNameHD+'</label></td>'+
				'<td align="center" width="25%" class="gridHD bdBottom"><label id="lblHalaServiceHD" class="gridHDFont fntBold">'+UI_Container.labels.lblHalaServiceHD+'</label></td>'+
				'<td align="center" width="35%" class="gridHD bdBottom"><label id="lblBaggagePriceHD" class="gridHDFont fntBold">'+UI_Container.labels.lblBaggagePriceHD+' &nbsp;('+UI_Anci.currency.selected+')</label></td>'+
			'</tr>';
			if (perResAvailability()){
				if (UI_Container.isModifySegment() && getSelectedPerResServices(apServiceList1, innerData, segIndex, segData.airportCode)) { 
					addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_R_'+segData.airportCode+'" class="odd">'+
					'<td class="defaultRowGap rowColor bdLeft bdBottom ">'+
					'<label name="paxName">'+UI_Container.labels.lblHalaCommonHD+'</label></td>'+
					'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_-_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, "-", "R", selectedVIPSer, true, true, segData.airportCode)+'</td>'+
					'</tr>';
				} else if(UI_Container.isModifyAncillary()){
					var perResServiceList = getPerReservationServicesForAddAnci(apServiceList1.apServiceList, innerData, segIndex, segData.airportCode);
					
					if((perResServiceList == null || perResServiceList.length == 0) || UI_Anci.modifyAnciAllowed(UI_Anci.anciType.HALA)){
						addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_R_'+segData.airportCode+'" class="odd">'+
						'<td class="defaultRowGap rowColor bdLeft bdBottom ">'+
						'<label name="paxName">'+UI_Container.labels.lblHalaCommonHD+'</label></td>'+
						'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_-_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, "-", "R", null, true, true, segData.airportCode)+'</td>'+
						'</tr>';
					} else {
						addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_R_'+segData.airportCode+'" class="odd">'+
						'<td class="defaultRowGap rowColor bdLeft bdBottom ">'+
						'<label name="paxName">'+UI_Container.labels.lblHalaCommonHD+'</label></td>'+
						'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_-_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, "-", "R", perResServiceList, false, false, segData.airportCode)+'</td>'+
						'</tr>';
					}
				}else{
					addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_R_'+segData.airportCode+'" class="odd">'+
					'<td class="defaultRowGap rowColor bdLeft bdBottom ">'+
					'<label name="paxName">'+UI_Container.labels.lblHalaCommonHD+'</label></td>'+
					'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_-_'+segData.airportCode+'" colspan="3" valign="top">'+UI_Anci.buildHalaServices(segIndex, "-", "R", null, false, false, segData.airportCode)+'</td>'+
					'</tr>';
				}
			}
			
			$.each(innerData, function(j,w){
					var seatRef ='{paxId:'+j+',segId:'+segIndex+'}';
					var bgClass = (j%2 == 1) ? "odd":"even";
					var infInd = '';
					if(w.paxType == 'PA'){
						infInd = UI_Anci.infantTypeSuffix;
					}
					var currAnciSegIndex = -1;
					if(UI_Container.isModifySegment() && w.currentAncillaries != null &&  w.currentAncillaries.length > 0){
						var modifyingSegIndex = 0;
						for(var p=0;p<w.currentAncillaries.length;p++){
							if(UI_Container.isOneOfModifyingSeg(w.currentAncillaries[p].flightSegmentTO.flightRefNumber)){
								if(modifyingSegIndex == segIndex){
									currAnciSegIndex = p;
									break;
								}
								modifyingSegIndex ++;
							}
						}
					}
					
					if (UI_Container.isModifyAncillary() && w.currentAncillaries != null && w.currentAncillaries.length > 0 
							&& w.currentAncillaries[segIndex].airportServiceDTOs != null && w.currentAncillaries[segIndex].airportServiceDTOs.length > 0){
						
						var apServicesList = getPerPaxServicesForAddAnci(apServiceList1.apServiceList, w.currentAncillaries[segIndex].airportServiceDTOs, segData.airportCode);
						if((apServicesList == null || apServicesList.length == 0) || UI_Anci.modifyAnciAllowed(UI_Anci.anciType.HALA)){
							addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">'+
							'<td class="defaultRowGap rowColor bdLeft bdBottom">'+
							'<label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
							'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_'+j+'_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, j, w.paxType, null, true, true, segData.airportCode)+'</td>'+
							'</tr>';
						} else {
							addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">'+
							'<td class="defaultRowGap rowColor bdLeft bdBottom">'+
							'<label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
							'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_'+j+'_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, j, w.paxType, apServicesList, false, false, segData.airportCode)+'</td>'+
							'</tr>';
						}
					/*}else if(UI_Container.isModifySegment() && UI_Anci.jsAnciSegModel[segData.segInd].pax[j].aps != null &&
							UI_Anci.jsAnciSegModel[segData.segInd].pax[j].aps.apss != null && 
							UI_Anci.jsAnciSegModel[segData.segInd].pax[j].aps.apss != ''){
						var apServicesList = UI_Anci.jsAnciSegModel[segData.segInd].pax[j].aps.apss;
						addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">'+
						'<td class="defaultRowGap rowColor bdLeft bdBottom ">'+
						'<label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
						'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_'+j+'_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, j, w.paxType, apServicesList, false, true, segData.airportCode)+'</td>'+
						'</tr>';
					*/
					}else if (UI_Container.isModifySegment() && w.currentAncillaries != null && w.currentAncillaries.length > 0 && currAnciSegIndex !=-1
							&& w.currentAncillaries[currAnciSegIndex].airportServiceDTOs != null && w.currentAncillaries[currAnciSegIndex].airportServiceDTOs.length > 0){
						
						if (UI_Anci.getHalaSegIndex(segIndex, segData.airportCode) != null){
							UI_Anci.notPerResApServices = [];
							$.each( UI_Anci.jsHalaModel[UI_Anci.getHalaSegIndex(segIndex, segData.airportCode)].airportServices, function (i, obj){
								if (obj.applicabilityType != "R"){
									UI_Anci.notPerResApServices[UI_Anci.notPerResApServices.length] = obj;
								}
							});
						}
						
						var apServicesList = w.currentAncillaries[currAnciSegIndex].airportServiceDTOs;
						
						var adServiceperAP = [];//get the currect apservices per airport
						for (var d = 0; d<apServicesList.length;d++){
							if ( apServicesList[d].airportCode == UI_Anci.getModifingAPcode(segData.airportCode) &&
									UI_Anci.isAPavailableByCode(apServicesList[d].ssrCode) ){
								adServiceperAP.push(UI_Anci.getAPavailableByCode(apServicesList[d].ssrCode));
							}
						}
						
						/*for (var p = 0; p<adServiceperAP.length;p++){
							if (adServiceperAP[p].applicabilityType == "R"){
								adServiceperAP.splice(p,1);
							}
						}*/
						
						if (adServiceperAP.length == 0) {
							adServiceperAP = null;
						}
						if (adServiceperAP != null){
							$.each(adServiceperAP, function(k,t){
								UI_Anci.paxHalaAdd(j,t.ssrCode, segIndex, segData.airportCode);
							});
						}

						addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">'+
						'<td class="defaultRowGap rowColor bdLeft bdBottom">'+
						'<label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
						'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_'+j+'_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, j, w.paxType,  adServiceperAP, true, true, segData.airportCode)+'</td>'+
						'</tr>';

					}
					else{
						addHTML +='<tr id="halaPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">'+
						'<td class="defaultRowGap rowColor bdLeft bdBottom ">'+
						'<label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
						'<td align="center" class="rowColor  bdBottom bdRight" id="hService_'+segIndex+'_'+j+'_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildHalaServices(segIndex, j, w.paxType, null, false, false, segData.airportCode)+'</td>'+
						'</tr>';
					}
				});
			addHTML += '</table>';
		return addHTML;
	};
	
	UI_Anci.getFltSegAvailAPServiceList = function(segCode, airportCode){
		var tAPObj = {}
		for(var i=0;i<UI_Anci.jsHalaModel.length;i++){
			var fltSegTO = UI_Anci.jsHalaModel[i].flightSegmentTO;
			if(fltSegTO.segmentCode == segCode && fltSegTO.airportCode == airportCode){
				tAPObj.apServiceList = UI_Anci.jsHalaModel[i].airportServices;
				tAPObj.airport = fltSegTO.airportCode;
				break;
			}
		}
		
		return tAPObj;
	}
	
	UI_Anci.getModifingAPcode = function(newApCode){
		var t = "";
		for( var i = 0; i< UI_Anci.jsHalaModifyAirportMap.length; i++){
			if (newApCode == UI_Anci.jsHalaModifyAirportMap[i].newApCode){
				t = UI_Anci.jsHalaModifyAirportMap[i].modifyApCode;
				break;
			}
		}
		return t;
	};
	UI_Anci.isAPavailableByCode = function(code){
		var t = false;
		for (var i = 0; i < UI_Anci.notPerResApServices.length; i++){
			if (code == UI_Anci.notPerResApServices[i].ssrCode){
				t = true;
				break;
			}
		}
		return t;
	};
	UI_Anci.getAPavailableByCode = function(code){
		var t = null;
		for (var i = 0; i < UI_Anci.notPerResApServices.length; i++){
			if (code == UI_Anci.notPerResApServices[i].ssrCode){
				t = UI_Anci.notPerResApServices[i];
				break;
			}
		}
		return t;
	};


	UI_Anci.buildHalaServices = function(sIndex, pIndex, paxType, sList, removeFlag, modifyFlag, airportCode){
		var subHtml = '<table cellspacing="0" cellpadding="1" border="0" align="center" width="100%" >';
		if (sList == null){
			subHtml += 	'<tr id="trHala_'+sIndex+'_'+pIndex+'_'+paxType+'_'+airportCode+'">'+
							'<td class="rowColor"><a href="javascript:void(0)" class="selectAici selHalaService" id="selHala_'+sIndex+'_'+pIndex+'_'+paxType+'__'+airportCode+'"><label>'+UI_Container.labels.lblHalaSelectHD+'</label></a></td>'+
							'<td class="rowColor alignRight" width="50"><label name="halaCharge"> - </label></td>'+
							'<td align="center" class="rowColor" width="20"><span name="clear">&nbsp;</span> </td>'+
						'</tr>';
		}else{
			var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.AIRPORT_SERVICE, null, UI_Anci.getOndSequence(sIndex));
			
			$.each(sList, function(i,service){
				
					var chargeAmount = '';
					subHtml += 	'<tr id="trHala_'+sIndex+'_'+pIndex+'_'+paxType+'_'+service.ssrCode+'_'+airportCode+'">';
					if (modifyFlag){
						subHtml +='<td class="rowColor"><a href="javascript:void(0)" class="selectAici selHalaService" id="selHala_'+sIndex+'_'+pIndex+'_'+paxType+'_'+service.ssrCode+'_'+airportCode+'"><label>'+service.ssrName+'</label></a></td>';
					}else{
						subHtml +='<td class="rowColor"><label class="selHalaService">'+service.ssrName+'</label></td>';
					}
					if (paxType == "AD"){
						chargeAmount = UI_Anci.convertAmountToSelectedCurrency((service.adultAmount == null)?service.serviceCharge:service.adultAmount);
					}else if (paxType == "CH"){
						chargeAmount =UI_Anci.convertAmountToSelectedCurrency((service.childAmount == null)?service.serviceCharge:service.childAmount);
					}else if (paxType == "PA"){
						var t = (service.adultAmount == null)?service.serviceCharge:parseFloat(service.adultAmount) + parseFloat(service.infantAmount);
						chargeAmount = UI_Anci.convertAmountToSelectedCurrency(t);
					}else if (paxType == "R"){
						chargeAmount =  UI_Anci.convertAmountToSelectedCurrency((service.reservationAmount == null || service.reservationAmount == 0)?service.serviceCharge:service.reservationAmount);
					}
					
					if(service.isFree){							
						var applyFormatter = UI_Anci.isApplyDecorator(chargeAmount,isFreeService);
						if(applyFormatter){
							chargeAmount = UI_Anci.freeServiceDecorator;
						}
					}

					subHtml +='<td class="rowColor alignRight" width="50"><label name="halaCharge">'+ chargeAmount +'</label></td>';
					
					
					if (removeFlag){
						subHtml +='<td align="center" class="rowColor" width="20"><span name="clear">'+
							'<a href="javascript:void(0)" id="clearHala_'+sIndex+'_'+pIndex+'_'+paxType+'_'+service.ssrCode+'_'+airportCode+'" class="noTextDec clearLabel clearHala" title="'+UI_Anci.jsonLabel["lblBaggageClear"]+'">&nbsp;</a>'+
							'</span>  </td>';
					}else{
						subHtml +='<td align="center" class="rowColor" width="20"><span name="clear"><label>  </label></span></td>';
					}
					subHtml +='</tr>';
			});
		}
		subHtml +='</table>';
		return subHtml;
	};
	
	UI_Anci.closeHalaPopUp = function(eve){
		if (eve.target.className != "close"){
			var halaSelected = [];
			var segId = $.data($("#newPopItem")[0], "haladata").segId;
			var paxId = $.data($("#newPopItem")[0], "haladata").paxId;
			var paxType = $.data($("#newPopItem")[0], "haladata").paxType;
			var airportCode = $.data($("#newPopItem")[0], "haladata").airportCode;
			var intSegRow =  UI_Anci.getHalaSegIndex(segId, airportCode);
			var selectedChecbox = $("#halaPop").find("input[type='checkbox']:checked");
			var subHtml = '';
			$.each(selectedChecbox, function (intIndex){
				var temp = getHalaByCode(this.id.split("_")[1], intSegRow);
				halaSelected.push(temp);
			});
			if (halaSelected.length<=0){
				$("#hService_"+segId+'_'+paxId+'_'+airportCode).html(UI_Anci.buildHalaServices(segId, paxId, paxType, null, false, false, airportCode));
			}else{
				$("#hService_"+segId+'_'+paxId+'_'+airportCode).html(UI_Anci.buildHalaServices(segId, paxId, paxType, halaSelected, true, true, airportCode));
			}
			
			UI_Anci.halaPaxChanged({paxID:paxId,segID:segId,slHalas:halaSelected,airportCode:airportCode,paxType:paxType});
			UI_Anci.buildHalaPax();
		}
		$("#newPopItem").closeMyPopUp();
	}
	
	/*
	 * Handled the event of passenger changing hala
	 * Remove old one validate and add new hala
	 */
	UI_Anci.halaPaxChanged = function(evt){
		if(evt.paxID == '-'){
			UI_Anci.currAirportServiceApplicability = UI_Anci.ChargeType.PER_RESERVATION;
		} else {
			UI_Anci.currAirportServiceApplicability = UI_Anci.ChargeType.PER_PAX;
		}
		UI_Anci.removeHalaFromPaxObj(evt.paxID, evt.segID, null, evt.airportCode);
		$.each(evt.slHalas, function(){
			var count = 1;
			UI_Anci.paxHalaAdd(evt.paxID,this.ssrCode, UI_Anci.hala.currentSegId, evt.airportCode);	
		});
	}
	
	/*
	 * Add given meal for the segment to the pax
	 */
	UI_Anci.paxHalaAdd = function(paxId, ssrCode, segId, airportCode){
		var intSegRow = UI_Anci.getHalaSegIndex(segId, airportCode);
		
		var hala = UI_Anci.getSelHalaObj(UI_Anci.jsHalaModel[intSegRow].airportServices, ssrCode);

		if(paxId == "-"){
			for(var i=0; i < UI_Anci.jsAnciSegModel[segId].pax.length; i++){
				UI_Anci.addHalaForPaxObj(i, ssrCode, segId, hala, intSegRow);
			}
		} else {
			UI_Anci.addHalaForPaxObj(paxId, ssrCode, segId, hala, intSegRow);
		}
		UI_Anci.updateAnciCounts(UI_Anci.anciType.HALA,1,'ADD');
		UI_Anci.updateFlightAnciView();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
	}
	
	UI_Anci.addHalaForPaxObj = function(paxId, ssrCode, segId, hala, halaSegRowIndex){
		var fltSeg = UI_Anci.jsHalaModel[halaSegRowIndex].flightSegmentTO;
		var halaData = UI_Anci.jsAnciSegModel[segId].pax[paxId].aps;
		var paxHalas = halaData.apss;
		var halaId = null;
		var amt = UI_Anci.getPaxApplicableHalaCharge(hala, UI_Anci.jsAnciSegModel[segId].pax[paxId].paxType, paxId);
		for(var i = 0  ; i < paxHalas.length; i ++){
			if(paxHalas[i].ssrCode == hala.ssrCode && fltSeg.airportCode == paxHalas[i].airport){
				return;
			}
		}
		halaId=paxHalas.length;
		paxHalas[halaId] = {ssrCode:hala.ssrCode,
				ssrName:hala.ssrName,
				ssrDescription:hala.ssrDescription,
				charge:parseFloat(amt),
				subTotal:parseFloat(amt),
				applicableType:hala.applicabilityType,
				airportType:fltSeg.airportType,
				airport:fltSeg.airportCode,
				serviceNumber:hala.serviceNumber
		};
		
		halaData.apsChargeTotal += parseFloat(amt);
		
		UI_Anci.updateAnciTotals(UI_Anci.anciType.HALA,parseFloat(amt),'ADD');
	}
	
	UI_Anci.getSelHalaObj = function(apsList, ssrCode){
		var apsObj = {};
		for(var i=0;i<apsList.length;i++){
			if(apsList[i].ssrCode == ssrCode){
				apsObj = apsList[i];
				break;
			}
		}
		return apsObj;
	}
	
	UI_Anci.getPaxApplicableHalaCharge = function(halaObj, paxType, paxId){
		var serviceCharge = 0;
		if(halaObj.applicabilityType == UI_Anci.ChargeType.PER_RESERVATION){
			var amtArr = UI_Anci._splitWithoutLoss(halaObj.reservationAmount, UI_Anci.jsonPaxAdults.length);
			serviceCharge = amtArr[paxId];
		} else if(halaObj.applicabilityType == UI_Anci.ChargeType.PER_PAX){
			if(paxType == UI_Anci.PaxType.PARENT){
				serviceCharge = parseFloat(halaObj.adultAmount) + parseFloat(halaObj.infantAmount);
			} else if(paxType == UI_Anci.PaxType.ADULT){
				serviceCharge = parseFloat(halaObj.adultAmount);
			} else if(paxType == UI_Anci.PaxType.CHILD){
				serviceCharge = parseFloat(halaObj.childAmount);
			}
		}
		
		return $.airutil.format.currency(serviceCharge);
	}
	
	/*
	 * AccelAero split without loss
	 */
	UI_Anci._splitWithoutLoss = function(amt, count){
		if(count == undefined || count == null){			
			count = UI_Anci.jsonPaxAdults.length;
		}
		var sliced = new Array();
		amt = parseFloat(amt);
		if(amt!=null){
			var piece = $.airutil.format.currency(amt/count);
			piece = parseFloat(piece);
			var allExceptOneTotal = 0;
			for(var i = 0 ; i < count-1; i++){
				allExceptOneTotal += piece;
				sliced[i] = piece;
			}
			sliced[sliced.length]  = (amt-allExceptOneTotal);
		}
		return sliced;
	}
	
	// get Hala service by code
	function getHalaByCode(code, selectedSegIndex, airportCode){
		var ret = null;
		var apServices = UI_Anci.jsHalaModel[selectedSegIndex].airportServices;
		for (var i=0; i < apServices.length;i++){
			if (code == apServices[i].ssrCode){
				ret = apServices[i];
				if(code == "FQTV"){
					ret['serviceNumber'] = $("#txtServiceNumber_" + code).val();
				} else {
					ret['serviceNumber'] = null;
				}				
				break;
			}
		}
		return ret;
	}
	
	//Hala build
	UI_Anci.buildHalaPax = function(){
		$("a.selHalaService").unbind("click").bind("click", UI_Anci.halaServiceSelect);
		$(".clearHala").unbind("click").bind("click", UI_Anci.paxHalaRemoveWrapper);
	};
	
	//Hala popup build
	UI_Anci.buildPopupHala = function(paxId, paxType, choosedHala, airportCode){
		//inner function for HALA
		checkChoosedHalas = function(dataArray, halaCode){
			var ret = false;
			for (var i = 0; i < dataArray.length; i++){
				if (dataArray[i].ssrCode == halaCode ){
						ret = true;
						break;
					}
				}
			return ret
		};
		
		
		var ret = '<table cellspacing="0" cellpadding="1" border="0" align="center" width="100%" id="halaPop"><tr><td>';
		var t = 0;
		var bgClass = "";
		var intSegRow = UI_Anci.getHalaSegIndex(UI_Anci.hala.currentSegId, airportCode);
		var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.AIRPORT_SERVICE, null, UI_Anci.getOndSequence(UI_Anci.hala.currentSegId));
		
		if (paxType == "R"){
			$.each(UI_Anci.jsHalaModel[intSegRow].airportServices, function(i, j){
				if (UI_Anci.ChargeType.PER_RESERVATION == j.applicabilityType){
					var frmtAmount = UI_Anci.convertAmountToSelectedCurrency(j.reservationAmount);
					var applyFormatter = UI_Anci.isApplyDecorator(frmtAmount,isFreeService);
					bgClass = (t%2 == 1) ? "odd":"even";
					ret += 	'<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr id="trService_'+j.ssrCode+'" class="halaPopupRow">';
						if ( UI_Anci.hasAPSThumbnails ){
							ret += 	'<td width="50" class="bdBottom" height="50"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden;"> <img src="'+solveImageUrl(j.ssrThumbnailImagePath)+'" height="50" /></div>'+
							'<span class="description" style="display:none"><label>'+j.ssrDescription+'</label></span>'+
							'<span class="imgpath" style="display:none">'+solveImageUrl(j.ssrImagePath)+'</span></td>';
						}
						if ( checkChoosedHalas(choosedHala,j.ssrCode) ){
							ret += 	'<td width="5%" class="bdBottom"><input type="checkbox" id="chkService_'+j.ssrCode+'" name="chkService" checked="checked"/></td>';
						}else{
							ret += 	'<td width="5%" class="bdBottom"><input type="checkbox" id="chkService_'+j.ssrCode+'" name="chkService"/></td>';
						}
						
						if(applyFormatter){
							frmtAmount = UI_Anci.freeServiceDecorator;
						}
						
						ret +=	'<td width="60%" class="bdBottom"><label class="thumbImg">'+j.ssrName+'</label></td>'+
								'<td class="bdBottom" width="10%"><label class="thumbImg">'+UI_Anci.currency.selected+'</label></td>'+
							'<td class="bdBottom"><label id="lblCharge_'+j.ssrCode+'" class="thumbImg">'+frmtAmount+'</label></td>'+
							'</tr></tabel>';
					t++;
				}
			});
		}else{
			var countPaxServices = 0;
			$.each(UI_Anci.jsHalaModel[intSegRow].airportServices, function(i, j){
				if (UI_Anci.ChargeType.PER_RESERVATION != j.applicabilityType){
					bgClass = (t%2 == 1) ? "odd":"even";
					ret += 	'<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr id="trService_'+j.ssrCode+'" class="halaPopupRow" >';
						if ( UI_Anci.hasAPSThumbnails ){
							ret += 	'<td width="50" class="bdBottom" height="50"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"> <img src="'+solveImageUrl(j.ssrThumbnailImagePath)+'" height="50"/></div>'+
							'<span class="description" style="display:none"><label>'+j.ssrDescription+'</label></span>'+
							'<span class="imgpath" style="display:none">'+solveImageUrl(j.ssrImagePath)+'</span></td>';
						}
						if ( checkChoosedHalas(choosedHala,j.ssrCode) ){
							ret += 	'<td width="5%" class="bdBottom"><input type="checkbox" id="chkService_'+j.ssrCode+'" name="chkService" checked="checked"/></td>';
						}else{
							ret += 	'<td width="5%" class="bdBottom"><input type="checkbox" id="chkService_'+j.ssrCode+'" name="chkService"/></td>';
						}
						ret +='<td width="70%" class="bdBottom"><label class="thumbImg">'+j.ssrName+'</label></td>'+
							'<td class="bdBottom" width="10%"><label class="thumbImg">'+UI_Anci.currency.selected+'</label></td>';
						
						var frmtAmount = '';
						if (paxType == "AD"){
							frmtAmount = UI_Anci.convertAmountToSelectedCurrency(j.adultAmount);
						}else if (paxType == "CH"){
							frmtAmount = UI_Anci.convertAmountToSelectedCurrency(j.childAmount);							
						}else if(paxType == "PA"){
							frmtAmount = UI_Anci.convertAmountToSelectedCurrency(parseFloat(j.adultAmount) + parseFloat(j.infantAmount));					
						}
						if(j.isFree){							
							var applyFormatter = UI_Anci.isApplyDecorator(frmtAmount,isFreeService);
							if(applyFormatter){
								frmtAmount = UI_Anci.freeServiceDecorator;
							}
						}
						ret +='<td class="bdBottom"><label class="thumbImg" id="lblCharge_'+j.ssrCode+'">'+frmtAmount+'</label></td></tr></table>';
					t++;
					countPaxServices ++;
				}
			});
			if(countPaxServices == "0"){
				ret += 	'<tr><td colspan="4"><label>'+UI_Container.labels.lblNoservices+'</label></td></tr>';
			}
		}
			ret += '</td></tr></table>';
		return ret;
	};
	
	UI_Anci.halaServiceSelect = function (e){
		var tArr = this.id.split("_");
		var airportCode = (tArr[5]!=undefined) ? tArr[5]:'';
		
		UI_Anci.hala.currentSegId = tArr[1];
		
		seletedHalaArray = [];
		$("#hService_"+tArr[1]+"_"+tArr[2]+"_"+tArr[5]).find(".selHalaService").each(function(){
			if (this.id.split("_")[3] != undefined && this.id.split("_")[4] != undefined 
					&& this.id.split("_")[4] != ''){
				var temp = getHalaByCode(this.id.split("_")[4], UI_Anci.getHalaSegIndex(tArr[1], airportCode));
				if (temp != null){
					seletedHalaArray.push(temp);
				}
			}
		});	
		
		//set Hidden data
		var intSegRow = UI_Anci.getHalaSegIndex(tArr[1], airportCode);
		
		$.data($("#newPopItem")[0], "haladata" ,
				{	segId:tArr[1],
					paxId:tArr[2],
					paxType:tArr[3] ,
					halaServices:seletedHalaArray,
					airportCode: airportCode
				});
		
		$("#newPopItem").openMyPopUp({
			width:460,
			height:250,
			topPoint: $(window).height() - (350),
			headerHTML:UI_Container.labels.lblHalaServiceHD,
			bodyHTML: function(){
				return UI_Anci.buildPopupHala($.data($("#newPopItem")[0], "haladata").paxId, 
											$.data($("#newPopItem")[0], "haladata").paxType,
											$.data($("#newPopItem")[0], "haladata").halaServices,
											$.data($("#newPopItem")[0], "haladata").airportCode);
			},
			footerHTML: function(){
				var passHTML = "<input type='button' value='"+UI_Anci.jsonLabel["btnMealConfirm"]+"' class='Button halaConfirm'/>";
				return passHTML;
			}
		});
		$(".close").unbind("click").bind("click",UI_Anci.closeHalaPopUp);
		$(".halaConfirm").unbind("click").bind("click",UI_Anci.closeHalaPopUp);
		$(".halaPopupRow").unbind("mouseenter").bind("mouseenter",function(){
			$(this).find("td").addClass("mOverMeal");
		});
		$(".halaPopupRow").unbind("mouseleave").bind("mouseleave",function(){
			$(this).find("td").removeClass("mOverMeal");
		});
		$(".halaPopupRow").find("input[type='checkbox']").unbind("click").bind("click",UI_Anci.halaSerPopupSelect);
	};
	
	UI_Anci.halaSerPopupSelect = function(){
		if ( $(this).find("input[type='checkbox']").attr("checked") ){
			$(this).find("input[type='checkbox']").attr("checked", false);
		}else{
			$(this).find("input[type='checkbox']").attr("checked", true);
		}
	};
	
	UI_Anci.paxHalaRemoveWrapper = function(){
		var sliderArray = this.id.split("_");
		var segId = sliderArray[1];
		var paxId = sliderArray[2];
		var paxType = sliderArray[3];
		var halaCode = sliderArray[4];
		var airportCode = sliderArray[5];
		UI_Anci.paxHalaRemove(segId,paxId,paxType,halaCode,airportCode);
	};
	
	UI_Anci.paxHalaRemove = function(segId,paxId,paxType,halaCode, airportCode){
		
		UI_Anci.removeHalaFromPaxObj(paxId, segId, halaCode, airportCode);
		
		var trParent = $("#trHala_"+segId+"_"+paxId+"_"+paxType+"_"+halaCode+"_"+airportCode).parent();
		$("#trHala_"+segId+"_"+paxId+"_"+paxType+"_"+halaCode+"_"+airportCode).remove();
		if (trParent.children().length <= 0){
			$("#hService_"+segId+"_"+paxId+"_"+airportCode).html(UI_Anci.buildHalaServices(segId, paxId, paxType, null, false, false, airportCode));
			UI_Anci.buildHalaPax();
		}

	};
	
	/*
	 * Remove all airport services from given pax
	 */
	UI_Anci.removeHalaFromPaxObj = function(paxId, segId, ssrCode, airPortCode){
		var processed = false;
		if(paxId != "-"){
			processed = UI_Anci.removePaxHalaData(paxId, ssrCode, segId, airPortCode);
		} else {
			//remove from all pax
			for(var k=0; k<UI_Anci.jsAnciSegModel[segId].pax.length; k++){
				processed = UI_Anci.removePaxHalaData(k, ssrCode, segId, airPortCode);
			}
		}
		
		if(processed){
			UI_Anci.updateAnciCounts(UI_Anci.anciType.HALA,1,'DEDUCT');
		}
	}
	
	UI_Anci.removePaxHalaData = function(paxId, ssrCode, segId, airPortCode){
		var processed = false;
		var halaData = UI_Anci.jsAnciSegModel[segId].pax[paxId].aps;
		if (ssrCode == null){
			var removableArr = new Array();
			for (var i = 0; i< halaData.apss.length; i++){
				if ( halaData.apss[i].applicableType == UI_Anci.currAirportServiceApplicability &&
					airPortCode == halaData.apss[i].airport ){
					var tempSbTot = halaData.apss[i].subTotal;
					removableArr.push(halaData.apss[i].ssrCode);
					UI_Anci.updateAnciTotals(UI_Anci.anciType.HALA,tempSbTot,'DEDUCT');
					halaData.apsChargeTotal -= tempSbTot;
					processed = true;
				}
			}
			
			//Remove element from hala obj
			for(var i=0;i<removableArr.length; i++){				
				for(var j=0;j<halaData.apss.length; j++){
					if(halaData.apss[j].ssrCode == removableArr[i] &&
						airPortCode == halaData.apss[j].airport	){
						halaData.apss.splice(j,1);
						break;
					}
				}
			}
			
		}else{
			for (var i = 0; i< halaData.apss.length; i++){
				if ( halaData.apss[i].ssrCode == ssrCode && airPortCode == halaData.apss[i].airport){
					var tempSbTot = halaData.apss[i].subTotal;
					halaData.apss.splice(i,1);
					UI_Anci.updateAnciTotals(UI_Anci.anciType.HALA,tempSbTot,'DEDUCT');
					halaData.apsChargeTotal -= tempSbTot;
					processed = true;
				}
			}
		}
		
		UI_Anci.updateFlightAnciView();	
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
		
		return processed;
	}

	/**
	 * 
	 * #################################### End of Airport Service Related Methods ########################################
	 * 
	 */
	
	
	//------------------------------------------------AIRPORT TRANSFER-----------------------------------------------------
	UI_Anci.loadAirportTransferData = function(sync){
		$("#tblApTransfer").show();
		$("#tbdyApTransferSeg").hide();
		UI_Anci.showLoadingMsg(UI_Anci.anciType.APTRANSFER);
		
		var data = {};
		
		if(UI_Container.isModifyAncillary() || UI_Container.isModifySegment()){
			data['resPaxInfo'] = $.toJSON(UI_Anci.jsonPaxAdults);
		}
		if(UI_Container.isModifyAncillary()){
			data['modifyAncillary'] = true;
		}
		
		$("#frmReservation").ajaxSubmit({dataType: 'json',url:'interlineAnciAirportTransfer.action', processData: false, async:!sync, data:data,		
				success: UI_Anci.loadApTransferDataSuccess, error:UI_commonSystem.setErrorStatus});
	}
	
	UI_Anci.loadApTransferDataSuccess = function(response){
		if(response!=null){
			UI_Anci.jsApTransferModel = response.airportTransferList;
			if(UI_Anci.jsApTransferModel != null){
				UI_Anci.jsApTransferModel = $.airutil.sort.quickSort(UI_Anci.jsApTransferModel,segmentComparator);
				UI_Anci.jsApTransferFlightSegmentModel = response.flightSegmentTOs;
				$("#tbdyApTransferSeg").show();
				UI_Anci.allowModifyApts = response.allowModify;
				UI_Anci.buildAirportTransferTab({id:"tbdyApTransferSeg", tdID:"tdApTransferSeg_", click:null, segs: UI_Anci.jsApTransferFlightSegmentModel});
			}
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.APTRANSFER);			
		}else{			
			UI_Anci.hideLoadingMsg(UI_Anci.anciType.APTRANSFER);
			jConfirm('Error occured while loading airport Transfers', 'Alert', function(){});
		}
	}
	
	UI_Anci.buildAirportTransferTab = function(p){
		UI_Anci.jsAnciAvail.apts = [];
		$.each( UI_Anci.jsApTransferFlightSegmentModel, function(intIndex, obj){
			var segObj = {segCode: obj.segmentCode,segInd: UI_Anci.getAnciSegIndexFromAPTIndex(intIndex, obj.airportCode), airportCode:obj.airportCode, returnFlag:obj.returnFlag };
			UI_Anci.jsAnciAvail.apts[intIndex] = segObj;
		});
		var availableSegs = UI_Anci.jsAnciAvail.apts;
		var adultChild = UI_Anci.jsonPaxAdults;	
		
		var addHTML = "";
		var obExist = false;
		var ibExist = false;
		var addInboundHTML = "";
		var addOutboundHTML = "";
		var j = 0;
		var i = 0;

		addOutboundHTML += '<div class="segs-full-baggage NoData">&nbsp;</div>';
		addInboundHTML += '<div class="segs-full-baggage NoData">&nbsp;</div>';
		while (j < availableSegs.length ){
			if (availableSegs[j].returnFlag == false){
				addOutboundHTML += '<div id="tdApTransferSeg_'+ availableSegs[j].segInd +'_'+availableSegs[j].airportCode+'" class="segs-full-baggage">'+
				UI_Anci.buildAirportTransferInner(availableSegs[j],adultChild,availableSegs[j].segInd)+'</div>';
				obExist = true;
			}else{
				addInboundHTML += '<div id="tdApTransferSeg_'+ availableSegs[j].segInd +'_'+availableSegs[j].airportCode+'" class="segs-full-baggage">'+
				UI_Anci.buildAirportTransferInner(availableSegs[j],adultChild,availableSegs[j].segInd)+'</div>';
				ibExist = true;
			}
			j++
		};

		addHTML += '<div class="outBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblOutbound ,UI_Anci.jsAnciSeg.length/2,obExist) +addOutboundHTML+'</div>'+
					'<div class="inBoundFlight">'+ checkSegsConnection(UI_Container.labels.lblInbound ,UI_Anci.jsAnciSeg.length/2,ibExist) + addInboundHTML+'</div><div class="clear"></div>';
		$("#"+p.id).html(addHTML);
		UI_Anci.mapDatePickers();
	}
	
	//airport wise
	UI_Anci.buildAirportTransferInner = function (segData,innerData,segIndex){

		var apTransferList = UI_Anci.getFltSegAvailTransferList(segData.segCode, segData.airportCode);
		var addHTML = "";
			addHTML += '<table cellspacing="0" cellpadding="1" border="0" align="center" width="99%" class="seat-details">'+
			'<tbody>'+
			'<tr><td colspan="5" class="segcodebar"><label>' + UI_Anci.getAirportNameFromSegment(segData.segCode, UI_Container.getSegmentName(segData.segCode), segData.airportCode)+'</label></td></tr>'+
			'<tr>'+ 
				'<td align="center" width="20%" class="gridHD bdBottom "><label id="lblSeatPaxNameHD" class="gridHDFont fntBold">'+UI_Container.labels.lblSeatPaxNameHD+'</label></td>'+
				'<td align="center" width="55%" class="gridHD bdBottom"><label class="gridHDFont fntBold">'+ 'Airport Transfer'+'</label></td>'+ 
				'<td align="center" width="15%" class="gridHD bdBottom"><label id="lblBaggagePriceHD" class="gridHDFont fntBold">'+UI_Container.labels.lblBaggagePriceHD+' &nbsp;('+UI_Anci.currency.selected+')</label></td>'+
			'</tr>';
			
			$.each(innerData, function(j,w){
					var bgClass = (j%2 == 1) ? "odd":"even";
					var infInd = '';
					if(w.paxType == 'PA'){
						infInd = UI_Anci.infantTypeSuffix;
					}
					var currAnciSegIndex = -1;
					if(UI_Container.isModifySegment() && w.currentAncillaries != null &&  w.currentAncillaries.length > 0){
						var modifyingSegIndex = 0;
						for(var p=0; p<w.currentAncillaries.length;p++){
							if(UI_Container.isOneOfModifyingSeg(w.currentAncillaries[p].flightSegmentTO.flightRefNumber)){
								if(modifyingSegIndex == segIndex){
									currAnciSegIndex = p;
									break;
								}
								modifyingSegIndex ++;
							}
						}
					}
					
					if (UI_Container.isModifyAncillary() && w.currentAncillaries != null && w.currentAncillaries.length > 0 
							&& w.currentAncillaries[segIndex].airportTransferDTOs != null && w.currentAncillaries[segIndex].airportTransferDTOs.length > 0){
                     	//pax has booked ancilaries, but will filter based on executing airport
						var paxReservedApts = UI_Anci.paxReservedAirportTransfers(apTransferList.airportTransferList, w.currentAncillaries[segIndex].airportTransferDTOs, segData.airportCode);
					
						if(paxReservedApts == null || paxReservedApts.length == 0){
							addHTML +='<tr id="aptPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">' + 
							  '<td class="defaultRowGap rowColor bdLeft bdBottom"> <label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
							   '<td align="center" class="rowColor  bdBottom bdRight">' + UI_Anci.buildAirportBasedPaxTransfers(segIndex, j, w.paxType, apTransferList, false, false) + '</td>' +
							   '<td align="center" class="rowColor  bdBottom bdRight"> <label id="aptPaxPrice_' + segIndex + '_' + apTransferList.airport + "_"+ j + '">-</label> </td>' +
							   '</tr>';
						} else {							
						  addHTML +='<tr id="aptPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">' + 
							  '<td class="defaultRowGap rowColor bdLeft bdBottom"> <label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
							   '<td align="center" class="rowColor  bdBottom bdRight">' + UI_Anci.buildReservedPaxTransfers(segIndex, j, w.paxType, paxReservedApts) + '</td>' +
							   '<td align="center" class="rowColor  bdBottom bdRight"> <label id="aptPaxPrice_' + segIndex + '_' + apTransferList.airport + "_"+ j + '">-</label> </td>' +
							   '</tr>';
						}
				
					}else if (UI_Container.isModifySegment() && w.currentAncillaries != null && w.currentAncillaries.length > 0 && currAnciSegIndex !=-1
							&& w.currentAncillaries[currAnciSegIndex].airportTransferDTOs != null && w.currentAncillaries[currAnciSegIndex].airportTransferDTOs.length > 0){
						
						if (UI_Anci.getAiportTransferSegIndex(segIndex, segData.airportCode) != null){
							UI_Anci.perPaxApTransfers = [];
							$.each( UI_Anci.jsApTransferModel[UI_Anci.getAiportTransferSegIndex(segIndex, segData.airportCode)].airportServices, function (i, obj){
								if (obj.applicabilityType == UI_Anci.ChargeType.PER_PAX){
									UI_Anci.perPaxApTransfers[UI_Anci.perPaxApTransfers.length] = obj;
								}
							});
						}
						
						var currentAPTs = w.currentAncillaries[currAnciSegIndex].airportTransferDTOs;
						//TODO: FIXME
					    var currentAPT = [];//get the currect ap transfers per airport
						for (var d = 0; d< currentAPTs.length;d++){
							if ( currentAPTs[d].airportCode == UI_Anci.getModifingAPcode(segData.airportCode) && UI_Anci.isAPavailableByCode(currentAPTs[d].ssrCode) ){
								adServiceperAP.push(UI_Anci.getAPavailableByCode(currentAPTs[d].ssrCode));
							}
						}
					
						
						if (currentAPT.length == 0) {
							adServiceperAP = null;
						}
						if (currentAPT != null){
							$.each(currentAPT, function(k,t){
								UI_Anci.paxHalaAdd(j,t.ssrCode, segIndex, segData.airportCode);
							});
						}

						addHTML +='<tr id="aptPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">'+
						'<td class="defaultRowGap rowColor bdLeft bdBottom">'+
						'<label name="paxName">'+w.firstName+' '+w.lastName+infInd+'</label></td>'+
						'<td align="center" class="rowColor  bdBottom bdRight" id="hTransfer_'+segIndex+'_'+j+'_'+segData.airportCode+'" colspan="2" valign="top">'+UI_Anci.buildAirportTransfers(segIndex, j, w.paxType,  currentAPT, true, true, segData.airportCode)+'</td>'+
						'</tr>';

					}else{
						addHTML +='<tr id="aptPaxTmplt_'+segIndex+'_'+j+'_'+segData.airportCode+'" class="'+bgClass+'">'+
						'<td class="defaultRowGap rowColor bdLeft bdBottom bdRight"> <label name="paxName">' + w.firstName+' '+w.lastName+infInd + '</label></td>'+
						 '<td align="center" class="rowColor  bdBottom bdRight">' + UI_Anci.buildAirportBasedPaxTransfers(segIndex, j, w.paxType, apTransferList, false, false) + '</td>' +
						 '<td align="center" class="rowColor  bdBottom bdRight"> <label id="aptPaxPrice_' + segIndex + '_' + apTransferList.airport + "_"+ j + '">-</label> </td>' +
						'</tr>';
					}
				});
			addHTML += '</table>';
		return addHTML;
	};
	
	UI_Anci.paxReservedAirportTransfers = function(transferList, paxTransferList, airportCode){
		var paxReservedApts = []
		$.each(transferList, function(m, transfer){
			if (transfer.applicabilityType == UI_Anci.ChargeType.PER_PAX) {
				$.each(paxTransferList, function(j, paxTransfer){
					if (paxTransfer.ssrCode != null && paxTransfer.airportCode == airportCode){
						paxTransfer.applicabilityType = UI_Anci.ChargeType.PER_PAX;
						paxTransfer.airportCode = airportCode; 
						paxTransfer.provider = transfer.provider;
						paxReservedApts.push(paxTransfer);
					}
				});
			}
		});
		
		return paxReservedApts;
	}
	
	//html generators
	UI_Anci.buildReservedPaxTransfers = function (sIndex, pIndex, paxType, paxReservedApts){
		var paxReservedHtml = "";
		for(var i=0; i< paxReservedApts.length; i++){
			paxReservedHtml += buildPerPaxSingleAiportTransfer(sIndex, paxReservedApts[i].airportCode, pIndex, paxType, paxReservedApts[i], true);
		}
		return paxReservedHtml;
	}
	
	UI_Anci.buildAirportBasedPaxTransfers = function (sIndex, pIndex, paxType, apTransferList, removeFlag, modifyFlag){
		var allTransferHtml = "";
		var airportCode = apTransferList.airport;
		var transferListPerAirport =  apTransferList.airportTransferList;
		for(var i= 0; i<transferListPerAirport.length; i++ ){
			allTransferHtml += buildPerPaxSingleAiportTransfer(sIndex, airportCode, pIndex, paxType, transferListPerAirport[i], modifyFlag);
		}
	  return allTransferHtml; 
   }

	UI_Anci.mapDatePickers = function (){
		$('[id*="_date"]').each( function () {
			var ref = this.id.split("_");
		    var segIndex  =	UI_Anci.getAiportTransferSegIndex(ref[1], ref[2]);
		   
		    $("#" + this.id).datepicker({
				dateFormat : UI_commonSystem.strDTFormat,
				changeMonth : 'true',
				changeYear : 'true',
				yearRange: '-1:+1',
				minDate : UI_Anci.allowedFlightDates(segIndex).previousDate,
				maxDate : UI_Anci.allowedFlightDates(segIndex).nextDate,
				defaultDate: UI_Anci.allowedFlightDates(segIndex).flightDate,
				showButtonPanel : 'true'
		    });
			
		});
	}
	
	UI_Anci.allowedFlightDates = function (segIndex){
		var dateArr = {minDate:'', maxDate: '', defaultDate: ''};
		 if(segIndex != null ){
			var cuttOffTime = UI_Anci.getAirportTransferCutoff(segIndex);
			//"2013-12-27T15:55:00"
			var dateTime = cuttOffTime.split("T");
			var dateStr = dateTime[0];
			var dateArr = dateStr.split("-");
			var formatedStrDate = dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2];
			var flightDate = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
				
			var d1 = new Date(formatedStrDate.replace(/(\d{4})(\d{2})(\d{2})/, '$1/$2/$3'));
			var d2 = new Date(formatedStrDate.replace(/(\d{4})(\d{2})(\d{2})/, '$1/$2/$3'));
			dateArr.previousDate = new Date(d1.setDate(d1.getDate() - 1)); //one-day before
			dateArr.nextDate = new Date(d2.setDate(d2.getDate() + 1)); //one-daye after
			dateArr.flightDate = flightDate;	
		 }
		return dateArr;
	}
	
	UI_Anci.getAirportTransferCutoff = function (segIndex){
		 var cuttOffTime = "";
		 var fltSeg = UI_Anci.jsApTransferModel[segIndex].flightSegmentTO;
		 if(fltSeg.airportType == UI_Anci.AirportType.ARRIVAL){
			 cuttOffTime = fltSeg.arrivalDateTime;
		 } else if(fltSeg.airportType == UI_Anci.AirportType.DEPARTURE || fltSeg.airportType == UI_Anci.AirportType.TRANSIT){
			 cuttOffTime = fltSeg.departureDateTime;
		 }
		 return cuttOffTime;
	}
	
	UI_Anci.buildAirportTransfers = function(sIndex, pIndex, paxType, sList, removeFlag, modifyFlag, airportCode){
		console.log("Aiport level transfers will be builded here");
		//TODO: FIXME
		return "<p> transfers </p>";
	}
	//utils
	UI_Anci.getFltSegAvailTransferList = function(segCode, airportCode){
		var trasferObj = {}
		for(var i=0; i<UI_Anci.jsApTransferModel.length;i++){
			var fltSegTO = UI_Anci.jsApTransferModel[i].flightSegmentTO;
			if(fltSegTO.segmentCode == segCode && fltSegTO.airportCode == airportCode){
				trasferObj.airportTransferList = UI_Anci.jsApTransferModel[i].airportServices;
				trasferObj.airport = fltSegTO.airportCode;
				break;
			}
		}
		
		return trasferObj;
	}
	
	UI_Anci.getAnciSegIndexFromAPTIndex = function(index, airportCode){
		var fltRefNo = UI_Anci.jsApTransferFlightSegmentModel[index].flightRefNumber;
		var anciInd = 0;
		for(var i=0;i<UI_Anci.jsAnciSeg.length;i++){
			if(compareFltRefNos(fltRefNo , UI_Anci.jsAnciSeg[i].flightSegmentTO.flightRefNumber)){
				return anciInd = i;
				break;
			}
		}
		return anciInd;
	}
	
	UI_Anci.getAiportTransferSegIndex = function(segId, airportCode){
		var intSegRow = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
		for(var i=0; i < UI_Anci.jsApTransferModel.length; i++){
			if(compareFltRefNos(fltRefNo,UI_Anci.jsApTransferModel[i].flightSegmentTO.flightRefNumber) &&
					UI_Anci.jsApTransferModel[i].flightSegmentTO.airportCode == airportCode){
				intSegRow = i; 
				break;
			}
		}
		return intSegRow;
	}

	//--------------------------------------------- SEAT -----------------------------------------------------------------
	
	/*
	 * Seat Segment Click
	 */
	UI_Anci.tabClickSeat = function(){
		// 0 - tdSeatSeg;
		// 1 - Seg Row;
		
		var arrData = this.id.split("_");
		var intSegRow = arrData[1];
		UI_Anci.clickSeatSegment({ind:intSegRow});
	}
	
	/*
	 * Baggage Segment Click
	 */
	UI_Anci.tabClickBaggage = function(){
		// 0 - tdSeatSeg;
		// 1 - Seg Row;
		
		var arrData = this.id.split("_");
		var intSegRow = arrData[1];
		UI_Anci.clickBaggageSegment({ind:intSegRow});
	}
	
	/*
	 * Click Seat segment
	 */
	UI_Anci.clickSeatSegment = function(p){
		UI_commonSystem.pageOnChange();
		
		UI_Anci.seat.currentSegId = p.ind;
		if (UI_Anci.tabSltdSeat != null){
			if (UI_Anci.seatViewType != "New-view"){
				$("#tdSeatSeg_" + UI_Anci.tabSltdSeat).removeClass("tabSltd");
				$("#tdSeatSeg_" + UI_Anci.tabSltdSeat).addClass("tabNotSltd");
			}
			
			$("#lbl_tdSeatSeg_" + UI_Anci.tabSltdSeat).removeClass("gridHDFont fntBold");
			$("#lbl_tdSeatSeg_" + UI_Anci.tabSltdSeat).addClass("fntDefault");
		}
		
		UI_Anci.tabSltdSeat = p.ind;
		if (UI_Anci.seatViewType != "New-view"){
			$("#tdSeatSeg_" + UI_Anci.tabSltdSeat).removeClass("tabNotSltd");
			$("#tdSeatSeg_" + UI_Anci.tabSltdSeat).addClass("tabSltd");
		}
		
		$("#lbl_tdSeatSeg_" + UI_Anci.tabSltdSeat).removeClass("fntDefault");
		$("#lbl_tdSeatSeg_" + UI_Anci.tabSltdSeat).addClass("gridHDFont fntBold");
		UI_Anci.buildSeatMap({segId:p.ind});
		UI_Anci.populateSMData({segId:p.ind});
	}
	/* 
	 * Get index of the seat model segment
	 */
	UI_Anci.getSeatSegIndex = function(segId){
		var intSegRow = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
		for(var i=0; i < UI_Anci.jsSMModel.length; i++){
			if(compareFltRefNos(fltRefNo,UI_Anci.jsSMModel[i].flightSegmentDTO.flightRefNumber)){
				intSegRow = i; break;
			}
		}
		return intSegRow;
	}
	
	/*
	 * Click Baggage segment
	 */
	UI_Anci.clickBaggageSegment = function(p){
		UI_commonSystem.pageOnChange();
		
		UI_Anci.baggage.currentSegId = p.ind;
		
		//UI_Anci.populateBaggageData({segId:p.ind});
	}
	/* 
	 * Get index of the seat model segment
	 */
	UI_Anci.getSeatSegIndex = function(segId){
		var intSegRow = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
		intSegRow = UI_Anci.getSeatSegIndexByFltRefNo(fltRefNo);
		return intSegRow;
	}

    UI_Anci.getSeatSegIndexByFltRefNo = function(fltRefNo){
		var intSegRow = null;
        if (UI_Anci.jsSMModel != null ) {
            for(var i=0; i < UI_Anci.jsSMModel.length; i++){
                if(compareFltRefNos(fltRefNo,UI_Anci.jsSMModel[i].flightSegmentDTO.flightRefNumber)){
                    intSegRow = i; break;
                }
            }
        }
		return intSegRow;
	}
	
	

	UI_Anci.setDefaultSeatSelection = function(){
		if(UI_Anci.selectDefaultSeatsForPax){
			var currFltRefNo = UI_Anci.getFltRefNoForIndex(UI_Anci.seat.currentSegId);
			$.each(UI_Anci.jsSMModel, function(index, smObj){
				var cabinCls = smObj.lccSeatMapDetailDTO.cabinClass;
				var fltSegment = smObj.flightSegmentDTO;
				var setSeatSelection = false;
				if(currFltRefNo == fltSegment.flightRefNumber){
					setSeatSelection = true;
				}
				
				for (var ccI = 0; ccI < cabinCls.length; ccI++) {
					var cc = cabinCls[ccI];
					if(fltSegment.cabinClassCode == cc.cabinType){
						var rgDtos = cc.airRowGroupDTOs;
						rgDtos = $.airutil.sort.quickSort(rgDtos,UI_Anci.rowGroupComparator);						
						for (var rgI = 0; rgI < rgDtos.length; rgI++) {
							var cgDtos = rgDtos[rgI].airColumnGroups;
							cgDtos = $.airutil.sort.quickSort(cgDtos,UI_Anci.columnGroupComparator);
							for (var cgI = 0; cgI < cgDtos.length; cgI++) {
								var rows  = cgDtos[cgI].airRows;
								rows = $.airutil.sort.quickSort(rows,UI_Anci.rowComparator);
								UI_Anci.setSeatForPax(rows, fltSegment, index, setSeatSelection);
							}
						}
					}
				}
			});
			UI_Anci.selectDefaultSeatsForPax = false;
		}
	}
	
	UI_Anci.setSeatForPax = function(rows, fltSegment, segIndex, setSeatSelection){		
		var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.SEAT_MAP, null, UI_Anci.getOndSequence(segIndex));
		
		OUTER:
		for(var paxIndex=0; paxIndex<UI_Anci.jsonPaxAdults.length; paxIndex++){
			if(UI_Anci.jsAnciSegModel[segIndex].pax[paxIndex].seat.seatNumber == undefined || 
					UI_Anci.jsAnciSegModel[segIndex].pax[paxIndex].seat.seatNumber == ""){				
				var paxObj = UI_Anci.jsonPaxAdults[paxIndex];
				for(var rowId=0; rowId<rows.length;rowId++){
					var seats = rows[rowId].airSeats;
					var rowNumber = rows[rowId].rowNumber;
					for(var seatIndex =0; seatIndex<seats.length;seatIndex++){
						var seat = seats[seatIndex];
						var strSeatNo =  seat.seatNumber;
						if(seat.logicalCabinClassCode == fltSegment.logicalCabinClassCode &&
								seat.seatAvailability == "VAC"){								
							var seatRefObj = {fltRefNo:fltSegment.flightRefNumber, seatNumber:strSeatNo, rowNumber:rowNumber};
							if (!UI_Anci.isSeatOccupied(seatRefObj) && UI_Anci.isSatisfySeatAssignementConstrains(seatRefObj)) {
								var paxType = paxObj.paxType;	
								
								if(UI_Anci.paxHasInfant(paxIndex)){
									//paxType = "PA";
									if(!UI_Anci.canSeatInfant(seatRefObj, paxIndex, segIndex) ||
											seat.seatType == "EXIT"){
										continue;
									}
								}
								
								var mObj= {paxId : paxIndex, paxType: paxType, refObj: seatRefObj, segId: segIndex};
								UI_Anci.markSeat(mObj);								
								
								var strSeatCharge = UI_Anci.convertAmountToSelectedCurrency(seat.seatCharge);
								var applyFormatter = UI_Anci.isApplyDecorator(strSeatCharge,isFreeService);                		
		                		if(applyFormatter){
		                			strSeatCharge = UI_Anci.freeServiceDecorator;	
		                		}	
								
								if (UI_Anci.seatViewType == "New-view"){
									var parentId = "#tdSeatSeg_"+segIndex+"";
									if(setSeatSelection){										
										$("#tdSeat_" + strSeatNo ).removeClass("availSeat");
										$("#tdSeat_" + strSeatNo ).addClass("SltdSeat");
									}
									$(parentId).find("#seatPax_SeatNo_" + paxIndex).html(strSeatNo);
									$(parentId).find("#seatPax_Charge_" + paxIndex).html(strSeatCharge);
									$(parentId).find('#seatPax_Clear_' + paxIndex).find('a').show();
									
								}else{
									var parentId = "";
									if(setSeatSelection){										
										$("#tdSeat_" + strSeatNo ).removeClass("availSeat");
										$("#tdSeat_" + strSeatNo ).addClass("SltdSeat");
									}
									$("#seatPax_SeatNo_" + paxIndex).html(strSeatNo);
									$("#seatPax_Charge_" + paxIndex).html(strSeatCharge);
									$('#seatPax_Clear_' + paxIndex).find('a').show();
								}								
							
								continue OUTER;
							}
						}
					}
				}
			}
			
		}
	}
	
	/*
	 * Build Seat Map
	 */
	UI_Anci.buildSeatMap = function(p){
		$("#tblSeatMap").hide();
		$("#spnAnciNoSeat").hide();
		var blnCCsFound = false;
		var cabinCls = null;
		var intLen = null;
		var intSegRow = null;
		var i = 0;
		
		var segIndex = parseInt(p.segId,10);
		var intSegRow = UI_Anci.getSeatSegIndex(segIndex);
		var populateRefMap = false;
		var seatRefMap = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segIndex);
		
		var selectedClassOfService = UI_Anci.getCabinClassInfoForIndex(segIndex);
		
		var availLCCArray = [];
		if (intSegRow != null){			
			cabinCls = UI_Anci.jsSMModel[intSegRow].lccSeatMapDetailDTO.cabinClass; 
			if(cabinCls!=null){
				seatRefMap = UI_Anci.seatRefMap[fltRefNo];
				if(seatRefMap == null){
					populateRefMap = true;
					seatRefMap={seats:{}};
				}
				blnCCsFound = true;
			}	
		}
		
		
		if (!blnCCsFound){
			$("#spnAnciNoSeat").show();
		}else{
			if (cabinCls != null){
				var ccLen = cabinCls.length;				
				
				if(ccLen>0){
					/* Initialize the Details */
					$("#tbdySeatMap").find("tr").remove();
					$("#tbdyWingRight").find("tr").remove();
					$("#tbdyWingLeft").find("tr").remove();				
			
					$("#tblSeatMap").show();
					
					var tblRow = null;
					var tblWingRowLeft = null;
					var tblWingRowRight = null;
					var isleRow = null;
					var blankIsle = {desc:"&nbsp;", 
							css:"aisle",
							textCss:"font-size:9px;",
							align:"right"};
					
					var columnLetters = new Array('A','B','C','D','E','F','G','H','I','J','K');
					var colLtrStr = "ABCDEFGHIJK";
					
					$('#tbdySeatMap').append("<tr id='trCabinClass'></tr>");
					$('#tbdyWingRight').append("<tr id='trRWCC'></tr>");
					$('#tbdyWingLeft').append("<tr id='trLWCC'></tr>");
					var buildLetters = true;
					for(var ccI=0; ccI < ccLen ; ccI++){
						buildLetters = true;
						var cc  = cabinCls[ccI];
						
						var activeCC = false;
						
						if(selectedClassOfService['cc'] != null){
							if(selectedClassOfService['cc'] == cc.cabinType){
								activeCC = true;
							}
						} else {
							activeCC = true;
						}
						
						
						$('#trCabinClass').append('<td><table cellspacing="0" cellpadding="0"><tbody class="cabinClass '+
								cc.cabinType+'" style="margin-right:5px;"><tr id="trCC_'+ccI+'"></tr></tbody></table></td>');		
						$('#trRWCC').append('<td><table ><tbody  style="margin-right:5px;"><tr id="trCCRW_'+ccI+'"></tr></tbody></table></td>');		
						$('#trLWCC').append('<td><table ><tbody  style="margin-right:5px;"><tr id="trCCLW_'+ccI+'"></tr></tbody></table></td>');	
						tblWingRowRight = document.createElement("TR");
						tblWingRowLeft = document.createElement("TR");
						
						var rgDtos = cc.airRowGroupDTOs;
						var winExitRightArr = new Array(); 
						var winExitLeftArr = new Array();
						for(var rgI=0; rgI< rgDtos.length;rgI++){
							$('#trCC_'+ccI).append('<td><table cellspacing="0" cellpadding="0"><tbody class="tbSeatMap" id="tbodyRG_'+ccI+'_'+rgI+'"></tbody></table></td>');
							var cgDtos = rgDtos[rgI].airColumnGroups; 
							var trArr = new Array(); /*
														 * array to contain td
														 * elements
														 */
							
							/*
							 * additional windows required for letters rows -
							 * column grps
							 */
							var winCell = {desc:"&nbsp;", 
									css:'window',
									textCss:"font-size:9px;",
									rowNum:0,
									align:"center"};
							winExitRightArr[winExitRightArr.length] = winCell;
							winExitLeftArr[winExitLeftArr.length] = winCell;
							
							isleRow =new Array();
							var groupBreakLetters = {};
							if(buildLetters){
								columnLetters = [];
								colLtrStr = "";
								var colGroupSizeArr = {};
								for(var cgI=0; cgI< cgDtos.length; cgI++ ){
									if(cgDtos[cgI].airRows.length>0){
										colGroupSizeArr[cgDtos[cgI].columnGroupId] = cgDtos[cgI].airRows[0].airSeats.length;
										var seats = cgDtos[cgI].airRows[0].airSeats;
										var groupLetters = [];
										for(var sI = 0; sI < seats.length;sI++){
											var colLetter = ((new String(seats[sI].seatNumber)).replace(/[\d]+/g, '')).toUpperCase();
											columnLetters[columnLetters.length]=colLetter;
											groupLetters[groupLetters.length] = colLetter;
										}
										groupLetters = $.airutil.sort.quickSort(groupLetters,$.airutil.string.comparator);
										groupBreakLetters[groupLetters[0]] = true;
									}
								}
								columnLetters = $.airutil.sort.quickSort(columnLetters,$.airutil.string.comparator);
								colLtrStr = columnLetters.join("");
								buildLetters = false;
							}
							var rowStatus = new Array();
							for(var cgI=0; cgI< cgDtos.length; cgI++ ){
								var rows  = cgDtos[cgI].airRows;
								/* initialize tr arrays */
								if(cgI == 0){
									for(var k =0; k< rows.length; k++){
										trArr[k] = new Array();
									}
								}
								
								for(var rI =0; rI< rows.length; rI++){
									var seats = rows[rI].airSeats;		
									var lastSeatC = "matchCC";
									var rowIndex = rows[rI].rowNumber;
									if(rowStatus[rowIndex] == undefined){
										rowStatus[rowIndex] = true;	
									}
									
									for(var sI = 0; sI < seats.length;sI++){
										strDesc = "&nbsp;";
										if(!activeCC){
											seats[sI].seatAvailability = "INALC";
											lastSeatC = "";
										} else {
											if(selectedClassOfService['lcc'] != null){												
												var seatLogicalCC = seats[sI].logicalCabinClassCode;
												if ($.inArray(seats[sI].logicalCabinClassCode, availLCCArray) == -1){
													availLCCArray[availLCCArray.length] = seats[sI].logicalCabinClassCode;
												}
												if(seatLogicalCC != selectedClassOfService['lcc']) {
													seats[sI].seatAvailability = "INALC";
													lastSeatC = "";
												}
											}
										}
										
										
										if(seats[sI].seatVisibility == "N"){
											strSeatClass = "style=visibility:hidden;";
										}else { 
											rowStatus[rowIndex] = false;
											switch (seats[sI].seatAvailability){
												case "VAC" : 
													var refObj = '{fltRefNo:\"'+fltRefNo+'\",seatNumber:\"'+seats[sI].seatNumber+'\"}';
													strSeatClass = "td_seat matchCC availSeat cursorPointer"; 												
													strDesc = "<a href='' onmouseover='UI_Anci.seatOnMouseOver(event,"+refObj+"); return false;'  onmouseout='UI_Anci.seatOnMouseOut("+refObj+"); return false;' onclick='return UI_Anci.seatOnClick("+refObj+"); return false;' class='txtSmall noTextDec'>&nbsp;&nbsp;&nbsp;&nbsp;<\/a>";
													break;
												case "INA" : strSeatClass = "td_seat matchCC notAvailSeat";  break;
												case "RES" : strSeatClass =  "td_seat matchCC notAvailSeat"; break; // occupied
																									// red
																									// -
																									// "SltdSeat"
												case "INALC" :										// occupied	
													var refObj = '{fltRefNo:\"'+fltRefNo+'\",seatNumber:\"'+seats[sI].seatNumber+'\"}';
													//strDesc = "<a href='#' onmouseover='UI_Anci.seatOnMouseOver(event,"+refObj+")'  onmouseout='UI_Anci.seatOnMouseOut("+refObj+")' class='txtSmall noTextDec'>&nbsp;&nbsp;&nbsp;&nbsp;<\/a>";
													strSeatClass =  "td_seat notAvailSeat"; break; 
												default: strSeatClass = "td_seat matchCC notAvailSeat";  break;
											}
										}
										var seatNo = seats[sI].seatNumber;
										
										if(populateRefMap){
											seatRefMap.seats[seatNo]={
													indexRefObj: {segId:intSegRow,ccId:ccI,rgId:rgI,cgId:cgI,rId:rI,sId:sI},
													avail:seats[sI].seatAvailability,
													seatCharge:seats[sI].seatCharge,
													seatMessage:seats[sI].seatMessage,
													seatNumber: seatNo,
													fltRefNo:fltRefNo,
													lccCode:seats[sI].logicalCabinClassCode,
													seatType:seats[sI].seatType
												};
										}
										
										var cell = {desc:strDesc, 
												id:"tdSeat_" + seatNo,// + "_"
																		// +
																		// objST[i].row[x].id,
												css:strSeatClass,
												rowNum:rows[rI].rowNumber,
												textCss:"font-size:9px;",
												align:"right",role:seatLogicalCC};
										var colLetter = ((new String(seatNo)).replace(/[\d]+/g, '')).toUpperCase();
										
										var letterIndex = colLtrStr.indexOf(colLetter)
										var sdLtrIndex = letterIndex % seats.length;
										var tIndex = rI;
										
										var totCols = 0;
										for(var i = 0;i < cgDtos.length;i++){
											totCols = totCols + cgDtos[i].airRows[0].airSeats.length;											
										}
										var colIndex = ( totCols- (letterIndex+1)); /*
																					 * F=0,
																					 * A=5
																					 */
										// row index and column index
										trArr[tIndex][colIndex] = cell;
										
										var blnFirstRow = (cgI == 0 && (colIndex == 0));
										var blnLastRow = (cgI == (cgDtos.length-1)) && (colIndex == (totCols -1));
										
										
										if(blnFirstRow||blnLastRow){
											switch (seats[sI].seatType){
												case "EXIT" : strWindowClass =(blnFirstRow? "exitRight":"exitLeft"); break;
												default : strWindowClass = "window"; break;
											}				
											var winExitCell = {desc:"&nbsp;", 
													css:strWindowClass,
													textCss:"font-size:9px;",
													rowNum:rows[rI].rowNumber,
													align:"center"};
											if(blnFirstRow){
												winExitRightArr[winExitRightArr.length] = winExitCell;	
											}else{
												winExitLeftArr[winExitLeftArr.length] = winExitCell;			
											}
										}
										
									}
									var rowNumber = rows[rI].rowNumber;
									var isEmptyRow = rowStatus[rowNumber];
									if(isEmptyRow){
										rowNumber = '';
									}
									// creating the isle row
									var cellIsle = {desc:(rowNumber), 
											css:"aisle "+lastSeatC,
											textCss:"font-size:9px;",
											rowNum:rows[rI].rowNumber,
											align:"right"};
									isleRow[rI] = cellIsle;
								}
							} /* end of column grp loop */
							
							
							/* populate table for rows per row group */
							/* cols = = = */
							/* rows | | | */
							
							if(trArr.length>0){
								
								trArr = $.airutil.sort.quickSort(trArr,seatRowComparator);
								var colNos = trArr[0].length;
								var rowNos = trArr.length;
								var colsPerCGrp = parseInt(colNos/cgDtos.length,10);
								var letterIndex = colNos;
								var colGrpCount = 1;
								var colCount = 0;
		
								/** adding seats and isles for row groups */
								for(var i=0; i < colNos; i++){							
									tblRow = document.createElement("TR");										
									if(i == colCount){
										colsPerCGrp = colGroupSizeArr[colGrpCount];
										colCount = colCount + colsPerCGrp;
										colGrpCount++;	
									}
									var lastLetter = '';
									inner : for(var j=0; j< rowNos; j++ ){
										/*
										 * booking class seat column letters
										 * print
										 */
										if(j==0 && letterIndex>0){
										    lastLetter = columnLetters[--letterIndex];
											var letterCell = {desc:lastLetter, 
													css:"aisle",
													textCss:"font-size:9px;font-weight:bold;color:red;",
													align:"right"};
											tblRow.appendChild(UI_commonSystem.createCell(letterCell));
											
										}
										/* adding seat columns to TR */
	                                    if (trArr[j][i] != undefined) {
	                                    	tdObj = UI_commonSystem.createCell(trArr[j][i]);
	                                    } else {
	                                    	continue inner;
	                                    }
										tblRow.appendChild(tdObj);
									}
									$('#tbodyRG_'+ccI+'_'+rgI).append(tblRow);
									
									/*
									 * creating isles inbetweeen seat column
									 * grps
									 */
									if( groupBreakLetters[lastLetter] && i < colNos-1){
										isleRow = $.airutil.sort.quickSort(isleRow,seatIsleComparator);
										isleRowtblRow = document.createElement("TR");	
										isleRowtblRow.appendChild(UI_commonSystem.createCell(blankIsle)); // Row
																											// Column
																											// Letter
										for (var k =0; k < rowNos; k++){
											isleRowtblRow.appendChild(UI_commonSystem.createCell(isleRow[k]));
										}
										$('#tbodyRG_'+ccI+'_'+rgI).append(isleRowtblRow);
									}									
								}	
							}
							
							
							
							
						} /* end of row groups loop */
						
						/* creating left wing windows */				
						winExitRightArr = 	$.airutil.sort.quickSort(winExitRightArr,seatIsleComparator);		
						for(var i=0; i < winExitRightArr.length ; i++){							 
							tblWingRowRight.appendChild(UI_commonSystem.createCell(winExitRightArr[i]));
						}
						$('#trCCRW_'+ccI).append(tblWingRowRight);
						
						/* creating right wing windows */
						winExitLeftArr = 	$.airutil.sort.quickSort(winExitLeftArr,seatIsleComparator);							
						for(var i=0; i < winExitLeftArr.length ; i++){
							tblWingRowLeft.appendChild(UI_commonSystem.createCell(winExitLeftArr[i]));
						}
						$('#trCCLW_'+ccI).append(tblWingRowLeft);
						
						
					} /* end of cabin class loop */
					
				}else{ /* end of cabin classes len check */
					$("#spnAnciNoSeat").show();
				}
				
			}else{ /* end of no segment */
				$("#spnAnciNoSeat").show();
			}
		}
		if(populateRefMap){
			var fltRefNo = UI_Anci.jsSMModel[intSegRow].flightSegmentDTO.flightRefNumber;
			UI_Anci.seatRefMap[fltRefNo] = seatRefMap;
		}
		
		var setCabinCalssLabel = function(){
			var cabinClass = $(".cabinClass");
			var zIn = 9;
			for (var _cc = 0; _cc<cabinClass.length; _cc++){
				var t_Cabin = $(cabinClass[_cc]);
				var firstRowSeats = t_Cabin.find(".tbSeatMap>tr:eq(1)").find(".td_seat");
				//UI_tabAnci.lccMap[seatObj.lccCode].description
				for(var x = 0; x<availLCCArray.length;x++){
					zIn++;
					var lebelLeft = 0;
					var showLable = false;
					for (var y = 0;y<firstRowSeats.length;y++){
						if ($(firstRowSeats[y]).attr("role") == availLCCArray[x]){
							lebelLeft = $(firstRowSeats[y]).position().left;
							showLable =true;
							break;
						}
					}
							if (showLable){
					var clnam = UI_Anci.lccMap[availLCCArray[x]].description.replace(/fare/gi, "Seat");
					//var divCC = $("<span class='labelLC'>"+UI_Anci.lccMap[availLCCArray[x]].description.replace(/fare/gi, "Seat")+"</span>");
					var divCC = $("<span class='labelLC' title='"+clnam+"'>"+clnam+"</span>");
					divCC.css({
						"position":"relative",
						"top":48,
						"padding":"2px 10px",
						"border":"1px solid #777",
						"border-bottom":"0px",
						"background-color":"#fff",
						//"left":(divCC.position().left) + ((lebelLeft + 16) - $(".wingRight").position().left ),
						"left":0,
						"z-index":zIn
					});
					if (selectedClassOfService['lcc'] == availLCCArray[x]){
						divCC.addClass("matchCC");
						$(".wingRight").prepend(divCC);
						getleft = ($(".wingRight").position().left - divCC.position().left) + ((lebelLeft + 16) - $(".wingRight").position().left );
						divCC.css("left",getleft);
					}
					
						}
					}
				}
		};
		$(".labelLC").remove();
		if (availLCCArray.length > 1){
			setCabinCalssLabel();;
		}
	}
	
	
	/*
	 * return the fltSegment Map,
	 * If not already build build and return the flight segment map
	 */
	function getFltSegMap(fltRefNo){
		var seatRefMap = UI_Anci.seatRefMap[fltRefNo];
		if(seatRefMap == null ){
			//-------------------------------------------------------
			UI_Anci.seatRefMap[fltRefNo] = {seats: {}};
			seatRefMap = UI_Anci.seatRefMap[fltRefNo];
			for(var intSegRow=0; intSegRow < UI_Anci.jsSMModel.length; intSegRow++){
				if(compareFltRefNos(UI_Anci.jsSMModel[intSegRow].flightSegmentDTO.flightRefNumber, fltRefNo)){
					var cabinCls = UI_Anci.jsSMModel[intSegRow].lccSeatMapDetailDTO.cabinClass; 			
					var ccLen = cabinCls.length;	
					if(ccLen>0){
						for(var ccI=0; ccI < ccLen ; ccI++){
							var cc  = cabinCls[ccI];
							var rgDtos = cc.airRowGroupDTOs;
							if(UI_Anci.jsSMModel[intSegRow].flightSegmentDTO.cabinClassCode === cc.cabinType){
								for(var rgI=0; rgI< rgDtos.length;rgI++){
									var cgDtos = rgDtos[rgI].airColumnGroups; 								
									for(var cgI=0; cgI< cgDtos.length; cgI++ ){
										var rows  = cgDtos[cgI].airRows;								
										for(var rI =0; rI< rows.length; rI++){
											var seats = rows[rI].airSeats;
											for(var sI = 0; sI < seats.length;sI++){
												
												var seatNo = seats[sI].seatNumber;
												
												seatRefMap.seats[seatNo]={
															indexRefObj: {segId:intSegRow,ccId:ccI,rgId:rgI,cgId:cgI,rId:rI,sId:sI},
															avail:seats[sI].seatAvailability,
															seatCharge:seats[sI].seatCharge,
															seatMessage:seats[sI].seatMessage,
															seatNumber: seatNo,
															fltRefNo:fltRefNo,
															lccCode:seats[sI].logicalCabinClassCode,
															seatType:seats[sI].seatType
												};
											}
										}
									}
								}
							}
						}
					}
				}
			}
			UI_Anci.seatRefMap[fltRefNo] = seatRefMap;
		}
			//-------------------------------------------------
		return seatRefMap;
	}
	
	/*
	 * Get seat object for given reference
	 */
	UI_Anci.getSeatObj = function (refObj){
		var seatRefMap = getFltSegMap(refObj.fltRefNo)
		var seatObj = seatRefMap.seats[refObj.seatNumber];
		if (seatObj == undefined) seatObj = null;
		return seatObj;
	}
	/*
	 * Get row object for given reference
	 */
	UI_Anci.getRowObj = function (seatRefObj){
		var refObj = UI_Anci.getSeatObj(seatRefObj).indexRefObj;
		var retSeatObj =  UI_Anci.jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].airSeats;
		retSeatObj.segCode = UI_Anci.jsSMModel[refObj.segId].flightSegmentDTO.segmentCode;
		return retSeatObj;
	}
	
	/*
	 * Get segment code for given reference
	 */
	UI_Anci.getSeatFltRefNo = function (refObj){
		return UI_Anci.jsSMModel[refObj.segId].flightSegmentDTO.flightRefNumber;
	}
	
	/*
	 * Get seat availability breakdown per row for given reference
	 */
	UI_Anci.getSeatAvailObjForRow = function (seatRefObj){
		var refObj = UI_Anci.getSeatObj(seatRefObj).indexRefObj;
		var rowSeatAvailObj =  UI_Anci.jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].passengerTypeQuantity;
		rowSeatAvailObj.segCode = UI_Anci.jsSMModel[refObj.segId].flightSegmentDTO.segmentCode;
		return rowSeatAvailObj;
	}
	
	/*
	 * Update seat availability
	 */
	UI_Anci.updateSeatAvailRow = function(seatRefObj,updateType){
		var refObj = UI_Anci.getSeatObj(seatRefObj.refObj).indexRefObj;
		var paxType = seatRefObj.paxType;
		var paxTypeHash = {};
		paxTypeHash[paxType] = true;
		if(paxType == 'PA'){
			paxTypeHash['IN'] = true;
		}
		var rowSeatAvailObj =  UI_Anci.jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].passengerTypeQuantity;
		for(var i=0; i < rowSeatAvailObj.length; i++){			
			if(paxTypeHash[rowSeatAvailObj[i].paxType]!=null && paxTypeHash[rowSeatAvailObj[i].paxType]){
				if(updateType == 'INC'){
					rowSeatAvailObj[i].quantity = parseInt(rowSeatAvailObj[i].quantity)+1;
				}else if(updateType == 'DEC'){
					rowSeatAvailObj[i].quantity = parseInt(rowSeatAvailObj[i].quantity)-1;
				}
				break;
			}
		}
	}
	/*
	 * Seat availability check
	 * 
	 * 
	 * @return true if seat is occupied false otherwise
	 */
	UI_Anci.isSeatOccupied = function(seatObjRef){
		var fltRefNo = seatObjRef.fltRefNo;		
		var occSeatObj = null;
		
		if (UI_Anci.occupiedSeats[fltRefNo] != undefined) {
			occSeatObj = UI_Anci.occupiedSeats[fltRefNo].seats[seatObjRef.seatNumber];
		}
		
		if(occSeatObj != undefined &&  occSeatObj !=null){
			return true;
		}else { 
			return false;
		}		
	}
	
	UI_Anci.isSatisfySeatAssignementConstrains = function (seatObjRef){
		
		var flightSeatsAvailable = false;
		var fltRefNo = seatObjRef.fltRefNo;
		for(var i=0; i < UI_Anci.jsAnciSeg.length; i++){
			if(compareFltRefNos(fltRefNo,UI_Anci.jsAnciSeg[i].flightSegmentTO.flightRefNumber)){
				flightSeatsAvailable = UI_Anci.jsAnciSeg[i].seat;
				break;
			}
		}
		
		if(flightSeatsAvailable){
		 var rowNumber = seatObjRef.rowNumber;
			if(rowNumber >= UI_Top.holder().GLOBALS.autoSeatAssignmentStartingRowNumber){
			  return true;
			}
		}
		return false;
	}
	/*
	 * Get occuppied seat reference object
	 */
	UI_Anci.getOccupiedSeat = function(p){
		var fltRefNo = UI_Anci.getFltRefNoForIndex(p.segId);
		var occSeatObj = null;
		$.each(UI_Anci.occupiedSeats[fltRefNo].seats, function(seatNo, seatRefObj){
				
			if(seatRefObj !=null && parseInt(seatRefObj.paxId,10)  == p.paxId){
				occSeatObj = seatRefObj;
				return;
			}
		});
		return occSeatObj;
	}
	/*
	 * Mark the seat occupied
	 * 
	 */
	UI_Anci.markSeat = function(seatObjRef){
		//{paxId : UI_Anci.seat.currentPaxId, paxType: paxType, refObj:refObj};
		UI_commonSystem.pageOnChange();
		var fltRefNo = seatObjRef.refObj.fltRefNo;
		var seatNumber = seatObjRef.refObj.seatNumber;
		UI_Anci.updateSeatAvailRow(seatObjRef,'DEC'); 
		var seatObj = UI_Anci.getSeatObj(seatObjRef.refObj);
		
		var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.SEAT_MAP, seatObj.seatCharge, seatObjRef.segId);
		
		if(isFreeService){
			seatObj.seatCharge = 0;
		}
		
		UI_Anci.updateAnciTotals(UI_Anci.anciType.SEAT,seatObj.seatCharge,'ADD');
		UI_Anci.updateAnciCounts(UI_Anci.anciType.SEAT,1,'ADD');
		
		if (UI_Anci.jsAnciSegModel.length == 1) {
			UI_Anci.jsAnciSegModel[0].pax[seatObjRef.paxId].seat.seatNumber=seatObjRef.refObj.seatNumber;
			UI_Anci.jsAnciSegModel[0].pax[seatObjRef.paxId].seat.seatCharge=seatObj.seatCharge;
		} else {
			UI_Anci.jsAnciSegModel[seatObjRef.segId].pax[seatObjRef.paxId].seat.seatNumber=seatObjRef.refObj.seatNumber;
			UI_Anci.jsAnciSegModel[seatObjRef.segId].pax[seatObjRef.paxId].seat.seatCharge=seatObj.seatCharge;
		}
		UI_Anci.updateFlightAnciView();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
		UI_Anci.occupiedSeats[fltRefNo].seats[seatNumber] = seatObjRef;
		UI_Anci.updateSeatChargeInView();
	}
	/*
	 * Clear the seat
	 * 
	 * @return true on successful removal false otherwise
	 * 
	 */
	UI_Anci.clearSeat = function(seatObjRef){
		UI_commonSystem.pageOnChange();
		var fltRefNo = seatObjRef.refObj.fltRefNo;
		var seatNumber = seatObjRef.refObj.seatNumber;
		var occSeat = UI_Anci.occupiedSeats[fltRefNo].seats[seatNumber];
		if(occSeat!=null &&  parseInt(occSeat.paxId,10) >= 0){
			UI_Anci.updateSeatAvailRow(seatObjRef,'INC');
			var seatObj =  UI_Anci.getSeatObj(occSeat.refObj);
			UI_Anci.updateAnciTotals(UI_Anci.anciType.SEAT,seatObj.seatCharge,'DEDUCT');
			UI_Anci.updateAnciCounts(UI_Anci.anciType.SEAT,1,'DEDUCT');
			
			if (UI_Anci.jsAnciSegModel.length == 1) {
				UI_Anci.jsAnciSegModel[0].pax[seatObjRef.paxId].seat.seatNumber='';
				UI_Anci.jsAnciSegModel[0].pax[seatObjRef.paxId].seat.seatCharge=0;
			} else {
				UI_Anci.jsAnciSegModel[seatObj.indexRefObj.segId].pax[seatObjRef.paxId].seat.seatNumber='';
				UI_Anci.jsAnciSegModel[seatObj.indexRefObj.segId].pax[seatObjRef.paxId].seat.seatCharge=0;
			}
			UI_Anci.updateFlightAnciView();
			UI_Anci.updateJnTax();
			UI_Anci.updateAnciPenalty();
			UI_Anci.occupiedSeats[fltRefNo].seats[seatNumber] = null;
			
			if(UI_Anci.getAncillarySummary(UI_Anci.anciType.SEAT).qty < 1){
			    $("#chkSeatTerms").attr('checked', false);	  
       			    $("#seatTermsRow").hide();			   
			}

			
			return true;
		}else{
			return false;
		}		
	}
	
	/*
	 * Pax has infant?
	 * 
	 * @return true if passenger id has infant associated with it otherwise
	 * false
	 */
	UI_Anci.paxHasInfant = function(paxId){
		if(UI_Anci.jsonPaxAdults == null){
			return false;
		}
		
		var pax = UI_Anci.jsonPaxAdults[parseInt(paxId,10)];
		if(pax.paxType == 'PA'){
			return true;
		}
		if(pax.parent == 'Y'){
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * Check seat for infant seating
	 */
	UI_Anci.canSeatInfant = function(refObj,paxId,segId){
		var rowAvail = UI_Anci.getSeatAvailObjForRow(refObj);
		for(var i=0; i < rowAvail.length; i++){
			if(rowAvail[i].paxType=='IN'){
				if(parseInt(rowAvail[i].quantity,10) >0 ){
					return true;
				}else if(parseInt(rowAvail[i].quantity,10) ==0){
					var seatRef = UI_Anci.getOccupiedSeat({paxId:paxId, segId: segId});
					if(seatRef == null)
						break;
					var seats = UI_Anci.getRowObj(seatRef.refObj);
					for(j=0; j< seats.length;j++){
						if(seats[j].seatNumber == refObj.seatNumber){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	/*
	 * Seat Mouse Over
	 */
	UI_Anci.seatOnMouseOver = function(objE, refObj){
		
		var x = 0
		var y = 0;
		if (objE != null){
			if (UI_Anci.browser.msie) {
				x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
				y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
			}else{
				if ((navigator.userAgent.indexOf("Safari") != -1) ||
					(navigator.userAgent.indexOf("Opera") != -1)){
					x = objE.clientX;
					y = objE.clientY;
				}else{
					x = objE.clientX + window.scrollX;
					y = objE.clientY + window.scrollY;
				}
			}	
		}
		$("#spnSeatInfo").show();
		$("#spnSeatInfo").css({ "left": (x + 10)+ "px"});
		$("#spnSeatInfo").css({ "top": (y + 10) + "px"});
		
		var seatObj = UI_Anci.getSeatObj(refObj);
		var selLogicalCC = UI_Container.getSelectedClassOfServiceDetails(refObj.fltRefNo);
		var strSeatNo = seatObj.seatNumber;
		var strPrice = seatObj.seatCharge;
		var strLccName = UI_Anci.lccMap[seatObj.lccCode].description;
		if (selLogicalCC['cc'] == seatObj.lccCode){
			$("#spnTTSeatNo").html(strSeatNo);
			var applyFormatter = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.SEAT_MAP,strPrice, UI_Anci.getOndSequence(UI_Anci.seat.currentSegId));
			var frmtSeatPrice = UI_Anci.convertAmountToSelectedCurrency(strPrice) + ' ' +UI_Anci.currency.selected;
			if(applyFormatter){
				frmtSeatPrice = UI_Anci.freeServiceDecorator;				
			}
			$("#spnTTSeatChg").html(frmtSeatPrice);
			if(!UI_Anci.isSeatOccupied(refObj)){
				$("#tdSeat_" + strSeatNo ).removeClass("availSeat");
				$("#tdSeat_" + strSeatNo ).addClass("SltdSeat");
			}
		}else{
			$("#spnTTSeatNo").html(strSeatNo);
			$("#spnTTSeatChg").html(strLccName);
		}
		return false;
	}
	
	/*
	 * Seat On Mouse out
	 */ 
	UI_Anci.seatOnMouseOut = function(refObj){
		$("#spnSeatInfo").hide();
		
		var strSeatNo = refObj.seatNumber;
		
		if(!UI_Anci.isSeatOccupied(refObj)){
			$("#tdSeat_" + strSeatNo ).removeClass("SltdSeat");
			$("#tdSeat_" + strSeatNo ).addClass("availSeat");
		}
		return false;
	}
	
	/*
	 * Validate the passenger id
	 */
	UI_Anci.validatePaxId = function(paxId){
		if(paxId < 0){
			jAlert(UI_Container.labels.lblAnciSelectPaxAlert);
			return false;
		}else{
			return true;
		}
	}
	
	UI_Anci.seatOnClick = function(refObj){
		if(!UI_Anci.validatePaxId(UI_Anci.seat.currentPaxId)){
			return false;
		}

		if(!UI_Anci.isSeatOccupied(refObj)){
			var seatObj = UI_Anci.getSeatObj(refObj);
			var seatMessage = seatObj.seatMessage;			
			var strSeatNo =  refObj.seatNumber;
			
			if (typeof seatMessage !='undefined' && seatMessage !=null && seatMessage.length >0){
			    seatMessage +=  '<br><br>' + UI_Anci.jsonLabel['lblSeatSelectionConfirmation']; 
			    (function (seatMsg, seatRef) {
				var confirmHeader = UI_Anci.jsonLabel['lblSeatSelectionConfirmationHeader'] + ' ' + strSeatNo;
			        jConfirm(seatMsg, confirmHeader, function(response) {
    					if (response){
					    UI_Anci.seatOnClickSelection(seatRef);	
					} else {
					    return false;	
					}       					
  				});
			     })(seatMessage, refObj);	
			} else {
			    	UI_Anci.seatOnClickSelection(refObj);
			}
		}

		return false;
	}
	
	/*
	 * Seat On Click
	 * 
	 * refobj keys - segId,ccId,rgId,cgId,rid,sId
	 */
	UI_Anci.seatOnClickSelection = function(refObj){
		//if (UI_Anci.validateApply()){
		if(!UI_Anci.validatePaxId(UI_Anci.seat.currentPaxId)){
			return false;
		}
			if(!UI_Anci.isSeatOccupied(refObj)){
						
				var strSeatNo =  refObj.seatNumber;
				var seatObj = UI_Anci.getSeatObj(refObj);
				var paxType = UI_Anci.jsonPaxAdults[UI_Anci.seat.currentPaxId].paxType;				
				/* derive Infant pax */
				if(UI_Anci.paxHasInfant(UI_Anci.seat.currentPaxId)){
					paxType = "PA";
					if(!UI_Anci.canSeatInfant(refObj, UI_Anci.seat.currentPaxId, seatObj.indexRefObj.segId) ||
							seatObj.seatType == "EXIT"){
						jConfirm(UI_Anci.jsonLabel['lblSeatNotAvailForParent'], 'Ok', function(){});						
						return false;
					}
				}
					
				var mObj= {paxId : UI_Anci.seat.currentPaxId, paxType: paxType, refObj:refObj, segId: seatObj.indexRefObj.segId};
				var rObj = {paxId: UI_Anci.seat.currentPaxId, segId: seatObj.indexRefObj.segId};
				UI_Anci.removeSeatPax(rObj);
				UI_Anci.markSeat(mObj);
				var seatCharge = UI_Anci.convertAmountToSelectedCurrency(seatObj.seatCharge);
				var applyFormatter = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.SEAT_MAP,seatCharge, UI_Anci.getOndSequence(seatObj.indexRefObj.segId));                		
        		if(applyFormatter){
        			seatCharge = UI_Anci.freeServiceDecorator;	
        		}	
				if (UI_Anci.seatViewType == "New-view"){
					
					var parentId = "#tdSeatSeg_"+seatObj.indexRefObj.segId+"";
					$(parentId).find("#seatPax_SeatNo_" + UI_Anci.seat.currentPaxId).html(strSeatNo);
					$(parentId).find("#seatPax_Charge_" + UI_Anci.seat.currentPaxId).html(seatCharge);
					$(parentId).find('#seatPax_Clear_' + UI_Anci.seat.currentPaxId).find('a').show();
				}else{
					var parentId = "";
					$(parentId+ "#seatPax_SeatNo_" + UI_Anci.seat.currentPaxId).html(strSeatNo);
					$(parentId+ "#seatPax_Charge_" + UI_Anci.seat.currentPaxId).html(seatCharge);
					$(parentId +'#seatPax_Clear_' + UI_Anci.seat.currentPaxId).find('a').show();
				}
				
				$("#tdSeat_" + strSeatNo ).removeClass("availSeat");
				$("#tdSeat_" + strSeatNo ).addClass("SltdSeat");
				if (anciConfig.showSeatMessageTermsCheckBox){
					$("#seatTermsRow").show();
				}
				
				
				
				if (UI_Anci.seatViewType == "New-view"){
					UI_Anci.segmentPaxUsedArray[UI_Anci.currentSegPax].seatUsed = true;
					if ($("input[name='radSeatPax']").length - 1 > UI_Anci.currentSegPax)
						UI_Anci.currentSegPax++;
					else
						UI_Anci.currentSegPax=0;
					
					UI_Anci.trigSegPax();
				}
				if(UI_Container.isModifyAncillary()){
					UI_Anci.displayOldSeats();
				}
			}
			return false;
		//}
	}
	
	UI_Anci.getOndSequence = function(segId){
		if(UI_Anci.jsAnciSeg != null && UI_Anci.jsAnciSeg[segId] != undefined
				&& UI_Anci.jsAnciSeg[segId] != null){
			var segAnciObj = UI_Anci.jsAnciSeg[segId];
			if(segAnciObj.flightSegmentTO != null
					&& segAnciObj.flightSegmentTO.ondSequence != null){
				return segAnciObj.flightSegmentTO.ondSequence;
			}
		}
		
		return segId;
	}
	
	/*
	 * Remove Pax Seat
	 */
	UI_Anci.removeSeatPax = function(refObj){
		if (UI_Anci.seatViewType == "New-view"){
			var eid = "radSeatPax-"+refObj.paxId+":tdSeatSeg_"+refObj.segId;
			UI_Anci.currentSegPax = getIndex($("input[name='radSeatPax']"),eid);
			UI_commonSystem.pageOnChange();
			var parentId = "#tdSeatSeg_"+refObj.segId+"";
			var paxperSeg = $(parentId).find("input[type='radio']");
			if ($(paxperSeg[refObj.paxId]).attr("checked")){
			
				$(parentId).find("#seatPax_SeatNo_" + refObj.paxId).html(" - ");
				$(parentId).find("#seatPax_Charge_" + refObj.paxId).html(" - ");
				$(parentId).find('#seatPax_Clear_' + refObj.paxId).find('a').hide();
				var seatRefObj = UI_Anci.getOccupiedSeat(refObj);
				if(seatRefObj==null){
					return;
				}
				UI_Anci.clearSeat(seatRefObj);
				
				var strSeatNo = seatRefObj.refObj.seatNumber;
				
				$("#tdSeat_" + strSeatNo ).removeClass("SltdSeat");
				$("#tdSeat_" + strSeatNo ).addClass("availSeat");
				UI_Anci.segmentPaxUsedArray[UI_Anci.currentSegPax].seatUsed = false;
			}
			UI_Anci.updateSeatChargeInView();
		}else{
			UI_commonSystem.pageOnChange();
			$("#seatPax_SeatNo_" + refObj.paxId).html(" - ");
			$("#seatPax_Charge_" + refObj.paxId).html(" - ");
			$('#seatPax_Clear_' + refObj.paxId).find('a').hide();
			refObj.segId = UI_Anci.seat.currentSegId;
			var seatRefObj = UI_Anci.getOccupiedSeat(refObj);
			UI_Anci.updateSeatChargeInView();
			if(seatRefObj==null){
				return;
			}
			UI_Anci.clearSeat(seatRefObj);
			
			var strSeatNo = seatRefObj.refObj.seatNumber;
			
			$("#tdSeat_" + strSeatNo ).removeClass("SltdSeat");
			$("#tdSeat_" + strSeatNo ).addClass("availSeat");
		}
		
	}
	/*
	 * Clear seat pax 
	 */
	UI_Anci.clearSeatPax = function (){
		UI_commonSystem.pageOnChange();
		for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
			
			$("#seatPax_SeatNo_" + i).html(" - ");
			$("#seatPax_Charge_" + i).html(" - ");
			$('#seatPax_Clear_' + i).find('a').hide();
		}
	}
	/*
	 * Populate seat map data
	 */
	UI_Anci.populateSMData = function (p){
		if(UI_Anci.occupiedSeats == null ){
			jAlert("Error occured while loading seat data");
			return ;
		}
		var fltRefNo = UI_Anci.getFltRefNoForIndex(p.segId);
		UI_Anci.fltRefNo = UI_Anci.getFltRefNoForIndex(p.segId);
		var occSeats = UI_Anci.occupiedSeats[fltRefNo].seats;
		var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.SEAT_MAP, null, UI_Anci.getOndSequence(p.segId));
		
		if (UI_Anci.seatViewType == "New-view"){
			var parentId = "#tdSeatSeg_"+p.segId+"";
			$.each(occSeats, function(seatNumber,occSeat){
				if(occSeat!=null){
					var strSeatNo = seatNumber;
					var strSeatCharge = UI_Anci.convertAmountToSelectedCurrency(UI_Anci.getSeatObj(occSeat.refObj).seatCharge);					
					var applyFormatter = UI_Anci.isApplyDecorator(strSeatCharge,isFreeService);					
	        		if(applyFormatter){
	        			strSeatCharge = UI_Anci.freeServiceDecorator;	
	        		}
					var paxId = parseInt(occSeat.paxId,10);
					$("#tdSeat_" + strSeatNo ).removeClass("availSeat");
					$("#tdSeat_" + strSeatNo ).addClass("SltdSeat");
					$(parentId).find("#seatPax_SeatNo_" + paxId).html(strSeatNo);
					$(parentId).find("#seatPax_Charge_" + paxId).html(strSeatCharge);
					$(parentId).find('#seatPax_Clear_' + paxId).find('a').show();
				}
			});
			
		}else{
			var parentId = "";
			UI_Anci.clearSeatPax();
			$.each(occSeats, function(seatNumber,occSeat){
				if(occSeat!=null){
					var strSeatNo = seatNumber;
					var strSeatCharge = UI_Anci.convertAmountToSelectedCurrency(UI_Anci.getSeatObj(occSeat.refObj).seatCharge);  
					var applyFormatter = UI_Anci.isApplyDecorator(strSeatCharge,isFreeService);
	        		if(applyFormatter){
	        			strSeatCharge = UI_Anci.freeServiceDecorator;	
	        		}	
					var paxId = parseInt(occSeat.paxId,10);
					$("#tdSeat_" + strSeatNo ).removeClass("availSeat");
					$("#tdSeat_" + strSeatNo ).addClass("SltdSeat");
					$("#seatPax_SeatNo_" + paxId).html(strSeatNo);
					$("#seatPax_Charge_" + paxId).html(strSeatCharge);
					$('#seatPax_Clear_' + paxId).find('a').show();
				}
			});
		}
		
		if(UI_Container.isModifyAncillary()){
			var selPaxId = -1;
			for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
				$('#radSeatPax_'+i).show();
				var seatDTO = UI_Anci.jsonPaxAdults[i].currentAncillaries[p.segId].airSeatDTO;
				if(seatDTO!=null){
					var strSeatCharge = UI_Anci.convertAmountToSelectedCurrency(seatDTO.seatCharge);  
					var applyFormatter = UI_Anci.isApplyDecorator(strSeatCharge,isFreeService);
		    		if(applyFormatter){
		    			strSeatCharge = UI_Anci.freeServiceDecorator;	
		    		}
					if($('#radSeatPax_'+i).hide().attr('checked')){
						$('#radSeatPax_'+i).attr('checked',false);
						clearSeatPaxStyle(UI_Anci.seat.currentPaxId)
						UI_Anci.seat.currentPaxId = -1;
					}					
					
					var refObj = '{fltRefNo:\"' + fltRefNo + '\",seatNumber:\"' + seatDTO.seatNumber + '\"}';
					var strDesc = "<a href='' onmouseover='UI_Anci.seatOnMouseOver(event,"+refObj+"); return false;'  onmouseout='UI_Anci.seatOnMouseOut("+refObj+"); return false;' onclick='return UI_Anci.seatOnClick("+refObj+"); return false;' class='txtSmall noTextDec'>&nbsp;&nbsp;&nbsp;&nbsp;<\/a>";

					$("#tdSeat_" + seatDTO.seatNumber ).removeClass("notAvailSeat");
					$("#tdSeat_" + seatDTO.seatNumber ).addClass("availSeat");
					$("#tdSeat_" + seatDTO.seatNumber ).html(strDesc);
					
					$(parentId + "#seatPax_SeatNo_" + i).html(seatDTO.seatNumber);
					$(parentId + "#seatPax_Charge_" + i).html(strSeatCharge);
					$(parentId + '#seatPax_Clear_' + i).find('a').hide();
				}else {
					if(selPaxId <0){
						selPaxId = i;
					}
				}
			}
			if(UI_Anci.seat.currentPaxId<0 && selPaxId>=0){
				UI_Anci.seat.currentPaxId = selPaxId;				
			}
			UI_Anci.displayOldSeats();
		}else{
			if (UI_Anci.seatViewType != "New-view"){
				UI_Anci.seat.currentPaxId =0;
			}
		}
		if(UI_Anci.seat.currentPaxId>=0){
			$('#radSeatPax_'+UI_Anci.seat.currentPaxId).click();
		}
		
		UI_SocialSeat.init();
	}
	
	// ---------------------------------------------- Meal --------------------------------------------------------------
	/*
	 * Meal On Click
	 */
	UI_Anci.tabClickMeal = function(){
		// 0 - tdMealSeg;
		// 1 - Seg Row;
		var arrData = this.id.split("_");
		var intSegRow = arrData[1];
		UI_Anci.clickMealSegment({ind:intSegRow});
	}
	
	/*
	 * Click Meal segment
	 */
	UI_Anci.clickMealSegment = function(p){
		UI_commonSystem.pageOnChange();
		UI_Anci.meal.currentSegId = p.ind;
		
		if (UI_Anci.tabSltdMeal != null){
			$("#tdMealSeg_" + UI_Anci.tabSltdMeal).removeClass("tabSltd");
			$("#tdMealSeg_" + UI_Anci.tabSltdMeal).addClass("tabNotSltd");
			
			$("#lbl_tdMealSeg_" + UI_Anci.tabSltdMeal).removeClass("gridHDFont fntBold");
			$("#lbl_tdMealSeg_" + UI_Anci.tabSltdMeal).addClass("fntDefault");
		}
		
		UI_Anci.tabSltdMeal = p.ind;
		$("#tdMealSeg_" + UI_Anci.tabSltdMeal).removeClass("tabNotSltd");
		$("#tdMealSeg_" + UI_Anci.tabSltdMeal).addClass("tabSltd");
		
		$("#lbl_tdMealSeg_" + UI_Anci.tabSltdMeal).removeClass("fntDefault");
		$("#lbl_tdMealSeg_" + UI_Anci.tabSltdMeal).addClass("gridHDFont fntBold");
		
		UI_Anci.buildMeals({segId:p.ind});
		UI_Anci.populateMealData({segId:p.ind});
	}
	
	
	/* 
	 * Get index of the meal model segment
	 */
	UI_Anci.getMealSegIndex = function(segId){
		var intSegRow = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
		intSegRow = UI_Anci.getMealSegIndexByFltRefNo(fltRefNo);
		return intSegRow;
	}

    UI_Anci.getMealSegIndexByFltRefNo = function(fltRefNo){
		var intSegRow = null;
        if (UI_Anci.jsMealModel != null) {
            for (var i = 0; i < UI_Anci.jsMealModel.length; i++) {
                if (compareFltRefNos(fltRefNo, UI_Anci.jsMealModel[i].flightSegmentTO.flightRefNumber)) {
                    intSegRow = i;
                    break;
                }
            }
        }
		return intSegRow;
	}
	
	/* 
	 * Get index of the baggage model segment
	 */
	UI_Anci.getBaggageSegIndex = function(segId){
		var intSegRow = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
        intSegRow = UI_Anci.getBaggageSegIndexByFltRefNo(fltRefNo);
		return intSegRow;
	}

    UI_Anci.getBaggageSegIndexByFltRefNo = function(fltRefNo){
        var intSegRow = null;
        if (UI_Anci.jsBaggageModel != null) {
            for(var i=0; i < UI_Anci.jsBaggageModel.length; i++){
                if(compareFltRefNos(fltRefNo,UI_Anci.jsBaggageModel[i].flightSegmentTO.flightRefNumber)){
                    intSegRow = i; break;
                }
            }
        }
        return intSegRow;
    }

    UI_Anci.getFlightSegIdOfBaggageSegId = function(flightRefNo) {
        var fltSegId = null;

        for(var i=0; i < UI_Anci.jsAnciSegModel.length; i++){
            if(compareFltRefNos(flightRefNo, UI_Anci.jsAnciSegModel[i].flightRefNumber)){
                fltSegId = i; break;
            }
        }

        return fltSegId;
    }
	
	/* 
	 * Get index of the baggage from the js model segment
	 */
	UI_Anci.getSegIndexForBaggages = function(fltRefNo){
		var intSegRow = null; 
		//var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
		for(var i=0; i < UI_Anci.jsAnciSeg.length; i++){
			if(compareFltRefNos(fltRefNo,UI_Anci.jsAnciSeg[i].flightSegmentTO.flightRefNumber)){
				intSegRow = i; break;
			}
		}
		return intSegRow;
		
	}
	
	/*
	 * Build meals
	 */
	UI_Anci.buildMeals = function(p){
		checkChoosedMeals = function(dataArray,catID,mealIndex,whatToReturn){
			var ret = (whatToReturn == "count") ? 0 : false;
			for (var i = 0; i < dataArray.length; i++){
                if(catID==="-"){
                    dataArray[i].mealCategoryID = "-";
                }
				if (dataArray[i].mealCategoryID == catID && dataArray[i].mealIndex == mealIndex){
					if (whatToReturn == "count"){
						ret = dataArray[i].count;
						break;
					}else{
						ret = true;
						break;
					}
				}
			};
			return ret
		};
		var intSegRow = UI_Anci.getMealSegIndex(p.segId);
		var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.MEALS, null, UI_Anci.getOndSequence(intSegRow));
		
		if (UI_Anci.mealViewType == "New-view"){
			var htmlSTR = '';
			if(intSegRow!=null){
				for (var i =0; i < UI_Anci.jsMealModel[intSegRow].meals.length; i++){
					var tm = UI_Anci.jsMealModel[intSegRow].meals[i];
					
					var applyFormatter = UI_Anci.isApplyDecorator(tm.mealCharge,isFreeService);					
					var mealPriceLabel = UI_Anci.currency.selected+'</label><br><label class="bigtext thumbImg">'+UI_Anci.convertAmountToSelectedCurrency(tm.mealCharge);
					if(applyFormatter){	        			
	        			mealPriceLabel =UI_Anci.freeServiceDecorator;
	        		}
					if (p.catId == "-"){
						if (i%2 == 1)
							htmlSTR += '<div class="mealItem" id="'+p.segId+'_'+p.paxId+'_'+p.catId+'_'+i+'">';
						else
							htmlSTR += '<div class="mealItem" id="'+p.segId+'_'+p.paxId+'_'+p.catId+'_'+i+'">';
						if (checkChoosedMeals(p.choosed,p.catId,i,'bool')){
							htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>'+
							'<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="'+solveImageUrl(tm.mealImagePath)+'" height="50"/></div></td>'+
							'<td class="name"><label class="thumbImg">'+tm.mealName+'</label></td><td class="qtyHead bdLeft"><label>Qty</label></td><td class="priceHead bdLeft"><label>Price</label></td></tr><tr>'+
							'<td class="description"><label>'+tm.mealDescription+'</label></td>'+
							'<td class="qtyBody bdLeft"><label id="spin_'+p.catId+'_'+i+'" class="mealSpinner">'+checkChoosedMeals(p.choosed,p.catId,i,'count')+'x</label></td>'+
							'<td class="priceBody bdLeft"><label id="lblcurrency">'+mealPriceLabel+'</label>'+
							'<span class="imgpath" style="display:none">'+solveImageUrl(tm.mealImagePath)+'</span></td></tr></table></div>';
						}else{
							htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>'+
							'<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="'+solveImageUrl(tm.mealImagePath)+'" height="50"/></div></td>'+
							'<td class="name"><label class="thumbImg">'+tm.mealName+'</label></td><td class="bdLeft"></td><td class="priceHead bdLeft"><label>Price</label></td></tr><tr>'+
							'<td class="description"><label>'+tm.mealDescription+'</label></td>'+
							'<td class="qtyBody bdLeft"><label id="spin_'+p.catId+'_'+i+'" class="mealSpinner">0</label></td>'+
							'<td class="priceBody bdLeft"><label id="lblcurrency">'+mealPriceLabel+'</label>'+
							'<span class="imgpath" style="display:none">'+solveImageUrl(tm.mealImagePath)+'</span></td></tr></table></div>';
						}
					}else if (tm.mealCategoryID == p.catId){
						if (i%2 == 1)
							htmlSTR += '<div class="mealItem" id="'+p.segId+'_'+p.paxId+'_'+p.catId+'_'+i+'">';
						else
							htmlSTR += '<div class="mealItem" id="'+p.segId+'_'+p.paxId+'_'+p.catId+'_'+i+'">';
						if (checkChoosedMeals(p.choosed,p.catId,i,'bool')){
							htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>'+
							'<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="'+solveImageUrl(tm.mealImagePath)+'" height="50"/></div></td>'+
							'<td class="name"><label  class="thumbImg">'+tm.mealName+'</label></td><td class="qtyHead bdLeft"><label>Qty</label></td><td class="priceHead bdLeft"><label>Price</label></td></tr><tr>'+
							'<td class="description"><label>'+tm.mealDescription+'</label></td>'+
							'<td class="qtyBody bdLeft"><label id="spin_'+p.catId+'_'+i+'" class="mealSpinner">'+checkChoosedMeals(p.choosed,p.catId,i,'count')+'x</label></td>'+
							'<td class="priceBody bdLeft"><label id="lblcurrency">'+mealPriceLabel+'</label>'+
							'<span class="imgpath" style="display:none">'+solveImageUrl(tm.mealImagePath)+'</span></td></tr></table></div>';
						}else{
							htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>'+
							'<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="'+solveImageUrl(tm.mealImagePath)+'" height="50"/></div></td>'+
							'<td class="name"><label class="thumbImg">'+tm.mealName+'</label></td><td class="qtyHead bdLeft"><label>Qty</label></td><td class="priceHead bdLeft"><label>Price</label></td></tr><tr>'+
							'<td class="description"><label>'+tm.mealDescription+'</label></td>'+
							'<td class="qtyBody bdLeft"><label id="spin_'+p.catId+'_'+i+'" class="mealSpinner">0</label></td>'+
							'<td class="priceBody bdLeft"><label id="lblcurrency">'+mealPriceLabel+'</label>'+
							'<span class="imgpath" style="display:none">'+solveImageUrl(tm.mealImagePath)+'</span></td></tr></table></div>';
						}
					}
				}
			}else{
				htmlSTR += '<div class="mealItemNone">Error occured while loading meal data</div>';
			}
			return htmlSTR;
		}else{
			if(intSegRow!=null){
				var tmpltSel = $('#paxMealTemplate select');
				for (var i = 0 ; i < UI_Anci.jsMealModel[intSegRow].meals.length; i++){
					var tm = UI_Anci.jsMealModel[intSegRow].meals[i];					
					tmpltSel.append('<option value="'+i+'">'+tm.mealName+'</option>');
				}
				for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i ++){
					var paxSel = $('#mealPax_MealList_'+i);
					paxSel.find('option').remove();
					paxSel.append('<option value="-1">&nbsp;</option>');
					tmpltSel.find('option').each(function(){
						var clone = $(this).clone();
						clone.appendTo(paxSel);
					});
					$('#mealPax_MealCharge_'+i).html("");
				}
				tmpltSel.find('option').remove();
			}else{
				jConfirm("Error occured while loading meal data", 'Ok', function(){});
			}
		}
	}
	
	function solveImageUrl(url){
		var newurl ="";
		if (window.location.protocol == "https:"){
			newurl = url.replace("http","https");
		}else{
			newurl = url;
		}
		
		return newurl;
	}
	
	UI_Anci.isMultiMealEnabledForSegment = function(segId){
		var mealSegObj = UI_Anci.jsMealModel[UI_Anci.getMealSegIndex(segId)];
		return UI_Anci.multimealEnebled && mealSegObj.multiMealEnabledForLogicalCabinClass;
	}
	
	UI_Anci.mealCategoryClick = function(event,mySeg, myCat, chMeals){
		//update Hidden data
		var selMeals;
		if (event == null){
			var intSegRow = parseInt(mySeg, 10) ;
			$.data($("#newPopItem")[0], "mealdata").catId = myCat;
			$.data($("#newPopItem")[0], "mealdata").segId = intSegRow;
			$("#newPopItem").find(".headeritems").html(UI_Anci.buildMealsHeaders({
                segId:UI_Anci.getMealSegIndex( $.data($("#newPopItem")[0], "mealdata").segId),
                catId:myCat}));
			selMeals = chMeals;
		}else{
			$.data($("#newPopItem")[0], "mealdata").catId = this.id.split("_")[1];
			$("#newPopItem").find(".headeritems").html(UI_Anci.buildMealsHeaders({
				segId:UI_Anci.getMealSegIndex( $.data($("#newPopItem")[0], "mealdata").segId ),
				catId:this.id.split("_")[1]
			}));
			selMeals = $.data($("#newPopItem")[0], "mealdata").meals;
		}
		var passHTML =  "<input type='button' value='"+UI_Anci.jsonLabel["btnMealBack"]+"' class='Button ButtonDefaultDisable mealBack'/><input type='button' value='"+UI_Anci.jsonLabel["btnMealConfirm"]+"' class='Button mealConfirm'/>";
		$("#newPopItem").find("#footeritems").html(passHTML);
		$("#navMealCat").unbind("click").bind("click", UI_Anci.mealCategorynavClick);
		$("#newPopItem").find("#bodyitems").html(UI_Anci.buildMeals({
			catId:$.data($("#newPopItem")[0], "mealdata").catId,
			segId:$.data($("#newPopItem")[0], "mealdata").segId,
			paxId:$.data($("#newPopItem")[0], "mealdata").paxId,
			choosed:selMeals
		}));
		
		var isMultiMealEnabled = UI_Anci.isMultiMealEnabledForSegment($.data($("#newPopItem")[0], "mealdata").segId);
		
		$(".mealConfirm").unbind("click").bind("click",UI_Anci.closeMealPopUp);
		$(".mealBack").unbind("click").bind("click",UI_Anci.mealCategorynavClick);
		$(".mealSpinner").spinnerisa({
			select:isMultiMealEnabled,
            selectedMeal: chMeals,
			selectAction : function (event, val){
				var mealId = val.split("_")[2];
				var mealArrar = $.data($("#newPopItem")[0], "mealdata").meals;
				mealArrar[0] = ({mealCategoryID:"-",mealIndex:mealId,count:null});
			},
			increase : function (event,val) {
				//useing val.objid can extract the catgoryid and mealindex
				//useing val.value can extract the count of items
				var catId = val.objid.split("_")[1];
				var mealId = val.objid.split("_")[2];
				var mealArrar = $.data($("#newPopItem")[0], "mealdata").meals;
				var boolupdated = false;
				$.each(mealArrar, function(i,j){
					if (j.mealCategoryID == catId && j.mealIndex == mealId){
						j.count = val.value;
						boolupdated = true;
					}
				});
				if(!boolupdated){
						mealArrar.push({mealCategoryID:catId,mealIndex:mealId,count:val.value});
				}
				$.data($("#newPopItem")[0], "mealdata").meals = mealArrar;
			},
			decrease : function (event,val) {
				//useing val.objid can extract the catgoryid and mealindex
				//useing val.value can extract the count of items
				var catId = val.objid.split("_")[1];
				var mealId = val.objid.split("_")[2];
				var mealArrar = $.data($("#newPopItem")[0], "mealdata").meals;
				$.each(mealArrar, function(i,j){
					if (j.mealCategoryID == catId && j.mealIndex == mealId){
						j.count = val.value;
					}
				});
				$.each(mealArrar, function(i,j){
					if (j != undefined && j.count == 0){
						mealArrar.splice(i,1);
					}
				});
				$.data($("#newPopItem")[0], "mealdata").meals = mealArrar;
			}
		});
		$(".mealItem").unbind("mouseenter").bind("mouseenter", function(){
			$(this).addClass("mOverMeal");
		});
		$(".mealItem").unbind("mouseleave").bind("mouseleave", function(){
			$(this).removeClass("mOverMeal");
		});
		
	};
	UI_Anci.mealCategorynavClick = function(){
		$("#newPopItem").find(".headeritems").html(UI_Anci.buildMealsHeaders({catId:"-"}));
		$("#newPopItem").find("#bodyitems").html(UI_Anci.buildMealsCats({
			segId:$.data($("#newPopItem")[0], "mealdata").segId,
			paxId:$.data($("#newPopItem")[0], "mealdata").paxId
		}));
		var passHTML =  "<input type='button' value='"+UI_Anci.jsonLabel["btnMealBack"]+"' class='Button mealConfirm'/>";
		$("#newPopItem").find("#footeritems").html(passHTML);
		$(".mealCatClick").unbind("click").bind("click", UI_Anci.mealCategoryClick);
		$(".mealConfirm").unbind("click").bind("click",UI_Anci.closeMealPopUp);
		$(".mealBack").unbind("click").bind("click",UI_Anci.mealCategorynavClick);
		/*$(".mealCatClick").unbind("mouseenter").bind("mouseenter", function(){
			$(this).addClass("mealMouseOver");
		});
		$(".mealCatClick").unbind("mouseleave").bind("mouseleave", function(){
			$(this).removeClass("mealMouseOver");
		});*/
	};
	
	UI_Anci.closeMealPopUp = function(eve){
		if (eve.target.className != "close"){
			var mealSelected = $.data($("#newPopItem")[0], "mealdata").meals;
			var segId = $.data($("#newPopItem")[0], "mealdata").segId;
			var paxId = $.data($("#newPopItem")[0], "mealdata").paxId;
			var intSegRow = UI_Anci.getMealSegIndex(UI_Anci.meal.currentSegId);
			var str = '<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">';
			var isMultiMealEnabled = UI_Anci.isMultiMealEnabledForSegment(segId);
			
			if (mealSelected.length<=0){
				str += '<tr>'+
				'<td class="mealCol defaultRowGap rowColor "><a href="javascript:void(0)" class="selectAici selectMeal" id="meal_'+segId+'_'+paxId+'"><label id="lblSelectMeal">'+UI_Anci.jsonLabel["lblSelectMeal"]+'</label></a></td>'+
				'<td class="priceCol defaultRowGap rowColor " align="center"><label id="price_'+segId+'_'+paxId+'"> - </label></td>'+
				'<td class="clearCol defaultRowGap rowColor " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_'+segId+'_'+paxId+'" style="display:none" title="'+UI_Anci.jsonLabel["lblSeatClear"]+'"></a></td>'+
				'</tr>';
			}else{
				var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.MEALS, null, UI_Anci.getOndSequence(intSegRow));
				
				$.each(mealSelected, function(i,j){
					var selectedMealItem = UI_Anci.jsMealModel[intSegRow].meals[j.mealIndex];
					var catId = j.mealCategoryID;
					var qty = 1;
					if (isMultiMealEnabled){
						qty = j.count;
					} else if(catId == '-'){
						catId = selectedMealItem.mealCategoryID;
					}
					var mealCharge = UI_Anci.convertAmountToSelectedCurrency(selectedMealItem.mealCharge * qty);
					var applyFormatter = UI_Anci.isApplyDecorator(mealCharge,isFreeService);
            		if(applyFormatter){
            			mealCharge = UI_Anci.freeServiceDecorator;	
            		}		
					
					if (isMultiMealEnabled){
						str += '<tr><td class="mealCol defaultRowGap rowColor ">'+
								'<a id="meal_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="selectAici selectMeal" href="javascript:void(0)"><label id="lblSelectMeal">'+j.count+'x'+selectedMealItem.mealName+'</label></a>'+
								'</td><td align="center" class="priceCol defaultRowGap rowColor  "><label>'+mealCharge+'</label></td>'+
								'<td align="center" class="clearCol defaultRowGap rowColor  ">'+
								'<a title="Clear" style="display: block;" id="clear_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>'+
								'</td></tr>';
					}else{
						str += '<tr><td class="mealCol defaultRowGap rowColor ">'+
						'<a id="meal_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="selectAici selectMeal" href="javascript:void(0)"><label id="lblSelectMeal">'+selectedMealItem.mealName+'</label></a>'+
						'</td><td align="center" class="priceCol defaultRowGap rowColor  "><label>'+mealCharge+'</label></td>'+
						'<td align="center" class="clearCol defaultRowGap rowColor  ">'+
						'<a title="Clear" style="display: block;" id="clear_'+segId+'_'+paxId+'_'+catId+'_'+j.mealIndex+'" class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>'+
						'</td></tr>';
					}
				});
			}
			str += '</table>';
			$("#multimeal_"+segId+'_'+paxId).html(str);
			UI_Anci.mealPaxChanged({paxID:paxId,segID:segId,slMeals:mealSelected});
		}
		$("#newPopItem").closeMyPopUp();
	};
	
	
	/*-------------------------- MEAL EVENT BINDINGS -----------------------*/
	
	$(document).on("click",".selectMeal",function (){
		var tArr = this.id.split("_");
		UI_Anci.meal.currentSegId = tArr[1];
		seletedmealArray = [];
		$("#multimeal_"+tArr[1]+"_"+tArr[2]).find(".selectMeal").each(function(){
			if (this.id.split("_")[3] != undefined && this.id.split("_")[4] != undefined){
				var t = {
						mealCategoryID:this.id.split("_")[3],
						mealIndex:this.id.split("_")[4],
						count:parseInt($(this).find("label").html())
						}
				seletedmealArray.push(t);
			}
		});
		//set Hidden data
		var intSegRow = UI_Anci.getMealSegIndex(tArr[1]);
		$.data($("#newPopItem")[0], "mealdata" ,
				{	segId:tArr[1],
					paxId:tArr[2] ,
					catId:(tArr[3]== undefined) ? "-" :tArr[3],
					meals:seletedmealArray
				});
		if ($.data($("#newPopItem")[0], "mealdata").catId != "-"){
			var selItemCount = parseInt($(this).text())
		}
		
		if(!UI_Anci.isMultiMealEnabledForSegment(tArr[1])){
			selItemCount = 1;
		}
		
		$("#newPopItem").openMyPopUp({
			width:645,
			height:465,
			topPoint: $(window).height() - (600),
			headerHTML:UI_Anci.buildMealsHeaders({catId:"-"}),
			bodyHTML: function(){
				var passHTML = "";
				if (!UI_Anci.mealCategoryEnebled){
					passHTML = UI_Anci.mealCategoryClick(null,tArr[1],"-",seletedmealArray);
				}else if (UI_Anci.mealCategoryEnebled && $.data($("#newPopItem")[0], "mealdata").catId != "-" && !isNaN(selItemCount)){
					passHTML = UI_Anci.mealCategoryClick(null,tArr[1],tArr[3],seletedmealArray);
				}else{
					passHTML = UI_Anci.buildMealsCats({segId:tArr[1],paxId:tArr[2]});
				}
				return passHTML;
			},
			footerHTML: function(){
				var passHTML = "";
				if ($.data($("#newPopItem")[0], "mealdata").catId != "-" && !isNaN(selItemCount)){
					passHTML =  "<input type='button' value='"+UI_Anci.jsonLabel["btnMealBack"]+"' class='Button ButtonDefaultDisable mealBack'/>"+
					"<input type='button' value='"+UI_Anci.jsonLabel["btnMealConfirm"]+"' class='Button mealConfirm'/>";
				}else{
					passHTML = "<input type='button' value='"+UI_Anci.jsonLabel["btnMealConfirm"]+"' class='Button mealConfirm'/>";
				}
				return passHTML;
			}
		});
		$(".mealCatClick").unbind("click").bind("click", UI_Anci.mealCategoryClick);
		$(".mealConfirm").unbind("click").bind("click",UI_Anci.closeMealPopUp);
		$(".mealBack").unbind("click").bind("click",UI_Anci.mealCategorynavClick);
		$(".close").unbind("click").bind("click",UI_Anci.closeMealPopUp);
		
		
	});
	
	UI_Anci.getMealCatByCode = function(seg,code){
		var t = null;
		$.each(UI_Anci.jsMealCategoryModel[seg], function(){
			if (this.catCode == code){
				t =	this.catName;
			}
		});
		return t;
	}
	
	
	UI_Anci.buildMealsHeaders = function(p){
		var str = '<table border="0" cellspacing="0" cellpadding="0"><tr>';
		if (p.catId == "-"){
			str += '<td class="redBar"><label>'+UI_Anci.jsonLabel["lblMealMenuHD"]+'</label></td><td class="redCorner"></td>';
		}else{
			str += '<td class="grayBar"><label id="navMealCat">'+UI_Anci.jsonLabel["lblMealMenuHD"]+'</label></td><td class="grayCorner"></td>'
			str += '<td class="redBar"><label>'+ UI_Anci.getMealCatByCode(p.segId, p.catId) +'</label></td><td class="redCorner"></td>';
		}
		str +='</tr></table>';
		return str;
	};
	
	UI_Anci.buildMealsCats = function(p){
		var intSegRow = UI_Anci.getMealSegIndex(p.segId);
		if (UI_Anci.mealViewType == "New-view"){
			var htmlSTR = '<div id="pan_'+p.segId+"_"+p.paxId+'" class="mealCatDiv">';
			if(intSegRow!=null){
				htmlSTR += '<div class="catBlock firstCat"></div>';
				$.each(UI_Anci.jsMealCategoryModel[intSegRow],function(i,j){
					htmlSTR += '<a href="javascript:void(0)" class="mealCatClick calClass'+j.catCode+'" id="cat_'+j.catCode+'">'+
					'<div class="catBlock">&nbsp;</div></a>';
				});
				htmlSTR += '</div>'
			}else{
				htmlSTR += '<div class="mealItemNone">Error occured while loading meal data</div>';
			}
			return htmlSTR;
		}
	}
	
	$(document).on("click",".clearMeal", function (){
		var tArr = this.id.split("_");
		UI_Anci.meal.currentSegId = tArr[1];
		var qtyStr = $("#meal_"+tArr[1]+"_"+tArr[2]+"_"+tArr[3]+"_"+tArr[4]).find("label").text();
		var qty = 1;
		if(qtyStr != undefined && qtyStr != null && qtyStr != ''){
			qty = parseInt(qtyStr);
		}
		UI_Anci.paxMealRemove(tArr[2],qty,tArr[4],tArr[3]);// qty has to pass
		$("#meal_"+tArr[1]+"_"+tArr[2]+"_"+tArr[3]+"_"+tArr[4]).parent().parent().remove();
		var innerHtml = "";
		if ($("#multimeal_"+tArr[1]+"_"+tArr[2]).find(".selectMeal").length == 0){
			innerHtml = '<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%"><tr>'+
			'<td class="mealCol defaultRowGap rowColor "><a href="javascript:void(0)" class="selectAici selectMeal" id="meal_'+tArr[1]+'_'+tArr[2]+'"><label id="lblSelectMeal">'+UI_Anci.jsonLabel["lblSelectMeal"]+'</label></a></td>'+
			'<td class="priceCol defaultRowGap rowColor " align="center"><label id="price_'+tArr[1]+'_'+tArr[2]+'"> - </label></td>'+
			'<td class="clearCol defaultRowGap rowColor " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_'+tArr[1]+'_'+tArr[2]+'" style="display:none" title="'+UI_Anci.jsonLabel["lblSeatClear"]+'"></a></td>'+
			'</tr></table>';
			$("#multimeal_"+tArr[1]+"_"+tArr[2]).html(innerHtml)
		}
		UI_Anci.updateFlightAnciView();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
		//$("#meal_"+tArr[1]+"_"+tArr[2]).children("label").html(UI_Anci.jsonLabel["lblSelectMeal"]);
		//$("#price_"+tArr[1]+"_"+tArr[2]).html("-");

	});
	
	
	/*$(document).on("click",".closethis", function (){
		colsePopup();
	});*/
	
	$(document).on("mouseover",".thumbImg", function (e){
		var poss = getEventPosition(e);
		var descrip = $(this).parent().parent().parent().find(".description").html();
		var lengthofLine = 40; 
		var leftAdjsment = 125;
		var noOfLines = 3;
		if (descrip != null){
			noOfLines = parseInt( descrip.length / lengthofLine);
		}
		if ( noOfLines > 5 ){leftAdjsment = 275;}else{leftAdjsment = 125;}
		var adjsment = ( (poss.top) > parseInt($(window).scrollTop() + 300 ) ) ? 
				parseInt(poss.top) - (330 + (noOfLines * 6 )): poss.top - 100;
		$(".zoomer").css({
			"top":(adjsment),
			"width": 265,
			"left":(poss.left - leftAdjsment)
		});
		
		$(".zoomer").show();
		var tempImg = new Image();
		tempImg.src = $(this).parent().parent().parent().find(".imgpath").html();
		var ratio = 0;
		if ($.browser = "msie"){
			 ratio = tempImg.width / tempImg.height;
		}else{
			 ratio = tempImg.naturalWidth / tempImg.naturalHeight;
		}
		var setval = "";
			if (ratio >= 1){
				setval = "width='250' style='width:250px;'";
			}else{
				setval = "height='250' style='height:250px;'";
			}
			$(".imgDescription").html(descrip);
			$(".loadImage").html('<img src='+tempImg.src+' '+setval+' />');
	});
	
	
	$(document).on("mouseout",".thumbImg", function (){
		$(".loadImage").html('<img src="../images/Loading_no_cache.gif"/>');
		$(".imgDescription").html("");
		$(".zoomer").hide()
	});
	/*----------------------- END OF MEAL EVENT BINDINGS -----------------------*/
	/*----------------------- MEAL PRIVATE FUNCTIONS ---------------------------*/
	function getEventPosition (e,obj){
		var pos = {top:e.pageY,left:e.pageX}
		return pos;
	} 
	
	/*function colsePopup(){
		UI_commonSystem.readOnly(false);
		$(".mealbody").html('<img class="loadingImg" src="../images/Loading_no_cache.gif"/>');
		$(".zoomer").hide()
		$(".mealloader").hide();
	}*/
	
	function fixNotLoaded(){
		$(".thumbImg").each(function(){
			if((typeof this.naturalWidth != "undefined" &&
			        this.naturalWidth == 0 ) 
			        || this.readyState == 'uninitialized' ) {
				// image was broken, replace with your new image
					this.src = "./../images/noImage_no_cache.jpg";
				}
		})
	};
	
	_resetPaxMealSelection = function(segIndex, paxIndex){
		var innerHtml = '<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%"><tr>'+
		'<td class="mealCol defaultRowGap rowColor "><a href="javascript:void(0)" class="selectAici selectMeal" id="meal_'+segIndex+'_'+paxIndex+'"><label id="lblSelectMeal">'+UI_Anci.jsonLabel["lblSelectMeal"]+'</label></a></td>'+
		'<td class="priceCol defaultRowGap rowColor " align="center"><label id="price_'+segIndex+'_'+paxIndex+'"> - </label></td>'+
		'<td class="clearCol defaultRowGap rowColor " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_'+segIndex+'_'+paxIndex+'" style="display:none" title="'+UI_Anci.jsonLabel["lblSeatClear"]+'"></a></td>'+
		'</tr></table>';
		
		$("#multimeal_"+segIndex+"_"+paxIndex).html(innerHtml);
	}
	
	/*----------------------- END OF MEAL PRIVATE FUNCTIONS ---------------------------*/
	
	/*
	 * Handled the event of passenger changing meal
	 * Remove old one validate and add new meal
	 */
	UI_Anci.mealPaxChanged = function(evt){
		if (UI_Anci.mealViewType == "New-view"){
			//var arr = evt.split('_');
			var paxId = evt.paxID;
			UI_Anci.paxMealRemove(paxId, null, null, null);
			var isMultiMealEnabled = UI_Anci.isMultiMealEnabledForSegment(UI_Anci.meal.currentSegId);
			$.each(evt.slMeals, function(){
				var oldMeal = [-1];
				if(UI_Anci.paxWiseAnci!=null && UI_Anci.paxWiseAnci[UI_Anci.meal.currentPaxId].selectedAncillaries[UI_Anci.meal.currentSegId] != undefined){
                  oldMeal =   UI_Anci.paxWiseAnci[UI_Anci.meal.currentPaxId].selectedAncillaries[UI_Anci.meal.currentSegId].mealDTOs;
				}
					
				var selVal = this.mealIndex;
                var selCatID = this.mealCategoryID;
				var count = 1;
                var sameSelect = 0;
				if (isMultiMealEnabled){
					count = this.count;
				}
                if(oldMeal[0]!=-1){
                    for(var i = 0; i <  oldMeal.length; i++){
					    if( oldMeal[i].mealCategoryID==selCatID){
                              sameSelect = -1;
                                                
                        }
                    }
                }
				if(selVal>=0){			
					if(UI_Anci.mealAvailable(selVal,count)){
						UI_Anci.paxMealAdd(paxId,selVal,count, UI_Anci.meal.currentSegId);	
					}else if(sameSelect != -1){
						$('#'+id).val('-1');
						_resetPaxMealSelection(UI_Anci.meal.currentSegId, paxId);
                        jConfirm("Selected meal is not available", 'Ok', function(){});
                   }
				}
				
			});
		}else{
			var id = evt.target.id
			var arr = id.split('_');
			var paxId = parseInt(arr[2],10);
			var selVal = parseInt($('#'+id).val(),10);
			UI_Anci.paxMealRemove(paxId, null, null, null);
			if(selVal>=0){			
				if(UI_Anci.mealAvailable(selVal,1)){
					UI_Anci.paxMealAdd(paxId,selVal,1, UI_Anci.meal.currentSegId);	
				}else{
					$('#'+id).val('-1');
					jConfirm("Selected meal is not available", 'Ok', function(){});
				}
			}
		}
	}
	
	/*
	 * Remove all meals from given pax
	 */
	UI_Anci.paxMealRemove = function(paxId,qty,mealInd,catid){
		var mealData = UI_Anci.jsAnciSegModel[UI_Anci.meal.currentSegId].pax[paxId].meal;
		if (qty == null && mealInd == null && catid == null){
			var amt =mealData.mealChargeTotal;
			UI_Anci.updateAnciTotals(UI_Anci.anciType.MEAL,amt,'DEDUCT');
			UI_Anci.updateAnciCounts(UI_Anci.anciType.MEAL,mealData.mealQtyTotal,'DEDUCT');
	
			$('#mealPax_MealCharge_'+paxId).html("");
			
			while(mealData.meals.length>0){
				var meal = mealData.meals.pop();
				UI_Anci.updateMealAvailCount(meal,meal.mealQty,'ADD', UI_Anci.meal.currentSegId);
			}
			
			mealData.meals = [];
			mealData.mealChargeTotal =0;
			mealData.mealQtyTotal = 0;
			UI_Anci.updateFlightAnciView();
			UI_Anci.updateJnTax();
			UI_Anci.updateAnciPenalty();
		}else{
			for (var i = 0; i< mealData.meals.length; i++){
				//validate catid also get the correct meal item
				if ( mealData.meals[i].mealIndex == mealInd && true){
					var meal = mealData.meals[i];
					var tempSbTot = meal.subTotal;
					var tempQty = meal.mealQty;
					
					UI_Anci.updateMealAvailCount(meal,tempQty,'ADD', UI_Anci.meal.currentSegId);
					mealData.meals.splice(i,1);
					UI_Anci.updateAnciTotals(UI_Anci.anciType.MEAL,tempSbTot,'DEDUCT');
					UI_Anci.updateAnciCounts(UI_Anci.anciType.MEAL,tempQty,'DEDUCT');
					mealData.mealChargeTotal -= tempSbTot;
					mealData.mealQtyTotal -= tempQty;
					
				}
			}
		}		
	}
	
	UI_Anci.paxBaggageRemoveWrapper = function(segId,paxId){	
		UI_Anci.paxBaggageRemove(segId, paxId, true);

		if(UI_Anci.jsBaggageModel[segId].flightSegmentTO.baggageONDGroupId == null || UI_Anci.getBaggageONDGrpIdWiseCount(UI_Anci.jsBaggageModel[segId].flightSegmentTO.baggageONDGroupId) == 1){
			var isConnectionFare = (UI_Container.fareQuote.fareType == '2');				
			if(isConnectionFare){
				var isReturn = UI_Anci.jsBaggageFlightSegmentModel[segId].returnFlag;
				for (var k = 0 ;  k < UI_Anci.jsBaggageFlightSegmentModel.length; k++){					
					if(UI_Anci.jsBaggageModel[k].baggages.length > 0 && k != segId && isReturn == UI_Anci.jsBaggageFlightSegmentModel[k].returnFlag){						
						UI_Anci.paxBaggageRemove(k, paxId, true);					
					}
				}
			}
		}
	}

	/*
	 * Remove all baggages from given pax
	 */
	UI_Anci.paxBaggageRemove = function(segId, paxId, reduceTotals){		
		var baggageData = UI_Anci.jsAnciSegModel[segId].pax[paxId].baggage;
		var amt = baggageData.baggageChargeTotal;
		
		if (reduceTotals) {
			UI_Anci.updateAnciTotals(UI_Anci.anciType.BAGGAGE,amt,'DEDUCT');
		}
		
		UI_Anci.updateAnciCounts(UI_Anci.anciType.BAGGAGE,baggageData.baggageQtyTotal,'DEDUCT');

		$('#baggagePax_Charge_'+segId + '_'+ paxId).html("-");
		$('#baggagePax_Clear_'+segId + '_'+ paxId).html("");
		$('#selBaggageType_'+segId + '_'+ paxId).val('-');
		
		while(baggageData.baggages.length>0){
			var baggage = baggageData.baggages.pop();
			//UI_Anci.updateMealAvailCount(meal,meal.mealQty,'ADD', UI_Anci.meal.currentSegId);
		}
		
		baggageData.baggages = [];
		baggageData.baggageChargeTotal =0;
		baggageData.baggageQtyTotal = 0;
		

		UI_Anci.updateFlightAnciView();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
	}
	
	/*
	 * Add given meal for the segment to the pax
	 */
	UI_Anci.paxMealAdd = function(paxId,mealIndex,qty, segId){
		var intSegRow = UI_Anci.getMealSegIndex(segId);
		
		var meal = UI_Anci.jsMealModel[intSegRow].meals[mealIndex];
		
		var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.MEALS, meal.mealCharge, UI_Anci.getOndSequence(segId));
		
		if(isFreeService){
			meal.mealCharge = 0;
		}

		var mealData = UI_Anci.jsAnciSegModel[segId].pax[paxId].meal;
		var paxMeals = mealData.meals;
		var mealId = null;
		var qty = qty;
		var amt = parseFloat(meal.mealCharge) * qty;
		for(var i = 0  ; i < paxMeals.length; i ++){
			if(paxMeals[i].mealCode == meal.mealCode){
				return;
			}
		}
		mealId=paxMeals.length;
		paxMeals[mealId] = {mealCode:meal.mealCode,
				mealName:meal.mealName,
				mealQty:qty,
				mealCharge:meal.mealCharge,
				subTotal:amt,
				mealIndex:mealIndex
				};
		
		mealData.mealChargeTotal += amt;
		mealData.mealQtyTotal += qty;
		
		UI_Anci.updateMealAvailCount(paxMeals[mealId],qty,'DEDUCT', segId);
		UI_Anci.updateAnciTotals(UI_Anci.anciType.MEAL,amt,'ADD');
		UI_Anci.updateAnciCounts(UI_Anci.anciType.MEAL,qty,'ADD');
		UI_Anci.updateFlightAnciView();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
		//$('#mealPax_MealCharge_'+paxId).html($.airutil.format.currency(meal.mealCharge));
	}
	
	/* 
	 * Meal available 
	 */
	UI_Anci.mealAvailable = function(mealIndex, qty){
		var intSegRow = UI_Anci.getMealSegIndex(UI_Anci.meal.currentSegId);
		
		if(UI_Anci.jsMealModel[intSegRow].meals[mealIndex].availableMeals>=qty){
			return true;
		}else{
			return false;
		}
	}
	/* 
	 * Baggage available 
	 */
	UI_Anci.baggageAvailable = function(mealIndex, qty){
		return true;
	}
	
	/*
	 * Update meal available count
	 */
	UI_Anci.updateMealAvailCount = function(meal, qty, op, segId){
		var intSegRow = UI_Anci.getMealSegIndex(segId);
		var mmMeal = UI_Anci.jsMealModel[intSegRow].meals[meal.mealIndex];
		mmMeal.availableMeals = parseInt(mmMeal.availableMeals,10);
		if(op == 'ADD'){
			mmMeal.availableMeals += qty;
		}else if (op== 'DEDUCT'){
			mmMeal.availableMeals -= qty;
		}		
	}
	
	/*
	 * Given the meal code and segment id return the 
	 * meal id for the mealModel
	 */
	UI_Anci.getMealOptionId = function(segId,mealCode){
		var meals= UI_Anci.jsMealModel[segId].meals;
		for(var i = 0 ; i < meals.length; i++){
			if(meals[i].mealCode == mealCode){
				return i;
			}
		}
		return -1;
	}
	
	/*
	 * Given the baggage name and segment id return the 
	 * baggage id for the baggageModel
	 */
	UI_Anci.getBaggageOptionId = function(segId,baggageName){
		var baggages= UI_Anci.jsBaggageModel[segId].baggages;
		for(var i = 0 ; i < baggages.length; i++){
			if(baggages[i].baggageName == baggageName){
				return i;
			}
		}
		
		// set the default baggage from model
		for(var i = 0 ; i < baggages.length; i++){
			if(baggages[i].defaultBaggage == "Y"){
				return i;
			}
		}
		return -1;
	}
	/*
	 * Populate Meal Data
	 */
	UI_Anci.populateMealData = function (p){
		var mealData = null;
		var paxId = null;
		
		var paxData = UI_Anci.jsAnciSegModel[parseInt(p.segId,10)].pax;
		for(var i =0 ; i < paxData.length ; i++){
			mealData = paxData[i].meal;
			if(mealData.meals.length == 1){
				var meal = mealData.meals[0];
				$("#mealPax_MealList_" + i ).enable();
				$('#mealPax_MealList_'+i).val(meal.mealIndex);
				$('#mealPax_MealCharge_'+i).html($.airutil.format.currency(meal.mealCharge));
				
			}			
		}
		if(UI_Container.isModifyAncillary()){
			for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
				var curMeals = UI_Anci.getDisplayMeals();
				for(var j=0; j< curMeals.length; j++){
					$("#mealPax_MealList_" + i ).enable();
				}
				var meals = UI_Anci.jsonPaxAdults[i].currentAncillaries[p.segId].mealDTOs;
				for(var j=0; j< meals.length; j++){
					var mealId = UI_Anci.getMealOptionId(p.segId,meals[j].mealCode);
					$("#mealPax_MealList_" + i ).val(mealId);
					$("#mealPax_MealList_" + i ).disable();
					$('#mealPax_MealCharge_'+i).html('');
				}
			}			
		}
		
		if(UI_Container.isModifySegment() && !UI_Anci.reprotectLoaded.meal ){			
			for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
				var currAnci = UI_Anci.jsonPaxAdults[i].currentAncillaries;
				
				if(currAnci[p.segId]== null || UI_Container.isOneOfModifyingSeg(currAnci[p.segId].flightSegmentTO.flightRefNumber)){					
					continue;
				}
				var meals = currAnci[p.segId].mealDTOs;
				for(var j=0; j< meals.length; j++){					
					var mealId = UI_Anci.getMealOptionId(p.segId,meals[j].mealCode);
					if(mealId == -1){
						UI_Anci.notAllAnciReprotect = true;
						continue;
					}
					UI_Anci.paxMealAdd(i, mealId,1, p.segId);
					$("#mealPax_MealList_" + i ).val(mealId);
					$('#mealPax_MealCharge_'+i).html($.airutil.format.currency(meals[j].mealCharge));
				}
			}	
			UI_Anci.reprotectLoaded.meal = true;
		}
	}
	/*
	 * Return meal list for sky cafe
	 */
	UI_Anci.getDisplayMeals = function(){
		var intSegRow = UI_Anci.getMealSegIndex(UI_Anci.meal.currentSegId);
		if(UI_Anci.jsMealModel[intSegRow]!=null){
			return UI_Anci.jsMealModel[intSegRow].meals;
		}else{
			return [];
		}
	}
	
	/*
	 * Return baggage
	 */
	UI_Anci.getDisplayBaggages = function(){
		var intSegRow = UI_Anci.getBaggageSegIndex(UI_Anci.baggage.currentSegId);
		if(UI_Anci.jsBaggageModel[intSegRow]!=null){
			return UI_Anci.jsBaggageModel[intSegRow].baggages;
		}else{
			return [];
		}
	}
	//------------------------------- BAGGAGE ---------------------------
	
	
	UI_Anci.getMatchingIOSegIds = function(currentSegId) {
	
		var segElements = [];
		var currentSegBaggageId = UI_Anci.getBaggageSegIndex(currentSegId);
		var baggageONDGroupId = UI_Anci.jsBaggageModel[currentSegBaggageId].flightSegmentTO.baggageONDGroupId;
		if(baggageONDGroupId != null){
			for (var i= 0; i < UI_Anci.jsBaggageModel.length; i++) {
				if(baggageONDGroupId == UI_Anci.jsBaggageModel[i].flightSegmentTO.baggageONDGroupId){
					segElements[segElements.length] = UI_Anci.getSegIndexForBaggages(UI_Anci.jsBaggageModel[i].flightSegmentTO.flightRefNumber);
				}
			}
		}
		return segElements;	

	}

	/*
	 * Add given baggage for the segment to the pax
	 */
	UI_Anci.paxBaggageAdd = function(paxId,baggageIndex, segId, isLoading){
		
		var correctBaggageSegId = UI_Anci.getBaggageSegIndex(segId);
		
		if(UI_Anci.jsBaggageModel[correctBaggageSegId].flightSegmentTO.baggageONDGroupId == null ||
                UI_Anci.getBaggageONDGrpIdWiseCount(UI_Anci.jsBaggageModel[correctBaggageSegId].flightSegmentTO.baggageONDGroupId) == 1){
			UI_Anci.paxBaggageAddProcess(paxId, baggageIndex, segId, true);
		}else if (isLoading) {
			var elements = UI_Anci.getMatchingIOSegIds(segId);
			if (elements != null) {
				if (elements[0] == segId) {
					UI_Anci.paxBaggageAddProcess(paxId, baggageIndex, segId, true);
				} else {
					UI_Anci.paxBaggageAddProcess(paxId, baggageIndex, segId, false);
				}
			}
		} else {
			var elements = UI_Anci.getMatchingIOSegIds(segId)
			if (elements != null) {
				for (var i = 0; i < elements.length; i++) {
					var element = elements[i];
					if (element == segId) {
						UI_Anci.paxBaggageAddProcess(paxId, baggageIndex, element, true);
					} else {
						UI_Anci.paxBaggageAddProcess(paxId, baggageIndex, element, false);
					}
				}
			}
		}
	}
	UI_Anci.paxBaggageAddProcess = function(paxId,baggageIndex, segId, addAmounts){
		
		var intSegRow = UI_Anci.getBaggageSegIndex(segId);
		var baggage = UI_Anci.jsBaggageModel[intSegRow].baggages[baggageIndex];
		var baggageData = UI_Anci.jsAnciSegModel[segId].pax[paxId].baggage;
		var paxBaggages = baggageData.baggages;
		var baggageId = null;
		var qty = 1;
		var amt = parseFloat(baggage.baggageCharge) * qty;
		for(var i = 0  ; i < paxBaggages.length; i ++){
			if(paxBaggages[i].baggageCode == baggage.baggageCode){
				return;
			}
		}
		baggageId=paxBaggages.length;
		paxBaggages[baggageId] = {
				//For the moment baggage code is not using.
				//baggageCode:baggage.baggageCode,
				baggageCode:"",
				baggageName:baggage.baggageName,
				baggageQty:qty,
				baggageCharge:baggage.baggageCharge,
				subTotal:amt,
				baggageIndex:baggageIndex,
				ondBaggageChargeId:baggage.ondBaggageChargeId,
				ondBaggageGroupId:baggage.ondBaggageGroupId
				};
		
		baggageData.baggageChargeTotal += amt;
		baggageData.baggageQtyTotal += qty;
		
		//UI_Anci.updateBaggageAvailCount(paxMeals[mealId],qty,'DEDUCT', segId);
		if (addAmounts) {
			UI_Anci.updateAnciTotals(UI_Anci.anciType.BAGGAGE,amt,'ADD');
		}
		UI_Anci.updateAnciCounts(UI_Anci.anciType.BAGGAGE,qty,'ADD');
		UI_Anci.updateFlightAnciView();
		
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
		
		$('#selBaggageType_'+segId + '_'+ paxId).val(baggageIndex);
		var convertedAmount=UI_Anci.convertAmountToSelectedCurrency(baggage.baggageCharge);
		 
		var applyFormatter = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.BAGGAGES,convertedAmount, UI_Anci.getOndSequence(segId));
		convertedAmount = $.airutil.format.currency(convertedAmount);
		if(applyFormatter){
			convertedAmount = UI_Anci.freeServiceDecorator;	
		}
		$('#baggagePax_Charge_'+ segId + '_' + paxId).html(convertedAmount);
		//var clearHTML = "<a href='#'  class='noTextDec clearLabel' onclick='UI_Anci.paxBaggageRemoveWrapper(" +  segId +","+paxId +  ");  return false;'>&nbsp;</a>";
		//$('#baggagePax_Clear_'+ segId + '_' + paxId).html(clearHTML);
	}

	UI_Anci.baggagePaxChanged = function(evt){
		
		if (UI_Anci.mealViewType == "New-view"){
			
			var id = evt.target.id;
			
			var arr = id.split('_');
			var paxId = parseInt(arr[2],10);
			UI_Anci.baggage.currentSegId = parseInt(arr[1],10);
			var selVal = evt.target.value;

			var correctBaggageSegId = UI_Anci.getBaggageSegIndex(UI_Anci.baggage.currentSegId);
			
			if(UI_Anci.jsBaggageModel[correctBaggageSegId].flightSegmentTO.baggageONDGroupId != null){
				var elements = UI_Anci.getMatchingIOSegIds(UI_Anci.baggage.currentSegId);
				if (elements != null) {
					var firstSegment = elements[0];
					for (var i= 0; i < elements.length; i++) {
						var element = elements[i];
						if (element == firstSegment) {
							UI_Anci.paxBaggageRemove(element, paxId, true);
						} else {
							UI_Anci.paxBaggageRemove(element, paxId, false);
						}
					}
				}
			} else {
				UI_Anci.paxBaggageRemove(UI_Anci.baggage.currentSegId, paxId, true);
			}
			
			if(selVal>=0){			
				if(UI_Anci.baggageAvailable(selVal,1)){
					UI_Anci.paxBaggageAdd(paxId,selVal, UI_Anci.baggage.currentSegId, false);	
				}else{
					$('#'+id).val('-1');
					jConfirm("Selected meal is not available", 'Ok', function(){});
				}
			}

			
			if(UI_Anci.jsBaggageModel[correctBaggageSegId].flightSegmentTO.baggageONDGroupId == null){
				var isConnectionFare = (UI_Container.fareQuote.fareType == '2');				
				if(isConnectionFare){
					var isReturn = UI_Anci.jsBaggageFlightSegmentModel[UI_Anci.baggage.currentSegId].returnFlag;
					for (var k = 0 ;  k < UI_Anci.jsBaggageFlightSegmentModel.length; k++){					
						if(UI_Anci.jsBaggageModel[k].baggages.length > 0 && k != UI_Anci.baggage.currentSegId && isReturn == UI_Anci.jsBaggageFlightSegmentModel[k].returnFlag && UI_Anci.jsBaggageModel[k].flightSegmentTO.baggageONDGroupId == null ){						
							UI_Anci.paxBaggageRemove(k, paxId, true);
							UI_Anci.paxBaggageAdd(paxId,selVal, k, false);
						}
					}
				}
			}

		}else{
			var id = evt.target.id
			var arr = id.split('_');
			var paxId = parseInt(arr[2],10);
			UI_Anci.baggage.currentSegId = parseInt(arr[1],10);
			var selVal = parseInt($('#'+id).val(),10);
			UI_Anci.paxBaggageRemove(UI_Anci.baggage.currentSegId, paxId, true);
			if(selVal>=0){			
				if(UI_Anci.baggageAvailable(selVal,1)){
					UI_Anci.paxBaggageAdd(paxId,selVal, UI_Anci.baggage.currentSegId, false);	
				}else{
					$('#'+id).val('-1');
					jConfirm("Selected baggage is not available", 'Ok', function(){});
				}
			}
		}
		if (UI_Anci.baggageViewType == "New-view"){
			UI_Anci.segmentPaxBaggageUsedArray[UI_Anci.currentBaggageSegPax].baggageUsed = true;
			if ($("input[name='radBaggagePax']").length - 1 > UI_Anci.currentBaggageSegPax)
				UI_Anci.currentBaggageSegPax++;
			else
				UI_Anci.currentBaggageSegPax=0;
			
			UI_Anci.trigSegBaggagePax();
		}

	}
	
	//------------------------------- SSR ------------------------------------------------------
	
	/*
	 * SSR On Click
	 */
	UI_Anci.tabClickSSR = function(){
		// 0 - tdSSRSeg;
		// 1 - Seg Row;
		
		var arrData = this.id.split("_");
		var intSegRow = arrData[1];
		UI_Anci.clickSSRSegment({ind:intSegRow});
	}
	
	/*
	 * Click SSR segment
	 */
	UI_Anci.clickSSRSegment = function(p){
		UI_commonSystem.pageOnChange();
		UI_Anci.ssr.currentSegId = p.ind;
		
		if (UI_Anci.tabSltdSSR != null){
			$("#tdSSRSeg_" + UI_Anci.tabSltdSSR).removeClass("tabSltd");
			$("#tdSSRSeg_" + UI_Anci.tabSltdSSR).addClass("tabNotSltd");
			
			$("#lbl_tdSSRSeg_" + UI_Anci.tabSltdSSR).removeClass("gridHDFont fntBold");
			$("#lbl_tdSSRSeg_" + UI_Anci.tabSltdSSR).addClass("fntDefault");
		}
		
		UI_Anci.tabSltdSSR = p.ind;
		$("#tdSSRSeg_" + UI_Anci.tabSltdSSR).removeClass("tabNotSltd");
		$("#tdSSRSeg_" + UI_Anci.tabSltdSSR).addClass("tabSltd");
		
		$("#lbl_tdSSRSeg_" + UI_Anci.tabSltdSSR).removeClass("fntDefault");
		$("#lbl_tdSSRSeg_" + UI_Anci.tabSltdSSR).addClass("gridHDFont fntBold");
		
		UI_Anci.buildSSRs({segId:p.ind});
		UI_Anci.populateSSRData({segId:p.ind});
	}
	
	
	/* 
	 * Get index of the ssr model segment
	 */
	UI_Anci.getSSRSegIndex = function(segId){
		var intSegRow = null;
		var fltRefNo = UI_Anci.getFltRefNoForIndex(segId);
		for(var i=0; i < UI_Anci.jsSSRModel.length; i++){
			if(compareFltRefNos(fltRefNo,UI_Anci.jsSSRModel[i].flightSegmentTO.flightRefNumber)){
				intSegRow = i; break;
			}
		}
		return intSegRow;
	}
	
	/*
	 * Build meals
	 */
	UI_Anci.buildSSRs = function(p){	
		var intSegRow = UI_Anci.getSSRSegIndex(p.segId);
		//IE6 Fixed 
		loadagain = function(){
			paxSel.find("option[value='-1']").attr("selected", "selected");
		}
		if(intSegRow!=null){
			var tmpltSel = $('#paxSSRTemplate select');
			for (var i = 0 ; i < UI_Anci.jsSSRModel[intSegRow].specialServiceRequest.length; i++){
				var ssr = UI_Anci.jsSSRModel[intSegRow].specialServiceRequest[i];				
				tmpltSel.append('<option value="'+i+'">'+ssr.description+'</option>');
			}
			for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i ++){
				var paxSel = $('#ssrPax_SSRList_'+i);
				paxSel.find('option').remove();
				paxSel.append('<option value="-1" >--select--</option>');
				tmpltSel.find('option').each(function(){
					var clone = $(this).clone();
					clone.appendTo(paxSel);
				});
				try{
					paxSel.find("option[value='-1']").attr("selected", "selected");
				}catch(e){
					setTimeout('loadagain()', 500);
				}
			}
			tmpltSel.find('option').remove();
		}else{
			jAlert("Error occured while loading SSR data");
		}
	}
	 
	
	/*
	 * Populate SSR Data
	 */
	UI_Anci.populateSSRData = function (p){
		var ssrData = null;
		var paxId = null;
		var paxData = {};
		
		if(UI_Anci.jsAnciSegModel[parseInt(p.segId,10)] != undefined){
			paxData = UI_Anci.jsAnciSegModel[parseInt(p.segId,10)].pax;
		}
		if(UI_Top.holder().GLOBALS != undefined) {
			UI_Anci.applyToAllSegments = UI_Top.holder().GLOBALS.FQTVApplicableForAllSegments;
		}
				
		var ssr = UI_Anci.jsSSRModel[p.segId].specialServiceRequest;
		
		for(var i =0 ; i < paxData.length ; i++){
			ssrData = paxData[i].ssr;
			if(ssrData.ssrs.length >= 1){
				
				var ssr = ssrData.ssrs[0];
				$('#ssrPax_SSRList_'+i).val(ssr.ssrIndex);
				$('#ssrPax_SSRText_'+i).val(ssr.text);
				
			}else{
				$('#ssrPax_SSRText_'+i).val('');
			}
		}
		
		if(UI_Container.isModifyAncillary()){
			for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
				var curSSRs = UI_Anci.getDisplaySSRs();
				for(var j=0; j< curSSRs.length; j++){
					$("#ssrPax_SSRList_" + i ).enable();
					$("#ssrPax_SSRText_" + i ).enable();
				}
				var ssrs = UI_Anci.jsonPaxAdults[i].currentAncillaries[p.segId].specialServiceRequestDTOs;
				for(var j=0; j< ssrs.length; j++){
					var ssrId = UI_Anci.getSSROptionId(p.segId,ssrs[j].ssrCode);
					$("#ssrPax_SSRList_" + i ).val(ssrId);					
					$('#ssrPax_SSRText_'+i).val(ssrs[j].text);
					$("#ssrPax_SSRList_" + i ).disable();
					$("#ssrPax_SSRText_" + i ).disable();
				}
			}			
		}
		
		if(UI_Container.isModifySegment() && !UI_Anci.reprotectLoaded.ssr){
			for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){		
				var currAnci = UI_Anci.jsonPaxAdults[i].currentAncillaries;
				
				if( currAnci[p.segId] != null && UI_Container.isOneOfModifyingSeg(currAnci[p.segId].flightSegmentTO.flightRefNumber)){					
					var ssrs = UI_Anci.getSelectedSSR(currAnci);
					if(ssrs != null){	
						for(var j=0; j< ssrs.length; j++){
							var ssrId = UI_Anci.getSSROptionId(p.segId,ssrs[j].ssrCode);
							if(ssrId == -1){
								UI_Anci.notAllAnciReprotect = true;
								continue;
							}
						
							var index = p.segId;
							if(UI_Anci.applyToAllSegments == true && ssrs[j].ssrCode == UI_Anci.globallyAppliedSsr){
								for(var k = 0;k < UI_Anci.jsAnciAvail.ssrs.length;k++){
									if(index != k && UI_Anci.getFQTVIndexForGivenSeg(k) != -1 && $("#ssrPax_SSRList_" + k).val() == undefined){
										UI_Anci.paxSSRRemove(i,k);
										UI_Anci.paxSSRAdd(i,ssrId, k,true);
										UI_Anci.paxSSRAddText(index, ssrs[j].text, i, k);
									}														
								}
							}
						
							UI_Anci.paxSSRAdd(i,ssrId, p.segId,UI_Anci.applyToAllSegments && ssrs[j].ssrCode == UI_Anci.globallyAppliedSsr);	
							UI_Anci.paxSSRAddText(index, ssrs[j].text, i, p.segId);
							$("#ssrPax_SSRList_" + i ).val(ssrId);					
							$('#ssrPax_SSRText_'+i).val(ssrs[j].text);
						}
					}
				}
			}			
			UI_Anci.reprotectLoaded.ssr = true;
		}
	}
	
	UI_Anci.getSelectedSSR = function(currAnci) {
		var arrival = UI_Container.getModifingFlightInfo()[0].arrival;
		var departure = UI_Container.getModifingFlightInfo()[0].departure;
		var segmentCode = departure + "/" + arrival;
		
		for(var i = 0;i < currAnci.length;i++){			
			if(currAnci[i].segCode == segmentCode){
				return currAnci[i].specialServiceRequestDTOs;
			}
		}
		return null;
	}
	
	UI_Anci.paxSSRAddText = function(index, ssrText, paxId, k) {
		var ssr = UI_Anci.jsAnciSegModel[parseInt(k,10)].pax[paxId].ssr.ssrs[0];						
		if(ssr != undefined && ssr.ssrCode == "FQTV"){
			UI_Anci.jsAnciSegModel[parseInt(k,10)].pax[paxId].ssr.ssrs[0].text = ssrText;
		}
	}
	
	UI_Anci.ssrTextChanged = function(evt){
		var id = evt.target.id
		var arr = id.split('_');
		var paxId = parseInt(arr[2],10);
		var ssrText = $('#ssrPax_SSRText_'+paxId).val();
		var index = UI_Anci.ssr.currentSegId;
		if(UI_Anci.isFQTVSelected[index][paxId]){			
			if(UI_Anci.applyToAllSegments == true){
				for(var i = 0;i < UI_Anci.jsAnciAvail.ssrs.length;i++){
					if(index != i){
						var ssr = UI_Anci.jsAnciSegModel[parseInt(i,10)].pax[paxId].ssr.ssrs[0];						
						if(ssr != undefined && ssr.ssrCode == "FQTV"){
							UI_Anci.jsAnciSegModel[parseInt(i,10)].pax[paxId].ssr.ssrs[0].text = ssrText;
						}						
					}
				}
			}
		} 
	}
	
	/*
	 * Pax SSR Changed
	 */
	UI_Anci.ssrPaxChanged = function(evt){
		var id = evt.target.id
		var arr = id.split('_');
		var paxId = parseInt(arr[2],10);
		var selVal = parseInt($('#'+id).val(),10);
		UI_Anci.paxSSRRemove(paxId, UI_Anci.ssr.currentSegId);
		if(selVal>=0){			
			if(UI_Anci.ssrAvailable(selVal,1)){
				UI_Anci.paxSSRAdd(paxId,selVal, UI_Anci.ssr.currentSegId,false);
				
				if(UI_Anci.isFQTVSelected[UI_Anci.ssr.currentSegId][paxId]){
					$("#ssrPax_SSRText_"+paxId).unbind("keyup").bind("keyup", function(e){
						var target = e.target;	
						var value = $.trim(target.value);
						if (!/^[a-zA-Z ]*$/.test(value)) {
							value  = value.replace(/[^a-zA-Z0-9 ]/g,"");
							target.value = value;
						}
					});
					var index = UI_Anci.ssr.currentSegId;
					if(UI_Anci.applyToAllSegments == true){
						for(var i = 0;i < UI_Anci.jsAnciAvail.ssrs.length;i++){
							if(index != i && UI_Anci.getFQTVIndexForGivenSeg(i) != -1){
								UI_Anci.paxSSRRemove(paxId,i);
								UI_Anci.paxSSRAdd(paxId,selVal, i,true);
							}
						}
					}
				} else {
					$("#ssrPax_SSRText_"+paxId).unbind("keyup").bind("keyup", function(e){
						var target = e.target;	
						var value = $.trim(target.value);
						if (!/^[a-zA-Z0-9 ]*$/.test(value)) {
							value  = value.replace(/[^a-zA-Z ]/g,"");
							target.value = value;
						}
					});
				}
				
			}else{
				$('#'+id).val('-1');
				jConfirm(UI_Anci.jsonLabel['lblSSRNotAvailable'], 'Ok', function(){});
			}
		}
	}
	
	/*
	 * Remove Pax SSR
	 */
	UI_Anci.paxSSRRemove = function(paxId,segId){		
		var ssrData = UI_Anci.jsAnciSegModel[segId].pax[paxId].ssr;
		var amt =ssrData.ssrChargeTotal;
		
		UI_Anci.updateAnciTotals(UI_Anci.anciType.SSR,amt,'DEDUCT');
		UI_Anci.updateAnciCounts(UI_Anci.anciType.SSR,ssrData.ssrQtyTotal,'DEDUCT');

		$('#ssrPax_SSRText_'+paxId).val("");
		
		while(ssrData.ssrs.length>0){
			var ssr = ssrData.ssrs.pop();
			UI_Anci.updateSSRAvailCount(ssr,1,'ADD',segId);
		}
		
		ssrData.ssrs = [];
		ssrData.ssrChargeTotal =0;
		ssrData.ssrQtyTotal = 0;
		UI_Anci.updateFlightAnciView();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
	}
	
	/*
	 * Get SSR id for the given ssrCode for the current segment
	 */
	UI_Anci.getSSROptionId = function(segId, ssrCode){
		var ssrs = UI_Anci.jsSSRModel[segId].specialServiceRequest;
		for(var i = 0 ; i < ssrs.length; i++){
			if(ssrs[i].ssrCode == ssrCode){
				return i;
			}
		}
		return -1;
	}
	
	/*
	 * Get SSR for displaying for the current active segment
	 */
	UI_Anci.getDisplaySSRs = function(){
		var ssrs = UI_Anci.jsSSRModel[UI_Anci.ssr.currentSegId].specialServiceRequest;
		if(ssrs!=null){
			return ssrs;
		}else{
			return [];
		}
	}
	
	UI_Anci.getFQTVIndexForGivenSeg = function(segId){
		var allSSR = {};
		if(UI_Anci.jsSSRModel[segId] != undefined){
			allSSR = UI_Anci.jsSSRModel[segId].specialServiceRequest;
		}		
		for(var j = 0; j < allSSR.length; j++){
			if(allSSR[j].ssrCode == "FQTV"){
				return j;
			}
		}
		return -1;
	}
	
	/*
	 * Add Pax SSR
	 */
	UI_Anci.paxSSRAdd = function(paxId,ssrIndex, segId, isFQTVSelected){
		var intSegRow = UI_Anci.getSSRSegIndex(segId);
		var fqtvIndex = -1;
		var ssr = {};
		
		if(isFQTVSelected){
			fqtvIndex = UI_Anci.getFQTVIndexForGivenSeg(segId);
			ssr = UI_Anci.jsSSRModel[intSegRow].specialServiceRequest[fqtvIndex];
		} else {
			ssr = UI_Anci.jsSSRModel[intSegRow].specialServiceRequest[ssrIndex];
		}
		
		if(ssr != undefined){
			var ssrData = UI_Anci.jsAnciSegModel[segId].pax[paxId].ssr;
			var paxSSRs = ssrData.ssrs;
			var ssrId = null;
			var qty = 1;
			var amt = parseFloat(ssr.charge) * qty;
			for(var i = 0 ; i < paxSSRs.length; i++){
				if(paxSSRs[i].ssrCode == ssr.ssrCode){
					return;
				}
			}
			ssrId=paxSSRs.length;
			if(UI_Anci.isFQTVSelected[segId] == undefined){
				UI_Anci.isFQTVSelected[segId] = {};
			}
			
			if(isFQTVSelected){
				paxSSRs[ssrId] = {ssrCode:ssr.ssrCode,
						description:ssr.description,
						charge:ssr.charge,
						text:'',
						ssrIndex:fqtvIndex};
				UI_Anci.isFQTVSelected[segId][paxId] = true;
			} else {
				paxSSRs[ssrId] = {ssrCode:ssr.ssrCode,
						description:ssr.description,
						charge:ssr.charge,
						text:'',
						ssrIndex:ssrIndex};
				
				if(paxSSRs[ssrId].ssrCode == 'FQTV'){					
					UI_Anci.isFQTVSelected[segId][paxId] = true;
				} else {
					UI_Anci.isFQTVSelected[segId][paxId] = false;
				}
			}
			
			ssrData.ssrChargeTotal += amt;
			ssrData.ssrQtyTotal += qty;
			
			UI_Anci.updateSSRAvailCount(paxSSRs[ssrId],qty,'DEDUCT',segId);
			UI_Anci.updateAnciTotals(UI_Anci.anciType.SSR,amt,'ADD');
			UI_Anci.updateAnciCounts(UI_Anci.anciType.SSR,qty,'ADD');
			UI_Anci.updateFlightAnciView();
			UI_Anci.updateJnTax();
			UI_Anci.updateAnciPenalty();
		}		
	}
	
	/*
	 * Update User comment
	 */
	UI_Anci.updateSSRTextValue = function(paxId,text){
		var intSegRow = UI_Anci.getSSRSegIndex(UI_Anci.ssr.currentSegId);
				var paxSSRs = UI_Anci.jsAnciSegModel[UI_Anci.ssr.currentSegId].pax[paxId].ssr.ssrs;
		if(paxSSRs.length == 1){
			paxSSRs[0].text = text;
		}
	}
	/* 
	 * SSR available 
	 */
	UI_Anci.ssrAvailable = function(ssrIndex, qty){
		var intSegRow = UI_Anci.getSSRSegIndex(UI_Anci.ssr.currentSegId);
		var availQty = UI_Anci.jsSSRModel[intSegRow].specialServiceRequest[ssrIndex].availableQty;
		if(availQty== -1 || availQty>=qty){
			return true;
		}else{
			return false;
		}
	}
	/*
	 * Update ssr available count
	 */
	UI_Anci.updateSSRAvailCount = function(ssr, qty, op,segId){
		var intSegRow = UI_Anci.getSSRSegIndex(segId);
		var smSSR = UI_Anci.jsSSRModel[intSegRow].specialServiceRequest[ssr.ssrIndex];
		smSSR.availableQty = parseInt(smSSR.availableQty,10);
		if(smSSR.availableQty==-1){
			return;
		}
		if(op == 'ADD'){
			smSSR.availableQty += qty;
		}else if (op== 'DEDUCT'){
			smSSR.availableQty -= qty;
		}		
	}
	
	//------------------------------ Insurance -------------------------------------------------
	
	/*
	 * Build Insurance Main Plan (Default plan/ Push Plan)
	 */
	UI_Anci.buildInsuranceMainProduct = function(insObjs,isDefaultPlan){
		$.each( insObjs, function( key, value ) {
		insObj =  value;
			$.each(UI_Anci.avilableInsuranceproductGroup , function(key,insProductGroup){
				if(insProductGroup.POPUPAVILABLE && insProductGroup.INSREF.indexOf(insObj.insuranceRefNumber)>-1){
					if(insObj!=null){
						if(isDefaultPlan){
							if(!insObj.popup){
								UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'PUSH');
								UI_Anci.formattedInsAmount = UI_Anci.currency.base+' '+$.airutil.format.currency(insObj.quotedTotalPremiumAmount);
							}else{
								UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'REMOVE');
							}
						}else {
							if(insObj.popup){
								UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'PUSH');
								UI_Anci.formattedInsAmount = UI_Anci.currency.base+' '+$.airutil.format.currency(insObj.quotedTotalPremiumAmount);
							}else{
								UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'REMOVE');
							}
						}
						if(UI_Anci.showInsCharge){
							$('#lblInsuranceChDesc').html(UI_Anci.jsonLabel["lblInsuranceChDesc"]);
							$('#lblInsuranceDesc').html(UI_Anci.jsonLabel["lblInsuranceDesc"].replace("{0}", "<span>"+UI_Anci.currency.base+' '+$.airutil.format.currency(insObj.totalPerPaxPremiumAmount)+"</span>"));
							if ($('#lblInsuranceChDesc').html() != null){
								$('#lblInsuranceChDesc').html($('#lblInsuranceChDesc').html().replace("{0}", "<span>"+UI_Anci.formattedInsAmount+"</span>"));
							}
							var insAmount = 0;
							if(isDefaultPlan ){
								if(!insObj.popup){
									$('#tblInsMain').html(insObj.insuranceExternalContent.PLAN_COVERED_INFOMATION_DESCRIPTION);
									$('#tblInsTnC').html(insObj.insuranceExternalContent.PLAN_TERMS_AND_CONDITIONS);
									insAmount = insObj.quotedTotalPremiumAmount;
									setDisplayInsuranceAmount(insAmount, true);
								}
							}else{
								if(insObj.popup){
									$('.insCoverInforTable').html(insObj.insuranceExternalContent.PUSH_PLAN_SELECT_YES_DESCRIPTION);
									$('#insProductName').text(insObj.insuranceExternalContent.PLAN_TITEL);
									insAmount = insObj.quotedTotalPremiumAmount;
									setDisplayInsuranceAmount(insAmount, true);
								}
							}
							$("#radAnciIns_Y").unbind("click").bind("click", function(){
								if ($("#radAnciIns_Y").is(':checked') == true){
									UI_Anci.insRadioOnClick('radAnciIns_Y');
								}else{
									UI_Anci.insRadioOnClick('radAnciIns_N');
								}
							});
							$("#radAnciIns_N").unbind("click").bind("click", function(){					
								UI_Anci.insRadioOnClick('radAnciIns_N');
							});		
						}
						if (anciConfig.insTCCheck){
							var hrmlSrt = $("<input type='checkbox'/>").attr({"name":"chkTC","id":"chkTC", "disabled":true, "checked":false});
							hrmlSrt.click(function(){
								if ($("#chkTC").attr("checked") == false){
									jAlert(UI_Container.labels.insTCuncheckMsg);
								}
							});
							$("#insTCCHKDiv").empty().append(hrmlSrt);
						}
						if(UI_Anci.insViewType == 'New-multiOption-view' && insObj.insTypeCharges != null && insObj.insTypeCharges != "") {
							$('#radAnciIns_C').val(insObj.insTypeCharges.CANCELATION);
							$('#radAnciIns_M').val(insObj.insTypeCharges.MULTIRISK);
						} else 	if (UI_Anci.insViewType != "New-view"){
							if(!insObj.popup){
							$('#chkAnciIns').val(insObj.quotedTotalPremiumAmount);
							}
						}  else{
							if(!insObj.popup){
							$('#radAnciIns_Y').val(insObj.quotedTotalPremiumAmount);
							}
						}
						if(insObj.anciOffer){
//							$(".insPromoOffer").html(insObj.selectedLanguageAnciOfferName);
							$(".insPromoOffer").html(insObj.selectedLanguageAnciOfferDescription);
						}else{
							$(".insPromoOffer").parent().parent().hide();
						}
						
						if (UI_Container.hasInsurance) {
//							setDisplayInsuranceAmount(insObj.premiumBalance);
							$("#chkAnciIns").attr("disabled","disabled");
						} else {
							$("#chkAnciIns").attr("disabled","");
						}
					}
				}
			});
		});
	}
	
	/*
	 * Build Insurance Optional Plans (Rider Plan/ and other plans)
	 */
	UI_Anci.buildInsuranceOptionalProduct = function(insObjs){
		var insuranceQuote =  null;
		$.each(UI_Anci.avilableInsuranceproductGroup, function(insProductGroupIndex, avilProductGroup) {
			if(!avilProductGroup.POPUPAVILABLE){ // Select the Rider Plan group
				$.each( insObjs, function( key, value ) {
					insObj =  value;
					if(insObj.rank == avilProductGroup.RANK && insObj.groupID==avilProductGroup.GROUP){ // check insurance quote is a rider plan
						if(insObj!=null){
//							UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'PUSH');
							if(UI_Anci.showInsCharge){
								$('#lblInsuranceChDesc').html(UI_Anci.jsonLabel["lblInsuranceChDesc"]);
								insAmount = insObj.quotedTotalPremiumAmount;
								$('#tblInsRider').html(insObj.insuranceExternalContent.PLAN_COVERED_INFOMATION_DESCRIPTION);
								setDisplayInsuranceAmount(insAmount, false);
								if ($("#radAnciIns_Y_Rider").is(':checked') == true){
									UI_Anci.selectedInsuranceRefNumbers.push(insObj.insuranceRefNumber);
								}				
								$("#radAnciIns_Y_Rider").unbind("click").bind("click", function(){
									if ($("#radAnciIns_Y_Rider").is(':checked') == true){
										UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'PUSH');
										UI_Anci.insRadioOnClick('radAnciIns_Y_Rider');	
									}else{
										UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'REMOVE');
										UI_Anci.insRadioOnClick('radAnciIns_N_Rider');
									}
								});			
								$("#radAnciIns_N_Rider").unbind("click").bind("click", function(){	
									UI_Anci.InsertOrRemoveInsuranceQuote(insObj,'REMOVE')
									UI_Anci.insRadioOnClick('radAnciIns_N_Rider');
								});
							}			
							if(insObj.anciOffer){
								$(".insPromoOffer").html(insObj.selectedLanguageAnciOfferDescription);
							}else{
								$(".insPromoOffer").parent().parent().hide();
							}
						}
					}
				});
			}
		});

	}
		
	UI_Anci.RemoveAllSelectedInsuranceQuotes = function () {
		UI_Anci.selectedInsuranceQuotes = [];
	}
	
	UI_Anci.InsertOrRemoveInsuranceQuote = function(insObj,operator){  
		  var insuranceQuote = {insuranceRefNumber:insObj.insuranceRefNumber,quotedTotalPremiumAmount:insObj.quotedTotalPremiumAmount,selected:true,operatingAirline:insObj.operatingAirline};
	      if( operator == 'REMOVE'){
	    	 $.each(UI_Anci.selectedInsuranceQuotes,function(index,insQuote){
	    		 console.log("QuoteLoop "+insQuote);
	    		 if(insQuote.insuranceRefNumber == insObj.insuranceRefNumber){
	    			 UI_Anci.selectedInsuranceQuotes.splice(index,1);
	    			 return false;
	    		 }
	    	 }); 
	      }else if(operator == 'PUSH'){
	    	  var quoteIsAlreadyAvailable = false;
	    	  $.each(UI_Anci.selectedInsuranceQuotes,function(index,insQuote){
		    		 if(insQuote.insuranceRefNumber == insObj.insuranceRefNumber){
		    			 quoteIsAlreadyAvailable = true;
		    			 return false;
		    		 }
		    	 });
	    	  if(!quoteIsAlreadyAvailable){
	    		  UI_Anci.selectedInsuranceQuotes.push(insuranceQuote);
	    	  }
	      }	   
	      var selectedINSREFs = '';
	      $.each(UI_Anci.selectedInsuranceQuotes,function(index,insQuote){
	    	  selectedINSREFs +=' '+ insQuote.insuranceRefNumber;
	      });
	      console.log("UI_Anci.selectedInsuranceQuotes "+selectedINSREFs);
	}
	
	UI_Anci.removeMainPlansFromSelectedInsuranceList = function(){  
		UI_Anci.createInsuranceGroups.GROUP;
		$.each(UI_Anci.avilableInsuranceproductGroup,function(key,insGroup){
			if(insGroup.POPUPAVILABLE){
				$.each(UI_Anci.selectedInsuranceQuotes,function(index,insuranceQuote){
					var selectedInsRef =  insuranceQuote.insuranceRefNumber;
					if(insGroup.INSREF.indexOf(selectedInsRef)>-1){
						 UI_Anci.selectedInsuranceQuotes.splice(index,1);	
						 return false;
					}
				});
			}
		});
		
	}
	
	/**
	 * Insurance Amount Set
	 */
	function setDisplayInsuranceAmount(insAmount, isMainPlan) {
		UI_Anci.formattedInsAmount = UI_Anci.currency.selected+' '+UI_Anci.convertAmountToSelectedCurrency(insAmount);
		if(isMainPlan){
			$('#spnInsCost').html(UI_Anci.formattedInsAmount);
		}else{
			$('#spnInsCost_Rider').html(UI_Anci.formattedInsAmount);
		}
	}
	/*
	 * Show pop up
	 */
	UI_Anci.showPopupWindow = function(title,url,width,height,scroll){
		if(UI_Anci.winO!=null && !UI_Anci.winO.closed )
			UI_Anci.winO.close();
		if(width==null) width = 630;
		if(height==null) height = 900;
		if(scroll==null) scroll = false;
		//do not put title IE gives a script error
		UI_Anci.winO = window.open(url,"",$("#popWindow").getPopWindowProp(width, height, scroll));
		UI_Anci.winO.focus();
	}
	/*
	 * Insurance on click
	 */
	UI_Anci.insOnClick = function(){
		UI_commonSystem.pageOnChange();
		var ins = $('#chkAnciIns:checked').val();
		if(ins!=null){
			var total = parseFloat(ins);
			UI_Anci.anciInsurance.total = total;
			UI_Anci.anciInsurance.selected = true;
			UI_Anci.updateAnciTotals(UI_Anci.anciType.INS,total,'ADD');	
			UI_Anci.updateAnciCounts(UI_Anci.anciType.INS,1,'ADD');
		}else{
			UI_Anci.updateAnciTotals(UI_Anci.anciType.INS,UI_Anci.anciInsurance.total,'DEDUCT');
			UI_Anci.updateAnciCounts(UI_Anci.anciType.INS,1,'DEDUCT');
			UI_Anci.anciInsurance.selected = false;
			UI_Anci.anciInsurance.total =  0;
		}
	}
	
	/*
	 * Handle the insurance select/deselect options
	 */
	var selectOption =  "";
	UI_Anci.insRadioOnClick = function(val){
		if(val == "radAnciIns_Y" ){
			var ins = $('#radAnciIns_Y:checked').val();
			setInsuranceType(UI_Anci.INSURANCE_TYPE.GENERAL);
			if (anciConfig.insTCCheck){
				$("#chkTC").attr("disabled", false);
			}
			if(UI_Anci.lastSelectIsDefault){
				UI_Anci.buildInsuranceMainProduct(UI_Anci.insuranceObj,true);					
			}else {
				UI_Anci.lastSelectIsDefault = false;
				UI_Anci.buildInsuranceMainProduct(UI_Anci.insuranceObj,false);										
			}
			$("#radAnciIns_Y").attr("checked", true);
		} else if (val == "radAnciIns_C" && $("#radAnciIns_C").is(":checked")) {				
			    deductInsCharges(); 
			    selectOption = 'radAnciIns_C';
				var ins = $('#radAnciIns_C:checked').val();					
				radioButtonValueChange(ins);
				setInsuranceType(UI_Anci.INSURANCE_TYPE.CNX);			
		} else if (val == "radAnciIns_M" && $("#radAnciIns_M").is(":checked")) {				
			    deductInsCharges(); 
			    selectOption = 'radAnciIns_M';
			    var ins = $('#radAnciIns_M:checked').val();				   
			    radioButtonValueChange(ins);
			    setInsuranceType(UI_Anci.INSURANCE_TYPE.MLTRISK);			
		} else if(val == "radAnciIns_N"){
			if($("#radAnciIns_Y").attr("checked")=='checked'){
				$("#radAnciIns_Y").attr("checked", true);
				UI_Anci.insRadioOnClick('radAnciIns_Y');
				if(UI_Anci.lastSelectIsDefault){
					UI_Anci.buildInsuranceMainProduct(UI_Anci.insuranceObj,true);					
				}else {
					UI_Anci.lastSelectIsDefault = false;
					UI_Anci.buildInsuranceMainProduct(UI_Anci.insuranceObj,false);										
				}
			}else{
				UI_Anci.showInsPopup();
			}
		}else if(val == "radAnciIns_N_Rider"){
			
			
		}else{
			
		}
		updateInsuranceTotalAmount();
	}
	
	UI_Anci.buildINSINOF = function(){
		var htmlStr = "<table border='0' width='100%'>";
		htmlStr += "<tr><td><label class='fntBold'>"+UI_Anci.jsonLabel["insPopupNote1"]+"</label></td></tr>";
		htmlStr += "<tr><td><label>"+UI_Anci.jsonLabel["insPopupNote2"]+"</label></td></tr>";
		htmlStr += "<tr><td><label>"+UI_Anci.jsonLabel["insPopupNote3"]+"</label></td></tr>";
		htmlStr += "<tr><td><label>"+UI_Anci.jsonLabel["insPopupNote4"]+"</label></td></tr>";
		htmlStr += "</table>";
		return htmlStr;
	};
	
	UI_Anci.showInsPopup = function(){
		var height = 120;
		if (UI_Anci.jsonLabel["insPopupNote2"] != ""){height+=((parseInt(UI_Anci.jsonLabel["insPopupNote2"].length/80, 10)*12)+13);}
		if(UI_Anci.jsonLabel["insPopupNote3"] != "") {height+=((parseInt(UI_Anci.jsonLabel["insPopupNote2"].length/80, 10)*12)+13);}
		if(UI_Anci.jsonLabel["insPopupNote4"] != ""){height+=((parseInt(UI_Anci.jsonLabel["insPopupNote2"].length/80, 10)*12)+13);}
		$.each( UI_Anci.insuranceObj, function( key, insObj ) {
			
			$("#newPopItem").openMyPopUp({
				width:480,
				height:height,
				topPoint: ($(window).height()/2)-((height+70)/2),
				headerHTML:"<label class='fntBold'>"+ UI_Container.labels.insPopupNoteHD+ "</label>",
				bodyHTML: function(){
					if(insObj.popup){
						return insObj.insuranceExternalContent.PLAN_COVERED_INFOMATION_DESCRIPTION;
					}else{
						return UI_Anci.buildINSINOF();
					}
				
				
				},
				footerHTML: function(){
					var passHTML = "<label class='fntBold'>"+UI_Anci.jsonLabel["insPopupNote5"]+"</label><br>" +
							"<input type='button' value='"+UI_Anci.jsonLabel["insPopupNote6"]+"' class='InsConfirm' style='margin:5px 0;padding:5px 20px'/><br>" +
							"<input type='button' value='"+UI_Anci.jsonLabel["insPopupNote7"]+"' class='InsCancel' style='margin:5px 0;padding:5px 20px'/>";
					return passHTML;
				}
			});
			//for tune insurance
			if(insObj.popup){
				insObj.insuranceExternalContent.PLAN_COVERED_INFOMATION_DESCRIPTION
				$(".InsConfirm").attr('value', insObj.insuranceExternalContent.POPUP_YES_BUTTON_TEXT);
				$(".InsCancel").attr('value', insObj.insuranceExternalContent.POPUP_NO_BUTTON_TEXT);
				var formattedSecondryInsAmount =  UI_Anci.currency.selected+' '+UI_Anci.convertAmountToSelectedCurrency(insObj.quotedTotalPremiumAmount);
				$('#spnPushInsCost').text(formattedSecondryInsAmount);
			
			}
			$(".close").unbind("click").bind("click",function(){
				$("#radAnciIns_Y").attr("checked", true);
				UI_Anci.insRadioOnClick('radAnciIns_Y');
				closeInsPopup();
			});
			$(".InsConfirm").unbind("click").bind("click",function(){
				$("#radAnciIns_Y").attr("checked", true);
				UI_Anci.lastSelectIsDefault = false;
				UI_Anci.reBuildInsuranceWithSecondaryPolicy();
				updateInsuranceTotalAmount();
				closeInsPopup();
			});
			$(".InsCancel").unbind("click").bind("click",function(){
				deductInsCharges();
				UI_Anci.removeMainPlansFromSelectedInsuranceList();
				updateInsuranceTotalAmount();
				$("#radAnciIns_N").attr("checked", true);
				$("#radAnciIns_Y").attr("checked", false);
				
				$("#radAnciIns_Y").unbind("click").bind("click", function(){
					if ($("#radAnciIns_Y").is(':checked') == true){
						UI_Anci.insRadioOnClick('radAnciIns_Y');
					}else{
						UI_Anci.insRadioOnClick('radAnciIns_N');
					}
				});
				if (anciConfig.insTCCheck){
					$("#chkTC").attr("checked", false);
					$("#chkTC").attr("disabled", true);
				}
				closeInsPopup();
			});
			if(insObj.popup){
				return false ;
			}
		});
	};
	
	function closeInsPopup(){
		$("#newPopItem").closeMyPopUp();
		
	}
	
	
	function deductInsCharges() {
		var currentInsTotal = 0;
		$.each(UI_Anci.anciSummary,function(index,ancisummaryObj){
			if(ancisummaryObj.type==UI_Anci.anciType.INS){
				currentInsTotal = ancisummaryObj.subTotal;
			}
		});
		UI_Anci.updateAnciTotals(UI_Anci.anciType.INS,currentInsTotal,'DEDUCT');
		UI_Anci.updateAnciCounts(UI_Anci.anciType.INS,1,'DEDUCT');
		setInsuranceType(UI_Anci.INSURANCE_TYPE.NONE);
		UI_Anci.updateFreeServiceDiscount();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
	}
	
	function radioButtonValueChange(ins) {
		var total = parseFloat(ins);
//		setDisplayInsuranceAmount(total);
		UI_Anci.anciInsurance.selected = true;
		UI_Anci.anciInsurance.total = total;		
		UI_Anci.updateAnciTotals(UI_Anci.anciType.INS,total,'ADD');	
		UI_Anci.updateAnciCounts(UI_Anci.anciType.INS,1,'ADD');
		UI_Anci.updateFreeServiceDiscount();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
	}
	
	function updateInsuranceTotalAmount() {
		deductInsCharges();
		var insTotal = 0;
		$.each(UI_Anci.selectedInsuranceQuotes,function(index,selectedQuote){
			insTotal += selectedQuote.quotedTotalPremiumAmount;
		});
		var total = parseFloat(insTotal);		
		UI_Anci.updateAnciTotals(UI_Anci.anciType.INS,total,'ADD');	
		UI_Anci.updateAnciCounts(UI_Anci.anciType.INS,1,'ADD');
		UI_Anci.updateFreeServiceDiscount();
		UI_Anci.updateJnTax();
		UI_Anci.updateAnciPenalty();
	}
	
	function setInsuranceType(insType) {
		$("#insType").val(insType);
	}
	
	//------------------------------ BUILD Flight Info -------------------------------------
	
	/* 
	 * Build flight info summary
	 * @Depricated
	 */
	UI_Anci.buildFlightInfoSummary = function(){
		//flightTemplate
		
		var templateName = '#flightTemplate';
		
		for (var i = 0 ;  i < UI_Anci.flightSegments.length; i++ ){		
				
			var seg = UI_Anci.flightSegments[i];
			
			var tmpltId = 'flightTemplate_'+i;
			$('#'+tmpltId).remove();
			var clone = $('#flightTemplate').clone();
			clone.attr('id', tmpltId );
			$('#flightTemplate').before(clone);  			
			
			var segName = $('#'+tmpltId+' td[name="name"]');		
			segName.html(seg.segmentCode);
			
			var fltNo = $('#'+tmpltId+' label[name="flightNumber"]');		
			fltNo.text(seg.flightNumber);
			
			var departDate = $('#'+tmpltId+' label[name="departureDate"]');		
			departDate.text(seg.departureDate);
			
			var departTime = $('#'+tmpltId+' label[name="departureTime"]');		
			departTime.text(seg.departureTime);
			
			var arrTime = $('#'+tmpltId+' label[name="arrivalTime"]');		
			arrTime.text(seg.arrivalTime);

			var anciDetails =$('#'+tmpltId+' span[name="anciDetails"]');	
			anciDetails.attr('id','anciDetails_'+i);
		}
		$('#flightTemplate').hide();
		
		var fltDir = UI_Anci.jsonLabel['lblRoundTrip'];
		var hasChildren = false;
		var hasInfants = false;
		var hasAdults = false;
		var adultCount = '';
		var childCount = '';
		var infantCount = '';
		//UI_Anci.journeySummary = {childCount:p.childCount,adultCount:p.adultCount,infantCount:p.infantCount,rtnTrip:p.blnReturn};
		if(UI_Anci.journeySummary.childCount > 0 ){
			hasChildren = true;
			if(UI_Anci.journeySummary.childCount == 1)
				childCount = '1 '+UI_Anci.jsonLabel['lblChildHD'];
			else 
				childCount = UI_Anci.journeySummary.childCount + ' ' +UI_Anci.jsonLabel['lblChildHD'];
		}	
		if(UI_Anci.journeySummary.adultCount > 0 ){
			hasAdults = true;
			adultCount = UI_Anci.journeySummary.adultCount + ' ';
			if(UI_Anci.journeySummary.adultCount>1){
				adultCount +=  UI_Anci.jsonLabel['lblAdultHD'];
			}else{
				adultCount +=  UI_Anci.jsonLabel['lblAdultHD'];
			}
		}	
		if(UI_Anci.journeySummary.infantCount > 0 ){
			hasInfants = true;
			infantCount = UI_Anci.journeySummary.infantCount + ' ';
			if(UI_Anci.journeySummary.infantCount>1){
				infantCount += UI_Anci.jsonLabel['lblInfantHD'];
			}else{
				infantCount += UI_Anci.jsonLabel['lblInfantHD'];
			}
		}
		if(!UI_Anci.journeySummary.rtnTrip)
			fltDir = UI_Anci.jsonLabel['lblOneWayTrip'];;
		
		$('#lblRetFlight').text(fltDir).show();
		$('#lblReturn').text(fltDir).show();
		
		if(hasAdults){
			$('#adultCount').text(adultCount).show();
			$('#lblAdults').text(adultCount).show();
		}
		
		if(hasChildren){
			$('#childCount').text(childCount).show();
			$('#lblChildren').text(childCount).show();
		}
		
		if(hasInfants){
			$('#infantCount').text(infantCount).show();
			$('#lblInfants').text(infantCount).show();
		}
				
		
	}
	
	/* 
	 * Update Flight Ancillary View 
	 */
	UI_Anci.updateFlightAnciView = function(){
		var segData = [];
		for(var i = 0 ; i < UI_Anci.jsAnciSegModel.length; i++ ){			
			segData[i]= {seats:[],meals:[],ssrs:[],baggages:[],apss:[], apts:[]};
			for(var j = 0 ; j< UI_Anci.jsAnciSegModel[i].pax.length; j++){
				if(UI_Anci.jsAnciSegModel[i].pax[j].seat.seatNumber != ''){
					segData[i].seats[segData[i].seats.length] = UI_Anci.jsAnciSegModel[i].pax[j].seat.seatNumber;
				}
				for( var k = 0 ; k < UI_Anci.jsAnciSegModel[i].pax[j].meal.meals.length; k++){
					//if(UI_Container.isFOCmealEnabled == false) {
						segData[i].meals[segData[i].meals.length]= UI_Anci.jsAnciSegModel[i].pax[j].meal.meals[k].mealQty + "x" + UI_Anci.jsAnciSegModel[i].pax[j].meal.meals[k].mealName;
					//}
				}
				for( var k = 0 ; k < UI_Anci.jsAnciSegModel[i].pax[j].ssr.ssrs.length; k++){
					segData[i].ssrs[segData[i].ssrs.length] = UI_Anci.jsAnciSegModel[i].pax[j].ssr.ssrs[k].description;
				}
				for( var k = 0 ; k < UI_Anci.jsAnciSegModel[i].pax[j].baggage.baggages.length; k++){					
					segData[i].baggages[segData[i].baggages.length]=UI_Anci.jsAnciSegModel[i].pax[j].baggage.baggages[k].baggageName;					
				}
				for(var k=0; k < UI_Anci.jsAnciSegModel[i].pax[j].aps.apss.length; k++){
					segData[i].apss[segData[i].apss.length] = UI_Anci.jsAnciSegModel[i].pax[j].aps.apss[k].ssrName;
				}
				for(var k=0; k < UI_Anci.jsAnciSegModel[i].pax[j].apt.apts.length; k++){
					segData[i].apts[segData[i].apts.length] = UI_Anci.jsAnciSegModel[i].pax[j].apt.apts[k].ssrName;
				}
			}
			
		}
		UI_Container.showSelectedAncillary(segData);
		UI_Anci.updateFreeServiceDiscount();
	}
	
	UI_Anci.updateAnciPenalty = function(){
		
		if(UI_Container.isModifyAncillary() && UI_Container.applyAncillaryPenalty){
			var paxWiseCurrentAnciTotal = UI_Anci.getPaxWiseCurrentAnciTotal();
			var paxWiseNewAnciTotal = UI_Anci.getPaxWiseAnciTotal();
			
			var totalAnciPenalty = 0;
			$.each(paxWiseCurrentAnciTotal, function(paxIndex, paxCurrentAnciTotal){
				var paxNewAnciTotal = paxWiseNewAnciTotal[paxIndex];
				//Anci credit exist for current pax
				if(parseFloat(paxCurrentAnciTotal) > parseFloat(paxNewAnciTotal)){
					totalAnciPenalty = totalAnciPenalty +( parseFloat(paxCurrentAnciTotal) - parseFloat(paxNewAnciTotal) );
				}
			});
			
			UI_Container.updateAnciPenaltyAmount(totalAnciPenalty);
		}
		
	}
	
	UI_Anci.updateJnTax = function() {
		if(UI_Anci.JNTax.isApplicable){	
			if(UI_Container.isModifyAncillary()){
				var paxWiseCurrentAnciTotal = UI_Anci.getPaxWiseCurrentAnciTotal();
				var paxWiseNewAnciTotal = UI_Anci.getPaxWiseAnciTotal();
				
				var totalAnciUpgradeAmount = 0;
				$.each(paxWiseCurrentAnciTotal, function(paxIndex, paxCurrentAnciTotal){
					var paxNewAnciTotal = paxWiseNewAnciTotal[paxIndex];
					//calculate JN tax for upgrade amount
					if(parseFloat(paxNewAnciTotal) > parseFloat(paxCurrentAnciTotal)){
						totalAnciUpgradeAmount = totalAnciUpgradeAmount +( parseFloat(paxNewAnciTotal) - parseFloat(paxCurrentAnciTotal) );
					}
				});
				var taxAmount = totalAnciUpgradeAmount*UI_Anci.JNTax.ratio;
				taxAmount = (Math.ceil(taxAmount*100))/100;
				UI_Anci.JNTax.taxAmount = taxAmount;
			}else{
				var paxWiseAnciTotal = UI_Anci.getPaxWiseAnciTotal();			
				var paxWiseCurrentAnciTotal = UI_Anci.getPaxWiseCurrentAnciTotal();
				
				UI_Anci.JNTax.taxAmount = 0;
				//Calculate pax wise tax and add it to total
				$.each(paxWiseAnciTotal, function(paxIndex, paxAnciAmount){
					var paxTaxAmount = paxAnciAmount * UI_Anci.JNTax.ratio;
					//Roundup to 2 decimal places & assign
					paxTaxAmount = (Math.ceil(paxTaxAmount*100))/100;
					UI_Anci.JNTax.taxAmount += paxTaxAmount;
				});
			}
			UI_Container.updateAnciJnTaxAmount(UI_Anci.JNTax);
		} 
	}
	
	UI_Anci.getPaxWiseCurrentAnciTotal = function(){
		var paxWiseCurrentAnciTotal = {};
		
		var modifyingFltSegCodes = new Array();
		for(var i = 0 ; i < UI_Anci.jsAnciSegModel.length; i++ ){	
			modifyingFltSegCodes.push(UI_Anci.jsAnciSegModel[i].segCode);
		}
		
		for ( var paxId = 0; paxId < UI_Anci.jsonPaxAdults.length; paxId++) {
			var paxObj = UI_Anci.jsonPaxAdults[paxId];
			var paxCurrentAncis = paxObj.currentAncillaries;
			
			if(paxWiseCurrentAnciTotal[paxId] == undefined || paxWiseCurrentAnciTotal[paxId] == null){
				paxWiseCurrentAnciTotal[paxId] = 0;
			}

			if(paxCurrentAncis != null && paxCurrentAncis.length > 0){				
				for ( var paxAnciId = 0; paxAnciId < paxCurrentAncis.length; paxAnciId++) {
					var anciObj = paxCurrentAncis[paxAnciId];
					var fltRefNum = anciObj.flightSegmentTO.flightRefNumber;
					
					if($.inArray(anciObj.flightSegmentTO.segmentCode, modifyingFltSegCodes) != -1){
						if(UI_Anci.isAnciEnabled(fltRefNum, 'seat') &&
                            (!UI_Container.isModifyAncillary() ||
                                (UI_Container.isModifyAncillary() && UI_Anci.modifyAnciAllowed(UI_Anci.anciType.SEAT) && UI_Anci.getSeatSegIndexByFltRefNo(fltRefNum) != null))) {
							var airSeatDTO = anciObj.airSeatDTO;
							if(airSeatDTO != null && airSeatDTO.seatNumber != ''){
								paxWiseCurrentAnciTotal[paxId] += airSeatDTO.seatCharge;
							}
						}
						
						if(UI_Anci.isAnciEnabled(fltRefNum, 'meal') &&
                            (!UI_Container.isModifyAncillary() ||
                                (UI_Container.isModifyAncillary() && UI_Anci.modifyAnciAllowed(UI_Anci.anciType.MEAL) && UI_Anci.getMealSegIndexByFltRefNo(fltRefNum) != null))) {
							var meals = anciObj.mealDTOs;
							if(meals != null && meals.length > 0){						
								for ( var mealCount = 0; mealCount < meals.length; mealCount++) {
									var mealObj = meals[mealCount];
									paxWiseCurrentAnciTotal[paxId] += parseFloat(mealObj.totalPrice);
								}
							}
						}
						
						if(UI_Anci.isAnciEnabled(fltRefNum, 'baggage') &&
                            (!UI_Container.isModifyAncillary() ||
                                (UI_Container.isModifyAncillary() && UI_Anci.modifyAnciAllowed(UI_Anci.anciType.BAGGAGE) && UI_Anci.getBaggageSegIndexByFltRefNo(fltRefNum) != null))) {
							var baggages = anciObj.baggageDTOs;
							if(baggages != null && baggages.length > 0){						
								for ( var baggageCount = 0; baggageCount < baggages.length; baggageCount++) {
									var baggageObj = baggages[baggageCount];
									paxWiseCurrentAnciTotal[paxId] += baggageObj.baggageCharge;
								}
							}
						}
						
						if(UI_Anci.isAnciEnabled(fltRefNum, 'hala') &&
                            (!UI_Container.isModifyAncillary() ||
                                (UI_Container.isModifyAncillary() && UI_Anci.modifyAnciAllowed(UI_Anci.anciType.HALA)))) {
                            var apss = anciObj.airportServiceDTOs;
							if(apss != null && apss.length > 0){						
								for ( var apssCount = 0; apssCount < apss.length; apssCount++) {
									var apsObj = apss[apssCount];
									paxWiseCurrentAnciTotal[paxId] += apsObj.serviceCharge;
								}
							}
						}
						
						if(UI_Anci.isAnciEnabled(fltRefNum, 'ssr') &&
                            (!UI_Container.isModifyAncillary() ||
                                (UI_Container.isModifyAncillary() && UI_Anci.modifyAnciAllowed(UI_Anci.anciType.SSR)))) {
							var ssrs = anciObj.specialServiceRequestDTOs;
							if(ssrs != null && ssrs.length > 0){						
								for ( var ssrsCount = 0; ssrsCount < ssrs.length; ssrsCount++) {
									var ssrObj = ssrs[ssrsCount];
									paxWiseCurrentAnciTotal[paxId] += ssrObj.serviceCharge;
								}
							}
						}
						
					}

				}
			}
			
		}
		
		return paxWiseCurrentAnciTotal;
	}
	
	UI_Anci.isAnciEnabled = function(flightRefNum, anciType){
			for ( var i = 0; i < UI_Anci.jsAnciSeg.length; i++) {
				var anciObj = UI_Anci.jsAnciSeg[i];
				flightRefNum = _getActualFlightReferenceNumber(flightRefNum);
				var anciFltRefNum = _getActualFlightReferenceNumber(anciObj.flightSegmentTO.flightRefNumber);
				
				if(flightRefNum == anciFltRefNum){
					return anciObj[anciType];
				}
				
			}
		}
	
	UI_Anci.calculateTaxableAnciTotal = function(paxWiseAnciTotal){
		var anciTotal = 0;
		$.each(paxWiseAnciTotal, function(paxIndex, paxAnciTotal){
			if(paxAnciTotal != null){
				anciTotal += paxAnciTotal;
			}
		});
		
		return anciTotal;
	}
	
	UI_Anci.getPaxWiseAnciTotal = function(){
		var paxWiseTotal = {};
		
		var anciSegLength = UI_Anci.jsAnciSegModel.length;
		var totalPaxCount = UI_Anci.jsonPaxAdults.length;
		
		var insuranceCharge = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.INS).subTotal || 0);
		var paxInsurance = insuranceCharge/(totalPaxCount * anciSegLength);
		
		var flexiCharge = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.FLEXI).subTotal || 0);
		var paxFlexiChg = flexiCharge/(totalPaxCount * anciSegLength);
		
		var baggegeOndGroupCount = UI_Anci.getBaggageOndGroupCount();
		var paxBgCurrentOndGroupCount = {};
        var visitedOndBaggageGroupIds = [];
		
		for ( var i = 0; i < anciSegLength; i++) {
			var jsAnciSegObj = UI_Anci.jsAnciSegModel[i];
			for(var paxId = 0 ; paxId< jsAnciSegObj.pax.length; paxId++){

				
				if(paxBgCurrentOndGroupCount[paxId] == undefined || paxBgCurrentOndGroupCount[paxId] == null){
					paxBgCurrentOndGroupCount[paxId] = {};
				}
				
				var bgCurrentOndGroupCount = paxBgCurrentOndGroupCount[paxId];
				
				var paxSegObj = jsAnciSegObj.pax[paxId];
				
				if(paxWiseTotal[paxId] == undefined || paxWiseTotal[paxId] == null){
					paxWiseTotal[paxId] = 0;
				}
				
				if(paxInsurance > 0){
					paxWiseTotal[paxId] += paxInsurance;
				}

				if(paxFlexiChg > 0){
					paxWiseTotal[paxId] += paxFlexiChg;
				}

				if(paxSegObj.seat.seatNumber != ''){
					paxWiseTotal[paxId] += paxSegObj.seat.seatCharge;
				}
				
				if(paxSegObj.meal.meals.length > 0){							
					for( var k = 0 ; k < paxSegObj.meal.meals.length; k++){
						paxWiseTotal[paxId] += paxSegObj.meal.meals[k].subTotal;
					}
				}
				
				if(paxSegObj.baggage.baggages.length > 0){
					
					for( var k = 0 ; k < paxSegObj.baggage.baggages.length; k++){
						var baggageObj = paxSegObj.baggage.baggages[k];

                        if (visitedOndBaggageGroupIds[paxSegObj.refI] === undefined) {
                            visitedOndBaggageGroupIds[paxSegObj.refI] = [];
                        }

                        if (visitedOndBaggageGroupIds[paxSegObj.refI][baggageObj.ondBaggageGroupId] === undefined) {
                            visitedOndBaggageGroupIds[paxSegObj.refI][baggageObj.ondBaggageGroupId] = [];
                        }

                        if (visitedOndBaggageGroupIds[paxSegObj.refI][baggageObj.ondBaggageGroupId][baggageObj.baggageName] === undefined) {
                            visitedOndBaggageGroupIds[paxSegObj.refI][baggageObj.ondBaggageGroupId][baggageObj.baggageName] = 1;

                            paxWiseTotal[paxId] += baggageObj.subTotal;

                        }
						
					}
				}
				
				if(paxSegObj.aps.apss.length > 0){					
					for( var k = 0 ; k < paxSegObj.aps.apss.length; k++){
						paxWiseTotal[paxId] += paxSegObj.aps.apss[k].subTotal;
					}
				}
				
				if(paxSegObj.ssr.ssrs.length > 0){					
					for( var k = 0 ; k < paxSegObj.ssr.ssrs.length; k++){
						paxWiseTotal[paxId] += paxSegObj.ssr.ssrs[k].subTotal;
					}
				}

			}
		}
		
		return paxWiseTotal;
	}
	
	UI_Anci.getBaggageOndGroupCount = function(){
		var bgOndGroupCount = {};
		if(UI_Anci.jsBaggageModel != null){			
			for(var i = 0 ; i< UI_Anci.jsBaggageModel.length; i++){
				
				var paxSegObj = UI_Anci.jsBaggageModel[i];
				
				if(paxSegObj.baggages.length > 0){							
					for( var k = 0 ; k < paxSegObj.baggages.length; k++){
						var baggageObj = paxSegObj.baggages[k];
						
						if(bgOndGroupCount[baggageObj.baggageName] == undefined 
								|| bgOndGroupCount[baggageObj.baggageName] == null){
							bgOndGroupCount[baggageObj.baggageName] = {};
						}
						
						if(bgOndGroupCount[baggageObj.baggageName][baggageObj.ondBaggageGroupId] == undefined 
								|| bgOndGroupCount[baggageObj.baggageName][baggageObj.ondBaggageGroupId] == null){
							bgOndGroupCount[baggageObj.baggageName][baggageObj.ondBaggageGroupId] = 0;
						}
						
						bgOndGroupCount[baggageObj.baggageName][baggageObj.ondBaggageGroupId] += 1;;
					}
				}
			}
		}
		
		return bgOndGroupCount;
	}
	
	// --------- ANCI Summary ----------------------------------------------------------------------
	
	//Free Service discount population to summary panel
	UI_Anci.updateFreeServiceDiscount = function(){
		
		var freeServiceDiscount = UI_Anci.getPromoDiscountObject(UI_Anci.promoTypes.FREESERVICE);
		var discountedAnciTypes = [];
		var applicableOnds = [];
		var deductFromTotal = false;

		if(freeServiceDiscount != null){
			discountedAnciTypes = freeServiceDiscount.applicableAncillaries;
			applicableOnds = freeServiceDiscount.applicableOnds;
			
			if(discountedAnciTypes != null && discountedAnciTypes.length > 0){
				
				if(freeServiceDiscount.discountAs == UI_Anci.promoDiscountAs.MONEY){
					deductFromTotal = true;
				}
				
				var paxWiseDisCountableTotal = {};        
				var totalPaxCount = UI_Anci.jsonPaxAdults.length;
				var anciSegLength = UI_Anci.jsAnciSegModel.length;
				
				var insurance = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.INS).subTotal || 0);
				var paxSegInsurance = insurance/(totalPaxCount * anciSegLength);
				var promoValid=false;	
				var paxwiseOndBaggageMap = {};	
				
				for(var i = 0 ; i < anciSegLength; i++ ){
					var jsAnciSegObj = UI_Anci.jsAnciSegModel[i];
					var segCode = jsAnciSegObj.flightRefNumber.split("$")[1];
					var segAnci = UI_Anci.jsAnciSeg[i];
					for(var l1 = 0 ; l1 < segAnci.ancillaryStatusList.length; l1++){
										
					if(segAnci.ancillaryStatusList[l1].available){ 
						for(var k1=0 ;k1< freeServiceDiscount.applicableAncillaries.length;k1++){
							var matchingAnciTypeVal = UI_Anci.promoAnciType[segAnci.ancillaryStatusList[l1].ancillaryType.ancillaryType];
							if(freeServiceDiscount.applicableAncillaries[k1]==matchingAnciTypeVal
									&& segAnci.ancillaryStatusList[l1].available){
								promoValid=true;
							}
						}
					}
					}
					if(!UI_Anci.isMatchOndFound(applicableOnds, segCode)){
						continue;
					}		

					for(var j = 0 ; j< jsAnciSegObj.pax.length; j++){
						
						paxwiseOndBaggageMap[j] = [];
						
						if(paxWiseDisCountableTotal[j] == undefined || paxWiseDisCountableTotal[j] == null){
							paxWiseDisCountableTotal[j] = 0;
						}
						
						if(UI_Anci.isAnciDiscountable(UI_Anci.anciType.SEAT, discountedAnciTypes)
								&& jsAnciSegObj.pax[j].seat.seatNumber != ''){
						
								var seg = UI_Anci.jsAnciSeg[i]; 
								for(var l = 0 ; l < seg.ancillaryStatusList.length; l++){
									if(seg.ancillaryStatusList[l].available){ 
										var anciType = seg.ancillaryStatusList[l].ancillaryType.ancillaryType;
										if(anciType==UI_Anci.anciType.SEAT){
											paxWiseDisCountableTotal[j] += jsAnciSegObj.pax[j].seat.seatCharge;
										}
									}
								}
						}
			
						if(!UI_Container.isFOCmealEnabled && UI_Anci.isAnciDiscountable(UI_Anci.anciType.MEAL, discountedAnciTypes)) {
							for( var k = 0 ; k < jsAnciSegObj.pax[j].meal.meals.length; k++){
								var seg = UI_Anci.jsAnciSeg[i]; 
								for(var l = 0 ; l < seg.ancillaryStatusList.length; l++){
									if(seg.ancillaryStatusList[l].available){ 
										var anciType = seg.ancillaryStatusList[l].ancillaryType.ancillaryType;
										if(anciType==UI_Anci.anciType.MEAL){
											paxWiseDisCountableTotal[j] += jsAnciSegObj.pax[j].meal.meals[k].subTotal;
										}
									}
								}
								
							}
						}
						
						
						
						if(UI_Anci.isAnciDiscountable(UI_Anci.anciType.BAGGAGE, discountedAnciTypes)){
							for( var k = 0 ; k < jsAnciSegObj.pax[j].baggage.baggages.length; k++){		
								var seg = UI_Anci.jsAnciSeg[i]; 
								for(var l = 0 ; l < seg.ancillaryStatusList.length; l++){
									if(seg.ancillaryStatusList[l].available){ 
										var anciType = seg.ancillaryStatusList[l].ancillaryType.ancillaryType;
										if(anciType==UI_Anci.anciType.BAGGAGE){					
											if(paxwiseOndBaggageMap[j][jsAnciSegObj.pax[j].baggage.baggages[k].ondBaggageGroupId] === undefined) {
												paxwiseOndBaggageMap[j][jsAnciSegObj.pax[j].baggage.baggages[k].ondBaggageGroupId] = {amount:jsAnciSegObj.pax[j].baggage.baggages[k].subTotal,count:1};
											} else {
												paxwiseOndBaggageMap[j][jsAnciSegObj.pax[j].baggage.baggages[k].ondBaggageGroupId].count +=1; 
											}
										 }
									}
								}
												
							}
						}
						
						if(UI_Anci.isAnciDiscountable(UI_Anci.anciType.HALA, discountedAnciTypes)){
							
							for(var k=0; k < jsAnciSegObj.pax[j].aps.apss.length; k++){
								var seg = UI_Anci.jsAnciSeg[i]; 
								for(var l = 0 ; l < seg.ancillaryStatusList.length; l++){
									if(seg.ancillaryStatusList[l].available){ 
										var anciType = seg.ancillaryStatusList[l].ancillaryType.ancillaryType;
										if(anciType==UI_Anci.anciType.HALA){
											paxWiseDisCountableTotal[j] += jsAnciSegObj.pax[j].aps.apss[k].subTotal;	
										}
									}
								}
								
							}
						}
						
						if(UI_Anci.isAnciDiscountable(UI_Anci.anciType.SSR, discountedAnciTypes)){					
							for( var k = 0 ; k < jsAnciSegObj.pax[j].ssr.ssrs.length; k++){
								var seg = UI_Anci.jsAnciSeg[i]; 
								for(var l = 0 ; l < seg.ancillaryStatusList.length; l++){
									if(seg.ancillaryStatusList[l].available){ 
										var anciType = seg.ancillaryStatusList[l].ancillaryType.ancillaryType;
										if(anciType==UI_Anci.anciType.SSR){
											paxWiseDisCountableTotal[j] += jsAnciSegObj.pax[j].ssr.ssrs[k].subTotal;
										}
									}
								}
								
							}
						}
						
						if(UI_Anci.isAnciDiscountable(UI_Anci.anciType.INS, discountedAnciTypes)){
						
								var seg = UI_Anci.jsAnciSeg[i]; 
								for(var l = 0 ; l < seg.ancillaryStatusList.length; l++){
									if(seg.ancillaryStatusList[l].available){ 
										var anciType = seg.ancillaryStatusList[l].ancillaryType.ancillaryType;
										if(anciType==UI_Anci.anciType.INS){
											paxWiseDisCountableTotal[j] += paxSegInsurance;
										}
									}
								}
								
							
							
						}
						
					}			
				}
				
				
				for(var pax in paxwiseOndBaggageMap){  
					var baggageValue = 0;
				    for(var bag in paxwiseOndBaggageMap[pax]) {
				    	baggageValue += (paxwiseOndBaggageMap[pax][bag].amount/paxwiseOndBaggageMap[pax][bag].count);
				    }
				    paxWiseDisCountableTotal[pax] += baggageValue;
				}
			
				if(promoValid){
					UI_Anci.freeServiceDiscount = UI_Anci.calculateAnciDiscount(freeServiceDiscount, paxWiseDisCountableTotal);
					UI_Container.updatePromoDiscount({discount: UI_Anci.freeServiceDiscount, deductFromTot: deductFromTotal});
					var fareQuote=UI_Container.fareQuote;
					//Add Promotion Banner
					$("#divAnciPromoBanner").show();
					$("#tempPromoBanner").remove();
					
					var curr = UI_Anci.currency.base;
					var selCur = UI_Anci.currency.selected;
					var disCurr = curr;
					var disAmount=freeServiceDiscount.farePercentage;
					
					if(curr != selCur){
						disCurr = selCur;
						disAmount=$.airutil.format.currency(freeServiceDiscount.farePercentage*fareQuote.inSelectedCurr.exchangeRate);
					}
					
					var strMSG = freeServiceDiscount.description;
					if(strMSG != null && strMSG != ''){
						var discountType = freeServiceDiscount.discountType;
						var strDiscountBnr = '';
						if(discountType == UI_Anci.promoDiscountType.PERCENTAGE){
							strDiscountBnr = freeServiceDiscount.farePercentage + '%';
						} else if(discountType == UI_Anci.promoDiscountType.VALUE) {
							strDiscountBnr = disCurr + ' ' + disAmount;
						}
						
						strMSG = strMSG.replace("{amount}", strDiscountBnr);
						strMSG = strMSG.replace("{Amount}", strDiscountBnr);
						var tBanner = $("<div id='tempPromoBanner'>"+strMSG+"</div>").addClass("spMsg");
						$("#divAnciPromoBanner").append(tBanner);
					}
				} else{
					$("#divAnciPromoBanner").hide();
					$("#tempPromoBanner").remove();
					var fareQuote=UI_Container.fareQuote;
					fareQuote.hasPromoDiscount = false;
				}
				
			} else {
				$("#divAnciPromoBanner").hide();
				$("#tempPromoBanner").remove();
			}
			
		}

	}
	
	UI_Anci.isMatchOndFound = function(ondList, segmentCode){
		for ( var i = 0; i < ondList.length; i++) {
			if((ondList[i]).indexOf(segmentCode) != -1){
				return true;
			}			
		}
		
		return false;
	}
	
	UI_Anci.getPromoDiscountObject = function(discountType){
		var promoObj = null;
		var appliedDiscount = UI_Container.getDiscountInfo();
		if(appliedDiscount != null && appliedDiscount.promotionId > 0 && appliedDiscount.promotionType == discountType){
			promoObj = appliedDiscount;
		}
		return promoObj;
	}
	
	UI_Anci.isAnciDiscountable = function(anciType, discountedAnciList){
		for ( var i = 0; i < discountedAnciList.length; i++) {
			if(UI_Anci.promoAnciType[anciType] == discountedAnciList[i]){
				return true;
			}
		}
		
		return false;
	}
	
	UI_Anci.calculateAnciDiscount = function(freeServiceDiscount, paxWiseDiscountableTotal){
		var anciDiscountAmount = 0;
		if(freeServiceDiscount != null){
			var totalPaxCount = UI_Anci.jsonPaxAdults.length;
			var discountType = freeServiceDiscount.discountType;
			var applyTo = freeServiceDiscount.discountApplyTo;
			var discountValue = freeServiceDiscount.farePercentage;
			
			if(discountType == UI_Anci.promoDiscountType.PERCENTAGE){
				var discountableAnciTotal = 0;
				$.each(paxWiseDiscountableTotal, function(key,value){
					discountableAnciTotal += value;
				});
				anciDiscountAmount = (discountableAnciTotal * discountValue)/100;
			} else if(discountType == UI_Anci.promoDiscountType.VALUE) {
				var paxEligibalDiscountAmount = 0;
				var paxAnciDiscount = 0;
				if(applyTo == UI_Anci.PromoAppicablity.RESERVATION){
					paxEligibalDiscountAmount = discountValue/totalPaxCount;
				} else if(applyTo == UI_Anci.PromoAppicablity.PER_PAX){
					paxEligibalDiscountAmount = discountValue;
				}
				
				for ( var i = 0; i < totalPaxCount; i++) {	
					if(paxWiseDiscountableTotal[i] > 0){						
						if(paxWiseDiscountableTotal[i] > paxEligibalDiscountAmount){
							anciDiscountAmount += paxEligibalDiscountAmount;
						} else {
							anciDiscountAmount += paxWiseDiscountableTotal[i];
						}
					}
				}
				
			}
			
		}
		
		return anciDiscountAmount;
	}
	
	/*
	 * Update ancillary total
	 */
	UI_Anci.updateAnciTotals = function (anciType,amount,action){
		var anciSum = UI_Anci.anciSummary;
		for(var i=0; i < anciSum.length; i++){
			if(anciSum[i].type == anciType){
				if(action=='ADD'){
					anciSum[i].subTotal += amount;
					UI_Anci.anciTotal += amount;
					
				}else if(action =='DEDUCT'){
					anciSum[i].subTotal -= amount;
					UI_Anci.anciTotal -= amount;
				}
			}
		}
		UI_Anci.updatePriceSummary();
	}
	/*
	 * Update ancillary total
	 */
	UI_Anci.updateAnciCounts = function (anciType,qty,action){
		var anciSum = UI_Anci.anciSummary;
		for(var i=0; i < anciSum.length; i++){
			if(anciSum[i].type == anciType){
				if(action=='ADD'){
					anciSum[i].qty += qty;
				}else if(action =='DEDUCT'){
					anciSum[i].qty -= qty;
				}
			}
		}
	}
	
	/*
	 * Dynamically update the price summary using selected ancillary
	 */
	UI_Anci.updatePriceSummary = function(){
		var meal = 0;
		if(UI_Container.isFOCmealEnabled == false) {
			meal = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.MEAL).subTotal || 0);
		}
		
		var seat = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.SEAT).subTotal || 0);
		
		var flexi = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.FLEXI).subTotal || 0);
		
		var insurance = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.INS).subTotal || 0);
		
		var baggage = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.BAGGAGE).subTotal || 0);
		
		var hala = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.HALA).subTotal || 0);
		
		var aptCharge = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.APTRANSFER).subTotal || 0);

		var ssrCharge = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.SSR).subTotal || 0);

        var currentMeal = 0;
        var currentSeat = 0;
        var currentFlexi = 0;
        var currentInsurance = 0;
        var currentBaggage = 0;
        var currentHala = 0;
        var currentAptCharge = 0;
        var currentSsrCharge = 0;

        if (UI_Container.isModifyAncillary()) {

        }

		UI_Container.updateAncillary({
            mealCharge:meal > currentMeal ? meal - currentMeal : 0,
            seatMapCharge:seat > currentSeat ? seat - currentSeat : 0,
            insuranceCharge:insurance > currentInsurance ? insurance - currentInsurance : 0,
            baggageCharge:baggage > currentBaggage ? baggage - currentBaggage : 0,
            halaCharge:hala > currentHala ? hala - currentHala : 0,
            apTransferCharge:aptCharge > currentAptCharge ? aptCharge - currentAptCharge : 0,
            flexiCharge:flexi > currentFlexi ? flexi - currentFlexi : 0,
            ssrCharge:ssrCharge > currentSsrCharge ? ssrCharge - currentSsrCharge : 0
            });
	}
	
	/* 
	 * Get ancilary summary for the given type if active
	 */
	UI_Anci.getAncillarySummary = function(type){
		var anciSum = UI_Anci.anciSummary;
		for(var i=0; i < anciSum.length; i++){
			if(anciSum[i].stat == 'ACT' && anciSum[i].type == type){	
				return anciSum[i];
			}
		}
		return {};
	}

    UI_Anci.getCurrentAnciPrice = function(type) {

    }

	/* 
	 * Build price summary
	 */
	UI_Anci.buildPriceSummary = function (){
		/* TODO handle if selected currency is not base currency */
		var anciSum = UI_Anci.anciSummary;
		var isBaseCur = true;
		var curr = UI_Anci.currency.base;
		var selCur = UI_Anci.currency.selected;
		isBaseCur = (curr==selCur);
		var totalFare =0;
		$('#trChargesSummary').parent().find('tr').filter(function() {			
			var regex = new RegExp('trChSummary_*');
	        return regex.test(this.id);
	    }).remove();
		sumTR = '<tr id="trChSummary_blank"><td><label ></label></td><td></td><td><div  class="fntDefault txtBold"></div></td></tr>';
		$('#trChargesSummary').after(sumTR);
		for(var i=0; i < anciSum.length; i++){
			if(anciSum[i].stat == 'ACT'){				
				var subT = anciSum[i].subTotal;
				var sumTR = '<tr id="trChSummary_'+i+'"><td><label >'+anciSum[i].name+'</label></td><td class="hdFontColor fntBold">:</td>'+
				'<td align="left"><div class="fntDefault txtBold">'+curr+'</div></td>'+
				'<td align="right"><div  class="fntDefault txtBold">'+ $.airutil.format.currency(subT)+'</div></td></tr>';
				$('#trChargesSummary').after(sumTR);
			}
		}	
		var grandTotal = $.airutil.format.currency(parseFloat(UI_Anci.fareQuote.totalPrice) + UI_Anci.anciTotal);
		$('#divAirFare').html( UI_Anci.fareQuote.totalFare);
		$('#divCharges').html(UI_Anci.fareQuote.totalTaxSurcharge);
		$('#divTotal').html(grandTotal);
		$('#flightTemplate div[name="anciCurrCode"]').html(curr);
		if(!isBaseCur){
			$('#trSelCurTotal').show();
			var selCurTotal = $.airutil.format.currency(parseFloat(UI_Anci.currency.exchangeRate) * grandTotal);
			$('#divSelCurTotal').html(selCurTotal);
			$('#divSelCur').html(selCur);
		}else{
			$('#trSelCurTotal').hide();
		}
	}
	/*
	 * return insurance
	 */
	UI_Anci.getInsurance = function(){
		return UI_Anci.selectedInsuranceQuotes;
	}
	/*
	 * CREATE PAX STRUCTURE 
	 */
	UI_Anci.generatePaxWiseAnci = function(){
		var paxList = $.airutil.dom.cloneObject(UI_Anci.getPaxArray(true));
		//var infList = $.airutil.dom.cloneObject(UI_Anci.jsonPaxInfants);
		var anciSeg = [];
		if(!UI_Container.makePayment){
			anciSeg = $.airutil.dom.cloneObject(UI_Anci.jsAnciSegModel);
		}
		var paxId = null;
		var baggageONDGroupId = null;
		//paxList = paxList.concat(infList);
		if(anciSeg != null){
			for(var i = 0 ; i < anciSeg.length ; i ++){
				if(UI_Anci.jsBaggageModel != null && UI_Anci.jsBaggageModel != undefined && UI_Anci.jsBaggageModel.length >= 0){
					for(var y=0;y<UI_Anci.jsBaggageModel.length;y++){
						if(UI_Anci.compareFlightRefNo(UI_Anci.jsBaggageModel[y].flightSegmentTO.flightRefNumber,
                            UI_Anci.jsAnciSeg[i].flightSegmentTO.flightRefNumber)){
							baggageONDGroupId = UI_Anci.jsBaggageModel[y].flightSegmentTO.baggageONDGroupId;
							break;
						}
					}
				}
				var pax = anciSeg[i].pax;
				var segInfo = { 
						segCode: anciSeg[i].segCode,
						flightRefNumber: anciSeg[i].flightRefNumber,
						returnFlag: UI_Anci.jsAnciSeg[i].flightSegmentTO.returnFlag,
						//isConnection: UI_Anci.isConnection, //FiX IE Issue
						baggageONDGroupId:baggageONDGroupId,
						pnrSegId: anciSeg[i].pnrSegId,
						flownSegment: anciSeg[i].flownSegment
				};
				for(var j = 0 ; j < paxList.length; j++){
					if(j<pax.length){
						paxList[j].anci[i]=pax[j];
						paxList[j].anci[i].segInfo = segInfo;
						var removeAnci = { seat: { seatNumber: '',seatCharge: 0	 },	 meal: { meals: [],	mealChargeTotal: 0,	mealQtyTotal: 0 },
												baggage: { baggages: [],baggageChargeTotal: 0, baggageQtyTotal: 0 },aps: {apss: [],	 apsChargeTotal: 0 }};
						
						paxList[j].removeAnci[i] = removeAnci;
	                
					}
				}
				baggageONDGroupId = null;
			}			
		}
		
		if(UI_Container.isModifyAncillary()){
			paxList = UI_Anci.populatedAddRemovedAnci(paxList);
		}
		
		return paxList;
	}
	
	UI_Anci.populatedAddRemovedAnci = function(paxList){

		 for (var i = 0; i < paxList.length; i++) {
			 var pax = paxList[i];
		        if (pax.currentAncillaries != null) {
		        	for (var j = 0; j < pax.anci.length; j++) {
		        		 for (var k = 0; k < pax.currentAncillaries.length; k++) {
		        			 
		        			 if (UI_Anci.compareFlightRefNo(
		                                 pax.anci[j].segInfo.flightRefNumber,
		                                 pax.currentAncillaries[k].flightSegmentTO.flightRefNumber)) {
		        				 
		        				 var fltRefNum = pax.currentAncillaries[k].flightSegmentTO.flightRefNumber;
		        				 var seat = {};
		        				 var meal = {};
		        				 var apService = {};
		        				 var baggage = {};
		        				 
		        				 if(UI_Anci.isAnciEnabled(fltRefNum, 'seat')){		        					 
		        					 seat = getAddRemovedSeat(pax.currentAncillaries[k].airSeatDTO,pax.anci[j].seat, anciModifiable("SEAT"));
		        				 }
		        				 
		        				 if(UI_Anci.isAnciEnabled(fltRefNum, 'meal')){		        					 
		        					 meal = getAddRemovedMelas(pax.currentAncillaries[k].mealDTOs,pax.anci[j].meal.meals, anciModifiable("MEAL"));
		        				 }
		        				 
		        				 if(UI_Anci.isAnciEnabled(fltRefNum, 'hala')){		        					 
		        					 apService = getAddRemovedAirportServices(pax.currentAncillaries[k].airportServiceDTOs,pax.anci[j].aps.apss, anciModifiable("AIRPORT_SERVICE"));
		        				 }
		        				 
		        				 if(UI_Anci.isAnciEnabled(fltRefNum, 'baggage')){		        					 
		        					 baggage = getAddRemovedBaggages(pax.currentAncillaries[k].baggageDTOs,pax.anci[j].baggage.baggages, anciModifiable("BAGGAGE"));
		        				 }
		        				 
		        				 
		        				 pax.anci[j].seat = seat.add;
		                         pax.anci[j].meal.meals = meal.add;
		                         pax.anci[j].baggage.baggages = baggage.add;
		                         pax.anci[j].aps.apss = apService.add;
		                         
		                         pax.removeAnci[j].seat = seat.remove;
		                         pax.removeAnci[j].meal.meals = meal.remove;
		                         pax.removeAnci[j].baggage.baggages = baggage.remove;
		                         pax.removeAnci[j].aps.apss = apService.remove;
		        				 break;
		        			 }
		        		 }
		        	}
		        }
		 }
		 return paxList;
	}


    function anciModifiable(anciType) {

        var isAnciModifiable = false;
        var modifyingAnci = $.parseJSON($('#selectedAncillary').val());

        for (var a = 0; a < modifyingAnci.length; a++) {
            if (modifyingAnci[a] == anciType) {
                isAnciModifiable = true;
                break;
            }
        }

        return isAnciModifiable;
    }
	
	function getAddRemovedSeat(currentSeat, newSeat, modifiable){
		var remSeat = {
                seatNumber: '',
                seatCharge: 0
            };
		var  seat = {
                seatNumber: '',
                seatCharge: 0
            };
		//add a seat to a pax with no seats in original res
        if (modifiable) {
            if (currentSeat == null && newSeat != null) {
                seat = newSeat;

                // remove seat
            } else if (currentSeat != null && newSeat != null && newSeat.seatNumber == "") {
                remSeat = {
                    seatNumber:currentSeat.seatNumber,
                    seatCharge:currentSeat.seatCharge
                };
                //change the current seat to another one
            } else if (currentSeat != null && newSeat != null) {
                if (currentSeat.seatNumber != newSeat.seatNumber) {
                    remSeat = {
                        seatNumber:currentSeat.seatNumber,
                        seatCharge:currentSeat.seatCharge
                    };
                    seat = newSeat;
                }
            }
        }
		return {
	        add: seat,
	        remove: remSeat
	    };
	}
	
	function getAddRemovedMelas(currentMeals, newMeals, modifiable){
		var removedMeals = [];
		var out = [];

        if (modifiable) {
            // no new meals but have current meals so this needs to be added in to removed list
            if (newMeals.length == 0 && currentMeals != null && currentMeals.length != 0) {
                for (var i = 0; i < currentMeals.length; i++) {
                    var remMeal = {
                        mealCharge:currentMeals[i].mealCharge,
                        mealCode:currentMeals[i].mealCode,
                        mealName:currentMeals[i].mealName,
                        mealQty:currentMeals[i].soldMeals
                    };
                    removedMeals.push(remMeal);
                }
            }
            // no current meals but has newMeals so this needed to added in to out list
            else if (currentMeals.length == 0 && newMeals.length != 0) {
                out = newMeals;
            }

            else if (currentMeals != null && currentMeals.length != null && newMeals != null && newMeals.length != null) {
                var commonMeals = {};

                for (var i = 0; i < currentMeals.length; i++) {
                    for (var j = 0; j < newMeals.length; j++) {

                        if ((newMeals[j].mealCode == currentMeals[i].mealCode && currentMeals[i].soldMeals == newMeals[j].mealQty)) {
                            commonMeals[newMeals[j].mealCode] = true;
                        }
                    }
                }

                // set newly added meals
                for (var i = 0; i < newMeals.length; i++) {
                    if (commonMeals[newMeals[i].mealCode] == null || !commonMeals[newMeals[i].mealCode]) {
                        out[out.length] = newMeals[i];
                    }
                }

                //set removed meals from existing meals
                for (var i = 0; i < currentMeals.length; i++) {
                    if (commonMeals[currentMeals[i].mealCode] == null || !commonMeals[currentMeals[i].mealCode]) {
                        var remMeal = {
                            mealCharge:currentMeals[i].mealCharge,
                            mealCode:currentMeals[i].mealCode,
                            mealName:currentMeals[i].mealName,
                            mealQty:currentMeals[i].soldMeals
                        };
                        removedMeals.push(remMeal);
                    }
                }
            } else {
                out = newMeals;
            }
        }
        
        return {
            add: out,
            remove: removedMeals
        };
        
	}
	
	function getAddRemovedAirportServices(currentServices, newServices, modifiable){
		
		var out = [];
		var removedServices = [];
		// no new services but have current services

        if (modifiable) {
            if (newServices.length == 0 && currentServices != null && currentServices.length != 0) {
                for (var i = 0; i < currentServices.length; i++) {
                    var remAirportSearvie = {
                        airport:currentServices[i].airportCode,
                        charge:currentServices[i].serviceCharge,
                        description:currentServices[i].ssrDescription,
                        ssrCode:currentServices[i].ssrCode
                    };
                    removedServices.push(remAirportSearvie);
                }
                //no current services but have new services
            } else if (currentServices.length == 0 && newServices != null && newServices.length != 0) {
                out = newServices;

            } else if (currentServices != null && currentServices.length != null && newServices != null && newServices.length != null) {
                var commonServices = {};

                for (var i = 0; i < currentServices.length; i++) {
                    for (var j = 0; j < newServices.length; j++) {
                        if (newServices[j].ssrCode == currentServices[i].ssrCode && newServices[j].airport == currentServices[i].airportCode) {
                            commonServices[newServices[j].ssrCode + newServices[j].airport] = true;
                        }
                    }
                }

                //set newly added services
                for (var i = 0; i < newServices.length; i++) {
                    if (commonServices[newServices[i].ssrCode + newServices[i].airport] == null || !commonServices[newServices[i].ssrCode + newServices[i].airport]) {
                        out[out.length] = newServices[i];
                    }
                }

                //set removed services from existing services
                for (var i = 0; i < currentServices.length; i++) {
                    if (commonServices[currentServices[i].ssrCode + currentServices[i].airportCode] == null || !commonServices[currentServices[i].ssrCode + currentServices[i].airportCode]) {
                        var remAirportSearvie = {
                            airport:currentServices[i].airportCode,
                            charge:currentServices[i].serviceCharge,
                            description:currentServices[i].ssrDescription,
                            ssrCode:currentServices[i].ssrCode
                        };
                        removedServices.push(remAirportSearvie);
                    }
                }

            } else {
                out = newServices;
            }
        }
        
        return {
            add: out,
            remove: removedServices
        };
	}
	
	function getAddRemovedBaggages(currentBaggage, newBaggage, modifiable){
		var removedBaggages = [];
		var out = [];

        if (modifiable) {
            if (currentBaggage != null && currentBaggage.length != null && newBaggage != null && newBaggage.length != null) {
                var alreadyExistingBaggages = {};
                var removedBaggagesMap = {};
                for (var i = 0; i < currentBaggage.length; i++) {
                    for (var j = 0; j < newBaggage.length; j++) {
                        if ((newBaggage[j].baggageName == currentBaggage[i].baggageName)) {
                            alreadyExistingBaggages[newBaggage[j].baggageName] = true;
                        } else {
                            if (!removedBaggagesMap[currentBaggage[i].baggageName]) {
                                var remBaggage = {
                                    baggageCharge:currentBaggage[i].baggageCharge,
                                    baggageName:currentBaggage[i].baggageName,
                                    baggageQty:currentBaggage[i].soldPieces,
                                    ondBaggageChargeId:currentBaggage[i].ondBaggageChargeId
                                };
                                removedBaggages.push(remBaggage);
                            }
                            removedBaggagesMap[currentBaggage[i].baggageName] = true;
                        }
                    }
                }


                for (var i = 0; i < newBaggage.length; i++) {
                    if (alreadyExistingBaggages[newBaggage[i].baggageName] == null || !alreadyExistingBaggages[newBaggage[i].baggageName]) {
                        out[out.length] = newBaggage[i];
                    }
                }

            } else {
                out = newBaggage;
            }
        }
	    
	    return {
            add: out,
            remove: removedBaggages
        };
	   
	}
	
	UI_Anci.compareFlightRefNo = function (first, second) {
	    var fArr = first.split('$');
	    var sArr = second.split('$');
	    if (fArr.length > 1) {
	        first = fArr[2];
	    }
	    if (sArr.length > 1) {
	        second = sArr[2];
	    }
	    return (first == second);
	}
	
	
	/**
	 * PRIVATE FUNCTIONS
	 */
	/*
	 * private FN get Date for string of type 2010-03-08T05:15:00
	 */
	function _getDate (dstr){
		  var a = dstr.split('T');
		  var d = a[0].split('-');
		  var t = a[1].split(':');
		  return new Date(d[0],parseInt(d[1],10)-1,d[2],t[0],t[1],t[2]);
	}
	
	/*
	 * Anci segment comparator for sort
	 */
	function segmentComparator(first ,second){
		var dF = _getDate(first.flightSegmentTO.departureDateTime);
		var dS = _getDate(second.flightSegmentTO.departureDateTime);
		return (dF.getTime()-dS.getTime());
	}
	
	/*
	 * Row group comparator
	 */
	UI_Anci.rowGroupComparator = function (first ,second){

		var firstRowGroupId = 0;
		var secondRowGroupId = 0;
		var firstRowGroupId = undefined;
		var secondTopElement = undefined;

		if (first.length != undefined) {
			var index = 0;
			do {
				firstTopElement = first[index];
				index ++;
			} while (firstTopElement == undefined);
			firstRowGroupId = firstTopElement.rowGroupId;
		} else {
			firstRowGroupId = first.rowGroupId;
		}

		if (second.length != undefined) {
			var index = 0;
			do {
				secondTopElement = second[index];
				index ++;
			} while (secondTopElement == undefined);
			secondRowGroupId = secondTopElement.rowGroupId;
		} else {
			secondRowGroupId = second.rowGroupId;
		}

		var numF = parseInt(first.rowGroupId,10);
		var numS = parseInt(second.rowGroupId,10);
		return (numF-numS);
	}
	
	/*
	 * Row comparator
	 */
	UI_Anci.rowComparator = function (first ,second){
		var numF = parseInt(first.rowNumber,10);
		var numS = parseInt(second.rowNumber,10);
		return (numF-numS);
	}
	
	/*
	 * Column group comparator
	 */
	UI_Anci.columnGroupComparator = function (first ,second){
		var numF = parseInt(first.columnGroupId,10);
		var numS = parseInt(second.columnGroupId,10);
		return (numF-numS);
	}
	
	/*
	 * Anci segment DTO comparator for sort
	 */
	function segmentDTOComparator(first ,second){
		var dF = _getDate(first.flightSegmentDTO.departureDateTime);
		var dS = _getDate(second.flightSegmentDTO.departureDateTime);
		return (dF.getTime()-dS.getTime());
	}
	
	/*
	 * 
	 *"G9$SHJ/SAW$198301$20100310202000$20100310231500$5724"
	 */
	function segComparatorByFltRef(first ,second){
		var dF = parseInt(first.flightRefNumber.split('$')[3]);
		var dS = parseInt(second.flightRefNumber.split('$')[3]);
		return (dF-dS);
	}
	
	/*
	 * Compare segments by departure time (in milllis)
	 */
	function segComparatorByDepartTime (first ,second){
		return (first.departureTimeLong-second.departureTimeLong);
	}
	
	/*
	 * Isle comparator for  sort
	 */
	function seatIsleComparator(first,second){
		var numF = parseInt(first.rowNum,10);
		var numS = parseInt(second.rowNum,10);
		return (numF-numS);
	}
	/*
	 * Row comparator for  sort
	 */
	function seatRowComparator(first, second) {
		var index = 0;
		var firstTopElement = undefined;
		do {
			firstTopElement = first[index];
			index ++;
		} while (firstTopElement == undefined);
		
		index = 0;
		var secondTopElement = undefined;
		do {
			secondTopElement = second[index];
			index ++;
		} while (secondTopElement == undefined);
			
	    var numF = parseInt(firstTopElement.rowNum, 10);
	    var numS = parseInt(secondTopElement.rowNum, 10);
	    return (numF - numS);
	}
	
	/*
	 * Compare flt reference numbers
	 */
	function compareFltRefNos(fltRefFirst,fltRefSecond){
		var a1 = fltRefFirst.split('$');
		var a2 = fltRefSecond.split('$');
		var no1, no2;
		if(a1.length >= 3){
			no1 = a1[2];
		}else{
			no1 = fltRefFirst;
		}
		if(a2.length >= 3){
			no2 = a2[2];
		}else{
			no2 = fltRefSecond;
		}
		return (no1 == no2)
	}
	
	
	function checkSegsConnection(inob,length,exist){
		var str ='';
		if (exist){
			if (length>1){
				str +='<div class="sub-head-inob"><label class="hdFontColor fntBold">'+inob +'&nbsp; ('+UI_Container.labels.lblConnecting+')</label></div>';
			}else if(length<=1){
				str +='<div class="sub-head-inob"><label class="hdFontColor fntBold">'+inob +'</label></div>';
			}
		}
		return str;
	}
	
	function isBaggageSelectedForCurrentBooking(){
		isPreviousBookingWithNoBaggages();
		var noOfSegmentsNotHaveCurrentAnciBaggages = 0;
		var noOfSegmentsHaveCurrentAnciBaggages = 0;
		if(UI_Anci.jsBaggageModel != undefined && UI_Anci.jsBaggageModel.length > 0){
			for(var k=0;k<UI_Anci.jsBaggageModel.length;k++){
				if (UI_Anci.jsBaggageModel[k].flightSegmentTO.baggageONDGroupId != null) {
					if(UI_Anci.isBaggageExistInCurrentAncillary(UI_Anci.jsBaggageModel[k].flightSegmentTO.flightRefNumber)){
						noOfSegmentsHaveCurrentAnciBaggages++;
					}else{
						noOfSegmentsNotHaveCurrentAnciBaggages++;
					}
				} else {
					if(UI_Anci.isBaggageExistInCurrentSegment(UI_Anci.jsBaggageModel[k].flightSegmentTO.segmentCode)){
						noOfSegmentsHaveCurrentAnciBaggages++;
					}else{
						noOfSegmentsNotHaveCurrentAnciBaggages++;
					}
				}
			}
		}else{
			return;
		}
		if(noOfSegmentsHaveCurrentAnciBaggages==UI_Anci.jsBaggageModel.length){
			UI_Anci.isBaggagesSelectedForCurrentBooking = true;
		}else if(noOfSegmentsNotHaveCurrentAnciBaggages > 0){
			UI_Anci.isBaggagesSelectedForCurrentBooking = false;
		}
	}
	
	UI_Anci.isBaggageExistInCurrentAncillary = function(flightRefNumber){
		
		var isBaggageExist = false;
		for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
			var currAnci = UI_Anci.jsonPaxAdults[i].currentAncillaries;
			for(var k = 0 ; k < currAnci.length; k++){
			
				if(currAnci[k].flightSegmentTO.flightRefNumber == flightRefNumber){
					isBaggageExist = true;
					break;
				} 
			}
		}
		return isBaggageExist;
	}
	
	UI_Anci.isBaggageExistInCurrentSegment = function(segCode){
		
		var isBaggageExist = false;
		for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
			var currAnci = UI_Anci.jsonPaxAdults[i].currentAncillaries;
			for(var k = 0 ; k < currAnci.length; k++){
			
				if(currAnci[k].flightSegmentTO.segmentCode == segCode){
					isBaggageExist = true;
					break;
				} 
			}
		}
		return isBaggageExist;
	}
	
	function isPreviousBookingWithNoBaggages(){
		
		for(var i = 0 ; i < UI_Anci.jsonPaxAdults.length; i++){
			var currAnci = UI_Anci.jsonPaxAdults[i].currentAncillaries;
			var y=0;
			if(currAnci == undefined || currAnci.length == 0){
				UI_Anci.isPreviousBookingWithNoBaggages = true;
				break;
			}
			for(var k = 0 ; k < currAnci.length; k++){
				if(currAnci[k].baggageDTOs == undefined || (currAnci[k].baggageDTOs.length == 0)){
					y++;
				}
			}
			if(y==currAnci.length){
				UI_Anci.isPreviousBookingWithNoBaggages = true;
				break;
			}
		}
	}
	UI_Anci.getOndBaggageInfoObj = function() {
		var obj = {rphOndMap:{}, skipRphs:[], ondCodes:[],firstSegId:[]};
		
		for (var i = 0 ; i < UI_Anci.jsBaggageModel.length; i++) {
			var baggageONDGroupId = UI_Anci.jsBaggageModel[i].flightSegmentTO.baggageONDGroupId;
			
			if (baggageONDGroupId != null && baggageONDGroupId != "") {
				var arr = UI_Anci.jsBaggageModel[i].flightSegmentTO.segmentCode.split("/");
				
				if(obj.firstSegId[baggageONDGroupId] == null){
					obj.firstSegId[baggageONDGroupId] = i;
				}
				obj.rphOndMap[UI_Anci.jsBaggageModel[i].flightSegmentTO.flightRefNumber] = baggageONDGroupId;
				if (obj.ondCodes[baggageONDGroupId] != null) {
					obj.skipRphs[obj.skipRphs.length] = UI_Anci.jsBaggageModel[i].flightSegmentTO.flightRefNumber;
				} 
				
				if (obj.ondCodes[baggageONDGroupId] == null){
					obj.ondCodes[baggageONDGroupId] = UI_Anci.jsBaggageModel[i].flightSegmentTO.segmentCode;
				} else {
					obj.ondCodes[baggageONDGroupId] = obj.ondCodes[baggageONDGroupId].split("/")[0]+ "/" + arr[arr.length -1];
				}
			}
		}

		return obj;
	}
	
	UI_Anci.getBaggageONDGrpIdWiseCount = function(currentBaggageGrpONDId){
		var count = 0;
		for (var i= 0; i< UI_Anci.jsBaggageModel.length; i++){
			var grpId = UI_Anci.jsBaggageModel[i].flightSegmentTO.baggageONDGroupId;
			if (grpId != "" && grpId == currentBaggageGrpONDId){
				count++;
			}
		}
		
		return count;
	}
	

	UI_Anci.getBaggageSegIndexSegmentCodeWise = function(segmentCode){
		for(var i=0;i<UI_Anci.jsBaggageModel.length;i++){
			if(segmentCode == UI_Anci.jsBaggageModel[i].flightSegmentTO.segmentCode){
				return i;
			}
		}
	}
	
	UI_Anci.convertAmountToSelectedCurrency = function(amount){
		var exchangeRate=UI_Anci.currency.exchangeRate == null ? 1 : UI_Anci.currency.exchangeRate;
		return $.airutil.format.currency(amount * exchangeRate);
	}
	
	UI_Anci.getFreeServiceDiscount = function(){
		return UI_Anci.freeServiceDiscount;
	}
	
	UI_Anci.calculateExtrachargeForAvailableBundledFare =  function(ondAnciSummary , ondBundledFare, ondSequence) {
		//holds the cheaper Bundlefare than sub total of the selected anci
		var cheaperBundleFares =  new Array();
		
		var ondWiseBundleFareSelected = _parseJSONStringToObject($("#resOndBundleFareStr").val());
		if(ondWiseBundleFareSelected != undefined && ondWiseBundleFareSelected != null &&
				ondWiseBundleFareSelected[ondSequence] != undefined &&
				ondWiseBundleFareSelected[ondSequence] != null 
				&& ondWiseBundleFareSelected[ondSequence] != 'null'
				&& ondWiseBundleFareSelected[ondSequence] != ''){
        	return cheaperBundleFares;
        }
		
		//matching Selected anci types with bundlefare freeservices names
		var bundleFareAnciMatchWithSelectedAnci = {"BAGGAGE":"BAGGAGES","MEAL": "MEALS","SEAT_MAP":"SEAT_MAP"};		
		//summary of selected Anci
		var anciSummary = ondAnciSummary ;
		//convert available bundlefare searchParam to a JS object
		var avilableBundleFares = UI_Anci.availableBundleFares;
		var outboundAvailableBundleFares =  ondBundledFare;
		//for holds the Types of selected Anci types
		var selectedAnciTypeSummary =  new Array();
		// Sub total of selected Anci
		var selectedAnciTotal = 0 ;		
		var bundleFareExtraCost = new Array();	
		var bundleFareExtaraCost =  new Array();
		if (ondBundledFare.length >0 && ondAnciSummary != undefined && ondAnciSummary != null){
			$.each(ondAnciSummary, function(index, summaryAnci){
				if(summaryAnci.subTotal>0){
					var anciTypeName  =  summaryAnci.type;
					selectedAnciTypeSummary[selectedAnciTypeSummary.length] =  anciTypeName;
					selectedAnciTotal += summaryAnci.subTotal;
				
				}
			});
				
			
			//Add Flexi Charge for OnD
			if(UI_Anci.ondAvailbleFlexi != null && (UI_Anci.jsAnciFlexiAdded[ondSequence] || UI_Anci.jsAnciFlexiUAdded[ondSequence])){
				var ondFlexiCharge = _parseJSONStringToObject($("#resOndwiseFlexiChargesForAnci").val());
				var flexiChgStr = ondFlexiCharge[ondSequence];
				if(flexiChgStr != undefined && flexiChgStr != null && flexiChgStr != ''){
					selectedAnciTotal += parseFloat(flexiChgStr);
				}
			}
		
			var cheaperBundleFares = new Array() ;
			$.each(outboundAvailableBundleFares , function(index,availableBundleFare){
				if(availableBundleFare != null){					
					var bundleFare =  availableBundleFare[0];
					
					var bundleFareFeeForSelectedAnci = bundleFare.bundleFareFee * UI_Anci.jsonPaxAdults.length;
                	if(selectedAnciTotal < bundleFareFeeForSelectedAnci){
                		var bundleFare =  availableBundleFare[0];
                		if(ondAnciSummary != undefined && ondAnciSummary != null){          			
                			$.each(ondAnciSummary, function(index, summaryAnci){
                				var isSelectedAnciAvalibaleInBundleFare = false;
                				$.each(bundleFare.bundleFareFreeServices, function(index, freeService) {
                					var feeBundlefareService =  freeService;
                					if(bundleFareAnciMatchWithSelectedAnci[feeBundlefareService] == summaryAnci.type){
                						isSelectedAnciAvalibaleInBundleFare = true;
                					}	
                				});
                				if(isSelectedAnciAvalibaleInBundleFare != true){
                					bundleFareFeeForSelectedAnci += summaryAnci.subTotal;
                				}
                			});
                			if(bundleFareFeeForSelectedAnci!= 0 && bundleFareFeeForSelectedAnci > selectedAnciTotal){
                				cheaperBundleFares[cheaperBundleFares.length]= bundleFare.bundleFareName ;
                			}
                		}
                	}					
				}
			});
		}
		return cheaperBundleFares;
	}
	
	
	UI_Anci.CheckForCheaperBundleFare = function(){
		var cheaperBundleFares =  new Array();
		
		if(UI_Container.isModifyAncillary()){
			cheaperBundleFares[UI_Anci.OND_SEQUENCE.OUT_BOUND] =  new Array();
			cheaperBundleFares[UI_Anci.OND_SEQUENCE.IN_BOUND] =  new Array();
		} else {			
			var bundleFareAnciMatchWithSelectedAnci = {"BAGGAGE":"BAGGAGES","MEAL": "MEALS","SEAT_MAP":"SEAT_MAP"};
			var anciSummary = UI_Anci.anciSummary ;
			
			if(UI_Anci.availableBundleFares != "" && UI_Anci.availableBundleFares != null) {
				
				var ondViceAnciSummary = UI_Anci.createOndViseAnciSummary();
				
				//Adding ond wise support
				$.each(UI_Anci.availableBundleFares, function(ondSeq, bundledFares){
					if (ondSeq < ondViceAnciSummary.length){
						var ondAnciSummary = ondViceAnciSummary[ondSeq];
						if(bundledFares != null && ondAnciSummary != undefined){					
							var ondCheaperBundledFares = UI_Anci.calculateExtrachargeForAvailableBundledFare(ondAnciSummary, bundledFares, ondSeq);
							cheaperBundleFares[ondSeq] =  ondCheaperBundledFares ;
						}
					}
				});
								
			} else {
				cheaperBundleFares[UI_Anci.OND_SEQUENCE.OUT_BOUND] =  new Array();
				cheaperBundleFares[UI_Anci.OND_SEQUENCE.IN_BOUND] =  new Array();
			}
		}
		
		return cheaperBundleFares;
	} 
	
	UI_Anci.createSegmentIntoOndMap = function(){
		var segmentOndMap  =  new Array();
		
		$.each(UI_Anci.jsAnciSeg,function(index,segmentInfo){
			var flightSegmentTO = segmentInfo.flightSegmentTO;
			var ondSeq  =  flightSegmentTO.ondSequence;
			var segmentSeq  =  flightSegmentTO.segmentSequence;
			if(index == 0){
				segmentOndMap[segmentSeq] = ondSeq;
			}
		});
		return segmentOndMap;
	}
	
	UI_Anci.createOndViseAnciSummary = function(){
		var ondAnciSummaryMap  =  new Array();
		var ondViseAnciSummaryMap = new Array();
		ondViseAnciSummaryMap[UI_Anci.OND_SEQUENCE.OUT_BOUND] ={};
		ondViseAnciSummaryMap[UI_Anci.OND_SEQUENCE.IN_BOUND] ={};
		var ondSegPaxApsCharge = 0;
		var ondSegPaxBaggageCharge = 0;
		var ondSegPaxMealCharge = 0;
		var ondSegPaxSeatCharge = 0;
		var ondSegPaxSsrCharge = 0;
		$.each(UI_Anci.jsAnciSegModel ,function(index,segmentAnciModle){
			var segmentPax = segmentAnciModle.pax;
			var ondSeq  =  UI_Anci.OND_SEQUENCE.OUT_BOUND ;
			if(segmentAnciModle.returnFlag){
				ondSeq =  UI_Anci.OND_SEQUENCE.IN_BOUND;
			}
			
			var segPaxApsCharge = 0;
			var segPaxBaggageCharge = 0;
			var segPaxMealCharge = 0;
			var segPaxSeatCharge = 0;
			var segPaxSsrCharge = 0;
			$.each(segmentPax,function(paxindex,pax){
				segPaxApsCharge  += parseFloat(pax.aps.apsChargeTotal);
				segPaxBaggageCharge  += parseFloat(pax.baggage.baggageChargeTotal);
				segPaxMealCharge  += parseFloat(pax.meal.mealChargeTotal);
				segPaxSeatCharge  += parseFloat(pax.seat.seatCharge);
				segPaxSsrCharge  += parseFloat(pax.ssr.ssrChargeTotal);
			});
			
			if(ondAnciSummaryMap[ondSeq] == undefined || ondAnciSummaryMap[ondSeq] == null){
	        		ondAnciSummaryMap[ondSeq] = {
	        	        apsTotal: 0,
	        	        baggageTotal: 0,
	        	        mealTotal: 0,
	        	        seatTotal: 0,
	        	        ssrTotal: 0
	        	    };
	        }
			
			segPaxApsCharge = ondAnciSummaryMap[ondSeq].apsTotal + segPaxApsCharge;
			segPaxBaggageCharge = ondAnciSummaryMap[ondSeq].baggageTotal + segPaxBaggageCharge;
			segPaxMealCharge = ondAnciSummaryMap[ondSeq].mealTotal + segPaxMealCharge;
			segPaxSeatCharge = ondAnciSummaryMap[ondSeq].seatTotal + segPaxSeatCharge;
			segPaxSsrCharge = ondAnciSummaryMap[ondSeq].ssrTotal + segPaxSsrCharge;
			ondAnciSummaryMap[ondSeq] = {apsTotal:segPaxApsCharge, baggageTotal :segPaxBaggageCharge ,
					mealTotal :segPaxMealCharge , seatTotal:segPaxSeatCharge , ssrTotal: segPaxSsrCharge};
		});
		
		$.each(ondAnciSummaryMap, function(ondSeq, anciObj){
			if(anciObj == undefined || anciObj == null){
				ondViseAnciSummaryMap[ondSeq] = {
						0: {
							type: 'APS',
							subTotal: 0
						},
						1: {
							type: 'BAGGAGES',
							subTotal: 0
						},
						2: {
							type: 'MEALS',
							subTotal: 0
						},
						3: {
							type: 'SEAT_MAP',
							subTotal: 0
						},
						4: {
							type: 'SSR',
							subTotal: 0
						}
				};
			} else {				
				ondViseAnciSummaryMap[ondSeq] = {
						0: {
							type: 'APS',
							subTotal: anciObj.apsTotal
						},
						1: {
							type: 'BAGGAGES',
							subTotal: anciObj.baggageTotal
						},
						2: {
							type: 'MEALS',
							subTotal: anciObj.mealTotal
						},
						3: {
							type: 'SEAT_MAP',
							subTotal: anciObj.seatTotal
						},
						4: {
							type: 'SSR',
							subTotal: anciObj.ssrTotal
						}
				};
			}
	    })
		
		return ondViseAnciSummaryMap;
	}
	
	
	
	
	UI_Anci.getCheaperBundleFareAvailableMessage =  function(cheaperBundleFares) {
		var strChepBundleFareMSG  = "";
		if((cheaperBundleFares[UI_Anci.OND_SEQUENCE.OUT_BOUND] != undefined && cheaperBundleFares[UI_Anci.OND_SEQUENCE.OUT_BOUND].length > 0 ) 
				|| (cheaperBundleFares[UI_Anci.OND_SEQUENCE.IN_BOUND] != undefined && cheaperBundleFares[UI_Anci.OND_SEQUENCE.IN_BOUND].length > 0)){
				
			strChepBundleFareMSG = UI_Anci.jsonLabel['lblChepBundleFareMSG'];
			if(typeof cheaperBundleFares[UI_Anci.OND_SEQUENCE.OUT_BOUND] != "undefined"
					&& cheaperBundleFares[UI_Anci.OND_SEQUENCE.OUT_BOUND].length > 0){
				strChepBundleFareMSG += '\n <b>' + UI_Anci.jsonLabel['lblOutbound'] + '</b> \n';
				$.each(cheaperBundleFares[UI_Anci.OND_SEQUENCE.OUT_BOUND],function(index,cheapBundleFare){
					strChepBundleFareMSG += cheapBundleFare +" , ";
					
				});
			}
			
			if(typeof cheaperBundleFares[UI_Anci.OND_SEQUENCE.IN_BOUND] != "undefined"
					&& cheaperBundleFares[UI_Anci.OND_SEQUENCE.IN_BOUND].length > 0){
				strChepBundleFareMSG += '\n<b>' +UI_Anci.jsonLabel['lblInbound'] + '</b>\n';
				$.each(cheaperBundleFares[UI_Anci.OND_SEQUENCE.IN_BOUND],function(index,cheapBundleFare){
					strChepBundleFareMSG += cheapBundleFare +" , ";
				});
			}
		}
		return strChepBundleFareMSG;
	}
	
	UI_Anci.manipulateFlexiSelectionParam =  function() {
		if(UI_Anci.jsAnciFlexiAdded != {} && UI_Anci.jsAnciFlexiUAdded != {}){
			
			var ondTotalFlexiChargeObj = _parseJSONStringToObject($("#resOndwiseFlexiChargesForAnci").val());
			var ondFlexiSelection = {};
			
			
			_isFlexiSelected = function(ondSeq){
				if(UI_Anci.jsAnciFlexiAdded[ondSeq] || UI_Anci.jsAnciFlexiUAdded[ondSeq]){
					if(UI_Anci.jsAnciFlexiAdded[ondSeq]){
						UI_Container.fareQuoteOndFlexiCharges[ondSeq] = ondTotalFlexiChargeObj[ondSeq];
					}
					return true;
				}
				
				return false;
			}
			
			UI_Anci.updateAnciTotals(UI_Anci.anciType.FLEXI, parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.FLEXI).subTotal || 0),'DEDUCT');
			
			if(UI_Anci.ondAvailbleFlexi != null){				
				$.each(UI_Anci.ondAvailbleFlexi, function(ondSeq, availability){
					var ondFlexiSelected = availability && _isFlexiSelected(ondSeq);
					ondFlexiSelection[ondSeq] = ondFlexiSelected;
					if(ondFlexiSelected){
						UI_Anci.updateAnciTotals(UI_Anci.anciType.FLEXI, parseFloat(ondTotalFlexiChargeObj[ondSeq]),'ADD');
					}
				});
			}
			
			UI_Anci.updateJnTax();
			
			//this is for use in paymennt Page
			UI_Container.resFlexiUserSelectionCharge = parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.FLEXI).subTotal || 0);
			//this is for use in anci page
			UI_Anci.updatePriceSummary();
			
			$("#resOndSelectedFlexiStr").val($.toJSON(ondFlexiSelection));				
		}
	}
	
	UI_Anci.isFlexiSelectionEnablesInAnciPage =  function(intAnciAvailability) {
		var enableFlexiInAnci =  false ; 
		$.each(intAnciAvailability,function (index,interlineAnciAvilabilityObj){
			$.each(interlineAnciAvilabilityObj.ancillaryStatusList,function(lstIndex,anciStatus){
				if((UI_Anci.anciType.FLEXI == anciStatus.ancillaryType.ancillaryType) && anciStatus.available){
					enableFlexiInAnci = anciStatus.available;
					return false;
				}
			});	
			if(enableFlexiInAnci) {
				return false;
			}	
		});
		return enableFlexiInAnci;
	}

	UI_Anci.reBuildInsuranceWithSecondaryPolicy = function() {
		if(UI_Anci.isPOPUPPlanAvailable()){
			UI_Anci.buildInsuranceMainProduct(UI_Anci.insuranceObj,false);
		}
		
	}

    UI_Anci.resolvePaxIdAndSegId = function(paxIndex, segIndex) {
        var retVal = {};
        
        if(UI_Anci.paxWiseAnci != null){
	        var travellerRefNo = UI_Anci.jsonPaxAdults[paxIndex].travelerRefNumber;
	        var flightRefNo = UI_Anci.jsAnciSeg[segIndex].flightSegmentTO.flightRefNumber;
	
	        $.each(UI_Anci.paxWiseAnci, function(index1, anciPax) {
	            if (anciPax.travellerRefNo == travellerRefNo) {
	                $.each(anciPax.selectedAncillaries, function(index2, anciPaxSeg) {
	                    if (compareFltRefNos(anciPaxSeg.flightSegmentTO.flightRefNumber, flightRefNo)) {
	                        retVal = {paxIndex : index1, segIndex : index2};
	                    }
	                });
	            }
	        });
        }

        return retVal;
    }

    UI_Anci.populateSelectedSeats = function() {
    	
        var paxIndex;
        var segIndex;
        var pax;
        var seg;
        var mObj;
        var rObj;
        var seatRefObj;
        var anciModelMapping;
        var anciModel;
        var seatModelIndex;        
        
        for (paxIndex = 0; paxIndex < UI_Anci.jsonPaxAdults.length; paxIndex++) {
            pax = UI_Anci.jsonPaxAdults[paxIndex];
            for (segIndex = 0; segIndex < UI_Anci.jsAnciSeg.length; segIndex++) {

                if(!UI_Anci.jsAnciSeg[segIndex].seat){
                    continue;
                }

                seatModelIndex = UI_Anci.getSeatSegIndex(segIndex);

            	if(seatModelIndex == null){
            		continue;
            	}
            	
            	var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.SEAT_MAP, null, UI_Anci.getOndSequence(segIndex));
            	
                seg = UI_Anci.jsAnciSeg[segIndex].flightSegmentTO;

                anciModelMapping = UI_Anci.resolvePaxIdAndSegId(paxIndex, segIndex);

                if (UI_Anci.paxWiseAnci.length - 1 >= anciModelMapping.paxIndex &&
                    UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries.length - 1 >= anciModelMapping.segIndex) {

                    anciModel =  UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries[anciModelMapping.segIndex];

                    if (anciModel.airSeatDTO != null && anciModel.airSeatDTO.seatNumber != null && anciModel.airSeatDTO.seatNumber != '') {

                    	// In modification flow, only allowed if the seats were able to reprotect.
                    	if(!UI_Container.isModifySegment() || (UI_Container.isModifySegment() 
	                			&& ((typeof UI_Anci.isSeatReprotected[paxIndex] !== 'undefined') ? UI_Anci.isSeatReprotected[paxIndex] : true))){	
                    	
	                        seatRefObj = {fltRefNo:seg.flightRefNumber, seatNumber:anciModel.airSeatDTO.seatNumber};
	
	                        rObj = {paxId:paxIndex, segId:segIndex};
	                        mObj = {paxId:paxIndex, paxType:pax.paxType, refObj:seatRefObj, segId:segIndex};
	
	                        UI_Anci.removeSeatPax(rObj);
	                        UI_Anci.markSeat(mObj);
	
	
	                        var seatCharge = UI_Anci.convertAmountToSelectedCurrency(anciModel.airSeatDTO.seatCharge);
	                        var seatNumber = anciModel.airSeatDTO.seatNumber;
	
	                        var refObj = '{fltRefNo:\"' + seg.flightRefNumber + '\",seatNumber:\"' + seatNumber + '\"}';
	                        strDesc = "<a href='' onmouseover='UI_Anci.seatOnMouseOver(event,"+refObj+"); return false;'  onmouseout='UI_Anci.seatOnMouseOut("+refObj+"); return false;' onclick='return UI_Anci.seatOnClick("+refObj+"); return false;' class='txtSmall noTextDec'>&nbsp;&nbsp;&nbsp;&nbsp;<\/a>";								
	                        
	                		var applyFormatter = UI_Anci.isApplyDecorator(seatCharge,isFreeService);                		
	                		if(applyFormatter){
	                			seatCharge = UI_Anci.freeServiceDecorator;	
	                		}
	
							$("#tdSeat_" + seatNumber).removeClass("notAvailSeat");
							$("#tdSeat_" + seatNumber).addClass("availSeat");
							$('#tdSeat_' + seatNumber).html(strDesc);
							
							if (UI_Anci.seatViewType == "New-view") {
								
								var parentId = "#tdSeatSeg_" + segIndex;
								$(parentId).find("#seatPax_SeatNo_" + paxIndex).html(seatNumber);
								$(parentId).find("#seatPax_Charge_" + paxIndex).html(seatCharge);
								$(parentId).find('#seatPax_Clear_' + paxIndex).find('a').show();
								
							} else {
								
								var parentId = "";
								$(parentId + "#seatPax_SeatNo_" + paxIndex).html(seatNumber);
								$(parentId + "#seatPax_Charge_" + paxIndex).html(seatCharge);
								$(parentId + '#seatPax_Clear_' + paxIndex).find('a').show();
							}
	                	}
                    }

                }

            }
        }
        
        if(UI_Container.isModifySegment() || UI_Container.isModifyAncillary()){

    		$(".removeallSeats").unbind("click").bind("click", function(){

    			UI_Anci.removeAllSelectedSeats();

    		});

    	}
        if(UI_Container.isModifyAncillary()){
        	UI_Anci.displayOldSeats();
        }

    }

    UI_Anci.populateSelectedMeals = function() {
        var paxIndex;
        var segIndex;
        var pax;
        var seg;
        var mealIndex;
        var soldMeals;
        var anciModelMapping;
        var anciMeals;
        var mealSelected ;
        var mealsHtml;
        var mealModelIndex;


        for (paxIndex = 0; paxIndex < UI_Anci.jsonPaxAdults.length; paxIndex++) {
            pax = UI_Anci.jsonPaxAdults[paxIndex];
            for (segIndex = 0; segIndex < UI_Anci.jsAnciSeg.length; segIndex++) {

                if(!UI_Anci.jsAnciSeg[segIndex].meal){
                    continue;
                }

                mealModelIndex = UI_Anci.getMealSegIndex(segIndex);

            	if(mealModelIndex == null){
            		continue;
            	}
            	
                seg = UI_Anci.jsAnciSeg[segIndex].flightSegmentTO;

                anciModelMapping = UI_Anci.resolvePaxIdAndSegId(paxIndex, segIndex);
                mealSelected = [];
                mealsHtml = '<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">';

                if (UI_Anci.paxWiseAnci.length - 1 >= anciModelMapping.paxIndex &&
                    UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries.length - 1 >= anciModelMapping.segIndex) {

                    anciMeals =  UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries[anciModelMapping.segIndex].mealDTOs;

                    if (anciMeals.length == 0) {
                        mealsHtml += '<tr>' +
                            '<td class="mealCol defaultRowGap rowColor "><a href="javascript:void(0)" class="selectAici selectMeal" id="meal_' + segIndex + '_' + paxIndex + '"><label id="lblSelectMeal">' + UI_Anci.jsonLabel["lblSelectMeal"] + '</label></a></td>' +
                            '<td class="priceCol defaultRowGap rowColor " align="center"><label id="price_' + segIndex + '_' + paxIndex + '"> - </label></td>' +
                            '<td class="clearCol defaultRowGap rowColor " align="center"><a href="javascript:void(0)" class="clearMeal clearLabel noTextDec" id="clear_' + segIndex + '_' + paxIndex + '" style="display:none" title="' + UI_Anci.jsonLabel["lblSeatClear"] + '"></a></td>' +
                            '</tr>';
                    } else {
                        $.each(anciMeals, function (index1, element1) {
                            $.each(UI_Anci.jsMealModel[mealModelIndex].meals, function (index2, element2) {
                                if (element1.mealCategoryID == element2.mealCategoryID && element1.mealCode == element2.mealCode) {
                                    mealSelected.push({mealCategoryID:element1.mealCategoryID, mealIndex:index2, count:element1.soldMeals, totalPrice:element1.totalPrice});
                                }
                            });
                        });

                        var isFreeService = UI_Anci.isApplyFreeServiceDecorator(UI_Anci.promoAnciType.MEALS, null, UI_Anci.getOndSequence(segIndex));
                        
                        $.each(mealSelected, function (i, j) {
                            var selectedMealItem = UI_Anci.jsMealModel[mealModelIndex].meals[j.mealIndex];
                            var catId = j.mealCategoryID;
                            var qty = j.count;
                            var mealCharge = UI_Anci.convertAmountToSelectedCurrency(selectedMealItem.mealCharge * qty);
                            if(j.totalPrice != undefined && parseFloat(j.totalPrice) == 0){
                            	mealCharge = 0;
                            }
        					var applyFormatter = UI_Anci.isApplyDecorator(mealCharge,isFreeService);                		
                    		if(applyFormatter){
                    			mealCharge = UI_Anci.freeServiceDecorator;	
                    		}
                    		
                    		var mealNameLabel = j.count + 'x' + selectedMealItem.mealName;
                    		if(!UI_Anci.isMultiMealEnabledForSegment(segIndex)){
                    			mealNameLabel = selectedMealItem.mealName;
                    		}
                    		
                            mealsHtml += '<tr><td class="mealCol defaultRowGap rowColor ">' +
                                '<a id="meal_' + segIndex + '_' + paxIndex + '_' + catId + '_' + j.mealIndex + '" class="selectAici selectMeal" href="javascript:void(0)"><label id="lblSelectMeal">' + mealNameLabel + '</label></a>' +
                                '</td><td align="center" class="priceCol defaultRowGap rowColor  "><label>' + mealCharge + '</label></td>' +
                                '<td align="center" class="clearCol defaultRowGap rowColor  ">' +
                                '<a title="Clear" style="display: block;" id="clear_' + segIndex + '_' + paxIndex + '_' + catId + '_' + j.mealIndex + '" class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>' +
                                '</td></tr>';

                        });

                        UI_Anci.meal.currentPaxId = paxIndex;
                        UI_Anci.meal.currentSegId = segIndex;

                        UI_Anci.mealPaxChanged({paxID:paxIndex, segID:segIndex, slMeals:mealSelected});
                    }

                    mealsHtml += '</table>';
                    $("#multimeal_"+segIndex+'_'+paxIndex).html(mealsHtml);
                }
            }
        }
    }

    UI_Anci.populateSelectedHala = function() {
        var paxIndex;
        var segIndex;
        var pax;
        var seg;
        var anciModelMapping;
        var anciModel;
        var halaSelected;
        var halaModelIndex;
        var i;


        for (paxIndex = 0; paxIndex < UI_Anci.jsonPaxAdults.length; paxIndex++) {
            pax = UI_Anci.jsonPaxAdults[paxIndex];
            var modelSegCounter = 0;
            for (segIndex = 0; segIndex < UI_Anci.jsAnciSeg.length; segIndex++) {

                if(!UI_Anci.jsAnciSeg[segIndex].hala){
                    continue;
                }

                seg = UI_Anci.jsAnciSeg[segIndex].flightSegmentTO;

                anciModelMapping = UI_Anci.resolvePaxIdAndSegId(paxIndex, segIndex);
                UI_Anci.hala.currentSegId = segIndex;

                if (UI_Anci.paxWiseAnci != null && UI_Anci.paxWiseAnci.length - 1 >= anciModelMapping.paxIndex &&
                    UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries.length - 1 >= anciModelMapping.segIndex) {

                    anciModel =  UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries[anciModelMapping.segIndex].airportServiceDTOs;

                    if (anciModel.length > 0) {
                        halaSelected = [];

                        for (i = 0; i < anciModel.length; i++ ){
                            halaModelIndex = UI_Anci.getHalaSegIndex(segIndex, anciModel[i].airportCode);

                            if(halaModelIndex == null){
                                continue;
                            }

                            if (!(anciModel[i].airportCode in halaSelected)) {
                                halaSelected[anciModel[i].airportCode] = [];
                            }

                            halaSelected[anciModel[i].airportCode].push(getHalaByCode(anciModel[i].ssrCode, halaModelIndex));

                        }

                        var airportCode;
                        for (airportCode in halaSelected) {
                            $("#hService_"+segIndex+'_'+paxIndex+'_'+airportCode)
                                .html(UI_Anci.buildHalaServices(segIndex, paxIndex, pax.paxType, halaSelected[airportCode], true, true, airportCode));

                            UI_Anci.halaPaxChanged({paxID:paxIndex,segID:segIndex,slHalas:halaSelected[airportCode],
                                airportCode:airportCode,paxType:pax.paxType});
                            UI_Anci.buildHalaPax();
                        }

                    }

                }

            }
        }
    }

    UI_Anci.populateSelectedBaggage = function() {

        var paxIndex;
        var segIndex;
        var pax;
        var seg;
        var mObj;
        var rObj;
        var seatRefObj;
        var anciModelMapping;
        var anciModel;
        var baggageIndex;
        var baggageModelIndex;


        for (paxIndex = 0; paxIndex < UI_Anci.jsonPaxAdults.length; paxIndex++) {
            pax = UI_Anci.jsonPaxAdults[paxIndex];
            for (segIndex = 0; segIndex < UI_Anci.jsAnciSeg.length; segIndex++) {

                if(!UI_Anci.jsAnciSeg[segIndex].baggage){
                    continue;
                }

                baggageModelIndex =  UI_Anci.getBaggageSegIndex(segIndex);

            	if(baggageModelIndex == null){
            		continue;
            	}
            	
                seg = UI_Anci.jsAnciSeg[segIndex].flightSegmentTO;

                anciModelMapping = UI_Anci.resolvePaxIdAndSegId(paxIndex, segIndex);

                if (UI_Anci.paxWiseAnci != null && UI_Anci.paxWiseAnci.length - 1 >= anciModelMapping.paxIndex &&
                    UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries.length - 1 >= anciModelMapping.segIndex) {

                    anciModel =  UI_Anci.paxWiseAnci[anciModelMapping.paxIndex].selectedAncillaries[anciModelMapping.segIndex].baggageDTOs;

                    UI_Anci.baggage.currentSegId = segIndex;
                    UI_Anci.baggage.currentPaxId = paxIndex;

                    if (anciModel.length > 0) {

                        // TOD0 -- multiple baggages not supported
                        $.each($('#selBaggageType_' + segIndex + '_' + paxIndex + ' option'), function(index,element){
                            if(element.text == anciModel[0].baggageDescription) {
                                baggageIndex = index;
                            }
                        });

                        if(UI_Anci.jsBaggageModel[baggageModelIndex].flightSegmentTO.baggageONDGroupId != null){
                            var elements = UI_Anci.getMatchingIOSegIds(UI_Anci.baggage.currentSegId);
                            if (elements != null) {

                                if (elements[0] == segIndex) {
                                    UI_Anci.paxBaggageRemove(segIndex, paxIndex, true);
                                    UI_Anci.paxBaggageAddProcess(paxIndex, baggageIndex, segIndex, true);
                                } else {
                                    UI_Anci.paxBaggageRemove(segIndex, paxIndex, false);
                                    UI_Anci.paxBaggageAddProcess(paxIndex, baggageIndex, segIndex, false);
                                }
                            }
                        }
                    }

                }

            }
        }
    }


    UI_Anci.modifyAnciAllowed = function (anciType) {
        var allowed = false;
        if (anciType == UI_Anci.anciType.SEAT) {
            allowed = UI_Container.isAnciModifyEnabled('SEAT');
        } else if (anciType == UI_Anci.anciType.MEAL) {
            allowed = UI_Container.isAnciModifyEnabled('MEAL');
        } else if (anciType == UI_Anci.anciType.BAGGAGE) {
            allowed = UI_Container.isAnciModifyEnabled('BAGGAGE');
        } else if (anciType == UI_Anci.anciType.HALA) {
            allowed = UI_Container.isAnciModifyEnabled('AIRPORT_SERVICE');
        }

        return allowed;
    }
	
	UI_Anci.removeAllSelectedSeats = function() {
		$("#lblAutoSeatSelectWhy").hide();
		var selectors = $("input[name='radSeatPax']");
		var clears = $("#tbdySeatSeg").find(".clearLabel");
		for(var kk = 0; kk < clears.length; kk++){
			if ($(clears[kk]).css("display") != "none"){
				$(selectors[kk]).trigger("click");
				$(clears[kk]).trigger("click");
			}
		}
		$(".divSearAnici").hide();
	}
	
	UI_Anci.updateSeatChargeInView = function(){
		
		var seatTotal = UI_Anci.currency.selected + " " + UI_Anci.convertAmountToSelectedCurrency(parseFloat(UI_Anci.getAncillarySummary(UI_Anci.anciType.SEAT).subTotal || 0));
		$("#lblAutoSeatSelectWhy").html(UI_Container.labels.lblAutoSeatSelectWhy.replace(/{xxx}/,"<b>"+seatTotal+"</b>"));
	}
	
	_parseJSONStringToObject = function(jsonStr){
		var jsonObj = null;
		if(jsonStr != undefined && jsonStr != null && jsonStr != ""){		
			jsonObj = $.parseJSON(jsonStr);
		}
		return jsonObj;
	}
	
	_getActualFlightReferenceNumber = function(flightRefNum){
		if(flightRefNum != null){			
			var refNumArr = flightRefNum.split("-");
			return refNumArr[0];
		}
		
		return '';
	}
	
	
	UI_Anci.isFreeServiceIncludedWithBundle = function(type,ondSequence){
		
		switch(type){
			case "SSR":
				type=UI_Anci.promoAnciType.SSR;
				break;
			case "MEALS":
				type=UI_Anci.promoAnciType.MEALS;
				break;	
			case "BAGGAGES":
				type=UI_Anci.promoAnciType.BAGGAGES;
				break;
		}
		var isFound = false;		
	    var ondWiseBundleFareSelected = _parseJSONStringToObject($("#resOndBundleFareStr").val());
	    if(ondWiseBundleFareSelected!=undefined && ondWiseBundleFareSelected!=null){
		    var bundledFarePeriodId = ondWiseBundleFareSelected[ondSequence];	    
			if(bundledFarePeriodId!=undefined && bundledFarePeriodId!=null){		
				var freeServices = UI_Anci.getAvailableFreeServices(ondSequence,bundledFarePeriodId);		
				if(freeServices!=undefined && freeServices!=null){
					$.each(freeServices, function (q, freeService) {        						
						if(type == freeService){   
							isFound = true;
						}        						
					 });
				}		
			}
	    }
		return isFound;
	}


	UI_Anci.getAvailableFreeServices = function(ondSequence,bundledFarePeriodId){
		var servicesList = [];
		if(bundledFarePeriodId!=undefined && bundledFarePeriodId!=null){
			var resAvailableBundleFareLCClassStr = _parseJSONStringToObject($("#resAvailableBundleFareLCClassStr").val());
			if(resAvailableBundleFareLCClassStr!=undefined && resAvailableBundleFareLCClassStr!=null){
				var availbundleFares = resAvailableBundleFareLCClassStr[ondSequence];
				if(availbundleFares!=undefined && availbundleFares!=null){
					$.each(availbundleFares, function (key, availbundleFare) {
						if(availbundleFare!=undefined && availbundleFare!=null){
							var selectedBundleFare = availbundleFare[0];
							if(selectedBundleFare.bundledFarePeriodId==bundledFarePeriodId){
								servicesList = selectedBundleFare.bundleFareFreeServices;				
							}
						}					
					 });        			
				}
			}			
		} 		
		return servicesList;
	}

	UI_Anci.isApplyFreeServiceDecorator = function(type, chargeAmount, segIndex){	
		var isFreeService = false;	
		
		if(UI_Container.isModifyAncillary()){
			
			if(UI_Anci.selectedBundledFares != null && UI_Anci.selectedBundledFares[segIndex] != undefined
					&& UI_Anci.selectedBundledFares[segIndex] != null){
				var ondBundledFare = UI_Anci.selectedBundledFares[segIndex];
				$.each(ondBundledFare.freeServices, function (j, service) {
					if(type == service.serviceName){   
						isFreeService = true;
					}        			
				});
			}		    
		} else {
			 isFreeService = UI_Anci.isFreeServiceIncludedWithBundle(type,segIndex);
		}
			
		if((chargeAmount == null || !parseFloat(chargeAmount) > 0) && isFreeService){
			return true;	
		}		
	    return false;	
	}
	
	UI_Anci.isApplyDecorator = function(chargeAmount,isFreeService){
		if(chargeAmount!=undefined && isFreeService!=undefined 
				&& chargeAmount!=null && isFreeService!=null
				&& (!parseFloat(chargeAmount) > 0 && isFreeService)){
			return true;
		}				
	    return false;	
	}
	
	UI_Anci.createInsuranceGroups =  function(avilInsuranceObjects) {
		var id = 0; var rank = 0; var group = 0; 
		var insrefs = 0;
		var groupObject ;
		var popupProductAvilable = false;
		var insurnceGroupArray =  [];
		$.each(avilInsuranceObjects,function(key,insuranceObj){
			
			if(insuranceObj.rank == rank && insuranceObj.groupID == group ){
				if(insuranceObj.popup || popupProductAvilable){
					popupProductAvilable = true;
				}
				insrefs = insrefs +","+insuranceObj.insuranceRefNumber;
				
			}else{
				insrefs =  insuranceObj.insuranceRefNumber;
				rank = insuranceObj.rank;
				group = insuranceObj.groupID;
				popupProductAvilable =insuranceObj.popup;
				id++;
			}
			
			groupObject = {'INSREF' : insrefs ,'GROUP':group ,'RANK':rank, 'POPUPAVILABLE':popupProductAvilable}
			insurnceGroupArray[id-1] =  groupObject;
			
		});
		return insurnceGroupArray;
	}
	
	UI_Anci.isPOPUPPlanAvailable =  function() {
		var isPopUPContentAvailable = false;
		$.each(UI_Anci.avilableInsuranceproductGroup,function(key,insuranceGroup){
			if(!isPopUPContentAvailable){
				isPopUPContentAvailable = insuranceGroup['POPUPAVILABLE']
			}
			
		});
		return isPopUPContentAvailable;
	}
	
	UI_Anci.getTotalPremiumAmount =  function() {
		var selectedInsTotalPremium =  0;
		$.each(UI_Anci.insuranceObj,function(key,insuranceQuote){
			if(UI_Anci.selectedInsuranceRefNumbers.indexOf(insuranceQuote.insuranceRefNumber)>-1){
				selectedInsTotalPremium  += insuranceQuote.quotedTotalPremiumAmount;
			}
		});
		
		return selectedInsTotalPremium;
	}

	/* Auto select type of seat which has preferred by the registered customer */
	UI_Anci.setPreferredSeatType = function(){
		UI_Anci.preferredSeatType;
		var segIndex;
		var seatCharge = null;		
		var seatNumber = null;
		var rows;
		
		if(UI_Anci.jsonPaxAdults.length == 1){
			
			for(i = 0 ; i < UI_Anci.jsAnciSeg.length ; i++){
				$.each(UI_Anci.jsSMModel[i].lccSeatMapDetailDTO.cabinClass, function(cabinClassIndx , cabinClass){
					
					
					$.each(cabinClass.airRowGroupDTOs , function(airRowGroupDTOIndx , airRowGroupDTO){
						
						rows = $.airutil.sort.quickSort(airRowGroupDTO.airColumnGroups[0].airRows , UI_Anci.rowComparator);
							
						$.each(rows , function(j , airRow){
							
							if(UI_Anci.preferredSeatType == "WindowSeat"){
								if ((seatCharge == null || airRow.airSeats[0].seatCharge < seatCharge)
										& airRow.airSeats[0].seatAvailability == "VAC") {
									segIndex = i;
									seatCharge = airRow.airSeats[0].seatCharge;
									seatNumber = airRow.airSeats[0].seatNumber;
								}
							}else if(UI_Anci.preferredSeatType == "AisleSeat"){
								if ((seatCharge == null || airRow.airSeats[airRow.airSeats.length - 1].seatCharge < seatCharge)
										& airRow.airSeats[airRow.airSeats.length - 1].seatAvailability == "VAC") {
									segIndex = i;
									seatCharge = airRow.airSeats[airRow.airSeats.length - 1].seatCharge;
									seatNumber = airRow.airSeats[airRow.airSeats.length - 1].seatNumber;
								}
							}
						});
						
						rows = $.airutil.sort.quickSort(airRowGroupDTO.airColumnGroups[airRowGroupDTO.airColumnGroups.length-1].airRows , UI_Anci.rowComparator);
						
						$.each(rows , function(j , airRow){
							
							if(UI_Anci.preferredSeatType == "WindowSeat"){
								if ((seatCharge == null || airRow.airSeats[airRow.airSeats.length - 1].seatCharge < seatCharge)
										& airRow.airSeats[airRow.airSeats.length - 1].seatAvailability == "VAC") {
									segIndex = i;
									seatCharge = airRow.airSeats[airRow.airSeats.length - 1].seatCharge;
									seatNumber = airRow.airSeats[airRow.airSeats.length - 1].seatNumber;
								}
							}else if(UI_Anci.preferredSeatType == "AisleSeat"){
								if ((seatCharge == null || airRow.airSeats[0].seatCharge < seatCharge)
										& airRow.airSeats[0].seatAvailability == "VAC") {
									segIndex = i;
									seatCharge = airRow.airSeats[0].seatCharge;
									seatNumber = airRow.airSeats[0].seatNumber;
								}
							}
						});
					});
					
					
				});
				
				if(seatNumber != null){
					refObj = [];
					refObj['fltRefNo'] = UI_Anci.jsAnciSeg[segIndex].flightSegmentTO.flightRefNumber;
					refObj['seatNumber'] = seatNumber;
					UI_Anci.seatOnClick(refObj);
				}
				
				seatCharge = null;		
				
			}
			
		}
	
	}
	


	UI_Anci.displayOldSeats = function() {
	    var strSeatNo;
	    var reservationPaxSelSeg;
	    if (UI_Anci.paxWiseAnci !== undefined) {
	        $
	            .each(
	                UI_Anci.paxWiseAnci,
	                function(key, reservationPax) {
	                    reservationPaxSelSeg = reservationPax.selectedAncillaries;
	                    $
	                        .each(
	                            reservationPaxSelSeg,
	                            function(key, segAnci) {
	                                if (UI_Anci.fltRefNo !== undefined && UI_Anci.fltRefNo
	                                    .indexOf(segAnci.flightSegmentTO.flightRefNumber) === 0 && segAnci.airSeatDTO !== undefined && segAnci.airSeatDTO !== null && segAnci.airSeatDTO.seatNumber !== undefined && segAnci.airSeatDTO.seatNumber !== null) {
	                                    strSeatNo = segAnci.airSeatDTO.seatNumber;
	                                    $(
	                                            "#tdSeat_" + strSeatNo)
	                                        .removeClass(
	                                            "availSeat");
	                                    $(
	                                            "#tdSeat_" + strSeatNo)
	                                        .removeClass(
	                                            "SltdSeat");
	                                    $(
	                                            "#tdSeat_" + strSeatNo)
	                                        .addClass(
	                                            "oldSeat");
	                                }
	                            });

	                });
	    }
	}	
	/*----------------------------------------------------------------END OF FILE ----------------------------------------------------*/
})(UI_Anci);
	