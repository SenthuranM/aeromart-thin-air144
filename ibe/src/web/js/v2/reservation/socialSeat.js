/**
 * Dynamic view upon,
 * 1- segment click
 * 2- social icon(s) click /login from session
 **/
function UI_SocialSeat() {};
UI_SocialSeat.sdkLoaded = {
    FB: false,
    LN: false
};
UI_SocialSeat.params = {
    showFacebook: false,
    showTwitter: false
};

/**selected segment, pax with social identities - server side**/
UI_SocialSeat.persistedProfiles = {
    FACEBOOK: [],
    LINKEDIN: []
};

/**Logged in user friends - client side **/
UI_SocialSeat.socialFriends = {
    FACEBOOK: [],
    LINKEDIN: []
};

/**friends in the flight - client side **/
UI_SocialSeat.travelingFriends = {
    FACEBOOK: [],
    LINKEDIN: []
};

UI_SocialSeat.init = function () {
    if (UI_SocialSeat.params.showFacebook) {
        if (UI_SocialSeat.sdkLoaded.FB == false) {
        	$("#socialSeatingFB").show();
            try {
                (function (d) {
                    var js, id = 'facebook-jssdk';
                    if (d.getElementById(id)) {
                        return;
                    }
                    js = d.createElement('script');
                    js.id = id;
                    js.async = true;
                    js.src = "//connect.facebook.net/en_US/all.js";
                    d.getElementsByTagName('head')[0].appendChild(js);
                }(document));

                window.fbAsyncInit = function () {
                    FB.init({
                        appId: UI_SocialSeat.params.facebookAppId,
                        status: true,
                        cookie: true,
                        xfbml: true
                    });
                }
                
                //invoke forcefully
                if (typeof window.FB != "undefined" && window.FB) {
                    UI_SocialSeat.loadFriendsFromFacebook("auto");
                }

                $("#btnSocialSeatingFacebook").click(function () {
                    UI_SocialSeat.loadFriendsFromFacebook("manual");
                });
                UI_SocialSeat.sdkLoaded.FB = true;
            } catch (e) {}
        }
    } else {
        $("#socialSeatingFB").remove();
    }

    if (UI_SocialSeat.params.showLinkedln) {
        if (UI_SocialSeat.sdkLoaded.LN == false) {
        	$("#socialSeatingLN").show();
            try {
                //need to replace this
                var e = document.createElement('script');
                e.type = 'text/javascript';
                e.async = true;
                e.src = document.location.protocol + '//platform.linkedin.com/in.js?async=true';
                e.onload = function () {
                    IN.init({
                        api_key: UI_SocialSeat.params.linkedInAppId,
                        authorize: true
                    });
                };

                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(e, s);

                $("#btnSeatLinkedIn").click(function () {
                    UI_SocialSeat.loadFriendsFromLinkedIn();
                });

                UI_SocialSeat.sdkLoaded.LN = true;
            } catch (e) {}
        }
    } else {
        $("#socialSeatingLN").remove();
    }

    if (UI_SocialSeat.params.showFacebook || UI_SocialSeat.params.showLinkedln) {
        $(".friendsSeatLegend").show();
    } else {
        $("#socialSeatingUserInfo").remove();
        $("#socialSeatingFriends").remove();
    }

}

//start- calls for external sites
UI_SocialSeat.loadFriendsFromFacebook = function (code) {
    function updateButton(response) {
        var userInfo = document.getElementById('user-info');
        if (response.authResponse) {
            UI_SocialSeat.loadFriendsFromFacebookSuccess(userInfo);
        } else if (code == "manual") {
            FB.login(function (response) {
                if (response.authResponse) {
                    UI_SocialSeat.loadFriendsFromFacebookSuccess(userInfo);
                } else {
                    //user cancelled login or did not grant authorization
                }
            }, {
                scope: 'email'
            });
        }
    }
    FB.getLoginStatus(updateButton);
    FB.Event.subscribe('auth.statusChange', updateButton);
}

UI_SocialSeat.loadFriendsFromFacebookSuccess = function (userInfo) {
    FB.api('/me', function (response) {
        userInfo.innerHTML = '<p> Hello, ' + response.name + ' </p>' + '<img src="https://graph.facebook.com/' + response.id + '/picture">';
        FB.api({
            method: 'fql.query',
            query: 'SELECT uid, name,pic_square FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) AND is_app_user = 1'
        }, function (response) {
            UI_SocialSeat.socialFriends.FACEBOOK = [];
            for (j = 0; j < response.length; j++) {
                var profile = [];
                profile.id = response[j].uid;
                profile.name = response[j].name;
                profile.firstName = '';
                profile.lastName = '';
                profile.pictureUrl = response[j].pic_square;
                profile.profileLink = '';
                UI_SocialSeat.socialFriends.FACEBOOK[UI_SocialSeat.socialFriends.FACEBOOK.length] = profile;
            }
            $("#socialSeatingFB").remove();
            UI_SocialSeat.filterSocialFriendsAndGenerateView();
        });
    });
};

UI_SocialSeat.loadFriendsFromLinkedIn = function () {
    if ((typeof IN != 'undefined') && (typeof IN.User != 'undefined')) {
        IN.User.authorize(function () {
            IN.API.Profile("me").fields("firstName", "lastName", "industry", "pictureUrl", "emailAddress", "id").result(UI_SocialSeat.loadLinkdinSelfInfo);
        });
    }
}

UI_SocialSeat.loadLinkdinSelfInfo = function (selfInfo) {
    selfProfile = selfInfo.values[0];
    if (typeof selfProfile.pictureUrl == 'undefined' || selfProfile.pictureUrl == "") {
        selfProfile.pictureUrl = '../images/empty_user_no_cache.png';
    }
    document.getElementById("user-info").innerHTML =
        "<p> Hello, " + selfProfile.firstName + " " + selfProfile.lastName + "</p> " + "<img src='" + selfProfile.pictureUrl + "'>";
    IN.API.Connections("me").result(UI_SocialSeat.loadFriendsFromLinkedInSuccess).error(UI_SocialSeat.linkedInConnectionErros);
}

UI_SocialSeat.loadFriendsFromLinkedInSuccess = function (connections) {
    UI_SocialSeat.socialFriends.LINKEDIN = [];
    var members = connections.values;

    for (var member in members) {
        var profile = [];
        profile.id = members[member].id;
        profile.firstName = members[member].firstName;
        profile.lastName = members[member].lastName;
        profile.pictureUrl = members[member].pictureUrl;
        profile.profileLink = members[member].siteStandardProfileRequest.url;
        UI_SocialSeat.socialFriends.LINKEDIN[UI_SocialSeat.socialFriends.LINKEDIN.length] = profile;
    }
    $("#socialSeatingLN").remove();
    $("#linkdinWrapper").remove();
    UI_SocialSeat.filterSocialFriendsAndGenerateView();
}

UI_SocialSeat.linkedInConnectionErros = function () {
    //do nothing
}
//end- calls for external sites

UI_SocialSeat.markSelectedPax = function () {
    if ((UI_SocialSeat.params.showFacebook && $("#socialSeatingFB").length == 0) ||
        (UI_SocialSeat.params.showLinkedln && $("#socialSeatingLN").length == 0)) {
        UI_SocialSeat.populatepersistedProfiles();
        UI_SocialSeat.filterSocialFriendsAndGenerateView();
    }
}

UI_SocialSeat.populateProfiles = function () {
    if (UI_SocialSeat.params.showFacebook || UI_SocialSeat.params.showLinkedln) {
        UI_SocialSeat.populatepersistedProfiles();
    }
}


//social-step1
UI_SocialSeat.populatepersistedProfiles = function () {

    UI_SocialSeat.persistedProfiles = {
        FACEBOOK: [],
        LINKEDIN: []
    };
    $("#mutualFriends").empty();

    var currFltRefNumber = UI_Anci.getFltRefNoForIndex(UI_Anci.seat.currentSegId);
    $.each(UI_Anci.jsSMModel, function (index, smObj) {
        var cabinClass = smObj.lccSeatMapDetailDTO.cabinClass;
        var fltSeg = smObj.flightSegmentDTO;

        if (currFltRefNumber == fltSeg.flightRefNumber) {

            for (var ccJ = 0; ccJ < cabinClass.length; ccJ++) {
                var cc1 = cabinClass[ccJ];
                if (fltSeg.cabinClassCode == cc1.cabinType) {
                    var rgDtos1 = cc1.airRowGroupDTOs;
                    rgDtos1 = $.airutil.sort.quickSort(rgDtos1, UI_Anci.rowGroupComparator);
                    for (var rgJ = 0; rgJ < rgDtos1.length; rgJ++) {
                        var cgDtos1 = rgDtos1[rgJ].airColumnGroups;
                        cgDtos1 = $.airutil.sort.quickSort(cgDtos1, UI_Anci.columnGroupComparator);
                        for (var cgJ = 0; cgJ < cgDtos1.length; cgJ++) {
                            var rowsJ = cgDtos1[cgJ].airRows;
                            rowsJ = $.airutil.sort.quickSort(rowsJ, UI_Anci.rowComparator);

                            for (var seatRowsNo = 0; seatRowsNo < rowsJ.length; seatRowsNo++) {
                                var airseats = rowsJ[seatRowsNo].airSeats;

                                for (var seatRowNo = 0; seatRowNo < airseats.length; seatRowNo++) {
                                    var airseatsRow = airseats[seatRowNo];
                                    var socialProfiles = airseatsRow.socialIdentity;

                                    $.each(socialProfiles, function (proIndex, proId) {
                                        if (proIndex != undefined && proIndex != null) {

                                            var t = {};
                                            t.socialSiteCustId = proId;
                                            t.paxName = airseatsRow.paxName;
                                            t.seatNumber = airseatsRow.seatNumber;

                                            if (proIndex == "1") {
                                                t.type = "FACEBOOK";
                                                UI_SocialSeat.persistedProfiles.FACEBOOK[UI_SocialSeat.persistedProfiles.FACEBOOK.length] = t;
                                            } else if (proIndex == "3") {
                                                t.type = "LINKEDIN";
                                                UI_SocialSeat.persistedProfiles.LINKEDIN[UI_SocialSeat.persistedProfiles.LINKEDIN.length] = t;
                                            }
                                        }
                                    });


                                }
                            }

                        }
                    }
                }
            }
        }
    });
}

//social:step3
UI_SocialSeat.filterSocialFriendsAndGenerateView = function () {
    UI_SocialSeat.travelingFriends.FACEBOOK = [];
    $.each(UI_SocialSeat.persistedProfiles.FACEBOOK, function (fbId, fbProf) {
        $.each(UI_SocialSeat.socialFriends.FACEBOOK, function (c_fbId, c_fbProf) {
            if (fbProf.socialSiteCustId == c_fbProf.id) {
                var infoText = "<p>" + c_fbProf.id + ", " + fbProf.paxName + ", " + fbProf.seatNumber + "</p>";
                var info = {};
                info.id = fbProf.socialSiteCustId;
                info.paxName = fbProf.paxName;
                info.seatNumber = fbProf.seatNumber;
                if (typeof c_fbProf.pictureUrl == 'undefined' || c_fbProf.pictureUrl == "") {
                	c_fbProf.pictureUrl = '../images/empty_user_no_cache.png';
                }
                info.picUrl = c_fbProf.pictureUrl;
                info.profurl = c_fbProf.profileLink;
                UI_SocialSeat.travelingFriends.FACEBOOK[UI_SocialSeat.travelingFriends.FACEBOOK.length] = info;
            }
        });
    });

    UI_SocialSeat.travelingFriends.LINKEDIN = [];
    $.each(UI_SocialSeat.persistedProfiles.LINKEDIN, function (lkdnIn, lkdnProf) {
        $.each(UI_SocialSeat.socialFriends.LINKEDIN, function (c_lkdnIn, c_lkdnProf) {
            if (lkdnProf.socialSiteCustId == c_lkdnProf.id) {
                var infoTextLinkdn = "<p>" + c_lkdnProf.id + ", " + lkdnProf.paxName + ", " + lkdnProf.seatNumber + "</p>";
                var info = {};
                info.id = c_lkdnProf.socialSiteCustId;
                info.paxName = lkdnProf.paxName;
                info.seatNumber = lkdnProf.seatNumber;
                if (typeof c_lkdnProf.pictureUrl == "undefined" || c_lkdnProf.pictureUrl == "") {
                    c_lkdnProf.pictureUrl = '../images/empty_user_no_cache.png';
                }
                info.picUrl = c_lkdnProf.pictureUrl;
                info.profurl = c_lkdnProf.profileLink;
                UI_SocialSeat.travelingFriends.LINKEDIN[UI_SocialSeat.travelingFriends.LINKEDIN.length] = info;
            }
        });
    });

    UI_SocialSeat.generateSocialSeatingView();
}


UI_SocialSeat.generateSocialSeatingView = function () {
    var seatingHtml = "";
    var firstFriend = false;

    if (UI_SocialSeat.params.showFacebook) {
        $.each(UI_SocialSeat.travelingFriends.FACEBOOK, function (profId, pro) {
            if (firstFriend == false) {
                firstFriend = true;
                seatingHtml += "<table>" +
                    "<tr> <td colspan='3'> <p>" + UI_Container.labels.mutualFriendsInFlight + "<a href='#' class='removeSocialFriends'><label id='lblFriendHider'>" + UI_Container.labels.hideFriends + "</label></a></p></td></tr>";
            }
            seatingHtml += "<tr class='socialRow'>" +
                "<td width='30%'> <a href='http://facebook.com/" + pro.id + "'> <img src='http://graph.facebook.com/" + pro.id + "/picture?type=square'/> </a> </td>" +
                "<td width='50%'> " + pro.paxName + "</td>" +
                "<td width='20%'> Seat: " + pro.seatNumber + "</td>" +
                "</tr>";
        });
    }

    if (UI_SocialSeat.params.showLinkedln) {
        $.each(UI_SocialSeat.travelingFriends.LINKEDIN, function (proId, prof) {
            if (firstFriend == false) {
                firstFriend = true;
                seatingHtml += "<table>" +
                    "<tr> <td colspan='3'> <p>" + UI_Container.labels.mutualFriendsInFlight + "<a href='#' class='removeSocialFriends'><label id='lblFriendHider'>" + UI_Container.labels.hideFriends + "</label></a></p></td></tr>";
            }

            seatingHtml += "<tr class='socialRow'>" +
                "<td width='30%'> <a href='" + prof.profurl + "'> <img src='" + prof.picUrl + "'/> </a> </td>" +
                "<td width='50%'> " + prof.paxName + "</td>" +
                "<td width='20%'> Seat: " + prof.seatNumber + "</td>" +
                "</tr>";
        });
    }

    if (firstFriend == false) {
        seatingHtml = "<p>" + UI_Container.labels.noFriends + "</p>";
        $("#mutualFriends").empty();
        $("#mutualFriends").append(seatingHtml);
    } else {
        seatingHtml += "</table>";
        $("#mutualFriends").empty();
        $("#mutualFriends").append(seatingHtml);

        $("#lblFriendHider").click(function () {
            $(".socialRow").toggle();
            if ($(".socialRow").is(":visible")) {
                $("#lblFriendHider").text(UI_Container.labels.hideFriends);
            } else {
                $("#lblFriendHider").text(UI_Container.labels.showFriends);
            }
        });

        //unification of image sizes-using 3rd party scripts
        try {
            var mutualFriendDiv = $("#mutualFriends").find("img");
            mutualFriendDiv.cropresize({
                width: 50,
                height: 50,
                wrapperCSS: {
                    "float": "left"
                }
            });
        } catch (e) {}
    }

    try {
        var userImgdiv = $("#user-info").find("img");
        userImgdiv.cropresize({
            width: 50,
            height: 50,
            wrapperCSS: {
                "float": "left"
            }
        });
    } catch (e) {}
    UI_SocialSeat.paintSocialSeat();

}

UI_SocialSeat.paintSocialSeat = function () {

    $.each(UI_SocialSeat.travelingFriends.FACEBOOK, function (profId, pro) {
        var seatId = "#tdSeat_" + pro.seatNumber;
        var profLink = "<img src='http://graph.facebook.com/" + pro.id + "/picture?type=square'/>";
        $(seatId).removeClass("notAvailSeat");
        $(seatId).addClass("friendsSeat");

        $(seatId).unbind("mouseover").bind("mouseover", function () {
        	if(typeof event =="undefined"){
        		event= arguments[0].originalEvent;
        	}
            UI_SocialSeat.seatOnMouseOver(event,profLink);
        });

        $(seatId).unbind("mouseout").bind("mouseout", function () {
            UI_SocialSeat.seatOnMouseOut();
        });
    });
    
    $.each(UI_SocialSeat.travelingFriends.LINKEDIN, function (profId, pro) {
    	//<img src='" + prof.picUrl + "'/>
        var seatId = "#tdSeat_" + pro.seatNumber;
        var profLink = "<img src='" + pro.picUrl + "'/>";
        $(seatId).removeClass("notAvailSeat");
        $(seatId).addClass("friendsSeat");

        $(seatId).unbind("mouseover").bind("mouseover", function () {
        	if(typeof event =="undefined"){
        		event= arguments[0].originalEvent;
        	}
            UI_SocialSeat.seatOnMouseOver(event,profLink);
        });

        $(seatId).unbind("mouseout").bind("mouseout", function () {
            UI_SocialSeat.seatOnMouseOut();
        });
    });

}

UI_SocialSeat.seatOnMouseOver = function (objE, profLink) {
    var x = 0
    var y = 0;
    if (objE != null) {
        if (UI_Anci.browser.msie) {
            x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
            y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
        } else {
            if ((navigator.userAgent.indexOf("Safari") != -1) ||
                (navigator.userAgent.indexOf("Opera") != -1)) {
                x = objE.clientX;
                y = objE.clientY;
            } else {
                x = objE.clientX + window.scrollX;
                y = objE.clientY + window.scrollY;
            }
        }
    }
    $("#spnSocialSeating").empty();
    $("#spnSocial").show();
    $("#spnSocial").css({
        "left": (x + 10) + "px"
    });
    $("#spnSocial").css({
        "top": (y + 10) + "px"
    });
    $("#spnSocialSeating").append(profLink);
    try {
    	//Image sizes received from social sites are different, so unify
        var socialSeatingImgs = $("#spnSocialSeating").find("img");
        socialSeatingImgs.cropresize({
            width: 50,
            height: 50,
            wrapperCSS: {
                "float": "left"
            }
        });
    } catch (e) {}
   return false;
}

UI_SocialSeat.seatOnMouseOut = function (refObj) {
    $("#spnSocial").hide();
    $("#spnSocialSeating").empty();
    return false;
}