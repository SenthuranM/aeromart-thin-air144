function UI_mcAvailabilitySearch(){

    this.currencyCode = UI_Top.holder().GLOBALS.baseCurrency;
    this.selectedCurrency = "";
    this.showPriceBDInSelCurr = false;
    this.sessionTimeOutDisplay =  UI_Top.holder().GLOBALS.sessionTimeOutDisplay;
    this.classOFService = "";
    this.isCaptchaEnabled = UI_Top.holder().GLOBALS.imageCapchaDisAvalSearch;
    this.showFareRules = null;
    this.fareRules = null;
    this.lblAdult = "";
    this.lblChild = "";
    this.lblInfant = "";
    this.weekdays = "";
    this.weekdaysFull = null;
    this.months = null;
    this.stopovers = null;
    this.jsonLabel = null;
    this.errorInfo = null;
    this.errorInfosearch = null;
    this.currencyList = [];
    this.arrCurrency = [];
    this.isAddBusSegment = false;
    this.hideTaxBDRows = true;
    this.hideSurchargeBDRows = true;
    this.isRegUser = false;
    this.arrFareCat = null;
    this.arrCusType =null;
    //for passenger DOB validation
    this.paxValidation = null;
    this.originList = [];
    this.strFromHD = "";
    this.strToHD = "";
    this.carrier = "";
    this.objResSrchParams= null;

    this.ondLogicalCCSelection = {};
    this.ondFlexiSelection = {};
    this.ondFlexiAvailability = {};
    this.ondFlexiCharges = {};
    this.ondBundledFareSelection = {};
    this.ondBookingClassSelection = {};
    this.segmentBookingClassSelection = {};

    this.ondString = [];

    var mustFilldArray = [];
    slFareQuoteRow = null;
    slFareQuoteTable = null;
    ondHTML = null;
    sl_flightHTML = null;
    flightHTML = null;
    flightHTMLTABLE = null;
    flightTableHTML = null;
    this.fareQuote = null;
    logicalCCHTML = null;
    this.flightDetailsPanelHTML = "#FlightDetailsPanel";
    this.promotionInfo = null;
    this.operatingCarriers = [];
    var actLogCabClassListAll =null;
    var calDaysList =null;
    this.promoDiscountType = {
        PERCENTAGE : "PERCENTAGE",
        VALUE : "VALUE"
    }

    this.loadingProgress = function(){
        $("#pageLoading").show();
    }

    this.hideProgress = function(){
        setTimeout(function(){$("#pageLoading").hide()}, 1000);
        UI_commonSystem.readOnly(false);
    }

    this.ready = function(){
        $("#divLoadBg").hide();
        $("#trAccept, #btnContinue").hide();
        UI_mcAvailabilitySearch.loadingProgress();
        $("#btnFindFlight").click(function(){findFlightClick();});
        $("#btnSOver").click(function(){UI_mcAvailabilitySearch.homeClick();});
        $("#btnPrevious").click(function(){showSearchPanal()});
        $("#btnContinue").click(function(){UI_commonSystem.pageOnChange();UI_mcAvailabilitySearch.captureSuccess();});
        $("#linkTerms").click(function(){UI_mcAvailabilitySearch.linkTermsClick(globalConfig.termsLink);});
        $("#selAdults").change(function(){selAdultsOnChange();});
        $("#btnAddDestination").click(function(){addNewONDHTML()});
        $("#btnfareQuate").click(function(){
            loadFareQuote();
            //$(this).hide();
        });
        $(document).on('click',"#newSearch", function(){
            clearAllDynamic();
            createOnDHTML(0);
            insertParam("searchCrit", null);
            $("#SearchPane").show();
            $("#resultsPane").hide();
        });
        $(document).on('click',"#modSearch", function(){
            clearResultArea()
             insertParam("searchCrit", null);
            $("#SearchPane").show();
            $("#resultsPane").hide();
        });
        $("#selSelectedCurrency").change(function(){
            UI_mcAvailabilitySearch.selectedCurrency = $(this).val();
        });
        loadPageBuildData();
        selAdultsOnChange();
        if(UI_Top.holder().GLOBALS.dynamicOndListEnabled && ondListLoaded()) {
            initDynamicOndList();
        }
        
        if(!UI_Top.holder().GLOBALS.promoCodeEnabled) {		
			$("#trPromoCode").hide();
		}

        if (calDisplayConfig.layout.termsConditionPos == "bottom"){
            var temp = $("#trTermsNCond").remove().clone();
            $(".buttonset").parent().append(temp);
        }

        /**
         * Build Steps
         */
        UI_commonSystem.stepsSettings(1,"availability");
    };


    loadPageBuildData = function(){
        UI_mcAvailabilitySearch.loadingProgress();
        var url = "availabilitySearchPageDetail.action";
        var data = {};
        $.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,
            success: loadPageBuildDataProcess, error:UI_commonSystem.setErrorStatus, cache : false});
        return false;
    };

    showSearchPanal = function(){
        if(readParam('st').indexOf("fareQuote")>-1){
            insertParam("st","mcAvail");
            UI_mcAvailabilitySearch.fareQuote=null;
            $("#trAccept, #btnContinue").hide();
            $("#trPriceBDPannel").slideUp();
            $("#farequote_done_Area").hide();
            $("#btnfareQuate").show();
            $("#FlightDetailsPanel, #farequoteArea").show();
            if (UI_AvailabilitySearch!=null){
            	$("#farequoteArea").hide();
            }
        }else{
            loadSearchCriteria();
            insertParam("searchCrit", null);
            insertParam("st","search");
            $("#resultsPane").hide();
            $("#SearchPane").show();
        }
    }

    loadSearchCriteria = function(){
        var strArray = UI_mcAvailabilitySearch.ondString;
        for (var i=0;i<strArray.length;i++){
            var index = (i+1);
            $("#selFromLoc_"+index).val(strArray[i].fromAirport);
            $("#selToLoc_"+index).val(strArray[i].toAirport);
            $("#departureDate_"+index).val(dateFromGregorian(strArray[i].depDate,SYS_IBECommonParam.locale, 'd/m/yy'));
            $("#selCOS_"+index).val(strArray[i].classOfService);
        }
        $("#selAdults").val($("#resAdultCount").val());
        $("#selChild").val($("#resChildCount").val());
        $("#selInfants").val($("#resInfantCount").val());
        $("#selSelectedCurrency").val($("#resSelectedCurrency").val());
        $("#promoCode").val($("#resPromoCode").val());
    }

    loadPageBuildDataProcess = function(response){
        loadAirPortSearchData();
        $("#PgFares").populateLanguage({messageList:response.jsonLabel});
        UI_mcAvailabilitySearch.labels = response.jsonLabel;
        UI_mcAvailabilitySearch.errorInfo = response.errorInfo;
        UI_mcAvailabilitySearch.lblAdult = UI_mcAvailabilitySearch.labels.lblAdult;
        UI_mcAvailabilitySearch.lblChild = UI_mcAvailabilitySearch.labels.lblChild;
        UI_mcAvailabilitySearch.lblInfant = UI_mcAvailabilitySearch.labels.lblInfant;
        UI_mcAvailabilitySearch.weekdays = UI_mcAvailabilitySearch.labels.weekdaysArray.split(",");
        UI_mcAvailabilitySearch.weekdaysFull= UI_mcAvailabilitySearch.labels.weekdaysFullArray.split(",");
        UI_mcAvailabilitySearch.months = UI_mcAvailabilitySearch.labels.monthsArray.split(",");
        UI_mcAvailabilitySearch.stopovers = UI_mcAvailabilitySearch.labels.stopoversArray.split(",");
        UI_mcAvailabilitySearch.showPriceBDInSelCurr = response.showPriceBDInSelCurr;
        UI_mcAvailabilitySearch.carrier = response.carrier;
        UI_mcAvailabilitySearch.lblAdultAgeRange = UI_mcAvailabilitySearch.labels.lblAdultAgeRange;
        UI_mcAvailabilitySearch.lblChildAgeRange = UI_mcAvailabilitySearch.labels.lblChildAgeRange;
        $("#SearchPane").show();
        $("#resultsPane").hide();
        $("#divLoadBg").show();
        $(".bookingSummary").summaryPanel({id:"SearchSummary", data:null, labels:response.jsonLabel});
        if ($.trim(response.termsNCond) == ""){
            $("#trTermsNCond").hide();
        }else{
            $("#termsNCond").html(response.termsNCond);
        }
        
        UI_mcAvailabilitySearch.airportMessage = response.airportMessage;
        UI_mcAvailabilitySearch.loadAirportSearchMessages();
        UI_CaptchaValidation.labels = UI_mcAvailabilitySearch.labels;
    };

    var setLabelJS = function(){
        var lbl = UI_mcAvailabilitySearch.labels;
        flightHTMLTABLE.find("#ln_Next").text(lbl.lblNextDay);
        flightHTMLTABLE.find("#ln_Prev").text(lbl.lblPreviousDay);
    }

    loadAirPortSearchData = function() {
        UI_mcAvailabilitySearch.loadingProgress();
        var url = "modifySearch.action";
        var data = {};
        $.ajax({type: "POST", dataType: 'json',data: data , url:url,
            success: loadAirPortSearchDataProcess, error:UI_commonSystem.setErrorStatus, cache : false});
        return false;
    };

    loadAirPortSearchDataProcess = function(response){
        UI_mcAvailabilitySearch.errorInfosearch = response.errorInfo;
        fillSearchDropDown(response);
        UI_mcAvailabilitySearch.initiatePage();
        setLabelJS();
        createOnDHTML(0);

        if(typeof isDrySearch!='undefined' && paxCountStr!=undefined && paxCountStr!=null
            && ondListStr!=undefined && ondListStr!=null) {

            var paxCountArray = paxCountStr.split("^");

            $("#resAdultCount").val(paxCountArray[0]);
            $("#resChildCount").val(paxCountArray[1]);
            $("#resInfantCount").val(paxCountArray[2]);
            $("#resIsMcSelted").val(true);
            $("#resPromoCode").val($("#promoCode").val());

            UI_mcAvailabilitySearch.ondString = ondListStr;
            $("#modlinks").show();
            var url = "multiCitySearch.action";

            var data = createSearchSubmitData();

            data["resIsMcSelted"] = true;
            data["searchParams.ondListString"] = JSON.stringify( UI_mcAvailabilitySearch.ondString, null);
            data["searchParams.searchSystem"] = "AA";

			if(UI_CaptchaValidation.isHumanVerificationEnabled) {
				UI_CaptchaValidation.setDataToProcessAfterCaptchaValidation(data,url,'MCSEARCH', null);
			} 	 
            $.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,
                success: UI_mcAvailabilitySearch.processFlightSearch, error:UI_commonSystem.setErrorStatus, cache : false});
        }
        
        if(readParam('st')!=null  && (readParam('st').indexOf("mcAvail") > -1 || readParam('st').indexOf("fareQuote") > -1 )){
        	//comment the below line to load to availability strait on page refresh
        	insertParam('searchCrit', 'null');
        	if (readParam('searchCrit')!=null){
        		findFlightClick();
        	}else if ($("#resOndListStr").val()!=""){
        		$("#resIsMcSelted").val(true);
        		UI_mcAvailabilitySearch.ondString = JSON.parse($("#resOndListStr").val());
            	mcSearchCall();
        	}
        }else if ($("#resOndListStr").val()!=""){
        	UI_mcAvailabilitySearch.ondString = JSON.parse($("#resOndListStr").val());
        	$("#resIsMcSelted").val(true);
        	if (typeof MCSEARCH != 'undefined'){
        		mcSearchCall();
        	}
        }
        UI_mcAvailabilitySearch.hideProgress();
    };

    toRenameAirpots = function(data) {
        var toArray = $.extend(true, [], data);
        toArray[0][1] = UI_mcAvailabilitySearch.strToHD;
        return toArray;
    }

    fillSearchDropDown = function(searchInfo) {
        if(UI_Top.holder().GLOBALS.dynamicOndListEnabled && ondListLoaded()) {
            var fromAirpotList = setAirportData(setAirportListHead('from') , UI_mcAvailabilitySearch.originList);
            fillFromToAirportDD(fromAirpotList,toRenameAirpots(fromAirpotList));
        } else {
            var fromAirpotList = setAirportData(setAirportListHead('from') , searchInfo.airportList);
            fillFromToAirportDD(fromAirpotList,toRenameAirpots(fromAirpotList));
        }
        $("#selCOS").fillDropDown({dataArray:searchInfo.activeCos, keyIndex:0, valueIndex:1, firstEmpty:false});
        $("#selSelectedCurrency").fillDropDown({dataArray:UI_mcAvailabilitySearch.currencyList, keyIndex:0, valueIndex:1, firstEmpty:false});
    };

    createPaxCategoryList = function(paxList) {
        var data = [];
        for (var i = 0; i < paxList.length; i++) {
            data[i] = [];
            data[i][0]= paxList[i].paxCategoryCode;
            data[i][1] = paxList[i].paxCategoryDesc;
        }
        return data;
    };

    createFareTypeList = function(fareList) {
        var data = [];
        for (var i = 0; i < fareList.length; i++) {
            data[i] = [];
            data[i][0]= fareList[i].fareCategoryCode;
            data[i][1] = fareList[i].fareCategoryDesc;
        }
        return data;
    };

    mergeKeyValueData = function(data) {
        var temp = "";
        if (data != null) {
            for(var i = 0; i < data.length; i++) {
                temp = data[i][1];
                delete data[i][1];
                data[i][1] = data[i][0] + " - " + temp;
            }
        }

        return data;
    };

    this.previousAirportData = {
        fromAirportsPrevious : null,toAirportsPrevious : null,
        fromAirportGroundOnlyData: null, toAirportGroundOnlyData:null,
        isOnADataRefresh : false,
        clear : function (){
            this.fromAirportsPrevious = null;
            this.toAirportsPrevious = null;
            this.isOnADataRefresh = false;
        },
        isEmpty : function (){
            if(this.fromAirportsPrevious == null){
                return true;
            }
            return false;
        }
    }

    fillFromToAirportDD = function (fromAirportList, toAirportList){
        if(UI_mcAvailabilitySearch.previousAirportData.isEmpty()){
            UI_mcAvailabilitySearch.previousAirportData.fromAirportsPrevious = fromAirportList;
            UI_mcAvailabilitySearch.previousAirportData.toAirportsPrevious = toAirportList;
        }else {
            //if not assuming modifysegment
            UI_mcAvailabilitySearch.previousAirportData.fromAirportGroundOnlyData = fromAirportList;
            UI_mcAvailabilitySearch.previousAirportData.toAirportGroundOnlyData = toAirportList;
        }
        $("#selFromLoc").children().remove();
        $("#selToLoc").children().remove();

        if(!UI_mcAvailabilitySearch.isAddBusSegment){
            $("#selFromLoc").fillDropDown({dataArray:fromAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
            $("#selToLoc").fillDropDown({dataArray:toAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
        } else {

            //Setting language descriptions from airport list
            if(UI_Top.holder().GLOBALS.dynamicOndListEnabled && UI_FlightSearch.ondListLoaded()){

                var lang = UI_Top.holder().GLOBALS.sysLanguage;
                if ($("#locale").val() != null && $("#locale").val() != "") {
                    lang = $("#locale").val();
                }
                if(lang != 'en') {
                    for(var i in fromAirportList){
                        for(var j in airports){
                            if(fromAirportList[i][0] == airports[j]['code'] && airports[j][lang] != null){
                                fromAirportList[i][1] = airports[j][lang];
                                break;
                            }
                        }
                    }
                    for(var k in toAirportList){
                        for(var l in airports){
                            if(toAirportList[k][0] == airports[l]['code'] && airports[l][lang] != null){
                                toAirportList[k][1] = airports[l][lang];
                                break;
                            }
                        }

                    }
                }
            }

            $("#selFromLoc").fillDropDown({dataArray:fromAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
            $("#selToLoc").fillDropDown({dataArray:toAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
            $("#hndFromAirport").val($("#selFromLoc :selected").text());
            $("#hndToAirport").val($("#selToLoc :selected").text());
        }
    };
    /**
     * Link terms Click
     */
    this.linkTermsClick = function(extUrl) {
        var url = "";
        if (extUrl != null){
            //set to external link
            url = extUrl;
        }else{
            url = SYS_IBECommonParam.nonSecurePath + "showTermsNCond.action";
        }
        var intHeight = (window.screen.height - 200);
        var intWidth = 795;
        var intX = ((window.screen.height - intHeight) / 2);
        var intY = ((window.screen.width - intWidth) / 2);
        var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
        if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)){UI_Top.holder().objCWindow.close();}
        UI_Top.holder().objCWindow = window.open(url,'ToolChild',strProp)
    };

    /**
     * Link terms Click
     */
    UI_mcAvailabilitySearch.compareFareClick = function(extUrl) {
        var url = "";
        if (extUrl != null){
            //set to external link
            url = extUrl;
        }else{
            url = SYS_IBECommonParam.nonSecurePath + "showLoadPage!showCompareFare.action";
        }
        var intHeight = (window.screen.height - 400);
        var intWidth = 500;
        var intX = ((window.screen.height - intHeight) / 2);
        var intY = ((window.screen.width - intWidth) / 2);
        var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
        if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)){UI_Top.holder().objCWindow.close();}
        UI_Top.holder().objCWindow = window.open(url,'ToolChild',strProp)
    };

    dataformater = function(txt){
        if (txt != null || txt != undefined)
            text = txt.split("/");
        var dat = null;
        if ($.browser.msie)
            dat = Number(text[2])+"/"+Number(text[1])+"/"+Number(text[0]);
        else
            dat = Number(text[1])+"/"+Number(text[0])+"/"+Number(text[2]);

        return dat;
    };

    dateVal = function( str1 ) {
        var diff = Date.parse( str1 );
        return isNaN( diff ) ? NaN : {
            diff : diff,
            ms : Math.floor( diff            % 1000 ),
            s  : Math.floor( diff /     1000 %   60 ),
            m  : Math.floor( diff /    60000 %   60 ),
            h  : Math.floor( diff /  3600000 %   24 ),
            d  : Math.floor( diff / 86400000        )
        };
    };

    DateToString =function(dtdate){
        var dtCM = dtdate.getMonth() + 1;
        var dtCD = dtdate.getDate();
        if (dtCM < 10){dtCM = "0" + dtCM}
        if (dtCD < 10){dtCD = "0" + dtCD}
        return dtCD + "/" + dtCM + "/" + dtdate.getFullYear();;
    };

    addDaysAsString = function (strDate, intDays){
        if (strDate != null){
            strDate = strDate.split("/")
            var dtDate = new Date(Number(strDate[2]), Number(strDate[1])-1, Number(strDate[0]));
            var gmtOffsetOri = dtDate.getTimezoneOffset();
            var calNewDate = new Date(dtDate.getTime() + Number(intDays) *24*60*60*1000);
            var gmtOffsetNew = calNewDate.getTimezoneOffset();
            //Assumption they won't be multiple dst changes with these days
            if (gmtOffsetOri != gmtOffsetNew){
                var offsetDifference = gmtOffsetOri - gmtOffsetNew;
                calNewDate = new Date(calNewDate.getTime() - (offsetDifference*60*1000));
            }
            return DateToString(calNewDate);
        }
    };


    ondListLoaded = function(){
        if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
            return false;
        } else {
            return true;
        }
    };

    setAirportData =  function(head, data) {
        return head.concat(data);
    };

    setAirportListHead = function(params) {
        var data = [];
        data[0] = [];
        data[0][0]= "";

        if(params == 'from'){
            data[0][1] = UI_mcAvailabilitySearch.strFromHD;
        } else {
            data[0][1] = UI_mcAvailabilitySearch.strToHD;
        }
        return  data;
    }

    createOnDHTML = function(index){
        x = parseInt(index,10) + 1;

        var tempTR = ondHTML.clone();
        tempTR.attr("id",tempTR.attr("id")+"_"+x);
        $.each(tempTR.find("input"),function(){
            $(this).attr("id", this.id+"_"+x);
            $(this).attr("tabindex", x+""+this.tabIndex);
        });
        $.each(tempTR.find("select"),function(){
            $(this).attr("id", this.id+"_"+x);
            $(this).attr("tabindex", x+""+this.tabIndex);
        });
        $.each(tempTR.find("button"),function(){
            $(this).attr("id", this.id+"_"+x);
        });
        $("#multiOND").append(tempTR);
        if (x==1){$("#btnDelDest_1").remove();}
        $("#departureDate_"+x).datepicker({regional: SYS_IBECommonParam.locale , minDate: new Date(), showOn: "button", buttonImage: "../images/Calendar2_no_cache.gif", buttonImageOnly: true, maxDate:'+1Y', yearRange:'-1:+1', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
        $("#selCOS_"+x).change(function(){clearFareQuote();});
        $("#departureDate_"+x).change(function(){clearFareQuote();});
        $("#departureDate_"+x).blur(function() { clearFareQuote();});
        $("#btnDelDest_"+x).click(function(){removeONDHTML(this.id);});
        $("#selCOS_"+x).val((globalConfig.defaultCabinCalss==undefined)?"Y":globalConfig.defaultCabinCalss);
    };

    addNewONDHTML = function(){
        var maxOND = UI_Top.holder().GLOBALS.allowedMaxNoONDMulticitySearch;    	
    	var currentOnds = $("#multiOND").children().length;
        var cLength = $("#multiOND>div:last").attr("id").split("_")[1];
        if (currentOnds < maxOND){
            if (ondValied(cLength)){
                createOnDHTML(cLength);
            }
        }else{
            jAlert("Reach maximum ONDs");
        }
    };

    removeONDHTML = function(id){
        var x = id.split("_")[1];
        if ($("#multiOND").children().length > 1){
            var x = id.split("_")[1];
            $("#ONDTeml_"+x).remove();
        }else{
            jAlert("Can not remove the last OND");
        }
    };

    ondValied = function(ind){
        var erroStr = isONDValied(ind);
        var ret = true;
        if (erroStr != ""){
            ret = false;
            $("#errorDiv").html(erroStr);
        }
        return ret;
    };

    sortOnds = function(ond1, ond2) {
        return ond1.dateInteger > ond2.dateInteger;
    };


    createSearchSubmitData = function() {
        var obj = {};
        $("#searchSubmitParams > input").each(function(i,o){
            var n = o.name, v = o.value;
            var jItm = $(o);
            if(jItm.attr('type') != 'radio' || jItm.attr('checked') ) {
                obj[n] = v;
            }
        });
        return obj;
    }
    
    mcSearchCall = function(){
    	 var url = "multiCitySearch.action";
         var data = createSearchSubmitData();
         data["resIsMcSelted"] = $("#resIsMcSelted").val();
         data["searchParams.ondListString"] = JSON.stringify( UI_mcAvailabilitySearch.ondString, null);
         data["searchParams.searchSystem"] = UI_mcAvailabilitySearch.setDefaultSearchOption(UI_mcAvailabilitySearch.ondString);
         insertParam("searchCrit", data);
         if(validDrySearchForwardToOperatingCarrier(data)){
             url= UI_Top.holder().GLOBALS.carrierUrlsIBE[UI_mcAvailabilitySearch.operatingCarriers[0]];

             $("#operCarrierFrm").attr('action',url);
             $("#operCarrierFrm").submit();

         } else {
         	if(UI_CaptchaValidation.isHumanVerificationEnabled) {
				UI_CaptchaValidation.setDataToProcessAfterCaptchaValidation(data,url,'MCSEARCH', null);
			} 
        	$.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,
                 success: UI_mcAvailabilitySearch.processFlightSearch, error:UI_commonSystem.setErrorStatus, cache : false});
         }
    }


    findFlightClick = function(){
        if(validateBeforeSearch()){
        	UI_mcAvailabilitySearch.loadingProgress();
            $("#resAdultCount").val($("#selAdults").val());
            $("#resChildCount").val($("#selChild").val());
            $("#resInfantCount").val($("#selInfants").val());
            $("#resPromoCode").val($("#promoCode").val());

            UI_mcAvailabilitySearch.ondString = UI_mcAvailabilitySearch.ondString.sort(sortOnds);
            $("#modlinks").show();
            mcSearchCall();
            return false;
        }
    };

    validDrySearchForwardToOperatingCarrier = function(data){

        if((($("#regCustomer").val() !="" &&  $("#regCustomer").val() == "false"))
            && data["searchParams.searchSystem"] !=undefined && data["searchParams.searchSystem"]!=null
            && data["searchParams.searchSystem"] == 'INT' && UI_Top.holder().GLOBALS.carrierUrlsIBE!=null
            && SYS_IBECommonParam.locale!=undefined && SYS_IBECommonParam.locale!=null
            &&  (($("#resSelectedCurrency").val()!=undefined &&  $("#resSelectedCurrency").val()!=null) 
            		|| ($("#selSelectedCurrency").val()!=undefined &&  $("#selSelectedCurrency").val()!=null)  )) {

            UI_mcAvailabilitySearch.setOperatingCarriers(UI_mcAvailabilitySearch.ondString);
            var operatingCarriersArr = UI_mcAvailabilitySearch.operatingCarriers;

            if(operatingCarriersArr!=undefined && operatingCarriersArr !=null && operatingCarriersArr.length == 1
                && operatingCarriersArr[0]!=null && $.trim(operatingCarriersArr[0])!=''
                && UI_Top.holder().GLOBALS.carrierUrlsIBE[operatingCarriersArr[0]]!=null
                && $.trim(UI_Top.holder().GLOBALS.carrierUrlsIBE[operatingCarriersArr[0]])!='') {

                $("#ondListString").val(JSON.stringify( UI_mcAvailabilitySearch.ondString, null));
                var paxCountStr = $("#resAdultCount").val()+"^"+$("#resChildCount").val()+"^"+$("#resInfantCount").val();
                $("#paxCountStr").val(paxCountStr);                
                
                var selCurrency = $("#resSelectedCurrency").val();
                if($("#selSelectedCurrency").val()!=undefined &&  $("#selSelectedCurrency").val()!=null){
                	selCurrency = $("#selSelectedCurrency").val();
                }                
                var hdnParamData =  SYS_IBECommonParam.locale+"^MC^"+selCurrency;
                $("#hdnParamData").val(hdnParamData);
                return true;
            }
        }


        return false;
    };

    function createCalendarDates(myind){
        //Create Calendar days
        var dayWidth = parseInt((calDisplayConfig.layout.calWidth - calDisplayConfig.design.selectedWidth)/7, 10);
        dayWidth = (dayWidth+11) - calDisplayConfig.design.gapInDaysPx;

        //$.each(calDaysList, function(myind, dayList){
        var dayList = $.data($("#flightTempl_"+myind)[0], "calDateArray");
        var ondData = $.data($("#flightTempl_"+myind)[0], "OnDData" );
        //ondDataFix for mahan
        if (SYS_IBECommonParam.locale=='fa'){
            ondData.depDate = ondData.departureDate;
            if(ondData.departureDate.indexOf("T")>-1){
                var tem = ondData.departureDate.split("T")[0].split("-");
                ondData.depDate = tem[2] + "/" + tem[1] + "/" + tem[0];
            }

        }
        var depDay = ondData.depDate.replace(/\//g,"");
        var calHTML = '<div class="flightCalendar" style="width: '+(calDisplayConfig.layout.calWidth+10) +'px; top: 1px;">'+
            '<div style="width: '+calDisplayConfig.layout.calWidth+'px;" class="c-scroller">'+
            '<ul class="calendar-ul" style="width:'+((dayWidth*8)+calDisplayConfig.design.selectedWidth)+'px" id="flCal_'+myind+'">';
        $.each(dayList, function(dayInd, day){
            var cdaydId = day.date.replace(/\//g,""),selectedDay="",setStyle = "width:"+dayWidth+"px;top:15px;";
            if (depDay===cdaydId){
                selectedDay = "selected";
                setStyle = "width:"+calDisplayConfig.design.selectedWidth+"px;top:0px;";
            }
            var tdate = new Date(dataformater(day.date));
            calHTML+='<li style="margin:'+calDisplayConfig.design.gapInDaysPx+'px">'+
                '<div class="getDay cal-day flight-body">'+
                '<a id="date_'+myind+'_'+cdaydId+'_'+dayInd+'" class="getFlightDay '+selectedDay+'" style="'+setStyle+'">'+
                '<div class="cal-indBg cal-base"><div class="sData"><span class="calCurrdate" style="display: none;" id="dayIndex_'+dayInd+'">'+day.date+'</span>';
            if (SYS_IBECommonParam.locale=='fa'){
                calHTML += '<div>'+dateFromGregorian(day.date,SYS_IBECommonParam.locale)+'</div>';
                if(day.isFareAvailable){
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.available +'</span></div>';
                }else{
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.notAvailable + '</span></div>';
                }

            }else {
                calHTML += '<div class="daySmall">' + UI_mcAvailabilitySearch.weekdays[tdate.getDay()] + '</div>' +
                '<div class="dateSmall">' + tdate.getDate() + ' ' + UI_mcAvailabilitySearch.months[tdate.getMonth()] + '</div>';
                if(day.isFareAvailable){
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.available +'</span></div>';
                }else{
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.notAvailable + '</span></div>';
                }

            }
            calHTML +='</div><div class="sDataFull">';
            if (SYS_IBECommonParam.locale=='fa') {
                calHTML += '<div>' + dateFromGregorian(day.date, SYS_IBECommonParam.locale) + '</div>';
                if(day.isFareAvailable){
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.available +'</span></div>';
                }else{
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.notAvailable + '</span></div>';
                }
            }else {
                calHTML += '<div class="daySmall">'+UI_mcAvailabilitySearch.weekdaysFull[tdate.getDay()]+'</div>'+
                    '<div class="dateSmall">'+ tdate.getDate()+ ' ' + UI_mcAvailabilitySearch.months[tdate.getMonth()]+'</div>';
                if(day.isFareAvailable){
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.available +'</span></div>';
                }else{
                    calHTML += '<div class="minFareValue"><span>' + UI_mcAvailabilitySearch.labels.notAvailable + '</span></div>';
                }
             }
            calHTML += '</div></div></a></div></li>';
        });
        calHTML+='</ul></div></div>';
        $("#flightTempl_"+myind).find(".aligend").html(calHTML);

        //});
        $("body").off("click", ".getFlightDay").on("click", ".getFlightDay", function(){
            var id=this.id.split("_")[1];
            var cWidth = $("#flCal_"+id).find(".getFlightDay:not('.selected')").width();
            $("#flCal_"+id).find(".getFlightDay")
                .removeClass("selected")
                .css({
                    "width":cWidth,
                    "top":15
                });

            $(this)
                .addClass("selected")
                .css({
                    "width":calDisplayConfig.design.selectedWidth,
                    "top":0
                });
            searchWithGivenDate(this.id);
        });
    }

    this.processFlightSearch = function(response){
        if(response.success) {
        	if(UI_CaptchaValidation.isHumanVerificationEnabled) {
        		if(response.captchaValidationRequired) {
        			UI_mcAvailabilitySearch.hideProgress();
 					UI_CaptchaValidation.openCaptchaPopup();
 					return false;
        	 	}
        		UI_CaptchaValidation.resetDataToProcessAfterCaptchaValidation(); 
        	}
            actLogCabClassListAll = response.ondWiseAvailLogCOSInfoList;
            var ondSeqFlightsList = response.ondSeqFlightsList;
            //calDaysList = response.calendarDayList;
            calDaysList =  response.calendarDayFlightAvailList;

            //airportMessage = response.airportMessage;
            clearResultArea();
            UI_mcAvailabilitySearch.objResSrchParams = response.searchParams;
            $("#resOndListStr").val(UI_mcAvailabilitySearch.objResSrchParams.ondListString);
            $.each(ondSeqFlightsList, function(ind, dataObj){
                dataObj = extractDataAddCabinClass(dataObj, ind)
                createONDFlightHTML(ind);
                mustFilldArray[mustFilldArray.length] = false;
                fillFlightGrid(dataObj,ind);
            });

            UI_mcAvailabilitySearch.hideProgress();
            $("#btnSearch").enable();
            $("#resultsPane").show();
            $("#SearchPane").hide();
            //$("#flightTempl_0").show();
            $("#btnfareQuate").hide();
            insertParam("st","mcAvail");
        } else {
            jAlert(response.messageTxt,"ERROR");
            insertParam("searchCrit" , null);
//			wrapAndDebugError('002',response);
        }
        dateChangerjobforLanguage(SYS_IBECommonParam.locale);
        UI_mcAvailabilitySearch.hideProgress();
    };
    
    this.processFlightReSearch = function(response, index){
    	 if(response.success){
    	 	if(UI_CaptchaValidation.isHumanVerificationEnabled) {
				if(response.captchaValidationRequired) {
    			 	UI_mcAvailabilitySearch.hideProgress();
    				UI_CaptchaValidation.openCaptchaPopup();
    				return false;
    		 	}
    		 	UI_CaptchaValidation.resetDataToProcessAfterCaptchaValidation(); 
			}
    		if (response.ondSeqFlightsList[0].length!=0){
             	actLogCabClassListAll[index] = response.ondWiseAvailLogCOSInfoList[0];
                 dataObj = extractDataAddCabinClass(response.ondSeqFlightsList[0], index)
                 createONDFlightHTML(index);
                 fillFlightGrid(dataObj,index);
                 $("#flightTempl_"+index).show();
             }else{
             	var emptTable = $("<tr class='even'><td colspan='6'> * No Flights available for the selected date, press Next / Previous button for nearest availability for you request.</td></tr>");
                 $("#flightTable_"+index).empty().append(emptTable);
                 createCalendarDates(index);
             }
         }else{
             UI_commonSystem.setErrorStatus();
         }
    	 UI_mcAvailabilitySearch.hideProgress();
    }

    clearResultArea = function(){
        $(UI_mcAvailabilitySearch.flightDetailsPanelHTML).empty();
        $("#SelectedItemsFL").empty();
        $("#shoping_Cart").hide();
        mustFilldArray = [];
    };

    getCabinClassList = function(list){
        var rList = [],tempCClist=[];
        $.each(list, function(i, j){
            if ($.inArray(j.split(",")[0], tempCClist)==-1){
                rList[rList.length] = j;
                tempCClist[tempCClist.length] = j.split(",")[0];
            }
        })
        return rList
    }

    extractDataAddCabinClass = function(dataObj, index){
        $.each(dataObj, function(flInd, flObj){
            var t_avaiCCList = flObj.availabilityByCOSList;
            var t_flightSegIdList = flObj.flightSegIdList;
            //actLogCabClassList active availabel cabin calss list
            if (t_avaiCCList.length == t_flightSegIdList.length){
                if (t_avaiCCList.length > 0){
                    for (var i=0;i<t_avaiCCList.length;i++){
                        if (t_flightSegIdList[i] == t_avaiCCList[i].split("$")[0]){
                            var ccInfo = getCabinClassList(t_avaiCCList[i].split("$")[1].split("#"));
                            for (var t = 0; t < ccInfo.length; t++){
                                //if(ccInfo[t].split(",")[3]==="true"){
                                ccInfo[t] += ","+index;
                                if(flObj["ccItem_"+t] != undefined){
                                    var tccI = flObj['ccItem_'+t].split(",");
                                    var ttccI = ccInfo[t].split(",");
                                    if(tccI[3] != ttccI[3] && ttccI[3] == "false"){
                                        flObj["ccItem_"+t] = ccInfo[t];
                                    }
                                } else {
                                    flObj["ccItem_"+t] = ccInfo[t];
                                }
                                //}
                            };
                        }
                    }
                }else{
                    var actLogCabClassList = (actLogCabClassListAll[index]).split("#")[0]
                    for (var d=0;d<actLogCabClassList;d++){
                        flObj["ccItem_"+d] = "";
                    }
                }
            }else{
                jAlert("Unmatch Class of service recived");
                return [];
            }
        });
        return dataObj;
    };

    createONDFlightHTML = function(index){
        var tClone = null;
        if ($("#flightTempl_"+index).length>0){
            tClone = flightHTMLTABLE.clone();
            $.each(tClone.find("a, #flightTable, span"),function(){
                $(this).attr("id", this.id+"_"+index);
            });
            $(UI_mcAvailabilitySearch.flightDetailsPanelHTML).find("#flightTempl_"+index)
                .empty()
                .append(tClone);
        }else{
            tClone = flightHTML.clone();
            tClone.attr("id", tClone.attr("id")+"_"+index);
            $.each(tClone.find("a, #flightTable, span"),function(){
                $(this).attr("id", this.id+"_"+index);
            });
            $(UI_mcAvailabilitySearch.flightDetailsPanelHTML).append(tClone);

            var _tsl = sl_flightHTML.clone();
            if (index%2==0){
                _tsl.attr("class", "even");
            }
            _tsl.attr("id", "SelectedItemsFL_"+index);
            $.each(_tsl.find("button"),function(){
                $(this).attr("id", this.id+"_"+index);
            });
            $.each(_tsl.find("div"),function(){
                $(this).attr("id", this.id+"_"+index);
            });
            _tsl.hide();
        }

        $("#SelectedItemsFL").append(_tsl);
        if($("#flightTempl_"+index)[0] != undefined){
            $.data($("#flightTempl_"+index)[0], "OnDData" ,UI_mcAvailabilitySearch.ondString[index]);
            $.data($("#flightTempl_"+index)[0], "calDateArray", calDaysList[index]);
        }
        createCalendarDates(index);
        var fromAirportName = getAirportName(UI_mcAvailabilitySearch.ondString[index].fromAirport);
        var toAirportName = getAirportName(UI_mcAvailabilitySearch.ondString[index].toAirport);
        var labelTo = "/";
        if(SYS_IBECommonParam.locale != 'en'){
        	labelTo = UI_mcAvailabilitySearch.labels.lblTo;
        }
        
        $("#hOND_"+index).text(fromAirportName + " " + labelTo + " " + toAirportName );
        $("#hDepDate_"+index).text(UI_mcAvailabilitySearch.ondString[index].depDate);
        $("#hDepDateP_"+index).text(UI_mcAvailabilitySearch.ondString[index].depDate);
        $("body").off("click", ".fl_select").on("click", ".fl_select", function(event){showAllSelectedFlight(event)});
        $("#ln_Prev_"+index+" ,#ln_Next_"+index).click(function(e){searchNextPrevDay(e)});
        $("#divFlightDetailsRequote").show();
    };

    searchWithGivenDate = function(clickId){
        var temp =clickId.split("_");
        var _ondListStr = [];
        var extDate = $("#"+clickId).find(".calCurrdate").text();
        _ondListStr[_ondListStr.length] = UI_mcAvailabilitySearch.ondString[temp[1]];
        _ondListStr[0].depDate = extDate;
        _ondListStr[0].departureDate = converAAToJson(extDate);
        $.data($("#flightTempl_"+temp[1]), "OnDData",_ondListStr[0]);
        if (UI_AvailabilitySearch != null){
        	$("#resDepartureDate").val(extDate);
        	$("#resDepartureVariance").val(0)
    		UI_AvailabilitySearch.loadAvailabilitySearchData();
        }else{
        	researchCallAction(temp[1], _ondListStr);
        }
        
        
    }

    searchNextPrevDay = function(e){
        var flags = e.target.id.split("_");
        var index = flags[2];
        var flg = flags[1];

        //get caldate Array
        var calDateArray = $.data($("#flightTempl_"+index)[0], "calDateArray");
        var _ondListStr = [];
        _ondListStr[_ondListStr.length] = UI_mcAvailabilitySearch.ondString[index];
        _ondListStr[0].flightRPHList = [];
        var extDate = UI_mcAvailabilitySearch.ondString[index].depDate;
        var boolSearch = false,newDateValue = "";
        if (flg.toUpperCase()=='NEXT'){
            newDateValue = addDaysAsString(extDate, 1);
            if(dateVal(dataformater(calDateArray[calDateArray.length-1])).d === dateVal(dataformater(_ondListStr[0].depDate)).d){
                calDateArray.splice(0, 1);
                calDateArray[calDateArray.length] = newDateValue;

            }

            _ondListStr[0].depDate = newDateValue
            _ondListStr[0].departureDate = converAAToJson(addDaysAsString(extDate, 1));
            boolSearch = true;
        }else{
            if (dateVal(dataformater(UI_Top.holder().strSysDate)).d < dateVal(dataformater(extDate)).d){
                newDateValue = addDaysAsString(extDate, -1);
                if(dateVal(dataformater(calDateArray[0])).d === dateVal(dataformater(_ondListStr[0].depDate)).d){
                    calDateArray.splice((calDateArray.length-1), 1);
                    var temNewArra = [newDateValue];
                    var myNewArray  = temNewArra.concat(calDateArray);
                    calDateArray = myNewArray;

                }

                _ondListStr[0].depDate = newDateValue;
                _ondListStr[0].departureDate = converAAToJson(addDaysAsString(extDate, -1));
                boolSearch = true;
            }
        }

        $.data($("#flightTempl_"+index)[0], "calDateArray", calDateArray);
        calDaysList[index] = calDateArray;

        if (boolSearch){
            $.data($("#flightTempl_"+index), "OnDData",_ondListStr[index]);
            $("#hDepDate_"+index).text(_ondListStr[0].depDate);
            researchCallAction(index, _ondListStr);
        }


    };

    researchCallAction = function(index, _ondListStr){
        UI_mcAvailabilitySearch.loadingProgress()
        $("#farequoteArea").slideUp("fast");
        $("#btnfareQuate").hide();
        var url = "multiCitySearch.action";
        var data = createSearchSubmitData();
        data["searchParams.ondListString"] = JSON.stringify( _ondListStr, null);
        data["searchParams.searchSystem"] = UI_mcAvailabilitySearch.setDefaultSearchOption(_ondListStr);
        data["resIsMcSelted"] = $("#resIsMcSelted").val();
        
        if(validDrySearchForwardToOperatingCarrier(data)){
            url= UI_Top.holder().GLOBALS.carrierUrlsIBE[UI_mcAvailabilitySearch.operatingCarriers[0]];

            $("#operCarrierFrm").attr('action',url);
            $("#operCarrierFrm").submit();

        } else {
        	if(UI_CaptchaValidation.isHumanVerificationEnabled) {
        		 var additionalData = [];
        		 additionalData['index'] = index;
				UI_CaptchaValidation.setDataToProcessAfterCaptchaValidation(data,url,'MCRESEARCH', additionalData);
			} 	 
            $.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,
                success: function (response) {
                	UI_mcAvailabilitySearch.processFlightReSearch(response, index);
                },
                error:UI_commonSystem.setErrorStatus,cache : false});
        }
        return false;

    }


    this.setDefaultSearchOption = function (sortOnds){
        var searchSystem = 'AA';
        if(UI_Top.holder().GLOBALS.dynamicOndListEnabled
            && ondListLoaded()
            && sortOnds!=null && sortOnds.length > 0 ) {


            for(var l=0;l<sortOnds.length;l++){
                var from = sortOnds[l].fromAirport;
                var to = sortOnds[l].toAirport;

                var fromIndex = -1;
                var toIndex = -1;
                // get from, to indexes
                var airportsLength = airports.length;
                for(var i=0;i<airportsLength;i++){
                    if(from == airports[i].code){
                        fromIndex = i;
                    } else if(to == airports[i].code) {
                        toIndex = i;
                    }
                }

                if(fromIndex != -1){
                    var availDestOptLength = origins[fromIndex].length;
                    for(var i=0;i<availDestOptLength;i++){
                        var destOptArr = origins[fromIndex][i];
                        if(destOptArr[0] == toIndex){
                            var isInt = false;
                            var optCarriersArr = destOptArr[3].split(",");
                            for(var j=0;j<optCarriersArr.length;j++){
                                if(optCarriersArr[j] != GLOBALS.defaultCarrierCode){
                                    isInt = true;
                                    break;
                                }
                            }

                            if(isInt){
                                searchSystem = 'INT';
                            } else {
                                searchSystem = 'AA';
                            }

                            break;
                        }
                    }
                } else {
                    searchSystem = 'INT';
                }

                if(searchSystem == 'INT'){
                    break;
                }


            }



        } else {
            searchSystem = 'AA';
        }

        $('#resSearchSystem').val(searchSystem);
        return searchSystem;
    }

    this.setOperatingCarriers = function (sortOnds){

        if(UI_Top.holder().GLOBALS.dynamicOndListEnabled
            && ondListLoaded()
            && sortOnds!=null && sortOnds.length > 0 ) {

            for(var l=0;l<sortOnds.length;l++){
                var from = sortOnds[l].fromAirport;
                var to = sortOnds[l].toAirport;

                var fromIndex = -1;
                var toIndex = -1;
                // get from, to indexes
                var airportsLength = airports.length;
                for(var i=0;i<airportsLength;i++){
                    if(from == airports[i].code){
                        fromIndex = i;
                    } else if(to == airports[i].code) {
                        toIndex = i;
                    }
                }

                if(fromIndex != -1){
                    var availDestOptLength = origins[fromIndex].length;
                    for(var i=0;i<availDestOptLength;i++){
                        var destOptArr = origins[fromIndex][i];
                        if(destOptArr[0] == toIndex){
                            var isInt = false;
                            var optCarriersArr = destOptArr[3].split(",");
                            for(var j=0;j<optCarriersArr.length;j++){

                                if ((this.operatingCarriers==undefined || this.operatingCarriers==null || this.operatingCarriers.length==0 )
                                    || (this.operatingCarriers.length > 0 && !(this.operatingCarriers.indexOf(optCarriersArr[j]) > -1))) {
                                    this.operatingCarriers.push(optCarriersArr[j]);
                                }

                            }

                        }
                    }
                }

            }

        }

    }

    getSelectedFlight = function(index, flg){
        var returnVal = null;
        var ondResultsData = $.data($("#flightTempl_"+index)[0], "ResultsData");
        var _seletedFlights = $("#flightTempl_"+index).find("input.fl_select:checked");
        if (_seletedFlights.length>0){
            var _selONDCabin = _seletedFlights[0].id.split("_");
            var tOBJ = ondResultsData[_selONDCabin[2]];
            
            var stops = tOBJ.viaPointList.length;
            var stopsText = UI_mcAvailabilitySearch.stopovers[stops] + " ";
            if(stops>0){
            	stopsText += UI_mcAvailabilitySearch.labels.shortovers;
            }else{
            	stopsText += UI_mcAvailabilitySearch.labels.shortover;
            }
            
            if (flg == "SEGS"){
                var selectedFlightSegments = [];
                for (var k=0;k<tOBJ.flightRPHList.length;k++){
                    var t = {};
                    t.segmentCode = tOBJ.segmentCodeList[k];
                    t.segmentName = tOBJ.segmentFullNameList[k];
                    t.flightNumber = tOBJ.flightNoList[k];
                    t.departureDate = tOBJ.depatureList[k];
                    t.departureTime = tOBJ.depatureList[k];
                    t.arrivalDateLong = tOBJ.arrivalTimeList[k];
                    t.departureDateLong = tOBJ.depatureTimeList[k];
                    t.arrivalTime = tOBJ.arrivalList[k];
                    t.duration = tOBJ.flightDurationList[k];
                    t.stops = stopsText;
                    t.cabin = _selONDCabin[3];
                    selectedFlightSegments[selectedFlightSegments.length] = t;
                }
                returnVal = selectedFlightSegments;
            }else{
                var t = {};
                t.segmentCode = tOBJ.segmentCodeList;
                t.segmentName = tOBJ.segmentFullNameList;
                t.flightNumber = tOBJ.flightNoList;
                t.arrivalDate = tOBJ.arrivalList;
                t.departureDate = tOBJ.depatureList;
                t.arrivalDateLong = tOBJ.arrivalTimeList;
                t.departureDateLong = tOBJ.depatureTimeList;
                t.departureTime = getTimefromString(tOBJ.depatureList);
                t.arrivalTime = getTimefromString(tOBJ.arrivalList);
                t.duration = tOBJ.flightDurationList;
                t.stops = stopsText;
                t.rphList = tOBJ.flightRPHList;
                t.cabin = _selONDCabin[3];
                t.arrivalDateVal = getDatefromString(tOBJ.arrivalList);
                t.departureDateVal = getDatefromString(tOBJ.depatureList);
                returnVal = t;
            }
        }
        return returnVal;
    };

    breakItems = function(arr){
        var outStr = '';
        var first = true;
        for(var i = 0; i < arr.length ; i++){
            if(!first) outStr  += '<br />';
            outStr +=arr[i];
            first = false;
        }
        return outStr;
    }

    var getSeatAvaiStatusByCOSList = function(oInd, list){
        var actLogCabClassLis = (actLogCabClassListAll[oInd]).split("#");
        var fList =[];
        if (parseInt(actLogCabClassLis[0],10)>1){
            fList = list;
        }else{
            var cabin = (actLogCabClassLis[1]).split("$")[0];
            var exist = $.inArray(cabin+"#true", list);
            if(exist>-1){
                fList[fList.length] = list[exist];
            }
        }
        return fList;
    }

    fillFlightGrid = function(dataOBJ, ind){
        var setCOSCols = function(flHObj, flDObj, flInd ,oInd){
            var seatAvaiList = flDObj.seatAvaiStatusByCOSList;
            var onlyAvail = getSeatAvaiStatusByCOSList(oInd, flDObj.seatAvaiStatusByCOSList);
            var wCOS = (30/seatAvaiList.length)+"%";
            $.each(seatAvaiList, function(i, j){
                if (flDObj['ccItem_'+i] != undefined){
                    var tccI = flDObj['ccItem_'+i].split(",");
                    //var tdhCOS = $("<td><label class='gridHDFont fntBold'>"+tccI[0]+"</label></td>").addClass("gridHD bdRight");
                    //tdhCOS.attr({"align":"center", "width":wCOS});
                    if ($.inArray(tccI[0]+"#true", onlyAvail)>-1){
                        var radFL = null;
                        if (tccI[3]=="true"){
                            radFL = $("<input type='radio'/>").attr({
                                "id":"radSel_"+ind+"_"+flInd+"_"+tccI[0],
                                "name":"radSel_"+ind,
                                "value":flDObj.flightRPHList.toString(),
                                "class":"fl_select"
                            });
                            radFL.click(function(){

                            });
                        }else{
                            radFL = $("<label>N/A</label>");
                        }

                        var tdbCOS = $("<td></td>").addClass("GridItems  thinBorderL tdFareColumn");
                        tdbCOS.attr({"align":"center"});
                        tdbCOS.append(radFL);
                        //flHObj.find(".tbHead").append(tdhCOS);

                        //flHObj.find(".tbbody").append(tdbCOS);
                        flHObj.append(tdbCOS);
                    }else{
                        if(onlyAvail.length>1){
                            radFL = $("<label>N/A</label>");
                            var tdbCOS = $("<td></td>").addClass("GridItems  thinBorderL tdFareColumn");
                            tdbCOS.attr({"align":"center"});
                            tdbCOS.append(radFL);
                            //flHObj.find(".tbHead").append(tdhCOS);

                            //flHObj.find(".tbbody").append(tdbCOS);
                            flHObj.append(tdbCOS);
                        }
                    }

                }
            });
        };
        var actLogCabClassList = (actLogCabClassListAll[ind]).split("#");
        if (parseInt(actLogCabClassList[0], 10)>1){
            actLogCabClassList = (actLogCabClassList[1]).split("^");
        }else{
            actLogCabClassList = [((actLogCabClassList[1]).split("$")[0])+ "$" + UI_mcAvailabilitySearch.labels.lblSelectaFlight];
        }


        var wCOS = (30/actLogCabClassList.length)+"%";

        for (var x=0;x<actLogCabClassList.length;x++){
            var addLeft = "";
            if(x==0){
                addLeft = "bdLeft";
            }
            var tdhCOS = $("<td><label class='gridHDFont fntBold'>"+actLogCabClassList[x].split("$")[1]+"</label></td>").addClass("gridHD bdRight " + addLeft);
            tdhCOS.attr({"align":"center", "width":wCOS});
            $("#flightTempl_"+ind).find(".tbHead").append(tdhCOS);
        }


        if (dataOBJ.length > 0 && dataOBJ[0].flightRPHList.length>0){
            $.each(dataOBJ, function(flI, flO){
                var ondIND = flI+1;
                var tFlTab = flightTableHTML.clone();
                if(flI%2==1){
                    tFlTab.removeClass("even");
                }
                
                var stops = flO.viaPointList.length;
                var stopsText = UI_mcAvailabilitySearch.stopovers[stops] + " ";
                if(stops>0){
                	stopsText += UI_mcAvailabilitySearch.labels.shortovers;
                }else{
                	stopsText += UI_mcAvailabilitySearch.labels.shortover;
                }
                tFlTab.find("label#optTxt").text("Option " + ondIND);
                tFlTab.find("label#flightNumber").html(breakItems(flO.flightNoList));
				tFlTab.find("label#departureDate").html(getDatefromString(flO.depatureList));
                tFlTab.find("label#departureTime").html(getTimefromString(flO.depatureList));
				tFlTab.find("label#arrivalDate").html(getDatefromString(flO.arrivalList));
                tFlTab.find("label#arrivalTime").html(getTimefromString(flO.arrivalList));
                tFlTab.find("label#departureDateLong").html(breakItems(flO.depatureTimeList));
                tFlTab.find("label#arrivalDateLong").html(breakItems(flO.arrivalTimeList));
                tFlTab.find("label#stops").html(stopsText);
                setCOSCols(tFlTab, flO, flI, ind);
                $("#flightTable_"+ind).append(tFlTab);
            });
            $("#flightTable_"+ind+">br:last").remove();
            $("#flightTempl_"+ind).find(".trAddFlight").show();
            $.data($("#flightTempl_"+ind)[0], "ResultsData" ,dataOBJ);
        }else{
            var emptTable = $("<tr class='even'><td colspan='6'><label>" + UI_mcAvailabilitySearch.errorInfo['ERR010'] + "</label></td></tr>");
            $("#flightTable_"+ind).append(emptTable);
            $("#flightTable_"+ind+">br:last").remove();
            $("#flightTempl_"+ind).find(".trAddFlight").hide();
        }
        dateChangerjobforLanguage(SYS_IBECommonParam.locale);
    }

    converAAToJson = function(dstr) {
        var sTDAr = dstr.split("/");
        var yy = sTDAr[2];
        var MM = sTDAr[1];
        var dd = sTDAr[0];
        var hh = '00';
        var mm = '00';
        return yy + '-' + MM + '-' + dd + 'T' + hh + ':' + mm + ':00';
    };


    validateBeforeSearch = function(){
        var ret = false;
        var searchData = readParam("searchCrit")
        if(searchData!=null){
            UI_mcAvailabilitySearch.ondString = JSON.parse(searchData['searchParams.ondListString']);
            ret = true;
        }else{
            UI_mcAvailabilitySearch.ondString = [];
        }

        if (UI_mcAvailabilitySearch.ondString.length==0){
            var onds = $("#multiOND").children();
            for (var i=0;i<onds.length;i++)	{
                var tempOnd = {};
                var t = onds[i].id.split("_")[1];
                var erroStr = isONDValied(t);
                if("" == erroStr){
                    tempOnd.fromAirport = $('#selFromLoc_'+t).val().toUpperCase();
                    tempOnd.toAirport = $('#selToLoc_'+t).val().toUpperCase();
                    tempOnd.fromAirportName = $('#selFromLoc_'+t+'>option:selected').text();
                    tempOnd.toAirportName = $('#selToLoc_'+t+'>option:selected').text();
                    tempOnd.flightRPHList = [];
                    tempOnd.flownOnd = false;
                    tempOnd.existingFlightRPHList = [];
                    tempOnd.existingResSegRPHList = [];
                    tempOnd.departureDate = converAAToJson(dateToGregorian($("#departureDate_"+t).val(),SYS_IBECommonParam.locale));
                    tempOnd.depDate = dateToGregorian($("#departureDate_"+t).val(),SYS_IBECommonParam.locale);
                    tempOnd.departureVariance = 0;
                    tempOnd.dateInteger = dateVal(dataformater(dateToGregorian($("#departureDate_"+t).val(),SYS_IBECommonParam.locale))).d;
                    // Set cabin class & logical cabin class based on selection
                    var cos = $("#selCOS_"+t).val();
                    var cosArr = cos.split("-");
                    // If user selects logical cabin class, value patter will be
                    // 'XXX-XXX' (logical_cc-cabin_class)
                    if(cosArr.length > 1){
                        tempOnd.classOfService = cosArr[0];
                        tempOnd.logicalCabinClass = cosArr[1];
                    } else if(cosArr.length == 1) {
                        tempOnd.classOfService = cosArr[0];
                        tempOnd.logicalCabinClass = null;
                    }
                    tempOnd.bookingType = "NORMAL";
                    tempOnd.bookingClass = null;
                    UI_mcAvailabilitySearch.ondString[UI_mcAvailabilitySearch.ondString.length] = tempOnd;
                    ret = true;
                }else{
                    ret = false;
                    $("#errorDiv").html(erroStr);
                    break;
                }

            }
        }
        return ret;
    };

    isONDValied = function(ind){
        var erroStr = "";
        var error = UI_mcAvailabilitySearch.errorInfosearch;
        var fromStr = $("#selFromLoc_"+ind);
        var toStr = $("#selToLoc_"+ind);
        var depDate = $("#departureDate_"+ind);
        fromStr.removeClass("errorControl");
        toStr.removeClass("errorControl");
        depDate.removeClass("errorControl");
        var cos = $("#selCOS_"+ind).val();
        if ($.trim(fromStr.val()) == ""){
            erroStr += error['ERR001']+"<br/>";
            fromStr.addClass("errorControl");
        }
        if ($.trim(toStr.val()) == ""){
            erroStr += error['ERR002']+"<br/>";
            toStr.addClass("errorControl");
        }
        if (($.trim(fromStr.val()) != "" && $.trim(toStr.val()) != "") && $.trim(fromStr.val()) == $.trim(toStr.val())){
            erroStr += error['ERR003']+"<br/>";
            fromStr.addClass("errorControl");
            toStr.addClass("errorControl");
        }
        if ($.trim(dateToGregorian(depDate.val(),SYS_IBECommonParam.locale)) == ""){
            erroStr += error['ERR007']+"<br/>";
            depDate.addClass("errorControl");
        }else if (dateVal(dataformater(DateToString(dtSysDate))).d > dateVal(dataformater($.trim(dateToGregorian(depDate.val(),SYS_IBECommonParam.locale)))).d){
            erroStr += error['ERR009']+"<br/>";
            depDate.addClass("errorControl");
        }
        return erroStr;
    };

    clearFareQuote = function(){
        $("#trPriceBDPannel").hide();
        $("#farequote_done_Area").hide()
    };

    selAdultsOnChange = function(){
        var intMaxAdults = 9;
        var arrInfants = new Array();
        var arrChild  = new Array();
        var intAdults = Number($("#selAdults").val());
        var strCInfants = $("#selInfants").val();
        var intChilds = $("#selChild").val();

        for (var i = 0 ; i <= intAdults ; i++){
            arrInfants[i] = new Array();
            arrInfants[i][0] = i;
        }
        $("#selInfants").empty();
        $("#selInfants").fillDropDown({dataArray:arrInfants, keyIndex:0, valueIndex:0, firstEmpty:false});

        if (strCInfants > 0) {
            strCInfants = (intAdults >= strCInfants) ? strCInfants : intAdults;
            $("#selInfants").val(strCInfants);
        }

        var arrChild  = new Array();
        for (var i = 0 ; i <= (intMaxAdults - intAdults) ; i++){
            arrChild[i] = new Array();
            arrChild[i][0] = i;
        }
        $("#selChild").empty();
        $("#selChild").fillDropDown({dataArray:arrChild, keyIndex:0, valueIndex:0, firstEmpty:false});
    };

    this.initiatePage = function(){
        $("#selAdults").val(1);
        $("#searchPageAirportMsg").hide();
        $("#browserMsgTr").hide();
        ondHTML = $("#ONDTeml");
        flightTableHTML = $("#flightTable>tr");
        $("#flightTable").empty();
        flightHTML = $("#flightTempl");
        flightHTMLTABLE = $("#flightTempl>table");
        sl_flightHTML = $("#SelectedItemsFL>tr");
        logicalCCHTML = '#SelectedItemsFL';
        slFareQuoteRow = $("#SelectedItemsFL_done>tr");
        slFareQuoteTable = $("#farequote_done_Area>table");
        $("#SelectedItemsFL_done").empty();
        $("#farequote_done_Area").empty();
        //Set Selected Currency
        if (strReqParam != ""){
            UI_mcAvailabilitySearch.selectedCurrency = strReqParam.split("^")[2];
            $("#resSelectedCurrency").val(UI_mcAvailabilitySearch.selectedCurrency);
            $("#selSelectedCurrency").val(UI_mcAvailabilitySearch.selectedCurrency);
        }else{
            if ($("#resSelectedCurrency").val() != ""){
                UI_mcAvailabilitySearch.selectedCurrency = $("#resSelectedCurrency").val();
				$("#selSelectedCurrency").val(UI_mcAvailabilitySearch.selectedCurrency);           
			}
        }
        clearAllDynamic();
    };

    clearAllDynamic = function(){
        $("#multiOND").empty();
        $("#SelectedItemsFL").empty();
        $(UI_mcAvailabilitySearch.flightDetailsPanelHTML).empty();
        $("#divSummaryPane").show();
    }

    showAllSelectedFlight = function(e){
        var tdWidth = ["30%", "10%", "15%", "15%", "15%", "10%", "0%"]
        var cur = parseInt(e.target.id.split("_")[1],10);
        var slFlight = getSelectedFlight(cur, 'OND');
        if (slFlight!=null){
            var thtml = $("#SelectedItemsFL_"+cur);
            thtml.find("#segments").html(breakItems(slFlight.segmentName));
            thtml.find("#flightNumber").html(breakItems(slFlight.flightNumber));
            thtml.find("#departureDate").html(slFlight.departureDateVal);
            thtml.find("#departureTime").html(slFlight.departureTime);
            thtml.find("#arrivalDate").html(slFlight.arrivalDateVal);
            thtml.find("#arrivalTime").html(slFlight.arrivalTime);
            thtml.find("#departureDateLong").html(breakItems(slFlight.departureDateLong));
            thtml.find("#arrivalDateLong").html(breakItems(slFlight.arrivalDateLong));
            thtml.find("#stops").html(slFlight.stops);
            thtml.find("#cabinClass").html(slFlight.cabin);
            //SetData
            $.data($("#SelectedItemsFL_"+cur)[0], "SLF", slFlight);
            var modFl = $("<a>Modify</a>").attr({"href":"javascript:void(0)","id":"MODThis_"+cur});
            mustFilldArray[cur] = true;
            modFl.unbind('click').click(function(e){
                var cur = parseInt(e.target.id.split("_")[1],10);
                $("#FlightDetailsPanel>div").hide();
                $("#SelectedItemsFL_"+cur).hide();
                $("#flightTempl_"+cur).show();
                mustFilldArray[cur] = false;
                setFarequotebutton();
                $.data($("#SelectedItemsFL_"+cur)[0], "SLF", null);
            });
            thtml.find("#moD").empty();
            thtml.find("#moD").append(modFl);
            thtml.css("display", "");
            var next = $.inArray(false, mustFilldArray);
            if (next != -1){
                $("#flightTempl_"+next).show();
            }
            //$("#flightTempl_"+cur).hide();
            $("#shoping_Cart>td").each(function(mid,j){
                $(j).attr("width", tdWidth[mid]);
            });
            $("#shoping_Cart").show();
            $(".clsLcc").hide();
            if (UI_AvailabilitySearch==null){
            	$("#farequoteArea").slideDown('fast');
            }
            
            setFarequotebutton();
            dateChangerjobforLanguage(SYS_IBECommonParam.locale);
            //requote flow
            if(UI_AvailabilitySearch!=null){
            	UI_AvailabilitySearch.loadRequoteSegment();
            }
        }else{
            jAlert("Select a flight for this Origin and Destination")
        }
    };

    setFarequotebutton = function(){
 /*       for (var i=0;i<mustFilldArray.length;i++){
            if (mustFilldArray[i]==false){
                $("#btnfareQuate").hide();
                clearFareQuote();
                break;
            }
            
        }*/
    	var flightDetails = "#flightTempl_";
    	var selectFlight  = "#radSel_";

    	for(i=0 ; i < 5 ; i++){
    	  
    	  if($(flightDetails+i).val() != undefined 
    	     && !($(selectFlight+i+"_0_Y").val() == undefined ||  $(selectFlight+i+"_0_C").val() == undefined)){
    	   
    	    $("#btnfareQuate").attr('disabled','disabled');
    	    
    	  }else if($(flightDetails+i).val() == undefined){
    	  
    	      break;
    	  
    	  }else{
    	    
    	    $("#btnfareQuate").removeAttr('disabled');
    	    
    	  }
    	  
    	}
    	
        $("#btnfareQuate").show();
    };

    fareQuoyeValidate = function(){
        var tBool = false;
        var selectedFlightCount = 0;
        for(var i =0;i<mustFilldArray.length;i++){
        	if(mustFilldArray[i] == true){
        		selectedFlightCount++;        		
        	}
        }
        if (UI_mcAvailabilitySearch.ondString.length == selectedFlightCount){
            for(var i =0;i<mustFilldArray.length;i++){
                if ( $.data($("#SelectedItemsFL_"+i)[0], "SLF") != null){
                    UI_mcAvailabilitySearch.ondString[i].flightRPHList = $.data($("#SelectedItemsFL_"+i)[0], "SLF").rphList;
                    UI_mcAvailabilitySearch.ondString[i].classOfService = $.data($("#SelectedItemsFL_"+i)[0], "SLF").cabin;
                    tBool = true;
                }else{
                    tBool = false;
                    break;
                }
            }
        }else{
            tBool = false;
        }
        return tBool;
    };

    loadFareQuote = function(){
        if (fareQuoyeValidate()){
        	UI_mcAvailabilitySearch.loadingProgress();
            var data = createSearchSubmitData();
            data['searchParams.ondListString'] = JSON.stringify(UI_mcAvailabilitySearch.ondString, null);
            $("#resOndListStr").val(data['searchParams.ondListString']);
            data['searchParams.selectedCurrency'] = UI_mcAvailabilitySearch.selectedCurrency;
            data['searchParams.adultCount'] = $("#resAdultCount").val();
            data['searchParams.childCount'] = $("#resChildCount").val();
            data['searchParams.infantCount'] = $("#resInfantCount").val();
            data['searchParams.bookingType'] = "NORMAL";
            data['searchParams.fareType'] = "A";

            //Setting flexi params
            var ondQuoteFlexi = {};
            for ( var i = 0; i < UI_mcAvailabilitySearch.ondString.length; i++) {
                ondQuoteFlexi[i] = true;
            }

            $("#resOndQuoteFlexi").val(JSON.stringify(ondQuoteFlexi, null));

            data['searchParams.ondQuoteFlexiStr'] = JSON.stringify(ondQuoteFlexi, null);
            data['searchParams.fareQuoteOndSegLogicalCCSelection'] = JSON.stringify(UI_mcAvailabilitySearch.ondLogicalCCSelection, null);
            data['searchParams.ondSelectedFlexiStr'] = JSON.stringify(UI_mcAvailabilitySearch.ondFlexiSelection, null);
            data['searchParams.flightSearchOndFlexiSelectionStr'] = JSON.stringify(UI_mcAvailabilitySearch.ondFlexiSelection, null);
            data['searchParams.preferredBundledFares'] = JSON.stringify(UI_mcAvailabilitySearch.ondBundledFareSelection, null);
            data['searchParams.preferredBookingCodes'] = JSON.stringify(UI_mcAvailabilitySearch.ondBookingClassSelection, null);
            data['searchParams.ondSegBookingClassStr'] = JSON.stringify(UI_mcAvailabilitySearch.segmentBookingClassSelection, null);
            data['searchParams.promoCode'] = $("#resPromoCode").val();
            data["searchParams.searchSystem"] = UI_mcAvailabilitySearch.setDefaultSearchOption(UI_mcAvailabilitySearch.ondString);
            //TODO Send the Call and process the response
            var url = "multiCityFareQuote.action";
            $.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,
                success: loadFareQuoteProcess, error:UI_commonSystem.setErrorStatus, cache: false});
            return false;
        } else {
        	jAlert("Select flights for all ONDs");
        	return false;
        }
    };

    mcSetFare = function(resObj) {
        var hideTaxBDRows = true;
        var hideSurchargeBDRows = true;
        var fareQuote = resObj.fareQuote;
        if (fareQuote != null && fareQuote.segmentFare != null) {
            insertParam("st","fareQuote");
            $("#FlightDetailsPanel, #farequoteArea").hide();
            $("#requestSessionIdentifier").val(resObj.requestSessionIdentifier);
            UI_mcAvailabilitySearch.fareQuote = fareQuote;
            promotionInfo = resObj.promoInfo;

            var promoInfo = $.airutil.dom.cloneObject(promotionInfo);
            $('#respromotionInfo').val($.toJSON(promoInfo));

            $("#priceBreakDownTemplate").nextAll().remove();
            $("#taxBreakDownTemplate").nextAll().remove();
            $("#surchargeBreakDownTemplate").nextAll().remove();

            var fareRuleMsg = '';
            if(resObj.showFareRules){
                $("#lblFareRules").show().parent().show();
                for(var i = 0; i < resObj.fareRules.length; i++){
                    if(resObj.fareRules[i].comment!= null && resObj.fareRules[i].comment!=''){
                        fareRuleMsg += '<b>'+ resObj.fareRules[i].ondCode + '</b><br /> &nbsp;&nbsp;'+ resObj.fareRules[i].comment + '<br />' ;
                    }
                }
                if(fareRuleMsg==''){
                    $("#lblFareRules").parent().hide();
                }
            } else {
                $("#lblFareRules").parent().hide();
            }

            if (UI_mcAvailabilitySearch.carrier!="W5"){
                $("#lblFareRules").tooltip({
                    bodyHandler: function() {
                        return fareRuleMsg;
                    },
                    showURL: false,
                    delay: 0,
                    track: true
                });
            }else{
                $("#lblFareRules").unbind("click").bind("click", function(){
                    var intX = ((window.screen.height - 600) / 2);
                    var intY = ((window.screen.width - 600) / 2);
                    var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=600,height=600,resizable=no,top=" + intX + ",left=" + intY;
                    var url = "";
                    var consoleRef = window.open('', 'ToolChild',strProp);

                    consoleRef.document.writeln(
                        '<html><head></head>'
                            +'<body bgcolor=white onLoad="self.focus()">'
                            +fareRuleMsg
                            +'</body></html>');

                });
            }

            //Clone the object as we need to get the original value if flexi is deselected
            var segSegFareQuote = $.airutil.dom.cloneObject(fareQuote.segmentFare);
            $("#priceBreakDownTemplate").iterateTempletePB({templeteName:"priceBreakDownTemplate", data:segSegFareQuote, dtoName:"paxWise"});
            for (var pp=0; pp<segSegFareQuote.length;pp++){
    			$(".pb-class").each(function(jj,ppOBJ){
    				if(pp%2==0){
    	                $("#priceBreakDownTemplate_"+pp+""+jj).addClass("even");
    	            }
    			});
    		}
            if (fareQuote.perPaxTaxBD != null && fareQuote.perPaxTaxBD.length >0){
                var  perPaxTaxBD = $.airutil.dom.cloneObject(fareQuote.perPaxTaxBD);
                $("#taxBreakDownTemplate").iterateTempleteTaxBD({templeteName:"taxBreakDownTemplate", data:perPaxTaxBD, dtoName:"perPaxTaxBD"});
                $('#taxBDDisplayLink').unbind('click');
                $('#taxBDDisplayLink').click(function() {
                    $(".taxBDDisplayRows").toggle();
                    if( $(".taxBDDisplayRows").is(":visible") ){
                        hideTaxBDRows = false;
                    }
                });

                if (hideTaxBDRows){
                    $(".taxBDDisplayRows").hide();
                }

            } else {
                // hide the set of rows displaying tax break down information.
                $(".taxBDDisplayRows").hide();
                $(".taxBDDisplayLinkRows").hide();
            }

            if (fareQuote.perPaxSurchargeBD !=null && fareQuote.perPaxSurchargeBD.length >0 ) {

                var  perPaxSurchargeBD = $.airutil.dom.cloneObject(fareQuote.perPaxSurchargeBD);
                $("#surchargeBreakDownTemplate").iterateTempleteTaxBD({templeteName:"surchargeBreakDownTemplate", data:perPaxSurchargeBD, dtoName:"perPaxSurchargeBD"});

                $('#surchargeBDDisplayLink').unbind();

                $('#surchargeBDDisplayLink').click(function() {
                    $(".surchargeBDDisplayRows").toggle();
                    if( $(".surchargeBDDisplayRows").is(":visible") ){
                        hideSurchargeBDRows = false;
                    }
                });

                if (hideTaxBDRows){
                    $(".surchargeBDDisplayRows").hide();
                }

            } else {
                // hide the set of rows displaying surcharge break down information.
                $(".surchargeBDDisplayRows").hide();
                $(".surchargeBDDisplayLinkRows").hide();
            }

            setTotalFare({inBaseCurr:fareQuote.inBaseCurr, inSelectedCurr:fareQuote.inSelectedCurr});

            setPromoDiscountAmount(UI_mcAvailabilitySearch.fareQuote);
            
            ondFlexiAvailability = resObj.ondWiseTotalFlexiAvailable;
            ondFlexiCharges = resObj.ondWiseTotalFlexiCharge;
            
            setAvailableLogicalCabinClasses(resObj.ondLogicalCCList);

            $("#trPriceBDPannel").slideDown();
            $("#farequote_done_Area").show()
            $.fn.summaryPanel.updateFlightPriceDetails();
            UI_mcAvailabilitySearch.setAvailableBundleFareLCClassStr(resObj.ondLogicalCCList);

            $("#resOndAvailbleFlexiStr").val($.toJSON(ondFlexiAvailability));
            $("#resOndwiseFlexiChargesForAnci").val($.toJSON(ondFlexiCharges));
            UI_mcAvailabilitySearch.airportMessage = resObj.airportMessage;
            UI_mcAvailabilitySearch.loadAirportSearchMessages();

        } else {
            UI_mcAvailabilitySearch.fareQuote = null;
            $("#trPriceBDPannel").slideUp();
            $("#farequote_done_Area").hide()
            $("#FlightDetailsPanel, #farequoteArea").show();
            var msg = UI_mcAvailabilitySearch.labels['faresNotAvailable'];
            msg = (msg==null?resObj.messageTxt:msg);
            jAlert(msg, 'Alert');
        }
    };

    setPromoDiscountAmount = function(fareQuote){

        var discountAmount = fareQuote.inBaseCurr.totalIbePromoDiscount;

        if (fareQuote.hasPromoDiscount && parseFloat(discountAmount) > 0) {
            var curr = fareQuote.inBaseCurr.currency;
            if (fareQuote.inBaseCurr.currency != fareQuote.inSelectedCurr.currency) {
                discountAmount = fareQuote.inSelectedCurr.totalIbePromoDiscount;
                curr = fareQuote.inSelectedCurr.currency;
            }

            if(fareQuote.deductFromTotal){
                var promoAppType = promotionInfo.applicability;
                $("#promoDiscountPanel").show();
                if (promoAppType == "OB"){
                    $(".lblPromoDiscount").text(UI_mcAvailabilitySearch.labels.lblPromoDiscount);
                    $(".promoDiscountAmount").text("-" + discountAmount + " " + curr);
                    $("#promoDiscountPanel_OB").css("width",(($("#resReturnFlag").val()=="true")?"49%":"100%")).show();
                    $("#promoDiscountPanel_IB").css("width",(($("#resReturnFlag").val()=="true")?"49%":"0%")).hide();
                }else if(promoAppType == "IB"){
                    $(".lblPromoDiscount").text(UI_mcAvailabilitySearch.labels.lblPromoDiscount);
                    $(".promoDiscountAmount").text("-" + discountAmount + " " + curr);
                    $("#promoDiscountPanel_IB").css("width","49%").show();
                    $("#promoDiscountPanel_OB").css("width","49%").hide();
                }else{
                    $(".lblPromoDiscount").text(UI_mcAvailabilitySearch.labels.lblPromoDiscount);
                    $(".promoDiscountAmount").text("-" + discountAmount + " " + curr);
                    $("#promoDiscountPanel_IB").css("width","99%").show();
                    $("#promoDiscountPanel_OB").css("width","0%").hide();
                }
                $("#promoDiscountPanelCredit").hide();
            } else {
                $(".lblPromoDiscount").text(UI_mcAvailabilitySearch.labels.lblPromoDiscountCredit);
                $(".promoDiscountAmount").text(discountAmount + " " + curr);
                $("#promoDiscountPanelCredit").show();
                $("#promoDiscountPanel").hide();
            }

            $("#tempPromoBanner").remove();
            if(fareQuote.deductFromTotal){
                $("#lblPromoDiscount").text(UI_mcAvailabilitySearch.labels.lblPromoDiscount);
            } else {
                $("#lblPromoDiscount").text(UI_mcAvailabilitySearch.labels.lblPromoDiscountCredit);
            }

            if(promotionInfo != null){
                var strMSG = promotionInfo.description;
                if(strMSG != undefined && strMSG != null){
                    var discountType = promotionInfo.discountType;
                    var strDiscountBnr = '';
                    if(discountType == promoDiscountType.PERCENTAGE){
                        strDiscountBnr = promotionInfo.farePercentage + '%';
                    } else if(discountType == promoDiscountType.VALUE) {
                        strDiscountBnr = curr + ' ' + discountAmount;
                    }

                    strMSG = strMSG.replace("{amount}", strDiscountBnr);
                    strMSG = strMSG.replace("{Amount}", strDiscountBnr);


                    var tBanner = $("<span id='tempPromoBanner' class='tbanner'>"+strMSG+"</span>").addClass("spMsg");

                    var topPx = 125;
                    if($("#tempDiscountBanner") != null && $("#tempDiscountBanner").length > 0){
                        topPx = 220;
                    }
                    $(".rightColumn").before(tBanner);
                    rePositionBanner(tBanner,topPx);
                }
            }

        } else {
            $("#promoDiscountPanel").hide();
            $("#tempPromoBanner").remove();
        }

    }

    rePositionBanner = function(tBanner,topPx){

        var pos = $("#tabs1").position();
        if (tBanner==undefined){
            topPx = 125;
            $(".tbanner").each(function(){
                var tBanner = $(this);
                tBanner.css({
                    "position":"absolute",
                    "top":pos.top + topPx,
                    "left": pos.left + 755,
                    "borderRadius":8,
                    "width":160
                });
                topPx += 75;
            });
        }else{
            tBanner.css({
                "position":"absolute",
                "top":pos.top + topPx,
                "left": pos.left + 755,
                "borderRadius":8,
                "width":160
            });
        }
    }

    getCommonLogicalClasses = function(ondLogicalCCList){
        var viewLCClist = [];
        var testLCClist = [];
        if(ondLogicalCCList != undefined && ondLogicalCCList != null){
            for ( var i = 0; i < ondLogicalCCList.length; i++) {
                var ondLogicalCCObj = ondLogicalCCList[i][0];
                if(ondLogicalCCObj != undefined && ondLogicalCCObj != null){
                    var availableLogicalCCs = ondLogicalCCObj.availableLogicalCCList;
                    var ondSeq = ondLogicalCCObj.sequence;
                    for (var k =0;k<availableLogicalCCs.length;k++){
                        var lcCode = availableLogicalCCs[k].logicalCCCode + "_" + ((availableLogicalCCs[k].withFlexi)?"Y":"N") + "_" + (availableLogicalCCs[k].bundledFarePeriodId||"N");
                        if($.inArray(lcCode,testLCClist)<0){
                            testLCClist[testLCClist.length] = lcCode;
                            var rew = [];
                            rew[0] = lcCode;
                            var t = {
                                "logicalCCDesc": availableLogicalCCs[k].logicalCCDesc,
                                "comment": availableLogicalCCs[k].comment,
                                "ondSeq": ondSeq,
                                "bookingCodes": availableLogicalCCs[k].bookingCodes,
                                "segBookingClasses": availableLogicalCCs[k].segmentBookingClasses,
                                "logicalCCCode":availableLogicalCCs[k].logicalCCCode,
                                "bundledFarePeriodId":availableLogicalCCs[k].bundledFarePeriodId,
                                "withFlexi":availableLogicalCCs[k].withFlexi,
                                "selected":availableLogicalCCs[k].selected,
                                "imageUrl": availableLogicalCCs[k].imageUrl
                            }
                            rew[1] = t;
                            viewLCClist[viewLCClist.length] =rew;
                        }
                    }
                }
            }
        }
        return viewLCClist;
    }


    setAvailabilityOfLogicalCabin = function(index, commonLCC, ondLogicalCCList, slFlight){
        var tdWidth = ["20%", "8%", "10%", "10%", "8%"];
        var flHTMLROW = slFareQuoteRow.clone();
        flHTMLROW.attr("id", "SelectedItemsFL_done_"+index);
        var lcWidth =parseInt(40/commonLCC.length,10);
        if(commonLCC.length>0){
            for (var cc=0;cc<commonLCC.length;cc++){
                slFlight[commonLCC[cc][0]] = "N/A";
                var obj = commonLCC[cc][1];
                var tdHtml = $('<td class="GridItems bdRight tdFareColumn" align="center" width="'+lcWidth+'">'+
                    '<label id="'+commonLCC[cc][0]+'" title="'+obj.comment+'">'+obj.logicalCCDesc+'</label>'+
                    '</td>');
                flHTMLROW.append(tdHtml);
            }

            for ( var i = 0; i < ondLogicalCCList.length; i++) {
                var ondLogicalCCObj = ondLogicalCCList[i][0];
                if(ondLogicalCCObj != undefined && ondLogicalCCObj != null){
                    var ondSeq = ondLogicalCCObj.sequence;
                    if(ondSeq == index){
                        var availableLogicalCCs = ondLogicalCCObj.availableLogicalCCList;
                        if(availableLogicalCCs != undefined && availableLogicalCCs != null){
                            for ( var j = 0; j < availableLogicalCCs.length; j++) {
                                var lcCode = availableLogicalCCs[j].logicalCCCode + "_" + ((availableLogicalCCs[j].withFlexi)?"Y":"N") + "_" + (availableLogicalCCs[j].bundledFarePeriodId||"N");
                                for (var cc=0;cc<commonLCC.length;cc++){
                                    if (lcCode===commonLCC[cc][0]){
                                        var obj = availableLogicalCCs[j];
                                        var value = "radio_"+ ondSeq + "_" + obj.logicalCCCode;
                                        value += obj.withFlexi ? '_Y' : '_N';
                                        value += '_' + obj.bundledFarePeriodId + '_' + $.toJSON(obj.bookingClasses) + '_' + cc + '_' + obj.selected;
                                        value += '_' + $.toJSON(obj.segmentBookingClasses);
                                        slFlight[commonLCC[cc][0]] = value;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
            flHTMLROW.find("label").each(function(indx){
                var tempData = ""
                if(typeof(slFlight[this.id])==="object"){
                    tempData = (slFlight[this.id]).join("<br/>");
                }else{
                    tempData = slFlight[this.id];
                }
                $(this).html(tempData);

                if(tempData.indexOf("radio_")>-1){
                    var temdataArray = tempData.split("_");
                    var name = 'logical_' + temdataArray[1];
                    var value = temdataArray[1] + "_" + temdataArray[2];
                    value += "_" +temdataArray[3] + '_' + temdataArray[4] ;
                    value += '_' + temdataArray[5] + '_' + temdataArray[8];
                    
                    var logicalRadio = $("<input>").attr({"type":"radio",
                        "id":temdataArray[1] + '_' + temdataArray[6],
                        "value" : value,
                        "name" : name,
                        "checked": (temdataArray[7]=="true")?"checked":""});
                    
                    var withFlexi = true;
                    if(temdataArray[3] != "Y" || (ondFlexiAvailability != null && ondFlexiAvailability.length > ondSeq
                    		&& !ondFlexiAvailability[ondSeq])){
                        withFlexi = false;
                    }
                    
                    var bundledFarePeriodId = null;
                    if(temdataArray[4] != '' && temdataArray[4] != 'null'){
                        bundledFarePeriodId = temdataArray[4];
                    }
                    
                    var bookingClasses = null;
                    if(temdataArray[5] != '' && temdataArray[5] != 'null'){
                        bookingClasses = $.parseJSON(temdataArray[5]);
                    }

                    var segBookingClasses = null;
                    if(temdataArray[8] != '' && temdataArray[8] != 'null'){
                    	segBookingClasses = $.parseJSON(temdataArray[8]);
                    }
                    
                    //Set Default selection
                    if(temdataArray[7]=="true"){
	                    setLogicalCCSelection({"ondSeq": temdataArray[1], "logicalCCCode": temdataArray[2],
	                    "withFlexi": withFlexi, "bundledFarePeriodId": bundledFarePeriodId,
	                    "bookingClasses": bookingClasses, "segBookingClasses" : segBookingClasses});
                    }
                    

                    logicalRadio.off("click").on("click",function(e){
                        var arrVal = e.target.value.split("_");
                        var ondSeq = parseInt(arrVal[0], 10);
                        var logicalCCCode = arrVal[1];
                        var withFlexi = true;
                        if(arrVal[2] != "Y" || (ondFlexiAvailability != null && ondFlexiAvailability.length > ondSeq
                        		&& !ondFlexiAvailability[ondSeq])){
                            withFlexi = false;
                        }

                        var bundledFarePeriodId = null;
                        if(arrVal[3] != '' && arrVal[3] != 'null'){
                            bundledFarePeriodId = arrVal[3];
                        }

                        var bookingClasses = null;
                        if(arrVal[4] != '' && arrVal[4] != 'null'){
                            bookingClasses = $.parseJSON(arrVal[4]);
                        }
                        
                        var segBookingClasses = null;
                        if(arrVal[5] != '' && arrVal[5] != 'null'){
                        	segBookingClasses = $.parseJSON(arrVal[5]);
                        }

                        setLogicalCCSelection({"ondSeq": ondSeq, "logicalCCCode": logicalCCCode,
                            "withFlexi": withFlexi, "bundledFarePeriodId": bundledFarePeriodId,
                            "bookingClasses": bookingClasses, "segBookingClasses" : segBookingClasses});
                        loadFareQuote();
                    });
                    $(this).parent().empty().append(logicalRadio);
                    logicalRadio.prop("checked", temdataArray[7]=="true");
                }

            });
            if(index%2==0){
                flHTMLROW.addClass("even")
            }
            $("#SelectedItemsFL_done").append(flHTMLROW);
        }
        dateChangerjobforLanguage(SYS_IBECommonParam.locale);
    }

    setAvailableLogicalCabinClasses = function(ondLogicalCCList){
        $("#farequote_done_Area")
            .empty()
            .append(slFareQuoteTable.clone());
        
        var commonLCC = getCommonLogicalClasses(ondLogicalCCList);
        var wd = (30/commonLCC.length);
        $.each(commonLCC, function(x,LC){
            var obj = commonLCC[x][1];
            var thHtml = $('<td class="gridHD fareClass bdRight Y_Y_class normal-table" align="center" width="'+wd+'%">' +
                '<label class="gridHDFont fntBold">'+obj.logicalCCDesc+
                ' <span role="'+obj.comment+ '|' + obj.imageUrl+'" class="ttip">&nbsp;<img src="../images/spacer_no_cache.gif" width="7" height="13" alt="">'+
                '</span></label></td>');
            $("#shoping_Cart_head").append(thHtml);
        });
        //$.data($("#SelectedItemsFL_"+cur)[0], "SLF", slFlight);
        var selectedFlightWithLogicalCCs = [];
        $("#SelectedItemsFL>tr").each(function(index){
            var slFlight = $.data(this, "SLF");
            setAvailabilityOfLogicalCabin(index, commonLCC, ondLogicalCCList, slFlight);
        });   
        
        $(".ttip").tooltip({
            bodyHandler: function() {
                var text = $(this).attr('role').split("|");
                var html = '';
                if(text[1] != undefined && text[1] != null 
    	    			&& text[1] != '' && text[1] != 'null'){
    	    		html += "<img src='"+text[1]+"'/><br/>";
    	    	}
    	 		return html; 
            },
            showURL: false,
            delay: 0,
            track: true,
            top: 10,
            extraClass: "caltooltip"
        });

        /*		if(ondLogicalCCList != undefined && ondLogicalCCList != null){
         for ( var i = 0; i < ondLogicalCCList.length; i++) {
         var ondLogicalCCObj = ondLogicalCCList[i][0];
         if(ondLogicalCCObj != undefined && ondLogicalCCObj != null){
         var ondSeq = ondLogicalCCObj.sequence;
         var availableLogicalCCs = ondLogicalCCObj.availableLogicalCCList;
         var parentElem = $(logicalCCHTML + '_' + ondSeq);

         if(availableLogicalCCs != undefined && availableLogicalCCs != null){
         parentElem.empty();
         for ( var j = 0; j < availableLogicalCCs.length; j++) {
         var logicalCCObj = availableLogicalCCs[j];
         var value = ondSeq + "_" + logicalCCObj.logicalCCCode;
         value += (logicalCCObj.withFlexi) ? '_Y' : '_N';
         value += '_' + logicalCCObj.bundledFarePeriodId + '_' + logicalCCObj.bookingCode;

         var title = logicalCCObj.comment;
         var name = 'logical_' + ondSeq;

         var logicalRadio = $("<input>").attr({"type":"radio",
         "id":ondSeq + '_' + j,
         "value" : value,
         "title" :title,
         "name" : name,
         "checked": logicalCCObj.selected});

         //Set Default selection
         if(logicalCCObj.selected){
         setLogicalCCSelection({ondSeq: ondSeq, logicalCCCode: logicalCCObj.logicalCCCode,
         withFlexi: logicalCCObj.withFlexi, bundledFarePeriodId: logicalCCObj.bundledFarePeriodId,
         bookingClass: logicalCCObj.bookingCode});
         }

         logicalRadio.unbind('click').click(function(e){
         var arrVal = e.target.value.split("_");
         $("#trPriceBDPannel").slideUp();
         $("#farequote_done_Area").hide()
         setFarequotebutton();

         var ondSeq = parseInt(arrVal[0], 10);
         var logicalCCCode = arrVal[1];
         var withFlexi = true;
         if(arrVal[2] != "Y"){
         withFlexi = false;
         }

         var bundledFarePeriodId = null;
         if(arrVal[3] != '' && arrVal[3] != 'null'){
         bundledFarePeriodId = arrVal[3];
         }

         var bookingClass = null;
         if(arrVal[4] != '' && arrVal[4] != 'null'){
         bookingClass = arrVal[4];
         }

         setLogicalCCSelection({ondSeq: ondSeq, logicalCCCode: logicalCCCode,
         withFlexi: withFlexi, bundledFarePeriodId: bundledFarePeriodId,
         bookingClass: bookingClass});

         });

         var labelElem = $("<label>"+logicalCCObj.logicalCCDesc+"</label>").attr({"title" : title});


         parentElem.append(logicalRadio);
         parentElem.append(labelElem);
         parentElem.append('&nbsp;&nbsp;');

         }
         }
         }

         }
         $(".clsLcc").show();
         }*/
    }

    setLogicalCCSelection = function(params){

    	UI_mcAvailabilitySearch.segmentBookingClassSelection[params.ondSeq] = null;
    	UI_mcAvailabilitySearch.ondBookingClassSelection[params.ondSeq] = null;
    	if(params.bookingClasses != null){    		
    		if(params.bookingClasses.length > 1){
    			UI_mcAvailabilitySearch.segmentBookingClassSelection[params.ondSeq] = params.segBookingClasses;
    		} else {
    			UI_mcAvailabilitySearch.ondBookingClassSelection[params.ondSeq] = params.bookingClasses[0];
    		}
    	}
    	
        UI_mcAvailabilitySearch.ondLogicalCCSelection[params.ondSeq] =  params.logicalCCCode;
        UI_mcAvailabilitySearch.ondFlexiSelection[params.ondSeq] = params.withFlexi;
        UI_mcAvailabilitySearch.ondBundledFareSelection[params.ondSeq] = params.bundledFarePeriodId;

        $("#resFareQuoteLogicalCCSelection").val($.toJSON(UI_mcAvailabilitySearch.ondLogicalCCSelection));
        $("#resOndSelectedFlexiStr").val($.toJSON(UI_mcAvailabilitySearch.ondFlexiSelection));
        //Keep track of flexi selection done in flight search page
        $("#resflightSearchOndFlexiSelection").val($.toJSON(UI_mcAvailabilitySearch.ondFlexiSelection));
        $("#resOndBundleFareStr").val($.toJSON(UI_mcAvailabilitySearch.ondBundledFareSelection));
        $("#resOndBookingClassStr").val($.toJSON(UI_mcAvailabilitySearch.ondBookingClassSelection));
        $("#resOndSegBookingClassStr").val($.toJSON(UI_mcAvailabilitySearch.segmentBookingClassSelection));
    }

    /**
     * Set Total Fare
     */
    setTotalFare = function(fareInCurrency) {
        var totalPriceInBase = fareInCurrency.inBaseCurr.totalPrice;
        $("#totalAmount").text(totalPriceInBase + " " + fareInCurrency.inBaseCurr.currency);

        if (fareInCurrency.inBaseCurr.currency != fareInCurrency.inSelectedCurr.currency) {
            var totalPriceInSelCur = fareInCurrency.inSelectedCurr.totalPrice;
            $("#totalAmountSel").text(totalPriceInSelCur + " " + fareInCurrency.inSelectedCurr.currency);
            $("#selectedTotalPanel").show();
            $("#selectedTotalPanel").find('#lblTotal').addClass('gridHDFont');
            $("#selectedTotalPanel").find('#totalAmountSel').addClass('gridHDFont');
        }
    };

    wrapAndDebugError = function(id, response){
        var msg = (response==null) ? null:response.messageTxt;
        if(msg == null || msg == ''){
            if(msg == null) return ; //no errors to be shown as browser back create a bug in ajaxSubmit plugin
            if(console !=null  && console.log !=null){
                console.log(id);
                console.log(response);
            }
            msg = 'REF:'+id + ' Error occured. Please report with reference';
        }
        UI_commonSystem.loadErrorPage({messageTxt:msg});
    }


    loadFareQuoteProcess = function(response){
        if(response != null && response.success == true) {
            mcSetFare(response);
            $("#trAccept, #btnContinue").show();
            $("#btnfareQuate").hide();
            $("#lblCurrencySupportMessage").text($("#lblCurrencySupportMessage").text().replace("{0}", UI_mcAvailabilitySearch.currencyCode));
        }else if (response != null) {
            fareQuote = "";
            $("#trPriceBDPannel").slideUp();
            $("#farequote_done_Area").hide()
            $("#FlightDetailsPanel, #farequoteArea").show();
            var msg = UI_mcAvailabilitySearch.labels['faresNotAvailable'];
            msg = (msg==null?response.messageTxt:msg);
            jAlert(msg, 'Alert');
        }else{
            wrapAndDebugError('003',response);
        }
        UI_mcAvailabilitySearch.hideProgress();
    }

    /**
     * Data lists to be used when dynamic ond lists are enabled will be populated
     * by this method
     */
    initDynamicOndList = function() {

        // Populate OnD data structure only for ond's which has current
        // operating carrier

        var defaultCarrier = UI_Top.holder().GLOBALS.defaultCarrierCode;
        var lang = UI_Top.holder().GLOBALS.sysLanguage;

        if ($("#locale").val() != null && $("#locale").val() != "") {
            lang = $("#locale").val();
        }

        for ( var originIndex in origins) {

            var tempDestArray = [];
            for ( var destIndex in origins[originIndex]) {
                // check whether operating carrier exists
                var curDest = origins[originIndex][destIndex];

                if(curDest[0] != null){
                    var currentSize = tempDestArray.length;
                    tempDestArray[currentSize] = [];
                    tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
                    tempDestArray[currentSize][1] = (airports[curDest[0]][lang] != null ? airports[curDest[0]][lang]
                        : airports[curDest[0]][UI_Top.holder().GLOBALS.sysLanguage]);
                    tempDestArray[currentSize][2] = currencies[curDest[2]]['code'];
                }

            }

            // Add entry if at least one destination is there to final ond map
            if (tempDestArray.length > 0) {

                tempDestArray.sort(UI_mcAvailabilitySearch.customSort);
                //UI_mcAvailabilitySearch.ondDataMap[airports[originIndex]['code']] = tempDestArray;

                var currentOriginSize = UI_mcAvailabilitySearch.originList.length;
                UI_mcAvailabilitySearch.originList[currentOriginSize] = [];
                UI_mcAvailabilitySearch.originList[currentOriginSize][0] = airports[originIndex]['code'];
                UI_mcAvailabilitySearch.originList[currentOriginSize][1] = (airports[originIndex][lang] != null ? airports[originIndex][lang]
                    : airports[originIndex][UI_Top.holder().GLOBALS.sysLanguage]);

                if (UI_mcAvailabilitySearch.hubDesc == ""
                    && UI_mcAvailabilitySearch.hub == airports[originIndex]['code']) {
                    UI_mcAvailabilitySearch.hubDesc = (airports[originIndex][lang] != null ? airports[originIndex][lang]
                        : airports[originIndex][UI_Top.holder().GLOBALS.sysLanguage]);
                }

                UI_mcAvailabilitySearch.originList.sort(UI_mcAvailabilitySearch.customSort);

            }


        }

        for ( var i = 0 ; i < currencies.length ; i++) {

            UI_mcAvailabilitySearch.currencyList[i] = [];
            UI_mcAvailabilitySearch.currencyList[i][0] = currencies[i]['code'];
            UI_mcAvailabilitySearch.currencyList[i][1] = (currencies[i][lang] != null ? currencies[i][lang]
                : currencies[i][UI_Top.holder().GLOBALS.sysLanguage]);

        }

        UI_mcAvailabilitySearch.currencyList.sort(UI_mcAvailabilitySearch.customSort);

    };

    this.captureSuccess = function (){
        if(UI_mcAvailabilitySearch.fareQuote!=null){
            if (!$("#chkTerms").is(':checked')){
                jAlert(UI_mcAvailabilitySearch.errorInfo["ERR042"],'Alert');
                return false;
            }else {
                var fareQuote = $.airutil.dom.cloneObject(UI_mcAvailabilitySearch.fareQuote);
                $('#resFareQuote').val($.toJSON(fareQuote));
                $('#resSelectedFlights').val($.toJSON({flightSegments:UI_mcAvailabilitySearch.setSelectedFlightInfo(),isReturnFlight:false}));
                $("#resOndListStr").val($.toJSON(UI_mcAvailabilitySearch.ondString));
                var hidParam = '';
                insertParam("searchCrit", null);
                $('#frmFare').attr('action',SYS_IBECommonParam.securePath+'loadContainer.action');
                $('#frmFare').append(hidParam);
                $('#frmFare').attr('target','_top');
                $('#frmFare').submit();
            }
        }else{
            if(UI_AvailabilitySearch ==null ||  !UI_AvailabilitySearch.isRequoteFlightSearch){
                jAlert(UI_mcAvailabilitySearch.errorInfo["ERR039"],'Alert');
            }
            return false;
        }
    }

    this.setSelectedFlightInfo = function(){    	

        //fmt yyMMddHHmm
        function decodeAADateForGetTime(dstr) {
            var yy = parseInt(dstr.substring(0, 2), 10) + 2000;
            var MM = parseInt(dstr.substring(2, 4), 10) - 1;
            var dd = parseInt(dstr.substring(4, 6), 10);
            var hh = parseInt(dstr.substring(6, 8), 10);
            var mm = parseInt(dstr.substring(8, 10), 10);
            
            var utcDateMillis = Date.UTC(yy, MM,dd,hh,mm,0);
            return new Date(utcDateMillis);           
            
        }
        
        function decodeAADateForGetDate(dstr) {
            var yy = parseInt(dstr.substring(0, 2), 10) + 2000;
            var MM = parseInt(dstr.substring(2, 4), 10) - 1;
            var dd = parseInt(dstr.substring(4, 6), 10);
            var hh = parseInt(dstr.substring(6, 8), 10);
            var mm = parseInt(dstr.substring(8, 10), 10);
            return new Date(yy, MM,dd,hh,mm,0);           
            
        }
                
    	// get the dateValue from date
    	function getDateTimeFromJason(jsdate){
    		var a = jsdate.split('T');
    		var d = a[0].split('-');
    		var t = a[1].split(':');
    		return d[0].substr(2,2) +""+ d[1] +""+ d[2] +""+ t[0] +""+ t[1];
    	}

        function ddFmt(dst){
            return $.datepicker.formatDate('D, dd M y', dst);
        }

        function leadingZero(val) {
            var str = val.toString();
            if(str.length == 1) {
                str = '0' + str;
            }

            return str;
        }

        function getTimeOnly(dstr) {
            return leadingZero(dstr.getHours())+":" +leadingZero(dstr.getMinutes());
        }

        var list= [];
        for(var i = 0 ; i < UI_mcAvailabilitySearch.ondString.length; i++){
            var ondResultsData = $.data($("#flightTempl_"+i)[0], "ResultsData");
            var _seletedFlights = $("#flightTempl_"+i).find("input.fl_select:checked");
            if (_seletedFlights.length>0){
                var _selONDCabin = _seletedFlights[0].id.split("_");
                var tOBJ = ondResultsData[_selONDCabin[2]];

                for(var j=0;j<tOBJ.flightRPHList.length;j++){
                    var t = {};

                    var arriDateObj = decodeAADateForGetDate(tOBJ.arrivalTimeList[j]);
                    var depaDateObj = decodeAADateForGetDate(tOBJ.depatureTimeList[j]);

                    t.arrivalDate = ddFmt(arriDateObj);
                	t.arrivalDateValue = getDateTimeFromJason(getJsonDate(arriDateObj.getTime()));
                    t.arrivalDateLong = arriDateObj;
                    t.arrivalTerminalName = null;
                    t.arrivalTime= getTimeOnly(arriDateObj);
                    t.arrivalTimeLong= decodeAADateForGetTime(tOBJ.arrivalTimeList[j]).getTime();
                    t.arrivalTimeZuluLong= decodeAADateForGetTime(tOBJ.arrivalTimeZuluList[j]).getTime();
                    t.carrierCode= tOBJ.carrierList[j];
                    t.carrierImagePath= null;
                    t.departureDate=  ddFmt(depaDateObj);
                    t.departureDateValue = getDateTimeFromJason(getJsonDate(depaDateObj.getTime()));
                    t.departureDateLong = arriDateObj;
                    t.departureTerminalName= null;
                    t.departureTime= getTimeOnly(depaDateObj);
                    t.departureTimeLong= decodeAADateForGetTime(tOBJ.depatureTimeList[j]).getTime();
                    t.departureTimeZuluLong= decodeAADateForGetTime(tOBJ.departureTimeZuluList[j]).getTime();
                    t.domesticFlight= tOBJ.domesticFlightList[j];
                    t.duration= tOBJ.flightDurationList[j];
                    t.flightNumber= tOBJ.flightNoList[j];
                    t.flightRefNumber= tOBJ.flightRPHList[j];
                    t.flightSeqNo= '';
                    t.returnFlag= false;
                    t.segmentCode= tOBJ.segmentFullNameList[j];
                    t.segmentId= null;
                    t.segmentName= null;
                    t.segmentShortCode= tOBJ.segmentCodeList[j];
                    t.pnrSegID = tOBJ.flightSegIdList[j];
                    t.system= tOBJ.system;
                    for(var segmentId=0;segmentId<list.length;segmentId++)
	                {
                    	var inboundSector = list[segmentId].returnFlag;
                    	var segmentCodeSplit = list[segmentId].segmentShortCode.split('/');
                    	var sourceFromList = segmentCodeSplit[0];
                    	var destinationFromList = segmentCodeSplit[1];
                    	var returnSegmentCodeSplit = t.segmentShortCode.split('/');
                    	var currentSource = returnSegmentCodeSplit[0];
                    	var currentDestination = returnSegmentCodeSplit[1];
                    	if(inboundSector){
                    		t.returnFlag= true;
             		        break;
                    	}
                    	else
                    	{
                    	      if(sourceFromList === currentDestination)
                    	      {
                    		    if(currentSource === destinationFromList)
                    			{
                    		       t.returnFlag= true;
                    		       break;
                    			}
                           	  }
                    	      else if (destinationFromList === currentDestination)
                    	      {
                    	    	  t.returnFlag= true;
                   		          break;
                    	      }
                    	}
	                } 
                    list[list.length] = t;
                }


            }

        }

        return list;

    };

    this.homeClick = function() {
        SYS_IBECommonParam.homeClick();
    };

    this.loadAirportSearchMessages = function() {

        var airportMsg = UI_mcAvailabilitySearch.airportMessage;
        $('#searchPageAirportMsg').html('');
        $('#searchPageAirportMsg').append(airportMsg);

        if(airportMsg != null && airportMsg != ''){
            $('#searchPageAirportMsg').slideDown('slow');
        } else {
            $('#searchPageAirportMsg').hide();
        }
    };
    
    
	this.setAvailableBundleFareLCClassStr = function(ondLogicalCCList){
		if(ondLogicalCCList != undefined && ondLogicalCCList != null){
			var outBoundAvailbleBundleFareArray = new Array();
			var resAvailableBundleFareLCClassStr = new Array();
			$.each(ondLogicalCCList, function(index, ondViseLogicalCCList) {
				var ondSequnce = ondViseLogicalCCList[0].sequence ;				
				$.each(ondViseLogicalCCList[0].availableLogicalCCList,function(avilableLogicalCCIndex, logicalCC){
					if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
						var arrIndex = outBoundAvailbleBundleFareArray.length;
						outBoundAvailbleBundleFareArray[arrIndex] = new Array();
						outBoundAvailbleBundleFareArray[arrIndex].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,
							"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, 
							"bundleFareName":logicalCC.logicalCCDesc});		
					}					
				});				
				resAvailableBundleFareLCClassStr[ondSequnce] = outBoundAvailbleBundleFareArray;				
			});
			$("#resAvailableBundleFareLCClassStr").val($.toJSON(resAvailableBundleFareLCClassStr));
		}		
	}
	
	// 2012-07-05T04:26:38
	function getJsonDate(timeInMillis){
		var d = new Date(timeInMillis);
		var month = d.getMonth()+1;
		var date = (d.getDate() < 10) ? ("0"+d.getDate()) : d.getDate();
		var hours = (d.getHours() < 10) ? ("0"+d.getHours()) : d.getHours();
		var minutes = (d.getMinutes() < 10) ? ("0"+d.getMinutes()) : d.getMinutes();
		var seconds = (d.getSeconds() < 10) ? ("0"+d.getSeconds()) : d.getSeconds();		
		if(month < 10){
			month = "0" + month;
		}
		return d.getFullYear()+'-'+month+'-'+date+'T'+hours+':'+minutes+':'+seconds;
	}
		function getTimefromString(str){
		var a = str[0].split(" ");
		var t = a.length;
		return " " + a[t-1]+ " ";
		
	}
	
	function getDatefromString(str){
		var a = str[0].split(" ");
		var t = a.length;
		var q = "";
		for(var i=0; i<t-1;i++){
			q = q + " "+ a[i];
		}
		return q;
	}
	function getAirportName(airportCode){
		var airportName ="null" ;
		 if(typeof(airports)!="undefined"){
	
			 var airportsLength = airports.length;
             for(var i=0;i<airportsLength;i++){
                 if(airportCode == airports[i].code){
                	 if(SYS_IBECommonParam.locale == 'fa'){
                		 airportName = airports[i].fa;
                	 }
                	 else{
                		 airportName = airports[i].en;
                	 } 
                 }
             }
             if(airportName == "null"){
            	 airportName = airportCode;
             }
		 }
		 else{
			 airportName = airportCode;
		 }
		 if(typeof(airportName) == "undefined"){
			 airportName = airportCode;
		 }			 
		 return airportName;
	}
}

UI_mcAvailabilitySearch = new UI_mcAvailabilitySearch();
$(document).ready(function(){
    UI_mcAvailabilitySearch.ready();
});
UI_AvailabilitySearch = null;