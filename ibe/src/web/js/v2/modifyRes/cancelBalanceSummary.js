/**
 * 
 * Author : Pradeep Karunanayake
 */
function UI_BalanceSummary(){}

UI_BalanceSummary.isCancel = false;

UI_BalanceSummary.details= {   PNR: "",
							   isGroupPNR: "",
							   version: "",
							   segmentRefNos: "",
							   allResSegments:"",
							   mode:"",
							   allConfirmSegs:""							   
							};
/**
 * Page Ready 
 */
UI_BalanceSummary.ready = function() {	
	$("#btnBackBalanceSummary").click(function(){UI_UserTabs.pnrClick();});
	$("#btnCancelConfirm").click(function(){UI_BalanceSummary.cancel();});
	UI_BalanceSummary.loadCancelSummaryData();		
}

UI_BalanceSummary.loadCancelSummaryData = function() {
	UI_BalanceSummary.cancelInitData();
	UI_BalanceSummary.pageBuild();
	UI_BalanceSummary.showBalcenceSummary();
	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
}

UI_BalanceSummary.cancelInitData = function() {
	var cancelData = UI_BalanceSummary.details;
	cancelData.PNR =  UI_CustReservation.pnr;
	var isGruupPnr = false;
	if (UI_CustomerHome.groupPnr == "true") {
		isGruupPnr = true;
	} else if (UI_CustomerHome.groupPnr == "false") {
		isGruupPnr = false;
	} else {
		isGruupPnr = $.trim(UI_CustomerHome.groupPnr) != "" ?true:false;
	}
	cancelData.isGroupPNR = isGruupPnr;
	cancelData.version = UI_CustReservation.version;
	cancelData.segmentRefNos =  UI_CustReservation.flightRefNos;
	cancelData.allResSegments =  UI_CustReservation.allReservationSegments;
	cancelData.mode = UI_CustReservation.mode;
	cancelData.allConfirmSegs = UI_CustReservation.getFlightDetails();		
}

/**
 * Modify Segment Page build Using Existing Data
 */
UI_BalanceSummary.pageBuild = function() {
	$("#reservationNum").text(UI_CustReservation.pnr);
	var mode = UI_BalanceSummary.details.mode;
	var flightData = "";
	if (mode != "" && mode == "cancelReservation") {
		UI_BalanceSummary.getAllSegmentRefNumbers();
		flightData = UI_BalanceSummary.details.allConfirmSegs.segments;
	} else { 
		flightData = UI_BalanceSummary.getSelectedFlight(UI_BalanceSummary.details.segmentRefNos); 
	}	
	$("#departueFlightCancels").nextAll().remove();
	$("#departueFlightCancels").iterateTemplete({templeteName:"departueFlightCancels", data:flightData, dtoName:"flightDepDetails"});	
}

/**
 * Get All Reference Numbers
 */
UI_BalanceSummary.getAllSegmentRefNumbers = function() {
	var flightSegmnts = UI_CustReservation.flightDepDetails.concat(UI_CustReservation.flightRetDetails);
	var refNum = "";
	if (flightSegmnts != null &&  flightSegmnts != "") {
		for (var i = 0; i < flightSegmnts.length; i++) {
			if (i == 0) {
				refNum = flightSegmnts[i].flightSegmentRefNumber;
			} else {
				refNum = refNum + ":" + flightSegmnts[i].flightSegmentRefNumber;
			}
		}
	}
	
	UI_BalanceSummary.details.segmentRefNos = refNum;
}



/**
 * Get Selected Segments
 */
UI_BalanceSummary.getSelectedFlight = function(flightRefNos) {	
	var segments = [];	
	var flightSegmnts = UI_CustReservation.flightDepDetails.concat(UI_CustReservation.flightRetDetails); 
	var segArray = flightRefNos.split(":");		
	for(var i = 0; i < segArray.length; i++) {		
			 if (flightSegmnts != null && flightSegmnts != "") {		 
				 for (var f = 0; f < flightSegmnts.length; f++) {							 
					 if(flightSegmnts[f].flightSegmentRefNumber == segArray[i]) {
						 segments[i] =  flightSegmnts[f];				
					 }
				 }			 
		 }	 
	 }	
	 return segments;	
}


/**
 * Show Balance Summary
 */
UI_BalanceSummary.showBalcenceSummary = function() {
	var details = UI_BalanceSummary.details;
	var data = {};
	data['pnr'] = UI_CustReservation.pnr;
	data['version'] = UI_CustReservation.bookingInfo.version;
	data['groupPNR'] = details.isGroupPNR;
	data['oldAllSegments'] = $.toJSON(details.allResSegments);
	data['mode'] = details.mode;
	if (details.mode == "cancelReservation") {
		data['cancelReservation'] = true;		
	} else if (details.mode == "cancelSegment"){
		data['cancelSegment'] = true;
	}
	data['modifySegmentRefNos'] = details.segmentRefNos;
	if(UI_CustReservation.resFlexiAlerts != null)
		data['resFlexiAlerts'] = $.toJSON(UI_CustReservation.resFlexiAlerts);
	$("#frmBalanceSummay").attr('action', 'balanceSummary.action');
	$("#frmBalanceSummay").ajaxSubmit({data:data,dataType: 'json', success: UI_BalanceSummary.showBalcenceSummaryProcess, error:UI_commonSystem.setErrorStatus});
	false;
}

/**
 * Balance Summary Process
 */
UI_BalanceSummary.showBalcenceSummaryProcess = function(response) {		
	    if(response != null && response.success == true) {	
	    	$("#frmBalanceSummay").populateLanguage({messageList:response.jsonLabel});
	    	$("#divRedeemCancel").html(response.jsonLabel.lblRedeem);
	    	UI_BalanceSummary.callCenterLinkSetup();
	    	UI_BalanceSummary.displaySummary(response.balanceSummary);
	    	if(response.flexiReservation){
	        	$("#flexiMessage").show();
	        	if(response.flexiInfo != null && response.flexiInfo != ""){
	        		$("#spnFlexibilities").text(UI_BalanceSummary.details.flexiInfo);
	        	}
	    	} else {
	    		$("#flexiMessage").hide();
	    	}
		} else {		
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}
		UI_commonSystem.loadingCompleted();	
}
/**
 * Dispaly Balance Summay
 */
UI_BalanceSummary.displaySummary = function(balanceSummary) {	
	if (balanceSummary != null && balanceSummary != "") {
		var  currency = UI_Top.holder().GLOBALS.baseCurrency;
		if(UI_CustReservation.fareQuote.inSelectedCurr.currency != null ){
			currency = UI_CustReservation.fareQuote.inSelectedCurr.currency;
		}
		var convertedAirFare = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.airFare);
		var convertedTax = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.tax);
		var convertedDiscount = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.discount);
		var convertedCreditableAmount = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.creditableAmount);
		var convertedCancelChrage = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.cancelChrage);
		var convertedPerPaxCancelCharge = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.perPaxCancelCharge);
		var convertedAvailableCredit = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.availableCredit);
		var convertedFinalCredit = UI_BalanceSummary.convertAmountToSelectedCurrency(balanceSummary.finalCredit);
		
		$("#airFareAmount").text(convertedAirFare + "  "+ currency);
		$("#surchargesAmount").text(convertedTax + "  "+ currency);
		$("#discountAmount").text(convertedDiscount + "  "+ currency);
		$("#creditableAmount").text(convertedCreditableAmount + "  "+ currency);
		$("#cancelChargeAmount").text(convertedCancelChrage  + "  "+ currency);
		$("#cancelAmount").text("  "+ currency +" " + convertedPerPaxCancelCharge);
		if (Number(balanceSummary.availableCredit) > 0) {
			$("#availableCredit").text(convertedAvailableCredit + "  "+ currency);
			$("#trReservationCredit").show();
		} else {
			$("#trReservationCredit").hide();
			$("#trSeperator").hide();
		}
		$("#finalCreditAmount").text(convertedFinalCredit + "  " + currency);		
	}
}

/**
 * Cancel Reservation / Cancel Segment
 */
UI_BalanceSummary.cancel = function() {
	if(!UI_BalanceSummary.isCancel){
		UI_BalanceSummary.isCancel = true;
		var details = UI_BalanceSummary.details;
		var data = {};
		data['pnr'] = UI_CustReservation.pnr;
		data['version'] = UI_CustReservation.bookingInfo.version;
		data['status'] = UI_CustReservation.bookingInfo.status;
		data['groupPNR'] = details.isGroupPNR;
		data['oldAllSegments'] = $.toJSON(details.allResSegments);
		data['contactInfoJson'] = $.toJSON(UI_CustReservation.contactDetails);
		data['mode'] = details.mode;
		data['modifySegmentRefNos'] = details.segmentRefNos;
		$("#frmBalanceSummay").attr('action', 'cancel.action');
		$("#frmBalanceSummay").ajaxSubmit({data:data,dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress,success: UI_BalanceSummary.showCancelProcess, error:UI_commonSystem.setErrorStatus});
		false;
	}
}

/**
 * Balance Summary Process
 */
UI_BalanceSummary.showCancelProcess = function(response) {		
	    if(response != null && response.success == true) {	
	    	UI_UserTabs.pnrClick();
		} else {		
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}
		UI_commonSystem.loadingCompleted();	
}
/**
 * Call Center Link
 */
UI_BalanceSummary.callCenterLinkSetup = function() {
	if(UI_Top.holder().GLOBALS.clientCallCenterURL === ""){
		$("#frmBalanceSummay").find("#linkCallCenter").attr("href", SYS_IBECommonParam.homeURL + "/callcentres");
	} else {
		$("#frmBalanceSummay").find("#linkCallCenter").attr("href", UI_Top.holder().GLOBALS.clientCallCenterURL).attr("target","_blank");
	}	
}

UI_BalanceSummary.convertAmountToSelectedCurrency = function(amount){
	var exchangeRate = UI_CustReservation.fareQuote.inSelectedCurr.exchangeRate == null ? 1 : UI_CustReservation.fareQuote.inSelectedCurr.exchangeRate;
	return $.airutil.format.currency(amount * exchangeRate);
}
/**
 * Onload Page Ready
 */
UI_BalanceSummary.ready();