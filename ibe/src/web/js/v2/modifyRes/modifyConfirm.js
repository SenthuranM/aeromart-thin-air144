/**
 * Modify Confirm
 * 
 * @author Pradeep Karuananayake
 *  
 */
	
function UI_ModifyConfirm(){};

;(function(UI_ModifyConfirm){
	
	UI_ModifyConfirm.blnNoPay = false;
	
	UI_ModifyConfirm.ready = function(){		
		UI_ModifyConfirm.pageBuild();
		//$('#jsonInsurance').val($.toJSON(UI_Container.getInsurance()));
		//$('#jsonPaxWiseAnci').val($.toJSON(UI_Container.getPaxWiseAnci()));		
		//UI_ModifyConfirm.showBalcenceSummary();		
	}
	
	UI_ModifyConfirm.nextClick = function() {
		if (UI_ModifyConfirm.blnNoPay != null && UI_ModifyConfirm.blnNoPay == true) {
			$('#frmFare').attr('target','_self');
			$('#jsonInsurance').val($.toJSON(UI_Container.getInsurance()));
			$('#jsonPaxWiseAnci').val($.toJSON(UI_Container.getPaxWiseAnci()));
			$("#jsonPaxInfo").val($.toJSON(UI_Container.getPaxArray()));
			$("#totalSegmentCount").val(UI_commonSystem.totalSegCount);
			$('#frmReservation').attr('action','interlinePostCardDetails.action');
			$('#frmReservation').attr('target','_self');
			$('#frmReservation').submit();
		} 
	}
	
	UI_ModifyConfirm.pageBuild = function() {
		$("#spnPNR").html(":  "+ $("#pnr").val());
		UI_ModifyConfirm.loadOldFlightData(UI_Container.getModifingFlightInfo());
		UI_ModifyConfirm.loadNewFlightData(UI_Container.getOutboundFlightSegs());		
	}
	
	UI_ModifyConfirm.loadNewFlightData = function(segments){	
		$("#departueFlightNew").iterateTemplete({templeteName:"departueFlightNew", data:segments, dtoName:"flightSegments"});		
	}
	
	UI_ModifyConfirm.getFlightRefNumbers = function(segments) {
		var data = {};
		if (segments != null && segments != "") {
			for (var i = 0; i < segments.length; i++) {
				 data["outFlightRPHList[" + i + "]"] = segments[i].flightRefNumber;
			}
		}
		
		return data;
	}
	
	UI_ModifyConfirm.loadOldFlightData = function(segments){	
		$("#departueFlightOld").iterateTemplete({templeteName:"departueFlightOld", data:segments, dtoName:"flightSegments"});		
	}
	
	/**
	 * Show Balance Summary
	 */
	UI_ModifyConfirm.showBalcenceSummary = function() {
		var rphData = UI_ModifyConfirm.getFlightRefNumbers(UI_Container.getOutboundFlightSegs());	
		$("#frmReservation").attr('action', 'balanceSummary.action');
		$("#frmReservation").ajaxSubmit({data:rphData,dataType: 'json', success: UI_ModifyConfirm.showBalcenceSummaryProcess, error:UI_commonSystem.setErrorStatus});
		false;
	}

	/**
	 * Balance Summary Process
	 */
	UI_ModifyConfirm.showBalcenceSummaryProcess = function(response) {		
		    if(response != null && response.success == true) {	
		    	UI_ModifyConfirm.displaySummary(response.updateCharge.chargesList);	
		    	UI_ModifyConfirm.blnNoPay = response.blnNoPay;
			} else {
				$("#balanceSummary").hide();
				jAlert(response.messageTxt,'Alert');
				//UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
			}
			//UI_commonSystem.loadingCompleted();	
	}
	/**
	 * Display Balance Summary
	 */
	UI_ModifyConfirm.displaySummary = function(chargesList) {
		$("#balanceSummaryTemplate").nextAll().remove();
		UI_ModifyConfirm.addCurrencyCode(chargesList);
		setBlankRowToList(chargesList);
		setNonRefundables(chargesList)
		$("#balanceSummaryTemplate").iterateTemplete({templeteName:"balanceSummaryTemplate", data:chargesList, dtoName:"chargesList"});
		$("#modifyBal tr:last td").addClass("SeperateorBGColor");
		$("#balanceSummary").show();
	}
	
	function setBlankRowToList(chargesList){
		for(var i=0;i<chargesList.length;i++){
			if(chargesList[i].displayDesc.toLowerCase()=="total charges"){
				var tempBlank = {}
				tempBlank.displayDesc = " ";
				tempBlank.displayNewCharges= "";
				tempBlank.displayOldCharges= "";
				tempBlank.enableDisplay= "";
				tempBlank.selCurDisplayNewCharges= "";
				tempBlank.selCurDisplayOldCharges= "";
				chargesList.splice((i+1), 0, tempBlank);
				break;
			}
		}
		return chargesList;
	}
	
	function setNonRefundables(chargesList){
		for(var i=0;i<chargesList.length;i++){
			if(chargesList[i].displayDesc.toLowerCase()=="non refundable amount"){
				chargesList[i].displayOldCharges = "-" + chargesList[i].displayOldCharges;
				chargesList[i].displayNewCharges = "-" + chargesList[i].displayNewCharges;
				break;
			}
		}
		return chargesList;
	}

	/**
	 * Set Currency Code
	 */
	UI_ModifyConfirm.addCurrencyCode = function(chargesList) {	
		var tempPax = "";
		var  baseCurrency  = UI_Container.getBaseCurrency();
		var selectedCurrency = UI_Container.fareQuote.inSelectedCurr.currency;
		for (var i = 0; i < chargesList.length; i++) {
			tempPax = chargesList[i];
			
			if (tempPax.displayOldCharges) {
				if(selectedCurrency != ""){
					var selCurDisplayOldCharges = (tempPax.selCurDisplayOldCharges == null) ? "0.00" : tempPax.selCurDisplayOldCharges;
					tempPax.displayOldCharges = selCurDisplayOldCharges + " "+ selectedCurrency;	
				} else {
					tempPax.displayOldCharges = tempPax.displayOldCharges + " "+ baseCurrency;	
				}				
			}
			if (tempPax.displayNewCharges) {
				if(selectedCurrency != ""){
					var selCurDisplayNewCharges = (tempPax.selCurDisplayNewCharges == null) ? "0.00" : tempPax.selCurDisplayNewCharges;
					tempPax.displayNewCharges = selCurDisplayNewCharges + " " + selectedCurrency;
				} else {
					tempPax.displayNewCharges = tempPax.displayNewCharges + " " + baseCurrency;					
				}
			}
			
			delete chargesList[i];
			chargesList[i] = tempPax;
		}
		return chargesList;
	}

})(UI_ModifyConfirm);