function UI_CustomerAddBusSegment(){}

UI_CustomerAddBusSegment.details= {PNR:"",
								   isGroupPNR:false,
								   version:"",
								   segmentRefNos:"",
								   allResSegments:"",
								   modifingFlightInfo:"",
								   from:"",
								   to:"",
								   depDate:"",
								   adultCount:"",
								   childCount:"",
								   infantsCount:"",
								   classOfService:"",
								   paxJson: "",
								   fareDetails:[],
								   firstDepature: "",
								   lastArrival:"",
								   hasInsurance:false,
								   depGroundStation:false,
								   arrGroundStation:false};
//segment modification IBE
UI_CustomerAddBusSegment.allowModifyByDate = "";
UI_CustomerAddBusSegment.allowModifyByRoute = "";
/**
 * Page Ready 
 */
UI_CustomerAddBusSegment.ready = function() {	
	$("#btnBackAddBusSegment").click(function(){ UI_FlightSearch.previousAirportData.isOnADataRefresh = true;UI_FlightSearch.resetSearhData(); UI_UserTabs.pnrClick();});	
	UI_CustomerAddBusSegment.pageBuild();
	UI_CustomerAddBusSegment.loadAddBusSegmentPageData();	
}

/**
 * Set OlD Fare Details
 */
UI_CustomerAddBusSegment.setOldFareDetails = function() {
	UI_CustomerAddBusSegment.getSegmentFareDetails();
}

/**
 * Get Segment Fare Details
 */
UI_CustomerAddBusSegment.getSegmentFareDetails = function() {
	var  fareDetails = UI_CustReservation.fareDetails;
	var modifyDetails = UI_CustomerAddBusSegment.details;
	var segRefNumber = modifyDetails.segmentRefNos.split(":")[0];		
	if (fareDetails != null && segRefNumber != "") {		
		for (var i = 0; i < fareDetails.length; i++) {			
			if (segRefNumber == fareDetails[i].flightRefNumber) {
				modifyDetails.fareDetails = fareDetails[i];			
				break;
			}
		}
	}
	
}

/**
 * Modify Segment Page build Using Existing Data
 */
UI_CustomerAddBusSegment.pageBuild = function() {
	UI_CustomerAddBusSegment.setModifyInitData();	
	$("#spnPNR").text(UI_CustReservation.pnr); 
	var flightData = UI_CustomerAddBusSegment.getSelectedFlight(UI_CustReservation.flightRefNos);
	UI_CustomerAddBusSegment.details.modifingFlightInfo = flightData;
	UI_CustomerAddBusSegment.setSegmentDetails(flightData);
	UI_CustomerAddBusSegment.setGroundSegmentData();
	UI_FlightSearch.setAddGroundSegmentSearchData(UI_CustomerAddBusSegment.details, UI_CustReservation.resFlexiAlerts,UI_CustomerAddBusSegment.allowModifyByDate,UI_CustomerAddBusSegment.allowModifyByRoute);
	$("#departueFlightMS").nextAll().remove();
	$("#departueFlightMS").iterateTemplete({templeteName:"departueFlightMS", data:flightData, dtoName:"flightDepDetails"});	
	UI_commonSystem.loadingCompleted();
}

UI_CustomerAddBusSegment.setModifyInitData = function() {
	var modifyData = UI_CustomerAddBusSegment.details;
	modifyData.PNR = UI_CustReservation.pnr;
	var isGruupPnr = false;
	if (UI_CustomerHome.groupPnr == "true") {
		isGruupPnr = true;
	} else if (UI_CustomerHome.groupPnr == "false") {
		isGruupPnr = false;
	} else {
		isGruupPnr = $.trim(UI_CustomerHome.groupPnr) != "" ?true:false;
	}
	modifyData.isGroupPNR = isGruupPnr;
	modifyData.version = UI_CustReservation.bookingInfo.version;
	modifyData.segmentRefNos = UI_CustReservation.flightRefNos;
	modifyData.allResSegments = UI_CustReservation.allReservationSegments;			  
	modifyData.adultCount = UI_CustReservation.bookingInfo.totalPaxAdultCount;
	modifyData.childCount = UI_CustReservation.bookingInfo.totalPaxChildCount;
	modifyData.infantsCount = UI_CustReservation.bookingInfo.totalPaxInfantCount;		 
	modifyData.paxJson = $.toJSON(UI_CustReservation.getPaxForAncillary());		   
  	modifyData.firstDepature = UI_CustReservation.firstDepature;
    modifyData.lastArrival= UI_CustReservation.lastArrival;
    modifyData.hasInsurance= UI_CustReservation.hasInsurance;
    UI_CustomerAddBusSegment.setOldFareDetails();
}

/**
 * Set Segment Details
 */
UI_CustomerAddBusSegment.setSegmentDetails = function(segmentflightsInfo) {	
	if (segmentflightsInfo != null && segmentflightsInfo != "") {
		if (segmentflightsInfo.length >= 1) {
			UI_CustomerAddBusSegment.details.from = segmentflightsInfo[0].segmentShortCode.split('/')[0];
			// UI_CustomerAddBusSegment.details.depDate = new Date(segmentflightsInfo[0].departureDateLong);	
		 
			if(segmentflightsInfo[0].surfaceStation != null) {
				UI_CustomerAddBusSegment.details.depGroundStation = true;
			}else{
				UI_CustomerAddBusSegment.details.depGroundStation = false;
			}
			UI_CustomerAddBusSegment.details.depDate= segmentflightsInfo[0].departureDisplayDate;	
			if (segmentflightsInfo.length == 1) {
				// Multi Leg flight segments
				var segmentCodes = (segmentflightsInfo[0].segmentShortCode).split("/");
				UI_CustomerAddBusSegment.details.to = segmentCodes[segmentCodes.length -1];				
			} else {
				UI_CustomerAddBusSegment.details.to = segmentflightsInfo[segmentflightsInfo.length -1].segmentShortCode.split('/')[1];	
			}
			UI_CustomerAddBusSegment.details.classOfService = segmentflightsInfo[segmentflightsInfo.length -1].cabinClass;
			if(segmentflightsInfo[segmentflightsInfo.length -1].surfaceStation != null) {
				//UI_CustomerAddBusSegment.details.arrGroundStation = true;
			}
		}		
	} else {
		alert("Error in segment details")
	}
}

/**
 * Get Selected Segments
 */
UI_CustomerAddBusSegment.getSelectedFlight = function(flightRefNos) {
	var segments = [];	
	var flightSegmnts = UI_CustReservation.flightDepDetails.concat(UI_CustReservation.flightRetDetails);
	var segArray = flightRefNos.split(":");		
	for(var i = 0; i < segArray.length; i++) {		
			 if (flightSegmnts != null && flightSegmnts != "") {		 
				 for (var f = 0; f < flightSegmnts.length; f++) {							 
					 if(flightSegmnts[f].flightSegmentRefNumber == segArray[i]) {
						 segments[i] =  flightSegmnts[f];						 
						 UI_CustomerAddBusSegment.allowModifyByDate = flightSegmnts[f].isAllowModifyByDate;
						 UI_CustomerAddBusSegment.allowModifyByRoute = flightSegmnts[f].isAllowModifyByRoute;						 
					 }
				 }			 
		 }	 
	 }	 
	 return segments;	
}

/**
* Request Page Data
*/

UI_CustomerAddBusSegment.loadAddBusSegmentPageData = function() {
	var modifyData = UI_CustomerAddBusSegment.details;
	var data = {};	
	data['segmentRefNo'] = modifyData.segmentRefNos;
	data['oldAllSegments'] = $.toJSON(modifyData.allResSegments);
	data['addGroundSegment'] = true;	
	$("#submitForm").ajaxSubmit({url:"addBusSegment.action", data:data, dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success: UI_CustomerAddBusSegment.loadAddBusSegmentPageDataProcess, error:UI_commonSystem.setErrorStatus});		
	return false;
} 
/**
 * Load Data Success
 */

UI_CustomerAddBusSegment.loadAddBusSegmentPageDataProcess =  function(response) {	
	if(response != null && response.success == true) {
		
		UI_FlightSearch.isAddBusSegment = true;
		$("#addBusSegment").populateLanguage({messageList:response.jsonLabel});		
		if(response.fromAirportData != null && response.fromAirportData != "" &&
				response.toAirportData != null && response.toAirportData != ""){
			UI_FlightSearch.fillFromToAirportDD(response.fromAirportData, response.toAirportData);
		}
		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

UI_CustomerAddBusSegment.setGroundSegmentData = function() {
	if(!UI_CustomerAddBusSegment.details.depGroundStation){
		UI_FlightSearch.previousAirportData.isOnADataRefresh = true;
		UI_FlightSearch.resetPreviousDataOnDropDowns();
	}else {
		if (UI_FlightSearch.previousAirportData.toAirportGroundOnlyData != null
				&& UI_FlightSearch.previousAirportData.fromAirportGroundOnlyData != null)
			UI_FlightSearch.fillFromToAirportDD(
					UI_FlightSearch.previousAirportData.fromAirportGroundOnlyData,
					UI_FlightSearch.previousAirportData.toAirportGroundOnlyData);
	}
}

 
/**
 * Onload Page Ready
 */
UI_CustomerAddBusSegment.ready();