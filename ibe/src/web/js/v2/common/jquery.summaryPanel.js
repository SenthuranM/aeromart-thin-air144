(function($) {
	
	$.fn.summaryPanel = function(o) {
		var config = {
				id:"",
				data:null,
				labels:null
		};

	if (o)
    	settings = $.extend(config, o);
	//Start of plugin
    return this.each(function(){
    	var	el = $(this);
    	var objid = this.className;
    	var addHTML = '<div class="Page-heading"><table width="100%" border="0" cellspacing="0" cellpadding="0">'+
    						'<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="headerRow">'+
								'<tr>'+
									'<td class="headerBG hLeft"></td>'+
									'<td class="headerBG hCenter alignLeft"><label id="lbl'+o.id+'" class="fntBold gridHDFont">'+o.labels.lblSearchSummary+'</label></td>'+
									'<td class="headerBG hRight"></td>'+
								'</tr></table></td></tr>'+
								'<tr>'+
									'<td class="mLeft"></td>'+
									'<td class="mCenter"><div class="pane-body"></div></td>'+
									'<td class="mRight"></td>'+
								'</tr>'+
								'<tr><td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerRow">'+
								'<tr>'+
								'<td class="headerBG bLeft"></td>'+
								'<td class="headerBG bCenter">&nbsp;</td>'+
								'<td class="headerBG bRight"></td>'+
								'</tr></table></td></tr>'+
							'</table>'+
						'</div>';

    	el.html(addHTML);
    	
    	displayItems = function(p){
    		if(parseFloat(p.totalFare)>0)
    			$('#trAirFare').show();
    		else
    			$('#trAirFare').hide();
    		
    		if(parseFloat(p.totalTaxSurcharge)>0)
    			$('#trCharges').show();
    		else
    			$('#trCharges').hide();
    		
    		if(p.hasFlexi)
    			$('#trFlexiCharges').show();
    		else
    			$("#trFlexiCharges").hide();    		
    		
    		if(p.hasFee)
    			$('#trTxnFee').show();
    		else
    			$('#trTxnFee').hide();
    		
    		if(p.hasReservationCredit)
    			$('#trUtilCredit').show();
    		else
    			$('#trUtilCredit').hide();
    		
    		if(p.hasReservationBalance)
    			$('#trResBalance').show();
    		else
    			$('#trResBalance').hide();
    		
    		if(p.hasSeatCharge)
    			$('#trSeatCharges').show();
    		else
    			$('#trSeatCharges').hide();
    		
    		if(p.hasMealCharge)
    			$('#trMealCharges').show();
    		else
    			$('#trMealCharges').hide();
    		
    		if(p.hasBaggageCharge)
    			$('#trBaggageCharges').show();
    		else
    			$('#trBaggageCharges').hide();
    		
			if(p.hasAirportServiceCharge)
				$("#trHalaCharges").show();
			else
				$("#trHalaCharges").hide();
			
			if(p.hasAirportTransferCharge)
				$("#trAptCharges").show();
			else
				$("#trAptCharges").hide();
			
    		if(p.hasInsurance)
    			$('#trInsuranceCharges').show();
    		else
    			$('#trInsuranceCharges').hide();
    		
    		if (p.inSelectedCurr!= null && p.inSelectedCurr.currency != p.inBaseCurr.currency
    				&& p.inSelectedCurr.currency != null  && 
    				parseFloat(p.inSelectedCurr.totalPrice)>= 0) {
    			$('label[name="valSelectedCurrCode"]').text(
    					p.inSelectedCurr.currency);
    			$("#valSelectedTotal").text(p.inSelectedCurr.totalPrice);
    			$('#trSelectedCurr').show();
    		} else {
    			$('#trSelectedCurr').hide();
    		}
    		
    		if(p.hasModifySegment)
    			$('#trModificationCharge').show();
    		else
    			$('#trModificationCharge').hide();
    		
    		if(p.hasTotalReservationCredit)
    			$('#trResCredit').show();
    		else
    			$('#trResCredit').hide();
    		
    		if(p.hasBalanceToPay)
    			$('#trResBalanceToPay').show();
    		else
    			$('#trResBalanceToPay').hide();
    		
    		if(p.hasCreditBalance)
    			$('#trCreditBalance').show();
    		else
    			$('#trCreditBalance').hide();
    			
    	};
    	
    	//TODO move the global variables out and pass them as params
    	 function showSelectedFlightsSummary(selectedFlights){
    			var labels = UI_Container.labels;
    			if(selectedFlights.isReturnFlight == true || selectedFlights.isReturnFlight == 'true') {
    				$("#lblReturn").text(labels['lblRoundTrip']);
    			}else {
    				$("#lblReturn").text(labels['lblOneWayTrip']);
    			}
    			var strMsgAdult = labels['lblAdult'];
    			var strMsgChild = labels['lblChild'];
    			var strMsgInfant = labels['lblInfant'];
    			
    			if(UI_Container.paxTypeCounts.adults > 0) {
    				$("#adultCount").text(UI_Container.paxTypeCounts.adults+ " " + strMsgAdult);
    			}
    			if(UI_Container.paxTypeCounts.children > 0) {
    				$("#childCount").text(", "+UI_Container.paxTypeCounts.children +" " + strMsgChild);
    			}
    			if(UI_Container.paxTypeCounts.infants > 0) {
    				$("#infantCount").text(", "+UI_Container.paxTypeCounts.infants+" " + strMsgInfant);
    			}
    			
    			$("#flightTemplate").iterateTemplete({templeteName:"flightTemplate", data:selectedFlights.flightSegments, dtoName:"flightSegments"});
    			$.each(selectedFlights.flightSegments, function(ind,obj){
        			if (obj.returnFlag){
        				$("#flightTemplate_"+ind).find(".imgCode").addClass("returnOND")
        			}
        		});
    			dateChangerjobforLanguage(SYS_IBECommonParam.locale);
    		};
    		
    		setSearchSummary = function(){
		
    			var setZero = function(){
        			if ($("#selAdults").val() == 0){$("#adultCount").text('')}
        			if ($("#selChild").val() == 0){$("#childCount").text('')}
        			if ($("#selInfants").val() == 0){$("#infantCount").text('')}
    			}
    			var strMsgAdult = o.labels['lblAdult'];
    			var strMsgChild = o.labels['lblChild'];
    			var strMsgInfant = o.labels['lblInfant'];
    			$("#adultCount").text($("#selAdults").val() + " " + strMsgAdult);
    			$("#childCount").text(", " + $("#selChild").val() + " " + strMsgChild);
    			$("#infantCount").text(", " + $("#selInfants").val() + " " + strMsgInfant);
    			$("#selAdults").change(function(){
    				$("#adultCount").text(this.value + " " + strMsgAdult);
    				setZero();
    			});
    			$("#selChild").change(function(){
    				$("#childCount").text(", " + this.value + " " + strMsgChild);
    				setZero();
    			});
    			$("#selInfants").change(function(){
    				$("#infantCount").text(", " + this.value + " " + strMsgInfant);
    				setZero();
    			});
    			setZero();

    		};
    		
    		 function createHTML(pName,processDate){
    				var str= '';
    				var strPromoDiscount = '';
    				var strTotalPromoDiscountDivCredit = '';
    				if (pName == 'paymentSummary'){
    					var curr = processDate.currency;    
    					var discountPanel = "";
    					if(processDate.totalIbePromoDiscount != "0.00"){    						
    						discountPanel = '<td width="55%" ><label id="lblPromoDiscount" class=\'fntBold\'>'+o.labels.lblPromoDiscount+'</label></td>'+
    						'<td width="2%" ><label>:&nbsp;</label></td>'+
    						'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    						'<td width="33%" class="alignRight"><label id=\'valTotal\' class=\'fntDefault fntBold hdFontColor\' >'+processDate.totalIbePromoDiscount+'</label></td>';
    					}
    					if(processDate.promoCodeEnabled){
    						strPromoDiscount = '<div id="totalPromoDiscountDiv" class="add-padding">'+
        					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
        					'<td width="55%" ><label id="lblPromoDiscount" class=\'fntBold\'>'+o.labels.lblPromoDiscount+'</label></td>'+
        					'<td width="2%" ><label>:&nbsp;</label></td>'+
        					'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
        					'<td width="33%" class="alignRight"><label id=\'valPromoDiscount\' class=\'fntDefault fntBold hdFontColor valPromoDiscount\' >'+processDate.totalIbePromoDiscount+'</label></td>'+
        					'</tr></table>'+
        					'</div>';
    						strTotalPromoDiscountDivCredit = 
    						'<div id="totalPromoDiscountDivCredit" class="add-padding">'+
        					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
        					'<td width="55%" ><label id="lblPromoDiscount" class=\'fntBold lblPromoDiscount\'>'+o.labels.lblPromoDiscountCredit+'</label></td>'+
        					'<td width="2%" ><label>:&nbsp;</label></td>'+
        					'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
        					'<td width="33%" class="alignRight"><label id=\'valPromoDiscount\' class=\'fntDefault fntBold hdFontColor valPromoDiscount\' >'+processDate.totalIbePromoDiscount+'</label></td>'+
        					'</tr></table>'+
        					'</div>';
    					}
    					str = '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="flightTemplateC">'+
    					'<tr><td class="pnlTop" style="height:8px"></td></tr><tr><td class="pnlBottom alignLeft">'+
    					'<div id="trAirFare" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblAirFare">'+o.labels.lblAirFare+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name="valSelectedCurrCode" class="fntDefault txtBold"> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id="valAirFare" class="fntDefault txtBold">'+processDate.totalFare+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trCharges" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblCharges">'+o.labels.lblCharges+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valCharges\' class=\'fntDefault txtBold\'>'+processDate.totalTaxSurcharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trFlexiCharges" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblFlexiCharges">'+o.labels.lblFlexiCharges+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valFlexiCharges\' class=\'fntDefault txtBold\'>'+processDate.flexiCharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trSeatCharges" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id=\'lblSeatSelection\'>'+o.labels.lblSeatSelection+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valSeatCharges\' class=\'fntDefault txtBold\'>'+processDate.seatMapCharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trInsuranceCharges" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id=\'lblTravelSecure\'>'+o.labels.lblTravelSecure+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valInsuranceCharges\' class=\'fntDefault txtBold\'>'+processDate.insuranceCharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trMealCharges" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id=\'lblMealSelection\'>'+o.labels.lblMealSelection+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valMealCharges\' class=\'fntDefault txtBold\'>'+processDate.mealCharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trBaggageCharges"  style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id=\'lblBaggageSelection\'>'+o.labels.lblBaggageSelection+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valBaggageCharges\' class=\'fntDefault txtBold\'>'+processDate.baggageCharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
                        '<div id="trSsrCharges"  style="display:none;" class="add-padding">'+
                        '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
                        '<td width="55%" ><label id=\'lblSsrSelection\'>'+o.labels.lblSsrSelection+'</label></td>'+
                        '<td width="2%" ><label>:&nbsp;</label></td>'+
                        '<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
                        '<td width="33%" class="alignRight"><label id=\'valSsrCharges\' class=\'fntDefault txtBold\'>'+processDate.baggageCharge+'</label></td>'+
                        '</tr></table>'+
                        '</div>'+
						'<div id="trHalaCharges"  style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id=\'lblHalaSelection\'>'+o.labels.lblHalaSelection+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valHalaCharges\' class=\'fntDefault txtBold\'>'+processDate.halaCharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trAptCharges"  style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id=\'lblAptSelection\'>'+ o.labels.lblAptSelection+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valAptCharges\' class=\'fntDefault txtBold\'>'+processDate.apTransferCharge+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trTxnFee" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblTNXFee">'+o.labels.lblTNXFee+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valTxnFee\' class=\'fntDefault txtBold\'>'+processDate.totalFee+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+    					
    					'<div id="trUtilCredit" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblUtilizedCredit">'+o.labels.lblUtilizedCredit+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>	'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valUtilCredit\' class=\'fntDefault txtBold\'>'+processDate.loyaltyCredit+'</label></td>'+
    					'</tr></table>'+    					
    					'</div>'+    					
    					'<div id="trModificationCharge" style="display:none;"  class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblModificationCharge">'+o.labels.lblModificationCharge+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>	'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valModificationCharge\' class=\'fntDefault txtBold\'></label></td>'+
    					'</tr></table>'+
    					'</div><div class="rowGap"></div>'+
    					'</div>'+    	
    					'<div id="trJnTaxAnci" style="display:none;"  class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblJnTaxForAnci">'+o.labels.lblJnTaxForAnci+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>	'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valJnTaxForAnci\' class=\'fntDefault txtBold\'></label></td>'+
    					'</tr></table>'+
    					'</div><div class="rowGap"></div>'+
    					'</div>'+
    					'<div id="trResBalance" style="display:none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblResBalance">'+o.labels.lblResBalance+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>	'+
    					'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valResBalance\' class=\'fntDefault txtBold\'>'+processDate.reservationBalance+'</label></td>'+
    					'</tr></table>'+
    					'</div><div class="rowGap"></div>'+ strPromoDiscount +
    					'<div class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblTotal" class=\'fntBold\'>'+o.labels.lblTotal+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valTotal\' class=\'fntDefault fntBold hdFontColor\' >'+processDate.totalPrice+'</label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div id="trSelectedCurr" style="display: none;" class="add-padding">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ></td>'+
    					'<td width="2%"><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valSelectedCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valSelectedTotal\' class=\'fntDefault txtBold hdFontColor\'>'+processDate.totalPrice+'</label></td>'+
    					'</tr></table>'+
    					'</div><br/>'+
    					'<div class="add-padding" style="display:none;" id="trResCredit">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblTotalReservationCredit">'+o.labels.lblReservationCredit+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valTotalReservationCredit\' class=\'fntDefault fntBold\' ></label></td>'+
    					'</tr></table>'+
    					'</div>'+    					
    					'<div class="add-padding" style="display:none;" id="trResBalanceToPay">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblBalanceToPay" class=\'fntBold\'>'+o.labels.lblBalanceToPay+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valModifyBalanceToPay\' class=\'fntDefault fntBold hdFontColor\' ></label></td>'+
    					'</tr></table>'+
    					'</div>'+
    					'<div class="add-padding" style="display:none;" id="trCreditBalance">'+
    					'<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'+
    					'<td width="55%" ><label id="lblCreditBalance" class=\'fntBold\'>'+o.labels.lblCreditBalance+'</label></td>'+
    					'<td width="2%" ><label>:&nbsp;</label></td>'+
    					'<td width="10%" ><label name=\'valBaseCurrCode\' class=\'fntDefault txtBold\'> '+curr+' </label></td>'+
    					'<td width="33%" class="alignRight"><label id=\'valResCreditBalance\' class=\'fntDefault fntBold hdFontColor\' ></label></td>'+
    					'</tr></table>'+
    					'</div>'+ strTotalPromoDiscountDivCredit +
    				'</td>'+
    				'</tr><tr><td class="rowGap"></td></tr></table>';
    				}else if (pName == 'bookingSummary'){
    					str += '<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
    					'<tr><td class="pnlTop"></td></tr>'+
    					'<tr><td class="pnlBottom add-padding alignLeft" style="height:52px">'+
    						'<table width="100%" border="0" cellspacing="1" cellpadding="1">';
    					if (o.id == 'SearchSummary'){
    						str += '<tr class="mcSearch"><td><label>'+o.labels.lblSearchMultiDes+'</label></td></tr>';
    					}
    						str += '<tr><td><label id="adultCount" /><label id="childCount" /><label id="infantCount" /></td></tr>'+
    						'</table></td></tr>'+
    						'<tr><td>';
    					if (o.id == 'SearchSummary'){
    						str += '<table width="100%" border="0" cellspacing="0" cellpadding="0" id="modlinks">'+
		        					'<tr><td class="pnlTop"></td></tr>'+
	        						'<tr><td class="mcSearch add-padding alignLeft pnlBottom" style="height:auto">'+
		        					'<table width="100%" border="0" cellpadding="1" cellspacing="1"><tr><td>'+
	        						'<a href="javascript:void(0)" id="newSearch">'+o.labels.lblNewSearch+'</a> | <a id="modSearch" href="javascript:void(0)">'+o.labels.lblModifySearch+'</a>'+
	        						'</td></tr></table></td></tr>'+
	    							'<tr><td class="rowGap"></td></tr>'+
		    						'</table>';
    					}
    						str += '<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flightTemplate" class="flightTemplateC">'+
    							'<tr><td class="pnlTop"></td></tr>'+
    							'<tr>'+
    								'<td class="pnlBottom add-padding alignLeft">'+
    								'<table width="100%" border="0" cellpadding="1" cellspacing="1">'+
    									'<tr><td colspan="3"><label id="segmentCode" class="fntBold hdFontColor imgCode" /></td></tr>'+
    									'<tr>'+
    										'<td width="50%" ><label id="lblFlightNo">'+o.labels.lblFlightNo+'</label></td>'+
    										'<td width="1%"><label>:</label></td>'+
    										'<td width="49%" ><label id="flightNumber" /></td>'+
    									'</tr><tr>'+
    										'<td><label id="lblDate">'+o.labels.lblDate+'</label></td>'+
    										'<td><label>:</label></td>'+
    										'<td><label id="departureDate" class="date-disp"/><label class="date-hid" id="departureDateValue" style="display:none"/></td>'+
    									'</tr><tr>'+
    										'<td ><label id="lblDepature" >'+o.labels.lblDepature+'</label></td>'+
    										'<td><label>:</label></td>'+
    										'<td ><label id="departureTime" /></td>'+
    									'</tr><tr>'+
    										'<td ><label id="lblArrival">'+o.labels.lblArrival+'</label></td>'+
    										'<td><label>:</label></td>'+
    										'<td ><label id="arrivalTime" /></td>'+
    									'</tr>'+
    									'</tr><tr>'+
										'<td ><label id="lblDuration">'+o.labels.lblDuration+'</label></td>'+
										'<td><label>:</label></td>'+
										'<td ><label id="duration" /></td>'+
										'</tr>'+
    									'<tr><td colspan="3" ></td></tr>'+
    									'<tr><td colspan="3" ><span name="anciDetails" ></span></td></tr>'+
    								'</table></td></tr>'+
    							'<tr><td class="rowGap"></td></tr>'+
    							'</table></td></tr>'+
    							'</table>';
    				}
    				return str
    			};

        $.fn.summaryPanel.update = function(pName){
        	el.find(".pane-body").html(createHTML(pName,o.data));
        	if (o.id == 'SearchSummary'){
    			setSearchSummary();
    			$(".searchData").show()
    			$("#flightTemplate").hide();
    			$("#modlinks").hide();
        	}else{
        		$(".searchData").hide()

        		//$("#flightTemplate").show();
        	}
    		if (o.data != null){
    			var flights = o.data.flightSegments;
    			if(SYS_IBECommonParam.locale != "en"){
    				if(typeof flights != 'undefined'){
    					for(var i = 0;i < flights.length;i++){
    						var depDate = flights[i].arrivalDate;
    						depDate = depDate.replace(/\\u[\dA-Fa-f]{4}/g, 
    			       	 	function (match) {
    			        	    return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
    			        	});
        					flights[i].departureDate = depDate;
    					}
    				}
    			}
    			
    			function unicodeToChar(text) {
    			    return text.replace(/\\u[\dA-Fa-f]{4}/g, 
    			        function (match) {
    			            return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
    			        });
    			}
    			
	    		if (pName == 'paymentSummary'){
	    			displayItems(o.data);
	    		}else if (pName == 'bookingSummary'){
	    			showSelectedFlightsSummary(o.data)
	    		}
    		}
    	};
    	$.fn.summaryPanel.update(objid);

   //end of plugin
    	$.fn.summaryPanel.firstLoad = function(){
    		$("#flightTemplate").nextAll().remove();
    		$("#flightTemplate").iterateTemplete({templeteName:"flightTemplate", data:UI_AvailabilitySearch.getSelectedFlightSegments("both"), dtoName:"flightSegments"});
    		$.each(UI_AvailabilitySearch.getSelectedFlightSegments("both"), function(ind,obj){
    			if (obj.returnFlag){
    				$("#flightTemplate_"+ind).find(".imgCode").addClass("returnOND")
    			}
    		});
    	};
    	// Design version 1 Extend
    	$.fn.summaryPanel.updateFlightPriceDetails =  function() {
    		
    	};
    });
	}
	    
})(jQuery);