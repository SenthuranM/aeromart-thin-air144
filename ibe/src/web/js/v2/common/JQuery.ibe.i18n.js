// To Do Remove function
/*$.fn.populateLanguage = function(params) {
	var screenId = params.screenId;
	var messageObject = params.messageList;
	//setting the value and id of labels
	$(this).find("label").each(function( intIndex ){
		var labelId = this.id;
		var messageId = screenId + "_" + labelId;
		var i18Message = messageObject[messageId];
		if(i18Message != null){
			$(this).text(i18Message);
		}		
	});	
	
	$(this).find(":button").each(function( intIndex ){
		var buttonId = this.id;
		var messageId = screenId + "_" + buttonId;
		var i18Message = messageObject[messageId];
		if(i18Message != null){
			$(this).val(i18Message);			
		}		
	});	
}
*/
$.fn.populateLanguage = function(params) {
    var trimLabels = function(){
        $(".trimMe").each(function(){
            var txt = $(this).text(),
                len = $(this).attr("data-len");
            if(txt.length>len){
                nTxt = $.trim(txt.substr(0, len)) + "...";
                $(this).text(nTxt);
            }
            $(this).attr("title", txt);
        })
    }
	var messageObject = params.messageList;
	//setting the value and id of labels
	$(this).find("label").each(function( intIndex ){
		var labelId = this.id;
		var messageId = labelId;
		var i18Message = messageObject[messageId];
		if(i18Message != null){
			$(this).text(i18Message);
		}
        if(i18Message === ""){
            $(this).closest(".hideMe-if-empty").hide();
        }
	});

    $(this).find("span, div").each(function( intIndex ){
        var labelId = this.id;
        var messageId = labelId;
        var i18Message = messageObject[messageId];
        if(i18Message != null){
            $(this).html(i18Message);
        }
        if(i18Message === ""){
            $(this).closest(".hideMe-if-empty").hide();
        }
    });
	
	$(this).find(":button").each(function( intIndex ){
		if ( $(this).hasClass("isabutton") ){
			var buttonId = this.id;
			var messageId = buttonId;
			var i18Message = messageObject[messageId];
			if(i18Message != null){
				$(this).find("td.buttonMiddle").text(i18Message);			
			}	
		}else{
			var buttonId = (this.id == "") ? this.name : this.id;
			var messageId = buttonId;
			var i18Message = messageObject[messageId];
			if(i18Message != null){
				if (this.nodeName == "BUTTON")
					$(this).text(i18Message);
				else
					$(this).val(i18Message);
				
			}
		}
	});
    trimLabels();
}