	/*
	*********************************************************
		Description		: Common system variables / function 
		Author			: Thushara Fernando
		Version			: 1.0
		Created on		: 04th January 2010
		Last Modified	: 04th January 2010
	*********************************************************	
	*/
					
	
	function UI_commonSystem(){}
	
	/* Common Variables */	
	UI_commonSystem.timerForSesionExpire = null
	
	UI_commonSystem.strTxtMandatory = "<font class='mandatory'>&nbsp;*</font>";
	UI_commonSystem.strDTFormat = "dd/mm/yy";
	UI_commonSystem.strAPS = "&#39;";
	UI_commonSystem.strPGID = "";
	UI_commonSystem.strPGMode = "";
	
	UI_commonSystem.strOHDCode = "OHD";
	UI_commonSystem.strCNFCode = "CNF";
	UI_commonSystem.strCNXCode = "CNX";
	
	UI_commonSystem.strAdtCode = "AD";
	UI_commonSystem.strInfCode = "IN";
	UI_commonSystem.strChdCode = "CH";
	
	UI_commonSystem.strCCCode = "CRED";	
	
	UI_commonSystem.timerTickerCount = 10;
	UI_commonSystem.timerTimeout = 0;
	UI_commonSystem.timerElement = null;
	UI_commonSystem.timerCallBackFn = null;
	UI_commonSystem.timerWithinTicker = false;
	UI_commonSystem.userActive = false;
	UI_commonSystem.userType = null;
	
	UI_commonSystem.loyaltyManagmentEnabled = false;
	UI_commonSystem.integrateMashreqWithLMSEnabled = false;
	UI_commonSystem.lmsDetails = null;
	
	UI_commonSystem.totalSegCount = 0;
	
	var timeoutSeconds = '';
	var timeoutMsg = '';
	var timeoutCancel = '';
	/*
	 * Hide Progress Bar
	 */
	UI_commonSystem.hideProgress = function(){
		$("#divLoadMsg").hide();
		$("#divLoadMsg").find(".mainPageLoaderFreame").hide();
		try{
			UI_commonSystem.resetTimeOut();
		}catch(ex){}
	}
	
	/*
	 * Show Progress Bar
	 */
	UI_commonSystem.showProgress = function(){
		$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").show();
	}
	
	UI_commonSystem.getUserType = function() {		
		if ($("#kioskAccessPoint").val() != null && ($("#kioskAccessPoint").val() == "true" || $("#kioskAccessPoint").val() == true)) {
			UI_commonSystem.userType = "KSK";
		}
	}
	/*
	 * Reset Page Time out 
	 */
	UI_commonSystem.resetTimeOut = function(){
		if (arguments.length == 1){
			opener.UI_Top.holder().ResetTimeOut();
		}else{
			UI_Top.holder().ResetTimeOut();
		}
	}	
	
	/*
	 * Create Cell
	 */
	UI_commonSystem.createCell = function(p){
		var tblCell = document.createElement("TD");
		var strBold = "";
		var strStyle = "";
		
		p = $.extend({id:"",
					rowSpan: 0,
					colSpan: 0,
					bold:false,
					textCss:"",
					click:"",
					width:"",
					role:"",
					css:"",
					align:"",
					vAlign:""}, 
					p);
		
		if (p.bold){strBold = "txtBold";}
		if (p.textCss != ""){strStyle = "style ='" + p.textCss  + "'";}
		
		if (p.id != ""){tblCell.setAttribute("id", p.id);}
		if (p.rowSpan > 1){tblCell.setAttribute("rowspan", p.rowSpan);}
		if (p.colSpan > 1){tblCell.setAttribute("colspan", p.colSpan);}
		if (p.width != ""){tblCell.setAttribute("width", p.width);}
		if (p.css != ""){tblCell.className = p.css;}
		if (p.align != ""){tblCell.setAttribute("align", p.align);}
		if (p.vAlign != ""){tblCell.setAttribute("valign", p.vAlign);}
		if (p.role != ""){tblCell.setAttribute("role", p.role);}
		if (p.click != ""){tblCell.onclick = p.click;}
		
		tblCell.innerHTML = "<p class='" + strBold + "' " + strStyle + ">" + p.desc + "<\/p>";
		
		return tblCell;
	}
	UI_commonSystem.pageOnChange = function(){
		$('#'+UI_commonSystem.timerElement+' div').hide();
		UI_commonSystem.userActive = true;
	}

	UI_commonSystem._runTimer = function(){
		if(!UI_commonSystem.timerWithinTicker|| UI_commonSystem.userActive){
			UI_commonSystem.userActive = true;
			$('#'+UI_commonSystem.timerElement+' div').hide();	
			//UI_commonSystem.timeoutAlertTicker();
			return;
		}
		UI_commonSystem.timerTickerCount -= 1000;
		$('#spnT').html(parseInt(UI_commonSystem.timerTickerCount/1000,10));
		if(UI_commonSystem.timerTickerCount>0){
			setTimeout('UI_commonSystem._runTimer()',1000);		
		}else {
			if(UI_commonSystem.timerCallBackFn!=null)
				UI_commonSystem.timerCallBackFn();
			
			UI_commonSystem.getUserType();
			if (UI_commonSystem.userType == "KSK") {
				SYS_IBECommonParam.homeClick();		
			} else {
				var strAction= "showLoadPage!expireSession.action";
					$("#frmSession").attr('target',"_self");
				$("#frmSession").attr('action',strAction);
	 			$("#frmSession").submit();
			}
		}
	}
	UI_commonSystem.cancelSessionTimeout  = function(){
		$('#'+UI_commonSystem.timerElement+' div').hide();		
		UI_commonSystem.userActive = true;
		UI_commonSystem.timerTimeout = parseInt(UI_commonSystem.timeoutDTO.timeout,10);
		act = UI_commonSystem.timerTimeout;
		UI_commonSystem.getUserType();
		if (UI_commonSystem.userType == "KSK"){ 
			UI_commonSystem.timerForSesionExpire = setInterval('UI_commonSystem.isUserActive()',10000);
			//UI_commonSystem.timerTickerCount = parseInt(UI_commonSystem.timeoutDTO.displayThreshold,10);
		}
	}
	
	UI_commonSystem.clearTimeoutSessionExpire = function() {
		clearInterval(UI_commonSystem.timerForSesionExpire);		
	}
	
	UI_commonSystem.showTimeoutCancel = function(){
		UI_commonSystem.timerWithinTicker = true;
		$('#'+UI_commonSystem.timerElement).html("<div style='position: absolute; z-index:100; left:"+($(window).width()/2-220)+"px; top:"+$(window).scrollTop()+"px; height: 42px; width: 600px;'></div>");
		
		var folderPath = '../images/alert_no_cache.png';

		var html = '';
		html+='<table border="0" cellpadding="0" cellspacing="0" height="40px" width="440px" align="center" style="background-color: #F3E798;opacity: .8; filter: alpha(opacity=83); background-repeat:repeat;-moz-border-radius-bottomright:8px; -moz-border-radius-bottomleft:8px; border-bottom: 1px solid  #FFDD00; border-left: 1px solid #FFDD00; border-right: 1px solid #FFDD00;">';
		html+='<tr>';
		html+='<td width="12%" valign="middle">&nbsp;<img id="imgConfirm" src="'+folderPath+'"></td><td width="88%" valign="middle" halign="center"><font class="fntBold">'+ UI_commonSystem.timeoutDTO.lblMsgTimeout +' &nbsp;<span id="spnT">&nbsp;&nbsp;</span></font>'; 
		html+=' <font class="fntBold"> '+ UI_commonSystem.timeoutDTO.lblSeconds  +'  &nbsp;&nbsp;<a href="javascript:UI_commonSystem.cancelSessionTimeout()">&nbsp;&nbsp;<u>'+ UI_commonSystem.timeoutDTO.lblMsgCancel +'</u></a></font>';
		html+='';
		html+='<form method="GET" id="frmSession" name="frmSession"></form>';
		html+='</td>';
		html+='</tr>';
		html+='</table>';
		$('#'+UI_commonSystem.timerElement).show();
		$('#'+UI_commonSystem.timerElement+' div').html(html);
		$('#'+UI_commonSystem.timerElement+ ' div').animate({top:$(window).scrollTop()+"px",left:($(window).width()/2-220)+"px" },{queue: false, duration: 150});  
		
		UI_commonSystem._runTimer();
	}
	var act = 0;
	UI_commonSystem.initSessionTimeout =  function(htmlElement,timeoutDTO,callBack){
		if(htmlElement==null||callBack==null||timeoutDTO == null){
			return;
		}
		UI_commonSystem.timerElement= htmlElement;
		UI_commonSystem.timerCallBackFn	= callBack;			
		$('#'+htmlElement).hide().html('');
		//UI_commonSystem.timerTimeout = 35000;
		//timeoutDTO.displayThreshold = 10000;
		UI_commonSystem.timerTimeout = parseInt(timeoutDTO.timeout,10);
		act = UI_commonSystem.timerTimeout;
		UI_commonSystem.timerTickerCount = parseInt(timeoutDTO.displayThreshold,10);
		timeoutSeconds = timeoutDTO.lblSeconds;
		timeoutMsg = timeoutDTO.lblMsgTimeout;
		timeoutCancel = timeoutDTO.lblMsgCancel;
		UI_commonSystem.timeoutDTO = timeoutDTO;
		
		$(window).scroll(function(){
			$('#'+UI_commonSystem.timerElement+' div').animate({top:$(window).scrollTop()+"px",left:($(window).width()/2-220)+"px" },{queue: false, duration: 150});  
		});
		UI_commonSystem.userActive = true;
		UI_commonSystem.timerForSesionExpire = setInterval('UI_commonSystem.isUserActive()',10000);
		// commented to add new Timeout
		//UI_commonSystem.timeoutAlertTicker();
		
	}
	//Right Click Disable
	$(document).bind("contextmenu",function(e){
        return false;
 	});
	$("html, body").mousemove(function(){
		act = UI_commonSystem.timerTimeout;
	});
	
//remove special Characters entered in the input fields and text areas - start
	
	//keypress when paste with CTRL+V  special characters
	$(document).on("keyup",'textarea, input:text, input:password',function(e) {
		if (e.which == 17 || e.which == 86 && e.ctrlKey) {
			var patt = /['|"`~]+/g;
			if (patt.test($(this).val()))
				$(this).val($(this).val().replace(patt,''));
		}
	});
	
	$(document).on("keypress",'textarea, input:text, input:password',function(e) {
		var patt = /['|"`~]+/g;
		if (patt.test($(this).val())){
			$(this).val($(this).val().replace(patt,''));
		}
	});
	//onblur when paste with menu special characters
	$(document).on("blur",'textarea, input:text, input:password',function(e) {
		var patt = /['|"`~]+/g;
		if (patt.test($(this).val()))
		$(this).val($(this).val().replace(patt,''));
	});
	
	//remove special Characters entered in the input fields and text areas - end
	
	
	UI_commonSystem.isUserActive = function (){
		act -= 10000;
		if (act < 0){
			UI_commonSystem.timerWithinTicker = true;
			UI_commonSystem.userActive = false;
			act = 0;
			UI_commonSystem.timerForSesionExpire = clearInterval(UI_commonSystem.timerForSesionExpire);
			UI_commonSystem.showTimeoutCancel();
		}
		return;
	};
	
	UI_commonSystem.timeoutAlertTicker = function(){
		if(!UI_commonSystem.userActive){
			UI_commonSystem.timerWithinTicker = true;
			UI_commonSystem.showTimeoutCancel();
		}else{
			UI_commonSystem.userActive = false;
			UI_commonSystem.timerWithinTicker = false;
			UI_commonSystem.timerTickerCount = parseInt(UI_commonSystem.timeoutDTO.displayThreshold,10);
			UI_commonSystem.timerTimeout = parseInt(UI_commonSystem.timeoutDTO.timeout,10);
			//setTimeout('UI_commonSystem.timeoutAlertTicker()',UI_commonSystem.timerTimeout -UI_commonSystem.timerTickerCount );
			UI_commonSystem.timerForSesionExpire = setTimeout('UI_commonSystem.timeoutAlertTicker()',5000 );
		}
	}
	/**
	 * Load Error Page
	 */
	UI_commonSystem.loadErrorPage = function(response){	
		window.location.href="showLoadPage!loadError.action?messageTxt="+response.messageTxt;
	};
	/**
	 * Show top loading message
	 */
	UI_commonSystem.loadingTimeoutRef = "";
	var showTimeOut = null;
	UI_commonSystem.loadingProgress = function() {		
		try{
			$("#pageLoading").hide();
			UI_commonSystem.readOnly(true);
			$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").show();
			// Set Loading Time out
			UI_commonSystem.loadingTimeoutRef = setTimeout('UI_commonSystem.loadingCompleted()', 1000*60*5);
		}catch(e){};
	};	
	
	function showLoading() {
		$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").show();	
	}
	
	/**
	 * Hide top loading message
	 */
	UI_commonSystem.loadingCompleted = function() {
		try{
			clearTimeout(showTimeOut);
			clearTimeout(UI_commonSystem.loadingTimeoutRef);
			$(".mainPageLoaderFreame").hide();
			$("#divLoadMsg").hide();
			$("#divLoadMsg").find(".mainPageLoaderFreame").hide();
			UI_commonSystem.readOnly(false);	
		}catch(e){
			$(".mainPageLoaderFreame").hide();
			$("#divLoadMsg").hide();
			$("#divLoadMsg").find(".mainPageLoaderFreame").hide();
			UI_commonSystem.readOnly(false);
		};
	};
	/**
	 * Disable input and link in divLoadBg
	 */
	UI_commonSystem.disableDivContent = function() {
		$('#divLoadBg').find('input').attr('disabled','disabled');
		$('#divLoadBg').find('select').attr('disabled','disabled');
		$("#divLoadBg").find('a').bind('click', UI_commonSystem.disableLink);
		$("#divLoadBg").find('td').bind('click', UI_commonSystem.disableLink);

	};
	/**
	 * Enable input and link in divLoadBg
	 */
	UI_commonSystem.enableDivContent = function() {
		$('#divLoadBg').find('input').attr('disabled',false);
		$('#divLoadBg').find('select').attr('disabled',false);
		$("#divLoadBg").find('a').unbind('click', UI_commonSystem.disableLink);
		$("#divLoadBg").find('td').unbind('click', UI_commonSystem.disableLink);
	};
	/**
	 * Event handling for bind
	 */
	UI_commonSystem.disableLink = function(event) {
		event.preventDefault();
		return false;
	}
	
	/** Reverse Order for String 
	 * IE arabic alert text (RTL)
	 */
	UI_commonSystem.alertText = function(data) {
		var s = "";
		if ($.browser.msie && data.language == 'ar') {    
		    var i = data.msg.length;
		    while (i>0) {
		        s += data.msg.substring(i-1,i);
		        i--;
		    }
		} else {
			s = data.msg;
		}
	    return s;
	}
	/**
	 * Check Empty
	 */	
	UI_commonSystem.isEmpty = function(obj) {
		if (obj == null || obj == undefined || $.trim(obj) == ""  )
			return true;
		else
			return false;
			
	}
	/**
	 * 
	 */
	
	UI_commonSystem.raiseError = function(strMsg){		
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMsg;
	}
	
	UI_commonSystem.setCommonButtonSettings = function() {
		if (SYS_IBECommonParam.locale == "ar"){
			$(".buttonset table tr td:last-child").attr("align","left");
			$(".buttonset table tr td:last-child").children().css("float", "left");
		}else{
			$(".buttonset table tr td:last-child").attr("align","right");
			$(".buttonset table tr td:last-child").children().css("float", "right");
		}

		if (globalConfig.showStopOvers){
			$("#btnSOver").css("display","block");
			$("button[name='btnSOver']").css("display","block");
			$("button[name='btnSOver']").click(function(){SYS_IBECommonParam.homeClick();})
		}else{
			$("#btnSOver").css("display","none");
			$("button[name='btnSOver']").css("display","none");
		}
		
		//$("#btnRecalculate").parent().attr("align","center");
	}
	
	UI_commonSystem.readOnly = function (status){
	   	if (status){
	   		$("#page-mask").remove();
	   		$("body").append('<div id="page-mask"></div>');
			$("#page-mask").css({
				position: 'absolute',
				zIndex: 1000,
				top: '0px',
				left: '0px',
				width: '100%',
				height: $(document).height(),
				backgroundColor:'#ffffff',
				opacity:0.1
			});
	   	}else{
	   		$("#page-mask").remove();
	   	}
   	};
   	UI_commonSystem.sectionLoaderShow = function(section, loadderCintainer){
   		var currentHeight =  $(section).parent().css("height");
   		var currentwidth = $(section).parent().css("width");
   		var htm = '<table border="0" cellpadding="0" cellspacing="0" width="100%" height="'+currentHeight+'">'+
		'<tr>'+
			'<td valign="middle" align="center">'+
				'<img src="../images/loading-cal_no_cache.gif">'+
			'</td>'+
		'</tr>'+
		'</table>';
		$(loadderCintainer).html(htm);
   		$(loadderCintainer).css({
   			position:"absolute",
   			height:currentHeight,
   			zIndex: 1000,
   			width:currentwidth,
   			top:$(section).position().top,
   			left:$(section).position().left,
   			background:"white"
   		});
   		UI_commonSystem.readOnly(true);
   		$(loadderCintainer).show();
   	};
   	
   	UI_commonSystem.setErrorStatus = function(resObj){	
   		//if(resObj!=null){
   			//if(resObj.responseText.indexOf('sessionExpiredNonExistanceAction')!= -1){
   				//window.location.href="showLoadPage!expireSession.action";
   			window.location.href="showLoadPage!loadError.action";
   			//}
   		//}
		
	};
   	
   	UI_commonSystem.sectionLoaderHide = function(section,loadderCintainer){
   		$(loadderCintainer).html("");
   		UI_commonSystem.readOnly(false);
   		$(loadderCintainer).hide();
   	};
   	UI_commonSystem.dragableLI = function(){
   		
	$("#divSummaryPane").stiky ({
		scrolltype:globalConfig.stikyType, //"slide" or "fixed" or "normal"
		scrollspeed:600, //time of the animation 
		headergap:147, //gap of the top header 
		footergap:380, //gap of the top header 
		topgap:0,
		panelheader:"none" //none is default Pass simple HTML for Header
		});
   	}
   	UI_commonSystem.stepsSettings = function(seclectedStepNo,page){
   		$(".stepsItem").removeClass("selected");
   		$(".stepsItem").addClass("selected")
   		var sets = $(".stepsItem");
   		if (page == "interLineContainer"){
   			if (sets.length > 3){
   				$(".stepsItem").removeClass("select");
   	   			$(sets[seclectedStepNo-1]).addClass("select");
   			}else{
   				$(sets[1]).addClass("select");
   			}
   		}else{
   			if (seclectedStepNo == "last"){
   				$(".stepsItem").removeClass("select");
	   			//$(sets[sets.length-1]).addClass("select");
	   			$(sets[sets.length-1]).addClass("select");
   			}else{
   				$(".stepsItem").removeClass("select");
	   			$(sets[seclectedStepNo-1]).addClass("select");
   			}
   		}

   		$(".stepsItem").removeClass("last-setected");
   		$(".select").prev().addClass("last-setected");
   		$(".select").removeClass("selected");
   		$(".select ~ td").removeClass("selected");
   	}
   	
   	UI_commonSystem.loadExternalContant = function (url,msg,container){
   		var yuiLoaded = false;
   	    // if the URL starts with http
   	    if(url != null && url.match('^http')){
   	      // assemble the YQL call
   	      var n = $.getJSON("http://query.yahooapis.com/v1/public/yql?"+
   	                "q=select%20*%20from%20html%20where%20url%3D%22"+
   	                encodeURIComponent(url)+
   	                "%22&format=html'&callback=?",
   	        function(data){
   	          if(data.results[0]){
   	            var data = filterData(data.results[0]);
   	            $(container).html(data);
   	            yuiLoaded = true;
   	            msg();
   	          } else {
   	        	yuiLoaded = false;
   	          }
   	        }
   	      );
   	    } else {
   	      $.ajax({
   	        url: url,
   	        timeout:2000,
   	        success: function(data){
   	    	  $(container).html(data);
   	        },
   	        error: function(req,error){
   	          if(error === 'error'){error = req.statusText;}
   	          var errormsg = 'There was a communication error: '+error;
   	          //container.html(errormsg);
   	        }
   	      });
   	    }
   	    if (!yuiLoaded){
   	    	//var srt = '<iframe src="'+url+'" width="100%" height="'+msg.height+'" id="iframe'+msg.id+'" scrolling="no" frameborder="0" ></iframe>'
   	    	//$(container).html(srt);
   	    }
 	    return true;
   	  }
   	  function filterData(data){
   	    // filter all the nasties out
   	    // no body tags
   	    data = data.replace(/<?\/body[^>]*>/g,'');
   	    // no linebreaks
   	    data = data.replace(/[\r|\n]+/g,'');
   	    // no comments
   	    data = data.replace(/<--[\S\s]*?-->/g,'');
   	    // no noscript blocks
   	    data = data.replace(/<noscript[^>]*>[\S\s]*?<\/noscript>/g,'');
   	    // no script blocks
   	    //data = data.replace(/<script[^>]*>[\S\s]*?<\/script>/g,'');
   	    // no self closing scripts
   	    //data = data.replace(/<script.*\/>/,'');
   	    // [... add as needed ...]
   	    return data;
   	  }
   	  
   	UI_commonSystem.checkHttps = function() {
   		var url = window.location.href;   		
   		if (url.indexOf("https:") == -1) {     			
   			return false;
   		} else {
   		   return true;
   		}
   	}
   	
   	UI_commonSystem.getRandomNumber = function() {
   		return Math.floor(Math.random()*100000);
   	}

   	UI_commonSystem.getDummyFrm = function(){
		var frmName = 'dummaryFrm';
		$('#'+frmName).remove();
		$('body').append('<form method="post" name="'+frmName+'" id="'+frmName+'"></form>');
		return $('#'+frmName);
	}
   	
   	UI_commonSystem.setPageFullView = function() {   		/*
		 When payment is processing if there is an error it will load to iframe. 
		 So  iframe view should  be full view
		*/
		// Payment Iframe resize	
		if ($("#cardInputs", parent.document).length > 0) {
			$("#divLoadBg", parent.document).hide();			
			$("#cardInputs", parent.document).css("top", "0px");
			$("#cardInputs", parent.document).css("left", "0px");	
			if ($.browser.msie && $.browser.version.substr(0,1)<7) {		
				$("#cardInputs", parent.document).attr("height",   $(parent.window).height());
				$("#cardInputs", parent.document).attr("width",   $(parent.window).width());
			} else {			
				$("#cardInputs", parent.document).attr("height", "100%");
				$("#cardInputs", parent.document).attr("width", "100%");
			}
		}  else {
			$("#mainContainer").css({"top":"0px", "left": "0px", "background": "white","position":"absolute","height": "250%","width": "100%"});
						
		}		
   	}
   	
   	// Auto complete off
	UI_commonSystem.autoCompleteOff = function() {
   		$('input').attr('autocomplete', 'off');
   	}
   	$(document).ready(function(){	
   		UI_commonSystem.autoCompleteOff();
   	});	
   	
   	UI_commonSystem.genarateProductID = function(flightSegments){
		var t = '';
		if (flightSegments != undefined || flightSegments.flightSegments != undefined){
			var _segment = flightSegments.flightSegments;
			var temp = _segment[0].segmentShortCode.split("/");
			t += temp[0] + "-" + temp[1];
			/*for (var i = 0; i < _segment.length; i++){
				var temp = _segment[i].segmentShortCode.split("/");
				if (i == _segment.length - 1){
					t += temp[0] + "-" + temp[1];
				}else{
					t += temp[0] + "-" + temp[1] + "_";
				}
			}*/
		}
		return t;
	}
   	
   	UI_commonSystem.joinLmsFieldValidation = function (){
   		var valid = true;
	   		if(!(SYS_IBECommonParam.regCustomer=="true"||SYS_IBECommonParam.regCustomer==true)){
	   			if(trim($("#txtPswdLoginLMS").val()) ==""){
					$("#txtPswdLogin").addClass("errorControl");
					valid = false;
				}/*else if(Util_CustomerValidation.hasSpecialCharactors(strPassword)==false){
					$("#txtPswdLogin").addClass("errorControl");					
				}*/else if (trim($("#txtPswdLoginLMS").val()).length < 6){
					$("#txtPswdLoginLMS").addClass("errorControl");
					valid = false;
				}else{
					$("#txtPswdLoginLMS").removeClass("errorControl");
				}
				
				if(trim($("#txtPswdVerifyLMS").val()) ==""){
					$("#txtPswdVerifyLMS").addClass("errorControl");
					valid = false;
				}else if(trim($("#txtPswdVerifyLMS").val()) != trim($("#txtPswdLoginLMS").val())){
					$("#txtPswdVerifyLMS").addClass("errorControl");
					valid = false;
				}else{
					$("#txtPswdVerifyLMS").removeClass("errorControl");
				}
				
				if(trim($("#txtEmailLoginLMS").val()) == ""){
					$("#txtEmailLoginLMS").addClass("errorControl");
					valid = false;
				}else if(checkEmail(trim($("#txtEmailLoginLMS").val())) == false){
					$("#txtEmailLoginLMS").addClass("errorControl");
					valid = false;
				}else{
	   				$("#txtEmailLoginLMS").removeClass("errorControl");
	   			}
	   		}   			
   		
   			/**
   			 * TODO validate lms data
   			 */
   			if(trim($("#txtEmailLMS").val()) ==""){
   				$("#txtEmailLMS").addClass("errorControl");
   				valid = false;
   			}else if(checkEmail(trim($("#txtEmailLMS").val())) == false){
   				$("#txtEmailLMS").addClass("errorControl");
   				valid = false;
   			}else{
   				$("#txtEmailLMS").removeClass("errorControl");
   			}
   			
   			/**
   			 * Mobile number validation
   			 *
   			 */
   			if(trim($("#txtMCountryLMS").val()) != "") {
   				if (validateInteger($("#txtMCountryLMS").val())){
   					removeZero("txtMCountryLMS");
   				}else {
   					setField("txtMCountryLMS", "");
   				}
   			}
   			if(trim($("#txtMAreaLMS").val()) != "") {
   				if (validateInteger($("#txtMAreaLMS").val())){
   					removeZero("txtMAreaLMS");
   				}else {
   					setField("txtMAreaLMS", "");
   				}
   			}
   			if(trim($("#txtMobileLMS").val()) != "") {
   				if (validateInteger($("#txtMobileLMS").val())){
   					removeZero("txtMobileLMS");
   				}else {
   					setField("txtMobileLMS", "");
   				}
   			}
   			if(trim($("#txtMobileLMS").val()) == ""){
   				$("#txtMobileLMS").addClass("errorControl");
   				$("#txtMCountryLMS").addClass("errorControl");
   				$("#txtMAreaLMS").addClass("errorControl");
   				valid = false;
   			}else{
   				$("#txtMobileLMS").removeClass("errorControl");
   				$("#txtMCountryLMS").removeClass("errorControl");
   				$("#txtMAreaLMS").removeClass("errorControl");
   				
   			}
   			
   			/**
   			 * Name validation
   			 */
   			
   			if (trim($("#txtFirstNameLMS").val())==""){
   				$("#txtFirstNameLMS").addClass("errorControl");
   				valid = false;
   			} else if (!isAlphaWhiteSpace(trim($("#txtFirstNameLMS").val()))){
   				$("#txtFirstNameLMS").addClass("errorControl fontCapitalize");
   				valid = false;
   			}else{
   				$("#txtFirstNameLMS").removeClass("errorControl");
   			}
   			
   			if (trim($("#txtLastNameLMS").val())==""){
   				$("#txtLastNameLMS").addClass("errorControl");
   				valid = false;
   			} else if (!isAlphaWhiteSpace(trim($("#txtLastNameLMS").val()))){
   				$("#txtLastNameLMS").addClass("errorControl fontCapitalize");
   				valid = false;
   			}else{
   				$("#txtLastNameLMS").removeClass("errorControl");
   			}
   			
   			/**
   			 * Age validation
   			 *
   			 */
   			
   			if(trim($("#textDOBLMS").val())==""){
   				$("#textDOBLMS").addClass("errorControl");
   				valid = false;
   			}else if(!dateValidDate($("#textDOBLMS").val())){
   				$("#textDOBLMS").addClass("errorControl");
   				valid = false;
   			}else{
   				$("#textDOBLMS").removeClass("errorControl");
   			}
   			
   			/**
   			 * Passport number validation
   			 * 
   			 */
   			
   			if(trim($("#txtPpnLMS").val()) ==""){
   				$("#txtPpnLMS").addClass("errorControl");
   				valid = false;
   			}else{
   				$("#txtPpnLMS").removeClass("errorControl");
   			}
   			
   			/**
   			 * Head ffid validation
   			 */
   			
   			var dob = $("#textDOBLMS").datepicker("getDate");
			var present = new Date();
			var age = (present - dob)/1000/60/60/24;
			if (age < (12*365+3)){
	   			if(trim($("#txtEmailHLMS").val()) ==""){
	   				$("#txtEmailHLMS").addClass("errorControl");
	   				valid = false;
	   			}
	   			else{
	   				$("#txtEmailHLMS").removeClass("errorControl");
	   			}
			}else if(checkEmail(trim($("#txtEmailHLMS").val())) == false){
				$("#txtEmailHLMS").addClass("errorControl");
				valid = false;
			}
			
			if(checkEmail(trim($("#txtEmailHLMS").val())) == false){
   				$("#txtEmailHLMS").addClass("errorControl");
   				valid = false;
   			}else{
   				$("#txtEmailHLMS").removeClass("errorControl");
   			}
   		return valid;
   		
   	}
   	
   	UI_commonSystem.ajaxRegistrtion = function(){
   		registerCustomer = function(){
			var url ="customerRegister.action";
			var  data = {};
			data["customer.title"] = $('#selTitle').val();
			data["customer.firstName"] = $('#txtFName').val();
			data["customer.lastName"] = $('#txtLName').val();
			data["customer.nationalityCode"] = $('#selNationality').val();
			data["customer.countryCode"] = $('#selCountry').val();
			data["customer.mobile"] = $('#txtMCountry').val() +"-"+ $('#txtMArea').val() +"-"+ $('#txtMobile').val();
			data["customer.dateOfBirth"] = "";
			data["customer.emailId"]= $('#txtEmailLoginLMS').val();
			data["customer.password"] = $('#txtPswdLoginLMS').val();
			data["lmsDetails.mobileNumber"] = $('#txtMCountry').val() +"-"+ $('#txtMAreaLMS').val() +"-"+ $('#txtMobileLMS').val();
			data["lmsDetails.headOFEmailId"] = $("#txtEmailHLMS").val();
			data["lmsDetails.genderCode"] = $('#selTitle').val();
			data["lmsDetails.dateOfBirth"] = $("#textDOBLMS").val();
			data["lmsDetails.firstName"] = $("#txtFirstNameLMS").val();
			data["lmsDetails.middleName"] = $("#txtMiddleNameLMS").val();
			data["lmsDetails.lastName"] = $("#txtLastNameLMS").val();
			data["lmsDetails.emailId"] = $("#txtEmailLMS").val();
			data["lmsDetails.appCode"] = "APP_IBE"; 
			$.ajax({type: "POST", dataType: 'json', data:data,url:url,		
				success: registerCustomerSuccess, error:UI_commonSystem.setErrorStatus});
			return false;
		};
		registerCustomerSuccess = function(response){
			if(response != null && response.success == true) {
				if (response.messageTxt != null && $.trim(response.messageTxt) != "") {
					UI_Container.hideLoading();
					var emailExistsError =label["lblEmailAlreadyExistError"].replace('{0}', UI_Container.carrierName);
					jConfirm(emailExistsError  , 'Confirm' ,
						function(response1){
							if (response1){
								UI_Passenger.cuntinueOptionClick([{id:"loginContinue", value:"on"}])
							}else{
								$("#txtEmailLoginLMS").focus();
							}
						}
					, label["lblYesLogin"] , label["lblNoNewAccount"]);
				}else{
					//new prfile were created
					customerLoginSucess = function(response){
						if(response.loginSuccess) {
							UI_Container.buildCustomer(response.customer, response.totalCustomerCredit, response.baseCurr);
							eval(response.custProfile);
							profileLoad();
							if(top!=null & top[0]!=null){
								top[0].strReturnPage = "CUSTOMER";
								top[0].strUN = response.txtUname;
								UI_Container.isRegisteredUser = true;
							}
							// Update Register Customer Login status
							SYS_IBECommonParam.setCustomerState(true);
							if (!contactConfig.areaCode1.ibeVisibility){UI_Passenger.setAreCode();}
						} else {
							UI_Container.hideLoading();
							jAlert(UI_commonSystem.alertText({msg:arrError["ERR061"], language:strLanguage}));
						}	
					}
					customerLogin = function(){
						//if(!loginValidation( $('#txtUID'), $('#txtPWD') )) return;
						var url = "ajaxLogin.action";
						var  data = new Object();
						data["txtUID"] = $('#txtEmailLoginLMS').val();
						data["txtPWD"] = $('#txtPswdLoginLMS').val();
						$.ajax({ type: "POST", dataType: 'json', data:data, url:url,							
							success: customerLoginSucess, error:UI_commonSystem.setErrorStatus});
					}
					customerLogin();
				}
				
			}else{
				UI_Container.hideLoading();
				UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
			}
			
		};
		$("#spnError").html("");
		registerCustomer();
   	}
   	
   	UI_commonSystem.joinLmsButtonClick = function(){
   		dialog = $( "#dialog-form-lms" ).dialog({
   			autoOpen: false,
   			modal: true,
   			width: 460,
   			heihgt: 700,
   			close: function() {
   			},
   			buttons: {
   				"Join Loyalty System": function(){
   					if(globalConfig.currentPage == "PAX"){
	   					if(UI_commonSystem.joinLmsFieldValidation()){
	   						$("#hdnMobileLMS").val($("#txtMCountryLMS").val() + "-" + $("#txtMAreaLMS").val() + "-" + trim($("#txtMobileLMS").val()).substr(2,trim($("#txtMobileLMS").val()).length));
							$("#hdnEmailHLMS").val(trim($("#txtEmailLMS").val()));
							$("#hdnDOBLMS").val(trim($("#textDOBLMS").val()));
							$("#hdnFirstNameLMS").val(trim($("#txtFirstNameLMS").val()));
							$("#hdnMiddleNameLMS").val(trim($("#txtMiddleNameLMS").val()));
							$("#hdnLastNameLMS").val(trim($("#txtLastNameLMS").val()));
							$("#hdnEmailLMS").val(trim($("#txtEmailHLMS").val()));
							if(SYS_IBECommonParam.regCustomer!="true" && SYS_IBECommonParam.regCustomer!=true){
			   					$("#isLMS").prop('checked', true);
								$("#txtLoginEmail").val(trim($("#txtEmailLoginLMS").val()));
						   		$("#txtPassword").val(trim($("#txtPswdLoginLMS").val()));
						   		$("#txtConPassword").val(trim($("#txtPswdVerifyLMS").val()));
						   		$("#registerContinue").prop('checked', true);
							}
							dialog.dialog( "close" );
	   					}
   					}
   					else{   						
   						if(UI_commonSystem.joinLmsFieldValidation()){
   							dialog.dialog( "close" );
   							UI_commonSystem.ajaxRegistrtion();
   						}
   					}
   				},
   				Cancel: function() {
   	   				$("#lmsDetailsForm").reset();
   					dialog.dialog( "close" );
   				}
   			}
   			
   		});
   		$("#dialog-form-lms").show();
   		if(SYS_IBECommonParam.regCustomer=="true"||SYS_IBECommonParam.regCustomer==true){
   			$("#trHeadLogin").html("");
   			$("#trEmailLogin").html("");
   			$("#trPswdLogin").html("");
   			$("#trPswdVerify").html("");
   			}
   		else{
   			$("#trHeadLogin").html('<td colspan="4" class="alignLeft">	<label class="fntBold hdFontColor" id="lblLoginDetails">Login Details</label></td>');
   			$("#trEmailLogin").html('<td class="alignLeft"><label id="lblEmailLogin"> Email </label></td><td><font>:</font><input type="text" maxlength="50" size="50" name="txtEmailLogin" id="txtEmailLoginLMS" style="width: 180px"/><label class="mandatory" id="lblEmailLoginMand">*</label><br><label class="fntSmall">This will be your login ID</label></td>');
   			$("#trPswdLogin").html('<td class="alignLeft"><label id="lblPswdLogin"> Password </label></td><td><font>:</font><input type="password" maxlength="50" size="50" name="txtPswdLogin" id="txtPswdLoginLMS" style="width: 180px"/><label class="mandatory" id="lblPswdLoginMand">*</label><br><label class="fntSmall">The password must contain at least 6 characters. Only alpha-numeric characters are allowed. The password is case-sensitive.</label></td>');
   			$("#trPswdVerify").html('<td class="alignLeft"><label id="lblPswdVerify"> Confirm Password </label></td><td><font>:</font><input type="password" maxlength="50" size="50" name="txtPswdVerifyLMS" id="txtPswdVerifyLMS" style="width: 180px"/><label class="mandatory" id="lblPswdVerifyMand">*</label></td>');
   		}
   		
   		$("#txtPswdLogin").alphaNumeric();
   		$("#txtPswdVerify").alphaNumeric();
   		$("#txtMCountryLMS").numeric();
   		$("#txtMobileLMS").numeric();
   		$("#txtMCountryLMS").val($('#txtMCountry').val());
   		$("#txtFirstNameLMS").val($('#txtFName').val());
   		$("#txtLastNameLMS").val($('#txtLName').val());
   		
   		$('#txtMCountry').val($('#txtMCountry').val());
   		$('#txtMAreaLMS').val($('#txtMArea').val());
   		$('#txtMobileLMS').val($('#txtMobile').val());
   		$("#txtFirstNameLMS,#txtLastNameLMS,#txtMiddleNameLMS").keypress(function (e) {
   			if (e.charCode != 0) {
   				var regex = new RegExp("^[a-zA-Z\\-\\s]+$");
   				var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
   				if (!regex.test(key)) {
   					e.preventDefault();
   					return false;
   				}
   			}
   		});
   		dialog.dialog( "open" );
   	}
   	

   	UI_commonSystem.showLmsInBar = function(){
   		if(UI_commonSystem.lmsDetails!=null && UI_commonSystem.lmsDetails.emailId != null
   	    		&& UI_commonSystem.lmsDetails.emailId != ""){
   			$("#tr-LMS-LoggedIn").show();
	   		if(UI_commonSystem.lmsDetails.emailStatus=="Y"){
	   			$("#lblAirewardsIDValue").html(UI_commonSystem.lmsDetails.emailId.toLowerCase());
	   			$("#lblAirewardsPointAvaialbleValue").html(UI_commonSystem.lmsDetails.availablePoints);
	   			$("#tr-LMS-NotConf").hide();
	   		}
	   		else if(UI_commonSystem.lmsDetails.emailStatus=="N"){
	   			$("#tr-LMS-NotConf").show();
	   		}
   		} else {
   			//$("#tr-LMS-LoggedIn").hide();
   			$("#tr-LMS-NotConf").hide();
   		}
   	}
   	
   	UI_commonSystem.changeUserLmsStatus = function(lmsDetails){
   		if(lmsDetails!=null){
   			UI_commonSystem.lmsDetails = lmsDetails;
   		}
   	}
   	
   	function isEmpty(s) {return (trim(s).length==0);}
   	function hasWSpace(s){return RegExp("^\s*$").test(s);}
   	function isAlpha(s){return RegExp("^[a-zA-Z]+$").test(s);}
   	function isInt(n){return RegExp("^[-+]?[0-9]+$").test(n);}
   	function isNumeric(n){return RegExp("^[0-9]+$").test(n);}
   	function isPositiveInt(n){return RegExp("^[+]?[0-9]+$").test( n );}
   	function isNegativeInt(n){return RegExp("^-[0-9]+$").test(n);}
   	function isAlphaNumeric(s) {return RegExp("^[a-zA-Z0-9-/]+$").test(s);} 
   	//function isAlphaNumericWhiteSpace(s){return RegExp("^[a-zA-Z0-9-/ \w\s]+$").test(s);}
   	function isAlphaNumericWhiteSpace(s){return RegExp("^[a-zA-Z0-9-.;,/ \w\s]+$").test(s);}
   	function isAlphaNumericWhiteSpaceComma(s){return RegExp("^[a-zA-Z0-9-,/ \w\s]+$").test(s);}
   	function isAlphaWhiteSpace(s){return RegExp("^[a-zA-Z-/ \w\s]+$").test(s);}
   	function isDecimal(n){return RegExp("^[-+]?[0-9]+[.][0-9]+$").test(n);}
   	function isDecimalWithMaxDecimalPoints(n,d){
   		if(isDecimal(n)){
   			return RegExp("^[-+]?[0-9]+[.][0-9]{1," + d + "}$").test(n);
   		}else{
   			return false
   		}
   	}
   	
    function insertParam(key, value){
        if(typeof(Storage)!=="undefined"){
            sessionStorage[key] = JSON.stringify(value);
        }
        $.data($("#sortable-ul")[0], key, value);
    }

    function readParam(key){
        var returnVal = $.data($("#sortable-ul")[0], key);
        if(typeof(Storage)!=="undefined"){
            returnVal = sessionStorage[key];
            if (returnVal!=undefined){
                returnVal = JSON.parse(returnVal);
            }else{
                returnVal = null;
            }
        }
        if (returnVal==undefined){
            returnVal = null;
        }
        if (returnVal=='null'){
            returnVal = null;
        }
        return returnVal;
    }
    UI_commonSystem.openedPopup = null;
    UI_commonSystem.sesionID = "";
    UI_commonSystem.openWindowPopup = function(linkparams){
        if(UI_commonSystem.openedPopup!=null && !UI_commonSystem.openedPopup.closed )
            UI_commonSystem.openedPopup.close();
        var url = "https://airewards.airarabia.com/portal/";
        UI_commonSystem.openedPopup = window.open(url,"",$("#popWindow").getPopWindowProp('650', '1000'));
        UI_commonSystem.openedPopup.focus();
    }



    UI_commonSystem.getAge = function (temDateVal, departureDate){
    	if(temDateVal==null){
    		return;
    	}
    	var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
    	
    	var depDateArr = departureDate.split("/");
    	
    	if(parseInt(depDateArr[1])<10){
    		depDateArr[1] = '0'+parseInt(depDateArr[1]);
    	}
    	if(parseInt(depDateArr[0])<10){
    		depDateArr[0] = '0'+parseInt(depDateArr[0]);
    	}
    	
    	var dobDateArr = temDateVal.split("/");
    	
    	if(parseInt(dobDateArr[1])<10){
    		dobDateArr[1] = '0'+parseInt(dobDateArr[1]);
    	}
    	if(parseInt(dobDateArr[0])<10){
    		dobDateArr[0] = '0'+parseInt(dobDateArr[0]);
    	}
    	
    	var strDep =(depDateArr[0]+'/'+depDateArr[1]+'/'+depDateArr[2]);
    	var strDob = (dobDateArr[0] + '/' + dobDateArr[1] + '/'+dobDateArr[2]);
    	var dob = new Date(strDob.replace(pattern,'$3-$2-$1'));		
    	var dep = new Date(strDep.replace(pattern,'$3-$2-$1'));	
    	return (dep - dob)/1000/60/60/24;
    }
    

    /* -------------------------------------S O U N D E X    N A M E   V A L I D A T I O N---------------------------------------*/
    /* ---- Algorithm 1 Steps Start ---- */
    /* Step 1 - Remove H if next char is a vowel and remove hypens and remove all single letters */
    function removeH(source) {
    	var vowels = [ "a", "e", "i", "o", "u" ], sourceLen = source.length;
    	result = "";
    	for (var idx = 0; idx < sourceLen; idx++) {
    		if ((source[idx].toUpperCase() === "H" && idx < (sourceLen - 1) && vowels
    				.indexOf(source[idx + 1]) == -1)
    				|| (source[idx].toUpperCase() === "H" && idx === (sourceLen - 1))) {
    			continue;
    		}
    		result += source[idx];
    	}

    	var chksingle = result.split(" ");
    	result = "";
    	for (var chk = 0; chk < chksingle.length; chk++) {
    		if (chksingle[chk].length === 1) {

    		} else {
    			result += chksingle[chk] + " ";
    		}
    	}
    	result = result.trim();

    	return result.replace(/-/g, "");
    }

    /* Step 2 - Remove Vowels */
    function removeVowels(sourceStrng) {
    	var vowels = /[aeiou]/gi;
    	replaceWith = ""
    	return sourceStrng.replace(vowels, replaceWith);
    }

    /* Step 3 - Replace X With KS and TH with TY */
    function rplceStrings(strngData) {
    	return strngData.replace(/X/g, "KS").replace(/TH/g, "TY");
    }

    /* Step 4 - Remove repeated and spaces */
    function removeDuplicates(replData) {
    	return replData.replace(/([A-Z])\1+/ig, "$1").replace(/\s/g, "");
    }

    /* Step 5 - Compare lengths and search shorter names */
    function algorithm1(fname1actual, fname2actual, sname1actual, sname2actual) {
    	var fname1removeH = removeH(fname1actual).toUpperCase();
    	var fname1removeV = removeVowels(fname1removeH);
    	var fname1replace = rplceStrings(fname1removeV);
    	var fname1removeD = removeDuplicates(fname1replace);

    	var fname2removeH = removeH(fname2actual).toUpperCase();
    	var fname2removeV = removeVowels(fname2removeH);
    	var fname2replace = rplceStrings(fname2removeV);
    	var fname2removeD = removeDuplicates(fname2replace);

    	var sname1removeH = removeH(sname1actual).toUpperCase();
    	var sname1removeV = removeVowels(sname1removeH);
    	var sname1replace = rplceStrings(sname1removeV);
    	var sname1removeD = removeDuplicates(sname1replace);

    	var sname2removeH = removeH(sname2actual).toUpperCase();
    	var sname2removeV = removeVowels(sname2removeH);
    	var sname2replace = rplceStrings(sname2removeV);
    	var sname2removeD = removeDuplicates(sname2replace);

    	var fnameResult;
    	if (fname1removeD.length > fname2removeD.length) {
    		fnameResult = fname1removeD.indexOf(fname2removeD) >= 0 ? true : false;
    	} else {
    		fnameResult = fname2removeD.indexOf(fname1removeD) >= 0 ? true : false;
    	}

    	var snameResult;
    	if (sname1removeD.length > sname2removeD.length) {
    		snameResult = sname1removeD.indexOf(sname2removeD) >= 0 ? true : false;
    	} else {
    		snameResult = sname2removeD.indexOf(sname1removeD) >= 0 ? true : false;
    	}

    	if (fnameResult && snameResult) {
    		return true;
    	} else {
    		/*
    		 * Step 6 - IF mismatch replace incoming name with db surname and
    		 * incoming surname with db name
    		 */
    		if (fname1removeD.length > sname2removeD.length) {
    			fnameResult = fname1removeD.indexOf(sname2removeD) >= 0 ? true
    					: false;
    		} else {
    			fnameResult = sname2removeD.indexOf(fname1removeD) >= 0 ? true
    					: false;
    		}

    		if (sname1removeD.length > fname2removeD.length) {
    			snameResult = sname1removeD.indexOf(fname2removeD) >= 0 ? true
    					: false;
    		} else {
    			snameResult = fname2removeD.indexOf(sname1removeD) >= 0 ? true
    					: false;
    		}
    		if (fnameResult && snameResult) {
    			return true;
    		} else {
    			return false;
    		}
    	}
    }
    /* ---- Algorithm 1 Steps End ---- */

    /* ---- Algorithm 3 Steps Start ---- */
    var soundex = function(s) {
    	var a = s.toLowerCase().split(''),
    	f = a.shift(), r = '', codes = {
    		a : '',
    		e : '',
    		i : '',
    		o : '',
    		u : '',
    		b : 1,
    		f : 1,
    		p : 1,
    		v : 1,
    		c : 2,
    		g : 2,
    		j : 2,
    		k : 2,
    		q : 2,
    		s : 2,
    		x : 2,
    		z : 2,
    		d : 3,
    		t : 3,
    		l : 4,
    		m : 5,
    		n : 5,
    		r : 6
    	};

    	r = f + a.map(function(v, i, a) {
    		return codes[v]
    	}).filter(function(v, i, a) {
    		return ((i === 0) ? v !== codes[f] : v !== a[i - 1]);
    	}).join('');

    	return (r + '000').slice(0, 4).toUpperCase();
    };

    function getFormatString(name) {
    	var param = name;
    	var getData = param.split(" ");
    	var length = 0;
    	var position = 0;
    	var res;

    	return getData.sort(function(a, b) {
    		return b.length - a.length;
    	})[0].replace(/-/g, '');
    }

    function compare(firstName1, lastName1, firstName2, lastName2) {

    	/* Step 1 Start */
    	var fname1 = firstName1.trim();
    	var fname2 = firstName2.trim();
    	var sname1 = lastName1.trim();
    	var sname2 = lastName2.trim();

    	var regex = new RegExp("^[a-zA-Z]+[.-]?[a-zA-Z]*$");

    	if ((!fname1 || !sname1)
    			|| (!regex.test(fname1.replace(/ /g, "")) || !regex.test(sname1
    					.replace(/ /g, "")))) {
    		return false;
    	}

    	fname1 = getFormatString(fname1);
    	fname2 = getFormatString(fname2);
    	sname1 = getFormatString(sname1);
    	sname2 = getFormatString(sname2);

    	if ((fname1 === fname2) && (sname1 === sname2)) {
    		return true;
    	} else {
    		/* Step 2 Start */
    		var param1 = soundex(fname1);
    		var param2 = soundex(fname2);
    		var param3 = soundex(sname1);
    		var param4 = soundex(sname2);

    		if ((param1 === param2) && (param3 === param4)) {
    			return true;
    		} else {
    			/* Step 3 Start */

    			var fname1actual = firstName1;
    			var fname1value = fname1actual.split(" ").join("");
    			var fname2actual = firstName2;
    			var fname2value = fname2actual.split(" ").join("");
    			var sname1actual = lastName1;
    			var sname1value = sname1actual.split(" ").join("");
    			var sname2actual = lastName2;
    			var sname2value = sname2actual.split(" ").join("");

    			if ((fname1value === fname2value) && (sname1value === sname2value)) {
    				return true;
    			} else {
    				/* Step 4 Start */
    				var param1value = soundex(fname1value);
    				var param2value = soundex(fname2value);
    				var param3value = soundex(sname1value);
    				var param4value = soundex(sname2value);

    				if ((param1value === param2value)
    						&& (param3value === param4value)) {
    					return true;
    				} else {
    					/* Step 5 Start */
    					if ((fname1actual === sname2actual)
    							&& (fname2actual === sname1actual)) {
    						return true;
    					} else {
    						if ((fname1 === sname2) && (fname2 === sname1)) {
    							return true;
    						} else {
    							if ((param3 === param2) && (param1 === param4)) {
    								return true;
    							} else {
    								if ((fname1value === sname2value)
    										&& (fname2value === sname1value)) {
    									return true;
    								} else {
    									if ((param1value === param4value)
    											&& (param3value === param2value)) {
    										return true;
    									} else {
    										return algorithm1(fname1actual,
    												fname2actual, sname1actual,
    												sname2actual);
    									}
    								}
    							}
    						}
    					}
    				}
    				return false;
    			}
    		}
    	}
    }
    
   	
	/* ------------------------------------------------------- End of File ---------------------------------- */
	