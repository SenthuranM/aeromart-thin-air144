(function($) {
	 $.fn.stiky = function(settings) {
	 var slideType  = "";
     var config = {
		scrolltype:"slide", //slide or fixed
		scrollspeed:350, //less value more speed
		headergap:100, //keep top header space
		footergap:100, //keep top header space
		topgap:0, //keep top header space
		panelheader:"none", //add new header to panel pass HTML oe TEXT to Render
		pinIconClass:"x" //add new header to panel pass HTML oe TEXT to Render
     };
	 
	 if (settings) $.extend(config, settings);
	 
		 return this.each(function() {
			var pined = false;
			var el = $(this);
			var pin = "";
			
			if (settings.pinIconClass != "x")
				pin = "class ="+settings.pinIconClass+"";
			
			slideType = (settings.scrolltype == 'slide') ? "relative" : "fixed" ;
			if (settings.scrolltype != "normal")
				el.css("position",slideType);
			
			if (settings.panelheader != "none")
				el.prepend('<div class="sHeader"><span>'+settings.panelheader+
						'</span><a href="javascript:void(0);" '+pin+'>&nbsp;</a><span style="clear:both"></span></div>');
			
			$(window).scroll(function () {
				var scrolltopValue = $(this).scrollTop();
				 if (slideType == 'relative' && settings.scrolltype == 'slide'){
					var topv = (scrolltopValue < settings.headergap) ? 
							0 + settings.topgap  : scrolltopValue - ( settings.headergap  + settings.topgap );
					
					/*var topv1 = ( parseInt(el.height() + topv) > parseInt($(document).height() - settings.footergap) ) ? 
							topv + parseInt(el.height() ) + "px" : topv + "px";*/
					if ( parseInt(el.height() + topv) < parseInt( $(document).height() - (settings.footergap)) ){	
						  el.animate({
							top:topv
						  },{queue: false, duration:settings.scrollspeed});
					}
				};
				
			 });
		el.find(".sHeader a").unbind("click").bind("click",function(){
				if (pined){							 
					slideType = "absolute";
					$(this).removeClass("pined")
					pined = false;
				}else{
					slideType = "fixed";
					$(this).addClass("pined")
					pined = true;
				}
			});
			
		 });
	 };
})(jQuery);