/** Flight Search
 * 
 * @author Pradeep Karunanayake
 *  
 */

function UI_FlightSearch(){};
 
;(function(UI_FlightSearch) {	

	UI_FlightSearch.errorInfo = "";
	UI_FlightSearch.arrCurrency = "";
	UI_FlightSearch.strFromHD = "";
	UI_FlightSearch.strToHD = "";
	UI_FlightSearch.strDash = "----------------------";	
	UI_FlightSearch.defaultPaxCategory = "A";
	UI_FlightSearch.arrCusType = "";
	UI_FlightSearch.arrFareCat = "";
	UI_FlightSearch.isLoaded = false;
	UI_FlightSearch.isModifySegment = false;
	UI_FlightSearch.msgMaxAdultChildCount = 0;
	UI_FlightSearch.ondDataMap = [];
	UI_FlightSearch.originList = [];
	UI_FlightSearch.currencyList = [];
	UI_FlightSearch.hubDesc = "";
	UI_FlightSearch.isAddGroundSegment = false;
	UI_FlightSearch.isAddBusSegment = false;
	UI_FlightSearch.modifyFlexiBooking = false;
	UI_FlightSearch.existingClassOfService = "";
	UI_FlightSearch.OND_SEQUENCE = {OUT_BOUND:0, IN_BOUND:1};
	UI_FlightSearch.isPromoCodeEnabled = false;

	
	UI_FlightSearch.surfaceSegmentConfigs = {
			sufaceSegmentAppendStr : "--:",
			surfSegCodeSplitter : ":"	
		}
	$(document).ready(function(){
		UI_FlightSearch.ready();
		UI_FlightSearch.isLoaded = true;
	});
	
	UI_FlightSearch.ready = function() {
		var noMonth = globalConfig.noOfMonthsinCalendaraPopup;
		$("#departureDateBox").datepicker({
			regional:SYS_IBECommonParam.locale,
			numberOfMonths: noMonth,
			dateFormat: "d M yy",
			altFormat: "dd/mm/yy",
			altField: "#departureDate",
			minDate: +0,
			//maxDate: '+1M +10D',
			showOn: 'both',
			buttonImage: globalConfig.calendaImagePath,
			buttonImageOnly: true,
			showButtonPanel: true,
			onSelect: function(dateStr) {
				var min = $(this).datepicker('getDate') || new Date(); // Selected date or today if none
				 $('#returnDateBox').datepicker('option', {minDate: min});
			}
		});

		$("#returnDateBox").datepicker({regional: SYS_IBECommonParam.locale ,
			numberOfMonths: noMonth,
			dateFormat: "d M yy",
			altFormat: "dd/mm/yy",
			altField: "#returnDate",
			minDate: +0,
			//maxDate: '+1M +10D',
			showOn: 'both',
			buttonImage: globalConfig.calendaImagePath,
			buttonImageOnly: true,
			showButtonPanel: true			
		});
		
		UI_FlightSearch.hub = UI_Top.holder().GLOBALS.hub;
		
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
				UI_FlightSearch.ondListLoaded()){
			UI_FlightSearch.initDynamicOndList();
		}
		
		if (UI_FlightSearch.isLoaded != true) {
			$("#selFromLoc").change(function(){UI_FlightSearch.selFromLocOnChange();});
			$("#selToLoc").change(function(){UI_FlightSearch.selToLocOnChange();});
			$("#chkReturnTrip").click(function(){UI_FlightSearch.chkReturnTripClick();});
			$("#selAdults").change(function(){UI_FlightSearch.selAdultsOnChange();});			
			$("#btnSearch").unbind('click').bind('click',function(){UI_FlightSearch.flightSearchBtnOnClick(0);});
		}
		
		UI_FlightSearch.isPromoCodeEnabled = UI_Top.holder().GLOBALS.promoCodeEnabled;
		if(!UI_FlightSearch.isModifySegment && UI_FlightSearch.isPromoCodeEnabled){			
			$("#divPromoCode").show();
			$("#promoCode").alphaNumeric();
			$("#bankIdNo").numeric({allowDecimal:false});
		} else {
			$("#divPromoCode").hide();
		}
		
	}
	
	UI_FlightSearch.pageInit = function(response) {	
		if (response != null) {
			UI_FlightSearch.strFromHD = response.jsonLabel.lblWhereFrom;
			UI_FlightSearch.strToHD = response.jsonLabel.lblWhereTo;
			UI_FlightSearch.msgMaxAdultChildCount = response.jsonLabel.msgMaxAdultChildCount;
			UI_FlightSearch.errorInfo = response.errorInfo;			
			UI_FlightSearch.fillDropDown(response.searchInfo);
			UI_FlightSearch.setDefaultFareType();
			UI_FlightSearch.flightSearchPopulateData();

		}
	}
	
	UI_FlightSearch.fillDropDown = function(searchInfo) {
			
		UI_FlightSearch.arrCusType = searchInfo.paxCategories;
		UI_FlightSearch.arrFareCat = searchInfo.fareTypes;
		
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
				UI_FlightSearch.ondListLoaded()) {		
			
			var fromAirpotList = UI_FlightSearch.setAirportData(UI_FlightSearch.setAirportListHead('from') , UI_FlightSearch.originList);
			//to locations should be set only to selects
			UI_FlightSearch.fillFromToAirportDD(fromAirpotList,UI_FlightSearch.toRenameAirpots(fromAirpotList));		
			searchInfo.currencyInfo = UI_FlightSearch.currencyList;
			
		} else {			
			var fromAirpotList = UI_FlightSearch.setAirportData(UI_FlightSearch.setAirportListHead('from') , searchInfo.airportList);	
			UI_FlightSearch.fillFromToAirportDD(fromAirpotList,UI_FlightSearch.toRenameAirpots(fromAirpotList));
			
		}
		
		//Set from locations, and set to please select
		UI_FlightSearch.arrCurrency  = searchInfo.currencyInfo;
		$("#selCurrency").fillDropDown({dataArray:UI_FlightSearch.mergeKeyValueData(searchInfo.currencyInfo), keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selCOS").fillDropDown({dataArray:searchInfo.cos, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selType").fillDropDown({dataArray:UI_FlightSearch.createPaxCategoryList(searchInfo.paxCategories), keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selFareType").fillDropDown({dataArray:UI_FlightSearch.createFareTypeList(searchInfo.fareTypes), keyIndex:0, valueIndex:1, firstEmpty:false});
	}
	
	UI_FlightSearch.fillFromToAirportDD = function (fromAirportList, toAirportList){
		if(UI_FlightSearch.previousAirportData.isEmpty()){
			UI_FlightSearch.previousAirportData.fromAirportsPrevious = fromAirportList;
			UI_FlightSearch.previousAirportData.toAirportsPrevious = toAirportList;
		}else {
			//if not assuming modifysegment
			UI_FlightSearch.previousAirportData.fromAirportGroundOnlyData = fromAirportList;
			UI_FlightSearch.previousAirportData.toAirportGroundOnlyData = toAirportList;
		}
		$("#selFromLoc").children().remove();
		$("#selToLoc").children().remove();
	
		if(!UI_FlightSearch.isAddBusSegment){
			  $("#selFromLoc").fillDropDown({dataArray:fromAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
			  $("#selToLoc").fillDropDown({dataArray:toAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
		} else {

			//Setting language descriptions from airport list
			   if(UI_Top.holder().GLOBALS.dynamicOndListEnabled && UI_FlightSearch.ondListLoaded()){
				
				   var lang = UI_Top.holder().GLOBALS.sysLanguage;
				   if ($("#locale").val() != null && $("#locale").val() != "") {
						lang = $("#locale").val();
				   }
		
				   if(lang != 'en') {
					   for(var i in fromAirportList){
						   for(var j in airports){
							   if(fromAirportList[i][0] == airports[j]['code'] && airports[j][lang] != null){
								   fromAirportList[i][1] = airports[j][lang];
								   break;
							   }
						   }
					   }
		
					   for(var k in toAirportList){
						   for(var l in airports){
							   if(toAirportList[k][0] == airports[l]['code'] && airports[l][lang] != null){
								   toAirportList[k][1] = airports[l][lang];
								   break;
							   }
						   }
		
					   }
				   }
			   }
			    
			   $("#selFromLoc").fillDropDown({dataArray:fromAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
			   $("#selToLoc").fillDropDown({dataArray:toAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});
			   $("#hndFromAirport").val($("#selFromLoc :selected").text());
			   $("#hndToAirport").val($("#selToLoc :selected").text());
		}		
	}
	
	UI_FlightSearch.mergeKeyValueData = function(data) {
		var temp = "";
		if (data != null) {
			for(var i = 0; i < data.length; i++) {
				temp = data[i][1];			
				delete data[i][1];
				data[i][1] = data[i][0] + " - " + temp;
			}
		}
		
		return data;
	}
	
	UI_FlightSearch.setAirportListHead = function(params) {
		var data = [];
		data[0] = [];
		data[0][0]= "";		
		
		if(params == 'from'){
			data[0][1] = UI_FlightSearch.strFromHD; 
		} else {
			data[0][1] = UI_FlightSearch.strToHD; 
		}
		
		
		/*data[1] = [];
		data[1][0] = "";
		data[1][1] = UI_FlightSearch.strDash;
		
		data[2] = [];
		data[2][0] = UI_FlightSearch.hub;
		data[2][1] = UI_FlightSearch.hubDesc ;
		
		data[3] = [];
		data[3][0] = "";
		data[3][1] = UI_FlightSearch.strDash;*/
		
		return  data;
	}
	
	
	UI_FlightSearch.setAirportData =  function(head, data) {		
		/*for (var i = 0; i < data.length; i++) {
			if (data[i][0] == UI_FlightSearch.hub){
				head[2][1] = data[i][1];				
				data.splice(i, 1);			
				break;
			}
		}*/		
		return head.concat(data);
	}
	
	UI_FlightSearch.toRenameAirpots = function(data) {	
		var toArray = $.extend(true, [], data);
		toArray[0][1] = UI_FlightSearch.strToHD;
		return toArray;
	}
	
	UI_FlightSearch.createPaxCategoryList = function(paxList) {
		var data = [];
		for (var i = 0; i < paxList.length; i++) {			
			data[i] = [];
			data[i][0]= paxList[i].paxCategoryCode;		
			data[i][1] = paxList[i].paxCategoryDesc;	
		}
		return data;
	}
	
	UI_FlightSearch.createFareTypeList = function(fareList) {
		var data = [];
		for (var i = 0; i < fareList.length; i++) {			
			data[i] = [];
			data[i][0]= fareList[i].fareCategoryCode;		
			data[i][1] = fareList[i].fareCategoryDesc;	
		}
		return data;
	}
	
	/**
	 * Set Default Fare Type
	 */
	UI_FlightSearch.setDefaultFareType = function() {
		var arrFareCat = UI_FlightSearch.arrFareCat;
		for(var strFare = 0; strFare < arrFareCat.length; strFare++) {
			if((arrFareCat[strFare].status).toUpperCase() == 'ACT'){
				UI_FlightSearch.defaultFareType = arrFareCat[strFare].fareCategoryCode;
			}
		}
		if(UI_FlightSearch.defaultFareType ==""){
			UI_FlightSearch.defaultFareType ="A";
		}
	}
	
	UI_FlightSearch.flightSearchPopulateData = function() {
		
		var arrAdults = new Array();
		var arrChild  = new Array();
		
		arrChild[0] = new Array();
		arrChild[0][0] = "0";
		for (var i = 1 ; i <= UI_Top.holder().GLOBALS.flightSeachMaxAdult ; i++){
			arrAdults[i - 1] = new Array();
			arrAdults[i - 1][0] = i;
			
			arrChild[i] = new Array();
			arrChild[i][0] = i;
		}
		$("#frmFlightSearch").find("#selAdults").fillDropDown({dataArray:arrAdults, keyIndex:0, valueIndex:0, firstEmpty:false});
		$("#frmFlightSearch").find("#selChild").fillDropDown({dataArray:arrChild, keyIndex:0, valueIndex:0, firstEmpty:false});		
		
		var arrDays = new Array();		
		
		arrDays.length = 0		
		var cusTypes = UI_FlightSearch.arrCusType;
		if ((cusTypes.length == 1) && (cusTypes[0].paxCategoryCode == UI_FlightSearch.defaultPaxCategory)) {
			$('#trPaxCategoryRow').hide();
			$('#trPaxCategoryLabel').hide();
			$('#trPaxCategoryList').hide();
			$("#selType").val(UI_FlightSearch.defaultPaxCategory);			
		}		
	    var arrFareCat = UI_FlightSearch.arrFareCat;
		if((arrFareCat.length == 1 || arrFareCat.length < 1)){
			$('#trFareTypeRow').hide();
			$('#trFareTypeLabel').hide();
			$('#trFareTypeList').hide();
			$("#selFareType").val(UI_FlightSearch.defaultFareType);			
		}	
		
		//if(UI_Top.holder().GLOBALS.isCOSSelectionEnabled == false || UI_Top.holder().GLOBALS.isCOSSelectionEnabled =='false'){
		//	$('#trClass1').hide();
		//	$('#trClass2').hide();
		//	$('#trClass3').hide();
		//} else {
			$('#trClass1').show();
			$('#trClass2').show();
			$('#trClass3').show();
		//}
		UI_FlightSearch.defaultDataLoad();		
	}
	
	UI_FlightSearch.defaultDataLoad = function() {
			var dtDepDate = addDays(UI_Top.holder().dtSysDate, Number(UI_Top.holder().GLOBALS.depDateDiffrence)); 
			var temDepDate = dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear();
			var strDDate = dateChk(temDepDate);
			strSeleDDay = dtDepDate.getDate();	
	
			var dtRetuDate = "";
			var strRDate = "";
			strSeleRDay = "  /  /    ";
			if (String(UI_Top.holder().GLOBALS.retDateDiffrence) != ""){
				var dtRetuDate = addDays(UI_Top.holder().dtSysDate, Number(UI_Top.holder().GLOBALS.retDateDiffrence));
				strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
				strSeleRDay = dtRetuDate.getDate();				
				$("#chkReturnTrip:checked");				
			}
			$("#departureDateBox").datepicker( "setDate" , dtDepDate); 
			$("#returnDateBox").datepicker( "setDate" , dtDepDate); 
			$("#selCOS").val(UI_Top.holder().GLOBALS.classOfService);
			if($("#resSelectedCurrency").val() != "" && $("#resSelectedCurrency").val() != null){
				$("#selCurrency").val($("#resSelectedCurrency").val());
			} else {
				$("#selCurrency").val(UI_Top.holder().GLOBALS.baseCurrency);
			}

			UI_FlightSearch.selAdultsOnChange();
			UI_FlightSearch.chkReturnTripClick();
	}
	
	
	UI_FlightSearch.chkReturnTripClick = function() { 
		if ($("#chkReturnTrip").is(':checked')) {			
			$("#resReturnFlag").val(true);
			$("#returnDateBox").datepicker("enable");
			$("input[name='inboundStopOver']").enable();
		}else{			
			$("#resReturnFlag").val(false);
			$("#returnDateBox").val("");
			$("#returnDateBox").datepicker("disable");
			$("#returnDate").val("");	
			$("input[name='inboundStopOver']").val(0).disable();
		}
		
	}
	
	UI_FlightSearch.selToLocOnChange = function() {
		if(!UI_FlightSearch.isAddBusSegment){
			if(trim($("#selToLoc").val()).indexOf(UI_FlightSearch.surfaceSegmentConfigs.surfSegCodeSplitter) != -1){
				var splited = $("#selToLoc").val().split(UI_FlightSearch.surfaceSegmentConfigs.surfSegCodeSplitter);
				$("#hndToAirport").val(splited[0]);
				$("#hdnToAirportSubStation").val(splited[1]);
			}else {
				if ($("#selToLoc :selected").text() == UI_FlightSearch.strDash ||
						$("#selToLoc").val() == "") {
					$("#selToLoc option:first").attr("selected",true);
				} else{
					$("#hndToAirport").val($("#selToLoc :selected").text());
				}
			}
			
			UI_FlightSearch.setDefaultSearchOption($("#selFromLoc").val(), $("#selToLoc").val());
		} else {
			$("#hndToAirport").val($("#selToLoc :selected").text());
		}

	}
	
	
	
	UI_FlightSearch.selFromLocOnChange = function() {
		
		
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
				UI_FlightSearch.ondListLoaded()){

			if(!UI_FlightSearch.isAddBusSegment){

				if ($("#selFromLoc :selected").text() == UI_FlightSearch.strDash ||
						$("#selFromLoc").val() == ""){
					$("#selFromLoc option:first").attr("selected",true);
				} else{			
					$("#hndFromAirport").val($("#selFromLoc :selected").text());
					//Set new list
					var currentTo = $("#selToLoc").val();
					var currentFrom = $("#selFromLoc").val();
					$("#selToLoc").children().remove();

					var toAirportList = [];
					/*if(currentFrom == UI_FlightSearch.hub){

				   var data = [];
			       data[0] = [];
				   data[0][0]= "";		
				   data[0][1] = UI_FlightSearch.strToHD; 

				   data[1] = [];
				   data[1][0] = "";
				   data[1][1] = UI_FlightSearch.strDash;
				   toAirportList = data.concat(UI_FlightSearch.ondDataMap[currentFrom]);
				} else {
				  toAirportList = UI_FlightSearch.setAirportData(UI_FlightSearch.setAirportListHead('to') , UI_FlightSearch.ondDataMap[currentFrom]);	
				}*/

					toAirportList = UI_FlightSearch.setAirportData(UI_FlightSearch.setAirportListHead('to') , UI_FlightSearch.ondDataMap[currentFrom]);	

					$("#selToLoc, #selToLoc_dummy").fillDropDown({dataArray:toAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});	


				}	
			} else {
				$("#hndFromAirport").val($("#selFromLoc :selected").text());
			}

		} else {
			
			if(!UI_FlightSearch.isAddBusSegment){
				
				if(trim($("#selFromLoc").val()).indexOf(UI_FlightSearch.surfaceSegmentConfigs.surfSegCodeSplitter) != -1){
					var splited = $("#selFromLoc").val().split(UI_FlightSearch.surfaceSegmentConfigs.surfSegCodeSplitter);
					$("#hndFromAirport").val(splited[0]);
					$("#hdnFromAirportSubStation").val(splited[1]);
				}else {
					if ($("#selFromLoc :selected").text() == UI_FlightSearch.strDash ||
							$("#selFromLoc").val() == ""){
						$("#selFromLoc option:first").attr("selected",true);
					} else{			
						$("#hndFromAirport").val($("#selFromLoc :selected").text());
					}	
				}
			} else {
				$("#hndFromAirport").val($("#selFromLoc :selected").text());
			}
		}	
	
	}
	
	UI_FlightSearch.selAdultsOnChange = function(){
		var arrInfants = new Array();
		var arrChild  = new Array();
		var intAdults = Number($("#selAdults").val());
		var strCInfants = $("#selInfants").val();
		var intChilds = $("#selChild").val();
		
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		$("#selInfants").empty();
		$("#selInfants").fillDropDown({dataArray:arrInfants, keyIndex:0, valueIndex:0, firstEmpty:false});
		
		if (strCInfants > 0) {
			strCInfants = (intAdults >= strCInfants) ? strCInfants : intAdults;
			$("#selInfants").val(strCInfants);
		}
		
		if(UI_FlightSearch.isModifySegment == false) {
			// Child Count Change According to Adult Count.		
			var numOfChild = UI_Top.holder().GLOBALS.flightSeachMaxAdult - intAdults;
			for (var i = 0 ; i <= numOfChild ; i++){
				arrChild[i] = new Array();
				arrChild[i][0] = i;
			}
			$("#selChild").empty();
			$("#selChild").fillDropDown({dataArray:arrChild, keyIndex:0, valueIndex:0, firstEmpty:false});	
			
			if (intChilds > 0) {			
				if (numOfChild < intChilds) {	
					jAlert(UI_FlightSearch.msgMaxAdultChildCount + " " + UI_Top.holder().GLOBALS.flightSeachMaxAdult + ".");
					intChilds = numOfChild;
				} 
				$("#selChild").val(intChilds);
			}
		}
		
	}	
	
	UI_FlightSearch.flightSearchBtnOnClick = function(intID){
		switch (intID){
			case 0 : 
				if (UI_FlightSearch.clientValidateFS()){
					//This is doing aftre OND selection completion time
					//UI_FlightSearch.setDefaultSearchOption($("#selFromLoc").val(), $("#selToLoc").val());
					UI_commonSystem.loadingProgress();
					//segment modification IBE
					$("#frmFlightSearch").find("#selFromLoc").enable();
					$("#frmFlightSearch").find("#selToLoc").enable();
					$("#resLocale").val($("#locale").val());
					$("#resNonSecurePath").val($("#nonSecurePath").val());
					$("#resSecurePath").val($("#securePath").val());
					$("#resHomeURL").val($("#homeURL").val());					
					$("#resRegCustomer").val($("#regCustomer").val());
					if(typeof(UI_CustReservation) != 'undefined'){			
//						var isOutboundFlexiRes = UI_FlightSearch.isFlexiResModify(UI_CustReservation.flightDepDetails);	
//						var isInboundFlexiRes  = UI_FlightSearch.isFlexiResModify(UI_CustReservation.flightRetDetails);	
//						var ondFlexiQuote = {}; 
//						ondFlexiQuote[UI_FlightSearch.OND_SEQUENCE.OUT_BOUND] = !isOutboundFlexiRes;
//						ondFlexiQuote[UI_FlightSearch.OND_SEQUENCE.IN_BOUND] = !isInboundFlexiRes;
//						$("#resOndQuoteFlexi").val($.toJSON(ondFlexiQuote));
//						$("#resFlexiSelected").val(isOutboundFlexiRes && isInboundFlexiRes);
						
						if(UI_FlightSearch.isModifySegment) {
							
							$("#fltPGWPaymentMobileNumber").val($("#pgwPaymentMobileNumber").val());	
							$("#fltPGWPaymentEmail").val($("#pgwPaymentEmail").val());
							$("#fltPGWPaymentCustomerName").val($("#pgwPaymentCustomerName").val());
							
							$("#requoteFlightSearch").val(UI_CustReservation.isRequoteFlow);	
						}						
						
					} else {
						var ondFlexiQuote = {}; 
						ondFlexiQuote[UI_FlightSearch.OND_SEQUENCE.OUT_BOUND] = true;
						ondFlexiQuote[UI_FlightSearch.OND_SEQUENCE.IN_BOUND] = true;
						$("#resOndQuoteFlexi").val($.toJSON(ondFlexiQuote));
						$("#resFlexiSelected").val(false);
						$("#requoteFlightSearch").val(false);
					}
					$("#departureDate").val(dateToGregorian($("#departureDate").val(), SYS_IBECommonParam.locale));
					$("#returnDate").val(dateToGregorian($("#returnDate").val(), SYS_IBECommonParam.locale));
					var hubTimeJson=[];
					var strHubTime="";
					
					if ($("#chkStopOver").is(':checked')){
						$.each(UI_FlightSearch.hubTimeDetailJSON, function(i,j){
							var tempModel = {};
							tempModel.hubCode = j.hubCode;
							var obStopOver=($("#outboundStopOver_"+i).val()=="")? 0:$("#outboundStopOver_"+i).val();
							var ibStopOver=($("#inboundStopOver_"+i).val()=="")? 0:$("#inboundStopOver_"+i).val();
							tempModel.outboundStopOver = obStopOver;
							tempModel.inboundStopOver = ibStopOver;
							hubTimeJson[i]=tempModel;
							strHubTime=","+strHubTime+j.hubCode+"_"+obStopOver+"_"+ibStopOver;
							 
						});			
						strHubTime=strHubTime.substr(1);
					}				 
					
					$("#hubTimeDetails").val(strHubTime);			
					$("#hubTimeDetailJSON").val(JSON.stringify(hubTimeJson, null));
					$("#frmFlightSearch").attr('action','showLoadPage!loadFlightSearch.action');					 
					$("#frmFlightSearch").submit();	
					
				}
				break;
			 case 2 : 
				 $("#frmFlightSearch").attr({'action':'showLoadPage!loadFlightSearch.action', 'target':'_top'});
					var hidfromRegUser = $("<input id='fromRegUserPage' name='fromRegUserPage' type='hidden' value='true'>");
					$("#frmFlightSearch").append(hidfromRegUser);
					$("#frmFlightSearch").submit();
					break;
		}
	}
	
	UI_FlightSearch.isFlexiResModify = function(flightSegList){
		if(UI_CustReservation.resFlexiAlerts != null &&
				UI_CustReservation.resFlexiAlerts != []){			
			for(var i=0;i<UI_CustReservation.resFlexiAlerts.length;i++){
				var flexiObj = UI_CustReservation.resFlexiAlerts[i];
				var result = _isFlightHasFlexi(flightSegList, flexiObj.flightSegmantRefNumber);
				if(result){
					return true;
				}	
			}
		}
		
		return false;
	}
	
	_isFlightHasFlexi = function(flightSegList, flightRefNo){
		if(flightSegList != null){
			for(var i=0;i<flightSegList.length;i++){
				if(flightSegList[i].flightSegmentRefNumber == flightRefNo){
					return true;
				}
			}
		}
		
		return false;
	}
	
	//return the hub name from airport list as necessary for the ibe
	getHubName = function(code){
		var hName = code;
		for (var i = 0;i<airports.length;i++){
			if (code == airports[i].code){
				hName = airports[i].en;
				break;
			}
		}
		return hName;
	}
	
	UI_FlightSearch.setDefaultSearchOption = function (from, to){
		$("#trCheckStopOver").hide();//to hide after ond selection
		UI_FlightSearch.hubTimeDetailJSON = [];
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
				UI_FlightSearch.ondListLoaded()){	
			var fromIndex = -1;
			var toIndex = -1;
			//get from, to indexes
			var airportsLength = airports.length;
			for(var i=0;i<airportsLength;i++){
				if(from == airports[i].code){
					fromIndex = i;
				} else if(to == airports[i].code) {
					toIndex = i;
				}
				
				if(fromIndex != -1 && toIndex != -1){
					break;
				}
			}
			
			if(fromIndex != -1){			
				var availDestOptLength = origins[fromIndex].length;
				for(var i=0;i<availDestOptLength;i++){
					var destOptArr = origins[fromIndex][i];
					if(destOptArr[0] == toIndex){
						var isInt = false;
						var optCarriersArr = destOptArr[3].split(",");
						for(var j=0;j<optCarriersArr.length;j++){
							if(optCarriersArr[j] != GLOBALS.defaultCarrierCode){
								isInt = true;
								break;
							}
						}
						
						if(isInt){
							$('#resSearchSystem').val('INT');
						} else {
							$('#resSearchSystem').val('AA');
						}
						
						if(UI_Top.holder().GLOBALS.userDefineTransitTimeEnabled && !UI_FlightSearch.isModifySegment){
							//prepare HUB stopover time option 
							var hubCodeList = destOptArr[5];
							if(hubCodeList != null && hubCodeList.length > 0){
								for(var k = 0; k < hubCodeList.length; k++){
									var hubCodes = hubCodeList[k];
									if(hubCodes != null && hubCodes.length > 0){
										for(var l = 0; l < hubCodes.length ; l++){
											var tempModel = {};
											tempModel.hubCode = hubCodes[l];
											tempModel.outboundStopOver = 0;
											tempModel.inboundStopOver = 0;
											UI_FlightSearch.hubTimeDetailJSON[UI_FlightSearch.hubTimeDetailJSON.length] = tempModel;
											//Calling to the chkStopOverAdd(); to display the checkbox	
											chkStopOverAdd();
										}
										
									}
								}
							}
						}
						break;
					}
				}
			} else {			
				$('#resSearchSystem').val('INT');
			}
		} else {
			$('#resSearchSystem').val('');
		}
		
	}
	
	UI_FlightSearch.ondListLoaded = function(){
		if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
			return false;
		} else {
			return true;
		}
	}
	
	UI_FlightSearch.clientValidateFS = function() {
		var arrError = UI_FlightSearch.errorInfo;
		if ($("#selFromLoc").val() == "") {
			jAlert(arrError["ERR001"]);		
			$("#selFromLoc").focus();		
			return false;
		}
		
		if ($("#selToLoc").val() == ""){
			jAlert(arrError["ERR002"]);
			$("#selToLoc").focus();
			return false;
		}		
		
		if ($("#selFromLoc").val() == $("#selToLoc").val()){
			jAlert(arrError["ERR003"]);
			$("#selToLoc").focus();			
			return false;
		}
		
		if ($.trim($("#departureDate").val()) == ""){
			jAlert(arrError["ERR007"]); 	
			$("#departureDateBox").focus();		
			return false;
		}else{
			if (!CheckDates(UI_Top.holder().strSysDate, dateToGregorian($.trim($("#departureDate").val()), SYS_IBECommonParam.locale))){
				jAlert(arrError["ERR009"] + UI_Top.holder().strSysDate + ".");
				$("#departureDateBox").focus();		
				return false;
			}			
		}
		
		if ($("#chkReturnTrip").is(':checked')){
			if ($("#returnDate").val() == ""){
				jAlert(arrError["ERR008"]); 	
				$("#returnDateBox").focus();		
				return false;
			}
		
			if (!CheckDates(dateToGregorian($.trim($("#departureDate").val()), SYS_IBECommonParam.locale) 
					, dateToGregorian($.trim($("#returnDate").val()), SYS_IBECommonParam.locale))){
				jAlert(arrError["ERR004"]);
				$("#returnDateBox").focus();	
				return false;
			}
		}
		
		if ($("#selCOS").val() == ""){
			jAlert(arrError["ERR010"]);
			$("#selCOS").focus();
			return false;
		}
		
		if(!UI_FlightSearch.isModifySegment && UI_FlightSearch.isPromoCodeEnabled){			
			var bankIdNo = $("#bankIdNo").val();
			if(bankIdNo != '' && bankIdNo.length < 6){
				jAlert(arrError['ERR096']);
				$("#bankIdNo").focus();
				return false;
			}
		}
				
		if ($("#selType").val() == "") {
			jAlert(arrError["ERR011"]);
			$("#selType").focus();			
			return false;
		}
		
	    if ($("#selFareType").val() == "") {
		    jAlert(arrError["ERR012"]);
			$("#selFareType").focus();			
			return false;
		}
		if ($("#selCurrency").val() == ""){
			jAlert(arrError["ERR006"]);
			$("#selCurrency").focus();
			return false;
		}	
		
		if(!UI_FlightSearch.isModifySegment) {
			if (Number($("#selAdults").val()) + Number($("#selChild").val()) > UI_Top.holder().GLOBALS.flightSeachMaxAdult) {
				jAlert(UI_FlightSearch.msgMaxAdultChildCount + " " + UI_Top.holder().GLOBALS.flightSeachMaxAdult + ".");
				$("#selAdults").focus();
				return false;
			}
        }
		
		//when downgrading to lower COS not enabled following validation required
		if(!UI_Top.holder().GLOBALS.allowModifyBookingsToLowPriorityCOS 
				&& UI_FlightSearch.isModifySegment 
				&& UI_CustReservation.availableCOSRankMap!=null 
				&& UI_FlightSearch.existingClassOfService!=null 
				&& UI_FlightSearch.existingClassOfService !="" 
					&& $("#selCOS").val()!=null && $("#selCOS").val()!="") {
			var changedCCMap = UI_CustReservation.availableCOSRankMap;				
			var newCOSRank = changedCCMap[UI_FlightSearch.existingClassOfService];
			var existCOSRank = 	changedCCMap[$("#selCOS").val()];
			
			if(Number(existCOSRank) > Number(newCOSRank)){
				jAlert(arrError["ERR095"]);			
				$("#selCOS").focus();
				return false;
			}			
		}
				
		return true;
	}
	
	/**
	 * Set Modify Flight Search data
	 */	
	UI_FlightSearch.setModifySegmentSearchData = function(modifySegment, resFlexiAlerts,allowModifyDate,allowModifyRoute) {
		
		_getFlightRefNumber = function(fltRefNumStr){
			var refArr = fltRefNumStr.split("-");
			return refArr[0];
		}

		_isModifyingFlightSegment = function(flightRefNum, pnrSegId, modifyingFlightSegs){
			for(var i=0;i<modifyingFlightSegs.length;i++){
				if(_getFlightRefNumber(modifyingFlightSegs[i].flightRefNumber) == flightRefNum
						&& parseInt(modifyingFlightSegs[i].pnrSegID) == pnrSegId){
					return true;
				}
			}
			return false;
		}		
		
		$("#trCheckStopOver").hide();//to hide after ond selection
		if (modifySegment != null) {	
			UI_FlightSearch.isModifySegment = true;
			var stringDateArr = (modifySegment.depDate).split("/");
			$("#frmFlightSearch").find("#selFromLoc").val(modifySegment.from);
			$("#frmFlightSearch").find("#selToLoc").val(modifySegment.to);			
			$("#frmFlightSearch").find("#hndFromAirport").val($("#selFromLoc :selected").text());
			$("#frmFlightSearch").find("#hndToAirport").val($("#selToLoc :selected").text());
			
			if(modifySegment.adultCount == 0){
				$("#frmFlightSearch").find("#selAdults").append("<option value='0'>0<option/>");
				$("#frmFlightSearch").find("#selAdults").val(modifySegment.adultCount).readonly(true);
			}else{
				$("#frmFlightSearch").find("#selAdults").val(modifySegment.adultCount).readonly(true);
			}
			$("#frmFlightSearch").find("#selChild").val(modifySegment.childCount).readonly(true);
			UI_FlightSearch.selAdultsOnChange();
			$("#frmFlightSearch").find("#selInfants").val(modifySegment.infantsCount).readonly(true);			
			$("#departureDateBox").datepicker( "setDate" , new Date(stringDateArr[0], (stringDateArr[1] -1),stringDateArr[2]));			
			$("#chkReturnTrip").attr("checked", false);
			UI_FlightSearch.chkReturnTripClick();
			$("#chkReturnTrip").disable();
			$("#resReturnDate").val("");
			$("#resReturnFlag").val(false);	
			$("#returnDate").val("");
			$("#resModifyPNR").val(modifySegment.PNR);
			$("#resGroupPNR").val(modifySegment.isGroupPNR);
			$("#resModifySegments").val($.toJSON(modifySegment.allResSegments));
			$("#resVersion").val(modifySegment.version);
			$("#resModifingFlightInfo").val($.toJSON(modifySegment.modifingFlightInfo));
			if (modifySegment.modifingFlightInfo.length > 0) {
				$("#resSearchSystem").val(modifySegment.modifingFlightInfo[0].system);
			}
			$("#resContactInfoJson").val($("#contactInfoJson").val());
			$("#resModifySegmentRefNos").val(modifySegment.segmentRefNos);
			$("#resPaxJson").val(modifySegment.paxJson);
			$("#resOldFareID").val(modifySegment.fareDetails.fareId);
			$("#resOldFareType").val(modifySegment.fareDetails.fareRuleID);
			$("#resOldFareAmount").val(modifySegment.fareDetails.amount);
			$("#resOldFareCarrierCode").val(modifySegment.fareDetails.carrierCode);
			$("#resFirstDeparture").val(modifySegment.firstDepature);	
			$("#resLastArrival").val(modifySegment.lastArrival);
			$("#resIsModifySegment").val(true);
			$("#hasInsurance").val(modifySegment.hasInsurance);
			if(resFlexiAlerts != null && modifySegment.bookingStatus != 'ONHOLD') {
				
				$("#resFlexiAlerts").val($.toJSON(resFlexiAlerts));	
				for(var i=0;i<modifySegment.modifingFlightInfo.length;i++){
					UI_FlightSearch.setModifyFlexiBooking(modifySegment.modifingFlightInfo[i].flightSegmentRefNumber,resFlexiAlerts);
					
				}
				
				var modifyOndQuoteFlexi = {};
				for(var i=0;i<modifySegment.allResSegments.length;i++){
					var segObj = modifySegment.allResSegments[i];
					var ondSequence = segObj.journeySequence - 1;
					var fltRefNum = _getFlightRefNumber(segObj.flightSegmentRefNumber);
					var isModifyHasFlexi = UI_FlightSearch.isBookingFlightSegmentHasFlexi(segObj.bookingFlightSegmentRefNumber, resFlexiAlerts);
					modifyOndQuoteFlexi[ondSequence] = !isModifyHasFlexi;
					if(_isModifyingFlightSegment(fltRefNum, segObj.pnrSegID, modifySegment.modifingFlightInfo)){	
						UI_FlightSearch.modifyFlexiBooking != isModifyHasFlexi;
					}
				}
				$("#resOndQuoteFlexi").val($.toJSON(modifyOndQuoteFlexi));	
				
				if(UI_FlightSearch.modifyFlexiBooking){
					$("#resModFlexiSelected").val(true);
				} else {
					$("#resModFlexiSelected").val(false);
				}

//				if(UI_FlightSearch.modifyFlexiBooking){
//					$('#resQuoteInboundFlexi').val(false);
//					$('#resQuoteOutboundFlexi').val(false);
//					$('#resFlexiSelected').val(true);
//				} else{
//					$('#resQuoteInboundFlexi').val(true);
//					$('#resQuoteOutboundFlexi').val(true);			
//					$('#resFlexiSelected').val(false);
//				}
				
			}
			//segment modification IBE
			if(allowModifyDate == false){
				$("#departureDateBox").datepicker('disable');				
				$("#modifyByDate").val("false");
			}else{
				$("#departureDateBox").datepicker('enable');
				$("#modifyByDate").val("true");
			}
			if(allowModifyRoute == false){
				$("#selFromLoc").disable();
				$("#selToLoc").disable();				
				$("#modifyByRoute").val("false");				
			}else{
				$("#selFromLoc").enable();
				$("#selToLoc").enable();
				$("#modifyByRoute").val("true");
			}
			if (modifySegment.classOfService != ""){
				$("#selCOS").val(modifySegment.classOfService);
				UI_FlightSearch.existingClassOfService = modifySegment.classOfService; 
			}
			$("#totalSegmentCount").val(UI_commonSystem.totalSegCount);
		}
	}
	
	UI_FlightSearch.setModifyFlexiBooking = function(flightSegmentRefNumber,objAltDt) {		
		UI_FlightSearch.modifyFlexiBooking = UI_FlightSearch.isBookingFlightSegmentHasFlexi(flightSegmentRefNumber,objAltDt);
	}
	
	UI_FlightSearch.isBookingFlightSegmentHasFlexi = function(flightSegmentRefNumber,objAltDt) {		
		var hasFlexi = false;
		if (objAltDt != null && objAltDt.length > 0) {
			for (var i = 0; i < objAltDt.length; i++) {
				if (flightSegmentRefNumber == objAltDt[i].flightSegmantRefNumber) {
					for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++){
						// Display either number of modifications cancellations are > 0
						if(objAltDt[i].alertTO[a].alertId == 1){//Modification
							var availModifications = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
							if(availModifications > 0){
								hasFlexi = true;
								break;
							}
						} else if(objAltDt[i].alertTO[a].alertId == 2){//Cancellation
							var availCancellations = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
							if(availCancellations > 0){ 
								hasFlexi = true;
								break;
							}
						}
					}
					break;
				}
			}
		}
		
		return hasFlexi;
	}
	
	/**
	 * Set Add Ground segment search data
	 */	
	UI_FlightSearch.setAddGroundSegmentSearchData = function(addingBusSegment, resFlexiAlerts,allowModifyDate,allowModifyRoute) {
		if (addingBusSegment != null) {	
			UI_FlightSearch.isAddGroundSegment = true;
			var stringDateArr = (addingBusSegment.depDate).split("/");
			$("#frmFlightSearch").find("#selAdults").val(addingBusSegment.adultCount).readonly(true);
			$("#frmFlightSearch").find("#selChild").val(addingBusSegment.childCount).readonly(true);
			UI_FlightSearch.selAdultsOnChange();
			$("#frmFlightSearch").find("#selInfants").val(addingBusSegment.infantsCount).readonly(true);			
			$("#departureDateBox").datepicker( "setDate" , new Date(stringDateArr[0], (stringDateArr[1] -1),stringDateArr[2]));			
			$("#chkReturnTrip").attr("checked", false);
			UI_FlightSearch.chkReturnTripClick();
			$("#chkReturnTrip").disable();
			$("#resReturnDate").val("");
			$("#resReturnFlag").val(false);	
			$("#returnDate").val("");
			$("#resModifyPNR").val(addingBusSegment.PNR);
			$("#resGroupPNR").val(addingBusSegment.isGroupPNR);
			$("#resModifySegments").val($.toJSON(addingBusSegment.allResSegments));
			$("#resVersion").val(addingBusSegment.version);
			$("#resModifingFlightInfo").val($.toJSON(addingBusSegment.modifingFlightInfo));
			if (addingBusSegment.modifingFlightInfo.length > 0) {
				$("#resSearchSystem").val(addingBusSegment.modifingFlightInfo[0].system);
			}
			$("#resAddGroundSegment").val(true);	
			
			$("#resContactInfoJson").val($("#contactInfoJson").val());
			$("#resModifySegmentRefNos").val(addingBusSegment.segmentRefNos);
			$("#resPaxJson").val(addingBusSegment.paxJson);
			$("#resFirstDeparture").val(addingBusSegment.firstDepature);	
			$("#resLastArrival").val(addingBusSegment.lastArrival);
			$("#hasInsurance").val(addingBusSegment.hasInsurance);
			if(resFlexiAlerts != null) {
				$("#resFlexiAlerts").val($.toJSON(resFlexiAlerts));
			}
			
			if (addingBusSegment.classOfService != ""){
				$("#selCOS").val(addingBusSegment.classOfService);
			}
		}
	}
	
	
	UI_FlightSearch.resetSearhData = function() {
		UI_FlightSearch.isModifySegment = false;
		UI_FlightSearch.resetPreviousDataOnDropDowns();
		UI_FlightSearch.resetModifySearhData();
		$("#selToLoc option:first").attr("selected",true);
		$("#selFromLoc option:first").attr("selected",true);
		$("#chkReturnTrip").enable();
		$("#chkReturnTrip").attr("checked", true);
		$("#selAdults").val(1);
		$("#selChild").val(0);		
		$("#selAdults").readonly(false);
		$("#selChild").readonly(false);
		$("#selInfants").readonly(false);
		$("#resSearchSystem").val("");
		UI_FlightSearch.selAdultsOnChange();		
		UI_FlightSearch.defaultDataLoad();
	}
	
	UI_FlightSearch.resetModifySearhData = function() {
		$("#returnDate").val("");
		$("#resModifyPNR").val("");
		$("#resGroupPNR").val(false);
		$("#resModifySegments").val("");
		$("#resVersion").val(0);
		$("#resModifingFlightInfo").val("");
		$("#resContactInfoJson").val("");
		$("#resModifySegmentRefNos").val("");
		$("#resPaxJson").val("");
		$("#resOldFareID").val("");
		$("#resOldFareType").val("");
		$("#resOldFareAmount").val("");
		$("#resOldFareCarrierCode").val("");
		$("#resFirstDeparture").val("");	
		$("#resLastArrival").val("");
		$("#resIsModifySegment").val(false);
		$("#hasInsurance").val(false);
		$("#resModifySegment").val(false);
		$("#resPNR").val("");
	}
	/**
	 * Used to replace previous Airport drop down data
	 * After setting the previous values are cleared.
	 */
	UI_FlightSearch.resetPreviousDataOnDropDowns = function (){
		if(UI_FlightSearch.previousAirportData.isOnADataRefresh){
			if(UI_FlightSearch.previousAirportData.fromAirportsPrevious != null){
				$("#selFromLoc, #selFromLoc_dummy").children().remove();
				$("#selFromLoc, #selFromLoc_dummy").fillDropDown({dataArray:UI_FlightSearch.previousAirportData.fromAirportsPrevious, 
					keyIndex:0, valueIndex:1, firstEmpty:false});
			}
			if(UI_FlightSearch.previousAirportData.toAirportsPrevious != null){
				$("#selToLoc, #selToLoc_dummy").children().remove();
				$("#selToLoc, #selToLoc_dummy").fillDropDown({dataArray:UI_FlightSearch.previousAirportData.toAirportsPrevious,
					keyIndex:0, valueIndex:1, firstEmpty:false});		
			}			
		}
	}
	/***
	 * Use fromAirportsPrevious and toAirportsPrevious to hold previous airports if airport drop down
	 * data changes and we want to restore the previous values in case of button click.
	 */
	UI_FlightSearch.previousAirportData = {
		fromAirportsPrevious : null,toAirportsPrevious : null,
		fromAirportGroundOnlyData: null, toAirportGroundOnlyData:null,
		isOnADataRefresh : false,
		clear : function (){
			this.fromAirportsPrevious = null;
			this.toAirportsPrevious = null;
			this.isOnADataRefresh = false;
		},
		isEmpty : function (){
			if(this.fromAirportsPrevious == null){
				return true;
			}
			return false;
		}
	}
		
	/**
	 * Data lists to be used when dynamic ond lists are enabled will be populated 
	 * by this method
	 */
	UI_FlightSearch.initDynamicOndList = function() {

		// Populate OnD data structure only for ond's which has current
		// operating carrier

		var defaultCarrier = UI_Top.holder().GLOBALS.defaultCarrierCode;
		var lang = UI_Top.holder().GLOBALS.sysLanguage;

		if ($("#locale").val() != null && $("#locale").val() != "") {
			lang = $("#locale").val();
		}

		for ( var originIndex in origins) {

			var tempDestArray = [];
			for ( var destIndex in origins[originIndex]) {
				// check whether operating carrier exists
				var curDest = origins[originIndex][destIndex];
				
				if(curDest[0] != null){							
					var currentSize = tempDestArray.length;
					tempDestArray[currentSize] = [];
					tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
					tempDestArray[currentSize][1] = (airports[curDest[0]][lang] != null ? airports[curDest[0]][lang]
							: airports[curDest[0]][UI_Top.holder().GLOBALS.sysLanguage]);
					tempDestArray[currentSize][2] = currencies[curDest[2]]['code'];
				}
				
				/*var opCarriers = curDest[3].split(",");
				for ( var carrier in opCarriers) {
					if (opCarriers[carrier] == defaultCarrier) {
						var currentSize = tempDestArray.length;
						tempDestArray[currentSize] = [];
						tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
						tempDestArray[currentSize][1] = (airports[curDest[0]][lang] != null ? airports[curDest[0]][lang]
								: airports[curDest[0]][UI_Top.holder().GLOBALS.sysLanguage]);
						tempDestArray[currentSize][2] = currencies[curDest[2]]['code'];
						break;
					}
				}*/
			}

			// Add entry if at least one destination is there to final ond map
			if (tempDestArray.length > 0) {
		
				tempDestArray.sort(UI_FlightSearch.customSort);
				UI_FlightSearch.ondDataMap[airports[originIndex]['code']] = tempDestArray;

				var currentOriginSize = UI_FlightSearch.originList.length;
				UI_FlightSearch.originList[currentOriginSize] = [];
				UI_FlightSearch.originList[currentOriginSize][0] = airports[originIndex]['code'];
				UI_FlightSearch.originList[currentOriginSize][1] = (airports[originIndex][lang] != null ? airports[originIndex][lang]
						: airports[originIndex][UI_Top.holder().GLOBALS.sysLanguage]);

				if (UI_FlightSearch.hubDesc == ""
						&& UI_FlightSearch.hub == airports[originIndex]['code']) {
					UI_FlightSearch.hubDesc = (airports[originIndex][lang] != null ? airports[originIndex][lang]
							: airports[originIndex][UI_Top.holder().GLOBALS.sysLanguage]);
				}
				
				UI_FlightSearch.originList.sort(UI_FlightSearch.customSort);

			}
			
			
		}
		
		for ( var i = 0 ; i < currencies.length ; i++) {
				
			UI_FlightSearch.currencyList[i] = [];
			UI_FlightSearch.currencyList[i][0] = currencies[i]['code'];
			UI_FlightSearch.currencyList[i][1] = (currencies[i][lang] != null ? currencies[i][lang]
					: currencies[i][UI_Top.holder().GLOBALS.sysLanguage]);
		
		}
		
		UI_FlightSearch.currencyList.sort(UI_FlightSearch.customSort);
			
	}
	
	/**
	 * Custom sort to sort 2 dimensional on index 1
	 */
	
	UI_FlightSearch.customSort = function(a,b) {

		a = a[1];
		b = b[1];
		return a == b ? 0 : (a < b ? -1 : 1)

	}
	
	//TransitTime 
	function chkStopOverAdd(){
		$("#trCheckStopOver").slideDown();
		UI_FlightSearch.showStopOver();
	}
	
	UI_FlightSearch.showStopOver = function(){
		$(".stopOverTimeInner").empty();
		$.each(UI_FlightSearch.hubTimeDetailJSON, function(i,j){
			var input = $("<div><span style='display:block;float:left;width:60px;vertical-align:bottom' ><br/><label name='lblHubCode' id='lblHubCode_"+i+"'>" + getHubName(j.hubCode) + "</label></span> <span style='display:block;float:left;width:88px'><label id='lblOutBoundDays'>Outbound Days</label><input type='text' maxlength='2' class='aa-input' style='width:60px' id='outboundStopOver_"+i+"' name='outboundStopOver' value='"+j.outboundStopOver+"'/> </span> " +
					"<span style='display:block;float:left;width:80px'><label id='lblInBoundDays'>Inbound Days</label><input type='text' maxlength='2' class='aa-input' style='width:60px' id='inboundStopOver_"+i+"' name='inboundStopOver' value='"+j.inboundStopOver+"'/> </span><div class='clear'></div></div>");
			$(".stopOverTimeInner").append(input);
		});
		
		$("input[name='outboundStopOver']").on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		});

		$("input[name='inboundStopOver']").on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		});

	};
	
	UI_FlightSearch.setDummyService = function(){
		$("#selCOS_dummy").empty();
		if(UI_FlightSearch.originList.length > 0){
			$("#selFromLoc_dummy").empty();
			$("#selToLoc_dummy").empty();
			var fromAirpotList = UI_FlightSearch.setAirportData(UI_FlightSearch.setAirportListHead('from') , UI_FlightSearch.originList);
			var toAirpotList = UI_FlightSearch.setAirportData(UI_FlightSearch.setAirportListHead('to') , UI_FlightSearch.originList);
			$("#selFromLoc_dummy").fillDropDown({dataArray:fromAirpotList, keyIndex:0, valueIndex:1, firstEmpty:false});
			$("#selToLoc_dummy").fillDropDown({dataArray:toAirpotList, keyIndex:0, valueIndex:1, firstEmpty:false});
		}
		$("#selCOS>option").each(function(){
			$("#selCOS_dummy").append("<option value='"+this.value+"'>"+this.innerHTML+"</option>");
		});
		var noMonth = globalConfig.noOfMonthsinCalendaraPopup;
		$("#departureDate_dummy").datepicker("destroy");
		$("#departureDate_dummy").datepicker({
			regional:SYS_IBECommonParam.locale,
			numberOfMonths: noMonth,
			dateFormat: "d M yy",
			altFormat: "dd/mm/yy",
			altField: "#departureDate",
			minDate: +0,
			showOn: 'both',
			buttonImage: globalConfig.calendaImagePath,
			buttonImageOnly: true,
			showButtonPanel: true,
		});
		$("#selFromLoc_dummy").val($("#selFromLoc").val());
		$("#selToLoc_dummy").val($("#selToLoc").val());
		$("#departureDate_dummy").val($("#departureDateBox").val());
		$("#selCOS_dummy").val($("#selCOS").val());
		//bind Event to set data to search
		
		$("#selFromLoc_dummy").on("change", function(){
			$("#selFromLoc").val(this.value);
			$("#hndFromAirport").val(this.options[this.selectedIndex].innerHTML);
		});
		$("#selToLoc_dummy").on("change", function(){
			$("#selToLoc").val(this.value);
			$("#hndToAirport").val(this.options[this.selectedIndex].innerHTML);
		})
		$("#departureDate_dummy").on("change", function(){
			$("#departureDateBox").val(this.value);
		})
		$("#selCOS_dummy").on("change", function(){
			$("#selCOS").val(this.value);
		})
		
		
	}
		
})(UI_FlightSearch);