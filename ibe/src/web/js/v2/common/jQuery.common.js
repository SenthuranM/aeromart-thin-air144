/*
 *********************************************************
 Description		: Common prototype routings
 Author			: Rilwan A. Latiff
 Version			: 1.2
 Created         : 1st June 2005
 Last Modified	: 25th August 2009
 ---------------------------------------------------------
 Ver 1.0
 *********************************************************
 */

/* Disable Any Control */
$.fn.disable = function() {$(this).attr('disabled', true);}

/* Enable Any control */
$.fn.enable = function() {$(this).attr('disabled', false);}

/* Read only Any Control */
$.fn.readOnly = function(blnStatus) {$(this).attr('readOnly', blnStatus);}

/* Enable Disable */
$.fn.disableEnable = function(blnStatus) {
    if (blnStatus){$(this).disable();}else{$(this).enable();}
}

/* Reset Form Data */
$.fn.extend({reset: function(){
    return this.each(function() {
        try{
            $(this).is('form') && this.reset();
        }catch(ex){}
    });
}
});



/* Set Tab Index Any control */
$.fn.setTabIndex = function(intIndex) {$(this).attr('tabindex', intIndex);}

/* Set Form Action */
$.fn.action = function(strFileName) {$(this).attr('action', strFileName);}

/* Alpha Numeric Only Allowed */
$.fn.alphaNumeric = function(p) {
    p = $.extend({paste: true}, p);
    $(this).keypress(function (e){
        if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
            var c = String.fromCharCode(e.which);
            if (!isAlphaNumeric(c)){e.preventDefault();};
            if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
        };
    });
};

/* Alpha Only allowed */
$.fn.alpha = function(p) {
    p = $.extend({paste: true}, p);
    $(this).keypress(function (e){
        if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
            var c = String.fromCharCode(e.which);
            if (!isAlpha(c)){e.preventDefault();};
            if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
        };
    });
};

/* Numeric Only Allowed */
$.fn.numeric = function(p) {
    $(this).keypress(function (e){
        if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
            var c = String.fromCharCode(e.which);
            if (!isNumeric($(this).val() + c)){e.preventDefault();};
        };
    });
};

/* Alpha White Space allowed */
$.fn.alphaWhiteSpace = function(p) {
    p = $.extend({paste: true}, p);
    $(this).keypress(function (e){
        if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
            var c = String.fromCharCode(e.which);
            if (!isAlphaWhiteSpace(c)){e.preventDefault();};
            if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
        };
    });
};

/* Alpha Numeric White space Allowed */
$.fn.alphaNumericWhiteSpace = function(p) {
    p = $.extend({paste: true}, p);
    $(this).keypress(function (e){
        if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
            var c = String.fromCharCode(e.which);
            if (!isAlphaNumericWhiteSpace(c)){e.preventDefault();};
            if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
        };
    });
};

$.fn.numbericHiphen = function(p) {
    $(this).keypress(function (e){
        if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
            var c = String.fromCharCode(e.which);
            var splitArray = $(this).val().split("-")
            if (!isNumeric(splitArray[splitArray.length-1] + c)&&c!="-"){e.preventDefault();};
        };
    });
};

/* get the HTML Popup  Properties  */

$.fn.openMyPopUp =  function (p){
    var el = $(this)
    var winHeight = $(window).height();
    var winWidth = $(window).width();
    var config = {
        position:"fixed",
        zindex: 1001,
        width:300,
        height: 300,
        topPoint: 0,
        leftPoint: 0,
        headerHTML:"Header",
        bodyHTML:"Body",
        footerHTML:""
    }
    p = $.extend(config, p);
    $("#newpage-mask").remove();
    $("body").append('<div id="newpage-mask"></div>');
    $("#newpage-mask").css({
        position: 'absolute',
        zIndex: 1000,
        top: '0px',
        left: '0px',
        width: '100%',
        height: $(document).height(),
        backgroundColor:'#000',
        opacity:0.8
    });
    $(this).css({
        position:p.position,
        zIndex: p.zindex,
        top: (p.topPoint == 0) ? (winHeight / 2 ) - (p.height / 2) : p.topPoint,
        left: (p.leftPoint == 0) ? (winWidth / 2 ) - (p.width / 2) : p.leftPoint,
        width:p.width
    });
    $(this).find("#bodyitems").css("height",p.height - 45);
    $(this).find(".headeritems").html(p.headerHTML);
    $(this).find("#bodyitems").html(p.bodyHTML);
    $(this).find("#footeritems").html(p.footerHTML);
    $(this).show();
}
$.fn.closeMyPopUp = function(){
    $("#newpage-mask").remove();
    $(this).hide()
}


/* get the Popup Window Properties  */
$.fn.getPopWindowProp =  function (intHeigh, intWidth){
    var intTop 	= (window.screen.height - intHeigh) / 2;
    var intLeft = (window.screen.width -  intWidth) / 2;
    var strScroll = "no";
    if (arguments.length > 2){
        if (arguments[2] == true){
            strScroll = "yes";
        }
    }
    var strProp = 'toolbar=no,' +
        'location=no,' +
        'status=no,' +
        'menubar=no,' +
        'scrollbars=' + strScroll + ','+
        'width=' + intWidth + ',' +
        'height=' + intHeigh + ',' +
        'resizable=no,' +
        'top=' + intTop +',' +
        'left=' + intLeft
    return strProp
}

/**
 * Fill First Item Of Select Box
 */
$.fn.fillFirstOption = function(label) {
    this.append("<option value=''>"+label + "</option>");
};

/* spinner object  */

$.fn.spinnerisa =  function (o){
    var config = {
        select : false,
        selectedMeal: null,
        selectAction : function (event,val){

        },
        increase : function (event,val) {

        },
        decrease : function (event,val) {

        }
    }
    if (o) o = $.extend(config, o);
    add=function(val,id){
        var i = parseInt(val.parent().parent().find(".value").html());
        i++;
        val.parent().parent().find(".value").html(i+"x");
        return {objid:id,value:i};
    }
    reduse=function(val,id){
        var i = parseInt(val.parent().parent().find(".value").html());
        if (!isNaN(i) && i!=0){
            i--;
            if (i==0)
                val.parent().parent().find(".value").html(i);
            else
                val.parent().parent().find(".value").html(i+"x");
        }
        return {objid:id,value:i};
    }
    return this.each(function(index){
        if (o.select){
            var id = this.id;
            var str = '<table border="0" id="'+id+'" class="'+this.className+'"><tr>'+
                '<td><div class="add"></div></td>'+
                '<td><div class="reduse"></div></td>'+
                '<td><label class="value">'+this.innerHTML+'</label></td>'+
                '</tr></table>';
            $(this).parent().html(str);
            $("#"+id).find(".add").unbind("click").bind("click",function(e){
                var i = add($(this),id);
                o.increase(e, i);
            });
            $("#"+id).find(".reduse").unbind("click").bind("click",function(e){
                var j =reduse($(this),id)
                o.decrease(e, j);
            });
        }else{
            var id = this.id, selectedRadio = "";
            if (o.selectedMeal!=undefined && o.selectedMeal != null && o.selectedMeal.length > 0){
                var tselMeal = "spin_"+ o.selectedMeal[0].mealCategoryID+"_"+ o.selectedMeal[0].mealIndex;
                if (tselMeal===id){
                    selectedRadio = "checked='checked'";
                }
            }
            var str = '<input type="radio" id="'+id+'" name="selectThis" class="radSelect" style="margin:0 10px" '+selectedRadio+'/>';
            $(this).parent().html(str);
            $("#"+id).unbind("click").bind("click",function(e){
                o.selectAction(e, id);
            });
        }
    });

};

(function($){
    $.fn.filterData = function(attr, startsWith){
        var filterFn = function (){
            var regex = new RegExp(startsWith+'*');
            return regex.test($(this).attr(attr));
        }
        var data = {};
        this.filter(filterFn).each(function(i, itm){
            var jItm = $(itm);
            if(jItm.attr('type') != 'radio' || jItm.attr('checked') ) {
                data[$(itm).attr('name')] = $(itm).val();
            }
        });
        return data;
    }
})(jQuery);

$.fn.dateToConvert =  function (o){
    return this.each(function(index){

    });
}