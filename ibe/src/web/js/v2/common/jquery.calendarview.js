/**
* Author : Baladewa Welathanthri
 * 
 */
(function($) {
	var func = null;
	var dayWidth = null;
	var dayGap = null;
	var loading = new Image();
	var calID = null;
	var dat = null;
	this.departureLowerFare = null;
	this.arrivalLowerFare = null
	this.newdayFare = null;
	
	 $.fn.calendarview = function(settings) {
		 var jsDays = UI_AvailabilitySearch.weekdays || ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
		 var jsMonths = UI_AvailabilitySearch.months || ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	     var config = {
		 noOfDays : 7,
		 gapOfDays : 3,
		 dispalyItems : {arrivalTime:false,departureTime:false,radio:true,flightNum:false,stopOverCount:true, displayNonstop:true},
		 loadingImage : "",
		 displayHeadeFormat : "ddd dd MMM",
		 topLabel : "",
		 travelType : "dep",
		 currencyCode : '',
		 data : {}, // json obj to send
		 getDataForAllDays : function(){			 
		 },
		 selectFlight:function(event, val){
			 
		 }
	     };

	    if (settings)
	    	settings = $.extend(config, settings);
	    
	    
	    var borderWidth = 2;
		loading.src = settings.loadingImage;
	    return this.each(function() {
	    	func = settings;
	    	var	el = $(this);
	    	var calWidth = parseInt(el.css("width"));
	    	var scrollSpace = 15;
	    	var bordegap = settings.gapOfDays + borderWidth;
	    	var vewDatesInCal = (settings.noOfDays > 7) ? 7 : settings.noOfDays;
	    	//dayWidth = Math.round((calWidth - ((bordegap * settings.noOfDays)+borderWidth))/settings.noOfDays);
	    	dayWidth = Math.round((calWidth - ((bordegap * settings.noOfDays)+borderWidth))/vewDatesInCal);
	    	//calWidth = (dayWidth * settings.noOfDays)+(bordegap * settings.noOfDays) + borderWidth;
	    	calWidth = (dayWidth * vewDatesInCal)+(bordegap * vewDatesInCal) + borderWidth;
	    	//calWidth = (dayWidth * 7)+(bordegap * settings.noOfDays) + borderWidth;
	    	dayGap = "style='margin:0px 0px "+settings.gapOfDays+"px "+settings.gapOfDays+"px'"
	    	headerdayGap = "style='margin:"+settings.gapOfDays+"px 0px 0px "+settings.gapOfDays+"px'"
	    	
	    	
	    	//var beforText = '<h3 class="label">'+settings.topLabel+'</h3><div class="nav-cal"><div class="sub-div cal_previous"><a href="javascript:void(0)">&lt;</a></div><div class="sub-div cal_next"><a href="javascript:void(0)">&gt;</a></div><div class="sub-div-total cal_next"><a href="javascript:void(0)">&gt;&gt;</a></div></div>';
	    	    	
	    	var beforText = '<h3 class="label">'+settings.topLabel+'</h3><div class="nav-cal">';
			beforText +='<div class="leftNavigation">';
			
			if (calDisplayConfig.layout.additionalNavigationEnabled){
				beforText +='<div class="sub-div-additional cal_previous"><a href="javascript:void(0)" style="padding:0 3px;display: none;" title="Previous Week">&lt;&lt;</a></div>';
				$('.flightCalendar').css("top","0px");
			}
			
			beforText +='<div class="sub-div cal_previous"><a href="javascript:void(0)" title="Previous Day">&lt;</a></div>'+
							'</div><div class="rightNavigation">';
				
			if (calDisplayConfig.layout.additionalNavigationEnabled){
				beforText +='<div class="sub-div-additional cal_next"><a href="javascript:void(0)" style="padding:0 3px;display: none;" title="Next Week">&gt;&gt;</a></div>';
			}
			
				beforText +='<div class="sub-div cal_next"><a href="javascript:void(0)" title="Next Day">&gt;</a></div>';
			beforText +='</div></div>';
	    	$(this).before(beforText);

	    	
	    	//$(this).after('<div class="nav-cal"><div class="sub-div cal_previous"><a href="javascript:void(0)">&lt;</a></div><div class="sub-div cal_next"><a href="javascript:void(0)">&gt;</a></div></div><div class="clear"></div>');
	    	var htmlToAdd ='';
	    	htmlToAdd += '<div style="width:'+(calWidth)+'px" class="cal-header"><ul class="cal-header-ul" style="width:'+Number(calWidth+20)+'px"></ul></div>';
	    	htmlToAdd += '<div style="width:'+Number(calWidth + scrollSpace)+'px" class="c-scroller"><ul class="calendar-ul" style="width:'+Number(calWidth+20)+'px">';
	    	//htmlToAdd += addChild (1);
		    	 for (var i = 0; i<vewDatesInCal; i++){
			    		//htmlToAdd += addChild();
			     };
	    	htmlToAdd +='</ul></div>';
	    	$(this).html(htmlToAdd);
	    	$(this).css("width",calWidth)
	    	var dispalyFlightCount = 2;
	    	if (settings.noOfDays>vewDatesInCal && UI_AvailabilitySearch.calendarType != "X")
	    		createDatesInClander21(settings.travelType, settings.data, null, settings.noOfDays,"#"+this.id+" .calendar-ul", "#"+this.id+" .cal-header-ul");
	    	else
	    		createDatesInClander(settings.travelType, settings.data, null, settings.noOfDays,"#"+this.id+" .calendar-ul", "#"+this.id+" .cal-header-ul");

	    	function createDatesInClander(type, dataArray, current, nod, o,oh){
	   		 var htmlToAdd = "";
	   		 var hraderHtmlToAdd = "";
	   		 if (dataArray == undefined || dataArray == undefined || dataArray == ""){
	   			 var htmlToAdd = "No flight data found for this Route";
	   			 $(o).html(htmlToAdd);
	   			 
	   			 if (type == 'dep'){
	   				 $("#fCalOut").next(".sub-div").hide();
	   			 	 $("#fCalOut").prev(".sub-div").hide();
	   			 }
	   		 	 if (type == 'arr'){
	   		 		$("#fCalIn").next(".sub-div").hide();
   			 	    $("#fCalIn").prev(".sub-div").hide();
	   		 	 }
	   		 }else{
		   		 if (nod == 1){
		   			 //$.each(dataArray, function (i , got){
		   					 htmlToAdd = careteOneDay(dataArray,null, null);
		   					 hraderHtmlToAdd = careteOneDayHeader(dataArray,null, null);
		   			 //});
		   		 }else{
		   			 if (type == 'dep'){
		   				 for (var i = 0; i<nod; i++){
		   					 htmlToAdd += addChild(dataArray[i],'dep',nod);
		   					 hraderHtmlToAdd += addHeaderChild(dataArray[i],'dep',nod)
		   			     };
		   			 }else{
		   				 for (var i = 0; i<nod; i++){
		   					 htmlToAdd += addChild(dataArray[i],'arr',nod);
		   					 hraderHtmlToAdd += addHeaderChild(dataArray[i],'arr',nod)
		   			     };
		   			 }
		   		 }
		   	     $(o).html(htmlToAdd);
		   	     $(oh).html(hraderHtmlToAdd);
	   		 }
	   		setLowasetFare(null,type)
	   		var sHeagth = 0;
	   		if ($(".cal-base").css("height") == ""){ sHeagth = 1}else {
	   			marginHeight = ($(".cal-base").css("margin") == "")? 0 : parseInt($(".cal-base").css("margin"));
	   			paddingHeight = ($(".cal-base").parent().css("padding") == "") ? 0 : parseInt($(".cal-base").parent().css("padding"));
	   				sHeagth = parseInt($(".cal-base").css("height"))+ marginHeight + paddingHeight;
	   			}
	   		if (type == 'dep'){
	   			if (calDisplayConfig.layout.moreThanTwoFlight == "SCROLL"){
	   				var total = $("#fCalOut").find(".flight-body").children();
		   			var nok = (total.length>7)? dispalyFlightCount: 1;
		   			$("#fCalOut").find(".c-scroller").css("height", Number( (nok*sHeagth) + 8 ));
		   		}
		   	//Set Navigation pos
		   		//scrollerHeight
	   		
	    	}else{
	    		if (calDisplayConfig.layout.moreThanTwoFlight == "SCROLL"){
		    		var total = $("#fCalIn").find(".flight-body").children();
			   		var nok = (total.length>7)? dispalyFlightCount: 1;
			   		$("#fCalIn").find(".c-scroller").css("height",Number(nok*sHeagth + 8));
			   		
			   		if (!$.browser.msie && $("body").css("direction") == "rtl")
			   			$(".c-scroller").css({"position":"relative", "left":"17px"});
	    		}
	    	}
	    	};
	   	 
	   	function createDatesInClander21(type, dataArray, current, nod, o,oh){
		   	 var htmlToAdd = "";
	   		 var hraderHtmlToAdd = "";
	   		if (dataArray == undefined || dataArray == undefined || dataArray == ""){
	   			 var htmlToAdd = "No flight data found for this Route";
	   			 $(o).html(htmlToAdd);
	   			 
	   			 if (type == 'dep'){
	   				 $("#fCalOut").next(".sub-div").hide();
	   			 	 $("#fCalOut").prev(".sub-div").hide();
	   			 }
	   		 	 if (type == 'arr'){
	   		 		$("#fCalIn").next(".sub-div").hide();
  			 	    $("#fCalIn").prev(".sub-div").hide();
	   		 	 }
	   		 }else{
		   		 if (nod == 1){
		   			 //$.each(dataArray, function (i , got){
	   					 htmlToAdd = careteOneDay(dataArray,null, null);
	   					 //hraderHtmlToAdd = careteOneDayHeader(dataArray,null, null);
		   			 //});
		   		 }else{
		   			 if (type == 'dep'){
		   				// for (var i = 0; i<nod; i++){
		   					 htmlToAdd += createDatesforWeek(dataArray,'dep',nod);
		   					 hraderHtmlToAdd += createWeekdays()
		   			    // };
		   			 }else{
		   				// for (var i = 0; i<nod; i++){
		   					 htmlToAdd += createDatesforWeek(dataArray,'arr',nod);
		   					 hraderHtmlToAdd += createWeekdays()
		   			    // };
		   			 }
		   		 }
		   	     $(o).html(htmlToAdd);
		   	     $(oh).html(hraderHtmlToAdd);
	   		 }
	   		/*var sHeagth = 0;
	   		if ($(".cal-base").css("height") == "NaN"){ sHeagth = 1}else {sHeagth = parseInt($(".cal-base").css("height"))}
	   		var total = $("#"+calID).find(".flight-body").children();
	   		var nok = (total.length>7)? dispalyFlightCount: 1;
	   		$("#"+calID).find(".c-scroller").css("height",Number(nok*sHeagth + 20));
	   	    if (!$.browser.msie && $("body").css("direction") == "rtl")
	   	    	$("#"+calID).find(".c-scroller").css({"position":"relative", "left":"15px"});*/
	   	};

	   	 
	   	function careteOneDayHeader(day,way,count,date){
	   		if (day != undefined && day !=null)
	   			var htmlToAdd ='<div>'+setDateFormat(day.departureDate)+'</div><span style="display:none">'+day.departureDate+'</span>';
	   		else
	   			var htmlToAdd ='<div>'+setDateFormat(date)+'</div><span style="display:none">'+date+'</span>';
	   		
	   		return htmlToAdd;
	   	};

	   	function setDateFormat(date){
	   		var temdate = date.split("/");
	   		var newDate = new Date(temdate[1]+"/"+temdate[0]+"/"+temdate[2]);
	   		
	   		if (settings.displayHeadeFormat = "ddd dd mmm")
	   			return jsDays[newDate.getDay()] + " " + newDate.getDate() + " " + jsMonths[newDate.getMonth()];
	   	};
	 	
	   	 
	   	 function careteOneDay(day,way,count){
	   		var htmlToAdd ='<div class="flight-body">';
	   		var lowerstAdded = false;
	   		//segment modification IBE
	   		var isModifyDate = getModifyDateStaus();
			 if (day != undefined && day.availableFlightInfo != null ){					
				if (day.availableFlightInfo.length != 0){
					//if (dispalyFlightCount < day.availableFlightInfo.length) dispalyFlightCount = day.availableFlightInfo.length 
					for(var i = 0; i<day.availableFlightInfo.length;i++){
						var segmentTootltip = '';
						var flightuniqeID = '';
						//var gradientID = (day.availableFlightInfo[i].priceGuidNo != undefined) ? "FareGridBorder0"+day.availableFlightInfo[i].priceGuidNo : "";
						var gradientID = "";
						var flightID = way + dateVal (dataformater(day.departureDate)).d;
						var seatAvai = day.availableFlightInfo[i].flightFareSummary.seatAvailable;
						var flightNumber = '';
						htmlToAdd +='<div id="fligt' + flightID + i +'" class="hasFlight">'; 
						if (seatAvai){
							if (day.availableFlightInfo[i].selected){
								if(!isModifyDate){
									if(day.departureDate == UI_AvailabilitySearch.departureDate){
										if (i == 0)
											htmlToAdd +='<a href="javascript:void(0)" class="selected"><div class="cal-base '+gradientID+' zeromargin"><div class="lowerstCon"></div>';
										else
											htmlToAdd +='<a href="javascript:void(0)" class="selected"><div class="cal-base '+gradientID+'"><div class="lowerstCon"></div>';
									}else{
										if (i == 0)
											htmlToAdd +='<div class="cal-base '+gradientID+' zeromargin"><div class="lowerstCon"></div>';
										else
											htmlToAdd +='<div class="cal-base '+gradientID+'"><div class="lowerstCon"></div>';
									}
								}else{
									if (i == 0)
										htmlToAdd +='<a href="javascript:void(0)" class="selected"><div class="cal-base '+gradientID+' zeromargin"><div class="lowerstCon"></div>';
									else
										htmlToAdd +='<a href="javascript:void(0)" class="selected"><div class="cal-base '+gradientID+'"><div class="lowerstCon"></div>';
								}
								//if (way == "dep")
									//departureLowerFare = day.availableFlightInfo[i].flightFareSummary.baseFareAmount;
									//departureLowerFare = day.availableFlightInfo[i].flightFareSummary.totalPrice;
								//else
									//arrivalLowerFare = day.availableFlightInfo[i].flightFareSummary.baseFareAmount;
									//arrivalLowerFare = day.availableFlightInfo[i].flightFareSummary.totalPrice;
							/*}else{
								if (count == null){
									if (way == "dep" && day.availableFlightInfo[i].flightFareSummary.totalPrice <= departureLowerFare && !lowerstAdded){
										htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+'"><div class="lowerstCon"><span class="lowerst">+ Lowest Fare</span></div>';
										$("#fCalOut").find(".lowerstCon").html("&nbsp;");
										lowerstAdded = true;
										departureLowerFare = day.availableFlightInfo[i].flightFareSummary.totalPrice;
									}else if(way == "arr" && day.availableFlightInfo[i].flightFareSummary.totalPrice <= arrivalLowerFare && !lowerstAdded){
										htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+'"><div class="lowerstCon"><span class="lowerst">+ Lowest Fare</span></div>';
										$("#fCalIn").find(".lowerstCon").html("&nbsp;");
										lowerstAdded = true;
										arrivalLowerFare = day.availableFlightInfo[i].flightFareSummary.totalPrice;
									}else{
										htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+'"><div class="lowerstCon">&nbsp;</div>';
									}
									
								}else{
									htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+'"><div class="lowerstCon">&nbsp;</div>';
								}
							}*/
							}else{
								if(!isModifyDate){
									if(day.departureDate == UI_AvailabilitySearch.departureDate){
										if (i == 0)
											htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+' zeromargin"><div class="lowerstCon">&nbsp;</div>';
										else
											htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+'"><div class="lowerstCon">&nbsp;</div>';
									}else{
										if (i == 0)
											htmlToAdd +='<div class="cal-base '+gradientID+' zeromargin"><div class="lowerstCon">&nbsp;</div>';
										else
											htmlToAdd +='<div class="cal-base '+gradientID+'"><div class="lowerstCon">&nbsp;</div>';
									}
								}else{
									if (i == 0)
										htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+' zeromargin"><div class="lowerstCon">&nbsp;</div>';
									else
										htmlToAdd +='<a href="javascript:void(0)"><div class="cal-base '+gradientID+'"><div class="lowerstCon">&nbsp;</div>';
								}
							}
						}else{
							if (i==0)
								htmlToAdd +='<div class="cal-base '+gradientID+' zeromargin"><div class="lowerstCon">&nbsp;</div>';
							else
								htmlToAdd +='<div class="cal-base '+gradientID+'"><div class="lowerstCon">&nbsp;</div>';
						}
						    // Add Base Currency
						    // TODO : Set Currency Correct way
						if (!seatAvai){
							htmlToAdd +='<div class="totalValue FlightFull">Flight Full</div>'+
							'<div class="totalValueSelected"><label class="currValueTotal" style="display:none">'+ day.availableFlightInfo[i].flightFareSummary.calDisplayAmout +'</label></div>';
						}else{
							if (settings.dispalyItems.radio == true){
								if (day.availableFlightInfo[i].selected)
									htmlToAdd +='<div class="selecRadio"><input type="radio" name="selectFlight'+way+'" id="rad'+flightuniqeID+'" checked="checked"/></div>';
								else
									htmlToAdd +='<div class="selecRadio"><input type="radio" name="selectFlight'+way+'" id="rad'+flightuniqeID+'"/></div>';
							}
//							var displayTotal = 0;
//							var displayTotalSelCurrensy = 0;
//							var currencyLabel = "";
//							if(calDisplayConfig.design.showTotalFare == false){
//								if (calDisplayConfig.design.fallsDecimals){
//									if (calDisplayConfig.design.selctedCurrency){
//										displayTotalSelCurrency = fallDecimals (0,day.availableFlightInfo[i].flightFareSummary.baseFareAmtInSelCurrency);
//										currencyLabel = UI_AvailabilitySearch.selectedCurrency;
//									}else{
//										displayTotalSelCurrency = fallDecimals (0,day.availableFlightInfo[i].flightFareSummary.baseFareAmount);
//										currencyLabel = UI_AvailabilitySearch.currencyCode;
//									}
//								}else{
//									if (calDisplayConfig.design.selctedCurrency){
//										displayTotalSelCurrency = day.availableFlightInfo[i].flightFareSummary.baseFareAmtInSelCurrency;
//										currencyLabel = UI_AvailabilitySearch.selectedCurrency;
//									}else{
//										displayTotalSelCurrency = day.availableFlightInfo[i].flightFareSummary.baseFareAmount;
//										currencyLabel = UI_AvailabilitySearch.currencyCode;
//									}
//								}	
//							}else{
//								if (calDisplayConfig.design.fallsDecimals){
//									if (calDisplayConfig.design.selctedCurrency){
//										displayTotalSelCurrency = fallDecimals (0,day.availableFlightInfo[i].flightFareSummary.totalPriceInSelCurrency);
//										currencyLabel = UI_AvailabilitySearch.selectedCurrency;
//									}else{
//										displayTotalSelCurrency = fallDecimals (0,day.availableFlightInfo[i].flightFareSummary.totalPrice);
//										currencyLabel = UI_AvailabilitySearch.currencyCode;
//									}
//								}else{
//									if (calDisplayConfig.design.selctedCurrency){
//										displayTotalSelCurrency = day.availableFlightInfo[i].flightFareSummary.totalPriceInSelCurrency;
//										currencyLabel = UI_AvailabilitySearch.selectedCurrency;
//									}else{
//										displayTotalSelCurrency = day.availableFlightInfo[i].flightFareSummary.totalPrice;
//										currencyLabel = UI_AvailabilitySearch.currencyCode;
//									}
//								}
//							}
//							htmlToAdd +='<span class="hidden" style="display:none! important">' +
//										'<label class="currValue">'+ day.availableFlightInfo[i].flightFareSummary.totalPrice +'</label>'+
//										'<label class="totalSelCurr">'+ day.availableFlightInfo[i].flightFareSummary.totalPriceInSelCurrency +'</label>'+
//										'<label class="farevalue">'+ day.availableFlightInfo[i].flightFareSummary.baseFareAmount +'</label>'+
//										'<label class="fareSelvalue">'+ day.availableFlightInfo[i].flightFareSummary.baseFareAmtInSelCurrency +'</label></span>';
							htmlToAdd +='<div class="totalValueSelected"><span>'+ settings.currencyCode +' </span><label class="currValueTotal">'+ day.availableFlightInfo[i].flightFareSummary.calDisplayAmout +'</label></div>';
								segmentTootltip += '<div id="seg'+flightID+i+'" style="display:none">';
							for (var j = 0 && day.availableFlightInfo[i].segment != null; j < day.availableFlightInfo[i].segments.length; j++){
								segmentTootltip += '<span>'+createRoot( day.availableFlightInfo[i].segments[j].segmentCode, j, day.availableFlightInfo[i].segments.length)+'</span>';
								if (j == day.availableFlightInfo[i].segments.length - 1)
									flightuniqeID += day.availableFlightInfo[i].segments[j].flightRefNumber;
								else
									flightuniqeID += day.availableFlightInfo[i].segments[j].flightRefNumber+":";
								
								// Search System
								if (j == (day.availableFlightInfo[i].segments.length-1)) {
									flightuniqeID = flightuniqeID + "!" + day.availableFlightInfo[i].segments[j].system;
								}
								if (settings.dispalyItems.departureTime == true){
									if (j == 0)
									htmlToAdd +='<div class="departureTime"><span>Dep</span> '+day.availableFlightInfo[i].segments[j].departureTime+'</div>';
								}
								if (settings.dispalyItems.arrivalTime == true){
									if (j == (day.availableFlightInfo[i].segments.length-1))
									htmlToAdd +='<div class="arivalTime"><span>Arr</span> '+day.availableFlightInfo[i].segments[j].arrivalTime+'</div>';
								}
								
								//get the flight number for each segment and combine to give a flight number for the whole flight.
								flightNumber += day.availableFlightInfo[i].segments[j].flightNumber; 
								if(j != (day.availableFlightInfo[i].segments.length-1) && day.availableFlightInfo[i].segments.length > 1){
									//add a flight number string seperator : TODO make this configurable.
									flightNumber += '/';
								}
							}
							segmentTootltip += '</div>';
						}
						if (seatAvai){
							if (day.availableFlightInfo[i].segments.length > 0) {
								var stopOverCount = day.availableFlightInfo[i].segments.length;
								if (stopOverCount == 1) {
									// Multi leg stop count - One flight has many stop overs
									var segmentCodeCount = (day.availableFlightInfo[i].segments[0].segmentShortCode).split("/").length;
									if (segmentCodeCount > 2) {
										stopOverCount = segmentCodeCount -1;
									}
								}
								
								//stop over count display
								if(settings.dispalyItems.stopOverCount == true) {									
									if (stopOverCount - 1 == 0){										
										if (settings.dispalyItems.displayNonstop ==true){
											htmlToAdd +='<div class="stopover" lang="#seg'+flightID+i+'">'+sendStopsCount(stopOverCount - 1)+'</div>';
										}
																				
									} else {
										htmlToAdd +='<div class="stopover" lang="#seg'+flightID+i+'">'+sendStopsCount(stopOverCount - 1)+'</div>';
									}																	
								}

								//flight number display
								if(settings.dispalyItems.flightNum ==true) {
									htmlToAdd += '<div class="flightNumber"><span>Flight No:</span></div>'
									htmlToAdd += '<div class="flightNumber">' + flightNumber +'</div>'
								}
									
							}
						}else{
							htmlToAdd +='<span class="full"></span>';
						}
							htmlToAdd +='</div></a><span class="flightRefID" style="display:none">'+flightuniqeID+
							'</span><span class="totalDuration" style="display:none">'+day.availableFlightInfo[i].totalDuration+'</span>'+segmentTootltip+'</div>';
						
					}
				 }else{

						 htmlToAdd +='<div><div class="cal-base flightNo zeromargin"><div class="lowerstCon">&nbsp;</div><div class="totalValue noFlight">No Flight</div></div></div>';
				 }
			 }else{
				 htmlToAdd +='<div>'+
					'<div class="cal-base flightNo zeromargin"><div class="lowerstCon">&nbsp;</div><div class="totalValue noFlight">No Flight</div></div></div>';
			 }
			 htmlToAdd +='</div>';
			 
			 return htmlToAdd;
		 };
		 
		 
		 /**
		  * 
		  * flightsArrayForAllDays : The flight infomation for the whole period. 
		  * 						(dto: AvailableFlightInfoByDateDTO.java)	
		  * depOrReturn : flag indicating whether flights are departures of arrivals ["dep" || "arr"]
		  * calDOMObjs : New calendar DOM objects containing. Structure :  { body=, header=} 
		  * numOfDays : The number of days : TODO - verify whether this is really wanted.
		  * dateArray : The date array containing the dates of the period in which flights are created.
		  */
		 function createNewCalendarContent(flightsArrayForAllDays, depOrReturn,calDOMObjs, numOfDays,dateArray){			 
			 //iterate over the date array and set the html contents for bodies and headers.
			 for(i=0; i<dateArray.length; i++){				 
				 var matchingFlightInfoFound = false;	

				 if (flightsArrayForAllDays != undefined && flightsArrayForAllDays != null ){
					 for(j=0; j<flightsArrayForAllDays.length ; j++ ){
						 if(dateArray[i] == flightsArrayForAllDays[j].departureDate){							 
							 $(calDOMObjs[i].body).html(careteOneDay(flightsArrayForAllDays[j],depOrReturn,numOfDays));
							 $(calDOMObjs[i].header).html(careteOneDayHeader(flightsArrayForAllDays[j], depOrReturn, numOfDays, dateArray[i]));
							 matchingFlightInfoFound = true;
							 break;
						 } 
					 }
				 }
				 //if no day is matched that means we have no flights!
				 if (!matchingFlightInfoFound){
					 //pass null and no flights representation would be generated by respective functions
					 $(calDOMObjs[i].body).html(careteOneDay(null,depOrReturn,numOfDays));
					 $(calDOMObjs[i].header).html(careteOneDayHeader(null, depOrReturn, numOfDays, dateArray[i]));
				 }
			 }			 
		 };
		 		 		
		 function createRoot(seg,current,segLength){
			 var strSEG = "";
			 if(current == (segLength-1)){
				 strSEG = seg.split("/")[0]+ " / " + seg.split("/")[1];
			 }else{
				 strSEG = seg.split("/")[0]+ " / "; 
			 }
			return strSEG;
		 };
		 
		 $.fn.calendarview.callTooltip = function(){
			 $(".tootTip").tooltip({ 
				    bodyHandler: function() { 
				 		return $($(this).parent().attr('lang')).html(); 
				    }, 
				    showURL: false,
				    delay: 0,
				    track: true
			 });
			setLowasetFare(calID);
  	     	var sHeagth = 0;
	   		if ($(".cal-base").css("height") == "NaN"){ sHeagth = 1}else {
	   			sHeagth = parseInt($(".cal-base").css("height"))+parseInt($(".cal-base").css("margin"))+parseInt($(".cal-base").parent().css("padding"));
	   		}
   			var total = $("#"+calID).find(".flight-body").children();
	   		if (calDisplayConfig.layout.moreThanTwoFlight == "SCROLL"){
	   			var nok = (total.length>7)? dispalyFlightCount: 1;
	   			$("#"+calID).find(".c-scroller").css("height",Number(nok*sHeagth + 8));
	   			
	   			if (!$.browser.msie && $("body").css("direction") == "rtl")
		   	    	$("#"+calID).find(".c-scroller").css({"position":"relative", "left":"17px"});
	   		}
	   		var tm  = calDisplayConfig.layout.navHeight == undefined ?"19":calDisplayConfig.layout.navHeight;
			$('.flightCalendar').css("top","-"+tm+"px");
			Array.max = function( array ){
			    return Math.max.apply( Math, array );
			};
			if (calID == null){
				$(".flightCalendar").each(function(i,k){
					var temArr = [];
					var flbArra = $(k).find(".flight-body");
					
					
					for(var i=0;i < flbArra.length;i++){
						temArr[temArr.length] = flbArra[i].children.length;
					}
					var t = Array.max(temArr);
					$(k).parent().css("height", ( ( t ) * parseInt($(".cal-base").css("height")) ) +  100 );
				});
			}else{
				setTiming = function(){
					var temArr = [];
					var flbArra = $("#"+calID).find(".flight-body");
					
					for(var i=0;i < flbArra.length;i++){
						temArr[temArr.length] = flbArra[i].children.length;
					}
					var t = Array.max(temArr);
					$("#"+calID).parent().css("height", ( ( t ) * parseInt($(".cal-base").css("height")) ) +  100 );
				}
				setTimeout('setTiming', 500);
				
			}
		 };
		 
		 function fallDecimals(count,value){
			 //alert(String(value).indexOf("."))
			 var falls = ( String(value).indexOf(".") >= count ) ? falls = parseInt(value) : value;
			 return falls;
		 }
		 
		 function getEgdeFare(calIDVal,maxMin){
			 var FareCollection = [];
			 var ind = 0;
			 $.each($("#"+calIDVal).find(".totalValueSelected label.currValueTotal"),function(i,j){
				 if($(j).parent().parent().find("span.full").length == 0){
					 FareCollection[ind++] = parseFloat(j.innerHTML,10);
				 }
			 });
			 var MinMaxFire = 0;
			 if (maxMin.toUpperCase() == "MIN"){
				 MinMaxFire = (FareCollection == "" || FareCollection == undefined)? 0 : Math.min.apply( Math, FareCollection );
			 }else if (maxMin.toUpperCase() == "MAX"){
				 MinMaxFire = (FareCollection == "" || FareCollection == undefined)? 0 : Math.max.apply( Math, FareCollection );
			 }
			 return MinMaxFire;
		 } 
		 
		 function setGradientColors(calIDVal){
			 var lowerst = getEgdeFare(calIDVal, "Min");
			 var highest = getEgdeFare(calIDVal, "Max");
			 if (lowerst == highest){
				 $("#"+calIDVal).find(".hasFlight .cal-base").removeClass("FareGridBorder05");
				 $("#"+calIDVal).find(".hasFlight .cal-base").removeClass("FareGridBorder04");
				 $("#"+calIDVal).find(".hasFlight .cal-base").removeClass("FareGridBorder03");
				 $("#"+calIDVal).find(".hasFlight .cal-base").removeClass("FareGridBorder02");
				 $("#"+calIDVal).find(".hasFlight .cal-base").addClass("FareGridBorder01");
			 }else{
				 var range = (parseFloat(highest) - parseFloat(lowerst))/5;
				$.each($("#"+calIDVal).find(".hasFlight .cal-base"), function(j, k){
					var currVal = parseFloat($(k).find(".totalValueSelected label.currValueTotal").get(0).innerHTML);
					var level = (parseFloat(currVal) - parseFloat(lowerst))/range;
					$($(k).get(0)).addClass(getFarelavel(level));
				}); 
			 }
		 }
		 
		 function setLowasetFare(id,way){
			 if (id == null)
				 var calIDVal = (way == "dep")? "fCalOut":"fCalIn";
			 else
				 calIDVal = id;
			 
			 var lowerst = getEgdeFare(calIDVal, "Min");
			 $.each($("#"+calIDVal).find(".totalValueSelected label.currValueTotal"),function(i,j){
				 if (parseFloat(j.innerHTML,10) == lowerst){
					 $(j).parent().parent().find(".lowerstCon").html("<span class='lowerst'>+ Lowest Fare</span>");
				 }else{
					 $(j).parent().parent().find(".lowerstCon").html("");
				 }
			 });
			 $(".cal-base span:.full").parent().addClass("flightFull");
			 $(".cal-base").removeClass("lowesrtOuter");
			 $("div.lowerstCon:has(.lowerst)").parent().addClass("lowesrtOuter");
			 if (calDisplayConfig.design.displayGradientFare){ setGradientColors(calIDVal);}
			 setCalLabels();
		 }
		 
		 function sendStopsCount(index){
			 var nomArr = UI_AvailabilitySearch.stopovers  || ["Non","One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];
			 if (index == 0)
				 return "<span class=''>" + nomArr[index] + "</span> <span class='lblStopover'>stop</span>";
			 else if (index == 1)
				 return "<span  class='tootTip'>" + nomArr[index] + "</span> <span class='lblStopover'>stop</span>";
			 else
				 return "<span class='tootTip'>" + nomArr[index] + "</span> <span class='lblStopovers'>stops</span>"; 
		 }
		 
		 function getFarelavel(flevel){
			 var str = null;
			 if (flevel != null || flevel != undefined){
				 switch (parseInt(flevel))
				 {
				 case 2:
					 str = "FareGridBorder02"
					  break;
				 case 3:
					 str = "FareGridBorder03"
					  break;
				 case 4:
					 str = "FareGridBorder04"
					  break;
				 case 5:
					 str = "FareGridBorder05"
					  break;
				 default:
					 str = "FareGridBorder01";
				 }
			 }
			return str;
		 };
		 
		 function addChild (d,w,c){
			 var str = null;
			 	if (c == 1){
			 		str = '<li '+dayGap+'><div class="getDay cal-day" style="width:0px;"><img src="'+loading.src+'" align="center" style="visibility:hidden"/></div></li>';
			 	}else
		 			str = '<li '+dayGap+'><div class="getDay cal-day" style="width:'+dayWidth+'px">'+careteOneDay(d,w,c)+'</div></li>';
			 return str;
		 };
		 
		 function addHeaderChild (d,w,c){
			 var str = null;
			 	if (c == 1){
			 		str = '<li '+headerdayGap+'><div class="cal-one-day" style="width:0px">&nbsp;</div></li>';
			 	}else
		 			str = '<li '+headerdayGap+'><div class="cal-one-day" style="width:'+dayWidth+'px">'+careteOneDayHeader(d,w,c)+'</div></li>';
			 return str;
		 };
		 
		 function createWeekdays (){
			 var str = "";
			 for (var i=0;i<jsDays.length;i++){
			  str += '<li '+headerdayGap+'><div class="cal-one-day" style="width:'+dayWidth+'px">'+jsDays[i]+'</div></li>';
			 }
			 return str
		 }
		 
		 function createDatesforWeek(data, way, nod){
			 var gradientID = "";
			 var str = '';
			 var j = 0;
			 var started = false;
			 nod = (nod%7 == 0)? (nod/7)*7 : ((nod/7)+1)*7;
			 for (var i=0;i<nod;i++){
				 if (i%7 == 0)
					j = 0;
				 else
					j++;
				 started = ( jsDays[j] == getDayofDate(data[0].departureDate) )? true : false;
				 if (started)
					 break;
				 else
				 	 str += '<li '+dayGap+'><div class="getDay cal-day" style="width:'+dayWidth+'px"><div class="flight-body">';
				 	 str += '<div><div class="cal-base '+gradientID+'">&nbsp;</div></div></div><div></li>';
			 }
			 for (var i=0;i<data.length;i++){
				 if (data[i].availableFlightInfo == null){
					 str += '<li '+dayGap+'><div class="getDay cal-day" style="width:'+dayWidth+'px"><div class="flight-body">'+
					 '<div><div class="cal-base flightNo '+gradientID+'">No Flight</div></div></div></div></li>';
				 }else if (data[i].availableFlightInfo[0].flightFareSummary.seatAvailable == false){
					 str += '<li '+dayGap+'><div class="getDay cal-day" style="width:'+dayWidth+'px"><div class="flight-body">'+
					 '<div><div class="cal-base flightFull '+gradientID+'">full</div></div></div></div></li>';
				 }else{
				 //gradientID = (data[i].availableFlightInfo[0].priceGuidNo != undefined) ? "FareGridBorder0"+data[i].availableFlightInfo[0].priceGuidNo : "";
					 gradientID = "";
				 if (started){
					 str += '<li '+dayGap+'><div class="getDay cal-day" style="width:'+dayWidth+'px"><div class="flight-body">';
					 str += '<div><a href="javascript:void(0)"><div class="cal-base '+gradientID+'">*</div>';
					 str += '</a></div></div>';
					 str += '</div></li>';
				 }
				 }
			 }
			 return str
		 }
		 
		 function getDayofDate(d){
			 var day = null;
			 var temdate = d.split("/");
		   		var newDate = new Date(temdate[1]+"/"+temdate[0]+"/"+temdate[2]);
		   		day =  jsDays[newDate.getDay()];
			 return day
		 }

		 function dataformater(txt){
			 if (txt != null || txt != undefined) 
				 text = txt.split("/");
			 var dat = null;
			 if ($.browser.msie)
				 dat = Number(text[2])+"/"+Number(text[1])+"/"+Number(text[0]);
			 else
				 dat = Number(text[1])+"/"+Number(text[0])+"/"+Number(text[2]);
			 
			 return dat;
		 };
		 $(document).on("click",".sub-div", function(event){
			 nextPrevBuilderFn(this, event,1); 
		 });
		 $(document).on("click",".sub-div-additional", function(event){
			 //the length of the navigation is the calendar length.
			 nextPrevBuilderFn(this, event,$(this).parent().parent().next().find("div.cal-header ul li").length); 
		 });
		 
		 function nextPrevBuilderFn(xthis, event, noOfDays){
			 calID = $(xthis).parent().parent().next().attr("id") || $(xthis).parent().parent().prev().attr("id");
			 var dat = (calID == "fCalOut")? "dep":"arr";
			 var current = null
			 //The animation duration for appearing and disappearing the calendar day pages.
			 var animationDuration = 250;
			 //to overcome the display issues for several days.
			 if (noOfDays >1){
				 animationDuration = 0;
			 }
			 
			 
			 if ($(xthis).hasClass("cal_previous") == true){
				 if(func.getDataForAllDays != undefined){
					 if ($(xthis).parent().parent().next().find(".calendar-ul").children().length == func.noOfDays &&
							 $(xthis).parent().parent().next().find("div.cal-header ul li:first-child").find(".cal-one-day").html() != "&nbsp;"){
					 				 						
						 // direction - previous						 
						 var dHs = $(xthis).parent().parent().next().find("div.cal-header ul li");
						 var dBs = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li");
						 
						 var toDayStr = new Date();						 
						 var todayIntVal =  dateVal(dataformater(toDayStr.getDate() + "/" + Number(toDayStr.getMonth()+1) + "/" + toDayStr.getFullYear())).d;
						 						 
						 var currentFirstDateStr = $(dHs[0]).find(".cal-one-day span").html();
						 //currentFirstDate = dateVal(dataformater(currentFirstDate)).d;
						 var navigateToDateStr = addDaysAsString(currentFirstDateStr , -noOfDays);
						 var navigateToDateIntVal = dateVal(dataformater(navigateToDateStr)).d;

						 // date validation - the navigated first date should be greater than the 
						 // current date.
						 if (navigateToDateIntVal >= todayIntVal){
							 var length = noOfDays;// $(dHs).length;	
							 var calendarLength = $(dHs).length;

							 //the middle date of the calendar.
							 var pivotalDate = null;
							 // var pivotalDateNumber = ((length -1)/2)+1;
							 //get the pivital date index to get the date value.
							 var pivotalDateIndex = (calendarLength -1) - ((((length -1)/2)+1) -1);							

							 //we need to get the array of dates for the new calendar pages.
							 //this needs to be done before dates are removed from the dom.
							 var dateArrayForNewCalendar = [];						 

							 //calculate the departure and arrival variance.
							 var departureVariance = null;
							 var arrivalVariance = null;

							 if (dat == "dep"){
								 departureVariance = (length -1)/2;
								 // NOTE : There's no difference between arrival variance and departure variance as of now.
								 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
								 arrivalVariance = departureVariance;
							 }						 						 
							 if (dat == "arr"){
								 arrivalVariance = (length -1)/2;		
								 // NOTE : There's no difference between arrival variance and departuer variance as of now.
								 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
								 departureVariance = arrivalVariance;
							 }

							 //array to hold new headers and bodies.
							 var calSearchDataArr = [];
							 var date = null;

							 var loopStartIndex = calendarLength - 1; // start from the end of the calendar.
							 var loopEndIndex = pivotalDateIndex - arrivalVariance;

							 //temp -previous click
							 for (i=loopStartIndex; i>=loopEndIndex; i--){

								 var bHeader = $(xthis).parent().parent().next().find("div.cal-header ul li:first-child");							 
								 var bBody = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li:first-child");

								 var dBody = $(dBs[i]);
								 var dHeader = $(dHs[i]);
							
								 if(date == null){
									 date =  bHeader.find(".cal-one-day span").html();
								 }
								 date = addDaysAsString(date , -1);

								 dateArrayForNewCalendar[dateArrayForNewCalendar.length] = date;

								 if(i == pivotalDateIndex){
									 pivotalDate = date;
								 }

								 current = dataformater(bHeader.find(".cal-one-day span").html());
								 current = dateVal(current).d + 1;	

								 bHeader.before(addHeaderChild(null,null,1));
								 bBody.before(addChild(null,null,1));

								 bHeader = $(xthis).parent().parent().next().find("div.cal-header ul li:first-child").children();
								 bBody = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li:first-child").children();

								 //to resolve scoping issues after animation time outs.
								 (function(dB, dH, bB, bH){
									 dH.animate({width: 1},{duration:animationDuration});
									 dB.animate({width: 1},{duration:animationDuration,
										 complete: function() {										 
											 dB.remove();
											 dH.remove();
										 }});

									 (function(bBody, bHeader){
										 bHeader.animate({width: dayWidth},{duration:animationDuration});
										 bBody.animate({width: dayWidth},{duration:animationDuration,
											 complete: function() {										
												 bBody.children().css("visibility","visible");
												 //alert('working');
												 //func.getData(event, nextDate,dat,7,bBody,"next",careteOneDay,bHeader,careteOneDayHeader);
											 }});
									 })(bB, bH);	 
								 })(dBody, dHeader, bBody, bHeader);

								 // get the data to an array.
								 var obj = {									
										 body:bBody,
										 header:bHeader									
								 };
								 calSearchDataArr[calSearchDataArr.length] = obj;							 
							 }

							 //collect all the data required for the action.
							 var nextPrevCalSearchParams = {
									 calDataArray : calSearchDataArr,
									 calNextDatesArray : dateArrayForNewCalendar,
									 noOfDays : 1,
									 depVar : departureVariance,
									 arrVar : arrivalVariance,
									 pivotalDate : pivotalDate,
									 depOrReturn : dat,
									 calCreateContentCB : createNewCalendarContent, 
									 moveDirection : "prev",
									 calendarLength : calendarLength
							 };
						 
							 func.getDataForAllDays(nextPrevCalSearchParams);
						 }
						 
						 
						 /**
						 var deadHeader = $(xthis).parent().next().find("div.cal-header ul li:last-child");
						 var bornHeader = $(xthis).parent().next().find("div.cal-header ul li:first-child");
						 var deadBody = $(xthis).parent().next().find("div.c-scroller ul.calendar-ul li:last-child");
						 var bornBody = $(xthis).parent().next().find("div.c-scroller ul.calendar-ul li:first-child");
						 var td = new Date();
						 var today =  dateVal(dataformater(td.getDate() + "/" + Number(td.getMonth()+1) + "/" + td.getFullYear())).d;
						 var previousDate = bornHeader.find(".cal-one-day span").html();
						 var current =  dateVal(dataformater(previousDate)).d;
						 previousDate = addDaysAsString(previousDate , -1);
						  if (current > today){
							 current = current - 1;
							 deadHeader.animate({width: 0},{duration:500});
							 deadBody.animate({width: 0},{duration:500,
							 complete: function() {
								 deadBody.remove();
								 deadHeader.remove();
							 }});
							 bornHeader.before(addHeaderChild(null,null,1));
							 bornBody.before(addChild(null,null,1));
							 bornHeader = $(xthis).parent().next().find("div.cal-header ul li:first-child").children();
							 bornBody = $(xthis).parent().next().find("div.c-scroller ul.calendar-ul li:first-child").children();
							 bornHeader.animate({width: dayWidth},{duration:500});
							 bornBody.animate({width: dayWidth},{duration:500,
							 complete: function() {
								 bornBody.children().css("visibility","visible");
								 func.getData(event, previousDate,dat,1,bornBody,"previous",careteOneDay,bornHeader,careteOneDayHeader);
							 }});
						  }

						  */
						  
					 }
				 }
			 }else{
				 if(func.getDataForAllDays != undefined){
					 
					 if ($(xthis).parent().parent().next().find(".calendar-ul").children().length == func.noOfDays &&
							 $(xthis).parent().parent().next().find("div.cal-header ul li:last-child").find(".cal-one-day").html() != "&nbsp;"){
						 
						 
						 var dHs = $(xthis).parent().parent().next().find("div.cal-header ul li");
						 var dBs = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li");
						 
						 var calendarLength = $(dHs).length;						 
						 var length = noOfDays;// $(dHs).length;				
						 
						 //the middle date of the calendar.
						 var pivotalDate = null;

						 //get the pivital date index to get the date value.
						 var pivotalDateIndex = (((length -1)/2)+1) -1;
						 
						 //we need to get the array of dates for the new calendar pages.
						 //this needs to be done before dates are removed from the dom in the next for loop.
						 var dateArrayForNewCalendar = [];						 

						 //calculate the departure and arrival variance.
						 var departureVariance = null;
						 var arrivalVariance = null;
						 if (dat == "dep"){
							 departureVariance = (length -1)/2;
							 // NOTE : There's no difference between arrival variance and departure variance as of now.
							 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
							 arrivalVariance = departureVariance;
						 }						 
						 
						 if (dat == "arr"){
							 arrivalVariance = (length -1)/2;		
							 // NOTE : There's no difference between arrival variance and departuer variance as of now.
							 // The action classes use departure variance for flight search. hence setting the both values to the same amount.
							 departureVariance = arrivalVariance;
						 }
						 
						 //array to hold new headers and bodies.
						 var calSearchDataArr = [];
						 var date = null;
						 for (i=0; i<length; i++){
							 
							 var dHeader = $(dHs[i]);// $(this).parent().next().find("div.cal-header ul li:first-child");
							 var bHeader = $(xthis).parent().parent().next().find("div.cal-header ul li:last-child");
							 var dBody = $(dBs[i]); //$(this).parent().next().find("div.c-scroller ul.calendar-ul li:first-child");
							 var bBody = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li:last-child");
							 			
							 if(date == null){
								 date =  bHeader.find(".cal-one-day span").html();
							 }
							 date = addDaysAsString(date , 1);
							 
							 dateArrayForNewCalendar[dateArrayForNewCalendar.length] = date;
							 
							 if(i == pivotalDateIndex){
								 pivotalDate = date;
							 }
							 
							 current = dataformater(bHeader.find(".cal-one-day span").html());
							 current = dateVal(current).d + 1;	
							 
							 bHeader.after(addHeaderChild(null,null,1));
							 bBody.after(addChild(null,null,1));
							 
							 bHeader = $(xthis).parent().parent().next().find("div.cal-header ul li:last-child").children();
							 bBody = $(xthis).parent().parent().next().find("div.c-scroller ul.calendar-ul li:last-child").children();
							 
							 //to resolve scoping issues after animation time outs.
							 (function(dB, dH, bB, bH){
								 dH.animate({width: 1},{duration:animationDuration});
								 dB.animate({width: 1},{duration:animationDuration,
									 complete: function() {										 
										 dB.remove();
										 dH.remove();
									}});

								 (function(bBody, bHeader){
									 bHeader.animate({width: dayWidth},{duration:animationDuration});
									 bBody.animate({width: dayWidth},{duration:animationDuration,
										 complete: function() {										
											 	bBody.children().css("visibility","visible");
											 	//alert('working');
											 	//func.getData(event, nextDate,dat,7,bBody,"next",careteOneDay,bHeader,careteOneDayHeader);
										 }});
								 })(bB, bH);	 
							 })(dBody, dHeader, bBody, bHeader);
							 
							 // get the data to an array.
							 var obj = {									
									body:bBody,
									header:bHeader									
							 };
							 calSearchDataArr[calSearchDataArr.length] = obj;							 
						 }
						 
						 //collect all the data required for the action.
						 var nextPrevCalSearchParams = {
								 calDataArray : calSearchDataArr,
								 calNextDatesArray : dateArrayForNewCalendar,
								 noOfDays : 1,
								 depVar : departureVariance,
								 arrVar : arrivalVariance,
								 pivotalDate : pivotalDate,
								 depOrReturn : dat,
								 calCreateContentCB : createNewCalendarContent, 
								 moveDirection : "next",
								 calendarLength : calendarLength									
						 };
						 
						 func.getDataForAllDays(nextPrevCalSearchParams);												
						 /**
						 
						 
						 var deadHeader = $(this).parent().next().find("div.cal-header ul li:first-child");
						 var bornHeader = $(this).parent().next().find("div.cal-header ul li:last-child");
						 var deadBody = $(this).parent().next().find("div.c-scroller ul.calendar-ul li:first-child");
						 var bornBody = $(this).parent().next().find("div.c-scroller ul.calendar-ul li:last-child");
						 current = dataformater(bornHeader.find(".cal-one-day span").html());
						 current = dateVal(current).d + 1;
						 var nextDate = bornHeader.find(".cal-one-day span").html();
					 	 nextDate = addDaysAsString(nextDate , 1);
						 deadHeader.animate({width: 1},{duration:500});
						 deadBody.animate({width: 1},{duration:500,
						 complete: function() {
							 deadBody.remove();
							 deadHeader.remove();
						 }});
						 bornHeader.after(addHeaderChild(null,null,1));
						 bornBody.after(addChild(null,null,1));
						 bornHeader = $(this).parent().next().find("div.cal-header ul li:last-child").children();
						 bornBody = $(this).parent().next().find("div.c-scroller ul.calendar-ul li:last-child").children();
						 bornHeader.animate({width: dayWidth},{duration:500});
						 bornBody.animate({width: dayWidth},{duration:500,
						 complete: function() {
							 	bornBody.children().css("visibility","visible");
							 	func.getData(event, nextDate,dat,1,bornBody,"next",careteOneDay,bornHeader,careteOneDayHeader);
						 }});
						 
						 **/
					 }	 
				 }
			 }
		 };
		 
		 function addDaysAsString(strDate, intDays){
			 if (strDate != null){
				 var dtDate = new Date(Number(strDate.substring(6,10)), Number(strDate.substring(3,5))-1, Number(strDate.substring(0,2)));
				 var gmtOffsetOri = dtDate.getTimezoneOffset();
				 var calNewDate = new Date(dtDate.getTime() + Number(intDays) *24*60*60*1000);
				 var gmtOffsetNew = calNewDate.getTimezoneOffset();
				 //Assumption they won't be multiple dst changes with these days
				 if (gmtOffsetOri != gmtOffsetNew){
					 var offsetDifference = gmtOffsetOri - gmtOffsetNew;
					 calNewDate = new Date(calNewDate.getTime() - (offsetDifference*60*1000));	
				 } 
				 return DateToString(calNewDate);
			 }
		 };
		 
		 function dateVal( str1 ) {
			    var diff = Date.parse( str1 );
			    return isNaN( diff ) ? NaN : {
			    	diff : diff,
			    	ms : Math.floor( diff            % 1000 ),
			    	s  : Math.floor( diff /     1000 %   60 ),
			    	m  : Math.floor( diff /    60000 %   60 ),
			    	h  : Math.floor( diff /  3600000 %   24 ),
			    	d  : Math.floor( diff / 86400000        )
			    };
		};
		 
		 
		 $(document).on("mouseover",".getDay",function(){
				$(this).addClass("mout");
		 });
		 $(document).on("mouseout",".getDay",function(){
				$(this).removeClass("mout");
		 });
		 
		 $(document).on("click",".flight-header a",function(){
				$(this).parent().parent().children().removeClass("_selected");
				$(this).parent().addClass("_selected");
				$(this).parent().parent().next().children().addClass("notshow");
				$("#b-"+this.id).removeClass("notshow");
		});
		 
		$.fn.calendarview.selectItem = function(event){
			//setting selected object
			UI_AvailabilitySearch.lastSelected = $(event.currentTarget).parent().parent().parent().parent().parent().find("a.selected");
			$(event.currentTarget).parent().parent().parent().parent().parent().find("a").removeClass("selected");
			$(event.currentTarget).addClass("selected")
			$(event.currentTarget).find("input[type='radio']").attr("checked",true);
		};
		
		$.fn.calendarview.changeSelectedFare = function(values){
			var defaults = {inbound:null, outbound:null}
			var values = $.extend(defaults,values);
			var maxWholeNumWithDecimal = 0;			
			
				if(values.outbound!=null ){
					//$("#fCalOut").find(".selected").parent().find(".totalValueSelected").find('.currValueTotal').text(fallDecimals(maxWholeNumWithDecimal, values.outbound.selcurr));
					$("#fCalOut").find(".selected").parent().find(".totalValueSelected").find('.currValueTotal').text(values.outbound);
					setLowasetFare("fCalOut")
				}
				if(values.inbound != null){
					//$("#fCalIn").find(".selected").parent().find(".totalValueSelected").find('.currValueTotal').text(fallDecimals(maxWholeNumWithDecimal, values.inbound.selcurr));
					$("#fCalIn").find(".selected").parent().find(".totalValueSelected").find('.currValueTotal').text(values.inbound);
					setLowasetFare("fCalIn")
				}
		}
		 
//end of plugin
	    });
	 };
	 
	 $(document).on("click","#fCalOut div.flight-body a", function(event){
		 var __selctedItem = $(this).parent().find(".flightRefID").text();
		 var __duration = $(this).parent().find(".totalDuration").text();
		 //ulSelectAll($(this));
		 //$(this).addClass("selected");
		 $.fn.calendarview.selectItem(event);
		 if (func.selectFlight!= undefined){
			func.selectFlight(event,__selctedItem,"departure",__duration);
		 }
	 });

	 function ulSelectAll(obj){
		 obj.parent().parent().parent().parent().parent().find("a").removeClass("selected");
	 };
	 
	 $(document).on("click","#fCalIn div.flight-body a", function(event){
   		 var __selctedItem = $(this).parent().find(".flightRefID").text();
   		 var __duration = $(this).parent().find(".totalDuration").text();
   		 //ulSelectAll($(this));
   		 //$(this).addClass("selected");
   		$.fn.calendarview.selectItem(event)
   		 if (func.selectFlight!= undefined){
   			 func.selectFlight(event,__selctedItem,"arrival",__duration);
   		 }
   	 });
	 
	 function setCalLabels(){
		 if (UI_AvailabilitySearch.labels != "" || UI_AvailabilitySearch.labels != null){
			 $(".lowerst").text(UI_AvailabilitySearch.labels.lowerstFare);
			 $(".lowerst").attr("title",UI_AvailabilitySearch.labels.lowerstFare);
			 $(".FlightFull").text(UI_AvailabilitySearch.labels.fullFlights);
			 $(".departureTime span").text(UI_AvailabilitySearch.labels.shortDeparture);
			 $(".arivalTime span").text(UI_AvailabilitySearch.labels.shortArrival);
			 $(".noFlight").text(UI_AvailabilitySearch.labels.noFlights);
			 $(".lblStopover").text(UI_AvailabilitySearch.labels.shortover);
			 $(".lblStopovers").text(UI_AvailabilitySearch.labels.shortovers);
			 $(".getDay").css({"borderColor":"transparent", "height":"auto","background":"transparent"});

		 }
	 }
	 
	 function getModifyDateStaus(){
		 var isModifyDate = true;
		 if(UI_AvailabilitySearch.isModifySegment == true){
			 if(UI_AvailabilitySearch.modifyByDate == false){
				 isModifyDate = false;
			 }
		 }
		 return isModifyDate;
	 }
	 
	 
	 /**
	  * add Flexi Itrms to Cal this was moved from availabilitySearch.js
	  */
	 $.fn.calendarview.includeFlexiInCal = function(){
			$("#fCalOut").append($("#outFlightFlexi"));
			$("#fCalIn").append($("#inFlightFlexi"));
	 }
	 
	 /**
	  * Setting in Place Flexi box in Calendar moved from availabilitySearch.js
	  */
	 $.fn.calendarview.settingInPlaceFlexiInCal = function(obj){
	 	var itemsSelected = obj.parent().find(".selected").find(".stopover");
	 	var leftval = ($("html").attr("dir") == "rtl")? 5 : 3;
	 	obj.css({
	 		position:"absolute",
	 		width:obj.parent().width() - 10,
	 		top:itemsSelected.position().top + 16,
	 		left:  leftval
	 	})
	 	obj.show();
	 }
	 /**
	  * to create tabs html when selecting the tabs
	  */
	 $.fn.calendarview.createTabData = function(ind){
	 	var str ="";
	 	if (ind == 0){
	 	str += '<div class="flight-oriented">';
	 	str +'<table border="0" width="100%" cellpadding="0" cellspacing="0" style="width:100%;margin:0 auto">';
	 	str +='<tr>'+
	 			'<td width="300" valign="top">'+
	 				'<div class="flightCalendar" style="width:255px" id="fCalOut">'+
	 				'</div>'+
	 			'</td>'+
	 			'<td width="6"><img src="../images/spacer_no_cache.gif"/></td>'+
	 			'<td style="border-left: 1px solid #AAA;width: 1px"><img src="../images/spacer_no_cache.gif"/></td>'+
	 			'<td width="5"><img src="../images/spacer_no_cache.gif"/></td>';
	 			if ($("#resReturnFlag").val() == "true"){
	 				str +='<td width="300" valign="top">'+
	 					'<div class="flightCalendar" style="width:255px" id="fCalIn">'+
	 					'</div>'+
	 				'</td>';
	 			}
	 		str +='</tr>'+
	 	'</table>'+
	 	'</div>';
	 	}else{
	 		str += '<div class="flight-oriented">';
	 		str +='<table border="0" width="100%" cellpadding="0" cellspacing="0" style="width:100%;margin:0 auto">';
	 		if (UI_AvailabilitySearch.calendarGradianView){
	 			str +='<tr><td colspan="2">'+
	 				'<span id="spnLegends"><table cellspacing="2" cellpadding="0" border="0" align="center" width="100%">'+
	 						'<tr>'+
	 						'<td align="right"><label class="fntEnglish" id="lblPriceGuide">'+UI_AvailabilitySearch.labels.lblPriceGuide+'</label></td>'+
	 						'<td align="right" width="28%">'+
	 							'<table cellspacing="2" cellpadding="0" border="0" align="center" width="100%" class="FareGridBorder">'+
	 							'<tr>'+
	 								'<td align="center" width="20%" class="FareGridBorder01"><label id="lblPriceGuideLow" title="Lowest fare" style="color: black;" class="fntLowHigh"><b>'+UI_AvailabilitySearch.labels.lblPriceGuideLow+'</b></label></td>'+
	 								'<td align="center" width="20%" class="FareGridBorder02"><font>&nbsp;</font></td><td align="center" width="20%" class="FareGridBorder03"><font>&nbsp;</font></td>'+
	 								'<td align="center" width="20%" class="FareGridBorder04"><font>&nbsp;</font></td>'+
	 								'<td align="center" width="20%" class="FareGridBorder05"><label id="lblPriceGuideHigh" title="Highest fare" style="color: black;" class="fntHighLow"><b>'+UI_AvailabilitySearch.labels.lblPriceGuideHigh+'</b></label></td>'+
	 							'</tr>'+
	 							'</table>'+
	 						'</td>'+
	 						'</tr>'+
	 					'</table></span>'+
	 				'</td></td>';
	 		}
	 		var currCode = UI_AvailabilitySearch.currencyCode;
	 		if(UI_AvailabilitySearch.currencyCode!= UI_AvailabilitySearch.selectedCurrency){
	 			currCode = UI_AvailabilitySearch.selectedCurrency;
	 		}
	 		str +='<tr><td valign="top" class="pnlTop pnlWidth"></td><td class="pnlTopR"></td></tr>'+
	 			'<tr>'+
	 				'<td width="100%" valign="top" colspan="2" class="pnlBottomTallCal"><div class="outerCal">'+
	 					'<div class="flightCalendar" style="width:'+calDisplayConfig.layout.calWidth+'px" id="fCalOut"></div></div>';
	 					
	 					str +='<div id="outFlightFlexi" class="flexibar" style="display:none">'+
	 						'<label><input type="checkbox" title="'+UI_AvailabilitySearch.labels.lblFlexiUpgradeTitle+'"  class="noBorder" name="chkFlexiOutbound" id="chkFlexiOutbound"/></label>'+
	 						'<label id="lblFlexiUpgrade">'+UI_AvailabilitySearch.labels.lblFlexiUpgrade+': + </label><label id="flexiChargeOut">0</label><label> ' + currCode +'</label>'+
	 						' <a href="javascript:void(0)"><u><label class="hdFontColor" id="lblmoreFlexiOut">('+UI_AvailabilitySearch.labels.lblmoreFlexiOut+')</label></u></a>'+
	 						'</div>';
	 					str +='</td></tr><tr><td width="100%" valign="top" colspan="2">';
	 					if (calDisplayConfig.layout.displayFlightDetailsInCal)
	 						str +='<div id="outFlightDetails" style="display:none"><table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable" ><tr>'+
	 								'<td align="center" class="gridHD"><label id="lblSegment" class="gridHDFont fntBold"></label></td>'+
	 								'<td  align="center" class="gridHD" colspan="2"><label id="lblDepature" class="gridHDFont fntBold"/>&nbsp;<label id="lblTime" class="gridHDFont fntBold"/></td>'+
	 								'<td  align="center" class="gridHD" colspan="2"><label id="lblArrival" class="gridHDFont fntBold"/>&nbsp;<label id="lblTime" class="gridHDFont fntBold"/></td>'+
	 								'<td  align="center" class="gridHD"><label id="lblFlightNo" class="gridHDFont fntBold"></label></td>'+
	 								//'<td rowspan="2" align="center" class="gridHD"><label id="lblCarrier" class="gridHDFont fntBold"></label></td>'+
	 								'<td align="center" class="gridHD"><label id="lblDuration" class="gridHDFont fntBold">Duration</label></td>'+
	 								'</tr>'+
	 							/*'<tr>            '+
	 								'<td class="gridHDDark" align="center"><label id="lblDate" class="gridHDFont fntBold"></label></td>'+
	 								'<td class="gridHDDark" align="center"><label id="lblTime" class="gridHDFont fntBold"></label></td>'+
	 								'<td class="gridHDDark" align="center"><label id="lblDate" class="gridHDFont fntBold"></label></td>'+
	 								'<td class="gridHDDark" align="center"><label id="lblTime" class="gridHDFont fntBold"></label></td>  '+          
	 							'</tr>*/
	 								'<tr id="tableOB">'+
	 								'<td width="40%" height="25" class="rowColor bdLeft bdRight bdBottom alignLeft"><label id="segmentCode"></label></td>'+
	 								'<td width="15%" class="rowColor bdRight bdBottom" align="center"><label id="departureDate"></label></td>'+
	 								'<td width="8%" class="rowColor bdRight bdBottom" align="center"><label id="departureTime"></label></td>'+
	 								'<td width="15%" class="rowColor bdRight bdBottom" align="center"><label id="arrivalDate"></label></td>'+
	 								'<td width="8%" class="rowColor bdRight bdBottom" align="center"><label id="arrivalTime"></label></td>'+
	 								'<td width="10%" class="rowColor bdRight bdBottom" align="center"><label id="flightNumber"></label></td>'+
	 								//'<td width="9%" class="rowColor bdRight bdBottom" align="center"><img id="carrierImagePath" src=""/></td>'+
	 								'<td width="9%" class="rowColor bdRight bdBottom outDuration" align="center"><label id="totalDuration"></label></td>'+
	 							'</tr></table></div>';
	 			if ($("#resReturnFlag").val() == "true"){
	 				str +='<tr><td colspan="2" class="rowGap" style="height:25px"></td></tr><tr><td valign="top" class="pnlTop pnlWidth"></td><td class="pnlTopR"></td></tr>';
	 				str +='<td width="100%" valign="top" colspan="2" class="pnlBottomTallCal"><div class="outerCal">'+
	 					'<div class="flightCalendar" style="width:'+calDisplayConfig.layout.calWidth+'px" id="fCalIn"></div></div>';
	 				
	 					str +='<div id="inFlightFlexi" class="flexibar" style="display:none">'+
	 						'<label><input type="checkbox" title="'+UI_AvailabilitySearch.labels.lblFlexiUpgradeTitle+'"  class="noBorder" name="chkFlexiInbound" id="chkFlexiInbound"/></label>'+
	 						'<label id="lblFlexiUpgrade">'+UI_AvailabilitySearch.labels.lblFlexiUpgrade+': +</label><label id="flexiChargeIn">0</label><label> '+currCode +'</label>'+
	 						' <a href="javascript:void(0)"><u><label class="hdFontColor" id="lblmoreFlexiIn">('+UI_AvailabilitySearch.labels.lblmoreFlexiOut+')</label></u></a>'+
	 					'</div>';
	 					str +='</td></tr><tr><td width="100%" valign="top" colspan="2">';
	 					if (calDisplayConfig.layout.displayFlightDetailsInCal)
	 						str +='<div id="inFlightDetails" style="display:none"><table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable" ><tr>'+
	 							'<td  align="center" class="gridHD"><label id="lblSegment" class="gridHDFont fntBold"></label></td>'+
	 							'<td  align="center" class="gridHD" colspan="2"><label id="lblDepature" class="gridHDFont fntBold"/>&nbsp;<label id="lblTime" class="gridHDFont fntBold"/></td>'+
	 							'<td  align="center" class="gridHD" colspan="2"><label id="lblArrival" class="gridHDFont fntBold"/>&nbsp;<label id="lblTime" class="gridHDFont fntBold"/></td>'+
	 							'<td  align="center" class="gridHD"><label id="lblFlightNo" class="gridHDFont fntBold"></label></td>'+
	 							//'<td rowspan="2" align="center" class="gridHD"><label id="lblCarrier" class="gridHDFont fntBold"></label></td>'+
	 							'<td  align="center" class="gridHD"><label id="lblDeuration" class="gridHDFont fntBold">Duration</label></td>'+
	 							'</tr>'+
	 						/*'<tr> '+
	 							'<td class="gridHDDark" align="center"><label id="lblDate" class="gridHDFont fntBold"></label></td>'+
	 							'<td class="gridHDDark" align="center"><label id="lblTime" class="gridHDFont fntBold"></label></td>'+
	 							'<td class="gridHDDark" align="center"><label id="lblDate" class="gridHDFont fntBold"></label></td>'+
	 							'<td class="gridHDDark" align="center"><label id="lblTime" class="gridHDFont fntBold"></label></td>  '+          
	 						'</tr>'+*/
	 						'<tr id="tableIB">'+
	 							'<td width="40%" height="25" class="rowColor bdLeft bdRight bdBottom alignLeft"><label id="segmentCode"></label></td>'+
	 							'<td width="15%" class="rowColor bdRight bdBottom" align="center"><label id="departureDate"></label></td>'+
	 							'<td width="8%" class="rowColor bdRight bdBottom" align="center"><label id="departureTime"></label></td>'+
	 							'<td width="15%" class="rowColor bdRight bdBottom" align="center"><label id="arrivalDate"></label></td>'+
	 							'<td width="8%" class="rowColor bdRight bdBottom" align="center"><label id="arrivalTime"></label></td>'+
	 							'<td width="10%" class="rowColor bdRight bdBottom" align="center"><label id="flightNumber"></label></td>'+
	 							//'<td width="9%" class="rowColor bdRight bdBottom" align="center"><img id="carrierImagePath" src=""/></td>'+
	 							'<td width="9%" class="rowColor bdRight bdBottom inDuration" align="center"><label id="totalDuration"></label></td>'+
	 						'</tr></table></div>';
	 				str +='</td>';
	 			}
	 		str +='</tr><tr><td colspan="2" class="rowGap"></td></tr>';
	 		
	 		
	 	str +='</table>'+
	 		'</div>';	
	 	}
	 	return str;
	 }

})(jQuery);