function UI_CaptchaValidation(){
    this.labels = null;
    this.capchaHTML = null;
    this.isHumanVerificationEnabled = UI_Top.holder().GLOBALS.humanVerificationEnabled;
    this.storedSearchData = null;
    this.lastTriggeredAction = null;
    this.responseEvalutionFunction = null;
    this.additionalData = null;
    this.openCaptchaPopup = function(){
        setCaptchaHTML();
        openPopup();
        refresh();
    };
    this.setDataToProcessAfterCaptchaValidation = function (data, action, evalFuction, additionalData) {
      	 this.storedSearchData = data;
       	 this.lastTriggeredAction = action;
       	 this.responseEvalutionFunction =  evalFuction;
       	 this.additionalData = additionalData;
    };
    
    this.resetDataToProcessAfterCaptchaValidation = function () {
      	 this.storedSearchData = null;
       	 this.lastTriggeredAction = null;
       	 this.responseEvalutionFunction =  null;
       	 this.additionalData = null;
    };

    var setCaptchaHTML = function(){
        var temp = $("#trPanelImageCaptcha_dummy").clone();
        temp.attr("id", "trPanelImageCaptchaLive");
        temp.find("input, img, button").each(function( i, j ){
            $(j).attr("id", j.id.replace("_dummy", "Live"));
            $(j).attr("name",j.name.replace("_dummy", "Live"));
        });
        UI_CaptchaValidation.capchaHTML = temp;
    };

    var openPopup = function(){
        $("#newPopItem").openMyPopUp({
            width: 480,
            height: 240,
            topPoint: ($(window).height()/2) - 200,
            headerHTML:"<label class='hdFontColor' style='font-weight: bold'>"+ UI_CaptchaValidation.labels.CaptchaHeading +"</label>",
            bodyHTML: function(){
                return UI_CaptchaValidation.capchaHTML.html();
            },
            footerHTML: function(){
                var passHTML = "<label class='hdFontColor'> </label>";
                return passHTML;
            }
        });
        $(".close").hide().unbind("click").bind("click",function(){$("#newPopItem").closeMyPopUp();});
        $("#btnCaptchaSubmitLive").off("click").on("click", isValidateImageChapcha);
        $("#imgCPTLive").off("click").on("click", refresh);
        $("#divLoadBg").show();
        $(document).bind("keypress", function(keyEvent){
            if(keyEvent.keyCode == 27){
                $("#newPopItem").closeMyPopUp();
                $(document).unbind("keypress");
            }
        });
    };

    var isValidateImageChapcha = function() {
        if ($("#txtCaptchaLive").val()!==""){
            var data = {};
            data['captcha'] = $("#txtCaptchaLive").val();
            UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, 
				url:'imageChapcha.action', async:false,beforeSubmit:UI_commonSystem.loadingProgress,
				success: processImageCaptcha, 
				error:UI_commonSystem.setErrorStatus});
           return  false;
        } else {
        	jAlert("Please enter captcha text",'Alert');
        }
    };

    var processImageCaptcha = function(response) {
        var msgTxt = response.messageTxt;
        if(response != null && response.success == true) {
            if (msgTxt != null && msgTxt == "valid") {
            	$("#newPopItem").closeMyPopUp();
            	UI_commonSystem.loadingProgress();
            	if(UI_CaptchaValidation.responseEvalutionFunction == 'SELDATE') {
            		UI_CaptchaValidation.additionalData['selectedHtmlToAdd'].trigger("click");
            	} else {
            	 	$.ajax({type: "POST", dataType: 'json',data: UI_CaptchaValidation.storedSearchData , url:UI_CaptchaValidation.lastTriggeredAction,		
               		 success: function(response){
               			 
               			 switch(UI_CaptchaValidation.responseEvalutionFunction){
               			 	case 'MCRESEARCH':
               			 		UI_mcAvailabilitySearch.processFlightReSearch(response,UI_CaptchaValidation.additionalData['index']);
               			 		break;
               			 	case 'AVAIL':
               			 		UI_AvailabilitySearch.loadAvailabilitySearchDataProcess(response);
               			 		break;
               			 	case 'MCSEARCH':
               			 		UI_mcAvailabilitySearch.processFlightSearch(response);
               			 		break;
               			 }
        				}
               		 , error:UI_commonSystem.setErrorStatus, cache : false});
            	}
            } else {
                refresh();
                jAlert("Incorrect Image",'Alert');
                UI_commonSystem.loadingCompleted();
            }
        } else {
            refresh();
            wrapAndDebugError('005',response);
            UI_commonSystem.loadingCompleted();
        }

    };

   var refresh = function(){
        $("#txtCaptchaLive").val("");
        document['imgCPTLive'].src = '../jcaptcha.jpg?relversion=' + (new Date()).getTime();
    };
}
var UI_CaptchaValidation = new UI_CaptchaValidation();