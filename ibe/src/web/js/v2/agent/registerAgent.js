/**
 * Author : Gavinda Nanayakkara
 * 
 */
function UI_RegAgent(){} 

var errorInfo  = "";
var arrCountryPhone = new Array();
var arrAreaPhone  = new Array();
var arrStationCountry  = new Array();
var acclaeroClientIdentifier = "";

$(document).ready(function() {
	UI_RegAgent.ready();
}); 
/**
 * Page Ready
 *
 */
UI_RegAgent.ready = function() {
	$("#trMsg").hide();
	if (taCo == "CO") {
		$("#taCoAgencyName").show();
		$("#taLicenseNo").hide();
		$("#taIATACode").hide();
		$("#taPayMethBS").hide();
		$("#coCompanyContactName").show();
		$("#coContactDesignation").show();
		$("#coCompanySize").show();
		$("#coConvenientTime").show();
		$("#coMobile").show();
		$("#TaFax").hide();
		$("#radPayMethAC").attr('checked', false);
		$("#agentTypeName").html(agentType.cooparate);
		$("#stationName").html(stationLbl.cooparate);
		$("#taPayMethAC").hide();
		$("#coPayMeth").show();
	} else {
		$("#taCoAgencyName").show();
		$("#taLicenseNo").show();
		$("#taIATACode").show();
		$("#taPayMethBS").show();
		$("#coCompanyContactName").hide();
		$("#coContactDesignation").hide();
		$("#coCompanySize").hide();
		$("#coConvenientTime").hide();
		$("#coMobile").hide();
		$("#TaFax").show();
		$("#radPayMethAC").attr('checked', true);
		$("#agentTypeName").html(agentType.normal);
		$("#stationName").html(stationLbl.normal);
		$("#taPayMethAC").show();
		$("#coPayMeth").hide();
	}
	UI_RegAgent.loadAgentRegisterData();
	$("#divLoadBg").show();
	$("#btnRegister").click(function(){UI_RegAgent.registerAgent();});
	$("#btnReset").click(function(){UI_RegAgent.resetButtonClick();});	
	$("#btnPrevious").click(function(){SYS_IBECommonParam.homeClick();});
	$("#selCountry").change(function(){Util_AgentValidation.countryListChange()});
	$("#txtPCountry").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtPArea").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtTelephone").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtFCountry").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtFArea").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtMCountry").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtMArea").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtMobile").keydown(function(){Util_AgentValidation.telephoneNoKeyPress(this);});

	$("#txtPCountry").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtPArea").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtTelephone").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtFCountry").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtFArea").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtMCountry").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtMArea").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	$("#txtMobile").keyup(function(){Util_AgentValidation.telephoneNoKeyPress(this);});
	
}
/**
 * Request Page data
 */
UI_RegAgent.loadAgentRegisterData = function() {	
	$("#submitForm").ajaxSubmit({url:"showAgentRegisterDetail.action", dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success: UI_RegAgent.loadAgentRegisterDataSucess, error:UI_commonSystem.setErrorStatus});	
	return false;
}
/**
 * 
 * Request data success
 */
UI_RegAgent.loadAgentRegisterDataSucess =  function(response) {	
	if(response != null && response.success == true) {	
		UI_RegAgent.buildTelephoneInfo(response.countryPhoneInfo, response.areaPhoneInfo);
		eval(response.stationCountryInfo);
		errorInfo = response.errorInfo;	
		acclaeroClientIdentifier = response.acclaeroClientIdentifier;
		$("#selCountry").append(response.countryInfo);	
		$("#selStation").append(response.stationInfo);
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}
/**
 * Register User
 */
UI_RegAgent.registerAgent = function() {
	if (!Util_AgentValidation.validateFormData())
		return;
	
	if (trim($("#txtTelephone").val()) == ""){
		$("#txtPCountry").val("");
		$("#txtPArea").val("");
	} 
	
	if (taCo == "CO" && trim($("#txtMobile").val()) == ""){
		$("#txtMCountry").val("");
		$("#txtMArea").val("");
	}
					
	$("#frmRegister").attr('action', SYS_IBECommonParam.securePath + 'agentRegister.action');
	$("#frmRegister").ajaxSubmit({dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress, success: UI_RegAgent.registerAgentSuccess, error:UI_commonSystem.setErrorStatus});			
	return false;
}

/**
 * Register Agent Success
 */
UI_RegAgent.registerAgentSuccess = function(response) {
	if(response != null && response.success == true) {
		if (response.messageTxt != null && $.trim(response.messageTxt) != "") {			
			$("#spnError").html("<font class='mandatory'>" + response.messageTxt + "<\/font>");		
		} else {
			$("#trForm").hide();
			$("#trMsg").show();
		}		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

/**
 * Button Reset Click
 */
UI_RegAgent.resetButtonClick = function() {
	$("#frmRegister").reset();
}

//Build Telephone data
UI_RegAgent.buildTelephoneInfo = function(strCountryPhone, strAreaPhone) {	
	
	var tempArrAreaPhone = null;
	var tempCountryCode = null;
	var tempNewArray = null;
	
	if (strCountryPhone != null && strAreaPhone != null) {		
		var arrCountryPhoneLocal = strCountryPhone.split("^");
		for (var i = 0; i < arrCountryPhoneLocal.length; i++) {
			arrCountryPhone[i] = arrCountryPhoneLocal[i].split(",");
		}
		
		var arrAreaPhoneLocal = strAreaPhone.split("^");
		for (var i = 0; i < arrAreaPhoneLocal.length; i++) {
			tempArrAreaPhone = arrAreaPhoneLocal[i].split(",");	
			tempCountryCode = tempArrAreaPhone[0];
			tempNewArray = new Array();
			tempNewArray[0] = tempArrAreaPhone[1];
			tempNewArray[1]= tempArrAreaPhone[2];
			tempNewArray[2]= tempArrAreaPhone[3];
			
			if (arrAreaPhone[tempCountryCode] == undefined) {
				arrAreaPhone[tempCountryCode] = new Array();
			}			
			arrAreaPhone[tempCountryCode][i] = new Array();
			arrAreaPhone[tempCountryCode][i]  = tempNewArray;			
		}		
	}	
}


