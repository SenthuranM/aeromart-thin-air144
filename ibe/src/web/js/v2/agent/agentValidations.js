function Util_AgentValidation(){} 

var strDefTitle = "";
var strErrorHTML = "";
// To DO : Get Using Locale
var strMsgFax = 'Fax No';
var strMsgAgencyName = 'Agency Name';
var strMsgAddress = 'Address';
var strMsgCity = 'City';
	
	Util_AgentValidation.hasSpecialCharactors = function(str){
		var validated=true;
		var chars= new Array('>','<' ,'"',"'",'/','^','|') ;
		var len=str.length;
		var val=removeInvalids(str);
		if(val!=str){
			return false;
		}
		return validated;
	
	}
	
	/**
	 * Return the country code for the selected country
	 */
	Util_AgentValidation.getCCForCountry = function() {
		var CONST = {CC: 0, COUNTRY: 4}
		var cArr = arrCountryPhone;
		var selectedCountry =  getValue("selCountry");
		for(var i=0; i<cArr.length; i++){
			if(cArr[i][CONST.COUNTRY] ==selectedCountry ){
				return cArr[i][CONST.CC];
			}
		}
		return '';
	}
	/**
	 * Validate Form Data
	 */
	Util_AgentValidation.validateFormData = function() {		
		Util_AgentValidation.removeClass();
		var validated=false;
		var strAgencyName = $("#txtAgencyName").val();
		var strLicenseNo = $("#txtLicenseNo").val();
		var strIATACode = $("#txtIATACode").val();
		var strStation = $("#selStation").val();
		var radPayMethAC = $("#radPayMethAC").val();
		var chkPayMeth = $("#chkPayMeth").val();
		var strAddress = $("#txtAddress").val();
		var strCity = $("#txtCity").val();
		var strCountry = $("#selCountry").val();
		var strState = $("#txtState").val();
		var strPostalCode = $("#txtPostalCode").val();
		var strEmail = $("#txtEmail").val();		
		var strTelephone = $("#txtTelephone").val();
		var strFax  = $("#txFax").val();
		var strComment = $("txtComments").val();
		
		var strAirLine = acclaeroClientIdentifier;
		
		strErrorHTML = "";
		$("#txtAgencyName").addClass("clsInput");
		$("#txtLicenseNo").addClass("clsInput");
		$("#txtIATACode").addClass("clsInput");
		$("#selStation").addClass("clsSelect");
		$("#selCountry").addClass("clsSelect");
				
		$("input[name='agent.payMeth']").addClass("NoBorder");
		$("#chkPayMeth").addClass("NoBorder");
		$("#txtAddress").addClass("clsInput");
		$("#txtCity").addClass("clsInput");
		$("#txtState").addClass("clsInput");
		$("#txtPostalCode").addClass("clsInput");
		$("#txtEmail").addClass("clsInput");
		
		$("#txtTelephone").addClass("clsInput");
		$("#txtPCountry").addClass("clsInput");
		$("#txtPArea").addClass("clsInput");
		$("#txFax").addClass("clsInput");
		$("#txtFCountry").addClass("clsInput");
		$("#txtFArea").addClass("clsInput");
		
		$("#txtMobile").addClass("clsInput");
		$("#txtMCountry").addClass("clsInput");
		$("#txtMArea").addClass("clsInput");
		
				
		if (trim($("#txtPCountry").val()) != "") {
			if (validateInteger($("#txtPCountry").val())){
				Util_AgentValidation.removeZero("txtPCountry");
			} else {
				setField("txtPCountry", "");
			}	
		}
		if (trim($("#txtPArea").val()) != "") {
			if (validateInteger($("#txtPArea").val())){
				Util_AgentValidation.removeZero("txtPArea");
			} else {
				setField("txtPArea", "");
			}	
		}
		if (trim($("#txtTelephone").val()) != "") {
			if (validateInteger($("#txtTelephone").val())){
				Util_AgentValidation.removeZero("txtTelephone");
			} else {
				setField("txtTelephone", "");
			}	
		}
		
		if (strAgencyName == "") {
			strErrorHTML += "<li> " +  errorInfo.userAgencyNameRqrd;
			$("#txtAgencyName").addClass("errorControl");
		}
//		if (!isAlphaWhiteSpace(trim(strAgencyName))){
//			strErrorHTML += "<li> " + errorInfo.userAgencyNameRqrd;
//			$("#txtAgencyName").addClass("errorControl fontCapitalize");
//		}
		
		if (strStation == "") {
			strErrorHTML += "<li> " + errorInfo.userStationRqrd;
			$("#selStation").addClass("errorControl");
		}
		
		if (taCo == "TA" && $('#radPayMethBS').attr('checked')) {
			if(strLicenseNo == "") {
				strErrorHTML += "<li> " +  errorInfo.userLicenceNoRqrd;
				$("#radPayMethBS").addClass("errorControl");
			}
			if(strIATACode == "") {
				strErrorHTML += "<li> " +  errorInfo.userIATACodeRqrd;
				$("#radPayMethBS").addClass("errorControl");
		    }
		}
			 
		if (strAddress == "") {
			strErrorHTML += "<li> " +  errorInfo.userAddrRqrd;
			$("#txtAddress").addClass("errorControl");
		}
//		if (!isAlphaWhiteSpace(trim(strAddress))){
//			strErrorHTML += "<li> " + errorInfo.userAddrRqrd;
//			$("#txtAddress").addClass("errorControl fontCapitalize");
//		}
		
		if (strCity == "") {
			strErrorHTML += "<li> " +  errorInfo.userCityRqrd;
			$("#txtCity").addClass("errorControl");
		}
//		if (!isAlphaWhiteSpace(trim(strCity))){
//			strErrorHTML += "<li> " + errorInfo.userCityRqrd;
//			$("#txtCity").addClass("errorControl fontCapitalize");
//		}
	
		if (strCountry == "") {
			strErrorHTML += "<li> " + errorInfo.userCountryRqrd;
			$("#selCountry").addClass("errorControl");
		}
		
		if (strEmail == "") {
			strErrorHTML += "<li> " + errorInfo.userEmailRqrd;
			$("#txtEmail").addClass("errorControl");
		} else if (checkEmail(strEmail) ==false) {
			strErrorHTML += "<li> " + errorInfo.userEmailIncorrect;
			$("#txtEmail").addClass("errorControl");
		}
		
		if (taCo == "TA" && trim($("#txFax").val()) != ""){
			if (!isAlphaNumericWhiteSpace($("#txFax").val())){
				strErrorHTML += "<li> " + raiseErrorMsg(errorInfo.errInvalidChar, "", strMsgFax);
				$("#txFax").addClass("errorControl");
			}
		}
		if (taCo != "CO" && trim($("#txtTelephone").val()) == "") {
			strErrorHTML += "<li> " + errorInfo.userTelephoneRqrd;
			$("#txtTelephone").addClass("errorControl");			
			$("#txtPCountry").addClass("errorControl");
			$("#txtPArea").addClass("errorControl");			
		}	
		
		if (taCo == "CO" && trim($("#txtTelephone").val()) == "" && trim($("#txtMobile").val()) == ""){
			strErrorHTML += "<li> " + errorInfo.userTelephoneRqrd;
			$("#txtTelephone").addClass("errorControl");	
			$("#txtMobile").addClass("errorControl");		
			$("#txtPCountry").addClass("errorControl");
			$("#txtPArea").addClass("errorControl");		
			$("#txtMCountry").addClass("errorControl");
			$("#txtMArea").addClass("errorControl");		
		}
		
		if (trim($("#txtTelephone").val()) != "") {
			if (trim($("#txtPCountry").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userTeleCountryphoneRqrd;
				$("#txtPCountry").addClass("errorControl");	
			}
			
			if (trim($("#txtPArea").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userTeleAreaphoneRqrd;
				$("#txtPArea").addClass("errorControl");	
			}
				
			if (!isAlphaNumericWhiteSpace($("#txtTelephone").val())){
				strErrorHTML += "<li> " + userTelephoneRqrd;
				$("#txtTelephone").addClass("errorControl");	
			}		
		}	
		
		if (taCo == "CO" && trim($("#txtMobile").val()) != ""){		
			if (trim($("#txtMCountry").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userTeleCountryphoneRqrd;
				$("#txtMCountry").addClass("errorControl");	
			}
			
			if (trim($("#txtMArea").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userTeleAreaphoneRqrd;
				$("#txtMArea").addClass("errorControl");	
			}

			if (!isAlphaNumericWhiteSpace(getValue("txtMobile"))){
				strErrorHTML += "<li> " + errorInfo.userTelephoneRqrd;
				setStyleClass("txtMobile","errorControl");	
			}					
		}
		
		if(taCo == "CO" && trim($("#txtMCountry").val()) != "") {
			if (validateInteger($("#txtMCountry").val())){
				Util_AgentValidation.removeZero("txtMCountry");
			}else {
				setField("txtMCountry", "");
			}	
		}
		if(taCo == "CO" && trim($("#txtMArea").val()) != "") {
			if (validateInteger($("#txtMArea").val())){
				Util_AgentValidation.removeZero("txtMArea");
			}else {
				setField("txtMArea", "");
			}	
		}
		if(taCo == "CO" && trim($("#txtMobile").val()) != "") {
			if (validateInteger($("#txtMobile").val())){
				Util_AgentValidation.removeZero("txtMobile");
			}else {
				setField("txtMobile", "");
			}	
		}
				 
		
		
		if (trim(strAgencyName) != ""){
			strChkEmpty = checkInvalidChar(strAgencyName, errorInfo.errInvalidChar, strMsgAgencyName);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#txtAgencyName").addClass("errorControl")
			}	
		}
		
		if (trim(strAddress) != ""){
			strChkEmpty = checkInvalidChar(strAddress, errorInfo.errInvalidChar, strMsgAddress);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#txtAddress").addClass("errorControl")
			}	
		}	
		
		if (trim(strCity) != ""){
			strChkEmpty = checkInvalidChar(strCity, errorInfo.errInvalidChar, strMsgCity);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#txtCity").addClass("errorControl")
			}	
		}
		
		if (strStation != "" && strCountry != "") {
			var chosedCountry = $("#selCountry").val();
			var chosedStation = $("#selStation").val();
			for(var x = 0; x < arrStationCountry.length; x++) {
				if(chosedStation == arrStationCountry[x][0]) {
					if (chosedCountry != arrStationCountry[x][1]) {
						strErrorHTML += "<li> " + errorInfo.stationContryWrongComb;
						$("#selStation").addClass("errorControl");
						$("#selCountry").addClass("errorControl");
					} else {
						break;
					}	
				}
			}
		}
					 
		if(strAirLine == 'AA' || strAirLine == 'GA') {
			if (taCo == "CO") {
				var countryCode = trim($("#txtMCountry").val());
				var areaCode =    trim($("#txtMArea").val());
				var mobileno =    trim($("#txtMobile").val());
				var blnFound = true;
				var hsaNo = false;
				var selectedCountry = trim($("#selCountry").val());	
				if ((selectedCountry != "OT" && selectedCountry != "" ) && mobileno != "" && countryCode != "") {		
					var phoneCC = Util_CustomerValidation.getCCForCountry();
					if(phoneCC != '' && countryCode != phoneCC){
						strErrorHTML += "<li> " + errorInfo.wrongComb;
						$("#txtMCountry").addClass("errorControl");							
					}else {
						for(var pl=0;pl < arrCountryPhone.length;pl++) {
							if(countryCode == arrCountryPhone[pl][0]) {
								hsaNo = true;
								if(arrCountryPhone[pl][1] == 'Y'){
									if(typeof(arrAreaPhone[countryCode]) != 'undefined') {
										for(var al=0;al < arrAreaPhone[countryCode].length; al++){
											if(arrAreaPhone[countryCode][al][0] == areaCode
												&& arrAreaPhone[countryCode][al][1] == 'MOBILE'
												&& arrAreaPhone[countryCode][al][2] == 'ACT') {
												blnFound = true;
												break;									
											} else {
												blnFound = false;
											}
										}								
										if(!blnFound) {
											strErrorHTML += "<li> " + errorInfo.wrongComb;
											$("#txtMCountry").addClass("errorControl");		
											$("#txtMArea").addClass("errorControl");	
										}							
									}						
								}						
								if(arrCountryPhone[pl][2] != ''){
									if(mobileno != "") {
										if(mobileno.length < Number(arrCountryPhone[pl][2])) {
											strErrorHTML += "<li> " + errorInfo.lenSmall;
											$("#txtMobile").addClass("errorControl");								
										}							
									}						
								}						
								if(arrCountryPhone[pl][3] != ''){							
									if(mobileno != "") {
										if(mobileno.length > Number(arrCountryPhone[pl][3])) {
											strErrorHTML += "<li> " + errorInfo.lenLong;
											$("#txtMobile").addClass("errorControl");								
										}							
									}						
								}					
							} 				
						}
						
						if(!hsaNo) {
							strErrorHTML += "<li> " + raiseError("ERR050");
							$("#txtMCountry").addClass("errorControl");		
							$("#txtMArea").addClass("errorControl");	
						}		
					}
				}
			}
			var lancountryCode = trim($("#txtPCountry").val());
			var lanareaCode =    trim($("#txtPArea").val());
			var lanno =    trim($("#txtTelephone").val());
			var blnLanFound = true;
			var hsaLanNo = false;
			var selectedCountry = trim($("#selCountry").val());	
			if ((selectedCountry != "OT" && selectedCountry != "" ) && lanno != "" && lancountryCode != "") {
				var phoneCC = Util_AgentValidation.getCCForCountry();
				if(phoneCC != '' && lancountryCode != phoneCC){
					strErrorHTML += "<li> " + errorInfo.wrongComb;
					$("#txtPCountry").addClass("errorControl");							
				}else {
					for(var pl=0;pl < arrCountryPhone.length;pl++) {
						if(lancountryCode == arrCountryPhone[pl][0]) {
							hsaLanNo = true;
							if(arrCountryPhone[pl][1] == 'Y'){
								if(typeof(arrAreaPhone[lancountryCode]) != 'undefined') {
									for(var al=0;al < arrAreaPhone[lancountryCode].length; al++){
										if(arrAreaPhone[lancountryCode][al][0] == lanareaCode 
											&& arrAreaPhone[lancountryCode][al][1] == 'LAND'
											&& arrAreaPhone[lancountryCode][al][2] == 'ACT') {
											blnLanFound = true;
											break;									
										} else {
											blnLanFound = false;
										}
									}
									
									if(!blnLanFound) {
										strErrorHTML += "<li> " + errorInfo.wrongComb;
										$("#txtPCountry").addClass("errorControl");		
										$("#txtPArea").addClass("errorControl");	
									}
								}
							}
							
							if(arrCountryPhone[pl][2] != ''){
								if(lanno != "") {
									if(lanno.length < Number(arrCountryPhone[pl][2])) {
										strErrorHTML += "<li> " + errorInfo.lenSmall;
										$("#txtTelephone").addClass("errorControl");									
									}
								}
							}
							
							if(arrCountryPhone[pl][3] != ''){
								
								if(lanno != "") {
									if(lanno.length > Number(arrCountryPhone[pl][3])) {
										strErrorHTML += "<li> " + errorInfo.lenLong;
										$("#txtTelephone").addClass("errorControl");									
									}
								}
							}
						} 					
					}
					
					if(!hsaLanNo) {
						strErrorHTML += "<li> " + raiseError("ERR050");
						$("#txtPCountry").addClass("errorControl");		
						$("#txtPArea").addClass("errorControl");	
					}				
				}
			}	
		}	
		
		if (strErrorHTML != ""){ strErrorHTML += "<br><br>";}
		$("#spnError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
		if (taCo == "TA" && strErrorHTML == ""){
			$("#hdnTelNo").val($("#txtPCountry").val() + "-" + $("#txtPArea").val() + "-" + trim($("#txtTelephone").val()));
			$("#hdnFax").val($("#txtFCountry").val() + "-" + $("#txtFArea").val() + "-" + trim($("#txFax").val()));
			return true;
		} else if (taCo == "CO" && strErrorHTML == ""){
			$("#hdnTelNo").val($("#txtPCountry").val() + "-" + $("#txtPArea").val() + "-" + trim($("#txtTelephone").val()));
			$("#hdnMobile").val($("#txtMCountry").val() + "-" + $("#txtMArea").val() + "-" + trim($("#txtMobile").val()));
			return true;
		} else {
			$("#btnRegister").focus();
			return false;
		}
	
		function raiseErrorMsg(strMsg){
			if (arguments.length >1){
				for (var i = 0 ; i < arguments.length - 1 ; i++){
					strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
				}
			}
			return strMsg;
		}
	}

/**
 * Country List Change
 */
Util_AgentValidation.countryListChange = function() {
	$("#txtMCountry").val("");
	$("#txtPCountry").val("");
	var choseCountry = $("#selCountry").val();
	for(var x=0;x<arrCountryPhone.length;x++) {
		if(choseCountry == arrCountryPhone[x][4]) {
			if (taCo == "CO") {
				$("#txtMCountry").val(arrCountryPhone[x][0]);
			}
			$("#txtPCountry").val(arrCountryPhone[x][0]);	
			break;
		}
	}
}
/**
 * Telephone Key Press/UP 
 *
 */
Util_AgentValidation.telephoneNoKeyPress = function(objC) {
	if (!validateInteger(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}
}

/**
 * Remove Zero
 */
Util_AgentValidation.removeZero = function(field) {
	var val = trim($("#"+field));
	$("#" + field, Number(val) * 1);		
}	


/**
 * Add CSS Class
 */
Util_AgentValidation.addClass = function(data) {
	$.each(data.Ids, function(index, value) { 
		$("#" + data).addClass(data.cssClass);		  
	});
}
/**
 * Remove CSS Class
 */
Util_AgentValidation.removeClass = function() {
	$("body").find(".errorControl").each(function( intIndex ) {
		$("#"+ this.id).removeClass("errorControl");
	});
}







