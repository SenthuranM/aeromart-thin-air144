function UI_giftVoucher() {
}

UI_giftVoucher.templateList = null;
UI_giftVoucher.templateListSalesPeriod = null;
UI_giftVoucher.showPaymentIcons = false;
UI_giftVoucher.paymentMethods = [];
UI_giftVoucher.objPGS = null;
UI_giftVoucher.isCardPanelBuild = false;
UI_giftVoucher.brokerType = null;
UI_giftVoucher.paymentGatewayID = null;
UI_giftVoucher.paymentGatewayTypeCode = null;
UI_giftVoucher.selectedPaymentMethodValStr = null;
UI_giftVoucher.systemDate = null;

var voucher = null;

var strSelectEmpty = "<option value=''> </option>";

UI_giftVoucher.paymentGatewayID = null;
UI_giftVoucher.paymentGatewayProviderCd = null;

$(document).ready(function() {
	UI_giftVoucher.ready();
});

UI_giftVoucher.ready = function() {
	$("#paxFirstName").alphaWhiteSpace();
	$("#paxLastName").alphaWhiteSpace();
	$("#divCardInfoPannel").hide();
	$("#btnPurchase").hide();
	$("#btnEmail").hide();
	$("#btnPrint").hide();
	$("#lblTotalDue").text("Total Due Amount");

	UI_giftVoucher.getGiftVoucherScreenDetails();

	$("#voucherName").change(function() {
		UI_giftVoucher.populateVoucherTemplate();
	});

	$("#btnBuy").click(function() {
		if (UI_giftVoucher.validate()) {
			UI_giftVoucher.displayPaymentPannel();
			$("#postPaymentSuccess").hide();
			$("#postPaymentDetails").hide();
			$("#btnEmail").hide();
			$("#btnPrint").hide();
		}
	});

	$("#btnReset").click(function() {
		UI_giftVoucher.resetPaxDetails();
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#postPaymentSuccess").hide();
		$("#postPaymentDetails").hide();
		$("#emailSuccess").hide();
		$("#btnPurchase").hide();
	});

	$("#expiryMonth").change(function() {
		UI_giftVoucher.expiryDateChange();
	});
	$("#expiryYear").change(function() {
		UI_giftVoucher.expiryDateChange();
	});
	$("#whatisthis").click(function() {
		UI_giftVoucher.displayCCD($(this));
	});
	$("#btnPurchase").click(function() {
		UI_giftVoucher.ccPayment();
	});
}

UI_giftVoucher.displayPaymentPannel = function() {
	var data = {};
	var template = JSON.parse($("#voucherName").val());
	var amountInLocal = template.amountInLocal;
	var amountInBase = template.amount;
	var currencyCode = template.currencyCode;
	data["currencyCode"] = currencyCode;
	data["amountInBase"] = amountInBase;
	data["amountInLocal"] = amountInLocal;
	$.ajax({
		url : 'showVoucherPaymentDetails.action',
		dataType : "json",
		data : data,
		type : "POST",
		beforeSend : showProgress(),
		success : UI_giftVoucher.voucherPaymentDetailSuccess
	});
}

UI_giftVoucher.getGiftVoucherScreenDetails = function() {
	showProgress();
	$.ajax({
		url : 'showVoucherDetails.action',
		dataType : "json",
		type : "POST",
		success : UI_giftVoucher.voucherDetailSuccess
	});
}

UI_giftVoucher.voucherDetailSuccess = function(response) {
	hideProgress();
	UI_giftVoucher.templateList = response.voucherTemplateList;
	UI_giftVoucher.populateTemplateDropDown(UI_giftVoucher.templateList);
	UI_giftVoucher.paymentMethods = response.paymentMethods;
	UI_giftVoucher.showPaymentIcons = response.showPaymentIcons;
	UI_giftVoucher.systemDate = response.systemDate;
	$("#lblYourIP").text(response.userIp);
	try {
		eval(response.errorList);
	} catch (err) {
		console.log("eval() error ==> errorList");
	}
}

UI_giftVoucher.populateTemplateDropDown = function(templates) {
	var voucherTemplates = "";
	voucherTemplates += "<option value = '' ></option>";
	if (templates != null) {
		for (i = 0; i < templates.length; i++) {
			voucherTemplates += "<option value = '"
					+ JSON.stringify(templates[i]) + "' >"
					+ templates[i].voucherName + "</option>";
		}
	}
	$('#voucherName').empty();
	$('#voucherName').append(voucherTemplates);
}

UI_giftVoucher.populateVoucherTemplate = function() {
	var template = JSON.parse($("#voucherName").val());
	var validFrom = new Date();
	var validTo = new Date(validFrom.getTime() + template.voucherValidity * 24 * 3600 * 1000);
	$("#amountInLocal").val(template.amountInLocal);
	$("#currencyCode").val(template.currencyCode);
	$("#validFrom").val(validFrom.getDate() + "/"+validFrom.getMonth() + "/" + validFrom.getFullYear());
	$("#validTo").val(validTo.getDate() + "/"+validTo.getMonth() + "/" + validTo.getFullYear());
	UI_giftVoucher.formDissable();
	$("#divCardInfoPannel").hide();
	// $("#divCardInfoPannel").show();
}

UI_giftVoucher.voucherPaymentDetailSuccess = function(response) {
	hideProgress();
	var amountInLocal = $("#amountInLocal").val();
	var currencyCode = $("#currencyCode").val();
	var ccTxnFee = response.ccTxnFee;
	var fullPayment = response.fullPayment;
	$("#lblTnxAmount").text(ccTxnFee + " " + currencyCode);
	$("#lblTotalDueAmount").text(fullPayment + " " + currencyCode);
	$("#lblVoucherAmount").text(amountInLocal + " " + currencyCode);
	$("#divTotalAmount").show();
	$("#hdnTnxFee").val(ccTxnFee);
	UI_giftVoucher.paymentGatewayID = response.paymentGatewayID;
	UI_giftVoucher.objPGS = response.paymentGateways;
	UI_giftVoucher.buildCardInitPannel();
	if (UI_giftVoucher.objPGS != null) {
		// Build Payment Card List
		UI_giftVoucher.buildPaymentCardListPannel(response.cardsInfo);
	} else {
		$("#divCardInfoPannel").hide();
	}
	try {
		eval(response.errorList);
	} catch (err) {
		console.log("eval() error ==> errorList");
	}
}

UI_giftVoucher.formDissable = function() {
	$("#amountInLocal").prop('disabled', true);
	$("#currencyCode").prop('disabled', true);
	$("#validFrom").prop('disabled', true);
	$("#validTo").prop('disabled', true);
}

UI_giftVoucher.resetPaxDetails = function() {
	$("#paxFirstName").val("");
	$("#paxLastName").val("");
	$("#email").val("");
	$("#remarks").val("");
	$("#divCardInfoPannel").hide();
	$("#divTotalAmount").hide();
}

UI_giftVoucher.resetVoucherDetails = function() {
	$('#voucherName').val("");
	$("#amountInLocal").val("");
	$("#currencyCode").val("");
	$("#validFrom").val("");
	$("#validTo").val("");

}

UI_giftVoucher.validate = function() {
	if (trim($("#voucherName").val()) == "") {
		jAlert("Please select a Gift Voucher!");
		return false;
	} else if (trim($("#paxFirstName").val()) == "") {
		jAlert("Enter the First Name!");
		return false;
	} else if (trim($("#paxLastName").val()) == "") {
		jAlert("Enter the Last Name!");
		return false;
	} else if (trim($("#email").val()) == "") {
		jAlert("Enter the email address!");
		return false;
	} else if (!checkEmail(trim($("#email").val()))) {
		jAlert("Enter a valid email address!");
		return false;
	} else {
		return true;
	}
}

UI_giftVoucher.formatDate = function(value) {
	var hipDate = value.substr(0, 10);
	return hipDate.replace("-", "/")
}

UI_giftVoucher.buildPaymentCardListPannel = function(cardsInfo) {

	if (cardsInfo == null || cardsInfo == "" || cardsInfo.length == 0) {
		jAlert("Can not find payment method. Invalid payment gateway configuration. Please contact call center or administrator");
		return;
	}
	setClassName = function(className) {
		var send = className.toUpperCase();
		// TODO Re-factor
		if (className.toUpperCase().indexOf("MASTER") > -1) {
			send = "MASTER";
		} else if (className.toUpperCase().indexOf("VISA") > -1) {
			send = "VISA";
		} else if (className.toUpperCase().indexOf("AMEX") > -1) {
			send = "AMEX";
		} else if (className.toUpperCase().indexOf("CMI") > -1) {
			send = "CMI";
		} else if (className.toUpperCase().indexOf("MTC") > -1) {
			send = "MTC";
		} else if (className.toUpperCase() === "CASH") {
			send = "CASH";
		} else if (className.toUpperCase().indexOf("PAYPAL") > -1) {
			send = "PAYPAL";
		} else if (className.toUpperCase().indexOf("BANCONTACT") > -1) {
			send = "BANCONTACT";
		} else if (className.toUpperCase().indexOf("BLEUE") > -1) {
			send = "BLEUE";
		} else if (className.toUpperCase().indexOf("GIROPAY") > -1) {
			send = "GIROPAY";
		} else if (className.toUpperCase().indexOf("IDEAL") > -1) {
			send = "IDEAL";
		} else if (className.toUpperCase().indexOf("SOFORT") > -1) {
			send = "SOFORT";
		} else if (className.toUpperCase() === "DEBIT") {
			send = "DEBIT";
		} else if (className.toUpperCase().indexOf("JCB") > -1) {
			send = "JCB";
		} else if (className.toUpperCase().indexOf("KNET") > -1) {
			send = "KNET";
		} else if (className.toUpperCase().indexOf("MOBICASH") > -1) {
			send = "MOBICASH";
		} else if (className.toUpperCase().indexOf("FATOURATI") > -1) {
			send = "FATOURATI";
		} else if (className.toUpperCase().indexOf("CANAUX1") > -1) {
			send = "CANAUX1";
		} else if (className.toUpperCase().indexOf("CANAUX2") > -1) {
			send = "CANAUX2";
		} else if (className.toUpperCase().indexOf("QIWI_OFFLINE") > -1) {
			send = "QIWI_OFFLINE";
		} else if (className.toUpperCase().indexOf("QIWI") > -1) {
			send = "QIWI";
		} else if (className.toUpperCase() === "CCAVENUE_CREDIT") {
			send = "CCAVENUE_CREDIT";
		} else if (className.toUpperCase() === "CCAVENUE_DEBIT") {
			send = "CCAVENUE_DEBIT";
		} else if (className.toUpperCase() === "CCAVENUE_NET") {
			send = "CCAVENUE_NET";
		}

		// TODO to implement all other card names to match with the card images
		return send;
	};

	if (UI_giftVoucher.isCardPanelBuild) {
		var cardImage = "";
		var cardCss = "";
		$("#listCard").html("")
		for (var y = 0; y < cardsInfo.length; y++) {
			cardCss = setClassName(cardsInfo[y].cssClassName);
			if (cardCss != "") {
				cardImage = "<span class='" + cardCss + "'></span>";
			} else {
				cardImage = "";
			}

			if (cardsInfo[y].modicifationAllowed) {

				$("#listCard").append("<li style='display: inline;'><div class='card-img'>"
					+ cardImage
					+ "</div><span class='card-desc'>"
					+ "<input type='RADIO' class='noBorder' name='cardRadio' id='cardRadio_"
					+ cardsInfo[y].cardType + "' value='"
					+ cardsInfo[y].cardType + ":"
					+ cardsInfo[y].paymentGateWayID + ":"
					+ cardsInfo[y].behaviour + ":"
					+ cardsInfo[y].cardDisplayName + "'>"
					+ "<label class='fntBold'> "
					+ cardsInfo[y].cardDisplayName
					+ "</label></span></li>");
			}

		}
		;

		$('input[name="cardRadio"]').click(function(e) {
			UI_giftVoucher.paymentMethodChange();
		});
	}
	UI_giftVoucher.buildCardDetailPanel();
}

UI_giftVoucher.expiryDateChange = function() {
	$("#expiryDate").val($("#expiryMonth").val() + $("#expiryYear").val());
}

UI_giftVoucher.paymentMethodChange = function() {

	var element = $('input:radio[name=cardRadio]:checked');
	var selectCardVal = element.val();
	UI_giftVoucher.selectedPaymentMethodValStr = selectCardVal;

	if (selectCardVal == null || selectCardVal == "") {
		alert("Can not process payment");
		return;
	}
	var selectCardValArr = selectCardVal.split(":");
	var cardTypeID = selectCardValArr[0];
	var paymentGatewayID = selectCardValArr[1];
	var paymentGatewayMode = selectCardValArr[2];
	var paymentMethod = selectCardValArr[3];

	UI_giftVoucher.brokerType = paymentGatewayMode;
	if (paymentGatewayID == "null") {
		UI_giftVoucher.paymentGatewayID = null;
	} else {
		$("#paymentType").val('NORMAL');
		UI_giftVoucher.paymentGatewayID = paymentGatewayID;
		UI_giftVoucher.paymentGatewayTypeCode = paymentMethod;
	}
	$("#cardType").val(cardTypeID);
	$("#paymentMethod").val(paymentMethod);

	UI_giftVoucher.pgSelected();

}

UI_giftVoucher.pgSelected = function() {
	var pgId = UI_giftVoucher.paymentGatewayID;
	if (pgId != null) {
		for (var pl = 0; pl < UI_giftVoucher.objPGS.length; pl++) {
			if (UI_giftVoucher.objPGS[pl].paymentGateway == pgId) {
				UI_giftVoucher.fillPaymentGW(UI_giftVoucher.objPGS[pl]);
				break;
			}
		}
	}
}

UI_giftVoucher.fillPaymentGW = function(pg) {
	// TODO need to change the pg according to the selected pg
	UI_giftVoucher.buildCardDetailPanel(pg);
	if (pg.payCurrency != null) {
		$("#lblTotalDueAmount").text(pg.payCurrencyAmount);
		$("input[name=valBaseCurrCode]").text(pg.payCurrency);
		$("#lblTotalDueAmount").text(
				pg.payCurrencyAmount + " " + pg.payCurrency);
		// $("#valBaseCurrCode").text(pg.payCurrencyAmount);
		$("#payCurrency").val(pg.payCurrency);
		$("#providerName").val(pg.providerName);
		$("#providerCode").val(pg.providerCode);
		$("#description").val(pg.description);
		$("#paymentGateway").val(pg.paymentGateway);
		$("#payCurrencyAmount").val(pg.payCurrencyAmount);
		$("#brokerType").val(UI_giftVoucher.brokerType);
		$("#switchToExternalURL").val(pg.switchToExternalURL);
		$("#viewPaymentInIframe").val(pg.viewPaymentInIframe);

		if (pg.switchToExternalURL) {
			UI_Container.useIframeInPostingParams = false;
		}
	}
}

UI_giftVoucher.buildCardDetailPanel = function(paymentGatyeway) {
	$("#divCardInfoPannel").show();
	if (UI_giftVoucher.brokerType == "internal-external") {
		$("#cardDetailPannel").show();
		// UI_giftVoucher.clearCardDetails();
		UI_giftVoucher.paymentGatewayProviderCd = paymentGatyeway.providerCode;
		$("#cardTypePics").empty();
		$("#cardType").focus();
		$("#btnPurchase").show();
		if ($("#cardType").val() != '6') {
			$("#extraCardDetailForPayPalPannel").hide();
		} else {
			$("#extraCardDetailForPayPalPannel").show();
		}
	} else if (UI_giftVoucher.brokerType == "external") {
		$("#paymentInfoPanel").show();
		$("#cardDetailPannel").hide();
	}
}

UI_giftVoucher.buildCardInitPannel = function() {
	if (!UI_giftVoucher.isCardPanelBuild) {
		UI_giftVoucher.buildMonths();
		UI_giftVoucher.buildYears();
		UI_giftVoucher.isCardPanelBuild = true;
	}
	$("#cardDetailPannel").hide();
}

UI_giftVoucher.buildMonths = function() {
	$("#expiryMonth").empty();
	var strMonths = strSelectEmpty;

	for (var i = 1; i <= 12; i++) {
		if (i < 10) {
			strMonths += "<option value='0" + i + "'> 0" + i + " </option>";
		} else {
			strMonths += "<option value='" + i + "'> " + i + " </option>";
		}
	}

	$("#expiryMonth").append(strMonths);
}

UI_giftVoucher.buildYears = function() {
	$("#expiryYear").empty();
	var strYears = strSelectEmpty;
	var strSystemYear = UI_giftVoucher.systemDate.split("/")[2];

	if (strSystemYear == null || strSystemYear == "") {
		strSystemYear = (new Date()).getFullYear();
	}

	for (var i = Number(strSystemYear); i < Number(strSystemYear) + 15; i++) {
		strYears += "<option value='" + i.toString().substring(2) + "'> " + i
				+ " </option>";
	}

	$("#expiryYear").append(strYears);
}

UI_giftVoucher.displayCCD = function(obj) {
	colseThisPopup = function() {
		UI_commonSystem.readOnly(false);
		$(".popuploader").hide();
	};
	colseThisPopup();
	UI_commonSystem.readOnly(true);
	var posobj = obj.position();
	$(".popuploader").css({
		"position" : "absolute",
		"z-index" : "1900",
		"top" : posobj.top - 260,
		"left" : posobj.left + 60
	});
	$(".closethisp").click(function() {
		colseThisPopup();
	});
	if ($("#cardType").val() != 3)
		$(".popupbody").html('<img src="../images/allCards_no_cache.gif" />');
	else
		$(".popupbody").html('<img src="../images/AmexCards_no_cache.gif" />');
	$(".popuploader").show();
	$("#page-mask").click(function() {
		colseThisPopup();
	});
}

UI_giftVoucher.ccPayment = function() {
	showProgress();
	if (UI_giftVoucher.validateCcDetails()) {

		var template = JSON.parse($("#voucherName").val());

		var cardHoldersName = trim($("#cardHoldersName").val());
		var cardNumber = trim($("#cardNumber").val());
		var expiryMonth = trim($("#expiryMonth").val());
		var expiryYear = trim($("#expiryYear").val());
		var cardCVV = trim($("#cardCVV").val());

		$("#camountInLocal").val(template.amountInLocal);
		$("#templateId").val(template.voucherId);
		$("#ccardNumber").val(cardNumber);
		$("#ccardHoldersName").val(cardHoldersName);
		$("#cexpiryDate").val(expiryMonth + expiryYear.slice(-2));
		$("#ccardCVV").val(cardCVV);
		$("#ccardTypedata").val($('input:radio[name=cardRadio]:checked').val().split(':')[0]);
		$("#cpaymentMethod").val($('input:radio[name=cardRadio]:checked').val().split(':')[3]);		
		$("#paymentGateway").val(UI_giftVoucher.paymentGatewayID);
		$("#vcardType").val($('input:radio[name=cardRadio]:checked').val().split(':')[0]);
		$("#vcardNumber").val(cardNumber);
		$("#vcardExpiry").val(expiryMonth + expiryYear.slice(-2));
		$("#vcardCVV").val(cardCVV);
		$("#vcardHolderName").val(cardHoldersName);
		$("#vpaymentMethod").val("CC");
		$("#vamountLocal").val(template.amountInLocal);
		$("#vcardTxnFeeLocal").val($("#hdnTnxFee").val());
		$("#vcurrencyCode").val(template.currencyCode);
		$("#vipgId").val($('input:radio[name=cardRadio]:checked').val().split(':')[1]);
		$("#vvalidFrom").val($("#validFrom").val());
		$("#vvalidTo").val($("#validTo").val());
		$("#vpaymentMethod").val("CC");
	    $("#frmVoucherPurchase").attr('action', 'issueGiftVoucher.action');
	    $("#frmVoucherPurchase").attr('method', 'POST');
	    $("#frmVoucherPurchase").submit();
	}
}

UI_giftVoucher.voucherSaveSuccess = function(response) {

	if (response.redirectUrl != null) {
		window.location.href = "/ibe" + response.redirectUrl;
	}
	hideProgress();
	voucher = response.voucherDTO;
	$("#divTotalAmount").hide();
	$("#divCardInfoPannel").hide();
	$("#postPaymentSuccess").show();
	$("#lblPostPaymentDetails").text(
			"Voucher ID : " + voucher.voucherId + "   Amount : "
					+ voucher.amountInLocal + " " + voucher.currencyCode
					+ "  Name : " + voucher.paxFirstName + " "
					+ voucher.paxLastName + "  Valid From : "
					+ voucher.validFrom + "  Valid To : " + voucher.validTo);
	$("#postPaymentDetails").show();
	$("#emailSuccess").hide();

	$("#btnEmail").show();
	$("#btnPrint").show();
	$("#btnPurchase").hide();
	UI_giftVoucher.resetPaxDetails();
	UI_giftVoucher.resetCardDetails();
	UI_giftVoucher.resetVoucherDetails();

}


UI_giftVoucher.validateCcDetails = function() {
	var cardType = $('input:radio[name=cardRadio]:checked').val().split(':')[0];
	var cardNumber = trim($("#cardNumber").val());
	if (trim($("#cardHoldersName").val()) == "") {
		jAlert("Please Enter the card holder's name!");
		return false;
	} else if (cardNumber == "") {
		jAlert("Please Enter the card number!");
		return false;
	} else if (!ValidateCardNos(cardType, cardNumber)) {
		jAlert("Please Enter a valid card number!");
		return false;
	} else if (trim($("#expiryMonth").val()) == "") {
		jAlert("Please select expiry month!");
		return false;
	} else if (trim($("#expiryYear").val()) == "") {
		jAlert("Please select expiry year!");
		return false;
	} else if (trim($("#cardCVV").val()) == "") {
		jAlert("Please enter the security code!");
		return false;
	} else if (!UI_giftVoucher.validateCvvCode(cardType, $("#cardCVV").val())) {
		jAlert("Please enter correct security code!");
		return false;
	} else {
		return true;
	}
}

UI_giftVoucher.resetCardDetails = function() {
	$("#cardHoldersName").val("");
	$("#cardNumber").val("");
	$("#expiryMonth").val("");
	$("#expiryYear").val("");
	$("#cardCVV").val("");
}

UI_giftVoucher.validateCvvCode = function(cardType, cvvCode) {

	var digits = 0;
	// TODO Set Card Type
	switch (cardType) {
	case '1': // MASTERCARD
	case 'EUROCARD': // EUROCARD
	case 'EUROCARD/MASTERCARD':
	case '2': // VISA
	case '5': // CB
	case 'DISCOVER':
	case '6': // PAYPAL
		digits = 3;
		break;
	case '3':
	case 'AMERICANEXPRESS':
	case 'AMERICAN EXPRESS':
		digits = 4;
		break;
	default:
		return false;
	}

	var regExp = new RegExp('[0-9]{' + digits + '}');
	return (cvvCode.length == digits && regExp.test(cvvCode))
}

showProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").show();
}

hideProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").hide();
}
