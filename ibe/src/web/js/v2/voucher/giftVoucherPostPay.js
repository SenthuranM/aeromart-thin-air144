function UI_giftVoucherPostPay() {
}

var voucher = null;

$(document).ready(function() {
	UI_giftVoucherPostPay.ready();
});

UI_giftVoucherPostPay.ready = function () {
	$("#btnEmailConf").click(function() {
		UI_giftVoucherPostPay.email();
	});
	$("#btnPrintConf").click(function() {
		UI_giftVoucherPostPay.print();
	});
	UI_giftVoucherPostPay.getVoucherInfo();
}

UI_giftVoucherPostPay.getVoucherInfo = function() {
	var data = {};
	data['getVoucherDTO'] = true; 
	$.ajax({
		url : 'issueVoucherConfirmation.action',
		dataType : "json",
		data : data,
		type : "POST",
		beforeSend : showProgress(),
		success : function(response) {
			voucher = response.voucherDTO;
			$("#lblVoucherMessage").text(response.message);
		},
	});
}

UI_giftVoucherPostPay.email = function() {

	if (voucher != null) {
		var data = {};
		data["voucherDTO.validFrom"] = voucher.validFrom;
		data["voucherDTO.validTo"] = voucher.validTo;
		data["voucherDTO.amountInLocal"] = voucher.amountInLocal;
		data["voucherDTO.currencyCode"] = voucher.currencyCode;
		data["voucherDTO.paxFirstName"] = voucher.paxFirstName;
		data["voucherDTO.paxLastName"] = voucher.paxLastName;
		data["voucherDTO.email"] = voucher.email;
		data["voucherDTO.voucherId"] = voucher.voucherId;
		data["voucherDTO.templateId"] = voucher.templateId;
		
		$.ajax({
			url : 'emailVoucherIBE.action',
			dataType : "json",
			data : data,
			type : "POST",
			beforeSend : showProgress(),
			success : UI_giftVoucherPostPay.voucherEmailSuccess
		});
	}
}

UI_giftVoucherPostPay.voucherEmailSuccess = function() {
	hideProgress();
//	$("#postPaymentSuccess").hide();
//	$("#postPaymentDetails").hide();
//	$("#emailSuccess").show();
}

UI_giftVoucherPostPay.print = function() {
	if (voucher != null) {
		var validFrom = voucher.validFrom;
		var validTo = voucher.validTo;
		var amountInLocal = voucher.amountInLocal;
		var currencyCode = voucher.currencyCode;
		var paxFirstName = voucher.paxFirstName;
		var paxLastName = voucher.paxLastName;
		var voucherId = voucher.voucherId;
		var templateId = voucher.templateId;

		var strUrl = "printVoucherIBE.action" + "?validFrom=" + validFrom
				+ "&validTo=" + validTo + "&amountInLocal=" + amountInLocal
				+ "&currencyCode=" + currencyCode + "&paxFirstName="
				+ paxFirstName + "&paxLastName=" + paxLastName + "&voucherId="
				+ voucherId + "&templateId=" + templateId;
		showProgress();
		top.objCW = window.open(strUrl, "myWindow", $("#popWindow")
				.getPopWindowProp(630, 900, true));
		if (top.objCW) {
			top.objCW.focus()
		}
		UI_giftVoucherPostPay.voucherPrintSuccess();
	}

}

UI_giftVoucherPostPay.voucherPrintSuccess = function() {
	hideProgress();
//	$("#postPaymentSuccess").hide();
//	$("#postPaymentDetails").hide();
//	$("#emailSuccess").hide();
}

showProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").show();
}

hideProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").hide();
}