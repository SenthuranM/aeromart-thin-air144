/**
 * Author : Pradeep Karunanayake Avoid conflict with Reservation modify, use
 * form ID
 * 
 */
function UI_ProfileUpdate() {}

var arrPaxTitle = new Array();
var arrError = new Array();
var arrCountryPhone = new Array();
var arrAreaPhone = new Array();
var errorInfo = "";
UI_ProfileUpdate.customer = null;
UI_ProfileUpdate.lmsDetails = null;
UI_ProfileUpdate.isPageLoaded = false;
UI_ProfileUpdate.profileUpdateInProgress = false;
/* Meal data structures */
UI_ProfileUpdate.jsMealModel = null;
/* Meal data structures */
UI_ProfileUpdate.jsMealCategoryModel = null;

UI_ProfileUpdate.selectedFamilyMemberId = "";

UI_ProfileUpdate.nationalityList;

UI_ProfileUpdate.relationshipList;

/**
 * Page Ready
 * 
 */
UI_ProfileUpdate.ready = function() {
    $("#trMsg").hide();
    $("#btnUpdate").click(function() {
        if (!UI_ProfileUpdate.profileUpdateInProgress) {
            UI_ProfileUpdate.profileUpdateInProgress = true;
            UI_ProfileUpdate.UpdateButtonClick();
        }
    });
    $("#btnCancel").click(function() {
        UI_UserTabs.myReservationsClick();
    });
    // $("#imgCalendar").click(function(event){Util_Calendar.loadCalendarDOB(event)});

    $("#imgCalendar").hide();
    $("#txtDateOfBirth").datepicker({
        regional: SYS_IBECommonParam.locale,
        dateFormat: "dd/mm/yy",
        maxDate: +0,
        showOn: 'both',
        yearRange: '-80:+10',
        buttonImage: globalConfig.calendaImagePath,
        buttonImageOnly: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true
    });

    $("#selCountry").change(function() {
        Util_CustomerValidation.countryListChange()
    });
    $("#txtTelephone").change(function() {
        Util_CustomerValidation.countryListChange()
    });
    $("#txtDateOfBirth").blur(function() {
        Util_CustomerValidation.validateDOB(this);
    });
    $("#txtPCountry").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtPArea").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtTelephone").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtMCountry").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtMArea").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtMobile").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtOCountry").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtOArea").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtOfficeTel").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtFCountry").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtFArea").keydown(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtEmgnPCountry").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtEmgnPArea").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtEmgnTelephone").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });

    $("#txtPCountry").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtPArea").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtTelephone").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtMCountry").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtMArea").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtMobile").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtOCountry").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtOArea").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtOfficeTel").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtFCountry").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtFArea").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtEmgnPCountry").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtEmgnPArea").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    $("#txtEmgnTelephone").keyup(function() {
        Util_CustomerValidation.telephoneNoKeyPress(this);
    });
    UI_commonSystem.loadingCompleted();

    UI_ProfileUpdate.loadCustomerProfilePageData();

    $("#txtEmail").change(function(event) {
        UI_commonSystem.pageOnChange();
        if (UI_RegCustomer.isBecomeLmsUser)
            $("#txtEmailLMS").val(this.value);
    });

    $("#txtFirstName").change(function(event) {
        UI_commonSystem.pageOnChange();
        if (UI_RegCustomer.isBecomeLmsUser)
            $("#txtNameLMS").val(this.value + " " + $("#txtLastName").val());
    });

    $("#txtLastName").change(function(event) {
        UI_commonSystem.pageOnChange();
        if (UI_RegCustomer.isBecomeLmsUser)
            $("#txtNameLMS").val($("#txtFirstName").val() + " " + this.value);
    });

    $("#txtMCountry").change(function(event) {
        UI_commonSystem.pageOnChange();
        if (UI_RegCustomer.isBecomeLmsUser)
            $("#txtMCountryLMS").val(this.value);
    });

    $("#txtMArea").change(function(event) {
        UI_commonSystem.pageOnChange();
        if (UI_RegCustomer.isBecomeLmsUser)
            $("#txtMAreaLMS").val(this.value);
    });

    $("#txtMobile").change(function(event) {
        UI_commonSystem.pageOnChange();
        if (UI_RegCustomer.isBecomeLmsUser)
            $("#txtMobileLMS").val(this.value);
    });

    $("#txtMCountryLMS").numeric();
    $("#txtMobileLMS").numeric();
    $("#trNameLMS").keypress(function(e) {
        if (e.charCode != 0) {
            var regex = new RegExp("^[a-zA-Z\\-\\s]+$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (!regex.test(key)) {
                e.preventDefault();
                return false;
            }
        }
    });
    $("#txtPpnLMS").alphaNumeric();
    if (UI_commonSystem.lmsDetails == null || UI_commonSystem.lmsDetails.emailId == null || UI_commonSystem.lmsDetails.emailId == "") {
        $("#tblLmsDetails").hide();
    } else {
        $("#tblLmsDetails").show();
    }

    $("#selectedMealsList").hide();

    $("#FMDOB").datepicker({
        regional: SYS_IBECommonParam.locale,
        dateFormat: "dd/mm/yy",
        showOn: 'both',
        yearRange: '-80:+0',
        buttonImage: globalConfig.calendaImagePath,
        buttonImageOnly: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true
    });

    UI_ProfileUpdate.disableFamilyMemberFields();

    $("#FMDOB").blur(function() {
        Util_CustomerValidation.validateDOB(this);
    });

    $("#btnAddFamilyMember").click(function() {
        UI_ProfileUpdate.clearFamilyMemberFields();
        UI_ProfileUpdate.enableFamilyMemberFields();
        UI_ProfileUpdate.rowSeperatorFamilyMembers();
        $("#btnEditFamilyMember").prop('disabled', true);
        $("#btnRemoveFamilyMember").prop('disabled', true);
    });
    $("#btnEditFamilyMember").click(function() {
        UI_ProfileUpdate.enableFamilyMemberFields();
    });
    $("#btnRemoveFamilyMember").click(function() {
        UI_ProfileUpdate.removeFamilyMember();
    });
    $("#btnSaveFamilyMember").click(function() {
        UI_ProfileUpdate.saveEditFamilyMember();
    });

    $("#btnEditFamilyMember").prop('disabled', true);
    $("#btnRemoveFamilyMember").prop('disabled', true);

}

/**
 * Page Clear
 */
UI_ProfileUpdate.pageClear = function() {
        UI_ProfileUpdate.removeClass();
        $("#spnErrorRes").html("");
    }
    /**
     * Update Button Click
     */
UI_ProfileUpdate.UpdateButtonClick = function() {
    if (!Util_CustomerValidation.validateFormData(false, true)) {
        UI_ProfileUpdate.profileUpdateInProgress = false;
        return;
    }
    if (trim($("#txtTelephone").val()) == "") {
        $("#txtPCountry").val("");
        $("#txtPArea").val("");
    }

    if (trim($("#txtMobile").val()) == "") {
        $("#txtMCountry").val("");
        $("#txtMArea").val("");
    }

    if (trim($("#txtOfficeTel").val()) == "") {
        $("#txtOCountry").val("");
        $("#txtOArea").val("");
    }

    if (trim($("#txFax").val()) == "") {
        $("#txtFCountry").val("");
        $("#txtFArea").val("");
    }

    if (trim($("#txtEmgnTelephone").val()) == "") {
        $("#txtEmgnPCountry").val("");
        $("#txtEmgnPArea").val("");
    }

    var selectedMealsList = [];
    $('[class="selectedMeals"]').each(function(index, elem) {
        selectedMealsList.push(elem.value);
    });

    $("#selectedMeals").val(selectedMealsList);

    $("#frmCutUpdate").attr('action', 'customerProfileUpdate.action');
    $("#frmCutUpdate").ajaxSubmit({
        dataType: 'json',
        beforeSubmit: UI_commonSystem.loadingProgress,
        success: UI_ProfileUpdate.profileUpdateSuccess,
        error: UI_commonSystem.setErrorStatus
    });
    return false;
}

/**
 * Request data Success of profile update
 */
UI_ProfileUpdate.profileUpdateSuccess = function(response) {
        if (response != null && response.success == true) {
            $("#profilePage").hide();
            $("#trMsg").show();
            $("#linkFocus").focus();
        } else {
            UI_commonSystem.loadErrorPage({
                messageTxt: response.messageTxt
            });
        }
        UI_ProfileUpdate.profileUpdateInProgress = false;
        UI_commonSystem.loadingCompleted();
    }
    /**
     * Show profile page
     */
UI_ProfileUpdate.showProfilePage = function() {
    $("#profilePage").show();
    $("#trMsg").hide();
}

/**
 * Request Profile data
 */
UI_ProfileUpdate.loadCustomerProfilePageData = function() {
        $("#spnError").html("");
        $("#tdUpdateProfile").ajaxSubmit({
            url: "showCustomerProfileDetail.action",
            type: "POST",
            dataType: 'json',
            beforeSubmit: UI_commonSystem.loadingProgress,
            success: UI_ProfileUpdate.loadCustomerProfilePageDataSuccess,
            error: UI_commonSystem.setErrorStatus
        });
        return false;
    }
    /**
     * 
     * Request data success
     */
UI_ProfileUpdate.loadCustomerProfilePageDataSuccess = function(response) {
        if (response != null && response.success == true) {
            if (UI_ProfileUpdate.isPageLoaded == false) {
                $("#divLoadBg").populateLanguage({
                    messageList: response.jsonLabel
                });
                $("#lblInfoAgree3").html(response.jsonLabel.lblInfoAgree3);
                // $("#lblInfoAgree3").html(response.jsonLabel.lblInfoAgree3).append(response.jsonLabel.lblInfoAgree4);
                UI_ProfileUpdate.policyLinkSetup();

                eval(response.titleInfo);
                Util_CustomerValidation.buildTelephoneInfo(
                    response.countryPhoneInfo, response.areaPhoneInfo);
                errorInfo = response.errorInfo;

                $("#selCountry")
                    .fillFirstOption(response.jsonLabel.lblPleaseSelect);
                $("#selCountry").fillDropDown({
                    dataArray: response.countryInfo,
                    keyIndex: 0,
                    valueIndex: 1,
                    firstEmpty: false
                });
                $("#selSecQ").fillDropDown({
                    dataArray: response.questionInfo,
                    keyIndex: 0,
                    valueIndex: 1,
                    firstEmpty: false
                });
                $("#selNationality").fillFirstOption(
                    response.jsonLabel.lblPleaseSelect);
                $("#selNationality").fillDropDown({
                    dataArray: response.nationalityInfo,
                    keyIndex: 0,
                    valueIndex: 1,
                    firstEmpty: false
                });
                $("#selTitle").fillDropDown({
                    dataArray: arrPaxTitle,
                    keyIndex: 0,
                    valueIndex: 1,
                    firstEmpty: true
                });
                $("#selEmgnTitle").fillDropDown({
                    dataArray: arrPaxTitle,
                    keyIndex: 0,
                    valueIndex: 1,
                    firstEmpty: true
                });

                UI_ProfileUpdate.setIbeSupportLanguages(response.ibeSuportLans);

            }
            UI_ProfileUpdate.customer = response.customer;
            if (response.lmsDetails != null) {
                UI_ProfileUpdate.lmsDetails = response.lmsDetails;
                UI_commonSystem.lmsDetails = response.lmsDetails;
                Util_CustomerValidation.isBecomeLmsUser = true;
            }
            setDropDowndata();

            UI_ProfileUpdate.showProfilePage();
            UI_UserTabs.clearPanels();
            UI_ProfileUpdate.isPageLoaded = true;

            UI_ProfileUpdate.nationalityList = response.nationalityInfo;

            if (response.seatMealPerefernceEnabled) {

                $('#seatPreferences').html("");

                for (i = 0; i < response.seatPreferencesList.length; i++) {
                    $('#seatPreferences').append("<input type='radio' name='preferredSeatType' value = '" + response.seatPreferencesList[i].seatType + "' id = '" + response.seatPreferencesList[i].seatType + "' class='label'/>" + response.seatPreferencesList[i].description + "");
                    $('#seatPreferences').append("<br>");
                }

                UI_ProfileUpdate.jsMealModel = response.mealList;
                UI_ProfileUpdate.jsMealCategoryModel = UI_ProfileUpdate.createMealCatModal(UI_ProfileUpdate.jsMealModel);

                if (response.preferredSeatType != null) {
                    $("#" + response.preferredSeatType + "").prop("checked", true);
                }

                setSelectedMeals(response.preferredMeals);

            } else {
                $('#trMealSeatPreference').hide();
            }

            if (response.addFamilyMembersEnabled) {
                $("#FMNationality").html("");
                $("#FMRelationship").html("");

                $("#FMNationality").fillFirstOption(response.jsonLabel.lblPleaseSelect);
                $("#FMNationality").fillDropDown({
                    dataArray: response.nationalityInfo,
                    keyIndex: 0,
                    valueIndex: 1,
                    firstEmpty: false
                });

                $("#FMRelationship").fillFirstOption(response.jsonLabel.lblPleaseSelect);
                $("#FMRelationship").fillDropDown({
                    dataArray: response.relationshipList,
                    keyIndex: 0,
                    valueIndex: 1,
                    firstEmpty: false
                });
                UI_ProfileUpdate.relationshipList = response.relationshipList;

                $("#trFamilyMembersList").find("#familyMembersTable").html("");
                setFamilyMembers(response.familyMemberDTOList);

                bindFamilyMemberGridClick();
                UI_ProfileUpdate.clearFamilyMemberFields();

            } else {
                $('#trFamilyMemberDetails').hide();
            }

        } else {
            UI_commonSystem.loadErrorPage({
                messageTxt: response.messageTxt
            });
        }
        Util_CustomerValidation.showHideFields();
        UI_commonSystem.loadingCompleted();
    }
    /**
     * Set Customer Info
     */
UI_ProfileUpdate.setCustomerInfo = function(customer) {
    if (customer != null || customer != "") {
        $("#selTitle").val(customer.title);
        $("#txtFirstName").val(customer.firstName);
        $("#txtLastName").val(customer.lastName);
        $("#txtCity").val(customer.city);
        $("#txtEmail").val(customer.emailId);
        $("#txtPassword").val(customer.password);
        $("#txtConPassword").val(customer.password);
        $("#selSecQ").val(customer.secretQuestion);
        $("#txtAns").val(customer.secretAnswer);
        $("#txtAlternateEmail").val(customer.alternativeEmailId);
        //$("#txtDateOfBirth").val(customer.dateOfBirth);
        $("#txtDateOfBirth").datepicker("setDate", customer.dateOfBirth);
        // Set Gender
        UI_ProfileUpdate.radioButtonChecked({
            name: 'customer\\.gender',
            value: customer.gender
        });

        var arrTel = customer.telephone.split("-");
        if (arrTel.length > 0) {
            $("#txtPCountry").val(arrTel[0]);
        }
        if (!contactConfig.areaCode1.ibeVisibility) {
            var temTelephone = "";
            if (arrTel.length > 1) {
                temTelephone += arrTel[1];
            }
            if (arrTel.length > 2) {
                $("#txtTelephone").val(temTelephone + arrTel[2]);
            }
        } else {
            if (arrTel.length > 1) {
                $("#txtPArea").val(arrTel[1]);
            }
            if (arrTel.length > 2) {
                $("#txtTelephone").val(arrTel[2]);
            }
        }

        arrTel = customer.mobile.split("-");
        if (arrTel.length > 0) {
            $("#txtMCountry").val(arrTel[0]);
        }
        if (!contactConfig.areaCode1.ibeVisibility) {
            var temMobile = "";
            if (arrTel.length > 1) {
                temMobile += arrTel[1];
            }
            if (arrTel.length > 2) {
                $("#txtMobile").val(temMobile + arrTel[2]);
            }
        } else {
            if (arrTel.length > 1) {
                $("#txtMArea").val(arrTel[1]);
            }
            if (arrTel.length > 2) {
                $("#txtMobile").val(arrTel[2]);
            }
        }

        arrTel = customer.officeTelephone.split("-");
        if (arrTel.length > 0) {
            $("#txtOCountry").val(arrTel[0]);
        }
        if (!contactConfig.areaCode1.ibeVisibility) {
            var temOfficeTelephone = "";
            if (arrTel.length > 1) {
                temOfficeTelephone = arrTel[1];
            }
            if (arrTel.length > 2) {
                $("#txtOfficeTel").val(temOfficeTelephone + arrTel[2]);
            }
        } else {
            if (arrTel.length > 1) {
                $("#txtOArea").val(arrTel[1]);
            }
            if (arrTel.length > 2) {
                $("#txtOfficeTel").val(arrTel[2]);
            }
        }

        arrTel = customer.fax.split("-");
        if (arrTel.length > 0) {
            $("#txtFCountry").val(arrTel[0]);
        }
        if (!contactConfig.areaCode1.ibeVisibility) {
            var temFax = "";
            if (arrTel.length > 1) {
                temFax += arrTel[1];
            }
            if (arrTel.length > 2) {
                $("#txFax").val(temFax + arrTel[2]);
            }
        } else {
            if (arrTel.length > 1) {
                $("#txtFArea").val(arrTel[1]);
            }
            if (arrTel.length > 2) {
                $("#txFax").val(arrTel[2]);
            }
        }

        $("#txtAddr1").val(customer.addressStreet);
        $("#txtAddr2").val(customer.addressLine);
        $("#txtZipCode").val(customer.zipCode);
        $("#selCountry").val(customer.countryCode);
        $("#selNationality").val(customer.nationalityCode);

        // Set Questionaire
        UI_ProfileUpdate.setQuestionaire(customer.customerQuestionaireDTO);
        // Set Hidden field values
        $("#hdnTelNo").val(customer.telephone);
        $("#hdnMobile").val(customer.mobile);
        $("#hdnOffice").val(customer.officeTelephone);
        $("#hdnFax").val(customer.fax);
        $("#customer\\.version").val(customer.version);

        // set Emergency contact details
        $("#selEmgnTitle").val(customer.emgnTitle);
        $("#txtEmgnFirstName").val(customer.emgnFirstName);
        $("#txtEmgnLastName").val(customer.emgnLastName);
        $("#txtEmgnEmail").val(customer.emgnEmail);

        arrTel = customer.emgnPhoneNo.split("-");
        if (arrTel.length > 0) {
            $("#txtEmgnPCountry").val(arrTel[0]);
        }
        if (!contactConfig.ibeUserRegAreaCodeVisible) {
            if (arrTel.length > 2) {
                $("#txtEmgnTelephone").val(arrTel[1] + arrTel[2]);
            }
        } else {
            if (arrTel.length > 1) {
                $("#txtEmgnPArea").val(arrTel[1]);
            }
            if (arrTel.length > 2) {
                $("#txtEmgnTelephone").val(arrTel[2]);
            }
        }

    }
}

UI_ProfileUpdate.setLmsDetails = function(lmsDetails) {
        $("#txtEmailLMS").val(lmsDetails.emailId);
        $("#txtDOBLMS").val(lmsDetails.dateOfBirth);
        $("#txtEmailHLMS").val(lmsDetails.headOFEmailId);
        $("#txtLanguageLMS").val(lmsDetails.language);
        $("#txtPpnLMS").val(lmsDetails.passportNum);
        $("#txtRefferedEmailLMS").val(lmsDetails.refferedEmail);
        if (lmsDetails.refferedEmail != null && lmsDetails.refferedEmail != "") {
            $("#txtRefferedEmailLMS").prop("readonly", true);
        }
        var dob = lmsDetails.dateOfBirth;
        var date = Date.parse(dob);
        var formatted_date = new Date(date);
        var present = new Date();
        var age = (present.getTime() - formatted_date.getTime()) / 1000 / 60 / 60 / 24;
        if (age > (12 * 365 + 3)) {
            $("#lblEmailHMandLMS").hide();
            headFfidReq = false;
        } else {
            $("#lblEmailHMandLMS").show();
            headFfidReq = true;
        }
    }
    /**
     * Checked Radio button Using value
     */
UI_ProfileUpdate.radioButtonChecked = function(data) {
        $("input:radio[name='" + data.name + "']").filter(
            "[value='" + data.value + "']").attr('checked', true);
    }
    /**
     * Set Questionaire data
     */
UI_ProfileUpdate.setQuestionaire = function(questionaires) {
        var questionaire = "";
        var radioChecked = "";
        var questionKey = "";
        for (var i = 0; i < questionaires.length; i++) {
            questionaire = questionaires[i];
            questionKey = $.trim(questionaire.questionKey);
            switch (questionKey) {
                case "1":
                    radioChecked = {
                        name: "questionaire\\[0\\]\\.questionAnswer",
                        value: ($.trim(questionaire.questionAnswer))
                    };
                    break;
                case "2":
                    radioChecked = {
                        name: "questionaire\\[1\\]\\.questionAnswer",
                        value: ($.trim(questionaire.questionAnswer))
                    };
                    break;
                case "3":
                    radioChecked = {
                        name: "questionaire\\[2\\]\\.questionAnswer",
                        value: ($.trim(questionaire.questionAnswer))
                    };
                    break;
                case "4":
                    radioChecked = {
                        name: "questionaire\\[3\\]\\.questionAnswer",
                        value: ($.trim(questionaire.questionAnswer))
                    };
                    break;
                case "5":
                    radioChecked = {
                        name: "questionaire\\[4\\]\\.questionAnswer",
                        value: ($.trim(questionaire.questionAnswer))
                    };
                    break;
                case "6":
                    $("#txtQ2").val($.trim(questionaire.questionAnswer));
                    break;

            }

            if (questionKey != 6) {
                UI_ProfileUpdate.radioButtonChecked(radioChecked);
            }
        }
    }
    /**
     * Remove CSS Class
     */
UI_ProfileUpdate.removeClass = function() {
    $("body").find(".errorControl").each(function(intIndex) {
        $("#" + this.id).removeClass("errorControl");
    });
}

/**
 * Policy Link Set up
 */
UI_ProfileUpdate.policyLinkSetup = function() {
    $("#linkPolicy").attr("href",
        SYS_IBECommonParam.homeURL + "/privacy-policy.html");
}

// IE 6 Bug Fix
function setDropDowndata() {
    if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
        setTimeout("filldata()", 10);
    } else {
        filldata();
    }
}

function filldata() {
    UI_ProfileUpdate.setCustomerInfo(UI_ProfileUpdate.customer);
    if (UI_ProfileUpdate.lmsDetails != null) {
        $("#isLMS").prop('checked', true);
        UI_ProfileUpdate.setLmsDetails(UI_ProfileUpdate.lmsDetails);
        UI_commonSystem.lmsDetails = UI_ProfileUpdate.lmsDetails;
    }
}

UI_ProfileUpdate.setIbeSupportLanguages = function(languagesMap) {
    var templeteName = "templateLanguages";
    if (languagesMap != null) {
        for (var i = 0; i < languagesMap.length; i++) {
            var clonedTemplateId = templeteName + "_" + i;
            var clone = $("#" + templeteName + "").clone();
            clone.attr("id", clonedTemplateId);
            if (i == 0) {
                clone.insertAfter($("#" + templeteName));
            } else {
                clone.insertAfter($("#" + templeteName + "_" + (i - 1)));
            }
            $("#" + clonedTemplateId).find("#radQ5").val(languagesMap[i][0]);
            $("#" + clonedTemplateId).find("#lblLanName").text(
                languagesMap[i][1]);
        }
        $("#" + templeteName).hide();

        // Hide if there is a one language - RAk Requirement
        if (languagesMap.length == 1) {
            $("#lblInfoLan").hide();
            $("#templateLanguages_0").hide();
        }
        $("input[name='questionaire[4].questionAnswer']").each(function(i, j) {
            if (GLOBALS.sysLanguage == $(j).val()) {
                $(j).attr("checked", true);
            }
        })

    }
}

/*-------------------------- PREFERRED MEAL EVENT BINDINGS -----------------------*/

$(document).on("click", ".selectMeal", function() {
    var tArr;
    seletedmealArray = [];

    $('#selectedMealsTable').closest('tbody').children().each(function() {

        tArr = this.id.split("_");
        var t = {
            mealCategoryID: tArr[1],
            mealIndex: tArr[2],
            count: parseInt($(this).find("#lblMealCount").html())
        }
        seletedmealArray.push(t);

    });

    var intSegRow = 0;
    $.data($("#newPopItem")[0], "mealdata", {
        segId: 0,
        paxId: 0,
        catId: "-",
        meals: seletedmealArray
    });

    $("#newPopItem").openMyPopUp({
        width: 645,
        height: 465,
        topPoint: $(window).height() - (600),
        headerHTML: UI_ProfileUpdate.buildMealsHeaders({
            catId: "-"
        }),
        bodyHTML: function() {
            return UI_ProfileUpdate.buildMealsCats({
                segId: 0,
                paxId: 0
            });
        },
        footerHTML: function() {
            var passHTML = "";
            if ($.data($("#newPopItem")[0], "mealdata").catId != "-" && !isNaN(selItemCount)) {
                passHTML = "<input type='button' value='back' class='Button ButtonDefaultDisable mealBack'/>" +
                    "<input type='button' value='confirm' class='Button mealConfirm'/>";
            } else {
                passHTML = "<input type='button' value='confirm' class='Button mealConfirm'/>";
            }
            return passHTML;
        }
    });
    $(".mealCatClick").unbind("click").bind("click", UI_ProfileUpdate.mealCategoryClick);
    $(".mealConfirm").unbind("click").bind("click", UI_ProfileUpdate.closeMealPopUp);
    $(".mealBack").unbind("click").bind("click", UI_ProfileUpdate.mealCategorynavClick);
    $(".close").unbind("click").bind("click", UI_ProfileUpdate.closeMealPopUp);


});

$(document).on("click", ".clearMeal", function() {
    $(this).parent().parent().remove();
    if ($('#selectedMealsTable').closest('tbody').children().length == 0) {
        $("#selectedMealsList").hide();
    }
    var strColor = "SeperateorBGColor";

    $('#selectedMealsTable tr').each(function() {

        if (strColor == "SeperateorBGColor") {
            strColor = "";
        } else {
            strColor = "SeperateorBGColor";
        }
        $(this).find('td').each(function() {
            $(this).removeClass("SeperateorBGColor");
            $(this).addClass(strColor);
        });

    });

    $('#selectedMealsTable tr').css('background-color', '');
});


$(document).on("mouseover", ".thumbImg", function(e) {
    var poss = getEventPosition(e);
    var descrip = $(this).parent().parent().parent().find(".description").html();
    var lengthofLine = 40;
    var leftAdjsment = 125;
    var noOfLines = 3;
    if (descrip != null) {
        noOfLines = parseInt(descrip.length / lengthofLine);
    }
    if (noOfLines > 5) {
        leftAdjsment = 275;
    } else {
        leftAdjsment = 125;
    }
    var adjsment = ((poss.top) > parseInt($(window).scrollTop() + 300)) ?
        parseInt(poss.top) - (330 + (noOfLines * 6)) : poss.top - 100;
    $(".zoomer").css({
        "top": (adjsment),
        "width": 265,
        "left": (poss.left - leftAdjsment)
    });

    $(".zoomer").show();
    var tempImg = new Image();
    tempImg.src = $(this).parent().parent().parent().find(".imgpath").html();
    var ratio = 0;
    if ($.browser = "msie") {
        ratio = tempImg.width / tempImg.height;
    } else {
        ratio = tempImg.naturalWidth / tempImg.naturalHeight;
    }
    var setval = "";
    if (ratio >= 1) {
        setval = "width='250' style='width:250px;'";
    } else {
        setval = "height='250' style='height:250px;'";
    }
    $(".imgDescription").html(descrip);
    $(".loadImage").html('<img src=' + tempImg.src + ' ' + setval + ' />');
});


$(document).on("mouseout", ".thumbImg", function() {
    $(".loadImage").html('<img src="../images/Loading_no_cache.gif"/>');
    $(".imgDescription").html("");
    $(".zoomer").hide()
});

function getEventPosition(e, obj) {
    var pos = {
        top: e.pageY,
        left: e.pageX
    }
    return pos;
}

UI_ProfileUpdate.buildMealsHeaders = function(p) {
    var str = '<table border="0" cellspacing="0" cellpadding="0"><tr>';
    if (p.catId == "-") {
        str += '<td class="redBar"><label>Menu</label></td><td class="redCorner"></td>';
    } else {
        str += '<td class="grayBar"><label id="navMealCat">Menu</label></td><td class="grayCorner"></td>'
        str += '<td class="redBar"><label>' + UI_ProfileUpdate.getMealCatByCode(p.segId, p.catId) + '</label></td><td class="redCorner"></td>';
    }
    str += '</tr></table>';
    return str;
};

UI_ProfileUpdate.getMealCatByCode = function(seg, code) {
    var t = null;
    $.each(UI_ProfileUpdate.jsMealCategoryModel[seg], function() {
        if (this.catCode == code) {
            t = this.catName;
        }
    });
    return t;
}

UI_ProfileUpdate.buildMealsCats = function(p) {
    var intSegRow = 0;

    var htmlSTR = '<div id="pan_' + p.segId + "_" + p.paxId + '" class="mealCatDiv">';
    if (intSegRow != null) {
        htmlSTR += '<div class="catBlock firstCat"></div>';
        $.each(UI_ProfileUpdate.jsMealCategoryModel[intSegRow], function(i, j) {
            htmlSTR += '<a href="javascript:void(0)" class="mealCatClick calClass' + j.catCode + '" id="cat_' + j.catCode + '">' +
                '<div class="catBlock">&nbsp;</div></a>';
        });
        htmlSTR += '</div>'
    } else {
        htmlSTR += '<div class="mealItemNone">Error occured while loading meal data</div>';
    }
    return htmlSTR;

}

UI_ProfileUpdate.createMealCatModal = function(arraList) {
    var objectList = [];
    for (var i = 0; i < arraList.length; i++) {
        var objectCatList = [];
        var arr = [];
        $.each(arraList, function(i, meal) {
            if ($.inArray(meal.mealCategoryID, arr) === -1) {
                arr.push(meal.mealCategoryID);
                var temp = {
                    "catCode": meal.mealCategoryID,
                    "catName": meal.mealCategoryCode
                };
                objectCatList.push(temp);
            }
        });
        objectList.push(objectCatList);
    }
    return objectList;
};

UI_ProfileUpdate.closeMealPopUp = function(eve) {
    if (eve.target.className != "close") {
        var mealSelected = $.data($("#newPopItem")[0], "mealdata").meals;
        var strHTMLText = '';
        $("#trSelectedMealsList").find("#selectedMealsTable").html("");
        var intLength = mealSelected.length;
        var strColor = "SeperateorBGColor";
        $("#selectedMealsList").show();

        if (mealSelected.length > 0) {

            $.each(mealSelected, function(i, j) {
                var selectedMealItem = UI_ProfileUpdate.jsMealModel[j.mealIndex];
                var catId = selectedMealItem.mealCategoryID;
                var qty = qty = j.count;

                if (strColor == "SeperateorBGColor") {
                    strColor = "";
                } else {
                    strColor = "SeperateorBGColor";
                }

                if (catId == '-') {
                    catId = selectedMealItem.mealCategoryID;
                }

                strHTMLText += '<tr id="meal_' + catId + '_' + j.mealIndex + '" >' +
                    '<td class="GridItems GridDblHighlight ' + strColor + '" align="left"">' +
                    '<label>' + selectedMealItem.mealName + '</label>' +
                    '</td>' +
                    '<td class="GridItems GridDblHighlight ' + strColor + '" align="left"">' +
                    '<label>' + selectedMealItem.mealDescription + '</label>' +
                    '</td>' +
                    '<td class="GridItems GridDblHighlight ' + strColor + '" align="right"">' +
                    '<label id="lblMealCount">' + j.count + '</label>' +
                    '</td>' +
                    '<td class="GridItems GridDblHighlight ' + strColor + '" align="left"">' +
                    '<a title="Clear" style="display: block;" id="clear_' + catId + '_' + j.mealIndex + '" class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>' +
                    '</td>' +
                    '<td style="display:none" >' +
                    '<input class="selectedMeals" value="' + selectedMealItem.mealCode + '_' + j.count + '" >' +
                    '</td>' +
                    '</tr>';

            });

            $("#trSelectedMealsList").find("#selectedMealsTable").append(strHTMLText);
        } else {
            $("#selectedMealsList").hide();
        }

    }
    $("#newPopItem").closeMyPopUp();
};

UI_ProfileUpdate.mealCategoryClick = function(event, mySeg, myCat, chMeals) {

    var selMeals;
    if (event == null) {
        var intSegRow = parseInt(mySeg, 10);
        $.data($("#newPopItem")[0], "mealdata").catId = myCat;
        $.data($("#newPopItem")[0], "mealdata").segId = intSegRow;
        $("#newPopItem").find(".headeritems").html(UI_ProfileUpdate.buildMealsHeaders({
            segId: 0,
            catId: myCat
        }));
        selMeals = chMeals;
    } else {
        $.data($("#newPopItem")[0], "mealdata").catId = this.id.split("_")[1];
        $("#newPopItem").find(".headeritems").html(UI_ProfileUpdate.buildMealsHeaders({
            segId: 0,
            catId: this.id.split("_")[1]
        }));
        selMeals = $.data($("#newPopItem")[0], "mealdata").meals;
    }
    var passHTML = "<input type='button' value='Back' class='Button ButtonDefaultDisable mealBack'/><input type='button' value='Confirm' class='Button mealConfirm'/>";
    $("#newPopItem").find("#footeritems").html(passHTML);
    $("#navMealCat").unbind("click").bind("click", UI_ProfileUpdate.mealCategorynavClick);
    $("#newPopItem").find("#bodyitems").html(UI_ProfileUpdate.buildMeals({
        catId: $.data($("#newPopItem")[0], "mealdata").catId,
        segId: $.data($("#newPopItem")[0], "mealdata").segId,
        paxId: $.data($("#newPopItem")[0], "mealdata").paxId,
        choosed: selMeals
    }));

    var isMultiMealEnabled = true;

    $(".mealConfirm").unbind("click").bind("click", UI_ProfileUpdate.closeMealPopUp);
    $(".mealBack").unbind("click").bind("click", UI_ProfileUpdate.mealCategorynavClick);
    $(".mealSpinner").spinnerisa({
        select: isMultiMealEnabled,
        selectedMeal: chMeals,
        selectAction: function(event, val) {
            var mealId = val.split("_")[2];
            var mealArrar = $.data($("#newPopItem")[0], "mealdata").meals;
            mealArrar[0] = ({
                mealCategoryID: "-",
                mealIndex: mealId,
                count: null
            });
        },
        increase: function(event, val) {
            var catId = val.objid.split("_")[1];
            var mealId = val.objid.split("_")[2];
            var mealArrar = $.data($("#newPopItem")[0], "mealdata").meals;
            var boolupdated = false;
            $.each(mealArrar, function(i, j) {
                if (j.mealCategoryID == catId && j.mealIndex == mealId) {
                    j.count = val.value;
                    boolupdated = true;
                }
            });
            if (!boolupdated) {
                mealArrar.push({
                    mealCategoryID: catId,
                    mealIndex: mealId,
                    count: val.value
                });
            }
            $.data($("#newPopItem")[0], "mealdata").meals = mealArrar;
        },
        decrease: function(event, val) {
            var catId = val.objid.split("_")[1];
            var mealId = val.objid.split("_")[2];
            var mealArrar = $.data($("#newPopItem")[0], "mealdata").meals;
            $.each(mealArrar, function(i, j) {
                if (j.mealCategoryID == catId && j.mealIndex == mealId) {
                    j.count = val.value;
                }
            });
            $.each(mealArrar, function(i, j) {
                if (j != undefined && j.count == 0) {
                    mealArrar.splice(i, 1);
                }
            });
            $.data($("#newPopItem")[0], "mealdata").meals = mealArrar;
        }
    });
    $(".mealItem").unbind("mouseenter").bind("mouseenter", function() {
        $(this).addClass("mOverMeal");
    });
    $(".mealItem").unbind("mouseleave").bind("mouseleave", function() {
        $(this).removeClass("mOverMeal");
    });

};

UI_ProfileUpdate.mealCategorynavClick = function() {
    $("#newPopItem").find(".headeritems").html(UI_ProfileUpdate.buildMealsHeaders({
        catId: "-"
    }));
    $("#newPopItem").find("#bodyitems").html(UI_ProfileUpdate.buildMealsCats({
        segId: $.data($("#newPopItem")[0], "mealdata").segId,
        paxId: $.data($("#newPopItem")[0], "mealdata").paxId
    }));
    var passHTML = "<input type='button' value='Confirm' class='Button mealConfirm'/>";
    $("#newPopItem").find("#footeritems").html(passHTML);
    $(".mealCatClick").unbind("click").bind("click", UI_ProfileUpdate.mealCategoryClick);
    $(".mealConfirm").unbind("click").bind("click", UI_ProfileUpdate.closeMealPopUp);
    $(".mealBack").unbind("click").bind("click", UI_ProfileUpdate.mealCategorynavClick);
};


UI_ProfileUpdate.buildMeals = function(p) {
    checkChoosedMeals = function(dataArray, catID, mealIndex, whatToReturn) {
        var ret = (whatToReturn == "count") ? 0 : false;
        for (var i = 0; i < dataArray.length; i++) {
            if (dataArray[i].mealCategoryID == catID && dataArray[i].mealIndex == mealIndex) {
                if (whatToReturn == "count") {
                    ret = dataArray[i].count;
                    break;
                } else {
                    ret = true;
                    break;
                }
            }
        };
        return ret
    };
    var intSegRow = 0;
    var isFreeService = true;

    var htmlSTR = '';
    if (intSegRow != null) {
        for (var i = 0; i < UI_ProfileUpdate.jsMealModel.length; i++) {
            var tm = UI_ProfileUpdate.jsMealModel[i];

            var mealPriceLabel = "";

            if (p.catId == "-") {
                if (i % 2 == 1)
                    htmlSTR += '<div class="mealItem" id="' + p.segId + '_' + p.paxId + '_' + p.catId + '_' + i + '">';
                else
                    htmlSTR += '<div class="mealItem" id="' + p.segId + '_' + p.paxId + '_' + p.catId + '_' + i + '">';
                if (checkChoosedMeals(p.choosed, p.catId, i, 'bool')) {
                    htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>' +
                        '<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="' + solveImageUrl(tm.mealImagePath) + '" height="50"/></div></td>' +
                        '<td class="name"><label class="thumbImg">' + tm.mealName + '</label></td><td class="qtyHead bdLeft"><label>Qty</label></td></tr><tr>' +
                        '<td class="description" style="width:445px"><label>' + tm.mealDescription + '</label></td>' +
                        '<td class="qtyBody bdLeft"><label id="spin_' + p.catId + '_' + i + '" class="mealSpinner">' + checkChoosedMeals(p.choosed, p.catId, i, 'count') + 'x</label></td>' +
                        '<span class="imgpath" style="display:none">' + solveImageUrl(tm.mealImagePath) + '</span></td></tr></table></div>';
                } else {
                    htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>' +
                        '<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="' + solveImageUrl(tm.mealImagePath) + '" height="50"/></div></td>' +
                        '<td class="name"><label class="thumbImg">' + tm.mealName + '</label></td><td class="bdLeft"></td></tr><tr>' +
                        '<td class="description" style="width:445px"><label>' + tm.mealDescription + '</label></td>' +
                        '<td class="qtyBody bdLeft"><label id="spin_' + p.catId + '_' + i + '" class="mealSpinner">0</label></td>' +
                        '<span class="imgpath" style="display:none">' + solveImageUrl(tm.mealImagePath) + '</span></td></tr></table></div>';
                }
            } else if (tm.mealCategoryID == p.catId) {
                if (i % 2 == 1)
                    htmlSTR += '<div class="mealItem" id="' + p.segId + '_' + p.paxId + '_' + p.catId + '_' + i + '">';
                else
                    htmlSTR += '<div class="mealItem" id="' + p.segId + '_' + p.paxId + '_' + p.catId + '_' + i + '">';
                if (checkChoosedMeals(p.choosed, p.catId, i, 'bool')) {
                    htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>' +
                        '<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="' + solveImageUrl(tm.mealImagePath) + '" height="50"/></div></td>' +
                        '<td class="name"><label  class="thumbImg">' + tm.mealName + '</label></td><td class="qtyHead bdLeft"><label>Qty</label></td></tr><tr>' +
                        '<td class="description" style="width:445px"><label>' + tm.mealDescription + '</label></td>' +
                        '<td class="qtyBody bdLeft"><label id="spin_' + p.catId + '_' + i + '" class="mealSpinner">' + checkChoosedMeals(p.choosed, p.catId, i, 'count') + 'x</label></td>' +
                        '<span class="imgpath" style="display:none">' + solveImageUrl(tm.mealImagePath) + '</span></td></tr></table></div>';
                } else {
                    htmlSTR += '<table border="0" cellpadding="0" cellspacing="0" class="bdBottom"><tr>' +
                        '<td rowspan="2" class="thumb"><div class="thumbImg" style="width:50px;height:50px;overflow:hidden"><img src="' + solveImageUrl(tm.mealImagePath) + '" height="50"/></div></td>' +
                        '<td class="name"><label class="thumbImg">' + tm.mealName + '</label></td><td class="qtyHead bdLeft"><label>Qty</label></td></tr><tr>' +
                        '<td class="description" style="width:445px"><label>' + tm.mealDescription + '</label></td>' +
                        '<td class="qtyBody bdLeft"><label id="spin_' + p.catId + '_' + i + '" class="mealSpinner">0</label></td>' +
                        '<span class="imgpath" style="display:none">' + solveImageUrl(tm.mealImagePath) + '</span></td></tr></table></div>';
                }
            }
        }
    } else {
        htmlSTR += '<div class="mealItemNone">Error occured while loading meal data</div>';
    }
    return htmlSTR;

}

function solveImageUrl(url) {
    var newurl = "";
    if (window.location.protocol == "https:") {
        newurl = url.replace("http", "https");
    } else {
        newurl = url;
    }

    return newurl;
}

setSelectedMeals = function(mealList) {

    var strHTMLText = '';
    $("#trSelectedMealsList").find("#selectedMealsTable").html("");
    var strColor = "SeperateorBGColor";
    $("#selectedMealsList").show();
    //var meals;
    var selectedMeals = [];

    if (mealList != null) {
        //meals = mealList.split(",");

        $.each(mealList, function(i, customerPreferredMealDTO) {
            $.each(UI_ProfileUpdate.jsMealModel, function(i, mealItem) {
                if (mealItem.mealCode == customerPreferredMealDTO.mealCode) {
                    data = {};
                    data["selectedMeal"] = mealItem;
                    data["quantity"] = customerPreferredMealDTO.quantity;
                    data["mealIndex"] = i;
                    selectedMeals.push(data);
                }
            });
        });

        $.each(selectedMeals, function(i, selectedMeal) {

            var selectedMealItem = selectedMeal.selectedMeal;
            var catId = selectedMealItem.mealCategoryID;
            var qty = selectedMeal.quantity;

            if (strColor == "SeperateorBGColor") {
                strColor = "";
            } else {
                strColor = "SeperateorBGColor";
            }

            strHTMLText += '<tr id="meal_' + catId + '_' + selectedMeal.mealIndex + '" >' +
                '<td class="GridItems GridDblHighlight bdRight ' + strColor + '" align="left"">' +
                '<label>' + selectedMealItem.mealName + '</label>' +
                '</td>' +
                '<td class="GridItems GridDblHighlight bdRight ' + strColor + '" align="left"">' +
                '<label>' + selectedMealItem.mealDescription + '</label>' +
                '</td>' +
                '<td class="GridItems GridDblHighlight ' + strColor + '" align="right"">' +
                '<label id="lblMealCount">' + qty + '</label>' +
                '</td>' +
                '<td class="GridItems GridDblHighlight ' + strColor + '" align="left"">' +
                '<a title="Clear" style="display: block;" id="clear_' + catId + '_' + selectedMeal.mealIndex + '" class="clearMeal clearLabel noTextDec" href="javascript:void(0)"></a>' +
                '</td>' +
                '<td style="display:none" >' +
                '<input class="selectedMeals" value="' + selectedMealItem.mealCode + '_' + qty + '" >' +
                '</td>' +
                '</tr>';

        });

        $("#trSelectedMealsList").find("#selectedMealsTable").append(strHTMLText);
    } else {
        $("#selectedMealsList").hide();
    }

}

setFamilyMembers = function(familyMemberDTOList) {
    $.each(familyMemberDTOList, function(i, familyMemberDTO) {
        UI_ProfileUpdate.addFamilyMemberEntry(familyMemberDTO);
    });

    UI_ProfileUpdate.rowSeperatorFamilyMembers();

}

UI_ProfileUpdate.disableFamilyMemberFields = function() {

    $("#FMFirstName").prop('disabled', true);
    $("#FMLastName").prop('disabled', true);
    $("#FMRelationship").prop('disabled', true);
    $("#FMNationality").prop('disabled', true);
    $("#btnSaveFamilyMember").prop('disabled', true);
    $("#FMDOB").datepicker("disable");
}

UI_ProfileUpdate.enableFamilyMemberFields = function() {

    $("#FMFirstName").prop('disabled', false);
    $("#FMLastName").prop('disabled', false);
    $("#FMRelationship").prop('disabled', false);
    $("#FMNationality").prop('disabled', false);
    $("#btnSaveFamilyMember").prop('disabled', false);
    $("#FMDOB").datepicker("enable");
}

UI_ProfileUpdate.clearFamilyMemberFields = function() {

    $("#FMFirstName").val('');
    $("#FMLastName").val('');
    $("#FMRelationship").val('');
    $("#FMNationality").val('');
    $("#FMDOB").val('');
    UI_ProfileUpdate.selectedFamilyMemberId = "";

}

UI_ProfileUpdate.saveEditFamilyMember = function() {

    if (!Util_CustomerValidation.saveFamilyMemberValidate()) {
        return false;
    }

    data = {};

    data['familyMember.firstName'] = $("#FMFirstName").val();
    data['familyMember.lastName'] = $("#FMLastName").val();
    data['familyMember.relationshipId'] = $("#FMRelationship").val();
    data['familyMember.nationalityCode'] = $("#FMNationality").val();
    data['familyMember.dateOfBirth'] = $("#FMDOB").val();
    if (UI_ProfileUpdate.selectedFamilyMemberId == "") {
        data['familyMember.familyMemberId'] = null;
    } else {
        data['familyMember.familyMemberId'] = UI_ProfileUpdate.selectedFamilyMemberId;
    }

    $.ajax({

        url: 'customerProfileUpdate!saveUpdateFamilyMembers.action',
        data: data,
        dataType: "json",
        beforeSend: UI_commonSystem.loadingProgress,
        type: "POST",
        success: UI_ProfileUpdate.saveEditFamilyMemberSuccess,
        error: UI_commonSystem.setErrorStatus

    });

    return true;

}

UI_ProfileUpdate.saveEditFamilyMemberSuccess = function(response) {
    if (response != null && response.success == true) {
        if (UI_ProfileUpdate.selectedFamilyMemberId == "") {
            UI_ProfileUpdate.addFamilyMemberEntry(response.familyMember);
        } else {
            var firstName = response.familyMember.firstName;
            var lastName = response.familyMember.lastName;
            $("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMFirstName").text(firstName.charAt(0).toUpperCase() + firstName.slice(1));
            $("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMLastName").text(lastName.charAt(0).toUpperCase() + lastName.slice(1));
            var relationshipDes = getRelationshipDescription(response.familyMember.relationshipId);
            $("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMRelationshipId").text(relationshipDes);
            var nationlityName = getNationalityName(response.familyMember.nationalityCode);
            $("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMVationalityCode").text(nationlityName);
            $("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMDateOfBirth").text(response.familyMember.dateOfBirth);
        }
    } else {
        UI_commonSystem.loadErrorPage({
            messageTxt: response.messageTxt
        });
    }
    UI_ProfileUpdate.clearFamilyMemberFields();
    UI_ProfileUpdate.disableFamilyMemberFields();
    UI_commonSystem.loadingCompleted();
    UI_ProfileUpdate.rowSeperatorFamilyMembers();
    bindFamilyMemberGridClick();
    $("#btnEditFamilyMember").prop('disabled', true);
    $("#btnRemoveFamilyMember").prop('disabled', true);

}

UI_ProfileUpdate.addFamilyMemberEntry = function(familyMemberDTO) {

    var id = familyMemberDTO.familyMemberId;
    var firstName = familyMemberDTO.firstName;
    var lastName = familyMemberDTO.lastName;

    var strHTMLText = '';
    strHTMLText = '<tr style="cursor:pointer;" id="familyMemberId_' + id + '" class="GridItems">';

    strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
    strHTMLText += '		<label style="cursor:pointer;" id="' + id + '_FMFirstName">' + firstName.charAt(0).toUpperCase() + firstName.slice(1) + '<\/label>';
    strHTMLText += '	<\/td>';

    strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
    strHTMLText += '		<label style="cursor:pointer;" id="' + id + '_FMLastName">' + lastName.charAt(0).toUpperCase() + lastName.slice(1) + '<\/label>';
    strHTMLText += '	<\/td>';

    strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
    strHTMLText += '		<label style="cursor:pointer;" id="' + id + '_FMRelationshipId">' + getRelationshipDescription(familyMemberDTO.relationshipId) + '<\/label>';
    strHTMLText += '	<\/td>';

    strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
    strHTMLText += '		<label style="cursor:pointer;" id="' + id + '_FMVationalityCode">' + getNationalityName(familyMemberDTO.nationalityCode) + '<\/label>';
    strHTMLText += '	<\/td>';

    strHTMLText += '	<td class="GridDblHighlight " align="center" style="padding-left:10px;">';
    strHTMLText += '		<label style="cursor:pointer;" id="' + id + '_FMDateOfBirth">' + familyMemberDTO.dateOfBirth + '<\/label>';
    strHTMLText += '	<\/td>';

    strHTMLText += '<\/tr>';

    $("#familyMembersList").find("#familyMembersTable").append(strHTMLText);
}

UI_ProfileUpdate.rowSeperatorFamilyMembers = function() {

    var strColor = "SeperateorBGColor";

    $('#familyMembersTable tr').each(function() {

        if (strColor == "SeperateorBGColor") {
            strColor = "";
        } else {
            strColor = "SeperateorBGColor";
        }

        $(this).addClass(strColor);
    });

    $('#familyMembersTable tr').css('background-color', '');
}

UI_ProfileUpdate.setFamilyMemberData = function() {
    $("#FMFirstName").val($("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMFirstName").text());
    $("#FMLastName").val($("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMLastName").text());
    var relationshipId = getRelationshipId($("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMRelationshipId").text());
    $("#FMRelationship").val(relationshipId);
    var nationlityCode = getNationalityCode($("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMVationalityCode").text());
    $("#FMNationality").val(nationlityCode);

    var dateOfBirth = $("#" + UI_ProfileUpdate.selectedFamilyMemberId + "_FMDateOfBirth").text().split("/");

    $("#FMDOB").datepicker("setDate", new Date(dateOfBirth[2], dateOfBirth[1] - 1, dateOfBirth[0]));
}

getNationalityName = function(nationalityCode) {
    var nationalityName;
    $.each(UI_ProfileUpdate.nationalityList, function(i, j) {
        if (j[0] == nationalityCode) {
            nationalityName = j[1];
            return false;
        }
    });

    return nationalityName;
}

getNationalityCode = function(nationalityName) {
    var nationalityCode;
    $.each(UI_ProfileUpdate.nationalityList, function(i, j) {
        if (j[1] == nationalityName) {
            nationalityCode = j[0];
            return false;
        }
    });
    return nationalityCode;
}

getRelationshipDescription = function(relationshipId) {
    var relationshipDes;
    $.each(UI_ProfileUpdate.relationshipList, function(i, j) {
        if (j[0] == relationshipId) {
            relationshipDes = j[1];
            return false;
        }
    });

    return relationshipDes;
}

getRelationshipId = function(relationshipDes) {
    var relationshipId;
    $.each(UI_ProfileUpdate.relationshipList, function(i, j) {
        if (j[1] == relationshipDes) {
            relationshipId = j[0];
            return false;
        }
    });

    return relationshipId;
}

UI_ProfileUpdate.removeFamilyMember = function() {

    data = {};

    data['familyMember.familyMemberId'] = UI_ProfileUpdate.selectedFamilyMemberId;


    $.ajax({

        url: 'customerProfileUpdate!removeFamilyMember.action',
        data: data,
        dataType: "json",
        beforeSend: UI_commonSystem.loadingProgress,
        type: "POST",
        success: UI_ProfileUpdate.removeFamilyMemberSuccess,
        error: UI_commonSystem.setErrorStatus

    });

    return true;
}

UI_ProfileUpdate.removeFamilyMemberSuccess = function(response) {
    if (response != null && response.success == true) {

        $("#familyMemberId_" + UI_ProfileUpdate.selectedFamilyMemberId + "").hide();
        UI_ProfileUpdate.clearFamilyMemberFields();
        UI_ProfileUpdate.disableFamilyMemberFields();
        UI_commonSystem.loadingCompleted();
        UI_ProfileUpdate.rowSeperatorFamilyMembers();
        $("#btnEditFamilyMember").prop('disabled', true);
        $("#btnRemoveFamilyMember").prop('disabled', true);

        bindFamilyMemberGridClick();

    } else {
        UI_commonSystem.loadErrorPage({
            messageTxt: response.messageTxt
        });
    }
}

bindFamilyMemberGridClick = function() {
    $("tbody#familyMembersTable tr").click(function() {
        $(this).css('background-color', '#ffb900').siblings().css('background-color', '');
        var value = $(this).attr('id');
        UI_ProfileUpdate.selectedFamilyMemberId = value.split('_')[1];
        UI_ProfileUpdate.setFamilyMemberData();
        UI_ProfileUpdate.disableFamilyMemberFields();
        $("#btnEditFamilyMember").prop('disabled', false);
        $("#btnRemoveFamilyMember").prop('disabled', false);
    });
}

UI_ProfileUpdate.ready();