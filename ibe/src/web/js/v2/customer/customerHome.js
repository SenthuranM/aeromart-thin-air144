/**
 *	Author : Pradeep Karunanayake
 */

function UI_CustomerHome(){}
/**
 * Hold PNR
 */
UI_CustomerHome.pnr = "";
UI_CustomerHome.groupPnr = "";
UI_CustomerHome.airlineCode= ""
UI_CustomerHome.marketingAirlineCode = "";
UI_CustomerHome.isLoyaltyExist =  false;
UI_CustomerHome.strLogoutMsg = "";
UI_CustomerHome.isRegUser = false;
UI_CustomerHome.lmsJoined = true;
UI_CustomerHome.regEmail = "";
UI_CustomerHome.sessionTimeOutDisplay = UI_Top.holder().GLOBALS.sessionTimeOutDisplay;
$(document).ready(function(){
	UI_CustomerHome.ready();
});

/**
 * Page Ready
 * 
 */
UI_CustomerHome.ready = function() {
	$("#divLoadBg").hide();
    $("#RewardsPanel-0").hide();
	UI_commonSystem.loadingProgress();
	UI_CustomerHome.loadHomePageData();
	if ($("#regCustomer").val() !="" &&  $("#regCustomer").val() == "true") {
		UI_CustomerHome.isRegUser  = true;
	}
    $(document).off("click").on("click", ".aiRewardsLink" , function(){
        UI_commonSystem.openWindowPopup('11');
    });
    UI_commonSystem.showLmsInBar();
}
/**
 * Request Home Page Data
 */
UI_CustomerHome.loadHomePageData = function() {
    UI_commonSystem.loadingProgress();
	$("#frmCustomerTab").attr('action', SYS_IBECommonParam.securePath + 'showCustomerHomeDetail.action');
	$("#frmCustomerTab").ajaxSubmit({dataType: 'json', success:
		UI_CustomerHome.loadHomePageDataSuccess, error:UI_commonSystem.setErrorStatus});	
	return false;
}
/**
 * Process Home Page Request data
 */
UI_CustomerHome.loadHomePageDataSuccess = function(response) {	
	if(response != null && response.success == true) {
		
		if (fromPromoPage != null && fromPromoPage != "" && fromPromoPage == 'true'){
			//already not visible
		} else{
			$("#divLoadBg").slideDown();
		}
		
		UI_CustomerHome.strLogoutMsg = response.jsonLabel.lblInfoAreYouSure;
		if (!globalConfig.layOut.newRegUser){
			$("#regUserHomeNewPanel").hide();
		}else{
			$("#lblInfoAgree1").html(response.jsonLabel.lblInfoAgree1);
			if(response.customer != null){
				$("#lblInfoAgree1").show();
			}			
			$("#lblInfoAgree2").html(response.jsonLabel.lblInfoAgree2);
			$("#lblInfoAgree3").html(response.jsonLabel.lblInfoAgree3).append(response.jsonLabel.lblInfoAgree4);	
		}
		if (response.customer != null && response.customer != "") {	
			var customer = response.customer;
			var customerTitle = customer.title;
			if (customerTitle != "") {
				customerTitle = customerTitle.toLowerCase() + ".";
			}
			var customerName = customerTitle + " " + (customer.firstName).toLowerCase() + " " + (customer.lastName).toLowerCase();
			
			UI_CustomerHome.regEmail = customer.emailId;
			
			$("#lblTxtName").text(customerName);
			// Set Loyalty
			if (customer.loyaltyAccountExist == true || UI_Top.holder().GLOBALS.loyaltyEnable == false) {
				$("#trActivateLoyalty").remove();
			} 
			UI_CustomerHome.isLoyaltyExist = customer.loyaltyAccountExist;
			
			if(socialSiteType != undefined && socialSiteType != null && socialSiteType != 'null' && socialSiteType != "" ){
				if(socialSiteType == "1"){
					var profilePic = "<a href='http://facebook.com/" + customer.socialSiteCustId + "'> " +
		             "<img src='https://graph.facebook.com/" + customer.socialSiteCustId + "/picture?type=square'/></a>";
					 $("#spnPic").html(profilePic);
				}else if(socialSiteType == "3"){
					if(socialPictureUrl == "undefined" || socialPictureUrl == ""){
						socialPictureUrl = '../images/empty_user_no_cache.png';
					}
					var profilePic = "<img src='" + socialPictureUrl+ "'>";
					$("#spnPic").html(profilePic);
				}
				
				var profilePicture = $("#spnPic").find("img");
				profilePicture.cropresize({
					width : 50,
					height : 50,
					wrapperCSS : {
						"float" : "left"
					}
				});
				
			}
            UI_CustomerHome.lmsJoined = response.joinLms;
		} else {
			$("#trTabPannel").remove();
		}
	   	UI_commonSystem.changeUserLmsStatus(response.lmsDetails);
	   	UI_commonSystem.loyaltyManagmentEnabled = response.loyaltyManagmentEnabled;
	   	UI_commonSystem.integrateMashreqWithLMSEnabled = response.integrateMashreqWithLMSEnabled;
		UI_UserTabs.ready();
        $("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
        // Set Credit
        UI_CustomerHome.buildCredit({credit:response.totalCustomerCredit});
		UI_FlightSearch.pageInit(response);
		// Session Time Display
		if (UI_CustomerHome.isRegUser && UI_CustomerHome.sessionTimeOutDisplay ){			
			$('body').append('<div id="divSessContainer"></div>');
			UI_commonSystem.initSessionTimeout('divSessContainer',response.timeoutDTO,function(){});
		}
		// Google Analytic Track
		try {
			if(strAnalyticEnable == "true") {	
				//pageTracker._trackPageview("/LoadModifyBookingDetails");
				_gaq.push(['_trackPageview', "/LoadModifyBookingDetails" ]);
			}
		} catch(ex) {}	
		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}


UI_CustomerHome.pageInit = function() {
	UI_FlightSearch.resetSearhData();
	UI_FlightSearch.isModifySegment = false;
}

/**
 * Build Credit
 */
UI_CustomerHome.buildCredit = function(data) {
	$("#spnCerdit").html(CurrencyFormat(data.credit, UI_Top.holder().GLOBALS.systemDecimal) + " " + UI_Top.holder().GLOBALS.baseCurrency);
}
