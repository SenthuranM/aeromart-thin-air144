function UI_SocialCustomer() {};

function UI_CommonCustomer() {};
UI_SocialCustomer.passedToBackend = false;
UI_SocialCustomer.params = null;
/**
 * loads 3rd party scripts on demand
 */
UI_SocialCustomer.initLogin = function (response) {
    UI_SocialCustomer.params = response.socialLoginParams;

    if (UI_SocialCustomer.params.register && (UI_SocialCustomer.params.showFacebook || UI_SocialCustomer.params.showLinkedln)) {
        $("#socialRegister").show();
    }

    if (UI_SocialCustomer.params.showFacebook) {
        try {
            (function (d) {
                var js, id = 'facebook-jssdk';
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                d.getElementsByTagName('head')[0].appendChild(js);
            }(document));

            window.fbAsyncInit = function () {
                FB.init({
                    appId: UI_SocialCustomer.params.facebookAppId,
                    status: true,
                    cookie: true,
                    xfbml: true
                });
            }
            $("#tdFBSignIn").show();
            $("#btnFBSignIn").click(function () {
                UI_SocialCustomer.facebooksignInClick();
            });
        } catch (e) {}
    }

    if (UI_SocialCustomer.params.showLinkedln) {
        try {
            var e = document.createElement('script');
            e.type = 'text/javascript';
            e.async = true;
            e.src = document.location.protocol + '//platform.linkedin.com/in.js?async=true';
            e.onload = function () {
                IN.init({
                    api_key: UI_SocialCustomer.params.linkedInAppId,
                    authorize: false
                });
            };
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(e, s);
            $("#tdLinkdinSignIn").show();
            $("#btnLinkdinSignIn").click(function () {
                UI_SocialCustomer.linkedInsignInClick();
            });
        } catch (e) {}
    } else {
        $("#tdLinkdinSignIn").remove();
    }

    if (!(UI_SocialCustomer.params.showFacebook || UI_SocialCustomer.params.showLinkedln)) {
        $(".socailBtnWrapper").hide();
    }
}

/**
 * Facebook Specific
 */
UI_SocialCustomer.facebooksignInClick = function () {

    function updateButton(response) {

        var button = document.getElementById('btnFBSignIn');

        if (response.authResponse) {
            //user is already logged in and connected
            FB.api('/me', function (response) {
                UI_SocialCustomer.fillSocialData(response, "FACEBOOK");
                if (!UI_SocialCustomer.passedToBackend) {
                    UI_SocialCustomer.passedToBackend = true;
                    UI_SocialCustomer.loginFromSocialSites();
                }
            });

        } else {
            //user is not connected to the app or logged out
            FB.login(function (response) {
                if (response.authResponse) {
                    // user is logged in but not connected
                    FB.api('/me', function (response) {
                        UI_SocialCustomer.fillSocialData(response, "FACEBOOK");
                        if (!UI_SocialCustomer.passedToBackend) {
                            UI_SocialCustomer.passedToBackend = true;
                            UI_SocialCustomer.loginFromSocialSites();
                        }
                    });
                } else {
                    //user cancelled login or did not grant authorization
                }
            }, {
                scope: 'email'
            });
        }
    }

    FB.getLoginStatus(updateButton);
    FB.Event.subscribe('auth.statusChange', updateButton);
}

UI_SocialCustomer.linkedInsignInClick = function () {
    if ((typeof IN != 'undefined') && (typeof IN.User != 'undefined')) {
        IN.User.authorize(function () {
            IN.API.Profile("me").fields("firstName", "lastName", "industry", "pictureUrl", "emailAddress", "id").result(UI_SocialCustomer.displayProfiles).error(UI_SocialCustomer.error);
        });
    }
}

UI_SocialCustomer.displayProfiles = function (profiles) {
    member = profiles.values[0];
    UI_SocialCustomer.fillSocialData(member, "LINKEDIN");
    UI_SocialCustomer.loginFromSocialSites();
}

UI_SocialCustomer.error = function (error) {
    //do nothing
}

/**
 * Social Sites - common
 */
UI_SocialCustomer.fillSocialData = function (response, site) {

    if (site == "FACEBOOK") {
        document.getElementById("exEmail").value = response.email;
        document.getElementById("exFirstName").value = response.first_name;
        document.getElementById("exGender").value = response.gender;
        document.getElementById("exId").value = response.id;
        document.getElementById("exLastName").value = response.last_name;
        document.getElementById("exLink").value = response.link;
        document.getElementById("exName").value = response.name;
        document.getElementById("exSiteType").value = 1; //FACEBOOK =1
    } else if (site == "LINKEDIN") {
        document.getElementById("exEmail").value = response.emailAddress;
        document.getElementById("exFirstName").value = response.firstName;
        //document.getElementById("exGender").value = response.gender;
        document.getElementById("exId").value = response.id;
        document.getElementById("exLastName").value = response.lastName;
        document.getElementById("exLink").value = response.pictureUrl;
        document.getElementById("exName").value = response.name;
        document.getElementById("exSiteType").value = 3; //LINKEDIN =3
    }
}

UI_SocialCustomer.loginFromSocialSites = function () {
    UI_commonSystem.loadingProgress();
    $("#frmLogin").attr('action', SYS_IBECommonParam.securePath + 'customerLoginFromSocialSite.action');
    $("#frmLogin").ajaxSubmit({
        dataType: 'json',
        beforeSubmit: UI_SocialCustomer.progressSetup,
        success: UI_SocialCustomer.redirect,
        error: UI_commonSystem.setErrorStatus
    });
    return false;
}

UI_SocialCustomer.redirect = function (response) {
    UI_commonSystem.loadingCompleted();
    if (response != null && response.success == true) {
        if (response.driveCode != "") {
            if (response.driveCode == "redirectToRegistrationWithAutoFill") {
                UI_SocialCustomer.redirectToRegistration();
            }

            if (response.driveCode == "allowSocialLogging") {
                UI_CommonCustomer.loadCustomerLoginSucess(response);
            }
        }
    } else {
        UI_commonSystem.loadErrorPage({
            messageTxt: response.messageTxt
        });
    }
}

UI_SocialCustomer.redirectToRegistration = function () {
    var strSubmitUrl = SYS_IBECommonParam.securePath + 'showReservation.action?hdnParamData=' + SYS_IBECommonParam.locale + '^RE';
    autoFill = {
        'email': $("#exEmail").val(),
        'firstName': $("#exFirstName").val(),
        'gender': $("#exGender").val(),
        'lastName': $("#exLastName").val(),
        'name': $("#exName").val(),
        'id': $("#exId").val(),
        'link' : $("#exLink").val(),
        'siteType': $("#exSiteType").val()
    };
    var url = strSubmitUrl + '&autoFillData=' + $.toJSON(autoFill);
    location.replace(url);
}

/**
 * Login Common
 */
UI_CommonCustomer.loadCustomerLoginSucess = function (response) {
    UI_commonSystem.loadingCompleted();
    UI_commonSystem.loyaltyManagmentEnabled = response.loyaltyManagmentEnabled;
    if (response != null && response.success == true) {
        if (response.messageTxt != "") {
            // Login Fail Handle
            $("#lblLoginStatus").text(response.messageTxt);
            $("#frmLogin input[type='password']").val('');
        } else {
            // Update Common params
            SYS_IBECommonParam.updateCommonParam(response.commonParams);
            // Login Success			
            $("#frmLogin").attr('action', 'showCustomerLoadPage!loadCustomerHomePage.action');
            $("#frmLogin").submit();
            //UI_commonSystem.loadingProgress();
        }

    } else {
        UI_commonSystem.loadErrorPage({
            messageTxt: response.messageTxt
        });
    }
}