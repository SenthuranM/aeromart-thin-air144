/**
 * 
 * Author : Pradeep Karunanayake
 */

function UI_ReservationCredit(){} 
/**
 * Customer Credit page panels
 */
UI_ReservationCredit.panels = "";
UI_ReservationCredit.requestAction = false;

if (UI_CustomerHome.isLoyaltyExist == true) {
	UI_ReservationCredit.panels = ["CustomerCreditHD","loyaltyCreditHD","creditUsageHD","CustomerCreditTable", "loyaltyCreditTable", "creditUsageTable"];
} else {
	UI_ReservationCredit.panels = ["CustomerCreditHD","CustomerCreditTable"];
}



/**
 * Page ready
 */
UI_ReservationCredit.ready = function() {
	UI_ReservationCredit.pageBuild();
	$("label[name=currencyCode]").text(UI_Top.holder().GLOBALS.baseCurrency);
	UI_ReservationCredit.customerCreditClick();	 	
	$("#btnCustomerCredit").click(function(){UI_ReservationCredit.customerCreditClick();});
	if(UI_commonSystem.integrateMashreqWithLMSEnabled){
		$("#btnMashCre").hide();
		$("#btnMashCreUse").hide();
	}
	$("#btnMashCre").click(function(){UI_ReservationCredit.customerMasreqCreditClick();});
	$("#btnMashCreUse").click(function(){UI_ReservationCredit.customerMasreqCreditUsageClick();});	
}
/**
 * Page Build For Loyalty Account
 */
UI_ReservationCredit.pageBuild = function() {
	if (UI_CustomerHome.isLoyaltyExist == false) {
		$("#loyaltyCreditHD").hide();
		$("#creditUsageHD").hide();
		$("#loyaltyCreditTable").hide();
		$("#creditUsageTable").hide();	
		$("#btnMashCre").hide();
		$("#btnMashCreUse").hide();
	}
}
/**
 * Request credit data
 */
UI_ReservationCredit.reservationCreditPageData = function() {	
	
	$("#tdReservationCredit").ajaxSubmit({url:"showCustomerCreditV2.action?randomNumber="+UI_UserTabs.getRandomNumber() ,type: "POST", dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress,  success: UI_ReservationCredit.reservationCreditPageDataSucess, error:UI_commonSystem.setErrorStatus});
	return false;
}
/**
 * Credit data success
 */
UI_ReservationCredit.reservationCreditPageDataSucess = function(response) {	
	if(response != null && response.success == true) {
		$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
		$("#divRedeem").html(response.jsonLabel.lblRedeem);
		UI_ReservationCredit.callCenterLinkSetup();
		$("#creditDetailsTemplate").nextAll().remove();
		$("#creditDetailsTemplate").iterateTempleteReservationCredit({templeteName:"creditDetailsTemplate", data:response.customerCreditDTO, dtoName:"customerCreditDTO"});
		UI_ReservationCredit.showCreditPage({panel:["CustomerCreditHD","CustomerCreditTable"]});
		UI_ReservationCredit.buttonSetUp({id:"btnCustomerCredit"});
		UI_UserTabs.clearPanels();
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_ReservationCredit.requestAction = false;
	
	UI_commonSystem.loadingCompleted();
}
/**
 * Credit Button Click
 */
UI_ReservationCredit.customerCreditClick = function() {	
	if(!UI_ReservationCredit.requestAction){
		
		UI_ReservationCredit.disableAllButton();
		UI_ReservationCredit.requestAction = true;
		
		$("#CustomerCreditHD").slideDown();
		$("#CustomerCreditTable").slideDown();
		$("#loyaltyCreditHD").hide();
		$("#loyaltyCreditTable").hide();	
		$("#creditUsageHD").hide();
		$("#creditUsageTable").hide();
		
		setTimeout('UI_ReservationCredit.reservationCreditPageData()',10);		
	} else {
		alert("Please Wait..");
	}
}
/**
 * Masreq credit click
 */
UI_ReservationCredit.customerMasreqCreditClick = function() {
	
	if(!UI_ReservationCredit.requestAction){	
		UI_ReservationCredit.disableAllButton();
		UI_ReservationCredit.requestAction = true;
		
		$("#CustomerCreditHD").hide();
		$("#CustomerCreditTable").hide();
		$("#loyaltyCreditHD").slideDown();
		$("#loyaltyCreditTable").slideDown();	
		$("#creditUsageHD").hide();
		$("#creditUsageTable").hide();
		setTimeout('UI_ReservationCredit.loadMasreqCreditData()',10);
		
	}else {
		alert("Please Wait..");
	}
}
/**
 * Credit usage click
 */
UI_ReservationCredit.customerMasreqCreditUsageClick = function() {

	if(!UI_ReservationCredit.requestAction){
		
		UI_ReservationCredit.disableAllButton();
		UI_ReservationCredit.requestAction = true;
		
		$("#CustomerCreditHD").hide();
		$("#CustomerCreditTable").hide();
		$("#loyaltyCreditHD").hide();
		$("#loyaltyCreditTable").hide();	
		$("#creditUsageHD").slideDown();
		$("#creditUsageTable").slideDown();
		
		setTimeout('UI_ReservationCredit.loadMasreqCreditUsageData()',10);		
	} else {
		alert("Please Wait..");
	}
}

UI_ReservationCredit.disableAllButton = function() {
		if (!UI_CustomerHome.isLoyaltyExist) {
			$("#btnCustomerCredit").remove();
		}
		UI_ReservationCredit.disableButton({id:"btnCustomerCredit"});
		UI_ReservationCredit.disableButton({id:"btnMashCre"});
		UI_ReservationCredit.disableButton({id:"btnMashCreUse"});
}

/**
 * Disable Credit button
 */
UI_ReservationCredit.disableButton = function(data) {
	if (data.id == 'btnCustomerCredit') {
		$("#"+data.id).addClass("btnCDisable").disable();
	} else {
		$("#"+data.id).addClass("btnCLargeDisable").disable();
	}
}
/**
 * Enable Buttons
 */
UI_ReservationCredit.enableButtons = function(data) {
	$("#btnMashCre").removeClass("btnCLargeDisable").enable();
	$("#btnMashCreUse").removeClass("btnCLargeDisable").enable();
	$("#btnCustomerCredit").removeClass("btnCDisable").enable();
}
/**
 * Page Button setup
 */
UI_ReservationCredit.buttonSetUp = function(data) {
	UI_ReservationCredit.enableButtons();
	UI_ReservationCredit.disableButton(data);
}
/**
 * Request Mashreq credit data
 */
UI_ReservationCredit.loadMasreqCreditData = function() {	
	$("#tdReservationCredit").ajaxSubmit({url:"showCustomerLoyaltyCredit.action?randomNumber="+UI_UserTabs.getRandomNumber() , type: "POST",dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress,  success: UI_ReservationCredit.loadMasreqCreditDataSucess, error:UI_commonSystem.setErrorStatus});
	return false;
}
/**
 * Request data success
 */
UI_ReservationCredit.loadMasreqCreditDataSucess = function(response) {
	if(response != null && response.success == true) {
		if (response.loyaltyCreditDTO.length > 0) {
			$("#loyaltyCreditTemplate").nextAll().remove();
			$("#loyaltyCreditTemplate").iterateTemplete({templeteName:"loyaltyCreditTemplate", data:response.loyaltyCreditDTO, dtoName:"loyaltyCreditDTO"});
			UI_ReservationCredit.showCreditPage({panel:["loyaltyCreditHD","loyaltyCreditTable"]});
		} else {
			$("#tdSubHeadLoyaltyCreNo").show();
			UI_ReservationCredit.showCreditPage({panel:["loyaltyCreditHD"]});
		}		
		UI_ReservationCredit.buttonSetUp({id:"btnMashCre"});
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_ReservationCredit.requestAction = false;
	UI_commonSystem.loadingCompleted();
}
/**
 * Request Mashreq  Credit Usage data
 */
UI_ReservationCredit.loadMasreqCreditUsageData = function() {	
	$("#tdReservationCredit").ajaxSubmit({url:"showCustomerLoyaltyCreditUsage.action?randomNumber="+UI_UserTabs.getRandomNumber() , type: "POST",dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress,  success: UI_ReservationCredit.loadMasreqCreditUsageDataSucess, error:UI_commonSystem.setErrorStatus});
	return false;
}
/**
 * Request data success
 */
UI_ReservationCredit.loadMasreqCreditUsageDataSucess = function(response) {
	if(response != null && response.success == true) {
		if (response.loyaltyCreditUsageDTO.length > 0) {
			$("#creditUsageTemplate").nextAll().remove();
			$("#creditUsageTemplate").iterateTempleteReservationCredit({templeteName:"creditUsageTemplate", data:response.loyaltyCreditUsageDTO, dtoName:"loyaltyCreditUsageDTO"});			
			UI_ReservationCredit.showCreditPage({panel:["creditUsageHD","creditUsageTable"]});
		} else {
			$("#trSubHeadCreditUsage").show();
			UI_ReservationCredit.showCreditPage({panel:["creditUsageHD"]});
		}		
		UI_ReservationCredit.buttonSetUp({id:"btnMashCreUse"});
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_ReservationCredit.requestAction = false;
	UI_commonSystem.loadingCompleted();
}
/**
 * Show Credit page
 */
UI_ReservationCredit.showCreditPage = function(data) {
	$.each(UI_ReservationCredit.panels, function(index, value) { 
		$("#"+value).hide(); 
	});
	$.each(data.panel, function(index, value) { 
		$("#"+value).slideDown(); 
	});
}
/**
 * Template functionality for Display data
 */
$.fn.iterateTempleteReservationCredit = function(params) {
	
	var templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;

	var dtoRecord ;
	var totalCredit = 0;
	var groupPnr = "";
	//iterate the dto list
	$("#" + templeteName).nextAll().remove();
	for(var i=0; i < params.data.length ; i++) {
		
		clonedTemplateId = templeteName + "_" + i;
		//clone the template row
		var clone = $( "#" + templeteName + "").clone();
		//set the id for the clone (to avoid to have the same id)
		clone.attr("id", clonedTemplateId );
		//append clone to the parent of the template
		clone.appendTo($("#" + templeteName).parent());  

		//get i th dto of the dto list
		dtoRecord = params.data[i];
		//appender for the element names (eg student[0].name)
		elementIdAppend = params.dtoName + "[" + i + "].";

		//setting the value and id of labels
		$("#"+clonedTemplateId).find("label").each(function( intIndex ) {
			var value =  dtoRecord[this.id];
			var groupPNR = dtoRecord.groupPnr;			
			$(this).text(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
			
			if ((this.id).split(".")[1] == 'pnr') {	
				var pnrArray = value.split("-");
				$(this).text(pnrArray[0]);
				$(this).click(function(){UI_ReservationCredit.callPnrSubmit({pnr:pnrArray[0], groupPnr: groupPNR});});
			}
						
		});	
		$( "#" + clonedTemplateId).show();
		totalCredit = Number(totalCredit) + Number(params.data[i].balance);
	}
	// Set credit amount
	if (totalCredit != 0 ) {
		UI_ReservationCredit.setTotalCredit({totCreditAmount:totalCredit});
	} else {
		UI_ReservationCredit.setTotalCredit({totCreditAmount:totalCredit});
		UI_ReservationCredit.pageSetUpNoRes();
	}
	$( "#" + templeteName).hide();
}

UI_ReservationCredit.callPnrSubmit =  function(data) {
	UI_CustomerHome.pnr = data.pnr;
	UI_CustomerHome.groupPnr = data.groupPnr;
	UI_UserTabs.pnrClick();		
}



UI_ReservationCredit.setTotalCredit = function(data) {
	$("#lblCreditAmt").text($("#spnCerdit").text());	
}

UI_ReservationCredit.pageSetUpNoRes = function() {
	$("#spnCredits").hide();
	$("#lblSubHead2").hide();
	$("#lblSubHead1").show();
}

/**
 * Call Center Link
 */
UI_ReservationCredit.callCenterLinkSetup = function() {
	if(UI_Top.holder().GLOBALS.clientCallCenterURL === ""){
		$("#linkCallCenter").attr("href", SYS_IBECommonParam.homeURL + "/callcentres");
	} else {
		$("#linkCallCenter").attr("href", UI_Top.holder().GLOBALS.clientCallCenterURL).attr("target","_blank");
	}
}

UI_ReservationCredit.ready();
