/**
 * Author : Pradeep Karunanayake
 */	

function UI_ReservationList(){}

var config = UI_Top.holder().GLOBALS.isShowAlertsInIBEEnabled; 
var arrError = new Array();
var errorInfo  = "";
UI_ReservationList.isBecomeMashreqUser=false;

/**
 * Page ready
 */
UI_ReservationList.ready = function() {
	UI_ReservationList.loadPageData();
	$("#btnRegisterCommon").click(function(){UI_ReservationList.activateLMSButtonClick();});
	$("#textPpnLMS").alphaNumeric();
	
	var now = new Date();
	var yearRange = (now.getFullYear() - 100) + ":" + now.getFullYear();
	
	$("#textDOBLMS").datepicker({
		regional: SYS_IBECommonParam.locale ,
		dateFormat: "dd/mm/yy",
		showOn: 'both',
		maxDate:'-2Y',
		minDate: "-99Y +1D",
		yearRange: yearRange,
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
	});
	
	$("#textDOBLoyalty").datepicker({
		regional: SYS_IBECommonParam.locale ,
		dateFormat: "dd/mm/yy",
		showOn: 'both',
		maxDate:'-2Y',
		minDate: "-99Y +1D",
		yearRange: yearRange,
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
	});
	
	$("#textDOBLMS").change(function(){
		var dob = $("#textDOBLMS").datepicker("getDate");
		var present = new Date();
		var age = (present.getTime() - dob.getTime())/1000/60/60/24;
		if (age < (12*365+3)){
			$("#lblEmailHMandLMS").show();
		}else{
			$("#lblEmailHMandLMS").hide();
		}
	});
	
	//if(UI_CustomerHome.lmsJoined && UI_commonSystem.lmsDetails==null){
		//UI_ReservationList.lmsCheckChange();
	//}	
	UI_ReservationList.loadAirewardPanel();
	UI_ReservationList.loadLoyaltyEnrollPanel();	
	UI_ReservationList.showHideRewardsPanels();
	UI_ReservationList.showHideButton();
}

UI_ReservationList.loadLoyaltyEnrollPanel = function(){
	$("input[name='optCardAccLoyalty']").change(function(){
		UI_ReservationList.loyaltyCheckChange();
	});
	
	$('input[name="optCardAccLoyalty"][value="N"]').prop('checked', true);
	UI_ReservationList.loyaltyCheckChange();
	
	if (UI_CustomerHome.isLoyaltyExist == true || UI_Top.holder().GLOBALS.loyaltyEnable == false) {
		$("#trActivateLoyalty").remove();
	} 

	if (UI_CustomerHome.isLoyaltyExist == false && UI_Top.holder().GLOBALS.loyaltyEnable == true) {
		$("#trActivateLoyalty").show();
	} 
	
	//if(UI_commonSystem.integrateMashreqWithLMSEnabled){
		//$("#trActivateLoyalty").hide();
	//}
}

UI_ReservationList.loadAirewardPanel = function(){
	$("input[name='cusOptLMS']").change(function(){
		UI_ReservationList.lmsCheckChange();
	});
	
	$('input[name="cusOptLMS"][value="N"]').prop('checked', true);;
	UI_ReservationList.lmsCheckChange();	
	
	if(!UI_commonSystem.loyaltyManagmentEnabled){
		$("#trAirwards").hide();
	}
}

UI_ReservationList.showHideButton = function(){
	$("#btnRegisterCommon").hide();
	//if(UI_CustomerHome.isLoyaltyExist!=true && UI_Top.holder().GLOBALS.loyaltyEnable==true){
	//	$("#btnRegisterCommon").show();
	//}
	//if(UI_commonSystem.loyaltyManagmentEnabled&&UI_commonSystem.lmsDetails==null){
	//	$("#btnRegisterCommon").show();
	//}   
	///// above this is old code
	var loyaltyEnrollEnabled=false;
	var lmsEnrollEnabled=false;
	if($('#trActivateLoyalty').css('display') == 'block'){		
		var isLoyaltyAdd = $("input[name='optCardAccLoyalty']:checked").val();
	    if (isLoyaltyAdd==="Y"){	    	
	    	loyaltyEnrollEnabled=true;
	    }	   
	}
	if($('#trAirwards').css('display') == 'block'){
		var isLmsAdd = $("input[name='cusOptLMS']:checked").val();
		if(isLmsAdd==="Y"){
			lmsEnrollEnabled = true;
		}
	}
	if(lmsEnrollEnabled || loyaltyEnrollEnabled){
		$("#btnRegisterCommon").show();
	}	
}

UI_ReservationList.loyaltyCheckChange = function(){
	var isLoyaltyAdd = $("input[name='optCardAccLoyalty']:checked").val();
    if (isLoyaltyAdd==="Y"){
    	if(UI_commonSystem.lmsDetails!=null && UI_commonSystem.lmsDetails.emailStatus!=null && UI_commonSystem.lmsDetails.emailStatus!=""){
	    	$("#trActivateLoyalty .extractor").show();
			UI_ReservationList.isBecomeMashreqUser = true;
	    }else{
	    	var isLmsAdd = $("input[name='cusOptLMS']:checked").val();
	    	var lmsyes=false;
		    if (isLmsAdd==="Y"){
		    	if($('#trAirwards').css('display') == 'block'){
		    		lmsyes = true;
		    	}		    	
			}
		    //UI_commonSystem.integrateMashreqWithLMSEnabled
		    if(UI_commonSystem.integrateMashreqWithLMSEnabled){
		    	if(lmsyes){
			    	$("#trActivateLoyalty .extractor").show();
					UI_ReservationList.isBecomeMashreqUser = true;
			    }else{
			    	alert("Please Enroll for Aireward as well!");
			    	$('input[name="optCardAccLoyalty"][value="N"]').prop('checked', true);
			    }
		    }else{
		    	$("#trActivateLoyalty .extractor").show();
				UI_ReservationList.isBecomeMashreqUser = true;
		    }
	    } 
	    
	}else{
		$("#trActivateLoyalty .extractor").hide();
		UI_ReservationList.isBecomeMashreqUser = false;
	}
    UI_ReservationList.showHideButton();
}

UI_ReservationList.lmsCheckChange = function(){
	var islmsAdd = $("input[name='cusOptLMS']:checked").val();
    if (islmsAdd==="Y"){
    	$("#trActivateLoyalty").show();
		$("#errorSpan").html("");
		$("#lmsActivate_reservationList").show();
		$("#trDOBLoyalty").hide();
	}else{
		$("#trDOBLoyalty").show();
		$("#lmsActivate_reservationList").hide();
		if(UI_commonSystem.integrateMashreqWithLMSEnabled){
			$('input[name="optCardAccLoyalty"][value="Y"]').prop('checked', false);
			$('input[name="optCardAccLoyalty"][value="N"]').prop('checked', true);
			UI_ReservationList.loyaltyCheckChange();
			$("#trActivateLoyalty").hide();
		}
	}
    UI_ReservationList.showHideButton();
}

/**
 * Load page data
 */
UI_ReservationList.loadPageData = function () {	
	UI_commonSystem.loadingProgress();
	$("#tdReservationList").ajaxSubmit({url:"showReservationList.action?randomNumber="+UI_UserTabs.getRandomNumber() , dataType: 'json', success: UI_ReservationList.loadReservationDataSuccess, error:UI_commonSystem.setErrorStatus});	
	return false;	
} 

/**
 * Load data success
 */
UI_ReservationList.loadReservationDataSuccess = function(response){		
	if(response != null && response.success == true) {	
		$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
        UI_ReservationList.showHideRewardsPanels();
		UI_ReservationList.buildReservations(response);
		UI_UserTabs.clearPanels();
        dateChangerjobforLanguage(SYS_IBECommonParam.locale);
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}

	if(UI_UserTabs.activateLms && UI_commonSystem.lmsDetails==null){
		UI_ReservationList.lmsCheckChange(); // was join lms
	}
	UI_commonSystem.loadingCompleted();
	
}

/**
 * show hide Airewards and Mashreq panels according to the status of the program
 * @param data
 */
UI_ReservationList.showHideRewardsPanels = function(){
	if(!UI_commonSystem.loyaltyManagmentEnabled){
		$("#trAirwards").hide();
	}else{
		if(UI_commonSystem.lmsDetails == null || UI_commonSystem.lmsDetails.emailId == null 
				|| UI_commonSystem.lmsDetails.emailId == ""){
			$("#trAirwards").show();	
			$("#trDOBLoyalty").hide();	
			
			$('input[name="cusOptLMS"][value="Y"]').prop('checked', true);
			UI_ReservationList.lmsCheckChange();
		}
		else{
			$("#trAirwards").hide();
		}
	}
}

UI_ReservationList.resetJoinLoyaltyPanel = function(){
	$("#errorLms").html("");
	$("#textDOBLMS").val("");
	$("#textLanguageLMS").val("");
	$("#textPpnLMS").val("");
	$("#textRefferedEmailLMS").val("");
	$("#textEmailHLMS").val("");
	$("#lmsJoinNow_reservationList").show();
	$("#lmsActivate_reservationList").hide();
}

// To Do: Remove Html from js file
// To Do: Implement template method
UI_ReservationList.buildReservations = 	function(data) {		
		var strHTMLText = '';
		$("#frmResList").find("#resTable").html("");
		var intLength = data.reservationListTO.length;
		var intRowCount = 0 
		var strRowSpan  = "";
		if (intLength > 0){
			
			var strColor = "SeperateorBGColor";
			for (var i = 0 ; i < intLength ; i++){
				var intRowCount = data.reservationListTO[i].flightInfo.length ;
				strRowSpan = "";
				if (intRowCount > 0){
				   strRowSpan = 'rowspan = "' + intRowCount + '" ' ;
				}
				
				if (strColor == "SeperateorBGColor"){
					strColor = "";
				}else{
					strColor = "SeperateorBGColor";
				}
				
				for (var x = 0 ; x < intRowCount ; x++){

				    strHTMLText = '<tr>';
					
					if (x == 0){	
						strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center" valign="top" ' + strRowSpan  + ' title="' + 'Click here to get details of Reservation' + '">';
						strHTMLText += '		<a href="javascript:UI_ReservationList.callPnrSubmit(' +  "'" +  data.reservationListTO[i].pnrNo + "','" +  data.reservationListTO[i].originatorPnr  + "'" + ')"><font><u>' + data.reservationListTO[i].pnrNo + '<\/u><\/font><\/a>';
						strHTMLText += '	<\/td>';
					}
					strHTMLText += '	<td class="alignLeft GridItems GridDblHighlight ' + strColor + '">';
					strHTMLText += '		<label>' + data.reservationListTO[i].flightInfo[x].orignNDest + '<\/label>';
					if(data.reservationListTO[i].originatorPnr !="" && config =='true'){
						strHTMLText += '		<img src="../images/AA169_no_cache.gif" onmouseover="UI_ReservationList.showFareFamily(event,' +  "'" + 'arrRes[i][1][x][4]' + "'" + '); return false;" onmouseout="hideTT()">';
					}
					strHTMLText += '	<\/td>';
					strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
					strHTMLText += '		<label id="departureDate" class="date-disp">' + data.reservationListTO[i].flightInfo[x].departureDate + '<\/label>' +
                        '<label id="departureDateValue" class="date-hid" style="display:none">'+data.reservationListTO[i].flightInfo[x].departureDateValue+'</label>  <label>' +data.reservationListTO[i].flightInfo[x].departureTime +'<\/label>';
					strHTMLText += '	<\/td>';
					strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
					strHTMLText += '		<label id="arrivalDate"  class="date-disp">' + data.reservationListTO[i].flightInfo[x].arrivalDate + '<\/label>' +
                        ' <label id="arrivalDateValue" class="date-hid" style="display:none">'+data.reservationListTO[i].flightInfo[x].arrivalDateValue+'</label>  <label>' + data.reservationListTO[i].flightInfo[x].arrivalTime +'<\/label>';
					strHTMLText += '	<\/td>';
					strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center">';
					strHTMLText += '		<img src="../images/' + UI_ReservationList.getStatus(data.reservationListTO[i].flightInfo[x].status) + '" title="' + 'Segment Status : ' + UI_ReservationList.getStatusDesc(data.reservationListTO[i].flightInfo[x].status)  + '">';
					strHTMLText += '	<\/td>';
					
					if (x == 0){						
						strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center" ' + strRowSpan  + '>';
						strHTMLText += '		<img src="../images/' + UI_ReservationList.getStatus(data.reservationListTO[i].pnrStatus) + '" title="' + 'Reservation Status : ' + UI_ReservationList.getStatusDesc(data.reservationListTO[i].pnrStatus)  + '">';
						
						strHTMLText += '	<\/td>';								
					} 								 
					strHTMLText += '<\/tr>';
					
					$("#frmResList").find("#resTable").append(strHTMLText);

				}
			}
			$("#frmResList").find("#lblMsgResNo").hide();			
		} else {
			$("#frmResList").find("#trResList").hide();
			$("#frmResList").find("#lblMsgClickRes").hide();
		}	    
	}
	/**
	 * PNR Click
	 */
	UI_ReservationList.callPnrSubmit =  function(stPnr, groupPnr) {
		$("#reservationList").slideUp("slow");
		UI_CustomerHome.pnr = stPnr;
		UI_CustomerHome.groupPnr = groupPnr;
		UI_UserTabs.pnrClick();		
	}
	/**
	 * Get Reservation Status
	 */	
	UI_ReservationList.getStatus =  function(strPNRStatus){	
		var strHTMLText = ""
		switch (strPNRStatus){
			case "CNF" : strHTMLText = "AA175_no_cache.gif"; break;
			case "CNX" : strHTMLText = "AA163_no_cache.gif"; break;
			case "OHD" : strHTMLText = "AA176_no_cache.gif"; break;
			case "CLS" : strHTMLText = "spacer_no_cache.gif"; break;
			case "FLN" : strHTMLText = "spacer_no_cache.gif"; break;
			case "WHD" : strHTMLText = "spacer_no_cache.gif"; break;
		}		
		return strHTMLText;									
	}
    /**
     * Show Fare Family
     */
	// To Do : Review
	UI_ReservationList.showFareFamily = function(objEvent,text){		
		var strTTText	= "";
		strTTText		+= "			<font class='fntBold'>"+ text +"<\/font>";
		showTT(strTTText, "top", "LEFT", objEvent, -30, 20, 350);
	}
	/**
	 * Get Reservation Status Description
	 */
	UI_ReservationList.getStatusDesc =  function(strPNRStatus){
		var strHTMLText = ""
		switch (strPNRStatus){
			case "CNF" : strHTMLText = "Confirmed"; break;
			case "CNX" : strHTMLText = "Cancelled"; break;
			case "OHD" : strHTMLText = "Onhold"; break;
			case "CLS" : strHTMLText = ""; break;
			case "FLN" : strHTMLText = "Flown"; break;
			case "WHD" : strHTMLText = ""; break;
		}		
		return strHTMLText;				
	}
	
	UI_ReservationList.activateLMSButtonClick = function(){
		// only mashreq
		//only lms
		// both mashreq lms
		var isAddLoyalty = $("input[name='optCardAccLoyalty']:checked").val();
		var addingMashreq=false;
	    if (isAddLoyalty==="Y"){
	    	addingMashreq=true;
	    }
	    
	    var isAddLMS = $("input[name='cusOptLMS']:checked").val();
	    var addingLms=false;
	    if (isAddLMS==="Y"){ 
	    	if($('#trAirwards').css('display') == 'block'){
	    		addingLms=true;
	    	}
	    }
		
		if(!addingLms && addingMashreq){
			if (UI_CustomerHome.isLoyaltyExist == false && UI_Top.holder().GLOBALS.loyaltyEnable == true) {			
				UI_ReservationList.mashreqOnlyEnroll();
			}
		}else{
			if(UI_commonSystem.loyaltyManagmentEnabled){
				UI_ReservationList.lmsHeadRefferedValidate();
			}
		}		
	}
	
	UI_ReservationList.lmsAjaxRegister = function() {
		$("#errorSpan").html("");
		data = {};
		url = SYS_IBECommonParam.securePath + "lmsAjaxRegister.action";
		
		var isAddLoyalty = $("input[name='optCardAccLoyalty']:checked").val();
	    if (isAddLoyalty==="Y"){
	    	data['loyaltyCity'] = $("#txtCityLoyalty").val();
			data['loyaltyAccountNo'] = $("#txtAccountNoLoyalty").val();
			data['loyaltyDateofBirth'] = $("#textDOBLoyalty").val();
			data['enrollLoyalty'] = true;
		}// this should come before lms coz, below replaces the dob
		
		var isAddLMS = $("input[name='cusOptLMS']:checked").val();
	    if (isAddLMS==="Y"){ 
	    	if($('#trAirwards').css('display') == 'block'){
	    		data['lmsDetails.dateOfBirth'] = $("#textDOBLMS").val();
		    	data['loyaltyDateofBirth'] = $("#textDOBLMS").val(); // copy lms date to loyalty
				data['lmsDetails.passportNum'] = $("#textPpnLMS").val();
				data['lmsDetails.headOFEmailId'] = $("#textEmailHLMS").val();
				data['lmsDetails.refferedEmail'] = $("#textRefferedEmailLMS").val();
				data['lmsDetails.language'] = $("#textLanguageLMS").val();
				data['lmsDetails.appCode'] = "APP_IBE";
				data['enrollLms'] = true;
	    	}	    	
		}	    
	    	
		
		$.ajax({
			url : url,
			beforeSend : UI_commonSystem.loadingProgress(),
			data : data,
			type : "POST",
			dataType : "json",
			success : function(response) {
				if(response != null ) {
					var hide=true;
					if($('#trActivateLoyalty').css('display') == 'block'){
						var isLoyaltyAdd = $("input[name='optCardAccLoyalty']:checked").val();
					    if (isLoyaltyAdd==="Y"){
					    	if(response.mashreqSuccess){
					    		$("#trActivateLoyalty").hide();
					    	}else{
					    		hide=false;
					    	}					    		
						}else{
							hide=false;					
						}
					}
					if($('#trAirwards').css('display') == 'block'){
						var isLmsAdd = $("input[name='cusOptLMS']:checked").val();
					    if (isLmsAdd==="Y"){
					    	if(response.lmsSuccess){
					    		$("#trAirwards").hide();
					    	}else{
					    		hide=false;
					    	}					    	
						}else{
							hide=false;					
						}
					}
					if(response.mashreqSuccess){						
						UI_CustomerHome.isLoyaltyExist = true; // mashreq activated
					}
					if(response.lmsSuccess){						
						UI_commonSystem.lmsDetails.emailStatus = response.lmsDetails.emailStatus;
						UI_UserTabs.showLmsStatus();
						$("#trDOBLoyalty").show();
					}
					if (hide) {
						$("#btnRegisterCommon").hide(); 
					} 
					if(response.success == true){
						if (response.messageTxt != null && $.trim(response.messageTxt) != "") {	
							$("#errorSpan").html("<font class='mandatory'>" + response.messageTxt + "<\/font>");												
						} 
					}else {												
						UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});					
					}					
				} else {
					UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
				}
				UI_commonSystem.loadingCompleted();
				UI_ReservationList.showHideButton();
		}
});
		return false;
	}
	
	UI_ReservationList.validateLmsFields = function(){
		var proceed = true;
		var strErrorHTML = "";
		
		var isAddLMS = $("input[name='cusOptLMS']:checked").val();
		var validateLms=false;
	    if (isAddLMS==="Y"){
	    	if($('#trAirwards').css('display') == 'block'){
	    		validateLms = true;
	    	}	    	
		}
	    
	    var isAddLoyalty = $("input[name='optCardAccLoyalty']:checked").val();
		var validateLoyalty=false;
	    if (isAddLoyalty==="Y"){
	    	validateLoyalty = true;
		}
		
		var lmsDOB = $("#textDOBLMS").val();
		var lmsRefEmail = $("#textRefferedEmailLMS").val();
		var lmsEmailH = $("#textEmailHLMS").val();
		
		var loyaltyDOB = $("#textDOBLoyalty").val();
		var loyaltyCity = $("#txtCityLoyalty").val();
		var loyaltyAcc = $("#txtAccountNoLoyalty").val();
		
				
		if(validateLoyalty){
			if(loyaltyAcc==""){
				strErrorHTML += "<li> " + "Account number required";
				$("#txtAccountNoLoyalty").addClass("errorControl");			
			}
			if(loyaltyCity=="") {
				strErrorHTML += "<li> " + "City Required";
				$("#txtCityLoyalty").addClass("errorControl");				
			}
			// if not hide dob 
			
			if(UI_commonSystem.lmsDetails == null || UI_commonSystem.lmsDetails.emailId == null 
					|| UI_commonSystem.lmsDetails.emailId == ""){
				if(UI_commonSystem.loyaltyManagmentEnabled){
					if(loyaltyDOB==""){
						$("#textDOBLoyalty").addClass("errorControl");
						strErrorHTML += "<li> " +  "Date of Birth Required";
						proceed = false;
					}else if(!dateValidDate(loyaltyDOB)){
						strErrorHTML += "<li> " +  "Date of Birth Required";
						$("#textDOBLoyalty").addClass("errorControl");
						proceed = false;
					}
				}		
			}	
			var showLms=false;
			if($('#trAirwards').css('display') == 'block'){
				var isLmsAdd = $("input[name='cusOptLMS']:checked").val();
				if(isLmsAdd==="Y"){
					showLms = true;
				}
			}
			if(showLms!=true){
				if(UI_commonSystem.loyaltyManagmentEnabled){
					if(loyaltyDOB==""){
						$("#textDOBLoyalty").addClass("errorControl");
						strErrorHTML += "<li> " +  "Date of Birth Required"
					}else if(!dateValidDate(loyaltyDOB)){
						strErrorHTML += "<li> " +  "Date of Birth Required";
						$("#textDOBLoyalty").addClass("errorControl");
						proceed = false;
					}
				}
			}
		}
		
		if(validateLms){
			if(lmsDOB==""){
				$("#textDOBLMS").addClass("errorControl");
				strErrorHTML += "<li> " +  errorInfo.userDateOfBirthRqrd;
				proceed = false;
			}else if(!dateValidDate(lmsDOB)){
				strErrorHTML += "<li> " +  errorInfo.userDateOfBirtIncorrect;
				$("#textDOBLMS").addClass("errorControl");
				proceed = false;
			}else{
	            var dob = $("#textDOBLMS").datepicker("getDate");
	            var present = new Date();
	            var age = (present.getTime() - dob.getTime())/1000/60/60/24;
	            if (age < (12*365+3)){
	            	if(lmsEmailH ==""){
	        			strErrorHTML += "<li> " +  errorInfo.headEmailReqrd;
	                	$("#textEmailHLMS").addClass("errorControl");
	                    proceed = false;
	                }else{
	                	$("#textEmailHLMS").removeClass("errorControl");
	                }
	            }   
			}
				
			if(lmsEmailH!=null && lmsEmailH!=""){
				if(checkEmail(lmsEmailH) == false){
					strErrorHTML += "<li> " +  errorInfo.headEmailIncorrect;
		            $("#textEmailHLMS").addClass("errorControl");
		            proceed = false;
		        }
				else{
					$("#textEmailHLMS").removeClass("errorControl");
				}
			}
				
			if(lmsRefEmail!=null && lmsRefEmail!=""){
				if(checkEmail(lmsRefEmail) == false){
					strErrorHTML += "<li> " +  errorInfo.refEmailIncorrect;
	                $("#textRefferedEmailLMS").addClass("errorControl");
	                proceed = false;
	            }else{
	            	$("#textRefferedEmailLMS").removeClass("errorControl");
	            }
			}
				
			if(UI_commonSystem.lmsDetails.headOFEmailId=="N"){
				strErrorHTML += "<li> " +  errorInfo.headEmailDoesntExist;
				$("#textEmailHLMS").addClass("errorControl");
				proceed = false;
			}else{
				$("#textEmailHLMS").removeClass("errorControl");
			}
			
			if(UI_commonSystem.lmsDetails.refferedEmail=="N"){
				strErrorHTML += "<li> " +  errorInfo.refEmailDoesntExist;
				$("#textRefferedEmailLMS").addClass("errorControl");
				proceed = false;
			}else{
				$("#textRefferedEmailLMS").removeClass("errorControl");
			}
		}			 
		return strErrorHTML;
	}
	

	UI_ReservationList.lmsHeadRefferedValidate = function(){
		var data = {};
		var url = "lmsAjaxRegister!validateHeadReffered.action"
		data["lmsDetails.headOFEmailId"] = $("#textEmailHLMS").val();
		data["lmsDetails.refferedEmail"] = $("#textRefferedEmailLMS").val();
		data["lmsDetails.emailId"] = UI_CustomerHome.regEmail;
		var response = $.ajax({
			type: "POST", 
			dataType: 'json', 
			asynch: false,
			data:data,url:url,		
			success: UI_ReservationList.lmsCheckSuccess,
			error:UI_commonSystem.setErrorStatus
		});
	}

	UI_ReservationList.mashreqOnlyEnroll = function(){
		var strErrorHTML = UI_ReservationList.validateLmsFields(); // will this validate lms as well, and give erros for a already lms user
		if(strErrorHTML=="" || strErrorHTML==null){
			UI_ReservationList.lmsAjaxRegister();
		}else{
			$("#errorSpan").html("<font class='mandatory'> "+strErrorHTML+"</font>");
		}
	}
	
	UI_ReservationList.lmsCheckSuccess = function(response){
		UI_commonSystem.lmsDetails = response.lmsDetails;
		errorInfo  = response.errorInfo;
		var strErrorHTML = UI_ReservationList.validateLmsFields();
		if(strErrorHTML=="" || strErrorHTML==null){			
			if(response.lmsNameMismatch){
				$("#errorSpan").html("<font class='mandatory'> "+response.messageTxt+"</font>");
			} else if (UI_commonSystem.lmsDetails.emailId == 'Y'){
				var mergeMsg = "<div>The account with this Airewards ID already exists. If you want to merge please click Ok.</div>";
				var accountMege = "Merge Account";
				jConfirm(mergeMsg, accountMege, function(response) {
					if (response){					
						UI_ReservationList.lmsAjaxRegister();
					} else {
						
					}
				});
			} else{
				UI_ReservationList.lmsAjaxRegister();
			}
		}
		else{
			$("#errorSpan").html("<font class='mandatory'> "+strErrorHTML+"</font>");
			UI_commonSystem.lmsDetails.emailId = ""; 
		}
	}
	
	function checkEmail(s){
		var re = new RegExp("^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,7}$");
		var ve = re.exec(s);
		if (ve == null) {return false;}else{return true}
		// return RegExp( "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$"
		// ).test(s);
	}
	
// To do: Remove function
UI_ReservationList.ready();