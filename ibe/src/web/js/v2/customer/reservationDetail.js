function UI_CustReservation(){}

var arrPaxTitle = new Array();
var arrNationality = new Array();
var errorInfo  = "";
var arrCountryPhone = new Array();
var arrAreaPhone  = new Array();
var modifyFFID = false;

UI_CustReservation.pnr = "";
UI_CustReservation.version = "";
UI_CustReservation.actionBound = false;
UI_CustReservation.fareQuote = null;
UI_CustReservation.returnFlag =  false;
UI_CustReservation.flightRetDetails = [];
UI_CustReservation.flightDepDetails = [];
UI_CustReservation.contactDetails = null;
UI_CustReservation.bookingInfo = "";
UI_CustReservation.allReservationSegments = "";
UI_CustReservation.mode= "";
UI_CustReservation.flightRefNos = "";
UI_CustReservation.colAdultCollection = "";
UI_CustReservation.colChildCollection = "";
UI_CustReservation.colInfantCollection = "";
UI_CustReservation.fareDetails = [];
UI_CustReservation.firstDepature = null;
UI_CustReservation.lastArrival = null;
UI_CustReservation.resFlexiAlerts = [];
UI_CustReservation.isPageDataInit = false;
UI_CustReservation.labelCallCenter = "";
UI_CustReservation.hasInsurance = false;
UI_CustReservation.addBusSegmentEnabled = false;
UI_CustReservation.isCarLinkDynamic = false;
UI_CustReservation.carLink = "";
UI_CustReservation.isRequoteFlow = true;
UI_CustReservation.isRequoteCancel= false;
UI_CustReservation.isRequoteNameChange = false;
UI_CustReservation.alertInfo = "";
UI_CustReservation.availableCOSRankMap = null;
UI_CustReservation.isCreditPromotion = false;
UI_CustReservation.noOfCookieExpiryDays = UI_Top.holder().GLOBALS.noOfCookieExpiryDays;
UI_CustReservation.contactUpdateInProgress = false;
UI_CustReservation.enableSelfReprotection = false;
UI_CustReservation.ondWiseBookingClassSelected = {};

var validateEmailDomain = false;

//this will keep the mapping of validation group to
//client error code
var jsonGroupValidation = {"1":"ERR088"};

var jsonGroupElementMapping = {"1": ["#txtPhoneRes","#txtPAreaRes","#txtPCountryRes","#txtMobileRes","#txtMAreaRes","#txtMCountryRes"]};

//this will keep the mapping of fieldname+groupid of t_contact_details_config
//to html form element
var jsonFieldToElementMapping = {"phoneNo1":"#txtPhoneRes", "mobileNo1":"#txtMobileRes"};

var today=new Date();
var expiry=new Date(today.getTime()+ UI_CustReservation.noOfCookieExpiryDays*24*3600*1000);

/*
 * Load the customer and reservation details
 */
UI_CustReservation.ready = function(){	
	$("#btnUpdateRes").decorateButton();
	$("#btnEmailRes").decorateButton();
	$("#btnItineraryRes").decorateButton();
	if(SYS_IBECommonParam.locale == "ar" || SYS_IBECommonParam.locale == "fa"){
		$(".div-Table").removeClass("div-Table").addClass("div-Table-rtl")
	}
	UI_CustReservation.showHideContactInfo();
	UI_CustReservation.displayEmailMeButton({emailAddress : ""});
	if(!UI_CustReservation.actionBound ){
		$("#selCountryRes").change(function(){UI_CustReservation.countryListChange()});	
		$("#btnModify").click(function(){ UI_CustReservation.modifySegemnt();});
		$("#btnCancelRes").click(function(){UI_CustReservation.mode="cancelReservation";UI_UserTabs.cancelReservationClick();});		
		$("#btnCancelSegment").click(function(){UI_CustReservation.mode="cancelSegment";UI_UserTabs.cancelSegmentClick();});		
		$("#btnAddBusSegment").click(function(){UI_CustReservation.addBusSegment();});
		$("#btnUpdateRes").click(function(){
			if (!UI_CustReservation.contactUpdateInProgress) {
				UI_CustReservation.contactUpdateInProgress = true;
				UI_CustReservation.updateClick();
			}			
		});
		$("#btnEmailRes").click(function(){UI_CustReservation.emailClick();});
		$("#btnItineraryRes").click(function(){UI_CustReservation.itineraryClick();});	
		$("#btnSeatUpdate").click(function(){UI_CustReservation.addModifyAncillary({id:'SEAT'});});	
		$("#btnInsUpdate").click(function(){UI_CustReservation.addModifyAncillary({id:'INSURANCE'});});	
		$("#btnmealUpdate").click(function(){UI_CustReservation.addModifyAncillary({id:'MEAL'});});	
		$("#btnBaggageUpdate").click(function(){UI_CustReservation.addModifyAncillary({id:'BAGGAGE'});});
		$("#btnHalaUpdate").click(function(){UI_CustReservation.addModifyAncillary({id:'AIRPORT_SERVICE'});});	
		$("#btnAPTUpdate").click(function(){UI_CustReservation.addModifyAncillary({id:'AIRPORT_TRANSFER'});});	
		$("#btnAddSrv4").click(function(){UI_CustReservation.addModifyAncillary({id:'SSR'});});	
		$('#btnMakePayment').click(function(){UI_CustReservation.makePayment();});
		$('#btnTransfer').click(function(){UI_CustReservation.transferSegment();});
		
		$("#btnNameChange").click(function(){
			UI_CustReservation.showNameChangePopup();
		});
		
		$("#btnFFIDChange").click(function(){
			UI_CustReservation.showFFIDChangePopup();
		});
		UI_CustReservation.actionBound = true;
	}
	UI_commonSystem.loadingCompleted();
	UI_CustReservation.loadReservationData();
	//$("#aggrementInfo").hide(); hiding to fix AARESAA-23165
}

UI_CustReservation.showNameChangePopup = function(){
	
	//The HTML snippets are available in a formatted form in /v3/customer/reservationDetail.jsp
	$("#nameChangePopup").openMyPopUp({
		width:550,
		height:300,
		topPoint: ($(window).height()/2)-((300+70)/2),
		headerHTML:"<label class='fntBold' >Change Passenger Details</label>",
		bodyHTML: function(){
			return '<div id="nameChangeTemplateContainer" ><table id="nameChangeAdultContainer" width="100%"><tr><td class="gridHDDark alignLeft"  width="100%" colspan="3"><label id="lblAdultHD" class="gridHDFont fntBold">Adults</label></td></tr><tr><td class="gridHDDark alignLeft"><label id="lblAdultFNameHD" class="gridHDFont fntBold">First Name</label></td><td class="gridHDDark alignLeft"><label id="lblAdultLName" class="gridHDFont fntBold">Last Name</label></td><td class="gridHDDark alignLeft"><label id="lblAdultFFID" class="gridHDFont fntBold">AiRewards ID</label></td></tr><tr id="nameChangeTemplateAdult"><td><input id="itnPaxFirstName" name="itnPaxFirstName" type="text"  style="width:98%"/></td><td><input id="itnPaxLastName" name="itnPaxLastName" type="text"  style="width:98%"/><input id="actualSeqNo" type="hidden"/></td><td><input id="itnFFID" name="itnFFID" type="text"  style="width:98%"/></td></tr></table><table id="nameChangeChildContainer" width="100%"><tr><td class="gridHDDark alignLeft"  width="100%" colspan="3"><label id="lblChildHD" class="gridHDFont fntBold">Children</label></td></tr><tr><td class="gridHDDark alignLeft"><label id="lblAdultFNameHD" class="gridHDFont fntBold">First Name</label></td><td class="gridHDDark alignLeft"><label id="lblAdultLName" class="gridHDFont fntBold">Last Name</label></td><td class="gridHDDark alignLeft"><label id="lblChildFFID" class="gridHDFont fntBold">AiRewards ID</label></td></tr><tr id="nameChangeTemplateChild"><td><input id="itnPaxFirstName" name="itnPaxFirstName" type="text" style="width:98%"/></td><td><input id="itnPaxLastName" name="itnPaxLastName" type="text" style="width:98%"/><input id="actualSeqNo" type="hidden"/></td><td><input id="itnFFID" name="itnFFID" type="text"  style="width:98%"/></td></tr></table><table id="nameChangeInfantContainer" width="100%"><tr><td class="gridHDDark alignLeft"  width="100%" colspan="3"><label id="lblInfantHD" class="gridHDFont fntBold">Infants</label></td></tr><tr><td class="gridHDDark alignLeft"><label id="lblAdultFNameHD" class="gridHDFont fntBold">First Name</label></td><td class="gridHDDark alignLeft"><label id="lblAdultLName" class="gridHDFont fntBold">Last Name</label></td></tr><tr id="nameChangeTemplateInfant"><td><input id="itnPaxFirstName" name="itnPaxFirstName" type="text"  style="width:98%"/>	</td><td><input id="itnPaxLastName" name="itnPaxLastName" type="text" style="width:98%"/><input id="actualSeqNo" type="hidden"/></td></tr></table>';
		},
		footerHTML: function(){
			return '<input type="button" id="btnCNXNameChange" class="Button ButtonLarge" title="Click here to cancel name change"  tabindex="23" value="Cancel"/><input type="button" id="btnCNFNameChange" class="Button ButtonLarge" title="Click here to confirm name change"  tabindex="24" value="Confirm"/>';
		}
	});
	
	$("#btnCNXNameChange").click(function(){
		$("#nameChangePopup").closeMyPopUp();
	});
	
	$("#btnCNFNameChange").click(function(){
		UI_CustReservation.nameChangeConfirmed();
	});
	
	$("select[name=itnPaxTitle]").each(function(){
		$(this).empty().fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true})
	});
	
	if(UI_CustReservation.colAdultCollection.length > 0){		
		$("#nameChangeTemplateAdult").nextAll().remove();
		$("#nameChangeTemplateAdult").iterateTemplete({templeteName:"nameChangeTemplateAdult", data:UI_CustReservation.colAdultCollection, 
			dtoName:"colAdultCollection"});
		
		UI_CustReservation.prepareEditablePaxData(UI_CustReservation.colAdultCollection, "nameChangeTemplateAdult_");		
		
		$("#nameChangeAdultContainer").show();
	}else{
		$("#nameChangeAdultContainer").hide();
	}
	
	if(UI_CustReservation.colChildCollection.length > 0){		
		$("#popupNameDetailContainer").find("#nameChangeTemplateChild").nextAll().remove();
		$("#popupNameDetailContainer").find("#nameChangeTemplateChild").iterateTemplete({templeteName:"nameChangeTemplateChild", data:UI_CustReservation.colChildCollection,
			dtoName:"colChildCollection"});
		
		UI_CustReservation.prepareEditablePaxData(UI_CustReservation.colChildCollection, "nameChangeTemplateChild_");
		
		$("#nameChangeChildContainer").show();
	}else{
		$("#nameChangeChildContainer").hide();
	}
	
	if(UI_CustReservation.colInfantCollection.length > 0){		
		$("#popupNameDetailContainer").find("#nameChangeTemplateInfant").nextAll().remove();
		$("#popupNameDetailContainer").find("#nameChangeTemplateInfant").iterateTemplete({templeteName:"nameChangeTemplateInfant", data:UI_CustReservation.colInfantCollection, 
			dtoName:"colInfantCollection"});
		UI_CustReservation.prepareEditablePaxData(UI_CustReservation.colInfantCollection, "nameChangeTemplateInfant_");
		
		$("#nameChangeInfantContainer").show();
	}else{
		$("#nameChangeInfantContainer").hide();
	}
	
	if (modifyFFID) {
		for (var ai = 0; $("#colAdultCollection"+"\\["+ai+"\\]\\.itnFFID").val() != undefined; ai++) {
			$("#colAdultCollection"+"\\["+ai+"\\]\\.itnFFID").prop("disabled", false);
		}
		
		for (var ci = 0; $("#colChildCollection"+"\\["+ci+"\\]\\.itnFFID").val() != undefined; ci++) {
			$("#colChildCollection"+"\\["+ci+"\\]\\.itnFFID").prop("disabled", false);
		}
	} else {
		for (var ai = 0; $("#colAdultCollection"+"\\["+ai+"\\]\\.itnFFID").val() != undefined; ai++) {
			$("#colAdultCollection"+"\\["+ai+"\\]\\.itnFFID").hide();
		}
		$("#lblAdultFFID").parent().hide();
		for (var ci = 0; $("#colChildCollection"+"\\["+ci+"\\]\\.itnFFID").val() != undefined; ci++) {
			$("#colChildCollection"+"\\["+ci+"\\]\\.itnFFID").hide();
		}
		$("#lblChildFFID").parent().hide();
	}
}

UI_CustReservation.showFFIDChangePopup = function(){
	
	//The HTML snippets are available in a formatted form in /v3/customer/reservationDetail.jsp
	$("#ffidChangePopup").openMyPopUp({
		width:550,
		height:300,
		topPoint: ($(window).height()/2)-((300+70)/2),
		headerHTML:"<label class='fntBold'>Change Airewards IDs</label>",
		bodyHTML: function(){
			return '<table id="ffidChangeAdultContainer" width="100%"><tr><td class="gridHDDark alignLeft"  width="100%" colspan="3"><label id="lblAdultHD" class="gridHDFont fntBold">Adults</label></td></tr><tr><td class="gridHDDark alignLeft"><label id="lblAdultFFIDHD" class="gridHDFont fntBold">Airewards ID</label></td></tr><tr id="ffidChangeTemplateAdult"><td><input id="itnFFID" name="itnFFID" type="text"  style="width:98%"/><input id="actualSeqNo" type="hidden"/></td></tr></table><table id="ffidChangeChildContainer" width="100%"><tr><td class="gridHDDark alignLeft"  width="100%" colspan="3"><label id="lblChildHD" class="gridHDFont fntBold"> Children</label></td></tr><tr><td class="gridHDDark alignLeft"><label id="lblAdultFFIDHD" class="gridHDFont fntBold"> Airewards ID</label></td></tr><tr id="ffidChangeTemplateChild"><td><input id="itnFFID" name="itnFFID" type="text" style="width:98%"/><input id="actualSeqNo" type="hidden"/></td></tr></table>';
		},
		footerHTML: function(){
			return '<input type="button" id="btnCNXFFIDChange" class="Button ButtonLarge" title="Click here to cancel Airewards ID change"  tabindex="25" value="Cancel"/><input type="button" id="btnCNFFFIDChange" class="Button ButtonLarge" title="Click here to confirm Airewards ID change"  tabindex="26" value="Confirm"/>';
		}
	});
	
	$("#btnCNXFFIDChange").click(function(){
		$("#ffidChangePopup").closeMyPopUp();
	});
	
	$("#btnCNFFFIDChange").click(function(){
		UI_CustReservation.ffidChangeConfirmed();
	});
	
	if(UI_CustReservation.colAdultCollection.length > 0){		
		$("#ffidChangeTemplateAdult").nextAll().remove();
		$("#ffidChangeTemplateAdult").iterateTemplete({templeteName:"ffidChangeTemplateAdult", data:UI_CustReservation.colAdultCollection, 
			dtoName:"colAdultCollection"});
		
		UI_CustReservation.prepareEditablePaxData(UI_CustReservation.colAdultCollection, "ffidChangeTemplateAdult_");		
		
		$("#ffidChangeAdultContainer").show();
	}else{
		$("#ffidChangeAdultContainer").hide();
	}
	
	if(UI_CustReservation.colChildCollection.length > 0){		
		$("#popupFFIDContainer").find("#ffidChangeTemplateChild").nextAll().remove();
		$("#popupFFIDContainer").find("#ffidChangeTemplateChild").iterateTemplete({templeteName:"ffidChangeTemplateChild", data:UI_CustReservation.colChildCollection,
			dtoName:"colChildCollection"});
		
		UI_CustReservation.prepareEditablePaxData(UI_CustReservation.colChildCollection, "ffidChangeTemplateChild_");
		
		$("#ffidChangeChildContainer").show();
	}else{
		$("#ffidChangeChildContainer").hide();
	}
}


UI_CustReservation.prepareEditablePaxData = function(colPaxData, collectionName){
	var colPaxTOs = [];
	$.each(colPaxData,function(index,paxData){
		if(!paxData.editableFlag){
			$("#" + collectionName + index).find("input, select").disable();			
		}
	});
}

UI_CustReservation.nameChangeConfirmed = function(){
	var paxTOs = [];
	
	var hasPaxNamesChanged = false;
	
	var hasFFIDsChanged = false;
	
	var onlyTitlesChanged = false;
	
	var validationSuccessfull = true;
	
	$('input').removeClass('errorControl');
	var nameElements = [{name:"colAdultCollection",value:UI_CustReservation.colAdultCollection},
	                    {name:"colChildCollection",value:UI_CustReservation.colChildCollection},
	                    {name:"colInfantCollection",value:UI_CustReservation.colInfantCollection}];
	var totalPax =0;
	$.each(nameElements,function(index,nameElement){
		$.each(nameElement.value,function(index,value){
			
			var paxTO = getPaxNames(nameElement.name,index,value);
			var nameChanged = hasNamesChanged(paxTO,value);
			var ffidChanged = hasFFIDChanged(paxTO,value);
			
			if(validationSuccessfull){
				validationSuccessfull = validateInput(nameElement.name,index,value);
			}
			
			if(!hasPaxNamesChanged){
				hasPaxNamesChanged = nameChanged;
			}
			
			if(!hasFFIDsChanged){
				hasFFIDsChanged = ffidChanged;
			}
			
			if(nameChanged){
				paxTOs.push(paxTO);
			}
			totalPax++;
		});
	});
	
	var airewardsIDErrors = UI_CustReservation.validateFFID();
	
	if (airewardsIDErrors != "") {
		validationSuccessfull = false;
	} 
	
	var booleanRemainUnchangedNames = (totalPax == paxTOs.length) ? false : true;

	if(!validationSuccessfull){
		if (airewardsIDErrors != "") {
			jAlert(airewardsIDErrors);
		} 
		return false;
	}else if(hasPaxNamesChanged){
		if(booleanRemainUnchangedNames){
			
			jConfirm(raiseError("ERR126"), 'Confirm', function(action){ 
				if (!action) {			
					return false;
				}else{
					UI_UserTabs.nameChangeClick(paxTOs);
				}
			});			
		}else{
			UI_UserTabs.nameChangeClick(paxTOs);
		}		
	} else if (hasFFIDsChanged){
		UI_CustReservation.ffidNameUpdate();
	} else {
		alert("No names or AiRewards ids have been changed.")
	}
}

function getPaxNames(collectionName,index,value){
		
	var paxTO = jQuery.extend({}, value);
	if($("#"+collectionName+"\\["+index+"\\]\\.itnPaxFirstName").val() != undefined){
		paxTO.itnPaxFirstName = trim($("#"+collectionName+"\\["+index+"\\]\\.itnPaxFirstName").val());		
		paxTO.itnPaxLastName = trim($("#"+collectionName+"\\["+index+"\\]\\.itnPaxLastName").val());
		if(collectionName != "colInfantCollection") {
			paxTO.itnFFID = trim($("#"+collectionName+"\\["+index+"\\]\\.itnFFID").val());
		}
		paxTO.actualSeqNo = $("#"+collectionName+"\\["+index+"\\]\\.actualSeqNo").val();
	}
	
	
	UI_CustReservation.getFFIDPerPax();
	
	return paxTO;
}

UI_CustReservation.validateFFID = function(){
	
	var url = "lmsMemberConfirmation!validateFFID.action";
	var  data = new Object();
	
	data["paxDetailJson"] = $.toJSON(UI_CustReservation.getFFIDPerPax());
	
	var response = $.ajax({ type: "POST", dataType: 'json', async: false, data:data, url:url});
	
	response = $.parseJSON(response.responseText);
	if(typeof response.paxDetail == undefined){
		return;
	}
	
	var paxDetails = response.paxDetail;
	var errorString = "";
	
	for(var pl=0;pl < paxDetails.adultList.length;pl++){
		if(paxDetails.adultList[pl].ffid != "" && !paxDetails.adultList[pl].validFFID){
			$("#colAdultCollection"+"\\["+pl+"\\]\\.itnFFID").addClass("errorControl");
			errorString += ("<li> " + raiseError("ERR122"));
		}else{
			$("#colAdultCollection"+"\\["+pl+"\\]\\.itnFFID").val($("#colAdultCollection"+"\\["+pl+"\\]\\.itnFFID").val());
		}
	}
	
	for(var pl=0;pl < paxDetails.childList.length;pl++){
		if(paxDetails.childList[pl].ffid != "" && !paxDetails.childList[pl].validFFID){
			$("#colChildCollection"+"\\["+pl+"\\]\\.itnFFID").addClass("errorControl");
			errorString += ("<li> " + raiseError("ERR122"));
		}else{
			$("#colChildCollection"+"\\["+pl+"\\]\\.itnFFID").val($("#colChildCollection"+"\\["+pl+"\\]\\.itnFFID").val());
		}
	}
	
	if (errorString != "") {
		return errorString;
	}
	
	var details = UI_CustReservation.getFFIDPerPax();
	
	for(var pl=0;pl < paxDetails.adultList.length;pl++){
		var firstName1 = paxDetails.adultList[pl].firstName.toLowerCase();
		var	lastName1 = paxDetails.adultList[pl].lastName.toLowerCase();
		var	firstName2 = $("#colAdultCollection"+"\\["+pl+"\\]\\.itnPaxFirstName").val();		
		var	lastName2 = $("#colAdultCollection"+"\\["+pl+"\\]\\.itnPaxLastName").val();
		if(!compare(firstName1, lastName1 , firstName2, lastName2)){
			if(paxDetails.adultList[pl].ffid!=null && paxDetails.adultList[pl].ffid!=""){
				$("#colAdultCollection"+"\\["+pl+"\\]\\.itnPaxFirstName").addClass("errorControl");				
				$("#colAdultCollection"+"\\["+pl+"\\]\\.itnPaxLastName").addClass("errorControl");
				errorString += ("<li> " + raiseError("ERR122"));
			}
		}
		var temDateVal = paxDetails.adultList[pl].dateOfBirth;
		if((temDateVal != "") && (UI_commonSystem.getAge(temDateVal,UI_CustReservation.flightDepDetails[0].departureDisplayDate) < 12*365+3)){
				errorString += "<li> " + raiseError("ERR124", " # " + (pl + 1));
		}
	}
	
	for(var pl=0;pl < paxDetails.childList.length;pl++){
		var firstName1 = paxDetails.childList[pl].firstName.toLowerCase();
		var	lastName1 = paxDetails.childList[pl].lastName.toLowerCase();
		var	firstName2 = $("#colChildCollection"+"\\["+pl+"\\]\\.itnPaxFirstName").val();		
		var	lastName2 = $("#colChildCollection"+"\\["+pl+"\\]\\.itnPaxLastName").val();
		if(!compare(firstName1, lastName1 , firstName2, lastName2)){
			if(paxDetails.childList[pl].ffid!=null && paxDetails.childList[pl].ffid!=""){
				$("#colChildCollection"+"\\["+pl+"\\]\\.itnPaxFirstName").addClass("errorControl");				
				$("#colChildCollection"+"\\["+pl+"\\]\\.itnPaxLastName").addClass("errorControl");
				errorString += ("<li> " + raiseError("ERR122"));
			}
		}
		var temDateVal = paxDetails.childList[pl].dateOfBirth;
		if((temDateVal != "") && (UI_commonSystem.getAge(temDateVal,UI_CustReservation.flightDepDetails[0].departureDisplayDate) > 12*365+3)){
				errorString += "<li> " + raiseError("ERR125", " # " + (pl + 1));
		}
	}

	return errorString;
}

UI_CustReservation.getFFIDPerPax = function(){
	var paxDetails = {};
	paxDetails.adultList = new Array();
	paxDetails.childList = new Array();
		
	for(var pl=0 ; $("#colAdultCollection"+"\\["+pl+"\\]\\.itnFFID").val() != undefined ; pl++){
		var adtPax = {};
		adtPax.ffid = $("#colAdultCollection"+"\\["+pl+"\\]\\.itnFFID").val();
		adtPax.firstName = $("#colAdultCollection"+"\\["+pl+"\\]\\.itnPaxFirstName").val();
		adtPax.lastName = $("#colAdultCollection"+"\\["+pl+"\\]\\.itnPaxLastName").val();
		paxDetails.adultList[pl] = adtPax;
	}
	
	for(var pl=0 ; $("#colChildCollection"+"\\["+pl+"\\]\\.itnFFID").val() != undefined ; pl++){
		var chdPax = {};
		chdPax.ffid = $("#colChildCollection"+"\\["+pl+"\\]\\.itnFFID").val();
		chdPax.firstName = $("#colChildCollection"+"\\["+pl+"\\]\\.itnPaxFirstName").val();
		chdPax.lastName = $("#colChildCollection"+"\\["+pl+"\\]\\.itnPaxLastName").val();
		paxDetails.childList[pl] = chdPax;
	}	
	
	return paxDetails;
}

function getFFIDs(collectionName,index,value){
	
	var paxTO = jQuery.extend({}, value);
	if($("#"+collectionName+"\\["+index+"\\]\\.itnFFID").val() != undefined){
		paxTO.itnFFID = trim($("#"+collectionName+"\\["+index+"\\]\\.itnFFID").val());

		paxTO.actualSeqNo = $("#"+collectionName+"\\["+index+"\\]\\.actualSeqNo").val();
	}	
	
	return paxTO;
}

function validateInput(collectionName,index,value){
	if(!isAlphaWhiteSpace(trim($("#"+collectionName+"\\["+index+"\\]\\.itnPaxFirstName").val()))){
		$("#"+collectionName+"\\["+index+"\\]\\.itnPaxFirstName").addClass("errorControl");
		return false;
	}
	
	if(!isAlphaWhiteSpace(trim($("#"+collectionName+"\\["+index+"\\]\\.itnPaxLastName").val()))){
		$("#"+collectionName+"\\["+index+"\\]\\.itnPaxLastName").addClass("errorControl");
		return false;
	}
	
	return true;
}

function validateFFID(collectionName,index,value){
	if(!checkEmail(trim($("#"+collectionName+"\\["+index+"\\]\\.itnFFID").val())) && trim($("#"+collectionName+"\\["+index+"\\]\\.itnFFID").val())!=""){
		$("#"+collectionName+"\\["+index+"\\]\\.itnFFID").addClass("errorControl");
		return false;
	}
		
	return true;
}

function hasNamesChanged(original,newValue){
	return newValue.itnPaxFirstName.toUpperCase() != original.itnPaxFirstName.toUpperCase() 
		    || newValue.itnPaxLastName.toUpperCase() != original.itnPaxLastName.toUpperCase();
}

function hasFFIDChanged(original,newValue){
	if(original.itnTravellingWith == null){
		return newValue.itnFFID.toUpperCase() != original.itnFFID.toUpperCase();
	}
	return false;
}

UI_CustReservation.showHideContactInfo = function(){	
	//Reservation contact related configurations
	if(contactConfig.title1.ibeVisibility){
		if(!contactConfig.title1.mandatory){
			$("#lblTitleMand").hide();
		}
	} else {
		$("#trTitle").hide();
	}
	
	if(contactConfig.firstName1.ibeVisibility){
		if(!contactConfig.firstName1.mandatory){
			$("#lblFirstNameMand").hide();
		}
	} else {
		$("#tblFirstName").hide();
	}
	
	if(contactConfig.lastName1.ibeVisibility){
		if(!contactConfig.lastName1.mandatory){
			$("#lblLastNameMand").hide();
		}
	} else {
		$("#tblLastName").hide();
	}
	
	if(contactConfig.nationality1.ibeVisibility){
		if(!contactConfig.nationality1.mandatory){
			$("#lblNationalityMand").hide();
		}
	} else {
		$("#tblNationality").hide();
	}
	
	if(contactConfig.city1.ibeVisibility){
		if(!contactConfig.city1.mandatory){
			$("#lblCityMand").hide();
		}
	} else {
		$("#tblCity").hide();
	}
	
	if(contactConfig.address1.ibeVisibility){
		if(!contactConfig.address1.mandatory){
			$("#lblAddrMand").hide();
		}
	} else {
		$("#tblAddr1").hide();
		$("#tblAddr2").hide();
	}
	
	if(contactConfig.country1.ibeVisibility){
		if(!contactConfig.country1.mandatory){
			$("#lblCountryMand").hide();
		}
	} else {
		$("#tblCountry").hide();
	}
	
	if(contactConfig.preferredLang1.ibeVisibility){
		if(!contactConfig.preferredLang1.mandatory){
			$("#lblPreferredLangMand").hide();
		}
	} else {
		$("#tblPreferredLang").hide();
	}
	
	if(contactConfig.zipCode1.ibeVisibility){
		if(!contactConfig.zipCode1.mandatory){
			$("#lblZipCodeMand").hide();
		}
	} else {
		$("#tblZipCode").hide();
	}
	
	if((!contactConfig.mobileNo1.ibeVisibility) &&
			(!contactConfig.phoneNo1.ibeVisibility) && (!contactConfig.fax1.ibeVisibility)){
		$("#tblContactNo").hide();
	} else {
		if(contactConfig.mobileNo1.ibeVisibility){
			if(!contactConfig.mobileNo1.mandatory){
				$("#lblMobileNoMand").hide();
			}
		} else {
			$("#trMobileNo").hide();
		}
		
		if(contactConfig.phoneNo1.ibeVisibility){
			if(!contactConfig.phoneNo1.mandatory){
				$("#lblPhoneNoMand").hide();
			}
		} else {
			$("#trPhoneNo").hide();
		}
		
		if(contactConfig.fax1.ibeVisibility){
			if(!contactConfig.fax1.mandatory){
				$("#lblFaxMand").hide();
			}
		} else {
			$("#trFaxNo").hide();
		}
	}
	
	if(contactConfig.email1.ibeVisibility){
		if(!contactConfig.email1.mandatory){
			$("#lblEmailMand").hide();
		}
	} else {
		$("#tblEmail").hide();
	}
	
	// show/hide Contact Details Labels
	if (!contactConfig.contact_detail_label1.ibeVisibility){
		$("#trPhoneNoHeader").hide();
		$("#trPhoneNoLabel").hide();
	}
	// show/hide Contact Details Labels
	if (!contactConfig.areaCode1.ibeVisibility){
		$(".areaCode").hide();
	}
	
	//Emergency Contact related configurations
	if(contactConfig.firstName2.ibeVisibility ||
			contactConfig.lastName2.ibeVisibility ||
			contactConfig.phoneNo2.ibeVisibility){
		
		if(contactConfig.title2.ibeVisibility){
			if(!contactConfig.title2.mandatory){
				$("#lblEmgnTitleMand").hide();
			}
		} else {
			$("#trEmgnTitle").hide();
		}
		
		if(contactConfig.firstName2.ibeVisibility){
			if(!contactConfig.firstName2.mandatory){
				$("#lblEmgnFNameMand").hide();
			}
		} else {
			$("#tblEmgnFirstName").hide();
		}
		
		if(contactConfig.lastName2.ibeVisibility){
			if(!contactConfig.lastName2.mandatory){
				$("#lblEmgnLNameMand").hide();
			}
		} else {
			$("#tblEmgnLastName").hide();
		}
		
		if(contactConfig.phoneNo2.ibeVisibility){
			if(!contactConfig.phoneNo2.mandatory){
				$("#lblEmgnPhoneNoMand").hide();
			}
		} else {
			$("#trEmgnPhoneNo").hide();
		}
		
		if(contactConfig.email2.ibeVisibility){
			if(!contactConfig.email2.mandatory){
				$("#lblEmgnEmailMand").hide();
			}
		} else {
			$("#tblEmgnEmail").hide();
		}		
	} else {
		$("#trEmgnContactInfo").hide();
	}
	
}

UI_CustReservation.modifySegemnt = function() {
	UI_CustReservation.mode="modifySegment"; 
	var paxArr = UI_CustReservation.getPaxForAncillary();	
	$('#paxJson').val($.toJSON(paxArr));	
	
	// setting for Common PGW related Data
	$('#pgwPaymentMobileNumber').val($('#txtMCountryRes').val() + $('#txtMobileRes').val());
	$('#pgwPaymentEmail').val($('#txtEmailRes').val());
	$('#pgwPaymentCustomerName').val($( "#txtFNameRes" ).val() + " " + $( "#txtLNameRes" ).val());
	
	UI_UserTabs.customerModifySegmentClick();
}

UI_CustReservation.addBusSegment = function() {
	UI_CustReservation.mode="addBusSegment"; 
	var paxArr = UI_CustReservation.getPaxForAncillary();	
	$('#paxJson').val($.toJSON(paxArr));	
	UI_UserTabs.customerAddBusSegmentClick();
}

UI_CustReservation.addModifyAncillary  = function(selection){
	/* set pax counts for common search params - ancillary */
	$("#resAdultCount").val(UI_CustReservation.colAdultCollection.length);
	$("#resChildCount").val(UI_CustReservation.colChildCollection.length);
	$("#resInfantCount").val(UI_CustReservation.colInfantCollection.length);

	var fltDetails = UI_CustReservation.getFlightDetails();
	$("#resFromAirport").val(fltDetails.from);
	$("#resToAirport").val(fltDetails.to);	
	$("#resClassOfService").val(fltDetails.cabinClass);	
	
	$('#resFareQuote').val($.toJSON(UI_CustReservation.fareQuote));
	$('#resSelectedFlights').val($.toJSON({flightSegments:fltDetails.segments, isReturnFlight:UI_CustReservation.returnFlag}));
	
	
	var anciTOMod = UI_CustReservation.anciToModify;
	if(anciTOMod.length == 0){
		jAlert('No ancillary to modify');
		return;
	}
	
	$('#selectedAncillary').val($.toJSON(anciTOMod));
	$('#modifyAncillary').val(true);	
	$('#oldAllSegments').val($.toJSON(UI_CustReservation.allReservationSegments));	
	var paxArr = UI_CustReservation.getPaxForAncillary();	
	$('#paxJson').val($.toJSON(paxArr));
	$('#pnr').val(UI_CustReservation.bookingTO.PNR);
	$('#groupPNR').val(UI_CustReservation.groupPNR());
	$("#frmCustReservation #resRegCustomer").val(SYS_IBECommonParam.regCustomer);
	$('#insuranceBooked').val(UI_CustReservation.bookingTO.hasInsurance);
	$("#resFareQuoteBookingCCSelection").val($.toJSON(UI_CustReservation.ondWiseBookingClassSelected));
	
	// setting for Common PGW related Data for add modify ancillary
	$('#pgwPaymentMobileNumber').val($('#txtMCountryRes').val() + $('#txtMobileRes').val());
	$('#pgwPaymentEmail').val($('#txtEmailRes').val());
	$('#pgwPaymentCustomerName').val($( "#txtFNameRes" ).val() + " " + $( "#txtLNameRes" ).val());
	
	$('#frmCustReservation').attr('action','loadContainer.action');
	$('#frmCustReservation').attr('target','_self');
	$('#frmCustReservation').submit();
}


UI_CustReservation.makePayment= function(){
	/* set pax counts for common search params - ancillary */
	$("#resAdultCount").val(UI_CustReservation.colAdultCollection.length);
	$("#resChildCount").val(UI_CustReservation.colChildCollection.length);
	$("#resInfantCount").val(UI_CustReservation.colInfantCollection.length);

	var fltDetails = UI_CustReservation.getFlightDetails();
	$("#resFromAirport").val(fltDetails.from);
	$("#resToAirport").val(fltDetails.to);	
	$("#resClassOfService").val(fltDetails.cabinClass);	
	
	$('#resFareQuote').val($.toJSON(UI_CustReservation.fareQuote));
	$('#resSelectedFlights').val($.toJSON({flightSegments:fltDetails.segments, isReturnFlight:UI_CustReservation.returnFlag}));
	var anciTOMod = UI_CustReservation.anciToModify;
	$('#selectedAncillary').val($.toJSON(anciTOMod));
	$('#modifyAncillary').val(false);
	$('#makePayment').val(true);
	var paxArr = UI_CustReservation.getPaxForAncillary();	
	$('#paxJson').val($.toJSON(paxArr));
	$('#pnr').val(UI_CustReservation.bookingTO.PNR);
	$('#groupPNR').val(UI_CustReservation.groupPNR());
	$("#frmCustReservation #resRegCustomer").val(SYS_IBECommonParam.regCustomer);
	
//	 modified for the payFort PGWs
	$('#payFortPayATHomeOfflinePayment').val(true);
	$('#payFortOfflinePayment').val(true);
	var payFortMobileNumber = $('#txtMCountryRes').val() + $('#txtMobileRes').val();
	$('#payFortMobileNumber').val(payFortMobileNumber);
	$('#payFortEmail').val($('#txtEmailRes').val());
	
	// setting for Common PGW related Data
	$('#pgwPaymentMobileNumber').val($('#txtMCountryRes').val() + $('#txtMobileRes').val());
	$('#pgwPaymentEmail').val($('#txtEmailRes').val());
	$('#pgwPaymentCustomerName').val($( "#txtFNameRes" ).val() + " " + $( "#txtLNameRes" ).val());
	
	$('#frmCustReservation').attr('action','loadContainer.action');
	$('#frmCustReservation').attr('target','_self');
	$('#frmCustReservation').submit();
}


UI_CustReservation.getPaxForAncillary = function(){
	var isSequenceMapProcessedForAdults = false;
	var sequenceMap = new Array();
	var composerFn = function(paxArr){
		var retPaxArr = [];
		for(var i = 0 ; i < paxArr.length; i++){
			var pax = {};
			pax['title'] = paxArr[i].itnPaxTitle;
			pax['firstName'] = paxArr[i].itnPaxFirstName;
			pax['lastName'] = paxArr[i].itnPaxLastName;
			pax['seqNumber'] = paxArr[i].actualSeqNo;
			pax['dateOfBirth'] = paxArr[i].dateOfBirth;
			pax['foidNumber'] = '';
			pax['foidType'] = '';
			pax['nationality'] = '';
			pax['travelerRefNumber'] = paxArr[i].travelerRefNumber;
			var arr  = pax['travelerRefNumber'].split('$')[0].split('/');
			if(arr.length > 1){
				pax['travelWith'] = arr[1].split('')[1];
			} else {
				pax['travelWith'] = 0;
			}
			pax['currentAncillaries']=$.airutil.sort.quickSort(paxArr[i].selectedAncillaries, segmentComparator);
			
			if(!isSequenceMapProcessedForAdults){
				sequenceMap[paxArr[i].actualSeqNo] = i;	
			}					
			retPaxArr[retPaxArr.length]=pax;
		}
		return retPaxArr;
	}
	var adults = composerFn(UI_CustReservation.colAdultCollection);
	isSequenceMapProcessedForAdults = true;
	var children = composerFn(UI_CustReservation.colChildCollection);
	var infants = composerFn(UI_CustReservation.colInfantCollection);

	for(var i = 0 ; i < infants.length ; i++){
		var index = sequenceMap[infants[i].travelWith];		
		adults[index].travelWith =  infants[i].seqNumber;
		adults[index].parent = true;
				
	}
	var paxDetails =  {
			adults: adults,
			children: children,
			infants: infants
			};
	return paxDetails;	
}

/**
 * Send only confirmed segments to be used for ancillary,etc
 */
UI_CustReservation.getFlightDetails = function(){
	var arr = [];
	var inputArr = UI_CustReservation.flightDepDetails.concat(UI_CustReservation.flightRetDetails);
	var from,to,fromMin,toMin,cabinClass;
	from=to=fromMin=toMin=cabinClass=null;
	for(var i=0; i<inputArr.length; i++ ){
		if(inputArr[i].status == 'CNF'){
			if(!inputArr[i].returnFlag){								
				if(fromMin!=null && inputArr[i].departureDateLong < fromMin){				
					from = inputArr[i].segmentShortCode.split('/')[0];
				}else{
					fromMin = inputArr[i].departureDateLong;
					from = inputArr[i].segmentShortCode.split('/')[0];
				}
				if(toMin!=null && inputArr[i].departureDateLong > toMin){
					to = inputArr[i].segmentShortCode.split('/')[1];
				}else{
					toMin = inputArr[i].departureDateLong;
					to = inputArr[i].segmentShortCode.split('/')[1];
				}
			}
			inputArr[i].segmentCode = inputArr[i].orignNDest; 
			inputArr[i].departureTimeLong = inputArr[i].departureDateLong;
			inputArr[i].arrivalTimeLong = inputArr[i].arrivalDateLong;
			inputArr[i].flightNumber = inputArr[i].flightNo;
			inputArr[i].carrierCode = inputArr[i].airLine;
			if(cabinClass ==null && inputArr[i].cabinClass!=null){
				cabinClass = inputArr[i].cabinClass;
			}
			arr[arr.length]=inputArr[i];
		}
	}
	return {segments:arr,from:from,to:to,cabinClass:cabinClass};
}
/**
 * Page Clear
 */
UI_CustReservation.pageClear = function() {
	UI_CustReservation.removeClass();
	$("#tdSuccessMsg").hide();
	$("#spnErrorRes").html("");
	$("#reservationPage").slideUp();	
}
/**
 * Request reservation data
 */
UI_CustReservation.loadReservationData = function() {
	UI_CustReservation.pageClear();	
	var data = {};
	data['hdnPNR'] = UI_CustomerHome.pnr;
	data['groupPNR'] = UI_CustReservation.groupPNR();
	data['pageData'] = UI_CustReservation.isPageDataInit;
	data['airlineCode'] = UI_CustomerHome.airlineCode;
	data['marketingAirlineCode'] = UI_CustomerHome.marketingAirlineCode;
	
	$("#submitForm").ajaxSubmit({url:"showLoadReservation.action",data:data, dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success:  UI_CustReservation.loadReservationProcess, error:UI_commonSystem.setErrorStatus});	
	return false;	
}

/**
 * customer contact details Update
 */
UI_CustReservation.updateClick = function() {
	if (UI_CustReservation.clientValidate()) {
		/*if (trim($("#txtPhoneRes").val()) == ""){
			$("#txtPCountryRes").val("");
			$("#txtPAreaRes").val("");
		} 
		
		if(trim($("#txtMobileRes").val()) == ""){
			$("#txtMCountryRes").val("");
			$("#txtMAreaRes").val("");
		}*/		
		
		var data = {};
		data['hdnPNR'] = UI_CustomerHome.pnr;		
		data['blnGroupPNR'] = UI_CustReservation.groupPNR();	
		data['hdnMobile'] = $("#txtMCountryRes").val() + "-" + $("#txtMAreaRes").val() + "-" + trim($("#txtMobileRes").val());
		data['hdnPhone'] = $("#txtPCountryRes").val() + "-" + $("#txtPAreaRes").val() + "-" + trim($("#txtPhoneRes").val());	
		data['hdnFax'] = $("#txtFCountryRes").val() + "-" + $("#txtFAreaRes").val() + "-" + trim($("#txtFaxRes").val());	
		data['hdnEmgnPhone'] = $("#txtEmgnPCountryRes").val() + "-" + $("#txtEmgnPAreaRes").val() + "-" + trim($("#txtEmgnPhoneRes").val());	
		$("#frmCustReservation").attr('action', 'updateReservationContactDetail.action');
		$("#frmCustReservation").ajaxSubmit({dataType: 'json', data :data, beforeSubmit:UI_commonSystem.loadingProgress, success: UI_CustReservation.updateContactDetailSuccess, error:UI_commonSystem.setErrorStatus});		
	} else {
		UI_CustReservation.contactUpdateInProgress =  false;
	}
}

/**
 * customer contact details Update
 */
UI_CustReservation.ffidNameUpdate = function() {
		var data = {};
		data['hdnPNR'] = UI_CustomerHome.pnr;		
		data['blnGroupPNR'] = UI_CustReservation.groupPNR();
		var newFFIDs = {};
		var ai = 0;
		var ci = 0;
		var paxIndex = 0;
		
		for (ai = 0; $("#colAdultCollection"+"\\["+ai+"\\]\\.itnFFID").val()!=undefined; ai++) {
			data["newNameList\["+paxIndex+"\].paxFFID"] = $("#colAdultCollection"+"\\["+ai+"\\]\\.itnFFID").val();
			data["newNameList\["+paxIndex+"\].paxSequence"] = $("#colAdultCollection"+"\\["+ai+"\\]\\.actualSeqNo").val();
			paxIndex++;
		}
		
		for (ci = 0; $("#colChildCollection"+"\\["+ci+"\\]\\.itnFFID").val()!=undefined; ci++) {
			data["newNameList\["+paxIndex+"\].paxFFID"] = $("#colChildCollection"+"\\["+ci+"\\]\\.itnFFID").val();
			data["newNameList\["+paxIndex+"\].paxSequence"] = $("#colChildCollection"+"\\["+ci+"\\]\\.actualSeqNo").val();
			paxIndex++;
		}
		$("#frmCustReservation").attr('action', 'updatePaxFFID.action');
		$("#frmCustReservation").ajaxSubmit({dataType: 'json', data :data, beforeSubmit:UI_commonSystem.loadingProgress, success: UI_CustReservation.ffidNameUpdateSuccess, error:UI_commonSystem.setErrorStatus});
	
}

UI_CustReservation.ffidNameUpdateSuccess = function (responce) {
	var resp = responce;
	$("#nameChangePopup").closeMyPopUp();
	UI_commonSystem.loadingCompleted();
	UI_CustReservation.ready();
}


/**
 * Update Contact Detail Success
 */
UI_CustReservation.updateContactDetailSuccess = function(response) {
	UI_CustReservation.contactUpdateInProgress = false;
	if(response != null && response.success == true) {
		$("#linkFocus").focus();
		UI_CustReservation.contactDetails  = response.contactInfo;
		UI_CustReservation.fillContactInfo(response.contactInfo);
		$("#tdSuccessMsg").show();		
		$("#versionRes").val(response.bookingTO.version);
		UI_CustReservation.bookingInfo.version = response.bookingTO.version;
		//UI_CustReservation.fillContactInfo(response.contactInfo);
		
		if(UI_Top.holder().GLOBALS.saveCookieEnable){
			var UserName = "saveOprtnDataCookie_";
			var today=new Date();
			var expiry=new Date(today.getTime()+ UI_Top.holder().GLOBALS.noOfCookieExpiryDays*24*3600*1000);
			document.cookie = UserName + "=" + JSON.stringify(response.reservationCookieDetails) + "; path=/; expires=" + expiry.toGMTString();
		}
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}	
	UI_CustReservation.updatePageSetUp();
	UI_commonSystem.loadingCompleted();
	
}
/**
 * Update Contact Details Page Set up
 */
UI_CustReservation.updatePageSetUp = function() {
	$("#txtVerifyEmailRes").val("");
}

/*
 * email the itinerary to customer
 */
UI_CustReservation.emailClick = function() {
	if($("#txtEmailRes").val() != "") {		
		$("#btnEmailRes").attr("disabled","disabled").addClass("ButtonLargeDisable");	
		var  data = new Object();
		data["hdnPNRNo"] = UI_CustReservation.pnr;
		data['groupPNR'] = UI_CustReservation.groupPNR();
		data['operationType'] = 'MODIFY_BOOKING';
		var printLanguage = $('#selPrefLang').val();
		if(printLanguage==null || $.trim(printLanguage) == ''){
			printLanguage = SYS_IBECommonParam.locale;
		}
		data["itineraryLanguage"] = printLanguage;	
		$("#submitForm").ajaxSubmit({url:"sendEmail.action",data:data, dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success: UI_CustReservation.emailSucess, error:UI_commonSystem.setErrorStatus});		
	}	
}

/*
 * call back method for email
 */
UI_CustReservation.emailSucess = function(response) {
	if(response != null && response.success == true) {
		jAlert("Email sent to your mail address.");
	} else  {
		jAlert("Email sending fail.");
	}
	UI_commonSystem.loadingCompleted();
}

/*
 * view itinerary clicked
 */
UI_CustReservation.itineraryClick = function(){
	
	var isGroupPNR = UI_CustReservation.groupPNR();	
	var intHeigh = 630;
	var intWidth = 900
	var intTop 	= (window.screen.height - intHeigh) / 2;
	var intLeft = (window.screen.width -  intWidth) / 2;	
	var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=' + intWidth + ',height=' + intHeigh + ',resizable=no,top='+ intTop +',left=' + intLeft
	var printLanguage = $('#selPrefLang').val();
	if(printLanguage==null || $.trim(printLanguage) == ''){
		printLanguage = SYS_IBECommonParam.locale;
	}	
	//objCW =  window.open("printItinerary.action?hdnPNRNo="+UI_CustReservation.pnr+ "&groupPNR="+ isGroupPNR+ "&itineraryLanguage="+printLanguage, "myWindow", strProp);
	var formHtml =  "<form id='dynamicFormPrint' method='post'>" +
	                	"<input type='hidden' name='hdnPNRNo' value='"+ UI_CustReservation.pnr +"'/>" +
	                	"<input type='hidden' name='groupPNR' value='"+ isGroupPNR +"'/>" +
	                	"<input type='hidden' name='itineraryLanguage' value='"+ printLanguage +"'/>" +
	                	"<input type='hidden' name='viewMode' value='loadRes'/>" +
	                	"<input type='hidden' name='operationType' value='MODIFY_BOOKING'/>" +	                	
	                "</form>";
	if($("#dynamicFormPrint")){
		$("#dynamicFormPrint").remove();
	}
	$("body").append(formHtml);	
	$("#dynamicFormPrint").attr("action", SYS_IBECommonParam.securePath + "printItinerary.action");	
	$('#dynamicFormPrint').submit(function() {		
        window.open('', 'myWindow', strProp);
        this.target = 'myWindow';
	});
	$('#dynamicFormPrint').submit();	
}

/*
 * Call back method for loading the reservation details
 */
UI_CustReservation.loadReservationProcess = function(response) {	
	if(response != null && response.success == true) {		
		var depSegmentCount = 0;
		var retSegmentCount = 0;
		var resStatus = response.bookingTO.status;
		// GUI Build Using Process Params
		UI_CustReservation.bookingTO  = response.bookingTO;
		// Update Home Page Credit
		UI_CustomerHome.buildCredit({credit:response.totalCustomerCredit});
		UI_CustReservation.pageControlBuild(response.resProcessParams, resStatus);		
		UI_CustReservation.bookingInfo = response.bookingTO;
		UI_CustReservation.allReservationSegments = response.resSegments;
		UI_CustReservation.fareDetails = response.fareDetails;
		UI_CustReservation.version = response.version;
		
		UI_CustReservation.setSelectedBookingClass(response);
		
		if(UI_Top.holder().GLOBALS.saveCookieEnable){
			var UserName = "saveOprtnDataCookie_";
			var today=new Date();
			var expiry=new Date(today.getTime()+ UI_Top.holder().GLOBALS.noOfCookieExpiryDays*24*3600*1000);
			document.cookie = UserName + "=" + JSON.stringify(response.reservationCookieDetails) + "; path=/; expires=" + expiry.toGMTString();
		}
		
		UI_CustReservation.firstDepature = response.firstDepature;
		UI_CustReservation.lastArrival = response.lastArrival;
		UI_CustReservation.isCarLinkDynamic = response.carLinkDynamic;
		UI_CustReservation.carLink = response.carLink;
		UI_CustReservation.isCreditPromotion = response.creditPromo;
		UI_CustReservation.alertInfo = response.alertInfo;
		validateEmailDomain = response.validateEmailDomain;
		modifyFFID = response.modifyFFID;
		UI_CustReservation.alertIdPnrSegIdMap = response.alertIdPnrSegIdMap;
		UI_CustReservation.enableSelfReprotection = response.enableSelfReprotection;
		
		if(response.availableCOSRankMap != null){
			UI_CustReservation.availableCOSRankMap = response.availableCOSRankMap;
		}
		
		if(!UI_CustReservation.isPageDataInit) {
			$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
			UI_CustReservation.labelCallCenter = response.jsonLabel.lblRedeem;			
			errorInfo = response.errorInfo;
			UI_FlightSearch.errorInfo = response.homePageErrorInfo;
			eval(response.nationalityInfo);	
			eval(response.titleVisibilityInfo);
			//eval(response.countryPhoneInfo);
			UI_CustReservation.buildTelephoneInfo(response.countryPhoneInfo, response.areaPhoneInfo);
			//$("#selLanguageRes").empty().append(response.languageInfo);
			$("#selPrefLang").empty().append(response.languageInfo);
			$("#selCountryRes").fillFirstOption(response.jsonLabel.lblPleaseSelect);			
			$("#selCountryRes").fillDropDown({dataArray:response.countryInfo, keyIndex:0, valueIndex:1, firstEmpty:false});			
			$("#selNationalityRes").fillFirstOption(response.jsonLabel.lblPleaseSelect);
			$("#selNationalityRes").fillDropDown({dataArray:response.nationalityInfo, keyIndex:0, valueIndex:1, firstEmpty:false});			
			$("#selCTitleRes").empty().fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
			$("#selEmgnTitleRes").empty().fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
			UI_CustReservation.isPageDataInit = true;
		} 
		$("#selLanguageRes").val(response.bookingTO.prefferedLanguage);
		if (resStatus == "CANCELLED") {
			$("#divRedeemReservation").html(UI_CustReservation.labelCallCenter);
			UI_CustReservation.callCenterLinkSetup();
		}
		
		$("#lblPnrNo").text(response.bookingTO.PNR);
		UI_CustReservation.buildInsuranceData(response.bookingTO);		
		$("#lblStrStatus").text(response.bookingTO.status);
		$("#lblStrZuluReleaseDate").text(response.bookingTO.releaseDate);
		
		$("#divRefundMessage").toggle(response.successfulRefund);

		$("#lblStrBkgDate").text(response.bookingTO.bookingDate);
		$("#lblStrBkgDateHid").text(response.bookingTO.bookingDate.replace(/-/g,"/"));
		UI_CustReservation.buildPaymentDetails({status:resStatus, paymentData: response.balanceSummary, reservationCredit: response.creditBalance});		
		
		UI_CustReservation.pnr = response.bookingTO.PNR;
		$("#versionRes").val(response.bookingTO.version);
		// Flight Details
		$("#departueFlight").nextAll().remove();
		UI_CustReservation.flightDepDetails = response.flightDepDetails;
		depSegmentCount = response.flightDepDetails.length;
		$("#departueFlight").iterateTempleteFS({templeteName:"departueFlight", data:response.flightDepDetails, dtoName:"flightDepDetails"});
		
		$("#jsonOnds").val($.toJSON(response.bookingTO.pnrOndGroups));
		
		// UI_CustReservation.registerEvent({templateName:"flightDepDetails",
		// eventId:"flightSegmentRefNumber",
		// segmentSize:response.flightDepDetails.length });
		if(response.flightRetDetails != null && response.flightRetDetails.length > 0){
			retSegmentCount = response.flightRetDetails.length;
			UI_CustReservation.flightRetDetails = response.flightRetDetails;
			UI_CustReservation.returnFlag = true;
			$("#trRetrunGrid").show();
			$("#returnFlight").nextAll().remove();
			$("#returnFlight").iterateTempleteFS({templeteName:"returnFlight", data:response.flightRetDetails, dtoName:"flightRetDetails"});
			// UI_CustReservation.registerEvent({templateName:"flightRetDetails",
			// eventId:"flightSegmentRefNumber",
			// segmentSize:response.flightDepDetails.length });
		}else {
			$("#trRetrunGrid").hide();	
			$("#lblReturning").hide();
		}
		
		//set total segment count in global variable to refer in modify segment
		UI_commonSystem.totalSegCount = depSegmentCount + retSegmentCount;		
		
		UI_CustReservation.fareQuote = response.fareQuote;
		
		UI_CustReservation.colAdultCollection = response.colAdultCollection;
		UI_CustReservation.colChildCollection = response.colChildCollection;
		UI_CustReservation.colInfantCollection = response.colInfantCollection;
		
		var colAdultCollection = summarizeAnciData(response.colAdultCollection);
		var colChildCollection = summarizeAnciData(response.colChildCollection);
		var colInfantCollection = summarizeAnciData(response.colInfantCollection);
		
		UI_CustReservation.contactDetails  = response.contactInfo;
		
		// Set flexi details
		if(response.flexiInfo != null && response.flexiInfo.length > 0){
			UI_CustReservation.resFlexiAlerts = response.reservationFlexiAlerts;
			$("#flexiDetails").show();
			$("#flexiInfo").nextAll().remove();
			$("#flexiInfo").iterateTemplete({templeteName:"flexiInfo", data:response.flexiInfo, dtoName:"flexiInfo"});
		}
		
		//Set Promotion details
		if(response.promotionInfo != null && response.promotionInfo.length > 0){
			$("#promotionDetails").show();
			$("#trPromoInfo").nextAll().remove();
			$("#trPromoInfo").iterateTemplete({templeteName:"trPromoInfo", data:response.promotionInfo, dtoName:"promotionInfo"});
		}
		
		if ($.trim(UI_CustReservation.carLink) != "") {
			if (UI_CustReservation.isCarLinkDynamic == true) {
				$("#frmrentcar").attr("src", UI_CustReservation.carLink);
			} else {
				$("#divRentCar").remove();
			}
		} else {
			$("#divRentCar").remove();
		}
		
		// show the pax details
		
		if(colAdultCollection.length > 0){
			$("#adultGrid").nextAll().remove();
			$("#adultGrid").iterateTemplete({templeteName:"adultGrid", data:colAdultCollection, dtoName:"colAdultCollection"});
		}else {
			$("#tradultfGrid").hide();
		}
		
		if(colChildCollection.length > 0){
			$("#childGrid").nextAll().remove();
			$("#childGrid").iterateTemplete({templeteName:"childGrid", data:colChildCollection, dtoName:"colChildCollection"});
		}else {
			$("#trChildGrid").hide();
		}
		
		if(colInfantCollection.length > 0){
			$("#infantGrid").nextAll().remove();
			$("#infantGrid").iterateTemplete({templeteName:"infantGrid", data:colInfantCollection, dtoName:"colInfantCollection"});
		}else {
			$("#trInfGrid").hide();
		}	
		UI_CustReservation.setDropDowndata();
		// UI_CustReservation.fillContactInfo(response.contactInfo);
		// Set Contact Info for Modify Segment/ Modify Ancilary
		$('#contactInfoJson').val($.toJSON(UI_CustReservation.contactDetails));
		if(fromPromoPage != null && fromPromoPage != ""){
			 UI_CustReservation.makePayment();
		}
		UI_CustReservation.isRequoteFlow = response.requoteEnabled;
		UI_CustReservation.addBusSegmentEnabled = response.resProcessParams.addGroundSegment;
		$("#resSelectedCurrency").val(UI_CustReservation.fareQuote.inSelectedCurr.currency);
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	
	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
	$("#reservationPage").slideDown();
	$("#lblInfoAgree3").html(response.jsonLabel.lblInfoAgree3).append(response.jsonLabel.lblInfoAgree4);
	UI_commonSystem.loadingCompleted();
	
	if($.isEmptyObject(UI_CustReservation.alertIdPnrSegIdMap)){
		$("#btnTransfer").hide();
	}
	
	$.each( UI_CustReservation.alertInfo , function( key, value ) {
	    	$.each(value.alertTO , function( key, value ) {
	            if(value.ibeActioned == 'Y'){
	            	$("#btnTransfer").hide();
	            }
	    	});
	});
	
	if(UI_CustReservation.enableSelfReprotection == false){
		$("#btnTransfer").hide();		
	}
}

UI_CustReservation.setSelectedBookingClass = function(response){
	
	if(response.segments != null){
		response.segments.forEach(function(segment, index) {
		    
			UI_CustReservation.ondWiseBookingClassSelected[index] = {};
			UI_CustReservation.ondWiseBookingClassSelected[index][segment.segmentCode] = segment.bookingClassCode;

		});
		
	}
	
}

/**
 * Flight Segments Display template
 */
// TODO:Refactor

$.fn.iterateTempleteFS = function(params) {

	var templeteName = params.templeteName;
	var clonedTemplateId;
	var tempClonedTemplateId = "";
	var elementIdAppend;
    var selected = false;
    var flightRefNumbers = "";
    var flightRefNumberTemp = "";
    var tempID = "";
	var dtoRecord;
	var rowspan = 1;
	var tempElementID = "";
	var isModifable = true;
	var isCancellable = true;
	var isAddGroundSegmentAllowed = UI_CustReservation.addBusSegmentEnabled;
	var isAddGroundSegmentButtonShow = false;

	var reservationStatus = UI_CustReservation.bookingInfo.status;
	for(var i=0; i < params.data.length ; i++) {		
		clonedTemplateId = templeteName + "_" + i;		
		var clone = $( "#" + templeteName + "").clone();		
		clone.attr("id", clonedTemplateId );		
		clone.appendTo($("#" + templeteName).parent());		
		dtoRecord = params.data[i];		
		elementIdAppend = params.dtoName + "[" + i + "].";	
		
		
		
		
		// setting the value and id of labels
		$("#"+clonedTemplateId).find("label").each(function( intIndex ){
			$(this).text(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});			
		
		// setting the value and id of labels
		$("#"+clonedTemplateId).find("img").each(function( intIndex ){			
			$(this).attr("src", dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		
		if (tempID != dtoRecord.interLineGroupKey) {
			rowspan = 1;
			// Reset values
			isModifable = true;
			isCancellable = true;
		}
		
		// setting the value, id and name of inputs
		$("#"+clonedTemplateId).find("input").each(function( intIndex ){
			$(this).val(dtoRecord[this.id]);
			if (this.id == "flightSegmentRefNumber") {
				flightRefNumberTemp = dtoRecord[this.id];				
				if (rowspan == 1) {
					tempElementID =  elementIdAppend +  this.id;					
				} 
			}
			$(this).attr("id",  elementIdAppend +  this.id);
			$(this).attr("name",  this.id);
			
		});		
		
		if (dtoRecord.status == 'CNX') {
			$("#"+clonedTemplateId).find(" td:last input").remove();
			$( "#" + clonedTemplateId).find(" label").addClass("fntCnxSeg");
		} else if (reservationStatus == "ONHOLD") {
			$("#"+clonedTemplateId).find(" td:last input").remove();
			$("#"+clonedTemplateId).find(" td:last img").remove();
	    } else {
	    	if (!(isModifable && dtoRecord.isModifiable)) {
	    		isModifable = false;
	    	}
	    	if (!(isCancellable && dtoRecord.isCancellable)) {
	    		isCancellable = false;
	    	}
	    	
	    	if (!(isAddGroundSegmentAllowed && dtoRecord.groundStationAddAllowed)) {
	    		isAddGroundSegmentAllowed = false;
	    	}

		if(!isAddGroundSegmentButtonShow && dtoRecord.groundStationAddAllowed){
		   isAddGroundSegmentButtonShow =  true;
		}

	    	if (!isModifable && !isCancellable && !isAddGroundSegmentAllowed) {
	    		if (tempClonedTemplateId != "") {
	    			$("#"+tempClonedTemplateId).find(" td:last input").disable();
	    		} else {
	    			$("#"+clonedTemplateId).find(" td:last input").disable();
	    		}
			} else {				
				if (tempClonedTemplateId != "") {
	    			$("#"+tempClonedTemplateId).find(" td:last input").enable();
	    			$("#"+tempClonedTemplateId).find(" td:last input").click(function(event){ UI_CustReservation.segmentCheckClick(this); });
	    		} else {
	    			$("#"+clonedTemplateId).find(" td:last input").enable();
	    			$("#"+clonedTemplateId).find(" td:last input").click(function(event){ UI_CustReservation.segmentCheckClick(this); });
	    		}
			}
			$("#"+clonedTemplateId).find(" td:last img").remove();
		}
		
		if (tempID != dtoRecord.interLineGroupKey) {			
			flightRefNumbers = flightRefNumberTemp;
			if (dtoRecord.status == 'CNX') {				
				isModifable = true;	
				isCancellable = true;
			} else {
				if (isModifable || isCancellable) {
					$("#"+clonedTemplateId).find(" td:last input").enable();
					$("#"+clonedTemplateId).find(" td:last input").click(function(event){ UI_CustReservation.segmentCheckClick(this); });
				}
				isModifable = dtoRecord.isModifiable;
				isCancellable = dtoRecord.isCancellable;
			}
			tempClonedTemplateId = clonedTemplateId;			
			rowspan++;								
		} else {			
			// : Use for Separate Flight Segments
			flightRefNumbers =  flightRefNumbers + ":" +flightRefNumberTemp;
			if (dtoRecord.status != 'CNX' &&  reservationStatus != "ONHOLD" ) {
				document.getElementById(tempElementID).value = flightRefNumbers;	
			}					
			$("#"+ tempClonedTemplateId +" td:last").attr("rowspan", rowspan++);
			$("#"+clonedTemplateId).find(" td:last").remove();
		}
		tempID = dtoRecord.interLineGroupKey;			
		
		
		if (i%2 == 0) {
			$( "#" + clonedTemplateId).find(" td").addClass("SeperateorBGColor");
		}
		
		$("#" + clonedTemplateId).show();		
	}
	
 	if(!isAddGroundSegmentButtonShow){
	     $("#btnAddBusSegment").hide();
	} 
	
	$( "#" + templeteName).hide();
}

/**
 * Page Control Setup
 */
UI_CustReservation.pageControlBuild =  function(params, status) {
	// Button Control
	if (status == "CANCELLED") {
		$("#contactInfomation").readonly(true);
		$("#anciButtonPannel").hide();
		$("#departureButtonsPannel").hide();
		$("#tdHdDepatureResCheck").hide();		
		$("#tdDepatureResCheck").hide();
		$("#tdHdReturnResCheck").hide();
		$("#tdReturnResCheck").hide();
		$("#pageBottomBtnPannel").hide();		
		$("#anciButtonHdPannel").hide();
		$("#tdDepModifyImage").hide();
		$("#sidePannelAll").show();		
		$("#divRentCar").show();
		$("#lblMsgAncilary").show();
		$("#lblMsgOnhold").hide();
		$("#onHoldInfoPannel").hide();
		$("#forcedCFMInfoPannel").hide();
		$("#lblSummary").show();
	} else if (status == "ONHOLD")  {
		$("#anciButtonPannel").show();	
		$("#sidePannelAll").hide();		
		$("#departureButtonsPannel").hide();
		$("#pageBottomBtnPannel").hide();		
		$("#divRentCar").hide();
		$("#lblMsgAncilary").hide();
		$("#lblMsgOnhold").show();
		$("#onHoldInfoPannel").show();
		$("#forcedCFMInfoPannel").hide();
		$("#lblSummary").hide();
		$("#trDataPanel").readonly(true);
	}else if (status == "FORCED-CONFIRM" && !params.allowForceConfirmModify)  {
		$("#anciButtonPannel").show();	
		$("#sidePannelAll").hide();		
		$("#departureButtonsPannel").hide();
		$("#pageBottomBtnPannel").hide();		
		$("#divRentCar").hide();
		$("#lblMsgAncilary").hide();
		$("#lblMsgOnhold").show();
		$("#forcedCFMInfoPannel").show();
		$("#onHoldInfoPannel").hide();
		$("#lblSummary").hide();
		$("#trDataPanel").readonly(true);
	} else {
		$("#contactInfomation").readonly(false);
		$("#anciButtonPannel").show();		
		$("#tdHdDepatureResCheck").show();
		$("#tdDepatureResCheck").show();
		$("#tdHdReturnResCheck").show();
		$("#tdReturnResCheck").show();
		$("#pageBottomBtnPannel").show();		
		$("#anciButtonHdPannel").show();
		$("#tdDepModifyImage").show();
		$("#sidePannelAll").show();
		$("#divRentCar").show();		
		$("#departureButtonsPannel").show();
		$("#pageBottomBtnPannel").show();		
		$("#lblMsgAncilary").show();
		$("#lblMsgOnhold").hide();
		$("#forcedCFMInfoPannel").hide();
		$("#onHoldInfoPannel").hide();
		$("#lblSummary").show();
		$("#trDataPanel").readonly(false);
	}		
		
	UI_CustReservation.buttonControl(params, status);
	UI_CustReservation.disableModifyBtns();
	UI_CustReservation.hidePageMassges(UI_CustomerHome.groupPnr);
}
/**
 * Button Control
 */

UI_CustReservation.buttonControl = function(params, status) {
	// Departure /Return Flights - Check button Display
	if (true) {
		$("#trDepartureButtons").show();		
		if (params.cancelReservation == true) {
			$("#btnCancelRes").show();
			$("#btnCancelRes").enableSegBtn();
		}else {
			$("#btnCancelRes").disableSegBtn();
			$("#btnCancelRes").hide();
		}
		
		//segment modification IBE		
		if (params.allowModifyDate == true || params.allowModifyRoute == true) {
			$("#btnModify").enableSegBtn();
		}else {
			$("#btnModify").disableSegBtn();
		}
		
		if (params.cancelSegment == true) {
			$("#btnCancelSegment").enableSegBtn();
			$("#btnCancelSegment").show();
		}else {
			$("#btnCancelSegment").disableSegBtn();
			$("#btnCancelSegment").hide();
		}
		var anciToModify = [];
		var showaddAnciHeading = false;
		if(params.showSeat){
			$('#btnSeatUpdate').show();
			showaddAnciHeading = true;
		}else{
			$('#btnSeatUpdate').hide();
		}
		if(params.showMeal){
			$('#btnmealUpdate').show();
			showaddAnciHeading = true;
		}else{
			$('#btnmealUpdate').hide();
		}
		if(params.showBaggage){
			$('#btnBaggageUpdate').show();
			showaddAnciHeading = true;
		}else{
			$('#btnBaggageUpdate').hide();
		}
		if(params.showInflightService){
			$('#btnAddSrv4').show();
			showaddAnciHeading = true;
		}else{
			$('#btnAddSrv4').hide();
		}
		if(params.showAirportService){
			$('#btnHalaUpdate').show();
			showaddAnciHeading = true;
		}else{
			$('#btnHalaUpdate').hide();
		}
		if(params.showAirportTransfer){
			$('#btnAPTUpdate').show();
			showaddAnciHeading = true;
		}else{
			$('#btnAPTUpdate').hide();
		}
		
		if(params.showInsurance){			
			$('#btnInsUpdate').show();
			showaddAnciHeading = true;
		}else{
			$('#btnInsUpdate').hide();
		}
		
		if(params.seatMapEnable){
			anciToModify[anciToModify.length]="SEAT";
			$('#btnSeatUpdate').enableAnciButton();
		}else{
			$('#btnSeatUpdate').disableAnciButton();
		}
		if(params.mealEnable){
			anciToModify[anciToModify.length]="MEAL";
			$('#btnmealUpdate').enableAnciButton();
		}else{
			$('#btnmealUpdate').disableAnciButton();
		}
		if(params.baggageEnable){
			anciToModify[anciToModify.length]="BAGGAGE";
			$('#btnBaggageUpdate').enableAnciButton();
		}else{
			$('#btnBaggageUpdate').disableAnciButton();
		}
		if(params.inflightServiceEnable){
			anciToModify[anciToModify.length]="SSR";
			$('#btnAddSrv4').enableAnciButton();
		}else{
			$('#btnAddSrv4').disableAnciButton();
		}
		if(params.airportServiceEnable){
			anciToModify[anciToModify.length]="AIRPORT_SERVICE";
			$('#btnHalaUpdate').enableAnciButton();
		}else{
			$('#btnHalaUpdate').disableAnciButton();
		}
		
		if(params.airportTransferEnable){
			anciToModify[anciToModify.length]="AIRPORT_TRANSFER";
			$('#btnAPTUpdate').enableAnciButton();
		}else{
			$('#btnAPTUpdate').disableAnciButton();
		}
		
		if(params.insuranceEnable){
			anciToModify[anciToModify.length]="INSURANCE";
			$('#btnInsUpdate').enableAnciButton();
		}else{
			$('#btnInsUpdate').disableAnciButton();
		}
		
		if (params.addGroundSegment) {
			$("#btnAddBusSegment").show();

		}else {
			$("#btnAddBusSegment").hide();
		}
		
		if (showaddAnciHeading){
			$("#anciButtonHdPannel").show();
		}else{
			$("#anciButtonHdPannel").hide();
		}
				
		if (params.makePayment == true) {
			$('#btnMakePayment').show();
			$('#btnMakePayment').enableAnciButton();
		}else {
			$('#btnMakePayment').hide();
			$('#btnMakePayment').disableAnciButton();
		}
		
		if ((status == "ONHOLD") || (status == "FORCED-CONFIRM" && !params.allowForceConfirmModify)) {
			$('#btnSeatUpdate').disableAnciButton();
			$('#btnSeatUpdate').hide();
			$('#btnmealUpdate').disableAnciButton();
			$('#btnmealUpdate').hide();
			$('#btnBaggageUpdate').disableAnciButton();
			$('#btnBaggageUpdate').hide();
			$('#btnAddSrv4').disableAnciButton();
			$('#btnAddSrv4').hide();
			$('#btnHalaUpdate').disableAnciButton();
			$('#btnHalaUpdate').hide();
			$('#btnAPTUpdate').disableAnciButton();
			$('#btnAPTUpdate').hide();
			$('#btnInsUpdate').disableAnciButton();
			$('#btnInsUpdate').hide();	
			$('#btnMakePayment').show();
		}
		
		modifyFFID = modifyFFID && params.lmsEnabled;

		if (params.nameChangeAllowed && isAtLeastOnePaxEditable()) {
			$("#btnNameChange").show();
			$("#btnFFIDChange").hide();
		}  else if (modifyFFID) {
			$("#btnNameChange").hide();
			$("#btnFFIDChange").show();			
		} else {
			$("#btnNameChange").hide();
			$("#btnFFIDChange").hide();
		}		
		
		UI_CustReservation.anciToModify=anciToModify;
	} else {
		$("#trDepartureButtons").hide();
	}

}

isAtLeastOnePaxEditable = function(){
	var atLeastOnePaxEditable = false;
	if(UI_CustReservation.colAdultCollection.length > 0){	
		$.each(UI_CustReservation.colAdultCollection,function(index,paxData){
			atLeastOnePaxEditable = atLeastOnePaxEditable || paxData.editableFlag;
		});
	}
	
	if(UI_CustReservation.colChildCollection.length > 0){	
		$.each(UI_CustReservation.colChildCollection,function(index,paxData){
			atLeastOnePaxEditable = atLeastOnePaxEditable || paxData.editableFlag;
		});
	}
	
	if(UI_CustReservation.colInfantCollection.length > 0){	
		$.each(UI_CustReservation.colInfantCollection,function(index,paxData){
			atLeastOnePaxEditable = atLeastOnePaxEditable || paxData.editableFlag;
		});
	}
	return atLeastOnePaxEditable;
}

UI_CustReservation.anButtonDisable = function(data) {
	$("#" + data.id).addClass("AuxButtonDisable");
}

/*
 * sets the contact details
 */
UI_CustReservation.fillContactInfo = function(contacInfo) {
	$("#txtFNameRes").val(contacInfo.firstName);
	$("#txtLNameRes").val(contacInfo.lastName);
	$("#selCTitleRes").val(contacInfo.title);	
	$("#txtStreetRes").val(contacInfo.addresStreet);
	$("#txtAddressRes").val(contacInfo.addresline);
	//$("#txtAdd1Res").val(contacInfo.streetAddress1);
	//$("#txtAdd2Res").val(contacInfo.streetAddress2);
	$("#selNationalityRes").val(contacInfo.nationality);
	$("#txtCityRes").val(contacInfo.city);
	$("#txtStateRes").val(contacInfo.state);
	$("#selCountryRes").val(contacInfo.country);
	$("#txtZipCodeRes").val(contacInfo.zipCode);
	$("#txtMCountryRes").val(contacInfo.mCountry);
	$("#txtPCountryRes").val(contacInfo.lCountry);
	$("#txtFCountryRes").val(contacInfo.fCountry);
	$("#txtEmailRes").val(contacInfo.emailAddress);
	if(contacInfo.preferredLangauge == null ||
			contacInfo.preferredLangauge == ""){
		$("#selPrefLang").val(GLOBALS.sysLanguage);
		//$("#selLanguageRes").val(GLOBALS.sysLanguage);
	} else {
		$("#selPrefLang").val(contacInfo.preferredLangauge);
		//$("#selLanguageRes").val(contacInfo.preferredLangauge);
	}
	
	//set emergency contact information
	$("#selEmgnTitleRes").val(contacInfo.emgnTitle);
	$("#txtEmgnFNameRes").val(contacInfo.emgnFirstName);
	$("#txtEmgnLNameRes").val(contacInfo.emgnLastName);
	$("#txtEmgnPCountryRes").val(contacInfo.emgnLCountry);
	$("#txtEmgnPAreaRes").val(contacInfo.emgnLArea);
	$("#txtEmgnPhoneRes").val(contacInfo.emgnLNumber);
	$("#txtEmgnEmailRes").val(contacInfo.emgnEmail);
	
	if (!contactConfig.areaCode1.ibeVisibility){
	//if(!contactConfig.ibeAreaCodeVisible){
		$("#txtEmgnPhoneRes").val(contacInfo.emgnLArea+contacInfo.emgnLNumber);
		$("#txtMobileRes").val(contacInfo.mArea + contacInfo.mNumber);
		$("#txtPhoneRes").val(contacInfo.lArea + contacInfo.lNumber);
		$("#txtFaxRes").val(contacInfo.fArea + contacInfo.fNumber);
	}else{
		$("#txtEmgnPAreaRes").val(contacInfo.emgnLArea);
		$("#txtEmgnPhoneRes").val(contacInfo.emgnLNumber);
		$("#txtMAreaRes").val(contacInfo.mArea);
		$("#txtMobileRes").val(contacInfo.mNumber);
		$("#txtPAreaRes").val(contacInfo.lArea);
		$("#txtPhoneRes").val(contacInfo.lNumber);
		$("#txtFAreaRes").val(contacInfo.fArea);
	    $("#txtFaxRes").val(contacInfo.fNumber);
	}
	
	// Control Email Me Button
	UI_CustReservation.displayEmailMeButton({emailAddress : $("#txtEmailRes").val()});
	
}

/**
 * Display Email Me Button
 */
UI_CustReservation.displayEmailMeButton = function(params) {
	if (params.emailAddress != null && $.trim(params.emailAddress) != "") {
		$("#btnEmailRes").show();
		$("#btnEmailRes").removeClass("ButtonLargeDisable");
		$("#btnEmailRes").enable();
	} else {	
		$("#btnEmailRes").disable();		
		$("#btnEmailRes").hide();
		
	}
}


// Set Flight logo using flight data
UI_CustReservation.setImage = function(flightSegments,carrierImage, flightData) {	
	for (var i = 0; i < flightData.length; i++) {
		 $('#' +flightSegments+ '\\['+ i +'\\]\\.' +carrierImage).attr({  
		  src: flightData[i].carrierImagePath, 
		  title: flightData[i].carrierCode, 
		  alt: flightData[i].carrierCode 	 
		});		
	}
}


UI_CustReservation.clientValidate = function(){
	var strChkEmpty = "";
	var blnReturn = true;
	var strErrorHTML = "";
	
	/*$("#selCTitleRes").addClass("clsSelect");
	$("#txtStreetRes").addClass("clsInput");
	$("#txtAddressRes").addClass("clsInput");
	$("#txtCityRes").addClass("clsInput");	
	$("#txtStateRes").addClass("clsInput");	
	$("#selCountryRes").addClass("clsSelect");
	$("#txtFaxRes").addClass("clsSelect");
	$("#selNationalityRes").addClass("clsSelect");
	$("#txtFNameRes").addClass("clsInput fontCapitalize");	
	$("#txtLNameRes").addClass("clsInput fontCapitalize");			
	$("#txtMobileRes").addClass("clsInput");
	$("#txtMCountryRes").addClass("clsInput");
	$("#txtMAreaRes").addClass("clsInput");	
	$("#txtPhoneRes").addClass("clsInput");
	$("#txtPCountryRes").addClass("clsInput");
	$("#txtPAreaRes").addClass("clsInput");
	$("#txFaxRes").addClass("clsInput");
	$("#txtFCountryRes").addClass("clsInput");
	$("#txtFAreaRes").addClass("clsInput");
	$("#txtEmailRes").addClass("clsInput");	
	$("#txtVerifyEmailRes").addClass("clsInput");
	
	$("#selEmgnTitleRes").addClass("clsSelect");
	$("#txtEmgnFirstNameRes").addClass("clsInput");
	$("#txtEmgnLastNameRes").addClass("clsInput");
	$("#txtEmgnPCountryRes").addClass("clsInput");	
	$("#txtEmgnPAreaRes").addClass("clsInput");	
	$("#txtEmgnPhoneRes").addClass("clsInput");
	$("#txtEmgnEmailRes").addClass("clsInput");*/
	
	$("#selCTitleRes").removeClass("errorControl");		
	$("#txtStreetRes").removeClass("errorControl");
	$("#txtAddressRes").removeClass("errorControl");
	$("#txtCityRes").removeClass("errorControl");
	$("#txtZipCodeRes").removeClass("errorControl");	
	$("#selCountryRes").removeClass("errorControl");
	$("#txtFaxRes").removeClass("errorControl");
	$("#selNationalityRes").removeClass("errorControl");
	$("#txtLNameRes").removeClass("errorControl");
	$("#txtFNameRes").removeClass("errorControl");		
	$("#txtMobileRes").removeClass("errorControl");
	$("#txtMCountryRes").removeClass("errorControl");
	$("#txtMAreaRes").removeClass("errorControl");	
	$("#txtPhoneRes").removeClass("errorControl");
	$("#txtPCountryRes").removeClass("errorControl");
	$("#txtPAreaRes").removeClass("errorControl");
	$("#txtFaxRes").removeClass("errorControl");
	$("#txtFCountryRes").removeClass("errorControl");
	$("#txtFAreaRes").removeClass("errorControl");
	$("#txtEmailRes").removeClass("errorControl");
	$("#txtVerifyEmailRes").removeClass("errorControl");
	$("#selEmgnTitleRes").removeClass("errorControl");
	$("#txtEmgnFNameRes").removeClass("errorControl");
	$("#txtEmgnLNameRes").removeClass("errorControl");
	$("#txtEmgnPCountryRes").removeClass("errorControl");
	$("#txtEmgnPAreaRes").removeClass("errorControl");
	$("#txtEmgnPhoneRes").removeClass("errorControl");
	$("#txtEmgnEmailRes").removeClass("errorControl");
	
	
	// ---------------- Buyers information
	if(contactConfig.title1.ibeVisibility && contactConfig.title1.mandatory){
		if ($("#selCTitleRes").val() == ""){
			strErrorHTML += "<li> " + raiseError("ERR042");
			$("#selCTitleRes").addClass("errorControl");	
		}
	}
	
	if(contactConfig.firstName1.ibeVisibility && contactConfig.firstName1.mandatory){
		if (trim($("#txtFNameRes").val()) == ""){
			strErrorHTML += "<li> " + raiseError("ERR028");
			$("#txtFNameRes").addClass("errorControl fontCapitalize");	
		} else if(!isAlphaWhiteSpace(trim(getValue("txtFNameRes")))){
			strErrorHTML += "<li> " + raiseError("VFNAME");
			$("#txtFNameRes").addClass("errorControl");			
		}
	} else if(contactConfig.firstName1.ibeVisibility && 
			!contactConfig.firstName1.mandatory && trim($("#txtFNameRes").val()) != ""){
		if(!isAlphaWhiteSpace(trim(getValue("txtFNameRes")))){
			strErrorHTML += "<li> " + raiseError("VFNAME");
			$("#txtFNameRes").addClass("errorControl");			
		}
	}
	
	if(contactConfig.lastName1.ibeVisibility && contactConfig.lastName1.mandatory){
		if (trim($("#txtLNameRes").val()) == ""){
			strErrorHTML += "<li> " + raiseError("ERR029");
			$("#txtLNameRes").addClass("errorControl");
		} else if(!isAlphaWhiteSpace(trim(getValue("txtLNameRes")))){
			strErrorHTML += "<li> " + raiseError("VLNAME");
			$("#txtLNameRes").addClass("errorControl");			
		}
	} else if(contactConfig.lastName1.ibeVisibility &&
			!contactConfig.lastName1.mandatory && trim($("#txtLNameRes").val()) != ""){
		if(!isAlphaWhiteSpace(trim(getValue("txtLNameRes")))){
			strErrorHTML += "<li> " + raiseError("VLNAME");
			$("#txtLNameRes").addClass("errorControl");			
		}
	}
	
	if(contactConfig.nationality1.ibeVisibility && contactConfig.nationality1.mandatory){
		if (trim($("#selNationalityRes").val()) == ""){
			strErrorHTML += "<li> " + raiseError("ERR048");
			$("#selNationalityRes").addClass("errorControl");	
		}
	}
	
	if(contactConfig.country1.ibeVisibility && contactConfig.country1.mandatory){
		if (trim($("#selCountryRes").val()) == ""){
			strErrorHTML += "<li> " + raiseError("ERR041");
		    $("#selCountryRes").addClass("errorControl");	
		}
	}
	
	if(contactConfig.city1.ibeVisibility && contactConfig.city1.mandatory){
		if (trim($("#txtCityRes").val()) == ""){
			strErrorHTML += "<li> " + raiseError("ERR049");
			$("#txtCityRes").addClass("errorControl");	
		} else if(!isAlphaWhiteSpace(trim(getValue("txtCityRes")))){
			strErrorHTML += "<li> " + raiseError("VCITY");
			$("#txtCityRes").addClass("errorControl");			
		}
	} else if(contactConfig.city1.ibeVisibility && 
			!contactConfig.city1.mandatory && trim($("#txtCityRes").val()) != ""){
		if(!isAlphaWhiteSpace(trim(getValue("txtCityRes")))){
			strErrorHTML += "<li> " + raiseError("VCITY");
			$("#txtCityRes").addClass("errorControl");			
		}
	}
	
	if(contactConfig.zipCode1.ibeVisibility && contactConfig.zipCode1.mandatory){
		if (trim(getValue("txtZipCodeRes")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR063");
			$("#txtZipCodeRes").addClass("errorControl");		
		} else if(!validateZipCode(trim(getValue("txtZipCode")))){
			strErrorHTML += "<li> " + raiseError("ERR064");
			$("#txtZipCodeRes").addClass("errorControl");
		}
	} else if(contactConfig.zipCode1.ibeVisibility && 
			!contactConfig.zipCode1.mandatory &&
			trim(getValue("txtZipCodeRes"))!=""){
		if(!validateZipCode(trim(getValue("txtZipCodeRes")))){
			strErrorHTML += "<li> " + raiseError("ERR064");
			$("#txtZipCodeRes").addClass("errorControl");
		}
	}
	
	var phoneErr = UI_CustReservation.TelePhoneValidate();
	
	if(phoneErr != ""){
		strErrorHTML += phoneErr;
	}	
	
	if(contactConfig.email1.ibeVisibility && contactConfig.email1.mandatory){
		if (trim($("#txtEmailRes").val()) == ""){
			strErrorHTML += "<li> " + raiseError("ERR032");
			$("#txtEmailRes").addClass("errorControl");		
		} else {
			if (!checkEmail($("#txtEmailRes").val())){
				strErrorHTML += "<li> " + raiseError("ERR032");
				$("#txtEmailRes").addClass("errorControl");		
			} else if ($("#txtEmailRes").val().toLowerCase() != $("#txtVerifyEmailRes").val().toLowerCase()){
				strErrorHTML += "<li> " + raiseError("ERR033");
				$("#txtEmailRes").addClass("errorControl");		
				$("#txtVerifyEmailRes").addClass("errorControl");		
			} else if(validateEmailDomain){
				if(!UI_CustReservation.checkEmailDomain($("#txtEmailRes").val())){
				strErrorHTML += "<li> " + raiseError("ERR032");
				$("#txtEmailRes").addClass("errorControl");	
				}
			}
		}
	} else if(contactConfig.email1.ibeVisibility &&
			contactConfig.email1.mandatory &&
			trim($("#txtEmailRes").val()) != ""){
		if (!checkEmail($("#txtEmailRes").val())){
			strErrorHTML += "<li> " + raiseError("ERR032");
			$("#txtEmailRes").addClass("errorControl");		
		} else if ($("#txtEmailRes").val().toLowerCase() != $("#txtVerifyEmailRes").val().toLowerCase()){
			strErrorHTML += "<li> " + raiseError("ERR033");
			$("#txtEmailRes").addClass("errorControl");		
			$("#txtVerifyEmailRes").addClass("errorControl");		
		} else if(validateEmailDomain){
			if(!UI_CustReservation.checkEmailDomain($("#txtEmailRes").val())){
			strErrorHTML += "<li> " + raiseError("ERR032");
			$("#txtEmailRes").addClass("errorControl");	
			}
		}
	}
	
	var groupErr = groupFieldValidation(validationGroup, jsonFieldToElementMapping, jsonGroupValidation);
	
	if(groupErr != ""){
		strErrorHTML += groupErr;
	}
	
	if(phoneErr == "" && groupErr == ""){
		if(trim($("#txtMobileRes").val()) == ""){
			$("#txtMAreaRes").val("");
			$("#txtMCountryRes").val("");
		}
		
		if(trim($("#txtPhoneRes").val()) == ""){
			$("#txtPCountryRes").val("");
			$("#txtPAreaRes").val("");
		}
		
		if(trim($("#txtFaxRes").val()) == ""){
			$("#txtFCountryRes").val("");
			$("#txtFAreaRes").val("");
		}
	}

	
	//validate emergency contact information
	if(contactConfig.firstName2.ibeVisibility ||
			contactConfig.lastName2.ibeVisibility ||
			contactConfig.phoneNo2.ibeVisibility){
		
		if(contactConfig.title2.ibeVisibility && contactConfig.title2.mandatory){
			if (trim(getValue("selEmgnTitleRes")) == ""){
				strErrorHTML += "<li> " + raiseError("ERR072");
				$("#selEmgnTitleRes").addClass("errorControl");	
			}
		}
		
		if(contactConfig.firstName2.ibeVisibility && contactConfig.firstName2.mandatory){
			if (trim(getValue("txtEmgnFNameRes")) == ""){
				strErrorHTML += "<li> " + raiseError("ERR073");
				$("#txtEmgnFNameRes").addClass("errorControl");
			} else if(!isAlphaWhiteSpace(trim(getValue("txtEmgnFNameRes")))){
				strErrorHTML += "<li> " + raiseError("ERR074");
				$("#txtEmgnFNameRes").addClass("errorControl");
			}
		} else if(contactConfig.firstName2.ibeVisibility &&
				!contactConfig.firstName2.mandatory
				&& trim(getValue("txtEmgnFNameRes")) != ""){
			if(!isAlphaWhiteSpace(trim(getValue("txtEmgnFNameRes")))){
				strErrorHTML += "<li> " + raiseError("ERR074");
				$("#txtEmgnFNameRes").addClass("errorControl");
			}
		}
		
		if(contactConfig.lastName2.ibeVisibility && contactConfig.lastName2.mandatory){
			if (trim(getValue("txtEmgnLNameRes")) == ""){
				strErrorHTML += "<li> " + raiseError("ERR075");
				$("#txtEmgnLNameRes").addClass("errorControl");	
			} else if(!isAlphaWhiteSpace(trim(getValue("txtEmgnLNameRes")))){
				strErrorHTML += "<li> " + raiseError("ERR076");
				$("#txtEmgnLNameRes").addClass("errorControl");
			}
		} else if(contactConfig.lastName2.ibeVisibility &&
				!contactConfig.lastName2.mandatory
				&& trim(getValue("txtEmgnLNameRes")) != ""){
			if(!isAlphaWhiteSpace(trim(getValue("txtEmgnLNameRes")))){
				strErrorHTML += "<li> " + raiseError("ERR076");
				$("#txtEmgnLNameRes").addClass("errorControl");
			}
		}
		
		if(contactConfig.phoneNo2.ibeVisibility && contactConfig.phoneNo2.mandatory){
	    	if(trim($("#txtEmgnPCountryRes").val()) != "") {
	    		if (validateInteger($("#txtEmgnPCountryRes").val())){				
	    			removeZero("txtEmgnPCountryRes");
	    		}else {
	    			$("#txtEmgnPCountryRes").val("");
	    		}			
	    	}
	    	if(trim($("#txtEmgnPAreaRes").val()) != "") {
	    		if (validateInteger($("#txtEmgnPAreaRes").val())){				
	    			removeZero("txtEmgnPAreaRes");
	    		}else {
	    			$("#txtEmgnPAreaRes").val("");
	    		}		
	    	}
	    	if(trim($("#txtEmgnPhoneRes").val()) != "") {
	    		if (validateInteger($("#txtEmgnPhoneRes").val())){				
	    			removeZero("txtEmgnPhoneRes");
	    		}else {
	    			$("#txtEmgnPhoneRes").val("");
	    		}		
	    	}
	    	
	    	if (trim($("#txtEmgnPhoneRes").val()) == ""){
	    		strErrorHTML += "<li>" + raiseError("ERR079");
	    		$("#txtEmgnPhoneRes").addClass("errorControl");	
	    		$("#txtEmgnPCountryRes").addClass("errorControl");
	    		$("#txtEmgnPAreaRes").addClass("errorControl");
	    	}
	    	
	    	if (trim($("#txtEmgnPhoneRes").val()) != ""){
	    		if (trim($("#txtEmgnPCountryRes").val()) == ""){
	    			strErrorHTML += "<li> " + raiseError("ERR077");
	    			$("#txtEmgnPCountryRes").addClass("errorControl");	
	    		}
	    		if (contactConfig.areaCode1.ibeVisibility){
	    		//if (contactConfig.ibeUserRegAreaCodeVisible){
		    		if (trim($("#txtEmgnPAreaRes").val()) == ""){
		    			strErrorHTML += "<li> " + raiseError("ERR078");
		    			$("#txtEmgnPAreaRes").addClass("errorControl");	
		    		}
	    		}
	    	}
	    	
	    //check phone no is not mandatory & user inserted a phone no
	    } else if(contactConfig.phoneNo2.ibeVisibility && 
	    		!contactConfig.phoneNo2.mandatory &&				
				trim($("#txtEmgnPhoneRes").val()) != ""){
	    	if (validateInteger($("#txtEmgnPCountryRes").val())){				
				removeZero("txtEmgnPCountryRes");
			}else {
				strErrorHTML += "<li> " + raiseError("ERR077");
    			$("#txtEmgnPCountryRes").addClass("errorControl");	
				$("#txtEmgnPCountryRes").val("");
			}
	    	if (contactConfig.areaCode1.ibeVisibility) {
	    	//if (contactConfig.ibeUserRegAreaCodeVisible){
		    	if (validateInteger($("#txtEmgnPAreaRes").val())){				
					removeZero("txtEmgnPAreaRes");
				}else {
					strErrorHTML += "<li> " + raiseError("ERR078");
	    			$("#txtEmgnPAreaRes").addClass("errorControl");
					$("#txtEmgnPAreaRes").val("");
				}
	    	}
	    	
	    	if (validateInteger($("#txtEmgnPhoneRes").val())){				
				removeZero("txtEmgnPhoneRes");
			}else {
				$("#txtEmgnPhoneRes").val("");
			}
	    	
	    } else {
	    	$("#txtEmgnPCountryRes").val("");
	    	$("#txtEmgnPAreaRes").val("");
	    	$("#txtEmgnPhoneRes").val("");
	    }
		
		if(contactConfig.email2.ibeVisibility && contactConfig.email2.mandatory){
			if ($("#txtEmgnEmailRes").val() == ""){
				strErrorHTML += "<li> " + raiseError("ERR080");
				$("#txtEmgnEmailRes").addClass("errorControl");
			}
			
			
			if ($("#txtEmgnEmailRes").val() != ""){
				if (!checkEmail($("#txtEmgnEmailRes").val())){
					strErrorHTML += "<li> " + raiseError("ERR080");
					$("#txtEmgnEmailRes").addClass("errorControl");
				}else if(validateEmailDomain){
					if(!UI_CustReservation.checkEmailDomain($("#txtEmgnEmailRes").val())){
						strErrorHTML += "<li> " + raiseError("ERR081");
						$("#txtEmgnEmailRes").addClass("errorControl");	
					}			
				} 
			}
		} else if(contactConfig.email2.ibeVisibility && 
				!contactConfig.email2.mandatory &&
				trim($("#txtEmgnEmailRes").val()) != ""){
			if (!checkEmail($("#txtEmgnEmailRes").val())){
				strErrorHTML += "<li> " + raiseError("ERR080");
				$("#txtEmgnEmailRes").addClass("errorControl");	
			}else if(validateEmailDomain){
				if(!UI_CustReservation.checkEmailDomain($("#txtEmgnEmailRes").val())){
					strErrorHTML += "<li> " + raiseError("ERR081");
					$("#txtEmgnEmailRes").addClass("errorControl");	
				}			
			} 	
		} else {
			$("#txtEmgnEmailRes").val("");
		}
	}
	
	if (strErrorHTML != "" && phoneErr == "") {
		UI_CustReservation.resetPhoneNumbers();
	}
		
	if (strErrorHTML != ""){ strErrorHTML += "<br><br>";}
	$("#spnErrorRes").html("<font class='mandatory'>" + strErrorHTML + "<\/font>");
	if (strErrorHTML == ""){
		UI_CustReservation.removeClass();
		return true;
	}else{
		return false;
	}
}

UI_CustReservation.resetPhoneNumbers = function() {
	if (!contactConfig.areaCode1.ibeVisibility) {
		if (trim($("#txtMAreaRes").val()) != "") {
			var mobileNum =  trim($("#txtMAreaRes").val()) + trim($("#txtMobileRes").val());
			$("#txtMobileRes").val(trim(mobileNum));
		}
		
		if (trim($("#txtPAreaRes").val()) != "") {
			var phoneNum = trim($("#txtPAreaRes").val()) + trim($("#txtPhoneRes").val());
			$("#txtPhoneRes").val(trim(phoneNum));
		}
	}
}


function groupFieldValidation(groupObj, jsonFieldToElementMapping, jsonGroupValidation) {
	var errorHtml = "";
	var rVal = true;
	$.each(groupObj, function(key, fieldList){
		var valid = false;
		$.each(fieldList, function(index, fVal){
			if(trim($(jsonFieldToElementMapping[fVal]).val()) != ""){
				valid = true;
			}
		});
		
		if(!valid){
			errorHtml += "<li> " + raiseError(jsonGroupValidation[key]);	
			$.each(jsonGroupElementMapping[key],function(index, elem){
				$(elem).addClass("errorControl");
			});
		}
	});
	
	return errorHtml;
}



// TODO move all the following methods to a common file

function isAlphaWhiteSpace(s){
	var re = new RegExp("^[a-zA-Z-/ \w\s]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
}

function validateInteger( strValue ) {
	var objRegExp  = /(^-?[0-9][0-9]*$)/;

  	// check for integer characters
  	return objRegExp.test(strValue);
}

function removeZero(field) {
	var val = trim($("#"+field).val());
	$("#"+field).val(Number(val) * 1);		
}

function checkEmail(s){
	var re = new RegExp("^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,7}$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	// return RegExp( "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$"
	// ).test(s);
}

function raiseError(strErrNo){
	var strMsg = errorInfo[strErrNo];
	if (arguments.length >1){
		for (var i = 0 ; i < arguments.length - 1 ; i++){
			strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
		}
	}
	return strMsg;
}

/**
 * Country List Change
 */
UI_CustReservation.countryListChange = function() {
	$("#txtMCountryRes").val("");
	$("#txtPCountryRes").val("");
	$("#txtFCountryRes").val("");
	var choseCountry = $("#selCountryRes").val();
	for(var x=0;x<arrCountryPhone.length;x++) {
		if(choseCountry == arrCountryPhone[x][4]) {
			$("#txtMCountryRes").val(arrCountryPhone[x][0]);
			$("#txtPCountryRes").val(arrCountryPhone[x][0]);	
			$("#txtFCountryRes").val(arrCountryPhone[x][0]);	
			break;
		}
	}
}

/**
 * Remove CSS Class
 */
UI_CustReservation.removeClass = function() {
	$("body").find(".errorControl").each(function( intIndex ) {
		$("#"+ this.id).removeClass("errorControl");
	});
}
/**
 * Register Event
 */
UI_CustReservation.registerEvent = function(params) {	
	for (var i = 0; i <= params.segmentSize; i++) {		
		$("#" + params.templateName + "\\[" + i +"\\]\\." + params.eventId ).click(function(event){ UI_CustReservation.segmentCheckClick(this); });
	}
}
/**
 * Segment Check click
 */
UI_CustReservation.segmentCheckClick = function(obj) {
	UI_CustReservation.flightRefNos = obj.value;	
	if(obj.checked == true) {
		UI_CustReservation.uncheckOtherSegments();
		obj.checked = true;
		UI_CustReservation.enableModifyBtns(UI_CustReservation.flightRefNos);
	} else {
		UI_CustReservation.disableModifyBtns();
	}
}

/**
 * Uncheck other check boxes
 */
UI_CustReservation.uncheckOtherSegments = function() { 	
	$("input[id$='flightSegmentRefNumber']").each(function( intIndex ) {	    
		this.checked = false;		
	});
}
/**
 * Enable Segment Click Buttons
 */
UI_CustReservation.enableModifyBtns = function(flightRefNos) {
	$("#btnCancelRes").enableSegBtn();
	var isCancellable = true;
	var isModifiable = true;
	var isModifiable = true;
	var isAddGroundSegmentAllowed = false;
	var isGroundSegment = false;
	var totalCnfSegment =0;
	var isConnFareExist = false;
	var flightSegs = flightRefNos.split(":");
	var inputArr = UI_CustReservation.flightDepDetails.concat(UI_CustReservation.flightRetDetails);
	var isWithGroundSegment = false;
	
	for(var i=0; i<inputArr.length; i++ ){
		if(inputArr[i].status == 'CNF'){
			for(var j=0; j<flightSegs.length; j++ ){
				if(inputArr[i].flightSegmentRefNumber==flightSegs[j]){

					if(j>0)
						isConnFareExist = true;					
					
					if(!inputArr[i].isCancellable){
						isCancellable = false;
					}					
					//segment modification IBE
					if(inputArr[i].isAllowModifyByDate || inputArr[i].isAllowModifyByRoute){
						isModifiable = isModifiable && true;
					}else{
						isModifiable = isModifiable && false;
					}
					
					if(inputArr[i].groundStationAddAllowed){
						isAddGroundSegmentAllowed = true;
					}	
					
					if(inputArr[i].groundSegment){
						isGroundSegment = true;
					}
					
					if(!isGroundSegment && inputArr[i].groundSegmentPnrSegId!=undefined 
							&& inputArr[i].groundSegmentPnrSegId!=null 
							&& $.trim(inputArr[i].groundSegmentPnrSegId)!=''){
						isWithGroundSegment = true;
					}
				}
			}
			totalCnfSegment++;
		}
	}
	
	if((isCancellable && totalCnfSegment < 2) || (totalCnfSegment==2 && isConnFareExist) 
			|| (totalCnfSegment==2 && isWithGroundSegment)){
		isCancellable=false;
	}
	
	if(isCancellable){
		$("#btnCancelSegment").enableSegBtn();
	} else {
		$("#btnCancelSegment").disableSegBtn();
	}

	if(isModifiable && !isGroundSegment){
		$("#btnModify").enableSegBtn();
	} else {
		$("#btnModify").disableSegBtn();
	}
	
	if(isAddGroundSegmentAllowed){
		$("#btnAddBusSegment").enableSegBtn();
	} else {
		$("#btnAddBusSegment").disableSegBtn();
	}
	
	if(!$.isEmptyObject(UI_CustReservation.alertIdPnrSegIdMap) & UI_CustReservation.alertIdPnrSegIdMap[flightRefNos]){
		$("#btnTransfer").enableSegBtn();
	}else{
		$("#btnTransfer").disableSegBtn();
	}
}
/**
 * Disable Segment Click Buttons
 */
UI_CustReservation.disableModifyBtns = function() {
	$("#btnCancelSegment").disableSegBtn();
	$("#btnModify").disableSegBtn();
	$("#btnAddBusSegment").disableSegBtn();
	$("#btnTransfer").disableSegBtn();
}
/**
 * Enable Modify Segment Buttons
 */
$.fn.enableSegBtn = function() {	
	return this.removeClass("ButtonLargeDisable").addClass("ButtonLarge").enable();
}

/**
 * Enable Modify Segment Buttons
 */
$.fn.disableSegBtn = function() {	
	return this.removeClass("ButtonLarge").addClass("ButtonLargeDisable").disable();
}


/**
 * Enable Ancillary Buttons
 */
$.fn.enableAnciButton = function() {	
	return this.removeClass("AuxButtonDisable").enable();
}

/**
 * Disable Ancillary Buttons
 */
$.fn.disableAnciButton = function() {	
	return this.addClass("AuxButtonDisable").disable();
}

/*
 * Summarize Ancillary data to display
 */
function summarizeAnciData (objPDt) {
	for ( var i = 0; i < objPDt.length; i++) {
		var pax = objPDt[i];
		
		var segAnciCount = 0 ;
		var additionalInfo = '';
		pax.selectedAncillaries = $.airutil.sort.quickSort(pax.selectedAncillaries, segmentComparator);
		for ( var j = 0; j < pax.selectedAncillaries.length; j++) {
			var hasSeat = false;
			var hasMeal = false;
			var hasSSR = false;
			var hasBaggage = false;
			var hasAirportService = true;
			var hasAirportTransfer = false;
			var anci = pax.selectedAncillaries[j];
			anci.seat = '';
			if (anci.airSeatDTO != null && anci.airSeatDTO.seatNumber != null
					&& anci.airSeatDTO.seatNumber != '') {
				anci.seat = anci.airSeatDTO.seatNumber;
				hasSeat = true;
			}
			var meals = '';
			var meal = null
			for ( var k = 0; k < anci.mealDTOs.length; k++) {
				meal = anci.mealDTOs[k];
				if (meal.mealName != null
						&& meal.mealName != '') {						
					meals += meal.soldMeals + " x " + meal.mealName;										 
					if (k < (anci.mealDTOs.length -1)) {
						 meals += " , ";
					}
					hasMeal = true;	
				}
			} 
			anci.meals = meals;
			
			var baggages = '';
			for ( var k = 0; k < anci.baggageDTOs.length; k++) {
				if (anci.baggageDTOs[k].baggageName != null
						&& anci.baggageDTOs[k].baggageName != '') {
					if (k > 0) {
						baggages += ', ';
					}
					baggages += anci.baggageDTOs[k].baggageName;
					hasBaggage = true;
				}
			}
			anci.baggages = baggages;

			var ssrs = '';
			for ( var k = 0; k < anci.specialServiceRequestDTOs.length; k++) {
				if (anci.specialServiceRequestDTOs[k].description != null
						&& anci.specialServiceRequestDTOs[k].description != '') {
					if (k > 0) {
						ssrs += ', ';
					}
					ssrs += anci.specialServiceRequestDTOs[k].description;
					if (anci.specialServiceRequestDTOs[k].text != null
							&& anci.specialServiceRequestDTOs[k].text != '') {
						ssrs += (' - ' + anci.specialServiceRequestDTOs[k].text);
					}
					hasSSR = true;
				}
			}
			anci.ssrs = ssrs;
			
			var apss = '';
			for(var k=0; k<anci.airportServiceDTOs.length; k++){
				if (anci.airportServiceDTOs[k].ssrDescription != null
						&& anci.airportServiceDTOs[k].ssrDescription != '') {
					if (k > 0) {
						apss += ', ';
					}
					apss += anci.airportServiceDTOs[k].ssrDescription;
					if (anci.airportServiceDTOs[k].airportCode != null
							&& anci.airportServiceDTOs[k].airportCode != '') {
						apss += (' - ' + anci.airportServiceDTOs[k].airportCode);
					}
					hasAirportService = true;
				}
			}
			anci.apss = apss;
			
			var apts = '';
			for(var k=0; k<anci.airportTransferDTOs.length; k++){
				if (anci.airportTransferDTOs[k].ssrName != null
						&& anci.airportTransferDTOs[k].ssrName != '') {
					if (k > 0) {
						apts += ', ';
					}
					apts += anci.airportTransferDTOs[k].ssrName;
					if (anci.airportTransferDTOs[k].airportCode != null
							&& anci.airportTransferDTOs[k].airportCode != '') {
						apts += (' - ' + anci.airportTransferDTOs[k].airportCode);
					}
					hasAirportTransfer = true;
				}
			}
			anci.apts = apts;
			
			anci.additionalInfo = '';
			anci.segCode = anci.flightSegmentTO.segmentCode;
			if(segAnciCount >0){
				additionalInfo += '<br /><br />';
			}
			if(hasSSR || hasSeat || hasMeal || hasBaggage || hasAirportService || hasAirportTransfer){
				anci.additionalInfo = (anci.segCode + ' : ');
				var hasEntry = false;
				if(hasSeat){
					anci.additionalInfo += (anci.seat);
					hasEntry = true;
				}
				if(hasMeal){
					if(hasEntry) anci.additionalInfo += ', ';
					anci.additionalInfo += (anci.meals);
					hasEntry = true;
				}
				if(hasBaggage){
					if(hasEntry) anci.additionalInfo += ', ';
					anci.additionalInfo += (anci.baggages);
					hasEntry = true;
				}
				if(hasSSR){
					if(hasEntry) anci.additionalInfo += ', ';
					anci.additionalInfo += (anci.ssrs);
					hasEntry = true;
				}
				if(hasAirportService){
					if(hasEntry) anci.additionalInfo += ', ';
					anci.additionalInfo += (anci.apss);
					hasEntry = true;
				}
				if(hasAirportTransfer){
					if(hasEntry) anci.additionalInfo += ', ';
					anci.additionalInfo += (anci.apts);
					hasEntry = true;
				}
				segAnciCount ++;
			}
			
			additionalInfo += anci.additionalInfo;
		}
		pax.additionalInfo = additionalInfo;
	}
	return objPDt;
}

function segmentComparator(first ,second){
	var parser  = function(d){
		var a = d.split('T');
		var b = a[0].split('-');
		var c = a[1].split(':');
		return new Date(b[0],parseInt(b[1],10)-1,b[2],c[0],c[1],c[2]);
	}
	var d1 = parser(first.flightSegmentTO.departureDateTime);
	var d2 = parser(second.flightSegmentTO.departureDateTime);
	return (d1.getTime()-d2.getTime());
}

UI_CustReservation.hidePageMassges = function(groupPnr) {
	if (groupPnr != null && $.trim(groupPnr) != "") {
		$("#spnPernMsg").hide();
	} else {
		$("#spnPernMsg").show();
	}
}

// IE 6 Bug Fix
UI_CustReservation.setDropDowndata = function() {
	if ($.browser.msie && $.browser.version.substr(0,1)<7) {
		setTimeout("filldata()",10); 		
	} else {
		filldata();
	}
}

// validate E-mail domain
UI_CustReservation.checkEmailDomain = function(emailAdd){
	var data = {};
	data['domain'] = emailAdd.substring(emailAdd.indexOf("@")+1);	
	
	var retValue = null;
	$("#frmCustReservation").ajaxSubmit({dataType: 'json', url:'checkEMailDomain.action', async:false,
		data:data,
		success: function(response){ 
		retValue = response.exists;		
	},
	error:function(req,error){
   	          if(error === 'error'){error = req.statusText;}
   	          var errormsg = 'There was a communication error: '+error;
   	            container.html(errormsg);
   	         navCallBack(false);
   	        }
	});	
	
	return retValue;
}

function filldata() {	
	UI_CustReservation.fillContactInfo(UI_CustReservation.contactDetails);	
}

UI_CustReservation.buildPaymentDetails = function(params) {
	if (params != "") {				
		if (params.status == "CANCELLED") {			
			$("#cancelBalanceSummary").show();		
		} else {
			$("#cancelBalanceSummary").hide();		
		}
		$("#paymentDetails").nextAll().remove();
		$("#paymentDetails").iterateTemplete({templeteName:"paymentDetails", data:params.paymentData, dtoName:"balanceSummary"});
		$("#paymentDetails").nextAll().filter(':odd').find(" td").removeClass("rowColor").addClass("rowColorAlternate");
		
		if(UI_CustReservation.isCreditPromotion){
			$("#paymentDetails").nextAll().filter('tr:last').prev().find(" td").removeClass("rowColor").addClass("totalCol");
			$("#paymentDetails").nextAll().filter('tr:last').prev().find(" label").addClass("gridHDFont fntBold");
		} else {			
			$("#paymentDetails").nextAll().filter('tr:last').find(" td").removeClass("rowColor").addClass("totalCol");
			$("#paymentDetails").nextAll().filter('tr:last').find(" label").addClass("gridHDFont fntBold");
		}
		
	}
}

/**
 * Call Center Link
 */
UI_CustReservation.callCenterLinkSetup = function() {
	if(UI_Top.holder().GLOBALS.clientCallCenterURL === ""){
		$("#cancelBalanceSummary").find("#linkCallCenter").attr("href", SYS_IBECommonParam.homeURL + "/callcentres");
	} else {
		$("#cancelBalanceSummary").find("#linkCallCenter").attr("href", UI_Top.holder().GLOBALS.clientCallCenterURL).attr("target","_blank");
	}	
}

UI_CustReservation.groupPNR = function() {
	var isGruupPnr = false;
	if (UI_CustomerHome.groupPnr == "true") {
		isGruupPnr = true;
	} else if (UI_CustomerHome.groupPnr == "false") {
		isGruupPnr = false;
	} else {
		isGruupPnr = $.trim(UI_CustomerHome.groupPnr) != "" ?true:false;
	}
	return isGruupPnr;
}
// Build Telephone data
UI_CustReservation.buildTelephoneInfo = function(strCountryPhone, strAreaPhone) {	
	
	var tempArrAreaPhone = null;
	var tempCountryCode = null;
	var tempNewArray = null;
	
	if (strCountryPhone != null && strAreaPhone != null) {		
		var arrCountryPhoneLocal = strCountryPhone.split("^");
		for (var i = 0; i < arrCountryPhoneLocal.length; i++) {
			arrCountryPhone[i] = arrCountryPhoneLocal[i].split(",");
		}
		
		var arrAreaPhoneLocal = strAreaPhone.split("^");
		for (var i = 0; i < arrAreaPhoneLocal.length; i++) {
			tempArrAreaPhone = arrAreaPhoneLocal[i].split(",");	
			tempCountryCode = tempArrAreaPhone[0];
			tempNewArray = new Array();
			tempNewArray[0] = tempArrAreaPhone[1];
			tempNewArray[1]= tempArrAreaPhone[2];
			tempNewArray[2]= tempArrAreaPhone[3];
			
			if (arrAreaPhone[tempCountryCode] == undefined) {
				arrAreaPhone[tempCountryCode] = new Array();
			}			
			arrAreaPhone[tempCountryCode][i] = new Array();
			arrAreaPhone[tempCountryCode][i]  = tempNewArray;			
		}		
	}	
}

UI_CustReservation.TelePhoneValidate = function() {
	var telHtml = "";
	//############################## Mobile Number Mandatory Validation #############################
	//check mobile no is a mandatory field
    if(contactConfig.mobileNo1.ibeVisibility && contactConfig.mobileNo1.mandatory){
    	if(trim($("#txtMCountryRes").val()) != "") {
    		if (validateInteger(trim($("#txtMCountryRes").val()))){				
    			removeZero("txtMCountryRes");
    		}else {
    			$("#txtMCountryRes").val("");
    		}			
    	}
    	
    	if ( contactConfig.areaCode1.ibeVisibility && trim($("#txtMAreaRes").val()) != ""){
    		if(trim($("#txtMAreaRes").val()) != "") {
        		if (validateInteger(trim($("#txtMAreaRes").val()))){				
        			removeZero("txtMAreaRes");
        		}else {
        			$("#txtMArea").val("");
        		}		
        	}
    	}    	
    	
    	if(trim($("#txtMobileRes").val()) != "") {
    		if (validateInteger($("#txtMobileRes").val())){				
    			removeZero("txtMobileRes");
    			if(!contactConfig.areaCode1.ibeVisibility) {
	    			$("#txtMAreaRes").val(trim($("#txtMobileRes").val()).substr(0,2));
	    			$("#txtMobileRes").val(trim($("#txtMobileRes").val()).substr(2,trim($("#txtMobileRes").val()).length));
    			}
    			
    		}else {
    			$("#txtMobileRes").val("");
    		}		
    			
    	}
    	    	
    	if (trim($("#txtMobileRes").val()) == ""){
    		telHtml += "<li>" + raiseError("ERR030");	
    		$("#txtMobileRes").addClass("errorControl");	
    		$("#txtMCountryRes").addClass("errorControl");
    		$("#txtMAreaRes").addClass("errorControl");
    	}
    	
    	if (trim($("#txtMobileRes").val()) != ""){		
    		if (trim($("#txtMCountryRes").val()) == ""){
    			telHtml += "<li> " + raiseError("ERR036");
    			$("#txtMCountryRes").addClass("errorControl");	
    		}
    		if ( contactConfig.areaCode1.ibeVisibility  ){
    			if (trim($("#txtMAreaRes").val()) == ""){
        			telHtml += "<li> " + raiseError("ERR037");
        			$("#txtMAreaRes").addClass("errorControl");	
        		}
    		}    		
    	}
    	
    	telHtml += UI_CustReservation.validateMobileNoFormat("#txtMCountryRes","#txtMAreaRes","#txtMobileRes");
    	
    //check mobile no is not mandatory & user inserted a mobile no
	} else if(!contactConfig.mobileNo1.mandatory &&
			trim($("#txtMobileRes").val()) != ""){
		
		if (validateInteger(trim($("#txtMCountryRes").val()))){				
			removeZero("txtMCountryRes");
		}else {
			telHtml += "<li> " + raiseError("ERR036");
			$("#txtMCountryRes").addClass("errorControl");
			$("#txtMCountryRes").val("");
		}
		
		if ( contactConfig.areaCode1.ibeVisibility  ){
			if (validateInteger(trim($("#txtMAreaRes").val()))){				
				removeZero("txtMAreaRes");
			}else {
	            telHtml += "<li> " + raiseError("ERR037");
	    		$("#txtMAreaRes").addClass("errorControl");	
				$("#txtMAreaRes").val("");
			}
		}		
		
		if (validateInteger($("#txtMobileRes").val())){				
			removeZero("txtMobileRes");
			if(!contactConfig.areaCode1.ibeVisibility) {
    			$("#txtMAreaRes").val(trim($("#txtMobileRes").val()).substr(0,2));
    			$("#txtMobileRes").val(trim($("#txtMobileRes").val()).substr(2,trim($("#txtMobileRes").val()).length));
			}
		}else {
			$("#txtMobileRes").val("");
		}
		
		telHtml += UI_CustReservation.validateMobileNoFormat("#txtMCountryRes","#txtMAreaRes","#txtMobileRes");
				
	}
    
    
    
    
    //############################ Phone Number Mandatory Validation ####################################
    //check phone no is a mandatory field
    if(contactConfig.phoneNo1.ibeVisibility && contactConfig.phoneNo1.mandatory){
    	if(trim($("#txtPCountryRes").val()) != "") {
    		if (validateInteger($("#txtPCountryRes").val())){				
    			removeZero("txtPCountryRes");
    		}else {
    			$("#txtPCountryRes").val("");
    		}			
    	}
    	if ( contactConfig.areaCode1.ibeVisibility  ){
    		if(trim($("#txtPAreaRes").val()) != "") {
        		if (validateInteger($("#txtPAreaRes").val())){				
        			removeZero("txtPAreaRes");
        		}else {
        			$("#txtPAreaRes").val("");
        		}		
        	}
    	}    	
    	if(trim($("#txtPhoneRes").val()) != "") {
    		if (validateInteger($("#txtPhoneRes").val())){				
    			removeZero("txtPhoneRes");
    			if(!contactConfig.areaCode1.ibeVisibility) {
	    			$("#txtPAreaRes").val(trim($("#txtPhoneRes").val()).substr(0,2));
	    			$("#txtPhoneRes").val(trim($("#txtPhoneRes").val()).substr(2,trim($("#txtPhoneRes").val()).length));
    			}
    		}else {
    			$("#txtPhoneRes").val("");
    		}		
    	}
    	
    	if (trim($("#txtPhoneRes").val()) == ""){
    		telHtml += "<li>" + raiseError("ERR030");
    		$("#txtPhoneRes").addClass("errorControl");	
    		$("#txtPCountryRes").addClass("errorControl");
    		$("#txtPAreaRes").addClass("errorControl");	
    	}
    	
    	if (trim($("#txtPhoneRes").val()) != ""){
    		if (trim($("#txtPCountryRes").val()) == ""){
    			telHtml += "<li> " + raiseError("ERR034");
    			$("#txtPCountryRes").addClass("errorControl");	
    		}
    		if ( contactConfig.areaCode1.ibeVisibility  ){
    			if (trim($("#txtPAreaRes").val()) == ""){
        			telHtml += "<li> " + raiseError("ERR035");
        			$("#txtPAreaRes").addClass("errorControl");	
        		}
    		}    		
    	}
    	
    	telHtml += UI_CustReservation.validatePhoneNoFormat("#txtPCountryRes","#txtPAreaRes","#txtPhoneRes");
    	    	
    //check phone no is not mandatory & user inserted a phone no
    } else if(!contactConfig.phoneNo1.mandatory &&
			trim($("#txtPhoneRes").val()) != ""){
    	if (validateInteger($("#txtPCountryRes").val())){				
			removeZero("txtPCountryRes");
		}else {
			telHtml += "<li> " + raiseError("ERR034");
			$("#txtPCountryRes").addClass("errorControl");	
			$("#txtPCountryRes").val("");
		}
    	if ( contactConfig.areaCode1.ibeVisibility  ){
    		if (validateInteger($("#txtPAreaRes").val())){				
    			removeZero("txtPAreaRes");
    		}else {
    			telHtml += "<li> " + raiseError("ERR035");
        		$("#txtPAreaRes").addClass("errorControl");	
    			$("#txtPAreaRes").val("");
    		}
    	}    	
    	
    	if (validateInteger($("#txtPhoneRes").val())){				
			removeZero("txtPhoneRes");
			if(!contactConfig.areaCode1.ibeVisibility) {
    			$("#txtPAreaRes").val(trim($("#txtPhoneRes").val()).substr(0,2));
    			$("#txtPhoneRes").val(trim($("#txtPhoneRes").val()).substr(2,trim($("#txtPhoneRes").val()).length));
			}
		}else {
			$("#txtPhoneRes").val("");
		}
    	
    	telHtml += UI_CustReservation.validatePhoneNoFormat("#txtPCountryRes","#txtPAreaRes","#txtPhoneRes");
    	    	
    }
    
  //############################ Fax Number Mandatory Validation ####################################
    //check fax no is a mandatory field
    if(contactConfig.fax1.ibeVisibility && contactConfig.fax1.mandatory){
    	if(trim($("#txtFCountryRes").val()) != "") {
    		if (validateInteger($("#txtFCountryRes").val())){				
    			removeZero("txtFCountryRes");
    		}else {
    			$("#txtFCountryRes").val("");
    		}			
    	}
    	if ( contactConfig.areaCode1.ibeVisibility  ){
    		if(trim($("#txtFAreaRes").val()) != "") {
        		if (validateInteger($("#txtFAreaRes").val())){				
        			removeZero("txtFAreaRes");
        		}else {
        			$("#txtFAreaRes").val("");
        		}		
        	}
    	}    	
    	if(trim($("#txtFaxRes").val()) != "") {
    		if (validateInteger($("#txtFaxRes").val())){				
    			removeZero("txtFaxRes");
    			if(!contactConfig.areaCode1.ibeVisibility) {
        			$("#txtFAreaRes").val(trim($("#txtFaxRes").val()).substr(0,2));
        			$("#txtFaxRes").val(trim($("#txtFaxRes").val()).substr(2,trim($("#txtFaxRes").val()).length));
    			}
    		}else {
    			$("#txtFaxRes").val("");
    		}		
    	}
    	
    	if (trim($("#txtFaxRes").val()) == ""){
    		telHtml += "<li>" + raiseError("ERR066");
    		$("#txtFaxRes").addClass("errorControl");	
    		$("#txtFCountryRes").addClass("errorControl");
    		$("#txtFAreaRes").addClass("errorControl");	
    	}
    	
    	if (trim($("#txtFaxRes").val()) != ""){
    		if (trim($("#txtFCountryRes").val()) == ""){
    			telHtml += "<li> " + raiseError("ERR067");
    			$("#txtFCountryRes").addClass("errorControl");	
    		}
    		if ( contactConfig.areaCode1.ibeVisibility  ){
    			if (trim($("#txtFAreaRes").val()) == ""){
        			telHtml += "<li> " + raiseError("ERR068");
        			$("#txtFAreaRes").addClass("errorControl");	
        		}
    		}    		
    	}
    	
    	telHtml += UI_CustReservation.validatePhoneNoFormat("#txtFCountryRes","#txtFAreaRes","#txtFaxRes");
    	    	
    //check phone no is not mandatory & user inserted a phone no
    } else if(!contactConfig.fax1.mandatory &&
			trim($("#txtFaxRes").val()) != ""){
    	if (validateInteger($("#txtFCountryRes").val())){				
			removeZero("txtFCountryRes");
		}else {
			telHtml += "<li> " + raiseError("ERR067");
			$("#txtFCountryRes").addClass("errorControl");	
			$("#txtFCountryRes").val("");
		}
    	if ( contactConfig.areaCode1.ibeVisibility  ){
    		if (validateInteger($("#txtFAreaRes").val())){				
    			removeZero("txtFAreaRes");
    		}else {
       			telHtml += "<li> " + raiseError("ERR068");
        		$("#txtFAreaRes").addClass("errorControl");
    			$("#txtFAreaRes").val("");
    		}
    	}    	
    	
    	if (validateInteger($("#txtFaxRes").val())){				
			removeZero("txtFaxRes");
			if(!contactConfig.areaCode1.ibeVisibility) {
    			$("#txtFAreaRes").val(trim($("#txtFaxRes").val()).substr(0,2));
    			$("#txtFaxRes").val(trim($("#txtFaxRes").val()).substr(2,trim($("#txtFaxRes").val()).length));
			}
		}else {
			$("#txtFaxRes").val("");
		}
    	
    	telHtml += UI_CustReservation.validatePhoneNoFormat("#txtFCountryRes","#txtFAreaRes","#txtFaxRes");
    	
    }
    
    if(!contactConfig.areaCode1.ibeVisibility && telHtml != "") {
    	
    	if($("#txtMobileRes").val() != "") {
    		$("#txtMobileRes").val($("#txtMAreaRes").val() + $("#txtMobileRes").val());
    		$("#txtMAreaRes").val("");
    	}
    	
    	if($("#txtPhoneRes").val() != "") {
			$("#txtPhoneRes").val($("#txtPAreaRes").val() + $("#txtPhoneRes").val());
			$("#txtPAreaRes").val("");
    	}
		
    	if($("#txtFaxRes").val() != "") {
			$("#txtFaxRes").val($("#txtFAreaRes").val() + $("#txtFaxRes").val());
			$("#txtFAreaRes").val("");
    	}
    } 
    
    
	return telHtml;
	
}

//check whether phone number format is correct
UI_CustReservation.validatePhoneNoFormat = function(fieldCountryCode, fieldAreaCode, fieldPhone){
	var telHtml = "";
	var lancountryCode = trim($(fieldCountryCode).val());
	var lanareaCode =    trim($(fieldAreaCode).val());
	var lanno =    trim($(fieldPhone).val());
	var blnLanFound = true;
	var hsaLanNo = false;
	var selectedCountry = trim($("#selCountryRes").val());
	
	if ((selectedCountry != "OT" && selectedCountry != "" ) && lanno != "" && lancountryCode != "") {
	
		for(var pl=0;pl < arrCountryPhone.length;pl++) {
			if(lancountryCode == arrCountryPhone[pl][0]) {
				hsaLanNo = true;
				if(arrCountryPhone[pl][1] == 'Y' && contactConfig.areaCode1.ibeVisibility){
					if(typeof(arrAreaPhone[lancountryCode]) != 'undefined') {
						for(var al=0;al < arrAreaPhone[lancountryCode].length; al++){
							if(arrAreaPhone[lancountryCode][al][0] == lanareaCode 
								&& arrAreaPhone[lancountryCode][al][1] == 'LAND'
								&& arrAreaPhone[lancountryCode][al][2] == 'ACT') {
								blnLanFound = true;
								break;									
							} else {
								blnLanFound = false;
							}
						}
						
						if(!blnLanFound) {
							telHtml += "<li> " + raiseError("ERR050");
							$(fieldCountryCode).addClass("errorControl");		
							$(fieldAreaCode).addClass("errorControl");	
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][2] != ''){
					if(lanno != "") {
						if(lanno.length < Number(arrCountryPhone[pl][2])) {
							telHtml += "<li> " + raiseError("ERR051");
							$(fieldPhone).addClass("errorControl");									
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][3] != ''){
					
					if(lanno != "") {
						if(lanno.length > Number(arrCountryPhone[pl][3])) {
							telHtml += "<li> " + raiseError("ERR052");
							$(fieldPhone).addClass("errorControl");									
						}
					
					}
				
				
				}
			
			} 
		
		}
		
		if(!hsaLanNo) {
			telHtml += "<li> " + raiseError("ERR050");
			$(fieldCountryCode).addClass("errorControl");		
			$(fieldAreaCode).addClass("errorControl");	
		}				
	
	}
	
	return telHtml;
}

//check whether mobile num format is correct
UI_CustReservation.validateMobileNoFormat = function(fieldCountryCode, fieldAreaCode, fieldMobile){
	var telHtml = "";
	var countryCode = trim($(fieldCountryCode).val());
	var areaCode =    trim($(fieldAreaCode).val());
	var mobileno =    trim($(fieldMobile).val());
	var blnFound = true;
	var hsaNo = false;
	var selectedCountry = trim($("#selCountryRes").val());			
	
	if ((selectedCountry != "OT" && selectedCountry != "" ) && mobileno != "" && countryCode != "") {
	
		for(var pl=0;pl < arrCountryPhone.length;pl++) {					
			if(countryCode == arrCountryPhone[pl][0]) {
				hsaNo = true;
				if(arrCountryPhone[pl][1] == 'Y' && contactConfig.areaCode1.ibeVisibility){
					if(typeof(arrAreaPhone[countryCode]) != 'undefined') {
						for(var al=0;al < arrAreaPhone[countryCode].length; al++){
							if(arrAreaPhone[countryCode][al][0] == areaCode 
								&& arrAreaPhone[countryCode][al][1] == 'MOBILE'
								&& arrAreaPhone[countryCode][al][2] == 'ACT') {
								blnFound = true;
								break;									
							} else {
								blnFound = false;
							}
						}
						
						if(!blnFound) {
							telHtml += "<li> " + raiseError("ERR050");
							$(fieldCountryCode).addClass("errorControl");		
							$(fieldAreaCode).addClass("errorControl");	
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][2] != ''){
					if(mobileno != "") {
						if( contactConfig.areaCode1.ibeVisibility ){
							if(mobileno.length < Number(arrCountryPhone[pl][2])) {
								telHtml += "<li> " + raiseError("ERR051");
								$(fieldMobile).addClass("errorControl");									
							}
						}
					}
				
				}
				
				if(arrCountryPhone[pl][3] != ''){
					
					if(mobileno != "") {
						if( contactConfig.areaCode1.ibeVisibility ){
							if(mobileno.length > Number(arrCountryPhone[pl][3])) {
								telHtml += "<li> " + raiseError("ERR052");
								$(fieldMobile).addClass("errorControl");									
							}
						}	
					
					}
				
				
				}
			
			} 
		
		}
		if(!hsaNo) {
			telHtml += "<li> " + raiseError("ERR050");
			$(fieldCountryCode).addClass("errorControl");		
			$(fieldAreaCode).addClass("errorControl");	
		}
						
	
	}
	
	return telHtml;
}

UI_CustReservation.buildInsuranceData = function(bookingData) {
	var policyCode = bookingData.policyNo;
	UI_CustReservation.hasInsurance = bookingData.hasInsurance;
	if ( policyCode != null && $.trim(policyCode) != "") {
		$("#policyCode").text(policyCode);		
		if (!UI_commonSystem.isEmpty(bookingData.insuranceType)) {
			$("#lblInsuranceTypeValue").text(bookingData.insuranceType);
			$("#tdInsTypeDisplay").show()
		} else {
			$("#tdInsTypeDisplay").hide();
		}
	} else {
		$("#policyPannel").hide();
	}
}

function validateZipCode(field){
	var valid = "0123456789-";
	var hyphencount = 0;
	
	if (field.length!=5 && field.length!=10) {
		return false;
	}
	for (var i=0; i < field.length; i++) {
		var temp = "" + field.substring(i, i+1);
		if (temp == "-"){
			hyphencount++;
		}		
		if (valid.indexOf(temp) == "-1") {
			return false;
		}
		if ((hyphencount > 1) || ((field.length==10) && ""+field.charAt(5)!="-")) {
			return false;
		}
	}
	return true;
}

function displayToolTip(objId){	
	var objAltDt = UI_CustReservation.alertInfo;
	if(objAltDt.length != 0){
		var arrID = objId.substring(17,18);
		var id = (new  Number(arrID[0])) - 0;
		var pnrSegID = objAltDt[id].flightSegmantRefNumber;			
		var strData = "";
		var sHtm = "";
		
		if (pnrSegID != "" && objAltDt.length != 0) {
			for (var i = 0; i < objAltDt.length; i++) {
				if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
					for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++){
						if (strData != ""){ strData += "<hr>";}
						strData += "<font>" + objAltDt[i].alertTO[a].content + "</font>"		
					}						
					break;
				}
			}
			sHtm += strData;
		}		
	}
	
	$("#toolTipBody").html(sHtm);	
	var position  =  $("#lblDisplayStatus").position();
	var pos = $("#lblDisplayStatus").offset();
    var width = $("#lblDisplayStatus").width();
    $("#toolTip").css({
        left: (pos.left) -145 + 'px',
        top: pos.top - 20 + 'px'    
    });      
	$("#toolTip").show(); 	
}

function hideToolTip(){
	$("#toolTip").hide();	
}

UI_CustReservation.transferSegment = function(){
	
	var pnrSegId = UI_CustReservation.flightRefNos;
	var alertId = UI_CustReservation.alertIdPnrSegIdMap[UI_CustReservation.flightRefNos];
	var pnr = UI_CustReservation.pnr;
	
	$.each( UI_CustReservation.alertInfo , function( key, value ) {
	    if(value.flightSegmantRefNumber == pnrSegId){
	    	$.each(value.alertTO , function( key, value ) {
	            if(value.ibeActioned == 'Y'){
	            	jAlert('Error', 'Already Transferred This Segment');
	            	return;
	            }
	    	});
	    }
	});
	
	window.location.href = "showReservation.action?hdnParamData="+ GLOBALS.sysLanguage +"^SR^"+ pnr +"^" + alertId + "^" + pnrSegId;

}

UI_CustReservation.ready();
