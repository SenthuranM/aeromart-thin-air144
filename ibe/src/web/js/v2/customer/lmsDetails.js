function LMSDetails(){

    function initPage(){
        $("#btnActivateLMS1").off("click").on("click",activateLMS);
        UI_LMSDetails.loadPageData();
    }

    function activateLMS(){
    	UI_UserTabs.activateLms = true;
        UI_UserTabs.myReservationsClick();
    }

    showHideLMSPanelwithStatus = function(){
        if (UI_commonSystem.lmsDetails == {} || UI_commonSystem.lmsDetails == null
        		|| UI_commonSystem.lmsDetails.emailId == null 
				|| UI_commonSystem.lmsDetails.emailId == ""){
            $("#userNotAirewards").show();
            $("#userAirewards").hide();
        }else if(UI_commonSystem.lmsDetails.emailStatus=="N"){
        	$("#userAirewards").hide();
        	$("#userNotAirewards").show();
        	$("#btnActivateLMS1").hide();
        }else{
            $("#userAirewards").show();
            $("#userNotAirewards").hide();
        }
        UI_commonSystem.loadingCompleted();
    }

    this.loadLmsDetails = function(){
        initPage();
        showHideLMSPanelwithStatus();
    }

}

var UI_LMSDetails = new LMSDetails();

/**
 * Load page data
 */
UI_LMSDetails.loadPageData = function () {	
	UI_commonSystem.loadingProgress();
	$("#tdLms").ajaxSubmit({url:"showReservationList.action?randomNumber="+UI_UserTabs.getRandomNumber() , dataType: 'json', success: UI_LMSDetails.loadReservationDataSuccess, error:UI_commonSystem.setErrorStatus});	
	return false;	
} 

/**
 * Load data success
 */
UI_LMSDetails.loadReservationDataSuccess = function(response){		
	if(response != null && response.success == true) {	
		$("#userNotAirewards").populateLanguage({messageList:response.jsonLabel});
		$("#userAirewards").populateLanguage({messageList:response.jsonLabel});		
		UI_UserTabs.clearPanels();
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
	
}
UI_LMSDetails.loadLmsDetails();
