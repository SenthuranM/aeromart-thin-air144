/**
 * @author suneth
 */


$(document).ready(function(){

	createGrid();
	$("#confirmTransfer").click(function(){confirmFlightTransfer();});
	$("#modifyBooking").click(function(){modifyBooking();});
	
});

/** function to get the reservation details for the table and the flight details */
createGrid = function(){

	var data = {};
	data['pnr'] = pnr;
	data['alertId'] = alertId;
	data['pnrSegId'] = pnrSegId;
	
	$.ajax({
        url : 'selfReprotect!getReservationDetailsGridData.action',
        data: data,
        dataType:"json",
        type : "POST",		           
		complete: function(jsonData,stat){	        	  
           if(stat) {
         	  mydata = eval("("+jsonData.responseText+")");
         	  if (mydata.messageTxt == ""){

         		  bulidReservationDetailList(mydata);
         	  }else{
         		 loadSelfReprotectErrorPage(mydata);
         	  }
         	  setFlights(mydata);
           }
           $("#pageLoading").hide();
        },
       error: function(){
     	  
    	   UI_commonSystem.setErrorStatus();
     	  
       }
     });

}

/** set the flight details in radio buttons */
setFlights = function(mydata){
	
	var departureDate;
	var departureTime;
	var ArrivalDate;
	var ArrivalTime;
	var temp;
	
	for (i = 0; i < mydata.flightsList.length ; i++) {
		
		temp = mydata.flightsList[i].departureDate.split('T');
		departureDate = $.datepicker.formatDate('dd MM yy' , new Date(temp[0]));
		departureTime = temp[1].substr(0 , 5);
		
		temp = mydata.flightsList[i].arrivalDate.split('T');
		ArrivalDate = $.datepicker.formatDate('dd MM yy' , new Date(temp[0]));
		ArrivalTime = temp[1].substr(0 , 5);
		
		if(new Date() <= new Date(mydata.flightsList[i].departureDate)){
			
			$('#flights').append("<input type='radio' name='flight' value = '" + mydata.flightsList[i].fltSegId + "," + mydata.flightsList[i].departureDate + "," + mydata.flightsList[i].arrivalDate + "'/>" 
		    		+ mydata.flightsList[i].flightNumber + "&nbsp;&nbsp;&nbsp;&nbsp;" + "Departure : " + departureDate + "&nbsp;" + departureTime 
		    		+ ",&nbsp;&nbsp; Arrival : " + ArrivalDate + "&nbsp;" + ArrivalTime);
		    $('#flights').append("<br>");
		    $('#flights').append("<br>");
			
		}
	   
	}
	
	$("input:radio[name=flight][disabled=false]:first").attr('checked', true);
	
}

/** re-protect with the selected flight segment */
confirmFlightTransfer = function(){
	
	var rowObj = $("#reservationDetailsGrid").getRowData(1);
	var radioButtonVal = $("input:radio[name='flight']:checked").val();
	
	data = {};
	
	data['pnr'] = pnr;
	data['alertId'] = alertId; 
	data['pnrSegId'] = pnrSegId; 
	data['origin'] = rowObj['from'];
	data['destination'] = rowObj['to'];
	data['departureDate'] = radioButtonVal.split(',')[1];
	data['fltSegId'] = radioButtonVal.split(',')[0];
	data['arrivalDate'] = radioButtonVal.split(',')[2];
	
	
	jConfirm('You can transfer only one time. <br> Are you sure you want to transfer to selected flight?' , 'Are you sure?', function(r) {
	    if(r){
	    	$.ajax({
	            url : 'selfReprotect.action',
	            data:data,
	            dataType:"json",
	            beforeSend :function(){$("#loadingMessage").append("<c:import url=\"../common/pageLoading.jsp\" />")},
	            type : "POST",
	            complete: function(jsonData,stat){	        	  
	                if(stat) {
	                	mydata = eval("("+jsonData.responseText+")");
	                	
	                	if(mydata.messageTxt == ""){
	                		var stringDateArr = (mydata.departureDate).split("/");
	                		var departureDate = new Date(stringDateArr[2], (stringDateArr[1] -1),stringDateArr[0]);
	                		data = {};
	                		data['pnr'] = mydata.pnr;
	                		data['dateOfDep'] = departureDate;
	                		data['lastName'] = mydata.contactName;
	                		
	                		$.ajax({
	                	        url : 'manageBookingV2.action',
	                	        data:data,
	                	        dataType:"json",
	                	        type : "POST",
	                	        success: function(response){
	                	        	if(response.success){
	                	        		UI_commonSystem.loadingProgress();
		                				$("#groupPNR").val(response.groupPNR);
		                				$("#airlineCode").val(response.airlineCode);
		                				$("#marketingAirlineCode").val(response.marketingAirlineCode);
		                				$("#pnr").val(response.pnr);
		                				$("#isTransferPage").val("true");
		                				$("#loadReservationForm").attr('action','showCustomerLoadPage!loadCustomerHomePage.action');
		                				$("#loadReservationForm").submit();	                	        		
	                	        	}
	                	        }});
	                		
	                	}else if(mydata.messageTxt == "error"){          		
	                		loadSelfReprotectErrorPage(mydata);
	                	}else{
	                		loadSelfReprotectErrorPage(mydata);
	                	}
	                }else{
	                	
	                	loadSelfReprotectErrorPage(mydata);
	                }
	                $("#pageLoading").hide();
	             },
	            error: function(){
	          	  
	            	loadSelfReprotectErrorPage(mydata);
	          	  
	            }
	         });
	    }
	});
 		
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}   

/** Populate the reservation details into a table */
bulidReservationDetailList = function(data) {		
	var strHTMLText = '';
	$("#trResDetailList").find("#resDetailTable").html("");
	var intLength = data.reservationDetailsList.length;

	if (intLength > 0){
		
		var strColor = "SeperateorBGColor";
		
		for (var i = 0 ; i < intLength ; i++){

			if (strColor == "SeperateorBGColor"){
				strColor = "";
			}else{
				strColor = "SeperateorBGColor";
			}

		    strHTMLText = '<tr>';
				

			strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center"">';
			strHTMLText += '		<label>' + data.reservationDetailsList[i].reservationDetailsDTO.pnr + '<\/label>';
			strHTMLText += '	<\/td>';

			strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center">';
			strHTMLText += '		<label>' + data.reservationDetailsList[i].reservationDetailsDTO.paxName + '<\/label>';
			strHTMLText += '	<\/td>';
				
			strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
			strHTMLText += '		<label>' + data.reservationDetailsList[i].reservationDetailsDTO.from +'<\/label>';
			strHTMLText += '	<\/td>';
			
			strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
			strHTMLText += '		<label>' + data.reservationDetailsList[i].reservationDetailsDTO.to +'<\/label>';
			strHTMLText += '	<\/td>';
			
			strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
			strHTMLText += '		<label>'	 + dateFormatter(data.reservationDetailsList[i].reservationDetailsDTO.departureDateTime) +'<\/label>';
			strHTMLText += '	<\/td>';
			
			if(i == 0){
				$("#depDate").val(dateFormatter(data.reservationDetailsList[i].reservationDetailsDTO.departureDateTime));
			}
			
			strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
			strHTMLText += '		<label>' + dateFormatter(data.reservationDetailsList[i].reservationDetailsDTO.arrivalDateTime) +'<\/label>';
			strHTMLText += '	<\/td>';
			
			strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
			strHTMLText += '		<label>' + data.reservationDetailsList[i].reservationDetailsDTO.status +'<\/label>';
			strHTMLText += '	<\/td>';
			
			strHTMLText += '<\/tr>';
				
			$("#trResDetailList").find("#resDetailTable").append(strHTMLText);

		}
			
	} else {
		$("#trResDetailList").hide();
	}	    
}

/** Format the date as wanted */
dateFormatter = function(val){
	var treturn = "&nbsp;";
	if (val!=null){
		treturn = $.datepicker.formatDate('dd-M-yy', new Date(val.split('T')[0]));
		treturn = treturn + "&nbsp;&nbsp;&nbsp;" + String(val.split('T')[1]).substr(0 , 5);
	}
	return treturn;
}

/** Load the modify booking page for the loaded pnr */
modifyBooking = function(){
	
	$("#pageLoading").show();
	
	var depDate = $.datepicker.formatDate('dd/MM/yy', new Date(($("#depDate").val()).substr(0 , 11)));
	var stringDateArr = (depDate).split("/");
	var departureDate = new Date(stringDateArr[2], (stringDateArr[1] -1),stringDateArr[0]);
	data = {};
	data['pnr'] = pnr;
	data['dateOfDep'] = departureDate;
	data['lastName'] = mydata.contactName;
	
	$.ajax({
        url : 'manageBookingV2.action',
        data:data,
        dataType:"json",
        type : "POST",
        success: function(response){
        	if(response.success){
        		UI_commonSystem.loadingProgress();
				$("#groupPNR").val(response.groupPNR);
				$("#airlineCode").val(response.airlineCode);
				$("#marketingAirlineCode").val(response.marketingAirlineCode);
				$("#pnr").val(response.pnr);
				$("#isTransferPage").val("true");
				$("#loadReservationForm").attr('action','showCustomerLoadPage!loadCustomerHomePage.action');
				$("#loadReservationForm").submit();	                	        		
        	}else{
        		loadSelfReprotectErrorPage(response);        		
        	}
        }});
}

/** Load Error Page for Self Re-protect page */
loadSelfReprotectErrorPage = function(response){	
	window.location.href="showLoadPage!loadSelfReprotectError.action?messageTxt="+response.messageTxt;
};
