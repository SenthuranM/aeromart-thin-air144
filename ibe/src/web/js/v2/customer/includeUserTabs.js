/**
 *
 * Register user tabs , manage booking and other loading functionality
 * Author-Pradeep Karunanayake
 */
function UI_UserTabs(){}
/**
 * Tab pannels
 */
UI_UserTabs.tabPanel = {ids:["reservationDetail","reservationList","reservationCredit","updateProfile","modifySegment","travelHistory", "balanceSummary","addBusSegment","lmsDetail"] };
/**
 * Tabs
 */
UI_UserTabs.tab = {ids:["tdReservationList","tdReservationCredit","tdTravelHistory","tdUpdateProfile","tdLogout","tdLms"] };
/**
 * Page load status
 */
UI_UserTabs.tabPageLoad = {reservationDetail:false,reservationList:false,reservationCredit:false,travelHistory:false,updateProfile:false,modifySegment:false, balanceSummary:false,addBusSegment:false,lmsDetail:false};
/**
 * Loading Tab Data-Avoid Fast Tab Moving Panel Display in Same page
 */
UI_UserTabs.tabData = {id:"",panel:""};

UI_UserTabs.tabTimeOut = "";

UI_UserTabs.emailResendOnce = true;

UI_UserTabs.activateLms = false;

/*$(document).ready(function() {
	UI_UserTabs.ready();
}); */
/**
 * Tabs ready
 */
UI_UserTabs.ready = function() {
	
	try{ 
		UI_UserTabs.loadSocialThirdPartyScripts();
	} catch(e){};
	$("#tdReservationList").click(function(){UI_UserTabs.myReservationsClick();UI_UserTabs.activateLms = false;});
    $("#tdReservationCredit").click(function(){UI_UserTabs.creditClick();});
    $("#tdTravelHistory").click(function(){UI_UserTabs.travelHistoryClick();UI_UserTabs.activateLms = false;});
    $("#tdUpdateProfile").click(function(){UI_UserTabs.updateProfileClick();UI_UserTabs.activateLms = false;});
    $("#tdLogout").click(function(){UI_UserTabs.logoutClick();UI_UserTabs.activateLms = false;});
    $("#linkLoyalty").click(function(){UI_UserTabs.loyaltyLinkClick();UI_UserTabs.activateLms = false;});
    
    $("#tdLms").on("click", UI_UserTabs.lmsClick);
    $('td.cursorPointer').mouseover(function(){
    	if (!$(this).hasClass("tabSltdUser"))
			$(this).addClass("tabSltdOver");
	});
    
	$('td.cursorPointer').mouseout(function(){
		if ($(this).hasClass("tabSltdOver"))
			$(this).removeClass("tabSltdOver");
	});
    
    var target = $('#tabTarget').val();
    if(target !=null && $.trim(target) != ''){
    	if(target == 'tdLogout'){
    		if(UI_UserTabs.facebookLoginEnabled || UI_UserTabs.linkedInLoginEnabled){
    			setTimeout("UI_UserTabs.systemLogOut()", 750); //time to loading of external scripts
    		}
    		UI_UserTabs.systemLogOut();
    		
    	}else{
    		//$('#'+target).click();
    		 UI_UserTabs.myReservationsClick();
    	}    	
    }else{    	
    	 UI_UserTabs.myReservationsClick();
    }
    
    $("#txtDOBLMS").datepicker({
		regional: SYS_IBECommonParam.locale ,
		dateFormat: "dd/mm/yy",
		showOn: 'both',
		maxDate:'-2Y',
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
	});
    
    UI_UserTabs.showLmsStatus();
	UI_commonSystem.showLmsInBar();

    $("#lblLmsResend").click(function(){UI_UserTabs.lmsMemberCheck();});
    
    if(!UI_commonSystem.loyaltyManagmentEnabled){
    	$("#tdLms").hide();
    	$("#lmsSeperater").hide();
	    $("#tdReservationCredit").attr("width" , "20%");
	    $("#tdReservationList").attr("width" , "20%");
	 	$("#tdTravelHistory").attr("width" , "20%");
	    $("#tdLogout").attr("width" , "20%");
	    $("#tdUpdateProfile").attr("width" , "20%");
	    $("#RewardsPanel-1").hide();
    }
    if(GLOBALS!= undefined && GLOBALS.loyaltyEnable != undefined && !GLOBALS.loyaltyEnable){
		$("#RewardsPanel-2").hide();
	}
    
}

UI_UserTabs.loadSocialThirdPartyScripts = function () {
	UI_UserTabs.facebookLoginEnabled = false;
	UI_UserTabs.linkedInLoginEnabled = false;
    if(showFacebookLogin != "" && showFacebookLogin =="true"){
    	UI_UserTabs.facebookLoginEnabled =true;
    	
 			(function(dt){
            		var jst, idt = 'facebook-jssdk'; 
            		if (dt.getElementById(idt)){
            			return;
            		}
             	jst = dt.createElement('script'); 
             	jst.id = idt; 
             	jst.async = true;
             	jst.src = "//connect.facebook.net/en_US/all.js";
             	dt.getElementsByTagName('head')[0].appendChild(jst);
        		}(document));
 			
    	 window.fbAsyncInit = function (){
    			FB.init({
    				appId : socialLoginFBAppId,
    				status : true, 
    				cookie : true, 
    				xfbml : true
    			});
    	 }
    }
    
    if(showLinkedInLogin != "" && showLinkedInLogin =="true"){
	  	var e = document.createElement('script');
	  	e.type = 'text/javascript';
	  	e.async = true;
	  	e.src = document.location.protocol +'//platform.linkedin.com/in.js?async=true';
	  	e.onload = function() {
	  		IN.init({
	  			api_key : socialLoginLinkedInAppId,
	  			authorize : true,
	  		    onLoad :'UI_UserTabs.linkdinloadSuccess'
	  			});
	  		};
	  	
	  	var s = document.getElementsByTagName('script')[0];
	  	s.parentNode.insertBefore(e, s);
    }
}
/**
 * Update Profile tab
 */
UI_UserTabs.updateProfileClick = function() {
	UI_UserTabs.tabData = {id:"tdUpdateProfile",panel:"updateProfile"};
	UI_UserTabs.tabChanges(UI_UserTabs.tabData);
	if (UI_UserTabs.tabPageLoad.updateProfile == false) {
		UI_commonSystem.loadingProgress();
		$("#sidePannelAll").removeClass("reg-user-flow");
		$("#updateProfile").load("showCustomerLoadPage!loadCustomerProfile.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
			UI_UserTabs.loadScript("../js/v2/customer/customerProfileUpdate.js?randomNumber="+UI_UserTabs.getRandomNumber());
			UI_UserTabs.loadScript("../js/v2/customer/customerValidations.js?randomNumber="+UI_UserTabs.getRandomNumber());
		});
		UI_UserTabs.setPageLoadStatus({tabPanel:"updateProfile"});
	} else {
		UI_ProfileUpdate.loadCustomerProfilePageData();
	}
}

UI_UserTabs.showLmsStatus = function (){

	var htmStr  = '<table id="tr-LMS-LoggedIn" width="100%" cellpadding="0" cellspacing="0" border="0">';
    htmStr  += '<tr><td colspan="3" class="rowGap"></td></tr>';
    if(UI_commonSystem.loyaltyManagmentEnabled){
	    htmStr  += '<tr><td colspan="3" class="paddingL5"><img src="../images/air-rewards_no_cache.png" alt="Airewards" height="25" style="margin-bottom: 5px"></td></tr>';
	    if(UI_commonSystem.lmsDetails != null && UI_commonSystem.lmsDetails.emailId != null
	    		&& UI_commonSystem.lmsDetails.emailId != ""){
	        if(UI_commonSystem.lmsDetails.emailStatus=="Y"){
		    	htmStr  += '<tr><td class="paddingL5" ><label id="lblAirewardsID"	class="fntBold">Email ID</label></td><td><label> : </label>'
		        	+'<label id="lblAirewardsIDValue" class="hdFontColor fntBold"></label></td></tr>'
		        	+'<tr><td class="paddingL5" ><label id="lblAirewardsPointAvaialble"	class="fntBold">Available Loyalty Points </label></td><td><label> : </label>'
		        	+'<label id="lblAirewardsPointAvaialbleValue" class="hdFontColor fntBold fontCapitalize"></label></td></tr>';
	        }else if(UI_commonSystem.lmsDetails.emailStatus=="N"){
		    	htmStr  += '<tr><td class="paddingL5" colspan="3"><label id="lblAirRewardsMailConf" class="hdFontColor fontCapitalize">Confirmation Email Sent<br/>'
				+'Click the Confirmation Link to Confirm</label></td></tr>'
				+'<tr><td class="paddingL5" colspan="3"><label id="lblLmsResend" class="paddingL5" style="cursor: pointer;">Resend Airewards Confirmation Email</label></td></tr>';
		    }
	
	    }

    }
    
    if($("#spnCerdit").html()==""||$("#spnCerdit").html()==undefined){
	    htmStr  +='<tr id="tr-credits-LoggedIn"><td class="paddingL5"><label id="lblResCredit" class="fntBold"></label></td><td><label> : </label>'
	    +'<label id="lblTxtCredit" class="hdFontColor fntBold fontCapitalize"><span id="spnCerdit"></span></label></td></tr>'
	    +'<tr><td colspan="3" class="rowSingleGap"></td></tr></table>';
    }
    else{
    	var spanHtml = $("#spnCerdit").html();
    	var creditTxt = $("#lblResCredit").html();
    	htmStr  +='<tr id="tr-credits-LoggedIn"><td class="paddingL5"><label id="lblResCredit" class="fntBold">'+creditTxt+'</label></td><td><label> : </label>'
    	    +'<label id="lblTxtCredit" class="hdFontColor fntBold fontCapitalize"><span id="spnCerdit">'+spanHtml+'</span></label></td></tr>'
    	    +'<tr><td colspan="3" class="rowSingleGap"></td></tr></table>';    	
    }
    $("#lms").html(htmStr);
}
/**
 * Airewards tab
 */
UI_UserTabs.lmsClick = function() {
    UI_UserTabs.tabData = {id:"tdLms",panel:"lmsDetail"};
    UI_UserTabs.tabChanges(UI_UserTabs.tabData);
    if (UI_UserTabs.tabPageLoad.lmsDetail == false) {
        UI_commonSystem.loadingProgress();
        $("#sidePannelAll").removeClass("reg-user-flow");
        $("#lmsDetail").load("showCustomerLoadPage!loadCustomerLms.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
            UI_UserTabs.loadScript("../js/v2/customer/lmsDetails.js?randomNumber="+UI_UserTabs.getRandomNumber());
            
            $("#earnPtsLnk").click(function(e){UI_UserTabs.earnPtsLinkClick(e);});
		    $("#redeemPtsLnk").click(function(e){UI_UserTabs.redeemPtsLinkClick(e);});
		    $("#linkLnk").click(function(e){UI_UserTabs.claimPtsLinkClick(e);});
        });
        UI_UserTabs.setPageLoadStatus({tabPanel:"lmsDetail"});
    } else {
        UI_LMSDetails.loadLmsDetails();
    }
}
/**
 * History tab
 */
UI_UserTabs.travelHistoryClick = function() {
	UI_UserTabs.tabData = {id:"tdTravelHistory",panel:"travelHistory"};
	UI_UserTabs.tabChanges(UI_UserTabs.tabData);
	if (UI_UserTabs.tabPageLoad.travelHistory == false) {
		UI_commonSystem.loadingProgress();
		$("#sidePannelAll").removeClass("reg-user-flow");
		$("#travelHistory").load("showCustomerLoadPage!loadReservationHistory.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
			UI_UserTabs.loadScript("../js/v2/customer/customerReservationHistoryV2.js?randomNumber="+UI_UserTabs.getRandomNumber());
		});
		UI_UserTabs.setPageLoadStatus({tabPanel:"travelHistory"});
	} else {
		UI_ReservationHistory.reservationHistoryPageData();
	}
}
/**
 * Credit tab
 */
UI_UserTabs.creditClick = function() {	
	UI_UserTabs.tabData = {id:"tdReservationCredit",panel:"reservationCredit"};
	UI_UserTabs.tabChanges(UI_UserTabs.tabData);
	if (UI_UserTabs.tabPageLoad.reservationCredit == false) {
		UI_commonSystem.loadingProgress();
		$("#sidePannelAll").removeClass("reg-user-flow");
		$("#reservationCredit").load("showCustomerLoadPage!loadReservationCredit.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
			UI_UserTabs.loadScript("../js/v2/customer/customerReservationCredit.js?randomNumber="+UI_UserTabs.getRandomNumber());
		});
		UI_UserTabs.setPageLoadStatus({tabPanel:"reservationCredit"});
	} else {
		UI_ReservationCredit.reservationCreditPageData();
	}
}

/**
 * Reservation tab click
 */
UI_UserTabs.myReservationsClick = function() {	
	UI_UserTabs.tabData = {id:"tdReservationList",panel:"reservationList"};
	var pnr = $("#hdnPNR").val();
	if (SYS_IBECommonParam.regCustomer == "true" && pnr == "") {	
		UI_UserTabs.tabChanges(UI_UserTabs.tabData)
		if (UI_UserTabs.tabPageLoad.reservationList == false) {	
			UI_commonSystem.loadingProgress();
			$("#reservationList").load("showCustomerLoadPage!loadReservationList.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
				UI_UserTabs.loadScript("../js/v2/customer/reservationList.js?randomNumber="+UI_UserTabs.getRandomNumber());
			});
			UI_UserTabs.setPageLoadStatus({tabPanel:"reservationList"});	
		} else {
			UI_ReservationList.loadPageData();
		}
	} else {			
		if (pnr != "") { 
			UI_CustomerHome.pnr = $("#hdnPNR").val();
			UI_CustomerHome.groupPnr = $("#groupPNR").val();
			UI_CustomerHome.marketingAirlineCode = $("#marketingAirlineCode").val();
			UI_CustomerHome.airlineCode = $("#airlineCode").val();
			$("#hdnPNR").val("");
			UI_UserTabs.pnrClick();
		}
	}
	$(".reg-user-flow").show();
	$("#sidePannelAll").removeClass("reg-user-flow");
}

UI_UserTabs.linkdinloadSuccess = function (){
	UI_UserTabs.linkedInLoginEnabled = true;
}
/**
 * Logout Click
 */
UI_UserTabs.logoutClick = function() {	
	jConfirm(UI_CustomerHome.strLogoutMsg, 'Confirm Logout', function(action){ 
		if (action) {
			UI_UserTabs.systemLogOut(); 
		}
	});	
}

UI_UserTabs.systemLogOut = function (){
	try{
		UI_UserTabs.socialSiteLogout();
	}catch (e) {
	}
	setTimeout("UI_UserTabs.submitLogout()", 500);
}

/**
 * Logout from social logout
 */
UI_UserTabs.socialSiteLogout = function (){
	
	if(UI_UserTabs.facebookLoginEnabled){
	  FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			  FB.logout(function(response) {
		    		//
		    }); 
		} 
	 });
   }
   if(UI_UserTabs.linkedInLoginEnabled){
		IN.User.logout();
	}
}

UI_UserTabs.submitLogout = function(){
	var tabData = {id:"tdLogout"};
	$("#divLoadBg").readonly(true);
	UI_UserTabs.tabChanges(tabData);
	UI_commonSystem.loadingProgress();
	$.post("customerLogout.action","", UI_UserTabs.logoutProcess, "json");
}
/**
 * Logout Process
 */

UI_UserTabs.logoutProcess = function(response) {
	if(response != null && response.success == true) {
		$("#regCustomer").val(false);
		 SYS_IBECommonParam.regCustomer = false;
		if ($("#kioskAccessPoint").val() == "true") {
			SYS_IBECommonParam.homeClick();
		} else {
			UI_Top.holder().location.replace(UI_Top.holder().GLOBALS.airLineURL);
		}
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	//UI_commonSystem.loadingCompleted();
}

/**
 * Set tab page loaded status
 */
UI_UserTabs.setPageLoadStatus = function(data) {
	UI_UserTabs.tabPageLoad[data.tabPanel] = true;
	if ((data.tabPanel == "reservationList" || data.tabPanel == "travelHistory" ) && $.browser.msie){
		setTimeout("UI_commonSystem.loadingCompleted()", 2000);
	}
}
/**
 * Hide  panel of tab
 */
UI_UserTabs.hidePanels = function() {
	for (var i = 0; i < UI_UserTabs.tabPanel.ids.length; i++) {
		$("#"+UI_UserTabs.tabPanel.ids[i]).hide();
	}
}
/**
 * Unselect tab
 */
// To Do: improve
UI_UserTabs.unselectTab = function() {
	for (var i = 0; i < UI_UserTabs.tab.ids.length; i++) {
		$("#"+UI_UserTabs.tab.ids[i]).removeClass("tabSltdUser").addClass("tabUnSltd");
		$("#"+UI_UserTabs.tab.ids[i] + " label").removeClass("fntWhite").addClass("fntBlack");	
	}
}
/**
 * Change tab status
 */
UI_UserTabs.tabChanges = function(tabData) {
	UI_CustomerHome.pageInit();
	$("#spnMemTabN").readonly(true);	
	UI_UserTabs.hidePanels();	
	UI_UserTabs.unselectTab();	
	$("#"+tabData.id).removeClass("tabUnSltd").addClass("tabSltdUser");	
	$("#"+tabData.id + " label").removeClass("fntBlack").addClass("fntWhite");	
	$("#"+tabData.panel).slideDown();
	UI_UserTabs.tabTimeOut = setTimeout('UI_UserTabs.enableTabs()', 250);		
}

UI_UserTabs.enableTabs = function() {
	$("#spnMemTabN").readonly(false);
	clearTimeout(UI_UserTabs.tabTimeOut);
}

/**
 * Avoid Fast moving- jQuery Display Delay
 */
UI_UserTabs.clearPanels = function() {
	for (var i = 0; i < UI_UserTabs.tabPanel.ids.length; i++) {
		if (UI_UserTabs.tabData.panel != UI_UserTabs.tabPanel.ids[i]) {
			$("#"+UI_UserTabs.tabPanel.ids[i]).hide();
		}
	}
}

/**
 * Reservation Details
 */
UI_UserTabs.pnrClick = function() {	
	var tabData = {id:"tdReservationList",panel:"reservationDetail"};
	UI_UserTabs.tabChanges(tabData)
//	if (UI_UserTabs.tabPageLoad.reservationDetail == false) {
		UI_commonSystem.loadingProgress();
		$("#reservationDetail").load("showCustomerLoadPage!loadCustomerReservationDetail.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
			UI_UserTabs.loadScript("../js/v2/customer/reservationDetail.js?randomNumber="+UI_UserTabs.getRandomNumber());
			$("#sidePannelAll").addClass("reg-user-flow");
		});
		UI_UserTabs.setPageLoadStatus({tabPanel:"reservationDetail"});	
//	} else {		
//			UI_CustReservation.loadReservationData();		
//	}
}

/**
 * Customer Modify Segment
 */
UI_UserTabs.customerModifySegmentClick = function() {	
	
	if(UI_CustReservation.isRequoteFlow && UI_UserTabs.hasCanceledFlight()){
		jAlert('Unable to process, canceled flight segment found!');
		return false;
	}
	
	var tabData = {id:"tdReservationList",panel:"modifySegment"};
	UI_UserTabs.tabChanges(tabData)
	if (UI_UserTabs.tabPageLoad.modifySegment == false) {
		UI_commonSystem.loadingProgress();
		$("#modifySegment").load("showCustomerLoadPage!loadCustomerModifySegment.action?randomNumber='+UI_UserTabs.getRandomNumber()",function() {
			UI_UserTabs.loadScript("../js/v2/modifyRes/modifySegment.js?randomNumber="+UI_UserTabs.getRandomNumber());
			$('#modifySegment').show();
		});
		UI_UserTabs.setPageLoadStatus({tabPanel:"modifySegment"});	
	} else {		
		UI_CustomerModifySegment.pageBuild();
	}
}

/**
 * Customer Add Segment 
 * Caters for Add Segment/Add bus segment both
 */
UI_UserTabs.customerAddBusSegmentClick = function() {	
	
	var tabData = {id:"tdReservationList",panel:"addBusSegment"};
	UI_UserTabs.tabChanges(tabData)
	UI_commonSystem.loadingProgress();
	$("#addBusSegment").load("showCustomerLoadPage!loadCustomerAddBusSegment.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
		UI_UserTabs.loadScript("../js/v2/modifyRes/addBusSegment.js?randomNumber="+UI_UserTabs.getRandomNumber());
	});
	UI_UserTabs.setPageLoadStatus({tabPanel:"addBusSegment"});	
}


/**
 * Cancel Reservation 
 */
UI_UserTabs.cancelReservationClick = function() {
	var tabData = {id:"tdReservationList",panel:"balanceSummary"};
	UI_UserTabs.tabChanges(tabData)
	if (UI_UserTabs.tabPageLoad.balanceSummary == false) {
		UI_commonSystem.loadingProgress();
		$("#balanceSummary").load("showCustomerLoadPage!loadBalanceSummary.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
			UI_UserTabs.loadScript("../js/v2/modifyRes/cancelBalanceSummary.js?randomNumber="+UI_UserTabs.getRandomNumber());
		});
		UI_UserTabs.setPageLoadStatus({tabPanel:"balanceSummary"});	
	} else {
		UI_BalanceSummary.loadCancelSummaryData();
	}
}

UI_UserTabs.nameChangeClick = function(paxData) {
	
	$("#nameChangeRequote").val(true);
	$("#nameChangePaxData").val($.toJSON(paxData));
	UI_CustReservation.isRequoteNameChange=true;
	
	var flightRefNos = [];
	
	$.each(UI_CustReservation.flightDepDetails,function(index,value){
		if(value.status != "CNX"){
			flightRefNos.push(value.flightSegmentRefNumber);
		}
	});
	
	UI_CustReservation.flightRefNos = flightRefNos.join(":");
	
	var tabData = {id:"tdReservationList",panel:"modifySegment"};
	
	UI_UserTabs.tabChanges(tabData)
	UI_commonSystem.loadingProgress();
	
	$("#modifySegment").load("showCustomerLoadPage!loadCustomerModifySegment.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
		UI_UserTabs.loadScript("../js/v2/modifyRes/modifySegment.js?randomNumber="+UI_UserTabs.getRandomNumber());
	});
	
	UI_UserTabs.setPageLoadStatus({tabPanel:"modifySegment"});
}

/**
 * Loyalty Link Click
 */
UI_UserTabs.loyaltyLinkClick = function() {			
	$("#frmCustomerTab").attr('action','showCustomerLoadPage!loadActivateLoyaltyAccount.action');
	$("#frmCustomerTab").submit();
}


/**
 * "Air Rewards" tab Link Clicks.
 *  Buggy in IE
 */
UI_UserTabs.earnPtsLinkClick = function(e){
	e.preventDefault();
	window.open($("#earnPtsLnk").attr('href'), '_blank');
}

UI_UserTabs.redeemPtsLinkClick = function(e){
	e.preventDefault();
	window.open($("#redeemPtsLnk").attr('href'), '_blank');
}

UI_UserTabs.claimPtsLinkClick = function(e){
	e.preventDefault();
	window.open($("#linkLnk").attr('href'), '_blank');
}


/**
 * Utility method for generate random number
 */
UI_UserTabs.getRandomNumber = function() {
	return Math.floor(Math.random()*1000);
}

UI_UserTabs.cancelSegmentClick = function() {

	if(UI_CustReservation.isRequoteFlow){
		var data = new Array();
		data['pnrSegId'] = UI_CustReservation.flightRefNos;
		var fareId = 0;
		for (var i = 0; i < UI_CustReservation.allReservationSegments.length; i++) {
			if (UI_CustReservation.flightRefNos == UI_CustReservation.allReservationSegments[i].bookingFlightSegmentRefNumber) {
				fareId = UI_CustReservation.allReservationSegments[i].fareTO.fareId;
				break;
			}
		}
		data['fareId'] = fareId;
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, 
				url:'cancelSegmentDetail.action', async:false,
				success:
	            function (response){
					if (response.cancelSegmentWithoutRequote) {
						UI_CustReservation.isRequoteFlow = false;
						var tabData = {id:"tdReservationList",panel:"balanceSummary"};
						UI_UserTabs.tabChanges(tabData)
						if (UI_UserTabs.tabPageLoad.balanceSummary == false) {
							UI_commonSystem.loadingProgress();
							$("#balanceSummary").load("showCustomerLoadPage!loadBalanceSummary.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
								UI_UserTabs.loadScript("../js/v2/modifyRes/cancelBalanceSummary.js?randomNumber="+UI_UserTabs.getRandomNumber());
							});
							UI_UserTabs.setPageLoadStatus({tabPanel:"balanceSummary"});	
						} else {
							UI_BalanceSummary.loadCancelSummaryData();
						}
					} else {
						if(UI_UserTabs.hasCanceledFlight()){
							jAlert('Unable to process, canceled flight segment found!');
						}else{
							UI_CustReservation.isRequoteCancel=true;
							UI_UserTabs.requoteCancelSegmentClick();
						}
					}
	            }});
	}else{
		var tabData = {id:"tdReservationList",panel:"balanceSummary"};
		UI_UserTabs.tabChanges(tabData)
		if (UI_UserTabs.tabPageLoad.balanceSummary == false) {
			UI_commonSystem.loadingProgress();
			$("#balanceSummary").load("showCustomerLoadPage!loadBalanceSummary.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
				UI_UserTabs.loadScript("../js/v2/modifyRes/cancelBalanceSummary.js?randomNumber="+UI_UserTabs.getRandomNumber());
			});
			UI_UserTabs.setPageLoadStatus({tabPanel:"balanceSummary"});	
		} else {
			UI_BalanceSummary.loadCancelSummaryData();
		}
	}	
	
}

UI_UserTabs.hasCanceledFlight=function(){
	fltSegList = UI_CustReservation.flightDepDetails;
	for ( var i = 0; i < fltSegList.length; i++) {
		if (fltSegList[i].displayStatus=='UN') {
			return true;
		}
	}
	return false;
}

UI_UserTabs.loadScript = function (path) {
    var loadIT = function(path){
        var result = $.Deferred(),
            script = document.createElement("script");
        script.async = "async";
        script.type = "text/javascript";
        script.src = path;
        script.id = "dynamicScript";
        script.onload = script.onreadystatechange = function(_, isAbort) {
            if (!script.readyState || /loaded|complete/.test(script.readyState)) {
                if (isAbort){
                    result.reject();
                }else{
                    result.resolve();
                }
            }
        };
        script.onerror = function () { result.reject(); };
        $("head")[0].appendChild(script);
        return result.promise();
    }
    if ($("#dynamicScript").length>0){
        $("#dynamicScript").remove();
    }
  //  if (isInDevMode==="true"){ // by sashrika
    	return loadIT(path);
   // }
}

UI_UserTabs.requoteCancelSegmentClick = function(){
	$("#cancelSegmentRequote").val(true);	
	var tabData = {id:"tdReservationList",panel:"modifySegment"};
	UI_UserTabs.tabChanges(tabData)
	UI_commonSystem.loadingProgress();
	$("#modifySegment").load("showCustomerLoadPage!loadCustomerModifySegment.action?randomNumber="+UI_UserTabs.getRandomNumber(),function() {
		UI_UserTabs.loadScript("../js/v2/modifyRes/modifySegment.js?randomNumber="+UI_UserTabs.getRandomNumber());
	});
	UI_UserTabs.setPageLoadStatus({tabPanel:"modifySegment"});	
}

UI_UserTabs.lmsMemberCheck = function(){
	var data =  {};
	data['ffid'] = UI_CustomerHome.regEmail;
	var url = "lmsMemberConfirmation!ffidCheck.action";
	var response = $.ajax({
		type: "POST",
		asynch: false,
		url:url,
		data : data,
		success: function(response){
			if(response.ffidAvailability){
				var mergeMsg = "<div>The account with this Airewards ID already exists. If you want to merge please click Ok.</div>";
				var accountMege = "Mege Account";
				if(response.nameMatch){
					jConfirm(mergeMsg, accountMege, function(eventResponse) {
						if (eventResponse){					
							UI_UserTabs.resendLmsConfirmation();
						} else {
						
						}
					});
				} else {
					jAlert('This Airewards account already exists with a different first name and last name.');
					return;
				}
			} else {
				UI_UserTabs.resendLmsConfirmation();
			}
		},
		error:UI_commonSystem.setErrorStatus
	});
}

UI_UserTabs.resendLmsConfirmation = function(){
	var url = "customerRegister!sendlmsConfEmail.action";
	var response = $.ajax({
		type: "POST",
		asynch: false,
		url:url,		
		success: UI_UserTabs.resendLmsConfirmationSuccess,
		error:UI_commonSystem.setErrorStatus
	});
}

UI_UserTabs.resendLmsConfirmationSuccess = function(response){
	var msg = "Confirmation email has been resent successfully";
	
	if(UI_UserTabs.emailResendOnce){
		var lmsMsg = $("<div><label>"+msg+"</label></div>")
			.css({
				"background-color":"#ffffdd",
				"padding":"10px",
				"position":"relative",
				"text-align":"center",
				"margin":"0 auto",
				"width":"60%"
			})
			.addClass("ui-corner-all");
			$("#lmsResend").prepend(lmsMsg);
			var tm= setTimeout(function(){
				lmsMsg
				.slideUp()
				.fadeOut(function(){
					lmsMsg.remove();
				})
			},8000);
			UI_UserTabs.emailResendOnce = false;
	}
}