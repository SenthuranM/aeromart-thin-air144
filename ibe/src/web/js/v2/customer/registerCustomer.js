/**
 * Author : Pradeep Karunanayake
 * 
 */
function UI_RegCustomer(){} 

var arrPaxTitle = new Array();
var errorInfo  = "";
var arrCountryPhone = new Array();
var arrAreaPhone  = new Array();
var userRegConfig = "";
var validationGroup = "";
var integrateMashreqWithLMSEnabled = false;
var isMashreqLoyaltyEnables = false;

$(document).ready(function() {
	UI_RegCustomer.ready();
}); 
/**
 * Page Ready
 *
 */

UI_RegCustomer.isBecomeLmsUser = false;

UI_RegCustomer.headFfidReq = false;

UI_RegCustomer.ready = function() {
	$("#divLoadBg").hide();
	$("#trMsg").hide();	
	$("#btnRegister").click(function(){UI_RegCustomer.registerCustomer();});
	$("#btnReset").click(function(){UI_RegCustomer.resetButtonClick();});	
	$("#btnPrevious").click(function(){SYS_IBECommonParam.homeClick();});
	$("#btnSignIN").click(function(){UI_RegCustomer.loadSignIn();});	
	$("#imgCalendar").hide();
	
	var now = new Date();
	var yearRange = (now.getFullYear() - 100) + ":" + now.getFullYear();
	
	$("#txtDateOfBirth").datepicker({regional: SYS_IBECommonParam.locale ,
		dateFormat: "dd/mm/yy",
		maxDate: +0,
		showOn: 'both',
		yearRange: yearRange,
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
	});
	
	$("#selCountry").change(function(){Util_CustomerValidation.countryListChange()});
	$("#txtDateOfBirth").blur(function(){Util_CustomerValidation.validateDOB(this);});
	$("#txtPCountry").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtPArea").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtTelephone").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtMCountry").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtMArea").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtMobile").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtOCountry").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtOArea").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtOfficeTel").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtFCountry").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtFArea").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});	
	$("#txFax").keydown(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtEmgnPCountry").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtEmgnPArea").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtEmgnTelephone").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});

	$("#txtPCountry").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtPArea").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtTelephone").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtMCountry").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtMArea").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtMobile").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtOCountry").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtOArea").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtOfficeTel").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtFCountry").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtFArea").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txFax").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtEmgnPCountry").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtEmgnPArea").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});
	$("#txtEmgnTelephone").keyup(function(){Util_CustomerValidation.telephoneNoKeyPress(this);});

	
	$("input[name='optLMS']").change(function(){
		UI_RegCustomer.lmsCheckChange();
	});
	
	$("input[name='optCardAcc']").change(function(){
		UI_RegCustomer.loyaltyCheckChange();
	});
	
		
	$("#txtEmail").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnEmailLMS").val(this.value);
	});
		
	$("#txtFirstName").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnFirstNameLMS").val(this.value);
	});
		
	$("#txtLastName").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnLastNameLMS").val(this.value);
	});
		
	$("#txtMCountry").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)	
			$("#hdnMCountryLMS").val(this.value);
	});
		
	$("#txtMArea").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnMAreaLMS").val(this.value);
	});
		
	$("#txtMobile").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnMobileLMS").val(this.value);
	});
	
	$("#selNationality").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnNationalityCodeLMS").val(this.value);
	});
	
	$("#selCountry").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnResidencyCodeLMS").val(this.value);
	});
	
	$("#selTitle").change(function(event){
		UI_commonSystem.pageOnChange();
		if (UI_RegCustomer.isBecomeLmsUser)
			$("#hdnGenderCodeLMS").val(this.value);
	});
	
	$("#txtDOBLMS").datepicker({
		regional: SYS_IBECommonParam.locale ,
		dateFormat: "dd/mm/yy",
		showOn: 'both',
		maxDate:'-2Y',
		minDate: "-99Y +1D",
		yearRange: yearRange,
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
	});
	
	$("#txtDOBLMS").change(function() {
		var dob = $("#txtDOBLMS").datepicker('getDate');
		var present = new Date();
		var age = (present - dob)/1000/60/60/24;
		if (age > (12*365+3)){
			$("#lblEmailHMandLMS").hide();
			UI_RegCustomer.headFfidReq = true;
		}else{
			$("#lblEmailHMandLMS").show();
			UI_RegCustomer.headFfidReq = false;
		}		
	});
	
	$("#txtMCountryLMS").numeric();
	$("#txtMobileLMS").numeric();
	$("#trFirstNameLMS,#trMiddleNameLMS,#trLastNameLMS").keypress(function (e) {
		if (e.charCode != 0) {
			var regex = new RegExp("^[a-zA-Z\\-\\s]+$");
			var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (!regex.test(key)) {
				e.preventDefault();
				return false;
			}
		}
	});
	$("#txtPpnLMS").alphaNumeric();
	
	UI_RegCustomer.loadCustomerRegisterData();
	try {
		if(strAnalyticEnable == "true") {	
			//pageTracker._trackPageview("/RegisterCustomer");
			_gaq.push(['_trackPageview', "/RegisterCustomer" ]);
		}
	} catch(ex) {}
	
}
/**
 * Request Page data
 */
UI_RegCustomer.loadCustomerRegisterData = function() {	
	UI_RegCustomer.isSocialRedirection = false;
	$("#submitForm").ajaxSubmit({url:"showCustomerRegisterDetail.action", dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success: UI_RegCustomer.loadCustomerRegisterDataSucess, error:UI_commonSystem.setErrorStatus});
	return false;
}

UI_RegCustomer.loyaltyCheckChange = function(){
	var isLoyaltyEnroll = $("input[name='optCardAcc']:checked").val();
	if (isLoyaltyEnroll == "Y"){
		$("#trLoyalty .extractor").slideDown(100);
		userRegConfig.dob1.userRegVisibilty = true;
		userRegConfig.dob1.mandatory = true;
		userRegConfig.city1.userRegVisibilty = true;
		userRegConfig.city1.mandatory = true;
		userRegConfig.loyaltyPref1.userRegVisibilty = true;
		userRegConfig.loyaltyPref1.mandatory = true;
	}else{
		$("#trLoyalty .extractor").slideUp(100);
		userRegConfig.dob1.userRegVisibilty = false;
		userRegConfig.dob1.mandatory = false;
		userRegConfig.city1.userRegVisibilty = false;
		userRegConfig.city1.mandatory = false;
		userRegConfig.loyaltyPref1.userRegVisibilty = false;
		userRegConfig.loyaltyPref1.mandatory = false;
		$("#txtAccountNo").val("");
		$("#trDOB").val("");
		$("#txtCity").val("");
	}
}

UI_RegCustomer.lmsCheckChange = function(){
	var isLmsAdd = $("input[name='optLMS']:checked").val();
    if (isLmsAdd==="Y"){
    	if(isMashreqLoyaltyEnables && integrateMashreqWithLMSEnabled){
    		$("#trLoyalty").show();
    	}
    	$("#trDOB").hide();	
		$("#trLMSLoyalty .extractor").show();
		UI_RegCustomer.isBecomeLmsUser = true;
		$("#hdnEmailLMS").val($("#txtEmail").val());
		$("#hdnFirstNameLMS").val($("#txtFirstName").val());
		$("#hdnLastNameLMS").val($("#txtLastName").val());
		$("#hdnMobileLMS").val($("#hdnMobile").val());
		$("#hdnNationalityCodeLMS").val($("#selNationality").val());
		$("#hdnResidencyCodeLMS").val($("#selCountry").val());
		$("#hdnGenderCodeLMS").val($("#selTitle").val());
	}else{
		$("#trDOB").show();	
		if(integrateMashreqWithLMSEnabled){
			$("#trLoyalty").hide();
			$('input[name="optCardAcc"][value="N"]').prop('checked', true);
			UI_RegCustomer.loyaltyCheckChange();
		}		
		$("#trLMSLoyalty .extractor").hide();
		UI_RegCustomer.isBecomeLmsUser = false;
		$("#hdnFirstNameLMS").val("");
		$("#hdnLastNameLMS").val("");
		$("#hdnMobileLMS").val("");
		$("#hdnNationalityCodeLMS").val("");
		$("#hdnResidencyCodeLMS").val("");
		$("#hdnGenderCodeLMS").val("");		
	}
}
/**
 * 
 * Request data success
 */
UI_RegCustomer.loadCustomerRegisterDataSucess =  function(response) {	
	if(response != null && response.success == true) {
		integrateMashreqWithLMSEnabled = response.integrateMashreqWithLMSEnabled
		userRegConfig = response.userRegConfig;
		validationGroup = response.validationGroup;
		if(userRegConfig != null && userRegConfig != ""){
			Util_CustomerValidation.showHideFields();
		}
		UI_RegCustomer.layoutChage();
		$("#divLoadBg").show();
		$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
		$("#lblInfoAgree1").html(response.jsonLabel.lblInfoAgree1);
		if(response.customer != null){
			$("#lblInfoAgree1").show();
		}
		$("#lblInfoAgree2").html(response.jsonLabel.lblInfoAgree2);
		$("#lblInfoAgree3").html(response.jsonLabel.lblInfoAgree3).append(response.jsonLabel.lblInfoAgree4);	
		UI_RegCustomer.loyaltyAccountEnableDisable(response.loyaltyEnable);
		isMashreqLoyaltyEnables = response.loyaltyEnable;
		UI_RegCustomer.policyLinkSetup();		
		eval(response.titleInfo);
		//eval(response.countryTelephoneInfo);
		Util_CustomerValidation.buildTelephoneInfo(response.countryPhoneInfo, response.areaPhoneInfo);
		errorInfo = response.errorInfo;	
		$("#selCountry").fillFirstOption(response.jsonLabel.lblPleaseSelect);
		$("#selCountry").fillDropDown({dataArray:response.countryInfo, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selSecQ").fillDropDown({dataArray:response.questionInfo, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selNationality").fillFirstOption(response.jsonLabel.lblPleaseSelect);
		$("#selNationality").fillDropDown({dataArray:response.nationalityInfo, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selTitle").fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selEmgnTitle").fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
		UI_RegCustomer.setIbeSupportLanguages(response.ibeSuportLans);
		UI_RegCustomer.setQuestionsDefault();
		$('body').append('<div id="divSessContainer"></div>');
		UI_commonSystem.initSessionTimeout('divSessContainer',response.timeoutDTO,function(){});
        $(".aiRewardsLink").unbind("click").bind("click",function(){UI_commonSystem.openWindowPopup("11")});
		if(dataFromSocialSite != undefined && dataFromSocialSite != ""){
			// current user_-> no (leagacy login and social login)
			UI_RegCustomer.populateDataFromSocialSite();
		}else{
			UI_SocialCustomer.initLogin(response);
		}

		UI_commonSystem.loyaltyManagmentEnabled = response.loyaltyManagmentEnabled;

		if(UI_commonSystem.loyaltyManagmentEnabled){	
			$('input[name="optLMS"][value="Y"]').prop('checked', true);
			UI_RegCustomer.lmsCheckChange();
			$("#trDOB").hide();			
		} else {
			$('input[name="optLMS"][value="Y"]').prop('checked', false);
			$("#RewardsPanel-1").hide();
			$("#lmsIntegrates").hide();
			$("#trLMSLoyalty").hide();
		}
		if(GLOBALS!= undefined && GLOBALS.loyaltyEnable != undefined && !GLOBALS.loyaltyEnable){
			$("#RewardsPanel-2").hide();
		}
		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

/**
 * Parse the data received from social site
 */
UI_RegCustomer.populateDataFromSocialSite = function() {
	
	data1 = {'jsonData' : dataFromSocialSite};
	url = "customerLoginFromSocialSite!retriveCustomerDataForPreFill.action?";
	
	$.ajax({
		url : url,
		beforeSend : UI_commonSystem.loadingProgress(),
		data : data1,
		type : "POST",
		dataType : "json",
		success : UI_RegCustomer.populateDataFromSocialSiteSuccess,
		error : function(response) {
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		},
		complete : function(response) {
		}

	});
}
/**
 * Parse the data received from social site Success
 */
UI_RegCustomer.populateDataFromSocialSiteSuccess = function (response){
	if (response != null && response.success == true) {
		if(response.externalSiteResponse != null){
			$("#txtEmail").val(response.externalSiteResponse.email);
			$("#txtFirstName").val(response.externalSiteResponse.firstName);
			$("#txtLastName").val(response.externalSiteResponse.lastName);
			$("#socialSiteCustId").val(response.externalSiteResponse.id);
			$("#socialCustomerTypeId").val(response.externalSiteResponse.siteType);
			$("#profilePicUrl").val(response.externalSiteResponse.link);
			$("#txtEmail").attr('readonly', true);
			var gender = response.externalSiteResponse.gender;
			if(gender != null && gender.length > 0){
				var title =  (gender == 'female') ? "MS" : "MR";
				var titles = document.getElementById('selTitle');
				for( var x=0; x< titles.length; x++){
					if (titles.options[x].value == title){
						titles.options[x].selected = true;
				        break;
				    }
				}
			}
			
			var randomNumber = Math.random().toString(36);
			if(randomNumber.length > 10){
				randomNumber = randomNumber.substr(0,10);
			}
			$("#txtPassword").val(randomNumber);
			$("#txtConPassword").val(randomNumber);
			$(".skipPass").hide();
			UI_RegCustomer.isSocialRedirection = true;
		}
		
	} else {
		UI_commonSystem.loadErrorPage({	messageTxt : response.messageTxt
		});
	}
	UI_commonSystem.loadingCompleted();
}


/**
 * Register User
 */
UI_RegCustomer.customerRegister = function(){
	
	if (trim($("#txtTelephone").val()) == ""){
		$("#txtPCountry").val("");
		$("#txtPArea").val("");
	} 
				
	if(trim($("#txtMobile").val()) == ""){
		$("#txtMCountry").val("");
		$("#txtMArea").val("");
	}
	
	if(trim($("#txtOfficeTel").val()) == ""){
		$("#txtOCountry").val("");
		$("#txtOArea").val("");
	}
	
	if(trim($("#txFax").val()) == ""){
		$("#txtFCountry").val("");
		$("#txtFArea").val("");
	}
	
	if(trim($("#txtEmgnTelephone").val()) == ""){
		$("#txtEmgnPCountry").val("");
		$("#txtEmgnPArea").val("");
	}
	
	if ($('#chkQ1').is(':checked') != true) {
		//$("#chkQ1").val("N");
		$("#promotional").val("N");
	}	
	
	var loyaltyOptionSelected=false;
	var isLoyaltyAdd = $("input[name='optCardAccLoyalty']:checked").val();
    if (isLoyaltyAdd!="Y"){
    	loyaltyOptionSelected=true;
	}
	if(!loyaltyOptionSelected && integrateMashreqWithLMSEnabled){
		$("#txtAccountNo").val("");
	}
	
	$("#frmRegister").attr('action', SYS_IBECommonParam.securePath + 'customerRegister.action');
	
	if(UI_commonSystem.loyaltyManagmentEnabled){
		var isLmsAdd = $("input[name='optLMS']:checked").val();
	    if (isLmsAdd==="Y"){
	    	var dob=$("#txtDOBLMS").val();
	    	$("#txtDateOfBirth").val(dob);	    	
		}
	}	
	$("#frmRegister").ajaxSubmit({dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress, success: UI_RegCustomer.registerCustomerSuccess, error:UI_commonSystem.setErrorStatus});		
	return false;
}

UI_RegCustomer.registerCustomer = function() {
	var validateDOB = true;
	var isLmsAdd = $("input[name='optLMS']:checked").val();
    if (isLmsAdd==="Y"){
    	validateDOB = false;
	}
	if (!Util_CustomerValidation.validateFormData(true,validateDOB))
		return;
	
	if (Util_CustomerValidation.lmsValidationData != null && !Util_CustomerValidation.ibeUserExists){
		if( Util_CustomerValidation.lmsValidationData.emailId != null && Util_CustomerValidation.lmsValidationData.emailId == 'Y'){
			var mergeMsg = "<div>The account with this Airewards ID already exists. If you want to merge please click Ok.</div>";
			var accountMege = "Mege Account";
			jConfirm(mergeMsg, accountMege, function(response) {
				if (response){
					UI_RegCustomer.customerRegister();	
				} else {
					return false;	
				}
			});
		}
		else{
			UI_RegCustomer.customerRegister();
		}
	} else{
		UI_RegCustomer.customerRegister();
	}

}

/**
 * Register User Success
 */
UI_RegCustomer.registerCustomerSuccess = function(response) {
	if(response != null && response.success == true) {
		$("#linkFocus").focus();
		if (response.messageTxt != null && $.trim(response.messageTxt) != "") {	
			$("#spnError").html("<font class='mandatory'>" + response.messageTxt + "<\/font>");
			
			if(!response.lmsNameMismatch){				
				$("#txtEmail").addClass("errorControl");
				$("#txtPassword").addClass("errorControl");
				$("#txtConPassword").addClass("errorControl");
			}

			if(UI_RegCustomer.isSocialRedirection == false){
				$("#txtConPassword").val("");
			}						
		} else {
			
			try {
				if(strAnalyticEnable == "true") {	
					//pageTracker._trackPageview("/RegisterCustomerSuccess");
					_gaq.push(['_trackPageview', "/RegisterCustomerSuccess" ]);
				}
			} catch(ex) {}
			
			if(response.autoLogin){
				//retrieved in registering with social sites
				$("#exSiteType").val($("#socialCustomerTypeId").val());
				$("#exLink").val($("#profilePicUrl").val());
				
				$("#frmLogin").attr("target", "_top");
				$("#frmLogin").attr('action','showCustomerLoadPage!loadCustomerHomePage.action');
				$("#frmLogin").submit();
				return false;
			}else{
				$("#trForm").hide();
				$("#trMsg").show();
			}
		}
		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
	var isLmsAdd = $("input[name='optLMS']:checked").val();
    if (isLmsAdd==="Y"){
		$("#trLmsReg").show();
	}else{
		$("#trLmsReg").hide();
		
	}
}
/**
 * Default Questions
 */
UI_RegCustomer.setQuestionsDefault = function() {
	UI_RegCustomer.radioButtonChecked({name:"questionaire[4].questionAnswer", value:"en"});
}

/**
 * Checked Radio button Using value
 */
UI_RegCustomer.radioButtonChecked = function(data) {
	$("input[name='"+ data.name +"']:radio").filter("[value='"+ data.value +"']").attr("checked", true);	
}
/**
 * Button Reset Click
 */
UI_RegCustomer.resetButtonClick = function() {
	$("#frmRegister").reset();
	UI_RegCustomer.setQuestionsDefault();
}

/**
 * Loyalty Account Enable Disable
 */
UI_RegCustomer.loyaltyAccountEnableDisable = function(isLoyaltyEnable) {	
	 if (isLoyaltyEnable == false || isLoyaltyEnable == "false") {
		 $("#trLoyalty").hide();
	 }else{
		 $("#trLoyalty .extractor").hide();
		 $("#dobloyalatyAccount").append($("#trDOB").show());
		 $("#dobloyalatyAccount").append($("#trCity").show());
		 
		 //set default values for dob, city & loyaltyPref
		 userRegConfig.dob1.userRegVisibilty = false;
		 userRegConfig.dob1.mandatory = false;
		 userRegConfig.city1.userRegVisibilty = false;
		 userRegConfig.city1.mandatory = false;
		 userRegConfig.loyaltyPref1.userRegVisibilty = false;
		 userRegConfig.loyaltyPref1.mandatory = false;
		 $("#txtAccountNo").val("");
		 $("#trDOB").val("");
		 $("#txtCity").val("");
		 
		/* $(document).on("click","input[name='optCardAcc']", function(){
				if ($(this).val() == "Y"){
					$("#trLoyalty .extractor").slideDown(100);
					userRegConfig.dob1.userRegVisibilty = true;
					userRegConfig.dob1.mandatory = true;
					userRegConfig.city1.userRegVisibilty = true;
					userRegConfig.city1.mandatory = true;
					userRegConfig.loyaltyPref1.userRegVisibilty = true;
					userRegConfig.loyaltyPref1.mandatory = true;
				}else{
					$("#trLoyalty .extractor").slideUp(100);
					userRegConfig.dob1.userRegVisibilty = false;
					userRegConfig.dob1.mandatory = false;
					userRegConfig.city1.userRegVisibilty = false;
					userRegConfig.city1.mandatory = false;
					userRegConfig.loyaltyPref1.userRegVisibilty = false;
					userRegConfig.loyaltyPref1.mandatory = false;
					$("#txtAccountNo").val("");
					$("#trDOB").val("");
					$("#txtCity").val("");
				}
		});	*/	 
	 }
}

/**
 * Policy Link Set up
 */
UI_RegCustomer.policyLinkSetup = function() {
	$("#linkPolicy").attr("href", SYS_IBECommonParam.homeURL + "/privacy-policy.html");
}



UI_RegCustomer.setIbeSupportLanguages = function(languagesMap) {
	var templeteName = "templateLanguages";
	if (languagesMap != null) {
		for (var i = 0; i < languagesMap.length; i++) {
			var clonedTemplateId = templeteName + "_" + i;
			var clone = $( "#" + templeteName + "").clone();	
			clone.attr("id", clonedTemplateId );
			if (i == 0) {				
				clone.insertAfter($("#" + templeteName));
			} else {
				clone.insertAfter($("#" + templeteName + "_" +(i -1)));
			}
			$("#" + clonedTemplateId).find("#radQ5").val(languagesMap[i][0]);
			$("#" + clonedTemplateId).find("#lblLanName").text(languagesMap[i][1]);			
		}
		$("#" + templeteName).hide();
		
		// Hide if there is a one language - RAk Requirement
		if (languagesMap.length == 1) {
			$("#lblInfoLan").hide();
			$("#templateLanguages_0").hide();
		}
	}
}

UI_RegCustomer.loadSignIn = function() {
	var formHtml =  "<form id='dynamicForm' method='post'> <input type='hidden' name='hdnParamData' value="+ SYS_IBECommonParam.locale +"^SI></form>";
	$("body").append(formHtml);
	$("#dynamicForm").attr("action", SYS_IBECommonParam.securePath + "showReservation.action");
	
	if(fromSocialSites != undefined && fromSocialSites != "" && fromSocialSites && fromSocialSites =='true'){
		$("#dynamicForm").attr("target", "_self");	
	} else{
		$("#dynamicForm").attr("target", "_top");	
	}
	$("#dynamicForm").submit();	
}

UI_RegCustomer.layoutChage = function(){
	//left side panel settings
	if(SYS_IBECommonParam.locale == "ar" || SYS_IBECommonParam.locale == "fa"){
		$(".div-Table").removeClass("div-Table").addClass("div-Table-rtl");
	}
	if (globalConfig.layOut != undefined){
		if (!globalConfig.layOut.newRegUser){
			$("#sortable-ul .sortable-child:first").hide();
			$(".rightColumn").css("width","98%");
		}
		if (globalConfig.layOut.agreementInfoPossition == 'bottom'){
			var temp = $("#aggrementInfo").remove().clone();
			$("#trButtons").parent().append(temp);
			$("#btnPrevious, #btnReset").hide();
			if(SYS_IBECommonParam.locale == "ar"){
				$("#btnRegister").parent().attr("align", "left");
			}else{
				$("#btnRegister").parent().attr("align", "right");
			}
		}
	}else{
		$("#sortable-ul .sortable-child:first").hide();
		$(".rightColumn").css("width","98%");
	}
}

