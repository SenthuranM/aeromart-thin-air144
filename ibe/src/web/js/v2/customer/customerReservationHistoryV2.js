/**
 * 
 * Author : Pradeep Karunanayake
 */

function UI_ReservationHistory(){}
var config = UI_Top.holder().GLOBALS.isShowAlertsInIBEEnabled; 

//To Do:Implement Document Ready
/*$(document).ready(function() {
	UI_ReservationHistory.ready();
}); */
/**
 * Page Ready
 */
UI_ReservationHistory.ready = function() {
	UI_ReservationHistory.reservationHistoryPageData();
}
/**
 * Request Reservation History data
 */
UI_ReservationHistory.reservationHistoryPageData = function() {	
	$("#tdTravelHistory").ajaxSubmit({url:"showReservationList.action?dispatchMode=travelHistory&randomNumber="+UI_UserTabs.getRandomNumber(),type: "POST", dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress,success: UI_ReservationHistory.reservationHistoryPageDataSucess, error:UI_commonSystem.setErrorStatus});
	return false;
}

UI_ReservationHistory.reservationHistoryPageDataSucess = function(response) {
	if(response != null && response.success == true) {
		//$("#travelHistoryTemplate").iterateTempleteHistory({templeteName:"travelHistoryTemplate", data:response.reservationListTO, dtoName:"reservationListTO"});		
		$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
		UI_ReservationHistory.buildReservations(response);
		UI_UserTabs.clearPanels();
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

// To Do : Implement Template 
// To Do: Remove Html from js file
UI_ReservationHistory.buildReservations = function(data) {		
	
	var strHTMLText = '';
	$("#resTableHistory").html("");
	var intLength = data.reservationListTO.length;
	var intRowCount = 0 
	var strRowSpan  = "";
	if (intLength > 0){
		
		var strColor = "SeperateorBGColor";
		for (var i = 0 ; i < intLength ; i++){
			var intRowCount = data.reservationListTO[i].flightInfo.length ;
			strRowSpan = "";
			if (intRowCount > 0){
			   strRowSpan = 'rowspan = "' + intRowCount + '" ' ;
			}
			
			if (strColor == "SeperateorBGColor"){
				strColor = "";
			}else{
				strColor = "SeperateorBGColor";
			}
			
			for (var x = 0 ; x < intRowCount ; x++){

				strHTMLText = '<tr>';
				
				if (x == 0){	
					strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center" valign="top" ' + strRowSpan  + ' title="' + 'Click here to get details of Reservation' + '">';
					strHTMLText += '		<a href="javascript:UI_ReservationHistory.callPnrSubmit(' +  "'" +  data.reservationListTO[i].pnrNo + "','" +  data.reservationListTO[i].originatorPnr  + "'" + ')"><font><u>' + data.reservationListTO[i].pnrNo + '<\/u><\/font><\/a>';
					strHTMLText += '	<\/td>';
				}
				strHTMLText += '	<td class="alignLeft GridItems GridDblHighlight ' + strColor + '">';
				strHTMLText += '		<label>' + data.reservationListTO[i].flightInfo[x].orignNDest + '<\/label>';
				if(data.reservationListTO[i].originatorPnr !="" && config =='true'){
					strHTMLText += '		<img src="../images/AA169_no_cache.gif" onmouseover="UI_ReservationHistory.showFareFamily(event,' +  "'" + 'arrRes[i][1][x][4]' + "'" + '); return false;" onmouseout="hideTT()">';
				}
				strHTMLText += '	<\/td>';
				strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
				strHTMLText += '		<label>' + data.reservationListTO[i].flightInfo[x].departureDate + '  ' +data.reservationListTO[i].flightInfo[x].departureTime +'<\/label>';
				strHTMLText += '	<\/td>';
				strHTMLText += '	<td class="GridItems GridDblHighlight correction ' + strColor + '" align="center">';
				strHTMLText += '		<label>' + data.reservationListTO[i].flightInfo[x].arrivalDate + ' ' + data.reservationListTO[i].flightInfo[x].arrivalTime +'<\/label>';
				strHTMLText += '	<\/td>';
				strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center">';
				strHTMLText += '		<img src="../images/' + UI_ReservationHistory.getStatus(data.reservationListTO[i].flightInfo[x].status) + '" title="' + 'Segment Status : ' + UI_ReservationHistory.getStatusDesc(data.reservationListTO[i].flightInfo[x].status)  + '">';
				strHTMLText += '	<\/td>';
				if (x == 0){						
					strHTMLText += '	<td class="GridItems GridDblHighlight ' + strColor + '" align="center" ' + strRowSpan  + '>';
					strHTMLText += '		<img src="../images/' + UI_ReservationHistory.getStatus(data.reservationListTO[i].pnrStatus) + '" title="' + 'Reservation Status : ' + UI_ReservationHistory.getStatusDesc(data.reservationListTO[i].pnrStatus)  + '">';
					
					strHTMLText += '	<\/td>';								
				} 								 
				strHTMLText += '<\/tr>';
				
				$("#resTableHistory").append(strHTMLText);
			}
		}
		$("#lblSubHeadHisResNo").hide();
			
	} else{
		$("#lblSubHeadHisRes").hide();
		$("#trResHisList").hide();
		$("#lblSubHeadHisResNo").show();
	}		
    
}
	/**
	 * PNR Click
	 */
   UI_ReservationHistory.callPnrSubmit =  function(stPnr, groupPnr) {
		$("#travelHistory").slideUp("slow");
	    UI_CustomerHome.pnr = stPnr;
		UI_CustomerHome.groupPnr = groupPnr;
		UI_UserTabs.pnrClick();		
	}
	/**
	 * Get Reservation Status
	 */	
	UI_ReservationHistory.getStatus =  function(strPNRStatus){	
		var strHTMLText = ""
		switch (strPNRStatus){
			case "CNF" : strHTMLText = "AA175_no_cache.gif"; break;
			case "CNX" : strHTMLText = "AA163_no_cache.gif"; break;
			case "OHD" : strHTMLText = "AA176_no_cache.gif"; break;
			case "CLS" : strHTMLText = "spacer_no_cache.gif"; break;
			case "FLN" : strHTMLText = "spacer_no_cache.gif"; break;
			case "WHD" : strHTMLText = "spacer_no_cache.gif"; break;
		}		
		return strHTMLText;									
	}
    /**
     * Show Fare Family
     */
	// To Do : Review
	UI_ReservationHistory.showFareFamily = function(objEvent,text){		
		var strTTText	= "";
		strTTText		+= "			<font class='fntBold'>"+ text +"<\/font>";
		showTT(strTTText, "top", "LEFT", objEvent, -30, 20, 350);
	}
	/**
	 * Get Reservation Status Description
	 */
	UI_ReservationHistory.getStatusDesc =  function(strPNRStatus){
		var strHTMLText = ""
		switch (strPNRStatus){
			case "CNF" : strHTMLText = "Confirmed"; break;
			case "CNX" : strHTMLText = "Cancelled"; break;
			case "OHD" : strHTMLText = "Onhold"; break;
			case "CLS" : strHTMLText = ""; break;
			case "FLN" : strHTMLText = "Flown"; break;
			case "WHD" : strHTMLText = ""; break;
		}		
		return strHTMLText;				
	}
	
	// To Do:Remove method
	UI_ReservationHistory.ready();
	