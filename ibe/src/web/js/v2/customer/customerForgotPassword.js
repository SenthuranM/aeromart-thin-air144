/**
 *Author - Pradeep Karunanayaka 
 *
 */	
function UI_ForgotPWD(){}

var errorInfo = "";
var seqQuestion ={};
UI_ForgotPWD.strAnswer = "";
UI_ForgotPWD.strPwdSentMsg = "";
$(document).ready(function(){
	UI_ForgotPWD.ready();
});
/**
 *Page Ready 
 *
 */
UI_ForgotPWD.ready = function() {
	UI_ForgotPWD.loadCssFile({language:opener.SYS_IBECommonParam.locale});
	UI_ForgotPWD.setPageDirection({language:opener.SYS_IBECommonParam.locale});
	UI_ForgotPWD.loadPageData();
	$("#txtEmail").focus();
	$("#btnGetPWD").click(function(){UI_ForgotPWD.forgotPwdClick();});
	$("#btnClose").click(function(){UI_ForgotPWD.buttonCloseClick()});
}
/**
 * Button Close Click
 */
UI_ForgotPWD.buttonCloseClick = function() {
	window.close();
}

/**
 * Request Page data
 */
UI_ForgotPWD.loadPageData = function() {
	$.post("showCustomerForgotPasswordDetail.action","", UI_ForgotPWD.loadPageDataSucess, "json");
}
/**
 * Request Page Data Success
 */
UI_ForgotPWD.loadPageDataSucess =  function(response) {
	if(response != null && response.success == true) {
		$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
		UI_ForgotPWD.strAnswer = response.jsonLabel.lblAnswer;
		UI_ForgotPWD.strPwdSentMsg = response.jsonLabel.msgPasswordSent;
		errorInfo = response.errorInfo;
		seqQuestion = response.configMap.securityQuestion1;
		if (!seqQuestion.ibeVisibility && !seqQuestion.mandatory){
			$("#securityQuestion").hide();
			$("#securityAnswer").hide();
		}
		$("#SelSecurityQuestion").fillDropDown({dataArray:response.questionList, keyIndex:0, valueIndex:1, firstEmpty:false});		
	}
}

/**
 * Load page css file
 */
UI_ForgotPWD.loadCssFile = function(data) {	
	var language = data.language;
	var href = "../css/myStyle_no_cache.css";
	if (language == 'ar') 
		href = "../css/myStyle_" + data.language + "_no_cache.css";
	
	$("head").append("<link>");
    css = $("head").children(":last");
    css.attr({
      rel:  "stylesheet",
      type: "text/css",
      href: href
    });	
}
/**
 * Page Direction Right/Left
 */
UI_ForgotPWD.setPageDirection = function(data) {	
	var language = data.language;
	if(language == 'ar') {
	   // Set body dir attribute
	   $('html').attr("dir", "rtl");	  
	   $("#rightPanel").attr("align", "left");
	}
}
/**
 * Forgot Password Button Click
 */
UI_ForgotPWD.forgotPwdClick = function() {
	if (UI_ForgotPWD.clientValidate()) {
		 $("#frmPWD").attr('action', opener.SYS_IBECommonParam.securePath + 'customerForgetPassword.action');
		 $("#frmPWD").ajaxSubmit({dataType: 'json', success: UI_ForgotPWD.forgotPwdClickSuccess, error:UI_commonSystem.setErrorStatus});		
		return false;		
	}
}
/**
 * Forgot Password Success
 */
UI_ForgotPWD.forgotPwdClickSuccess = function(response) {
	var ErrorMsg = "";
	if(response != null && response.success == true) {	
		var message = response.messageTxt;		
		if (message == 'true') {
			ErrorMsg = UI_ForgotPWD.strPwdSentMsg;
			$("#spnError").html("<font style='color:#3B8937'><ul>" + ErrorMsg + "<\/ul><\/font>");			
			setTimeout("window.close()", 2500);			
		} else {
			ErrorMsg = "<li>"+message+"<\/li>";
			//jAlert(message);
			$("#spnError").html("<font class='mandatory'><ul>" + ErrorMsg + "<\/ul><\/font>");
		}		
	} else {
		ErrorMsg = response.messageTxt;
		//jAlert(response.messageTxt);
		$("#spnError").html("<font class='mandatory'><ul>" + ErrorMsg + "<\/ul><\/font>");
	}
	
}
// To Do Progress Setup
UI_ForgotPWD.progressSetup = function() {
	UI_commonSystem.loadingProgress();
}	
/**
 * Validate Form Data
 */	
UI_ForgotPWD.clientValidate = function () {
	var ErrorMsg = "";
	if (trim($("#txtEmail").val()) == ""){
		//jAlert(UI_commonSystem.raiseError(errorInfo["ERR043"]));
		//$("#txtEmail").focus();	
		ErrorMsg += "<li>"+UI_commonSystem.raiseError(errorInfo["ERR043"])+"</li>";
		//return false;
	}		
	if (seqQuestion.ibeVisibility && seqQuestion.mandatory){
		if ($("#SelSecurityQuestion").val() == ""){
			//jAlert(UI_commonSystem.raiseError(errorInfo["ERR044"]));
			ErrorMsg += "<li>"+UI_commonSystem.raiseError(errorInfo["ERR044"])+"</li>";
			//$("#SelSecurityQuestion").focus();	
			//return false;
		}		
		
		if ($("#txtAnswer").val() == ""){
			//jAlert(UI_commonSystem.raiseError(errorInfo["ERR045"]));
			ErrorMsg += "<li>"+UI_commonSystem.raiseError(errorInfo["ERR045"])+"</li>";
			//$("#txtAnswer").focus();	
			//return false;
		}
	}
	
	if (!checkEmail($("#txtEmail").val())){
		//jAlert(UI_commonSystem.raiseError(errorInfo["ERR046"]));
		ErrorMsg += "<li>"+UI_commonSystem.raiseError(errorInfo["ERR046"])+"</li>";
		//$("#txtEmail").focus();
		//return false;
	}
	
	var strChkEmpty = checkInvalidChar($("#txtAnswer").val(), errorInfo["ERR047"], UI_ForgotPWD.strAnswer);
	if (strChkEmpty != ""){
		ErrorMsg += "<li>"+strChkEmpty+"</li>";
		//jAlert(strChkEmpty);
		//$("#txtAnswer").focus();
		//return false ;		
	}
	$("#spnError").html("<font class='mandatory'><ul>" + ErrorMsg + "<\/ul><\/font>");
	if (ErrorMsg == "")
		return true
	else
		return false
}	