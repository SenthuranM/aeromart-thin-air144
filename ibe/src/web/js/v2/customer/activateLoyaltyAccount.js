/**
 * Author : Pradeep Karunanayake
 * 
 */
function UI_Loyalty(){} 

UI_Loyalty.labels = "";
$(document).ready(function() {
	UI_Loyalty.ready();
}); 
/**
 * Page Ready
 */
UI_Loyalty.ready = function() {
	$("#btnActivate").click(function(){UI_Loyalty.activateClick();});
	$("#btnPrevious").click(function(){UI_Loyalty.LoadHomePage();});
	//$("#imgCalendar").click(function(event){Util_Calendar.loadCalendarDOB(event)});
	$("#txtDateOfBirth").blur(function(){UI_Loyalty.dateOfBirthValidate(this);});
	$("#imgCalendar").hide();
	$("#txtDateOfBirth").datepicker({regional: SYS_IBECommonParam.locale ,
		dateFormat: "dd/mm/yy",
		maxDate: +0,
		showOn: 'both',
		yearRange:'-80:+10',
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
	});
	
	$("#txtMCountry").keydown(function(){UI_Loyalty.telephoneNoKeyPress(this);});
	$("#txtMArea").keydown(function(){UI_Loyalty.telephoneNoKeyPress(this);});
	$("#txtMobile").keydown(function(){UI_Loyalty.telephoneNoKeyPress(this);});
	
	$("#txtMCountry").keyup(function(){UI_Loyalty.telephoneNoKeyPress(this);});
	$("#txtMArea").keyup(function(){UI_Loyalty.telephoneNoKeyPress(this);});
	$("#txtMobile").keyup(function(){UI_Loyalty.telephoneNoKeyPress(this);});
	
	UI_Loyalty.loadPageData();
}

/**
 * 
 * Request Page initial Data
 */
UI_Loyalty.loadPageData = function() {	
	UI_commonSystem.loadingProgress();
	$("#submitForm").ajaxSubmit({url:"loyaltyAccountDetail.action", dataType: 'json', success: UI_Loyalty.loadPageDataProcess, error:UI_commonSystem.setErrorStatus});
	return false;
}
/**
 * 
 * Request Initial DataProcess
 */
UI_Loyalty.loadPageDataProcess = function(response) {
	
	if(response != null && response.success == true) {
		$("#divLoadBg").show();
		$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});	
		UI_Loyalty.labels = response.jsonLabel;
		$("#selNationality").fillFirstItem(response.jsonLabel.lblPleaseSelect);
		$("#selNationality").fillDropDown({dataArray:response.nationalityList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selCountry").fillFirstItem(response.jsonLabel.lblPleaseSelect);
		$("#selCountry").fillDropDown({dataArray:response.countryNameList, keyIndex:0, valueIndex:1, firstEmpty:false});		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
	
}


/**
 * Activate Account
 */
UI_Loyalty.activateClick = function() {
	if (UI_Loyalty.validateFields()) {
		$("#frmLoyalty").attr('action', 'activateLoyaltyAccountV2.action');
		$("#frmLoyalty").ajaxSubmit({dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress, 
			success: UI_Loyalty.activateClickDataProcess, error:UI_commonSystem.setErrorStatus});	
	}
}

/**
 * Activate Loyalty Data Process
 */
UI_Loyalty.activateClickDataProcess = function(response) {
	if(response != null && response.success == true) {
			var messge = response.messageTxt;
			var labels = UI_Loyalty.labels;
			if (!UI_commonSystem.isEmpty(messge)) {
				if (messge == "notExist") {					
					 $("#spnError").html("<font class='mandatory'>" + labels.msgAccountNotExist + "<\/font>");
				} else {
					if(messge == "alreadyLinked") {
						 $("#spnError").html("<font class='mandatory'>" + labels.msgAccountLinked + "<\/font>");
					}
					else {
						if(messge == "success"){
							jAlert(labels.msgAccountActivated);							
							UI_Loyalty.LoadHomePage
						}
					}
				}	
			}
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}
 

 
UI_Loyalty.validateFields =	function() {
		var labels = UI_Loyalty.labels;
	    var strErrorHTML = "";
	    UI_Loyalty.removeClass();
	    UI_Loyalty.addClass({ids:['txtDateOfBirth','txtMobile','txtMCountry','txtMArea', 'txtCity'], cssClass:"clsInput"});
	    UI_Loyalty.addClass({ids:['selNationality','selCountry','txtAccountNo'], cssClass:"clsSelect"});		

		var strNationality = $("#selNationality").val();
		var strCountry = $("#selCountry").val();
		var strCity = $("#txtCity").val();
		var strAccountNo = $("#txtAccountNo").val();

		if($.trim($("#txtMCountry").val()) != "") {
			if (validateInteger($("#txtMCountry").val())){
				removeZero("txtMCountry");
			}else {
				$("#txtMCountry").val("");
			}	
		}
		if($.trim($("#txtMArea").val()) != "") {
			if (validateInteger($("#txtMArea").val())){
				removeZero("txtMArea");
			}else {
				$("#txtMArea").val("");
			}	
		}
		if($.trim($("#txtMobile").val()) != "") {
			if (validateInteger($("#txtMobile").val())){
				removeZero("txtMobile");
			}else {
				$("#txtMobile").val("");
			}	
		}
		if ($.trim($("#txtMobile").val()) == ""){
			strErrorHTML += "<li> " + labels["msgMobile"];			
			UI_Loyalty.addClass({ids:['txtMobile','txtMCountry','txtMArea'], cssClass:"errorControl"});
		}		
	
		if ($.trim($("#txtMobile").val()) != ""){		
			if ($.trim($("#txtMCountry").val()) == ""){
				strErrorHTML += "<li> " + labels["msgMobile"];
				$("#txtMCountry").addClass("errorControl");	
			}
			
			if ($.trim($("#txtMArea").val()) == ""){
				strErrorHTML += "<li> " + labels["msgMobile"];
				$("#txtMArea").addClass("errorControl");				
			}

			if (!isAlphaNumericWhiteSpace($("#txtMobile").val())){
				strErrorHTML += "<li> " + labels["msgMobile"];
				$("#txtMobile").addClass("errorControl");				
			}					
		}					 			 

		if(strAccountNo==""){
			strErrorHTML += "<li> " + labels["msgAccountNo"];
			$("#txtAccountNo").addClass("errorControl");			
		}
		
		if(strCountry==""){
			strErrorHTML += "<li> " + labels["msgCountry"];
			$("#selCountry").addClass("errorControl");			
		}
		if(strCity=="") {
			strErrorHTML += "<li> " + labels["msgCity"];
			$("#txtCity").addClass("errorControl");				
		}
		if(strNationality == ""){
			strErrorHTML += "<li> " + labels["msgNationality"];
			$("#selNationality").addClass("errorControl");			
		}

		var strDOB = $("#txtDateOfBirth").val();
		if(strDOB==""){
			strErrorHTML += "<li> " +  labels["msgDateOfBirth"];
			$("#txtDateOfBirth").addClass("errorControl");			
		}else if(!dateValidDate(strDOB)){
			strErrorHTML += "<li> " +  labels["msgValidDate"];
			$("#txtDateOfBirth").addClass("errorControl");			
		}
		if (strErrorHTML != ""){ 
			strErrorHTML += "<br><br>";
			$("#spnError").html("<font class='mandatory'>" + strErrorHTML + "<\/font>");
			return false;
		}
		$("#mobileNo").val($("#txtMCountry").val() + "-" + $("#txtMArea").val() + "-" + trim($("#txtMobile").val()));
		
		return true;
		
	}

UI_Loyalty.LoadHomePage = function() {
	$("#frmLoyalty").attr('action','showCustomerLoadPage!loadCustomerHomePage.action');
	$("#frmLoyalty").submit();
	UI_commonSystem.loadingProgress();
}

UI_Loyalty.dateOfBirthValidate = function(objC){
	if (objC.value != ""){
		var strReturn = dateChk(objC.value)
		if (!strReturn){
			$("#selNationality").focus();
		}else{
			objC.value = strReturn;
		}
	}
}
/**
 * Telephone Key Press/UP 
 *
 */
UI_Loyalty.telephoneNoKeyPress = function(objC) {
	if (!validateInteger(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}
}
function removeZero(field) {
	var val = trim($("#"+field));
	$("#"+ field, Number(val) * 1);		
}

/**
 * Add CSS Class
 */
UI_Loyalty.addClass = function(data) {
	for (var i =0; i < data.ids.length; i++) {		
		$("#"+data.ids[i]).addClass(data.cssClass);
	}	
}

/**
 * Remove Error CSS Class
 */
UI_Loyalty.removeClass = function() {
	$("body").find(".errorControl").each(function( intIndex ) {
		$("#"+ this.id).removeClass("errorControl");
	});
}
/**
 * Fill First Item Of Select Box
 */
$.fn.fillFirstItem = function(label) {
	this.append("<option value=''>"+label + "</option>");
}	
