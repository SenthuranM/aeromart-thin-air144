function Util_CustomerValidation(){} 

var strDefTitle = "";
var strErrorHTML = "";
// To DO : Get Using Locale
var strMsgFax = 'Fax No';
var strMsgAns = 'Your Answer';
var strMsgFName = 'First Name';
var strMsgLName = 'Last Name';
var strMsgPref = ' Your Preferences';
Util_CustomerValidation.isBecomeLmsUser = false;
//this will keep the mapping of validation group to
//client error code
var jsonGroupValidation = "";

var jsonGroupElementMapping = {"1": ["#txtTelephone","#txtPArea","#txtPCountry","#txtMobile","#txtMArea","#txtMCountry"]};

//this will keep the mapping of fieldname+groupid of t_contact_details_config
//to html form element
var jsonFieldToElementMapping = {"phoneNo1":"#txtTelephone", "mobileNo1":"#txtMobile"};

Util_CustomerValidation.lmsValidationData = null;
Util_CustomerValidation.ibeUserExists = false;

Util_CustomerValidation.hasSpecialCharactors = function(str){
		var validated=true;
		var chars= new Array('>','<' ,'"',"'",'/','^','|') ;
		var len=str.length;
		var val=removeInvalids(str);
		if(val!=str){
			return false;
		}
		return validated;
	
	}
	
	/**
	 * Return the country code for the selected country
	 */
	Util_CustomerValidation.getCCForCountry = function() {
		var CONST = {CC: 0, COUNTRY: 4}
		var cArr = arrCountryPhone;
		var selectedCountry =  getValue("selCountry");
		for(var i=0; i<cArr.length; i++){
			if(cArr[i][CONST.COUNTRY] ==selectedCountry ){
				return cArr[i][CONST.CC];
			}
		}
		return '';
	}
	/**
	 * Validate Form Data
	 */

	Util_CustomerValidation.validateFormData = function(isRegisterUser,validateTxtDateOfBirth) {
		
		Util_CustomerValidation.removeClass();
		var validated=false;
		var telValidate = false;
		var mobileValidate = false;
		var homeOfficeValidate = false;
		var faxValidate = false;
		var emgnTelValidate = false;
		jsonGroupValidation = errorInfo.userMobileIncorrect;
		var selTitle = $("#selTitle").val();
		var strFirstName = $("#txtFirstName").val();
		var strLastName = $("#txtLastName").val();
		var strDOB = $("#txtDateOfBirth").val();		
		var strNationality = $("#selNationality").val();
		var strAddrStreet = $("#txtAddr1").val();
		var strAddrLine = $("#txtAddr2").val();
		var strZipCode = $("#txtZipCode").val();
		var strCountry = $("#selCountry").val();
		var radGender= $("#radGender").val();
		
		var strTelephone = $("#txtTelephone").val();
		var strMobile = $("#txtMobile").val();
		var strTelephoneOffice = $("#txtOfficeTel").val();
		var strFax  = $("#txFax").val();
		
		var strEmail = $("#txtEmail").val();
		var strPassword = $("#txtPassword").val();
		var strConfpassword = $("#txtConPassword").val();
		
		var strQS =	$("#selSecQ").val();
		var strAns = $("#txtAns").val();
		var strAltEmail = $("#txtAlternateEmail").val();
		var strCity = $("#txtCity").val();
		var strAirLine = UI_Top.holder().GLOBALS.acclaeroClientIdentifier;
		
		//Emergency contact details
		var emgnTitle = $("#selEmgnTitle").val();
		var emgnFirstName = $("#txtEmgnFirstName").val();
		var emgnLastName = $("#txtEmgnLastName").val();
		var emgnPhone = $("#txtEmgnTelephone").val();
		var emgnEmail = $("#txtEmgnEmail").val();
		
		var lmsCtry = $("#txtMCountryLMS").val();
		var lmsMobile = $("#txtMobileLMS").val();
		var lmsEmail = $("#txtEmailLMS").val();
		var lmsFirstName = $("#txtFirstNameLMS").val();
		var lmsLastName = $("#txtLastNameLMS").val();
		var lmsDOB = $("#txtDOBLMS").val();
		var lmsLanguage = $("#txtLanguageLMS").val();
		var lmsPpn = $("#txtPpnLMS").val();
		var lmsEmailH = $("#txtEmailHLMS").val();
		var lmsRefEmail = $("#txtRefferedEmailLMS").val();		
		
		var lmsOpt = $("input[name='optLMS']:checked").val();
		
	    if (lmsEmail != null && lmsEmail != ""){
			Util_CustomerValidation.isBecomeLmsUser = true;
		} else if(UI_commonSystem.lmsDetails!=null){
			Util_CustomerValidation.isBecomeLmsUser = true;
		}else if(lmsOpt == 'Y'){
			Util_CustomerValidation.isBecomeLmsUser = true;
		}else{
			Util_CustomerValidation.isBecomeLmsUser = false;
		}		
		
		strErrorHTML = "";
		$("#txtFirstName").addClass("clsInput");
		$("#txtLastName").addClass("clsInput");
		$("#txtDateOfBirth").addClass("clsInput");
		$("#selNationality").addClass("clsSelect");
		$("#selCountry").addClass("clsSelect");
		$("#txtAddr1").addClass("clsInput");
		$("#txtAddr2").addClass("clsInput");
		$("#txtZipCode").addClass("clsInput");
				
		$("input[name='customer.gender']").addClass("NoBorder");
		
		$("#txtTelephone").addClass("clsInput");
		$("#txtPCountry").addClass("clsInput");
		$("#txtPArea").addClass("clsInput");
		
		$("#txtMobile").addClass("clsInput");
		$("#txtMCountry").addClass("clsInput");
		$("#txtMArea").addClass("clsInput");
		$("#txtOfficeTel").addClass("clsInput");
		$("#txtOCountry").addClass("clsInput");
		$("#txtOArea").addClass("clsInput");
		$("#txFax").addClass("clsInput");
		$("#txtFCountry").addClass("clsInput");
		$("#txtFArea").addClass("clsInput");
		
		$("#txtEmail").addClass("clsInput");	
		$("#txtPassword").addClass("clsInput");	
		$("#txtConPassword").addClass("clsInput");	
		$("#txtQ2").addClass("clsInput");	
		
		$("#txtCity").addClass("clsInput");	
		
		$("#selSecQ").addClass("clsSelect");	
		$("#txtAns").addClass("clsInput");	
		$("#txtAlternateEmail").addClass("clsInput");	
		
		$("#selEmgnTitle").addClass("clsSelect");
		$("#txtEmgnFirstName").addClass("clsInput");
		$("#txtEmgnLastName").addClass("clsInput");
		$("#txtEmgnPCountry").addClass("clsInput");
		$("#txtEmgnPArea").addClass("clsInput");
		$("#txtEmgnTelephone").addClass("clsInput");
		$("#txtEmgnEmail").addClass("clsInput");
		
		$("#txtMCountryLMS").addClass("clsInput");
		$("#txtMobileLMS").addClass("clsInput");
		$("#txtEmailLMS").addClass("clsInput");
		$("#txtNameLMS").addClass("clsInput");
		$("#txtDOBLMS").addClass("clsInput");
		$("#txtLanguageLMS").addClass("clsSelect");
		$("#txtPPLMS").addClass("clsInput");
		$("#txtEmailHLMS").addClass("clsInput");
		
		if(userRegConfig.email1.userRegVisibilty && 
				userRegConfig.email1.mandatory){
			if(strEmail ==""){
				strErrorHTML += "<li> " + errorInfo.userEmailRqrd;
				$("#txtEmail").addClass("errorControl");
			}else if(checkEmail(strEmail) ==false){
				strErrorHTML += "<li> " + errorInfo.userEmailIncorrect;
				$("#txtEmail").addClass("errorControl");
			}
		} else if(userRegConfig.email1.userRegVisibilty &&
				!userRegConfig.email1.mandatory &&
				strEmail !=""){
			if(checkEmail(strEmail) ==false){
				strErrorHTML += "<li> " + errorInfo.userEmailIncorrect;
				$("#txtEmail").addClass("errorControl");
			}
		}
		
		if(Util_CustomerValidation.isBecomeLmsUser && UI_commonSystem.loyaltyManagmentEnabled){
			/**
			 * Age validation
			 * 
			 */
			
			if(lmsDOB==""){
				strErrorHTML += "<li> " +  errorInfo.userDateOfBirthRqrd;
				$("#txtDOBLMS").addClass("errorControl");
			}else if(!dateValidDate(lmsDOB)){
				strErrorHTML += "<li> " +  errorInfo.userDateOfBirtIncorrect;
				$("#txtDOBLMS").addClass("errorControl");
			}else{
			
                 /**
                  * passport validation
                  */
                var dob = $("#txtDOBLMS").datepicker("getDate");
                if(dob == null){
                	var dobArr = lmsDOB.split("/");
                	dob = new Date(dobArr[2], dobArr[1] - 1, dobArr[0]);
                }
                var present = new Date();
                var age = (present.getTime() - dob.getTime())/1000/60/60/24;
                if (age < (12*365+3)){
                    if(lmsEmailH ==""){
                        strErrorHTML += "<li> " + errorInfo.headEmailReqrd;
                        $("#txtEmailHLMS").addClass("errorControl");
                    }
                }   
			}
			
			var errorInHeadEmail = false;
			var errorInRefEmail = false;
			
			if(lmsEmailH!=null && lmsEmailH!=""){
				if(checkEmail(lmsEmailH) == false || lmsEmailH.toLowerCase() == strEmail.toLowerCase()){
					errorInHeadEmail = true;
	                strErrorHTML += "<li> " + errorInfo.headEmailIncorrect;
	                $("#txtEmailHLMS").addClass("errorControl");
	            }
			}
			
			if(lmsRefEmail!=null && lmsRefEmail!=""){
				if(checkEmail(lmsRefEmail) == false || lmsRefEmail.toLowerCase() == strEmail.toLowerCase()){
					errorInRefEmail = true;
                    strErrorHTML += "<li> " + errorInfo.refEmailIncorrect;
                    $("#txtRefferedEmailLMS").addClass("errorControl");
                }
			}
			
			var lmsPreConditionValidationError = Util_CustomerValidation.lmsHeadRefferedValidate(isRegisterUser);
			if(Util_CustomerValidation.ibeUserExists){
				strErrorHTML += "<li> " + lmsPreConditionValidationError;
			} else if(lmsPreConditionValidationError != ""){
				strErrorHTML += "<li> " + lmsPreConditionValidationError;
			}
						
			if(Util_CustomerValidation.lmsValidationData != null){
				if(!errorInHeadEmail && Util_CustomerValidation.lmsValidationData.headOFEmailId=="N"){
					strErrorHTML += "<li> " + errorInfo.headEmailDoesntExist;
					$("#txtEmailHLMS").addClass("errorControl");
					proceed = false;
				}
				if(!errorInRefEmail && Util_CustomerValidation.lmsValidationData.refferedEmail=="N"){
					strErrorHTML += "<li> " + errorInfo.refEmailDoesntExist;
					$("#txtRefferedEmailLMS").addClass("errorControl");
					proceed = false;
				}
			}
		}		
		
		
		if(userRegConfig.password1.userRegVisibilty &&
				userRegConfig.password1.mandatory){
			if(strPassword ==""){
				strErrorHTML += "<li> " + errorInfo.userPasswordRqrd;
				$("#txtPassword").addClass("errorControl");
			}else if(FindChar(strPassword) != "0"){
				strErrorHTML += "<li> " + errorInfo.passwordIncorrect;
				$("#txtPassword").addClass("errorControl");
				
			}else if (trim(strPassword).length < 6){
				strErrorHTML += "<li> " + errorInfo.passwordMax;
				$("#txtPassword").addClass("errorControl");
			}	
			
			if(strConfpassword ==""){
				strErrorHTML += "<li> " + errorInfo.userConfPasswordRqrd;
				$("#txtConPassword").addClass("errorControl");
			}else if(strPassword != strConfpassword){
				strErrorHTML += "<li> " + errorInfo.userPasswordsNotMatch
				$("#txtConPassword").addClass("errorControl");
			}
		} else if(userRegConfig.password1.userRegVisibilty &&
				!userRegConfig.password1.mandatory &&
				strPassword != ""){
			if(Util_CustomerValidation.hasSpecialCharactors(strPassword)==false){
				strErrorHTML += "<li> " + errorInfo.passwordIncorrect;
				$("#txtPassword").addClass("errorControl");
				
			}else if (trim(strPassword).length < 6){
				strErrorHTML += "<li> " + errorInfo.passwordMax;
				$("#txtPassword").addClass("errorControl");
			}	
			
			if(strConfpassword ==""){
				strErrorHTML += "<li> " + errorInfo.userConfPasswordRqrd;
				$("#txtConPassword").addClass("errorControl");
			}else if(strPassword != strConfpassword){
				strErrorHTML += "<li> " + errorInfo.userPasswordsNotMatch
				$("#txtConPassword").addClass("errorControl");
			}
		}
		
		if(userRegConfig.securityQuestion1.userRegVisibilty &&
				userRegConfig.securityQuestion1.mandatory){
			if(trim(strQS) == ""){
				strErrorHTML += "<li> " + errorInfo.userSecreteQsRqrd;
				$("#selSecQ").addClass("errorControl");
			}
			
			if(strAns ==""){
				strErrorHTML += "<li> " + errorInfo.userAnswerRqrd;
				$("#txtAns").addClass("errorControl");
			}else {
				strChkEmpty = checkInvalidChar(strAns, errorInfo.errInvalidChar, strMsgAns);
				if (strChkEmpty != ""){
					strErrorHTML += "<li> " + strChkEmpty;
					$("#txtAns").addClass("errorControl")
				}
			}
		}
		
		if(userRegConfig.alternateEmail1.userRegVisibilty &&
				userRegConfig.alternateEmail1.mandatory){
			if(strAltEmail ==""){
				strErrorHTML += "<li> " + errorInfo.UserAltEmailRqrd;
				$("#txtAlternateEmail").addClass("errorControl");
			} else if(checkEmail(strAltEmail) ==false){
				strErrorHTML += "<li> " + errorInfo.userAltEmailIncorrect;
				$("#txtAlternateEmail").addClass("errorControl");
			}
		} else if(userRegConfig.alternateEmail1.userRegVisibilty &&
				!userRegConfig.alternateEmail1.mandatory &&
				strAltEmail !=""){
			if(checkEmail(strAltEmail) ==false){
				strErrorHTML += "<li> " + errorInfo.userAltEmailIncorrect;
				$("#txtAlternateEmail").addClass("errorControl");
			}
		}
		
		
		if(userRegConfig.title1.userRegVisibilty && userRegConfig.title1.mandatory){
			if (trim($("#selTitle").val()) == ""){
				strErrorHTML += "<li> " +  errorInfo.titleRequired;
				$("#selTitle").addClass("errorControl");
			}
		}
		
		if(userRegConfig.firstName1.userRegVisibilty && userRegConfig.firstName1.mandatory){
			if (strFirstName==""){
				strErrorHTML += "<li> " +  errorInfo.userFirstNameRqrd;
				$("#txtFirstName").addClass("errorControl");
			} else if (!isAlphaWhiteSpace(trim(strFirstName))){
				strErrorHTML += "<li> " + errorInfo.userFirstNameAlpha;
				$("#txtFirstName").addClass("errorControl fontCapitalize");
			}
		} else if(userRegConfig.firstName1.userRegVisibilty && !userRegConfig.firstName1.mandatory &&
				strFirstName != ""){
			if (!isAlphaWhiteSpace(trim(strFirstName))){
				strErrorHTML += "<li> " + errorInfo.userFirstNameAlpha;
				$("#txtFirstName").addClass("errorControl fontCapitalize");
			}
		}
		if (trim(strFirstName) != ""){
			strChkEmpty = checkInvalidChar(strFirstName, errorInfo.errInvalidChar, strMsgFName);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#txtFirstName").addClass("errorControl")
			}	
		}
			 
		if(userRegConfig.lastName1.userRegVisibilty && 
				userRegConfig.lastName1.mandatory){
			if(strLastName==""){
				strErrorHTML += "<li> " +  errorInfo.userLastNameRqrd;
				$("#txtLastName").addClass("errorControl");
			} else if (!isAlphaWhiteSpace(trim(strLastName))){
				strErrorHTML += "<li> " + errorInfo.userLastNameAlpha;
				$("#txtFirstName").addClass("errorControl fontCapitalize");
			}
		} else if(userRegConfig.lastName1.userRegVisibilty &&
				!userRegConfig.lastName1.mandatory &&
				strLastName != ""){
			if (!isAlphaWhiteSpace(trim(strLastName))){
				strErrorHTML += "<li> " + errorInfo.userLastNameRqrd;
				$("#txtFirstName").addClass("errorControl fontCapitalize");
			}
		}
		
		if (trim(strLastName) != ""){
			strChkEmpty = checkInvalidChar(strLastName, errorInfo.errInvalidChar, strMsgLName);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#txtLastName").addClass("errorControl")
			}	
		}
		
		if(userRegConfig.gender1.userRegVisibilty && userRegConfig.gender1.mandatory){
			if (radGender == ""){
				objC[0].className = "errorControl";
				objC[1].className = "errorControl";		
				strErrorHTML += "<li> " +  errorInfo.userGenderRqrd;
			}
		}
		
		if(validateTxtDateOfBirth){
			if(userRegConfig.dob1.userRegVisibilty && userRegConfig.dob1.mandatory){
				if(strDOB==""){
					strErrorHTML += "<li> " +  errorInfo.userDateOfBirthRqrd;
					$("#txtDateOfBirth").addClass("errorControl");
				}else if(!dateValidDate(strDOB)){
					strErrorHTML += "<li> " +  errorInfo.userDateOfBirtIncorrect;
					$("#txtDateOfBirth").addClass("errorControl");
				}else if (!CheckDates(strDOB, UI_Top.holder().strToDate)){
					strErrorHTML += "<li> " +  errorInfo.DOBWrong;
					$("#txtDateOfBirth").addClass("errorControl");
				}else if (ageCompare(strDOB,  UI_Top.holder().strToDate, "12")){
					strErrorHTML += "<li> " +  errorInfo.ageLimit;
					$("#txtDateOfBirth").addClass("errorControl");
				}
			} else if(userRegConfig.dob1.userRegVisibilty && 
					!userRegConfig.dob1.mandatory && strDOB != ""){
				if(!dateValidDate(strDOB)){
					strErrorHTML += "<li> " +  errorInfo.userDateOfBirtIncorrect;
					$("#txtDateOfBirth").addClass("errorControl");
				}else if (!CheckDates(strDOB, UI_Top.holder().strToDate)){
					strErrorHTML += "<li> " +  errorInfo.DOBWrong;
					$("#txtDateOfBirth").addClass("errorControl");
				}else if (ageCompare(strDOB,  UI_Top.holder().strToDate, "12")){
					strErrorHTML += "<li> " +  errorInfo.ageLimit;
					$("#txtDateOfBirth").addClass("errorControl");
				}
			}
		}		
		
		if(userRegConfig.nationality1.userRegVisibilty && 
				userRegConfig.nationality1.mandatory){
			if(strNationality == ""){
				strErrorHTML += "<li> " + errorInfo.userNationalityRqrd;
				$("#selNationality").addClass("errorControl");
			}
		}
		
		if(userRegConfig.address1.userRegVisibilty && 
				userRegConfig.address1.mandatory){
			if(strAddrStreet == ""){
				strErrorHTML += "<li> " + errorInfo.userAddressRqrd;
				$("#txtAddr1").addClass("errorControl");
			}
		}
		
		if(userRegConfig.city1.userRegVisibilty && 
				userRegConfig.city1.mandatory){
			if(strCity=="") {
				strErrorHTML += "<li> " + errorInfo.userCityRqrd;
				$("#txtCity").addClass("errorControl");
			}
		}
		
		if(userRegConfig.zipCode1.userRegVisibilty && 
				userRegConfig.zipCode1.mandatory){
			if(strZipCode=="") {
				strErrorHTML += "<li> " + errorInfo.userZipCodeRqrd;
				$("#txtZipCode").addClass("errorControl");
			} else if(!Util_CustomerValidation.validateZipCode(strZipCode)){
				strErrorHTML += "<li> " + errorInfo.userZipCodeIncorrect;
				$("#txtZipCode").addClass("errorControl");
			}
		} else if(userRegConfig.zipCode1.userRegVisibilty && !userRegConfig.zipCode1.mandatory &&
				strZipCode != ""){
			if(!Util_CustomerValidation.validateZipCode(strZipCode)){
				strErrorHTML += "<li> " + errorInfo.userZipCodeIncorrect;
				$("#txtZipCode").addClass("errorControl");
			}
		}
		
		if(userRegConfig.country1.userRegVisibilty && 
				userRegConfig.country1.mandatory){
			if(strCountry==""){
				strErrorHTML += "<li> " + errorInfo.userCountryRqrd;
				$("#selCountry").addClass("errorControl");
			}
		}
		
		if(userRegConfig.phoneNo1.userRegVisibilty &&
				userRegConfig.phoneNo1.mandatory){
			if(trim($("#txtPCountry").val()) != "") {
				if (validateInteger($("#txtPCountry").val())){
					Util_CustomerValidation.removeZero("txtPCountry");
				}else {
					setField("txtPCountry", "");
				}	
			}
			if(trim($("#txtPArea").val()) != "") {
				if (validateInteger($("#txtPArea").val())){
					Util_CustomerValidation.removeZero("txtPArea");
				}else {
					setField("txtPArea", "");
				}	
			}
			if(trim($("#txtTelephone").val()) != "") {
				if (validateInteger($("#txtTelephone").val())){
					Util_CustomerValidation.removeZero("txtTelephone");
				}else {
					setField("txtTelephone", "");
				}	
			}
			
			if(trim($("#txtTelephone").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userTelephoneRqrd;
				$("#txtTelephone").addClass("errorControl");
				$("#txtPCountry").addClass("errorControl");	
				$("#txtPArea").addClass("errorControl");
			} else {
				if (trim($("#txtPCountry").val()) == ""){
					strErrorHTML += "<li> " + errorInfo.userTeleCountryphoneRqrd;
					$("#txtPCountry").addClass("errorControl");	
				}
				
				if (trim($("#txtPArea").val()) == "" && (typeof contactConfig == 'undefined'?true:contactConfig.ibeUserRegAreaCodeVisible)){
					strErrorHTML += "<li> " + errorInfo.userTeleAreaphoneRqrd;
					$("#txtPArea").addClass("errorControl");	
				}
				
				if (!isAlphaNumericWhiteSpace($("#txtTelephone").val())){
					strErrorHTML += "<li> " + userTelephoneRqrd;
					$("#txtTelephone").addClass("errorControl");	
				}
			}
			telValidate = true;
		} else if(userRegConfig.phoneNo1.userRegVisibilty &&
				!userRegConfig.phoneNo1.mandatory &&
				trim($("#txtTelephone").val()) != "" && 
				(trim($("#txtPCountry").val()) != "" || trim($("#txtPArea").val()) != "")){
			
			if (validateInteger($("#txtPCountry").val())){
				Util_CustomerValidation.removeZero("txtPCountry");
			}else {
				setField("txtPCountry", "");
			}	
			
			if (validateInteger($("#txtPArea").val())){
				Util_CustomerValidation.removeZero("txtPArea");
			}else {
				setField("txtPArea", "");
			}
			
			if (validateInteger($("#txtTelephone").val())){
				Util_CustomerValidation.removeZero("txtTelephone");
			}else {
				setField("txtTelephone", "");
			}	
			telValidate = true;
		} else {
			$("#txtTelephone").val("");	
			$("#txtPCountry").val("");
			$("#txtPArea").val("");	
		}
		
		if(userRegConfig.mobileNo1.userRegVisibilty &&
				userRegConfig.mobileNo1.mandatory){
			if(trim($("#txtMCountry").val()) != "") {
				if (validateInteger($("#txtMCountry").val())){
					Util_CustomerValidation.removeZero("txtMCountry");
				}else {
					setField("txtMCountry", "");
				}	
			}
			if(trim($("#txtMArea").val()) != "") {
				if (validateInteger($("#txtMArea").val())){
					Util_CustomerValidation.removeZero("txtMArea");
				}else {
					setField("txtMArea", "");
				}	
			}
			if(trim($("#txtMobile").val()) != "") {
				if (validateInteger($("#txtMobile").val())){
					Util_CustomerValidation.removeZero("txtMobile");
				}else {
					setField("txtMobile", "");
				}	
			}
			
			if(trim($("#txtMobile").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userMobileRqrd;
				$("#txtMobile").addClass("errorControl");	
				$("#txtMCountry").addClass("errorControl");	
				$("#txtMArea").addClass("errorControl");
			} else {
				if (trim($("#txtMCountry").val()) == ""){
					strErrorHTML += "<li> " + errorInfo.userMobileCountryphoneRqrd;
					$("#txtMCountry").addClass("errorControl");	
				}
				
				if (trim($("#txtMArea").val()) == "" && (typeof contactConfig == 'undefined'?true:contactConfig.ibeUserRegAreaCodeVisible)){
					strErrorHTML += "<li> " + errorInfo.userMobileAreaphoneRqrd;
					$("#txtMArea").addClass("errorControl");	
				}

				if (!isAlphaNumericWhiteSpace(getValue("txtMobile"))){
					strErrorHTML += "<li> " + errorInfo.userMobileRqrd;
					setStyleClass("txtMobile","errorControl");	
				}
			}
			
			mobileValidate = true;
			
		} else if(userRegConfig.mobileNo1.userRegVisibilty &&
				!userRegConfig.mobileNo1.mandatory &&
				trim($("#txtMobile").val()) != "" &&
				(trim($("#txtMCountry").val()) != "" || trim($("#txtMArea").val()) != "")){
			if (validateInteger($("#txtMCountry").val())){
				Util_CustomerValidation.removeZero("txtMCountry");
			}else {
				setField("txtMCountry", "");
			}
			
			if (validateInteger($("#txtMArea").val())){
				Util_CustomerValidation.removeZero("txtMArea");
			}else {
				setField("txtMArea", "");
			}	
			
			if (validateInteger($("#txtMobile").val())){
				Util_CustomerValidation.removeZero("txtMobile");
			}else {
				setField("txtMobile", "");
			}
			
			mobileValidate = true;
			
		} else {
			$("#txtMobile").val("");		
			$("#txtMCountry").val("");
			$("#txtMArea").val("");	
		}	
		
		if(userRegConfig.homeOfficeNo1.userRegVisibilty &&
				userRegConfig.homeOfficeNo1.mandatory){
			if(trim($("#txtOCountry").val()) != "") {
				if (validateInteger($("#txtOCountry").val())){
					Util_CustomerValidation.removeZero("txtOCountry");
				}else {
					setField("txtOCountry", "");
				}	
			}
			if(trim($("#txtOArea").val()) != "") {
				if (validateInteger($("#txtOArea").val())){
					Util_CustomerValidation.removeZero("txtOArea");
				}else {
					setField("txtOArea", "");
				}	
			}
			if(trim($("#txtOfficeTel").val()) != "") {
				if (validateInteger($("#txtOfficeTel").val())){
					Util_CustomerValidation.removeZero("txtOfficeTel");
				}else {
					setField("txtOfficeTel", "");
				}	
			}
			
			if(trim($("#txtOfficeTel").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userHomeOfficeRqrd;
				$("#txtMobile").addClass("errorControl");	
				$("#txtOCountry").addClass("errorControl");	
				$("#txtOArea").addClass("errorControl");	
			} else {
				if (trim($("#txtOCountry").val()) == ""){
					strErrorHTML += "<li> " + errorInfo.userHomeOfficeCountryphoneRqrd;
					$("#txtOCountry").addClass("errorControl");	
				}
				
				if (trim($("#txtOArea").val()) == "" && (typeof contactConfig == 'undefined'?true:contactConfig.ibeUserRegAreaCodeVisible)){
					strErrorHTML += "<li> " + errorInfo.userHomeOfficeAreaphoneRqrd;
					$("#txtOArea").addClass("errorControl");	
				}

				if (!isAlphaNumericWhiteSpace(getValue("txtOfficeTel"))){
					strErrorHTML += "<li> " + errorInfo.userHomeOfficeRqrd;
					setStyleClass("txtOfficeTel","errorControl");	
				}
			}
			
			homeOfficeValidate = true;
			
		} else if(userRegConfig.homeOfficeNo1.userRegVisibilty &&
				!userRegConfig.homeOfficeNo1.mandatory &&
				trim($("#txtOfficeTel").val()) != "" &&
				(trim($("#txtOCountry").val()) != "" || trim($("#txtOArea").val()) != "")){
			
			if (validateInteger($("#txtOCountry").val())){
				Util_CustomerValidation.removeZero("txtOCountry");
			}else {
				setField("txtOCountry", "");
			}
			
			if (validateInteger($("#txtOArea").val())){
				Util_CustomerValidation.removeZero("txtOArea");
			}else {
				setField("txtOArea", "");
			}
			
			if (validateInteger($("#txtOfficeTel").val())){
				Util_CustomerValidation.removeZero("txtOfficeTel");
			}else {
				setField("txtOfficeTel", "");
			}
			
			homeOfficeValidate = true;
			
		} else {
			$("#txtOfficeTel").val("");		
			$("#txtOCountry").val("");
			$("#txtOArea").val("");	
		}
		
		if(userRegConfig.fax1.userRegVisibilty &&
				userRegConfig.fax1.mandatory){
			
			if(trim($("#txtFCountry").val()) != "") {
				if (validateInteger($("#txtFCountry").val())){
					Util_CustomerValidation.removeZero("txtFCountry");
				}else {
					setField("txtFCountry", "");
				}	
			}
			if(trim($("#txtFArea").val()) != "") {
				if (validateInteger($("#txtFArea").val())){
					Util_CustomerValidation.removeZero("txtFArea");
				}else {
					setField("txtFArea", "");
				}	
			}
			if(trim($("#txFax").val()) != "") {
				if (validateInteger($("#txFax").val())){
					Util_CustomerValidation.removeZero("txFax");
				}else {
					setField("txFax", "");
				}	
			}
			
			if(trim($("#txFax").val()) == ""){
				strErrorHTML += "<li> " + errorInfo.userFaxRqrd;
				$("#txFax").addClass("errorControl");	
				$("#txtFCountry").addClass("errorControl");
				$("#txtFArea").addClass("errorControl");	
			} else {
				if (trim($("#txtFCountry").val()) == ""){
					strErrorHTML += "<li> " + errorInfo.userfaxCountryphoneRqrd;
					$("#txtFCountry").addClass("errorControl");	
				}
				
				if (trim($("#txtFArea").val()) == "" && (typeof contactConfig == 'undefined'?true:contactConfig.ibeUserRegAreaCodeVisible)){
					strErrorHTML += "<li> " + errorInfo.userFaxAreaphoneRqrd;
					$("#txtFArea").addClass("errorControl");	
				}

				if (!isAlphaNumericWhiteSpace(getValue("txFax"))){
					strErrorHTML += "<li> " + errorInfo.userFaxRqrd;
					setStyleClass("txFax","errorControl");	
				}
			}
			
			faxValidate = true;
			
		} else if(userRegConfig.fax1.userRegVisibilty &&
				!userRegConfig.fax1.mandatory &&
				trim($("#txFax").val()) != "" &&
				(trim($("#txtFArea").val()) != "" || trim($("#txtFCountry").val()) != "")){
			if (validateInteger($("#txtFCountry").val())){
				Util_CustomerValidation.removeZero("txtFCountry");
			}else {
				setField("txtFCountry", "");
			}	
			
			if (validateInteger($("#txtFArea").val())){
				Util_CustomerValidation.removeZero("txtFArea");
			}else {
				setField("txtFArea", "");
			}
			
			if (validateInteger($("#txFax").val())){
				Util_CustomerValidation.removeZero("txFax");
			}else {
				setField("txFax", "");
			}
			
			faxValidate = true;
			
		} else {
			$("#txFax").val("");		
			$("#txtFCountry").val("");
			$("#txtFArea").val("");	
		}
		
		if (trim($("#txtQ2").val()) != ""){
			strChkEmpty = checkInvalidChar($("#txtQ2").val(), errorInfo.errInvalidChar, strMsgPref);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#txtQ2").addClass("errorControl");
			}	
		}
		
		//Loyalaty account no validation
		if(userRegConfig.loyaltyPref1.userRegVisibilty &&
				userRegConfig.loyaltyPref1.mandatory){
			if($("#optCardAcc_yes").attr("checked")){				
				if (trim($("#txtAccountNo").val()) == ""){
					strErrorHTML += "<li> " + errorInfo.userMashreqAccountNo;
					$("#txtAccountNo").addClass("errorControl");
				}
			}
		}
		
	/*	var  strChar = "";
		var numCount = 0;
		for(var i= 0;i < strPassword.length; i++) {
			strChar = strPassword.substr(i,1); 
			if(isPositiveInt(strChar)){
				numCount++;
			}	
		}
		if((numCount < 3) || (numCount > (strPassword.length - 3))){
			strErrorHTML += "<li> " + invalidCombine;
			setStyleClass("txtPassword", "errorControl");
		} */		
		
		// To Do: Review
		/**
		if ($("#"+arrControlList[0][0]).val() != ""){
			strChkEmpty = checkInvalidChar(getValue(arrControlList[0][0]), errInvalidChar, arrControlList[0][1]);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#" + arrControlList[0][0]).addClass("errorControl");	
			}			
		}
		
		if ($("#"+arrControlList[1][0]).val() != ""){
			strChkEmpty = checkInvalidChar($("#"+arrControlList[1][0]).val(), errInvalidChar, arrControlList[1][1]);
			if (strChkEmpty != ""){
				strErrorHTML += "<li> " + strChkEmpty;
				$("#"+arrControlList[1][0]).addClass("errorControl");	
			}			
		}		
		*/
		
		
		if(mobileValidate){
			Util_CustomerValidation.validateMobileNoFormat("#txtMCountry", "#txtMArea", "#txtMobile");
		}
		
		if(telValidate){
			Util_CustomerValidation.validateUserPhone("#txtPCountry", "#txtPArea", "#txtTelephone");
		}
		
		if(homeOfficeValidate){
			Util_CustomerValidation.validateUserPhone("#txtOCountry", "#txtOArea", "#txtOfficeTel");
		}
		
		if(faxValidate){
			Util_CustomerValidation.validateUserPhone("#txtFCountry", "#txtFArea", "#txFax");
		}
				
		var groupErrHTML = groupFieldValidation(validationGroup, jsonFieldToElementMapping, jsonGroupValidation);	
		
		if(groupErrHTML != ""){
			strErrorHTML += groupErrHTML;
			//isNavExc = false;
		}
		//Emergency contact details validation
		
		if(userRegConfig.firstName2.userRegVisibilty ||
				userRegConfig.lastName2.userRegVisibilty ||
				userRegConfig.phoneNo2.userRegVisibilty){
			
			if(userRegConfig.title2.userRegVisibilty && 
					userRegConfig.title2.mandatory){
				if(emgnTitle == ""){
					strErrorHTML += "<li> " + errorInfo.userEmgnTitleRqrd;
					$("#selEmgnTitle").addClass("errorControl");
				}
			}
			
			if(userRegConfig.firstName2.userRegVisibilty &&
					userRegConfig.firstName2.mandatory){
				if(emgnFirstName == ""){
					strErrorHTML += "<li> " + errorInfo.userEmgnFirstNameRqrd;
					$("#txtEmgnFirstName").addClass("errorControl");
				} else if (!isAlphaWhiteSpace(trim(emgnFirstName))){
					strErrorHTML += "<li> " + errorInfo.userEmgnFirstNameAlpha;
					$("#txtEmgnFirstName").addClass("errorControl fontCapitalize");
				}
			} else if(userRegConfig.firstName2.userRegVisibilty &&
					!userRegConfig.firstName2.mandatory &&
					emgnFirstName != ""){
				if (!isAlphaWhiteSpace(trim(emgnFirstName))){
					strErrorHTML += "<li> " + errorInfo.userEmgnFirstNameAlpha;
					$("#txtEmgnFirstName").addClass("errorControl fontCapitalize");
				}
			}
			
			if(userRegConfig.lastName2.userRegVisibilty &&
					userRegConfig.lastName2.mandatory){
				if(emgnLastName == ""){
					strErrorHTML += "<li> " + errorInfo.userEmgnLastNameRqrd;
					$("#txtEmgnLastName").addClass("errorControl");
				} else if (!isAlphaWhiteSpace(trim(emgnLastName))){
					strErrorHTML += "<li> " + errorInfo.userEmgnLastNameAlpha;
					$("#txtEmgnLastName").addClass("errorControl fontCapitalize");
				}
			} else if(userRegConfig.lastName2.userRegVisibilty &&
					!userRegConfig.lastName2.mandatory &&
					emgnLastName != ""){
				if (!isAlphaWhiteSpace(trim(emgnLastName))){
					strErrorHTML += "<li> " + errorInfo.userEmgnLastNameAlpha;
					$("#txtEmgnLastName").addClass("errorControl fontCapitalize");
				}
			}
				
			
			if(userRegConfig.phoneNo2.userRegVisibilty &&
					userRegConfig.phoneNo2.mandatory){
				if(trim($("#txtEmgnPCountry").val()) != "") {
					if (validateInteger($("#txtEmgnPCountry").val())){
						Util_CustomerValidation.removeZero("txtEmgnPCountry");
					}else {
						setField("txtEmgnPCountry", "");
					}	
				}
				if(trim($("#txtEmgnPArea").val()) != "") {
					if (validateInteger($("#txtEmgnPArea").val())){
						Util_CustomerValidation.removeZero("txtEmgnPArea");
					}else {
						setField("txtEmgnPArea", "");
					}	
				}
				if(trim($("#txtEmgnTelephone").val()) != "") {
					if (validateInteger($("#txtEmgnTelephone").val())){
						Util_CustomerValidation.removeZero("txtEmgnTelephone");
					}else {
						setField("txtEmgnTelephone", "");
					}	
				}
				
				if(trim($("#txtEmgnTelephone").val()) == ""){
					strErrorHTML += "<li> " + errorInfo.userEmgnPhoneRqrd;
					$("#txtEmgnTelephone").addClass("errorControl");
					$("#txtEmgnPCountry").addClass("errorControl");	
					$("#txtEmgnPArea").addClass("errorControl");	
				} else {
					if (trim($("#txtEmgnPCountry").val()) == ""){
						strErrorHTML += "<li> " + errorInfo.userEmgnCountryPhoneRqrd;
						$("#txtEmgnPCountry").addClass("errorControl");	
					}
					
					/*contactConfig.ibeUserRegAreaCodeVisible if true then validate area code. This is passed from database.
					 * But when registering it's not passed over. Which is why the shortcut if else operator was used.
					 */
					if (trim($("#txtEmgnPArea").val()) == "" && (typeof contactConfig == 'undefined'?true:contactConfig.ibeUserRegAreaCodeVisible)){
						strErrorHTML += "<li> " + errorInfo.userEmgnAreaPhoneRqrd;
						$("#txtEmgnPArea").addClass("errorControl");	
					}
					
					if (!isAlphaNumericWhiteSpace($("#txtEmgnTelephone").val())){
						strErrorHTML += "<li> " + userEmgnPhoneRqrd;
						$("#txtEmgnTelephone").addClass("errorControl");	
					}
				}
				emgnTelValidate = true;
			} else if(userRegConfig.phoneNo2.userRegVisibilty &&
					!userRegConfig.phoneNo2.mandatory &&
					trim($("#txtEmgnTelephone").val()) != "" &&
					trim($("#txtEmgnPCountry").val()) != "" &&
					trim($("#txtEmgnPArea").val()) != ""){
				if (validateInteger($("#txtEmgnPCountry").val())){
					Util_CustomerValidation.removeZero("txtEmgnPCountry");
				}else {
					setField("txtEmgnPCountry", "");
				}	
				
				if (validateInteger($("#txtEmgnPArea").val())){
					Util_CustomerValidation.removeZero("txtEmgnPArea");
				}else {
					setField("txtEmgnPArea", "");
				}
				
				if (validateInteger($("#txtEmgnTelephone").val())){
					Util_CustomerValidation.removeZero("txtEmgnTelephone");
				}else {
					setField("txtEmgnTelephone", "");
				}
				
				emgnTelValidate = true;
			} else {
				$("#txtEmgnTelephone").val("");
				$("#txtEmgnPCountry").val("");
				$("#txtEmgnPArea").val("");
			}
			
			if(emgnTelValidate){
				Util_CustomerValidation.validateUserPhone("#txtEmgnPCountry", "#txtEmgnPArea", "#txtEmgnTelephone");
			}
			
			if(userRegConfig.email2.userRegVisibilty &&
					userRegConfig.email2.mandatory){
				if(emgnEmail ==""){
					strErrorHTML += "<li> " + errorInfo.userEmgnEmailRqrd;
					$("#txtEmgnEmail").addClass("errorControl");
				}else if(checkEmail(emgnEmail) ==false){
					strErrorHTML += "<li> " + errorInfo.userEmgnEmailIncorrect;
					$("#txtEmgnEmail").addClass("errorControl");
				}
			} else if(userRegConfig.email2.userRegVisibilty &&
					!userRegConfig.email2.mandatory &&
					emgnEmail != ""){
				if(checkEmail(emgnEmail) ==false){
					strErrorHTML += "<li> " + errorInfo.userEmgnEmailIncorrect;
					$("#txtEmgnEmail").addClass("errorControl");
				}
			}
		}
		
		if (strErrorHTML != ""){ strErrorHTML += "<br><br>";}
		$("#spnError").html("<font class='mandatory paddingL5'>" + strErrorHTML + "<\/font>");
		
		
		if(!userRegConfig.areaCode1.userRegVisibilty){
			$("#txtPArea").val(trim($("#txtTelephone").val()).substr(0,2));
			$("#txtMArea").val(trim($("#txtMobile").val()).substr(0,2));
			$("#txtOArea").val(trim($("#txtOfficeTel").val()).substr(0,2));
			$("#txtFArea").val(trim($("#txFax").val()).substr(0,2));
			$("#txtEmgnPArea").val(trim($("#txtEmgnTelephone").val()).substr(0,2));
		} 

		if(userRegConfig.areaCode1.userRegVisibilty){

			if($("#txtPCountry").val()=="" || $("#txtPCountry").val()==null){
				Util_CustomerValidation.countryListChange();
			}
			if (strErrorHTML == ""){
				$("#hdnTelNo").val($("#txtPCountry").val() + "-" + $("#txtPArea").val() + "-" + trim($("#txtTelephone").val()));
				$("#hdnMobile").val($("#txtMCountry").val() + "-" + $("#txtMArea").val() + "-" + trim($("#txtMobile").val()));
				$("#hdnOffice").val($("#txtOCountry").val() + "-" + $("#txtOArea").val() + "-" + trim($("#txtOfficeTel").val()));
				$("#hdnFax").val($("#txtFCountry").val() + "-" + $("#txtFArea").val() + "-" + trim($("#txFax").val()));
				$("#hdnEmgnPhoneNo").val($("#txtEmgnPCountry").val() + "-" + $("#txtEmgnPArea").val() + "-" + trim($("#txtEmgnTelephone").val()));
				return true;
			}else{
				$("#linkFocus").focus();
				return false;
			}
		}else{
			if (strErrorHTML == ""){			
				$("#hdnTelNo").val($("#txtPCountry").val() + "-" + $("#txtPArea").val() + "-" + trim($("#txtTelephone").val()).substr(2,trim($("#txtTelephone").val()).length));
				$("#hdnMobile").val($("#txtMCountry").val() + "-" + $("#txtMArea").val() + "-" + trim($("#txtMobile").val()).substr(2,trim($("#txtMobile").val()).length));
				$("#hdnOffice").val($("#txtOCountry").val() + "-" + $("#txtOArea").val() + "-" + trim($("#txtOfficeTel").val()).substr(2,trim($("#txtOfficeTel").val()).length));
				$("#hdnFax").val($("#txtFCountry").val() + "-" + $("#txtFArea").val() + "-" + trim($("#txFax").val()).substr(2,trim($("#txFax").val()).length));
				$("#hdnEmgnPhoneNo").val($("#txtEmgnPCountry").val() + "-" + $("#txtEmgnPArea").val() + "-" + trim($("#txtEmgnTelephone").val()).substr(2,trim($("#txtEmgnTelephone").val()).length));
				return true;
			}else{
				$("#linkFocus").focus();
				return false;
			}	
		}
		
		function raiseErrorMsg(strMsg){
			if (arguments.length >1){
				for (var i = 0 ; i < arguments.length - 1 ; i++){
					strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
				}
			}
			return strMsg;
		}
	}
	


/**
 * Validate Date of Birth
 */
Util_CustomerValidation.validateDOB = function(objC){
	if (objC.value != ""){
		var strReturn = dateChk(objC.value)
		if (!strReturn){
			$("#selNationality").focus();
		}else{
			objC.value = strReturn;
		}
	}
}

/**
 * Country List Change
 */
Util_CustomerValidation.countryListChange = function() {
	$("#txtMCountry").val("");
	$("#txtPCountry").val("");
	$("#txtOCountry").val("");
	$("#txtFCountry").val("");
	$("#txtFCountry").val("");
	$("#txtMCountryLMS").val("");	
	var choseCountry = $("#selCountry").val();
	for(var x=0;x<arrCountryPhone.length;x++) {
		if(choseCountry == arrCountryPhone[x][4]) {
			$("#txtMCountry").val(arrCountryPhone[x][0]);
			$("#txtPCountry").val(arrCountryPhone[x][0]);	
			$("#txtOCountry").val(arrCountryPhone[x][0]);
			$("#txtFCountry").val(arrCountryPhone[x][0]);
			$("#txtMCountryLMS").val(arrCountryPhone[x][0]);
			break;
		}
	}	
}
/**
 * Telephone Key Press/UP 
 *
 */
Util_CustomerValidation.telephoneNoKeyPress = function(objC) {
	if (!validateInteger(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}
}

/**
 * Remove Zero
 */
Util_CustomerValidation.removeZero = function(field) {
	var val = trim($("#"+field));
	$("#" + field, Number(val) * 1);		
}	


/**
 * Add CSS Class
 */
Util_CustomerValidation.addClass = function(data) {
	$.each(data.Ids, function(index, value) { 
		$("#" + data).addClass(data.cssClass);		  
	});
}
/**
 * Remove CSS Class
 */
Util_CustomerValidation.removeClass = function() {
	$("body").find(".errorControl").each(function( intIndex ) {
		$("#"+ this.id).removeClass("errorControl");
	});
}

//Build Telephone data
Util_CustomerValidation.buildTelephoneInfo = function(strCountryPhone, strAreaPhone) {	
	
	var tempArrAreaPhone = null;
	var tempCountryCode = null;
	var tempNewArray = null;
	
	if (strCountryPhone != null && strAreaPhone != null) {		
		var arrCountryPhoneLocal = strCountryPhone.split("^");
		for (var i = 0; i < arrCountryPhoneLocal.length; i++) {
			arrCountryPhone[i] = arrCountryPhoneLocal[i].split(",");
		}
		
		var arrAreaPhoneLocal = strAreaPhone.split("^");
		for (var i = 0; i < arrAreaPhoneLocal.length; i++) {
			tempArrAreaPhone = arrAreaPhoneLocal[i].split(",");	
			tempCountryCode = tempArrAreaPhone[0];
			tempNewArray = new Array();
			tempNewArray[0] = tempArrAreaPhone[1];
			tempNewArray[1]= tempArrAreaPhone[2];
			tempNewArray[2]= tempArrAreaPhone[3];
			
			if (arrAreaPhone[tempCountryCode] == undefined) {
				arrAreaPhone[tempCountryCode] = new Array();
			}			
			arrAreaPhone[tempCountryCode][i] = new Array();
			arrAreaPhone[tempCountryCode][i]  = tempNewArray;			
		}		
	}	
}

// show/hide customer register & profile update fields beased on configurations
Util_CustomerValidation.showHideFields = function(){
	if(!userRegConfig.email1.userRegVisibilty){
		$("#trEmail").hide();
	} else if(!userRegConfig.email1.mandatory){
		$("#lblEmailMand").hide();
	}
	
	if(!userRegConfig.password1.userRegVisibilty){
		$("#tblPassword").hide();
	} else if(!userRegConfig.password1.mandatory){
		$("#lblpasswordMand").hide();
	}
	
	if(!userRegConfig.securityQuestion1.userRegVisibilty){
		$("#tblSecurityQuestion").hide();
	} else if(!userRegConfig.securityQuestion1.mandatory){
		$("#lblSecQuesMand").hide();
	}
	
	if(!userRegConfig.alternateEmail1.userRegVisibilty){
		$("#trAlternateEmail").hide();
	} else if(!userRegConfig.alternateEmail1.mandatory){
		$("#lblAlternateEmailMand").hide();
	}
	
	if(!userRegConfig.title1.userRegVisibilty){
		$("#trTitle").hide();
	} else if(!userRegConfig.title1.mandatory){
		$("#lblTitleMand").hide();
	}
	
	if(!userRegConfig.firstName1.userRegVisibilty){
		$("#trFirstName").hide();
	} else if(!userRegConfig.firstName1.mandatory){
		$("#lblFirstNameMand").hide();
	}
	
	if(!userRegConfig.lastName1.userRegVisibilty){
		$("#trLastName").hide();
	} else if(!userRegConfig.lastName1.mandatory){
		$("#lblLastNameMand").hide();
	}
	
	if(!userRegConfig.gender1.userRegVisibilty){
		$("#trGender").hide();
	} else if(!userRegConfig.gender1.mandatory){
		$("#lblGenderMand").hide();
	}
	
	if(!userRegConfig.dob1.userRegVisibilty){
		$("#trDOB").hide();
	} else if(!userRegConfig.dob1.mandatory){
		$("#lblDOBMand").hide();
	}
	
	if(!userRegConfig.nationality1.userRegVisibilty){
		$("#trNationality").hide();
	} else if(!userRegConfig.nationality1.mandatory){
		$("#lblNationalityMand").hide();
	}
	
	if(!userRegConfig.address1.userRegVisibilty){
		$("#trAddr1").hide();
		$("#trAddr2").hide();
	} else if(!userRegConfig.address1.mandatory){
		$("#lblAddrMand").hide();
	}
	
	if(!userRegConfig.city1.userRegVisibilty){
		$("#trCity").hide();
	} else if(!userRegConfig.city1.mandatory){
		$("#lblCityMand").hide();
	}
	
	if(!userRegConfig.zipCode1.userRegVisibilty){
		$("#trZipCode").hide();
	} else if(!userRegConfig.zipCode1.mandatory){
		$("#lblZipCodeMand").hide();
	}
	
	if(!userRegConfig.country1.userRegVisibilty){
		$("#trCountry").hide();
	} else if(!userRegConfig.country1.mandatory){
		$("#lblCountryMand").hide();
	}
	
	if(!userRegConfig.phoneNo1.userRegVisibilty && !userRegConfig.mobileNo1.userRegVisibilty
			&& !userRegConfig.homeOfficeNo1.userRegVisibilty && !userRegConfig.fax1.userRegVisibilty){
		$("#tblContactNo").hide();
	} else {
		if(!userRegConfig.phoneNo1.userRegVisibilty){
			$("#trPhoneNo").hide();
		} else if(!userRegConfig.phoneNo1.mandatory){
			$("#lblPhoneMand").hide();
		}
		
		if(!userRegConfig.mobileNo1.userRegVisibilty){
			$("#trMobileNo").hide();
		} else if(!userRegConfig.mobileNo1.mandatory){
			$("#lblMobileMand").hide();
		}
		
		if(!userRegConfig.homeOfficeNo1.userRegVisibilty){
			$("#trHomeOfficeNo").hide();
		} else if(!userRegConfig.homeOfficeNo1.mandatory){
			$("#lblHomeOfficeMand").hide();
		}
		
		if(!userRegConfig.fax1.userRegVisibilty){
			$("#trFaxNo").hide();
		} else if(!userRegConfig.fax1.mandatory){
			$("#lblFaxMand").hide();
		}
	}
	
	//Emergency contact details show/hide
	if(!(userRegConfig.firstName2.userRegVisibilty ||
			userRegConfig.lastName2.userRegVisibilty ||
			userRegConfig.phoneNo2.userRegVisibilty)){
		$("#trEmgnContactInfo").hide();
	} else {
		if(!userRegConfig.title2.userRegVisibilty){
			$("#trEmgnTitle").hide();
		} else if(!userRegConfig.title2.mandatory){
			$("#lblEmgnTitleMand").hide();
		}
		
		if(!userRegConfig.firstName2.userRegVisibilty){
			$("#trEmgnFirstName").hide();
		} else if(!userRegConfig.firstName2.mandatory){
			$("#lblEmgnFirstNameMand").hide();
		}
		
		if(!userRegConfig.lastName2.userRegVisibilty){
			$("#trEmgnLastName").hide();
		} else if(!userRegConfig.lastName2.mandatory){
			$("#lblEmgnLastNameMand").hide();
		}
		
		if(!userRegConfig.phoneNo2.userRegVisibilty){
			$("#emgnPhone1").hide();
			$("#emgnPhone2").hide();
		} else if(!userRegConfig.phoneNo2.mandatory){
			$("#lblEmgnPhoneMand").hide();
		}
		
		if(!userRegConfig.email2.userRegVisibilty){
			$("#trEmgnEmail").hide();
		} else if(!userRegConfig.email2.mandatory){
			$("#lblEmgnEmailMand").hide();
		}
	}
	
	// show/hide preference
	if(!userRegConfig.promotionPref1.userRegVisibilty && !userRegConfig.emailPref1.userRegVisibilty
			&& !userRegConfig.smsPref1.userRegVisibilty && !userRegConfig.languagePref1.userRegVisibilty){
		$("#tblPreference").hide();
	} else {
		if(!userRegConfig.promotionPref1.userRegVisibilty){
			$("#trPromotionPref").hide();
		}
		
		if(!userRegConfig.emailPref1.userRegVisibilty){
			$("#tblEmailPref").hide();
		}
		
		if(!userRegConfig.smsPref1.userRegVisibilty){
			$("#tblSmsPref").hide();
		}
		
		if(!userRegConfig.languagePref1.userRegVisibilty){
			$("#tblLanguagePref").hide();
		}
	}
	// show/hide Contact Details Labels
	if (!userRegConfig.contact_detail_label1.userRegVisibilty){
//	if (!userRegConfig.ibeUserRegContactDetailLabel){
		$(".trPhoneNoHeader").hide();
		$(".trPhoneNoLabel").hide();
	}
	// show/hide Contact Details Labels
	if (!userRegConfig.areaCode1.userRegVisibilty){
//	if (!userRegConfig.ibeUserRegAreaCodeVisible){
		$(".areaCode").hide();
	}
	
}

Util_CustomerValidation.validateZipCode = function(field){
	var valid = "0123456789-";
	var hyphencount = 0;
	
	if (field.length<3 && field.length>10) {
		return false;
	}
	for (var i=0; i < field.length; i++) {
		var temp = "" + field.substring(i, i+1);
		if (temp == "-"){
			hyphencount++;
		}		
		if (valid.indexOf(temp) == "-1") {
			return false;
		}
		if ((hyphencount > 1) || ((field.length==10) && ""+field.charAt(5)!="-")) {
			return false;
		}
	}
	return true;
}

function groupFieldValidation(groupObj, jsonFieldToElementMapping, jsonGroupValidation) {
	var errorHtml = "";
	var rVal = true;
	$.each(groupObj, function(key, fieldList){
		var valid = false;
		$.each(fieldList, function(index, fVal){
			if(trim($(jsonFieldToElementMapping[fVal]).val()) != ""){
				valid = true;
			}
		});
		
		if(!valid){
			errorHtml += "<li> " + jsonGroupValidation;	
			$.each(jsonGroupElementMapping[key],function(index, elem){
				$(elem).addClass("errorControl");
			});
		}
	});
	
	return errorHtml;
}

Util_CustomerValidation.validateUserPhone =  function(fieldCountryCode, fieldAreaCode, fieldPhone){
	/*var lancountryCode = trim($(fieldCountry).val());
	var lanareaCode =    trim($(fieldArea).val());
	var lanno =    trim($(fieldPhone).val());
	var blnLanFound = true;
	var hsaLanNo = false;
	var selectedCountry = trim($("#selCountry").val());	
	if ((selectedCountry != "OT" && selectedCountry != "" ) && lanno != "" && lancountryCode != "") {
		var phoneCC = Util_CustomerValidation.getCCForCountry();
		if(phoneCC != '' && lancountryCode != phoneCC){
			strErrorHTML += "<li> " + errorInfo.wrongComb;
			$(fieldCountry).addClass("errorControl");							
		}else {
			for(var pl=0;pl < arrCountryPhone.length;pl++) {
				if(lancountryCode == arrCountryPhone[pl][0]) {
					hsaLanNo = true;
					if(arrCountryPhone[pl][1] == 'Y'){
						if(typeof(arrAreaPhone[lancountryCode]) != 'undefined') {
							for(var al=0;al < arrAreaPhone[lancountryCode].length; al++){
								if(arrAreaPhone[lancountryCode][al][0] == lanareaCode 
									&& arrAreaPhone[lancountryCode][al][1] == 'LAND'
									&& arrAreaPhone[lancountryCode][al][2] == 'ACT') {
									blnLanFound = true;
									break;									
								} else {
									blnLanFound = false;
								}
							}
							
							if(!blnLanFound) {
								strErrorHTML += "<li> " + errorInfo.wrongComb;
								$(fieldCountry).addClass("errorControl");		
								$(fieldArea).addClass("errorControl");	
							}
						
						}
					
					}
					
					if(arrCountryPhone[pl][2] != ''){
						if(lanno != "") {
							if(lanno.length < Number(arrCountryPhone[pl][2])) {
								strErrorHTML += "<li> " + errorInfo.lenSmall;
								$(fieldPhone).addClass("errorControl");									
							}
						
						}
					
					}
					
					if(arrCountryPhone[pl][3] != ''){
						
						if(lanno != "") {
							if(lanno.length > Number(arrCountryPhone[pl][3])) {
								strErrorHTML += "<li> " + errorInfo.lenLong;
								$(fieldPhone).addClass("errorControl");									
							}
						
						}
					
					
					}
				
				} 
			
			}
			
			if(!hsaLanNo) {
				strErrorHTML += "<li> " + raiseError("ERR050");
				$(fieldCountry).addClass("errorControl");		
				$(fieldArea).addClass("errorControl");	
			}				
		}
	
	} */
	
	
	var strErrorHTML = "";
	var lancountryCode = trim($(fieldCountryCode).val());
	var lanareaCode =    trim($(fieldAreaCode).val());
	var lanno =    trim($(fieldPhone).val());
	var blnLanFound = true;
	var hsaLanNo = false;
	var selectedCountry = trim($("#selCountry").val());
	
	if ((selectedCountry != "OT" && selectedCountry != "" ) && lanno != "" && lancountryCode != "") {
	
		for(var pl=0;pl < arrCountryPhone.length;pl++) {
			if(lancountryCode == arrCountryPhone[pl][0]) {
				hsaLanNo = true;
				if(arrCountryPhone[pl][1] == 'Y' && userRegConfig.areaCode1.userRegVisibilty){
					if(typeof(arrAreaPhone[lancountryCode]) != 'undefined') {
						for(var al=0;al < arrAreaPhone[lancountryCode].length; al++){
							if(arrAreaPhone[lancountryCode][al][0] == lanareaCode 
								&& arrAreaPhone[lancountryCode][al][1] == 'LAND'
								&& arrAreaPhone[lancountryCode][al][2] == 'ACT') {
								blnLanFound = true;
								break;									
							} else {
								blnLanFound = false;
							}
						}
						
						if(!blnLanFound) {
							strErrorHTML += "<li> " + raiseError("ERR050");
							$(fieldCountryCode).addClass("errorControl");		
							$(fieldAreaCode).addClass("errorControl");	
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][2] != ''){
					if(lanno != ""  && userRegConfig.areaCode1.userRegVisibilty) {
						if(lanno.length < Number(arrCountryPhone[pl][2])) {
							strErrorHTML += "<li> " + raiseError("ERR051");
							$(fieldPhone).addClass("errorControl");									
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][3] != ''){
					
					if(lanno != ""  && userRegConfig.areaCode1.userRegVisibilty) {
						if(lanno.length > Number(arrCountryPhone[pl][3])) {
							strErrorHTML += "<li> " + raiseError("ERR052");
							$(fieldPhone).addClass("errorControl");									
						}
					
					}
				
				
				}
			
			} 
		
		}
		
		if(!hsaLanNo) {
			strErrorHTML += "<li> " + raiseError("ERR050");
			$(fieldCountryCode).addClass("errorControl");		
			$(fieldAreaCode).addClass("errorControl");	
		}				
	
	}	
	return strErrorHTML;
}


//check whether mobile num format is correct
Util_CustomerValidation.validateMobileNoFormat = function(fieldCountryCode, fieldAreaCode, fieldMobile){
	var strErrorHTML = "";
	var countryCode = trim($(fieldCountryCode).val());
	var areaCode =    trim($(fieldAreaCode).val());
	var mobileno =    trim($(fieldMobile).val());
	var blnFound = true;
	var hsaNo = false;
	var selectedCountry = trim($("#selCountry").val());			
	
	if ((selectedCountry != "OT" && selectedCountry != "" ) && mobileno != "" && countryCode != "") {
	
		for(var pl=0;pl < arrCountryPhone.length;pl++) {					
			if(countryCode == arrCountryPhone[pl][0]) {
				hsaNo = true;
				if(arrCountryPhone[pl][1] == 'Y' && userRegConfig.areaCode1.userRegVisibilty){
					if(typeof(arrAreaPhone[countryCode]) != 'undefined') {
						for(var al=0;al < arrAreaPhone[countryCode].length; al++){
							if(arrAreaPhone[countryCode][al][0] == areaCode 
								&& arrAreaPhone[countryCode][al][1] == 'MOBILE'
								&& arrAreaPhone[countryCode][al][2] == 'ACT') {
								blnFound = true;
								break;									
							} else {
								blnFound = false;
							}
						}
						
						if(!blnFound) {
							strErrorHTML += "<li> " + raiseError("ERR050");
							$(fieldCountryCode).addClass("errorControl");		
							$(fieldAreaCode).addClass("errorControl");	
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][2] != ''){
					if(mobileno != "") {
						if( userRegConfig.areaCode1.userRegVisibilty ){
							if(mobileno.length < Number(arrCountryPhone[pl][2])) {
								strErrorHTML += "<li> " + raiseError("ERR051");
								$(fieldMobile).addClass("errorControl");									
							}
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][3] != ''){
					
					if(mobileno != "") {
						if( userRegConfig.areaCode1.userRegVisibilty ){
							if(mobileno.length > Number(arrCountryPhone[pl][3])) {
								strErrorHTML += "<li> " + raiseError("ERR052");
								$(fieldMobile).addClass("errorControl");									
							}
						}
					}
				
				
				}
			
			} 
		
		}
		if(!hsaNo) {
			strErrorHTML += "<li> " + raiseError("ERR050");
			$(fieldCountryCode).addClass("errorControl");		
			$(fieldAreaCode).addClass("errorControl");	
		}
						
	
	}
	
	return strErrorHTML;
}

Util_CustomerValidation.lmsHeadRefferedValidate = function(isRegisterUser){
	var data = {};
	var url = "lmsAjaxRegister!validateHeadReffered.action";
	var errorMsg = "";
	data["lmsDetails.headOFEmailId"] = $("#txtEmailHLMS").val();
	data["lmsDetails.refferedEmail"] = $("#txtRefferedEmailLMS").val();
	data["lmsDetails.emailId"] = $("#txtEmail").val();
	data["lmsDetails.firstName"] = $("#hdnFirstNameLMS").val();
	data["lmsDetails.lastName"] = $("#hdnLastNameLMS").val();
	data["registration"] = isRegisterUser;
	data["skipNameMatching"] = !isRegisterUser;
	var response = $.ajax({
		type: "POST", 
		dataType: 'json', 
		data:data,url:url,
		async:false,
		success: function(response){
			Util_CustomerValidation.lmsValidationData = response.lmsDetails;
			Util_CustomerValidation.ibeUserExists = response.ibeAccountExists;
			errorMsg = response.messageTxt;
		},
		error: function(response){
			Util_CustomerValidation.lmsValidationData = null;
		}
	});
	
	return errorMsg;
}

Util_CustomerValidation.saveFamilyMemberValidate = function(){
	
	if($("#FMFirstName").val() == ""){
		strErrorHTML += "<li> " + errorInfo.FMFirstNameReq;
		$("#FMFirstName").addClass("errorControl");
		return false;
	}
	
	if($("#FMLastName").val() == ""){
		strErrorHTML += "<li> " + errorInfo.FMLastNameReq;
		$("#FMLastName").addClass("errorControl");
		return false;
	}
	
	if($("#FMRelationship").val() == ""){
		strErrorHTML += "<li> " + errorInfo.FMRelationshipReq;
		$("#FMRelationship").addClass("errorControl");
		return false;
	}
	
	if($("#FMNationality").val() == ""){
		strErrorHTML += "<li> " + errorInfo.FMNationalityReq;
		$("#FMNationality").addClass("errorControl");
		return false;
	}
	
	return true;
}
