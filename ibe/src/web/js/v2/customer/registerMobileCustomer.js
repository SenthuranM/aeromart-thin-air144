/**
 * Author : Chethiya Palliyaguruge
 * 
 */
function UI_Mobile_RegCustomer(){}


$(document).ready(function() {
	UI_Mobile_RegCustomer.ready();
}); 
/**
 * Page Ready
 *
 */

UI_Mobile_RegCustomer.isBecomeLmsUser = false;

UI_Mobile_RegCustomer.headFfidReq = false;

UI_Mobile_RegCustomer.lmsAvailable = false;

UI_Mobile_RegCustomer.lmsNameMatch = false;

UI_Mobile_RegCustomer.secureIBEUrl = "";

UI_Mobile_RegCustomer.paxAgeValidations = {};

UI_Mobile_RegCustomer.countryCodeLength = 0;

UI_Mobile_RegCustomer.translations = null;

var errorInfo = "";
var arrPaxTitle = new Array();
var arrCountryPhone = new Array();

UI_Mobile_RegCustomer.ready = function() {
	UI_Mobile_RegCustomer.loadCustomerRegisterData();
	$("#txtFirstName").alphaWhiteSpace();
	$("#txtLastLame").alphaWhiteSpace();
	$("#txtLMSPPN").alphaNumeric();
	$("#txtMobile").numbericHiphen();
	
	$("#submitRegistration").click(function (){
		if($('#optLMS').prop('checked')){
			UI_Mobile_RegCustomer.lmsHeadRefferedValidate();
		} else {
			UI_Mobile_RegCustomer.customerRegister();
		}
	});
	
	$("#selCountry").select(function (){
		UI_Mobile_RegCustomer.countryListChange();
	});
	
	$("#mergeOk").click(function (){
		UI_Mobile_RegCustomer.checkName();
	});
	
	$("#contMyAA").click(function (){
		UI_Mobile_RegCustomer.loadSignIn();
	});
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });

    
//	  Auto Population of the country code
    $("#selCountry").change(function (){
    	UI_Mobile_RegCustomer.populateCountryCode();
    });
    
    $('#optLMS').change(function (){
    	if($('#optLMS').prop('checked')){
    		$('#lmsForm').show();
    	} else {
    		$('#lmsForm').hide();
    		$("#txtLMSDOB").val("");
    		$("#txtLMSPPN").val("");
    	}
    });
    
    $("#txtEmail,#password,#confirmPassword,#txtFirstName,#txtLastLame,#txtMobile,#txtLMSDOB,#txtLMSPPN").css("color", "#555");
	
    $("#selTitle").change(function () {
		if ($("#selTitle").val()!="") {
			$("#selTitle").css("color", "#555");
		} else {
			$("#selTitle").css("color", "#999");
		}
	});
	
    $("#selNationality").change(function () {
		if ($("#selNationality").val()!="") {
			$("#selNationality").css("color", "#555");
		} else {
			$("#selNationality").css("color", "#999");
		}
	});
	
    $("#selCountry").change(function () {
		if ($("#selCountry").val()!="") {
			$("#selCountry").css("color", "#555");
		} else {
			$("#selCountry").css("color", "#999");
		}
	});

	var lang = getLangParameter() || 'en';

	UI_Mobile_RegCustomer.translatePage(lang);
};

/**
 * Get query parameter
 */
function getParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getLangParameter() {
	var langCode = getParameterByName('hdnParamData');
	if (langCode != null) {
		langCode = langCode.split('^')[0].toLowerCase();
	} else {
		langCode = 'en';
	}
	return langCode;
}

/**
 * Translate Page elements
 */
UI_Mobile_RegCustomer.translatePage = function(lang){
	var translations;

	if(lang == 'ar' || lang == 'fa'){
		$('#regContent,#successContent').css('direction', 'rtl');
	}else{
		$('#regContent,#successContent').css('direction', 'ltr');
	}

	//Get translations form json
	$.get('../js/v2/customer/i18n/'+ lang +'.json', function(data){
		UI_Mobile_RegCustomer.translations = JSON.parse(data);

		translations = JSON.parse(data);
		$.i18n.load(translations);

		var placeholders = $("[data-i18n-ph]");
		var labels = $("[data-i18n]");

		placeholders.each(function(index, item){
			var element = $(item);

			var translationString = element.attr('data-i18n-ph');
			var translation = $.i18n._(translationString);

			if(translationString != translation){
				element.prop({ placeholder: translation })
			}
		});

		labels.each(function(index, item){
			var element = $(item);
			var translationString = element.attr('data-i18n');

			if(translationString != $.i18n._(translationString)){
				element._t(translationString);
			}
		});
	});
};

/**
 * Get translations for a given key
 */
UI_Mobile_RegCustomer.getTranslations = function(key){
	var translationString = key;
	var translation = $.i18n._(translationString);

	if(translationString != translation){
		return translation;
	}
};

/**
 * Helper function to get validation message from key
 */
UI_Mobile_RegCustomer.getValidationTranslation = function(key){
	var validationKey = 'MobileLoyaltyReg_' + key;
	return UI_Mobile_RegCustomer.getTranslations(validationKey)
};

/**
 * Request Page data
 */
UI_Mobile_RegCustomer.loadCustomerRegisterData = function() {
	$.ajax({
		url : 'showMobileCustomerDetail.action',
		type : "POST",
		dataType : "json",
		beforeSend:UI_Mobile_RegCustomer.showLoadingProgress,
		success : UI_Mobile_RegCustomer.loadCustomerRegisterDataSucess,		
		error : function(response) {
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}

	});
	return false;
}


UI_Mobile_RegCustomer.loadCustomerRegisterDataSucess =  function(response) {
	if(response != null && response.success == true) {
		$("#selCountry").fillDropDown({dataArray:response.countryInfo, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selNationality").fillDropDown({dataArray:response.nationalityInfo, keyIndex:0, valueIndex:1, firstEmpty:false});
		errorInfo = response.errorInfo;
		UI_Mobile_RegCustomer.secureIBEUrl = response.secureIBEUrl;
		UI_Mobile_RegCustomer.buildTelephoneInfo(response.countryPhoneInfo);
		UI_Mobile_RegCustomer.paxAgeValidations = response.paxValidationTO;
		
		var date = new Date();
        var currentMonth = date.getMonth();
        var currentDate = date.getDate();
        var currentYear = date.getFullYear();
        var maxYear = currentYear-UI_Mobile_RegCustomer.paxAgeValidations.childAgeCutOverYears;
        var minYear = currentYear-UI_Mobile_RegCustomer.paxAgeValidations.adultAgeCutOverYears;

        var dateOfBirth = $("#txtLMSDOB").datepicker({
        	autoclose: true,
        	startView: 'decade',
        	format: 'dd/mm/yyyy',
        }).on('changeDate', function (ev) {
            //close when viewMode='0' (days)
            if(ev.viewMode === 'days'){
                $('#txtLMSDOB').datepicker('hide');
            }
        });
        $("#txtLMSDOB").datepicker('setStartDate', new Date(minYear, currentMonth, currentDate));
        $("#txtLMSDOB").datepicker('setEndDate', new Date(maxYear, currentMonth, currentDate));
        
		if(response.loyaltyManagmentEnabled){
			$("#lmsDetails").show();
		} else {
			$("#lmsDetails").hide();
		}
		try {
			eval(response.titleInfo);
			$("#selTitle").fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:false});
		} catch (e) {
			console.log(e);
		}
		
		
		UI_Mobile_RegCustomer.hideLoadingProgress();
	}
}

/**
 *Validate Information 
 */

UI_Mobile_RegCustomer.validateInfo = function(){
	
	var email = $("#txtEmail").val().trim();
	var password = $("#password").val();
	var confPassword = $("#confirmPassword").val();
	var title = $("#selTitle").val();
	var firstName = $("#txtFirstName").val().trim();
	var lastName = $("#txtLastLame").val().trim();
	var nationality = $("#selNationality").val();
	var country = $("#selCountry").val();
	var mobile = $("#txtMobile").val().trim();
	var dob = $("#txtLMSDOB").val();
	var ppn = $("#txtLMSPPN").val().trim();
	
	var valid = true;
	$('#joinLMS').val(false);
	$("#accountAlert").removeClass("alert-danger");
	$("#detailsAlert").removeClass("alert-danger");
	$("#lmsAlert").removeClass("alert-danger");
	$("#accountAlert").empty();
	$("#detailsAlert").empty();
	$("#lmsAlert").empty();
	
	if( email == "" || email == null) {
		var errorMessage = UI_Mobile_RegCustomer.getValidationTranslation('userEmailRqrd') || errorInfo.userEmailRqrd;
		UI_Mobile_RegCustomer.alertError('userEmailRqrd', '#accountAlert');

		$("#txtEmail").addClass("input-alert-danger");
		valid = false;
	} else if(!checkEmail(email)){
		UI_Mobile_RegCustomer.alertError('userEmailIncorrect', '#accountAlert');
		$("#txtEmail").addClass("input-alert-danger");
		valid = false;
	} else {
		$("#txtEmail").removeClass("input-alert-danger");
		$("#lmsEmailId").val(email);
	}
	
	if( password == "" || password == null) {
		UI_Mobile_RegCustomer.alertError('userPasswordRqrd', '#accountAlert');
		$("#password").addClass("input-alert-danger");
		valid = false;
	} else if (password.length < 6) {
		UI_Mobile_RegCustomer.alertError('userPasswordMax', '#accountAlert');
		$("#password").addClass("input-alert-danger");
		valid = false;
	} else if(UI_Mobile_RegCustomer.hasSpecialCharactrs(password)){
		UI_Mobile_RegCustomer.alertError('passwordIncorrect', '#accountAlert');
		$("#password").addClass("input-alert-danger");
		valid = false;
	} else if( confPassword == "" || password == null) {
		UI_Mobile_RegCustomer.alertError('userConfPasswordRqrd', '#accountAlert');
		$("#confirmPassword").addClass("input-alert-danger");
		valid = false;
	} else if( confPassword != password ){
		UI_Mobile_RegCustomer.alertError('userPasswordsNotMatch', '#accountAlert');
		$("#confirmPassword").addClass("input-alert-danger");
		valid = false;
	} else {
		$("#confirmPassword").removeClass("input-alert-danger");
		$("#password").removeClass("input-alert-danger");
	}
	
	
	if( title == "" || title == null) {
		UI_Mobile_RegCustomer.alertError('titleRequired', '#detailsAlert');
		$("#selTitle").addClass("input-alert-danger");
		valid = false;
	} else {
		$("#lmsGenderCode").val(title);
		$("#selTitle").removeClass("input-alert-danger");
	}
	
	if( firstName == "" || firstName == null) {
		UI_Mobile_RegCustomer.alertError('userFirstNameRqrd', '#detailsAlert');
		$("#txtFirstName").addClass("input-alert-danger");
		valid = false;
	} else if (!isAlphaWhiteSpace(firstName)) {
		UI_Mobile_RegCustomer.alertError('userFirstNameAlpha', '#detailsAlert');
		$("#txtFirstName").addClass("input-alert-danger");
		valid = false;
	} else {
		$("#lmsFirstName").val(firstName);
		$("#txtFirstName").removeClass("input-alert-danger");
	}
	
	if( lastName == "" || lastName == null) {
		UI_Mobile_RegCustomer.alertError('userLastNameRqrd', '#detailsAlert');
		$("#txtLastLame").addClass("input-alert-danger");
		valid = false;
	}else if (!isAlphaWhiteSpace(lastName)) {
		UI_Mobile_RegCustomer.alertError('userLastNameAlpha', '#detailsAlert');
		$("#txtLastLame").addClass("input-alert-danger");
		valid = false;		
	} else {
		$("#lmsLastName").val(lastName);
		$("#txtLastLame").removeClass("input-alert-danger");
	}
	
	if( nationality == "" || nationality == null) {
		UI_Mobile_RegCustomer.alertError('userNationalityRqrd', '#detailsAlert');
		$("#selNationality").addClass("input-alert-danger");
		valid = false;
	} else {
		$("#lmsNationalityCode").val(nationality);
		$("#selNationality").removeClass("input-alert-danger");
	}
	
	if( country == "" || country == null) {
		UI_Mobile_RegCustomer.alertError('userCountryRqrd', '#detailsAlert');
		$("#selCountry").addClass("input-alert-danger");
		valid = false;
	} else {
		$("#lmsResidencyCode").val(country);
		$("#selCountry").removeClass("input-alert-danger");
	}
	
	if( mobile == "" || mobile == null) {
		UI_Mobile_RegCustomer.alertError('userMobileRqrd', '#detailsAlert');
		$("#txtMobile").addClass("input-alert-danger");
		valid = false;
	} else {
		$("#lmsMobileNumber").val(mobile);
		$("#txtMobile").removeClass("input-alert-danger");
	}
	
	
	UI_Mobile_RegCustomer.populateMobileNumber();
//	else if(!UI_Mobile_RegCustomer.validateMobile()){
//		UI_Mobile_RegCustomer.alertError(errorInfo.userMobileFormatIncorrect, '#detailsAlert');
//		$("#txtMobile").addClass("input-alert-danger");
//		valid = false;
//	} else {
//		$("#txtMobile").removeClass("input-alert-danger");
//	}
	
	$("#txtLMSDOB").removeClass("input-alert-danger");
	$("#txtLMSPPN").removeClass("input-alert-danger");

	if($('#optLMS').prop('checked')){
		
		$('#joinLMS').val(true);
		if( dob == "" || dob == null) {
			UI_Mobile_RegCustomer.alertError('userDateOfBirthRqrd', '#lmsAlert');
			$("#txtLMSDOB").addClass("input-alert-danger");
			valid = false;
		}
		
		if( ppn == "" || ppn == null) {
			UI_Mobile_RegCustomer.alertError("lblEnterPassport", '#lmsAlert');
			$("#txtLMSPPN").addClass("input-alert-danger");
			valid = false;
		} else if (!isAlphaNumeric(ppn)) {
			UI_Mobile_RegCustomer.alertError("userPassportAlpha", '#lmsAlert');
			$("#txtLMSPPN").addClass("input-alert-danger");
			valid = false;
		}		
	}	
	return valid;
}

/**
 * Register User
 */
UI_Mobile_RegCustomer.customerRegister = function(){
	if (UI_Mobile_RegCustomer.validateInfo()){
		
		$("#customerRegForm").ajaxSubmit({
			url:"registerMobileCustomer.action", 
			dataType: 'json',
			beforeSend:UI_Mobile_RegCustomer.showLoadingProgress,
			success: UI_Mobile_RegCustomer.customerRegisterSuccess, 
			error:UI_commonSystem.setErrorStatus
		});
	}
}


UI_Mobile_RegCustomer.customerRegisterSuccess = function(response) {
	if (response.registeredCustomer){
		UI_Mobile_RegCustomer.alertError('userEmailRegistered', '#accountAlert');
		$("#txtEmail").addClass("input-alert-danger");
	} else {
		$("#successContent").show();
		$("#regContent").hide();
		if (response.joinLMS) {
			$("#cnfEmailMsg").show();
		}
	}
	UI_Mobile_RegCustomer.hideLoadingProgress();
	
}

UI_Mobile_RegCustomer.alertError = function (errorKey, field) {
	var error = UI_Mobile_RegCustomer.getValidationTranslation(errorKey);

	$(field).append('<li>' + error);
	$(field).addClass("alert-danger");
}

UI_Mobile_RegCustomer.hasSpecialCharactrs = function(str){
	var validated=true;
	var chars= new Array('>','<' ,'"',"'",'/','^','|') ;
	var len = str.length;
	var val = removeInvalids(str);
	if( val == str){
		return false;
	}
	return validated;
}


UI_Mobile_RegCustomer.buildTelephoneInfo = function(strCountryPhone) {		
	if (strCountryPhone != null) {		
		var arrCountryPhoneLocal = strCountryPhone.split("^");
		for (var i = 0; i < arrCountryPhoneLocal.length; i++) {
			arrCountryPhone[i] = arrCountryPhoneLocal[i].split(",");
		}
	}	
}

UI_Mobile_RegCustomer.countryListChange = function() {
	if($("#txtMobile").val().trim() == "")	{
		var choseCountry = $("#selCountry").val();
		for(var x=0;x<arrCountryPhone.length;x++) {
			if(choseCountry == arrCountryPhone[x][4]) {
				$("#txtMobile").val(arrCountryPhone[x][0]);
				break;
			}
		}	
	}
}

UI_Mobile_RegCustomer.lmsHeadRefferedValidate = function(isRegisterUser){
	if(UI_Mobile_RegCustomer.validateInfo()){
		var data = {};
		var url = "registerMobileCustomer!validateHeadReffered.action";
		data["lmsDetails.emailId"] = $("#txtEmail").val().trim();
		data["lmsDetails.firstName"] = $("#txtFirstName").val().trim();
		data["lmsDetails.lastName"] = $("#txtLastLame").val().trim();
		data["lmsDetails.nationalityCode"] = $("#selNationality").val();
		data["lmsDetails.residencyCode"] = $("#selCountry").val();
		data["skipNameMatching"] = false;
		if ($("#optLMS").is(":checked")) {		
			data['joinLMS'] = true;		
		} else {
			data['joinLMS'] = false;		
		}
		var response = $.ajax({
			type: "POST", 
			dataType: 'json',
			data:data,
			beforeSend:UI_Mobile_RegCustomer.showLoadingProgress,
			url:url,
			async:false,
			success: function(response){
				UI_Mobile_RegCustomer.lmsAvailable = response.lmsAvailable;
				UI_Mobile_RegCustomer.lmsNameMatch = response.lmsNameMatch;
				UI_Mobile_RegCustomer.registration();
			},
		});
	}	
}

UI_Mobile_RegCustomer.registration = function () {
	UI_Mobile_RegCustomer.hideLoadingProgress();
	if (UI_Mobile_RegCustomer.lmsAvailable) {
		$('#lmsMergeModal').modal('show');
	} else {
		UI_Mobile_RegCustomer.customerRegister();
	}	
}

UI_Mobile_RegCustomer.checkName = function () {
	$('#lmsMergeModal').modal('hide');
	if (!UI_Mobile_RegCustomer.lmsNameMatch) {
		$('#nameModal').modal('show');
	} else {
		UI_Mobile_RegCustomer.customerRegister();
	}
}

UI_Mobile_RegCustomer.validateMobile = function () {
	var mobileNo = $("#txtMobile").val().trim();
	var mobileArray = mobileNo.split('-');
	if (mobileArray.length != 3) {
		return false;
	} else if (mobileArray[0].lenght < 1) {
		return false;
	} else if (mobileArray[1].lenght < 2) {
		return false;
	} else if (mobileArray[2].lenght < 4) {
		return false;
	}
	return true;
}

UI_Mobile_RegCustomer.populateCountryCode = function() {
	for (var i = 0 ; i < arrCountryPhone.length ; i++){
		if (arrCountryPhone[i][4] == $("#selCountry").val()) {
			var countryCodeString = arrCountryPhone[i][0];
			if ($("#txtMobile").val()=="" || $("#txtMobile").val()==null) {
				$("#txtMobile").val(countryCodeString);
				UI_Mobile_RegCustomer.countryCodeLength = countryCodeString.length;
			} else {
				var mobileString = $("#txtMobile").val();
				mobileString = countryCodeString + mobileString.substring(UI_Mobile_RegCustomer.countryCodeLength);
				UI_Mobile_RegCustomer.countryCodeLength = countryCodeString.length;
				$("#txtMobile").val(mobileString);
			}
			break;
		}	
	}
}

UI_Mobile_RegCustomer.populateMobileNumber = function () {
	var mobileString = $("#txtMobile").val(); 
	var countryCode = mobileString.substring(0,UI_Mobile_RegCustomer.countryCodeLength);
	var areaCode = mobileString.substring(UI_Mobile_RegCustomer.countryCodeLength, UI_Mobile_RegCustomer.countryCodeLength+2);
	var mobileNumber = mobileString.substring(UI_Mobile_RegCustomer.countryCodeLength+2);
	var fullNumber = countryCode + "-" + areaCode + "-" + mobileNumber;
	$("#customerMobile").val(fullNumber);
}


UI_Mobile_RegCustomer.loadSignIn = function() {
	var formHtml =  "<form id='dynamicForm' method='post'> <input type='hidden' name='hdnParamData' value=EN^SI></form>";
	$("body").append(formHtml);
	$("#dynamicForm").attr("action", UI_Mobile_RegCustomer.secureIBEUrl + "showReservation.action");
	
	$("#dynamicForm").submit();	
}

///Repositioning model dialog pop-up to center

function reposition() {
    var modal = $(this),
        dialog = modal.find('.modal-dialog');
    modal.css('display', 'block');
    
    // Dividing by two centers the modal exactly, but dividing by three 
    // or four works better for larger screens.
    dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
}

UI_Mobile_RegCustomer.showLoadingProgress = function(){
    $('#bar').show();
}

UI_Mobile_RegCustomer.hideLoadingProgress = function(){
    $('#bar').hide();
}


