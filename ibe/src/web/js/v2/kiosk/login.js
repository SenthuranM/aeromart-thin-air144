/** Kiosk Login
 * 
 * @author Pradeep Karunanayake
 *  
 */

function UI_KioskLogin(){};
	
;(function(UI_KioskLogin) {	
	
	UI_KioskLogin.errorInfo = "";
	UI_KioskLogin.ibeSecureURL= "";
	UI_KioskLogin.ibeNonSecureURL= "";
	UI_KioskLogin.reOpen = false;
	
	$(document).ready(function() {
		UI_KioskLogin.checkWindowOpenStatus();
		if (window.name == "winIBE") {
			$("#btnLogin").click(function() { UI_KioskLogin.loginClick(); });
			$("#btnCancel").click(function() { UI_KioskLogin.closeWindow(); });		
			UI_KioskLogin.ready();
		}
	});
	
	UI_KioskLogin.ready = function() {
		UI_KioskLogin.loadPageData();
	}
	
	UI_KioskLogin.checkWindowOpenStatus = function() {
		if(window.name !== "winIBE"){
			$("body").hide();			
			UI_KioskLogin.reOpen = true;
			UI_KioskLogin.fullScreen();
		}
	}
	
	UI_KioskLogin.fullScreen = function() {		
		//var strFileName = UI_KioskLogin.ibeSecureURL + "showLoadPage!kioskLogin.action";
		var strFileName = UI_KioskLogin.ibeSecureURL + "showReservation.action?hdnParamData=EN^KOL";	
		window.open(strFileName, 'winIBE', 'fullscreen=yes, scrollbars=yes');
	}
	
	UI_KioskLogin.loadPageData = function() {	
	    $("#frmKisokLogin").attr('action', 'kioskLogin.action');
		$("#frmKisokLogin").ajaxSubmit({dataType: 'json', success: UI_KioskLogin.loadPageDataProcess, error:UI_commonSystem.setErrorStatus});	
		return false;	
	}
	
	UI_KioskLogin.loadPageDataProcess = function(response){		
		if(response != null && response.success == true) {	
			var carrier = response.carrierName;
			$("#carrierName").text(carrier);
			UI_KioskLogin.errorInfo = response.errorInfo;
			UI_KioskLogin.ibeSecureURL = response.ibeSecureURL;
			UI_KioskLogin.ibeNonSecureURL = response.ibeNonSecureURL;
			document.title = " : : " + carrier;	
			UI_KioskLogin.reSubmitHttp(UI_commonSystem.checkHttps());
		} else {
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}
		UI_KioskLogin.loadingCompleted();
	}
	
	UI_KioskLogin.loadingProgress = function(){	
		$("#divLoadMsg").show();
	}

	UI_KioskLogin.loadingCompleted = function(){
		$("#divLoadMsg").hide();	
	}
	
	UI_KioskLogin.loginClick = function() {
		if (UI_KioskLogin.validate()) {			
			$("#frmKisokLogin").ajaxSubmit({url:UI_KioskLogin.ibeSecureURL + 'kioskLogin.action?mode=login' , beforeSubmit:UI_KioskLogin.loadingProgress, dataType: 'json', success: UI_KioskLogin.loginDataProcess, error:UI_commonSystem.setErrorStatus});		
			return false;
		} 
	}
	
	UI_KioskLogin.loginDataProcess = function(response) {		
		var msgText = response.messageTxt;
		var errorMsgs = UI_KioskLogin.errorInfo;
		if(response != null && response.success == true) {			
			if (msgText != null && $.trim(msgText) != "") {
				if (msgText == "LOGINPRIV") {
					jAlert(errorMsgs.userInvalidUserIdPassword);
				} else if (msgText == "Error") {
					jAlert(errorMsgs.userNoPrivilege);
				}
			} else {
				$("#hdnParamData").val("EN^KOS");
				$("#frmKisokLogin").attr("action", UI_KioskLogin.ibeSecureURL + "showReservation.action");
				$("#dynamicForm").attr("target", "_top");	
				$("#frmKisokLogin").submit();
			}
		} else {
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}
		UI_KioskLogin.loadingCompleted();
	}
	
	UI_KioskLogin.validate = function(){
		
		var username = $("#username").val();
		var password = $("#password").val();
		var errorMsgs = UI_KioskLogin.errorInfo;
		
		if ($.trim(username).length == 0) {
			jAlert(errorMsgs.userInvalidID);
			$("#username").focus();
			return false;	
		}
		
		if ($.trim(password).length == 0) {
			jAlert(errorMsgs.userInvalidPassword);
			$("#password").focus();
			return false;	
		}		
		$("#username").val(username.toUpperCase());
		
		return true;
	}
	
	UI_KioskLogin.closeWindow = function() {
		window.close();		
	}
	
	UI_KioskLogin.reSubmitHttp = function(isHttps) {
		if (isHttps == false) {		
			var strSubmitUrl = UI_KioskLogin.ibeSecureURL + 'showReservation.action?hdnParamData=en^KOS';
			location.replace(strSubmitUrl);
			UI_commonSystem.loadingProgress();
		}
	}
		
	
})(UI_KioskLogin);