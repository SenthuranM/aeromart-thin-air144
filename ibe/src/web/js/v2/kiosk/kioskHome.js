/** Kiosk Home Page
 * 
 * @author Pradeep Karunanayake
 *  
 */

function UI_KioskHome(){};
	
;(function(UI_KioskHome) {
	
	UI_KioskHome.language = "en";
	UI_KioskHome.ibeSecureURL = "";
	$(document).ready(function() {
			
		UI_KioskHome.ready();		
	});
	
	UI_KioskHome.ready = function() {	
		$("#signIn").click(function() { UI_KioskHome.systemLinkClick(1); });
		$("#register").click(function() { UI_KioskHome.systemLinkClick(2); });	
		
		$("#destinations").click(function() { UI_KioskHome.promoButtonClick(0); });	
		$("#baggage").click(function() { UI_KioskHome.promoButtonClick(1); });	
		$("#bookHotel").click(function() { UI_KioskHome.promoButtonClick(2); });	
		$("#rentCar").click(function() { UI_KioskHome.promoButtonClick(3); });	
		$(".lanuage").click(function (){UI_KioskHome.languageClick($(this).text())})
		UI_KioskHome.loadPageData();
	}
	
	UI_KioskHome.loadPageData = function(){
		$("#submitFrom").attr('action', 'kioskHome.action');
		$("#submitFrom").ajaxSubmit({dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success: UI_KioskHome.loadPageDataProcess, error:UI_commonSystem.setErrorStatus});
		return false;
	}
	
	setInterval('UI_KioskHome.loadPageData()',500000);
	
	
	UI_KioskHome.loadPageDataProcess = function(response){		
		if(response != null && response.success == true) {	
			UI_KioskHome.ibeSecureURL = response.ibeSecureURL;
			UI_KioskHome.language = response.language;
			var carrier = response.carrierName;
			document.title = " : : " + carrier;
			$("#divLoadBg").populateLanguage({messageList:response.jsonLabel});
			UI_KioskHome.errorInfo = response.errorInfo;
			var welcomeMsg = "Welcome to "+carrier+" Kiosk";
			$("#KioskLable").html(welcomeMsg);
			if(response.regUserAccessInKiosk == "N"){
				$("#signIn").parent().hide();
			}
			UI_FlightSearch.pageInit(response);	
		} else {
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}
		UI_commonSystem.loadingCompleted();		
	}
	
	UI_KioskHome.loadingProgress = function(){	
		$("#divLoadMsg").show();
	}

	UI_KioskHome.loadingCompleted = function(){
		$("#divLoadMsg").hide();	
	}
	
	UI_KioskHome.systemLinkClick = function(intID) {
		var lan = UI_KioskHome.language;
		$("#submitFrom").attr("action" , UI_KioskHome.ibeSecureURL + "showReservation.action");	
		$("#submitFrom").attr("target", "_top");
		
		switch (intID){
			case 1 : 				
				var strPostData =  lan + "^SI"
				$("#hdnParamData").val(strPostData);								
				$("#submitFrom").submit();
				break;
			case 2 :				
				var strPostData =  lan + "^RE";
				$("#hdnParamData").val(strPostData);								
				$("#submitFrom").submit();
				break;
		}

	}
	UI_KioskHome.languageClick = function(lang){
		var ln = (String(lang).toUpperCase() == "ARABIC") ? "AR" : "EN";
		UI_KioskHome.language = ln;
		$("#hdnParamData").val(ln+"^KOS");
		$("#submitFrom").attr("action", UI_KioskHome.ibeSecureURL + "showReservation.action");
		$("#submitFrom").attr("target", "_top");	
		$("#submitFrom").submit();
	}
	
	UI_KioskHome.promoButtonClick = function(intID) {
		var objWindow;
		var props = 'height=700,width=940,scrollbars=yes,top=5,left=10';
		var  language = $("#locale").val();
		var lowLang = (language.toLowerCase() == "en") ? "/" : "ar/";
		if(language == "AR") {			
			props = 'height=700,width=550,scrollbars=yes,top=5,left=450';
		}
		
		switch (intID){
			case 0 :
							
				objWindow = window.open('http://www.airarabia.com/'+lowLang+'destination-map','hoteWin', props);
				break;
				
			case 1 :
							
				objWindow = window.open('http://www.airarabia.com/'+lowLang+'baggage', 'hoteWin', props);							
				break;
				
			case 2 :			
				var firststr=language.substr(0,1);					
				objWindow = window.open('http://www.octopustravel.ae/airarabia/','hoteWin', props);				
				break;
		
			case 3 :
				
				objWindow = window.open('http://www.holidayautos.ae/cgi-bin/liveweb.sh/QSearch.w?ctryref=MEA&lang=ME&aff=airarabiauae','hoteWin', props);		
				break;
		}		
	}	
		
	
})(UI_KioskHome);