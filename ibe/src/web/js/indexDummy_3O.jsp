<%@ page language="java"%>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Air Arabia Maroc - TEST PAGE</title>
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
<meta name=ProgId content=VisualStudio.HTML>
<meta name=Originator content="Microsoft Visual Studio .NET 7.1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<%! 
String url = AppParamUtil.getSecureIBEUrl() + "showReservation.action";
String kioskUrl = AppParamUtil.getNonsecureIBEUrl()+"showKiosk.action";

%>
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
</head>
<body style="background-image: url(../images/Testing_no_cache.jpg);">
<table width="775" cellpadding="2" cellspacing="0" border="0" align="center">
	<tr>
		<td width="180">
			<!-- Flight Search Goes Here -->
			<table width='100%' border='0' cellpadding='0' cellspacing='2' ID='Table7' bgcolor="white">
				<tr>
					<td colspan='4'><font><b>Book Flights</b></font></td>
					<td> <font><b><a href='javascript:void(0)' onclick='flightSearchBtnOnClick(7)'>MultiCity Search</a></b></font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selFromLoc' size='1' style='width:175px;' NAME='selFromLoc' onchange='selFromLocOnChange()' class='selectBox'>

						</select>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selToLoc' size='1' style='width:175px;' NAME='selToLoc' onchange='selToLocOnChange()' class='selectBox'>
						</select>
					</td>

				</tr>
				<tr>
					<td colspan='4'><font>Departing Date</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
							<tr>

								<td width="95%">
									<select id='selDtDept' size='1' style='width:50px;' NAME='selDtDept' onChange="selDtDeptOnChange()">
										<option></option>
									</select>
									<select id='selYrDept' size='1' style='width:100px;' NAME='selYrDept' onChange="selYrDeptOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>

									<a href='javascript:void(0)' onclick='LoadCalendar(0,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selDVariance' size='1' style='width:50px;' NAME='selDVariance' title='No of days before or after departure date'>

						</select>
						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td><font>Returning Date</font></td>
					<td colspan="3" align='right'>

						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td align='right'>
									<font>&nbsp;Return Trip&nbsp;</font>
								</td>
								<td align='right'>
									<input type="checkbox" id="chkReturnTrip" name="chkReturnTrip" onClick="chkReturnTrip_click()" class="noBorder">
								</td>

							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table3">
							<tr>
								<td width="95%">

									<select id='selDtRetu' size='1' style='width:50px;' NAME='selDtRetu' onChange="selDtRetuOnChange()">
										<option></option>
									</select>
									<select id='selYrRetu' size='1' style='width:100px;' NAME='selYrRetu' onChange="selYrRetuOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>
									<a href='javascript:void(0)' onclick='LoadCalendar(1,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>

								</td>
							</tr>
						</table>
					</td>
				</tr>		
				<tr>
					<td colspan='4'>
						<select id='selRVariance' size='1' style='width:50px;' NAME='selRVariance' title='No of days before or after return date'>
						</select>

						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' ID='Table8'>
							<tr>

								<td width='30%'><font>Adults</font></td>
								<td width='33%'><font>Children</font></td>
								<td width='37%'><font>Infants&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
							</tr>
							<tr>
								<td><font>&nbsp;</font></td>
								<td><font class='fntsmall'>(2-14 years)</font></td>

								<td><font class='fntsmall'>(under 2 years)</font></td>
							</tr>
							<tr>
								<td>
									<select id='selAdults' size='1' style='width:45px;' NAME='selAdults' onChange='selAdults_onChange()'>
									</select>														
								</td>			
								<td>
									<select id='selChild' size='1' style='width:50px;' NAME='selChild'>

									</select>														
								</td>
								<td align='right'>
									<select id='selInfants' size='1' style='width:65px;' NAME='selInfants'>
									</select>														
								</td>			
							</tr>	
						</table>
					</td>
				</tr>
				<tr>

					<td colspan='4'>
						<a href="#" onClick="faqClick()"><font class='fntsmall'><u>Important information on Child and Infant bookings</u></font></a>
					</td>
				</tr>
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				<tr>

					<td colspan='4'><font>Currency</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selCurrency' size='1' style='width:175px;' NAME='selCurrency'>
						</select>
					</td>
				</tr>
				
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				<tr>

					<td colspan='4'><font>Origin Country</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selOriginCountry' size='1' style='width:175px;' NAME='selOriginCountry'>
						</select>
					</td>
				</tr>

				<tr>
					<td colspan='4' align='right'>
						<input type='button' id='btnSearch' class='Button' value='Search' title='Click here to Search for flights' NAME='btnSearch' onclick='flightSearchBtnOnClick(0)'>
					</td>
				</tr>
			</table>
		</td>
		<td rowspan="3" valign="top" halign="left"><table width="200px"><tr>
		<td>&nbsp;<a href="<%=kioskUrl%>"><font class="fntBold fntRed"><blink>Test Kiosk</blink></font></a><br></td>
		 	
			</tr>
			<tr><td><br><font class="fntBold">Configurations</font></td></tr>
				<tr> 
					<td align="left">
						<font>Load Configs from DB : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isUsingDBConfigs()%></font>
					</td></tr>
				<tr> 
						<td align="left">
							<font>Default PGW Currency: </font>
						</td>
						<td align="right">
							<font><%=AppParamUtil.getDefaultPGW()%></font>
						</td></tr>
				<tr> 
					<td align="left">
						<font>Apply CC Charge: </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isApplyCreditCardCharge()%></font>
					</td></tr>
					<tr> 
					<td align="left">
						<font>Show Travel Guard : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isTravelGuardEnabled()%></font>
					</td></tr>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="btn1" value="Register" class='Button' onclick='flightSearchBtnOnClick(1)'>
			<input type="button" id="btnMobileReg" value="Register Mobile" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(8)'>
		</td>
	</tr>
	<tr>
		<td>			 
			<input type="button" id="btnAgent" value="RegisterAgent" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(3)'>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="Button1" value="Sign In" class='Button' onclick='flightSearchBtnOnClick(2)'>
			<input type="button" id="Button2" value="Manage Booking" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(4)'>
		</td>
		<td>
			 
		</td>
	</tr>
	<tr>
		<td>
			<font>Language</font>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<select id='selLangunage' size='1' style='width:175px;' NAME='selCurrency'>
				<option value="EN">English</option>
				<option value="AR">Arabic</option>
				<option value="RU">Russian</option>
				<option value="FR">French</option>
				<option value="ES">Spanish</option>
				<option value="IT">Italian</option>
				<option value="DE">German</option>
				<option value="TR">Turkish</option>			
			</select>
		</td>
		<td>
		</td>
	</tr>
	
	<tr>
		<td>
			<font>Origin Static Website</font>
		</td>
		<td>
		</td>
	</tr>
	
	<tr>
		<td>
			<select id='selOriginCarrier' size='1' style='width:175px;' NAME='selOriginCarrier'>
				<option value="3O">Air Arabia Maroc</option>
			</select>
		</td>
		<td>
		<br>
		</td>
		
	</tr>
	<tr>
		<td>
		 
		 
		</td>
	</tr>
	
</table>
<form id="form1" method="post" action="../public/showReservation.action">
	<input type="hidden" id="hdnParamData" name="hdnParamData">
	<input type="hidden" id="hdnCarrier" name="hdnCarrier">
	<input type="hidden" id="hdnOriginCountry" name="hdnOriginCountry"/>
	<input type="hidden" id="hdnParamUrl" name="hdnParamUrl" value="<%=url%>">
</form>
<script language="javascript" type="text/javascript">
<!--
var arrCurrency = new Array();
var arrError = new Array();
var arrAirports = new Array();

<%@ include file="airarabiaAirportsList.jsp" %>

arrCurrency[0] = new Array('GBP','Britain Pound'); 
arrCurrency[1] = new Array('MAD','Moroccan Dhiram'); 
arrCurrency[2] = new Array('AED','UAE Dirham');
arrCurrency[3] = new Array('USD','US Dollar');
arrCurrency[4] = new Array('EUR','Euro');

//--------- Country list to populate agent search & IBE marketing carrier identification --------------------------//
var arrCountry = new Array();
arrCountry[0] = new Array('OL', 'COUNTRY FOR OFFLINE AGENT', 'G9');
arrCountry[1] = new Array('AE', 'UAE', 'G9|3O');
arrCountry[2] = new Array('AF','Afghanistan','G9');
arrCountry[3] = new Array('AM','Armenia','G9'); 	
arrCountry[4] = new Array('BD','Bangladesh','G9'); 	
arrCountry[5] = new Array('BH','Bahrain','G9'); 	
arrCountry[6] = new Array('DJ','Djibouti','G9');
arrCountry[7] = new Array('EG','Egypt','G9'); 	
arrCountry[8] = new Array('FR','France','G9'); 	
arrCountry[9] = new Array('GR','Greece','G9'); 	
arrCountry[10] = new Array('IN','India','G9'); 	
arrCountry[11] = new Array('IR','Iran','G9'); 	
arrCountry[12] = new Array('JO','Jordan','G9'); 	
arrCountry[13] = new Array('KE','Kenya','G9'); 	
arrCountry[14] = new Array('KW','Kuwait','G9');	 	
arrCountry[15] = new Array('KZ','Kazakhstan','G9'); 	
arrCountry[16] = new Array('LB','Lebanon','G9'); 
arrCountry[17] = new Array('LK','Sri Lanka','G9'); 	
arrCountry[18] = new Array('MA','Morocco','G9'); 	
arrCountry[19] = new Array('MV','Maldives','G9'); 	
arrCountry[20] = new Array('MY ','Malaysia','G9');	
arrCountry[21] = new Array('NP','Nepal','G9'); 	
arrCountry[22] = new Array('OM','Oman','G9');
arrCountry[23] = new Array('PK','Pakistan','G9'); 	 	
arrCountry[24] = new Array('PN','Palestine','G9'); 	
arrCountry[25] = new Array('QA','Qatar','G9'); 	
arrCountry[26] = new Array('RU','Russia','G9');	
arrCountry[27] = new Array('SA','Saudi Arabia','G9');
arrCountry[28] = new Array('SD','Sudan','G9'); 	
arrCountry[29] = new Array('SG ','Singapore','G9');	
arrCountry[30] = new Array('ES','Spain','G9');	 	
arrCountry[31] = new Array('SY','Syria','G9'); 	
arrCountry[32] = new Array('TR','Turkey','G9'); 	
arrCountry[33] = new Array('UA','Ukraine','G9'); 	
arrCountry[34] = new Array('YE','Yemen','G9');	
arrCountry[35] = new Array("GE","Georgia", 'G9');	
arrCountry[36] = new Array("ID","Indonesia", 'G9');
arrCountry[37] = new Array("LI","Liechtenstein", 'G9');
arrCountry[38] = new Array("MR","Mauritania", 'G9');	
arrCountry[39] = new Array("MD","Moldova", 'G9');	
arrCountry[40] = new Array("MM","Myanmar", 'G9');	
arrCountry[41] = new Array("NG","Nigeria", 'G9');	
arrCountry[42] = new Array("ST","Sao Tome and Principe", 'G9');
arrCountry[43] = new Array("GB","United Kingdom", 'G9');	
arrCountry[44] = new Array("TZ","Tanzania", 'G9');	
arrCountry[45] = new Array("TM","Turkmenistan", 'G9');	
arrCountry[46] = new Array("AZ","Azerbaijan", 'G9');	
arrCountry[47] = new Array("BS","Bahamas", 'G9');	
arrCountry[48] = new Array("BB","Barbados", 'G9');	
arrCountry[49] = new Array("GA","Gabon", 'G9');	
arrCountry[50] = new Array("CF","Central African Republic", 'G9');
arrCountry[51] = new Array("CL","Chile", 'G9');	
arrCountry[52] = new Array("CU","Cuba", 'G9');	
arrCountry[53] = new Array("DK","Denmark", 'G9');	
arrCountry[54] = new Array("GL","Greenland", 'G9');	
arrCountry[55] = new Array("FO","Faroe Islands", 'G9');	
arrCountry[56] = new Array("EE","Estonia", 'G9');	
arrCountry[57] = new Array("PF","French Polynesia", 'G9');
arrCountry[58] = new Array("WF","Wallis and Futuna Islands", 'G9');	
arrCountry[59] = new Array("GH","Ghana", 'G9');
arrCountry[60] = new Array("GI","Gibraltar", 'G9');	
arrCountry[61] = new Array("GY","Guyana", 'G9');	
arrCountry[62] = new Array("HT","Haiti", 'G9');	
arrCountry[63] = new Array("HN","Honduras", 'G9');	
arrCountry[64] = new Array("LT","Lithuania", 'G9');	
arrCountry[65] = new Array("MU","Mauritius", 'G9');	
arrCountry[66] = new Array("MX","Mexico", 'G9');	
arrCountry[67] = new Array("NA","Namibia");	
arrCountry[68] = new Array("WS","Western Samoa", 'G9');	
arrCountry[69] = new Array("VN","Vietnam", 'G9');	
arrCountry[70] = new Array("ZW","Zimbabwe", 'G9');	
arrCountry[71] = new Array("BT","Bhutan", 'G9');	
arrCountry[72] = new Array("NO","Norway", 'G9');	
arrCountry[73] = new Array("BN","Brunei", 'G9');	
arrCountry[74] = new Array("ML","Mali", 'G9');
arrCountry[75] = new Array("NE","Niger", 'G9');	
arrCountry[76] = new Array("SN","Senegal", 'G9');	
arrCountry[77] = new Array("CI","Ivory Coast", 'G9');	
arrCountry[78] = new Array("BF","Burkina Faso", 'G9');	
arrCountry[79] = new Array("KH","Cambodia", 'G9');	
arrCountry[80] = new Array("PA","Panama", 'G9');	
arrCountry[81] = new Array("SZ","Swaziland", 'G9');	
arrCountry[82] = new Array("TJ","Tajikistan", 'G9');	
arrCountry[83] = new Array("TN","Tunisia", 'G9');	
arrCountry[84] = new Array("VE","Venezuela", 'G9');	
arrCountry[85] = new Array("IT","Italy", 'G9');	
arrCountry[86] = new Array("MC","Monaco", 'G9');	
arrCountry[87] = new Array("AD","Andorra", 'G9');	
arrCountry[88] = new Array("AT","Austria", 'G9');
arrCountry[89] = new Array("BE","Belgium", 'G9');
arrCountry[90] = new Array("FI","Finland", 'G9');
arrCountry[91] = new Array("PT","Portugal", 'G9');
arrCountry[92] = new Array("LU","Luxembourg", 'G9');	
arrCountry[93] = new Array("MQ","Martinique", 'G9');	
arrCountry[94] = new Array("SM","San Marino", 'G9');	
arrCountry[95] = new Array("NL","Netherlands", 'G9');	
arrCountry[96] = new Array("GF","French Guiana", 'G9');
arrCountry[97] = new Array("AR","Argentina", 'G9');	
arrCountry[98] = new Array("KR","Korea, South", 'G9');
arrCountry[99] = new Array("GP","Guadeloupe", 'G9');	
arrCountry[100] = new Array("JM","Jamaica", 'G9');	
arrCountry[101] = new Array("BM","Bermuda", 'G9');	
arrCountry[102] = new Array("MO","Macau", 'G9');	
arrCountry[103] = new Array("CH","Switzerland", 'G9');	
arrCountry[104] = new Array("BO","Bolivia", 'G9');	
arrCountry[105] = new Array("BW","Botswana", 'G9');	
arrCountry[106] = new Array("ET","Ethiopia", 'G9');
arrCountry[107] = new Array("HU","Hungary", 'G9');
arrCountry[108] = new Array("LA","Laos", 'G9');
arrCountry[109] = new Array("MK","Macedonia", 'G9');
arrCountry[110] = new Array("MN","Mongolia", 'G9');
arrCountry[111] = new Array("NI","Nicaragua", 'G9');
arrCountry[112] = new Array("SI","Slovenia", 'G9');
arrCountry[113] = new Array("SE","Sweden", 'G9');
arrCountry[114] = new Array("VU","Vanuatu", 'G9');
arrCountry[115] = new Array("AO","Angola", 'G9');
arrCountry[116] = new Array("AW","Aruba", 'G9');
arrCountry[117] = new Array("NR","Nauru", 'G9');
arrCountry[118] = new Array("KI","Kiribati", 'G9');
arrCountry[119] = new Array("BI","Burundi", 'G9');
arrCountry[120] = new Array("CV","Cape Verde", 'G9');
arrCountry[121] = new Array("KY","Cayman Islands", 'G9');
arrCountry[122] = new Array("CR","Costa Rica", 'G9');
arrCountry[123] = new Array("ER","Eritrea", 'G9');
arrCountry[124] = new Array("FJ","Fiji", 'G9');
arrCountry[125] = new Array("GM","Gambia", 'G9');
arrCountry[126] = new Array("LR","Liberia", 'G9');
arrCountry[127] = new Array("MT","Malta", 'G9');
arrCountry[128] = new Array("SC","Seychelles", 'G9');
arrCountry[129] = new Array("SB","Solomon Islands", 'G9');
arrCountry[130] = new Array("TO","Tonga", 'G9');
arrCountry[131] = new Array("ZM","Zambia", 'G9');
arrCountry[132] = new Array("GU","Guam", 'G9');
arrCountry[133] = new Array("PW","Palau", 'G9');
arrCountry[134] = new Array("EC","Ecuador", 'G9');
arrCountry[135] = new Array("PR","Puerto Rico", 'G9');
arrCountry[136] = new Array("AS","American Samoa", 'G9');
arrCountry[137] = new Array("MH","Marshall Islands", 'G9');
arrCountry[138] = new Array("VG","British Virgin Islands", 'G9');
arrCountry[139] = new Array("MP","Northern Mariana Islands", 'G9');
arrCountry[140] = new Array("BZ","Belize", 'G9');
arrCountry[141] = new Array("NU","Niue", 'G9');
arrCountry[142] = new Array("NZ","New Zealand", 'G9');
arrCountry[143] = new Array("CK","Cook Islands", 'G9');
arrCountry[144] = new Array("CY","Cyprus", 'G9');
arrCountry[145] = new Array("GT","Guatemala", 'G9');
arrCountry[146] = new Array("JP","Japan", 'G9');
arrCountry[147] = new Array("LS","Lesotho", 'G9');
arrCountry[148] = new Array("MW","Malawi", 'G9');
arrCountry[149] = new Array("PG","Papua New Guinea", 'G9');
arrCountry[150] = new Array("PY","Paraguay", 'G9');
arrCountry[151] = new Array("TW","Taiwan", 'G9');
arrCountry[152] = new Array("UZ","Uzbekistan", 'G9');
arrCountry[153] = new Array("BR","Brazil", 'G9');
arrCountry[154] = new Array("BG","Bulgaria", 'G9');
arrCountry[155] = new Array("DO","Dominican Republic", 'G9');
arrCountry[156] = new Array("GW","Guinea", 'G9');
arrCountry[157] = new Array("IS","Iceland", 'G9');
arrCountry[158] = new Array("LV","Latvia", 'G9');
arrCountry[159] = new Array("MZ","Mozambique", 'G9');
arrCountry[160] = new Array("PE","Peru", 'G9');
arrCountry[161] = new Array("SR","Suriname", 'G9');
arrCountry[162] = new Array("UG","Uganda", 'G9');
arrCountry[163] = new Array("GD","Grenada", 'G9');
arrCountry[164] = new Array("AI","Anguilla", 'G9');
arrCountry[165] = new Array("MS","Montserrat", 'G9');
arrCountry[166] = new Array("AG","Antigua and Barbuda", 'G9');
arrCountry[167] = new Array("CO","Colombia", 'G9');
arrCountry[168] = new Array("HR","Croatia", 'G9');
arrCountry[169] = new Array("CZ","Czech Republic", 'G9');
arrCountry[170] = new Array("HK","Hong Kong", 'G9');
arrCountry[171] = new Array("PL","Poland", 'G9');
arrCountry[172] = new Array("RO","Romania", 'G9');
arrCountry[173] = new Array("RW","Rwanda", 'G9');
arrCountry[174] = new Array("SL","Sierra Leone", 'G9');
arrCountry[175] = new Array("UY","Uruguay", 'G9');
arrCountry[176] = new Array("AU","Australia", 'G9');
arrCountry[177] = new Array("CA","Canada", 'G9');
arrCountry[178] = new Array("CN","China", 'G9');
arrCountry[179] = new Array("DE","Germany", 'G9');
arrCountry[180] = new Array("DZ","Algeria", 'G9');
arrCountry[181] = new Array("IE","Ireland", 'G9');
arrCountry[182] = new Array("LY","Libya", 'G9');
arrCountry[183] = new Array("NB","Northern Ireland", 'G9');
arrCountry[184] = new Array("PH","Philippines", 'G9');
arrCountry[185] = new Array("SO","Somalia", 'G9');
arrCountry[186] = new Array("SV","El Salvador", 'G9');
arrCountry[187] = new Array("TH","Thailand", 'G9');
arrCountry[188] = new Array("UK","United Kingdom", 'G9');
arrCountry[189] = new Array("US","United States", 'G9');
arrCountry[190] = new Array("ZA","South Africa", 'G9');
arrCountry[191] = new Array("IQ","Iraq", 'G9');
arrCountry[192] = new Array("OT","Others", 'G9');
arrCountry[193] = new Array("BA","Bank Misr", 'G9');
arrCountry[194] = new Array("LC","Saint Lucia", 'G9');
//----------- End of country list -----------------------------//

arrError['ERR009'] = 'Your Departure Date cannot be before the current date. Please check and enter again.';
arrError['ERR008'] = 'Please select your Returning Date.';
arrError['ERR002'] = 'Please select your destination.';
arrError['ERR006'] = 'Please select a Currency.';
arrError['ERR004'] = 'Your Returning Date cannot be before your Departing Date. Please check your dates and enter again. ';
arrError['ERR001'] = 'Please select the city you will be departing from.';
arrError['ERR005'] = 'Please enter a valid Date.';
arrError['ERR007'] = 'Please select your Departing Date.';
arrError['ERR003'] = 'The From & To locations cannot be the same. Please check and enter again';

var appURL = getValue("hdnParamUrl");

var strDash = "----------------------"
var strSeleDDay = "";
var strSeleRDay = "";
var intEndMonth  = "";
var intEndYear = "";	
var objCW ;
var strRDate = "";
var strDDate = "";
var blnReturn = false;

// Default values
var intMaxAdults = 9
var strFromHD = 'Where are you flying from'
var strToHD = 'Where are you flying to'
var strDDDefValue = 'Please Select'
var intRDaysDef = 0;
var strDefDayRetu = "0";
var strBaseAirport = "CMN"
var strFrom = "";
var strTo = "";
var strCurr = "MAD";
var strSelCurr = "EUR";
var strDDays = "0";
var strRDays = "0";
var intDays = "3";
var intRDays = "3";
var strDefDayDept = "0";
var strLanguage = "EN";
var strOriginCarrier = "3O";

var dtC = new Date();
var dtCM = dtC.getMonth() + 1;
var dtCD = dtC.getDate();
if (dtCM < 10){dtCM = "0" + dtCM}
if (dtCD < 10){dtCD = "0" + dtCD}

var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); //"25/08/2005";
var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));

	function buildYearDropDown(strName, intYears){
		var dtDepDate	= addDays(dtSysDate, 0);
		var intMonth	= dtDepDate.getMonth();
		var intYear		= dtDepDate.getFullYear();
		var arrMonths	= new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		
		var objDrop		= getFieldByID(strName);
		var strValue    = "";
		var intMonths	= 12 * intYears
		for (var i = 0 ; i < intMonths; i++){
			
			if ((intMonth + 1) < 10){
				strValue = "0" + (intMonth + 1);
			}else{
				strValue = (intMonth + 1);
			}
			
			objDrop.length =  objDrop.length + 1
			objDrop.options[objDrop.length - 1].text = arrMonths[intMonth] + " " + intYear;
			objDrop.options[objDrop.length - 1].value = strValue + "/" + intYear;
			intEndMonth = strValue;
			intEndYear  = intYear;
			intMonth++
			
			if (intMonth > 11){
				intMonth = 0 ;
				intYear++;
			}
		}
	}
	
	function faqClick(){
		var intHeight = (window.screen.height - 200);
		var intWidth = 795;
		var intX = ((window.screen.height - intHeight) / 2);
		var intY = ((window.screen.width - intWidth) / 2);
		var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
		if ((objCW)  && (!objCW.closed)){objCW.close();}
		objCW = window.open(strFAQLink,'ToolChild',strProp)
		
	}
	
	function FillCalenderDay(objDName, strValue){
		var objdrop = getFieldByID(objDName)
		var strCD	= strSysDate
		var strDMonth = Number(strSysDate.substr(3,2));
		var strDYear = Number(strSysDate.substr(6,4));
		
		var strMonth = Number(strValue.substr(0,2));
		var strYear = Number(strValue.substr(3,4));
		
		var strDays = getDaysInMonth(strMonth, strYear);
		var strDay = ""
		var strStartDate = 1;
		if ((strDMonth == strMonth) && (strYear == strDYear)){
			strStartDate = Number(strSysDate.substr(0,2))
		}
		
		objdrop.length = 1;
		for (var i = strStartDate; i <= strDays ; i ++){
			strDay = i 
			if (strDay < 10){
				strDay = "0" + strDay
			}
			objdrop.length = objdrop.length + 1
			objdrop.options[objdrop.length -1].text =  strDay;
			objdrop.options[objdrop.length -1].value =  strDay;
		}
	}
	
	function buildDropDowns(){
		var intCount = arrAirports.length ; 
		var objDF 	 = getFieldByID("selFromLoc");
		var objDT 	 = getFieldByID("selToLoc");
		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] == strBaseAirport){
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strFromHD;
				objDF.options[objDF.length - 1].value = "";
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strToHD;
				objDT.options[objDT.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";
				break;
			}
		}
		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] != strBaseAirport){
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];
			}
		}
		
		buildYearDropDown("selYrDept",1);
		buildYearDropDown("selYrRetu",1);
		FillCalenderDay("selDtDept", getFieldByID("selYrDept").options[1].value);
		
		defaultSetYearDay();
		
		var objLB = new listBox();
		objLB.dataArray = arrCurrency; 
		objLB.id = "selCurrency";
		objLB.blnMergeTextValue = true;
		objLB.firstTextValue = strDDDefValue;
		objLB.blnMergeStyle = "-";
		objLB.blnFirstEmpty = true;
		objLB.fillListBox();		
		
		var objOriginLB = new listBox();
		objOriginLB.dataArray = arrCountry;
		objOriginLB.id = "selOriginCountry";
		objOriginLB.blnMergeTextValue = false;
		objOriginLB.firstTextValue = "Morocco";
		objOriginLB.firstValue = "MA";
		objOriginLB.blnMergeStyle = "-";
		objOriginLB.blnFirstEmpty = true;
		objOriginLB.fillListBox();
	}
	
	
	function defaultSetYearDay(){
		FillCalenderDay("selDtRetu", getFieldByID("selYrRetu").options[1].value);
	}
	
	function selYrDeptOnChange(){
		FillCalenderDay("selDtDept", getValue("selYrDept"));
		setField("selDtDept", strSeleDDay);
	}
	
	function selYrRetuOnChange(){
		FillCalenderDay("selDtRetu", getValue("selYrRetu"));
		setField("selDtRetu", strSeleRDay);
	}
	
	function selDtDeptOnChange(){
		strSeleDDay = getValue("selDtDept");
	}

	function selDtRetuOnChange(){
		strSeleRDay = getValue("selDtRetu");
	}	
	
	function selToLocOnChange(){
		if (getText("selToLoc") == strDash){
			getFieldByID("selToLoc").options[0].selected = true;
		}
	}
	
	function selFromLocOnChange(){
		if (getText("selFromLoc") == strDash){
			getFieldByID("selFromLoc").options[0].selected = true;
		}
	}
	
	function setDate(strDate, strID){
		var arrStrValue = strDate.split("/");
		switch (strID){
			case "0" : 
				setField("selYrDept", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrDeptOnChange();
				setField("selDtDept", arrStrValue[0]);
				break ;
			case "1" : 
				setField("selYrRetu", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrRetuOnChange();
				setField("selDtRetu", arrStrValue[0]);
				break ;
		}
	}
	
	function chkReturnTrip_click(){
		if (getFieldByID("chkReturnTrip").checked){
			setField("selRVariance", intRDaysDef);
			Disable("selDtRetu", false);
			Disable("selYrRetu", false);
			Disable("selRVariance", false);
		}else{
			setField("selRVariance", "0");
			setField("selDtRetu", "");
			setField("selYrRetu", "");
			Disable("selDtRetu", true);
			Disable("selYrRetu", true);
			Disable("selRVariance", true);
			defaultSetYearDay();
		}
	}
	
	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 0 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.currentDate = strSysDate;
		switch (strID){
			case 0 : 
				if (getValue("selDtDept") != "" && getValue("selYrDept")){
					objCal1.currentDate = getValue("selDtDept") + "/" + getValue("selYrDept");
				}
				break;
			case 1 : 
			if (getFieldByID("chkReturnTrip").checked){
				if (getValue("selDtRetu") != "" && getValue("selYrRetu")){
					objCal1.currentDate = getValue("selDtRetu") + "/" + getValue("selYrRetu");
				}
			}else{
				return;
			}
			break;			
		}		
		objCal1.showCalendar(objEvent);
	}
	
	// --------------------- Maximum Passengers
	function selAdults_onChange(){
		var arrInfants = new Array();
		var intAdults = Number(getValue("selAdults"))// + Number(getValue("selChild"));
		var strCInfants = getValue("selInfants");
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrInfants; 
		objLB.textIndex = "0";
		objLB.id = "selInfants";
		objLB.fillListBox();
		setPaxArrays();
		setField("selInfants",  strCInfants);
	}
	
	function flightSearchBtnOnClick(intID){
		switch (intID){
			case 0 :				 
				if (getValue("selFromLoc")=="--From--"){
					alert("Please select the location of your departure");
					return false;
				}
				else if (getValue("selToLoc")=="--To--"){
					alert("Please select the location of your arrival");
					return false;
				}
				if (clientValidateFS()){
					var objFrm = getFieldByID("form1");
					setField("hdnCarrier",  getValue("selOriginCarrier"));	
					setField("hdnOriginCountry", getValue("selOriginCountry"));
					objFrm.target = "_top";
					objFrm.action = "../public/showReservation.action";
					objFrm.submit();
				}
				break;
			case 1 : 
				var strPostData =  getValue("selLangunage") + "^RE^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
			case 2 :
				var strPostData = getValue("selLangunage") + "^SI^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));				
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 3 : 
				var strPostData =  getValue("selLangunage") + "^REA^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 4 : 
				var strPostData =  getValue("selLangunage") + "^MAN^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 7 : 
			    var strPostData =  getValue("selLangunage") + "^MC^" + getValue("selCurrency") ;
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 8 : 
				var strPostData =  getValue("selLangunage") + "^MRE^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
		}
	}	
	
	
	function clientValidateFS(){
		if (getValue("selFromLoc") == ""){
			alert(arrError["ERR001"]);		
			getFieldByID("selFromLoc").focus();		
			return false;
		}
		
		if (getValue("selToLoc") == ""){
			alert(arrError["ERR002"]);
			getFieldByID("selToLoc").focus();
			return false;
		}		
		
		if (getValue("selCurrency") == ""){
			alert(arrError["ERR006"]);
			getFieldByID("selCurrency").focus();
			return false;
		}				
		
		if (getValue("selFromLoc") == getValue("selToLoc")){
			alert(arrError["ERR003"]);
			getFieldByID("selToLoc").focus();			
			return false;
		}
		
		if ((getValue("selDtDept") == "") || (getValue("selYrDept") == "")){
			alert(arrError["ERR007"]); 	
			getFieldByID("selDtDept").focus();		
			return false;
		}else{
			if (!CheckDates(strSysDate, getValue("selDtDept") + "/" + getValue("selYrDept"))){
				alert(arrError["ERR009"] + strSysDate + ".");
				getFieldByID("selDtDept").focus();	
				return false;
			}			
		}
		
		if (getFieldByID("chkReturnTrip").checked){
			if ((getValue("selDtRetu") == "") || (getValue("selYrRetu") == "")){
				alert(arrError["ERR008"]); 	
				getFieldByID("selDtRetu").focus();		
				return false;
			}
		
			if (!CheckDates(getValue("selDtDept") + "/" + getValue("selYrDept"), getValue("selDtRetu") + "/" + getValue("selYrRetu"))){
				alert(arrError["ERR004"]);
				getFieldByID("selDtRetu").focus();
				return false;
			}
		}
		
		strRDate = "";
		blnReturn = false;
		if (getValue("selDtRetu") != "" && getValue("selYrRetu") != ""){
			strRDate	= getValue("selDtRetu") + "/" + getValue("selYrRetu");
		}
		if (getFieldByID("chkReturnTrip").checked){blnReturn	= true;}
		
		var strPostData = getValue("selLangunage") + "^" + 
						  "AS^" +
					   getValue("selFromLoc") + "^" +
					   getValue("selToLoc") + "^" +
					   getValue("selDtDept") + "/" + getValue("selYrDept") + "^" +
					   strRDate + "^" +
					   getValue("selDVariance") + "^" +
					   getValue("selRVariance") + "^" +
					   getValue("selCurrency") + "^" +
					   getValue("selAdults") + "^" +
					   getValue("selChild") + "^" +
					   getValue("selInfants") + "^" +
					   getText("selFromLoc") + "^" +
					   getText("selToLoc") + "^" +
					   blnReturn;
		setField("hdnParamData", strPostData);
		return true;
	}
	
	function defaultDataLoad(){
		var dtDepDate = addDays(dtSysDate, Number(strDefDayDept));
		strDDate = dateChk(dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear());
		strSeleDDay = dtDepDate.getDate();
		
		var dtRetuDate = "";
		strRDate = "";
		strSeleRDay = "  /  /    ";
		if (String(strDefDayRetu) != ""){
			var dtRetuDate = addDays(dtSysDate, Number(strDefDayRetu));
			strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
			strSeleRDay = dtRetuDate.getDate();
			setField("chkReturnTrip", true);
		}
		
		setField("selDVariance", strDDays); 
		setField("selRVariance", strRDays);
		chkReturnTrip_click();
	}
	
	function dateOnBlur(strID, objC){
		var blnDateEntered = false;
		if (objC.value != ""){blnDateEntered = true;}
		dateChk(strID);
		if (objC.value == "" && blnDateEntered){
			alert(arrError["ERR005"]); 
		}
	}
	
	function cacheData(){
		setField("selFromLoc", strFrom);
		setField("selToLoc", strTo);
		
		var arrDtDept = strDDate.split("/");
		setField("selYrDept", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrDeptOnChange();
		setField("selDtDept", arrDtDept[0]);
		
		setField("selYrRetu", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrRetuOnChange();
		setField("selDtRetu", arrDtDept[0]);
		
		if (getValue("selDtRetu") == ""){
			defaultSetYearDay();
		}
				
		setField("selDVariance", strDDays);
		setField("selRVariance", strRDays);
		setField("selCurrency", strSelCurr);
		setField("selOriginCarrier", strOriginCarrier);
		
		getFieldByID("selFromLoc").focus();
	}
	
	var arrAdults = new Array();
	
	function setPaxArrays(){
		var arrChild  = new Array();
		var adCount = getValue("selAdults");
		for (var i = 0 ; i <= (intMaxAdults - parseInt(adCount,10)) ; i++){
			arrChild[i] = new Array();
			arrChild[i][0] = i;
		}
		objLB = new listBox();
		objLB.dataArray = arrChild; 
		objLB.textIndex = "0";
		objLB.id = "selChild";
		objLB.fillListBox();
	}
	
	// ------------------------ 
	function flightSearchOnLoad(){
		buildDropDowns();
		

		for (var i = 1 ; i <= intMaxAdults ; i++){
			arrAdults[i - 1] = new Array();
			arrAdults[i - 1][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrAdults; 
		objLB.textIndex = "0";
		objLB.id = "selAdults";
		objLB.fillListBox();
		
		setPaxArrays();
		selAdults_onChange();
		
		// ------------------------ 
		var arrDays = new Array();
		for (var i = 0 ; i <= intDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		
		var objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selDVariance";
		objLB2.fillListBox();
		
		arrDays.length = 0
		for (var i = 0 ; i <= intRDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selRVariance";
		objLB2.fillListBox();

		Disable("selDtRetu", true);
		Disable("selYrRetu", true);
		Disable("selRVariance", true);
		
		defaultDataLoad()
		cacheData();
	}	
	flightSearchOnLoad();
	
	// ---------------- Calendar
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.borderColor = "#FE0000";
	objCal1.borderClass = "calendarBorder"
	objCal1.headerTextColor = "white";
	objCal1.noYears = "1";
	objCal1.blnArabic = false;
	objCal1.disableUpto = strSysDate;
	objCal1.disableFrom = getDaysInMonth(intEndMonth, intEndYear) + "/" + intEndMonth + "/" + intEndYear;
	objCal1.buildCalendar();
//-->
</script>
<script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script> 
<script type="text/javascript"> 
    try {
    	var pageTracker = _gat._getTracker("UA-1319699-8"); 
        pageTracker._setDomainName(".isaaviations.com");
        pageTracker._setAllowHash("false");
        pageTracker._initData();
		pageTracker._trackPageview("/Landing_3O");
    } catch(err) {}
</script>
</body>
</html>
