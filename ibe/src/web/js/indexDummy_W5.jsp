<%@ page language="java"%>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title><%=AppParamUtil.getDefaultCarrier()%></title>
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
<meta name=ProgId content=VisualStudio.HTML>
<meta name=Originator content="Microsoft Visual Studio .NET 7.1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<%! 
String url = AppParamUtil.getSecureIBEUrl() + "showReservation.action";
//String kioskUrl = AppParamUtil.getNonsecureIBEUrl()+"showKiosk.action";
String kioskUrl = AppParamUtil.getSecureIBEUrl()+"showLoadPage!kioskLogin.action";
String scheduleUrl =  AppParamUtil.getNonsecureIBEUrl()+"js/testSchedule.jsp";
String agentsURL =  AppParamUtil.getNonsecureIBEUrl()+"showAgentsInfo.action";
String dummyAgentURL = "retrieveAgentDummy_GA.jsp";
%>
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../js/common/commonLoad.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../js/SSTbl.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	

<style type="text/css">
     .clFont {
        font-family: Verdana,Arial,Helvetica,sans-serif;;
		color: #737373;
		font-size:10px;
      }

	
.ssThinBorder{
	BORDER-RIGHT: #a6a6a6 1px solid; 
	BORDER-TOP: #a6a6a6 1px solid; 
	BORDER-LEFT: #a6a6a6 1px solid; 
	BORDER-BOTTOM: #a6a6a6 1px solid; 
	}

.ssThinBG{
	background-color:#EFEFEF;
	}
.ssHref:link
{
	font-family:Verdana,Arial,Helvetica,sans-serif;
	font-size:12px;
	color:Black;
}	
.ssHref:HOVER
{
	color:Red;
	text-decoration:underline;}

.ssThinBGSltd
{
	background-color:#CFCFCF;
	}	
	body{
	overflow-x:hidden;
}
</style>
    

</head>
<body style="background-image: url(../images/Testing_no_cache.jpg);">
<table width="100%" cellpadding="2" cellspacing="0" border="0" align="top">
	<tr>
		<td colspan="3" width="100%" style="border-left-width: 1; border-right-width: 1; border-top-width: 1; border-bottom: 1px solid #808080">
			<font class="clFont">Server Time : <span id="spnClock" name ="spnClock"></span></font>
		</td>
	</tr>
</table>

<table width="775" cellpadding="2" cellspacing="0" border="0" align="center">
	<tr>
		<td width="180" valign="top">
			<!-- Flight Search Goes Here -->
			<table width='100%' border='0' cellpadding='0' cellspacing='2' ID='Table7' bgcolor="white">
				<tr>
					<td colspan='4'><font><b>Book My Flight</b></font> |
					 <font><b><a href='javascript:void(0)' onclick='flightSearchBtnOnClick(7)'>MultiCity Search</a></b></font></td>
				</tr>
				<tr>
					<td colspan='4'>
					<select id='selFromLoc' size='1' style='width:175px;' NAME='selFromLoc' onchange='selFromLocOnChange()'>
						
						</select>
					
					
							
					</td>
				</tr>
				<tr>
					<td colspan='4'>
					<select id='selToLoc' size='1' style='width:175px;' NAME='selToLoc' onchange='selToLocOnChange()'>
						
						</select>
				
					
					</td>

				</tr>
				<tr>
					<td colspan='4'><font>Departing Date</font></td>
				</tr>
				<tr>
					<td colspan='1'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
							<tr>

								<td width="95%">
									<select id='selDtDept' size='1' style='width:50px;' NAME='selDtDept' onchange="selDtDeptOnChange()">
										<option></option>
									</select>
									<select id='selYrDept' size='1' style='width:100px;' NAME='selYrDept' onchange="selYrDeptOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>

									<a href='javascript:void(0)' onclick='LoadCalendar(0,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selDVariance' size='1' style='width:50px;' NAME='selDVariance' title='No of days before or after departure date'>

						</select>
						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
				<td>
				<table>
				<tr>
					<td colspan='1'><font>Returning Date</font></td>
					<td align='right'>

						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td align='right'>
									<font>&nbsp;Return Trip&nbsp;</font>
								</td>
								<td align='right'>
									<input type="checkbox" id="chkReturnTrip" name="chkReturnTrip" onclick="chkReturnTrip_click()" class="noBorder">
								</td>

							</tr>
						</table>
					</td>
					</tr>
				</table>
				</td>
				</tr>
				<tr>
					<td colspan='1'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table3">
							<tr>
								<td width="95%">

									<select id='selDtRetu' size='1' style='width:50px;' NAME='selDtRetu' onchange="selDtRetuOnChange()">
										<option></option>
									</select>
									<select id='selYrRetu' size='1' style='width:100px;' NAME='selYrRetu' onchange="selYrRetuOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>
									<a href='javascript:void(0)' onclick='LoadCalendar(1,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>

								</td>
							</tr>
						</table>
					</td>
				</tr>		
				<tr>
					<td colspan='4'>
						<select id='selRVariance' size='1' style='width:50px;' NAME='selRVariance' title='No of days before or after return date'>
						</select>

						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' ID='Table8'>
							<tr>

								<td width='30%'><font>Adults</font></td>
								<td width='33%'><font>Children</font></td>
								<td width='37%'><font>Infants&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
							</tr>
							<tr>
								<td><font>&nbsp;</font></td>
								<td><font class='fntsmall'>(2-14 years)</font></td>

								<td><font class='fntsmall'>(under 2 years)</font></td>
							</tr>
							<tr>
								<td>
									<select id='selAdults' size='1' style='width:45px;' NAME='selAdults' onChange='selAdults_onChange()'>
									</select>														
								</td>			
								<td>
									<select id='selChild' size='1' style='width:50px;' NAME='selChild'>

									</select>														
								</td>
								<td align='right'>
									<select id='selInfants' size='1' style='width:65px;' NAME='selInfants'>
									</select>														
								</td>			
							</tr>	
						</table>
					</td>
				</tr>				
				<tr>

					<td colspan='4'>
						<a href="#" onclick="faqClick()"><font class='fntsmall'><u>Important information on Child and Infant bookings</u></font></a>
					</td>
				</tr>
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				</tr>
				<tr id="trClass2">
					<td colspan='4'><font>Class</font></td>
				</tr>
				<tr id="trClass3">
						<td colspan='4'>
						<select id='selCOS' size='1' style='width:175px;' NAME='selCOS'>
							<option value="Y">Economy Class</option>
							<option value="C">Business Class</option>
						</select>
					</td>
				</tr>					
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				<tr>
				<tr>
					<td colspan='4'><font>Currency</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selCurrency' size='1' style='width:175px;' NAME='selCurrency'>
						</select>
					</td>
				</tr>

				<tr>
					<td colspan='4' align='right'>
						<input type='button' id='btnSearch' class='Button' value='Search' title='Click here to Search for flights' NAME='btnSearch' onclick='flightSearchBtnOnClick(0)'>
					</td>
				</tr>

	<tr>
		<td>
			<input type="button" id="btn1" value="Register" class='Button' onclick='flightSearchBtnOnClick(1)'>
			<input type="button" id="btnAgent" value="RegisterAgent" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(3)'>
		<!-- 	<input type="button" id="btnCorporate" value="RegisterCorporate" class='Button ButtonMedium' onclick='flightSearchBtnOnClick(5)'> -->
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="Button1" value="Sign In" class='Button' onclick='flightSearchBtnOnClick(2)'>
			<input type="button" id="Button2" value="Manage Booking Link" class='Button ButtonLarge' onclick='flightSearchBtnOnClick(4)'>	
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<font>Language</font>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<select id='selLangunage' size='1' style='width:175px;' NAME='selLangunage'>
				<option value="EN">English</option>
				<option value="FA">Persian</option>
			</select>
		</td>
		<td>
		</td>
	</tr>
	
	<tr>
		<td>
			<font>Origin Static Website</font>
		</td>
		<td>
		</td>
	</tr>
	
	<tr>
		<td>
			<select id='selOriginCarrier' size='1' style='width:175px;' NAME='selOriginCarrier'>
				<option value="W5">Mahan Airways</option>
			</select>
		</td>
		<td>
		<br>
		</td>
	</tr>
	<tr>
		<td>
		 
		 
		</td>
	</tr>
				
			</table>
			
		</td>
		<td rowspan="3" valign="top" halign="left"><table width="200px"><tr>

			<td> 
			<a href="javascript:updateTitle()"><font class="fntBold">Set Time to Title</font></a><br>
			</td>	 	
			</tr>		 
			 
			<tr>

			<tr><td><br><font class="fntBold">Configurations</font></td></tr>
				<tr> 
					<td align="left">
						<font>Load Configs from DB : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isUsingDBConfigs()%></font>
					</td></tr>
				<tr> 
						<td align="left">
							<font>Default PGW Currency: </font>
						</td>
						<td align="right">
							<font><%=AppParamUtil.getDefaultPGW()%></font>
						</td></tr>
				<tr> 
					<td align="left">
						<font>Apply CC Charge: </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isApplyCreditCardCharge()%></font>
					</td></tr>
					<tr> 
					<td align="left">
						<font>Show Travel Guard : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isTravelGuardEnabled()%></font>
					</td></tr>
					<tr> 
					<td align="left">
						<font>Is In Dev Mode : </font>
					</td>
					<td align="right">
						<font><%=AppParamUtil.isInDevMode()%></font>
					</td></tr>
			 
		</table>
		</td>
		<td valign="top" align="right"> 
			<input type="hidden" name="schurl" id="schurl" value="<%=AppParamUtil.getNonsecureIBEUrl()%>">
			<input type="hidden" id="specialFltFromDate" name="specialFltFromDate" value="">
			<input type="hidden" id="specialFltToDate" name="specialFltToDate" value="">
			<input type="hidden" id="specialFltNo" name="specialFltNo" value="">
			<input type="hidden" id="specialFltDst" name="specialFltDst" value="JED">
		<table border="0" width="300">
			<tr>
			<td  valign="top" align="right">
			<span id= "schedule" style="visibility:visible;">
			<table border="0" width="300" style="border-collapse: collapse; border-style: inset; border-width: 1" bordercolor="#111111">
						 <tr>
        				<td width="16%" align="left" colspan="2" style="padding-right:3px;border:0;vertical-align:middle; "><font class="fntBold fntRed">Test Schedule</font></td>
      				</tr>
					<tr>
        				<td width="16%" align="left" style="padding-right:3px;border:0;vertical-align:middle; "><font>Round Trip:</font></td>
	
      			 		 <td width="91%" style="padding-right:3px;border:0; "><input type="checkbox" id="chkReturnTrip2" name="chkReturnTrip2"  class="noBorder"></td>
      				</tr>	
					 <tr>
				        <td width="5%" style="padding-right:3px;border:0; "><font>From*</font></td>
				
				        <td width="20%" style="padding-right:3px;border:0; "><select id='selFromLoc2' class="form_destinations" style="width:150px;" name='selFromLoc2' onChange='selFromLoc2OnChange()'>
				        </select></td>
				        <td width="12%" style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td width="63%" style="padding-right:3px;border:0; ">&nbsp;</td>
		      		</tr>
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>To*</font></td>
				        <td style="padding-right:3px;border:0; "><select id='selToLoc2' class="form_destinations" style="width:150px;" name='selToLoc2' onChange='selToLoc2OnChange()'>
				
				        </select></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      		</tr>
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>Month*</font></td>
				        <td style="padding-right:3px;border:0;width:180px "><select id='selYrDept2' class="form_date" style="width:150px;" name='selYrDept2' >
				        </select></td>
				
				        <td style="padding-right:3px;border:0; " align="left"><input name="go" type="button" id="go" value="Search" class='Button' onClick="onGo()"></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      </tr>
		      <tr>
		      		<td colspan="3"><iframe  id="sch_container" name="sch_container" width="99%" height="200px" src="" scrolling="auto" frameborder="0" style="width:99%;height:200px;"></iframe></td>				
		      </tr>
		      </table>
		      </span>
		      </td>
		      </tr>
		      <!-- 
		      <tr> 
		      <td valign="top" align="right"> 
		 		<table border="0" width="300" style="border-collapse: collapse; border-style: inset; border-width: 1" bordercolor="#111111">
					 <tr>
        				<td width="16%" align="left" colspan="2" style="padding-right:3px;border:0;vertical-align:middle; "><font class="fntBold fntRed">Test Agents</font></td>
      				</tr>
						
					 <tr>
				        <td width="5%" style="padding-right:3px;border:0; "><font>Country*</font></td>
				
				        <td width="20%" style="padding-right:3px;border:0; "><select id='selCountry' class="form_destinations" style="width:150px;" name='selCountry' onChange='selCountryOnChange()'>
				        </select></td>
				        <td width="12%" style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td width="63%" style="padding-right:3px;border:0; ">&nbsp;</td>
		      		</tr>
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>City*</font></td>
				        <td style="padding-right:3px;border:0; "><select id='selStation' class="form_destinations" style="width:150px;" name='selStation'>
				
				        </select></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      		</tr>
		      		<tr>
				        <td style="padding-right:3px;border:0; "><font>Agent Type*</font></td>
				        <td style="padding-right:3px;border:0; "><select id='selAgentType' class="form_destinations" style="width:150px;" name='selAgentType' >
				        <option value="ALL">All</option>
				        <option value="GSASO">Sales Shops and GSA</option>
				        <option value="TA">Travel Agents</option>
				        </select></td>
				
				        <td style="padding-right:3px;border:0; " align="left"><input name="continue" type="button" id="continue" value="Continue" class='Button' onClick="onContinue()"></td>
				        <td style="padding-right:3px;border:0; ">&nbsp;</td>
		      </tr>
		      <tr>
		      		<td colspan="4"><iframe  id="sch_agents" name="sch_agents" width="99%" height="350" src=""  scrolling="yes" frameborder="0"></iframe></td>				
		      </tr>
		      -->
			</table>
			<table>
			<!-- 	<tr>
					<td>
					<form id="manageBookingWithData" method="post">
					<fieldset>
						<legend style="text-decoration: blink">
						<font style="color: red; font-weight: bold;"> Test Manage booking with External Data <font></legend>
						<table>
							<tr>
								<td><font>PNR </font></td>
								<td width="5">:</td>
								<td width="250" class="alignLeft" align="left"><input
									type="text" name="pnr" id="pnr" /> <font class="mandatory">
								*</font></td>
							</tr>
							<tr>
								<td><font>Last Name </font></td>
								<td width="5">:</td>
								<td class="alignLeft" width="250" align="left"><input
									type="text" style="width: 220px;" name="lastName"
									id="txtLastName" /> <font class="mandatory"> *</font></td>
							</tr>
							<tr>
								<td><font>Departure Date </font></td>
								<td width="5">:</td>
								<td class="alignLeft" width="250" align="left"><input
									type="text" style="width: 80px;" name="dateOfDep"
									id="txtDateOfDep" /><font>(dd/mm/yyyy) </font> <label
									class="mandatory"> *</label> <br />
								</td>
							</tr>
							<tr>
								<td align="center" colspan="3"><input type="button"
									class="Button" value="Continue" title="Continue" tabindex="24"
									id="btnContinue" onclick="flightSearchBtnOnClick(6)" /></td>
							</tr>
						</table>
					</fieldset>
					</form>
					</td>
				</tr> -->
			</table>
 	 	</td>
		</tr>
		</table>
		</td>
		</tr>
<!-- -->		
	
	
</table>
<form id="form1" method="post" action="../public/showReservation.action">
	<input type="hidden" id="hdnParamData" name="hdnParamData">
	<input type="hidden" id="hdnCarrier" name="hdnCarrier">
	<input type="hidden" id="hdnParamUrl" name="hdnParamUrl" value="<%=url%>">
</form>

<form method="post" name="formRetrieveAgents" id="formRetrieveAgents">
	<input type="hidden" id="hdnMode" name="hdnMode" value="">
	<input type="hidden" id="hdnLanguage" name="hdnLanguage" value="en">
	<input type="hidden" id="hdnurl" name="hdnurl" value="<%=agentsURL%>">
 </form>
	<input type="hidden" id="specialFltFromDate" name="specialFltFromDate" value="">
	<input type="hidden" id="specialFltToDate" name="specialFltToDate" value="">
	<input type="hidden" id="specialFltNo" name="specialFltNo" value="">
	<input type="hidden" id="specialFltDst" name="specialFltDst" value="JED">
	
	<input type="hidden" name="sysDate" id="sysDate" value="<%=AppParamUtil.getSysDate() %>">
	
<script language=javascript>
<!--
var arrAirports = new Array();
var arrCurrency = new Array();
var arrError = new Array();
var strBaseAirport1 = "THR";
var strBaseAirport2 = "IKA";
var shjHub="";
var maHub="";
var egHub="";
arrAirports [0] = new Array('THR',' Tehran','');
//arrAirports [3] = new Array('BLR',' Bangalore Internatinal Airport','');
//arrAirports [4] = new Array('BOM',' Bombay airport','');
arrAirports [1] = new Array('ABD',' Abadan','');
arrAirports [2] = new Array('ACZ',' Zabol','');
arrAirports [3] = new Array('ADU',' Ardebil','');
arrAirports [4] = new Array('ALA',' Almaty','');
arrAirports [5] = new Array('AWZ',' AHWAZ','');
arrAirports [6] = new Array('AZD',' Yazd','');
arrAirports [7] = new Array('BGW',' Baghdad','');
arrAirports [8] = new Array('BKK',' Bangkok','');
arrAirports [9] = new Array('BND',' BANDARRABBAS','');
arrAirports [10] = new Array('DAM',' DAMASCUS','');
arrAirports [11] = new Array('DEL',' Delhi','');
arrAirports [12] = new Array('DUS',' Dusseldorf','');
arrAirports [13] = new Array('DXB',' Dubai International Airport','');
arrAirports [14] = new Array('HKT',' Phuket','');
arrAirports [15] = new Array('IFN',' ISFAHAN','');
arrAirports [16] = new Array('IHR',' Iranshahr','');
arrAirports [17] = new Array('IKA',' Tehran - Imam Khomeini','');
arrAirports [18] = new Array('IST',' ATATURK','');
arrAirports [19] = new Array('JWN',' Zanjan','');
arrAirports [20] = new Array('KER',' kerman','');
arrAirports [21] = new Array('KHD',' khoramabad','');
arrAirports [22] = new Array('KIH',' kish','');
arrAirports [23] = new Array('KSH',' kermanshah','');
arrAirports [24] = new Array('KUL',' kuala lumpur','');
arrAirports [25] = new Array('MHD',' Mashhad','');
arrAirports [26] = new Array('MRX',' Mahshahr','');
arrAirports [27] = new Array('PGU',' Asaloueyeh','');
arrAirports [28] = new Array('PGV',' Pudong','');
arrAirports [29] = new Array('RAS',' Rasht','');
arrAirports [30] = new Array('SAW',' SABIHA','');
arrAirports [31] = new Array('SDG',' Sanandaj','');
arrAirports [32] = new Array('SRY',' Sari','');
arrAirports [33] = new Array('SYJ',' Sirjan','');
arrAirports [34] = new Array('SYZ',' Shiraz','');
arrAirports [35] = new Array('TBZ',' Tabriz','');
arrAirports [36] = new Array('XBJ',' Birjand','');
arrAirports [37] = new Array('ZAH',' Zahedan','');
arrAirports [38] = new Array('PVG',' Shanghai-terminal2','');
arrAirports [39] = new Array('ADB',' IZMIR','');
arrAirports [40] = new Array('BAH',' Bahrain','');
arrAirports [41] = new Array('KBP',' BORISPOL-KIEV','');
var arrAirportSCH = arrAirports;

arrCurrency[0] = new Array('IRR','Iran Riyal');  
arrCurrency[1] = new Array('USD','US Dollar');


arrError['ERR009'] = 'Your Departure Date cannot be before the current date. Please check and enter again.';
arrError['ERR008'] = 'Please select your Returning Date.';
arrError['ERR002'] = 'Please select your destination.';
arrError['ERR006'] = 'Please select a Currency.';
arrError['ERR004'] = 'Your Returning Date cannot be before your Departing Date. Please check your dates and enter again. ';
arrError['ERR001'] = 'Please select the city you will be departing from.';
arrError['ERR005'] = 'Please enter a valid Date.';
arrError['ERR007'] = 'Please select your Departing Date.';
arrError['ERR003'] = 'The From & To locations cannot be the same. Please check and enter again';

var appURL = getValue("hdnParamUrl");

var strDash = "----------------------"
var strSeleDDay = "";
var strSeleRDay = "";
var intEndMonth  = "";
var intEndYear = "";	
var objCW ;
var strRDate = "";
var strDDate = "";
var blnReturn = false;

// Default values
var intMaxAdults = 9
var strFromHD = 'Where are you flying from'
var strToHD = 'Where are you flying to'
var strDDDefValue = 'Please Select'
var intRDaysDef = 0;
var strDefDayRetu = "0";
var strBaseAirport = "THR"
var strFrom = "";
var strTo = "";
var strCurr = "IQD";
var strSelCurr = "IQD";
var strDDays = "0";
var strRDays = "0";
var intDays = "3";
var intRDays = "3";
var strDefDayDept = "0";
var strLanguage = "EN";
var strOriginCarrier = "W5";

var dtC = new Date();
var dtCM = dtC.getMonth() + 1;
var dtCD = dtC.getDate();
if (dtCM < 10){dtCM = "0" + dtCM}
if (dtCD < 10){dtCD = "0" + dtCD}

var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); //"25/08/2005";
var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));

	function buildYearDropDown(strName, intYears){
		var dtDepDate	= addDays(dtSysDate, 0);
		var intMonth	= dtDepDate.getMonth();
		var intYear		= dtDepDate.getFullYear();
		var arrMonths	= new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		
		var objDrop		= getFieldByID(strName);
		var strValue    = "";
		var intMonths	= 12 * intYears
		for (var i = 0 ; i < intMonths; i++){
			
			if ((intMonth + 1) < 10){
				strValue = "0" + (intMonth + 1);
			}else{
				strValue = (intMonth + 1);
			}
			
			objDrop.length =  objDrop.length + 1
			objDrop.options[objDrop.length - 1].text = arrMonths[intMonth] + " " + intYear;
			objDrop.options[objDrop.length - 1].value = strValue + "/" + intYear;
			intEndMonth = strValue;
			intEndYear  = intYear;
			intMonth++
			
			if (intMonth > 11){
				intMonth = 0 ;
				intYear++;
			}
		}
	}
	
	function faqClick(){
		var intHeight = (window.screen.height - 200);
		var intWidth = 795;
		var intX = ((window.screen.height - intHeight) / 2);
		var intY = ((window.screen.width - intWidth) / 2);
		var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
		if ((objCW)  && (!objCW.closed)){objCW.close();}
		objCW = window.open(strFAQLink,'ToolChild',strProp)
		
	}
	
	function FillCalenderDay(objDName, strValue){
		var objdrop = getFieldByID(objDName)
		var strCD	= strSysDate
		var strDMonth = Number(strSysDate.substr(3,2));
		var strDYear = Number(strSysDate.substr(6,4));
		
		var strMonth = Number(strValue.substr(0,2));
		var strYear = Number(strValue.substr(3,4));
		
		var strDays = getDaysInMonth(strMonth, strYear);
		var strDay = ""
		var strStartDate = 1;
		if ((strDMonth == strMonth) && (strYear == strDYear)){
			strStartDate = Number(strSysDate.substr(0,2))
		}
		
		objdrop.length = 1;
		for (var i = strStartDate; i <= strDays ; i ++){
			strDay = i 
			if (strDay < 10){
				strDay = "0" + strDay
			}
			objdrop.length = objdrop.length + 1
			objdrop.options[objdrop.length -1].text =  strDay;
			objdrop.options[objdrop.length -1].value =  strDay;
		}
	}
	
	
	function onGo(){
	 
	 
		
	var selValueDF = getValue("selFromLoc2");
	var selValueDT = getValue("selToLoc2");
		
	var roundTrip = document.getElementById('chkReturnTrip2').checked;
	var from = "";
	var to = "";
	 
		from = arrAirports[selValueDF][0];
		to = arrAirports[selValueDT][0];
	 
	var date = document.getElementById('selYrDept2').value;
	var specFltFromDate = document.getElementById('specialFltFromDate').value;
	var specFltToDate = document.getElementById('specialFltToDate').value;
	var specFltNo = document.getElementById('specialFltNo').value;
	var specFltDst = document.getElementById('specialFltDst').value;
	var surl = "";
	var hub ="";
	var lang = document.getElementById('selLangunage').options[document.getElementById('selLangunage').selectedIndex].value;
	
		surl = document.getElementById('schurl').value +"showFlightSchedule.action";
	 
		 
	
	surl = surl+"?startDate="+date+"&fromDst="+from+"&toDst="+to+"&roundTrip="+roundTrip+"&lang="+lang.toLowerCase()+"&sffd="+specFltFromDate+"&sftd="+specFltToDate+"&sfno="+specFltNo+"&sfd="+specFltDst;
	var frm = document.getElementById('sch_container');
	frm.src = surl;


	//var agentFrm =document.getElementById('sch_agents'); 
	//agentFrm.src = document.getElementById('schurl').value+"showAgentsInfo.action";
}
	
	
	function buildYearDropDown(strName, intYears){
		var dtDepDate	= addDays(dtSysDate, 0);
		var intMonth	= dtDepDate.getMonth();
		var intYear		= dtDepDate.getFullYear();
		var arrMonths	= new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		
		var objDrop		= getFieldByID(strName);
		if(objDrop == null)
			return ;
		var strValue    = "";
		var intMonths	= 12 * intYears
		for (var i = 0 ; i < intMonths; i++){
			
			if ((intMonth + 1) < 10){
				strValue = "0" + (intMonth + 1);
			}else{
				strValue = (intMonth + 1);
			}
			
			objDrop.length =  objDrop.length + 1
			objDrop.options[objDrop.length - 1].text = arrMonths[intMonth] + " " + intYear;
			objDrop.options[objDrop.length - 1].value = strValue + "/" + intYear;
			intEndMonth = strValue;
			intEndYear  = intYear;
			intMonth++
			
			if (intMonth > 11){
				intMonth = 0 ;
				intYear++;
			}
		}
	}
	
	
	
	
	function buildDestList(obj, hub, dText){
		var intCount = arrAirports.length ; 
		
		obj.length =  obj.length + 1
		obj.options[obj.length - 1].text = dText;
		obj.options[obj.length - 1].value = "";
		
		obj.length =  obj.length + 1
		obj.options[obj.length - 1].text = strDash;
		obj.options[obj.length - 1].value = "";
		var hubCodes = hub.split(",");		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] == strBaseAirport || arrAirports[i][0] == strBaseAirport2){
				
				if(isHubMatch(arrAirports[i][2], hubCodes) == true){
					obj.length =  obj.length + 1
					obj.options[obj.length - 1].text = arrAirports[i][1];
					obj.options[obj.length - 1].value = i;
					
					obj.length =  obj.length + 1
					obj.options[obj.length - 1].text = strDash;
					obj.options[obj.length - 1].value = "";
				}
			}
		}
			

		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] != strBaseAirport && arrAirports[i][0] != strBaseAirport2){
				if(isHubMatch(arrAirports[i][2], hubCodes) == true){
					obj.length =  obj.length + 1
					obj.options[obj.length - 1].text = arrAirports[i][1];
					obj.options[obj.length - 1].value = i;
				}
			}
		}
	}
	
	 
	
	function buildDropDowns(){
	
		var intCount = arrAirports.length ; 
		var objDF 	 = getFieldByID("selFromLoc");
		var objDT 	 = getFieldByID("selToLoc");
		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] == strBaseAirport){
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strFromHD;
				objDF.options[objDF.length - 1].value = "";
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strToHD;
				objDT.options[objDT.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";
				break;
			}
		}
		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] != strBaseAirport){
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];
			}
		}
		
		buildYearDropDown("selYrDept",1);
		buildYearDropDown("selYrRetu",1);
		FillCalenderDay("selDtDept", getFieldByID("selYrDept").options[1].value);
		
		defaultSetYearDay();
		
		var objLB = new listBox();
		objLB.dataArray = arrCurrency; 
		objLB.id = "selCurrency";
		objLB.blnMergeTextValue = true;
		objLB.firstTextValue = strDDDefValue;
		objLB.blnMergeStyle = "-";
		objLB.blnFirstEmpty = false;
		objLB.fillListBox();		
	}

	//**************** Related to Agents search ****************//
	/** Populates Country list*/
	 

	function selCountryOnChange(){
		var country = getValue("selCountry");
		buildStationDropDown(country);
	}

	function onContinue(){
		setField("hdnMode", "SEARCH");
		setField("hdnLanguage", "en"); // Language
		//var object = document.getElementById('sch_agents');
		var url = getValue("hdnurl");

		var country=getValue("selCountry");
		var station = getValue("selStation");
		var agentType=getValue("selAgentType");
			
		url = url+"?selCountry="+country+"&selStation="+station+"&selAgentType="+agentType;
		//object.src =url+'&hdnMode=SEARCH';
		
		getFieldByID("formRetrieveAgents").action = url+'&hdnMode=SEARCH' ;
		getFieldByID("formRetrieveAgents").submit();
		
	}
	
	
	function defaultSetYearDay(){
		FillCalenderDay("selDtRetu", getFieldByID("selYrRetu").options[1].value);
	}
	
	function selYrDeptOnChange(){
		FillCalenderDay("selDtDept", getValue("selYrDept"));
		setField("selDtDept", strSeleDDay);
	}
	
	function selYrRetuOnChange(){
		FillCalenderDay("selDtRetu", getValue("selYrRetu"));
		setField("selDtRetu", strSeleRDay);
	}
	
	function selDtDeptOnChange(){
		strSeleDDay = getValue("selDtDept");
	}

	function selDtRetuOnChange(){
		strSeleRDay = getValue("selDtRetu");
	}	
	
	function selToLocOnChange(){
		if (getText("selToLoc") == strDash){
			getFieldByID("selToLoc").options[0].selected = true;
		}
	}
	
	function selFromLocOnChange(){
		if (getText("selFromLoc") == strDash){
			//getFieldByID("selFromLoc").options[0].selected = true;
		}
	}
	
	function setDate(strDate, strID){
		var arrStrValue = strDate.split("/");
		switch (strID){
			case "0" : 
				setField("selYrDept", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrDeptOnChange();
				setField("selDtDept", arrStrValue[0]);
				break ;
			case "1" : 
				setField("selYrRetu", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrRetuOnChange();
				setField("selDtRetu", arrStrValue[0]);
				break ;
		}
	}
	
	function chkReturnTrip_click(){
		if (getFieldByID("chkReturnTrip").checked){
			setField("selRVariance", intRDaysDef);
			Disable("selDtRetu", false);
			Disable("selYrRetu", false);
			Disable("selRVariance", false);
		}else{
			setField("selRVariance", "0");
			setField("selDtRetu", "");
			setField("selYrRetu", "");
			Disable("selDtRetu", true);
			Disable("selYrRetu", true);
			Disable("selRVariance", true);
			defaultSetYearDay();
		}
	}
	
	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 0 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.currentDate = strSysDate;
		switch (strID){
			case 0 : 
				if (getValue("selDtDept") != "" && getValue("selYrDept")){
					objCal1.currentDate = getValue("selDtDept") + "/" + getValue("selYrDept");
				}
				break;
			case 1 : 
			if (getFieldByID("chkReturnTrip").checked){
				if (getValue("selDtRetu") != "" && getValue("selYrRetu")){
					objCal1.currentDate = getValue("selDtRetu") + "/" + getValue("selYrRetu");
				}
			}else{
				return;
			}
			break;			
		}		
		objCal1.showCalendar(objEvent);
	}
	
	// --------------------- Maximum Passengers
	function selAdults_onChange(){
		var arrInfants = new Array();
		var intAdults = Number(getValue("selAdults"))// + Number(getValue("selChild"));
		var strCInfants = getValue("selInfants");
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrInfants; 
		objLB.textIndex = "0";
		objLB.id = "selInfants";
		objLB.fillListBox();
		
		setField("selInfants",  strCInfants);
	}
	
	function flightSearchBtnOnClick(intID){
		switch (intID){
			case 0 : 
				if (clientValidateFS()){
					var objFrm = getFieldByID("form1");
					setField("hdnCarrier",  getValue("selOriginCarrier"));	
					objFrm.target = "_top";
					objFrm.action = "../public/showReservation.action";
					objFrm.submit();
				}
				break;
			case 1 : 
				var strPostData =  getValue("selLangunage") + "^RE^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
			case 2 :
				var strPostData = getValue("selLangunage") + "^SI^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));				
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 3 : 
				var strPostData =  getValue("selLangunage") + "^REA^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 4 : 
				var strPostData =  getValue("selLangunage") + "^MAN^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));				
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 5 : 
				var strPostData =  getValue("selLangunage") + "^REC^";
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 6 : 
				var strPostData =  getValue("selLangunage") + "^ELINK^" + getValue("pnr") + "^" + getValue("txtLastName") + "^" + getValue("txtDateOfDep");
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		   case 7 : 
			    var strPostData =  getValue("selLangunage") + "^MC^" + getValue("selCurrency") ;
				setField("hdnParamData", strPostData);
				setField("hdnCarrier",  getValue("selOriginCarrier"));	
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
		}
	}	
	
	
	function clientValidateFS(){
		if (getValue("selFromLoc") == ""){
			alert(arrError["ERR001"]);		
			//getFieldByID("selFromLoc").focus();		
			return false;
		}
		
		if (getValue("selToLoc") == ""){
			alert(arrError["ERR002"]);
			//getFieldByID("selToLoc").focus();
			return false;
		}		
		
		if (getValue("selCurrency") == ""){
			alert(arrError["ERR006"]);
			getFieldByID("selCurrency").focus();
			return false;
		}				
		
		if (getValue("selFromLoc") == getValue("selToLoc")){
			alert(arrError["ERR003"]);
			//getFieldByID("selToLoc").focus();			
			return false;
		}
		
		if ((getValue("selDtDept") == "") || (getValue("selYrDept") == "")){
			alert(arrError["ERR007"]); 	
			getFieldByID("selDtDept").focus();		
			return false;
		}else{
			if (!CheckDates(strSysDate, getValue("selDtDept") + "/" + getValue("selYrDept"))){
				alert(arrError["ERR009"] + strSysDate + ".");
				getFieldByID("selDtDept").focus();	
				return false;
			}			
		}
		
		if (getFieldByID("chkReturnTrip").checked){
			if ((getValue("selDtRetu") == "") || (getValue("selYrRetu") == "")){
				alert(arrError["ERR008"]); 	
				getFieldByID("selDtRetu").focus();		
				return false;
			}
		
			if (!CheckDates(getValue("selDtDept") + "/" + getValue("selYrDept"), getValue("selDtRetu") + "/" + getValue("selYrRetu"))){
				alert(arrError["ERR004"]);
				getFieldByID("selDtRetu").focus();
				return false;
			}
		}
		
		strRDate = "";
		blnReturn = false;
		if (getValue("selDtRetu") != "" && getValue("selYrRetu") != ""){
			strRDate	= getValue("selDtRetu") + "/" + getValue("selYrRetu");
		}
		if (getFieldByID("chkReturnTrip").checked){blnReturn	= true;}
		
		var strPostData = getValue("selLangunage") + "^" + 
						  "AS^" +
					   getValue("selFromLoc") + "^" +
					   getValue("selToLoc") + "^" +
					   getValue("selDtDept") + "/" + getValue("selYrDept") + "^" +
					   strRDate + "^" +
					   getValue("selDVariance") + "^" +
					   getValue("selRVariance") + "^" +
					   getValue("selCurrency") + "^" +
					   getValue("selAdults") + "^" +
					   getValue("selChild") + "^" +
					   getValue("selInfants") + "^" +
					   getText("selFromLoc") + "^" +
					   getText("selToLoc") + "^" +
					   blnReturn + "^" +
					   getValue("selCOS");		 
		setField("hdnParamData", strPostData);
		return true;
	}
	
	function defaultDataLoad(){
		var dtDepDate = addDays(dtSysDate, Number(strDefDayDept));
		strDDate = dateChk(dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear());
		strSeleDDay = dtDepDate.getDate();
		
		var dtRetuDate = "";
		strRDate = "";
		strSeleRDay = "  /  /    ";
		if (String(strDefDayRetu) != ""){
			var dtRetuDate = addDays(dtSysDate, Number(strDefDayRetu));
			strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
			strSeleRDay = dtRetuDate.getDate();
			setField("chkReturnTrip", true);
		}
		
		setField("selDVariance", strDDays); 
		setField("selRVariance", strRDays);
		chkReturnTrip_click();
	}
	
	function dateOnBlur(strID, objC){
		var blnDateEntered = false;
		if (objC.value != ""){blnDateEntered = true;}
		dateChk(strID);
		if (objC.value == "" && blnDateEntered){
			alert(arrError["ERR005"]); 
		}
	}
	
	function cacheData(){
		setField("selFromLoc", strFrom);
		setField("selToLoc", strTo);
		
		var arrDtDept = strDDate.split("/");
		setField("selYrDept", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrDeptOnChange();
		setField("selDtDept", arrDtDept[0]);
		
		setField("selYrRetu", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrRetuOnChange();
		setField("selDtRetu", arrDtDept[0]);
		
		if (getValue("selDtRetu") == ""){
			defaultSetYearDay();
		}
				
		setField("selDVariance", strDDays);
		setField("selRVariance", strRDays);
		setField("selCurrency", strSelCurr);
		setField("selOriginCarrier", strOriginCarrier);
		
		//getFieldByID("selFromLoc").focus();
	}
	
	// ------------------------ 
	function flightSearchOnLoad(){
		buildDropDowns();
		
		var arrAdults = new Array();
		var arrChild  = new Array();
		arrChild[0] = new Array();
		arrChild[0][0] = "0";
		for (var i = 1 ; i <= intMaxAdults ; i++){
			arrAdults[i - 1] = new Array();
			arrAdults[i - 1][0] = i;
			
			arrChild[i] = new Array();
			arrChild[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrAdults; 
		objLB.textIndex = "0";
		objLB.id = "selAdults";
		objLB.fillListBox();
		
		objLB = new listBox();
		objLB.dataArray = arrChild; 
		objLB.textIndex = "0";
		objLB.id = "selChild";
		objLB.fillListBox();
		selAdults_onChange();
		
		// ------------------------ 
		var arrDays = new Array();
		for (var i = 0 ; i <= intDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		
		var objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selDVariance";
		objLB2.fillListBox();
		
		arrDays.length = 0
		for (var i = 0 ; i <= intRDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selRVariance";
		objLB2.fillListBox();

		Disable("selDtRetu", true);
		Disable("selYrRetu", true);
		Disable("selRVariance", true);
		
		defaultDataLoad()
		cacheData();
	}	
	flightSearchOnLoad();
	
	// ---------------- Calendar
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.borderColor = "#0A3986";
	objCal1.borderClass = "calendarBorder"
	objCal1.headerTextColor = "white";
	objCal1.noYears = "1";
	objCal1.blnArabic = false;
	objCal1.disableUpto = strSysDate;
	objCal1.disableFrom = getDaysInMonth(intEndMonth, intEndYear) + "/" + intEndMonth + "/" + intEndYear;
	objCal1.buildCalendar();
	
	
	var objSecs;
	function startTime() {
		var sysDate = ""
		sysDate = document.getElementById('sysDate').value
		var arrdt = sysDate.split(" ");
		var arryear = arrdt[0].split("/");
		var arrhrs = arrdt[1].split(":");
		vrSyaDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
				Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
				Number(arrhrs[2]));
		objSecs = setInterval("addSeconds()", 1000);
	}

	function addSeconds() {
		vrSyaDate.setSeconds(vrSyaDate.getSeconds() + 1);
		var clDate = vrSyaDate;
		getFieldByID("spnClock").innerHTML = clDate.toLocaleString();
	}
	
	try{
		startTime();
	}catch(e){ getFieldByID("spnClock").innerHTML = "error" }
	
	var objTSecs;
	function startStatusTime() {
		var sysDate = ""
		sysDate = document.getElementById('sysDate').value
		var arrdt = sysDate.split(" ");
		var arryear = arrdt[0].split("/");
		var arrhrs = arrdt[1].split(":");
		vrSyaDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
				Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
				Number(arrhrs[2]));
		objSecs = setInterval("addSeconds()", 1000);
	}
	
	
	function updateTitle(){
	try{
		var sysTDate = ""
			sysTDate = document.getElementById('sysDate').value
			var arrdt = sysTDate.split(" ");
			var arryear = arrdt[0].split("/");
			var arrhrs = arrdt[1].split(":");
			vrSyTDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
					Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
					Number(arrhrs[2]));
			objTSecs = setInterval("addSecondsToTitle()", 1000);
		}catch(e){ }
	}
	
	function addSecondsToTitle() {
		try{
			vrSyTDate.setSeconds(vrSyaDate.getSeconds() + 1);
			var clDate = vrSyTDate.getHours()+": "+ vrSyTDate.getMinutes()+": "+vrSyTDate.getSeconds();
			document.title = clDate.toLocaleString();
			}catch(e){ }
	}
	
/*	 var objFD = new filterDropDown();
	objFD.id  = "spnOrg";
	objFD.width = "163px";
	objFD.listWidth = "280px";
	objFD.dataArray = arrAirports;
	 
	//objFD.preference = "CMB";
	objFD.onClick = "pgOnClickFr";
	objFD.onSelect = "pgOnSelectFr";
	//objFD.value = "CMB";
	objFD.wrapText = false;
	objFD.defaultText = "--From--";
	objFD.tabIndex = 1;
	//objFD.displayFilterDropDown();

	 var objTD = new filterDropDown();
	objTD.id  = "spnDes";
	objTD.width = "163px";
	objTD.listWidth = "280px";
	objTD.dataArray = arrAirports;
	 
	//objTD.preference = "MWZ";
	objTD.onClick = "pgOnClickT";
	objTD.onSelect = "pgOnSelectT";
	//objTD.value = "MWZ";
	objTD.wrapText = false;
	objTD.defaultText = "--To--";
	objTD.tabIndex = 2;
	objTD.displayFilterDropDown();

	 */ 
	 
//-->
</script>
<script src="../js/CommonLoader.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../js/dummydest.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
</body>
</html>
