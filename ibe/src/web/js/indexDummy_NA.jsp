<%@ page language="java"%>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
<meta name=ProgId content=VisualStudio.HTML>
<meta name=Originator content="Microsoft Visual Studio .NET 7.1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<%! 
String url = AppParamUtil.getSecureIBEUrl() + "showReservation.action";
  
%>
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../css/CalendarStyle_no_cache.css"></script>	
</head>
<body style="background-image: url(../images/Testing_no_cache.jpg);">
<table width="775" cellpadding="2" cellspacing="0" border="0" align="center">
	<tr>
		<td width="180">
			<!-- Flight Search Goes Here -->
			<table width='100%' border='0' cellpadding='0' cellspacing='2' ID='Table7' bgcolor="white">
				<tr>
					<td colspan='4'><font><b>Book Flights</b></font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selFromLoc' size='1' style='width:175px;' NAME='selFromLoc' onchange='selFromLocOnChange()'>

						</select>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selToLoc' size='1' style='width:175px;' NAME='selToLoc' onchange='selToLocOnChange()'>
						</select>
					</td>

				</tr>
				<tr>
					<td colspan='4'><font>Departing Date</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
							<tr>

								<td width="95%">
									<select id='selDtDept' size='1' style='width:50px;' NAME='selDtDept' onchange="selDtDeptOnChange()">
										<option></option>
									</select>
									<select id='selYrDept' size='1' style='width:100px;' NAME='selYrDept' onchange="selYrDeptOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>

									<a href='javascript:void(0)' onclick='LoadCalendar(0,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selDVariance' size='1' style='width:50px;' NAME='selDVariance' title='No of days before or after departure date'>

						</select>
						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td><font>Returning Date</font></td>
					<td colspan="3" align='right'>

						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td align='right'>
									<font>&nbsp;Return Trip&nbsp;</font>
								</td>
								<td align='right'>
									<input type="checkbox" id="chkReturnTrip" name="chkReturnTrip" onclick="chkReturnTrip_click()" class="noBorder">
								</td>

							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table3">
							<tr>
								<td width="95%">

									<select id='selDtRetu' size='1' style='width:50px;' NAME='selDtRetu' onchange="selDtRetuOnChange()">
										<option></option>
									</select>
									<select id='selYrRetu' size='1' style='width:100px;' NAME='selYrRetu' onchange="selYrRetuOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>
									<a href='javascript:void(0)' onclick='LoadCalendar(1,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>

								</td>
							</tr>
						</table>
					</td>
				</tr>		
				<tr>
					<td colspan='4'>
						<select id='selRVariance' size='1' style='width:50px;' NAME='selRVariance' title='No of days before or after return date'>
						</select>

						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' ID='Table8'>
							<tr>

								<td width='30%'><font>Adults</font></td>
								<td width='33%'><font>Children</font></td>
								<td width='37%'><font>Infants&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
							</tr>
							<tr>
								<td><font>&nbsp;</font></td>
								<td><font class='fntsmall'>(2-14 years)</font></td>

								<td><font class='fntsmall'>(under 2 years)</font></td>
							</tr>
							<tr>
								<td>
									<select id='selAdults' size='1' style='width:45px;' NAME='selAdults' onChange='selAdults_onChange()'>
									</select>														
								</td>			
								<td>
									<select id='selChild' size='1' style='width:50px;' NAME='selChild'>

									</select>														
								</td>
								<td align='right'>
									<select id='selInfants' size='1' style='width:65px;' NAME='selInfants'>
									</select>														
								</td>			
							</tr>	
						</table>
					</td>
				</tr>
				<tr>

					<td colspan='4'>
						<a href="#" onclick="faqClick()"><font class='fntsmall'><u>Important information on Child and Infant bookings</u></font></a>
					</td>
				</tr>
																<tr id="trClass2">
																		<td colspan='4'><font>Class</font></td>
																</tr>
				<tr id="trClass3">
																	<td colspan='4'>
																		<select id='selCOS' size='1' style='width:175px;' NAME='selCOS'>
																		</select>
																	</td>
																</tr>					
				<tr>
					<td colspan='4' class="setHeight">
					</td>
				<tr>

					<td colspan='4'><font>Currency</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selCurrency' size='1' style='width:175px;' NAME='selCurrency'>
						</select>
					</td>
				</tr>
<!-- TMA passenger type -->
<tr>

					<td colspan='4'><font>Passenger Category</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selType' size='1' style='width:175px;' NAME='selType' onChange='currMaldivian()'>
					
						</select>
					</td>
				</tr>

<!-- END  -->


<!-- FARE RULES -->
<tr id='trFareTypeLabel'>
																	<td colspan='4'><font>Fare Type</font></td>
																</tr>
																<tr id='trFareTypeList'>
																	<td colspan='4'>
																		<select id='selFareType' size='1' style='width:175px;' NAME='selFareType'>
																		</select>
																	</td>
																</tr>

<!-- END -->



				<tr>
					<td colspan='4' align='right'>
						<input type='button' id='btnSearch' class='Button' value='Search' title='Click here to Search for flights' NAME='btnSearch' onclick='flightSearchBtnOnClick(0)'>
					</td>
				</tr>
			</table>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="btn1" value="Register" class='Button' onclick='flightSearchBtnOnClick(1)'>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="Button1" value="Sign In" class='Button' onclick='flightSearchBtnOnClick(2)'>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<font>Language</font>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<select id='selLangunage' size='1' style='width:175px;' NAME='selCurrency'>
				<option value="EN">English</option>
				<option value="AR">Arabic</option>
			</select>
		</td>
		<td>
		</td>
	</tr>
	<input type="hidden" id="hdnParamUrl" name="hdnParamUrl" value="<%=url%>">
</table>
<form id="form1" method="post" action="../public/showReservation.action">
	<input type="hidden" id="hdnParamData" name="hdnParamData">
</form>
<script language=javascript>
<!--
var arrAirports 	= new Array();
var arrCurrency = new Array();
var arrError = new Array();
var arrType = new Array();
var arrClass = new Array();
var arrFareType= new Array();

arrAirports [0] = new Array('ALP','Aleppo','');
arrAirports [1] = new Array('ALY','Alexandria -Al Nozha','');
arrAirports [2] = new Array('ALA','Almaty ','');
arrAirports [3] = new Array('AMM','Amman Queen Alia int','');
arrAirports [4] = new Array('AQJ','Aqaba','');
arrAirports [5] = new Array('ATZ','Assiut','');
arrAirports [6] = new Array('TSE','Astana International','');
arrAirports [7] = new Array('BAH','Bahrain','');
arrAirports [8] = new Array('BLR','Bangalore','');
arrAirports [9] = new Array('BEY','Beirut','');
arrAirports [10] = new Array('CMB','Colombo','');
arrAirports [11] = new Array('DAM','Damascus','');
arrAirports [12] = new Array('DMM','Dammam King Fahad','');
arrAirports [13] = new Array('DEL','Delhi','');
arrAirports [14] = new Array('DOH','Doha','');
arrAirports [15] = new Array('GOI','Goa','');
arrAirports [16] = new Array('SAW','Istanbul Sabiha Int Airport','');
arrAirports [17] = new Array('JAI','Jaipur','');
arrAirports [18] = new Array('JED','Jeddah King A Aziz','');
arrAirports [19] = new Array('KBL','Kabul','');
arrAirports [20] = new Array('KRT','Khartoum','');
arrAirports [21] = new Array('COK','Kochi','');
arrAirports [22] = new Array('CCU','Kolkatta','');
arrAirports [23] = new Array('KWI','Kuwait','');
arrAirports [24] = new Array('LXR','Luxor International','');
arrAirports [25] = new Array('IXE','Mangalore','');
arrAirports [26] = new Array('MED','Medinah','');
arrAirports [27] = new Array('BOM','Mumbai','');
arrAirports [28] = new Array('MCT','Muscat','');
arrAirports [29] = new Array('NAG','Nagpur','');
arrAirports [30] = new Array('RUH','Riyadh King Khaled','');
arrAirports [31] = new Array('SAH','Sanaa','');
arrAirports [32] = new Array('SHJ','Sharjah','');
arrAirports [33] = new Array('SSH','Sharm El Sheikh INTL','');
arrAirports [34] = new Array('IKA','Tehran','');
arrAirports [35] = new Array('MLE','Male','');
arrAirports [36] = new Array('GAN','GAN','');
arrAirports [37] = new Array('HAN','Hanimadu','');

arrCurrency[0] = new Array('BHD','Bahrani Dinar'); arrCurrency[1] = new Array('EGP','Egyptian Pound'); arrCurrency[2] = new Array('INR','Indian Rupee'); arrCurrency[3] = new Array('JOD','Jordanian Dinar'); arrCurrency[4] = new Array('LBP','Lebanese Pound'); arrCurrency[5] = new Array('QAR','Qatari Riyal'); arrCurrency[6] = new Array('OMR','Rial Omani'); arrCurrency[7] = new Array('RUB','Russian Rouble'); arrCurrency[8] = new Array('SAR','Saudi Arabian Riyal'); arrCurrency[9] = new Array('LKR','Sri Lanka Rupee'); arrCurrency[10] = new Array('AED','UAE Dirham'); arrCurrency[11] = new Array('USD','US Dollar'); arrCurrency[12] = new Array('MRF','Maldivian Ruffiah');
arrError['ERR009'] = 'Your Departure Date cannot be before the current date. Please check and enter again.';arrError['ERR008'] = 'Please select your Returning Date.';arrError['ERR002'] = 'Please select your destination.';arrError['ERR006'] = 'Please select a Currency.';arrError['ERR004'] = 'Your Returning Date cannot be before your Departing Date. Please check your dates and enter again. ';arrError['ERR001'] = 'Please select the city you will be departing from.';arrError['ERR005'] = 'Please enter a valid Date.';arrError['ERR007'] = 'Please select your Departing Date.';arrError['ERR003'] = 'The From & To locations cannot be the same. Please check and enter again';arrError['ERR010'] = 'Please select passenger type';arrError['ERR011'] = 'Please select Class of Service';

// passenger Types
arrType[0] = new Array('L','Maldivian'); // 1
arrType[1] = new Array('F','Non Maldivian'); // 2 
// arrType[2] = new Array('A','All'); // 2 

arrClass[0] = new Array('J','Business Class'); // 1
arrClass[1] = new Array('Y','Economy Class'); // 2 



arrFareType[0] = new Array('N','Standard Fares'); // 1
arrFareType[1] = new Array('A','Lowest Fares'); // 2 
arrFareType[2] = new Array('R','Restricted Fares'); // 3 
var appURL = getValue("hdnParamUrl");

var strDash = "----------------------"
var strSeleDDay = "";
var strSeleRDay = "";
var intEndMonth  = "";
var intEndYear = "";	
var objCW ;
var strRDate = "";
var strDDate = "";
var blnReturn = false;
var strLanguage = "EN";

// Default values
var intMaxAdults = 9
var strFromHD = 'Where are you flying from'
var strToHD = 'Where are you flying to'
var strDDDefValue = 'Please Select'
var intRDaysDef = 0;
var strDefDayRetu = "0";
var strBaseAirport = "MLE"
var strFrom = "";
var strTo = "";
var strCurr = "MRF";
var strSelCurr = "MRF";	// def currency
var strDDays = "0";
var strRDays = "0";
var intDays = "3";
var intRDays = "3";
var strDefDayDept = "0";
var DefSelType='Please Select' // default value for passenger category

var dtC = new Date();
var dtCM = dtC.getMonth() + 1;
var dtCD = dtC.getDate();
if (dtCM < 10){dtCM = "0" + dtCM}
if (dtCD < 10){dtCD = "0" + dtCD}

var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); //"25/08/2005";
var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));

	// Selects the default currency to MRF if passenger category is Maldivian else default currency is USD
	function currMaldivian()
	{
		
		if(document.getElementById("selType").value=="L")
		{
			setField("selCurrency", strSelCurr);
		}
	}

	function buildYearDropDown(strName, intYears){
		var dtDepDate	= addDays(dtSysDate, 0);
		var intMonth	= dtDepDate.getMonth();
		var intYear		= dtDepDate.getFullYear();
		var arrMonths	= new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		
		var objDrop		= getFieldByID(strName);
		var strValue    = "";
		var intMonths	= 12 * intYears
		for (var i = 0 ; i < intMonths; i++){
			
			if ((intMonth + 1) < 10){
				strValue = "0" + (intMonth + 1);
			}else{
				strValue = (intMonth + 1);
			}
			
			objDrop.length =  objDrop.length + 1
			objDrop.options[objDrop.length - 1].text = arrMonths[intMonth] + " " + intYear;
			objDrop.options[objDrop.length - 1].value = strValue + "/" + intYear;
			intEndMonth = strValue;
			intEndYear  = intYear;
			intMonth++
			
			if (intMonth > 11){
				intMonth = 0 ;
				intYear++;
			}
		}
	}
	
	function faqClick(){
		var intHeight = (window.screen.height - 200);
		var intWidth = 795;
		var intX = ((window.screen.height - intHeight) / 2);
		var intY = ((window.screen.width - intWidth) / 2);
		var strProp		= "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=" + intWidth + ",height=" + intHeight +",resizable=no,top=" + intX + ",left=" + intY;
		if ((objCW)  && (!objCW.closed)){objCW.close();}
		objCW = window.open(strFAQLink,'ToolChild',strProp)
		
	}
	
	function FillCalenderDay(objDName, strValue){
		var objdrop = getFieldByID(objDName)
		var strCD	= strSysDate
		var strDMonth = Number(strSysDate.substr(3,2));
		var strDYear = Number(strSysDate.substr(6,4));
		
		var strMonth = Number(strValue.substr(0,2));
		var strYear = Number(strValue.substr(3,4));
		
		var strDays = getDaysInMonth(strMonth, strYear);
		var strDay = ""
		var strStartDate = 1;
		if ((strDMonth == strMonth) && (strYear == strDYear)){
			strStartDate = Number(strSysDate.substr(0,2))
		}
		
		objdrop.length = 1;
		for (var i = strStartDate; i <= strDays ; i ++){
			strDay = i 
			if (strDay < 10){
				strDay = "0" + strDay
			}
			objdrop.length = objdrop.length + 1
			objdrop.options[objdrop.length -1].text =  strDay;
			objdrop.options[objdrop.length -1].value =  strDay;
		}
	}
	
	function buildDropDowns(){
		var intCount = arrAirports.length ; 
		var objDF 	 = getFieldByID("selFromLoc");
		var objDT 	 = getFieldByID("selToLoc");
		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] == strBaseAirport){
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strFromHD;
				objDF.options[objDF.length - 1].value = "";
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];
				
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = strDash;
				objDF.options[objDF.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strToHD;
				objDT.options[objDT.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = strDash;
				objDT.options[objDT.length - 1].value = "";
				break;
			}
		}
		
		for (var i = 0 ; i < intCount ; i++)		{
			if (arrAirports[i][0] != strBaseAirport){
				objDF.length =  objDF.length + 1
				objDF.options[objDF.length - 1].text = arrAirports[i][1];
				objDF.options[objDF.length - 1].value = arrAirports[i][0];
				
				objDT.length =  objDT.length + 1
				objDT.options[objDT.length - 1].text = arrAirports[i][1];
				objDT.options[objDT.length - 1].value = arrAirports[i][0];
			}
		}
		
		buildYearDropDown("selYrDept",1);
		buildYearDropDown("selYrRetu",1);
		FillCalenderDay("selDtDept", getFieldByID("selYrDept").options[1].value);
		
		defaultSetYearDay();
		
		var objLB = new listBox();
		objLB.dataArray = arrCurrency; 
		objLB.id = "selCurrency";
		objLB.blnMergeTextValue = true;
		objLB.firstTextValue = strDDDefValue;
		objLB.blnMergeStyle = "-";
		objLB.blnFirstEmpty = true;
		objLB.fillListBox();		
		
		//edit
		// passenger Type
		var objLB1 = new listBox();
		objLB1.dataArray = arrType; 
		objLB1.id = "selType";
		objLB1.blnMergeTextValue = false;
		objLB1.firstTextValue = DefSelType; // default value for passenger type
		objLB1.blnMergeStyle = " ";
		objLB1.blnFirstEmpty = true;
		objLB1.fillListBox();		

		var objLB2 = new listBox();
		objLB2.dataArray = arrClass; 
		objLB2.id = "selCOS";
		objLB2.blnMergeTextValue = false;
		objLB2.firstTextValue = DefSelType;
		objLB2.blnFirstEmpty = true;
		objLB2.fillListBox();		
		
		
		var objLB3 = new listBox();
		objLB2.dataArray = arrFareType; 
		objLB2.id = "selFareType";
		objLB2.blnMergeTextValue = false;
		//objLB2.firstTextValue = DefSelType;
		objLB2.blnFirstEmpty = true;
		objLB2.fillListBox();		
	}
	
	
	function defaultSetYearDay(){
		FillCalenderDay("selDtRetu", getFieldByID("selYrRetu").options[1].value);
	}
	
	function selYrDeptOnChange(){
		FillCalenderDay("selDtDept", getValue("selYrDept"));
		setField("selDtDept", strSeleDDay);
	}
	
	function selYrRetuOnChange(){
		FillCalenderDay("selDtRetu", getValue("selYrRetu"));
		setField("selDtRetu", strSeleRDay);
	}
	
	function selDtDeptOnChange(){
		strSeleDDay = getValue("selDtDept");
	}

	function selDtRetuOnChange(){
		strSeleRDay = getValue("selDtRetu");
	}	
	
	function selToLocOnChange(){
		if (getText("selToLoc") == strDash){
			getFieldByID("selToLoc").options[0].selected = true;
		}
	}
	
	function selFromLocOnChange(){
		if (getText("selFromLoc") == strDash){
			getFieldByID("selFromLoc").options[0].selected = true;
		}
	}
	
	function setDate(strDate, strID){
		var arrStrValue = strDate.split("/");
		switch (strID){
			case "0" : 
				setField("selYrDept", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrDeptOnChange();
				setField("selDtDept", arrStrValue[0]);
				break ;
			case "1" : 
				setField("selYrRetu", arrStrValue[1] + "/" + arrStrValue[2]);
				selYrRetuOnChange();
				setField("selDtRetu", arrStrValue[0]);
				break ;
		}
	}
	
	function chkReturnTrip_click(){
		if (getFieldByID("chkReturnTrip").checked){
			setField("selRVariance", intRDaysDef);
			Disable("selDtRetu", false);
			Disable("selYrRetu", false);
			Disable("selRVariance", false);
		}else{
			setField("selRVariance", "0");
			setField("selDtRetu", "");
			setField("selYrRetu", "");
			Disable("selDtRetu", true);
			Disable("selYrRetu", true);
			Disable("selRVariance", true);
			defaultSetYearDay();
		}
	}
	
	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 0 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.currentDate = strSysDate;
		switch (strID){
			case 0 : 
				if (getValue("selDtDept") != "" && getValue("selYrDept")){
					objCal1.currentDate = getValue("selDtDept") + "/" + getValue("selYrDept");
				}
				break;
			case 1 : 
			if (getFieldByID("chkReturnTrip").checked){
				if (getValue("selDtRetu") != "" && getValue("selYrRetu")){
					objCal1.currentDate = getValue("selDtRetu") + "/" + getValue("selYrRetu");
				}
			}else{
				return;
			}
			break;			
		}		
		objCal1.showCalendar(objEvent);
	}
	
	// --------------------- Maximum Passengers
	function selAdults_onChange(){
		var arrInfants = new Array();
		var intAdults = Number(getValue("selAdults"))// + Number(getValue("selChild"));
		var strCInfants = getValue("selInfants");
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrInfants; 
		objLB.textIndex = "0";
		objLB.id = "selInfants";
		objLB.fillListBox();
		
		setField("selInfants",  strCInfants);
	}
	
	function flightSearchBtnOnClick(intID){
		switch (intID){
			case 0 : 
				if (clientValidateFS()){
					var objFrm = getFieldByID("form1");
					objFrm.target = "_top";
					objFrm.action = "../public/showReservation.action";
					objFrm.submit();
				}
				break;
			case 1 : 
				var strPostData =  getValue("selLangunage") + "^RE"
				setField("hdnParamData", strPostData);
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
			case 2 :
				var strPostData = getValue("selLangunage") + "^SI"
				setField("hdnParamData", strPostData);			
				var objFrm = getFieldByID("form1");
				objFrm.action = appURL;
				objFrm.target = "_top";
				objFrm.submit();
				break;
		}
	}	
	
	
	function clientValidateFS(){
		if (getValue("selFromLoc") == ""){
			alert(arrError["ERR001"]);		
			getFieldByID("selFromLoc").focus();		
			return false;
		}
		
		if (getValue("selToLoc") == ""){
			alert(arrError["ERR002"]);
			getFieldByID("selToLoc").focus();
			return false;
		}		
		
		if (getValue("selCurrency") == ""){
			alert(arrError["ERR006"]);
			getFieldByID("selCurrency").focus();
			return false;
		}				
		
		if (getValue("selFromLoc") == getValue("selToLoc")){
			alert(arrError["ERR003"]);
			getFieldByID("selToLoc").focus();			
			return false;
		}
		
		if ((getValue("selDtDept") == "") || (getValue("selYrDept") == "")){
			alert(arrError["ERR007"]); 	
			getFieldByID("selDtDept").focus();		
			return false;
		}else{
			if (!CheckDates(strSysDate, getValue("selDtDept") + "/" + getValue("selYrDept"))){
				alert(arrError["ERR009"] + strSysDate + ".");
				getFieldByID("selDtDept").focus();	
				return false;
			}			
		}
		
		if (getFieldByID("chkReturnTrip").checked){
			if ((getValue("selDtRetu") == "") || (getValue("selYrRetu") == "")){
				alert(arrError["ERR008"]); 	
				getFieldByID("selDtRetu").focus();		
				return false;
			}
		
			if (!CheckDates(getValue("selDtDept") + "/" + getValue("selYrDept"), getValue("selDtRetu") + "/" + getValue("selYrRetu"))){
				alert(arrError["ERR004"]);
				getFieldByID("selDtRetu").focus();
				return false;
			}

		}

		if (getValue("selType") == ""){
			alert(arrError["ERR010"]); 	
			getFieldByID("selType").focus();		
			return false;
		}

		if (getValue("selCOS") == ""){
			alert(arrError["ERR011"]); 	
			getFieldByID("selCOS").focus();		
			return false;
		}
		
		strRDate = "";
		blnReturn = false;
		if (getValue("selDtRetu") != "" && getValue("selYrRetu") != ""){
			strRDate	= getValue("selDtRetu") + "/" + getValue("selYrRetu");
		}
		if (getFieldByID("chkReturnTrip").checked){blnReturn	= true;}
		
		var strPostData = getValue("selLangunage") + "^" + 
						  "AS^" +
					   getValue("selFromLoc") + "^" +
					   getValue("selToLoc") + "^" +
					   getValue("selDtDept") + "/" + getValue("selYrDept") + "^" +
					   strRDate + "^" +
					   getValue("selDVariance") + "^" +
					   getValue("selRVariance") + "^" +
					   getValue("selCurrency") + "^" +
					   getValue("selAdults") + "^" +
					   getValue("selChild") + "^" +
					   getValue("selInfants") + "^" +
					   getText("selFromLoc") + "^" +
					   getText("selToLoc") + "^" +
					   blnReturn + "^" +
					   getValue("selCOS")+ "^" + 
					   getValue("selType")+ "^" + 
					   getValue("selFareType");
		 
		  //alert(strPostData)
		//edit
		setField("hdnParamData", strPostData);
		return true;
	}
	
	function defaultDataLoad(){
		var dtDepDate = addDays(dtSysDate, Number(strDefDayDept));
		strDDate = dateChk(dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear());
		strSeleDDay = dtDepDate.getDate();
		
		var dtRetuDate = "";
		strRDate = "";
		strSeleRDay = "  /  /    ";
		if (String(strDefDayRetu) != ""){
			var dtRetuDate = addDays(dtSysDate, Number(strDefDayRetu));
			strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
			strSeleRDay = dtRetuDate.getDate();
			setField("chkReturnTrip", true);
		}
		
		setField("selDVariance", strDDays); 
		setField("selRVariance", strRDays);
		chkReturnTrip_click();
	}
	
	function dateOnBlur(strID, objC){
		var blnDateEntered = false;
		if (objC.value != ""){blnDateEntered = true;}
		dateChk(strID);
		if (objC.value == "" && blnDateEntered){
			alert(arrError["ERR005"]); 
		}
	}
	
	function cacheData(){
		setField("selFromLoc", strFrom);
		setField("selToLoc", strTo);
		
		var arrDtDept = strDDate.split("/");
		setField("selYrDept", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrDeptOnChange();
		setField("selDtDept", arrDtDept[0]);
		
		setField("selYrRetu", arrDtDept[1] + "/" + arrDtDept[2]);
		selYrRetuOnChange();
		setField("selDtRetu", arrDtDept[0]);
		
		if (getValue("selDtRetu") == ""){
			defaultSetYearDay();
		}
				
		setField("selDVariance", strDDays);
		setField("selRVariance", strRDays);
		setField("selCurrency", strSelCurr);
		
		getFieldByID("selFromLoc").focus();
	}
	
	// ------------------------ 
	function flightSearchOnLoad(){
		buildDropDowns();
		
		var arrAdults = new Array();
		var arrChild  = new Array();
		arrChild[0] = new Array();
		arrChild[0][0] = "0";
		for (var i = 1 ; i <= intMaxAdults ; i++){
			arrAdults[i - 1] = new Array();
			arrAdults[i - 1][0] = i;
			
			arrChild[i] = new Array();
			arrChild[i][0] = i;
		}
		var objLB = new listBox();
		objLB.dataArray = arrAdults; 
		objLB.textIndex = "0";
		objLB.id = "selAdults";
		objLB.fillListBox();
		
		objLB = new listBox();
		objLB.dataArray = arrChild; 
		objLB.textIndex = "0";
		objLB.id = "selChild";
		objLB.fillListBox();
		selAdults_onChange();
		
		// ------------------------ 
		var arrDays = new Array();
		for (var i = 0 ; i <= intDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		
		var objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selDVariance";
		objLB2.fillListBox();
		
		arrDays.length = 0
		for (var i = 0 ; i <= intRDays ; i++){
			arrDays[i] = new Array();
			arrDays[i][0] = i;
			arrDays[i][1] =  "+/- " + i ;			
		}
		objLB2 = new listBox();
		objLB2.dataArray = arrDays; 
		objLB2.id = "selRVariance";
		objLB2.fillListBox();

		Disable("selDtRetu", true);
		Disable("selYrRetu", true);
		Disable("selRVariance", true);
		
		defaultDataLoad()
		cacheData();
	}	
	flightSearchOnLoad();
	
	// ---------------- Calendar
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.borderColor = "#537425";
	objCal1.borderClass = "calendarBorder"
	objCal1.headerTextColor = "white";
	objCal1.noYears = "1";
	objCal1.blnArabic = false;
	objCal1.disableUpto = strSysDate;
	objCal1.disableFrom = getDaysInMonth(intEndMonth, intEndYear) + "/" + intEndMonth + "/" + intEndYear;
	objCal1.buildCalendar();
//-->
</script>
</body>
</html>
