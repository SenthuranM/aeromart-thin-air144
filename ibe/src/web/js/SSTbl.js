    /*
    *********************************************************
	    Description		: Soft Search Table Drop Down
	    Author			: Rilwan A. Latiff
	    Version			: 1.0
	    Careated on		: 04th May 2009
	    Last Modified	: 
    *********************************************************	
    */
    function filterDropDown(){
	    this.id = "";																				// ID
	    this.width = "100px";																		// Width
	    this.listWidth = "100px";																	// List Width
	    this.listHeight = "100px";																	// List Height
	    this.dataArray = new Array();                                                               // Data Array
	    this.valueIndex = 0;                                                                        // Value Index
	    this.textIndex = 1;                                                                         // Text Index
	    this.tabIndex = 1;                                                                          // tab Index
	    this.textHeight = 15;                                                                       // Text Height
	    this.preference = "";                                                                       // Preference Codes
	    this.wrapText = false;                                                                      // Wrap Text
	    this.currentStatus = false;                                                                 // Current expand collapse status
	    this.onClick = "";                                                                          // on Click Client function call
	    this.onSelect = "";																			// on Selec of the value
	    this.classBorder = "ssThinBorder";                                                          // Border Class
	    this.classLink = "ssHref";                                                                  // Href Class
	    this.classThinBG = "ssThinBG";                                                              // Thin Background
	    this.classThinBGSltd = "ssThinBGSltd";                                                      // Thin Background Selected
    	
	    this.value = "";                                                                            // Selected Value
	    this.text = "";                                                                             // Selected Text
    	
	    this.imgPath = "";
	    this.folderPath = "";
    	
	    this.showImg = "../images/Plus_no_cache.jpg";													                // Show Image
	    this.hideImg = "../images/Minus_no_cache.jpg";													                // Hide Image
	    this.closeImg = "../images/Close_Light_no_cache.jpg";                                                                // Close Image
	    this.dataPage = "FDTbl.htm";											                    // Data Container Page
    	
	    this.closeImgBufferScroll = 12;                                                             // Close Image Buffer scroll
	    this.closeImgBuffer = 15;                                                                   // Close Image Buffer
	    this.borderBuffer = 18;                                                                     // Border Buffer    
	    this.closeTop = "19px";                                                                     // Close Top Position
	    this.minRecords = 6;                                                                        // Minimum Records to split as 2 columns
    	
	    this.toolTipImage = "Click to View the list";												// Image Click Tooltip
	    this.defaultText = "-- Please Select --";													// Default Value
	    this.noMatchingText = "There are no cities matching your request.";                         // No Matching record found error message
    	
    	
    	
	    // Methods
	    this.displayFilterDropDown = _displayFilterDropDown											// Display the Schedule
	    this.focus = _txtControlFocus;                                                              // Control Focus
	    this.collapse = _collapseControl;                                                           // Collapse the Control
	    this.refresh = _refresh;                                                                    // Refresh
	    this.clearControl = _clearControl;															// Clear Controls Value
    	
	    if (!window._arrFD) window._arrFD = new Array();
	    window._arrFD[this.id] = this;
	    window._arrFD[window._arrFD.length] = this;
	    window._FDLayer	= "spnFDLayer_";
    	
	    this._allTextHTML = "";                                                                      // All Text HTML
	    this._blnDataiAvail = false;                                                                 // Data Available or not
	    this._blnDisplay = true;																	 // Display Status
	    this._objLoadTimer = null;                                                                   // Load Timer
	    this._intLastMove = -1;                                                                      // Last Move
	    this._intAllTotCount = 0;                                                                    // All Total Count
	    this._intRecCount = 0;                                                                       // Current List Total count
	    this._arrFilterd = new Array();                                                              // Filtered Array
	    this._filtered = false;                                                                      // Filtered or no
	    this._blnSltdApply = true;                                                                   // Sltd apply or not
	    this._blnAutoMove = false;
    }

    /*
     * Build the Drop Down
     * PUBLIC
     */
    function _displayFilterDropDown(){
	    var _objFD = this;
		
	    document.writeln('<span id="' + window._FDLayer  + '" style="visibility:hidden;"><\/span>');
	    var objCont = document.getElementById(window._FDLayer);
	    var _objFD = null;
	    objCont.arrFD = new Array();
    	
	    var strHTMLText = "";
	    for (var i = 0 ; i < window._arrFD.length ; i++){
		    objCont.arrFD[i] = window._arrFD[i];
		    _objFD = objCont.arrFD[i];
    		
		    strHTMLText = "";
		    strHTMLText = "<table border='0' cellpadding='0' cellspacing='0'>";
		    strHTMLText += "		<tr>";
		    strHTMLText += "			<td nowrap><input type='text' ";
		    strHTMLText += "						id='txt_" + _objFD.id + "' name='txt_" + _objFD.id + "' ";
		    strHTMLText += "						style='width:" + _objFD.width + "' ";
		    strHTMLText += "						value='" + _objFD.defaultText + "' ";
		    strHTMLText += "                        onKeyUp='_txtKeyUp(\"" + _objFD.id + "\", event)' ";
		    if (browser.isOpera){
		        strHTMLText += "                    onkeypress ='_txtKeyUp(\"" + _objFD.id + "\", event)' ";
		    }
		    strHTMLText += "                        onClick='_txtClick(\"" + _objFD.id + "\")' ";
		    strHTMLText += "                        tabIndex='" + (_objFD.tabIndex++) + "' ";
		    strHTMLText += "                        class='ssInput' ";
		    strHTMLText += "				><\/td><td nowrap><a href='#' onclick='_imgOnClick(" + '"' + _objFD.id + '"' + "); return false'><img id='img_" + _objFD.id + "' name='img_" + _objFD.id + "' ";
		    strHTMLText += "					src='" + _objFD.imgPath + _objFD.showImg + "' ";
		    strHTMLText += "					alt='' ";
		    strHTMLText += "					title='" + _objFD.toolTipImage + "' ";
		    strHTMLText += "                    tabIndex='-1'";
		    strHTMLText += "					border='0'><\/a><\/td>"
		    strHTMLText += "		<\/tr>";
		    strHTMLText += "	<\/table>";
		    strHTMLText += "	<iframe src='" + _objFD.folderPath + _objFD.dataPage + "?date="+new Date() +"' id='frm_" + _objFD.id + "' name='frm_" + _objFD.id + "' height='" + _objFD.listHeight + "' width='" + _objFD.listWidth + "' frameborder='0' scrolling='auto' class='" + _objFD.classBorder + "' style='display:none;'><\/iframe>";
		    strHTMLText += "    <span id='spn_Close_" + _objFD.id +  "' style='position:absolute;top:" + _objFD.closeTop + ";width:16px;left:" + _objFD.listWidth + ";display:none;'><a href='#' onclick='_collapseControl(" + '"' + _objFD.id + '"' + "); return false;'><img src='" + _objFD.imgPath + _objFD.closeImg + "' alt='' border='0'></span>";
            getFieldByID(_objFD.id).style.position = "absolute";
		    DivWrite(_objFD.id, strHTMLText);
    			
		    _objFD._objLoadTimer = setInterval("_refresh('" + _objFD.id + "')", 300);
	    }
    }

    /*
     * Get the Object
     * PRIVATE
     */
    function _getFDObject(strID){
	    var objCont = document.getElementById(window._FDLayer);
	    var _objFD = null;
	    for (var y = 0 ; y < objCont.arrFD.length ; y++){
		    if (objCont.arrFD[y].id == strID){
			    _objFD = objCont.arrFD[y];
			    break;
		    }
	    }
	    return _objFD;
    }

    /*
     * Image On Click
     * Public
     */
    function _imgOnClick(strID){
	    var _objFD = _getFDObject(strID);
	    var frmName = "frm_" + _objFD.id;
	    setDisplay(frmName, _objFD._blnDisplay);
	    setDisplay("spn_Close_" + _objFD.id, _objFD._blnDisplay);
	    if (_objFD._blnDisplay){
	        var strHTMLText = _objFD._allTextHTML;
            if (strHTMLText != ""){
                _objFD._blnDataiAvail = true;
            }
            _objFD._intRecCount = _objFD._intAllTotCount;
            _objFD._filtered = false;
            _showData(_objFD, strHTMLText);
            _objFD._blnDisplay = true;
	    }
    	
	    if (_objFD._blnDisplay){_objFD._blnDisplay = false}else{_objFD._blnDisplay=true;}
	    _changeImage(strID);
    }

    /*
     * Change Image
     * Private
     */
    function _changeImage(strID){
        var _objFD = _getFDObject(strID);
        if (_objFD.onClick != ""){
            eval(_objFD.onClick + "('" + _objFD.id + "')");	
	    }
	    
        if (_objFD._blnDisplay){
            setImage("img_" + _objFD.id, _objFD.imgPath + _objFD.showImg);
            if (getValue("txt_" + _objFD.id) == ""){
                setField("txt_" + _objFD.id, _objFD.defaultText);
            }else{
                _applyLastSltd(_objFD);
            }
            _objFD._blnSltdApply = true;
            _objFD.currentStatus = false;
        }else{
            setImage("img_" + _objFD.id, _objFD.imgPath + _objFD.hideImg);
            if (getValue("txt_" + _objFD.id) == _objFD.defaultText){
                setField("txt_" + _objFD.id, "");
            }
            setFocus("txt_" + _objFD.id);
            _objFD._blnSltdApply = false;
            _objFD.currentStatus = true;
        }
    }

    /*
     * Private
     * Txt Control on Focus;
     */
    function _txtControlFocus(){
        var _objFD = null;
        if (arguments.length == 0){
            _objFD = this;
        }else{
            _objFD = _getFDObject(arguments[0]);
        }
        if (_objFD != undefined || _objFD != null)
        	setFocus("txt_" + _objFD.id);
    }

    /*
     * Initialize all
     */
    function _initialize(_objFD){
        _objFD._allTextHTML = "";         
        _objFD._blnDataiAvail = false; 
        _objFD._blnDisplay = true;	
        _objFD._objLoadTimer = null;
        _objFD._intLastMove = -1;   
        _objFD._intAllTotCount = 0; 
        _objFD._intRecCount = 0;   
        
        setField("txt_" + _objFD.id, _objFD.defaultText);
        _collapseControl(_objFD.id);
    }
    
    /*
     * PUBLIC
     * Initilize 
     */
    function _clearControl(){
		var _objFD = this;
		setField("txt_" + _objFD.id, _objFD.defaultText);
		_objFD.value = "";           
	    _objFD.text = "";   
    }

    /* 
     * Private
     * Load Delay
     */ 
    function _refresh(){
        var _objFD = null;
        if (arguments.length == 0){
            _objFD = this;
        }else{
            _objFD = _getFDObject(arguments[0])
        }
        var strID = _objFD.id;
        if (frames["frm_" + strID].blnPgLoaded){
            clearInterval(_objFD._objLoadTimer);
            
            _initialize(_objFD);
            var strHTMLText = "";
            var _objData = null;
            if (_objFD.preference != ""){
                _reorgData(_objFD);            
            }
            _objData = _objFD.dataArray;
            
            var intTotRec = _objData.length;
            var strWrapText = "";
		    if (!_objFD.wrapText){strWrapText = "nowrap";}
    		
            if (intTotRec > 0){
                var i = 0;
                 _objFD._intAllTotCount = intTotRec;
                var intLen = intTotRec;
                var intColStart = null;
                if (intLen > _objFD.minRecords){
                    intLen = Math.round(intTotRec / 2);
                    if (intLen == 0){intLen++;}
                    if (intTotRec > intLen){
				        intColStart = intLen;
			        }
			    }
                strHTMLText += "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
                do{
                    strHTMLText += "<tr>";
                    strHTMLText += "        <td " + strWrapText + " width='50%' id='td_Col_" + i + "' name='td_Col_" + i + "'><font><a href='#' class='" + _objFD.classLink + "' onclick='parent._selectRecord(" + '"' + _objFD.id + '"' + "," + i + "); return false;'>" + _objData[i][_objFD.textIndex] + " (" + _objData[i][_objFD.valueIndex] + ")" + "<\/a><\/font><\/td>";
                    if (intColStart != null){
                        if (intColStart < intTotRec){
					        strHTMLText += "    <td  " + strWrapText + " width='50%' id='td_Col_" + intColStart + "' name='td_Col_" + intColStart + "'><font><a href='#' class='" + _objFD.classLink + "' onclick='parent._selectRecord(" + '"' + _objFD.id + '"' + "," + intColStart + "); return false;'>" + _objData[intColStart][_objFD.textIndex] + " (" + _objData[intColStart][_objFD.valueIndex] + ")" + "<\/a><\/font><\/td>";
					        if (_objFD.dataArray[intColStart][_objFD.valueIndex] == _objFD.value){
                                _objFD.text = _objFD.dataArray[intColStart][_objFD.textIndex];
                            }
					        intColStart++;
				        }
				    }
                    strHTMLText += "<\/tr>";
                    
                    if (_objFD.dataArray[i][_objFD.valueIndex] == _objFD.value){
                        _objFD.text = _objFD.dataArray[i][_objFD.textIndex];
                    }
                    
                    i++;
                }while(--intLen);
                strHTMLText += "<\/table>";
                _objFD._allTextHTML = strHTMLText;
            }
            
            frames["frm_" + strID].strID = _objFD.id;
            frames["frm_" + strID].DivWrite("spnDataDes", _objFD._allTextHTML);
            
            if (_objFD.text != ""){
                setField("txt_" + _objFD.id, _getCodNText(_objFD));
            }
        }
    }

    /*
     *
     */
    function _reorgData(_objFD){
        var arrPref = _objFD.preference.toUpperCase().split(",");
        var intPrefLen = arrPref.length
        var arrNewData = new Array();
        var intLen = _objFD.dataArray.length;
        var i = 0;
        var x = 0;
        var intLenP = intPrefLen;
        var arrRemove = new Array();
        do{
            intLenP = intPrefLen;
            x = 0;
            do{
                if (arrPref[x] == _objFD.dataArray[i][_objFD.valueIndex].toUpperCase()){
                    arrNewData.push(_objFD.dataArray[i]);
                    arrRemove.push(i);
                }
                x++;
            }while(--intLenP);
            i++;
        }while(--intLen);
        
        // Create the New Data List
        intLen = _objFD.dataArray.length;
        var strRemove = "," + arrRemove.join() + ",";
        i = 0;
        do{
            if (strRemove.indexOf("," + i + ",") == -1){
                arrNewData.push(_objFD.dataArray[i]);
            }
            i++;
        }while(--intLen)
        _objFD.dataArray = arrNewData;
    }

    /*
     * key up
     */
    function _txtKeyUp(strID, objEvent){
        if (browser.isIE){objEvent = window.event;}
	    var strKeyValue = objEvent.keyCode ;
	    var _objFD = _getFDObject(strID);
	    if (_objFD._blnDisplay){
	        _filterContent(_objFD);
	    }
	    var intID = 0;
	    var intIDLast = _objFD._intLastMove;
    	
        switch (strKeyValue){
            case 38 :
                if (_objFD._blnDataiAvail){
                    intID = --_objFD._intLastMove;
                    _setCurMove(_objFD, intID, intIDLast);
                }
                break;
            case 40 :
                if (_objFD._blnAutoMove){
                   _moveScroll(_objFD, _objFD._intLastMove);
                   _objFD._blnAutoMove = false;
                   return false;
                }
                
                if (_objFD._blnDataiAvail){
                    intID = ++_objFD._intLastMove;
                    _setCurMove(_objFD, intID, intIDLast);
                }
                break;
            case 13 :
                 _selectRecord(strID, _objFD._intLastMove);
                break;
            default :
                _filterContent(_objFD);
                break;
        }
    }

    /*
     * Select Record
     * Private
     */
    function _selectRecord(strID, intID){
        var _objFD = _getFDObject(strID);
        var intCol = null;
        if (intID >= 0){
            if (!_objFD._filtered){
                intCol = intID;
            }else{
                intCol = _objFD._arrFilterd[intID][0];
            }
        }
        _collapseControl(_objFD.id);
        if (intCol != null){
            _objFD.value = _objFD.dataArray[intCol][_objFD.valueIndex];
            _objFD.text = _objFD.dataArray[intCol][_objFD.textIndex];
            setField("txt_" + _objFD.id, _getCodNText(_objFD));
            
            if (_objFD.onSelect != ""){
				eval(_objFD.onSelect + "('" + _objFD.id + "')");	
            }
        }
    }

    /*
     *
     */
    function _setCurMove(_objFD, intID, intIDLast){
        var blnProceed = true;
        if ((intID + 1) > _objFD._intRecCount){
            blnProceed = false;
            _objFD._intLastMove = _objFD._intRecCount - 1;
        }
        if (intID < 0){
            blnProceed = false;
            _objFD._intLastMove = 0;
        }
        
        if (blnProceed){
            if (intIDLast > -1){
                frames["frm_" + _objFD.id].setStyleClass("td_Col_" + intIDLast, _objFD.classThinBG);
            }
            frames["frm_" + _objFD.id].setStyleClass("td_Col_" + intID, _objFD.classThinBGSltd);
            _moveScroll(_objFD, intID);
        }
    }

    /*
     * Move the cursor
     */
    function _moveScroll(_objFD, intID){
        if (intID > 5){
            var intScrollHeight = _objFD.textHeight * intID;
            
            if (_objFD._intRecCount > _objFD.minRecords){
                if (intID > _objFD.minRecords){
                    var intDivRec = Math.round(_objFD._intRecCount / 2);
                    if (intID >= intDivRec){
                        intScrollHeight = _objFD.textHeight * ((intID - intDivRec) - 1); 
                    }
                }
            }
        }
        frames["frm_" + _objFD.id].window.scrollTo(0, intScrollHeight);
    }

    /*
     * Filter Content
     */
    function _filterContent(_objFD){
        var strHTMLText = "";
        var strValue = getValue("txt_" + _objFD.id);
        if (strValue == _objFD.defaultText){
            strValue = "";
        }
        var blnFiltered = true;
        var blnAutoMove = false;
        _objFD._blnDataiAvail = false;
        if (strValue == ""){
            blnFiltered = false;
        }else{
            if (getValue("txt_" + _objFD.id) == (_getCodNText(_objFD))){
                blnFiltered = false;
                _objFD._blnAutoMove = true;
            }
        }
        
        frames["frm_" + _objFD.id].window.scrollTo(0, 0);    
        if (!blnFiltered){
            strHTMLText = _objFD._allTextHTML;
            if (strHTMLText != ""){
                _objFD._blnDataiAvail = true;
            }
            _objFD._intRecCount = _objFD._intAllTotCount;
            _objFD._filtered = false;
        }else{
            strValue = strValue.toUpperCase();
            var intValLen = strValue.length;
            
            var _objData = _objFD.dataArray;
            var intLen = _objData.length;
            
            _objFD._filtered = true;
            _objFD._arrFilterd = new Array();
            var x = 0;
            var i = 0;
            var strDesc = "";
            var strCode = "";
            var strFullText = "";
            
            var strATxt = "";
            var strAVal = "";
            var strDescS= "";
            var strDescM= "";
            var strDescE= "";
            var intFoundPos = -1;
            do{
                strDesc = "";
                strCode = "";
                
                strAVal = _objData[i][_objFD.valueIndex].toUpperCase();
                strATxt = _objData[i][_objFD.textIndex].toUpperCase();
                
                if (strAVal.substr(0, intValLen) == strValue){
	                strCode = "(<b>" + _objData[i][_objFD.valueIndex].substr(0, intValLen) + "<\/b>" + _objData[i][_objFD.valueIndex].substr(intValLen) + ")";
	            }
	            if (strATxt.substr(0, intValLen) == strValue){
	                strDesc = "<b>" + _objData[i][_objFD.textIndex].substr(0, intValLen) + "<\/b>" + _objData[i][_objFD.textIndex].substr(intValLen);
	            }
	            
    	        if (intValLen > 1){
					if (strDesc == ""){
						intFoundPos = strATxt.indexOf(strValue);
						if (intFoundPos != -1){
							strATxt = _objData[i][_objFD.textIndex];
							strDescS= strATxt.substr(0, intFoundPos);
							strDescM= "<b>" + strATxt.substr(intFoundPos, intValLen) + "<\/b>";
							strDescE= strATxt.substr(intFoundPos + intValLen);
	                        
							strDesc = strDescS + strDescM + strDescE;
						}
					}
	    	        
					if (strCode == ""){
						intFoundPos = strAVal.indexOf(strValue);
						if (intFoundPos != -1){
							strAVal = _objData[i][_objFD.valueIndex];
							strDescS= strAVal.substr(0, intFoundPos);
							strDescM= "<b>" + strAVal.substr(intFoundPos, intValLen) + "<\/b>";
							strDescE= strAVal.substr(intFoundPos + intValLen);
	                        
							strCode = "(" + strDescS + strDescM + strDescE + ")";
						}
					}
				}
    	        
	            if ((strCode == "") && (strDesc == "")){
	                strFullText = _objData[i][_objFD.textIndex] + " (" + _objData[i][_objFD.valueIndex] + ")";
	                strFullText = strFullText.toUpperCase();
	                if (strFullText.substr(0, intValLen).toUpperCase() == strValue){
	                    strCode = "(" + _objData[i][_objFD.valueIndex] + ")";
	                    strDesc = _objData[i][_objFD.textIndex];
	                }
	            }
    	        
	            if ((strDesc != "") || (strCode != "")){
	                if (strCode == ""){strCode = "(" + _objData[i][_objFD.valueIndex] + ")";}
	                if (strDesc == ""){strDesc = _objData[i][_objFD.textIndex];}
	                _objFD._arrFilterd[x] = new Array(i, strDesc + " " + strCode);
		            x++;
	            }
	            i++;
            }while(--intLen);
    		
            var intTotRec = _objFD._arrFilterd.length;
            if (intTotRec > 0){
	            i = 0;
	            _objFD._intRecCount = intTotRec;
	            var intColStart = null;
	            intLen = intTotRec;
	            if (intLen > _objFD.minRecords){
                    intLen = Math.round(intTotRec / 2);
                    if (intLen == 0){intLen++;}
                    
                    if (intTotRec > intLen){
		                intColStart = intLen;
	                }
                }
                            
                var strWrapText = "";
	            if (!_objFD.wrapText){strWrapText = "nowrap";}
    	
                strHTMLText += "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
                do{
                    strHTMLText += "<tr>";
                    strHTMLText += "        <td " + strWrapText + " width='50%' id='td_Col_" + i + "' name='td_Col_" + i + "'><font><a href='#' class='" + _objFD.classLink + "' onclick='parent._selectRecord(" + '"' + _objFD.id + '"' + "," + i + "); return false;'>" + _objFD._arrFilterd[i][1] + "<\/a><\/font><\/td>";
                    if (intColStart != null){
                        if (intColStart < intTotRec){
                            strHTMLText += "    <td " + strWrapText + " width='50%' id='td_Col_" + intColStart + "' name='td_Col_" + intColStart + "'><font><a href='#' class='" + _objFD.classLink + "' onclick='parent._selectRecord(" + '"' + _objFD.id + '"' + "," + intColStart + "); return false;'>" + _objFD._arrFilterd[intColStart][1] + "<\/a><\/font><\/td>";
			                intColStart++;
		                }
		            }
                    strHTMLText += "<\/tr>";
                    i++;
                }while(--intLen);
                strHTMLText += "<\/table>";
                _objFD._blnDataiAvail = true;
            }else{
                strHTMLText = "<font>" + _objFD.noMatchingText + "<\/font>"
            }
        }
        _showData(_objFD, strHTMLText);
    }

    /*
     * private
     * Show the Data
     */
    function _showData(_objFD, strHTMLText){
        _objFD._intLastMove = -1;
       	frames["frm_" + _objFD.id].DivWrite("spnDataDes", strHTMLText);
        _objFD._blnDisplay = false;
        setDisplay("frm_" + _objFD.id, true);
        setDisplay("spn_Close_" + _objFD.id, true);
        getFieldByID("frm_" + _objFD.id).style.width = _objFD.listWidth;
        var intLeft = frames["frm_" + _objFD.id].getPageInnerWidth()
        var intWidthB = intLeft;
        if (frames["frm_" + _objFD.id].getPageInnerHeight() > _objFD.listHeight.split("px")[0]){
            intWidthB = intWidthB + _objFD.borderBuffer;
            intLeft = intLeft - _objFD.closeImgBufferScroll;
        }else{
            intLeft = intLeft - _objFD.closeImgBuffer;
        }
        getFieldByID("frm_" + _objFD.id).style.width = intWidthB;
        getFieldByID("spn_Close_" + _objFD.id).style.left = intLeft;
        
        if (!_objFD._filtered){
            if (_objFD._blnSltdApply){
                if (_objFD.value != ""){
                    var intLen = _objFD.dataArray.length;
                    var i = 0;
                    do{
                        if (_objFD.dataArray[i][_objFD.valueIndex] == _objFD.value){
                            _objFD.text = _objFD.dataArray[i][_objFD.textIndex];
                            break;
                        }
                        i++;
                    }while(--intLen);
                    setField("txt_" + _objFD.id, _getCodNText(_objFD));
                    selectAll("txt_" + _objFD.id);

                    _objFD._intLastMove = i;
                    _setCurMove(_objFD, _objFD._intLastMove, -1);
                }
            }
        }
        
        _changeImage(_objFD.id);
    }

    /*
     * Text Box Click
     */
    function _txtClick(strID){
        var _objFD = _getFDObject(strID);
        if (!_objFD._blnDisplay){
            _imgOnClick(strID);
        }else{
            if (getValue("txt_" + _objFD.id) == _objFD.defaultText){
                setField("txt_" + _objFD.id, "");
            }
        }
    }

    /*
     * collapse the control
     */
    function _collapseControl(){
        var _objFD = null;
        if (arguments.length == 0){
            _objFD = this;
        }else{
            _objFD = _getFDObject(arguments[0]);
        }
        _objFD._blnDisplay = true;
        setDisplay("frm_" + _objFD.id, false);
        setDisplay("spn_Close_" + _objFD.id, false);
        _changeImage(_objFD.id);
    }

    /*
     * Apply Last Selected
     */
    function _applyLastSltd(_objFD){
        if (_objFD.text != ""){
            var strValue = _getCodNText(_objFD);
            if (getValue("txt_" + _objFD.id) != _objFD.defaultText){
                setField("txt_" + _objFD.id, strValue);
            }
        }
    }

    /*
     * Code N Text
     * PRIVATE
     */
    function _getCodNText(_objFD){
        var strValue = "";
        if (_objFD.text != ""){
            strValue = _objFD.text + " (" + _objFD.value + ")"
        }
        return strValue;
    }

    // ----------------------------- End of File ---------------------------------