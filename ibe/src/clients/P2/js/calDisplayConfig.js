/*
 * DEFAULT CONFIGURATION
 * If Client specific configuration is needed please modify in the file inside client/js
 */
var calDisplayConfig= {};
calDisplayConfig.layout = {};
calDisplayConfig.layout.displayFlightDetailsInCal = false; //Display Selected Flight Details with Calendar 
calDisplayConfig.layout.calWidth = 625; //Actual Calendar width without Next Previous
calDisplayConfig.layout.moreThanTwoFlight = "SHOW"; // If we get More Than one Flight per Day {SHOW/SCROLL} - *Use the same case
calDisplayConfig.layout.termsConditionPos = "bottom"; // Terms and Conditions Location 
calDisplayConfig.layout.additionalNavigationEnabled = false; // enable << and >> navigation for multiple days
calDisplayConfig.layout.newWindowForFareRuleDetails = true; //Display Fare Rule Details in a tooltip or window popup

calDisplayConfig.design = {};
calDisplayConfig.design.displayGradientFare = UI_Top.holder().GLOBALS.calendarGradianView; //True or False or call sys-params
calDisplayConfig.design.ItemsToDispalyInCal = {arrivalTime:false,departureTime:false,radio:true,flightNum:false,stopOverCount:false, displayNonstop:false};
calDisplayConfig.design.loadingImage = "../images/loading-cal_no_cache.gif";
calDisplayConfig.design.gapInDaysPx = 2;
calDisplayConfig.design.showTotalFare = false; //show Total fare true or false
calDisplayConfig.design.fallsDecimals = false; // show Decimals true or false
calDisplayConfig.design.selctedCurrency = false // Show Selected Currency 
calDisplayConfig.design.blinks = {webonly:false,onlyNSeat:false}
calDisplayConfig.design.noStopColumn = true;//set stop over column visibility
calDisplayConfig.design.fareColumnWidth = 60//set percentage of fare column
calDisplayConfig.design.blinks = {webonly:false,onlyNSeat:false}