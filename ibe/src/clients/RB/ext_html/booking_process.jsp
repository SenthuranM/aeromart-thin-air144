<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<div class="outerSteps" style="height:30px">
<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
		<tr>
			<td class="stepsItem selected" style="width:210px;" align="center">
				<div><span class="icon"></span><font class="fntBold"><fmt:message key="msg.pg.process1"/></font></div>
			</td>
			<td class="stepsItem selected" style="width:240px;" align="center">
				<div><span class="icon"></span><font class="fntBold"><fmt:message key="msg.pg.process2"/></font></div>
			</td>
			<td class="stepsItem selected" style="width:210px;" align="center">
				<div><span class="icon"></span><font class="fntBold"><fmt:message key="msg.pg.process3"/></font></div>
			</td>
		</tr>
	</table>
</div>

