<tr><td colspan='3'><div class="kamFooter">

<div class="bottompart fl">
  <div class="fl" style="width:970px; padding:0px 5px 0px 5px;">
    <ul class="bottomul fl">
      <li><a href="http://www.flykamair.com/cmspage.php?id=2"><span></span>Corporate Information</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=3"><span></span>Management</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=4"><span></span>Airline Partners</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=5"><span></span>Career</a></li>
      <li><a href="http://www.flykamair.com/flight-status.php"><span></span>Flight Status</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=34"><span></span>Cargo</a></li>
    </ul>
    <ul class="bottomul fl">
	    <li><a href="http://www.flykamair.com/cmspage.php?id=25"><span></span>Our Fleet</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=26"><span></span>In-Flight Experience</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=27"><span></span>Seating</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=28"><span></span>Dining</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=29"><span></span>Lounges</a></li>
		</ul>
    <ul class="bottomul fl">
      <li><a href="http://www.flykamair.com/cmspage.php?id=21"><span></span>TV Commercial</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=22"><span></span>In Flight Magazine</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=23"><span></span>Media</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=30"><span></span>Flight Deck Crew</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=31"><span></span>Cabin Crew</a></li>
		</ul>
		<ul class="bottomul fl">
      <li><a href="http://www.flykamair.com/cmspage.php?id=12"><span></span>Passport &amp; Visa</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=13"><span></span>Terminal Information</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=14"><span></span>Baggage Policy</a></li>      
      <li><a href="http://www.flykamair.com/cmspage.php?id=15"><span></span>Baggage Transfers</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=32"><span></span>Safety</a></li>
		</ul>
    <ul class="bottomul fl">
	    <li><a href="http://www.flykamair.com/cmspage.php?id=33"><span></span>Operation</a></li>
      <li><a href="http://www.flykamair.com/route-map.php"><span></span>Route Map</a></li>
      <li><a href="http://www.flykamair.com/schedule.php"><span></span>Flight Schedule</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=18"><span></span>Dubai Stopover</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=19"><span></span>Special Offers</a></li>
		</ul>
    <ul class="bottomul fl">
      <li><a href="http://www.flykamair.com/cmspage.php?id=7"><span></span>Contact Us</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=8"><span></span>Worldwide Offices</a></li>
      <li><a href="http://www.flykamair.com/cmspage.php?id=9"><span></span>Travel Agents</a></li>      
      <li><a href="http://www.flykamair.com/cmspage.php?id=10"><span></span>UAE Visa</a></li>
      <li><a href="http://www.flykamair.com/complains.php"><span></span>Complains</a></li>
		</ul>
  </div>
  <div class="cl" style="height:8px;"></div>
  <div class="bttpartcopyright fl">
    <div class="cl" style="border-top:solid 1px #c8c8c8; height:0px; width:940px;"></div>
    <div class="cl" style="height:3px;"></div>
    Copyright &copy; <?php echo date("Y");?> - flykamair.com, All Rights Reserved.</div>
  <div class="cl" style="height:9px;"></div>
</div>

</div></td></tr>
<script type="text/javascript" src="../js/ddsmoothmenu.js"></script>
<script type="text/javascript">
<!--
	//var msgFun = function(){
	//	return true;
	//} 
	//var obj = $(".kamFooter");
	//UI_commonSystem.loadExternalContant(window.location.protocol+"//"+baseURL+"bottom.php",msgFun,obj)
-->
</script>