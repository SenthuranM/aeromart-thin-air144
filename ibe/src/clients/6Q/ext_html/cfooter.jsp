<tr><td colspan='3' class="footer">
    <div class="wrap">
        <div class="copyrights">

            <span>&copy; Cham Wings</span>
            <div class="menu-copyrights-english-container">
                <ul id="menu-copyrights-english" class="menu">
                    <li id="menu-item-332">
                        <a href="https://www.chamwings.com/en/privacy-policy/">Privacy Policy</a>
                    </li>
                    <li id="menu-item-331">
                        <a href="https://www.chamwings.com/en/legal/">Legal</a>
                    </li>
                    <li id="menu-item-333">
                        <a href="https://www.chamwings.com/en/terms-and-conditions/">Terms and Conditions</a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</td></tr>
