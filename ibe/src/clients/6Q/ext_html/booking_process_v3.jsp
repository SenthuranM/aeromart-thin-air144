<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<table width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="margin-bottom: 15px;">
    <tr><td align="center">
        <table width='502' border='0' cellpadding='2' cellspacing='0' align="center" class="StepsTable">
            <tr>
                <td width="210" align="center" class="stepsItem"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process1"/></font></td>
                <td width="200" align="center" class="stepsItem"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process2"/></font></td>
                <td width="240" align="center" class="stepsItem"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process3"/></font></td>
                <td width="210" align="center" class="stepsItem"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process4"/></font></td>
            </tr>
        </table>
    </td>
    </tr>
</table>
