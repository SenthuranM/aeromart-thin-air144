
var calDisplayConfig= {};
calDisplayConfig.layout = {};
calDisplayConfig.layout.displayFlexiInCal = false;
calDisplayConfig.layout.displayFlightDetailsInCal = true;
calDisplayConfig.layout.calWidth = 773; //Actual Calendar width with out Next Previous
calDisplayConfig.layout.moreThanTwoFlight = "SHOW"; // If we get More Than one Flight per Day {SHOW/SCROLL} - *Use the same case
calDisplayConfig.layout.termsConditionPos = "bottom";
calDisplayConfig.layout.additionalNavigationEnabled = true; // enable << and >> navigation for multiple days
calDisplayConfig.layout.newWindowForFareRuleDetails = true; //Display Fare Rule Details in a tooltip or window popup

calDisplayConfig.design = {};
calDisplayConfig.design.displayGradientFare = UI_Top.holder().GLOBALS.calendarGradianView; //True or False or call sys-params
calDisplayConfig.design.ItemsToDispalyInCal = {arrivalTime:false,departureTime:true,radio:false,flightNum:false,stopOverCount:true, stopOverDisplayBy:"default", displayNonstop:true};
calDisplayConfig.design.loadingImage = "../images/loading-cal_no_cache.gif";
calDisplayConfig.design.gapInDaysPx = 0;
calDisplayConfig.design.showTotalFare = false; //show Total fare true or false
calDisplayConfig.design.fallsDecimals = true; // show Decimals true or false
calDisplayConfig.design.selctedCurrency = true; // Show Selected Currency
calDisplayConfig.design.selectedWidth = 153;//set the selected day width
calDisplayConfig.design.noStopColumn = true;//set stop over column visibility
calDisplayConfig.design.fareColumnWidth = 60;//set percentage of fare column
calDisplayConfig.design.totelinSelectedWhileBase = false;
calDisplayConfig.design.blinks = {webonly:false,onlyNSeat:false}
calDisplayConfig.design.currencyAfterAmount=true;