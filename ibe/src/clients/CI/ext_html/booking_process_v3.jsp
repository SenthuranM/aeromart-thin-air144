<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<div class="top_bok_bar"><table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
	<tr>
		<td width="20" class="processLeft">&nbsp;</td>
		<td width="150" align="center" class="stepsItem">
			<label ><fmt:message key="msg.pg.process1"/></label>
		</td>
        <td width="13" align="center"><img src="../images/v3/arrow_br.png">
		</td>
		<td  width="150" align="center" class="stepsItem">
			<label ><fmt:message key="msg.pg.process2"/></label>
		</td>
        <td width="13" align="center"><img src="../images/v3/arrow_br.png">
		</td>
		<td  width="150" align="center" class="stepsItem">
			<label ><fmt:message key="msg.pg.process3"/></label>
		</td>
        <td width="13" align="center"><img src="../images/v3/arrow_br.png">
		</td>
		<td  width="150" align="center" class="stepsItem">
			<label ><fmt:message key="msg.pg.process4"/></label>
		</td>
		<td width="20" class="processRight">&nbsp;</td>
	</tr>
</table></div>
<div style="height:10px">&nbsp;</div>

