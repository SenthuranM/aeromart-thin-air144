<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
		<tr>
			<td align="center" valign="top" class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" class="first" height="67px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process1.v3"/></font></td>
					</tr>
                    <tr>
						<td align="center" valign="middle" class="peocess-text_dot"><font class="fntBold">1</font></td>
  </tr>
</table>
			</td>
			<td align="center" valign="top" class="stepsItem selected">
			  <table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" height="67px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process2.v3"/></font></td>
					</tr>
                    <tr>
						<td align="center" valign="middle" class="peocess-text_dot"><font class="fntBold">2</font></td>
  </tr>
</table>
			</td>
			<td align="center" valign="top" class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" height="67px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process3.v3"/></font></td>
					</tr>
                    <tr>
						<td align="center" valign="middle" class="peocess-text_dot"><font class="fntBold">3</font></td>
  </tr>
</table>
			</td>
			<td align="center" valign="top" class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" height="67px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process4.v3"/></font></td>
					</tr>
                    <tr>
						<td align="center" valign="middle" class="peocess-text_dot"><font class="fntBold">4</font></td>
  </tr>
</table>
			</td>
		</tr>
	</table>
