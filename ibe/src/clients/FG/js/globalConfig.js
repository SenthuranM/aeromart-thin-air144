
var globalConfig= {};
globalConfig.layOut = {};
globalConfig.layOut.agreementInfoPossition = 'normal';
globalConfig.layOut.newRegUser = false;
globalConfig.stikyType = 'normal';
globalConfig.showStopOvers =true;
globalConfig.termsLink = null;//if no external link null only accept
globalConfig.noOfMonthsinCalendaraPopup = 1;//calendar months count
globalConfig.calendaImagePath = '../images/Calendar2_no_cache.gif';
globalConfig.trackingLoadOnPaxPage = false;

/**
 * ui_paymentGWManger to configure the payment gateways in front end
 * there are no connection with the backend app params
 */

ui_paymentGWManger = {}; 
ui_paymentGWManger.showPGS = false; //to Show/Hide integrated PGs in the select box, just for know integrated PGWs in IBE for testing
ui_paymentGWManger.showCardList = false; //to Show/Hide the payment methods (card list) in the page
ui_paymentGWManger.selectDefaultCard = true; // to select a payment method by default
ui_paymentGWManger.defaultCardindex = 0; //if selectDefaultCard true selected index of the card list
ui_paymentGWManger.iframesHeight = {1:900} ; //external PGs shown in iframe height map can be added in order to the PG id  