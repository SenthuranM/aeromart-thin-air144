<html>
<head>
<style type="text/css">
/* jAlert */  
body{
	margin: 0px;padding: 0px;overflow: hidden;
	font-family:Arial,Helvetica,sans-serif;
	font-size: 12px;
}
input{
	background:#efefef;
	border:1px solid #e4e4e4;
	color:#333;
	cursor:pointer;
	font-family:tahoma;
	font-size:11px;
	font-weight:bold;
	height:20px;
	text-align:center;
	text-transform:capitalize;
}
#popup_container {
	font-family:Arial,Helvetica,sans-serif;
	font-size: 12px;
	min-width: 300px; /* Dialog will be no smaller than this */
	max-width: 600px; /* Dialog will wrap after this width */
	background: #FFF;
	border: solid 2px #d5241e;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	color: #fff;
}
.popuo_title_bg{
	background:url("../images/panelBGall_no_cache.jpg") repeat scroll 0 -80px transparent;
	border-bottom:1px solid #FEDA79;
	padding:0px 5px;
	height: 25px;
}

.popuo_title_bg #popup_title {
	color:#444;
	cursor:default;
	font-size:14px;
	font-weight:bold;
	line-height:1.75em;
	margin:0;
	text-align:left;
	position: relative;
	display: inline;
	width:auto;
	float: left;
}

.popuo_title_bg span.popuo_close{
	background: url("../images/popupClose_no_cache.gif") no-repeat;
	height: 18px;width: 15px;display: block;
	float: right;position: relative;top:4px;
}

#popup_content {
	background: 16px 16px no-repeat url(../images/info_no_cache.gif);
	padding: 1em 1.75em 0.5em 1.75em;
	margin: 0em;
	color: #000;
}

#popup_content.alert {
	background-image: url(../images/important_no_cache.gif);
}

#popup_content.confirm {
	background-image: url(../images/help_no_cache.gif);
}

#popup_content.prompt {
	background-image: url(../images/info_no_cache.gif);
}

#popup_message {
	padding-left: 48px;
}

#popup_panel {
	text-align: center;
	margin: 1em 0em 0em 1em;
}

#popup_prompt {
	margin: .5em 0em;
}
</style>
	
</head>
<body style="margin: 0;padding: 0">
	
	<div class="popuo_title_bg">
		<h1 id="popup_title"></h1>
		<span class="popuo_close"></span>
	</div>
	<div style="clear:both"></div>
		  <div id="popup_content">
		  <div id="popup_message"></div>
		  <div id="popup_panel"></div>
	</div>
</body>
</html>
