<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
	<tr>
		<td class="stepsItem">
				<fmt:message key="msg.pg.process1"/>
		</td>
		<td class="gap">
			<img src="../images/arrow_br.jpg" alt=""/>
		</td>
		<td class="stepsItem">
				<fmt:message key="msg.pg.process2"/>
		</td>
		<td class="gap">
			<img src="../images/arrow_br.jpg" alt=""/>
		</td>
		<td class="stepsItem">
			<fmt:message key="msg.pg.process3"/>
		</td>
	</tr>
</table>

