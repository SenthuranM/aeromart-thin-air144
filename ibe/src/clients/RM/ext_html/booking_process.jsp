<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<table width="100%" cellspacing="0" cellpadding="2" border="0" align="center">
    <tr><td align="center">
        <table width='502' border='0' cellpadding='2' cellspacing='0' align="center" class="StepsTable">
            <tr>
                <td width="40"><font class="fntEnglish fntBold">&nbsp;</font></td>
                <td width="5" align="center"><img src="../images/AA045_no_cache.jpg"></td>
                <td width="80" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process1"/></font></td>
                <td width="5" align="center"><img src="../images/AA045_no_cache.jpg"></td>
                <td width="70" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process2"/></font></td>
                <td width="5" align="center"><img src="../images/AA045_no_cache.jpg"></td>
                <td width="100" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process3"/></font></td>
                <td width="5" align="center"><img src="../images/AA045_no_cache.jpg"></td>
                <td width="90" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process4"/></font></td>
                <td width="5" align="center"><img src="../images/AA045_no_cache.jpg"></td>
                <td width="80" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process5"/></font></td>
                <td><font class="fntEnglish">&nbsp;</font></td>
            </tr>
            <tr>
                <td align="center" colspan="12">
                    <div class="stepsItem">
                        <img src="../images/NF02_no_cache.jpg">
                    </div>
                    <div class="stepsItem">
                        <img src="../images/NF03_no_cache.jpg">
                    </div>
                    <div class="stepsItem">
                        <img src="../images/NF04_no_cache.jpg">
                    </div>
                    <div class="stepsItem">
                        <img src="../images/NF05_no_cache.jpg">
                    </div>
                </td>
            </tr>
        </table>
    </td>
    </tr>
</table>
