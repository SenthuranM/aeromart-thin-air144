<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<tr>
<td colspan="3" class="topBannerMB">
	<table cellspacing="0" cellpadding="0" border="0" align="center" class="sortable-ul" height="85">
		<tr>
			<td width="50%" valign="top">
				<div id="lnkHomeBanner" style="margin: 20px 10px"></div>
			</td>
			<td align="right" width="50%" valign="middle" style="padding: 0 20px; display: none;">
			<c:if test='${sessionScope.sessionDataDTO.customerId == "-1"}'>
				<a href="javascript:gotoAction('SignIn')" style="font-size: 14px;text-decoration: underline;">Sign in</a>&nbsp;&nbsp;
				<a href="javascript:gotoAction('Reg')" style="font-size: 14px;text-decoration: underline;">Register</a>
			</c:if>
			</td>
		</tr>
	</table>
	

	<script type="text/javascript">
	<!--
	
	
	function gotoAction(flg){
		var th1=$("<input type='hidden'>").attr({
			"id":"hdnParamData",
			"name":"hdnParamData",
			"value":(flg.toLowerCase()=="signin")?"EN^SI":"EN^RE",
		});
		
		var th2=$("<input type='hidden'>").attr({
			"id":"hdnCarrier",
			"name":"hdnCarrier",
			"value":"GT",
		});
		
		var tFrom = $("<form></form>")
		.attr({
			"method":"post",
			"id":(flg.toLowerCase()=="signin")?"formRegister":"formLogin",
			"name":(flg.toLowerCase()=="signin")?"formRegister":"formLogin",
			"target":"_top",
			"action":"../../ibe/public/showReservation.action"
		});
		var temAction = tFrom[0].action;
		temAction = temAction.replace("http:", "https:");
		var tsplit = temAction.split(":");
		//This will not required for production setup its only for testing
		if(tsplit.length>2){
			if (!isNaN(parseInt(tsplit[2],10))){
				var urlsplit = tsplit[2].split("/");
				if(urlsplit[0]=="8080"){
					urlsplit[0] = "8443";
				}else if(tsplit[3]=="8180"){
					urlsplit[0] = "8543";
				}
				tsplit[2] = urlsplit.join("/");
			}
		}
		tFrom[0].action = tsplit.join(":");
		tFrom.append(th1, th2).submit();
	}
	
		
	-->
	</script>
</td>
</tr>