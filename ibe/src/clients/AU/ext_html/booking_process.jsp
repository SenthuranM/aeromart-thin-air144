<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
		<table width="100%" cellspacing="0" cellpadding="2" border="0" align="center">
			<tr><td align="center">
					<table width='620' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
						<tr>
							<td align="center" width="620">
								<div class="stepsItem">
									<table border='0' cellpadding='2' cellspacing='0' align="center" class="processbar select">
										<tr>
											<td width="40"><font class="fntEnglish fntBold">&nbsp;</font></td>
											<td width="108" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process1"/></font></td>
											<td width="102" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process2"/></font></td>
											<td width="120" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process3"/></font></td>
											<td width="118" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process4"/></font></td>
											<td width="80" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process5"/></font></td>
											<td><font class="fntEnglish">&nbsp;</font></td>
										</tr>
									</table>
								</div>
								<div class="stepsItem">
									<table border='0' cellpadding='2' cellspacing='0' align="center" class="processbar fares">
										<tr>
											<td width="40"><font class="fntEnglish fntBold">&nbsp;</font></td>
											<td width="108" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process1"/></font></td>
											<td width="92" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process2"/></font></td>
											<td width="136" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process3"/></font></td>
											<td width="118" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process4"/></font></td>
											<td width="80" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process5"/></font></td>
											<td><font class="fntEnglish">&nbsp;</font></td>
										</tr>
									</table>
								</div>
								<div class="stepsItem">
									<table border='0' cellpadding='2' cellspacing='0' align="center" class="processbar pax">
										<tr>
											<td width="40"><font class="fntEnglish fntBold">&nbsp;</font></td>
											<td width="108" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process1"/></font></td>
											<td width="102" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process2"/></font></td>
											<td width="126" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process3"/></font></td>
											<td width="120" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process4"/></font></td>
											<td width="80" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process5"/></font></td>
											<td><font class="fntEnglish">&nbsp;</font></td>
										</tr>
									</table>
								</div>
								<div class="stepsItem">
									<table border='0' cellpadding='2' cellspacing='0' align="center" class="processbar pay">
										<tr>
											<td width="40"><font class="fntEnglish fntBold">&nbsp;</font></td>
											<td width="108" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process1"/></font></td>
											<td width="102" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process2"/></font></td>
											<td width="126" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process3"/></font></td>
											<td width="120" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process4"/></font></td>
											<td width="80" align="left"><font class="fntEnglish fntBold"><fmt:message key="msg.pg.process5"/></font></td>
											<td><font class="fntEnglish">&nbsp;</font></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
