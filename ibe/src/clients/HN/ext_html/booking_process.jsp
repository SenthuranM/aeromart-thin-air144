<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
		<tr>
			<td align="center" valign="top" class="stepsItem selected">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="first" height="30px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process1.v3"/></font></td>
					</tr>
                    <tr>
						<td align="right" valign="middle" class="peocess-text_dot"></td>
  </tr>
</table>
			</td>
			<td align="center" valign="top" class="stepsItem selected">
			  <table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" height="30px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process2.v3"/></font></td>
					</tr>
                    <tr>
						<td align="right" valign="middle" class="peocess-text_dot"></td>
  </tr>
</table>
			</td>
			<td align="center" valign="top" class="stepsItem selected">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" height="30px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process3.v3"/></font></td>
					</tr>
                    <tr>
						<td align="right" valign="middle" class="peocess-text_dot"></td>
  </tr>
</table>
			</td>
			<td align="center" valign="top" class="stepsItem selected">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" height="30px">
					<tr>
						<td align="center" valign="middle" class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process4.v3"/></font></td>
					</tr>
                    <tr>
						<td align="right" valign="middle" class="peocess-text_dot"></td>
  </tr>
</table>
			</td>
		</tr>
	</table>
