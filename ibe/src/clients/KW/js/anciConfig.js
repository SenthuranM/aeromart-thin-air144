var anciConfig= {};
anciConfig.layout = {};
anciConfig.layout.anciOrder = ['baggageTr','insTr', 'seatTr', 'mealTr', 'ssrTr', 'halaTr', 'apTransferTr']; // Ancillary order with same names
anciConfig.layout.addModifyAncillary =  [[{id:'ANCI'}], [{id:'PAYMENT'}]];
anciConfig.layout.modifySegment = [[{id:'ANCI'}], [{id:'MODIFYCONFIRM'}], [{id:'PAYMENT'}]];
anciConfig.layout.createReservation = [[{id:'PAX'}], [{id:'ANCI'}], [{id:'PAYMENT'}]];
anciConfig.layout.makePayment =  [[{id:'PAYMENT'}]];
anciConfig.layout.requoteFlow = [[{id:'ANCI'}], [{id:'PAYMENT'}]];

anciConfig.paymentOnAnici = false;

anciConfig.mealview = "New-view"; //New-view or Tab-view
anciConfig.seatview = "New-view"; //New-view or Tab-view
anciConfig.insViewType = "New-view"; //New-view or any other for old view
anciConfig.baggageview = "New-view";
anciConfig.menuView = "N"; //N or Y, N does not display the menu
anciConfig.showInsCharge =true;
anciConfig.insChargeDesc ="";
anciConfig.paxNewLayout = true; //new layout for AA if old view use false
anciConfig.anciContAlertType = 'ALL'; // ALL or ANY
anciConfig.showSeatMessageTermsCheckBox = false;

anciConfig.displayCCMsgAlert = false; //configuration for displaying alert for Credit card messages
anciConfig.onHoldvisibility = 1; //configuration for setting for the on-hold booking visibility 
								// 1 = display on top of the panel
								// 2 = display with card types 