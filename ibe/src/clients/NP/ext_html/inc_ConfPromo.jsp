<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<div id="promoContent">
<style>
	.largeT{
		font-family:arial;
		font-size: 16px;
		font-weight: bold;
		font-style: itelc;
	}
</style>
<br/>

	<div id="divPromos">
		<div id="divHotel">
			<table width="98%" border="0"><tr>
			<td valign="top" align="center">
				<img src="../images/hotels_no_cache.jpg" alt="Hotels"/>
			</td>
			<td valign="top" >
				<label id="lblHotelHeading" class="largeT">Hotels</label><br/><br/>
				<img src="../images/AA175_no_cache.gif" alt="" align="absmiddle"/>&nbsp;
				<label id="lblHotelText1">263,948 properties worldwide!</label><br/>
				<img src="../images/AA175_no_cache.gif" alt="" align="absmiddle"/>&nbsp;
				<label id="lblHotelText2">41,000+ destinations!</label><br/>
				<img src="../images/AA175_no_cache.gif" alt="" align="absmiddle"/>&nbsp;
				<label id="lblHotelText3">No booking fees! </label><br/>
				<img src="../images/AA175_no_cache.gif" alt="" align="absmiddle"/>&nbsp;
				<label id="lblHotelText4">Best Price Guaranteed!</label><br/><br/>
				<a href="http://www.booking.com/index.html?aid=364197;label=airarabia-confpage" target="_blank" >
					<u:hButton name="btnBookNowHotels" id="btnBookNowHotels" value="Book Now"  cssClass="redNormal" inLineStyles="cursor:pointer"/>
				</a>
			</td>
			</tr></table>
			
			<!-- <iframe name="frmhotel" id="frmhotel" src="showBlank" frameborder="0" width="700px" height="250px">
			</iframe> -->
		</div>
		<div style="margin:10px 10px 0px 10px;border-top:1px solid #ccc">&nbsp;</div>
		<div id="divRentCar">
			<table width="98%"><tr>
			<td valign="top" align="center">
				<img src="../images/cars_no_cache.jpg" alt="Cars"/>
			</td>
			<td valign="top" >
				<label id="lblCarsHeading" class="largeT">From AED 70 /day </label><br/><br/>
				<img src="../images/AA175_no_cache.gif" alt="" align="absmiddle"/>&nbsp;
				<label id="lblCarsText1">800 car rental suppliers!</label><br/>
				<img src="../images/AA175_no_cache.gif" alt="" align="absmiddle"/>&nbsp;
				<label id="lblCarsText2">30,000 locations worldwide! </label><br/>
				<img src="../images/AA175_no_cache.gif" alt="" align="absmiddle"/>&nbsp;
				<label id="lblCarsText3">Great car hire deals! </label><br/><br/>
				<a href="https://www.cartrawler.com/airarabia/?clientId=420667" target="_blank" >
					<u:hButton name="btnBookNowCars" id="btnBookNowCars" value="Book Now"  cssClass="redNormal" inLineStyles="cursor:pointer"/>
				</a>
			</td>
			</tr></table>
		</div>
		<div style="margin:10px 10px 0px 10px;border-top:1px solid #ccc">&nbsp;</div>
		
		<div id="socialNet" style="display:none">
		 	<div class="socialWrapper">
 				<div class="socialBtnArea">
					<label class='hdFontColor' style="margin:2px 10px;"> Share this on,</label>
					<div id="divSocial" style="margin:2px 10px;">
						<div class="btnSocial fbook refFacebook" id="btnFaceBook" style="display: none"></div>
						<div class="btnSocial twiter refTwitter" id="btnTwitter" style="display: none"></div>
						<div class="btnSocial googlep refGoogle" id="btnGooglep" style="display: none"></div>
						<div class="btnSocial linkIn refLinkedIn" id="btnLinkIn" style="display: none"></div>
					</div>
				    <div style="clear:both"></div>
			  </div>
		 </div>
	 </div>
	</div>
</div>

