<tr><td colspan='3' class="footer">
<div id="footer">
  <div class="left"> &copy; 2009 Jubba Airways. All rights reserved. <br>
    <a href="/sitemap.aspx">Site Map</a> | <a href="/privacy-statement.aspx">Privacy Statement</a> | <a href="/terms-of-service.aspx">Terms of Service</a> | <a href="/disclaimer.aspx">Disclaimer</a> </div>
  <div class="right"> <span><a target="_blank" href="http://reservations.jubbaairways.com/agents">Ticket Agent Login</a> | <a target="_blank" href="http://jubba-airways.com/cargoagent.asp">Cargo Agent Login</a> | <a target="_blank" href="http://mail.jubba-airways.com/webmail">Staff Mail</a></span> <br>
    <a target="_blank" href="http://www.graphicallyspeaking.ca/">Vancouver Web Design</a> by <a target="_blank" href="http://www.graphicallyspeaking.ca/">Graphically Speaking</a></div>
  <br class="clear">
</div>
</td></tr>
