<%--
Author: Baladewa
// ******** Changing the Insurance Content to display in XBE Ancillary **** //
1. Get a copy of the "ibe/src/web/ext_html/ins_IBEContent.jsp" file and place in 
	to the XBE clients ext_html folder (if not exist create a folder in Client folder).
2. Be sure not change any of the ID in the existing elements and change the only the 
	HTML as client expect.
3. what ever additional (client specific) functionalities included in the client's ins_content.jsp
	with out breaking the flow
 --%>

<table border='0' cellpadding='1' cellspacing='1' width='100%' class="ancipanelBody">
<tr>
	<td class='alignLeft'><label id='lblInsuranceHD' class='fntBold hdFontColor'>
	<span class="ttip" id="insuranceAnciOfferDescription" role="">
								<label id="insuranceAnciOfferTitle"></label>
							</span>
	</label></td>
</tr>
<tr>
	<td class='rowGap' colspan="2"></td>
</tr>
<tr>
	<td class='alignLeft'><label id='lblInsuranceDesc'></label></td>
</tr>
<tr>
	<td class='rowGap' colspan="2"></td>
</tr>
</table>
</td>
	<td class='pnlMidR'><img src="../images/spacer_no_cache.gif"/></td>
</tr>
<tr id="insBannerRow" class="BannerRow" style="display:none"><td colspan="3"><img src="<fmt:message key="msg.ancillary.banner.ins"/>" border="0" width="680"/></td></tr>
<tr>
	<td class="pnlMid pnlGroupbottom" colspan="3"><img src="../images/spacer_no_cache.gif"/></td>
</tr>
<tr>
<td class='pnlMidL'><img src="../images/spacer_no_cache.gif"/></td>
<td class='pnlMid' valign="top">
	<table border='0' cellpadding='1' cellspacing='1' width='650' class="ancipanelBody">
	<tr>
	<tr><td class="insuranceViewChange">
	<table border='0' cellpadding='0' cellspacing='0' width='100%'>
		<tr>
			<td width="50%"><table>
				<tr>
					<td width='2%' valign="top"><input type='radio' name='radAnciIns' id='radAnciIns_Y'/></td>
					<td width='98%' class='alignLeft'>
					<label id='lblInsuranceYes'></label><br>
					<label id='lblInsuranceChDesc'></label>&nbsp;<label style='display:none' id='spnInsCost'></label>
					</td>
				</tr>
				<tr>
					<td width='2%'><input type='radio' name='radAnciIns' id='radAnciIns_N'/></td>
					<td width='98%' class='alignLeft'><label id='lblInsuranceNo'></label></td>	
				</tr>
			</table>
			</td>
		</tr>
	</table></td></tr>
	<tr>
			<td class='rowGap' colspan="2"></td>
		</tr>
		<tr><td align='center'>
			<div class='alignLeft insCoverNote'><label id='insCoverInfo1'></label></div>
		</td></tr>
		<tr>
			<td class='rowGap' colspan="2"></td>
	</tr>
	<tr>
		<td class='alignLeft'>
			<div id="insTCCHKDiv" class='insTCChk'></div>
			<label id='lblInsuranceTNC'>
				(*) This travel insurance is underwritten by Chartis Inc. member companies or third party affiliates licensed to provide travel insurance in the country of origin of travel. Click here to view the Zest Travel Secure 
				<a href="#" class="fntDefault fntBold">Terms &amp; Conditions</a>
			</label>
		</td>
	</tr>
	<tr>
		<td  class="alignRight" colspan="2"> 
			<img src='../images/v3/ibeInsLogo.jpg' alt='ACE Insurance' />
		</td>
	</tr>
	<tr>
		<td id="insuranceLoad"></td>
	</tr>
	</table>


