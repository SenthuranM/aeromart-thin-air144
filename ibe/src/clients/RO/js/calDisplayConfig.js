
var calDisplayConfig= {};
calDisplayConfig.layout = {};
calDisplayConfig.layout.displayFlexiInCal = false;
calDisplayConfig.layout.displayFlightDetailsInCal = false;
calDisplayConfig.layout.calWidth = 660; //Actual Calendar width with out Next Previous
calDisplayConfig.layout.moreThanTwoFlight = "SHOW"; // If we get More Than one Flight per Day {SHOW/SCROLL} - *Use the same case
calDisplayConfig.layout.termsConditionPos = "default";
calDisplayConfig.layout.additionalNavigationEnabled = false; // enable << and >> navigation for multiple days
calDisplayConfig.layout.navHeight = 102;
calDisplayConfig.layout.newWindowForFareRuleDetails = true; //Display Fare Rule Details in a tooltip or window popup

calDisplayConfig.design = {};
calDisplayConfig.design.displayGradientFare = false;//top.GLOBALS.calendarGradianView; //True or False or call sys-params
calDisplayConfig.design.ItemsToDispalyInCal = {arrivalTime:false,departureTime:false,radio:true,flightNum:false,stopOverCount:false,displayNonstop:false};
calDisplayConfig.design.loadingImage = "../images/Loading-cal_no_cache.gif";
calDisplayConfig.design.gapInDaysPx = 4;
calDisplayConfig.design.showTotalFare = false; //show Total fare true or false
calDisplayConfig.design.fallsDecimals = false; // show Decimals true or false
calDisplayConfig.design.selctedCurrency = false // Show Selected Currency
calDisplayConfig.design.selectedWidth = 115;//set the selected day width
calDisplayConfig.design.noStopColumn = true;//set stop over column visibility
calDisplayConfig.design.fareColumnWidth = 60//set percentage of fare column
calDisplayConfig.design.blinks = {webonly:false,onlyNSeat:false}