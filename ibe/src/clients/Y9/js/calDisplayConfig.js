
var calDisplayConfig= {};
calDisplayConfig.layout = {};
calDisplayConfig.layout.displayFlexiInCal = false;
calDisplayConfig.layout.displayFlightDetailsInCal = true;
calDisplayConfig.layout.calWidth = 561; //Actual Calendar width with out Next Previous
calDisplayConfig.layout.moreThanTwoFlight = "SHOW"; // If we get More Than one Flight per Day {SHOW/SCROLL} - *Use the same case
calDisplayConfig.layout.termsConditionPos = "default";
calDisplayConfig.layout.additionalNavigationEnabled = false; // enable << and >> navigation for multiple days

calDisplayConfig.design = {};
calDisplayConfig.design.displayGradientFare = UI_Top.holder().GLOBALS.calendarGradianView; //True or False or call sys-params
calDisplayConfig.design.ItemsToDispalyInCal = {arrivalTime:false,departureTime:true,radio:true,flightNum:true,stopOverCount:true,displayNonstop:true};
calDisplayConfig.design.loadingImage = "../images/Loading-cal_no_cache.gif";
calDisplayConfig.design.gapInDaysPx = 0;
calDisplayConfig.design.showTotalFare = false; //show Total fare true or false
calDisplayConfig.design.fallsDecimals = false; // show Decimals true or false
calDisplayConfig.design.selctedCurrency = true; // Show Selected Currency
calDisplayConfig.design.selectedWidth = 129;
calDisplayConfig.design.noStopColumn = true;//set stop over column visibility
calDisplayConfig.design.fareColumnWidth = 60//set percentage of fare column
calDisplayConfig.design.blinks = {webonly:false,onlyNSeat:false}
calDisplayConfig.design.blinks = {webonly:false,onlyNSeat:false}