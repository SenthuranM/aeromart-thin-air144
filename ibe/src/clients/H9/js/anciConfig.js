/*
 * DEFAULT CONFIGURATION
 * If Client specific configuration is needed please modify in the file inside client/js
 */
var anciConfig = {};
anciConfig.layout = {};
anciConfig.layout.anciOrder = [ 'baggageTr','seatTr', 'insTr', 'mealTr','ssrTr','halaTr', 'flexiTr', 'apTransferTr']; // Ancillary order with same names
anciConfig.layout.addModifyAncillary =  [[{id:'ANCI'}], [{id:'PAYMENT'}]];
anciConfig.layout.modifySegment = [[{id:'ANCI'}], [{id:'MODIFYCONFIRM'}], [{id:'PAYMENT'}]];
anciConfig.layout.createReservation = [[{id:'PAX'}], [{id:'ANCI'}], [{id:'PAYMENT'}]];
anciConfig.layout.makePayment =  [[{id:'PAYMENT'}]];
anciConfig.layout.requoteFlow = [[{id:'ANCI'}], [{id:'PAYMENT'}]];

anciConfig.paymentOnAnici = false;

anciConfig.mealview = "New-view"; //New-view or Tab-view
anciConfig.seatview = "New-view"; //New-view or Tab-view
anciConfig.insViewType = "New-view"; //New-view or any other for old view
anciConfig.menuView = "N"; //N or Y, N does not display the menu
anciConfig.baggageview = "New-view"; //New-view or Tab-view
anciConfig.showInsCharge =true;
anciConfig.insTCCheck = false;
anciConfig.showSeatMessageTermsCheckBox = false;

anciConfig.anciContAlertType = 'ANY'; // ALL or ANY

anciConfig.displayCCMsgAlert = false; //configuration for displaying alert for Credit card messages

anciConfig.paxNewLayout = true; //new layout for AA if old view use false

anciConfig.onHoldvisibility = 2; //configuration for setting for the on-hold booking visibility 
								// 1 = display on top of the panel
								// 2 = display with card types  
anciConfig.termandConditiononPayment = false; //to hide and show terms and condition on payment