<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<table width="100%" cellspacing="0" cellpadding="2" border="0" align="center">
	<tr><td align="center">
		<table width='100%' border='0' cellpadding='2' cellspacing='0' align="center" class="StepsTable">
			<tr>
				<td width="180" align="left" class="stepsItem selected">
					<img src="../images/AA175_no_cache.gif" align="absmiddle">
					<font class="fntEnglish fntBold">
						<fmt:message key="msg.pg.process1.v3"/>
					</font></td>
				<td width="193" align="left" class="stepsItem">
					<img src="../images/AA175_no_cache.gif" align="absmiddle">
					<font class="fntEnglish fntBold">
						<fmt:message key="msg.pg.process2.v3"/>
					</font>
				</td>
				<td width="180" align="left" class="stepsItem">
					<img src="../images/AA175_no_cache.gif" align="absmiddle">
					<font class="fntEnglish fntBold">
						<fmt:message key="msg.pg.process3.v3"/>
					</font>
				</td>
				<td width="180" align="left" class="stepsItem">
					<img src="../images/AA175_no_cache.gif" align="absmiddle">
					<font class="fntEnglish fntBold">
						<fmt:message key="msg.pg.process4.v3"/>
					</font>
				</td>
			</tr>
		</table>
	</td>
	</tr>
</table>