<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
	<tr>
		<td width="20" class="processLeft">&nbsp;</td>
		<td width="150" align="center" class="stepsItem">
			<font ><fmt:message key="msg.pg.process1.v3"/></font>
		</td>
		<td  width="150" align="center" class="stepsItem">
			<font ><fmt:message key="msg.pg.process2.v3"/></font>
		</td>
		<td  width="150" align="center" class="stepsItem">
			<font ><fmt:message key="msg.pg.process3.v3"/></font>
		</td>
		<td  width="150" align="center" class="stepsItem">
			<font ><fmt:message key="msg.pg.process4.v3"/></font>
		</td>
		<td width="20" class="processRight">&nbsp;</td>
	</tr>
</table>
<div style="height:10px">&nbsp;</div>

