var anciConfig= {};
anciConfig.layout = {};
anciConfig.layout.anciOrder = ['insTr', 'seatTr', 'mealTr', 'ssrTr', 'halaTr','flexiTr']; // Ancillary order with same names
anciConfig.layout.addModifyAncillary =  [[{id:'ANCI'}], [{id:'PAYMENT'}]];
anciConfig.layout.modifySegment = [[{id:'ANCI'}], [{id:'MODIFYCONFIRM'}], [{id:'PAYMENT'}]];
anciConfig.layout.createReservation = [[{id:'PAX'}], [{id:'ANCI'}], [{id:'PAYMENT'}]];

anciConfig.paymentOnAnici = false;

anciConfig.mealview = "Tab-view"; //New-view or Tab-view
anciConfig.seatview = "Tab-view"; //New-view or Tab-view
anciConfig.insViewType = "oldview"; //New-view or any other for old view
anciConfig.menuView = "N"; //N or Y, N does not display the menu
anciConfig.showInsCharge =true;
anciConfig.insChargeDesc ="";

anciConfig.anciContAlertType = 'ALL'; // ALL or ANY
anciConfig.showSeatMessageTermsCheckBox = false;

anciConfig.displayCCMsgAlert = false; //configuration for displaying alert for Credit card messages
anciConfig.onHoldvisibility = 1; //configuration for setting for the on-hold booking visibility 
								// 1 = display on top of the panel
								// 2 = display with card types 