package com.isa.thinair.ibe.core.web.v2.action.agent;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * Load data for agent registration
 * 
 * @author Gavinda Nanayakkara
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowAgentRegisterDetailAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ShowAgentRegisterDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private String countryInfo;

	private String stationInfo;

	private String countryPhoneInfo;

	private String areaPhoneInfo;

	private String stationCountryInfo;

	private Map<String, String> errorInfo;

	private String acclaeroClientIdentifier;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			countryInfo = JavaScriptGenerator.getCountryHtml();
			stationInfo = SelectListGenerator.createStationHtml();
			errorInfo = ErrorMessageUtil.getAgentProfileErrors(request);
			String[] phoneInfoArray = SelectListGenerator.createTelephoneString();
			countryPhoneInfo = phoneInfoArray[0];
			areaPhoneInfo = phoneInfoArray[1];
			stationCountryInfo = SelectListGenerator.createStationCountryArray();
			acclaeroClientIdentifier = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER);
		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowAgentProfileDetailAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowAgentProfileDetailAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getCountryInfo() {
		return countryInfo;
	}

	public String getStationInfo() {
		return stationInfo;
	}

	public void setStationInfo(String stationInfo) {
		this.stationInfo = stationInfo;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public String getStationCountryInfo() {
		return stationCountryInfo;
	}

	public void setStationCountryInfo(String stationCountryInfo) {
		this.stationCountryInfo = stationCountryInfo;
	}

	public String getAcclaeroClientIdentifier() {
		return acclaeroClientIdentifier;
	}

	public String getCountryPhoneInfo() {
		return countryPhoneInfo;
	}

	public String getAreaPhoneInfo() {
		return areaPhoneInfo;
	}

}
