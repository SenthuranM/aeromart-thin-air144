package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airproxy.api.LccSubOperationConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.ibe.api.dto.FFIDChange;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Chethiya Palliayguruge
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class UpdatePaxFFIDAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(UpdatePaxFFIDAction.class);

	private boolean success = true;

	private String messageTxt;

	private String hdnPNR;

	private boolean blnGroupPNR;

	private String version;

	private Collection<FFIDChange> newFFIDList;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {

			Customer customer = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));

			LCCClientReservation reservation = ReservationUtil.loadProxyReservation(hdnPNR, blnGroupPNR, getTrackInfo(), false,
					(customer != null) ? true : false, null, null, false);
			
			LCCClientReservation oldReservation = populateOldLccReservation(reservation);

			Set<LCCClientReservationPax> lccClientReservationPaxs = reservation.getPassengers();
			for (LCCClientReservationPax lccClientReservationPax : lccClientReservationPaxs) {
				for (FFIDChange ffidChange : newFFIDList) {
					if (ffidChange.getPaxSequence().equals(lccClientReservationPax.getPaxSequence().toString())) {
						lccClientReservationPax.getLccClientAdditionPax().setFfid(ffidChange.getPaxFFID());
					}
				}
			}

			Collection<String> grantedOperations = new HashSet<String>();
			grantedOperations.add(LccSubOperationConstants.ModifyReservation.ALTER_PAX_INFO);

			ModuleServiceLocator.getAirproxyReservationBD().modifyPassengerInfo(reservation, oldReservation, blnGroupPNR,
					getClientInfoDTO(), AppIndicatorEnum.APP_IBE.toString(), true, false, grantedOperations, getTrackInfo());

		} catch (ModuleException me) {
			success = false;
			messageTxt = I18NUtil.getMessage(me.getExceptionCode(), SessionUtil.getLanguage(request));
			log.error("UpdateReservationContactDetailAction==>" + me);
		} catch (Exception re) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("UpdateReservationContactDetailAction==>" + re);
		}
		return forward;
	}

	private LCCClientReservation populateOldLccReservation(LCCClientReservation lccClientReservation) {
		LCCClientReservation oldLccClientReservation = new LCCClientReservation();
		oldLccClientReservation.setPNR(lccClientReservation.getPNR());
		oldLccClientReservation.setLastUserNote(lccClientReservation.getLastUserNote());
		oldLccClientReservation.setVersion(lccClientReservation.getVersion());
		oldLccClientReservation.setItineraryFareMask(lccClientReservation.getItineraryFareMask());
		Collection<LCCClientReservationPax> colpaxs = lccClientReservation.getPassengers();
		for (LCCClientReservationPax lccClientReservationPax : colpaxs) {
			oldLccClientReservation.addPassenger(lccClientReservationPax);
		}
		return oldLccClientReservation;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setBlnGroupPNR(boolean blnGroupPNR) {
		this.blnGroupPNR = blnGroupPNR;
	}

	public void setVersion(String version) {
		this.version = version;
	}


	public String getHdnPNR() {
		return hdnPNR;
	}

	public void setHdnPNR(String hdnPNR) {
		this.hdnPNR = hdnPNR;
	}

	public boolean isBlnGroupPNR() {
		return blnGroupPNR;
	}

	public String getVersion() {
		return version;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<FFIDChange> getNewNameList() {
		return newFFIDList;
	}

	public void setNewNameList(Collection<FFIDChange> newNameList) {
		this.newFFIDList = newNameList;
	}
}
