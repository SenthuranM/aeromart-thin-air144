package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.CustomizedItineraryUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class SendEmailAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(SendEmailAction.class);
	private String hdnPNRNo;
	private boolean isGroupPNR;
	private String itineraryLanguage;
	private boolean success = true;
	private String messageTxt;
	private String operationType;
	private String viewMode;

	private static String ACTION_EMAIL = "EMAIL";
	private static String ACTION_PRINT = "PRINT";

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;

		try {
			sendEmail();
		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("SendEmailAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("SendEmailAction==>", ex);
		}

		return forward;
	}

	/**
	 * Send Email
	 * 
	 * @throws ModuleException
	 */
	private void sendEmail() throws ModuleException {

		String pnr = hdnPNRNo;
		String station = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		boolean includeBaggageAllowance = false;
		boolean isbaggageEnable = false;
		boolean isbaggageAllowanceRemarksEnable = false;

		LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, isGroupPNR, itineraryLanguage, request, false, false);

		/*
		 * Need to set this to true for itinerary.
		 */
		pnrModesDTO.setLoadOriginCountry(true);

		String strBaggageAllowanceRemarks = IBEModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.SHOW_BAGGAGE_ALLOWANCE_REMARKS);
		String strBaggage = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE);

		if (strBaggageAllowanceRemarks != null && strBaggageAllowanceRemarks.trim().equals("Y")) {
			isbaggageAllowanceRemarksEnable = true;
		}
		if (strBaggage != null && strBaggage.trim().equals("Y")) {
			isbaggageEnable = true;
		}
		if (isbaggageAllowanceRemarksEnable && !isbaggageEnable) {
			includeBaggageAllowance = true;
		}

		CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
		commonItineraryParam.setItineraryLanguage(itineraryLanguage);
		commonItineraryParam.setIncludePaxFinancials(true);
		commonItineraryParam.setIncludePaymentDetails(true);
		if (AppSysParamsUtil.showChargesPaymentsInItinerary()) {
			commonItineraryParam.setIncludeTicketCharges(true);
		} else {
			commonItineraryParam.setIncludeTicketCharges(false);
		}
		commonItineraryParam.setIncludePaxContactDetails(AppSysParamsUtil.showPassenegerContactDetailsInItinerary());
		commonItineraryParam.setIncludeStationContactDetails(AppSysParamsUtil.showStationContactDetailsInItinerary());
		commonItineraryParam.setIncludeBaggageAllowance(includeBaggageAllowance);
		commonItineraryParam.setStation(station);
		commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
		commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(isGroupPNR));
		commonItineraryParam.setIncludeTermsAndConditions(true);
		commonItineraryParam.setOperationType(operationType);

		// ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
		// getTrackInfo());

		/*
		 * CustomizedItineraryUtil.itineraryCompose(request, pnr, "", ACTION_EMAIL, isGroupPNR, false,
		 * isShowCustomizedItinerary(), commonItineraryParam, getTrackInfo(), pnrModesDTO);
		 */

		CommonItineraryParamDTO commonItineraryParamDTO = null;
		List<CommonItineraryParamDTO> itineraryDtoList = null;
		if (isShowCustomizedItinerary()) {
			itineraryDtoList = CustomizedItineraryUtil.itineraryCompose(request, ACTION_EMAIL, commonItineraryParam,
					getTrackInfo(), pnrModesDTO);
		} else {
			commonItineraryParamDTO = commonItineraryParam;
		}

		if (itineraryDtoList != null) {
			for (CommonItineraryParamDTO itineraryParamDTO : itineraryDtoList) {
				getItinerary(itineraryParamDTO, pnrModesDTO, getTrackInfo(), false, "", ACTION_EMAIL);
			}
		} else {
			getItinerary(commonItineraryParamDTO, pnrModesDTO, getTrackInfo(), false, "", ACTION_EMAIL);
		}

	}

	/**
	 * @return the hdnPNRNo
	 */
	public String getHdnPNRNo() {
		return hdnPNRNo;
	}

	/**
	 * @param hdnPNRNo
	 *            the hdnPNRNo to set
	 */
	public void setHdnPNRNo(String hdnPNRNo) {
		this.hdnPNRNo = hdnPNRNo;
	}

	/**
	 * 
	 * @return the itineraryLanguage
	 */
	public String getItineraryLanguage() {
		return itineraryLanguage;
	}

	/**
	 * 
	 * @param itineraryLanguage
	 *            the itineraryLanguage to set
	 */
	public void setItineraryLanguage(String itineraryLanguage) {
		this.itineraryLanguage = itineraryLanguage;
	}

	/**
	 * 
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * 
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * 
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * 
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isGroupPNR() {
		return isGroupPNR;
	}

	public void setGroupPNR(boolean isGroupPNR) {
		this.isGroupPNR = isGroupPNR;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}

	public boolean isShowCustomizedItinerary() {
		return AppSysParamsUtil.getShowCustomizedItinerary();
	}

	public String getItinerary(CommonItineraryParamDTO commonItineraryParam, LCCClientPnrModesDTO pnrModesDTO,
			TrackInfoDTO trackInfoDTO, boolean individualPax, String selectedPax, String action) throws ModuleException {
		String itinerary = "";
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(hdnPNRNo);
		if (individualPax) {
			String paxs[] = selectedPax.split(",");
			for (String pax : paxs) {
				if (pax != null || !pax.equals("")) {
					commonItineraryParam.setSelectedPaxDetails(pax);
					if (action.equals(ACTION_EMAIL)) {
						ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
								trackInfoDTO);
					} else if (action.equals(ACTION_PRINT)) {
						itinerary += ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
								commonItineraryParam, reservationSearchDTO, trackInfoDTO);
					}
				}
			}
		} else {
			commonItineraryParam.setSelectedPaxDetails(selectedPax);
			if (action.equals(ACTION_EMAIL)) {
				ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
						trackInfoDTO);
			} else if (action.equals(ACTION_PRINT)) {
				itinerary += ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
						commonItineraryParam, reservationSearchDTO, trackInfoDTO);
			}
		}

		return itinerary;
	}

}
