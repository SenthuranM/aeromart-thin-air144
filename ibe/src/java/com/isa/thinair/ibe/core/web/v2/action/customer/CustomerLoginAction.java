package com.isa.thinair.ibe.core.web.v2.action.customer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CustomerLoginAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(CustomerLoginAction.class);

	private boolean success = true;

	private String messageTxt = "";

	private String driveCode = "";

	private String emailId;

	private String password;
	
	private boolean isJoinLms = false;
	
	private LmsMemberDTO lmsDetails;

	private IBECommonDTO commonParams;

	private boolean loyaltyManagmentEnabled = false;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		commonParams = super.getCommonParams();
		
		boolean logged = false;
		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			Customer custModel = null;
						
			if (request.getAttribute(CustomerWebConstants.GRANT_SOCIAL_LOGIN) != null && (Boolean) request.getAttribute(CustomerWebConstants.GRANT_SOCIAL_LOGIN)) {
				custModel = customerDelegate.authenticate(SessionUtil.getCustomerEmail(request),
						SessionUtil.getCustomerPassword(request));
				driveCode = "allowSocialLogging";
				request.setAttribute(CustomerWebConstants.GRANT_SOCIAL_LOGIN, null);

			} else {
				custModel = customerDelegate.authenticate(emailId, password);
				if(custModel!= null && !custModel.getCustomerLmsDetails().isEmpty()){
					lmsDetails = CustomerUtilV2.getLmsMemberDTO(custModel.getCustomerLmsDetails().iterator().next());
					
				}
			}

			// Clear Session Data
			

			if(AppSysParamsUtil.isLMSEnabled()){
				loyaltyManagmentEnabled = true;
				SessionUtil.setCustomerId(request, -1);
				SessionUtil.setLoyaltyAccountNo(request, null);
				SessionUtil.setLoyaltyFFID(request, null);
			} else {
				loyaltyManagmentEnabled = false;
			}

			if (custModel != null) {
				SessionUtil.setCustomerId(request, custModel.getCustomerId());

				LoyaltyCustomerServiceBD loyaltyCustomerBD = ModuleServiceLocator.getLoyaltyCustomerBD();
				LoyaltyCustomerProfile loyaltyProfile = loyaltyCustomerBD.getLoyaltyCustomerByCustomerId(custModel
						.getCustomerId());
				if (loyaltyProfile != null)
					SessionUtil.setLoyaltyAccountNo(request, loyaltyProfile.getLoyaltyAccountNo());

				SessionUtil.setCustomerEMail(request, custModel.getEmailId());
				SessionUtil.setCustomerPassword(request, custModel.getPassword());

				if (custModel.getLMSMemberDetails() != null) {
					SessionUtil.setLoyaltyFFID(request, custModel.getLMSMemberDetails().getFfid());
				}

				SystemUtil.setCommonParameters(request, commonParams);
				logged = true;
			} else {
				logged = false;
				messageTxt = I18NUtil.getMessage(CustomerWebConstants.LOGIN_FAILED, getCommonParams().getLocale());
			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("CustomerLoginAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, getCommonParams().getLocale());
			log.error("CustomerLoginAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public IBECommonDTO getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

	public String getDriveCode() {
		return driveCode;
	}
	

	public boolean isJoinLms() {
		return isJoinLms;
	}

	public void setJoinLms(boolean isJoinLms) {
		this.isJoinLms = isJoinLms;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

}
