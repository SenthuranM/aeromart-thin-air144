package com.isa.thinair.ibe.core.web.v2.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.ibe.api.dto.PaxTO;
import com.isa.thinair.webplatform.api.v2.ancillary.ItineraryAnciAvailability;

public class ItineraryUtil {

	public static String composeName(String title, String firstName, String lastName) {
		title = BeanUtils.nullHandler(title);
		firstName = StringUtil.toInitCap(BeanUtils.nullHandler(firstName));
		lastName = StringUtil.toInitCap(BeanUtils.nullHandler(lastName));

		StringBuilder name = new StringBuilder();

		if (title.length() > 0) {
			name.append(title).append(" ");
		}

		if (firstName.length() > 0) {
			name.append(firstName).append(" ");
		}

		if (lastName.length() > 0) {
			name.append(lastName).append(" ");
		}

		return name.toString().trim();
	}

	public static Object[] createPassengerList(LCCClientReservation lccClientReservation) {
		Object[] paxCol = new Object[3];
		Collection<PaxTO> colAdultCollection = new ArrayList<PaxTO>();
		Collection<PaxTO> colChildCollection = new ArrayList<PaxTO>();
		Collection<PaxTO> colinfantCollection = new ArrayList<PaxTO>();
		boolean hasUNSegment = hasUncancelledUnSegment(lccClientReservation.getSegments());
		boolean afterCutoffTime = modifyNameChangeBufferExceededOrFlown(lccClientReservation.getSegments());
		int i = 1;
		for (LCCClientReservationPax lccClientReservationPax : com.isa.thinair.airproxy.api.utils.SortUtil
				.sortPax(lccClientReservation.getPassengers())) {

			PaxTO paxTO = new PaxTO();
			paxTO.setItnSeqNo(String.valueOf(i));
			paxTO.setItnPaxName(composeName(lccClientReservationPax.getTitle(), lccClientReservationPax.getFirstName(),
					lccClientReservationPax.getLastName()));
			
			paxTO.setItnPaxTitle(lccClientReservationPax.getTitle());
			paxTO.setItnPaxFirstName(lccClientReservationPax.getFirstName());
			paxTO.setItnPaxLastName(lccClientReservationPax.getLastName());

			// If a UN segment is there or the first segment namechange buffer time is passed pax changes not allowed
			if (hasUNSegment || afterCutoffTime) {
				paxTO.setEditableFlag(false);
			}

			if (lccClientReservationPax.getParent() != null) {
				paxTO.setItnTravellingWith(composeName(lccClientReservationPax.getParent().getTitle(), lccClientReservationPax
						.getParent().getFirstName(), lccClientReservationPax.getParent().getLastName()));
			}
			paxTO.setItnMeal("");
			paxTO.setItnSeat("");
			paxTO.setItnBaggage("");
			paxTO.setActualSeqNo(lccClientReservationPax.getPaxSequence() + "");
			paxTO.setSelectedAncillaries(lccClientReservationPax.getSelectedAncillaries());
			paxTO.setTravelerRefNumber(lccClientReservationPax.getTravelerRefNumber());
			paxTO.setDateOfBirth(DateUtil.dateToStringFormat(lccClientReservationPax.getDateOfBirth()));
			i++;
			if (ReservationInternalConstants.PassengerType.ADULT.equals(lccClientReservationPax.getPaxType())) {
				paxTO.setItnFFID(StringUtil.nullConvertToString(lccClientReservationPax.getLccClientAdditionPax().getFfid()));
				colAdultCollection.add(paxTO);
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(lccClientReservationPax.getPaxType())) {
				paxTO.setItnFFID(StringUtil.nullConvertToString(lccClientReservationPax.getLccClientAdditionPax().getFfid()));
				colChildCollection.add(paxTO);
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(lccClientReservationPax.getPaxType())) {
				colinfantCollection.add(paxTO);
			}
		}
		paxCol[0] = colAdultCollection;
		paxCol[1] = colChildCollection;
		paxCol[2] = colinfantCollection;

		return paxCol;
	}

	public static boolean hasUncancelledUnSegment(Set<LCCClientReservationSegment> segments) {
		boolean hasUnSegment = false;
		for (LCCClientReservationSegment segment : segments) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())
					&& segment.isUnSegment())
				hasUnSegment = true;

		}
		return hasUnSegment;

	}

	public static boolean modifyNameChangeBufferExceededOrFlown(Set<LCCClientReservationSegment> segments) {
		boolean afterCutoffTimeORFlown = false;
		long nameChangeCutoffTime = AppSysParamsUtil.getPassenegerNameModificationCutoffTimeInMillis();
		List<LCCClientReservationSegment> segmentList = new ArrayList<>(segments);
		Collections.sort(segmentList);
		for (LCCClientReservationSegment segment : segmentList) {
			if (!ReservationSegmentSubStatus.EXCHANGED.equals(segment.getSubStatus())) {
				long timeDiff = segment.getDepartureDateZulu().getTime() - CalendarUtil.getCurrentZuluDateTime().getTime();
				afterCutoffTimeORFlown = (timeDiff < nameChangeCutoffTime) | segment.isFlownSegment();
				break;
			}

		}
		return afterCutoffTimeORFlown;

	}

	/*
	 * private static List<LCCSelectedSegmentAncillaryDTO> addIBEMealQuantityView(List<LCCSelectedSegmentAncillaryDTO>
	 * selectedAncillaries) { if (selectedAncillaries != null) { Map<String, LCCMealDTO> mealQuantityMap = null;
	 * LCCMealDTO tempMeal = null;
	 * 
	 * for (LCCSelectedSegmentAncillaryDTO ancillaryDTO : selectedAncillaries) { if (ancillaryDTO != null &&
	 * ancillaryDTO.getMealDTOs() != null) { mealQuantityMap = new HashMap<String, LCCMealDTO>(); for (LCCMealDTO meal :
	 * ancillaryDTO.getMealDTOs()) { if (mealQuantityMap.containsKey(meal.getMealCode())) { tempMeal =
	 * mealQuantityMap.get(meal.getMealCode()); tempMeal.setMealQuntity(tempMeal.getMealQuntity() + 1);
	 * tempMeal.setTotalPrice(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply( meal.getMealCharge() ,
	 * tempMeal.getMealQuntity()))); mealQuantityMap.put(meal.getMealCode(), tempMeal); } else { meal.setMealQuntity(1);
	 * meal.setTotalPrice(AccelAeroCalculator.formatAsDecimal(meal.getMealCharge()));
	 * mealQuantityMap.put(meal.getMealCode(), meal); } } ancillaryDTO.setMealDTOs(new
	 * ArrayList<LCCMealDTO>(mealQuantityMap.values())); } } }
	 * 
	 * return selectedAncillaries; }
	 */

	public static ItineraryAnciAvailability getAncillaryAvailability(Object[] paxCollectionArr) {
		ItineraryAnciAvailability availability = new ItineraryAnciAvailability();

		Collection<PaxTO> paxTOCollection = new ArrayList<PaxTO>();
		paxTOCollection.addAll((Collection<PaxTO>) paxCollectionArr[0]);
		paxTOCollection.addAll((Collection<PaxTO>) paxCollectionArr[1]);
		paxTOCollection.addAll((Collection<PaxTO>) paxCollectionArr[1]);

		outermost: for (PaxTO paxTO : paxTOCollection) {
			for (LCCSelectedSegmentAncillaryDTO anci : paxTO.getSelectedAncillaries()) {
				LCCAirSeatDTO seat = anci.getAirSeatDTO();
				if (seat != null && seat.getSeatNumber() != null && !seat.getSeatNumber().equals("")) {
					availability.setSeatAvailable(true);
				}
				List<LCCMealDTO> meals = anci.getMealDTOs();
				if (meals != null && meals.size() > 0) {
					availability.setMealAvailable(true);
				}
				List<LCCSpecialServiceRequestDTO> ssrs = anci.getSpecialServiceRequestDTOs();
				if (ssrs != null && ssrs.size() > 0) {
					availability.setSsrAvailable(true);
				}
				
				List<LCCAirportServiceDTO> apts = anci.getAirportTransferDTOs();
				if (apts != null && apts.size() > 0) {
					availability.setAirportTransferAvailable(true);
				}
				
				if (availability.isAllAvailable()) {
					break outermost;
				}
			}
		}

		return availability;
	}

}
