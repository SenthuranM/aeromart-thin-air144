package com.isa.thinair.ibe.core.web.v2.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * TODO: Merge this with the same class in XBE. Put int a common place Helper Class to Store Airport Drop Down List
 * Creation Common Data
 * 
 * @author Navod Ediriweera
 * @since 04 Oct 2010
 */
public class AirportDDListGenerator {

	public static List createIBEAirportDDList(List<String[]> filterList) throws ModuleException {
		boolean isSubStationsAllowed = AppSysParamsUtil.isGroundServiceEnabled();
		if (AppSysParamsUtil.isLCCConnectivityEnabled() && AppSysParamsUtil.showLCCResultsInIBE()) {
			return (List) sortAirpots(SelectListGenerator.getIBEAirportsListWSub(true, isSubStationsAllowed, filterList));
		} else {
			return SelectListGenerator.getIBEAirportsListWSub(false, isSubStationsAllowed, filterList);
		}
	}

	/**
	 * TODO Move to common place. {@link SortUtil}
	 * 
	 * @param airports
	 * @return
	 */
	private static Collection<String[]> sortAirpots(List<String[]> airports) {
		Collections.sort(airports, new Comparator<String[]>() {
			public int compare(String[] obj1, String[] obj2) {
				return (obj1[0]).compareTo(obj2[0]);
			}
		});
		return airports;
	}
}
