package com.isa.thinair.ibe.core.web.v2.action.system;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airreservation.api.dto.IbeExitUserDetailsDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ExitPopupUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;

/**
 * This action is handling functions relates to IBE browser exit popup
 * @author Eshan
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ExitPopupAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ExitPopupAction.class);
	
	private FlightSearchDTO searchParams;

	private String exitStep;

	private boolean isExitPopupDisplayed;
	
	private ContactInfoDTO contactInfo;
	
	public String sendSearchDetailsEmail() {
		log.debug("sending flight search details email to :" + contactInfo.getEmailAddress());
		
		ExitPopupUtil exitPopupUtill = new ExitPopupUtil()
				.withFlighSearchInfo(searchParams)
				.withIbeExitStep(exitStep)
				.withCustomer(getCustomer())
				.withContactInfoDTO(contactInfo);
				
		IbeExitUserDetailsDTO savedExitDetails = exitPopupUtill.saveIbeExitDetails();
		exitPopupUtill.sendFlightSearchDetailsEmail(request,savedExitDetails);
		
		return StrutsConstants.Result.SUCCESS;
	}
	
	public String updateExitPopupDisplayStatus(){
		SessionUtil.updateExitPopdisplayStatus(request,true);
		return StrutsConstants.Result.SUCCESS;
	}
	
	public String lookupExitPopupDisplayStatus(){
		this.setExitPopupDisplayed(SessionUtil.isExitpopupDisplayed(request));
		return StrutsConstants.Result.SUCCESS;
	}
	
	private CustomerDTO getCustomer() {
		try {
			Customer customerModel = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			if(customerModel != null){
				return CustomerUtilV2.getCustomerDTO(customerModel);
			}
		} catch (ModuleException e) {
			log.error("Error while obtaing logged in user");
		}
		return null;
	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public String getExitStep() {
		return exitStep;
	}

	public void setExitStep(String exitStep) {
		this.exitStep = exitStep;
	}

	public boolean isExitPopupDisplayed() {
		return isExitPopupDisplayed;
	}

	public void setExitPopupDisplayed(boolean isExitPopupDisplayed) {
		this.isExitPopupDisplayed = isExitPopupDisplayed;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

}