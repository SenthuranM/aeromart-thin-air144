package com.isa.thinair.ibe.core.web.v2.action.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.BookingType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.VoucherPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.ibe.api.dto.BalanceTo;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.api.dto.PutOnHoldBeforePaymentDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.RequestAttribute;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.SessionAttribute;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.BalanceSummaryUtil;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard.FieldName;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.PAYPALResponse;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.CreditCardUtil;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.paypal.PAYPALPaymentUtils;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.opensymphony.xwork2.ActionChainResult;
import com.paypal.soap.api.CountryCodeType;
import com.paypal.soap.api.CurrencyCodeType;
import com.paypal.soap.api.ErrorType;
import com.paypal.soap.api.PaymentActionCodeType;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.WIRECARD_SUCCESS, value = StrutsConstants.Jsp.Payment.WIRECARD_INTERLINE_CONTAINER),
		@Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Payment.POST_INPUT_DATA),
		@Result(name = StrutsConstants.Result.BYPASS, type = ActionChainResult.class, value = StrutsConstants.Action.HANDLE_INTERLINE_IPG_RESPONSE),
		@Result(name = StrutsConstants.Action.SAVE_INTERLINE_RESERVATION, type = ActionChainResult.class, value = StrutsConstants.Action.SAVE_INTERLINE_RESERVATION),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.LANDING_3DSECURE, value = StrutsConstants.Jsp.Payment.LANDING3DSECUREPAGE),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Result.CARD_PAYMENT_ERROR, value = StrutsConstants.Jsp.Payment.CARD_PAYMENT_ERROR) })
public class InterlinePostCardDetailsAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(InterlinePostCardDetailsAction.class);
	private static final String RETURN_URL = AppParamUtil.getSecureIBEUrl() + "handleInterlineIPGResponse.action";
	private static final String STATUS_URL = AppParamUtil.getSecureIBEUrl()
			+ "handleInterlineIPGResponse!executeStatusRequest.action";
	private static final String RECEIPT_URL = AppParamUtil.getSecureIBEUrl() + "receiptIPGResponse.action";

	private static final String PAY_FORT_STORE = "PAYFORT_PAY_AT_STORE";

	private PaymentGateWayInfoDTO pgw = new PaymentGateWayInfoDTO();

	private ContactInfoDTO contactInfo = new ContactInfoDTO();

	private String jsonPaxWiseAnci = null;

	private String jsonPaxInfo = null;

	private String jsonInsurance = null;

	private String nameChangePaxData;

	private boolean nameChangeRequote;

	private boolean modifyAncillary = false;

	private String pnr = null;

	private String version = null;

	private String oldAllSegments;

	private boolean modifySegment;

	private String modifySegmentRefNos;

	private String modifingFlightInfo;

	private String paymentType;

	private boolean groupPNR;

	private int oldFareID;

	private int oldFareType;

	private String oldFareAmount;

	private boolean hasInsurance;

	private String requestSessionIdentifier;

	private CreditCardTO card;

	private int insType;

	private boolean addGroundSegment;

	private final UserInputDTO userInputDTO = new UserInputDTO();

	private boolean makePayment;

	private boolean requoteFlightSearch;

	private String balanceQueryData;
	
	private boolean createdOnhold;

	public String execute() throws ParseException {

		if (log.isDebugEnabled()) {
			log.debug("Interline paymentgateway retrieve started");
		}

		if (SystemPropertyUtil.isPaymentBokerDisabled()) {
			return StrutsConstants.Result.BYPASS;
		}

		IBEReservationInfoDTO resInfo;
		IBEReservationPostPaymentDTO resPostPay;
		boolean isIntExtPaymentGateway = false;
		BigDecimal loyaltyCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal lmsPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal voucherPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalExcludingExtChgs = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCreditCardPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		synchronized (request) {
			resInfo = SessionUtil.getIBEreservationInfo(request);
			if (!modifyAncillary && AppSysParamsUtil.getImageCapchaDisplayAvailabilitySearch() && !resInfo.isCaptchaValidated()) {
				releaseBlockedSeats(request);
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, "Unauthorized User. Access Denied!");// getServerErrorMessage(request,"")
				SessionUtil.resetSesionDataInError(request); // );
				if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
					return StrutsConstants.Result.CARD_PAYMENT_ERROR;
				} else {
					return StrutsConstants.Result.ERROR;
				}
			}
			if (resInfo == null || (!resInfo.isFromPaymentPage() && !resInfo.isNoPay())) {
				releaseBlockedSeats(request);
				SessionUtil.expireSession(request);
				return StrutsConstants.Result.SESSION_EXPIRED;
			} else {
				resInfo.setFromPaymentPage(false);
			}

			resInfo.setFromPostCardDetails(true);
		}

		if ((paymentType != null && BookingType.getEnum(paymentType) == BookingType.ONHOLD) && resInfo.getDiscountInfo() != null
				&& resInfo.getDiscountInfo().getPromotionId() != null) {
			resInfo.removeFareDiscount();
		}

		GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		CommonMasterBD commonMasterBD = ModuleServiceLocator.getCommoMasterBD();
		String carrierCode = SessionUtil.getCarrier(request);

		try {
			resPostPay = new IBEReservationPostPaymentDTO();
			SessionUtil.setIBEReservationPostPaymentDTO(request, resPostPay);

			lmsPaymentAmount = CustomerUtil.getLMSPaymentAmount(resInfo);

			voucherPaymentAmount = CustomerUtil.getVoucherPaymentAmount(resInfo);

			loyaltyCredit = resInfo.getLoyaltyCredit();
			if (loyaltyCredit == null) {
				loyaltyCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
			}

			// Set Session Attribute for track user itinerary confirmation page refresh
			SessionUtil.setIbeReservationPostDTO(request, null);
			request.getSession().setAttribute("fromItinearyPage", false);

			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());
			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			checkReservationDataIntergrity(resInfo.getPriceInfoTO(), flightSegmentTOs); /* Validate Reservation Data */
			boolean isReturn = fltSegBuilder.isReturn();
			this.setSubStations(getSearchParams(), flightSegmentTOs);

			LCCInsuredJourneyDTO insJrnyDto = null;
			SelectedFltSegBuilder totalFltSegBuilder = null;
			List<LCCInsuranceQuotationDTO> insuranceQuotations = null;
			boolean isCsOcFlightAvailable = false;
			// When adding bus segment, insurance will be quoted for whole journey

			if (!hasInsurance && !makePayment) {
				if (addGroundSegment) {
					// Insurance is added only if its not there already
					totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);
					totalFltSegBuilder.getSelectedFlightSegments().addAll(fltSegBuilder.getSelectedFlightSegments());
					insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(totalFltSegBuilder.getSelectedFlightSegments(),
							isReturn, getTrackInfo().getOriginChannelId());
				} else {
					insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(flightSegmentTOs, isReturn, getTrackInfo()
							.getOriginChannelId());

				}
				insuranceQuotations = AncillaryJSONUtil.getInsuranceQuotation(jsonInsurance, insJrnyDto);
				Iterator<LCCInsuranceQuotationDTO> it = insuranceQuotations.iterator();
				ExternalChgDTO insChgDTO = null;
				BigDecimal selectedInsuranceTotalPremiumAmount = BigDecimal.ZERO;
				List<String> selectedInsuranceRefNumbers = new ArrayList<String>();
				while (it.hasNext()) {
					LCCInsuranceQuotationDTO insuranceQuotation = it.next();
					if (insuranceQuotation != null) {
						if (insChgDTO == null) {
							insChgDTO = new ExternalChgDTO();
							insChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.INSURANCE);
						}
						selectedInsuranceTotalPremiumAmount = selectedInsuranceTotalPremiumAmount.add(insuranceQuotation
								.getQuotedTotalPremiumAmount());
						selectedInsuranceRefNumbers.add(insuranceQuotation.getInsuranceRefNumber());
						insuranceQuotation.setInsuranceType(insType);

						boolean isAllDomastic = true;
						for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
							if (!flightSegmentTO.isDomesticFlight()) {
								isAllDomastic = false;
								break;
							}
						}
						insuranceQuotation.setAllDomastic(isAllDomastic);

						// temp Validation purpose
						if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.CCC)) {
							if ("1".equals(insType)) {
								throw new ModuleException("airreservations.insurance.invalid");
							}
						}
					}
				}
				if (insChgDTO != null) {
					insChgDTO.setAmount(selectedInsuranceTotalPremiumAmount);
					insChgDTO.setChargeCode(selectedInsuranceRefNumbers.toString());
					resInfo.addExternalCharge(insChgDTO);
				}
			}

			if (SessionUtil.getIbePromotionPaymentPostDTO(request) != null) {
				ExternalChgDTO promoChgDTO = new ExternalChgDTO();
				String nextSeatChargeStr = SessionUtil.getIbePromotionPaymentPostDTO(request).getTotalPayment();
				BigDecimal nextSeatCharge = new BigDecimal(nextSeatChargeStr);
				promoChgDTO.setAmount(nextSeatCharge);
				promoChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE);
				resInfo.addExternalCharge(promoChgDTO);

			}

			List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPax(jsonPaxWiseAnci, insuranceQuotations,
					ApplicationEngine.IBE, (addGroundSegment && !hasInsurance) ? totalFltSegBuilder.getSelectedFlightSegments()
							: flightSegmentTOs, null, resInfo.getAnciOfferTemplates());
			validatePaxName(paxList);
			ExternalChargeUtil.injectOndBaggageGroupId(paxList, flightSegmentTOs);

			if (resInfo.getPriceInfoTO() != null) {
				ReservationUtil.setOndSequence(resInfo.getPriceInfoTO(), flightSegmentTOs);
			}

			if (getSearchParams().isFlexiSelected()) {
				ReservationUtil.applyFlexiCharges(paxList, getSearchParams().getOndSelectedFlexi(), resInfo.getPriceInfoTO(),
						flightSegmentTOs);
			}

			Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModSegAnciTotal = null;
			if (resInfo.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI) && modifySegment && jsonPaxInfo != null
					&& !jsonPaxInfo.equals("")
					&& !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(resInfo.getReservationStatus())) {
				Collection<LCCClientReservationPax> colPax = ReservationUtil.transformJsonPassengers(jsonPaxInfo);
				List<String> modSegList = ReservationUtil.getModifyingFlightRefNumberList(modifingFlightInfo);
				paxWiseModSegAnciTotal = ExternalChargeUtil.getPaxWiseModifyingSegmentAnciTotal(flightSegmentTOs, modSegList,
						colPax);
			}

			Collections.sort(flightSegmentTOs);
			ReservationUtil.applyAncillaryTax(paxList, flightSegmentTOs.get(0), resInfo, paxWiseModSegAnciTotal);
			if (requoteFlightSearch) {
				ReservationUtil.updateEffectiveTax(paxList, resInfo.getlCCClientReservationBalance().getPaxEffectiveTax(), resInfo.isInfantPaymentSeparated());
			}

			if (modifyAncillary && resInfo != null && resInfo.isApplyPenaltyForAnciModification()) {
				// Add credit amount created by anci modification as a penalty amount

				// most recent unflown segment
				Date now = CalendarUtil.getCurrentSystemTimeInZulu();
				FlightSegmentTO segmentToPass = null;
				for (FlightSegmentTO seg : flightSegmentTOs) {
					if (now.before(seg.getDepartureDateTimeZulu())) {
						segmentToPass = seg;
						break;
					}
				}

				if (segmentToPass == null) {
					segmentToPass = flightSegmentTOs.get(0);
				}

				ReservationUtil.applyAncillaryModifyPenalty(paxList, segmentToPass);
			}

			FlightPriceRQ flightPriceRQ = null;
			boolean isFlexiQuote = getSearchParams().isFlexiSelected();
			if (!modifyAncillary && !makePayment) {
				// FIXME avoid creating AvailableFlightSearchDTOBuilder and directly create the flightPriceRQ
				// Current implementation leads unnecessary DTO transformation step
				AvailableFlightSearchDTOBuilder avilBuilder = SearchUtil.getSearchBuilder(getSearchParams());
				if (modifySegment && !groupPNR) {
					SearchUtil.setModifingSegmentSearchData(avilBuilder, oldFareID, oldFareType, oldFareAmount);
				}
				if (pnr != null && !"".equals(pnr)) {
					SearchUtil.setExistingSegInfo(avilBuilder.getSearchDTO(), oldAllSegments, modifySegmentRefNos);
				}
				flightPriceRQ = BeanUtil.createFareQuoteRQ(avilBuilder.getSearchDTO(), flightSegmentTOs, getSearchParams()
						.getClassOfService(), getSearchParams().getLogicalCabinClass(), getSearchParams()
						.getFareQuoteLogicalCCSelection());

			}
			if ((contactInfo.getFirstName() == null || contactInfo.getFirstName().isEmpty())
					&& SessionUtil.getReservationContactInfo(request) != null) {
				contactInfo = SessionUtil.getReservationContactInfo(request);
			}

			String selectedCurrency = getSearchParams().getSelectedCurrency();
			resPostPay.setPaxList(paxList);
			resPostPay.setFlightPriceRQ(flightPriceRQ);
			resPostPay.setSelectedFlightSegments(flightSegmentTOs);
			SessionUtil.getPutOnHoldBeforePaymentDTO(request).setDepartureDate(flightSegmentTOs.get(0).getDepartureDateTime());

			resPostPay.setInsuranceQuotes(insuranceQuotations);
			resPostPay.setContactInfo(contactInfo);
			if (contactInfo.getLastName() != null && !contactInfo.getLastName().equals("")) {
				SessionUtil.getPutOnHoldBeforePaymentDTO(request).setLastName(contactInfo.getLastName());
			}
			resPostPay.setAddModifyAncillary(modifyAncillary);
			if (pnr != null && !pnr.trim().isEmpty()) {
				resPostPay.setPnr(pnr);
			} else {
				resPostPay.setPnr(null);
			}
			resPostPay.setSelectedCurrency(selectedCurrency);
			resPostPay.setFlexiQuote(isFlexiQuote);

			resPostPay.setBookingType(resInfo.getBookingType());
			resPostPay.setPriceInfoTO(resInfo.getPriceInfoTO());
			resPostPay.setReservationCredit(resInfo.getReservationCredit());
			resPostPay.setNoPay(resInfo.isNoPay());
			resPostPay.setCreditCardFee(resInfo.getCreditCardFee());
			resPostPay.setOutboundExCharge(resInfo.getOutboundExCharge());
			resPostPay.setInboundExCharge(resInfo.getInboundExCharge());

			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null)) {
				resPostPay.setPaymentAdminFee(true);

				Set<EXTERNAL_CHARGES> chargeKeySet = resInfo.getSelectedExternalCharges().keySet();
				for (EXTERNAL_CHARGES selectedExtChg : chargeKeySet) {
					resPostPay.addExternalCharge(resInfo.getSelectedExternalCharges().get(selectedExtChg));
				}
			} else {
				resPostPay.setPaymentAdminFee(false);
			}

			resPostPay.setlCCClientReservationBalance(resInfo.getlCCClientReservationBalance());
			resPostPay.setPaxCreditMap(resInfo.getPaxCreditMap(pnr));
			resPostPay.setLoyaltyPayOption(resInfo.getLoyaltyPayOption());
			resPostPay.setBalanceToPay(resInfo.hasBalanceToPay());
			resPostPay.setLoyaltyCredit(loyaltyCredit);
			resPostPay.setTransactionId(resInfo.getTransactionId());
			resPostPay.setCarrierWiseLoyaltyPaymentInfo(resInfo.getCarrierWiseLoyaltyPaymentInfo());
			resPostPay.setPayByVoucherInfo(resInfo.getPayByVoucherInfo());			

			// Name change requote data
			if (nameChangeRequote) {
				resPostPay.setNameChange(nameChangeRequote);
				resPostPay.setChangedPaxNames(ReservationUtil.transformNameChangePax(nameChangePaxData));
			}

			if (version != null && !"".equals(version)) {
				resPostPay.setVersion(version);
			}

			// Modify Segment
			if (oldAllSegments != null && !oldAllSegments.isEmpty() && (modifySegment || addGroundSegment || modifyAncillary)) {
				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
				String[] arrModifingAllSegs = modifySegmentRefNos.split(":");
				if (modifySegment) {
					Collection<LCCClientReservationSegment> colModifiedSegs = new ArrayList<LCCClientReservationSegment>();
					for (LCCClientReservationSegment resSeg : colsegs) {
						for (String arrModifingAllSeg : arrModifingAllSegs) {
							if (resSeg.getBookingFlightSegmentRefNumber().equals(arrModifingAllSeg)) {
								colModifiedSegs.add(resSeg);
							}
						}
					}
					resPostPay.setExistingAllLccSegments(colsegs);
					resPostPay.setModifiedLccSegments(colModifiedSegs);

					// Modify Flow with requote
					if (requoteFlightSearch) {
						flightPriceRQ = BeanUtil.createFareReQuoteRQ(getSearchParams(), false);
						resPostPay.setRequoteFlightSearch(true);
						resPostPay.setBalanceQueryData(balanceQueryData);
						resPostPay.setOndList(getSearchParams().getOndList());
						resPostPay.setFlightPriceRQ(flightPriceRQ);
					}
					resPostPay.setModifySegment(true);

				} else if (addGroundSegment) {
					// We assume modifying seg number carries add seg id of the bus connector
					resPostPay.setSelectedPnrSegment(arrModifingAllSegs[0]);
					resPostPay.setModifiedLccSegments(colsegs);
					resPostPay.setExistingAllLccSegments(colsegs);
					resPostPay.setAddGroundSegment(true);
				} else if (modifyAncillary) {
					resPostPay.setExistingAllLccSegments(colsegs);
				}

			}

			if (resInfo.getTotalPriceWithoutExtChg() != null) {
				totalExcludingExtChgs = new BigDecimal(resInfo.getTotalPriceWithoutExtChg());
			}

			if (modifySegment) {
				if (resInfo.getlCCClientReservationBalance().getVersion() != null) {
					resPostPay.setVersion(resInfo.getlCCClientReservationBalance().getVersion());
				}
				BalanceTo balanceTo = BalanceSummaryUtil.getTotalBalanceToPayAmount(resInfo.getlCCClientReservationBalance(),
						null, resInfo.isInfantPaymentSeparated());
				// totalExcludingExtChgs = AccelAeroCalculator.subtract(balanceTo.getBalanceToPay(),
				// BalanceSummaryUtil.getUtilizeCreditBalance(resInfo.getlCCClientReservationBalance(), paxList));

				totalExcludingExtChgs = balanceTo.getBalanceToPay();

				if (resPostPay.isRequoteFlightSearch()) {
					// deduct external charge amount from amount due
					List<EXTERNAL_CHARGES> skipCharges = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
					skipCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
					skipCharges.add(EXTERNAL_CHARGES.JN_OTHER);
					for (ReservationPaxTO reservationPaxTO : paxList) {
						if (!PaxTypeTO.INFANT.equals(reservationPaxTO.getPaxType())) {
							totalExcludingExtChgs = AccelAeroCalculator.subtract(totalExcludingExtChgs,
									BookingUtil.getTotalExtCharge(reservationPaxTO.getExternalCharges(), skipCharges));
						}
					}
				}
			}

			if (log.isDebugEnabled()) {
				log.debug("############################## NO PAY : " + resInfo.isNoPay());
			}
			if (resInfo.isNoPay() && SessionUtil.getIbePromotionPaymentPostDTO(request) == null) {
				return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
			}
			// OnHold Booking
			if ((paymentType != null && BookingType.getEnum(paymentType) == BookingType.ONHOLD)) {
				resPostPay.setBookingType(BookingType.ONHOLD);
				return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
			} else if ((paymentType != null && BookingType.getEnum(paymentType) == BookingType.NORMAL)) {
				resPostPay.setBookingType(BookingType.NORMAL);
			} else {
				resPostPay.setBookingType(BookingType.NONE);
			}

			if (modifyAncillary || makePayment) {
				if (resInfo.getReservationCredit() != null
						&& resInfo.getReservationCredit().negate().compareTo(BigDecimal.ZERO) > 0) {
					totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, resInfo.getReservationCredit());
				}
				Map<String, BigDecimal> paxCreditMap = resInfo.getPaxCreditMap(pnr);
				BigDecimal reservationBalance = (BigDecimal) request.getSession().getAttribute(
						SessionAttribute.RESERVATION_BALANCE);
				BigDecimal removeAnciTotal = BigDecimal.ZERO;
				if (reservationBalance == null) {
					reservationBalance = BigDecimal.ZERO;
				}
				for (ReservationPaxTO pax : paxList) {
					BigDecimal paxCredit = paxCreditMap.get(pax.getTravelerRefNumber());
					if (paxCredit != null) {
						if (paxCredit.compareTo(BigDecimal.ZERO) > 0) {
							reservationBalance = AccelAeroCalculator.add(reservationBalance, paxCredit);
						}
					}
					removeAnciTotal = AccelAeroCalculator.add(removeAnciTotal, pax.getToRemoveAncillaryTotal());
				}
				totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, reservationBalance,
						removeAnciTotal.negate());
			}

			// (next seat) promotions should not put on hold when paying
			if (makePayment && SessionUtil.getIbePromotionPaymentPostDTO(request) == null) { // We have to set onHold
																								// data before getting
																								// sent to confirmation
																								// Page due to total
				// loyalty payment
				resPostPay.setMakePayment(true);
				setPutOnHoldBeforePaymentSessionData(pnr);
				ReservationUtil.addTicketPriceOnHoldBeforePaymentSessionData(request, resInfo.getTotalTicketPriceInBase());

			}

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			boolean checkBalanceToPay = false;

			if (AppSysParamsUtil.isLoyalityEnabled()) {

				LoyaltyPaymentOption loyaltyPayOpt = resInfo.getLoyaltyPayOption();
				String loyaltyAcc = SessionUtil.getLoyaltyAccountNo(request);
				if (log.isDebugEnabled()) {
					log.debug("############################## LOYALT OPTION : " + resInfo.getLoyaltyPayOption() + " ["
							+ loyaltyCredit + "]");
				}
				// Users Existing in system with out Loyalty Account
				/*
				 * if(loyaltyAcc==null && customerId >= 0){ throw new ModuleException("Invalid user data"); }
				 */
				if (loyaltyAcc != null) {
					BigDecimal totCredit = ModuleServiceLocator.getLoyaltyCreditBD().getTotalNonExpireCredit(loyaltyAcc);

					if (loyaltyPayOpt == LoyaltyPaymentOption.PART) {
						checkBalanceToPay = true;
						if (loyaltyCredit.compareTo(totCredit) > 0) {
							throw new ModuleException("errors.loyalty.invalid.credit");
						}
					}
					if (loyaltyPayOpt == LoyaltyPaymentOption.TOTAL) {
						if (SessionUtil.getIbePromotionPaymentPostDTO(request) == null) {
							resInfo.removeExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD); // Just in case something went
																						// wrong in payment action
							resInfo.removeExternalCharge(EXTERNAL_CHARGES.JN_OTHER);
						}

						BigDecimal reservationBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

						boolean creditDiscount = (resInfo.getDiscountInfo() != null) ? DiscountApplyAsTypes.CREDIT.equals(resInfo
								.getDiscountInfo().getDiscountAs()) : true;

						boolean promoExists = (resInfo.getDiscountInfo() != null)
								&& (resInfo.getDiscountInfo().getPromotionId() != null);

						FareQuoteTO fareQuote = ReservationUtil.fillFareQuote(paxList, null, totalExcludingExtChgs,
								resInfo.getCreditCardFee(), resInfo.getTotalFare(), resInfo.getTotalTax(),
								resInfo.getTotalTaxSurchages(), loyaltyPayOpt, loyaltyCredit, false, null, null, false,
								reservationBalance, false, exchangeRateProxy, promoExists, resInfo.getDiscountAmount(false),
								creditDiscount, resInfo.getIbeReturnFareDiscountAmount(), resPostPay.isRequoteFlightSearch(),
								CustomerUtil.getLMSPaymentAmount(resInfo), voucherPaymentAmount, resInfo.isInfantPaymentSeparated());
						BigDecimal totalPrice = new BigDecimal(fareQuote.getTotalPrice());
						if (totalPrice.compareTo(totCredit) > 0) {
							throw new ModuleException("errors.loyalty.invalid.credit");
						}
						resInfo.setLoyaltyCredit(totalPrice);
						resPostPay.setLoyaltyCredit(totalPrice);
						return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
					}
				}
			}

			if (AppSysParamsUtil.isLMSEnabled() || checkBalanceToPay) {
				LoyaltyPaymentOption loyaltyPayOpt = resInfo.getLoyaltyPayOption();

				BigDecimal reservationBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

				boolean creditDiscount = (resInfo.getDiscountInfo() != null) ? DiscountApplyAsTypes.CREDIT.equals(resInfo
						.getDiscountInfo().getDiscountAs()) : true;

				boolean promoExists = (resInfo.getDiscountInfo() != null) && (resInfo.getDiscountInfo().getPromotionId() != null);

				FareQuoteTO fareQuote = ReservationUtil.fillFareQuote(paxList, null, totalExcludingExtChgs,
						resInfo.getCreditCardFee(), resInfo.getTotalFare(), resInfo.getTotalTax(),
						resInfo.getTotalTaxSurchages(), loyaltyPayOpt, loyaltyCredit, false, null, null, false,
						reservationBalance, false, exchangeRateProxy, promoExists, resInfo.getDiscountAmount(false),
						creditDiscount, resInfo.getIbeReturnFareDiscountAmount(), resPostPay.isRequoteFlightSearch(),
						CustomerUtil.getLMSPaymentAmount(resInfo), voucherPaymentAmount, resInfo.isInfantPaymentSeparated());

				BigDecimal totalPayable = AccelAeroCalculator.getTwoScaledBigDecimalFromString(fareQuote.getTotalPayable());
				if (totalPayable.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) <= 0) {
					return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
				}
			}

			if (AppSysParamsUtil.isVoucherEnabled()) {

				if (resInfo.getPayByVoucherInfo() != null && resInfo.getPayByVoucherInfo().getVoucherDTOList() != null) {
					BigDecimal redeemdTotal = voucherPaymentAmount;

					if (resInfo.getPayByVoucherInfo().getVoucherPaymentOption() == VoucherPaymentOption.TOTAL) {
						BigDecimal reservationBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

						boolean creditDiscount = (resInfo.getDiscountInfo() != null) ? DiscountApplyAsTypes.CREDIT.equals(resInfo
								.getDiscountInfo().getDiscountAs()) : true;

						boolean promoExists = (resInfo.getDiscountInfo() != null)
								&& (resInfo.getDiscountInfo().getPromotionId() != null);


						FareQuoteTO fareQuote = ReservationUtil.fillFareQuote(paxList, null, totalExcludingExtChgs,
								resInfo.getCreditCardFee(), resInfo.getTotalFare(), resInfo.getTotalTax(),
								resInfo.getTotalTaxSurchages(), LoyaltyPaymentOption.NONE, loyaltyCredit, modifySegment,
								resInfo.getlCCClientReservationBalance(), AccelAeroCalculator.getDefaultBigDecimalZero(),
								modifyAncillary, reservationBalance, addGroundSegment, exchangeRateProxy, promoExists,
								resInfo.getDiscountAmount(false), creditDiscount, resInfo.getIbeReturnFareDiscountAmount(),
								requoteFlightSearch, CustomerUtil.getLMSPaymentAmount(resInfo), redeemdTotal, resInfo.isInfantPaymentSeparated());
						BigDecimal totalPrice = new BigDecimal(fareQuote.getTotalPayable());
						// if (totalPrice.compareTo(redeemdTotal) > 0) {
						// throw new ModuleException("errors.loyalty.invalid.credit");
						// }
						// resInfo.setLoyaltyCredit(totalPrice);
						// resPostPay.setLoyaltyCredit(totalPrice);
						return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
					}
				}
			}

			if (!modifyAncillary && !makePayment) {
				checkSeatAvailability(resInfo, flightPriceRQ, isFlexiQuote);
			}
			String baseCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
			String hdnSelCurrency = pgw.getPayCurrency();
			PayCurrencyDTO payCurrencyDTO = null;

			if (log.isDebugEnabled()) {
				log.debug("Interline paymentgateway checking for selected currency");
			}
			Currency currency = null;
			CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(hdnSelCurrency, ApplicationEngine.IBE);

			if (exchangeRate != null) {
				currency = commonMasterBD.getCurrency(hdnSelCurrency);
				if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultIbePGId() != null) {
					baseCurrencyCode = hdnSelCurrency;
					payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, exchangeRate.getMultiplyingExchangeRate(),
							currency.getBoundryValue(), currency.getBreakPoint());
				}
			}

			if (payCurrencyDTO == null) {
				if (log.isDebugEnabled()) {
					log.debug("Interline paymentgateway checking for selected currency does not have pg getting the default ");
				}
				currency = commonMasterBD.getCurrency(baseCurrencyCode);

				exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(baseCurrencyCode, ApplicationEngine.IBE);
				payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, new BigDecimal(1), currency.getBoundryValue(),
						currency.getBreakPoint());
			}

			pgw.setPayCurrency(baseCurrencyCode);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					new Integer(pgw.getPaymentGateway()), baseCurrencyCode);

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);

			ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
					resInfo.getSelectedExternalCharges(), true, true);

			BigDecimal totalCashDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
			ReservationDiscountDTO resDiscountDTO = ReservationUtil.calculateDiscountForReservation(resInfo, paxList,
					flightPriceRQ, getTrackInfo(), false, resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(),
					resInfo.getTransactionId(), true);

			if (resDiscountDTO != null) {
				totalCashDiscount = resDiscountDTO.getTotalDiscount();
				resPostPay.setPromoDiscountTotal(totalCashDiscount);
			}

			BigDecimal totalBaseCardPayAmount = AccelAeroCalculator.subtract(totalExcludingExtChgs, lmsPaymentAmount);
			totalBaseCardPayAmount = AccelAeroCalculator.subtract(totalBaseCardPayAmount, voucherPaymentAmount);
			totalBaseCardPayAmount = AccelAeroCalculator.subtract(totalBaseCardPayAmount, loyaltyCredit);
			totalBaseCardPayAmount = AccelAeroCalculator.subtract(totalBaseCardPayAmount, totalCashDiscount);

			Collection<ExternalChgDTO> externalCharges = new LinkedList<ExternalChgDTO>();
			externalCharges = externalChargesMediator.getAllExternalCharges();

			// Add per pax flexi charges
			// I think this can be done inside externalChargesMediator.getAllExternalCharges() instead having it here.
			for (ReservationPaxTO reservationPax : paxList) {
				Collection<ExternalChgDTO> paxExternalCharges = reservationPax.getExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);
				if (paxExternalCharges != null) {
					if (externalCharges == null) {
						externalCharges = new LinkedList<ExternalChgDTO>();
					}
					externalCharges.addAll(paxExternalCharges);
				}
			}
			String brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
			String requestMethod = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_REQUEST_METHOD);
			String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);
			boolean isSwitchToExternalURL = Boolean.parseBoolean(ipgProps
					.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SWITCH_TO_EXTERNAL_URL));
			boolean isViewPaymentInIframe = Boolean.parseBoolean(ipgProps
					.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_VIEW_PAYMENT_IFRAME));
			boolean isSupportOfflinePayment = Boolean.parseBoolean(ipgProps
					.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SUPPORT_OFFLINE_PAYMENT));

			String selCardType = getCardType();
			String txtCardNo = "";
			String txtName = "";
			String txtSCode = "";
			String strExpiry = "";

			if (brokerType != null
					&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)) {
				selCardType = card.getCardType();
				txtCardNo = card.getCardNo();
				txtName = card.getCardHoldersName();
				txtSCode = card.getCardCVV();
				strExpiry = card.getExpiryDateYYMMFormat();
				isIntExtPaymentGateway = true;

				if (StringUtil.isEmpty(selCardType) || StringUtil.isEmpty(txtCardNo) || StringUtil.isEmpty(txtName)
						|| StringUtil.isEmpty(txtSCode) || StringUtil.isEmpty(strExpiry) || strExpiry.length() != 4) {
					throw new ModuleException("paymentbroker.card.invalid");
				}
			}

			IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();

			ipgRequestDTO.setDefaultCarrierName(SystemUtil.getCarrierName(request));
			ipgRequestDTO.setCardType(selCardType);
			ipgRequestDTO.setCardNo(txtCardNo);
			ipgRequestDTO.setSecureCode(txtSCode);
			ipgRequestDTO.setHolderName(txtName);
			ipgRequestDTO.setExpiryDate(strExpiry);
			ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
			ipgRequestDTO.setPaymentMethod(card.getPaymentMethod());

			TrackInfoDTO trackInfoDTO = getTrackInfo();
			trackInfoDTO.setCarrierCode(SessionUtil.getCarrier(request));
			trackInfoDTO.setOriginChannelId(TrackInfoUtil.getWebOriginSalesChannel(request));
			
			String marketingUserId = "";
			
			if (trackInfoDTO.getOriginChannelId().equals(SalesChannelsUtil.SALES_CHANNEL_WEB)) {
				marketingUserId = "WEB-USER";
			} else if (trackInfoDTO.getOriginChannelId().equals(SalesChannelsUtil.SALES_CHANNEL_WEB)) {
				marketingUserId = "IOS-USER";
			} else if (trackInfoDTO.getOriginChannelId().equals(SalesChannelsUtil.SALES_CHANNEL_WEB)) {
				marketingUserId = "ANDROID-USER";
			} 

			LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();
			lccClientPaymentAssembler.addExternalCharges(externalCharges);

			if (log.isDebugEnabled()) {
				log.debug("Interline paymentgateway assembling external gateway");
			}

			List<CardDetailConfigDTO> cardDetailConfigData = null;
			if (isIntExtPaymentGateway) {
				// Get payment gateway configuration data, avoid getting for 'external'
				cardDetailConfigData = paymentBrokerBD.getPaymentGatewayCardConfigData(ipgIdentificationParamsDTO.getIpgId()
						.toString());
				Map<String, String> cardStoreDataMap = CreditCardUtil.getDBStroreData(cardDetailConfigData, txtCardNo, txtName,
						strExpiry, txtSCode);
				txtCardNo = cardStoreDataMap.get(FieldName.CARDNUMBER.code());
				txtName = cardStoreDataMap.get(FieldName.CARDHOLDERNAME.code());
				strExpiry = cardStoreDataMap.get(FieldName.EXPIRYDATE.code());
				txtSCode = cardStoreDataMap.get(FieldName.CVV.code());
			}

			if (isIntExtPaymentGateway) {
				lccClientPaymentAssembler.addInternalCardPayment(Integer.parseInt(selCardType), strExpiry, txtCardNo, txtName,
						null, txtSCode, totalBaseCardPayAmount, AppIndicatorEnum.APP_IBE, TnxModeEnum.SECURE_3D,
						ipgIdentificationParamsDTO, payCurrencyDTO, new Date(), txtName, "", null, null, null);
			} else {
				lccClientPaymentAssembler.addExternalCardPayment(Integer.parseInt(selCardType), "", totalBaseCardPayAmount,
						AppIndicatorEnum.APP_IBE, TnxModeEnum.SECURE_3D, null, ipgIdentificationParamsDTO, payCurrencyDTO,
						new Date(), null, null, isSupportOfflinePayment, null);
			}

			// Getting the contact information
			CommonReservationContactInfo reservationContactInfo = ReservationUtil.retrieveContactInfo(contactInfo);
			String originatorCarrierCode = getOriginatorCarrierCode(flightSegmentTOs);

			if (resPostPay.getPnr() == null) {
				PutOnHoldBeforePaymentDTO onHoldBeforePaymentDTO = SessionUtil.getPutOnHoldBeforePaymentDTO(request);
				if (onHoldBeforePaymentDTO.isPutOnHoldBeforePayment()) {
					validatePaxPayment(onHoldBeforePaymentDTO.getTotalTicketPrice(),
							lccClientPaymentAssembler.getTotalPayAmount(), loyaltyCredit, lmsPaymentAmount,
							resInfo.getSelectedExternalCharges(), totalCashDiscount, voucherPaymentAmount);
					pnr = onHoldBeforePaymentDTO.getPutOnHoldBeforePaymentPnr();
				}
			}
			isCsOcFlightAvailable = ReservationUtil.isCsOcFlightAvailable(flightSegmentTOs);

			Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap = ModuleServiceLocator.getAirproxyReservationBD()
					.makeTemporyPaymentEntry(pnr, reservationContactInfo, lccClientPaymentAssembler, true,
							resInfo.getSelectedSystem(), originatorCarrierCode, trackInfoDTO, isCsOcFlightAvailable);

			if (log.isDebugEnabled()) {
				log.debug("Interline paymentgateway got the pg info");
			}

			String ipgPNR = null;
			if (pnr == null || pnr.isEmpty()) {
				ipgPNR = LCCClientApiUtils.getPNRFromLccTmpPayMap(mapTempPayMap);
			} else {
				ipgPNR = pnr;
			}

			int temporyPayId = LCCClientApiUtils.getTmpPayIdFromTmpPayMap(mapTempPayMap);

			totalCreditCardPaymentAmount = lccClientPaymentAssembler.getTotalPayAmount();

			ipgRequestDTO.setAmount(AccelAeroRounderPolicy.convertAndRound(lccClientPaymentAssembler.getTotalPayAmount(),
					payCurrencyDTO).toString());

			log.debug("===============Tracking Pyament Geteway Issue: totalTicketPrice: " + totalCreditCardPaymentAmount
					+ " exchangeRate.getMultiplyingExchangeRate(): " + exchangeRate.getMultiplyingExchangeRate());
			log.debug("===============Tracking Pyament Geteway Issue: payCurrencyDTO.getPayCurrencyCode: "
					+ payCurrencyDTO.getPayCurrencyCode() + " exchangeRate.getMultiplyingExchangeRate(): "
					+ payCurrencyDTO.getTotalPayCurrencyAmount());
			// ****************************************************************************************

			ipgRequestDTO.setApplicationTransactionId(temporyPayId);

			ipgRequestDTO.setReturnUrl(RETURN_URL);
			ipgRequestDTO.setStatusUrl(STATUS_URL);
			ipgRequestDTO.setReceiptUrl(RECEIPT_URL);

			ipgRequestDTO.setOfferUrl(globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, carrierCode));
			ipgRequestDTO.setApplicationIndicator(AppIndicatorEnum.APP_IBE);
			ipgRequestDTO.setPnr(ipgPNR);
			ipgRequestDTO.setNoOfDecimals(2);

			ipgRequestDTO.setRequestedCarrierCode(carrierCode);
			ipgRequestDTO.setSessionID(request.getSession().getId());

			ipgRequestDTO.setEmail(StringUtils.trimToEmpty(contactInfo.getEmailAddress()));
			ipgRequestDTO.setContactFirstName(StringUtils.trimToEmpty(contactInfo.getFirstName()));
			ipgRequestDTO.setContactLastName(StringUtils.trimToEmpty(contactInfo.getLastName()));
			ipgRequestDTO.setContactAddressLine1(StringUtils.trimToEmpty(contactInfo.getAddresline()));
			ipgRequestDTO.setContactAddressLine2(StringUtils.trimToEmpty(contactInfo.getAddresStreet()));
			ipgRequestDTO.setContactCity(StringUtils.trimToEmpty(contactInfo.getCity()));
			ipgRequestDTO.setContactState(StringUtils.trimToEmpty(contactInfo.getState()));
			ipgRequestDTO.setContactCountryCode(StringUtils.trimToEmpty(contactInfo.getCountry()));
			ipgRequestDTO.setContactCountryName(StringUtils.trimToEmpty(contactInfo.getCountryName()));
			ipgRequestDTO.setContactMobileNumber(StringUtils.trimToEmpty(contactInfo.getmCountry() + "-" + contactInfo.getmArea()
					+ "-" + contactInfo.getmNumber()));
			ipgRequestDTO.setContactPhoneNumber(StringUtils.trimToEmpty(contactInfo.getlCountry() + "-" + contactInfo.getlArea()
					+ "-" + contactInfo.getlNumber()));
			ipgRequestDTO.setSessionLanguageCode(StringUtils.trimToEmpty(SessionUtil.getLanguage(request)));
			ipgRequestDTO.setUserIPAddress(getClientInfoDTO().getIpAddress());
			ipgRequestDTO.setUserAgent(getClientInfoDTO().getBrowser());
			ipgRequestDTO.setServiceAppFlow(false);

			// setting original country for ipcountry code in test systems
			if (AppSysParamsUtil.isTestStstem()) {
				String testSysOriginCountry = (String) request.getSession().getAttribute("originCountry");
				if (testSysOriginCountry == null || testSysOriginCountry.trim().length() == 0) {
					testSysOriginCountry = "OT"; // Other country
					// testSysOriginCountry = "AE"; // Other country
				}
				ipgRequestDTO.setIpCountryCode(testSysOriginCountry);
			} else {
				ipgRequestDTO.setIpCountryCode(commonMasterBD.getCountryByIpAddress(getClientInfoDTO().getIpAddress()));
			}

			ipgRequestDTO.setSelectedSystem(resInfo.getSelectedSystem().name());
			ipgRequestDTO.setContactPostalCode(StringUtils.trimToEmpty(contactInfo.getZipCode()));

			PAYPALResponse paypalResponse = null;
			if (pgw.getProviderCode().equalsIgnoreCase("PAYPAL")) {
				paypalResponse = new PAYPALResponse();
				setPPRequestDTO(ipgRequestDTO, payCurrencyDTO, ipgProps);// set pp data
				userInputDTO.setCreditCardType(getStandardCardType(Integer.parseInt(card.getCardType())));
				paypalResponse.setCreditCardType(card.getCardType());
				ipgRequestDTO.setIntExtPaymentGateway(isIntExtPaymentGateway);
				if (isIntExtPaymentGateway) {
					setPPCreditCardInfo(ipgRequestDTO);
					paypalResponse.setCVV2(card.getCardCVV());
				}
			}

			ReservationUtil.addTravelData(ipgRequestDTO, flightSegmentTOs);

			if (log.isDebugEnabled()) {
				log.debug("Interline paymentgateway call for payment broker");
			}

			if (isIntExtPaymentGateway) {
				// Get payment gateway configuration data, avoid getting for 'external'
				cardDetailConfigData = paymentBrokerBD.getPaymentGatewayCardConfigData(ipgIdentificationParamsDTO.getIpgId()
						.toString());
			}
			IPGRequestResultsDTO ipgRequestResultsDTO = null;
			String strRequestData = null;
			String strRequestDataForPP = null;

			ipgRequestDTO.setPaymentGateWayName(pgw.getProviderName());

			if (isSupportOfflinePayment) {
				long onholdexpireIntervalinSec = calculateOnholdExpirePeriod(OnHold.IBEOFFLINEPAYMENT);

				ipgRequestDTO.setExpireMTCOffLinePeriod(onholdexpireIntervalinSec);
			}
			
			// adding selected external charges to "resPostPay"
			// these charges will be referred in interLinePostCardRetryAction
			Set<EXTERNAL_CHARGES> chargeKeySet = resInfo.getSelectedExternalCharges().keySet();
			for (EXTERNAL_CHARGES selectedExtChg : chargeKeySet) {
				resPostPay.addExternalCharge(resInfo.getSelectedExternalCharges().get(selectedExtChg));
			}

			if (isIntExtPaymentGateway) {
				if (pgw.getProviderCode().equalsIgnoreCase("PAYPAL")) {
					ipgRequestDTO.setUserInputDTO(userInputDTO);
					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
					paypalResponse.setDoDirectPaymentResponseType(ipgRequestResultsDTO.getDirectPaymentResponse());
					strRequestDataForPP = RETURN_URL;
					if (!(ipgRequestResultsDTO.getDirectPaymentResponse().getAck().getValue().toString()
							.equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS))) {
						ErrorType[] errorArry = ipgRequestResultsDTO.getSetExpressCheckoutResponse().getErrors();
						throw new ModuleException(PAYPALPaymentUtils.getFilteredErrorCode(errorArry[0].getErrorCode().toString()));
					}
				} else {
					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
				}
			} else {
				if (pgw.getProviderCode().equalsIgnoreCase("PAYPAL")) {
					ipgRequestDTO.setUserInputDTO(userInputDTO);
					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);
					paypalResponse.setSetExpressCheckoutResponseType(ipgRequestResultsDTO.getSetExpressCheckoutResponse());
					strRequestDataForPP = ipgRequestResultsDTO.getRequestData();
					if (!(ipgRequestResultsDTO.getSetExpressCheckoutResponse().getAck().getValue().toString()
							.equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS))) {
						ErrorType[] errorArry = ipgRequestResultsDTO.getSetExpressCheckoutResponse().getErrors();
						throw new ModuleException(PAYPALPaymentUtils.getFilteredErrorCode(errorArry[0].getErrorCode().toString()));
					}

				} else if (pgw.getProviderCode().equalsIgnoreCase("QIWI")) {

					resPostPay.setPaymentGateWay(pgw);
					resPostPay.setPnr(ipgPNR);

					ipgRequestDTO.setContactMobileNumber(request.getParameter(QiwiPRequest.HDN_QIWI_MOBILE));

					// create on hold reservation
					LCCClientReservation res = this.createOnHoldReservation(isSupportOfflinePayment, payCurrencyDTO,
							totalCreditCardPaymentAmount, loyaltyCredit, lmsPaymentAmount, ipgPNR, pgw.getOnholdReleaseTime(), voucherPaymentAmount);
					if (res != null) {
						createdOnhold = true;
						ipgRequestDTO.setPnr(res.getPNR());
					}

					resPostPay.setOnHoldCreated(createdOnhold);
					ipgRequestDTO.setInvoiceExpirationTime(this.setupOnholdOfflinePaymentTime(res));

					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);
					QiwiPResponse invoiceCreationResponse = ipgRequestResultsDTO.getQiwiResponse();

					isSwitchToExternalURL = !invoiceCreationResponse.isOfflineMode();

					resPostPay.setTimeToSpare(invoiceCreationResponse.getTimeToSpare());
					resPostPay.setSwitchToExternalURL(isSwitchToExternalURL);
					resPostPay.setInvoiceStatus(invoiceCreationResponse.getStatus());
					resPostPay.setBillId(invoiceCreationResponse.getBillId());

					if (invoiceCreationResponse.getStatus().equals(QiwiPRequest.FAIL)) {

						log.debug("[InterlinePostCardDetailAction::Qiwi Invoice creation Failed]");
						resPostPay.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
								ipgRequestResultsDTO.getPaymentBrokerRefNo(), mapTempPayMap));
						resPostPay.setPaymentGateWay(pgw);
						request.setAttribute(QiwiPRequest.QIWI_ERROR_CODE, ipgRequestResultsDTO.getErrorCode());
						resPostPay.setErrorCode(invoiceCreationResponse.getError());

						return StrutsConstants.Result.BYPASS;
					}
					// offline payment mode
					if (!isSwitchToExternalURL && invoiceCreationResponse.getStatus().equals(QiwiPRequest.WAITING)) {

						log.debug("[InterlinePostCardDetailAction::Qiwi Offline Mode]");
						pgw.setSwitchToExternalURL(isSwitchToExternalURL);
						return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
					}

					requestMethod = PaymentBrokerBD.PaymentBrokerProperties.REQUEST_METHODS.POST.toString();

				} else if (pgw.getProviderName().equalsIgnoreCase(PAY_FORT_STORE)) {

					resPostPay.setPaymentGateWay(pgw);
					resPostPay.setPnr(ipgPNR);
					resPostPay.setGroupPnr(groupPNR);
					resPostPay.setPayFortStatus(true);
					resPostPay.setPayFortStatusVoucher(false);

					ipgRequestDTO.setContactMobileNumber(request.getParameter("hdnPayFortMobileNumber"));
					ipgRequestDTO.setContactEmail(request.getParameter("hdnPayFortEmail"));

					// create on hold reservation
					LCCClientReservation res = this.createOnHoldReservation(isSupportOfflinePayment, payCurrencyDTO,
							totalCreditCardPaymentAmount, loyaltyCredit, lmsPaymentAmount, ipgPNR, pgw.getOnholdReleaseTime(), voucherPaymentAmount);
					if (res != null) {
						createdOnhold = true;
						ipgRequestDTO.setPnr(res.getPNR());
					}

					resPostPay.setOnHoldCreated(createdOnhold);
					ipgRequestDTO.setInvoiceExpirationTime(this.setupOnholdOfflinePaymentTime(res));

					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

					resPostPay.setSwitchToExternalURL(isSwitchToExternalURL);
					resPostPay.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
							ipgRequestResultsDTO.getPaymentBrokerRefNo(), mapTempPayMap));

					if (ipgRequestResultsDTO.getRequestData().equals("")) {

						log.debug("[InterlinePostCardDetailAction::PayFort Voucher Creation Failed]");
						return StrutsConstants.Result.CARD_PAYMENT_ERROR;
					} else {
						if (!createdOnhold) {
							int releaseTimeInMinutes = pgw.getOnholdReleaseTime();
							Date zuluLocalTime = CalendarUtil.getCurrentZuluDateTime();
							long longZuluLocalTime = zuluLocalTime.getTime();
							Date afterAddingPGWonholdTime = new Date(longZuluLocalTime + (releaseTimeInMinutes * 60000));
							String versionNo = "0";
							if (version != null && !"".equals(version)) {
								versionNo = version;
							}
							if (releaseTimeInMinutes > 0) {
								trackInfoDTO.setMarketingUserId(marketingUserId);
								ModuleServiceLocator.getAirproxyReservationBD().extendOnHold(ipgPNR, afterAddingPGWonholdTime,
										versionNo, groupPNR, getClientInfoDTO(), trackInfoDTO);
							}
						}
						resPostPay.setPayFortStatusVoucher(true);
						request.setAttribute(Pay_AT_StoreResponseDTO.REQUEST_ID,
								ipgRequestResultsDTO.getPostDataMap().get(Pay_AT_StoreResponseDTO.REQUEST_ID));
						request.setAttribute(Pay_AT_StoreResponseDTO.VOUCHER_ID,
								ipgRequestResultsDTO.getPostDataMap().get(Pay_AT_StoreResponseDTO.VOUCHER_ID));
						request.setAttribute(Pay_AT_StoreResponseDTO.PAYFORT, StrutsConstants.Result.SUCCESS);
						request.setAttribute(Pay_AT_StoreResponseDTO.PAY_AT_STORE_VOUCHER, StrutsConstants.Result.SUCCESS);

						log.debug("[InterlinePostCardDetailAction::PayFort Offline]");
						pgw.setSwitchToExternalURL(isSwitchToExternalURL);
						return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
					}

				} else {
					validatePGWCommonDetailsPanel(request, ipgRequestDTO, pgw);
					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);
				}
			}

			if (ipgRequestResultsDTO.getAmeriaBankSessionInfo() != null) {
				if (ipgRequestResultsDTO.getAmeriaBankSessionInfo().getError() != null) {
					log.error(ipgRequestResultsDTO.getAmeriaBankSessionInfo().getError());
					ModuleException me = new ModuleException("payment.api.ameriabank.payment.failed");
					throw me;
				}

				resPostPay.setAmeriaBankSessionInfo(ipgRequestResultsDTO.getAmeriaBankSessionInfo());
			}

			if (pgw.getProviderCode().equalsIgnoreCase("PAYPAL")) {
				resPostPay.setPaypalResponse(paypalResponse);
				strRequestData = strRequestDataForPP;
			} else {
				strRequestData = ipgRequestResultsDTO.getRequestData();
			}
			resPostPay.setPnr(ipgPNR); // update pnr for the create reservation
			resPostPay.setIpgRefenceNo(ipgRequestResultsDTO.getAccelAeroTransactionRef());
			resPostPay.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
					ipgRequestResultsDTO.getPaymentBrokerRefNo(), mapTempPayMap));

			resPostPay.setPaymentGateWay(pgw);

			request.setAttribute(RequestAttribute.REQUEST_METHOD, requestMethod);
			request.setAttribute(RequestAttribute.PAY_GATEWAY_TYPE, brokerType);
			request.setAttribute(RequestAttribute.POST_INPUT_DATA_FORM_HTML, strRequestData);
			request.setAttribute(RequestAttribute.PAY_GATEWAY_SWITCH_TOEXTERNAL_URL, isSwitchToExternalURL);

			if (SessionUtil.getIbeReservationPostDTO(request) != null
					&& SessionUtil.getIbeReservationPostDTO(request).getPnr() != null) {
				SessionUtil.getIbeReservationPostDTO(request).setPnr(null);
			}

			if (pgw.getProviderCode().equalsIgnoreCase("PAYPAL")) {
				return StrutsConstants.Result.SUCCESS;
			}

			resPostPay.setViewPaymentInIframe(isViewPaymentInIframe);
			if (isViewPaymentInIframe && !isSwitchToExternalURL
					&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL.equals(brokerType)) {
				request.setAttribute(WebConstants.DISPLAY_TOP_BOTTOM_BANNERS, true);
				return StrutsConstants.Result.WIRECARD_SUCCESS;
			}

			// Exclude Internal - External Payment gateway request data from log
			if (isIntExtPaymentGateway) {
				strRequestData = "";
				log.debug("Payment gateway type : Exclude Internal");
			}
			try {
				// TODO re-factor
				if (log.isInfoEnabled()) {
					log.info("Before posting IPG request :" + "|pnr=" + ipgPNR + "|merchantTxnRef="
							+ ipgRequestResultsDTO.getAccelAeroTransactionRef() + "|tptId=" + temporyPayId
							+ "|totalPayAmountInBaseCurr=" + totalBaseCardPayAmount + "|payCurCode="
							+ payCurrencyDTO.getPayCurrencyCode() + "|payCurExRate=" + exchangeRate.getMultiplyingExchangeRate()
							+ "|totalPayAmountInPayCurr=" + ipgRequestDTO.getAmount() + "|ContactInfo::firstName="
							+ ipgRequestDTO.getContactFirstName() + ",lastName=" + ipgRequestDTO.getContactLastName() + ",email="
							+ ipgRequestDTO.getEmail() + ",country=" + ipgRequestDTO.getContactCountryCode() + ",mobile="
							+ ipgRequestDTO.getContactMobileNumber() + ",telephone=" + ipgRequestDTO.getContactPhoneNumber()
							+ ",sessLang=" + ipgRequestDTO.getSessionLanguageCode() + "|sessId="
							+ request.getRequestedSessionId() + "|request=[" + strRequestData + "]");
				}
			} catch (Throwable e) {
				log.error("Error in logging data prior to IPG request", e);
			}

			if (!createdOnhold) {
				LCCClientReservation res = this.createOnHoldReservation(isSupportOfflinePayment, payCurrencyDTO,
						totalCreditCardPaymentAmount, loyaltyCredit, lmsPaymentAmount, ipgPNR, pgw.getOnholdReleaseTime(), voucherPaymentAmount);
				if (res != null) {
					createdOnhold = true;
				}
				resPostPay.setOnHoldCreated(createdOnhold);
			}

			if (pgw.getProviderCode().equalsIgnoreCase("PAYFORT") && !createdOnhold) {

				int releaseTimeInMinutes = pgw.getOnholdReleaseTime();

				Date zuluLocalTime = CalendarUtil.getCurrentZuluDateTime();
				long longZuluLocalTime = zuluLocalTime.getTime();
				Date afterAddingPGWonholdTime = new Date(longZuluLocalTime + (releaseTimeInMinutes * 60000));
				String versionNo = "0";
				if (version != null && !"".equals(version)) {
					versionNo = version;
				}
				if (releaseTimeInMinutes > 0) {
					trackInfoDTO.setMarketingUserId(marketingUserId);
					ModuleServiceLocator.getAirproxyReservationBD().extendOnHold(ipgPNR, afterAddingPGWonholdTime, versionNo,
							groupPNR, getClientInfoDTO(), trackInfoDTO);
				}

			}
			// If the PGW response is an XML, need to process the XML to handle the response(eg : OGONE PGW)
			if (responseTypeXML != null && !responseTypeXML.equals("") && responseTypeXML.equals("true")) {
				log.debug("Execute the payment and get the response for XML response type PGWs");
				// XMLResponse response = getOgoneXMLResponse(ipgRequestResultsDTO.getPostDataMap());
				XMLResponseDTO response = paymentBrokerBD.getXMLResponse(ipgRequestDTO.getIpgIdentificationParamsDTO(),
						ipgRequestResultsDTO.getPostDataMap());
				log.debug("Recieved XMLresponse");
				ArrayList<Integer> tptIds = new ArrayList<Integer>();
				tptIds.add(temporyPayId);
				log.debug("Redirecting depending on the response");
				resPostPay.setSecurePayment3D(false);
				if (response != null && Integer.valueOf(response.getStatus()) == 46) {
					// add to session
					if (log.isDebugEnabled()) {
						log.debug("3D Secure payment: PNR:" + ipgPNR + "SessionID :" + request.getRequestedSessionId());
					}
					resInfo.setBank3DSecureHTML(response.getHtml());
					resPostPay.setSwitchToExternalURL(true);
					resPostPay.setSecurePayment3D(true);
					return StrutsConstants.Result.LANDING_3DSECURE;
				} else if (response != null && !response.getStatus().equals("")) {
					request.setAttribute(WebConstants.XML_RESPONSE_PARAMETERS, response);
					return StrutsConstants.Result.BYPASS;
				} else if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
					return StrutsConstants.Result.CARD_PAYMENT_ERROR;
				} else {
					return StrutsConstants.Result.ERROR;
				}
			}
			if (ipgRequestResultsDTO.getBehpardakhtResponse() != null
					&& ("error").equals(ipgRequestResultsDTO.getBehpardakhtResponse().getError())) {
				return StrutsConstants.Result.ERROR;
			}

		} catch (ModuleException e) {
			log.error("Interline paymentgateway call for payment broker", e);
			revertLoyaltyRedemption();
			if ("paymentbroker.card.invalid".equals(e.getExceptionCode())) {
				request.setAttribute(WebConstants.INVALID_CARD, true);
				resInfo.setFromPaymentPage(true);
				resInfo.setFromPostCardDetails(false);
				SessionUtil.resetSesionCardDataInError(request);
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
						I18NUtil.getMessage(e.getExceptionCode(), SessionUtil.getLanguage(request)));
				if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
					return StrutsConstants.Result.CARD_PAYMENT_ERROR;
				}
			} else {
				releaseBlockedSeats(request);
				if (ResponseCodes.DUPLICATE_NAMES_IN_FLIGHT_SEGMENT.equals(e.getExceptionCode())) {
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
							getServerErrorMessage(request, e.getExceptionCode()));
					// AS a precaution duplicate names are not reviled to out side
					// in ibe,e.getExceptionDetails() contains the duplicate names
				} else {
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
							I18NUtil.getMessage(e.getExceptionCode(), SessionUtil.getLanguage(request)));
				}
				SystemUtil.setCommonParameters(request, getCommonParams());
				SessionUtil.resetSesionDataInError(request);
			}

			if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
				return StrutsConstants.Result.CARD_PAYMENT_ERROR;
			} else {
				return StrutsConstants.Result.ERROR;
			}

		} catch (Exception e) {
			log.error("Interline paymentgateway call for payment broker", e);
			revertLoyaltyRedemption();
			releaseBlockedSeats(request);
			SystemUtil.setCommonParameters(request, getCommonParams());
			SessionUtil.resetSesionDataInError(request);

			if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
				return StrutsConstants.Result.CARD_PAYMENT_ERROR;
			} else {
				return StrutsConstants.Result.ERROR;
			}
		}

		return StrutsConstants.Result.SUCCESS;
	}

	private String getCardType() {
		String selCardType = "5";
		if (card != null && card.getCardType() != null && !card.getCardType().trim().isEmpty()) {
			selCardType = card.getCardType();
		}
		return selCardType;
	}

	/*
	 * Validate reservation data - As we are keeping price quote in session, user can change data using separate browser
	 * session
	 */
	private void checkReservationDataIntergrity(PriceInfoTO priceInfoTO, List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		if (!modifyAncillary && !makePayment) {
			/* Validate Request Flight Details and Session Flight Details */
			if (SessionUtil.getRequestSessionIdentifier(request) == null || requestSessionIdentifier == null) {
				throw new ModuleException("msg.multiple.invalid.flight");
			} else if (SessionUtil.getRequestSessionIdentifier(request) != null
					&& !SessionUtil.getRequestSessionIdentifier(request).equals(requestSessionIdentifier)) {
				throw new ModuleException("msg.multiple.invalid.flight");
			}
			/* Own airline flight details validate */
			ReservationUtil.validatePassengerFlightDetails(flightSegmentTOs, priceInfoTO);
		} else {
			if (priceInfoTO != null) {
				throw new ModuleException("msg.multiple.invalid.flight");
			}
		}
	}

	private void validatePaxName(List<ReservationPaxTO> paxList) throws ModuleException {
		if (paxList != null) {
			for (ReservationPaxTO pax : paxList) {
				if (pax != null) {
					if (PaxTypeTO.ADULT.equalsIgnoreCase(pax.getPaxType())
							&& (pax.getFirstName() == null || pax.getFirstName().trim().isEmpty())) {
						log.error("System detected pax name as null. " + request.getRequestedSessionId());
						throw new ModuleException("server.default.operation.fail");
					}
				} else {
					log.error("System detected pax object as null. " + request.getRequestedSessionId());
				}
			}
		} else {
			log.error("System detected empty pax list. " + request.getRequestedSessionId());
		}
	}
	
	private Date getOnholdReleaseTime(IBEReservationPostPaymentDTO postPayDTO, Date depDateTimeZulu) throws ModuleException {
		
		PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
		FareTypeTO fareTypeTo = priceInfoTO.getFareTypeTO();
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(
				postPayDTO.getSelectedFlightSegments(), fareTypeTo);
		
		return ReservationUtil.calculateOnHoldReleaseTime(OnHold.IBEPAYMENT, depDateTimeZulu,
				onHoldReleaseTimeDTO);	
		
	}

	private boolean putOnHoldBeforePayment(IBEReservationInfoDTO resInfo) throws ModuleException {
		if (log.isInfoEnabled()) {
			log.info("### Enter onhold validation data for ibe payment : " + request.getRequestedSessionId());
		}	
		
		if (AppSysParamsUtil.isOnHoldEnable(OnHold.IBEPAYMENT) && !modifySegment && !modifyAncillary && !addGroundSegment) {
			
			IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
			Date depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(postPayDTO.getSelectedFlightSegments());
			
			if (getOnholdReleaseTime(postPayDTO,depDateTimeZulu) != null
					&& ModuleServiceLocator.getAirproxySearchAndQuoteBD().isOnholdEnabled(postPayDTO.getSelectedFlightSegments(),
							ReservationUtil.getReservationPaxList((List<ReservationPaxTO>) postPayDTO.getPaxList()),
							contactInfo.getEmailAddress(), ResOnholdValidationDTO.IBE_PAYMENT_VALIDATION,
							getTrackInfo().getOriginChannelId(), resInfo.getSelectedSystem(),
							getClientInfoDTO().getIpAddress(), getTrackInfo())) {
				if (log.isDebugEnabled())
					log.debug("onhold data validation end - status true :");
				return true;
			} else {
				if (log.isInfoEnabled()) {
					Date currentTime = new Date();
					log.info("### IBE payment onhold enable false - validate data: " + request.getRequestedSessionId());
					StringBuilder logInfo = new StringBuilder();
					logInfo.append("### IBE onhold applicable data:").append(postPayDTO.getPnr());
					logInfo.append("| Current time :").append(currentTime);
					logInfo.append("| Departure zulu time: ").append(depDateTimeZulu);
					if (depDateTimeZulu != null) {
						logInfo.append("| Time to flight depature applicable(with in 3 days) :").append(
								((depDateTimeZulu.getTime() - currentTime.getTime()) - 3 * 24 * 60 * 60 * 1000) > 0);
					}
					logInfo.append("| Email address: ").append(contactInfo.getEmailAddress());
					logInfo.append("| IP Address : ").append(getClientInfoDTO().getIpAddress());
					logInfo.append("| Session ID: ").append(request.getRequestedSessionId());
					log.info(logInfo.toString());
				}
			}
		} else {
			if (log.isDebugEnabled())
				log.debug("### IBE payment onhold enable false - Basic condition " + request.getRequestedSessionId());

		}
		if (log.isDebugEnabled())
			log.debug("### END onhold validation data for ibe payment : " + request.getRequestedSessionId());

		return false;
	}

	private String getOriginatorCarrierCode(List<FlightSegmentTO> flightSegmentTOs) {
		String originatorCarrierCode = null;
		if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
			// sort by dept time zulu
			Collections.sort(flightSegmentTOs);
			FlightSegmentTO flightSegment = flightSegmentTOs.get(0);
			if (flightSegment != null && flightSegment.getFlightNumber() != null && flightSegment.getFlightNumber().length() > 2) {
				originatorCarrierCode = flightSegment.getFlightNumber().substring(0, 2);
			}
		}

		return originatorCarrierCode;
	}

	public void setPPCreditCardInfo(IPGRequestDTO ipgRequestDTO) {
		userInputDTO.setCreditCardNumber(card.getCardNo());
		userInputDTO.setCVV2(card.getCardCVV());
		userInputDTO.setExpMonth(Integer.parseInt(ipgRequestDTO.getExpiryDate().substring(2, 4)));
		userInputDTO.setExpYear(2000 + Integer.parseInt(ipgRequestDTO.getExpiryDate().substring(0, 2)));
		userInputDTO.setBuyerAddress1(card.getBillingAddress1());
		userInputDTO.setBuyerAddress2(card.getBillingAddress2());
		userInputDTO.setBuyerCity(card.getCity());
		userInputDTO.setPostalCode(card.getPostalCode());
		userInputDTO.setBuyerState(card.getState());
	}

	private String getStandardCardType(int card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 11-PAYPAL]
		String myString = null;
		char[] stringArray = PaymentType.getPaymentTypeDesc(card).toLowerCase().toCharArray();
		stringArray[0] = Character.toUpperCase(stringArray[0]);
		myString = new String(stringArray);
		return myString;
	}

	public void setPPRequestDTO(IPGRequestDTO ipgRequestDTO, PayCurrencyDTO payCurrencyDTO, Properties ipgProps)
			throws ModuleException {
		userInputDTO.setAmount(ipgRequestDTO.getAmount());
		userInputDTO.setBuyerFirstName(ipgRequestDTO.getContactFirstName());
		userInputDTO.setBuyerLastName(ipgRequestDTO.getContactLastName());
		userInputDTO.setCountryCodeType(CountryCodeType.fromString(ipgRequestDTO.getContactCountryCode()));
		userInputDTO.setCurrencyCodeType(CurrencyCodeType.fromString(payCurrencyDTO.getPayCurrencyCode()));
		userInputDTO.setInvoiceId(ipgRequestDTO.getPnr());
		userInputDTO.setIPAddress(getClientInfoDTO().getIpAddress());
		userInputDTO.setNoShipping(PAYPALPaymentUtils.NoShipping);
		userInputDTO.setPayerCountry(StringUtils.trimToEmpty(ipgRequestDTO.getContactCountryCode()));
		userInputDTO.setPaymentAction(PaymentActionCodeType.Sale);
		userInputDTO.setReturnUrl(RETURN_URL);
		userInputDTO.setTypeOfGoods(PAYPALPaymentUtils.TypeOfGoods);
		userInputDTO.setUserId(ipgRequestDTO.getSessionID());// use to store something like sessions
	}

	private void checkSeatAvailability(IBEReservationInfoDTO resInfo, FlightPriceRQ flightPriceRQ, boolean isFlexiQuote)
			throws ModuleException {
		if (resInfo.getSelectedSystem() == SYSTEM.AA) {
			OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(flightPriceRQ, resInfo
					.getPriceInfoTO().getFareSegChargeTO());
			if (AppSysParamsUtil.isIBEBlockSeatEnabled()) {
				FareSegChargeTO fareSegChargeTO = resInfo.getPriceInfoTO().getFareSegChargeTO();
				Collection<OndFareDTO> collFares = AirproxyModuleUtils.getAirReservationQueryBD().recreateFareSegCharges(
						ondRebuildCriteria);
				Collection<TempSegBcAlloc> blockSeats = ModuleServiceLocator.getReservationBD().blockSeats(collFares, null);
				fareSegChargeTO.setBlockSeatIds(blockSeats);
			} else {
				if (!ModuleServiceLocator.getReservationQueryBD().checkInventoryAvailability(ondRebuildCriteria)) {
					throw new ModuleException("server.error.fare.nolonger.available");
				}
			}
		}
	}

	private void setSubStations(FlightSearchDTO flightSearchDTO, List<FlightSegmentTO> fligtSegList) {
		if (AppSysParamsUtil.isGroundServiceEnabled()) {
			for (FlightSegmentTO flightSegmentTO : fligtSegList) {
				String subStationShortName = null;

				if (AddModifySurfaceSegmentValidations.isMatchingSegmentCodeForFromSubStation(flightSegmentTO.getSegmentCode(),
						flightSearchDTO.getFromAirportSubStation(), flightSegmentTO.isReturnFlag())) {
					subStationShortName = flightSearchDTO.getFromAirportSubStation();
				}
				if (AddModifySurfaceSegmentValidations.isMatchingSegmentCodeForToSubStation(flightSegmentTO.getSegmentCode(),
						flightSearchDTO.getToAirportSubStation(), flightSegmentTO.isReturnFlag())) {
					subStationShortName = flightSearchDTO.getToAirportSubStation();
				}
				flightSegmentTO.setSubStationShortName(subStationShortName);
			}
		}
	}

	private LCCClientReservation putOnHoldBeforePayment(PayCurrencyDTO payCurrencyDTO, BigDecimal creditCardPayAmount,
			BigDecimal loyaltyAmount, BigDecimal lmsPaymentAmount, BigDecimal voucherPaymentAmount) throws Exception {
		if (log.isDebugEnabled())
			log.debug("Satrt creating onhold:");
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
		IPGResponseDTO creditInfo = postPayDTO.getIpgResponseDTO();

		String strPayCurCode = postPayDTO.getPaymentGateWay().getPayCurrency();
		int ipgId = postPayDTO.getPaymentGateWay().getPaymentGateway();
		IPGIdentificationParamsDTO ipgDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(new Integer(ipgId),
				strPayCurCode);
		Date paymentTimestamp = null;
		String loyaltyAccount = SessionUtil.getLoyaltyAccountNo(request);
		String loyaltyAgent = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.LOYALITY_AGENT_CODE);

		PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
		FareTypeTO fareTypeTo = priceInfoTO.getFareTypeTO();
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(
				postPayDTO.getSelectedFlightSegments(), fareTypeTo);
		Date depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(postPayDTO.getSelectedFlightSegments());
		Date pnrReleaseTimeZulu = ReservationUtil.calculateOnHoldReleaseTime(OnHold.IBEPAYMENT, depDateTimeZulu,
				onHoldReleaseTimeDTO);
		String brokerType = null;
		if (SessionUtil.getCustomerId(request) > 0) {
			contactInfo.setCustomerId(SessionUtil.getCustomerId(request));
		}

		ReservationDiscountDTO resDiscountDTO = ReservationUtil.calculateDiscountForReservation(resInfo, postPayDTO.getPaxList(),
				postPayDTO.getFlightPriceRQ(), getTrackInfo(), false, resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(),
				resInfo.getTransactionId(), true);

		LCCClientReservation commonReservation = ReservationUtil.makeReservation(postPayDTO.getPnr(), postPayDTO, resInfo,
				contactInfo, true, null, creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp, loyaltyAccount, loyaltyAgent,
				pnrReleaseTimeZulu, brokerType, this.getTrackInfo(), resDiscountDTO, null);

		setPutOnHoldBeforePaymentSessionData(commonReservation.getPNR());
		ReservationUtil.addTicketPriceOnHoldBeforePaymentSessionData(request, commonReservation.getTotalTicketPrice());
		validatePaxPayment(commonReservation.getTotalTicketPrice(), creditCardPayAmount, loyaltyAmount, lmsPaymentAmount,
				resInfo.getSelectedExternalCharges(), null, voucherPaymentAmount);

		if (log.isDebugEnabled())
			log.debug("End creating onhold:");
		return commonReservation;
	}

	private LCCClientReservation putOnHoldForOfflinePayment(PayCurrencyDTO payCurrencyDTO, BigDecimal creditCardPayAmount,
			BigDecimal loyaltyAmount, BigDecimal lmsLoyaltyPayment, int onholdReleaseTime,  BigDecimal voucherPaymentAmount) throws Exception {
		if (log.isDebugEnabled())
			log.debug("Start creating onhold:");
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
		IPGResponseDTO creditInfo = postPayDTO.getIpgResponseDTO();
		Date pgwWiseOnholdTime = null;

		String strPayCurCode = postPayDTO.getPaymentGateWay().getPayCurrency();
		int ipgId = postPayDTO.getPaymentGateWay().getPaymentGateway();
		IPGIdentificationParamsDTO ipgDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(new Integer(ipgId),
				strPayCurCode);
		Date paymentTimestamp = null;
		String loyaltyAccount = SessionUtil.getLoyaltyAccountNo(request);
		String loyaltyAgent = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.LOYALITY_AGENT_CODE);

		PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
		FareTypeTO fareTypeTo = priceInfoTO.getFareTypeTO();
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(
				postPayDTO.getSelectedFlightSegments(), fareTypeTo);
		Date depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(postPayDTO.getSelectedFlightSegments());
		Date pnrReleaseTimeZulu = ReservationUtil.calculateOnHoldReleaseTime(OnHold.IBEOFFLINEPAYMENT, depDateTimeZulu,
				onHoldReleaseTimeDTO);
		if (onholdReleaseTime > 0) {
			Date zuluLocalTime = CalendarUtil.getCurrentZuluDateTime();
			long longZuluLocalTime = zuluLocalTime.getTime();
			pnrReleaseTimeZulu = new Date(longZuluLocalTime + (onholdReleaseTime * 60000));
			pgwWiseOnholdTime = pnrReleaseTimeZulu;
		}

		String brokerType = null;
		if (SessionUtil.getCustomerId(request) > 0) {
			contactInfo.setCustomerId(SessionUtil.getCustomerId(request));
		}

		ReservationDiscountDTO resDiscountDTO = ReservationUtil.calculateDiscountForReservation(resInfo, postPayDTO.getPaxList(),
				postPayDTO.getFlightPriceRQ(), getTrackInfo(), false, resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(),
				resInfo.getTransactionId(), true);

		LCCClientReservation commonReservation = ReservationUtil.makeReservation(postPayDTO.getPnr(), postPayDTO, resInfo,
				contactInfo, true, null, creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp, loyaltyAccount, loyaltyAgent,
				pnrReleaseTimeZulu, brokerType, this.getTrackInfo(), resDiscountDTO, pgwWiseOnholdTime);

		setPutOnHoldBeforePaymentSessionData(commonReservation.getPNR());
		ReservationUtil.addTicketPriceOnHoldBeforePaymentSessionData(request, commonReservation.getTotalTicketPrice());
		validatePaxPayment(commonReservation.getTotalTicketPrice(), creditCardPayAmount, loyaltyAmount, lmsLoyaltyPayment,
				resInfo.getSelectedExternalCharges(), null, voucherPaymentAmount);

		if (log.isDebugEnabled())
			log.debug("End creating onhold:");

		if (commonReservation == null
				|| !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(commonReservation.getStatus())) {
			log.error("Can not create a onhold reservation for offline payment");
			throw new ModuleException(ExceptionConstants.ERR_PAYMENT_UNSUCESS);
		}
		return commonReservation;
	}

	private long calculateOnholdExpirePeriod(OnHold ibepayment) throws Exception {
		if (log.isDebugEnabled())
			log.debug("Satrt creating onhold:");
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
		IPGResponseDTO creditInfo = postPayDTO.getIpgResponseDTO();
		PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
		FareTypeTO fareTypeTo;
		if (priceInfoTO != null) {
			fareTypeTo = priceInfoTO.getFareTypeTO();
		} else {
			fareTypeTo = null;
		}

		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(
				postPayDTO.getSelectedFlightSegments(), fareTypeTo);
		Date depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(postPayDTO.getSelectedFlightSegments());
		Date pnrReleaseTimeZulu = ReservationUtil.calculateOnHoldReleaseTime(ibepayment, depDateTimeZulu, onHoldReleaseTimeDTO);
		Date currentDateTime = Calendar.getInstance().getTime();
		long OHDIntervalinSeconds = 0;
		if (pnrReleaseTimeZulu != null) {
			OHDIntervalinSeconds = (pnrReleaseTimeZulu.getTime() - currentDateTime.getTime()) / 1000;
		}

		return OHDIntervalinSeconds;

	}

	private void validatePaxPayment(BigDecimal reservationAmountDue, BigDecimal creditCardPayAmount, BigDecimal loyaltyAmount,
			BigDecimal lmsPaymentAmount, Map<EXTERNAL_CHARGES, ExternalChgDTO> externalCharges, BigDecimal discountTotal, BigDecimal voucherPaymentAmount)
			throws ModuleException {
		ExternalChgDTO jnExternalChargeDTO = externalCharges.get(EXTERNAL_CHARGES.JN_OTHER);
		ExternalChgDTO ccExternalChgDTO = externalCharges.get(EXTERNAL_CHARGES.CREDIT_CARD);

		BigDecimal externalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (jnExternalChargeDTO != null && jnExternalChargeDTO.getAmount() != null) {
			externalChargeAmount = AccelAeroCalculator.add(externalChargeAmount, jnExternalChargeDTO.getAmount());
		}
		if (ccExternalChgDTO != null && ccExternalChgDTO.getAmount() != null) {
			externalChargeAmount = AccelAeroCalculator.add(externalChargeAmount, ccExternalChgDTO.getAmount());
		}

		if (discountTotal == null) {
			discountTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		// When regulation is enabled reservationAmountDue comes with cc + jn
		// Need to subtract cc + jn to pass validation
		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null)) {
			reservationAmountDue = AccelAeroCalculator.subtract(reservationAmountDue, externalChargeAmount);
		}

		BigDecimal totalTicketPrice = AccelAeroCalculator.add(reservationAmountDue, externalChargeAmount);
		BigDecimal totalPaxPayment = AccelAeroCalculator.add(creditCardPayAmount, loyaltyAmount, lmsPaymentAmount,
				voucherPaymentAmount, discountTotal.negate());
		BigDecimal paymentDifference = AccelAeroCalculator.subtract(totalTicketPrice, totalPaxPayment);
		// Fix me
		// Need to improve payment difference
		if (paymentDifference.abs().compareTo(new BigDecimal(0.5)) > 0) {

			if (log.isErrorEnabled()) {
				log.error("### System can not process the payment.Payment diffrence : " + paymentDifference);
			}

			throw new ModuleException("msg.invalid.booking");
		}
	}

	private LCCClientReservation createOnHoldReservation(boolean isSupportOfflinePayment, PayCurrencyDTO payCurrencyDTO,
			BigDecimal totalCreditCardPaymentAmount, BigDecimal loyaltyCredit, BigDecimal lmsLoyaltyPayment, String ipgPNR,
			int onholdReleaseTime, BigDecimal voucherPaymentAmount) throws Exception {

		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		LCCClientReservation res = null;

		if (resInfo.isAllowOnhold() && !this.makePayment && !this.modifyAncillary && !this.modifySegment
				&& !this.addGroundSegment && !SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()
				&& putOnHoldBeforePayment(resInfo)) {
			if (!isSupportOfflinePayment) {
				res = putOnHoldBeforePayment(payCurrencyDTO, totalCreditCardPaymentAmount, loyaltyCredit, lmsLoyaltyPayment, voucherPaymentAmount);
			} else {
				res = putOnHoldForOfflinePayment(payCurrencyDTO, totalCreditCardPaymentAmount, loyaltyCredit, lmsLoyaltyPayment,
						onholdReleaseTime, voucherPaymentAmount);
			}
			this.createdOnhold = true;

		} else {
			// handle onhold failure
			if (log.isInfoEnabled()) {
				StringBuilder loginfo = new StringBuilder();
				loginfo.append("### IBE payment onhold method can not call :").append(ipgPNR);
				loginfo.append("| isCabinClass onhold enable : ").append(resInfo.isAllowOnhold());
				loginfo.append("| isMakepayment : ").append(makePayment);
				loginfo.append("| isModifyAncillary : ").append(modifyAncillary);
				loginfo.append("| isModifySegment : ").append(modifySegment);
				loginfo.append("| isAddGroundSegment : ").append(modifySegment);
				loginfo.append("| isPutOnHoldBeforePayment : ").append(
						SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment());
				loginfo.append("| SessionID: ").append(request.getRequestedSessionId());
				log.info(loginfo.toString());
			}
		}
		return res;
	}

	private Date setupOnholdOfflinePaymentTime(LCCClientReservation res) throws ModuleException {

		if (res != null) {
			return res.getZuluReleaseTimeStamp();
		} else {
			Date offlineTempOnhold = new Date(AppSysParamsUtil.getOnholdOfflinePaymentTimeInMillis());
			offlineTempOnhold = new Date(new Date().getTime() + offlineTempOnhold.getTime());
			return offlineTempOnhold;
		}
	}
	
	private void setPutOnHoldBeforePaymentSessionData(String pnr) {
		SessionUtil.getPutOnHoldBeforePaymentDTO(request).setPutOnHoldBeforePayment(true);
		SessionUtil.getPutOnHoldBeforePaymentDTO(request).setPutOnHoldBeforePaymentPnr(pnr);
		SessionUtil.getPutOnHoldBeforePaymentDTO(request).setCard(card);
	}
	
	// this method can be used to update IPGRequestDTO with cards extra details
	private void validatePGWCommonDetailsPanel(HttpServletRequest request, IPGRequestDTO ipgRequestDTO, PaymentGateWayInfoDTO paymentGateWay){
		if(pgw.getProviderCode().equalsIgnoreCase("TAP_GOSELL")){
			ipgRequestDTO.setContactMobileNumber(request.getParameter("hdnPGWPaymentMobileNumber"));
			ipgRequestDTO.setContactEmail(request.getParameter("hdnPGWPaymentEmail"));
			ipgRequestDTO.setHolderName(request.getParameter("hdnPGWPaymentCustomerName"));
		}
	}

	public PaymentGateWayInfoDTO getPgw() {
		return pgw;
	}

	public void setPgw(PaymentGateWayInfoDTO pgw) {
		this.pgw = pgw;
	}

	private static void releaseBlockedSeats(HttpServletRequest request) {
		try {
			SeatMapUtil.releaseBlockedSeats(request);
		} catch (Exception e1) {
			log.error("SEAT release failed", e1);
		}
	}

	private void revertLoyaltyRedemption() {
		String loyaltyMemberId = SessionUtil.getLoyaltyFFID(request);
		try {
			CustomerUtil.revertLoyaltyRedemption(request);
		} catch (ModuleException e) {
			log.error("Error in reverting redeeemed loyalty points for member " + loyaltyMemberId, e);
		}
	}

	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	private static String getServerErrorMessage(HttpServletRequest request, String key) {
		return I18NUtil.getMessage(key, SessionUtil.getLanguage(request));
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getJsonPaxWiseAnci() {
		return jsonPaxWiseAnci;
	}

	public void setJsonPaxWiseAnci(String jsonPaxWiseAnci) {
		this.jsonPaxWiseAnci = jsonPaxWiseAnci;
	}

	public void setJsonPaxInfo(String jsonPaxInfo) {
		this.jsonPaxInfo = jsonPaxInfo;
	}

	public String getJsonInsurance() {
		return jsonInsurance;
	}

	public void setJsonInsurance(String jsonInsurance) {
		this.jsonInsurance = jsonInsurance;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public void setModifingFlightInfo(String modifingFlightInfo) {
		this.modifingFlightInfo = modifingFlightInfo;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setOldFareID(int oldFareID) {
		this.oldFareID = oldFareID;
	}

	public void setOldFareType(int oldFareType) {
		this.oldFareType = oldFareType;
	}

	public void setOldFareAmount(String oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public void setHasInsurance(boolean hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	public void setRequestSessionIdentifier(String requestSessionIdentifier) {
		this.requestSessionIdentifier = requestSessionIdentifier;
	}

	public CreditCardTO getCard() {
		return card;
	}

	public void setCard(CreditCardTO card) {
		this.card = card;
	}

	public void setInsType(int insType) {
		this.insType = insType;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public boolean isMakePayment() {
		return makePayment;
	}

	public void setMakePayment(boolean makePayment) {
		this.makePayment = makePayment;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public String getBalanceQueryData() {
		return balanceQueryData;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	public String getNameChangePaxData() {
		return nameChangePaxData;
	}

	public void setNameChangePaxData(String nameChangePaxData) {
		this.nameChangePaxData = nameChangePaxData;
	}

	public boolean isNameChangeRequote() {
		return nameChangeRequote;
	}

	public void setNameChangeRequote(boolean nameChangeRequote) {
		this.nameChangeRequote = nameChangeRequote;
	}

}