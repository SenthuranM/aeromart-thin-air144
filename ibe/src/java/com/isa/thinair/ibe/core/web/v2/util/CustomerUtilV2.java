package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.constants.SocialCustomerType;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerQuestionaire;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.aircustomer.api.utils.LmsMemberGenderEnum;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditUsageDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.CustomerCreditDTO;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.api.dto.CustomerLoyaltyCreditDTO;
import com.isa.thinair.ibe.api.dto.CustomerLoyaltyCreditUsageDTO;
import com.isa.thinair.ibe.api.dto.FamilyMemberDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.dto.CustomerQuestionaireDTO;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.webplatform.core.commons.MapGenerator;
import com.isa.thinair.wsclient.api.util.LMSConstants;

/**
 * @author Pradeep Karunanayake
 * 
 */
public class CustomerUtilV2 {

	private static Log log = LogFactory.getLog(CustomerUtilV2.class);
	/**
	 * Get CustomerDTO
	 * 
	 * @param customer
	 * @return CustomerDTO
	 */
	public static CustomerDTO getCustomerDTO(Customer customer) {
		CustomerDTO customerDTO = null;
		SimpleDateFormat dateFormat = null;
		String dob = "";
		if (customer != null) {
			dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			customerDTO = new CustomerDTO();
			customerDTO.setAlternativeEmailId(StringUtil.nullConvertToString(customer.getAlternativeEmailId()));
			customerDTO.setCity(StringUtil.nullConvertToString(customer.getCity()));
			customerDTO.setAddressStreet(StringUtil.nullConvertToString(customer.getAddressStreet()));
			customerDTO.setAddressLine(StringUtil.nullConvertToString(customer.getAddressLine()));
			customerDTO.setZipCode(StringUtil.nullConvertToString(customer.getZipCode()));
			customerDTO.setCountryCode(StringUtil.nullConvertToString(customer.getCountryCode()));

			if (customer.getDateOfBirth() != null) {
				dob = dateFormat.format(customer.getDateOfBirth());
			}
			customerDTO.setDateOfBirth(dob);
			customerDTO.setEmailId(StringUtil.nullConvertToString(customer.getEmailId()));
			customerDTO.setFax(StringUtil.nullConvertToString(customer.getFax()));
			customerDTO.setFirstName(StringUtil.nullConvertToString(customer.getFirstName()));
			customerDTO.setGender(StringUtil.nullConvertToString(customer.getGender()));
			customerDTO.setLastName(StringUtil.nullConvertToString(customer.getLastName()));
			customerDTO.setMobile(StringUtil.nullConvertToString(customer.getMobile()));
			customerDTO.setNationalityCode(StringUtil.nullConvertToString(customer.getNationalityCode()));
			customerDTO.setOfficeTelephone(StringUtil.nullConvertToString(customer.getOfficeTelephone()));
			customerDTO.setSecretAnswer(StringUtil.nullConvertToString(customer.getSecretAnswer()));
			customerDTO.setSecretQuestion(StringUtil.nullConvertToString(customer.getSecretQuestion()));
			customerDTO.setStatus(StringUtil.nullConvertToString(customer.getStatus()));
			customerDTO.setTelephone(StringUtil.nullConvertToString(customer.getTelephone()));
			customerDTO.setTitle(StringUtil.nullConvertToString(customer.getTitle()));
			customerDTO.setPassword(StringUtil.nullConvertToString(customer.getPassword()));
			customerDTO.setVersion(StringUtil.nullConvertToString(customer.getVersion()));

			// Set Emergency contact details
			customerDTO.setEmgnTitle(StringUtil.nullConvertToString(customer.getEmgnTitle()));
			customerDTO.setEmgnFirstName(StringUtil.nullConvertToString(customer.getEmgnFirstName()));
			customerDTO.setEmgnLastName(StringUtil.nullConvertToString(customer.getEmgnLastName()));
			customerDTO.setEmgnPhoneNo(StringUtil.nullConvertToString(customer.getEmgnPhoneNo()));
			customerDTO.setEmgnEmail(StringUtil.nullConvertToString(customer.getEmgnEmail()));

			List<CustomerQuestionaireDTO> setCusQusDTO = new ArrayList<CustomerQuestionaireDTO>();
			CustomerQuestionaire customerQuestionaire;
			CustomerQuestionaireDTO customerQuestionaireDTO;
			Set set = customer.getCustomerQuestionaire();
			Iterator iterSet = set.iterator();
			while (iterSet.hasNext()) {
				customerQuestionaire = (CustomerQuestionaire) iterSet.next();
				customerQuestionaireDTO = new CustomerQuestionaireDTO();
				customerQuestionaireDTO.setQuestionKey(StringUtil.nullConvertToString(customerQuestionaire.getQuestionKey()));
				customerQuestionaireDTO
						.setQuestionAnswer(StringUtil.nullConvertToString(customerQuestionaire.getQuestionAnswer()));
				setCusQusDTO.add(customerQuestionaireDTO);
			}
			customerDTO.setCustomerQuestionaireDTO(setCusQusDTO);

		}
		return customerDTO;
	}

	/**
	 * Get Model Customer
	 * 
	 * @param customerDTO
	 * @return
	 * @throws ModuleException
	 */
	public static Customer getModelCustomer(CustomerDTO customerDTO) throws ModuleException {
		Customer customer = null;
		if (customerDTO.getCustomerID() != 0) {
			customer = CustomerUtil.getCustomer(customerDTO.getCustomerID());
		} else {
			customer = new Customer();
			customer.setFirstName(customerDTO.getFirstName());
			customer.setLastName(customerDTO.getLastName());
		}
		customer.setEmailId(customerDTO.getEmailId());
		customer.setTitle(customerDTO.getTitle());
		customer.setAddressStreet(customerDTO.getAddressStreet());
		customer.setAddressLine(customerDTO.getAddressLine());
		customer.setZipCode(customerDTO.getZipCode());
		customer.setCountryCode(customerDTO.getCountryCode());
		customer.setGender(customerDTO.getGender());
		customer.setNationalityCode("".equals(customerDTO.getNationalityCode()) ? null : (new Integer(customerDTO
				.getNationalityCode())));
		customer.setCity(customerDTO.getCity());
		String birthDay = customerDTO.getDateOfBirth();
		SimpleDateFormat dateFormat = null;

		if (birthDay != null && !birthDay.equals("")) {

			if (birthDay.indexOf(' ') != -1) {
				birthDay = birthDay.substring(0, birthDay.indexOf(' '));
			}

			if (birthDay.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			}
			if (birthDay.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			}

			Date dateOfBirth = null;
			try {
				dateOfBirth = dateFormat.parse(birthDay);
			} catch (Exception ex) {
			}
			customer.setDateOfBirth(dateOfBirth);
		}

		customer.setTelephone(customerDTO.getTelephone());
		customer.setFax(customerDTO.getFax());
		customer.setMobile(customerDTO.getMobile());
		customer.setOfficeTelephone(customerDTO.getOfficeTelephone());
		customer.setPassword(customerDTO.getPassword());
		customer.setAlternativeEmailId(customerDTO.getAlternativeEmailId());
		customer.setSecretQuestion(customerDTO.getSecretQuestion());
		customer.setSecretAnswer(customerDTO.getSecretAnswer());
		customer.setIsLmsMember('N');
		
		if (customerDTO.getVersion() != null && customerDTO.getVersion().trim().equals("")) {
			customer.setVersion(Long.parseLong(customerDTO.getVersion()));
		}

		Set<CustomerQuestionaire> setQuestions = customer.getCustomerQuestionaire();
		List<CustomerQuestionaireDTO> customerQuestionaireDTOs = customerDTO.getCustomerQuestionaireDTO();
		// Update Customer
		if (setQuestions != null) {
			Iterator iterSet = setQuestions.iterator();
			CustomerQuestionaire customerQuestionaire;
			while (iterSet.hasNext()) {
				customerQuestionaire = (CustomerQuestionaire) iterSet.next();
				if (customerQuestionaireDTOs != null) {
					for (CustomerQuestionaireDTO customerQuestionaireDTO : customerQuestionaireDTOs) {
						if (customerQuestionaireDTO != null
								&& Integer.parseInt(customerQuestionaireDTO.getQuestionKey()) == customerQuestionaire
										.getQuestionKey()) {
							if (customerQuestionaireDTO.getQuestionAnswer() != null) {
								customerQuestionaire.setQuestionAnswer(customerQuestionaireDTO.getQuestionAnswer().trim());
								setQuestions.add(customerQuestionaire);
							}
							break;
						}
					}
				}
			}
		} else { // New Customer
			setQuestions = new HashSet<CustomerQuestionaire>();
			CustomerQuestionaire customerQuestionaire;
			if (customerQuestionaireDTOs != null) {
				for (CustomerQuestionaireDTO customerQuestionaireDTO : customerQuestionaireDTOs) {
					customerQuestionaire = new CustomerQuestionaire();
					if (customerQuestionaireDTO != null) {
						customerQuestionaire.setQuestionAnswer(customerQuestionaireDTO.getQuestionAnswer().trim());
						customerQuestionaire.setQuestionKey(Integer.parseInt(customerQuestionaireDTO.getQuestionKey()));
						setQuestions.add(customerQuestionaire);
					}
				}
			}
		}
		customer.setCustomerQuestionaire(setQuestions);

		// set emergency contact details

		customer.setEmgnTitle(customerDTO.getEmgnTitle());
		customer.setEmgnFirstName(customerDTO.getEmgnFirstName());
		customer.setEmgnLastName(customerDTO.getEmgnLastName());
		customer.setEmgnPhoneNo(customerDTO.getEmgnPhoneNo());
		customer.setEmgnEmail(customerDTO.getEmgnEmail());

		return customer;
	}

	/**
	 * Get Customer Name Details
	 * 
	 */
	public static CustomerDTO getCustomerNameDetail(Customer customer) {
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setTitle(StringUtil.nullConvertToString(customer.getTitle()));
		customerDTO.setFirstName(StringUtil.nullConvertToString(customer.getFirstName()));
		customerDTO.setLastName(StringUtil.nullConvertToString(customer.getLastName()));
		customerDTO.setEmailId(StringUtil.nullConvertToString(customer.getEmailId()));

		Set<SocialCustomer> linkedSocialProfiles = customer.getSocialCustomer();
		String socialSiteCustId = null;
		int socialCustomerTypeId = -1;
		
		if (linkedSocialProfiles != null && !linkedSocialProfiles.isEmpty()) {
			Iterator<SocialCustomer> it = linkedSocialProfiles.iterator();
			while (it.hasNext()) {
				SocialCustomer socialProfile = it.next();

				if (socialProfile.getSocialCustomerTypeId().equals(SocialCustomerType.FACEBOOK.getId())) {
					socialSiteCustId = socialProfile.getSocialSiteCustId();
					socialCustomerTypeId = socialProfile.getSocialCustomerTypeId().intValue();
					break;

				}

			}

		}
		customerDTO.setSocialSiteCustId(socialSiteCustId != null ? socialSiteCustId : "-1");
		customerDTO.setSocialCustomerTypeId(socialCustomerTypeId);
		return customerDTO;
	}

	/**
	 * Create LoyaltyCustomerProfile
	 * 
	 * @param customer
	 * @param accountNo
	 * @return
	 */
	public static LoyaltyCustomerProfile getLoyaltyCustomerProfile(Customer customer, String accountNo) {
		LoyaltyCustomerProfile loyaltyCustomerProfile = new LoyaltyCustomerProfile();

		loyaltyCustomerProfile.setCity(customer.getCity());
		loyaltyCustomerProfile.setCountryCode(customer.getCountryCode());
		loyaltyCustomerProfile.setDateOfBirth(customer.getDateOfBirth());
		loyaltyCustomerProfile.setLoyaltyAccountNo(accountNo);
		loyaltyCustomerProfile.setMobile(customer.getMobile());
		loyaltyCustomerProfile.setNationalityCode(customer.getNationalityCode());
		loyaltyCustomerProfile.setEmail(customer.getEmailId());

		return loyaltyCustomerProfile;

	}

	/**
	 * Set Loyalty Account Status
	 */

	public static CustomerDTO setLoyaltyAccountStatus(CustomerDTO customer, HttpServletRequest request) {
		String loyaltyAccountNo = SessionUtil.getLoyaltyAccountNo(request);
		if (loyaltyAccountNo != null)
			customer.setLoyaltyAccountExist(true);
		else
			customer.setLoyaltyAccountExist(false);

		return customer;
	}

	public static Collection<CustomerCreditDTO> getCustomerCreditFromAllCarriers(int intCustomerID, BasicTrackInfo trackInfo)
			throws ModuleException {
		List<String> pnrList = getPnrList(intCustomerID);

		Collection<PaxCreditDTO> colPaxCredit = ModuleServiceLocator.getReservationQueryBD().getPaxCreditForPnrs(pnrList);

		Collection<PaxCreditDTO> colOtherCarriersPaxCredit = null;
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			try {
				colOtherCarriersPaxCredit = ModuleServiceLocator.getLCCPaxCreditBD().getPaxCreditAccrossCarriers(pnrList,
						trackInfo);
				fillOriginatorPnr(colOtherCarriersPaxCredit);
			} catch (ModuleException e) {
				// WS access Issue. Skip other carrier pax credit retrieval
			}
		}

		if (colOtherCarriersPaxCredit != null && !colOtherCarriersPaxCredit.isEmpty()) {
			colPaxCredit.addAll(colOtherCarriersPaxCredit);
		}

		Collection<CustomerCreditDTO> colCustCreditDto = mergeCustomerCreditFromCarriers(colPaxCredit);
		return SortUtil.sortCustomerCreditByExpiryDate(colCustCreditDto);
	}

	private static void fillOriginatorPnr(Collection<PaxCreditDTO> colOtherCarriersPaxCredit) {
		if (colOtherCarriersPaxCredit != null && !colOtherCarriersPaxCredit.isEmpty()) {
			for (PaxCreditDTO paxCreditDTO : colOtherCarriersPaxCredit) {
				paxCreditDTO.setOriginatorPnr(paxCreditDTO.getPnr());
			}
		}
	}

	private static Collection<CustomerCreditDTO> mergeCustomerCreditFromCarriers(Collection<PaxCreditDTO> coll) {
		Collection<CustomerCreditDTO> customerCreditDTOs = new ArrayList<CustomerCreditDTO>();
		Map<String, CustomerCreditDTO> custCreditMap = new HashMap<String, CustomerCreditDTO>();
		if (coll != null) {
			for (PaxCreditDTO paxCreditDTO : coll) {
				CustomerCreditDTO customerCreditDTO;
				String key = paxCreditDTO.getPnr() + "#" + paxCreditDTO.getPaxSequence();
				SimpleDateFormat smpdtFormat = new SimpleDateFormat("dd MMM yyyy");
				DecimalFormat decimalFormat = new DecimalFormat("#########.00");

				if (custCreditMap.containsKey(key)) {
					customerCreditDTO = custCreditMap.get(key);
					BigDecimal bal = AccelAeroCalculator.add(new BigDecimal(customerCreditDTO.getBalance()),
							paxCreditDTO.getBalance());
					customerCreditDTO.setBalance(decimalFormat.format(bal));
				} else {
					customerCreditDTO = new CustomerCreditDTO();
					customerCreditDTO.setPnr(paxCreditDTO.getPnr());
					customerCreditDTO.setGroupPnr(paxCreditDTO.getOriginatorPnr());
					customerCreditDTO.setFullName(StringUtil.nullConvertToString(paxCreditDTO.getTitle()) + " "
							+ StringUtil.nullConvertToString(paxCreditDTO.getFirstName()) + " "
							+ StringUtil.nullConvertToString(paxCreditDTO.getLastName()));

					BigDecimal bal = new BigDecimal((paxCreditDTO.getBalance() != null) ? paxCreditDTO.getBalance().toString()
							: "0");

					customerCreditDTO.setBalance(decimalFormat.format(bal));
					customerCreditDTO.setDateOfExpiry(smpdtFormat.format(paxCreditDTO.getDateOfExpiry()));
				}

				custCreditMap.put(key, customerCreditDTO);
			}
		}

		if (!custCreditMap.isEmpty()) {
			for (Iterator mapIt = custCreditMap.entrySet().iterator(); mapIt.hasNext();) {
				Map.Entry<String, CustomerCreditDTO> entry = (Map.Entry<String, CustomerCreditDTO>) mapIt.next();
				customerCreditDTOs.add(entry.getValue());
			}
		}

		return customerCreditDTOs;
	}

	public static String calculateCustomerTotalCredit(Collection<CustomerCreditDTO> col) {
		BigDecimal dblTotal = null;

		if (col != null) {
			dblTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (CustomerCreditDTO custCreditDTO : col) {

				BigDecimal val = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (custCreditDTO.getBalance() != null && !custCreditDTO.getBalance().equals("")) {
					val = new BigDecimal(custCreditDTO.getBalance());
				}

				dblTotal = AccelAeroCalculator.add(dblTotal, val);

			}
		}

		return dblTotal == null ? "" : dblTotal.toString();

	}

	/**
	 * Get Customer Credit Total
	 * 
	 */
	public static String getCustomerTotalCredit(Collection<PaxCreditDTO> coll) {
		BigDecimal dblTotal = null;

		if (coll != null) {
			dblTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
			PaxCreditDTO paxCreditDTO;
			Iterator<PaxCreditDTO> iterator = coll.iterator();
			while (iterator.hasNext()) {
				paxCreditDTO = (PaxCreditDTO) iterator.next();
				dblTotal = AccelAeroCalculator.add(dblTotal, paxCreditDTO.getBalance());

			}
		}

		return dblTotal == null ? "" : dblTotal.toString();
	}

	/**
	 * Get Customer Reservation list from today
	 * 
	 * @param customerID
	 * @param dateNow
	 * @param trackInfoDTO TODO
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ReservationListTO> getAfterReservationsList(int customerID, Date dateNow, TrackInfoDTO trackInfoDTO) throws ModuleException {
		Collection<ReservationDTO> reservationDTOs = ModuleServiceLocator.getReservationQueryBD().getAfterReservations(
				customerID, dateNow, null, trackInfoDTO);
		return composeReservationsList(reservationDTOs, false);
	}

	/**
	 * Get All Customer Reservations
	 * 
	 * @param customerID
	 * @param dateNow
	 * @param trackInfoDTO TODO
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ReservationListTO> getEarlyReservationsList(int customerID, Date dateNow, TrackInfoDTO trackInfoDTO) throws ModuleException {
		Collection<ReservationDTO> reservationDTOs = ModuleServiceLocator.getReservationQueryBD().getEarlyReservations(
				customerID, dateNow, null, trackInfoDTO);
		return composeReservationsList(reservationDTOs, false);
	}

	private static List<String> getPnrList(int customerID) throws ModuleException {
		return ModuleServiceLocator.getReservationQueryBD().getPnrList(customerID);
	}

	/**
	 * 
	 * @param colReservationDTO
	 * @param includeExchangedSegment
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<ReservationListTO> composeReservationsList(Collection<ReservationDTO> colReservationDTO,
			boolean includeExchangedSegment) throws ModuleException {
		Collection<ReservationListTO> colReservationListTO = new ArrayList<ReservationListTO>();
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat smpdtValDate = new SimpleDateFormat("yyMMddHHmm");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");

		if (colReservationDTO != null && colReservationDTO.size() > 0) {
			Iterator<ReservationDTO> iterReservation = colReservationDTO.iterator();

			// Reservation Information
			while (iterReservation.hasNext()) {
				ReservationDTO reservationDTO = iterReservation.next();
				ReservationContactInfo reservationContactInfo = reservationDTO.getReservationContactInfo();

				ReservationListTO reservationListTO = new ReservationListTO();
				reservationListTO.setPnrNo(BeanUtils.nullHandler(reservationDTO.getPnr()));
				reservationListTO.setPaxName(BeanUtils.nullHandler(reservationContactInfo.getFirstName()) + ", "
						+ BeanUtils.nullHandler(reservationContactInfo.getLastName()));
				reservationListTO.setPnrStatus(reservationDTO.getStatus());
				reservationListTO.setOriginatorPnr(BeanUtils.nullHandler(reservationDTO.getOriginatorPnr()));

				List<FlightInfoTO> lstFlightInfo = new ArrayList<FlightInfoTO>();

				String strPaxCount = reservationDTO.getAdultCount() + "/" + reservationDTO.getChildCount() + "/"
						+ reservationDTO.getInfantCount();

				for (Iterator iterator = reservationDTO.getSegments().iterator(); iterator.hasNext();) {
					ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) iterator.next();

					if (!includeExchangedSegment
							&& ReservationSegmentSubStatus.EXCHANGED.equals(reservationSegmentDTO.getSubStatus())) {
						continue;
					}

					FlightInfoTO flightInfoTO = new FlightInfoTO();

					flightInfoTO.setDepartureDate(smpdtDate.format(reservationSegmentDTO.getDepartureDate()));
					flightInfoTO.setDepartureTime(smpdtTime.format(reservationSegmentDTO.getDepartureDate()));
                    flightInfoTO.setDepartureDateValue(smpdtValDate.format(reservationSegmentDTO.getDepartureDate()));
                    flightInfoTO.setArrivalDateValue(smpdtValDate.format(reservationSegmentDTO.getArrivalDate()));
					flightInfoTO.setDepartureDateLong(reservationSegmentDTO.getZuluDepartureDate().getTime());
					flightInfoTO.setArrivalDate(smpdtDate.format(reservationSegmentDTO.getArrivalDate()));
					flightInfoTO.setArrivalTime(smpdtTime.format(reservationSegmentDTO.getArrivalDate()));
					flightInfoTO.setFlightNo(reservationSegmentDTO.getFlightNo());
					flightInfoTO.setPaxCount(strPaxCount);

					boolean isOpenReturn = reservationSegmentDTO.isOpenReturnSegment();
					flightInfoTO.setOpenReturnSegment(isOpenReturn);

					String segmentStatus = reservationSegmentDTO.getStatus();
					if (isOpenReturn && !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segmentStatus)) {
						segmentStatus = "";
					}

					flightInfoTO.setStatus(segmentStatus);
					flightInfoTO.setSubStatus(reservationSegmentDTO.getSubStatus());

					String segmentCode = BeanUtils.nullHandler(reservationSegmentDTO.getSegmentCode());

					if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
						segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
					}

					flightInfoTO.setOrignNDest(getONDDetails(segmentCode));

					lstFlightInfo.add(flightInfoTO);
				}

				if (reservationDTO.getColExternalSegments() != null && reservationDTO.getColExternalSegments().size() > 0) {
					for (ReservationExternalSegmentTO reservationExternalSegmentTO : reservationDTO.getColExternalSegments()) {
						String segmentCode = reservationExternalSegmentTO.getSegmentCode();

						if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
							segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
						}

						FlightInfoTO flightInfoTO = new FlightInfoTO();
						flightInfoTO.setFlightNo(reservationExternalSegmentTO.getFlightNo());
						flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(reservationExternalSegmentTO.getFlightNo()));
						flightInfoTO.setDepartureDate(smpdtDate.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureTime(smpdtTime.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureDateLong(reservationExternalSegmentTO.getDepartureDate().getTime());
						flightInfoTO.setArrivalDate(smpdtDate.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setArrivalTime(smpdtTime.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setOrignNDest(getONDDetails(segmentCode));
						flightInfoTO.setPaxCount(strPaxCount);
						flightInfoTO.setStatus(reservationExternalSegmentTO.getStatus());
						lstFlightInfo.add(flightInfoTO);
					}
				}

				Collections.sort(lstFlightInfo);
				SortUtil.sortByReservationStatus(lstFlightInfo);
				reservationListTO.setFlightInfo(lstFlightInfo);
				colReservationListTO.add(reservationListTO);
			}
		}

		return colReservationListTO;
	}

	public static Collection<CustomerLoyaltyCreditDTO> getCustomerMashreqLoyaltyCredit(
			Collection<LoyaltyCreditDTO> resloyaltyCreditDTO) {

		Collection<CustomerLoyaltyCreditDTO> customerLoyaltyCreditDTOs = new ArrayList<CustomerLoyaltyCreditDTO>();

		if (resloyaltyCreditDTO != null) {
			LoyaltyCreditDTO resLoyaltyCredit = null;
			CustomerLoyaltyCreditDTO customerLoyaltyCreditDTO = null;
			Iterator iterator = resloyaltyCreditDTO.iterator();

			SimpleDateFormat smpdtFormat = new SimpleDateFormat("dd MMM yyyy");
			DecimalFormat decimalFormat = new DecimalFormat("#########.00");

			while (iterator.hasNext()) {
				resLoyaltyCredit = (LoyaltyCreditDTO) iterator.next();
				customerLoyaltyCreditDTO = new CustomerLoyaltyCreditDTO();
				if (resLoyaltyCredit.getCredit() != null) {
					if (resLoyaltyCredit.getCredit().compareTo(new BigDecimal(0.01)) == 1) {
						customerLoyaltyCreditDTO.setCredit(decimalFormat.format(resLoyaltyCredit.getCredit()));
						customerLoyaltyCreditDTO.setDateOfExpiry(smpdtFormat.format(resLoyaltyCredit.getDateOfExpiry()));
						customerLoyaltyCreditDTO.setDateOfEarn(smpdtFormat.format(resLoyaltyCredit.getDateOfEarn()));
						customerLoyaltyCreditDTOs.add(customerLoyaltyCreditDTO);
					}

				}
			}
		}
		return customerLoyaltyCreditDTOs;
	}

	public static Collection<CustomerLoyaltyCreditUsageDTO> getCustomerMashreqCreditUsage(
			Collection<LoyaltyCreditUsageDTO> resloyaltyCreditUsageDTO) {

		Collection<CustomerLoyaltyCreditUsageDTO> customerLoyaltyCreditUsageDTOs = new ArrayList<CustomerLoyaltyCreditUsageDTO>();

		if (resloyaltyCreditUsageDTO != null) {
			LoyaltyCreditUsageDTO resLoyaltyCreditUsage = null;
			CustomerLoyaltyCreditUsageDTO customerLoyaltyCreditUsageDTO = null;
			Iterator iterator = resloyaltyCreditUsageDTO.iterator();

			SimpleDateFormat smpdtFormat = new SimpleDateFormat("dd MMM yyyy");
			DecimalFormat decimalFormat = new DecimalFormat("#########.00");

			while (iterator.hasNext()) {
				resLoyaltyCreditUsage = (LoyaltyCreditUsageDTO) iterator.next();
				customerLoyaltyCreditUsageDTO = new CustomerLoyaltyCreditUsageDTO();
				customerLoyaltyCreditUsageDTO.setPnr(StringUtil.nullConvertToString(resLoyaltyCreditUsage.getPnr()) + "-"
						+ StringUtil.nullConvertToString(resLoyaltyCreditUsage.getOriginatorPnr()));
				customerLoyaltyCreditUsageDTO.setPaidAmount(decimalFormat.format(resLoyaltyCreditUsage.getPaidAmount()));
				customerLoyaltyCreditUsageDTO.setPaidDate(smpdtFormat.format(resLoyaltyCreditUsage.getPaidDate()));
				customerLoyaltyCreditUsageDTOs.add(customerLoyaltyCreditUsageDTO);
			}
		}
		return customerLoyaltyCreditUsageDTOs;
	}

	private static String getONDDetails(String strOND) {
		Map mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strOND.split("/");
		strSegements = "";
		for (int i = 0; i < strArrSegment.length; i++) {
			if (AppSysParamsUtil.isHideStopOverEnabled() && strArrSegment.length > 2 && i > 0 && i < (strArrSegment.length - 1))
				continue;
			if (strSegements != "") {
				strSegements += " / ";
			}
			if (mapAirports.get(strArrSegment[i]) != null) {
				strSegements += mapAirports.get(strArrSegment[i]);
			} else {
				strSegements += strArrSegment[i];
			}
		}
		return strSegements;
	}

	public static Customer createCustomerProfile(ContactInfoDTO contactInfo, Customer oldCustObj) {
		Customer customer = new Customer();
		customer.setCustomerId(oldCustObj.getCustomerId());
		customer.setPassword(oldCustObj.getPassword());
		customer.setGender(oldCustObj.getGender());
		customer.setAlternativeEmailId(oldCustObj.getAlternativeEmailId());
		customer.setOfficeTelephone(oldCustObj.getOfficeTelephone());
		customer.setDateOfBirth(oldCustObj.getDateOfBirth());
		customer.setStatus(oldCustObj.getStatus());
		customer.setSecretQuestion(oldCustObj.getSecretQuestion());
		customer.setSecretAnswer(oldCustObj.getSecretAnswer());
		customer.setRegistration_date(oldCustObj.getRegistration_date());
		customer.setConfirmation_date(customer.getConfirmation_date());
		customer.setEmailId(contactInfo.getEmailAddress());

		if (contactInfo.getTitle() != null && (!contactInfo.getTitle().equals(""))) {
			customer.setTitle(contactInfo.getTitle());
		} else {
			customer.setTitle(oldCustObj.getTitle());
		}

		customer.setFirstName(contactInfo.getFirstName());
		customer.setLastName(contactInfo.getLastName());
		customer.setTelephone(contactInfo.getlCountry() + "-" + contactInfo.getlArea() + "-" + contactInfo.getlNumber());
		customer.setMobile(contactInfo.getmCountry() + "-" + contactInfo.getmArea() + "-" + contactInfo.getmNumber());
		customer.setFax(contactInfo.getfCountry() + "-" + contactInfo.getfArea() + "-" + contactInfo.getfNumber());
		customer.setAddressStreet(contactInfo.getAddresStreet());
		customer.setAddressLine(contactInfo.getAddresline());
		customer.setZipCode(contactInfo.getZipCode());

		if (contactInfo.getCountry() != null && (!contactInfo.getCountry().equals(""))) {
			customer.setCountryCode(contactInfo.getCountry());
		} else {
			customer.setCountryCode(oldCustObj.getCountryCode());
		}

		if (contactInfo.getNationality() != null && (!contactInfo.getNationality().equals(""))) {
			customer.setNationalityCode(Integer.parseInt(contactInfo.getNationality()));
		} else {
			customer.setNationalityCode(oldCustObj.getNationalityCode());
		}

		customer.setCity(contactInfo.getCity());
		customer.setEmgnTitle(contactInfo.getEmgnTitle());
		customer.setEmgnFirstName(contactInfo.getEmgnFirstName());
		customer.setEmgnLastName(contactInfo.getEmgnLastName());
		customer.setEmgnPhoneNo(contactInfo.getEmgnLCountry() + "-" + contactInfo.getEmgnLArea() + "-"
				+ contactInfo.getEmgnLNumber());
		customer.setEmgnEmail(contactInfo.getEmgnEmail());
		customer.setVersion(oldCustObj.getVersion());
		return customer;
	}
	
	public static LmsMember getModelLmsMember(LmsMemberDTO lmsDetails, int customerId){
		
		LmsMember lmsMember = new LmsMember();
		
	    lmsMember.setFfid(lmsDetails.getEmailId());		
		lmsMember.setFirstName(lmsDetails.getFirstName());
		lmsMember.setMiddleName("".equals(lmsDetails.getMiddleName()) ? null : lmsDetails.getMiddleName());
	    lmsMember.setLastName(lmsDetails.getLastName());
	    lmsMember.setCustomerId(customerId);
	    String birthDay = lmsDetails.getDateOfBirth();
		SimpleDateFormat dateFormat = null;

		if (!birthDay.equals("")) {

			if (birthDay.indexOf(' ') != -1) {
				birthDay = birthDay.substring(0, birthDay.indexOf(' '));
			}

			if (birthDay.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			}
			if (birthDay.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			}

			Date dateOfBirth = null;
			try {
				dateOfBirth = dateFormat.parse(birthDay);
			} catch (Exception ex) {
			}
			lmsMember.setDateOfBirth(dateOfBirth);
		}
	    
		lmsMember.setMobileNumber(LmsCommonUtil.getValidPhoneNumber(lmsDetails.getMobileNumber()));
		lmsMember.setPhoneNumber("".equals(lmsDetails.getPhoneNumber()) ? null : LmsCommonUtil.getValidPhoneNumber(lmsDetails
				.getPhoneNumber()));
		lmsMember.setRegDate(new Date());
	    lmsMember.setEmailConfSent('N');
	    lmsMember.setEmailConfirmed('N');
	    
		Map<String, String> locationRefMap = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyLocationExtReferences();
		Map<String, String> enrollingChannelMap = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyEnrollingChannelExtReferences();

		String enrollmentLocExtRef = lmsDetails.getAppCode();
		
		if (enrollmentLocExtRef.equals("APP_IBE")){
			lmsMember.setEnrollmentLocExtRef(locationRefMap.get(AppIndicatorEnum.APP_IBE.toString()));
			lmsMember.setEnrollingChannelExtRef(enrollingChannelMap.get(AppIndicatorEnum.APP_IBE.toString()));
			
		}
		else if (enrollmentLocExtRef.equals("APP_XBE")){
			lmsMember.setEnrollmentLocExtRef(locationRefMap.get(AppIndicatorEnum.APP_XBE.toString()));
			lmsMember.setEnrollingChannelExtRef(enrollingChannelMap.get(AppIndicatorEnum.APP_XBE.toString()));
		}
		lmsMember.setEnrollingChannelIntRef(enrollingChannelMap.get(AppIndicatorEnum.APP_IBE.toString()));
	    lmsMember.setPassportNum("".equals(lmsDetails.getPassportNum()) ? null :lmsDetails.getPassportNum());
	    lmsMember.setLanguage("".equals(lmsDetails.getLanguage()) ? null :lmsDetails.getLanguage().toLowerCase());
	    lmsMember.setNationalityCode("".equals(lmsDetails.getNationalityCode()) ? null : lmsDetails.getNationalityCode());
	    lmsMember.setResidencyCode("".equals(lmsDetails.getResidencyCode()) ? null :lmsDetails.getResidencyCode());	    
	    lmsMember.setHeadOFEmailId("".equals(lmsDetails.getHeadOFEmailId()) ? null : lmsDetails.getHeadOFEmailId());
	    lmsMember.setRefferedEmail("".equals(lmsDetails.getRefferedEmail()) ? null : lmsDetails.getRefferedEmail());
	    lmsMember.setEnrollingCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		
		String gender = lmsDetails.getGenderCode();
		
		if(gender.equalsIgnoreCase("MR")){
			lmsMember.setGenderTypeId(LmsMemberGenderEnum.MALE.getCode());
		} else if (gender.equalsIgnoreCase("MS")) {
			lmsMember.setGenderTypeId(LmsMemberGenderEnum.FEMALE.getCode());
		} else {
			lmsMember.setGenderTypeId(LmsMemberGenderEnum.UNDEFINED.getCode());
		}					
		
		String password = UUID.randomUUID().toString().replace("-", "").substring(0,20);
		
		lmsMember.setPassword(password.toUpperCase());
		
		return lmsMember;
	}
	
	public static LmsMemberDTO getLmsMemberDTO(LmsMember lmsMember){
		
		LmsMemberDTO lmsDetails = new LmsMemberDTO();
		
	    lmsDetails.setEmailId(lmsMember.getFfid());		
		lmsDetails.setFirstName(lmsMember.getFirstName());
		lmsDetails.setMiddleName(StringUtil.nullConvertToString(lmsMember.getMiddleName()));
	    lmsDetails.setLastName(lmsMember.getLastName());
	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String birthDay = dateFormat.format(lmsMember.getDateOfBirth());
		lmsDetails.setDateOfBirth(birthDay);
	    	    
		lmsDetails.setPassportNum(StringUtil.nullConvertToString(lmsMember.getPassportNum()));
		lmsDetails.setLanguage(StringUtil.nullConvertToString(lmsMember.getLanguage()));
		lmsDetails.setNationalityCode(StringUtil.nullConvertToString(lmsMember.getNationalityCode()));
		lmsDetails.setResidencyCode(StringUtil.nullConvertToString(lmsMember.getResidencyCode()));
		lmsDetails.setHeadOFEmailId(StringUtil.nullConvertToString(lmsMember.getHeadOFEmailId()));
		lmsDetails.setRefferedEmail(StringUtil.nullConvertToString(lmsMember.getRefferedEmail()));
	    lmsDetails.setEmailStatus(lmsMember.getEmailConfirmed()=='Y' ? "Y" : "N");
	    if(lmsMember.getEmailConfirmed()=='Y'){
	    	LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
	    	try {
				LoyaltyPointDetailsDTO existingMember = lmsManagementBD.getLoyaltyMemberPointBalances(lmsMember.getFfid(), lmsMember.getMemberExternalId());
				lmsDetails.setAvailablePoints(String.valueOf(existingMember.getMemberPointsAvailable()));
			} catch (ModuleException e) {
				log.error("Error in getLmsMemberDTO ", e);
			}
	    }
		return lmsDetails;
	}

	public static boolean isExistingLMSNameMatches(LmsMemberDTO lmsDetails) throws ModuleException {
		LoyaltyMemberCoreDTO existingMember = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyMemberCoreDetails(
				lmsDetails.getEmailId());

		if (existingMember != null
				&& existingMember.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK
				&& (!existingMember.getMemberFirstName().equalsIgnoreCase(lmsDetails.getFirstName()) || !existingMember
						.getMemberLastName().equalsIgnoreCase(lmsDetails.getLastName()))) {
			return false;
		}

		return true;
	}
	
	public static FamilyMember getFamilyMemberModel(FamilyMemberDTO familyMemberDTO){
		
		FamilyMember familyMember = new FamilyMember();
		
		familyMember.setFamilyMemberId(familyMemberDTO.getFamilyMemberId());		
		familyMember.setFirstName(familyMemberDTO.getFirstName().toLowerCase());
		familyMember.setLastName(familyMemberDTO.getLastName().toLowerCase());
		familyMember.setNationalityCode(familyMemberDTO.getNationalityCode());
		familyMember.setRelationshipId(familyMemberDTO.getRelationshipId());
		
		String birthDay = familyMemberDTO.getDateOfBirth();

		if (!birthDay.equals("")) {

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			Date dateOfBirth = null;
			try {
				dateOfBirth = dateFormat.parse(birthDay);
			} catch (Exception ex) {
				log.debug("Date parse failed in FamilyMember model to FamilyMemberDTO "  + ex);
			}
			familyMember.setDateOfBirth(dateOfBirth);
			
		}

		return familyMember;
		
	}
	
	public static FamilyMemberDTO getFamilyMemberDTO(FamilyMember familyMember){
		
		FamilyMemberDTO familyMemberDTO = new FamilyMemberDTO();
		String firstName = familyMember.getFirstName();
		String lastName = familyMember.getLastName();
		familyMemberDTO.setFamilyMemberId(familyMember.getFamilyMemberId());		
		familyMemberDTO.setFirstName(firstName.substring(0, 1).toUpperCase() + firstName.substring(1));
		familyMemberDTO.setLastName(lastName.substring(0, 1).toUpperCase() + lastName.substring(1));
		familyMemberDTO.setNationalityCode(familyMember.getNationalityCode());
		familyMemberDTO.setRelationshipId(familyMember.getRelationshipId());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		familyMemberDTO.setDateOfBirth(dateFormat.format(familyMember.getDateOfBirth()));

		return familyMemberDTO;
		
	}
	
}
