package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.opensymphony.xwork2.ActionSupport;

@Result(name = ActionSupport.SUCCESS, type = JSONResult.class, value = "")
public class PaymentGatewayWiseChargeAction extends IBEBaseAction {
	
	public static final String CHARGE_BY_VALUE = "V";
	public static final String CHARGE_BY_PERCENTAGE = "P";
	
	private String paymentGatewayId;
	private String totalFare;
	private String paxWiseAnci;
	private boolean makePayment = false;
	private boolean addGroundSegment;
	private String selectedFlightJson = null;
	private String insurance;
	private boolean modifySegment = false;
	private boolean modifyAncillary = false;
	private String oldAllSegments;
	private String paymentGatewayWiseCharge = null;
	private String currency;
	private boolean pgwChargeApplied = false;
	private String jnTax;
	
	public String execute() throws Exception {
		String forward = ActionSupport.SUCCESS;
		
		ExternalChgDTO ccChgDTO_Default = null;
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalCharges = resInfo.getSelectedExternalCharges();
		
		BigDecimal ccFee = BigDecimal.ZERO;		
		Collection<ReservationPaxTO> paxList = null;
		List<LCCInsuranceQuotationDTO> insuranceQuotations = null;
		SelectedFltSegBuilder totalFltSegBuilder = null;
		LCCInsuredJourneyDTO insJrnyDto = null;
		List<FlightSegmentTO> flightSegmentTOs = null;		
		
		Integer operationalType = ((modifySegment || modifyAncillary || addGroundSegment)
				? ChargeRateOperationType.MODIFY_ONLY
				: ChargeRateOperationType.MAKE_ONLY);
		
		// take the normal cc charge and override it if pgw charge is defined
		SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());
		flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
		boolean isReturn = fltSegBuilder.isReturn();
		boolean isRakInsurance = AppSysParamsUtil.isRakEnabled();
		
		if (SessionUtil.getIbePromotionPaymentPostDTO(request) == null) {
			if (!addGroundSegment) {
				insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(flightSegmentTOs, isReturn, getTrackInfo().getOriginChannelId());
			} else {
				totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);
				totalFltSegBuilder.getSelectedFlightSegments().addAll(flightSegmentTOs);
				insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(totalFltSegBuilder.getSelectedFlightSegments(),
						isReturn, getTrackInfo().getOriginChannelId());
			}
		}
		
		if (!makePayment) {
			insuranceQuotations = AncillaryJSONUtil.getInsuranceQuotation(insurance, insJrnyDto);
		}
		
		paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotations, getApplicationEngine(getTrackInfo()
				.getOriginChannelId()), (addGroundSegment == true ? totalFltSegBuilder.getSelectedFlightSegments()
				: flightSegmentTOs), null, resInfo.getAnciOfferTemplates());		
		
		ccChgDTO_Default = calculateCCCharges(new BigDecimal(totalFare), PaxUtil.getPayablePaxCount(paxList, modifyAncillary),
				(modifyAncillary ? ReservationUtil.getAnciUpdatedSegmentCount(paxList) : flightSegmentTOs.size()),
				operationalType);		
		
		PaymentGatewayWiseCharges pgwCharge = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge(Integer.parseInt(paymentGatewayId));
		
		// if PG Wise charges available, override the default CCCharge
		if(pgwCharge != null && pgwCharge.getChargeId() > 0){			
			//valid charge is defined for the selected pgw
			if(pgwCharge.getValuePercentageFlag() != CHARGE_BY_VALUE){
				ccChgDTO_Default.setRatioValueInPercentage(true);
				ccChgDTO_Default.setRatioValue(new BigDecimal(pgwCharge.getChargeValuePercentage()));
				ccChgDTO_Default.calculateAmount(new BigDecimal(totalFare));
			}else{
				ccChgDTO_Default.setRatioValueInPercentage(false);
				ccChgDTO_Default.setRatioValue(new BigDecimal(pgwCharge.getValueInDefaultCurrency()));
				ccChgDTO_Default.calculateAmount(PaxUtil.getPayablePaxCount(paxList, modifyAncillary), (modifyAncillary ? ReservationUtil.getAnciUpdatedSegmentCount(paxList) : flightSegmentTOs.size()));
			}
			setPgwChargeApplied(true);
		}	
		
		//override the chargeDTO stored in session
		externalCharges.put(EXTERNAL_CHARGES.CREDIT_CARD, ccChgDTO_Default);
		ccFee = ccChgDTO_Default.getAmount();	
		
		ExternalChgDTO ccFeeTax = ReservationUtil.getApplicableServiceTax(resInfo, EXTERNAL_CHARGES.JN_OTHER, operationalType);
		
		if (ccFeeTax != null) {
			ccFee = AccelAeroCalculator.add(ccFee, ccFeeTax.getTotalAmount());
			setJnTax(ccFeeTax.getTotalAmount().toString());
		}
		
		setPaymentGatewayWiseCharge(ccFee.toString());
		return forward;
	}


	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}


	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}


	public boolean isMakePayment() {
		return makePayment;
	}


	public void setMakePayment(boolean makePayment) {
		this.makePayment = makePayment;
	}


	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}


	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}


	public String getSelectedFlightJson() {
		return selectedFlightJson;
	}


	public void setSelectedFlightJson(String selectedFlightJson) {
		this.selectedFlightJson = selectedFlightJson;
	}


	public String getInsurance() {
		return insurance;
	}


	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}


	/**
	 * @return the paymentGatewayId
	 */
	public String getPaymentGatewayId() {
		return paymentGatewayId;
	}


	/**
	 * @param paymentGatewayId the paymentGatewayId to set
	 */
	public void setPaymentGatewayId(String paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}


	/**
	 * @return the totalFare
	 */
	public String getTotalFare() {
		return totalFare;
	}


	/**
	 * @param totalFare the totalFare to set
	 */
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}	
	
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}


	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}


	/**
	 * @return the paymentGatewayWiseCharge
	 */
	public String getPaymentGatewayWiseCharge() {
		return paymentGatewayWiseCharge;
	}


	/**
	 * @param paymentGatewayWiseCharge the paymentGatewayWiseCharge to set
	 */
	public void setPaymentGatewayWiseCharge(String paymentGatewayWiseCharge) {
		this.paymentGatewayWiseCharge = paymentGatewayWiseCharge;
	}


	/**
	 * @return the pgwChargeApplied
	 */
	public boolean isPgwChargeApplied() {
		return pgwChargeApplied;
	}


	/**
	 * @param pgwChargeApplied the pgwChargeApplied to set
	 */
	public void setPgwChargeApplied(boolean pgwChargeApplied) {
		this.pgwChargeApplied = pgwChargeApplied;
	}


	/**
	 * @return the jnTax
	 */
	public String getJnTax() {
		return jnTax;
	}


	/**
	 * @param jnTax the jnTax to set
	 */
	public void setJnTax(String jnTax) {
		this.jnTax = jnTax;
	}


	@SuppressWarnings("unchecked")
	private ExternalChgDTO calculateCCCharges(BigDecimal totalTicketPriceExcludingCC, int payablePaxCount, int segmentCount,
			Integer rateApplicableType) throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, rateApplicableType);
		ExternalChgDTO externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
		if (externalChgDTO.isRatioValueInPercentage()) {
			externalChgDTO.calculateAmount(totalTicketPriceExcludingCC);
		} else {
			externalChgDTO.calculateAmount(payablePaxCount, segmentCount);
		}
		return externalChgDTO;
	}
}
