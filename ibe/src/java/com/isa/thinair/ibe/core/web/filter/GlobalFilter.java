/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.ibe.core.web.filter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.HumanVerificationConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.core.service.IBEConfig;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.util.RequestUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.HttpUtil;

public class GlobalFilter implements Filter {

	private static Log log = LogFactory.getLog(GlobalFilter.class);

	private FilterConfig filterConfig;

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static IBEConfig ibeConfig = IBEModuleUtils.getConfig();

	private static Collection<String> restrictedIps = new ArrayList<String>();

	private static Collection<String> denyURIs = new ArrayList<String>();

	private static long lastTimeRefreshed = 0;

	/**
	 * Filter authentication
	 */
	public void doFilter(ServletRequest sRequest, ServletResponse sResponse, FilterChain chain)
			throws ServletException, IOException {

		if (globalConfig.isGenerateAppCookie()) {
			if (log.isDebugEnabled()) {
				HttpServletRequest request = (HttpServletRequest) sRequest;
				Cookie[] reqCookies = request.getCookies();

				StringBuilder message = new StringBuilder();
				message.append(request.getRequestURI());
				if (reqCookies != null && reqCookies.length > 0) {
					for (int i = 0; i < reqCookies.length; ++i) {
						Cookie cookie = reqCookies[i];
						message.append(":").append(cookie.getName()).append("=").append(cookie.getValue());
					}
				}
				message.append("::").append(request.getRequestedSessionId());
				log.debug(message.toString());
			}

			HttpServletResponse response = (HttpServletResponse) sResponse;
			String cookieName = globalConfig.getCookieName();
			InetAddress host = null;
			String ip = null;
			String cookieValue = null;
			try {
				host = InetAddress.getLocalHost();
				ip = host.getHostAddress();
				Map cookieValueMap = globalConfig.getCookieValueMap();
				if (cookieValueMap != null) {
					cookieValue = (String) cookieValueMap.get(ip);
				}
				if ((cookieName == null) || (cookieName.trim().equals("")) || (cookieValue == null)
						|| (cookieValue.trim().equals(""))) {
					throw new Exception("Cookie Name and/or Cookie Value is NULL or EMPTY");
				}
			} catch (UnknownHostException uhe) {
				log.error("Error in doFilter - server name could not be obtained to get the cookie value - ["
						+ uhe.getMessage() + "]");
				return;
			} catch (Exception e) {
				log.error("Error in doFilter - [" + e.getMessage() + "]");
				return;
			}
			Cookie cookie = new Cookie(cookieName, cookieValue);
			response.addCookie(cookie);
		}

		if (globalConfig.isControlIBECachingHeaders()) {
			String requestURI = ((HttpServletRequest) sRequest).getRequestURI();
			if (requestURI != null) {
				try {
					Map expirationDurationsMap = globalConfig.getCacheControlProperties();
					Iterator expirationDurationsMapIt = expirationDurationsMap.keySet().iterator();
					String path;
					while (expirationDurationsMapIt.hasNext()) {
						path = (String) expirationDurationsMapIt.next();
						if (requestURI.indexOf(path) != -1) {
							((HttpServletResponse) sResponse).addHeader("Cache-Control",
									"max-age=" + expirationDurationsMap.get(path));
							break;
						}
					}
				} catch (Exception e) {
					log.error("Error in doFilter - [" + e.getMessage() + "]", e);
					return;
				}
			}
		}

		if (globalConfig.isControlIBESecureHeaders()) {
			((HttpServletResponse) sResponse).addHeader("Cache-Control", "1");
			((HttpServletResponse) sResponse).addHeader("X-Content-Type-Options", "nosniff");
		}

		if (ibeConfig.isRestrictIps()) {
			try {
				if (System.currentTimeMillis() - lastTimeRefreshed > ibeConfig.getIpRestictionRefreshTime()) {
					lastTimeRefreshed = System.currentTimeMillis();

					// IP deny list
					Collection<String> currentIPs = null;
					BufferedReader br = new BufferedReader(new FileReader(ibeConfig.getIpRestictionFile()));
					String regExp = br.readLine();
					while (regExp != null) {
						if (currentIPs == null) {
							currentIPs = new ArrayList<String>();
						}
						log.warn("Adding IP address pattern for denying [IP=" + regExp.trim() + "]");
						currentIPs.add(regExp.trim());
						regExp = br.readLine();
					}
					br.close();

					if (!(restrictedIps.size() == 0 && currentIPs == null)) {
						if (log.isDebugEnabled()) {
							log.debug("Timestamp before updating IP denylist [" + System.currentTimeMillis() + "]");
						}

						synchronized (restrictedIps) {
							if (currentIPs == null) {
								restrictedIps.clear();
							} else {
								restrictedIps = currentIPs;
							}
						}

						if (log.isDebugEnabled()) {
							log.debug("Timestamp after updating IP denylist [" + System.currentTimeMillis() + "]");
						}
					}

					// URIs to deny
					if (ibeConfig.isDenyURIs()) {
						Collection<String> currentDenyURIs = null;
						BufferedReader br2 = new BufferedReader(new FileReader(ibeConfig.getDenyURIFile()));
						String regExp2 = br2.readLine();
						while (regExp2 != null) {
							if (currentDenyURIs == null) {
								currentDenyURIs = new ArrayList<String>();
							}
							log.warn("Adding deny URI pattern for denying [URI=" + regExp2.trim() + "]");
							currentDenyURIs.add(regExp2.trim());
							regExp2 = br2.readLine();
						}
						br2.close();

						if (!(denyURIs.size() == 0 && currentDenyURIs == null)) {
							if (log.isDebugEnabled()) {
								log.debug(
										"Timestamp before updating URI denylist [" + System.currentTimeMillis() + "]");
							}

							synchronized (denyURIs) {
								if (currentDenyURIs == null) {
									denyURIs.clear();
								} else {
									denyURIs = currentDenyURIs;
								}
							}

							if (log.isDebugEnabled()) {
								log.debug("Timestamp after updating URI denylist [" + System.currentTimeMillis() + "]");
							}
						}
					}
				}

				HttpServletRequest httpReq = (HttpServletRequest) sRequest;
				for (String regExp : restrictedIps) {
					Pattern pattern = Pattern.compile(regExp);
					Matcher matcher = pattern.matcher(httpReq.getRemoteAddr());
					if (matcher.find() && !httpReq.getRequestURI().endsWith("/js/notice.jsp")) {
						String timestamp = CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date());
						String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());
						log.warn("Access Denied From IP [regExp=" + regExp + "]" + "|" + timestamp + "|"
								+ httpReq.getRequestedSessionId() + "|" + requestURI + "|"
								+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "X-Forwarded-For")) + "|"
								+ httpReq.getRemoteAddr() + "|["
								+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "User-Agent")) + "]" + "|"
								+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "Referer")));

						((HttpServletResponse) sResponse).sendRedirect(httpReq.getContextPath() + "/js/notice.jsp");
						return;
					}
				}

				for (String regExp : denyURIs) {
					Pattern pattern = Pattern.compile(regExp);
					Matcher matcher = pattern.matcher(httpReq.getRequestURI());
					if (matcher.find() && !httpReq.getRequestURI().endsWith("/js/notice.jsp")) {
						String timestamp = CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date());
						String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());
						log.warn("Access Denied for URI [regExp=" + regExp + "]" + "|" + timestamp + "|"
								+ httpReq.getRequestedSessionId() + "|" + requestURI + "|"
								+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "X-Forwarded-For")) + "|"
								+ httpReq.getRemoteAddr() + "|["
								+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "User-Agent")) + "]" + "|"
								+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "Referer")));

						((HttpServletResponse) sResponse).sendRedirect(httpReq.getContextPath() + "/js/notice.jsp");
						return;
					}
				}
			} catch (Exception ex) {
				log.debug("Error in loading IP or URI deny list", ex);
			}
		}

		if (globalConfig.isLogIBEAccessInfo()) {
			try {

				HttpServletRequest httpReq = (HttpServletRequest) sRequest;
				String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());

				if (isLogAccessIncluded(PlatformUtiltiies.nullHandler(requestURI))) {

					boolean isShowReservation = requestURI.indexOf("showReservation.action") > -1;
					boolean isLoadPostCard = requestURI.indexOf("interlinePostCardDetails.action") > -1;
					boolean isLmsConf = requestURI.indexOf("lmsMemberConfirmation.action") > -1;
					boolean isLmsAjax = requestURI.indexOf("lmsAjaxRegister.action") > -1;

					String timestamp = CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date());
					StringBuilder logHeaderMesgBuilder = new StringBuilder();
					logHeaderMesgBuilder.append("AccessInfo |").append(timestamp).append("|");
					logHeaderMesgBuilder.append(httpReq.getRequestedSessionId()).append("|");
					logHeaderMesgBuilder.append(requestURI).append("|");
					logHeaderMesgBuilder
							.append(getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "X-Forwarded-For")))
							.append("|");
					logHeaderMesgBuilder.append(httpReq.getRemoteAddr()).append("|");
					logHeaderMesgBuilder.append(getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "User-Agent")))
							.append("|");
					logHeaderMesgBuilder.append(getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "Referer")))
							.append("|");
					logHeaderMesgBuilder
							.append(((globalConfig.isLogIBEReqParams()) ? getAllRequestParams(httpReq) : ""))
							.append("|");

					if (isShowReservation) {

						logHeaderMesgBuilder.append("HiddenParamData |")
								.append(RequestUtil.getHiddenParamData(httpReq));
						log.info(logHeaderMesgBuilder.toString());

					} else if (isLoadPostCard) {

						logHeaderMesgBuilder.append("Selected flight data |")
								.append(httpReq.getParameter("selectedFlightJson"));
						log.info(logHeaderMesgBuilder.toString());

					} else {

						log.info(logHeaderMesgBuilder.toString());

					}
				}
			} catch (Exception ex) {
				log.error("Error capturing access info", ex);
			}

		}

		if (System.getProperty(ReservationWebConstnts.SYS_PROP_DEBUG_FRONTEND) != null
				&& "true".equals(System.getProperty(ReservationWebConstnts.SYS_PROP_DEBUG_FRONTEND))) {
			filterConfig.getServletContext().setAttribute(CustomerWebConstants.APP_RELEASE_VERSION,
					CustomerWebConstants.APP_RELEASE_VERSION_URL_KEY + "=" + System.currentTimeMillis());
		}

		if (AppSysParamsUtil.isIBEHumanVerficationEnabled()) {
			try {
				HttpServletRequest httpReq = (HttpServletRequest) sRequest;
				String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());

				boolean isCaptchaValidationAction = (requestURI.indexOf("availabilitySearchCalendar.action") > -1
						|| requestURI.indexOf("nextPreviousCalendar.action") > -1
						|| requestURI.indexOf("multiCitySearch.action") > -1);

				if (isCaptchaValidationAction) {
					// Header name has to be configured in lower case in app
					// param
					String ipHitsCount = httpReq.getHeader(AppSysParamsUtil.getIPHitCountHeaderName());

					if (ipHitsCount == null) {
						ipHitsCount = httpReq.getHeader(AppSysParamsUtil.getIPHitCountHeaderName().toUpperCase());
					}

					if (ipHitsCount != null) {
						if (Integer.parseInt(ipHitsCount) > AppSysParamsUtil.maxPerIPHitCount()) {
							httpReq.setAttribute(HumanVerificationConstants.VALIDATE_CAPTCHA, true);
						}
					}
				}

			} catch (Exception ex) {

			}
		}
		chain.doFilter(sRequest, sResponse);
	}

	private boolean isLogAccessIncluded(String requestURI) {
		boolean includes = false;
		if (globalConfig.getLogAccessInfoIncludes() != null) {
			for (String include : globalConfig.getLogAccessInfoIncludes()) {
				if (requestURI.indexOf(include) > -1) {
					includes = true;
					break;
				}
			}
		}
		return includes;
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void destroy() {

	}

	private String getNullSafeValue(String value) {
		return StringUtils.trimToEmpty(value);
	}

	private static String getAllRequestParams(HttpServletRequest httpReq) {
		StringBuilder sb = new StringBuilder("|<REQPARAMS>");

		Enumeration pnames = httpReq.getParameterNames();
		String pvalues[];
		String pname;
		while (pnames.hasMoreElements()) {
			pname = (String) pnames.nextElement();

			if (!isExcludeParameter(pname)) {
				pvalues = httpReq.getParameterValues(pname);
				sb.append('|').append(pname);
				sb.append('|');
				for (int i = 0; i < pvalues.length; i++) {
					if (i > 0)
						sb.append(',');
					sb.append(pvalues[i]);
				}
			}
		}
		sb.append("|</REQPARAMS>");
		return sb.toString();
	}

	private static boolean isExcludeParameter(String variableName) {
		boolean isExclude = false;
		String[] excludeParms = { "card.cardNo", "card.cardCVV", "card.expiryDate", "expiryYear", "expiryMonth",
				"card.cardHoldersName" };

		for (String param : excludeParms) {
			if (param.equals(variableName)) {
				isExclude = true;
				break;
			}
		}

		return isExclude;
	}
}
