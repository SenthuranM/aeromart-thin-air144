package com.isa.thinair.ibe.core.web.v2.constants;

public class WebConstants {

	public static final int MAX_PRICE_GUIDE = 5;

	public static final int MAX_DEPARTURE_VARIANCE = 15;

	public static final String PARAM_SPLIT = "\\^";

	public static final String INVALID_CARD = "invalidCard";

	public static final String SESSION_TIMEOUT = "timeOut";

	public static final String XML_RESPONSE_PARAMETERS = "xmlResponse";
	
	public static final String AGENT_SERVICE_RESPONSE = "agentServiceResponse";

	public static final String BANKS_3D_SECURE_HTML_PAGE = "banks3DSecureHtml";

	public static final String DISPLAY_TOP_BOTTOM_BANNERS = "displayBanner";

	public static final String WAIT_TILL_RECEIPT = "WAIT_TILL_RECEIPT";

	public static final String IBE_NEW_SESSION_ID = "IBE_NEW_SESSION_ID";

	public static final String PNR = "pnr";

	public static final String GROUP_PNR = "groupPNR";

	public static final String LMS_URL = "crossProtalUrl";

	public static final String LOGIN_SUCCESS = "loginSuccess";

	/* IBE main functionality actions */
	/*
	 * DIRECTLOARDPNR - To retrieve the reservation using PNR. Pls ensure all validation before call this functionality
	 * You need to valid data before call this functionality Also please put PNR in session before call this
	 * functionality
	 */
	public enum PAGE {
		AS, MAN, MB, RE, SI, ASI, KOS, KOL, REA, REC, ELINK, NONE, MC, PRM, DIRECTLOARDPNR, MRE, GV, SR;

		public static PAGE getEnum(String strEnum, PAGE def) {
			for (PAGE enm : PAGE.values()) {
				if (enm.toString().equals(strEnum)) {
					return enm;
				}
			}

			return (def != null) ? def : PAGE.NONE;
		}

		public static PAGE getEnum(String strEnum) {
			return getEnum(strEnum, null);
		}
	};

	public enum Mode {
		CREATE_RESERVATION("CRE"), MODIFY_SEGMENT("MODSEG"), ADD_MODIFY_ANCILARY("ADDMODANCI"), NONE("NONE"), ADD_GROUND_SEGMENT(
				"ADDGRSEG");

		private String code;

		private Mode(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum PaymentFlow {
		FIRST_ATTEMPT, PAYMENT_RETRY
	};

}
