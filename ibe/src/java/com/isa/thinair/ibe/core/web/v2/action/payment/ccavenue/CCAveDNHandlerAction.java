package com.isa.thinair.ibe.core.web.v2.action.payment.ccavenue;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueRequest;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueResponse;
import com.isa.thinair.paymentbroker.core.bl.ccAvenue.CCAvenuePaymentUtils;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * 
 * @author Tharindu Galappaththi
 * CCAveDNHandlerAction : CCAvenue Dynamic Notification(Server to server response) handler
 *
 * **Dynamic Notifications are coming at the same time as the redirection happens to ibe.
 * **This is causing problems, hence this action is removed from the ibe/web.xml session filter exclusion list
 *
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
public class CCAveDNHandlerAction extends  BaseRequestResponseAwareAction {
	
	private static Log log = LogFactory.getLog(CCAveDNHandlerAction.class);
	
	public String execute() throws ModuleException {
		
		log.info("CCAveDNHandlerAction ### Starting..");
		synchronized (request.getSession()) {

			
			Map<String, String> receiptyMap = RequestParameterUtil.getReceiptMap(request);
			log.info("CCAveDNHandlerAction ### receiptMap : " + receiptyMap);
			
			if (AppSysParamsUtil.enableServerToServerMessages()) {
				
				String encResponseData = request.getParameter(CCAvenueResponse.ENCRESP); 
				if(encResponseData == null){
					log.error("CCAveDNHandlerAction ### encrypted response is NULL ");
					return null;
				}
				
				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
				List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(CCAvenueRequest.AA_CCAVENUE_PROVIDER_CODE);
				IPGPaymentOptionDTO ipgConfigs = pgwList.get(0);			
				
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO =  WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(ipgConfigs.getPaymentGateway(), ipgConfigs.getBaseCurrency());
				String ccAvenueEncKey = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO).getProperty(CCAvenueRequest.ENCRYPTION_KEY);
				String responseData = CCAvenuePaymentUtils.decrypt(encResponseData, ccAvenueEncKey);
				encResponseData = null;
				ccAvenueEncKey = null;
				pgwList = null;
				ipgConfigs = null;
				
				receiptyMap.put(CCAvenueResponse.ENCRESP, responseData);
				
				CCAvenueResponse response = CCAvenuePaymentUtils.processDynamicEventNotification(responseData);
				if (log.isDebugEnabled()){
					log.info("CCAveDNHandlerAction ### decrypted response : " + response.toString());
				}
				
				try {
					if (CCAvenueResponse.RESPONSE_SUCCESS.equals(response.getOrderStatus()) 
							&& !StringUtil.isNullOrEmpty(response.getMerchantParam2())) {
						String[] trakingDataArr = response.getMerchantParam2().split(CCAvenueRequest.MERCHANT_PARAM_DELIMETER);
						
						if (trakingDataArr.length == 3) {
							ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(new 
																						Integer(trakingDataArr[1]), trakingDataArr[2]);
							ipgResponseDTO = new IPGResponseDTO();
							int paymentBrokerRefNo = Integer.parseInt(trakingDataArr[0]);
							
							ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
							ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
							ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);
							
						} else {
							log.info("CCAveDNHandlerAction ### invalid merchant_param2" + response.toString());
						}
					}
				} catch(Exception ex) {
					log.error("CCAveDNHandlerAction ==> ", ex);
				}
			}
		}		
		return null;
	}	
}
