package com.isa.thinair.ibe.core.web.v2.action.reservation;

import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONObject;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.converters.AncillaryConverterUtil;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.FareCategoryTO.FareCategoryStatusCodes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfoByDateDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.v2.FareRuleDisplayTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.CalendarFlightAvailInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Pradeep Karunanayake
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class AvailabilitySearchCalendarAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(AvailabilitySearchCalendarAction.class);

	private boolean success = true;

	private String messageTxt;

	private Collection<AvailableFlightInfoByDateDTO> departueFlights;

	private Collection<AvailableFlightInfoByDateDTO> arrivalFlights;

	private final List<LogicalCabinClassInfoTO> outboundAvailableLogicalCC = new ArrayList<LogicalCabinClassInfoTO>();

	private final List<LogicalCabinClassInfoTO> inboundAvailableLogicalCC = new ArrayList<LogicalCabinClassInfoTO>();

	private String searchSystem;

	private FareQuoteTO fareQuote;

	// Modifing data
	private boolean modifySegment;

	private int oldFareID;

	private int oldFareType;

	private String oldFareAmount = "";

	private String oldFareCarrierCode = null;

	private String pnr;

	private boolean groupPNR;

	private String oldAllSegments;

	private String modifySegmentRefNos;

	private String airportMessage;

	private boolean showFareRules;

	private List<FareRuleDisplayTO> fareRules;

	private String calDisplayCurrency;

	private String requestSessionIdentifier;

	// segment modification IBE
	private Boolean modifyByDate;

	private Boolean modifyByRoute;

	private boolean addGroundSegment;

	private boolean allowOnhold;
	
	private boolean showPaymentIcons;

	private Set<String> paymentMethods;

	private boolean requoteFlightSearch;

	private DiscountedFareDetails promoInfo;

	private List<Collection<com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo>> ondSeqFlightsList;

	private List<String> actLogCabClassList = new ArrayList<String>();

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();

	private List<List<String>> calendarDayList;

	private List<List<CalendarFlightAvailInfo>> calendarDayFlightAvailList;

	private List<String> ondWiseAvailLogCOSInfoList;
	
	private JSONObject searchParamsCookieDetails  = new JSONObject();
	
	private boolean captchaValidationRequired;
	
	private boolean logicalCabinClassEnabled = false;
	
	@SuppressWarnings("unchecked")
	public String execute() {

		String result = StrutsConstants.Result.SUCCESS;
		boolean isCabinClassOnholdEnable = false;
		try {
			
			captchaValidationRequired = ReservationBeanUtil.isCaptchaValidationRequired(request);
			if(captchaValidationRequired) {
				return result;
			}
			
			// if (requoteFlightSearch && !AppSysParamsUtil.isCalendarViewEnabledForIBERequote()) {
			// getSearchParams().setDepartureVariance(0);
			// getSearchParams().setReturnVariance(0);
			// }
			request.getSession().setAttribute(ReservationWebConstnts.IS_MULTYCITY_SELECTED, false);

			FlightAvailRQ flightAvailRQ = BeanUtil.createFlightAvailRQ(getSearchParams(), (pnr == null || "".equals(pnr)));
			flightAvailRQ.getAvailPreferences().setQuoteFares(true);
			String language = SessionUtil.getLanguage(request);
			flightAvailRQ.getAvailPreferences().setPreferredLanguage(language);
			FareQuoteTO segFareQuote = null;
			if (pnr != null && !"".equals(pnr)) {
				flightAvailRQ.getAvailPreferences().setModifyBooking(true);
			} else {
			}

			if (modifySegment && !groupPNR) {
				SearchUtil.setModifySegmentData(flightAvailRQ, oldFareID, oldFareType, oldFareAmount);
			}
			if (AppSysParamsUtil.isGroundServiceEnabled() && !groupPNR && !addGroundSegment) {
				if (!AddModifySurfaceSegmentValidations.isValidSearchForFreshBooking(getSearchParams())) {
					success = false;
					// messageTxt = I18NUtil.getMessage("errors.resv.fare.Dept.Flight.null");
					messageTxt = I18NUtil.getMessage("errors.resv.flt.bus.only");
					return StrutsConstants.Result.SUCCESS;
				}
			}
			// Set Half Return Fare
			if (pnr != null && !"".equals(pnr)) {
				SearchUtil.setExistingSegInfo(flightAvailRQ, oldAllSegments, modifySegmentRefNos, getSearchParams()
						.isReturnFlag());
			}

			flightAvailRQ.getAvailPreferences().setOldFareCarrierCode(oldFareCarrierCode);
			if (groupPNR) {
				flightAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			} else {
				flightAvailRQ.getAvailPreferences().setSearchSystem(
						getSearchSystem(flightAvailRQ.getOriginDestinationInformationList(), pnr, groupPNR));
			}

			if (AppSysParamsUtil.isGroundServiceEnabled() && addGroundSegment) {

				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
				if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
					List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
					originDestinationInformationList.add(originDestinationInfoWithConnectionValidations(colsegs, flightAvailRQ
							.getOriginDestinationInformationList().iterator().next()));
					flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
				}
			}

			FlightAvailRS flightAvailRS = null;

			if (pnr != null && !"".equals(pnr)) {
				AnalyticsLogger.logAvlSearch(AnalyticSource.IBE_MOD_AVL, flightAvailRQ, request, getTrackInfo());
			} else {
				AnalyticsLogger.logAvlSearch(AnalyticSource.IBE_CRE_AVL, flightAvailRQ, request, getTrackInfo());
			}

			if (requoteFlightSearch) {
				flightAvailRQ.getAvailPreferences().setRequoteFlightSearch(true);
				flightAvailRQ.getAvailPreferences().setQuoteFares(false);
				flightAvailRQ.getAvailPreferences().setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
				flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
						getTrackInfo(), false);
			} else {
				flightAvailRS = SearchUtil.getFlightAvailableFlights(flightAvailRQ, getTrackInfo());
			}

			String accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);

			IBEReservationInfoDTO reservationInfo = SessionUtil.getIBEreservationInfo(request);
			if (reservationInfo == null) {
				reservationInfo = new IBEReservationInfoDTO();
				reservationInfo
						.setSelectedSystem(getSearchSystem(flightAvailRQ.getOriginDestinationInformationList(), pnr, groupPNR));
				SessionUtil.setIBEreservationInfo(request, reservationInfo);
			}

			reservationInfo.addNewServiceTaxes(flightAvailRS.getApplicableServiceTaxes());

			calendarDayList = ReservationBeanUtil
					.buildIBECalendarDays(flightAvailRQ.getOrderedOriginDestinationInformationList());
			// check flight availability for requote calendar
			if (requoteFlightSearch) {
				calendarDayFlightAvailList = ReservationBeanUtil.updateCalendarFlightAvailability(
						flightAvailRS.getOrderedOriginDestinationInformationList(), calendarDayList);
				ReservationBeanUtil.filterSelDateFlights(flightAvailRS);
			}

			actLogCabClassList.add(getSearchParams().getClassOfService());
			List<List<String>> ondWiseAvailableCOSList = new ArrayList<List<String>>();
			ondSeqFlightsList = ReservationBeanUtil.createAvailFlightInfoListMultiCity(flightAvailRS, getSearchParams(), false,
					false, false, null, actLogCabClassList, ondWiseAvailableCOSList);

			ondWiseAvailLogCOSInfoList = ReservationBeanUtil.generateCOSAvailabilityStringList(ondWiseAvailableCOSList);

			// TODO Re-factor
			if ((!CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint)) && !modifySegment && !addGroundSegment) {

				isCabinClassOnholdEnable = ReservationUtil.isReservationCabinClassOnholdable(flightAvailRS);
				reservationInfo.setAllowOnhold(isCabinClassOnholdEnable);

				if (isCabinClassOnholdEnable && AppSysParamsUtil.isOnHoldEnable(OnHold.IBE)) {
					boolean reservationOtherValidationOnholdable = ReservationUtil.isReservationOtherValidationOnholdable(
							flightAvailRS, SYSTEM.getEnum(getSearchParams().getSearchSystem()), getTrackInfo(),
							getClientInfoDTO().getIpAddress());
					reservationInfo.setReservationOnholdableOtherValidation(reservationOtherValidationOnholdable);

					allowOnhold = isCabinClassOnholdEnable && reservationOtherValidationOnholdable;

					if (allowOnhold) {
						for (OriginDestinationInformationTO ondInfo : flightAvailRS.getOriginDestinationInformationList()) {
							Date depTimeZulu = BeanUtil.getSelectedFlightDepartureDate(ondInfo.getOrignDestinationOptions());
							boolean ohdEnabled = (depTimeZulu != null) ? ReservationUtil.isOnHoldPaymentEnable(OnHold.IBE,
									depTimeZulu) : false;
							if (!(allowOnhold && ohdEnabled)) {
								allowOnhold = allowOnhold && ohdEnabled;
								break;
							}
							allowOnhold = allowOnhold && ohdEnabled;
						}
					}
				}
			}

			// segment modification interline fix when modifyByDate is false
			if (flightAvailRQ.getAvailPreferences().getSearchSystem() == SYSTEM.ALL
					|| flightAvailRQ.getAvailPreferences().getSearchSystem() == SYSTEM.INT) {
				if (modifyByDate != null) {
					if (modifyByDate == false) {
						String departureDate = getSearchParams().getDepartureDate();
						for (OriginDestinationInformationTO informationTO : flightAvailRS.getOriginDestinationInformationList()) {
							for (OriginDestinationOptionTO optionTO : informationTO.getOrignDestinationOptions()) {
								if (optionTO.isSelected()) {
									Date segmentDepDate = informationTO.getDepartureDateTimeStart();
									String fmtSegDepDate = CalendarUtil.formatDate(segmentDepDate, "dd/MM/yyyy");
									if (!departureDate.equals(fmtSegDepDate)) {
										optionTO.setSelected(false);
										flightAvailRS.setSelectedPriceFlightInfo(null);
									}
								}
							}
						}
					}
				}
			}

			Date departDateTimeStart = null;
			Date departDateTimeEnd = null;
			Date arrivalDateTimeStart = null;
			Date arrivalDateTimeEnd = null;

			for (OriginDestinationInformationTO ond : flightAvailRQ.getOriginDestinationInformationList()) {
				if (ond.isReturnFlag()) {
					arrivalDateTimeStart = ond.getDepartureDateTimeStart();
					arrivalDateTimeEnd = ond.getDepartureDateTimeEnd();
				} else {

					// Temporary fix for obtaining results for 7 - day calender
					if (AppSysParamsUtil.isGroundServiceEnabled() && addGroundSegment && !searchParams.isBusForDeparture()) {
						departDateTimeStart = CalendarUtil.getStartTimeOfDate(ond.getPreferredDate());
						departDateTimeStart = CalendarUtil.getOfssetAddedTime(departDateTimeStart,
								searchParams.getDepartureVariance() * -1);
						departDateTimeEnd = CalendarUtil.getEndTimeOfDate(ond.getPreferredDate());
						departDateTimeEnd = CalendarUtil.getOfssetAddedTime(departDateTimeEnd,
								searchParams.getDepartureVariance());

					} else {
						departDateTimeStart = ond.getDepartureDateTimeStart();
						departDateTimeEnd = ond.getDepartureDateTimeEnd();
					}

				}
			}

			PriceInfoTO priceInfoTO = flightAvailRS.getSelectedPriceFlightInfo();

			reservationInfo.setPriceInfoTO(priceInfoTO);

			reservationInfo.setBaggageSummaryTo(AncillaryConverterUtil.toLccReservationBaggageSummaryTo(flightAvailRS));

			// Set promotion discount information if any
			reservationInfo.setFareDiscount(flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
			if (priceInfoTO != null
					&& reservationInfo.getDiscountInfo() != null
					&& !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(reservationInfo.getDiscountInfo()
							.getPromotionType())) {				
				ReservationUtil.calculateDiscountForReservation(reservationInfo, null, flightAvailRQ, getTrackInfo(), true,
						reservationInfo.getDiscountInfo(), priceInfoTO, flightAvailRS.getTransactionIdentifier(), false);

			}		

			Object[] arrObj = SearchUtil.composeDisplayObjects(flightAvailRS, getSearchParams().getSelectedCurrency(), false,
					false, new Locale(CommonUtil.getJavaSupportLocale(language)), reservationInfo.getDiscountAmount(false));
			this.calDisplayCurrency = (String) arrObj[3];

			departueFlights = SearchUtil.getFlightsGroupByDate((List<AvailableFlightInfo>) arrObj[0], departDateTimeStart,
					departDateTimeEnd, getSearchParams().getDepartureDate());
			List<AvailableFlightInfo> selDateDepartureFlights = SearchUtil.getSelectedDateAvailableFlights(departueFlights);
			outboundAvailableLogicalCC.addAll(SearchUtil.getAvailableLogicalCabinClassInfo(getSearchParams().getClassOfService(),
					language, selDateDepartureFlights, flightAvailRQ.getAvailPreferences().getPreferredLanguage()));
			if (getSearchParams().isReturnFlag()) {
				arrivalFlights = SearchUtil.getFlightsGroupByDate((List<AvailableFlightInfo>) arrObj[1], arrivalDateTimeStart,
						arrivalDateTimeEnd, getSearchParams().getReturnDate());
				List<AvailableFlightInfo> selDateReturnFlights = SearchUtil.getSelectedDateAvailableFlights(arrivalFlights);
				inboundAvailableLogicalCC.addAll(SearchUtil.getAvailableLogicalCabinClassInfo(getSearchParams()
						.getClassOfService(), language, selDateReturnFlights, flightAvailRQ.getAvailPreferences()
						.getPreferredLanguage()));
			}

			promoInfo = reservationInfo.getDiscountInfo();

			fareQuote = (FareQuoteTO) arrObj[2];
			segFareQuote = (FareQuoteTO) arrObj[4];

			List<FareRuleDTO> ondFareRules = new ArrayList<FareRuleDTO>();
			if (priceInfoTO != null) {
				ondFareRules = BeanUtil.getFareRules(priceInfoTO, flightAvailRS.getOriginDestinationInformationList());
				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());
			}
			GlobalConfig globalConfig = new GlobalConfig();
			Map<String, FareCategoryTO> sortedFareCategoryTOMap = globalConfig.getFareCategories();
			fareRules = new ArrayList<FareRuleDisplayTO>();

			for (FareRuleDTO fareRule : ondFareRules) {
				FareCategoryTO fareCategoryTO = sortedFareCategoryTOMap.get(fareRule.getFareCategoryCode());
				if (fareCategoryTO != null && fareCategoryTO.getStatus().equals(FareCategoryStatusCodes.ACT)) {
					showFareRules = (showFareRules || fareCategoryTO.getShowComments());
					if (fareCategoryTO.getShowComments()
							|| AppSysParamsUtil.isBundledFaresEnabled(ApplicationEngine.IBE.toString())) {
						FareRuleDisplayTO frDis = new FareRuleDisplayTO();
						frDis.setOndCode(fareRule.getOrignNDest());
						frDis.setComment(fareRule.getComments());
						frDis.setBookingClass(fareRule.getBookingClassCode());
						frDis.setOndSequence(fareRule.getOndSequence());
						fareRules.add(frDis);
					}
				}
			}

			if (AppSysParamsUtil.isCalendarGradiantView()) {
				BigDecimal zero = BigDecimal.ZERO;
				SearchUtil.setPriceGuide(departueFlights, zero, zero);
				SearchUtil.setPriceGuide(arrivalFlights, zero, zero);
			}

			reservationInfo.setTransactionId(flightAvailRS.getTransactionIdentifier());
			reservationInfo.setTotalPriceWithoutExtChg(BeanUtil.getTotalPriceWithoutExtCharges(priceInfoTO));
			reservationInfo.setTotalFare(fareQuote.getTotalFare());
			reservationInfo.setTotalTax(fareQuote.getTotalTax());
			reservationInfo.setTotalTaxSurchages(fareQuote.getTotalTaxSurcharge());
			// Map<Integer, BigDecimal> ondTotalFlexiCharges =
			// getOndWiseTotalFlexiCharge(reservationInfo.getPriceInfoTO()
			// .getFareTypeTO().getOndExternalCharges());
			// // fareQuote.setOndFlexiTotalCharge(ondTotalFlexiCharges);

			// Request Session Identifier
			requestSessionIdentifier = (new Date().getTime()) + "";
			SessionUtil.setRequestSessionIdentifier(request, requestSessionIdentifier);

			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightAvailRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.IBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, language);
			// Payment Icons Display
			showPaymentIcons = AppSysParamsUtil.showPaymentMethodsOnIBE();

			if (showPaymentIcons) {

				String countryCode = ReservationUtil.getCountryCode(request, getClientInfoDTO().getIpAddress());
				
				boolean enableOfflinePaymentImages = ReservationUtil.isOfflineBookingEligible(flightAvailRS.getSelectedPriceFlightInfo(), 
						getSelectedFlights(flightAvailRS),ResOnholdValidationDTO.IBE_OFFLINE_VALIDATION, new ArrayList<ReservationPaxTO>(), null, 
						getClientInfoDTO().getIpAddress(), SYSTEM.getEnum(getSearchParams().getSearchSystem()),isCreateReservation(), getTrackInfo());

				PriceInfoTO info = flightAvailRS.getSelectedPriceFlightInfo();
				List<OndClassOfServiceSummeryTO> availableLogicalCCList =  (info == null) ? null :  info.getAvailableLogicalCCList();
				OndClassOfServiceSummeryTO firstOndClass = (availableLogicalCCList == null) ? null : availableLogicalCCList.get(0);
				String firstSedOndCode = (firstOndClass == null) ? null : firstOndClass.getOndCode();
				
				PaymentMethodDetailsDTO paymentMethodDetailsDTO =	(new PaymentMethodDetailsDTO())
						.setFirstSegmentONDCode(firstSedOndCode)
						.setCountryCode(countryCode)
						.setEnableOffline(enableOfflinePaymentImages);

				paymentMethods = ModuleServiceLocator.getPaymentBrokerBD().getPaymentMethodNames(paymentMethodDetailsDTO);
				// Add Cash method
				if (allowOnhold) {
					paymentMethods.add("CASH");
				}
				// Add System default icons
				paymentMethods.addAll(AppSysParamsUtil.getDefaultPaymentMethods());
			}

			/*
			 * If segment fare quote is available after all the session data insertion and calculations are done set
			 * discount data and switch them.
			 */
			if (segFareQuote != null) {
				boolean applyDiscount = (Boolean) arrObj[5];
				if (applyDiscount) {
					fareQuote = ReservationUtil.getDiscountSetFareQuote(fareQuote, segFareQuote);
				}
				if (fareQuote != null) {
					if (fareQuote.getTotalIbeFareDiscount() != null && !"".equals(fareQuote.getTotalIbeFareDiscount())) {
						reservationInfo.setIbeReturnFareDiscountAmount(new BigDecimal(fareQuote.getTotalIbeFareDiscount()));
					}
				}

			} else {
				reservationInfo.setIbeReturnFareDiscountAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}

			if (AppSysParamsUtil.isCookieSaveFunctionalityEnabled() && !requoteFlightSearch) {

				List<AvailableFlightInfo> selFlights = SearchUtil.getSelectedDateAvailableFlights(departueFlights);
				if (getSearchParams().isReturnFlag()) {
					List<AvailableFlightInfo> selReturnFlights = SearchUtil.getSelectedDateAvailableFlights(arrivalFlights);
					selFlights.addAll(selReturnFlights);
				}
				this.searchParamsCookieDetails = ReservationUtil.getSearchParamCookie(getSearchParams(), fareQuote, selFlights);
			}
			this.logicalCabinClassEnabled = AppSysParamsUtil.isLogicalCabinClassEnabled();
		} catch (ModuleException me) {
			log.error(me.getModuleCode(), me);
			success = false;
			// FIXME All Error Codes propergate
			if (!me.getExceptionCode().isEmpty()) {
				messageTxt = I18NUtil.getMessage(me.getExceptionCode());
			} else {
				messageTxt = me.getMessage();
			}
		} catch (Exception ex) {
			/**
			 * Added additional logs to identify date format issue [25-Nov-20142014 23:59:59]
			 */
			log.error("AvailabilitySearchCalendarAction==>SessionID :" +  request.getRequestedSessionId() 
					+ " Departure Date : " + getSearchParams().getDepartureDate()
					+ " Return Date :" + getSearchParams().getReturnDate() , ex);
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			SessionUtil.resetSesionDataInError(request);
		}
		return result;
	}

	private SYSTEM getSearchSystem(List<OriginDestinationInformationTO> ondInfoTOList, String pnr, boolean groupPnr) throws ModuleException {

		if (pnr != null && groupPnr) {
			return SYSTEM.INT;
		}

		return ModuleServiceLocator.getCommoMasterBD().getSearchSystem(ondInfoTOList, getTrackInfo().getCarrierCode());
	}

	public List<List<String>> getCalendarDayList() {
		return calendarDayList;
	}

	public List<String> getOndWiseAvailLogCOSInfoList() {
		return ondWiseAvailLogCOSInfoList;
	}

	private List<FlightSegmentTO> getSelectedFlights(FlightAvailRS flightAvailRS) {
		List<FlightSegmentTO> flightSegmentTOs = null;

		if (flightAvailRS != null) {
			for (OriginDestinationInformationTO informationTO : flightAvailRS.getOriginDestinationInformationList()) {
				for (OriginDestinationOptionTO optionTO : informationTO.getOrignDestinationOptions()) {
					if (optionTO.isSelected()) {
						flightSegmentTOs = optionTO.getFlightSegmentList();
					}
				}
			}
		}

		return flightSegmentTOs;
	}

	private boolean isCreateReservation() {
		boolean createReservation = true;

		if (modifySegment || addGroundSegment) {
			createReservation = false;
		}

		return createReservation;
	}

	private OriginDestinationInformationTO originDestinationInfoWithConnectionValidations(
			Collection<LCCClientReservationSegment> colsegs, OriginDestinationInformationTO originDestinationInformation)
			throws ModuleException {

		String[] selectedSegs = modifySegmentRefNos.split(",");
		String[] selectedSeg = selectedSegs[0].split("\\$");

		ArrayList<Integer> arrFlightSegId = new ArrayList<Integer>();
		for (LCCClientReservationSegment resSeg : colsegs) {
			if (resSeg.getBookingFlightSegmentRefNumber().equals(selectedSeg[0])) {
				arrFlightSegId.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));
				break;
			}
		}

		Collection<FlightSegmentDTO> flightSegments = ModuleServiceLocator.getFlightBD().getFlightSegments(arrFlightSegId);
		Iterator<FlightSegmentDTO> segIter = flightSegments.iterator();
		FlightSegmentDTO flightSegment = null;
		String[] minMaxTransitDurations = null;
		Calendar valiedTimeFrom = Calendar.getInstance();
		Calendar valiedTimeTo = Calendar.getInstance();
		boolean busForDeparture = false;

		if (segIter.hasNext()) {
			flightSegment = segIter.next();
		}

		Set<String> busConnectingAirports = CommonsServices.getGlobalConfig().getBusConnectingAirports();
		if (flightSegment != null && flightSegment.getSegmentCode() != null) {
			String airlineCarrier = flightSegment.getFlightNumber().substring(0, 2);
			String busCarrier = SelectListGenerator.getDefaultBusCarrierCode(airlineCarrier);
			// Departure airport of air segment is connecting to bus segments
			if (busConnectingAirports.contains(flightSegment.getSegmentCode().substring(0, 3))) {
				busForDeparture = true;
				minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
						flightSegment.getSegmentCode().substring(0, 3), busCarrier, airlineCarrier);

				String[] minDurationHHMM = minMaxTransitDurations[1].split(":");
				valiedTimeFrom.setTime(flightSegment.getDepartureDateTime());
				valiedTimeFrom.add(Calendar.HOUR, -(Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, -(Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = minMaxTransitDurations[0].split(":");
				valiedTimeTo.setTime(flightSegment.getDepartureDateTime());
				valiedTimeTo.add(Calendar.HOUR, -(Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, -(Integer.parseInt(maxDurationHHMM[1])));

				if (originDestinationInformation != null) {
					originDestinationInformation.setArrivalDateTimeStart(valiedTimeFrom.getTime());
					originDestinationInformation.setArrivalDateTimeEnd(valiedTimeTo.getTime());
				}

			} else if (busConnectingAirports.contains(flightSegment.getSegmentCode().substring(
					flightSegment.getSegmentCode().length() - 3))) {
				minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
						flightSegment.getSegmentCode().substring(flightSegment.getSegmentCode().length() - 3), airlineCarrier,
						busCarrier);

				String[] minDurationHHMM = minMaxTransitDurations[0].split(":");
				valiedTimeFrom.setTime(flightSegment.getArrivalDateTime());
				valiedTimeFrom.add(Calendar.HOUR, (Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, (Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = minMaxTransitDurations[1].split(":");
				valiedTimeTo.setTime(flightSegment.getArrivalDateTime());
				valiedTimeTo.add(Calendar.HOUR, (Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, (Integer.parseInt(maxDurationHHMM[1])));

				if (originDestinationInformation != null) {
					Calendar dptStart = Calendar.getInstance();
					dptStart.setTime(originDestinationInformation.getDepartureDateTimeStart());
					Calendar dptEnd = Calendar.getInstance();
					dptEnd.setTime(originDestinationInformation.getDepartureDateTimeEnd());

					// Valied times should be within the search date.

					List<Calendar> valiedPeriod = CommonUtil.validPeriod(dptStart, dptEnd, valiedTimeFrom, valiedTimeTo);

					if (valiedPeriod.size() == 2) {
						originDestinationInformation.setDepartureDateTimeStart(valiedPeriod.get(0).getTime());
						originDestinationInformation.setDepartureDateTimeEnd(valiedPeriod.get(1).getTime());
					}
				}
			}
		}
		// Set required parameters for Next/Previous navigations
		searchParams.setBusForDeparture(busForDeparture);
		searchParams.setBusConValiedTimeFrom(String.valueOf(valiedTimeFrom.getTimeInMillis()));
		searchParams.setBusConValiedTimeTo(String.valueOf(valiedTimeTo.getTimeInMillis()));

		return originDestinationInformation;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<AvailableFlightInfoByDateDTO> getDepartueFlights() {
		return departueFlights;
	}

	public Collection<AvailableFlightInfoByDateDTO> getArrivalFlights() {
		return arrivalFlights;
	}

	public String getSearchSystem() {
		return searchSystem;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	@Override
	public FlightSearchDTO getSearchParams() {
		return super.getSearchParams();
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public void setOldFareID(int oldFareID) {
		this.oldFareID = oldFareID;
	}

	public void setOldFareType(int oldFareType) {
		this.oldFareType = oldFareType;
	}

	public void setOldFareAmount(String oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public boolean isShowFareRules() {
		return showFareRules;
	}

	public List<FareRuleDisplayTO> getFareRules() {
		return fareRules;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getCalDisplayCurrency() {
		return calDisplayCurrency;
	}

	public String getRequestSessionIdentifier() {
		return requestSessionIdentifier;
	}

	public Boolean getModifyByDate() {
		if (modifyByDate == null) {
			modifyByDate = false;
		}
		return modifyByDate;
	}

	public void setModifyByDate(Boolean modifyByDate) {
		this.modifyByDate = modifyByDate;
	}

	public Boolean getModifyByRoute() {
		if (modifyByRoute == null) {
			modifyByRoute = false;
		}
		return modifyByRoute;
	}

	public void setModifyByRoute(Boolean modifyByRoute) {
		this.modifyByRoute = modifyByRoute;
	}

	public void setOldFareCarrierCode(String oldFareCarrierCode) {
		this.oldFareCarrierCode = oldFareCarrierCode;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	public List<LogicalCabinClassInfoTO> getOutboundAvailableLogicalCC() {
		return outboundAvailableLogicalCC;
	}

	public List<LogicalCabinClassInfoTO> getInboundAvailableLogicalCC() {
		return inboundAvailableLogicalCC;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public String getModifySegmentRefNos() {
		return modifySegmentRefNos;
	}

	public Set<String> getPaymentMethods() {
		return paymentMethods;
	}

	public DiscountedFareDetails getPromoInfo() {
		return promoInfo;
	}

	public List<Collection<com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo>> getOndSeqFlightsList() {
		return ondSeqFlightsList;
	}

	public List<String> getActLogCabClassList() {
		return actLogCabClassList;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public JSONObject getSearchParamsCookieDetails() {
		return searchParamsCookieDetails;
	}

	public void setSearchParamsCookieDetails(JSONObject searchParamsCookieDetails) {
		this.searchParamsCookieDetails = searchParamsCookieDetails;
	}

	public boolean isShowPaymentIcons() {
		return showPaymentIcons;
	}

	public void setShowPaymentIcons(boolean showPaymentIcons) {
		this.showPaymentIcons = showPaymentIcons;
	}

	public boolean isCaptchaValidationRequired() {
		return captchaValidationRequired;
	}


	public void setCaptchaValidationRequired(boolean captchaValidationRequired) {
		this.captchaValidationRequired = captchaValidationRequired;
	}

	public boolean isLogicalCabinClassEnabled() {
		return logicalCabinClassEnabled;
	}

	public void setLogicalCabinClassEnabled(boolean logicalCabinClassEnabled) {
		this.logicalCabinClassEnabled = logicalCabinClassEnabled;
	}

	public List<List<CalendarFlightAvailInfo>> getCalendarDayFlightAvailList() {
		return calendarDayFlightAvailList;
	}

	public void setCalendarDayFlightAvailList(List<List<CalendarFlightAvailInfo>> calendarDayFlightAvailList) {
		this.calendarDayFlightAvailList = calendarDayFlightAvailList;
	}
}
