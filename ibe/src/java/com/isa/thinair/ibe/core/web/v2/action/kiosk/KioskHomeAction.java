package com.isa.thinair.ibe.core.web.v2.action.kiosk;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.FlightSearchInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;

/**
 * Kiosk Home functionality
 * 
 * @author Pradeep Karunanayake
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class KioskHomeAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(KioskHomeAction.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private Map<String, String> errorInfo;

	private boolean success = true;

	private String messageTxt;

	private HashMap<String, String> jsonLabel;

	private FlightSearchInfoDTO searchInfo;

	private String carrierName;

	private String ibeSecureURL;

	private String regUserAccessInKiosk;

	private String language;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			language = strLanguage;
			String[] pagesIDs = { "PgSearch", "PgKioskHome" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			searchInfo = SearchUtil.getSearchInfoDTO(request);
			errorInfo = ErrorMessageUtil.getFlightSearchErrors(request);
			carrierName = SystemUtil.getCarrierName(request);
			ibeSecureURL = AppParamUtil.getSecureIBEUrl();
			regUserAccessInKiosk = globalConfig.getBizParam(SystemParamKeys.ENABLE_REGISTERED_USER_ACCESS_IN_KIOSK);
		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("KioskHomeAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("KioskHomeAction==>", ex);
		}
		return forward;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public FlightSearchInfoDTO getSearchInfo() {
		return searchInfo;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public String getIbeSecureURL() {
		return ibeSecureURL;
	}

	public String getRegUserAccessInKiosk() {
		return regUserAccessInKiosk;
	}

	public String getLanguage() {
		return language;
	}

}
