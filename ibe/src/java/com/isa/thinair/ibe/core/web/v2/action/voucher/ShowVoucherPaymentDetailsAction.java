package com.isa.thinair.ibe.core.web.v2.action.voucher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.action.reservation.PaymentGatewayWiseChargeAction;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author chanaka
 *
 */
@SuppressWarnings("unchecked")
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR)

})
public class ShowVoucherPaymentDetailsAction extends IBEBaseAction {

	private static final Log log = LogFactory.getLog(ShowVoucherPaymentDetailsAction.class);
	
	public static final String CHARGE_BY_VALUE = "V";
	
	public static final String CHARGE_BY_PERCENTAGE = "P";

	private Collection<PaymentGateWayInfoDTO> paymentGateways;

	private Collection<PaymentGateWayInfoDTO> allPaymentGateways;

	private Collection<CountryPaymentCardBehaviourDTO> cardsInfo;

	private int paymentGatewayID;

	private String ccTxnFee;

	private String fullPayment;

	private String currencyCode;

	private String amountInLocal;

	private String amountInBase;

	private String ccTxnFeeInBase;

	private String fullPaymentInBase;
	
	private List<VoucherTemplateTo> voucherList;

	public String execute() {

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		if (AppSysParamsUtil.isVoucherEnabled()) {
			if (AppSysParamsUtil.isCreditCardPaymentsEnabled()) {
				// Object[] pgArr = getPaymentgateWay(exchangeRateProxy, offlinePaymentOnholdTimeInsec, resInfo);
				Object[] pgArr;
				try {
					pgArr = getPaymentgateWay(exchangeRateProxy);
					this.paymentGateways = (Collection<PaymentGateWayInfoDTO>) pgArr[0];
				} catch (ModuleException e) {
					log.error("ShowVoucherPaymentDetailAction ==> execute()", e);
				}

			}
			
			if (voucherList != null) {
				Collection<EXTERNAL_CHARGES> colExternalCharges = new ArrayList<EXTERNAL_CHARGES>();
				colExternalCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
				try {
					Map<EXTERNAL_CHARGES, ExternalChgDTO> externalCharges = ModuleServiceLocator.getReservationBD()
							.getQuotedExternalCharges(colExternalCharges, null, ChargeRateOperationType.MAKE_ONLY);
					ExternalChgDTO externalChgDTO = externalCharges.get(EXTERNAL_CHARGES.CREDIT_CARD);
					for (VoucherTemplateTo temp : voucherList) {
						externalChgDTO.calculateAmount(new BigDecimal(temp.getAmountInLocal()));
						temp.setCcTnxFee(AccelAeroCalculator.scaleValueDefault(externalChgDTO.getAmount()).toString());
						temp.setFullPayment(AccelAeroCalculator.scaleValueDefault(
								AccelAeroCalculator.add(new BigDecimal(temp.getAmountInLocal()), externalChgDTO.getAmount()))
								.toString());
					}
					// externalChgDTO.calculateAmount(new BigDecimal(amountInLocal));
					// ccTxnFee = AccelAeroCalculator.scaleValueDefault(externalChgDTO.getAmount()).toString();
					// fullPayment = AccelAeroCalculator.scaleValueDefault(
					// AccelAeroCalculator.add(new BigDecimal(amountInLocal), externalChgDTO.getAmount())).toString();

				} catch (ModuleException ex) {
					log.error("ShowVoucherPaymentDetailAction ==> execute()", ex);
				}
			}
		}
		return StrutsConstants.Result.SUCCESS;
	}

	private Object[] getPaymentgateWay(ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		Object[] pgArray = new Object[2];
		Collection<PaymentGateWayInfoDTO> colPgs = new ArrayList<PaymentGateWayInfoDTO>();
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		if (getSearchParams() != null) {
			ipgPaymentOptionDTO.setSelCurrency(getSearchParams().getSelectedCurrency());
		}
		ipgPaymentOptionDTO.setModuleCode("IBE");
		String brokerType = null;

		Map<String, Collection<String[]>> cardTypes = SelectListGenerator.createCreditCardTypesForAllPGW();

		Map paymentMap = ModuleServiceLocator.getPaymentBrokerBD().getPaymentOptions(ipgPaymentOptionDTO);
		List baseCurrencyList = (List) paymentMap.get("BASE_CURRENCY");
		List paymentGatewayList = (List) paymentMap.get("PAYMENT_GATEWAY");

		// Get Country Code
		String countryCode = ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(getClientInfoDTO().getIpAddress());

		if (AppSysParamsUtil.isTestStstem()) {
			String testSysOriginCountry = (String) request.getSession().getAttribute("originCountry");
			if (testSysOriginCountry == null || testSysOriginCountry.trim().length() == 0) {
				testSysOriginCountry = "OT"; // Other country
			}
			countryCode = testSysOriginCountry;
		}

		Collection<Integer> paymentGatewaysIds =  ReservationUtil.getPaymentGatewayIDs(paymentGatewayList);
		PaymentBrokerBD paymentBroker = ModuleServiceLocator.getPaymentBrokerBD();
		boolean isOndWisePayment = AppSysParamsUtil.getIsONDWisePayment();

		String defaultOndCode = null;
		cardsInfo = isOndWisePayment ? paymentBroker.getONDWiseCountryCardBehavior(defaultOndCode,paymentGatewaysIds):
				paymentBroker.getContryPaymentCardBehavior(countryCode,paymentGatewaysIds);

		Map<Integer, String> PGsModificationAllowMap = ReservationUtil.getPaymentModificationAllowMap(paymentGatewayList);
		Iterator<CountryPaymentCardBehaviourDTO> itCardsInfo = cardsInfo.iterator();

		while (itCardsInfo.hasNext()) {
			CountryPaymentCardBehaviourDTO cardInfo = itCardsInfo.next();
			cardInfo.setModicifationAllowed(isModifiAllow(PGsModificationAllowMap.get(cardInfo.getPaymentGateWayID())));
		}

		Object[] listArr = baseCurrencyList.toArray();
		setPGWiseCharges();

		for (int i = 0; i < listArr.length; i++) {
			ipgPaymentOptionDTO = (IPGPaymentOptionDTO) listArr[i];
			PaymentGateWayInfoDTO pgw = new PaymentGateWayInfoDTO();
			pgw.setDescription(ipgPaymentOptionDTO.getDescription());
			pgw.setPayCurrency(ipgPaymentOptionDTO.getBaseCurrency());
			pgw.setPaymentGateway(ipgPaymentOptionDTO.getPaymentGateway());
			pgw.setProviderCode(ipgPaymentOptionDTO.getProviderCode());
			pgw.setProviderName(ipgPaymentOptionDTO.getProviderName());
			brokerType = getBrokerType(ipgPaymentOptionDTO.getPaymentGateway(), ipgPaymentOptionDTO.getBaseCurrency());
			pgw.setBrokerType(brokerType);
			pgw.setModifyAllow(isModifiAllow(ipgPaymentOptionDTO.getIsModificationAllow()));
			pgw.setSwitchToExternalURL(isSwitchToExternalURL(ipgPaymentOptionDTO.getPaymentGateway(),
					ipgPaymentOptionDTO.getBaseCurrency()));

			if (cardTypes != null
					&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)) {
				pgw.setCardTypes(cardTypes.get(Integer.toString(ipgPaymentOptionDTO.getPaymentGateway())));
			}
			try {
				CurrencyExchangeRate currencyExrate = exchangeRateProxy.getCurrencyExchangeRate(
						ipgPaymentOptionDTO.getBaseCurrency(), ApplicationEngine.IBE);
				Currency pgCurrency = currencyExrate.getCurrency();

				// if paymentGatewayWiseCharges Enabled
				Collection<EXTERNAL_CHARGES> colExternalCharges = new ArrayList<EXTERNAL_CHARGES>();
				colExternalCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
				Map<EXTERNAL_CHARGES, ExternalChgDTO> externalCharges = ModuleServiceLocator.getReservationBD()
						.getQuotedExternalCharges(colExternalCharges, null, ChargeRateOperationType.MAKE_ONLY);
				ExternalChgDTO ccChgDTO = externalCharges.get(EXTERNAL_CHARGES.CREDIT_CARD);

				PaymentGatewayWiseCharges pgwCharge = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge(
						pgw.getPaymentGateway());

				if (pgwCharge != null && pgwCharge.getChargeId() > 0 && ccChgDTO != null) {
					BigDecimal ccFee = new BigDecimal("0");
					ccChgDTO = (ExternalChgDTO) ccChgDTO.clone();
					// valid charge is defined for the selected pgw
					if (pgwCharge.getValuePercentageFlag() != PaymentGatewayWiseChargeAction.CHARGE_BY_VALUE) {
						ccChgDTO.setRatioValueInPercentage(true);
						ccChgDTO.setRatioValue(new BigDecimal(pgwCharge.getChargeValuePercentage()));
						ccChgDTO.calculateAmount(new BigDecimal(amountInBase));
					} else {
						ccChgDTO.setRatioValueInPercentage(false);
						ccChgDTO.setRatioValue(new BigDecimal(pgwCharge.getValueInDefaultCurrency()));
						ccChgDTO.calculateAmount(new BigDecimal(amountInBase));
					}

					ccFee = ccChgDTO.getAmount();

					pgw.setPayCurrencyAmount(AccelAeroRounderPolicy
							.convertAndRound(AccelAeroCalculator.add(new BigDecimal(amountInBase), ccFee),
									currencyExrate.getMultiplyingExchangeRate(), pgCurrency.getBoundryValue(),
									pgCurrency.getBreakPoint()).toString());
				} else {
					pgw.setPayCurrencyAmount(AccelAeroRounderPolicy
							.convertAndRound(new BigDecimal(amountInBase), currencyExrate.getMultiplyingExchangeRate(),
									pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());
				}
				colPgs.add(pgw);
			} catch (ModuleException me) {
				log.error("Exchange rate is not defined for " + ipgPaymentOptionDTO.getBaseCurrency(), me);
			}
		}
		pgArray[0] = colPgs;
		return pgArray;
	}

	/**
	 * Update cardsInfo with configuration broker type
	 * 
	 * @param colAllPgs
	 */

	public static String getBaseCurrencyAmount(String currencyCode, BigDecimal value) throws ModuleException {
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		BigDecimal convertedValue = null;
		if (!baseCurrencyCode.equals(currencyCode) && value != null) {
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);
			convertedValue = AccelAeroRounderPolicy.convertAndRound(value, exchangeRate, null, null);
		} else {
			convertedValue = value;
		}
		return convertedValue.toString();
	}

	private String getBrokerType(int paymentGatewayID, String baseCurrency) throws ModuleException {
		Properties ipgProps = getIPGProperites(paymentGatewayID, baseCurrency);
		return ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
	}

	private boolean isSwitchToExternalURL(int paymentGatewayID, String baseCurrency) throws ModuleException {
		Properties ipgProps = getIPGProperites(paymentGatewayID, baseCurrency);
		return Boolean.parseBoolean(ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SWITCH_TO_EXTERNAL_URL));
	}

	private Properties getIPGProperites(int paymentGatewayID, String baseCurrency) throws ModuleException {
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
				paymentGatewayID, baseCurrency);
		return ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
	}

	private boolean isModifiAllow(String isModifiAllow) {
		if ("Y".equalsIgnoreCase(isModifiAllow)) {
			return true;
		}
		return false;
	}
	
	private void setPGWiseCharges () {
		try {		
			ExternalChgDTO ccChgDTO = calculateCCCharges(new BigDecimal(amountInBase), paymentGatewayID);
			BigDecimal ccFee = BigDecimal.ZERO;
			if (ccChgDTO != null && ccChgDTO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
				ccFee = ccChgDTO.getAmount();	
				amountInBase = AccelAeroCalculator.add(new BigDecimal(amountInBase),ccFee).toString();
			}		
			ccTxnFeeInBase = ccFee.toString();
		} catch (ModuleException me){
			log.error("Exchange rate is not defined for " + currencyCode, me);
		}
	}
	
	@SuppressWarnings("unchecked")
	private ExternalChgDTO calculateCCCharges(BigDecimal voucherAmount, int paymentGatewayId) throws ModuleException {

		ExternalChgDTO externalChgDTO = null;
		if (paymentGatewayID > 0) {
			@SuppressWarnings("rawtypes")
			Collection colEXTERNAL_CHARGES = new ArrayList();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
			Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(
					colEXTERNAL_CHARGES, null, ChargeRateOperationType.MAKE_ONLY);
			externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);

			// Check for Payment gateway wise charges and override if exists.
			PaymentGatewayWiseCharges pgwCharge = null;

			pgwCharge = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge(paymentGatewayId);
			if (pgwCharge != null && pgwCharge.getChargeId() > 0) {
				// valid charge is defined for the selected pgw
				if (pgwCharge.getValuePercentageFlag() != CHARGE_BY_VALUE) {
					externalChgDTO.setRatioValueInPercentage(true);
					externalChgDTO.setRatioValue(new BigDecimal(pgwCharge.getChargeValuePercentage()));
				} else {
					externalChgDTO.setRatioValueInPercentage(false);
					externalChgDTO.setRatioValue(new BigDecimal(pgwCharge.getValueInDefaultCurrency()));
				}
			}

			if (externalChgDTO.isRatioValueInPercentage()) {
				externalChgDTO.calculateAmount(voucherAmount);
			}
		}

		return externalChgDTO;
	}

	public Collection<PaymentGateWayInfoDTO> getPaymentGateways() {
		return paymentGateways;
	}

	public void setPaymentGateways(Collection<PaymentGateWayInfoDTO> paymentGateways) {
		this.paymentGateways = paymentGateways;
	}

	public Collection<PaymentGateWayInfoDTO> getAllPaymentGateways() {
		return allPaymentGateways;
	}

	public void setAllPaymentGateways(Collection<PaymentGateWayInfoDTO> allPaymentGateways) {
		this.allPaymentGateways = allPaymentGateways;
	}

	public Collection<CountryPaymentCardBehaviourDTO> getCardsInfo() {
		return cardsInfo;
	}

	public void setCardsInfo(Collection<CountryPaymentCardBehaviourDTO> cardsInfo) {
		this.cardsInfo = cardsInfo;
	}

	public int getPaymentGatewayID() {
		return paymentGatewayID;
	}

	public void setPaymentGatewayID(int paymentGatewayID) {
		this.paymentGatewayID = paymentGatewayID;
	}

	public String getCcTxnFee() {
		return ccTxnFee;
	}

	public void setCcTxnFee(String ccTxnFee) {
		this.ccTxnFee = ccTxnFee;
	}

	public String getFullPayment() {
		return fullPayment;
	}

	public void setFullPayment(String fullPayment) {
		this.fullPayment = fullPayment;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the amountInLocal
	 */
	public String getAmountInLocal() {
		return amountInLocal;
	}

	/**
	 * @param amountInLocal
	 *            the amountInLocal to set
	 */
	public void setAmountInLocal(String amountInLocal) {
		this.amountInLocal = amountInLocal;
	}

	/**
	 * @return the amountInBase
	 */
	public String getAmountInBase() {
		return amountInBase;
	}

	/**
	 * @param amountInBase
	 *            the amountInBase to set
	 */
	public void setAmountInBase(String amountInBase) {
		this.amountInBase = amountInBase;
	}

	/**
	 * @return the ccTxnFeeInBase
	 */
	public String getCcTxnFeeInBase() {
		return ccTxnFeeInBase;
	}

	/**
	 * @param ccTxnFeeInBase
	 *            the ccTxnFeeInBase to set
	 */
	public void setCcTxnFeeInBase(String ccTxnFeeInBase) {
		this.ccTxnFeeInBase = ccTxnFeeInBase;
	}

	/**
	 * @return the fullPaymentInBase
	 */
	public String getFullPaymentInBase() {
		return fullPaymentInBase;
	}

	/**
	 * @param fullPaymentInBase
	 *            the fullPaymentInBase to set
	 */
	public void setFullPaymentInBase(String fullPaymentInBase) {
		this.fullPaymentInBase = fullPaymentInBase;
	}

	public List<VoucherTemplateTo> getVoucherList() {
		return voucherList;
	}

	public void setVoucherList(List<VoucherTemplateTo> voucherList) {
		this.voucherList = voucherList;
	}


}
