package com.isa.thinair.ibe.core.web.constants;

public class AgentWebConstants {

	public final static String REQ_HTML_COUNTRY_SELECT_LIST = "countryList";
	public final static String REQ_HTML_STATION_SELECT_LIST = "stationList";
	public final static String REQ_AGENT_TYPES = "reqAgentTypes";

}
