package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.converters.AncillaryConverterUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.FareCategoryTO.FareCategoryStatusCodes;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.v2.FareRuleDisplayTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;

/**
 * @author M.Rikaz
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "")

})
public class MultiCityFareQuoteAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(MultiCityFareQuoteAction.class);

	private boolean success = true;

	private String messageTxt;

	private FareQuoteTO fareQuote;

	private boolean groupPNR;

	private String airportMessage;

	private boolean forceRefresh = false;

	private BigDecimal outboundCalFare;

	private BigDecimal inboundCalFare;

	private List<FareRuleDisplayTO> fareRules;

	private boolean showFareRules;

	private boolean fromPostCardPage;

	private String msgFromPostCardPage;

	private String requestSessionIdentifier;

	private boolean allowOnhold;

	private Collection<AvailableFlightInfo> flightInfo;

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private DiscountedFareDetails promoInfo;

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		boolean isCabinClassOnholdEnable = false;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
			Locale locale = new Locale(SessionUtil.getLanguage(request));
			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(getSearchParams(), AppIndicatorEnum.APP_IBE);

			if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
					&& !tempPaymentPNR.trim().isEmpty()) {
				messageTxt = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
				success = false;
				return result;
			}

			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			if (resInfo != null && resInfo.isFromPostCardDetails()) {
				fromPostCardPage = resInfo.isFromPostCardDetails();
				msgFromPostCardPage = I18NUtil.getMessage("msg.postCardPage", strLanguage);
			}

			String searchSystem = getSearchParams().getSearchSystem();
			SYSTEM system = SYSTEM.getEnum(searchSystem);
			if (system == SYSTEM.NONE) {
				throw new ModuleRuntimeException("Invalid Seach System :" + searchSystem);
			}

			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());
			flightPriceRQ.getAvailPreferences().setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
			flightPriceRQ.getAvailPreferences().setQuoteFares(true);
			flightPriceRQ.getAvailPreferences().setMultiCitySearch(true);

			if (groupPNR) {
				flightPriceRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}
			AnalyticsLogger.logAvlSearch(AnalyticSource.IBE_MCS_FQ, flightPriceRQ, request, getTrackInfo());
			FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ,
					getTrackInfo());

			PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();
			resInfo.setPriceInfoTO(priceInfoTO);
			resInfo.addNewServiceTaxes(flightPriceRS.getApplicableServiceTaxes());
			resInfo.setTransactionId(flightPriceRS.getTransactionIdentifier());

			String accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);

			// TODO Re-factor
			if ((!CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint))) {
				isCabinClassOnholdEnable = ReservationUtil.isReservationCabinClassOnholdable(flightPriceRS);
				resInfo.setAllowOnhold(isCabinClassOnholdEnable);

				if (isCabinClassOnholdEnable && AppSysParamsUtil.isOnHoldEnable(OnHold.IBE)) {
					boolean reservationOtherValidationOnholdable = ReservationUtil.isReservationOtherValidationOnholdable(
							flightPriceRS, system, getTrackInfo(), getClientInfoDTO().getIpAddress());
					resInfo.setReservationOnholdableOtherValidation(reservationOtherValidationOnholdable);

					allowOnhold = isCabinClassOnholdEnable && reservationOtherValidationOnholdable;

					if (allowOnhold) {
						for (OriginDestinationInformationTO ondInfo : flightPriceRS.getOriginDestinationInformationList()) {
							Date depTimeZulu = BeanUtil.getSelectedFlightDepartureDate(ondInfo.getOrignDestinationOptions());
							boolean ohdEnabled = (depTimeZulu != null) ? ReservationUtil.isOnHoldPaymentEnable(OnHold.IBE,
									depTimeZulu) : false;
							if (!(allowOnhold && ohdEnabled)) {
								allowOnhold = allowOnhold && ohdEnabled;
								break;
							}
							allowOnhold = allowOnhold && ohdEnabled;
						}
					}
				}
			}

			if (priceInfoTO != null) {

				flightInfo = BeanUtil.createAvailFlightInfoList(flightPriceRS, false, locale);

				List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
				ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);

				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

				resInfo.setPriceInfoTO(priceInfoTO);

				// Set promotion discount information if any
				resInfo.setFareDiscount(flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
				if (resInfo.getDiscountInfo() != null
						&& !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(resInfo.getDiscountInfo()
								.getPromotionType())) {

					ReservationUtil.calculateDiscountForReservation(resInfo, null, flightPriceRQ, getTrackInfo(), true,
							resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(), flightPriceRS.getTransactionIdentifier(), false);

				}			
				
				promoInfo = resInfo.getDiscountInfo();

				fareQuote = BeanUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(),
						flightPriceRS.getOriginDestinationInformationList(), BigDecimal.ZERO, getSearchParams(),
						resInfo.getDiscountAmount(false), exchangeRateProxy, flightInfo);

				resInfo.setTotalPriceWithoutExtChg(BeanUtil.getTotalPriceWithoutExtCharges(priceInfoTO));
				resInfo.setTotalFare(fareQuote.getTotalFare());
				resInfo.setTotalTax(fareQuote.getTotalTax());
				resInfo.setTotalTaxSurchages(fareQuote.getTotalTaxSurcharge());

				// Request Session Identifier
				requestSessionIdentifier = (new Date().getTime()) + "";
				SessionUtil.setRequestSessionIdentifier(request, requestSessionIdentifier);

				List<FareRuleDTO> ondFareRules = BeanUtil.getFareRules(flightPriceRS.getSelectedPriceFlightInfo(),
						flightPriceRS.getOriginDestinationInformationList());
				GlobalConfig globalConfig = new GlobalConfig();
				Map<String, FareCategoryTO> sortedFareCategoryTOMap = globalConfig.getFareCategories();
				fareRules = new ArrayList<FareRuleDisplayTO>();

				for (FareRuleDTO fareRule : ondFareRules) {
					FareCategoryTO fareCategoryTO = sortedFareCategoryTOMap.get(fareRule.getFareCategoryCode());
					if (fareCategoryTO != null && fareCategoryTO.getStatus().equals(FareCategoryStatusCodes.ACT)) {
						showFareRules = (showFareRules || fareCategoryTO.getShowComments());
						if (fareCategoryTO.getShowComments()) {
							FareRuleDisplayTO frDis = new FareRuleDisplayTO();
							frDis.setOndCode(fareRule.getOrignNDest());
							frDis.setComment(fareRule.getComments());
							fareRules.add(frDis);
						}
					}
				}

				resInfo.setBaggageSummaryTo(AncillaryConverterUtil.toLccReservationBaggageSummaryTo(flightPriceRS));

				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());

			} else {
				forceRefresh = true;
			}

			// Load Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightPriceRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.IBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, SessionUtil.getLanguage(request));

		} catch (ModuleException me) {
			log.error(me.getModuleCode(), me);
			success = false;
			log.error("MultiCityFareQuoteAction ==>", me);
			if (me.getExceptionCode().equals("ibe.fare.quote.flights.not.matching")) {
				messageTxt = "Returned flights not matching with fare quoted flights";
			} else if (me.getExceptionCode().equals("err.58-maxico.exposed.cannot.mix.agreements.in.onefare.quote")) {
				messageTxt = MessagesUtil.getMessage(me.getExceptionCode());
			} else if (me.getExceptionCode().equals("airinventory.arg.farequote.flightclosed")) {
				forceRefresh = true;
				messageTxt = "flightsHaveChanged";
			} else {
				messageTxt = me.getMessage();
			}

		} catch (Exception ex) {
			log.error("MultiCityFareQuoteAction ==>", ex);
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
		}
		return result;
	}

	public boolean isForceRefresh() {
		return forceRefresh;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public boolean isShowFareRules() {
		return showFareRules;
	}

	public List<FareRuleDisplayTO> getFareRules() {
		return fareRules;
	}

	public BigDecimal getInboundCalFare() {
		return inboundCalFare;
	}

	public BigDecimal getOutboundCalFare() {
		return outboundCalFare;
	}

	public boolean isFromPostCardPage() {
		return fromPostCardPage;
	}

	public String getMsgFromPostCardPage() {
		return msgFromPostCardPage;
	}

	public String getRequestSessionIdentifier() {
		return requestSessionIdentifier;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	public Collection<AvailableFlightInfo> getFlightInfo() {
		return flightInfo;
	}

	public void setFlightInfo(Collection<AvailableFlightInfo> flightInfo) {
		this.flightInfo = flightInfo;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	public DiscountedFareDetails getPromoInfo() {
		return promoInfo;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

}
