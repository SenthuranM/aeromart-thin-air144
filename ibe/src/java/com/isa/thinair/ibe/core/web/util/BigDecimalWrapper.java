package com.isa.thinair.ibe.core.web.util;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

class BigDecimalWrapper {

	private BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();

	public BigDecimal getValue() {
		return value;
	}

	protected void add(BigDecimal addition) {
		value = AccelAeroCalculator.add(value, addition);
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getDisplayValue() {
		return AccelAeroCalculator.formatAsDecimal(this.value);
	}
}
