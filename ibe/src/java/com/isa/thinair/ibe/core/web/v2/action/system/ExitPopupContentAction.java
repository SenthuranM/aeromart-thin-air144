package com.isa.thinair.ibe.core.web.v2.action.system;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.ExitPopupUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * This action is handling localized content for IBE exitpopup
 * 
 * @author Eshan
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ExitPopupContentAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ExitPopupContentAction.class);

	private boolean success = true;

	private Map<String, String> exitPopupContent = new HashMap<String, String>();
	private boolean isCustomerLoggedIn;
	private int maxPopupTimes;

	public String execute() {
		log.debug("loading exitpopup content");
		setExitPopupContent(ExitPopupUtil.getExitPopupContent(request, exitPopupContent));
		this.setMaxPopupTimes(AppSysParamsUtil.getMaximumNumberOfExitPopupDisplayTimesPerPage());
		Customer customerModel;
		try {
			customerModel = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			if (customerModel != null) {
				isCustomerLoggedIn = true;
			} else {
				isCustomerLoggedIn = false;
			}
		} catch (ModuleException e) {
			log.error("error while obtaining logged in customer");
		}
		return StrutsConstants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Map<String, String> getExitPopupContent() {
		return exitPopupContent;
	}

	public void setExitPopupContent(Map<String, String> exitPopupContent) {
		this.exitPopupContent = exitPopupContent;
	}

	public boolean isCustomerLoggedIn() {
		return isCustomerLoggedIn;
	}

	public void setCustomerLoggedIn(boolean isCustomerLoggedIn) {
		this.isCustomerLoggedIn = isCustomerLoggedIn;
	}

	public int getMaxPopupTimes() {
		return maxPopupTimes;
	}

	public void setMaxPopupTimes(int maxPopupTimes) {
		this.maxPopupTimes = maxPopupTimes;
	}

}