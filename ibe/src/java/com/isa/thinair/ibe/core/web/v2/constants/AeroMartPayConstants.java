package com.isa.thinair.ibe.core.web.v2.constants;

import java.util.Arrays;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AeroMartPayConstants {

	// FIXME input params hard-coded

	public final static List<String> INT_EXT_CARD_INPUT = Arrays.asList(CardData.CARD_NUMBER.getInputName(),
			CardData.CVV.getInputName(), CardData.EXPIRY_DATE.getInputName(), CardData.CARD_HOLDER_NAME.getInputName(),
			CardData.DESCRIPTION.getInputName());

	public final static List<String> EXT_CARD_INPUT = Arrays.asList(CardData.DESCRIPTION.getInputName());

	public final static String COMMON_DESCRIPTION = "The next screen you view may be from your bank. "
			+ "Please do not Press [Back] or [Forward] buttons in your browser while the transaction takes place. "
			+ "If you encounter an error during the process, please try with another browser (Mozilla Firefox, Google Chrome, Internet Explorer...)."
			+ " Make sure the transaction is complete and you are re-directed to the page"
			+ " after payment to obtain your flight reservation number";

	public enum CardBehavior {

		INT_EXT("internal-external"), EXT("external");

		private String cardBehavior;

		private CardBehavior(String cardBehavior) {
			this.cardBehavior = cardBehavior;
		}

		public String getCardBehavior() {
			return cardBehavior;
		}

		@Override
		public String toString() {
			return cardBehavior;
		}

	}

	public enum MerchantStatus {
		INA("INA"), ACT("ACT");

		private String status;

		MerchantStatus(String status) {
			this.status = status;
		}

		public String getStatus() {
			return this.status;
		}

	}

	public enum IPGProviderCode {
		KNET(
				"KNET",
				"Redirecting to an external payment gateway, Do not perform [Close the browser], [Refresh], [Back] & [Next] actions, until payment success."),

		CCAVENUE(
				"CCAvenue",
				"Redirecting to an external payment gateway, Do not perform [Close the browser], [Refresh], [Back] & [Next] actions, until payment success."),

		EDIRHAM(
				"EDIRHAM",
				"Redirecting to an external payment gateway, Do not perform [Close the browser], [Refresh], [Back] & [Next] actions, until payment success."),

		BEHPARDAKHT(
				"BEHPARDAKHT",
				"Redirecting to an external payment gateway, Do not perform [Close the browser], [Refresh], [Back] & [Next] actions, until payment success."),

		COMMON("COMMON", COMMON_DESCRIPTION);

		private String ipgProviderCode;
		private String description;

		IPGProviderCode(String status, String statusMessage) {
			this.ipgProviderCode = status;
			this.description = statusMessage;

		}

		public String getIpgProviderCode() {
			return this.ipgProviderCode;

		}

		public String getDescription() {
			return this.description;
		}

	}

	public enum AeroMartPayStatus {
		PAYMENT_COMPLETED("RS", "Completed"), PAYMENT_FAILED("PF", "Failed"), PAYMENT_INITIATED("I", "Initiated"), PAYMENT_REFUNDED(
				"IS", "Refunded"), PAYMENT_REFUND_FAILED("II", "Failed"), MULTIPLE_RECORDS_INVALID("", "Invalid"), UNDEFINED("",
				"Undefined"), NOT_FOUND("", "NotFound"), MULTIPLE_ATTEMPTS_INVALID("", "Invalid");

		private String status;
		private String statusMessage;

		AeroMartPayStatus(String status, String statusMessage) {
			this.status = status;
			this.statusMessage = statusMessage;

		}

		public String getStatus() {
			return this.status;

		}

		public String getStatusMessage() {
			return this.statusMessage;
		}

	}

	public enum CardData {

		CVV("cardCVV"), CARD_NUMBER("cardNumber"), EXPIRY_DATE("cardExpiry"), CARD_HOLDER_NAME("cardHolderName"), DESCRIPTION(
				"description"), MOBILE("mobile"), EMAIL("email");

		private String inputName;

		private CardData(String inputName) {
			this.inputName = inputName;
		}

		public String getInputName() {
			return inputName;
		}

		@Override
		public String toString() {
			return inputName;
		}

	}

	public static final String PARAM_AGENT_TYPE = "agentType";
	
	public static final String PARAM_MERCHANT_ID = "merchantID";
	public static final String PARAM_USERNAME = "userName";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_HASH_STRING = "hashString";
	public static final String PARAM_RETURN_URL = "returnURL";
	public static final String PARAM_ERROR_URL = "errorURL";

	public static final String PARAM_ORDER_ID = "orderID";
	public static final String PARAM_TOTAL_AMOUNT = "totalAmount";
	public static final String PARAM_CURRENCY_CODE = "currencyCode";

	public static final String PARAM_REFERENCE_ID = "referenceID";
	public static final String PARAM_RESPONSE_CODE = "responseCode";
	public static final String PARAM_RESPONSE_MESSAGE = "responseMessage";

	public static final String HANDLE_IPG_PAYMENT = "handleIPGPaymentURL";
	public static final String PAYMENT_BROKER_REF_NO = "paymentBrokerRefNo";

	public static final String PAYMENT_GATEWAY_ID = "paymentGatewayID";

	public static final String RESPONSE_RECEIVED = "responseReceived";

	public static final String PARAM_NAME = "name";
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_MOBILE = "mobile";
	public static final String PARAM_LANGUAGE_CODE = "languageCode";
	public static final String PARAM_GENDER = "gender";
	public static final String PARAM_DOB = "dob";
	public static final String PARAM_AREA = "area";
	public static final String PARAM_BLOCK = "block";
	public static final String PARAM_STREET = "street";
	public static final String PARAM_AVENUE = "avenue";
	public static final String PARAM_BUILDING = "building";
	public static final String PARAM_FLOOR = "floor";
	public static final String PARAM_APARTMENT = "apartment";

	public static final String BANKS_3D_SECURE_HTML_PAGE = "banks3DSecureHtml";
	public static final String DEFAULT_LANGUAGE_CODE = "en";
	public static final String LOAD_PAYMENT_OPTIONS = "loadPaymentOptions";
	public static final String LOAD_PAYMENT_OPTIONS_URL = AppSysParamsUtil.getSecureIBEUrl() + "loadAeroMartPayOptions.action";
	public static final String HANDLE_PAYMENT_URL = AppSysParamsUtil.getSecureIBEUrl() + "handleAeroMartPay!handlePayment.action";
	public static final char PRODUCT_TYPE = 'G';
	public static final String PAYMENT_OPTIONS = "paymentOptions";
	public static final String IPG_PAYMENT_RESPONSE_URL = AppSysParamsUtil.getSecureIBEUrl()
			+ "handleAeroMartPayIPGResponse.action";
	public static final String USER_INPUT_PG_ID = "paymentgatewayId";
	public static final String USER_INPUT_CARD_TYPE = "cardType";
	public static final String USER_INPUT_CARD_NUMBER = "cardNumber";
	public static final String USER_INPUT_CARDHOLDER_NAME = "cardHolderName";
	public static final String USER_INPUT_CVV = "cvv";
	public static final String USER_INPUT_EXPIRY = "expiryDate";
	public static final String SESSION_INFO = "aeroMartSessionInfo";
	public static final String MESSAGES = "messages";
	public static final String USER_INPUT_CARD_BEHAVIOR = "cardBehavior";
	public static final String DATE_FORMAT = "MMyy";
	public static final String PAYMENT_OPTIONS_SESSION_LOCK = "PaymentOptionsSessionLock";

	public static final String RETRAILS = "PaymentRetrails";

	public static final String MESSAGE = "message";

	public static final String MODULE_CODE_IBE = "IBE";
	public static final String MODULE_CODE_XBE = "XBE";
	public static final String PAYMENT_GATEWAY = "PAYMENT_GATEWAY";

	public static final String ALL_COUNTRIES = "OT";

	public static final String POST = "POST";

	public static final String HMAC_ALGO = "HmacSHA256";

	public static final String FORM_STRING = "formString";
	public static final String FORM_NAME = "formName";
	public static final String PAYMENT_FORM_NAME = "paymentForm";

	public static final String REGEX_ALPHA_NUMERIC = "^[a-zA-Z0-9]*$";
	public static final String REGEX_CARDHOLDER_NAME = "^[a-zA-Z][a-zA-Z ]*[a-zA-Z]$";
	public static final String REGEX_NUMERIC = "\\d+";

	public static final String PASSWORD_KEY = "*Y&v4&#@";
	public static final String PASSWORD_IV = "9J%g#81)";
	
	public static final String AGENT = "AGENT";
	public static final String PUBLIC = "PUBLIC";

	public static enum AeroMartPayResponse {
		SUCCESS("00", "Successful"),

		ERR_01("01", "Invalid Merchant ID"), ERR_02("02", "Invalid Username"), ERR_03("03", "Invalid password"), ERR_04("04",
				"Invalid Order ID"), ERR_05("05", "Invalid Total Amount"), ERR_06("06", "Invalid currency code"), ERR_07("07",
				"Invalid Hash String"), ERR_08("08", "Hash String not matching"), ERR_09("09", "Invalid orderID length"), ERR_10(
				"10", "Not a POST request"), ERR_11("11", "orderID not alphanumeric"), ERR_12("12",
				"System currency conversion failed"), ERR_13("13", "Request Validation Failed"), ERR_14("14",
				"Merchant Account Not Activated"),

		ERR_49("49", "Payment request invalid"), ERR_50("50", "orderID not unique"), ERR_51("51", "referenceID not generated"), ERR_52(
				"52", "No payment options available for the specified currency"), ERR_53("53", "Transaction Initiation Failed"), ERR_54(
				"54", "Unexpected error while initiating the transaction"), ERR_55("55", "Error while loading payment options"), ERR_56(
				"56", "Error while Payment Option loading"), ERR_57("57", "Error while processing payment query"), ERR_58("58",
				"Payment option not supported"),

		ERR_80("80", "Payment request invalid"), ERR_81("81", "Error while processing the payment"), ERR_82("82",
				"Conversion Error"), ERR_83("83", "Unexpected error while procssing the payment"),

		ERR_101("101", "Payment Receipt Empty"), ERR_102("102", "Payment Failed"), ERR_103("103", "Payment Processing Failed"), ERR_104(
				"104", "Operation Failed"), ERR_105("105", "Invalid Session"), ERR_106("106", "Multiple payment responses"), ERR_107(
				"107", "Session already used, try with a different browser"), ERR_108("108", "Multiple user input submission"),

		ERR_201("201", "Payment attempts limit exceeded");
		String respCode;
		String respMessage;

		private AeroMartPayResponse(String respCode, String respMessage) {
			this.respCode = respCode;
			this.respMessage = respMessage;
		}

		public String getRespCode() {
			return this.respCode;
		}

		public String getRespMessage() {
			return this.respMessage;
		}
	}

}
