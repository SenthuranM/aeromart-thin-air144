package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

/**
 * Action class for blocking anci
 * 
 * @author Dilan Anuruddha
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciBlockingAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(InterlineAnciBlockingAction.class);

	private boolean success = true;

	private String messageTxt;

	private String selectedSeats;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {

			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			List<LCCSegmentSeatDTO> blockSeatDTOs = AncillaryJSONUtil.getBlockSeatDTOs(selectedSeats);

			if (blockSeatDTOs.size() > 0) {
				if (ibeResInfo.getBlockedSeatJson() != null) {
					List<LCCSegmentSeatDTO> prevBlkedSeats = AncillaryJSONUtil.getBlockSeatDTOs(ibeResInfo.getBlockedSeatJson());
					ModuleServiceLocator.getAirproxyAncillaryBD().releaseSeats(prevBlkedSeats, ibeResInfo.getTransactionId(),
							ibeResInfo.getSelectedSystem(), TrackInfoUtil.getBasicTrackInfo(request));
					ibeResInfo.setBlockedSeatJson(null);
				}
				success = ModuleServiceLocator.getAirproxyAncillaryBD().blockSeats(blockSeatDTOs, ibeResInfo.getTransactionId(),
						ibeResInfo.getSelectedSystem(), TrackInfoUtil.getBasicTrackInfo(request));
				if (success) {
					ibeResInfo.setBlockedSeatJson(selectedSeats);
				}
			}
		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("ANCI BLOCKING ERROR", e);
		}
		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getSelectedSeats() {
		return selectedSeats;
	}

	public void setSelectedSeats(String selectedSeats) {
		this.selectedSeats = selectedSeats;
	}

}
