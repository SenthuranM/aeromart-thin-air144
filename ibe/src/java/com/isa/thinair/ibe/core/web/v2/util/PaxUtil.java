package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.PaxValidationTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

/**
 * Pax Utility
 * 
 * @author Thushara
 * @since Dec 26, 2009
 */
public class PaxUtil {

	public static PaxValidationTO fillPaxValidation() {
		PaxValidationTO paxValidationTO = new PaxValidationTO();
		GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();

		paxValidationTO.setChildAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears());
		paxValidationTO.setAdultAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears());
		paxValidationTO.setInfantAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getCutOffAgeInYears());

		paxValidationTO.setInfantAgeCutLowerBoundaryDays(globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT)
				.getAgeLowerBoundaryInDays());
		paxValidationTO.setInfantAgeCutLowerBoundaryMonths(globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT)
				.getAgeLowerBoundaryInMonths());

		paxValidationTO.setAdultsPassportUniqueOnly(AppSysParamsUtil.isCheckDuplicatePassportNumberOnlyWithinAdults());

		String carrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		paxValidationTO.setPhoneValidation(true);

		return paxValidationTO;
	}

	public static void populatePaxCatConfig(List allConfigList, String paxType, Map<String, IBEPaxConfigDTO> configAD,
			Map<String, IBEPaxConfigDTO> configCH, Map<String, IBEPaxConfigDTO> configIN) {
		for (Object obj : allConfigList) {
			if (obj instanceof IBEPaxConfigDTO) {
				IBEPaxConfigDTO config = (IBEPaxConfigDTO) obj;
				if (config.getPaxCatCode().equals(paxType)) {
					if (ReservationInternalConstants.PassengerType.ADULT.equals(config.getPaxTypeCode())) {
						configAD.put(config.getFieldName(), config);
					} else if (ReservationInternalConstants.PassengerType.CHILD.equals(config.getPaxTypeCode())) {
						configCH.put(config.getFieldName(), config);
					} else if (ReservationInternalConstants.PassengerType.INFANT.equals(config.getPaxTypeCode())) {
						configIN.put(config.getFieldName(), config);
					}
				}
			}
		}
	}

	public static void populateCountryWisePaxConfigs(Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs,
			Map<String, Boolean> countryPaxConfigAD, Map<String, Boolean> countryPaxConfigCH,
			Map<String, Boolean> countryPaxConfigIN, String paxType) {

		for (List<PaxCountryConfigDTO> paxCountryConfigs : countryWiseConfigs.values()) {
			for (PaxCountryConfigDTO paxCountryConfig : paxCountryConfigs) {
				if (paxCountryConfig.getPaxCatCode().equals(paxType)) {
					if (ReservationInternalConstants.PassengerType.ADULT.equals(paxCountryConfig.getPaxTypeCode())) {
						fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigAD);
					} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxCountryConfig.getPaxTypeCode())) {
						fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigCH);
					} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxCountryConfig.getPaxTypeCode())) {
						fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigIN);
					}
				}
			}
		}
	}

	private static void fillPaxCountryConfigMap(PaxCountryConfigDTO paxCountryConfig, Map<String, Boolean> countryPaxConfig) {
		String fieldName = paxCountryConfig.getFieldName();
		if (countryPaxConfig.get(fieldName) == null) {
			countryPaxConfig.put(fieldName, paxCountryConfig.isMandatory());
		} else {
			Boolean currentValue = countryPaxConfig.get(fieldName);
			countryPaxConfig.put(fieldName, (currentValue || paxCountryConfig.isMandatory()));
		}
	}

	public static void populateContactConfig(List configList, Map<String, IBEContactConfigDTO> configMap,
			Map<String, List<String>> validationGroup) {
		for (Object obj : configList) {
			if (obj instanceof IBEContactConfigDTO) {
				IBEContactConfigDTO ibeConfig = (IBEContactConfigDTO) obj;
				configMap.put(ibeConfig.getFieldName() + ibeConfig.getGroupId(), ibeConfig);
				createContactGroupMap(ibeConfig, validationGroup);

			}
		}
	}

	private static void createContactGroupMap(IBEContactConfigDTO config, Map<String, List<String>> map) {
		List<String> fieldList = new ArrayList<String>();
		if (config.getValidationGroup() != null && !config.getValidationGroup().equals("")) {
			if (map.containsKey(config.getValidationGroup())) {
				fieldList = map.get(config.getValidationGroup());
			}

			fieldList.add(config.getFieldName() + config.getGroupId());
			map.put(config.getValidationGroup(), fieldList);
		}
	}

	public static void populateUserRegConfig(List<UserRegConfigDTO> configList, Map<String, UserRegConfigDTO> configMap,
			Map<String, List<String>> validationGroup) {
		for (UserRegConfigDTO userRegConfigDTO : configList) {
			configMap.put(userRegConfigDTO.getFieldName() + userRegConfigDTO.getGroupId(), userRegConfigDTO);
			createUserRegGroupMap(userRegConfigDTO, validationGroup);
		}
	}

	private static void createUserRegGroupMap(UserRegConfigDTO userConfig, Map<String, List<String>> map) {
		List<String> fieldList = new ArrayList<String>();
		if (userConfig.getValidationGroup() != null && !userConfig.getValidationGroup().equals("")) {
			if (map.containsKey(userConfig.getValidationGroup())) {
				fieldList = map.get(userConfig.getValidationGroup());
			}

			fieldList.add(userConfig.getFieldName() + userConfig.getGroupId());
			map.put(userConfig.getValidationGroup(), fieldList);
		}
	}

	public static int getPayablePaxCount(Collection<ReservationPaxTO> paxList, boolean update) {
		int payablePaxCount = 0;
		for (ReservationPaxTO pax : paxList) {
			if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
				if (update) {
					if (pax.getAncillaryTotal(true).compareTo(BigDecimal.ZERO) > 0) {
						payablePaxCount++;
					}
				} else {
					payablePaxCount++;
				}
			}
		}
		return payablePaxCount;
	}

}