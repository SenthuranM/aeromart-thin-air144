package com.isa.thinair.ibe.core.web.v2.action.payment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AeroMartPayQueryDTO;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayResponse;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayStatus;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayUtil;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidator;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidatorImpl;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class HandleAeroMartPayQueryAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(HandleAeroMartPayQueryAction.class);

	private String orderID;
	private String status;
	private String hashString;
	private String referenceID = "";
	private String currencyCode = "";
	private String totalAmount = "";

	public String execute() {

		if (AeroMartPayUtil.isPost(request)) {

			AeroMartPayResponse aeroMartPayResponse = null;
			Map<String, String> requestParams = AeroMartPayUtil.setQueryRequestParamMap(request);
			String merchantID = requestParams.get(AeroMartPayConstants.PARAM_MERCHANT_ID);
			String username = requestParams.get(AeroMartPayConstants.PARAM_USERNAME);
			String password = requestParams.get(AeroMartPayConstants.PARAM_PASSWORD);

			try {

				AeroMartPayValidator aeroMartPayValidator = new AeroMartPayValidatorImpl();

				aeroMartPayResponse = aeroMartPayValidator.validateQueryRequest(requestParams);

				if (aeroMartPayResponse != null) {
					throw new ModuleException(aeroMartPayResponse.getRespMessage());
				}

				this.orderID = requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID);

				AeroMartPayQueryDTO aeroMartPayQueryDTO = AeroMartPayUtil.getPaymentDetails(this.orderID);

				if (aeroMartPayQueryDTO != null) {

					this.status = aeroMartPayQueryDTO.getStatus();

					if (!StringUtil.isNullOrEmpty(aeroMartPayQueryDTO.getReferenceID())) {
						this.referenceID = aeroMartPayQueryDTO.getReferenceID();
					}
					if (!StringUtil.isNullOrEmpty(aeroMartPayQueryDTO.getPaymentCurrencyCode())) {
						this.currencyCode = aeroMartPayQueryDTO.getPaymentCurrencyCode();
					}

					if (!StringUtil.isNullOrEmpty(aeroMartPayQueryDTO.getPaymentCurrencyAmount())) {

						this.totalAmount = aeroMartPayQueryDTO.getPaymentCurrencyAmount();
					}

				} else {
					this.status = AeroMartPayStatus.NOT_FOUND.getStatusMessage();
				}

			} catch (Exception e) {

				if (aeroMartPayResponse == null) {
					aeroMartPayResponse = AeroMartPayResponse.ERR_57;
				}

				log.error(aeroMartPayResponse.getRespMessage(), e);

				this.status = aeroMartPayResponse.getRespMessage();
			}

			Merchant merchant = AeroMartPayUtil.getMerchant(merchantID);
			List<String> responseParams = new ArrayList<String>();
			responseParams.add(merchantID);
			responseParams.add(username);
			responseParams.add(password);
			responseParams.add(status);
			responseParams.add(orderID);
			responseParams.add(totalAmount);
			responseParams.add(currencyCode);
			responseParams.add(referenceID);

			this.hashString = AeroMartPayUtil.buildHashString(responseParams, merchant.getSecureHashKey());

		} else {
			this.status = AeroMartPayResponse.ERR_10.getRespMessage();

		}

		return StrutsConstants.Result.SUCCESS;

	}

	public static Log getLog() {
		return log;
	}

	public static void setLog(Log log) {
		HandleAeroMartPayQueryAction.log = log;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHashString() {
		return hashString;
	}

	public void setHashString(String hashString) {
		this.hashString = hashString;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

}
