package com.isa.thinair.ibe.core.web.v2.action.system;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * Generic Action for Client Log
 * 
 * @author pkarunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class IBEClientLogAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(IBEClientLogAction.class);

	private boolean success = true;

	private String messageTxt;

	public String updateUserReceivedItineary() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			String pnr = SessionUtil.getTempPaymentPNR(request);

			SessionUtil.setTempPaymentPNR(request, null);
			SessionUtil.setResponseReceivedFromPaymentGateway(request, false);

			StringBuilder logMessage = new StringBuilder();
			logMessage.append("User Received itineary successfully");
			logMessage.append("|timestamp=");
			logMessage.append(CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date()));
			logMessage.append("|sessId=");
			logMessage.append(request.getRequestedSessionId());
			logMessage.append("|xFwdFor=");
			logMessage.append(CommonUtil.getNullSafeValue(CommonUtil.getHTTPHeaderValue(request, "X-Forwarded-For")));
			logMessage.append("|pnr=");
			logMessage.append(pnr);

			if (log.isDebugEnabled()) {
				log.debug(logMessage.toString());
			}

		} catch (Exception ex) {
			log.error(ex);
		}

		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}
}
