package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.dto.lms.LMSMemberDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.api.dto.PassengerDTO;
import com.isa.thinair.ibe.api.dto.PaxDetailDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.wsclient.api.util.LMSConstants;

/**
 * @author chethiya
 *
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
	@Result(name = StrutsConstants.Result.BYPASS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Promotion.LMS_CONFIRMATION_PAGE),
	//@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
	@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class LmsMemberConfirmationAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(LmsMemberConfirmationAction.class);

	private String ffid;

	private String validationString;
	
	private String message;
	
	private String status;     //	Merged-"M" , Created-"C" , Invalid or expired-I 
	
	private boolean validationStatus;
	
	private PaxDetailDTO paxDetail;
	
	private String paxDetailJson;
	
	private LmsMemberDTO lmsDetails;

	private String originCarrier;

	private boolean ffidAvailability;

	private boolean nameMatch;
	
	private boolean linkValid = true;

	public String execute() {
		
		setCommonParameters(this.getCommonParams());	


		String forward = StrutsConstants.Result.SUCCESS;
		if(AppSysParamsUtil.isLMSEnabled()){	
			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			try {
				
				int merges = lmsMemberDelegate.numberOfMerges(validationString, ffid);
				LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
	
				if (merges > 0) {
					AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
					// TODO:CP move business logic to aircustomer
					Customer customer = customerDelegate.getCustomer(ffid);
					LmsMember lmsMember = customer.getLMSMemberDetails();
					if (lmsMember != null) {
						LoyaltyMemberCoreDTO existingMember = lmsManagementBD.getLoyaltyMemberCoreDetails(lmsMember.getFfid());
						if (existingMember.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
							setMessage("Your AirRewards account has been successfully created");
							setStatus("C");
							String memberExternalId = lmsManagementBD.createLoyaltyMember(customer);
	
							lmsMember = customer.getLMSMemberDetails();
							//lmsMember.setSbInternalId(sbInternalId);
							lmsMember.setMemberExternalId(memberExternalId);
							lmsMember.setEmailConfirmed('Y');
							lmsMemberDelegate.saveOrUpdate(lmsMember);
							customerDelegate.confirmLMSMemberStatus(customer.getEmailId());
							lmsMemberDelegate.mergeLmsMember(validationString, ffid);
						} else {
	
							LMSMemberDTO lmsMemberDTO = ModuleServiceLocator.getLoyaltyManagementBD().getMergeLoyaltyMember(ffid,
									getTrackInfo());
							lmsMember = lmsMemberDelegate.getLmsMember(ffid);
							if (lmsMemberDTO != null) {
								if (!(lmsMemberDTO.getFirstName().toLowerCase().equals(lmsMember.getFirstName().toLowerCase()))
										|| !(lmsMemberDTO.getLastName().toLowerCase().equals(lmsMember.getLastName()
												.toLowerCase()))) {
									log.info("LmsMemberConfirmationAction ==> Names are not matched");
									setMessage("The Airewards account cannot be merged since the names are not matched.");
									setStatus("I");
								} else if (lmsMemberDTO != null && lmsMember != null) {
									setMessage("Your AirRewards account has been successfully merged");
									setStatus("M");
									lmsMember.setSbInternalId(lmsMemberDTO.getSbInternalId());
									lmsMember.setMemberExternalId(lmsMemberDTO.getMemberExternalId());
									lmsMember.setEnrollingCarrier(lmsMemberDTO.getEnrollingCarrier());
									lmsMember.setEnrollingChannelExtRef(lmsMemberDTO.getEnrollingChannelExtRef());
									lmsMember.setEnrollingChannelIntRef(lmsMemberDTO.getEnrollingChannelIntRef());
									lmsMember.setEmailConfirmed('Y');
									lmsMember.setPassword(lmsMemberDTO.getPassword());
									lmsMemberDelegate.saveOrUpdate(lmsMember);
									if (lmsMemberDTO.getConfirmed() == 'Y') {
										LmsCommonUtil.sendLMSMergeSuccessEmail(lmsMember, originCarrier, lmsMemberDelegate);
									}
									customerDelegate.confirmLMSMemberStatus(customer.getEmailId());
									lmsMemberDelegate.mergeLmsMember(validationString, ffid);
								}

							} else if (lmsMemberDTO == null && existingMember != null) {
								if ((lmsMember.getFirstName().toLowerCase().equals(existingMember.getMemberFirstName()
										.toLowerCase()))
										|| (lmsMember.getLastName().toLowerCase().equals(existingMember.getMemberLastName()
												.toLowerCase()))) {
									setMessage("Your AirRewards account has been successfully merged");
									setStatus("M");
									lmsMember.setSbInternalId(existingMember.getSBInternalMemberId());
									lmsMember.setMemberExternalId(existingMember.getMemberExternalId());
									lmsMember.setEmailConfirmed('Y');
									lmsMemberDelegate.saveOrUpdate(lmsMember);
									lmsMember.setEnrollingCarrier(lmsMemberDTO.getEnrollingCarrier());
									lmsMember.setEnrollingChannelExtRef(lmsMemberDTO.getEnrollingChannelExtRef());
									lmsMember.setEnrollingChannelIntRef(lmsMemberDTO.getEnrollingChannelIntRef());
									customerDelegate.confirmLMSMemberStatus(customer.getEmailId());
									lmsMemberDelegate.mergeLmsMember(validationString, ffid);
								} else {
									log.info("LmsMemberConfirmationAction ==> Names are not matched");
									setMessage("The Airewards account cannot be created. Already registered with different first name and last name.");
									setStatus("I");
								}
							} else {
								linkValid = false;
							}
	
						}
					} else {
						linkValid = false;
					}
				} else {
					linkValid = false;
				}
	
			} catch (ModuleException me) {
				forward = StrutsConstants.Result.ERROR;
				log.error("LmsMemberConfirmationAction==>", me);
			}
		}

		if (!linkValid) {
			setLinkInvalid();
		}

		return forward;
	}

	private void setCommonParameters(IBECommonDTO params) {
		
		if (SessionUtil.getLanguage(request) != null) {
			params.setLocale(SessionUtil.getLanguage(request));
		}
		params.setNonSecurePath(AppParamUtil.getNonsecureIBEUrl());
		params.setSecurePath(AppParamUtil.getSecureIBEUrl());
	}

	/**
	 *	validating the FFID at the pax info page
	 * @throws ParseException 
	 */

	
	public String validateFFID() throws ModuleException, ParseException{
		if (AppSysParamsUtil.isLMSEnabled()){
			this.setPaxDetail(extractPaxDetails(paxDetailJson));		
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			LmsMemberServiceBD lmsMemberServiceBD = ModuleServiceLocator.getLmsMemberServiceBD();
			Collection<PassengerDTO> adults = paxDetail.getAdultList();
			for (PassengerDTO adult : adults) {
				if(LmsCommonUtil.validateFFIDEmail(adult.getFfid())){
					if (LmsCommonUtil.isNonMandAccountAvailable(adult.getFfid(), loyaltyManagementBD)
							&& LmsCommonUtil.availableForCarrier(adult.getFfid(), lmsMemberServiceBD)) {
						adult.setValidFFID(true);
						LoyaltyMemberCoreDTO member = LmsCommonUtil.getPassenger(adult.getFfid(), loyaltyManagementBD);
						adult.setFirstName(member.getMemberFirstName());
						adult.setLastName(member.getMemberLastName());
						adult.setDateOfBirth(member.getMemberBirthdateDay()+"/"+member.getMemberBirthdateMonth()+"/"+member.getMemberBirthdateYear());
					}
				}
			}
			
			Collection<PassengerDTO> children = paxDetail.getChildList();
			for (PassengerDTO child : children) {
				if(LmsCommonUtil.validateFFIDEmail(child.getFfid())){
					if (LmsCommonUtil.isNonMandAccountAvailable(child.getFfid(), loyaltyManagementBD)
							&& LmsCommonUtil.availableForCarrier(child.getFfid(), lmsMemberServiceBD)) {
						child.setValidFFID(true);
						LoyaltyMemberCoreDTO member = LmsCommonUtil.getPassenger(child.getFfid(), loyaltyManagementBD);
						child.setFirstName(member.getMemberFirstName());
						child.setLastName(member.getMemberLastName());
						child.setDateOfBirth(member.getMemberBirthdateDay()+"/"+member.getMemberBirthdateMonth()+"/"+member.getMemberBirthdateYear());
					}
				}
			}
		}
		return StrutsConstants.Result.BYPASS;
	}
	
	
	private PaxDetailDTO extractPaxDetails(String jsonString) throws ParseException {
		
		PaxDetailDTO paxDetails = new PaxDetailDTO();
		if (AppSysParamsUtil.isLMSEnabled()){
			if (jsonString != null && !"".equals(jsonString)) {
				JSONParser parser = new JSONParser();
				JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
				
				Collection<PassengerDTO> adults = new ArrayList<PassengerDTO>();
				Collection<PassengerDTO> children = new ArrayList<PassengerDTO>();
				
				JSONArray adultsArray = (JSONArray) jsonObject.get("adultList");
				Iterator<?> adultsArrayiterator = adultsArray.iterator();
				
				while(adultsArrayiterator.hasNext()){
					PassengerDTO adult = new PassengerDTO();
					JSONObject paxObj = (JSONObject) adultsArrayiterator.next();
					adult.setFfid(BeanUtils.nullHandler(paxObj.get("ffid")));
					
					adults.add(adult);
				}
				
				paxDetails.setAdultList(adults);
				
				JSONArray childrenArray = (JSONArray) jsonObject.get("childList");
				Iterator<?> childrenArrayiterator = childrenArray.iterator();
				
				while(childrenArrayiterator.hasNext()){
					PassengerDTO child = new PassengerDTO();
					JSONObject paxObj = (JSONObject) childrenArrayiterator.next();
					child.setFfid(BeanUtils.nullHandler(paxObj.get("ffid")));
					
					children.add(child);
				}
				paxDetails.setChildList(children);
			}
		}
		return paxDetails;
	}
	
	public String ffidCheck(){
		if (AppSysParamsUtil.isLMSEnabled()){
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			LmsMemberServiceBD lmsMemberServiceBD = ModuleServiceLocator.getLmsMemberServiceBD();
			ffidAvailability = false;
			nameMatch = true;
			try{			
				LoyaltyMemberCoreDTO member = loyaltyManagementBD.getLoyaltyMemberCoreDetails(ffid);
				LmsMember lmsMember = lmsMemberServiceBD.getLmsMember(ffid);
				if(member.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK){
					ffidAvailability = true;
				}
				if(!(lmsMember.getFirstName().toLowerCase().equals(member.getMemberFirstName().toLowerCase()))
						||!(lmsMember.getLastName().toLowerCase().equals(member.getMemberLastName().toLowerCase()))){
					nameMatch = false;
					
				}
			}catch(ModuleException me){
				log.error("LmsMemberConfirmationAction ==>", me);
			}
		}
		return StrutsConstants.Result.BYPASS;
	}
	
	/**
	 * @return the validationString
	 */
	public String getValidationString() {
		return validationString;
	}

	/**
	 * @param validationString
	 *            the validationString to set
	 */
	public void setValidationString(String validationString) {
		this.validationString = validationString;
	}

	/**
	 * @return the ffid
	 */
	public String getFfid() {
		return ffid;
	}

	/**
	 * @param ffid
	 *            the ffid to set
	 */
	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the validationStatus
	 */
	public boolean isValidationStatus() {
		return validationStatus;
	}

	/**
	 * @param validationStatus the validationStatus to set
	 */
	public void setValidationStatus(boolean validationStatus) {
		this.validationStatus = validationStatus;
	}

	/**
	 * @return the paxDetail
	 */
	public PaxDetailDTO getPaxDetail() {
		return paxDetail;
	}

	/**
	 * @param paxDetail the paxDetail to set
	 */
	public void setPaxDetail(PaxDetailDTO paxDetail) {
		this.paxDetail = paxDetail;
	}

	/**
	 * @return the paxDetailJson
	 */
	public String getPaxDetailJson() {
		return paxDetailJson;
	}

	/**
	 * @param paxDetailJson the paxDetailJson to set
	 */
	public void setPaxDetailJson(String paxDetailJson) {
		this.paxDetailJson = paxDetailJson;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public String getOriginCarrier() {
		return originCarrier;
	}

	public void setOriginCarrier(String originCarrier) {
		this.originCarrier = originCarrier;
	}

	public boolean isFfidAvailability() {
		return ffidAvailability;
	}

	public void setFfidAvailability(boolean ffidAvailability) {
		this.ffidAvailability = ffidAvailability;
	}

	public boolean isNameMatch() {
		return nameMatch;
	}

	public void setNameMatch(boolean nameMatch) {
		this.nameMatch = nameMatch;
	}

	public void setLinkInvalid() {
		setMessage("This link is expired or invalid. To register with us click register button below");
		setStatus("I");
	}

}
