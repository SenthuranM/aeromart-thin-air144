/**
 * 
 */
package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.dto.ReservationDetailsDTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO.SegmentDetails;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.util.AlertConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

/**
 * @author suneth
 *
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.ERROR, type = JSONResult.class, value = StrutsConstants.Result.ERROR) })
public class SelfReprotectAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(SelfReprotectAction.class);

	private String pnr = null;

	private String alertId = null;

	private String pnrSegId = null;

	private ArrayList<Map<String, Object>> reservationDetailsList;

	Collection<SelfReprotectFlightDTO> flightsList = null;

	private boolean success = true;

	protected FlightSearchDTO searchParams = null;

	private String departureDate;

	private String fltSegId;

	private String arrivalDate;

	private String messageTxt = "";

	private String contactName;

	private String operatingCarrier;

	@SuppressWarnings("unused")
	public String execute() {

		String forward = StrutsConstants.Result.SUCCESS;
		String groupPNR = "";
		boolean isGroupPNR = false;
		String language = SessionUtil.getLanguage(request);

		try {

			searchParams = new FlightSearchDTO();
			Date departureDate;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-M-dd'T'HH:mm:ss");
			SimpleDateFormat wantedFormat = new SimpleDateFormat("dd/MM/yyyy");
			AvailableFlightInfo ondSeqFlight;
			String flightRPH;
			int aduldCount;
			int childCount;
			int infantCount;

			FlightAvailRS flightAvailRS = new FlightAvailRS();

			FlightSegmentDTO flightSegment = new FlightSegmentDTO();

			ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
			reservationSearchDTO.setPnr(pnr);

			Collection<ReservationListTO> reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(
					reservationSearchDTO, null, TrackInfoUtil.getBasicTrackInfo(request));

			ReservationListTO reservationItem = BeanUtils.getFirstElement(reservations);
			groupPNR = extractGroupPNR(reservationItem);

			if (!StringUtil.isNullOrEmpty(groupPNR)) {
				isGroupPNR = true;
			}

			LCCClientReservation reservation = ReservationUtil.loadProxyReservation(pnr, isGroupPNR, getTrackInfo(), false,
					false, null, null, false);

		     validateLinkDetails(reservation);


			Collection<LCCClientReservationSegment> reservationSegmentList = reservation.getSegments();

			LCCClientReservationSegment reservationSegment = null;

			for (LCCClientReservationSegment segment : reservationSegmentList) {

				if (pnrSegId.equals(segment.getPnrSegID().toString()) && segment.getStatus().equals("CNF")) {
					reservationSegment = segment;
					break;
				}

			}

			if (reservationSegment == null) {
				if (log.isDebugEnabled()) {
					log.debug("reservation segment didn't found");
				}
				success = false;
				forward = StrutsConstants.Result.ERROR;
				messageTxt = I18NUtil.getMessage("invalid.pnr", language);
				return forward;

			}

			String segmentCode = reservationSegment.getSegmentCode();

			aduldCount = reservation.getTotalPaxAdultCount();
			childCount = reservation.getTotalPaxChildCount();
			infantCount = reservation.getTotalPaxInfantCount();

			searchParams.setAdultCount(aduldCount);
			searchParams.setChildCount(childCount);
			searchParams.setInfantCount(infantCount);

			searchParams.setClassOfService(reservationSegment.getCabinClassCode());

			searchParams.setFromAirport(segmentCode.split("/")[0]);
			searchParams.setToAirport(segmentCode.split("/")[1]);

			departureDate = format.parse(this.departureDate);

			searchParams.setDepartureDate(wantedFormat.format(departureDate));

			ONDSearchDTO ondSearchDTO = new ONDSearchDTO();

			ondSearchDTO.setFromAirport(segmentCode.split("/")[0]);
			ondSearchDTO.setToAirport(segmentCode.split("/")[1]);
			ondSearchDTO.setClassOfService(reservationSegment.getCabinClassCode());
			ondSearchDTO.setDepartureDate(format.parse(this.departureDate));

			flightSegment.setFlightNumber(reservationSegment.getFlightNo());
			flightSegment.setSegmentCode(segmentCode);
			flightSegment.setDepartureDateTime(format.parse(this.departureDate));
			flightSegment.setArrivalDateTime(format.parse(this.arrivalDate));
			flightSegment.setSegmentId(Integer.parseInt(fltSegId));

			List<String> flightRPHList = new ArrayList<String>();
			flightRPH = FlightRefNumberUtil.composeFlightRPH(flightSegment);
			flightRPHList.add(flightRPH);
			ondSearchDTO.setFlightRPHList(flightRPHList);

			searchParams.getOndList().add(ondSearchDTO);

			FlightAvailRQ flightAvailRQ = ReservationBeanUtil
					.createMultiCityAvailSearchRQ(searchParams, AppIndicatorEnum.APP_IBE);

			flightAvailRQ.getAvailPreferences().setQuoteFares(false);
			flightAvailRQ.getAvailPreferences().setMultiCitySearch(false);
			flightAvailRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.IBE);
			flightAvailRQ.getAvailPreferences().setSearchSystem(reservationSegment.getSystem());

			flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
					getTrackInfo(), false);

			boolean isSeatsAvailable = ReservationBeanUtil.hasInventoryForFlight(flightAvailRS, flightRPH, aduldCount
					+ childCount, infantCount);

			if (isSeatsAvailable) {

				LCCClientTransferSegmentTO transferSegmentTO = new LCCClientTransferSegmentTO();

				if (isGroupPNR) {

					Map<String, SegmentDetails> oldSegmentDetails = new HashMap<String, SegmentDetails>();

					SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
					segmentDetails.setDepartureDate(reservationSegment.getDepartureDate());
					segmentDetails.setDepartureDateZulu(reservationSegment.getDepartureDateZulu());
					segmentDetails.setArrivalDate(reservationSegment.getArrivalDate());
					segmentDetails.setArrivalDateZulu(reservationSegment.getArrivalDateZulu());

					segmentDetails.setPnrSegId(new Integer(reservationSegment.getBookingFlightSegmentRefNumber()));
					segmentDetails.setSegmentCode(reservationSegment.getSegmentCode());
					segmentDetails.setCarrier(AppSysParamsUtil.extractCarrierCode(reservationSegment.getFlightNo()));
					segmentDetails.setCabinClass(reservationSegment.getCabinClassCode());
					segmentDetails.setLogicalCabinClass(reservationSegment.getLogicalCabinClass());
					segmentDetails.setFlightNumber(reservationSegment.getFlightNo());
					segmentDetails.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
					segmentDetails.setSegmentSeq(reservationSegment.getSegmentSeq());
					oldSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

					transferSegmentTO.setOldSegmentDetails(oldSegmentDetails);
					/* Create New Segment Details */
					Map<String, SegmentDetails> newSegmentDetails = new HashMap<String, SegmentDetails>();

					FlightSegmentTO newFlightSegment = flightAvailRS.getOriginDestinationInformationList().get(0)
							.getOrignDestinationOptions().get(0).getFlightSegmentList().get(0);

					segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
					segmentDetails.setDepartureDate(newFlightSegment.getDepartureDateTime());
					segmentDetails.setDepartureDateZulu(newFlightSegment.getDepartureDateTimeZulu());
					segmentDetails.setArrivalDate(newFlightSegment.getArrivalDateTime());
					segmentDetails.setArrivalDateZulu(newFlightSegment.getArrivalDateTimeZulu());
					segmentDetails.setFlightSegId(newFlightSegment.getFlightSegId());
					segmentDetails.setSegmentCode(newFlightSegment.getSegmentCode());
					segmentDetails.setCarrier(AppSysParamsUtil.extractCarrierCode(newFlightSegment.getFlightNumber()));
					segmentDetails.setCabinClass(newFlightSegment.getCabinClassCode());
					segmentDetails.setLogicalCabinClass(newFlightSegment.getLogicalCabinClassCode());
					segmentDetails.setFlightNumber(newFlightSegment.getFlightNumber());
					segmentDetails.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
					segmentDetails.setSegmentSeq(newFlightSegment.getSegmentSequence());
					newSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

					transferSegmentTO.setNewSegmentDetails(newSegmentDetails);

					if (oldSegmentDetails.size() != newSegmentDetails.size()) {
						throw new ModuleException("airreservations.transfersegment.segment.notmatch", language);
					}

				} else {

					Map<String, Integer> oldNewFlightSegId = new HashMap<String, Integer>();
					oldNewFlightSegId.put(reservationSegment.getFlightSegmentRefNumber(), Integer.parseInt(fltSegId));
					transferSegmentTO.setOldNewFlightSegId(oldNewFlightSegId);
				}

				transferSegmentTO.setPnr(pnr);
				transferSegmentTO.setGroupPNR(reservation.isGroupPNR());
				transferSegmentTO.setReservationVersion(reservation.getVersion());
				transferSegmentTO.setAlert(true);

				ModuleServiceLocator.getAirproxySegmentBD().transferSegments(transferSegmentTO,
						TrackInfoUtil.getBasicTrackInfo(request), SalesChannelsUtil.SALES_CHANNEL_WEB_KEY);

				contactName = reservation.getContactInfo().getLastName();
				this.departureDate = wantedFormat.format(departureDate);
				
				updateAlertToActioned(Integer.parseInt(alertId));

			} else {
				if (log.isDebugEnabled()) {
					log.debug("Seats not available");
				}
				success = false;
				forward = StrutsConstants.Result.ERROR;
				messageTxt = I18NUtil.getMessage("airinventory.transfer.flight.inventory.unavilable", language);
				return forward;

			}

		} catch (ModuleException me) {
			log.error("SelfReprotect Action error ==> ", me);
			success = false;
			messageTxt = I18NUtil.getMessage(me.getExceptionCode(), language);
			forward = StrutsConstants.Result.ERROR;
		} catch (ParseException pe) {
			log.error("SelfReprotect Action error ==> ", pe);
			success = false;
			messageTxt = I18NUtil.getMessage("invalid.link", language);
			forward = StrutsConstants.Result.ERROR;
		} catch (Exception e) {
			messageTxt = I18NUtil.getMessage(e.getMessage(), language);
			success = false;
			forward = StrutsConstants.Result.ERROR;
		}

		return forward;

	}

	public String getReservationDetailsGridData() {

		String forward = StrutsConstants.Result.SUCCESS;
		String groupPNR = "";
		boolean isGroupPNR = false;
		String originChannel;
		String language = SessionUtil.getLanguage(request);

		try {

			ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
			reservationSearchDTO.setPnr(pnr);

			Collection<ReservationListTO> reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(
					reservationSearchDTO, null, TrackInfoUtil.getBasicTrackInfo(request));

			ReservationListTO reservationItem = BeanUtils.getFirstElement(reservations);
			groupPNR = extractGroupPNR(reservationItem);

			if (groupPNR != null && !groupPNR.equals("")) {
				isGroupPNR = true;
			}

			LCCClientReservation reservation = ReservationUtil.loadProxyReservation(pnr, isGroupPNR, getTrackInfo(), false,
					false, null, null, false);

	         validateLinkDetails(reservation);
			 validateIBEActionedAlerts(reservation, Integer.parseInt(alertId));

			if (isGroupPNR) {
				originChannel = reservation.getAdminInfo().getOriginChannelId().split(",")[0].split("\\|")[1];
			} else {
				originChannel = reservation.getAdminInfo().getOriginChannelId();
			}

			if (!AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(originChannel)) {
				throw new ModuleException("ibe.booking.not.allowed.to.modify.through.web");
			}

			setReservationDetailsList(getReservationDetailsDTOList(reservation));
			
			retrieveSelfReprotectionFlights(isGroupPNR);

		} catch (ModuleException me) {
			success = false;
			messageTxt = I18NUtil.getMessage(me.getExceptionCode(), language);
			forward = StrutsConstants.Result.ERROR;
			log.error("SelfReprotect Reservation Details Fetching Error ==> ", me);
		} catch (ParseException pe) {
			success = false;
			messageTxt = I18NUtil.getMessage("invalid.link", language);
			forward = StrutsConstants.Result.ERROR;
			log.error("SelfReprotect Action error ==> ", pe);
		} catch (Exception e) {
			success = false;
			messageTxt = I18NUtil.getMessage(e.getMessage(), language);
			forward = StrutsConstants.Result.ERROR;
			log.error("SelfReprotect Action error" , e);
		}

		return forward;

	}

	/**
	 * This will create a list of reservationDetailsDTO mapped with string name of the 'reservationDetailsDTO'
	 * 
	 * @param reservation
	 * @return list of reservationDetailsDTO mapped with string name of the 'reservationDetailsDTO'
	 */
	public ArrayList<Map<String, Object>> getReservationDetailsDTOList(LCCClientReservation reservation) {

		ArrayList<Map<String, Object>> reservationDetailsDTOList = new ArrayList<Map<String, Object>>();
		ReservationDetailsDTO reservationDetailsDTO = null;
		LCCClientReservationPax reservationPax;
		LCCClientReservationSegment reservationSegment = null;
		int i = 0;

		Set<LCCClientReservationSegment> reservationSegmentList = reservation.getSegments();
		Iterator<LCCClientReservationSegment> segmentIterator = reservationSegmentList.iterator();

		while (segmentIterator.hasNext()) {
			reservationSegment = segmentIterator.next();
			if (reservationSegment.getPnrSegID().equals(pnrSegId)) {
				break;
			}
		}

		operatingCarrier = reservationSegment.getCarrierCode();
		Set<LCCClientReservationPax> reservationPaxList = reservation.getPassengers();

		Iterator<LCCClientReservationPax> iterator = reservationPaxList.iterator();

		while (iterator.hasNext()) {
			reservationPax = iterator.next();

			reservationDetailsDTO = new ReservationDetailsDTO();

			reservationDetailsDTO.setPaxName(reservationPax.getTitle() + ". " + reservationPax.getFirstName() + " "
					+ reservationPax.getLastName());
			reservationDetailsDTO.setPnr(reservation.getPNR());
			reservationDetailsDTO.setStatus(reservationPax.getStatus());
			reservationDetailsDTO.setDepartureDateTime(reservationSegment.getDepartureDate());
			reservationDetailsDTO.setArrivalDateTime(reservationSegment.getArrivalDate());
			reservationDetailsDTO.setFrom(reservationSegment.getDepartureAirportName());
			reservationDetailsDTO.setTo(reservationSegment.getArrivalAirportName());

			Map<String, Object> row = new HashMap<String, Object>();
			row.put("reservationDetailsDTO", reservationDetailsDTO);
			row.put("id", i++);
			reservationDetailsDTOList.add(row);
		}

		return reservationDetailsDTOList;

	}

	private void retrieveSelfReprotectionFlights(boolean isGroupPNR) throws ModuleException {

			flightsList = ModuleServiceLocator.getAirproxyReservationBD().getSelfReprotectFlights(alertId, getTrackInfo(),
					isGroupPNR, operatingCarrier);
			if (flightsList == null || flightsList.size() == 0) {
				throw new ModuleException("module.no.reprotect.flights");
			}	

	}

	/** Extract the groupPnr */
	private String extractGroupPNR(ReservationListTO reservationItem) {
		String groupPNR = "";
		groupPNR = reservationItem.getOriginatorPnr();
		if (StringUtils.isEmpty(groupPNR)) {
			// if this condition is true, it implies that the reservation is a dry reservation.
			if (StringUtils.isNotEmpty(reservationItem.getAirlineCode())
					&& StringUtils.isNotEmpty(reservationItem.getMarketingAirlineCode())) {
				groupPNR = reservationItem.getPnrNo();
			}
		}
		return groupPNR;
	}

	private void validateLinkDetails(LCCClientReservation reservation) throws ModuleException {

		boolean validLink = false;
		Collection<LCCClientAlertInfoTO> lccClientAlertInfoTOList = reservation.getAlertInfoTOs();
		for (LCCClientAlertInfoTO lccClientAlertInfoTO : lccClientAlertInfoTOList) {
			if (lccClientAlertInfoTO.getFlightSegmantRefNumber().equals(pnrSegId)) {
				for (LCCClientAlertTO lccClientAlertTO : lccClientAlertInfoTO.getAlertTO()) {
					if (lccClientAlertTO.getAlertId() == Integer.parseInt(alertId)) {
						validLink = true;
					}
				}
			}
		}

		if (!validLink) {
			throw new ModuleException("invalid.link");
		}

	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param alertId
	 *            the alertId to set
	 */
	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(String pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return the pnrSegId
	 */
	public String getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param searchParams
	 *            the searchParams to set
	 */
	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	/**
	 * @param departureDate
	 *            the departureDate to set
	 */
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the departureDate
	 */
	public String getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param fltSegId
	 *            the fltSegId to set
	 */
	public void setFltSegId(String fltSegId) {
		this.fltSegId = fltSegId;
	}

	/**
	 * @param arrivalDate
	 *            the arrivalDate to set
	 */
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the reservationDetailsList
	 */
	public ArrayList<Map<String, Object>> getReservationDetailsList() {
		return reservationDetailsList;
	}

	/**
	 * @param reservationDetailsList
	 *            the reservationDetailsList to set
	 */
	public void setReservationDetailsList(ArrayList<Map<String, Object>> reservationDetailsList) {
		this.reservationDetailsList = reservationDetailsList;
	}

	/**
	 * @return the flightsList
	 */
	public Collection<SelfReprotectFlightDTO> getFlightsList() {
		return flightsList;
	}

	/**
	 * @param flightsList
	 *            the flightsList to set
	 */
	public void setFlightsList(Collection<SelfReprotectFlightDTO> flightsList) {
		this.flightsList = flightsList;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}
	
	private void validateIBEActionedAlerts(LCCClientReservation reservation, Integer alertId) throws ModuleException {
		List<LCCClientAlertInfoTO> alerts = reservation.getAlertInfoTOs();

		if (CollectionUtils.isEmpty(alerts)) {
			log.error("No alerts found on the reservation for the PNR: " + reservation.getPNR());
			throw new ModuleException("invalid.link");
		}

		boolean valid = false;
		for (LCCClientAlertInfoTO alert : alerts) {
			List<LCCClientAlertTO> alertTOs = alert.getAlertTO();
			if (alertTOs != null) {
				for (LCCClientAlertTO alertTO : alertTOs) {
					Integer resAlertId = alertTO.getAlertId();
					if (resAlertId.equals(alertId)) {
						String alreadyActionedAlert = alertTO.getIbeActioned();
						if (!StringUtil.isNullOrEmpty(alreadyActionedAlert)) {
							if (AlertConstants.IBEOperations.IBE_ACTIONED_YES.equals(alreadyActionedAlert)) {
								log.error("Rejecting since alert is already actioned for the alert id: " + alertId);
								throw new ModuleException("has.actioned.using.IBE");
							} else {
								valid = true;
							}
							break;
						}

					}
				}
			}
		}

		if (!valid) {
			log.error("Requested alert is not avaible in the reservation for the alert ID: " + alertId);
			throw new ModuleException("invalid.link");
		}
	}
	
	
	private void updateAlertToActioned(Integer alertId) throws ModuleException {
		List<Integer> alertIds = new ArrayList<Integer>();
		alertIds.add(alertId);

		//alert id will be always from the operating carrier
		List<Alert> alerts = ModuleServiceLocator.getAlertingBD().retrieveAlerts(alertIds);
		if (CollectionUtils.isEmpty(alerts) || alerts.size() > 1) {
			throw new ModuleException("pax.reprotect.error.link.details.wrong");
		}
		Alert alert = alerts.get(0);
		alert.setIbeActoined(AlertConstants.IBEOperations.IBE_ACTIONED_YES);
		ModuleServiceLocator.getAlertingBD().updateAlert(alert);

	}
	

	public boolean isSuccess() {
		return success;
	}

	
}
