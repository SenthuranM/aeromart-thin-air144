package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerForgotPasswordDetailAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ShowCustomerForgotPasswordDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private HashMap<String, String> jsonLabel;

	private Map<String, String> errorInfo;

	private List questionList;

	private Map<String, IBEContactConfigDTO> configMap = new HashMap<String, IBEContactConfigDTO>();

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		String language = SessionUtil.getLanguage(request);
		try {
			jsonLabel = I18NUtil.getMessagesInJSON(language, "PgForgotPwd");
			errorInfo = ErrorMessageUtil.getForgotPwdErrors(request);
			questionList = SelectListGenerator.getSecreatQuestionList();

			List allConfigs = IBEModuleUtils.getGlobalConfig().getContactDetailsConfig(AppIndicatorEnum.APP_IBE.toString());
			PaxUtil.populateContactConfig(allConfigs, configMap, new HashMap<String, List<String>>());

		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, getCommonParams().getLocale());
			log.error("ShowCustomerForgotPasswordDetailAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public List getQuestionList() {
		return questionList;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public Map<String, IBEContactConfigDTO> getConfigMap() {
		return configMap;
	}

	public void setConfigMap(Map<String, IBEContactConfigDTO> configMap) {
		this.configMap = configMap;
	}

}
