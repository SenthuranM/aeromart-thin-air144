package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.ibe.api.dto.BalanceTo;
import com.isa.thinair.ibe.api.dto.CancelBalanceSummaryDTO;
import com.isa.thinair.ibe.api.dto.DisplayUtilDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.ModifyBalanceDTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesListTO;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.BigDecimalWrapper;

public class BalanceSummaryUtil {

	/**
	 * Get Modify Segment Balance Details
	 * 
	 * @param lCCClientReservationBalanceTO
	 * @param paxList
	 * @param selectedCurrency
	 * @param isOutboundFlexiSelected
	 * @param isInboundFlexiselected
	 * @param isRequoteFlow
	 * @param paxCreditMap
	 * @return
	 * @throws ParseException
	 */
	public static ConfirmUpdateChargesTO getChargesDetails(ReservationBalanceTO lCCClientReservationBalanceTO,
			Collection<ReservationPaxTO> paxList, IBEReservationInfoDTO resInfo, String selectedCurrency, String language,
			String pnr, boolean isOutboundFlexiSelected, boolean isInboundFlexiselected, boolean isRequoteFlow)
			throws ParseException, ModuleException {
		ConfirmUpdateChargesTO confirmUpdateChargesTO = new ConfirmUpdateChargesTO();

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency, ApplicationEngine.IBE);

		if (lCCClientReservationBalanceTO != null) {
			BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal creditCardFee = AccelAeroCalculator.getDefaultBigDecimalZero();

			confirmUpdateChargesTO.setDisplayCredit(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO
					.getTotalCreditAmount()));
			confirmUpdateChargesTO.setDisplayDue(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO
					.getTotalAmountDue()));

			Collection<ConfirmUpdateChargesListTO> collection = new ArrayList<ConfirmUpdateChargesListTO>();
			LCCClientSegmentSummaryTO lCCClientSegmentSummaryTO = lCCClientReservationBalanceTO.getSegmentSummary();

			ConfirmUpdateChargesListTO confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);

			// Just adding flexi charges with display flag set to false. Cannot check with ZERO value as there might be
			// ZERO flexi fares.
			// Either outbound or inbound will be available at this point
			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
			confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblFlexiChages", language));
			if (isOutboundFlexiSelected && lCCClientSegmentSummaryTO.getOutBoundExternalCharge().compareTo(BigDecimal.ZERO) > 0) {
				flexiCharge = lCCClientSegmentSummaryTO.getOutBoundExternalCharge();
			} else if (isInboundFlexiselected
					&& lCCClientSegmentSummaryTO.getInBoundExternalCharge().compareTo(BigDecimal.ZERO) > 0) {
				flexiCharge = lCCClientSegmentSummaryTO.getInBoundExternalCharge();
			}
			if (flexiCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
						.getDefaultBigDecimalZero()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(flexiCharge));
				confirmUpdateChargesListTO.setEnableDisplay(false);
				collection.add(confirmUpdateChargesListTO);
			}

			// Flexi will be added to new fare, because in IBE, flexi will be shown as a different logical cabin class
			// option and flexi will be included in the fare. So, in modify also flexi needs to be added to fare
			BigDecimal newFareWithFlexi = AccelAeroCalculator.add(lCCClientSegmentSummaryTO.getNewFareAmount(), flexiCharge);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
			confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblAirFare", language));
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentFareAmount()));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(newFareWithFlexi));
			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
			confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblTaxNSurcharges", language));
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentTaxAmount(), lCCClientSegmentSummaryTO.getCurrentSurchargeAmount(),
					lCCClientSegmentSummaryTO.getCurrentAdjAmount())));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getNewTaxAmount(), lCCClientSegmentSummaryTO.getNewSurchargeAmount(),
					lCCClientSegmentSummaryTO.getNewExtraFeeAmount())));
			collection.add(confirmUpdateChargesListTO);

			// Add Ancillary
			List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
			if (paxList != null) {
				for (ReservationPaxTO pax : paxList) {
					extChgList.addAll(pax.getExternalCharges());
				}
			}

			BigDecimal seatChg = BigDecimal.ZERO;
			BigDecimal mealChg = BigDecimal.ZERO;
			BigDecimal baggageChg = BigDecimal.ZERO;
			BigDecimal ssrChg = BigDecimal.ZERO;
			BigDecimal airportServiceChg = BigDecimal.ZERO;
			BigDecimal airportTransferChg = BigDecimal.ZERO;
			BigDecimal insuranceChg = BigDecimal.ZERO;
			BigDecimal totalAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

			for (LCCClientExternalChgDTO chgDTO : extChgList) {
				if (chgDTO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
					if (!chgDTO.isAmountConsumedForPayment()) {
						totalAncillaryCharges = AccelAeroCalculator.add(totalAncillaryCharges, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.SEAT_MAP) {
						seatChg = AccelAeroCalculator.add(seatChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.MEAL) {
						mealChg = AccelAeroCalculator.add(mealChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.BAGGAGE) {
						baggageChg = AccelAeroCalculator.add(baggageChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
						ssrChg = AccelAeroCalculator.add(ssrChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
						airportServiceChg = AccelAeroCalculator.add(airportServiceChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_TRANSFER) {
						airportTransferChg = AccelAeroCalculator.add(airportTransferChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INSURANCE) {
						insuranceChg = AccelAeroCalculator.add(insuranceChg, chgDTO.getAmount());
					}
				}
			}

			if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
				if (seatChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentSeatAmount().compareTo(BigDecimal.ZERO) != 0) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblSeatSelection", language));
					confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentSeatAmount()));
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(seatChg));
					collection.add(confirmUpdateChargesListTO);
				}

				if (mealChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentMealAmount().compareTo(BigDecimal.ZERO) != 0) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblMealSelection", language));
					confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentMealAmount()));
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(mealChg));
					collection.add(confirmUpdateChargesListTO);
				}

				if (baggageChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentBaggageAmount().compareTo(BigDecimal.ZERO) != 0) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblBaggageSelection",
							language));
					confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentBaggageAmount()));
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(baggageChg));
					collection.add(confirmUpdateChargesListTO);
				}

				if (ssrChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentSSRAmount().compareTo(BigDecimal.ZERO) != 0) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblSSRSelection", language));
					confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentSSRAmount()));
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(ssrChg));
					collection.add(confirmUpdateChargesListTO);
				}

				if (airportServiceChg.compareTo(BigDecimal.ZERO) > 0) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblHalaSelection", language));
					confirmUpdateChargesListTO.setDisplayOldCharges("");
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(airportServiceChg));
					collection.add(confirmUpdateChargesListTO);
				}

				if (airportTransferChg.compareTo(BigDecimal.ZERO) > 0) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil
							.getMessage("PgPriceBreakDown_lblAirportTransfer", language));
					confirmUpdateChargesListTO.setDisplayOldCharges("");
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(airportTransferChg));
					collection.add(confirmUpdateChargesListTO);
				}

				if (insuranceChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentInsuranceAmount().compareTo(BigDecimal.ZERO) != 0) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblInsurance", language));
					confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentInsuranceAmount()));
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(insuranceChg));
					collection.add(confirmUpdateChargesListTO);
				}
			}

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
			confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblmodificationCharge", language));
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentModAmount()));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getNewModAmount()));
			collection.add(confirmUpdateChargesListTO);

			if (isRequoteFlow) {
				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
				confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblCancelCharge", language));
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentCnxAmount()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getNewCnxAmount()));
				collection.add(confirmUpdateChargesListTO);

				if (lCCClientSegmentSummaryTO.getModificationPenalty() != null) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblPenalties", language));
					// confirmUpdateChargesListTO.setDisplayOldCharges("0.00");
					confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentModificationPenatly()));
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getModificationPenalty()));
					collection.add(confirmUpdateChargesListTO);
				}

				if ((lCCClientSegmentSummaryTO.getCurrentAdjAmount() != null && lCCClientSegmentSummaryTO.getNewAdjAmount() != null)
						&& (lCCClientSegmentSummaryTO.getCurrentAdjAmount().doubleValue() != 0 || lCCClientSegmentSummaryTO
								.getNewAdjAmount().doubleValue() != 0)) {
					confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
					confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblAdjustmentCharge",
							language));
					confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentAdjAmount()));
					confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getNewAdjAmount()));
					collection.add(confirmUpdateChargesListTO);
				}

			}

			if (resInfo.getCreditCardFee() != null) {
				creditCardFee = resInfo.getCreditCardFee();
				BigDecimal ccTax = resInfo.getServiceChargeTax(EXTERNAL_CHARGES.JN_OTHER);
				creditCardFee = AccelAeroCalculator.add(creditCardFee, ccTax);
			}

			if (creditCardFee.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
				confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblTranFree", language));
				confirmUpdateChargesListTO.setDisplayOldCharges(new BigDecimalWrapper().getDisplayValue());
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(creditCardFee));
				collection.add(confirmUpdateChargesListTO);
			}

			// Total Current + New Charges
			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
			confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblTotalCharge", language));
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentFareAmount(), lCCClientSegmentSummaryTO.getCurrentTaxAmount(),
					lCCClientSegmentSummaryTO.getCurrentSurchargeAmount(), lCCClientSegmentSummaryTO.getCurrentCnxAmount(),
					lCCClientSegmentSummaryTO.getCurrentModAmount(), lCCClientSegmentSummaryTO.getCurrentAdjAmount(),
					lCCClientSegmentSummaryTO.getCurrentModificationPenatly(),
					lCCClientSegmentSummaryTO.getCurrentBaggageAmount(), lCCClientSegmentSummaryTO.getCurrentSeatAmount(),
					lCCClientSegmentSummaryTO.getCurrentMealAmount(), lCCClientSegmentSummaryTO.getCurrentSSRAmount(),
					lCCClientSegmentSummaryTO.getCurrentInsuranceAmount())));
			BigDecimal newtotalWithAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal modificationPenalty = lCCClientSegmentSummaryTO.getModificationPenalty() != null
					? lCCClientSegmentSummaryTO.getModificationPenalty()
					: AccelAeroCalculator.getDefaultBigDecimalZero();

			newtotalWithAnci = AccelAeroCalculator.add(newFareWithFlexi, lCCClientSegmentSummaryTO.getNewTaxAmount(),
					lCCClientSegmentSummaryTO.getNewSurchargeAmount(), lCCClientSegmentSummaryTO.getNewCnxAmount(),
					lCCClientSegmentSummaryTO.getNewModAmount(), lCCClientSegmentSummaryTO.getNewAdjAmount(),
					(isRequoteFlow?AccelAeroCalculator.getDefaultBigDecimalZero():totalAncillaryCharges), creditCardFee, modificationPenalty, lCCClientSegmentSummaryTO.getNewExtraFeeAmount());
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(newtotalWithAnci));
			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
			confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblNonRefundAmount", language));
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentNonRefunds()));
			confirmUpdateChargesListTO.setDisplayNewCharges(new BigDecimalWrapper().getDisplayValue());
			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
			confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblCreditableAmount", language));
			confirmUpdateChargesListTO.setDisplayOldCharges("");
			if (lCCClientSegmentSummaryTO.getCurrentRefunds().compareTo(BigDecimal.ZERO) > 0) {
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentRefunds()));
			} else {
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentRefunds()));
			}
			collection.add(confirmUpdateChargesListTO);

			// Reservation Credit
			BigDecimal totalPaxCredit = getTotalPaxCredit(resInfo.getPaxCreditMap(pnr)).negate();
			if (totalPaxCredit.compareTo(BigDecimal.ZERO) > 0) {
				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
				confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblReservationCredit", language));
				confirmUpdateChargesListTO.setDisplayOldCharges("");
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(getTotalPaxCredit(
						resInfo.getPaxCreditMap(pnr)).negate()));

				collection.add(confirmUpdateChargesListTO);
			}

			BalanceTo balanceTo = getTotalBalanceToPayAmount(lCCClientReservationBalanceTO, isRequoteFlow ? null : paxList, resInfo.isInfantPaymentSeparated());
			BigDecimal balanceToPay = balanceTo.getBalanceToPay();
			balanceToPay = AccelAeroCalculator.add(balanceToPay, creditCardFee);
			if (balanceTo.getTotalCreditBalace().compareTo(BigDecimal.ZERO) < 0) {
				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
				confirmUpdateChargesListTO.setDisplayOldCharges("");
				confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblCreditBalance", language));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(balanceTo
						.getTotalCreditBalace().negate()));
				collection.add(confirmUpdateChargesListTO);
			}
			if (balanceToPay.compareTo(BigDecimal.ZERO) >= 0) {
				// resInfo.setNoPay(false);
				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO(exchangeRate);
				confirmUpdateChargesListTO.setDisplayOldCharges("");
				confirmUpdateChargesListTO.setDisplayDesc(I18NUtil.getMessage("PgPriceBreakDown_lblBalanceToPay", language));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(balanceToPay));
				collection.add(confirmUpdateChargesListTO);
			}
			/*
			 * if (balanceTo.getBalanceToPay().compareTo(BigDecimal.ZERO) == 0) { resInfo.setNoPay(true); }
			 */
			confirmUpdateChargesTO.setChargesList(collection);

		}

		return confirmUpdateChargesTO;
	}

	/**
	 * Get Cancel Balance Summary
	 * 
	 * @param lCCClientReservationBalanceTO
	 * @return
	 */
	public static CancelBalanceSummaryDTO getCancelBalanceSummary(ReservationBalanceTO lCCClientReservationBalanceTO,
			Map<String, BigDecimal> paxCreditMap) {
		CancelBalanceSummaryDTO cancelBalanceSummary = new CancelBalanceSummaryDTO();

		if (lCCClientReservationBalanceTO != null) {

			LCCClientSegmentSummaryTO lCCClientSegmentSummaryTO = lCCClientReservationBalanceTO.getSegmentSummary();
			cancelBalanceSummary
					.setAirFare(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentFareAmount()));
			cancelBalanceSummary.setTax(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentTaxAmount(), lCCClientSegmentSummaryTO.getCurrentSurchargeAmount())));
			cancelBalanceSummary.setDiscount(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentDiscount()));
			cancelBalanceSummary
					.setCancelChrage(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getTotalCnxCharge()));

			if (lCCClientReservationBalanceTO.getPassengerSummaryList() != null) {
				for (LCCClientPassengerSummaryTO pax : lCCClientReservationBalanceTO.getPassengerSummaryList()) {
					if (!PaxTypeTO.INFANT.equalsIgnoreCase(pax.getPaxType())) {
						if (PaxTypeTO.ADULT.equalsIgnoreCase(pax.getPaxType())) {
							cancelBalanceSummary
									.setPerPaxCancelCharge(AccelAeroCalculator.formatAsDecimal(pax.getTotalCnxCharge()));
							break;
						}
					}
				}
			}

			cancelBalanceSummary
					.setCreditableAmount(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentRefunds()));
			cancelBalanceSummary
					.setAvailableCredit(AccelAeroCalculator.formatAsDecimal(getTotalPaxCredit(paxCreditMap).negate()));
			cancelBalanceSummary
					.setFinalCredit(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getTotalCreditAmount()));

		}

		return cancelBalanceSummary;
	}

	/**
	 * Get Total Pax Credit
	 * 
	 * @param paxCreditMap
	 * @return
	 */
	public static BigDecimal getTotalPaxCredit(Map<String, BigDecimal> paxCreditMap) {
		paxCreditMap = new HashMap<String, BigDecimal>();
		BigDecimal totalPaxCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxCreditMap != null && !paxCreditMap.isEmpty()) {
			Set<String> tmpKeySet = paxCreditMap.keySet();

			for (String key : tmpKeySet) {
				if (paxCreditMap.get(key).compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
					totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, paxCreditMap.get(key));
				}
			}
		}

		return totalPaxCredit;
	}

	public static ModifyBalanceDTO getFareQuoteUpdateDetails(ReservationBalanceTO lCCClientReservationBalanceTO,
			Map<String, BigDecimal> paxCreditMap, String selectedCurrency, boolean isInfantPaymentSeparated) {
		ModifyBalanceDTO modifyBalanceDTO = new ModifyBalanceDTO();
		if (lCCClientReservationBalanceTO != null) {
			/*
			 * BigDecimal totalBalance = getTotalBalanceWithOutAncilary(lCCClientReservationBalanceTO, paxCreditMap); if
			 * (totalBalance.compareTo(BigDecimal.ZERO) > 0) {
			 * modifyBalanceDTO.setBalanceToPay(AccelAeroCalculator.formatAsDecimal(totalBalance)); } else {
			 * modifyBalanceDTO.setTotalCreditBalace(AccelAeroCalculator.formatAsDecimal(totalBalance)); }
			 */
			BalanceTo balanceTo = getTotalBalanceToPayAmount(lCCClientReservationBalanceTO, null, isInfantPaymentSeparated);
			if (balanceTo.getBalanceToPay().compareTo(BigDecimal.ZERO) > 0) {
				modifyBalanceDTO.setBalanceToPay(AccelAeroCalculator.formatAsDecimal(balanceTo.getBalanceToPay()));
			} else {
				modifyBalanceDTO.setTotalCreditBalace(AccelAeroCalculator.formatAsDecimal(balanceTo.getTotalCreditBalace()));
			}
			modifyBalanceDTO.setModificationCharge(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO
					.getSegmentSummary().getNewModAmount()));
			modifyBalanceDTO.setTotalReservationCredit(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientReservationBalanceTO.getSegmentSummary().getCurrentRefunds(), getTotalPaxCredit(paxCreditMap)
							.negate())));
		}
		return modifyBalanceDTO;

	}

	public static BalanceTo getTotalBalanceToPayAmount(ReservationBalanceTO lCCClientReservationBalanceTO,
			Collection<ReservationPaxTO> paxList, boolean isInfantPaymentSeparated) {
		BalanceTo balanceTo = new BalanceTo();

		BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (lCCClientReservationBalanceTO != null) {
			Collection<LCCClientPassengerSummaryTO> colPassengerSummary = lCCClientReservationBalanceTO.getPassengerSummaryList();

			if (colPassengerSummary != null) {
				BigDecimal paxAmountWithAncilary = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal paxAmountDueWithOutAncilary = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (LCCClientPassengerSummaryTO passenger : colPassengerSummary) {
					// TODO Review
					// Due to Balance Payment change Issue
					if (!PaxTypeTO.INFANT.equalsIgnoreCase(passenger.getPaxType()) || isInfantPaymentSeparated) {
						paxAmountWithAncilary = AccelAeroCalculator.getDefaultBigDecimalZero();
						paxAmountDueWithOutAncilary = AccelAeroCalculator.add(passenger.getTotalCreditAmount().negate(),
								passenger.getTotalAmountDue());

						BigDecimal paxChgs = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (paxList != null) {
							for (ReservationPaxTO paxTO : paxList) {
								String travelerRef = paxTO.getTravelerRefNumber().split(",")[0].trim();
								if (passenger.getTravelerRefNumber().contains(travelerRef)) {
									paxChgs = getPaxExtCharges(paxTO.getExternalCharges());
									break;
								}
							}
						}

						paxAmountWithAncilary = AccelAeroCalculator.add(paxAmountDueWithOutAncilary, paxChgs);

						if (paxAmountWithAncilary.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
							balanceToPay = AccelAeroCalculator.add(balanceToPay, paxAmountWithAncilary);
						} else {
							creditBalance = AccelAeroCalculator.add(creditBalance, paxAmountWithAncilary);
						}
					}
				}
			}

		}

		balanceTo.setBalanceToPay(balanceToPay);
		balanceTo.setTotalCreditBalace(creditBalance);
		return balanceTo;

	}

	private static BigDecimal getPaxExtCharges(List<LCCClientExternalChgDTO> list) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : list) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	public static Collection<DisplayUtilDTO> getReservationBalanceSummay(LCCClientReservation lccClientReservation,
			String selCurrency, String language) throws ModuleException {
		List<DisplayUtilDTO> reservationBalanceList = new ArrayList<DisplayUtilDTO>();

		if (lccClientReservation != null) {
			BigDecimal totalListSurchages = AccelAeroCalculator.getDefaultBigDecimalZero();
			// BigDecimal insuranceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			DisplayUtilDTO displayUtilDTO = null;
			Map<String, BigDecimal> paxChargers = getPaxCharges(lccClientReservation.getPassengers());
			Map<EXTERNAL_CHARGES, BigDecimal> externalSurcharges = lccClientReservation.getExternalChargersSummary();

			BigDecimal seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal baggageCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal insuranceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal crediCardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal airportServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal airportTransferCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal promotionRewardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal additionalSeatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal anciPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (externalSurcharges != null) {
				seatCharge = externalSurcharges.get(EXTERNAL_CHARGES.SEAT_MAP) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.SEAT_MAP) : seatCharge;
				mealCharge = externalSurcharges.get(EXTERNAL_CHARGES.MEAL) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.MEAL) : mealCharge;
				baggageCharge = externalSurcharges.get(EXTERNAL_CHARGES.BAGGAGE) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.BAGGAGE) : baggageCharge;
				airportServiceCharge = externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_SERVICE) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.AIRPORT_SERVICE) : airportServiceCharge;
				airportTransferCharge = externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER) : airportTransferCharge;
				insuranceCharge = externalSurcharges.get(EXTERNAL_CHARGES.INSURANCE) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.INSURANCE) : insuranceCharge;
				flexiCharge = externalSurcharges.get(EXTERNAL_CHARGES.FLEXI_CHARGES) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.FLEXI_CHARGES) : flexiCharge;
				crediCardCharge = externalSurcharges.get(EXTERNAL_CHARGES.CREDIT_CARD) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.CREDIT_CARD) : crediCardCharge;
				promotionRewardCharge = externalSurcharges.get(EXTERNAL_CHARGES.PROMOTION_REWARD) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.PROMOTION_REWARD) : promotionRewardCharge;
				additionalSeatCharge = externalSurcharges.get(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE)
						: additionalSeatCharge;
				anciPenalty = externalSurcharges.get(EXTERNAL_CHARGES.ANCI_PENALTY) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.ANCI_PENALTY)
						: anciPenalty;
			}
			// if (lccClientReservation.getReservationInsurance() != null) {
			// insuranceCharge = lccClientReservation.getReservationInsurance().getTotalPremium();
			// }
			totalListSurchages = AccelAeroCalculator.add(seatCharge, mealCharge, insuranceCharge, flexiCharge, crediCardCharge,
					baggageCharge, airportServiceCharge, airportTransferCharge);
			displayUtilDTO = new DisplayUtilDTO();
			displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblAirFare", language));
			displayUtilDTO.setValue(new StringBuffer()
					.append(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketFare())).append(" ")
					.append(selCurrency).toString());
			reservationBalanceList.add(displayUtilDTO);

			displayUtilDTO = new DisplayUtilDTO();
			displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblTax", language));
			displayUtilDTO.setValue(new StringBuffer()
					.append(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketTaxCharge())).append(" ")
					.append(selCurrency).toString());
			reservationBalanceList.add(displayUtilDTO);

			displayUtilDTO = new DisplayUtilDTO();
			displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblSurcharges", language));
			displayUtilDTO.setValue(new StringBuffer()
					.append(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.subtract(
							AccelAeroCalculator.add(lccClientReservation.getTotalTicketSurCharge(),
									lccClientReservation.getTotalTicketAdjustmentCharge()), totalListSurchages))).append(" ")
					.append(selCurrency).toString());
			reservationBalanceList.add(displayUtilDTO);

			if (seatCharge.compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblSeatSelection", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(seatCharge)).append(" ")
						.append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (mealCharge.compareTo(BigDecimal.ZERO) > 0 && !AppSysParamsUtil.isFOCMealSelectionEnabled()) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblMealSelection", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(mealCharge)).append(" ")
						.append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (baggageCharge.compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblBaggageSelection", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(baggageCharge)).append(" ")
						.append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (airportServiceCharge.compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblHalaSelection", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(airportServiceCharge))
						.append(" ").append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (airportTransferCharge.compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblAptSelection", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(airportTransferCharge))
						.append(" ").append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (flexiCharge.compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblFlexiChages", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(flexiCharge)).append(" ")
						.append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (lccClientReservation.getReservationInsurances() != null
					&& !lccClientReservation.getReservationInsurances().isEmpty()
					&& insuranceCharge.compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				String screenLabel = I18NUtil.getMessage("PgPriceBreakDown_lblInsurance", language);
				displayUtilDTO.setLabelName(screenLabel);
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(insuranceCharge))
						.append(" ").append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (lccClientReservation.getTotalTicketModificationCharge().compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblmodificationCharge", language));
				displayUtilDTO.setValue(new StringBuffer()
						.append(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketModificationCharge()))
						.append(" ").append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (lccClientReservation.getTotalTicketCancelCharge().compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblCancelCharge", language));
				displayUtilDTO.setValue(new StringBuffer()
						.append(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketCancelCharge()))
						.append(" ").append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			if (crediCardCharge.compareTo(BigDecimal.ZERO) > 0) {
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblTranFree", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(crediCardCharge))
						.append(" ").append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}
			
			if(anciPenalty.compareTo(BigDecimal.ZERO) > 0){
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblAnciPenalty", language));
				displayUtilDTO.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(anciPenalty))
						.append(" ").append(selCurrency).toString());
				reservationBalanceList.add(displayUtilDTO);
			}

			DisplayUtilDTO discountDisplayDto = null;

			LCCPromotionInfoTO lccPromotionInfoTO = lccClientReservation.getLccPromotionInfoTO();
			if (lccPromotionInfoTO != null) {

				String discountLabel = "PgPriceBreakDown_lblDiscount";
				if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
					discountLabel = "PgPriceBreakDown_lblDiscountCredit";
				}

				BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal dispTotalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (lccClientReservation.getTotalDiscount() != null
						&& !AccelAeroCalculator.isEqual(lccClientReservation.getTotalDiscount(),
								AccelAeroCalculator.getDefaultBigDecimalZero())) {
					dispTotalDiscount = lccClientReservation.getTotalDiscount();
					if (AccelAeroCalculator.isLessThan(dispTotalDiscount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						dispTotalDiscount = dispTotalDiscount.negate();
					}

					if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
						discount = dispTotalDiscount;
					} else {
						discount = dispTotalDiscount.negate();
					}
				} else if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())
						&& lccPromotionInfoTO.getCreditDiscountAmount() != null
						&& lccPromotionInfoTO.getCreditDiscountAmount().doubleValue() != 0) {

					dispTotalDiscount = lccPromotionInfoTO.getCreditDiscountAmount();
					if (AccelAeroCalculator.isLessThan(dispTotalDiscount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						dispTotalDiscount = dispTotalDiscount.negate();
					}

					discount = dispTotalDiscount;
				}

				discountDisplayDto = new DisplayUtilDTO();
				discountDisplayDto.setLabelName(I18NUtil.getMessage(discountLabel, language));
				discountDisplayDto.setValue(new StringBuffer().append(AccelAeroCalculator.formatAsDecimal(discount)).append(" ")
						.append(selCurrency).toString());
				// reservationBalanceList.add(displayUtilDTO);
			}

			if (lccPromotionInfoTO != null && DiscountApplyAsTypes.MONEY.equals(lccPromotionInfoTO.getDiscountAs())) {
				reservationBalanceList.add(discountDisplayDto);
			}

			displayUtilDTO = new DisplayUtilDTO();
			displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPayment_lblTotalPaid", language));
			displayUtilDTO.setValue(new StringBuffer()
					.append(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalPaidAmount())).append(" ")
					.append(selCurrency).toString());
			reservationBalanceList.add(displayUtilDTO);

			if (!selCurrency.equals(lccClientReservation.getLastCurrencyCode())) {
				String selectedCurrency = lccClientReservation.getLastCurrencyCode();
				displayUtilDTO = new DisplayUtilDTO();
				displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblTotalChargeInSelectedCurrency", language));
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

				if (selectedCurrency != null && (!"".equals(selectedCurrency))) {
					CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency,
							ApplicationEngine.IBE);

					if (currencyExchangeRate != null) {
						Currency pgCurrency = currencyExchangeRate.getCurrency();
						BigDecimal totalSelCurrency = AccelAeroRounderPolicy.convertAndRound(
								lccClientReservation.getTotalTicketPrice(), currencyExchangeRate.getMultiplyingExchangeRate(),
								pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint());
						displayUtilDTO.setValue(new StringBuffer().append(totalSelCurrency).append(" ").append(selectedCurrency)
								.toString());
						reservationBalanceList.add(displayUtilDTO);

					}
				}
			}

			if (lccClientReservation.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) != 0) {

				if (paxChargers.get("creditBalance").compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
					displayUtilDTO = new DisplayUtilDTO();
					displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblCreditBalance", language));
					displayUtilDTO.setValue(new StringBuffer()
							.append(AccelAeroCalculator.formatAsDecimal(paxChargers.get("creditBalance"))).append(" ")
							.append(selCurrency).toString());
					reservationBalanceList.add(displayUtilDTO);
				}
				if (paxChargers.get("balanceToPay").compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
					displayUtilDTO = new DisplayUtilDTO();
					displayUtilDTO.setLabelName(I18NUtil.getMessage("PgPriceBreakDown_lblBalanceToPay", language));
					displayUtilDTO.setValue(new StringBuffer()
							.append(AccelAeroCalculator.formatAsDecimal(paxChargers.get("balanceToPay"))).append(" ")
							.append(selCurrency).toString());
					reservationBalanceList.add(displayUtilDTO);
				}

			}

			if (lccPromotionInfoTO != null && DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
				reservationBalanceList.add(discountDisplayDto);
			}

			if (lccClientReservation.getLastCurrencyCode() != null
					&& !selCurrency.equals(lccClientReservation.getLastCurrencyCode())) {
				setSelctedCurrencyValues(reservationBalanceList, lccClientReservation.getLastModificationTimestamp(),
						lccClientReservation.getLastCurrencyCode(), language);
			}
		}

		return reservationBalanceList;
	}

	private static List<DisplayUtilDTO> setSelctedCurrencyValues(List<DisplayUtilDTO> displayValues, Date lastModifiedDate,
			String selectedCurrency, String language) throws ModuleException {
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(lastModifiedDate);

		for (DisplayUtilDTO displayVal : displayValues) {

			if (!displayVal.getLabelName().equals(I18NUtil.getMessage("PgPayment_lblTotalPaid", language))
					&& !displayVal.getLabelName().equals(I18NUtil.getMessage("PgPriceBreakDown_lblTotalCharge", language))) {

				String[] strValArr = displayVal.getValue().split(" ");

				if (strValArr[1].equals(selectedCurrency)) {
					continue;
				}

				BigDecimal val = new BigDecimal(strValArr[0]);

				displayVal.setValue(AccelAeroCalculator.formatAsDecimal(exchangeRateProxy.convert(selectedCurrency, val))
						+ selectedCurrency);
			}
		}
		return displayValues;
	}

	// TODO Refactor - Merge with getReservationBalanceSummay method
	public static FareQuoteTO fillFareQuote(LCCClientReservation lccClientReservation) throws ModuleException {
		FareQuoteTO fareQuote = new FareQuoteTO();

		if (lccClientReservation != null) {
			BigDecimal totalListSurchages = AccelAeroCalculator.getDefaultBigDecimalZero();
			Map<EXTERNAL_CHARGES, BigDecimal> externalSurcharges = lccClientReservation.getExternalChargersSummary();

			BigDecimal seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal baggageCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal airportServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal airportTransferCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal inflightServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal insuranceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal crediCardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal promotionRewardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal additionalSeatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (externalSurcharges != null) {
				seatCharge = externalSurcharges.get(EXTERNAL_CHARGES.SEAT_MAP) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.SEAT_MAP) : seatCharge;
				mealCharge = externalSurcharges.get(EXTERNAL_CHARGES.MEAL) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.MEAL) : mealCharge;
				baggageCharge = externalSurcharges.get(EXTERNAL_CHARGES.BAGGAGE) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.BAGGAGE) : baggageCharge;
				airportServiceCharge = externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_SERVICE) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.AIRPORT_SERVICE) : airportServiceCharge;
				airportTransferCharge = externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER) : airportTransferCharge;
				inflightServiceCharge = externalSurcharges.get(EXTERNAL_CHARGES.INFLIGHT_SERVICES) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.INFLIGHT_SERVICES) : inflightServiceCharge;
				insuranceCharge = externalSurcharges.get(EXTERNAL_CHARGES.INSURANCE) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.INSURANCE) : insuranceCharge;
				flexiCharge = externalSurcharges.get(EXTERNAL_CHARGES.FLEXI_CHARGES) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.FLEXI_CHARGES) : flexiCharge;
				crediCardCharge = externalSurcharges.get(EXTERNAL_CHARGES.CREDIT_CARD) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.CREDIT_CARD) : crediCardCharge;
				promotionRewardCharge = externalSurcharges.get(EXTERNAL_CHARGES.PROMOTION_REWARD) != null ? externalSurcharges
						.get(EXTERNAL_CHARGES.PROMOTION_REWARD) : promotionRewardCharge;
				additionalSeatCharge = externalSurcharges.get(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE)
						: additionalSeatCharge;
			}
			totalListSurchages = AccelAeroCalculator.add(seatCharge, mealCharge, insuranceCharge, crediCardCharge, baggageCharge,
					airportServiceCharge, airportTransferCharge, inflightServiceCharge, flexiCharge, promotionRewardCharge,
					additionalSeatCharge);

			fareQuote.setTotalFare(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketFare()));
			fareQuote.setTotalTax(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketTaxCharge()));
			fareQuote.setTotalTaxSurcharge(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.subtract(AccelAeroCalculator
					.add(lccClientReservation.getTotalTicketSurCharge(), lccClientReservation.getTotalTicketAdjustmentCharge(),
							lccClientReservation.getTotalTicketTaxCharge()), totalListSurchages)));
			fareQuote.setTotalPrice(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketPrice()));
			fareQuote.setTotalIbePromoDiscount(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalDiscount()));

			if (seatCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setSeatMapCharge(AccelAeroCalculator.formatAsDecimal(seatCharge));
				fareQuote.setHasSeatCharge(true);
			}

			if (mealCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setMealCharge(AccelAeroCalculator.formatAsDecimal(mealCharge));
				fareQuote.setHasMealCharge(true);
			}

			if (baggageCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setBaggageCharge(AccelAeroCalculator.formatAsDecimal(baggageCharge));
				fareQuote.setHasBaggageCharge(true);
			}

			if (airportServiceCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setAirportServiceCharge(AccelAeroCalculator.formatAsDecimal(airportServiceCharge));
				fareQuote.setHasAirportServiceCharge(true);
			}

			if (airportTransferCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setAirportTransferCharge(AccelAeroCalculator.formatAsDecimal(airportTransferCharge));
				fareQuote.setHasAirportTransferCharge(true);
			}

			if (inflightServiceCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setSsrCharge(AccelAeroCalculator.formatAsDecimal(inflightServiceCharge));
				fareQuote.setHasSsrCharge(true);
			}

			if (insuranceCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setInsuranceCharge(AccelAeroCalculator.formatAsDecimal(insuranceCharge));
				fareQuote.setHasInsurance(true);
			}

			if (flexiCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setFlexiCharge(AccelAeroCalculator.formatAsDecimal(flexiCharge));
				fareQuote.setHasFlexi(true);
			}

			if (crediCardCharge.compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setTotalFee(AccelAeroCalculator.formatAsDecimal(crediCardCharge));
				fareQuote.setHasFee(true);
			}

			if (lccClientReservation.getTotalTicketModificationCharge().compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setModifyCharge(AccelAeroCalculator.formatAsDecimal(lccClientReservation
						.getTotalTicketModificationCharge()));
				fareQuote.setHasModifySegment(true);
			}

			fareQuote.setHasFareQuote(true);

		}

		return fareQuote;

	}

	/**
	 * 
	 * @param passengers
	 * @return
	 */
	private static Map<String, BigDecimal> getPaxCharges(Collection<LCCClientReservationPax> passengers) {
		Map<String, BigDecimal> ancilaryChargesMap = new HashMap<String, BigDecimal>();
		BigDecimal seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal baggageCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal airportServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal airportTranferCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (passengers != null) {
			for (LCCClientReservationPax pax : passengers) {
				// Pax Balance
				if (pax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) > 0) {
					balanceToPay = AccelAeroCalculator.add(balanceToPay, pax.getTotalAvailableBalance());
				} else {
					creditBalance = AccelAeroCalculator.add(creditBalance, pax.getTotalAvailableBalance());
				}

				// Pax Ancilary
				/*
				 * List<LCCSelectedSegmentAncillaryDTO> paxAncilary = pax.getSelectedAncillaries(); if (paxAncilary !=
				 * null) { for (LCCSelectedSegmentAncillaryDTO paxSelectedAncilary : paxAncilary) { if
				 * (paxSelectedAncilary.getAirSeatDTO() != null && paxSelectedAncilary.getAirSeatDTO().getSeatCharge()
				 * != null) { seatCharge = AccelAeroCalculator.add(seatCharge ,
				 * paxSelectedAncilary.getAirSeatDTO().getSeatCharge()); } List<LCCMealDTO> mealList =
				 * paxSelectedAncilary.getMealDTOs(); if (mealList != null) { for (LCCMealDTO meal : mealList) { if
				 * (meal.getMealCharge() != null) { mealCharge = AccelAeroCalculator.add(mealCharge ,
				 * meal.getMealCharge()); } } }
				 * 
				 * } }
				 */
			}
		}

		ancilaryChargesMap.put("seat", seatCharge);
		ancilaryChargesMap.put("meal", mealCharge);
		ancilaryChargesMap.put("baggage", baggageCharge);
		ancilaryChargesMap.put("airportService", airportServiceCharge);
		ancilaryChargesMap.put("airportTransfer", airportTranferCharge);
		ancilaryChargesMap.put("creditBalance", creditBalance);
		ancilaryChargesMap.put("balanceToPay", balanceToPay);

		return ancilaryChargesMap;
	}

	public static BigDecimal getUtilizeCreditBalance(ReservationBalanceTO lCCClientReservationBalanceTO,
			Collection<ReservationPaxTO> paxList, boolean isInfantPaymentSeparated) {

		BigDecimal paxUtilizedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (lCCClientReservationBalanceTO != null) {
			Collection<LCCClientPassengerSummaryTO> colPassengerSummary = lCCClientReservationBalanceTO.getPassengerSummaryList();

			if (colPassengerSummary != null) {
				BigDecimal paxCreditableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (LCCClientPassengerSummaryTO passenger : colPassengerSummary) {
					// TODO Review
					// Due to Balance Payment change Issue
					if (!PaxTypeTO.INFANT.equalsIgnoreCase(passenger.getPaxType()) || isInfantPaymentSeparated) {
						paxCreditableAmount = passenger.getTotalCreditAmount();

						BigDecimal paxChgs = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (paxList != null) {
							for (ReservationPaxTO paxTO : paxList) {
								String travelerRef = paxTO.getTravelerRefNumber().split(",")[0].trim();
								if (passenger.getTravelerRefNumber().contains(travelerRef)) {
									paxChgs = getPaxExtCharges(paxTO.getExternalCharges());
									break;
								}
							}
						}

						if (paxCreditableAmount.compareTo(paxChgs) >= 0) {
							paxUtilizedAmount = AccelAeroCalculator.add(paxUtilizedAmount, paxChgs);
						} else if (paxCreditableAmount.compareTo(BigDecimal.ZERO) > 0) {
							paxUtilizedAmount = AccelAeroCalculator.add(paxUtilizedAmount, paxCreditableAmount);
						}

					}
				}
			}

		}

		return paxUtilizedAmount;

	}

}