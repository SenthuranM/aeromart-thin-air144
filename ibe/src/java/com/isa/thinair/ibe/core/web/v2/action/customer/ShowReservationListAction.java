package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowReservationListAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(ShowReservationListAction.class);

	private boolean success = true;

	private String messageTxt;

	private String dispatchMode;

	private Collection<ReservationListTO> reservationListTO;

	private HashMap<String, String> jsonLabel;

	private Map<String, String> errorInfo;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			Customer customer = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			if (customer != null) {
				int intCustomerID = customer.getCustomerId();
				if (dispatchMode == null || dispatchMode.trim().equals("")) {
					String[] pagesIDs = { "PgResList", "PgSearch", "PgLms" };
					jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
					reservationListTO = CustomerUtilV2.getAfterReservationsList(intCustomerID, new Date(), getTrackInfo());
				} else if (dispatchMode.equalsIgnoreCase("travelHistory")) {
					String[] pagesIDs = { "PgResList", "PgResHistory", "PgSearch", "PgLms" };
					jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
					reservationListTO = CustomerUtilV2.getEarlyReservationsList(intCustomerID, new Date(), getTrackInfo());
				}
			}

			errorInfo = ErrorMessageUtil.getCustomerProfileErrors(request);

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowReservationListAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowReservationListAction==>", ex);
		}

		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<ReservationListTO> getReservationListTO() {
		return reservationListTO;
	}

	public void setReservationListTO(Collection<ReservationListTO> reservationListTO) {
		this.reservationListTO = reservationListTO;
	}

	public String getDispatchMode() {
		return dispatchMode;
	}

	public void setDispatchMode(String dispatchMode) {
		this.dispatchMode = dispatchMode;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

}
