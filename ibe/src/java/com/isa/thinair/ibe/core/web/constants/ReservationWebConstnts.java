package com.isa.thinair.ibe.core.web.constants;

public class ReservationWebConstnts {

	public final static String XML_FILE_PATH = "/templates/web/";

	public final static String MSG_ERROR_MESSAGE = "error";
	public final static String REQ_HTML_TERMS_COND_FULL = "termsNCondFull";
	public final static String MSG_ERROR_CODE = "errorCode";

	/*
	 * Parameter Retrieve from AirArabia
	 */
	public final static String REQ_PARAM_DETAILS = "sysReqParamAA";

	/*
	 * System Parameter Information
	 */
	public final static String PARAM_SYSTEM_DEFAULT = "systemDefaultParam";
	public final static String PARAM_DEFAULT_LANG = "sysDefLang";
	public final static String PARAM_OND_SOURCE = "sysOndSource";
	public final static String PARAM_COMMON = "commonParams";
	public final static String PARAM_REDIRECT_ATTEMPT = "redirectAttempt";
	public final static String PARAM_PAYMENT_FLOW = "paymentFlow";
	public final static String PARAM_HDNPARAMDATA = "hdnParamData";
	public final static String PARAM_REQUOTE_CALENDAR_ENABLED = "requoteCalendarEnabled";
	public final static String PARAM_IMAGE_HOST_PATH = "sysImagePath";

	/*
	 * Passengers
	 */
	public final static String REQ_CONTACT_CONFIG = "contactConfig";
	public final static String REQ_USER_REG_CONFIG = "userRegConfig";
	public final static String REQ_VALIDATION_GROUP = "validationGroup";
	public final static String REQ_PAX_CATEGORY = "paxCat";
	public final static String REQ_PRODUCT_ID = "productID";

	/*
	 * Promotion
	 */
	public final static String RESERVATION_CONTACT_FULL_NAME = "resContactName";
	public final static String PNR = "pnr";
	public final static String PROM_REQ_ATTR = "promReqAttr";
	public final static String REQ_FROM_PROMOTION_PAGE = "fromPromoPage";
	public final static String PROMO_MESSAGES = "promoMessages";
	public final static String PROMO_INFO_JSON = "promoInfoJson";
	public final static String LMS_CURRENCY_TO_POINTS = "pointsPerUSD";

	/*
	 * OHD Confirm
	 */
	public final static String OHD_LABELS = "ibeOHDLabels";

	/*
	 * Fares
	 */
	public final static String REQ_FARES_ARRAY_NAME = "arrFareCat";
	/*
	 * PNR No Session Key
	 */
	public final static String REQ_SESSION_PNR_NO = "sesPNRNO";
	
	public final static String REQ_SESSION_MODE = "sesMode";

	// Pay currency session key
	public final static String REQ_SESSION_PAY_CURRENCY = "sesPayCurrencyCode";

	// Pay currency session key
	public final static String REQ_SESSION_TOTAL_PAY_AMOUNT = "sesPayAmount";

	public final static String EXTERNAL_PARTY_ID = "externalPartyID";
	
	public final static String EXTERNAL_MARKER_ID = "marker";

	// Payment gateway error
	public final static String REQ_PAYMENT_GATEWAY_ERROR = "pay.gateway.error.";

	public final static String REQ_DEFAULT_ERROR = "server.default.operation.fail.";

	// constant for random number
	public static final String APP_IBECONFIG_JS = "appIBEConfigJS";
	public static final String SYS_PROP_DEBUG_FRONTEND = "com.isa.thinair.debug.frontend";
	public static final String APP_KIOSK_SESSION_TIMEOUT = "kskSessionTimeout";
	public static final String APP_IBE_SESSION_TIMEOUT_POPUP = "sessionTimeoutPopup";
	public static final String REQ_RECORD_STATUS_ACTIVE = "ACT";

	// https compatibility
	public static final String HTTPS_COMPATIBILITY = "httpsEnable";

	public static final String IS_MULTYCITY_SELECTED = "isMcSelected";
	public static final String IS_ON_PREVIOUS_CLICK = "onPreviousClick";
	public static final String IS_FROM_REGUSER = "fromRegUserPage";
	public static final String PNR_OND_GROUPS = "jsonOnds";

	/**
	 * User Language Codes
	 * 
	 */
	public enum LanguageCode {
		ENGLISH("en"), FRENCH("fr"), RUSSIAN("ru"), ARABIC("ar"), SPANISH("es"), ITALIANO("it");

		private String language;

		private LanguageCode(String language) {
			this.language = language;
		}

		public String getLanguageCode() {
			return language;
		}

	}

	/*
	 * Common request parameters
	 */
	public final static String REQ_FORM_REDIRECT_URL = "redirectUrl";
	public final static String REQ_FORM_HIDDEN_DATAMAP = "formHiddenDataMap";
	public final static String REQ_FORM_IS_TARGET_TOP = "isTargetTop";
	public final static String REQ_FORM_RANDOM_ID = "randomID";
	public final static String LANGUAGE = "language";

}
