package com.isa.thinair.ibe.core.web.v2.action.voucher;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "")
})

public class EmailVoucherIBEAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(IssueVoucherConfirmationAction.class);
	private List<VoucherDTO> voucherDTOs = new ArrayList<VoucherDTO>();
	
	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		
		try {
			VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
			for(VoucherDTO voucherDTO : voucherDTOs){
				voucherDelegate.emailVoucher(voucherDTO);
			}
		} catch (ModuleException me) {
			if (me.getExceptionCode().equals("promotions.voucher.amount.local.null")) {
				forward = StrutsConstants.Result.ERROR;
			}
			log.debug("[Email Gift Voucher Error");
		}
		return forward;
	}

	public List<VoucherDTO> getVoucherDTOs() {
		return voucherDTOs;
	}

	public void setVoucherDTOs(List<VoucherDTO> voucherDTOs) {
		this.voucherDTOs = voucherDTOs;
	}
}
