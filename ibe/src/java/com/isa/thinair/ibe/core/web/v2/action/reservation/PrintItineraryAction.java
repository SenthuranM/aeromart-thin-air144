package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.CustomizedItineraryUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Reservation.V2_PRINTITINERARY),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class PrintItineraryAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(PrintItineraryAction.class);

	private static String ACTION_EMAIL = "EMAIL";
	private static String ACTION_PRINT = "PRINT";

	private String hdnPNRNo;
	private boolean isGroupPNR;
	private String itineraryLanguage;
	private boolean includePaymentDetails;
	private boolean includeTicketCharges;
	private String viewMode;

	private String operationType;

	private String selectedPax;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			boolean isRegUser = isValidateRegisterUser();
			boolean isAccess = true;
			if ("loadRes".equals(viewMode)) {
				if (hdnPNRNo != null && !hdnPNRNo.trim().isEmpty() && isRegUser == false
						&& !SessionUtil.getManageBookingPNR(request).equals(hdnPNRNo)) {
					isAccess = false;
				} else if (isRegUser) {
					IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
					if (resInfo == null || resInfo.getPaxCreditMap(hdnPNRNo) == null) {
						isAccess = false;
					}
				}
			} else if ("confirmRes".equals(viewMode)) {
				if (hdnPNRNo != null
						&& !hdnPNRNo.equals(request.getSession().getAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO))) {
					isAccess = false;
				}
			} else {
				isAccess = false;
			}
			if (isAccess == false) {
				throw new ModuleException("unauthorized.operation");
			}
			getItineraryInfo();
		} catch (Exception e) {
			String msg = createErrorMessage(e);
			forward = StrutsConstants.Result.ERROR;
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
			log.error(msg, e);
			SessionUtil.resetSesionDataInError(request);
		}

		return forward;
	}

	public String printIndividualPaxItinerary() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			boolean isRegUser = isValidateRegisterUser();
			boolean isAccess = true;
			if ("loadRes".equals(viewMode)) {
				if (hdnPNRNo != null && !hdnPNRNo.trim().isEmpty() && isRegUser == false
						&& !SessionUtil.getManageBookingPNR(request).equals(hdnPNRNo)) {
					isAccess = false;
				} else if (isRegUser) {
					IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
					if (resInfo == null || resInfo.getPaxCreditMap(hdnPNRNo) == null) {
						isAccess = false;
					}
				}
			} else if ("confirmRes".equals(viewMode)) {
				if (hdnPNRNo != null
						&& !hdnPNRNo.equals(request.getSession().getAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO))) {
					isAccess = false;
				}
			} else {
				isAccess = false;
			}
			if (isAccess == false) {
				throw new ModuleException("unauthorized.operation");
			}
			getIndividualPaxItineraryInfo();
		} catch (Exception e) {
			String msg = createErrorMessage(e);
			forward = StrutsConstants.Result.ERROR;
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
			log.error(msg, e);
			SessionUtil.resetSesionDataInError(request);
		}

		return forward;
	}

	/**
	 * TODO Generate the Itinerary Body
	 * 
	 * @throws ModuleException
	 */
	private void getItineraryInfo() throws ModuleException {

		String pnr = hdnPNRNo;
		String station = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		boolean includeBaggageAllowance = false;
		boolean isbaggageEnable = false;
		boolean isbaggageAllowanceRemarksEnable = false;
		
		LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, isGroupPNR, itineraryLanguage, request, false,
				AppSysParamsUtil.isPromoCodeEnabled());
		/*
		 * Need the origin country for itinerary. It is necessary to determine whether additional charge details will be
		 * printed based on the country's configuration.
		 */
		pnrModesDTO.setLoadOriginCountry(true);

		String strBaggageAllowanceRemarks = ModuleServiceLocator.getGlobalConfig().getBizParam(
				SystemParamKeys.SHOW_BAGGAGE_ALLOWANCE_REMARKS);
		String strBaggage = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE);

		if (strBaggageAllowanceRemarks != null && strBaggageAllowanceRemarks.trim().equals("Y")) {
			isbaggageAllowanceRemarksEnable = true;
		}
		if (strBaggage != null && strBaggage.trim().equals("Y")) {
			isbaggageEnable = true;
		}
		if (isbaggageAllowanceRemarksEnable && !isbaggageEnable) {
			includeBaggageAllowance = true;
		}

		CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
		commonItineraryParam.setItineraryLanguage(itineraryLanguage);
		commonItineraryParam.setIncludePaxFinancials(true);
		commonItineraryParam.setIncludePaymentDetails(true);
		if (AppSysParamsUtil.showChargesPaymentsInItinerary()) {
			commonItineraryParam.setIncludeTicketCharges(true);
		} else {
			commonItineraryParam.setIncludeTicketCharges(false);
		}
		commonItineraryParam.setIncludeBaggageAllowance(includeBaggageAllowance);
		commonItineraryParam.setStation(station);
		commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
		commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(isGroupPNR));
		commonItineraryParam.setIncludeTermsAndConditions(true);
		commonItineraryParam.setIncludeStationContactDetails(AppSysParamsUtil.showStationContactDetailsInItinerary());
		commonItineraryParam.setOperationType(operationType);

		String itinerary = "";
		CommonItineraryParamDTO commonItineraryParamDTO = null;
		List<CommonItineraryParamDTO> itineraryDtoList = null;
		if (isShowCustomizedItinerary()) {
			itineraryDtoList = CustomizedItineraryUtil.itineraryCompose(request, ACTION_PRINT, commonItineraryParam, null,
					pnrModesDTO);
		} else {
			commonItineraryParamDTO = commonItineraryParam;
		}

		if (itineraryDtoList != null) {
			for (CommonItineraryParamDTO itineraryParamDTO : itineraryDtoList) {
				itinerary = getItinerary(itineraryParamDTO, pnrModesDTO, getTrackInfo(), false, "", ACTION_PRINT);
			}
			request.setAttribute("reqItineraryBodyHtml", itinerary);
		} else {
			itinerary = getItinerary(commonItineraryParamDTO, pnrModesDTO, getTrackInfo(), false, "", ACTION_PRINT);
			request.setAttribute("reqItineraryBodyHtml", itinerary);
		}

	}

	private void getIndividualPaxItineraryInfo() throws ModuleException {
		String selectedPaxList = "";
		String pnr = hdnPNRNo;
		String station = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);

		LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, isGroupPNR, itineraryLanguage, request, false,
				AppSysParamsUtil.isPromoCodeEnabled());

		/*
		 * Need the origin country for itinerary. It is necessary to determine whether additional charge details will be
		 * printed based on the country's configuration.
		 */
		pnrModesDTO.setLoadOriginCountry(true);

		CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
		commonItineraryParam.setItineraryLanguage(itineraryLanguage);
		commonItineraryParam.setIncludePaxFinancials(true);
		commonItineraryParam.setIncludePaymentDetails(true);
		commonItineraryParam.setIncludeTicketCharges(false);
		commonItineraryParam.setIncludePaxContactDetails(AppSysParamsUtil.showPassenegerContactDetailsInItinerary());
		commonItineraryParam.setIncludeStationContactDetails(AppSysParamsUtil.showStationContactDetailsInItinerary());
		commonItineraryParam.setStation(station);
		commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
		commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(isGroupPNR));
		commonItineraryParam.setIncludeTermsAndConditions(true);
		commonItineraryParam.setOperationType(operationType);

		String itinerary = "";
		CommonItineraryParamDTO commonItineraryParamDTO = null;
		List<CommonItineraryParamDTO> itineraryDtoList = null;
		if (isShowCustomizedItinerary()) {
			itineraryDtoList = CustomizedItineraryUtil.itineraryCompose(request, ACTION_PRINT, commonItineraryParam, null,
					pnrModesDTO);
		} else {
			commonItineraryParamDTO = commonItineraryParam;
		}

		if (selectedPax == null)
			selectedPax = "";
		else {
			for (int i = 1; i < Integer.valueOf(selectedPax).intValue() + 1; i++) {
				selectedPaxList += i + ",";
			}
		}

		if (itineraryDtoList != null) {
			for (CommonItineraryParamDTO itineraryParamDTO : itineraryDtoList) {
				itinerary = getItinerary(itineraryParamDTO, pnrModesDTO, getTrackInfo(), true, selectedPaxList, ACTION_PRINT);
			}
			request.setAttribute("reqItineraryBodyHtml", itinerary);
		} else {
			itinerary = getItinerary(commonItineraryParamDTO, pnrModesDTO, getTrackInfo(), true, selectedPaxList, ACTION_PRINT);
			request.setAttribute("reqItineraryBodyHtml", itinerary);
		}

	}

	/**
	 * @return the hdnPNRNo
	 */
	public String getHdnPNRNo() {
		return hdnPNRNo;
	}

	/**
	 * @param hdnPNRNo
	 *            the hdnPNRNo to set
	 */
	public void setHdnPNRNo(String hdnPNRNo) {
		this.hdnPNRNo = hdnPNRNo;
	}

	/**
	 * @return the itineraryLanguage
	 */
	public String getItineraryLanguage() {
		return itineraryLanguage;
	}

	/**
	 * @param itineraryLanguage
	 *            the itineraryLanguage to set
	 */
	public void setItineraryLanguage(String itineraryLanguage) {
		this.itineraryLanguage = itineraryLanguage;
	}

	/**
	 * @return the includePaymentDetails
	 */
	public boolean isIncludePaymentDetails() {
		return includePaymentDetails;
	}

	/**
	 * @param includePaymentDetails
	 *            the includePaymentDetails to set
	 */
	public void setIncludePaymentDetails(boolean includePaymentDetails) {
		this.includePaymentDetails = includePaymentDetails;
	}

	/**
	 * @return the includeTicketCharges
	 */
	public boolean isIncludeTicketCharges() {
		return includeTicketCharges;
	}

	/**
	 * @param includeTicketCharges
	 *            the includeTicketCharges to set
	 */
	public void setIncludeTicketCharges(boolean includeTicketCharges) {
		this.includeTicketCharges = includeTicketCharges;
	}

	public boolean isGroupPNR() {
		return isGroupPNR;
	}

	public void setGroupPNR(boolean isGroupPNR) {
		this.isGroupPNR = isGroupPNR;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType
	 *            the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the selectedPax
	 */
	public String getSelectedPax() {
		return selectedPax;
	}

	/**
	 * @param selectedPax
	 *            the selectedPax to set
	 */
	public void setSelectedPax(String selectedPax) {
		this.selectedPax = selectedPax;
	}

	/**
	 * extracts the error message from a Exception
	 * 
	 * @param ex
	 * @return
	 */
	protected static String createErrorMessage(Exception ex) {
		String errMsg = null;

		if (ex instanceof ModuleException) {
			ModuleException me = (ModuleException) ex;
			errMsg = I18NUtil.getMessage(me.getExceptionCode());
		} else {
			errMsg = ex.getMessage();
		}

		return errMsg;

	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}

	private boolean isValidateRegisterUser() {
		boolean isRegUser = false;
		int customerID = SessionUtil.getCustomerId(request);

		if (customerID >= 0) {
			isRegUser = true;
		}
		return isRegUser;
	}

	public boolean isShowCustomizedItinerary() {
		return AppSysParamsUtil.getShowCustomizedItinerary();

	}

	public String getItinerary(CommonItineraryParamDTO commonItineraryParam, LCCClientPnrModesDTO pnrModesDTO,
			TrackInfoDTO trackInfoDTO, boolean individualPax, String selectedPax, String action) throws ModuleException {
		String itinerary = "";
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(hdnPNRNo);
		if (individualPax) {
			String paxs[] = selectedPax.split(",");
			for (String pax : paxs) {
				if (pax != null || !pax.equals("")) {
					commonItineraryParam.setSelectedPaxDetails(pax);
					if (action.equals(ACTION_EMAIL)) {
						ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
								trackInfoDTO);
					} else if (action.equals(ACTION_PRINT)) {
						itinerary += ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
								commonItineraryParam, reservationSearchDTO, trackInfoDTO);
					}
				}
			}
		} else {
			commonItineraryParam.setSelectedPaxDetails(selectedPax);
			if (action.equals(ACTION_EMAIL)) {
				ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
						trackInfoDTO);
			} else if (action.equals(ACTION_PRINT)) {
				itinerary += ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
						commonItineraryParam, reservationSearchDTO, trackInfoDTO);
			}
		}

		return itinerary;
	}

}