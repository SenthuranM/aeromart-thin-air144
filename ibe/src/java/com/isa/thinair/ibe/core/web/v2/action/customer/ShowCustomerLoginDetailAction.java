package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.SocialTO;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.FlyingSocialUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerLoginDetailAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ShowCustomerLoginDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private HashMap<String, String> jsonLabel;

	private Map<String, String> errorInfo;

	private boolean loyaltyManagmentEnabled = false;
	
	private boolean loyaltyEnable = false;

	private SocialTO socialLoginParams = new SocialTO();

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		String language = SessionUtil.getLanguage(request);
		try {
			String[] pagesIDs = { "Form", "PgLogin", "PgUserCommon" };
			jsonLabel = I18NUtil.getMessagesInJSON(language, pagesIDs);
			errorInfo = ErrorMessageUtil.getLoginClientErrors(request);
			loyaltyManagmentEnabled = AppSysParamsUtil.isLMSEnabled();
			loyaltyEnable = AppSysParamsUtil.isLoyalityEnabled();
			FlyingSocialUtil.composeSocialLoginParams(socialLoginParams, false);

		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, language);
			log.error("ShowCustomerLoginDetailAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public void setJsonLabel(HashMap<String, String> jsonLabel) {
		this.jsonLabel = jsonLabel;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public SocialTO getSocialLoginParams() {
		return socialLoginParams;
	}

	public void setSocialLoginParams(SocialTO socialLoginParams) {
		this.socialLoginParams = socialLoginParams;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

	public boolean isLoyaltyEnable() {
		return loyaltyEnable;
	}

	public void setLoyaltyEnable(boolean loyaltyEnable) {
		this.loyaltyEnable = loyaltyEnable;
	}
}
