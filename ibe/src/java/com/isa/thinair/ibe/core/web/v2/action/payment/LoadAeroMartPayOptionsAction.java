package com.isa.thinair.ibe.core.web.v2.action.payment;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AeroMartPayPaymentGatewayInfo;
import com.isa.thinair.ibe.api.dto.AeroMartPaySessionInfo;
import com.isa.thinair.ibe.api.dto.SearchPayOptionDTO;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayResponse;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayUtil;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidator;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidatorImpl;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.AEROMART_PAY_RESPONSE, value = StrutsConstants.Jsp.Common.AEROMART_PAY_RESPONSE),
		@Result(name = StrutsConstants.Result.AEROMART_PAY_OPTIONS, value = StrutsConstants.Jsp.Common.AEROMART_PAY_OPTIONS) })
public class LoadAeroMartPayOptionsAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(LoadAeroMartPayOptionsAction.class);

	public String execute() {

		String result = null;
		AeroMartPayResponse aeroMartPayResponse = null;
		AeroMartPayValidator aeroMartPayValidator = new AeroMartPayValidatorImpl();

		AeroMartPaySessionInfo sessionInfo = (AeroMartPaySessionInfo) request.getSession()
				.getAttribute(AeroMartPayConstants.SESSION_INFO);

		try {

			if (!aeroMartPayValidator.validateSessionDetails(sessionInfo)) {
				aeroMartPayResponse = AeroMartPayResponse.ERR_105;
				log.error(aeroMartPayResponse.getRespMessage() + ", orderID: " + sessionInfo != null
						? sessionInfo.getOrderID()
						: "(null)");
				throw new ModuleException(aeroMartPayResponse.getRespMessage());
			}
			String ipAddress = getClientInfoDTO().getIpAddress();
			SearchPayOptionDTO searchPayOptionTO = new SearchPayOptionDTO(sessionInfo,ipAddress);

			List<AeroMartPayPaymentGatewayInfo> paymentGatewayInfo = AeroMartPayUtil.getPaymentOptions(searchPayOptionTO);

			try {
				aeroMartPayValidator.validatePaymentGatewaysInfo(paymentGatewayInfo);
			} catch (Exception e) {
				aeroMartPayResponse = AeroMartPayResponse.ERR_52;
				throw new Exception();
			}

			List<String> messages = null;
			if (request.getSession().getAttribute(AeroMartPayConstants.MESSAGE) != null) {
				String message = (String) request.getSession().getAttribute(AeroMartPayConstants.MESSAGE);
				if (!StringUtil.isNullOrEmpty(message)) {
					messages = Arrays.asList(message);
				}
			}

			AeroMartPayUtil.setPaymentPageDisplayInfo(request, paymentGatewayInfo, messages);

			result = StrutsConstants.Result.AEROMART_PAY_OPTIONS;

		} catch (Exception e) {
			if (aeroMartPayResponse == null) {
				aeroMartPayResponse = AeroMartPayResponse.ERR_55;
			}
			log.error(aeroMartPayResponse.getRespMessage(), e);
			if (sessionInfo == null) {
				AeroMartPayUtil.setPaymentResponseAttributes(request, sessionInfo.getOrderID(), sessionInfo.getErrorURL(),
						aeroMartPayResponse.getRespCode(), aeroMartPayResponse.getRespMessage(), sessionInfo.getReferenceID(),
						sessionInfo.getTotalAmount());
			}

			AeroMartPayUtil.invalidateSession(request);

			if (sessionInfo == null || !StringUtil.isNullOrEmpty(sessionInfo.getErrorURL())) {
				result = StrutsConstants.Result.AEROMART_PAY_RESPONSE;
			} else {
				log.error("errorURL is null");
			}

		}

		return result;
	}
}
