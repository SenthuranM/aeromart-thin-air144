package com.isa.thinair.ibe.core.web.constants;

public class CustomerWebConstants {

	// Customer Constants
	public final static String LOGIN_FAILED = "loginFailed";
	public final static String CUSTOMER_EXISTS = "customerExists";
	public final static String LOYALTY_ACCOUNT_NOT_EXISTS = "loyalty.account.not.exist";
	public final static String LOYALTY_ACCOUNT_ALREADY_LINKED = "loyalty.account.already.linked";
	public final static String LOYALTY_ACCOUNT_NAMES_NOT_MATCHES = "errors.resv.lms.names.not.matched";
	public final static String LOYALTY_JOIN_NOT_ALLOW = "errors.resv.lms.enroll.not.allow";
	public final static String REQ_LOGGED_TYPE = "logType";

	// Kiosk Constants
	public final static String SYS_ACCESS_POINT = "IBE";
	public static final String REQ_KSK_USER = "sesKSKUser";
	public final static String SYS_ACCESS_POINT_KSK = "KSK";

	// Other constants
	public static final String APP_IBE_URLS_JS = "appIBEUrlsJs";
	public static final String APP_INDEX_JS_FILE = "appIndexJSFile";
	public static final String APP_AIRLINE_ID_STR = "appAirlineIdStr";
	public static final String APP_AIRLINE_NAME_STR = "appAirlineNameStr"; // not used
	public static final String APP_AIRLINE_HOME_TEXT_STR = "appAirlineHomeTextStr"; // get from the
																					// app
	public static final String APP_AIRLINE_HOME_URL_STR = "appAirlineHomeUrlStr"; // app
																					// parameters
	public static final String APP_RELEASE_VERSION = "appAccelAeroRelVersion";
	public static final String APP_RELEASE_VERSION_URL_KEY = "relversion";
	public static final String APP_ANALYTIC_JS = "appAnalyticJs";
	public static final String APP_ANALYTIC_ENABLE = "appAnalyticEnable";
	public static final String APP_CARRIER_CODE = "appCarrierCode";
	public static final String APP_TEMPLATE = "IBETemplate";
	public static final String APP_NAV_STEPS = "IBENavSteps";
	public static final String APP_BANNER_TYPE = "IBEBannerType";
	public static final String DYNAMIC_OND_URL = "dynamicOndUrl";

	// Social Site Constants
	public static final String FACEBOOK_USER_SHARED = "facebookUserShared";
	public static final String GRANT_SOCIAL_LOGIN = "grantSocialLogin";
	public static final String ENABLE_FACEBOOK_LOGIN = "enableFacebookLogin";
	public static final String ENABLE_LINKEDIN_LOGIN = "enableLinkedInLogin";
	public static final String FACEBOOK_APP_ID = "fbAppId";
	public static final String SOCIAL_SITE_TYPE = "socialSiteType";
	public static final String SOCIAL_PIC_URL = "socialPictureUrl";
	public static final String LINKEDIN_APP_ID = "linkedInAPIKey";
	
	public static final String APP_DEV_MODE = "isDevModeOn";
	public static final String ENABLE_MULTICITY_SEARCH = "enableMultiCity";
	
	//LMS COnstants
	public static final String NO_LMS_FAMILY_HEAD = "noFamilyHead";
	public static final String NO_LMS_DOB = "noDateOfBirth";
	public static final String LMS_DETAILS = "lmsDetails";
	public static final String NO_REFERED_LMS = "lmsReference";
}
