package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CancelAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(CancelAction.class);

	private boolean success = true;

	private String messageTxt;

	private String mode;

	private boolean groupPNR;

	private String pnr;

	private String version;

	private String modifySegmentRefNos;

	private String oldAllSegments;

	private String contactInfoJson;

	private String status;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		try {

			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
			CommonReservationContactInfo contactInfo = ReservationUtil.transformJsonContactInfo(contactInfoJson);

			if (resInfo == null) {
				throw new ModuleException("session.error");
			}
			Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getvalidateModifingSegment(colsegs,
					resInfo.getSegmentStatus(), modifySegmentRefNos);
			/*
			 * If no exceptions occurs during cancel segment and cancel reservation it will be takes as a successful
			 * refund.
			 */
			boolean successfullRefund = false;
			if (mode.trim().equals("cancelReservation")) {
				LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO.composeCancelReservationRequest(pnr,
						null, null, null, version, null);
				lccClientResAlterModesTO.setGroupPnr(groupPNR);
				LCCClientReservation reservation = ModuleServiceLocator.getAirproxyReservationBD().cancelReservation(
						lccClientResAlterModesTO, getClientInfoDTO(), getTrackInfo(), false, false);
				successfullRefund = reservation.isSuccessfulRefund();
				if (request.getSession().getAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO) == null
						&& reservation.getPNR() != null) {
					request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO, reservation.getPNR());
				}
				request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_MODE, "cancel");
			} else if (mode.trim().equals("cancelSegment")) {
				LCCClientResAlterModesTO lccClientResAlterModesTO = composeCancelSegmentRequest(pnr, groupPNR, colModSegs,
						colsegs, contactInfo, version, convertStatus(status));
				LCCClientReservation reservation = ModuleServiceLocator.getAirproxySegmentBD().cancelSegments(
						lccClientResAlterModesTO, getClientInfoDTO(), getTrackInfo());
				successfullRefund = reservation.isSuccessfulRefund();
			}
			/*
			 * Need to pass this to the other action. Session seem to be the only viable option.
			 */
			request.getSession().setAttribute(PaymentConstants.PARAM_SUCCESSFULL_REFUND, successfullRefund);

		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("CancelAction==>" + ex, ex);
		}
		return forward;
	}

	// this is not right. But as in ReservationUtil#createBookingDetails(LCCClientReservation lccClientReservation)
	// uses the hard coded reservation statuses and @ js pages they are referred, the following equallity was used.
	// TODO - Refactoring required :
	private String convertStatus(String status) {
		String reservationStatus = null;
		if ("CONFIRMED".equals(status)) {
			reservationStatus = ReservationInternalConstants.ReservationStatus.CONFIRMED;
		} else if ("ONHOLD".equals(status)) {
			reservationStatus = ReservationInternalConstants.ReservationStatus.ON_HOLD;
		} else if ("CANCELLED".equals(status)) {
			reservationStatus = ReservationInternalConstants.ReservationStatus.CANCEL;
		}
		return reservationStatus;
	}

	private LCCClientResAlterModesTO composeCancelSegmentRequest(String pnr, boolean isGroupPnr,
			Collection<LCCClientReservationSegment> oldPnrSegs, Collection<LCCClientReservationSegment> colExistingAllPnrSegs,
			CommonReservationContactInfo contactinfo, String version, String reservationStatus) {

		LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO.composeCancelSegmentRequest(pnr, oldPnrSegs,
				colExistingAllPnrSegs, null, null, null, version, reservationStatus);
		lccClientResAlterModesTO.setGroupPnr(isGroupPnr);

		LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
		lccClientSegmentAssembler.setContactInfo(contactinfo);
		lccClientResAlterModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);

		return lccClientResAlterModesTO;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param contactInfoJson
	 *            the contactInfoJson to set
	 */
	public void setContactInfoJson(String contactInfoJson) {
		this.contactInfoJson = contactInfoJson;
	}

}
