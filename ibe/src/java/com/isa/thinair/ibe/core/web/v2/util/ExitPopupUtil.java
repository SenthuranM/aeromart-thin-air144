package com.isa.thinair.ibe.core.web.v2.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.IbeExitUserDetailsDTO;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;

/**
 * Utility class for Ibe browser exit pop
 * 
 * @author Eshan
 */
public class ExitPopupUtil {

	private static Log log = LogFactory.getLog(ExitPopupUtil.class);
	
	private static final String EXIT_POPUP_SEARCH_DETAILS_EMAIL = "IBE_exitpopup_search_details_email";
	
	public static final String EXIT_POPUP_MODIFY_SEARCH_LABLE = "exitpopup.modify.search.lable";
	public static final String EXIT_POPUP_CHECK_OTHER_FARES_LABEL = "exitpopup.check.other.fares.lable";
	public static final String EXITPOPUP_SEND_SEARCH_DETAILS_LABEL = "exitpopup.send.search.details.email.label";
	public static final String EXITPOPUP_ERROR_ON_PAGE_LABEL = "exitpopup.error.on.page.label";
	public static final String EXITPOPUP_ENTER_PAX_EMAIL_LABEL = "exitpopup.pax.enter.email";
	public static final String EXITPOPUP_PAX_EMAIL_INVALID_LABEL = "exitpopup.pax.email.invalid";

	public static final String EXIT_POPUP_MODIFY_SEARCH_BTN = "exitpopup.modify.search.btn";
	public static final String EXIT_POPUP_CHECK_OTHER_FARES_BTN = "exitpopup.check.other.fares.btn";
	public static final String EXITPOPUP_SEND_SEARCH_DETAILS_BTN = "exitpopup.send.search.details.email.btn";
	public static final String EXITPOPUP_ERROR_ON_PAGE_BTN = "exitpopup.error.on.page.btn";
	public static final String EXITPOPUP_SEND_SEARCH_WITH_EMAIL_BTN = "exitpopup.send.search.details.with.email.btn";
	public static final String EXITPOPUP_DIALOG_TITLE = "exitpopup.dialog.title";
	
	public static final String RETURN_FLIGHT_SEARCH = "Return Flight Search";
	public static final String ONE_WAY_FLIGHT_SEARCH = "One Way Flight Search";
	

	private FlightSearchDTO searchParams;
	private String exitStep;
	private CustomerDTO customer;	
	private ContactInfoDTO contactInfo;

	public ExitPopupUtil withFlighSearchInfo(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
		return this;
	}

	public ExitPopupUtil withIbeExitStep(String exitStep) {
		this.exitStep = exitStep;
		return this;
	}

	public ExitPopupUtil withCustomer(CustomerDTO customer) {
		this.customer = customer;
		return this;
	}
	
	
	public ExitPopupUtil withContactInfoDTO(ContactInfoDTO contactInfoDTO){
		this.contactInfo = contactInfoDTO;
		return this;
	}
	
	public void sendFlightSearchDetailsEmail(HttpServletRequest request,IbeExitUserDetailsDTO exitDetails) {

		MessagingServiceBD messagingServiceBD = ModuleServiceLocator.getMessagingServiceBD();

		UserMessaging userMessaging = new UserMessaging();
		if(customer !=null){
			userMessaging.setToAddres(customer.getEmailId());
		}else{
			userMessaging.setToAddres(contactInfo.getEmailAddress());
		}

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		HashMap<String, Object> ibeExitEmailDataMap = getIbeExitEmailDataMap(request,exitDetails);

		Topic topic = new Topic();
		topic.setTopicParams(ibeExitEmailDataMap);
		topic.setTopicName(EXIT_POPUP_SEARCH_DETAILS_EMAIL);
		topic.setAttachMessageBody(true);
		Locale locale = new Locale(SessionUtil.getLanguage(request));
		topic.setLocale(locale);

		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		messagingServiceBD.sendMessage(messageProfile);

	}
	
	public IbeExitUserDetailsDTO saveIbeExitDetails(){
		PassengerBD passengerBD = ModuleServiceLocator.getPassengerBD();
		return passengerBD.saveIbeExitDetais(getExitPopupDetails());
	}
	
	public static Map<String,String> getExitPopupContent(HttpServletRequest request,Map<String,String> exitPopupContent){
		exitPopupContent.put("modifySearch",I18NUtil.getMessage(EXIT_POPUP_MODIFY_SEARCH_LABLE, SessionUtil.getLanguage(request)));
		exitPopupContent.put("otherFares",I18NUtil.getMessage(EXIT_POPUP_CHECK_OTHER_FARES_LABEL, SessionUtil.getLanguage(request)));
		exitPopupContent.put("sendSearchDetails",I18NUtil.getMessage(EXITPOPUP_SEND_SEARCH_DETAILS_LABEL, SessionUtil.getLanguage(request)));
		exitPopupContent.put("errorOnPage",I18NUtil.getMessage(EXITPOPUP_ERROR_ON_PAGE_LABEL, SessionUtil.getLanguage(request)));
		exitPopupContent.put("paxEmailLabel",I18NUtil.getMessage(EXITPOPUP_ENTER_PAX_EMAIL_LABEL, SessionUtil.getLanguage(request)));
		exitPopupContent.put("paxEmailInvaid",I18NUtil.getMessage(EXITPOPUP_PAX_EMAIL_INVALID_LABEL, SessionUtil.getLanguage(request)));
		
		exitPopupContent.put("modifySearchBtn",I18NUtil.getMessage(EXIT_POPUP_MODIFY_SEARCH_BTN, SessionUtil.getLanguage(request)));
		exitPopupContent.put("otherFaresBtn",I18NUtil.getMessage(EXIT_POPUP_CHECK_OTHER_FARES_BTN, SessionUtil.getLanguage(request)));
		exitPopupContent.put("sendSearchDetailsBtn",I18NUtil.getMessage(EXITPOPUP_SEND_SEARCH_DETAILS_BTN, SessionUtil.getLanguage(request)));
		exitPopupContent.put("errorOnPageBtn",I18NUtil.getMessage(EXITPOPUP_ERROR_ON_PAGE_BTN, SessionUtil.getLanguage(request)));
		exitPopupContent.put("sendSearchWithEmail",I18NUtil.getMessage(EXITPOPUP_SEND_SEARCH_WITH_EMAIL_BTN, SessionUtil.getLanguage(request)));
		
		exitPopupContent.put("dialogTitle", I18NUtil.getMessage(EXITPOPUP_DIALOG_TITLE, SessionUtil.getLanguage(request)));
		exitPopupContent.put("bestFaresUrl", AppSysParamsUtil.getIbeBestOffersUrl());
		exitPopupContent.put("surveyUrl", AppSysParamsUtil.getIbeSurveyUrl());
		exitPopupContent.put("selectedLang", SessionUtil.getLanguage(request).toLowerCase());
		
		return exitPopupContent;
	}

	public static void updateExitpopupTracking(Integer exitDetailsId){
		PassengerBD passengerBD = ModuleServiceLocator.getPassengerBD();
		IbeExitUserDetailsDTO ibeExitDetais = passengerBD.getIbeExitDetais(exitDetailsId);
		ibeExitDetais.setClickedExitDetailEmailLink(true);
		passengerBD.saveIbeExitDetais(ibeExitDetais);
	}
	
	private HashMap<String, Object> getIbeExitEmailDataMap(HttpServletRequest request,IbeExitUserDetailsDTO exitDetails) {
		HashMap<String, Object> ibeExitEmailDataMap = new HashMap<String, Object>();
		ibeExitEmailDataMap.put("departureDate", searchParams.getDepartureDate());
		ibeExitEmailDataMap.put("adultCount", searchParams.getAdultCount());
		ibeExitEmailDataMap.put("infantCount", searchParams.getInfantCount());
		ibeExitEmailDataMap.put("clidrenCount", searchParams.getChildCount());
		ibeExitEmailDataMap.put("returnFlag", searchParams.isReturnFlag() ? RETURN_FLIGHT_SEARCH : ONE_WAY_FLIGHT_SEARCH);
		ibeExitEmailDataMap.put("fromAirport", searchParams.getFromAirportName() != null ? searchParams.getFromAirportName()
				: searchParams.getFromAirport());
		ibeExitEmailDataMap.put("toAirport", searchParams.getToAirportName() != null ? searchParams.getToAirportName()
				: searchParams.getToAirport());
		if(searchParams.getReturnDate() !=null && !searchParams.getReturnDate().isEmpty()){
			ibeExitEmailDataMap.put("returnDate", searchParams.getReturnDate());
		}
		if(customer != null){
			String customerName= customer.getFirstName();
			ibeExitEmailDataMap.put("customerName", customerName );
		}else if(contactInfo.getFirstName() !=null && !contactInfo.getFirstName().isEmpty()){
			ibeExitEmailDataMap.put("customerName", contactInfo.getFirstName() );
		}
		String ibeUrl = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL);
		ibeExitEmailDataMap.put("ibeURL", ibeUrl);
		
		String encoding = "UTF-8";
		StringBuilder urlBuild = new StringBuilder();
		try {
			String hndParams = getHndParams(request);
			urlBuild.append(ibeUrl);
			urlBuild.append("/public/showReservation.action?hdnParamData=");
			urlBuild.append(URLEncoder.encode(hndParams, encoding));
			urlBuild.append("&ibeExitDetailsId="+exitDetails.getIbeExitDetailsId());

			ibeExitEmailDataMap.put("ibeSearchUrl", urlBuild.toString());
		}
		catch (UnsupportedEncodingException e) {
			log.error("error while encoding url for ibe exit details email : " + e);
		}
		return ibeExitEmailDataMap;
	}
	
	private IbeExitUserDetailsDTO getExitPopupDetails(){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		IbeExitUserDetailsDTO ibeExitUserDetailsDTO = new IbeExitUserDetailsDTO();
		
		if(customer != null){
			ibeExitUserDetailsDTO.setPaxEmail(customer.getEmailId());
			ibeExitUserDetailsDTO.setTitle(customer.getTitle());
			ibeExitUserDetailsDTO.setFirstName(customer.getFirstName());
			ibeExitUserDetailsDTO.setLastName(customer.getLastName());
			ibeExitUserDetailsDTO.setMobileNumber(customer.getMobile());
			ibeExitUserDetailsDTO.setTravelMobileNumber(customer.getTelephone());
		}else if(contactInfo !=null){
			ibeExitUserDetailsDTO.setPaxEmail(contactInfo.getEmailAddress());
			ibeExitUserDetailsDTO.setTitle(contactInfo.getTitle());
			ibeExitUserDetailsDTO.setFirstName(contactInfo.getFirstName());
			ibeExitUserDetailsDTO.setLastName(contactInfo.getLastName());
			ibeExitUserDetailsDTO.setNationality(contactInfo.getNationality());
			ibeExitUserDetailsDTO.setCountry(contactInfo.getCountry());
			ibeExitUserDetailsDTO.setLanguage(contactInfo.getPreferredLangauge());
			if(contactInfo.getmCountry() !=null && contactInfo.getmNumber() !=null){
				ibeExitUserDetailsDTO.setMobileNumber(contactInfo.getmArea() + contactInfo.getmNumber());
			}
			if(contactInfo.getlCountry() !=null && contactInfo.getlNumber() !=null){
				ibeExitUserDetailsDTO.setTravelMobileNumber(contactInfo.getlCountry() + contactInfo.getlNumber());
			}
		}
		ibeExitUserDetailsDTO.setPaxFlightSearchFrom(searchParams.getFromAirport());
		ibeExitUserDetailsDTO.setPaxFlightSearchTo(searchParams.getToAirport());
		ibeExitUserDetailsDTO.setSearchTime(new Date());
		ibeExitUserDetailsDTO.setPaxFlightSearchNoAdults(searchParams.getAdultCount());
		ibeExitUserDetailsDTO.setPaxFlightSearchNoChildren(searchParams.getChildCount());
		ibeExitUserDetailsDTO.setPaxFlightSearchNoOfInfants(searchParams.getInfantCount());
		ibeExitUserDetailsDTO.setSearchFlightType(searchParams.isReturnFlag() ? RETURN_FLIGHT_SEARCH : ONE_WAY_FLIGHT_SEARCH);
		ibeExitUserDetailsDTO.setExitStep(Integer.valueOf(exitStep.trim()));
		
		try {
			if (searchParams.getReturnDate() != null && !(searchParams.getReturnDate().isEmpty())) {
				ibeExitUserDetailsDTO.setPaxFlightSearchArrivalDate(format.parse(searchParams.getReturnDate()));
			}
			if (searchParams.getDepartureDate() != null && !(searchParams.getDepartureDate().isEmpty())) {
				ibeExitUserDetailsDTO.setPaxFlihtSearchDepartureDate(format.parse(searchParams.getDepartureDate()));
			}
		} catch (Exception ex) {
			log.error("cannot parse the date : " + ex.getMessage());
		}
		
		return ibeExitUserDetailsDTO;
	}
	
	private String getHndParams(HttpServletRequest request) {

		Integer adultCount = searchParams.getAdultCount() != null ? searchParams.getAdultCount() : 0;
		Integer childCount = searchParams.getChildCount() != null ? searchParams.getChildCount() : 0;
		Integer infantCount = searchParams.getInfantCount() != null ? searchParams.getInfantCount() : 0;

		StringBuilder hdnData = new StringBuilder();
		hdnData.append(SessionUtil.getLanguage(request)).append("^");
		hdnData.append("AS").append("^");
		hdnData.append(searchParams.getFromAirport()).append("^");
		hdnData.append(searchParams.getToAirport()).append("^");
		hdnData.append(searchParams.getDepartureDate()).append("^");
		hdnData.append(searchParams.getReturnDate()).append("^");
		hdnData.append(searchParams.getDepartureVariance()).append("^");
		hdnData.append(searchParams.getReturnVariance()).append("^");
		hdnData.append(searchParams.getSelectedCurrency()).append("^");
		hdnData.append(adultCount).append("^");
		hdnData.append(childCount).append("^");
		hdnData.append(infantCount).append("^");
		hdnData.append(searchParams.getFromAirportName()).append("^");
		hdnData.append(searchParams.getToAirportName()).append("^");
		hdnData.append(searchParams.isReturnFlag()).append("^^^^");
		hdnData.append(searchParams.getPromoCode());
		return hdnData.toString();
	}
}
