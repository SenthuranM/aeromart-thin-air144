package com.isa.thinair.ibe.core.web.v2.action.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.commons.api.constants.CommonsConstants.HumanVerificationConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.service.captchaService.CaptchaService;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ImageChapchaAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ImageChapchaAction.class);

	private boolean success = true;

	private String messageTxt;

	private String captcha = "";

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {

			if (AppSysParamsUtil.getImageCapchaDisplayAvailabilitySearch() || AppSysParamsUtil.isIBEHumanVerficationEnabled()) {
				boolean captchaPassed = CaptchaService.getInstance().validateResponseForID(request.getSession().getId(), captcha);
				if (captchaPassed) {
					if (AppSysParamsUtil.getImageCapchaDisplayAvailabilitySearch()) {
						IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
						ibeResInfo.setCaptchaValidated(true);
					}

					if (AppSysParamsUtil.isIBEHumanVerficationEnabled()) {
						request.getSession().setAttribute(HumanVerificationConstants.CAPTCHA_VALIDATED, true);
						request.getSession().setAttribute(HumanVerificationConstants.MAX_SESSION_HITS, 0);
					}
					messageTxt = "valid";
				} else {
					messageTxt = "Enter the word displayed in the image to view";
				}
			} else {
				success = true;
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ImageChapchaAction==>" + ex, ex);
		}

		return result;

	}

	public String onHoldBooking() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			if (AppSysParamsUtil.isOHDImageCapcha()) {
				boolean captchaPassed = CaptchaService.getInstance().validateResponseForID(request.getSession().getId(), captcha);
				if (captchaPassed) {
					ibeResInfo.setCaptchaValidated(true);
					messageTxt = "valid";
				} else {
					messageTxt = "Enter the word displayed in the image to view";
				}
			} else {
				success = true;
			}

		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ImageChapchaAction==>" + ex, ex);
		}

		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

}
