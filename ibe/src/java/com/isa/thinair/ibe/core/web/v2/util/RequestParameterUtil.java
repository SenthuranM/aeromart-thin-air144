package com.isa.thinair.ibe.core.web.v2.util;

import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants.PaymentFlow;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
/**
 * 
 * @author Pradeep Karunanayake
 *
 */
public class RequestParameterUtil {
	
	public static boolean isPaymentRetry(HttpServletRequest request) {
		boolean isRetry = false;
		if (PaymentFlow.PAYMENT_RETRY.toString().equalsIgnoreCase(request.getParameter(ReservationWebConstnts.PARAM_PAYMENT_FLOW))) {
			isRetry = true;
		}
		return isRetry;
	}
	
	public static  Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

}
