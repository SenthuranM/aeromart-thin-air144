package com.isa.thinair.ibe.core.web.v2.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.JourneyInfo;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.api.dto.SocialTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

public class FlyingSocialUtil {

	/**
	 * Live feeds just after the Itinerary page
	 * 
	 * @param socialTo
	 * @param flightSegmentDetails
	 */
	public static void displaySocialLiveFeeds(SocialTO socialTo, Collection<SegInfoDTO> flightSegmentDetails) {

		socialTo.setMarketingPopupDelay(AppSysParamsUtil.getMarketingPopUpDelay());

		boolean showLiveFeeds = false;

		if (AppSysParamsUtil.isShowFacebook()) {
			showLiveFeeds = true;
			socialTo.setShowFacebook(true);
			socialTo.setFacebookAppId(AppSysParamsUtil.getFacebookDialogFeedAppId());
		}

		if (AppSysParamsUtil.isShowTwitter()) {
			showLiveFeeds = true;
			socialTo.setShowTwitter(true);
		}

		if (AppSysParamsUtil.isShowGooglePlus()) {
			showLiveFeeds = true;
			socialTo.setShowGooglePlus(true);
		}
		if (AppSysParamsUtil.isShowLinkedln()) {
			showLiveFeeds = true;
			socialTo.setShowLinkedln(true);
		}

		if (AppSysParamsUtil.isShowInlineSocialFeeds()) {
			socialTo.setShowInlinePanel(true);
		}

		if (showLiveFeeds && !AppSysParamsUtil.getMarketingPopUpDelay().equals("-1")) {
			composeJourneyInformation(socialTo, flightSegmentDetails);
		}
	}

	private static void composeJourneyInformation(SocialTO socialTo, Collection<SegInfoDTO> colflightSegmentDetails) {

		List<SegInfoDTO> flightSegmentDetails = (List<SegInfoDTO>) colflightSegmentDetails;
		Collections.sort(flightSegmentDetails, new Comparator<SegInfoDTO>() {
			@Override
			public int compare(SegInfoDTO seg1, SegInfoDTO seg2) {
				return seg1.getJourneySequence().compareTo(seg2.getJourneySequence());
			}
		});

		SortedMap<Integer, List<SegInfoDTO>> journeySegments = new TreeMap<Integer, List<SegInfoDTO>>();
		Iterator<SegInfoDTO> segments = flightSegmentDetails.iterator();
		while (segments.hasNext()) {
			SegInfoDTO segment = segments.next();
			Integer journeySequence = segment.getJourneySequence();
			if (!journeySegments.containsKey(journeySequence)) {
				journeySegments.put(journeySequence, new ArrayList<SegInfoDTO>());
			}
			journeySegments.get(journeySequence).add(segment);
		}

		Integer firstSeq = journeySegments.firstKey();
		Integer lastSeq = journeySegments.lastKey();

		List<SegInfoDTO> outbounds = journeySegments.get(firstSeq);
		socialTo.setOutbound(getDestinationInfo(outbounds, false));

		if (lastSeq != null && !firstSeq.equals(lastSeq)) {
			List<SegInfoDTO> inbound = journeySegments.get(lastSeq);
			socialTo.setInbound(getDestinationInfo(inbound, true));
		}

	}

	private static JourneyInfo getDestinationInfo(List<SegInfoDTO> segments, boolean inbound) {
		JourneyInfo journeyInfo = new JourneyInfo();

		journeyInfo.setStationStartCode(getDestination(segments, false, true));
		journeyInfo.setStationEndCode(getDestination(segments, true, true));

		boolean showDetailedInfoInFacebook = AppSysParamsUtil.isShowDetailedInfoInFacebook();
		if (showDetailedInfoInFacebook) {
			journeyInfo.setAirportStartCode(getDestination(segments, false, false));
			journeyInfo.setAirportEndCode(getDestination(segments, true, false));
		} else {
			journeyInfo.setAirportStartCode(journeyInfo.getStationStartCode());
			journeyInfo.setAirportEndCode(journeyInfo.getStationEndCode());
		}

		if (inbound) {
			journeyInfo.setJourneyDate(segments.get(segments.size() - 1).getArrivalDate());
		} else {
			journeyInfo.setJourneyDate(segments.get(0).getDepartureDate());
		}

		return journeyInfo;
	}

	/**
	 * returnStation = true ->return station name
	 * 
	 * returnStation = false ->return descriptive airport name
	 * 
	 * @param segments
	 * @param endNode
	 * @param returnStation
	 * @return
	 */
	private static String getDestination(List<SegInfoDTO> segments, boolean endNode, boolean returnStation) {
		String code = null;
		Station station = null;
		if (endNode) {
			SegInfoDTO lastSegment = segments.get(segments.size() - 1);
			String[] codesInLastSeg = returnStation ? lastSegment.getSegmentCodeOnly().split("/") : lastSegment.getSegmentCode()
					.split("/");
			code = codesInLastSeg[codesInLastSeg.length - 1];
		} else {
			SegInfoDTO firstSegment = segments.get(0);
			code = returnStation ? firstSegment.getSegmentCodeOnly().split("/")[0] : firstSegment.getSegmentCode().split("/")[0];
		}

		if (returnStation) {
			try {
				station = ModuleServiceLocator.getLocationBD().getStation(code);
			} catch (ModuleException e) {

			}
			if (station != null) {
				code = station.getStationName();
			}
		}
		return code;
	}

	public static void composeSocialLoginParams(SocialTO socialLoginParams, boolean isRegister) {
		socialLoginParams.setShowFacebook(AppSysParamsUtil.isSocialLoginFromFacebookEnabled());
		socialLoginParams.setShowLinkedln(AppSysParamsUtil.isSocialLoginFromLinkedInEnabled());
		socialLoginParams.setFacebookAppId(AppSysParamsUtil.getFacebookAppIdForSocialLogin());
		socialLoginParams.setLinkedInAppId(AppSysParamsUtil.getLinkedInAPIKeyForSocialLogin());
		socialLoginParams.setRegister(isRegister);
	}

	public static SocialTO composeSocialSeatingParams() {
		SocialTO seatingParams = new SocialTO();
		seatingParams.setShowFacebook(AppSysParamsUtil.isSocialSeatingEnabledWithFacebook());
		seatingParams.setShowLinkedln(AppSysParamsUtil.isSocialSeatingEnablesWithLinkedIn());
		seatingParams.setFacebookAppId(AppSysParamsUtil.getFacebookAppIdForSocialSeating());
		seatingParams.setLinkedInAppId(AppSysParamsUtil.getLinkedInApiIDForSocialSeating());
		return seatingParams;
	}

}
