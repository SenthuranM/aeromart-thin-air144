package com.isa.thinair.ibe.core.service;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.platform.core.controller.ModuleFramework;

public class StartupServlet extends HttpServlet {

	private static final long serialVersionUID = -3586197310811722560L;

	private static GlobalConfig globalConfig = null;

	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		ModuleFramework.startup();

		globalConfig = IBEModuleUtils.getGlobalConfig();

		setGlobals();
	}

	public void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, java.io.IOException {

	}

	private void setGlobals() {
		setIBEUrlJS();
		setSystemData();
		setIndexDummyJSHtml();
		setReleaseVersion();
		setConfig();
		setGoogleAnalyticData();
		setGoogleAnalyticEnability();
		setSessionXpired();
		setIsIBEVersionTwo();
		setIBENumberOfSteps();
		setIBEBannerType();
		setSessionTimeout();
		setTimeoutBannerPopupTime();
		setDynamicOndUrl();
		setIsDevModeOn();
	}
	
	private void setIsDevModeOn(){
		getServletContext().setAttribute(CustomerWebConstants.APP_DEV_MODE ,AppParamUtil.isInDevMode());
	}

	private void setIsIBEVersionTwo() {
		getServletContext().setAttribute(CustomerWebConstants.APP_TEMPLATE,
				globalConfig.getBizParam(SystemParamKeys.IBE_TEMPLATE_CODE).trim());
	}

	private void setIBENumberOfSteps() {
		getServletContext().setAttribute(CustomerWebConstants.APP_NAV_STEPS, AppSysParamsUtil.getIBENavSteps());
	}

	private void setIBEBannerType() {
		getServletContext().setAttribute(CustomerWebConstants.APP_BANNER_TYPE, AppSysParamsUtil.getIBEBannerType());
	}

	private void setConfig() {
		getServletContext().setAttribute(ReservationWebConstnts.APP_IBECONFIG_JS, JavaScriptGenerator.createIBEConfigJS());
	}

	private void setSessionTimeout() {
		getServletContext().setAttribute(ReservationWebConstnts.APP_KIOSK_SESSION_TIMEOUT,
				globalConfig.getBizParam(SystemParamKeys.KIOSK_SES_TIMEOUT));
	}

	private void setIBEUrlJS() {
		StringBuffer sb = new StringBuffer();

		sb.append("var nonSecureUrl='");
		sb.append(AppParamUtil.getNonsecureIBEUrl());
		sb.append("';");

		sb.append("var secureUrl='");
		sb.append(AppParamUtil.getSecureIBEUrl());
		sb.append("';");

		getServletContext().setAttribute(CustomerWebConstants.APP_IBE_URLS_JS, sb.toString());

	}

	private void setSystemData() {
		getServletContext().setAttribute(CustomerWebConstants.APP_AIRLINE_ID_STR,
				globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER));
	}

	private void setIndexDummyJSHtml() {
		StringBuffer sb = new StringBuffer();

		sb.append("<script src='../js/index");
		sb.append(globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE));
		sb.append(".js' type='text/javascript'></script>");

		getServletContext().setAttribute(CustomerWebConstants.APP_INDEX_JS_FILE, sb.toString());

	}

	private void setReleaseVersion() {
		if (SystemPropertyUtil.isCreateNewBuildIdWithEveryRestart()) {
			getServletContext().setAttribute(CustomerWebConstants.APP_RELEASE_VERSION,
					CustomerWebConstants.APP_RELEASE_VERSION_URL_KEY + "=" + new Date().getTime());
		} else {
			getServletContext().setAttribute(CustomerWebConstants.APP_RELEASE_VERSION,
					CustomerWebConstants.APP_RELEASE_VERSION_URL_KEY + "=" + globalConfig.getLastBuildVersion());
		}
	}

	private void setDynamicOndUrl() {
		if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
			getServletContext().setAttribute(CustomerWebConstants.DYNAMIC_OND_URL, AppSysParamsUtil.getDynamicOndListUrl());
		}
	}

	private void setGoogleAnalyticData() {
		StringBuffer sb = new StringBuffer();

		if (AppSysParamsUtil.isGoogleAnalyticsEnabled()) {

			sb.append("<script type='text/javascript'>");
			sb.append("var _gaq = _gaq || [];");
			sb.append("_gaq.push(['_setAccount', '" + AppSysParamsUtil.getGoogleAnalyticsId() + "']);");
			sb.append("_gaq.push(['_setDomainName', '." + AppSysParamsUtil.getAirlineDomainName() + "']);");

			sb.append("(function() {");
			sb.append("var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;");
			sb.append("ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';");
			sb.append("var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);");
			sb.append("})();");
			sb.append("</script>");

			/*
			 * Old Google Analytics Code
			 * 
			 * sb.append("<script type='text/javascript'>");
			 * sb.append("var gaJsHost = (('https:' == document.location.protocol) ? 'https://ssl.' : 'http://www.');");
			 * sb.append(
			 * " document.write(unescape(\"%3Cscript src='../js/common/ga.js' type='text/javascript'%3E%3C/script%3E\"));"
			 * ); sb.append("</script>"); sb.append("<script type='text/javascript'>");
			 * sb.append("var pageTracker = _gat._getTracker(\"" + AppSysParamsUtil.getGoogleAnalyticsId() + "\");");
			 */
			// -- sb.append("var pageTracker = _gat._getTracker(\"UA-1319699-9\");");
			/* sb.append("pageTracker._setDomainName(\"." + AppSysParamsUtil.getAirlineDomainName() + "\");"); */
			// -- sb.append("pageTracker._setDomainName(\".isaaviations.com\");");
			/*
			 * sb.append("pageTracker._setAllowHash(\"false\");"); sb.append("pageTracker._initData();");
			 */
			// -- sb.append("pageTracker._trackPageview();");
			/* sb.append("</script>"); */
		}
		getServletContext().setAttribute(CustomerWebConstants.APP_ANALYTIC_JS, sb.toString());
	}

	private void setGoogleAnalyticEnability() {
		String strAnalytic = AppSysParamsUtil.isGoogleAnalyticsEnabled() ? "true" : "false";
		getServletContext().setAttribute(CustomerWebConstants.APP_ANALYTIC_ENABLE, strAnalytic);
	}

	/**
	 * Sets all the active carriers in the system.
	 */
	private void setSessionXpired() {
		Collection colCarrierCode = globalConfig.getActiveSysCarriersMap().keySet();

		int i = 0;
		String carrierCode = "";
		String strHomeurl = "";
		StringBuilder sb = new StringBuilder();
		for (Iterator iter = colCarrierCode.iterator(); iter.hasNext();) {
			String defCarrier = "false";
			carrierCode = (String) iter.next();
			strHomeurl = globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, carrierCode);
			if (carrierCode.equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE))) {
				defCarrier = "true";
			}

			sb.append("arrCarriers[" + i + "] = new Array('" + carrierCode + "', '" + strHomeurl + "', '" + defCarrier + "');");
			i++;
		}
		getServletContext().setAttribute(CustomerWebConstants.APP_CARRIER_CODE, sb.toString());
	}

	private void setTimeoutBannerPopupTime() {
		getServletContext().setAttribute(ReservationWebConstnts.APP_IBE_SESSION_TIMEOUT_POPUP,
				globalConfig.getBizParam(SystemParamKeys.SESSION_TIMEOUT_BANNER_POPUP));
	}
}
