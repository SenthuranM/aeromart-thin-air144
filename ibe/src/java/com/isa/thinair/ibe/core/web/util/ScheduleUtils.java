package com.isa.thinair.ibe.core.web.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ScheduleUtils {

	/**
	 * Formats specified date with specified String format
	 * 
	 * @param utilDate
	 *            the Date need to be Formatted
	 * @param fmt
	 *            the Format
	 * @return String the Formatted Date
	 */
	public static String formatDate(Date utilDate, String fmt) {

		String strDate = "";
		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			strDate = sdFmt.format(utilDate);
		}

		return strDate;
	}

	/**
	 * Method to Check Empty or not null
	 * 
	 * @param str
	 *            the String need to be Checked
	 * @return boolean true false
	 */
	public static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("All") || str.trim().equals("-1"));
	}

}
