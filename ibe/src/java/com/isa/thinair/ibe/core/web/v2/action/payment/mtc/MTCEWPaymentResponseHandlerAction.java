package com.isa.thinair.ibe.core.web.v2.action.payment.mtc;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.paymentbroker.api.constants.PaymentAPIConsts;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * 
 * @author Primal Suaris
 *
 */
/**
 * This is specific servlet related to MTC online payment gateway integration.
 * MTCEWPaymentResponseHandlerAction?CartId=123&TotalmountTx=2050.75&Checksum=2cf0200e671964339236a857a2580f82
 * 
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
public class MTCEWPaymentResponseHandlerAction extends BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(MTCEWPaymentResponseHandlerAction.class);

	public String execute() {
		if (log.isDebugEnabled())
			log.debug("###Start.." + request.getRequestedSessionId());

		response.setContentType("text/html;charset=UTF-8");
		String cartID = request.getParameter("cartId");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String totalAmountTx = request.getParameter("totalAmountTx");
		String orderNumber = request.getParameter("orderNumber");
		String paymentSeqID = request.getParameter("pmtSeqId");
		String approvalCode = request.getParameter("approvalCode");
		String paymentCardType = request.getParameter("paymentCardType");
		String ThreeDSecureResult = request.getParameter("3DSecureResult");
		String eciValue = request.getParameter("eciValue");
		String bankName = request.getParameter("bankName");
		String expirationDate = request.getParameter("expirationDate");
		String ccn = request.getParameter("ccn");
		String paymentBrandName = request.getParameter("paymentBrandName");
		String checksum = request.getParameter("checksum");
		String apiChecksum = request.getParameter("apiChecksum");

		String bookURL = AppSysParamsUtil.getSecureIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_MTC_EW_URL_SUFFIX;		

		Map<String, String> receiptyMap = getReceiptMap(request);
		Date date = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String today = formatter.format(date);
		try {
			PrintWriter out = response.getWriter();
			// Get message details sent from MTC Offline Gateway

			// Logging Request parameters
			StringBuilder strLog = new StringBuilder();
			strLog.append("cartID:").append(cartID).append(", ");
			strLog.append("Name :").append(name).append(", ");
			strLog.append("Email:").append(email).append(", ");
			strLog.append("totalAmountTx:").append(totalAmountTx).append(", ");
			strLog.append("OrderNumber:").append(orderNumber).append(", ");
			strLog.append("PaymentSeqID:").append(paymentSeqID).append(", ");
			strLog.append("approvalCode:").append(approvalCode).append(", ");
			strLog.append("paymentCardType:").append(paymentCardType).append(", ");
			strLog.append("ThreeDSecureResult:").append(ThreeDSecureResult).append(", ");
			strLog.append("eciValue:").append(eciValue).append(", ");
			strLog.append("bankName:").append(bankName).append(", ");
			strLog.append("expirationDate:").append(expirationDate).append(", ");
			strLog.append("ccn:").append(ccn).append(", ");
			strLog.append("paymentBrandName:").append(paymentBrandName).append(", ");
			strLog.append("apiChecksum:").append(apiChecksum).append(", ");
			strLog.append("checksum:").append(checksum).append(", ");
			strLog.append("SessionID:").append(request.getRequestedSessionId()).append(", ");

			log.info(strLog.toString());

			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					13, "MAD");
			PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			String storeID = ipgProps.getProperty("merchantId");
			String merchantSymenticKey = ipgProps.getProperty("merchantAccessCode");

			String checksumGenarated = "";
			// String dataMD5 = ipgURLPay + strStoreId + strCartId + strCount + strAmountTX + strEmail +
			// strSLKSecretKey;

			StringBuilder strCheckSumLOG = new StringBuilder("Checksum genarater params ");
			strCheckSumLOG.append("bookURL:").append(bookURL);
			strCheckSumLOG.append("storeID:").append(storeID);
			strCheckSumLOG.append("cartID:").append(cartID);
			strCheckSumLOG.append("totalAmountTx:").append(totalAmountTx);
			strCheckSumLOG.append("email:").append(email);
			strCheckSumLOG.append("merchantSymenticKey:").append(merchantSymenticKey);

			log.info(strCheckSumLOG.toString());

			String dataMD5 = bookURL + storeID + cartID + totalAmountTx + email + merchantSymenticKey;

			try {
				String uRLEncodeData = URLEncoder.encode(dataMD5, "UTF-8");
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.reset();
				md.update((uRLEncodeData).getBytes());
				checksumGenarated = StringUtil.byteArrayToHexString(md.digest());
				checksumGenarated = checksumGenarated.toLowerCase();
				String check = checksumGenarated;
				log.info("Genarated Checksum :" + checksumGenarated);
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("");
			}

			boolean isAccepted = false;
			boolean isResConfirmationSuccess = false;
			// TODO: if CheckSum not match respondcode = 0;
			// TODO: if after the onhold period respondcode = 4;
			// TODO: if payment is acceptable respondcode = 1;
			// TODO: if already Paid respondcode = 2;
			// if (!checksum.equalsIgnoreCase(checksum)) {
			if (!checksumGenarated.equals(checksum)) {
				out.print("0;" + cartID + ";" + today + ";1");
				out.flush();
				out.close();
				return null;
			} else {
				isAccepted = true;
			}
			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			int tempPayId = Integer.parseInt(getTempID(cartID));
			Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);

			ipgResponseDTO.setRequestTimsStamp(requestTime);
			ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
			ipgResponseDTO.setTemporyPaymentId(tempPayId);
			if (isAccepted) {
				ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);
			} else {
				ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_REJECTED);
			}
			// Update receipt map for as intermediate response

			receiptyMap.put("responseType", "PAYMENTSUCCESS");
			// ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap, ipgResponseDTO,
			// ipgIdentificationParamsDTO);
			ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(receiptyMap, ipgResponseDTO,
					ipgIdentificationParamsDTO);

			int paymentBrokerRefNo = ipgResponseDTO.getPaymentBrokerRefNo();
			Map<Integer, CommonCreditCardPaymentInfo> tempPaymetTNXID = new HashMap<Integer, CommonCreditCardPaymentInfo>();
			tempPaymetTNXID.put(tempPayId, new CommonCreditCardPaymentInfo());

			out.print("1;" + cartID + ";" + today + ";1");
			out.flush();
			out.close();

			if (log.isDebugEnabled())
				log.debug("###END.." + request.getRequestedSessionId());

		} catch (Exception ex) {
			String mtcResponseCode = "0";

			if (ex instanceof ModuleException) {
				if (((ModuleException) ex).getExceptionCode().equals("Transaction.Already.Updated")) {

				}

				PrintWriter out;
				try {
					out = response.getWriter();

					out.print(mtcResponseCode + ";" + cartID + ";" + today + ";1");
					out.flush();
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					log.error("###Error...## Transaction.Already.Updated", ex);
				}

				log.error("###Error...##" + request.getRequestedSessionId() + "paymentid" + getTempID(cartID) + "Result"
						+ "Succes" + checksum, ex);
			}
		}
		return null;
	}

	private Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	private String getTempID(String cartID) {
		String tempPayID = "";
		if (cartID != null) {
			String tempPayIDPNR = cartID.substring(1); // Remove the AppIndicator character result contains temPayid-PNR
			tempPayID = tempPayIDPNR.split("-")[0]; // get the tempPayID from temPayid-PNR
		}
		return tempPayID;
	}

}
