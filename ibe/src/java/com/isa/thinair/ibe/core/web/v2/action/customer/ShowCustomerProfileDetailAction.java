package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.api.dto.FamilyMemberDTO;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerProfileDetailAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowCustomerProfileDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private String titleInfo;

	private CustomerDTO customer;

	private List questionInfo;

	private String countryPhoneInfo;

	private String areaPhoneInfo;

	private Map<String, String> errorInfo;

	private HashMap<String, String> jsonLabel;

	private List<String[]> ibeSuportLans;

	private List countryInfo;

	private List nationalityInfo;
	
	private LmsMemberDTO lmsDetails;
	
	private List<SeatType> seatPreferencesList; 
	
	private List<LCCMealDTO> mealList = null;
	
	private String preferredSeatType;
	
	private List<CustomerPreferredMealDTO> preferredMeals = null;
	
	private List relationshipList;
	
	private List relationshipTitleList;
	
	private List<FamilyMemberDTO> familyMemberDTOList;
	
	private boolean seatMealPerefernceEnabled;
	
	private boolean addFamilyMembersEnabled;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "Form", "PgUser", "PgUserUpdate" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);

			Customer customerModel = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			customer = CustomerUtilV2.getCustomerDTO(customerModel);
			countryInfo = SelectListGenerator.getCountryList();
			//nationalityInfo = SelectListGenerator.getNationalityList();
			titleInfo = JavaScriptGenerator.getTiteVisibilityHtml();
			questionInfo = SelectListGenerator.getSecreatQuestionList();
			errorInfo = ErrorMessageUtil.getCustomerProfileErrors(request);
			String[] phoneInfoArray = SelectListGenerator.createTelephoneString();
			countryPhoneInfo = phoneInfoArray[0];
			areaPhoneInfo = phoneInfoArray[1];
			ibeSuportLans = I18NUtil.getIBESupportedLanguages(request);
			
			seatMealPerefernceEnabled = AppSysParamsUtil.isSeatMealPreferenceEnabled();
			addFamilyMembersEnabled = AppSysParamsUtil.isAddFamilyMemberEnabled();
			
			if(seatMealPerefernceEnabled){
				seatPreferencesList = CustomerUtil.getPreferenceSeatTypes();
				mealList = ModuleServiceLocator.getAirproxyAncillaryBD().getMealsForPreference(strLanguage);
				
				preferredSeatType = CustomerUtil.getCustomerPreferredSeatType(SessionUtil.getCustomerId(request));
				preferredMeals = CustomerUtil.getCustomerPreferredMeals(SessionUtil.getCustomerId(request));
			}
			
			if(addFamilyMembersEnabled){
				setFamilyMemberDTOList();
			}else{
				setNationalityInfo();
			}
			
			LmsMember lmsMember = customerModel.getLMSMemberDetails();
			if(lmsMember!=null){
				setLmsDetails(CustomerUtilV2.getLmsMemberDTO(lmsMember));
			}
			
		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowCustomerProfileDetailAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowCustomerProfileDetailAction==>", ex);
		}
		return forward;
	}
	
	public String setFamilyMemberDTOList(){
		String forward = StrutsConstants.Result.SUCCESS;
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		familyMemberDTOList = new ArrayList<FamilyMemberDTO>();
		try {
			setRelationshipList();
			setNationalityInfo();
			setRelationshipTitleList();
			List<FamilyMember> familyMemberList = customerDelegate.getFamilyMembers(SessionUtil.getCustomerId(request));
			for(FamilyMember familyMember : familyMemberList){
				familyMemberDTOList.add(CustomerUtilV2.getFamilyMemberDTO(familyMember));
			}
		} catch (ModuleException e) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("Family Member search failed ==> ", e);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public List getCountryInfo() {
		return countryInfo;
	}

	public List getNationalityInfo() {
		return nationalityInfo;
	}

	public String getTitleInfo() {
		return titleInfo;
	}

	public void setTitleInfo(String titleInfo) {
		this.titleInfo = titleInfo;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public List getQuestionInfo() {
		return questionInfo;
	}

	public void setQuestionInfo(List questionInfo) {
		this.questionInfo = questionInfo;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(Map<String, String> errorInfo) {
		this.errorInfo = errorInfo;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public List<String[]> getIbeSuportLans() {
		return ibeSuportLans;
	}

	public String getCountryPhoneInfo() {
		return countryPhoneInfo;
	}

	public String getAreaPhoneInfo() {
		return areaPhoneInfo;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public List<SeatType> getSeatPreferencesList() {
		return seatPreferencesList;
	}

	public void setSeatPreferencesList(List<SeatType> seatPreferencesList) {
		this.seatPreferencesList = seatPreferencesList;
	}

	public List<LCCMealDTO> getMealList() {
		return mealList;
	}

	public String getPreferredSeatType() {
		return preferredSeatType;
	}
	
	public List<CustomerPreferredMealDTO> getPreferredMeals() {
		return preferredMeals;
	}

	public List getRelationshipList() {
		return relationshipList;
	}

	public List<FamilyMemberDTO> getFamilyMemberDTOList() {
		return familyMemberDTOList;
	}
	
	public void setRelationshipList() throws ModuleException{
		relationshipList = SelectListGenerator.getRelationshipList();
	}
	
	public void setNationalityInfo() throws ModuleException{
		nationalityInfo = SelectListGenerator.getNationalityList();
	}

	public List getRelationshipTitleList() {
		return relationshipTitleList;
	}

	public void setRelationshipTitleList() throws ModuleException {
		relationshipTitleList = SelectListGenerator.getRelationshipTitleList();
	}

	public boolean isSeatMealPerefernceEnabled() {
		return seatMealPerefernceEnabled;
	}

	public boolean isAddFamilyMembersEnabled() {
		return addFamilyMembersEnabled;
	}
	
}
