package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.api.dto.SocialTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.FlyingSocialUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerRegisterDetailAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowCustomerRegisterDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private List countryInfo;

	private List nationalityInfo;

	private List docTypeInfo;

	private String titleInfo;

	private List questionInfo;

	private String countryPhoneInfo;

	private String areaPhoneInfo;

	private Map<String, String> errorInfo;

	private HashMap<String, String> jsonLabel;

	private List<String[]> ibeSuportLans;

	private boolean loyaltyEnable;

	private Map<String, UserRegConfigDTO> userRegConfig = new HashMap<String, UserRegConfigDTO>();

	private Map<String, List<String>> validationGroup = new HashMap<String, List<String>>();

	private SessionTimeoutDataDTO timeoutDTO;

	private SocialTO socialLoginParams = new SocialTO();

	private boolean loyaltyManagmentEnabled = false;

	private boolean integrateMashreqWithLMSEnabled = false;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "Form", "PgUser", "PgUserCommon" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);

			countryInfo = SelectListGenerator.getCountryList();
			docTypeInfo = SelectListGenerator.getDocTypeList();
			nationalityInfo = SelectListGenerator.getNationalityList();
			titleInfo = JavaScriptGenerator.getTiteVisibilityHtml();
			questionInfo = SelectListGenerator.getSecreatQuestionList();
			errorInfo = ErrorMessageUtil.getCustomerProfileErrors(request);
			String[] phoneInfoArray = SelectListGenerator.createTelephoneString();
			countryPhoneInfo = phoneInfoArray[0];
			areaPhoneInfo = phoneInfoArray[1];
			ibeSuportLans = I18NUtil.getIBESupportedLanguages(request);
			loyaltyEnable = AppSysParamsUtil.isLoyalityEnabled();
			integrateMashreqWithLMSEnabled = AppSysParamsUtil.isIntegrateMashreqWithLMS();
			timeoutDTO = ReservationUtil.fetchSessionTimeoutData(request);

			List<UserRegConfigDTO> configList = IBEModuleUtils.getGlobalConfig().getUserRegConfig();
			PaxUtil.populateUserRegConfig(configList, userRegConfig, validationGroup);
			FlyingSocialUtil.composeSocialLoginParams(socialLoginParams, true);

			if (AppSysParamsUtil.isLMSEnabled()) {
				loyaltyManagmentEnabled = true;
			} else {
				loyaltyManagmentEnabled = false;
			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowCustomerProfileDetailAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowCustomerProfileDetailAction==>", ex);
		}
		return forward;
	}

	public SessionTimeoutDataDTO getTimeoutDTO() {
		return timeoutDTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public List getCountryInfo() {
		return countryInfo;
	}

	public void setCountryInfo(List countryInfo) {
		this.countryInfo = countryInfo;
	}

	public String getTitleInfo() {
		return titleInfo;
	}

	public List getQuestionInfo() {
		return questionInfo;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public List getNationalityInfo() {
		return nationalityInfo;
	}

	public List getDocTypeInfo() {
		return docTypeInfo;
	}

	public void setDocTypeInfo(List docTypeInfo) {
		this.docTypeInfo = docTypeInfo;
	}

	public List<String[]> getIbeSuportLans() {
		return ibeSuportLans;
	}

	public void setIbeSuportLans(List<String[]> ibeSuportLans) {
		this.ibeSuportLans = ibeSuportLans;
	}

	public boolean isLoyaltyEnable() {
		return loyaltyEnable;
	}

	public String getCountryPhoneInfo() {
		return countryPhoneInfo;
	}

	public String getAreaPhoneInfo() {
		return areaPhoneInfo;
	}

	/**
	 * @return the userRegConfig
	 */
	public Map<String, UserRegConfigDTO> getUserRegConfig() {
		return userRegConfig;
	}

	/**
	 * @param userRegConfig
	 *            the userRegConfig to set
	 */
	public void setUserRegConfig(Map<String, UserRegConfigDTO> userRegConfig) {
		this.userRegConfig = userRegConfig;
	}

	/**
	 * @return the validationGroup
	 */
	public Map<String, List<String>> getValidationGroup() {
		return validationGroup;
	}

	/**
	 * @param validationGroup
	 *            the validationGroup to set
	 */
	public void setValidationGroup(Map<String, List<String>> validationGroup) {
		this.validationGroup = validationGroup;
	}

	public SocialTO getSocialLoginParams() {
		return socialLoginParams;
	}

	public void setSocialLoginParams(SocialTO socialLoginParams) {
		this.socialLoginParams = socialLoginParams;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public boolean isIntegrateMashreqWithLMSEnabled() {
		return integrateMashreqWithLMSEnabled;
	}

	public void setIntegrateMashreqWithLMSEnabled(boolean integrateMashreqWithLMSEnabled) {
		this.integrateMashreqWithLMSEnabled = integrateMashreqWithLMSEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

}
