package com.isa.thinair.ibe.core.web.v2.action.kiosk;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;

/**
 * 
 * @author pkarunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Jsp.Kiosk.LOGIN, value = StrutsConstants.Jsp.Kiosk.LOGIN),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class ShowKioskAction extends IBEBaseAction {
	public String execute() {
		return StrutsConstants.Jsp.Kiosk.LOGIN;
	}
}
