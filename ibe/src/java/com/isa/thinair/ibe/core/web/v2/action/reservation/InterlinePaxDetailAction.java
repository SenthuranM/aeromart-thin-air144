package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.PassengerDTO;
import com.isa.thinair.ibe.api.dto.PaxDetailDTO;
import com.isa.thinair.ibe.api.dto.PaxValidationTO;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.api.dto.SocialTO;
import com.isa.thinair.ibe.api.dto.URLTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.generator.createres.ShowAddPaxHG;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.FlyingSocialUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thushara Fernando
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlinePaxDetailAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(InterlinePaxDetailAction.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private List countryInfo;

	private List nationalityInfo;
	
	private List docTypeInfo;

	private String titleInfo;

	private String customerInfo;

	private String titleVisibilityInfo;
	
	private String adtTitleVisibilityInfo;
	
	private String chdTitleVisibilityInfo;	

	private String onHoldInfo;

	private String foidValidationInfo;

	private String countryPhoneInfo;

	private String areaPhoneInfo;

	private String fareTypeInfo;

	private String languageInfo;

	private String errorMessageInfo;

	private Integer customerId;

	private Date depatureDateTime;

	// available as only getter, never expected to be null
	private PaxDetailDTO paxDetail = new PaxDetailDTO();

	private FareQuoteTO fareQuote;

	private Collection<SegInfoDTO> flightSegments;

	private boolean blnReturn = false;

	private HashMap<String, String> jsonLabel;

	private String direction = "ltr";

	private String currentDate;

	private PaxValidationTO paxValidation;

	private String language;

	private URLTO urlInfo;

	private ContactInfoDTO contactInfo = new ContactInfoDTO();

	private boolean success = true;

	private String messageTxt;

	private SessionTimeoutDataDTO timeoutDTO;

	private String accessPoint;

	private String regUserAccessInKiosk;

	private boolean validateEmailDomain;

	private boolean showRegUserLoginInPaxDetailPage;

	private boolean showSocialLoginInPaxDetailPage;
	
	private LmsMemberDTO lmsDetails;

	private SocialTO socialParams = new SocialTO();

	private String airlineCarrierCode;

	private Set <String> busCarrierCodes;

	public String execute() {
		
		String result = StrutsConstants.Result.SUCCESS;
		try {
			airlineCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "Common", "PgPassenger" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			this.timeoutDTO = ReservationUtil.fetchSessionTimeoutData(request);
			countryInfo = SelectListGenerator.getCountryList();
			docTypeInfo = SelectListGenerator.getDocTypeList();
			nationalityInfo = SelectListGenerator.getNationalityList();
			this.titleInfo = JavaScriptGenerator.getTitleHtml();
			this.titleVisibilityInfo = JavaScriptGenerator.getTiteVisibilityHtml();
			this.adtTitleVisibilityInfo = JavaScriptGenerator.getTitleForAdultVisibilityHtml();
			this.chdTitleVisibilityInfo = JavaScriptGenerator.getTitleForChildVisibilityHtml();
			String[] phoneInfoArray = SelectListGenerator.createTelephoneString();
			countryPhoneInfo = phoneInfoArray[0];
			areaPhoneInfo = phoneInfoArray[1];
			this.language = strLanguage;
			this.languageInfo = ShowAddPaxHG.createItineryLanguagesOptionSelected(strLanguage);
			this.fareTypeInfo = SelectListGenerator.createActiveFareCategories(ReservationWebConstnts.REQ_FARES_ARRAY_NAME);
			this.errorMessageInfo = ErrorMessageUtil.getClientErrors(strLanguage);

			int adultCount = getSearchParams().getAdultCount();
			int childCount = getSearchParams().getChildCount();
			int infCount = getSearchParams().getInfantCount();

			for (int i = 0; i < adultCount; i++) {
				PassengerDTO adult = new PassengerDTO();
				adult.setPaxSequence(i + 1);
				adult.setImageId(i);
				paxDetail.addAdult(adult);
			}

			for (int i = 0; i < childCount; i++) {
				PassengerDTO child = new PassengerDTO();
				child.setPaxSequence(i + 1);
				child.setImageId(i);
				paxDetail.addChild(child);
			}

			for (int i = 0; i < infCount; i++) {
				PassengerDTO infant = new PassengerDTO();
				infant.setPaxSequence(i + 1);
				infant.setImageId(i);
				paxDetail.addInfant(infant);
			}

			// Get Customer ID from sessionUtil
			customerId = SessionUtil.getCustomerId(request);
			URLTO paxUrlTo = BookingUtil.getURLData(request);
			accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);
			regUserAccessInKiosk = globalConfig.getBizParam(SystemParamKeys.ENABLE_REGISTERED_USER_ACCESS_IN_KIOSK);
			showRegUserLoginInPaxDetailPage = AppSysParamsUtil.isShowRegUserLoginInPaxDetailPage();
			showSocialLoginInPaxDetailPage = showRegUserLoginInPaxDetailPage && AppSysParamsUtil.isShowIbeSocialLoginInPaxPage();
			validateEmailDomain = AppSysParamsUtil.isEmailDomainValidationEnabled();
			if (customerId != null) {
				Customer customer = CustomerUtil.getCustomer(customerId);
				if (customer != null) {
					paxUrlTo.setCustomer(true);
					createProfileInfo(customer);
					if(!customer.getCustomerLmsDetails().isEmpty()){
						lmsDetails = CustomerUtilV2.getLmsMemberDTO(customer.getCustomerLmsDetails().iterator().next());
					}
				}
			}

			FlyingSocialUtil.composeSocialLoginParams(socialParams, false);
			this.paxValidation = PaxUtil.fillPaxValidation();

			SimpleDateFormat smf = new SimpleDateFormat("dd/MM/yyyy");
			this.currentDate = smf.format(new Date());
			this.urlInfo = paxUrlTo;
			busCarrierCodes = new HashSet<String>();
			busCarrierCodes.addAll(SelectListGenerator.getBusCarrierCodes());
			// SessionUtil.setIBEURLInfo(request, this.urlInfo);
		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("InterlinePaxDetailAction==>" + me, me);
		} catch (RuntimeException re) {
			success = false;
			messageTxt = getServerErrorMessage(request, "server.default.operation.fail");
			log.error("InterlinePaxDetailAction==>" + re, re);
		}

		return result;
	}

	// getters
	public PaxDetailDTO getPaxDetail() {
		return paxDetail;
	}

	public List getCountryInfo() {
		return countryInfo;
	}

	public List getNationalityInfo() {
		return nationalityInfo;
	}

	public String getTitleInfo() {
		return titleInfo;
	}

	public List getDocTypeInfo() {
		return docTypeInfo;
	}

	public void setDocTypeInfo(List docTypeInfo) {
		this.docTypeInfo = docTypeInfo;
	}

	public String getCustomerInfo() {
		return customerInfo;
	}

	public String getTitleVisibilityInfo() {
		return titleVisibilityInfo;
	}

	public String getOnHoldInfo() {
		return onHoldInfo;
	}

	public String getFoidValidationInfo() {
		return foidValidationInfo;
	}

	public String getCountryPhoneInfo() {
		return countryPhoneInfo;
	}

	public String getFareTypeInfo() {
		return fareTypeInfo;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public Date getDepatureDateTime() {
		return depatureDateTime;
	}

	public String getAdtTitleVisibilityInfo() {
		return adtTitleVisibilityInfo;
	}

	public String getChdTitleVisibilityInfo() {
		return chdTitleVisibilityInfo;
	}

	public void setAdtTitleVisibilityInfo(String adtTitleVisibilityInfo) {
		this.adtTitleVisibilityInfo = adtTitleVisibilityInfo;
	}

	public void setChdTitleVisibilityInfo(String chdTitleVisibilityInfo) {
		this.chdTitleVisibilityInfo = chdTitleVisibilityInfo;
	}

	public String getLanguageInfo() {
		return languageInfo;
	}

	public String getErrorMessageInfo() {
		return errorMessageInfo;
	}

	// setters
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public void setDepatureDateTime(Date depatureDateTime) {
		this.depatureDateTime = depatureDateTime;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public Collection<SegInfoDTO> getFlightSegments() {
		return flightSegments;
	}

	public boolean isBlnReturn() {
		return blnReturn;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public String getDirection() {
		return direction;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public PaxValidationTO getPaxValidation() {
		return paxValidation;
	}

	public String getLanguage() {
		return language;
	}

	public URLTO getUrlInfo() {
		return urlInfo;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public String getAccessPoint() {
		return accessPoint;
	}

	// Create ContactInfoDTO object
	private void createProfileInfo(Customer customer) throws ModuleException {

		this.contactInfo.setTitle(StringUtil.nullConvertToString(customer.getTitle()));
		this.contactInfo.setFirstName(StringUtil.nullConvertToString(customer.getFirstName()));
		this.contactInfo.setLastName(StringUtil.nullConvertToString(customer.getLastName()));
		
		if (customer.getDateOfBirth() != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String birthDay = dateFormat.format(customer.getDateOfBirth());
			this.contactInfo.setDateOfBirth(birthDay);
		}
		String[] phoneDataTel = retrievePhoneData(StringUtil.nullConvertToString(customer.getTelephone()));
		this.contactInfo.setlCountry(StringUtil.nullConvertToString(phoneDataTel[0]));
		this.contactInfo.setlArea(StringUtil.nullConvertToString(phoneDataTel[1]));
		this.contactInfo.setlNumber(StringUtil.nullConvertToString(phoneDataTel[2]));

		String[] phoneDataMob = retrievePhoneData(StringUtil.nullConvertToString(customer.getMobile()));
		this.contactInfo.setmCountry(StringUtil.nullConvertToString(phoneDataMob[0]));
		this.contactInfo.setmArea(StringUtil.nullConvertToString(phoneDataMob[1]));
		this.contactInfo.setmNumber(StringUtil.nullConvertToString(phoneDataMob[2]));

		String[] phoneDataFax = retrievePhoneData(StringUtil.nullConvertToString(customer.getFax()));
		this.contactInfo.setfCountry(StringUtil.nullConvertToString(phoneDataFax[0]));
		this.contactInfo.setfArea(StringUtil.nullConvertToString(phoneDataFax[1]));
		this.contactInfo.setfNumber(StringUtil.nullConvertToString(phoneDataFax[2]));

		this.contactInfo.setEmailAddress(StringUtil.nullConvertToString(customer.getEmailId()));

		if (customer.getNationalityCode() != null) {
			this.contactInfo.setNationality(StringUtil.nullConvertToString(new Integer(customer.getNationalityCode())));
		} else {
			this.contactInfo.setNationality("");
		}

		this.contactInfo.setCity(StringUtil.nullConvertToString(customer.getCity()));
		this.contactInfo.setZipCode(StringUtil.nullConvertToString(customer.getZipCode()));
		this.contactInfo.setCountry(StringUtil.nullConvertToString(customer.getCountryCode()));
		this.contactInfo.setAddresStreet(StringUtil.nullConvertToString(customer.getAddressStreet()));
		this.contactInfo.setAddresline(StringUtil.nullConvertToString(customer.getAddressLine()));

		// Set Emergency contact details
		this.contactInfo.setEmgnTitle(StringUtil.nullConvertToString(customer.getEmgnTitle()));
		this.contactInfo.setEmgnFirstName(StringUtil.nullConvertToString(customer.getEmgnFirstName()));
		this.contactInfo.setEmgnLastName(StringUtil.nullConvertToString(customer.getEmgnLastName()));
		this.contactInfo.setEmgnEmail(StringUtil.nullConvertToString(customer.getEmgnEmail()));

		String[] emgnPhoneDataTel = retrievePhoneData(StringUtil.nullConvertToString(customer.getEmgnPhoneNo()));
		this.contactInfo.setEmgnLCountry(StringUtil.nullConvertToString(emgnPhoneDataTel[0]));
		this.contactInfo.setEmgnLArea(StringUtil.nullConvertToString(emgnPhoneDataTel[1]));
		this.contactInfo.setEmgnLNumber(StringUtil.nullConvertToString(emgnPhoneDataTel[2]));

	}

	private String[] retrievePhoneData(String phoneNo) {
		String[] telephoneData = null;
		if (phoneNo.length() > 0) {
			telephoneData = phoneNo.split("-");
			if (telephoneData.length != 3) {
				telephoneData = new String[3];
			}
		} else {
			telephoneData = new String[3];
		}
		return telephoneData;
	}

	private static String getServerErrorMessage(HttpServletRequest request, String key) {
		return I18NUtil.getMessage(key, SessionUtil.getLanguage(request));
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public SessionTimeoutDataDTO getTimeoutDTO() {
		return timeoutDTO;
	}

	public void setTimeoutDTO(SessionTimeoutDataDTO timeoutDTO) {
		this.timeoutDTO = timeoutDTO;
	}

	public String getRegUserAccessInKiosk() {
		return regUserAccessInKiosk;
	}

	public boolean isValidateEmailDomain() {
		return validateEmailDomain;
	}

	public void setValidateEmailDomain(boolean validateEmailDomain) {
		this.validateEmailDomain = validateEmailDomain;
	}

	public String getAreaPhoneInfo() {
		return areaPhoneInfo;
	}

	public boolean isShowRegUserLoginInPaxDetailPage() {
		return showRegUserLoginInPaxDetailPage;
	}

	public boolean isShowSocialLoginInPaxDetailPage() {
		return showSocialLoginInPaxDetailPage;
	}

	public String getAirlineCarrierCode() {
		return airlineCarrierCode;
	}

	public void setAirlineCarrierCode(String airlineCarrierCode) {
		this.airlineCarrierCode = airlineCarrierCode;
	}

	public SocialTO getSocialParams() {
		return socialParams;
	}

	public void setSocialParams(SocialTO socialParams) {
		this.socialParams = socialParams;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public Set<String> getBusCarrierCodes() {
		return busCarrierCodes;
	}

	public void setBusCarrierCodes(Set<String> busCarrierCodes) {
		this.busCarrierCodes = busCarrierCodes;
	}

}
