package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.RedeemCalculateRQ;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.ibe.api.dto.AncillaryPaxTo;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.BalanceTo;
import com.isa.thinair.ibe.api.dto.BookingTO;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.FareDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.PassengerDTO;
import com.isa.thinair.ibe.api.dto.PaxTO;
import com.isa.thinair.ibe.api.dto.PutOnHoldBeforePaymentDTO;
import com.isa.thinair.ibe.api.dto.ReservationProcessDTO;
import com.isa.thinair.ibe.api.dto.ReservationSegmentStatusDTO;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.DateUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelSegmentDTO;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaTypesDesc;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.webplatform.api.dto.FlexiDTO;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.webplatform.core.commons.MapGenerator;

public class ReservationUtil {
	private static Log log = LogFactory.getLog(ReservationUtil.class);

	/**
	 * 
	 * @param lccClientReservation
	 * @return
	 */
	public static BookingTO createBookingDetails(LCCClientReservation lccClientReservation) {
		BookingTO bookingTO = new BookingTO();

		bookingTO.setBookingDate(BeanUtils.parseDateFormat(lccClientReservation.getZuluBookingTimestamp(), "dd-MM-yyyy"));

		bookingTO.setPNR(lccClientReservation.getPNR());
		bookingTO.setVersion(lccClientReservation.getVersion());
		bookingTO.setTotalPaxAdultCount(Integer.toString(lccClientReservation.getTotalPaxAdultCount()));
		bookingTO.setTotalPaxChildCount(Integer.toString(lccClientReservation.getTotalPaxChildCount()));
		bookingTO.setTotalPaxInfantCount(Integer.toString(lccClientReservation.getTotalPaxInfantCount()));

		List<LCCClientReservationInsurance> reservationInsurances = lccClientReservation.getReservationInsurances();

		if (reservationInsurances != null && !reservationInsurances.isEmpty()) {
			for (LCCClientReservationInsurance reservationInsurance : reservationInsurances) {

				bookingTO.setPolicyNo(reservationInsurance.getPolicyCode());
				if (ReservationInternalConstants.INSURANCE_STATES.SC.toString().equals(reservationInsurance.getState())
						|| ReservationInternalConstants.INSURANCE_STATES.FL.toString().equals(reservationInsurance.getState())
						|| ReservationInternalConstants.INSURANCE_STATES.RQ.toString().equals(reservationInsurance.getState())
						|| ReservationInternalConstants.INSURANCE_STATES.TO.toString().equals(reservationInsurance.getState())) {
					bookingTO.setHasInsurance(true);
					break;
				}
			}
		} else {
			bookingTO.setPolicyNo("");
		}

		String releaseDate = "";

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())) {
			bookingTO.setStatus("CONFIRMED");
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(lccClientReservation.getStatus())) {
			bookingTO.setStatus("CANCELLED");
		} else {
			bookingTO.setStatus("ONHOLD");
			releaseDate = BeanUtils.nullHandler(
					BeanUtils.parseDateFormat(lccClientReservation.getZuluReleaseTimeStamp(), "dd-MM-yyyy HH:mm (EEEE)")
							+ " GMT");
		}

		if (lccClientReservation.getTotalAvailableBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			bookingTO.setStatus("FORCED-CONFIRM");
		}

		bookingTO.setReleaseDate(releaseDate);
		bookingTO.setPrefferedLanguage(lccClientReservation.getPreferrenceInfo() == null
				? "en"
				: lccClientReservation.getPreferrenceInfo().getPreferredLanguage());
		bookingTO.setPnrOndGroups(lccClientReservation.getCarrierOndGrouping());

		return bookingTO;
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, boolean recordAudit, String airlineCode,
			String marketingAirlineCode, boolean skipPromoAdjustment) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		boolean loadFares = true; // Set True
		boolean loadLastUserNote = false;
		boolean loadOndChargesView = true;
		boolean loadPaxAvaBalance = true;
		boolean loadSegView = true;

		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegView(loadSegView);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(ApplicationEngine.IBE);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setSkipPromoAdjustment(skipPromoAdjustment);
		pnrModesDTO.setLoadAirportTransferInfo(true);

		if (StringUtils.isNotEmpty(marketingAirlineCode)) {
			if (marketingAirlineCode.trim().length() == 2) {
				pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
			}
		}

		if (StringUtils.isNotEmpty(airlineCode)) {
			if (airlineCode.trim().length() == 2) {
				pnrModesDTO.setAirlineCode(airlineCode);
			}
		}

		return pnrModesDTO;
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, String preferredLanguage,
			HttpServletRequest request, boolean recordAudit, boolean skipPromoAdjustment) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
		} else {
			pnrModesDTO.setPnr(pnr);
		}

		boolean loadFares = true;
		boolean loadLastUserNote = true;
		// boolean loadLocalTimes = true;
		boolean loadOndChargesView = true;
		boolean loadPaxAvaBalance = true;
		boolean loadSegView = true;

		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		// pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegView(loadSegView);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(ApplicationEngine.IBE);
		pnrModesDTO.setPreferredLanguage(preferredLanguage);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setSkipPromoAdjustment(skipPromoAdjustment);
		pnrModesDTO.setLoadAirportTransferInfo(true);

		return pnrModesDTO;

	}

	/**
	 * Load Minimum Reservation Data
	 * 
	 * @param pnr
	 * @param isGroupPNR
	 * @param trackInfo
	 * @param recordAudit
	 * @param airlineCode
	 * @param marketingAirlineCode
	 * @param skipPromoAdjustment
	 * @return
	 * @throws ModuleException
	 */
	// Fix Me : Need to load minimum reservation data
	public static LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR, TrackInfoDTO trackInfo,
			boolean recordAudit, boolean isRegisteredUser, String airlineCode, String marketingAirlineCode,
			boolean skipPromoAdjustment) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, isGroupPNR, recordAudit, airlineCode, marketingAirlineCode,
				skipPromoAdjustment);

		ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
		modificationParamRQInfo.setAppIndicator(AppIndicatorEnum.APP_IBE);
		modificationParamRQInfo.setIsRegisteredUser(isRegisteredUser);

		if (log.isDebugEnabled())
			log.debug(" Search reservation initiated for loadProxyReservation. " + pnrModesDTO.toString());
		return ModuleServiceLocator.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, modificationParamRQInfo,
				trackInfo);
	}

	/**
	 * 
	 * @param reservation
	 * @param includeExchangedSegment
	 * @param lccPromotionInfoTO
	 * @return
	 */
	public static Object[] loadFlightSegments(LCCClientReservation reservation, boolean includeExchangedSegment,
			LCCPromotionInfoTO lccPromotionInfoTO) throws ModuleException {
		Set<LCCClientReservationSegment> setReservationSegments = reservation.getSegments();
		Object[] flightList = new Object[2];
		Collection<FlightInfoTO> collectionDepature = new ArrayList<FlightInfoTO>();
		Collection<FlightInfoTO> collectionReturn = new ArrayList<FlightInfoTO>();
		List<FlightInfoTO> collectionGroundSegments = new ArrayList<FlightInfoTO>();
		Collection<FlightInfoTO> collectionDepatureGround = new ArrayList<FlightInfoTO>();
		Collection<FlightInfoTO> collectionReturnGround = new ArrayList<FlightInfoTO>();
		Collection<FlightInfoTO> collectionRemainingGround = new ArrayList<FlightInfoTO>();

		// Segment map
		Map<Integer, LCCClientReservationSegment> segMap = new HashMap<Integer, LCCClientReservationSegment>();

		for (LCCClientReservationSegment segment : setReservationSegments) {
			segMap.put(segment.getPnrSegID(), segment);
		}

		SimpleDateFormat smpdtDate = new SimpleDateFormat("EEE, dd MMM yy");
		SimpleDateFormat smpdtDateValue = new SimpleDateFormat("yyMMddHHmm");
		SimpleDateFormat stringDate = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
		long sysTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		long segModifyBuffStart = 0l;
		long segCancelBuffStart = 0l;
		String orginChannelId = "";

		if (reservation.getAdminInfo() != null && reservation.getAdminInfo().getOriginChannelId() != null) {
			orginChannelId = CommonUtil.getCarrierStringRemoved(reservation.getAdminInfo().getOriginChannelId());
		}

		if (setReservationSegments != null && setReservationSegments.size() > 0) {
			LCCClientReservationSegment[] resSegs = com.isa.thinair.airproxy.api.utils.SortUtil.sort(setReservationSegments);
			int finalJourneyOnd = resSegs[resSegs.length - 1].getJourneySequence();
			for (LCCClientReservationSegment lccClientReservationSegment : com.isa.thinair.airproxy.api.utils.SortUtil
					.sort(setReservationSegments)) {

				String segmentSubStatus = lccClientReservationSegment.getSubStatus();

				if (!includeExchangedSegment && ReservationSegmentSubStatus.EXCHANGED.equals(segmentSubStatus)) {
					continue;
				}

				segModifyBuffStart = 0l;
				segCancelBuffStart = 0l;
				FlightInfoTO flightInfoTO = new FlightInfoTO();
				flightInfoTO.setOrignNDest(ReservationHG.getSegementDetails(lccClientReservationSegment.getSegmentCode()));

				// FIXME:
				// Setting modify segment option false for all ground segments as it is not allowed as per current
				// implementation
				// This check should be ported to backend. This not a good design

				if (ModuleServiceLocator.getAirportBD().isBusSegment(lccClientReservationSegment.getSegmentCode())) {
					flightInfoTO.setGroundSegment(true);
				}

				flightInfoTO.setFlightNo(lccClientReservationSegment.getFlightNo());
				flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(lccClientReservationSegment.getFlightNo()));
				flightInfoTO.setDepartureDate(smpdtDate.format(lccClientReservationSegment.getDepartureDate()));
				flightInfoTO.setDepartureDateValue(smpdtDateValue.format(lccClientReservationSegment.getDepartureDate()));
				flightInfoTO.setDepartureDateLong(lccClientReservationSegment.getDepartureDate().getTime());
				flightInfoTO.setDepartureTimeZuluLong(lccClientReservationSegment.getDepartureDateZulu().getTime());
				flightInfoTO.setDepartureText("DEP");
				flightInfoTO.setDeparture("");
				flightInfoTO.setDepartureTime(smpdtTime.format(lccClientReservationSegment.getDepartureDate()));
				flightInfoTO.setArrivalText("ARR");
				flightInfoTO.setArrivalDate(smpdtDate.format(lccClientReservationSegment.getArrivalDate()));
				flightInfoTO.setArrivalDateValue(smpdtDateValue.format(lccClientReservationSegment.getArrivalDate()));
				flightInfoTO.setArrivalDateLong(lccClientReservationSegment.getArrivalDate().getTime());
				flightInfoTO.setArrivalTimeZuluLong(lccClientReservationSegment.getArrivalDateZulu().getTime());
				flightInfoTO.setDisplayStatus(lccClientReservationSegment.getDisplayStatus());
				String segmentCode = BeanUtils.nullHandler(lccClientReservationSegment.getSegmentCode());
				if (segmentCode != null && segmentCode.split("/").length >= 2) {
					String[] locCodes = segmentCode.split("/");
					flightInfoTO.setDeparture(locCodes[0]);
					flightInfoTO.setArrival(locCodes[locCodes.length - 1]);
				} else {
					flightInfoTO.setDeparture("");
					flightInfoTO.setArrival("");
				}
				flightInfoTO.setArrivalTime(smpdtTime.format(lccClientReservationSegment.getArrivalDate()));
				flightInfoTO.setArrivalDateLong(lccClientReservationSegment.getArrivalDate().getTime());
				flightInfoTO.setCabinClass(lccClientReservationSegment.getCabinClassCode());
				flightInfoTO.setLogicalCC(lccClientReservationSegment.getLogicalCabinClass());
				flightInfoTO.setType("");
				flightInfoTO.setStatus(lccClientReservationSegment.getStatus());
				flightInfoTO.setSubStatus(lccClientReservationSegment.getSubStatus());
				flightInfoTO.setFlightSegmentRefNumber(lccClientReservationSegment.getBookingFlightSegmentRefNumber());
				flightInfoTO.setInterLineGroupKey(lccClientReservationSegment.getInterlineGroupKey());
				// Temp Fix Add group key
				flightInfoTO.setFlightRefNumber(lccClientReservationSegment.getFlightSegmentRefNumber() + "-"
						+ lccClientReservationSegment.getInterlineGroupKey());
				flightInfoTO.setSystem(lccClientReservationSegment.getSystem().toString());
				flightInfoTO.setSegmentShortCode(lccClientReservationSegment.getSegmentCode());
				flightInfoTO.setDuration(CommonUtil.getTimeHHMM(lccClientReservationSegment.getArrivalDateZulu().getTime()
						- lccClientReservationSegment.getDepartureDateZulu().getTime()));
				flightInfoTO.setDepartureDisplayDate(stringDate.format(lccClientReservationSegment.getDepartureDate()));
				flightInfoTO.setPnrSegID(BeanUtils.nullHandler(lccClientReservationSegment.getPnrSegID()));
				flightInfoTO.setGroundSegmentPnrSegId(lccClientReservationSegment.getGroundStationPnrSegmentID() != null
						? BeanUtils.nullHandler(lccClientReservationSegment.getGroundStationPnrSegmentID())
						: null);

				flightInfoTO.setFlownSegment(lccClientReservationSegment.isFlownSegment());
				flightInfoTO.setBaggageOndGroupId(lccClientReservationSegment.getBaggageONDGroupId());

				// Setting modifiable according to privilege
				segModifyBuffStart = lccClientReservationSegment.getModifyTillBufferDateTime().getTime();
				segCancelBuffStart = lccClientReservationSegment.getCancelTillBufferDateTime().getTime();
				if (!lccClientReservationSegment.getStatus()
						.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {

					if ((ReservationInternalConstants.SalesChannel.WEB == Integer.parseInt(orginChannelId)
							|| ReservationInternalConstants.SalesChannel.IOS == Integer.parseInt(orginChannelId) || ReservationInternalConstants.SalesChannel.ANDROID == Integer
							.parseInt(orginChannelId))
							&& lccClientReservationSegment.isAllPaxNoShow()
							&& AppSysParamsUtil.isRequoteEnabled() && AppSysParamsUtil.isAllowModifyNoShowSegmentsIBE()) {
						if (AppSysParamsUtil.isAllowModifySegmentDateIBE()) {
							flightInfoTO.setIsAllowModifyByDate(reservation.isModifiableReservation()
									&& lccClientReservationSegment.isModifyByDate());
						} else {
							flightInfoTO.setIsAllowModifyByDate(false);
						}
						if (AppSysParamsUtil.isAllowModifySegmentRouteIBE()) {
							flightInfoTO.setIsAllowModifyByRoute(reservation.isModifiableReservation()
									&& lccClientReservationSegment.isModifyByRoute());
						} else {
							flightInfoTO.setIsAllowModifyByRoute(false);
						}

						if (reservation.isModifiableReservation() && lccClientReservationSegment.isModifible())
							flightInfoTO.setCancellable(true);

					} else {
						if (sysTime < segModifyBuffStart) {
							/* segment modification IBE */
							if (AppSysParamsUtil.isAllowModifySegmentDateIBE()) {
								flightInfoTO.setIsAllowModifyByDate(
										reservation.isModifiableReservation() && lccClientReservationSegment.isModifyByDate());
							} else {
								flightInfoTO.setIsAllowModifyByDate(false);
							}
							if (AppSysParamsUtil.isAllowModifySegmentRouteIBE()) {
								flightInfoTO.setIsAllowModifyByRoute(
										reservation.isModifiableReservation() && lccClientReservationSegment.isModifyByRoute());
							} else {
								flightInfoTO.setIsAllowModifyByRoute(false);
							}
						}

						if (sysTime < segCancelBuffStart) {
							if (reservation.isModifiableReservation() && lccClientReservationSegment.isModifible())
								flightInfoTO.setCancellable(true);
						}
					}

					if (finalJourneyOnd == lccClientReservationSegment.getJourneySequence()
							&& lccClientReservationSegment.getTicketValidTill() != null) {
						flightInfoTO.setTicketValidTill(
								BeanUtils.parseDateFormat(lccClientReservationSegment.getTicketValidTill(), "dd MMM yyyy"));
					}

				}

				if (lccClientReservationSegment.isFlownSegment()) {
					flightInfoTO.setFlownSegment(true);
					flightInfoTO.setIsAllowModifyByDate(false);
					flightInfoTO.setIsAllowModifyByRoute(false);
					flightInfoTO.setCancellable(false);
					flightInfoTO.setModifiable(false);
				}
				
				if(!isSegmentModifiableAsPerETicketStatus(reservation,lccClientReservationSegment.getPnrSegID())){
					flightInfoTO.setIsAllowModifyByDate(false);
					flightInfoTO.setIsAllowModifyByRoute(false);
					flightInfoTO.setCancellable(false);
					flightInfoTO.setModifiable(false);
				}
				
				if (lccClientReservationSegment.isCancellable()) {
					lccClientReservationSegment.setCancellable(isSegmentModifiableAsPerETicketStatus(reservation,
							lccClientReservationSegment.getPnrSegID()));
				}
				
				if (AppSysParamsUtil.isGroundServiceEnabled()) {

					if (lccClientReservationSegment.getSubStationShortName() == null) {
						if (lccClientReservationSegment.getGroundStationPnrSegmentID() != null) {
							LCCClientReservationSegment actualGroundSeg = segMap
									.get(lccClientReservationSegment.getGroundStationPnrSegmentID());
							if (actualGroundSeg != null && ReservationSegmentStatus.CANCEL.equals(actualGroundSeg.getStatus())) {
								flightInfoTO.setGroundStationAddAllowed(Boolean.TRUE);

							}
						} else {
							flightInfoTO.setGroundStationAddAllowed(Boolean.TRUE);
						}
					}
				}

				if (!flightInfoTO.isGroundSegment()) {
					if (lccClientReservationSegment.getReturnFlag() != null
							&& lccClientReservationSegment.getReturnFlag().equalsIgnoreCase("N")) {
						flightInfoTO.setReturnFlag(false);
						collectionDepature.add(flightInfoTO);
					} else if (lccClientReservationSegment.getReturnFlag() != null
							&& lccClientReservationSegment.getReturnFlag().equalsIgnoreCase("Y")) {
						// flightInfoTO.setReturnFlag(true);
						// collectionReturn.add(flightInfoTO);
						flightInfoTO.setReturnFlag(false);
						collectionDepature.add(flightInfoTO);
					} else if (lccClientReservationSegment.getReturnFlag() == null) {
						collectionDepature.add(flightInfoTO);
					}
				} else {
					collectionGroundSegments.add(flightInfoTO);
				}

				// override modify/cancel/split operation
				if (lccPromotionInfoTO != null) {
					if (!lccPromotionInfoTO.isModificationAllowed()) {
						flightInfoTO.setModifiable(false);
						flightInfoTO.setIsModifiable(false);
						flightInfoTO.setIsAllowModifyByDate(false);
						flightInfoTO.setIsAllowModifyByRoute(false);
					}

					if (!lccPromotionInfoTO.isCancellationAllowed()) {
						flightInfoTO.setCancellable(false);
						flightInfoTO.setIsCancellable(false);
					}
				}

			}

			if (collectionGroundSegments.size() > 0) {
				Collections.sort(collectionGroundSegments);

				for (FlightInfoTO flightInfoTO : collectionDepature) {

					for (FlightInfoTO flightInfoGroundTO : collectionGroundSegments) {
						if (isCorrectSegment(flightInfoTO, flightInfoGroundTO)
								&& isCorrectBusSegment(flightInfoTO, flightInfoGroundTO)) {
							if (!hasSegmentInCollection(collectionDepatureGround, flightInfoGroundTO)) {
								collectionDepatureGround.add(flightInfoGroundTO);
							}

						} else {
							if (!hasSegmentInCollection(collectionRemainingGround, flightInfoGroundTO)) {
								collectionRemainingGround.add(flightInfoGroundTO);
							}
						}
					}
				}

				for (FlightInfoTO flightInfoTO : collectionReturn) {

					for (FlightInfoTO flightInfoGroundTO : collectionRemainingGround) {
						if (isCorrectSegment(flightInfoTO, flightInfoGroundTO)
								&& isCorrectBusSegment(flightInfoTO, flightInfoGroundTO)) {
							collectionReturnGround.add(flightInfoGroundTO);
						}
					}
				}
			}

			if (collectionDepatureGround.size() > 0) {
				collectionDepature.addAll(collectionDepatureGround);
				Collections.sort((List<FlightInfoTO>) collectionDepature);
			}

			if (collectionReturnGround.size() > 0) {
				collectionReturn.addAll(collectionReturnGround);
				Collections.sort((List<FlightInfoTO>) collectionReturn);
			}

			flightList[0] = collectionDepature;
			flightList[1] = collectionReturn;

		}

		return flightList;
	}

	private static boolean hasSegmentInCollection(Collection<FlightInfoTO> collectionSegments, FlightInfoTO currentFlightInfoTO) {

		if (collectionSegments != null) {
			for (FlightInfoTO flightInfoTO : collectionSegments) {
				if (flightInfoTO.getFlightRefNumber().equals(currentFlightInfoTO.getFlightRefNumber())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * This method will check if the given FlightInfoTO's are correct by comparing segment codes. As in SHJ/CMB, CMB/AAB
	 * and vice versa CMB/SHJ, AAB/CMB is correct and return true. Anything else will return false.
	 * 
	 * @param airFlightInfo
	 *            FlightInfoTO containing flight details
	 * @param groundFlightInfo
	 *            FlightInfoTO containing ground transport(busses etc.) details
	 */
	private static boolean isCorrectSegment(FlightInfoTO airFlightInfo, FlightInfoTO groundFlightInfo) {
		String[] groundSegmentShortCodes = groundFlightInfo.getSegmentShortCode().split("/");
		String[] airSegmentShortCodes = airFlightInfo.getSegmentShortCode().split("/");

		return airSegmentShortCodes[0].equals(groundSegmentShortCodes[groundSegmentShortCodes.length - 1])
				|| airSegmentShortCodes[airSegmentShortCodes.length - 1].equals(groundSegmentShortCodes[0]);
	}

	private static boolean isCorrectBusSegment(FlightInfoTO airFlightInfoTO, FlightInfoTO busFlightInfoTO) {

		String airSegmentBugPnrSegId = BeanUtils.nullHandler(airFlightInfoTO.getGroundSegmentPnrSegId());
		String busPnrSegId = BeanUtils.nullHandler(busFlightInfoTO.getPnrSegID());

		if (airSegmentBugPnrSegId.length() > 0 && busPnrSegId.length() > 0) {
			if (airSegmentBugPnrSegId.equals(busPnrSegId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get Fare Rule Details
	 * 
	 * @param setReservationSegments
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<FareDTO> getFareRuleDetails(Set<LCCClientReservationSegment> setReservationSegments)
			throws ModuleException {

		Collection<FareDTO> colFareDetails = new ArrayList<FareDTO>();

		if (setReservationSegments != null) {
			for (LCCClientReservationSegment lccClientReservationSegment : com.isa.thinair.airproxy.api.utils.SortUtil
					.sort(setReservationSegments)) {
				if (lccClientReservationSegment.getStatus()
						.equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					FareDTO fareDTO = new FareDTO();
					FareTO fareTO = lccClientReservationSegment.getFareTO();
					fareDTO.setAmount(fareTO.getAmount());
					fareDTO.setFareId(fareTO.getFareId());
					fareDTO.setFareRuleID(fareTO.getFareRuleID());
					fareDTO.setFlightRefNumber(lccClientReservationSegment.getBookingFlightSegmentRefNumber());
					fareDTO.setCarrierCode(fareTO.getCarrierCode());
					colFareDetails.add(fareDTO);
				}
			}
		}

		return colFareDetails;
	}

	public static ContactInfoDTO getContactDetails(CommonReservationContactInfo resContact) throws ModuleException {
		ContactInfoDTO contactInfo = new ContactInfoDTO();
		contactInfo.setAddresline(StringUtil.nullConvertToString(resContact.getStreetAddress2()));
		contactInfo.setAddresStreet(StringUtil.nullConvertToString(resContact.getStreetAddress1()));
		contactInfo.setCity(StringUtil.nullConvertToString(resContact.getCity()));
		contactInfo.setCountry(StringUtil.nullConvertToString(resContact.getCountryCode()));
		contactInfo
				.setCountryName(IBEModuleUtils.getLocationServiceBD().getCountry(resContact.getCountryCode()).getCountryName());
		contactInfo.setZipCode(StringUtil.nullConvertToString(resContact.getZipCode()));
		contactInfo.setEmailAddress(StringUtil.nullConvertToString(resContact.getEmail()));
		/*
		 * String[] faxno = resContact.getFax().split("-"); if(faxno.length == 3){ contactInfo.setfArea(faxno[1]);
		 * contactInfo.setfCountry(faxno[0]); contactInfo.setfNumber(faxno[2]); }
		 */
		if (resContact.getPhoneNo() != null) {
			String[] landNo = resContact.getPhoneNo().split("-");
			if (landNo.length == 3) {
				contactInfo.setlArea(StringUtil.nullConvertToString(landNo[1]));
				contactInfo.setlCountry(StringUtil.nullConvertToString(landNo[0]));
				contactInfo.setlNumber(StringUtil.nullConvertToString(landNo[2]));
			}
		} else {
			contactInfo.setlArea("");
			contactInfo.setlCountry("");
			contactInfo.setlNumber("");
		}

		if (resContact.getMobileNo() != null) {
			String[] strMobile = resContact.getMobileNo().split("-");
			if (strMobile.length == 3) {
				contactInfo.setmArea(StringUtil.nullConvertToString(strMobile[1]));
				contactInfo.setmCountry(StringUtil.nullConvertToString(strMobile[0]));
				contactInfo.setmNumber(StringUtil.nullConvertToString(strMobile[2]));
			}
		} else {
			contactInfo.setmArea("");
			contactInfo.setmCountry("");
			contactInfo.setmNumber("");
		}

		contactInfo.setFirstName(StringUtil.nullConvertToString(resContact.getFirstName()));
		contactInfo.setLastName(StringUtil.nullConvertToString(resContact.getLastName()));
		contactInfo.setTitle(StringUtil.nullConvertToString(resContact.getTitle()));
		contactInfo.setNationality(StringUtil.nullConvertToString(resContact.getNationality()));
		contactInfo.setPreferredLangauge(resContact.getPreferredLanguage());

		// set emergency contact information
		contactInfo.setEmgnTitle(StringUtil.nullConvertToString(resContact.getEmgnTitle()));
		contactInfo.setEmgnFirstName(StringUtil.nullConvertToString(resContact.getEmgnFirstName()));
		contactInfo.setEmgnLastName(StringUtil.nullConvertToString(resContact.getEmgnLastName()));

		if (resContact.getEmgnPhoneNo() != null) {
			String[] emgnLandNo = resContact.getEmgnPhoneNo().split("-");
			if (emgnLandNo.length == 3) {
				contactInfo.setEmgnLArea(StringUtil.nullConvertToString(emgnLandNo[1]));
				contactInfo.setEmgnLCountry(StringUtil.nullConvertToString(emgnLandNo[0]));
				contactInfo.setEmgnLNumber(StringUtil.nullConvertToString(emgnLandNo[2]));
			}
		} else {
			contactInfo.setEmgnLArea("");
			contactInfo.setEmgnLCountry("");
			contactInfo.setEmgnLNumber("");
		}

		contactInfo.setEmgnEmail(StringUtil.nullConvertToString(resContact.getEmgnEmail()));

		return contactInfo;
	}

	public static CommonReservationContactInfo retrieveContactInfo(ContactInfoDTO contactInfo) {
		CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();

		lccClientReservationContactInfo.setTitle(contactInfo.getTitle());
		lccClientReservationContactInfo.setFirstName(contactInfo.getFirstName());
		lccClientReservationContactInfo.setLastName(contactInfo.getLastName());
		lccClientReservationContactInfo.setStreetAddress1(contactInfo.getAddresline());
		lccClientReservationContactInfo.setStreetAddress2(contactInfo.getAddresStreet());
		lccClientReservationContactInfo.setCity(contactInfo.getCity());
		lccClientReservationContactInfo.setCountryCode(contactInfo.getCountry());
		lccClientReservationContactInfo
				.setMobileNo(contactInfo.getmCountry() + "-" + contactInfo.getmArea() + "-" + contactInfo.getmNumber());
		lccClientReservationContactInfo
				.setPhoneNo(contactInfo.getlCountry() + "-" + contactInfo.getlArea() + "-" + contactInfo.getlNumber());
		lccClientReservationContactInfo.setFax("--");
		lccClientReservationContactInfo.setEmail(contactInfo.getEmailAddress());
		if (contactInfo.getNationality() != null && !contactInfo.getNationality().trim().equals("")) {
			lccClientReservationContactInfo.setNationalityCode(Integer.parseInt(contactInfo.getNationality()));
		}
		return lccClientReservationContactInfo;
	}

	public static SessionTimeoutDataDTO fetchSessionTimeoutData(HttpServletRequest request) {
		SessionTimeoutDataDTO sdTO = new SessionTimeoutDataDTO();
		String language = SessionUtil.getLanguage(request);
		String parameterKey = SystemParamKeys.SESSION_TIMEOUT_BANNER_POPUP;

		String strSec = I18NUtil.getMessage("Reservation_lblSeconds", language);
		String strTimeout = I18NUtil.getMessage("Reservation_lblMsgTimeout", language);
		String strCancel = I18NUtil.getMessage("Reservation_lblMsgCancel", language);
		sdTO.setLblSeconds(strSec);
		sdTO.setLblMsgTimeout(strTimeout);
		sdTO.setLblMsgCancel(strCancel);
		if (SystemUtil.isKioskUser(request)) {
			parameterKey = SystemParamKeys.KIOSK_SES_TIMEOUT;
		}
		sdTO.setTimeout(new Integer(IBEModuleUtils.getGlobalConfig().getBizParam(parameterKey)).intValue());
		sdTO.setDisplayThreshold(80000);
		return sdTO;
	}

	/**
	 * Get Reservation Process Parameters
	 * 
	 * @param request
	 * @param reservation
	 * @param system
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationProcessDTO getReservationProcessParams(HttpServletRequest request, LCCClientReservation reservation,
			SYSTEM system) throws ModuleException {
		ReservationProcessDTO resParams = new ReservationProcessDTO();
		String loyaltyAccountNo = SessionUtil.getLoyaltyAccountNo(request);

		if (loyaltyAccountNo != null)
			resParams.setLoyaltyAccountExist(true);
		else
			resParams.setLoyaltyAccountExist(false);

		Customer cust = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
		boolean cancelRes = (reservation.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL));

		if (cust != null) {
			// boolean isIBECancalAllow = AppSysParamsUtil.isAllowIBECancellationServicesReservation();
			boolean isIBECancalAllow = isResParamEnabled(reservation, ModifcationParamTypes.CANCEL_ALLOW, system, true);
			resParams.setCancelSegment(isIBECancalAllow && reservation.isModifiableReservation());
			resParams.setCancelReservation(isIBECancalAllow && reservation.isModifiableReservation());
			// resParams.setModifyReservation(true);
			/* segment modification IBE */
			resParams.setAllowModifyDate(isResParamEnabled(reservation, ModifcationParamTypes.ALLOW_MODIFY_DATE, system, true)
					&& reservation.isModifiableReservation());
			resParams.setAllowModifyRoute(isResParamEnabled(reservation, ModifcationParamTypes.ALLOW_MODIFY_ROUTE, system, true)
					&& reservation.isModifiableReservation());

			resParams.setShowSeat(isResParamEnabled(reservation, ModifcationParamTypes.ADD_SEAT, system, true));
			resParams.setShowMeal(isResParamEnabled(reservation, ModifcationParamTypes.ADD_MEAL, system, true));
			resParams.setShowBaggage(isResParamEnabled(reservation, ModifcationParamTypes.ADD_BAGGAGE, system, true));
			resParams.setShowAirportService(
					isResParamEnabled(reservation, ModifcationParamTypes.ADD_AIRPORT_SERVICE, system, true));
			resParams.setShowAirportTransfer(
					isResParamEnabled(reservation, ModifcationParamTypes.ADD_AIRPORT_TRANSFER, system, true));
			// AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddInflightServicesEnabled()
			resParams.setShowInflightService(isResParamEnabled(reservation, ModifcationParamTypes.ADD_SSR, system, true));
			resParams.setShowInsurance(AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddInsurance());
			resParams.setAddGroundSegment(isResParamEnabled(reservation, ModifcationParamTypes.ADD_BUS_SEGMENT, system, true)
					&& reservation.isModifiableReservation());
		} else {
			resParams.setCancelSegment(isResParamEnabled(reservation, ModifcationParamTypes.CANCEL_ALLOW, system, false)
					&& reservation.isModifiableReservation());
			resParams.setCancelReservation(isResParamEnabled(reservation, ModifcationParamTypes.CANCEL_ALLOW, system, false)
					&& reservation.isModifiableReservation());
			// resParams.setModifyReservation(AppSysParamsUtil.isUnregisterdUserCanMofidyReservation());
			/* segment modification IBE */

			resParams.setAllowModifyDate(isResParamEnabled(reservation, ModifcationParamTypes.ALLOW_MODIFY_DATE, system, false)
					&& reservation.isModifiableReservation());
			resParams.setAllowModifyRoute(isResParamEnabled(reservation, ModifcationParamTypes.ALLOW_MODIFY_ROUTE, system, false)
					&& reservation.isModifiableReservation());

			resParams.setShowSeat(isResParamEnabled(reservation, ModifcationParamTypes.ADD_SEAT, system, false));
			resParams.setShowMeal(isResParamEnabled(reservation, ModifcationParamTypes.ADD_MEAL, system, false));
			resParams.setShowBaggage(isResParamEnabled(reservation, ModifcationParamTypes.ADD_BAGGAGE, system, false));
			resParams.setShowAirportService(
					isResParamEnabled(reservation, ModifcationParamTypes.ADD_AIRPORT_SERVICE, system, false));
			resParams.setShowAirportTransfer(
					isResParamEnabled(reservation, ModifcationParamTypes.ADD_AIRPORT_TRANSFER, system, false));
			resParams.setShowInsurance(isResParamEnabled(reservation, ModifcationParamTypes.ADD_INSURANCE, system, false));
			resParams.setAddGroundSegment(isResParamEnabled(reservation, ModifcationParamTypes.ADD_BUS_SEGMENT, system, false)
					&& reservation.isModifiableReservation());
			resParams.setShowInflightService(isResParamEnabled(reservation, ModifcationParamTypes.ADD_SSR, system, false));
		}

		if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
				&& (!AppSysParamsUtil.allowForceCNFBookingModification() && reservation.getTotalAvailableBalance()
						.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0)) {
			resParams.setCancelSegment(false);
			resParams.setCancelReservation(false);
			// resParams.setModifyReservation(false);
			resParams.setAllowModifyDate(false);
			resParams.setAllowModifyRoute(false);
			resParams.setMakePayment(true);
		}
		if (reservation.getTotalAvailableBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			resParams.setMakePayment(true);
		}
		resParams.setAllowForceConfirmModify(AppSysParamsUtil.allowForceCNFBookingModification());
		resParams.setSeatMapEnable(!cancelRes && resParams.isShowSeat());
		resParams.setMealEnable(!cancelRes && resParams.isShowMeal());
		resParams.setBaggageEnable(!cancelRes && resParams.isShowBaggage());
		resParams.setAirportServiceEnable(!cancelRes && resParams.isShowAirportService());
		resParams.setAirportTransferEnable(!cancelRes && resParams.isShowAirportTransfer());

		resParams.setInsuranceEnable(
				(reservation.getReservationInsurances() == null || reservation.getReservationInsurances().isEmpty()
						|| reservation.getReservationInsurances().get(0).getInsuranceQuoteRefNumber() == null
						|| reservation.getReservationInsurances().get(0).getInsuranceQuoteRefNumber().trim().equals(""))
						&& !cancelRes && resParams.isShowInsurance());

		resParams.setInflightServiceEnable(!cancelRes && resParams.isShowInflightService());

		Collection collSegment = reservation.getSegments();
		boolean insuranceBufferOk = false;
		for (Object object : collSegment) {
			LCCClientReservationSegment reservationSegmentDTO = (LCCClientReservationSegment) object;
			if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				insuranceBufferOk = (insuranceBufferOk
						|| isEnableService(LCCAncillaryType.INSURANCE, reservationSegmentDTO.getDepartureDate(), true, system));
			}
		}
		resParams.setInsuranceEnable(resParams.isInsuranceEnable() && insuranceBufferOk);
		resParams = updateWithAncillaryAvailability(resParams, reservation, request, system);

		// Reservation Cancel Status - if flown Segments
		for (Object object : collSegment) {
			LCCClientReservationSegment reservationSegmentDTO = (LCCClientReservationSegment) object;
			if (reservationSegmentDTO.isModifible() == false
					&& !reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				resParams.setCancelReservation(false);
				break;
			}
			if (reservationSegmentDTO.isFlownSegment()) {
				resParams.setCancelReservation(false);
				break;
			}
		}

		// override cancel/modify/split operations
		LCCPromotionInfoTO lccPromotionInfoTO = reservation.getLccPromotionInfoTO();
		if (lccPromotionInfoTO != null) {
			if (!lccPromotionInfoTO.isModificationAllowed()) {
				resParams.setAllowModifyDate(false);
				resParams.setAllowModifyRoute(false);
			}

			if (!lccPromotionInfoTO.isCancellationAllowed()) {
				resParams.setCancelSegment(false);
			}
		}

		resParams.setLmsEnabled(AppSysParamsUtil.isLMSEnabled());
		resParams.setNameChangeAllowed(ReservationUtil.isResEligibleForNameChange(reservation));

		return resParams;
	}

	public static List<AncillaryPaxTo> createPaxWiseAnci(Set<LCCClientReservationPax> passengers) {
		List<AncillaryPaxTo> paxWiseAnci = new ArrayList<AncillaryPaxTo>();
		for (LCCClientReservationPax pax : passengers) {
			AncillaryPaxTo anciPax = new AncillaryPaxTo();
			anciPax.setFirstName(pax.getFirstName());
			anciPax.setLastName(pax.getLastName());
			anciPax.setDateOfBirth(pax.getDateOfBirth());
			anciPax.setNationalityCode(pax.getNationalityCode());
			anciPax.setPaxType(pax.getPaxType());
			anciPax.setTitle(pax.getTitle());
			anciPax.setPaxSequence(pax.getPaxSequence());
			anciPax.setTravellerRefNo(pax.getTravelerRefNumber());
			anciPax.setSelectedAncillaries(pax.getSelectedAncillaries());
			paxWiseAnci.add(anciPax);
			Set<LCCClientReservationPax> infants = pax.getInfants();
			if (infants != null) {
				paxWiseAnci.addAll(createPaxWiseAnci(infants));
			}
		}
		if (paxWiseAnci.size() > 0) {
			Collections.sort(paxWiseAnci);
		}
		return paxWiseAnci;
	}

	private static boolean isResParamEnabled(LCCClientReservation reservation, String modificationType, SYSTEM system,
			boolean isRegUser) {
		if (system == SYSTEM.INT) {
			return reservation.getInterlineModificationParams().contains(modificationType);
		} else {
			if (modificationType.equals(ModifcationParamTypes.ADD_INSURANCE)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddInsurance());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddInsurance());
				}
			} else if (modificationType.equals(ModifcationParamTypes.EDIT_INSURANCE)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditInsurance());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditInsurance());
				}
			} else if (modificationType.equals(ModifcationParamTypes.ADD_MEAL)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddMeal());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddMeal());
				}
			} else if (modificationType.equals(ModifcationParamTypes.EDIT_MEAL)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditMeal());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditMeal());
				}
			} else if (modificationType.equals(ModifcationParamTypes.ADD_BAGGAGE)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddBaggage());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddBaggage());
				}
			} else if (modificationType.equals(ModifcationParamTypes.EDIT_BAGGAGE)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditBaggage());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditBaggage());
				}
			} else if (modificationType.equals(ModifcationParamTypes.ADD_SEAT)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSeat());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSeat());
				}
			} else if (modificationType.equals(ModifcationParamTypes.EDIT_SEAT)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSeat());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSeat());
				}
			} else if (modificationType.equals(ModifcationParamTypes.ADD_AIRPORT_SERVICE)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddAirportServices());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddAirportServices());
				}
			} else if (modificationType.equals(ModifcationParamTypes.EDIT_AIRPORT_SERVICE)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditAirportServices());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditAirportServices());
				}
			} else if (modificationType.equals(ModifcationParamTypes.ADD_AIRPORT_TRANSFER)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isIBEAddAirportTransfers()); // as request
				} else {
					return (AppSysParamsUtil.isIBEAddAirportTransfers());
				}
			} else if (modificationType.equals(ModifcationParamTypes.ALLOW_MODIFY_DATE)) {
				if (isRegUser) {
					return AppSysParamsUtil.isAllowModifySegmentDateIBE();
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanMofidyReservation()
							&& AppSysParamsUtil.isAllowModifySegmentDateIBE());
				}

			} else if (modificationType.equals(ModifcationParamTypes.ALLOW_MODIFY_ROUTE)) {
				if (isRegUser) {
					return AppSysParamsUtil.isAllowModifySegmentRouteIBE();
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanMofidyReservation()
							&& AppSysParamsUtil.isAllowModifySegmentRouteIBE());
				}
			} else if (modificationType.equals(ModifcationParamTypes.CANCEL_ALLOW)) {
				if (isRegUser) {
					return AppSysParamsUtil.isAllowIBECancellationServicesReservation();
				} else {
					return AppSysParamsUtil.isUnregisterdUserCanCancelSegment();
				}
			} else if (modificationType.equals(ModifcationParamTypes.ADD_BUS_SEGMENT)) {
				// Both regitered and unregistered users are allowed add bus segment
				return AppSysParamsUtil.isIBEAddBusSegment();
			} else if (modificationType.equals(ModifcationParamTypes.ADD_SSR)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSSR());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSSR());
				}
			} else if (modificationType.equals(ModifcationParamTypes.EDIT_SSR)) {
				if (isRegUser) {
					return (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSSR());
				} else {
					return (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSSR());
				}
			}
		}
		return false;
	}

	private static ReservationProcessDTO updateWithAncillaryAvailability(ReservationProcessDTO resParams,
			LCCClientReservation reservation, HttpServletRequest request, SYSTEM system) throws ModuleException {
		/*
		 * if(system == SYSTEM.INT){ resParams.setSeatMapEnable(false); resParams.setMealEnable(false);
		 * resParams.setInsuranceEnable(false); resParams.setShowSeat(false); resParams.setShowMeal(false);
		 * resParams.setShowInsurance(false); }else{
		 */

		LCCAncillaryAvailabilityInDTO inAncillaryAvailabilityDTO = new LCCAncillaryAvailabilityInDTO();
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		boolean isReturn = false;
		for (LCCClientReservationSegment seg : reservation.getSegments()) {
			if (!ReservationStatus.CANCEL.equals(seg.getStatus())
					&& !BookingClass.BookingClassType.STANDBY.equals(seg.getBookingType())) {
				FlightSegmentTO fltSegTO = new FlightSegmentTO();
				fltSegTO.setSegmentCode(seg.getSegmentCode());
				fltSegTO.setFlightRefNumber(seg.getFlightSegmentRefNumber());
				fltSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(seg.getFlightSegmentRefNumber()));
				fltSegTO.setDepartureDateTime(seg.getDepartureDate());
				fltSegTO.setDepartureDateTimeZulu(seg.getDepartureDateZulu());
				fltSegTO.setArrivalDateTime(seg.getArrivalDate());
				fltSegTO.setArrivalDateTimeZulu(seg.getArrivalDateZulu());
				fltSegTO.setOperatingAirline(seg.getCarrierCode());
				fltSegTO.setLogicalCabinClassCode(seg.getLogicalCabinClass());
				fltSegTO.setCabinClassCode(seg.getCabinClassCode());
				fltSegTO.setFlightNumber(seg.getFlightNo());
				fltSegTO.setBaggageONDGroupId(seg.getBaggageONDGroupId());
				fltSegTO.setBookingClass(seg.getBookingClassCode());
				flightSegmentTOs.add(fltSegTO);
				isReturn = (isReturn || "Y".equals(seg.getReturnFlag()));
			}
		}
		AnciAvailChecker anciAvailChecker = new AnciAvailChecker();

		Collections.sort(flightSegmentTOs);
		inAncillaryAvailabilityDTO.addAllFlightSegmentTOs(flightSegmentTOs);
		inAncillaryAvailabilityDTO.setQueryingSystem(system);
		InsuranceFltSegBuilder insFltSegBldr = null;
		if (resParams.isSeatMapEnable())
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.SEAT_MAP);
		if (resParams.isMealEnable())
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.MEALS);
		if (resParams.isBaggageEnable())
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.BAGGAGE);
		if (resParams.isAirportServiceEnable()) {
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.AIRPORT_SERVICE);
		}
		if (resParams.isAirportTransferEnable()) {
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.AIRPORT_TRANSFER);
		}
		if (resParams.isInflightServiceEnable()) {
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.SSR);
		}
		if (resParams.isInsuranceEnable()) {
			insFltSegBldr = new InsuranceFltSegBuilder(flightSegmentTOs);
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.INSURANCE);

			inAncillaryAvailabilityDTO.setInsuranceOrigin(insFltSegBldr.getOriginAirport());
		}
		anciAvailChecker.recordAvailability(reservation.getPassengers());

		inAncillaryAvailabilityDTO.setAppIndicator(AppIndicatorEnum.APP_IBE);
		inAncillaryAvailabilityDTO.setModifyAncillary(true);

		inAncillaryAvailabilityDTO.setDepartureSegmentCode(
				com.isa.thinair.webplatform.api.util.ReservationUtil.getDepartureSegmentCode(flightSegmentTOs));

		LCCReservationBaggageSummaryTo baggageSummaryTo = AncillaryCommonUtil.getResBaggageSummary(reservation);
		baggageSummaryTo.setAllowTillFinalCutOver(false);
		inAncillaryAvailabilityDTO.setBaggageSummaryTo(baggageSummaryTo);

		List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO = null;
		if (inAncillaryAvailabilityDTO.getAncillaryTypes() != null && !inAncillaryAvailabilityDTO.getAncillaryTypes().isEmpty()) {
			AnciAvailabilityRS anciAvailabilityRS = ModuleServiceLocator.getAirproxyAncillaryBD()
					.getAncillaryAvailability(inAncillaryAvailabilityDTO, TrackInfoUtil.getBasicTrackInfo(request));
			availabilityOutDTO = anciAvailabilityRS.getLccAncillaryAvailabilityOutDTOs();
		}

		boolean isFutureSegmentPresent = isFutureSegmentPresent(flightSegmentTOs);

		if (resParams.isSeatMapEnable()) {
			resParams.setSeatMapEnable(isFutureSegmentPresent && resParams.isSeatMapEnable()
					&& anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.SEAT_MAP, anciAvailChecker, system));
		}
		if (resParams.isMealEnable()) {
			resParams.setMealEnable(isFutureSegmentPresent && resParams.isMealEnable()
					&& anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.MEALS, anciAvailChecker, system));
		}
		if (resParams.isBaggageEnable()) {
			resParams.setBaggageEnable(isFutureSegmentPresent && resParams.isBaggageEnable()
					&& anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.BAGGAGE, anciAvailChecker, system));
		}
		if (resParams.isAirportServiceEnable()) {
			resParams.setAirportServiceEnable(
					isFutureSegmentPresent && resParams.isAirportServiceEnable() && checkAirportLevelAvailability(
							availabilityOutDTO, flightSegmentTOs, anciAvailChecker, LCCAncillaryType.AIRPORT_SERVICE));
		}
		if (resParams.isAirportTransferEnable()) {
			resParams.setAirportTransferEnable(
					isFutureSegmentPresent && resParams.isAirportTransferEnable() && checkAirportLevelAvailabilityForApt(
							availabilityOutDTO, flightSegmentTOs, anciAvailChecker, LCCAncillaryType.AIRPORT_TRANSFER));
		}
		if (resParams.isInflightServiceEnable()) {
			resParams.setInflightServiceEnable(isFutureSegmentPresent && resParams.isInflightServiceEnable()
					&& anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.SSR, anciAvailChecker, system));
		}
		if (resParams.isInsuranceEnable()) {
			resParams.setInsuranceEnable(isFutureSegmentPresent && resParams.isInsuranceEnable()
					&& checkInsuranceAvailability(availabilityOutDTO, insFltSegBldr.getOriginFltRefNo()));
		}

		if (availabilityOutDTO != null) {
			updateAncillariesWithModificationParameters(resParams, reservation, availabilityOutDTO, flightSegmentTOs, system);
		}

		return resParams;
	}

	private static void updateAncillariesWithModificationParameters(ReservationProcessDTO resParams,
			LCCClientReservation reservation, List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			List<FlightSegmentTO> flightSegmentTOs, SYSTEM system) {

		if (isAnciAlreadyExists(reservation, LCCAncillaryType.SEAT_MAP)) {
			boolean seatAvailable = isAnciAvailableAtLeastOneSegment(availabilityOutDTO, LCCAncillaryType.SEAT_MAP, system);
			boolean isEditSeatEnable = isResParamEnabled(reservation, ModifcationParamTypes.EDIT_SEAT, system, false);
			resParams.setSeatMapEnable(seatAvailable && isEditSeatEnable && reservation.isModifiableReservation());
		}
		if (isAnciAlreadyExists(reservation, LCCAncillaryType.MEALS)) {
			boolean mealAvailable = isAnciAvailableAtLeastOneSegment(availabilityOutDTO, LCCAncillaryType.MEALS, system);
			boolean isEditMealEnable = isResParamEnabled(reservation, ModifcationParamTypes.EDIT_MEAL, system, false);
			resParams.setMealEnable(mealAvailable && isEditMealEnable && reservation.isModifiableReservation());
		}
		if (isAnciAlreadyExists(reservation, LCCAncillaryType.BAGGAGE)) {
			boolean baggageAvailable = isAnciAvailableAtLeastOneSegment(availabilityOutDTO, LCCAncillaryType.BAGGAGE, system);
			boolean isEditBaggageEnable = isResParamEnabled(reservation, ModifcationParamTypes.EDIT_BAGGAGE, system, false);
			resParams.setBaggageEnable(baggageAvailable && isEditBaggageEnable && reservation.isModifiableReservation());
		}
		if (isAnciAlreadyExists(reservation, LCCAncillaryType.AIRPORT_SERVICE)) {
			AnciAvailChecker anciAvailChecker = new AnciAvailChecker();
			anciAvailChecker.recordAvailability(reservation.getPassengers());
			boolean airportServiceAvailable = checkAirportLevelAvailability(availabilityOutDTO, flightSegmentTOs,
					anciAvailChecker, LCCAncillaryType.AIRPORT_SERVICE);
			boolean isEditBaggageEnable = isResParamEnabled(reservation, ModifcationParamTypes.EDIT_AIRPORT_SERVICE, system,
					false);
			resParams.setAirportServiceEnable(
					airportServiceAvailable && isEditBaggageEnable && reservation.isModifiableReservation());
		}
		if (isAnciAlreadyExists(reservation, LCCAncillaryType.SSR)) {
			boolean inflightServiceAvailable = isAnciAvailableAtLeastOneSegment(availabilityOutDTO, LCCAncillaryType.SSR, system);
			boolean isEditSSREnable = isResParamEnabled(reservation, ModifcationParamTypes.EDIT_SSR, system, false);
			resParams.setInflightServiceEnable(
					inflightServiceAvailable && isEditSSREnable && reservation.isModifiableReservation());
		}
		if (!resParams.isInsuranceEnable()) {
			boolean insuranceAvailable = checkInsuranceAvailability(availabilityOutDTO);
			boolean isEditInsuarance = isResParamEnabled(reservation, ModifcationParamTypes.EDIT_INSURANCE, system, false);
			resParams.setInsuranceEnable(insuranceAvailable && isEditInsuarance && reservation.isModifiableReservation());
		}

	}

	// returns true if there are any future segments present in the list of flight segments.
	private static boolean isFutureSegmentPresent(List<FlightSegmentTO> flightSegmentTOs) {
		boolean isFutureSegmentPresent = false;
		if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
			for (FlightSegmentTO fltSegmentTO : flightSegmentTOs) {
				if (fltSegmentTO.getDepartureDateTimeZulu().after(new Date())) {
					isFutureSegmentPresent = true;
					break;
				}
			}
		}
		return isFutureSegmentPresent;
	}

	private static boolean checkInsuranceAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String fltRefNo) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (FlightRefNumberUtil
						.getSegmentIdFromFlightRPH(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber())
						.intValue() == FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo).intValue()) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(LCCAncillaryType.INSURANCE))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
					break;
				}
			}
		}
		return false;
	}

	private static boolean checkInsuranceAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(LCCAncillaryType.INSURANCE))
							&& ancillaryStatus.isAvailable()) {
						return true;
					}
					break;
				}
			}
		}
		return false;
	}

	private static boolean anciActiveInLeastOneSeg(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType,
			AnciAvailChecker anciAvailChecker, SYSTEM system) {
		for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
			for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
				if ((ancillaryStatus.isAvailable())
						&& !anciAvailChecker.isSelectedByAll(anciType,
								ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber())
						&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))
						&& isEnableService(anciType, ancillaryAvailabilityOutDTO.getFlightSegmentTO().getDepartureDateTimeZulu(),
								true, system)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isAnciAvailableAtLeastOneSegment(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			String anciType, SYSTEM system) {
		for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
			for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
				if ((ancillaryStatus.isAvailable())
						&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))
						&& isEnableService(anciType, ancillaryAvailabilityOutDTO.getFlightSegmentTO().getDepartureDateTimeZulu(),
								true, system)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean checkAirportLevelAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			List<FlightSegmentTO> flightSegmentTOs, AnciAvailChecker anciAvailChecker, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (isAvailableForSelecetedSegment(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber(),
						flightSegmentTOs)) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))
								&& ancillaryStatus.isAvailable() && !anciAvailChecker.isSelectedByAll(anciType,
										ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber())) {
							List<String> airportList = anciAvailChecker.getServicesNotAttachedAirportsInASegment(
									ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber(), anciType);
							if (airportList.size() == 0) {
								return true;
							} else if (airportList.contains(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getAirportCode())) {
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	private static boolean checkAirportLevelAvailabilityForApt(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			List<FlightSegmentTO> flightSegmentTOs, AnciAvailChecker anciAvailChecker, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (isAvailableForSelecetedSegment(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber(),
						flightSegmentTOs)) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))
								&& ancillaryStatus.isAvailable() && !anciAvailChecker.isSelectedByAll(anciType,
										ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber())) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	private static boolean checkAirportLevelAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			List<FlightSegmentTO> flightSegmentTOs, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (isAvailableForSelecetedSegment(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber(),
						flightSegmentTOs)) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	public static boolean isAvailableForSelecetedSegment(String fltRefNo, List<FlightSegmentTO> flightSegmentTOs) {
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getFlightRefNumber() != null) {
				String[] fltRefArr1 = flightSegmentTO.getFlightRefNumber().split("-");
				String[] fltRefArr2 = fltRefNo.split("-");
				if (fltRefArr1[0].equals(fltRefArr2[0])) {
					return true;
				}
			}
		}

		return false;
	}

	private static boolean isAnciAlreadyExists(LCCClientReservation reservation, String anciType) {
		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			for (LCCSelectedSegmentAncillaryDTO segAnci : pax.getSelectedAncillaries()) {

				if (LCCAncillaryType.SEAT_MAP.equals(anciType)) {
					LCCAirSeatDTO seatDTO = segAnci.getAirSeatDTO();
					if (seatDTO != null && seatDTO.getSeatNumber() != null && !seatDTO.getSeatNumber().equals("")) {
						return true;
					}
				}

				if (LCCAncillaryType.MEALS.equals(anciType)) {
					List<LCCMealDTO> mealDTOs = segAnci.getMealDTOs();
					if (mealDTOs != null && mealDTOs.size() > 0) {
						return true;
					}
				}

				if (LCCAncillaryType.BAGGAGE.equals(anciType)) {
					List<LCCBaggageDTO> baggageDTOs = segAnci.getBaggageDTOs();
					if (baggageDTOs != null && baggageDTOs.size() > 0) {
						return true;
					}
				}

				if (LCCAncillaryType.AIRPORT_SERVICE.equals(anciType)) {
					List<LCCAirportServiceDTO> airportServices = segAnci.getAirportServiceDTOs();
					if (airportServices != null && airportServices.size() > 0) {
						return true;
					}
				}
				if (LCCAncillaryType.SSR.equals(anciType)) {
					List<LCCSpecialServiceRequestDTO> ssrs = segAnci.getSpecialServiceRequestDTOs();
					if (ssrs != null && ssrs.size() > 0) {
						return true;
					}
				}
			}
		}

		return false;
	}

	private static boolean isEnableService(String caller, Date DepDateZulu, boolean isInEditMode, SYSTEM system) {
		if (system == SYSTEM.INT) {
			return true;
		} else {
			long diffMils = 3600000;

			long lngBufferTime = 0;
			Date currentTime = DateUtil.getCurrentZuluDateTime();

			if (caller.equals(LCCAncillaryType.SEAT_MAP)) {
				diffMils = AppSysParamsUtil.getIBESeatmapStopCutoverInMillis();

			} else if (caller.equals(LCCAncillaryType.MEALS)) {
				diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());

			} else if (caller.equals(LCCAncillaryType.BAGGAGE)) {
				diffMils = BaggageTimeWatcher.getBaggageCutOverTimeForIBEInMillis(isInEditMode);

			} else if (caller.equals(LCCAncillaryType.SSR)) {
				diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();

			} else if (caller.equals(LCCAncillaryType.INSURANCE)) {
				diffMils = lngBufferTime;
			}

			if (isInEditMode && ((DepDateZulu.getTime() - lngBufferTime) < currentTime.getTime())) {
				if (log.isDebugEnabled()) {
					log.debug("Cannot Modify " + caller + " is in Modification Buffer");
				}
				return false;
			} else if (DepDateZulu.getTime() - diffMils < currentTime.getTime()) {
				return false;
			} else {
				return true;
			}
		}
	}

	public static BigDecimal getTotalAncillaryCharges(Collection<ReservationPaxTO> paxList) {
		BigDecimal totalAnci = BigDecimal.ZERO;
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
		if (paxList != null) {
			for (ReservationPaxTO pax : paxList) {
				extChgList.addAll(pax.getExternalCharges());
			}
		}

		return getPaxExtCharges(extChgList);
	}

	public static BigDecimal getPaxExtCharges(List<LCCClientExternalChgDTO> list) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : list) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	public static int getAnciUpdatedSegmentCount(Collection<ReservationPaxTO> paxList) {
		List<String> segments = new ArrayList<String>();
		for (ReservationPaxTO pax : paxList) {
			if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
				if (pax.getAncillaryTotal(true).compareTo(BigDecimal.ZERO) > 0) {
					pax.updateAnciModifiedSegments(segments);
				}
			}
		}
		return segments.size();

	}

	public static FareQuoteTO fillFareQuote(Collection<ReservationPaxTO> paxList, String selectedCurrency,
			BigDecimal totalWithoutAnci, BigDecimal ccFee, String totalFare, String totalTax, String totalTaxSurcharge,
			LoyaltyPaymentOption loyalPayOpt, BigDecimal loyaltyCredit, boolean isModifySegment, ReservationBalanceTO balance,
			BigDecimal creditableAmount, boolean isAddModifyAnci, BigDecimal reservationBalance, boolean isAddGroundSegment,
			ExchangeRateProxy exchangeRateProxy, boolean promoExists, BigDecimal promoDiscount, boolean isCreditDiscount,
 BigDecimal ibeReturnFareDiscount,
					boolean isRequoteSearch, BigDecimal lmsPaymentAmount, BigDecimal voucherRedeemAmount, boolean infantPaymentSeparated) throws ModuleException {
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
		BigDecimal removedAnciTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPaxTO pax : paxList) {
			extChgList.addAll(pax.getExternalCharges());
			if (isAddModifyAnci) {
				removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, pax.getToRemoveAncillaryTotal());
			}
		}
		BigDecimal totalAncillaryCharges = BigDecimal.ZERO;
		BigDecimal seatChg = BigDecimal.ZERO;
		BigDecimal mealChg = BigDecimal.ZERO;
		BigDecimal baggageChg = BigDecimal.ZERO;
		BigDecimal inflightServiceChg = BigDecimal.ZERO;
		BigDecimal airportServiceChg = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal airportTransferChg = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal insuranceChg = BigDecimal.ZERO;
		BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalJnTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalAnciPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();

		FareQuoteTO fareQuote = new FareQuoteTO();
		for (LCCClientExternalChgDTO chgDTO : extChgList) {
			if (chgDTO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
				if (!chgDTO.isAmountConsumedForPayment()) {
					totalAncillaryCharges = AccelAeroCalculator.add(totalAncillaryCharges, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.SEAT_MAP) {
					fareQuote.setHasSeatCharge(true);
					seatChg = AccelAeroCalculator.add(seatChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.MEAL) {
					fareQuote.setHasMealCharge(true);
					mealChg = AccelAeroCalculator.add(mealChg, chgDTO.getAmount());
				}
				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.BAGGAGE) {
					fareQuote.setHasBaggageCharge(true);
					baggageChg = AccelAeroCalculator.add(baggageChg, chgDTO.getAmount());
				}

				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
					fareQuote.setHasSsrCharge(true);
					inflightServiceChg = AccelAeroCalculator.add(inflightServiceChg, chgDTO.getAmount());
				}

				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
					fareQuote.setHasAirportServiceCharge(true);
					airportServiceChg = AccelAeroCalculator.add(airportServiceChg, chgDTO.getAmount());
				}

				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_TRANSFER) {
					fareQuote.setHasAirportTransferCharge(true);
					airportTransferChg = AccelAeroCalculator.add(airportTransferChg, chgDTO.getAmount());
				}

				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INSURANCE) {
					fareQuote.setHasInsurance(true);
					insuranceChg = AccelAeroCalculator.add(insuranceChg, chgDTO.getAmount());
				}

				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
					fareQuote.setHasFlexi(true);
					flexiCharge = AccelAeroCalculator.add(flexiCharge, chgDTO.getAmount());
				}

				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.JN_ANCI) {
					fareQuote.setJnTaxApplicable(true);
					totalJnTax = AccelAeroCalculator.add(totalJnTax, chgDTO.getAmount());
				}

				if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.ANCI_PENALTY) {
					fareQuote.setAnciPenaltyApplicable(true);
					totalAnciPenalty = AccelAeroCalculator.add(totalAnciPenalty, chgDTO.getAmount());
				}
			}
		}

		fareQuote.setHasPromoDiscount(promoExists);

		if (promoDiscount != null && promoDiscount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1) {
			fareQuote.setTotalIbePromoDiscount(AccelAeroCalculator.formatAsDecimal(promoDiscount));
			if (!isCreditDiscount) {
				fareQuote.setDeductFromTotal(true);
				totalWithoutAnci = AccelAeroCalculator.add(totalWithoutAnci, promoDiscount.negate());
			}
		} else {
			promoDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		if (ibeReturnFareDiscount != null
				&& ibeReturnFareDiscount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1) {
			fareQuote.setTotalIbeFareDiscount(AccelAeroCalculator.formatAsDecimal(ibeReturnFareDiscount));
		}

		fareQuote.setReservationBalance(AccelAeroCalculator.formatAsDecimal(reservationBalance));
		if (reservationBalance.compareTo(BigDecimal.ZERO) > 0) {
			fareQuote.setHasReservationBalance(true);
		}

		BigDecimal totalPrice = AccelAeroCalculator.add(totalWithoutAnci, totalAncillaryCharges, ccFee, reservationBalance,
				totalAnciPenalty);
		BigDecimal totalPayable = totalPrice;

		if (isModifySegment) {
			fareQuote.setHasReservationBalance(false);

			BigDecimal modificationCharge = null;
			if (balance != null && balance.getSegmentSummary() != null) {
				modificationCharge = balance.getSegmentSummary().getNewModAmount();
			}

			if (modificationCharge == null) {
				modificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			}
			fareQuote.setCreditableAmount(AccelAeroCalculator.formatAsDecimal(creditableAmount.negate()));
			fareQuote.setModifyCharge(AccelAeroCalculator.formatAsDecimal(modificationCharge));
			fareQuote.setHasModifySegment(true);

			fareQuote.setHasTotalReservationCredit(true);
			fareQuote.getInBaseCurr().setTotalReservationCredit(AccelAeroCalculator.formatAsDecimal(creditableAmount.negate()));
			BalanceTo balanceTo = BalanceSummaryUtil.getTotalBalanceToPayAmount(balance, isRequoteSearch ? null : paxList, infantPaymentSeparated);
			if (balanceTo.getTotalCreditBalace().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
				fareQuote.getInBaseCurr()
						.setCreditBalnce(AccelAeroCalculator.formatAsDecimal(balanceTo.getTotalCreditBalace().negate()));
				fareQuote.setHasCreditBalance(true);
			}

			if (balanceTo.getBalanceToPay().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				fareQuote.setHasBalanceToPay(true);
				fareQuote.getInBaseCurr().setBalaceToPay(
						AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(balanceTo.getBalanceToPay(), ccFee)));
			}

			if ((balanceTo.getTotalCreditBalace().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0)
					&& balanceTo.getBalanceToPay().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				fareQuote.setHasCreditBalance(false);
				fareQuote.setHasBalanceToPay(false);
			}
			totalPrice = AccelAeroCalculator.add(totalPrice, modificationCharge);
			totalPayable = AccelAeroCalculator.add(balanceTo.getBalanceToPay(), ccFee);

		}

		if ((isAddModifyAnci || isAddGroundSegment) && creditableAmount.negate().compareTo(BigDecimal.ZERO) > 0) {
			totalPayable = AccelAeroCalculator.add(totalPayable, creditableAmount);
			fareQuote.setReservationCredit(AccelAeroCalculator.formatAsDecimal(creditableAmount));
			fareQuote.setHasReservationCredit(true);

		}
		fareQuote.setSeatMapCharge(AccelAeroCalculator.formatAsDecimal(seatChg));
		fareQuote.setMealCharge(AccelAeroCalculator.formatAsDecimal(mealChg));
		fareQuote.setSsrCharge(AccelAeroCalculator.formatAsDecimal(inflightServiceChg));
		fareQuote.setAirportServiceCharge(AccelAeroCalculator.formatAsDecimal(airportServiceChg));
		fareQuote.setAirportTransferCharge(AccelAeroCalculator.formatAsDecimal(airportTransferChg));
		fareQuote.setInsuranceCharge(AccelAeroCalculator.formatAsDecimal(insuranceChg));
		fareQuote.setFlexiCharge(AccelAeroCalculator.formatAsDecimal(flexiCharge));
		fareQuote.setBaggageCharge(AccelAeroCalculator.formatAsDecimal(baggageChg));
		fareQuote.setJnTaxAmount(AccelAeroCalculator.formatAsDecimal(totalJnTax));
		fareQuote.setAnciPenaltyAmount(AccelAeroCalculator.formatAsDecimal(totalAnciPenalty));

		fareQuote.setTotalFee(AccelAeroCalculator.formatAsDecimal(ccFee));
		if (ccFee.compareTo(BigDecimal.ZERO) > 0) {
			fareQuote.setHasFee(true);
		}
		fareQuote.setTotalFare(totalFare);
		fareQuote.setTotalTax(totalTax);
		fareQuote.setTotalTaxSurcharge(totalTaxSurcharge); // No need to add flexi charges breakdown to this.
		fareQuote.setTotalPrice(AccelAeroCalculator.formatAsDecimal(totalPrice));

		totalPayable = AccelAeroCalculator.add(totalPayable, removedAnciTotal.negate());
		totalPayable = AccelAeroCalculator.subtract(totalPayable, lmsPaymentAmount);
		if (voucherRedeemAmount != null) {
			totalPayable = AccelAeroCalculator.subtract(totalPayable, voucherRedeemAmount);
		}

		if (loyalPayOpt == LoyaltyPaymentOption.TOTAL) {
			loyaltyCredit = totalPayable;
		} else if (loyalPayOpt == LoyaltyPaymentOption.NONE) {
			loyaltyCredit = BigDecimal.ZERO;
		}

		totalPayable = AccelAeroCalculator.add(totalPayable, loyaltyCredit.negate());

		fareQuote.setLoyaltyCredit("-" + AccelAeroCalculator.formatAsDecimal(loyaltyCredit));
		if (loyaltyCredit.compareTo(BigDecimal.ZERO) > 0) {
			fareQuote.setHasLoyaltyCredit(true);
		}
		fareQuote.setTotalPayable(AccelAeroCalculator.formatAsDecimal(totalPayable));

		fareQuote.setCurrency(AppSysParamsUtil.getBaseCurrency());
		if (selectedCurrency != null && !"".equals(selectedCurrency) && !fareQuote.getCurrency().equals(selectedCurrency)) {
			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency,
					ApplicationEngine.IBE);

			if (currencyExchangeRate != null) {
				Currency pgCurrency = currencyExchangeRate.getCurrency();
				fareQuote.setSelectedCurrency(selectedCurrency);
				fareQuote.setSelectedEXRate(currencyExchangeRate.getMultiplyingExchangeRate().toPlainString());
				fareQuote.setSelectedtotalPrice(
						AccelAeroRounderPolicy.convertAndRound(totalPrice, currencyExchangeRate.getMultiplyingExchangeRate(),
								pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());
				fareQuote.setSelectedTotalPayable(
						AccelAeroRounderPolicy.convertAndRound(totalPayable, currencyExchangeRate.getMultiplyingExchangeRate(),
								pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());

				if (fareQuote.isHasFee()) {
					fareQuote.setTotalFeeInSelected(
							AccelAeroRounderPolicy.convertAndRound(ccFee, currencyExchangeRate.getMultiplyingExchangeRate(),
									pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());
				}

				fareQuote.setTotalSelectedIbePromoDiscount(
						AccelAeroRounderPolicy.convertAndRound(promoDiscount, currencyExchangeRate.getMultiplyingExchangeRate(),
								pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());
				fareQuote.setTotalSelectedIbeFareDiscount(AccelAeroRounderPolicy
						.convertAndRound(ibeReturnFareDiscount, currencyExchangeRate.getMultiplyingExchangeRate(),
								pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint())
						.toString());
				fareQuote.getTotalSelectedFee(
						AccelAeroRounderPolicy.convertAndRound(ccFee, currencyExchangeRate.getMultiplyingExchangeRate(),
								pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());
			}
		}
		fareQuote.setHasFareQuote(true);

		return fareQuote;
	}

	public static Collection<ReservationSegmentStatusDTO>
			getReservationSegmentStatus(Set<LCCClientReservationSegment> setReservationSegments) {
		Collection<ReservationSegmentStatusDTO> segmentStatusDTOs = new ArrayList<ReservationSegmentStatusDTO>();

		for (LCCClientReservationSegment reservationSegment : setReservationSegments) {
			ReservationSegmentStatusDTO reservationSegmentStatus = new ReservationSegmentStatusDTO();
			reservationSegmentStatus.setFlightSegmentRefNo(
					reservationSegment.getFlightSegmentRefNumber() + "-" + reservationSegment.getInterlineGroupKey());
			reservationSegmentStatus.setModifiable(reservationSegment.isModifible());
			reservationSegmentStatus.setStatus(reservationSegment.getStatus());
			reservationSegmentStatus.setModifyTillBufferDateTime(reservationSegment.getModifyTillBufferDateTime());
			reservationSegmentStatus.setCancelTillBufferDateTime(reservationSegment.getCancelTillBufferDateTime());
			reservationSegmentStatus.setModifyTillFlightClosureDateTime(reservationSegment.getModifyTillFlightClosureDateTime());
			segmentStatusDTOs.add(reservationSegmentStatus);
		}

		return segmentStatusDTOs;
	}

	public static Map<String, BigDecimal> getPaxCreditMap(Set<LCCClientReservationPax> passengers) {
		Map<String, BigDecimal> paxCreditMap = new HashMap<String, BigDecimal>();

		for (LCCClientReservationPax pax : passengers) {
			paxCreditMap.put(pax.getTravelerRefNumber(), pax.getTotalAvailableBalance());
		}

		return paxCreditMap;
	}

	/**
	 * Get Departure Date Time Zulu
	 * 
	 * @param flights
	 * @return
	 */
	public static Date getDepartureTimeZulu(List<FlightSegmentTO> flights) {
		Date depTimeZulu = null;

		if (flights != null && !flights.isEmpty()) {
			SortUtil.sortFlightSegmentTO(flights);
			FlightSegmentTO flightSegmentTO = flights.get(0);
			depTimeZulu = flightSegmentTO.getDepartureDateTimeZulu();
		}

		return depTimeZulu;
	}

	// TODO implement correct method to support ibe
	// This method need review
	public static boolean isOnHoldPaymentEnable(OnHold appOnHold, Date zuluDepDateTime) {

		boolean isOnHoldEnable = false;
		String strOnHoldHHMM = AppSysParamsUtil.getOnHoldBufferStartCutOver(appOnHold);

		long onholdStartCutOver = CommonUtil.getTotalMinutes(strOnHoldHHMM) * 60 * 1000;

		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(DateUtil.getCurrentZuluDateTime());

		if (currentDate.getTimeInMillis() < (zuluDepDateTime.getTime() - onholdStartCutOver)) {
			isOnHoldEnable = true;
		}

		return isOnHoldEnable;

	}

	public static boolean isOnHoldEnable(OnHold appOnHold, Date pnrReleaseTime) {
		boolean isOnHoldEnable = false;

		if (pnrReleaseTime == null) {
			isOnHoldEnable = false;
		} else {
			Date currentDateTime = Calendar.getInstance().getTime();
			if (pnrReleaseTime.getTime() > currentDateTime.getTime()) {
				isOnHoldEnable = true;
			}
		}

		if (log.isDebugEnabled()) {
			log.info("### Onhold availability(PNR Release time) :" + isOnHoldEnable);
		}

		return isOnHoldEnable;
	}

	public static String getOnHoldDisplayTime(OnHold appOnHold, Date zuluDepDateTime, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO,
			Date pnrReleaseDate) throws ModuleException {

		if (pnrReleaseDate == null)
			pnrReleaseDate = calculateOnHoldReleaseTime(appOnHold, zuluDepDateTime, onHoldReleaseTimeDTO);

		return getOnHoldDisplayTime(appOnHold, pnrReleaseDate);

	}

	public static String getOnHoldDisplayTime(OnHold appOnHold, Date pnrReleaseDate) {
		long displayTimelong = CommonUtil.getTotalMinutes(AppSysParamsUtil.getOnHoldDisplayTime(appOnHold)) * 60 * 1000;
		Calendar currentDate = Calendar.getInstance();
		long onHoldDuration = 0;

		if (pnrReleaseDate != null) {
			onHoldDuration = pnrReleaseDate.getTime() - currentDate.getTimeInMillis();
		}

		if (!AppSysParamsUtil.isApplyRouteBookingClassLevelOnholdReleaseTime() && onHoldDuration > displayTimelong) {
			return AppSysParamsUtil.getOnHoldDisplayTime(appOnHold);
		} else {
			return CommonUtil.getTimeDDHHMM(onHoldDuration);
		}
	}

	public static Date calculateOnHoldReleaseTime(OnHold appOnHold, Date depDate, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO)
			throws ModuleException {

		if (onHoldReleaseTimeDTO == null) {
			onHoldReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		}
		AppIndicatorEnum appIndicatorEnum = AppIndicatorEnum.APP_IBE;

		boolean enforcePrivilegeCheck = false;
		if (appOnHold == OnHold.KIOSK) {
			appIndicatorEnum = AppIndicatorEnum.APP_KIOSK;
		} else if (appOnHold == OnHold.IBEPAYMENT) {
			appIndicatorEnum = AppIndicatorEnum.APP_IBEPAYMENT;
		} else if (appOnHold == OnHold.IBEOFFLINEPAYMENT) {
			enforcePrivilegeCheck = true;
			appIndicatorEnum = AppIndicatorEnum.APP_OFFLINEPAYMENT;
		} else if (appOnHold == OnHold.IBE) {
			appIndicatorEnum = AppIndicatorEnum.APP_IBE;
		}

		onHoldReleaseTimeDTO.setModuleCode(appIndicatorEnum.toString());

		return ReleaseTimeUtil.getReleaseTime(null, depDate, enforcePrivilegeCheck, null, appIndicatorEnum.toString(),
				onHoldReleaseTimeDTO);
	}

	/**
	 * Validate Modifying Segments
	 * 
	 * @param colResSegs
	 * @param sessionColResSegs
	 * @param segId
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<LCCClientReservationSegment> getvalidateModifingSegment(
			Collection<LCCClientReservationSegment> colResSegs, Collection<ReservationSegmentStatusDTO> sessionColResSegs,
			String segId) throws ModuleException {

		Collection<LCCClientReservationSegment> colSegs = new ArrayList<LCCClientReservationSegment>();

		String[] selectedSegs = segId.split(":");
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MILLISECOND, -(cal.getTimeZone().getRawOffset() + cal.getTimeZone().getDSTSavings()));
		long sysTime = cal.getTimeInMillis();
		// long sysTime = new Date().getTime();

		for (LCCClientReservationSegment resSeg : colResSegs) {

			for (ReservationSegmentStatusDTO sesSeg : sessionColResSegs) {
				// Check Segment Status And Value Change
				if ((resSeg.getFlightSegmentRefNumber() + "-" + resSeg.getInterlineGroupKey())
						.equals(sesSeg.getFlightSegmentRefNo())
						&& !resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					if (resSeg.isModifible() != sesSeg.isModifiable() || !resSeg.getStatus().equals(sesSeg.getStatus())
							|| resSeg.getModifyTillBufferDateTime().compareTo(sesSeg.getModifyTillBufferDateTime()) != 0
							|| resSeg.getCancelTillBufferDateTime().compareTo(sesSeg.getCancelTillBufferDateTime()) != 0
							|| resSeg.getModifyTillFlightClosureDateTime()
									.compareTo(sesSeg.getModifyTillFlightClosureDateTime()) != 0) {

						throw new ModuleException(ExceptionConstants.KEY_UNAUTHORIZED_OPERATION);
					}
					break;
				}
			}

			for (int i = 0; i < selectedSegs.length; i++) {
				long segModifyBuffStart = 0l;
				long segCancelBuffStart = 0l;
				long segBuffEnd = 0l;

				if (resSeg.getBookingFlightSegmentRefNumber().equals(selectedSegs[i])) {
					segModifyBuffStart = resSeg.getModifyTillBufferDateTime().getTime();
					segCancelBuffStart = resSeg.getCancelTillBufferDateTime().getTime();
					segBuffEnd = resSeg.getModifyTillFlightClosureDateTime().getTime();
					if (!resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
						if (sysTime > segModifyBuffStart && sysTime > segCancelBuffStart) {
							// Need && operation here because cancel and modification time will only
							// differ in one flexi cancellation and no flexi modification.
							if (sysTime < segBuffEnd) {
								throw new ModuleException(ExceptionConstants.KEY_UNAUTHORIZED_OPERATION);
							}
						}
					}
					colSegs.add(resSeg);
					break;
				}
			}

		}
		return colSegs;
	}

	public static Object[] getDepArrZulutimes(Set<LCCClientReservationSegment> colsegs) {
		Object[] arr = new Object[2];
		Date tempDepZulu = null;
		Date tempArrZulu = null;

		for (LCCClientReservationSegment segment : com.isa.thinair.airproxy.api.utils.SortUtil.sort(colsegs)) {
			if (segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				if (tempDepZulu == null || tempDepZulu.after(segment.getDepartureDate())) {
					tempDepZulu = segment.getDepartureDate();
				}
				if (tempArrZulu == null || tempArrZulu.before(segment.getArrivalDate())) {
					tempArrZulu = segment.getArrivalDate();
				}
			}
		}
		arr[0] = tempDepZulu;
		arr[1] = tempArrZulu;

		return arr;

	}

	public static Collection<LCCClientReservationSegment> getConfirmSegments(Collection<LCCClientReservationSegment> colResSegs)
			throws ModuleException {

		Collection<LCCClientReservationSegment> colSegs = new ArrayList<LCCClientReservationSegment>();
		for (LCCClientReservationSegment resSeg : colResSegs) {
			if (!resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				colSegs.add(resSeg);
			}
		}
		return colSegs;
	}

	@SuppressWarnings("unchecked")
	public static boolean isRAKInsuranceModificationAllowed(LCCClientReservation reservation) throws ModuleException {
		Object[] flightDetails = loadFlightSegments(reservation, true, null);
		Collection<FlightInfoTO> flightDepDetails = null;
		if (isAllowedInsPurchaseOnline(flightDetails)) {
			if (flightDetails[0] != null) {
				flightDepDetails = (Collection<FlightInfoTO>) flightDetails[0];
				for (FlightInfoTO flightSeg : flightDepDetails) {
					int modificationCutover = Integer.parseInt(AppSysParamsUtil.getRakModificationCutoverTime());
					GregorianCalendar calendar2 = new GregorianCalendar();
					calendar2.setTime(new Date(flightSeg.getDepartureTimeZuluLong()));
					calendar2.add(GregorianCalendar.MINUTE, -modificationCutover);
					if (calendar2.getTimeInMillis() >= new Date().getTime()) {
						return true;
					}
					break;
				}

			}
		} else {
			return false;
		}

		return false;
	}

	/**
	 * Helper method to determine the duration of a round trip exceeds 90 days or not
	 * 
	 * @param flightDetails
	 * @return
	 */
	private static boolean isAllowedInsPurchaseOnline(Object[] flightDetails) {
		boolean isAllowed = false;
		FlightInfoTO startSeg = null;
		FlightInfoTO endSeg = null;
		Collection<FlightInfoTO> flightDepDetails = (Collection<FlightInfoTO>) flightDetails[0];
		Collection<FlightInfoTO> flightArrvDetails = (Collection<FlightInfoTO>) flightDetails[1];

		if (flightArrvDetails != null && flightArrvDetails.size() > 0) {// If this is a return trip
			for (FlightInfoTO flightSeg : flightDepDetails) {
				if (startSeg == null || startSeg.getDepartureTimeZuluLong() > flightSeg.getDepartureTimeZuluLong()) {
					startSeg = flightSeg;
				}
			}
			for (FlightInfoTO flightSeg : flightArrvDetails) {
				if (endSeg == null || endSeg.getArrivalTimeZuluLong() < flightSeg.getArrivalTimeZuluLong()) {
					endSeg = flightSeg;
				}
			}
			long dateDifferenceInMillis = endSeg.getDepartureTimeZuluLong() - startSeg.getDepartureTimeZuluLong();
			long diffrenceInDays = (dateDifferenceInMillis / (1000 * 60 * 60 * 24)) + 1;
			isAllowed = diffrenceInDays < 91;
		} else {// One way trip
			isAllowed = true;
		}

		return isAllowed;
	}

	public static String getFlightDuration(List<SegInfoDTO> segInfoDTOList) {
		String duration = "";
		if (segInfoDTOList.size() > 0) {
			long depFirstDateLong = (segInfoDTOList.get(0)).getDepartureTimeZuluLong();
			long arrLastDateLong = (segInfoDTOList.get(segInfoDTOList.size() - 1)).getArrivalTimeZuluLong();
			duration = CommonUtil.getTimeHHMM(arrLastDateLong - depFirstDateLong);
		}

		return duration;
	}

	public static Collection<FlexiDTO> getFlexiInfo(Collection<FlightInfoTO> flightInfo, List<LCCClientAlertInfoTO> flexiAlerts,
			String language) {
		List<FlightInfoTO> flightInfoList = new ArrayList<FlightInfoTO>(flightInfo);
		Collections.sort(flightInfoList);
		Collection<FlexiDTO> flexiInfo = new ArrayList<FlexiDTO>();
		if (flexiAlerts != null) {
			for (FlightInfoTO flightInfoTO : flightInfoList) {
				for (LCCClientAlertInfoTO flexiAlert : flexiAlerts) {
					if (flexiAlert.getFlightSegmantRefNumber().equals(flightInfoTO.getFlightSegmentRefNumber())
							&& !flightInfoTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
						String flexiMessage = I18NUtil.getMessage("PgFlexi_lblFlexiMessage", language);
						String flexiCnxMessage = I18NUtil.getMessage("PgFlexi_lblFlexiCnxMessage", language);
						String flexiModCnxMessage = I18NUtil.getMessage("PgFlexi_lblFlexiModCnxMessage", language);
						List<LCCClientAlertTO> alertsList = flexiAlert.getAlertTO();
						for (LCCClientAlertTO alertTO : alertsList) {
							/*
							 * Alert id 1 means modification and number of modifications contain in the alert content
							 * Alert id 2 means cancellation and we dont need the number of cancellations because it is
							 * only one always Alert id 0 means buffer time in hours. We can push this to cancellation
							 * content but if will fail when a system doesnt have cancellation flexibily. Eg 3O
							 */
							if (alertTO.getAlertId() == 1) {
								flexiMessage = flexiMessage.replace("#1", alertTO.getContent());
							} else if (alertTO.getAlertId() == 2) {
								flexiMessage = flexiMessage.replace("#3", flexiModCnxMessage);
							} else if (alertTO.getAlertId() == 0) {
								flexiMessage = flexiMessage.replace("#2", alertTO.getContent());
								flexiCnxMessage = flexiCnxMessage.replace("#2", alertTO.getContent());
							}
						}
						// All modifications consumed
						if (flexiMessage.contains("#1")) {
							flexiMessage = flexiCnxMessage;
						}
						// Flexi rule has no cancellations
						if (flexiMessage.contains("#3")) {
							flexiMessage = flexiMessage.replace("#3", "");
						}
						FlexiDTO flexiDTO = new FlexiDTO();
						flexiDTO.setOriginDestination(flightInfoTO.getOrignNDest());
						flexiDTO.setFlexiDetails(flexiMessage);
						flexiInfo.add(flexiDTO);
					}
				}
			}
		}

		return flexiInfo;
	}

	public static List<LCCClientAlertTO> generatePromotionInfoAlerts(LCCClientReservation reservation, String language) {
		LCCPromotionInfoTO lccPromotionInfoTO = reservation.getLccPromotionInfoTO();
		List<LCCClientAlertTO> alerts = null;
		if (lccPromotionInfoTO != null) {
			alerts = new ArrayList<LCCClientAlertTO>();
			String promoTypeLabel = I18NUtil.getMessage("PgPromotion_lblPromoType", language);
			LCCClientAlertTO alert = new LCCClientAlertTO();
			alert.setAlertId(0);

			String promoTypeMsg = I18NUtil.getMessage("PgPromotion_lbl" + lccPromotionInfoTO.getPromoType(), language);
			alert.setContent(
					promoTypeLabel.replace("#1", promoTypeMsg != null ? promoTypeMsg : lccPromotionInfoTO.getPromoType()));

			alerts.add(alert);

			if (PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA.equals(lccPromotionInfoTO.getPromoType())) {
				String freePaxCountLabel = I18NUtil.getMessage("PgPromotion_lblFreePaxCount", language);
				freePaxCountLabel = freePaxCountLabel.replace("#1", Integer.toString(lccPromotionInfoTO.getAppliedAdults()))
						.replace("#2", Integer.toString(lccPromotionInfoTO.getAppliedChilds()))
						.replace("#3", Integer.toString(lccPromotionInfoTO.getAppliedInfants()));
				alert = new LCCClientAlertTO();
				alert.setAlertId(0);
				alert.setContent(freePaxCountLabel);
				alerts.add(alert);
			} else if (PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA.equals(lccPromotionInfoTO.getPromoType())
					|| PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(lccPromotionInfoTO.getPromoType())
					|| PromotionCriteriaTypesDesc.SYS_GEN_PROMO.equals(lccPromotionInfoTO.getPromoType())) {

				if (PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(lccPromotionInfoTO.getPromoType())) {
					String freePaxCountLabel = I18NUtil.getMessage("PgPromotion_lblFreeAnci", language);
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					String anciContent = "";
					for (Iterator<String> iterator = lccPromotionInfoTO.getAppliedAncis().iterator(); iterator.hasNext();) {
						String anciCode = iterator.next();
						anciContent += PromotionCriteriaConstants.ANCILLARIES.getName(anciCode);

						if (iterator.hasNext()) {
							anciContent += ", ";
						}

					}
					alert.setContent(freePaxCountLabel.replace("#1", anciContent));
					alerts.add(alert);
				}

				if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(lccPromotionInfoTO.getDiscountType())
						|| PromotionCriteriaTypesDesc.SYS_GEN_PROMO.equals(lccPromotionInfoTO.getPromoType())) {
					String discountValueLabel = I18NUtil.getMessage("PgPromotion_lblDiscValue", language);

					BigDecimal discount;
					if (PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {

						discount = lccPromotionInfoTO.getCreditDiscountAmount();
					} else {
						discount = reservation.getTotalDiscount();
					}

					discountValueLabel = discountValueLabel.replace("#1", AppSysParamsUtil.getBaseCurrency())
							.replace("#2",
									AccelAeroCalculator.formatAsDecimal(new BigDecimal(lccPromotionInfoTO.getDiscountValue())))
							.replace("#3", lccPromotionInfoTO.getDiscountApplyTo());
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					alert.setContent(discountValueLabel);
					alerts.add(alert);

					String discountTotalLabel = I18NUtil.getMessage("PgPromotion_lblTotalDiscValue", language);
					discountTotalLabel = discountTotalLabel.replace("#1", AppSysParamsUtil.getBaseCurrency()).replace("#2",
							AccelAeroCalculator.formatAsDecimal(discount));
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					alert.setContent(discountTotalLabel);
					alerts.add(alert); // here
				} else if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(lccPromotionInfoTO.getDiscountType())) {
					String discountTotalLabel = I18NUtil.getMessage("PgPromotion_lblDiscountPercentage", language);
					discountTotalLabel = discountTotalLabel.replace("#1", Float.toString(lccPromotionInfoTO.getDiscountValue()));
					if (lccPromotionInfoTO.getDiscountApplyTo() == null) {
						discountTotalLabel = discountTotalLabel.replace("#2",
								I18NUtil.getMessage("PgPromotion_lblTotal", language)); // get from lbl
					} else {

						String discountTotalApplyToMsg = I18NUtil
								.getMessage("PgPromotion_lbl" + lccPromotionInfoTO.getDiscountApplyTo(), language);
						discountTotalLabel = discountTotalLabel.replace("#2", discountTotalApplyToMsg != null
								? discountTotalApplyToMsg
								: lccPromotionInfoTO.getDiscountApplyTo());
					}
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					alert.setContent(discountTotalLabel);
					alerts.add(alert);
				}
			}

			if (!lccPromotionInfoTO.isSystemGenerated()) {
				String promoRestrictionLabel = I18NUtil.getMessage("PgPromotion_lblPromoRestrictionAlert", language);
				String optNot = I18NUtil.getMessage("PgPromotion_lblAllowed", language);
				if (!lccPromotionInfoTO.isModificationAllowed()) {
					optNot = I18NUtil.getMessage("PgPromotion_lblNot", language) + " " + optNot;
				}

				promoRestrictionLabel = promoRestrictionLabel.replace("#1", optNot);

				optNot = I18NUtil.getMessage("PgPromotion_lblAllowed", language);
				if (!lccPromotionInfoTO.isCancellationAllowed()) {
					optNot = I18NUtil.getMessage("PgPromotion_lblNot", language) + " " + optNot;
				}

				promoRestrictionLabel = promoRestrictionLabel.replace("#2", optNot);

				alert = new LCCClientAlertTO();
				alert.setAlertId(0);
				alert.setContent(promoRestrictionLabel);
				alerts.add(alert);
			}

		}
		return alerts;
	}

	public static Collection<FlexiDTO> getFlexiInfoForSegments(Collection<SegInfoDTO> segInfoDTOs,
			List<LCCClientAlertInfoTO> flexiAlerts, String language) {
		List<SegInfoDTO> segInfoDTOList = new ArrayList<SegInfoDTO>(segInfoDTOs);
		Collections.sort(segInfoDTOList);
		Collection<FlexiDTO> flexiInfo = new ArrayList<FlexiDTO>();
		if (flexiAlerts != null) {
			for (SegInfoDTO segInfoDTO : segInfoDTOList) {
				for (LCCClientAlertInfoTO flexiAlert : flexiAlerts) {
					if (flexiAlert.getFlightSegmantRefNumber().equals(getFlightRefNumber(segInfoDTO.getFlightRefNumber()))) {
						String flexiMessage = I18NUtil.getMessage("PgFlexi_lblFlexiMessage", language);
						String flexiCnxMessage = I18NUtil.getMessage("PgFlexi_lblFlexiCnxMessage", language);
						String flexiModCnxMessage = I18NUtil.getMessage("PgFlexi_lblFlexiModCnxMessage", language);
						List<LCCClientAlertTO> alertsList = flexiAlert.getAlertTO();
						/*
						 * Alert id 1 means modification and number of modifications contain in the alert content Alert
						 * id 2 means cancellation and we dont need the number of cancellations because it is only one
						 * always Alert id 0 means buffer time in hours. We can push this to cancellation content but if
						 * will fail when a system doesnt have cancellation flexibily. Eg 3O
						 */
						for (LCCClientAlertTO alertTO : alertsList) {
							if (alertTO.getAlertId() == 1) {
								flexiMessage = flexiMessage.replace("#1", alertTO.getContent());
							} else if (alertTO.getAlertId() == 2) {
								flexiMessage = flexiMessage.replace("#3", flexiModCnxMessage);
							} else if (alertTO.getAlertId() == 0) {
								flexiMessage = flexiMessage.replace("#2", alertTO.getContent());
								flexiCnxMessage = flexiCnxMessage.replace("#2", alertTO.getContent());
							}
						}
						// All modifications consumed
						if (flexiMessage.contains("#1")) {
							flexiMessage = flexiCnxMessage;
						}
						// Flexi rule has no cancellations
						if (flexiMessage.contains("#3")) {
							flexiMessage = flexiMessage.replace("#3", "");
						}
						FlexiDTO flexiDTO = new FlexiDTO();
						flexiDTO.setOriginDestination(segInfoDTO.getSegmentCode());
						flexiDTO.setFlexiDetails(flexiMessage);
						flexiInfo.add(flexiDTO);
					}
				}
			}
		}

		return flexiInfo;
	}

	public static void applyFlexiCharges(Collection<ReservationPaxTO> paxList, Map<Integer, Boolean> ondFlexiSelection,
			PriceInfoTO priceInfoTO, List<FlightSegmentTO> flightSegmentTOs) {
		if (paxList != null) {
			for (ReservationPaxTO reservationPaxTO : paxList) {
				for (PerPaxPriceInfoTO paxPriceInfoTO : priceInfoTO.getPerPaxPriceInfo()) {
					if ((paxPriceInfoTO.getPassengerType().equals(reservationPaxTO.getPaxType())
							|| (paxPriceInfoTO.getPassengerType().equals(PaxTypeTO.ADULT) && reservationPaxTO.getIsParent()))
							&& !reservationPaxTO.getPaxType().equals(PaxTypeTO.INFANT)) {
						for (Entry<Integer, Boolean> entry : ondFlexiSelection.entrySet()) {
							int ondSequence = entry.getKey();
							boolean ondFlexi = entry.getValue();
							if (ondFlexi) {
								reservationPaxTO.addExternalCharges(com.isa.thinair.webplatform.api.util.ReservationUtil
										.transform(paxPriceInfoTO.getPassengerPrice().getOndExternalCharge(ondSequence)
												.getExternalCharges(), flightSegmentTOs));
							}
						}
						break;
					}
				}
			}
		}
	}

	public static double calculateTotalFlexiCharge(Map<Integer, Boolean> ondFlexiSelection, PriceInfoTO priceInfoTO) {
		BigDecimal totalFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (PerPaxPriceInfoTO paxPriceInfoTO : priceInfoTO.getPerPaxPriceInfo()) {
			for (Entry<Integer, Boolean> entry : ondFlexiSelection.entrySet()) {
				int ondSequence = entry.getKey();
				boolean ondFlexi = entry.getValue();
				if (ondFlexi) {
					List<ExternalChargeTO> externalChargeTOs = paxPriceInfoTO.getPassengerPrice()
							.getOndExternalCharge(ondSequence).getExternalCharges();
					if (externalChargeTOs != null && !externalChargeTOs.isEmpty()) {
						for (ExternalChargeTO externalChargeTO : externalChargeTOs) {
							if (externalChargeTO.getType() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
								totalFlexiCharge = AccelAeroCalculator.add(totalFlexiCharge, externalChargeTO.getAmount());
							}
						}
					}
				}
			}
		}

		return totalFlexiCharge.doubleValue();
	}

	public static boolean setFlexiInformation(boolean cancelReservation, boolean cancelSegment, String fltSegId,
			StringBuffer flexiInfo, String flexiAlerts) throws Exception {
		if (flexiAlerts != null && !flexiAlerts.equals("") && fltSegId != null && !flexiAlerts.equals("[]")) {
			if (cancelReservation) {
				return true;
			}
			String[] selectedSegs = fltSegId.split(":");
			JSONArray jsonFlexiArray = (JSONArray) new JSONParser().parse(flexiAlerts);
			for (Iterator<?> iterator = jsonFlexiArray.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				if (selectedSegs[0].equals(BeanUtils.nullHandler(jsonObject.get("flightSegmantRefNumber")))) {
					if (cancelSegment) {
						return true;
					} else {
						JSONArray jsonFlexiAlertsArray = (JSONArray) new JSONParser()
								.parse(BeanUtils.nullHandler(jsonObject.get("alertTO")));
						for (Iterator<?> alertsIterator = jsonFlexiAlertsArray.iterator(); alertsIterator.hasNext();) {
							JSONObject jsonAlertObject = (JSONObject) alertsIterator.next();
							if (BeanUtils.nullHandler(jsonAlertObject.get("alertId")).equals("1")) {
								// Modification
								int remaining = new Integer(BeanUtils.nullHandler(jsonAlertObject.get("content"))).intValue() - 1;
								if (remaining > 0) {
									flexiInfo.append(remaining + " Modification(s), ");
								}
							} else if (BeanUtils.nullHandler(jsonAlertObject.get("alertId")).equals("2")) {
								flexiInfo.append(BeanUtils.nullHandler(jsonAlertObject.get("content")) + " Cancellation");
							}
						}
					}
					return true;
				}
			}
		}
		return false;
	}

	private static String getFlightRefNumber(String flightRefNumber) {
		flightRefNumber = flightRefNumber.substring(flightRefNumber.indexOf("$") + 1, flightRefNumber.length());
		flightRefNumber = flightRefNumber.substring(flightRefNumber.indexOf("$") + 1, flightRefNumber.length());
		return flightRefNumber.substring(0, flightRefNumber.indexOf("$"));
	}

	public static void validatePassengerFlightDetails(List<FlightSegmentTO> flightSegmentTOs, PriceInfoTO priceInfoTO)
			throws ModuleException {
		boolean isSuccess = true;

		if (flightSegmentTOs != null && priceInfoTO != null) {
			FareSegChargeTO fareSegChargeTO = priceInfoTO.getFareSegChargeTO();

			if (fareSegChargeTO != null) {
				Collection<OndFareSegChargeTO> ondFareSegChargeTOs = fareSegChargeTO.getOndFareSegChargeTOs();
				if (ondFareSegChargeTOs == null) {
					isSuccess = false;
				}

				Collection<Integer> flightSegIdList = getFlightSegmentIdList(flightSegmentTOs);
				for (OndFareSegChargeTO ondFareSegChargeTO : ondFareSegChargeTOs) {
					Set<Integer> keySet = ondFareSegChargeTO.getFsegBCAlloc().keySet();
					for (Integer key : keySet) {
						if (!flightSegIdList.contains(key)) {
							isSuccess = false;
							break;
						}
					}
				}

				if (isSuccess == false) {
					throw new ModuleException("msg.multiple.invalid.flight");
				}
			}
		}

	}

	private static Collection<Integer> getFlightSegmentIdList(List<FlightSegmentTO> flightSegmentTOs) {
		Collection<Integer> flightSegList = new ArrayList<Integer>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			flightSegList.add(new Integer(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentTO.getFlightRefNumber())));
		}

		return flightSegList;
	}

	public static CommonReservationContactInfo transformJsonContactInfo(String resContactInfo) throws Exception {
		CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
		JSONObject jsonPaySumObject = (JSONObject) new JSONParser().parse(resContactInfo);

		contactInfo.setTitle(BeanUtils.nullHandler(jsonPaySumObject.get("title")));
		contactInfo.setFirstName(BeanUtils.nullHandler(jsonPaySumObject.get("firstName")));
		contactInfo.setLastName(BeanUtils.nullHandler(jsonPaySumObject.get("lastName")));

		contactInfo.setCity(BeanUtils.nullHandler(jsonPaySumObject.get("city")));
		contactInfo.setCountryCode(BeanUtils.nullHandler(jsonPaySumObject.get("country")));
		contactInfo.setEmail(BeanUtils.nullHandler(jsonPaySumObject.get("emailAddress")));

		if (jsonPaySumObject.get("customerId") != null) {
			contactInfo.setCustomerId(new Integer(BeanUtils.nullHandler(jsonPaySumObject.get("customerId"))));
		} else {
			contactInfo.setCustomerId(null);
		}

		String faxNo = BeanUtils.nullHandler(jsonPaySumObject.get("fNumber"));

		if (!faxNo.equals("")) {
			contactInfo.setFax(BeanUtils.nullHandler(jsonPaySumObject.get("fCountry")) + "-"
					+ BeanUtils.nullHandler(jsonPaySumObject.get("fArea")) + "-" + faxNo);
		} else {
			contactInfo.setFax("");
		}

		String mobileNo = BeanUtils.nullHandler(jsonPaySumObject.get("mNumber"));

		if (!mobileNo.equals("")) {
			contactInfo.setMobileNo(BeanUtils.nullHandler(jsonPaySumObject.get("mCountry")) + "-"
					+ BeanUtils.nullHandler(jsonPaySumObject.get("mArea")) + "-" + mobileNo);
		} else {
			contactInfo.setMobileNo("");
		}

		String phoneNo = BeanUtils.nullHandler(jsonPaySumObject.get("lNumber"));

		if (!phoneNo.equals("")) {
			contactInfo.setPhoneNo(BeanUtils.nullHandler(jsonPaySumObject.get("lCountry")) + "-"
					+ BeanUtils.nullHandler(jsonPaySumObject.get("lArea")) + "-" + phoneNo);
		} else {
			contactInfo.setPhoneNo("");
		}

		if (!BeanUtils.nullHandler(jsonPaySumObject.get("nationality")).equals("")) {
			contactInfo.setNationalityCode(Integer.parseInt(BeanUtils.nullHandler(jsonPaySumObject.get("nationality"))));
		} else {
			contactInfo.setNationalityCode(null);
		}

		contactInfo.setState(BeanUtils.nullHandler(jsonPaySumObject.get("state")));

		contactInfo.setStreetAddress1(BeanUtils.nullHandler(jsonPaySumObject.get("addresStreet")));
		contactInfo.setStreetAddress2(BeanUtils.nullHandler(jsonPaySumObject.get("addresline")));

		contactInfo.setZipCode(BeanUtils.nullHandler(jsonPaySumObject.get("zipCode")));

		contactInfo.setPreferredLanguage(BeanUtils.nullHandler(jsonPaySumObject.get("preferredLangauge")));

		// Set Emergency Contact
		contactInfo.setEmgnTitle(BeanUtils.nullHandler(jsonPaySumObject.get("emgnTitle")));
		contactInfo.setEmgnFirstName(BeanUtils.nullHandler(jsonPaySumObject.get("emgnFirstName")));
		contactInfo.setEmgnLastName(BeanUtils.nullHandler(jsonPaySumObject.get("emgnLastName")));

		String emgnPhoneNo = BeanUtils.nullHandler(jsonPaySumObject.get("emgnLNumber"));
		if (emgnPhoneNo.equals("")) {
			contactInfo.setEmgnPhoneNo("");
		} else {
			contactInfo.setEmgnPhoneNo(BeanUtils.nullHandler(jsonPaySumObject.get("emgnLCountry")) + "-"
					+ BeanUtils.nullHandler(jsonPaySumObject.get("emgnLArea")) + "-" + emgnPhoneNo);
		}

		contactInfo.setEmgnEmail(BeanUtils.nullHandler(jsonPaySumObject.get("emgnEmail")));

		return contactInfo;
	}

	public static String getInsuranceSelectionText(LCCClientReservation reservation, String language) {
		String insuranceTypeText = "";
		List<LCCClientReservationInsurance> insurances = reservation.getReservationInsurances();
		if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.CCC) && insurances != null
				&& !insurances.isEmpty()) {
			for (LCCClientReservationInsurance insurance : insurances) {
				int insType = insurance.getInsuranceType();
				if (insType == InsuranceTypes.CANCELATION.getCode()) {
					insuranceTypeText = I18NUtil.getMessage("PgInterlineConfirm_lblInsuranceTypeCancel", language);
				} else if (insType == InsuranceTypes.MULTIRISK.getCode()) {
					insuranceTypeText = I18NUtil.getMessage("PgInterlineConfirm_lblInsuranceTypeMultiRisk", language);
				}
				break;
			}
		}
		return insuranceTypeText.toUpperCase();
	}

	/**
	 * Get Payment gateway ID list
	 * 
	 * @param paymentGateways
	 * @return
	 */
	public static Collection<Integer> getPaymentGatewayIDs(Collection<IPGPaymentOptionDTO> paymentGateways) {
		Set<Integer> gatewayIDs = new HashSet<Integer>();
		if (paymentGateways != null) {
			for (IPGPaymentOptionDTO iPGPaymentOptionDTO : paymentGateways) {
				gatewayIDs.add(iPGPaymentOptionDTO.getPaymentGateway());
			}
		}
		return gatewayIDs;
	}

	public static Map<Integer, String> getPaymentModificationAllowMap(Collection<IPGPaymentOptionDTO> paymentGateways) {
		Map<Integer, String> gatewayModificationAllowMap = new HashMap<Integer, String>();
		if (paymentGateways != null) {
			for (IPGPaymentOptionDTO iPGPaymentOptionDTO : paymentGateways) {
				gatewayModificationAllowMap.put(iPGPaymentOptionDTO.getPaymentGateway(),
						iPGPaymentOptionDTO.getIsModificationAllow());
			}
		}
		return gatewayModificationAllowMap;
	}

	/**
	 * TODO: Duplication of code:Should be put into a common place This method is used only for get the operating
	 * carrier of dry reservation. So, getting first segment carrier code is enough for that. This should not use for
	 * interline bookings.
	 * 
	 * @param colResSegs
	 * @return
	 */
	public static String getDryOperatingCarrier(Collection<LCCClientReservationSegment> colResSegs) {
		String carrier = null;
		for (LCCClientReservationSegment resSeg : colResSegs) {
			carrier = AppSysParamsUtil.extractCarrierCode(resSeg.getFlightNo());
			return carrier;
		}
		return carrier;
	}

	public static List<ReservationPax> getReservationPaxList(Collection<ReservationPaxTO> reservationPaxTOs) {
		List<ReservationPax> reservationPaxs = new ArrayList<ReservationPax>();
		for (ReservationPaxTO reservationPaxTO : reservationPaxTOs) {
			ReservationPax reservationPax = new ReservationPax();
			reservationPax.setFirstName(reservationPaxTO.getFirstName());
			reservationPax.setLastName(reservationPaxTO.getLastName());
			reservationPax.setTitle(reservationPaxTO.getTitle());
			reservationPax.setPaxSequence(reservationPaxTO.getSeqNumber());
			reservationPaxs.add(reservationPax);
		}

		return reservationPaxs;
	}

	/*
	 * transform json string with pax details to LCCClientReservationPax
	 */
	public static Collection<LCCClientReservationPax> transformJsonPassengers(String resPaxss) throws Exception {
		Collection<LCCClientReservationPax> colpaxs = new ArrayList<LCCClientReservationPax>();
		JSONArray jsonPaxArray = (JSONArray) new JSONParser().parse(resPaxss);

		Iterator<?> itIter = jsonPaxArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			if (jPPObj != null) {
				colpaxs.add(transformPax(jPPObj));
			}
		}
		return colpaxs;
	}

	public static Collection<PaxTO> transformNameChangePax(String resPaxss) throws ParseException {
		Collection<PaxTO> colpaxs = new ArrayList<PaxTO>();
		JSONArray jsonPaxArray = (JSONArray) new JSONParser().parse(resPaxss);

		Iterator<?> itIter = jsonPaxArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			if (jPPObj != null) {
				colpaxs.add(transformNameChangePax(jPPObj));
			}
		}
		return colpaxs;
	}

	public static Map<Integer, NameDTO> transformToPaxMap(Collection<PaxTO> paxTOs) {

		Map<Integer, NameDTO> paxMap = new HashMap<Integer, NameDTO>();

		for (PaxTO paxTO : paxTOs) {
			NameDTO nDTO = new NameDTO();
			nDTO.setFirstname(paxTO.getItnPaxFirstName());
			nDTO.setLastName(paxTO.getItnPaxLastName());
			nDTO.setTitle(paxTO.getItnPaxTitle());
			nDTO.setPnrPaxId(PaxTypeUtils.getPnrPaxId(paxTO.getTravelerRefNumber()));
			nDTO.setPaxReference(paxTO.getTravelerRefNumber());
			nDTO.setFFID(paxTO.getItnFFID());

			paxMap.put(nDTO.getPnrPaxId(), nDTO);
		}

		return paxMap;
	}

	public static List<Collection<PassengerDTO>> transformJsonPassengerList(String resPaxss) throws Exception {
		List<Collection<PassengerDTO>> paxList = new ArrayList<Collection<PassengerDTO>>();
		Collection<PassengerDTO> colAdults = new ArrayList<PassengerDTO>();
		Collection<PassengerDTO> colChild = new ArrayList<PassengerDTO>();
		Collection<PassengerDTO> colInfant = new ArrayList<PassengerDTO>();

		JSONArray jsonPaxArray = (JSONArray) new JSONParser().parse(resPaxss);

		Iterator<?> itIter = jsonPaxArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			if (jPPObj != null) {
				if (("CH").equals(jPPObj.get("paxType"))) {
					colChild.add(transformPassenger(jPPObj));
				} else if (("IN").equals(jPPObj.get("paxType"))) {
					colInfant.add(transformInfantPassenger(jPPObj));
				} else {
					colAdults.add(transformPassenger(jPPObj));
				}
			}
		}
		paxList.add(0, colChild);
		paxList.add(1, colAdults);
		paxList.add(2, colInfant);
		return paxList;
	}

	public static List<Collection<PassengerDTO>> transformJsonPassengerAnciModifyList(String resPaxss) throws Exception {
		List<Collection<PassengerDTO>> paxList = new ArrayList<Collection<PassengerDTO>>();
		Collection<PassengerDTO> colAdults = new ArrayList<PassengerDTO>();
		Collection<PassengerDTO> colChild = new ArrayList<PassengerDTO>();
		Collection<PassengerDTO> colInfant = new ArrayList<PassengerDTO>();

		JSONObject jsonPaxArray = (JSONObject) new JSONParser().parse(resPaxss);

		JSONArray paxAdult = (JSONArray) jsonPaxArray.get("adults");
		Iterator<?> itIterAdlt = paxAdult.iterator();
		while (itIterAdlt.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIterAdlt.next();
			colAdults.add(transformPassenger(jPPObj));
		}

		JSONArray paxChld = (JSONArray) jsonPaxArray.get("children");
		Iterator<?> itIterChld = paxAdult.iterator();
		while (itIterAdlt.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIterChld.next();
			colChild.add(transformPassenger(jPPObj));
		}
		paxList.add(0, colChild);
		paxList.add(1, colAdults);
		paxList.add(2, colInfant);
		return paxList;
	}

	private static PassengerDTO transformPassenger(JSONObject jsonObj) {
		PassengerDTO pax = new PassengerDTO();
		pax.setTitle(BeanUtils.nullHandler(jsonObj.get("title")).trim());
		pax.setFirstName(BeanUtils.nullHandler(jsonObj.get("firstName")).trim());
		pax.setLastName(BeanUtils.nullHandler(jsonObj.get("lastName")).trim());
		pax.setDateOfBirth(BeanUtils.nullHandler(jsonObj.get("dateOfBirth")));
		pax.setParent((("PA").equals(BeanUtils.nullHandler(jsonObj.get("paxType")))) ? true : false);
		return pax;
	}

	private static PassengerDTO transformInfantPassenger(JSONObject jsonObj) {
		PassengerDTO pax = new PassengerDTO();
		pax.setTitle("");
		pax.setFirstName(BeanUtils.nullHandler(jsonObj.get("firstName")).trim());
		pax.setLastName(BeanUtils.nullHandler(jsonObj.get("lastName")).trim());
		pax.setDateOfBirth(BeanUtils.nullHandler(jsonObj.get("dateOfBirth")));
		pax.setParent(false);
		return pax;
	}

	private static LCCClientReservationPax transformPax(JSONObject jPPObj) throws Exception {
		LCCClientReservationPax pax = new LCCClientReservationPax();

		List<LCCSelectedSegmentAncillaryDTO> anciSegList = new ArrayList<LCCSelectedSegmentAncillaryDTO>();

		if (jPPObj.get("currentAncillaries") != null) {
			JSONArray jsonAnciArray = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("currentAncillaries")));
			transformAnciList(anciSegList, jsonAnciArray);
		}

		pax.setFirstName(BeanUtils.nullHandler(jPPObj.get("firstName")));
		pax.setLastName(BeanUtils.nullHandler(jPPObj.get("lastName")));
		pax.setPaxSequence(new Integer(BeanUtils.nullHandler((jPPObj.get("seqNumber")))));
		pax.setPaxType(BeanUtils.nullHandler(jPPObj.get("paxType")));
		pax.setTitle(BeanUtils.nullHandler(jPPObj.get("title")));
		pax.setSelectedAncillaries(anciSegList);

		pax.setTravelerRefNumber(BeanUtils.nullHandler(jPPObj.get("travelerRefNumber")));

		return pax;
	}

	private static PaxTO transformNameChangePax(JSONObject jPPObj) {
		PaxTO pax = new PaxTO();

		pax.setItnPaxFirstName(BeanUtils.nullHandler(jPPObj.get("itnPaxFirstName")));
		pax.setItnPaxLastName(BeanUtils.nullHandler(jPPObj.get("itnPaxLastName")));
		pax.setItnFFID(BeanUtils.nullHandler(jPPObj.get("itnFFID")));
		pax.setItnPaxTitle(BeanUtils.nullHandler((jPPObj.get("itnPaxTitle"))));
		pax.setActualSeqNo(BeanUtils.nullHandler((jPPObj.get("actualSeqNo"))));
		pax.setTravelerRefNumber(BeanUtils.nullHandler((jPPObj.get("travelerRefNumber"))));

		return pax;
	}

	private static void transformAnciList(List<LCCSelectedSegmentAncillaryDTO> anciSegList, JSONArray anciArray) {
		Iterator<?> itIter = anciArray.iterator();
		while (itIter.hasNext()) {
			LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
			JSONObject innerObj = (JSONObject) itIter.next();
			JSONObject fltObj = (JSONObject) innerObj.get("flightSegmentTO");

			if (fltObj != null) {
				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
				flightSegmentTO.setFlightRefNumber(BeanUtils.nullHandler(fltObj.get("flightRefNumber")));
				flightSegmentTO.setSegmentCode(BeanUtils.nullHandler(fltObj.get("segmentCode")));
				flightSegmentTO.setFlightNumber(BeanUtils.nullHandler(fltObj.get("flightNumber")));
				flightSegmentTO.setOperatingAirline(BeanUtils.nullHandler(fltObj.get("operatingAirline")));
				flightSegmentTO.setFlownSegment(new Boolean(BeanUtils.nullHandler(fltObj.get("flownSegment"))));
				lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(flightSegmentTO);
			}

			JSONObject seatObj = (JSONObject) innerObj.get("airSeatDTO");
			if (seatObj != null && !"".equals(BeanUtils.nullHandler(seatObj.get("seatCharge")))) {
				LCCAirSeatDTO seatDto = new LCCAirSeatDTO();
				seatDto.setSeatCharge(new BigDecimal(seatObj.get("seatCharge").toString()));
				seatDto.setSeatNumber(BeanUtils.nullHandler(seatObj.get("seatNumber")));
				lccSelectedSegmentAncillaryDTO.setAirSeatDTO(seatDto);
			}

			JSONArray mealArr = (JSONArray) innerObj.get("mealDTOs");
			if (mealArr != null && !mealArr.isEmpty()) {
				List<LCCMealDTO> mealDTOs = new ArrayList<LCCMealDTO>();

				Iterator<?> itMeal = mealArr.iterator();
				while (itMeal.hasNext()) {
					JSONObject mealObj = (JSONObject) itMeal.next();
					if (mealObj != null) {
						LCCMealDTO mealDTO = new LCCMealDTO();
						mealDTO.setMealCode(BeanUtils.nullHandler(mealObj.get("mealCode")));
						mealDTO.setSoldMeals(Integer.parseInt(mealObj.get("soldMeals").toString()));
						mealDTO.setMealCharge(new BigDecimal(mealObj.get("mealCharge").toString()));
						mealDTO.setTotalPrice(mealObj.get("totalPrice").toString());
						mealDTOs.add(mealDTO);
					}

				}

				lccSelectedSegmentAncillaryDTO.setMealDTOs(mealDTOs);
			}

			JSONArray bgArr = (JSONArray) innerObj.get("baggageDTOs");
			if (bgArr != null && !bgArr.isEmpty()) {
				List<LCCBaggageDTO> baggageList = new ArrayList<LCCBaggageDTO>();
				Iterator<?> itBg = bgArr.iterator();
				while (itBg.hasNext()) {
					JSONObject bgObj = (JSONObject) itBg.next();
					if (bgObj != null) {
						LCCBaggageDTO baggageDTO = new LCCBaggageDTO();
						baggageDTO.setBaggageCharge(new BigDecimal(bgObj.get("baggageCharge").toString()));
						baggageDTO.setSoldPieces(Integer.parseInt(bgObj.get("soldPieces").toString()));
						baggageList.add(baggageDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setBaggageDTOs(baggageList);
			}

			JSONObject insObj = (JSONObject) innerObj.get("insuranceQuotation");
			if (insObj != null && !"".equals(BeanUtils.nullHandler(insObj.get("totalPerPaxPremiumAmount")))) {
				LCCInsuranceQuotationDTO ins = new LCCInsuranceQuotationDTO();
				ins.setTotalPerPaxPremiumAmount(new BigDecimal(insObj.get("totalPerPaxPremiumAmount").toString()));

				List<LCCInsuranceQuotationDTO> insurances = new ArrayList<LCCInsuranceQuotationDTO>();
				insurances.add(ins);
				lccSelectedSegmentAncillaryDTO.setInsuranceQuotations(insurances);
			}

			JSONArray apServiceArr = (JSONArray) innerObj.get("airportServiceDTOs");

			if (apServiceArr != null && !apServiceArr.isEmpty()) {
				List<LCCAirportServiceDTO> airportServiceDTOs = new ArrayList<LCCAirportServiceDTO>();

				Iterator<?> itAps = apServiceArr.iterator();
				while (itAps.hasNext()) {
					JSONObject apsObj = (JSONObject) itAps.next();
					if (apsObj != null) {
						LCCAirportServiceDTO lccAirportServiceDTO = new LCCAirportServiceDTO();
						lccAirportServiceDTO.setSsrCode(BeanUtils.nullHandler(apsObj.get("ssrCode")));
						lccAirportServiceDTO.setSsrDescription(BeanUtils.nullHandler(apsObj.get("ssrDescription")));
						lccAirportServiceDTO.setAirportCode(BeanUtils.nullHandler(apsObj.get("airportCode")));
						lccAirportServiceDTO.setServiceCharge(new BigDecimal(BeanUtils.nullHandler(apsObj.get("serviceCharge"))));
						airportServiceDTOs.add(lccAirportServiceDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setAirportServiceDTOs(airportServiceDTOs);
			}

			JSONArray aptArr = (JSONArray) innerObj.get("airportTransferDTOs");
			if (aptArr != null && !aptArr.isEmpty()) {
				List<LCCAirportServiceDTO> airportTransferDTOs = new ArrayList<LCCAirportServiceDTO>();

				Iterator<?> itApt = aptArr.iterator();
				while (itApt.hasNext()) {
					JSONObject aptObj = (JSONObject) itApt.next();
					if (aptObj != null) {
						LCCAirportServiceDTO lccAirportTransferDTO = new LCCAirportServiceDTO();
						lccAirportTransferDTO.setSsrCode(BeanUtils.nullHandler(aptObj.get("ssrCode")));
						lccAirportTransferDTO.setSsrDescription(BeanUtils.nullHandler(aptObj.get("ssrDescription")));
						lccAirportTransferDTO.setAirportCode(BeanUtils.nullHandler(aptObj.get("airportCode")));
						lccAirportTransferDTO
								.setServiceCharge(new BigDecimal(BeanUtils.nullHandler(aptObj.get("serviceCharge"))));
						lccAirportTransferDTO
								.setAirportTransferId(new Integer(BeanUtils.nullHandler(aptObj.get("airportTransferId"))));
						lccAirportTransferDTO.setTransferContact(BeanUtils.nullHandler(aptObj.get("transferContact")));
						lccAirportTransferDTO.setTransferAddress(BeanUtils.nullHandler(aptObj.get("transferAddress")));
						lccAirportTransferDTO.setTransferType(BeanUtils.nullHandler(aptObj.get("transferType")));
						lccAirportTransferDTO.setTranferDate(BeanUtils.nullHandler(aptObj.get("tranferDate")));
						airportTransferDTOs.add(lccAirportTransferDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setAirportServiceDTOs(airportTransferDTOs);
			}

			JSONArray ssrArr = (JSONArray) innerObj.get("specialServiceRequestDTOs");

			if (ssrArr != null && !ssrArr.isEmpty()) {
				List<LCCSpecialServiceRequestDTO> ssrDTOs = new ArrayList<LCCSpecialServiceRequestDTO>();

				Iterator<?> itAps = ssrArr.iterator();
				while (itAps.hasNext()) {
					JSONObject ssrObj = (JSONObject) itAps.next();
					if (ssrObj != null) {
						LCCSpecialServiceRequestDTO lccSSRDTO = new LCCSpecialServiceRequestDTO();
						lccSSRDTO.setSsrCode(BeanUtils.nullHandler(ssrObj.get("ssrCode")));
						lccSSRDTO.setCharge(new BigDecimal(ssrObj.get("charge").toString()));
						lccSSRDTO.setServiceQuantity(BeanUtils.nullHandler(ssrObj.get("serviceQuantity")).toString());
						ssrDTOs.add(lccSSRDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setSpecialServiceRequestDTOs(ssrDTOs);
			}

			anciSegList.add(lccSelectedSegmentAncillaryDTO);
		}
	}

	/**
	 * @param reservationPaxTOs
	 * @return
	 */
	public static Collection<LCCClientReservationPax>
			getLccReservationPassengers(Collection<ReservationPaxTO> reservationPaxTOs) {
		Collection<LCCClientReservationPax> lccReservationPaxs = new ArrayList<LCCClientReservationPax>();
		for (ReservationPaxTO reservationPaxTO : reservationPaxTOs) {
			LCCClientReservationPax reservationPax = new LCCClientReservationPax();
			reservationPax.setFirstName(reservationPaxTO.getFirstName());
			reservationPax.setLastName(reservationPaxTO.getLastName());
			reservationPax.setTitle(reservationPaxTO.getTitle());
			reservationPax.setPaxSequence(reservationPaxTO.getSeqNumber());
			reservationPax.setPaxType(reservationPaxTO.getPaxType());
			lccReservationPaxs.add(reservationPax);
		}
		return lccReservationPaxs;
	}

	private static void validateReservationData(IBEReservationPostPaymentDTO postPayDTO, IBEReservationInfoDTO resInfo)
			throws ModuleException {

		// Validate promotion discount
		BigDecimal postPayPromoDiscountTotal = AccelAeroCalculator.scaleValueDefault(postPayDTO.getPromoDiscountTotal());
		BigDecimal prepayPromoDiscountTotal = resInfo.getDiscountAmount(true);

		if (!postPayPromoDiscountTotal.equals(prepayPromoDiscountTotal)) {
			throw new ModuleException("msg.multiple.invalid.flight");
		}

		// Validate others

	}

	public static LCCClientReservation makeReservation(String pnr, IBEReservationPostPaymentDTO postPayDTO,
			IBEReservationInfoDTO resInfo, ContactInfoDTO contactInfo, boolean onHold,
			Map<Integer, CommonCreditCardPaymentInfo> tempPayMap, IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String loyaltyAccount, String loyaltyAgent,
			Date pnrReleaseTimeZulu, String brokerType, TrackInfoDTO trackInfo, ReservationDiscountDTO resDiscountDTO,
			Date pgwWiseOnholdDateTime) throws Exception {

		validateReservationData(postPayDTO, resInfo);
		// PriceInfoTO priceInfoTO = resInfo.getPriceInfoTO();
		PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
		FlightPriceRQ flightPriceRQ = postPayDTO.getFlightPriceRQ();
		CommonCreditCardPaymentInfo cardPaymentInfo = BookingUtil.getCommonCreditCardPaymentInfo(tempPayMap,
				creditInfo == null ? false : creditInfo.isSuccess());

		SSRServicesUtil.setSegmentSeqForSSR((List<ReservationPaxTO>) postPayDTO.getPaxList(),
				postPayDTO.getSelectedFlightSegments(), null);

		boolean isPromotionResWithZeroPayment = false;
		if (postPayDTO.isNoPay() && resInfo.getDiscountInfo() != null && resInfo.getDiscountInfo().getPromotionId() != null) {
			isPromotionResWithZeroPayment = true;
		}

		Map<Integer, BigDecimal> paxWiseDiscount = getPaxWisePromoDiscount(resInfo, postPayDTO.getPaxList());

		CommonReservationAssembler commonReservationAssembler = BookingUtil.createBookingDetails(creditInfo, ipgDTO,
				payCurrencyDTO, postPayDTO.getPaxList(), priceInfoTO, onHold, paymentTimestamp,
				postPayDTO.getSelectedExternalCharges(), postPayDTO.isAddCCExternalCharge(), postPayDTO.getLoyaltyCredit(),
				loyaltyAccount, loyaltyAgent, postPayDTO.getCarrierWiseLoyaltyPaymentInfo(), cardPaymentInfo, brokerType,
				isPromotionResWithZeroPayment, resDiscountDTO, postPayDTO.getPayByVoucherInfo());

		commonReservationAssembler.setChargesApplicability(ChargeRateOperationType.MAKE_ONLY);

		commonReservationAssembler.addContactInfo("", BookingUtil.getContactDetails(contactInfo), null);
		// BookingUtil.addContactDetails(commonReservationAssembler,
		// contactInfo);
		ExternalChargeUtil.injectOndBaggageGroupId(postPayDTO.getPaxList(), postPayDTO.getSelectedFlightSegments());
		BookingUtil.addSegments(commonReservationAssembler, postPayDTO.getSelectedFlightSegments());
		BookingUtil.addPreferenceDetails(commonReservationAssembler, contactInfo);
		BookingUtil.setSubStationData(commonReservationAssembler, resInfo);

		commonReservationAssembler.setAppIndicator(AppIndicatorEnum.APP_IBE);
		commonReservationAssembler.getLccreservation().setPNR(pnr);
		commonReservationAssembler.setSelectedFare(priceInfoTO.getFareTypeTO());
		commonReservationAssembler.setSelectedFareSegChargeTO(priceInfoTO.getFareSegChargeTO());
		commonReservationAssembler.setFlexiQuote(postPayDTO.isFlexiQuote());
		commonReservationAssembler.setFlightPriceRQ(flightPriceRQ);
		commonReservationAssembler.setLccTransactionIdentifier(postPayDTO.getTransactionId());
		commonReservationAssembler.setTemporaryPaymentMap(tempPayMap);
		commonReservationAssembler.setTargetSystem(resInfo.getSelectedSystem());
		commonReservationAssembler.setPnrZuluReleaseTimeStamp(pnrReleaseTimeZulu);
		commonReservationAssembler.setSelectedCurrency(postPayDTO.getSelectedCurrency());
		commonReservationAssembler.setDiscountedFareDetails(resInfo.getDiscountInfo());
		commonReservationAssembler.setDiscountAmount(postPayDTO.getPromoDiscountTotal());
		commonReservationAssembler.setAnciOfferTemplateIDs(resInfo.getAnciOfferTemplates());
		commonReservationAssembler.setServiceTaxRatio(resInfo.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));
		commonReservationAssembler.setCarrierWiseLoyaltyPaymentMap(postPayDTO.getCarrierWiseLoyaltyPaymentInfo());
		commonReservationAssembler.setPgwWiswZuluReleaseTimeStamp(pgwWiseOnholdDateTime);
		commonReservationAssembler.setPayByVoucherInfo(postPayDTO.getPayByVoucherInfo());

		AncillaryDTOUtil.populateResPaxWithSelAnciDtls(postPayDTO.getPaxList(), commonReservationAssembler);

		AncillaryDTOUtil.populateInsurance(postPayDTO.getInsuranceQuotes(), commonReservationAssembler);
		ServiceResponce responce = ModuleServiceLocator.getAirproxyReservationBD().makeReservation(commonReservationAssembler,
				trackInfo);
		LCCClientReservation commonReservation = (LCCClientReservation) responce.getResponseParam(CommandParamNames.RESERVATION);

		// Update release timestamp of the dummy booking if not own airline involved reservation
		if (onHold && (commonReservation.getZuluReleaseTimeStamp()
				.compareTo(commonReservationAssembler.getPnrZuluReleaseTimeStamp()) != 0)) {
			ModuleServiceLocator.getAirproxyReservationBD().updateReleaseTimeForDummyReservation(commonReservation.getPNR(),
					commonReservation.getZuluReleaseTimeStamp());
		}

		return commonReservation;
	}

	public static boolean isReservationCabinClassOnholdable(BaseAvailRS flightAvailRS) throws ModuleException {
		boolean allowOnhold = false;
		PriceInfoTO priceInfoTO = flightAvailRS.getSelectedPriceFlightInfo();
		if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
			allowOnhold = !priceInfoTO.getFareTypeTO().isOnHoldRestricted();
		}
		return allowOnhold;
	}

	/**
	 * Return if the resrvation is onholdable based on ResOnholdValidationDTO's OTHER_VALIDATION. Will return if there
	 * is no selected flight in flightAvailRS
	 * 
	 * @param flightAvailRS
	 * @param groupPNR
	 * @param trackInfo
	 * @param ipAddress
	 * @param reservationInfo
	 * @return
	 * @throws ModuleException
	 */

	public static boolean isReservationOtherValidationOnholdable(BaseAvailRS flightAvailRS, SYSTEM system,
			TrackInfoDTO trackInfo, String ipAddress) throws ModuleException {
		List<FlightSegmentTO> selectedFlightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (OriginDestinationInformationTO ondInformationTO : flightAvailRS.getOriginDestinationInformationList()) {
			for (OriginDestinationOptionTO ondOptionTO : ondInformationTO.getOrignDestinationOptions()) {
				if (ondOptionTO.isSelected()) {
					selectedFlightSegmentTOs.addAll(ondOptionTO.flightSegmentList);
				}
			}
		}
		if (selectedFlightSegmentTOs.size() > 0) {
			return ModuleServiceLocator.getAirproxySearchAndQuoteBD().isOnholdEnabled(selectedFlightSegmentTOs,
					new ArrayList<ReservationPax>(), null, ResOnholdValidationDTO.OTHER_VALIDATION,
					trackInfo.getOriginChannelId(), system, ipAddress, trackInfo);
		} else {
			return false;
		}

	}

	public static void addTicketPriceOnHoldBeforePaymentSessionData(HttpServletRequest request, BigDecimal totalTicketPrice) {
		PutOnHoldBeforePaymentDTO beforePaymentDTO = SessionUtil.getPutOnHoldBeforePaymentDTO(request);
		beforePaymentDTO.setTotalTicketPrice(totalTicketPrice);
	}

	public static String getServerErrorMessage(HttpServletRequest request, String key) {
		return I18NUtil.getMessage(key, SessionUtil.getLanguage(request));
	}

	/**
	 * Gets the discount detail set, segment fare quote if discounts are applicable.
	 * 
	 * @param selectedQuote
	 *            Selected fare quote with return fare price amounts.
	 * @param segmentFareQuote
	 *            Segment fare quote with segment fare price details.
	 * @param isCreditPromotion
	 *            whether to add promotion discount to ibe fare discount total
	 * @return selectedQuote if no discounts are applicable or segmentFareQuote if discounts are applicable.
	 */
	// public static FareQuoteTO getDiscountSetFareQuote(FareQuoteTO selectedQuote, FareQuoteTO segmentFareQuote,
	// boolean isCreditPromotion) {
	// boolean returnTotalLessThanSegTotal = false;
	//
	// BigDecimal selectedTotalBase = new BigDecimal(selectedQuote.getTotalPrice());
	// BigDecimal segFareTotalBase = new BigDecimal(segmentFareQuote.getTotalPrice());
	// BigDecimal promoDiscountAmount = new BigDecimal(selectedQuote.getTotalPromoDiscount());
	//
	// // If segment fare total is bigger than selected fare total we can say there is a discount.
	// if (segFareTotalBase.compareTo(selectedTotalBase) == 1) {
	// segmentFareQuote.setInBaseCurr(selectedQuote.getInBaseCurr());
	//
	// BigDecimal totDiscAmount = AccelAeroCalculator.add(segFareTotalBase, selectedTotalBase.negate());
	// totDiscAmount = isCreditPromotion ? AccelAeroCalculator.add(totDiscAmount, promoDiscountAmount) : totDiscAmount;
	//
	// segmentFareQuote.setTotalIbeFareDiscount(totDiscAmount.toPlainString());
	// segmentFareQuote.setTotalPrice(selectedTotalBase.toString());
	// segmentFareQuote.setTotalSegmentFare(segFareTotalBase.toString());
	//
	// returnTotalLessThanSegTotal = true;
	// }
	//
	// BigDecimal selectedTotalSelCur = new BigDecimal(selectedQuote.getSelectedtotalPrice());
	// BigDecimal segFareTotalSelCur = new BigDecimal(segmentFareQuote.getSelectedtotalPrice());
	// BigDecimal promoDiscountAmountInSelCurr = new BigDecimal(selectedQuote.getTotalSelectedPromoDiscount());
	//
	// if (segFareTotalSelCur.compareTo(selectedTotalSelCur) == 1) {
	// segmentFareQuote.setInSelectedCurr(selectedQuote.getInSelectedCurr());
	//
	// BigDecimal totDiscInSelCurr = AccelAeroCalculator.add(segFareTotalSelCur, selectedTotalSelCur.negate());
	// totDiscInSelCurr = isCreditPromotion
	// ? AccelAeroCalculator.add(totDiscInSelCurr, promoDiscountAmountInSelCurr)
	// : totDiscInSelCurr;
	//
	// segmentFareQuote.setTotalSelectedIbeFareDiscount(totDiscInSelCurr.toPlainString());
	// segmentFareQuote.setSelectedtotalPrice(selectedTotalSelCur.toString());
	// segmentFareQuote.setTotalSelectedCurSegmentFare(segFareTotalSelCur.toString());
	//
	// returnTotalLessThanSegTotal = true;
	// }
	//
	// FareQuoteTO retFareQuoteTO = returnTotalLessThanSegTotal ? segmentFareQuote : selectedQuote;
	//
	// // override total promotion discount amounts if credit promotion
	// if (isCreditPromotion) {
	// retFareQuoteTO.setTotalPromoDiscount("0.00");
	// retFareQuoteTO.setTotalSelectedPromoDiscount("0.00");
	// }
	//
	// return retFareQuoteTO;
	// }

	/**
	 * Gets the discount detail set, segment fare quote if discounts are applicable.
	 * 
	 * @param selectedQuote
	 *            Selected fare quote with return fare price amounts.
	 * @param segmentFareQuote
	 *            Segment fare quote with segment fare price details.
	 * @return selectedQuote if no discounts are applicable or segmentFareQuote if discounts are applicable.
	 */
	public static FareQuoteTO getDiscountSetFareQuote(FareQuoteTO selectedQuote, FareQuoteTO segmentFareQuote) {
		boolean returnTotalLessThanSegTotal = false;

		BigDecimal selectedTotalBase = new BigDecimal(selectedQuote.getTotalPrice());
		BigDecimal segFareTotalBase = new BigDecimal(segmentFareQuote.getTotalPrice());
		BigDecimal promoDiscount = new BigDecimal(selectedQuote.getTotalIbePromoDiscount());

		// If segment fare total is bigger than selected fare total we can say there is a discount.
		if (segFareTotalBase.compareTo(selectedTotalBase) > 0) {
			segmentFareQuote.setInBaseCurr(selectedQuote.getInBaseCurr());

			BigDecimal totalIbeFareDiscount = AccelAeroCalculator.add(segFareTotalBase, selectedTotalBase.negate());
			if (selectedQuote.isHasPromoDiscount() && selectedQuote.isDeductFromTotal()) {
				totalIbeFareDiscount = AccelAeroCalculator.add(totalIbeFareDiscount, promoDiscount.negate());
			}

			segmentFareQuote.setTotalIbeFareDiscount(totalIbeFareDiscount.toString());

			segmentFareQuote.setTotalPrice(selectedTotalBase.toString());
			segmentFareQuote.setTotalSegmentFare(segFareTotalBase.toString());

			segmentFareQuote.setHasPromoDiscount(selectedQuote.isHasPromoDiscount());
			segmentFareQuote.setDeductFromTotal(selectedQuote.isDeductFromTotal());

			returnTotalLessThanSegTotal = true;
		}

		BigDecimal selectedTotalSelCur = new BigDecimal(selectedQuote.getSelectedtotalPrice());
		BigDecimal segFareTotalSelCur = new BigDecimal(segmentFareQuote.getSelectedtotalPrice());
		BigDecimal promoDiscountSelCur = new BigDecimal(selectedQuote.getTotalSelectedIbePromoDiscount());

		if (segFareTotalSelCur.compareTo(selectedTotalSelCur) > 0) {
			segmentFareQuote.setInSelectedCurr(selectedQuote.getInSelectedCurr());

			BigDecimal totalIbeFareDiscountSelCur = AccelAeroCalculator.add(segFareTotalSelCur, selectedTotalSelCur.negate());
			if (selectedQuote.isHasPromoDiscount() && selectedQuote.isDeductFromTotal()) {
				totalIbeFareDiscountSelCur = AccelAeroCalculator.add(totalIbeFareDiscountSelCur, promoDiscountSelCur.negate());
			}

			segmentFareQuote.setTotalSelectedIbeFareDiscount(totalIbeFareDiscountSelCur.toString());

			segmentFareQuote.setSelectedtotalPrice(selectedTotalSelCur.toString());
			segmentFareQuote.setTotalSelectedCurSegmentFare(segFareTotalSelCur.toString());

			segmentFareQuote.setHasPromoDiscount(selectedQuote.isHasPromoDiscount());
			segmentFareQuote.setDeductFromTotal(selectedQuote.isDeductFromTotal());

			returnTotalLessThanSegTotal = true;
		}

		return returnTotalLessThanSegTotal ? segmentFareQuote : selectedQuote;
	}

	public static ReservationDTO getReservationDTO(String pnr) throws ModuleException {
		ReservationDTO reservation = null;
		if (pnr != null) {
			ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
			reservationSearchDTO.setPnr(pnr.trim());
			Collection<ReservationDTO> collection = ModuleServiceLocator.getReservationQueryBD()
					.getReservations(reservationSearchDTO);
			if (!collection.isEmpty()) {
				for (ReservationDTO reservationDTO : collection) {
					reservation = reservationDTO;
					break;
				}
			}
		}
		return reservation;
	}

	public static void addTravelData(IPGRequestDTO ipgRequestDTO, List<FlightSegmentTO> flightSegmentTOs) {

		if (flightSegmentTOs != null && ipgRequestDTO != null) {
			List<TravelSegmentDTO> segmentDTOs = new ArrayList<TravelSegmentDTO>();
			TravelSegmentDTO segmentDTO = null;
			Map mapAirports = MapGenerator.getAirportMap();
			String depSegmentCode = null;
			String arrivalSegmentCode = null;
			Date SysteDate = Calendar.getInstance().getTime();
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (SysteDate.compareTo(flightSegmentTO.getDepartureDateTimeZulu()) < 0) {
					segmentDTO = new TravelSegmentDTO();
					segmentDTO.setFlightNumber(Util.extractFlightNumber(flightSegmentTO.getFlightNumber()));
					segmentDTO.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightSegmentTO.getFlightNumber()));
					segmentDTO.setFlightDate(flightSegmentTO.getDepartureDateTime());
					depSegmentCode = Util.getDepartureAirportCode(flightSegmentTO.getSegmentCode());
					segmentDTO.setDepartureAirportCode(depSegmentCode);
					segmentDTO.setDepartureAirportName(String.valueOf(mapAirports.get(depSegmentCode)));
					arrivalSegmentCode = Util.getAirvalAirportCode(flightSegmentTO.getSegmentCode());
					segmentDTO.setArrivalAirportCode(arrivalSegmentCode);
					segmentDTO.setArrivalAirportName(String.valueOf(mapAirports.get(arrivalSegmentCode)));
					segmentDTO.setNoOfStopvers(flightSegmentTO.getSegmentCode().split("/").length - 2);
					segmentDTO.setClassOfService(flightSegmentTO.getCabinClassCode());
					segmentDTOs.add(segmentDTO);
				}
			}
			TravelDTO travelDTO = new TravelDTO();
			travelDTO.setTravelSegments(segmentDTOs);
			ipgRequestDTO.setTravelDTO(travelDTO);
		}
	}

	public static List<LCCClientAlertInfoTO> getSortedAlertInfo(List<LCCClientAlertInfoTO> lCCClientAlertInfoTO) {
		for (LCCClientAlertInfoTO lCCClientAlertInfo : lCCClientAlertInfoTO) {
			lCCClientAlertInfo.setAlertTO(SortUtil.sortAlertInfo(lCCClientAlertInfo.getAlertTO()));
		}
		return lCCClientAlertInfoTO;
	}

	public static void applyAncillaryTax(Collection<ReservationPaxTO> paxList, FlightSegmentTO flightSegmentTO,
			IBEReservationInfoDTO ibeReservationInfoDTO,
			Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModifyingSegAnciTotal) {
		if (ibeReservationInfoDTO.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI) && paxList != null) {
			for (ReservationPaxTO resPaxTO : paxList) {
				if (!PaxTypeTO.INFANT.equals(resPaxTO.getPaxType())) {
					ExternalChargeUtil.setPaxEffectiveExternalChargeTaxAmount(resPaxTO,
							ibeReservationInfoDTO.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI),
							flightSegmentTO.getFlightRefNumber(), paxWiseModifyingSegAnciTotal);
				}
			}
		}
	}

	public static void updateEffectiveTax(Collection<ReservationPaxTO> paxList,
			Map<Integer, List<ExternalChgDTO>> paxEffectiveTaxes, boolean isInfantPaymentSeparated) {
		// Update pax external charges
		for (ReservationPaxTO reservationPaxTO : paxList) {
			if (!PaxTypeTO.INFANT.equals(reservationPaxTO.getPaxType()) || isInfantPaymentSeparated) {
				List<LCCClientExternalChgDTO> lccClientExternalChgDTOs = reservationPaxTO.getExternalCharges();
				boolean resetAll = false;
				if (paxEffectiveTaxes != null && !paxEffectiveTaxes.isEmpty()) {
					if (paxEffectiveTaxes.containsKey(reservationPaxTO.getSeqNumber())) {
						List<ExternalChgDTO> effectiveExtCharges = paxEffectiveTaxes.get(reservationPaxTO.getSeqNumber());
						updateWithEffectiveCharges(lccClientExternalChgDTOs, effectiveExtCharges);
					} else {
						resetAll = true;
					}

				} else {
					resetAll = true;
				}

				if (resetAll) {
					for (LCCClientExternalChgDTO lccClientExternalChgDTO : lccClientExternalChgDTOs) {
						if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.JN_ANCI) {
							lccClientExternalChgDTO.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						}
					}
				}
			}
		}
	}

	private static void updateWithEffectiveCharges(List<LCCClientExternalChgDTO> externalCharges,
			List<ExternalChgDTO> effectiveExtCharges) {
		if (effectiveExtCharges != null && !effectiveExtCharges.isEmpty()) {
			for (ExternalChgDTO effectiveExtChg : effectiveExtCharges) {
				if (effectiveExtChg instanceof ServiceTaxExtChgDTO) {
					ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) effectiveExtChg;
					for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalCharges) {
						if (lccClientExternalChgDTO.getExternalCharges() == serviceTaxExtChgDTO.getExternalChargesEnum()
								&& serviceTaxExtChgDTO.getFlightRefNumber()
										.equals(lccClientExternalChgDTO.getFlightRefNumber())) {
							lccClientExternalChgDTO.setAmount(serviceTaxExtChgDTO.getAmount());
							break;
						}
					}
				}
			}
		}
	}

	public static void applyAncillaryModifyPenalty(Collection<ReservationPaxTO> paxList, FlightSegmentTO flightSegmentTO) {
		if (paxList != null && !paxList.isEmpty()) {
			String flightRefNumber = flightSegmentTO.getFlightRefNumber();
			for (ReservationPaxTO reservationPaxTO : paxList) {
				if (!PaxTypeTO.INFANT.equals(reservationPaxTO.getPaxType())) {
					BigDecimal addAnciTotal = getPaxExtCharges(reservationPaxTO.getExternalCharges());
					BigDecimal removeAnciTotal = reservationPaxTO.getToRemoveAncillaryTotal();
					BigDecimal updateAnciTotal = reservationPaxTO.getToUpdateAncillaryTotal();
					BigDecimal paxChgs = AccelAeroCalculator.add(addAnciTotal, updateAnciTotal, removeAnciTotal.negate());

					if (AccelAeroCalculator.isLessThan(paxChgs, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						BigDecimal anciPenalty = paxChgs.negate();
						LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
						chgDTO.setExternalCharges(EXTERNAL_CHARGES.ANCI_PENALTY);
						chgDTO.setAmount(anciPenalty);
						chgDTO.setFlightRefNumber(flightRefNumber);
						chgDTO.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightRefNumber));
						chgDTO.setAmountConsumedForPayment(true);
						reservationPaxTO.addExternalCharges(chgDTO);
					}
				}
			}
		}
	}

	public static List<String> getModifyingFlightRefNumberList(String modifingFlightInfo) throws ParseException {
		List<String> modifyingFlightRefNumbers = new ArrayList<String>();

		if (modifingFlightInfo != null && !"".equals(modifingFlightInfo) && !"[]".equals(modifingFlightInfo)) {
			JSONParser parser = new JSONParser();
			JSONArray jSegments = (JSONArray) parser.parse(modifingFlightInfo);
			Iterator<?> iterator = jSegments.iterator();
			while (iterator.hasNext()) {
				JSONObject jSeg = (JSONObject) iterator.next();
				if (jSeg.isEmpty()) {
					continue;
				}

				String flightRefNo = jSeg.get("flightRefNumber").toString();
				modifyingFlightRefNumbers.add(flightRefNo.split("-")[0]);
			}

			Collections.sort(modifyingFlightRefNumbers, new Comparator<String>() {

				@Override
				public int compare(String str1, String str2) {
					Date depDate1 = FlightRefNumberUtil.getDepartureDateFromFlightRPH(str1);
					Date depDate2 = FlightRefNumberUtil.getDepartureDateFromFlightRPH(str2);

					return depDate1.compareTo(depDate2);
				}

			});
		}

		return modifyingFlightRefNumbers;
	}

	public static BigDecimal addTaxToServiceCharge(IBEReservationInfoDTO ibeReservationInfoDTO, BigDecimal serviceCharge) {
		BigDecimal serviceTax = calculatedServiceTax(ibeReservationInfoDTO, serviceCharge);
		return AccelAeroCalculator.add(serviceCharge, serviceTax);
	}

	private static BigDecimal calculatedServiceTax(IBEReservationInfoDTO ibeReservationInfoDTO, BigDecimal serviceCharge) {
		BigDecimal serviceChargeTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (ibeReservationInfoDTO.isTaxApplicable(EXTERNAL_CHARGES.JN_OTHER)) {
			if (AccelAeroCalculator.isGreaterThan(serviceCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				serviceChargeTax = AccelAeroCalculator.multiplyDefaultScale(serviceCharge,
						ibeReservationInfoDTO.getServiceTaxRatio(EXTERNAL_CHARGES.JN_OTHER));
			}
		}

		return serviceChargeTax;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ExternalChgDTO getApplicableServiceTax(IBEReservationInfoDTO ibeReservationInfoDTO,
			EXTERNAL_CHARGES taxChargeCode, Integer rateApplicableType) throws ModuleException {

		ExternalChgDTO externalChgDTO = null;
		ServiceTaxContainer serviceTaxObj = ibeReservationInfoDTO.getServiceTax(taxChargeCode);

		if (serviceTaxObj != null && serviceTaxObj.isTaxApplicable()) {
			BigDecimal totalJnTax = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (EXTERNAL_CHARGES taxableService : serviceTaxObj.getTaxableExternalCharges()) {
				BigDecimal serviceCharge = ibeReservationInfoDTO.getServiceCharge(taxableService);
				if (AccelAeroCalculator.isGreaterThan(serviceCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
					BigDecimal serviceJNTax = AccelAeroCalculator.multiplyDefaultScale(serviceCharge,
							ibeReservationInfoDTO.getServiceTaxRatio(taxChargeCode));

					totalJnTax = AccelAeroCalculator.add(totalJnTax, serviceJNTax);
				}
			}

			if (AccelAeroCalculator.isGreaterThan(totalJnTax, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				Collection colEXTERNAL_CHARGES = new ArrayList();
				colEXTERNAL_CHARGES.add(taxChargeCode);
				Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD()
						.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, rateApplicableType);
				externalChgDTO = extChgMap.get(taxChargeCode);

				if (externalChgDTO != null) {
					externalChgDTO.setTotalAmount(AccelAeroCalculator.parseBigDecimal(totalJnTax.doubleValue()));
					externalChgDTO.setAmount(externalChgDTO.getTotalAmount());
					((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(serviceTaxObj.getTaxApplyingFlightRefNumber());
					ibeReservationInfoDTO.addExternalCharge(externalChgDTO);
				}
			}
		}

		return externalChgDTO;
	}

	public static void setCorrectOndSequence(FlightSearchDTO flightSearchDTO, List<FlightSegmentTO> flightSegmentTOs) {
		List<ONDSearchDTO> ondSearchDTOs = flightSearchDTO.getOndList();
		if (ondSearchDTOs != null && !ondSearchDTOs.isEmpty()) {
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				int ondSeq = 0;
				String flrRefNum = FlightRefNumberUtil.filterFlightRPH(flightSegmentTO.getFlightRefNumber(), null);
				OND: for (ONDSearchDTO ondSearchDTO : ondSearchDTOs) {
					for (String ondFlightRPH : ondSearchDTO.getFlightRPHList()) {
						ondFlightRPH = FlightRefNumberUtil.filterFlightRPH(ondFlightRPH, null);
						if (ondFlightRPH.equals(flrRefNum)) {
							flightSegmentTO.setOndSequence(ondSeq);
							break OND;
						}
					}
					ondSeq++;
				}

			}
		}
	}

	public static boolean isSameFlightMofication(FlightSearchDTO flightSearchDTO, String flightRefNumber) {
		List<ONDSearchDTO> ondSearchDTOs = flightSearchDTO.getOndList();
		if (ondSearchDTOs != null && !ondSearchDTOs.isEmpty()) {
			String oldFltRefNum = FlightRefNumberUtil.filterFlightRPH(flightRefNumber, null);
			for (ONDSearchDTO ondSearchDTO : ondSearchDTOs) {
				List<String> newFlightRPHs = ondSearchDTO.getFlightRPHList();
				if (newFlightRPHs != null && !newFlightRPHs.isEmpty()) {
					for (String newFlightRPH : newFlightRPHs) {
						String newFltRefNum = FlightRefNumberUtil.filterFlightRPH(newFlightRPH, null);
						if (newFltRefNum.equals(oldFltRefNum)) {
							return ondSearchDTO.isSameFlightModification();
						}
					}
				}
			}
		}

		return false;
	}

	public static void setOndSequence(PriceInfoTO priceInfo, List<FlightSegmentTO> flightSegmentTOs) {
		for (FlightSegmentTO flightSegment : flightSegmentTOs) {
			for (FareRuleDTO fareRule : priceInfo.getFareTypeTO().getApplicableFareRules()) {
				if (fareRule.getOrignNDest().contains(flightSegment.getSegmentCode())) {
					flightSegment.setFareBasisCode(fareRule.getFareBasisCode());
					flightSegment.setBookingClass(fareRule.getBookingClassCode());
					flightSegment.setOndSequence(fareRule.getOndSequence());
					break;
				}
			}
		}
	}

	public static Map<Integer, BigDecimal> getPaxWisePromoDiscount(IBEReservationInfoDTO resInfo,
			Collection<ReservationPaxTO> colPax) {
		Map<Integer, BigDecimal> paxWisePromoDiscount = new HashMap<Integer, BigDecimal>();
		DiscountedFareDetails discountedFareDetails = resInfo.getDiscountInfo();

		if (discountedFareDetails == null
				|| PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discountedFareDetails.getDiscountAs())) {
			return paxWisePromoDiscount;
		}

		String promotionType = discountedFareDetails.getPromotionType();
		String discountType = discountedFareDetails.getDiscountType();
		List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>(colPax);
		Collections.sort(paxList);
		PriceInfoTO priceInfoTO = resInfo.getPriceInfoTO();
		int ondCount = priceInfoTO.getFareTypeTO().getOndWiseFareType().keySet().size();

		int paxCount = paxList.size();
		if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promotionType)) {
			paxCount = getPayablePaxCount(colPax);
		}

		BigDecimal[] discAmountArr = PromotionsUtils.getEffectivePaxDiscountValueWithoutLoss(
				discountedFareDetails.getFarePercentage(), discountedFareDetails.getDiscountApplyTo(),
				discountedFareDetails.getDiscountType(), paxCount, discountedFareDetails.getApplicableOnds().size());

		BigDecimal[] effectiveDiscAmountArr = null;
		if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
			effectiveDiscAmountArr = PromotionsUtils.getPaxWiseTotalDiscount(discAmountArr, paxCount);
		} else {
			effectiveDiscAmountArr = discAmountArr;
		}

		float effectiveDiscountValue;

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promotionType)) {
			List<String> applicableAncis = discountedFareDetails.getApplicableAncillaries();
			if (applicableAncis != null && !applicableAncis.isEmpty()) {
				BigDecimal totalAnciDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				int count = 0;
				for (ReservationPaxTO reservationPaxTO : paxList) {
					BigDecimal paxAnciTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal paxAnciDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal offeredDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

					List<LCCClientExternalChgDTO> externalCharges = reservationPaxTO.getExternalCharges();
					if (externalCharges != null && !externalCharges.isEmpty()) {
						for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalCharges) {
							if (applicableAncis.contains(lccClientExternalChgDTO.getExternalCharges().toString())
									&& lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.INSURANCE) {
								paxAnciTotalAmount = AccelAeroCalculator.add(paxAnciTotalAmount,
										lccClientExternalChgDTO.getAmount());
							} else if (applicableAncis.contains(lccClientExternalChgDTO.getExternalCharges().toString())
									&& com.isa.thinair.commons.core.util.StringUtil
											.isMatchingStringFound(discountedFareDetails.getApplicableOnds(), FlightRefNumberUtil
													.getSegmentCodeFromFlightRPH(lccClientExternalChgDTO.getFlightRefNumber()))) {
								paxAnciTotalAmount = AccelAeroCalculator.add(paxAnciTotalAmount,
										lccClientExternalChgDTO.getAmount());
							}
						}

						if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
							effectiveDiscountValue = effectiveDiscAmountArr[0].floatValue();
							paxAnciDiscount = AccelAeroCalculator.calculatePercentage(paxAnciTotalAmount, effectiveDiscountValue,
									RoundingMode.UP);
						} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
							offeredDiscount = effectiveDiscAmountArr[count];
							if (AccelAeroCalculator.isGreaterThan(offeredDiscount, paxAnciTotalAmount)) {
								paxAnciDiscount = paxAnciTotalAmount;
							} else {
								paxAnciDiscount = offeredDiscount;
							}
						}

						paxWisePromoDiscount.put(reservationPaxTO.getSeqNumber(), paxAnciDiscount);
						totalAnciDiscount = AccelAeroCalculator.add(totalAnciDiscount, paxAnciDiscount);
					}

					count++;
				}
			}

		} else {

			int adultItr = 0, childItr = 0, infantItr = 0;
			BigDecimal adultDiscount = AccelAeroCalculator.getDefaultBigDecimalZero(),
					childDiscount = AccelAeroCalculator.getDefaultBigDecimalZero(),
					infantDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

			Map<String, Integer> applicablePaxCount = null;

			if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(promotionType)) {
				applicablePaxCount = discountedFareDetails.getApplicablePaxCount();
			} else if (PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT.equals(promotionType)) {
				applicablePaxCount = new HashMap<String, Integer>();
				applicablePaxCount.put(PaxTypeTO.ADULT, 0);
				applicablePaxCount.put(PaxTypeTO.CHILD, 0);
				applicablePaxCount.put(PaxTypeTO.INFANT, null);
				for (ReservationPaxTO reservationPaxTO : paxList) {
					if (PaxTypeTO.ADULT.equals(reservationPaxTO.getPaxType())
							|| PaxTypeTO.PARENT.equals(reservationPaxTO.getPaxType())) {
						applicablePaxCount.put(PaxTypeTO.ADULT, applicablePaxCount.get(PaxTypeTO.ADULT) + 1);
					} else if (PaxTypeTO.CHILD.equals(reservationPaxTO.getPaxType())) {
						applicablePaxCount.put(PaxTypeTO.CHILD, applicablePaxCount.get(PaxTypeTO.CHILD) + 1);
					} else if (PaxTypeTO.INFANT.equals(reservationPaxTO.getPaxType())) {
						if (applicablePaxCount.get(PaxTypeTO.INFANT) == null) {
							applicablePaxCount.put(PaxTypeTO.INFANT, 0);
						}
						applicablePaxCount.put(PaxTypeTO.INFANT, applicablePaxCount.get(PaxTypeTO.INFANT) + 1);
					}
				}
			}

			int count = 0;
			int infCount = paxList.size();
			for (ReservationPaxTO reservationPaxTO : paxList) {

				if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
					effectiveDiscountValue = effectiveDiscAmountArr[0].floatValue();
				} else {
					effectiveDiscountValue = effectiveDiscAmountArr[count].floatValue();
				}

				if (PaxTypeTO.ADULT.equals(reservationPaxTO.getPaxType())
						|| PaxTypeTO.PARENT.equals(reservationPaxTO.getPaxType())) {
					BigDecimal discountAmount = null;

					if (adultItr < applicablePaxCount.get(PaxTypeTO.ADULT)) {
						adultDiscount = priceInfoTO.getPerPaxDiscountAmount(PaxTypeTO.ADULT, effectiveDiscountValue,
								discountedFareDetails.getDiscountType(), discountedFareDetails.getDiscountApplyTo(),
								discountedFareDetails.getApplicableOnds());
						discountAmount = adultDiscount;
						adultItr++;
					}

					if (reservationPaxTO.getIsParent()) {
						float infEffectiveDiscAmount = 0;
						if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
							infEffectiveDiscAmount = effectiveDiscAmountArr[0].floatValue();
						} else {
							infEffectiveDiscAmount = effectiveDiscAmountArr[reservationPaxTO.getInfantWith() - 1].floatValue();
						}

						if ((applicablePaxCount.get(PaxTypeTO.INFANT) == null
								|| (infantItr < applicablePaxCount.get(PaxTypeTO.INFANT)))) {
							infantDiscount = priceInfoTO.getPerPaxDiscountAmount(PaxTypeTO.INFANT, infEffectiveDiscAmount,
									discountedFareDetails.getDiscountType(), discountedFareDetails.getDiscountApplyTo(),
									discountedFareDetails.getApplicableOnds());
							discountAmount = (discountAmount == null)
									? infantDiscount
									: AccelAeroCalculator.add(infantDiscount, discountAmount);
							infantItr++;
						}

					}

					if (discountAmount == null) {
						discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					}

					paxWisePromoDiscount.put(reservationPaxTO.getSeqNumber(), discountAmount);

				} else if (PaxTypeTO.CHILD.equals(reservationPaxTO.getPaxType())) {
					if (childItr < applicablePaxCount.get(PaxTypeTO.CHILD)) {
						childDiscount = priceInfoTO.getPerPaxDiscountAmount(reservationPaxTO.getPaxType(), effectiveDiscountValue,
								discountedFareDetails.getDiscountType(), discountedFareDetails.getDiscountApplyTo(),
								discountedFareDetails.getApplicableOnds());
						paxWisePromoDiscount.put(reservationPaxTO.getSeqNumber(), childDiscount);
						childItr++;
					}
				}

				count++;
			}
		}

		return paxWisePromoDiscount;
	}

	private static int getPayablePaxCount(Collection<ReservationPaxTO> colPax) {
		int payablePaxCount = 0;
		if (colPax != null) {
			for (ReservationPaxTO pax : colPax) {
				if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
					payablePaxCount++;
				}
			}
		}
		return payablePaxCount;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject getSearchParamCookie(FlightSearchDTO searchParams, FareQuoteTO fareQuote,
			Collection<AvailableFlightInfo> flightInfo) {
		JSONObject searchParamsCookieDetails = new JSONObject();

		searchParamsCookieDetails.put("ond", searchParams.getFromAirport() + "/" + searchParams.getToAirport());
		searchParamsCookieDetails.put("outBoundDate", searchParams.getDepartureDate());
		searchParamsCookieDetails.put("isReturn", searchParams.isReturnFlag());

		if (searchParams.isReturnFlag()) {
			searchParamsCookieDetails.put("inBoundDate", searchParams.getReturnDate());
		}

		JSONObject paxCountObj = new JSONObject();
		paxCountObj.put("AD", searchParams.getAdultCount());
		paxCountObj.put("CH", searchParams.getChildCount());
		paxCountObj.put("INF", searchParams.getInfantCount());

		searchParamsCookieDetails.put("pax", paxCountObj);

		JSONArray flights = new JSONArray();
		if (flightInfo != null) {

			for (AvailableFlightInfo fltInfo : flightInfo) {
				if (fltInfo.isSelected()) {
					for (SegInfoDTO segment : fltInfo.getSegments()) {
						JSONObject df = new JSONObject();
						df.put("segCode", segment.getSegmentCode());
						df.put("flightNo", segment.getFlightNumber());
						df.put("date", new StringBuffer().append(segment.getDepartureDate()).append(" ")
								.append(segment.getDepartureTime()).toString());
						flights.add(df);
					}
				}

			}
		}
		JSONObject selectedFlights = new JSONObject();
		selectedFlights.put("flights", flights);
		selectedFlights.put("ond", searchParams.getFromAirport() + "/" + searchParams.getToAirport());
		BigDecimal totalFare = new BigDecimal(fareQuote.getTotalFare());
		BigDecimal totalTaxSurCharge = new BigDecimal(fareQuote.getTotalTaxSurcharge());
		selectedFlights.put("price", AccelAeroCalculator.add(totalFare, totalTaxSurCharge) + fareQuote.getCurrency());
		searchParamsCookieDetails.put("selectedFlights", selectedFlights);
		return searchParamsCookieDetails;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject getReservationCookieDetails(LCCClientReservation reservation) {
		JSONObject reservationCookieDetails = new JSONObject();
		reservationCookieDetails.put("pnr", reservation.getPNR());

		CommonReservationContactInfo contactInfo = reservation.getContactInfo();
		JSONObject contactInfoObj = new JSONObject();
		contactInfoObj.put("title", contactInfo.getTitle());
		contactInfoObj.put("firstName", contactInfo.getFirstName());
		contactInfoObj.put("lastName", contactInfo.getLastName());

		reservationCookieDetails.put("contactDetails", contactInfoObj);

		JSONArray segmentsArr = new JSONArray();
		for (LCCClientReservationSegment segment : reservation.getSegments()) {
			if (!ReservationSegmentSubStatus.EXCHANGED.equals(segment.getSubStatus())) {
				JSONObject segmentObj = new JSONObject();
				segmentObj.put("segmentCode", segment.getSegmentCode());
				segmentObj.put("sequence", segment.getSegmentSeq());
				segmentObj.put("departureDate", segment.getDepartureDate());
				segmentObj.put("status", segment.getStatus());

				segmentsArr.add(segmentObj);
			}
		}
		reservationCookieDetails.put("segments", segmentsArr);
		Object[] paxCol = ItineraryUtil.createPassengerList(reservation);

		Collection<PaxTO> colAdultCollection = (Collection<PaxTO>) paxCol[0];
		Collection<PaxTO> colChildCollection = (Collection<PaxTO>) paxCol[1];
		Collection<PaxTO> colInfantCollection = (Collection<PaxTO>) paxCol[2];

		int noOfPax = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount()
				+ reservation.getTotalPaxInfantCount();
		reservationCookieDetails.put("NumberOfPax", noOfPax);

		JSONObject anciInfoObj = new JSONObject();
		JSONArray paxAnciArray = new JSONArray();

		addAncilaryDetailsForCookieDetails(colAdultCollection, paxAnciArray);
		addAncilaryDetailsForCookieDetails(colChildCollection, paxAnciArray);
		addAncilaryDetailsForCookieDetails(colInfantCollection, paxAnciArray);
		anciInfoObj.put("paxAnci", paxAnciArray);

		List<LCCClientReservationInsurance> resInsurances = reservation.getReservationInsurances();
		if (resInsurances != null && !resInsurances.isEmpty()) {
			// TODO Handle for multiple insurances
			JSONObject insuranceObj = new JSONObject();
			insuranceObj.put("journeyStartAirport", resInsurances.get(0).getOrigin());
			insuranceObj.put("journeyEndAirport", resInsurances.get(0).getDestination());
			insuranceObj.put("totoalPremium", resInsurances.get(0).getQuotedTotalPremium());
			anciInfoObj.put("insurance", insuranceObj);

		}
		reservationCookieDetails.put("ancillaries", anciInfoObj);

		return reservationCookieDetails;
	}

	@SuppressWarnings("unchecked")
	private static void addAncilaryDetailsForCookieDetails(Collection<PaxTO> paxCol, JSONArray paxArray) {

		JSONArray mealArr = new JSONArray();
		JSONArray baggageArr = new JSONArray();
		JSONArray seatArr = new JSONArray();
		JSONArray airPortServicesArr = new JSONArray();
		if (paxCol != null && !paxCol.isEmpty()) {
			for (PaxTO pax : paxCol) {
				JSONObject adultObj = new JSONObject();
				adultObj.put("name", pax.getItnPaxName());
				adultObj.put("sequence", pax.getItnSeqNo());

				for (LCCSelectedSegmentAncillaryDTO selectedAncillary : pax.getSelectedAncillaries()) {
					String segmentCode = selectedAncillary.getFlightSegmentTO().getSegmentCode();
					List<LCCMealDTO> mealDTOs = selectedAncillary.getMealDTOs();
					if (mealDTOs != null) {
						for (LCCMealDTO meal : mealDTOs) {
							JSONObject mealObj = new JSONObject();
							mealObj.put("mealName", meal.getMealName());
							mealObj.put("soldMeals", meal.getSoldMeals());
							mealObj.put("totalCharge", meal.getTotalPrice());
							mealObj.put("segmentCode", segmentCode);
							mealArr.add(mealObj);
						}
						adultObj.put("meals", mealArr);

					}

					List<LCCBaggageDTO> baggageDTOs = selectedAncillary.getBaggageDTOs();
					if (baggageDTOs != null) {
						for (LCCBaggageDTO baggage : baggageDTOs) {
							JSONObject baggageObj = new JSONObject();
							baggageObj.put("baggageName", baggage.getBaggageName());
							baggageObj.put("baggageCharge", baggage.getBaggageCharge());
							baggageObj.put("segmentCode", segmentCode);
							baggageArr.add(baggageObj);
						}
						adultObj.put("baggages", baggageArr);
					}

					LCCAirSeatDTO airSeatDTO = selectedAncillary.getAirSeatDTO();
					JSONObject seatObj = new JSONObject();
					if (airSeatDTO != null && airSeatDTO.getSeatNumber() != null) {
						seatObj.put("seatNumber", airSeatDTO.getSeatNumber());
						seatObj.put("seatCharge", airSeatDTO.getSeatCharge());
						seatObj.put("segmentCode", segmentCode);
						seatArr.add(seatObj);
					}
					adultObj.put("seat", seatArr);

					List<LCCAirportServiceDTO> airportServiceDTOs = selectedAncillary.getAirportServiceDTOs();
					if (airportServiceDTOs != null) {
						for (LCCAirportServiceDTO airportServiceDTO : airportServiceDTOs) {
							JSONObject airportServiceObj = new JSONObject();
							airportServiceObj.put("ssrCode", airportServiceDTO.getSsrCode());
							airportServiceObj.put("ssrName", airportServiceDTO.getSsrName());
							airportServiceObj.put("serviceCharge", airportServiceDTO.getServiceCharge());
							airportServiceObj.put("airportCode", airportServiceDTO.getAirportCode());
							airPortServicesArr.add(airportServiceObj);
						}
						adultObj.put("airportServices", airPortServicesArr);
					}
				}
				paxArray.add(adultObj);

			}
		}
	}

	public static boolean isResEligibleForNameChange(LCCClientReservation reservation) {

		if (!AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.IBE)) {
			return false;
		}

		if (reservation.getNameChangeCount() >= AppSysParamsUtil.getNameChangeMaxCount(ApplicationEngine.IBE)) {
			return false;
		}

		if (!hasConfirmedSegments(reservation)) {
			return false;
		}

		boolean allowFlownSegs = AppSysParamsUtil.isEnableFareRuleLevelNCCForFlown(ApplicationEngine.IBE);

		if (hasFlownSegments(reservation) && !allowFlownSegs) {
			return false;
		}

		if (!isWithinCutoff(reservation)) {
			return false;
		}

		return true;
	}

	private static boolean isWithinCutoff(LCCClientReservation reservation) {

		long nameChangeCutoffTime = reservation.getNameChangeCutoffTime();

		for (LCCClientReservationSegment segment : com.isa.thinair.airproxy.api.utils.SortUtil
				.sortByDepDate(reservation.getSegments())) {

			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus())) {

				long timeDiff = segment.getDepartureDateZulu().getTime() - CalendarUtil.getCurrentZuluDateTime().getTime();

				if (timeDiff > -1 && timeDiff > nameChangeCutoffTime) {
					return true;
				}
			}
		}

		return false;
	}

	private static boolean hasFlownSegments(LCCClientReservation reservation) {

		FLOWN_FROM flownDecisionMethod = AppSysParamsUtil.getFlownCalculateMethod();

		switch (flownDecisionMethod) {
		case DATE:
			return hasFlownSegmentByDate(reservation);
		case ETICKET:
			return hasFlownSegmentByETicket(reservation);
		case PFS:
			return hasFlownSegmentByPFS(reservation);
		default:
			throw new IllegalArgumentException("Unknown FLOWN decision method.");
		}
	}

	private static boolean hasFlownSegmentByDate(LCCClientReservation reservation) {
		for (LCCClientReservationSegment segment : reservation.getSegments()) {
			if (segment.getDepartureDate().after(new Date())) {
				return true;
			}
		}

		return false;
	}

	private static boolean hasFlownSegmentByETicket(LCCClientReservation reservation) {
		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			for (LccClientPassengerEticketInfoTO eticket : pax.geteTickets()) {
				if (eticket.getPaxETicketStatus().equals(EticketStatus.FLOWN.code())) {
					return true;
				}else if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()
						&&	(EticketStatus.CHECKEDIN.code().equals(eticket.getPaxETicketStatus())
								|| EticketStatus.BOARDED.code().equals(eticket.getPaxETicketStatus()))){
					return true;
				}
			}
		}
		return false;
	}

	private static boolean hasFlownSegmentByPFS(LCCClientReservation reservation) {
		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			for (LccClientPassengerEticketInfoTO eticket : pax.geteTickets()) {
				if (eticket.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
					return true;
				}else if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()
						&&	(EticketStatus.CHECKEDIN.code().equals(eticket.getPaxETicketStatus())
								|| EticketStatus.BOARDED.code().equals(eticket.getPaxETicketStatus()))){
					return true;
				}
			}
		}
		return false;
	}

	private static boolean hasConfirmedSegments(LCCClientReservation reservation) {
		for (LCCClientReservationSegment lccClientReservationSegment : reservation.getSegments()) {
			if (lccClientReservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				return true;
			}
		}

		return false;
	}

	public static ReservationDiscountDTO calculateDiscountForReservation(IBEReservationInfoDTO resInfo,
			Collection<ReservationPaxTO> paxList, BaseAvailRQ baseAvailRQ, TrackInfoDTO trackInfoDTO, boolean calculateTotalOnly,
			DiscountedFareDetails discountInfo, PriceInfoTO priceInfoTO, String transactionId, boolean isSkipDiscountQuote)
			throws ModuleException {
		ReservationDiscountDTO resDiscountDTO = null;

		if (discountInfo != null && priceInfoTO != null && baseAvailRQ != null) {

			if (isSkipDiscountQuote
					&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discountInfo.getDiscountAs())) {
				return resDiscountDTO;
			}

			DiscountRQ promotionRQ = ReservationBeanUtil.getPromotionCalculatorRQ(discountInfo,
					ReservationBeanUtil.transformPaxList(paxList),
					baseAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
			promotionRQ.setCalculateTotalOnly(calculateTotalOnly);
			SYSTEM searchSystem = baseAvailRQ.getAvailPreferences().getSearchSystem();

			if (searchSystem != SYSTEM.AA && searchSystem != SYSTEM.INT && resInfo != null
					&& resInfo.getSelectedSystem() != null) {
				searchSystem = resInfo.getSelectedSystem();
			}

			if (promotionRQ != null) {
				resDiscountDTO = ReservationApiUtils.getApplicableDiscount(promotionRQ, priceInfoTO.getFareSegChargeTO(),
						baseAvailRQ, searchSystem, transactionId, trackInfoDTO);
				if (resInfo != null && resDiscountDTO.isDiscountExist()) {
					resInfo.setDiscountAmount(resDiscountDTO.getTotalDiscount());
				}
			}

		}

		return resDiscountDTO;
	}

	public static void setDiscountCalculatorRQ(Collection<ReservationPaxTO> paxList, IBEReservationInfoDTO resInfo,
			RedeemCalculateRQ redeemCalculateRQ) {
		DiscountRQ discountRQ = null;
		if (resInfo != null && resInfo.getDiscountInfo() != null && paxList != null && !paxList.isEmpty()) {
			DiscountedFareDetails discountInfoDTO = resInfo.getDiscountInfo();
			DISCOUNT_METHOD discountMethod = null;
			if (discountInfoDTO.getPromotionId() == null) {
				if (discountInfoDTO.getDomesticSegmentCodeList().size() > 0 && AppSysParamsUtil.isDomesticFareDiscountEnabled()) {
					discountMethod = DISCOUNT_METHOD.DOM_FARE_DISCOUNT;

				} else if (discountInfoDTO.getFarePercentage() > 0) {
					discountMethod = DISCOUNT_METHOD.FARE_DISCOUNT;

				}
			} else {
				discountMethod = DISCOUNT_METHOD.PROMOTION;
			}

			discountRQ = new DiscountRQ(discountMethod);
			discountRQ.setDiscountInfoDTO(resInfo.getDiscountInfo());
			discountRQ.setPaxChargesList(ReservationBeanUtil.transformPaxList(paxList));
			discountRQ.setSystem(resInfo.getSelectedSystem());
			discountRQ.setTransactionIdentifier(resInfo.getTransactionId());

			if (redeemCalculateRQ != null) {
				redeemCalculateRQ.setDiscountRQ(discountRQ);
			}

		}

	}

	public static boolean isOfflineBookingEligible(PriceInfoTO priceInfoTO, List<FlightSegmentTO> flightSegmentTOs,
			int validationType, Collection<ReservationPaxTO> paxList, String contactEmail, String ipAddress, SYSTEM searchSystem,
			boolean createRes, TrackInfoDTO trackInfo) throws ModuleException {

		boolean onholdEligible = false;

		if (AppSysParamsUtil.isOnHoldEnable(OnHold.IBEOFFLINEPAYMENT) && createRes && priceInfoTO != null
				&& !priceInfoTO.getFareTypeTO().isOnHoldRestricted()) {

			OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(flightSegmentTOs,
					priceInfoTO.getFareTypeTO());
			Date pnrReleaseTime = calculateOnHoldReleaseTime(OnHold.IBEOFFLINEPAYMENT,
					onHoldReleaseTimeDTO.getFlightDepartureDate(), onHoldReleaseTimeDTO);
			onholdEligible = isOnHoldEnable(OnHold.IBEOFFLINEPAYMENT, pnrReleaseTime);
			if (onholdEligible) {
				onholdEligible = onholdEligible && ModuleServiceLocator.getAirproxySearchAndQuoteBD().isOnholdEnabled(
						flightSegmentTOs, ReservationUtil.getReservationPaxList(paxList), contactEmail, validationType,
						trackInfo.getOriginChannelId(), searchSystem, ipAddress, trackInfo);
			}
		}

		return onholdEligible;
	}

	public static boolean isCsOcFlightAvailable(List<FlightSegmentTO> flightSegmentTOs) {
		boolean isCsOcFlightAvailable = false;
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getCsOcCarrierCode() != null) {
				isCsOcFlightAvailable = true;
			}
		}
		return isCsOcFlightAvailable;
	}
	
	public static boolean isSegmentModifiableAsPerETicketStatus(LCCClientReservation reservation, Integer pnrSegId) {
		if (AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				for (LccClientPassengerEticketInfoTO eTicket : pax.geteTickets()) {
					if (pnrSegId != null) {
						if (Integer.parseInt(eTicket.getPnrSegId()) == pnrSegId.intValue()) {
							if (EticketStatus.CHECKEDIN.code().equals(eTicket.getPaxETicketStatus())
									|| EticketStatus.BOARDED.code().equals(eTicket.getPaxETicketStatus())) {
								return false;
							}
						}
					} else {
						if (EticketStatus.CHECKEDIN.code().equals(eTicket.getPaxETicketStatus())
								|| EticketStatus.BOARDED.code().equals(eTicket.getPaxETicketStatus())) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}


	public static String getCountryCode(HttpServletRequest request, String IPAddress) throws ModuleException {
		String countryCode = null;

		if (!AppSysParamsUtil.getIsONDWisePayment()) {
			countryCode = ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(IPAddress);
			// Testing purpose , We are overriding the IP based country
			if (AppSysParamsUtil.isTestStstem()) {
				String testSysOriginCountry = (String) request.getSession().getAttribute("originCountry");
				if (testSysOriginCountry == null || testSysOriginCountry.trim().length() == 0) {
					testSysOriginCountry = "OT"; // Other country
				}
				countryCode = testSysOriginCountry;
			}
		}

		return countryCode;

	}
}
