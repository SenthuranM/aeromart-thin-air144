package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.api.dto.SocialSiteResponseTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.opensymphony.xwork2.ActionChainResult;

/**
 * 
 * IBE login for registered customer from social sites - Currently supports for sign in with facebook
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Action.REDIRECT_TO_CUSTOMER_LOGIN, type = ActionChainResult.class, value = StrutsConstants.Action.REDIRECT_TO_CUSTOMER_LOGIN)

})
public class CustomerLoginFromSocialSiteAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(CustomerLoginFromSocialSiteAction.class);

	private boolean success = true;
	private String messageTxt = "";
	private String driveCode = "";

	private IBECommonDTO commonParams = null;
	AirCustomerServiceBD customerDelegate = null;

	private SocialSiteResponseTO externalSiteResponse = new SocialSiteResponseTO();

	private String emailId;
	private String password;

	private String jsonData;

	public String execute() {
		String forward = StrutsConstants.Result.ERROR;

		try {

			customerDelegate = ModuleServiceLocator.getCustomerBD();

			String socialSiteCustomerId = externalSiteResponse.getId();
			String socialSiteType = externalSiteResponse.getSiteType();
			String socialEmail = externalSiteResponse.getEmail();

			if (log.isDebugEnabled()) {
				log.debug("External Site Response : " + externalSiteResponse.toString());
			}

			String socialSiteCustId = null;
			String socialEmailId = null;

			if (!StringUtil.isEmpty(socialSiteCustomerId)) {
				socialSiteCustomerId = socialSiteCustomerId.trim();
				socialSiteCustId = socialSiteCustomerId.trim();
			}
			if (!StringUtil.isEmpty(socialEmail)) {
				socialEmailId = socialEmail;
			}

			boolean legacyRegistrationExists = false;
			boolean socialRegistrationExists = false;
			boolean socialSiteRespondWithEmail = false;

			if (socialEmailId != null) {
				socialSiteRespondWithEmail = true;
				legacyRegistrationExists = checkSubscriptionsInLegacyLogin(socialEmailId);
			}

			if (socialSiteCustId != null && socialSiteType != null) {
				socialRegistrationExists = checkSubscriptionsInSocialLogin(socialSiteCustId, socialSiteType);
			}

			if (socialRegistrationExists) {
				// both social and legacy reg exists
				Customer persistedCustomer = customerDelegate.getCustomer(socialEmailId);

				SessionUtil.setCustomerId(request, persistedCustomer.getCustomerId());
				SessionUtil.setCustomerEMail(request, persistedCustomer.getEmailId());
				SessionUtil.setCustomerPassword(request, persistedCustomer.getPassword());

				if (persistedCustomer.getLMSMemberDetails() != null) {
					SessionUtil.setLoyaltyFFID(request, persistedCustomer.getLMSMemberDetails().getFfid());
				}

				request.setAttribute("grantSocialLogin", true);
				return StrutsConstants.Action.REDIRECT_TO_CUSTOMER_LOGIN;

			} else if (socialSiteRespondWithEmail && legacyRegistrationExists) {
				// legacy registration exists, create social account and merge, then auto log
				Customer persistedCustomer = customerDelegate.getCustomer(socialEmailId);

				Set<SocialCustomer> existingSocialCustomerProfiles = persistedCustomer.getSocialCustomer();

				if (existingSocialCustomerProfiles == null) {
					existingSocialCustomerProfiles = new HashSet<SocialCustomer>();
				}
				boolean alreadyRegistered = false;
				for (SocialCustomer existingProfile : existingSocialCustomerProfiles) {
					String existingSiteType = null;
					if (existingProfile.getSocialCustomerTypeId() != null) {
						existingSiteType = existingProfile.getSocialCustomerTypeId().toString();
					}
					if (existingSiteType != null && socialSiteType.equals(existingSiteType)) {
						alreadyRegistered = true;
						break;
					}
				}

				if (!alreadyRegistered) {
					SocialCustomer socialProfileToMerge = new SocialCustomer();
					socialProfileToMerge.setCustomerId(new Integer(persistedCustomer.getCustomerId()));
					socialProfileToMerge.setSocialSiteCustId(socialSiteCustId);
					socialProfileToMerge.setSocialCustomerTypeId(new Integer(socialSiteType));
					existingSocialCustomerProfiles.add(socialProfileToMerge);
				}

				Set<SocialCustomer> updatedSocialProfiles = existingSocialCustomerProfiles;

				persistedCustomer.setSocialCustomer(updatedSocialProfiles);
				customerDelegate.saveOrUpdate(persistedCustomer);

				SessionUtil.setCustomerId(request, persistedCustomer.getCustomerId());
				SessionUtil.setCustomerEMail(request, persistedCustomer.getEmailId());
				SessionUtil.setCustomerPassword(request, persistedCustomer.getPassword());

				if (persistedCustomer.getLMSMemberDetails() != null) {
					SessionUtil.setLoyaltyFFID(request, persistedCustomer.getLMSMemberDetails().getFfid());
				}

				request.setAttribute("grantSocialLogin", true);
				return StrutsConstants.Action.REDIRECT_TO_CUSTOMER_LOGIN;

			} else if (socialSiteRespondWithEmail) {
				// no social registration, no legacy registration (if customer proceed, accounts will be merged)
				driveCode = "redirectToRegistrationWithAutoFill";
			} else {
				log.info("Privacy restrictions in social site, check permisson of email settings");
			}

			forward = StrutsConstants.Result.SUCCESS;

		} catch (Exception e) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("LoginFromSocialSitesAction==>", e);
		}

		return forward;

	}

	public String retriveCustomerDataForPreFill() {

		String forward = StrutsConstants.Result.ERROR;
		try {

			if (jsonData != null && !jsonData.isEmpty()) {
				externalSiteResponse = JSONFETOParser.parse(SocialSiteResponseTO.class, this.jsonData);

				if (log.isDebugEnabled()) {
					log.debug("****  parsed auto fill data : " + externalSiteResponse.toString());
				}
			}

			forward = StrutsConstants.Result.SUCCESS;
		} catch (Exception e) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request)); // TODO
			log.error("LoginFromSocialSitesAction : retriveAutoFillDataFromSocialSite==>", e);
		}

		return forward;

	}

	private boolean checkSubscriptionsInLegacyLogin(String socialEmailId) throws ModuleException {
		boolean regCustomerExists = false;
		Customer persistedCustomer = customerDelegate.getCustomer(socialEmailId);

		if (persistedCustomer != null) {
			regCustomerExists = true;
		}

		return regCustomerExists;
	}

	private boolean checkSubscriptionsInSocialLogin(String socialCustomerId, String socialSiteType) throws ModuleException {
		boolean regCustomerExists = false;
		SocialCustomer socialCustomer = customerDelegate.getSocialCustomer(socialCustomerId, socialSiteType);

		if (socialCustomer != null) {
			regCustomerExists = true;
		}

		return regCustomerExists;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getDriveCode() {
		return driveCode;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public IBECommonDTO getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getPassword() {
		return password;
	}

	public SocialSiteResponseTO getExternalSiteResponse() {
		return externalSiteResponse;
	}

	public void setExternalSiteResponse(SocialSiteResponseTO externalSiteResponse) {
		this.externalSiteResponse = externalSiteResponse;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

}
