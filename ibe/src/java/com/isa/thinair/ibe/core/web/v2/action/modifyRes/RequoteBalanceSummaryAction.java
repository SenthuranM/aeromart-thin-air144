package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.ibe.api.dto.CancelBalanceSummaryDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.ModifyBalanceDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.BalanceSummaryUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;

/**
 * @author M.Rikaz
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class RequoteBalanceSummaryAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(RequoteBalanceSummaryAction.class);

	private boolean success = true;

	private String messageTxt;

	private ConfirmUpdateChargesTO updateCharge;

	private ArrayList<String> outFlightRPHList = new ArrayList<String>();

	private ArrayList<String> retFlightRPHList = new ArrayList<String>();

	private String version;

	// private String mode;

	private String pnr;

	private boolean groupPNR;

	private FareQuoteTO fareQuote;

	private CancelBalanceSummaryDTO balanceSummary;

	private boolean blnNoPay;

	private boolean modifySegment;

	private boolean cancelReservation;

	private boolean cancelSegment;

	private HashMap<String, String> jsonLabel = new HashMap<String, String>();

	// Fare Quote Update Details
	private ModifyBalanceDTO modifyBalanceDTO;

	private boolean addGroundSegment;

	private String balanceQueryData;
	
	private String nameChangePaxData;
    
    private boolean nameChangeRequote;

	private Double remainingLoyaltyPoints = null;
	
	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		ReservationBalanceTO lCCClientReservationBalanceTO = null;
		List<ReservationPaxTO> paxList = null;
		String strLanguage = SessionUtil.getLanguage(request);

		try {

			if (resInfo == null) {
				// TODO
				throw new ModuleException("session.error");
			}
			RequoteBalanceQueryRQ balanceQueryRQ = JSONFETOParser.parse(RequoteBalanceQueryRQ.class, balanceQueryData);
			SYSTEM system = SYSTEM.getEnum(getSearchParams().getSearchSystem());
			if (system == SYSTEM.INT || groupPNR) {
				version = ReservationCommonUtil.getCorrectInterlineVersion(version);
				system = SYSTEM.INT;
				balanceQueryRQ.setSystem(SYSTEM.INT);
				balanceQueryRQ.setVersion(version);
				balanceQueryRQ.setTransactionIdentifier(resInfo.getTransactionId());
				balanceQueryRQ.setCarrierWiseFlightRPHMap(ReservationCommonUtil
						.getCarrierWiseRPHs(getSearchParams().getOndList()));
				balanceQueryRQ.setGroupPnr(pnr);
			}

			FlightPriceRQ flightPriceRQ = BeanUtil.createFareReQuoteRQ(getSearchParams(), false);
			// FIXME avoid creating AvailableFlightSearchDTOBuilder and directly create the flightPriceRQ
			// flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());

			balanceQueryRQ.setExcludedSegFarePnrSegIds(getSearchParams().getExcludedSegFarePnrSegIds());
			balanceQueryRQ.setRequoteSegmentMap(ReservationBeanUtil.getReQuoteResSegmentMap(getSearchParams().getOndList()));
			
			remainingLoyaltyPoints = CustomerUtil.getMemberAvailablePoints(request);
			balanceQueryRQ.setRemainingLoyaltyPoints(remainingLoyaltyPoints);

			if (nameChangeRequote) {
			    balanceQueryRQ.setNameChangedPaxMap(ReservationUtil.transformToPaxMap(ReservationUtil
	                    .transformNameChangePax(nameChangePaxData)));
	        }
			
			Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();
			balanceQueryRQ.setOndFareTypeByFareIdMap(ReservationBeanUtil.getONDFareTypeByFareIdMap(
					getSearchParams().getOndList(), oldFareIdByFltSegIdMap));
			balanceQueryRQ.setOldFareIdByFltSegIdMap(oldFareIdByFltSegIdMap);

			QuotedFareRebuildDTO fareInfo = null;
			if (!cancelSegment && resInfo.getPriceInfoTO() != null) {
				fareInfo = new QuotedFareRebuildDTO(resInfo.getPriceInfoTO().getFareSegChargeTO(), flightPriceRQ, null);
			}
			balanceQueryRQ.setFareInfo(fareInfo);
			lCCClientReservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD().getRequoteBalanceSummary(
					balanceQueryRQ, getTrackInfo());

			// }

			// lCCClientReservationBalanceTO =
			// ModuleServiceLocator.getAirproxyReservationBD().getResAlterationBalanceSummary(
			// lCCClientResAlterQueryModesTO, getTrackInfo());

			version = lCCClientReservationBalanceTO.getVersion();

			if (lCCClientReservationBalanceTO.getVersion() != null) {
				// lCCClientResAlterQueryModesTO.setVersion(lCCClientReservationBalanceTO.getVersion());
			}

			// TODO Remove
			resInfo.setCreditableAmount(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getSegmentSummary()
					.getCurrentRefunds()));
			resInfo.setModifyCharge(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getSegmentSummary()
					.getNewModAmount()));

			if (resInfo.getSelectedSystem() == null) {
				resInfo.setSelectedSystem(system);
			}

			if (cancelReservation || cancelSegment) {
				// Local for Cancel Reservation/Segment
				String[] pagesIDs = { "PgCancel", "Common", "PgPriceBreakDown" };
				jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
				balanceSummary = BalanceSummaryUtil.getCancelBalanceSummary(lCCClientReservationBalanceTO,
						resInfo.getPaxCreditMap(pnr));
			} else {
				resInfo.setlCCClientReservationBalance(lCCClientReservationBalanceTO);
				// Clear Display Modify Data if not display in calendar page
				// if (!AppSysParamsUtil.isModifySegmentBalanceDisplayAvailabilitySearch()) {
				// updateCharge = null;
				// } else {
				resInfo.setCreditCardFee(AccelAeroCalculator.getDefaultBigDecimalZero());
				String[] pagesIDs = { "PgModifySement", "PgPriceBreakDown" };
				jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
				updateCharge = BalanceSummaryUtil.getChargesDetails(lCCClientReservationBalanceTO, paxList, resInfo,
						getSearchParams().getSelectedCurrency(), strLanguage, pnr, getSearchParams().isQuoteOutboundFlexi(),
						getSearchParams().isQuoteInboundFlexi(), true);
				blnNoPay = resInfo.isNoPay();
				// }
				modifyBalanceDTO = BalanceSummaryUtil.getFareQuoteUpdateDetails(lCCClientReservationBalanceTO,
						resInfo.getPaxCreditMap(pnr), getSearchParams().getSelectedCurrency(), resInfo.isInfantPaymentSeparated());
			}
		} catch (ModuleException me) {
			success = false;
			messageTxt = I18NUtil.getMessage(me.getExceptionCode(), SessionUtil.getLanguage(request));
			log.error("BalanceSummaryAction==>" , me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("BalanceSummaryAction==>" + ex, ex);
		}

		return forward;
	}

	private Map<Integer, List<LCCClientExternalChgDTO>> getPaxExtChargesMap(Collection<ReservationPaxTO> paxList) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		for (ReservationPaxTO pax : paxList) {
			paxExtChgMap.put(pax.getSeqNumber(), pax.getExternalCharges());
		}
		return paxExtChgMap;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	public String getBalanceQueryData() {
		return balanceQueryData;
	}

	public ConfirmUpdateChargesTO getUpdateCharge() {
		return updateCharge;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public static void setLog(Log log) {
		RequoteBalanceSummaryAction.log = log;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public void setUpdateCharge(ConfirmUpdateChargesTO updateCharge) {
		this.updateCharge = updateCharge;
	}

	public void setCancelReservation(boolean cancelReservation) {
		this.cancelReservation = cancelReservation;
	}

	public void setCancelSegment(boolean cancelSegment) {
		this.cancelSegment = cancelSegment;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public void setFareQuote(FareQuoteTO fareQuote) {
		this.fareQuote = fareQuote;
	}

	public CancelBalanceSummaryDTO getBalanceSummary() {
		return balanceSummary;
	}

	public boolean isBlnNoPay() {
		return blnNoPay;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public ModifyBalanceDTO getModifyBalanceDTO() {
		return modifyBalanceDTO;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

    public String getNameChangePaxData() {
        return nameChangePaxData;
    }

    public void setNameChangePaxData(String nameChangePaxData) {
        this.nameChangePaxData = nameChangePaxData;
    }

    public boolean isNameChangeRequote() {
        return nameChangeRequote;
    }

    public void setNameChangeRequote(boolean nameChangeRequote) {
        this.nameChangeRequote = nameChangeRequote;
    }

}
