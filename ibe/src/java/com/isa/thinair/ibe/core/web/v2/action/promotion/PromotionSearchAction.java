package com.isa.thinair.ibe.core.web.v2.action.promotion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.parser.ParseException;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class PromotionSearchAction extends IBEBaseAction {

	private static final Log log = LogFactory.getLog(PromotionSearchAction.class);

	private boolean success = true;

	private String preferedLang;

	private String promoCode;

	private boolean addGroundSegment = false;

	private boolean modifySegment = false;

	private boolean modifyAncillary = false;

	private boolean makePayment = false;

	private boolean checkPromotionWithPromoCode;

	private String insurance;

	private String paxWiseAnci;

	private FareQuoteTO fareQuote;

	private DiscountedFareDetails discountInfo;

	private boolean hasExistingPromotion;

	private boolean hasBinPromotion;

	private boolean overrideExistingPromotion = false;

	private BigDecimal existingDiscountAmount;

	private BigDecimal binPromotionDiscountAmount;

	public String execute() {

		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);

		String cabinClass = getSearchParams().getClassOfService();
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		SelectedFltSegBuilder fltSegBuilder;
		try {
			fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());

			flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			SortUtil.sortFlightSegByDepDate(flightSegmentTOs);

			if (resInfo.getPriceInfoTO() != null) {
				for (FlightSegmentTO flightSegment : flightSegmentTOs) {
					for (FareRuleDTO fareRule : resInfo.getPriceInfoTO().getFareTypeTO().getApplicableFareRules()) {
						if (fareRule.getOrignNDest().contains(flightSegment.getSegmentCode())) {
							flightSegment.setCabinClassCode(fareRule.getCabinClassCode());
							flightSegment.setOndSequence(fareRule.getOndSequence());
							break;
						}
					}
				}
			}

			ApplicablePromotionDetailsTO applicablePromotion = getApplicableDiscountPromotionsForBIN(flightSegmentTOs, resInfo);
			List<PassengerTypeQuantityTO> paxQtyList = constructPaxQtyList();

			if (applicablePromotion != null && resInfo.getDiscountInfo() != null) {
				hasExistingPromotion = true;
				// check the picked promotion is same as the promotion which is already applied
				if (!resInfo.getDiscountInfo().getPromotionId().equals(applicablePromotion.getPromoCriteriaId())) {
					hasBinPromotion = true;
					existingDiscountAmount = resInfo.getDiscountAmount(false);

					//*********************** FIX ME *************************
					//use back  ReservationUtil.calculateDiscountForReservation method for calcuation
					binPromotionDiscountAmount = getDiscountAmount(applicablePromotion, paxQtyList);
				}
			}

			if (applicablePromotion != null && (overrideExistingPromotion || !hasExistingPromotion)) {
				resInfo.setFareDiscount(paxQtyList, applicablePromotion);

				FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
				ReservationUtil.calculateDiscountForReservation(resInfo, null, flightAvailRQ, getTrackInfo(), true,
						resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(), resInfo.getTransactionId(), false);

				this.fareQuote = constructFareQuote(resInfo, flightSegmentTOs, fltSegBuilder);
				this.discountInfo = resInfo.getDiscountInfo();
				hasExistingPromotion = false;
				hasBinPromotion = true;
			}

		} catch (java.text.ParseException ex) {
			success = false;
			log.error(ex);
		} catch (ParseException e) {
			success = false;
			log.error(e);
		} catch (ModuleException e) {
			success = false;
			log.error(e);
		}
		return StrutsConstants.Result.SUCCESS;
	}

	public String removeBinPromotion() {

		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		SelectedFltSegBuilder fltSegBuilder;
		try {
			fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());
			flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
			List<PassengerTypeQuantityTO> paxQtyList = flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList();
			resInfo.setFareDiscount(paxQtyList);

			ReservationUtil.calculateDiscountForReservation(resInfo, null, flightAvailRQ, getTrackInfo(), true,
					resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(), resInfo.getTransactionId(), false);			

			this.fareQuote = constructFareQuote(resInfo, flightSegmentTOs, fltSegBuilder);
			this.fareQuote.setHasPromoDiscount(false);

		} catch (java.text.ParseException ex) {
			success = false;
			log.error(ex);
		} catch (ParseException e) {
			success = false;
			log.error(e);
		} catch (ModuleException e) {
			success = false;
			log.error(e);
		}
		return StrutsConstants.Result.SUCCESS;
	}

	private ApplicablePromotionDetailsTO getApplicableDiscountPromotionsForBIN(List<FlightSegmentTO> flightSegmentTOs,
			IBEReservationInfoDTO resInfo) {

		try {
			AirproxyReservationQueryBD airproxySearchAndQuoteBD = ModuleServiceLocator.getAirproxySearchAndQuoteBD();
			ApplicablePromotionDetailsTO promotion = airproxySearchAndQuoteBD.pickApplicablePromotions(
					buildPromotionCriteria(flightSegmentTOs, resInfo),
					PromotionsUtils.getPreferedSystem(getSearchParams().getSearchSystem()), getTrackInfo());

			if (promotion != null && promotion.getPromoType().equals(PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT)) {
				return promotion;
			}
			return null;
		} catch (ModuleException e) {
			success = false;
			log.error(e);
		}
		return null;
	}

	private PromoSelectionCriteria buildPromotionCriteria(List<FlightSegmentTO> flightSegmentTOs, IBEReservationInfoDTO resInfo) {

		PriceInfoTO priceInfoTO = resInfo.getPriceInfoTO();
		List<OndClassOfServiceSummeryTO> availableLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
		List<FareRuleDTO> applicableFareRules = priceInfoTO.getFareTypeTO().getApplicableFareRules();

		PromoSelectionCriteria promoSelectionCriteria = new PromoSelectionCriteria();
		promoSelectionCriteria.setReservationDate(CalendarUtil.getCurrentSystemTimeInZulu());
		promoSelectionCriteria.setAdultCount(searchParams.getAdultCount());
		promoSelectionCriteria.setChildCount(searchParams.getChildCount());
		promoSelectionCriteria.setInfantCount(searchParams.getInfantCount());
		if (promoCode != null && !promoCode.isEmpty()) {
			this.checkPromotionWithPromoCode = true;
			promoSelectionCriteria.setPromoCode(promoCode);
		}

		if (searchParams.getBankIdentificationNo() != null && !searchParams.getBankIdentificationNo().isEmpty()) {
			promoSelectionCriteria.setBankIdNo(Integer.parseInt(searchParams.getBankIdentificationNo().trim()));
		}

		promoSelectionCriteria.setSalesChannel(getTrackInfo().getOriginChannelId());
		promoSelectionCriteria.setPreferredLanguage(preferedLang);

		if (searchParams.getReturnDate() != null && !searchParams.getReturnDate().isEmpty()) {
			promoSelectionCriteria.setJourneyType(JourneyType.ROUNDTRIP);
		} else {
			promoSelectionCriteria.setJourneyType(JourneyType.SINGLE_SECTOR);
		}

		if (flightSegmentTOs != null) {
			for (FlightSegmentTO flightSegment : flightSegmentTOs) {
				promoSelectionCriteria.getFlights().add(flightSegment.getFlightNumber());
				promoSelectionCriteria.getFlightSegIds().add(flightSegment.getFlightSegId());
				promoSelectionCriteria.getCabinClasses().add(flightSegment.getCabinClassCode());
				promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().put(flightSegment.getFlightSegId(),
						flightSegment.getLogicalCabinClassCode());
			}

			promoSelectionCriteria.setOndFlightDates(PromotionsUtils.getOndFlightDates(flightSegmentTOs));
		}

		for (FareRuleDTO fareRule : applicableFareRules) {
			promoSelectionCriteria.getBookingClasses().add(fareRule.getBookingClassCode());
		}
		for (OndClassOfServiceSummeryTO ondClassSummery : availableLogicalCCList) {
			promoSelectionCriteria.getOndList().add(ondClassSummery.getOndCode());

			for (LogicalCabinClassInfoTO cabinClassInfoTO : ondClassSummery.getAvailableLogicalCCList()) {
				promoSelectionCriteria.getLogicalCabinClasses().add(cabinClassInfoTO.getLogicalCCCode());
			}
		}

		promoSelectionCriteria.setDryOperatingAirline(PromotionsUtils.getDryOperatingAirline(flightSegmentTOs,
				searchParams.getSearchSystem()));

		return promoSelectionCriteria;
	}

	private FareQuoteTO constructFareQuote(IBEReservationInfoDTO resInfo, List<FlightSegmentTO> flightSegmentTOs,
			SelectedFltSegBuilder fltSegBuilder) throws ParseException, ModuleException, java.text.ParseException {

		BigDecimal totalWithoutAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal reservationBalance = BigDecimal.ZERO;
		BigDecimal loyaltyCredit = BigDecimal.ZERO;
		BigDecimal ccFee = BigDecimal.ZERO;
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		List<LCCInsuranceQuotationDTO> insuranceQuotations = null;
		LCCInsuredJourneyDTO insJrnyDto = null;
		Collection<ReservationPaxTO> paxList = null;

		if (resInfo.getTotalPriceWithoutExtChg() != null) {
			totalWithoutAnci = new BigDecimal(resInfo.getTotalPriceWithoutExtChg());
		}

		if (SessionUtil.getIbePromotionPaymentPostDTO(request) == null) {
			if (!addGroundSegment) {
				insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(flightSegmentTOs, fltSegBuilder.isReturn(),
						getTrackInfo().getOriginChannelId());
			} else {
				insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(flightSegmentTOs, fltSegBuilder.isReturn(),
						getTrackInfo().getOriginChannelId());
			}
		}

		if (!makePayment) {
			insuranceQuotations = AncillaryJSONUtil.getInsuranceQuotation(insurance, insJrnyDto);
		}

		paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotations, ApplicationEngine.IBE,
				flightSegmentTOs, null, resInfo.getAnciOfferTemplates());

		BigDecimal totalForCCCal = new BigDecimal(totalWithoutAnci.toString());

		totalForCCCal = AccelAeroCalculator.add(totalForCCCal, ReservationUtil.getTotalAncillaryCharges(paxList), resInfo
				.getDiscountAmount(true).negate());

		ExternalChgDTO ccChgDTO = calculateCCCharges(totalForCCCal, PaxUtil.getPayablePaxCount(paxList, modifyAncillary),
				(modifyAncillary ? ReservationUtil.getAnciUpdatedSegmentCount(paxList) : flightSegmentTOs.size()),
				((modifySegment || modifyAncillary || addGroundSegment)
						? ChargeRateOperationType.MODIFY_ONLY
						: ChargeRateOperationType.MAKE_ONLY));
		ccFee = ccChgDTO.getAmount();
		resInfo.addExternalCharge(ccChgDTO);
		resInfo.setCreditCardFee(ccFee);

		boolean creditDiscount = (resInfo.getDiscountInfo() != null) ? DiscountApplyAsTypes.CREDIT.equals(resInfo
				.getDiscountInfo().getDiscountAs()) : true;

		BigDecimal voucherRedeemdAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (resInfo.getPayByVoucherInfo() != null) {
			voucherRedeemdAmount = resInfo.getPayByVoucherInfo().getRedeemedTotal();
		}

		FareQuoteTO fareQuote = ReservationUtil.fillFareQuote(paxList, getSearchParams().getSelectedCurrency(), totalWithoutAnci,
				ccFee, resInfo.getTotalFare(), resInfo.getTotalTax(), resInfo.getTotalTaxSurchages(),
				resInfo.getLoyaltyPayOption(), loyaltyCredit, modifySegment, resInfo.getlCCClientReservationBalance(),
				creditableAmount, modifyAncillary, reservationBalance, addGroundSegment, exchangeRateProxy, true,
				resInfo.getDiscountAmount(false), creditDiscount, resInfo.getIbeReturnFareDiscountAmount(),
				(modifySegment && AppSysParamsUtil.isRequoteEnabled()), CustomerUtil.getLMSPaymentAmount(resInfo),
				voucherRedeemdAmount, resInfo.isInfantPaymentSeparated());

		return fareQuote;
	}

	@SuppressWarnings("unchecked")
	private ExternalChgDTO calculateCCCharges(BigDecimal totalTicketPriceExcludingCC, int payablePaxCount, int segmentCount,
			Integer rateApplicableType) throws ModuleException {

		@SuppressWarnings("rawtypes")
		Collection colEXTERNAL_CHARGES = new ArrayList();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, rateApplicableType);
		ExternalChgDTO externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
		if (externalChgDTO.isRatioValueInPercentage()) {
			externalChgDTO.calculateAmount(totalTicketPriceExcludingCC);
		} else {
			externalChgDTO.calculateAmount(payablePaxCount, segmentCount);
		}
		return externalChgDTO;
	}
	@Deprecated
	private BigDecimal getDiscountAmount(ApplicablePromotionDetailsTO promoDetails, List<PassengerTypeQuantityTO> paxQtyList) {
		BigDecimal value = BigDecimal.ZERO;
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);

		int totalPaxCount = 0;
		for (PassengerTypeQuantityTO paxTypeQty : paxQtyList) {
			totalPaxCount += paxTypeQty.getQuantity();
		}

		float effectiveDiscountValue = 0;
		BigDecimal[] effectiveDiscAmountArr = PromotionsUtils.getEffectivePaxDiscountValueWithoutLoss(
				promoDetails.getDiscountValue(), promoDetails.getApplyTo(), promoDetails.getDiscountType(), totalPaxCount, 0);
		int count = 0;
		for (PassengerTypeQuantityTO paxTypeQty : paxQtyList) {

			Integer paxCount = paxTypeQty.getQuantity();
			if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(promoDetails.getPromoType())) {
				paxCount = discountInfo.getPaxCount(paxTypeQty.getPassengerType());
			}

			if (paxCount != null && paxCount > 0) {
				for (int i = 0; i < paxCount; i++) {
					if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(promoDetails.getDiscountType())) {
						effectiveDiscountValue = effectiveDiscAmountArr[0].floatValue();
					} else {
						effectiveDiscountValue = effectiveDiscAmountArr[count].floatValue();
					}

					BigDecimal discountAmount = resInfo.getPriceInfoTO().getPerPaxDiscountAmount(paxTypeQty.getPassengerType(),
							effectiveDiscountValue, promoDetails.getDiscountType(), promoDetails.getApplyTo(),
							promoDetails.getApplicableOnds());

					value = AccelAeroCalculator.add(value, discountAmount);
					count++;
				}
			}

		}
		return AccelAeroCalculator.scaleDefaultRoundingDown(value);
	}

	private List<PassengerTypeQuantityTO> constructPaxQtyList() {

		TravelerInfoSummaryTO traverlerInfo = new TravelerInfoSummaryTO();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);

		if (searchParams.getAdultCount() == null) {
			adultsQuantity.setQuantity(0);
		} else {
			adultsQuantity.setQuantity(searchParams.getAdultCount());
		}

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);

		if (searchParams.getChildCount() == null) {
			childQuantity.setQuantity(0);
		} else {
			childQuantity.setQuantity(searchParams.getChildCount());
		}

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);

		if (searchParams.getInfantCount() == null) {
			infantQuantity.setQuantity(0);
		} else {
			infantQuantity.setQuantity(searchParams.getInfantCount());
		}
		return traverlerInfo.getPassengerTypeQuantityList();
	}

	public String getPreferedLang() {
		return preferedLang;
	}

	public void setPreferedLang(String preferedLang) {
		this.preferedLang = preferedLang;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public void setFareQuote(FareQuoteTO fareQuote) {
		this.fareQuote = fareQuote;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public boolean isMakePayment() {
		return makePayment;
	}

	public void setMakePayment(boolean makePayment) {
		this.makePayment = makePayment;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public boolean isCheckPromotionWithPromoCode() {
		return checkPromotionWithPromoCode;
	}

	public void setCheckPromotionWithPromoCode(boolean checkPromotionWithPromoCode) {
		this.checkPromotionWithPromoCode = checkPromotionWithPromoCode;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public DiscountedFareDetails getDiscountInfo() {
		return discountInfo;
	}

	public void setDiscountInfo(DiscountedFareDetails discountInfo) {
		this.discountInfo = discountInfo;
	}

	public boolean isHasExistingPromotion() {
		return hasExistingPromotion;
	}

	public void setHasExistingPromotion(boolean hasExistingPromotion) {
		this.hasExistingPromotion = hasExistingPromotion;
	}

	public BigDecimal getExistingDiscountAmount() {
		return existingDiscountAmount;
	}

	public void setExistingDiscountAmount(BigDecimal existingDiscountAmount) {
		this.existingDiscountAmount = existingDiscountAmount;
	}

	public BigDecimal getBinPromotionDiscountAmount() {
		return binPromotionDiscountAmount;
	}

	public void setBinPromotionDiscountAmount(BigDecimal binPromotionDiscountAmount) {
		this.binPromotionDiscountAmount = binPromotionDiscountAmount;
	}

	public boolean isHasBinPromotion() {
		return hasBinPromotion;
	}

	public void setHasBinPromotion(boolean hasBinPromotion) {
		this.hasBinPromotion = hasBinPromotion;
	}

	public boolean isOverrideExistingPromotion() {
		return overrideExistingPromotion;
	}

	public void setOverrideExistingPromotion(boolean overrideExistingPromotion) {
		this.overrideExistingPromotion = overrideExistingPromotion;
	}
}
