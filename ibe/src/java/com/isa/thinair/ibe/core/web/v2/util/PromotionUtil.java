package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.ibe.api.dto.PromoChargeRewardTO;
import com.isa.thinair.ibe.api.dto.PromoMetaTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.promotion.api.to.PromoChargeTo;
import com.isa.thinair.promotion.api.to.PromotionRequestTo;
import com.isa.thinair.promotion.api.to.PromotionTemplateTo;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.api.utils.ResponceCodes;

public class PromotionUtil {


	/**
	 * Build Next seat promotion chargers for F/E from filtered segments
	 * 
	 * @param promotionRequestTo
	 * @param filteredSegs
	 * @param persistedPromoTemplate
	 * @param lowerBound
	 * @param upperBound
	 * @param isOutboundSegment
	 * @return
	 */
	public static PromotionRequestTo buildNextSeatCharges(PromotionRequestTo promotionRequestTo, List<String> filteredSegs,
			PromotionTemplateTo persistedPromoTemplate, int lowerBound, int upperBound, boolean isOutboundSegment) {

		if (isOutboundSegment) {
			promotionRequestTo.setDirection(PromotionsInternalConstants.DIRECTION_OUTBOUND);
		} else {
			promotionRequestTo.setDirection(PromotionsInternalConstants.DIRECTION_INBOUND);
		}

		List<String> promoDefinedPersistedRoutes = persistedPromoTemplate.getRoutes();
		List<PromoChargeTo> chargeList = persistedPromoTemplate.getPromoCharges();

		List<String> promoRequestedPossibleMatchingSegments = resolveMatchingSegments(filteredSegs);

		for (String promotionDefinedRoute : promoDefinedPersistedRoutes) {

			if (promoRequestedPossibleMatchingSegments.contains(promotionDefinedRoute)) {

				for (PromoChargeTo promoChargeTo : chargeList) {

					for (int j = lowerBound; j <= upperBound; j++) {

						if (promoChargeTo.getChargeCode().equals(
								PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG.getFreeSeatChargeParam(j))) {
							promotionRequestTo.addPromotionRequestParam(String.valueOf(j), promoChargeTo.getCharge());

						}

						if (promoChargeTo.getChargeCode().equals(
								PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_REG_CHRG.getFreeSeatRegistrationChargeParam(j))) {
							promotionRequestTo.addPromotionRequestParam("NF_" + String.valueOf(j), promoChargeTo.getCharge());
						}

					}

				}
				break;
			}
		}

		return promotionRequestTo;
	}

	/**
	 * Build Flexi date rewards for F/E from filtered segments
	 * 
	 * @param flexiRequestTo
	 * @param filteredSegs
	 * @param persistedPromoTemplate
	 * @param lowerBound
	 * @param upperBound
	 * @param isOutboundSegment
	 * @return
	 */
	public static PromotionRequestTo builFlexiDateRewards(PromotionRequestTo flexiRequestTo, List<String> filteredSegs,
			PromotionTemplateTo persistedPromoTemplate, int lowerBound, int upperBound, boolean isOutboundSegment) {

		if (isOutboundSegment) {
			flexiRequestTo.setDirection(PromotionsInternalConstants.DIRECTION_OUTBOUND);
		} else {
			flexiRequestTo.setDirection(PromotionsInternalConstants.DIRECTION_INBOUND);
		}

		List<PromoChargeTo> chargeList = persistedPromoTemplate.getPromoCharges();
		List<String> promoDefinedPersistedRoutes = persistedPromoTemplate.getRoutes();
		List<String> promoRequestedPossibleMatchingSegments = resolveMatchingSegments(filteredSegs);

		for (String promotionDefinedRoute : promoDefinedPersistedRoutes) {
			if (promoRequestedPossibleMatchingSegments.contains(promotionDefinedRoute)) {
				for (PromoChargeTo promoChargeTo : chargeList) {

					int diff = lowerBound + upperBound;

					for (int day = 1; day <= diff; day++) {
						if (promoChargeTo.getChargeCode().equals(
								PromoTemplateParam.ChargeAndReward.FLEXI_DATE_DAYS_REWARD.getFlexibilityRewardParam(day))) {
							StringBuilder rewardKey = new StringBuilder();
							rewardKey.append(PromoTemplateParam.ChargeAndReward.FLEXI_DATE_DAYS_REWARD.getPrefix());
							rewardKey.append(day);
							flexiRequestTo.addPromotionRequestParam(rewardKey.toString(), promoChargeTo.getCharge());

						}
					}

				}
				break;
			}
		}

		return flexiRequestTo;

	}

	public static String builFlexiDateRewardsForFE(PromoChargeRewardTO promoChargeRewardTo) throws ModuleException {

		BigDecimal outboundReward = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal inboundReward = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (isNullSafe(promoChargeRewardTo.getOutboundLower()) || isNullSafe(promoChargeRewardTo.getOutboundUpper())) {
			outboundReward = flexiDateRewardCalculator(promoChargeRewardTo.getPromotionId(),
					promoChargeRewardTo.getOutboundSegCodes(), promoChargeRewardTo.getOutboundLower(),
					promoChargeRewardTo.getOutboundUpper());
		}

		if (isNullSafe(promoChargeRewardTo.getInboundLower()) || isNullSafe(promoChargeRewardTo.getInboundUpper())) {
			inboundReward = flexiDateRewardCalculator(promoChargeRewardTo.getPromotionId(),
					promoChargeRewardTo.getInboundSegCodes(), promoChargeRewardTo.getInboundLower(),
					promoChargeRewardTo.getInboundUpper());
		}
		BigDecimal totalReward = AccelAeroCalculator.add(outboundReward, inboundReward);
		String baseCurrency = AppSysParamsUtil.getBaseCurrency();
		return totalReward.toString() + " " + baseCurrency;

	}

	private static BigDecimal flexiDateRewardCalculator(String promotionId, String boundSegmentCodes, String lowerLimit,
			String upperLimit) throws ModuleException {

		BigDecimal boundCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		String[] boundSegments = boundSegmentCodes.split(",");

		int lowerBound = 0;
		int upperBound = 0;

		if (isNullSafe(lowerLimit)) {
			lowerBound = Integer.parseInt(lowerLimit);
		}
		if (isNullSafe(upperLimit)) {
			upperBound = Integer.parseInt(upperLimit);
		}
		int diff = lowerBound + upperBound;

		if (diff != 0) {

			PromotionTemplateTo persistedPromoTemplate = ModuleServiceLocator.getPromotionManagementBD().loadPromotionTemplateTo(
					Integer.valueOf(promotionId));
			List<PromoChargeTo> chargeList = persistedPromoTemplate.getPromoCharges();
			List<String> promoDefinedPersistedRoutes = persistedPromoTemplate.getRoutes();

			List<String> promoRequestedSegments = new ArrayList<String>();
			for (String segmentCode : boundSegments) {
				promoRequestedSegments.add(segmentCode);
			}
			List<String> promoRequestedPossibleMatchingSegments = resolveMatchingSegments(promoRequestedSegments);

			for (String promotionDefinedRoute : promoDefinedPersistedRoutes) {

				if (promoRequestedPossibleMatchingSegments.contains(promotionDefinedRoute)) {

					for (PromoChargeTo promoChargeTo : chargeList) {

						if (promoChargeTo.getChargeCode().equals(
								PromoTemplateParam.ChargeAndReward.FLEXI_DATE_DAYS_REWARD.getFlexibilityRewardParam(diff))) {
							boundCharge = AccelAeroCalculator.getTwoScaledBigDecimalFromString(promoChargeTo.getCharge());

						}

					}
					break;
				}
			}
		}

		return boundCharge;

	}

	/**
	 * Compose promotion meta info which is required in F/E
	 * 
	 * Filtering Criteria: confirmed, future, promotion applicable pnr_segements
	 * 
	 * @param lccClientReservationSegmnets
	 * @param persistedPromoTemplate
	 * @param metaTo
	 * @param inbound
	 * @param trackInfoDTO TODO
	 * @return
	 * @throws ModuleException
	 */
	public static String composePromotionFEMetaInfo(Set<LCCClientReservationSegment> lccClientReservationSegmnets,
			PromotionTemplateTo persistedPromoTemplate, PromoMetaTO metaTo, boolean inbound, TrackInfoDTO trackInfoDTO) throws ModuleException {

		StringBuilder pnrSegIds = new StringBuilder();
		StringBuilder segmentShortCode = new StringBuilder();
		StringBuilder segmentDisplayCode = new StringBuilder();

		List<String> promoDefinedRoutes = persistedPromoTemplate.getRoutes();
		List<String> promoRequestedFilterdSegCodes = new ArrayList<String>();
		List<LCCClientReservationSegment> promoRequestedFliterdResSegments = new ArrayList<LCCClientReservationSegment>();

		for (LCCClientReservationSegment lccClientReservationSegment : SortUtil.sort(lccClientReservationSegmnets)) {
			if (isSatisfySegmentLevelConstrains(lccClientReservationSegment, trackInfoDTO)) {
				promoRequestedFilterdSegCodes.add(lccClientReservationSegment.getSegmentCode());
				promoRequestedFliterdResSegments.add(lccClientReservationSegment);
			}
		}

		if (promoRequestedFilterdSegCodes.isEmpty()) {
			throw new ModuleException(ResponceCodes.PROMOTION_REQUEST_OUT_OF_VALIDITY);
		}

		List<String> promoRequestedPossibleMatchingSegments = resolveMatchingSegments(promoRequestedFilterdSegCodes);
		boolean firstCounted = false;
		String firstDate = "";

		for (LCCClientReservationSegment resSegment : promoRequestedFliterdResSegments) {
			for (String possibleMatchingSegment : promoRequestedPossibleMatchingSegments) {

				if (promoDefinedRoutes.contains(possibleMatchingSegment)
						&& isBetweenPromoValidity(resSegment, persistedPromoTemplate)) {

					if (pnrSegIds.length() > 0) {
						pnrSegIds.append(",");
					}
					if (segmentShortCode.length() > 0) {
						segmentShortCode.append(",");
						segmentDisplayCode.append(" & ");
					}

					pnrSegIds.append(resSegment.getPnrSegID());

					segmentShortCode.append(resSegment.getSegmentCode());
					segmentDisplayCode.append(ReservationHG.getSegementDetailsWithTermainals(resSegment.getSegmentCode(),
							resSegment.getSubStationShortName(), resSegment.getDepartureTerminalName(),
							resSegment.getArrivalTerminalName()));

					if (!firstCounted) {
						firstDate = getFormatedDate(resSegment.getDepartureDate().getTime());
						firstCounted = true;
					}

					if (inbound) {
						metaTo.getInSegList().add(resSegment.getSegmentCode());
					} else {
						metaTo.getOutSegList().add(resSegment.getSegmentCode());
					}

					break;
				}
			}
		}

		if (inbound) {
			metaTo.setInboudDefined(firstCounted);
			metaTo.setInboundSegmentIds(pnrSegIds.toString());
			metaTo.setInboundSegmentCode(segmentShortCode.toString());
			metaTo.setInboundSegDisplayName(segmentDisplayCode.toString());
			metaTo.setInboundDepartureDate(firstDate);

		} else {
			metaTo.setOutboundDefined(firstCounted);
			metaTo.setOutboundSegmentIds(pnrSegIds.toString());
			metaTo.setOutboundSegmentCode(segmentShortCode.toString());
			metaTo.setOutboundSegDisplayName(segmentDisplayCode.toString());
			metaTo.setOutboundDepartureDate(firstDate);
		}
		return pnrSegIds.toString();
	}

	private static boolean isSatisfySegmentLevelConstrains(LCCClientReservationSegment lccClientReservationSegment, TrackInfoDTO trackInfoDTO) {
		boolean satisfy = false;
		satisfy = lccClientReservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
				&& lccClientReservationSegment.getSystem().equals(SYSTEM.AA)
				&& lccClientReservationSegment.getStatusModifiedChannelCode().equals(trackInfoDTO.getOriginChannelId());
		if (satisfy) {
			long deptTime = lccClientReservationSegment.getDepartureDateZulu().getTime();
			long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
			if (currentTime > deptTime) {
				satisfy = false;
			}
		}
		return satisfy;
	}

	private static boolean isBetweenPromoValidity(LCCClientReservationSegment resSegment, PromotionTemplateTo promo)
			throws ModuleException {
		boolean valid = false;
		valid = CalendarUtil.isBetweenIncludingLimits(CalendarUtil.getStartTimeOfDate(resSegment.getDepartureDateZulu()),
				promo.getPromoStartDate(), promo.getPromoEndDate());
		if (valid && promo.getPromotionTypeId() == PromotionType.PROMOTION_NEXT_SEAT_FREE.getPromotionId()) {
			valid = isSeatMapAvailableForFlightSegment(resSegment.getFlightSegmentRefNumber());
		}
		return valid;
	}

	private static boolean isSeatMapAvailableForFlightSegment(String flightRefNo) throws ModuleException {
		boolean seatMapAvailable = false;
		Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNo);
		if (flightSegId != null) {
			seatMapAvailable = ModuleServiceLocator.getFlightBD().isSeatMapAvailableForFlightSegement(flightSegId);
		}
		return seatMapAvailable;
	}

	private static List<String> resolveMatchingSegments(List<String> promoRequestedSegs) {

		String routeCode = PromotionsUtils.getOndCode(promoRequestedSegs);

		Pair<String, String> origAndDest = PromotionsUtils.getOriginAndDestinationAirports(routeCode);
		List<String> matchingOrder = new ArrayList<String>();

		if (promoRequestedSegs.size() == 1) {
			// For Single-Seg ---- A/B, Multi seg
			// A/B > A/ALL > ALL/B > ALL/ALL
			matchingOrder.add(routeCode);
			matchingOrder
					.add(origAndDest.getLeft() + PromotionsInternalConstants.OND_DELIM + PromotionsInternalConstants.OND_ALL);
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ origAndDest.getRight());
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);

		} else {
			// For Connection-Seg ---- A/B + B/C
			// A/B/C > ALL/ALL
			matchingOrder.add(routeCode);
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);

		}
		return matchingOrder;

	}

	public static Integer[] extractPnrPaxIds(String travelerRefNumbers) {
		// travelerRefNumbers = "G9|A1$30665660,G9|A2$30665661,G9|A3$30665659";
		String TRAVELLER_SEPERATOR = ",";
		String[] travelRefArray = travelerRefNumbers.split(TRAVELLER_SEPERATOR);
		Integer[] pnrPaxIds = new Integer[travelRefArray.length];
		for (int i = 0; i < travelRefArray.length; i++) {
			pnrPaxIds[i] = PaxTypeUtils.getPnrPaxId(travelRefArray[i]);
		}
		return pnrPaxIds;
	}

	private static String getFormatedDate(long longDate) {
		SimpleDateFormat formatter = new SimpleDateFormat(CalendarUtil.PATTERN1);
		Date date = new Date(longDate);
		return formatter.format(date);
	}

	public static String resolvePromotionType(String promotionId) throws ModuleException {
		PromotionTemplateTo persistedPromoTemplate;

		persistedPromoTemplate = ModuleServiceLocator.getPromotionManagementBD().loadPromotionTemplateTo(
				Integer.parseInt(promotionId));
		int promotinTypeId = persistedPromoTemplate.getPromotionTypeId();

		if (promotinTypeId == PromotionType.PROMOTION_NEXT_SEAT_FREE.getPromotionId()) {
			return "next";
		} else if (promotinTypeId == PromotionType.PROMOTION_FLEXI_DATE.getPromotionId()) {
			return "flexi";
		}

		return "";
	}

	public static boolean isNullSafe(String limit) {
		return (limit != null && !limit.isEmpty());
	}

	// FIX: Relate to PRCA - throw a runtime exception, if the payment values from frontend is not matched with backend
	// [track hacks]
	public static String retrieveNextSeatCharge(PromotionRequestTo promoRequsetTO) throws ModuleException {

		PromotionTemplateTo promotionTemplate = ModuleServiceLocator.getPromotionManagementBD().loadPromotionTemplateTo(
				promoRequsetTO.getPromotionId());
		List<PromoChargeTo> promotionChargers = promotionTemplate.getPromoCharges();

		String strSeatRequested = promoRequsetTO.getPromotionRequestParams().get(PromoRequestParam.FreeSeat.SEATS_TO_FREE);
		int seatRequested = Integer.parseInt(strSeatRequested);

		for (PromoChargeTo promoCharge : promotionChargers) {
			if (promoCharge.getChargeCode().equals(
					PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG.getFreeSeatChargeParam(seatRequested))) {
				return promoCharge.getCharge();
			}
		}

		return null;
	}

}