package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.JSONObject;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.ibe.api.dto.BookingTO;
import com.isa.thinair.ibe.api.dto.DisplayUtilDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostDTO;
import com.isa.thinair.ibe.api.dto.PaxTO;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.api.dto.SocialTO;
import com.isa.thinair.ibe.core.service.IBEConfig;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants.Mode;
import com.isa.thinair.ibe.core.web.v2.util.BalanceSummaryUtil;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.FlyingSocialUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ItineraryUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.FlexiDTO;
import com.isa.thinair.webplatform.api.dto.HolidaysLinkParamsTO;
import com.isa.thinair.webplatform.api.v2.ancillary.ItineraryAnciAvailability;
import com.isa.thinair.webplatform.core.commons.MapGenerator;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineItineraryAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(InterlineItineraryAction.class);
	
	private static IBEConfig ibeConfig = IBEModuleUtils.getConfig();

	private CommonReservationContactInfo contactInfo;

	private boolean success = true;

	private BookingTO bookingTO;

	private Collection<PaxTO> colAdultCollection = new ArrayList<PaxTO>();

	private Collection<PaxTO> colChildCollection = new ArrayList<PaxTO>();

	private Collection<PaxTO> colInfantCollection = new ArrayList<PaxTO>();

	private Collection<SegInfoDTO> flightDepDetails;

	private Collection<SegInfoDTO> flightRetDetails;

	private FareQuoteTO fareQuote;

	private HashMap<String, String> jsonLabel;

	private int adultCount = 0;

	private int childCount = 0;

	private int infantCount = 0;
	
	private Set<LCCClientReservationSegment> segments;

	private String language;

	private String emailLanguage;

	private String messageTxt;

	private String hotelLink;

	private String carLink;

	private boolean isCarLinkDynamic;

	private boolean isHotelLinkLoadWithinFrame;

	@Deprecated
	private LCCClientReservationInsurance insurance;
	
	private List<LCCClientReservationInsurance> insurances;

	private ItineraryAnciAvailability ancillaryAvailability;

	private boolean isGroupPNR;

	private static final int ASCII_A = 64;

	private String strRandom;

	private List<String[]> reservationTransItemInfo;

	private BigDecimal[] amountsInGoogleAnlCurrency;

	private String policyNo;

	private String mode;

	/* Hold Flexi information */
	private Collection<FlexiDTO> flexiInfo;

	private SessionTimeoutDataDTO timeoutDTO;

	private String outboundFlightDuration;

	private String inboundFlightDuration;

	private String carrierCode;
	
	private String expiryDays;

	private static BigDecimal totalAncillaryCharge = BigDecimal.ZERO;

	private Collection<DisplayUtilDTO> balanceSummary;

	private boolean allowIndivPrint;

	private IPGTransactionResultDTO ipgTransactionResultDTO;

	private SocialTO socialTo = new SocialTO();
	
	private JSONObject reservationCookieDetails  = new JSONObject();
	
	private boolean isSaveCookieEnable;

	/**
	 * Variable to denote to the view whether a successful refund occurred
	 */
	private boolean successfulRefund;
	private boolean itineraryEmailGenerated;

	private boolean creditPromo;
	private boolean moneyPromo;
	private boolean displayCreditCardProofLable;

	@SuppressWarnings("unchecked")
	public String execute() {
		if (log.isDebugEnabled()) {
			log.debug("### InterlineItineraryAction start... ###");
		}

		String forward = StrutsConstants.Result.SUCCESS;
		String strPNRNo = "";
		try {
			// need to assign the priviledge for individual print
			allowIndivPrint = AppSysParamsUtil.isPrintEmailIndItinerary();

			IBEReservationPostDTO ibeReservationPostDTO = SessionUtil.getIbeReservationPostDTO(request);
			// Set Session Attribute for track user itinerary confirmation page refresh
			request.getSession().setAttribute("fromItinearyPage", true);
			language = SessionUtil.getLanguage(request);
			timeoutDTO = ReservationUtil.fetchSessionTimeoutData(request);
			String[] pagesIDs = { "Common", "PgInterlineConfirm" };
			this.jsonLabel = I18NUtil.getMessagesInJSON(language, pagesIDs);
			strPNRNo = ibeReservationPostDTO.getPnr();

			successfulRefund = ibeReservationPostDTO.isRefundSuccessful();

			itineraryEmailGenerated = ibeReservationPostDTO.isItineraryEmailGenerated();
			displayCreditCardProofLable = ibeConfig.isDisplayCreditCardProofLable();

			SYSTEM system = ibeReservationPostDTO.getSelectedSystem();
			this.isGroupPNR = (system == SYSTEM.INT);
			if (log.isDebugEnabled())
				log.debug("### InterlineItineraryAction before load reservation... ###");
			Customer cust = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			LCCClientReservation reservation = ReservationUtil.loadProxyReservation(strPNRNo, this.isGroupPNR, getTrackInfo(),
					false, (cust != null) ? true : false, null, null, AppSysParamsUtil.isPromoCodeEnabled());

			bookingTO = ReservationUtil.createBookingDetails(reservation);
			Map<String, String> mapNationality = MapGenerator.getNationalityMap();
			Map<String, String> mapCountry = MapGenerator.getCountryMap();

			insurances = reservation.getReservationInsurances();

			Object[] paxCol = ItineraryUtil.createPassengerList(reservation);
			this.colAdultCollection = (Collection<PaxTO>) paxCol[0];
			this.colChildCollection = (Collection<PaxTO>) paxCol[1];
			this.colInfantCollection = (Collection<PaxTO>) paxCol[2];

			this.ancillaryAvailability = ItineraryUtil.getAncillaryAvailability(paxCol);
			contactInfo = reservation.getContactInfo();
			carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			// Process segments and identify inbound and outbound
			Object[] colResSegments = BeanUtil.getOutboundInboundList(reservation.getSegments(), true);
			flightDepDetails = BeanUtil.getFlightLists((Set<LCCClientReservationSegment>) colResSegments[0], new Locale(
					CommonUtil.getJavaSupportLocale(language)));
			flightRetDetails = BeanUtil.getFlightLists((Set<LCCClientReservationSegment>) colResSegments[1], new Locale(
					CommonUtil.getJavaSupportLocale(language)));

			if (flightDepDetails != null) {
				outboundFlightDuration = ReservationUtil.getFlightDuration((List) flightDepDetails);
			}
			if (flightRetDetails != null) {
				inboundFlightDuration = ReservationUtil.getFlightDuration((List) flightRetDetails);
			}

			Collection<SegInfoDTO> depRetFlights = new ArrayList<SegInfoDTO>();
			if (flightDepDetails != null) {
				depRetFlights.addAll(flightDepDetails);
			}
			if (flightRetDetails != null) {
				depRetFlights.addAll(flightRetDetails);
			}
			flexiInfo = ReservationUtil.getFlexiInfoForSegments(depRetFlights, reservation.getFlexiAlertInfoTOs(), language);

			this.adultCount = reservation.getTotalPaxAdultCount();
			this.childCount = reservation.getTotalPaxChildCount();
			this.infantCount = reservation.getTotalPaxInfantCount();
			
			this.segments = reservation.getSegments();

			if (log.isDebugEnabled())
				log.debug("### InterlineItineraryAction after load reservation... ###");

			if (JavaScriptGenerator.nullConvertToString(contactInfo.getNationalityCode()) != "") {
				contactInfo.setNationality((String) mapNationality.get(contactInfo.getNationalityCode().toString()));
			}
			
			expiryDays = AppSysParamsUtil.getNoOfCookieExpiryDays();

			balanceSummary = BalanceSummaryUtil.getReservationBalanceSummay(reservation, AppSysParamsUtil.getBaseCurrency(),
					SessionUtil.getLanguage(request));
			fareQuote = BalanceSummaryUtil.fillFareQuote(reservation);

			if (JavaScriptGenerator.nullConvertToString(contactInfo.getCountryCode()) != "") {
				contactInfo.setCountryCode((String) mapCountry.get(contactInfo.getCountryCode()));
			}
			if (log.isDebugEnabled())
				log.debug("### InterlineItineraryAction prepare display data... ###");

			this.emailLanguage = reservation.getPreferrenceInfo().getPreferredLanguage();
			// Hotel link url
			HolidaysLinkParamsTO holidaysLinkParamsTO = new HolidaysLinkParamsTO();
			holidaysLinkParamsTO.setLanguage(language);
			holidaysLinkParamsTO.setPassengers(reservation.getPassengers());
			holidaysLinkParamsTO.setSegmentView(reservation.getSegments());
			this.hotelLink = ReservationHG.createHolidaysLink(holidaysLinkParamsTO);
			this.carLink = ReservationHG.createRentACarLink(language, flightDepDetails, flightRetDetails,
					reservation.getLastCurrencyCode(), reservation.getPNR());
			this.isCarLinkDynamic = AppSysParamsUtil.isRentACarLinkDynamic();
			this.isHotelLinkLoadWithinFrame = AppSysParamsUtil.isHotelLinkLoadWithinFrame();

			// Data to send for google analytics e-commerce transactions
			this.strRandom = getRandomNumber();
			BigDecimal[] semantics = getGoogleAnalyticsCurrencySemantics();
			this.reservationTransItemInfo = createGoogleDataInfo(reservation, new Locale(language), semantics);
			populateGoogleCurrencyArray(fareQuote, semantics);

			if (insurance != null) {
				this.policyNo = insurance.getPolicyCode();
			}
			if (ibeReservationPostDTO.getMode() == Mode.ADD_MODIFY_ANCILARY
					|| ibeReservationPostDTO.getMode() == Mode.MODIFY_SEGMENT) {
				this.mode = "MOD";
			} else {
				this.mode = "CRE";
				loggingGoogleDataInfo();
			}
			bookingTO.setInsuranceType(ReservationUtil.getInsuranceSelectionText(reservation, language));
			ipgTransactionResultDTO = ibeReservationPostDTO.getIpgTransactionResultDTO();
			FlyingSocialUtil.displaySocialLiveFeeds(socialTo, flightDepDetails);

			if (reservation.getLccPromotionInfoTO() != null) {
				LCCPromotionInfoTO lccPromotionInfoTO = reservation.getLccPromotionInfoTO();
				if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
					creditPromo = true;
				} else if (DiscountApplyAsTypes.MONEY.equals(lccPromotionInfoTO.getDiscountAs())) {
					moneyPromo = true;
				}
			}
			
			if(AppSysParamsUtil.isCookieSaveFunctionalityEnabled()){
				this.setSaveCookieEnable(AppSysParamsUtil.isCookieSaveFunctionalityEnabled());
				this.reservationCookieDetails = ReservationUtil.getReservationCookieDetails(reservation);
			}

		} catch (Exception me) {
			log.info(
					"###  InterlineItineraryAction: ### PNR : " + strPNRNo + "###  Session ID:" + request.getRequestedSessionId()
							+ "###", me);
			log.error(
					"#### Critical issue. User can not view reservation. Please immediately fix this issue.##### InterlineItineraryAction==>"
							+ me, me);
			success = false;
			messageTxt = CommonUtil.getExistingReservationMsg(request, strPNRNo);
		}
		if (log.isDebugEnabled()) {
			log.debug("### InterlineItineraryAction end... ###");
		}

		return forward;
	}

	private void loggingGoogleDataInfo() {
		if (log.isInfoEnabled()) {
			StringBuffer gaLogSB = new StringBuffer();
			gaLogSB.append("Start Logging Google Analytics Data Info for: " + this.bookingTO.getPNR());
			for (String[] item : this.reservationTransItemInfo) {

				for (int i = 0; i < item.length; i++) {
					switch (i) {
					case 0:
						gaLogSB.append("| sku: " + item[0].toString());
						break;
					case 1:
						gaLogSB.append("| segmentDetails: " + item[1].toString());
						break;
					case 2:
						gaLogSB.append("| productType: " + item[2].toString());
						break;
					case 3:
						gaLogSB.append("| amount: " + item[3].toString());
						break;
					case 4:
						gaLogSB.append("| quantity: " + item[4].toString());
						break;
					default:
						gaLogSB.append("| ");
						break;
					}
				}
			}
			gaLogSB.append(CommonsConstants.NEWLINE);
			for (int i = 0; i < this.amountsInGoogleAnlCurrency.length; i++) {
				switch (i) {
				case 0:
					gaLogSB.append("| Total Fare: " + this.amountsInGoogleAnlCurrency[0].toString());
					break;
				case 1:
					gaLogSB.append("| Total Tax: " + this.amountsInGoogleAnlCurrency[1].toString());
					break;
				case 2:
					gaLogSB.append("| Insurance Charge: " + this.amountsInGoogleAnlCurrency[2].toString());
					break;
				case 3:
					gaLogSB.append("| Total Fee: " + this.amountsInGoogleAnlCurrency[3].toString());
					break;
				case 4:
					gaLogSB.append("| Total Price: " + this.amountsInGoogleAnlCurrency[4].toString());
					break;
				case 5:
					gaLogSB.append("| surcharges Amount: " + this.amountsInGoogleAnlCurrency[5].toString());
					break;
				default:
					gaLogSB.append("| ");
					break;
				}
			}
			gaLogSB.append(CommonsConstants.NEWLINE + "Stop Logging Google Analytics Data Info for: " + this.bookingTO.getPNR());
			log.info(gaLogSB.toString());
		}
	}

	private void populateGoogleCurrencyArray(FareQuoteTO fareQuote, BigDecimal[] semantics) {
		this.amountsInGoogleAnlCurrency = new BigDecimal[6];
		for (int i = 0; i < this.amountsInGoogleAnlCurrency.length; i++) {
			this.amountsInGoogleAnlCurrency[i] = BigDecimal.ZERO;
		}
		if (fareQuote.isHasFareQuote()) {
			this.amountsInGoogleAnlCurrency[0] = convertToGoogleAnalyticsCurrency(new BigDecimal(fareQuote.getTotalFare()),
					semantics);
			this.amountsInGoogleAnlCurrency[1] = convertToGoogleAnalyticsCurrency(new BigDecimal(fareQuote.getTotalTax()),
					semantics);
		}
		if (fareQuote.isHasInsurance()) {
			this.amountsInGoogleAnlCurrency[2] = convertToGoogleAnalyticsCurrency(new BigDecimal(fareQuote.getInsuranceCharge()),
					semantics);
		}
		if (fareQuote.isHasFee()) {
			this.amountsInGoogleAnlCurrency[3] = convertToGoogleAnalyticsCurrency(new BigDecimal(fareQuote.getTotalFee()),
					semantics);
		}
		if (fareQuote.getTotalPrice() != null) {
			this.amountsInGoogleAnlCurrency[4] = convertToGoogleAnalyticsCurrency(new BigDecimal(fareQuote.getTotalPrice()),
					semantics);
		}
		this.amountsInGoogleAnlCurrency[5] = AccelAeroCalculator.subtract(this.amountsInGoogleAnlCurrency[4], AccelAeroCalculator
				.add(this.amountsInGoogleAnlCurrency[0], this.amountsInGoogleAnlCurrency[1], this.amountsInGoogleAnlCurrency[2],
						this.amountsInGoogleAnlCurrency[3], totalAncillaryCharge));
	}

	private static String getRandomNumber() throws ModuleException {
		StringBuffer strkey = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd/HH/mm/ss");
		Date dt = new Date();
		String wt = sdf.format(dt);
		String date[] = StringUtils.split(wt, '/');
		int y = Integer.parseInt(date[1]) + ASCII_A;
		int m = Integer.parseInt(date[2]) + ASCII_A;
		strkey.append(String.valueOf((char) y) + String.valueOf((char) m));
		strkey.append(date[3] + date[4]);

		return strkey.toString();
	}

	private static List<String[]> createGoogleDataInfo(LCCClientReservation reservation, Locale locale, BigDecimal[] semantics) {

		Set<LCCClientReservationPax> paxList = reservation.getPassengers();
		List<String[]> ancillaryList = new ArrayList<String[]>();
		HashMap<String, List<LCCMealDTO>> mealsMap = new HashMap<String, List<LCCMealDTO>>();
		HashMap<String, List<LCCSpecialServiceRequestDTO>> ssrMap = new HashMap<String, List<LCCSpecialServiceRequestDTO>>();

		for (LCCClientReservationPax reservationPax : paxList) {
			List<LCCSelectedSegmentAncillaryDTO> lCCSelectedSegmentAncillaryDTOList = reservationPax.getSelectedAncillaries();
			for (LCCSelectedSegmentAncillaryDTO lCCSelectedSegmentAncillaryDTO : lCCSelectedSegmentAncillaryDTOList) {
				if ((lCCSelectedSegmentAncillaryDTO.getAirSeatDTO() != null)
						&& (lCCSelectedSegmentAncillaryDTO.getAirSeatDTO().getSeatNumber() != null)) {
					String segmentDetails = ReservationHG.getSegementDetails(lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO()
							.getSegmentCode());
					String flightSegmentCode = lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode();
					LCCAirSeatDTO lccAirSeatDto = lCCSelectedSegmentAncillaryDTO.getAirSeatDTO();
					ancillaryList.add(createSeatInfo(lccAirSeatDto, segmentDetails, flightSegmentCode, semantics));
				}
				if (lCCSelectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs() != null) {
					List<LCCSpecialServiceRequestDTO> lCCSpecialServiceRequestDTOList = new ArrayList<LCCSpecialServiceRequestDTO>(
							lCCSelectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs());
					Collections.copy(lCCSpecialServiceRequestDTOList,
							lCCSelectedSegmentAncillaryDTO.getSpecialServiceRequestDTOs());
					if (ssrMap.get(lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode()) == null) {
						ssrMap.put(lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode(),
								lCCSpecialServiceRequestDTOList);
					} else {
						ssrMap.get(lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode()).addAll(
								lCCSpecialServiceRequestDTOList);
					}
				}
				if (lCCSelectedSegmentAncillaryDTO.getMealDTOs() != null) {
					List<LCCMealDTO> mealList = new ArrayList<LCCMealDTO>(lCCSelectedSegmentAncillaryDTO.getMealDTOs());
					Collections.copy(mealList, lCCSelectedSegmentAncillaryDTO.getMealDTOs());
					if (mealsMap.get(lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode()) == null) {
						mealsMap.put(lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode(), mealList);
					} else {
						mealsMap.get(lCCSelectedSegmentAncillaryDTO.getFlightSegmentTO().getSegmentCode()).addAll(mealList);
					}
				}
			}
		}

		Object[] arr = BeanUtil.createFlightLists(reservation.getSegments(), locale);
		Collection<SegInfoDTO> reservationSegments = (Collection<SegInfoDTO>) arr[0];
		for (SegInfoDTO segInfoDTO : reservationSegments) {
			HashMap<LCCMealDTO, Integer> mealCountMap = getMealInfoForReservation(mealsMap.get(segInfoDTO.getSegmentShortCode()));
			HashMap<LCCSpecialServiceRequestDTO, Integer> ssrCountMap = getSsrInfoForReservation(ssrMap.get(segInfoDTO
					.getSegmentShortCode()));
			String segmentDetails = ReservationHG.getSegementDetails(segInfoDTO.getSegmentShortCode());
			String flightSegmentCode = segInfoDTO.getSegmentShortCode();

			for (Entry<LCCMealDTO, Integer> mealCountEntry : mealCountMap.entrySet()) {
				ancillaryList.add(createMealInfo(mealCountEntry, segmentDetails, flightSegmentCode, semantics));
			}
			for (Entry<LCCSpecialServiceRequestDTO, Integer> ssrCountEntry : ssrCountMap.entrySet()) {
				ancillaryList.add(createASInfo(ssrCountEntry, segmentDetails, flightSegmentCode, semantics));
			}
		}
		BigDecimal totalAncillaryAmount = BigDecimal.ZERO;
		for (String[] ancillary : ancillaryList) {
			if (!ancillary[3].equals("") && !ancillary[4].equals("")) {
				totalAncillaryAmount = AccelAeroCalculator.add(totalAncillaryAmount,
						AccelAeroCalculator.multiply(new BigDecimal(ancillary[3]), new BigDecimal(ancillary[4])));
			}
		}
		totalAncillaryCharge = totalAncillaryAmount;
		return ancillaryList;
	}

	private static HashMap<LCCSpecialServiceRequestDTO, Integer> getSsrInfoForReservation(
			Collection<LCCSpecialServiceRequestDTO> ssr) {
		HashMap<LCCSpecialServiceRequestDTO, Integer> ssrCountMap = new HashMap<LCCSpecialServiceRequestDTO, Integer>();

		if (ssr != null) {
			Set<String> ssrIds = new HashSet<String>();
			for (LCCSpecialServiceRequestDTO ssrTo : ssr) {
				ssrIds.add(ssrTo.getSsrCode());
			}

			for (String ssrId : ssrIds) {
				int ssrCount = getSsrCount(ssr, ssrId);
				LCCSpecialServiceRequestDTO lccSsrTO = getLccSsrTOForMealId(ssr, ssrId);

				ssrCountMap.put(lccSsrTO, ssrCount);
			}
		}
		return ssrCountMap;
	}

	private static HashMap<LCCMealDTO, Integer> getMealInfoForReservation(Collection<LCCMealDTO> meals) {
		HashMap<LCCMealDTO, Integer> mealCountMap = new HashMap<LCCMealDTO, Integer>();

		if (meals != null) {
			Set<String> mealIds = new HashSet<String>();
			for (LCCMealDTO mealsTo : meals) {
				mealIds.add(mealsTo.getMealCode());
			}

			for (String mealId : mealIds) {
				int mealCount = getMealCount(meals, mealId);
				LCCMealDTO lccMealTO = getLccMealTOForMealId(meals, mealId);

				mealCountMap.put(lccMealTO, mealCount);
			}
		}
		return mealCountMap;
	}

	private static LCCMealDTO getLccMealTOForMealId(Collection<LCCMealDTO> meals, String mealId) {
		LCCMealDTO lccMealDTO = null;
		for (LCCMealDTO mealsTo : meals) {
			if (mealId.equals(mealsTo.getMealCode())) {
				lccMealDTO = mealsTo;
				break;
			}
		}
		return lccMealDTO;
	}

	private static LCCSpecialServiceRequestDTO getLccSsrTOForMealId(Collection<LCCSpecialServiceRequestDTO> ssr, String ssrId) {
		LCCSpecialServiceRequestDTO lccSsrDTO = null;
		for (LCCSpecialServiceRequestDTO ssrTo : ssr) {
			if (ssrId.equals(ssrTo.getSsrCode())) {
				lccSsrDTO = ssrTo;
				break;
			}
		}
		return lccSsrDTO;
	}

	private static int getMealCount(Collection<LCCMealDTO> meals, String mealCode) {
		int mealCount = 0;

		for (LCCMealDTO mealsTo : meals) {
			if (mealCode.equals(mealsTo.getMealCode())) {
				mealCount = mealCount + 1;
			}
		}
		return mealCount;
	}

	private static int getSsrCount(Collection<LCCSpecialServiceRequestDTO> ssr, String ssrCode) {
		int ssrCount = 0;

		for (LCCSpecialServiceRequestDTO ssrTo : ssr) {
			if (ssrCode.equals(ssrTo.getSsrCode())) {
				ssrCount = ssrCount + 1;
			}
		}
		return ssrCount;
	}

	private static String[] createSeatInfo(LCCAirSeatDTO lccAirSeatDto, String segmentDetails, String flightSegmentCode,
			BigDecimal[] semantics) {

		String[] seatInfo = new String[5];
		seatInfo[0] = "SEAT/" + flightSegmentCode + "/" + lccAirSeatDto.getSeatNumber();
		seatInfo[1] = segmentDetails;
		seatInfo[2] = "Seat Selection";
		if (lccAirSeatDto.getSeatCharge() != null) {
			seatInfo[3] = convertToGoogleAnalyticsCurrency(lccAirSeatDto.getSeatCharge(), semantics).toString();
		} else {
			seatInfo[3] = "";
		}
		seatInfo[4] = "1";
		return seatInfo;
	}

	private static String[] createMealInfo(Entry<LCCMealDTO, Integer> mealCountEntry, String segmentDetails,
			String flightSegmentCode, BigDecimal[] semantics) {

		String[] mealInfo = new String[5];
		mealInfo[0] = "MEAL/" + flightSegmentCode + "/" + mealCountEntry.getKey().getMealCode() + "/"
				+ mealCountEntry.getKey().getMealName();
		mealInfo[1] = segmentDetails;
		mealInfo[2] = "Meal Selection";
		if (mealCountEntry.getKey().getMealCharge() != null) {
			mealInfo[3] = convertToGoogleAnalyticsCurrency(mealCountEntry.getKey().getMealCharge(), semantics).toString();
		} else {
			mealInfo[3] = "";
		}
		mealInfo[4] = mealCountEntry.getValue().toString();
		return mealInfo;
	}

	private static String[] createASInfo(Entry<LCCSpecialServiceRequestDTO, Integer> ssrCountEntry, String segmentDetails,
			String flightSegmentCode, BigDecimal[] semantics) {

		String[] asInfo = new String[5];
		asInfo[0] = "SSR/" + flightSegmentCode + "/" + ssrCountEntry.getKey().getSsrCode() + "/"
				+ ssrCountEntry.getKey().getDescription();
		asInfo[1] = segmentDetails;
		asInfo[2] = "Airport Services";
		if (ssrCountEntry.getKey().getCharge() != null) {
			asInfo[3] = convertToGoogleAnalyticsCurrency(ssrCountEntry.getKey().getCharge(), semantics).toString();
		} else {
			asInfo[3] = "";
		}
		asInfo[4] = ssrCountEntry.getValue().toString();
		return asInfo;
	}

	private static BigDecimal[] getGoogleAnalyticsCurrencySemantics() {

		// decide the payment currency
		String payCurrency = AppSysParamsUtil.getBaseCurrency();
		// String selCurrency = "AED";
		String selCurrency = AppSysParamsUtil.getGoogleAnalyticsCurrency();
		// exchange date effective time is now.
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate selCurCurrentExRate = null;
		try {
			// pay currency exchange rate, this will be used to get break point
			// and boundary
			selCurCurrentExRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency, ApplicationEngine.IBE);
		} catch (ModuleException e) {
			// do nothing, payment currency will be default payment currency
		}

		BigDecimal boundary = null;
		BigDecimal breakPoint = null;
		// if currency rounding is enabled then get the boundary and break point
		// for payment currency,
		// else keep payBoundary and payBreakPoint null which will add default
		// two decimal round up.
		if (AppSysParamsUtil.isCurrencyRoundupEnabled()) {
			if (selCurCurrentExRate != null) {
				boundary = selCurCurrentExRate.getCurrency().getBoundryValue();
				breakPoint = selCurCurrentExRate.getCurrency().getBreakPoint();
			}
		}

		BigDecimal viewCurrMultiExRate = null;
		try {
			viewCurrMultiExRate = exchangeRateProxy.getExchangeRate(payCurrency, selCurrency);
		} catch (ModuleException e) {
			// do nothing, payment currency will be default payment currency
		}

		return new BigDecimal[] { viewCurrMultiExRate, boundary, breakPoint };
	}

	private static BigDecimal convertToGoogleAnalyticsCurrency(BigDecimal value, BigDecimal[] amounts) {
		BigDecimal currMultiExRate = amounts[0];
		BigDecimal boundary = amounts[1];
		BigDecimal breakPoint = amounts[2];

		if (currMultiExRate != null) {
			return AccelAeroRounderPolicy.convertAndRound(value, currMultiExRate, boundary, breakPoint);
		} else {
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		}
	}

	public String getCarLink() {
		return carLink;
	}

	public String getHotelLink() {
		return hotelLink;
	}

	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	public boolean isSuccess() {
		return success;
	}

	public BookingTO getBookingTO() {
		return bookingTO;
	}

	public Collection<SegInfoDTO> getFlightDepDetails() {
		return flightDepDetails;
	}

	public Collection<SegInfoDTO> getFlightRetDetails() {
		return flightRetDetails;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public String getLanguage() {
		return language;
	}

	public Collection<PaxTO> getColAdultCollection() {
		return colAdultCollection;
	}

	public Collection<PaxTO> getColChildCollection() {
		return colChildCollection;
	}

	public Collection<PaxTO> getColInfantCollection() {
		return colInfantCollection;
	}

	public String getEmailLanguage() {
		return emailLanguage;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Deprecated
	public LCCClientReservationInsurance getInsurance() {
		return insurance;
	}

	@Deprecated
	public void setInsurance(LCCClientReservationInsurance insurance) {
		this.insurance = insurance;
	}

	public List<LCCClientReservationInsurance> getInsurances() {
		return insurances;
	}

	public void setInsurances(List<LCCClientReservationInsurance> insurances) {
		this.insurances = insurances;
	}

	public ItineraryAnciAvailability getAncillaryAvailability() {
		return ancillaryAvailability;
	}

	public void setAncillaryAvailability(ItineraryAnciAvailability ancillaryAvailability) {
		this.ancillaryAvailability = ancillaryAvailability;
	}

	public boolean isGroupPNR() {
		return isGroupPNR;
	}

	public void setGroupPNR(boolean isGroupPNR) {
		this.isGroupPNR = isGroupPNR;
	}

	public String getStrRandom() {
		return strRandom;
	}

	public void setStrRandom(String strRandom) {
		this.strRandom = strRandom;
	}

	public List<String[]> getReservationTransItemInfo() {
		return reservationTransItemInfo;
	}

	public void setReservationTransItemInfo(List<String[]> reservationTransItemInfo) {
		this.reservationTransItemInfo = reservationTransItemInfo;
	}

	public BigDecimal[] getAmountsInGoogleAnlCurrency() {
		return amountsInGoogleAnlCurrency;
	}

	public void setAmountsInGoogleAnlCurrency(BigDecimal[] amountsInGoogleAnlCurrency) {
		this.amountsInGoogleAnlCurrency = amountsInGoogleAnlCurrency;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Collection<FlexiDTO> getFlexiInfo() {
		return flexiInfo;
	}

	public void setFlexiInfo(Collection<FlexiDTO> flexiInfo) {
		this.flexiInfo = flexiInfo;
	}

	public SessionTimeoutDataDTO getTimeoutDTO() {
		return timeoutDTO;
	}

	public String getOutboundFlightDuration() {
		return outboundFlightDuration;
	}

	public String getInboundFlightDuration() {
		return inboundFlightDuration;
	}

	public BigDecimal getTotalAncillaryCharge() {
		return totalAncillaryCharge;
	}

	public void setTotalAncillaryCharge(BigDecimal totalAncillaryCharge) {
		this.totalAncillaryCharge = totalAncillaryCharge;
	}

	public String getCarrierCode() {
		return carrierCode;
	}
	
	public Set<LCCClientReservationSegment> getSegments() {
		return segments;
	}
	
	public String getExpiryDays() {
		return expiryDays;
	}
	
	public Collection<DisplayUtilDTO> getBalanceSummary() {
		return balanceSummary;
	}

	public boolean isAllowIndivPrint() {
		return allowIndivPrint;
	}

	public void setAllowIndivPrint(boolean allowIndivPrint) {
		this.allowIndivPrint = allowIndivPrint;
	}

	public boolean isCarLinkDynamic() {
		return isCarLinkDynamic;
	}

	public void setCarLinkDynamic(boolean isCarLinkDynamic) {
		this.isCarLinkDynamic = isCarLinkDynamic;
	}

	public boolean isHotelLinkLoadWithinFrame() {
		return isHotelLinkLoadWithinFrame;
	}

	public void setHotelLinkLoadWithinFrame(boolean isHotelLinkLoadWithinFrame) {
		this.isHotelLinkLoadWithinFrame = isHotelLinkLoadWithinFrame;
	}

	public IPGTransactionResultDTO getIpgTransactionResultDTO() {
		return ipgTransactionResultDTO;
	}

	/**
	 * @param nonSamanRefund
	 *            the nonSamanRefund to set
	 */
	public void setSuccessfulRefund(boolean successfulRefund) {
		this.successfulRefund = successfulRefund;
	}

	/**
	 * @return the nonSamanRefund
	 */
	public boolean isSuccessfulRefund() {
		return successfulRefund;
	}

	public boolean isItineraryEmailGenerated() {
		return itineraryEmailGenerated;
	}

	public void setItineraryEmailGenerated(boolean itineraryEmailGenerated) {
		this.itineraryEmailGenerated = itineraryEmailGenerated;
	}

	public SocialTO getSocialTo() {
		return socialTo;
	}

	public void setSocialTo(SocialTO socialTo) {
		this.socialTo = socialTo;
	}

	public boolean isCreditPromo() {
		return creditPromo;
	}

	public boolean isMoneyPromo() {
		return moneyPromo;
	}

	public JSONObject getReservationCookieDetails() {
		return reservationCookieDetails;
	}

	public void setReservationCookieDetails(JSONObject reservationCookieDetails) {
		this.reservationCookieDetails = reservationCookieDetails;
	}

	public boolean isSaveCookieEnable() {
		return isSaveCookieEnable;
	}


	public void setSaveCookieEnable(boolean isSaveCookieEnable) {
		this.isSaveCookieEnable = isSaveCookieEnable;
	}

	public boolean isDisplayCreditCardProofLable() {
		return displayCreditCardProofLable;
	}

	public void setDisplayCreditCardProofLable(boolean displayCreditCardProofLable) {
		this.displayCreditCardProofLable = displayCreditCardProofLable;
	}
}
