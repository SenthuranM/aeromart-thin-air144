package com.isa.thinair.ibe.core.web.v2.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;

public class CommonUtil {

	public static final String CARRIER_SEPERATOR = "|";
	public static final String AIRLINE_SEPERATOR = ",";

	private static Log log = LogFactory.getLog(CommonUtil.class);

	/**
	 * <b> Deprecated </b> Directly use JSONUtil.serialize
	 * 
	 * @param rpParams
	 * @return
	 */
	@Deprecated
	public static String convertToJSON(Object rpParams) {
		try {
			return JSONUtil.serialize(rpParams);
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * Get Total Minutes
	 * 
	 * @param duration
	 * @return
	 */
	protected static int getTotalMinutes(String duration) {

		if (duration == null || duration.equals("") || duration.length() < 5) {
			return 0;
		}
		String[] arrDuration = duration.split(":");
		return (new Integer(arrDuration[0]).intValue() * 60) + new Integer(arrDuration[1]).intValue();
	}

	public static String getTimeDDHHMM(long time) {
		String dddhhmm = "";

		long days = TimeUnit.MILLISECONDS.toDays(time);
		long hours = TimeUnit.MILLISECONDS.toHours(time - TimeUnit.DAYS.toMillis(days));
		long minutes = TimeUnit.MILLISECONDS.toMinutes(time - (TimeUnit.DAYS.toMillis(days) + TimeUnit.HOURS.toMillis(hours)));

		dddhhmm = days + ":" + hours + ":" + minutes;

		return dddhhmm;
	}

	public static String getTimeHHMM(long time) {
		String hhmm = "";
		long days = TimeUnit.MILLISECONDS.toDays(time);
		long hours = TimeUnit.MILLISECONDS.toHours(time - TimeUnit.DAYS.toMillis(days));
		long minutes = TimeUnit.MILLISECONDS.toMinutes(time - (TimeUnit.DAYS.toMillis(days) + TimeUnit.HOURS.toMillis(hours)));
		hhmm = days * 24 + hours + ":" + minutes;
		return hhmm;
	}

	public static String formatDateTimeYYYYMMDDHHMM(long timeLong) {
		Date date = new Date(timeLong);
		if (date != null) {
			SimpleDateFormat formetter = new SimpleDateFormat("yyyyMMddHHmm");
			return formetter.format(date);
		} else {
			return null;
		}
	}

	/**
	 * Support Months:days:hours:minuts or hours : minutes
	 * 
	 * @param hhmm
	 * @return
	 */
	public static String wrapWithDDHHMM(String hhmm) {
		StringBuilder wrapTextHM = new StringBuilder();
		if (hhmm != null && !hhmm.isEmpty()) {
			String[] textArray = hhmm.split(":");
			if (textArray.length == 3) {

				if (textArray[0] != null && !textArray[0].equals("0")) {
					wrapTextHM.append(textArray[0]);
					wrapTextHM.append(" Day(s) : ");
				}

				wrapTextHM.append(textArray[1]);
				wrapTextHM.append(" Hour(s) : ");

				wrapTextHM.append(textArray[2]);
				wrapTextHM.append(" Minute(s) ");

			}

			if (textArray.length == 2) {
				wrapTextHM.append(textArray[0]);
				wrapTextHM.append(" Hour(s) : ");

				wrapTextHM.append(textArray[1]);
				wrapTextHM.append(" Minute(s) ");
			}
		}

		return wrapTextHM.toString();
	}

	public static String wrapWithHM(String hhmm) {
		StringBuilder wrapTextHM = new StringBuilder();
		if (hhmm != null && !hhmm.isEmpty()) {
			String[] textArray = hhmm.split(":");
			if (textArray.length == 2) {
				wrapTextHM.append(textArray[0]);
				wrapTextHM.append("h ");
				wrapTextHM.append(textArray[1]);
				wrapTextHM.append("m");
			}
		}

		return wrapTextHM.toString();
	}

	public static String getHTTPHeaderValue(HttpServletRequest request, String headerName) {
		String headerParamValue = "";
		try {
			headerParamValue = request.getHeader(headerName);
		} catch (Exception e) {
			headerParamValue = "-";
		}
		return headerParamValue;
	}

	public static String getNullSafeValue(String value) {
		return StringUtils.trimToEmpty(value);
	}

	public static String getExistingReservationMsg(HttpServletRequest request, String pnr) {
		String language = SessionUtil.getLanguage(request);
		// TODO
		language = "en";
		StringBuffer messageBuilder = new StringBuffer();
		messageBuilder.append("<span style='font-size: 14px;'>" + I18NUtil.getMessage("msg.booking.heading", language)
				+ "</span>");
		messageBuilder.append("<br/>");
		messageBuilder.append("<br/>");
		messageBuilder.append(I18NUtil.getMessage("msg.booking.ques1", language));
		messageBuilder.append("<br/>");
		messageBuilder.append(I18NUtil.getMessage("msg.booking.ques2", language));
		messageBuilder.append("<br/>");
		messageBuilder.append("3. " + I18NUtil.getMessage("msg.please.contact", language));
		messageBuilder.append("<br/>");
		if (pnr != null && !pnr.trim().isEmpty()) {
			messageBuilder.append(I18NUtil.getMessage("msg.reference.number", language));
			messageBuilder.append(" : <span style='font-size: 2em;'>");
			messageBuilder.append(pnr);
			messageBuilder.append("</span>");
		}
		SessionUtil.setTempPaymentPNR(request, null);
		SessionUtil.setResponseReceivedFromPaymentGateway(request, false);
		SessionUtil.resetSesionDataInError(request);

		StringBuilder logMessage = new StringBuilder();
		logMessage.append("Detected new search when previous booking attempt is partially completed");
		logMessage.append("|timestamp=");
		logMessage.append(CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date()));
		logMessage.append("|sessId=");
		logMessage.append(request.getRequestedSessionId());
		logMessage.append("|xFwdFor=");
		logMessage.append(CommonUtil.getNullSafeValue(CommonUtil.getHTTPHeaderValue(request, "X-Forwarded-For")));
		logMessage.append("|pnr=");
		logMessage.append(pnr);

		if (log.isInfoEnabled()) {
			log.info(logMessage.toString());
		}

		return messageBuilder.toString();
	}

	public static String getRefundMessage(String language) {
		StringBuffer messageBuilder = new StringBuffer();
		messageBuilder.append(I18NUtil.getMessage("msg.booking.fail.refund", language));
		return messageBuilder.toString();
	}

	/**
	 * 
	 * Get country details based on originator IP. Currently this will include countryCode, marketingCarrier &
	 * onholdEnabled
	 * 
	 * @param originIp
	 * @return
	 * @throws ModuleException
	 */
	public static Country getCountryFromIPAddress(long originIp) throws ModuleException {
		return ModuleServiceLocator.getCommoMasterBD().getCountryFromIPAddress(originIp);
	}

	public static String getMarketingCarrierFromCountry(String countryCode) throws ModuleException {
		return ModuleServiceLocator.getCommoMasterBD().getMarketingCarrierFromCountry(countryCode);
	}

	public static List<Calendar> validPeriod(Calendar depStart, Calendar depEnd, Calendar validFrom, Calendar validTo) {
		List<Calendar> period = new ArrayList<Calendar>();
		if (validFrom.before(depStart) && (validTo.before(depStart) || validTo.equals(depStart))) {
			period.add(depStart);
			period.add(depStart);
		} else if (validFrom.before(depStart) && validTo.after(depStart)) {
			period.add(depStart);
			if (validTo.before(depEnd)) {
				period.add(validTo);
			} else {
				period.add(depEnd);
			}
		} else if ((validFrom.after(depStart) || validFrom.equals(depStart)) && validTo.before(depEnd)) {
			period.add(validFrom);
			period.add(validTo);
		} else if (validFrom.after(depStart) && validFrom.before(depEnd) && (validTo.after(depEnd) || validTo.equals(depEnd))) {
			period.add(validFrom);
			period.add(depEnd);
		} else if (validFrom.after(depStart) && (validFrom.after(depEnd) || validFrom.equals(depEnd)) && validTo.after(depEnd)) {
			period.add(depEnd);
			period.add(depEnd);
		} else {
			period.add(depStart);
			period.add(depEnd);
		}

		return period;
	}

	/**
	 * Only use for java Local support
	 * 
	 * @param language
	 * @return
	 */
	public static String getJavaSupportLocale(String language) {
		if ("ar".equalsIgnoreCase(language) || "ru".equalsIgnoreCase(language) || "zh".equalsIgnoreCase(language)) {
			language = Locale.ENGLISH.getLanguage();
		}
		return language;
	}

	/**
	 * Removing the carrier code from String. if String is like G9|3, this will return 3. If String is like AB3, this
	 * will return the same.
	 * 
	 * @param string
	 * @return
	 */
	public static String getCarrierStringRemoved(String carrierString) {
		String string = carrierString.split(AIRLINE_SEPERATOR)[0];
		int index = string.indexOf(CARRIER_SEPERATOR);
		if (index == -1) {
			return string;
		}
		return string.substring(index + 1);
	}
	
	public static ModuleException getStandardPaymentBrokerError(String paymentErrorCode) {

		ModuleException me = new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR);
		Map<String, String> errorMap = new HashMap<String, String>();

		errorMap.put(paymentErrorCode, "External payment broker error:" + paymentErrorCode);
		me.setExceptionDetails(errorMap);

		return me;
	}

	public static String getServerErrorMessage(HttpServletRequest request, String key) {
		return ReservationUtil.getServerErrorMessage(request, key);
	}

	/**
	 * Get Broker type - internal payment gateway or external payment gateway
	 * 
	 * @param ipgDTO
	 * @return
	 * @throws ModuleException
	 */
	public static String getPaymentBrokerType(IPGIdentificationParamsDTO ipgDTO) throws ModuleException {
		String brokerType = null;
		if (ipgDTO != null && ipgDTO.getIpgId() != null) {
			Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgDTO);
			brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
		}
		return brokerType;
	}
	public static String timeDiffInStr(long diff) {

		StringBuffer str = new StringBuffer();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffDays > 0) {
			str.append(diffDays + " days, ");
		}
		if (diffHours > 0) {
			str.append(diffHours + " hours, ");
		}
		if (diffMinutes > 0) {
			str.append(diffMinutes + " minutes, ");
		}
		if (diffSeconds > 0) {
			str.append(diffSeconds + " seconds.");
		}
		return str.toString();
	}
	

}
