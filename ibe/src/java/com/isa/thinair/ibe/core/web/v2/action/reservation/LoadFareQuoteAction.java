package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.JSONObject;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.converters.AncillaryConverterUtil;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.FareCategoryTO.FareCategoryStatusCodes;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.v2.FareRuleDisplayTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.MessageUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "")

})
public class LoadFareQuoteAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(LoadFareQuoteAction.class);

	private boolean success = true;

	private String messageTxt;

	private ArrayList<String> outFlightRPHList = new ArrayList<String>();

	private ArrayList<String> retFlightRPHList = new ArrayList<String>();

	private FareQuoteTO fareQuote;

	private String pnr;

	private boolean groupPNR;

	private String oldAllSegments;

	private String modifySegmentRefNos;

	private String airportMessage;

	private boolean isOutBoundFlexiAvailable = false;

	private boolean isInBoundFlexiAvailable = false;

	private boolean modifySegment;

	private int oldFareID;

	private int oldFareType;

	private String oldFareAmount;

	private String oldFareCarrierCode;

	private String outBoundFlexiMessage;

	private String inBoundFlexiMessage;

	private boolean forceRefresh = false;

	private BigDecimal outboundCalFare;

	private BigDecimal inboundCalFare;

	private List<FareRuleDisplayTO> fareRules;

	private boolean showFareRules;

	private boolean fromPostCardPage;

	private String msgFromPostCardPage;

	private String requestSessionIdentifier;

	private boolean allowOnhold;

	private DiscountedFareDetails promoInfo;

	private Set<String> paymentMethods;

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();

	private JSONObject searchParamsCookieDetails = new JSONObject();

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private String requoteFlightSearch;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		boolean isCabinClassOnholdEnable = false;
		try {
			FareQuoteTO segFareQuote = null;
			String strLanguage = SessionUtil.getLanguage(request);
			String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
			Locale locale = new Locale(strLanguage);
			if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
					&& !tempPaymentPNR.trim().isEmpty()) {
				messageTxt = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
				success = false;
				return result;
			}

			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			if (resInfo != null && resInfo.isFromPostCardDetails()) {
				fromPostCardPage = resInfo.isFromPostCardDetails();
				msgFromPostCardPage = I18NUtil.getMessage("msg.postCardPage", strLanguage);
			}
			getSearchParams().setPreferredBookingCodes("");
			String searchSystem = getSearchParams().getSearchSystem();
			SYSTEM system = SYSTEM.getEnum(searchSystem);
			if (resInfo != null && resInfo.getSelectedSystem() != null) {
				system = resInfo.getSelectedSystem();
			}
			if (system == SYSTEM.NONE) {
				throw new ModuleRuntimeException("Invalid Seach System :" + searchSystem);
			}

			FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
			// FIXME avoid creating AvailableFlightSearchDTOBuilder and directly create the flightPriceRQ
			// Current implementation leads unnecessary DTO transformation step
			AvailableFlightSearchDTOBuilder fareQuteParams = SearchUtil.getSearchBuilder(getSearchParams());
			if (modifySegment && !groupPNR) {
				SearchUtil.setModifingSegmentSearchData(fareQuteParams, oldFareID, oldFareType, oldFareAmount);
			}

			// Set Half Return Fare
			String fromAirport = fareQuteParams.getSearchDTO().getFromAirport();
			String toAirport = fareQuteParams.getSearchDTO().getToAirport();
			boolean isFromOpByOwn = ModuleServiceLocator.getAirportBD().isAirportOptByOwn(fromAirport);
			boolean isToOptByOwn = ModuleServiceLocator.getAirportBD().isAirportOptByOwn(toAirport);
			if (isFromOpByOwn && isToOptByOwn) {
				if (pnr != null && !"".equals(pnr) && !groupPNR) {
					SearchUtil.setExistingSegInfo(fareQuteParams.getSearchDTO(), oldAllSegments, modifySegmentRefNos);
				} else if (fareQuteParams.getSearchDTO().isReturnFlag() && !fareQuteParams.getSearchDTO().isOpenReturnSearch()) {
					boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
					fareQuteParams.getSearchDTO().setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
				}
			}

			flightPriceRQ = BeanUtil.createFareQuoteRQ(fareQuteParams, outFlightRPHList, retFlightRPHList, getSearchParams());
			if (pnr != null && !"".equals(pnr)) {
				flightPriceRQ.getAvailPreferences().setModifyBooking(true);
			}

			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());

			flightPriceRQ.getAvailPreferences().setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
			flightPriceRQ.getAvailPreferences().setOldFareCarrierCode(oldFareCarrierCode);

			if (groupPNR) {
				system = SYSTEM.INT;
			}

			flightPriceRQ.getAvailPreferences().setSearchSystem(system);
			flightPriceRQ.getAvailPreferences().setPreferredLanguage(strLanguage);
			flightPriceRQ.getAvailPreferences().setOndFlexiSelected(getSearchParams().getOndSelectedFlexi());

			if (pnr != null && !"".equals(pnr)) {
				AnalyticsLogger.logAvlSearch(AnalyticSource.IBE_MOD_FQ, flightPriceRQ, request, getTrackInfo());
			} else {
				AnalyticsLogger.logAvlSearch(AnalyticSource.IBE_CRE_FQ, flightPriceRQ, request, getTrackInfo());
			}

			FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ,
					getTrackInfo());

			String accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);

			resInfo.addNewServiceTaxes(flightPriceRS.getApplicableServiceTaxes());

			// TODO Re-factor
			if ((!CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint)) && !modifySegment) {
				isCabinClassOnholdEnable = ReservationUtil.isReservationCabinClassOnholdable(flightPriceRS);
				resInfo.setAllowOnhold(isCabinClassOnholdEnable);

				if (isCabinClassOnholdEnable && AppSysParamsUtil.isOnHoldEnable(OnHold.IBE)) {
					boolean reservationOtherValidationOnholdable = ReservationUtil.isReservationOtherValidationOnholdable(
							flightPriceRS, system, getTrackInfo(), getClientInfoDTO().getIpAddress());
					resInfo.setReservationOnholdableOtherValidation(reservationOtherValidationOnholdable);

					allowOnhold = isCabinClassOnholdEnable && reservationOtherValidationOnholdable;

					if (allowOnhold) {
						for (OriginDestinationInformationTO ondInfo : flightPriceRS.getOriginDestinationInformationList()) {
							Date depTimeZulu = BeanUtil.getSelectedFlightDepartureDate(ondInfo.getOrignDestinationOptions());
							boolean ohdEnabled = (depTimeZulu != null) ? ReservationUtil.isOnHoldPaymentEnable(OnHold.IBE,
									depTimeZulu) : false;
							if (!(allowOnhold && ohdEnabled)) {
								allowOnhold = allowOnhold && ohdEnabled;
								break;
							}
							allowOnhold = allowOnhold && ohdEnabled;
						}
					}
				}
			}

			FlightFareSummaryTO outboundFlightFareSummaryTO = new FlightFareSummaryTO();
			FlightFareSummaryTO inboundFlightFareSummaryTO = new FlightFareSummaryTO();

			// Segment fare summary objects.
			FlightFareSummaryTO inboundFlightSegmentFareSummaryTO = null;
			FlightFareSummaryTO outboundFlightSegmentFareSummaryTO = null;

			boolean machingResponceFound = true;
			if (outFlightRPHList != null) {
				for (String outFlt : outFlightRPHList) {
					if (!outFlt.trim().isEmpty()) {
						if (outFlt.indexOf("#") != -1) {
							outFlt = outFlt.split("#")[0];
						}
						boolean ondMatched = false;

						for (OriginDestinationInformationTO priceRSOnDInfo : flightPriceRS.getOriginDestinationInformationList()) {
							for (OriginDestinationOptionTO originDestOpt : priceRSOnDInfo.getOrignDestinationOptions()) {
								for (FlightSegmentTO flightsegs : originDestOpt.getFlightSegmentList()) {
									if (outFlt.equalsIgnoreCase(flightsegs.getFlightRefNumber())) {
										ondMatched = true;
										outboundFlightFareSummaryTO = originDestOpt.getSelectedFlightFareSummary();
										break;
									}
								}
							}
						}

						for (OriginDestinationInformationTO priceRSOnDInfo : flightPriceRS.getOriginDestinationInformationList()) {
							for (OriginDestinationOptionTO originDestOpt : priceRSOnDInfo.getOriginDestinationSegFareOptions()) {
								for (FlightSegmentTO flightsegs : originDestOpt.getFlightSegmentList()) {
									if (outFlt.equalsIgnoreCase(flightsegs.getFlightRefNumber())) {
										outboundFlightSegmentFareSummaryTO = originDestOpt.getSelectedFlightFareSummary();
										break;
									}
								}
							}
						}
						machingResponceFound = ondMatched && machingResponceFound;
						if (!machingResponceFound) {
							forceRefresh = true;
							// TODO refactor this to a constant file
							throw new ModuleException("ibe.fare.quote.flights.not.matching");
						}
					}
				}
			}

			if (retFlightRPHList != null) {
				for (String inFlt : retFlightRPHList) {
					if (!inFlt.trim().isEmpty()) {
						if (inFlt.indexOf("#") != -1) {
							inFlt = inFlt.split("#")[0];
						}
						boolean ondMatched = false;

						for (OriginDestinationInformationTO priceRSOnDInfo : flightPriceRS.getOriginDestinationInformationList()) {
							for (OriginDestinationOptionTO originDestOpt : priceRSOnDInfo.getOrignDestinationOptions()) {
								for (FlightSegmentTO flightsegs : originDestOpt.getFlightSegmentList()) {
									if (inFlt.equalsIgnoreCase(flightsegs.getFlightRefNumber())) {
										ondMatched = true;
										inboundFlightFareSummaryTO = originDestOpt.getSelectedFlightFareSummary();
										break;
									}
								}
							}
						}

						for (OriginDestinationInformationTO priceRSOnDInfo : flightPriceRS.getOriginDestinationInformationList()) {
							for (OriginDestinationOptionTO originDestOpt : priceRSOnDInfo.getOriginDestinationSegFareOptions()) {
								for (FlightSegmentTO flightsegs : originDestOpt.getFlightSegmentList()) {
									if (inFlt.equalsIgnoreCase(flightsegs.getFlightRefNumber())) {
										inboundFlightSegmentFareSummaryTO = originDestOpt.getSelectedFlightFareSummary();
										break;
									}
								}
							}
						}

						machingResponceFound = ondMatched && machingResponceFound;
						if (!machingResponceFound) {
							forceRefresh = true;
							// TODO refactor this to a constant file
							throw new ModuleException("ibe.fare.quote.flights.not.matching");
						}
					}
				}
			}

			PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();
			boolean applyReturnFareDiscount = false;
			if (priceInfoTO != null) {
				Collection<AvailableFlightInfo> outboundFlights = BeanUtil.createAvailFlightInfoList(flightPriceRS, fromAirport,
						toAirport, false, locale, false);
				Collection<AvailableFlightInfo> outboundSegFlights = BeanUtil.createAvailFlightInfoList(flightPriceRS,
						fromAirport, toAirport, false, locale, true);

				Collection<AvailableFlightInfo> returnFlights = BeanUtil.createAvailFlightInfoList(flightPriceRS, toAirport,
						fromAirport, false, locale, false);
				Collection<AvailableFlightInfo> returnSegFlights = BeanUtil.createAvailFlightInfoList(flightPriceRS, toAirport,
						fromAirport, false, locale, true);

				String selCurr = getSearchParams().getSelectedCurrency();
				if (!AppSysParamsUtil.showCalFareInSelectedCurrency()) {
					selCurr = AppSysParamsUtil.getBaseCurrency();
				}

				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurr, ApplicationEngine.IBE);

				BigDecimal onewayOutboundFare = BigDecimal.ZERO;
				BigDecimal onewayInboundFare = BigDecimal.ZERO;

				// Total price - Fare + SUR + TAX for one adult
				if (AppSysParamsUtil.showTotalFareInCalendar()) {
					outboundCalFare = (outboundFlightFareSummaryTO != null)
							? outboundFlightFareSummaryTO.getTotalPrice()
							: BigDecimal.ZERO;
					onewayOutboundFare = (outboundFlightSegmentFareSummaryTO != null) ? outboundFlightSegmentFareSummaryTO
							.getTotalPrice() : BigDecimal.ZERO;

					inboundCalFare = (inboundFlightFareSummaryTO != null)
							? inboundFlightFareSummaryTO.getTotalPrice()
							: BigDecimal.ZERO;
					onewayInboundFare = (inboundFlightSegmentFareSummaryTO != null) ? inboundFlightSegmentFareSummaryTO
							.getTotalPrice() : BigDecimal.ZERO;

				} else {
					outboundCalFare = (outboundFlightFareSummaryTO != null)
							? outboundFlightFareSummaryTO.getBaseFareAmount()
							: BigDecimal.ZERO;
					onewayOutboundFare = (outboundFlightSegmentFareSummaryTO != null) ? outboundFlightSegmentFareSummaryTO
							.getBaseFareAmount() : BigDecimal.ZERO;

					inboundCalFare = (inboundFlightFareSummaryTO != null)
							? inboundFlightFareSummaryTO.getBaseFareAmount()
							: BigDecimal.ZERO;
					onewayInboundFare = (inboundFlightSegmentFareSummaryTO != null) ? inboundFlightSegmentFareSummaryTO
							.getBaseFareAmount() : BigDecimal.ZERO;

				}

				BigDecimal totalQuoted = outboundCalFare.add(inboundCalFare);
				BigDecimal totalSegmentFare = onewayOutboundFare.add(onewayInboundFare);

				applyReturnFareDiscount = totalSegmentFare.compareTo(totalQuoted) > 0;
				if (applyReturnFareDiscount) {
					outboundCalFare = onewayOutboundFare;
					inboundCalFare = onewayInboundFare;
				}

				Currency currency = exchangeRate.getCurrency();
				boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
				BigDecimal boundry = BigDecimal.ZERO;
				BigDecimal breakpoint = BigDecimal.ZERO;
				if (isCurrencyRoundUpEnabled) {
					boundry = (currency.getBoundryValue() != null) ? currency.getBoundryValue() : AppSysParamsUtil
							.getIBECalBoundry();
					breakpoint = (currency.getBreakPoint() != null) ? currency.getBreakPoint() : AppSysParamsUtil
							.getIBECalBreakpoint();
				}

				if (outboundCalFare != null) {

					if (isCurrencyRoundUpEnabled) {
						outboundCalFare = AccelAeroRounderPolicy.convertAndRound(outboundCalFare,
								exchangeRate.getMultiplyingExchangeRate(), boundry, breakpoint);
					} else {
						outboundCalFare = AccelAeroCalculator
								.multiply(outboundCalFare, exchangeRate.getMultiplyingExchangeRate());
						outboundCalFare = AccelAeroRounderPolicy.getRoundedValue(outboundCalFare,
								AppSysParamsUtil.getIBECalBoundry(), AppSysParamsUtil.getIBECalBreakpoint());
					}
				}
				if (inboundCalFare != null) {

					if (isCurrencyRoundUpEnabled) {
						inboundCalFare = AccelAeroRounderPolicy.convertAndRound(inboundCalFare,
								exchangeRate.getMultiplyingExchangeRate(), boundry, breakpoint);
					} else {
						inboundCalFare = AccelAeroCalculator.multiply(inboundCalFare, exchangeRate.getMultiplyingExchangeRate());
						inboundCalFare = AccelAeroRounderPolicy.getRoundedValue(inboundCalFare,
								AppSysParamsUtil.getIBECalBoundry(), AppSysParamsUtil.getIBECalBreakpoint());
					}
				}

				resInfo.setPriceInfoTO(priceInfoTO);

				// Set promotion discount information if any
				resInfo.setFareDiscount(flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
				if (resInfo.getDiscountInfo() != null
						&& !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(resInfo.getDiscountInfo()
								.getPromotionType())) {

					ReservationUtil.calculateDiscountForReservation(resInfo, null, flightPriceRQ, getTrackInfo(), true,
							resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(), flightPriceRS.getTransactionIdentifier(), false);

				}

				promoInfo = resInfo.getDiscountInfo();

				fareQuote = BeanUtil.fillFareQuote(fareQuote, priceInfoTO, getSearchParams().getSelectedCurrency(),
						outboundFlights, returnFlights, exchangeRateProxy, resInfo.getDiscountAmount(false));

				// Map<Integer, BigDecimal> ondTotalFlexiCharges = getOndWiseTotalFlexiCharge(resInfo.getPriceInfoTO()
				// .getFareTypeTO().getOndExternalCharges());

				if (flightPriceRS.getSelectedSegmentFlightPriceInfo() != null) {
					segFareQuote = BeanUtil.fillFareQuote(segFareQuote, flightPriceRS.getSelectedSegmentFlightPriceInfo(),
							getSearchParams().getSelectedCurrency(), outboundSegFlights, returnSegFlights, exchangeRateProxy,
							null);
				}
				resInfo.setTotalPriceWithoutExtChg(BeanUtil.getTotalPriceWithoutExtCharges(priceInfoTO));
				resInfo.setTotalFare(fareQuote.getTotalFare());
				resInfo.setTotalTax(fareQuote.getTotalTax());
				resInfo.setTotalTaxSurchages(fareQuote.getTotalTaxSurcharge());

				// Request Session Identifier
				requestSessionIdentifier = (new Date().getTime()) + "";
				SessionUtil.setRequestSessionIdentifier(request, requestSessionIdentifier);

				List<FareRuleDTO> ondFareRules = BeanUtil.getFareRules(flightPriceRS.getSelectedPriceFlightInfo(),
						flightPriceRS.getOriginDestinationInformationList());
				GlobalConfig globalConfig = new GlobalConfig();
				Map<String, FareCategoryTO> sortedFareCategoryTOMap = globalConfig.getFareCategories();
				fareRules = new ArrayList<FareRuleDisplayTO>();

				for (FareRuleDTO fareRule : ondFareRules) {
					FareCategoryTO fareCategoryTO = sortedFareCategoryTOMap.get(fareRule.getFareCategoryCode());
					if (fareCategoryTO != null && fareCategoryTO.getStatus().equals(FareCategoryStatusCodes.ACT)) {
						showFareRules = (showFareRules || fareCategoryTO.getShowComments());
						if (fareCategoryTO.getShowComments() || AppSysParamsUtil.isBundledFaresEnabled(ApplicationEngine.IBE.toString())) {
							FareRuleDisplayTO frDis = new FareRuleDisplayTO();
							frDis.setOndCode(fareRule.getOrignNDest());
							frDis.setComment(fareRule.getComments());
							frDis.setBookingClass(fareRule.getBookingClassCode());
							fareRules.add(frDis);
						}
					}
				}

				resInfo.setBaggageSummaryTo(AncillaryConverterUtil.toLccReservationBaggageSummaryTo(flightPriceRS));

				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());

				List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
				ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);

			} else {
				forceRefresh = true;
			}

			// Load Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightPriceRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.IBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, SessionUtil.getLanguage(request));

			// If segmentFareQuote is available after all the caluculations and session data insertion is done, replace
			// them
			if (segFareQuote != null && applyReturnFareDiscount) {
				SearchUtil.checkFlexiAvailability(flightPriceRQ.getAvailPreferences().getOndFlexiSelected(),
						priceInfoTO.getAvailableLogicalCCList());
				fareQuote = ReservationUtil.getDiscountSetFareQuote(fareQuote, segFareQuote);
				if (fareQuote != null) {
					if (fareQuote.getTotalIbeFareDiscount() != null && !"".equals(fareQuote.getTotalIbeFareDiscount())) {
						resInfo.setIbeReturnFareDiscountAmount(new BigDecimal(fareQuote.getTotalIbeFareDiscount()));
					}
				}
			} else {
				resInfo.setIbeReturnFareDiscountAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}

			if (AppSysParamsUtil.isCookieSaveFunctionalityEnabled()) {
				Collection<AvailableFlightInfo> outboundFlights = BeanUtil.createAvailFlightInfoList(flightPriceRS, fromAirport,
						toAirport, false, locale, false);
				Collection<AvailableFlightInfo> returnFlights = BeanUtil.createAvailFlightInfoList(flightPriceRS, toAirport,
						fromAirport, false, locale, false);
				if (getSearchParams().isReturnFlag() && returnFlights != null) {
					outboundFlights.addAll(returnFlights);
				}
				this.searchParamsCookieDetails = ReservationUtil.getSearchParamCookie(getSearchParams(), fareQuote,
						outboundFlights);

			}
			// Payment Icons Display
			if (AppSysParamsUtil.showPaymentMethodsOnIBE()) {

				String countryCode = ReservationUtil.getCountryCode(request, getClientInfoDTO().getIpAddress());

				boolean enableOfflinePaymentImages = ReservationUtil.isOfflineBookingEligible(flightPriceRS.getSelectedPriceFlightInfo(), 
						getSelectedFlights(flightPriceRS),ResOnholdValidationDTO.IBE_OFFLINE_VALIDATION, new ArrayList<ReservationPaxTO>(), null, 
						getClientInfoDTO().getIpAddress(), SYSTEM.getEnum(getSearchParams().getSearchSystem()),isCreateReservation(), getTrackInfo());

				List<OndClassOfServiceSummeryTO> ccList = flightPriceRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList();
				String firstOnd = (ccList.get(0) == null) ? null : ccList.get(0).getOndCode();
				PaymentMethodDetailsDTO paymentMethodDetailsDTO =	(new PaymentMethodDetailsDTO())
						.setFirstSegmentONDCode(firstOnd)
						.setCountryCode(countryCode)
						.setEnableOffline(enableOfflinePaymentImages);

				paymentMethods = ModuleServiceLocator.getPaymentBrokerBD().getPaymentMethodNames(paymentMethodDetailsDTO);
				// Add Cash method
				if (allowOnhold) {
					paymentMethods.add("CASH");
				}
				// Add System default icons
				paymentMethods.addAll(AppSysParamsUtil.getDefaultPaymentMethods());
			}

		} catch (ModuleException me) {
			log.error(me.getModuleCode(), me);
			success = false;
			log.error("LoadFareQuoteAction==>", me);
			if (me.getExceptionCode().equals("ibe.fare.quote.flights.not.matching")) {
				messageTxt = "Returned flights not matching with fare quoted flights";
			} else if (me.getExceptionCode().equals("ibe.fare.quote.flexi.not.available")) {
				forceRefresh = true;
				messageTxt = "Selected flexi fare not available";
			} else if (me.getExceptionCode().equals("err.58-maxico.exposed.cannot.mix.agreements.in.onefare.quote")) {
				messageTxt = MessagesUtil.getMessage(me.getExceptionCode());
			} else if (me.getExceptionCode().equals("err.60-maxico.exposed.no.fares.detected")) {
				messageTxt = "Fares no longer available for the selected flight(s).";
				forceRefresh = true;
			} else if (me.getExceptionCode().equals("airinventory.arg.farequote.flightclosed")) {
				forceRefresh = true;
				messageTxt = "flightsHaveChanged";
			} else {
				messageTxt = me.getMessage();
			}

		} catch (Exception ex) {
			log.error("LoadFareQuoteAction==>", ex);
			success = false;
			messageTxt = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
		}
		return result;
	}

	private List<FlightSegmentTO> getSelectedFlights(FlightPriceRS flightAvailRS) {
		List<FlightSegmentTO> flightSegmentTOs = null;

		if (flightAvailRS != null) {
			for (OriginDestinationInformationTO informationTO : flightAvailRS.getOriginDestinationInformationList()) {
				for (OriginDestinationOptionTO optionTO : informationTO.getOrignDestinationOptions()) {
					if (optionTO.isSelected()) {
						flightSegmentTOs = optionTO.getFlightSegmentList();
					}
				}
			}
		}

		return flightSegmentTOs;
	}


	private boolean isCreateReservation() {
		boolean createReservation = true;
		if (modifySegment) {
			createReservation = false;
		}
		return createReservation;
	}

	public boolean isForceRefresh() {
		return forceRefresh;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public boolean isOutBoundFlexiAvailable() {
		return isOutBoundFlexiAvailable;
	}

	public void setOutBoundFlexiAvailable(boolean isOutBoundFlexiAvailable) {
		this.isOutBoundFlexiAvailable = isOutBoundFlexiAvailable;
	}

	public boolean isInBoundFlexiAvailable() {
		return isInBoundFlexiAvailable;
	}

	public void setInBoundFlexiAvailable(boolean isInBoundFlexiAvailable) {
		this.isInBoundFlexiAvailable = isInBoundFlexiAvailable;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setOldFareID(int oldFareID) {
		this.oldFareID = oldFareID;
	}

	public void setOldFareType(int oldFareType) {
		this.oldFareType = oldFareType;
	}

	public void setOldFareAmount(String oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public String getOutBoundFlexiMessage() {
		return outBoundFlexiMessage;
	}

	public void setOutBoundFlexiMessage(String outBoundFlexiMessage) {
		this.outBoundFlexiMessage = outBoundFlexiMessage;
	}

	public String getInBoundFlexiMessage() {
		return inBoundFlexiMessage;
	}

	public void setInBoundFlexiMessage(String inBoundFlexiMessage) {
		this.inBoundFlexiMessage = inBoundFlexiMessage;
	}

	public boolean isShowFareRules() {
		return showFareRules;
	}

	public List<FareRuleDisplayTO> getFareRules() {
		return fareRules;
	}

	public BigDecimal getInboundCalFare() {
		return inboundCalFare;
	}

	public BigDecimal getOutboundCalFare() {
		return outboundCalFare;
	}

	public boolean isFromPostCardPage() {
		return fromPostCardPage;
	}

	public String getMsgFromPostCardPage() {
		return msgFromPostCardPage;
	}

	public String getRequestSessionIdentifier() {
		return requestSessionIdentifier;
	}

	public void setOldFareCarrierCode(String oldFareCarrierCode) {
		this.oldFareCarrierCode = oldFareCarrierCode;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	public DiscountedFareDetails getPromoInfo() {
		return promoInfo;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public Set<String> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(Set<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public JSONObject getSearchParamsCookieDetails() {
		return searchParamsCookieDetails;
	}

	public void setSearchParamsCookieDetails(JSONObject searchParamsCookieDetails) {
		this.searchParamsCookieDetails = searchParamsCookieDetails;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	public String getRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(String requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

}
