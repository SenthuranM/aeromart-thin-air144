package com.isa.thinair.ibe.core.web.v2.action.voucher;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.api.dto.VoucherCCPaymentDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants.StrutsAction;
import com.isa.thinair.ibe.core.web.util.MessageUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants.PaymentFlow;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.opensymphony.xwork2.ActionChainResult;

@SuppressWarnings("deprecation")
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Common.VOUCHER_PAYMENT_SUCCESS),
		@Result(name = StrutsConstants.Result.BYPASS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.PAYMENT, type = ActionChainResult.class, value = StrutsConstants.Action.IBE_PAYMENT),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Result.LANDING_BACKBUTTON_HANDLE, value = StrutsConstants.Jsp.Common.BACKBUTTON_HANDLE_ERRORPAGE),
		@Result(name = StrutsConstants.Jsp.Common.REDIRECT, value = StrutsConstants.Jsp.Common.REDIRECT),
		@Result(name = StrutsConstants.Result.CARD_PAYMENT_ERROR, value = StrutsConstants.Jsp.Payment.CARD_PAYMENT_ERROR),
		@Result(name = StrutsConstants.Result.OFFLINE_PAYMENT_INFO, value = StrutsConstants.Jsp.Payment.OFFLINE_PAYMENT_INFO) })
public class IssueVoucherConfirmationAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(IssueVoucherConfirmationAction.class);
	
	private VoucherDTO voucherDTO;
	
	private List<VoucherDTO> voucherList;

	private boolean getVoucherDTO = false;

	private String message;
	
	private static final String VOUCHERPAGEID = "Voucher";
	
	private HashMap<String, String> jsonLabel;

	private Map<String, String> errorInfo;
	
	private boolean success = true;	

	@SuppressWarnings("unused")
	public String execute() {
		if (log.isDebugEnabled()) {
			log.debug("### IssueVoucherConfirmationAction start... ###");
		}
		String forward = StrutsConstants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			SessionUtil.setSystemRefresh(request, new Integer(1));
			boolean isPaymentSuccess = false;
			String msg = null;
			String language = SessionUtil.getLanguage(request);
			String brokerType = null;
			boolean switchToExternalURL = false;
			boolean isCreditCardError = false;
			boolean isException = false;
			boolean isViewPaymentInIframe = false;
			VoucherCCPaymentDTO postPayDTO = null;
			
			
			IPGTransactionResultDTO ipgTransactionResultDTO = null;
			try {
				if (SessionUtil.isSessionExpired(request)) {
					forward = StrutsConstants.Result.SESSION_EXPIRED;
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, CommonUtil.getRefundMessage(language));
				} else if (getVoucherDTO) {
					if (voucherList == null) {
						String strLanguage = SessionUtil.getLanguage(request);
						voucherList = SessionUtil.getVoucherDTOlist(request);
						setMessage("Payment has been successfully completed and Gift Voucher(s) generated.");
						String[] pagesIDs = {VOUCHERPAGEID};
						jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
						errorInfo = ErrorMessageUtil.getVoucherErrors(request);
						return StrutsConstants.Result.BYPASS;
					} else {
						throw new ModuleException(ExceptionConstants.SERVER_OPERATION_FAIL);
					}
				} else {
					postPayDTO = SessionUtil.getVoucherPaymentDTO(request);
					List<VoucherDTO> voucherDTOs = postPayDTO.getVoucherDTOList();
					if (postPayDTO == null) {
						SessionUtil.expireSession(request);
						forward = StrutsConstants.Result.SESSION_EXPIRED;
						request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
								CommonUtil.getRefundMessage(SessionUtil.getLanguage(request)));

					} else {

						if (postPayDTO.getIpgResponseDTO() != null) {
							ipgTransactionResultDTO = postPayDTO.getIpgResponseDTO().getIpgTransactionResultDTO();
							if (!postPayDTO.getIpgResponseDTO().isSuccess()) {

								String paymentErrorCode = postPayDTO.getIpgResponseDTO().getErrorCode();
								if (paymentErrorCode != null && paymentErrorCode.trim().length() > 0) {
									ModuleException me = CommonUtil.getStandardPaymentBrokerError(paymentErrorCode);
									PaymentGateWayInfoDTO pgInfo = postPayDTO.getPaymentGateWay();
									switchToExternalURL = (pgInfo != null ? pgInfo.isSwitchToExternalURL() : switchToExternalURL);

									if (switchToExternalURL == false && postPayDTO.isSwitchToExternalURL()) {
										switchToExternalURL = true;
									}

									isViewPaymentInIframe = postPayDTO.isViewPaymentInIframe();

									if (isViewPaymentInIframe || switchToExternalURL) {
										request.setAttribute(WebConstants.DISPLAY_TOP_BOTTOM_BANNERS, false);
										request.setAttribute(WebConstants.INVALID_CARD, false);
									} else {
										request.setAttribute(WebConstants.DISPLAY_TOP_BOTTOM_BANNERS, true);
										request.setAttribute(WebConstants.INVALID_CARD, true);
									}

									isCreditCardError = true;

									throw me;

								} else {
									if (log.isInfoEnabled())
										log.info("Possible Intruder IP Detected : " + getClientInfoDTO().getIpAddress());
									throw new ModuleException(ExceptionConstants.ERR_PAYMENT_SESSION_EXPIRED);
								}
							}
						}
						// Null check, code will break in modification with no payment
						if (postPayDTO.getIpgResponseDTO() != null)
							isPaymentSuccess = postPayDTO.getIpgResponseDTO().isSuccess();

						IPGIdentificationParamsDTO ipgDTO = null;

						IPGResponseDTO creditInfo = postPayDTO.getIpgResponseDTO();
						brokerType = CommonUtil.getPaymentBrokerType(ipgDTO);
						Map<Integer, CommonCreditCardPaymentInfo> tempPayMap = postPayDTO.getTemporyPaymentMap();
					
						// Set Page Building Parameters
						SystemUtil.setCommonParameters(request, getCommonParams());

						if (voucherDTOs != null) {
							VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
							voucherDelegate.issueGiftVouchersFromIBE(voucherDTOs);
							SessionUtil.setVoucherDTOList(request, voucherDTOs);
						}

						if (log.isDebugEnabled()) {
							log.debug("Gift Voucher Issued. VoucherID [" + voucherDTO.getVoucherId() + "] ");
						}

					}

				}
			} catch (Exception me) {
				log.error("IssueVoucherConfirmation", me);
				isException = true;
				/* If successful reservation exist. Load the reservation. */
				if (SessionUtil.isSuccessfulReservationExit(request)) {
					forward = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE;
				} else {
					boolean isPaymentBrokerError = false;

					if (me instanceof ModuleException) {
						ModuleException moduleException = (ModuleException) me;
						String errorCode = moduleException.getExceptionCode();
						/**
						 * When credit card payment fail , the system recreate the payment page for any payment gateway.
						 * 
						 */
						if (errorCode.equals(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR)) {
							isPaymentBrokerError = true;
							String strErrMsg = "";
							try {
								Map<String, String> errorMap = (Map) moduleException.getExceptionDetails();
								Iterator<String> itErrorMap = errorMap.keySet().iterator();

								if (itErrorMap.hasNext()) {
									errorCode = (String) itErrorMap.next();
								}

								strErrMsg = CommonUtil.getServerErrorMessage(request,
										ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR + errorCode);
								if (errorCode == null || "".equals(errorCode) || strErrMsg == null || "".equals(strErrMsg)) {
									errorCode = "pay.gateway.error.less.0";
								}
							} catch (Exception ex) {
								log.error("IssueVoucherConfirmationAction", ex);
								errorCode = "pay.gateway.error.less.0";
							}
							SessionUtil.getIBEreservationInfo(request).setFromPostCardDetails(false);
							Map<String, String> formHiddenDataMap = new HashMap<String, String>();
							formHiddenDataMap
									.put(ReservationWebConstnts.PARAM_PAYMENT_FLOW, PaymentFlow.PAYMENT_RETRY.toString());
							formHiddenDataMap.put(ReservationWebConstnts.MSG_ERROR_CODE, errorCode);
							forward = StrutsConstants.Jsp.Common.REDIRECT;
							SystemUtil.createRedirectFormData(request, StrutsAction.LOAD_CONTAINER, formHiddenDataMap, true);

						} else {
							isPaymentBrokerError = false;
						}
					} else {
						isPaymentBrokerError = false;
					}

					if (isPaymentBrokerError == false) {
						if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
							forward = StrutsConstants.Result.CARD_PAYMENT_ERROR;
						} else {
							forward = StrutsConstants.Result.ERROR;
						}

						if (isPaymentSuccess) {
							msg = CommonUtil.getRefundMessage(language);
						} else {
							msg = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL,
									SessionUtil.getLanguage(request));
						}
						request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
						SessionUtil.resetSesionDataInError(request);
					}
				}
			}
			if (log.isDebugEnabled()) {
				log.debug("### IssueVoucherConfirmationAction end... ###");
			}
		} else {
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
					I18NUtil.getMessage(ExceptionConstants.KEY_UNAUTHORIZED_OPERATION, SessionUtil.getLanguage(request)));
			return StrutsConstants.Result.ERROR;
		}
		return forward;

	}	

	public boolean isGetVoucherDTO() {
		return getVoucherDTO;
	}

	public void setGetVoucherDTO(boolean getVoucherDTO) {
		this.getVoucherDTO = getVoucherDTO;
	}
	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_IBE);
		return trackInfoDTO;
	}


	/**
	 * @return the voucherDTO
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO the voucherDTO to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the jsonLabel
	 */
	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	/**
	 * @param jsonLabel the jsonLabel to set
	 */
	public void setJsonLabel(HashMap<String, String> jsonLabel) {
		this.jsonLabel = jsonLabel;
	}

	/**
	 * @return the errorInfo
	 */
	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	/**
	 * @param errorInfo the errorInfo to set
	 */
	public void setErrorInfo(Map<String, String> errorInfo) {
		this.errorInfo = errorInfo;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<VoucherDTO> getVoucherList() {
		return voucherList;
	}

	public void setVoucherList(List<VoucherDTO> voucherList) {
		this.voucherList = voucherList;
	}


}