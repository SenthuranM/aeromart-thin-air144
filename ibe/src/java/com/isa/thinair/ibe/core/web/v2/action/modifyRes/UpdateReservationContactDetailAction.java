package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.JSONObject;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.BookingTO;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class UpdateReservationContactDetailAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(UpdateReservationContactDetailAction.class);

	private boolean success = true;

	private String messageTxt;
	// TODO Move webflatform
	private ContactInfoDTO contactInfo;

	private String hdnPNR;

	private boolean blnGroupPNR;

	private String hdnMobile;

	private String hdnPhone;

	private String hdnFax;

	private String hdnEmgnPhone;

	private String version;
	// TODO:Move webflatform
	private BookingTO bookingTO;

	// holds old contact info string
	private String contactInfoJson;
	
	private JSONObject reservationCookieDetails = new JSONObject();

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {

			int intCustomerID = SessionUtil.getCustomerId(request);

			CommonReservationContactInfo oldContactInfo = ReservationUtil.transformJsonContactInfo(contactInfoJson);

			CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();
			lccClientReservationContactInfo.setCity(StringUtil.nullConvertToString(contactInfo.getCity()));
			lccClientReservationContactInfo.setCountryCode(StringUtil.nullConvertToString(contactInfo.getCountry()));
			lccClientReservationContactInfo.setZipCode(StringUtil.nullConvertToString(contactInfo.getZipCode()));
			lccClientReservationContactInfo.setEmail(StringUtil.nullConvertToString(contactInfo.getEmailAddress()));
			lccClientReservationContactInfo.setFirstName(StringUtil.nullConvertToString(contactInfo.getFirstName()));
			lccClientReservationContactInfo.setLastName(StringUtil.nullConvertToString(contactInfo.getLastName()));
			lccClientReservationContactInfo.setMobileNo(StringUtil.nullConvertToString(hdnMobile));
			lccClientReservationContactInfo.setPhoneNo(StringUtil.nullConvertToString(hdnPhone));
			lccClientReservationContactInfo.setFax(StringUtil.nullConvertToString(hdnFax));
			lccClientReservationContactInfo.setStreetAddress1(StringUtil.nullConvertToString(contactInfo.getAddresStreet()));
			lccClientReservationContactInfo.setStreetAddress2(StringUtil.nullConvertToString(contactInfo.getAddresline()));
			lccClientReservationContactInfo.setTitle(contactInfo.getTitle());
			if (!"".equals(StringUtil.nullConvertToString(contactInfo.getNationality()))) {
				lccClientReservationContactInfo.setNationalityCode(Integer.parseInt(StringUtil.nullConvertToString(contactInfo
						.getNationality())));
			}
			lccClientReservationContactInfo.setState(StringUtil.nullConvertToString(contactInfo.getState()));

			if (intCustomerID != -1) {
				lccClientReservationContactInfo.setCustomerId(new Integer(intCustomerID));
			}

			lccClientReservationContactInfo.setPreferredLanguage(contactInfo.getPreferredLangauge());

			// set emergency contact information
			lccClientReservationContactInfo.setEmgnTitle(StringUtil.nullConvertToString(contactInfo.getEmgnTitle()));
			lccClientReservationContactInfo.setEmgnFirstName(StringUtil.nullConvertToString(contactInfo.getEmgnFirstName()));
			lccClientReservationContactInfo.setEmgnLastName(StringUtil.nullConvertToString(contactInfo.getEmgnLastName()));
			lccClientReservationContactInfo.setEmgnPhoneNo(StringUtil.nullConvertToString(hdnEmgnPhone));
			lccClientReservationContactInfo.setEmgnEmail(StringUtil.nullConvertToString(contactInfo.getEmgnEmail()));

			ModuleServiceLocator.getAirproxyReservationBD().modifyContactInfo(hdnPNR, version, lccClientReservationContactInfo,
					oldContactInfo, blnGroupPNR, AppIndicatorEnum.APP_IBE.toString(), getTrackInfo());
			// Load Reservation Contact Details
			Customer cust = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			LCCClientReservation reservation = ReservationUtil.loadProxyReservation(hdnPNR, blnGroupPNR, getTrackInfo(), false,
					(cust != null) ? true : false, null, null, false);
			bookingTO = ReservationUtil.createBookingDetails(reservation);

			contactInfo = ReservationUtil.getContactDetails(reservation.getContactInfo());
			contactInfo.setNationality(StringUtil.nullConvertToString(reservation.getContactInfo().getNationalityCode()));
			
			if(AppSysParamsUtil.isCookieSaveFunctionalityEnabled()){
				this.reservationCookieDetails = ReservationUtil.getReservationCookieDetails(reservation);
			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = I18NUtil.getMessage(me.getExceptionCode(), SessionUtil.getLanguage(request));
			log.error("UpdateReservationContactDetailAction==>" + me);
		} catch (Exception re) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("UpdateReservationContactDetailAction==>" + re);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public void setHdnPNR(String hdnPNR) {
		this.hdnPNR = hdnPNR;
	}

	public void setHdnMobile(String hdnMobile) {
		this.hdnMobile = hdnMobile;
	}

	public void setHdnPhone(String hdnPhone) {
		this.hdnPhone = hdnPhone;
	}

	public void setHdnFax(String hdnFax) {
		this.hdnFax = hdnFax;
	}

	public void setHdnEmgnPhone(String hdnEmgnPhone) {
		this.hdnEmgnPhone = hdnEmgnPhone;
	}

	public void setBlnGroupPNR(boolean blnGroupPNR) {
		this.blnGroupPNR = blnGroupPNR;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public BookingTO getBookingTO() {
		return bookingTO;
	}

	@JSON(serialize = false)
	public String getContactInfoJson() {
		return contactInfoJson;
	}

	public void setContactInfoJson(String contactInfoJson) {
		this.contactInfoJson = contactInfoJson;
	}

	public JSONObject getReservationCookieDetails() {
		return reservationCookieDetails;
	}

	public void setReservationCookieDetails(JSONObject reservationCookieDetails) {
		this.reservationCookieDetails = reservationCookieDetails;
	}

}
