package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.webplatform.core.service.captchaService.CaptchaService;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ManageBookingV2Action extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ManageBookingV2Action.class);

	private boolean success = true;

	private String messageTxt;

	private String pnr = "";

	private String groupPNR;

	private String lastName = "";

	private String txtDateOfDep = "";

	private String captcha = "";

	private String airlineCode;

	private String marketingAirlineCode;

	private boolean imageCapchaEnable = false;

	private boolean isAllowLoadResUsingPaxLastName = false;

	private final String dateFormat = "dd/MM/yyyy";

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		Date departureDate = new Date();
		try {
			boolean captchaPassed = true;
			boolean isCapCha = AppSysParamsUtil.isCaptchaEnabledForUnregisterdUser();
			SessionUtil.setCustomerId(request, -1);
			CustomerUtil.revertLoyaltyRedemption(request);
			SessionUtil.setLoyaltyFFID(request, null);

			if (isCapCha) {
				imageCapchaEnable = true;
				captchaPassed = CaptchaService.getInstance().validateResponseForID(request.getSession().getId(), captcha);
			}

			if (AppSysParamsUtil.isAllowLoadReservationsUsingPaxNameInIBE()) {
				isAllowLoadResUsingPaxLastName = true;
			}

			// IF Link is external Link then Set captchaPassed= true
			if (request.getParameter("link") != null && request.getParameter("link").equals("ELINK")) {
				captchaPassed = true;
			}

			if (captchaPassed) {
				// TODO Implement New API For Load IBE Booking
				ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
				reservationSearchDTO.setPnr(pnr.trim());
				reservationSearchDTO.setLastName(lastName.trim());

				// with dry reservation changes we can't guarantee where the reservation is going to be.
				// so we have to do a search all operation if LCC connectivity is enabled.
				if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
					reservationSearchDTO.setSearchAll(true);
				} else {
					reservationSearchDTO.setSearchAll(false);
				}
				try {
					departureDate = new SimpleDateFormat(dateFormat).parse(txtDateOfDep.trim());
					reservationSearchDTO.setDepartureDate(departureDate);
				} catch (Exception ex) {
					log.warn("Invalid departure date format detected in IBE manage booking" + departureDate);
				}

				Collection<ReservationListTO> reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(
						reservationSearchDTO, null, TrackInfoUtil.getBasicTrackInfo(request));

				if (reservations.isEmpty()) {
					messageTxt = "invalidCombination";
					return forward;
				} else {

					if (reservations.size() > 1) {
						messageTxt = "invalidCombination";
						return forward;
					} else {
						ReservationListTO reservationItem = BeanUtils.getFirstElement(reservations);
						groupPNR = extractGroupPNR(reservationItem);
						marketingAirlineCode = reservationItem.getMarketingAirlineCode();
						airlineCode = reservationItem.getAirlineCode();
						/*
						 * reservationItem.getPaxName() is in the format of "firstname,lastname"
						 */
						String foundLastName = reservationItem.getPaxName().split(",")[1].trim();
						if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservationItem.getPnrStatus())) {
							messageTxt = "Cancelled Bookings are not allowed to view / modify";
						} else if (!isValidBookingData(reservationItem, txtDateOfDep)) {
							messageTxt = "invalidCombination";
						} else if (!lastName.equalsIgnoreCase(foundLastName)) {
							messageTxt = "invalidCombination";
							if (AppSysParamsUtil.isAllowLoadReservationsUsingPaxNameInIBE()) {
								LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
								modes.setPnr(reservationItem.getPnrNo());

								Reservation reservation = ModuleServiceLocator.getReservationBD().getReservation(modes, null);
								// Allow load reservation using passenger last name.
								for (ReservationPax pax : reservation.getPassengers()) {
									if (pax.getLastName().equalsIgnoreCase(lastName)) {
										messageTxt = null;
										break;
									}
								}
							}

						}
					}
				}
			} else {
				messageTxt = "Enter the word displayed in the image to view / modify your reservation";
			}

			// Update Session State for manage booking
			if (messageTxt == null && success == true) {
				SessionUtil.setManageBookingPNR(request, pnr.trim());
			}

		} catch (ModuleException me) {
			log.error("ManageBookingV2Action==>" + me, me);
			if (me.getExceptionCode().equalsIgnoreCase("airreservations.arg.noLongerExist")) {
				success = true;
				messageTxt = "invalidCombination";
			} else if (!me.getExceptionCode().isEmpty()) {
				success = false;
				messageTxt = I18NUtil.getMessage(me.getExceptionCode());
			} else {
				success = false;
				messageTxt = me.getMessageString();
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ManageBookingV2Action==>" + ex, ex);
		}
		return forward;

	}

	public String reservationExit() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			if (SessionUtil.getIbeReservationPostDTO(request) != null
					&& SessionUtil.getIbeReservationPostDTO(request).getPnr() != null) {
				pnr = SessionUtil.getIbeReservationPostDTO(request).getPnr();
			}

			if (pnr == null || pnr.isEmpty()) {
				success = false;
			} else {
				ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
				reservationSearchDTO.setPnr(pnr.trim());
				Collection<ReservationDTO> collection = ModuleServiceLocator.getReservationQueryBD().getReservations(
						reservationSearchDTO);
				if (!collection.isEmpty()) {
					for (ReservationDTO reservationDTO : collection) {
						if (reservationDTO.getOriginatorPnr() != null && !reservationDTO.getOriginatorPnr().isEmpty()) {
							groupPNR = reservationDTO.getOriginatorPnr();
						} else {
							groupPNR = "";
						}
						break;
					}
					success = true;
				}
			}

		} catch (Exception ex) {
			success = false;
			log.error("ManageBookingV2Action==>Reservation not exist yet...Pending.." + request.getRequestedSessionId());
		}
		return forward;
	}

	private boolean isValidBookingData(ReservationListTO reservationListTO, String depDate) {
		boolean isValid = false;
		if (reservationListTO != null && reservationListTO.getPnrNo().equals(pnr)) {

			List<FlightInfoTO> flightInfo = new ArrayList<FlightInfoTO>(reservationListTO.getFlightInfo());
			Collections.sort(flightInfo);

			for (FlightInfoTO flightInfoTO : flightInfo) {
				if (flightInfoTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					SimpleDateFormat departureDateFormat = new SimpleDateFormat(dateFormat);
					Date flightDepartureDate = new Date(flightInfoTO.getDepartureDateLong());

					if (flightDepartureDate.compareTo(CalendarUtil.getCurrentSystemTimeInZulu()) > 0 && flightInfo.size() > 1) {
						if (departureDateFormat.format(flightDepartureDate).equals(depDate)) {
							isValid = true;
							break;
						} else {
							isValid = false;
							break;
						}
					} else {
						if (departureDateFormat.format(flightDepartureDate).equals(depDate)) {
							isValid = true;
							break;
						}
					}
				}
			}
		}
		return isValid;
	}

	private String extractGroupPNR(ReservationListTO reservationItem) {
		String groupPNR = "";
		groupPNR = reservationItem.getOriginatorPnr();
		if (StringUtils.isEmpty(groupPNR)) {
			// if this condition is true, it implies that the reservation is a dry reservation.
			if (StringUtils.isNotEmpty(reservationItem.getAirlineCode())
					&& StringUtils.isNotEmpty(reservationItem.getMarketingAirlineCode())) {
				groupPNR = reservationItem.getPnrNo();
			}
		}
		return groupPNR;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setTxtDateOfDep(String dateOfDep) {
		this.txtDateOfDep = dateOfDep;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isImageCapchaEnable() {
		return imageCapchaEnable;
	}

	public void setImageCapchaEnable(boolean imageCapchaEnable) {
		this.imageCapchaEnable = imageCapchaEnable;
	}

	public boolean isAllowLoadResUsingPaxLastName() {
		return isAllowLoadResUsingPaxLastName;
	}

	public void setAllowLoadResUsingPaxLastName(boolean isAllowLoadResUsingPaxLastName) {
		this.isAllowLoadResUsingPaxLastName = isAllowLoadResUsingPaxLastName;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}
}
