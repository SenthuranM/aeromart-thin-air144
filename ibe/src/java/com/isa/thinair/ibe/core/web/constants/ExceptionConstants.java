package com.isa.thinair.ibe.core.web.constants;

public interface ExceptionConstants {
	public static final String ERR_PAYMENT_SESSION_EXPIRED = "paymentbroker.session.expired";
	public static final String SERVER_OPERATION_FAIL = "server.default.operation.fail";
	// TODO Set Exception
	public static final String KEY_UNAUTHORIZED_OPERATION = "server.default.operation.fail";
	public static final String ERR_PAYMENT_UNSUCESS = "error.payment.notprocessorder";
}
