package com.isa.thinair.ibe.core.web.v2.action.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.PassengerDTO;
import com.isa.thinair.ibe.api.dto.VoucherCCPaymentDTO;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;

/**
 * @author Thushara Fernando
 * 
 *         This is a special action file to show files directly with out any presentations logic being executed. File
 *         Name itself is used instead of separate result value to make mappings simple. So in the result annotation
 *         filename itself is specified as name as well as the value.
 * 
 *         eg : @Result(name=S2Constants.Jsp.Common.EXAMPLE_FILE, value=S2Constants.Jsp.Common.EXAMPLE_FILE)
 * 
 * 
 *         To work this <constant name="struts.enable.DynamicMethodInvocation" value="true" /> should be set in the
 *         struts.xml file
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_PAX, value = StrutsConstants.Jsp.Respro.INTERLINE_PAX),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_ANCI, value = StrutsConstants.Jsp.Respro.INTERLINE_ANCI),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_PAYMENT, value = StrutsConstants.Jsp.Respro.INTERLINE_PAYMENT),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_RES, value = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_RES),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE, value = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3, value = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FLIGHTS_REQUOTE, value = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FLIGHTS_REQUOTE),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FLIGHTS_REQUOTE_V3, value = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FLIGHTS_REQUOTE_V3),
		@Result(name = StrutsConstants.Jsp.Respro.MANAGE_BOOKING, value = StrutsConstants.Jsp.Respro.MANAGE_BOOKING),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM, value = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM_V3, value = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM_V3),
		@Result(name = StrutsConstants.Jsp.Kiosk.LOGIN, value = StrutsConstants.Jsp.Kiosk.LOGIN),
		@Result(name = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK, value = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Jsp.Common.LOADING_MESSAGE, value = StrutsConstants.Jsp.Common.LOADING_MESSAGE),
		@Result(name = StrutsConstants.Jsp.Common.TRACKING, value = StrutsConstants.Jsp.Common.TRACKING),
        @Result(name = StrutsConstants.Jsp.Common.NO_TRACKING, value = StrutsConstants.Jsp.Common.NO_TRACKING),
		@Result(name = StrutsConstants.Jsp.Insurance.CANCELATION, value = StrutsConstants.Jsp.Insurance.CANCELATION),
		@Result(name = StrutsConstants.Jsp.Insurance.MULTIRISK, value = StrutsConstants.Jsp.Insurance.MULTIRISK),
		// @Result(name = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY, type = ActionChainResult.class, value =
		// StrutsConstants.Action.MULTICITY_ON_PREVIOUS_CLK),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY, value = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY_V3, value = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY_V3),
		@Result(name = StrutsConstants.Jsp.Respro.COMPARE_FARE, value = StrutsConstants.Jsp.Respro.COMPARE_FARE),
		@Result(name = StrutsConstants.Result.INTERNAL_EXTERNAL_3DSECURE, value = StrutsConstants.Jsp.Common.INTERNAL_EXTERNAL_3DSECURE),
		@Result(name = StrutsConstants.Result.LANDING_BACKBUTTON_HANDLE, value = StrutsConstants.Jsp.Common.BACKBUTTON_HANDLE_ERRORPAGE),
		@Result(name = StrutsConstants.Result.CARD_PAYMENT_ERROR, value = StrutsConstants.Jsp.Payment.CARD_PAYMENT_ERROR),
		@Result(name = StrutsConstants.Jsp.Promotion.PROMOTION_REGISTRATION_CONFIRM, value = StrutsConstants.Jsp.Promotion.PROMOTION_REGISTRATION_CONFIRM),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Jsp.Customer.SELF_REPROTECTION_ERROR, value = StrutsConstants.Jsp.Customer.SELF_REPROTECTION_ERROR)})
public class ShowLoadPageAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ShowLoadPageAction.class);

	private Collection<PassengerDTO> adultList = new ArrayList<PassengerDTO>();

	private Collection<PassengerDTO> childList = new ArrayList<PassengerDTO>();

	private Collection<PassengerDTO> infantList = new ArrayList<PassengerDTO>();

	private ContactInfoDTO contactInfo = new ContactInfoDTO();
	
	private String fltPGWPaymentMobileNumber;
	
	private String fltPGWPaymentEmail;
	
	private String fltPGWPaymentCustomerName;	

	public String loadPax() {
		return StrutsConstants.Jsp.Respro.INTERLINE_PAX;
	}

	public String loadAnci() {
		return StrutsConstants.Jsp.Respro.INTERLINE_ANCI;
	}

	public String loadError() {
		try {
			// Fix Ajax Error issue for all languages - Read language specific text
			String strMessage = request.getParameter("messageTxt");
			String message = "";
			if (strMessage != null)
				message = new String(strMessage.getBytes("8859_1"), "UTF-8");
			// Check Previous Reservation status and present reservation details
			String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
			if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
					&& !tempPaymentPNR.trim().isEmpty()) {
				message = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
			}

			SystemUtil.setCommonParameters(request, getCommonParams());
			SessionUtil.resetSesionDataInError(request);
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, message);
			if (log.isInfoEnabled())
				log.info("Tracking Ajax error" + message + "SessionID:" + request.getRequestedSessionId());
		} catch (Exception ex) {
			request.setAttribute("error", ex.getMessage());
			log.error("Ajax Error loading fail", ex);
		}
		return StrutsConstants.Result.ERROR;
	}
	
	public String loadSelfReprotectError() {
		try {
			// Fix Ajax Error issue for all languages - Read language specific text
			String strMessage = request.getParameter("messageTxt");
			String message = "";
			if (strMessage != null)
				message = new String(strMessage.getBytes("8859_1"), "UTF-8");
			// Check Previous Reservation status and present reservation details
			String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
			if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
					&& !tempPaymentPNR.trim().isEmpty()) {
				message = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
			}

			SystemUtil.setCommonParameters(request, getCommonParams());
			SessionUtil.resetSesionDataInError(request);
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, message);
			if (log.isInfoEnabled())
				log.info("Tracking Ajax error" + message + "SessionID:" + request.getRequestedSessionId());
		} catch (Exception ex) {
			request.setAttribute("error", ex.getMessage());
			log.error("Ajax Error loading fail", ex);
		}
		return StrutsConstants.Jsp.Customer.SELF_REPROTECTION_ERROR;
	}

	public String loadPayment() {
		return StrutsConstants.Jsp.Respro.INTERLINE_PAYMENT;
	}

	public String showCompareFare() {
		return StrutsConstants.Jsp.Respro.COMPARE_FARE;
	}

	private boolean isMultiCitySelected() {
		boolean returnValue = false;
		if (request.getSession().getAttribute(ReservationWebConstnts.IS_MULTYCITY_SELECTED) != null) {
			returnValue = Boolean.parseBoolean(request.getSession().getAttribute(ReservationWebConstnts.IS_MULTYCITY_SELECTED)
					.toString());
		}
		return returnValue;
	}

	private boolean isFromRegUserPage() {
		boolean returnValue = false;
		if (request.getParameter(ReservationWebConstnts.IS_FROM_REGUSER) != null) {
			returnValue = Boolean.parseBoolean(request.getParameter(ReservationWebConstnts.IS_FROM_REGUSER).toString());
		}
		return returnValue;
	}

	private boolean isOnPreviousClick() {
		boolean returnValue = false;
		if (request.getParameter(ReservationWebConstnts.IS_ON_PREVIOUS_CLICK) != null) {
			returnValue = Boolean.parseBoolean(request.getParameter(ReservationWebConstnts.IS_ON_PREVIOUS_CLICK).toString());
		}
		return returnValue;
	}

	public String loadFlightSearch() {
		request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
				CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));
		request.setAttribute(ReservationWebConstnts.PARAM_IMAGE_HOST_PATH, AppSysParamsUtil.getImageUploadPath());
		SystemUtil.setCommonParameters(request, getCommonParams());
		String forward;

		if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
			request.setAttribute(ReservationWebConstnts.PARAM_OND_SOURCE, AppSysParamsUtil.getDynamicOndListUrl());
		}

		boolean flightRequoteEnabled = Boolean.parseBoolean(request.getParameter("requoteFlightSearch"));

		if (flightRequoteEnabled) {
			request.setAttribute(ReservationWebConstnts.PARAM_REQUOTE_CALENDAR_ENABLED,
					AppSysParamsUtil.isCalendarViewEnabledForIBERequote());
			if (SessionUtil.isNewDesignVersion(request)) {
				forward = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FLIGHTS_REQUOTE_V3;
			} else {
				forward = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FLIGHTS_REQUOTE;
			}
		} else {
			if (SessionUtil.isNewDesignVersion(request)) {
				forward = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3;
			} else {
				forward = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE;
				// To Set to both flows in to one in availability
				// forward = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3;
			}
		}
		if (!flightRequoteEnabled && ((isOnPreviousClick() && isMultiCitySelected()) || isFromRegUserPage())) {
			if (SessionUtil.isNewDesignVersion(request)) {
				forward = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY_V3;
			} else {
				forward = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY;
			}
		}
		
		request.setAttribute("fltPGWPaymentMobileNumber", fltPGWPaymentMobileNumber);
		request.setAttribute("fltPGWPaymentEmail", fltPGWPaymentEmail);
		request.setAttribute("fltPGWPaymentCustomerName", fltPGWPaymentCustomerName);

		request.setAttribute("paxJason", request.getParameter("paxJson"));

		return forward;
	}

	public String loadManageBooking() {
		return StrutsConstants.Jsp.Respro.MANAGE_BOOKING;
	}

	public String kioskLogin() {
		return StrutsConstants.Jsp.Kiosk.LOGIN;
	}

	public String kioskHome() {
		if (SystemUtil.isKioskUser(request)) {
			request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
					CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));
			SystemUtil.setCommonParameters(request, getCommonParams());
			return StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK;
		} else {
			return StrutsConstants.Jsp.Kiosk.LOGIN;
		}
	}

	public String expireSession() {
		SessionUtil.expireSession(request);
		return StrutsConstants.Result.SESSION_EXPIRED;
	}

	public String loadCardDetailPage() {
		return StrutsConstants.Jsp.Common.CREDITCARD_DETAILS;
	}

	public String loadLoadingPage() {
		return StrutsConstants.Jsp.Common.LOADING_MESSAGE;
	}

	public String load3DSecurePage() {
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		if (resInfo != null && resInfo.getBank3DSecureHTML() != null) {
			request.setAttribute(WebConstants.BANKS_3D_SECURE_HTML_PAGE, resInfo.getBank3DSecureHTML());
			// Clear the session
			resInfo.setBank3DSecureHTML(null);
			return StrutsConstants.Result.INTERNAL_EXTERNAL_3DSECURE;
		} else {
			if (log.isInfoEnabled()) {
				log.info("Loading 3D secure page is fail.Please try again..");
			}
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, "Loading 3D secure page is fail.Please try again..");
			return StrutsConstants.Result.ERROR;
		}
	}
	
	public String load3DSecurePageForVoucher() {
		VoucherCCPaymentDTO voucherCCPaymentDTO = SessionUtil.getVoucherPaymentDTO(request);
		if (voucherCCPaymentDTO != null && voucherCCPaymentDTO.getBank3DSecureHTML() != null) {
			request.setAttribute(WebConstants.BANKS_3D_SECURE_HTML_PAGE, voucherCCPaymentDTO.getBank3DSecureHTML());
			// Clear the session
			voucherCCPaymentDTO.setBank3DSecureHTML(null);
			return StrutsConstants.Result.INTERNAL_EXTERNAL_3DSECURE;
		} else {
			if (log.isInfoEnabled()) {
				log.info("Loading 3D secure page is fail.Please try again..");
			}
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
					"Loading 3D secure page for voucher payment is fail.Please try again..");
			return StrutsConstants.Result.ERROR;
		}
	}

	public String loadConfimationPage() {
		String forward;
		SystemUtil.setCommonParameters(request, getCommonParams());
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM_V3;
		} else {
			forward = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM;
		}
		return forward;
	}

	public String loadPromotionRegisterConfimationPage() {
		SystemUtil.setCommonParameters(request, getCommonParams());
		String language = SessionUtil.getLanguage(request);
		String forward = StrutsConstants.Jsp.Promotion.PROMOTION_REGISTRATION_CONFIRM;
		try {
			Map<String, String> promMessages = ErrorMessageUtil.getPromotionMessages(request);
			request.setAttribute(ReservationWebConstnts.PROMO_MESSAGES, JSONUtil.serialize(promMessages));
		} catch (Exception e) {
			log.error("Error in loading message codes " + e);
			forward = StrutsConstants.Result.ERROR;
		}

		return forward;
	}

	public String loadTrackingPage() {
		if (AppSysParamsUtil.isTrackingEnabled()) {
			return StrutsConstants.Jsp.Common.TRACKING;
		} else {
			return StrutsConstants.Jsp.Common.NO_TRACKING;
		}
	}

	// Used for the Airmed insurance
	public String loadInsuranceTermsCon() {
		String forward = "";
		String insuranceType = request.getParameter("insType");
		String language = SessionUtil.getLanguage(request);
		if (insuranceType != null) {
			if ("CNX".equalsIgnoreCase(insuranceType)) {
				if ("fr".equalsIgnoreCase(language)) {
					forward = StrutsConstants.Jsp.Insurance.CANCELATION;
				} else if ("en".equalsIgnoreCase(language)) {
					forward = StrutsConstants.Jsp.Insurance.CANCELATION_EN;
				}
			} else if ("MLTRISK".equalsIgnoreCase(insuranceType)) {
				if ("fr".equalsIgnoreCase(language)) {
					forward = StrutsConstants.Jsp.Insurance.MULTIRISK;
				} else if ("en".equalsIgnoreCase(language)) {
					forward = StrutsConstants.Jsp.Insurance.MULTIRISK_EN;
				}
			}
		}
		return forward;
	}

	public String loadBackButtonHandleErrorPage() {
		String page = StrutsConstants.Result.ERROR;
		String errorMessage = ReservationUtil.getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR
				+ request.getParameter(ReservationWebConstnts.MSG_ERROR_CODE));
		String attempt = request.getParameter(ReservationWebConstnts.PARAM_REDIRECT_ATTEMPT);
		if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
			page = StrutsConstants.Result.CARD_PAYMENT_ERROR;
		}
		if (attempt != null && attempt.equalsIgnoreCase("first")) {
			page = StrutsConstants.Result.LANDING_BACKBUTTON_HANDLE;
			request.setAttribute(ReservationWebConstnts.PARAM_REDIRECT_ATTEMPT, "second");
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_CODE,
					request.getParameter(ReservationWebConstnts.MSG_ERROR_CODE));
			request.setAttribute(WebConstants.INVALID_CARD, true);
		} else {
			request.setAttribute(WebConstants.INVALID_CARD, false);
		}
		SystemUtil.setCommonParameters(request, getCommonParams());
		request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, errorMessage);

		return page;
	}

	// Getters & Setters
	public Collection<PassengerDTO> getAdultList() {
		return adultList;
	}

	public void setAdultList(Collection<PassengerDTO> adultList) {
		this.adultList = adultList;
	}

	public Collection<PassengerDTO> getChildList() {
		return childList;
	}

	public void setChildList(Collection<PassengerDTO> childList) {
		this.childList = childList;
	}

	public Collection<PassengerDTO> getInfantList() {
		return infantList;
	}

	public void setInfantList(Collection<PassengerDTO> infantList) {
		this.infantList = infantList;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getFltPGWPaymentMobileNumber() {
		return fltPGWPaymentMobileNumber;
	}

	public void setFltPGWPaymentMobileNumber(String fltPGWPaymentMobileNumber) {
		this.fltPGWPaymentMobileNumber = fltPGWPaymentMobileNumber;
	}

	public String getFltPGWPaymentEmail() {
		return fltPGWPaymentEmail;
	}

	public void setFltPGWPaymentEmail(String fltPGWPaymentEmail) {
		this.fltPGWPaymentEmail = fltPGWPaymentEmail;
	}

	public String getFltPGWPaymentCustomerName() {
		return fltPGWPaymentCustomerName;
	}

	public void setFltPGWPaymentCustomerName(String fltPGWPaymentCustomerName) {
		this.fltPGWPaymentCustomerName = fltPGWPaymentCustomerName;
	}

}
