package com.isa.thinair.ibe.core.web.v2.action.agent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.AgentDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.AgentUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;

/**
 * Agent register process
 * 
 * @author Gavinda Nanayakkara
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class AgentRegisterAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(AgentRegisterAction.class);

	private boolean success = true;

	private String messageTxt;

	private AgentDTO agentDTO;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String agentType = (String) request.getSession().getAttribute("taCo");
			TravelAgentBD travelAgentDelegate = ModuleServiceLocator.getTravelAgentBD();
			Agent agent = AgentUtilV2.getModelAgent(agentDTO, agentType);
			Agent newAgent = travelAgentDelegate.saveAgent(agent);

			if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
				ModuleServiceLocator.getCommoMasterBD().publishSpecificUserDataUpdate(newAgent);
			}
			this.agentDTO = null;

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("AgentRegisterAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("AgentRegisterAction==>", ex);
		}
		return forward;
	}

	public static Log getLog() {
		return log;
	}

	public static void setLog(Log log) {
		AgentRegisterAction.log = log;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public AgentDTO getAgent() {
		return agentDTO;
	}

	public void setAgent(AgentDTO agentDTO) {
		this.agentDTO = agentDTO;
	}

}
