package com.isa.thinair.ibe.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.api.dto.PaxTO;
import com.isa.thinair.ibe.api.dto.PromoChargeRewardTO;
import com.isa.thinair.ibe.api.dto.PromoMetaTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.MessageUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ItineraryUtil;
import com.isa.thinair.ibe.core.web.v2.util.PromotionUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.PromotionRequestTo;
import com.isa.thinair.promotion.api.to.PromotionTemplateTo;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;

@SuppressWarnings("unchecked")
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR)

})
public class PromotionRegisterationAction extends BaseRequestAwareAction {

	private static final Log log = LogFactory.getLog(PromotionRegisterationAction.class);

	private boolean success = true;
	private boolean duplicates = false;
	private boolean unsubscribe = false;
	private String messageTxt = "";

	// input
	private String promReqAttr;
	private String flexiPromoRequestData;
	private PromoChargeRewardTO promoChargeRewardTo;

	// input and output
	private PromotionRequestTo promotionRequestTo = new PromotionRequestTo();

	// out
	private Collection<PaxTO> colAdultCollection = new ArrayList<PaxTO>();
	private Collection<PaxTO> colChildCollection = new ArrayList<PaxTO>();
	private HashMap<String, String> jsonLabel;
	private String totalReward;

	// Moved to PromoMetaTO, apply on F/E
	private String inboundSegmentCode = "";
	private String inboundSegmentIds = "";
	private String outboundSegmentCode = "";
	private String outboundSegmentIds = "";

	private String outboundSegDisplayName = "";
	private String inboundSegDisplayName = "";

	private String outboundDepartureDate;
	private String inboundDepartureDate;

	// output
	private PromoMetaTO metaTo = new PromoMetaTO();

	private IBECommonDTO commonParams;

	private String contactLastName;

	private String promotionId;

	private String language = "en";

	public String execute() {

		String forward = StrutsConstants.Result.ERROR;
		if (promReqAttr != null) {
			language = SessionUtil.getLanguage(request);
			try {

				String[] reqParams = promReqAttr.split(WebConstants.PARAM_SPLIT);

				String pnr = reqParams[0];
				String groupPNR = reqParams[1];
				String cusomerId = reqParams[2];
				String airlineCode = reqParams[3];
				String marketingAirlineCode = reqParams[4];
				promotionId = reqParams[5];

				String[] pagesIDs = { "Common", "pgPromo" };
				jsonLabel = I18NUtil.getMessagesInJSON(language, pagesIDs);
				request.getSession().setAttribute("promoPage", null);

				ServiceResponce validity = ModuleServiceLocator.getPromotionManagementBD().checkPromotionSubscriptions(pnr,
						Integer.parseInt(promotionId));

				if (reqParams.length >= 7) {
					duplicates = true;
					unsubscribe = true;

					if (validity.isSuccess()) {
						messageTxt = "no_subscriptions";
					} else if (validity.getResponseCode().equals(ResponceCodes.PROMOTION_SUBSCRIBE_REPEAT)) {
						messageTxt = "appr_reject";
					} else {
						messageTxt = buildMessage(language, "promotion.unsubcribe.confirmation", promotionId);
					}
					promotionRequestTo.setPromotionId(Integer.parseInt(promotionId));
					promotionRequestTo.setPnr(pnr);
					return StrutsConstants.Result.SUCCESS;
				}

				if (validity.isSuccess()) {

					Customer customer = null;
					if (cusomerId != null && cusomerId != "null" && !cusomerId.isEmpty()) {
						customer = CustomerUtil.getCustomer(Integer.parseInt(cusomerId));
					}

					SYSTEM system = null;
					if (groupPNR.equals("true")) {
						system = SYSTEM.INT;
					} else {
						system = SYSTEM.AA;
					}

					LCCClientReservation reservation = ReservationUtil.loadProxyReservation(pnr, (system == SYSTEM.INT),
							getTrackInfo(), true, (customer != null) ? true : false, airlineCode, marketingAirlineCode, false);

					String resContact = ItineraryUtil.composeName(reservation.getContactInfo().getTitle(), reservation
							.getContactInfo().getFirstName(), reservation.getContactInfo().getLastName());

					contactLastName = reservation.getContactInfo().getLastName();

					promotionRequestTo.setResContactName(resContact);
					promotionRequestTo.setPnr(pnr);

					Object[] paxCol = ItineraryUtil.createPassengerList(reservation);
					colAdultCollection = (Collection<PaxTO>) paxCol[0];
					colChildCollection = (Collection<PaxTO>) paxCol[1];

					Object[] colResSegments = BeanUtil.getOutboundInboundList(reservation.getSegments(), false);
					forward = loadPageDisplayConfigs(Integer.valueOf((promotionId)), colResSegments);

				} else if (validity.getResponseCode().equals(ResponceCodes.PROMOTION_TEMPLATE_NOT_ACTIVE)) {
					forward = StrutsConstants.Result.SUCCESS;
					messageTxt = I18NUtil.getMessage("promotion.no.active.promotion.defined", language);
					duplicates = true;
				} else if (validity.getResponseCode().equals(ResponceCodes.PROMOTION_SUBSRIBED)
						|| validity.getResponseCode().equals(ResponceCodes.PROMOTION_SUBSCRIBE_REPEAT)) {
					forward = StrutsConstants.Result.SUCCESS;
					messageTxt = buildMessage(language, "promotion.already.subcribed", promotionId);
					duplicates = true;
				} else if (validity.getResponseCode().equals(ResponceCodes.PROMOTION_SUBSCRIBE_MULTIPLE)) {
					forward = StrutsConstants.Result.SUCCESS;
					messageTxt = I18NUtil.getMessage("promotion.cannot.subcribe.to.multiple", language);
					duplicates = true;
				}
				commonParams = new IBECommonDTO();
				SystemUtil.setCommonParameters(request, commonParams);
			} catch (ModuleException me) {
				if (me.getExceptionCode().equals(ResponceCodes.PROMOTION_REQUEST_OUT_OF_VALIDITY)) {
					forward = StrutsConstants.Result.SUCCESS;
					messageTxt = I18NUtil.getMessage("promotion.out.of.validity", language);
					duplicates = true;
				} else {
					success = false;
					messageTxt = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, language);
					log.error("Error in Promotion Registration", me);
				}

			} catch (Exception e) {
				success = false;
				messageTxt = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, language);
				log.error("Error in Promotion Registration", e);
			}

		}
		return forward;

	}

	private String loadPageDisplayConfigs(Integer promotionId, Object[] colResSegments) throws ModuleException {

		String forward = StrutsConstants.Result.ERROR;

		int lowerBound = 0;
		int upperBound = 0;

		PromotionTemplateTo persistedPromoTemplate = ModuleServiceLocator.getPromotionManagementBD().loadPromotionTemplateTo(
				promotionId);
		int promotinTypeId = persistedPromoTemplate.getPromotionTypeId();

		PromotionUtil.composePromotionFEMetaInfo((Set<LCCClientReservationSegment>) colResSegments[0], persistedPromoTemplate,
				metaTo, false, getTrackInfo());

		if (colResSegments[1] != null && !((Set<LCCClientReservationSegment>) colResSegments[1]).isEmpty()) {
			PromotionUtil.composePromotionFEMetaInfo((Set<LCCClientReservationSegment>) colResSegments[1],
					persistedPromoTemplate, metaTo, true, getTrackInfo());
		}

		if (log.isDebugEnabled()) {
			log.debug(metaTo.toString());
		}

		// populate using metaTO at F/E
		outboundSegmentCode = metaTo.getOutboundSegmentCode();
		outboundSegDisplayName = metaTo.getOutboundSegDisplayName();
		outboundDepartureDate = metaTo.getOutboundDepartureDate();
		outboundSegmentIds = metaTo.getOutboundSegmentIds();

		inboundSegmentCode = metaTo.getInboundSegmentCode();
		inboundSegDisplayName = metaTo.getInboundSegDisplayName();
		inboundDepartureDate = metaTo.getInboundDepartureDate();
		inboundSegmentIds = metaTo.getInboundSegmentIds();

		if (!metaTo.isOutboundDefined() && !metaTo.isInboudDefined()) {
			duplicates = true;
			messageTxt = buildMessage(language, "promotion.not.defined", promotionId.toString());
			return StrutsConstants.Result.SUCCESS;
		}

		if (promotinTypeId == PromotionType.PROMOTION_NEXT_SEAT_FREE.getPromotionId()) {
			Map<String, String> promoParams = persistedPromoTemplate.getPromotionTemplateParams();

			for (String key : promoParams.keySet()) {
				if (key.equals(PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_SEATS_MIN)) {
					lowerBound = Integer.parseInt(promoParams.get(key));
				} else if (key.equals(PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_SEATS_MAX)) {
					upperBound = Integer.parseInt(promoParams.get(key));
				}

			}

			if (getMetaTo().isOutboundDefined()) {
				promotionRequestTo = PromotionUtil.buildNextSeatCharges(promotionRequestTo, getMetaTo().getOutSegList(),
						persistedPromoTemplate, lowerBound, upperBound, true);
			}

			if (getMetaTo().isInboudDefined()) {
				promotionRequestTo = PromotionUtil.buildNextSeatCharges(promotionRequestTo, getMetaTo().getInSegList(),
						persistedPromoTemplate, lowerBound, upperBound, false);
			}
			forward = StrutsConstants.Result.SUCCESS;

		} else if (promotinTypeId == PromotionType.PROMOTION_FLEXI_DATE.getPromotionId()) {

			Map<String, String> promoParams = persistedPromoTemplate.getPromotionTemplateParams();
			for (String key : promoParams.keySet()) {
				if (key.equals(PromoTemplateParam.TemplateConfig.FLEXI_DATE_LOWER_BOUND)) {
					lowerBound = Integer.parseInt(promoParams.get(key));

				} else if (key.equals(PromoTemplateParam.TemplateConfig.FLEXI_DATE_UPPER_BOUND)) {
					upperBound = Integer.parseInt(promoParams.get(key));
				}

				promotionRequestTo.setLowerBound(lowerBound);
				promotionRequestTo.setUpperBound(upperBound);

			}

			if (getMetaTo().isOutboundDefined()) {
				promotionRequestTo = PromotionUtil.builFlexiDateRewards(promotionRequestTo, getMetaTo().getOutSegList(),
						persistedPromoTemplate, lowerBound, upperBound, false);
			}

			if (getMetaTo().isInboudDefined()) {
				promotionRequestTo = PromotionUtil.builFlexiDateRewards(promotionRequestTo, getMetaTo().getInSegList(),
						persistedPromoTemplate, lowerBound, upperBound, true);
			}

			forward = StrutsConstants.Result.SUCCESS;

		}
		promotionRequestTo.setPromoTypeId(String.valueOf(promotinTypeId));
		promotionRequestTo.setPromotionId(promotionId);
		return forward;

	}

	public String saveFlexidateRequests() {
		String forward = StrutsConstants.Result.ERROR;

		try {

			List<PromotionRequestTo> colPromotionRequestTo = JSONFETOParser.parseCollection(ArrayList.class,
					PromotionRequestTo.class, flexiPromoRequestData);

			List<PromotionRequestTo> colFlexiDateRequestToPersist = new ArrayList<PromotionRequestTo>();

			for (PromotionRequestTo promotionRequestTo : colPromotionRequestTo) {

				String reservationSegIds = promotionRequestTo.getReservationSegId();
				String[] resSegmentIds = reservationSegIds.split(",");

				List<String> segmentsPerBound = new ArrayList<String>(); // outbound or inbound at a time, not both
				for (String resSeg : resSegmentIds) {
					if (!resSeg.isEmpty()) {
						segmentsPerBound.add(resSeg);
					}
				}

				String travelRefNumbers = promotionRequestTo.getFlexiTravelRefNumbers();
				Integer[] pnrPaxIds = PromotionUtil.extractPnrPaxIds(travelRefNumbers);

				for (int j = 0; j < resSegmentIds.length; j++) {

					for (int i = 0; i < pnrPaxIds.length; i++) {
						PromotionRequestTo clone = promotionRequestTo.clone();
						clone.setReservationPaxId(pnrPaxIds[i]);
						clone.setReservationSegId(resSegmentIds[j]);
						clone.setBoundSegmentIds(segmentsPerBound);
						clone.addPromotionRequestParam(PromoRequestParam.FlexiDate.FLEXI_DATE_LOWER_BOUND,
								String.valueOf(promotionRequestTo.getLowerBound()));
						clone.addPromotionRequestParam(PromoRequestParam.FlexiDate.FLEXI_DATE_UPPER_BOUND,
								String.valueOf(promotionRequestTo.getUpperBound()));

						colFlexiDateRequestToPersist.add(clone);
					}

				}

			}

			PromotionType promotionType = PromotionType.PROMOTION_FLEXI_DATE;
			ServiceResponce serviceResponce = ModuleServiceLocator.getPromotionManagementBD().savePromotionRequests(
					colFlexiDateRequestToPersist, null, promotionType, getTrackInfo());

			if (serviceResponce.isSuccess()) {
				request.getSession().setAttribute("promoPage", "FD");
				forward = StrutsConstants.Result.SUCCESS;
			} else {
				log.error("Error in Flexi date promotion requests concurrent update");
			}

		} catch (Exception e) {
			success = false;
			messageTxt = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, language);
			log.error("Error in saving the promotion Flexidate requests", e);

		}
		return forward;
	}

	public String withdrawPromotionRequests() {
		// TODO: get the languagecode
		String forward = StrutsConstants.Result.ERROR;

		try {
			int promoId = promotionRequestTo.getPromotionId();
			String pnr = promotionRequestTo.getPnr();
			duplicates = true;

			ServiceResponce unsubscribeValidator = ModuleServiceLocator.getPromotionManagementBD().withdrawPromotionRequest(pnr,
					Integer.valueOf(promoId));
			if (unsubscribeValidator.isSuccess()) {
				messageTxt = buildMessage(language, "promotion.unsubcribed", String.valueOf(promoId));
			} else {
				if (unsubscribeValidator.getResponseCode().equals(ResponceCodes.PROMOTION_UNSUBSCRIBE_PROCESSED_REPEAT)) {
					messageTxt = buildMessage(language, "promotion.already.unsubscribed.or.processed", String.valueOf(promoId));
				}
			}

			forward = StrutsConstants.Result.SUCCESS;

		} catch (Exception e) {
			success = false;
			messageTxt = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, language);
			log.error("Error in withdrawing promotion requests", e);
		}

		return forward;

	}

	public String calculateChargeAndRewards() {

		String forward = StrutsConstants.Result.ERROR;
		try {
			this.totalReward = PromotionUtil.builFlexiDateRewardsForFE(promoChargeRewardTo);
			forward = StrutsConstants.Result.SUCCESS;
		} catch (Exception e) {
			success = false;
			messageTxt = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, language);
			log.error("Error in in calculation Flexi date rewards", e);
		}

		return forward;

	}

	public void setPromReqAttr(String promReqAttr) {
		this.promReqAttr = promReqAttr;
	}

	public Collection<PaxTO> getColAdultCollection() {
		return colAdultCollection;
	}

	public Collection<PaxTO> getColChildCollection() {
		return colChildCollection;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getInboundSegmentCode() {
		return inboundSegmentCode;
	}

	public String getOutboundSegmentCode() {
		return outboundSegmentCode;
	}

	public String getOutboundSegDisplayName() {
		return outboundSegDisplayName;
	}

	public String getInboundSegDisplayName() {
		return inboundSegDisplayName;
	}

	public String getInboundSegmentIds() {
		return inboundSegmentIds;
	}

	public String getOutboundSegmentIds() {
		return outboundSegmentIds;
	}

	public void setInboundSegmentIds(String inboundSegmentIds) {
		this.inboundSegmentIds = inboundSegmentIds;
	}

	public void setOutboundSegmentIds(String outboundSegmentIds) {
		this.outboundSegmentIds = outboundSegmentIds;
	}

	public PromotionRequestTo getPromotionRequestTo() {
		return promotionRequestTo;
	}

	public void setPromotionRequestTo(PromotionRequestTo promotionRequestTo) {
		this.promotionRequestTo = promotionRequestTo;
	}

	public IBECommonDTO getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public String getOutboundDepartureDate() {
		return outboundDepartureDate;
	}

	public String getInboundDepartureDate() {
		return inboundDepartureDate;
	}

	public boolean isDuplicates() {
		return duplicates;
	}

	public boolean isUnsubscribe() {
		return unsubscribe;
	}

	public PromoChargeRewardTO getPromoChargeRewardTo() {
		return promoChargeRewardTo;
	}

	public void setPromoChargeRewardTo(PromoChargeRewardTO promoChargeRewardTo) {
		this.promoChargeRewardTo = promoChargeRewardTo;
	}

	public String getTotalReward() {
		return totalReward;
	}

	public void setFlexiPromoRequestData(String flexiPromoRequestData) {
		this.flexiPromoRequestData = flexiPromoRequestData;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public void setJsonLabel(HashMap<String, String> jsonLabel) {
		this.jsonLabel = jsonLabel;
	}

	public PromoMetaTO getMetaTo() {
		return metaTo;
	}

	public void setMetaTo(PromoMetaTO metaTo) {
		this.metaTo = metaTo;
	}

	private static String buildMessage(String language, String messageCode, String promotionId) throws ModuleException {
		if (promotionId != null && !promotionId.isEmpty()) {
			String promoType = PromotionUtil.resolvePromotionType(promotionId);
			messageCode = messageCode + "." + promoType;
		}
		return I18NUtil.getMessage(messageCode, language);
	}

}