package com.isa.thinair.ibe.core.web.v2.action.mobile;


import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.PaxValidationTO;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.customer.ShowCustomerRegisterDetailAction;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chethiya Palliyaguruge
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowMobileCustomerDetailAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowCustomerRegisterDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private List countryInfo;

	private List nationalityInfo;

	private String titleInfo;

	private String countryPhoneInfo;

	private PaxValidationTO paxValidationTO = new PaxValidationTO();

	private String secureIBEUrl;

	private Map<String, String> errorInfo;

	private boolean loyaltyEnable;

	private SessionTimeoutDataDTO timeoutDTO;

	private boolean loyaltyManagmentEnabled = false;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();

			paxValidationTO.setChildAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears());
			paxValidationTO.setAdultAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears());
			paxValidationTO.setInfantAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getCutOffAgeInYears());

			setSecureIBEUrl(AppSysParamsUtil.getSecureIBEUrl());
			countryInfo = SelectListGenerator.getCountryList();
			nationalityInfo = SelectListGenerator.getNationalityList();
			titleInfo = JavaScriptGenerator.getTiteVisibilityHtml();
			errorInfo = ErrorMessageUtil.getMobileCustomerProfileErrors(request);
			String[] phoneInfoArray = SelectListGenerator.createTelephoneString();
			countryPhoneInfo = phoneInfoArray[0];
			loyaltyEnable = AppSysParamsUtil.isLoyalityEnabled();
			timeoutDTO = ReservationUtil.fetchSessionTimeoutData(request);

			if (AppSysParamsUtil.isLMSEnabled()) {
				loyaltyManagmentEnabled = true;
			} else {
				loyaltyManagmentEnabled = false;
			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowMobileCustomerDetailAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowCustomerProfileDetailAction==>", ex);
		}
		return forward;
	}

	public SessionTimeoutDataDTO getTimeoutDTO() {
		return timeoutDTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public List getCountryInfo() {
		return countryInfo;
	}

	public void setCountryInfo(List countryInfo) {
		this.countryInfo = countryInfo;
	}

	public String getTitleInfo() {
		return titleInfo;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public List getNationalityInfo() {
		return nationalityInfo;
	}

	public boolean isLoyaltyEnable() {
		return loyaltyEnable;
	}

	public String getCountryPhoneInfo() {
		return countryPhoneInfo;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

	public String getSecureIBEUrl() {
		return secureIBEUrl;
	}

	public void setSecureIBEUrl(String secureIBEUrl) {
		this.secureIBEUrl = secureIBEUrl;
	}

	public PaxValidationTO getPaxValidationTO() {
		return paxValidationTO;
	}

	public void setPaxValidationTO(PaxValidationTO paxValidationTO) {
		this.paxValidationTO = paxValidationTO;
	}

}
