package com.isa.thinair.ibe.core.web.v2.action.payment.payFort;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.common.base.Splitter;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.PaymentGatewayBO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.ibe.api.dto.OnHoldDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.SessionAttribute;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PayFortPaymentUtils;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.opensymphony.xwork2.ActionChainResult;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.BYPASS, type = ActionChainResult.class, value = StrutsConstants.Action.QIWI_INVOICE_NOTIFICATION_HANDLER),
		@Result(name = StrutsConstants.Result.SUCCESS, value = ""),
		@Result(name = StrutsConstants.Result.ERROR, value = ""),
		@Result(name = StrutsConstants.Result.VOUCHER_AVAILABLE, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.OFFLINE_PAYMENT_PAYFORT_VOUCHER, value = StrutsConstants.Jsp.Payment.OFFLINE_PAYMENT_PAYFORT_VOUCHER),
		@Result(name = StrutsConstants.Result.OFFLINE_PAYMENT_PAYFORT, value = StrutsConstants.Jsp.Payment.OFFLINE_PAYMENT_PAYFORT),
		@Result(name = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION_PAY_AT_HOME, value = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION_PAY_AT_HOME),
		@Result(name = StrutsConstants.Action.INTERLINE_PAYMENT, type = ActionChainResult.class, value = StrutsConstants.Action.INTERLINE_PAYMENT), })
public class HandleInterlinePayFortAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(HandleInterlinePayFortAction.class);

	private String voucherID;
	private String requestID;
	private String trackingNumber;
	private String pnr;
	private boolean voucherAvailable = false;
	private boolean trackingNumberAvailable = false;
	private String popupMessage;
	private String valTxnFee;
	private boolean groupPNR;
	private String payAtstoreScriptUrl;

	public String getPayAtstoreScriptUrl() {
		return payAtstoreScriptUrl;
	}

	public void setPayAtstoreScriptUrl(String payAtstoreScriptUrl) {
		this.payAtstoreScriptUrl = payAtstoreScriptUrl;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getValTxnFee() {
		return valTxnFee;
	}

	public void setValTxnFee(String valTxnFee) {
		this.valTxnFee = valTxnFee;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public boolean isTrackingNumberAvailable() {
		return trackingNumberAvailable;
	}

	public void setTrackingNumberAvailable(boolean trackingNumberAvailable) {
		this.trackingNumberAvailable = trackingNumberAvailable;
	}

	public boolean isVoucherAvailable() {
		return voucherAvailable;
	}

	public void setVoucherAvailable(boolean voucherAvailable) {
		this.voucherAvailable = voucherAvailable;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVoucherID() {
		return voucherID;
	}

	public void setVoucherID(String voucherID) {
		this.voucherID = voucherID;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getPopupMessage() {
		return popupMessage;
	}

	public void setPopupMessage(String popupMessage) {
		this.popupMessage = popupMessage;
	}

	// call by the pay@home when redirecting back to merchant when he cancel button is clicked
	public String execute() {

		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		OnHold onHoldApp = OnHold.DEFAULT;
		Date pnrReleaseTimeZulu = null;
		String language = "en";

		Map<String, String> receiptyMap = getReceiptMap(request);
		log.info(receiptyMap);

		String status = receiptyMap.get("status");
		String orderID = receiptyMap.get("orderID");
		String trackingNumber = receiptyMap.get("trackingNumber");

		Properties ipgProps = null;

		// load payment related properties and information
		List<IPGPaymentOptionDTO> pgwList = paymentBrokerBD
				.getActivePaymentGatewayByProviderName(PayFortPaymentUtils.PAY_AT_HOME);
		IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO;
		try {
			ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					payFortPgwConfigs.getPaymentGateway(), payFortPgwConfigs.getBaseCurrency());
			ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
		} catch (ModuleException e) {
			log.info("HandleInterlinePayFortAction!payAtHomeReturnUrlCall : Loading Pay@Store properties Failed");
			return StrutsConstants.Result.ERROR;
		}

		if (status.equals("success") && checkAuthentication(ipgProps, receiptyMap)) {

			TempPaymentTnx tempPaymentTnx = null;
			Reservation reservation = null;
			Integer tempPayId = Integer.parseInt(getTempID(orderID));
			try {
				tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);
				reservation = ReservationModuleUtils.getReservationBD().getReservation(tempPaymentTnx.getPnr(), true);
				pnrReleaseTimeZulu = reservation.getPassengers().iterator().next().getZuluReleaseTimeStamp();

				onHoldApp = OnHold.IBE;

				CreditCardTransaction creditCardTransaction = paymentBrokerBD.loadTransactionByReference(tempPaymentTnx.getPnr());
				String strRequestText = "";
				String strRequestTextCC = creditCardTransaction.getRequestText();
				List<String> sellItems = Arrays.asList(strRequestTextCC.split(","));
				for (int i = 0; i < sellItems.size() - 1; i++) {
					strRequestText += sellItems.get(i) + ",";
				}
				strRequestText += "trackingNumber : " + trackingNumber + "," + sellItems.get(sellItems.size() - 1);

				// CreditCardTransaction ccTransaction = auditTransaction(tempPaymentTnx.getPnr()
				// ,creditCardTransaction.getTransactionRefCccompany() , creditCardTransaction.getTempReferenceNum(),
				// creditCardTransaction.getTemporyPaymentId(),creditCardTransaction.getServiceType() ,
				// strRequestText, creditCardTransaction.getResponseText() ,
				// creditCardTransaction.getPaymentGatewayConfigurationName() , creditCardTransaction.getTransactionId()
				// , false);
				//

				CreditCardTransaction ccTransaction = loadAuditTransactionByTmpPayID(tempPaymentTnx.getTnxId());
				updateAuditTransactionByTnxRefNo(ccTransaction, strRequestText, ipgIdentificationParamsDTO.getIpgId().toString());

				if (ccTransaction != null) {
					Calendar onHoldCalander = Calendar.getInstance();
					onHoldCalander.setTime(pnrReleaseTimeZulu);
					String onHDReleaseDisplay = ReservationUtil.getOnHoldDisplayTime(onHoldApp, null, null,
							onHoldCalander.getTime());

					OnHoldDTO onHoldDTO = new OnHoldDTO();
					onHoldDTO.setPnr(tempPaymentTnx.getPnr());
					onHoldDTO.setReleaseTime(CommonUtil.wrapWithDDHHMM(onHDReleaseDisplay));
					request.setAttribute(SessionAttribute.KSK_CONF_DATA, JSONUtil.serialize(onHoldDTO));
					request.setAttribute(WebConstants.SESSION_TIMEOUT,
							CommonUtil.convertToJSON(ReservationUtil.fetchSessionTimeoutData(request)));
					request.setAttribute("trackingNumber", trackingNumber);
					request.setAttribute("language", language);
					request.setAttribute("payAtHomeCancelText", "none");
					request.setAttribute("pnr", tempPaymentTnx.getPnr());
					Map<String, String> jsonLabel = I18NUtil.getMessagesInJSON(language, "PgOHDconfirm");
					request.setAttribute(ReservationWebConstnts.OHD_LABELS, JSONUtil.serialize(jsonLabel));
				} else {
					log.info("HandleInterlinePayFortAction!payAtHomeReturnUrlCall : Updating the temp trasaction Failed");
					return StrutsConstants.Result.ERROR;
				}

			} catch (ModuleException e) {
				log.info("HandleInterlinePayFortAction!payAtHomeReturnUrlCall : Loading Tempary Trasaction Failed", e);
				return StrutsConstants.Result.ERROR;
			} catch (org.apache.struts2.json.JSONException e) {
				log.info("HandleInterlinePayFortAction!payAtHomeReturnUrlCall : Loading Tempary Trasaction Failed", e);
				return StrutsConstants.Result.ERROR;
			}

			return StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION_PAY_AT_HOME;

		}

		return StrutsConstants.Result.ERROR;
	}

	public String payAtStoreNotificationCall() {

		log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: Start");

		if (log.isDebugEnabled()) {
			log.debug("### Start.. HandleInterlinePayFortAction " + request.getRequestedSessionId());
		}

		String responseStr = null;
		Map<String, String> NotificationMap = new HashMap<String, String>();
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		try {

			Map<String, String> receiptyMap = getReceiptMap(request);
			log.info(receiptyMap);

			// get notification info to map
			String XMLNotification = receiptyMap.get(PayFortPaymentUtils.NOTIFICATION);
			NotificationMap = getNotificationMap(XMLNotification);

			// load payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					PayFortPaymentUtils.PAY_AT_STORE);
			IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					payFortPgwConfigs.getPaymentGateway(), payFortPgwConfigs.getBaseCurrency());

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			TempPaymentTnx tempPaymentTnx = null;

			if (ipgProps != null && NotificationMap.size() > 0 && checkAuthentication(ipgProps, NotificationMap)) {
				String orderId = NotificationMap.get(PayFortPaymentUtils.ORDER_ID);
				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
				Integer tempPayId = Integer.parseInt(getTempID(orderId));
				Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);
				ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);
				NotificationMap.put("responseType", "PAYFORTGENARATEPAYMENTREFARANCE");

				tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);

				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(tempPaymentTnx.getPnr());
				modes.setRecordAudit(false);

				requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);

				Calendar cal = Calendar.getInstance();
				cal.setTime(ipgResponseDTO.getResponseTimeStamp());

				NotificationMap.remove("responseType");
				NotificationMap.put("responseType", "PAYMENTSUCCESS");
				NotificationMap.put("pnr", tempPaymentTnx.getPnr());
				NotificationMap.put(PayFortPaymentUtils.TICKET_NUMBER, tempPaymentTnx.getPnr());

				ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(NotificationMap, ipgResponseDTO,
						ipgIdentificationParamsDTO);

			} else {
				log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: Basic Authorization Failure");
				return StrutsConstants.Result.ERROR;
			}
			responseStr = dataToString(NotificationMap);
			log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: Failed Confirm the Booking " + responseStr);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("HandleInterlinePayFortAction payAtStoreNotificationCall: Failed Confirm the Booking");
		}

		log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: End");
		return StrutsConstants.Result.SUCCESS;

	}

	public String payAtHomeNotificationCall() {

		log.info("HandleInterlinePayFortAction:payAtHomeNotificationCall : Start");

		if (log.isDebugEnabled()) {
			log.debug("### Start.. HandleInterlinePayFortAction:payAtHomeNotificationCall " + request.getRequestedSessionId());
		}

		String responseStr = null;
		Map<String, String> NotificationMap = new HashMap<String, String>();
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		try {

			Map<String, String> receiptyMap = getReceiptMap(request);
			log.info(receiptyMap);

			// get notification info to map
			String XMLNotification = receiptyMap.get(PayFortPaymentUtils.NOTIFICATION);
			NotificationMap = getNotificationMap(XMLNotification);

			// load payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					PayFortPaymentUtils.PAY_AT_HOME);
			IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					payFortPgwConfigs.getPaymentGateway(), payFortPgwConfigs.getBaseCurrency());

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			TempPaymentTnx tempPaymentTnx = null;

			if (ipgProps != null && NotificationMap.size() > 0 && checkAuthentication(ipgProps, NotificationMap)) {
				String orderId = NotificationMap.get(PayFortPaymentUtils.ORDER_ID);
				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
				Integer tempPayId = Integer.parseInt(getTempID(orderId));
				Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);
				ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);
				NotificationMap.put("responseType", "PAYFORTGENARATEPAYMENTREFARANCE");

				tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);

				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(tempPaymentTnx.getPnr());
				modes.setRecordAudit(false);

				requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);

				Calendar cal = Calendar.getInstance();
				cal.setTime(ipgResponseDTO.getResponseTimeStamp());

				NotificationMap.remove("responseType");
				NotificationMap.put("responseType", "PAYMENTSUCCESS");
				NotificationMap.put("pnr", tempPaymentTnx.getPnr());
				NotificationMap.put(PayFortPaymentUtils.TICKET_NUMBER, tempPaymentTnx.getPnr());

				ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(NotificationMap, ipgResponseDTO,
						ipgIdentificationParamsDTO);

			} else {
				log.info("HandleInterlinePayFortAction payAtHomeNotificationCall: Basic Authorization Failure");
				return StrutsConstants.Result.ERROR;
			}
			responseStr = dataToString(NotificationMap);
			log.info("HandleInterlinePayFortAction payAtHomeNotificationCall: Failed Confirm the Booking " + responseStr);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("HandleInterlinePayFortAction payAtHomeNotificationCall: Failed Confirm the Booking");
		}

		log.info("HandleInterlinePayFortAction : End");
		return StrutsConstants.Result.SUCCESS;

	}

	/*
	 * This method is a generic method to receive the payfort notification. if using this method can omit using
	 * payAtHomeNotificationCall() and payAtStoreNotificationCall() methods to handle notifications separately. if using
	 * this method 'PAYatHOME Integration URL' and 'PAYatSTORE Integration URL' should be updated to
	 * '{IBESECUREURL}/public/handleInterlinePayFort!payAtHomeNotificationCall.action'
	 */
	public String payFortPayAtNotification() {

		log.info("HandleInterlinePayFortAction payFortPayAtNotification: Start");

		if (log.isDebugEnabled()) {
			log.debug("### Start.. HandleInterlinePayFortAction " + request.getRequestedSessionId());
		}

		String responseStr = null;
		Map<String, String> NotificationMap = new HashMap<String, String>();
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		try {

			Map<String, String> receiptyMap = getReceiptMap(request);
			log.info(receiptyMap);

			// get notification info to map
			String XMLNotification = receiptyMap.get(PayFortPaymentUtils.NOTIFICATION);
			NotificationMap = getNotificationMap(XMLNotification);

			// filter payment type
			String paymentType = NotificationMap.get(PayFortPaymentUtils.PAYMENT);
			String IPGName = "";
			if (paymentType.equals("PAYatSTORE")) {
				IPGName = PayFortPaymentUtils.PAY_AT_STORE;
			} else if (paymentType.equals("PAYatHOME")) {
				IPGName = PayFortPaymentUtils.PAY_AT_HOME;
			}
			// load payment related properties and information
			if (!IPGName.equals("")) {
				List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD()
						.getActivePaymentGatewayByProviderName(IPGName);
				IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil
						.validateAndPrepareIPGConfigurationParamsDTO(payFortPgwConfigs.getPaymentGateway(),
								payFortPgwConfigs.getBaseCurrency());

				Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
				TempPaymentTnx tempPaymentTnx = null;

				if (ipgProps != null && NotificationMap.size() > 0 && checkAuthentication(ipgProps, NotificationMap)) {
					String orderId = NotificationMap.get(PayFortPaymentUtils.ORDER_ID);
					IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
					Integer tempPayId = Integer.parseInt(getTempID(orderId));
					Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
					ipgResponseDTO.setRequestTimsStamp(requestTime);
					ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
					ipgResponseDTO.setTemporyPaymentId(tempPayId);
					ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);

					tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);

					requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
					ipgResponseDTO.setRequestTimsStamp(requestTime);
					ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
					ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
					ipgResponseDTO.setTemporyPaymentId(tempPayId);

					NotificationMap.put("pnr", tempPaymentTnx.getPnr());
					NotificationMap.put(PayFortPaymentUtils.TICKET_NUMBER, tempPaymentTnx.getPnr());

					ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(NotificationMap, ipgResponseDTO,
							ipgIdentificationParamsDTO);

				} else {
					log.info("HandleInterlinePayFortAction payFortPayAtNotification: Basic Authorization Failure");
					return StrutsConstants.Result.ERROR;
				}
				responseStr = dataToString(NotificationMap);
				log.info("HandleInterlinePayFortAction payFortPayAtNotification: Failed Confirm the Booking " + responseStr);
			} else {
				log.info("HandleInterlinePayFortAction payFortPayAtNotification: No Payment Type From Notification");
				return StrutsConstants.Result.ERROR;
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("HandleInterlinePayFortAction payFortPayAtNotification: Failed Confirm the Booking");
		}

		log.info("HandleInterlinePayFortAction payFortPayAtNotification: End");
		return StrutsConstants.Result.SUCCESS;

	}

	public String showVoucher() {

		request.setAttribute(Pay_AT_StoreResponseDTO.VOUCHER_ID, getVoucherID());
		request.setAttribute(Pay_AT_StoreResponseDTO.REQUEST_ID, getRequestID());
		request.setAttribute(Pay_AT_StoreResponseDTO.PAY_AT_STORE_SCRIPT_URL, getPayAtstoreScriptUrl());

		return StrutsConstants.Result.OFFLINE_PAYMENT_PAYFORT_VOUCHER;
	}

	public String showVoucherDisplay() {

		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		try {
			// load payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = paymentBrokerBD
					.getActivePaymentGatewayByProviderName(PayFortPaymentUtils.PAY_AT_STORE);
			IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO;
			ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					payFortPgwConfigs.getPaymentGateway(), payFortPgwConfigs.getBaseCurrency());
			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			String scriptUrl = ipgProps.getProperty("payAtstoreScriptUrl");

			if (scriptUrl.length() > 0) {
				String waitingMsg = getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR
						+ PayFortPaymentUtils.PAYFORT_PAYSTORE_PAYMENT_DISPLAY_AGAIN);
				String[] pnrStr = pnr.split(",");
				waitingMsg = waitingMsg.replace("{0}", pnrStr[0].trim());

				request.setAttribute(StrutsConstants.Result.OFFLINE_PAYMENT_INFO, waitingMsg);
				request.setAttribute(WebConstants.PNR, pnrStr[0].trim());
				request.setAttribute(Pay_AT_StoreResponseDTO.REQUEST_ID, requestID);
				request.setAttribute(Pay_AT_StoreResponseDTO.VOUCHER_ID, voucherID);
				request.setAttribute(ReservationWebConstnts.LANGUAGE, "en");
				request.setAttribute(Pay_AT_StoreResponseDTO.PAY_AT_STORE_SCRIPT_URL, scriptUrl);
				return StrutsConstants.Result.OFFLINE_PAYMENT_PAYFORT;

			} else {
				log.error("PayAtStore script URL not specified in the configuration.");
				return StrutsConstants.Result.ERROR;
			}

		} catch (ModuleException e) {
			e.printStackTrace();
			log.error(" PayATStore Property Reading Errora: " + e.getStackTrace());
			return StrutsConstants.Result.ERROR;
		}

	}

	public String payAtHomeOfflineCall() {

		Map<String, String> NotificationMap = new HashMap<String, String>();
		Map<String, String> receiptyMap = getReceiptMap(request);
		log.info(receiptyMap);
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		// get notification info to map
		String XMLNotification = receiptyMap.get(PayFortPaymentUtils.NOTIFICATION);
		NotificationMap = getOfflineNotificationMap(XMLNotification);
		// TO-DO need to filter using the service
		String notifiedOrderId = NotificationMap.get(PayFortPaymentUtils.ORDER_ID);

		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();

		try {

			Integer tempPayId = Integer.parseInt(getTempID(notifiedOrderId));
			Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
			ipgResponseDTO.setRequestTimsStamp(requestTime);
			ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
			ipgResponseDTO.setTemporyPaymentId(tempPayId);
			ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);
			NotificationMap.put("responseType", "PAYATHOMEREASSIGNORDER");
			NotificationMap.put("status", PayFortPaymentUtils.SUCCESS);

			TempPaymentTnx tempPaymentTnx = null;

			// load payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					PayFortPaymentUtils.PAY_AT_HOME);
			IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					payFortPgwConfigs.getPaymentGateway(), payFortPgwConfigs.getBaseCurrency());

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			String serviceName = ipgProps.getProperty("serviceName");
			NotificationMap.put("serviceName", serviceName);

			tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);
			Collection<Integer> colPaymentTnxIds = new ArrayList<Integer>();
			colPaymentTnxIds.add(tempPayId);
			String status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE;
			PaymentGatewayBO.updateTempPaymentEntries(colPaymentTnxIds, status, null, tempPaymentTnx.getLast4DigitsCC(), true,
					null, null);
		} catch (ModuleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String checkPayFort() {

		String pnr = getPnr();
		String tnxVal = getValTxnFee();
		if (!"".equals(pnr) && !"".equals(tnxVal)) {
			CreditCardTransaction creditCardTransaction = null;
			try {
				creditCardTransaction = ModuleServiceLocator.getPaymentBrokerBD().loadTransactionByReference(pnr);
			} catch (ModuleException e1) {
				log.info("HandleInterlinePayFortAction!checkPayFort : Loading Tempary Trasaction Failed", e1);
			}
			if (creditCardTransaction != null) {

				TempPaymentTnx tempPaymentTnx = null;
				BigDecimal ccAmount = null;
				BigDecimal resAmount = null;
				Reservation reservation = null;
				LCCClientReservation commonReservation = null;
				BigDecimal tnxValAmount = new BigDecimal(tnxVal);
				try {
					if (groupPNR) {

						LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, groupPNR, false, "", "", true);
						commonReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO,
								null, getTrackInfo());
						tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(
								creditCardTransaction.getTemporyPaymentId());
						ccAmount = tempPaymentTnx.getAmount();
						resAmount = AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.add(
								commonReservation.getTotalChargeAmount(), tnxValAmount));

					} else {

						reservation = ReservationModuleUtils.getReservationBD().getReservation(pnr, true);
						tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(
								creditCardTransaction.getTemporyPaymentId());
						ccAmount = tempPaymentTnx.getAmount();
						resAmount = AccelAeroCalculator.add(reservation.getTotalChargeAmount(), tnxValAmount);

					}
				} catch (CommonsDataAccessException e) {
					log.error("Reservation Load Fails for check already exists payment entries beacause: " + e.getStackTrace());
					return StrutsConstants.Result.ERROR;

				} catch (ModuleException e) {
					log.error("Reservation Load Fails for check already exists payment entries beacause: " + e.getStackTrace());
					return StrutsConstants.Result.ERROR;
				}
				String requestText = creditCardTransaction.getRequestText();

				if ((reservation != null || commonReservation != null)
						&& ccAmount != null
						&& (ccAmount.equals(resAmount) || (-0.5 < AccelAeroCalculator.subtract(ccAmount, resAmount).doubleValue() && AccelAeroCalculator
								.subtract(ccAmount, resAmount).doubleValue() < 0.5))) {
					if (requestText.contains(Pay_AT_StoreResponseDTO.VOUCHER_ID)
							&& requestText.contains(Pay_AT_StoreResponseDTO.REQUEST_ID)) {
						List<String> sellItems = Arrays.asList(requestText.split(","));
						String strRequestText = "";
						for (int i = 0; i < sellItems.size() - 1; i++) {
							strRequestText += sellItems.get(i) + ",";
						}
						strRequestText = strRequestText.substring(0, strRequestText.length() - 1);
						Map<String, String> params = Splitter.on(",").withKeyValueSeparator(":").split(strRequestText);
						setVoucherID(params.get("voucherID ").trim());
						setRequestID(params.get("requestID ").trim());
						setVoucherAvailable(true);

						setPopupMessage(getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR
								+ PayFortPaymentUtils.PAYFORT_PAYSTORE_VOUCHER_POPUP_MESSAGE));
					} else if (requestText.contains("orderReference") && requestText.contains("trackingNumber")) {

						List<String> sellItems = Arrays.asList(requestText.split(","));
						String strRequestText = "";
						for (int i = 0; i < sellItems.size() - 1; i++) {
							strRequestText += sellItems.get(i) + ",";
						}
						strRequestText = strRequestText.substring(0, strRequestText.length() - 1);
						Map<String, String> params = Splitter.on(",").withKeyValueSeparator(":").split(strRequestText);
						setTrackingNumberAvailable(true);
						String popUpMsg = getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR
								+ PayFortPaymentUtils.PAYFORT_PAYHOME_TRACK_NO_POPUP_MESSAGE);
						popUpMsg = popUpMsg.replace("{0}", params.get("trackingNumber ").trim());
						setPopupMessage(popUpMsg);
					} else if (requestText.contains("orderReference")) {
						setTrackingNumberAvailable(true);
						setPopupMessage(null);
					}
				}
			}
		}

		return StrutsConstants.Result.VOUCHER_AVAILABLE;
	}

	/**
	 * Returns ReceiptMap. Map will holds all parameters send by the payment gateway. It is up the payment broker
	 * implementation extract the required parameter and values
	 * 
	 * @param request
	 * @return
	 */
	private static Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	/**
	 * Returns Notification Map from PayFort. Map will holds all parameters send by the payment gateway. It is up the
	 * payment broker implementation extract the required parameter and values
	 * 
	 * @param request
	 * @return
	 */
	private static Map<String, String> getNotificationMap(String XMLNotification) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		Document document;

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			document = builder.parse(new InputSource(new StringReader(XMLNotification)));
			Element rootElement = document.getDocumentElement();

			String merchantID = getString(PayFortPaymentUtils.MERCHANT_ID, rootElement);
			fields.put(PayFortPaymentUtils.MERCHANT_ID, merchantID);
			String orderID = getString(PayFortPaymentUtils.ORDER_ID, rootElement);
			fields.put(PayFortPaymentUtils.ORDER_ID, orderID);
			String amount = getString("amount", rootElement);
			fields.put("amount", amount);
			String currency = getString(PayFortPaymentUtils.CURRENCY, rootElement);
			fields.put(PayFortPaymentUtils.CURRENCY, currency);
			String payment = getString(PayFortPaymentUtils.PAYMENT, rootElement);
			fields.put(PayFortPaymentUtils.PAYMENT, payment);
			String serviceName = getString(PayFortPaymentUtils.SERVICE_NAME, rootElement);
			fields.put(PayFortPaymentUtils.SERVICE_NAME, serviceName);
			String signature = getString(PayFortPaymentUtils.SIGNATURE, rootElement);
			fields.put(PayFortPaymentUtils.SIGNATURE, signature);

		} catch (ParserConfigurationException e) {
			log.error(e);
		} catch (SAXException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}

		return fields;
	}

	private static Map<String, String> getOfflineNotificationMap(String XMLNotification) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		Document document;
		boolean changeOrder = false;
		boolean reassignOrder = false;
		boolean trackingNumber = false;

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			document = builder.parse(new InputSource(new StringReader(XMLNotification)));
			document.getDocumentElement().normalize();

			NodeList header = document.getElementsByTagName("header");
			NodeList orderInfoList = document.getElementsByTagName("orderInfo");
			NodeList changeOrderList = null;
			NodeList reassignOrderList = null;
			NodeList trackingNumberList = null;

			Node headerNode = header.item(0);
			Node orderInfoNode = orderInfoList.item(0);
			Node changeOrderNode = null;
			Node reassignOrderNode = null;
			Node trackingNumberNode = null;

			if (headerNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) headerNode;

				String msgType = eElement.getElementsByTagName("msgType").item(0).getTextContent();
				if (msgType.equals("CHANGE_ORDER_STATUS")) {
					changeOrder = true;
					fields.put("service", "CHANGE_ORDER_STATUS");
					changeOrderList = document.getElementsByTagName("CHANGE_ORDER_STATUS");
					changeOrderNode = changeOrderList.item(0);
				} else if (msgType.equals("REASSIGN_ORDER")) {
					reassignOrder = true;
					fields.put("service", "REASSIGN_ORDER");
					reassignOrderList = document.getElementsByTagName("REASSIGN_ORDER");
					reassignOrderNode = reassignOrderList.item(0);
				} else if (msgType.equals("TRACKING_NUMBER")) {
					trackingNumber = true;
					fields.put("service", "TRACKING_NUMBER");
					trackingNumberList = document.getElementsByTagName("TRACKING_NUMBER");
					trackingNumberNode = trackingNumberList.item(0);
				}

			}
			if (orderInfoNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) orderInfoNode;

				String merchantID = eElement.getElementsByTagName(PayFortPaymentUtils.MERCHANT_ID).item(0).getTextContent();
				fields.put(PayFortPaymentUtils.MERCHANT_ID, merchantID);
				String orderID = eElement.getElementsByTagName(PayFortPaymentUtils.ORDER_ID).item(0).getTextContent();
				fields.put(PayFortPaymentUtils.ORDER_ID, orderID);
				String currency = eElement.getElementsByTagName(PayFortPaymentUtils.CURRENCY).item(0).getTextContent();
				fields.put(PayFortPaymentUtils.CURRENCY, currency);

			}
			if (changeOrder && changeOrderNode != null && changeOrderNode.getNodeType() == Node.ELEMENT_NODE) {

				changeOrderNode = changeOrderList.item(0);
				Element eElement = (Element) changeOrderNode;

				String fromStatus = eElement.getElementsByTagName("fromStatus").item(0).getTextContent();
				fields.put("fromStatus", fromStatus);
				String toStatus = eElement.getElementsByTagName("toStatus").item(0).getTextContent();
				fields.put("toStatus", toStatus);
				String expiryDate = eElement.getElementsByTagName("expiryDate").item(0).getTextContent();
				fields.put("expiryDate", expiryDate);

			}
			if (reassignOrder && reassignOrderNode != null && reassignOrderNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) reassignOrderNode;

				String trackingNumber1 = eElement.getElementsByTagName("trackingNumber").item(0).getTextContent();
				fields.put("trackingNumber", trackingNumber1);

			}
			if (trackingNumber && trackingNumberNode != null && trackingNumberNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) trackingNumberNode;

				String trackingNumbe = eElement.getElementsByTagName("trackingNumber").item(0).getTextContent();
				fields.put("trackingNumbe", trackingNumbe);

			}
			// add signature validation

		} catch (ParserConfigurationException e) {
			log.error(e);
		} catch (SAXException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}

		return fields;
	}

	private static String getString(String tagName, Element element) {
		NodeList list = element.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();

			if (subList != null && subList.getLength() > 0) {
				return subList.item(0).getNodeValue();
			}
		}

		return null;
	}

	private boolean checkAuthentication(Properties ipgProps, Map<String, String> NotificationMap) {

		String storeID = ipgProps.getProperty("merchantId");
		String merchantSymenticKey = ipgProps.getProperty("encryptionKey");
		String serviceName = ipgProps.getProperty("serviceName");

		// TO-DO need to replace "merchantID" and "serviceName" with proper way
		if (NotificationMap.get(PayFortPaymentUtils.MERCHANT_ID).equals(storeID)
				&& NotificationMap.get(PayFortPaymentUtils.SERVICE_NAME).equals(serviceName)) {
			String merchantID = NotificationMap.get(PayFortPaymentUtils.MERCHANT_ID);
			String orderID = NotificationMap.get(PayFortPaymentUtils.ORDER_ID);
			String amount = NotificationMap.get("amount");
			String currency = NotificationMap.get(PayFortPaymentUtils.CURRENCY);
			String payment = NotificationMap.get(PayFortPaymentUtils.PAYMENT);
			String serviceNameNotified = NotificationMap.get(PayFortPaymentUtils.SERVICE_NAME);
			String signatureNotified = NotificationMap.get(PayFortPaymentUtils.SIGNATURE);
			String signature = "";
			if (NotificationMap.containsKey("orderReference")) {

				String status = NotificationMap.get("status");
				String orderReference = NotificationMap.get("orderReference");
				String trackingNumber = NotificationMap.get("trackingNumber");

				signature = amount + "" + currency + "" + merchantID + "" + orderID + "" + orderReference + ""
						+ serviceNameNotified + "" + status + "" + trackingNumber + "" + merchantSymenticKey;
			} else {

				signature = amount + "" + currency + "" + merchantID + "" + orderID + "" + payment + "" + serviceNameNotified
						+ "" + merchantSymenticKey;

			}
			String signatureSHA1 = "";
			try {
				signatureSHA1 = SHA1(signature);
			} catch (NoSuchAlgorithmException e) {
				log.error(e);
			} catch (UnsupportedEncodingException e) {
				log.error(e);
			}

			if (signatureSHA1.equals(signatureNotified)) {
				return true;
			} else {
				return false;
			}

		}

		return false;
	}

	private static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md;
		md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		sha1hash = md.digest();
		return convertToHex(sha1hash);
	}

	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	private String getTempID(String orderID) {
		String tempPayID = "";
		if (orderID != null) {
			tempPayID = orderID.substring(1);
		}
		return tempPayID;
	}

	private static String getServerErrorMessage(HttpServletRequest request, String key) {
		return ReservationUtil.getServerErrorMessage(request, key);
	}

	private static String dataToString(Map<String, String> dataMap) {
		String resString = "";
		for (Map.Entry<String, String> entry : dataMap.entrySet()) {
			resString += entry.getKey() + ":" + entry.getValue() + ", ";
		}
		resString = resString.substring(0, resString.length() - 1);
		return resString;
	}

	private CreditCardTransaction loadAuditTransactionByTmpPayID(int temporyPayId) throws ModuleException {
		CreditCardTransaction creditCardTransaction = ModuleServiceLocator.getPaymentBrokerBD().loadTransactionByTempPayId(
				temporyPayId);
		return creditCardTransaction;
	}

	private void updateAuditTransactionByTnxRefNo(CreditCardTransaction creditCardTransaction, String plainTextRequest,
			String paymentID) throws ModuleException {
		creditCardTransaction.setRequestText(plainTextRequest);
		creditCardTransaction.setGatewayPaymentID(paymentID);
		ModuleServiceLocator.getPaymentBrokerBD().saveTransaction(creditCardTransaction);
	}

}
