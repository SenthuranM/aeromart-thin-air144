package com.isa.thinair.ibe.core.web.v2.action.voucher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;



@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name =  StrutsConstants.Result.SUCCESS, value =  StrutsConstants.Jsp.Common.PRINT_VOUCHER),
	@Result(name =  StrutsConstants.Result.ERROR, value =  StrutsConstants.Jsp.Common.ERROR) })

public class PrintVoucherIBEAction extends BaseRequestAwareAction {
	
	private static Log log = LogFactory.getLog(IssueVoucherConfirmationAction.class);
	private String validFrom;
	private String validTo;
	private String amountInLocal;
	private String currencyCode;
	private String paxFirstName;
	private String paxLastName;
	private String voucherId;
	private String templateId;
	private VoucherDTO voucherDTO = new VoucherDTO();
	
	public String execute() {

		String forward = StrutsConstants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			voucherDTO.setValidFrom(validFrom);
			voucherDTO.setValidTo(validTo);
			voucherDTO.setAmountInLocal(amountInLocal);
			voucherDTO.setCurrencyCode(currencyCode);
			voucherDTO.setPaxFirstName(paxFirstName);
			voucherDTO.setPaxLastName(paxLastName);
			voucherDTO.setVoucherId(voucherId);
			if (templateId != null) {
				voucherDTO.setTemplateId(Integer.parseInt(templateId));
			}

			String printHtmlContent = "";

			try {
				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				printHtmlContent += voucherDelegate.printVoucher(voucherDTO);

				request.setAttribute("printVoucherBodyHtml", printHtmlContent);

			} catch (ModuleException me) {
				log.error("PrintVoucherAction ==> execute() printing voucher failed", me);
				if (me.getExceptionCode().equals("promotions.voucher.amount.local.null")) {
					forward = StrutsConstants.Result.ERROR;
				}
			}
		}
		return forward;
	}

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the validFrom
	 */
	public String getValidFrom() {
		return validFrom;
	}
	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	/**
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}
	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	/**
	 * @return the amountInLocal
	 */
	public String getAmountInLocal() {
		return amountInLocal;
	}
	/**
	 * @param amountInLocal the amountInLocal to set
	 */
	public void setAmountInLocal(String amountInLocal) {
		this.amountInLocal = amountInLocal;
	}
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}
	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	/**
	 * @return the paxFirstName
	 */
	public String getPaxFirstName() {
		return paxFirstName;
	}
	/**
	 * @param paxFirstName the paxFirstName to set
	 */
	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}
	/**
	 * @return the paxLastName
	 */
	public String getPaxLastName() {
		return paxLastName;
	}
	/**
	 * @param paxLastName the paxLastName to set
	 */
	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}
	/**
	 * @return the voucherId
	 */
	public String getVoucherId() {
		return voucherId;
	}
	/**
	 * @param voucherId the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}
	/**
	 * @return the voucherDTO
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}
	/**
	 * @param voucherDTO the voucherDTO to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

}
