package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;

/**
 * Separate Action for load page build details - avoid user longer waiting because of Flight Search
 * 
 * @author Pradeep Karunanayake
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class AvailabilitySearchPageDetailAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(AvailabilitySearchCalendarAction.class);

	private boolean blnShowOneWay = false;

	private HashMap<String, String> jsonLabel;

	private Map<String, String> errorInfo;

	private String currentDate;

	private boolean success = true;

	private String messageTxt;

	private SessionTimeoutDataDTO timeoutDTO;

	private String termsNCond;

	private String carrier;

	private String airportMessage;

	private boolean showPriceBDInSelCurr;

	private boolean fromPostCardPage;

	private String msgFromPostCardPage;
	private String promoCodeNotFound;

	@SuppressWarnings("unchecked")
	public String execute() {

		String result = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
			// Set Session Attribute for track user itinerary confirmation page refresh
			request.getSession().setAttribute("fromItinearyPage", false);
			if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
					&& !tempPaymentPNR.trim().isEmpty()) {
				messageTxt = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
				success = false;
				return result;
			}

			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			if (resInfo != null && resInfo.isFromPostCardDetails()) {
				fromPostCardPage = resInfo.isFromPostCardDetails();
				msgFromPostCardPage = I18NUtil.getMessage("msg.postCardPage", strLanguage);
			}
			reSetResInfoForFreshReservation(resInfo);
			promoCodeNotFound = I18NUtil.getMessage("msg.promoCodeNotFound", strLanguage);
			String[] pagesIDs = { "Common", "Form", "PgFlightSearch", "PgSearch" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			errorInfo = ErrorMessageUtil.getFlightNFareSearchErrors(request);
			// IBE return one way enabled
			blnShowOneWay = AppSysParamsUtil.isIBEReturnEnabled();
			timeoutDTO = ReservationUtil.fetchSessionTimeoutData(request);
			termsNCond = ReservationHG.createTermsNCond(strLanguage);

			SimpleDateFormat smf = new SimpleDateFormat("dd/MM/yyyy");
			this.currentDate = smf.format(new Date());
			carrier = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			this.currentDate = smf.format(new Date());
			showPriceBDInSelCurr = AppSysParamsUtil.showIBEPriceBreakdownInSelectedCurrency();
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("AvailabilitySearchCalendarAction==>", ex);
		}

		return result;
	}

	private void reSetResInfoForFreshReservation(IBEReservationInfoDTO resInfo) {
		if (resInfo != null && !resInfo.isFromPostCardDetails() && resInfo.getPnr() == null) {
			resInfo.setReservationStatus(null);
		}
	}

	public boolean isBlnShowOneWay() {
		return blnShowOneWay;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public SessionTimeoutDataDTO getTimeoutDTO() {
		return timeoutDTO;
	}

	public String getTermsNCond() {
		return termsNCond;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public boolean isShowPriceBDInSelCurr() {
		return showPriceBDInSelCurr;
	}

	public boolean isFromPostCardPage() {
		return fromPostCardPage;
	}

	public String getMsgFromPostCardPage() {
		return msgFromPostCardPage;
	}

	public String getPromoCodeNotFound() {
		return promoCodeNotFound;
	}

	public void setPromoCodeNotFound(String promoCodeNotFound) {
		this.promoCodeNotFound = promoCodeNotFound;
	}
}
