package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONObject;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAdminInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.AncillaryPaxTo;
import com.isa.thinair.ibe.api.dto.BookingTO;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.CustomerCreditDTO;
import com.isa.thinair.ibe.api.dto.DisplayUtilDTO;
import com.isa.thinair.ibe.api.dto.FareDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEPromotionPaymentDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.ItineraryPaymentSummaryTO;
import com.isa.thinair.ibe.api.dto.PaxTO;
import com.isa.thinair.ibe.api.dto.ReservationProcessDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.RequestParameter;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.ibe.core.web.generator.createres.ShowAddPaxHG;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.BalanceSummaryUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ItineraryUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.FlexiDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowLoadReservationAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowLoadReservationAction.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private Collection<FlightInfoTO> flightDepDetails;

	private Collection<FlightInfoTO> flightRetDetails;

	private ItineraryPaymentSummaryTO itineraryPaymentSummaryTO;

	private HashMap<String, String> jsonLabel;

	private int adultCount = 0;

	private int childCount = 0;

	private int infantCount = 0;

	private Set<LCCClientReservationSegment> segments;

	private ContactInfoDTO contactInfo = new ContactInfoDTO();

	private BookingTO bookingTO;

	private String hdnPNR;

	private boolean groupPNR;

	private Collection<PaxTO> colAdultCollection = new ArrayList<PaxTO>();

	private Collection<PaxTO> colChildCollection = new ArrayList<PaxTO>();

	private Collection<PaxTO> colInfantCollection = new ArrayList<PaxTO>();

	private List countryInfo;

	private List nationalityInfo;

	private List docTypeInfo;

	private String titleVisibilityInfo;

	private String languageInfo;

	private boolean success = true;

	private String messageTxt;

	private ReservationProcessDTO resProcessParams;

	private Map<String, String> errorInfo;

	private Map<String, String> homePageErrorInfo;

	private String countryPhoneInfo;

	private String areaPhoneInfo;

	private FareQuoteTO fareQuote;

	private Collection<LCCClientReservationSegment> resSegments;

	private String totalCustomerCredit;

	private Collection<FareDTO> fareDetails;

	private boolean allowInsModify = false;

	/* Hold Flexi information */
	private Collection<FlexiDTO> flexiInfo;

	private String firstDepature = null;

	private String lastArrival = null;

	/* Hold Flexi alerts for cancel & modify res */
	private Collection<LCCClientAlertInfoTO> reservationFlexiAlerts;

	private boolean pageData;

	private String creditBalance = "";

	private boolean validateEmailDomain;

	/** The reservation's version */
	private String version;

	private Collection<DisplayUtilDTO> balanceSummary;

	private String airlineCode;

	private String marketingAirlineCode;

	private String carLink;

	private boolean isCarLinkDynamic;

	/**
	 * Variable to denote to the view whether a successful refund occurred.
	 */
	private boolean successfulRefund;

	private boolean requoteEnabled;

	private List<LCCClientAlertInfoTO> alertInfo;

	private Map<String, Integer> availableCOSRankMap;

	private List<LCCClientAlertTO> promotionInfo;

	private boolean creditPromo;

	private JSONObject reservationCookieDetails = new JSONObject();

	private boolean modifyFFID = false;
	
	private String marker;

	private Map<String,String> alertIdPnrSegIdMap = null;
	
	private boolean enableSelfReprotection;

	@SuppressWarnings("unchecked")
	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		String strLanguage = SessionUtil.getLanguage(request);
		List<AncillaryPaxTo> paxWiseAnci;

		try {
			if (pageData == false) {
				countryInfo = SelectListGenerator.getCountryList();
				docTypeInfo = SelectListGenerator.getDocTypeList();
				nationalityInfo = SelectListGenerator.getNationalityList();
				titleVisibilityInfo = JavaScriptGenerator.getTiteVisibilityHtml();
				String[] phoneInfoArray = SelectListGenerator.createTelephoneString();
				countryPhoneInfo = phoneInfoArray[0];
				areaPhoneInfo = phoneInfoArray[1];
				validateEmailDomain = AppSysParamsUtil.isEmailDomainValidationEnabled();
			}

			// retrieve and remove from session to make sure any future non relevent requests won't show this message.
			successfulRefund = Boolean
					.valueOf(String.valueOf(request.getSession().getAttribute(PaymentConstants.PARAM_SUCCESSFULL_REFUND)));
			request.getSession().removeAttribute(PaymentConstants.PARAM_SUCCESSFULL_REFUND);

			SYSTEM system = null;
			if (groupPNR) {
				system = SYSTEM.INT;
			} else {
				system = SYSTEM.AA;
			}
			
			// Yandex pixel markerID
			marker = (String) request.getAttribute(ReservationWebConstnts.EXTERNAL_MARKER_ID);
			if (validMarkerID(marker)) {
				request.getSession().setAttribute(ReservationWebConstnts.EXTERNAL_MARKER_ID, marker);
			} else {
				log.error("Invalid marker param : " + marker);
			}
		
			// Set Session Attribute for track user itinerary confirmation page refresh
			request.getSession().setAttribute("fromItinearyPage", false);
			Customer cust = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			LCCClientReservation reservation = ReservationUtil.loadProxyReservation(hdnPNR, (system == SYSTEM.INT),
					getTrackInfo(), true, (cust != null) ? true : false, airlineCode, marketingAirlineCode,
					AppSysParamsUtil.isPromoCodeEnabled());
			enableFFIDAddEdit(reservation);
			this.adultCount = reservation.getTotalPaxAdultCount();
			this.childCount = reservation.getTotalPaxChildCount();
			this.infantCount = reservation.getTotalPaxInfantCount();

			this.segments = reservation.getSegments();

			// Invalid or null languages are handled by SessionUtil.setLanguage()
			strLanguage = reservation.getContactInfo().getPreferredLanguage();
			request.setAttribute(RequestParameter.HIDDEN_LANGUAGE, strLanguage);
			SessionUtil.setLanguage(request);
			SessionUtil.setLocale(request);

			// Get the language from session again. If previous language set was invalid default will be set.
			strLanguage = SessionUtil.getLanguage(request);

			// Need to load these every time reservation is loaded now since language can vary by reservation
			languageInfo = ShowAddPaxHG.createItineryLanguagesOptionSelected(strLanguage);
			String[] pagesIDs = { "Form", "Common", "PgInterlineConfirm", "PgContactInfo", "PgReservation", "PgSearch",
					"PgUserTab", "PgUserCommon", "PgUser" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			errorInfo = ErrorMessageUtil.getReservationContactDetailsErrors(request);
			homePageErrorInfo = ErrorMessageUtil.getFlightSearchErrors(request);

			checkLoadingConstraint(reservation);
			bookingTO = ReservationUtil.createBookingDetails(reservation);

			balanceSummary = BalanceSummaryUtil.getReservationBalanceSummay(reservation, AppSysParamsUtil.getBaseCurrency(),
					SessionUtil.getLanguage(request));

			Object[] flightInfo = ReservationUtil.loadFlightSegments(reservation, false, reservation.getLccPromotionInfoTO());

			if (flightInfo != null) {
				if (flightInfo[0] != null) {
					flightDepDetails = (Collection<FlightInfoTO>) flightInfo[0];
				}

				if (flightInfo[1] != null) {
					flightRetDetails = (Collection<FlightInfoTO>) flightInfo[1];
				}

			}

			Object[] paxCol = ItineraryUtil.createPassengerList(reservation);
			colAdultCollection = (Collection<PaxTO>) paxCol[0];
			colChildCollection = (Collection<PaxTO>) paxCol[1];
			colInfantCollection = (Collection<PaxTO>) paxCol[2];

			contactInfo = ReservationUtil.getContactDetails(reservation.getContactInfo());
			contactInfo.setNationality(StringUtil.nullConvertToString(reservation.getContactInfo().getNationalityCode()));
			SessionUtil.setReservationContactInfo(request, contactInfo);

			resProcessParams = ReservationUtil.getReservationProcessParams(request, reservation, system);
			allowInsModify = (AppSysParamsUtil.isRakEnabled() && resProcessParams.isInsuranceEnable());

			fareQuote = new FareQuoteTO();
			resSegments = ReservationUtil.getConfirmSegments(reservation.getSegments());

			Collection<FlightInfoTO> depRetFlights = new ArrayList<FlightInfoTO>();
			depRetFlights.addAll(flightDepDetails);
			depRetFlights.addAll(flightRetDetails);

			flightDepDetails = SortUtil.sortByDepatureDate(new ArrayList<FlightInfoTO>(flightDepDetails));
			flightRetDetails = SortUtil.sortByDepatureDate(new ArrayList<FlightInfoTO>(flightRetDetails));

			flexiInfo = ReservationUtil.getFlexiInfo(depRetFlights, reservation.getFlexiAlertInfoTOs(),
					SessionUtil.getLanguage(request));
			reservationFlexiAlerts = reservation.getFlexiAlertInfoTOs();

			carLink = ReservationHG.createRentACarLinkForManageBooking(strLanguage, depRetFlights, reservation.getPNR());
			isCarLinkDynamic = AppSysParamsUtil.isRentACarLinkDynamic();

			Customer customerModel = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			if (customerModel != null) {
				int intCustomerID = customerModel.getCustomerId();
				/*
				 * totalCustomerCredit = CustomerUtilV2.getCustomerTotalCredit(ModuleServiceLocator
				 * .getReservationQueryBD().getReservationCredits(intCustomerID));
				 */

				Collection<CustomerCreditDTO> customerCreditDTO = CustomerUtilV2.getCustomerCreditFromAllCarriers(intCustomerID,
						getTrackInfo());
				totalCustomerCredit = CustomerUtilV2.calculateCustomerTotalCredit(customerCreditDTO);
			}

			// Clear all previous reservation data for freshly loaded reservations.
			IBEReservationInfoDTO reservationInfo = new IBEReservationInfoDTO();
			SessionUtil.setIBEreservationInfo(request, reservationInfo);

			reservationInfo.setReservationStatus(reservation.getStatus());
			reservationInfo.setSegmentStatus(ReservationUtil.getReservationSegmentStatus(reservation.getSegments()));
			reservationInfo.addPnrPaxCreditMap(hdnPNR, (ReservationUtil.getPaxCreditMap(reservation.getPassengers())));
			reservationInfo.setTotalTicketPriceInBase(reservation.getTotalTicketPrice());
			reservationInfo.setBundledFareDTOs(reservation.getBundledFareDTOs());
			reservationInfo.addNewServiceTaxes(reservation.getApplicableServiceTaxes());
			reservationInfo.setSelectedSystem(reservation.isGroupPNR() ? SYSTEM.INT : SYSTEM.AA);
			reservationInfo.setPnr(reservation.getPNR());
			
			reservationInfo.setMarketingCarrier(reservation.getMarketingCarrier());
			reservationInfo.setInfantPaymentSeparated(reservation.isInfantPaymentSeparated());
			reservationInfo.setPnr(reservation.getPNR());

			if (reservation.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) > 0) {
				fareQuote.setReservationBalance(AccelAeroCalculator.formatAsDecimal(reservation.getTotalAvailableBalance()));
				fareQuote.setTotalPrice(AccelAeroCalculator.formatAsDecimal(reservation.getTotalAvailableBalance()));
				fareQuote.setHasReservationBalance(true);
			}

			if (SessionUtil.getIbePromotionPaymentPostDTO(request) != null) {
				IBEPromotionPaymentDTO ibePromotionPaymentDTO = SessionUtil.getIbePromotionPaymentPostDTO(request);
				if (ibePromotionPaymentDTO.getTotalPayment() != null) {

					String nextSeatChargeStr = SessionUtil.getIbePromotionPaymentPostDTO(request).getTotalPayment();
					BigDecimal nextSeatCharge = new BigDecimal(nextSeatChargeStr);

					BigDecimal totalAvailableBalance = AccelAeroCalculator.add(nextSeatCharge,
							reservation.getTotalAvailableBalance());

					fareQuote.setReservationBalance(AccelAeroCalculator.formatAsDecimal(totalAvailableBalance));
					fareQuote.setTotalPrice(AccelAeroCalculator.formatAsDecimal(totalAvailableBalance));
					fareQuote.setHasReservationBalance(true);

					fareQuote.setHasNextSeatPromoCharge(true);
					fareQuote.setNextSeatPromoCharge(AccelAeroCalculator.formatAsDecimal(nextSeatCharge));

					// CONFIRM THIS
					BigDecimal totalTicketPriceWithNextSeatCharge = AccelAeroCalculator.add(reservation.getTotalTicketPrice(),
							nextSeatCharge);

					reservationInfo.setTotalTicketPriceInBase(totalTicketPriceWithNextSeatCharge);

				}
			}

			SessionUtil.setIBEreservationInfo(request, reservationInfo);
			SessionUtil.setIBEReservationPostPaymentDTO(request, null);

			// Set Fare Rules
			fareDetails = ReservationUtil.getFareRuleDetails(reservation.getSegments());

			Object[] arrDepArrZulu = ReservationUtil.getDepArrZulutimes(new HashSet(resSegments));

			// Set First Departure And Last Arrival
			if (arrDepArrZulu != null) {
				if (arrDepArrZulu[0] != null) {
					firstDepature = DateFormatUtils.format((Date) arrDepArrZulu[0], "yyyy-MM-dd HH:mm:ss.S");
				}
				if (arrDepArrZulu[1] != null) {
					lastArrival = DateFormatUtils.format((Date) arrDepArrZulu[1], "yyyy-MM-dd HH:mm:ss.S");
				}
			}

			// Cancel Segment Balance
			if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
				creditBalance = AccelAeroCalculator.formatAsDecimal(
						BalanceSummaryUtil.getTotalPaxCredit(ReservationUtil.getPaxCreditMap(reservation.getPassengers())));
			}
			bookingTO.setInsuranceType(ReservationUtil.getInsuranceSelectionText(reservation, strLanguage));

			version = reservation.getVersion();
			requoteEnabled = AppSysParamsUtil.isRequoteEnabled();
			alertInfo = ReservationUtil.getSortedAlertInfo(reservation.getAlertInfoTOs());
			alertIdPnrSegIdMap = createAlertIdPnrSegIdMap(alertInfo);
			availableCOSRankMap = SelectListGenerator.createCabinClassRankingMap();
			enableSelfReprotection = AppSysParamsUtil.isSelfReprotectionAllowed();
			promotionInfo = ReservationUtil.generatePromotionInfoAlerts(reservation, strLanguage);

			paxWiseAnci = ReservationUtil.createPaxWiseAnci(reservation.getPassengers());
			reservationInfo.setAncillaryPaxTos(paxWiseAnci);

			reservationInfo.setBaggageSummaryTo(AncillaryCommonUtil.getResBaggageSummary(reservation));

			if (reservation.getLccPromotionInfoTO() != null) {
				LCCPromotionInfoTO lccPromotionInfoTO = reservation.getLccPromotionInfoTO();
				if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
					creditPromo = true;
				}
			}

			if (AppSysParamsUtil.isCookieSaveFunctionalityEnabled()) {
				this.reservationCookieDetails = ReservationUtil.getReservationCookieDetails(reservation);
			}

			String selectedCurrency = reservation.getLastCurrencyCode();
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			if (selectedCurrency != null && !fareQuote.getCurrency().equals(selectedCurrency)) {
				CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency,
						ApplicationEngine.IBE);
				if (currencyExchangeRate != null) {
					fareQuote.setSelectedCurrency(reservation.getLastCurrencyCode());
					fareQuote.setSelectedEXRate(currencyExchangeRate.getMultiplyingExchangeRate().toPlainString());
				}
			} else if (selectedCurrency != null) {
				fareQuote.setSelectedCurrency(reservation.getLastCurrencyCode());
			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowLoadReservationAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowLoadReservationAction==>", ex);
		}
		return forward;
	}

	private void checkLoadingConstraint(LCCClientReservation reservation) throws ModuleException {
		boolean isError = false;

		if ((reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount()) > AppSysParamsUtil
				.getMaxChildAdultCount()) {
			isError = true;
		}

		for (LCCClientReservationSegment segment : reservation.getSegments()) {
			if (segment.isOpenReturnSegment()) {
				isError = true;
				break;
			}
		}
		CommonReservationAdminInfo adminInfo = reservation.getAdminInfo();
		String AIRLINE_SEPERATOR = ",";
		String CARRIER_SEPERATOR = "|";
		String LCC_CHANNEL_CODE = String.valueOf(ReservationInternalConstants.SalesChannel.LCC);
		if (AppSysParamsUtil.isIBEModificationRestrictionEnabled()) {
			if (adminInfo != null && adminInfo.getOriginChannelId() != null && !adminInfo.getOriginChannelId().equals("")) {

				/*
				 * Check if only one airline is participating in the reservation. If this is true it's either a dry
				 * reservation or an normal one.
				 */
				if (adminInfo.getOriginChannelId().lastIndexOf(AIRLINE_SEPERATOR) == -1) {

					/*
					 * Check if the carrier code is present in the channel id. If it's not then it's a normal
					 * reservation. Otherwise it's a dry reservation.
					 */
					if (adminInfo.getOriginChannelId().lastIndexOf(CARRIER_SEPERATOR) == -1) {
						if (!AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(adminInfo.getOriginChannelId())) {
							isError = true;
						}
					} else {
						String channelCode[] = StringUtils.split(adminInfo.getOriginChannelId(), CARRIER_SEPERATOR);
						if (!AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(channelCode[1])) {
							isError = true;
						} else if (LCC_CHANNEL_CODE.equals(channelCode[1])
								&& !AppSysParamsUtil.isIBEModificationAllowedForDry()) {
							isError = true;
						}
					}
				} else {
					boolean isOneCarrierOwn = false;
					boolean isViaLCC = false;
					for (String code : adminInfo.getOriginChannelId().split(AIRLINE_SEPERATOR)) {
						String channelCode[] = StringUtils.split(code, CARRIER_SEPERATOR);

						if (LCC_CHANNEL_CODE.equals(channelCode[1])) {
							isViaLCC = true;
						}

						if (channelCode[0].equals(AppSysParamsUtil.getDefaultCarrierCode())) {
							isOneCarrierOwn = true;

							if (!AppSysParamsUtil.allowedChannelCodesForIBEModification().contains(channelCode[1])) {
								isError = true;
								break;
							}
						}
					}

					/*
					 * If the reservation is done through LCC then check if it has at least one own carrier segment, if
					 * it has that then the reservation is an interline reservation.
					 * 
					 * Else it's a Dry reservation.
					 */
					if (isViaLCC && isOneCarrierOwn && !AppSysParamsUtil.isIBEModificationAllowedForInterline()) {
						isError = true;
					} else if (isViaLCC && !AppSysParamsUtil.isIBEModificationAllowedForDry()) {
						isError = true;
					}
				}
			}
		}
		if (isError) {
			throw new ModuleException("ibe.booking.not.allowed.to.modify.through.web");
		}

	}

	/** create the alertId-pnrSegId map for each pnr segment */
	private Map<String,String> createAlertIdPnrSegIdMap(List<LCCClientAlertInfoTO> sortedAlertInfo){
		
		Map<String,String> map = new LinkedHashMap<String, String>();
		for(LCCClientAlertInfoTO lccClientAlertInfoTO : sortedAlertInfo){
			List<LCCClientAlertTO> lccClientAlertTOList = lccClientAlertInfoTO.getAlertTO();
			if(lccClientAlertTOList.size() != 0){
				
 				for(int i = lccClientAlertTOList.size() -1 ; i >= 0 ; i--){
					if(lccClientAlertTOList.get(i).isHasFlightSegments()){
						map.put(lccClientAlertInfoTO.getFlightSegmantRefNumber(), String.valueOf(lccClientAlertTOList.get(i).getAlertId()));
						break;
					}
				}
			}
			
		}
		return map;
	}

	private void enableFFIDAddEdit(LCCClientReservation reservation) {
		List<LCCClientReservationSegment> segmentList = new ArrayList<>(reservation.getSegments());
		boolean hasUnSegment = ItineraryUtil.hasUncancelledUnSegment(reservation.getSegments());
		Collections.sort(segmentList);
		for (LCCClientReservationSegment segment : segmentList) {
			if (!ReservationSegmentSubStatus.EXCHANGED.equals(segment.getSubStatus())) {
				modifyFFID = segment.getModifyTillBufferDateTime().after(new Date()) && !segment.isFlownSegment()
						&& !hasUnSegment;
				if (!modifyFFID) {
					break;
				}
			}
		}
	}
	
	private boolean validMarkerID(String markerID){
		boolean isValidMarkerID = false;
		if (marker != null && !marker.equals("") && marker.length() <= 200){
			isValidMarkerID = true;
		}
		return isValidMarkerID;
	}

	public Collection<FlightInfoTO> getFlightDepDetails() {
		return flightDepDetails;
	}

	public static Log getLog() {
		return log;
	}

	public Collection<FlightInfoTO> getFlightRetDetails() {
		return flightRetDetails;
	}

	public ItineraryPaymentSummaryTO getItineraryPaymentSummaryTO() {
		return itineraryPaymentSummaryTO;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public List getDocTypeInfo() {
		return docTypeInfo;
	}

	public void setDocTypeInfo(List docTypeInfo) {
		this.docTypeInfo = docTypeInfo;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public BookingTO getBookingTO() {
		return bookingTO;
	}

	public String getHdnPNR() {
		return hdnPNR;
	}

	public void setHdnPNR(String hdnPNR) {
		this.hdnPNR = hdnPNR;
	}

	public Collection<PaxTO> getColAdultCollection() {
		return colAdultCollection;
	}

	public Collection<PaxTO> getColChildCollection() {
		return colChildCollection;
	}

	public Collection<PaxTO> getColInfantCollection() {
		return colInfantCollection;
	}

	public List getCountryInfo() {
		return countryInfo;
	}

	public void setCountryInfo(List countryInfo) {
		this.countryInfo = countryInfo;
	}

	public List getNationalityInfo() {
		return nationalityInfo;
	}

	public void setNationalityInfo(List nationalityInfo) {
		this.nationalityInfo = nationalityInfo;
	}

	public String getTitleVisibilityInfo() {
		return titleVisibilityInfo;
	}

	public String getLanguageInfo() {
		return languageInfo;
	}

	public ReservationProcessDTO getResProcessParams() {
		return resProcessParams;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public String getCountryPhoneInfo() {
		return countryPhoneInfo;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public Set<LCCClientReservationSegment> getSegments() {
		return segments;
	}

	public void setFareQuote(FareQuoteTO fareQuote) {
		this.fareQuote = fareQuote;
	}

	public Collection<LCCClientReservationSegment> getResSegments() {
		return resSegments;
	}

	public String getTotalCustomerCredit() {
		return totalCustomerCredit;
	}

	public Collection<FareDTO> getFareDetails() {
		return fareDetails;
	}

	public boolean isAllowInsModify() {
		return allowInsModify;
	}

	public void setAllowInsModify(boolean allowInsModify) {
		this.allowInsModify = allowInsModify;
	}

	public Collection<FlexiDTO> getFlexiInfo() {
		return flexiInfo;
	}

	public void setFlexiInfo(Collection<FlexiDTO> flexiInfo) {
		this.flexiInfo = flexiInfo;
	}

	public Collection<LCCClientAlertInfoTO> getReservationFlexiAlerts() {
		return reservationFlexiAlerts;
	}

	public void setReservationFlexiAlerts(Collection<LCCClientAlertInfoTO> reservationFlexiAlerts) {
		this.reservationFlexiAlerts = reservationFlexiAlerts;
	}

	public void setFareDetails(Collection<FareDTO> fareDetails) {
		this.fareDetails = fareDetails;
	}

	public String getFirstDepature() {
		return firstDepature;
	}

	public String getLastArrival() {
		return lastArrival;
	}

	public void setPageData(boolean pageData) {
		this.pageData = pageData;
	}

	public String getCreditBalance() {
		return creditBalance;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isValidateEmailDomain() {
		return validateEmailDomain;
	}

	public void setValidateEmailDomain(boolean validateEmailDomain) {
		this.validateEmailDomain = validateEmailDomain;
	}

	public Collection<DisplayUtilDTO> getBalanceSummary() {
		return balanceSummary;
	}

	public String getAreaPhoneInfo() {
		return areaPhoneInfo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}

	public String getCarLink() {
		return carLink;
	}

	public void setCarLink(String carLink) {
		this.carLink = carLink;
	}

	public boolean isCarLinkDynamic() {
		return isCarLinkDynamic;
	}

	public void setCarLinkDynamic(boolean isCarLinkDynamic) {
		this.isCarLinkDynamic = isCarLinkDynamic;
	}

	/**
	 * @param nonSamanRefund
	 *            the nonSamanRefund to set
	 */
	public void setSuccessfulRefund(boolean successfulRefund) {
		this.successfulRefund = successfulRefund;
	}

	/**
	 * @return the nonSamanRefund
	 */
	public boolean isSuccessfulRefund() {
		return successfulRefund;
	}

	public boolean isRequoteEnabled() {
		return requoteEnabled;
	}

	public void setRequoteEnabled(boolean requoteEnabled) {
		this.requoteEnabled = requoteEnabled;
	}

	/**
	 * @return the alertInfo
	 */
	public List<LCCClientAlertInfoTO> getAlertInfo() {
		return alertInfo;
	}

	/**
	 * @param alertInfo
	 *            the alertInfo to set
	 */
	public void setAlertInfo(List<LCCClientAlertInfoTO> alertInfo) {
		this.alertInfo = alertInfo;
	}

	public Map<String, Integer> getAvailableCOSRankMap() {
		return availableCOSRankMap;
	}

	public void setAvailableCOSRankMap(Map<String, Integer> availableCOSRankMap) {
		this.availableCOSRankMap = availableCOSRankMap;
	}

	public List<LCCClientAlertTO> getPromotionInfo() {
		return promotionInfo;
	}

	public Map<String, String> getHomePageErrorInfo() {
		return homePageErrorInfo;
	}

	public void setHomePageErrorInfo(Map<String, String> homePageErrorInfo) {
		this.homePageErrorInfo = homePageErrorInfo;
	}

	public boolean isCreditPromo() {
		return creditPromo;
	}

	public JSONObject getReservationCookieDetails() {
		return reservationCookieDetails;
	}

	public void setReservationCookieDetails(JSONObject reservationCookieDetails) {
		this.reservationCookieDetails = reservationCookieDetails;
	}

	public boolean isModifyFFID() {
		return modifyFFID;
	}

	public void setModifyFFID(boolean modifyFFID) {
		this.modifyFFID = modifyFFID;
	}

	public Map<String, String> getAlertIdPnrSegIdMap() {
		return alertIdPnrSegIdMap;
	}

	public boolean isEnableSelfReprotection() {
		return enableSelfReprotection;
	}

	public String getMarker() {
		return marker;
	}

	public void setMarker(String marker) {
		this.marker = marker;
	}
}