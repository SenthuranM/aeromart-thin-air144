package com.isa.thinair.ibe.core.web.v2.action.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciReleaseAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(InterlineAnciReleaseAction.class);

	private boolean success = true;

	private String messageTxt;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			success = SeatMapUtil.releaseBlockedSeats(request);

		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("ANCI RELEASE ERROR", e);
		}
		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

}
