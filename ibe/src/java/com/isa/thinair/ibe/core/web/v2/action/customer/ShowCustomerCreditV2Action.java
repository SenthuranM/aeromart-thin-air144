package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.CustomerCreditDTO;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerCreditV2Action extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ShowCustomerCreditV2Action.class);

	private String hdnCat;

	private boolean success = true;

	private String messageTxt;

	private Collection<CustomerCreditDTO> customerCreditDTO;

	private HashMap<String, String> jsonLabel;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, "PgUserCredit");
			Customer customer = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			int intCustomerID = customer.getCustomerId();

			customerCreditDTO = CustomerUtilV2.getCustomerCreditFromAllCarriers(intCustomerID, getTrackInfo());

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowReservationListAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowReservationListAction==>", ex);
		}
		return forward;
	}

	public String getHdnCat() {
		return hdnCat;
	}

	public void setHdnCat(String hdnCat) {
		this.hdnCat = hdnCat;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<CustomerCreditDTO> getCustomerCreditDTO() {
		return customerCreditDTO;
	}

	public void setCustomerCreditDTO(Collection<CustomerCreditDTO> customerCreditDTO) {
		this.customerCreditDTO = customerCreditDTO;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public void setJsonLabel(HashMap<String, String> jsonLabel) {
		this.jsonLabel = jsonLabel;
	}

	private List<String> extractPnrList(Collection<ReservationListTO> resList) {
		List<String> pnrList = new ArrayList<String>();

		for (ReservationListTO resTo : resList) {
			pnrList.add(resTo.getPnrNo());
		}

		return pnrList;
	}
}
