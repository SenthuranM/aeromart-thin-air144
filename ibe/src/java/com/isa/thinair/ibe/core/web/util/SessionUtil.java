package com.isa.thinair.ibe.core.web.util;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.core.Config;

import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEPromotionPaymentDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.PutOnHoldBeforePaymentDTO;
import com.isa.thinair.ibe.api.dto.SessionDataDTO;
import com.isa.thinair.ibe.api.dto.VoucherCCPaymentDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.SessionAttribute;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.login.util.ForceLoginInvoker;

public class SessionUtil {

	private static final String DEFAULT_LANGUAGE = "en";

	/**
	 * adds a new SessionDataDTO object to the session
	 * 
	 * @param request
	 */
	public static void initialize(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = new SessionDataDTO();
		request.getSession().setAttribute(SessionAttribute.SESSION_DATA_DTO, sessionDataDTO);

	}

	/**
	 * adds a new SessionDataDTO object to the session, is not initialized
	 * 
	 * @param request
	 */
	public static void initializeIfNot(HttpServletRequest request) {

		if (getSessionDataDTO(request) == null) {
			SessionDataDTO sessionDataDTO = new SessionDataDTO();
			request.getSession().setAttribute(SessionAttribute.SESSION_DATA_DTO, sessionDataDTO);
		}
	}

	/**
	 * checks whether the session is new
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isSessionExpired(HttpServletRequest request) {
		return (request.getSession(false) == null || request.getSession().getAttribute(SessionAttribute.SESSION_DATA_DTO) == null);
	}

	/**
	 * returns SessionDataDTO session object
	 * 
	 * @param request
	 * @return
	 */
	private static SessionDataDTO getSessionDataDTO(HttpServletRequest request) {
		return (SessionDataDTO) request.getSession().getAttribute(SessionAttribute.SESSION_DATA_DTO);
	}

	/**
	 * caches the customer id in the user session
	 * 
	 * @param request
	 * @param customerId
	 */
	public static void setCustomerId(HttpServletRequest request, int customerId) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setCustomerId(customerId);
	}

	/**
	 * gets the customer id from the user session
	 * 
	 * @param request
	 * @return
	 */
	public static int getCustomerId(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);

		return sessionDataDTO.getCustomerId();
	}

	/**
	 * caches the customer email in the user session
	 * 
	 * @param request
	 * @param customerEmail
	 */
	public static void setCustomerEMail(HttpServletRequest request, String customerEMail) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setCustomerEmail(customerEMail);
	}

	/**
	 * gets the customer email from the user session
	 * 
	 * @param request
	 * @return
	 */
	public static String getCustomerEmail(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);

		return sessionDataDTO.getCustomerEmail();
	}

	/**
	 * caches the customer password in the user session
	 * 
	 * @param request
	 * @param customerPassword
	 */
	public static void setCustomerPassword(HttpServletRequest request, String customerPassword) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setCustomerPassword(customerPassword);
	}

	/**
	 * gets the customer password from the user session
	 * 
	 * @param request
	 * @return
	 */
	public static String getCustomerPassword(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);

		return sessionDataDTO.getCustomerPassword();
	}

	/**
	 * checks whether the customer is logged in.
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isCustomerLoggedIn(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);

		return (sessionDataDTO != null && sessionDataDTO.getCustomerId() != -1);
	}

	/**
	 * caches the selected language
	 * 
	 * @param request
	 */
	public static void setLanguage(HttpServletRequest request) {
		String lan = AppSysParamsUtil.getSystemDefaultLanguage().toLowerCase();
		String hdnLanguage = RequestUtil.getHiddenLanguage(request);
		if (isIBESupportLanguage(hdnLanguage)) {
			lan = hdnLanguage.toLowerCase();
		} else {
			String hdnParamData = RequestUtil.getHiddenParamData(request);
			if (isNotEmpty(hdnParamData)) {
				String paramArr[] = hdnParamData.split(WebConstants.PARAM_SPLIT);
				if (paramArr.length > 0 && isNotEmpty(paramArr[0])) {
					if (isIBESupportLanguage(paramArr[0])) {
						lan = paramArr[0].toLowerCase();
					}
				}
			}
		}
		getSessionDataDTO(request).setLanguage(lan);
	}

	private static boolean isNotEmpty(String code) {
		boolean isEmpty = false;
		if (code != null && !code.trim().isEmpty()) {
			isEmpty = true;
		}
		return isEmpty;
	}

	private static boolean isIBESupportLanguage(String language) {
		boolean isSupport = false;
		if (isNotEmpty(language)) {
			if (AppSysParamsUtil.getIBESupportedLanguages().containsKey(language.toLowerCase())) {
				isSupport = true;
			}
		}
		return isSupport;
	}

	/**
	 * returns the
	 * 
	 * @param request
	 * @return
	 */
	public static String getLanguage(HttpServletRequest request) {
		SessionDataDTO sessionData = getSessionDataDTO(request);
		String lan = null;

		if (sessionData != null) {
			lan = sessionData.getLanguage();
		} else {
			lan = DEFAULT_LANGUAGE;
		}

		return lan;
	}

	/**
	 * sets locale for the customer
	 * 
	 * @param request
	 */
	public static void setLocale(HttpServletRequest request) {
		String userLang = getLanguage(request);
		Locale locale = new Locale(userLang);
		Config.set(request.getSession(), Config.FMT_LOCALE, locale);
	}

	/**
	 * caches the no of attempts
	 * 
	 * @param request
	 * @param attempts
	 */
	public static void setNumberOfAttempts(HttpServletRequest request, Integer attempts) {
		SessionDataDTO sessionData = getSessionDataDTO(request);

		sessionData.setNumberOfAttempts(attempts);
	}

	/**
	 * gets no of attempts from cache
	 * 
	 * @param request
	 * @param attempts
	 * @return
	 */
	public static Integer getNumberOfAttempts(HttpServletRequest request) {
		SessionDataDTO sessionData = getSessionDataDTO(request);

		return sessionData.getNumberOfAttempts();
	}

	/**
	 * caches the carrier
	 * 
	 * @param request
	 * @param carrier
	 */
	public static void setCarrier(HttpServletRequest request, String carrier) {
		getSessionDataDTO(request).setCarrier(carrier);
	}

	/**
	 * gets the carrier from the cache
	 * 
	 * @param request
	 * @return
	 */
	public static String getCarrier(HttpServletRequest request) {
		SessionDataDTO sessionData = getSessionDataDTO(request);
		String carrier = sessionData.getCarrier();

		if (carrier == null) {
			// session expired
			carrier = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}

		return carrier;
	}

	/**
	 * caches system refresh
	 * 
	 * @param request
	 * @param systemRefresh
	 */
	public static void setSystemRefresh(HttpServletRequest request, Integer systemRefresh) {
		getSessionDataDTO(request).setSystemRefresh(systemRefresh);
	}

	public static void setLoyaltyAccountNo(HttpServletRequest request, String accountNo) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setLoyaltyAccountNo(accountNo);
	}

	public static String getLoyaltyAccountNo(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);

		return sessionDataDTO.getLoyaltyAccountNo();
	}

	public static IBEReservationInfoDTO getIBEreservationInfo(HttpServletRequest request) {
		return getSessionDataDTO(request).getIbeReservationInfo();
	}

	public static void resetSesionDataInError(HttpServletRequest request) {
		setIBEreservationInfo(request, null);
		setIBEReservationPostPaymentDTO(request, null);
		setTempPaymentPNR(request, null);
		setResponseReceivedFromPaymentGateway(request, false);
	}

	public static IBEReservationPostPaymentDTO getIBEReservationPostPaymentDTO(HttpServletRequest request) {
		return getSessionDataDTO(request).getIbeReservationPostPaymentDTO();
	}

	public static void setIBEreservationInfo(HttpServletRequest request, IBEReservationInfoDTO ibeReservationInfo) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setIbeReservationInfo(ibeReservationInfo);
	}

	public static void setIBEReservationPostPaymentDTO(HttpServletRequest request,
			IBEReservationPostPaymentDTO ibeReservationPostPaymentDTO) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setIbeReservationPostPaymentDTO(ibeReservationPostPaymentDTO);
	}

	public static void expireSession(HttpServletRequest request) {
		request.getSession().invalidate();
		ForceLoginInvoker.close();
	}

	public static void setTempPaymentPNR(HttpServletRequest request, String tempPaymentPNR) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setTempPaymentPNR(tempPaymentPNR);
	}

	public static String getTempPaymentPNR(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		return sessionDataDTO.getTempPaymentPNR();
	}

	public static boolean isResponseReceivedFromPaymentGateway(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		return sessionDataDTO.isResponseReceivedFromPaymentGateway();
	}

	public static void setResponseReceivedFromPaymentGateway(HttpServletRequest request,
			boolean responseReceivedFromPaymentGateway) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setResponseReceivedFromPaymentGateway(responseReceivedFromPaymentGateway);
	}

	public static String getManageBookingPNR(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		return sessionDataDTO.getManageBookingPNR();
	}

	public static void setManageBookingPNR(HttpServletRequest request, String manageBookingPNR) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setManageBookingPNR(manageBookingPNR);
	}

	public static String getRequestSessionIdentifier(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		return sessionDataDTO.getRequestSessionIdentifier();
	}

	public static void setRequestSessionIdentifier(HttpServletRequest request, String requestSessionIdentifier) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setRequestSessionIdentifier(requestSessionIdentifier);
	}

	public static void resetSesionCardDataInError(HttpServletRequest request) {
		setIBEReservationPostPaymentDTO(request, null);
		setTempPaymentPNR(request, null);
		setResponseReceivedFromPaymentGateway(request, false);
	}

	public static IBEReservationPostDTO getIbeReservationPostDTO(HttpServletRequest request) {
		return getSessionDataDTO(request).getIbeReservationPostDTO();
	}

	public static void setIbeReservationPostDTO(HttpServletRequest request, IBEReservationPostDTO ibeReservationPostDTO) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setIbeReservationPostDTO(ibeReservationPostDTO);
	}

	public static void setDesignVersion(HttpServletRequest request, String hdnDesign) {
		request.getSession().setAttribute("hdnDesign", AppSysParamsUtil.isIbeDesignVersionAvail() ? hdnDesign : "0");
	}

	public static boolean isNewDesignVersion(HttpServletRequest request) {
		boolean _version = false;
		if (request.getSession().getAttribute("hdnDesign") == null) {
			_version = false;
		} else if (request.getSession().getAttribute("hdnDesign").toString().equals("0")) {
			_version = false;
		} else {
			_version = true;
		}
		return _version;
	}

	public static PutOnHoldBeforePaymentDTO getPutOnHoldBeforePaymentDTO(HttpServletRequest request) {
		return getSessionDataDTO(request).getPutOnHoldBeforePaymentDTO();
	}

	public static void setPutOnHoldBeforePaymentDTO(HttpServletRequest request,
			PutOnHoldBeforePaymentDTO putOnHoldBefreoPaymentDTO) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setPutOnHoldBeforePaymentDTO(putOnHoldBefreoPaymentDTO);
	}

	public static boolean isMakePayment(HttpServletRequest request) {
		boolean makePayment = false;
		if (getPutOnHoldBeforePaymentDTO(request) != null && getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()
				&& getPutOnHoldBeforePaymentDTO(request).getPutOnHoldBeforePaymentPnr() != null) {
			makePayment = true;
		}
		return makePayment;
	}

	public static void setIbePromotionPaymentPostDTO(HttpServletRequest request, IBEPromotionPaymentDTO ibePromotionPaymentDTO) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setIbePromotionPaymentDTO(ibePromotionPaymentDTO);
	}

	public static IBEPromotionPaymentDTO getIbePromotionPaymentPostDTO(HttpServletRequest request) {
		return getSessionDataDTO(request).getIbePromotionPaymentDTO();
	}

	public static void setReservationContactInfo(HttpServletRequest request, ContactInfoDTO contactInfo) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setContactInfo(contactInfo);
	}

	public static ContactInfoDTO getReservationContactInfo(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		return sessionDataDTO.getContactInfo();
	}

	public static boolean isSuccessfulReservationExit(HttpServletRequest request) {
		boolean isReservationExit = false;
		if (getIbeReservationPostDTO(request) != null && getIbeReservationPostDTO(request).getPnr() != null) {
			isReservationExit = true;
		}
		return isReservationExit;
	}

	public static boolean isExitpopupDisplayed(HttpServletRequest request) {
		return getSessionDataDTO(request).isExitPopupDisplayed();
	}

	public static void updateExitPopdisplayStatus(HttpServletRequest request, boolean shown) {
		getSessionDataDTO(request).setExitPopupDisplayed(shown);
	}

	public static void setLoyaltyFFID(HttpServletRequest request, String ffid) {
		getSessionDataDTO(request).setLoyaltyFFID(ffid);
	}

	public static String getLoyaltyFFID(HttpServletRequest request) {
		return getSessionDataDTO(request).getLoyaltyFFID();
	}
	
	public static void setVoucherPaymentDTO(HttpServletRequest request, VoucherCCPaymentDTO voucherCCPaymentDTO) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		sessionDataDTO.setVoucherCCPaymentDTO(voucherCCPaymentDTO);
	}
	
	public static VoucherCCPaymentDTO getVoucherPaymentDTO(HttpServletRequest request) {
		SessionDataDTO sessionDataDTO = getSessionDataDTO(request);
		return sessionDataDTO.getVoucherPaymentDTO();
	}

	public static void setVoucherDTO(HttpServletRequest request, VoucherDTO voucherDTO) {
		getSessionDataDTO(request).setVoucherDTO(voucherDTO);
	}

	public static VoucherDTO getVoucherDTO(HttpServletRequest request) {
		return getSessionDataDTO(request).getVoucherDTO();
	}
	
	public static void setVoucherDTOList(HttpServletRequest request, List<VoucherDTO> voucherDTOs) {
		getSessionDataDTO(request).setVoucherList(voucherDTOs);
	}

	public static List<VoucherDTO> getVoucherDTOlist(HttpServletRequest request) {
		return getSessionDataDTO(request).getVoucherList();
	}
}
