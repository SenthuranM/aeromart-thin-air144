package com.isa.thinair.ibe.core.web.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Use I18NUtil instead
 */
@Deprecated 
public class MessageUtil {

	private static final String IBE_USER_MESSEAGES_CODES_RESOURCE_BUNDLE_NAME = "resources/i18n/IBECommonMessages";

	private static final String IBE_DEFULT_USER_MESSAGE_CODE = "customer.module.message.code.empty";

	private static final String DEFALT_LANGUGAE_CODE = "en";

	private static Map<String, ResourceBundle> customerMessagesCodeMap = new HashMap<String, ResourceBundle>();

	static {
		loadResourceBundles();
	}

	private static void loadResourceBundles() {
		Map<String, String> suppotedLanguagesMap = AppSysParamsUtil.getIBESupportedLanguages();
		Set<String> lanKeySet = suppotedLanguagesMap.keySet();

		for (Iterator<String> iterator = lanKeySet.iterator(); iterator.hasNext();) {
			String lan = (String) iterator.next();
			Locale locale = new Locale(lan);

			customerMessagesCodeMap.put(lan, ResourceBundle.getBundle(IBE_USER_MESSEAGES_CODES_RESOURCE_BUNDLE_NAME, locale));
		}
	}

	/**
	 * returns a message description
	 * 
	 * @param messageCode
	 * @param languge
	 * @return
	 */
	public static String getMessage(String messageCode) {
		return getMessage(messageCode, DEFALT_LANGUGAE_CODE);
	}

	/**
	 * returns a message description
	 * 
	 * @param messageCode
	 * @param languge
	 * @return
	 */
	public static String getMessage(String messageCode, String languge) {
		ResourceBundle rb = customerMessagesCodeMap.get(languge);

		if (messageCode == null || messageCode.equals(""))
			return rb.getString(IBE_DEFULT_USER_MESSAGE_CODE);
		else {
			String message = null;
			try {
				message = rb.getString(messageCode);
			} catch (MissingResourceException mre) {
				message = MessagesUtil.getMessage(messageCode);
			}

			if (message != null) {
				return message;
			}
		}

		return rb.getString(IBE_DEFULT_USER_MESSAGE_CODE);
	}
}
