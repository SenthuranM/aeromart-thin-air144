package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.converters.OndConvertUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfoByDateDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.FlightSearchInfoDTO;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class SearchUtil {
	private static Log log = LogFactory.getLog(SearchUtil.class);

	/**
	 * 
	 * @param flighSearch
	 * @return
	 * @throws ModuleException
	 */
	public static AvailableFlightSearchDTOBuilder getSearchBuilder(FlightSearchDTO flighSearch) throws ModuleException {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat smf = new SimpleDateFormat("dd/MM/yyyy");
		String selCOS = null;
		AvailableFlightSearchDTOBuilder availableFlightSearchDTO = new AvailableFlightSearchDTOBuilder();

		availableFlightSearchDTO.setAdultCount(flighSearch.getAdultCount());
		availableFlightSearchDTO.setChildCount(flighSearch.getChildCount());
		availableFlightSearchDTO.setInfantCount(flighSearch.getInfantCount());
		availableFlightSearchDTO.setFareCategoryType(flighSearch.getFareType());
		availableFlightSearchDTO.setBookingPaxType(flighSearch.getPaxType());
		availableFlightSearchDTO.setFromAirport(flighSearch.getFromAirport());
		availableFlightSearchDTO.setToAirport(flighSearch.getToAirport());
		availableFlightSearchDTO.setDepartureVariance(flighSearch.getDepartureVariance());
		availableFlightSearchDTO.setArrivalVariance(flighSearch.getReturnVariance());
		availableFlightSearchDTO.setFromAirportSubStation(flighSearch.getFromAirportSubStation());
		availableFlightSearchDTO.setToAirportSubStation(flighSearch.getToAirportSubStation());
		try {
			availableFlightSearchDTO.setDepartureDate(smf.parse(flighSearch.getDepartureDate()));
			availableFlightSearchDTO.setReturnFlag(flighSearch.isReturnFlag());

			if (flighSearch.getReturnDate() != null && !flighSearch.getReturnDate().trim().isEmpty()) {
				if (smf.parse(flighSearch.getReturnDate()).compareTo(availableFlightSearchDTO.getDepartureDate()) >= 0) {
					availableFlightSearchDTO.setReturnDate(smf.parse(flighSearch.getReturnDate()));
					availableFlightSearchDTO.setReturnFlag(true);
				}
			}
		} catch (ParseException prse) {
			log.error(prse);
		}
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));

		selCOS = flighSearch.getClassOfService();
		selCOS = (selCOS != null && !selCOS.equals("")) ? selCOS : ModuleServiceLocator.getGlobalConfig().getBizParam(
				SystemParamKeys.CLASS_OF_SERVICE);
		availableFlightSearchDTO.setCabinClassCode(selCOS);

		// Modify
		try {
			if (flighSearch.getFirstDeparture() != null && !flighSearch.getFirstDeparture().equals("")) {
				availableFlightSearchDTO.setFirstDepartureDateTimeZulu(sdf.parse(flighSearch.getFirstDeparture()));
			}

			if (flighSearch.getLastArrival() != null && !flighSearch.getLastArrival().equals("")) {
				availableFlightSearchDTO.setLastArrivalDateTimeZulu(sdf.parse(flighSearch.getLastArrival()));
			}
		} catch (ParseException prse) {
			log.error(prse);
		}
		availableFlightSearchDTO.getSearchDTO().setHalfReturnFareQuote(flighSearch.isHalfReturnFareQuote());
		availableFlightSearchDTO.getSearchDTO().setStayOverMillis(flighSearch.getStayOverTimeInMillis());
		availableFlightSearchDTO.getSearchDTO().setOndQuoteFlexi(flighSearch.getOndQuoteFlexi());
		availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
		availableFlightSearchDTO.getSearchDTO().setPromoCode(flighSearch.getPromoCode());
		if (flighSearch.getBankIdentificationNo() != null && !"".equals(flighSearch.getBankIdentificationNo())) {
			availableFlightSearchDTO.getSearchDTO().setBankIdentificationNo(
					Integer.parseInt(flighSearch.getBankIdentificationNo()));
		}
		HashMap<Double, List<Integer>> dateFareMap = new HashMap<Double, List<Integer>>();
		for (Entry<String, List<Integer>> entry : flighSearch.getFlightDateFareMap().entrySet()) {
			dateFareMap.put(new Double(entry.getKey()), entry.getValue());
		}
		availableFlightSearchDTO.getSearchDTO().setFlightDateFareMap(dateFareMap);
		availableFlightSearchDTO.getSearchDTO().setPointOfSale(flighSearch.getPointOfSale());
		return availableFlightSearchDTO;
	}

	/**
	 * Compose outbound, inbound flight segments, and farequote from Flight Available response and selected currency
	 * 
	 * @param flightAvailRS
	 * @param selectedCurrency
	 * @param isInbound
	 * @param discountAmount
	 * @return Object[] containing results. Object[4] contains segment fare Quote.
	 * @throws ModuleException
	 */
	public static Object[] composeDisplayObjects(FlightAvailRS flightAvailRS, String selectedCurrency, boolean nextPrevious,
			boolean isInbound, Locale locale, BigDecimal discountAmount) throws ModuleException {

		List<AvailableFlightInfo> selectedDateOutboundFlights = new ArrayList<AvailableFlightInfo>();

		// Segment fare outbound flights for the selected date
		List<AvailableFlightInfo> selectedDateSegmentOutboundFlights = new ArrayList<AvailableFlightInfo>();

		List<AvailableFlightInfo> selectedDateReturnFlights = new ArrayList<AvailableFlightInfo>();

		// Segment fare return flights for the selected date.
		List<AvailableFlightInfo> selectedDateSegmentReturnFlights = new ArrayList<AvailableFlightInfo>();

		List<AvailableFlightInfo> allOutboundFlights = new ArrayList<AvailableFlightInfo>();
		List<AvailableFlightInfo> allReturnFlights = new ArrayList<AvailableFlightInfo>();

		boolean returnFaresAvailable = false;
		FareQuoteTO fareQuote = new FareQuoteTO();

		// Segment fare quote.
		FareQuoteTO segmentFareQuote = null;
		boolean applyReturnFareDiscount = false;

		String faresInCurrency = selectedCurrency;
		boolean isOutboundSelected = false;
		boolean isInboundSelected = false;

		boolean isOutboundSelectedForSegment = false;
		boolean isInboundSelectedForSegment = false;

		if (!AppSysParamsUtil.showCalFareInSelectedCurrency()) {
			faresInCurrency = AppSysParamsUtil.getBaseCurrency();
		}
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(faresInCurrency, ApplicationEngine.IBE);

		if (exchangeRate == null) {
			faresInCurrency = AppSysParamsUtil.getBaseCurrency();
			exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(faresInCurrency, ApplicationEngine.IBE);
		}
		if (flightAvailRS != null) {
			for (OriginDestinationInformationTO ondInfo : flightAvailRS.getOriginDestinationInformationList()) {
				for (OriginDestinationOptionTO ondOption : ondInfo.getOrignDestinationOptions()) {
					AvailableFlightInfo availFltInfo = BeanUtil.getAvailableFlightInfo(ondOption, nextPrevious, exchangeRate,
							isInbound, locale, isOutboundSelected, isInboundSelected);
					/*
					 * For next previous action, ondInfo is not set as return for inbound segment search. It cannot be
					 * set since we only want segment fares for next previous action. But in avail flight info it's
					 * available. Therefore if nextPrevious is true and availFltInfo.isReturnFlag() is true it's a
					 * return flight.
					 */
					if (!ondInfo.isReturnFlag() && !(nextPrevious && availFltInfo.isReturnFlag())) {
						allOutboundFlights.add(availFltInfo);
						if (availFltInfo.isSelected()) {
							isOutboundSelected = true;
						}

					} else {
						returnFaresAvailable = true;
						allReturnFlights.add(availFltInfo);
						if (availFltInfo.isSelected()) {
							isInboundSelected = true;
						}
					}
				}

				// Get segment fare options
				for (OriginDestinationOptionTO ondOption : ondInfo.getOriginDestinationSegFareOptions()) {
					AvailableFlightInfo availFltInfo = BeanUtil.getAvailableFlightInfo(ondOption, nextPrevious, exchangeRate,
							isInbound, locale, isOutboundSelectedForSegment, isInboundSelectedForSegment);
					// Only selected segment fares for selected return fares will(should) come here.
					// availFltInfo.setSelected(true);
					if (ondInfo.isReturnFlag() || isInbound) {
						selectedDateSegmentReturnFlights.add(availFltInfo);
						if (availFltInfo.isSelected()) {
							isInboundSelectedForSegment = true;
						}
					} else {
						selectedDateSegmentOutboundFlights.add(availFltInfo);
						if (availFltInfo.isSelected()) {
							isOutboundSelectedForSegment = true;
						}
					}
				}
			}

			for (OriginDestinationInformationTO ondInfo : flightAvailRS.getOriginDestinationInformationList()) {
				if (ondInfo.isReturnFlag() || isInbound) {
					selectedDateReturnFlights.addAll(getSelectedDateAvailableFlights(allReturnFlights,
							getSelectedStartTime(ondInfo.getPreferredDate()), getSelectedEndTime(ondInfo.getPreferredDate())));
				} else {
					selectedDateOutboundFlights.addAll(getSelectedDateAvailableFlights(allOutboundFlights,
							getSelectedStartTime(ondInfo.getPreferredDate()), getSelectedEndTime(ondInfo.getPreferredDate())));
				}
			}

			boolean fillFareQuote = !selectedDateOutboundFlights.isEmpty()
					&& ((returnFaresAvailable && !selectedDateReturnFlights.isEmpty() || !returnFaresAvailable));
			if (flightAvailRS.getSelectedPriceFlightInfo() != null && fillFareQuote) {
				fareQuote = BeanUtil.fillFareQuote(fareQuote, flightAvailRS.getSelectedPriceFlightInfo(), selectedCurrency,
						selectedDateOutboundFlights, selectedDateReturnFlights, exchangeRateProxy, discountAmount);
			}

			// Create the the segment fare quote if data is available
			if (flightAvailRS.getSelectedSegmentFlightPriceInfo() != null && fillFareQuote
					&& isReturnSearch(flightAvailRS.getSelectedPriceFlightInfo().getAvailableLogicalCCList())) {
				segmentFareQuote = BeanUtil.fillFareQuote(segmentFareQuote, flightAvailRS.getSelectedSegmentFlightPriceInfo(),
						selectedCurrency, selectedDateSegmentOutboundFlights, selectedDateSegmentReturnFlights,
						exchangeRateProxy, null);

				applyReturnFareDiscount = new BigDecimal(segmentFareQuote.getTotalPrice()).compareTo(new BigDecimal(fareQuote
						.getTotalPrice())) > 0;
				boolean replaceReturnFareFlights = new BigDecimal(segmentFareQuote.getTotalPrice()).compareTo(new BigDecimal(
						fareQuote.getTotalPrice())) == 1;
				if (replaceReturnFareFlights) {
					// Replace after all calculations are done.
					allOutboundFlights = replaceReturnFareSelectedFlights(allOutboundFlights, selectedDateSegmentOutboundFlights,
							flightAvailRS.getSelectedPriceFlightInfo());
					if (!selectedDateSegmentReturnFlights.isEmpty()) {
						allReturnFlights = replaceReturnFareSelectedFlights(allReturnFlights, selectedDateSegmentReturnFlights,
								flightAvailRS.getSelectedPriceFlightInfo());
					}
				}
			}
		}

		Object[] arrObj = new Object[6];
		arrObj[0] = allOutboundFlights;
		arrObj[1] = allReturnFlights;
		arrObj[2] = fareQuote;
		arrObj[3] = faresInCurrency;
		arrObj[4] = segmentFareQuote;
		arrObj[5] = applyReturnFareDiscount;
		return arrObj;
	}

	/**
	 * Selected flight segment contains return fare quote price details that will be displayed in the IBE calendar. So
	 * they need to be replaced by SegFare selected flight segment for display purposes.
	 * 
	 * @param selectedFareFlights
	 *            List<AvailableFlightInfo> of selected fare flights.
	 * @param selectedSegFareFlights
	 *            List<AvailableFlightInfo> the selected segment fare flight that will replace the selected return fare
	 *            flight.
	 * @return List<AvailableFlightInfo> of selected fare flights after being replaced by the segment fare flight.
	 */
	private static List<AvailableFlightInfo> replaceReturnFareSelectedFlights(List<AvailableFlightInfo> selectedFareFlights,
			List<AvailableFlightInfo> selectedSegFareFlights, PriceInfoTO priceInfoTO) {

		List<OndClassOfServiceSummeryTO> availableLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
		boolean hasOutboundFlexi = false;
		boolean hasInboundFlexi = false;

		if (availableLogicalCCList != null) {
			if (availableLogicalCCList.get(OndSequence.OUT_BOUND) != null
					&& availableLogicalCCList.get(OndSequence.OUT_BOUND).getAvailableLogicalCCList() != null) {
				for (LogicalCabinClassInfoTO logicalCCInfoTO : availableLogicalCCList.get(OndSequence.OUT_BOUND)
						.getAvailableLogicalCCList()) {
					if (logicalCCInfoTO.isWithFlexi() && logicalCCInfoTO.getBundledFarePeriodId() == null) {
						hasOutboundFlexi = true;
						break;
					}
				}
			}

			if (availableLogicalCCList.get(OndSequence.IN_BOUND) != null
					&& availableLogicalCCList.get(OndSequence.IN_BOUND).getAvailableLogicalCCList() != null) {
				for (LogicalCabinClassInfoTO logicalCCInfoTO : availableLogicalCCList.get(OndSequence.IN_BOUND)
						.getAvailableLogicalCCList()) {
					if (logicalCCInfoTO.isWithFlexi() && logicalCCInfoTO.getBundledFarePeriodId() == null) {
						hasInboundFlexi = true;
						break;
					}
				}
			}
		}

		for (ListIterator<AvailableFlightInfo> iterator = selectedFareFlights.listIterator(); iterator.hasNext();) {
			AvailableFlightInfo availableFlightInfo = iterator.next();
			for (AvailableFlightInfo selectedSegFareFlight : selectedSegFareFlights) {

				if (availableFlightInfo.compareTo(selectedSegFareFlight) == 0) {
					// Remove flexi if selected fare doesn't have flexi
					Iterator<FlightFareSummaryTO> fltFareSummaryTOIte = selectedSegFareFlight.getFlightFareSummaryList()
							.iterator();
					while (fltFareSummaryTOIte.hasNext()) {
						FlightFareSummaryTO fltFareSummaryTO = fltFareSummaryTOIte.next();
						if (fltFareSummaryTO.isWithFlexi() && !availableFlightInfo.isReturnFlag() && !hasOutboundFlexi) {
							fltFareSummaryTOIte.remove();
						} else if (fltFareSummaryTO.isWithFlexi() && availableFlightInfo.isReturnFlag() && !hasInboundFlexi) {
							fltFareSummaryTOIte.remove();
						}
					}

					// Appending any BundledFares available in the availableFlightInfo
					selectedSegFareFlight.getFlightFareSummaryList().addAll(
							getAvailableBundledFareForSelectedSegFareFlight(availableFlightInfo, selectedSegFareFlight));

					iterator.set(selectedSegFareFlight);
				}
			}
		}

		return selectedFareFlights;
	}

	private static boolean isReturnSearch(List<OndClassOfServiceSummeryTO> availableLogicalCCList) {
		if (availableLogicalCCList != null && availableLogicalCCList.size() < 2) {
			return false;
		}
		return true;
	}

	private static Collection<AvailableFlightInfo> getSelectedDateAvailableFlights(
			Collection<AvailableFlightInfo> availableFlights, Date selectedDepartureStartTime, Date selectedDepartureEndTime) {
		Collection<AvailableFlightInfo> selectedDateAvailableFlights = new ArrayList<AvailableFlightInfo>();
		for (AvailableFlightInfo availableFlightInfo : availableFlights) {
			if (CalendarUtil.isBetweenIncludingLimits(availableFlightInfo.getDepartureDate(), selectedDepartureStartTime,
					selectedDepartureEndTime)) {
				selectedDateAvailableFlights.add(availableFlightInfo);
			}
		}
		return selectedDateAvailableFlights;
	}

	public static List<AvailableFlightInfo> getSelectedDateAvailableFlights(
			Collection<AvailableFlightInfoByDateDTO> flightsByDateList) {
		List<AvailableFlightInfo> availableFlightInfos = new ArrayList<AvailableFlightInfo>();

		for (AvailableFlightInfoByDateDTO availableFlightInfoByDateDTO : flightsByDateList) {
			if (availableFlightInfoByDateDTO.isSelected() && availableFlightInfoByDateDTO.getAvailableFlightInfo() != null) {
				availableFlightInfos.addAll(availableFlightInfoByDateDTO.getAvailableFlightInfo());
			}
		}

		return availableFlightInfos;
	}

	public static Collection<LogicalCabinClassInfoTO> getAvailableLogicalCabinClassInfo(String selectedCabinClassCode,
			String language, List<AvailableFlightInfo> availableFlightInfos, String selectedLanguage) {
		Set<LogicalCabinClassInfoTO> availableLogicalCabinClassInfo = new HashSet<LogicalCabinClassInfoTO>();
		boolean isBundledServiceAvailable = false;
		if (!AppSysParamsUtil.displayOnlyAllocatedLogicalCabinClasses()) {
			Map<String, com.isa.thinair.commons.api.dto.LogicalCabinClassDTO> cachedLccMap = CommonsServices.getGlobalConfig()
					.getAvailableLogicalCCMap(language);
			if (cachedLccMap != null) {

				for (Entry<String, com.isa.thinair.commons.api.dto.LogicalCabinClassDTO> entry : cachedLccMap.entrySet()) {
					com.isa.thinair.commons.api.dto.LogicalCabinClassDTO cachedLcc = entry.getValue();
					if (CommonsConstants.STATUS_INACTIVE.equals(cachedLcc.getStatus())
							|| !cachedLcc.getCabinClassCode().equals(selectedCabinClassCode)) {
						continue;
					}

					if (AppSysParamsUtil.isFlexiFareEnabled() && AppSysParamsUtil.isDisplayFlexiAsSeparteBundle()) {
						// If flexi fare enabled in the airline

						// Creating dummy logical cc for with flexi logical cc
						LogicalCabinClassInfoTO logicalCabinClassInfoTOWithFlexi = new LogicalCabinClassInfoTO();
						logicalCabinClassInfoTOWithFlexi.setLogicalCCCode(cachedLcc.getLogicalCCCode());
						if (cachedLcc.isFreeFlexiEnabled()) {
							logicalCabinClassInfoTOWithFlexi.setLogicalCCDesc(cachedLcc.getDescription());
						} else {
							logicalCabinClassInfoTOWithFlexi.setLogicalCCDesc(cachedLcc.getFlexiDescription());
						}
						logicalCabinClassInfoTOWithFlexi.setWithFlexi(true);
						logicalCabinClassInfoTOWithFlexi.setRank(cachedLcc.getNestRank());
						logicalCabinClassInfoTOWithFlexi.setComment(cachedLcc.getFlexiComment());
						logicalCabinClassInfoTOWithFlexi.setImageUrl(OndConvertUtil.generateLogicalCCImageUrl(
								cachedLcc.getLogicalCCCode(), true, selectedLanguage));
						logicalCabinClassInfoTOWithFlexi.setBundleFareDescriptionTemplateDTO(cachedLcc.getBundleFareDescriptionTemplateDTO());
						availableLogicalCabinClassInfo.add(logicalCabinClassInfoTOWithFlexi);

						// If logical cabin class defined as not free flexi enabled
						if (!cachedLcc.isFreeFlexiEnabled()) {
							LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
							logicalCabinClassInfoTO.setLogicalCCCode(cachedLcc.getLogicalCCCode());
							logicalCabinClassInfoTO.setLogicalCCDesc(cachedLcc.getDescription());
							logicalCabinClassInfoTO.setWithFlexi(false);
							logicalCabinClassInfoTO.setRank(cachedLcc.getNestRank());
							logicalCabinClassInfoTO.setComment(cachedLcc.getComment());
							logicalCabinClassInfoTO.setImageUrl(OndConvertUtil.generateLogicalCCImageUrl(
									cachedLcc.getLogicalCCCode(), false, selectedLanguage));
							logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(cachedLcc.getBundleFareDescriptionTemplateDTO());
							availableLogicalCabinClassInfo.add(logicalCabinClassInfoTO);
						}
					} else {
						// If flexi fare disabled in the airline

						LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
						logicalCabinClassInfoTO.setLogicalCCCode(cachedLcc.getLogicalCCCode());
						logicalCabinClassInfoTO.setLogicalCCDesc(cachedLcc.getDescription());
						logicalCabinClassInfoTO.setWithFlexi(false);
						logicalCabinClassInfoTO.setRank(cachedLcc.getNestRank());
						logicalCabinClassInfoTO.setComment(cachedLcc.getComment());
						logicalCabinClassInfoTO.setImageUrl(OndConvertUtil.generateLogicalCCImageUrl(
								cachedLcc.getLogicalCCCode(), false, selectedLanguage));
						logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(cachedLcc.getBundleFareDescriptionTemplateDTO());
						availableLogicalCabinClassInfo.add(logicalCabinClassInfoTO);
					}
				}
			}
			if (availableFlightInfos != null) {
				for (AvailableFlightInfo availableFlightInfo : availableFlightInfos) {
					List<FlightFareSummaryTO> flightFareSummaryList = availableFlightInfo.getFlightFareSummaryList();
					if (flightFareSummaryList != null && flightFareSummaryList.size() > 0) {
						for (FlightFareSummaryTO flightFareSummaryTO : flightFareSummaryList) {
							if (flightFareSummaryTO.getBundledFarePeriodId() != null) {
								isBundledServiceAvailable = true;
								LogicalCabinClassInfoTO bundleFareLogicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
								bundleFareLogicalCabinClassInfoTO.setLogicalCCCode(flightFareSummaryTO.getLogicalCCCode());
								bundleFareLogicalCabinClassInfoTO.setBundledFarePeriodId(flightFareSummaryTO.getBundledFarePeriodId());
								bundleFareLogicalCabinClassInfoTO.setLogicalCCDesc(flightFareSummaryTO.getLogicalCCDesc());
								bundleFareLogicalCabinClassInfoTO.setSelected(flightFareSummaryTO.isSelected());
								bundleFareLogicalCabinClassInfoTO.setRank(flightFareSummaryTO.getRank());
								bundleFareLogicalCabinClassInfoTO.setComment(flightFareSummaryTO.getComment());
								bundleFareLogicalCabinClassInfoTO.setBookingClasses(flightFareSummaryTO.getBookingCodes());
								bundleFareLogicalCabinClassInfoTO.setSegmentBookingClasses(flightFareSummaryTO
										.getSegmentBookingClasses());
								bundleFareLogicalCabinClassInfoTO.setBundledFareFee(flightFareSummaryTO.getBundleFareFee());
								bundleFareLogicalCabinClassInfoTO.setBundledFareFreeServiceName(flightFareSummaryTO
										.getBundledFareFreeServiceNames());
								bundleFareLogicalCabinClassInfoTO.setWithFlexi(flightFareSummaryTO.isWithFlexi());
								bundleFareLogicalCabinClassInfoTO.setImageUrl(flightFareSummaryTO.getImageUrl());
								bundleFareLogicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(flightFareSummaryTO.getBundleFareDescriptionTemplateDTO());
								availableLogicalCabinClassInfo.add(bundleFareLogicalCabinClassInfoTO);
							}
						}
					}
				}
			}

		} else {
			if (availableFlightInfos != null) {
				for (AvailableFlightInfo availableFlightInfo : availableFlightInfos) {
					List<FlightFareSummaryTO> flightFareSummaryList = availableFlightInfo.getFlightFareSummaryList();
					if (flightFareSummaryList != null && flightFareSummaryList.size() > 0) {
						for (FlightFareSummaryTO flightFareSummaryTO : flightFareSummaryList) {
							LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
							logicalCabinClassInfoTO.setLogicalCCCode(flightFareSummaryTO.getLogicalCCCode());
							logicalCabinClassInfoTO.setLogicalCCDesc(flightFareSummaryTO.getLogicalCCDesc());
							logicalCabinClassInfoTO.setSelected(flightFareSummaryTO.isSelected());
							logicalCabinClassInfoTO.setWithFlexi(flightFareSummaryTO.isWithFlexi());
							logicalCabinClassInfoTO.setRank(flightFareSummaryTO.getLogicalCCRank());
							logicalCabinClassInfoTO.setComment(flightFareSummaryTO.getComment());
							logicalCabinClassInfoTO.setBundledFarePeriodId(flightFareSummaryTO.getBundledFarePeriodId());
							logicalCabinClassInfoTO.setBookingClasses(flightFareSummaryTO.getBookingCodes());
							logicalCabinClassInfoTO.setSegmentBookingClasses(flightFareSummaryTO.getSegmentBookingClasses());
							logicalCabinClassInfoTO.setBundledFareFee(flightFareSummaryTO.getBundleFareFee());
							logicalCabinClassInfoTO.setBundledFareFreeServiceName(flightFareSummaryTO
									.getBundledFareFreeServiceNames());
							logicalCabinClassInfoTO.setImageUrl(flightFareSummaryTO.getImageUrl());
							logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(flightFareSummaryTO.getBundleFareDescriptionTemplateDTO());
							availableLogicalCabinClassInfo.add(logicalCabinClassInfoTO);

							isBundledServiceAvailable = (flightFareSummaryTO.getBundledFarePeriodId() != null);
						}
					}
				}
			}
		}
		List<LogicalCabinClassInfoTO> sortedLogicalCabinClassInfoTOs = new ArrayList<LogicalCabinClassInfoTO>(
				availableLogicalCabinClassInfo);

		if (isBundledServiceAvailable) {
			// override rank with total price
			if (availableFlightInfos != null) {
				Map<LogicalCabinClassInfoTO, BigDecimal> rankMap = new HashMap<LogicalCabinClassInfoTO, BigDecimal>();
				for (AvailableFlightInfo availableFlightInfo : availableFlightInfos) {
					List<FlightFareSummaryTO> flightFareSummaryList = availableFlightInfo.getFlightFareSummaryList();
					for (FlightFareSummaryTO flightFareSummaryTO : flightFareSummaryList) {
						for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : sortedLogicalCabinClassInfoTOs) {
							if (flightFareSummaryTO.getLogicalCCCode().equals(logicalCabinClassInfoTO.getLogicalCCCode())
									&& flightFareSummaryTO.isWithFlexi() == logicalCabinClassInfoTO.isWithFlexi()
									&& BeanUtils.nullHandler(flightFareSummaryTO.getBundledFarePeriodId()).equals(
											BeanUtils.nullHandler(logicalCabinClassInfoTO.getBundledFarePeriodId()))) {

								if (rankMap.containsKey(logicalCabinClassInfoTO)) {
									BigDecimal currentRank = rankMap.get(logicalCabinClassInfoTO);
									BigDecimal newRank = flightFareSummaryTO.getCalDisplayAmout();

									if (AccelAeroCalculator.isLessThan(newRank, currentRank)) {
										rankMap.put(logicalCabinClassInfoTO, newRank);
									}

								} else {
									rankMap.put(logicalCabinClassInfoTO, flightFareSummaryTO.getCalDisplayAmout());
								}
							}
						}
					}
				}

				for (Entry<LogicalCabinClassInfoTO, BigDecimal> rankEntry : rankMap.entrySet()) {
					if (rankEntry.getValue() != null) {
						rankEntry.getKey().setRank(rankEntry.getValue().intValue());
					}
				}

				Set<LogicalCabinClassInfoTO> updatedLcc = rankMap.keySet();
				if (updatedLcc != null && !updatedLcc.containsAll(sortedLogicalCabinClassInfoTOs)) {
					for (LogicalCabinClassInfoTO sortedLcc : sortedLogicalCabinClassInfoTOs) {
						if (!updatedLcc.contains(sortedLcc)) {
							sortedLcc.setRank(Integer.MAX_VALUE);
						}
					}
				}
			}
		}

		Collections.sort(sortedLogicalCabinClassInfoTOs);
		return sortedLogicalCabinClassInfoTOs;

	}

	/**
	 * Get Search Info Data
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static FlightSearchInfoDTO getSearchInfoDTO(HttpServletRequest request) throws ModuleException {
		FlightSearchInfoDTO searchInfo = new FlightSearchInfoDTO();
		searchInfo.setCurrencyInfo(SelectListGenerator.createIBECurrencyActiveDescList());

		searchInfo.setAirportList(AirportDDListGenerator.createIBEAirportDDList(null));
		searchInfo.setCos(SelectListGenerator.getCOSActiveCodeDescList());
		searchInfo.setFareTypes(SelectListGenerator.getFareCategories(ReservationWebConstnts.REQ_RECORD_STATUS_ACTIVE));
		searchInfo.setPaxCategories(SelectListGenerator.getPaxCategories(ReservationWebConstnts.REQ_RECORD_STATUS_ACTIVE));

		return searchInfo;
	}

	public static FlightAvailRS getFlightAvailableFlights(FlightAvailRQ flightAvailRQ, TrackInfoDTO trackInfo)
			throws ModuleException {
		flightAvailRQ.getAvailPreferences().setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
		FlightAvailRS flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlightsWithAllFares(
				flightAvailRQ, trackInfo);
		return flightAvailRS;
	}

	/**
	 * Prepare Data for Calendar view
	 * 
	 * @param availableFlightInfoList
	 * @param dateStart
	 * @param dateEnd
	 * @param selectedDate
	 * @param availableLogicalCabinClasses
	 * @param selectedCabinClassCode
	 * @return
	 */

	public static Collection<AvailableFlightInfoByDateDTO> getFlightsGroupByDate(
			List<AvailableFlightInfo> availableFlightInfoList, Date dateStart, Date dateEnd, String selectedDate) {

		/**
		 * Functionality - Sorting <AvailableFlightInfo>, Build List of <AvailableFlightInfoByDateDTO> By Date(insert
		 * Empty flight data for empty availability result)
		 */
		Collection<AvailableFlightInfoByDateDTO> flights = new ArrayList<AvailableFlightInfoByDateDTO>();

		Calendar dateStartCal = Calendar.getInstance();
		Calendar dateEndCal = Calendar.getInstance();
		Calendar calDep = Calendar.getInstance();
		Date CurrentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		if (dateStart.compareTo(CurrentDate) < 0) {
			dateStartCal.setTime(CurrentDate);
		} else {
			dateStartCal.setTime(dateStart);
		}

		if (dateEnd.compareTo(CurrentDate) < 0) {
			dateEndCal.setTime(CurrentDate);
		} else {
			dateEndCal.setTime(dateEnd);
		}

		SimpleDateFormat smpDtFormat = new SimpleDateFormat("dd/MM/yyyy");
		Collections.sort(availableFlightInfoList);
		while (dateEndCal.after(dateStartCal)) {
			AvailableFlightInfoByDateDTO availableFlightInfoByDateDTO = new AvailableFlightInfoByDateDTO();
			Collection<AvailableFlightInfo> availableFlightInfos = new LinkedHashSet<AvailableFlightInfo>();

			for (AvailableFlightInfo flightInfo : availableFlightInfoList) {
				calDep.setTime(flightInfo.getDepartureDate());
				String dateStr = smpDtFormat.format(flightInfo.getDepartureDate());

				if (dateStartCal.get(Calendar.DAY_OF_MONTH) == calDep.get(Calendar.DAY_OF_MONTH)) {
					if (dateStr.equals(selectedDate)) {
						availableFlightInfoByDateDTO.setSelected(true);
					}
					availableFlightInfoByDateDTO.setDepartureDate(dateStr);
					availableFlightInfos.add(flightInfo);
				} else if (dateStartCal.compareTo(calDep) < 0) {
					break;
				}
			}

			if (AppSysParamsUtil.displayDirectFlightsFirst()) {
				availableFlightInfos = sortFlightsBySegments(availableFlightInfos);
			}

			availableFlightInfoByDateDTO.setAvailableFlightInfo(availableFlightInfos);

			if (availableFlightInfoByDateDTO.getAvailableFlightInfo() == null
					|| availableFlightInfoByDateDTO.getAvailableFlightInfo().size() == 0) {
				availableFlightInfoByDateDTO.setDepartureDate(smpDtFormat.format(dateStartCal.getTime()));
			} else {
				// set min fare to display on calendar head
				availableFlightInfoByDateDTO.setMinFare(getMinimumFareForDate(availableFlightInfoByDateDTO
						.getAvailableFlightInfo()));
			}
			flights.add(availableFlightInfoByDateDTO);
			dateStartCal.add(Calendar.DATE, 1);
		}

		return flights;
	}

	/**
	 * Sorts the available flights so direct flights will come on top. Flights with multiple legs, segments will come at
	 * the bottom.
	 * 
	 * @param availableFlightInfos
	 *            FLights info objects to be sorted.
	 * @return list of sorted flightInfo list.
	 */
	public static Collection<AvailableFlightInfo> sortFlightsBySegments(Collection<AvailableFlightInfo> availableFlightInfos) {

		ArrayList<AvailableFlightInfo> tempList = new ArrayList<AvailableFlightInfo>();
		tempList.addAll(availableFlightInfos);
		Collections.sort(tempList, new Comparator<AvailableFlightInfo>() {
			@Override
			public int compare(AvailableFlightInfo flightInfo1, AvailableFlightInfo flightInfo2) {
				int pointCount1 = flightInfo1.getSegments().size();
				int pointCount2 = flightInfo2.getSegments().size();

				/*
				 * If the flight has only one segment then legs(stops) needed to be considered. So if pointCount is one
				 */
				if (pointCount1 == 1) {
					SegInfoDTO seg = flightInfo1.getSegments().get(0);
					/*
					 * Basic segment code is like SHJ/CMB splitting will give an array of length 2. Which is why -1 is
					 * there.
					 */
					pointCount1 = (seg.getSegmentShortCode().split("/").length - 1);
				}
				if (pointCount2 == 1) {
					SegInfoDTO seg = flightInfo2.getSegments().get(0);
					pointCount2 = (seg.getSegmentShortCode().split("/").length - 1);
				}
				return Double.compare(pointCount1, pointCount2);
			}
		});

		return tempList;
	}

	private static BigDecimal getMinimumFareForDate(Collection<AvailableFlightInfo> availableFlightInfos) {
		BigDecimal minFare = null;
		for (AvailableFlightInfo availableFlightInfo : availableFlightInfos) {
			List<FlightFareSummaryTO> flightFareSummaryList = availableFlightInfo.getFlightFareSummaryList();
			if (flightFareSummaryList != null) {
				for (FlightFareSummaryTO flightFareSummaryTO : flightFareSummaryList) {
					if (flightFareSummaryTO.isSelected()) {
						return flightFareSummaryTO.getCalDisplayAmout();
					}

					if (minFare == null) {
						minFare = flightFareSummaryTO.getCalDisplayAmout();
					} else if (minFare.compareTo(flightFareSummaryTO.getCalDisplayAmout()) == 1) {
						minFare = flightFareSummaryTO.getCalDisplayAmout();
					}
				}
			}
		}
		return minFare;
	}

	public static void setModifySegmentData(FlightAvailRQ flightAvailRQ, int fareID, int fareType, String amount) {
		GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
		String strForce = globalConfig.getBizParam(SystemParamKeys.ENFORCE_SAME_HIGHER);
		if (strForce != null && strForce.equalsIgnoreCase("Y")) {
			if (!StringUtil.isNullOrEmpty(amount)) {
				flightAvailRQ.getAvailPreferences().setOldFareAmount(new BigDecimal(amount));
			}
			flightAvailRQ.getAvailPreferences().setOldFareID(fareID);
			flightAvailRQ.getAvailPreferences().setOldFareType(fareType);
		} else {
			flightAvailRQ.getAvailPreferences().setOldFareAmount(null);
			flightAvailRQ.getAvailPreferences().setOldFareID(0);
			flightAvailRQ.getAvailPreferences().setOldFareType(0);
		}
	}

	public static void setModifingSegmentSearchData(AvailableFlightSearchDTOBuilder avilDto, int fareID, int fareType,
			String amount) {
		GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
		String strForce = globalConfig.getBizParam(SystemParamKeys.ENFORCE_SAME_HIGHER);
		if (strForce != null && strForce.equalsIgnoreCase("Y")) {
			avilDto.getSearchDTO().setModifiedOndFareId(fareID);
			avilDto.getSearchDTO().setModifiedOndFareType(fareType);
			if (amount != null && !amount.isEmpty()) {
				avilDto.getSearchDTO().setModifiedOndPaxFareAmount(new BigDecimal(amount));
			}
		} else {
			avilDto.getSearchDTO().setModifiedOndFareId(0);
			avilDto.getSearchDTO().setModifiedOndFareType(0);
			avilDto.getSearchDTO().setModifiedOndPaxFareAmount(null);
		}
	}

	public static void setPriceGuide(Collection<AvailableFlightInfoByDateDTO> flights, BigDecimal existingMaxAmount,
			BigDecimal existingMinAmount) {

		TreeSet<BigDecimal> fareTreeSet = new TreeSet<BigDecimal>();
		BigDecimal maxAmounnt = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal minAmounnt = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal rangeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		int dayCount = 0;// to handle the Next day previous day gradient
		if (flights != null) {
			for (AvailableFlightInfoByDateDTO byDateDTO : flights) {
				dayCount++;
				Collection<AvailableFlightInfo> availablFlights = byDateDTO.getAvailableFlightInfo();
				if (availablFlights != null) {
					for (AvailableFlightInfo availableFlightInfo : availablFlights) {
						// fareTreeSet.add(availableFlightInfo.getFlightFareSummary().getBaseFareAmount());
						// fareTreeSet.add(availableFlightInfo.getFlightFareSummary().getTotalPrice());
						FlightFareSummaryTO fltFareSummary = availableFlightInfo.getSelectedFlightFareSummary();
						BigDecimal calAmt = fltFareSummary == null
								? AccelAeroCalculator.getDefaultBigDecimalZero()
								: fltFareSummary.getCalDisplayAmout();
						fareTreeSet.add(calAmt);
					}
				}
			}
		}
		// Handle Next/ Previous
		if (dayCount == 1) {
			// if (!existingMaxAmount.equals(BigDecimal.ZERO)) {//when flight full situation can send the 0 as minimum
			// and maximum
			fareTreeSet.add(existingMaxAmount);
			// }
			// if (!existingMinAmount.equals(BigDecimal.ZERO)) {
			fareTreeSet.add(existingMinAmount);
			// }
		}

		if (!fareTreeSet.isEmpty()) {
			maxAmounnt = fareTreeSet.last();
			minAmounnt = fareTreeSet.first();
		}

		rangeAmount = AccelAeroCalculator.divide(AccelAeroCalculator.subtract(maxAmounnt, minAmounnt), new BigDecimal(
				WebConstants.MAX_PRICE_GUIDE));

		if (flights != null) {
			for (AvailableFlightInfoByDateDTO byDateDTO : flights) {
				Collection<AvailableFlightInfo> availablFlights = byDateDTO.getAvailableFlightInfo();
				if (availablFlights != null) {
					for (AvailableFlightInfo availableFlightInfo : availablFlights) {
						if (maxAmounnt.equals(minAmounnt)) {
							availableFlightInfo.setPriceGuidNo(1);
						} else {
							// availableFlightInfo.setPriceGuidNo(getPriceGuideNo(availableFlightInfo.getFlightFareSummary().getBaseFareAmount(),
							// rangeAmount, minAmounnt));
							FlightFareSummaryTO fltFareSummary = availableFlightInfo.getSelectedFlightFareSummary();
							BigDecimal calAmt = fltFareSummary == null
									? AccelAeroCalculator.getDefaultBigDecimalZero()
									: fltFareSummary.getCalDisplayAmout();
							availableFlightInfo.setPriceGuidNo(getPriceGuideNo(calAmt, rangeAmount, minAmounnt));
						}
					}
				}
			}
		}
	}

	private static int getPriceGuideNo(BigDecimal fareAmount, BigDecimal range, BigDecimal lowestFare) {
		int intPriceGuidNo = 1;
		BigDecimal priceGuideNo = AccelAeroCalculator.divide(AccelAeroCalculator.subtract(fareAmount, lowestFare), range);
		double dobleAmount = priceGuideNo.doubleValue();

		if (dobleAmount <= 1) {
			intPriceGuidNo = 1;
		} else if (dobleAmount <= 2) {
			intPriceGuidNo = 2;
		} else if (dobleAmount <= 3) {
			intPriceGuidNo = 3;
		} else if (dobleAmount <= 4) {
			intPriceGuidNo = 4;
		} else if (dobleAmount <= 5) {
			intPriceGuidNo = 5;
		}

		return intPriceGuidNo;
	}

	public static void setExistingSegInfo(FlightAvailRQ flightAvailRQ, String oldAllSegments, String modifyingSegmentRefNos,
			boolean returnFlag) throws ModuleException, Exception {

		Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
		Collections.sort((List) colsegs);

		String carrierCode = null;
		for (LCCClientReservationSegment seg : colsegs) {
			if (carrierCode == null) {
				carrierCode = seg.getCarrierCode();
			} else if (carrierCode.equals(seg.getCarrierCode())) {
				continue;
			} else {
				return;
			}
		}

		flightAvailRQ.getAvailPreferences().setExistingSegmentsList(
				ReservationBeanUtil.convertReservationSegmentsToFlightSegmentDTOs(colsegs));

		Collection<LCCClientReservationSegment> colModifiedSegs = new ArrayList<LCCClientReservationSegment>();
		if (modifyingSegmentRefNos != null) {
			String[] arrModifingAllSegs = modifyingSegmentRefNos.split(":");
			for (String arrModifingAllSeg : arrModifingAllSegs) {
				for (LCCClientReservationSegment resSeg : colsegs) {
					if (resSeg.getBookingFlightSegmentRefNumber().equals(arrModifingAllSeg)) {
						colModifiedSegs.add(resSeg);
					}
				}
			}
		}
		flightAvailRQ.getAvailPreferences().setModifiedSegmentsList(
				ReservationBeanUtil.convertReservationSegmentsToFlightSegmentDTOs(colModifiedSegs));

		if (modifyingSegmentRefNos != null) {
			String modifiedInterlineReturnGroupKey = null;
			String modifiedInterlineGroupKey = null;
			boolean isInboundFareModified = false;
			BigDecimal modifiedOndFareAmount = null;
			Integer modifiedOndFareId = null;

			for (LCCClientReservationSegment resSeg : colModifiedSegs) {
				modifiedInterlineGroupKey = resSeg.getInterlineGroupKey();
				modifiedInterlineReturnGroupKey = resSeg.getInterlineReturnGroupKey();
				isInboundFareModified = "Y".equals(resSeg.getReturnFlag());
				if (resSeg.getFareTO() != null) {
					modifiedOndFareAmount = resSeg.getFareTO().getAmount();
					modifiedOndFareId = resSeg.getFareTO().getFareId();
				}
				break;
			}

			flightAvailRQ.getAvailPreferences().setOldFareID(modifiedOndFareId);
			flightAvailRQ.getAvailPreferences().setOldFareAmount(modifiedOndFareAmount);
			flightAvailRQ.getAvailPreferences().setInboundFareModified(isInboundFareModified);

			if (!returnFlag) {
				// boolean blnAllowHalfReturnFaresForModification = AppSysParamsUtil
				// .isAllowHalfReturnFaresForModification();
				// if (blnAllowHalfReturnFaresForModification) {
				Collection<LCCClientReservationSegment> colInverseSegs = new ArrayList<LCCClientReservationSegment>();
				Integer inverseOndFareId = null;

				if (colModifiedSegs != null) {
					// Get inverse of modified segments
					for (LCCClientReservationSegment resSeg : colsegs) {
						if (!resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
							if (modifiedInterlineReturnGroupKey != null
									&& modifiedInterlineReturnGroupKey.equals(resSeg.getInterlineReturnGroupKey())) {// same
								// return
								// grouping
								if (modifiedInterlineGroupKey != null
										&& !modifiedInterlineGroupKey.equals(resSeg.getInterlineGroupKey())) { // different
									// ond
									// grouping
									colInverseSegs.add(resSeg);
									if (resSeg.getFareTO() != null) {
										inverseOndFareId = resSeg.getFareTO().getFareId();
									}
								}
							}
						}
					}
				}

				flightAvailRQ.getAvailPreferences().setInverseSegmentsList(
						ReservationBeanUtil.convertReservationSegmentsToFlightSegmentDTOs(colInverseSegs));
				flightAvailRQ.getAvailPreferences().setInverseFareID(inverseOndFareId);

				// }
			}
		}

	}

	public static void setExistingSegInfo(AvailableFlightSearchDTO availableFlightSearchDTO, String oldAllSegments,
			String modifyingSegmentRefNos) throws ModuleException, Exception {

		Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
		Collections.sort((List) colsegs);
		availableFlightSearchDTO.setExistingFlightSegments(ReservationBeanUtil
				.convertReservationSegmentsToFlightSegmentDTOs(colsegs));

		Collection<LCCClientReservationSegment> colModifiedSegs = new ArrayList<LCCClientReservationSegment>();
		if (modifyingSegmentRefNos != null) {
			String[] arrModifingAllSegs = modifyingSegmentRefNos.split(":");
			for (String arrModifingAllSeg : arrModifingAllSegs) {
				for (LCCClientReservationSegment resSeg : colsegs) {
					if (resSeg.getBookingFlightSegmentRefNumber().equals(arrModifingAllSeg)) {
						colModifiedSegs.add(resSeg);
					}
				}
			}
		}
		availableFlightSearchDTO.setModifiedFlightSegments(ReservationBeanUtil
				.convertReservationSegmentsToFlightSegmentDTOs(colModifiedSegs));

		if (modifyingSegmentRefNos != null) {
			String modifiedInterlineReturnGroupKey = null;
			String modifiedInterlineGroupKey = null;
			boolean isInboundFareModified = false;
			BigDecimal modifiedOndFareAmount = null;
			Integer modifiedOndFareId = null;

			for (LCCClientReservationSegment resSeg : colModifiedSegs) {
				modifiedInterlineGroupKey = resSeg.getInterlineGroupKey();
				modifiedInterlineReturnGroupKey = resSeg.getInterlineReturnGroupKey();
				isInboundFareModified = "Y".equals(resSeg.getReturnFlag());
				if (resSeg.getFareTO() != null) {
					modifiedOndFareAmount = resSeg.getFareTO().getAmount();
					modifiedOndFareId = resSeg.getFareTO().getFareId();
				}
				break;
			}

			availableFlightSearchDTO.setModifiedOndFareId(modifiedOndFareId);
			availableFlightSearchDTO.setModifiedOndPaxFareAmount(modifiedOndFareAmount);
			availableFlightSearchDTO.setInboundFareQuote(isInboundFareModified);

			if (!availableFlightSearchDTO.isReturnFlag()) {
				// boolean blnAllowHalfReturnFaresForModification = AppSysParamsUtil
				// .isAllowHalfReturnFaresForModification();
				// if (blnAllowHalfReturnFaresForModification) {
				Collection<LCCClientReservationSegment> colInverseSegs = new ArrayList<LCCClientReservationSegment>();
				Integer inverseOndFareId = null;

				if (colModifiedSegs != null) {
					// Get inverse of modified segments
					for (LCCClientReservationSegment resSeg : colsegs) {
						if (!resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
							if (modifiedInterlineReturnGroupKey != null
									&& modifiedInterlineReturnGroupKey.equals(resSeg.getInterlineReturnGroupKey())) {
								// same return grouping
								if (modifiedInterlineGroupKey != null
										&& !modifiedInterlineGroupKey.equals(resSeg.getInterlineGroupKey())) {
									// different ond grouping
									colInverseSegs.add(resSeg);
									if (resSeg.getFareTO() != null) {
										inverseOndFareId = resSeg.getFareTO().getFareId();
									}
								}
							}
						}
					}
				}

				availableFlightSearchDTO.setInverseSegmentsOfModifiedFlightSegment(ReservationBeanUtil
						.convertReservationSegmentsToFlightSegmentDTOs(colInverseSegs));
				availableFlightSearchDTO.setInverseFareID(inverseOndFareId);

				// }
			}
		}

	}

	public static String getFlexiMessage(List<ExternalChargeTO> flexiCharges, String language) {
		String flexiMessage = "";
		if (flexiCharges != null && language != null) {
			int count = 0;
			for (SurchargeTO surchargeTO : flexiCharges) {
				ExternalChargeTO flexiCharge = (ExternalChargeTO) surchargeTO;
				if (count != 0) {
					flexiMessage += "<br>";
				}
				flexiMessage += flexiCharge.getSegmentCode() + " - " + I18NUtil.getMessage("PgFlexi_lblFlexiMessage", language);
				String flexiModCnxMessage = I18NUtil.getMessage("PgFlexi_lblFlexiModCnxMessage", language);
				count++;
				for (FlexiInfoTO flexiInfoTO : flexiCharge.getAdditionalDetails()) {
					if (flexiInfoTO.getFlexibilityTypeId() == 1) {// Modification
						flexiMessage = flexiMessage.replace("#1", String.valueOf(flexiInfoTO.getAvailableCount()));
						flexiMessage = flexiMessage
								.replace("#2", String.valueOf((int) flexiInfoTO.getCutOverBufferInMins() / 60));
					} else if (flexiInfoTO.getFlexibilityTypeId() == 2) {// Cancellation
						flexiMessage = flexiMessage.replace("#3", flexiModCnxMessage);
					}
				}
				if (flexiMessage.contains("#3")) {
					flexiMessage = flexiMessage.replace("#3", "");
				}
			}
		}
		return flexiMessage;
	}

	/*
	 * For IBE Calendar Dispalys Bundled Fares Get BundledFare for the for flightSummayTO and Replace the celendar
	 * DispalyAmout bundle fare with Segment bundle fare
	 */
	private static List<FlightFareSummaryTO> getAvailableBundledFareForSelectedSegFareFlight(
			AvailableFlightInfo availableFlightInfo, AvailableFlightInfo selectedSegFareFlight) {
		List<FlightFareSummaryTO> FlightFareSummaryWithBundledFare = new ArrayList<FlightFareSummaryTO>();
		List<FlightFareSummaryTO> availableAllFlightFareSummaries = availableFlightInfo.getFlightFareSummaryList();
		Iterator<FlightFareSummaryTO> availableFlightFareSummaryIterator = availableAllFlightFareSummaries.iterator();
		while (availableFlightFareSummaryIterator.hasNext()) {
			FlightFareSummaryTO availablefligtFaresummaryTo = availableFlightFareSummaryIterator.next();
			if (availablefligtFaresummaryTo.getBundledFarePeriodId() != null) {
				BigDecimal basicFareCalDisplayAmount = getBasicFareAmount(availableAllFlightFareSummaries,
						availablefligtFaresummaryTo.getLogicalCCCode());
				BigDecimal bundledCalDisplayAmount = availablefligtFaresummaryTo.getCalDisplayAmout();

				BigDecimal bundlFareInCalDisplayCurr = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (bundledCalDisplayAmount != null) {
					bundlFareInCalDisplayCurr = AccelAeroCalculator.subtract(bundledCalDisplayAmount, basicFareCalDisplayAmount);
				} else {
					bundlFareInCalDisplayCurr = availablefligtFaresummaryTo.getBundleFareFee();
				}

				if (bundlFareInCalDisplayCurr == null) {
					bundlFareInCalDisplayCurr = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				for (FlightFareSummaryTO selectedfligtFaresummaryTo : selectedSegFareFlight.getFlightFareSummaryList()) {
					if (!selectedfligtFaresummaryTo.isWithFlexi()) {
						availablefligtFaresummaryTo.setCalDisplayAmout(bundlFareInCalDisplayCurr.add(selectedfligtFaresummaryTo
								.getCalDisplayAmout()));
						availablefligtFaresummaryTo.setNoOfAvailableSeats(selectedfligtFaresummaryTo.getNoOfAvailableSeats());
						availablefligtFaresummaryTo.setVisibleChannelName(selectedfligtFaresummaryTo.getVisibleChannelName());
					}
				}

				FlightFareSummaryWithBundledFare.add(availablefligtFaresummaryTo);
			}
		}

		return FlightFareSummaryWithBundledFare;
	}

	private static BigDecimal getBasicFareAmount(List<FlightFareSummaryTO> flightFareSummaryTOs, String bundledLogicalCC) {
		for (FlightFareSummaryTO flightFareSummaryTO : flightFareSummaryTOs) {
			if (!flightFareSummaryTO.isWithFlexi() && flightFareSummaryTO.getBundledFarePeriodId() == null
					&& flightFareSummaryTO.getLogicalCCCode().equals(bundledLogicalCC)) {
				return flightFareSummaryTO.getCalDisplayAmout();
			}
		}

		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public static void checkFlexiAvailability(Map<Integer, Boolean> quoteOndFlexiMap,
			List<OndClassOfServiceSummeryTO> availableLogicalCCList) throws ModuleException {
		if (quoteOndFlexiMap != null && quoteOndFlexiMap.size() > 0) {
			for (Entry<Integer, Boolean> entry : quoteOndFlexiMap.entrySet()) {
				if (availableLogicalCCList.size() > entry.getKey()) {
					OndClassOfServiceSummeryTO ondClassSummary = availableLogicalCCList.get(entry.getKey());
					if (entry.getValue() && ondClassSummary != null) {
						boolean hasFlexi = false;
						boolean isBundledServiceSelected = false;
						for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : ondClassSummary.getAvailableLogicalCCList()) {
							if (logicalCabinClassInfoTO.isWithFlexi() && logicalCabinClassInfoTO.getBundledFarePeriodId() == null) {
								hasFlexi = true;
							}

							if (logicalCabinClassInfoTO.getBundledFarePeriodId() != null && logicalCabinClassInfoTO.isSelected()) {
								isBundledServiceSelected = true;
							}
						}

						// Flexi validation should skip if bundled fare is selected
						if (!isBundledServiceSelected && !hasFlexi) {
							// TODO refactor this to a constant file or rebuild the calendar and remove this method
							throw new ModuleException("ibe.fare.quote.flexi.not.available");
						}
					}
				}
			}
		}
	}

	// TODO same methods duplicated in OndConvertAssembler.
	private static Date getSelectedEndTime(Date selectedDate) {
		Date date = new Date(selectedDate.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	private static Date getSelectedStartTime(Date selectedDate) {
		Date date = new Date(selectedDate.getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
}