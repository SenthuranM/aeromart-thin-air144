package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.BookingType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.OnHoldDTO;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.SessionAttribute;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants.StrutsAction;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.MessageUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants.Mode;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants.PaymentFlow;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.core.bl.payFort.PayFortPaymentUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaxPaymentUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;
import com.opensymphony.xwork2.ActionChainResult;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM, value = StrutsConstants.Jsp.Respro.INTERLINE_CONFIRM),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE, value = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE),
		@Result(name = StrutsConstants.Jsp.Respro.KIOSK_ONHOLD_CONFIRMATION, value = StrutsConstants.Jsp.Respro.KIOSK_ONHOLD_CONFIRMATION),
		@Result(name = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION, value = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION),
		@Result(name = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION_V3, value = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION_V3),
		@Result(name = StrutsConstants.Result.PAYMENT, type = ActionChainResult.class, value = StrutsConstants.Action.IBE_PAYMENT),
		@Result(name = StrutsConstants.Action.LOAD_CONTAINER, type = ActionChainResult.class, value = StrutsConstants.Action.LOAD_CONTAINER),
		@Result(name = StrutsConstants.Result.FARE_EXPIRED, value = StrutsConstants.Jsp.Common.PAYMENT_SUCEESS),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Result.LANDING_BACKBUTTON_HANDLE, value = StrutsConstants.Jsp.Common.BACKBUTTON_HANDLE_ERRORPAGE),
		@Result(name = StrutsConstants.Jsp.Common.REDIRECT, value = StrutsConstants.Jsp.Common.REDIRECT),
		@Result(name = StrutsConstants.Result.CARD_PAYMENT_ERROR, value = StrutsConstants.Jsp.Payment.CARD_PAYMENT_ERROR),
		@Result(name = StrutsConstants.Result.OFFLINE_PAYMENT_INFO, value = StrutsConstants.Jsp.Payment.OFFLINE_PAYMENT_INFO),
		@Result(name = StrutsConstants.Result.OFFLINE_PAYMENT_PAYFORT, value = StrutsConstants.Jsp.Payment.OFFLINE_PAYMENT_PAYFORT) })
public class InterLineConfirmationAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(InterLineConfirmationAction.class);
	private boolean isItineraryEmailGenerated = false;

	public String execute() {
		if (log.isDebugEnabled()) {
			log.debug("### InterLineConfirmationAction start... ###");
		}
		String forward = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE;
		SessionUtil.setSystemRefresh(request, new Integer(1));

		int intAtempts = 0;
		boolean onHold = false;
		boolean isPaymentSuccess = false;
		String msg = null;
		String language = SessionUtil.getLanguage(request);
		String brokerType = null;
		boolean switchToExternalURL = false;
		boolean isCreditCardError = false;
		boolean isException = false;
		boolean isViewPaymentInIframe = false;
		IBEReservationPostPaymentDTO postPayDTO = null;
		IPGTransactionResultDTO ipgTransactionResultDTO = null;
		try {
			if (SessionUtil.isSessionExpired(request)) {
				forward = StrutsConstants.Result.SESSION_EXPIRED;
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, CommonUtil.getRefundMessage(language));
			} else {
				postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
				IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
				if (postPayDTO == null) {
					SessionUtil.expireSession(request);
					forward = StrutsConstants.Result.SESSION_EXPIRED;
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
							CommonUtil.getRefundMessage(SessionUtil.getLanguage(request)));
				} else {
					intAtempts = setAttempts(request);
					String ipgDTOlog = "";
					String pnr = postPayDTO.getPnr();

					if (postPayDTO.getInvoiceStatus() != null && postPayDTO.getInvoiceStatus().equals(QiwiPRequest.FAIL)) {
						String paymentErrorCode = QiwiPRequest.QIWI_PAYMENT_CONF_FAILED + "." + postPayDTO.getErrorCode();
						CustomerUtil.revertLoyaltyRedemption(request);
						ModuleException me = getStandardPaymentBrokerError(paymentErrorCode);
						isCreditCardError = true;
						postPayDTO.setErrorCode(null);
						throw me;
					}

					if (postPayDTO.getInvoiceStatus() != null && postPayDTO.getInvoiceStatus().equals(QiwiPRequest.WAITING)
							&& (postPayDTO.getBillId() != null || postPayDTO.getBillId() != "")) {

						long timeToSpare = postPayDTO.getTimeToSpare();
						String billId = postPayDTO.getBillId();

						String waitingMsg = null;
						if (!postPayDTO.isOnHoldCreated()) {
							waitingMsg = getServerErrorMessage(request,
									ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR + QiwiPRequest.QIWI_PAYMENT_CONF_FAILED);
						} else {
							waitingMsg = getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR
									+ QiwiPRequest.QIWI_WAITING_FOR_PAYMENT_CONF);
						}
						waitingMsg = waitingMsg.replace("{0}", timeDiffInStr(timeToSpare));
						waitingMsg = waitingMsg.replace("{1}", billId);
						waitingMsg = waitingMsg.replace("{2}", pnr);

						log.debug("[InterlineConfirmationAction::Qiwi Offline Mode] Redirecting to Offline Info Page");

						request.setAttribute(StrutsConstants.Result.OFFLINE_PAYMENT_INFO, waitingMsg);
						request.setAttribute(WebConstants.PNR, pnr);
						postPayDTO.setInvoiceStatus(null);
						request.setAttribute(ReservationWebConstnts.LANGUAGE, language);
						return StrutsConstants.Result.OFFLINE_PAYMENT_INFO;
					}

					if (postPayDTO.isPayFortStatus() && postPayDTO.isPayFortStatusVoucher()) {

						String waitingMsg = null;
						if (!postPayDTO.isOnHoldCreated()) {
							waitingMsg = getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR
									+ PayFortPaymentUtils.PAYFORT_PAYSTORE_PAYMENT_FAIL);
						} else {
							waitingMsg = getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR
									+ PayFortPaymentUtils.PAYFORT_PAYSTORE_PAYMENT_CONF);
						}

						String voucherId = (String) request.getAttribute(Pay_AT_StoreResponseDTO.VOUCHER_ID);
						String requestId = (String) request.getAttribute(Pay_AT_StoreResponseDTO.REQUEST_ID);

						waitingMsg = waitingMsg.replace("{0}", pnr);

						// load payment related properties and information
						List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD()
								.getActivePaymentGatewayByProviderName(PayFortPaymentUtils.PAY_AT_STORE);
						IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
						IPGIdentificationParamsDTO ipgIdentificationParamsDTO;
						ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
								payFortPgwConfigs.getPaymentGateway(), payFortPgwConfigs.getBaseCurrency());
						Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
						String scriptUrl = ipgProps.getProperty("payAtstoreScriptUrl");
						request.setAttribute(Pay_AT_StoreResponseDTO.PAY_AT_STORE_SCRIPT_URL, scriptUrl);

						log.debug(
								"[InterlineConfirmationAction::PayFort Offline pay@store ] Redirecting to Offline payfort Page");

						request.setAttribute(StrutsConstants.Result.OFFLINE_PAYMENT_INFO, waitingMsg);
						request.setAttribute(WebConstants.PNR, pnr);
						postPayDTO.setInvoiceStatus(null);
						request.setAttribute(ReservationWebConstnts.LANGUAGE, language);
						return StrutsConstants.Result.OFFLINE_PAYMENT_PAYFORT;
					}

					if (postPayDTO.getIpgResponseDTO() != null) {
						ipgTransactionResultDTO = postPayDTO.getIpgResponseDTO().getIpgTransactionResultDTO();
						if (!postPayDTO.getIpgResponseDTO().isSuccess()) {
							if (postPayDTO.getIpgResponseDTO() != null) {
								ipgDTOlog = "[status:" + postPayDTO.getIpgResponseDTO().getStatus() + ",tmpPayId:"
										+ postPayDTO.getIpgResponseDTO().getTemporyPaymentId() + ",payBkrRefNo:"
										+ postPayDTO.getIpgResponseDTO().getPaymentBrokerRefNo() + "]";
							}
							String paymentErrorCode = postPayDTO.getIpgResponseDTO().getErrorCode();
							if (paymentErrorCode != null && paymentErrorCode.trim().length() > 0) {
								CustomerUtil.revertLoyaltyRedemption(request);
								ModuleException me = getStandardPaymentBrokerError(paymentErrorCode);
								PaymentGateWayInfoDTO pgInfo = postPayDTO.getPaymentGateWay();
								switchToExternalURL = (pgInfo != null ? pgInfo.isSwitchToExternalURL() : switchToExternalURL);

								if (switchToExternalURL == false && postPayDTO.isSwitchToExternalURL()) {
									switchToExternalURL = true;
								}

								isViewPaymentInIframe = postPayDTO.isViewPaymentInIframe();

								if (isViewPaymentInIframe || switchToExternalURL) {
									request.setAttribute(WebConstants.DISPLAY_TOP_BOTTOM_BANNERS, false);
									request.setAttribute(WebConstants.INVALID_CARD, false);
								} else {
									request.setAttribute(WebConstants.DISPLAY_TOP_BOTTOM_BANNERS, true);
									request.setAttribute(WebConstants.INVALID_CARD, true);
									resInfo.setFromPaymentPage(true);
									resInfo.setFromPostCardDetails(false);
								}

								isCreditCardError = true;

								throw me;

							} else {
								if (log.isInfoEnabled())
									log.info("Possible Intruder IP Detected : " + getClientInfoDTO().getIpAddress());
								throw new ModuleException(ExceptionConstants.ERR_PAYMENT_SESSION_EXPIRED);
							}
						}
					}
					// Null check, code will break in modification with no payment
					if (postPayDTO.getIpgResponseDTO() != null)
						isPaymentSuccess = postPayDTO.getIpgResponseDTO().isSuccess();

					IPGIdentificationParamsDTO ipgDTO = null;
					PayCurrencyDTO payCurrencyDTO = null;
					Date paymentTimestamp = null;
					BookingType bookingType = postPayDTO.getBookingType();
					OnHold onHoldApp = OnHold.DEFAULT;
					IPGResponseDTO creditInfo = postPayDTO.getIpgResponseDTO();

					if (!postPayDTO.isNoPay() && bookingType != BookingType.ONHOLD && !postPayDTO.hasNoCCPayment()) {
						String strPayCurCode = postPayDTO.getPaymentGateWay().getPayCurrency();
						int ipgId = postPayDTO.getPaymentGateWay().getPaymentGateway();

						ipgDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(new Integer(ipgId), strPayCurCode);
						CommonCreditCardPaymentInfo creditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils
								.getFirstElement(postPayDTO.getTemporyPaymentMap().values());
						payCurrencyDTO = creditCardPaymentInfo.getPayCurrencyDTO();
						paymentTimestamp = creditCardPaymentInfo.getTxnDateTime();

						if (creditInfo.geteDirhamFee() != null
								&& creditInfo.geteDirhamFee().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
							BigDecimal actualPayAmountWithEDirham = AccelAeroCalculator
									.add(payCurrencyDTO.getTotalPayCurrencyAmount(), creditInfo.geteDirhamFee());
							payCurrencyDTO.seteDirhamFee(creditInfo.geteDirhamFee());
							payCurrencyDTO.setTotalPayCurrencyAmount(actualPayAmountWithEDirham);
							creditCardPaymentInfo.setPayCurrencyAmount(actualPayAmountWithEDirham);
						}
					}

					brokerType = getPaymentBrokerType(ipgDTO);
					Map<Integer, CommonCreditCardPaymentInfo> tempPayMap = postPayDTO.getTemporyPaymentMap();
					if (tempPayMap != null && !PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL
							.equals(brokerType)) {
						for (CommonCreditCardPaymentInfo ccPayInfo : tempPayMap.values()) {
							ccPayInfo.setNo(creditInfo.getCcLast4Digits());
						}
					}

					// If registered user update the contact details with
					// the customer id
					ContactInfoDTO contactInfo = postPayDTO.getContactInfo();
					Integer customerId = SessionUtil.getCustomerId(request);
					String loyaltyAccount = SessionUtil.getLoyaltyAccountNo(request);
					String loyaltyAgent = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.LOYALITY_AGENT_CODE);
					if (customerId.intValue() >= 0) {
						contactInfo.setCustomerId(customerId);
					}

					if (contactInfo.isUpdateCustomerProf()) {
						AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
						Customer oldCust = customerDelegate.getCustomer(contactInfo.getEmailAddress());

						if (oldCust != null) {
							Customer newCust = CustomerUtilV2.createCustomerProfile(contactInfo, oldCust);
							customerDelegate.saveOrUpdate(newCust);
						}
					}

					Date depDateTimeZulu = null;
					Date pnrReleaseTimeZulu = null;
					boolean isValidOnHold = true;
					String accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);
					PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
					FareTypeTO fareTypeTo = null;

					if (priceInfoTO != null)
						fareTypeTo = priceInfoTO.getFareTypeTO();

					OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReservationBeanUtil
							.getOnHoldReleaseTimeDTO(postPayDTO.getSelectedFlightSegments(), fareTypeTo);
					boolean isOnHoldBeforePayment = false;
					if (SessionUtil.getPutOnHoldBeforePaymentDTO(request) != null) {
						isOnHoldBeforePayment = SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment();
					}
					if (CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint)
							&& AppSysParamsUtil.isOnHoldEnable(OnHold.KIOSK) && bookingType == BookingType.ONHOLD) {
						onHold = true;
						onHoldApp = OnHold.KIOSK;
						depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(postPayDTO.getSelectedFlightSegments());
						pnrReleaseTimeZulu = ReservationUtil.calculateOnHoldReleaseTime(OnHold.KIOSK, depDateTimeZulu,
								onHoldReleaseTimeDTO);
						isValidOnHold = ReservationUtil.isOnHoldEnable(OnHold.KIOSK, pnrReleaseTimeZulu);
					} else if (AppSysParamsUtil.isOnHoldEnable(OnHold.IBEPAYMENT) && isOnHoldBeforePayment) {
						onHold = true;
						onHoldApp = OnHold.IBEPAYMENT;
						depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(postPayDTO.getSelectedFlightSegments());
						pnrReleaseTimeZulu = ReservationUtil.calculateOnHoldReleaseTime(OnHold.IBEPAYMENT, depDateTimeZulu,
								onHoldReleaseTimeDTO);
						isValidOnHold = ReservationUtil.isOnHoldEnable(OnHold.IBEPAYMENT, pnrReleaseTimeZulu);

					} else if (AppSysParamsUtil.isOnHoldEnable(OnHold.IBE) && bookingType == BookingType.ONHOLD) {
						onHold = true;
						onHoldApp = OnHold.IBE;
						depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(postPayDTO.getSelectedFlightSegments());
						pnrReleaseTimeZulu = ReservationUtil.calculateOnHoldReleaseTime(OnHold.IBE, depDateTimeZulu,
								onHoldReleaseTimeDTO);
						isValidOnHold = ReservationUtil.isOnHoldEnable(OnHold.IBE, pnrReleaseTimeZulu);

						if (isValidOnHold) {
							isValidOnHold &= resInfo.isReservationOnholdableCombineValidation();
						}
					}

					if (!isOnHoldBeforePayment && (onHold && !isValidOnHold)) {
						log.error("Invalid OnHold Booking - Departure Flight Date is not valid for OnHold Booking");
						throw new ModuleException(ExceptionConstants.SERVER_OPERATION_FAIL);
					}

					if (onHold && bookingType == BookingType.ONHOLD) {
						if (CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint)) {
							forward = StrutsConstants.Jsp.Respro.KIOSK_ONHOLD_CONFIRMATION;
						} else {
							if (SessionUtil.isNewDesignVersion(request)) {
								forward = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION_V3;
							} else {
								forward = StrutsConstants.Jsp.Respro.IBE_ONHOLD_CONFIRMATION;
							}
							Map<String, String> jsonLabel = I18NUtil.getMessagesInJSON(language, "PgOHDconfirm");
							request.setAttribute(ReservationWebConstnts.OHD_LABELS, JSONUtil.serialize(jsonLabel));
							request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_MODE, "onhold");
						}
					}

					// Set Page Building Parameters
					SystemUtil.setCommonParameters(request, getCommonParams());
					IBEReservationPostDTO iBEReservationPostDTO = new IBEReservationPostDTO();
					log.debug("### InterLineConfirmationAction Before main action(create/modify/Add Ancilary)... ###");

					boolean isCCPayment = postPayDTO.isAddCCExternalCharge() && !postPayDTO.isNoPay();
					ExternalChgDTO externalChgDTO = null;

					if (postPayDTO.getSelectedExternalCharges() != null) {
						externalChgDTO = postPayDTO.getSelectedExternalCharges().get(EXTERNAL_CHARGES.CREDIT_CARD);
					}

					// Set the e-dirhamFee as credit card charge
					if (isCCPayment && payCurrencyDTO != null && payCurrencyDTO.geteDirhamFee() != null
							&& payCurrencyDTO.geteDirhamFee().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						Collection colEXTERNAL_CHARGES = new ArrayList();
						colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
						Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD()
								.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MAKE_ONLY);
						externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
						externalChgDTO.setRatioValueInPercentage(false);
						externalChgDTO.setRatioValue(payCurrencyDTO.geteDirhamFee());
						externalChgDTO.setAmount(payCurrencyDTO.geteDirhamFee());
						postPayDTO.addExternalCharge(externalChgDTO);
					}

					if (isCCPayment && payCurrencyDTO != null) {
						request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_PAY_CURRENCY,
								payCurrencyDTO.getPayCurrencyCode());
						request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_TOTAL_PAY_AMOUNT,
								payCurrencyDTO.getTotalPayCurrencyAmount());
					}

					if (postPayDTO.isAddModifyAncillary()) {
						addModifyAncillary(pnr, postPayDTO, resInfo, contactInfo, tempPayMap, creditInfo, ipgDTO, payCurrencyDTO,
								paymentTimestamp, loyaltyAccount, loyaltyAgent, brokerType);
						iBEReservationPostDTO.setMode(Mode.ADD_MODIFY_ANCILARY);
					} else if (postPayDTO.isRequoteFlightSearch()) {
						LCCClientReservation modifyReservation = new LCCClientReservation();
						modifyReservation = requoteModifySegments(pnr, postPayDTO, resInfo, contactInfo, onHold, tempPayMap,
								creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp, loyaltyAccount, loyaltyAgent, brokerType);
						modifyReservation.setPNR(pnr);
						iBEReservationPostDTO.setMode(Mode.MODIFY_SEGMENT);
						iBEReservationPostDTO.setRefundSuccessful(modifyReservation.isSuccessfulRefund());
					} else if (postPayDTO.isModifySegment()) {
						LCCClientReservation modifyReservation = modifySegment(pnr, postPayDTO, resInfo, contactInfo, onHold,
								tempPayMap, creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp, loyaltyAccount, loyaltyAgent,
								brokerType);
						pnr = modifyReservation.getPNR();
						iBEReservationPostDTO.setMode(Mode.MODIFY_SEGMENT);
						iBEReservationPostDTO.setRefundSuccessful(modifyReservation.isSuccessfulRefund());
					} else if (postPayDTO.isAddGroundSegment()) {
						LCCClientReservation addGroundSegReservation = addGroundSegment(pnr, postPayDTO, resInfo, contactInfo,
								onHold, tempPayMap, creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp, loyaltyAccount,
								loyaltyAgent, brokerType);
						pnr = addGroundSegReservation.getPNR();
						iBEReservationPostDTO.setMode(Mode.ADD_GROUND_SEGMENT);
					} else if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()
							&& SessionUtil.getPutOnHoldBeforePaymentDTO(request).getPutOnHoldBeforePaymentPnr() != null) {
						if (bookingType != BookingType.ONHOLD) {
							confirmReservation(contactInfo, payCurrencyDTO, ipgDTO, (resInfo.getSelectedSystem() == SYSTEM.INT));
						}

						iBEReservationPostDTO.setMode(Mode.CREATE_RESERVATION);
						iBEReservationPostDTO
								.setPnr(SessionUtil.getPutOnHoldBeforePaymentDTO(request).getPutOnHoldBeforePaymentPnr());
						request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO,
								SessionUtil.getPutOnHoldBeforePaymentDTO(request).getPutOnHoldBeforePaymentPnr());
					} else {

						ReservationDiscountDTO resDiscountDTO = ReservationUtil.calculateDiscountForReservation(resInfo,
								postPayDTO.getPaxList(), postPayDTO.getFlightPriceRQ(), getTrackInfo(), false,
								resInfo.getDiscountInfo(), resInfo.getPriceInfoTO(), resInfo.getTransactionId(), true);

						LCCClientReservation commonReservation = ReservationUtil.makeReservation(pnr, postPayDTO, resInfo,
								contactInfo, onHold, tempPayMap, creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp,
								loyaltyAccount, loyaltyAgent, pnrReleaseTimeZulu, brokerType, getTrackInfo(), resDiscountDTO,
								null);
						pnr = commonReservation.getPNR();
						// Multiple response handling
						request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO, pnr);
						iBEReservationPostDTO.setPnr(pnr);

						sendEmail(pnr, (resInfo.getSelectedSystem() == SYSTEM.INT), commonReservation.getContactInfo(),
								commonReservation);
						sendOnHoldPaymentReminderEmail(pnr, (resInfo.getSelectedSystem() == SYSTEM.INT),
								commonReservation.getContactInfo(), commonReservation);

						iBEReservationPostDTO.setMode(Mode.CREATE_RESERVATION);

					}

					// send medical ssr email for any booking unless it is onhold
					if (bookingType == null || !bookingType.equals(BookingType.ONHOLD)) {
						boolean isGroupPNR = SessionUtil.getIBEreservationInfo(request).getSelectedSystem() == SYSTEM.INT;
						Customer cust = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
						LCCClientReservation lccReservation = ReservationUtil.loadProxyReservation(pnr, isGroupPNR,
								getTrackInfo(), true, cust != null, null, null, false);
						SSRServicesUtil.sendMedicalSsrEmail(lccReservation);
					}

					if (log.isDebugEnabled()) {
						log.debug("### InterLineConfirmationAction after main action(create/modify/Add Ancilary)... ###");
					}
					// Set Reservation Load Data
					iBEReservationPostDTO.setSelectedSystem(resInfo.getSelectedSystem());
					iBEReservationPostDTO.setIpgTransactionResultDTO(ipgTransactionResultDTO);
					if (iBEReservationPostDTO.getMode() != Mode.CREATE_RESERVATION) {
						iBEReservationPostDTO.setPnr(pnr);
						request.getSession().setAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO, pnr);
					}

					iBEReservationPostDTO.setItineraryEmailGenerated(isItineraryEmailGenerated);

					SessionUtil.setIbeReservationPostDTO(request, iBEReservationPostDTO);

					if (onHold) {
						String ohdPnr;
						if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
							ohdPnr = SessionUtil.getPutOnHoldBeforePaymentDTO(request).getPutOnHoldBeforePaymentPnr();
						} else {
							ohdPnr = pnr;
						}
						setOnHoldData(request, ohdPnr, onHoldApp, depDateTimeZulu, pnrReleaseTimeZulu);
					}

					if (log.isDebugEnabled()) {
						log.debug("Interline Reservation created. PNR [" + pnr + "] ");
					}

				}
			}

		} catch (Exception me) {
			revertLoyaltyRedemption();
			log.error("InterLineConfirmationAction", me);
			isException = true;
			/* If successful reservation exist. Load the reservation. */
			if (SessionUtil.isSuccessfulReservationExit(request)) {
				forward = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE;
			} else {
				boolean isPaymentBrokerError = false;

				if (me instanceof ModuleException) {
					ModuleException moduleException = (ModuleException) me;
					String errorCode = moduleException.getExceptionCode();
					/**
					 * When credit card payment fail , the system recreate the payment page for any payment gateway.
					 * 
					 */
					if (errorCode.equals(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR)) {
						isPaymentBrokerError = true;
						String strErrMsg = "";
						try {
							Map<String, String> errorMap = (Map) moduleException.getExceptionDetails();
							Iterator<String> itErrorMap = errorMap.keySet().iterator();

							if (itErrorMap.hasNext()) {
								errorCode = (String) itErrorMap.next();
							}

							strErrMsg = getServerErrorMessage(request,
									ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR + errorCode);
							if (errorCode == null || "".equals(errorCode) || strErrMsg == null || "".equals(strErrMsg)) {
								errorCode = "pay.gateway.error.less.0";
							}
						} catch (Exception ex) {
							log.error("InterLineConfirmationAction", ex);
							errorCode = "pay.gateway.error.less.0";
						}
						SessionUtil.getIBEreservationInfo(request).setFromPostCardDetails(false);
						Map<String, String> formHiddenDataMap = new HashMap<String, String>();
						formHiddenDataMap.put(ReservationWebConstnts.PARAM_PAYMENT_FLOW, PaymentFlow.PAYMENT_RETRY.toString());
						formHiddenDataMap.put(ReservationWebConstnts.MSG_ERROR_CODE, errorCode);
						formHiddenDataMap.put("fareQuoteJson", getFareQuoteJson());
						forward = StrutsConstants.Jsp.Common.REDIRECT;
						SystemUtil.createRedirectFormData(request, StrutsAction.LOAD_CONTAINER, formHiddenDataMap, true);

					} else {
						isPaymentBrokerError = false;
					}
				} else {
					isPaymentBrokerError = false;
				}

				if (isPaymentBrokerError == false) {
					if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
						forward = StrutsConstants.Result.CARD_PAYMENT_ERROR;
					} else {
						forward = StrutsConstants.Result.ERROR;
					}

					if (isPaymentSuccess) {
						msg = CommonUtil.getRefundMessage(language);
					} else {
						msg = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
					}
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
					SessionUtil.resetSesionDataInError(request);
				}
			}
		} finally {
			if (!forward.equals(StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE)
					&& !forward.equals(StrutsConstants.Result.OFFLINE_PAYMENT_INFO)) {
				try {
					SeatMapUtil.releaseBlockedSeats(request);
				} catch (Exception e1) {
					log.error("SEAT release failed", e1);
				}
			}

			if (isCreditCardError == false) {
				SessionUtil.setIBEreservationInfo(request, null);
				SessionUtil.setIBEReservationPostPaymentDTO(request, null);
				if (!isException) {
					SessionUtil.setPutOnHoldBeforePaymentDTO(request, null);
				}
			}

		}
		if (log.isDebugEnabled()) {
			log.debug("### InterLineConfirmationAction end... ###");
		}

		return forward;
	}

	private void confirmReservation(ContactInfoDTO contactInfo, PayCurrencyDTO payCurrencyDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, boolean isGroupPnr) throws Exception {
		// TODO isGroupPnr and version CHECK VERSION IN POSTPAYDTO
		boolean isGroupPNR = SessionUtil.getIBEreservationInfo(request).getSelectedSystem() == SYSTEM.INT;
		String pnr = SessionUtil.getPutOnHoldBeforePaymentDTO(request).getPutOnHoldBeforePaymentPnr();

		IBEReservationPostPaymentDTO postPaymentDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);

		Customer cust = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
		LCCClientReservation lccReservation = ReservationUtil.loadProxyReservation(pnr, isGroupPNR, getTrackInfo(), true,
				cust != null, null, null, false);
		LCCClientBalancePayment balancePayment = getBalancePayment(contactInfo, payCurrencyDTO, ipgIdentificationParamsDTO,
				lccReservation);
		balancePayment.setReservationStatus(lccReservation.getStatus());
		balancePayment.setPnrSegments(lccReservation.getSegments());
		balancePayment.setPassengers(lccReservation.getPassengers());
		balancePayment
				.setLoyaltyPaymentInfo(postPaymentDTO.getCarrierLoyaltyPaymentInfo(AppSysParamsUtil.getDefaultCarrierCode()));
		balancePayment.setPayByVoucherInfo(postPaymentDTO.getPayByVoucherInfo());

		Map<Integer, CommonCreditCardPaymentInfo> mapTnxIds = null;
		if (!SessionUtil.getIBEReservationPostPaymentDTO(request).hasNoCCPayment()) {
			mapTnxIds = SessionUtil.getIBEReservationPostPaymentDTO(request).getTemporyPaymentMap();
		}

		balancePayment.setTemporyPaymentMap(mapTnxIds);
		ServiceResponce responce = ModuleServiceLocator.getAirproxyReservationBD().balancePayment(balancePayment,
				lccReservation.getVersion(), isGroupPNR,
				AppSysParamsUtil.isFraudCheckEnabled(getTrackInfo().getOriginChannelId()), getClientInfoDTO(), getTrackInfo(),
				false, false);
		lccReservation = (LCCClientReservation) responce.getResponseParam(CommandParamNames.RESERVATION);

		sendEmail(pnr, isGroupPNR, lccReservation.getContactInfo(), lccReservation);
	}

	private int getNoOfAdultCreditCardPayment(Collection<LCCClientReservationPax> passengers, BigDecimal totalLoyaltyPayAmount,
			Map<Integer, BigDecimal> paxWiseRedeemedAmounts, boolean isInfantPaymentSeparated) {
		int adultCount = 0;
		if (passengers != null) {
			for (LCCClientReservationPax pax : passengers) {
				if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					BigDecimal amountDue = ReservationApiUtils.getAmountDueWhenOnlyInfantHasAmountDue(pax,
							isInfantPaymentSeparated);
					BigDecimal lmsMemberPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (paxWiseRedeemedAmounts.get(pax.getPaxSequence()) != null) {
						lmsMemberPayment = paxWiseRedeemedAmounts.get(pax.getPaxSequence());
					}

					if (amountDue.compareTo(lmsMemberPayment) > 0) {
						amountDue = AccelAeroCalculator.subtract(amountDue, lmsMemberPayment);
						if (amountDue.subtract(totalLoyaltyPayAmount).compareTo(BigDecimal.ZERO) < 0) {
							totalLoyaltyPayAmount = AccelAeroCalculator.subtract(totalLoyaltyPayAmount, amountDue);
						} else {
							totalLoyaltyPayAmount = BigDecimal.ZERO;
							adultCount++;
						}
					}

				}
			}
		}

		return adultCount;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	// This method need to re-factor
	// Temp commit to fix live urgent issue
	// Need to review
	private LCCClientBalancePayment getBalancePayment(ContactInfoDTO contactInfo, PayCurrencyDTO cardPayCurrencyDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, LCCClientReservation reservation) throws ModuleException {

		ExternalChgDTO externalChgDTO = null;
		ExternalChgDTO jnExternalChgDTO = null;
		int adultChildCount = 0;
		ExternalChargesMediator externalChargesMediator = null;
		LinkedList perPaxExternalCharges = null;
		boolean isLoyaltyPayExit = false;
		LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
		Collection<ExternalChgDTO> paxCharges = new ArrayList<ExternalChgDTO>();
		// lccClientBalancePayment.setOwnerAgent(ownerAgentCode);
		lccClientBalancePayment.setContactInfo(composeCommonReservationContactInfo(contactInfo));

		IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
		Collection<LCCClientReservationPax> passengers = reservation.getPassengers();
		lccClientBalancePayment.setGroupPNR(SessionUtil.getPutOnHoldBeforePaymentDTO(request).getPutOnHoldBeforePaymentPnr());
		IPGResponseDTO ipgResponseDTO = postPayDTO.getIpgResponseDTO();
		CommonCreditCardPaymentInfo creditInfo = BookingUtil.getCommonCreditCardPaymentInfo(postPayDTO.getTemporyPaymentMap(),
				ipgResponseDTO == null ? false : ipgResponseDTO.isSuccess());

		CommonCreditCardPaymentInfo creditCardPaymentInfo = null;
		BigDecimal totalLoyaltyPayAmount = BigDecimal.ZERO;
		Date currentDatetime = new Date();

		BigDecimal totalAmountAlreadyPaidAtIPG = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (!LoyaltyPaymentOption.NONE.equals(postPayDTO.getLoyaltyPayOption())) {
			totalLoyaltyPayAmount = postPayDTO.getLoyaltyCredit();
			isLoyaltyPayExit = true;
		}

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments = postPayDTO.getCarrierWiseLoyaltyPaymentInfo();
		Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
				.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);

		String loyaltyMemberAccountId = null;
		String[] rewardIDs = null;

		BigDecimal totalLMSMemberPayment = AccelAeroCalculator.getDefaultBigDecimalZero();

		PayByVoucherInfo payByVoucherInfo = postPayDTO.getPayByVoucherInfo();
		BigDecimal remainingVoucherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal voucherPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (payByVoucherInfo != null) {
			voucherPaymentAmount = payByVoucherInfo.getRedeemedTotal();
			remainingVoucherAmount = payByVoucherInfo.getRedeemedTotal();
		}

		if (!LoyaltyPaymentOption.TOTAL.equals(postPayDTO.getLoyaltyPayOption())) {
			creditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils
					.getFirstElement(postPayDTO.getTemporyPaymentMap().values());
			if (postPayDTO.getSelectedExternalCharges() != null) {
				externalChgDTO = postPayDTO.getSelectedExternalCharges().get(EXTERNAL_CHARGES.CREDIT_CARD);
				jnExternalChgDTO = postPayDTO.getSelectedExternalCharges().get(EXTERNAL_CHARGES.JN_OTHER);
			}

			// Credit card payment adult count
			adultChildCount = getNoOfAdultCreditCardPayment(passengers, totalLoyaltyPayAmount, paxWiseRedeemedAmounts,
					reservation.isInfantPaymentSeparated());

			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null)) {

				// To identify a onHold booking and skip adding cc charge
				boolean onHold = false;
				if (SessionUtil.getIBEreservationInfo(request).getReservationStatus() == null) {
					onHold = false;
				} else if (SessionUtil.getIBEreservationInfo(request).getReservationStatus().equals("OHD")) {
					onHold = true;
				} else {
					onHold = false;
				}
				if (!onHold && SessionUtil.getIBEreservationInfo(request).getReservationStatus() == null) {
					if (adultChildCount > 0) {
						// Should be base currency amount
						totalAmountAlreadyPaidAtIPG = creditCardPaymentInfo.getPayCurrencyAmount();
					}
				} else {
					if (adultChildCount > 0) {
						// Fix me: validate & review
						// IBE balance payment only credit card payment
						Map<EXTERNAL_CHARGES, ExternalChgDTO> creditCard = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
						creditCard.put(EXTERNAL_CHARGES.CREDIT_CARD, externalChgDTO);
						creditCard.put(EXTERNAL_CHARGES.JN_OTHER, jnExternalChgDTO);
						lccClientBalancePayment.setExternalChargesMap(creditCard);
						externalChargesMediator = new ExternalChargesMediator(postPayDTO.getPaxList(), creditCard, true, false);
						externalChargesMediator.setCalculateJNTaxForCCCharge(true);
						perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(adultChildCount, 0);
						// Should be base currency amount
						totalAmountAlreadyPaidAtIPG = creditCardPaymentInfo.getPayCurrencyAmount();
					}
				}

			} else {

				if (adultChildCount > 0) {
					// Fix me: validate & review
					// IBE balance payment only credit card payment
					Map<EXTERNAL_CHARGES, ExternalChgDTO> creditCard = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
					creditCard.put(EXTERNAL_CHARGES.CREDIT_CARD, externalChgDTO);
					creditCard.put(EXTERNAL_CHARGES.JN_OTHER, jnExternalChgDTO);
					lccClientBalancePayment.setExternalChargesMap(creditCard);
					externalChargesMediator = new ExternalChargesMediator(postPayDTO.getPaxList(), creditCard, true, false);
					externalChargesMediator.setCalculateJNTaxForCCCharge(true);
					perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(adultChildCount, 0);
					// Should be base currency amount
					totalAmountAlreadyPaidAtIPG = creditCardPaymentInfo.getPayCurrencyAmount();
				}
			}
		}

		for (LCCClientReservationPax pax : passengers) {

			Integer paxSequence = pax.getPaxSequence();
			BigDecimal amountDue = pax.getTotalAvailableBalance();

			if ((reservation.isInfantPaymentSeparated() || !pax.getPaxType().equals(PaxTypeTO.INFANT))
					&& (amountDue.compareTo(BigDecimal.ZERO) >= 0
							|| ReservationApiUtils.hasInfantOnlyAmountDue(pax, reservation.isInfantPaymentSeparated()))) {

				List<LCCClientExternalChgDTO> extCharges = new ArrayList<LCCClientExternalChgDTO>();
				if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
					Collection<ExternalChgDTO> ppec = ((perPaxExternalCharges != null && perPaxExternalCharges.size() > 0)
							? (Collection<ExternalChgDTO>) perPaxExternalCharges.pop()
							: null);
					extCharges = BookingUtil.converToProxyExtCharges(ppec);
				}
				PaymentAssemblerComposer paymentComposer = new PaymentAssemblerComposer(extCharges);

				BigDecimal lmsMemberPayment = reconcileLmsPayAmount(paxWiseRedeemedAmounts.get(paxSequence), amountDue);

				BigDecimal totalExtChgs = BookingUtil.getTotalExtCharge(extCharges);

				PaxPaymentUtil paxPayUtil = new PaxPaymentUtil(AccelAeroCalculator.add(amountDue, totalExtChgs), totalExtChgs,
						totalLoyaltyPayAmount, lmsMemberPayment, remainingVoucherAmount,
						AccelAeroCalculator.getDefaultBigDecimalZero());

				totalLoyaltyPayAmount = paxPayUtil.getRemainingLoyalty();
				remainingVoucherAmount = paxPayUtil.getRemainingVoucherAmount();
				while (paxPayUtil.hasConsumablePayments()) {
					PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
					BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
					if (payMethod == PaymentMethod.LMS) {
						for (String operatingCarrierCode : carrierWiseLoyaltyPayments.keySet()) {
							LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = carrierWiseLoyaltyPayments.get(operatingCarrierCode);
							Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
									.getPaxProductPaymentBreakdown();
							BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil
									.getPaxRedeemedTotal(carrierPaxProductRedeemed, paxSequence);

							loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
							rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
							totalLMSMemberPayment = AccelAeroCalculator.add(totalLMSMemberPayment, carrierLmsMemberPayment);
							if (cardPayCurrencyDTO != null) {
								cardPayCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
							}
							if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
								paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs, carrierPaxProductRedeemed,
										carrierLmsMemberPayment, cardPayCurrencyDTO, currentDatetime, operatingCarrierCode);
							}
						}
					} else if (payMethod == PaymentMethod.MASHREQ_LOYALTY) {
						paymentComposer.addLoyaltyCreditPayment(
								IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.LOYALITY_AGENT_CODE), paxPayAmt,
								SessionUtil.getLoyaltyAccountNo(request), cardPayCurrencyDTO, currentDatetime);
					} else if (payMethod == PaymentMethod.VOUCHER) {
						paymentComposer.addVoucherPayment(payByVoucherInfo, paxPayAmt, cardPayCurrencyDTO, currentDatetime);
					} else {
						paymentComposer.addCreditCardPaymentInternal(paxPayAmt, ipgResponseDTO, ipgIdentificationParamsDTO,
								cardPayCurrencyDTO, currentDatetime, creditInfo.getNo(), creditInfo.geteDate(),
								creditInfo.getSecurityCode(), creditInfo.getName(), creditCardPaymentInfo.getPayReference(),
								creditCardPaymentInfo.getActualPaymentMethod(), null);
					}

				}

				LCCClientPaymentAssembler paymentAssembler = paymentComposer.getPaymentAssembler();
				lccClientBalancePayment.addPassengerPayments(pax.getPaxSequence(), paymentAssembler);
			}
		}

		// This is because AARESAA-8570. Currently we don't do a match between the IPG payment amount with amount due.
		// Due to damm loyalty, we can't rely on the IPG amount to do the payment. So a check is enforced at the end
		// to verify these amounts tally or not.
		if (lccClientBalancePayment.getPaxSeqWisePayAssemblers() != null
				&& lccClientBalancePayment.getPaxSeqWisePayAssemblers().size() > 0) {

			BigDecimal totalAmountAboutToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (LCCClientPaymentAssembler paymentAssembler : lccClientBalancePayment.getPaxSeqWisePayAssemblers().values()) {
				totalAmountAboutToPay = AccelAeroCalculator.add(totalAmountAboutToPay, paymentAssembler.getTotalPayAmount());
			}

			BigDecimal totalAmountAlreadyPaid = AccelAeroCalculator.add(totalAmountAlreadyPaidAtIPG,
					postPayDTO.getLoyaltyCredit(), totalLMSMemberPayment, voucherPaymentAmount);
			if (AccelAeroCalculator.subtract(totalAmountAboutToPay, totalAmountAlreadyPaid).abs().doubleValue() > 0.3) {
				log.info("#airreservations.balance.payment.failed# LoyaltyCredit :" + postPayDTO.getLoyaltyCredit()
						+ " totalLMSMemberPayment : " + totalLMSMemberPayment + " voucherPaymentAmount :" + voucherPaymentAmount
						+ " totalAmountAboutToPay : " + totalAmountAboutToPay + " totalAmountAlreadyPaid : "
						+ totalAmountAlreadyPaid + " totalAmountAlreadyPaidAtIPG :" + totalAmountAlreadyPaidAtIPG);
				throw new ModuleException("airreservations.balance.payment.failed");
			}
		}

		return lccClientBalancePayment;

	}

	private static BigDecimal reconcileLmsPayAmount(BigDecimal lmsPayAmount, BigDecimal amountDue) {
		if ((lmsPayAmount != null && amountDue != null) && (lmsPayAmount.equals(amountDue) || AccelAeroCalculator
				.subtract(amountDue, lmsPayAmount).abs().compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0)) {
			return amountDue;
		}
		return lmsPayAmount;
	}

	private void sendEmail(String pnr, boolean isGroupPnr, CommonReservationContactInfo contactInfo,
			LCCClientReservation commonReservation) throws ModuleException {
		// Email Itinerary
		try {
			if (commonReservation.getStatus() != null
					&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(commonReservation.getStatus())) {
				if (BeanUtils.nullHandler(contactInfo.getEmail()).length() > 0) {
					String station = IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);

					LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, isGroupPnr, false, null, null, false);

					/*
					 * Need to set this to true to load the originCountry.
					 */
					pnrModesDTO.setLoadOriginCountry(true);

					CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
					commonItineraryParam.setItineraryLanguage(contactInfo.getPreferredLanguage());
					commonItineraryParam.setIncludePaxFinancials(true);
					commonItineraryParam.setIncludePaymentDetails(true);
					commonItineraryParam.setIncludeTicketCharges(false);
					commonItineraryParam.setIncludeTermsAndConditions(true);
					commonItineraryParam.setStation(station);
					commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
					commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(isGroupPnr));
					commonItineraryParam.setIncludePaxContactDetails(AppSysParamsUtil.showPassenegerContactDetailsInItinerary());
					commonItineraryParam.setIncludeStationContactDetails(AppSysParamsUtil.showStationContactDetailsInItinerary());

					if (AppSysParamsUtil.isPrintEmailIndItinerary()) {
						String selectedPax = "";
						for (int i = 1; i < (commonReservation.getTotalPaxAdultCount() + commonReservation.getTotalPaxChildCount()
								+ 1); i++)
							selectedPax += i + ",";

						String paxs[] = selectedPax.split(",");
						for (String pax : paxs) {
							if (pax != null || !pax.equals("")) {
								commonItineraryParam.setSelectedPaxDetails(pax);
								ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO,
										commonItineraryParam, getTrackInfo());
							}
						}
					} else
						ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
								getTrackInfo());

					this.isItineraryEmailGenerated = true;
				}
			}
		} catch (Exception e) {
			log.error("Error while trying to send itinerary by email after payment", e);
		}
	}

	/**
	 * 
	 * @param pnr
	 * @param isGroupPnr
	 * @param contactInfo
	 * @param commonReservation
	 * @throws ModuleException
	 */
	private void sendOnHoldPaymentReminderEmail(String pnr, boolean isGroupPnr, CommonReservationContactInfo contactInfo,
			LCCClientReservation commonReservation) throws ModuleException {

		if (AppSysParamsUtil.isOHDPaymentEamilEnabled() && contactInfo.getEmail() != null
				&& ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(commonReservation.getStatus())) {
			try {
				ModuleServiceLocator.getReservationBD().sendIBEOnholdPaymentReminderEmail(pnr, isGroupPnr, true);
			} catch (Exception e) {
				// Error is not thrown because email failures should not affect the booking flow
				log.error("Error while trying to send payment reminder by email after onhold for PNR : - " + pnr, e);
			}
		}
	}

	private int getAdultCount(Collection<ReservationPaxTO> paxList) {
		int j = 0;
		for (ReservationPaxTO pax : paxList) {
			if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
				j++;
			}
		}
		return j;
	}

	private LCCClientReservation modifySegment(String pnr, IBEReservationPostPaymentDTO postPayDTO, IBEReservationInfoDTO resInfo,
			ContactInfoDTO contactInfo, boolean onHold, Map<Integer, CommonCreditCardPaymentInfo> tempPayMap,
			IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp,
			String loyaltyAccount, String loyaltyAgent, String brokerType) throws Exception {

		BigDecimal customAdultCancelCharge = null;
		BigDecimal customInfantCancelCharge = null;
		BigDecimal customChildCancelCharge = null;

		LCCClientSegmentAssembler lccClientSegmentAssembler = BookingUtil.getSegmentAssembler(
				postPayDTO.getSelectedFlightSegments(),
				BookingUtil.getNextSegmentSequnce(postPayDTO.getExistingAllLccSegments()));
		// PriceInfoTO priceInfoTO = resInfo.getPriceInfoTO();
		PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
		FlightPriceRQ flightPriceRQ = postPayDTO.getFlightPriceRQ();
		lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
		lccClientSegmentAssembler.setLccTransactionIdentifier(postPayDTO.getTransactionId());
		lccClientSegmentAssembler.setTemporyPaymentMap(tempPayMap);
		lccClientSegmentAssembler.setContactInfo(BookingUtil.getContactDetails(contactInfo));
		BookingUtil.populateInsurance(postPayDTO, lccClientSegmentAssembler, getAdultCount(postPayDTO.getPaxList()));

		// Set External Payment Map
		lccClientSegmentAssembler
				.setExternalChargesMap(getExternalChargesMap(postPayDTO.getPaxList(), postPayDTO.getSelectedExternalCharges()));

		SSRServicesUtil.setSegmentSeqForSSR((List<ReservationPaxTO>) postPayDTO.getPaxList(),
				postPayDTO.getSelectedFlightSegments(), resInfo.getTotalSegmentCount());

		lccClientSegmentAssembler.setPassengerExtChargeMap(getPaxExtChargesMap(postPayDTO.getPaxList()));
		// Set Passenger Payment
		CommonCreditCardPaymentInfo cardPaymentInfo = BookingUtil.getCommonCreditCardPaymentInfo(tempPayMap,
				creditInfo == null ? false : creditInfo.isSuccess());
		lccClientSegmentAssembler.setPassengerPaymentsMap(
				getPaxPaymentMap(postPayDTO, resInfo, creditInfo, ipgDTO, paymentTimestamp, loyaltyAgent, loyaltyAccount,
						payCurrencyDTO, lccClientSegmentAssembler.getLccTransactionIdentifier(), brokerType, cardPaymentInfo));
		lccClientSegmentAssembler.setSelectedFareSegChargeTO(priceInfoTO.getFareSegChargeTO());
		lccClientSegmentAssembler.setFlexiSelected(postPayDTO.isFlexiQuote());

		LCCClientResAlterModesTO lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeModifySegmentRequest(pnr,
				postPayDTO.getModifiedLccSegments(), postPayDTO.getExistingAllLccSegments(), lccClientSegmentAssembler,
				customAdultCancelCharge, customChildCancelCharge, customInfantCancelCharge, postPayDTO.getVersion(),
				resInfo.getReservationStatus());
		lCCClientResAlterQueryModesTO.setAppIndicator(getTrackInfo().getAppIndicator());
		lCCClientResAlterQueryModesTO.setPaymentTypes(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);
		lCCClientResAlterQueryModesTO.setReservationStatus(resInfo.getReservationStatus());
		lCCClientResAlterQueryModesTO.setPassengers(
				new HashSet<LCCClientReservationPax>(ReservationUtil.getLccReservationPassengers(postPayDTO.getPaxList())));
		lCCClientResAlterQueryModesTO.setAnciOfferTemplates(resInfo.getAnciOfferTemplates());

		if (resInfo.getSelectedSystem() == SYSTEM.INT) {
			lCCClientResAlterQueryModesTO.setGroupPnr(true);
		} else {
			lCCClientResAlterQueryModesTO.setGroupPnr(false);
		}
		if (postPayDTO.getlCCClientReservationBalance().getTotalAmountDue().compareTo(BigDecimal.ZERO) == 0
				&& postPayDTO.isNoPay() == true
				&& (lCCClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerPaymentsMap() == null
						|| lCCClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerPaymentsMap().size() == 0)) {
			addExtChargesWithDummyPayments(postPayDTO.getlCCClientReservationBalance(),
					lCCClientResAlterQueryModesTO.getLccClientSegmentAssembler(), lCCClientResAlterQueryModesTO.isGroupPnr());
		}

		LCCClientReservation Modifyreservation = ModuleServiceLocator.getAirproxySegmentBD().modifySegments(
				lCCClientResAlterQueryModesTO, AppSysParamsUtil.isFraudCheckEnabled(getTrackInfo().getOriginChannelId()),
				getClientInfoDTO(), getTrackInfo());
		return Modifyreservation;
	}

	private LCCClientReservation addGroundSegment(String pnr, IBEReservationPostPaymentDTO postPayDTO,
			IBEReservationInfoDTO resInfo, ContactInfoDTO contactInfo, boolean onHold,
			Map<Integer, CommonCreditCardPaymentInfo> tempPayMap, IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String loyaltyAccount, String loyaltyAgent, String brokerType)
			throws Exception {

		BigDecimal customAdultCancelCharge = null;
		BigDecimal customInfantCancelCharge = null;
		BigDecimal customChildCancelCharge = null;

		LCCClientSegmentAssembler lccClientSegmentAssembler = BookingUtil.getSegmentAssembler(
				postPayDTO.getSelectedFlightSegments(),
				BookingUtil.getNextSegmentSequnce(postPayDTO.getExistingAllLccSegments()));
		// PriceInfoTO priceInfoTO = resInfo.getPriceInfoTO();
		PriceInfoTO priceInfoTO = postPayDTO.getPriceInfoTO();
		FlightPriceRQ flightPriceRQ = postPayDTO.getFlightPriceRQ();
		lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
		lccClientSegmentAssembler.setLccTransactionIdentifier(postPayDTO.getTransactionId());
		lccClientSegmentAssembler.setTemporyPaymentMap(tempPayMap);
		lccClientSegmentAssembler.setContactInfo(BookingUtil.getContactDetails(contactInfo));
		BookingUtil.populateInsurance(postPayDTO, lccClientSegmentAssembler, getAdultCount(postPayDTO.getPaxList()));

		// Set External Payment Map
		lccClientSegmentAssembler
				.setExternalChargesMap(getExternalChargesMap(postPayDTO.getPaxList(), postPayDTO.getSelectedExternalCharges()));
		// Set Passenger Payment
		CommonCreditCardPaymentInfo cardPaymentInfo = BookingUtil.getCommonCreditCardPaymentInfo(tempPayMap,
				creditInfo == null ? false : creditInfo.isSuccess());
		lccClientSegmentAssembler.setPassengerPaymentsMap(
				getPaxPaymentMap(postPayDTO, resInfo, creditInfo, ipgDTO, paymentTimestamp, loyaltyAgent, loyaltyAccount,
						payCurrencyDTO, lccClientSegmentAssembler.getLccTransactionIdentifier(), brokerType, cardPaymentInfo));
		lccClientSegmentAssembler.setSelectedFareSegChargeTO(priceInfoTO.getFareSegChargeTO());
		lccClientSegmentAssembler.setFlexiSelected(postPayDTO.isFlexiQuote());

		LCCClientResAlterModesTO lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeAddSegmentRequest(pnr, null,
				postPayDTO.getModifiedLccSegments(), lccClientSegmentAssembler, customAdultCancelCharge, customChildCancelCharge,
				customInfantCancelCharge, postPayDTO.getVersion(), postPayDTO.getSelectedPnrSegment(),
				resInfo.getReservationStatus());

		lCCClientResAlterQueryModesTO.setAppIndicator(getTrackInfo().getAppIndicator());
		lCCClientResAlterQueryModesTO.setPaymentTypes(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);
		if (resInfo.getSelectedSystem() == SYSTEM.INT) {
			lCCClientResAlterQueryModesTO.setGroupPnr(true);
		} else {
			lCCClientResAlterQueryModesTO.setGroupPnr(false);
		}

		LCCClientReservation segmentAddedReservation = ModuleServiceLocator.getAirproxySegmentBD().addSegment(
				lCCClientResAlterQueryModesTO, getClientInfoDTO(),
				AppSysParamsUtil.isFraudCheckEnabled(getTrackInfo().getOriginChannelId()), getTrackInfo());
		return segmentAddedReservation;
	}

	private LCCClientReservation requoteModifySegments(String pnr, IBEReservationPostPaymentDTO postPayDTO,
			IBEReservationInfoDTO resInfo, ContactInfoDTO contactInfo, boolean onHold,
			Map<Integer, CommonCreditCardPaymentInfo> tempPayMap, IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String loyaltyAccount, String loyaltyAgent, String brokerType)
			throws Exception {

		FlightPriceRQ flightPriceRQ = postPayDTO.getFlightPriceRQ();

		RequoteModifyRQ requoteModifyRQ = JSONFETOParser.parse(RequoteModifyRQ.class, postPayDTO.getBalanceQueryData());
		requoteModifyRQ.setContactInfo(ReservationUtil.retrieveContactInfo(contactInfo));
		requoteModifyRQ.setSystem(resInfo.getSelectedSystem());
		if (resInfo.getSelectedSystem() == SYSTEM.INT) {
			requoteModifyRQ.setSystem(SYSTEM.INT);
			requoteModifyRQ.setTransactionIdentifier(resInfo.getTransactionId());
			requoteModifyRQ.setCarrierWiseFlightRPHMap(ReservationCommonUtil.getCarrierWiseRPHs(postPayDTO.getOndList()));
			requoteModifyRQ.setGroupPnr(pnr);
			requoteModifyRQ.setVersion(ReservationCommonUtil.getCorrectInterlineVersion(requoteModifyRQ.getVersion()));
		}

		if (resInfo.getPriceInfoTO() != null) {
			QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(resInfo.getPriceInfoTO().getFareSegChargeTO(), flightPriceRQ,
					null);
			requoteModifyRQ.setFareInfo(fareInfo);
			requoteModifyRQ.setLastFareQuoteDate(resInfo.getPriceInfoTO().getLastFareQuotedDate());
			requoteModifyRQ.setFQWithinValidity(resInfo.getPriceInfoTO().isFQWithinValidity());
		}
		requoteModifyRQ.setRequoteSegmentMap(ReservationBeanUtil.getReQuoteResSegmentMap(postPayDTO.getOndList()));

		if (postPayDTO.isNameChange()) {
			requoteModifyRQ.setNameChangedPaxMap(ReservationUtil.transformToPaxMap(postPayDTO.getChangedPaxNames()));
		}

		Double remainingLoyaltyPoints = null;
		remainingLoyaltyPoints = CustomerUtil.getMemberAvailablePoints(request);
		requoteModifyRQ.setRemainingLoyaltyPoints(remainingLoyaltyPoints);

		Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();
		requoteModifyRQ.setOndFareTypeByFareIdMap(
				ReservationBeanUtil.getONDFareTypeByFareIdMap(postPayDTO.getOndList(), oldFareIdByFltSegIdMap));
		requoteModifyRQ.setOldFareIdByFltSegIdMap(oldFareIdByFltSegIdMap);

		BookingUtil.populateInsurance(postPayDTO.getInsuranceQuotes(), requoteModifyRQ, getAdultCount(postPayDTO),
				getTrackInfo());

		List<ReservationPaxTO> resPaxTo = (List<ReservationPaxTO>) postPayDTO.getPaxList();
		BigDecimal extraServicesCharge = BigDecimal.ZERO;
		Map<Integer, List<LCCClientExternalChgDTO>> paxMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		if (resPaxTo != null && !resPaxTo.isEmpty()) {
			for (ReservationPaxTO resPax : resPaxTo) {
				List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
				extChgList.addAll(resPax.getExternalCharges());
				paxMap.put(resPax.getSeqNumber(), extChgList);
				for (LCCClientExternalChgDTO exCharge : resPax.getExternalCharges()) {
					extraServicesCharge = AccelAeroCalculator.add(extraServicesCharge, exCharge.getAmount());
				}
			}
		}
		requoteModifyRQ.setPaxExtChgMap(paxMap);

		ReservationBalanceTO reservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
				.getRequoteBalanceSummary(requoteModifyRQ, getTrackInfo());
		requoteModifyRQ.setLastCurrencyCode(postPayDTO.getSelectedCurrency());
		requoteModifyRQ.setReservationStatus(resInfo.getReservationStatus());
		requoteModifyRQ.setExcludedSegFarePnrSegIds(resInfo.getExcludedSegFarePnrSegIds());

		Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap = reservationBalanceTO.getPaxWiseAdjustmentAmountMap();

		if (reservationBalanceTO.hasBalanceToPay()
				|| reservationBalanceTO.getTotalCreditAmount().compareTo(extraServicesCharge) < 0) {
			requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);
			requoteModifyRQ.setActualPayment(true);
			requoteModifyRQ.setLccTemporaryTnxMap(tempPayMap);

			SSRServicesUtil.setSegmentSeqForSSR((List<ReservationPaxTO>) postPayDTO.getPaxList(),
					postPayDTO.getSelectedFlightSegments(), resInfo.getTotalSegmentCount());

			CommonCreditCardPaymentInfo cardPaymentInfo = BookingUtil.getCommonCreditCardPaymentInfo(tempPayMap,
					creditInfo == null ? false : creditInfo.isSuccess());
			requoteModifyRQ.setLccPassengerPayments(getPaxPaymentMapWithTravelRefNo(postPayDTO, resInfo, creditInfo, ipgDTO,
					paymentTimestamp, loyaltyAgent, loyaltyAccount, payCurrencyDTO, null, brokerType, cardPaymentInfo));
			requoteModifyRQ.setExternalChargesMap(
					getExternalChargesMap(postPayDTO.getPaxList(), postPayDTO.getSelectedExternalCharges()));

		} else {
			requoteModifyRQ.setNoBalanceToPay(true);
			requoteModifyRQ.setPaxWiseAdjustmentAmountMap(paxWiseAdjustmentAmountMap);
			requoteModifyRQ.setExternalChargesMap(getExternalChargesMap(resPaxTo, resInfo.getSelectedExternalCharges()));
			addExtChargesWithDummyPayments(requoteModifyRQ, resPaxTo);
			requoteModifyRQ.setPaymentType(
					ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE);
		}

		requoteModifyRQ.setCarrierWiseLoyaltyPaymentInfo(resInfo.getCarrierWiseLoyaltyPaymentInfo());

		LCCClientReservation modifyreservation = ModuleServiceLocator.getAirproxyReservationBD()
				.requoteModifySegmentsWithAutoRefundForIBE(requoteModifyRQ, getTrackInfo());

		return modifyreservation;
	}

	private void addExtChargesWithDummyPayments(RequoteModifyRQ requoteModifyRQ, List<ReservationPaxTO> resPaxTos) {
		if (resPaxTos != null && !resPaxTos.isEmpty()) {
			for (ReservationPaxTO resPaxTo : resPaxTos) {
				if (!resPaxTo.getExternalCharges().isEmpty()) {
					LCCClientPaymentAssembler payAsm = new LCCClientPaymentAssembler();
					for (LCCClientExternalChgDTO externalCharge : resPaxTo.getExternalCharges()) {
						externalCharge.setAmountConsumedForPayment(true);
					}
					payAsm.getPerPaxExternalCharges().addAll(resPaxTo.getExternalCharges());
					payAsm.addCashPayment(BigDecimal.ZERO, null, new Date(), null, null, null, null);
					requoteModifyRQ.getLccPassengerPayments().put(resPaxTo.getTravelerRefNumber(), payAsm);
				}
			}
		}
	}

	/**
	 * 
	 * @param paxList
	 * @param selectedChargers
	 * @return
	 * @throws ModuleException
	 */
	private Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap(Collection<ReservationPaxTO> paxList,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedChargers) throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();

		for (ReservationPaxTO pax : paxList) {
			for (LCCClientExternalChgDTO extChg : pax.getExternalCharges()) {
				colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
			}

		}

		Set<EXTERNAL_CHARGES> tmpKeySet = selectedChargers.keySet();
		for (EXTERNAL_CHARGES key : tmpKeySet) {
			colEXTERNAL_CHARGES.add(key);
		}

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap = AirproxyModuleUtils.getReservationBD()
				.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MODIFY_ONLY);
		return extExternalChgDTOMap;
	}

	private Map<Integer, List<LCCClientExternalChgDTO>> getPaxExtChargesMap(Collection<ReservationPaxTO> paxList) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		for (ReservationPaxTO pax : paxList) {
			paxExtChgMap.put(pax.getSeqNumber(), pax.getExternalCharges());
		}
		return paxExtChgMap;
	}

	private void addExtChargesWithDummyPayments(ReservationBalanceTO reservationBalanceTO, LCCClientSegmentAssembler segmentAsm,
			boolean isGroupPNR) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = segmentAsm.getPassengerExtChargeMap();
		if (paxExtChgMap != null && paxExtChgMap.size() > 0) {
			for (LCCClientPassengerSummaryTO pax : reservationBalanceTO.getPassengerSummaryList()) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(pax.getTravelerRefNumber());
				if (paxExtChgMap.containsKey(paxSeq)) {
					LCCClientPaymentAssembler payAsm = new LCCClientPaymentAssembler();
					payAsm.getPerPaxExternalCharges().addAll(paxExtChgMap.get(paxSeq));
					if (isGroupPNR) {
						segmentAsm.addPassengerPayments(paxSeq, payAsm);
					} else {
						segmentAsm.addPassengerPayments(PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber()), payAsm);
					}
				}
			}
		}
	}

	private static boolean paxHasPaymentForModSeg(ReservationPaxTO pax, LCCClientPassengerSummaryTO summaryPax) {
		if (summaryPax != null) {
			BigDecimal amountDue = AccelAeroCalculator.add(summaryPax.getTotalCreditAmount().negate(),
					summaryPax.getTotalAmountDue(), pax.getAncillaryTotal(true));
			if (amountDue.compareTo(BigDecimal.ZERO) > 0) {
				return true;
			}
		}
		return false;
	}

	private static Map<Integer, LCCClientPaymentAssembler> getPaxPaymentMap(IBEReservationPostPaymentDTO postPayDTO,
			IBEReservationInfoDTO resInfo, IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO, Date paymentTimestamp,
			String loyaltyAgentCode, String loyaltyAccount, PayCurrencyDTO payCurrencyDTO, String lccUniqueKey, String brokerType,
			CommonCreditCardPaymentInfo cardPaymentInfo) throws ModuleException {

		Map<String, LCCClientPaymentAssembler> passengerRefNoPaymentMap = getPaxPaymentMapWithTravelRefNo(postPayDTO, resInfo,
				creditInfo, ipgDTO, paymentTimestamp, loyaltyAgentCode, loyaltyAccount, payCurrencyDTO, lccUniqueKey, brokerType,
				cardPaymentInfo);
		Map<Integer, LCCClientPaymentAssembler> passengerPaymentMap = new HashMap<Integer, LCCClientPaymentAssembler>();
		for (String travellerRefNo : passengerRefNoPaymentMap.keySet()) {
			if (resInfo.getSelectedSystem() == SYSTEM.INT) {
				passengerPaymentMap.put(PaxTypeUtils.getPaxSeq(travellerRefNo), passengerRefNoPaymentMap.get(travellerRefNo));
			} else {
				passengerPaymentMap.put(PaxTypeUtils.getPnrPaxId(travellerRefNo), passengerRefNoPaymentMap.get(travellerRefNo));
			}
		}
		return passengerPaymentMap;
	}

	private static Map<String, LCCClientPaymentAssembler> getPaxPaymentMapWithTravelRefNo(IBEReservationPostPaymentDTO postPayDTO,
			IBEReservationInfoDTO resInfo, IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO, Date paymentTimestamp,
			String loyaltyAgentCode, String loyaltyAccount, PayCurrencyDTO payCurrencyDTO, String lccUniqueKey, String brokerType,
			CommonCreditCardPaymentInfo cardPaymentInfo) throws ModuleException {

		Map<String, LCCClientPaymentAssembler> passengerPaymentMap = new HashMap<String, LCCClientPaymentAssembler>();
		Date currentDatetime = new Date();

		List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) postPayDTO
				.getlCCClientReservationBalance().getPassengerSummaryList();
		Collection<ReservationPaxTO> paxList = postPayDTO.getPaxList();

		LoyaltyPaymentOption loyaltyPayOption = postPayDTO.getLoyaltyPayOption();
		int adultChildCount = 0;
		int infantCount = 0;
		// Fail with Reservation pax credit
		/*
		 * for (ReservationPaxTO pax : paxList) { if (!PaxTypeTO.INFANT.equals(pax.getPaxType()) &&
		 * paxHasPaymentForModSeg(pax, colPassengerSummary)) { adultChildCount++; } }
		 */
		boolean isInfantPaymentSeparated = resInfo.isInfantPaymentSeparated();
		for (LCCClientPassengerSummaryTO lccClientPassengerSummaryTO : colPassengerSummary) {
			BigDecimal amountDue = lccClientPassengerSummaryTO.getTotalAmountDue();
			if (!PaxTypeTO.INFANT.equals(lccClientPassengerSummaryTO.getPaxType())
					&& amountDue.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				adultChildCount++;
			} else if (isInfantPaymentSeparated && PaxTypeTO.INFANT.equals(lccClientPassengerSummaryTO.getPaxType())
					&& amountDue.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				infantCount++;
			}
		}

		boolean isCCPayment = postPayDTO.isAddCCExternalCharge() && !postPayDTO.isNoPay();
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
				postPayDTO.getSelectedExternalCharges(), isCCPayment, false);
		externalChargesMediator.setCalculateJNTaxForCCCharge(true);

		LinkedList<ExternalChgDTO> infantCreditReservationCharges = new LinkedList<>();

		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(adultChildCount,
				infantCount);

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments = postPayDTO.getCarrierWiseLoyaltyPaymentInfo();
		Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
				.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);

		String loyaltyMemberAccountId = null;
		String[] rewardIDs = null;

		BigDecimal loyaltyPayAmount = postPayDTO.getLoyaltyCredit();
		boolean isExtCardPay = true;
		if (brokerType != null
				&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)
				&& cardPaymentInfo != null) {
			isExtCardPay = false;
		}

		PayByVoucherInfo payByVoucherInfo = postPayDTO.getPayByVoucherInfo();
		BigDecimal voucherPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (payByVoucherInfo != null) {
			voucherPaymentAmount = payByVoucherInfo.getRedeemedTotal();
		}

		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {

			PaymentAssemblerComposer paymentComposer = null;
			List<LCCClientExternalChgDTO> extCharges = new ArrayList<LCCClientExternalChgDTO>();

			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT) || isInfantPaymentSeparated) {
				BigDecimal amountDue = AccelAeroCalculator.add(paxSummaryTo.getTotalCreditAmount().negate(),
						paxSummaryTo.getTotalAmountDue());
				// Need to fix for Interline - Incorrect traverler Reference
				// number
				// FIXME this should be cleaned up
				for (ReservationPaxTO pax : paxList) {
					String travelerRef = pax.getTravelerRefNumber().split(",")[0].trim();
					if (paxSummaryTo.getTravelerRefNumber().contains(travelerRef)) {
						BigDecimal paxAmountDue = paxSummaryTo.getTotalAmountDue();

						if (paxAmountDue.compareTo(BigDecimal.ZERO) > 0 && perPaxExternalCharges.size() > 0) {
							extCharges = BookingUtil
									.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
						}
						extCharges.addAll(pax.getExternalCharges());
						break;
					}
				}

				paymentComposer = new PaymentAssemblerComposer(extCharges);
				if (!postPayDTO.isNoPay()) {

					Integer paxSequence = PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber());
					BigDecimal lmsMemberPayment = reconcileLmsPayAmount(paxWiseRedeemedAmounts.get(paxSequence), amountDue);

					BigDecimal totalWithAncillary = amountDue;
					BigDecimal totalExtChgs = BookingUtil.getTotalExtCharge(extCharges);
					if (!postPayDTO.isRequoteFlightSearch()) {
						totalWithAncillary = AccelAeroCalculator.add(totalWithAncillary,
								BookingUtil.getTotalExtCharge(extCharges));
					} else {
						// deduct external charge amount from amount due
						List<EXTERNAL_CHARGES> skipCharges = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
						skipCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
						skipCharges.add(EXTERNAL_CHARGES.JN_OTHER);
						amountDue = AccelAeroCalculator.subtract(amountDue,
								BookingUtil.getTotalExtCharge(extCharges, skipCharges));
						totalWithAncillary = AccelAeroCalculator.add(amountDue, totalExtChgs);
					}
					PaxPaymentUtil paxPayUtil = new PaxPaymentUtil(totalWithAncillary, totalExtChgs, loyaltyPayAmount,
							lmsMemberPayment, voucherPaymentAmount, AccelAeroCalculator.getDefaultBigDecimalZero());

					loyaltyPayAmount = paxPayUtil.getRemainingLoyalty();
					voucherPaymentAmount = paxPayUtil.getRemainingVoucherAmount();
					while (paxPayUtil.hasConsumablePayments()) {
						PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
						BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
						if (payMethod == PaymentMethod.LMS) {
							for (String operatingCarrierCode : carrierWiseLoyaltyPayments.keySet()) {
								LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = carrierWiseLoyaltyPayments
										.get(operatingCarrierCode);
								Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
										.getPaxProductPaymentBreakdown();
								BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil
										.getPaxRedeemedTotal(carrierPaxProductRedeemed, paxSequence);

								loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
								rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
								if (payCurrencyDTO != null) {
									payCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
								}
								if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
									paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs,
											carrierPaxProductRedeemed, carrierLmsMemberPayment, payCurrencyDTO, currentDatetime,
											operatingCarrierCode);
								}
							}
						} else if (payMethod == PaymentMethod.MASHREQ_LOYALTY) {
							paymentComposer.addLoyaltyCreditPayment(loyaltyAgentCode, paxPayAmt, loyaltyAccount, payCurrencyDTO,
									currentDatetime);
						} else if (payMethod == PaymentMethod.VOUCHER) {
							paymentComposer.addVoucherPayment(payByVoucherInfo, paxPayAmt, payCurrencyDTO, currentDatetime);
						} else {
							if (isExtCardPay) {
								paymentComposer.addCreditCardPayment(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
										paymentTimestamp, lccUniqueKey);
							} else {
								paymentComposer.addCreditCardPaymentInternal(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
										paymentTimestamp, cardPaymentInfo.getNo(), cardPaymentInfo.geteDate(),
										cardPaymentInfo.getSecurityCode(), cardPaymentInfo.getName(), lccUniqueKey);
							}
						}

					}

				}

				LCCClientPaymentAssembler payment = paymentComposer.getPaymentAssembler();

				if (PaxTypeTO.ADULT.equals(paxSummaryTo.getPaxType()) && paxSummaryTo.getInfantName() != null
						&& !paxSummaryTo.getInfantName().equals("")) {
					payment.setPaxType(PaxTypeTO.PARENT);
				} else {
					payment.setPaxType(paxSummaryTo.getPaxType());
				}

				passengerPaymentMap.put(paxSummaryTo.getTravelerRefNumber(), payment);
			}

		}

		return passengerPaymentMap;
	}

	private boolean paxHasPayment(ReservationPaxTO pax, Map<String, BigDecimal> paxCreditMap) {
		BigDecimal paxCredit = null;
		if (paxCreditMap != null) {
			paxCredit = paxCreditMap.get(pax.getTravelerRefNumber());
		}
		if (paxCredit == null)
			paxCredit = BigDecimal.ZERO;

		BigDecimal totalPaxPayment = AccelAeroCalculator.add(pax.getAncillaryTotal(true), paxCredit,
				pax.getToRemoveAncillaryTotal().negate());
		if (totalPaxPayment.compareTo(BigDecimal.ZERO) <= 0) {
			return false;
		} else {
			return true;
		}
	}

	private void addModifyAncillary(String pnr, IBEReservationPostPaymentDTO postPayDTO, IBEReservationInfoDTO resInfo,
			ContactInfoDTO contactInfo, Map<Integer, CommonCreditCardPaymentInfo> tempPayMap, IPGResponseDTO creditInfo,
			IPGIdentificationParamsDTO ipgDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String loyaltyAccount,
			String loyaltyAgentCode, String brokerType) throws Exception {

		Collection<ReservationPaxTO> paxList = postPayDTO.getPaxList();
		Map<String, BigDecimal> paxCreditMap = postPayDTO.getPaxCreditMap();
		BigDecimal paymentAdjustment;

		int paxWithPaymentCount = 0;
		for (ReservationPaxTO pax : paxList) {
			if ((PaxTypeTO.ADULT.equals(pax.getPaxType()) || PaxTypeTO.CHILD.equals(pax.getPaxType())
					|| PaxTypeTO.PARENT.equals(pax.getPaxType())) && paxHasPayment(pax, paxCreditMap)) {
				BigDecimal paxCredit = null;
				if (paxCreditMap != null) {
					paxCredit = paxCreditMap.get(pax.getTravelerRefNumber());
				}
				if (paxCredit == null)
					paxCredit = BigDecimal.ZERO;
				BigDecimal addAnciTotal = AccelAeroCalculator.add(BookingUtil.getTotalExtCharge(pax.getExternalCharges()),
						paxCredit);
				BigDecimal remAnciTotal = pax.getToRemoveAncillaryTotal();

				if (AccelAeroCalculator.isGreaterThan(addAnciTotal, remAnciTotal)) {
					paxWithPaymentCount++;
				}
			}
		}
		boolean isCCPayment = postPayDTO.isAddCCExternalCharge() && !postPayDTO.isNoPay();
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
				postPayDTO.getSelectedExternalCharges(), isCCPayment, false);
		externalChargesMediator.setCalculateJNTaxForCCCharge(true);
		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(paxWithPaymentCount, 0);

		BigDecimal loyaltyPayAmount = postPayDTO.getLoyaltyCredit();

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments = postPayDTO.getCarrierWiseLoyaltyPaymentInfo();
		Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
				.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);

		String loyaltyMemberAccountId = null;
		String[] rewardIDs = null;

		// tracking info
		TrackInfoDTO trackingInfo = getTrackInfo();

		Date currentDatetime = new Date();

		CommonAncillaryModifyAssembler anciAssembler = new CommonAncillaryModifyAssembler();
		anciAssembler.setPnr(pnr);
		anciAssembler.setVersion(postPayDTO.getVersion());
		anciAssembler.setTargetSystem(resInfo.getSelectedSystem());
		anciAssembler.setTransactionIdentifier(postPayDTO.getTransactionId());
		anciAssembler.setTemporyPaymentMap(postPayDTO.getTemporyPaymentMap());
		anciAssembler.setReservationStatus(resInfo.getReservationStatus());
		anciAssembler.setLccSegments(postPayDTO.getExistingAllLccSegments());
		if (resInfo.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI)) {
			anciAssembler.setServiceTaxRatio(resInfo.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));
		}
		anciAssembler.setApplyPenaltyForAnciModification(resInfo.isApplyPenaltyForAnciModification());
		anciAssembler.setLoyaltyPaymentInfo(postPayDTO.getCarrierLoyaltyPaymentInfo(AppSysParamsUtil.getDefaultCarrierCode()));

		Collection<LCCClientReservationPax> lccpassengers = new ArrayList<LCCClientReservationPax>();
		if (postPayDTO.getPaxList() != null) {
			for (ReservationPaxTO paxTO : postPayDTO.getPaxList()) {
				LCCClientReservationPax lccPax = new LCCClientReservationPax();
				lccPax.setPaxSequence(paxTO.getSeqNumber());
				lccpassengers.add(lccPax);
			}
		}
		anciAssembler.setLccPassengers(lccpassengers);
		CommonCreditCardPaymentInfo cardPaymentInfo = BookingUtil.getCommonCreditCardPaymentInfo(tempPayMap,
				creditInfo == null ? false : creditInfo.isSuccess());
		boolean isExtCardPay = true;
		if (brokerType != null
				&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)
				&& cardPaymentInfo != null) {
			isExtCardPay = false;
		}

		PayByVoucherInfo payByVoucherInfo = postPayDTO.getPayByVoucherInfo();
		BigDecimal voucherPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (payByVoucherInfo != null) {
			voucherPaymentAmount = payByVoucherInfo.getRedeemedTotal();
		}

		for (ReservationPaxTO reservationPax : paxList) {
			Integer paxSequence = reservationPax.getSeqNumber();
			if (reservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
				continue;
			}
			List<LCCClientExternalChgDTO> extCharges = new ArrayList<LCCClientExternalChgDTO>();
			if (paxHasPayment(reservationPax, paxCreditMap)) {
				extCharges = BookingUtil.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
			}
			extCharges.addAll(reservationPax.getExternalCharges());
			PaymentAssemblerComposer paymentComposer = new PaymentAssemblerComposer(extCharges);
			if (!postPayDTO.isNoPay()) {
				BigDecimal totalPaymentAmount = BigDecimal.ZERO;
				paymentAdjustment = BigDecimal.ZERO;

				BigDecimal totalWithAncillary = AccelAeroCalculator.add(totalPaymentAmount,
						BookingUtil.getTotalExtCharge(extCharges), reservationPax.getToRemoveAncillaryTotal().negate());
				paymentAdjustment = paymentAdjustment.add(reservationPax.getToRemoveAncillaryTotal());

				if (paxCreditMap != null && paxCreditMap.get(reservationPax.getTravelerRefNumber()) != null) {
					BigDecimal paxCredit = paxCreditMap.get(reservationPax.getTravelerRefNumber());
					if (paxCredit.negate().compareTo(BigDecimal.ZERO) > 0) {
						if (paxCredit.negate().compareTo(totalWithAncillary) < 0) {
							totalWithAncillary = AccelAeroCalculator.add(totalWithAncillary, paxCredit);
							totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, paxCredit);
						} else {
							totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, totalWithAncillary.negate());
							totalWithAncillary = BigDecimal.ZERO;
						}
					} else if (paxCredit.compareTo(BigDecimal.ZERO) > 0) {
						totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, paxCredit);
						totalWithAncillary = AccelAeroCalculator.add(totalWithAncillary, paxCredit);
					}
				}

				BigDecimal lmsMemberPayment = reconcileLmsPayAmount(paxWiseRedeemedAmounts.get(paxSequence), totalWithAncillary);

				if (totalWithAncillary.compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal totalExtChgs = BookingUtil.getTotalExtCharge(extCharges);
					PaxPaymentUtil paxPayUtil = new PaxPaymentUtil(totalWithAncillary, totalExtChgs, loyaltyPayAmount,
							lmsMemberPayment, voucherPaymentAmount, AccelAeroCalculator.getDefaultBigDecimalZero());

					loyaltyPayAmount = paxPayUtil.getRemainingLoyalty();
					while (paxPayUtil.hasConsumablePayments()) {
						PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
						BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
						if (payMethod == PaymentMethod.LMS) {
							for (String operatingCarrierCode : carrierWiseLoyaltyPayments.keySet()) {
								LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = carrierWiseLoyaltyPayments
										.get(operatingCarrierCode);
								Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
										.getPaxProductPaymentBreakdown();
								BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil
										.getPaxRedeemedTotal(carrierPaxProductRedeemed, paxSequence);

								loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
								rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
								if (payCurrencyDTO != null) {
									payCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
								}
								if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
									paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs,
											carrierPaxProductRedeemed, carrierLmsMemberPayment, payCurrencyDTO, currentDatetime,
											operatingCarrierCode);
								}
							}

						} else if (payMethod == PaymentMethod.MASHREQ_LOYALTY) {
							paymentComposer.addLoyaltyCreditPayment(loyaltyAgentCode, paxPayAmt, loyaltyAccount, payCurrencyDTO,
									currentDatetime);
						} else if (payMethod == PaymentMethod.VOUCHER) {
							paymentComposer.addVoucherPayment(payByVoucherInfo, paxPayAmt, payCurrencyDTO, currentDatetime);
						} else {
							if (isExtCardPay) {
								paymentComposer.addCreditCardPayment(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
										paymentTimestamp, null);
							} else {
								paymentComposer.addCreditCardPaymentInternal(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
										paymentTimestamp, cardPaymentInfo.getNo(), cardPaymentInfo.geteDate(),
										cardPaymentInfo.getSecurityCode(), cardPaymentInfo.getName(), null);
							}
						}

					}
				}
			}

			LCCClientPaymentAssembler payment = paymentComposer.getPaymentAssembler();

			payment.setPaxType(com.isa.thinair.webplatform.api.util.CommonUtil.getActualPaxType(reservationPax));

			anciAssembler.addPassengerPayment(reservationPax.getSeqNumber(), payment);

			// there can be situations where no payments exits but ancillary is
			// added (e.g. no charge ssr)
			List<LCCSelectedSegmentAncillaryDTO> selectedAnci = reservationPax.getSelectedAncillaries();
			anciAssembler.addAncillary(reservationPax.getSeqNumber(), selectedAnci);
			anciAssembler.removeAncillary(reservationPax.getSeqNumber(), reservationPax.getAncillariesToRemove());
			anciAssembler.updateAncillary(reservationPax.getSeqNumber(), reservationPax.getAncillariesToUpdate());

		}
		if (log.isDebugEnabled()) {
			for (LCCClientPaymentAssembler pay : anciAssembler.getPassengerPaymentMap().values()) {
				String debug = "\n********************MODIFY ANCI PAYMENT DOUBLE ERRROR DEBUG *************************\n: ";
				debug += "PNR : " + anciAssembler.getPnr() + "\n";
				for (LCCClientPaymentInfo paymt : pay.getPayments()) {
					debug += "[pay = " + paymt.getPayCurrencyAmount() + " ][base = " + paymt.getTotalAmount() + "]\n";
				}
				debug += "************************************************************\n";
				log.debug(debug);
			}
		}

		Map<String, Set<String>> fltRefWiseSelectedMeal = AncillaryDTOUtil
				.getFlightReferenceWiseSelectedMeals((List<ReservationPaxTO>) paxList);

		ModuleServiceLocator.getAirproxyReservationBD().updateAncillary(anciAssembler,
				AppSysParamsUtil.isFraudCheckEnabled(getTrackInfo().getOriginChannelId()),
				composeCommonReservationContactInfo(contactInfo),
				isInterlinePaymentAllowed(anciAssembler.getPaxAddAncillaryMap().values()), fltRefWiseSelectedMeal, trackingInfo);
	}

	private void revertLoyaltyRedemption() {
		String loyaltyMemberId = SessionUtil.getLoyaltyFFID(request);
		try {
			CustomerUtil.revertLoyaltyRedemption(request);
		} catch (ModuleException e) {
			log.error("Error in reverting redeeemed loyalty points for member " + loyaltyMemberId, e);
		}
	}

	private static int setAttempts(HttpServletRequest request) {
		Integer attempts = SessionUtil.getNumberOfAttempts(request);

		if (attempts == null) {
			attempts = new Integer(0);
		}

		attempts++;
		SessionUtil.setNumberOfAttempts(request, attempts);

		return attempts;
	}

	private static ModuleException getStandardPaymentBrokerError(String paymentErrorCode) {

		ModuleException me = new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR);
		Map<String, String> errorMap = new HashMap<String, String>();

		errorMap.put(paymentErrorCode, "External payment broker error:" + paymentErrorCode);
		me.setExceptionDetails(errorMap);

		return me;
	}

	private static String getServerErrorMessage(HttpServletRequest request, String key) {
		return ReservationUtil.getServerErrorMessage(request, key);
	}

	private void setOnHoldData(HttpServletRequest request, String pnr, OnHold app, Date depDate, Date pnrReleaseTimeZulu)
			throws ModuleException, JSONException {

		String onHDReleaseDisplay = ReservationUtil.getOnHoldDisplayTime(app, depDate, null, pnrReleaseTimeZulu);

		OnHoldDTO onHoldDTO = new OnHoldDTO();
		onHoldDTO.setPnr(pnr);
		onHoldDTO.setReleaseTime(CommonUtil.wrapWithDDHHMM(onHDReleaseDisplay));
		request.setAttribute(SessionAttribute.KSK_CONF_DATA, JSONUtil.serialize(onHoldDTO));
		request.setAttribute(WebConstants.SESSION_TIMEOUT,
				CommonUtil.convertToJSON(ReservationUtil.fetchSessionTimeoutData(request)));

	}

	/**
	 * Get Broker type - internal payment gateway or external payment gateway
	 * 
	 * @param ipgDTO
	 * @return
	 * @throws ModuleException
	 */
	private String getPaymentBrokerType(IPGIdentificationParamsDTO ipgDTO) throws ModuleException {
		String brokerType = null;
		if (ipgDTO != null && ipgDTO.getIpgId() != null) {
			Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgDTO);
			brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
		}
		return brokerType;
	}

	private boolean isInterlinePaymentAllowed(Collection<List<LCCSelectedSegmentAncillaryDTO>> selectedAnci) {
		boolean paymentAllowed = false;
		for (List<LCCSelectedSegmentAncillaryDTO> anciList : selectedAnci)
			for (LCCSelectedSegmentAncillaryDTO ancillaryDTO : anciList) {
				if (ancillaryDTO.getAirSeatDTO() != null) {
					paymentAllowed = true;
				} else if (ancillaryDTO.getInsuranceQuotations() != null && !ancillaryDTO.getInsuranceQuotations().isEmpty()) {
					paymentAllowed = true;
				} else if (ancillaryDTO.getMealDTOs() != null && ancillaryDTO.getMealDTOs().size() > 0) {
					paymentAllowed = true;
				} else if (ancillaryDTO.getBaggageDTOs() != null && ancillaryDTO.getBaggageDTOs().size() > 0) {
					paymentAllowed = true;
				} else if (ancillaryDTO.getAirportServiceDTOs() != null && ancillaryDTO.getAirportServiceDTOs().size() > 0) {
					paymentAllowed = true;
				} else if (ancillaryDTO.getAirportTransferDTOs() != null && ancillaryDTO.getAirportTransferDTOs().size() > 0) {
					paymentAllowed = true;
				}
			}
		return paymentAllowed;
	}

	private CommonReservationContactInfo composeCommonReservationContactInfo(ContactInfoDTO contactInfo) {
		CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();
		lccClientReservationContactInfo.setTitle(contactInfo.getTitle());
		lccClientReservationContactInfo.setFirstName(contactInfo.getFirstName());
		lccClientReservationContactInfo.setLastName(contactInfo.getLastName());
		lccClientReservationContactInfo.setStreetAddress1(contactInfo.getAddresline());
		lccClientReservationContactInfo.setStreetAddress2(contactInfo.getAddresStreet());
		lccClientReservationContactInfo.setCity(contactInfo.getCity());
		lccClientReservationContactInfo.setCountryCode(contactInfo.getCountry());
		lccClientReservationContactInfo.setZipCode(contactInfo.getZipCode());
		lccClientReservationContactInfo
				.setMobileNo(contactInfo.getmCountry() + "-" + contactInfo.getmArea() + "-" + contactInfo.getmNumber());
		lccClientReservationContactInfo
				.setPhoneNo(contactInfo.getlCountry() + "-" + contactInfo.getlArea() + "-" + contactInfo.getlNumber());
		lccClientReservationContactInfo
				.setFax(contactInfo.getfCountry() + "-" + contactInfo.getfArea() + "-" + contactInfo.getfNumber());
		lccClientReservationContactInfo.setEmail(contactInfo.getEmailAddress());
		// Fix Me, When loading XBE booking for IBE Modification
		if (contactInfo.getNationality() != null && !contactInfo.getNationality().isEmpty()) {
			lccClientReservationContactInfo.setNationalityCode(new Integer(contactInfo.getNationality()));
		}
		lccClientReservationContactInfo.setState(contactInfo.getState());
		lccClientReservationContactInfo.setCustomerId(contactInfo.getCustomerId());
		lccClientReservationContactInfo.setPreferredLanguage(contactInfo.getPreferredLangauge());

		// set emergency contact info details
		lccClientReservationContactInfo.setEmgnTitle(contactInfo.getEmgnTitle());
		lccClientReservationContactInfo.setEmgnFirstName(contactInfo.getEmgnFirstName());
		lccClientReservationContactInfo.setEmgnLastName(contactInfo.getEmgnLastName());
		lccClientReservationContactInfo.setEmgnPhoneNo(
				contactInfo.getEmgnLCountry() + "-" + contactInfo.getEmgnLArea() + "-" + contactInfo.getEmgnLNumber());
		lccClientReservationContactInfo.setEmgnEmail(contactInfo.getEmgnEmail());

		return lccClientReservationContactInfo;
	}

	private int getAdultCount(IBEReservationPostPaymentDTO postPayDTO) {
		Collection<ReservationPaxTO> paxList = postPayDTO.getPaxList();
		int adultChildCount = 0;
		for (ReservationPaxTO pax : paxList) {
			if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				adultChildCount++;
			}
		}
		return adultChildCount;
	}

	private String timeDiffInStr(long diff) {

		StringBuffer str = new StringBuffer();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffDays > 0) {
			str.append(diffDays + " days, ");
		}
		if (diffHours > 0) {
			str.append(diffHours + " hours, ");
		}
		if (diffMinutes > 0) {
			str.append(diffMinutes + " minutes, ");
		}
		if (diffSeconds > 0) {
			str.append(diffSeconds + " seconds.");
		}
		return str.toString();
	}

}