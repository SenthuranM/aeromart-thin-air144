package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.util.HashSet;

import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.ibe.api.dto.AgentDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

public class AgentUtilV2 {

	private static final String CURRENCY_CODE = "AED";

	private static final String SPECIFIED_IN = "B";

	private static final String AUTO_INVOCE = "N";

	private static final BigDecimal CREDIT_LIMIT = AccelAeroCalculator.getDefaultBigDecimalZero();

	private static final BigDecimal BANK_GUARANTY = AccelAeroCalculator.getDefaultBigDecimalZero();

	private static final String ON_HOLD_N = "N";

	private static final String STATUS_NEW = "NEW";

	private static final String AGENT_TYPE_TA = "TA";

	private static final String AGENT_TYPE_BSP = "TA";

	private static final String AGENT_TYPE_CO = "CO";

	private static final String PREF_RPT_FORMAT = "US";

	private static final String PAY_REF_MANDATORY = "N";

	private static final String WEBSITE_VISIBILITY = "N";

	private static final String CAPTURE_PAYMENT_REF = "N";

	private static final String HANDLING_CHG_ENABLED = "Y"; // Default value defined in the database
	private static final String HANDLING_CHG_BASIS_CODE = "V"; // Default value defined in the database
	private static final int HANDLING_CHG_APPLIES_TO = 6; // Default value defined in the database
	private static final String AGENT_WISE_TKT_ENABLE = "N"; // Default value defined in the database
	private static final String CHARTER_AGENT = "N"; // Default value defined in the database

	public static Agent getModelAgent(AgentDTO agentDTO, String agentType) throws ModuleException {
		Agent agent = new Agent();
		agent.setAgentName(StringUtil.nullConvertToString(agentDTO.getAgencyName()));
		agent.setStationCode(StringUtil.nullConvertToString(agentDTO.getStation()));
		agent.setLicenceNumber(StringUtil.nullConvertToString(agentDTO.getLicenseNo()));
		agent.setAgentIATANumber(StringUtil.nullConvertToString(agentDTO.getiATACode()));

		HashSet<String> set = new HashSet<String>();
		if (agentType != AGENT_TYPE_CO) {
			if (agentDTO.getPayMeth() != null && agentDTO.getPayMeth().equals("BS")) {
				set.add("OA"); // TODO Verify
				agent.setAgentTypeCode(AGENT_TYPE_BSP);
			}
			if (agentDTO.getPayMeth() != null && agentDTO.getPayMeth().equals("AC")) {
				set.add("OA");
				agent.setAgentTypeCode(AGENT_TYPE_TA);
			}
			if (agentDTO.getChkPayMeth() != null && agentDTO.getChkPayMeth().equals("CC")) {
				set.add("CC");
			}
		} else {
			if (agentDTO.getChkCoPayMeth() != null && agentDTO.getChkCoPayMeth().equals("AC")) {
				set.add("OA");
			}
			if (agentDTO.getChkPayMeth() != null && agentDTO.getChkPayMeth().equals("CC")) {
				set.add("CC");
			}
			agent.setAgentTypeCode(AGENT_TYPE_CO);
		}

		agent.setPaymentMethod(set);
		agent.setAddressLine1(StringUtil.nullConvertToString(agentDTO.getAddress()));
		agent.setAddressCity(StringUtil.nullConvertToString(agentDTO.getCity()));
		agent.setAddressStateProvince(StringUtil.nullConvertToString(agentDTO.getState()));
		agent.setPostalCode(StringUtil.nullConvertToString(agentDTO.getPostalCode()));
		agent.setTelephone(StringUtil.nullConvertToString(agentDTO.getTelephone()));
		agent.setEmailId(StringUtil.nullConvertToString(agentDTO.getEmail()));
		agent.setFax(StringUtil.nullConvertToString(agentDTO.getFax()));

		// If agent code is not set when passed over set the default value by getting it from app parameters.
		if (agent.getAirlineCode() == null) {
			agent.setAirlineCode(AppSysParamsUtil.getDefaultAirlineIdentifierCode());
		}

		if (agent.getHandlingFeeEnable() == null) {
			agent.setHandlingFeeEnable(HANDLING_CHG_ENABLED);
		}
		if (agent.getHandlingFeeChargeBasisCode() == null) {
			agent.setHandlingFeeChargeBasisCode(HANDLING_CHG_BASIS_CODE);
		}
		if (agent.getHandlingFeeAppliesTo() == 0) {
			agent.setHandlingFeeAppliesTo(HANDLING_CHG_APPLIES_TO);
		}
		if (agent.getAgentWiseTicketEnable() == null) {
			agent.setAgentWiseTicketEnable(AGENT_WISE_TKT_ENABLE);
		}
		if (agent.getCharterAgent() == null) {
			agent.setCharterAgent(CHARTER_AGENT);
		}

		if (agentType != AGENT_TYPE_CO) {
			agent.setNotes(StringUtil.nullConvertToString(agentDTO.getComments()));
		} else {
			StringBuilder note = new StringBuilder("");
			if (agentDTO.getComments() != null && !agentDTO.getComments().equals("")) {
				note.append(agentDTO.getComments()).append(", ");
			}
			if (agentDTO.getCompanySize() != null && !agentDTO.getCompanySize().equals("")) {
				note.append("Company Size : ").append(agentDTO.getCompanySize()).append(", ");
			}
			if (agentDTO.getConvenientTime() != null && !agentDTO.getConvenientTime().equals("")) {
				note.append("Convenient Time : ").append(agentDTO.getConvenientTime());
			}
			agent.setNotes(note.toString());
			agent.setMobile(StringUtil.nullConvertToString(agentDTO.getMobile()));
			agent.setDesignation(StringUtil.nullConvertToString(agentDTO.getContactDesignation()));
			agent.setContactPersonName(StringUtil.nullConvertToString(agentDTO.getCompanyContactName()));
		}

		if (agentDTO.getCountry() != null && agentDTO.getCountry() != "") {
			Country country = ModuleServiceLocator.getCommoMasterBD().getCountryByName(agentDTO.getCountry());
			agent.setCurrencyCode(country.getCurrencyCode());
		} else {
			agent.setCurrencyCode(CURRENCY_CODE);
		}
		agent.setCurrencySpecifiedIn(SPECIFIED_IN);
		agent.setAutoInvoice(AUTO_INVOCE);
		agent.setCreditLimit(CREDIT_LIMIT);
		agent.setBankGuaranteeCashAdvance(BANK_GUARANTY);
		agent.setOnHold(ON_HOLD_N);
		agent.setStatus(STATUS_NEW);
		agent.setReportingToGSA(Agent.REPORTING_TO_GSA_NO);
		agent.setPrefferedRptFormat(PREF_RPT_FORMAT);
		agent.setPayRefMandatory(PAY_REF_MANDATORY);
		agent.setPublishToWeb(WEBSITE_VISIBILITY);
		agent.setCapturePaymentRef(CAPTURE_PAYMENT_REF);
		agent.setAgentWiseFareMaskEnable(Agent.CHARTER_AGENT_NO);
		agent.setLccPublishStatus(Util.LCC_TOBE_PUBLISHED);
		return agent;
	}

}
