package com.isa.thinair.ibe.core.web.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.BookingPaxType;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.assembler.OndConvertAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.api.dto.SegmentFareTO;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxFareTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxPriceTO;
import com.isa.thinair.webplatform.api.v2.reservation.SurchargeFETO;
import com.isa.thinair.webplatform.api.v2.reservation.TaxFETO;
import com.isa.thinair.webplatform.api.v2.util.BigDecimalWrapper;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.FareTypeCodes;

public class BeanUtil {

	private static Log log = LogFactory.getLog(BeanUtil.class);

	public static FlightAvailRQ createFlightAvailRQ(FlightSearchDTO flightSearch, boolean addStayOverMillis)
			throws ParseException, ModuleException {

		FlightAvailRQ flightAvailRQ = new FlightAvailRQ();

		// setting origin destination information
		OriginDestinationInformationTO depatureOnD = flightAvailRQ.addNewOriginDestinationInformation();

		depatureOnD.setOrigin(flightSearch.getFromAirport());
		depatureOnD.setDestination(flightSearch.getToAirport());

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date depatureDate = null;
		try {
			depatureDate = format.parse(flightSearch.getDepartureDate());
		} catch (Exception ex) {
			throw new ModuleException("module.invalid.departuredate");
		}

		ReservationBeanUtil.validateDateVarience(depatureDate, flightSearch.getDepartureVariance(), AppIndicatorEnum.APP_IBE);

		Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
		depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, flightSearch.getDepartureVariance() * -1);
		Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
		depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, flightSearch.getDepartureVariance());

		/**
		 * Additional logs to identify the date format issue. When issue is resolved, need to remove the logs
		 */
		log.info("Caller ClassName : " + Thread.currentThread().getStackTrace()[2].getFileName() + " Tread Name : "
				+ Thread.currentThread().getName() + " depatureDateTimeStart : " + depatureDateTimeStart
				+ " depatureDateTimeEnd " + depatureDateTimeEnd);

		depatureOnD.setPreferredDate(depatureDate);
		depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
		depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
		depatureOnD.setReturnFlag(false);
		depatureOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.OUT_BOUND));
		depatureOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.OUT_BOUND));
		depatureOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.OUT_BOUND));

		if (flightSearch.getHubTimeDetails() != null && !flightSearch.getHubTimeDetails().equals("")) {
			Map<String, Integer> hubDetailMap = new HashMap<String, Integer>();
			String hubTimeDetails[] = flightSearch.getHubTimeDetails().split("_");
			hubDetailMap.put(hubTimeDetails[0].trim(), Integer.parseInt(hubTimeDetails[1].trim()));
			depatureOnD.setHubTimeDetailMap(hubDetailMap);
		}

		if (!StringUtil.isNullOrEmpty(flightSearch.getReturnDate())) {

			OriginDestinationInformationTO returnOnD = flightAvailRQ.addNewOriginDestinationInformation();
			returnOnD.setOrigin(flightSearch.getToAirport());
			returnOnD.setDestination(flightSearch.getFromAirport());
			Date returnDate = null;
			try {
				returnDate = format.parse(flightSearch.getReturnDate());
			} catch (Exception ex) {
				throw new ModuleException("module.invalid.returndate");
			}

			ReservationBeanUtil.validateDateVarience(returnDate, flightSearch.getReturnVariance(), AppIndicatorEnum.APP_IBE);

			Date returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
			returnDateTimeStart = CalendarUtil.getOfssetAddedTime(returnDateTimeStart, flightSearch.getReturnVariance() * -1);
			Date returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
			returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(returnDateTimeEnd, flightSearch.getReturnVariance());

			returnOnD.setPreferredDate(returnDate);
			returnOnD.setDepartureDateTimeStart(returnDateTimeStart);
			returnOnD.setDepartureDateTimeEnd(returnDateTimeEnd);
			returnOnD.setReturnFlag(true);
			returnOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.IN_BOUND));
			returnOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.IN_BOUND));
			returnOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.IN_BOUND));

			if (flightSearch.getHubTimeDetails() != null && !flightSearch.getHubTimeDetails().equals("")) {
				Map<String, Integer> returnHubDetailMap = new HashMap<String, Integer>();
				String hubTimeDetails[] = flightSearch.getHubTimeDetails().split("_");
				if (hubTimeDetails.length == 3) {
					returnHubDetailMap.put(hubTimeDetails[0].trim(), Integer.parseInt(hubTimeDetails[2].trim()));
					returnOnD.setHubTimeDetailMap(returnHubDetailMap);
				}
			}

			if (addStayOverMillis) {
				if (flightSearch.getValidity() == null) {
					boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
					flightSearch.setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
				}

				if (flightSearch.isHalfReturnFareQuote() && flightSearch.getReturnVariance() != null
						|| flightSearch.getDepartureVariance() != null) { // searched
																			// with
																			// variance
					long stayOverTimeBetweenRequestedDates = CalendarUtil.getEndTimeOfDate(returnDate).getTime()
							- CalendarUtil.getStartTimeOfDate(depatureDate).getTime();
					flightSearch.setStayOverTimeInMillis(stayOverTimeBetweenRequestedDates);
				}
			}
		}

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightAvailRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		if (flightSearch.getAdultCount() == null) {
			adultsQuantity.setQuantity(0);
		} else {
			adultsQuantity.setQuantity(flightSearch.getAdultCount());
		}

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		if (flightSearch.getChildCount() == null) {
			childQuantity.setQuantity(0);
		} else {
			childQuantity.setQuantity(flightSearch.getChildCount());
		}

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		if (flightSearch.getInfantCount() == null) {
			infantQuantity.setQuantity(0);
		} else {
			infantQuantity.setQuantity(flightSearch.getInfantCount());
		}

		// Check Valid Adult Count
		int maxAdultCount = AppSysParamsUtil.getMaxAdultCountIBE();
		int searchAdultCount = adultsQuantity.getQuantity();

		if (maxAdultCount < searchAdultCount) {
			if (addStayOverMillis) {
				throw new ModuleException("module.invalid.adultchild.count");
			}
		}

		if (addStayOverMillis) {
			flightAvailRQ.getAvailPreferences().setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());
			flightAvailRQ.getAvailPreferences().setStayOverTimeInMillis(flightSearch.getStayOverTimeInMillis());
		}

		SYSTEM system = SYSTEM.AA;
		if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
			system = SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.AA);
		} else {
			if (AppSysParamsUtil.isLCCConnectivityEnabled() && AppSysParamsUtil.showLCCResultsInIBE()) {
				system = SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.INT);
			} else {
				system = SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.AA);
			}
		}

		flightAvailRQ.getAvailPreferences().setSearchSystem(system);
		flightAvailRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.IBE);
		flightAvailRQ.getAvailPreferences().setFareCalendarSearch(true);

		if (AppSysParamsUtil.isIbeAddPaxTypeEnable() && flightSearch.getPaxType() != null && !"".equals(flightSearch.getPaxType())
				&& !flightSearch.getPaxType().equals(BookingPaxType.ANY)) {
			flightAvailRQ.getAvailPreferences().setBookingPaxType(flightSearch.getPaxType());
		}

		flightAvailRQ.getAvailPreferences().setQuoteOndFlexi(flightSearch.getOndQuoteFlexi());
		flightAvailRQ.getAvailPreferences().setPromoCode(flightSearch.getPromoCode());
		if (flightSearch.getBankIdentificationNo() != null && !"".equals(flightSearch.getBankIdentificationNo())) {
			flightAvailRQ.getAvailPreferences().setBankIdentificationNumber(
					Integer.parseInt(flightSearch.getBankIdentificationNo()));
		}
		HashMap<Double, List<Integer>> dateFareMap = new HashMap<Double, List<Integer>>();
		for (Entry<String, List<Integer>> entry : flightSearch.getFlightDateFareMap().entrySet()) {
			dateFareMap.put(new Double(entry.getKey()), entry.getValue());
		}
		flightAvailRQ.getAvailPreferences().setFlightDateFareMap(dateFareMap);
		flightAvailRQ.getAvailPreferences().setPointOfSale(flightSearch.getPointOfSale());

		return flightAvailRQ;
	}

	public static AvailableFlightInfo getAvailableFlightInfo(OriginDestinationOptionTO ondOpTO, boolean nextPrevious,
			CurrencyExchangeRate exchangeRate, boolean isInbound, Locale locale, boolean isOutboundSelected,
			boolean isInboundSelected) {
		AvailableFlightInfo availableFlightInfo = new AvailableFlightInfo();
		SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yy", locale);
		SimpleDateFormat smpdtDateValue = new SimpleDateFormat("yyMMddHHmm");
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		List<SegInfoDTO> segInfoDTOList = new ArrayList<SegInfoDTO>();
		boolean returnFlag = false;
		Date departureDateTemp = null;
		Date arrivalDateTemp = null;
		String departureTimeTemp = null;
		String arrivalTimeTemp = null;

		int seqNo = 1;
		int loopCount = 0;
		List<String> viaPoints = new ArrayList<String>();
		StringBuilder ondCodeBuilder = new StringBuilder();
		int segmentCount = ondOpTO.getFlightSegmentList().size();
		// Validate if more than selected flights available
		// Ideally It should be validate in back end
		boolean isSelectedFlight = false;
		for (FlightSegmentTO flightSegmentTO : ondOpTO.getFlightSegmentList()) {
			isSelectedFlight = false;

			SegInfoDTO segInfoDTO = new SegInfoDTO();
			if (segmentCount == 1) {
				segInfoDTO.setFlightSeqNo("");
				seqNo++;
			} else {
				segInfoDTO.setFlightSeqNo(seqNo++ + ". ");
			}
			segInfoDTO.setSegmentShortCode(flightSegmentTO.getSegmentCode());
			// segInfoDTO.setSegmentCode(ReservationHG.getSegementDetails(flightSegmentTO.getSegmentCode()));
			segInfoDTO.setSegmentCode(ReservationHG.getSegementDetailsWithTermainals(flightSegmentTO.getSegmentCode(), null,
					flightSegmentTO.getDepartureTerminalName(), flightSegmentTO.getArrivalTerminalName()));
			segInfoDTO.setFlightNumber(flightSegmentTO.getFlightNumber());
			segInfoDTO.setCarrierCode(flightSegmentTO.getOperatingAirline());
			segInfoDTO.setDepartureDate(formatter.format(flightSegmentTO.getDepartureDateTime()));
			segInfoDTO.setDepartureDateValue(smpdtDateValue.format(flightSegmentTO.getDepartureDateTime()));
			segInfoDTO.setDomesticFlight(flightSegmentTO.isDomesticFlight());
			// segInfoDTO.setDepartureDate(CommonUtil.getDateFormatLocale(flightSegmentTO.getDepartureDateTime(),
			// "it"));

			String departTime = timeFormatter.format(flightSegmentTO.getDepartureDateTime());
			String arrivalTime = timeFormatter.format(flightSegmentTO.getArrivalDateTime());

			segInfoDTO.setDepartureTime(departTime);
			segInfoDTO.setArrivalDate(formatter.format(flightSegmentTO.getArrivalDateTime()));
			segInfoDTO.setArrivalDateValue(smpdtDateValue.format(flightSegmentTO.getArrivalDateTime()));
			segInfoDTO.setArrivalTime(arrivalTime);
			segInfoDTO.setDepartureTimeLong(flightSegmentTO.getDepartureDateTime().getTime());
			segInfoDTO.setDepartureTimeZuluLong(flightSegmentTO.getDepartureDateTimeZulu().getTime());
			segInfoDTO.setArrivalTimeLong(flightSegmentTO.getArrivalDateTime().getTime());
			segInfoDTO.setArrivalTimeZuluLong(flightSegmentTO.getArrivalDateTimeZulu().getTime());
			segInfoDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber()
					+ (flightSegmentTO.getRouteRefNumber() != null ? "#" + flightSegmentTO.getRouteRefNumber() : ""));
			segInfoDTO.setSystem(ondOpTO.getSystem().toString());
			segInfoDTO.setDomesticFlight(flightSegmentTO.isDomesticFlight());
			if (!nextPrevious) {
				returnFlag = flightSegmentTO.isReturnFlag();
				segInfoDTO.setReturnFlag(flightSegmentTO.isReturnFlag());
			} else {
				returnFlag = isInbound;
				segInfoDTO.setReturnFlag(isInbound);
			}

			// required for validation cannot use the time stamp due to it consider client browser local timezone
			segInfoDTO.setDepZuluDateTimeJson(formatToJsonDateTime(flightSegmentTO.getDepartureDateTimeZulu()));
			segInfoDTO.setArrZuluDateTimeJson(formatToJsonDateTime(flightSegmentTO.getArrivalDateTimeZulu()));
			segInfoDTO.setDepLocalDateTimeJson(formatToJsonDateTime(flightSegmentTO.getDepartureDateTime()));
			segInfoDTO.setArrLocalDateTimeJson(formatToJsonDateTime(flightSegmentTO.getArrivalDateTime()));

			segInfoDTO.setArrivalTerminalName(flightSegmentTO.getArrivalTerminalName());
			segInfoDTO.setDepartureTerminalName(flightSegmentTO.getDepartureTerminalName());

			segInfoDTO.setDuration(CommonUtil.wrapWithHM(CommonUtil.getTimeHHMM(flightSegmentTO.getArrivalDateTimeZulu()
					.getTime() - flightSegmentTO.getDepartureDateTimeZulu().getTime())));
			
			segInfoDTO.setCsOcCarrierCode(flightSegmentTO.getCsOcCarrierCode());
			segInfoDTO.setFlightModelNumber(flightSegmentTO.getFlightModelNumber());
			segInfoDTO.setFlightStopOverDuration(flightSegmentTO.getFlightStopOverDuration());
			
			segInfoDTOList.add(segInfoDTO);

			if (departureDateTemp == null) {
				departureDateTemp = new Date(flightSegmentTO.getDepartureDateTime().getTime());
				departureTimeTemp = departTime;
			}

			if ((segmentCount - 1) == loopCount) {
				arrivalDateTemp = new Date(flightSegmentTO.getArrivalDateTime().getTime());
				arrivalTimeTemp = arrivalTime;
			}

			// Set via points
			if (AppSysParamsUtil.isShowMultilegStopOversInIBE()) {
				if (loopCount > 0) {
					ondCodeBuilder.append(flightSegmentTO.getSegmentCode().substring(3));
				} else {
					ondCodeBuilder.append(flightSegmentTO.getSegmentCode());
				}
			} else if (segmentCount > 1 && loopCount > 0) {
				viaPoints.add(flightSegmentTO.getSegmentCode().split("/")[0]);
			}

			loopCount++;
		}

		if (AppSysParamsUtil.isShowMultilegStopOversInIBE()) {
			viaPoints.addAll(Arrays.asList(ondCodeBuilder.toString().split("/")));
			viaPoints.remove(0);
			viaPoints.remove(viaPoints.size() - 1);
		}

		Collections.sort(segInfoDTOList);
		availableFlightInfo.setSegments(segInfoDTOList);

		if (returnFlag == false && isOutboundSelected == false) {
			isSelectedFlight = nextPrevious ? false : ondOpTO.isSelected();
		} else if (returnFlag == false && isOutboundSelected == true) {
			isSelectedFlight = false;
		} else if (returnFlag == true && isInboundSelected == false) {
			isSelectedFlight = nextPrevious ? false : ondOpTO.isSelected();
		} else if (returnFlag == true && isInboundSelected == true) {
			isSelectedFlight = false;
		} else {
			// Error all conditions check
			if (log.isInfoEnabled()) {
				log.info("###### ERROR : ==> Please verify the selected flight status");
			}
		}
		availableFlightInfo.setSelected(isSelectedFlight);
		availableFlightInfo.setReturnFlag(returnFlag);
		availableFlightInfo.setFlightFareSummaryList(ondOpTO.getFlightFareSummaryList());
		availableFlightInfo.setDepartureDate(departureDateTemp == null ? new Date() : departureDateTemp);
		availableFlightInfo.setArrivalDate(arrivalDateTemp == null ? new Date() : arrivalDateTemp);
		availableFlightInfo.setDepartureTime(departureTimeTemp);
		availableFlightInfo.setArrivalTime(arrivalTimeTemp);
		availableFlightInfo.setViaPoints(viaPoints);
		if (ondOpTO.getFlightSegmentList().size() == 2) {
			availableFlightInfo.setTransitDuration(CommonUtil.wrapWithHM(CommonUtil.getTimeHHMM(ondOpTO.getFlightSegmentList()
					.get(1).getDepartureDateTimeZulu().getTime()
					- ondOpTO.getFlightSegmentList().get(0).getArrivalDateTimeZulu().getTime())));
		}

		if (availableFlightInfo.getFlightFareSummaryList() != null) {
			for (FlightFareSummaryTO flightfareSummaryTO : availableFlightInfo.getFlightFareSummaryList()) {
				BigDecimal baseAmtToShow = BigDecimal.ZERO;
				if (AppSysParamsUtil.showTotalFareInCalendar()) {
					baseAmtToShow = flightfareSummaryTO.getTotalPrice();
				} else {
					baseAmtToShow = flightfareSummaryTO.getBaseFareAmount();
				}

				BigDecimal calAmt = BigDecimal.ZERO;
				Currency currency = exchangeRate.getCurrency();
				boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
				if (isCurrencyRoundUpEnabled) {

					BigDecimal boundry = (currency.getBoundryValue() != null) ? currency.getBoundryValue() : AppSysParamsUtil
							.getIBECalBoundry();
					BigDecimal breakpoint = (currency.getBreakPoint() != null) ? currency.getBreakPoint() : AppSysParamsUtil
							.getIBECalBreakpoint();

					calAmt = AccelAeroRounderPolicy.convertAndRound(baseAmtToShow, exchangeRate.getMultiplyingExchangeRate(),
							boundry, breakpoint);
				} else {
					calAmt = AccelAeroCalculator.multiply(baseAmtToShow, exchangeRate.getMultiplyingExchangeRate());
					if (AppSysParamsUtil.getIBECalBoundry() != null && AppSysParamsUtil.getIBECalBoundry().doubleValue() != 0
							&& AppSysParamsUtil.getIBECalBreakpoint() != null) {
						calAmt = AccelAeroRounderPolicy.getRoundedValue(calAmt, AppSysParamsUtil.getIBECalBoundry(),
								AppSysParamsUtil.getIBECalBreakpoint());
					}
				}
				flightfareSummaryTO.setCalDisplayAmout(calAmt);
			}
		}

		// required for re-quote flow
		availableFlightInfo.setSeatAvailable(ondOpTO.isSeatAvailable());

		// BigDecimal selCurBaseFare =
		// AccelAeroCalculator.multiply(availableFlightInfo.getFlightFareSummary().getBaseFareAmount(),
		// exchangeRate.getMultiplyingExchangeRate());
		// selCurBaseFare = new
		// BigDecimal(AccelAeroCalculator.formatAsDecimal(selCurBaseFare));
		// availableFlightInfo.getFlightFareSummary().setBaseFareAmtInSelCurrency(selCurBaseFare);
		// BigDecimal selCurTotalPrice =
		// AccelAeroCalculator.multiply(availableFlightInfo.getFlightFareSummary().getTotalPrice(),
		// exchangeRate.getMultiplyingExchangeRate());
		// selCurTotalPrice = new
		// BigDecimal(AccelAeroCalculator.formatAsDecimal(selCurTotalPrice));
		// availableFlightInfo.getFlightFareSummary().setTotalPriceInSelCurrency(selCurTotalPrice);
		// Total Duration
		/*
		 * if (segInfoDTOList.size() > 0) { long depFirstDateLong =
		 * ((SegInfoDTO)segInfoDTOList.get(0)).getDepartureTimeZuluLong(); long arrLastDateLong =
		 * ((SegInfoDTO)segInfoDTOList.get(segInfoDTOList.size() -1)).getArrivalTimeZuluLong();
		 * availableFlightInfo.setTotalDuration(CommonUtil .getTimeHHMM(arrLastDateLong - depFirstDateLong)); }
		 */

		return availableFlightInfo;
	}

	/**
	 * @param takeSegFareFlights
	 *            boolean denoting whether to create AvailFlightInfo list for segment fare details.
	 */
	public static Collection<AvailableFlightInfo> createAvailFlightInfoList(FlightPriceRS flightPriceRS, String origin,
			String destination, boolean isNextPrivious, Locale locale, boolean takeSegFareFlights) throws ModuleException {

		SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yy", locale);
		SimpleDateFormat smpdtDateValue = new SimpleDateFormat("yyMMddHHmm");
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		Collection<AvailableFlightInfo> flightInfoList = new ArrayList<AvailableFlightInfo>();
		for (OriginDestinationInformationTO ondInfoTO : flightPriceRS.getOriginDestinationInformationList()) {
			if (ondInfoTO.getOrigin().equals(origin) && ondInfoTO.getDestination().equals(destination)) {

				List<OriginDestinationOptionTO> ondOptionList = takeSegFareFlights ? ondInfoTO
						.getOriginDestinationSegFareOptions() : ondInfoTO.getOrignDestinationOptions();

				for (OriginDestinationOptionTO ondOpTO : ondOptionList) {
					AvailableFlightInfo availableFlightInfo = new AvailableFlightInfo();
					String originCode = origin;
					for (int i = 0; i < ondOpTO.getFlightSegmentList().size(); i++) {

						FlightSegmentTO flightSegmentTO = getOriginFlightSegment(ondOpTO.getFlightSegmentList(), originCode);
						SegInfoDTO segInfoDTO = new SegInfoDTO();
						segInfoDTO.setSegmentShortCode(flightSegmentTO.getSegmentCode());
						// segInfoDTO.setSegmentCode(ReservationHG.getSegementDetails(flightSegmentTO.getSegmentCode()));
						segInfoDTO.setSegmentCode(ReservationHG.getSegementDetailsWithTermainals(
								flightSegmentTO.getSegmentCode(), null, flightSegmentTO.getDepartureTerminalName(),
								flightSegmentTO.getArrivalTerminalName()));
						segInfoDTO.setFlightNumber(flightSegmentTO.getFlightNumber());
						segInfoDTO.setCarrierCode(flightSegmentTO.getOperatingAirline());
						segInfoDTO.setDepartureDate(formatter.format(flightSegmentTO.getDepartureDateTime()));
						segInfoDTO.setDepartureDateValue(smpdtDateValue.format(flightSegmentTO.getDepartureDateTime()));
						segInfoDTO.setDepartureTime(timeFormatter.format(flightSegmentTO.getDepartureDateTime()));
						segInfoDTO.setArrivalDate(formatter.format(flightSegmentTO.getArrivalDateTime()));
						segInfoDTO.setArrivalDateValue(smpdtDateValue.format(flightSegmentTO.getArrivalDateTime()));
						segInfoDTO.setArrivalTime(timeFormatter.format(flightSegmentTO.getArrivalDateTime()));
						segInfoDTO.setDepartureTimeLong(flightSegmentTO.getDepartureDateTime().getTime());
						segInfoDTO.setDepartureTimeZuluLong(flightSegmentTO.getDepartureDateTimeZulu().getTime());
						segInfoDTO.setArrivalTimeLong(flightSegmentTO.getArrivalDateTime().getTime());
						segInfoDTO.setArrivalTimeZuluLong(flightSegmentTO.getArrivalDateTimeZulu().getTime());
						segInfoDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
						segInfoDTO.setSystem(ondOpTO.getSystem().toString());
						segInfoDTO.setArrivalTerminalName(flightSegmentTO.getArrivalTerminalName());
						segInfoDTO.setDepartureTerminalName(flightSegmentTO.getDepartureTerminalName());
						originCode = flightSegmentTO.getSegmentCode().substring(
								flightSegmentTO.getSegmentCode().lastIndexOf("/") + 1);
						availableFlightInfo.AddSegment(segInfoDTO);
					}
					availableFlightInfo.setFlightFareSummaryList(ondOpTO.getFlightFareSummaryList());
					if (!isNextPrivious && ondOpTO.isSelected()) {
						availableFlightInfo.setSelected(true);
					}
					if (availableFlightInfo.getSegments().size() > 0) {
						flightInfoList.add(availableFlightInfo);
					}
				}
			}
		}

		return flightInfoList;
	}

	public static Collection<AvailableFlightInfo> createAvailFlightInfoList(FlightPriceRS flightPriceRS, boolean isNextPrivious,
			Locale locale) throws ModuleException {

		SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yy", locale);
		SimpleDateFormat smpdtDateValue = new SimpleDateFormat("yyMMddHHmm");

		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		Collection<AvailableFlightInfo> flightInfoList = new ArrayList<AvailableFlightInfo>();
		for (OriginDestinationInformationTO ondInfoTO : flightPriceRS.getOriginDestinationInformationList()) {

			for (OriginDestinationOptionTO ondOpTO : ondInfoTO.getOrignDestinationOptions()) {
				AvailableFlightInfo availableFlightInfo = new AvailableFlightInfo();

				if (ondOpTO.isSelected()) {
					for (FlightSegmentTO flightSegmentTO : ondOpTO.getFlightSegmentList()) {

						SegInfoDTO segInfoDTO = new SegInfoDTO();
						segInfoDTO.setSegmentShortCode(flightSegmentTO.getSegmentCode());

						segInfoDTO.setSegmentCode(ReservationHG.getSegementDetailsWithTermainals(
								flightSegmentTO.getSegmentCode(), null, flightSegmentTO.getDepartureTerminalName(),
								flightSegmentTO.getArrivalTerminalName()));
						segInfoDTO.setFlightNumber(flightSegmentTO.getFlightNumber());
						segInfoDTO.setCarrierCode(flightSegmentTO.getOperatingAirline());
						segInfoDTO.setDepartureDate(formatter.format(flightSegmentTO.getDepartureDateTime()));
						segInfoDTO.setDepartureDateValue(smpdtDateValue.format(flightSegmentTO.getDepartureDateTime()));
						segInfoDTO.setDepartureTime(timeFormatter.format(flightSegmentTO.getDepartureDateTime()));
						segInfoDTO.setArrivalDate(formatter.format(flightSegmentTO.getArrivalDateTime()));
						segInfoDTO.setArrivalDateValue(smpdtDateValue.format(flightSegmentTO.getArrivalDateTime()));
						segInfoDTO.setArrivalTime(timeFormatter.format(flightSegmentTO.getArrivalDateTime()));
						segInfoDTO.setDepartureTimeLong(flightSegmentTO.getDepartureDateTime().getTime());
						segInfoDTO.setDepartureTimeZuluLong(flightSegmentTO.getDepartureDateTimeZulu().getTime());
						segInfoDTO.setArrivalTimeLong(flightSegmentTO.getArrivalDateTime().getTime());
						segInfoDTO.setArrivalTimeZuluLong(flightSegmentTO.getArrivalDateTimeZulu().getTime());
						segInfoDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
						segInfoDTO.setSystem(ondOpTO.getSystem().toString());
						segInfoDTO.setArrivalTerminalName(flightSegmentTO.getArrivalTerminalName());
						segInfoDTO.setDepartureTerminalName(flightSegmentTO.getDepartureTerminalName());

						availableFlightInfo.AddSegment(segInfoDTO);
					}
					availableFlightInfo.setFlightFareSummaryList(ondOpTO.getFlightFareSummaryList());
					if (!isNextPrivious && ondOpTO.isSelected()) {
						availableFlightInfo.setSelected(true);
					}
					flightInfoList.add(availableFlightInfo);
				}

			}
		}

		return flightInfoList;
	}

	/**
	 * This method is written assuming same type of passengers are having same taxes, surcharges and fares.
	 * 
	 * @param outboundFlights
	 * @param returnFlights
	 * @param discountAmount
	 * @param perPriceInfoCollection
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static FareQuoteTO fillFareQuote(FareQuoteTO fareQuote, PriceInfoTO priceInfoTO, String selectedCurrency,
			Collection<AvailableFlightInfo> outboundFlights, Collection<AvailableFlightInfo> returnFlights,
			ExchangeRateProxy exchangeRateProxy, BigDecimal discountAmount) throws ModuleException {

		int totalAdultCount = 0;
		int totalChildCount = 0;
		int totalInfantCount = 0;

		boolean blnReturn = returnFlights != null ? returnFlights.size() > 0 : false;

		boolean perAdultPriceCalculated = false;
		boolean perChildPriceCalculated = false;
		boolean perInfantPriceCalculated = false;

		BigDecimalWrapper ppAdultDepatureFare = new BigDecimalWrapper();
		BigDecimalWrapper ppChildDepatureFare = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantDepatureFare = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultReturnFare = new BigDecimalWrapper();
		BigDecimalWrapper ppChildReturnFare = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantReturnFare = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultDepatureTax = new BigDecimalWrapper();
		BigDecimalWrapper ppChildDepatureTax = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantDepatureTax = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultReturnTax = new BigDecimalWrapper();
		BigDecimalWrapper ppChildReturnTax = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantReturnTax = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultDepatureSurcharge = new BigDecimalWrapper();
		BigDecimalWrapper ppChildDepatureSurcharge = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantDepatureSurcharge = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultReturnSurcharge = new BigDecimalWrapper();
		BigDecimalWrapper ppChildReturnSurcharge = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantReturnSurcharge = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultDepatureExternalCharge = new BigDecimalWrapper();
		BigDecimalWrapper ppChildDepatureExternalCharge = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantDepatureExternalCharge = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultReturnExternalCharge = new BigDecimalWrapper();
		BigDecimalWrapper ppChildReturnExternalCharge = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantReturnExternalCharge = new BigDecimalWrapper();

		SegmentFareTO segmentFareTo = null;
		Collection<SegmentFareTO> segCollection = new ArrayList<SegmentFareTO>();
		Collection<PaxPriceTO> paxPriceTOs = new ArrayList<PaxPriceTO>();
		Collection<PaxFareTO> paxFareTOs = new ArrayList<PaxFareTO>();

		Collection<SurchargeTO> adultSurchargeTOs = new ArrayList<SurchargeTO>();
		Collection<SurchargeTO> childSurchargeTOs = new ArrayList<SurchargeTO>();
		Collection<SurchargeTO> infantSurchargeTOs = new ArrayList<SurchargeTO>();

		Collection<TaxTO> adultTaxTOs = new ArrayList<TaxTO>();
		Collection<TaxTO> childTaxTOs = new ArrayList<TaxTO>();
		Collection<TaxTO> infantTaxTOs = new ArrayList<TaxTO>();

		// We need to have the list of taxes and surcharges to create the tax and surcharge
		// breakdown in the ibe.
		Collection<TaxTO> totalTaxList = new ArrayList<TaxTO>();
		Collection<SurchargeTO> totalSurchargeList = new ArrayList<SurchargeTO>();

		String strDepartureSegCode = null;
		String strReturnSegCode = null;
		String strDepartureSegName = null;
		String strReturnSegName = null;
		Collection<PerPaxPriceInfoTO> perPriceInfoCollection = priceInfoTO.getPerPaxPriceInfo();

		FareTypeTO fareTypeTO = priceInfoTO.getFareTypeTO();
		boolean addOutboundFlexiFee = false;
		boolean addInboundFlexiFee = false;
		Map<Integer, Boolean> ondFlexiSelection = fareTypeTO.getOndFlexiSelection();

		if (ondFlexiSelection != null) {
			// FIXME OND sequence hard coding
			if (ondFlexiSelection.containsKey(OndSequence.OUT_BOUND)) {
				addOutboundFlexiFee = ondFlexiSelection.get(OndSequence.OUT_BOUND);
			}

			if (ondFlexiSelection.containsKey(OndSequence.IN_BOUND)) {
				addInboundFlexiFee = ondFlexiSelection.get(OndSequence.IN_BOUND);
			}
		}

		for (PerPaxPriceInfoTO perPaxPriceInfoTO : perPriceInfoCollection) {
			if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.ADULT) == 0) {

				totalAdultCount++;
				// assume all adults have same fares, taxes, surcharges and fees
				if (!perAdultPriceCalculated) {
					findPerPaxPrices(perPaxPriceInfoTO, ppAdultDepatureFare, ppAdultReturnFare, ppAdultDepatureTax,
							ppAdultReturnTax, ppAdultDepatureSurcharge, ppAdultReturnSurcharge, outboundFlights, returnFlights,
							blnReturn, ppAdultDepatureExternalCharge, ppAdultReturnExternalCharge);
					adultSurchargeTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getSurcharges());
					adultTaxTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getTaxes());
					perAdultPriceCalculated = true;
				}
			} else if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.CHILD) == 0) {

				totalChildCount++;
				// assume all children have same fares, taxes, surcharges and
				// fees
				if (!perChildPriceCalculated) {
					findPerPaxPrices(perPaxPriceInfoTO, ppChildDepatureFare, ppChildReturnFare, ppChildDepatureTax,
							ppChildReturnTax, ppChildDepatureSurcharge, ppChildReturnSurcharge, outboundFlights, returnFlights,
							blnReturn, ppChildDepatureExternalCharge, ppChildReturnExternalCharge);
					childSurchargeTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getSurcharges());
					childTaxTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getTaxes());
					perChildPriceCalculated = true;
				}
			} else if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.INFANT) == 0) {

				totalInfantCount++;
				// assume all infants have same fares, taxes, surcharges and
				// fees
				if (!perInfantPriceCalculated) {
					findPerPaxPrices(perPaxPriceInfoTO, ppInfantDepatureFare, ppInfantReturnFare, ppInfantDepatureTax,
							ppInfantReturnTax, ppInfantDepatureSurcharge, ppInfantReturnSurcharge, outboundFlights,
							returnFlights, blnReturn, ppInfantDepatureExternalCharge, ppInfantReturnExternalCharge);
					infantSurchargeTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getSurcharges());
					infantTaxTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getTaxes());
					perInfantPriceCalculated = true;
				}
			} else {
				// TODO : Fix error message
				throw new ModuleException("Wrong passenger type detected! Passenger type passed = ["
						+ perPaxPriceInfoTO.getPassengerType() + "]");
			}
		}

		// Get the total surcharge list by adding up surcharges for all types of passengers.
		ReservationBeanUtil.meargeBasedOnCodeAndAddAllSurcharges(totalSurchargeList, adultSurchargeTOs);
		ReservationBeanUtil.meargeBasedOnCodeAndAddAllSurcharges(totalSurchargeList, childSurchargeTOs);
		ReservationBeanUtil.meargeBasedOnCodeAndAddAllSurcharges(totalSurchargeList, infantSurchargeTOs);

		// Get the total tax list by adding up taxes for all types of passengers.
		ReservationBeanUtil.meargeBasedOnCodeAndAddAllTaxes(totalTaxList, adultTaxTOs);
		ReservationBeanUtil.meargeBasedOnCodeAndAddAllTaxes(totalTaxList, childTaxTOs);
		ReservationBeanUtil.meargeBasedOnCodeAndAddAllTaxes(totalTaxList, infantTaxTOs);

		Collection<SurchargeFETO> perPaxSurchargeBreakDown = ReservationBeanUtil.convertToSurchargeFETO(totalSurchargeList,
				selectedCurrency, exchangeRateProxy);
		Collection<TaxFETO> perPaxTaxBreakDown = ReservationBeanUtil.convertToTaxFETO(totalTaxList, selectedCurrency,
				exchangeRateProxy);

		// Setting the departure fare information
		Collection<PaxFareTO> collectionDepaturePaxWise = new ArrayList<PaxFareTO>();
		BigDecimalWrapper totalDepaturePrice = new BigDecimalWrapper();
		BigDecimalWrapper totalPrice = new BigDecimalWrapper();

		BigDecimalWrapper totalFare = new BigDecimalWrapper();
		BigDecimalWrapper totalTaxSurcharges = new BigDecimalWrapper();
		BigDecimalWrapper totalTax = new BigDecimalWrapper();

		BigDecimalWrapper totalSegmentTaxSurcharge = new BigDecimalWrapper();

		// Setting this to Zero when creating no other charges
		BigDecimalWrapper ppOther = new BigDecimalWrapper();
		if (totalAdultCount > 0) {

			// add departure flexi charge to departure fare if flexi is chargeable
			if (addOutboundFlexiFee) {
				ppAdultDepatureSurcharge.add(ppAdultDepatureExternalCharge.getValue());
			}

			// No need the display type sending the pax type
			PaxFareTO adultFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.ADULT, totalAdultCount, totalDepaturePrice,
					ppAdultDepatureFare, ppAdultDepatureTax, ppAdultDepatureSurcharge, ppOther, selectedCurrency,
					totalSegmentTaxSurcharge, exchangeRateProxy);
			collectionDepaturePaxWise.add(adultFareTO);
		}
		if (totalChildCount > 0) {

			// add departure flexi charge to departure fare if flexi is chargeable
			if (addOutboundFlexiFee) {
				ppChildDepatureSurcharge.add(ppChildDepatureExternalCharge.getValue());
			}

			PaxFareTO childFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.CHILD, totalChildCount, totalDepaturePrice,
					ppChildDepatureFare, ppChildDepatureTax, ppChildDepatureSurcharge, ppOther, selectedCurrency,
					totalSegmentTaxSurcharge, exchangeRateProxy);
			collectionDepaturePaxWise.add(childFareTO);
		}
		if (totalInfantCount > 0) {

			// add departure flexi charge to departure fare if flexi is chargeable
			if (addOutboundFlexiFee) {
				ppInfantDepatureSurcharge.add(ppInfantDepatureExternalCharge.getValue());
			}

			PaxFareTO intantFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.INFANT, totalInfantCount, totalDepaturePrice,
					ppInfantDepatureFare, ppInfantDepatureTax, ppInfantDepatureSurcharge, ppOther, selectedCurrency,
					totalSegmentTaxSurcharge, exchangeRateProxy);
			collectionDepaturePaxWise.add(intantFareTO);
		}

		for (AvailableFlightInfo flightInfo : outboundFlights) {
			if (flightInfo.isSelected()) {
				for (SegInfoDTO segInfo : flightInfo.getSegments()) {
					String strSegment = segInfo.getSegmentCode();
					if (strDepartureSegName == null) {
						strDepartureSegName = strSegment;
					} else {
						// strDepartureSegName = strDepartureSegName + " - "
						// + strSegment.substring(strSegment.indexOf("-") + 1);

						strDepartureSegName = strDepartureSegName + " / " + strSegment.split("/")[1];
					}

					String[] arrSegment = segInfo.getFlightRefNumber().split("\\$");
					String strSegmentShort = arrSegment[1];
					if (strDepartureSegCode == null) {
						strDepartureSegCode = strSegmentShort;
					} else {
						strDepartureSegCode = strDepartureSegCode + " / "
								+ strSegmentShort.substring(strSegmentShort.indexOf("/") + 1);
					}
				}
				break;
			}
		}

		segmentFareTo = new SegmentFareTO();
		segmentFareTo.setPaxWise(collectionDepaturePaxWise);
		segmentFareTo.setSegmentCode(strDepartureSegCode);
		segmentFareTo.setSegmentName(strDepartureSegName);
		segmentFareTo.setTotalPrice(totalDepaturePrice.getDisplayValue());

		segmentFareTo.setTotalTaxSurcharges(AccelAeroCalculator.formatAsDecimal(totalSegmentTaxSurcharge.getValue()));
		CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency, ApplicationEngine.IBE);
		if (exchangeRate != null) {
			Currency currency = exchangeRate.getCurrency();
			boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
			BigDecimal boundryValue = null;
			BigDecimal breakPoint = null;
			if (isCurrencyRoundUpEnabled) {
				boundryValue = currency.getBoundryValue();
				breakPoint = currency.getBreakPoint();
			}

			segmentFareTo.setTotalPriceInSelectedCurr(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					totalDepaturePrice.getValue(), exchangeRate.getMultiplyingExchangeRate(), boundryValue, breakPoint)));

			segmentFareTo.setTotalTaxSurchargesInSelectedCurr(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy
					.convertAndRound(totalSegmentTaxSurcharge.getValue(), exchangeRate.getMultiplyingExchangeRate(),
							boundryValue, breakPoint)));
		}
		segCollection.add(segmentFareTo);
		totalSegmentTaxSurcharge.setValue(AccelAeroCalculator.getDefaultBigDecimalZero());

		if (blnReturn) {
			Collection<PaxFareTO> collectionReturnPaxWise = new ArrayList<PaxFareTO>();
			BigDecimalWrapper totalReturnPrice = new BigDecimalWrapper();

			if (totalAdultCount > 0) {

				// add departure flexi charge to return fare if flexi is chargeable
				if (addInboundFlexiFee) {
					ppAdultReturnSurcharge.add(ppAdultReturnExternalCharge.getValue());
				}

				// No need the display type sending the pax type
				PaxFareTO adultFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.ADULT, totalAdultCount, totalReturnPrice,
						ppAdultReturnFare, ppAdultReturnTax, ppAdultReturnSurcharge, ppOther, selectedCurrency,
						totalSegmentTaxSurcharge, exchangeRateProxy);
				collectionReturnPaxWise.add(adultFareTO);
			}
			if (totalChildCount > 0) {

				// add departure flexi charge to return fare if flexi is chargeable
				if (addInboundFlexiFee) {
					ppChildReturnSurcharge.add(ppChildReturnExternalCharge.getValue());
				}

				PaxFareTO childFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.CHILD, totalChildCount, totalReturnPrice,
						ppChildReturnFare, ppChildReturnTax, ppChildReturnSurcharge, ppOther, selectedCurrency,
						totalSegmentTaxSurcharge, exchangeRateProxy);
				collectionReturnPaxWise.add(childFareTO);
			}
			if (totalInfantCount > 0) {

				// add departure flexi charge to return fare if flexi is chargeable
				if (addInboundFlexiFee) {
					ppInfantReturnSurcharge.add(ppInfantReturnExternalCharge.getValue());
				}

				PaxFareTO intantFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.INFANT, totalInfantCount,
						totalReturnPrice, ppInfantReturnFare, ppInfantReturnTax, ppInfantReturnSurcharge, ppOther,
						selectedCurrency, totalSegmentTaxSurcharge, exchangeRateProxy);
				collectionReturnPaxWise.add(intantFareTO);
			}

			for (AvailableFlightInfo flightInfo : returnFlights) {
				if (flightInfo.isSelected()) {
					for (SegInfoDTO segInfo : flightInfo.getSegments()) {
						String strSegment = segInfo.getSegmentCode();
						if (strReturnSegName == null) {
							strReturnSegName = strSegment;
						} else {
							strReturnSegName = strReturnSegName + " - " + strSegment.substring(strSegment.indexOf("-") + 1);
						}

						String[] arrSegment = segInfo.getFlightRefNumber().split("\\$");
						String strSegmentShort = arrSegment[1];
						if (strReturnSegCode == null) {
							strReturnSegCode = strSegmentShort;
						} else {
							strReturnSegCode = strReturnSegCode + "/"
									+ strSegmentShort.substring(strSegmentShort.indexOf("/") + 1);
						}
					}
					break;
				}
			}

			segmentFareTo = new SegmentFareTO();
			segmentFareTo.setPaxWise(collectionReturnPaxWise);
			segmentFareTo.setSegmentCode(strReturnSegCode);
			segmentFareTo.setSegmentName(strReturnSegName);
			segmentFareTo.setTotalPrice(totalReturnPrice.getDisplayValue());
			segmentFareTo.setTotalTaxSurcharges(AccelAeroCalculator.formatAsDecimal(totalSegmentTaxSurcharge.getValue()));

			if (exchangeRate != null) {

				segmentFareTo.setTotalPriceInSelectedCurr(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
						totalReturnPrice.getValue(), exchangeRate.getMultiplyingExchangeRate())));
				segmentFareTo.setTotalTaxSurchargesInSelectedCurr(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
						.multiply(totalSegmentTaxSurcharge.getValue(), exchangeRate.getMultiplyingExchangeRate())));
			}
			segCollection.add(segmentFareTo);
		}

		if (totalAdultCount > 0) {
			BigDecimalWrapper ppAdultFare = new BigDecimalWrapper();
			ppAdultFare.add(ppAdultDepatureFare.getValue());
			if (blnReturn) {
				ppAdultFare.add(ppAdultReturnFare.getValue());
			}

			BigDecimalWrapper ppAdultSurcharge = new BigDecimalWrapper();
			ppAdultSurcharge.add(ppAdultDepatureSurcharge.getValue());
			if (blnReturn) {
				ppAdultSurcharge.add(ppAdultReturnSurcharge.getValue());
			}

			BigDecimalWrapper ppAdultTax = new BigDecimalWrapper();
			ppAdultTax.add(ppAdultDepatureTax.getValue());
			if (blnReturn) {
				ppAdultTax.add(ppAdultReturnTax.getValue());
			}

			PaxFareTO adultFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_ADULT_DISPLAY, totalAdultCount,
					totalPrice, totalFare, totalTax, totalTaxSurcharges, ppAdultFare, ppAdultSurcharge, ppAdultTax, ppOther,
					selectedCurrency, exchangeRateProxy);
			paxFareTOs.add(adultFareTO);

			PaxPriceTO paxPriceTO = getPerPaxPriceInfo(perPriceInfoCollection, PaxTypeTO.ADULT, selectedCurrency,
					exchangeRateProxy);
			paxPriceTOs.add(paxPriceTO);
		}

		if (totalChildCount > 0) {
			BigDecimalWrapper ppChildFare = new BigDecimalWrapper();
			ppChildFare.add(ppChildDepatureFare.getValue());
			if (blnReturn) {
				ppChildFare.add(ppChildReturnFare.getValue());
			}

			BigDecimalWrapper ppChildSurcharge = new BigDecimalWrapper();
			ppChildSurcharge.add(ppChildDepatureSurcharge.getValue());
			if (blnReturn) {
				ppChildSurcharge.add(ppChildReturnSurcharge.getValue());
			}

			BigDecimalWrapper ppChildTax = new BigDecimalWrapper();
			ppChildTax.add(ppChildDepatureTax.getValue());
			if (blnReturn) {
				ppChildTax.add(ppChildReturnTax.getValue());
			}

			PaxFareTO childFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_CHILD_DISPLAY, totalChildCount,
					totalPrice, totalFare, totalTax, totalTaxSurcharges, ppChildFare, ppChildSurcharge, ppChildTax, ppOther,
					selectedCurrency, exchangeRateProxy);
			paxFareTOs.add(childFareTO);

			PaxPriceTO paxPriceTO = getPerPaxPriceInfo(perPriceInfoCollection, PaxTypeTO.CHILD, selectedCurrency,
					exchangeRateProxy);
			paxPriceTOs.add(paxPriceTO);
		}

		if (totalInfantCount > 0) {
			BigDecimalWrapper ppInfantFare = new BigDecimalWrapper();
			ppInfantFare.add(ppInfantDepatureFare.getValue());
			if (blnReturn) {
				ppInfantFare.add(ppInfantReturnFare.getValue());
			}

			BigDecimalWrapper ppInfantSurcharge = new BigDecimalWrapper();
			ppInfantSurcharge.add(ppInfantDepatureSurcharge.getValue());
			if (blnReturn) {
				ppInfantSurcharge.add(ppInfantReturnSurcharge.getValue());
			}

			BigDecimalWrapper ppInfantTax = new BigDecimalWrapper();
			ppInfantTax.add(ppInfantDepatureTax.getValue());
			if (blnReturn) {
				ppInfantTax.add(ppInfantReturnTax.getValue());
			}

			PaxFareTO intantFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_INFANT_DISPLAY, totalInfantCount,
					totalPrice, totalFare, totalTax, totalTaxSurcharges, ppInfantFare, ppInfantSurcharge, ppInfantTax, ppOther,
					selectedCurrency, exchangeRateProxy);
			paxFareTOs.add(intantFareTO);

			PaxPriceTO paxPriceTO = getPerPaxPriceInfo(perPriceInfoCollection, PaxTypeTO.INFANT, selectedCurrency,
					exchangeRateProxy);
			paxPriceTOs.add(paxPriceTO);
		}

		fareQuote = new FareQuoteTO();
		fareQuote.setTotalFare(totalFare.getDisplayValue());
		fareQuote.setTotalTax(totalTax.getDisplayValue());
		fareQuote.setTotalTaxSurcharge(totalTaxSurcharges.getDisplayValue());
		fareQuote.setFareType(fareTypeTO.getFareType());

		ApplicablePromotionDetailsTO promotionTO = fareTypeTO.getPromotionTO();
		if (promotionTO != null && discountAmount != null) {
			fareQuote.setHasPromoDiscount(true);
			if (PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY.equals(promotionTO.getDiscountAs())) {
				totalPrice.add(discountAmount.negate());
				fareQuote.setDeductFromTotal(true);
			}
		} else {
			discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		fareQuote.setTotalIbePromoDiscount(AccelAeroCalculator.formatAsDecimal(discountAmount));

		fareQuote.setTotalPrice(totalPrice.getDisplayValue());
		fareQuote.setSegmentFare(segCollection);
		fareQuote.setPaxWisePrice(paxPriceTOs);
		fareQuote.setHasFareQuote(true);
		fareQuote.setCurrency(AppSysParamsUtil.getBaseCurrency());
		fareQuote.setSelectedCurrency(selectedCurrency);
		if (!fareQuote.getCurrency().equals(selectedCurrency)) {
			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency,
					ApplicationEngine.IBE);
			if (currencyExchangeRate != null) {
				Currency currency = exchangeRate.getCurrency();
				boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
				BigDecimal boundryValue = null;
				BigDecimal breakPoint = null;
				if (isCurrencyRoundUpEnabled) {
					boundryValue = currency.getBoundryValue();
					breakPoint = currency.getBreakPoint();
				}
				fareQuote.setSelectedCurrency(selectedCurrency);
				fareQuote.setSelectedEXRate(currencyExchangeRate.getMultiplyingExchangeRate().toPlainString());
				fareQuote.setSelectedtotalPrice(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(fareQuote.getTotalPrice()),
						boundryValue, breakPoint)));
				fareQuote.setTotalSelectedIbePromoDiscount(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy
						.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), discountAmount, boundryValue,
								breakPoint)));

			}
		}

		// set the surcharge and tax breakdown - only if the application parameter is enabled.
		if (AppSysParamsUtil.isShowIBEChargesBreakdownEnabled()) {
			fareQuote.setPerPaxTaxBD(perPaxTaxBreakDown);
			fareQuote.setPerPaxSurchargeBD(perPaxSurchargeBreakDown);
		}

		return fareQuote;
	}

	private static PaxPriceTO getPerPaxPriceInfo(Collection<PerPaxPriceInfoTO> perPriceInfoCollection, String paxType,
			String selectedCurrency, ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency,
				ApplicationEngine.IBE);
		Currency currency = currencyExchangeRate.getCurrency();
		boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
		BigDecimal boundryValue = null;
		BigDecimal breakPoint = null;
		if (isCurrencyRoundUpEnabled) {
			boundryValue = currency.getBoundryValue();
			breakPoint = currency.getBreakPoint();
		}

		for (PerPaxPriceInfoTO perPaxPriceInfoTO : perPriceInfoCollection) {
			if (perPaxPriceInfoTO.getPassengerType().equals(paxType)) {

				PaxPriceTO paxPriceTO = new PaxPriceTO();
				paxPriceTO.setPaxType(perPaxPriceInfoTO.getPassengerType());
				paxPriceTO.setPaxTotalPrice(AccelAeroCalculator.formatAsDecimal(perPaxPriceInfoTO.getPassengerPrice()
						.getTotalPrice()));
				paxPriceTO.setPaxTotalPriceWithInfant(AccelAeroCalculator.formatAsDecimal(perPaxPriceInfoTO.getPassengerPrice()
						.getTotalPriceWithInfant()));
				paxPriceTO.setPaxTotalPriceInSelectedCurr(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy
						.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), perPaxPriceInfoTO.getPassengerPrice()
								.getTotalPrice(), boundryValue, breakPoint)));

				return paxPriceTO;
			}
		}

		// This can not happen
		throw new IllegalArgumentException();
	}

	private static FlightSegmentTO getOriginFlightSegment(List<FlightSegmentTO> segList, String origin) {
		for (FlightSegmentTO segment : segList) {
			String segmentOrigin = segment.getSegmentCode().substring(0, segment.getSegmentCode().indexOf("/"));
			if (segmentOrigin.equals(origin)) {
				return segment;
			}
		}
		return null;
	}

	private static void findPerPaxPrices(PerPaxPriceInfoTO perPaxPriceInfoTO, BigDecimalWrapper ppDFare,
			BigDecimalWrapper ppRFare, BigDecimalWrapper ppDTax, BigDecimalWrapper ppRTax, BigDecimalWrapper ppDSurcharge,
			BigDecimalWrapper ppRSurcharge, Collection<AvailableFlightInfo> outboundFlights,
			Collection<AvailableFlightInfo> returnFlights, boolean blnReturn, BigDecimalWrapper ppDExternalCharge,
			BigDecimalWrapper ppRExternalCharge) throws ModuleException {

		String strDepSegment = null;
		String strRetSegment = null;
		for (AvailableFlightInfo flightInfo : outboundFlights) {
			if (flightInfo.isSelected()) {
				for (SegInfoDTO segInfo : flightInfo.getSegments()) {
					String[] arrSegment = segInfo.getFlightRefNumber().split("\\$");
					String strSegment = arrSegment[1];
					if (strDepSegment == null) {
						strDepSegment = strSegment;
					} else {
						strDepSegment = strDepSegment + "/" + strSegment.substring(strSegment.indexOf("/") + 1);
					}
				}
				for (BaseFareTO basefare : perPaxPriceInfoTO.getPassengerPrice().getBaseFares()) {
					if (strDepSegment.indexOf(basefare.getSegmentCode()) != -1) {
						ppDFare.add(basefare.getAmount());
					}
				}
				for (TaxTO taxTo : perPaxPriceInfoTO.getPassengerPrice().getTaxes()) {
					if (strDepSegment.indexOf(taxTo.getSegmentCode()) != -1) {
						ppDTax.add(taxTo.getAmount());
					}
				}
				for (SurchargeTO surchargeTo : perPaxPriceInfoTO.getPassengerPrice().getSurcharges()) {
					if (strDepSegment.indexOf(surchargeTo.getSegmentCode()) != -1) {
						ppDSurcharge.add(surchargeTo.getAmount());
					}
				}
				// FIXME harcoded for outbound ond
				for (SurchargeTO externalChargeTo : perPaxPriceInfoTO.getPassengerPrice()
						.getOndExternalCharge(OndSequence.OUT_BOUND).getExternalCharges()) {
					if (strDepSegment.indexOf(externalChargeTo.getSegmentCode()) != -1) {
						ppDExternalCharge.add(externalChargeTo.getAmount());
					}
				}

			}

		}
		if (blnReturn) {
			for (AvailableFlightInfo flightInfo : returnFlights) {
				if (flightInfo.isSelected()) {
					for (SegInfoDTO segInfo : flightInfo.getSegments()) {
						String[] arrSegment = segInfo.getFlightRefNumber().split("\\$");
						String strSegment = arrSegment[1];
						if (strRetSegment == null) {
							strRetSegment = strSegment;
						} else {
							strRetSegment = strRetSegment + "/" + strSegment.substring(strSegment.indexOf("/") + 1);
						}
					}
					for (BaseFareTO basefare : perPaxPriceInfoTO.getPassengerPrice().getBaseFares()) {
						if (strRetSegment.indexOf(basefare.getSegmentCode()) != -1) {
							ppRFare.add(basefare.getAmount());
						}
					}
					for (TaxTO taxTo : perPaxPriceInfoTO.getPassengerPrice().getTaxes()) {
						if (strRetSegment.indexOf(taxTo.getSegmentCode()) != -1) {
							ppRTax.add(taxTo.getAmount());
						}
					}
					for (SurchargeTO surchargeTo : perPaxPriceInfoTO.getPassengerPrice().getSurcharges()) {
						if (strRetSegment.indexOf(surchargeTo.getSegmentCode()) != -1) {
							ppRSurcharge.add(surchargeTo.getAmount());
						}
					}
					// FIXME harcoded for inbound ond
					for (SurchargeTO externalChargeTo : perPaxPriceInfoTO.getPassengerPrice()
							.getOndExternalCharge(OndSequence.IN_BOUND).getExternalCharges()) {
						if (strRetSegment.indexOf(externalChargeTo.getSegmentCode()) != -1) {
							ppRExternalCharge.add(externalChargeTo.getAmount());
						}
					}
				}
			}
		}
		if (perPaxPriceInfoTO.getPassengerPrice().getFees() == null || !perPaxPriceInfoTO.getPassengerPrice().getFees().isEmpty()) {
			throw new ModuleException("fees are not supported");
		}

	}

	private static PaxFareTO createPaxFareTOWhileAddingTotalPrice(String paxTypeDisplay, int paxCount,
			BigDecimalWrapper totalPrice, BigDecimalWrapper totalFare, BigDecimalWrapper totalTax,
			BigDecimalWrapper totalTaxSurcharges, BigDecimalWrapper ppFare, BigDecimalWrapper ppSurcharge,
			BigDecimalWrapper ppTax, BigDecimalWrapper ppOther, String selCurrCode, ExchangeRateProxy exchangeRateProxy) {

		BigDecimalWrapper totalPerPaxPrice = new BigDecimalWrapper();
		totalPerPaxPrice.add(AccelAeroCalculator.add(ppFare.getValue(), ppSurcharge.getValue(), ppTax.getValue(),
				ppOther.getValue()));

		BigDecimalWrapper totalPerPaxFare = new BigDecimalWrapper();
		totalPerPaxFare.add(ppFare.getValue());

		BigDecimalWrapper totalPerPaxTax = new BigDecimalWrapper();
		totalPerPaxTax.add(ppTax.getValue());

		BigDecimalWrapper totalPerPaxCharges = new BigDecimalWrapper();
		totalPerPaxCharges.add(AccelAeroCalculator.add(ppSurcharge.getValue(), ppTax.getValue(), ppOther.getValue()));

		BigDecimalWrapper totalPriceForAllPax = new BigDecimalWrapper();
		totalPriceForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxPrice.getValue(), new BigDecimal(paxCount)));

		BigDecimalWrapper totalFareForAllPax = new BigDecimalWrapper();
		totalFareForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxFare.getValue(), new BigDecimal(paxCount)));

		BigDecimalWrapper totalTaxForAllPax = new BigDecimalWrapper();
		totalTaxForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxTax.getValue(), new BigDecimal(paxCount)));

		BigDecimalWrapper totalSurchargesForAllPax = new BigDecimalWrapper();
		totalSurchargesForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxCharges.getValue(), new BigDecimal(paxCount)));

		PaxFareTO ppFareTO = createPaxFareTO(paxTypeDisplay, paxCount, totalPriceForAllPax, totalPerPaxPrice, ppFare,
				ppSurcharge, ppTax, ppOther, selCurrCode, exchangeRateProxy);
		totalPrice.add(totalPriceForAllPax.getValue());

		totalFare.add(totalFareForAllPax.getValue());
		totalTax.add(totalTaxForAllPax.getValue());
		totalTaxSurcharges.add(totalSurchargesForAllPax.getValue());

		return ppFareTO;
	}

	private static PaxFareTO createPaxFareTOWhileAddingTotalPrice(String paxTypeDisplay, int paxCount,
			BigDecimalWrapper totalPrice, BigDecimalWrapper ppFare, BigDecimalWrapper ppTax, BigDecimalWrapper ppSurcharge,
			BigDecimalWrapper ppOther, String selCurrCode, BigDecimalWrapper totalSegmentTaxSurcharge,
			ExchangeRateProxy exchangeRateProxy) {

		BigDecimalWrapper totalPerPaxPrice = new BigDecimalWrapper();
		totalPerPaxPrice.add(AccelAeroCalculator.add(ppFare.getValue(), ppSurcharge.getValue(), ppTax.getValue(),
				ppOther.getValue()));

		BigDecimalWrapper totalPerPaxFare = new BigDecimalWrapper();
		totalPerPaxFare.add(ppFare.getValue());

		BigDecimalWrapper totalPerPaxCharges = new BigDecimalWrapper();
		totalPerPaxCharges.add(AccelAeroCalculator.add(ppSurcharge.getValue(), ppTax.getValue(), ppOther.getValue()));

		BigDecimalWrapper totalPriceForAllPax = new BigDecimalWrapper();
		totalPriceForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxPrice.getValue(), new BigDecimal(paxCount)));

		BigDecimalWrapper totalFareForAllPax = new BigDecimalWrapper();
		totalFareForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxFare.getValue(), new BigDecimal(paxCount)));

		BigDecimalWrapper totalSurchargesForAllPax = new BigDecimalWrapper();
		totalSurchargesForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxCharges.getValue(), new BigDecimal(paxCount)));
		totalSegmentTaxSurcharge.add(totalSurchargesForAllPax.getValue());

		PaxFareTO ppFareTO = createPaxFareTO(paxTypeDisplay, paxCount, totalPriceForAllPax, totalPerPaxPrice, ppFare,
				ppSurcharge, ppTax, ppOther, selCurrCode, exchangeRateProxy);
		totalPrice.add(totalPriceForAllPax.getValue());

		return ppFareTO;
	}

	private static PaxFareTO createPaxFareTO(String paxTypeDescription, int totalPaxCount,
			BigDecimalWrapper totalPriceForPaxCount, BigDecimalWrapper ppTotalPrice, BigDecimalWrapper ppFare,
			BigDecimalWrapper ppSurcharge, BigDecimalWrapper ppTax, BigDecimalWrapper ppOther, String selCurrency,
			ExchangeRateProxy exchangeRateProxy) {

		// Set PaxFareTO for Child & Children charges
		PaxFareTO paxFareTo = new PaxFareTO();
		paxFareTo.setPaxType(paxTypeDescription);
		paxFareTo.setFare(ppFare.getDisplayValue());
		paxFareTo.setTotalPaxFare(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(ppFare.getValue(),
				totalPaxCount)));
		paxFareTo.setNoPax(totalPaxCount + "");
		paxFareTo.setPerPax(ppTotalPrice.getDisplayValue());

		// setting tax + surcharge as need ed in ibe
		BigDecimal totalTaxSurcharges = AccelAeroCalculator.add(ppTax.getValue(), ppSurcharge.getValue());

		paxFareTo.setSur(AccelAeroCalculator.formatAsDecimal(totalTaxSurcharges));

		paxFareTo.setTax(ppTax.getDisplayValue());

		paxFareTo.setTotal(totalPriceForPaxCount.getDisplayValue());

		paxFareTo.setOther(ppOther.getDisplayValue());

		// Geting the exchangeRate
		try {
			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency,
					ApplicationEngine.IBE);
			Currency currency = currencyExchangeRate.getCurrency();
			boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
			BigDecimal boundryValue = null;
			BigDecimal breakPoint = null;
			if (isCurrencyRoundUpEnabled) {
				boundryValue = currency.getBoundryValue();
				breakPoint = currency.getBreakPoint();
			}

			paxFareTo.setSelCurSur(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), totalTaxSurcharges, boundryValue, breakPoint)));
			paxFareTo.setSelCurTax(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), ppTax.getValue(), boundryValue, breakPoint)));
			paxFareTo.setSelCurFare(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), ppFare.getValue(), boundryValue, breakPoint)));
			paxFareTo.setSelCurTotalPaxFare(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(),
					AccelAeroCalculator.multiply(ppFare.getValue(), totalPaxCount), boundryValue, breakPoint)));

			paxFareTo.setSelCurtotal(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), ppTotalPrice.getValue(), boundryValue, breakPoint)));

		} catch (Exception e) {
			log.error(e);
		}

		return paxFareTo;
	}

	/**
	 * method to create AvailableFlightSearchDTO from FlightSearchDTO
	 * 
	 * @param cabinClassCode
	 * @param logicalCabinClass
	 * @param logicalCCSelection
	 * 
	 * @throws ParseException
	 */
	public static FlightPriceRQ createFareQuoteRQ(AvailableFlightSearchDTO flightSearch, List<FlightSegmentTO> fltSegList,
			String cabinClassCode, String logicalCabinClass, String logicalCCSelection) {

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();

		// setting origin destination information
		OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
		depatureOnD.setOrigin(flightSearch.getFromAirport());
		depatureOnD.setDestination(flightSearch.getToAirport());

		depatureOnD.setDepartureDateTimeStart(flightSearch.getDepatureDateTimeStart());
		depatureOnD.setDepartureDateTimeEnd(flightSearch.getDepatureDateTimeEnd());

		OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
		OriginDestinationOptionTO retOndOptionTO = new OriginDestinationOptionTO();
		boolean returnFlag = false;

		// Map to flight seg code & flight seg Id key value for populating cabin class & logical cabin class selection
		Map<String, Integer> selectedFlightSegMap = new HashMap<String, Integer>();

		if (fltSegList != null) {
			for (FlightSegmentTO fltSeg : fltSegList) {
				FlightSegmentTO flightSegementTO = new FlightSegmentTO();
				flightSegementTO.setFlightRefNumber(fltSeg.getFlightRefNumber());
				if (!fltSeg.isReturnFlag()) {
					depOndOptionTO.getFlightSegmentList().add(flightSegementTO);
				} else {
					retOndOptionTO.getFlightSegmentList().add(flightSegementTO);
					returnFlag = true;
				}

				selectedFlightSegMap.put(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltSeg.getFlightRefNumber()),
						FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltSeg.getFlightRefNumber()));
			}
		}

		// TODO: this needs to be re-factored after the re-quote changes
		depatureOnD.setPreferredLogicalCabin(logicalCabinClass);
		depatureOnD.setPreferredClassOfService(cabinClassCode);

		depatureOnD.getOrignDestinationOptions().add(depOndOptionTO);

		if (returnFlag) {
			OriginDestinationInformationTO returnOnD = flightPriceRQ.addNewOriginDestinationInformation();
			returnOnD.setOrigin(flightSearch.getToAirport());
			returnOnD.setDestination(flightSearch.getFromAirport());

			returnOnD.setDepartureDateTimeStart(flightSearch.getReturnDateTimeStart());
			returnOnD.setDepartureDateTimeEnd(flightSearch.getReturnDateTimeEnd());
			returnOnD.setOrigin(flightSearch.getToAirport());
			returnOnD.setDestination(flightSearch.getFromAirport());

			returnOnD.setDepartureDateTimeStart(flightSearch.getReturnDateTimeStart());
			returnOnD.setDepartureDateTimeEnd(flightSearch.getReturnDateTimeEnd());

			// TODO: this needs to be re-factored after the re-quote changes
			returnOnD.setPreferredLogicalCabin(logicalCabinClass);
			returnOnD.setPreferredClassOfService(cabinClassCode);

			returnOnD.getOrignDestinationOptions().add(retOndOptionTO);
		}

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(flightSearch.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(flightSearch.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(flightSearch.getInfantCount());

		// Same Fare Or HigerFare
		flightPriceRQ.getAvailPreferences().setOldFareAmount(flightSearch.getModifiedOndPaxFareAmount());
		flightPriceRQ.getAvailPreferences().setOldFareID(
				flightSearch.getModifiedOndFareId() != null ? flightSearch.getModifiedOndFareId() : null);
		flightPriceRQ.getAvailPreferences().setOldFareType(
				flightSearch.getModifiedOndFareType() != null ? flightSearch.getModifiedOndFareType() : null);

		// Half return parameters
		flightPriceRQ.getAvailPreferences().setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());
		flightPriceRQ.getAvailPreferences().setStayOverTimeInMillis(flightSearch.getStayOverMillis());
		flightPriceRQ.getAvailPreferences().setExistingSegmentsList(flightSearch.getExistingFlightSegments());
		flightPriceRQ.getAvailPreferences().setModifiedSegmentsList(flightSearch.getModifiedFlightSegments());
		flightPriceRQ.getAvailPreferences().setInverseSegmentsList(flightSearch.getInverseSegmentsOfModifiedFlightSegment());
		flightPriceRQ.getAvailPreferences().setInboundFareModified(flightSearch.isInboundFareQuote());
		flightPriceRQ.getAvailPreferences().setInverseFareID(flightSearch.getInverseFareID());

		return flightPriceRQ;
	}

	public static FlightPriceRQ createFareReQuoteRQ(FlightSearchDTO flightSearch, boolean isNameChangeRequote)
			throws ModuleException {

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		// setting origin destination information

		// Map to flight seg code & flight seg Id key value for populating cabin class & logical cabin class selection
		// Map<String, Integer> selectedFlightSegMap = new HashMap<String, Integer>();
		int ondSequence = 0;
		if (AppSysParamsUtil.isSkipNonModifiedSegFareONDs()) {
			List<String> excludedSegFarePnrSegIds = new ArrayList<String>();
			List<ONDSearchDTO> removeOndList = new ArrayList<ONDSearchDTO>();
			for (ONDSearchDTO ond : flightSearch.getOndList()) {
				try {
					if (ond.getModifiedResSegList() == null
							&& (ond.getExistingResSegRPHList() != null && !ond.getExistingResSegRPHList().isEmpty()
									&& ond.getOldPerPaxFareTOList() != null && FareTypeCodes.SEGMENT
										.equalsIgnoreCase(SelectListGenerator.getFareType(ond.getExistingResSegRPHList().get(0),
												Integer.toString(ond.getOldPerPaxFareTOList().get(0).getFareId()))))) {
						excludedSegFarePnrSegIds.addAll(ond.getExistingResSegRPHList());
						removeOndList.add(ond);
					}
				} catch (ModuleException e) {
					// // Continue if the fare id is empty.
					e.printStackTrace();
				}
			}
			if (!removeOndList.isEmpty()) {
				flightSearch.getOndList().removeAll(removeOndList);
			}
			flightSearch.setExcludedSegFarePnrSegIds(excludedSegFarePnrSegIds);
		}

		List<Integer> unTouchedOndList = new ArrayList<Integer>();
		List<String> unTouchedFltRPHList = new ArrayList<String>();
		List<String> sameFareRequired = new ArrayList<String>();
		Map<Integer, List<Integer>> existRetGropMap = new HashMap<Integer, List<Integer>>();

		if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs() && !isNameChangeRequote) {
			int seq = 0;
			List<Integer> skipOndList = ReservationBeanUtil.getSkipFareQuoteOnD(flightSearch.getOndList(), unTouchedOndList,
					unTouchedFltRPHList);
			for (ONDSearchDTO ond : flightSearch.getOndList()) {
				if (skipOndList.contains(seq)) {
					sameFareRequired.addAll(ond.getFlightRPHList());
				}
				seq++;
			}
		}

		for (ONDSearchDTO ond : flightSearch.getOndList()) {
			OriginDestinationInformationTO ondInfoTO = flightPriceRQ.addNewOriginDestinationInformation();
			ondInfoTO.setOrigin(ond.getFromAirport());
			ondInfoTO.setDestination(ond.getToAirport());

			Date depatureDate = ond.getDepartureDate();

			ReservationBeanUtil.validateDateVarience(depatureDate, ond.getDepartureVariance(), AppIndicatorEnum.APP_IBE);

			Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
			depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, ond.getDepartureVariance() * -1);

			Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
			depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, ond.getDepartureVariance());

			ondInfoTO.setPreferredDate(depatureDate);
			ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
			ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);
			ondInfoTO.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(ondSequence));
			ondInfoTO.setPreferredClassOfService(flightSearch.getPreferredClassOfService(ondSequence));
			ondInfoTO.setPreferredBookingClass(flightSearch.getPreferredBookingClass(ondSequence));
			ondInfoTO.setPreferredBookingType(flightSearch.getPreferredBookingType(ondSequence));

			if (ond.getBundledServicePeriodId() != null) {
				ondInfoTO.setPreferredBundleFarePeriodId(ond.getBundledServicePeriodId());
			} else {
				ondInfoTO.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(ondSequence));
			}

			ondInfoTO.setSameFlightModification(ond.isSameFlightModification());

			// Skip this if name change to
			if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs()) {

				if (ond.getOldPerPaxFareTOList() != null) {
					ondInfoTO.setOldPerPaxFareTOList(ond.getOldPerPaxFareTOList());
				}

				if (unTouchedFltRPHList.containsAll(ond.getFlightRPHList())
						&& sameFareRequired.containsAll(ond.getFlightRPHList())) {

					if (ond.getExistingResSegRPHList() != null && ond.getExistingResSegRPHList().size() > 0) {
						List<Integer> flightSegmentIds = new ArrayList<Integer>();
						ondInfoTO.setUnTouchedResSegList(ond.getExistingResSegRPHList());

						ondInfoTO.setUnTouchedOnd(true);

						FareTO oldPerPaxFareTO = ReservationBeanUtil.getOwnOldPerPaxFareTO(ond);

						if (oldPerPaxFareTO != null && oldPerPaxFareTO.getReturnGroupId() > 0) {
							int returnGrpId = oldPerPaxFareTO.getReturnGroupId();
							List<Integer> pnrSegIdList = new ArrayList<Integer>();

							for (String flightRefNumber : ond.getExistingFlightRPHList()) {
								if (flightRefNumber.indexOf("#") != -1) {
									String arr[] = flightRefNumber.split("#");
									flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]));
								} else {
									flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNumber));
								}
							}

							if (existRetGropMap.get(returnGrpId) != null) {
								pnrSegIdList = existRetGropMap.get(returnGrpId);
							}
							pnrSegIdList.addAll(flightSegmentIds);

							existRetGropMap.put(returnGrpId, pnrSegIdList);
						}

					}

				}

			}

			if (AppSysParamsUtil.isSameFareModificationEnabled() || AppSysParamsUtil.isEnforceSameHigher()) {
				if (ond.getOldPerPaxFareTOList() != null) {
					ondInfoTO.setOldPerPaxFareTOList(ond.getOldPerPaxFareTOList());
					if(isNameChangeRequote){
						ondInfoTO.setSameFlightModification(true);
					}
				}
				ondInfoTO.setEligibleToSameBCMod(ond.isEligibleToSameBCMod());

				if (ond.getDateChangedResSegList() != null) {
					List<Integer> dateChangedPnrSegIds = new ArrayList<Integer>();
					for (String dateChangedPnrSegIdStr : ond.getDateChangedResSegList()) {
						if (dateChangedPnrSegIdStr != null) {
							dateChangedPnrSegIds.add(new Integer(dateChangedPnrSegIdStr));
						}
					}
					ondInfoTO.setDateChangedResSegList(dateChangedPnrSegIds);
				}
			}

			OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
			if (ond.getFlightRPHList() != null) {
				for (String flightRefNumber : ond.getFlightRPHList()) {
					FlightSegmentTO flightSegementTO = new FlightSegmentTO();
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegementTO.setFlightRefNumber(arr[0]);
						flightSegementTO.setRouteRefNumber(arr[1]);
						flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(arr[0]));
						flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(arr[0]));
					} else {
						flightSegementTO.setFlightRefNumber(flightRefNumber);
						flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightRefNumber));
						flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(flightRefNumber));
					}
					depOndOptionTO.getFlightSegmentList().add(flightSegementTO);
					// selectedFlightSegMap.put(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRefNo),
					// FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo));
				}
			}

			if (ond.getExistingFlightRPHList() != null) {
				List<Integer> flightSegmentIds = new ArrayList<Integer>();
				for (String flightRefNumber : ond.getExistingFlightRPHList()) {
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]));
					} else {
						flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNumber));
					}
				}
				ondInfoTO.getExistingFlightSegIds().addAll(flightSegmentIds);
			}

			if (ond.getExistingResSegRPHList() != null) {
				ondInfoTO.getExistingPnrSegRPHs().addAll(ond.getExistingResSegRPHList());
			}

			ondInfoTO.setFlownOnd(ond.isFlownOnd());

			ondInfoTO.getOrignDestinationOptions().add(depOndOptionTO);
			ondSequence++;

		}

		// setClassOfServiceSelection(flightPriceRQ.getClassOfServicePrefTO(), flightSearch.getClassOfService(),
		// flightSearch.getLogicalCabinClass(), flightSearch.getFareQuoteLogicalCCSelection(), selectedFlightSegMap);

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(flightSearch.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(flightSearch.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(flightSearch.getInfantCount());

		// setting traveler preferences
		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		travelerPref.setBookingType(flightSearch.getBookingType());
		travelerPref.setOpenReturn(flightSearch.getOpenReturn());
		travelerPref.setValidityId(flightSearch.getValidity());
		if (AppSysParamsUtil.isBCSelectionAtAvailabilitySearchEnabled()
				&& StringUtils.isNotEmpty(flightSearch.getBookingClassCode())) {
			travelerPref.setBookingClassCode(flightSearch.getBookingClassCode());
		}
		travelerPref.setOpenReturnConfirm(flightSearch.isOpenReturnConfirm());

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		availPref.setBookingPaxType(flightSearch.getPaxType());
		availPref.setFareCategoryType(flightSearch.getFareType());
		availPref.setLastFareQuotedDate(flightSearch.getLastFareQuoteDate());
		availPref.setTicketValidTill(flightSearch.getTicketValidTill());
		availPref.setRequoteFlightSearch(true);
		availPref.setFromNameChange(isNameChangeRequote);

		availPref.setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());
		availPref.setStayOverTimeInMillis(setDefaultStayOverDaysForReturnFareSearch(flightPriceRQ));

		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.INT));
		} else {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.AA));
		}
		availPref.setAppIndicator(ApplicationEngine.IBE);

		overrideOndQuoteFlexi(flightSearch.getOndQuoteFlexi(), flightPriceRQ.getOrderedOriginDestinationInformationList(),
				flightSearch.isModifyFlexiSelected());

		availPref.setQuoteOndFlexi(flightSearch.getOndQuoteFlexi());
		availPref.setOndFlexiSelected(flightSearch.getOndSelectedFlexi());
		availPref.setTravelAgentCode(flightSearch.getTravelAgentCode());
		availPref.setPointOfSale(flightSearch.getPointOfSale());

		return flightPriceRQ;
	}

	private static void overrideOndQuoteFlexi(Map<Integer, Boolean> ondQuoteFlexi,
			List<OriginDestinationInformationTO> ondInformationList, boolean isFlexiSelectedForModifyingSegment) {
		if (isFlexiSelectedForModifyingSegment) {
			int ondSeq = 0;
			for (OriginDestinationInformationTO ondInformationTO : ondInformationList) {
				if (!ondInformationTO.isUnTouchedOnd()) {
					ondQuoteFlexi.put(ondSeq, false);
				}
				ondSeq++;
			}
		}
	}

	/**
	 * method to create AvailableFlightSearchDTO from FlightSearchDTO
	 * 
	 * @param cabinClassCode
	 * @param logicalCabinClass
	 * @param logicalCCSelection
	 * 
	 * @throws ParseException
	 */
	public static FlightPriceRQ createFareQuoteRQ(AvailableFlightSearchDTOBuilder flightSearchBldr,
			ArrayList<String> outFlightRPHList, ArrayList<String> retFlightRPHList, FlightSearchDTO flightSearchParam) {

		String cabinClassCode = flightSearchParam.getClassOfService();
		String logicalCabinClass = flightSearchParam.getLogicalCabinClass();
		String logicalCCSelection = flightSearchParam.getFareQuoteLogicalCCSelection();
		String outBoundBundlefareID = flightSearchParam.getPreferredBundledFares();
		AvailableFlightSearchDTO flightSearch = flightSearchBldr.getSearchDTO();
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();

		// setting origin destination information
		OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
		depatureOnD.setOrigin(flightSearch.getFromAirport());
		depatureOnD.setDestination(flightSearch.getToAirport());

		depatureOnD.setDepartureDateTimeStart(flightSearch.getDepatureDateTimeStart());
		depatureOnD.setDepartureDateTimeEnd(flightSearch.getDepatureDateTimeEnd());
		depatureOnD.setPreferredDate(flightSearchBldr.getDepartureDate());
		depatureOnD.setPreferredBookingType(flightSearch.getBookingType());
		if (flightSearchParam.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND) != null)
			depatureOnD.setPreferredBundleFarePeriodId(flightSearchParam.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND));
		if (flightSearchParam.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND) != null)
			depatureOnD.setPreferredBundleFarePeriodId(flightSearchParam.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND));
		if (flightSearchParam.getPreferredBookingClass(OndSequence.OUT_BOUND) != null)
			depatureOnD.setPreferredBookingClass(flightSearchParam.getPreferredBookingClass(OndSequence.OUT_BOUND));

		// Map to flight seg code & flight seg Id key value for populating cabin class & logical cabin class selection
		Map<String, Integer> selectedFlightSegMap = new HashMap<String, Integer>();
		Map<String, String> segWiseLogicalCC = ReservationBeanUtil.getSegmentWiseLogicalCC(logicalCCSelection);

		OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
		if (outFlightRPHList != null) {
			String segLogicalCabin = null;
			for (String flightRefNumber : outFlightRPHList) {
				FlightSegmentTO flightSegementTO = new FlightSegmentTO();
				String fltRefNo = "";
				if (flightRefNumber.indexOf("#") != -1) {
					String arr[] = flightRefNumber.split("#");
					flightSegementTO.setFlightRefNumber(arr[0]);
					flightSegementTO.setRouteRefNumber(arr[1]);
					fltRefNo = arr[0];
				} else {
					flightSegementTO.setFlightRefNumber(flightRefNumber);
					fltRefNo = flightRefNumber;
				}
				String segCode = FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRefNo);
				flightSegementTO.setSegmentCode(segCode);
				depOndOptionTO.getFlightSegmentList().add(flightSegementTO);
				selectedFlightSegMap.put(segCode, FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo));
				if (segLogicalCabin == null && segWiseLogicalCC.containsKey(segCode)) {
					segLogicalCabin = segWiseLogicalCC.get(segCode);
				}
			}
			if (segLogicalCabin != null) {
				depatureOnD.setPreferredLogicalCabin(segLogicalCabin);
			} else {
				depatureOnD.setPreferredLogicalCabin(logicalCabinClass);
			}
		}

		depatureOnD.setPreferredClassOfService(cabinClassCode);
		depatureOnD.getOrignDestinationOptions().add(depOndOptionTO);

		if (flightSearch.isReturnFlag()) {

			OriginDestinationInformationTO returnOnD = flightPriceRQ.addNewOriginDestinationInformation();
			returnOnD.setOrigin(flightSearch.getToAirport());
			returnOnD.setDestination(flightSearch.getFromAirport());

			returnOnD.setDepartureDateTimeStart(flightSearch.getReturnDateTimeStart());
			returnOnD.setDepartureDateTimeEnd(flightSearch.getReturnDateTimeEnd());
			returnOnD.setPreferredDate(flightSearchBldr.getReturnDate());
			returnOnD.setPreferredBookingType(flightSearch.getBookingType());
			if (flightSearchParam.getPreferredBundledFarePeriodId(OndSequence.IN_BOUND) != null)
				returnOnD.setPreferredBundleFarePeriodId(flightSearchParam.getPreferredBundledFarePeriodId(OndSequence.IN_BOUND));
			if (flightSearchParam.getPreferredBookingClass(OndSequence.IN_BOUND) != null)
				returnOnD.setPreferredBookingClass(flightSearchParam.getPreferredBookingClass(OndSequence.IN_BOUND));

			OriginDestinationOptionTO retOndOptionTO = new OriginDestinationOptionTO();
			if (outFlightRPHList != null) {
				String segLogicalCabin = null;
				for (String flightRefNumber : retFlightRPHList) {
					FlightSegmentTO flightSegementTO = new FlightSegmentTO();
					String fltRefNo = "";
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegementTO.setFlightRefNumber(arr[0]);
						flightSegementTO.setRouteRefNumber(arr[1]);
						fltRefNo = arr[0];
					} else {
						flightSegementTO.setFlightRefNumber(flightRefNumber);
						fltRefNo = flightRefNumber;
					}
					retOndOptionTO.getFlightSegmentList().add(flightSegementTO);
					String segCode = FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRefNo);
					flightSegementTO.setSegmentCode(segCode);
					selectedFlightSegMap.put(segCode, FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo));
					if (segLogicalCabin == null && segWiseLogicalCC.containsKey(segCode)) {
						segLogicalCabin = segWiseLogicalCC.get(segCode);
					}
				}
				if (segLogicalCabin != null) {
					returnOnD.setPreferredLogicalCabin(segLogicalCabin);
				} else {
					returnOnD.setPreferredLogicalCabin(logicalCabinClass);
				}
			}

			returnOnD.setPreferredClassOfService(cabinClassCode);
			returnOnD.getOrignDestinationOptions().add(retOndOptionTO);
			returnOnD.setReturnFlag(true);
		}

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(flightSearch.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(flightSearch.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(flightSearch.getInfantCount());

		flightPriceRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.IBE);
		flightPriceRQ.getAvailPreferences().setFareCalendarSearch(true);
		// Same Fare Or HigerFare
		flightPriceRQ.getAvailPreferences().setOldFareAmount(flightSearch.getModifiedOndPaxFareAmount());
		flightPriceRQ.getAvailPreferences().setOldFareID(
				flightSearch.getModifiedOndFareId() != null ? flightSearch.getModifiedOndFareId() : null);
		flightPriceRQ.getAvailPreferences().setOldFareType(
				flightSearch.getModifiedOndFareType() != null ? flightSearch.getModifiedOndFareType() : null);

		if (AppSysParamsUtil.isIbeAddPaxTypeEnable() && flightSearch.getBookingPaxType() != null
				&& !"".equals(flightSearch.getBookingPaxType()) && !flightSearch.getBookingPaxType().equals(BookingPaxType.ANY)) {
			flightPriceRQ.getAvailPreferences().setBookingPaxType(flightSearch.getBookingPaxType());
		}
		// Half return parameters
		flightPriceRQ.getAvailPreferences().setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());
		flightPriceRQ.getAvailPreferences().setStayOverTimeInMillis(flightSearch.getStayOverMillis());
		flightPriceRQ.getAvailPreferences().setExistingSegmentsList(flightSearch.getExistingFlightSegments());
		flightPriceRQ.getAvailPreferences().setModifiedSegmentsList(flightSearch.getModifiedFlightSegments());
		flightPriceRQ.getAvailPreferences().setInverseSegmentsList(flightSearch.getInverseSegmentsOfModifiedFlightSegment());
		flightPriceRQ.getAvailPreferences().setInboundFareModified(flightSearch.isInboundFareQuote());
		flightPriceRQ.getAvailPreferences().setInverseFareID(flightSearch.getInverseFareID());
		flightPriceRQ.getAvailPreferences().setQuoteOndFlexi(flightSearch.getOndQuoteFlexi());
		flightPriceRQ.getAvailPreferences().setPromoCode(flightSearch.getPromoCode());
		flightPriceRQ.getAvailPreferences().setBankIdentificationNumber(flightSearch.getBankIdentificationNo());
		flightPriceRQ.getAvailPreferences().setFlightDateFareMap(flightSearch.getFlightDateFareMap());
		flightPriceRQ.getAvailPreferences().setPointOfSale(flightSearch.getPointOfSale());

		return flightPriceRQ;
	}

	public static Object[] createFlightLists(Set<LCCClientReservationSegment> resFltSegs, Locale locale) {
		SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yy", locale);
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		Collection<SegInfoDTO> allSegs = new ArrayList<SegInfoDTO>();
		Collection<SegInfoDTO> outBSegs = new ArrayList<SegInfoDTO>();
		Collection<SegInfoDTO> inBSegs = new ArrayList<SegInfoDTO>();
		int seqNo = 1;
		int segmentCount = resFltSegs.size();
		for (LCCClientReservationSegment fltSeg : SortUtil.sort(resFltSegs)) {
			if (fltSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				SegInfoDTO segInfoDTO = new SegInfoDTO();
				segInfoDTO.setSegmentShortCode(fltSeg.getSegmentCode());
				if (segmentCount == 1) {
					segInfoDTO.setFlightSeqNo("");
					seqNo++;
				} else {
					segInfoDTO.setFlightSeqNo(seqNo++ + ". ");
				}
				// Set the segment code with terminal names.
				segInfoDTO.setSegmentCode(ReservationHG.getSegementDetailsWithTermainals(fltSeg.getSegmentCode(),
						fltSeg.getSubStationShortName(), fltSeg.getDepartureTerminalName(), fltSeg.getArrivalTerminalName()));
				segInfoDTO.setFlightNumber(fltSeg.getFlightNo());
				segInfoDTO.setCarrierCode(fltSeg.getCarrierCode());
				segInfoDTO.setDepartureDate(formatter.format(fltSeg.getDepartureDate()));
				segInfoDTO.setDepartureTime(timeFormatter.format(fltSeg.getDepartureDate()));
				segInfoDTO.setDepartureTimeZuluLong(fltSeg.getDepartureDateZulu().getTime());
				segInfoDTO.setArrivalDate(formatter.format(fltSeg.getArrivalDate()));
				segInfoDTO.setArrivalTime(timeFormatter.format(fltSeg.getArrivalDate()));
				segInfoDTO.setDepartureTimeLong(fltSeg.getDepartureDate().getTime());
				segInfoDTO.setArrivalTimeLong(fltSeg.getArrivalDate().getTime());
				segInfoDTO.setArrivalTimeZuluLong(fltSeg.getArrivalDateZulu().getTime());
				segInfoDTO.setFlightRefNumber(fltSeg.getFlightSegmentRefNumber());
				segInfoDTO.setSystem(fltSeg.getSystem().toString());
				segInfoDTO
						.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(fltSeg.getReturnFlag()));
				segInfoDTO.setDuration(CommonUtil.wrapWithHM(CommonUtil.getTimeHHMM(fltSeg.getArrivalDateZulu().getTime()
						- fltSeg.getDepartureDateZulu().getTime())));
				segInfoDTO.setArrivalTerminalName(fltSeg.getArrivalTerminalName());
				segInfoDTO.setDepartureTerminalName(fltSeg.getDepartureTerminalName());

				if (segInfoDTO.getReturnFlag()) {
					inBSegs.add(segInfoDTO);
				} else {
					outBSegs.add(segInfoDTO);
				}
			}
		}
		allSegs.addAll(outBSegs);
		allSegs.addAll(inBSegs);
		return new Object[] { allSegs, outBSegs, inBSegs };
	}

	public static Collection<SegInfoDTO> getFlightLists(Set<LCCClientReservationSegment> resFltSegs, Locale locale) {
		SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yy", locale);
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		final String dateValueFormater = "yyMMddHHmm";
		Collection<SegInfoDTO> allSegs = new ArrayList<SegInfoDTO>();
		int seqNo = 1;
		int segmentCount = resFltSegs.size();

		for (LCCClientReservationSegment fltSeg : SortUtil.sort(resFltSegs)) {
			if (fltSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				SegInfoDTO segInfoDTO = new SegInfoDTO();
				segInfoDTO.setSegmentShortCode(fltSeg.getSegmentCode());
				segInfoDTO.setSegmentId(fltSeg.getPnrSegID());
				if (segmentCount == 1) {
					segInfoDTO.setFlightSeqNo("");
					seqNo++;
				} else {
					segInfoDTO.setFlightSeqNo(seqNo++ + ". ");
				}
				// Set the segment code with terminal names.
				segInfoDTO.setSegmentCode(ReservationHG.getSegementDetailsWithTermainals(fltSeg.getSegmentCode(),
						fltSeg.getSubStationShortName(), fltSeg.getDepartureTerminalName(), fltSeg.getArrivalTerminalName()));
				segInfoDTO.setSegmentCodeOnly(fltSeg.getSegmentCode());
				segInfoDTO.setFlightNumber(fltSeg.getFlightNo());
				segInfoDTO.setCarrierCode(fltSeg.getCarrierCode());
				segInfoDTO.setDepartureDate(formatter.format(fltSeg.getDepartureDate()));
				segInfoDTO.setDepartureTime(timeFormatter.format(fltSeg.getDepartureDate()));
				segInfoDTO.setDepartureTimeZuluLong(fltSeg.getDepartureDateZulu().getTime());
				segInfoDTO.setArrivalDate(formatter.format(fltSeg.getArrivalDate()));
				segInfoDTO.setArrivalTime(timeFormatter.format(fltSeg.getArrivalDate()));
				segInfoDTO.setArrivalDateValue(DateUtil.formatDate(fltSeg.getArrivalDate(), dateValueFormater));
				segInfoDTO.setDepartureDateValue(DateUtil.formatDate(fltSeg.getDepartureDate(), dateValueFormater));
				segInfoDTO.setDepartureTimeLong(fltSeg.getDepartureDate().getTime());
				segInfoDTO.setArrivalTimeLong(fltSeg.getArrivalDate().getTime());
				segInfoDTO.setArrivalTimeZuluLong(fltSeg.getArrivalDateZulu().getTime());
				segInfoDTO.setFlightRefNumber(fltSeg.getFlightSegmentRefNumber());
				segInfoDTO.setSystem(fltSeg.getSystem().toString());
				segInfoDTO
						.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(fltSeg.getReturnFlag()));
				segInfoDTO.setJourneySequence(fltSeg.getJourneySequence());
				segInfoDTO.setDuration(CommonUtil.wrapWithHM(CommonUtil.getTimeHHMM(fltSeg.getArrivalDateZulu().getTime()
						- fltSeg.getDepartureDateZulu().getTime())));
				segInfoDTO.setArrivalTerminalName(fltSeg.getArrivalTerminalName());
				segInfoDTO.setDepartureTerminalName(fltSeg.getDepartureTerminalName());
				allSegs.add(segInfoDTO);
			}
		}
		return allSegs;
	}

	public static Object[] getOutboundInboundList(Set<LCCClientReservationSegment> flightSegmentTOs, boolean mergeToOutbound) {
		LCCClientReservationSegment[] clientReservationSegments = SortUtil.sortByDepDate(flightSegmentTOs);

		Set<LCCClientReservationSegment> outboundFlightSegmentTOs = new HashSet<LCCClientReservationSegment>();
		Set<LCCClientReservationSegment> inboundFlightSegmentTOs = new HashSet<LCCClientReservationSegment>();

		for (LCCClientReservationSegment flightSegmentTO : clientReservationSegments) {
			if (!ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(flightSegmentTO.getReturnFlag())) {
				outboundFlightSegmentTOs.add(flightSegmentTO);
			} else {
				if (mergeToOutbound) {
					outboundFlightSegmentTOs.add(flightSegmentTO);
				} else {
					inboundFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		}
		return new Object[] { outboundFlightSegmentTOs, inboundFlightSegmentTOs };
	}

	public static List<FareRuleDTO> getFareRules(PriceInfoTO priceInfoTO, List<OriginDestinationInformationTO> ondList) {
		List<FareRuleDTO> fareRuleDTOs = new ArrayList<FareRuleDTO>();

		fareRuleDTOs = com.isa.thinair.webplatform.api.util.ReservationUtil.getMergedFareRuleDTOs(priceInfoTO.getFareTypeTO()
				.getApplicableFareRules());
		com.isa.thinair.webplatform.api.util.ReservationUtil.injectDepartureDateToFareRules(fareRuleDTOs, ondList);

		return fareRuleDTOs;
	}

	public static Date getSelectedFlightDepartureDate(List<OriginDestinationOptionTO> ondOptions) {
		Date deptDate = null;
		for (OriginDestinationOptionTO ondTO : ondOptions) {
			if (ondTO.isSelected()) {
				Date segDate = null;
				for (IFlightSegment fltSeg : ondTO.getFlightSegmentList()) {
					if (segDate == null) {
						segDate = fltSeg.getDepartureDateTimeZulu();
					} else {
						if (segDate.after(fltSeg.getDepartureDateTimeZulu())) {
							segDate = fltSeg.getDepartureDateTimeZulu();
						}
					}
				}
				deptDate = segDate;
				break;
			}
		}

		return deptDate;
	}

	public static String getTotalPriceWithoutExtCharges(PriceInfoTO priceInfoTO) {
		if (priceInfoTO != null) {
			FareTypeTO fareTypeTO = priceInfoTO.getFareTypeTO();
			String totalPriceWithoutExtCharges = AccelAeroCalculator.getDefaultBigDecimalZero().toString();
			if (fareTypeTO != null) {
				return fareTypeTO.getTotalPrice().toString();
			}

			return totalPriceWithoutExtCharges;
		} else {
			return AccelAeroCalculator.getDefaultBigDecimalZero().toString();
		}
	}

	private static long setDefaultStayOverDaysForReturnFareSearch(BaseAvailRQ baseAvailRQ) {
		long stayOverTimeBetweenRequestedDates = 0;
		OndConvertAssembler ondAssm = new OndConvertAssembler(baseAvailRQ.getOriginDestinationInformationList());
		if (ondAssm.isReturnFlight() && !baseAvailRQ.getTravelPreferences().isOpenReturn()) {
			stayOverTimeBetweenRequestedDates = ondAssm.getSelectedInboundDateTimeEnd().getTime()
					- ondAssm.getSelectedOutboundDateTimeStart().getTime();
		}
		return stayOverTimeBetweenRequestedDates;
	}

	public static FareQuoteTO buildFareTypeInfo(FareTypeTO fareTypeTO, List<OriginDestinationInformationTO> ondList,
			BigDecimal handlingCharge, FlightSearchDTO searchParams, BigDecimal fareDiscountAmount,
			ExchangeRateProxy exchangeRateProxy, Collection<AvailableFlightInfo> flightInfo) throws ParseException,
			ModuleException {

		int ondSequence = 0;
		List<String> selectedOndCodes = new ArrayList<String>();
		List<Set<String>> selectedSegCodes = new ArrayList<Set<String>>();
		Map<String, Long> segmentDeptTimeMap = new HashMap<String, Long>();

		for (OriginDestinationInformationTO ondInfo : ondList) {
			for (OriginDestinationOptionTO ondOpt : ondInfo.getOrignDestinationOptions()) {
				if (ondOpt.isSelected()) {
					Set<String> selectedONDSegCodes = new HashSet<String>();
					for (IFlightSegment flightSegmentTO : ondOpt.getFlightSegmentList()) {
						selectedONDSegCodes.add(flightSegmentTO.getSegmentCode());
						segmentDeptTimeMap
								.put(flightSegmentTO.getSegmentCode(), flightSegmentTO.getDepartureDateTime().getTime());
					}

					selectedOndCodes.add(ondSequence, ondOpt.getOndCode());
					selectedSegCodes.add(ondSequence, selectedONDSegCodes);
					break;
				}
			}
			ondSequence++;
		}

		return buildFareTypeInfo(fareTypeTO, segmentDeptTimeMap, handlingCharge, searchParams, selectedOndCodes,
				selectedSegCodes, fareDiscountAmount, exchangeRateProxy, flightInfo);
	}

	public static List<SegInfoDTO> resolveFlightLists(Set<LCCClientReservationSegment> resFltSegs, Locale locale) {
		SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yy", locale);
		SimpleDateFormat smpdtDateValue = new SimpleDateFormat("yyMMddHHmm");
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		List<SegInfoDTO> allSegs = new ArrayList<SegInfoDTO>();
		int seqNo = 1;
		int segmentCount = resFltSegs.size();

		for (LCCClientReservationSegment fltSeg : SortUtil.sort(resFltSegs)) {
			if (fltSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				SegInfoDTO segInfoDTO = new SegInfoDTO();
				segInfoDTO.setSegmentShortCode(fltSeg.getSegmentCode());
				segInfoDTO.setSegmentId(fltSeg.getPnrSegID());
				if (segmentCount == 1) {
					segInfoDTO.setFlightSeqNo("");
					seqNo++;
				} else {
					segInfoDTO.setFlightSeqNo(seqNo++ + ". ");
				}
				// Set the segment code with terminal names.
				segInfoDTO.setSegmentCode(ReservationHG.getSegementDetailsWithTermainals(fltSeg.getSegmentCode(),
						fltSeg.getSubStationShortName(), fltSeg.getDepartureTerminalName(), fltSeg.getArrivalTerminalName()));
				segInfoDTO.setFlightNumber(fltSeg.getFlightNo());
				segInfoDTO.setCarrierCode(fltSeg.getCarrierCode());
				segInfoDTO.setDepartureDate(formatter.format(fltSeg.getDepartureDate()));
				segInfoDTO.setDepartureDateValue(smpdtDateValue.format(fltSeg.getDepartureDate()));
				segInfoDTO.setDepartureTime(timeFormatter.format(fltSeg.getDepartureDate()));
				segInfoDTO.setDepartureTimeZuluLong(fltSeg.getDepartureDateZulu().getTime());
				segInfoDTO.setArrivalDate(formatter.format(fltSeg.getArrivalDate()));
				segInfoDTO.setArrivalDateValue(smpdtDateValue.format(fltSeg.getArrivalDate()));
				segInfoDTO.setArrivalTime(timeFormatter.format(fltSeg.getArrivalDate()));
				segInfoDTO.setDepartureTimeLong(fltSeg.getDepartureDate().getTime());
				segInfoDTO.setArrivalTimeLong(fltSeg.getArrivalDate().getTime());
				segInfoDTO.setArrivalTimeZuluLong(fltSeg.getArrivalDateZulu().getTime());
				segInfoDTO.setFlightRefNumber(fltSeg.getFlightSegmentRefNumber());
				segInfoDTO.setSystem(fltSeg.getSystem().toString());
				segInfoDTO
						.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(fltSeg.getReturnFlag()));
				segInfoDTO.setDuration(CommonUtil.wrapWithHM(CommonUtil.getTimeHHMM(fltSeg.getArrivalDateZulu().getTime()
						- fltSeg.getDepartureDateZulu().getTime())));
				segInfoDTO.setArrivalTerminalName(fltSeg.getArrivalTerminalName());
				segInfoDTO.setDepartureTerminalName(fltSeg.getDepartureTerminalName());
				allSegs.add(segInfoDTO);
			}
		}
		return allSegs;
	}

	public static FareQuoteTO buildFareTypeInfo(FareTypeTO fareTypeTO, Map<String, Long> segmentDepartureTimeMap,
			BigDecimal handlingCharge, FlightSearchDTO searchParams, List<String> ondCodeList,
			List<Set<String>> selectedOndSegCodes, BigDecimal fareDiscountAmount, ExchangeRateProxy exchangeRateProxy,
			Collection<AvailableFlightInfo> flightInfo) throws ParseException, ModuleException {

		String selectedCurrency = searchParams.getSelectedCurrency();

		CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency, ApplicationEngine.IBE);

		FareQuoteTO fareQuote = new FareQuoteTO();

		List<com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO> fareQuoteTO = new ArrayList<com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO>();
		List<PaxFareTO> paxFareTOs = new ArrayList<PaxFareTO>();
		List<PaxPriceTO> paxPriceTOs = new ArrayList<PaxPriceTO>();
		Collection<SurchargeTO> surcharges = new ArrayList<SurchargeTO>();
		Collection<TaxTO> taxes = new ArrayList<TaxTO>();
		List<Collection<ExternalChargeTO>> ondExternalCharges = new ArrayList<Collection<ExternalChargeTO>>();

		// fill fare details
		ReservationBeanUtil.fillFareQuote(fareQuoteTO, paxFareTOs, paxPriceTOs, surcharges, taxes, ondExternalCharges,
				fareTypeTO, ondCodeList, selectedOndSegCodes, selectedCurrency, exchangeRateProxy);

		String strCurrnecy = AppSysParamsUtil.getBaseCurrency();

		BigDecimal promoDiscountAmountForDeduction = AccelAeroCalculator.getDefaultBigDecimalZero();
		ApplicablePromotionDetailsTO promotionTO = fareTypeTO.getPromotionTO();
		if (promotionTO != null && fareDiscountAmount != null) {
			fareQuote.setHasPromoDiscount(true);
			if (PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY.equals(promotionTO.getDiscountAs())) {
				fareQuote.setDeductFromTotal(true);
				promoDiscountAmountForDeduction = fareDiscountAmount;
			}
		} else {
			fareDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		FareQuoteSummaryTO fareQuoteSummaryTO = ReservationBeanUtil.createFareQuoteSummary(fareTypeTO,
				fareTypeTO.isOnHoldRestricted(), strCurrnecy, selectedCurrency, fareQuoteTO, handlingCharge,
				promoDiscountAmountForDeduction);

		Collection<SurchargeFETO> perPaxSurchargeBreakDown = ReservationBeanUtil.convertToSurchargeFETO(surcharges,
				selectedCurrency, exchangeRateProxy);
		Collection<TaxFETO> perPaxTaxBreakDown = ReservationBeanUtil.convertToTaxFETO(taxes, selectedCurrency, exchangeRateProxy);

		Collection<SurchargeFETO> externalChargeTOs = new ArrayList<SurchargeFETO>();
		Collection<ExternalChargeTO> flexiCharges = new ArrayList<ExternalChargeTO>();
		for (Collection<ExternalChargeTO> ondExtObj : ondExternalCharges) {
			externalChargeTOs.addAll(ReservationBeanUtil.convertExternalChargeTOToSurchargeFETO(ondExtObj,
					searchParams.getSelectedCurrency(), exchangeRateProxy));
			flexiCharges.addAll(ondExtObj);
		}

		// requote flow
		Collection<SegmentFareTO> segmentFareTOColl = new ArrayList<SegmentFareTO>();
		if (fareQuoteTO != null && fareQuoteTO.size() > 0) {
			Iterator<com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO> fareQuoteTOItr = fareQuoteTO.iterator();

			while (fareQuoteTOItr.hasNext()) {
				com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO fareQuoteTOPlat = fareQuoteTOItr.next();

				Iterator<PaxFareTO> paxFareDTOItr = fareQuoteTOPlat.getPaxWise().iterator();
				Collection<PaxFareTO> paxfareDto = new ArrayList<PaxFareTO>();
				while (paxFareDTOItr.hasNext()) {
					PaxFareTO paxFareDTOOrg = paxFareDTOItr.next();

					paxfareDto.add(copyPaxFareTOValues(selectedCurrency, exchangeRateProxy, paxFareDTOOrg));
				}
				segmentFareTOColl.add(copyToSegmentFareTo(fareQuoteTOPlat, exchangeRate, paxfareDto, flightInfo));

			}

		}

		Collection<SegmentFareTO> segmentFareToTempCol = new ArrayList<SegmentFareTO>();
		Iterator<SegmentFareTO> segFareToItr = segmentFareTOColl.iterator();
		ArrayList<String> segFareCodesList = new ArrayList<String>();
		boolean inboundSector = false;
		while (segFareToItr.hasNext()) {
			SegmentFareTO segFareTOTemp = segFareToItr.next();
			String segCode = segFareTOTemp.getSegmentCode();
			if(inboundSector)
				segFareTOTemp.setHasInbound(true);
			else
			{
			  if (contains(segFareCodesList, segCode)) {
				segFareTOTemp.setHasInbound(true);
				inboundSector = true;
			  } else {			
			    segFareTOTemp.setHasInbound(false);
			  }
			}
			String totalPrice = segFareTOTemp.getTotalPrice();
			String totalPriceInSelCurr = segFareTOTemp.getTotalPriceInSelectedCurr();
			String paxWiseTotalPrice = getPaxwiseTotalPrice(segFareTOTemp.getPaxWise());
			String paxWiseTotalPriceSelCurr = getPaxwiseTotalPriceInSelCurr(segFareTOTemp.getPaxWise());
			if(!totalPrice.equalsIgnoreCase(paxWiseTotalPrice) || !totalPriceInSelCurr.equalsIgnoreCase(paxWiseTotalPriceSelCurr))
			{
				totalPrice = paxWiseTotalPrice;
				segFareTOTemp.setTotalPrice(paxWiseTotalPrice);
				segFareTOTemp.setTotalPriceInSelectedCurr(paxWiseTotalPriceSelCurr);
			}
			segFareCodesList.add(segCode);
			segmentFareToTempCol.add(segFareTOTemp);
		}

		fareQuote.setTotalIbePromoDiscount(AccelAeroCalculator.formatAsDecimal(fareDiscountAmount));

		fareQuote.setTotalFare(fareQuoteSummaryTO.getBaseFare());
		fareQuote.setTotalTax(fareQuoteSummaryTO.getTotalTaxes());

		BigDecimalWrapper totatTaxSurCharges = new BigDecimalWrapper();
		totatTaxSurCharges.add(new BigDecimal(fareQuoteSummaryTO.getTotalTaxes()));
		totatTaxSurCharges.add(new BigDecimal(fareQuoteSummaryTO.getTotalSurcharges()));

		fareQuote.setTotalTaxSurcharge(totatTaxSurCharges.getDisplayValue());

		fareQuote.setFareType(fareTypeTO.getFareType());
		fareQuote.setTotalPrice(fareQuoteSummaryTO.getTotalPrice());

		fareQuote.setSegmentFare(segmentFareToTempCol);
		fareQuote.setPaxWisePrice(paxPriceTOs);
		fareQuote.setHasFareQuote(true);
		fareQuote.setCurrency(AppSysParamsUtil.getBaseCurrency());
		fareQuote.setSelectedCurrency(selectedCurrency);
		if (!fareQuote.getCurrency().equals(selectedCurrency)) {
			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency,
					ApplicationEngine.IBE);
			if (currencyExchangeRate != null) {
				Currency currency = exchangeRate.getCurrency();
				boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
				BigDecimal boundryValue = null;
				BigDecimal breakPoint = null;
				if (isCurrencyRoundUpEnabled) {
					boundryValue = currency.getBoundryValue();
					breakPoint = currency.getBreakPoint();
				}
				fareQuote.setSelectedCurrency(selectedCurrency);
				fareQuote.setSelectedEXRate(currencyExchangeRate.getMultiplyingExchangeRate().toPlainString());
				fareQuote.setSelectedtotalPrice(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(fareQuote.getTotalPrice()),
						boundryValue, breakPoint)));
				fareQuote.setTotalSelectedIbePromoDiscount(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy
						.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), fareDiscountAmount, boundryValue,
								breakPoint)));

			}
		}

		// set the surcharge and tax breakdown - only if the application parameter is enabled.
		if (AppSysParamsUtil.isShowIBEChargesBreakdownEnabled()) {
			fareQuote.setPerPaxTaxBD(perPaxTaxBreakDown);
			fareQuote.setPerPaxSurchargeBD(perPaxSurchargeBreakDown);
		}

		return fareQuote;
	}

	private static SegmentFareTO copyToSegmentFareTo(com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO fareQuoteTo,
			CurrencyExchangeRate exchangeRate, Collection<PaxFareTO> paxFareDTOColl, Collection<AvailableFlightInfo> flightInfo) {

		List<com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO> segmentFareTOList = new ArrayList<com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO>(
				fareQuoteTo.getSegmentWise());
		SegmentFareTO segmentFareTo = new SegmentFareTO();
		com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO segmentFareToOrg = null;

		Map<String, String> segmentInfoMap = flightSegmentInfoMap(flightInfo);

		String segmentCodeFull = "";
		String segmentFareTotalPrice = "0";

		if (segmentFareTOList != null && segmentFareTOList.size() > 0) {
			segmentFareToOrg = segmentFareTOList.get(0);
			segmentFareTotalPrice = segmentFareToOrg.getTotalPrice();
		}

		// derive full segment code string
		String ondArray[] = fareQuoteTo.getFullOndCode().split("/");

		String firstSegmentCode = segmentInfoMap.get(ondArray[0]);
		String lastSegmentStr = segmentInfoMap.get(ondArray[ondArray.length - 1]);

		segmentCodeFull = firstSegmentCode + " / " + lastSegmentStr;

		segmentFareTo.setPaxWise(paxFareDTOColl);
		segmentFareTo.setSegmentCode(fareQuoteTo.getOndCode());
		segmentFareTo.setSegmentName(segmentCodeFull);
		segmentFareTo.setTotalPrice(segmentFareTotalPrice);

		// segmentFareTo.setTotalTaxSurcharges(AccelAeroCalculator.formatAsDecimal(taxes.getValue()));

		if (exchangeRate != null) {

			segmentFareTo.setTotalPriceInSelectedCurr(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
					new BigDecimal(segmentFareTotalPrice), exchangeRate.getMultiplyingExchangeRate())));
			// segmentFareTo.setTotalTaxSurchargesInSelectedCurr(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
			// taxes.getValue(), exchangeRate.getMultiplyingExchangeRate())));
		}

		return segmentFareTo;
	}

	private static PaxFareTO copyPaxFareTOValues(String selCurrency, ExchangeRateProxy exchangeRateProxy, PaxFareTO paxFareTo) {

		// Set PaxFareTO for Child & Children charges
		String paxTypeDescription = PaxTypeTO.ADULT;

		if (paxFareTo.getPaxType() != null) {
			if (paxFareTo.getPaxType().toUpperCase().startsWith(PaxTypeTO.CHILD)) {
				paxTypeDescription = PaxTypeTO.CHILD;
			} else if (paxFareTo.getPaxType().toUpperCase().startsWith(PaxTypeTO.INFANT)) {
				paxTypeDescription = PaxTypeTO.INFANT;
			} else {
				paxTypeDescription = PaxTypeTO.ADULT;
			}
		}

		paxFareTo.setPaxType(paxTypeDescription);

		// setting tax + surcharge as need ed in ibe
		BigDecimal totalTaxSurcharges = AccelAeroCalculator.add(new BigDecimal(paxFareTo.getTax()),
				new BigDecimal(paxFareTo.getSur()));
		paxFareTo.setSur(AccelAeroCalculator.formatAsDecimal(totalTaxSurcharges));

		// Geting the exchangeRate
		try {
			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency,
					ApplicationEngine.IBE);
			Currency currency = currencyExchangeRate.getCurrency();
			boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
			BigDecimal boundryValue = null;
			BigDecimal breakPoint = null;
			if (isCurrencyRoundUpEnabled) {
				boundryValue = (currency.getBoundryValue() != null) ? currency.getBoundryValue() : AppSysParamsUtil
						.getIBECalBoundry();
				breakPoint = (currency.getBreakPoint() != null) ? currency.getBreakPoint() : AppSysParamsUtil
						.getIBECalBreakpoint();
			}

			paxFareTo.setSelCurSur(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), totalTaxSurcharges, boundryValue, breakPoint)));
			paxFareTo.setSelCurTax(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(paxFareTo.getTax()), boundryValue,
					breakPoint)));
			paxFareTo.setSelCurFare(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(paxFareTo.getFare()), boundryValue,
					breakPoint)));

			paxFareTo.setSelCurTotalPaxFare(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(),
					AccelAeroCalculator.multiply(new BigDecimal(paxFareTo.getFare()), new BigDecimal(paxFareTo.getNoPax())),
					boundryValue, breakPoint)));

			paxFareTo.setSelCurtotal(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
					currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(paxFareTo.getPerPax()), boundryValue,
					breakPoint)));

		} catch (Exception e) {
			log.error(e);
		}

		return paxFareTo;
	}

	private static Map<String, String> flightSegmentInfoMap(Collection<AvailableFlightInfo> outboundFlights) {

		Map<String, String> segmentInfoMap = new HashMap<String, String>();

		for (AvailableFlightInfo flightInfo : outboundFlights) {
			for (SegInfoDTO segInfo : flightInfo.getSegments()) {
				String segShortCodeStr = segInfo.getSegmentShortCode();
				String segCodeStr = segInfo.getSegmentCode();

				if (segShortCodeStr != null && segCodeStr != null) {
					String ondShortStrArr[] = segShortCodeStr.split("/");
					String ondFullStrArr[] = segCodeStr.split("/");

					segmentInfoMap.put(ondShortStrArr[0], ondFullStrArr[0]);
					segmentInfoMap.put(ondShortStrArr[1], ondFullStrArr[1]);
				}

			}
		}
		return segmentInfoMap;
	}

	static boolean contains(ArrayList<String> list, String segmentShortCode) {
		String currentSource = segmentShortCode.split("/")[0];
		String currentDestination = segmentShortCode.split("/")[1];
		for (String shortCode : list) {
			String sourceFromList = shortCode.split("/")[0];
			String destinationFromList = shortCode.split("/")[1];
			// A to B
			if (destinationFromList.equalsIgnoreCase(currentSource)) {
				// Check if returning back to source B to A
				if (currentDestination.equalsIgnoreCase(sourceFromList))
					return true;
			} else if (destinationFromList.equalsIgnoreCase(currentDestination))
				return true;
		}
		return false;
	}

	static String getPaxwiseTotalPrice(Collection<PaxFareTO> collection)
	{
		BigDecimal totalPrice= AccelAeroCalculator.getDefaultBigDecimalZero();
		for(PaxFareTO paxFare:collection)
		{
			totalPrice = totalPrice.add(new BigDecimal(Double.parseDouble(paxFare.getTotal())));
		}
		return totalPrice.toPlainString();
	}
	
	static String getPaxwiseTotalPriceInSelCurr(Collection<PaxFareTO> collection)
	{
		BigDecimal totalPrice= AccelAeroCalculator.getDefaultBigDecimalZero();
		for(PaxFareTO paxFare:collection)
		{
			totalPrice = totalPrice.add(new BigDecimal(Double.parseDouble(paxFare.getSelCurtotal())));
		}
		return totalPrice.toPlainString();
	}
	
	private static String formatToJsonDateTime(Date utilDate) {
		String strDateformat = "yyyy-MM-dd'T'HH:mm:ss";
		String formatedDate = null;

		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(strDateformat);
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}

}
