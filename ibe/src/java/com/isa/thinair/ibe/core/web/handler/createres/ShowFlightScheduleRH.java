/*
 * ShowFlightScheduleRH.java
 * Cearted by : Haider Sahib 23July08
 * 
 */
package com.isa.thinair.ibe.core.web.handler.createres;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.ScheduleUtils;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

public class ShowFlightScheduleRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ShowFlightScheduleRH.class);
	private static GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
	private static final String strSMDateFormat = "dd/MM/yyyy";
	private static final String strSMDateYearFormat = "MM/dd/yyyy";

	private static final String defaultFormat // get the default date format
	= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);
	private static final String dayOffSet = "1";

	public static String execute(HttpServletRequest request) {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String s = getScheduleRowHtml(request);
			request.setAttribute("schedule", s);
			request.setAttribute("displayAdvancedScheduleDetails", IBEModuleUtils.getConfig().isDisplayAdvancedScheduleDetails());
		} catch (ModuleException me) {
			forward = StrutsConstants.Result.ERROR;
			log.error("ShowFlightScheduleRH.execute", me);
		} catch (RuntimeException re) {
			forward = StrutsConstants.Result.ERROR;
			log.error("ShowFlightScheduleRH.execute", re);
		}
		return forward;
	}

	/**
	 * Gets the Grid Array for the Flight Schedule default search is commented
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the Grid Array for Schedules
	 * @throws ModuleException
	 *             the ModuleException
	 */

	private static String getScheduleRowHtml(HttpServletRequest request) throws ModuleException {
		log.debug("inside ShowFlightScheduleRH.getScheduleRowHtml()");

		// set the not required parameters to defaults value
		Collection<Collection<FlightScheduleInfoDTO>> colSchedules = null;
		Collection<Collection<FlightScheduleInfoDTO>> colSchedulesReturns = null;
		StringBuffer sb = new StringBuffer();
		String strMode = "SEARCH";

		// fields relavant to both searchs (default/non-default)
		String strStartDate = "";
		String strStopDate = "";
		String strFrom = "";
		String strTo = "";

		Date dStartDate = null;
		Date dStopDate = null;
		String arFrom = "";
		String arTo = "";
		String arSHJ = "";
		String tmp = null;
		tmp = request.getParameter("arFrom");
		if (tmp != null)
			arFrom = getUnicode(tmp);
		tmp = request.getParameter("arTo");
		if (tmp != null)
			arTo = getUnicode(tmp);
		tmp = request.getParameter("arSHJ");
		if (tmp != null)
			arSHJ = getUnicode(tmp);

		String specialFltFromDate = request.getParameter("sffd");
		String specialFltToDate = request.getParameter("sftd");
		String specialFltNo = request.getParameter("sfno");
		String specialFltDest = request.getParameter("sfd");
		request.setAttribute("sffd", specialFltFromDate);
		request.setAttribute("sftd", specialFltToDate);
		request.setAttribute("sfno", specialFltNo);
		request.setAttribute("sfd", specialFltDest);

		String startDate = request.getParameter("startDate");
		strFrom = request.getParameter("fromDst");
		strTo = request.getParameter("toDst");
		String roundTrip = request.getParameter("roundTrip");
		String lang = request.getParameter("lang");
		Locale locale = new Locale(lang);
		Config.set(request.getSession(), Config.FMT_LOCALE, locale);
		if (startDate == null || strFrom == null || strTo == null || lang == null)
			return "";
		if (startDate.length() == 0 || strFrom.length() == 0 || strTo.length() == 0 || lang.length() == 0)
			return "";
		if (roundTrip == null)
			roundTrip = "false";

		if (strMode != null) {
			Hashtable<String, String> hash = new Hashtable<String, String>();
			hash.put("01", "31");
			hash.put("02", "28");
			hash.put("03", "31");
			hash.put("04", "30");
			hash.put("05", "31");
			hash.put("06", "30");
			hash.put("07", "31");
			hash.put("08", "31");
			hash.put("09", "30");
			hash.put("10", "31");
			hash.put("11", "30");
			hash.put("12", "31");

			String[] mon_year = StringUtils.split(startDate, "/");
			strStartDate = "01/" + startDate; // the date format recivied as mm/yyyy
			// strStopDate = request.getParameter("stopDate");
			int year = Integer.valueOf(mon_year[1]);
			String mon = hash.get(mon_year[0]);
			if (year % 4 == 0 && mon_year[1].equalsIgnoreCase("02"))
				mon = "29";
			strStopDate = mon + "/" + startDate;
			DateFormat formatter = new SimpleDateFormat(strSMDateFormat);

			try {
				dStartDate = formatter.parse(strStartDate);
				dStopDate = formatter.parse(strStopDate);
			} catch (Exception e) {
				log.error(e);
			}

			request.setAttribute("selStartDate", ScheduleUtils.formatDate(dStartDate, strSMDateYearFormat));
			request.setAttribute("selStopDate", ScheduleUtils.formatDate(dStopDate, strSMDateYearFormat));
			request.setAttribute("lang", lang);
			request.setAttribute("mainData", "var mainData = new Array();");

			int nData = 0;

			// Added for AARESAA-5190
			ScheduleAvailRS scheduleAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().getSchedulesList(
					createScheduleAvailRQ(strFrom, strTo, dStartDate, dStopDate, (roundTrip.equalsIgnoreCase("true")
							? true
							: false), arFrom, arTo, arSHJ), TrackInfoUtil.getBasicTrackInfo(request));

			colSchedules = getFlightSchedulesWhichHasActiveFlights(scheduleAvailRS.getFlightSchedules());
			colSchedulesReturns = getFlightSchedulesWhichHasActiveFlights(scheduleAvailRS.getFlightSchedulesReturns());

			// Added for AARESAA-5190 End
			List<Collection<FlightScheduleInfoDTO>> list = (List) colSchedules;
			int len = list.size();

			for (int j = 0; j < len; j++) {
				if (colSchedules.isEmpty())
					return "";
				String buf = createScheduleRowHTML((List) list.get(j), nData++, lang);
				sb.append(buf);
			}
			if (roundTrip.equalsIgnoreCase("true") && colSchedulesReturns != null && !colSchedulesReturns.isEmpty()) {
				List<Collection<FlightScheduleInfoDTO>> returnList = (List) colSchedulesReturns;
				len = returnList.size();
				for (int j = len - 1; j >= 0; j--) {
					if (colSchedules.isEmpty())
						return "";
					String buf = createScheduleRowHTML((List) returnList.get(j), nData++, lang);
					sb.append(buf);
				}
			}
		}
		log.debug("inside ScheduleHTMLGenerator.getScheduleRowHtml search values");
		return sb.toString();
	}

	private static Collection<Collection<FlightScheduleInfoDTO>> getFlightSchedulesWhichHasActiveFlights(
			Collection<Collection<FlightScheduleInfoDTO>> allSchedules) throws ModuleException {

		ArrayList<Collection<FlightScheduleInfoDTO>> schedulesWithActiveFlights = new ArrayList<Collection<FlightScheduleInfoDTO>>();
		if (allSchedules != null) {
			for (Collection<FlightScheduleInfoDTO> flightScheduleInfo : allSchedules) {
				Collection<FlightScheduleInfoDTO> colFlightScheduleInfoDTO = new ArrayList<FlightScheduleInfoDTO>();
				for (FlightScheduleInfoDTO flightScheduleInfoDTO : flightScheduleInfo) {
					int numberOfNonCanceledFlights = 0;
					if (flightScheduleInfoDTO.getScheduleId() != null) {
						numberOfNonCanceledFlights = ModuleServiceLocator.getFlightBD().getNumberOfNonCanceledFlights(
								flightScheduleInfoDTO.getScheduleId());
						if (numberOfNonCanceledFlights > 0) {
							colFlightScheduleInfoDTO.add(flightScheduleInfoDTO);
						}
					}
					else{
						colFlightScheduleInfoDTO.add(flightScheduleInfoDTO);
					}
				}
				schedulesWithActiveFlights.add(colFlightScheduleInfoDTO);
			}
		}
		return schedulesWithActiveFlights;
	}

	private static ScheduleAvailRQ createScheduleAvailRQ(String strFrom, String strTo, Date dStartDate, Date dStopDate,
			boolean roundTrip, String arFrom, String arTo, String arSHJ) {
		ScheduleAvailRQ scheduleAvailRQ = new ScheduleAvailRQ();

		scheduleAvailRQ.setOriginLocation(strFrom);
		scheduleAvailRQ.setDestLocation(strTo);
		scheduleAvailRQ.setStartDateTime(dStartDate);
		scheduleAvailRQ.setStopDateTime(dStopDate);
		scheduleAvailRQ.setRoundTrip(roundTrip);
		scheduleAvailRQ.setArFrom(arFrom);
		scheduleAvailRQ.setArTo(arTo);
		scheduleAvailRQ.setArSHJ(arSHJ);
		scheduleAvailRQ.setDefaultCarrierCode(AppParamUtil.getDefaultCarrier());
		scheduleAvailRQ.setSystemHub(globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM));
		scheduleAvailRQ.setAAServiceRequest(false);

		return scheduleAvailRQ;
	}

	private static String createScheduleRowHTML(List<FlightScheduleInfoDTO> flightSchedules, int nData, String lang)
			throws ModuleException {

		StringBuffer sb = new StringBuffer();
		int len = flightSchedules.size();
		if (flightSchedules != null && flightSchedules.size() > 0) {
			List list = (List) flightSchedules;
			Object[] listArr = list.toArray();
			String dstFrom = null;
			String dstTo = null;
			FlightScheduleInfoDTO flightSchedule = null;
			sb.append("var arrData = new Array();");
			for (int i = 0; i < listArr.length; i++) {
				flightSchedule = (FlightScheduleInfoDTO) listArr[i];

				String strStart = "";
				String strStop = "";
				Frequency frq = null;
				Frequency frqLocal = null;
				strStart = ScheduleUtils.formatDate(flightSchedule.getStartDateLocal(), strSMDateYearFormat);
				strStop = ScheduleUtils.formatDate(flightSchedule.getStopDateLocal(), strSMDateYearFormat);
				frqLocal = flightSchedule.getFrequencyLocal();
				frq = flightSchedule.getFrequencyLocal();

				sb.append("arrData[" + i + "] = new Array();");
				sb.append("arrData[" + i + "][0] = '" + i + "';");
				sb.append("arrData[" + i + "][1] = '" + getNotNullString(flightSchedule.getFlightNumber()) + "';");
				sb.append("arrData[" + i + "][30] = '" + strStart + "';");
				sb.append("arrData[" + i + "][31] = '" + strStop + "';");

				String strFrequency = createSegmentArray(frq);

				HashMap daysLocalMap = createDayOfWeekMap(frqLocal);

				sb.append("arrData[" + i + "][4] = '" + strFrequency + "';");

				String strSegmentDtls = "";// all legs;
				String legArr = "var arrLegDtls = new Array();";// leg detail array
				String legArrZulu = "var arrLegZuluDtls = new Array();";// leg detail array
				Collection segDetails = flightSchedule.getFlightScheduleLegs();

				for (int legnum = 1; legnum < segDetails.size() + 1; legnum++) {

					Iterator segDtls = segDetails.iterator();
					while (segDtls.hasNext()) {

						FlightScheduleLegInfoDTO legs = (FlightScheduleLegInfoDTO) segDtls.next();

						if (legs.getLegNumber() == legnum) {

							int arrayIndex = legnum - 1;
							strSegmentDtls += legs.getOrigin() + ", " + legs.getDestination() + ", ";

							// if (strTimeMode.equals(WebConstants.LOCALTIME)) {

							legArr += "arrLegDtls[" + arrayIndex + "] =new Array('" + legs.getLegNumber() + "','"
									+ legs.getOrigin() + "','" + legs.getDestination() + "','"
									+ ScheduleUtils.formatDate(legs.getEstDepartureTimeLocal(), defaultFormat) + "','"
									+ legs.getEstDepartureDayOffsetLocal() + "','"
									+ ScheduleUtils.formatDate(legs.getEstArrivalTimeLocal(), defaultFormat) + "','"
									+ legs.getEstArrivalDayOffsetLocal() + "','" + legs.getDuration() + "','" + legs.getId()
									+ "','" + SegmentUtil.isOverlappingLeg(legs, flightSchedule) + "');";

							if (legs.getLegNumber() == 1) {
								sb.append("arrData[" + i + "][28] ='"
										+ ScheduleUtils.formatDate(legs.getEstDepartureTimeLocal(), defaultFormat) + "';");
							}

							// }else {

							legArrZulu += "arrLegZuluDtls[" + arrayIndex + "] =new Array('" + legs.getLegNumber() + "','"
									+ legs.getOrigin() + "','" + legs.getDestination() + "','"
									+ ScheduleUtils.formatDate(legs.getEstDepartureTimeZulu(), defaultFormat) + "','"
									+ legs.getEstDepartureDayOffset() + "','"
									+ ScheduleUtils.formatDate(legs.getEstArrivalTimeZulu(), defaultFormat) + "','"
									+ legs.getEstArrivalDayOffset() + "','" + legs.getDuration() + "','" + legs.getId() + "','"
									+ SegmentUtil.isOverlappingLeg(legs, flightSchedule) + "');";
							if (legs.getLegNumber() == 1) {
								sb.append("arrData[" + i + "][29] ='"
										+ ScheduleUtils.formatDate(legs.getEstDepartureTimeZulu(), defaultFormat) + "';");
							}
							// }
							break;
						}
					}
				}
				if (segDetails.size() == 0) {
					sb.append("arrData[" + i + "][28] ='&nbsp';");
					sb.append("arrData[" + i + "][29] ='&nbsp';");
				}
				if (!"".equals(strSegmentDtls)) {
					strSegmentDtls = strSegmentDtls.substring(0, strSegmentDtls.lastIndexOf(","));
				}

				dstFrom = flightSchedule.getDepartureStnCode();
				dstTo = flightSchedule.getArrivalStnCode();
				sb.append("arrData[" + i + "][13] = '" + flightSchedule.getModelNumber() + "';");
				sb.append("arrData[" + i + "][14] = '" + dstFrom + "';");
				sb.append("arrData[" + i + "][15] = '" + dstTo + "';");

				sb.append("arrData[" + i + "][33] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.MONDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][34] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.TUESDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][35] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.WEDNESDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][36] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.THURSDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][37] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.FRIDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][38] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.SATURDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][39] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.SUNDAY)).booleanValue() + "';");

				sb.append(legArr);
				sb.append("arrData[" + i + "][32] = arrLegDtls;");
				sb.append(legArrZulu);
				sb.append("arrData[" + i + "][40] = arrLegZuluDtls;");
				Collection segCol = flightSchedule.getFlightScheduleSegments();
				sb.append(setSegmentArr(segCol));
			}
			if (dstFrom != null && dstTo != null && dstFrom.length() > 0 && dstTo.length() > 0) {
				sb.append("mainData[" + nData + "] = new Array();");
				sb.append("mainData[" + nData + "][0] = arrData;");
				if (lang != null) {
					if (lang.equals("en")) {
						String airportFrom = flightSchedule.getDepartureAptName();
						String airportTo = flightSchedule.getArrivalAptName();
						if (airportFrom != null && "".compareTo(airportFrom) != 0 && airportTo != null
								&& "".compareTo(airportTo) != 0) {
							sb.append("mainData[" + nData + "][1] = '" + airportFrom + "';");
							sb.append("mainData[" + nData + "][2] = '" + airportTo + "';");
							sb.append("mainData[" + nData + "][3] = '" + flightSchedule.getSequenceId() + "';");
						} else {
							sb.append("mainData[" + nData + "][1] = '" + dstFrom + "';");
							sb.append("mainData[" + nData + "][2] = '" + dstTo + "';");
							sb.append("mainData[" + nData + "][3] = '" + flightSchedule.getSequenceId() + "';");
						}
					} else {
						Collection<String> airportList = new ArrayList<String>();
						airportList.add(dstTo);
						airportList.add(dstFrom);
						Collection<AirportForDisplay> airportCodeList = ModuleServiceLocator.getTranslationBD().getAirport(
								airportList, lang);
						String airportFrom = "";
						String airportTo = "";
						for (AirportForDisplay airport : airportCodeList) {
							if (dstFrom.equals(airport.getAirportCode()))
								airportFrom = StringUtil.getUnicode(airport.getAirportNameOl());
							if (dstTo.equals(airport.getAirportCode()))
								airportTo = StringUtil.getUnicode(airport.getAirportNameOl());
						}

						if (!airportFrom.isEmpty() && !airportTo.isEmpty()) {
							sb.append("mainData[" + nData + "][1] = '" + airportFrom + "';");
							sb.append("mainData[" + nData + "][2] = '" + airportTo + "';");
							sb.append("mainData[" + nData + "][3] = '" + flightSchedule.getSequenceId() + "';");
						} else {
							sb.append("mainData[" + nData + "][1] = '';");
							sb.append("mainData[" + nData + "][2] = '';");
							sb.append("mainData[" + nData + "][3] = '" + flightSchedule.getSequenceId() + "';");
						}
					}
				} else {
					sb.append("mainData[" + nData + "][1] = '';");
					sb.append("mainData[" + nData + "][2] = '';");
					sb.append("mainData[" + nData + "][3] = '" + flightSchedule.getSequenceId() + "';");
				}
			}
		}
		return sb.toString();

	}

	/**
	 * Gets a not null string for Grid
	 * 
	 * @param str
	 *            the String
	 * @return String the not null string for Grid
	 */
	private static String getNotNullString(String str) {
		return (str == null) ? "&nbsp" : str;
	}

	/**
	 * Method to get the Frequency segment
	 * 
	 * @param freq
	 *            the Frequency
	 * @return String the string requency Su_TuWd__Sa
	 */
	private static String createSegmentArray(Frequency freq) {

		HashMap daysMap = createDayOfWeekMap(freq);

		// create frequency string using daysMap
		String strFrq = "";
		String[] dayweek = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", };

		int intoffset = Integer.parseInt(dayOffSet);
		intoffset = intoffset % 7;
		for (int i = 0; i < 7; i++) {
			if (intoffset == 7)
				intoffset = 0;
			strFrq += (((Boolean) daysMap.get(CalendarUtil.getDayFromDayNumber(i))).booleanValue()) ? dayweek[intoffset] : "__";
			intoffset++;
		}
		return strFrq;
	}

	/**
	 * Creates a Day of Week Map
	 * 
	 * @param freq
	 *            the Frequency
	 * @return HashMap contating Day of Week
	 */
	static private HashMap createDayOfWeekMap(Frequency freq) {

		// initialize hashmap
		HashMap<DayOfWeek, Boolean> daysMap = new HashMap<DayOfWeek, Boolean>();
		daysMap.put(DayOfWeek.MONDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SUNDAY, new Boolean(false));
		daysMap.put(DayOfWeek.TUESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.WEDNESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.THURSDAY, new Boolean(false));
		daysMap.put(DayOfWeek.FRIDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SATURDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SUNDAY, new Boolean(false));

		Collection days = CalendarUtil.getDaysFromFrequency(freq);
		Iterator itDays = days.iterator();

		while (itDays.hasNext()) {
			DayOfWeek day = (DayOfWeek) itDays.next();

			// reset map with selected days
			daysMap.put(day, new Boolean(true));
		}

		return daysMap;
	}

	/**
	 * Method to Create the Segment array
	 * 
	 * @param segs
	 *            the ollection of segments
	 * @return String the Segment Detail Array
	 */
	static private String setSegmentArr(Collection segs) {

		String segArr = "var arrSegDtls = new Array();";// leg detail array
		int j = 0;
		if (segs != null) {
			Iterator iter = segs.iterator();
			while (iter.hasNext()) {
				FlightScheduleSegmentInfoDTO schSeg = (FlightScheduleSegmentInfoDTO) iter.next();
				segArr += "arrSegDtls[" + j + "] =new Array('" + schSeg.getFlSchSegId() + "','" + schSeg.getSegmentCode() + "','"
						+ schSeg.isValidFlag() + "','" + schSeg.getOverlapSegmentId() + "');";

				j++;
			}
		}
		return segArr;
	}

	private static String getUnicode(String in) {
		String out = "";
		if (in != null && in.length() > 0) {
			char c4;
			StringBuffer output = new StringBuffer();
			String subStr = null;
			for (int i = 0; i < in.length(); i = i + 4) {
				subStr = in.substring(i, i + 4);
				c4 = (char) Integer.parseInt(subStr, 10);
				output.append(c4);
			}
			out = output.toString();
		}
		return out;
	}
}
