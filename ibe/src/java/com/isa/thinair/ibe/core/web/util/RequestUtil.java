package com.isa.thinair.ibe.core.web.util;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.ibe.core.web.constants.RequestParameter;

public class RequestUtil {
	/**
	 * returns the hdnParamData param value
	 * 
	 * @param request
	 * @return
	 */
	public static String getHiddenParamData(HttpServletRequest request) {
		return request.getParameter(RequestParameter.HIDDEN_PARAM_DATA);
	}

	/**
	 * returns the hdnLanguage param value
	 * 
	 * @param request
	 * @return
	 */
	protected static String getHiddenLanguage(HttpServletRequest request) {
		String language = request.getAttribute(RequestParameter.HIDDEN_LANGUAGE) == null ? null : String.valueOf(request
				.getAttribute(RequestParameter.HIDDEN_LANGUAGE));
		// If language is set manually as an attribute it will take precedence. Otherwise try to get it from parameters
		if (language == null) {
			language = request.getParameter(RequestParameter.HIDDEN_LANGUAGE);
		}
		return language;
	}

}
