package com.isa.thinair.ibe.core.web.v2.action.promotion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.BookingType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEPromotionPaymentDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.PromotionRequestTo;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Promotion.PROMOTION_REGISTRATION_REDIRECT_PAGE),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class PromotionRegisterConfirmationAction extends IBEBaseAction {

	private static final Log log = LogFactory.getLog(PromotionRegisterConfirmationAction.class);

	private boolean success = true;
	private String messageTxt = "";

	public String execute() {
		String forward = StrutsConstants.Result.ERROR;
		String language = SessionUtil.getLanguage(request);
		IBEPromotionPaymentDTO ibePromotionPaymentDTO = null;
		IBEReservationPostPaymentDTO postPayDTO = null;

		try {
			if (SessionUtil.isSessionExpired(request)) {
				forward = StrutsConstants.Result.SESSION_EXPIRED;
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, CommonUtil.getRefundMessage(language));

			}
			postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);

			if (postPayDTO == null) {
				SessionUtil.expireSession(request);
				forward = StrutsConstants.Result.SESSION_EXPIRED;
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
						CommonUtil.getRefundMessage(SessionUtil.getLanguage(request)));
			} else {

				ibePromotionPaymentDTO = SessionUtil.getIbePromotionPaymentPostDTO(request);

				// postPayDTO
				boolean isPaymentSuccess = false;
				if (postPayDTO.getIpgResponseDTO() != null)
					isPaymentSuccess = postPayDTO.getIpgResponseDTO().isSuccess();

				IPGIdentificationParamsDTO ipgDTO = null;
				PayCurrencyDTO payCurrencyDTO = null;
				Date paymentTimestamp = null;
				String pnr = postPayDTO.getPnr();
				BookingType bookingType = postPayDTO.getBookingType();
				OnHold onHoldApp = OnHold.DEFAULT;
				CommonCreditCardPaymentInfo creditCardPaymentInfo = null;
				if (!postPayDTO.isNoPay() && bookingType != BookingType.ONHOLD && !postPayDTO.hasNoCCPayment()) {
					String strPayCurCode = postPayDTO.getPaymentGateWay().getPayCurrency();
					int ipgId = postPayDTO.getPaymentGateWay().getPaymentGateway();

					ipgDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(new Integer(ipgId), strPayCurCode);
					creditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils.getFirstElement(postPayDTO
							.getTemporyPaymentMap().values());
					payCurrencyDTO = creditCardPaymentInfo.getPayCurrencyDTO();
					paymentTimestamp = creditCardPaymentInfo.getTxnDateTime();
				}

				IPGResponseDTO creditInfo = postPayDTO.getIpgResponseDTO();
				String brokerType = getPaymentBrokerType(ipgDTO);
				Map<Integer, CommonCreditCardPaymentInfo> tempPayMap = postPayDTO.getTemporyPaymentMap();
				if (tempPayMap != null
						&& !PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)) {
					for (CommonCreditCardPaymentInfo ccPayInfo : tempPayMap.values()) {
						ccPayInfo.setNo(creditInfo.getCcLast4Digits());
					}
				}

				List<PromotionRequestTo> colPromotionRequestTo = ibePromotionPaymentDTO.getColPromotionRequestTo();
				List<PromotionRequestTo> colPromotionRequestsToPersist = new ArrayList<PromotionRequestTo>();
				Map<Integer, BigDecimal> paxPaymentAmounts = new HashMap<Integer, BigDecimal>();
				Map<String, BigDecimal> paxWiseAmounts = new HashMap<String, BigDecimal>();

				for (PromotionRequestTo promRequest : colPromotionRequestTo) {

					String reservationSegIds = promRequest.getReservationSegId();
					String[] resSegmentIds = reservationSegIds.split(",");

					List<String> segmentsPerBound = new ArrayList<String>(); // outbound or inbound at a time, not both
					for (String resSeg : resSegmentIds) {
						if (!resSeg.isEmpty()) {
							segmentsPerBound.add(resSeg);
						}
					}

					String reservationTravelRefNumbers = promRequest.getFlexiTravelRefNumbers();
					String[] resTravelRefNumbers = reservationTravelRefNumbers.split(",");

					List<String> countedPaxsForBalancePayment = new ArrayList<String>(); // outbound or inbound at a
																							// time,

					for (int i = 0; i < resSegmentIds.length; i++) {

						for (int j = 0; j < resTravelRefNumbers.length; j++) {

							PromotionRequestTo clone = promRequest.clone();

							clone.setReservationSegId(resSegmentIds[i]);
							clone.setBoundSegmentIds(segmentsPerBound); // cater for connection flights

							String SeatRequest = String.valueOf(clone.getLowerBound());
							clone.addPromotionRequestParam(PromoRequestParam.FreeSeat.SEATS_TO_FREE, SeatRequest);

							clone.addPromotionRequestParam(PromoRequestParam.FreeSeat.MOVE_SEAT,
									clone.getAgreedToMove().equals("true")
											? PromotionsInternalConstants.BOOLEAN_TRUE
											: PromotionsInternalConstants.BOOEAN_FALSE);

							Integer reservationPaxId = PaxTypeUtils.getPnrPaxId(resTravelRefNumbers[j]);
							clone.setReservationPaxId(reservationPaxId);

							if (!countedPaxsForBalancePayment.contains(resTravelRefNumbers[j])) {

								if (!paxWiseAmounts.containsKey(resTravelRefNumbers[j])) {
									paxWiseAmounts.put(resTravelRefNumbers[j], promRequest.getPromoRequestCharge());
								} else {
									BigDecimal previousSegWisePaxChargers = paxWiseAmounts.get(resTravelRefNumbers[j]);
									BigDecimal updatedSegWisePaxChargers = AccelAeroCalculator.add(previousSegWisePaxChargers,
											promRequest.getPromoRequestCharge());
									paxWiseAmounts.put(resTravelRefNumbers[j], updatedSegWisePaxChargers);
								}
							}

							countedPaxsForBalancePayment.add(resTravelRefNumbers[j]);
							colPromotionRequestsToPersist.add(clone);
						}
					}
				}

				composePaxSequenceWisePayments(paxPaymentAmounts, paxWiseAmounts);

				LCCClientBalancePayment balancePayment = getPaxPaymentMap(postPayDTO, creditInfo, ipgDTO, paymentTimestamp, null,
						null, payCurrencyDTO, null, brokerType, creditCardPaymentInfo, paxPaymentAmounts);
				balancePayment.setTemporyPaymentMap(tempPayMap);
				Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
				if (postPayDTO.getSelectedExternalCharges() != null) {
					extChgMap.put(EXTERNAL_CHARGES.CREDIT_CARD,
							postPayDTO.getSelectedExternalCharges().get(EXTERNAL_CHARGES.CREDIT_CARD));
				}
				balancePayment.setExternalChargesMap(extChgMap);

				PromotionType promotionType = PromotionType.PROMOTION_NEXT_SEAT_FREE;
				ServiceResponce promotionSaveResponse = ModuleServiceLocator.getPromotionManagementBD().savePromotionRequests(
						colPromotionRequestsToPersist, balancePayment, promotionType, getTrackInfo());

				if (promotionSaveResponse.isSuccess()) {
					request.getSession().setAttribute("promoPage", "NS");
					forward = StrutsConstants.Result.SUCCESS;
				} else {
					// TODO: refund the payment
					log.error("Promotion Requests were not saved after the payment");
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, CommonUtil.getRefundMessage(language));
				}

			}

		} catch (Exception e) {
			success = false;
			messageTxt = "Promotion Confirmation Failed";
			log.error("Error in Promotion Confirmation", e);

		} finally {
			SessionUtil.setIbePromotionPaymentPostDTO(request, null);
			SessionUtil.setIBEreservationInfo(request, null);
			SessionUtil.setIbeReservationPostDTO(request, null);
			SessionUtil.setIBEReservationPostPaymentDTO(request, null);
			SessionUtil.setPutOnHoldBeforePaymentDTO(request, null);
		}
		return forward;
	}

	/**
	 * Get Broker type - internal payment gateway or external payment gateway
	 * 
	 * @param ipgDTO
	 * @return
	 * @throws ModuleException
	 */
	private String getPaymentBrokerType(IPGIdentificationParamsDTO ipgDTO) throws ModuleException {
		String brokerType = null;
		if (ipgDTO != null && ipgDTO.getIpgId() != null) {
			Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgDTO);
			brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
		}
		return brokerType;
	}

	public static LCCClientBalancePayment getPaxPaymentMap(IBEReservationPostPaymentDTO postPayDTO, IPGResponseDTO creditInfo,
			IPGIdentificationParamsDTO ipgDTO, Date paymentTimestamp, String loyaltyAgentCode, String loyaltyAccount,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueKey, String brokerType, CommonCreditCardPaymentInfo cardPaymentInfo,
			Map<Integer, BigDecimal> paxPaymentAmounts) {
		LCCClientBalancePayment balancePayment = new LCCClientBalancePayment();
		Date currentDatetime = new Date();

		LoyaltyPaymentOption loyaltyPayOption = postPayDTO.getLoyaltyPayOption();
		int adultChildCount = paxPaymentAmounts.keySet().size(); // FIXME set the correct count

		boolean isCCPayment = postPayDTO.isAddCCExternalCharge() && !postPayDTO.isNoPay();
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(new ArrayList<ReservationPaxTO>(),
				postPayDTO.getSelectedExternalCharges(), isCCPayment, false);
		externalChargesMediator.setCalculateJNTaxForCCCharge(true);
		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(adultChildCount, 0);

		// LMS payment option will not come here from UI. However handled it by getting default carrier lmsPaymant
		LoyaltyPaymentInfo loyaltyPaymentInfo = postPayDTO.getCarrierLoyaltyPaymentInfo(AppSysParamsUtil.getDefaultCarrierCode());
		String loyaltyMemberAccountId = null;
		Map<Integer, Map<String, BigDecimal>> paxProductRedeemed = null;
		String[] rewardIDs = null;
		if (loyaltyPaymentInfo != null) {
			loyaltyMemberAccountId = loyaltyPaymentInfo.getMemberAccountId();
			paxProductRedeemed = loyaltyPaymentInfo.getPaxProductPaymentBreakdown();
			rewardIDs = loyaltyPaymentInfo.getLoyaltyRewardIds();
		}

		BigDecimal loyaltyPayAmount = postPayDTO.getLoyaltyCredit();
		boolean isExtCardPay = true;
		if (brokerType != null
				&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)
				&& cardPaymentInfo != null) {
			isExtCardPay = false;
		}
		for (Integer paxSeqId : paxPaymentAmounts.keySet()) {

			BigDecimal lmsMemberPayment = AirProxyReservationUtil.getPaxRedeemedTotal(paxProductRedeemed, paxSeqId);

			PaymentAssemblerComposer paymentComposer = null;
			List<LCCClientExternalChgDTO> extCharges = BookingUtil
					.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
			if (extCharges == null) {
				extCharges = new ArrayList<LCCClientExternalChgDTO>();
			}

			BigDecimal amountDue = paxPaymentAmounts.get(paxSeqId);

			paymentComposer = new PaymentAssemblerComposer(extCharges);
			if (!postPayDTO.isNoPay()) {
				BigDecimal totalWithAncillary = AccelAeroCalculator.add(amountDue, BookingUtil.getTotalExtCharge(extCharges));

				if (AccelAeroCalculator.isEqual(lmsMemberPayment, AccelAeroCalculator.getDefaultBigDecimalZero())
						&& loyaltyPayOption == LoyaltyPaymentOption.NONE) {
					if (totalWithAncillary.compareTo(BigDecimal.ZERO) > 0) {
						if (isExtCardPay) {
							paymentComposer.addCreditCardPayment(amountDue, creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp,
									lccUniqueKey);
						} else {
							paymentComposer.addCreditCardPaymentInternal(amountDue, creditInfo, ipgDTO, payCurrencyDTO,
									paymentTimestamp, cardPaymentInfo.getNo(), cardPaymentInfo.geteDate(),
									cardPaymentInfo.getSecurityCode(), cardPaymentInfo.getName(), lccUniqueKey);
						}
					}
				} else if (totalWithAncillary.compareTo(BigDecimal.ZERO) > 0) {
					if (AccelAeroCalculator.isGreaterThan(lmsMemberPayment, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						amountDue = AccelAeroCalculator.subtract(amountDue, lmsMemberPayment);
						totalWithAncillary = AccelAeroCalculator.subtract(totalWithAncillary, lmsMemberPayment);
						paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs, paxProductRedeemed,
								lmsMemberPayment, payCurrencyDTO, currentDatetime, AppSysParamsUtil.getDefaultCarrierCode());
					}

					if (loyaltyPayOption != LoyaltyPaymentOption.NONE) {
						amountDue = AccelAeroCalculator.subtract(amountDue, loyaltyPayAmount);
						if (totalWithAncillary.compareTo(loyaltyPayAmount) > 0) {
							if (loyaltyPayAmount.compareTo(BigDecimal.ZERO) > 0) {
								paymentComposer.addLoyaltyCreditPayment(loyaltyAgentCode, loyaltyPayAmount, loyaltyAccount,
										payCurrencyDTO, currentDatetime);
							}
							loyaltyPayAmount = BigDecimal.ZERO;
						} else {
							paymentComposer.addLoyaltyCreditPayment(loyaltyAgentCode, amountDue, loyaltyAccount, payCurrencyDTO,
									currentDatetime);
							loyaltyPayAmount = AccelAeroCalculator.subtract(loyaltyPayAmount, totalWithAncillary);
						}
					}

					if (AccelAeroCalculator.isGreaterThan(amountDue, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						if (isExtCardPay) {
							paymentComposer.addCreditCardPayment(amountDue, creditInfo, ipgDTO, payCurrencyDTO, paymentTimestamp,
									lccUniqueKey);
						} else {
							paymentComposer.addCreditCardPaymentInternal(amountDue, creditInfo, ipgDTO, payCurrencyDTO,
									paymentTimestamp, cardPaymentInfo.getNo(), cardPaymentInfo.geteDate(),
									cardPaymentInfo.getSecurityCode(), cardPaymentInfo.getName(), lccUniqueKey);
						}
					}
				}
			}

			LCCClientPaymentAssembler payment = paymentComposer.getPaymentAssembler();

			// Hack to identify the promotions
			// TODO : refactor the card assembler to accept remarks
			for (LCCClientPaymentInfo objPay : payment.getPayments()) {
				if (objPay instanceof CommonCreditCardPaymentInfo) {
					CommonCreditCardPaymentInfo payInfo = (CommonCreditCardPaymentInfo) objPay;
					payInfo.setRemarks(PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG.getChargeDescription());
				}
			}

			balancePayment.addPassengerPayments(paxSeqId, payment);

		}

		return balancePayment;
	}

	private static void composePaxSequenceWisePayments(Map<Integer, BigDecimal> paxPaymentAmounts,
			Map<String, BigDecimal> paxWiseAmounts) {

		for (String paxTravelRefNumber : paxWiseAmounts.keySet()) {

			Integer paxSeqId = PaxTypeUtils.getPaxSeq(paxTravelRefNumber);
			BigDecimal paxWiseTotal = paxWiseAmounts.get(paxTravelRefNumber);

			if (!paxPaymentAmounts.containsKey(paxSeqId)) {
				paxPaymentAmounts.put(paxSeqId, paxWiseTotal);

			}

		}

	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

}
