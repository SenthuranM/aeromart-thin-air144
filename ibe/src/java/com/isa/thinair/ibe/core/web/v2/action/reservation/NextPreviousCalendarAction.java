package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfoByDateDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;

/**
 * @author Pradeep Karunanayake
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class NextPreviousCalendarAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(NextPreviousCalendarAction.class);

	private boolean success = true;

	private String messageTxt;

	private Collection<AvailableFlightInfoByDateDTO> departueFlights;

	private Collection<AvailableFlightInfoByDateDTO> arrivalFlights;

	private final List<LogicalCabinClassInfoTO> outboundAvailableLogicalCC = new ArrayList<LogicalCabinClassInfoTO>();

	private final List<LogicalCabinClassInfoTO> inboundAvailableLogicalCC = new ArrayList<LogicalCabinClassInfoTO>();

	private String searchSystem;

	// Modifing data
	private boolean modifySegment;

	private int oldFareID;

	private int oldFareType;

	private String oldFareAmount;

	private String pnr;

	private boolean groupPNR;

	private String oldAllSegments;

	private String modifySegmentRefNos;

	private String maxAmount = "0";

	private String minAmount = "0";

	private boolean addGroundSegment;

	private boolean busForDeparture;

	private String busConValidTimeFrom;

	private String busConValidTimeTo;

	private boolean allowOnhold;

	private boolean requoteFlightSearch;
	
	private boolean captchaValidationRequired;

	@SuppressWarnings("unchecked")
	public String execute() {

		String result = StrutsConstants.Result.SUCCESS;
		try {
						
			boolean blnEnableHalfReturnFareSearch = false;
			if (getSearchParams().getReturnDate() != null && !"".equals(getSearchParams().getReturnDate())) {
				// identify whether the whole booking is a return
				blnEnableHalfReturnFareSearch = true;
			}
			String language = SessionUtil.getLanguage(request);
			boolean isReturnFlight = false;
			String tempFromAirport = getSearchParams().getFromAirport();

			if (getSearchParams().isReturnFlag()) {
				isReturnFlight = true;
				getSearchParams().setReturnFlag(false);
				getSearchParams().setFromAirport(getSearchParams().getToAirport());
				getSearchParams().setToAirport(tempFromAirport);
				getSearchParams().setDepartureDate(getSearchParams().getReturnDate());
				getSearchParams().setReturnDate(null);
			} else {
				getSearchParams().setReturnFlag(false);
				getSearchParams().setReturnDate(null);
			}

			FlightAvailRQ flightAvailRQ = BeanUtil.createFlightAvailRQ(getSearchParams(), (pnr == null || "".equals(pnr)));
			flightAvailRQ.getAvailPreferences().setQuoteFares(true);
			// Modify Segment
			// TODO Interline
			if (modifySegment && !groupPNR) {
				SearchUtil.setModifySegmentData(flightAvailRQ, oldFareID, oldFareType, oldFareAmount);
			}

			// Set Half Return Fare
			if (pnr != null && !"".equals(pnr)) {
				flightAvailRQ.getAvailPreferences().setModifyBooking(true);
			}
			// TODO this will need to be modified to accommodate dry booking modification in IBE
			if (pnr != null && !"".equals(pnr)) {
				SearchUtil.setExistingSegInfo(flightAvailRQ, oldAllSegments, modifySegmentRefNos, getSearchParams()
						.isReturnFlag());
			} else if ((getSearchParams().isReturnFlag() || blnEnableHalfReturnFareSearch)
					&& getSearchParams().getValidity() == null) {
				boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
				flightAvailRQ.getAvailPreferences().setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
				flightAvailRQ.getAvailPreferences().setInboundFareModified(isReturnFlight);
			}

			if (AppSysParamsUtil.isGroundServiceEnabled() && addGroundSegment) {
				if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
					List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
					originDestinationInformationList.add(originDestinationInfoWithConnectionValidations(flightAvailRQ
							.getOriginDestinationInformationList().iterator().next()));
					flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
				}
			}

			if (pnr != null && !"".equals(pnr)) {
				AnalyticsLogger.logModSearch(AnalyticSource.IBE_MOD_NXTPREV, flightAvailRQ, request, pnr, getTrackInfo());
			} else {
				AnalyticsLogger.logAvlSearch(AnalyticSource.IBE_CRE_NXTPREV, flightAvailRQ, request, getTrackInfo());
			}

			FlightAvailRS flightAvailRS = null;
			if (requoteFlightSearch) {
				flightAvailRQ.getAvailPreferences().setRequoteFlightSearch(true);
				flightAvailRQ.getAvailPreferences().setQuoteFares(false);
				flightAvailRQ.getAvailPreferences().setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
				flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
						getTrackInfo(), false);
			} else {
				flightAvailRS = SearchUtil.getFlightAvailableFlights(flightAvailRQ, getTrackInfo());
			}

			IBEReservationInfoDTO reservationInfo = SessionUtil.getIBEreservationInfo(request);
			allowOnhold = ReservationUtil.isReservationCabinClassOnholdable(flightAvailRS);
			boolean reservationOtherValidationOnholdable = ReservationUtil.isReservationOtherValidationOnholdable(flightAvailRS,
					SYSTEM.getEnum(getSearchParams().getSearchSystem()), getTrackInfo(), getClientInfoDTO().getIpAddress());

			if (reservationInfo != null) {
				reservationInfo.setReservationOnholdableOtherValidation(reservationOtherValidationOnholdable);
			}

			allowOnhold = allowOnhold && reservationOtherValidationOnholdable;

			reservationInfo.addNewServiceTaxes(flightAvailRS.getApplicableServiceTaxes());

			Date departDateTimeStart = null;
			Date departDateTimeEnd = null;

			for (OriginDestinationInformationTO ond : flightAvailRQ.getOriginDestinationInformationList()) {
				if (!ond.isReturnFlag()) {
					departDateTimeStart = ond.getDepartureDateTimeStart();
					departDateTimeEnd = ond.getDepartureDateTimeEnd();
					break;
				}
			}

			Object[] arrObj = SearchUtil.composeDisplayObjects(flightAvailRS, getSearchParams().getSelectedCurrency(), true,
					isReturnFlight, new Locale(CommonUtil.getJavaSupportLocale(language)), null);

			Collection<AvailableFlightInfoByDateDTO> tempFlights;

			if (isReturnFlight) {
				tempFlights = SearchUtil.getFlightsGroupByDate((List<AvailableFlightInfo>) arrObj[1], departDateTimeStart,
						departDateTimeEnd, null);
				List<AvailableFlightInfo> selDateReturnFlights = SearchUtil.getSelectedDateAvailableFlights(tempFlights);
				inboundAvailableLogicalCC.addAll(SearchUtil.getAvailableLogicalCabinClassInfo(getSearchParams()
						.getClassOfService(), language, selDateReturnFlights, flightAvailRQ.getAvailPreferences()
						.getPreferredLanguage()));
			} else {
				tempFlights = SearchUtil.getFlightsGroupByDate((List<AvailableFlightInfo>) arrObj[0], departDateTimeStart,
						departDateTimeEnd, null);
				List<AvailableFlightInfo> selDateDepartureFlights = SearchUtil.getSelectedDateAvailableFlights(tempFlights);
				outboundAvailableLogicalCC.addAll(SearchUtil.getAvailableLogicalCabinClassInfo(getSearchParams()
						.getClassOfService(), language, selDateDepartureFlights, flightAvailRQ.getAvailPreferences()
						.getPreferredLanguage()));
			}

			if (AppSysParamsUtil.isCalendarGradiantView()) {
				BigDecimal existMaxAmount = new BigDecimal((maxAmount == null || maxAmount.trim().isEmpty()) ? "0" : maxAmount);
				BigDecimal existMinAmount = new BigDecimal((minAmount == null || minAmount.trim().isEmpty()) ? "0" : minAmount);
				SearchUtil.setPriceGuide(tempFlights, existMaxAmount, existMinAmount);
			}

			if (isReturnFlight) {
				arrivalFlights = tempFlights;
				departueFlights = null;
			} else {
				departueFlights = tempFlights;
				arrivalFlights = null;
			}

		} catch (ModuleException me) {
			success = false;
			// FIXME Verify All Error Codes
			if (!me.getExceptionCode().isEmpty()) {
				messageTxt = I18NUtil.getMessage(me.getExceptionCode());
			} else {
				messageTxt = me.getMessageString();
				log.error("NextPreviousCalendarAction==>", me);
			}
			SessionUtil.resetSesionDataInError(request);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			/**
			 * Added additional logs to identify date format issue [25-Nov-20142014 23:59:59]
			 */
			log.error("NextPreviousCalendarAction==>SessionID:" +  request.getRequestedSessionId() 
					+ " Departure Date : " + getSearchParams().getDepartureDate()
					+ " Return Date :" + getSearchParams().getReturnDate() , ex);			
			SessionUtil.resetSesionDataInError(request);
		}

		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<AvailableFlightInfoByDateDTO> getDepartueFlights() {
		return departueFlights;
	}

	public Collection<AvailableFlightInfoByDateDTO> getArrivalFlights() {
		return arrivalFlights;
	}

	public String getSearchSystem() {
		return searchSystem;
	}

	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}

	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public int getOldFareID() {
		return oldFareID;
	}

	public void setOldFareID(int oldFareID) {
		this.oldFareID = oldFareID;
	}

	public void setOldFareType(int oldFareType) {
		this.oldFareType = oldFareType;
	}

	public void setOldFareAmount(String oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public boolean isBusForDeparture() {
		return busForDeparture;
	}

	public void setBusForDeparture(boolean busForDeparture) {
		this.busForDeparture = busForDeparture;
	}

	public String getBusConValidTimeFrom() {
		return busConValidTimeFrom;
	}

	public void setBusConValidTimeFrom(String busConValidTimeFrom) {
		this.busConValidTimeFrom = busConValidTimeFrom;
	}

	public String getBusConValidTimeTo() {
		return busConValidTimeTo;
	}

	public void setBusConValidTimeTo(String busConValidTimeTo) {
		this.busConValidTimeTo = busConValidTimeTo;
	}

	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	public List<LogicalCabinClassInfoTO> getOutboundAvailableLogicalCC() {
		return outboundAvailableLogicalCC;
	}

	public List<LogicalCabinClassInfoTO> getInboundAvailableLogicalCC() {
		return inboundAvailableLogicalCC;
	}

	private OriginDestinationInformationTO originDestinationInfoWithConnectionValidations(
			OriginDestinationInformationTO originDestinationInformation) throws ModuleException {

		Calendar valiedTimeFrom = Calendar.getInstance();
		valiedTimeFrom.setTimeInMillis(Long.valueOf(getBusConValidTimeFrom()));
		Calendar valiedTimeTo = Calendar.getInstance();
		valiedTimeTo.setTimeInMillis(Long.valueOf(getBusConValidTimeTo()));

		if (isBusForDeparture()) {
			originDestinationInformation.setArrivalDateTimeStart(valiedTimeFrom.getTime());
			originDestinationInformation.setArrivalDateTimeEnd(valiedTimeTo.getTime());
		} else {
			Calendar dptStart = Calendar.getInstance();
			dptStart.setTime(originDestinationInformation.getDepartureDateTimeStart());
			Calendar dptEnd = Calendar.getInstance();
			dptEnd.setTime(originDestinationInformation.getDepartureDateTimeEnd());

			// Valied times should be within the search date.
			List<Calendar> valiedPeriod = CommonUtil.validPeriod(dptStart, dptEnd, valiedTimeFrom, valiedTimeTo);
			if (valiedPeriod.size() == 2) {
				originDestinationInformation.setDepartureDateTimeStart(valiedPeriod.get(0).getTime());
				originDestinationInformation.setDepartureDateTimeEnd(valiedPeriod.get(1).getTime());
			}
		}

		return originDestinationInformation;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public boolean isCaptchaValidationRequired() {
		return captchaValidationRequired;
	}

	public void setCaptchaValidationRequired(boolean captchaValidationRequired) {
		this.captchaValidationRequired = captchaValidationRequired;
	}
}
