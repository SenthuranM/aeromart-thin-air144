package com.isa.thinair.ibe.core.web.v2.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;

public class I18NUtil {

	private static final String IBE_COMMON_MESSAGES = "resources/i18n/IBECommonMessages";

	private static final String IBE_CLIENT_MESSAGES = "resources/i18n/v2/IBEApplicationResourcesClient";

	private static final String DEFALT_LANGUGAE_CODE = "en";

	private static final String IBE_DEFULT_USER_MESSAGE_CODE = "customer.module.message.code.empty";

	private static Map<String, ResourceBundle> commonMessages = new HashMap<String, ResourceBundle>();

	private static Map<String, ResourceBundle> clientMessages = new HashMap<String, ResourceBundle>();

	static {
		loadResourceBundles();
	}

	private static void loadResourceBundles() {
		Map<String, String> suppotedLanguagesMap = AppSysParamsUtil.getIBESupportedLanguages();
		Set<String> lanKeySet = suppotedLanguagesMap.keySet();

		for (Iterator<String> iterator = lanKeySet.iterator(); iterator.hasNext();) {
			String lan = (String) iterator.next();
			Locale locale = new Locale(lan);
			commonMessages.put(lan, ResourceBundle.getBundle(IBE_COMMON_MESSAGES, locale));
			clientMessages.put(lan, ResourceBundle.getBundle(IBE_CLIENT_MESSAGES, locale));
		}
	}

	public static String getMessage(String messageCode) {
		return getMessage(messageCode, DEFALT_LANGUGAE_CODE);
	}

	public static String getMessage(String messageCode, String languge) {
		ResourceBundle rb = clientMessages.get(languge);
		String message = "";
		if (messageCode != null && !messageCode.equals("")) {
			try {
				message = rb.getString(messageCode);
			} catch (Exception ibeClintEx) {
				try {
					rb = commonMessages.get(languge);
					message = rb.getString(messageCode);
				} catch (Exception ibeCommonEx) {
					try {
						message = MessagesUtil.getMessage(messageCode);
					} catch (Exception commonEx) {
						message = commonMessages.get(languge).getString(IBE_DEFULT_USER_MESSAGE_CODE);
					}
				}
			}
			if (message == null) {
				message = "";
			}
		}
		return message;
	}

	/**
	 * Get Screen Labels with out Screen IDs
	 * 
	 * @param languge
	 * @param pageIds
	 * @return
	 */
	public static HashMap<String, String> getMessagesInJSON(String languge, String... pageIds) {
		HashMap<String, String> pageMesage = new HashMap<String, String>();
		ResourceBundle rb = commonMessages.get(languge);
		if (rb != null) {
			for (String key : rb.keySet()) {
				for (String pageID : pageIds) {
					if (key.startsWith(pageID + "_")) {
						pageMesage.put(key.split(pageID + "_")[1], rb.getString(key));
						break;
					}
				}
			}
		}
		// Client Messages
		ResourceBundle client = clientMessages.get(languge);
		if (client != null) {
			for (String key : client.keySet()) {
				for (String pageID : pageIds) {
					if (key.startsWith(pageID + "_")) {
						pageMesage.put(key.split(pageID + "_")[1], client.getString(key));
						break;
					}
				}
			}
		}
		return pageMesage;
	}

	public static List<String[]> getIBESupportedLanguages(HttpServletRequest request) {
		List<String[]> languagesList = new ArrayList<String[]>();
		String userLang = SessionUtil.getLanguage(request);
		Map<String, String> mapLan = AppSysParamsUtil.getIBESupportedLanguages();
		Set colKey = mapLan.keySet();
		for (Iterator iter = colKey.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			String lanName = getMessage("msg.res.psg.Itn.Lng." + key, userLang);
			String[] tempArry = { key, lanName };
			languagesList.add(tempArry);
		}
		return languagesList;
	}

}
