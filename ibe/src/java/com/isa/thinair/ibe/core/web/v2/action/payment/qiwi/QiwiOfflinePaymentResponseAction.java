package com.isa.thinair.ibe.core.web.v2.action.payment.qiwi;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.jdom.output.XMLOutputter;

import com.ibm.misc.BASE64Encoder;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.opensymphony.xwork2.ActionChainResult;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
	@Result(name = StrutsConstants.Result.BYPASS, type = ActionChainResult.class, value = StrutsConstants.Action.QIWI_INVOICE_NOTIFICATION_HANDLER),
	@Result(name = StrutsConstants.Result.SUCCESS, value = "") })
public class QiwiOfflinePaymentResponseAction extends  BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(QiwiOfflinePaymentResponseAction.class);	
	
	public String execute(){
		log.info("QiwiOfflinePaymentResponseAction : Start");
		
		if (log.isDebugEnabled()){
			log.debug("### Start.. QiwiOfflinePaymentResponseAction " + request.getRequestedSessionId());
		}
		
		
		try {
			
			PrintWriter out = null;
			String responseStr = null;
			Map<String, String> receiptyMap = RequestParameterUtil.getReceiptMap(request);		
			log.info(receiptyMap);	
			
			String auth = request.getHeader("Authorization");
			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(QiwiPRequest.QIWI_PROVIDER_CODE);
			IPGPaymentOptionDTO qiwiPgwConfigs = pgwList.get(0);			
			
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO =  WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(qiwiPgwConfigs.getPaymentGateway(), qiwiPgwConfigs.getBaseCurrency());
			
			// check authorization header
			if(!checkAuthorization(auth,ipgIdentificationParamsDTO)){
				log.info("QiwiOfflinePaymentResponseAction : Basic Authorization Failure");
				return StrutsConstants.Result.SUCCESS;
			}
			
			QiwiPResponse res = new QiwiPResponse();
			res.setOnOnhold(true);
			res.setCommand(receiptyMap.get(QiwiPResponse.COMMAND));
			res.setQiwiTxnId(receiptyMap.get(QiwiPResponse.TXN_ID));
			// pnr
			res.setPnr(receiptyMap.get(QiwiPResponse.ACCOUNT));
			res.setAmount(receiptyMap.get(QiwiPResponse.SUM));
			res.setCurrency(receiptyMap.get(QiwiPResponse.CCY));
			
			if(QiwiPResponse.BILL.equals(res.getCommand())){
				log.info("QiwiOfflinePaymentResponseAction : Bypassing...");
				return StrutsConstants.Result.BYPASS;
			}
					
			Reservation reservation = ReservationModuleUtils.getReservationBD().getReservation(res.getPnr(), false);
			TempPaymentTnx tempPaymentTnx = null;		
			
			if(reservation != null && ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())){
				
				Collection<TempPaymentTnx> tempPaymentEntries = IBEModuleUtils.getReservationBD().getTempPaymentsForReservation(reservation.getPnr());
				for (TempPaymentTnx tmpTnx : tempPaymentEntries) {						
					if (ReservationInternalConstants.TempPaymentTnxTypes.INITIATED.equals(tmpTnx.getStatus())) {
						tempPaymentTnx = tmpTnx;							
					}						
				}	
				
				res.setBillId(String.valueOf(tempPaymentTnx.getTnxId()));
				
				if(tempPaymentTnx != null && tempPaymentTnx.getPaymentCurrencyAmount().toString().equals(res.getAmount())
						&& tempPaymentTnx.getPaymentCurrencyCode().equals(res.getCurrency())){
					// request is valid
					// error code 0 : OK
					res.setResultCode("0");
					res.setComment(res.getPnr() + ": is Available for Confirmation");
					log.info("QiwiOfflinePaymentResponseAction : " + res.getComment());
				}else{
					// invalid payment amount
					// error code 7 : Payment refused according to business rules 				
					res.setResultCode("7");
					res.setComment("Invalid Payment amount or currency. Payment refused according to business rules");
					log.info("QiwiOfflinePaymentResponseAction : " + res.getComment());
				}
				
			}else{
				// no onhold to confirm
				// error code 5 : PNR is not available
				res.setResultCode("5");
				res.setComment(res.getPnr() + ": is not Available. Onhold duration has Expired");
				log.info("QiwiOfflinePaymentResponseAction : " + res.getComment());
			}
			
			if(res.getCommand().equals(QiwiPResponse.PAY)){				
				
				Date requestTime = tempPaymentTnx.getPaymentTimeStamp();
				
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
				ipgResponseDTO.setTemporyPaymentId(Integer.parseInt(res.getBillId()));	
				
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Calendar cal = Calendar.getInstance(); 
			    cal.setTime(ipgResponseDTO.getResponseTimeStamp());
			    
				res.setPaymentCompletionTime(df.format(cal.getTime()));
				
				receiptyMap.put("Qiwi's Tnx Ref", res.getQiwiTxnId());
				receiptyMap.put("PNR", res.getPnr());
				receiptyMap.put("responseType", "PAYMENTSUCCESS");
				receiptyMap.put("qiwiOfflineMode", "true");
				receiptyMap.put(QiwiPRequest.TNX_ID, res.getBillId());
				receiptyMap.put(QiwiPResponse.STATUS, QiwiPResponse.INVOICE_PAID);
				receiptyMap.put(QiwiPRequest.AMOUNT, res.getAmount());
				receiptyMap.put(QiwiPRequest.ERROR, "0");
				receiptyMap.put(QiwiPRequest.USER, "");
				receiptyMap.put(QiwiPResponse.CCY, res.getCurrency());
				
				ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);
				
				String resStatus = ReservationModuleUtils.getReservationBD().getReservation(res.getPnr(), false).getStatus();
				
				if(ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(resStatus)){
					res.setResultCode("0");
					res.setComment(res.getPnr() + ": Has been Confirmed");
				}else{
					res.setResultCode("1");
					res.setComment(res.getPnr() + ": Could not Confirm");
				}			
				
			}				
			
			responseStr = composeXMLResponse(res);				
			
			out = this.response.getWriter();  
			out.println(responseStr);  
			out.flush(); 
			out.close();
		
		} catch (ModuleException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		log.info("QiwiOfflinePaymentResponseAction : End");
		return StrutsConstants.Result.SUCCESS;
	}
	
	
	private boolean checkAuthorization(String auth,	IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		
		try {			
			Properties props = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
			String token = new BASE64Encoder().encode((props.get("merchantId") + ":" + props.get(QiwiPRequest.SERVER_TO_SERVER_RES_PASSWORD)).getBytes());
			token = "Basic " + token;
			
			if(token.equals(auth)){
				return true;
			}			
			log.info("Authorization token mismatch : " + auth + " != " + token);
			
		} catch (ModuleException e) {			
			e.printStackTrace();
		}
		return false;		
	}


	private static String composeXMLResponse(QiwiPResponse res){
		
		String responseStr =  "";
		org.jdom.Document doc = new org.jdom.Document();
		org.jdom.Element response = new org.jdom.Element("response")
				.addContent(new org.jdom.Element("osmp_txn_id").setText(res.getQiwiTxnId()))			
				.addContent(new org.jdom.Element("prv_txn").setText(res.getBillId()))
				.addContent(new org.jdom.Element("sum").setText(res.getAmount()))
				.addContent(new org.jdom.Element("ccy").setText(res.getCurrency()))
				.addContent(new org.jdom.Element("result").setText(res.getResultCode()))
				.addContent(new org.jdom.Element("comment").setText(res.getComment()));
		
		if(res.getCommand().equals(QiwiPResponse.PAY)){
			org.jdom.Element fields = new org.jdom.Element("fields");					
			org.jdom.Element field = new org.jdom.Element("field");
			field.setAttribute("name", "prv-date");
			field.setText(res.getPaymentCompletionTime());
			
			fields.addContent(field);			
			response.addContent(fields);
		}	
	
		doc.addContent(response);
		responseStr = new XMLOutputter().outputString(doc);
		
		log.debug("[QiwiOfflinePaymentResponseAction::sendXMLResponse] " + responseStr);
	
		return responseStr;
	}
}

	
