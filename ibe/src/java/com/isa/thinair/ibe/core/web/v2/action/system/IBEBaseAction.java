package com.isa.thinair.ibe.core.web.v2.action.system;

import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

public class IBEBaseAction extends BaseRequestAwareAction {

	protected FlightSearchDTO searchParams = null;

	private IBECommonDTO commonParams = new IBECommonDTO();

	private String fareQuoteJson = null;

	private String selectedFlightJson = null;

	private boolean fromSecure = false;

	private String flexiSelection = null;
	
	private String promoInfoJson = null;

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public IBECommonDTO getCommonParams() {
		if (commonParams == null)
			commonParams =  new IBECommonDTO();
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

	public String getFareQuoteJson() {
		return fareQuoteJson;
	}

	public void setFareQuoteJson(String fareQuoteJson) {
		this.fareQuoteJson = fareQuoteJson;
	}

	public String getSelectedFlightJson() {
		return selectedFlightJson;
	}

	public void setSelectedFlightJson(String selectedFlightJson) {
		this.selectedFlightJson = selectedFlightJson;
	}

	public boolean isFromSecure() {
		return fromSecure;
	}

	public void setFromSecure(boolean fromSecure) {
		this.fromSecure = fromSecure;
	}

	public String getFlexiSelection() {
		return flexiSelection;
	}

	public void setFlexiSelection(String flexiSelection) {
		this.flexiSelection = flexiSelection;
	}

	public String getPromoInfoJson() {
		return promoInfoJson;
	}

	public void setPromoInfoJson(String promoInfoJson) {
		this.promoInfoJson = promoInfoJson;
	}
	
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setIpAddress(getClientInfoDTO().getIpAddress());
		trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		trackInfoDTO = TrackInfoUtil.updateTrackInfo(trackInfoDTO, request);
		trackInfoDTO.setOriginChannelId(TrackInfoUtil.getWebOriginSalesChannel(request));
		trackInfoDTO.setAppIndicator(TrackInfoUtil.getAppIndicatorEnum(trackInfoDTO.getOriginChannelId()));
		return trackInfoDTO;
	}
	
	protected ApplicationEngine getApplicationEngine(int salesChannelCode) {

		ApplicationEngine appIndicator = null;
		switch (salesChannelCode) {
		case SalesChannelsUtil.SALES_CHANNEL_AGENT:
			appIndicator = ApplicationEngine.XBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_PUBLIC:
			appIndicator = ApplicationEngine.IBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_WEB:
			appIndicator = ApplicationEngine.IBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES:
			appIndicator = ApplicationEngine.WS;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_LCC:
			appIndicator = ApplicationEngine.LCC;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_IOS:
			appIndicator = ApplicationEngine.IOS;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_ANDROID:
			appIndicator = ApplicationEngine.ANDROID;
			break;
		default:
			break;
		}

		return appIndicator;
	}

}
