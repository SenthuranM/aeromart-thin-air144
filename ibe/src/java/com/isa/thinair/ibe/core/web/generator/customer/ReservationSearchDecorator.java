package com.isa.thinair.ibe.core.web.generator.customer;

import java.util.Collection;

import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * Decorates the search results based on the decoration policy.
 * 
 * @author Nilindra Fernando
 * 
 */
public class ReservationSearchDecorator {

	private Collection<ReservationListTO> colReservationListTO;

	public ReservationSearchDecorator(Collection<ReservationListTO> colReservationListTO) {
		this.colReservationListTO = colReservationListTO;
	}

	public String decorateForV1() {
		StringBuilder arrayScript = new StringBuilder();

		if (colReservationListTO != null && colReservationListTO.size() > 0) {
			int pnrCount = 0;
			String strDeptDate;
			String returnDate;
			String segStatus;
			String originNDest;
			boolean blnGroupPNRNo = false;

			for (ReservationListTO reservationListTO : colReservationListTO) {
				blnGroupPNRNo = false;
				arrayScript.append("arrRes[" + pnrCount + "] = new Array();");
				arrayScript.append("arrRes[" + pnrCount + "][0] = '" + reservationListTO.getPnrNo() + "';");
				arrayScript.append("arrRes[" + pnrCount + "][2] = '" + reservationListTO.getPaxName() + "';");
				arrayScript.append("arrRes[" + pnrCount + "][1]= new Array();");
				arrayScript.append("arrRes[" + pnrCount + "][3] = '" + reservationListTO.getPnrStatus() + "';");
				if (reservationListTO.getOriginatorPnr() != null && !"".equals(reservationListTO.getOriginatorPnr().trim())) {
					blnGroupPNRNo = true;
				}
				arrayScript.append("arrRes[" + pnrCount + "][4] = " + blnGroupPNRNo + ";");

				int intSegmentCount = 0;

				for (FlightInfoTO flightInfoTO : reservationListTO.getFlightInfo()) {
					originNDest = flightInfoTO.getOrignNDest();

					if (!flightInfoTO.isOpenReturnSegment()) {
						strDeptDate = flightInfoTO.getDepartureDate() + " " + flightInfoTO.getDepartureTime();
						returnDate = flightInfoTO.getArrivalDate() + " " + flightInfoTO.getArrivalTime();
						segStatus = flightInfoTO.getStatus();
					} else {
						strDeptDate = "&nbsp;";
						returnDate = "&nbsp;";

						if (BeanUtils.nullHandler(flightInfoTO.getStatus()).length() == 0) {
							segStatus = "&nbsp;";
						} else {
							segStatus = flightInfoTO.getStatus();
						}
					}

					arrayScript.append("arrRes[" + pnrCount + "][1][" + intSegmentCount + "] = new Array('" + originNDest + "',"
							+ "'" + strDeptDate + "'," + "'" + returnDate + "'," + "'" + segStatus + "'," + "'');"); // last
																														// element
																														// is
																														// alert

					intSegmentCount++;
				}
				pnrCount++;
			}
		}
		return arrayScript.toString();
	}

}
