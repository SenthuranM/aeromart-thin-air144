package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ManageBookingDetailV2Action extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ManageBookingV2Action.class);

	private boolean success = true;

	private String messageTxt;

	private HashMap<String, String> jsonLabel;

	private Map<String, String> errorInfo;

	private boolean imageCapchaEnable;

	private boolean isAllowLoadResUsingPaxLastName = false;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;

		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "Common", "Form", "PgManageBooking" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);

			if (AppSysParamsUtil.isCaptchaEnabledForUnregisterdUser()) {
				imageCapchaEnable = true;
			} else {
				imageCapchaEnable = false;
			}

			if (AppSysParamsUtil.isAllowLoadReservationsUsingPaxNameInIBE()) {
				isAllowLoadResUsingPaxLastName = true;
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowReservationListAction==>" + ex);
		}
		return forward;

	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public boolean isImageCapchaEnable() {
		return imageCapchaEnable;
	}

	public boolean isAllowLoadResUsingPaxLastName() {
		return isAllowLoadResUsingPaxLastName;
	}

	public void setAllowLoadResUsingPaxLastName(boolean isAllowLoadResUsingPaxLastName) {
		this.isAllowLoadResUsingPaxLastName = isAllowLoadResUsingPaxLastName;
	}

}
