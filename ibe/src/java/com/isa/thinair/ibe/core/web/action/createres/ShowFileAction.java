package com.isa.thinair.ibe.core.web.action.createres;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.ibe.core.web.constants.StrutsConstants;

/**
 * @author Thushara Fernando
 * 
 *         This is a special action file to show JSP files directly with out any presentations logic being executed.
 *         File Name itself is used instead of separate result value to make mappings simple. So in the result
 *         annotation filename itself is specified as name as well as the value.
 * 
 *         eg : @Result(name=S2Constants.Jsp.Common.EXAMPLE_FILE, value=S2Constants.Jsp.Common.EXAMPLE_FILE)
 * 
 * 
 *         To work this <constant name="struts.enable.DynamicMethodInvocation" value="true" /> should be set in the
 *         struts.xml file
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({@Result(name = StrutsConstants.Jsp.Respro.DISPLAY_MEALS, value = StrutsConstants.Jsp.Respro.DISPLAY_MEALS)

})
public class ShowFileAction {

	public String displayMeals() {
		return StrutsConstants.Jsp.Respro.DISPLAY_MEALS;
	};

}
