/**
 * 
 */
package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

/**
 * @author Dilan Anuruddha
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciSSRAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(InterlineAnciMealAction.class);

	private boolean success = true;

	private String messageTxt;

	private List<LCCFlightSegmentSSRDTO> flightSegmentSSRs;

	private String oldAllSegments;

	private boolean modifySegment;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {

			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			String strTxnIdntifier = ibeResInfo.getTransactionId();

			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}

			SYSTEM system = ibeResInfo.getSelectedSystem();

			String selectedLanguage = SessionUtil.getLanguage(request);

			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams(),
					(modifySegment && AppSysParamsUtil.isRequoteEnabled()));
			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();

			flightSegmentSSRs = ModuleServiceLocator.getAirproxyAncillaryBD().getSpecialServiceRequests(flightSegmentTOs,
					strTxnIdntifier, system, selectedLanguage, TrackInfoUtil.getBasicTrackInfo(request), null, false, false, false, false);

		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("SSR REQ ERROR", e);
		}
		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public List<LCCFlightSegmentSSRDTO> getFlightSegmentSSRs() {
		return flightSegmentSSRs;
	}

	public void setFlightSegmentSSRs(List<LCCFlightSegmentSSRDTO> flightSegmentSSRs) {
		this.flightSegmentSSRs = flightSegmentSSRs;
	}

	public String getOldAllSegments() {
		return oldAllSegments;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}
}
