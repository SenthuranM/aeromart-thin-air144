package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.BookingPaxType;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.PutOnHoldBeforePaymentDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_CONTAINER, value = StrutsConstants.Jsp.Respro.INTERLINE_CONTAINER),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_CONTAINER_V3, value = StrutsConstants.Jsp.Respro.INTERLINE_CONTAINER_V3),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class LoadContainerAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(LoadContainerAction.class);

	private boolean modifyAncillary = false;

	private String selectedAncillary;

	private String totalAnciCharges;

	private String paxJson;

	private String contactInfoJson;

	private String pnr;

	private String version;

	private String oldAllSegments;

	private boolean makePayment;

	private boolean payFortOfflinePayment;
	
	private boolean payFortPayATHomeOfflinePayment;
	
	private String balanceQueryData;
	
	private String payFortMobileNumber = "";

	private String payFortEmail = "";

	private String jsonOnds;
	
	private boolean insuranceBooked;
	
	private String pgwPaymentMobileNumber;
	
	private String pgwPaymentEmail;
	
	private String pgwPaymentCustomerName;

	public String execute() throws JSONException {
		/*
		 * We are tracking search parameters because customers are complaining, they are receiving wrong flights
		 * selection
		 */
		logSearchParameters();
		/* Itinerary Page Refresh Handle */
		if (!isValidReservationFlowRequest()) {
			return StrutsConstants.Result.ERROR;
		}
		if (modifyAncillary) {
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			SystemUtil.setCommonParameters(request, getCommonParams());
			if (resInfo.isFromPostCardDetails()) { // clicked the back button
				try {
					SeatMapUtil.releaseBlockedSeats(request);
				} catch (Exception e1) {
					log.warn("seat releasing failed", e1);
				}
				// request.getSession().invalidate();
				// return StrutsConstants.Result.SESSION_EXPIRED;
			}
		}
		/* Cirteo pixel requirement to pass the product ID */
		request.setAttribute(ReservationWebConstnts.REQ_PRODUCT_ID, request.getParameter("hidProductID"));
		if (getSearchParams() == null || StringUtil.isNullOrEmpty((this.getSearchParams().getPaxType()))) {
			request.setAttribute(ReservationWebConstnts.REQ_PAX_CATEGORY, "'" + BookingPaxType.ANY + "'");
		} else {
			request.setAttribute(ReservationWebConstnts.REQ_PAX_CATEGORY, "'" + this.getSearchParams().getPaxType() + "'");
		}
		request.setAttribute(ReservationWebConstnts.PARAM_DEFAULT_LANG,
				"'" + IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_LANGUAGE) + "'");
		String forward;

		request.setAttribute(CustomerWebConstants.LINKEDIN_APP_ID, AppSysParamsUtil.getLinkedInApiIDForSocialSeating());
		request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
				CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));
		request.setAttribute(ReservationWebConstnts.IS_MULTYCITY_SELECTED, request.getSession().getAttribute(ReservationWebConstnts.IS_MULTYCITY_SELECTED));
		request.setAttribute(ReservationWebConstnts.PNR_OND_GROUPS, jsonOnds);
		request.setAttribute(ReservationWebConstnts.PARAM_IMAGE_HOST_PATH, AppSysParamsUtil.getImageUploadPath());
		request.setAttribute(ReservationWebConstnts.LMS_CURRENCY_TO_POINTS,
				AppSysParamsUtil.getCurrencyToLoyaltyPointsConversionRate());
		
		
		if (SessionUtil.isNewDesignVersion(request)) {
			if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
				request.setAttribute(ReservationWebConstnts.PARAM_OND_SOURCE, AppSysParamsUtil.getDynamicOndListUrl());
			}
			request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
					CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));
			forward = StrutsConstants.Jsp.Respro.INTERLINE_CONTAINER_V3;
		} else {
			forward = StrutsConstants.Jsp.Respro.INTERLINE_CONTAINER;
		}
		/*
		 * Create new session object Override the Payment onhold data Handling browser refresh, back button, payment
		 * amount change
		 */
		if (!RequestParameterUtil.isPaymentRetry(request)) {
			SessionUtil.setPutOnHoldBeforePaymentDTO(request, new PutOnHoldBeforePaymentDTO());
		} else {
			/* Payment related error message */
			addErrorMessage();
			addRequiredSearchParameters();
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			if (resInfo != null && resInfo.getDiscountInfo() != null) {
				DiscountedFareDetails promoInfo = resInfo.getDiscountInfo();
				request.setAttribute(ReservationWebConstnts.PROMO_INFO_JSON, JSONUtil.serialize(promoInfo));
			}
		}
		return forward;
	}

	private boolean isValidReservationFlowRequest() {
		boolean valid = true;
		IBEReservationPostDTO ibeReservationPostDTO = SessionUtil.getIbeReservationPostDTO(request);
		String reservationPNR = null;
		String msg = "";
		if (ibeReservationPostDTO != null) {
			reservationPNR = ibeReservationPostDTO.getPnr();
		}

		boolean isPaymentSuccess = SessionUtil.isResponseReceivedFromPaymentGateway(request);
		if (isPaymentSuccess) {
			log.info("PNR :" + reservationPNR + " isPaymentSuccess :" + isPaymentSuccess + " SessionID :"
					+ request.getRequestedSessionId());
			if (reservationPNR != null && !reservationPNR.trim().isEmpty()) {
				msg = CommonUtil.getExistingReservationMsg(request, reservationPNR);
			} else {
				msg = CommonUtil.getRefundMessage(SessionUtil.getLanguage(request));
			}
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
			SystemUtil.setCommonParameters(request, getCommonParams());
			valid = false;
		}
		return valid;
	}

	private void logSearchParameters() {
		FlightSearchDTO search = getSearchParams();
		if (search != null) {
			StringBuilder logStr = new StringBuilder();
			logStr.append("From :").append(search.getFromAirport()).append("&");
			logStr.append("To :").append(search.getToAirport()).append("&");
			logStr.append("Departure Date:").append(search.getDepartureDate()).append("&");
			logStr.append("Return Date:").append(search.getReturnDate()).append("&");
			logStr.append("Adult Count:").append(search.getAdultCount()).append("&");
			logStr.append("SessionID:").append(request.getRequestedSessionId());
			log.info(logStr.toString());
		}
	}

	private void addRequiredSearchParameters() {
		IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
		Collection<ReservationPaxTO> paxList = postPayDTO.getPaxList();
		int adultCount = 0;
		int childCount = 0;
		int infantCount = 0;
		if (paxList != null) {
			for (ReservationPaxTO pax : paxList) {
				if (PaxTypeTO.ADULT.equals(pax.getPaxType())) {
					adultCount++;
				} else if (PaxTypeTO.CHILD.equals(pax.getPaxType())) {
					childCount++;
				} else if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					infantCount++;
				}
			}

		}
		if (getSearchParams() == null) {
			searchParams = new FlightSearchDTO();
		}
		searchParams.setAdultCount(adultCount);
		searchParams.setChildCount(childCount);
		searchParams.setInfantCount(infantCount);
		// Set navigation parameters
		SystemUtil.setCommonParameters(request, getCommonParams());
	}

	private void addErrorMessage() {
		String errorCode = request.getParameter(ReservationWebConstnts.MSG_ERROR_CODE);
		if (errorCode != null && !errorCode.trim().isEmpty()) {
			errorCode = ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR + errorCode;
		} else {
			errorCode = ReservationWebConstnts.REQ_DEFAULT_ERROR;
		}
		request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, ReservationUtil.getServerErrorMessage(request, errorCode));
		addCreditCardTransactionDetail();
	}

	// TODO Move HTML to JSP
	private void addCreditCardTransactionDetail() {
		IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
		if (postPayDTO != null && postPayDTO.getIpgResponseDTO() != null) {
			IPGTransactionResultDTO ipgTransactionResultDTO = postPayDTO.getIpgResponseDTO().getIpgTransactionResultDTO();
			if (ipgTransactionResultDTO != null && ipgTransactionResultDTO.isShowTransactionDetail()) {
				String msgLabel = "PgInterlineConfirm_";
				String strLanguage = SessionUtil.getLanguage(request);
				StringBuilder messageBuilder = new StringBuilder();
				messageBuilder.append("<b>").append(request.getAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE))
						.append("</b>");
				messageBuilder.append("<br/><br/>");
				messageBuilder.append("<b>").append(I18NUtil.getMessage(msgLabel + "transaction.header", strLanguage))
						.append("</b>");
				messageBuilder.append("<table border='0' width='320px'>");
				if (ipgTransactionResultDTO.getTransactionTime() != null) {
					messageBuilder.append("<tr>");
					messageBuilder.append("<td>").append(I18NUtil.getMessage(msgLabel + "transaction.time", strLanguage))
							.append("</td>");
					messageBuilder.append("<td><b>:").append(ipgTransactionResultDTO.getTransactionTime()).append("</b></td>");
					messageBuilder.append("</tr>");
				}

				if (ipgTransactionResultDTO.getTransactionStatus() != null) {
					messageBuilder.append("<tr>");
					messageBuilder.append("<td>").append(I18NUtil.getMessage(msgLabel + "transaction.status", strLanguage))
							.append("</td>");
					if (IPGResponseDTO.STATUS_ACCEPTED.equalsIgnoreCase(ipgTransactionResultDTO.getTransactionStatus())) {
						messageBuilder.append("<td><b>:")
								.append(I18NUtil.getMessage(msgLabel + "transaction.status.success", strLanguage))
								.append("</b></td>");
					} else {
						messageBuilder.append("<td><b>:")
								.append(I18NUtil.getMessage(msgLabel + "transaction.status.reject", strLanguage))
								.append("</b></td>");
					}
					messageBuilder.append("</tr>");
				}

				if (ipgTransactionResultDTO.getAmount() != null && ipgTransactionResultDTO.getCurrency() != null) {
					messageBuilder.append("<tr>");
					messageBuilder.append("<td>").append(I18NUtil.getMessage(msgLabel + "transaction.amount", strLanguage))
							.append("</td>");
					messageBuilder.append("<td><b>:").append(ipgTransactionResultDTO.getCurrency() + " ")
							.append(ipgTransactionResultDTO.getAmount()).append("</b></td>");
					messageBuilder.append("</tr>");
				}

				if (ipgTransactionResultDTO.getReferenceID() != null) {
					messageBuilder.append("<tr>");
					messageBuilder.append("<td>").append(I18NUtil.getMessage(msgLabel + "transaction.referenceID", strLanguage))
							.append("</td>");
					messageBuilder.append("<td><b>:").append(ipgTransactionResultDTO.getReferenceID()).append("</b></td>");
					messageBuilder.append("</tr>");
				}

				if (ipgTransactionResultDTO.getMerchantTrackID() != null) {
					messageBuilder.append("<tr>");
					messageBuilder.append("<td>")
							.append(I18NUtil.getMessage(msgLabel + "transaction.merchanttrackID", strLanguage)).append("</td>");
					messageBuilder.append("<td><b>:").append(ipgTransactionResultDTO.getMerchantTrackID()).append("</b></td>");
					messageBuilder.append("</tr>");
				}
				messageBuilder.append("</table><br/>");
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, messageBuilder.toString());
			}
		}
		postPayDTO.setIpgResponseDTO(null);
	}
	

	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public String getSelectedAncillary() {
		return selectedAncillary;
	}

	public void setSelectedAncillary(String selectedAncillary) {
		this.selectedAncillary = selectedAncillary;
	}

	public String getPaxJson() {
		return paxJson;
	}

	public void setPaxJson(String paxJson) {
		this.paxJson = paxJson;
	}

	public String getContactInfoJson() {
		return contactInfoJson;
	}

	public void setContactInfoJson(String contactInfoJson) {
		this.contactInfoJson = contactInfoJson;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getOldAllSegments() {
		return oldAllSegments;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public boolean isMakePayment() {
		return makePayment;
	}

	public void setMakePayment(boolean makePayment) {
		this.makePayment = makePayment;
	}

	public String getBalanceQueryData() {
		return balanceQueryData;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	public String getTotalAnciCharges() {
		return totalAnciCharges;
	}

	public void setTotalAnciCharges(String totalAnciCharges) {
		this.totalAnciCharges = totalAnciCharges;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public boolean isPayFortOfflinePayment() {
		return payFortOfflinePayment;
	}

	public void setPayFortOfflinePayment(boolean payFortOfflinePayment) {
		this.payFortOfflinePayment = payFortOfflinePayment;
	}

	public boolean isPayFortPayATHomeOfflinePayment() {
		return payFortPayATHomeOfflinePayment;
	}

	public void setPayFortPayATHomeOfflinePayment(boolean payFortPayATHomeOfflinePayment) {
		this.payFortPayATHomeOfflinePayment = payFortPayATHomeOfflinePayment;
	}

	public String getPayFortMobileNumber() {
		return payFortMobileNumber;
	}

	public void setPayFortMobileNumber(String payFortMobileNumber) {
		this.payFortMobileNumber = payFortMobileNumber;
	}

	public String getPayFortEmail() {
		return payFortEmail;
	}

	public void setPayFortEmail(String payFortEmail) {
		this.payFortEmail = payFortEmail;
	}

	public boolean isInsuranceBooked() {
		return insuranceBooked;
	}

	public void setInsuranceBooked(boolean insuranceBooked) {
		this.insuranceBooked = insuranceBooked;
	}

	public String getPgwPaymentMobileNumber() {
		return pgwPaymentMobileNumber;
	}

	public void setPgwPaymentMobileNumber(String pgwPaymentMobileNumber) {
		this.pgwPaymentMobileNumber = pgwPaymentMobileNumber;
	}

	public String getPgwPaymentEmail() {
		return pgwPaymentEmail;
	}

	public void setPgwPaymentEmail(String pgwPaymentEmail) {
		this.pgwPaymentEmail = pgwPaymentEmail;
	}

	public String getPgwPaymentCustomerName() {
		return pgwPaymentCustomerName;
	}

	public void setPgwPaymentCustomerName(String pgwPaymentCustomerName) {
		this.pgwPaymentCustomerName = pgwPaymentCustomerName;
	}	
	
}
