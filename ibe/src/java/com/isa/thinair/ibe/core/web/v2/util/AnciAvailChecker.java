package com.isa.thinair.ibe.core.web.v2.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

class AnciAvailChecker {
	private Map<String, AvailDTO> segmentAvailabilityMap;

	public AnciAvailChecker() {
		segmentAvailabilityMap = new HashMap<String, AvailDTO>();
	}

	private boolean isSeatSelectedByAll(String fltRefNo) {
		return getAvailDTO(fltRefNo).seat;
	}

	private boolean isMealSelectedByAll(String fltRefNo) {
		return getAvailDTO(fltRefNo).meal;
	}

	private boolean isBaggageSelectedByAll(String fltRefNo) {
		return getAvailDTO(fltRefNo).baggage;
	}

	private boolean isSSRSelectedByAll(String fltRefNo) {
		return getAvailDTO(fltRefNo).ssr;
	}

	private boolean isAirportServiceByAll(String fltrefNo) {
		return getAvailDTO(fltrefNo).airportService;
	}

	private boolean isAirportTransferByAll(String fltrefNo) {
		return getAvailDTO(fltrefNo).airportTransfer;
	}

	private List<String> getServiceAirportList(String fltRefNo, String ancilaryType) {
		if (ancilaryType.equals(LCCAncillaryType.AIRPORT_SERVICE)) {
			return getAvailDTO(fltRefNo).serviceAirportList;
		} else if (ancilaryType.equals(LCCAncillaryType.AIRPORT_TRANSFER)) {
			return getAvailDTO(fltRefNo).transferAirportList;
		}
		return null;
	}

	private AvailDTO getAvailDTO(String fltRefNo) {
		if (segmentAvailabilityMap.containsKey(fltRefNo)) {
			return segmentAvailabilityMap.get(fltRefNo);
		} else {
			return new AvailDTO(false);
		}
	}

	protected void recordAvailability(Set<LCCClientReservationPax> paxSet) {
		if (paxSet != null) {
			for (LCCClientReservationPax pax : paxSet) {
				if (pax.getSelectedAncillaries() != null) {
					for (LCCSelectedSegmentAncillaryDTO segAnci : pax.getSelectedAncillaries()) {
						String segment = segAnci.getFlightSegmentTO().getFlightRefNumber();
						if (!segmentAvailabilityMap.containsKey(segment)) {
							segmentAvailabilityMap.put(segment, new AvailDTO());
						}
						AvailDTO av = segmentAvailabilityMap.get(segment);

						LCCAirSeatDTO seatDTO = segAnci.getAirSeatDTO();
						if (seatDTO == null || seatDTO.getSeatNumber() == null || seatDTO.getSeatNumber().equals("")) {
							av.seat = false;
						}

						List<LCCMealDTO> mealDTOs = segAnci.getMealDTOs();
						if (mealDTOs == null || mealDTOs.size() == 0) {
							av.meal = false;
						}

						List<LCCBaggageDTO> baggageDTOs = segAnci.getBaggageDTOs();
						if (baggageDTOs == null || baggageDTOs.size() == 0) {
							av.baggage = false;
						}
						List<LCCSpecialServiceRequestDTO> ssrDTOs = segAnci.getSpecialServiceRequestDTOs();
						if (ssrDTOs == null || ssrDTOs.size() == 0) {
							av.ssr = false;
						}

						av.airportService = isAirportLevelServiceAddedForAllAirportsInSegment(segAnci.getAirportServiceDTOs(),
								segAnci.getFlightSegmentTO().getSegmentCode(), av, false);
						
						if(av.airportTransfer){
							av.airportTransfer = isAirportLevelServiceAddedForAllAirportsInSegment(segAnci.getAirportTransferDTOs(),
									segAnci.getFlightSegmentTO().getSegmentCode(), av, true);
						}else{
							isAirportLevelServiceAddedForAllAirportsInSegment(segAnci.getAirportTransferDTOs(),
									segAnci.getFlightSegmentTO().getSegmentCode(), av, true);
						}

					}
				}
			}
		}
	}

	protected boolean isAirportLevelServiceAddedForAllAirportsInSegment(List<LCCAirportServiceDTO> airportServiceDTOs,
			String segmentCode, AvailDTO av, boolean airportTransfer) {
		boolean isAdded = true;

		String[] allAirportsArr = segmentCode.split("/");
		// get only departure & arrival airport from segment code
		String[] airportArr = { allAirportsArr[0], allAirportsArr[allAirportsArr.length - 1] };
		Map<String, Integer> airportServiceCount = new HashMap<String, Integer>();

		for (String airport : airportArr) {
			if (airportServiceCount.get(airport) == null) {
				airportServiceCount.put(airport, 0);
			}
			if (airportServiceDTOs != null) {
				for (LCCAirportServiceDTO lccAirportServiceDTO : airportServiceDTOs) {
					if (airport.equals(lccAirportServiceDTO.getAirportCode())) {
						airportServiceCount.put(airport, airportServiceCount.get(airport) + 1);
					}
				}
			}
		}

		for (Entry<String, Integer> entry : airportServiceCount.entrySet()) {
			if (entry.getValue() == 0) {
				isAdded = false;
				if (airportTransfer) {
					av.transferAirportList.add(entry.getKey());
				}
			} else {
				if (!airportTransfer) {
					isAdded = true;
					av.serviceAirportList.add(entry.getKey());
				}
			}
		}

		return isAdded;
	}

	private class AvailDTO {
		private boolean seat = true;
		private boolean meal = true;
		private boolean ssr = true;
		private boolean baggage = true;
		private boolean airportService = true;
		private boolean airportTransfer = true;
		private List<String> serviceAirportList = null;
		private List<String> transferAirportList = null;

		public AvailDTO() {
			serviceAirportList = new ArrayList<String>();
			transferAirportList = new ArrayList<String>();
		};

		private AvailDTO(boolean selected) {
			this.seat = selected;
			this.meal = selected;
			this.ssr = selected;
			this.baggage = selected;
			this.airportService = selected;
			this.airportTransfer = selected;
			serviceAirportList = new ArrayList<String>();
			transferAirportList = new ArrayList<String>();
		}
	}

	protected boolean isSelectedByAll(String anciType, String fltRefNo) {
		if (anciType.equals(LCCAncillaryType.MEALS)) {
			return isMealSelectedByAll(fltRefNo);
		} else if (anciType.equals(LCCAncillaryType.BAGGAGE)) {
			return isBaggageSelectedByAll(fltRefNo);
		} else if (anciType.equals(LCCAncillaryType.SEAT_MAP)) {
			return isSeatSelectedByAll(fltRefNo);
		} else if (anciType.equals(LCCAncillaryType.SSR)) {
			return isSSRSelectedByAll(fltRefNo);
		} else if (anciType.equals(LCCAncillaryType.AIRPORT_SERVICE)) {
			return isAirportServiceByAll(fltRefNo);
		}
		return false;
	}

	protected List<String> getServicesNotAttachedAirportsInASegment(String fltRefNo, String ancilaryType) {
		return getServiceAirportList(fltRefNo, ancilaryType);
	}

}
