/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.ibe.core.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.isa.thinair.ibe.api.dto.Kiosk;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;

/**
 * Custom Authentication Filter
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CustomAuthenticatonFilter implements Filter {
	/**
	 * Filter authentication
	 */
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain) throws ServletException, IOException {

		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpSession session = request.getSession(false);

		if (session != null) {
			Kiosk kiosk = (Kiosk) session.getAttribute(CustomerWebConstants.REQ_LOGGED_TYPE);
			String strKskLogin = request.getParameter("hdnKioskLogin");

			if (kiosk != null && strKskLogin != null && strKskLogin.trim().equals("KSKLOGIN")) {
				String strName = request.getParameter("j_username");
				String strPass = request.getParameter("j_password");

				if (strName != null && !strName.equals("")) {
					kiosk.setUserName(strName);
				}

				if (strPass != null && !strPass.equals("")) {
					kiosk.setPassword(strPass);
				}

				session.setAttribute(CustomerWebConstants.REQ_LOGGED_TYPE, kiosk);
			}

			// Kiosk Application
			if (kiosk != null) {
				if (SessionUtil.isCustomerLoggedIn(request)) {
					ForceLoginInvoker.kioskLogin(kiosk.getUserName(), kiosk.getPassword(), SessionUtil.getCustomerEmail(request),
							SessionUtil.getCustomerPassword(request));
				} else {
					ForceLoginInvoker.kioskDefaultLogin(kiosk.getUserName(), kiosk.getPassword());
				}
				// IBE Application
			} else {
				if (SessionUtil.isCustomerLoggedIn(request)) {
					ForceLoginInvoker.login(SessionUtil.getCustomerEmail(request), SessionUtil.getCustomerPassword(request));
				} else {
					ForceLoginInvoker.defaultLogin();
				}

			}
		} else {
			ForceLoginInvoker.defaultLogin();
		}

		chain.doFilter(arg0, arg1);
		
		ForceLoginInvoker.close();
	}

	/**
	 * Initialise
	 */
	public void init(FilterConfig arg0) throws ServletException {

	}

	/**
	 * Destroy the filter
	 */
	public void destroy() {
	}
}
