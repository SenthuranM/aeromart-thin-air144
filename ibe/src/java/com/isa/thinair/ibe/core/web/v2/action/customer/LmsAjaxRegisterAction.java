package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSSmartButtonWebServiceInvoker;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = StrutsConstants.Result.SUCCESS)
public class LmsAjaxRegisterAction extends IBEBaseAction {

	private LmsMemberDTO lmsDetails;

	private boolean loginSuccess;

	private boolean success = true;

	private String messageTxt = "";

	private Map<String, String> errorInfo;

	private boolean registration = false;

	private boolean ibeAccountExists = false;

	private boolean lmsNameMismatch = false;

	private boolean skipNameMatching = false;

	private String loyaltyCity = "";

	private String loyaltyAccountNo = "";

	private String loyaltyDateofBirth;

	private boolean mashreqSuccess = false;

	private boolean lmsSuccess = false;

	private boolean enrollLms = false;

	private boolean enrollLoyalty = false;

	private static Log log = LogFactory.getLog(LmsAjaxRegisterAction.class);

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			Customer modelCustomer = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			boolean alreadyLmsmember = false;
			if (modelCustomer != null) {
				char lmsuser = modelCustomer.getIsLmsMember();
				if (lmsuser == 'Y') {
					alreadyLmsmember = true;
				}
			}
			LoyaltyCustomerProfile loyaltyProfile = null;
			if (AppSysParamsUtil.isLMSEnabled()) {
				if (enrollLms) {
					// AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
					Locale locale = new Locale(SessionUtil.getLanguage(request));
					Integer customerId = SessionUtil.getCustomerId(request);
					// Customer customerr = customerDelegate.getCustomer(customerId);
					lmsDetails = LmsCommonUtil.getLmsByCustomer(modelCustomer, lmsDetails);
					// TODO:CP move business logic to aircustomer
					LmsMember lmsMember = CustomerUtilV2.getModelLmsMember(lmsDetails, customerId);
					LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
					LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
					LmsCommonUtil.lmsEnroll(lmsMember, SessionUtil.getCarrier(request), loyaltyManagementBD, lmsMemberDelegate,
							locale, false);
					lmsDetails.setEmailStatus("N");
					setLoginSuccess(true);
					lmsSuccess = true;
				}
			}
			if (AppSysParamsUtil.isLoyalityEnabled()) {
				if (loyaltyAccountNo != null && loyaltyAccountNo.length() > 0 && enrollLoyalty) {
					LoyaltyCustomerServiceBD loyaltyCustomerBD = ModuleServiceLocator.getLoyaltyCustomerBD();
					LoyaltyCustomerProfile loyaltyCustomerProfile = CustomerUtilV2.getLoyaltyCustomerProfile(modelCustomer,
							loyaltyAccountNo);
					loyaltyCustomerProfile.setCity(loyaltyCity);
					String birthDay = getLoyaltyDateofBirth();
					SimpleDateFormat dateFormat = null;
					if (!birthDay.equals("")) {
						if (birthDay.indexOf(' ') != -1) {
							birthDay = birthDay.substring(0, birthDay.indexOf(' '));
						}

						if (birthDay.indexOf('-') != -1) {
							dateFormat = new SimpleDateFormat("dd-MM-yyyy");
						}
						if (birthDay.indexOf('/') != -1) {
							dateFormat = new SimpleDateFormat("dd/MM/yyyy");
						}

						Date dateOfBirth = null;
						try {
							dateOfBirth = dateFormat.parse(birthDay);
						} catch (Exception ex) {
						}
						loyaltyCustomerProfile.setDateOfBirth(dateOfBirth);
					}
					loyaltyProfile = loyaltyCustomerBD.getLoyaltyCustomerByExample(loyaltyCustomerProfile);
					if (loyaltyProfile == null)
						throw new ModuleException("aircustomer.loyalty.account.not.exist");
					else {
						if (loyaltyProfile.getCustomerId() != null)
							throw new ModuleException("aircustomer.loyalty.account.already.linked");
					}
					if (AppSysParamsUtil.isIntegrateMashreqWithLMS() && alreadyLmsmember) {
						LmsMember lmsMember = ModuleServiceLocator.getLmsMemberServiceBD().getLmsMember(
								modelCustomer.getEmailId());
						ModuleServiceLocator.getWSClientBD().addMemberAccountId(
								LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), modelCustomer.getEmailId(),
								lmsMember.getMemberExternalId(), "Mid", loyaltyAccountNo);

						log.debug("Successfully called Smart Button API to link Mashreq Account " + loyaltyAccountNo
								+ " and LMS account " + modelCustomer.getEmailId());
					}
					if (loyaltyProfile != null) {
						loyaltyProfile.setCustomerId(modelCustomer.getCustomerId());
						loyaltyCustomerBD.saveOrUpdate(loyaltyProfile);
						SessionUtil.setLoyaltyAccountNo(request, loyaltyProfile.getLoyaltyAccountNo());
					}
					mashreqSuccess = true;
				}
			}
		} catch (ModuleException me) {
			setLoginSuccess(false);
			log.error("LmsAjaxRegister==>", me);
			// do proper error handling
			if (me.getExceptionCode().equals("aircustomer.logic.loginid.already.exist")) {
				success = true;
				messageTxt = I18NUtil.getMessage(CustomerWebConstants.CUSTOMER_EXISTS, SessionUtil.getLanguage(request));
			} else if (me.getExceptionCode().equals("aircustomer.loyalty.account.not.exist")) {
				success = true;
				messageTxt = I18NUtil.getMessage(CustomerWebConstants.LOYALTY_ACCOUNT_NOT_EXISTS,
						SessionUtil.getLanguage(request));
			} else if (me.getExceptionCode().equals("aircustomer.loyalty.account.already.linked")) {
				success = true;
				messageTxt = I18NUtil.getMessage(CustomerWebConstants.LOYALTY_ACCOUNT_ALREADY_LINKED,
						SessionUtil.getLanguage(request));
			} else {
				success = false;
				messageTxt = me.getMessageString();
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("CustomerRegisterAction==>", ex);
		}
		return forward;
	}

	public String validateHeadReffered() throws ModuleException {
		// TODO:CP move business logic to aircustomer

		if (AppSysParamsUtil.isLMSEnabled()) {
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			LmsMemberServiceBD lmsMemberServiceBD = ModuleServiceLocator.getLmsMemberServiceBD();
			setErrorInfo(ErrorMessageUtil.getCustomerProfileErrors(request));
			if (lmsDetails.getRefferedEmail() != null && !lmsDetails.getRefferedEmail().equals("")) {
				if (!LmsCommonUtil.isNonMandAccountAvailable(lmsDetails.getRefferedEmail(), loyaltyManagementBD)) {
					lmsDetails.setRefferedEmail("N");
				}
			}
			if (lmsDetails.getHeadOFEmailId() != null && !lmsDetails.getHeadOFEmailId().equals("")) {
				LoyaltyMemberCoreDTO member = loyaltyManagementBD.getLoyaltyMemberCoreDetails(lmsDetails.getHeadOFEmailId());
				if (!LmsCommonUtil.isValidFamilyHeadAccount(lmsDetails.getHeadOFEmailId(), member)) {
					lmsDetails.setHeadOFEmailId("N");
				}
			}
			if (lmsDetails.getEmailId() != null && !lmsDetails.getEmailId().equals("")) {

				Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(lmsDetails.getEmailId());

				if (registration) {
					if (customer != null) {
						ibeAccountExists = true;
						messageTxt = I18NUtil.getMessage(CustomerWebConstants.CUSTOMER_EXISTS, SessionUtil.getLanguage(request));
						return StrutsConstants.Result.SUCCESS;
					}
				}

				if (customer != null) {
					if (lmsDetails.getFirstName() == null || "".equals(lmsDetails.getFirstName())) {
						lmsDetails.setFirstName(customer.getFirstName());
					}
					if (lmsDetails.getLastName() == null || "".equals(lmsDetails.getLastName())) {
						lmsDetails.setLastName(customer.getLastName());
					}
				}

				if (!skipNameMatching && !CustomerUtilV2.isExistingLMSNameMatches(lmsDetails)) {
					lmsNameMismatch = true;
					if (!registration) {
						messageTxt = I18NUtil.getMessage(CustomerWebConstants.LOYALTY_JOIN_NOT_ALLOW,
								SessionUtil.getLanguage(request));
					} else {
						messageTxt = I18NUtil.getMessage(CustomerWebConstants.LOYALTY_ACCOUNT_NAMES_NOT_MATCHES,
								SessionUtil.getLanguage(request));
					}
					return StrutsConstants.Result.SUCCESS;
				}

				if (!LmsCommonUtil.availableForCarrier(lmsDetails.getEmailId(), lmsMemberServiceBD)) {
					if (LmsCommonUtil.isNonMandAccountAvailable(lmsDetails.getEmailId(), loyaltyManagementBD)) {
						lmsDetails.setEmailId("Y");
					}
				}
			}
		}
		return StrutsConstants.Result.SUCCESS;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public String getLoyaltyCity() {
		return loyaltyCity;
	}

	public void setLoyaltyCity(String loyaltyCity) {
		this.loyaltyCity = loyaltyCity;
	}

	public String getLoyaltyAccountNo() {
		return loyaltyAccountNo;
	}

	public void setLoyaltyAccountNo(String loyaltyAccountNo) {
		this.loyaltyAccountNo = loyaltyAccountNo;
	}

	public String getLoyaltyDateofBirth() {
		return loyaltyDateofBirth;
	}

	public void setLoyaltyDateofBirth(String loyaltyDateofBirth) {
		this.loyaltyDateofBirth = loyaltyDateofBirth;
	}

	public boolean isLoginSuccess() {
		return loginSuccess;
	}

	public boolean isSuccess() {
		return success;
	}

	public boolean isEnrollLms() {
		return enrollLms;
	}

	public void setEnrollLms(boolean enrollLms) {
		this.enrollLms = enrollLms;
	}

	public boolean isEnrollLoyalty() {
		return enrollLoyalty;
	}

	public void setEnrollLoyalty(boolean enrollLoyalty) {
		this.enrollLoyalty = enrollLoyalty;
	}

	public boolean isMashreqSuccess() {
		return mashreqSuccess;
	}

	public void setMashreqSuccess(boolean mashreqSuccess) {
		this.mashreqSuccess = mashreqSuccess;
	}

	public boolean isLmsSuccess() {
		return lmsSuccess;
	}

	public void setLmsSuccess(boolean lmsSuccess) {
		this.lmsSuccess = lmsSuccess;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setLoginSuccess(boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(Map<String, String> errorInfo) {
		this.errorInfo = errorInfo;
	}

	public void setRegistration(boolean registration) {
		this.registration = registration;
	}

	public boolean isIbeAccountExists() {
		return ibeAccountExists;
	}

	public boolean isLmsNameMismatch() {
		return lmsNameMismatch;
	}

	public void setSkipNameMatching(boolean skipNameMatching) {
		this.skipNameMatching = skipNameMatching;
	}

}
