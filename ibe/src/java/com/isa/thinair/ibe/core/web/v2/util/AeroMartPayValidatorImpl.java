package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AeroMartPayPaymentGatewayInfo;
import com.isa.thinair.ibe.api.dto.AeroMartPaySessionInfo;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayResponse;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.CardBehavior;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.MerchantStatus;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;

public class AeroMartPayValidatorImpl implements AeroMartPayValidator {

	private static Log log = LogFactory.getLog(AeroMartPayValidatorImpl.class);

	@Override
	public AeroMartPayResponse validatePaymentRequest(Map<String, String> requestParams) {

		AeroMartPayResponse aeroMartPayError = null;
//		String alphaNumericPattern = AeroMartPayConstants.REGEXF_ALPHA_NUMERIC;

		String ALPHA_NUMERIC_NOT_EMPTY = "^[a-zA-Z0-9]+$";
		String CURRENCY_CODE = "^[a-zA-Z]{3}$";
		String ANY_REGEX = ".+";
		String CURRENCY = "^([0-9])+(.([0-9]){2})?$";

		String merchantID = requestParams.get(AeroMartPayConstants.PARAM_MERCHANT_ID);

		if (!isValidInput(merchantID, ALPHA_NUMERIC_NOT_EMPTY)) {
			aeroMartPayError = AeroMartPayResponse.ERR_01;
		} else {

			String totalAmount = requestParams.get(AeroMartPayConstants.PARAM_TOTAL_AMOUNT);
			String currencyCode = requestParams.get(AeroMartPayConstants.PARAM_CURRENCY_CODE);
			// String languageCode = requestParams.get(AeroMartPayConstants.PARAM_LANGUAGE_CODE);
			String returnURL = requestParams.get(AeroMartPayConstants.PARAM_RETURN_URL);
			// String errorURL = requestParams.get(AeroMartPayConstants.PARAM_ERROR_URL);
			String orderID = requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID);
			String username = requestParams.get(AeroMartPayConstants.PARAM_USERNAME);
			String password = requestParams.get(AeroMartPayConstants.PARAM_PASSWORD);
			String hashString = requestParams.get(AeroMartPayConstants.PARAM_HASH_STRING);

			Merchant merchant = null;
			try {
				merchant = AeroMartPayUtil.getMerchant(merchantID);
				if (merchant == null || StringUtil.isNullOrEmpty(merchant.getMerchantID())) {
					aeroMartPayError = AeroMartPayResponse.ERR_01;
				} else if (!MerchantStatus.ACT.getStatus().contentEquals(merchant.getStatus())) {
					aeroMartPayError = AeroMartPayResponse.ERR_14;
				} else if (!isValidInput(username, ALPHA_NUMERIC_NOT_EMPTY) || !merchant.getUsername().contentEquals(username)) {
					aeroMartPayError = AeroMartPayResponse.ERR_02;
				} else if (!isValidInput(password, ANY_REGEX)
						|| !merchant.getPassword().contentEquals(AeroMartPayUtil.encryptAeroMartPassword(password))) {
					aeroMartPayError = AeroMartPayResponse.ERR_03;

				} else if (!isValidInput(orderID, ALPHA_NUMERIC_NOT_EMPTY)) {
					aeroMartPayError = AeroMartPayResponse.ERR_04;

				} else if (orderID.length() > 20) {
					aeroMartPayError = AeroMartPayResponse.ERR_09;

					// } else if (!orderID.matches(alphaNumericPattern)) {
					// aeroMartPayError = AeroMartPayResponse.ERR_11;

				} else if (!isValidInput(totalAmount, CURRENCY)) {
					aeroMartPayError = AeroMartPayResponse.ERR_05;
					// } else if (new BigDecimal(totalAmount).doubleValue() <= 0) {
					// aeroMartPayError = AeroMartPayResponse.ERR_05;

				} else if (!isValidInput(currencyCode, CURRENCY_CODE)) {
					aeroMartPayError = AeroMartPayResponse.ERR_06;

				} else if (!isValidInput(hashString, ANY_REGEX)) {
					aeroMartPayError = AeroMartPayResponse.ERR_07;

				} else if (isValidInput(hashString, ANY_REGEX)) {
					List<String> hashStringParamsInOrder = new ArrayList<String>();
					hashStringParamsInOrder.add(merchantID);
					hashStringParamsInOrder.add(merchant.getUsername());
					hashStringParamsInOrder.add(password);
					hashStringParamsInOrder.add(totalAmount);
					hashStringParamsInOrder.add(currencyCode);
					hashStringParamsInOrder.add(returnURL);
					hashStringParamsInOrder.add(orderID);
					if (!AeroMartPayUtil.buildHashString(hashStringParamsInOrder, merchant.getSecureHashKey())
							.contentEquals(hashString)) {
						aeroMartPayError = AeroMartPayResponse.ERR_08;
					}
				} else {
					try {
						new BigDecimal(totalAmount);

					} catch (Exception e) {
						aeroMartPayError = AeroMartPayResponse.ERR_05;

					}
				}

			} catch (Exception e) {
				log.error("Request Valiation Failed, orderID: " + orderID, e);
				aeroMartPayError = AeroMartPayResponse.ERR_13;

			}

		}

		return aeroMartPayError;
	}

	private boolean isValidInput(String paramValue, String regex) {
		if (!StringUtil.isNullOrEmpty(paramValue) && paramValue.matches(regex)) {
			return true;
		}
		return false;
	}

	@Override
	public IPGIdentificationParamsDTO validateAndPrepareIPGConfigurationParamsDTO(Integer ipgId, String paymentCurrencyCode)
			throws ModuleException {

		if (ipgId == null && paymentCurrencyCode == null) {
			log.warn("IPG identification params with carrier or pay currency unspecified found " + "[ipgId=" + ipgId
					+ ",payCurCode=" + paymentCurrencyCode + "]");
		}

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, paymentCurrencyCode);
		ipgIdentificationParamsDTO.setFQIPGConfigurationName(ipgId + "_" + paymentCurrencyCode);
		ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
		return ipgIdentificationParamsDTO;
	}

	@Override
	public List<String> validateUserInput(String cardBehavior, String paymentgatewayId, String cardNumber, String cardType,
			String cardHolderName, String cvv, String expiryDate) {

		List<String> errorMessages = null;
		String errorMessage = null;

		String numericRegex = AeroMartPayConstants.REGEX_NUMERIC; // pattern match for numeric
		String cardHolderNameRegex = AeroMartPayConstants.REGEX_CARDHOLDER_NAME;

		if (StringUtil.isNullOrEmpty(cardType) || StringUtil.isNullOrEmpty(paymentgatewayId) || !cardType.matches(numericRegex)
				|| !paymentgatewayId.matches(numericRegex)) {
			errorMessage = "Invalid Payment Option Details";
		}

		if (CardBehavior.INT_EXT.getCardBehavior().equals(cardBehavior)) {

			if (StringUtil.isNullOrEmpty(cardHolderName) || !cardHolderName.matches(cardHolderNameRegex)) {
				errorMessage = "Invalid Cardholder Name";
			}

			if (StringUtil.isNullOrEmpty(expiryDate) || !expiryDate.matches(numericRegex) || expiryDate.length() != 4) {
				errorMessage = "Invalid Expiry Date";
			} else {
				DateFormat df = new SimpleDateFormat(AeroMartPayConstants.DATE_FORMAT);
				Date startDate;

				try {
					startDate = df.parse(expiryDate);
					Date thisDate = new Date();
					if (thisDate.after(startDate)) {
						throw new Exception();
					}

				} catch (Exception e) {
					errorMessage = "Invalid Expiry Date";
				}

			}

			if (StringUtil.isNullOrEmpty(cardNumber) || cardNumber.length() != 16 || !cardNumber.matches(numericRegex)) {
				errorMessage = "Invalid Card Number";
			}

			if (StringUtil.isNullOrEmpty(cvv) || !cvv.matches(numericRegex) || cvv.length() < 3) {
				errorMessage = "Invalid CVV";
			}

		}

		if (errorMessage != null) {
			errorMessages = Arrays.asList(errorMessage);
		}

		return errorMessages;
	}

	@Override
	public boolean validateReferenceID(String referenceID) throws ModuleException {

		boolean result = true;

		if (StringUtil.isNullOrEmpty(referenceID)) {
			result = false;
		}
		return result;
	}

	@Override
	public boolean validateSessionDetails(AeroMartPaySessionInfo sessionInfo) {
		boolean result = true;

		if (sessionInfo != null) {

			if (StringUtil.isNullOrEmpty(sessionInfo.getOrderID()) || StringUtil.isNullOrEmpty(sessionInfo.getReferenceID())
					|| StringUtil.isNullOrEmpty(sessionInfo.getCurrencyCode())
					|| StringUtil.isNullOrEmpty(sessionInfo.getTotalAmount())
					|| StringUtil.isNullOrEmpty(sessionInfo.getReturnURL())
					|| StringUtil.isNullOrEmpty(sessionInfo.getErrorURL())
					|| StringUtil.isNullOrEmpty(sessionInfo.getMerchantID())
					|| StringUtil.isNullOrEmpty(sessionInfo.getMerchantName())) {
				result = false;
			}

			if ((new BigDecimal(sessionInfo.getTotalAmount()).doubleValue() < 0 || sessionInfo.getOrderID().length() < 6
					|| sessionInfo.getCurrencyCode().length() != 3)) {
				result = false;
			}

		} else {
			result = false;
		}

		return result;
	}

	@Override
	public void validatePaymentGatewaysInfo(List<AeroMartPayPaymentGatewayInfo> paymentGatewayInfo) throws ModuleException {
		if (paymentGatewayInfo == null || paymentGatewayInfo.size() == 0) {
			throw new ModuleException("");
		}

	}

	// TODO remove duplication, already in validatePaymentRequest
	@Override
	public AeroMartPayResponse validateQueryRequest(Map<String, String> requestParams) {

		AeroMartPayResponse aeroMartPayError = null;

		String alphaNumericPattern = AeroMartPayConstants.REGEX_ALPHA_NUMERIC;

		String merchantID = requestParams.get(AeroMartPayConstants.PARAM_MERCHANT_ID);

		if (StringUtil.isNullOrEmpty(requestParams.get(AeroMartPayConstants.PARAM_MERCHANT_ID))) {
			aeroMartPayError = AeroMartPayResponse.ERR_01;
		}

		else {

			Merchant merchant = null;

			try {

				merchant = CommonsServices.getGlobalConfig().getMerchantByID(merchantID);

				if (merchant == null) {
					aeroMartPayError = AeroMartPayResponse.ERR_01;
				} else if (!MerchantStatus.ACT.getStatus().contentEquals(merchant.getStatus())) {
					aeroMartPayError = AeroMartPayResponse.ERR_14;
				} else if (StringUtil.isNullOrEmpty(requestParams.get(AeroMartPayConstants.PARAM_USERNAME))
						|| !merchant.getUsername().contentEquals(requestParams.get(AeroMartPayConstants.PARAM_USERNAME))) {
					aeroMartPayError = AeroMartPayResponse.ERR_02;
				} else if (StringUtil.isNullOrEmpty(requestParams.get(AeroMartPayConstants.PARAM_PASSWORD))
						|| !merchant.getPassword().contentEquals(AeroMartPayUtil
								.encryptAeroMartPassword(requestParams.get(AeroMartPayConstants.PARAM_PASSWORD)))) {
					aeroMartPayError = AeroMartPayResponse.ERR_03;

				} else if (StringUtil.isNullOrEmpty(requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID))) {
					aeroMartPayError = AeroMartPayResponse.ERR_04;

				} else if (((String) requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID)).length() > 20) {
					aeroMartPayError = AeroMartPayResponse.ERR_09;

				} else if (!requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID).matches(alphaNumericPattern)) {
					aeroMartPayError = AeroMartPayResponse.ERR_11;

				}

				else if (!StringUtil.isNullOrEmpty(requestParams.get(AeroMartPayConstants.PARAM_HASH_STRING))) {
					List<String> hashStringParamsInOrder = new ArrayList<String>();
					hashStringParamsInOrder.add(merchantID);
					hashStringParamsInOrder.add(merchant.getUsername());
					hashStringParamsInOrder.add(requestParams.get(AeroMartPayConstants.PARAM_PASSWORD));
					hashStringParamsInOrder.add(requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID));
					if (!AeroMartPayUtil.buildHashString(hashStringParamsInOrder, merchant.getSecureHashKey())
							.contentEquals(requestParams.get(AeroMartPayConstants.PARAM_HASH_STRING))) {
						aeroMartPayError = AeroMartPayResponse.ERR_08;

					}

				}
			} catch (Exception e) {
				log.error("Request Valiation Failed, orderID: " + requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID), e);
				aeroMartPayError = AeroMartPayResponse.ERR_13;

			}
		}

		return aeroMartPayError;
	}
}
