package com.isa.thinair.ibe.core.web.v2.action.kiosk;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;

;

/**
 * Kiosk Login
 * 
 * @author Pradeep Karunanayake
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class KioskLoginAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(KioskLoginAction.class);

	private Map<String, String> errorInfo;

	private boolean success = true;

	private String messageTxt;

	private String ibeSecureURL;

	private String ibeNonSecureURL;

	private String carrierName;

	private String mode;

	private String username;

	private String password;

	@SuppressWarnings("unchecked")
	public String execute() {

		String result = StrutsConstants.Result.SUCCESS;
		try {
			// Set Page Build Details
			if (mode == null || mode.isEmpty()) {
				errorInfo = ErrorMessageUtil.getKioskLoginErrors(request);
				ibeSecureURL = AppParamUtil.getSecureIBEUrl();
				ibeNonSecureURL = AppParamUtil.getNonsecureIBEUrl();
				carrierName = SystemUtil.getCarrierName(request);
			} else if (mode.equalsIgnoreCase("login")) {
				// Set Login Details
				HttpSession reqsession = request.getSession();
				boolean hasKioskPre = false;
				if (username != null && password != null) {
					username = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + username;
					User user = ModuleServiceLocator.getSecurityBD().authenticate(username, password);

					if (user != null) {
						String kioskAuthCode = "ibe.kiosk.access";
						String strprivIds = null;
						reqsession.setAttribute(CustomerWebConstants.REQ_KSK_USER, user);
						Set set = user.getPrivitedgeIDs();
						Iterator iter = set.iterator();

						while (iter.hasNext()) {
							strprivIds = (String) iter.next();
							if (strprivIds.equals(kioskAuthCode)) {
								hasKioskPre = true;
								reqsession.setAttribute(CustomerWebConstants.SYS_ACCESS_POINT,
										CustomerWebConstants.SYS_ACCESS_POINT_KSK);
								break;
							}
						}

					}

					if (user == null || hasKioskPre == false) {
						messageTxt = "LOGINPRIV";
					}

				} else {
					messageTxt = "Error";
				}
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("KioskLoginAction==>", ex);
		}

		return result;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getIbeSecureURL() {
		return ibeSecureURL;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getIbeNonSecureURL() {
		return ibeNonSecureURL;
	}

}
