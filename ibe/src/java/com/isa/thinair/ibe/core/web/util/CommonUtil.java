package com.isa.thinair.ibe.core.web.util;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

public class CommonUtil {

	public static Customer loginCustomer(String userID, String password, HttpServletRequest request) throws ModuleException {
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		Customer cust = customerDelegate.authenticate(userID, password);

		if (cust != null) {
			SessionUtil.setCustomerId(request, cust.getCustomerId());
			SessionUtil.setCustomerEMail(request, cust.getEmailId());
			SessionUtil.setCustomerPassword(request, cust.getPassword());

			if (cust.getLMSMemberDetails() != null) {
				SessionUtil.setLoyaltyFFID(request, cust.getLMSMemberDetails().getFfid());
			}

			return cust;
		} else {
			return null;
		}
	}

}
