package com.isa.thinair.ibe.core.web.v2.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.CustomerCreditDTO;

/**
 * Sort Utility
 * 
 * @author Zaki
 * @since Nov 03, 2009
 */
class SortUtil {

	/**
	 * Method to sort AvailableFlightInfo by date
	 * 
	 * @param AvailableFlightInfo
	 *            A set of AvailableFlightInfo
	 * @return An List of AvailableFlightInfo
	 */
	protected static Collection<AvailableFlightInfo> sort(List<AvailableFlightInfo> availableFlightInfo) { // NO_UCD
		Collections.sort(availableFlightInfo, new Comparator<AvailableFlightInfo>() {
			public int compare(AvailableFlightInfo obj1, AvailableFlightInfo obj2) {
				return (obj1.getDepartureDate()).compareTo(obj2.getDepartureDate());
			}
		});
		return availableFlightInfo;
	}

	public static List<LCCClientAlertTO> sortAlertInfo(List<LCCClientAlertTO> lCCClientAlertTO) {
		Collections.sort(lCCClientAlertTO, new Comparator<LCCClientAlertTO>() {
			@Override
			public int compare(LCCClientAlertTO at1, LCCClientAlertTO at2) {
				return (new Integer(at2.getAlertId())).compareTo(new Integer(at1.getAlertId()));
			}
		});
		return lCCClientAlertTO;
	}

	protected static Collection<FlightSegmentTO> sortFlightSegmentTO(List<FlightSegmentTO> availableFlightInfo) {
		Collections.sort(availableFlightInfo, new Comparator<FlightSegmentTO>() {
			public int compare(FlightSegmentTO obj1, FlightSegmentTO obj2) {
				return (obj1.getDepartureDateTime()).compareTo(obj2.getDepartureDateTime());
			}
		});
		return availableFlightInfo;
	}

	protected static Collection<FlightInfoTO> sortByReservationStatus(List<FlightInfoTO> availableFlightInfo) {
		Collections.sort(availableFlightInfo, new Comparator<FlightInfoTO>() {
			public int compare(FlightInfoTO obj1, FlightInfoTO obj2) {
				return (obj1.getStatus()).compareTo(obj2.getStatus());
			}
		});
		return availableFlightInfo;
	}
	
	protected static Collection<CustomerCreditDTO> sortCustomerCreditByExpiryDate(Collection<CustomerCreditDTO> colCustDto) {

		List<CustomerCreditDTO> custDtoList = new ArrayList<CustomerCreditDTO>(colCustDto);

		Collections.sort(custDtoList, new Comparator<CustomerCreditDTO>() {

			public int compare(CustomerCreditDTO obj1, CustomerCreditDTO obj2) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
				Date d1 = null;
				Date d2 = null;
				try {
					d1 = (Date) formatter.parse(obj1.getDateOfExpiry());
					d2 = (Date) formatter.parse(obj2.getDateOfExpiry());
				} catch (ParseException e) {
					/*
					 * this should be avoid
					 */
				}

				if (d1 != null & d2 != null) {
					return d1.compareTo(d2);
				} else {
					return 0;
				}

			}

		});

		return custDtoList;
	}
}