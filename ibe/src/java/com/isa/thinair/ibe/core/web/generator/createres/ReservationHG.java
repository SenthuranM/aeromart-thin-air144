package com.isa.thinair.ibe.core.web.generator.createres;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.generator.customer.ReservationSearchDecorator;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.webplatform.api.dto.CarLinkParamsTO;
import com.isa.thinair.webplatform.api.dto.HolidaysLinkParamsTO;
import com.isa.thinair.webplatform.api.util.ExternalLinkCreatorUtil;
import com.isa.thinair.webplatform.core.commons.MapGenerator;

public class ReservationHG {

	private static Log log = LogFactory.getLog(ReservationHG.class);

	public static String createHolidaysLink(HolidaysLinkParamsTO holidaysLinkParamsTO) throws ModuleException {
		String holidaysURL = BeanUtils.nullHandler(AppSysParamsUtil.getHolidaysURL());
		if (holidaysURL.length() > 0 && AppSysParamsUtil.isShowHolidaysLink()) {
			if (holidaysURL.charAt(holidaysURL.length() - 1) != '&') {
				holidaysURL += "&";
			}
			holidaysLinkParamsTO.setStaticURL(holidaysURL);
			return ExternalLinkCreatorUtil.createLink(holidaysLinkParamsTO);
		} else {
			return ""; // should be empty for UI validation
		}
	}

	public static String createRentACarLink(String language, Collection<SegInfoDTO> flightsDep,
			Collection<SegInfoDTO> flightsRet, String currencyCode, String pnr) throws ModuleException {
		String carURL = BeanUtils.nullHandler(AppSysParamsUtil.getRentACarURL());
		if (carURL.length() > 0 && AppSysParamsUtil.isShowRentACarLink()) {
			if (AppSysParamsUtil.isRentACarLinkDynamic()) {
				CarLinkParamsTO carLinkParams = new CarLinkParamsTO();
				ArrayList<SegInfoDTO> flightDepDetails = (ArrayList) flightsDep;
				ArrayList<SegInfoDTO> flightRetDetails = (ArrayList) flightsRet;
				String locCode = "";
				String rtDateTime = "";
				String pkDateTime = "";
				if (flightDepDetails != null && flightDepDetails.size() > 0) {
					SegInfoDTO lastOutboundSeg = flightDepDetails.get(flightDepDetails.size() - 1);
					if (lastOutboundSeg != null) {
						long pkDateTimeLong = lastOutboundSeg.getArrivalTimeLong() + 7200000; // 2 hours after arrival
																								// time
						pkDateTime = CommonUtil.formatDateTimeYYYYMMDDHHMM(pkDateTimeLong);
						carLinkParams.setPkDateTime(pkDateTime);
						String[] locCodes = lastOutboundSeg.getSegmentShortCode().split("/");
						locCode = locCodes[locCodes.length - 1];
						carLinkParams.setLocCode(locCode);
						Collection<String> colAirPortCodes = new ArrayList<String>();
						colAirPortCodes.add(locCode);
						Map<String, CachedAirportDTO> mapAirports = new HashMap<String, CachedAirportDTO>();
						if (colAirPortCodes.size() != mapAirports.size()) {
							mapAirports = ModuleServiceLocator.getAirportBD().getCachedAllAirportMap(colAirPortCodes);
							carLinkParams.setResidencyId(mapAirports.get(locCode) != null ? mapAirports.get(locCode)
									.getCountryCode() : "");
						}
					}
				}
				if (flightRetDetails != null && flightRetDetails.size() > 0) {
					SegInfoDTO firstInboundSeg = flightRetDetails.get(0);
					if (firstInboundSeg != null) {
						rtDateTime = CommonUtil.formatDateTimeYYYYMMDDHHMM(firstInboundSeg.getDepartureTimeLong());
					}
				}
				if (rtDateTime.equals("")) {
					rtDateTime = pkDateTime;
				}
				carLinkParams.setRtDateTime(rtDateTime);
				carLinkParams.setLang(language.toUpperCase());
				if (currencyCode != null && currencyCode.length() > 0) {
					carLinkParams.setCurrency(currencyCode);
				}
				carLinkParams.setOrderId(pnr);
				if (carURL.charAt(carURL.length() - 1) != '?') {
					carURL += "?";
				}
				carLinkParams.setStaticURL(carURL);
				carURL = ExternalLinkCreatorUtil.createCarLink(carLinkParams);
			}
			return carURL;
		} else {
			return ""; // should be empty for UI validation
		}
	}

	public static String createRentACarLinkForManageBooking(String language, Collection<FlightInfoTO> flights, String pnr)
			throws ModuleException {
		String carURL = BeanUtils.nullHandler(AppSysParamsUtil.getRentACarURL());
		if (carURL.length() > 0 && AppSysParamsUtil.isShowRentACarLink()) {
			if (AppSysParamsUtil.isRentACarLinkDynamic()) {
				CarLinkParamsTO carLinkParams = new CarLinkParamsTO();
				carLinkParams.setLang(language.toUpperCase());
				// TODO set booking currency
				carLinkParams.setCurrency(AppSysParamsUtil.getBaseCurrency());
				String clientId = AppSysParamsUtil.getRentACarManageBookingClientId();
				if (clientId != null) {
					carLinkParams.setClientId(clientId);
				}
				carLinkParams.setOrderId(pnr);
				if (carURL.charAt(carURL.length() - 1) != '?') {
					carURL += "?";
				}
				carLinkParams.setStaticURL(carURL);
				carURL = ExternalLinkCreatorUtil.createCarLinkForManageBooking(flights, carLinkParams);
			}
			return carURL;
		} else {
			return ""; // should be empty for UI validation
		}
	}

	private static String createReservationsHtml(Collection<ReservationDTO> coll) throws ModuleException {
		Collection<ReservationListTO> colReservationListTO = composeReservationsList(coll);
		return new ReservationSearchDecorator(colReservationListTO).decorateForV1();
	}

	private static Collection<ReservationListTO> composeReservationsList(Collection<ReservationDTO> colReservationDTO)
			throws ModuleException {
		Collection<ReservationListTO> colReservationListTO = new ArrayList<ReservationListTO>();
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd MMM yyyy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");

		if (colReservationDTO != null && colReservationDTO.size() > 0) {
			Iterator<ReservationDTO> iterReservation = colReservationDTO.iterator();

			// Reservation Information
			while (iterReservation.hasNext()) {
				ReservationDTO reservationDTO = iterReservation.next();
				ReservationContactInfo reservationContactInfo = reservationDTO.getReservationContactInfo();

				ReservationListTO reservationListTO = new ReservationListTO();
				reservationListTO.setPnrNo(BeanUtils.nullHandler(reservationDTO.getPnr()));
				reservationListTO.setPaxName(BeanUtils.nullHandler(reservationContactInfo.getFirstName()) + ", "
						+ BeanUtils.nullHandler(reservationContactInfo.getLastName()));
				reservationListTO.setPnrStatus(reservationDTO.getStatus());
				reservationListTO.setOriginatorPnr(BeanUtils.nullHandler(reservationDTO.getOriginatorPnr()));

				List<FlightInfoTO> lstFlightInfo = new ArrayList<FlightInfoTO>();

				String strPaxCount = reservationDTO.getAdultCount() + "/" + reservationDTO.getChildCount() + "/"
						+ reservationDTO.getInfantCount();

				for (Iterator iterator = reservationDTO.getSegments().iterator(); iterator.hasNext();) {
					ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) iterator.next();
					FlightInfoTO flightInfoTO = new FlightInfoTO();

					flightInfoTO.setDepartureDate(smpdtDate.format(reservationSegmentDTO.getDepartureDate()));
					flightInfoTO.setDepartureTime(smpdtTime.format(reservationSegmentDTO.getDepartureDate()));
					flightInfoTO.setDepartureDateLong(reservationSegmentDTO.getZuluDepartureDate().getTime());
					flightInfoTO.setArrivalDate(smpdtDate.format(reservationSegmentDTO.getArrivalDate()));
					flightInfoTO.setArrivalTime(smpdtTime.format(reservationSegmentDTO.getArrivalDate()));
					flightInfoTO.setFlightNo(reservationSegmentDTO.getFlightNo());
					flightInfoTO.setPaxCount(strPaxCount);

					boolean isOpenReturn = reservationSegmentDTO.isOpenReturnSegment();
					flightInfoTO.setOpenReturnSegment(isOpenReturn);

					String segmentStatus = reservationSegmentDTO.getStatus();
					if (isOpenReturn && !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segmentStatus)) {
						segmentStatus = "";
					}

					flightInfoTO.setStatus(segmentStatus);
					flightInfoTO.setSubStatus(reservationSegmentDTO.getSubStatus());

					String segmentCode = BeanUtils.nullHandler(reservationSegmentDTO.getSegmentCode());

					// Haider 4Feb09
					if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
						segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
					}

					flightInfoTO.setOrignNDest(getONDDetails(segmentCode));

					lstFlightInfo.add(flightInfoTO);
				}

				if (reservationDTO.getColExternalSegments() != null && reservationDTO.getColExternalSegments().size() > 0) {
					for (ReservationExternalSegmentTO reservationExternalSegmentTO : reservationDTO.getColExternalSegments()) {
						String segmentCode = reservationExternalSegmentTO.getSegmentCode();

						if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
							segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
						}

						FlightInfoTO flightInfoTO = new FlightInfoTO();
						flightInfoTO.setFlightNo(reservationExternalSegmentTO.getFlightNo());
						flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(reservationExternalSegmentTO.getFlightNo()));
						flightInfoTO.setDepartureDate(smpdtDate.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureTime(smpdtTime.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureDateLong(reservationExternalSegmentTO.getDepartureDate().getTime());
						flightInfoTO.setArrivalDate(smpdtDate.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setArrivalTime(smpdtTime.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setOrignNDest(getONDDetails(segmentCode));
						flightInfoTO.setPaxCount(strPaxCount);
						flightInfoTO.setStatus(reservationExternalSegmentTO.getStatus());
						lstFlightInfo.add(flightInfoTO);
					}
				}

				Collections.sort(lstFlightInfo);

				reservationListTO.setFlightInfo(lstFlightInfo);
				colReservationListTO.add(reservationListTO);
			}
		}

		return colReservationListTO;
	}

	public static String getSegementDetails(String strSegement) {
		return getSegementDetails(strSegement, null);
	}

	private static String getSegementDetails(String strSegement, String subStationShortCode) {
		Map mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strSegement.split("/");
		strSegements = "";
		for (int i = 0; i < strArrSegment.length; i++) {
			// Haider 4Feb09
			if (AppSysParamsUtil.isHideStopOverEnabled() && strArrSegment.length > 2 && i > 0 && i < (strArrSegment.length - 1))
				continue;
			if (!strSegements.equals("")) {
				strSegements += " / ";
			}
			if (mapAirports.get(strArrSegment[i]) != null) {
				if (AppSysParamsUtil.isGroundServiceEnabled() && subStationShortCode != null) {
					strSegements += SubStationUtil.getSubStationName(strArrSegment[i], subStationShortCode,
							mapAirports.get(strArrSegment[i]).toString());
				} else {
					strSegements += mapAirports.get(strArrSegment[i]);
				}
			} else {
				strSegements += strArrSegment[i];
			}
		}
		return strSegements;
	}

	/**
	 * Generate segment code with terminal information.
	 * 
	 * @param strSegement
	 * @param subStationShortCode
	 * @param departureTerminal
	 * @param arrivalTerminal
	 * 
	 * @return
	 */
	public static String getSegementDetailsWithTermainals(String strSegement, String subStationShortCode,
			String departureTerminal, String arrivalTerminal) {
		Map mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strSegement.split("/");
		strSegements = "";
		for (int i = 0; i < strArrSegment.length; i++) {
			// Haider 4Feb09
			if (AppSysParamsUtil.isHideStopOverEnabled() && strArrSegment.length > 2 && i > 0 && i < (strArrSegment.length - 1))
				continue;

			if (!strSegements.equals("")) {
				strSegements += " / ";
			}
			if (mapAirports.get(strArrSegment[i]) != null) {
				if (AppSysParamsUtil.isGroundServiceEnabled() && subStationShortCode != null) {
					strSegements += SubStationUtil.getSubStationName(strArrSegment[i], subStationShortCode,
							mapAirports.get(strArrSegment[i]).toString());
				} else {
					strSegements += mapAirports.get(strArrSegment[i]);
				}
			} else {
				strSegements += strArrSegment[i];
			}

			// add the departure terminal for the first segment.
			if (i == 0) {
				if (departureTerminal != null) {
					strSegements += " - " + departureTerminal;
				}
			}

			// add the arrival terminal for the last segment.
			if (i == (strArrSegment.length - 1)) {
				if (arrivalTerminal != null) {
					strSegements += " - " + arrivalTerminal;
				}
			}
		}
		return strSegements;
	}

	private static String getONDDetails(String strOND) {
		Map mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strOND.split("/");
		strSegements = "";
		for (int i = 0; i < strArrSegment.length; i++) {
			if (AppSysParamsUtil.isHideStopOverEnabled() && strArrSegment.length > 2 && i > 0 && i < (strArrSegment.length - 1))
				continue;
			if (strSegements != "") {
				strSegements += " / ";
			}
			if (mapAirports.get(strArrSegment[i]) != null) {
				strSegements += mapAirports.get(strArrSegment[i]);
			} else {
				strSegements += strArrSegment[i];
			}
		}
		return strSegements;
	}

	public static String createTermsNCond(String local) throws ModuleException {
		Locale locale = new Locale(local == null ? "en" : local);
		return ReservationTemplateUtils.getTermsNConditions("TermsSummary_IBE", locale);
	}

	/*
	 * Terms & Cond
	 */
	public static String createTermsNCondFromXML(String strXMLFilePath) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilderFactory.setCoalescing(true);
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(strXMLFilePath);

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("terms");
			int intRecords = listOfRecords.getLength();
			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				strbData.append(menuNode.getFirstChild().getNodeValue());
			}
		} catch (SAXParseException err) {
			log.error("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId(), err);
		} catch (Throwable t) {
			log.error(t);
		}
		return strbData.toString();
	}
}
