package com.isa.thinair.ibe.core.web.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.ibe.core.service.IBEConfig;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;

public class AppParamUtil {
	private static IBEConfig ibeConfig = IBEModuleUtils.getConfig();

	public static String getSecureIBEUrl() {
		String secureUrl = null;

		if (SystemPropertyUtil.isXMLConfigsEnabled()) {
			secureUrl = ibeConfig.getSecureIBEUrl();
		} else {
			secureUrl = AppSysParamsUtil.getSecureIBEUrl();
		}

		return secureUrl;
	}

	public static String getNonsecureIBEUrl() {
		String nonsecureUrl = null;

		if (SystemPropertyUtil.isXMLConfigsEnabled()) {
			nonsecureUrl = ibeConfig.getNonsecureIBEUrl();
		} else {
			nonsecureUrl = AppSysParamsUtil.getNonsecureIBEUrl();
		}

		return nonsecureUrl;
	}

	public static String getDefaultPGW() {
		return AppSysParamsUtil.getDefaultPGCurrency();
	}

	public static boolean isUsingDBConfigs() {
		if (SystemPropertyUtil.isXMLConfigsEnabled()) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean isApplyCreditCardCharge() {
		return AppSysParamsUtil.applyCreditCardCharge();
	}
	
	public static boolean isAeromartPayEnabled() {
		return AppSysParamsUtil.isAeroMartPayEnabled();
	}


	public static boolean isTravelGuardEnabled() {
		return AppSysParamsUtil.isShowTravelInsurance();
	}

	public static String getSysDate() {
		Date date = CalendarUtil.getCurrentSystemTimeInZulu();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			return sdf.format(date);
		} catch (Exception e) {
			return "";
		}
	}

	public static boolean isInDevMode() {
		return SystemPropertyUtil.isDevMode();
	}

	public static String getDefaultCarrier() {
		return AppSysParamsUtil.getDefaultCarrierCode();
	}

	public static boolean isDynamicOndListPopulationEnabled() {
		return AppSysParamsUtil.isDynamicOndListPopulationEnabled();
	}

	public static String getDynamicOndListUrl() {
		return AppSysParamsUtil.getDynamicOndListUrl();
	}

	public static boolean isUserDefineTransitTimeEnabled() {
		return AppSysParamsUtil.isEnableUserDefineTransitTime();
	}
	
	public static boolean isSSREnabledForIBE(){
		return AppSysParamsUtil.isSSREnabled();
	}

	public static boolean isVoucherEnabled() {
		return AppSysParamsUtil.isVoucherEnabled();
	}

}
