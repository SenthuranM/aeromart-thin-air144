package com.isa.thinair.ibe.core.web.v2.action.voucher;

import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.api.dto.VoucherCCPaymentDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.listener.AAHttpSessionListener;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.opensymphony.xwork2.ActionChainResult;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, type = ActionChainResult.class, value = StrutsConstants.Action.SAVE_GIFT_VOUCHER),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE, value = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class HandleVoucherIPGResponseAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(HandleVoucherIPGResponseAction.class);
	private IBECommonDTO commonParams = new IBECommonDTO();
	private String sessionId;

	private String handleMerchantPost() {
		if (BeanUtils.nullHandler(this.sessionId).length() > 0) {

			HttpSession oldSession = AAHttpSessionListener.getSession(this.sessionId);
			synchronized (oldSession) {

				if (oldSession.getAttribute(WebConstants.IBE_NEW_SESSION_ID) == null) {
					if (log.isDebugEnabled()) {
						log.debug("####### Session ID as recevied from the PG : " + request.getSession().getId());
					}

					AAHttpSessionListener.handleIPGPost(this.sessionId, request.getSession().getId());

					if (log.isDebugEnabled()) {
						log.debug("####### Previous Session Attributess were copied to current Session");
					}
				} else {
					composeCommonError();
					return StrutsConstants.Result.ERROR;
				}
			}
		}

		return null;
	}

	private void composeCommonError() {
		StringBuffer messageBuilder = new StringBuffer();
		messageBuilder.append(I18NUtil.getMessage("msg.booking.session.timeout"));
		messageBuilder.append("<br/>");

		request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, messageBuilder.toString());
	}

	public String execute() {

		String forward = StrutsConstants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			VoucherCCPaymentDTO sessVoucherPaymentinfo = null;
			boolean isPaymentSuccess = false;

			try {
				String returnStatus = BeanUtils.nullHandler(this.handleMerchantPost());
				if (returnStatus.length() > 0) {
					return returnStatus;
				}
				Map<String, String> receiptyMap = getReceiptMap(request);
				if (log.isDebugEnabled())
					log.debug("Payment response details : " + receiptyMap + "Session ID: " + request.getRequestedSessionId());
				if (isSessionExpiry(receiptyMap)) {
					return StrutsConstants.Result.ERROR;
				}
				/* Set navigation parameter */
				SystemUtil.setCommonParameters(request, commonParams);
				sessVoucherPaymentinfo = SessionUtil.getVoucherPaymentDTO(request);
				// TODO handle proper way
				// FIXME

				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();

				if (SystemPropertyUtil.isPaymentBokerDisabled() && AppParamUtil.isInDevMode()) { // only in dev mode
					ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
					ipgResponseDTO.setCardType(2);
				} else {
					int paymentBrokerRefNo = sessVoucherPaymentinfo.getPaymentBrokerRef();
					int tempPayId = sessVoucherPaymentinfo.getTempTxId();
					String voucherGroupId = sessVoucherPaymentinfo.getVoucherGroupId();
					String merchantTxnRef = sessVoucherPaymentinfo.getIpgRefenceNo();

					String strPayCurCode = sessVoucherPaymentinfo.getPaymentGateWay().getPayCurrency();
					int ipgId = sessVoucherPaymentinfo.getPaymentGateWay().getPaymentGateway();
					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil
							.validateAndPrepareIPGConfigurationParamsDTO(new Integer(ipgId), strPayCurCode);
					Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
					String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);

					if (responseTypeXML != null && !responseTypeXML.equals("") && responseTypeXML.equals("true")) {
						log.debug("### Respons is of type XML... ");
						XMLResponseDTO xmlResponse = (XMLResponseDTO) request.getAttribute(WebConstants.XML_RESPONSE_PARAMETERS);
						if (xmlResponse != null) {
							receiptyMap = getReceiptMap(
									(XMLResponseDTO) request.getAttribute(WebConstants.XML_RESPONSE_PARAMETERS), request
											.getSession().getId());
						}
					}

					Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
					ipgResponseDTO.setRequestTimsStamp(requestTime);
					ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
					ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
					ipgResponseDTO.setTemporyPaymentId(tempPayId);
					CommonCreditCardPaymentInfo ccPayInfo = (CommonCreditCardPaymentInfo) sessVoucherPaymentinfo.getTemporyPaymentMap().get(
							tempPayId);
					ipgResponseDTO.setCcLast4Digits(ccPayInfo.getNoLastDigits());
					ipgResponseDTO.setBrokerType(ipgProps.getProperty("brokerType"));

					log.debug("### Getting response data... ###");
					ipgResponseDTO = ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap, ipgResponseDTO,
							ipgIdentificationParamsDTO);
					addPaymentDetail(sessVoucherPaymentinfo.getPaymentGateWay(), ipgResponseDTO);
					if (log.isDebugEnabled()) {
						log.debug("IPG Response Status:" + "|voucherGroupId=" + voucherGroupId + "|merchantTxnRef=" + merchantTxnRef
								+ "|sessId=" + request.getRequestedSessionId() + "|isSuccess=" + ipgResponseDTO.isSuccess()
								+ "|receivedMerchantTxnRef=" + ipgResponseDTO.getApplicationTransactionId());
					}
					isPaymentSuccess = ipgResponseDTO.isSuccess();
					// Tempory fix for cybersource.
					// TODO Please find ways to make it configurable
					if (BeanUtils.nullHandler(this.sessionId).length() > 0) {
						if (!ipgResponseDTO.isSuccess()) {
							HttpSession oldSession = AAHttpSessionListener.getSession(this.sessionId);
							oldSession.setAttribute(WebConstants.IBE_NEW_SESSION_ID, null);
							composeCommonError();
							return StrutsConstants.Result.ERROR;
						}
					}

					synchronized (request.getSession()) {
						if (!sessVoucherPaymentinfo.isResponceReceived()) {
							sessVoucherPaymentinfo.setResponceReceived(true);
							ModuleServiceLocator.getReservationBD().updateTempPaymentEntryPaymentStatus(
									sessVoucherPaymentinfo.getTemporyPaymentMap(), ipgResponseDTO);
						} else {
							if (StringUtil.isNullOrEmpty(voucherGroupId)) {
								voucherGroupId = (String) request.getSession().getAttribute(ReservationWebConstnts.REQ_SESSION_PNR_NO);
							}
							return processMultiplePaymentResponses(ipgResponseDTO, merchantTxnRef, voucherGroupId);
						}
					}
				}

				sessVoucherPaymentinfo.setIpgResponseDTO(ipgResponseDTO);
				if (IPGResponseDTO.STATUS_ACCEPTED.equals(ipgResponseDTO.getStatus())) {
					sessVoucherPaymentinfo.setInvoiceStatus(QiwiPRequest.PAID);
				}

				// redirecting to re-enter qiwi mobile number
				if (!isPaymentSuccess && sessVoucherPaymentinfo.getPaymentGateWay().getProviderCode().equalsIgnoreCase("QIWI")) {
					log.debug("[HandleVoucherIPGResponseAction ] Qiwi Payment Is Not Success : Redirecting ");
					return StrutsConstants.Result.SUCCESS;
				}
				validateTransaction(sessVoucherPaymentinfo.getIpgRefenceNo(), ipgResponseDTO, isPaymentSuccess,
						SessionUtil.getLanguage(request));
				SessionUtil.setTempPaymentPNR(request, sessVoucherPaymentinfo.getVoucherGroupId());
				SessionUtil.setResponseReceivedFromPaymentGateway(request, isPaymentSuccess);

				if (log.isDebugEnabled())
					log.debug("### HandleVoucherIPGResponseAction end... ###");
			} catch (Exception ex) {
				log.error("HandleVoucherIPGResponseAction.SessionID:" + request.getRequestedSessionId(), ex);
				if (SessionUtil.isSuccessfulReservationExit(request)) {
					forward = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE;
				} else {
					String message = null;
					forward = StrutsConstants.Result.ERROR;
					if (isPaymentSuccess) {
						try {
							String pnr = sessVoucherPaymentinfo.getVoucherGroupId();
							ReservationDTO reservationDTO = ReservationUtil.getReservationDTO(pnr);
							if (reservationDTO != null) {
								message = CommonUtil.getExistingReservationMsg(request, pnr);
							} else {
								message = CommonUtil.getRefundMessage(SessionUtil.getLanguage(request));
							}
						} catch (Exception e) {
							log.error(e);
						}
					} else {
						message = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
					}
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, message);
					SessionUtil.resetSesionDataInError(request);
				}
			}
		}

		return forward;
	}

	/**
	 * Handle multiple redirects for same payment request on given session Handle back buttons payments also
	 */
	private String processMultiplePaymentResponses(IPGResponseDTO ipgResponseDTO, String merchantTxnRef, String pnr) {
		String forward = StrutsConstants.Result.ERROR;
		/* Set page navigation parameters */
		SystemUtil.setCommonParameters(request, commonParams);
		String language = SessionUtil.getLanguage(request);
		StringBuilder logInfo = new StringBuilder();
		logInfo.append("### Multiple Response process start.. ###").append("\n");
		logInfo.append("& PNR :").append(pnr);
		logInfo.append("& merchantTxnRef :").append(merchantTxnRef);
		logInfo.append("& Payment Status :").append(ipgResponseDTO.isSuccess());
		logInfo.append("& SessionID : ").append(request.getRequestedSessionId());

		log.info(logInfo.toString());

		StringBuffer messageBuilder = new StringBuffer();
		messageBuilder.append(I18NUtil.getMessage("msg.booking.browser.backbutton", language));
		messageBuilder.append("<br/>");

		if (ipgResponseDTO.isSuccess()) {
			boolean isUpdate = false;
			try {
				isUpdate = ModuleServiceLocator.getReservationBD().processMultiplePaymentResponses(
						ipgResponseDTO.getTemporyPaymentId());
			} catch (Exception ex) {
				isUpdate = false;
				log.error("Couldn't update payment status:", ex);
			}
			logInfo = new StringBuilder();
			logInfo.append("Multiple response process status : ").append(isUpdate);
			logInfo.append("& merchantTxnRef : ").append(merchantTxnRef);
			logInfo.append("& SessionID : ").append(request.getRequestedSessionId());
			log.info(logInfo.toString());

			if (isUpdate) {
				messageBuilder.append(I18NUtil.getMessage("msg.booking.fail.refund", language));
			}

		}
		request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, messageBuilder.toString());
		SessionUtil.resetSesionDataInError(request);

		return forward;
	}

	private boolean isSessionExpiry(Map<String, String> receiptyMap) {
		boolean isError = false;
		if (SessionUtil.isSessionExpired(request) || SessionUtil.getVoucherPaymentDTO(request) == null) {
			// Present error page with appropriate message
			isError = true;
		}
		return isError;
	}

	private void addPaymentDetail(PaymentGateWayInfoDTO gateWayInfoDTO, IPGResponseDTO ipgResponseDTO) {
		if (ipgResponseDTO != null && ipgResponseDTO.getIpgTransactionResultDTO() != null && gateWayInfoDTO != null) {
			IPGTransactionResultDTO ipgTransactionResultDTO = ipgResponseDTO.getIpgTransactionResultDTO();
			ipgTransactionResultDTO.setAmount(gateWayInfoDTO.getPayCurrencyAmount());
			ipgTransactionResultDTO.setCurrency(gateWayInfoDTO.getPayCurrency());
		}
	}

	/**
	 * 
	 * @param ipgTransactionId
	 * @param ipgResponseDTO
	 */
	private static void validateTransaction(String ipgTransactionId, IPGResponseDTO ipgResponseDTO, boolean isPaymentSuccess,
			String language) {

		if (!ipgTransactionId.equals(ipgResponseDTO.getApplicationTransactionId())) {
			log.error("Application Transaction Ids not match[AccelAero TransactionId Sent=" + ipgTransactionId
					+ ",AccelAero TransactionId Received" + ipgResponseDTO.getApplicationTransactionId() + "]");
			String msg = "";
			if (isPaymentSuccess) {
				msg = CommonUtil.getRefundMessage(language);
			} else {
				msg = I18NUtil.getMessage("msg.invalid.booking", language);
			}
			throw new RuntimeException(msg);
		}
	}

	/**
	 * Returns ReceiptMap. Map will holds all parameters send by the payment gateway. It is up the payment broker
	 * implementation extract the required parameter and values
	 * 
	 * @param request
	 * @return
	 */
	private static Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	/**
	 * Returns ReceiptMap. Map will holds all parameters send by the payment gateway.
	 * 
	 * @param xmlResponse
	 * @return
	 */
	private static Map<String, String> getReceiptMap(XMLResponseDTO xmlResponse, String sessionId) {
		Map<String, String> fields = new LinkedHashMap<String, String>();

		if (log.isDebugEnabled()) {
			log.debug("### Started generating receipt map...");
			log.debug("### Order ID : " + StringUtil.getNotNullString(xmlResponse.getOrderId()));
		}

		fields.put(PaymentConstants.XML_RESPONSE.ORDERID.getValue(), StringUtil.getNotNullString(xmlResponse.getOrderId()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYID.getValue(), StringUtil.getNotNullString(xmlResponse.getPayId()));
		fields.put(PaymentConstants.XML_RESPONSE.NCSTATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getNcStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERROR.getValue(), StringUtil.getNotNullString(xmlResponse.getNcError()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERRORPLUS.getValue(),
				StringUtil.getNotNullString(xmlResponse.getNcErrorPlus()));
		fields.put(PaymentConstants.XML_RESPONSE.ACCEPTANCE.getValue(), StringUtil.getNotNullString(xmlResponse.getAcceptance()));
		fields.put(PaymentConstants.XML_RESPONSE.STATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.ECI.getValue(), StringUtil.getNotNullString(xmlResponse.getEci()));
		fields.put(PaymentConstants.XML_RESPONSE.AMOUNT.getValue(), StringUtil.getNotNullString(xmlResponse.getAmount()));
		fields.put(PaymentConstants.XML_RESPONSE.CURRENCY.getValue(), StringUtil.getNotNullString(xmlResponse.getCurrency()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYMENT_METHOD.getValue(),
				StringUtil.getNotNullString(xmlResponse.getPaymentMethod()));
		fields.put(PaymentConstants.XML_RESPONSE.BRAND.getValue(), StringUtil.getNotNullString(xmlResponse.getBrand()));
		fields.put(PaymentConstants.XML_RESPONSE.SHAREQUIRED.getValue(), "N");

		fields.put(PaymentConstants.IPG_SESSION_ID, sessionId);

		log.debug("### Returning the receipt map...");
		return fields;
	}

	public IBECommonDTO getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
