package com.isa.thinair.ibe.core.web.v2.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;

/**
 * 
 * @author pkarunanayake
 * 
 */
public class SessionFilter implements Filter {

	private FilterConfig config;

	public SessionFilter() {
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.config = filterConfig;
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws java.io.IOException, ServletException {

		boolean isSessionCheck = true;
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		// TODO Need to improve
		String requestUrl = request.getRequestURI();
		String excludeActions = config.getInitParameter("excludeActions");

		if (excludeActions != null && !excludeActions.isEmpty()) {
			for (String excludeAction : excludeActions.split(",")) {
				if (requestUrl != null && requestUrl.trim().contains(excludeAction.trim())) {
					isSessionCheck = false;
					break;
				}
			}
		}
		if (isSessionCheck && SessionUtil.isSessionExpired(request)) {
			request.getRequestDispatcher(StrutsConstants.Jsp.Common.SESSION_EXPIRED).forward(request, response);
		} else {
			chain.doFilter(servletRequest, servletResponse);
		}
	}

	public void destroy() {
		/*
		 * called before the Filter instance is removed from service by the web container
		 */
	}

}
