package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.Date;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.CustomerSession;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.opensymphony.xwork2.ActionChainResult;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Jsp.Customer.LOGIN, value = StrutsConstants.Jsp.Customer.LOGIN),
		@Result(name = StrutsConstants.Jsp.Customer.LOGIN_V3, value = StrutsConstants.Jsp.Customer.LOGIN_V3),
		@Result(name = StrutsConstants.Jsp.Customer.HOME, value = StrutsConstants.Jsp.Customer.HOME),
		@Result(name = StrutsConstants.Jsp.Customer.HOME_V3, value = StrutsConstants.Jsp.Customer.HOME_V3)
})
public class LoginApiAction extends CustomerForcedLoginAction {

	private String token;

	@Override
	protected boolean accessGranted() {

		boolean accessGranted = false;
		AirCustomerServiceBD airCustomerServiceBD = ModuleServiceLocator.getCustomerBD();
		CustomerSession customerSession = airCustomerServiceBD.loadCustomerSession(token);

		int tokenValidityPeriod = AppSysParamsUtil.getLoginApiTokenValidityPriod() * 60 * 1000;
				
		long createdDate;
		long currentDate;
		
		if (customerSession != null) {

			createdDate = customerSession.getCreatedTime().getTime();
			currentDate = new Date().getTime();
			
			if (tokenValidityPeriod >= (currentDate - createdDate)) {
				customerId = customerSession.getCustomerId();
				accessGranted = true;
			} else {
			}
		} else {
		}

		return accessGranted;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
