package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

/**
 * Action class for IBE Baggages
 * 
 * @author Ishan
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciBaggageAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(InterlineAnciMealAction.class);

	private boolean success = true;

	private String messageTxt;

	private LCCBaggageResponseDTO baggageResponceDTO;

	private List<FlightSegmentTO> flightSegmentTOs;

	private String oldAllSegments;

	private boolean modifySegment;

	private String isModifyAnci;

	private boolean isAnciOffer;

	private String anciOfferName;

	private String anciOfferDescription;

	private String jsonOnds;

	private String pnr;

	private boolean modifyAncillary;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			String strTxnIdntifier = ibeResInfo.getTransactionId();

			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}

			SYSTEM system = ibeResInfo.getSelectedSystem();
			String selectedLanguage = SessionUtil.getLanguage(request);
			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams(),
					(modifySegment && AppSysParamsUtil.isRequoteEnabled()));
			flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();

			SelectedFltSegBuilder totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);

			if (ibeResInfo.getPriceInfoTO() != null) {
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(ibeResInfo.getPriceInfoTO(),
						flightSegmentTOs, totalFltSegBuilder.isReturn(), totalFltSegBuilder.getSelectedFlightSegments());
			} else {
				List<FlightSegmentTO> oldFlightSegmentTOs = totalFltSegBuilder.getSelectedFlightSegments();
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(oldFlightSegmentTOs, flightSegmentTOs,
						searchParams.getAdultCount(), searchParams.getChildCount(), searchParams.getInfantCount(),
						totalFltSegBuilder.isReturn());
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
			}

			List<LCCBaggageRequestDTO> lccBaggageRequestDTOs = composeBaggageRequest(flightSegmentTOs, getSearchParams()
					.getClassOfService(), strTxnIdntifier);

			List<BundledFareDTO> bundledFareDTOs = ibeResInfo.getSelectedBundledFares();

			baggageResponceDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableBaggages(lccBaggageRequestDTOs,
					strTxnIdntifier, system, selectedLanguage, BeanUtils.nullHandler(isModifyAnci).equals("true") ? true : false,
					false, false, ApplicationEngine.IBE, ibeResInfo.getBaggageSummaryTo(), bundledFareDTOs, pnr,
					TrackInfoUtil.getBasicTrackInfo(request));

			setAnciOfferDataToSession(baggageResponceDTO, ibeResInfo);
			setAnciOfferDetailsForDisplay(baggageResponceDTO);

		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("BAGGAGE REQ ERROR", e);
		}
		return result;
	}

	/**
	 * Compose the baggage request list from the flight segment list
	 * 
	 * @param flightSegTO
	 * @param txnId
	 * @return List<LCCBaggageRequestDTO>
	 */
	private List<LCCBaggageRequestDTO> composeBaggageRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCBaggageRequestDTO> bL = new ArrayList<LCCBaggageRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCBaggageRequestDTO brd = new LCCBaggageRequestDTO();
			brd.setTransactionIdentifier(txnId);
			brd.setCabinClass(cabinClass);
			brd.setFlightSegment(fst);
			bL.add(brd);
		}
		return bL;
	}

	private void setAnciOfferDataToSession(LCCBaggageResponseDTO baggageDetails, IBEReservationInfoDTO ibeResInfo) {
		for (LCCFlightSegmentBaggagesDTO segmentBaggages : baggageDetails.getFlightSegmentBaggages()) {

			if (!segmentBaggages.isAnciOffer()) {
				continue;
			}

			String flightRef = AnciOfferUtil.getFlightRefWithoutLCCTxnRefDetails(segmentBaggages.getFlightSegmentTO()
					.getFlightRefNumber());

			if (ibeResInfo.getAnciOfferTemplates().get(flightRef) == null) {
				ibeResInfo.getAnciOfferTemplates().put(flightRef, new HashMap<EXTERNAL_CHARGES, Integer>());
			}
			ibeResInfo.getAnciOfferTemplates().get(flightRef)
					.put(EXTERNAL_CHARGES.BAGGAGE, segmentBaggages.getOfferedTemplateID());
		}
		SessionUtil.setIBEreservationInfo(request, ibeResInfo);
	}

	private void setAnciOfferDetailsForDisplay(LCCBaggageResponseDTO baggageDetails) {
		Collections.sort(baggageDetails.getFlightSegmentBaggages());
		for (LCCFlightSegmentBaggagesDTO segmentBaggages : baggageDetails.getFlightSegmentBaggages()) {
			if (segmentBaggages.isAnciOffer()) {
				isAnciOffer = true;
				anciOfferName = segmentBaggages.getOfferName();
				anciOfferDescription = segmentBaggages.getOfferDescription();
				break;
			}
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public LCCBaggageResponseDTO getBaggageResponceDTO() {
		return baggageResponceDTO;
	}

	public void setBaggageResponceDTO(LCCBaggageResponseDTO baggageResponceDTO) {
		this.baggageResponceDTO = baggageResponceDTO;
	}

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	/**
	 * @return the isModifyAnci
	 */
	public String getIsModifyAnci() {
		return isModifyAnci;
	}

	/**
	 * @param isModifyAnci
	 *            the isModifyAnci to set
	 */
	public void setIsModifyAnci(String isModifyAnci) {
		this.isModifyAnci = isModifyAnci;
	}

	public String getOldAllSegments() {
		return oldAllSegments;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	/**
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	/**
	 * @return the anciOfferName
	 */
	public String getAnciOfferName() {
		return anciOfferName;
	}

	/**
	 * @param anciOfferName
	 *            the anciOfferName to set
	 */
	public void setAnciOfferName(String anciOfferName) {
		this.anciOfferName = anciOfferName;
	}

	/**
	 * @return the anciOfferDescription
	 */
	public String getAnciOfferDescription() {
		return anciOfferDescription;
	}

	/**
	 * @param anciOfferDescription
	 *            the anciOfferDescription to set
	 */
	public void setAnciOfferDescription(String anciOfferDescription) {
		this.anciOfferDescription = anciOfferDescription;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}
}
