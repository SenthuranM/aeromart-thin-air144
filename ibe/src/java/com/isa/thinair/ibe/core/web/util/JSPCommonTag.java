package com.isa.thinair.ibe.core.web.util;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * God forgive me for this. Need to do a quick fix to hide multicity option in the IBE.
 * There is no other faster way of doing it than this option.
 * 
 * @author Nilindra Fernando
 */
public class JSPCommonTag {

	private static final boolean isShowMultiCitySearch = AppSysParamsUtil.isCityBasedAvailabilityEnabled();
	
	public static boolean isShowMultiCitySearch() {
		return isShowMultiCitySearch;
	}
	
}
