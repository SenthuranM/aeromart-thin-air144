package com.isa.thinair.ibe.core.web.handler.agent;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.AgentWebConstants;
import com.isa.thinair.ibe.core.web.constants.RequestParameter;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Nasly Modified by lalanthi JIRA : 2650 Handles agent search for IBE.
 */
public class ShowAgentsInfoRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ShowAgentsInfoRH.class);

	public static String execute(HttpServletRequest request) {
		String forward = StrutsConstants.Result.SUCCESS;

		String mode = request.getParameter(RequestParameter.MODE);
		try {
			if (mode == null || mode == "") {
				setDisplayData(request);
			} else if (mode.equalsIgnoreCase("SEARCH")) {
				setDisplayData(request);
				String s = getAgentsRowHtml(request);
				request.setAttribute("agents", s);
				String agentType = getSelectedAgentType(request);
				request.setAttribute("agentType", agentType);
			}
		} catch (ModuleException me) {
			forward = StrutsConstants.Result.ERROR;
			log.error("ShowAgentsInfoRH.execute", me);
		} catch (RuntimeException re) {
			forward = StrutsConstants.Result.ERROR;
			log.error("ShowAgentsInfoRH.execute", re);
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) {

		try {
			setStationSelectList(request);
			setClientErrors(request);
			setCountrySelectList(request);
			setAgentTypesList(request);
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * Builds the agents details screen.
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	private static String getAgentsRowHtml(HttpServletRequest request) throws ModuleException {

		// String country = request.getParameter("selCountry");
		// String city = request.getParameter("selStation");
		// String agentType = request.getParameter("selAgentType");

		String country = request.getParameter("hdnCountryCode");
		String city = request.getParameter("hdnCityCode");
		// String agentTypeCodes = request.getParameter("selAgentType");
		String agentTypeCodes = request.getParameter("hdnAgentTypes");
		String agentType = null;
		if (agentTypeCodes != null && "2".equals(agentTypeCodes)) {
			agentType = "GSASO";
		} else if (agentTypeCodes != null && "3".equals(agentTypeCodes)) {
			agentType = "TA";
		} else {
			agentType = "ALL";
		}

		if (city == null || city.length() <= 0) {
			city = "ALL";
		}

		StringBuilder sb = new StringBuilder();
		sb.append("var arrAgents = new Array();");

		if ((country != null && country.length() > 2) || city.length() > 3) {
			log.error("##AGENT_INFO## Invalid Values in AgentInfo Search Country : - " + country + " AND city : - " + city);
			return sb.toString();
		}

		Collection<AgentInfoDTO> agentsList = ModuleServiceLocator.getTravelAgentBD().getAgentsByLocationAndType(country, city,
				agentType);
		if (agentsList != null && agentsList.size() > 0) {
			int i = 0;
			for (AgentInfoDTO agent : agentsList) {

				String address = isEmptyString(agent.getAddressLine1()) ? "" : JavaScriptGenerator.nullConvertToString(agent
						.getAddressLine1()) + ",";
				address += isEmptyString(agent.getAddressLine2()) ? "" : JavaScriptGenerator.nullConvertToString(agent
						.getAddressLine2());
				/** JIRA- AARESAA:3334 */
				// address +=
				// isEmptyString(agent.getAddressCity())?"":JavaScriptGenerator.nullConvertToString(agent.getAddressCity())+",";
				// String agentProvince= isEmptyString(agent.getAddressStateProvince())?"":
				// JavaScriptGenerator.nullConvertToString(agent.getAddressStateProvince())+",";
				/*
				 * if(agentProvince.indexOf(" Not Required")>0){ address +=""; } else{ address +=agentProvince; }
				 */
				// address += JavaScriptGenerator.nullConvertToString(agent.getPostalCode());
				if (address.length() > 0 && (address.lastIndexOf(",") == (address.length() - 1))) {
					address = address.substring(0, address.lastIndexOf(","));// To remove the trailing comma,if other
																				// fields are empty
				}
				sb.append(" arrAgents[" + i + "] = new Array();");
				sb.append("arrAgents[" + i + "][0]='" + JavaScriptGenerator.nullConvertToString(agent.getAgentCode()) + "';");
				sb.append("arrAgents[" + i + "][1]='" + JavaScriptGenerator.nullConvertToString(agent.getAgentName()) + "';");
				sb.append("arrAgents[" + i + "][2]='" + JavaScriptGenerator.nullConvertToString(agent.getAgentTypeDesc()) + "';");
				sb.append("arrAgents[" + i + "][3]='" + JavaScriptGenerator.nullConvertToString(address) + "';");
				/** JIRA - AARESAA 3334 (Lalanthi) */
				String email = agent.getEmailId();
				if (email != null && email.length() > 0) {
					if (email.indexOf(",") > 0) {
						email = email.substring(0, email.indexOf(","));
					}
				}
				sb.append("arrAgents[" + i + "][8]='" + JavaScriptGenerator.nullConvertToString(email) + "';");
				sb.append("arrAgents[" + i + "][9]='" + JavaScriptGenerator.nullConvertToString(agent.getFax()) + "';");
				sb.append("arrAgents[" + i + "][10]='" + JavaScriptGenerator.nullConvertToString(agent.getCurrencyCode()) + "';");
				sb.append("arrAgents[" + i + "][11]='" + JavaScriptGenerator.nullConvertToString(agent.getBillingEmail()) + "';");
				sb.append("arrAgents[" + i + "][12]='" + JavaScriptGenerator.nullConvertToString(agent.getLocationUrl()) + "';");
				sb.append("arrAgents[" + i + "][13]='" + JavaScriptGenerator.nullConvertToString(agent.getTelephone()) + "';");
				sb.append("arrAgents[" + i + "][14]='" + JavaScriptGenerator.nullConvertToString(agent.getStationCode()) + "';");
				sb.append("arrAgents[" + i + "][15]='" + agent.getVersion() + "';");
				sb.append("arrAgents[" + i + "][16]='" + JavaScriptGenerator.nullConvertToString(agent.getStatus()) + "';");
				i++;
			}
		}
		return sb.toString();
	}

	/**
	 * This method will set the requested agent type and sends back in the response as in the UI logic need to show
	 * GoogleMap URL only if the agent type is not TA
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	private static String getSelectedAgentType(HttpServletRequest request) throws ModuleException {
		String agentTypeCodes = request.getParameter("hdnAgentTypes");
		String agentType = "var agentType=";
		if (agentTypeCodes != null && "2".equals(agentTypeCodes)) {
			agentType += "'GSASO';";
		} else {
			agentType += "'OTHER';";
		}
		return agentType;
	}

	/**
	 * @param request
	 * @throws ModulesException
	 */
	private static void setCountrySelectList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createCountryNameList();
		request.setAttribute(AgentWebConstants.REQ_HTML_COUNTRY_SELECT_LIST, strHtml);

	}

	private static void setStationSelectList(HttpServletRequest request) throws ModuleException {
		String strStationComboList = SelectListGenerator.createStationCodeList();
		request.setAttribute(AgentWebConstants.REQ_HTML_STATION_SELECT_LIST, strStationComboList);

	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {

	}

	private static void setAgentTypesList(HttpServletRequest request) throws ModuleException {
		String strAgentTypes = SelectListGenerator.createAgentTypeCodesList();
		request.setAttribute(AgentWebConstants.REQ_AGENT_TYPES, strAgentTypes);
	}

	private static boolean isEmptyString(String theString) {
		return (theString == null || theString.length() == 0);
	}
}
