package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

/**
 * @author Janaka Padukka
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class AddBusSegmentAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AddBusSegmentAction.class);

	private boolean success = true;

	private String messageTxt;

	private boolean groupPNRNo = false;

	private String segmentRefNo;

	private String oldAllSegments;

	private HashMap<String, String> jsonLabel;

	private List<String[]> fromAirportData;

	private List<String[]> toAirportData;

	@SuppressWarnings("unchecked")
	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
			Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getvalidateModifingSegment(colsegs,
					resInfo.getSegmentStatus(), segmentRefNo);

			// For connection bookings
			String operatingCarrierCode = ReservationUtil.getDryOperatingCarrier(colModSegs);
			String depSegment = null;
			String arrSegment = null;
			Date arrivalDate = null;
			Date departureDateZulu = null;
			String selectedOnd;
			for (LCCClientReservationSegment seg : colsegs) {
				for (LCCClientReservationSegment intModSeg : colModSegs) {
					if (intModSeg.getBookingFlightSegmentRefNumber().equals(seg.getBookingFlightSegmentRefNumber())) {
						if (departureDateZulu == null || departureDateZulu.after(seg.getDepartureDateZulu())) {
							depSegment = seg.getSegmentCode();
							departureDateZulu = seg.getDepartureDateZulu();
						}
						if (arrivalDate == null || arrivalDate.before(seg.getArrivalDateZulu())) {
							arrivalDate = seg.getArrivalDateZulu();
							arrSegment = seg.getSegmentCode();
						}
					}
				}
			}
			String depAirport = depSegment.split("/")[0];
			String[] arrSegmentCodes = arrSegment.split("/");
			String arrAirport = arrSegmentCodes[arrSegmentCodes.length - 1];
			selectedOnd = depAirport + "/" + arrAirport;
			this.prepareGroundSegmentData(groupPNRNo, selectedOnd, true, operatingCarrierCode);
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "Form", "PgAddBusSegment", "Common" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);

		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("addBusegmentAction==>" + ex);
		}
		return forward;
	}

	private void
			prepareGroundSegmentData(boolean groupPNRNo, String strOnd, boolean isSubStationAllowed, String operatingCarrier)
					throws JSONException, ModuleException {

		GroundSegmentTO groundSegmentTO = new GroundSegmentTO();
		groundSegmentTO.setAddGroundSegment(isSubStationAllowed);
		groundSegmentTO.setGroupPNRNo(groupPNRNo);
		groundSegmentTO.setSelectedOnd(strOnd);
		groundSegmentTO.setOperatingCarrier(operatingCarrier);
		List[] arrList = ModuleServiceLocator.getAirproxySegmentBD().getGroundSegment(groundSegmentTO,
				TrackInfoUtil.getBasicTrackInfo(request));
		fromAirportData = arrList[0];
		toAirportData = arrList[1];

	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setSegmentRefNo(String segmentRefNo) {
		this.segmentRefNo = segmentRefNo;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public List<String[]> getFromAirportData() {
		return fromAirportData;
	}

	public void setFromAirportData(List<String[]> fromAirportData) {
		this.fromAirportData = fromAirportData;
	}

	public List<String[]> getToAirportData() {
		return toAirportData;
	}

	public void setToAirportData(List<String[]> toAirportData) {
		this.toAirportData = toAirportData;
	}

}
