package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class LoyaltyAccountDetailAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(LoyaltyAccountDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private HashMap<String, String> jsonLabel;

	private String errorInfo;

	private List nationalityList;

	private List countryNameList;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "PgLoyalty", "Form" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			nationalityList = SelectListGenerator.getNationalityList();
			countryNameList = SelectListGenerator.getCountryNameList();

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("LoyaltyAccountDetailAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("LoyaltyAccountDetailAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public List getNationalityList() {
		return nationalityList;
	}

	public List getCountryNameList() {
		return countryNameList;
	}

}
