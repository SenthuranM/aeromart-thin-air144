package com.isa.thinair.ibe.core.web.action.agent;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.handler.agent.ShowAgentsInfoRH;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Agent.AGENTS_INFO),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Agent.AGENTS_INFO) })
public class ShowAgentsInfoAction {
	public String execute() {
		return ShowAgentsInfoRH.execute(ServletActionContext.getRequest());
	}
}
