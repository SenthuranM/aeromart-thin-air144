package com.isa.thinair.ibe.core.web.action.login;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.api.dto.URLTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.createres.ShowAddPaxHG;
import com.isa.thinair.ibe.core.web.util.CommonUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;

/**
 * POJO to handle the passenger login
 * 
 * @author Haider copied from AvailabilitySearchAction which done by Lasantha Pambagoda
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AjaxLoginAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AjaxLoginAction.class);

	private String custProfile;

	private String txtUID;

	private String txtPWD;

	private String txtUname;

	private boolean loginSuccess;

	private CustomerDTO customer;

	private String totalCustomerCredit;

	private Object baseCurr;

	private boolean socialLogin;
	
	private LmsMemberDTO lmsDetails;
	
	private String joinLms;

	private ContactInfoDTO contactInfo = new ContactInfoDTO();
	
	public boolean isLoginSuccess() {
		return loginSuccess;
	}

	public String getTxtUname() {
		return txtUname;
	}

	public void setTxtUname(String txtUname) {
		this.txtUname = txtUname;
	}

	public void setLoginSuccess(boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}

	public String getCustProfile() {
		return custProfile;
	}

	public void setTxtUID(String txtUID) {
		this.txtUID = txtUID;
	}

	public void setTxtPWD(String password) {
		this.txtPWD = password;
	}

	public String execute() throws ModuleException {
		try {
			Customer customerModel = null;
			
			if (AppSysParamsUtil.isShowIbeSocialLoginInPaxPage() && socialLogin) {
				AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
				customerModel = customerDelegate.getCustomer(txtUID); //3rd party site authorization is done already
				if (customerModel != null) {
					SessionUtil.setCustomerId(request, customerModel.getCustomerId());
					SessionUtil.setCustomerEMail(request, customerModel.getEmailId());
					SessionUtil.setCustomerPassword(request, customerModel.getPassword());
				}
			} else {
				customerModel = CommonUtil.loginCustomer(txtUID, txtPWD, request);
			}
			
			if (customerModel != null) {
				custProfile = ShowAddPaxHG.createProfileInfo(customerModel);
				txtUname = customerModel.getTitle().toLowerCase() + ". " + customerModel.getFirstName().toLowerCase() + " "
						+ customerModel.getLastName().toLowerCase();
				loginSuccess = true;
				// Interline login User
				if (request.getParameter("page") != null && request.getParameter("page").equals("interline")) {
					URLTO paxUrlTo = BookingUtil.getURLData(request);
					paxUrlTo.setCustomer(true);
					// SessionUtil.setIBEURLInfo(request, paxUrlTo);
				}

				LoyaltyCustomerServiceBD loyaltyCustomerBD = ModuleServiceLocator.getLoyaltyCustomerBD();
				LoyaltyCustomerProfile loyaltyProfile = loyaltyCustomerBD.getLoyaltyCustomerByCustomerId(customerModel
						.getCustomerId());
				if (loyaltyProfile != null)
					SessionUtil.setLoyaltyAccountNo(request, loyaltyProfile.getLoyaltyAccountNo());

				// Customer customerModel = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
				if (customerModel != null) {
					int intCustomerID = customerModel.getCustomerId();
					customer = CustomerUtilV2.getCustomerNameDetail(customerModel);
					createProfileInfo(customerModel);
					totalCustomerCredit = CustomerUtilV2.getCustomerTotalCredit(ModuleServiceLocator.getReservationQueryBD()
							.getReservationCredits(intCustomerID));
				}
				baseCurr = AppSysParamsUtil.getBaseCurrency();

				if (!customerModel.getCustomerLmsDetails().isEmpty()){
					LmsMember lmsMember = customerModel.getCustomerLmsDetails().iterator().next();
					SessionUtil.setLoyaltyFFID(request, lmsMember.getFfid());
					lmsDetails = CustomerUtilV2.getLmsMemberDTO(lmsMember);
				}

			} else {
				loginSuccess = false;
			}
			
			if ("on".equals(joinLms)){
				Locale locale = new Locale(SessionUtil.getLanguage(request));
				LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
				LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
				LmsMember lmsMember = CustomerUtilV2.getModelLmsMember(lmsDetails, customerModel.getCustomerId());
				LmsCommonUtil.lmsEnroll(lmsMember, SessionUtil.getCarrier(request), loyaltyManagementBD, lmsMemberDelegate, locale, false);
			}
		} catch (Exception e) {
			loginSuccess = false;
			log.error("Unhandled Error @ AjaxLoginAction", e);
		}

		return StrutsConstants.Result.SUCCESS;
	}
	
	private void createProfileInfo(Customer customer) throws ModuleException {

		this.contactInfo.setTitle(StringUtil.nullConvertToString(customer.getTitle()));
		this.contactInfo.setFirstName(StringUtil.nullConvertToString(customer.getFirstName()));
		this.contactInfo.setLastName(StringUtil.nullConvertToString(customer.getLastName()));

		this.contactInfo.setEmailAddress(StringUtil.nullConvertToString(customer.getEmailId()));

		if (customer.getNationalityCode() != null) {
			this.contactInfo.setNationality(StringUtil.nullConvertToString(new Integer(customer.getNationalityCode())));
		} else {
			this.contactInfo.setNationality("");
		}

	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public String getTotalCustomerCredit() {
		return totalCustomerCredit;
	}

	public void setTotalCustomerCredit(String totalCustomerCredit) {
		this.totalCustomerCredit = totalCustomerCredit;
	}

	public Object getBaseCurr() {
		return baseCurr;
	}

	public void setBaseCurr(Object baseCurr) {
		this.baseCurr = baseCurr;
	}

	public void setSocialLogin(boolean socialLogin) {
		this.socialLogin = socialLogin;
	}

	public boolean isSocialLogin() {
		return socialLogin;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public String getJoinLms() {
		return joinLms;
	}

	public void setJoinLms(String joinLms) {
		this.joinLms = joinLms;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

}
