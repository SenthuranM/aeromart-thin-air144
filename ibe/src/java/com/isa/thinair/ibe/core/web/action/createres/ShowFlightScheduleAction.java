package com.isa.thinair.ibe.core.web.action.createres;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.handler.createres.ShowFlightScheduleRH;

/**
 * @author Haider Sahib on 23July2008
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Reservation.FLIGHT_SCHEDULE),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Reservation.FLIGHT_SCHEDULE) })
public class ShowFlightScheduleAction {
	public String execute() {
		return ShowFlightScheduleRH.execute(ServletActionContext.getRequest());
	}
}
