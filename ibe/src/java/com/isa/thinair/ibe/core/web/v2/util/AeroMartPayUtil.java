package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AeroMartPayCardInfo;
import com.isa.thinair.ibe.api.dto.AeroMartPayPaymentGatewayInfo;
import com.isa.thinair.ibe.api.dto.AeroMartPayQueryDTO;
import com.isa.thinair.ibe.api.dto.AeroMartPaySessionInfo;
import com.isa.thinair.ibe.api.dto.SearchPayOptionDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayStatus;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.CardBehavior;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.IPGProviderCode;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.platform.api.ServiceResponce;

public class AeroMartPayUtil {

	private static Log log = LogFactory.getLog(AeroMartPayUtil.class);

	public static String buildHashString(List<String> hashStringParamsInOrder, String secureHashKey) {
		String hashString = "";

		try {

			StringBuilder messageBuilder = new StringBuilder();

			for (String param : hashStringParamsInOrder) {
				messageBuilder.append(param);
			}

			Mac sha256HMAC = Mac.getInstance(AeroMartPayConstants.HMAC_ALGO);
			SecretKeySpec secretKey = new SecretKeySpec(secureHashKey.getBytes(), AeroMartPayConstants.HMAC_ALGO);
			sha256HMAC.init(secretKey);

			hashString = Base64.getEncoder().encodeToString(sha256HMAC.doFinal(messageBuilder.toString().getBytes()));

		} catch (Exception e) {
			log.error("Error while creating hashString", e);
		}

		return hashString;
	}

	public static boolean isUniqueOrderID(String orderID) throws ModuleException {

		ServiceResponce serviceResponce = ModuleServiceLocator.getAirproxyReservationBD().isPaymentReferenceAvailable(orderID,
				AeroMartPayConstants.PRODUCT_TYPE);
		// if success then exists
		return !serviceResponce.isSuccess();

	}

	public static AeroMartPayQueryDTO getPaymentDetails(String orderID) throws ModuleException {
		AeroMartPayQueryDTO aeroMartPayQueryDTO = new AeroMartPayQueryDTO();

		try {

			Collection<TempPaymentTnx> tempTnxCollection = ModuleServiceLocator.getReservationBD().getTempPaymentsForReservation(
					orderID);

			if (tempTnxCollection == null || tempTnxCollection.size() == 0) {
				log.error("aeroMartPay Query orderID lookup result null,  orderID: " + orderID);
				aeroMartPayQueryDTO.setStatus(AeroMartPayStatus.NOT_FOUND.getStatusMessage());

			} else if (tempTnxCollection.size() > 1) {
				log.info("aeroMartPay Query orderID lookup multiple results, orderID: " + orderID);

				int countFails = 0;
				int countSuccesses = 0;
				TempPaymentTnx successTnx = null;
				TempPaymentTnx failureTnx = null;
				TempPaymentTnx effectivePaymentTnx = null;

				for (TempPaymentTnx tempTnx : tempTnxCollection) {
					if (AeroMartPayStatus.PAYMENT_FAILED.getStatus().contentEquals(tempTnx.getStatus())) {
						countFails++;
						failureTnx = tempTnx;
					} else if (AeroMartPayStatus.PAYMENT_COMPLETED.getStatus().contentEquals(tempTnx.getStatus())) {
						countSuccesses++;
						successTnx = tempTnx;
					}

				}

				if ((countFails + countSuccesses) > AppSysParamsUtil.getPaymentAttemptLimit()) {
					log.error("Attempts exceeded the limit found for orderID: " + orderID + ",  requires manual inspection");
					aeroMartPayQueryDTO.setStatus(AeroMartPayStatus.MULTIPLE_ATTEMPTS_INVALID.getStatusMessage());

				} else if (countSuccesses == 0 && countFails > 0) {
					log.info("Failure attempts only found for orderID: " + orderID + ",  paymentfailed attempts: " + countFails);
					aeroMartPayQueryDTO.setStatus(AeroMartPayStatus.PAYMENT_FAILED.getStatusMessage());
					effectivePaymentTnx = failureTnx; // update last failure Tnx

				} else if (countSuccesses > 1) {
					log.error("Multiple Success entries for orderID: " + orderID + ", requires manual inspection");
					aeroMartPayQueryDTO.setStatus(AeroMartPayStatus.MULTIPLE_RECORDS_INVALID.getStatusMessage());

				} else if (countSuccesses == 1) {
					log.info("Payment completed for orderID: " + orderID + ", paymentfailed attempts: " + countFails);
					aeroMartPayQueryDTO.setStatus(AeroMartPayStatus.PAYMENT_COMPLETED.getStatusMessage());
					effectivePaymentTnx = successTnx; // update last failure Tnx

				} else {
					log.error("Undefined state for orderID: " + orderID + ", requires manual inspection");
					aeroMartPayQueryDTO.setStatus(AeroMartPayStatus.UNDEFINED.getStatusMessage());
				}

				if (effectivePaymentTnx != null) {
					prepareAeroMartPayQueryDTO(aeroMartPayQueryDTO, effectivePaymentTnx);

				}
			} else if (tempTnxCollection.size() == 1) {

				prepareAeroMartPayQueryDTO(aeroMartPayQueryDTO, tempTnxCollection.iterator().next());
			}

		} catch (Exception e) {

			log.error("aeroMartPay Query orderID lookup Failed, orderID: " + orderID, e);

		}

		return aeroMartPayQueryDTO;

	}

	private static void prepareAeroMartPayQueryDTO(AeroMartPayQueryDTO aeroMartPayQueryDTO, TempPaymentTnx tempPaymentTnx) {

		aeroMartPayQueryDTO.setReferenceID(String.valueOf(tempPaymentTnx.getTnxId()));
		aeroMartPayQueryDTO.setStatus(deriveAeroMartPayStatus(tempPaymentTnx.getStatus()));
		aeroMartPayQueryDTO.setPaymentCurrencyCode(tempPaymentTnx.getPaymentCurrencyCode());
		aeroMartPayQueryDTO.setPaymentCurrencyAmount(String.valueOf(tempPaymentTnx.getPaymentCurrencyAmount()));
	}

	private static String deriveAeroMartPayStatus(String status) {

		String aeroMartPayStatus = null;

		if (AeroMartPayStatus.PAYMENT_FAILED.getStatus().contentEquals(status)) {
			aeroMartPayStatus = AeroMartPayStatus.PAYMENT_FAILED.getStatusMessage();
		} else if (AeroMartPayStatus.PAYMENT_INITIATED.getStatus().contentEquals(status)) {
			aeroMartPayStatus = AeroMartPayStatus.PAYMENT_INITIATED.getStatusMessage();
		} else if (AeroMartPayStatus.PAYMENT_COMPLETED.getStatus().contentEquals(status)) {
			aeroMartPayStatus = AeroMartPayStatus.PAYMENT_COMPLETED.getStatusMessage();
		} else if (AeroMartPayStatus.PAYMENT_REFUNDED.getStatus().contentEquals(status)) {
			aeroMartPayStatus = AeroMartPayStatus.PAYMENT_REFUNDED.getStatusMessage();
		} else if (AeroMartPayStatus.PAYMENT_REFUND_FAILED.getStatus().contentEquals(status)) {
			aeroMartPayStatus = AeroMartPayStatus.PAYMENT_REFUND_FAILED.getStatusMessage();
		} else {
			aeroMartPayStatus = AeroMartPayStatus.UNDEFINED.getStatusMessage();
		}

		return aeroMartPayStatus;
	}

	public static String generateReferenceID(BigDecimal totalAmount, String ccNo, boolean isCredit, String firstName,
			String lastName, String productID, String mobileNumber, String telephoneNumber, BigDecimal paymentCurrencyAmount,
			String paymentCurrency, String email, char productType) throws ModuleException {

		TemporyPaymentTO temporyPaymentTO = composeTemporyPaymentTO(totalAmount, ccNo, true, firstName, lastName, productID,
				mobileNumber, mobileNumber, paymentCurrencyAmount, paymentCurrency, email, productType);

		TempPaymentTnx tempPaymentTnx = ReservationModuleUtils.getReservationBD().recordTemporyPaymentEntry(temporyPaymentTO);

		return String.valueOf(tempPaymentTnx.getTnxId());

	}

	public static IPGPaymentOptionDTO getSelectedPaymentGateway(Integer ipgId) throws ModuleException {

		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		ipgPaymentOptionDTO.setPaymentGateway(ipgId);
		ipgPaymentOptionDTO.setOfflinePayment(true);

		List<IPGPaymentOptionDTO> ipgPaymentGateways = paymentBrokerBD.getPaymentGateways(ipgPaymentOptionDTO);

		if (ipgPaymentGateways != null && !ipgPaymentGateways.isEmpty()) {
			return ipgPaymentGateways.get(0);

		}
		return null;
	}

	private static TemporyPaymentTO composeTemporyPaymentTO(BigDecimal totalAmount, String ccNo, boolean isCredit,
			String firstName, String lastName, String productID, String mobileNumber, String telephoneNumber,
			BigDecimal paymentCurrencyAmount, String paymentCurrency, String email, char productType) {

		TemporyPaymentTO temporyPaymentTO = new TemporyPaymentTO();
		temporyPaymentTO.setAmount(totalAmount);
		temporyPaymentTO.setCcNo(ccNo);
		temporyPaymentTO.setCredit(isCredit);
		temporyPaymentTO.setFirstName(firstName);
		temporyPaymentTO.setLastName(lastName);
		temporyPaymentTO.setGroupPnr(productID);
		temporyPaymentTO.setMobileNo(mobileNumber);
		temporyPaymentTO.setTelephoneNo(telephoneNumber);
		temporyPaymentTO.setPaymentCurrencyAmount(paymentCurrencyAmount);
		temporyPaymentTO.setPaymentCurrencyCode(paymentCurrency);
		temporyPaymentTO.setEmailAddress(email);
		temporyPaymentTO.setProductType(productType);

		return temporyPaymentTO;
	}

	// TODO duplicated from PaymentServiceAdapter
	public static String getCountryCode(String ipAddress) throws ModuleException {
		String countryCode = "OT";

		if (!AppSysParamsUtil.isTestStstem() && !StringUtil.isNullOrEmpty(ipAddress)) {
			countryCode = ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(ipAddress);
		}

		return countryCode;
	}

	public static Properties getIPGProperties(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {

		return ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
	}

	public static TempPaymentTnx getTempPaymentTnx(String referenceID) throws ModuleException, NumberFormatException {

		TempPaymentTnx tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(Integer.valueOf(referenceID));

		if (tempPaymentTnx == null
				|| !("I".contentEquals(tempPaymentTnx.getStatus()) && "I".contentEquals(tempPaymentTnx.getHistory()))) {
			log.error("Invalid Transaction Status, referenceID: " + referenceID);
			throw new ModuleException("Invalid Transaction Status");
		}

		return tempPaymentTnx;

	}

	// TODO duplicated from HandleInterlineIPGResponseAction
	public static Map<String, String> getReceiptMap(XMLResponseDTO xmlResponse, String sessionId) {
		Map<String, String> fields = new LinkedHashMap<String, String>();

		if (log.isDebugEnabled()) {
			log.debug("### Started generating receipt map...");
			log.debug("### Order ID : " + StringUtil.getNotNullString(xmlResponse.getOrderId()));
		}

		fields.put(PaymentConstants.XML_RESPONSE.ORDERID.getValue(), StringUtil.getNotNullString(xmlResponse.getOrderId()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYID.getValue(), StringUtil.getNotNullString(xmlResponse.getPayId()));
		fields.put(PaymentConstants.XML_RESPONSE.NCSTATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getNcStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERROR.getValue(), StringUtil.getNotNullString(xmlResponse.getNcError()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERRORPLUS.getValue(),
				StringUtil.getNotNullString(xmlResponse.getNcErrorPlus()));
		fields.put(PaymentConstants.XML_RESPONSE.ACCEPTANCE.getValue(), StringUtil.getNotNullString(xmlResponse.getAcceptance()));
		fields.put(PaymentConstants.XML_RESPONSE.STATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.ECI.getValue(), StringUtil.getNotNullString(xmlResponse.getEci()));
		fields.put(PaymentConstants.XML_RESPONSE.AMOUNT.getValue(), StringUtil.getNotNullString(xmlResponse.getAmount()));
		fields.put(PaymentConstants.XML_RESPONSE.CURRENCY.getValue(), StringUtil.getNotNullString(xmlResponse.getCurrency()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYMENT_METHOD.getValue(),
				StringUtil.getNotNullString(xmlResponse.getPaymentMethod()));
		fields.put(PaymentConstants.XML_RESPONSE.BRAND.getValue(), StringUtil.getNotNullString(xmlResponse.getBrand()));
		fields.put(PaymentConstants.XML_RESPONSE.SHAREQUIRED.getValue(), "N");

		fields.put(PaymentConstants.IPG_SESSION_ID, sessionId);

		log.debug("### Returning the receipt map...");
		return fields;
	}

	public static Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	public static void setPaymentResponseAttributes(HttpServletRequest request, String orderID, String returnURL,
			String respCode, String respMessage, String referenceID, String totalAmount) {
		// general response settings
		request.setAttribute(AeroMartPayConstants.PARAM_ORDER_ID, orderID);
		request.setAttribute(AeroMartPayConstants.PARAM_RETURN_URL, returnURL);
		request.setAttribute(AeroMartPayConstants.PARAM_RESPONSE_CODE, respCode);
		request.setAttribute(AeroMartPayConstants.PARAM_RESPONSE_MESSAGE, respMessage);
		request.setAttribute(AeroMartPayConstants.PARAM_REFERENCE_ID, referenceID);

		String merchantID = (String) request.getSession().getAttribute(AeroMartPayConstants.PARAM_MERCHANT_ID);
		request.setAttribute(AeroMartPayConstants.PARAM_HASH_STRING,
				getPaymentResponseHashString(merchantID, respCode, orderID, referenceID, totalAmount));

	}

	public static String getPaymentResponseHashString(String merchantID, String respCode, String orderID, String referenceId,
			String totalAmount) {

		Merchant merchant = getMerchant(merchantID);
		List<String> responseParams = new ArrayList<String>();
		responseParams.add(merchantID);
		responseParams.add(merchant.getUsername());
		responseParams.add(AeroMartPayUtil.decryptAeroMartPassword(merchant.getPassword()));
		responseParams.add(orderID);
		responseParams.add(totalAmount);
		responseParams.add(respCode);
		responseParams.add(referenceId);
		return buildHashString(responseParams, merchant.getSecureHashKey());
	}

	// TODO duplicated from PaymentServiceAdapter
	public static IPGRequestDTO composeIPGRequestDTO(String cardType, String cardNumber, String cardHolderName, String cardCVV,
			String expiryDate, IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String ipgPNR, int temporyPayId,
			String paymentAmount, boolean isIntExt, String handleIPGPaymentResponseURL, String languageCode,
			TrackInfoDTO trackInfoDTO, ClientCommonInfoDTO clientInfoDTO) throws ModuleException {

		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

		IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
		ipgRequestDTO.setDefaultCarrierName(globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME));

		ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);

		ipgRequestDTO.setPaymentMethod(PaymentType.getPaymentTypeDesc(Integer.valueOf(cardType)));

		ipgRequestDTO.setAmount(paymentAmount);

		ipgRequestDTO.setCardType(cardType);

		if (isIntExt) {
			ipgRequestDTO.setCardNo(cardNumber);
			ipgRequestDTO.setSecureCode(cardCVV);
			ipgRequestDTO.setHolderName(cardHolderName);
			expiryDate = expiryDate.substring(2, 4) + expiryDate.substring(0, 2);
			ipgRequestDTO.setExpiryDate(expiryDate);
		}

		ipgRequestDTO.setApplicationTransactionId(temporyPayId);

		final String OFFER_URL = globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, trackInfoDTO.getCarrierCode());

		ipgRequestDTO.setReturnUrl(handleIPGPaymentResponseURL);
		ipgRequestDTO.setStatusUrl(handleIPGPaymentResponseURL);
		ipgRequestDTO.setReceiptUrl(handleIPGPaymentResponseURL);
		ipgRequestDTO.setOfferUrl(OFFER_URL);

		ipgRequestDTO.setApplicationIndicator(trackInfoDTO.getAppIndicator());
		ipgRequestDTO.setPnr(ipgPNR);

		// As in current IBE (hard-coded to 2)
		ipgRequestDTO.setNoOfDecimals(2);
		ipgRequestDTO.setRequestedCarrierCode(trackInfoDTO.getCarrierCode());
		ipgRequestDTO.setSessionID(trackInfoDTO.getSessionId());

		ipgRequestDTO.setSessionLanguageCode(languageCode);

		ipgRequestDTO.setUserAgent(clientInfoDTO.getBrowser());

		ipgRequestDTO.setUserIPAddress(clientInfoDTO.getIpAddress());

		ipgRequestDTO.setIpCountryCode(AeroMartPayUtil.getCountryCode(clientInfoDTO.getIpAddress()));

		return ipgRequestDTO;
	}

	public static Map<String, String> setPaymentRequestParamMap(HttpServletRequest request) {
		Map<String, String> requestParams = new HashMap<String, String>();

		setBasicRequestParams(request, requestParams);

		requestParams.put(AeroMartPayConstants.PARAM_RETURN_URL, request.getParameter(AeroMartPayConstants.PARAM_RETURN_URL));

		if (!StringUtil.isNullOrEmpty(request.getParameter(AeroMartPayConstants.PARAM_ERROR_URL))) {
			requestParams.put(AeroMartPayConstants.PARAM_ERROR_URL, request.getParameter(AeroMartPayConstants.PARAM_ERROR_URL));
		} else {
			requestParams.put(AeroMartPayConstants.PARAM_ERROR_URL, request.getParameter(AeroMartPayConstants.PARAM_RETURN_URL));

		}
		requestParams.put(AeroMartPayConstants.PARAM_TOTAL_AMOUNT, request.getParameter(AeroMartPayConstants.PARAM_TOTAL_AMOUNT));
		requestParams.put(AeroMartPayConstants.PARAM_CURRENCY_CODE,
				request.getParameter(AeroMartPayConstants.PARAM_CURRENCY_CODE));

		if (!StringUtil.isNullOrEmpty(request.getParameter(AeroMartPayConstants.PARAM_LANGUAGE_CODE))) {
			requestParams.put(AeroMartPayConstants.PARAM_LANGUAGE_CODE,
					request.getParameter(AeroMartPayConstants.PARAM_LANGUAGE_CODE));
		} else {
			requestParams.put(AeroMartPayConstants.PARAM_LANGUAGE_CODE, AeroMartPayConstants.DEFAULT_LANGUAGE_CODE);
		}
		
		requestParams.put(AeroMartPayConstants.PARAM_AGENT_TYPE, request.getParameter(AeroMartPayConstants.PARAM_AGENT_TYPE));

		return requestParams;
	}

	public static Map<String, String> setQueryRequestParamMap(HttpServletRequest request) {

		Map<String, String> requestParams = new HashMap<String, String>();

		setBasicRequestParams(request, requestParams);

		return requestParams;
	}

	private static void setBasicRequestParams(HttpServletRequest request, Map<String, String> requestParams) {
		requestParams.put(AeroMartPayConstants.PARAM_MERCHANT_ID, request.getParameter(AeroMartPayConstants.PARAM_MERCHANT_ID));
		requestParams.put(AeroMartPayConstants.PARAM_USERNAME, request.getParameter(AeroMartPayConstants.PARAM_USERNAME));
		requestParams.put(AeroMartPayConstants.PARAM_PASSWORD, request.getParameter(AeroMartPayConstants.PARAM_PASSWORD));
		requestParams.put(AeroMartPayConstants.PARAM_HASH_STRING, request.getParameter(AeroMartPayConstants.PARAM_HASH_STRING));
		requestParams.put(AeroMartPayConstants.PARAM_ORDER_ID, request.getParameter(AeroMartPayConstants.PARAM_ORDER_ID));
	}

	public static List<AeroMartPayCardInfo> getCardListForPG(int paymentGateway,
			Collection<CountryPaymentCardBehaviourDTO> cardsInfo) {

		List<AeroMartPayCardInfo> cardListForPG = new ArrayList<AeroMartPayCardInfo>();

		for (CountryPaymentCardBehaviourDTO cardInfo : cardsInfo) {
			if (cardInfo.getPaymentGateWayID() == paymentGateway) {
				AeroMartPayCardInfo aeroMartPayCardInfo = new AeroMartPayCardInfo();
				aeroMartPayCardInfo.setCardType(cardInfo.getCardType());
				aeroMartPayCardInfo.setCssClassName(cardInfo.getCssClassName());
				aeroMartPayCardInfo.setDisplayName(cardInfo.getCardDisplayName());
                //todo into count behavior case
				boolean isOndWisePayment = AppSysParamsUtil.getIsONDWisePayment();

				if (CardBehavior.INT_EXT.getCardBehavior().contentEquals(cardInfo.getBehaviour())) {
					aeroMartPayCardInfo.setRequiredUserInputs(AeroMartPayConstants.INT_EXT_CARD_INPUT);
					aeroMartPayCardInfo.setCardBehavior(CardBehavior.INT_EXT.getCardBehavior());
				} else if (CardBehavior.EXT.getCardBehavior().contentEquals(cardInfo.getBehaviour())) {
					aeroMartPayCardInfo.setRequiredUserInputs(AeroMartPayConstants.EXT_CARD_INPUT);
					aeroMartPayCardInfo.setCardBehavior(CardBehavior.EXT.getCardBehavior());
				}
				cardListForPG.add(aeroMartPayCardInfo);
			}
		}

		return cardListForPG;
	}

	// FIXME add description based on the payment option
	public static String getPaymentGatewayDescription(String pgProviderName) throws ModuleException {

		String description = "";

		if (AeroMartPayConstants.IPGProviderCode.KNET.getIpgProviderCode().contentEquals(pgProviderName)) {
			description = IPGProviderCode.KNET.getDescription();

		} else if (AeroMartPayConstants.IPGProviderCode.CCAVENUE.getIpgProviderCode().contentEquals(pgProviderName)) {
			description = IPGProviderCode.CCAVENUE.getDescription();

		} else if (AeroMartPayConstants.IPGProviderCode.EDIRHAM.getIpgProviderCode().contentEquals(pgProviderName)) {
			description = IPGProviderCode.EDIRHAM.getDescription();

		} else if (AeroMartPayConstants.IPGProviderCode.BEHPARDAKHT.getIpgProviderCode().contentEquals(pgProviderName)) {
			description = IPGProviderCode.BEHPARDAKHT.getDescription();

		}

		else {
			description = IPGProviderCode.COMMON.getDescription();
		}

		return description;
	}

	// TODO duplicated from ReservationUtil
	public static Collection<Integer> getPaymentGatewayIDs(Collection<IPGPaymentOptionDTO> paymentGateways) {
		Set<Integer> gatewayIDs = new HashSet<Integer>();
		if (paymentGateways != null) {
			for (IPGPaymentOptionDTO iPGPaymentOptionDTO : paymentGateways) {
				gatewayIDs.add(iPGPaymentOptionDTO.getPaymentGateway());
			}
		}
		return gatewayIDs;
	}

	public static void setPaymentPageDisplayInfo(HttpServletRequest request,
			List<AeroMartPayPaymentGatewayInfo> paymentGatewayInfo, List<String> messages) throws JSONException {
		request.setAttribute(AeroMartPayConstants.PAYMENT_OPTIONS, JSONUtil.serialize(paymentGatewayInfo));
		request.setAttribute(AeroMartPayConstants.HANDLE_IPG_PAYMENT, AeroMartPayConstants.HANDLE_PAYMENT_URL);
		request.setAttribute(AeroMartPayConstants.MESSAGES, JSONUtil.serialize(messages));

	}

	public static boolean isPost(HttpServletRequest request) {
		if (AeroMartPayConstants.POST.contentEquals(request.getMethod())) {
			return true;
		}

		return false;
	}

	public static List<AeroMartPayPaymentGatewayInfo> getPaymentOptions(SearchPayOptionDTO searchPayOptionTO) {

		List<AeroMartPayPaymentGatewayInfo> aeroMartPayPaymentGatewayInfo = null;

		try {

			List paymentGatewayList = getPaymentGatewayList(searchPayOptionTO);

			if (AppSysParamsUtil.isSelectedCurrencyIPGs()) {
				filterSelectedCurrencyPGs(searchPayOptionTO.getSelectedCurrency(), paymentGatewayList);
			}

			if (paymentGatewayList != null && !paymentGatewayList.isEmpty()) {
				Collection<CountryPaymentCardBehaviourDTO> cardsInfo;
				
				if (AeroMartPayConstants.AGENT.equals(searchPayOptionTO.getAgentType())){
					cardsInfo = getCreditCardsListForAgent(paymentGatewayList);
				}else{
					cardsInfo = getCountryBasedCardsList(searchPayOptionTO.getIpAddress(), paymentGatewayList);
				}

				aeroMartPayPaymentGatewayInfo = new ArrayList<AeroMartPayPaymentGatewayInfo>();

				buildPaymentGatewayInfo(searchPayOptionTO.getSelectedCurrency(),
						searchPayOptionTO.getSelectedCurrencyPaymentAmount(), aeroMartPayPaymentGatewayInfo,
						paymentGatewayList,cardsInfo);

			}
		} catch (ModuleException e) {
			log.error("Get Payment options error", e);
		}

		return aeroMartPayPaymentGatewayInfo;

	}

	private static void buildPaymentGatewayInfo(String selectedCurrency, BigDecimal selectedCurrencyPaymentAmount,
			List<AeroMartPayPaymentGatewayInfo> paymentGatewayInfoList, List paymentGatewayList,
			Collection<CountryPaymentCardBehaviourDTO> cardsInfo) throws ModuleException {

		IPGPaymentOptionDTO ipgPaymentOptionDTO;
		Object[] allPGArr = paymentGatewayList.toArray();

		for (int i = 0; i < allPGArr.length; i++) {

			ipgPaymentOptionDTO = (IPGPaymentOptionDTO) allPGArr[i];

			int paymentGatewayID = ipgPaymentOptionDTO.getPaymentGateway();

			List<AeroMartPayCardInfo> cardInfoList = AeroMartPayUtil.getCardListForPG(paymentGatewayID, cardsInfo);

			if (cardInfoList == null || cardInfoList.isEmpty()) {
				continue;
			}

			AeroMartPayPaymentGatewayInfo paymentGatewayInfo = new AeroMartPayPaymentGatewayInfo();
			paymentGatewayInfoList.add(paymentGatewayInfo);
			paymentGatewayInfo.setCards(cardInfoList);
			paymentGatewayInfo.setGatewayId(paymentGatewayID);
			paymentGatewayInfo.setProviderCode(ipgPaymentOptionDTO.getProviderCode());
			paymentGatewayInfo.setProviderName(ipgPaymentOptionDTO.getProviderName());
			paymentGatewayInfo.setPaymentAmount(selectedCurrencyPaymentAmount);
			paymentGatewayInfo.setPaymentCurrency(ipgPaymentOptionDTO.getSelCurrency());
			paymentGatewayInfo.setUserSelectedCurrency(selectedCurrency);

			BigDecimal amountInPgBaseCurrency = convertAmount(selectedCurrencyPaymentAmount,
					selectedCurrency, ipgPaymentOptionDTO.getBaseCurrency());
			paymentGatewayInfo.setAmountInPgBaseCurrency(amountInPgBaseCurrency.toString());
			// TODO also check i18n
			paymentGatewayInfo
					.setDescription(AeroMartPayUtil.getPaymentGatewayDescription(ipgPaymentOptionDTO.getProviderCode()));

		}

	}
	
	private static BigDecimal getExchangeAmount(BigDecimal paymentAmount, CurrencyExchangeRate currencyExrate, boolean toBaseCurr) {
		if (paymentAmount != null && paymentAmount.doubleValue() > 0 && currencyExrate != null) {
			return convertToPaymentCurrency(paymentAmount, currencyExrate, toBaseCurr);
		}
		return null;
	}
	
	private static BigDecimal convertToPaymentCurrency(BigDecimal effectivePaymentAmount, CurrencyExchangeRate currencyExrate,
			boolean toBaseCurr) {
		if (toBaseCurr) {
			Currency pgCurrency = currencyExrate.getCurrency();
			return AccelAeroRounderPolicy.convertAndRoundToBase(effectivePaymentAmount,
					currencyExrate.getMultiplyingExchangeRate(), pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint());
		} else {
			Currency pgCurrency = currencyExrate.getCurrency();
			return AccelAeroRounderPolicy.convertAndRound(effectivePaymentAmount, currencyExrate.getMultiplyingExchangeRate(),
					pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint());
		}
	}

	private static BigDecimal convertAmount(BigDecimal amount, String fromCurr, String toCurr) throws ModuleException {
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		String baseCurr = AppSysParamsUtil.getBaseCurrency();
		BigDecimal toCurrAmount = amount;

		if (fromCurr.equals(toCurr)) {
			toCurrAmount = amount;
		} else if (fromCurr.equals(baseCurr)) {
			CurrencyExchangeRate currExrateToCurr = exchangeRateProxy.getCurrencyExchangeRate(toCurr,
					ApplicationEngine.IBE);
			toCurrAmount = getExchangeAmount(amount, currExrateToCurr, false);
		} else {
			CurrencyExchangeRate currExrateToBaseCurr = exchangeRateProxy.getCurrencyExchangeRate(fromCurr,
					ApplicationEngine.IBE);
			BigDecimal fromCurrAmountInBaseCurrency = getExchangeAmount(amount, currExrateToBaseCurr, true);
			CurrencyExchangeRate currExrateToCurr = exchangeRateProxy.getCurrencyExchangeRate(toCurr,
					ApplicationEngine.IBE);
			toCurrAmount = getExchangeAmount(fromCurrAmountInBaseCurrency, currExrateToCurr, false);
		}

		return toCurrAmount;
	}

	private static BigDecimal getPaymentAmount(BigDecimal paymentAmount, String userCurrency, String pgCurrency) throws ModuleException {
		return convertAmount(paymentAmount, userCurrency, pgCurrency);
	}

	private static Collection<CountryPaymentCardBehaviourDTO> getCountryBasedCardsList(String ipAddress, List paymentGatewayList)
			throws ModuleException {
		String countryCode = ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(ipAddress);

		if (AppSysParamsUtil.isTestStstem()) {
			countryCode = AeroMartPayConstants.ALL_COUNTRIES;

		}

		// Get Country payment gateway card type behavior
		Collection<CountryPaymentCardBehaviourDTO> cardsInfo = ModuleServiceLocator.getPaymentBrokerBD()
				.getContryPaymentCardBehavior(countryCode, AeroMartPayUtil.getPaymentGatewayIDs(paymentGatewayList));
		return cardsInfo;
	}
	
	private static Collection<CountryPaymentCardBehaviourDTO> getCreditCardsListForAgent(List paymentGatewayList)
			throws ModuleException {
		
		Collection<CountryPaymentCardBehaviourDTO> cardsInfo = ModuleServiceLocator.getPaymentBrokerBD()
				.getCreditCardsListForAgent(AeroMartPayUtil.getPaymentGatewayIDs(paymentGatewayList));
		return cardsInfo;
	}

	private static void filterSelectedCurrencyPGs(String selectedCurrency, List paymentGatewayList) {
		List<IPGPaymentOptionDTO> selCurrencyPGList = new ArrayList<IPGPaymentOptionDTO>();

		for (IPGPaymentOptionDTO ipgPGOptDTO : (Collection<IPGPaymentOptionDTO>) paymentGatewayList) {
			if (ipgPGOptDTO.getBaseCurrency().equals(selectedCurrency)) {
				selCurrencyPGList.add(ipgPGOptDTO);
			}
		}
		// only consider PGs that support specified currency
		paymentGatewayList.clear();
		paymentGatewayList.addAll(selCurrencyPGList);

	}

	private static List getPaymentGatewayList(SearchPayOptionDTO searchPayOptionTO) throws ModuleException {
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();

		ipgPaymentOptionDTO.setSelCurrency(searchPayOptionTO.getSelectedCurrency());

		ipgPaymentOptionDTO.setEntityCode(searchPayOptionTO.getEntityCode());
		// module code hard-coded
		ipgPaymentOptionDTO.setModuleCode(getModuleCode(searchPayOptionTO.getAgentType()));

		ipgPaymentOptionDTO.setOfflinePayment(searchPayOptionTO.isOfflineOptionsAllowed());

		Map paymentMap = ModuleServiceLocator.getPaymentBrokerBD().getPaymentOptions(ipgPaymentOptionDTO);

		List paymentGatewayList = (List) paymentMap.get(AeroMartPayConstants.PAYMENT_GATEWAY);
		return paymentGatewayList;
	}

	public static boolean sessionHandlePaymentLock(HttpSession session, boolean isLock) {

		synchronized (session) {
			if (isLock ^ (boolean) session.getAttribute(AeroMartPayConstants.PAYMENT_OPTIONS_SESSION_LOCK)) {
				session.setAttribute(AeroMartPayConstants.PAYMENT_OPTIONS_SESSION_LOCK, isLock);
				return true;
			}

			return false;

		}

	}

	public static boolean incrementRetrails(HttpSession session) {

		synchronized (session) {

			int retrails = (int) session.getAttribute(AeroMartPayConstants.RETRAILS);
			++retrails;
			if (AppSysParamsUtil.getPaymentAttemptLimit() < retrails) {
				return false;
			}
			session.setAttribute(AeroMartPayConstants.RETRAILS, retrails);
			return true;
		}
	}

	public static void invalidateSession(HttpServletRequest request) {

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();

		}
	}

	public static BigDecimal getBaseCurrencyValue(String paymentCurrency, BigDecimal paymentCurrencyAmount)
			throws ModuleException {
		BigDecimal baseCurrencyAmount = null;
		if (AppSysParamsUtil.getBaseCurrency().contentEquals(paymentCurrency)) {
			baseCurrencyAmount = paymentCurrencyAmount;
		} else {
			// appIndictor set to IBE
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currencyExrate = exchangeRateProxy.getCurrencyExchangeRate(paymentCurrency,
					ApplicationEngine.IBE);

			baseCurrencyAmount = convertToBaseCurrency(paymentCurrencyAmount, currencyExrate);

		}
		return baseCurrencyAmount;

	}

	private static BigDecimal convertToBaseCurrency(BigDecimal paymentCurrencyAmount, CurrencyExchangeRate currencyExrate) {
		Currency paymentCurrency = currencyExrate.getCurrency();
		return AccelAeroRounderPolicy.convertAndRound(paymentCurrencyAmount,
				AccelAeroCalculator.divide(BigDecimal.valueOf(1), currencyExrate.getMultiplyingExchangeRate()),
				paymentCurrency.getBoundryValue(), paymentCurrency.getBreakPoint());
	}

	public static Merchant getMerchant(String merchantID) {
		Merchant merchant = null;
		try {
			merchant = CommonsServices.getGlobalConfig().getMerchantByID(merchantID);
		} catch (Exception e) {
			log.error("Errorwhile retrieveing merchant, merchantID: " + merchantID, e);
		}
		return merchant;
	}

	public static String encryptAeroMartPassword(String password) throws NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, ShortBufferException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException {
		byte[] keyBytes = AeroMartPayConstants.PASSWORD_KEY.getBytes();
		byte[] ivBytes = AeroMartPayConstants.PASSWORD_IV.getBytes();

		SecretKeySpec key = new SecretKeySpec(keyBytes, "DES");
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);

		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
		byte[] encrypted = cipher.doFinal(password.getBytes());

		return Base64.getEncoder().encodeToString(encrypted);

	}

	public static String decryptAeroMartPassword(String encryptedPassword) {

		byte[] keyBytes = AeroMartPayConstants.PASSWORD_KEY.getBytes();
		byte[] ivBytes = AeroMartPayConstants.PASSWORD_IV.getBytes();

		String decryptedPassword = "";
		SecretKeySpec key = new SecretKeySpec(keyBytes, "DES");
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);

		try {

			Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
			byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(encryptedPassword.getBytes()));
			decryptedPassword = new String(decrypted);

		} catch (Exception e) {
			log.error("Error while aeroMartPassword Decryption", e);

		}

		return decryptedPassword;
	}
	
	private static String getModuleCode(String agentType){
		if (AeroMartPayConstants.PUBLIC.equals(agentType)){
			return AeroMartPayConstants.MODULE_CODE_IBE;
		} else if (AeroMartPayConstants.AGENT.equals(agentType)){
			return AeroMartPayConstants.MODULE_CODE_XBE;
		}else{
			return AeroMartPayConstants.MODULE_CODE_IBE;
		}
	}
}
