package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.converters.AncillaryConverterUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo;
import com.isa.thinair.webplatform.api.v2.reservation.CalendarFlightAvailInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author M.Rikaz
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class MultiCitySearchAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(MultiCitySearchAction.class);

	private boolean success = true;

	private String messageTxt;

	private String searchSystem;

	private boolean groupPNR;

	private String airportMessage;

	private String calDisplayCurrency;

	private String requestSessionIdentifier;

	private boolean addGroundSegment;

	private boolean allowOnhold;

	private List<Collection<AvailableFlightInfo>> ondSeqFlightsList;

	private boolean currencyRound = false;

	private boolean addSegment = false;

	private boolean resIsMcSelted;

	private boolean onPreviousClick;

	private String classOfServiceDesc;

	private List<String> actLogCabClassList;

	private List<List<String>> calendarDayList;

	private List<List<CalendarFlightAvailInfo>> calendarDayFlightAvailList;

	private List<String> ondWiseAvailLogCOSInfoList;
	
	private boolean captchaValidationRequired;

	@SuppressWarnings("unchecked")
	public String execute() {

		String result = StrutsConstants.Result.SUCCESS;
		try {
			
			captchaValidationRequired = ReservationBeanUtil.isCaptchaValidationRequired(request);
			if(captchaValidationRequired) {
				return result;
			}

			String language = SessionUtil.getLanguage(request);

			request.getSession().setAttribute(ReservationWebConstnts.IS_MULTYCITY_SELECTED, isResIsMcSelted());

			FlightAvailRS flightAvailRS = new FlightAvailRS();

			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createMultiCityAvailSearchRQ(getSearchParams(), AppIndicatorEnum.APP_IBE);
			
			// Max Adult child Count
			if (AppSysParamsUtil.getMaxAdultCountIBE() < getSearchParams().getAdultCount()) {
				throw new ModuleException("module.invalid.adultchild.count");
			}
			
			if (groupPNR) {
				flightAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}

			// cannot search bus segment alone,
			if (AppSysParamsUtil.isGroundServiceEnabled()
					&& AddModifySurfaceSegmentValidations.isGroundSegmentOnlyExist(getSearchParams())) {
				success = false;
				messageTxt = I18NUtil.getMessage("errors.resv.flt.bus.only");
				return StrutsConstants.Result.SUCCESS;
			}
			flightAvailRQ.getAvailPreferences().setQuoteFares(false);
			flightAvailRQ.getAvailPreferences().setMultiCitySearch(true);
			flightAvailRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.IBE);

			AnalyticsLogger.logAvlSearch(AnalyticSource.IBE_MCS_AVL, flightAvailRQ, request, getTrackInfo());

			flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
					getTrackInfo(), false);

			actLogCabClassList = SelectListGenerator.getActiveLogicalCabinClassList();

			// build IBE calendar date list
			calendarDayList = ReservationBeanUtil
					.buildIBECalendarDays(flightAvailRQ.getOrderedOriginDestinationInformationList());

			// check flight availability for calendar view
			calendarDayFlightAvailList = ReservationBeanUtil.updateCalendarFlightAvailability(
					flightAvailRS.getOrderedOriginDestinationInformationList(),
					calendarDayList);
			ReservationBeanUtil.filterSelDateFlights(flightAvailRS);
			
			List<List<String>> ondWiseAvailableCOSList = new ArrayList<List<String>>();
			ondSeqFlightsList = ReservationBeanUtil.createAvailFlightInfoListMultiCity(flightAvailRS, getSearchParams(), false,
					false, false, null, actLogCabClassList, ondWiseAvailableCOSList);

			ondWiseAvailLogCOSInfoList = ReservationBeanUtil.generateCOSAvailabilityStringList(ondWiseAvailableCOSList);

			IBEReservationInfoDTO reservationInfo = SessionUtil.getIBEreservationInfo(request);
			if (reservationInfo == null) {
				reservationInfo = new IBEReservationInfoDTO();
				reservationInfo.setSelectedSystem(SYSTEM.getEnum(getSearchParams().getSearchSystem()));
				SessionUtil.setIBEreservationInfo(request, reservationInfo);
			}

			reservationInfo.setBaggageSummaryTo(AncillaryConverterUtil.toLccReservationBaggageSummaryTo(flightAvailRS));

			// Request Session Identifier
			requestSessionIdentifier = (new Date().getTime()) + "";
			SessionUtil.setRequestSessionIdentifier(request, requestSessionIdentifier);

			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightAvailRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.IBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, language);

		} catch (ModuleException me) {
			log.error(me.getModuleCode(), me);
			success = false;
			// FIXME All Error Codes propergate
			if (!me.getExceptionCode().isEmpty()) {
				messageTxt = I18NUtil.getMessage(me.getExceptionCode());
			} else {
				messageTxt = me.getMessage();
			}
		} catch (Exception ex) {
			log.error("MultiCitySearchAction ==> ", ex);
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			SessionUtil.resetSesionDataInError(request);
		}
		return result;
	}

	public List<List<String>> getCalendarDayList() {
		return calendarDayList;
	}

	public void setCalendarDayList(List<List<String>> calendarDayList) {
		this.calendarDayList = calendarDayList;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getSearchSystem() {
		return searchSystem;
	}

	@Override
	public FlightSearchDTO getSearchParams() {
		return super.getSearchParams();
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getCalDisplayCurrency() {
		return calDisplayCurrency;
	}

	public String getRequestSessionIdentifier() {
		return requestSessionIdentifier;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	public List<Collection<AvailableFlightInfo>> getOndSeqFlightsList() {
		return ondSeqFlightsList;
	}

	public void setOndSeqFlightsList(List<Collection<AvailableFlightInfo>> ondSeqFlightsList) {
		this.ondSeqFlightsList = ondSeqFlightsList;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public void setCurrencyRound(boolean currencyRound) {
		this.currencyRound = currencyRound;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}

	public List<String> getActLogCabClassList() {
		return actLogCabClassList;
	}

	public void setActLogCabClassList(List<String> actLogCabClassList) {
		this.actLogCabClassList = actLogCabClassList;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public boolean isResIsMcSelted() {
		return resIsMcSelted;
	}

	public void setResIsMcSelted(boolean resIsMcSelted) {
		this.resIsMcSelted = resIsMcSelted;
	}

	public boolean isOnPreviousClick() {
		return onPreviousClick;
	}

	public void setOnPreviousClick(boolean onPreviousClick) {
		this.onPreviousClick = onPreviousClick;
	}

	public List<String> getOndWiseAvailLogCOSInfoList() {
		return ondWiseAvailLogCOSInfoList;
	}

	public boolean isCaptchaValidationRequired() {
		return captchaValidationRequired;
	}

	public void setCaptchaValidationRequired(boolean captchaValidationRequired) {
		this.captchaValidationRequired = captchaValidationRequired;
	}

	public List<List<CalendarFlightAvailInfo>> getCalendarDayFlightAvailList() {
		return calendarDayFlightAvailList;
	}

	public void setCalendarDayFlightAvailList(List<List<CalendarFlightAvailInfo>> calendarDayFlightAvailList) {
		this.calendarDayFlightAvailList = calendarDayFlightAvailList;
	}

}
