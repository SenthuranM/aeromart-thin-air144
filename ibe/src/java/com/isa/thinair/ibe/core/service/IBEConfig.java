package com.isa.thinair.ibe.core.service;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @isa.module.config-bean
 * @author sumudu
 * 
 */
public class IBEConfig extends DefaultModuleConfig {
	private String secureIBEUrl = null;

	private String nonsecureIBEUrl = null;

	private boolean restrictIps;

	private String ipRestictionFile;

	private long ipRestictionRefreshTime;

	private boolean denyURIs;

	private String denyURIFile;

	private boolean displayAdvancedScheduleDetails;

	private int cookieExpiryDays;
	
	private boolean skipSegmentCodeInLabelsForOtherLanguages;

	private boolean displayCreditCardProofLable;

	/**
	 * @return the secureIBEUrl
	 */
	public String getSecureIBEUrl() {
		return this.secureIBEUrl;
	}

	/**
	 * @param secureIBEUrl
	 *            the secureIBEUrl to set
	 */
	public void setSecureIBEUrl(String secureIBEUrl) {
		this.secureIBEUrl = secureIBEUrl;
	}

	/**
	 * @return the nonsecureIBEUrl
	 */
	public String getNonsecureIBEUrl() {
		return this.nonsecureIBEUrl;
	}

	/**
	 * @param nonsecureIBEUrl
	 *            the nonsecureIBEUrl to set
	 */
	public void setNonsecureIBEUrl(String nonsecureIBEUrl) {
		this.nonsecureIBEUrl = nonsecureIBEUrl;
	}

	public boolean isRestrictIps() {
		return restrictIps;
	}

	public void setRestrictIps(boolean restrictIps) {
		this.restrictIps = restrictIps;
	}

	public String getIpRestictionFile() {
		return ipRestictionFile;
	}

	public void setIpRestictionFile(String ipRestictionFile) {
		this.ipRestictionFile = ipRestictionFile;
	}

	public long getIpRestictionRefreshTime() {
		return ipRestictionRefreshTime;
	}

	public void setIpRestictionRefreshTime(long ipRestictionRefreshTime) {
		this.ipRestictionRefreshTime = ipRestictionRefreshTime;
	}

	public boolean isDenyURIs() {
		return denyURIs;
	}

	public void setDenyURIs(boolean denyURIs) {
		this.denyURIs = denyURIs;
	}

	public String getDenyURIFile() {
		return denyURIFile;
	}

	public void setDenyURIFile(String denyURIFile) {
		this.denyURIFile = denyURIFile;
	}

	/**
	 * @return the displayAdvancedScheduleDetails
	 */
	public boolean isDisplayAdvancedScheduleDetails() {
		return displayAdvancedScheduleDetails;
	}

	/**
	 * @param displayAdvancedScheduleDetails
	 *            the displayAdvancedScheduleDetails to set
	 */
	public void setDisplayAdvancedScheduleDetails(boolean displayAdvancedScheduleDetails) {
		this.displayAdvancedScheduleDetails = displayAdvancedScheduleDetails;
	}

	/**
	 * @return the cookieExpiryDays
	 */
	public int getCookieExpiryDays() {
		return cookieExpiryDays;
	}

	/**
	 * @param cookieExpiryDays
	 *            the cookieExpiryDays to set
	 */
	public void setCookieExpiryDays(int cookieExpiryDays) {
		this.cookieExpiryDays = cookieExpiryDays;
	}

	public boolean isSkipSegmentCodeInLabelsForOtherLanguages() {
		return skipSegmentCodeInLabelsForOtherLanguages;
	}

	public void setSkipSegmentCodeInLabelsForOtherLanguages(boolean skipSegmentCodeInLabelsForOtherLanguages) {
		this.skipSegmentCodeInLabelsForOtherLanguages = skipSegmentCodeInLabelsForOtherLanguages;
	}

	public boolean isDisplayCreditCardProofLable() {
		return displayCreditCardProofLable;
	}

	public void setDisplayCreditCardProofLable(boolean displayCreditCardProofLable) {
		this.displayCreditCardProofLable = displayCreditCardProofLable;
	}
}
