package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.CustomerCreditDTO;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.PaxValidationTO;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED) })
public class IBEContainerLocaleAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(IBEContainerLocaleAction.class);

	private boolean success = true;

	private String messageTxt;

	private SessionTimeoutDataDTO timeoutDTO;

	private String systemDate;

	private Map<String, String> errorInfo;

	private HashMap<String, String> jsonLabel;

	private HashMap<String, String> specialMessages;

	private CustomerDTO customer;

	private LmsMemberDTO lmsDetails;

	private String totalCustomerCredit;

	private String baseCurr;

	private PaxValidationTO paxValidation;

	private String carrier;

	private String carrierName;

	private String airportMessage;

	private boolean sessionTimeOutDisplay;

	private boolean isKiosk = false;

	private boolean focMealEnabled = false;

	private boolean allowInfantInsurance = false;

	private boolean loyaltyManagmentEnabled = false;
	
	private boolean bundleFarePopupEnabled = false;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			if (resInfo.isFromPostCardDetails()) { // clicked the back button
				try {
					SeatMapUtil.releaseBlockedSeats(request);
				} catch (Exception e1) {
					log.warn("seat releasing failed", e1);
				}
				// request.getSession().invalidate();
				// return StrutsConstants.Result.SESSION_EXPIRED;
			}
			Customer customerModel = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));

			if (customerModel != null) {
				int intCustomerID = customerModel.getCustomerId();
				customer = CustomerUtilV2.getCustomerNameDetail(customerModel);
				LmsMember lmsMember = null;
				if (customerModel.getCustomerLmsDetails().size() > 0) {
					lmsMember = customerModel.getCustomerLmsDetails().iterator().next();
				}
				if (lmsMember != null) {
					lmsDetails = CustomerUtilV2.getLmsMemberDTO(lmsMember);
				}
				/*
				 * totalCustomerCredit = CustomerUtilV2.getCustomerTotalCredit(ModuleServiceLocator
				 * .getReservationQueryBD().getReservationCredits(intCustomerID));
				 */

				Collection<CustomerCreditDTO> customerCreditDTO = CustomerUtilV2.getCustomerCreditFromAllCarriers(intCustomerID,
						getTrackInfo());
				totalCustomerCredit = CustomerUtilV2.calculateCustomerTotalCredit(customerCreditDTO);

				CustomerUtilV2.setLoyaltyAccountStatus(customer, request);
			}
			baseCurr = AppSysParamsUtil.getBaseCurrency();
			sessionTimeOutDisplay = AppSysParamsUtil.getSessionTimeOutDisplay();

			String pageIds[] = { "Common", "Form", "PgPassenger", "PgInterlineAncillary", "PgPayment", "PgUserTab" };
			String language = SessionUtil.getLanguage(request);
			jsonLabel = I18NUtil.getMessagesInJSON(language, pageIds);
			errorInfo = ErrorMessageUtil.getContainerErrors(request);
			timeoutDTO = ReservationUtil.fetchSessionTimeoutData(request);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			systemDate = sdf.format(Calendar.getInstance().getTime());
			populateSpecialMessages(language);
			addLoyatlyMessagesToLabels(language);
			this.paxValidation = PaxUtil.fillPaxValidation();
			carrier = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			carrierName = AppSysParamsUtil.getDefaultCarrierName();

			String accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);
			if (CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint)) {
				isKiosk = true;
			}
			focMealEnabled = AppSysParamsUtil.isFOCMealSelectionEnabled();
			allowInfantInsurance = AppSysParamsUtil.allowAddInsurnaceForInfants();
			bundleFarePopupEnabled = AppSysParamsUtil.isBundleFarePopupEnabled(ApplicationEngine.IBE);

			if (AppSysParamsUtil.isLMSEnabled()) {
				loyaltyManagmentEnabled = true;
			}

		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("Container LOCALE loading error", e);
			SessionUtil.resetSesionDataInError(request);

		}
		return result;
	}

	private void addLoyatlyMessagesToLabels(String language) {
		Properties propertyMap = new Properties();
		propertyMap.put("msg.res.pay.msq.credit.sel", "lblLoyaltyCredit_1");
		propertyMap.put("msg.res.pay.msq.credit.sel1", "lblLoyaltyCredit_2");
		propertyMap.put("msg.res.pay.msq.credit.opt1", "lblLoyaltyUseAll");
		propertyMap.put("msg.res.pay.msq.credit.opt2", "lblLoyaltyUseAmt_1");
		propertyMap.put("msg.res.pay.msq.credit.opt3", "lblLoyaltyUseAmt_2");
		propertyMap.put("msg.res.pay.msq.credit.opt4", "lblLoyaltyNo");

		Set<Object> propSet = propertyMap.keySet();
		for (Object key : propSet) {
			jsonLabel.put(propertyMap.getProperty(key.toString()), I18NUtil.getMessage(key.toString(), language));
		}
	}

	private void populateSpecialMessages(String language) {
		GlobalConfig globalConfig = new GlobalConfig();
		Map<String, ArrayList<String>> mapSpecialMsg = globalConfig.getAirportMessages();

		List<String> msgs = mapSpecialMsg.get(getFromAirport());
		if (msgs == null || msgs.size() == 0) {
			msgs = mapSpecialMsg.get(getToAirport());
		}
		specialMessages = new HashMap<String, String>();

		Properties propertyMap = new Properties();
		propertyMap.put("msg.fltSearch.spMsg", "okToBoardMsg");
		propertyMap.put("msg.fltSearch.spMsgCC", "creditCardMsg");
		if (msgs != null) {
			for (String key : msgs) {
				propertyMap.contains(key);
				specialMessages.put(propertyMap.getProperty(key), I18NUtil.getMessage(key, language));
			}
		}
	}

	private String getFromAirport() {
		String fromAirport = getSearchParams().getFromAirport();
		if (RequestParameterUtil.isPaymentRetry(request)) {
			IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
			// TODO Get airport code correct way
			if (postPayDTO != null) {
				List<FlightSegmentTO> flightSegmentTOs = postPayDTO.getSelectedFlightSegments();
				if (flightSegmentTOs != null && flightSegmentTOs.size() != 0) {
					for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
						if (!flightSegmentTO.isReturnFlag() && !flightSegmentTO.isSurfaceSegment()) {
							fromAirport = flightSegmentTO.getSegmentCode().split("/")[0];
							break;
						}
					}
				}
			}
		}
		return fromAirport;
	}

	private String getToAirport() {
		String toAirport = getSearchParams().getToAirport();
		if (RequestParameterUtil.isPaymentRetry(request)) {
			IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
			// TODO Get airport code correct way
			if (postPayDTO != null) {
				List<FlightSegmentTO> flightSegmentTOs = postPayDTO.getSelectedFlightSegments();
				if (flightSegmentTOs != null && flightSegmentTOs.size() != 0) {
					for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
						if (!flightSegmentTO.isReturnFlag() && !flightSegmentTO.isSurfaceSegment()) {
							String[] segmentDetails = flightSegmentTO.getSegmentCode().split("/");
							toAirport = segmentDetails[segmentDetails.length - 1];
						}
					}
				}
			}
		}
		return toAirport;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public void setJsonLabel(HashMap<String, String> jsonLabel) {
		this.jsonLabel = jsonLabel;
	}

	public SessionTimeoutDataDTO getTimeoutDTO() {
		return timeoutDTO;
	}

	public void setTimeoutDTO(SessionTimeoutDataDTO timeoutDTO) {
		this.timeoutDTO = timeoutDTO;
	}

	public String getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public String getTotalCustomerCredit() {
		return totalCustomerCredit;
	}

	public void setTotalCustomerCredit(String totalCustomerCredit) {
		this.totalCustomerCredit = totalCustomerCredit;
	}

	public String getBaseCurr() {
		return baseCurr;
	}

	public void setBaseCurr(String baseCurr) {
		this.baseCurr = baseCurr;
	}

	public HashMap<String, String> getSpecialMessages() {
		return specialMessages;
	}

	public void setSpecialMessages(HashMap<String, String> specialMessages) {
		this.specialMessages = specialMessages;
	}

	public PaxValidationTO getPaxValidation() {
		return paxValidation;
	}

	public void setPaxValidation(PaxValidationTO paxValidation) {
		this.paxValidation = paxValidation;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public boolean isKiosk() {
		return isKiosk;
	}

	public void setKiosk(boolean isKiosk) {
		this.isKiosk = isKiosk;
	}

	/**
	 * @return the sessionTimeOutDisplay
	 */
	public boolean isSessionTimeOutDisplay() {
		return sessionTimeOutDisplay;
	}

	/**
	 * @param sessionTimeOutDisplay
	 *            the sessionTimeOutDisplay to set
	 */
	public void setSessionTimeOutDisplay(boolean sessionTimeOutDisplay) {
		this.sessionTimeOutDisplay = sessionTimeOutDisplay;
	}

	public boolean isFocMealEnabled() {
		return focMealEnabled;
	}

	public void setFocMealEnabled(boolean focMealEnabled) {
		this.focMealEnabled = focMealEnabled;
	}

	public boolean isAllowInfantInsurance() {
		return allowInfantInsurance;
	}

	public void setAllowInfantInsurance(boolean allowInfantInsurance) {
		this.allowInfantInsurance = allowInfantInsurance;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(Map<String, String> errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean isBundleFarePopupEnabled() {
		return bundleFarePopupEnabled;
	}

	public void setBundleFarePopupEnabled(boolean bundleFarePopupEnabled) {
		this.bundleFarePopupEnabled = bundleFarePopupEnabled;
	}
	
	

}
