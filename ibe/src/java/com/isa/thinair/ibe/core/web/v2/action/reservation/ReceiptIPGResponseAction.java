package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.listener.AAHttpSessionListener;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE, value = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Result.CARD_PAYMENT_ERROR, value = StrutsConstants.Jsp.Payment.CARD_PAYMENT_ERROR) })
public class ReceiptIPGResponseAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ReceiptIPGResponseAction.class);
	private IBECommonDTO commonParams = new IBECommonDTO();
	private String sessionId;

	public String execute() {
		String forward = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE;
		String language = SessionUtil.getLanguage(request);
		boolean isPaymentSuccess = false;

		try {
			if (log.isDebugEnabled()) {
				log.debug("####### Session ID as recevied : " + request.getSession().getId());
			}
			AAHttpSessionListener.handleCustomerReceipt(this.sessionId, request.getSession().getId());
			
			if (log.isDebugEnabled()) {
				log.debug("####### Previous Session Attributess were copied to current Session");
			}

			if (SessionUtil.isSessionExpired(request)) {
				forward = StrutsConstants.Result.SESSION_EXPIRED;
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, CommonUtil.getRefundMessage(language));
			} else {
				IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);

				if (postPayDTO == null) {
					SessionUtil.expireSession(request);
					forward = StrutsConstants.Result.SESSION_EXPIRED;
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
							CommonUtil.getRefundMessage(SessionUtil.getLanguage(request)));
				} else {
					if (postPayDTO.getIpgResponseDTO() != null) {
						isPaymentSuccess = postPayDTO.getIpgResponseDTO().isSuccess();
					}

					if (postPayDTO.getIpgResponseDTO() != null && !postPayDTO.getIpgResponseDTO().isSuccess()) {
						String paymentErrorCode = postPayDTO.getIpgResponseDTO().getErrorCode();
						if (paymentErrorCode != null && paymentErrorCode.trim().length() > 0) {
							CustomerUtil.revertLoyaltyRedemption(request);
							ModuleException me = getStandardPaymentBrokerError(paymentErrorCode);
							throw me;
						} else {
							if (log.isInfoEnabled())
								log.info("Possible Intruder IP Detected : " + getClientInfoDTO().getIpAddress());
							throw new ModuleException(ExceptionConstants.ERR_PAYMENT_SESSION_EXPIRED);
						}
					} else {
						if (SessionUtil.getIbeReservationPostDTO(request) != null
								&& SessionUtil.getIbeReservationPostDTO(request).getPnr() != null) {
							forward = StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE;
						} else {
							forward = StrutsConstants.Result.ERROR;
							request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
									CommonUtil.getRefundMessage(SessionUtil.getLanguage(request)));
						}
					}
				}
			}
		} catch (ModuleException me) {
			if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
				forward = StrutsConstants.Result.CARD_PAYMENT_ERROR;
			} else {
				forward = StrutsConstants.Result.ERROR;
			}
			if (me.getExceptionCode().equals(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR)) {
				String strErrMsg = "";
				String errorCode = "";
				String errorMsg = "";

				try {
					Map<String, String> errorMap = (Map) me.getExceptionDetails();
					Iterator<String> itErrorMap = errorMap.keySet().iterator();

					if (itErrorMap.hasNext()) {
						errorCode = (String) itErrorMap.next();
						errorMsg = (String) errorMap.get(errorCode);
					}

					strErrMsg = getServerErrorMessage(request, ReservationWebConstnts.REQ_PAYMENT_GATEWAY_ERROR + errorCode);

					if (strErrMsg == null || strErrMsg.equals("")) {
						strErrMsg = getServerErrorMessage(request, "pay.gateway.error.less.0");
					}

				} catch (Exception ex) {
					log.error("ReceiptIPGResponseAction", ex);
					if (!errorMsg.equals("")) {
						strErrMsg = errorMsg;
					} else {
						strErrMsg = getServerErrorMessage(request, "pay.gateway.error.less.0");
					}
				}
				if (isPaymentSuccess) {
					strErrMsg = strErrMsg + "<br/>" + CommonUtil.getRefundMessage(language);
				}

				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, strErrMsg + "<br>");
			} else {
				String msg;

				if (isPaymentSuccess) {
					msg = CommonUtil.getRefundMessage(language);
				} else {
					msg = getServerErrorMessage(request, me.getExceptionCode());
				}
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
			}
		} catch (RuntimeException re) {
			log.error("ReceiptIPGResponseAction", re);
			if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
				forward = StrutsConstants.Result.CARD_PAYMENT_ERROR;
			} else {
				forward = StrutsConstants.Result.ERROR;
			}
			String msg;
			if (isPaymentSuccess) {
				msg = CommonUtil.getRefundMessage(language);
			} else {
				msg = getServerErrorMessage(request, "server.default.operation.fail");
			}
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
		} catch (Exception ex) {
			log.error("ReceiptIPGResponseAction", ex);
			if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
				forward = StrutsConstants.Result.CARD_PAYMENT_ERROR;
			} else {
				forward = StrutsConstants.Result.ERROR;
			}
			String msg;
			if (isPaymentSuccess) {
				msg = CommonUtil.getRefundMessage(language);
			} else {
				msg = ex.getMessage();
			}
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
		} finally {
			if (!forward.equals(StrutsConstants.Jsp.ModifyReservation.LOAD_REDIRECTPAGE)) {
				try {
					SeatMapUtil.releaseBlockedSeats(request);
				} catch (Exception e1) {
					log.error("SEAT release failed", e1);
				}
			}

			try {
				SystemUtil.setCommonParameters(request, commonParams);
				SessionUtil.setIBEreservationInfo(request, null);
				SessionUtil.setIBEReservationPostPaymentDTO(request, null);
				SessionUtil.setPutOnHoldBeforePaymentDTO(request, null);
				SessionUtil.resetSesionDataInError(request);
				SessionUtil.resetSesionCardDataInError(request);
			} catch (Exception e) {
				log.error("ERROR", e);
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("### InterLineConfirmationAction end... ###");
		}

		return forward;

	}

	private static ModuleException getStandardPaymentBrokerError(String paymentErrorCode) {

		ModuleException me = new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR);
		Map<String, String> errorMap = new HashMap<String, String>();

		errorMap.put(paymentErrorCode, "External payment broker error:" + paymentErrorCode);
		me.setExceptionDetails(errorMap);

		return me;
	}

	private static String getServerErrorMessage(HttpServletRequest request, String key) {
		return I18NUtil.getMessage(key, SessionUtil.getLanguage(request));
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public IBECommonDTO getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

}