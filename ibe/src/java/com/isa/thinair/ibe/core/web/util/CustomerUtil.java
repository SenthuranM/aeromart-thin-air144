package com.isa.thinair.ibe.core.web.util;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

public class CustomerUtil {

	private static Log log = LogFactory.getLog(CustomerUtil.class);

	/**
	 * returns a customer
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	public static Customer getCustomer(int customerId) throws ModuleException {
		Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(customerId);

		return customer;
	}

	public static BigDecimal getLMSPaymentAmount(IBEReservationInfoDTO resInfo) {
		BigDecimal lmsPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (resInfo.getCarrierWiseLoyaltyPaymentInfo() != null) {
			for (LoyaltyPaymentInfo carrierLoyaltyPaymentInfo : resInfo.getCarrierWiseLoyaltyPaymentInfo().values()) {
				if (carrierLoyaltyPaymentInfo != null
						&& AccelAeroCalculator.isGreaterThan(carrierLoyaltyPaymentInfo.getTotalPayment(), BigDecimal.ZERO)) {
					lmsPaymentAmount = AccelAeroCalculator.add(lmsPaymentAmount, carrierLoyaltyPaymentInfo.getTotalPayment());
				}
			}
		}
		return lmsPaymentAmount;
	}

	public static BigDecimal getVoucherPaymentAmount(IBEReservationInfoDTO resInfo) {
		BigDecimal voucherPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (resInfo.getPayByVoucherInfo() != null
				&& AccelAeroCalculator.isGreaterThan(resInfo.getPayByVoucherInfo().getRedeemedTotal(), BigDecimal.ZERO)) {
			voucherPaymentAmount = resInfo.getPayByVoucherInfo().getRedeemedTotal();
		}
		return voucherPaymentAmount;
	}

	public static void revertLoyaltyRedemption(HttpServletRequest request) throws ModuleException {
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		String memberAccountId = SessionUtil.getLoyaltyFFID(request);

		if (resInfo != null && memberAccountId != null && !memberAccountId.isEmpty()
				&& resInfo.getCarrierWiseLoyaltyPaymentInfo() != null) {
			for (LoyaltyPaymentInfo carrierLoyaltyPaymentInfo : resInfo.getCarrierWiseLoyaltyPaymentInfo().values()) {
				if (carrierLoyaltyPaymentInfo != null && carrierLoyaltyPaymentInfo.getLoyaltyRewardIds() != null
						&& carrierLoyaltyPaymentInfo.getLoyaltyRewardIds().length > 0) {
					ModuleServiceLocator.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(resInfo.getPnr(),
							carrierLoyaltyPaymentInfo.getLoyaltyRewardIds(), memberAccountId);
				}
			}
		}

	}

	public static Double getMemberAvailablePoints(HttpServletRequest request) throws ModuleException {
		Double availablePoints = null;
		String memberAccountId = SessionUtil.getLoyaltyFFID(request);
		if (memberAccountId != null && !"".equals(memberAccountId)) {
			LoyaltyPointDetailsDTO loyaltyPointDetailsDTO = ModuleServiceLocator.getLoyaltyManagementBD()
					.getLoyaltyMemberPointBalances(memberAccountId, null);

			if (loyaltyPointDetailsDTO != null) {
				availablePoints = loyaltyPointDetailsDTO.getMemberPointsAvailable();
			}
		}

		return availablePoints;
	}
	
	public static List<SeatType> getPreferenceSeatTypes(){
		return ModuleServiceLocator.getCustomerBD().getPreferenceSeatTypes();
	}

	public static String getCustomerPreferredSeatType(int customerId){
		return ModuleServiceLocator.getCustomerBD().getCustomerPreferredSeatType(customerId);
	}
	
	public static List<CustomerPreferredMealDTO> getCustomerPreferredMeals(int customerId){
		return ModuleServiceLocator.getCustomerBD().getCustomerPreferredMeals(customerId);
	}
	
}
