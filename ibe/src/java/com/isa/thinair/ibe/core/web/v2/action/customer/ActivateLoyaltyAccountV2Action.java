package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.LoyaltyAccountDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ActivateLoyaltyAccountV2Action extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ActivateLoyaltyAccountV2Action.class);

	private boolean success = true;

	private String messageTxt;

	private LoyaltyAccountDTO account;

	public String execute() {

		String forward = StrutsConstants.Result.SUCCESS;

		try {
			SimpleDateFormat dateFormat = null;
			Date dateOfBirth = null;
			String dob = account.getDateOfBirth();
			if (!dob.equals("")) {

				if (dob.indexOf(' ') != -1) {
					dob = dob.substring(0, dob.indexOf(' '));
				}

				if (dob.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				}
				if (dob.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				}

				try {
					dateOfBirth = dateFormat.parse(dob);
				} catch (Exception ex) {
				}
			}
			LoyaltyCustomerServiceBD loyaltyCustomerBD = ModuleServiceLocator.getLoyaltyCustomerBD();
			LoyaltyCustomerProfile loyaltyProfile = null;
			Customer customer = null;

			if (account.getLoyaltyAccountNo() != null && !account.getLoyaltyAccountNo().isEmpty()) {
				LoyaltyCustomerProfile ex = new LoyaltyCustomerProfile();
				ex.setCity(account.getCity());
				ex.setCountryCode(account.getCountryCode());
				ex.setDateOfBirth(dateOfBirth);
				ex.setLoyaltyAccountNo(account.getLoyaltyAccountNo());
				ex.setMobile(account.getMobile());
				ex.setNationalityCode(account.getNationalityCode());

				try {
					customer = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
					ex.setEmail(customer.getEmailId());
					loyaltyProfile = loyaltyCustomerBD.getLoyaltyCustomerByExample(ex);
				} catch (ModuleException e) {
					log.error(e);
				}
				if (loyaltyProfile == null)
					messageTxt = "notExist";
				else {
					if (loyaltyProfile.getCustomerId() != null)
						messageTxt = "alreadyLinked";
					else {
						loyaltyProfile.setCustomerId(customer.getCustomerId());
						loyaltyProfile.setConfirmedDate(new Date());
						loyaltyCustomerBD.saveOrUpdate(loyaltyProfile);
						messageTxt = "success";
						SessionUtil.setLoyaltyAccountNo(request, loyaltyProfile.getLoyaltyAccountNo());
					}
				}

			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ActivateLoyaltyAccountV2Action==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ActivateLoyaltyAccountV2Action==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public LoyaltyAccountDTO getAccount() {
		return account;
	}

	public void setAccount(LoyaltyAccountDTO account) {
		this.account = account;
	}

}
