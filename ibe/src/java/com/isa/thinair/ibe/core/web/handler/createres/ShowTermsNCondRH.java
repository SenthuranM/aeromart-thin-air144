package com.isa.thinair.ibe.core.web.handler.createres;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.ibe.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.platform.api.constants.PlatformConstants;

public class ShowTermsNCondRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ShowTermsNCondRH.class);

	public static String execute(HttpServletRequest request) {
		String forward = "success";
		try {
			setTermsNCondFull(request);
		} catch (ModuleException me) {
			forward = "errors";
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, me.getMessageString());
			log.error("ShowTermsNCondRH.execute", me);
		} catch (RuntimeException re) {
			forward = "errors";
			String msg = getServerErrorMessage(request, "server.default.operation.fail");
			log.error("ShowTermsNCondRH.execute", re);
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
		}
		return forward;
	}

	private static void setTermsNCondFull(HttpServletRequest request) throws ModuleException {
		String templateName = "TermsFull_IBE";
		String language = "en";
		if (Config.get(request.getSession(), Config.FMT_LOCALE) != null) {
			String strLocalse = Config.get(request.getSession(), Config.FMT_LOCALE).toString();
			language = strLocalse.toLowerCase();
		}

		String tnc = ModuleServiceLocator.getReservationQueryBD().getTermsNConditions(templateName, new Locale(language));
		if (tnc.equals("")) {
			String strFileName = "TermsFull_IBE_en.xml";
			if (language != null) {
				strFileName = templateName + "_" + language + ".xml";
				String strXMLFilePath = PlatformConstants.getConfigRootAbsPath() + ReservationWebConstnts.XML_FILE_PATH
						+ strFileName;
				tnc = ReservationHG.createTermsNCondFromXML(strXMLFilePath);
			}
		}
		request.setAttribute(ReservationWebConstnts.REQ_HTML_TERMS_COND_FULL, tnc);

	}
}
