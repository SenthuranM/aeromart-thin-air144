package com.isa.thinair.ibe.core.web.v2.action.system;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.ibe.api.dto.AirlineRequestDTO;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.api.dto.IBEPromotionPaymentDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.SessionDataDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.RequestParameter;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.SessionAttribute;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.RequestUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.ExitPopupUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.promotion.api.to.PromotionRequestTo;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.opensymphony.xwork2.ActionChainResult;

/**
 * @author pkarunanayake
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Respro.MIDDEL_PAGE_LOADER),
		@Result(name = StrutsConstants.Jsp.Respro.MIDDEL_PAGE_LOADER, value = StrutsConstants.Jsp.Respro.MIDDEL_PAGE_LOADER),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE, value = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3, value = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3),
		@Result(name = StrutsConstants.Jsp.Customer.LOGIN, value = StrutsConstants.Jsp.Customer.LOGIN),
		@Result(name = StrutsConstants.Jsp.Customer.LOGIN_V3, value = StrutsConstants.Jsp.Customer.LOGIN_V3),
		@Result(name = StrutsConstants.Jsp.Kiosk.LOGIN, value = StrutsConstants.Jsp.Kiosk.LOGIN),
		@Result(name = StrutsConstants.Jsp.Kiosk.LOGIN_V3, value = StrutsConstants.Jsp.Kiosk.LOGIN_V3),
		@Result(name = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK, value = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK),
		@Result(name = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK_V3, value = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK_V3),
		@Result(name = StrutsConstants.Jsp.Respro.MANAGE_BOOKING, value = StrutsConstants.Jsp.Respro.MANAGE_BOOKING),
		@Result(name = StrutsConstants.Jsp.Respro.MANAGE_BOOKING_V3, value = StrutsConstants.Jsp.Respro.MANAGE_BOOKING_V3),
		@Result(name = StrutsConstants.Jsp.Customer.HOME, value = StrutsConstants.Jsp.Customer.HOME),
		@Result(name = StrutsConstants.Jsp.Customer.HOME_V3, value = StrutsConstants.Jsp.Customer.HOME_V3),
		@Result(name = StrutsConstants.Jsp.Customer.REGISTER, value = StrutsConstants.Jsp.Customer.REGISTER),
		@Result(name = StrutsConstants.Jsp.Customer.REGISTER_V3, value = StrutsConstants.Jsp.Customer.REGISTER_V3),
		@Result(name = StrutsConstants.Jsp.Customer.MOBILE_REGISTER_V3, value = StrutsConstants.Jsp.Customer.MOBILE_REGISTER_V3),
		@Result(name = StrutsConstants.Jsp.Agent.REGISTER, value = StrutsConstants.Jsp.Agent.REGISTER),
		@Result(name = StrutsConstants.Jsp.Agent.REGISTER_V3, value = StrutsConstants.Jsp.Agent.REGISTER_V3),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY, value = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY),
		@Result(name = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY_V3, value = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY_V3),
		@Result(name = StrutsConstants.Jsp.Common.REDIRECT, value = StrutsConstants.Jsp.Common.REDIRECT),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Jsp.Promotion.PROMOTION_REGISTRATION, value = StrutsConstants.Jsp.Promotion.PROMOTION_REGISTRATION),
		@Result(name = StrutsConstants.Action.SHOWCUSTOMERLOADPAGE, type = ActionChainResult.class, value = StrutsConstants.Action.SHOWCUSTOMERLOADPAGE),
		@Result(name = StrutsConstants.Action.SELF_REPROTECTION, type = ActionChainResult.class, value = StrutsConstants.Action.SELF_REPROTECTION),
		@Result(name = StrutsConstants.Jsp.Customer.SELF_REPROTECTION, value = StrutsConstants.Jsp.Customer.SELF_REPROTECTION),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Jsp.Voucher.GIFT_VOUCHER, value = StrutsConstants.Jsp.Voucher.GIFT_VOUCHER) })
public class ShowReservationAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowReservationAction.class);

	private String hdnParamData = "";

	private String hdnMode;

	private String promReqAttr = "";

	public String getHdnParamUrl() {
		return hdnParamUrl;
	}

	public void setHdnParamUrl(String hdnParamUrl) {
		this.hdnParamUrl = hdnParamUrl;
	}

	private String hdnParamUrl;

	private String hdnDesign;

	private String hdnTrackingID;
	
	private String marker;

	private IBECommonDTO commonParams;

	// variable used for testing marketing carrier
	// identification based on origin country
	private String hdnOriginCountry;

	private boolean fromPromoRegister = false;

	private String promoRequestData;

	private String totalPayment;

	private String promoPnr;

	private String promoLastName;

	private String promoDepDate;

	private String ibeExitDetailsId;

	private String ondListString;

	private boolean drySearch;

	private String paxCountStr;

	public String execute() {

		String forward = StrutsConstants.Result.ERROR;
		String fwd = null;
		String redirectHdnParamData = new String(hdnParamData);
		try {

			if (hdnParamData != null && !hdnParamData.equals("")) {
				request.getSession().setAttribute(ReservationWebConstnts.PARAM_HDNPARAMDATA, hdnParamData);
			}
			if ((hdnParamData == null || "".equals(hdnParamData))
					&& Boolean.parseBoolean(request.getParameter(ReservationWebConstnts.IS_ON_PREVIOUS_CLICK))) {
				hdnParamData = (String) request.getSession().getAttribute(ReservationWebConstnts.PARAM_HDNPARAMDATA);
			}

			// TODO Verify if Error Occur
			hdnParamData = URLDecoder.decode(hdnParamData, "UTF-8");
			// Get data form request
			if (hdnParamData == null || hdnParamData.isEmpty()) {
				hdnParamData = (String) request.getAttribute(ReservationWebConstnts.PARAM_HDNPARAMDATA);
			}
			
			// Yandex pixel markerID
			marker = (String) request.getAttribute(ReservationWebConstnts.EXTERNAL_MARKER_ID);
			if (validMarkerID(marker)) {
				request.getSession().setAttribute(ReservationWebConstnts.EXTERNAL_MARKER_ID, marker);
			} else {
				log.error("Invalid marker param : " + marker);
			}

			if (ibeExitDetailsId != null && !ibeExitDetailsId.isEmpty()) {
				try {
					int exitDetailsId = Integer.parseInt(ibeExitDetailsId);
					ExitPopupUtil.updateExitpopupTracking(exitDetailsId);
				} catch (NumberFormatException e) {
					log.error("Invalid ibeExitDetailsId parameter : " + ibeExitDetailsId);
				}
			}
			if (hdnParamData == null || "".equals(hdnParamData)) {
				String timestamp = CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date());
				log.error("ShowReservationAction requested is with empty request parameters :" + "|timestamp="
						+ timestamp + "|sessId=" + request.getRequestedSessionId() + "|xFwdFor="
						+ getNullSafeValue(getHTTPHeaderValue(request, "X-Forwarded-For")) + "|remoteAddr="
						+ request.getRemoteAddr() + "|browser=["
						+ getNullSafeValue(getHTTPHeaderValue(request, "User-Agent")) + "]" + "|referer="
						+ getNullSafeValue(getHTTPHeaderValue(request, "Referer")) + "]");

				String msg = "Invalid request detected.";
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);

				return StrutsConstants.Result.ERROR;
			}

			if (log.isDebugEnabled()) {
				log.debug(" hdnParamData = " + RequestUtil.getHiddenParamData(request));
			}
		} catch (Exception e) {
			log.error("ShowReservationAction requested is with invalid request parameters [hdnParamData="
					+ RequestUtil.getHiddenParamData(request) + "]", e);
			String msg = "Invalid request detected.";
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
			return StrutsConstants.Result.ERROR;
		}

		try {
			// Only for the testing purpose
			// TODO Remove
			if (AppSysParamsUtil.isTestStstem() && hdnOriginCountry != null && hdnOriginCountry.trim().length() > 0) {
				request.getSession().setAttribute("originCountry", hdnOriginCountry);
			}

			if (AppSysParamsUtil.isIbeDesignVersionAvail()
					&& AppSysParamsUtil.getDefaultDesign().toUpperCase().equals("GWO")
					&& (hdnDesign == null || hdnDesign.equals(""))) {
				request.setAttribute("defaultCCODE", AppSysParamsUtil.getDefaultCarrierCode());
				request.setAttribute(RequestParameter.HIDDEN_PARAM_DATA, hdnParamData);
				request.setAttribute("hdnMode", hdnMode);
				request.setAttribute("hdnParamUrl", hdnParamUrl);
				forward = StrutsConstants.Jsp.Respro.MIDDEL_PAGE_LOADER;

			} else {
				// TODO set the application parameter to Maintain Design
				// Versions
				if (hdnDesign == null) {
					hdnDesign = AppSysParamsUtil.getDefaultDesign();
				}
				SessionUtil.setDesignVersion(request, hdnDesign);
				String paramArr[] = hdnParamData.split(WebConstants.PARAM_SPLIT);
				AirlineRequestDTO airlineRequestParam = new AirlineRequestDTO(paramArr);

				request.getSession().setAttribute(ReservationWebConstnts.EXTERNAL_PARTY_ID, hdnTrackingID);
				switch (airlineRequestParam.getRequestType()) {
				case AS:
					// Check client's IP for system redirect
					String redirectUrl = checkApplicationRedirect(request, hdnOriginCountry);

					if (redirectUrl != null) {
						request.setAttribute("param", redirectHdnParamData);
						// request.setAttribute("redirectUrl", redirectUrl);
						Map<String, String> formHiddenDataMap = new HashMap<String, String>();
						formHiddenDataMap.put(RequestParameter.HIDDEN_PARAM_DATA, hdnParamData);
						SystemUtil.createRedirectFormData(request, redirectUrl, formHiddenDataMap, false);
						return StrutsConstants.Jsp.Common.REDIRECT;
					}

					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3;
						} else {
							forward = StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE;
							// To Set to both flows in to one in availability
							// forward =
							// StrutsConstants.Jsp.Respro.INTERLINE_LOAD_FARE_V3;
						}
						if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
							request.setAttribute(ReservationWebConstnts.PARAM_OND_SOURCE,
									AppSysParamsUtil.getDynamicOndListUrl());
						}
						request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
								CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));

					}
					break;
				case SI:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Customer.LOGIN_V3;
						} else {
							forward = StrutsConstants.Jsp.Customer.LOGIN;
						}
					}
					break;
				case PRM:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						String promReqString = URLDecoder.decode(promReqAttr, "UTF-8");
						request.setAttribute(ReservationWebConstnts.PROM_REQ_ATTR, promReqString);
						forward = StrutsConstants.Jsp.Promotion.PROMOTION_REGISTRATION;
					}
					break;
				case KOS:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						if (SystemUtil.isKioskUser(request)) {
							request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
									CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));
							if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
								request.setAttribute(ReservationWebConstnts.PARAM_OND_SOURCE,
										AppSysParamsUtil.getDynamicOndListUrl());
							}
							if (SessionUtil.isNewDesignVersion(request)) {
								forward = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK_V3;
							} else {
								forward = StrutsConstants.Jsp.Kiosk.WELCOME_KIOSK;
							}
						} else {
							if (SessionUtil.isNewDesignVersion(request)) {
								forward = StrutsConstants.Jsp.Kiosk.LOGIN_V3;
							} else {
								forward = StrutsConstants.Jsp.Kiosk.LOGIN;
							}
						}
					}
					break;
				case KOL:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Kiosk.LOGIN_V3;
						} else {
							forward = StrutsConstants.Jsp.Kiosk.LOGIN;
						}
					}
					break;
				case MAN:
					SessionUtil.initialize(request);
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						if (fromPromoRegister) {
							IBEPromotionPaymentDTO ibePromotionPaymentDTO = new IBEPromotionPaymentDTO();
							List<PromotionRequestTo> colPromotionRequestTo = JSONFETOParser
									.parseCollection(ArrayList.class, PromotionRequestTo.class, promoRequestData);
							ibePromotionPaymentDTO.setColPromotionRequestTo(colPromotionRequestTo);
							// TODO: resolve totalPayment by promo requests
							ibePromotionPaymentDTO.setTotalPayment(totalPayment);
							SessionUtil.setIbePromotionPaymentPostDTO(request, ibePromotionPaymentDTO);
							request.setAttribute("promoPnr", promoPnr);
							request.setAttribute("promoLastName", promoLastName);
							request.setAttribute("promoDepDate", promoDepDate);
						}

						request.setAttribute(ReservationWebConstnts.HTTPS_COMPATIBILITY,
								AppSysParamsUtil.isHttpsEnabledForIBE());

						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Respro.MANAGE_BOOKING_V3;
						} else {
							forward = StrutsConstants.Jsp.Respro.MANAGE_BOOKING;
						}
					}
					break;
				case RE:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
								CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));

						String jsonString = request.getParameter("autoFillData");
						if (jsonString != null && !jsonString.isEmpty()) {
							request.setAttribute("dataFromSocialSite", jsonString);
						}

						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Customer.REGISTER_V3;
						} else {
							forward = StrutsConstants.Jsp.Customer.REGISTER;
						}
					}
					break;
				case MRE:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
								CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));

						String jsonString = request.getParameter("autoFillData");
						if (jsonString != null && !jsonString.isEmpty()) {
							request.setAttribute("dataFromSocialSite", jsonString);
						}
						request.setAttribute("enableGoogleAnalytics", AppSysParamsUtil.isGoogleAnalyticsEnabled());
						request.setAttribute("googleAnalyticsKey", AppSysParamsUtil.getGoogleAnalyticsId());
						forward = StrutsConstants.Jsp.Customer.MOBILE_REGISTER_V3;

					}
					break;
				case REA:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Agent.REGISTER_V3;
						} else {
							forward = StrutsConstants.Jsp.Agent.REGISTER;
						}
						request.getSession().setAttribute("taCo", "TA");
					}
					break;
				case REC:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Agent.REGISTER_V3;
						} else {
							forward = StrutsConstants.Jsp.Agent.REGISTER;
						}
						request.getSession().setAttribute("taCo", "CO");
					}
					break;
				case MB:
					// clear all session data when going to MB page
					if (BeanUtils.isNull(airlineRequestParam.getPnr())
							|| BeanUtils.isNull(airlineRequestParam.getLastName())
							|| BeanUtils.isNullHandler(airlineRequestParam.getDepartueDate())) {
						String message = "Invalid request detected.";
						log.error(message);
						request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, message);
						return StrutsConstants.Result.ERROR;
					}
					SessionUtil.initializeIfNot(request);
					SessionUtil.resetSesionDataInError(request);
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {

						request.setAttribute(ReservationWebConstnts.HTTPS_COMPATIBILITY,
								AppSysParamsUtil.isHttpsEnabledForIBE());

						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Respro.MANAGE_BOOKING_V3;
						} else {
							forward = StrutsConstants.Jsp.Respro.MANAGE_BOOKING;
						}
						setManageBookingData(request, airlineRequestParam);
					}
					break;
				case ELINK:
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {

						request.setAttribute(ReservationWebConstnts.HTTPS_COMPATIBILITY,
								AppSysParamsUtil.isHttpsEnabledForIBE());

						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Respro.MANAGE_BOOKING_V3;
						} else {
							forward = StrutsConstants.Jsp.Respro.MANAGE_BOOKING;
						}
						setManageBookingData(request, airlineRequestParam);
					}
					break;
				case MC:
					// Used for multicity search
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
								CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));
						if (SessionUtil.isNewDesignVersion(request)) {
							forward = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY_V3;
						} else {
							forward = StrutsConstants.Jsp.Respro.INTERLINE_MULTICITY;
						}
						if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
							request.setAttribute(ReservationWebConstnts.PARAM_OND_SOURCE,
									AppSysParamsUtil.getDynamicOndListUrl());
						}

						if (isDrySearch()) {
							request.setAttribute("drySearch", isDrySearch());
							request.setAttribute("ondListString", getOndListString());
							request.setAttribute("paxCountStr", getPaxCountStr());
						}

					}
					break;
				case DIRECTLOARDPNR:
					// Functionality can use to load the reservation directly
					// Pls validate data before load the reservation
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						String pnr = (String) request.getAttribute(WebConstants.PNR);

						if (pnr == null || "".equals(pnr)) {
							pnr = request.getParameter(WebConstants.PNR);
						}

						if (pnr != null) {
							SessionUtil.setManageBookingPNR(request, pnr);
							return StrutsConstants.Action.SHOWCUSTOMERLOADPAGE;
						} else {
							request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, I18NUtil.getMessage(
									ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request)));
							return StrutsConstants.Result.ERROR;
						}
					}
				case SR:
					
					if (BeanUtils.isNull(airlineRequestParam.getPnr()) || BeanUtils.isNull(airlineRequestParam.getAlertId())
							|| BeanUtils.isNullHandler(airlineRequestParam.getPnrSegId())) {
						String message = "Invalid request detected.";
						log.error(message);
						request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, message);
						return StrutsConstants.Result.ERROR;
					}
					
					if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
						
						request.setAttribute("pnr", airlineRequestParam.getPnr());
						request.setAttribute("alertId", airlineRequestParam.getAlertId());
						request.setAttribute("pnrSegId" , airlineRequestParam.getPnrSegId());
						SessionDataDTO sessionDataDTO = (SessionDataDTO) request.getSession().getAttribute(SessionAttribute.SESSION_DATA_DTO);
						sessionDataDTO.setManageBookingPNR(airlineRequestParam.getPnr());
						request.getSession().setAttribute(SessionAttribute.SESSION_DATA_DTO, sessionDataDTO);
						
						if (SessionUtil.isNewDesignVersion(request)) {
							return StrutsConstants.Jsp.Customer.SELF_REPROTECTION;
						} else {
							return StrutsConstants.Jsp.Customer.SELF_REPROTECTION;
						}
					}
					
					break;
				case GV:
					if (AppSysParamsUtil.isVoucherEnabled()) {
						if ((fwd = handleSessionUpdate(request, hdnMode)) == null) {
							forward = StrutsConstants.Jsp.Voucher.GIFT_VOUCHER;
						}
						request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
								CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));
						break;
					}
				default:
					log.error("ShowReservationAction requested is with invalid request parameters [hdnParamData="
							+ RequestUtil.getHiddenParamData(request) + "]");
					String msg = "Invalid request detected.";
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
					return StrutsConstants.Result.ERROR;
				}

				// Check Selected Currency Exchange rate
				// if selected currency is not available , change to base
				// currency.
				if (paramArr.length > 9) {
					ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(
							CalendarUtil.getCurrentSystemTimeInZulu());
					CurrencyExchangeRate exchangeRate = null;
					try {
						exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(airlineRequestParam.getCurrency(),
								ApplicationEngine.IBE);
					} catch (Exception ex) {
						exchangeRate = null;
					}
					if (exchangeRate == null) {
						paramArr[8] = AppSysParamsUtil.getBaseCurrency();
						hdnParamData = "";
						for (int i = 0; i < paramArr.length; i++) {
							if (i > 0) {
								hdnParamData += "^";
							}
							hdnParamData += paramArr[i];
						}
					}
				}

				// splitting code to allow sub airports
				if (paramArr.length > 4) {
					// from to is required
					if (paramArr[2].indexOf(SubStationUtil.SUB_STATION_CODE_SEPERATOR) != -1) {
						paramArr[2] = paramArr[2].split(SubStationUtil.SUB_STATION_CODE_SEPERATOR)[0];
					}
					if (paramArr[3].indexOf(SubStationUtil.SUB_STATION_CODE_SEPERATOR) != -1) {
						paramArr[3] = paramArr[3].split(SubStationUtil.SUB_STATION_CODE_SEPERATOR)[0];
					}
				}

				request.setAttribute(ReservationWebConstnts.REQ_PARAM_DETAILS, hdnParamData);
				request.setAttribute(ReservationWebConstnts.PARAM_IMAGE_HOST_PATH,
						AppSysParamsUtil.getImageUploadPath());
				commonParams = new IBECommonDTO();
				SystemUtil.setCommonParameters(request, commonParams);
				request.setAttribute(ReservationWebConstnts.PARAM_COMMON, commonParams);
			}
		} catch (ModuleException me) {
			forward = StrutsConstants.Result.ERROR;
			setCommonParams();
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, I18NUtil.getMessage(me.getExceptionCode()));
			log.error("ShowReservationAction==>", me);
		} catch (Exception re) {
			forward = StrutsConstants.Result.ERROR;
			setCommonParams();
			String msg = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL,
					SessionUtil.getLanguage(request));
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
			log.error("ShowReservationAction", re);
		}

		if (fwd != null) {
			forward = fwd;
		}
		return forward;
	}

	private void setCommonParams() {
		commonParams = new IBECommonDTO();
		commonParams.setHomeURL(IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.AIRLINE_URL));
		request.setAttribute(ReservationWebConstnts.PARAM_COMMON, commonParams);
	}

	private static String checkApplicationRedirect(HttpServletRequest request, String originCountry)
			throws ModuleException {
		String marketingCarrier;

		if (originCountry == null || originCountry.isEmpty()) {

			String clientIp = WebplatformUtil.getClientIpAddress(request);
			long originIp = PlatformUtiltiies.getOriginIP(clientIp);
			Country country = CommonUtil.getCountryFromIPAddress(originIp);
			marketingCarrier = (country == null) ? null : country.getMarketingCarrier();

		} else {
			/*
			 * This is used in IBE dummy page search only
			 */
			marketingCarrier = CommonUtil.getMarketingCarrierFromCountry(originCountry);
		}

		marketingCarrier = (marketingCarrier == null) ? AppSysParamsUtil.getDefaultCarrierCode() : marketingCarrier;

		String redirectUrl = null;
		if (!marketingCarrier.equals(AppSysParamsUtil.getDefaultCarrierCode())) {
			Map<String, String> carrierUrlMap = ModuleServiceLocator.getGlobalConfig().getCarrierUrlsIBE();
			redirectUrl = carrierUrlMap.get(marketingCarrier);
		}

		return redirectUrl;
	}

	private static String handleSessionUpdate(HttpServletRequest request, String hdnMode) throws ModuleException {
		if (!SessionUtil.isSessionExpired(request)) {

			IBEReservationPostDTO ibeReservationPostDTO = SessionUtil.getIbeReservationPostDTO(request);
			String reservationPNR = null;
			if (ibeReservationPostDTO != null) {
				reservationPNR = ibeReservationPostDTO.getPnr();
			}
			boolean isPaymentSuccess = SessionUtil.isResponseReceivedFromPaymentGateway(request);
			if (isPaymentSuccess) {
				String msg = "";
				log.info("PNR :" + reservationPNR + " isPaymentSuccess :" + isPaymentSuccess + " SessionID :"
						+ request.getRequestedSessionId());
				if (reservationPNR != null && !reservationPNR.trim().isEmpty()) {
					msg = CommonUtil.getExistingReservationMsg(request, reservationPNR);
				} else {
					msg = CommonUtil.getRefundMessage(SessionUtil.getLanguage(request));
				}
				request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
				SessionUtil.expireSession(request);
				return StrutsConstants.Result.ERROR;
			}

		} else {
			SessionUtil.initialize(request);
			log.info("Initialized new session [sessionId=" + request.getSession().getId() + "]");
		}

		request.getSession().setAttribute(SessionAttribute.HDN_MODE, hdnMode);
		SessionUtil.setLanguage(request);
		SessionUtil.setLocale(request);
		// Set Session Data
		setOriginCarrier(request);
		setNewData(request);
		return null;
	}

	private void setManageBookingData(HttpServletRequest request, AirlineRequestDTO airlineRequestParam) {
		// TODO Load Reservation using Manage booking data
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		request.setAttribute("link", "ELINK");
		request.setAttribute("pnr", airlineRequestParam.getPnr());
		request.setAttribute("lname", airlineRequestParam.getLastName());
		request.setAttribute("ddate", dateFormat.format(airlineRequestParam.getDepartueDate()));
	}

	private static void setOriginCarrier(HttpServletRequest request) throws ModuleException {
		String strOriginCarrier = request.getParameter(RequestParameter.CARRIER);

		if (strOriginCarrier != null && !strOriginCarrier.trim().equals("")) {
			SessionUtil.setCarrier(request, strOriginCarrier);
		} else {
			SessionUtil.setCarrier(request, AppSysParamsUtil.getDefaultCarrierCode());
		}

	}

	private static void setNewData(HttpServletRequest request) throws ModuleException {
		HttpSession session = request.getSession();
		String strOriginCarrier = request.getParameter(RequestParameter.CARRIER);

		GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
		session.setAttribute(CustomerWebConstants.APP_AIRLINE_NAME_STR,
				globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME, strOriginCarrier));
		session.setAttribute(CustomerWebConstants.APP_AIRLINE_HOME_TEXT_STR,
				globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, strOriginCarrier));
		session.setAttribute(CustomerWebConstants.APP_AIRLINE_HOME_URL_STR,
				globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, strOriginCarrier));
	}

	private static String handleNewSearchAfterPartialPaymentConfirm(HttpServletRequest request) {

		try {
			String timestamp = CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date());
			IBEReservationPostPaymentDTO sessResInfo = SessionUtil.getIBEReservationPostPaymentDTO(request);
			int tempPayId = LCCClientApiUtils.getTmpPayIdFromTmpPayMap(sessResInfo.getTemporyPaymentMap());
			String pnr = sessResInfo.getPnr();
			String merchantTxnRef = sessResInfo.getIpgRefenceNo();

			log.error("Detected new search when previous booking attempt is partially completed. :" + "|timestamp="
					+ timestamp + "|sessId=" + request.getRequestedSessionId() + "|xFwdFor="
					+ getNullSafeValue(getHTTPHeaderValue(request, "X-Forwarded-For")) + "|remoteAddr="
					+ request.getRemoteAddr() + "|browser=["
					+ getNullSafeValue(getHTTPHeaderValue(request, "User-Agent")) + "]" + "|referer="
					+ getNullSafeValue(getHTTPHeaderValue(request, "Referer")) + "|pnr=" + pnr + "|merchantTxnRef="
					+ merchantTxnRef + "|tptId=" + tempPayId + "]");
		} catch (Exception ex) {
			log.equals(ex);
		}

		try {
			SeatMapUtil.releaseBlockedSeats(request);
		} catch (Exception ex) {
			log.error("Blocked seats release failed", ex);
		}

		// FIXME - get proper message from user; also maintain i18n
		String msg = "Detected new search when previous booking attempt is either aborted or still in progress. <br>"
				+ "Please start over again. <br>"
				+ "If unable to get through, please close all your web browsers and start over.";

		request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
		SystemUtil.setCommonParameters(request, new IBECommonDTO());
		SessionUtil.resetSesionDataInError(request); // Note : This will cause
														// any parallel
														// transaction in
														// progress on
														// same session to fail
														// when response
														// received

		return StrutsConstants.Result.ERROR;

	}

	public String getHdnParamData() {
		return hdnParamData;
	}

	public void setHdnParamData(String hdnParamData) {
		this.hdnParamData = hdnParamData;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public IBECommonDTO getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

	public String getHdnOriginCountry() {
		return hdnOriginCountry;
	}

	public void setHdnOriginCountry(String hdnOriginCountry) {
		this.hdnOriginCountry = hdnOriginCountry;
	}

	// TODO - refactor into common place
	private static String getRequestParamsString(Map<String, String> params) {
		StringBuffer requestParams = new StringBuffer();

		if (params != null) {
			String key;
			for (String string : params.keySet()) {
				key = string;
				if ("".equals(requestParams)) {
					requestParams.append(key + "=" + params.get(key));
				} else {
					requestParams.append(", " + key + "=" + params.get(key));
				}
			}
		}
		return requestParams.toString();
	}

	// TODO - refactor into common place
	private static String getHTTPHeaderValue(HttpServletRequest request, String headerName) {
		String headerParamValue = "";
		try {
			headerParamValue = request.getHeader(headerName);
		} catch (Exception e) {
			headerParamValue = "-";
		}
		return headerParamValue;
	}

	// TODO - refactor into common place
	private static String getNullSafeValue(String value) {
		return StringUtils.trimToEmpty(value);
	}
	
	private boolean validMarkerID(String markerID) {
		boolean isValidMarkerID = false;
		if (marker != null && !marker.equals("") && marker.length() <= 200) {
			isValidMarkerID = true;
		}
		return isValidMarkerID;
	}

	public void setHdnDesign(String hdnDesign) {
		this.hdnDesign = hdnDesign;
	}

	public String getHdnDesign() {
		return hdnDesign;
	}

	public void setPromReqAttr(String promReqAttr) {
		this.promReqAttr = promReqAttr;
	}

	public void setFromPromoRegister(boolean fromPromoRegister) {
		this.fromPromoRegister = fromPromoRegister;
	}

	public void setPromoRequestData(String promoRequestData) {
		this.promoRequestData = promoRequestData;
	}

	public void setTotalPayment(String totalPayment) {
		this.totalPayment = totalPayment;
	}

	public void setPromoPnr(String promoPnr) {
		this.promoPnr = promoPnr;
	}

	public void setPromoLastName(String promoLastName) {
		this.promoLastName = promoLastName;
	}

	public void setPromoDepDate(String promoDepDate) {
		this.promoDepDate = promoDepDate;
	}

	public String getIbeExitDetailsId() {
		return ibeExitDetailsId;
	}

	public void setIbeExitDetailsId(String ibeExitDetailsId) {
		this.ibeExitDetailsId = ibeExitDetailsId;
	}

	public String getOndListString() {
		return ondListString;
	}

	public void setOndListString(String ondListString) {
		this.ondListString = ondListString;
	}

	public boolean isDrySearch() {
		return drySearch;
	}

	public void setDrySearch(boolean drySearch) {
		this.drySearch = drySearch;
	}

	public String getPaxCountStr() {
		return paxCountStr;
	}

	public void setPaxCountStr(String paxCountStr) {
		this.paxCountStr = paxCountStr;
	}

	public String getHdnTrackingID() {
		return hdnTrackingID;
	}

	public void setHdnTrackingID(String hdnTrackingID) {
		this.hdnTrackingID = hdnTrackingID;
	}

	public String getMarker() {
		return marker;
	}

	public void setMarker(String marker) {
		this.marker = marker;
	}

}
