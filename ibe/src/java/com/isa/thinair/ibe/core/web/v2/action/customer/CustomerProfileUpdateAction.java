package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.ibm.icu.text.SimpleDateFormat;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMeal;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMealPK;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredSeat;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.api.dto.FamilyMemberDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CustomerQuestionaireDTO;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CustomerProfileUpdateAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(CustomerProfileUpdateAction.class);

	private boolean success = true;

	private String messageTxt;

	private CustomerDTO customer;

	private LmsMemberDTO lmsDetails;

	private List<CustomerQuestionaireDTO> questionaire;
	
	private String preferredSeatType = null;
	
	private String preferredMealsList = null;
	
	private FamilyMemberDTO familyMember;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();

			customer.setCustomerID(SessionUtil.getCustomerId(request));
			customer.setCustomerQuestionaireDTO(questionaire);

			Customer updatedCustomer = CustomerUtilV2.getModelCustomer(customer);

			if (AppSysParamsUtil.isLMSEnabled()){
				if (updatedCustomer.getLMSMemberDetails() != null) {
					LmsMember lmsMember = lmsMemberDelegate.getLmsMember(customer.getEmailId());
					lmsMember.setResidencyCode(customer.getCountryCode());
					lmsMember.setNationalityCode(customer.getNationalityCode());
					lmsMember.setMobileNumber(LmsCommonUtil.getValidPhoneNumber(customer.getMobile()));
					lmsMember.setPhoneNumber(LmsCommonUtil.getValidPhoneNumber(customer.getTelephone()));
					lmsMember.setLanguage(lmsDetails.getLanguage());
					lmsMember.setHeadOFEmailId(lmsDetails.getHeadOFEmailId());
					lmsMember.setRefferedEmail(lmsDetails.getRefferedEmail());
					lmsMember.setPassportNum(lmsDetails.getPassportNum());
	
					// 10/01/1983
					if (lmsDetails.getDateOfBirth() != null && !"".equals(lmsDetails.getDateOfBirth().trim())) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						lmsMember.setDateOfBirth(sdf.parse(lmsDetails.getDateOfBirth()));
					}
					
					if(lmsDetails.getHeadOFEmailId() != null && !"".equals(lmsDetails.getHeadOFEmailId().trim())){
						LmsMember headOfEmailFFID = lmsMemberDelegate.getLmsMember(lmsDetails.getHeadOFEmailId());
						if(headOfEmailFFID == null){
							success = false;
							//FIXME improve error and successful handling of the FE also
							messageTxt = "No LMS account found for head of familly FFID";
						}
					}
	
					if (lmsDetails.getRefferedEmail() != null && !"".equals(lmsDetails.getRefferedEmail().trim())) {
						lmsMember.setRefferedEmail(lmsDetails.getRefferedEmail());
					}
	
					if(success){
						Set<LmsMember> lmsMembers = new HashSet<LmsMember>();
						lmsMembers.add(lmsMember);
						updatedCustomer.setCustomerLmsDetails(lmsMembers);
						updatedCustomer.setIsLmsMember('Y');
						loyaltyManagementBD.updateMemberProfile(updatedCustomer);
					}
				}
			}

			if (success) {
				customerDelegate.saveOrUpdate(updatedCustomer);
			}
			
			if(preferredSeatType != null && !preferredSeatType.equals("")){
				CustomerPreferredSeat customerPreferredSeat = new CustomerPreferredSeat();
				customerPreferredSeat.setCustomerId(SessionUtil.getCustomerId(request));
				customerPreferredSeat.setSeatType(preferredSeatType);
				customerDelegate.setCustomerPreferredSeat(customerPreferredSeat);
			}
			
			if(preferredMealsList != null && !preferredMealsList.equals("")){
			
				int customerId = SessionUtil.getCustomerId(request);
				List<CustomerPreferredMeal> customerPreferredMealList = new ArrayList<CustomerPreferredMeal>();
				String mealList[] = preferredMealsList.split(",");
				
				for(String meal : mealList){
					CustomerPreferredMeal customerPreferredMeal = new CustomerPreferredMeal();
					CustomerPreferredMealPK customerPreferredMealPK = new CustomerPreferredMealPK(customerId, meal.split("_")[0]);
					customerPreferredMeal.setId(customerPreferredMealPK);
					customerPreferredMeal.setQuantity(Integer.parseInt(meal.split("_")[1]));
					customerPreferredMealList.add(customerPreferredMeal);
				}
				
				customerDelegate.setCustomerPreferredMeals(customerPreferredMealList);
			}

		} catch (ModuleException me) {
			if(!me.getExceptionCode().equals("lms.data.pass")){
				success = false;
				messageTxt = me.getMessageString();
			}
			log.error("CustomerProfileUpdateAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("CustomerProfileUpdateAction==>", ex);
		}
		return forward;
	}
	
	public String saveUpdateFamilyMembers(){
		String forward = StrutsConstants.Result.SUCCESS;
		FamilyMember familyMember = CustomerUtilV2.getFamilyMemberModel(this.familyMember);
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		FamilyMember tempFamilyMember;
		try {
			if(familyMember.getFamilyMemberId() != null){
				tempFamilyMember = customerDelegate.getFamilyMember(familyMember.getFamilyMemberId());
				tempFamilyMember.setTitle(familyMember.getTitle());
				tempFamilyMember.setFirstName(familyMember.getFirstName());
				tempFamilyMember.setLastName(familyMember.getLastName());
				tempFamilyMember.setRelationshipId(familyMember.getRelationshipId());
				tempFamilyMember.setNationalityCode(familyMember.getNationalityCode());
				tempFamilyMember.setDateOfBirth(familyMember.getDateOfBirth());
				customerDelegate.saveOrUpdate(tempFamilyMember);
				this.familyMember = CustomerUtilV2.getFamilyMemberDTO(customerDelegate.getFamilyMember(familyMember.getFamilyMemberId()));
			}else{
				familyMember.setCustomerId(SessionUtil.getCustomerId(request));
				customerDelegate.saveOrUpdate(familyMember);
				this.familyMember = CustomerUtilV2.getFamilyMemberDTO(customerDelegate.getFamilyMember(familyMember.getFirstName(), familyMember.getLastName(), SessionUtil.getCustomerId(request)));
			}
		} catch (ModuleException e) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("Family Member save failed ==> ", e);
		}
		
		return forward;
	}
	
	public String removeFamilyMember(){
		String forward = StrutsConstants.Result.SUCCESS;
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		try {
			FamilyMember familyMember = customerDelegate.getFamilyMember(this.familyMember.getFamilyMemberId());
			customerDelegate.removeFamilyMember(familyMember);
		} catch (ModuleException e) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("Family Member delete failed ==> ", e);
		}
		
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public List<CustomerQuestionaireDTO> getQuestionaire() {
		return questionaire;
	}

	public void setQuestionaire(List<CustomerQuestionaireDTO> questionaire) {
		this.questionaire = questionaire;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public void setPreferredSeatType(String preferredSeatType) {
		this.preferredSeatType = preferredSeatType;
	}

	public void setPreferredMealsList(String preferredMealsList) {
		this.preferredMealsList = preferredMealsList;
	}

	public FamilyMemberDTO getFamilyMember() {
		return familyMember;
	}

	public void setFamilyMember(FamilyMemberDTO familyMember) {
		this.familyMember = familyMember;
	}

}
