package com.isa.thinair.ibe.core.web.v2.action.payment.cmi;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;

import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

@Namespace(StrutsConstants.Namespace.PUBLIC)
public class CmiNotifyPaymentResponseHandler extends
		BaseRequestResponseAwareAction {

	private static Log log = LogFactory
			.getLog(CmiNotifyPaymentResponseHandler.class);

	public String execute() {
		if (log.isDebugEnabled())
			log.debug("###Start.." + request.getRequestedSessionId());
		response.setContentType("text/html;charset=UTF-8");
		try {
			PrintWriter out = response.getWriter();
			Map<String, String> recieptMap = getRecieptMap(request);
			String ipGId = "";
			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			int paymentBrokerRefNo = Integer.parseInt(ipGId);// to set paymet
																// broker id
			String paymentId = request.getParameter("Response");
			String result = request.getParameter("ProcReturnCode");
			String trackid = request.getParameter("trackid");													// value
			String trnSId = request.getParameter("TransId");
			ipgResponseDTO.setAuthorizationCode(result);
			ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance()
					.getTime());
			ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
			ipgResponseDTO.setTemporyPaymentId(Integer.parseInt(trackid));
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil
					.validateAndPrepareIPGConfigurationParamsDTO(new Integer(
							ipGId), "504");
			ModuleServiceLocator.getPaymentBrokerBD().getReponseData(
					recieptMap, ipgResponseDTO, ipgIdentificationParamsDTO);

			out.flush();
			out.close();

		} catch (Exception e) {
			log.error(e);
		}
		if (log.isDebugEnabled())
			log.debug("###END.." + request.getRequestedSessionId());
		return null;
	}

	public Map<String, String> getRecieptMap(HttpServletRequest request) {

		Map<String, String> allParams = new HashMap();
		Map<String, String[]> parameterMap = request.getParameterMap();
		Set<String> requestParams = parameterMap.keySet();
		for (String requestParam : requestParams) {
			String[] allRequestParamValues = parameterMap.get(requestParam);
			if (allRequestParamValues != null
					&& allRequestParamValues.length > 0) {
				String value = allRequestParamValues[0];
				allParams.put(requestParam, value);
				log.debug("Response parameters from CMI" + requestParam + "="
						+ value);
			}
		}
		return allParams;
	}
}
