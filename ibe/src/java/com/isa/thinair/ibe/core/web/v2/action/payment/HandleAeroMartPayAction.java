package com.isa.thinair.ibe.core.web.v2.action.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AeroMartPaySessionInfo;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayResponse;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayUtil;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidator;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidatorImpl;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.opensymphony.xwork2.ActionChainResult;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.AEROMART_PAY_RESPONSE, value = StrutsConstants.Jsp.Common.AEROMART_PAY_RESPONSE),
		@Result(name = StrutsConstants.Result.AEROMART_PAY_IPG_REDIRECTION, value = StrutsConstants.Jsp.Common.AEROMART_PAY_IPG_REDIRECTION),
		@Result(name = StrutsConstants.Result.AEROMART_PAY_EXT_IPG_REDIRECTION, value = StrutsConstants.Jsp.Common.AEROMART_PAY_EXT_IPG_REDIRECTION),
		@Result(name = StrutsConstants.Action.AEROMART_PAY_BYPASS, type = ActionChainResult.class, value = StrutsConstants.Action.AEROMART_PAY_BYPASS),
		@Result(name = StrutsConstants.Result.LOAD_AEROMARTPAY_PAYMENT_OPTIONS, value = StrutsConstants.Jsp.Common.LOAD_AEROMARTPAY_PAYMENT_OPTIONS) })
public class HandleAeroMartPayAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(HandleAeroMartPayAction.class);
	private static final Object staticLock = new Object();

	public String execute() {

		String result = null;
		// TODO impl as service
		AeroMartPayValidator aeroMartPayValidator = new AeroMartPayValidatorImpl();
		if (AeroMartPayUtil.isPost(request)) {

			AeroMartPayResponse aeroMartPayResponse = null;

			String referenceID = "";

			HttpSession httpSession = null;

			Map<String, String> requestParams = AeroMartPayUtil.setPaymentRequestParamMap(request);
			String totalAmount = requestParams.get(AeroMartPayConstants.PARAM_TOTAL_AMOUNT);
			String currencyCode = requestParams.get(AeroMartPayConstants.PARAM_CURRENCY_CODE);
			String languageCode = requestParams.get(AeroMartPayConstants.PARAM_LANGUAGE_CODE);
			String returnURL = requestParams.get(AeroMartPayConstants.PARAM_RETURN_URL);
			String errorURL = requestParams.get(AeroMartPayConstants.PARAM_ERROR_URL);
			String orderID = requestParams.get(AeroMartPayConstants.PARAM_ORDER_ID);
			String merchantID = requestParams.get(AeroMartPayConstants.PARAM_MERCHANT_ID);
			String agentType = getAgentType(requestParams);

			try {

				aeroMartPayResponse = aeroMartPayValidator.validatePaymentRequest(requestParams);

				if (aeroMartPayResponse != null) {
					throw new ModuleException(aeroMartPayResponse.getRespMessage());
				}

				// TODO move to DB level synchronization (couldn't do as it's product_type based)
				synchronized (staticLock) {

					if (!AeroMartPayUtil.isUniqueOrderID(orderID)) {
						aeroMartPayResponse = AeroMartPayResponse.ERR_50;
						throw new ModuleException(aeroMartPayResponse.getRespMessage());
					}

					httpSession = request.getSession(false);
					if (httpSession != null) {
						log.info("Invaildate current session.");
						httpSession.invalidate();
					}

					// create new session
					httpSession = request.getSession(true);
					BigDecimal basecurrencyValue = null;
					try {

						basecurrencyValue = AeroMartPayUtil.getBaseCurrencyValue(currencyCode, new BigDecimal(totalAmount));

						if (basecurrencyValue == null || basecurrencyValue.doubleValue() <= 0) {
							throw new ModuleException("");
						}
					} catch (Exception e) {
						aeroMartPayResponse = AeroMartPayResponse.ERR_12;
						throw new ModuleException(aeroMartPayResponse.getRespMessage());

					}

					referenceID = AeroMartPayUtil.generateReferenceID(basecurrencyValue, "", true, "", "", orderID, "", "",
							new BigDecimal(totalAmount), currencyCode, "", AeroMartPayConstants.PRODUCT_TYPE);

				}

				if (!aeroMartPayValidator.validateReferenceID(referenceID)) {
					aeroMartPayResponse = AeroMartPayResponse.ERR_51;
					throw new ModuleException(aeroMartPayResponse.getRespMessage());
				}

				request.getSession().setAttribute(AeroMartPayConstants.PAYMENT_OPTIONS_SESSION_LOCK, false);
				request.getSession().setAttribute(AeroMartPayConstants.RETRAILS, 0);
				request.getSession().setAttribute(AeroMartPayConstants.PARAM_MERCHANT_ID, merchantID);

			//	String merchantDisplayName = AeroMartPayUtil.getMerchant(merchantID).getDisplayName();
				
				AeroMartPaySessionInfo aeroMartPaySessionInfoDTO = new AeroMartPaySessionInfo(orderID, referenceID,
						languageCode, currencyCode, totalAmount, returnURL, errorURL, merchantID, agentType);

				httpSession.setAttribute(AeroMartPayConstants.SESSION_INFO, aeroMartPaySessionInfoDTO);

				request.setAttribute(AeroMartPayConstants.LOAD_PAYMENT_OPTIONS, AeroMartPayConstants.LOAD_PAYMENT_OPTIONS_URL);

				result = StrutsConstants.Result.LOAD_AEROMARTPAY_PAYMENT_OPTIONS;

			} catch (Exception e) {

				if (aeroMartPayResponse == null) {
					aeroMartPayResponse = AeroMartPayResponse.ERR_53;
				}

				log.error(aeroMartPayResponse.getRespMessage(), e);

				AeroMartPayUtil.setPaymentResponseAttributes(request, orderID, errorURL, aeroMartPayResponse.getRespCode(),
						aeroMartPayResponse.getRespMessage(), referenceID, totalAmount);

				AeroMartPayUtil.invalidateSession(request);

				if (!StringUtil.isNullOrEmpty(errorURL)) {
					result = StrutsConstants.Result.AEROMART_PAY_RESPONSE;
				} else {
					log.error("errorURL is null ");
				}
			}

		}

		return result;

	}

	public String handlePayment() {

		String result = null;

		// TODO impl as service
		AeroMartPayValidator aeroMartPayValidator = new AeroMartPayValidatorImpl();

		if (AeroMartPayUtil.isPost(request)) {

			AeroMartPayResponse aeroMartPayResponse = null;

			HttpSession session = request.getSession(false);

			AeroMartPaySessionInfo sessionInfo = (AeroMartPaySessionInfo) session.getAttribute(AeroMartPayConstants.SESSION_INFO);

			try {

				if (!aeroMartPayValidator.validateSessionDetails(sessionInfo)) {
					aeroMartPayResponse = AeroMartPayResponse.ERR_105;
					throw new ModuleException(aeroMartPayResponse.getRespMessage());
				} else {
					if (!AeroMartPayUtil.sessionHandlePaymentLock(session, true)) {
						aeroMartPayResponse = AeroMartPayResponse.ERR_108;
						throw new ModuleException(aeroMartPayResponse.getRespMessage());

					}

				}

				String paymentgatewayId = request.getParameter(AeroMartPayConstants.USER_INPUT_PG_ID);
				String cardType = request.getParameter(AeroMartPayConstants.USER_INPUT_CARD_TYPE);
				String cardNumber = request.getParameter(AeroMartPayConstants.USER_INPUT_CARD_NUMBER);
				String cardHolderName = request.getParameter(AeroMartPayConstants.USER_INPUT_CARDHOLDER_NAME);
				String cvv = request.getParameter(AeroMartPayConstants.USER_INPUT_CVV);
				String expiryDate = request.getParameter(AeroMartPayConstants.USER_INPUT_EXPIRY);
				String cardBehavior = request.getParameter(AeroMartPayConstants.USER_INPUT_CARD_BEHAVIOR);

				List<String> messages = aeroMartPayValidator.validateUserInput(cardBehavior, paymentgatewayId, cardNumber,
						cardType, cardHolderName, cvv, expiryDate);

				if (messages == null) {
					TempPaymentTnx temporyPayTnx = AeroMartPayUtil.getTempPaymentTnx(sessionInfo.getReferenceID());

					String productID = temporyPayTnx.getPnr();

					if (StringUtil.isNullOrEmpty(productID) || productID.length() < 6) {
						throw new ModuleException("Invalid Reference ID Generated");
					}

					IBEReservationPostPaymentDTO ibeReservationPostPaymentDTO = new IBEReservationPostPaymentDTO();

					IPGPaymentOptionDTO pgOptionsDto = AeroMartPayUtil.getSelectedPaymentGateway(Integer
							.valueOf(paymentgatewayId));

					if (pgOptionsDto == null) {
						aeroMartPayResponse = AeroMartPayResponse.ERR_56;
						throw new ModuleException(aeroMartPayResponse.getRespMessage());
					}

					BigDecimal totalAmountExchanged = getPaymentAmount(new BigDecimal(sessionInfo.getTotalAmount()),
							sessionInfo.getCurrencyCode(), pgOptionsDto.getBaseCurrency());
					// need to update session info
					sessionInfo.setPaymentAmount(totalAmountExchanged.toString());
					sessionInfo.setPaymentCurr(pgOptionsDto.getBaseCurrency());

					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = aeroMartPayValidator
							.validateAndPrepareIPGConfigurationParamsDTO(Integer.valueOf(paymentgatewayId),
									sessionInfo.getPaymentCurr());
					Properties ipgProperties = AeroMartPayUtil.getIPGProperties(ipgIdentificationParamsDTO);
					String brokerType = ipgProperties.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
					
					if (sessionInfo.getAgentType().equals(AeroMartPayConstants.AGENT)) {
						IPGRequestDTO ipgRequestDTO = AeroMartPayUtil.composeIPGRequestDTO(cardType, cardNumber, cardHolderName,
								cvv, expiryDate, ipgIdentificationParamsDTO, productID, temporyPayTnx.getTnxId(),
								sessionInfo.getPaymentAmount(), true, null,
								sessionInfo.getLanguageCode(), getTrackInfo(), getClientInfoDTO());
						
						PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
						// Create credit card payment
						CreditCardPayment creditCardPayment = createCreditCardPayment(ipgRequestDTO);
						ServiceResponce serviceResponce = paymentBrokerBD.charge(creditCardPayment, creditCardPayment.getPnr(), AppIndicatorEnum.APP_XBE, creditCardPayment.getTnxMode(), null);
					
						if (serviceResponce != null && serviceResponce.isSuccess()) {
							request.setAttribute(WebConstants.AGENT_SERVICE_RESPONSE, serviceResponce);
							result = StrutsConstants.Action.AEROMART_PAY_BYPASS;
						}else{
							aeroMartPayResponse = AeroMartPayResponse.ERR_201;
							throw new ModuleException(aeroMartPayResponse.getRespMessage());
						}
						
					} else {
						boolean isBrokerTypeInternalExternal = PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL
								.equals(brokerType);

						String responseTypeXML = ipgProperties
								.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);

						PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

						IPGRequestResultsDTO ipgRequestResultsDTO;

						String handleIPGPaymentResponseURL = AeroMartPayConstants.IPG_PAYMENT_RESPONSE_URL;

						IPGRequestDTO ipgRequestDTO = AeroMartPayUtil.composeIPGRequestDTO(cardType, cardNumber, cardHolderName,
								cvv, expiryDate, ipgIdentificationParamsDTO, productID, temporyPayTnx.getTnxId(),
								sessionInfo.getPaymentAmount(), isBrokerTypeInternalExternal, handleIPGPaymentResponseURL,
								sessionInfo.getLanguageCode(), getTrackInfo(), getClientInfoDTO());

						ipgRequestDTO.setIntExtPaymentGateway(isBrokerTypeInternalExternal);
						ipgRequestDTO.setPaymentGateWayName(pgOptionsDto.getProviderName());

						// hardcoded SYSTEM and App Indicator
						ipgRequestDTO.setSelectedSystem(SYSTEM.AA.name());
						ipgRequestDTO.setApplicationIndicator(AppIndicatorEnum.APP_IBE);

						if (isBrokerTypeInternalExternal) {

							List<CardDetailConfigDTO> cardDetailConfigData = paymentBrokerBD
									.getPaymentGatewayCardConfigData(ipgIdentificationParamsDTO.getIpgId().toString());

							ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
						} else {
							ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);
						}

						String strRequestData = ipgRequestResultsDTO.getRequestData();

						ibeReservationPostPaymentDTO.setPnr(productID);
						ibeReservationPostPaymentDTO.setIpgRefenceNo(ipgRequestResultsDTO.getAccelAeroTransactionRef());

						request.getSession().setAttribute(AeroMartPayConstants.PAYMENT_BROKER_REF_NO,
								ipgRequestResultsDTO.getPaymentBrokerRefNo());
						request.getSession().setAttribute(AeroMartPayConstants.PAYMENT_GATEWAY_ID, paymentgatewayId);
						request.getSession().setAttribute(AeroMartPayConstants.RESPONSE_RECEIVED, false);

						if (responseTypeXML != null && !"".equals(responseTypeXML) && "true".equals(responseTypeXML)) {
							log.debug("Execute the payment and get the response for XML response type PGWs");
							XMLResponseDTO response = paymentBrokerBD.getXMLResponse(
									ipgRequestDTO.getIpgIdentificationParamsDTO(), ipgRequestResultsDTO.getPostDataMap());
							log.debug("Recieved XMLresponse");
							ArrayList<Integer> tptIds = new ArrayList<Integer>();
							tptIds.add(temporyPayTnx.getTnxId());
							log.debug("Redirecting depending on the response");
							ibeReservationPostPaymentDTO.setSecurePayment3D(false);
							if (response != null && Integer.valueOf(response.getStatus()) == 46) {
								// add to session
								if (log.isDebugEnabled()) {
									log.debug("3D Secure payment: PNR:" + productID + "SessionID :"
											+ request.getRequestedSessionId());
								}
								ibeReservationPostPaymentDTO.setSwitchToExternalURL(true);
								ibeReservationPostPaymentDTO.setSecurePayment3D(true);

								if (!StringUtil.isNullOrEmpty(response.getHtml())) {
									request.setAttribute(AeroMartPayConstants.BANKS_3D_SECURE_HTML_PAGE, response.getHtml());
									result = StrutsConstants.Result.AEROMART_PAY_IPG_REDIRECTION;

								} else {
									aeroMartPayResponse = AeroMartPayResponse.ERR_80;
									log.error("3D-Secure XML response contains empty HTML, GoQUO orderID "
											+ sessionInfo.getOrderID());
									throw new ModuleException(aeroMartPayResponse.getRespMessage());
								}

							} else if (response != null && !response.getStatus().equals("")) {
								request.setAttribute(WebConstants.XML_RESPONSE_PARAMETERS, response);
								result = StrutsConstants.Action.AEROMART_PAY_BYPASS;
							}
						} else if (PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL.equals(brokerType)
								&& !StringUtil.isNullOrEmpty(strRequestData)) {

							request.setAttribute(AeroMartPayConstants.FORM_STRING, strRequestData);
							request.setAttribute(AeroMartPayConstants.FORM_NAME, AeroMartPayConstants.PAYMENT_FORM_NAME);

							result = StrutsConstants.Result.AEROMART_PAY_EXT_IPG_REDIRECTION;
						} else {
							aeroMartPayResponse = AeroMartPayResponse.ERR_58;
							throw new ModuleException(aeroMartPayResponse.getRespMessage());
						}

					}
				}

				else {
					log.error(messages.get(0) + ", orderID " + sessionInfo.getOrderID());

					if (AeroMartPayUtil.incrementRetrails(session)) {

						log.info("Recreating the Payment Page");

						if (!AeroMartPayUtil.sessionHandlePaymentLock(session, false)) {
							log.error("Multiple Payment Details Submission, Concurernt LOCK Access, orderID: "
									+ sessionInfo.getOrderID());
							aeroMartPayResponse = AeroMartPayResponse.ERR_108;
							log.error(aeroMartPayResponse.getRespMessage() + ", orderID: " + sessionInfo.getOrderID());
							throw new ModuleException(aeroMartPayResponse.getRespMessage());
						}

						request.getSession().setAttribute(AeroMartPayConstants.MESSAGE, messages.get(0) + ": Retry");

						request.setAttribute(AeroMartPayConstants.LOAD_PAYMENT_OPTIONS,
								AeroMartPayConstants.LOAD_PAYMENT_OPTIONS_URL);

						result = StrutsConstants.Result.LOAD_AEROMARTPAY_PAYMENT_OPTIONS;

					} else {
						aeroMartPayResponse = AeroMartPayResponse.ERR_201;
						throw new ModuleException(aeroMartPayResponse.getRespMessage());
					}

				}

			} catch (Exception e) {
				if (aeroMartPayResponse == null) {
					aeroMartPayResponse = AeroMartPayResponse.ERR_81;

				}

				log.error(aeroMartPayResponse.getRespMessage(), e);

				AeroMartPayUtil.setPaymentResponseAttributes(request, sessionInfo.getOrderID(), sessionInfo.getErrorURL(),
						aeroMartPayResponse.getRespCode(), aeroMartPayResponse.getRespMessage(), sessionInfo.getReferenceID(),
						sessionInfo.getTotalAmount());

				AeroMartPayUtil.invalidateSession(request);

				result = StrutsConstants.Result.AEROMART_PAY_RESPONSE;
			}

		}

		return result;
	}

	private BigDecimal getExchangeAmount(BigDecimal paymentAmount, CurrencyExchangeRate currencyExrate, boolean toBaseCurr) {
		if (paymentAmount != null && paymentAmount.doubleValue() > 0 && currencyExrate != null) {
			return convertToPaymentCurrency(paymentAmount, currencyExrate, toBaseCurr);
		}
		return null;
	}

	private BigDecimal convertToPaymentCurrency(BigDecimal effectivePaymentAmount, CurrencyExchangeRate currencyExrate,
			boolean toBaseCurr) {
		if (toBaseCurr) {
			Currency pgCurrency = currencyExrate.getCurrency();
			return AccelAeroRounderPolicy.convertAndRoundToBase(effectivePaymentAmount,
					currencyExrate.getMultiplyingExchangeRate(), pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint());
		} else {
			Currency pgCurrency = currencyExrate.getCurrency();
			return AccelAeroRounderPolicy.convertAndRound(effectivePaymentAmount, currencyExrate.getMultiplyingExchangeRate(),
					pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint());
		}
	}

	private BigDecimal convertAmount(BigDecimal amount, String fromCurr, String toCurr) throws ModuleException {
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		String baseCurr = AppSysParamsUtil.getBaseCurrency();
		BigDecimal toCurrAmount = amount;

		if (fromCurr.equals(toCurr)) {
			toCurrAmount = amount;
		} else if (fromCurr.equals(baseCurr)) {
			CurrencyExchangeRate currExrateToCurr = exchangeRateProxy.getCurrencyExchangeRate(toCurr,
					ApplicationEngine.IBE);
			toCurrAmount = getExchangeAmount(amount, currExrateToCurr, false);
		} else {
			CurrencyExchangeRate currExrateToBaseCurr = exchangeRateProxy.getCurrencyExchangeRate(fromCurr,
					ApplicationEngine.IBE);
			BigDecimal fromCurrAmountInBaseCurrency = getExchangeAmount(amount, currExrateToBaseCurr, true);
			CurrencyExchangeRate currExrateToCurr = exchangeRateProxy.getCurrencyExchangeRate(toCurr,
					ApplicationEngine.IBE);
			toCurrAmount = getExchangeAmount(fromCurrAmountInBaseCurrency, currExrateToCurr, false);
		}

		return toCurrAmount;
	}

	private BigDecimal getPaymentAmount(BigDecimal paymentAmount, String userCurrency, String pgCurrency) throws ModuleException {
		return convertAmount(paymentAmount, userCurrency, pgCurrency);
	}

	private String getAgentType(Map<String, String> requestParams) {
		String agentType = requestParams.get(AeroMartPayConstants.PARAM_AGENT_TYPE);
		if (agentType == null || "".equals(agentType)) {
			agentType = AeroMartPayConstants.AGENT;
		}
		return agentType;
	}
	
	/**
	 * Create credit card payment
	 * 
	 * @param cardPaymentInfo
	 * @return
	 * @throws ModuleException
	 */
	public static CreditCardPayment createCreditCardPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		CreditCardPayment creditCardPayment = new CreditCardPayment();

		// paypal
//		if (cardPaymentInfo.getUserInputDTO() != null) {
//			creditCardPayment.setUserInputDTO(cardPaymentInfo.getUserInputDTO());
//		}

		creditCardPayment.setCardholderName(ipgRequestDTO.getHolderName());
		creditCardPayment.setCardNumber(ipgRequestDTO.getCardNo());
		creditCardPayment.setExpiryDate(ipgRequestDTO.getExpiryDate());
		
		creditCardPayment.setAmount(ipgRequestDTO.getAmount());
		creditCardPayment.setCurrency(AppSysParamsUtil.getBaseCurrency());

//		int noOfDecimalPoints = BeanUtils.countNoOfDecimals(payCurrencyDTO.getBoundaryValue());
//		if (noOfDecimalPoints > 0) {
//			creditCardPayment.setNoOfDecimalPoints(noOfDecimalPoints);
//		}

		creditCardPayment.setCvvField(ipgRequestDTO.getSecureCode());
		creditCardPayment.setPnr(ipgRequestDTO.getPnr());
		creditCardPayment.setAppIndicator(ipgRequestDTO.getApplicationIndicator());
		
		//hard corded
		creditCardPayment.setTnxMode(TnxModeEnum.MAIL_TP_ORDER);
		
		creditCardPayment.setTemporyPaymentId(ipgRequestDTO.getApplicationTransactionId());
		creditCardPayment.setIpgIdentificationParamsDTO(ipgRequestDTO.getIpgIdentificationParamsDTO());
		creditCardPayment.setCardType(new Integer(ipgRequestDTO.getCardType()));
		creditCardPayment.setTravelDTO(ipgRequestDTO.getTravelDTO());
		creditCardPayment.setUserIP(ipgRequestDTO.getUserIPAddress());
		return creditCardPayment;
	}
	
	private static IPGIdentificationParamsDTO prepareIPGIdentificationParamsDTO(String payCurrencyCode, Integer ipgId)
			throws ModuleException {

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, payCurrencyCode);
		boolean isPGExists = PromotionModuleServiceLocator.getPaymentBrokerBD().checkForIPG(ipgIdentificationParamsDTO);
		if (AppSysParamsUtil.isXBECreditCardPaymentsEnabled() && !isPGExists) {
			throw new ModuleException("error.cardpay.ipgconfig.notfound");
		}
		return ipgIdentificationParamsDTO;
	}

}
