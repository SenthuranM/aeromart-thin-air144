package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.constants.SocialCustomerType;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.dto.CustomerQuestionaireDTO;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CustomerRegisterAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(CustomerRegisterAction.class);

	private boolean success = true;

	private String messageTxt;

	private CustomerDTO customer;

	private LmsMemberDTO lmsDetails;

	private String familyHead = "Y";

	private String refferedEmail = "Y";

	private List<CustomerQuestionaireDTO> questionaire;

	private String optLMS;

	private boolean autoLogin = false;

	private boolean lmsNameMismatch = false;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;

		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			LoyaltyCustomerServiceBD loyaltyCustomerBD = ModuleServiceLocator.getLoyaltyCustomerBD();
			if (questionaire != null) {
				customer.setCustomerQuestionaireDTO(questionaire);
			}
			Customer modelCustomer = CustomerUtilV2.getModelCustomer(customer);
			String accountNo = customer.getLoyaltyAccountNumber();
			LoyaltyCustomerProfile loyaltyProfile = null;

			// if (!AppSysParamsUtil.isLMSEnabled()) {
			if (accountNo != null && accountNo.length() > 0) {
				LoyaltyCustomerProfile loyaltyCustomerProfile = CustomerUtilV2
						.getLoyaltyCustomerProfile(modelCustomer, accountNo);
				loyaltyProfile = loyaltyCustomerBD.getLoyaltyCustomerByExample(loyaltyCustomerProfile);

				if (loyaltyProfile == null)
					throw new ModuleException("aircustomer.loyalty.account.not.exist");
				else {
					if (loyaltyProfile.getCustomerId() != null)
						throw new ModuleException("aircustomer.loyalty.account.already.linked");
				}
			}
			// / }

			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			if (AppSysParamsUtil.isLMSEnabled()) {
				if (optLMS.equals("Y")) {
					LoyaltyMemberCoreDTO member = loyaltyManagementBD.getLoyaltyMemberCoreDetails(lmsDetails.getHeadOFEmailId());
					if (!LmsCommonUtil.isValidFamilyHeadAccount(lmsDetails.getHeadOFEmailId(), member)) {
						familyHead = "N";
					}
					if (!LmsCommonUtil.isNonMandAccountAvailable(lmsDetails.getRefferedEmail(), loyaltyManagementBD)) {
						refferedEmail = "N";
					}
				}
				// removed loyalty enrollment
			}

			customerDelegate.registerCustomer(modelCustomer, SessionUtil.getCarrier(request), loyaltyProfile, false);

			int customerId = customerDelegate.getCustomer(modelCustomer.getEmailId()).getCustomerId();

			if (isUpdateSocialProfiles()) {
				String socialSiteType = customer.getSocialCustomerTypeId() + "";
				Customer customerToBeMerged = customerDelegate.getCustomer(modelCustomer.getEmailId());
				Set<SocialCustomer> existingSocialCustomerProfiles = customerToBeMerged.getSocialCustomer();

				if (existingSocialCustomerProfiles == null) {
					existingSocialCustomerProfiles = new HashSet<SocialCustomer>();
				}

				boolean alreadyRegistered = false;
				for (SocialCustomer existingProfile : existingSocialCustomerProfiles) {
					String existingSiteType = null;
					if (existingProfile.getSocialCustomerTypeId() != null) {
						existingSiteType = existingProfile.getSocialCustomerTypeId().toString();
					}

					if (existingSiteType != null && socialSiteType.equals(existingSiteType)) {
						alreadyRegistered = true;
						break;
					}
				}

				if (!alreadyRegistered) {
					SocialCustomer socialProfileToMerge = new SocialCustomer();
					socialProfileToMerge.setCustomerId(new Integer(customerToBeMerged.getCustomerId()));
					socialProfileToMerge.setSocialSiteCustId(customer.getSocialSiteCustId());
					socialProfileToMerge.setSocialCustomerTypeId(new Integer(socialSiteType));

					existingSocialCustomerProfiles.add(socialProfileToMerge);
					customerToBeMerged.setSocialCustomer(existingSocialCustomerProfiles);
					customerDelegate.saveOrUpdate(customerToBeMerged);
				}
				customer.setSocialSiteCustId(null);
				customer.setSocialCustomerTypeId(-1);
			}

			if (AppSysParamsUtil.isLMSEnabled()) {
				if (optLMS.equals("Y") && familyHead.equals("Y") && refferedEmail.equals("Y")) {
					Locale locale = new Locale(SessionUtil.getLanguage(request));
					lmsDetails.setFirstName(customer.getFirstName());
					lmsDetails.setLastName(customer.getLastName());
					lmsDetails.setMobileNumber(customer.getMobile());
					lmsDetails.setPhoneNumber(customer.getTelephone());
					lmsDetails.setNationalityCode(customer.getNationalityCode());
					lmsDetails.setResidencyCode(customer.getCountryCode());
					LmsMember lmsMember = CustomerUtilV2.getModelLmsMember(lmsDetails, customerId);
					LmsCommonUtil.lmsEnroll(lmsMember, SessionUtil.getCarrier(request), loyaltyManagementBD, lmsMemberDelegate,
							locale, false);
					lmsDetails.setEmailStatus("N");
				}
			}

			if (!AppSysParamsUtil.isCustomerRegiterConfirmation() && AppSysParamsUtil.isAutoLoginAfterRegistration()) {
				Customer persistedCust = customerDelegate.getCustomer(modelCustomer.getEmailId());
				SessionUtil.setCustomerId(request, persistedCust.getCustomerId());
				SessionUtil.setCustomerEMail(request, persistedCust.getEmailId());
				SessionUtil.setCustomerPassword(request, persistedCust.getPassword());

				if (persistedCust.getLMSMemberDetails() != null) {
					SessionUtil.setLoyaltyFFID(request, persistedCust.getLMSMemberDetails().getFfid());
				}

				autoLogin = true;
			}

			this.customer = null;
			this.questionaire = null;
		} catch (ModuleException me) {
			log.error("CustomerRegisterAction==>", me);
			if (me.getExceptionCode().equals("aircustomer.logic.loginid.already.exist")) {
				success = true;
				messageTxt = I18NUtil.getMessage(CustomerWebConstants.CUSTOMER_EXISTS, SessionUtil.getLanguage(request));
			} else if (me.getExceptionCode().equals("aircustomer.loyalty.account.not.exist")) {
				success = true;
				messageTxt = I18NUtil.getMessage(CustomerWebConstants.LOYALTY_ACCOUNT_NOT_EXISTS,
						SessionUtil.getLanguage(request));
			} else if (me.getExceptionCode().equals("aircustomer.loyalty.account.already.linked")) {
				success = true;
				messageTxt = I18NUtil.getMessage(CustomerWebConstants.LOYALTY_ACCOUNT_ALREADY_LINKED,
						SessionUtil.getLanguage(request));
			} else {
				success = false;
				messageTxt = me.getMessageString();
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("CustomerRegisterAction==>", ex);
		}
		return forward;

	}

	public String checkRegCustomer() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			Customer customergot = customerDelegate.getCustomer(customer.getEmailId());

			if (customergot != null && customergot.getCustomerId() != 0) {
				success = true;
				messageTxt = "true";
			} else {
				success = false;
				messageTxt = I18NUtil.getMessage("msg.user.pwd.forgot.Error", SessionUtil.getLanguage(request));
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("CustomerRegisterAction==>", ex);
		}
		return forward;
	}

	public String sendlmsConfEmail() {
		String forward = StrutsConstants.Result.SUCCESS;
		Locale locale = new Locale(SessionUtil.getLanguage(request));
		LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
		LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
		try {
			LmsMember lmsMember = lmsMemberDelegate.getLmsMember(SessionUtil.getLoyaltyFFID(request));
			String validationString = lmsMemberDelegate.getVerificationString(lmsMember.getFfid());
			LmsCommonUtil.resendLmsConfEmail(lmsMember, SessionUtil.getCarrier(request), validationString, lmsMemberDelegate,
					loyaltyManagementBD, locale, false);
		} catch (ModuleException me) {
			log.error("CustomerRegisterAction==>", me);
		}
		return forward;
	}

	private boolean isUpdateSocialProfiles() {
		boolean update = false;
		update = customer != null && customer.getSocialSiteCustId() != null && !customer.getSocialSiteCustId().isEmpty()
				&& customer.getSocialCustomerTypeId() > 0;
		return update;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public void setQuestionaire(List<CustomerQuestionaireDTO> questionaire) {
		this.questionaire = questionaire;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public List<CustomerQuestionaireDTO> getQuestionaire() {
		return questionaire;
	}

	public boolean isAutoLogin() {
		return autoLogin;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public String getOptLMS() {
		return optLMS;
	}

	public void setOptLMS(String optLMS) {
		this.optLMS = optLMS;
	}

	public String getFamilyHead() {
		return familyHead;
	}

	public void setFamilyHead(String familyHead) {
		this.familyHead = familyHead;
	}

	public String getRefferedEmail() {
		return refferedEmail;
	}

	public void setRefferedEmail(String refferedEmail) {
		this.refferedEmail = refferedEmail;
	}

	public boolean isLmsNameMismatch() {
		return lmsNameMismatch;
	}

}
