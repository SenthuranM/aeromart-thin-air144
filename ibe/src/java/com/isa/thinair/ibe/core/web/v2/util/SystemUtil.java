package com.isa.thinair.ibe.core.web.v2.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.api.dto.SystemDefaultParamDTO;
import com.isa.thinair.ibe.core.service.IBEConfig;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
public class SystemUtil {

	private static Log log = LogFactory.getLog(SystemUtil.class);

	private static GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
	private static IBEConfig ibeConfig = IBEModuleUtils.getConfig();
	private static final String DEFAULT_LANGUAGE = "en";

	/**
	 * Get System Default information
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	// To Do: Remove unused variables
	public static SystemDefaultParamDTO getSystemDefaultParams(HttpServletRequest request) {

		SystemDefaultParamDTO sysParam = new SystemDefaultParamDTO();
		Date dtCurrent = new Date();
		SimpleDateFormat smpDtFormat = new SimpleDateFormat("dd/MM/yyyy");

		String strCarrier = SessionUtil.getCarrier(request);

		sysParam.setFlightSeachMaxAdult(globalConfig.getBizParam(SystemParamKeys.MAX_ADULT_COUNT_ALLOWED_FOR_RESERVATION, strCarrier).split(",")[0].split("=")[1]);
		sysParam.setCurrentDate(smpDtFormat.format(dtCurrent));
		sysParam.setFlightSearchDeptVariance(globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_IBE, strCarrier));
		sysParam.setFlightSearchDeptVarianceDifference(globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_DEF_IBE,
				strCarrier));
		sysParam.setHub(globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM, strCarrier));
		sysParam.setBaseCurrency(globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY, strCarrier));
		sysParam.setAdultCode(ReservationInternalConstants.PassengerType.ADULT.toString());
		sysParam.setChildCode(ReservationInternalConstants.PassengerType.CHILD);
		sysParam.setInfantCode(ReservationInternalConstants.PassengerType.INFANT.toString());
		sysParam.setInsuranceCharged(globalConfig.getBizParam(SystemParamKeys.INSURCHG, strCarrier).toString());
		sysParam.setDepDateDiffrence(globalConfig.getBizParam(SystemParamKeys.DEPT_DATE, strCarrier));

		if (!globalConfig.getBizParam(SystemParamKeys.RETU_DATE).toString().equals("-1")) {
			sysParam.setRetDateDiffrence(globalConfig.getBizParam(SystemParamKeys.RETU_DATE, strCarrier).toString());
		} else {
			sysParam.setRetDateDiffrence("");
		}
		sysParam.setInfantCutOffAgeInYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getCutOffAgeInYears());
		sysParam.setSystemDecimal(globalConfig.getBizParam(SystemParamKeys.DECIMALS, strCarrier).toString());
		sysParam.setFaqURL(globalConfig.getBizParam(SystemParamKeys.FAQ_URL).toString());

		if (request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT) != null) {
			String strAccessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);
			if (strAccessPoint.equals(CustomerWebConstants.SYS_ACCESS_POINT_KSK)) {
				sysParam.setKioskHomePageURL(globalConfig.getBizParam(SystemParamKeys.KIOSK_HOME_PAGE_URL));
			} else {
				sysParam.setAirLineURL(globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, strCarrier));
			}
		} else {
			sysParam.setAirLineURL(globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, strCarrier));
		}

		sysParam.setSysLanguage(globalConfig.getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));

		sysParam.setClassOfService(globalConfig.getBizParam(SystemParamKeys.CLASS_OF_SERVICE, strCarrier));
		sysParam.setCarrierName(globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME, strCarrier));
		sysParam.setChildCutOffAgeInYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears());
		sysParam.setAdultCutOffAgeInYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears());
		sysParam.setLoyaltyEnable(AppSysParamsUtil.isLoyalityEnabled());
		sysParam.setDuplicateNameCheckEnable(AppSysParamsUtil.getDuplicateNameCheckEnabled());
		sysParam.setDynamicOndListEnabled(AppSysParamsUtil.isDynamicOndListPopulationEnabled());
		sysParam.setAllowedMaxNoONDMulticitySearch(AppSysParamsUtil.getMulticitySearchMaximumNoONDIBE());

		sysParam.setCreditCardPaymentsEnabled(AppSysParamsUtil.isCreditCardPaymentsEnabled());
		sysParam.setReservationOnholdEnabled(AppSysParamsUtil.isReservationOnholdEnabled());
		sysParam.setCOSSelectionEnabled(AppSysParamsUtil.isCOSSelectionEnabled());
		sysParam.setAcclaeroClientIdentifier(CommonsServices.getGlobalConfig().getBizParam(
				SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER));
		sysParam.setShowAlertsInIBEEnabled(AppSysParamsUtil.isShowAlertsInIBEEnabled());
		sysParam.setShowOperatingCarrierLegend("Y".equals(globalConfig.getBizParam(SystemParamKeys.SHOW_OPERATING_CARRIER_LEGEND))
				? "true"
				: "false");

		sysParam.setSecureIBEUrl(AppParamUtil.getSecureIBEUrl());
		sysParam.setNonsecureIBEUrl(AppParamUtil.getNonsecureIBEUrl());
		sysParam.setKioskAccessPoint(isKioskUser(request));
		sysParam.setCalendarGradianView(AppSysParamsUtil.isCalendarGradiantView());
		sysParam.setCalendarViews(AppSysParamsUtil.getCalendarViews());
		sysParam.setCalendarType(AppSysParamsUtil.getCalendarType());
		sysParam.setImageCapchaDisAvalSearch(AppSysParamsUtil.getImageCapchaDisplayAvailabilitySearch());
		sysParam.setModSegBalAvalSearch(AppSysParamsUtil.isModifySegmentBalanceDisplayAvailabilitySearch());
		sysParam.setSessionTimeOutDisplay(AppSysParamsUtil.getSessionTimeOutDisplay());
		sysParam.setCustomerRegiterConfirmation(AppSysParamsUtil.isCustomerRegiterConfirmation());

		String transferTimeHHMM = globalConfig.getBizParam(SystemParamKeys.MIN_RETURN_TRANSITION_TIME).toString();
		int durationMinutes = CommonUtil.getTotalMinutes(transferTimeHHMM);
		sysParam.setMinReturnTransitionTime(String.valueOf(durationMinutes * 60 * 1000));
		sysParam.setDefaultCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		sysParam.setAutoSeatAssignmentEnabled(AppSysParamsUtil.isAutomaticSeatAssignmentEnabled());
		sysParam.setAutoSeatAssignmentStartingRowNumber(AppSysParamsUtil.getAutoSeatAssignmentStartingRowNumberForIBE());
		sysParam.setAllowModifyBookingsToLowPriorityCOS(AppSysParamsUtil.isIBEAllowModifyBookingsToLowPriorityCOS());

		sysParam.setPromoCodeEnabled(AppSysParamsUtil.isPromoCodeEnabled());
		sysParam.setCookieChargesApplicable(AppSysParamsUtil.isCookieBasedSurchargesApplyForIBE());
		sysParam.setNoOfCookieExpiryDays(ibeConfig.getCookieExpiryDays());
		sysParam.setExitPopupEnable(AppSysParamsUtil.isEnableExitPopupForIBE());
		sysParam.setUserDefineTransitTimeEnabled(AppSysParamsUtil.isEnableUserDefineTransitTime());
		sysParam.setExitPopupDisplayTimeGap(AppSysParamsUtil.getExitPopupTimeGapForDisaplay());
		sysParam.setExitPopupEnableStep(AppSysParamsUtil.getIbeExitPopupEnableStep());
		sysParam.setCarrierUrlsIBE(globalConfig.getCarrierUrlsIBE());
		sysParam.setSaveCookieEnable(AppSysParamsUtil.isCookieSaveFunctionalityEnabled());
		sysParam.setAddMobileAreaCodePrefix(AppSysParamsUtil.addMobileAreaCodePrefix());
		sysParam.setFQTVApplicableForAllSegments(AppSysParamsUtil.applyFQTVInAllSegments());
		sysParam.setHumanVerificationEnabled(AppSysParamsUtil.isIBEHumanVerficationEnabled());
		sysParam.setSkipSegmentCodeInLabelsForOtherLanguages(ibeConfig.isSkipSegmentCodeInLabelsForOtherLanguages());
		sysParam.setDisplayNotificationForLastNameInIBE(AppSysParamsUtil.isDisplayNotificationForLastNameInContactDetailsInIBE());
		sysParam.setEnableStopOverInIBE(AppSysParamsUtil.isEnableStopOverInIBE());
		sysParam.setAllowSameFlightModificationInIBE(AppSysParamsUtil.isAllowSameFlightModificationInIBE());
		sysParam.setSkipPersianDateChange(AppSysParamsUtil.skipPersianDateChangeInIBE());
		sysParam.setClientCallCenterURL(AppSysParamsUtil.getClientCallCenterURL());
		
		return sysParam;
	}

	/**
	 * Set Common Parameters
	 * 
	 * @param request
	 * @param params
	 */
	public static void setCommonParameters(HttpServletRequest request, IBECommonDTO params) {

		boolean isRegUser = false;
		int customerID = SessionUtil.getCustomerId(request);

		if (customerID >= 0) {
			isRegUser = true;
		}

		params.setRegCustomer(isRegUser);
		params.setHomeURL(globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, SessionUtil.getCarrier(request)));
		if (SessionUtil.getLanguage(request) == null) {
			params.setLocale(DEFAULT_LANGUAGE);
		} else {
			params.setLocale(SessionUtil.getLanguage(request));
		}
		params.setNonSecurePath(AppParamUtil.getNonsecureIBEUrl());
		params.setSecurePath(AppParamUtil.getSecureIBEUrl());
		params.setKioskAccessPoint(isKioskUser(request));
		params.setSkipPersianCalendarChange(AppSysParamsUtil.skipPersianDateChangeInIBE());
		// Get user country code from requesting IP
		String clientIp = WebplatformUtil.getClientIpAddress(request);
		long originIp = PlatformUtiltiies.getOriginIP(clientIp);
		try {
			Country country = CommonUtil.getCountryFromIPAddress(originIp);
			if (country != null) {
				params.setPointOfSale(country.getCountryCode());
			} else {
				log.info("POS identification failed. No country configured for IP " + originIp);
			}
		} catch (ModuleException e) {
			log.error("Error retriving country by IP address for POS ", e);
		}
	}

	/**
	 * Get Carrier Name
	 * 
	 * @param request
	 * @return
	 */
	public static String getCarrierName(HttpServletRequest request) {
		return globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME);
	}

	/**
	 * Is Kiosk User
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isKioskUser(HttpServletRequest request) {
		boolean isKioskUser = false;
		HttpSession reqsession = request.getSession();
		String accessPoint = (String) reqsession.getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);
		if (CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint)) {
			isKioskUser = true;

			User user = (User) reqsession.getAttribute(CustomerWebConstants.REQ_KSK_USER);

			if (user == null) {
				isKioskUser = false;
			}

		}

		return isKioskUser;
	}
	
	public static void createRedirectFormData(HttpServletRequest request, String redirectURL,
			Map<String, String> formHiddenDataMap, boolean isTargetTop) {
		if (formHiddenDataMap == null) {
			formHiddenDataMap = new HashMap<String, String>();
		}
		formHiddenDataMap
				.put(ReservationWebConstnts.REQ_FORM_RANDOM_ID, String.valueOf(Calendar.getInstance().getTimeInMillis()));
		request.setAttribute(ReservationWebConstnts.REQ_FORM_REDIRECT_URL, redirectURL);
		request.setAttribute(ReservationWebConstnts.REQ_FORM_HIDDEN_DATAMAP, formHiddenDataMap);
		request.setAttribute(ReservationWebConstnts.REQ_FORM_IS_TARGET_TOP, isTargetTop);
	}
}
