package com.isa.thinair.ibe.core.web.v2.action.customer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CustomerForgetPasswordAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(CustomerForgetPasswordAction.class);

	private boolean success = true;

	private String messageTxt;

	private CustomerDTO customer;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			boolean blnStatus = customerDelegate.getForgotPassword(customer.getEmailId(), customer.getSecretQuestion(),
					customer.getSecretAnswer(), SessionUtil.getCarrier(request));
			if (blnStatus) {
				messageTxt = "true";
			} else {
				messageTxt = I18NUtil.getMessage("msg.user.pwd.forgot.Error", SessionUtil.getLanguage(request));
			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("CustomerForgetPasswordAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("CustomerForgetPasswordAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

}
