package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAddressDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.ibe.api.dto.AncillaryPaxTo;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.PassengerDTO;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.api.dto.SocialTO;
import com.isa.thinair.ibe.api.dto.URLTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.MessageUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.DateUtil;
import com.isa.thinair.ibe.core.web.v2.util.FlyingSocialUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciInitAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(InterlineAnciInitAction.class);

	private boolean success = true;

	private String messageTxt;

	private boolean seatMapAvailable = false;

	private boolean mealsAvailable = false;

	private boolean baggagesAvailable = false;

	private boolean insuranceAvailable = false;
	// To chnage the view of the IBE insurance display alone with the country
	private boolean insuranceViewChanged = false;

	private boolean airportServicesAvailable = false;

	private boolean airportTransferAvailable = false;

	private boolean ssrAvailable = false;

	private boolean blnReturn = false;

	private String insuranceSelectedByDefault = "0";

	private int childCount = 0;

	private int adultCount = 0;

	private int infantCount = 0;

	private List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO;

	private Collection<SegInfoDTO> flightSegments;

	private URLTO urlInfo;

	private List<LCCInsuranceQuotationDTO> insuranceDTO;

	private ContactInfoDTO contactInfo = new ContactInfoDTO();

	private Collection<PassengerDTO> adultList = new ArrayList<PassengerDTO>();

	private Collection<PassengerDTO> childList = new ArrayList<PassengerDTO>();

	private Collection<PassengerDTO> infantList = new ArrayList<PassengerDTO>();

	private String pnr;

	@Deprecated
	private boolean allowInsModify = false;

	@Deprecated
	private boolean rakEnabled = false;

	private boolean groupPNR;

	private boolean modifySegment;

	private final boolean showSSRByDefault = false;

	private String oldAllSegments;

	private boolean hasInsurance;

	private String modifySegmentRefNos;

	private boolean addGroundSegment;

	private String resPaxInfo;

	private String paxJson;

	private SocialTO socialSeatingParams = new SocialTO();

	private Map<String, LogicalCabinClassDTO> lccMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();

	private boolean jnTaxApplicable;

	private double jnTaxRatio;

	private String modifingFlightInfo;

	private boolean dateModification;

	private DiscountedFareDetails discountInfo;

	private double flexiCharge;

	private boolean isFlexiEnableInAnci;

	private Collection<AncillaryPaxTo> paxWiseAnci;

	private boolean applyAncillaryPenalty;

	private List<BundledFareDTO> selectedBundledFareDTOs;

	private String jsonOnds;

	private Set<String> busCarrierCodes;

	public String execute() throws ModuleException {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
			if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
					&& !tempPaymentPNR.trim().isEmpty()) {
				messageTxt = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
				success = false;
				return result;
			}
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			if (ibeResInfo == null) {
				log.info("IBEReservationInfoDTO is null");
				throw new ModuleException(ExceptionConstants.SERVER_OPERATION_FAIL);
			} else {
				selectedBundledFareDTOs = ibeResInfo.getSelectedBundledFares();
			}
			// boolean isInterlineModifySegment = groupPNR && modifySegment ? true : false;
			BigDecimal totalTicketPriceWithoutExternal = null;
			if (ibeResInfo != null && ibeResInfo.getTotalPriceWithoutExtChg() != null) {
				totalTicketPriceWithoutExternal = new BigDecimal(ibeResInfo.getTotalPriceWithoutExtChg());
			}

			String strTxnIdntifier = ibeResInfo.getTransactionId();

			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}

			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams(),
					(modifySegment && AppSysParamsUtil.isRequoteEnabled()));

			SelectedFltSegBuilder totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);

			// When adding bus segment, insurance will be quoted for whole journey
			if (addGroundSegment) {

				totalFltSegBuilder.getSelectedFlightSegments().addAll(fltSegBuilder.getSelectedFlightSegments());

			}

			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			List<FlightSegmentTO> insurableFlightSegmentTOs = fltSegBuilder.getAllFlightSegments();
			SYSTEM system = com.isa.thinair.airproxy.api.utils.CommonUtil.deriveSystem(flightSegmentTOs);
			if (groupPNR) {
				system = SYSTEM.INT;
			}
			ibeResInfo.setSelectedSystem(system);

			boolean isCancelSegment = false;

			ReservationUtil.setCorrectOndSequence(getSearchParams(), flightSegmentTOs);

			boolean isModifyOperation = false;
			if (pnr != null && pnr.indexOf(",") > 0) {
				pnr = pnr.split(",")[0];
				isModifyOperation = true;
			}

			LCCInsuredJourneyDTO insJrnyDto = null;

			InsuranceFltSegBuilder insFltSegBldr;

			if (!addGroundSegment) {
				insFltSegBldr = new InsuranceFltSegBuilder(insurableFlightSegmentTOs);
			} else {
				List<FlightSegmentTO> insurableFlightSegments = totalFltSegBuilder.getSelectedFlightSegments();
				if (!SYSTEM.INT.equals(system)) {
					system = com.isa.thinair.airproxy.api.utils.CommonUtil.deriveSystem(insurableFlightSegments);
				}
				insFltSegBldr = new InsuranceFltSegBuilder(insurableFlightSegments);
			}
			insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(insFltSegBldr.getInsurableFlightSegments(),
					insFltSegBldr.isReturnJourney(), getTrackInfo().getOriginChannelId());

			if (resPaxInfo != null && !resPaxInfo.equals("") && adultList.size() == 0) {
				List<Collection<PassengerDTO>> paxList = ReservationUtil.transformJsonPassengerList(resPaxInfo);
				childList = paxList.get(0);
				adultList = paxList.get(1);
				infantList = paxList.get(2);
			} else if (paxJson != null && !paxJson.equals("") && adultList.size() == 0) {
				List<Collection<PassengerDTO>> paxList = ReservationUtil.transformJsonPassengerAnciModifyList(paxJson);
				childList = paxList.get(0);
				adultList = paxList.get(1);
				infantList = paxList.get(2);
			}

			Collection<PassengerDTO> paxAdultChildList = new ArrayList<PassengerDTO>();
			paxAdultChildList.addAll(adultList);
			paxAdultChildList.addAll(childList);

			// This needs CLEAN UP: encapsulate insurance related operations
			List<LCCInsuredPassengerDTO> insPax = compileInsuredPaxDTOList(paxAdultChildList, contactInfo);

			if (AppSysParamsUtil.allowAddInsurnaceForInfants() && infantList != null && !infantList.isEmpty()) {
				insPax.addAll(compileInsuredPaxDTOList(infantList, contactInfo));
			}

			LCCInsuredContactInfoDTO insurContactInfo = getLCCInsuranceContactInfo(contactInfo);

			if (!hasInsurance) {
				if (ibeResInfo.getPriceInfoTO() != null) {
					AnciOfferUtil.populateMissingDataForAnciOfferSearch(ibeResInfo.getPriceInfoTO(),
							insFltSegBldr.getInsurableFlightSegments(), totalFltSegBuilder.isReturn(),
							totalFltSegBuilder.getSelectedFlightSegments());
				} else {

					List<FlightSegmentTO> oldFlightSegmentTOs = totalFltSegBuilder.getSelectedFlightSegments();
					AnciOfferUtil.populateMissingDataForAnciOfferSearch(oldFlightSegmentTOs,
							insFltSegBldr.getInsurableFlightSegments(), searchParams.getAdultCount(),
							searchParams.getChildCount(), searchParams.getInfantCount(), totalFltSegBuilder.isReturn());
				}

				insJrnyDto.setSelectedLanguage(SessionUtil.getLanguage(request));

				insuranceDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getInsuranceQuotation(
						insFltSegBldr.getInsurableFlightSegments(), insPax, insJrnyDto, strTxnIdntifier, system, pnr,
						getClientInfoDTO(), totalTicketPriceWithoutExternal, TrackInfoUtil.getBasicTrackInfo(request),
						ApplicationEngine.IBE, insurContactInfo);
			}

			insuranceSelectedByDefault = String.valueOf(ModuleServiceLocator.getCommoMasterBD().getAIGDefaultSelected(
					insFltSegBldr.getOriginAirport()));

			// TODO implement an app paramter to show SSR or not by default
			// showSSRByDefault = AppSysParamsUtil.isShowSSRByDefault();

			if (ibeResInfo.getPriceInfoTO() != null) {
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(ibeResInfo.getPriceInfoTO(),
						flightSegmentTOs, totalFltSegBuilder.isReturn(), totalFltSegBuilder.getSelectedFlightSegments());
			} else {
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
			}

			LCCAncillaryAvailabilityInDTO inAncillaryAvailabilityDTO = new LCCAncillaryAvailabilityInDTO();
			inAncillaryAvailabilityDTO.addAllFlightSegmentTOs(flightSegmentTOs);
			inAncillaryAvailabilityDTO.setTransactionIdentifier(strTxnIdntifier);
			inAncillaryAvailabilityDTO.setQueryingSystem(ibeResInfo.getSelectedSystem());

			// if (AppSysParamsUtil.isShowSeatMap())
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.SEAT_MAP);
			// if (AppSysParamsUtil.isShowMeals())
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.MEALS);
			// if (AppSysParamsUtil.isShowTravelGuard()) {
			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.INSURANCE);
			// }

			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.SSR);

			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.BAGGAGE);

			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.AIRPORT_SERVICE);

			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.AIRPORT_TRANSFER);

			inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.FLEXI);

			inAncillaryAvailabilityDTO.setInsuranceOrigin(insFltSegBldr.getOriginAirport());

			inAncillaryAvailabilityDTO.setAppIndicator(getTrackInfo().getAppIndicator());

			String departureSegmentCode = com.isa.thinair.webplatform.api.util.ReservationUtil
					.getDepartureSegmentCode(fltSegBuilder.getAllFlightSegments());
			inAncillaryAvailabilityDTO.setDepartureSegmentCode(departureSegmentCode);

			if (modifySegment && resPaxInfo != null && !resPaxInfo.equals("")) {
				Collection<LCCClientReservationPax> colPax = ReservationUtil.transformJsonPassengers(resPaxInfo);
				inAncillaryAvailabilityDTO
						.setAirportServiceCountMap(SSRServicesUtil.extractAiportServiceCountFromJsonObj(colPax));
			}

			if (ibeResInfo.getBaggageSummaryTo() != null) {
				ibeResInfo.getBaggageSummaryTo().setAllowTillFinalCutOver(false);
			}
			inAncillaryAvailabilityDTO.setBaggageSummaryTo(ibeResInfo.getBaggageSummaryTo());
			// if (isModifyOperation || modifySegment || pnr == null) {
			// modify segments/anci & new reservation
			// inAncillaryAvailabilityDTO.setBaggageSummaryTo(ibeResInfo.getBaggageSummaryTo());
			// } else {
			// LCCReservationBaggageSummaryTo baggageSummaryTo = new LCCReservationBaggageSummaryTo();
			// baggageSummaryTo.setBookingClasses(new HashMap<String, String>());
			// baggageSummaryTo.setFlightNumbers(new ArrayList<String>());
			// baggageSummaryTo.setClassesOfService(new HashMap<String, String>());
			// inAncillaryAvailabilityDTO.setBaggageSummaryTo(baggageSummaryTo);
			// }

			AnciAvailabilityRS anciAvailabilityRS = ModuleServiceLocator.getAirproxyAncillaryBD().getAncillaryAvailability(
					inAncillaryAvailabilityDTO, TrackInfoUtil.getBasicTrackInfo(request));

			availabilityOutDTO = anciAvailabilityRS.getLccAncillaryAvailabilityOutDTOs();
			availabilityOutDTO = checkBufferTimes(availabilityOutDTO, isModifyOperation);

			ibeResInfo.addServiceTax(anciAvailabilityRS.isJnTaxApplicable(), anciAvailabilityRS.getTaxRatio(),
					EXTERNAL_CHARGES.JN_ANCI);

			ibeResInfo.setApplyPenaltyForAnciModification(anciAvailabilityRS.isApplyPenaltyForModification());
			applyAncillaryPenalty = anciAvailabilityRS.isApplyPenaltyForModification();

			jnTaxApplicable = anciAvailabilityRS.isJnTaxApplicable();
			if (jnTaxApplicable) {
				jnTaxRatio = anciAvailabilityRS.getTaxRatio().doubleValue();
			}

			if (ibeResInfo.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI) && modifySegment
					&& !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ibeResInfo.getReservationStatus())) {
				List<String> modSegList = ReservationUtil.getModifyingFlightRefNumberList(modifingFlightInfo);
				dateModification = com.isa.thinair.webplatform.api.util.ReservationUtil.isDateModification(flightSegmentTOs,
						modSegList);
			}

			// Disable ancillaries for ground segments
			disableAnciForGroundSegments(availabilityOutDTO);

			/*
			 * Determine to show the tab based on app parameter and segment anci avail
			 */
			// if (AppSysParamsUtil.isShowSeatMap()) {
			seatMapAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.SEAT_MAP);
			// }
			// if (AppSysParamsUtil.isShowMeals()) {
			mealsAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.MEALS);
			// }
			baggagesAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.BAGGAGE);

			airportServicesAvailable = checkAirportLevelAvailability(availabilityOutDTO, flightSegmentTOs,
					LCCAncillaryType.AIRPORT_SERVICE);

			airportTransferAvailable = checkAirportLevelAvailability(availabilityOutDTO, flightSegmentTOs,
					LCCAncillaryType.AIRPORT_TRANSFER);

			// if (AppSysParamsUtil.isShowTravelGuard() && insuranceDTO != null
			// && insuranceDTO.getInsuranceRefNumber() != null) {
			if (insuranceDTO != null && insuranceDTO.size() > 0 && insuranceDTO.get(0).getInsuranceRefNumber() != null) {
				insuranceAvailable = checkInsuranceAvailability(availabilityOutDTO, insFltSegBldr.getOriginFltRefNo());
			}

			if (insuranceDTO != null && insuranceAvailable) {
				insuranceViewChanged = insuranceDTO.get(0).isEuropianCountry();
			}

			if (((modifySegment) || addGroundSegment) && hasInsurance) {
				insuranceAvailable = false;
			}

			// TODO remove this after SSR support implemented for IBE
			ssrAvailable = AppParamUtil.isSSREnabledForIBE() && anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.SSR);

			// // FIXME For Interline
			// // Temp Fix -Interline Ancilary Will be implement later. no ancilary
			// // for interline modification right now
			// if (isInterlineModifySegment) {
			// seatMapAvailable = false;
			// mealsAvailable = false;
			// insuranceAvailable = false;
			// airportServicesAvailable = false;
			// ssrAvailable = false;
			// }

			// Calculate total flexi charge front-end
			if (getSearchParams().isFlexiSelected()) {
				flexiCharge = ReservationUtil.calculateTotalFlexiCharge(getSearchParams().getOndSelectedFlexi(),
						ibeResInfo.getPriceInfoTO());
			}

			if (isCancelSegment) {
				seatMapAvailable = false;
				mealsAvailable = false;
				baggagesAvailable = false;
				insuranceAvailable = false;
				airportServicesAvailable = false;
				airportTransferAvailable = false;
				ssrAvailable = false;
			}

			discountInfo = ibeResInfo.getDiscountInfo();
			socialSeatingParams = FlyingSocialUtil.composeSocialSeatingParams();
			success = true;

			paxWiseAnci = ibeResInfo.getAncillaryPaxTos();

			busCarrierCodes = new HashSet<String>();
			busCarrierCodes.addAll(SelectListGenerator.getBusCarrierCodes());
			
			setSegmentFlownStatusInAvailabilityOutDTO();

		} catch (Exception e) {
			success = false;
			messageTxt = MessageUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("Error occured in Ancillary Availablilty  ", e);
		}

		return result;
	}

	private void disableAnciForGroundSegments(List<LCCAncillaryAvailabilityOutDTO> availDTO) {
		if (availDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO anciAvail : availDTO) {
				if (anciAvail.getFlightSegmentTO() != null) {
					try {
						if (ModuleServiceLocator.getAirportBD().isBusSegment(anciAvail.getFlightSegmentTO().getSegmentCode())) {
							List<LCCAncillaryStatus> ancillaryStatusList = anciAvail.getAncillaryStatusList();
							if (ancillaryStatusList != null) {
								for (LCCAncillaryStatus lccAncillaryStatus : ancillaryStatusList) {
									if (!LCCAncillaryType.INSURANCE.equals(lccAncillaryStatus.getAncillaryType()
											.getAncillaryType())) {
										lccAncillaryStatus.setAvailable(false);
									}
								}
							}
						}
					} catch (ModuleException e) {
						log.error("Error in disabling Anci for ground seg", e);
					}
				}
			}
		}
	}

	private List<LCCInsuredPassengerDTO> compileInsuredPaxDTOList(Collection<PassengerDTO> paxAdultChildList,
			ContactInfoDTO contactInfo) {
		List<LCCInsuredPassengerDTO> paxList = new ArrayList<LCCInsuredPassengerDTO>();
		for (PassengerDTO p : paxAdultChildList) {
			String strBday = p.getDateOfBirth();
			String fn = p.getFirstName();
			String lastName = p.getLastName();
			String title = p.getTitle();
			String nationality = p.getNationality();

			NameDTO nameDto = new NameDTO();
			nameDto.setFirstName(fn);
			nameDto.setLastName(lastName);
			nameDto.setPaxTitle(title);

			LCCAddressDTO address = new LCCAddressDTO();
			address.setCellPhoneNo(contactInfo.getmCountry() + "-" + contactInfo.getmArea() + "-" + contactInfo.getmNumber());
			address.setCityName(contactInfo.getCity());
			address.setCountryName(contactInfo.getCountry());
			address.setEmailAddr(contactInfo.getEmailAddress());
			address.setHomePhoneNo(contactInfo.getlCountry() + "-" + contactInfo.getlArea() + "-" + contactInfo.getlNumber());
			address.setStateName(contactInfo.getState());

			LCCInsuredPassengerDTO pax = new LCCInsuredPassengerDTO();
			pax.setBirthDate(BookingUtil.stringToDate(strBday));
			pax.setAddressDTO(address);
			pax.setEmail(contactInfo.getEmailAddress());
			pax.setHomePhoneNumber(address.getHomePhoneNo());
			pax.setNameDTO(nameDto);
			pax.setNationality(nationality);
			paxList.add(pax);
		}
		return paxList;
	}

	private boolean anciActiveInLeastOneSeg(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
			for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
				if ((ancillaryStatus.isAvailable())
						&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkAirportLevelAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			List<FlightSegmentTO> flightSegmentTOs, String ancilaryType) {

		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (ReservationUtil.isAvailableForSelecetedSegment(ancillaryAvailabilityOutDTO.getFlightSegmentTO()
						.getFlightRefNumber(), flightSegmentTOs)) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(ancilaryType))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	private boolean checkInsuranceAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String fltRefNo) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (FlightRefNumberUtil.getSegmentIdFromFlightRPH(
						ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber()).intValue() == FlightRefNumberUtil
						.getSegmentIdFromFlightRPH(fltRefNo).intValue()) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(LCCAncillaryType.INSURANCE))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
					break;
				}
			}
		}
		return false;
	}

	private List<LCCAncillaryAvailabilityOutDTO> checkBufferTimes(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			boolean isModifyOperation) throws ModuleException {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				// FIXME this should be changed to the zulu time and checking
				// should be done against zulu tooo...
				Date departureDate = FlightRefNumberUtil.getDepartureDateFromFlightRPH(ancillaryAvailabilityOutDTO
						.getFlightSegmentTO().getFlightRefNumber());
				String segmentCode = FlightRefNumberUtil.getSegmentCodeFromFlightRPH(ancillaryAvailabilityOutDTO
						.getFlightSegmentTO().getFlightRefNumber());
				boolean isDomesticFlight = ancillaryAvailabilityOutDTO.getFlightSegmentTO().isDomesticFlight();
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					ancillaryStatus.setAvailable(ancillaryStatus.isAvailable()
							&& isEnableService(ancillaryStatus.getAncillaryType().getAncillaryType(), departureDate, segmentCode,
									isModifyOperation, isDomesticFlight));

				}
			}
		}
		return availabilityOutDTO;
	}

	private boolean isEnableService(String anciType, Date departDate, String segmentCode, boolean isModifyOperation,
			boolean isDomesticFlight) throws ModuleException {
		String airportCode = null;
		if (segmentCode != null && segmentCode.length() > 0) {
			airportCode = segmentCode.split("/")[0];
		}

		long diffMils = 0;
		Calendar currentTime = Calendar.getInstance();
		currentTime.setTime(DateUtil.getZuluTime());
		if (airportCode != null) {
			try {
				departDate = DateUtil.getZuluDateTime(departDate, airportCode);
			} catch (ModuleException e) {
				// Do nothing.Once segment RPH send zulu time too, then this check should be done on Zulu time
			}
		}

		if (anciType.equals(LCCAncillaryType.SEAT_MAP)) {
			diffMils = AppSysParamsUtil.getIBESeatmapStopCutoverInMillis();
		} else if (anciType.equals(LCCAncillaryType.MEALS)) {
			diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
		} else if (anciType.equals(LCCAncillaryType.BAGGAGE)) {
			diffMils = BaggageTimeWatcher.getBaggageCutOverTimeForIBEInMillis(isModifyOperation);
		} else if (anciType.equals(LCCAncillaryType.SSR)) {
			if (AppSysParamsUtil.isMaintainSSRWiseCutoffTime()) {
				return true;
			}
			diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();
		} else if (anciType.equals(LCCAncillaryType.INSURANCE)) {
			diffMils = 0;
		} else if (anciType.equals(LCCAncillaryType.AIRPORT_TRANSFER)) {
			if (isDomesticFlight) {
				diffMils = AppSysParamsUtil.getAirportTransferStopCutoverForDomesticInMillis();
			} else {
				diffMils = AppSysParamsUtil.getAirportTransferStopCutoverForInternationalInMillis();
			}
		}

		if (departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
			return false;
		} else {
			return true;
		}
	}

	private LCCInsuredContactInfoDTO getLCCInsuranceContactInfo(ContactInfoDTO contactInfo) {
		LCCInsuredContactInfoDTO lccInsuranceContactInfo = new LCCInsuredContactInfoDTO();
		lccInsuranceContactInfo.setContactPerson(contactInfo.getFirstName() + " " + contactInfo.getLastName());
		lccInsuranceContactInfo.setAddress1(contactInfo.getAddresline() + contactInfo.getAddresStreet());
		lccInsuranceContactInfo.setAddress2(contactInfo.getCity());
		lccInsuranceContactInfo.setHomePhoneNum(contactInfo.getlCountry() + "-" + contactInfo.getlArea() + "-"
				+ contactInfo.getlNumber());
		lccInsuranceContactInfo.setMobilePhoneNum(contactInfo.getmCountry() + "-" + contactInfo.getmArea() + "-"
				+ contactInfo.getmNumber());
		lccInsuranceContactInfo.setCity(contactInfo.getCity());
		lccInsuranceContactInfo.setCountry(contactInfo.getCountry());
		lccInsuranceContactInfo.setEmailAddress(contactInfo.getEmailAddress());
		lccInsuranceContactInfo.setPrefLanguage(this.getCommonParams().getLocale().toUpperCase());

		return lccInsuranceContactInfo;
	}

	private void setSegmentFlownStatusInAvailabilityOutDTO() {
		if (availabilityOutDTO != null && paxWiseAnci != null) {
			for (Iterator<LCCAncillaryAvailabilityOutDTO> avalOutDtoIterator = availabilityOutDTO.iterator(); avalOutDtoIterator
					.hasNext();) {
				LCCAncillaryAvailabilityOutDTO avalOutDto = avalOutDtoIterator.next();
				for (Iterator<AncillaryPaxTo> paxWiseAnciIterator = paxWiseAnci.iterator(); paxWiseAnciIterator.hasNext();) {
					AncillaryPaxTo paxWiseAnci = paxWiseAnciIterator.next();
					List<LCCSelectedSegmentAncillaryDTO> segmentWiseAnciList = paxWiseAnci.getSelectedAncillaries();
					for (Iterator<LCCSelectedSegmentAncillaryDTO> segAnciIterator = segmentWiseAnciList.iterator(); segAnciIterator
							.hasNext();) {
						LCCSelectedSegmentAncillaryDTO segmentAnci = segAnciIterator.next();
						if (segmentAnci.getFlightSegmentTO().getFlightRefNumber()
								.equals(avalOutDto.getFlightSegmentTO().getFlightRefNumber().substring(0, avalOutDto.getFlightSegmentTO().getFlightRefNumber().length()-2))) {
							avalOutDto.getFlightSegmentTO().setFlownSegment(segmentAnci.getFlightSegmentTO().isFlownSegment());
						}
					}
				}
			}
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isSeatMapAvailable() {
		return seatMapAvailable;
	}

	public void setSeatMapAvailable(boolean seatMapAvailable) {
		this.seatMapAvailable = seatMapAvailable;
	}

	public boolean isMealsAvailable() {
		return mealsAvailable;
	}

	public void setMealsAvailable(boolean mealsAvailable) {
		this.mealsAvailable = mealsAvailable;
	}

	public boolean isInsuranceAvailable() {
		return insuranceAvailable;
	}

	public void setInsuranceAvailable(boolean insuranceAvailable) {
		this.insuranceAvailable = insuranceAvailable;
	}

	public boolean isAirportServicesAvailable() {
		return airportServicesAvailable;
	}

	public boolean isSsrAvailable() {
		return ssrAvailable;
	}

	public void setSsrAvailable(boolean ssrAvailable) {
		this.ssrAvailable = ssrAvailable;
	}

	public void setAirportServicesAvailable(boolean airportServicesAvailable) {
		this.airportServicesAvailable = airportServicesAvailable;
	}

	public boolean isAirportTransferAvailable() {
		return airportTransferAvailable;
	}

	public List<LCCAncillaryAvailabilityOutDTO> getAvailabilityOutDTO() {
		return availabilityOutDTO;
	}

	public void setAvailabilityOutDTO(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO) {
		this.availabilityOutDTO = availabilityOutDTO;
	}

	public Collection<PassengerDTO> getAdultList() {
		return adultList;
	}

	public void setAdultList(Collection<PassengerDTO> adultList) {
		this.adultList = adultList;
	}

	public Collection<PassengerDTO> getChildList() {
		return childList;
	}

	public void setChildList(Collection<PassengerDTO> childList) {
		this.childList = childList;
	}

	public Collection<PassengerDTO> getInfantList() {
		return infantList;
	}

	public void setInfantList(Collection<PassengerDTO> infantList) {
		this.infantList = infantList;
	}

	public URLTO getUrlInfo() {
		return urlInfo;
	}

	public void setUrlInfo(URLTO urlInfo) {
		this.urlInfo = urlInfo;
	}

	public List<LCCInsuranceQuotationDTO> getInsuranceDTO() {
		return insuranceDTO;
	}

	public void setInsuranceDTO(List<LCCInsuranceQuotationDTO> insuranceDTO) {
		this.insuranceDTO = insuranceDTO;
	}

	public Collection<SegInfoDTO> getFlightSegments() {
		return flightSegments;
	}

	public void setFlightSegments(Collection<SegInfoDTO> flightSegments) {
		this.flightSegments = flightSegments;
	}

	public boolean isBlnReturn() {
		return blnReturn;
	}

	public void setBlnReturn(boolean blnReturn) {
		this.blnReturn = blnReturn;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isAllowInsModify() {
		return allowInsModify;
	}

	public boolean isRakEnabled() {
		return rakEnabled;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public boolean isShowSSRByDefault() {
		return showSSRByDefault;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setHasInsurance(boolean hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	public String getModifySegmentRefNos() {
		return modifySegmentRefNos;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public String getInsuranceSelectedByDefault() {
		return insuranceSelectedByDefault;
	}

	public void setInsuranceSelectedByDefault(String insuranceSelectedByDefault) {
		this.insuranceSelectedByDefault = insuranceSelectedByDefault;
	}

	public boolean isBaggagesAvailable() {
		return baggagesAvailable;
	}

	public void setBaggagesAvailable(boolean baggagesAvailable) {
		this.baggagesAvailable = baggagesAvailable;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	/**
	 * @param lccMap
	 *            the lccMap to set
	 */
	public void setLccMap(Map<String, LogicalCabinClassDTO> lccMap) {
		this.lccMap = lccMap;
	}

	/**
	 * @return the lccMap
	 */
	public Map<String, LogicalCabinClassDTO> getLccMap() {
		return lccMap;
	}

	public boolean isInsuranceViewChanged() {
		return insuranceViewChanged;
	}

	public void setInsuranceViewChanged(boolean insuranceViewChanged) {
		this.insuranceViewChanged = insuranceViewChanged;
	}

	public DiscountedFareDetails getDiscountInfo() {
		return discountInfo;
	}

	public SocialTO getSocialSeatingParams() {
		return socialSeatingParams;
	}

	public void setSocialSeatingParams(SocialTO socialSeatingParams) {
		this.socialSeatingParams = socialSeatingParams;
	}

	public boolean isJnTaxApplicable() {
		return jnTaxApplicable;
	}

	public double getJnTaxRatio() {
		return jnTaxRatio;
	}

	public void setModifingFlightInfo(String modifingFlightInfo) {
		this.modifingFlightInfo = modifingFlightInfo;
	}

	public boolean isDateModification() {
		return dateModification;
	}

	public double getFlexiCharge() {
		return flexiCharge;
	}

	public boolean isFlexiEnableInAnci() {
		return isFlexiEnableInAnci;
	}

	public void setFlexiEnableInAnci(boolean isFlexiEnableInAnci) {
		this.isFlexiEnableInAnci = isFlexiEnableInAnci;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getPaxJson() {
		return paxJson;
	}

	public void setPaxJson(String paxJson) {
		this.paxJson = paxJson;
	}

	public Collection<AncillaryPaxTo> getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(Collection<AncillaryPaxTo> paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public boolean isApplyAncillaryPenalty() {
		return applyAncillaryPenalty;
	}

	public List<BundledFareDTO> getSelectedBundledFareDTOs() {
		return selectedBundledFareDTOs;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public Set<String> getBusCarrierCodes() {
		return busCarrierCodes;
	}

	public void setBusCarrierCodes(Set<String> busCarrierCodes) {
		this.busCarrierCodes = busCarrierCodes;
	}

}
