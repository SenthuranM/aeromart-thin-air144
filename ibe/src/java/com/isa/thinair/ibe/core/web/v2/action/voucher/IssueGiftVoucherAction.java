package com.isa.thinair.ibe.core.web.v2.action.voucher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherCCDetailsDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.api.dto.VoucherCCPaymentDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.RequestAttribute;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.opensymphony.xwork2.ActionChainResult;

/**
 * @author chanaka
 *
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.WIRECARD_SUCCESS, value = StrutsConstants.Jsp.Payment.WIRECARD_INTERLINE_CONTAINER),
		@Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Payment.POST_INPUT_DATA),
		@Result(name = StrutsConstants.Result.BYPASS, type = ActionChainResult.class, value = StrutsConstants.Action.HANDLE_VOUCHER_IPG_RESPONSE),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.LANDING_VOUCHER_3DSECURE, value = StrutsConstants.Jsp.Payment.LANDING_VOUCHER_3D_SECURE_PAGE),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Result.CARD_PAYMENT_ERROR, value = StrutsConstants.Jsp.Payment.CARD_PAYMENT_ERROR) })
public class IssueGiftVoucherAction extends IBEBaseAction {

	private Log log = LogFactory.getLog(IssueGiftVoucherAction.class);

	private VoucherDTO voucherDTO;
	
	private VoucherCCDetailsDTO voucherCCDetailsDTO;	

	private CreditCardTO card;

	private PaymentGateWayInfoDTO pgw = new PaymentGateWayInfoDTO();

	private String paymentMethod;

	private String ipgId;

	private boolean success = true;

	private String paymentError;
	
	@SuppressWarnings("rawtypes")
	private String selectedList;

	private static final String RETURN_URL = AppParamUtil.getSecureIBEUrl() + "handleVoucherIPGResponse.action";
	private static final String STATUS_URL = AppParamUtil.getSecureIBEUrl();
	private static final String RECEIPT_URL = AppParamUtil.getSecureIBEUrl() + "receiptIPGResponse.action";
	private static final int STATUS_3DSECURE_ENABLE = 46;
	private static final String GENERIC_CARD_TYPE = "5";

	public String execute() throws ParseException {

		if (AppSysParamsUtil.isVoucherEnabled()) {
			if (log.isDebugEnabled()) {
				log.debug("Voucher paymentgateway retrieve started");
			}

			if (SystemPropertyUtil.isPaymentBokerDisabled()) {
				return StrutsConstants.Result.BYPASS;
			}

			boolean isIntExtPaymentGateway = false;

			synchronized (request) {
				if (voucherDTO == null) {
					SessionUtil.expireSession(request);
					return StrutsConstants.Result.SESSION_EXPIRED;
				}
			}
			List<VoucherDTO> voucherList = prepareVoucherDTOList(selectedList);			

			VoucherCCPaymentDTO postVoucherPayment;
			GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
			PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
			CommonMasterBD commonMasterBD = ModuleServiceLocator.getCommoMasterBD();
			String carrierCode = SessionUtil.getCarrier(request);

			try {
				postVoucherPayment = new VoucherCCPaymentDTO();
				SessionUtil.setVoucherPaymentDTO(request, postVoucherPayment);
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				String baseCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
				String hdnSelCurrency = pgw.getPayCurrency();
				PayCurrencyDTO payCurrencyDTO = null;

				if (log.isDebugEnabled()) {
					log.debug("checking for selected currency");
				}
				Currency currency = null;

				CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(hdnSelCurrency,
						ApplicationEngine.IBE);

				if (exchangeRate != null) {
					currency = commonMasterBD.getCurrency(hdnSelCurrency);
					if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultIbePGId() != null) {
						baseCurrencyCode = hdnSelCurrency;
						payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, exchangeRate.getMultiplyingExchangeRate(),
								currency.getBoundryValue(), currency.getBreakPoint());
					}
				}

				if (payCurrencyDTO == null) {
					if (log.isDebugEnabled()) {
						log.debug("Issue Gift Voucher checking for selected currency does not have pg getting the default ");
					}
					currency = commonMasterBD.getCurrency(baseCurrencyCode);

					exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(baseCurrencyCode, ApplicationEngine.IBE);
					payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, new BigDecimal(1), currency.getBoundryValue(),
							currency.getBreakPoint());
				}

				pgw.setPayCurrency(baseCurrencyCode);
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil
						.validateAndPrepareIPGConfigurationParamsDTO(new Integer(pgw.getPaymentGateway()), baseCurrencyCode);

				Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);

				String brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
				String requestMethod = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_REQUEST_METHOD);
				String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);

				boolean isSwitchToExternalURL = Boolean.parseBoolean(ipgProps
						.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SWITCH_TO_EXTERNAL_URL));
				boolean isViewPaymentInIframe = Boolean.parseBoolean(ipgProps
						.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_VIEW_PAYMENT_IFRAME));

				String selCardType = getCardType();
				String txtCardNo = "";
				String txtName = "";
				String txtSCode = "";
				String strExpiry = "";

				if (brokerType != null
						&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)) {
					selCardType = card.getCardType();
					txtCardNo = card.getCardNo();
					txtName = card.getCardHoldersName();
					txtSCode = card.getCardCVV();
					strExpiry = card.getExpiryDateYYMMFormat();
					isIntExtPaymentGateway = true;

					if (StringUtil.isEmpty(selCardType) || StringUtil.isEmpty(txtCardNo) || StringUtil.isEmpty(txtName)
							|| StringUtil.isEmpty(txtSCode) || StringUtil.isEmpty(strExpiry) || strExpiry.length() != 4) {
						throw new ModuleException("paymentbroker.card.invalid");
					}
				}

				IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();

				ipgRequestDTO.setDefaultCarrierName(SystemUtil.getCarrierName(request));
				ipgRequestDTO.setCardType(selCardType);
				ipgRequestDTO.setCardNo(txtCardNo);
				ipgRequestDTO.setSecureCode(txtSCode);
				ipgRequestDTO.setHolderName(txtName);
				ipgRequestDTO.setExpiryDate(strExpiry);
				ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
				ipgRequestDTO.setPaymentMethod(card.getPaymentMethod());

				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				voucherList = voucherDelegate.prepareVouchersForIBEPayment(voucherList, pgw.getPayCurrency(),pgw.getPayCurrencyAmount());
				int temporyPayId = voucherList.get(0).getVoucherPaymentDTO().getTempPaymentID();
				postVoucherPayment.setTempTxId(temporyPayId);
				postVoucherPayment.setVoucherDTOList(voucherList);
				postVoucherPayment.setVoucherGroupId(voucherList.get(0).getVoucherGroupId());
				String ipgPNR = voucherList.get(0).getVoucherGroupId();

				Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap = createTempPayMap(temporyPayId, payCurrencyDTO,
						ipgIdentificationParamsDTO);

				ipgRequestDTO.setAmount(AccelAeroRounderPolicy.convertAndRound(new BigDecimal(pgw.getPayCurrencyAmount()),
						payCurrencyDTO).toString());

				log.debug("===============Tracking Payment Geteway Issue:" + " exchangeRate.getMultiplyingExchangeRate(): "
						+ exchangeRate.getMultiplyingExchangeRate());
				log.debug("===============Tracking Payment Geteway Issue: payCurrencyDTO.getPayCurrencyCode: "
						+ payCurrencyDTO.getPayCurrencyCode() + " exchangeRate.getMultiplyingExchangeRate(): "
						+ payCurrencyDTO.getTotalPayCurrencyAmount());
				// ****************************************************************************************

				ipgRequestDTO.setApplicationTransactionId(temporyPayId);
				ipgRequestDTO.setReturnUrl(RETURN_URL);
				ipgRequestDTO.setStatusUrl(STATUS_URL);
				ipgRequestDTO.setReceiptUrl(RECEIPT_URL);
				ipgRequestDTO.setOfferUrl(globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, carrierCode));
				ipgRequestDTO.setApplicationIndicator(AppIndicatorEnum.APP_IBE);
				ipgRequestDTO.setNoOfDecimals(2);
				ipgRequestDTO.setPnr(ipgPNR);
				ipgRequestDTO.setRequestedCarrierCode(carrierCode);
				ipgRequestDTO.setSessionID(request.getSession().getId());
				ipgRequestDTO.setSessionLanguageCode(StringUtils.trimToEmpty(SessionUtil.getLanguage(request)));
				ipgRequestDTO.setUserIPAddress(getClientInfoDTO().getIpAddress());
				ipgRequestDTO.setUserAgent(getClientInfoDTO().getBrowser());
				ipgRequestDTO.setIpCountryCode(commonMasterBD.getCountryByIpAddress(getClientInfoDTO().getIpAddress()));

				if (log.isDebugEnabled()) {
					log.debug("Voucher paymentgateway call for payment broker");
				}

				List<CardDetailConfigDTO> cardDetailConfigData = null;
				if (isIntExtPaymentGateway) {
					// Get payment gateway configuration data, avoid getting for 'external'
					cardDetailConfigData = paymentBrokerBD.getPaymentGatewayCardConfigData(ipgIdentificationParamsDTO.getIpgId()
							.toString());
				}
				IPGRequestResultsDTO ipgRequestResultsDTO = null;
				String strRequestData = null;
				ipgRequestDTO.setPaymentGateWayName(pgw.getProviderName());

				// TODO PAYPAL configuration
				if (isIntExtPaymentGateway) {
					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
				} else {					
					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);					
				}

				int paymentBrokerRefno = ipgRequestResultsDTO.getPaymentBrokerRefNo();

				postVoucherPayment.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
						ipgRequestResultsDTO.getPaymentBrokerRefNo(), mapTempPayMap));

				postVoucherPayment.setPaymentBrokerRef(paymentBrokerRefno);
				strRequestData = ipgRequestResultsDTO.getRequestData();
				postVoucherPayment.setIpgRefenceNo(ipgRequestResultsDTO.getAccelAeroTransactionRef());
				postVoucherPayment.setPaymentGateWay(pgw);
				request.setAttribute(RequestAttribute.REQUEST_METHOD, requestMethod);
				request.setAttribute(RequestAttribute.PAY_GATEWAY_TYPE, brokerType);
				request.setAttribute(RequestAttribute.POST_INPUT_DATA_FORM_HTML, strRequestData);
				request.setAttribute(RequestAttribute.PAY_GATEWAY_SWITCH_TOEXTERNAL_URL, isSwitchToExternalURL);

				if (SessionUtil.getIbeReservationPostDTO(request) != null
						&& SessionUtil.getIbeReservationPostDTO(request).getPnr() != null) {
					SessionUtil.getIbeReservationPostDTO(request).setPnr(null);
				}

				postVoucherPayment.setViewPaymentInIframe(isViewPaymentInIframe);
				if (isViewPaymentInIframe && !isSwitchToExternalURL
						&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL.equals(brokerType)) {
					request.setAttribute(WebConstants.DISPLAY_TOP_BOTTOM_BANNERS, true);
					return StrutsConstants.Result.WIRECARD_SUCCESS;
				}

				// Exclude Internal - External Payment gateway request data from log
				if (isIntExtPaymentGateway) {
					strRequestData = "";
					log.debug("Payment gateway type : Exclude Internal");
				} 
				try {
					// TODO re-factor
					if (log.isInfoEnabled()) {
						log.info("Before posting IPG request :" + "|merchantTxnRef="
								+ ipgRequestResultsDTO.getAccelAeroTransactionRef() + "|tptId=" + temporyPayId
								+ "|totalPayAmountInBaseCurr=" + pgw.getPayCurrencyAmount() + "|payCurCode="
								+ payCurrencyDTO.getPayCurrencyCode() + "|voucherGroupId=" + ipgPNR + "|payCurExRate="
								+ exchangeRate.getMultiplyingExchangeRate() + "|totalPayAmountInPayCurr="
								+ ipgRequestDTO.getAmount() + "|ContactInfo::firstName=" + ipgRequestDTO.getContactFirstName()
								+ ",lastName=" + ipgRequestDTO.getContactLastName() + ",email=" + ipgRequestDTO.getEmail()
								+ ",country=" + ipgRequestDTO.getContactCountryCode() + ",mobile="
								+ ipgRequestDTO.getContactMobileNumber() + ",telephone=" + ipgRequestDTO.getContactPhoneNumber()
								+ ",sessLang=" + ipgRequestDTO.getSessionLanguageCode() + "|sessId="
								+ request.getRequestedSessionId() + "|request=[" + strRequestData + "]");
					}
				} catch (Throwable e) {
					log.error("Error in logging data prior to IPG request", e);
				}

				// If the PGW response is an XML, need to process the XML to handle the response(eg : OGONE PGW)
				if (responseTypeXML != null && !responseTypeXML.equals("") && responseTypeXML.equals("true")) {
					log.debug("Execute the payment and get the response for XML response type PGWs");
					XMLResponseDTO response = paymentBrokerBD.getXMLResponse(ipgRequestDTO.getIpgIdentificationParamsDTO(),
							ipgRequestResultsDTO.getPostDataMap());
					log.debug("Recieved XMLresponse");
					ArrayList<Integer> tptIds = new ArrayList<Integer>();
					tptIds.add(temporyPayId);
					log.debug("Redirecting depending on the response");
					postVoucherPayment.setSecurePayment3D(false);
					if (response != null && Integer.valueOf(response.getStatus()) == STATUS_3DSECURE_ENABLE) {
						// TODO secure 3D enabled credit card payments
						if (log.isDebugEnabled()) {
							log.debug("3D Secure payment: SessionID :" + request.getRequestedSessionId());
						}
						postVoucherPayment.setBank3DSecureHTML(response.getHtml());
						postVoucherPayment.setSwitchToExternalURL(true);
						postVoucherPayment.setSecurePayment3D(true);
						return StrutsConstants.Result.LANDING_VOUCHER_3DSECURE;
					} else if (response != null && !response.getStatus().equals("")) {
						request.setAttribute(WebConstants.XML_RESPONSE_PARAMETERS, response);
						return StrutsConstants.Result.BYPASS;
					} else {
						return StrutsConstants.Result.ERROR;
					}
				}
			} catch (ModuleException e) {
				log.error("Voucher paymentgateway call for payment broker", e);
				if ("paymentbroker.card.invalid".equals(e.getExceptionCode())) {
					request.setAttribute(WebConstants.INVALID_CARD, true);
					SessionUtil.resetSesionCardDataInError(request);
					request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
							I18NUtil.getMessage(e.getExceptionCode(), SessionUtil.getLanguage(request)));
					if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
						return StrutsConstants.Result.CARD_PAYMENT_ERROR;
					}
				} else {
					return StrutsConstants.Result.ERROR;
				}

			} catch (Exception e) {
				log.error("Voucher paymentgateway call for payment broker", e);

				SystemUtil.setCommonParameters(request, getCommonParams());
				SessionUtil.resetSesionDataInError(request);

				if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
					return StrutsConstants.Result.CARD_PAYMENT_ERROR;
				} else {
					return StrutsConstants.Result.ERROR;
				}
			}
		} else {
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
					I18NUtil.getMessage(ExceptionConstants.KEY_UNAUTHORIZED_OPERATION, SessionUtil.getLanguage(request)));
			return StrutsConstants.Result.ERROR;
		}
		return StrutsConstants.Result.SUCCESS;
	}

	private String getCardType() {
		String selCardType = GENERIC_CARD_TYPE;
		if (card != null && card.getCardType() != null && !card.getCardType().trim().isEmpty()) {
			selCardType = card.getCardType();
		}
		return selCardType;
	}

	// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 11-PAYPAL]

	private Map<Integer, CommonCreditCardPaymentInfo> createTempPayMap(int temporyPayId, PayCurrencyDTO payCurrencyDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		CommonCreditCardPaymentInfo ccPaymentInfo = new CommonCreditCardPaymentInfo();
		ccPaymentInfo.setAppIndicator(AppIndicatorEnum.APP_IBE);
		ccPaymentInfo.seteDate(card.getExpiryDateYYMMFormat());
		ccPaymentInfo.setCardHoldersName(card.getCardHoldersName());
		ccPaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		ccPaymentInfo.setNo(card.getCardNo());
		ccPaymentInfo.setNoFirstDigits(BeanUtils.getFirst6Digits(card.getCardNo()));
		ccPaymentInfo.setNoLastDigits(BeanUtils.getLast4Digits(card.getCardNo()));
		ccPaymentInfo.setPayCurrencyAmount(new BigDecimal(pgw.getPayCurrencyAmount()));
		ccPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		ccPaymentInfo.setSecurityCode(card.getCardCVV());
		ccPaymentInfo.setTemporyPaymentId(temporyPayId);
		ccPaymentInfo.setTnxMode(TnxModeEnum.SECURE_3D);
		ccPaymentInfo.setType(Integer.parseInt(card.getCardType()));

		Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap = new HashMap<Integer, CommonCreditCardPaymentInfo>();
		mapTempPayMap.put(temporyPayId, ccPaymentInfo);

		return mapTempPayMap;
	}
	
	private List<VoucherDTO> prepareVoucherDTOList(String selectedVoucherList) {
		List<VoucherDTO> voucherSet = new ArrayList<VoucherDTO>();
		JSONArray detailsArray;
		try {
			detailsArray = new JSONArray(selectedVoucherList);
			for (int i = 0; i < detailsArray.length(); i++) {
				JSONObject jObject = detailsArray.getJSONObject(i);
				String amountInLocal = jObject.getString("amountInLocal");
				int templateId = jObject.getInt("templateId");
				String currencyCode = jObject.getString("currencyCode");
				String validFrom = jObject.getString("validFrom");
				String validTo = jObject.getString("validTo");
				String ccTxnFeeLocal = jObject.getString("ccTxnFeeLocal");
				int quantity = jObject.getInt("quantity");

				for (int j = 0; j < quantity; j++) {
					VoucherDTO voucherDTOLocal = new VoucherDTO();
					VoucherPaymentDTO voucherPaymentDTO = new VoucherPaymentDTO();

					voucherDTOLocal.setAmountInLocal(amountInLocal);
					voucherDTOLocal.setTemplateId(templateId);
					voucherDTOLocal.setValidFrom(validFrom);
					voucherDTOLocal.setValidTo(validTo);
					voucherDTOLocal.setCurrencyCode(currencyCode);
					voucherDTOLocal.setPaxFirstName(voucherDTO.getPaxFirstName());
					voucherDTOLocal.setPaxLastName(voucherDTO.getPaxLastName());
					voucherDTOLocal.setEmail(voucherDTO.getEmail());
					voucherDTOLocal.setRemarks(voucherDTO.getRemarks());

					voucherPaymentDTO.setAmountLocal(amountInLocal);
					voucherPaymentDTO.setCardCVV(voucherCCDetailsDTO.getCardCVV());
					voucherPaymentDTO.setCardExpiry(voucherCCDetailsDTO.getCardExpiry());
					voucherPaymentDTO.setCardHolderName(voucherCCDetailsDTO.getCardHolderName());
					voucherPaymentDTO.setCardNumber(voucherCCDetailsDTO.getCardNumber());
					voucherPaymentDTO.setCardTxnFeeLocal(ccTxnFeeLocal);
					voucherPaymentDTO.setCardType(voucherCCDetailsDTO.getCardType());
					voucherPaymentDTO.setIpgId(Integer.parseInt(voucherCCDetailsDTO.getIpgId()));
					voucherPaymentDTO.setPaymentMethod(voucherCCDetailsDTO.getPaymentMethod());

					voucherDTOLocal.setVoucherPaymentDTO(voucherPaymentDTO);
					voucherSet.add(voucherDTOLocal);
				}
			}

		} catch (JSONException e) {
			log.error("JSON converter error!", e);
		}
		return voucherSet;
	}
	/**
	 * @return the voucherDTO
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO
	 *            the voucherDTO to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

	/**
	 * @return the card
	 */
	public CreditCardTO getCard() {
		return card;
	}

	/**
	 * @param card
	 *            the card to set
	 */
	public void setCard(CreditCardTO card) {
		this.card = card;
	}

	/**
	 * @return the pgw
	 */
	public PaymentGateWayInfoDTO getPgw() {
		return pgw;
	}

	/**
	 * @param pgw
	 *            the pgw to set
	 */
	public void setPgw(PaymentGateWayInfoDTO pgw) {
		this.pgw = pgw;
	}

	/**
	 * @return the paymentMethod
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod
	 *            the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the ipgId
	 */
	public String getIpgId() {
		return ipgId;
	}

	/**
	 * @param ipgId
	 *            the ipgId to set
	 */
	public void setIpgId(String ipgId) {
		this.ipgId = ipgId;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the paymentError
	 */
	public String getPaymentError() {
		return paymentError;
	}

	/**
	 * @param paymentError
	 *            the paymentError to set
	 */
	public void setPaymentError(String paymentError) {
		this.paymentError = paymentError;
	}

	@SuppressWarnings("rawtypes")
	public String getSelectedList() {
		return selectedList;
	}

	public void setSelectedList(String selectedList) {
		this.selectedList = selectedList;
	}
	
	public VoucherCCDetailsDTO getVoucherCCDetailsDTO() {
		return voucherCCDetailsDTO;
	}

	public void setVoucherCCDetailsDTO(VoucherCCDetailsDTO voucherCCDetailsDTO) {
		this.voucherCCDetailsDTO = voucherCCDetailsDTO;
	}



}
