package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.CustomerCreditDTO;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.api.dto.FlightSearchInfoDTO;
import com.isa.thinair.ibe.api.dto.SessionTimeoutDataDTO;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerHomeDetailAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ShowCustomerHomeDetailAction.class);

	private boolean success = true;

	private String messageTxt;

	private HashMap<String, String> jsonLabel;

	private CustomerDTO customer;

	private FlightSearchInfoDTO searchInfo;

	private Map<String, String> errorInfo;

	private String totalCustomerCredit;

	private SessionTimeoutDataDTO timeoutDTO;

	private LmsMemberDTO lmsDetails;

	private boolean socialLoginEnabled = false;

	private boolean loyaltyManagmentEnabled = false;

	private boolean integrateMashreqWithLMSEnabled = false;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "Common", "Form", "PgSearch", "PgUserTab", "PgUserCommon" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			Customer customerModel = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			loyaltyManagmentEnabled = AppSysParamsUtil.isLMSEnabled();
			integrateMashreqWithLMSEnabled = AppSysParamsUtil.isIntegrateMashreqWithLMS();
			if (customerModel != null) {
				int intCustomerID = customerModel.getCustomerId();
				customer = CustomerUtilV2.getCustomerNameDetail(customerModel);
				Collection<CustomerCreditDTO> customerCreditDTO = CustomerUtilV2.getCustomerCreditFromAllCarriers(intCustomerID,
						getTrackInfo());
				totalCustomerCredit = CustomerUtilV2.calculateCustomerTotalCredit(customerCreditDTO);

				if (loyaltyManagmentEnabled) {
					if (!customerModel.getCustomerLmsDetails().isEmpty()) {
						LmsMember lmsMember = customerModel.getCustomerLmsDetails().iterator().next();
						lmsDetails = CustomerUtilV2.getLmsMemberDTO(lmsMember);
					}
				}
				/*
				 * totalCustomerCredit = CustomerUtilV2.getCustomerTotalCredit(ModuleServiceLocator
				 * .getReservationQueryBD().getReservationCredits(intCustomerID));
				 */
				CustomerUtilV2.setLoyaltyAccountStatus(customer, request);
				socialLoginEnabled = AppSysParamsUtil.isSocialLoginFromFacebookEnabled();
			}
			searchInfo = SearchUtil.getSearchInfoDTO(request);
			errorInfo = ErrorMessageUtil.getFlightSearchErrors(request);
			timeoutDTO = ReservationUtil.fetchSessionTimeoutData(request);
		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowCustomerHomeDetailAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowCustomerHomeDetailAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public FlightSearchInfoDTO getSearchInfo() {
		return searchInfo;
	}

	public void setSearchInfo(FlightSearchInfoDTO searchInfo) {
		this.searchInfo = searchInfo;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public String getTotalCustomerCredit() {
		return totalCustomerCredit;
	}

	public SessionTimeoutDataDTO getTimeoutDTO() {
		return timeoutDTO;
	}

	public boolean isSocialLoginEnabled() {
		return socialLoginEnabled;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

	public boolean isIntegrateMashreqWithLMSEnabled() {
		return integrateMashreqWithLMSEnabled;
	}

	public void setIntegrateMashreqWithLMSEnabled(boolean integrateMashreqWithLMSEnabled) {
		integrateMashreqWithLMSEnabled = integrateMashreqWithLMSEnabled;
	}

}
