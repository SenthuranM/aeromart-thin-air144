package com.isa.thinair.ibe.core.web.v2.action.payment.mtc;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.paymentbroker.api.constants.PaymentAPIConsts;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.opensymphony.xwork2.ActionChainResult;

/**
 * 
 * @author Primal Suaris
 *
 */
/**
 * This is specific servlet related to MTC Offline payment gateway integration.
 * MTCOfflinePaymentResponseHandlerAction?CartId=123&TotalmountTx=2050.75&Checksum=2cf0200e671964339236a857a2580f82
 * 
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Action.SHOWRESERVATION, type = ActionChainResult.class, value = StrutsConstants.Action.SHOWRESERVATION),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED) })
public class MTCLoadReservationAction extends BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(MTCLoadReservationAction.class);

	public String execute() {
		if (log.isDebugEnabled())
			log.debug("###Start.." + request.getRequestedSessionId());
		String forward = null;
		response.setContentType("text/html;charset=UTF-8");
		String cartID = request.getParameter("cartId");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String totalAmountTx = request.getParameter("totalAmountTx");
		String orderNumber = request.getParameter("orderNumber");
		String paymentSeqID = request.getParameter("pmtSeqId");
		String approvalCode = request.getParameter("approvalCode");
		String paymentCardType = request.getParameter("paymentCardType");
		String ThreeDSecureResult = request.getParameter("3DSecureResult");
		String eciValue = request.getParameter("eciValue");
		String bankName = request.getParameter("bankName");
		String expirationDate = request.getParameter("expirationDate");
		String ccn = request.getParameter("ccn");
		String paymentBrandName = request.getParameter("paymentBrandName");
		String checksum = request.getParameter("checksum");
		String apiChecksum = request.getParameter("apiChecksum");

		String bookURL = AppSysParamsUtil.getSecureIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_MTC_LOAD_URL_SUFFIX;

		Map<String, String> receiptyMap = getReceiptMap(request);
		Date date = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String today = formatter.format(date);
		try {
			PrintWriter out = response.getWriter();
			// Get message details sent from MTC Offline Gateway

			// Logging Request parameters
			StringBuilder strLog = new StringBuilder();
			strLog.append("cartID:").append(cartID).append(", ");
			strLog.append("Name :").append(name).append(", ");
			strLog.append("Email:").append(email).append(", ");
			strLog.append("totalAmountTx:").append(totalAmountTx).append(", ");
			strLog.append("OrderNumber:").append(orderNumber).append(", ");
			strLog.append("PaymentSeqID:").append(paymentSeqID).append(", ");
			strLog.append("approvalCode:").append(approvalCode).append(", ");
			strLog.append("paymentCardType:").append(paymentCardType).append(", ");
			strLog.append("ThreeDSecureResult:").append(ThreeDSecureResult).append(", ");
			strLog.append("eciValue:").append(eciValue).append(", ");
			strLog.append("bankName:").append(bankName).append(", ");
			strLog.append("expirationDate:").append(expirationDate).append(", ");
			strLog.append("ccn:").append(ccn).append(", ");
			strLog.append("paymentBrandName:").append(paymentBrandName).append(", ");
			strLog.append("apiChecksum:").append(apiChecksum).append(", ");
			strLog.append("checksum:").append(checksum).append(", ");
			strLog.append("SessionID:").append(request.getRequestedSessionId()).append(", ");

			log.info(strLog.toString());

			//FIXME note that MOBI-CASH (PG ID 13, inactive now) is using the same action
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					14, "MAD");
			PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			String storeID = ipgProps.getProperty("merchantId");
			String merchantSymenticKey = ipgProps.getProperty("merchantAccessCode");

			String checksumGenarated = "";
			// String dataMD5 = ipgURLPay + strStoreId + strCartId + strCount + strAmountTX + strEmail +
			// strSLKSecretKey;
			String dataMD5 = bookURL + storeID + cartID + totalAmountTx + email + merchantSymenticKey;

			try {
				String uRLEncodeData = URLEncoder.encode(dataMD5, "UTF-8");
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.reset();
				md.update((uRLEncodeData).getBytes());
				checksumGenarated = StringUtil.byteArrayToHexString(md.digest());
				checksumGenarated = checksumGenarated.toLowerCase();
				String check = checksumGenarated;
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("");
			}

			boolean isAccepted = false;
			boolean isResConfirmationSuccess = false;
			if (!checksumGenarated.equals(checksumGenarated)) {
				out.print("0;" + cartID + ";" + today + ";1");
				out.flush();
				out.close();
				return null;
			} else {
				isAccepted = true;
			}

			if (isAccepted) {
				request.setAttribute(WebConstants.PNR, getpnr(cartID));
				if (isGroupPNR(cartID)) {
					request.setAttribute(WebConstants.GROUP_PNR, getpnr(cartID));
				}
				request.setAttribute(ReservationWebConstnts.PARAM_HDNPARAMDATA, "EN^DIRECTLOARDPNR");

				forward = StrutsConstants.Action.SHOWRESERVATION;

				return forward;
			}
		} catch (Exception ex) {
			String mtcResponseCode = "0";

			if (ex instanceof ModuleException) {
				if (((ModuleException) ex).getExceptionCode().equals("Transaction.Already.Updated")) {

				}

				PrintWriter out;
				try {
					out = response.getWriter();

					out.print(mtcResponseCode + ";" + cartID + ";" + today + ";1");
					out.flush();
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					log.error("###Error...## Transaction.Already.Updated", ex);
				}

			}
		}
		return forward;
	}

	private Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	private String getpnr(String cartID) {
		String tempPayID = "";
		if (cartID != null) {
			String tempPayIDPNR = cartID.substring(1); // Remove the AppIndicator character result contains temPayid-PNR
			tempPayID = tempPayIDPNR.split("-")[1]; // get the tempPayID from temPayid-PNR
		}
		return tempPayID;
	}

	private boolean isGroupPNR(String cartID) {
		boolean isGroupPNR = false;
		if (cartID != null) {
			String tempPayIDPNR = cartID.substring(1); // Remove the AppIndicator character result contains temPayid-PNR
			isGroupPNR = SYSTEM.INT.name().equalsIgnoreCase(tempPayIDPNR.split("-")[2]); // get the tempPayID from
																							// temPayid-PNR
		}
		return isGroupPNR;
	}

}
