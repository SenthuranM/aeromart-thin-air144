package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;
import org.json.simple.parser.ParseException;

import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.dto.RedeemCalculateRQ;
import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.BookingType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.ibe.api.dto.BalanceTo;
import com.isa.thinair.ibe.api.dto.CustomerLoyalty;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.PaxDetailDTO;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.api.dto.SegInfoDTO;
import com.isa.thinair.ibe.api.dto.v2.SelectedPaymentMethodDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.generator.createres.ReservationHG;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.BalanceSummaryUtil;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlinePaymentAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(InterlinePaymentAction.class);

	private PaxDetailDTO paxDetail = new PaxDetailDTO();

	private FareQuoteTO fareQuote;

	private Collection<SegInfoDTO> flightSegments;

	private Collection<SegInfoDTO> flightDepDetails;

	private Collection<SegInfoDTO> flightRetDetails;

	private boolean blnReturn = false;

	private Collection<PaymentGateWayInfoDTO> paymentGateways;

	private Collection<PaymentGateWayInfoDTO> allPaymentGateways;

	private String pgOptions = "";

	private String securePath = "";

	private boolean success = true;

	private String messageTxt;

	private String defaultPayCurCode = "";

	private String insurance;

	private String paxWiseAnci;

	private CustomerLoyalty customerLoyalty;

	private String loyaltyPayOption;

	private String loyaltyCreditAmount;

	private boolean modifySegment = false;

	private boolean modifyAncillary = false;

	private String accessPoint;

	private boolean onHoldEnable = false;

	private String onHoldDisplayTime;

	private String paymentType;

	private String pnr;

	private String airportMessage;

	private boolean onHoldRestrictedBC = false;

	private ConfirmUpdateChargesTO updateCharge;

	private boolean modifyBalanceDisplayPayment;

	private boolean hasInsurance;

	private String userIp;

	private String requestSessionIdentifier;

	private boolean hasNopay;

	private Map<String, String> cardErrorInfo;

	private Collection<CountryPaymentCardBehaviourDTO> cardsInfo;

	private boolean addGroundSegment;

	private boolean oNDImageCapcha;

	private String oldAllSegments;

	private String contactEmail;

	private int totalSegmentCount;

	private boolean promotionPayment = false;

	private boolean makePayment = false;

	private boolean payFortOfflinePayment = false;

	private boolean payFortPayATHomeOfflinePayment = false;

	private String payFortMobileNumber = "";

	private String payFortEmail = "";

	private SelectedPaymentMethodDTO selectedPaymentMethodDTO;

	private boolean requoteFlightSearch = false;

	private String freeServiceDiscount;

	private String resPaxInfo;

	private String modifingFlightInfo;

	private boolean onHoldCreated;

	private String balanceQueryData;

	private String resPnr;

	private boolean groupPNR;

	private int paymentGatewayID;

	private boolean loyaltyBased;

	private boolean redeemLoyaltyPoints;

	private BigDecimal redeemRequestAmount;

	private BigDecimal totalRedeemedAmount;

	private Double remainingLoyaltyPoints = null;

	private String reservationStatus;
	
	private String hdnPGWPaymentMobileNumber = "";

	private String hdnPGWPaymentEmail = "";
	
	private String hdnPGWPaymentCustomerName = "";

	public static final String CHARGE_BY_VALUE = "V";
	public static final String CHARGE_BY_PERCENTAGE = "P";

	private String nameChangePaxData;

	private boolean nameChangeRequote;

	private boolean loggedUser;

	private boolean integrateMashreqWithLMSEnabled;

	private boolean adminFeeRegulationEnabled;

	private String errorMessageInfo;

	// voucher
	private VoucherRedeemRequest voucherRedeemRequest;

	private VoucherRedeemResponse voucherRedeemResponse;

	private String selCurrencyCode;

	private boolean canceledVouchersAvailable = false;

	private boolean disableRedeem = false;

	private String message;

	private boolean onLoad;

	private boolean voucherRedemption = false;

	private boolean voucherRedeemSuccess = true;

	private boolean removeVouchers = false;

	private boolean voucherEnabled;

	private boolean cashPayment = false;
	
	private boolean infantPaymentSeparated;

	@SuppressWarnings("unchecked")
	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		boolean loyaltyRedeemResponse = false;
		try {
			integrateMashreqWithLMSEnabled = AppSysParamsUtil.isIntegrateMashreqWithLMS();
			adminFeeRegulationEnabled = AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null);
			CommonMasterBD commonMasterBD = ModuleServiceLocator.getCommoMasterBD();
			boolean hideLoyalty = false;
			accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			Collection<ReservationPaxTO> paxList = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			String selectedCurrency = null;
			if (getSearchParams() != null) {
				selectedCurrency = getSearchParams().getSelectedCurrency();
			}
			PriceInfoTO priceInfoTO = null;
			infantPaymentSeparated = resInfo.isInfantPaymentSeparated();
			/*
			 * Payment retry. Payment page will be recreated using session data initial stage, required data will be
			 * display
			 */

			if (isPaymentRetry()) {
				IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
				validateData(postPayDTO);
				refreshReservationFlowData(postPayDTO);/* Update with session data */
				postPayDTO.removeExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD); /*
																				 * Remove External Charge For Payment
																				 * Retry
																				 */
				resInfo.removeExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD);
				resInfo.removeExternalCharge(EXTERNAL_CHARGES.JN_OTHER);
				priceInfoTO = postPayDTO.getPriceInfoTO();
				paxList = postPayDTO.getPaxList();
				flightSegmentTOs = postPayDTO.getSelectedFlightSegments();
				createSelectedFlights(flightSegmentTOs);
				resInfo.setTotalPriceWithoutExtChg(BeanUtil.getTotalPriceWithoutExtCharges(priceInfoTO));
				resInfo.setlCCClientReservationBalance(postPayDTO.getlCCClientReservationBalance());
				createselectedPaymentMethodDTO(postPayDTO);
				resPnr = postPayDTO.getPnr();
				removeLoyaltyDataInSession(resInfo, postPayDTO);
				removeCardPaymentDataInSession(resInfo, postPayDTO);
				removeLMSDataInSession(resInfo, postPayDTO);
				setOnHoldCreated(postPayDTO.isOnHoldCreated());
				if (getSearchParams() == null) {
					setSearchParams(new FlightSearchDTO());
					getSearchParams().setSelectedCurrency(postPayDTO.getSelectedCurrency());
					selectedCurrency = postPayDTO.getSelectedCurrency();
				} else if (getSearchParams().getSelectedCurrency() == null || "".equals(getSearchParams().getSelectedCurrency())) {
					getSearchParams().setSelectedCurrency(postPayDTO.getSelectedCurrency());
					selectedCurrency = postPayDTO.getSelectedCurrency();
				}

				if (paymentGatewayID == 0 && !loyaltyBased) {
					this.setPaymentGatewayID(postPayDTO.getPaymentGateWay().getPaymentGateway());
				}

			} else {
				boolean isRakInsurance = AppSysParamsUtil.isRakEnabled();
				isSuccessfulPaymentExit();
				if (!success) {
					return result;
				}

				String cabinClass = getSearchParams().getClassOfService();
				flightSegmentTOs = new ArrayList<FlightSegmentTO>();
				boolean isReturn = false;
				SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				isReturn = fltSegBuilder.isReturn();

				resInfo.setTotalSegmentCount(totalSegmentCount);
				priceInfoTO = resInfo.getPriceInfoTO();

				checkReservationDataIntergrity(priceInfoTO, flightSegmentTOs);

				/* Clear External charges */
				resInfo.getSelectedExternalCharges().clear();
				resInfo.setNoPay(false);

				List<LCCInsuranceQuotationDTO> insuranceQuotation = null;
				SelectedFltSegBuilder totalFltSegBuilder = null;
				LCCInsuredJourneyDTO insJrnyDto = null;

				if (SessionUtil.getIbePromotionPaymentPostDTO(request) == null) {
					if (!addGroundSegment) {
						insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(flightSegmentTOs, isReturn, getTrackInfo()
								.getOriginChannelId());
					} else {
						totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);
						totalFltSegBuilder.getSelectedFlightSegments().addAll(flightSegmentTOs);
						insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(totalFltSegBuilder.getSelectedFlightSegments(),
								isReturn, getTrackInfo().getOriginChannelId());
					}
				}

				if (!makePayment) {
					insuranceQuotation = AncillaryJSONUtil.getInsuranceQuotation(insurance, insJrnyDto);
				}

				/* old insurance, only the difference should be charged */
				/**
				 * TODO: Review- what is the reason pnr as multiple values
				 */
				if (resPnr != null && resPnr.indexOf(",") > 0) {
					resPnr = resPnr.split(",")[0];
				}
				paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotation,
						getApplicationEngine(getTrackInfo().getOriginChannelId()),
						(addGroundSegment == true ? totalFltSegBuilder.getSelectedFlightSegments() : flightSegmentTOs), null,
						resInfo.getAnciOfferTemplates());

				/* On-hold availability */
				setOnholdData(resInfo, priceInfoTO, flightSegmentTOs, cabinClass, paxList);

				Collections.sort(flightSegmentTOs);

				if (priceInfoTO != null) {
					ReservationUtil.setOndSequence(priceInfoTO, flightSegmentTOs);
				}

				if (getSearchParams().isFlexiSelected()) {
					ReservationUtil.applyFlexiCharges(paxList, getSearchParams().getOndSelectedFlexi(), resInfo.getPriceInfoTO(),
							flightSegmentTOs);
				}

				Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModSegAnciTotal = null;
				if (resInfo.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI) && modifySegment && resPaxInfo != null
						&& !resPaxInfo.equals("")
						&& !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(resInfo.getReservationStatus())) {
					Collection<LCCClientReservationPax> colPax = ReservationUtil.transformJsonPassengers(resPaxInfo);
					List<String> modSegList = ReservationUtil.getModifyingFlightRefNumberList(modifingFlightInfo);
					paxWiseModSegAnciTotal = ExternalChargeUtil.getPaxWiseModifyingSegmentAnciTotal(flightSegmentTOs, modSegList,
							colPax);
				}
				if (flightSegmentTOs.size() > 0) {
					ReservationUtil.applyAncillaryTax(paxList, flightSegmentTOs.get(0), resInfo, paxWiseModSegAnciTotal);
				}

				if (requoteFlightSearch) {
					performRequoteBalanceCalculation(resInfo, paxList);
					ReservationUtil.updateEffectiveTax(paxList, resInfo.getlCCClientReservationBalance().getPaxEffectiveTax(), resInfo.isInfantPaymentSeparated());
				}

				if (modifyAncillary && resInfo != null && resInfo.isApplyPenaltyForAnciModification()) {
					// Add credit amount created by anci modification as a penalty amount
					if (flightSegmentTOs.size() > 0) {
						ReservationUtil.applyAncillaryModifyPenalty(paxList, flightSegmentTOs.get(0));
					}
				}
			}

			if (AppSysParamsUtil.isLMSEnabled() && redeemLoyaltyPoints) {
				loyaltyRedeemResponse = redeemLoyaltyPoints(flightSegmentTOs, paxList);
				if (!loyaltyRedeemResponse) {
					success = false;
					return result;
				}
			}

			BigDecimal totalWithoutAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal creditableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			LoyaltyPaymentOption loyalPayOpt = LoyaltyPaymentOption.getEnum(loyaltyPayOption);
			if (resInfo.getTotalPriceWithoutExtChg() != null) {
				totalWithoutAnci = new BigDecimal(resInfo.getTotalPriceWithoutExtChg());
			}

			if (resInfo.getDiscountInfo() != null
					&& PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(resInfo.getDiscountInfo()
							.getPromotionType()) && freeServiceDiscount != null && !"".equals(freeServiceDiscount)) {
				resInfo.setDiscountAmount(AccelAeroCalculator.scaleDefaultRoundingDown(new BigDecimal(freeServiceDiscount)));
			}

			/* Modify Segment */
			if (resInfo.getCreditableAmount() != null && (modifySegment || addGroundSegment)) {
				BigDecimal reservationCredit = BalanceSummaryUtil.getTotalPaxCredit(resInfo.getPaxCreditMap(resPnr));
				creditableAmount = AccelAeroCalculator.add(
						resInfo.getlCCClientReservationBalance().getSegmentSummary().getCurrentRefunds(),
						reservationCredit.negate()).negate();
			}

			BigDecimal loyaltyCredit = BigDecimal.ZERO;
			BigDecimal ccFee = BigDecimal.ZERO;
			BigDecimal utilizedCredit = BigDecimal.ZERO;
			BigDecimal reservationBalance = BigDecimal.ZERO;
			BigDecimal totalPaxExtChgs = BigDecimal.ZERO;
			BigDecimal lmsPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal voucherPayment = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (modifyAncillary || makePayment) {
				Map<String, BigDecimal> paxCreditMap = resInfo.getPaxCreditMap(resPnr);
				for (ReservationPaxTO pax : paxList) {
					BigDecimal paxChgs = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal paxCredit = paxCreditMap.get(pax.getTravelerRefNumber());
					BigDecimal addAnciTotal = ReservationUtil.getPaxExtCharges(pax.getExternalCharges());
					BigDecimal removeAnciTotal = pax.getToRemoveAncillaryTotal();
					BigDecimal updateAnciTotal = pax.getToUpdateAncillaryTotal();
					paxChgs = AccelAeroCalculator.add(paxChgs, addAnciTotal, updateAnciTotal, removeAnciTotal.negate());

					if (paxCredit != null) {
						if (paxCredit.negate().compareTo(BigDecimal.ZERO) > 0 && paxChgs.compareTo(BigDecimal.ZERO) > 0) {
							if (paxCredit.negate().compareTo(paxChgs) > 0) {
								utilizedCredit = AccelAeroCalculator.add(utilizedCredit, paxChgs.negate());
							} else {
								utilizedCredit = AccelAeroCalculator.add(utilizedCredit, paxCredit);
							}
						} else if (paxCredit.compareTo(BigDecimal.ZERO) > 0) {
							reservationBalance = AccelAeroCalculator.add(reservationBalance, paxCredit);
						}
					}

					if (AccelAeroCalculator.isGreaterThan(paxChgs, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						totalPaxExtChgs = AccelAeroCalculator.add(totalPaxExtChgs, paxChgs);
					}
				}
				creditableAmount = utilizedCredit;
			}

			if (SessionUtil.getIbePromotionPaymentPostDTO(request) != null) {
				if (SessionUtil.getIbePromotionPaymentPostDTO(request).getTotalPayment() != null) {

					String nextSeatChargeStr = SessionUtil.getIbePromotionPaymentPostDTO(request).getTotalPayment();
					BigDecimal nextSeatCharge = new BigDecimal(nextSeatChargeStr);
					reservationBalance = AccelAeroCalculator.add(reservationBalance, nextSeatCharge);
					promotionPayment = true;

					// ExternalChgDTO externalChargeDTO = calculateCCCharges(reservationBalance,
					// PaxUtil.getPayablePaxCount(paxList, false), flightSegmentTOs.size(),
					// ChargeRateOperationType.MAKE_ONLY);
					// log.info("***************Before admin fee charge adding: " + reservationBalance);
					// reservationBalance = AccelAeroCalculator.add(reservationBalance, externalChargeDTO.getAmount());
					// log.info("***************after admin fee charge adding: " + reservationBalance);

				}
			}

			BigDecimal totalForCCCal = new BigDecimal(totalWithoutAnci.toString());

			if (!modifyAncillary) {
				totalPaxExtChgs = ReservationUtil.getTotalAncillaryCharges(paxList);
			}

			if (cashPayment) {
				totalForCCCal = AccelAeroCalculator.add(totalForCCCal, totalPaxExtChgs, (addGroundSegment
						? creditableAmount
						: reservationBalance));
			} else {
				totalForCCCal = AccelAeroCalculator.add(totalForCCCal, totalPaxExtChgs, (addGroundSegment
						? creditableAmount
						: reservationBalance), resInfo.getDiscountAmount(true).negate());
			}
			/* Transaction Frees for Modification */
			if (modifySegment) {
				BalanceTo balanceToPayAmount = BalanceSummaryUtil.getTotalBalanceToPayAmount(
						resInfo.getlCCClientReservationBalance(), null, resInfo.isInfantPaymentSeparated());
				totalForCCCal = balanceToPayAmount.getBalanceToPay();
				if (totalForCCCal.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) <= 0) {
					totalForCCCal = AccelAeroCalculator.getDefaultBigDecimalZero();
					utilizedCredit = totalForCCCal;
				}
			}

			ExternalChgDTO ccFeeTax = null;

			boolean calculateCCFee = true;
			if (totalForCCCal.compareTo(utilizedCredit.negate()) > 0) {
				totalForCCCal = AccelAeroCalculator.add(totalForCCCal, utilizedCredit);

				lmsPayment = CustomerUtil.getLMSPaymentAmount(resInfo);

				if (!(AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
						.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null))) {
					totalForCCCal = AccelAeroCalculator.subtract(totalForCCCal, lmsPayment);
				}

				voucherPayment = CustomerUtil.getVoucherPaymentAmount(resInfo);
				totalForCCCal = AccelAeroCalculator.subtract(totalForCCCal, voucherPayment);

				if (totalForCCCal.compareTo(BigDecimal.ZERO) == 0) {
					calculateCCFee = false;
				}

				if (loyalPayOpt != LoyaltyPaymentOption.TOTAL) {
					if (loyalPayOpt == LoyaltyPaymentOption.PART) {
						loyaltyCredit = AccelAeroCalculator.scaleValueDefault(new BigDecimal(loyaltyCreditAmount));
						totalForCCCal = AccelAeroCalculator.subtract(totalForCCCal, loyaltyCredit);
					}
					if (totalForCCCal.compareTo(BigDecimal.ZERO) == 0) {
						resInfo.removeExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD);
						resInfo.removeExternalCharge(EXTERNAL_CHARGES.JN_OTHER);
						calculateCCFee = false;
					}
				} else {
					resInfo.removeExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD);
					resInfo.removeExternalCharge(EXTERNAL_CHARGES.JN_OTHER);
					calculateCCFee = false;
				}

			} else {
				if (SessionUtil.getIbePromotionPaymentPostDTO(request) == null) {
					hideLoyalty = true;
					resInfo.setNoPay(true);
					hasNopay = true;
				}
				calculateCCFee = false;
			}

			String marketingCarrier = resInfo.getMarketingCarrier();
			boolean isPaymentCarrierSame = true;
			if (marketingCarrier != null) {
				isPaymentCarrierSame = marketingCarrier.equals(AppSysParamsUtil.getDefaultCarrierCode());
			}

			if (calculateCCFee) {
				Integer operationalType = ((modifySegment || modifyAncillary || addGroundSegment) ? ChargeRateOperationType.MODIFY_ONLY
						: ChargeRateOperationType.MAKE_ONLY);

				ExternalChgDTO ccChgDTO = calculateCCCharges(totalForCCCal, PaxUtil.getPayablePaxCount(paxList, modifyAncillary),
						(modifyAncillary ? ReservationUtil.getAnciUpdatedSegmentCount(paxList) : flightSegmentTOs.size()),
						operationalType, paymentGatewayID);

				if (ccChgDTO != null && ccChgDTO.getAmount().compareTo(BigDecimal.ZERO) > 0) {

					if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
							.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null))
							&& resInfo.getReservationStatus() != null && resInfo.getReservationStatus().equals("OHD")) {

						if (isPaymentCarrierSame) {
							ccFee = BigDecimal.ZERO;
							ccFeeTax = ReservationUtil.getApplicableServiceTax(resInfo, EXTERNAL_CHARGES.JN_OTHER,
									operationalType);
							if (ccFeeTax != null) {
								ccFeeTax.setAmount(BigDecimal.ZERO);
							}
						} else {
							ccFee = ccChgDTO.getAmount();
							resInfo.addExternalCharge(ccChgDTO);
							ccFeeTax = ReservationUtil.getApplicableServiceTax(resInfo, EXTERNAL_CHARGES.JN_OTHER,
									operationalType);
						}

					} else if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
							&& !AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null)) {
						ccFee = BigDecimal.ZERO;
						ccFeeTax = ReservationUtil.getApplicableServiceTax(resInfo, EXTERNAL_CHARGES.JN_OTHER, operationalType);
						if (ccFeeTax != null) {
							ccFeeTax.setAmount(BigDecimal.ZERO);
						}
					} else {
						ccFee = ccChgDTO.getAmount();
						resInfo.addExternalCharge(ccChgDTO);
						ccFeeTax = ReservationUtil.getApplicableServiceTax(resInfo, EXTERNAL_CHARGES.JN_OTHER, operationalType);
					}
				}

				/* Update new credit card free for payment retry */
				if (isPaymentRetry()) {
					SessionUtil.getIBEReservationPostPaymentDTO(request).addExternalCharge(ccChgDTO);
					if (ccFeeTax != null) {
						SessionUtil.getIBEReservationPostPaymentDTO(request).addExternalCharge(ccFeeTax);
					}
				}
			}

			resInfo.setCreditCardFee(ccFee);
			resInfo.setLoyaltyCredit(new BigDecimal(loyaltyCredit.toString()));
			resInfo.setLoyaltyPayOption(loyalPayOpt);
			resInfo.setReservationCredit(utilizedCredit);
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			boolean creditDiscount = (resInfo.getDiscountInfo() != null) ? DiscountApplyAsTypes.CREDIT.equals(resInfo
					.getDiscountInfo().getDiscountAs()) : true;

			boolean promoExists = (resInfo.getDiscountInfo() != null) && (resInfo.getDiscountInfo().getPromotionId() != null);

			if (ccFeeTax != null) {
				if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
						.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null))
						&& resInfo.getReservationStatus() != null
						&& resInfo.getReservationStatus().equals("OHD")
						&& isPaymentCarrierSame) {
					ccFee = BigDecimal.ZERO;
				} else {
					ccFee = AccelAeroCalculator.add(ccFee, ccFeeTax.getTotalAmount());
				}
			}

			BigDecimal voucherRedeemdAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (resInfo.getPayByVoucherInfo() != null) {
				if(!voucherRedemption) {
					voucherRedeemdAmount = resInfo.getPayByVoucherInfo().getRedeemedTotal();
				}
			}
			if (creditableAmount == null) {
				creditableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			}
			this.fareQuote = ReservationUtil.fillFareQuote(paxList, selectedCurrency, totalWithoutAnci, ccFee,
					resInfo.getTotalFare(), resInfo.getTotalTax(), resInfo.getTotalTaxSurchages(), loyalPayOpt, loyaltyCredit,
					modifySegment, resInfo.getlCCClientReservationBalance(), creditableAmount, modifyAncillary,
					reservationBalance, addGroundSegment, exchangeRateProxy, promoExists, resInfo.getDiscountAmount(false),
					creditDiscount, resInfo.getIbeReturnFareDiscountAmount(), requoteFlightSearch,
					CustomerUtil.getLMSPaymentAmount(resInfo), voucherRedeemdAmount, resInfo.isInfantPaymentSeparated());
			
			BigDecimal payable = new BigDecimal(this.fareQuote.getTotalPayable());

			if (AppSysParamsUtil.isVoucherEnabled() && voucherRedemption) {
				if (!removeVouchers) {
					this.redeemVouchers(payable, (List<ReservationPaxTO>) paxList);
					if (voucherRedeemResponse != null) {
						fareQuote.setTotalPayable(AccelAeroCalculator.subtract(new BigDecimal(fareQuote.getTotalPayable()),
								new BigDecimal(voucherRedeemResponse.getRedeemedTotal())).toString());
						payable = new BigDecimal(this.fareQuote.getTotalPayable());
					}
				} else {
					PayByVoucherInfo payByVoucherInfo = resInfo.getPayByVoucherInfo();
					payByVoucherInfo.setUnsuccessfulAttempts(resInfo.getPayByVoucherInfo().getUnsuccessfulAttempts());
					resInfo.setPayByVoucherInfo(payByVoucherInfo);
					VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
					try {
						voucherDelegate.unblockRedeemedVouchers(resInfo.getPayByVoucherInfo().getVoucherIDList());
					} catch (ModuleException ex) {
						log.error("InterLinePaymentAction ==> unblockRedeemedVouchers()", ex);
					}
				}
			}
			resInfo.setBalanceToPay(payable.compareTo(BigDecimal.ZERO) > 0);

			if (promotionPayment) {
				// while in next seat promo payments, no other payments to be made. Offers only for confirm reservation
				// with base currency
				this.fareQuote.setHasNextSeatPromoCharge(true);
				this.fareQuote.setNextSeatPromoCharge(AccelAeroCalculator.formatAsDecimal(reservationBalance));
				this.fareQuote.setSelectedCurrency(AppSysParamsUtil.getBaseCurrency());
			}

			// Identify the card payment currency
			Currency currency = commonMasterBD.getCurrency(selectedCurrency);
			if (currency != null && Currency.STATUS_ACTIVE.equals(currency.getStatus())
					&& currency.getCardPaymentVisibility() == 1 && currency.getDefaultIbePGId() != null) {
				if (exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency, ApplicationEngine.IBE) != null) {
					setDefaultPayCurCode(currency.getCurrencyCode());
				}
			}

			if (getDefaultPayCurCode() == null) {
				// Card payment not supported for selected currency, so choosing
				// default pay currency
				setDefaultPayCurCode(AppSysParamsUtil.getDefaultPGCurrency());
			}

			if (priceInfoTO != null) {
				FareTypeTO fareTypeTo = priceInfoTO.getFareTypeTO();
				onHoldRestrictedBC = fareTypeTo.isOnHoldRestricted();
			}

			boolean isOfflinePaymentsEnabled = (onHoldCreated || ReservationUtil.isOfflineBookingEligible(priceInfoTO,
					flightSegmentTOs, ResOnholdValidationDTO.IBE_PAYMENT_VALIDATION, paxList, null, getClientInfoDTO()
							.getIpAddress(), resInfo.getSelectedSystem(), isCreateReservation(), getTrackInfo()));

			reservationStatus = resInfo.getReservationStatus(); // to identify reservation status for offline payment
																// methods
			if (AppSysParamsUtil.isCreditCardPaymentsEnabled()) {
				boolean firstSegExists = (flightSegmentTOs != null && flightSegmentTOs.get(0) != null);
				String firstSegment = firstSegExists ? flightSegmentTOs.get(0).getSegmentCode(): null;
				Object[] pgArr = getPaymentgateWay(exchangeRateProxy, isOfflinePaymentsEnabled, resInfo,firstSegment);
				this.paymentGateways = (Collection<PaymentGateWayInfoDTO>) pgArr[0];
				this.allPaymentGateways = (Collection<PaymentGateWayInfoDTO>) pgArr[1];
			}

			resInfo.setFromPaymentPage(true);
			this.securePath = IBEModuleUtils.getConfig().getSecureIBEUrl();
			userIp = BookingUtil.getURLData(request).getUserIp();
			fillCustomerLoyalty(hideLoyalty);

			/* Load Airport Messages */
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessagesForFlightSegmentWise(flightSegmentTOs,
					ReservationInternalConstants.AirportMessageSalesChannel.IBE,
					ReservationInternalConstants.AirportMessageStages.CARD_PAYMENT, SessionUtil.getLanguage(request));

			if (!AppSysParamsUtil.isModifySegmentBalanceDisplayAvailabilitySearch() && modifySegment) {
				updateCharge = BalanceSummaryUtil.getChargesDetails(resInfo.getlCCClientReservationBalance(), paxList, resInfo,
						selectedCurrency, SessionUtil.getLanguage(request), resPnr, getSearchParams().isQuoteOutboundFlexi(),
						getSearchParams().isQuoteInboundFlexi(), AppSysParamsUtil.isRequoteEnabled());
				modifyBalanceDisplayPayment = true;
			}
			cardErrorInfo = ErrorMessageUtil.getCardErrors(request);

			if (modifyAncillary || requoteFlightSearch || makePayment) {
				errorMessageInfo = ErrorMessageUtil.getClientErrors(SessionUtil.getLanguage(request));
			}

		} catch (ModuleException me) {
			revertLoyaltyRedemption(loyaltyRedeemResponse);
			errorTrack(me);
			messageTxt = I18NUtil.getMessage(me.getExceptionCode(), SessionUtil.getLanguage(request));
		} catch (RuntimeException re) {
			revertLoyaltyRedemption(loyaltyRedeemResponse);
			errorTrack(re);
			messageTxt = getServerErrorMessage(request, ExceptionConstants.SERVER_OPERATION_FAIL);
		} catch (Exception e) {
			revertLoyaltyRedemption(loyaltyRedeemResponse);
			errorTrack(e);
			messageTxt = e.getMessage();
		}

		return result;
	}

	private boolean isCreateReservation() {
		boolean createReservation = true;
		if (makePayment || modifySegment || modifyAncillary || addGroundSegment) {
			createReservation = false;
		}
		return createReservation;
	}

	private void createSelectedFlights(List<FlightSegmentTO> flightSegmentTOs) {
		flightSegments = new ArrayList<SegInfoDTO>();
		if (flightSegmentTOs != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yy");
			SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
			final String dateValueFimater = "yyMMddHHmm";

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {

				SegInfoDTO segInfoDTO = new SegInfoDTO();
				segInfoDTO.setSegmentShortCode(flightSegmentTO.getSegmentCode());
				segInfoDTO.setSegmentCode(ReservationHG.getSegementDetailsWithTermainals(flightSegmentTO.getSegmentCode(), null,
						flightSegmentTO.getDepartureTerminalName(), flightSegmentTO.getArrivalTerminalName()));
				segInfoDTO.setFlightNumber(flightSegmentTO.getFlightNumber());
				segInfoDTO.setCarrierCode(flightSegmentTO.getOperatingAirline());
				segInfoDTO.setDepartureDate(formatter.format(flightSegmentTO.getDepartureDateTime()));

				String departTime = timeFormatter.format(flightSegmentTO.getDepartureDateTime());
				String arrivalTime = timeFormatter.format(flightSegmentTO.getArrivalDateTime());

				segInfoDTO.setDepartureTime(departTime);
				segInfoDTO.setArrivalDate(formatter.format(flightSegmentTO.getArrivalDateTime()));
				segInfoDTO.setArrivalTime(arrivalTime);
				segInfoDTO.setDepartureTimeLong(flightSegmentTO.getDepartureDateTime().getTime());
				segInfoDTO.setArrivalDateValue(DateUtil.formatDate(flightSegmentTO.getArrivalDateTime(), dateValueFimater));
				segInfoDTO.setDepartureDateValue(DateUtil.formatDate(flightSegmentTO.getDepartureDateTime(), dateValueFimater));
				segInfoDTO.setDepartureTimeZuluLong(flightSegmentTO.getDepartureDateTimeZulu().getTime());
				segInfoDTO.setArrivalTimeLong(flightSegmentTO.getArrivalDateTime().getTime());
				segInfoDTO.setArrivalTimeZuluLong(flightSegmentTO.getArrivalDateTimeZulu().getTime());
				segInfoDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber() + "#" + flightSegmentTO.getRouteRefNumber());
				segInfoDTO.setDomesticFlight(flightSegmentTO.isDomesticFlight());
				segInfoDTO.setReturnFlag(flightSegmentTO.isReturnFlag());

				segInfoDTO.setArrivalTerminalName(flightSegmentTO.getArrivalTerminalName());
				segInfoDTO.setDepartureTerminalName(flightSegmentTO.getDepartureTerminalName());

				segInfoDTO.setDuration(CommonUtil.wrapWithHM(CommonUtil.getTimeHHMM(flightSegmentTO.getArrivalDateTimeZulu()
						.getTime() - flightSegmentTO.getDepartureDateTimeZulu().getTime())));
				// set setFlightSeqNo empty for fix the null display in booking summary panel
				segInfoDTO.setFlightSeqNo("");
				flightSegments.add(segInfoDTO);

			}
		}

	}

	private void errorTrack(Exception ex) {
		log.error("InterlinePaymentAction==>", ex);
		success = false;
		SessionUtil.resetSesionDataInError(request);
	}

	private void removeCardPaymentDataInSession(IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO postPayDTO) {
		BigDecimal zero = AccelAeroCalculator.getDefaultBigDecimalZero();
		resInfo.setCreditCardFee(zero);
		postPayDTO.setCreditCardFee(zero);

		resInfo.setReservationCredit(zero);
		postPayDTO.setReservationCredit(zero);
	}

	private void removeLoyaltyDataInSession(IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO postPayDTO) {
		if (!LoyaltyPaymentOption.NONE.equals(postPayDTO.getLoyaltyPayOption())) {
			postPayDTO.setLoyaltyPayOption(LoyaltyPaymentOption.NONE);
			postPayDTO.setLoyaltyCredit(AccelAeroCalculator.getDefaultBigDecimalZero());

			resInfo.setLoyaltyPayOption(LoyaltyPaymentOption.NONE);
			resInfo.setLoyaltyCredit(AccelAeroCalculator.getDefaultBigDecimalZero());
		}
	}

	private void removeLMSDataInSession(IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO postPayDTO) {
		if (postPayDTO.getCarrierWiseLoyaltyPaymentInfo() != null) {
			postPayDTO.setCarrierWiseLoyaltyPaymentInfo(null);
			resInfo.setCarrierWiseLoyaltyPaymentInfo(null);
		}
	}

	@SuppressWarnings("unchecked")
	private void createselectedPaymentMethodDTO(IBEReservationPostPaymentDTO postPayDTO) throws ModuleException {
		if (postPayDTO != null) {
			Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap = postPayDTO.getTemporyPaymentMap();
			if (mapTempPayMap != null) {
				CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = mapTempPayMap.get(LCCClientApiUtils
						.getTmpPayIdFromTmpPayMap(mapTempPayMap));
				selectedPaymentMethodDTO = new SelectedPaymentMethodDTO();
				selectedPaymentMethodDTO.setCardType(lccClientCreditCardPaymentInfo.getType());
				selectedPaymentMethodDTO.setGatewayID(lccClientCreditCardPaymentInfo.getIpgIdentificationParamsDTO().getIpgId());
				selectedPaymentMethodDTO.setLoyaltyOption(String.valueOf(postPayDTO.getLoyaltyPayOption()));
				selectedPaymentMethodDTO.setLoyaltyCredit(String.valueOf(postPayDTO.getLoyaltyCredit()));
			}
		}
	}

	private void setOnholdData(IBEReservationInfoDTO resInfo, PriceInfoTO priceInfoTO, List<FlightSegmentTO> flightSegmentTOs,
			String cabinClass, Collection<ReservationPaxTO> paxList) throws ModuleException {
		Date depTimeZulu = null;
		OnHold onHoldApp = OnHold.DEFAULT;
		Date pnrReleaseTime = null;
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = null;

		if (priceInfoTO != null) {
			FareTypeTO fareTypeTo = priceInfoTO.getFareTypeTO();
			onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(flightSegmentTOs, fareTypeTo);
			onHoldReleaseTimeDTO.setCabinClass(cabinClass);
		}
		
		if (isKioskApplication()) {
			onHoldApp = OnHold.KIOSK;
		} else if (SalesChannelsUtil.SALES_CHANNEL_WEB == getTrackInfo().getOriginChannelId()) {
			onHoldApp = OnHold.IBE;
		} else if (SalesChannelsUtil.SALES_CHANNEL_IOS == getTrackInfo().getOriginChannelId()) {
			onHoldApp = OnHold.IOS;
		} else if (SalesChannelsUtil.SALES_CHANNEL_ANDROID == getTrackInfo().getOriginChannelId()) {
			onHoldApp = OnHold.ANDROID;
		}

		if (makePayment || modifySegment || modifyAncillary || addGroundSegment) {
			onHoldEnable = false;
		} else if (isKioskApplication() && AppSysParamsUtil.isOnHoldEnable(onHoldApp)) {
			depTimeZulu = ReservationUtil.getDepartureTimeZulu(flightSegmentTOs);
			pnrReleaseTime = ReservationUtil.calculateOnHoldReleaseTime(onHoldApp, depTimeZulu, onHoldReleaseTimeDTO);
			onHoldEnable = ReservationUtil.isOnHoldEnable(onHoldApp, pnrReleaseTime);
		} else if (AppSysParamsUtil.isOnHoldEnable(onHoldApp) && resInfo.isAllowOnhold()) {
			oNDImageCapcha = AppSysParamsUtil.isOHDImageCapcha();
			depTimeZulu = ReservationUtil.getDepartureTimeZulu(flightSegmentTOs);
			pnrReleaseTime = ReservationUtil.calculateOnHoldReleaseTime(onHoldApp, depTimeZulu, onHoldReleaseTimeDTO);
			onHoldEnable = ReservationUtil.isOnHoldEnable(onHoldApp, pnrReleaseTime);
			if (onHoldEnable) {
				onHoldEnable = onHoldEnable
						&& ModuleServiceLocator.getAirproxySearchAndQuoteBD().isOnholdEnabled(flightSegmentTOs,
								ReservationUtil.getReservationPaxList(paxList), contactEmail,
								ResOnholdValidationDTO.COMBINE_VALIDATION, getTrackInfo().getOriginChannelId(),
								resInfo.getSelectedSystem(), getClientInfoDTO().getIpAddress(), getTrackInfo());
				resInfo.setReservationOnholdableCombineValidation(onHoldEnable);
			}

		} else {
			onHoldEnable = false;
		}

		if (onHoldEnable && (paymentType == null || paymentType.isEmpty())) {
			paymentType = BookingType.ONHOLD.toString();
		}

		if (priceInfoTO != null) {
			FareTypeTO fareTypeTo = priceInfoTO.getFareTypeTO();
			onHoldRestrictedBC = fareTypeTo.isOnHoldRestricted();

			if (onHoldEnable) {
				onHoldDisplayTime = CommonUtil.wrapWithDDHHMM(ReservationUtil.getOnHoldDisplayTime(onHoldApp, pnrReleaseTime));
			}
		}
	}

	private void setMakePaymentFlowData(IBEReservationInfoDTO resInfo) {
		if (makePayment) {
			String groupPNR = request.getParameter("groupPNR");
			resInfo.setSelectedSystem(SYSTEM.AA);
			if (groupPNR != null && "true".equals(groupPNR)) {
				resInfo.setSelectedSystem(SYSTEM.INT);
			}
		}
	}

	/**
	 * Validate reservation data - As we are keeping price quote in session, user can change data using separate browser
	 * session
	 */
	private void checkReservationDataIntergrity(PriceInfoTO priceInfoTO, List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		if (!isRequoteFlightSearch()) {
			if (!modifyAncillary && !makePayment) {
				/* Validate Request Flight Details and Session Flight Details */
				if (SessionUtil.getRequestSessionIdentifier(request) == null || requestSessionIdentifier == null) {
					throw new ModuleException("msg.multiple.invalid.flight");
				} else if (SessionUtil.getRequestSessionIdentifier(request) != null
						&& !SessionUtil.getRequestSessionIdentifier(request).equals(requestSessionIdentifier)) {
					throw new ModuleException("msg.multiple.invalid.flight");
				}
				/* Own airline flight details validate */
				ReservationUtil.validatePassengerFlightDetails(flightSegmentTOs, priceInfoTO);
			} else {
				if (priceInfoTO != null) {
					throw new ModuleException("msg.multiple.invalid.flight");
				}
			}
		}
	}

	private boolean isKioskApplication() {
		boolean isKiosk = false;
		if (CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint)) {
			isKiosk = true;
		}
		return isKiosk;
	}

	private void isSuccessfulPaymentExit() {
		String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
		if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
				&& !tempPaymentPNR.trim().isEmpty()) {
			messageTxt = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
			success = false;
		}
	}

	private void refreshReservationFlowData(IBEReservationPostPaymentDTO postPayDTO) {
		if (postPayDTO != null) {
			if (postPayDTO.isModifySegment()) {
				modifySegment = true;
			} else if (postPayDTO.isAddModifyAncillary()) {
				modifyAncillary = true;
			} else if (postPayDTO.isAddGroundSegment()) {
				addGroundSegment = true;
			} else if (postPayDTO.isMakePayment()) {
				makePayment = true;
			}

			if (postPayDTO.isRequoteFlightSearch()) {
				requoteFlightSearch = true;
			}

			if (postPayDTO.isRequoteFlightSearch()) {
				requoteFlightSearch = true;
			}
		}
	}

	/**
	 * Validate data
	 * 
	 * @param data
	 * @throws ModuleException
	 */
	private void validateData(Object data) throws ModuleException {
		if (data == null) {
			throw new ModuleException("server.default.operation.fail");
		}
	}

	private boolean isPaymentRetry() {
		return RequestParameterUtil.isPaymentRetry(request);
	}

	private void fillCustomerLoyalty(boolean hideLoyalty) throws ModuleException {
		customerLoyalty = new CustomerLoyalty();
		if (hideLoyalty) {
			customerLoyalty.setEnabled(false);
		} else {
			boolean isOwnAirline = (SessionUtil.getIBEreservationInfo(request).getSelectedSystem() == SYSTEM.AA);
			String loyaltyAcc = SessionUtil.getLoyaltyAccountNo(request);
			int customerId = SessionUtil.getCustomerId(request);

			if (loyaltyAcc == null && customerId >= 0) {
				LoyaltyCustomerProfile loyaltyProfile = ModuleServiceLocator.getLoyaltyCustomerBD()
						.getLoyaltyCustomerByCustomerId(customerId);

				if (loyaltyProfile != null) {
					loyaltyAcc = loyaltyProfile.getLoyaltyAccountNo();
					SessionUtil.setLoyaltyAccountNo(request, loyaltyAcc);
				}
			}

			if (isOwnAirline && AppSysParamsUtil.isLoyalityEnabled() && customerId >= 0 && loyaltyAcc != null) {
				BigDecimal totCredit = ModuleServiceLocator.getLoyaltyCreditBD().getTotalNonExpireCredit(loyaltyAcc);
				if (totCredit == null)
					totCredit = BigDecimal.ZERO;
				// customerLoyalty.setCreditAmount(AccelAeroCalculator.formatAsDecimal(totCredit));
				customerLoyalty.setCreditAmount(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
						.scaleDefaultRoundingDown(totCredit)));
				customerLoyalty.setAccountNo(loyaltyAcc);
				if (totCredit.compareTo(BigDecimal.ZERO) > 0) {
					customerLoyalty.setEnabled(true);
				} else {
					customerLoyalty.setEnabled(false);
				}
			} else {
				customerLoyalty.setEnabled(false);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private ExternalChgDTO calculateCCCharges(BigDecimal totalTicketPriceExcludingCC, int payablePaxCount, int segmentCount,
			Integer rateApplicableType, int paymentGatewayId) throws ModuleException {

		ExternalChgDTO externalChgDTO = null;

		if (paymentGatewayID > 0
				|| (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
						.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null))) {
			Collection colEXTERNAL_CHARGES = new ArrayList();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
			Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(
					colEXTERNAL_CHARGES, null, rateApplicableType);
			externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);

			// Check for Payment gateway wise charges and override if exists.
			PaymentGatewayWiseCharges pgwCharge = null;

			boolean isCCFeeFromPGW = ModuleServiceLocator.getPaymentBrokerBD().isCreditCardFeeFromPGW(paymentGatewayId);

			if (!isCCFeeFromPGW) {
				pgwCharge = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge(paymentGatewayId);
				if (pgwCharge != null && pgwCharge.getChargeId() >= 0) {
					// valid charge is defined for the selected pgw
					if (pgwCharge.getValuePercentageFlag() != CHARGE_BY_VALUE) {
						externalChgDTO.setRatioValueInPercentage(true);
						externalChgDTO.setRatioValue(new BigDecimal(pgwCharge.getChargeValuePercentage()));
					} else {
						externalChgDTO.setRatioValueInPercentage(false);
						externalChgDTO.setRatioValue(new BigDecimal(pgwCharge.getValueInDefaultCurrency()));
					}
				}

				if (externalChgDTO.isRatioValueInPercentage()) {
					externalChgDTO.calculateAmount(totalTicketPriceExcludingCC);
				} else {
					externalChgDTO.calculateAmount(payablePaxCount, segmentCount);
				}
			} else { // if Credit card charge will be define in payment gateway end
				externalChgDTO.setRatioValueInPercentage(false);
				externalChgDTO.setRatioValue(AccelAeroCalculator.getDefaultBigDecimalZero());
				externalChgDTO.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}
		}

		return externalChgDTO;
	}

	/**
	 * @return the defaultPayCurCode
	 */
	public String getDefaultPayCurCode() {
		return defaultPayCurCode;
	}

	/**
	 * @param defaultPayCurCode
	 *            the defaultPayCurCode to set
	 */
	private void setDefaultPayCurCode(String defaultPayCurCode) {
		this.defaultPayCurCode = defaultPayCurCode;
	}

	// getters
	public PaxDetailDTO getPaxDetail() {
		return paxDetail;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public Collection<SegInfoDTO> getFlightSegments() {
		return flightSegments;
	}

	public boolean isBlnReturn() {
		return blnReturn;
	}

	public Collection<PaymentGateWayInfoDTO> getPaymentGateways() {
		return paymentGateways;
	}

	public Collection<SegInfoDTO> getFlightDepDetails() {
		return flightDepDetails;
	}

	public Collection<SegInfoDTO> getFlightRetDetails() {
		return flightRetDetails;
	}

	public String getPgOptions() {
		return pgOptions;
	}

	public String getSecurePath() {
		return securePath;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<PaymentGateWayInfoDTO> getAllPaymentGateways() {
		return allPaymentGateways;
	}

	@SuppressWarnings("unchecked")
	private Object[] getPaymentgateWay(ExchangeRateProxy exchangeRateProxy, boolean isOfflinePaymentsEnabled,
			IBEReservationInfoDTO resInfo, String firstOndSegment) throws ModuleException {
		// TODO pass the amount and selected currency
		Integer operationalType = ((modifySegment || modifyAncillary || addGroundSegment) ? ChargeRateOperationType.MODIFY_ONLY
				: ChargeRateOperationType.MAKE_ONLY);
		ExternalChgDTO ccFeeTax = ReservationUtil.getApplicableServiceTax(resInfo, EXTERNAL_CHARGES.JN_OTHER, operationalType);

		// TODO: we need to add buffer time for create onhold reservations.
		// Because if onholdTimeSecond is near zero such as 10 minutes, it is useless to put onhold for offline
		// payments.

		Object[] pgArray = new Object[2];
		Collection<PaymentGateWayInfoDTO> colPgs = new ArrayList<PaymentGateWayInfoDTO>();
		Collection<PaymentGateWayInfoDTO> colAllPgs = new ArrayList<PaymentGateWayInfoDTO>();
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		ipgPaymentOptionDTO.setFirstSegmentONDCode(firstOndSegment);
		if (getSearchParams() != null) {
			ipgPaymentOptionDTO.setSelCurrency(getSearchParams().getSelectedCurrency());
		}
		ipgPaymentOptionDTO.setModuleCode("IBE");
		StringBuilder sb = new StringBuilder();
		String brokerType = null;

		Map<String, Collection<String[]>> cardTypes = SelectListGenerator.createCreditCardTypesForAllPGW();

		ipgPaymentOptionDTO.setOfflinePayment(isOfflinePaymentsEnabled);

		Map paymentMap = ModuleServiceLocator.getPaymentBrokerBD().getPaymentOptions(ipgPaymentOptionDTO);
		List baseCurrencyList = (List) paymentMap.get("BASE_CURRENCY");
		List paymentGatewayList = (List) paymentMap.get("PAYMENT_GATEWAY");
	

		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
			// load payFort pay@store payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					"PAYFORT_PAY_AT_STORE");
			if (pgwList.size() > 0) {
				payFortOfflinePayment = true;
				IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);

				baseCurrencyList.add(payFortPgwConfigs);
				if (ipgPaymentOptionDTO.getSelCurrency() != null && !ipgPaymentOptionDTO.getSelCurrency().equals("")) {
					payFortPgwConfigs.setSelCurrency(ipgPaymentOptionDTO.getSelCurrency());
				} else {
					payFortPgwConfigs.setSelCurrency(payFortPgwConfigs.getBaseCurrency());
				}
				paymentGatewayList.add(payFortPgwConfigs);
			}
		}
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
			// load payFort pay@home payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					"PAYFORT_PAY_AT_HOME");
			if (pgwList.size() > 0) {
				payFortPayATHomeOfflinePayment = true;
				IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);

				baseCurrencyList.add(payFortPgwConfigs);
				if (ipgPaymentOptionDTO.getSelCurrency() != null && !ipgPaymentOptionDTO.getSelCurrency().equals("")) {
					payFortPgwConfigs.setSelCurrency(ipgPaymentOptionDTO.getSelCurrency());
				} else {
					payFortPgwConfigs.setSelCurrency(payFortPgwConfigs.getBaseCurrency());
				}
				paymentGatewayList.add(payFortPgwConfigs);
			}
		}
		
		if (AppSysParamsUtil.isSelectedCurrencyIPGs()) {

			List<IPGPaymentOptionDTO> selCurrencyPGList = new ArrayList<IPGPaymentOptionDTO>();

			for (IPGPaymentOptionDTO ipgPGOptDTO : (Collection<IPGPaymentOptionDTO>) paymentGatewayList) {
				if (ipgPGOptDTO.getBaseCurrency().equals(getSearchParams().getSelectedCurrency())) {
					selCurrencyPGList.add(ipgPGOptDTO);
				}
			}

			if (!selCurrencyPGList.isEmpty()) {
				paymentGatewayList.clear();
				paymentGatewayList.addAll(selCurrencyPGList);
			}
		}
		
		// Get Country Code
		String countryCode = ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(getClientInfoDTO().getIpAddress());

		if (AppSysParamsUtil.isTestStstem()) {
			String testSysOriginCountry = (String) request.getSession().getAttribute("originCountry");
			if (testSysOriginCountry == null || testSysOriginCountry.trim().length() == 0) {
				testSysOriginCountry = "OT"; // Other country
				// testSysOriginCountry = "AE"; // Emirates country
			}
			countryCode = testSysOriginCountry;
		}

		boolean isOndWisePayment = AppSysParamsUtil.getIsONDWisePayment();
    	PaymentBrokerBD brokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		Collection<Integer> paymentGatewayIds =  ReservationUtil.getPaymentGatewayIDs(paymentGatewayList);

		// Get Country payment gateway card type behavior
		cardsInfo = isOndWisePayment ? brokerBD.getONDWiseCountryCardBehavior(firstOndSegment,paymentGatewayIds)
				: brokerBD.getContryPaymentCardBehavior(countryCode,paymentGatewayIds);

		Map<Integer, String> PGsModificationAllowMap = ReservationUtil.getPaymentModificationAllowMap(paymentGatewayList);
		Iterator<CountryPaymentCardBehaviourDTO> itCardsInfo = cardsInfo.iterator();
		while (itCardsInfo.hasNext()) {

			CountryPaymentCardBehaviourDTO cardInfo = itCardsInfo.next();
			cardInfo.setModicifationAllowed(isModifiAllow(PGsModificationAllowMap.get(cardInfo.getPaymentGateWayID())));
			cardInfo.setWithinOnholdPeriod(isOfflinePaymentsEnabled);

		}
		
		

		Object[] listArr = baseCurrencyList.toArray();

		for (int i = 0; i < listArr.length; i++) {
				ipgPaymentOptionDTO = (IPGPaymentOptionDTO) listArr[i];
			if ((AppSysParamsUtil.isSelectedCurrencyIPGs() && getSearchParams().getSelectedCurrency() != null && getSearchParams()
					.getSelectedCurrency().equals(ipgPaymentOptionDTO.getBaseCurrency()))
					|| !AppSysParamsUtil.isSelectedCurrencyIPGs()) {
				PaymentGateWayInfoDTO pgw = new PaymentGateWayInfoDTO();
				pgw.setDescription(ipgPaymentOptionDTO.getDescription());
				pgw.setPayCurrency(ipgPaymentOptionDTO.getBaseCurrency());
				pgw.setPaymentGateway(ipgPaymentOptionDTO.getPaymentGateway());
				pgw.setProviderCode(ipgPaymentOptionDTO.getProviderCode());
				pgw.setProviderName(ipgPaymentOptionDTO.getProviderName());
				brokerType = getBrokerType(ipgPaymentOptionDTO.getPaymentGateway(), ipgPaymentOptionDTO.getBaseCurrency());
				pgw.setBrokerType(brokerType);
				pgw.setModifyAllow(isModifiAllow(ipgPaymentOptionDTO.getIsModificationAllow()));
				pgw.setSwitchToExternalURL(isSwitchToExternalURL(ipgPaymentOptionDTO.getPaymentGateway(),
						ipgPaymentOptionDTO.getBaseCurrency()));
				pgw.setOnholdReleaseTime(ipgPaymentOptionDTO.getOnholdReleaseTime());

				if (cardTypes != null
						&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)) {
					pgw.setCardTypes(cardTypes.get(Integer.toString(ipgPaymentOptionDTO.getPaymentGateway())));
				}
				try {
					CurrencyExchangeRate currencyExrate = exchangeRateProxy.getCurrencyExchangeRate(
							ipgPaymentOptionDTO.getBaseCurrency(), ApplicationEngine.IBE);
					Currency pgCurrency = currencyExrate.getCurrency();

					// if paymentGatewayWiseCharges Enabled
					PaymentGatewayWiseCharges pgwCharge = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge(
							pgw.getPaymentGateway());
					ExternalChgDTO ccChgDTO = (ExternalChgDTO) SessionUtil.getIBEreservationInfo(request)
							.getSelectedExternalCharges().get(EXTERNAL_CHARGES.CREDIT_CARD);

					if (pgwCharge != null && pgwCharge.getChargeId() > 0 && ccChgDTO != null) {
						BigDecimal ccFee = new BigDecimal("0");
						ccChgDTO = (ExternalChgDTO) ccChgDTO.clone();
						// valid charge is defined for the selected pgw
						if (pgwCharge.getValuePercentageFlag() != PaymentGatewayWiseChargeAction.CHARGE_BY_VALUE) {
							ccChgDTO.setRatioValueInPercentage(true);
							ccChgDTO.setRatioValue(new BigDecimal(pgwCharge.getChargeValuePercentage()));
							ccChgDTO.calculateAmount(AccelAeroCalculator.subtract(new BigDecimal(fareQuote.getInBaseCurr()
									.getTotalPayable()), new BigDecimal(fareQuote.getInBaseCurr().getTotalFee())));
						} else {
							ccChgDTO.setRatioValueInPercentage(false);
							ccChgDTO.setRatioValue(new BigDecimal(pgwCharge.getValueInDefaultCurrency()));
							ccChgDTO.calculateAmount(AccelAeroCalculator.subtract(new BigDecimal(fareQuote.getInBaseCurr()
									.getTotalPayable()), new BigDecimal(fareQuote.getInBaseCurr().getTotalFee())));
						}

						ccFee = ccChgDTO.getAmount();
						if (ccFeeTax != null) {
							ccFee = AccelAeroCalculator.add(ccFee, ccFeeTax.getTotalAmount());
						}
						pgw.setPayCurrencyAmount(AccelAeroRounderPolicy
								.convertAndRound(
										AccelAeroCalculator.add(AccelAeroCalculator.subtract(new BigDecimal(fareQuote.getInBaseCurr()
												.getTotalPayable()), new BigDecimal(fareQuote.getInBaseCurr().getTotalFee())), ccFee),
										currencyExrate.getMultiplyingExchangeRate(), pgCurrency.getBoundryValue(),
										pgCurrency.getBreakPoint()).toString());
					} else {
						pgw.setPayCurrencyAmount(AccelAeroRounderPolicy.convertAndRound(
								new BigDecimal(this.fareQuote.getTotalPayable()), currencyExrate.getMultiplyingExchangeRate(),
								pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());
					}

					sb.append("<option value='" + pgw.getPaymentGateway() + "'>" + pgw.getDescription() + "</option>");
					colPgs.add(pgw);
				} catch (ModuleException me) {
					log.error("Exchange rate is not defined for " + ipgPaymentOptionDTO.getBaseCurrency(), me);
				}
			}
		}
		this.pgOptions = sb.toString();

		Object[] listAllArr = paymentGatewayList.toArray();

		for (int i = 0; i < listAllArr.length; i++) {
			ipgPaymentOptionDTO = (IPGPaymentOptionDTO) listAllArr[i];
			PaymentGateWayInfoDTO pgw = new PaymentGateWayInfoDTO();
			pgw.setDescription(ipgPaymentOptionDTO.getDescription());
			pgw.setPayCurrency(ipgPaymentOptionDTO.getSelCurrency());
			pgw.setPaymentGateway(ipgPaymentOptionDTO.getPaymentGateway());
			pgw.setProviderCode(ipgPaymentOptionDTO.getProviderCode());
			pgw.setProviderName(ipgPaymentOptionDTO.getProviderName());
			brokerType = getBrokerType(ipgPaymentOptionDTO.getPaymentGateway(), ipgPaymentOptionDTO.getBaseCurrency());
			pgw.setBrokerType(brokerType);
			pgw.setModifyAllow(isModifiAllow(ipgPaymentOptionDTO.getIsModificationAllow()));
			pgw.setSwitchToExternalURL(isSwitchToExternalURL(ipgPaymentOptionDTO.getPaymentGateway(),
					ipgPaymentOptionDTO.getBaseCurrency()));
			pgw.setOnholdReleaseTime(ipgPaymentOptionDTO.getOnholdReleaseTime());

			if (cardTypes != null
					&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)) {
				pgw.setCardTypes(cardTypes.get(Integer.toString(ipgPaymentOptionDTO.getPaymentGateway())));
			}
			try {
				CurrencyExchangeRate currencyExrate = exchangeRateProxy.getCurrencyExchangeRate(
						ipgPaymentOptionDTO.getSelCurrency(), ApplicationEngine.IBE);
				Currency pgCurrency = currencyExrate.getCurrency();

				// if paymentGatewayWiseCharges Enabled
				PaymentGatewayWiseCharges pgwCharge = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge(
						pgw.getPaymentGateway());
				ExternalChgDTO ccChgDTO = (ExternalChgDTO) SessionUtil.getIBEreservationInfo(request)
						.getSelectedExternalCharges().get(EXTERNAL_CHARGES.CREDIT_CARD);

				if (pgwCharge != null && pgwCharge.getChargeId() > 0 && ccChgDTO != null) {
					BigDecimal ccFee = new BigDecimal("0");
					ccChgDTO = (ExternalChgDTO) ccChgDTO.clone();
					// valid charge is defined for the selected pgw
					if (pgwCharge.getValuePercentageFlag() != PaymentGatewayWiseChargeAction.CHARGE_BY_VALUE) {
						ccChgDTO.setRatioValueInPercentage(true);
						ccChgDTO.setRatioValue(new BigDecimal(pgwCharge.getChargeValuePercentage()));
						ccChgDTO.calculateAmount(AccelAeroCalculator.subtract(new BigDecimal(fareQuote.getInBaseCurr()
								.getTotalPayable()), new BigDecimal(fareQuote.getInBaseCurr().getTotalFee())));
					} else {
						ccChgDTO.setRatioValueInPercentage(false);
						ccChgDTO.setRatioValue(new BigDecimal(pgwCharge.getValueInDefaultCurrency()));
						ccChgDTO.calculateAmount(AccelAeroCalculator.subtract(new BigDecimal(fareQuote.getInBaseCurr()
								.getTotalPayable()), new BigDecimal(fareQuote.getInBaseCurr().getTotalFee())));
					}

					ccFee = ccChgDTO.getAmount();
					if (ccFeeTax != null) {
						ccFee = AccelAeroCalculator.add(ccFee, ccFeeTax.getTotalAmount());
					}
					pgw.setPayCurrencyAmount(AccelAeroRounderPolicy
							.convertAndRound(
									AccelAeroCalculator.add(AccelAeroCalculator.subtract(new BigDecimal(fareQuote.getInBaseCurr()
											.getTotalPayable()), new BigDecimal(fareQuote.getInBaseCurr().getTotalFee())), ccFee),
									currencyExrate.getMultiplyingExchangeRate(), pgCurrency.getBoundryValue(),
									pgCurrency.getBreakPoint()).toString());
				} else {
					pgw.setPayCurrencyAmount(AccelAeroRounderPolicy.convertAndRound(
							new BigDecimal(this.fareQuote.getTotalPayable()), currencyExrate.getMultiplyingExchangeRate(),
							pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint()).toString());
				}

				colAllPgs.add(pgw);
			} catch (ModuleException me) {
				log.error("Exchange rate is not defined for " + ipgPaymentOptionDTO.getBaseCurrency(), me);
			}
		}
		updatePaymentBrokerforPaymentCards(colAllPgs);
		pgArray[0] = colPgs;
		pgArray[1] = colAllPgs;
		return pgArray;
	}

	/**
	 * Update cardsInfo with configuration broker type
	 * 
	 * @param colAllPgs
	 */
	private void updatePaymentBrokerforPaymentCards(Collection<PaymentGateWayInfoDTO> colAllPgs) {
		if (colAllPgs != null && cardsInfo != null) {
			Map<Integer, String> paymentbrokerMap = createPaymentGatewayBrokerMap(colAllPgs);
			String brokerType = null;
			String messageKey = null;
			for (CountryPaymentCardBehaviourDTO cardBehaviourDTO : cardsInfo) {
				brokerType = paymentbrokerMap.get(cardBehaviourDTO.getPaymentGateWayID());
				if (brokerType != null) {
					cardBehaviourDTO.setBehaviour(brokerType);
				}
				messageKey = cardBehaviourDTO.getI18nMsgKey();
				if (messageKey != null && messageKey.trim().length() != 0) {
					cardBehaviourDTO.setCardDisplayName(I18NUtil.getMessage(messageKey, SessionUtil.getLanguage(request)));
				}
			}
		}
	}

	private Map createPaymentGatewayBrokerMap(Collection<PaymentGateWayInfoDTO> colAllPgs) {
		Map<Integer, String> paymentbrokerMap = new HashMap<Integer, String>();
		for (PaymentGateWayInfoDTO gateWayInfoDTO : colAllPgs) {
			paymentbrokerMap.put(gateWayInfoDTO.getPaymentGateway(), gateWayInfoDTO.getBrokerType());
		}
		return paymentbrokerMap;
	}

	private String getBrokerType(int paymentGatewayID, String baseCurrency) throws ModuleException {
		Properties ipgProps = getIPGProperites(paymentGatewayID, baseCurrency);
		return ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
	}

	private boolean isSwitchToExternalURL(int paymentGatewayID, String baseCurrency) throws ModuleException {
		Properties ipgProps = getIPGProperites(paymentGatewayID, baseCurrency);
		return Boolean.parseBoolean(ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SWITCH_TO_EXTERNAL_URL));
	}

	private Properties getIPGProperites(int paymentGatewayID, String baseCurrency) throws ModuleException {
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
				paymentGatewayID, baseCurrency);
		return ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
	}

	private boolean isModifiAllow(String isModifiAllow) {
		if ("Y".equalsIgnoreCase(isModifiAllow)) {
			return true;
		}
		return false;
	}

	private long calculateOnholdExpirePeriod(List<FlightSegmentTO> flightSegments, FareTypeTO fareTypeTo) throws Exception {

		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(flightSegments, fareTypeTo);
		Date depDateTimeZulu = ReservationUtil.getDepartureTimeZulu(flightSegments);
		Date pnrReleaseTimeZulu = ReservationUtil.calculateOnHoldReleaseTime(OnHold.IBEOFFLINEPAYMENT, depDateTimeZulu,
				onHoldReleaseTimeDTO);

		log.info("*************** calculateOnholdExpirePeriod pnrReleaseTimeZulu : " + pnrReleaseTimeZulu);
		Date currentDateTime = Calendar.getInstance().getTime();
		long OHDIntervalinSeconds = 0;
		if (pnrReleaseTimeZulu != null) {
			OHDIntervalinSeconds = (pnrReleaseTimeZulu.getTime() - currentDateTime.getTime()) / 1000;
		}
		log.info("*************** calculateOnholdExpirePeriod OHDIntervalinSeconds : " + OHDIntervalinSeconds);
		return OHDIntervalinSeconds;

	}

	private static String getServerErrorMessage(HttpServletRequest request, String key) {
		return I18NUtil.getMessage(key, SessionUtil.getLanguage(request));
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public CustomerLoyalty getCustomerLoyalty() {
		return customerLoyalty;
	}

	public void setCustomerLoyalty(CustomerLoyalty customerLoyalty) {
		this.customerLoyalty = customerLoyalty;
	}

	public String getLoyaltyPayOption() {
		return loyaltyPayOption;
	}

	public void setLoyaltyPayOption(String loyaltyPayOption) {
		this.loyaltyPayOption = loyaltyPayOption;
	}

	public String getLoyaltyCreditAmount() {
		return loyaltyCreditAmount;
	}

	public void setLoyaltyCreditAmount(String loyaltyCreditAmount) {
		this.loyaltyCreditAmount = loyaltyCreditAmount;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public String getAccessPoint() {
		return accessPoint;
	}

	public boolean isOnHoldEnable() {
		return onHoldEnable;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getOnHoldDisplayTime() {
		return onHoldDisplayTime;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the airportMessage
	 */
	public String getAirportMessage() {
		return airportMessage;
	}

	/**
	 * @param airportMessage
	 *            the airportMessage to set
	 */
	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	/**
	 * @return the onHoldRestrictedBC
	 */
	public boolean isOnHoldRestrictedBC() {
		return onHoldRestrictedBC;
	}

	/**
	 * @param onHoldRestrictedBC
	 *            the onHoldRestrictedBC to set
	 */
	public void setOnHoldRestrictedBC(boolean onHoldRestrictedBC) {
		this.onHoldRestrictedBC = onHoldRestrictedBC;
	}

	public ConfirmUpdateChargesTO getUpdateCharge() {
		return updateCharge;
	}

	public boolean isModifyBalanceDisplayPayment() {
		return modifyBalanceDisplayPayment;
	}

	public void setHasInsurance(boolean hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	public String getUserIp() {
		return userIp;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public void setRequestSessionIdentifier(String requestSessionIdentifier) {
		this.requestSessionIdentifier = requestSessionIdentifier;
	}

	public boolean isHasNopay() {
		return hasNopay;
	}

	public Map<String, String> getCardErrorInfo() {
		return cardErrorInfo;
	}

	public Collection<CountryPaymentCardBehaviourDTO> getCardsInfo() {
		return cardsInfo;
	}

	public void setCardsInfo(Collection<CountryPaymentCardBehaviourDTO> cardsInfo) {
		this.cardsInfo = cardsInfo;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setoNDImageCapcha(boolean oNDImageCapcha) {
		this.oNDImageCapcha = oNDImageCapcha;
	}

	public boolean isoNDImageCapcha() {
		return oNDImageCapcha;
	}

	public void setTotalSegmentCount(int totalSegmentCount) {
		this.totalSegmentCount = totalSegmentCount;
	}

	public boolean isMakePayment() {
		return makePayment;
	}

	public void setMakePayment(boolean makePayment) {
		this.makePayment = makePayment;
	}

	public boolean isPromotionPayment() {
		return promotionPayment;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public SelectedPaymentMethodDTO getSelectedPaymentMethodDTO() {
		return selectedPaymentMethodDTO;
	}

	public void setFreeServiceDiscount(String freeServiceDiscount) {
		this.freeServiceDiscount = freeServiceDiscount;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public void setModifingFlightInfo(String modifingFlightInfo) {
		this.modifingFlightInfo = modifingFlightInfo;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	public void setResPnr(String resPnr) {
		this.resPnr = resPnr;
	}

	private void performRequoteBalanceCalculation(IBEReservationInfoDTO resInfo, Collection<ReservationPaxTO> resPaxTo)
			throws ModuleException, ParseException {
		RequoteModifyRQ requoteModifyRQ = JSONFETOParser.parse(RequoteModifyRQ.class, balanceQueryData);
		requoteModifyRQ.setContactInfo(ReservationUtil.retrieveContactInfo(SessionUtil.getReservationContactInfo(request)));
		requoteModifyRQ.setSystem(resInfo.getSelectedSystem());
		List<ONDSearchDTO> ondSearchDTOs = getSearchParams().getOndList();
		if (resInfo.getSelectedSystem() == SYSTEM.INT) {
			requoteModifyRQ.setSystem(SYSTEM.INT);
			requoteModifyRQ.setTransactionIdentifier(resInfo.getTransactionId());
			requoteModifyRQ.setCarrierWiseFlightRPHMap(ReservationCommonUtil.getCarrierWiseRPHs(ondSearchDTOs));
			requoteModifyRQ.setGroupPnr(resPnr);
			requoteModifyRQ.setVersion(ReservationCommonUtil.getCorrectInterlineVersion(requoteModifyRQ.getVersion()));
		}

		if (resInfo.getPriceInfoTO() != null) {
			FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
			constructPaxQtyList(flightPriceRQ);
			flightPriceRQ.getTravelPreferences().setBookingType(getSearchParams().getBookingType());
			QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(resInfo.getPriceInfoTO().getFareSegChargeTO(),
					flightPriceRQ, null);
			requoteModifyRQ.setFareInfo(fareInfo);
			requoteModifyRQ.setLastFareQuoteDate(resInfo.getPriceInfoTO().getLastFareQuotedDate());
			requoteModifyRQ.setFQWithinValidity(resInfo.getPriceInfoTO().isFQWithinValidity());
		}
		requoteModifyRQ.setRequoteSegmentMap(ReservationBeanUtil.getReQuoteResSegmentMap(ondSearchDTOs));
		if (nameChangeRequote) {
			requoteModifyRQ.setNameChangedPaxMap(ReservationUtil.transformToPaxMap(ReservationUtil
					.transformNameChangePax(nameChangePaxData)));
		}
		remainingLoyaltyPoints = CustomerUtil.getMemberAvailablePoints(request);
		requoteModifyRQ.setRemainingLoyaltyPoints(remainingLoyaltyPoints);

		Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();
		requoteModifyRQ.setOndFareTypeByFareIdMap(ReservationBeanUtil.getONDFareTypeByFareIdMap(ondSearchDTOs,
				oldFareIdByFltSegIdMap));
		requoteModifyRQ.setOldFareIdByFltSegIdMap(oldFareIdByFltSegIdMap);

		Map<Integer, List<LCCClientExternalChgDTO>> paxMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		if (resPaxTo != null && !resPaxTo.isEmpty()) {
			for (ReservationPaxTO resPax : resPaxTo) {
				List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
				extChgList.addAll(resPax.getExternalCharges());
				paxMap.put(resPax.getSeqNumber(), extChgList);
			}
		}
		requoteModifyRQ.setPaxExtChgMap(paxMap);

		ReservationBalanceTO reservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD().getRequoteBalanceSummary(
				requoteModifyRQ, getTrackInfo());

		resInfo.setlCCClientReservationBalance(reservationBalanceTO);
		resInfo.setCreditableAmount(AccelAeroCalculator.formatAsDecimal(reservationBalanceTO.getSegmentSummary()
				.getCurrentRefunds()));
		resInfo.setModifyCharge(AccelAeroCalculator.formatAsDecimal(reservationBalanceTO.getSegmentSummary().getNewModAmount()));
	}

	private List<PassengerTypeQuantityTO> constructPaxQtyList(FlightPriceRQ flightPriceRQ) {

		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);

		if (searchParams.getAdultCount() == null) {
			adultsQuantity.setQuantity(0);
		} else {
			adultsQuantity.setQuantity(searchParams.getAdultCount());
		}

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);

		if (searchParams.getChildCount() == null) {
			childQuantity.setQuantity(0);
		} else {
			childQuantity.setQuantity(searchParams.getChildCount());
		}

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);

		if (searchParams.getInfantCount() == null) {
			infantQuantity.setQuantity(0);
		} else {
			infantQuantity.setQuantity(searchParams.getInfantCount());
		}
		return traverlerInfo.getPassengerTypeQuantityList();
	}

	@SuppressWarnings("unchecked")
	private boolean redeemLoyaltyPoints(List<FlightSegmentTO> flightSegmentTOs, Collection<ReservationPaxTO> paxList) {

		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		RedeemCalculateRQ redeemCalculateRQ = new RedeemCalculateRQ();
		boolean response = false;
		String memberAccountId = SessionUtil.getLoyaltyFFID(request);
		try {

			// revert already requested loyalty rewards if any
			CustomerUtil.revertLoyaltyRedemption(request);
			remainingLoyaltyPoints = CustomerUtil.getMemberAvailablePoints(request);
			if (AccelAeroCalculator.isEqual(redeemRequestAmount, BigDecimal.ZERO)) {
				resInfo.setCarrierWiseLoyaltyPaymentInfo(null);
				totalRedeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				response = true;
			} else {
				redeemCalculateRQ.setMemberAccountID(memberAccountId);
				redeemCalculateRQ.setRedeemRequestAmount(redeemRequestAmount);
				redeemCalculateRQ.setSystem(resInfo.getSelectedSystem());

				FlightPriceRQ flightPriceRQ = getFlightPriceRQ(flightSegmentTOs);

				redeemCalculateRQ.setFlightPriceRQ(flightPriceRQ);

				Map<String, List<String>> carrierWiseFlightRPH = new HashMap<String, List<String>>();
				Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightBD().getBusAirCarrierCodes();
				if (flightSegmentTOs != null) {
					for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
						String operatingCarrier = FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber());
						if (busAirCarrierCodes.containsKey(operatingCarrier)) {
							operatingCarrier = busAirCarrierCodes.get(operatingCarrier);
						}

						if (operatingCarrier != null && carrierWiseFlightRPH.containsKey(operatingCarrier)) {
							carrierWiseFlightRPH.get(operatingCarrier).add(flightSegmentTO.getFlightRefNumber());
						} else {
							List<String> flightRefNumList = new ArrayList<String>();
							flightRefNumList.add(flightSegmentTO.getFlightRefNumber());
							carrierWiseFlightRPH.put(operatingCarrier, flightRefNumList);
						}
					}

				}
				redeemCalculateRQ.setCarrierWiseFlightRPH(carrierWiseFlightRPH);
				redeemCalculateRQ.setIssueRewardIds(true);
				redeemCalculateRQ.setPnr(pnr);
				redeemCalculateRQ.setRemainingPoint(remainingLoyaltyPoints);

				if (requoteFlightSearch) {
					redeemCalculateRQ.setPaxCarrierProductDueAmount(WebplatformUtil.populatePaxProductDueAmounts(resInfo
							.getlCCClientReservationBalance()));
				} else if (makePayment) {
					redeemCalculateRQ.setPaxCarrierProductDueAmount(ModuleServiceLocator.getAirproxyReservationBD()
							.getPaxProduDueAmount(pnr, groupPNR, false, remainingLoyaltyPoints));
					redeemCalculateRQ.setPayForOHD(true);
				} else {
					FareSegChargeTO fareSegChargeTO = null;
					if (resInfo.getPriceInfoTO() != null) {
						fareSegChargeTO = resInfo.getPriceInfoTO().getFareSegChargeTO();
					}

					boolean isFlexiQuote = getSearchParams().isFlexiSelected();

					Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges = WebplatformUtil
							.populatePaxExternalCharges(paxList);
					redeemCalculateRQ.setPaxCarrierExternalCharges(paxCarrierExternalCharges);
					redeemCalculateRQ.setFareSegChargeTo(fareSegChargeTO);
					redeemCalculateRQ.setFlexiQuote(isFlexiQuote);
				}

				String strTxnIdntifier = resInfo.getTransactionId();
				redeemCalculateRQ.setTxnIdntifier(strTxnIdntifier);

				TrackInfoDTO trackInfoDTO = this.getTrackInfo();
				trackInfoDTO.setAppIndicator(getTrackInfo().getAppIndicator());

				ReservationUtil.setDiscountCalculatorRQ(paxList, resInfo, redeemCalculateRQ);

				ServiceResponce serviceResponce = ModuleServiceLocator.getAirproxyReservationBD()
						.calculateLoyaltyPointRedeemables(redeemCalculateRQ, trackInfoDTO);

				resInfo.setCarrierWiseLoyaltyPaymentInfo(null);

				if (serviceResponce.isSuccess()) {
					Map<Integer, Map<String, Double>> paxWiseProductPoints = (Map<Integer, Map<String, Double>>) serviceResponce
							.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_POINTS);
					Map<Integer, Map<String, BigDecimal>> paxWiseProductAmount = (Map<Integer, Map<String, BigDecimal>>) serviceResponce
							.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_AMOUNT);

					Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo = new HashMap<String, LoyaltyPaymentInfo>();

					if (paxWiseProductPoints != null && !paxWiseProductPoints.isEmpty()) {
						if (resInfo.getSelectedSystem() == SYSTEM.AA) {
							Map<String, Double> productTotalRedeemPoints = WebplatformUtil
									.getProductTotalRedeemPoints(paxWiseProductPoints);
							serviceResponce = ModuleServiceLocator.getAirproxyReservationBD().issueLoyaltyRewards(pnr,
									memberAccountId, productTotalRedeemPoints, resInfo.getSelectedSystem(),
									getTrackInfo().getAppIndicator(), null, null);
						}

						if (serviceResponce.isSuccess()) {
							totalRedeemedAmount = AirProxyReservationUtil.getTotalRedeemedPoints(paxWiseProductAmount);
							remainingLoyaltyPoints = CustomerUtil.getMemberAvailablePoints(request);

							if (resInfo.getSelectedSystem() == SYSTEM.AA) {
								LoyaltyPaymentInfo loyaltyPaymentInfo = new LoyaltyPaymentInfo();
								loyaltyPaymentInfo.setTotalPayment(AirProxyReservationUtil
										.getTotalRedeemedPoints(paxWiseProductAmount));
								loyaltyPaymentInfo.setPaxProductPaymentBreakdown(paxWiseProductAmount);
								String[] loyaltyRewardIds = (String[]) serviceResponce
										.getResponseParam(ResponceCodes.ResponseParams.LOYALTY_REWARD_IDS);
								loyaltyPaymentInfo.setLoyaltyRewardIds(loyaltyRewardIds);
								loyaltyPaymentInfo.setMemberAccountId(memberAccountId);
								carrierWiseLoyaltyPaymentInfo.put(AppSysParamsUtil.getDefaultCarrierCode(), loyaltyPaymentInfo);
							} else {
								carrierWiseLoyaltyPaymentInfo = (Map<String, LoyaltyPaymentInfo>) serviceResponce
										.getResponseParam(ResponceCodes.ResponseParams.CARRIER_LOYALTY_PAYMENTS);
							}

							resInfo.setCarrierWiseLoyaltyPaymentInfo(carrierWiseLoyaltyPaymentInfo);
							response = true;
						} else {
							log.error("Error in redeem loyalty points for member: " + memberAccountId);
						}
					}

				} else {
					log.error("Error in calculating redeemable amount for member: " + memberAccountId);
				}
			}

		} catch (Exception e) {
			log.error("Error in redeeeming loyalty points for member " + memberAccountId, e);
			ModuleException me = null;
			if (e instanceof ModuleException) {
				me = (ModuleException) e;
				messageTxt = I18NUtil.getMessage(me.getExceptionCode(), SessionUtil.getLanguage(request));
			} else {
				messageTxt = e.getMessage();
			}
		}

		return response;
	}

	public void redeemVouchers(BigDecimal totalPayable, List<ReservationPaxTO> paxList) throws Exception {
		
		if (AppSysParamsUtil.isVoucherEnabled()) {
			AirproxyVoucherBD airproxyVoucherBD = ModuleServiceLocator.getAirproxyVoucherBD();
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);

			if (resInfo.getPayByVoucherInfo() == null
					|| (resInfo.getPayByVoucherInfo() != null && resInfo.getPayByVoucherInfo().getUnsuccessfulAttempts() < 2)) {
				if (resInfo.getPayByVoucherInfo() != null && resInfo.getPayByVoucherInfo().getVoucherDTOList() != null) {
					if (onLoad) {
						voucherRedeemRequest = new VoucherRedeemRequest();
						voucherRedeemRequest.setBlockedVoucherIDList(resInfo.getPayByVoucherInfo().getVoucherIDList());
						voucherRedeemRequest.setVoucherIDList(resInfo.getPayByVoucherInfo().getVoucherIDList());
					} else {
						voucherRedeemRequest.setBlockedVoucherIDList(resInfo.getPayByVoucherInfo().getVoucherIDList());
					}
				}
				try {
					voucherRedeemRequest.setBalanceToPay(totalPayable.toString());
	
					if (AppSysParamsUtil.isVoucherNameValidationEnabled()) {
						ArrayList<String> paxNameList = new ArrayList<String>();
						for (ReservationPaxTO reservationPax : paxList) {
							paxNameList.add(reservationPax.getFirstName() + reservationPax.getLastName());
						}
						voucherRedeemRequest.setPaxNameList(paxNameList);
					}
	
					setVoucherRedeemResponse(airproxyVoucherBD.getVoucherRedeemResponse(voucherRedeemRequest));
					if (resInfo.getPayByVoucherInfo() == null) {
						resInfo.setPayByVoucherInfo(new PayByVoucherInfo());
					}
					resInfo.getPayByVoucherInfo().setRedeemedTotal(new BigDecimal(voucherRedeemResponse.getRedeemedTotal()));
					resInfo.getPayByVoucherInfo().setVoucherDTOList(voucherRedeemResponse.getRedeemVoucherList());
					resInfo.getPayByVoucherInfo().setVouchersTotal(voucherRedeemResponse.getVouchersTotal());
					resInfo.getPayByVoucherInfo().setVoucherPaymentOption(voucherRedeemResponse.getVoucherPaymentOption());
					messageTxt = I18NUtil.getMessage("success.voucher.redeem", SessionUtil.getLanguage(request));
				} catch (ModuleException me) {
					log.error("InterlinePaymentAction ==> redeemVouchers(BigDecimal, List<ReservationPaxTO>)", me);
					if (resInfo.getPayByVoucherInfo() == null) {
						resInfo.setPayByVoucherInfo(new PayByVoucherInfo());
					}
					resInfo.getPayByVoucherInfo().incrementUnsuccessfullAttempts();
					voucherRedeemSuccess = false;
					if (resInfo.getPayByVoucherInfo().getUnsuccessfulAttempts() >= 2) {
						disableRedeem = true;
					}
	
					messageTxt = I18NUtil.getMessage(me.getExceptionCode(), SessionUtil.getLanguage(request));
					if ("error.voucher.redeem.invalid".equals(me.getExceptionCode())) {
						messageTxt += me.getCustumValidateMessage();
					}
				} catch (Exception e) {
					log.error("InterlinePaymentAction ==> redeemVouchers(BigDecimal, List<ReservationPaxTO>)", e);
					success = false;
				}
			}
		}
	}

	private void revertLoyaltyRedemption(boolean loyaltyRedemptionResponse) {
		if (loyaltyRedemptionResponse) {
			String loyaltyMemberId = null;
			try {
				CustomerUtil.revertLoyaltyRedemption(request);
			} catch (ModuleException e) {
				log.error("Error in reverting redeeemed loyalty points for member " + loyaltyMemberId, e);
			}
		}
	}

	private FlightPriceRQ getFlightPriceRQ(List<FlightSegmentTO> flightSegmentTOs) throws Exception {
		FlightPriceRQ flightPriceRQ = null;
		if (isPaymentRetry()) {
			IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
			flightPriceRQ = postPayDTO != null ? postPayDTO.getFlightPriceRQ() : null;
		} else {
			if (!modifyAncillary && !makePayment) {
				AvailableFlightSearchDTOBuilder avilBuilder = SearchUtil.getSearchBuilder(getSearchParams());
				flightPriceRQ = BeanUtil.createFareQuoteRQ(avilBuilder.getSearchDTO(), flightSegmentTOs, getSearchParams()
						.getClassOfService(), getSearchParams().getLogicalCabinClass(), getSearchParams()
						.getFareQuoteLogicalCCSelection());

			}
		}

		return flightPriceRQ;
	}

	/**
	 * @return the onHoldCreated
	 */
	public boolean isOnHoldCreated() {
		return onHoldCreated;
	}

	/**
	 * @param onHoldCreated
	 *            the onHoldCreated to set
	 */
	public void setOnHoldCreated(boolean onHoldCreated) {
		this.onHoldCreated = onHoldCreated;
	}

	/**
	 * @return the paymentGatewayID
	 */
	public int getPaymentGatewayID() {
		return paymentGatewayID;
	}

	/**
	 * @return the loyaltyBased
	 */
	public boolean isLoyaltyBased() {
		return loyaltyBased;
	}

	/**
	 * @param loyaltyBased
	 *            the loyaltyBased to set
	 */
	public void setLoyaltyBased(boolean loyaltyBased) {
		this.loyaltyBased = loyaltyBased;
	}

	/**
	 * @param paymentGatewayID
	 *            the paymentGatewayID to set
	 */
	public void setPaymentGatewayID(int paymentGatewayID) {
		this.paymentGatewayID = paymentGatewayID;
	}

	public void setRedeemLoyaltyPoints(boolean redeemLoyaltyPoints) {
		this.redeemLoyaltyPoints = redeemLoyaltyPoints;
	}

	public boolean isRedeemLoyaltyPoints() {
		return redeemLoyaltyPoints;
	}

	public boolean isIntegrateMashreqWithLMSEnabled() {
		return integrateMashreqWithLMSEnabled;
	}

	public void setIntegrateMashreqWithLMSEnabled(boolean integrateMashreqWithLMSEnabled) {
		this.integrateMashreqWithLMSEnabled = integrateMashreqWithLMSEnabled;
	}

	public void setRedeemRequestAmount(String redeemRequestAmount) {
		if (redeemRequestAmount != null && !"".equals(redeemRequestAmount)) {
			this.redeemRequestAmount = new BigDecimal(redeemRequestAmount);
		} else {
			this.redeemRequestAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
	}

	public BigDecimal getTotalRedeemedAmount() {
		return totalRedeemedAmount;
	}

	public Double getRemainingLoyaltyPoints() {
		return remainingLoyaltyPoints;
	}

	public String getNameChangePaxData() {
		return nameChangePaxData;
	}

	public void setNameChangePaxData(String nameChangePaxData) {
		this.nameChangePaxData = nameChangePaxData;
	}

	public boolean isNameChangeRequote() {
		return nameChangeRequote;
	}

	public void setNameChangeRequote(boolean nameChangeRequote) {
		this.nameChangeRequote = nameChangeRequote;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isLoggedUser() {
		loggedUser = false;
		int customerID = SessionUtil.getCustomerId(request);

		if (customerID >= 0) {
			loggedUser = true;
		}
		return loggedUser;
	}

	public String getErrorMessageInfo() {
		return errorMessageInfo;
	}

	public boolean isPayFortOfflinePayment() {
		return payFortOfflinePayment;
	}

	public void setPayFortOfflinePayment(boolean payFortOfflinePayment) {
		this.payFortOfflinePayment = payFortOfflinePayment;
	}

	public boolean isPayFortPayATHomeOfflinePayment() {
		return payFortPayATHomeOfflinePayment;
	}

	public void setPayFortPayATHomeOfflinePayment(boolean payFortPayATHomeOfflinePayment) {
		this.payFortPayATHomeOfflinePayment = payFortPayATHomeOfflinePayment;
	}

	public String getPayFortMobileNumber() {
		return payFortMobileNumber;
	}

	public void setPayFortMobileNumber(String payFortMobileNumber) {
		this.payFortMobileNumber = payFortMobileNumber;
	}

	public String getPayFortEmail() {
		return payFortEmail;
	}

	public void setPayFortEmail(String payFortEmail) {
		this.payFortEmail = payFortEmail;
	}

	public boolean isAdminFeeRegulationEnabled() {
		return adminFeeRegulationEnabled;
	}

	public void setAdminFeeRegulationEnabled(boolean adminFeeRegulationEnabled) {
		this.adminFeeRegulationEnabled = adminFeeRegulationEnabled;
	}


	public VoucherRedeemRequest getVoucherRedeemRequest() {
		return voucherRedeemRequest;
	}

	public void setVoucherRedeemRequest(VoucherRedeemRequest voucherRedeemRequest) {
		this.voucherRedeemRequest = voucherRedeemRequest;
	}

	public VoucherRedeemResponse getVoucherRedeemResponse() {
		return voucherRedeemResponse;
	}

	public void setVoucherRedeemResponse(VoucherRedeemResponse voucherRedeemResponse) {
		this.voucherRedeemResponse = voucherRedeemResponse;
	}

	public String getSelCurrencyCode() {
		return selCurrencyCode;
	}

	public void setSelCurrencyCode(String selCurrencyCode) {
		this.selCurrencyCode = selCurrencyCode;
	}

	public boolean isCanceledVouchersAvailable() {
		return canceledVouchersAvailable;
	}

	public void setCanceledVouchersAvailable(boolean canceledVouchersAvailable) {
		this.canceledVouchersAvailable = canceledVouchersAvailable;
	}

	public boolean isDisableRedeem() {
		return disableRedeem;
	}

	public void setDisableRedeem(boolean disableRedeem) {
		this.disableRedeem = disableRedeem;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isOnLoad() {
		return onLoad;
	}

	public void setOnLoad(boolean onLoad) {
		this.onLoad = onLoad;
	}

	public boolean isVoucherRedemption() {
		return voucherRedemption;
	}

	public void setVoucherRedemption(boolean voucherRedemption) {
		this.voucherRedemption = voucherRedemption;
	}

	public boolean isVoucherRedeemSuccess() {
		return voucherRedeemSuccess;
	}

	public void setVoucherRedeemSuccess(boolean voucherRedeemSuccess) {
		this.voucherRedeemSuccess = voucherRedeemSuccess;
	}

	public boolean isRemoveVouchers() {
		return removeVouchers;
	}

	public void setRemoveVouchers(boolean removeVouchers) {
		this.removeVouchers = removeVouchers;
	}

	public boolean isVoucherEnabled() {
		voucherEnabled = AppSysParamsUtil.isVoucherEnabled();
		return voucherEnabled;
	}

	public void setVoucherEnabled(boolean voucherEnabled) {
		this.voucherEnabled = AppSysParamsUtil.isVoucherEnabled();
	}

	public boolean isCashPayment() {
		return cashPayment;
	}

	public void setCashPayment(boolean cashPayment) {
		this.cashPayment = cashPayment;
	}

	public String getHdnPGWPaymentEmail() {
		return hdnPGWPaymentEmail;
	}


	public void setHdnPGWPaymentEmail(String hdnPGWPaymentEmail) {
		this.hdnPGWPaymentEmail = hdnPGWPaymentEmail;
	}


	public String getHdnPGWPaymentCustomerName() {
		return hdnPGWPaymentCustomerName;
	}


	public void setHdnPGWPaymentCustomerName(String hdnPGWPaymentCustomerName) {
		this.hdnPGWPaymentCustomerName = hdnPGWPaymentCustomerName;
	}
}
