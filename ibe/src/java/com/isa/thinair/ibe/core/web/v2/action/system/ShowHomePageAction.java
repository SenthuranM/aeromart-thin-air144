/*
 * package com.isa.thinair.ibe.core.web.v2.action.system;
 * 
 * import org.apache.struts2.config.Namespace; import org.apache.struts2.config.Result; import
 * org.apache.struts2.config.Results;
 * 
 * import com.isa.thinair.ibe.core.web.constants.StrutsConstants; import com.isa.thinair.ibe.core.web.util.SessionUtil;
 * import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction; import com.opensymphony.xwork2.ActionChainResult;
 * 
 * @Namespace(StrutsConstants.Namespace.PUBLIC)
 * 
 * @Results({
 * 
 * @Result(name=StrutsConstants.Result.SESSION_EXPIRED, value=StrutsConstants.Jsp.Customer.SESSION_EXPIRED),
 * 
 * @Result(name=StrutsConstants.Result.CUSTOMER_HOME, type=ActionChainResult.class,
 * value=StrutsConstants.Action.SHOW_AVAILABILITY_SEARCH),
 * 
 * @Result(name=StrutsConstants.Jsp.Respro.INTERLINE_TMP, value=StrutsConstants.Jsp.Respro.INTERLINE_TMP),
 * 
 * @Result(name=StrutsConstants.Result.ERROR, value=StrutsConstants.Jsp.Common.ERROR)
 * 
 * })
 * 
 * public class ShowHomePageAction extends BaseRequestAwareAction {
 * 
 * 
 * public String execute() { String forward = ""; if (SessionUtil.isSessionExpired(request)){ forward =
 * StrutsConstants.Result.SESSION_EXPIRED; }else{ if(SessionUtil.isCustomerLoggedIn(request)){ forward =
 * StrutsConstants.Result.CUSTOMER_HOME; }else { forward = StrutsConstants.Jsp.Respro.INTERLINE_TMP; } } return forward;
 * } }
 */