package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciAirportTransferAction extends IBEBaseAction {

	private static final Log log = LogFactory.getLog(InterlineAnciAirportTransferAction.class);

	// output
	private boolean success = true;
	private String messageTxt;

	private List<LCCFlightSegmentAirportServiceDTO> airportTransferList;
	private List<FlightSegmentTO> flightSegmentTOs;

	private boolean allowModify = false;

	// input
	private boolean modifyAncillary = false;
	private String oldAllSegments;
	private String modifySegmentRefNos;
	private String resPaxInfo;

	public String execute() {

		try {
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			String strTxnIdntifier = ibeResInfo.getTransactionId();

			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}

			SYSTEM system = ibeResInfo.getSelectedSystem();

			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());

			List<FlightSegmentTO> selFlightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();

			List<FlightSegmentTO> flightSegmentTOs = mergeFlightSegmentList(selFlightSegmentTOs);

			SegmentUtil.setFltSegSequence(flightSegmentTOs, null);

			Map<String, Integer> serviceCount = null;
			if (resPaxInfo != null && !"".equals(resPaxInfo)) {
				Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPaxInfo);
				serviceCount = SSRServicesUtil.extractAiportServiceCountFromJsonObj(colpaxs);
			} else {
				serviceCount = new HashMap<String, Integer>();
			}

			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability = ModuleServiceLocator
					.getAirproxyAncillaryBD().getAvailableAiportTransfers(flightSegmentTOs, AppIndicatorEnum.APP_IBE,
							system, serviceCount, false);

			List<LCCFlightSegmentAirportServiceDTO> aptList = populateLCCAiportTransferList(aiportWiseAvailability);
			filterServicesForSelectedSegments(aptList, selFlightSegmentTOs);
			populateResponseFlightList();
			//Ancillary modifications are currently not available to IBE. Will allow this later
			//allowModify = AppSysParamsUtil.isAllowEditAirportTransfers();
			allowModify = false;

		} catch (Exception e) {
			log.error("Error in fetching Airport Tranfer data", e);
			success = false;
			messageTxt = e.getMessage();
		}

		return StrutsConstants.Result.SUCCESS;
	}

	private List<FlightSegmentTO> mergeFlightSegmentList(List<FlightSegmentTO> selectedFltSegList) throws Exception {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		flightSegmentTOs.addAll(selectedFltSegList);

		if (!modifyAncillary && oldAllSegments != null && !"".equals(oldAllSegments)) {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);

			if (colsegs != null) {
				if (modifySegmentRefNos != null && !"".equals(modifySegmentRefNos)) {
					String[] arrModifingAllSegs = modifySegmentRefNos.split(":");
					Collection<LCCClientReservationSegment> removableCol = new ArrayList<LCCClientReservationSegment>();

					for (int i = 0; i < arrModifingAllSegs.length; i++) {
						for (LCCClientReservationSegment resSeg : colsegs) {
							if (resSeg.getBookingFlightSegmentRefNumber().equals(arrModifingAllSegs[i])) {
								removableCol.add(resSeg);
								break;
							}
						}
					}

					if (removableCol.size() > 0) {
						colsegs.removeAll(removableCol);
					}
				}

				flightSegmentTOs.addAll(ReservationBeanUtil.convertReservationSegmentsToFlightSegmentTOs(colsegs));
			}

		}

		return flightSegmentTOs;
	}

	private List<LCCFlightSegmentAirportServiceDTO> populateLCCAiportTransferList(
			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability) {
		List<LCCFlightSegmentAirportServiceDTO> lccAvailabilityList = new ArrayList<LCCFlightSegmentAirportServiceDTO>();

		for (Entry<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> entry : aiportWiseAvailability.entrySet()) {
			AirportServiceKeyTO keyTO = entry.getKey();

			LCCFlightSegmentAirportServiceDTO airportServiceDTO = entry.getValue();
			airportServiceDTO.setFlightSegmentTO(airportServiceDTO.getFlightSegmentTO().clone());
			airportServiceDTO.getFlightSegmentTO().setAirportCode(keyTO.getAirport());
			airportServiceDTO.getFlightSegmentTO().setAirportType(keyTO.getAirportType());
			lccAvailabilityList.add(airportServiceDTO);
		}
		SortUtil.sortAirportServicesByFltDeparture(lccAvailabilityList);
		return lccAvailabilityList;
	}

	private void filterServicesForSelectedSegments(List<LCCFlightSegmentAirportServiceDTO> apsList,
			List<FlightSegmentTO> selflightSegmentTOs) {
		airportTransferList = new ArrayList<LCCFlightSegmentAirportServiceDTO>();
		long diffMilsForDomestic = AppSysParamsUtil.getAirportTransferStopCutoverForDomesticInMillis();
		long diffMilsForInternational = AppSysParamsUtil.getAirportTransferStopCutoverForInternationalInMillis();
		Date currentTime = new Date();

		if (apsList != null) {
			for (LCCFlightSegmentAirportServiceDTO lccFlightSegmentAirportTransferDTO : apsList) {
				for (FlightSegmentTO flightSegmentTO : selflightSegmentTOs) {
					boolean isWithinCutoffTime = false;
					if (((flightSegmentTO.getDepartureDateTimeZulu().getTime() - diffMilsForDomestic < currentTime.getTime()) && flightSegmentTO
							.isDomesticFlight())
							|| ((flightSegmentTO.getDepartureDateTimeZulu().getTime() - diffMilsForInternational < currentTime
									.getTime()) && !flightSegmentTO.isDomesticFlight())) {
						isWithinCutoffTime = true;
					}
					if (!isWithinCutoffTime) {
						String[] fltRefArr = flightSegmentTO.getFlightRefNumber().split("-");
						String[] apFltRefArr = lccFlightSegmentAirportTransferDTO.getFlightSegmentTO().getFlightRefNumber()
								.split("-");
						if (fltRefArr != null && fltRefArr.length > 0 && apFltRefArr != null && apFltRefArr.length > 0
								&& fltRefArr[0].equals(apFltRefArr[0])) {
							airportTransferList.add(lccFlightSegmentAirportTransferDTO);
						}
					}
				}
			}
		}
	}

	private void populateResponseFlightList() {
		flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		for (LCCFlightSegmentAirportServiceDTO apt : airportTransferList) {
			flightSegmentTOs.add(apt.getFlightSegmentTO());
		}
	}

	public List<LCCFlightSegmentAirportServiceDTO> getAirportTransferList() {
		return airportTransferList;
	}

	public void setAirportTransferList(List<LCCFlightSegmentAirportServiceDTO> airportTransferList) {
		this.airportTransferList = airportTransferList;
	}

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public String getResPaxInfo() {
		return resPaxInfo;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public boolean isAllowModify() {
		return allowModify;
	}

	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

}
