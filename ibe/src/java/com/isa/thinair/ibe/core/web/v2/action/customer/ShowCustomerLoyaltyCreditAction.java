package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.CustomerLoyaltyCreditDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerLoyaltyCreditAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowCustomerLoyaltyCreditAction.class);

	private boolean success = true;

	private String messageTxt;

	private Collection<CustomerLoyaltyCreditDTO> loyaltyCreditDTO;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			Customer customer = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			int intCustomerID = customer.getCustomerId();
			Collection reservMashCreditColls = ModuleServiceLocator.getReservationQueryBD().getMashreqCredits(intCustomerID);
			loyaltyCreditDTO = CustomerUtilV2.getCustomerMashreqLoyaltyCredit(reservMashCreditColls);
		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowCustomerLoyaltyCreditAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowCustomerLoyaltyCreditAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<CustomerLoyaltyCreditDTO> getLoyaltyCreditDTO() {
		return loyaltyCreditDTO;
	}

	public void setLoyaltyCreditDTO(Collection<CustomerLoyaltyCreditDTO> loyaltyCreditDTO) {
		this.loyaltyCreditDTO = loyaltyCreditDTO;
	}

}
