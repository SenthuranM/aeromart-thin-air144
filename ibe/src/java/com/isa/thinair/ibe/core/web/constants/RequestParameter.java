package com.isa.thinair.ibe.core.web.constants;

public interface RequestParameter {

	public static final String HIDDEN_PARAM_DATA = "hdnParamData";

	public static final String HIDDEN_LANGUAGE = "hdnLanguage";

	public static final String MODE = "hdnMode";

	public static final String CARRIER = "hdnCarrier";

}
