package com.isa.thinair.ibe.core.service;

import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class IBEModuleUtils {

	public static IBEConfig getConfig() {
		return (IBEConfig) LookupServiceFactory.getInstance().getBean("isa:base://modules/ibe?id=ibeConfig");
	}

	/**
	 * returns GlobalConfig
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}
	
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}
	
	
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getConfig(), "ibe",
				"ibe.config.dependencymap.invalid");
	}
	
	/**
	 * 
	 * @return LocationBD the Location delegate
	 */
	public final static LocationBD getLocationServiceBD() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}
}
