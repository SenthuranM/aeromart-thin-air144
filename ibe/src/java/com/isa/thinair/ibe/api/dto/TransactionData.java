package com.isa.thinair.ibe.api.dto;

import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;

public class TransactionData {
	private Collection<OndFareDTO> fares;

	/**
	 * @return the fares
	 */
	public Collection<OndFareDTO> getFares() {
		return this.fares;
	}

	/**
	 * @param fares
	 *            the fares to set
	 */
	public void setFares(Collection<OndFareDTO> fares) {
		this.fares = fares;
	}

}
