package com.isa.thinair.ibe.api.dto;

public class PaymentSummaryTO {
	private String totalPrice;
	private String totalFee;
	private String totalFare;

	/**
	 * Total segment fare. This is only applicable if return fare are picked.
	 */
	private String totalSegmentFare;

	private String totalTax;
	private String totalTaxSurcharge;

	/**
	 * Total IBE fare discount applicable. This is dependent on whether return fares were selected and whether
	 * corresponding segment fare total is greater. In effect this is just a fake discount. :)
	 */
	private String totalIbeFareDiscount;

	/**
	 * Store total promotion discount offered
	 */
	private String totalIbePromoDiscount;

	private String loyaltyCredit;
	private String reservationCredit;
	private String totalPayable; // Charith TODO:May need to break this to inbound/outbound
	private String currency;
	private String exchangeRate;
	private String creditableAmount;
	private String modifyCharge;
	private String balaceToPay;
	private String creditBalnce;
	// TODO:Refactor
	private String totalReservationCredit;

	public PaymentSummaryTO() {
		String zero = "0.00";
		this.totalPrice = zero;
		this.totalFee = zero;
		this.totalFare = zero;
		this.totalTax = zero;
		this.totalTaxSurcharge = zero;
		this.totalPayable = zero;
		this.loyaltyCredit = zero;
		this.reservationCredit = zero;
		this.balaceToPay = zero;
		this.creditBalnce = zero;
		this.totalReservationCredit = zero;
		this.setTotalSegmentFare(zero);
		this.totalIbeFareDiscount = zero;
		this.totalIbePromoDiscount = zero;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}

	public String getTotalTaxSurcharge() {
		return totalTaxSurcharge;
	}

	public void setTotalTaxSurcharge(String totalTaxSurcharge) {
		this.totalTaxSurcharge = totalTaxSurcharge;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getLoyaltyCredit() {
		return loyaltyCredit;
	}

	public void setLoyaltyCredit(String loyaltyCredit) {
		this.loyaltyCredit = loyaltyCredit;
	}

	public String getTotalPayable() {
		return totalPayable;
	}

	public void setTotalPayable(String totalPayable) {
		this.totalPayable = totalPayable;
	}

	public String getCreditableAmount() {
		return creditableAmount;
	}

	public void setCreditableAmount(String creditableAmount) {
		this.creditableAmount = creditableAmount;
	}

	public String getModifyCharge() {
		return modifyCharge;
	}

	public void setModifyCharge(String modifyCharge) {
		this.modifyCharge = modifyCharge;
	}

	public String getReservationCredit() {
		return reservationCredit;
	}

	public void setReservationCredit(String reservationCredit) {
		this.reservationCredit = reservationCredit;
	}

	public String getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}

	public String getBalaceToPay() {
		return balaceToPay;
	}

	public void setBalaceToPay(String balaceToPay) {
		this.balaceToPay = balaceToPay;
	}

	public String getCreditBalnce() {
		return creditBalnce;
	}

	public void setCreditBalnce(String creditBalnce) {
		this.creditBalnce = creditBalnce;
	}

	public String getTotalReservationCredit() {
		return totalReservationCredit;
	}

	public void setTotalReservationCredit(String totalReservationCredit) {
		this.totalReservationCredit = totalReservationCredit;
	}

	public String getTotalIbeFareDiscount() {
		return totalIbeFareDiscount;
	}

	public void setTotalIbeFareDiscount(String totalIbeFareDiscount) {
		this.totalIbeFareDiscount = totalIbeFareDiscount;
	}

	/**
	 * @return the totalSegmentFare
	 */
	public String getTotalSegmentFare() {
		return totalSegmentFare;
	}

	/**
	 * @param totalSegmentFare
	 *            the totalSegmentFare to set
	 */
	public void setTotalSegmentFare(String totalSegmentFare) {
		this.totalSegmentFare = totalSegmentFare;
	}

	public String getTotalIbePromoDiscount() {
		return totalIbePromoDiscount;
	}

	public void setTotalIbePromoDiscount(String totalIbePromoDiscount) {
		this.totalIbePromoDiscount = totalIbePromoDiscount;
	}
}