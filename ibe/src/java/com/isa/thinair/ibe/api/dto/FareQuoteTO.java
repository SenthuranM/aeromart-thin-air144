package com.isa.thinair.ibe.api.dto;

import java.util.Collection;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.PaxFareTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxPriceTO;
import com.isa.thinair.webplatform.api.v2.reservation.SurchargeFETO;
import com.isa.thinair.webplatform.api.v2.reservation.TaxFETO;

public class FareQuoteTO {

	private Collection<PaxFareTO> paxWise;

	private Collection<PaxPriceTO> paxWisePrice;

	// Per pax tax breakdown for the fare quote.
	private Collection<TaxFETO> perPaxTaxBD;

	// Per pax surcharge breakdown for the fare quote.
	private Collection<SurchargeFETO> perPaxSurchargeBD;

	private boolean hasSeatCharge = false;

	private boolean hasMealCharge = false;

	private boolean hasAirportServiceCharge = false;

	private boolean hasAirportTransferCharge = false;

	private boolean hasSsrCharge = false;

	private boolean hasInsurance = false;

	private boolean hasModifySegment = false;

	private boolean hasReservationCredit = false;

	private String seatMapCharge;

	private String mealCharge;

	private String airportServiceCharge;

	private String airportTransferCharge;

	private String ssrCharge;

	private String insuranceCharge;

	private boolean hasFareQuote = false;

	private PaymentSummaryTO inBaseCurr = new PaymentSummaryTO();

	private PaymentSummaryTO inSelectedCurr = new PaymentSummaryTO();

	private Collection<SegmentFareTO> segmentFare;

	private boolean hasFee = false;

	private boolean hasLoyaltyCredit = false;

	private String flexiCharge;

	private boolean hasFlexi = false;

	private String reservationBalance;

	private boolean hasReservationBalance;

	private boolean hasBalanceToPay;
	// TODO:Refactor
	private boolean hasCreditBalance;
	// TODO:Refactor
	private boolean hasTotalReservationCredit;

	private boolean hasBaggageCharge = false;

	private String baggageCharge;

	private boolean hasNextSeatPromoCharge = false;

	private String nextSeatPromoCharge;

	private int fareType;

	private boolean hasPromoDiscount;

	// store whether or not to deduct promotion discount from total
	private boolean deductFromTotal;

	private boolean jnTaxApplicable;

	private String jnTaxAmount;

	private boolean anciPenaltyApplicable;

	private String anciPenaltyAmount;

	// TODO:RW remove this
	@Deprecated
	private String strOndFlexiTotalChargeInbound;
	@Deprecated
	private String strOndFlexiTotalChargeOutbound;

	public FareQuoteTO() {
		this.setCurrency(AppSysParamsUtil.getBaseCurrency());
		this.setFlexiCharge(AccelAeroCalculator.getDefaultBigDecimalZero().toString());
	}

	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice() {
		return inBaseCurr.getTotalPrice();
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(String totalPrice) {
		this.inBaseCurr.setTotalPrice(totalPrice);
	}

	public String getTotalSegmentFare() {
		return inBaseCurr.getTotalSegmentFare();
	}

	public void setTotalSegmentFare(String totalSegmentFare) {
		this.inBaseCurr.setTotalSegmentFare(totalSegmentFare);
	}

	public String getTotalSelectedCurSegmentFare() {
		return inSelectedCurr.getTotalSegmentFare();
	}

	public void setTotalSelectedCurSegmentFare(String totalSegmentFare) {
		this.inSelectedCurr.setTotalSegmentFare(totalSegmentFare);
	}

	public boolean isHasFareQuote() {
		return hasFareQuote;
	}

	public void setHasFareQuote(boolean hasFareQuote) {
		this.hasFareQuote = hasFareQuote;
	}

	public String getTotalFare() {
		return inBaseCurr.getTotalFare();
	}

	public void setTotalFare(String totalFare) {
		this.inBaseCurr.setTotalFare(totalFare);
	}

	public void setCreditableAmount(String creditableAmount) {
		this.inBaseCurr.setCreditableAmount(creditableAmount);
	}

	public void setModifyCharge(String modifyCharge) {
		this.inBaseCurr.setModifyCharge(modifyCharge);
	}

	public String getTotalTaxSurcharge() {
		return inBaseCurr.getTotalTaxSurcharge();
	}

	public void setTotalTaxSurcharge(String totalTaxSurcharge) {
		this.inBaseCurr.setTotalTaxSurcharge(totalTaxSurcharge);
	}

	public String getTotalTax() {
		return inBaseCurr.getTotalTax();
	}

	public void setTotalTax(String totalTax) {
		this.inBaseCurr.setTotalTax(totalTax);
	}

	public String getCurrency() {
		return inBaseCurr.getCurrency();
	}

	public void setCurrency(String currency) {
		this.inBaseCurr.setCurrency(currency);
	}

	public String getSelectedCurrency() {
		return inSelectedCurr.getCurrency();
	}

	public void setSelectedCurrency(String selectedCurrency) {
		inSelectedCurr.setCurrency(selectedCurrency);
	}

	public String getSelectedEXRate() {
		return inSelectedCurr.getExchangeRate();
	}

	public void setSelectedEXRate(String selectedEXRate) {
		inSelectedCurr.setExchangeRate(selectedEXRate);
	}

	public String getSelectedtotalPrice() {
		return inSelectedCurr.getTotalPrice();
	}

	public void setSelectedtotalPrice(String selectedtotalPrice) {
		inSelectedCurr.setTotalPrice(selectedtotalPrice);
	}

	/**
	 * @return the totalFee
	 */
	public String getTotalFee() {
		return inBaseCurr.getTotalFee();
	}

	/**
	 * @param totalFee
	 *            the totalFee to set
	 */
	public void setTotalFee(String totalFee) {
		this.inBaseCurr.setTotalFee(totalFee);
	}

	public void setTotalFeeInSelected(String totalFeeSel) {
		this.inSelectedCurr.setTotalFee(totalFeeSel);
	}

	public boolean isHasSeatCharge() {
		return hasSeatCharge;
	}

	public void setHasSeatCharge(boolean hasSeatCharge) {
		this.hasSeatCharge = hasSeatCharge;
	}

	public boolean isHasMealCharge() {
		return hasMealCharge;
	}

	public void setHasMealCharge(boolean hasMealCharge) {
		this.hasMealCharge = hasMealCharge;
	}

	public boolean isHasAirportServiceCharge() {
		return hasAirportServiceCharge;
	}

	public void setHasAirportServiceCharge(boolean hasAirportServiceCharge) {
		this.hasAirportServiceCharge = hasAirportServiceCharge;
	}

	public boolean isHasAirportTransferCharge() {
		return hasAirportTransferCharge;
	}

	public void setHasAirportTransferCharge(boolean hasAirportTransferCharge) {
		this.hasAirportTransferCharge = hasAirportTransferCharge;
	}

	public boolean isHasSsrCharge() {
		return hasSsrCharge;
	}

	public void setHasSsrCharge(boolean hasInflightServiceCharge) {
		this.hasSsrCharge = hasInflightServiceCharge;
	}

	public String getSeatMapCharge() {
		return seatMapCharge;
	}

	public void setSeatMapCharge(String seatMapCharge) {
		this.seatMapCharge = seatMapCharge;
	}

	public String getMealCharge() {
		return mealCharge;
	}

	public void setMealCharge(String mealCharge) {
		this.mealCharge = mealCharge;
	}

	public String getAirportServiceCharge() {
		return airportServiceCharge;
	}

	public void setAirportServiceCharge(String airportServiceCharge) {
		this.airportServiceCharge = airportServiceCharge;
	}

	public String getAirportTransferCharge() {
		return airportTransferCharge;
	}

	public void setAirportTransferCharge(String airportTransferCharge) {
		this.airportTransferCharge = airportTransferCharge;
	}

	public String getSsrCharge() {
		return ssrCharge;
	}

	public void setSsrCharge(String inflightServiceCharge) {
		this.ssrCharge = inflightServiceCharge;
	}

	public boolean isHasInsurance() {
		return hasInsurance;
	}

	public void setHasInsurance(boolean hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	public String getInsuranceCharge() {
		return insuranceCharge;
	}

	public void setInsuranceCharge(String insuranceCharge) {
		this.insuranceCharge = insuranceCharge;
	}

	public Collection<SegmentFareTO> getSegmentFare() {
		return segmentFare;
	}

	public void setSegmentFare(Collection<SegmentFareTO> segmentFare) {
		this.segmentFare = segmentFare;
	}

	public String getFlexiCharge() {
		return flexiCharge;
	}

	public void setFlexiCharge(String flexiCharge) {
		this.flexiCharge = flexiCharge;
	}

	public boolean isHasFlexi() {
		return hasFlexi;
	}

	public void setHasFlexi(boolean hasFlexi) {
		this.hasFlexi = hasFlexi;
	}

	public Collection<PaxFareTO> getPaxWise() {
		return paxWise;
	}

	public void setPaxWise(Collection<PaxFareTO> paxWise) {
		this.paxWise = paxWise;
	}

	public Collection<PaxPriceTO> getPaxWisePrice() {
		return paxWisePrice;
	}

	public void setPaxWisePrice(Collection<PaxPriceTO> paxWisePrice) {
		this.paxWisePrice = paxWisePrice;
	}

	public PaymentSummaryTO getInBaseCurr() {
		return inBaseCurr;
	}

	public void setInBaseCurr(PaymentSummaryTO inBaseCurr) {
		this.inBaseCurr = inBaseCurr;
	}

	public PaymentSummaryTO getInSelectedCurr() {
		return inSelectedCurr;
	}

	public void setInSelectedCurr(PaymentSummaryTO inSelectedCurr) {
		this.inSelectedCurr = inSelectedCurr;
	}

	public void setHasFee(boolean hasFee) {
		this.hasFee = hasFee;
	}

	public boolean isHasFee() {
		return this.hasFee;
	}

	public String getLoyaltyCredit() {
		return this.inBaseCurr.getLoyaltyCredit();
	}

	public void setLoyaltyCredit(String loyaltyCredit) {
		this.inBaseCurr.setLoyaltyCredit(loyaltyCredit);
	}

	public boolean isHasLoyaltyCredit() {
		return hasLoyaltyCredit;
	}

	public void setHasLoyaltyCredit(boolean hasLoyaltyCredit) {
		this.hasLoyaltyCredit = hasLoyaltyCredit;
	}

	public String getTotalPayable() {
		return this.inBaseCurr.getTotalPayable();
	}

	public void setTotalPayable(String totalPayable) {
		this.inBaseCurr.setTotalPayable(totalPayable);
	}

	public String getSelectedTotalPayable() {
		return this.inSelectedCurr.getTotalPayable();
	}

	public void setSelectedTotalPayable(String totalPayable) {
		this.inSelectedCurr.setTotalPayable(totalPayable);
	}

	public boolean isHasModifySegment() {
		return hasModifySegment;
	}

	public void setHasModifySegment(boolean hasModifySegment) {
		this.hasModifySegment = hasModifySegment;
	}

	public boolean isHasReservationCredit() {
		return hasReservationCredit;
	}

	public void setHasReservationCredit(boolean hasReservationCredit) {
		this.hasReservationCredit = hasReservationCredit;
	}

	public String getReservationCredit() {
		return this.inBaseCurr.getReservationCredit();
	}

	public void setReservationCredit(String reservationCredit) {
		this.inBaseCurr.setReservationCredit(reservationCredit);
	}

	public void setReservationBalance(String reservationBalance) {
		this.reservationBalance = reservationBalance;
	}

	public String getReservationBalance() {
		return reservationBalance;
	}

	public void setHasReservationBalance(boolean hasReservationBalance) {
		this.hasReservationBalance = hasReservationBalance;
	}

	public boolean isHasReservationBalance() {
		return hasReservationBalance;
	}

	public boolean isHasBalanceToPay() {
		return hasBalanceToPay;
	}

	public void setHasBalanceToPay(boolean hasBalanceToPay) {
		this.hasBalanceToPay = hasBalanceToPay;
	}

	public boolean isHasCreditBalance() {
		return hasCreditBalance;
	}

	public void setHasCreditBalance(boolean hasCreditBalance) {
		this.hasCreditBalance = hasCreditBalance;
	}

	public boolean isHasTotalReservationCredit() {
		return hasTotalReservationCredit;
	}

	public void setHasTotalReservationCredit(boolean hasTotalReservationCredit) {
		this.hasTotalReservationCredit = hasTotalReservationCredit;
	}

	public Collection<TaxFETO> getPerPaxTaxBD() {
		return perPaxTaxBD;
	}

	public void setPerPaxTaxBD(Collection<TaxFETO> perPaxTaxBD) {
		this.perPaxTaxBD = perPaxTaxBD;
	}

	public Collection<SurchargeFETO> getPerPaxSurchargeBD() {
		return perPaxSurchargeBD;
	}

	public void setPerPaxSurchargeBD(Collection<SurchargeFETO> perPaxSurchargeBD) {
		this.perPaxSurchargeBD = perPaxSurchargeBD;
	}

	/**
	 * @return the hasBaggageCharge
	 */
	public boolean isHasBaggageCharge() {
		return hasBaggageCharge;
	}

	/**
	 * @param hasBaggageCharge
	 *            the hasBaggageCharge to set
	 */
	public void setHasBaggageCharge(boolean hasBaggageCharge) {
		this.hasBaggageCharge = hasBaggageCharge;
	}

	/**
	 * @return the baggageCharge
	 */
	public String getBaggageCharge() {
		return baggageCharge;
	}

	/**
	 * @param baggageCharge
	 *            the baggageCharge to set
	 */
	public void setBaggageCharge(String baggageCharge) {
		this.baggageCharge = baggageCharge;
	}

	public boolean isHasNextSeatPromoCharge() {
		return hasNextSeatPromoCharge;
	}

	public void setHasNextSeatPromoCharge(boolean hasNextSeatPromoCharge) {
		this.hasNextSeatPromoCharge = hasNextSeatPromoCharge;
	}

	public String getNextSeatPromoCharge() {
		return nextSeatPromoCharge;
	}

	public void setNextSeatPromoCharge(String nextSeatPromoCharge) {
		this.nextSeatPromoCharge = nextSeatPromoCharge;
	}

	public int getFareType() {
		return fareType;
	}

	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	public String getTotalIbeFareDiscount() {
		return inBaseCurr.getTotalIbeFareDiscount();
	}

	public void setTotalIbeFareDiscount(String totalSelectedIbeFareDiscount) {
		inBaseCurr.setTotalIbeFareDiscount(totalSelectedIbeFareDiscount);
	}

	public String getTotalSelectedIbeFareDiscount() {
		return inSelectedCurr.getTotalIbeFareDiscount();
	}

	public void setTotalSelectedIbeFareDiscount(String totalSelectedIbeFareDiscount) {
		inSelectedCurr.setTotalIbeFareDiscount(totalSelectedIbeFareDiscount);
	}

	public String getTotalSelectedFee() {
		return inSelectedCurr.getTotalFee();
	}

	public void getTotalSelectedFee(String totalFee) {
		inSelectedCurr.setTotalFee(totalFee);
	}

	/**
	 * 
	 * If ibeFareDiscount is greater than 0 that means we have discounts available.
	 */
	public boolean getHasIbeDiscount() {
		return Double.parseDouble(getTotalIbeFareDiscount()) > 0;
	}

	public String getTotalIbePromoDiscount() {
		return inBaseCurr.getTotalIbePromoDiscount();
	}

	public void setTotalIbePromoDiscount(String totalIbePromoDiscount) {
		inBaseCurr.setTotalIbePromoDiscount(totalIbePromoDiscount);
	}

	public String getTotalSelectedIbePromoDiscount() {
		return inSelectedCurr.getTotalIbePromoDiscount();
	}

	public void setTotalSelectedIbePromoDiscount(String totalSelectedIbePromoDiscount) {
		inSelectedCurr.setTotalIbePromoDiscount(totalSelectedIbePromoDiscount);
	}

	public boolean isHasPromoDiscount() {
		return hasPromoDiscount;
	}

	public void setHasPromoDiscount(boolean hasPromoDiscount) {
		this.hasPromoDiscount = hasPromoDiscount;
	}

	public boolean isDeductFromTotal() {
		return deductFromTotal;
	}

	public void setDeductFromTotal(boolean deductFromTotal) {
		this.deductFromTotal = deductFromTotal;
	}

	public boolean isJnTaxApplicable() {
		return jnTaxApplicable;
	}

	public void setJnTaxApplicable(boolean jnTaxApplicable) {
		this.jnTaxApplicable = jnTaxApplicable;
	}

	public String getJnTaxAmount() {
		return jnTaxAmount;
	}

	public void setJnTaxAmount(String jnTaxAmount) {
		this.jnTaxAmount = jnTaxAmount;
	}

	public boolean isAnciPenaltyApplicable() {
		return anciPenaltyApplicable;
	}

	public void setAnciPenaltyApplicable(boolean anciPenaltyApplicable) {
		this.anciPenaltyApplicable = anciPenaltyApplicable;
	}

	public String getAnciPenaltyAmount() {
		return anciPenaltyAmount;
	}

	public void setAnciPenaltyAmount(String anciPenaltyAmount) {
		this.anciPenaltyAmount = anciPenaltyAmount;
	}

	public String getStrOndFlexiTotalChargeInbound() {
		return strOndFlexiTotalChargeInbound;
	}

	public void setStrOndFlexiTotalChargeInbound(String strOndFlexiTotalChargeInbound) {
		this.strOndFlexiTotalChargeInbound = strOndFlexiTotalChargeInbound;
	}

	public String getStrOndFlexiTotalChargeOutbound() {
		return strOndFlexiTotalChargeOutbound;
	}

	public void setStrOndFlexiTotalChargeOutbound(String strOndFlexiTotalChargeOutbound) {
		this.strOndFlexiTotalChargeOutbound = strOndFlexiTotalChargeOutbound;
	}
}
