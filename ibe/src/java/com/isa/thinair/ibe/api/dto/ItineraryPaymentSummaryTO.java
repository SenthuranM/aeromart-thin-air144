package com.isa.thinair.ibe.api.dto;

import java.util.Collection;

public class ItineraryPaymentSummaryTO {
	private String currency;

	private String totalAmount;

	private String selCurrency;

	private String totalCurrencyAmount;

	private Collection<ItineraryPaymentListTO> payDetails;

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the totalAmount
	 */
	public String getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            the totalAmount to set
	 */
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the payDetails
	 */
	public Collection<ItineraryPaymentListTO> getPayDetails() {
		return payDetails;
	}

	/**
	 * @param payDetails
	 *            the payDetails to set
	 */
	public void setPayDetails(Collection<ItineraryPaymentListTO> payDetails) {
		this.payDetails = payDetails;
	}

	public String getSelCurrency() {
		return selCurrency;
	}

	public void setSelCurrency(String selCurrency) {
		this.selCurrency = selCurrency;
	}

	public String getTotalCurrencyAmount() {
		return totalCurrencyAmount;
	}

	public void setTotalCurrencyAmount(String totalCurrencyAmount) {
		this.totalCurrencyAmount = totalCurrencyAmount;
	}

}
