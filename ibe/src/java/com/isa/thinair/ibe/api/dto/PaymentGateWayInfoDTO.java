package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.ibe.api.dto.v2.CardInfo;
import com.isa.thinair.paymentbroker.api.util.CVVOptions;

public class PaymentGateWayInfoDTO implements Serializable {

	private static final long serialVersionUID = 3190271753568488500L;

	private String payCurrency;

	private String providerName;

	private String providerCode;

	private String description;

	private int paymentGateway;

	private int cvvOption = CVVOptions.CARD_CVV_OPTION_MANDATORY;

	private String payCurrencyAmount;

	private String brokerType;
	// TODO Remove
	private Collection<String[]> cardTypes;

	private Collection<CardInfo> cardInfo;

	private boolean switchToExternalURL;

	private boolean viewPaymentInIframe;

	private boolean isModifyAllow;
	
	private int onholdReleaseTime;

	public String getPayCurrency() {
		return payCurrency;
	}

	public void setPayCurrency(String payCurrency) {
		this.payCurrency = payCurrency;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(int paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public int getCvvOption() {
		return cvvOption;
	}

	public void setCvvOption(int cvvOption) {
		this.cvvOption = cvvOption;
	}

	public String getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	public void setPayCurrencyAmount(String payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}

	public String getBrokerType() {
		return brokerType;
	}

	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	public Collection<String[]> getCardTypes() {
		return cardTypes;
	}

	public void setCardTypes(Collection<String[]> cardTypes) {
		this.cardTypes = cardTypes;
	}

	public Collection<CardInfo> getCardInfo() {
		return cardInfo;
	}

	public void setCardInfo(Collection<CardInfo> cardInfo) {
		this.cardInfo = cardInfo;
	}

	public boolean isSwitchToExternalURL() {
		return switchToExternalURL;
	}

	public void setSwitchToExternalURL(boolean switchToExternalURL) {
		this.switchToExternalURL = switchToExternalURL;
	}

	/**
	 * @return the viewPaymentInIframe
	 */
	public boolean isViewPaymentInIframe() {
		return viewPaymentInIframe;
	}

	/**
	 * @param viewPaymentInIframe
	 *            the viewPaymentInIframe to set
	 */
	public void setViewPaymentInIframe(boolean viewPaymentInIframe) {
		this.viewPaymentInIframe = viewPaymentInIframe;
	}

	public boolean isModifyAllow() {
		return isModifyAllow;
	}

	public void setModifyAllow(boolean isModifyAllow) {
		this.isModifyAllow = isModifyAllow;
	}

	public int getOnholdReleaseTime() {
		return onholdReleaseTime;
	}

	public void setOnholdReleaseTime(int onholdReleaseTime) {
		this.onholdReleaseTime = onholdReleaseTime;
	}

}
