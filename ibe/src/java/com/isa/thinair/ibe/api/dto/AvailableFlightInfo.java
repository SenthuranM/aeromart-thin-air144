package com.isa.thinair.ibe.api.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;

public class AvailableFlightInfo implements Comparable<AvailableFlightInfo> {

	private List<SegInfoDTO> segments;
	private boolean isSelected = false;
	private boolean returnFlag = false;
	private int priceGuidNo;
	private List<FlightFareSummaryTO> flightFareSummaryList;
	private Date departureDate;
	private Date arrivalDate;
	private String departureTime;
	private String arrivalTime;
	private String totalDuration;
	private List<String> viaPoints;
	private boolean seatAvailable;
	private String transitDuration;

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public List<SegInfoDTO> getSegments() {
		if (segments == null) {
			segments = new ArrayList<SegInfoDTO>();
		}
		return segments;
	}

	public void setSegments(List<SegInfoDTO> segments) {
		this.segments = segments;
	}

	public void AddSegment(SegInfoDTO segment) {
		getSegments().add(segment);
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public FlightFareSummaryTO getSelectedFlightFareSummary() {
		if (this.flightFareSummaryList != null) {
			for (FlightFareSummaryTO fareSummary : this.flightFareSummaryList) {
				if (fareSummary.isSelected()) {
					return fareSummary;
				}
			}
		}
		return null;
	}

	public List<FlightFareSummaryTO> getFlightFareSummaryList() {
		return flightFareSummaryList;
	}

	public void setFlightFareSummaryList(List<FlightFareSummaryTO> flightFareSummaryList) {
		this.flightFareSummaryList = flightFareSummaryList;
	}

	public int getPriceGuidNo() {
		return priceGuidNo;
	}

	public void setPriceGuidNo(int priceGuidNo) {
		this.priceGuidNo = priceGuidNo;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}

	public List<String> getViaPoints() {
		return viaPoints;
	}

	public void setViaPoints(List<String> viaPoints) {
		this.viaPoints = viaPoints;
	}

	@Override
	public int compareTo(AvailableFlightInfo availableFlightInfo) {
		int minDirectFlights = Math.min(getSegments().size(), availableFlightInfo.getSegments().size());
		Collections.sort(getSegments());
		Collections.sort(availableFlightInfo.getSegments());

		boolean sameSegSize = (getSegments().size() == availableFlightInfo.getSegments().size());
		int matchedSegs = 0;
		for (int i = 0; i < minDirectFlights; i++) {
			int compare = getSegments().get(i).compareTo(availableFlightInfo.getSegments().get(i));
			if (compare == 0) {
				if (sameSegSize
						&& getSegments().get(i).getFlightNumber()
								.equals(availableFlightInfo.getSegments().get(i).getFlightNumber())) {
					if ((i == minDirectFlights - 1) && (matchedSegs == minDirectFlights - 1)) {
						return 0;
					} else {
						matchedSegs++;
						continue;
					}
				}

			} else {
				return compare;
			}

		}
		return 1;
	}

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	public String getTransitDuration() {
		return transitDuration;
	}

	public void setTransitDuration(String transitDuration) {
		this.transitDuration = transitDuration;
	}

}
