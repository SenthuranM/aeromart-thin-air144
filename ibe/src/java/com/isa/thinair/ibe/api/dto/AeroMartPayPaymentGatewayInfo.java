package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;
import java.util.List;

public class AeroMartPayPaymentGatewayInfo {

	private int gatewayId;
	private String providerName;
	private String paymentCurrency;
	private BigDecimal paymentAmount;
	private String providerCode;
	private String description;
	private List<AeroMartPayCardInfo> cards;
	private String amountInPgBaseCurrency;
	private String userSelectedCurrency;

	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<AeroMartPayCardInfo> getCards() {
		return cards;
	}

	public void setCards(List<AeroMartPayCardInfo> cards) {
		this.cards = cards;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getAmountInPgBaseCurrency() {
		return amountInPgBaseCurrency;
	}

	public void setAmountInPgBaseCurrency(String amountInPgBaseCurrency) {
		this.amountInPgBaseCurrency = amountInPgBaseCurrency;
	}

	public String getUserSelectedCurrency() {
		return userSelectedCurrency;
	}

	public void setUserSelectedCurrency(String userSelectedCurrency) {
		this.userSelectedCurrency = userSelectedCurrency;
	}
}
