package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

/**
 * @author chethiya
 *
 */
public class FFIDChange implements Serializable{
	
	private static final long serialVersionUID = -3045025710152377389L;

	String paxSequence;
	
	String paxFFID;

	public String getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(String paxSequence) {
		this.paxSequence = paxSequence;
	}

	public String getPaxFFID() {
		return paxFFID;
	}

	public void setPaxFFID(String paxFFID) {
		this.paxFFID = paxFFID;
	}	
	
}
