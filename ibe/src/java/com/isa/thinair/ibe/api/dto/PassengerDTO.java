package com.isa.thinair.ibe.api.dto;

import com.isa.thinair.ibe.core.web.v2.util.StringUtil;

public class PassengerDTO {

	private String title;

	private String firstName;

	private String lastName;

	private String nationality;

	private String dateOfBirth;

	private Integer paxSequence;

	private String foidType;

	private String foidNumber;

	private String foidExpiry;

	private String foidPlace;

	private String ssrCode;

	private Integer travelWith;

	private Integer imageId;

	private String nationalIDNo;

	private boolean parent = false;

	private PriceDTO seatChrges = new PriceDTO();

	private PriceDTO insChrages = new PriceDTO();

	private PriceDTO paxFare = new PriceDTO();

	private PriceDTO paxTnS = new PriceDTO();

	private PriceDTO paxCredit = new PriceDTO();
	
	private String ffid;
	
	private boolean validFFID;

	public String getTitle() {
		return StringUtil.nullConvertToString(title);
	}

	public String getFirstName() {
		return StringUtil.nullConvertToString(firstName);
	}

	public String getLastName() {
		return StringUtil.nullConvertToString(lastName);
	}

	public String getNationality() {
		return StringUtil.nullConvertToString(nationality);
	}

	public String getDateOfBirth() {
		return StringUtil.nullConvertToString(dateOfBirth);
	}

	public PriceDTO getSeatChrges() {
		return seatChrges;
	}

	public PriceDTO getInsChrages() {
		return insChrages;
	}

	public PriceDTO getPaxFare() {
		return paxFare;
	}

	public PriceDTO getPaxTnS() {
		return paxTnS;
	}

	public PriceDTO getPaxCredit() {
		return paxCredit;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setSeatChrges(PriceDTO seatChrges) {
		this.seatChrges = seatChrges;
	}

	public void setInsChrages(PriceDTO insChrages) {
		this.insChrages = insChrages;
	}

	public void setPaxFare(PriceDTO paxFare) {
		this.paxFare = paxFare;
	}

	public void setPaxTnS(PriceDTO paxTnS) {
		this.paxTnS = paxTnS;
	}

	public void setPaxCredit(PriceDTO paxCredit) {
		this.paxCredit = paxCredit;
	}

	public Integer getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	public Integer getTravelWith() {
		return travelWith;
	}

	public void setTravelWith(Integer travelWith) {
		this.travelWith = travelWith;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getFoidType() {
		return StringUtil.nullConvertToString(foidType);
	}

	public String getFoidNumber() {
		return StringUtil.nullConvertToString(foidNumber);
	}

	public String getFoidExpiry() {
		return StringUtil.nullConvertToString(foidExpiry);
	}

	public void setFoidExpiry(String foidExpiry) {
		this.foidExpiry = foidExpiry;
	}

	public String getFoidPlace() {
		return StringUtil.nullConvertToString(foidPlace);
	}

	public void setFoidPlace(String foidPlace) {
		this.foidPlace = foidPlace;
	}

	public void setFoidType(String foidType) {
		this.foidType = foidType;
	}

	public void setFoidNumber(String foidNumber) {
		this.foidNumber = foidNumber;
	}

	public boolean isParent() {
		return parent;
	}

	public void setParent(boolean parent) {
		this.parent = parent;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}

	public String getFfid() {
		return StringUtil.nullConvertToString(ffid);
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	/**
	 * @return the validFFID
	 */
	public boolean isValidFFID() {
		return validFFID;
	}

	/**
	 * @param validFFID the validFFID to set
	 */
	public void setValidFFID(boolean validFFID) {
		this.validFFID = validFFID;
	}

	public String getNationalIDNo() {
		return nationalIDNo;
	}

	public void setNationalIDNo(String nationalIDNo) {
		this.nationalIDNo = nationalIDNo;
	}
}
