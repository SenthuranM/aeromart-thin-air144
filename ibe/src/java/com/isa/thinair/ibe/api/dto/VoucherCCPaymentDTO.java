package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.PAYPALResponse;

/**
 * Contain Voucher data from payment page and post card details page to other pages after the payment gateway response is
 * received
 * 
 * @author chanaka
 *
 */

public class VoucherCCPaymentDTO {	
	
	private int paymentBrokerRef; 
	
	private int tempTxId;
	
	private VoucherDTO voucherDTO;
	
	private List<VoucherDTO> voucherDTOList;
	
	private PaymentGateWayInfoDTO paymentGateWay;

	@SuppressWarnings("rawtypes")
	private Map temporyPaymentMap;

	private String ipgRefenceNo;

	private IPGResponseDTO ipgResponseDTO;

	private String version;
	
	private String selectedCurrency;

	private boolean responceReceived;

	private boolean noPay = false;

	private BigDecimal creditCardFee;

	private String transactionId;

	private PAYPALResponse paypalResponse;

	private boolean switchToExternalURL;

	private boolean securePayment3D;

	private boolean isMakePayment;

	private boolean viewPaymentInIframe;

	private String invoiceStatus;

	private String billId;
	
	private long timeToSpare; 
	
	private String errorCode;
	
	private boolean retryingWithAnotherIPG;
	
	private String voucherId;
	
	private String voucherGroupId;

	private String bank3DSecureHTML;

	/**
	 * @return the paymentBrokerRef
	 */
	public int getPaymentBrokerRef() {
		return paymentBrokerRef;
	}

	/**
	 * @param paymentBrokerRef the paymentBrokerRef to set
	 */
	public void setPaymentBrokerRef(int paymentBrokerRef) {
		this.paymentBrokerRef = paymentBrokerRef;
	}

	/**
	 * @return the tempTxId
	 */
	public int getTempTxId() {
		return tempTxId;
	}

	/**
	 * @param tempTxId the tempTxId to set
	 */
	public void setTempTxId(int tempTxId) {
		this.tempTxId = tempTxId;
	}

	/**
	 * @return the voucherDTO
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO the voucherDTO to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

	/**
	 * @return the voucherId
	 */
	public String getVoucherId() {
		return voucherId;
	}

	/**
	 * @param voucherId the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * @return the paymentGateWay
	 */
	public PaymentGateWayInfoDTO getPaymentGateWay() {
		return paymentGateWay;
	}

	/**
	 * @param paymentGateWay the paymentGateWay to set
	 */
	public void setPaymentGateWay(PaymentGateWayInfoDTO paymentGateWay) {
		this.paymentGateWay = paymentGateWay;
	}

	/**
	 * @return the temporyPaymentMap
	 */
	public Map getTemporyPaymentMap() {
		return temporyPaymentMap;
	}

	/**
	 * @param temporyPaymentMap the temporyPaymentMap to set
	 */
	public void setTemporyPaymentMap(Map temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	/**
	 * @return the ipgRefenceNo
	 */
	public String getIpgRefenceNo() {
		return ipgRefenceNo;
	}

	/**
	 * @param ipgRefenceNo the ipgRefenceNo to set
	 */
	public void setIpgRefenceNo(String ipgRefenceNo) {
		this.ipgRefenceNo = ipgRefenceNo;
	}

	/**
	 * @return the ipgResponseDTO
	 */
	public IPGResponseDTO getIpgResponseDTO() {
		return ipgResponseDTO;
	}

	/**
	 * @param ipgResponseDTO the ipgResponseDTO to set
	 */
	public void setIpgResponseDTO(IPGResponseDTO ipgResponseDTO) {
		this.ipgResponseDTO = ipgResponseDTO;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the selectedCurrency
	 */
	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	/**
	 * @param selectedCurrency the selectedCurrency to set
	 */
	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	/**
	 * @return the responceReceived
	 */
	public boolean isResponceReceived() {
		return responceReceived;
	}

	/**
	 * @param responceReceived the responceReceived to set
	 */
	public void setResponceReceived(boolean responceReceived) {
		this.responceReceived = responceReceived;
	}

	/**
	 * @return the noPay
	 */
	public boolean isNoPay() {
		return noPay;
	}

	/**
	 * @param noPay the noPay to set
	 */
	public void setNoPay(boolean noPay) {
		this.noPay = noPay;
	}

	/**
	 * @return the creditCardFee
	 */
	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	/**
	 * @param creditCardFee the creditCardFee to set
	 */
	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the paypalResponse
	 */
	public PAYPALResponse getPaypalResponse() {
		return paypalResponse;
	}

	/**
	 * @param paypalResponse the paypalResponse to set
	 */
	public void setPaypalResponse(PAYPALResponse paypalResponse) {
		this.paypalResponse = paypalResponse;
	}

	/**
	 * @return the switchToExternalURL
	 */
	public boolean isSwitchToExternalURL() {
		return switchToExternalURL;
	}

	/**
	 * @param switchToExternalURL the switchToExternalURL to set
	 */
	public void setSwitchToExternalURL(boolean switchToExternalURL) {
		this.switchToExternalURL = switchToExternalURL;
	}

	/**
	 * @return the securePayment3D
	 */
	public boolean isSecurePayment3D() {
		return securePayment3D;
	}

	/**
	 * @param securePayment3D the securePayment3D to set
	 */
	public void setSecurePayment3D(boolean securePayment3D) {
		this.securePayment3D = securePayment3D;
	}

	/**
	 * @return the isMakePayment
	 */
	public boolean isMakePayment() {
		return isMakePayment;
	}

	/**
	 * @param isMakePayment the isMakePayment to set
	 */
	public void setMakePayment(boolean isMakePayment) {
		this.isMakePayment = isMakePayment;
	}

	/**
	 * @return the viewPaymentInIframe
	 */
	public boolean isViewPaymentInIframe() {
		return viewPaymentInIframe;
	}

	/**
	 * @param viewPaymentInIframe the viewPaymentInIframe to set
	 */
	public void setViewPaymentInIframe(boolean viewPaymentInIframe) {
		this.viewPaymentInIframe = viewPaymentInIframe;
	}

	/**
	 * @return the invoiceStatus
	 */
	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	/**
	 * @param invoiceStatus the invoiceStatus to set
	 */
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	/**
	 * @return the billId
	 */
	public String getBillId() {
		return billId;
	}

	/**
	 * @param billId the billId to set
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	/**
	 * @return the timeToSpare
	 */
	public long getTimeToSpare() {
		return timeToSpare;
	}

	/**
	 * @param timeToSpare the timeToSpare to set
	 */
	public void setTimeToSpare(long timeToSpare) {
		this.timeToSpare = timeToSpare;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the retryingWithAnotherIPG
	 */
	public boolean isRetryingWithAnotherIPG() {
		return retryingWithAnotherIPG;
	}

	/**
	 * @param retryingWithAnotherIPG the retryingWithAnotherIPG to set
	 */
	public void setRetryingWithAnotherIPG(boolean retryingWithAnotherIPG) {
		this.retryingWithAnotherIPG = retryingWithAnotherIPG;
	}

	/**
	 * @return the bank3DSecureHTML
	 */
	public String getBank3DSecureHTML() {
		return bank3DSecureHTML;
	}

	/**
	 * @param bank3dSecureHTML
	 *            the bank3DSecureHTML to set
	 */
	public void setBank3DSecureHTML(String bank3dSecureHTML) {
		bank3DSecureHTML = bank3dSecureHTML;
	}

	public List<VoucherDTO> getVoucherDTOList() {
		return voucherDTOList;
	}

	public void setVoucherDTOList(List<VoucherDTO> voucherDTOList) {
		this.voucherDTOList = voucherDTOList;
	}

	public String getVoucherGroupId() {
		return voucherGroupId;
	}

	public void setVoucherGroupId(String voucherGroupId) {
		this.voucherGroupId = voucherGroupId;
	}
	
}
