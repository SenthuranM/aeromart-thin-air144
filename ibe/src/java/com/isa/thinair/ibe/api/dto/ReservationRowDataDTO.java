package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ReservationRowDataDTO implements Serializable {

	private static final long serialVersionUID = 7178539175548905374L;

	public static final String HDN_MODIFY_PNR = "hdnModifyPNR";

	public static final String HDN_RFLT_INFO = "hdnRFltInfo";

	public static final String HDN_OFLT_INFO = "hdnOFltInfo";

	public static final String HDN_OLD_SEGINFO = "hdnOldSegInfo";
	
	public static final String PAYMENT_IS_SUCCESS = "hdnPaymentStatus";

	public static final String HDN_IPG_ID = "hdnIPGId";

	/** Payment currency code */
	public static final String HDN_REQ_PAYMENT_CURRENCY = "hdnPayCurrency";

	private Map<Object, Object> dataMap = new HashMap<Object, Object>();

	public Object get(Object key) {
		return dataMap.get(key);
	}

	public Object put(Object key, Object value) {
		return dataMap.put(key, value);
	}
}
