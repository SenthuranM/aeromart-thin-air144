package com.isa.thinair.ibe.api.dto;

import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

public class SystemDefaultParamDTO {

	private String nonsecureIBEUrl;

	private String secureIBEUrl;

	private String currentDate;

	private String flightSeachMaxAdult;

	private String flightSearchDeptVariance;

	private String flightSearchDeptVarianceDifference;

	private String hub;

	private String baseCurrency;

	private String adultCode;

	private String childCode;

	private String infantCode;

	private String insuranceCharged;

	private String depDateDiffrence;

	private String retDateDiffrence;

	private int infantCutOffAgeInYears;

	private int adultCutOffAgeInYears;

	private int childCutOffAgeInYears;

	private String systemDecimal;

	private String faqURL;

	private String kioskHomePageURL;

	private String airLineURL;

	private String classOfService;

	private String carrierName;

	private boolean isInfantAgeMandatoryIbe;

	private boolean isChildAgeMandatoryIbe;

	private boolean isAdultAgeMandatoryIbe;

	private boolean loyaltyEnable;

	private String duplicateNameCheckEnable;

	private boolean isCreditCardPaymentsEnabled;

	private boolean isReservationOnholdEnabled;

	private boolean isCOSSelectionEnabled;

	private boolean isShowAlertsInIBEEnabled;

	private String acclaeroClientIdentifier;

	private String showOperatingCarrierLegend;

	private String sysLanguage;

	private boolean kioskAccessPoint = false;

	private String calendarViews;

	private boolean calendarGradianView;

	private boolean modSegBalAvalSearch;

	private String mealDisVersion;

	private String seatMapDisVersion;

	private String calendarType;

	private boolean sessionTimeOutDisplay;

	private String minReturnTransitionTime;

	private boolean imageCapchaDisAvalSearch;

	private boolean customerRegiterConfirmation;

	private boolean dynamicOndListEnabled = false;

	private boolean ibeDesignVersionAvail;

	private String defaultCarrierCode;

	private boolean isAutoSeatAssignmentEnabled;

	private String autoSeatAssignmentStartingRowNumber;
	private boolean allowModifyBookingsToLowPriorityCOS;

	private boolean promoCodeEnabled;

	private boolean isCookieChargesApplicable;

	private int noOfCookieExpiryDays;

	private boolean userDefineTransitTimeEnabled;

	private boolean isExitPopupEnable;
	
	private int exitPopupDisplayTimeGap;
	
	private int exitPopupEnableStep;
	
	private Map<String, String> carrierUrlsIBE;
	
	private int allowedMaxNoONDMulticitySearch;
	
	private boolean saveCookieEnable;
	
	private boolean humanVerificationEnabled;

	private boolean addMobileAreaCodePrefix;
	
	private boolean isFQTVApplicableForAllSegments;

	private boolean skipSegmentCodeInLabelsForOtherLanguages;
	
	private boolean skipPersianDateChange;

	private boolean isDisplayNotificationForLastNameInIBE;
	
	private boolean isEnableStopOverInIBE = false;
	
	private boolean isAllowSameFlightModificationInIBE = false;
	
	private String clientCallCenterURL;

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getFlightSeachMaxAdult() {
		return flightSeachMaxAdult;
	}

	public void setFlightSeachMaxAdult(String flightSeachMaxAdult) {
		this.flightSeachMaxAdult = flightSeachMaxAdult;
	}

	public String getFlightSearchDeptVariance() {
		return flightSearchDeptVariance;
	}

	public void setFlightSearchDeptVariance(String flightSearchDeptVariance) {
		this.flightSearchDeptVariance = flightSearchDeptVariance;
	}

	public String getFlightSearchDeptVarianceDifference() {
		return flightSearchDeptVarianceDifference;
	}

	public void setFlightSearchDeptVarianceDifference(String flightSearchDeptVarianceDifference) {
		this.flightSearchDeptVarianceDifference = flightSearchDeptVarianceDifference;
	}

	public String getHub() {
		return hub;
	}

	public void setHub(String hub) {
		this.hub = hub;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getAdultCode() {
		return adultCode;
	}

	public void setAdultCode(String adultCode) {
		this.adultCode = adultCode;
	}

	public String getChildCode() {
		return childCode;
	}

	public void setChildCode(String childCode) {
		this.childCode = childCode;
	}

	public String getInfantCode() {
		return infantCode;
	}

	public void setInfantCode(String infantCode) {
		this.infantCode = infantCode;
	}

	public String getInsuranceCharged() {
		return insuranceCharged;
	}

	public void setInsuranceCharged(String insuranceCharged) {
		this.insuranceCharged = insuranceCharged;
	}

	public int getInfantCutOffAgeInYears() {
		return infantCutOffAgeInYears;
	}

	public void setInfantCutOffAgeInYears(int infantCutOffAgeInYears) {
		this.infantCutOffAgeInYears = infantCutOffAgeInYears;
	}

	public int getAdultCutOffAgeInYears() {
		return adultCutOffAgeInYears;
	}

	public void setAdultCutOffAgeInYears(int adultCutOffAgeInYears) {
		this.adultCutOffAgeInYears = adultCutOffAgeInYears;
	}

	public int getChildCutOffAgeInYears() {
		return childCutOffAgeInYears;
	}

	public void setChildCutOffAgeInYears(int childCutOffAgeInYears) {
		this.childCutOffAgeInYears = childCutOffAgeInYears;
	}

	public String getSystemDecimal() {
		return systemDecimal;
	}

	public void setSystemDecimal(String systemDecimal) {
		this.systemDecimal = systemDecimal;
	}

	public String getFaqURL() {
		return faqURL;
	}

	public void setFaqURL(String faqURL) {
		this.faqURL = faqURL;
	}
	
	public String getKioskHomePageURL() {
		return kioskHomePageURL;
	}

	public void setKioskHomePageURL(String kioskHomePageURL) {
		this.kioskHomePageURL = kioskHomePageURL;
	}

	public String getAirLineURL() {
		return airLineURL;
	}

	public void setAirLineURL(String airLineURL) {
		this.airLineURL = airLineURL;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public boolean isLoyaltyEnable() {
		return loyaltyEnable;
	}

	public void setLoyaltyEnable(boolean loyaltyEnable) {
		this.loyaltyEnable = loyaltyEnable;
	}

	public String getDuplicateNameCheckEnable() {
		return duplicateNameCheckEnable;
	}

	public void setDuplicateNameCheckEnable(String duplicateNameCheckEnable) {
		this.duplicateNameCheckEnable = duplicateNameCheckEnable;
	}

	@JSON(serialize = false)
	public boolean isInfantAgeMandatoryIbe() {
		return isInfantAgeMandatoryIbe;
	}

	public boolean getIsInfantAgeMandatoryIbe() {
		return isInfantAgeMandatoryIbe;
	}

	public void setInfantAgeMandatoryIbe(boolean isInfantAgeMandatoryIbe) {
		this.isInfantAgeMandatoryIbe = isInfantAgeMandatoryIbe;
	}

	@JSON(serialize = false)
	public boolean isChildAgeMandatoryIbe() {
		return isChildAgeMandatoryIbe;
	}

	public boolean getIsChildAgeMandatoryIbe() {
		return isChildAgeMandatoryIbe;
	}

	public void setChildAgeMandatoryIbe(boolean isChildAgeMandatoryIbe) {
		this.isChildAgeMandatoryIbe = isChildAgeMandatoryIbe;
	}

	@JSON(serialize = false)
	public boolean isAdultAgeMandatoryIbe() {
		return isAdultAgeMandatoryIbe;
	}

	public boolean getIsAdultAgeMandatoryIbe() {
		return isAdultAgeMandatoryIbe;
	}

	public void setAdultAgeMandatoryIbe(boolean isAdultAgeMandatoryIbe) {
		this.isAdultAgeMandatoryIbe = isAdultAgeMandatoryIbe;
	}

	@JSON(serialize = false)
	public boolean isCreditCardPaymentsEnabled() {
		return isCreditCardPaymentsEnabled;
	}

	public boolean getIsCreditCardPaymentsEnabled() {
		return isCreditCardPaymentsEnabled;
	}

	public void setCreditCardPaymentsEnabled(boolean isCreditCardPaymentsEnabled) {
		this.isCreditCardPaymentsEnabled = isCreditCardPaymentsEnabled;
	}

	@JSON(serialize = false)
	public boolean isReservationOnholdEnabled() {
		return isReservationOnholdEnabled;
	}

	public boolean getIsReservationOnholdEnabled() {
		return isReservationOnholdEnabled;
	}

	public void setReservationOnholdEnabled(boolean isReservationOnholdEnabled) {
		this.isReservationOnholdEnabled = isReservationOnholdEnabled;
	}

	@JSON(serialize = false)
	public boolean isCOSSelectionEnabled() {
		return isCOSSelectionEnabled;
	}

	public boolean getIsCOSSelectionEnabled() {
		return isCOSSelectionEnabled;
	}

	public void setCOSSelectionEnabled(boolean isCOSSelectionEnabled) {
		this.isCOSSelectionEnabled = isCOSSelectionEnabled;
	}

	@JSON(serialize = false)
	public boolean isShowAlertsInIBEEnabled() {
		return isShowAlertsInIBEEnabled;
	}

	public boolean getIsShowAlertsInIBEEnabled() {
		return isShowAlertsInIBEEnabled;
	}

	public void setShowAlertsInIBEEnabled(boolean isShowAlertsInIBEEnabled) {
		this.isShowAlertsInIBEEnabled = isShowAlertsInIBEEnabled;
	}

	public String getAcclaeroClientIdentifier() {
		return acclaeroClientIdentifier;
	}

	public void setAcclaeroClientIdentifier(String acclaeroClientIdentifier) {
		this.acclaeroClientIdentifier = acclaeroClientIdentifier;
	}

	public String getShowOperatingCarrierLegend() {
		return showOperatingCarrierLegend;
	}

	public void setShowOperatingCarrierLegend(String showOperatingCarrierLegend) {
		this.showOperatingCarrierLegend = showOperatingCarrierLegend;
	}

	public String getNonsecureIBEUrl() {
		return nonsecureIBEUrl;
	}

	public void setNonsecureIBEUrl(String nonsecureIBEUrl) {
		this.nonsecureIBEUrl = nonsecureIBEUrl;
	}

	public String getSecureIBEUrl() {
		return secureIBEUrl;
	}

	public void setSecureIBEUrl(String secureIBEUrl) {
		this.secureIBEUrl = secureIBEUrl;
	}

	public String getSysLanguage() {
		return sysLanguage;
	}

	public void setSysLanguage(String sysLanguage) {
		this.sysLanguage = sysLanguage;
	}

	public String getDepDateDiffrence() {
		return depDateDiffrence;
	}

	public void setDepDateDiffrence(String depDateDiffrence) {
		this.depDateDiffrence = depDateDiffrence;
	}

	public String getRetDateDiffrence() {
		return retDateDiffrence;
	}

	public void setRetDateDiffrence(String retDateDiffrence) {
		this.retDateDiffrence = retDateDiffrence;
	}

	public boolean isKioskAccessPoint() {
		return kioskAccessPoint;
	}

	public void setKioskAccessPoint(boolean kioskAccessPoint) {
		this.kioskAccessPoint = kioskAccessPoint;
	}

	public String getCalendarViews() {
		return calendarViews;
	}

	public void setCalendarViews(String calendarViews) {
		this.calendarViews = calendarViews;
	}

	public boolean isCalendarGradianView() {
		return calendarGradianView;
	}

	public void setCalendarGradianView(boolean calendarGradianView) {
		this.calendarGradianView = calendarGradianView;
	}

	public boolean isModSegBalAvalSearch() {
		return modSegBalAvalSearch;
	}

	public void setModSegBalAvalSearch(boolean modSegBalAvalSearch) {
		this.modSegBalAvalSearch = modSegBalAvalSearch;
	}

	public String getMealDisVersion() {
		return mealDisVersion;
	}

	public void setMealDisVersion(String mealDisVersion) {
		this.mealDisVersion = mealDisVersion;
	}

	public String getSeatMapDisVersion() {
		return seatMapDisVersion;
	}

	public void setSeatMapDisVersion(String seatMapDisVersion) {
		this.seatMapDisVersion = seatMapDisVersion;
	}

	public String getCalendarType() {
		return calendarType;
	}

	public void setCalendarType(String calendarType) {
		this.calendarType = calendarType;
	}

	public boolean isSessionTimeOutDisplay() {
		return sessionTimeOutDisplay;
	}

	public void setSessionTimeOutDisplay(boolean sessionTimeOutDisplay) {
		this.sessionTimeOutDisplay = sessionTimeOutDisplay;
	}

	public boolean isImageCapchaDisAvalSearch() {
		return imageCapchaDisAvalSearch;
	}

	public void setImageCapchaDisAvalSearch(boolean imageCapchaDisAvalSearch) {
		this.imageCapchaDisAvalSearch = imageCapchaDisAvalSearch;
	}

	public String getMinReturnTransitionTime() {
		return minReturnTransitionTime;
	}

	public void setMinReturnTransitionTime(String minReturnTransitionTime) {
		this.minReturnTransitionTime = minReturnTransitionTime;
	}

	public void setCustomerRegiterConfirmation(boolean customerRegiterConfirmation) {
		this.customerRegiterConfirmation = customerRegiterConfirmation;
	}

	public boolean isCustomerRegiterConfirmation() {
		return customerRegiterConfirmation;
	}

	public boolean isDynamicOndListEnabled() {
		return dynamicOndListEnabled;
	}

	public void setDynamicOndListEnabled(boolean dynamicOndListEnabled) {
		this.dynamicOndListEnabled = dynamicOndListEnabled;
	}

	public String getDefaultCarrierCode() {
		return defaultCarrierCode;
	}

	public void setDefaultCarrierCode(String defaultCarrierCode) {
		this.defaultCarrierCode = defaultCarrierCode;
	}

	public void setIbeDesignVersionAvail(boolean ibeDesignVersionAvail) {
		this.ibeDesignVersionAvail = ibeDesignVersionAvail;
	}

	public boolean isIbeDesignVersionAvail() {
		return ibeDesignVersionAvail;
	}

	public boolean isAutoSeatAssignmentEnabled() {
		return isAutoSeatAssignmentEnabled;
	}

	public void setAutoSeatAssignmentEnabled(boolean isAutoSeatAssignmentEnabled) {
		this.isAutoSeatAssignmentEnabled = isAutoSeatAssignmentEnabled;
	}

	public boolean isAllowModifyBookingsToLowPriorityCOS() {
		return allowModifyBookingsToLowPriorityCOS;
	}

	public void setAllowModifyBookingsToLowPriorityCOS(boolean allowModifyBookingsToLowPriorityCOS) {
		this.allowModifyBookingsToLowPriorityCOS = allowModifyBookingsToLowPriorityCOS;
	}

	public boolean isPromoCodeEnabled() {
		return promoCodeEnabled;
	}

	public void setPromoCodeEnabled(boolean promoCodeEnabled) {
		this.promoCodeEnabled = promoCodeEnabled;
	}

	public String getAutoSeatAssignmentStartingRowNumber() {
		return autoSeatAssignmentStartingRowNumber;
	}

	public void setAutoSeatAssignmentStartingRowNumber(String autoSeatAssignmentStartingRowNumber) {
		this.autoSeatAssignmentStartingRowNumber = autoSeatAssignmentStartingRowNumber;
	}

	public boolean isCookieChargesApplicable() {
		return isCookieChargesApplicable;
	}

	public void setCookieChargesApplicable(boolean isCookieChargesApplicable) {
		this.isCookieChargesApplicable = isCookieChargesApplicable;
	}

	public int getNoOfCookieExpiryDays() {
		return noOfCookieExpiryDays;
	}

	public void setNoOfCookieExpiryDays(int noOfCookieExpiryDays) {
		this.noOfCookieExpiryDays = noOfCookieExpiryDays;
	}

	public boolean isExitPopupEnable() {
		return isExitPopupEnable;
	}

	public void setExitPopupEnable(boolean isExitPopupEnable) {
		this.isExitPopupEnable = isExitPopupEnable;
	}

	public boolean isUserDefineTransitTimeEnabled() {
		return userDefineTransitTimeEnabled;
	}

	public void setUserDefineTransitTimeEnabled(boolean userDefineTransitTimeEnabled) {
		this.userDefineTransitTimeEnabled = userDefineTransitTimeEnabled;
	}

	public int getExitPopupDisplayTimeGap() {
		return exitPopupDisplayTimeGap;
	}

	public void setExitPopupDisplayTimeGap(int exitPopupDisplayTimeGap) {
		this.exitPopupDisplayTimeGap = exitPopupDisplayTimeGap;
	}

	public int getExitPopupEnableStep() {
		return exitPopupEnableStep;
	}

	public void setExitPopupEnableStep(int exitPopupEnableStep) {
		this.exitPopupEnableStep = exitPopupEnableStep;
	}

	public Map<String, String> getCarrierUrlsIBE() {
		return carrierUrlsIBE;
	}

	public void setCarrierUrlsIBE(Map<String, String> carrierUrlsIBE) {
		this.carrierUrlsIBE = carrierUrlsIBE;
	}

	public int getAllowedMaxNoONDMulticitySearch() {
		return allowedMaxNoONDMulticitySearch;
	}

	public void setAllowedMaxNoONDMulticitySearch(int allowedMaxNoONDMulticitySearch) {
		this.allowedMaxNoONDMulticitySearch = allowedMaxNoONDMulticitySearch;
	}

	public boolean isSaveCookieEnable() {
		return saveCookieEnable;
	}

	public void setSaveCookieEnable(boolean saveCookieEnable) {
		this.saveCookieEnable = saveCookieEnable;
	}

	public boolean isAddMobileAreaCodePrefix() {
		return addMobileAreaCodePrefix;
	}

	public void setAddMobileAreaCodePrefix(boolean addMobileAreaCodePrefix) {
		this.addMobileAreaCodePrefix = addMobileAreaCodePrefix;
	}

	public boolean isFQTVApplicableForAllSegments() {
		return isFQTVApplicableForAllSegments;
	}

	public void setFQTVApplicableForAllSegments(boolean isFQTVApplicableForAllSegments) {
		this.isFQTVApplicableForAllSegments = isFQTVApplicableForAllSegments;
	}

	public boolean isHumanVerificationEnabled() {
		return humanVerificationEnabled;
	}

	public void setHumanVerificationEnabled(boolean humanVerificationEnabled) {
		this.humanVerificationEnabled = humanVerificationEnabled;
	}
	
	public boolean isSkipSegmentCodeInLabelsForOtherLanguages() {
		return skipSegmentCodeInLabelsForOtherLanguages;
	}

	public void setSkipSegmentCodeInLabelsForOtherLanguages(boolean skipSegmentCodeInLabelsForOtherLanguages) {
		this.skipSegmentCodeInLabelsForOtherLanguages = skipSegmentCodeInLabelsForOtherLanguages;
	}

	public boolean isSkipPersianDateChange() {
		return skipPersianDateChange;
	}

	public void setSkipPersianDateChange(boolean skipPersianDateChange) {
		this.skipPersianDateChange = skipPersianDateChange;
	}

	public boolean isDisplayNotificationForLastNameInIBE() {
		return isDisplayNotificationForLastNameInIBE;
	}

	public void setDisplayNotificationForLastNameInIBE(boolean isDisplayNotificationForLastNameInIBE) {
		this.isDisplayNotificationForLastNameInIBE = isDisplayNotificationForLastNameInIBE;
	}

	public boolean isEnableStopOverInIBE() {
		return isEnableStopOverInIBE;
	}

	public void setEnableStopOverInIBE(boolean isEnableStopOverInIBE) {
		this.isEnableStopOverInIBE = isEnableStopOverInIBE;
	}

	public boolean isAllowSameFlightModificationInIBE() {
		return isAllowSameFlightModificationInIBE;
	}

	public void setAllowSameFlightModificationInIBE(boolean isAllowSameFlightModificationInIBE) {
		this.isAllowSameFlightModificationInIBE = isAllowSameFlightModificationInIBE;
	}

	public String getClientCallCenterURL() {
		return clientCallCenterURL;
	}

	public void setClientCallCenterURL(String clientCallCenterURL) {
		this.clientCallCenterURL = clientCallCenterURL;
	}
	
}
