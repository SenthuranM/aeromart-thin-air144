package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

public class ContactInfoDTO implements Serializable {

	private static final long serialVersionUID = 10023659894L;

	private String title;

	private String firstName;

	private String lastName;
	
	private String dateOfBirth;

	private String nationality;

	private String addresline;

	private String addresStreet;

	private String state;

	private String city;

	private String country;
	
	private String countryName;

	private String zipCode;

	private String mCountry = "";

	private String mArea = "";

	private String mNumber = "";

	private String lCountry = "";

	private String lArea = "";

	private String lNumber = "";

	private String fCountry = "";

	private String fArea = "";

	private String fNumber = "";

	private String emailAddress = "";

	private String emailLanguage = "";

	private Integer customerId;

	private boolean updateCustomerProf = false;

	private String preferredLangauge;

	/*
	 * Emergency Contact Related Attributes
	 */
	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private String emgnLCountry;

	private String emgnLArea;

	private String emgnLNumber;

	private String emgnEmail;

	public String getTitle() {
		return title;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getNationality() {
		return nationality;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getAddresline() {
		return addresline;
	}

	public void setAddresline(String addresline) {
		this.addresline = addresline;
	}

	public String getAddresStreet() {
		return addresStreet;
	}

	public void setAddresStreet(String addresStreet) {
		this.addresStreet = addresStreet;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getmCountry() {
		return mCountry;
	}

	public void setmCountry(String mCountry) {
		this.mCountry = mCountry;
	}

	public String getmArea() {
		return mArea;
	}

	public void setmArea(String mArea) {
		this.mArea = mArea;
	}

	public String getmNumber() {
		return mNumber;
	}

	public void setmNumber(String mNumber) {
		this.mNumber = mNumber;
	}

	public String getlCountry() {
		return lCountry;
	}

	public void setlCountry(String lCountry) {
		this.lCountry = lCountry;
	}

	public String getlArea() {
		return lArea;
	}

	public void setlArea(String lArea) {
		this.lArea = lArea;
	}

	public String getlNumber() {
		return lNumber;
	}

	public void setlNumber(String lNumber) {
		this.lNumber = lNumber;
	}

	public String getfCountry() {
		return fCountry;
	}

	public void setfCountry(String fCountry) {
		this.fCountry = fCountry;
	}

	public String getfArea() {
		return fArea;
	}

	public void setfArea(String fArea) {
		this.fArea = fArea;
	}

	public String getfNumber() {
		return fNumber;
	}

	public void setfNumber(String fNumber) {
		this.fNumber = fNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailLanguage() {
		return emailLanguage;
	}

	public void setEmailLanguage(String emailLanguage) {
		this.emailLanguage = emailLanguage;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the preferredLangauge
	 */
	public String getPreferredLangauge() {
		return preferredLangauge;
	}

	/**
	 * @param preferredLangauge
	 *            the preferredLangauge to set
	 */
	public void setPreferredLangauge(String preferredLangauge) {
		this.preferredLangauge = preferredLangauge;
	}

	/**
	 * @return the emgnTitle
	 */
	public String getEmgnTitle() {
		return emgnTitle;
	}

	/**
	 * @param emgnTitle
	 *            the emgnTitle to set
	 */
	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	/**
	 * @return the emgnFirstName
	 */
	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	/**
	 * @param emgnFirstName
	 *            the emgnFirstName to set
	 */
	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	/**
	 * @return the emgnLastName
	 */
	public String getEmgnLastName() {
		return emgnLastName;
	}

	/**
	 * @param emgnLastName
	 *            the emgnLastName to set
	 */
	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	/**
	 * @return the emgnLCountry
	 */
	public String getEmgnLCountry() {
		return emgnLCountry;
	}

	/**
	 * @param emgnLCountry
	 *            the emgnLCountry to set
	 */
	public void setEmgnLCountry(String emgnLCountry) {
		this.emgnLCountry = emgnLCountry;
	}

	/**
	 * @return the emgnLArea
	 */
	public String getEmgnLArea() {
		return emgnLArea;
	}

	/**
	 * @param emgnLArea
	 *            the emgnLArea to set
	 */
	public void setEmgnLArea(String emgnLArea) {
		this.emgnLArea = emgnLArea;
	}

	/**
	 * @return the emgnLNumber
	 */
	public String getEmgnLNumber() {
		return emgnLNumber;
	}

	/**
	 * @param emgnLNumber
	 *            the emgnLNumber to set
	 */
	public void setEmgnLNumber(String emgnLNumber) {
		this.emgnLNumber = emgnLNumber;
	}

	/**
	 * @return the emgnEmail
	 */
	public String getEmgnEmail() {
		return emgnEmail;
	}

	/**
	 * @param emgnEmail
	 *            the emgnEmail to set
	 */
	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}

	/**
	 * @return the updateCustomerProf
	 */
	public boolean isUpdateCustomerProf() {
		return updateCustomerProf;
	}

	/**
	 * @param updateCustomerProf
	 *            the updateCustomerProf to set
	 */
	public void setUpdateCustomerProf(boolean updateCustomerProf) {
		this.updateCustomerProf = updateCustomerProf;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}
