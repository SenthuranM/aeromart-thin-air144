package com.isa.thinair.ibe.api.dto;

public class CustomerLoyaltyCreditDTO {

	private String dateOfExpiry;

	private String dateOfEarn;

	private String credit;

	public String getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(String dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getDateOfEarn() {
		return dateOfEarn;
	}

	public void setDateOfEarn(String dateOfEarn) {
		this.dateOfEarn = dateOfEarn;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

}
