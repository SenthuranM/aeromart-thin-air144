package com.isa.thinair.ibe.api.dto;

public class PaxValidationTO {
	private int adultAgeCutOverYears;
	private int childAgeCutOverYears;
	private int infantAgeCutOverYears;
	private boolean phoneValidation = false;
	private int infantAgeLowerDays;
	private int infantAgeLowerMonths;
	private boolean adultsPassportUniqueOnly;

	public int getAdultAgeCutOverYears() {
		return adultAgeCutOverYears;
	}

	public void setAdultAgeCutOverYears(int adultAgeCutOverYears) {
		this.adultAgeCutOverYears = adultAgeCutOverYears;
	}

	public int getChildAgeCutOverYears() {
		return childAgeCutOverYears;
	}

	public void setChildAgeCutOverYears(int childAgeCutOverYears) {
		this.childAgeCutOverYears = childAgeCutOverYears;
	}

	public int getInfantAgeCutOverYears() {
		return infantAgeCutOverYears;
	}

	public void setInfantAgeCutOverYears(int infantAgeCutOverYears) {
		this.infantAgeCutOverYears = infantAgeCutOverYears;
	}

	public boolean isPhoneValidation() {
		return phoneValidation;
	}

	public void setPhoneValidation(boolean phoneValidation) {
		this.phoneValidation = phoneValidation;
	}

	public int getInfantAgeCutLowerBoundaryDays() {
		return infantAgeLowerDays;
	}

	public void setInfantAgeCutLowerBoundaryDays(int infantAgeLowerDays) {
		this.infantAgeLowerDays = infantAgeLowerDays;
	}

	public int getInfantAgeCutLowerBoundaryMonths() {
		return infantAgeLowerMonths;
	}

	public void setInfantAgeCutLowerBoundaryMonths(int infantAgeLowerMonths) {
		this.infantAgeLowerMonths = infantAgeLowerMonths;
	}

	public void setAdultsPassportUniqueOnly(boolean adultsPassportUniqueOnly) {
		this.adultsPassportUniqueOnly = adultsPassportUniqueOnly;
	}

	public boolean isAdultsPassportUniqueOnly() {
		return adultsPassportUniqueOnly;
	}

}
