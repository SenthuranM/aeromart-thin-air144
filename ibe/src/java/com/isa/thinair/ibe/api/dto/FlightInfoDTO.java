package com.isa.thinair.ibe.api.dto;

import java.util.ArrayList;
import java.util.Collection;

public class FlightInfoDTO {

	private Collection<SegInfoDTO> segInfoList = new ArrayList<SegInfoDTO>();

	private boolean selected = false;

	public Collection<SegInfoDTO> getSegInfoList() {
		return segInfoList;
	}

	public void setSegInfoList(Collection<SegInfoDTO> segInfoList) {
		this.segInfoList = segInfoList;
	}

	public void addSegmentInfo(SegInfoDTO segInfoDTO) {
		if (this.segInfoList == null)
			this.segInfoList = new ArrayList<SegInfoDTO>();
		this.segInfoList.add(segInfoDTO);
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
