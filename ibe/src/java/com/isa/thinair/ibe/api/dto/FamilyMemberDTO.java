/**
 * 
 */
package com.isa.thinair.ibe.api.dto;

/**
 * @author suneth
 *
 */
public class FamilyMemberDTO {
	
	private Integer familyMemberId;
	
	private int customerId;
	
	private String firstName;
	
	private String lastName;
	
	private String relationshipId;
	
	private int nationalityCode;
	
	private String dateOfBirth;

	public Integer getFamilyMemberId() {
		return familyMemberId;
	}

	public void setFamilyMemberId(Integer familyMemberId) {
		this.familyMemberId = familyMemberId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRelationshipId() {
		return relationshipId;
	}

	public void setRelationshipId(String relationshipId) {
		this.relationshipId = relationshipId;
	}

	public int getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
}
