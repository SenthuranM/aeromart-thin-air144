package com.isa.thinair.ibe.api.dto.v2;

public class FareRuleDisplayTO {
	private String ondCode;
	private Integer ondSequence;
	private String bookingClass;
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public Integer getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(Integer ondSequence) {
		this.ondSequence = ondSequence;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

}
