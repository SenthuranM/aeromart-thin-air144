package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.promotion.api.to.PromotionRequestTo;

/**
 * 
 * Used to keep the promotion request data in the session since the payment
 * 
 */
public class IBEPromotionPaymentDTO implements Serializable {

	private static final long serialVersionUID = -8715583157304191696L;

	private List<PromotionRequestTo> colPromotionRequestTo;

	private String totalPayment;

	public List<PromotionRequestTo> getColPromotionRequestTo() {
		return colPromotionRequestTo;
	}

	public void setColPromotionRequestTo(List<PromotionRequestTo> colPromotionRequestTo) {
		this.colPromotionRequestTo = colPromotionRequestTo;
	}

	public String getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(String totalPayment) {
		this.totalPayment = totalPayment;
	}

}
