package com.isa.thinair.ibe.api.dto.v2;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
public class SelectedPaymentMethodDTO {

	private int cardType;

	private int gatewayID;

	private String brokerType;

	private String loyaltyOption;

	private String loyaltyCredit;

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public int getGatewayID() {
		return gatewayID;
	}

	public void setGatewayID(int gatewayID) {
		this.gatewayID = gatewayID;
	}

	public String getBrokerType() {
		return brokerType;
	}

	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	public String getLoyaltyOption() {
		return loyaltyOption;
	}

	public void setLoyaltyOption(String loyaltyOption) {
		this.loyaltyOption = loyaltyOption;
	}

	public String getLoyaltyCredit() {
		return loyaltyCredit;
	}

	public void setLoyaltyCredit(String loyaltyCredit) {
		this.loyaltyCredit = loyaltyCredit;
	}
}
