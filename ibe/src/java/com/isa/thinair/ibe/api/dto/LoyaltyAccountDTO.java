package com.isa.thinair.ibe.api.dto;

public class LoyaltyAccountDTO {

	private String loyaltyAccountNo;

	private String email;

	private String mobile;

	private String dateOfBirth;

	private String countryCode;

	private int nationalityCode;

	private String city;

	public String getLoyaltyAccountNo() {
		return loyaltyAccountNo;
	}

	public void setLoyaltyAccountNo(String loyaltyAccountNo) {
		this.loyaltyAccountNo = loyaltyAccountNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
