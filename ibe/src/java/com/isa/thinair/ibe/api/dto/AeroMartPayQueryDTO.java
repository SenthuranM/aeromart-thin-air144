package com.isa.thinair.ibe.api.dto;

public class AeroMartPayQueryDTO {

	private String status;
	private String paymentCurrencyCode;
	private String paymentCurrencyAmount;
	private String referenceID;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getPaymentCurrencyAmount() {
		return paymentCurrencyAmount;
	}

	public void setPaymentCurrencyAmount(String paymentCurrencyAmount) {
		this.paymentCurrencyAmount = paymentCurrencyAmount;
	}

}
