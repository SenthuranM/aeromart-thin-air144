package com.isa.thinair.ibe.api.dto;

/**
 * 
 * Generic Class for Display Dynamic data in front end
 * 
 * @author pkarunanayake
 * 
 */
public class DisplayUtilDTO {

	private String labelName;

	private String value;

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
