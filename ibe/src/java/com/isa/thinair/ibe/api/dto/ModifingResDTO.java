package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.dto.FlightInfoTO;

public class ModifingResDTO {

	private Integer modifiedOndFareId = null;

	private Integer modifiedOndFareType = null;

	private BigDecimal modifiedOndPaxFareAmount = null;
	// TODO : Review
	private Date firstDepartureDateTimeZulu = null;
	// TODO : Review
	private Date lastArrivalDateTimeZulu = null;
	// TODO : Review
	private List<FlightInfoTO> flightInfoList;

	public Integer getModifiedOndFareId() {
		return modifiedOndFareId;
	}

	public void setModifiedOndFareId(Integer modifiedOndFareId) {
		this.modifiedOndFareId = modifiedOndFareId;
	}

	public Integer getModifiedOndFareType() {
		return modifiedOndFareType;
	}

	public void setModifiedOndFareType(Integer modifiedOndFareType) {
		this.modifiedOndFareType = modifiedOndFareType;
	}

	public BigDecimal getModifiedOndPaxFareAmount() {
		return modifiedOndPaxFareAmount;
	}

	public void setModifiedOndPaxFareAmount(BigDecimal modifiedOndPaxFareAmount) {
		this.modifiedOndPaxFareAmount = modifiedOndPaxFareAmount;
	}

	public Date getFirstDepartureDateTimeZulu() {
		return firstDepartureDateTimeZulu;
	}

	public void setFirstDepartureDateTimeZulu(Date firstDepartureDateTimeZulu) {
		this.firstDepartureDateTimeZulu = firstDepartureDateTimeZulu;
	}

	public Date getLastArrivalDateTimeZulu() {
		return lastArrivalDateTimeZulu;
	}

	public void setLastArrivalDateTimeZulu(Date lastArrivalDateTimeZulu) {
		this.lastArrivalDateTimeZulu = lastArrivalDateTimeZulu;
	}

	public List<FlightInfoTO> getFlightInfoList() {
		return flightInfoList;
	}

	public void setFlightInfoList(List<FlightInfoTO> flightInfoList) {
		this.flightInfoList = flightInfoList;
	}

}
