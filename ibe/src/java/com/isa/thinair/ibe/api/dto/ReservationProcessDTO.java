package com.isa.thinair.ibe.api.dto;

public class ReservationProcessDTO {

	private boolean fromCNX = false;

	private boolean seatMapEnable = false;

	private boolean mealEnable = false;

	private boolean baggageEnable = false;

	private boolean inflightServiceEnable = false;

	private boolean airportServiceEnable = false;

	private boolean airportTransferEnable = false;

	private boolean insuranceEnable = false;

	private boolean showMeal = false;

	private boolean showBaggage = false;

	private boolean showSeat = false;

	private boolean showInflightService = false;

	private boolean showAirportService = false;

	private boolean showAirportTransfer = false;

	private boolean showInsurance = false;

	private boolean loyaltyAccountExist = false;

	private boolean cancelSegment = false;

	private boolean cancelReservation = false;
	/* segment modification IBE */
	private boolean allowModifyDate = false;

	private boolean allowModifyRoute = false;

	private boolean addGroundSegment = false;

	private boolean makePayment = false;

	private boolean nameChangeAllowed = false;

	private boolean lmsEnabled = false;

	private boolean allowForceConfirmModify;

	public boolean isMakePayment() {
		return makePayment;
	}

	public void setMakePayment(boolean makePayment) {
		this.makePayment = makePayment;
	}

	public boolean isFromCNX() {
		return fromCNX;
	}

	public void setFromCNX(boolean fromCNX) {
		this.fromCNX = fromCNX;
	}

	public boolean isSeatMapEnable() {
		return seatMapEnable;
	}

	public void setSeatMapEnable(boolean seatMapEnable) {
		this.seatMapEnable = seatMapEnable;
	}

	public boolean isMealEnable() {
		return mealEnable;
	}

	public void setMealEnable(boolean mealEnable) {
		this.mealEnable = mealEnable;
	}

	public boolean isInflightServiceEnable() {
		return inflightServiceEnable;
	}

	public void setInflightServiceEnable(boolean inflightServiceEnable) {
		this.inflightServiceEnable = inflightServiceEnable;
	}

	/**
	 * @return the airportServiceEnable
	 */
	public boolean isAirportServiceEnable() {
		return airportServiceEnable;
	}

	/**
	 * @param airportServiceEnable
	 *            the airportServiceEnable to set
	 */
	public void setAirportServiceEnable(boolean airportServiceEnable) {
		this.airportServiceEnable = airportServiceEnable;
	}

	public boolean isInsuranceEnable() {
		return insuranceEnable;
	}

	public void setInsuranceEnable(boolean insuranceEnable) {
		this.insuranceEnable = insuranceEnable;
	}

	public boolean isLoyaltyAccountExist() {
		return loyaltyAccountExist;
	}

	public void setLoyaltyAccountExist(boolean loyaltyAccountExist) {
		this.loyaltyAccountExist = loyaltyAccountExist;
	}

	public boolean isCancelSegment() {
		return cancelSegment;
	}

	public void setCancelSegment(boolean cancelSegment) {
		this.cancelSegment = cancelSegment;
	}

	public boolean isCancelReservation() {
		return cancelReservation;
	}

	public void setCancelReservation(boolean cancelReservation) {
		this.cancelReservation = cancelReservation;
	}

	public boolean isShowMeal() {
		return showMeal;
	}

	public void setShowMeal(boolean showMeal) {
		this.showMeal = showMeal;
	}

	public boolean isShowSeat() {
		return showSeat;
	}

	public void setShowSeat(boolean showSeat) {
		this.showSeat = showSeat;
	}

	public boolean isShowInflightService() {
		return showInflightService;
	}

	public void setShowInflightService(boolean showInflightService) {
		this.showInflightService = showInflightService;
	}

	/**
	 * @return the showAirportService
	 */
	public boolean isShowAirportService() {
		return showAirportService;
	}

	/**
	 * @param showAirportService
	 *            the showAirportService to set
	 */
	public void setShowAirportService(boolean showAirportService) {
		this.showAirportService = showAirportService;
	}

	public boolean isShowInsurance() {
		return showInsurance;
	}

	public void setShowInsurance(boolean showInsurance) {
		this.showInsurance = showInsurance;
	}

	public boolean isAllowModifyDate() {
		return allowModifyDate;
	}

	public void setAllowModifyDate(boolean allowModifyDate) {
		this.allowModifyDate = allowModifyDate;
	}

	public boolean isAllowModifyRoute() {
		return allowModifyRoute;
	}

	public void setAllowModifyRoute(boolean allowModifyRoute) {
		this.allowModifyRoute = allowModifyRoute;
	}

	public boolean isBaggageEnable() {
		return baggageEnable;
	}

	public void setBaggageEnable(boolean baggageEnable) {
		this.baggageEnable = baggageEnable;
	}

	public boolean isShowBaggage() {
		return showBaggage;
	}

	public void setShowBaggage(boolean showBaggage) {
		this.showBaggage = showBaggage;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public boolean isShowAirportTransfer() {
		return showAirportTransfer;
	}

	public void setShowAirportTransfer(boolean showAirportTransfer) {
		this.showAirportTransfer = showAirportTransfer;
	}

	public boolean isAirportTransferEnable() {
		return airportTransferEnable;
	}

	public void setAirportTransferEnable(boolean airportTransferEnable) {
		this.airportTransferEnable = airportTransferEnable;
	}

	public boolean isNameChangeAllowed() {
		return nameChangeAllowed;
	}

	public void setNameChangeAllowed(boolean nameChangeAllowed) {
		this.nameChangeAllowed = nameChangeAllowed;
	}

	public boolean isLmsEnabled() {
		return lmsEnabled;
	}

	public void setLmsEnabled(boolean lmsEnabled) {
		this.lmsEnabled = lmsEnabled;
	}

	public void setAllowForceConfirmModify(boolean allowForceConfirmModify) {
		this.allowForceConfirmModify = allowForceConfirmModify;
	}

	public boolean isAllowForceConfirmModify() {
		return allowForceConfirmModify;
	}

}
