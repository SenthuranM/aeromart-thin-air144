package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.BookingType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * DTO to keep all the IBE reservation related Information
 * 
 * 
 * @author Thushara
 */
public class IBEReservationInfoDTO implements Serializable {

	private static final long serialVersionUID = 2337654111098L;

	public IBEReservationInfoDTO() {
		this.selectedExtCharges = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		this.pnrPaxCreditMap = new HashMap<String, Map<String, BigDecimal>>();
		this.anciOfferTemplates = new HashMap<String, Map<EXTERNAL_CHARGES, Integer>>();
	}

	/**
	 * VARIABLES USED THROUGHOUT THE FLOW
	 */
	private Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtCharges;

	private String transactionId;

	private String totalPriceWithoutExtChg;

	private String totalFare;

	private String totalTax;

	private String totalTaxSurchages;

	private ReservationBalanceTO lCCClientReservationBalance;

	private String modifyCharge;

	private String creditableAmount;

	private SYSTEM selectedSystem;

	private PriceInfoTO priceInfoTO;

	private boolean fromPaymentPage = false;

	private boolean noPay = false;

	private BigDecimal creditCardFee;

	private String blockedSeatJson;

	private BigDecimal loyaltyCredit;

	private LoyaltyPaymentOption loyaltyPayOption;

	private BookingType bookingType;

	// Modifying Flights Segment Status
	private Collection<ReservationSegmentStatusDTO> segmentStatus;

	// private Map<String, BigDecimal> paxCreditMap;
	private Map<String, Map<String, BigDecimal>> pnrPaxCreditMap;

	private BigDecimal reservationCredit;

	private String outboundExCharge;

	private String inboundExCharge;

	private BigDecimal insuranceCalculatedPremium;

	private boolean isCaptchaValidated;

	private boolean fromPostCardDetails = false;

	private String pnr;

	private int totalSegmentCount;

	private String reservationStatus;

	private String bank3DSecureHTML;

	private boolean allowOnhold;

	private boolean reservationOnholdableOtherValidation;

	private boolean reservationOnholdableCombineValidation;

	private BigDecimal totalTicketPriceInBase;

	private DiscountedFareDetails discountInfo;

	private BigDecimal discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal ibeReturnFareDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private List<String> excludedSegFarePnrSegIds;

	private List<ServiceTaxContainer> applicableServiceTaxes;

	private Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplates;

	private LCCReservationBaggageSummaryTo baggageSummaryTo;

	private List<BundledFareDTO> bundledFareDTOs;

	private List<AncillaryPaxTo> ancillaryPaxTos;

	private boolean applyPenaltyForAnciModification;

	private Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo;

	private boolean hasBalanceToPay;

	private PayByVoucherInfo payByVoucherInfo;

	private String marketingCarrier;
	
	private boolean infantPaymentSeparated = false;

	/**
	 * @return the allowOnhold
	 */
	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	/**
	 * @param allowOnhold
	 *            the allowOnhold to set
	 */
	public void setAllowOnhold(boolean allowOnhold) {
		this.allowOnhold = allowOnhold;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTotalPriceWithoutExtChg() {
		return totalPriceWithoutExtChg;
	}

	public void setTotalPriceWithoutExtChg(String totalPriceWithoutExtChg) {
		this.totalPriceWithoutExtChg = totalPriceWithoutExtChg;
	}

	public String getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}

	public String getTotalTaxSurchages() {
		return totalTaxSurchages;
	}

	public void setTotalTaxSurchages(String totalTaxSurchages) {
		this.totalTaxSurchages = totalTaxSurchages;
	}

	public SYSTEM getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(SYSTEM selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public PriceInfoTO getPriceInfoTO() {
		return priceInfoTO;
	}

	public void setPriceInfoTO(PriceInfoTO priceInfoTO) {
		this.priceInfoTO = priceInfoTO;
	}

	public void addExternalCharge(ExternalChgDTO ccChgDTO) {
		this.selectedExtCharges.put(ccChgDTO.getExternalChargesEnum(), ccChgDTO);
	}

	public void removeExternalCharge(EXTERNAL_CHARGES extChgEnum) {
		this.selectedExtCharges.remove(extChgEnum);
	}

	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getSelectedExternalCharges() {
		return this.selectedExtCharges;
	}

	public boolean isFromPaymentPage() {
		return fromPaymentPage;
	}

	public void setFromPaymentPage(boolean fromPaymentPage) {
		this.fromPaymentPage = fromPaymentPage;
	}

	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	public String getBlockedSeatJson() {
		return blockedSeatJson;
	}

	public void setBlockedSeatJson(String blockedSeatJson) {
		this.blockedSeatJson = blockedSeatJson;
	}

	public void setLoyaltyCredit(BigDecimal loyaltyCredit) {
		this.loyaltyCredit = loyaltyCredit;
	}

	public void setLoyaltyPayOption(LoyaltyPaymentOption loyaltyPayOption) {
		this.loyaltyPayOption = loyaltyPayOption;
	}

	public BigDecimal getLoyaltyCredit() {
		return loyaltyCredit;
	}

	public LoyaltyPaymentOption getLoyaltyPayOption() {
		return loyaltyPayOption;
	}

	public String getModifyCharge() {
		return modifyCharge;
	}

	public void setModifyCharge(String modifyCharge) {
		this.modifyCharge = modifyCharge;
	}

	public String getCreditableAmount() {
		return creditableAmount;
	}

	public void setCreditableAmount(String creditableAmount) {
		this.creditableAmount = creditableAmount;
	}

	public Collection<ReservationSegmentStatusDTO> getSegmentStatus() {
		return segmentStatus;
	}

	public void setSegmentStatus(Collection<ReservationSegmentStatusDTO> segmentStatus) {
		this.segmentStatus = segmentStatus;
	}

	public ReservationBalanceTO getlCCClientReservationBalance() {
		return lCCClientReservationBalance;
	}

	public void setlCCClientReservationBalance(ReservationBalanceTO lCCClientReservationBalance) {
		this.lCCClientReservationBalance = lCCClientReservationBalance;
	}

	/*
	 * public void setPaxCreditMap(Map<String, BigDecimal> paxCreditMap) { this.paxCreditMap = paxCreditMap; }
	 * 
	 * public Map<String, BigDecimal> getPaxCreditMap(){ return this.paxCreditMap; }
	 */
	public void setReservationCredit(BigDecimal reservationCredit) {
		this.reservationCredit = reservationCredit;
	}

	public BigDecimal getReservationCredit() {
		return this.reservationCredit;
	}

	public boolean isNoPay() {
		return noPay;
	}

	public void setNoPay(boolean noPay) {
		this.noPay = noPay;
	}

	public BookingType getBookingType() {
		return bookingType;
	}

	public void setBookingType(BookingType bookingType) {
		this.bookingType = bookingType;
	}

	public String getOutboundExCharge() {
		return outboundExCharge;
	}

	public void setOutboundExCharge(String outboundExCharge) {
		this.outboundExCharge = outboundExCharge;
	}

	public String getInboundExCharge() {
		return inboundExCharge;
	}

	public void setInboundExCharge(String inboundExCharge) {
		this.inboundExCharge = inboundExCharge;
	}

	public String getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}

	public BigDecimal getInsuranceCalculatedPremium() {
		return insuranceCalculatedPremium;
	}

	public void setInsuranceCalculatedPremium(BigDecimal insuranceCalculatedPremium) {
		this.insuranceCalculatedPremium = insuranceCalculatedPremium;
	}

	public void setCaptchaValidated(boolean isCaptchaValidated) {
		this.isCaptchaValidated = isCaptchaValidated;
	}

	public boolean isCaptchaValidated() {
		return isCaptchaValidated;
	}

	public boolean isFromPostCardDetails() {
		return fromPostCardDetails;
	}

	public void setFromPostCardDetails(boolean fromPostCardDetails) {
		this.fromPostCardDetails = fromPostCardDetails;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Map<String, Map<String, BigDecimal>> getPnrPaxCreditMap() {
		return pnrPaxCreditMap;
	}

	public void addPnrPaxCreditMap(String pnr, Map<String, BigDecimal> paxCreditMap) {
		this.pnrPaxCreditMap.put(pnr, paxCreditMap);
	}

	public Map<String, BigDecimal> getPaxCreditMap(String CreditPnr) {
		return pnrPaxCreditMap.get(CreditPnr);
	}

	public int getTotalSegmentCount() {
		return totalSegmentCount;
	}

	public void setTotalSegmentCount(int totalSegmentCount) {
		this.totalSegmentCount = totalSegmentCount;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getBank3DSecureHTML() {
		return bank3DSecureHTML;
	}

	public void setBank3DSecureHTML(String bank3dSecureHTML) {
		bank3DSecureHTML = bank3dSecureHTML;
	}

	/**
	 * @return the reservationOnholdableOtherValidation
	 */
	public boolean isReservationOnholdableOtherValidation() {
		return reservationOnholdableOtherValidation;
	}

	/**
	 * @param reservationOnholdableOtherValidation
	 *            the reservationOnholdableOtherValidation to set
	 */
	public void setReservationOnholdableOtherValidation(boolean reservationOnholdableOtherValidation) {
		this.reservationOnholdableOtherValidation = reservationOnholdableOtherValidation;
	}

	/**
	 * @return the reservationOnholdableCombineValidation
	 */
	public boolean isReservationOnholdableCombineValidation() {
		return reservationOnholdableCombineValidation;
	}

	/**
	 * @param reservationOnholdableCombineValidation
	 *            the reservationOnholdableCombineValidation to set
	 */
	public void setReservationOnholdableCombineValidation(boolean reservationOnholdableCombineValidation) {
		this.reservationOnholdableCombineValidation = reservationOnholdableCombineValidation;
	}

	public BigDecimal getTotalTicketPriceInBase() {
		return totalTicketPriceInBase;
	}

	public void setTotalTicketPriceInBase(BigDecimal totalTicketPriceInBase) {
		this.totalTicketPriceInBase = totalTicketPriceInBase;
	}

	public List<String> getExcludedSegFarePnrSegIds() {
		return excludedSegFarePnrSegIds;
	}

	public void setExcludedSegFarePnrSegIds(List<String> excludedSegFarePnrSegIds) {
		this.excludedSegFarePnrSegIds = excludedSegFarePnrSegIds;
	}

	public DiscountedFareDetails getDiscountInfo() {
		return discountInfo;
	}

	public void setDiscountInfo(DiscountedFareDetails discountInfo) {
		this.discountInfo = discountInfo;
	}

	public BigDecimal getDiscountAmount(boolean excludeCreditDiscount) {
		BigDecimal disAmount = this.discountAmount;
		if (excludeCreditDiscount && this.discountInfo != null && this.discountInfo.getPromotionId() != null
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(this.discountInfo.getDiscountAs())) {
			disAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		return disAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public void removeFareDiscount() {
		this.discountInfo = null;
		this.discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public void setFareDiscount(List<PassengerTypeQuantityTO> paxQtyList) {
		if (getPriceInfoTO() != null && getPriceInfoTO().getFareTypeTO() != null) {

			ApplicablePromotionDetailsTO promoDetails = getPriceInfoTO().getFareTypeTO().getPromotionTO();
			setFareDiscountWith(paxQtyList, promoDetails);
		} else {
			removeFareDiscount();
		}
	}

	public void setFareDiscount(List<PassengerTypeQuantityTO> paxQtyList, ApplicablePromotionDetailsTO promoDetails) {
		setFareDiscountWith(paxQtyList, promoDetails);
	}

	private void setFareDiscountWith(List<PassengerTypeQuantityTO> paxQtyList, ApplicablePromotionDetailsTO promoDetails) {

		if (promoDetails != null) {
			discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			discountInfo = new DiscountedFareDetails();
			discountInfo.setPromotionId(promoDetails.getPromoCriteriaId());
			discountInfo.setPromoCode(promoDetails.getPromoCode());
			discountInfo.setPromotionType(promoDetails.getPromoType());
			discountInfo.setDescription(promoDetails.getDescription());
			discountInfo.setSystemGenerated(promoDetails.isSystemGenerated());
			discountInfo.setNotes(promoDetails.getPromoName());
			discountInfo.setFarePercentage(promoDetails.getDiscountValue());
			discountInfo.setDiscountType(promoDetails.getDiscountType());
			discountInfo.setDiscountApplyTo(promoDetails.getApplyTo());
			discountInfo.setDiscountAs(promoDetails.getDiscountAs());
			discountInfo.addApplicablePaxCount(PaxTypeTO.ADULT, promoDetails.getApplicableAdultCount());
			discountInfo.addApplicablePaxCount(PaxTypeTO.CHILD, promoDetails.getApplicableChildCount());
			discountInfo.addApplicablePaxCount(PaxTypeTO.INFANT, promoDetails.getApplicableInfantCount());
			discountInfo.setApplicableAncillaries(promoDetails.getApplicableAncillaries());
			discountInfo.setApplicableBINs(promoDetails.getApplicableBINs());
			discountInfo.setApplicableOnds(promoDetails.getApplicableOnds());
			discountInfo.setAllowSamePaxOnly(promoDetails.isAllowSamePaxOnly());
			discountInfo.setOriginPnr(promoDetails.getOriginPnr());
			discountInfo.setApplicableForOneway(promoDetails.isApplicableForOneWay());
			discountInfo.setApplicableForReturn(promoDetails.isApplicableForReturn());
			discountInfo.setApplicability(promoDetails.getDiscountApplicability());

		} else {
			removeFareDiscount();
		}
	}

	public BigDecimal getIbeReturnFareDiscountAmount() {
		return ibeReturnFareDiscountAmount;
	}

	public void setIbeReturnFareDiscountAmount(BigDecimal ibeReturnFareDiscountAmount) {
		this.ibeReturnFareDiscountAmount = ibeReturnFareDiscountAmount;
	}

	public List<ServiceTaxContainer> getApplicableServiceTaxes() {
		if (this.applicableServiceTaxes == null) {
			this.applicableServiceTaxes = new ArrayList<ServiceTaxContainer>();
		}

		return this.applicableServiceTaxes;
	}

	public void addServiceTax(boolean isTaxApplicable, BigDecimal taxRatio, EXTERNAL_CHARGES externalCharge) {
		ServiceTaxContainer serviceTaxContainer = new ServiceTaxContainer(isTaxApplicable, taxRatio, externalCharge);
		getApplicableServiceTaxes().add(serviceTaxContainer);
	}

	public void addNewServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		getApplicableServiceTaxes().clear();
		if (applicableServiceTaxes != null) {
			getApplicableServiceTaxes().addAll(applicableServiceTaxes);
		}
	}

	public void addServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		if (applicableServiceTaxes != null) {
			getApplicableServiceTaxes().addAll(applicableServiceTaxes);
		}
	}

	public ServiceTaxContainer getServiceTax(EXTERNAL_CHARGES externalCharge) {
		for (ServiceTaxContainer serviceTaxContainer : getApplicableServiceTaxes()) {
			if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == externalCharge) {
				return serviceTaxContainer;
			}
		}

		return null;
	}

	public boolean isTaxApplicable(EXTERNAL_CHARGES externalCharge) {
		for (ServiceTaxContainer serviceTaxContainer : getApplicableServiceTaxes()) {
			if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == externalCharge) {
				return serviceTaxContainer.isTaxApplicable();
			}
		}

		return false;
	}

	public BigDecimal getServiceTaxRatio(EXTERNAL_CHARGES externalCharge) {
		for (ServiceTaxContainer serviceTaxContainer : getApplicableServiceTaxes()) {
			if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == externalCharge) {
				return serviceTaxContainer.getTaxRatio();
			}
		}

		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public BigDecimal getServiceChargeTax(EXTERNAL_CHARGES externalCharge) {
		BigDecimal serviceChargeTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.getSelectedExternalCharges().get(externalCharge) != null) {
			serviceChargeTax = this.getSelectedExternalCharges().get(externalCharge).getTotalAmount();
		}
		return serviceChargeTax;
	}

	public BigDecimal getServiceCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.getSelectedExternalCharges().get(externalCharge) != null) {
			serviceCharge = this.getSelectedExternalCharges().get(externalCharge).getAmount();
		}
		return serviceCharge;
	}

	public Map<String, Map<EXTERNAL_CHARGES, Integer>> getAnciOfferTemplates() {
		return anciOfferTemplates;
	}

	public void setAnciOfferTemplates(Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplates) {
		this.anciOfferTemplates = anciOfferTemplates;
	}

	public LCCReservationBaggageSummaryTo getBaggageSummaryTo() {
		return baggageSummaryTo;
	}

	public void setBaggageSummaryTo(LCCReservationBaggageSummaryTo baggageSummaryTo) {
		this.baggageSummaryTo = baggageSummaryTo;
	}

	public List<BundledFareDTO> getSelectedBundledFares() {
		List<BundledFareDTO> bundledFareDTOs = null;
		if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
			bundledFareDTOs = priceInfoTO.getFareTypeTO().getOndBundledFareDTOs();
		} else {
			bundledFareDTOs = this.bundledFareDTOs;
		}
		return bundledFareDTOs;
	}

	public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
		this.bundledFareDTOs = bundledFareDTOs;
	}

	public List<AncillaryPaxTo> getAncillaryPaxTos() {
		return ancillaryPaxTos;
	}

	public void setAncillaryPaxTos(List<AncillaryPaxTo> ancillaryPaxTos) {
		this.ancillaryPaxTos = ancillaryPaxTos;
	}

	public boolean isApplyPenaltyForAnciModification() {
		return applyPenaltyForAnciModification;
	}

	public void setApplyPenaltyForAnciModification(boolean applyPenaltyForAnciModification) {
		this.applyPenaltyForAnciModification = applyPenaltyForAnciModification;
	}

	public Map<String, LoyaltyPaymentInfo> getCarrierWiseLoyaltyPaymentInfo() {
		return carrierWiseLoyaltyPaymentInfo;
	}

	public void setCarrierWiseLoyaltyPaymentInfo(Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo) {
		this.carrierWiseLoyaltyPaymentInfo = carrierWiseLoyaltyPaymentInfo;
	}

	public void setBalanceToPay(boolean hasBalanceToPay) {
		this.hasBalanceToPay = hasBalanceToPay;
	}

	public boolean hasBalanceToPay() {
		return hasBalanceToPay;
	}

	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	public String getMarketingCarrier() {
		return marketingCarrier;
	}

	public void setMarketingCarrier(String marketingCarrier) {
		this.marketingCarrier = marketingCarrier;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}
	
}
