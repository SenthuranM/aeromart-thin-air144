package com.isa.thinair.ibe.api.dto;

import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;

public class PaxTO {

	private String itnSeqNo;

	private String actualSeqNo;

	private String itnPaxName;
	
	private String itnPaxTitle;
	
	private String itnPaxFirstName;
	
	private String itnFFID;

	private String itnPaxLastName;

	private String itnSeat;

	private String itnMeal;

	private String itnBaggage;

	private String itnTravellingWith;
	
	private String dateOfBirth;

	/* Identify Pax */
	private String travelerRefNumber;

	private List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries;

	private boolean editableFlag = true;

	/**
	 * @return the itnSeqNo
	 */
	public String getItnSeqNo() {
		return itnSeqNo;
	}

	/**
	 * @param itnSeqNo
	 *            the itnSeqNo to set
	 */
	public void setItnSeqNo(String itnSeqNo) {
		this.itnSeqNo = itnSeqNo;
	}

	/**
	 * @return the itnPaxName
	 */
	public String getItnPaxName() {
		return itnPaxName;
	}

	/**
	 * @param itnPaxName
	 *            the itnPaxName to set
	 */
	public void setItnPaxName(String itnPaxName) {
		this.itnPaxName = itnPaxName;
	}

	/**
	 * @return the itnSeat
	 */
	public String getItnSeat() {
		return itnSeat;
	}

	/**
	 * @param itnSeat
	 *            the itnSeat to set
	 */
	public void setItnSeat(String itnSeat) {
		this.itnSeat = itnSeat;
	}

	/**
	 * @return the itnMeal
	 */
	public String getItnMeal() {
		return itnMeal;
	}

	/**
	 * @param itnMeal
	 *            the itnMeal to set
	 */
	public void setItnMeal(String itnMeal) {
		this.itnMeal = itnMeal;
	}

	public String getItnTravellingWith() {
		return itnTravellingWith;
	}

	public void setItnTravellingWith(String itnTravellingWith) {
		this.itnTravellingWith = itnTravellingWith;
	}

	public List<LCCSelectedSegmentAncillaryDTO> getSelectedAncillaries() {
		return selectedAncillaries;
	}

	public void setSelectedAncillaries(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		this.selectedAncillaries = selectedAncillaries;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public String getActualSeqNo() {
		return actualSeqNo;
	}

	public void setActualSeqNo(String actualSeqNo) {
		this.actualSeqNo = actualSeqNo;
	}

	public String getItnBaggage() {
		return itnBaggage;
	}

	public void setItnBaggage(String itnBaggage) {
		this.itnBaggage = itnBaggage;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth =dateOfBirth;
	}

    public String getItnPaxTitle() {
        return itnPaxTitle;
    }

    public void setItnPaxTitle(String itnPaxTitle) {
        this.itnPaxTitle = itnPaxTitle;
    }

    public String getItnPaxFirstName() {
        return itnPaxFirstName;
    }

    public void setItnPaxFirstName(String itnPaxFirstName) {
        this.itnPaxFirstName = itnPaxFirstName;
    }

    public String getItnPaxLastName() {
        return itnPaxLastName;
    }

    public void setItnPaxLastName(String itnPaxLastName) {
        this.itnPaxLastName = itnPaxLastName;
    }

	public boolean isEditableFlag() {
		return editableFlag;
	}

	public void setEditableFlag(boolean editableFlag) {
		this.editableFlag = editableFlag;
	}

	public String getItnFFID() {
		return itnFFID;
	}

	public void setItnFFID(String itnFFID) {
		this.itnFFID = itnFFID;
	}
}
