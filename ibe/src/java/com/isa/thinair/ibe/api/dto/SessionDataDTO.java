package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.commons.api.dto.VoucherDTO;

public class SessionDataDTO implements Serializable {

	private static final long serialVersionUID = -15331362963325053L;

	private int customerId = -1;
	private String customerEmail;
	private String customerPassword;
	private String language = null;
	private Integer numberOfAttempts = null;
	private String carrier = null;
	private Integer systemRefresh = null;
	private String loyaltyAccountNo = null;

	private ContactInfoDTO contactInfo = null;

	// private TransactionData transactionData = null;

	private IBEReservationInfoDTO ibeReservationInfo = null;

	private String tempPaymentPNR;

	private boolean responseReceivedFromPaymentGateway;

	private String manageBookingPNR;

	private IBEReservationPostPaymentDTO ibeReservationPostPaymentDTO = null;

	private String requestSessionIdentifier;

	private IBEReservationPostDTO ibeReservationPostDTO;

	private PutOnHoldBeforePaymentDTO putOnHoldBeforePaymentDTO = null;

	private IBEPromotionPaymentDTO ibePromotionPaymentDTO;

	private LCCReservationBaggageSummaryTo baggageSummaryTo;
	
	private VoucherCCPaymentDTO voucherCCPaymentDTO;

	private VoucherDTO voucherDTO;

	private boolean isExitPopupDisplayed = false;

	private String loyaltyFFID;
	
	private List<VoucherDTO> voucherList;

	/**
	 * @return the customerId
	 */
	public int getCustomerId() {
		return this.customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the customerEmail
	 */
	public String getCustomerEmail() {
		return this.customerEmail;
	}

	/**
	 * @param customerEmail
	 *            the customerEmail to set
	 */
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	/**
	 * @return the customerPassword
	 */
	public String getCustomerPassword() {
		return this.customerPassword;
	}

	/**
	 * @param customerPassword
	 *            the customerPassword to set
	 */
	public void setCustomerPassword(String customerPassword) {
		this.customerPassword = customerPassword;
	}

	/**
	 * @return the transactionData
	 */
	public TransactionData getTransactionData() {
		// return this.transactionData;
		return null;
	}

	/**
	 * @param transactionData
	 *            the transactionData to set
	 */
	public void setTransactionData(TransactionData transactionData) {
		// this.transactionData = transactionData;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return this.language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		if (language != null) {
			this.language = language.intern();
		} else {
			this.language = language;
		}
	}

	/**
	 * @return the attempts
	 */
	public Integer getNumberOfAttempts() {
		return this.numberOfAttempts;
	}

	/**
	 * @param attempts
	 *            the attempts to set
	 */
	public void setNumberOfAttempts(Integer attempts) {
		this.numberOfAttempts = attempts;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return this.carrier;
	}

	/**
	 * @param carrier
	 *            the carrier to set
	 */
	public void setCarrier(String carrier) {
		if (carrier != null) {
			this.carrier = carrier.intern();
		} else {
			this.carrier = carrier;
		}
	}

	/**
	 * @return the systemRefresh
	 */
	public Integer getSystemRefresh() {
		return this.systemRefresh;
	}

	/**
	 * @param systemRefresh
	 *            the systemRefresh to set
	 */
	public void setSystemRefresh(Integer systemRefresh) {
		this.systemRefresh = systemRefresh;
	}

	public String getLoyaltyAccountNo() {
		return loyaltyAccountNo;
	}

	public void setLoyaltyAccountNo(String accountNo) {
		this.loyaltyAccountNo = accountNo;
	}

	public IBEReservationInfoDTO getIbeReservationInfo() {
		return ibeReservationInfo;
	}

	public void setIbeReservationInfo(IBEReservationInfoDTO ibeReservationInfo) {
		this.ibeReservationInfo = ibeReservationInfo;
	}

	public IBEReservationPostPaymentDTO getIbeReservationPostPaymentDTO() {
		return ibeReservationPostPaymentDTO;
	}

	public void setIbeReservationPostPaymentDTO(IBEReservationPostPaymentDTO ibeReservationPostPaymentDTO) {
		this.ibeReservationPostPaymentDTO = ibeReservationPostPaymentDTO;
	}

	public String getTempPaymentPNR() {
		return tempPaymentPNR;
	}

	public void setTempPaymentPNR(String tempPaymentPNR) {
		this.tempPaymentPNR = tempPaymentPNR;
	}

	public boolean isResponseReceivedFromPaymentGateway() {
		return responseReceivedFromPaymentGateway;
	}

	public void setResponseReceivedFromPaymentGateway(boolean responseReceivedFromPaymentGateway) {
		this.responseReceivedFromPaymentGateway = responseReceivedFromPaymentGateway;
	}

	public String getManageBookingPNR() {
		return manageBookingPNR;
	}

	public void setManageBookingPNR(String manageBookingPNR) {
		this.manageBookingPNR = manageBookingPNR;
	}

	public String getRequestSessionIdentifier() {
		return requestSessionIdentifier;
	}

	public void setRequestSessionIdentifier(String requestSessionIdentifier) {
		this.requestSessionIdentifier = requestSessionIdentifier;
	}

	public IBEReservationPostDTO getIbeReservationPostDTO() {
		return ibeReservationPostDTO;
	}

	public void setIbeReservationPostDTO(IBEReservationPostDTO ibeReservationPostDTO) {
		this.ibeReservationPostDTO = ibeReservationPostDTO;
	}

	public PutOnHoldBeforePaymentDTO getPutOnHoldBeforePaymentDTO() {
		return putOnHoldBeforePaymentDTO;
	}

	public void setPutOnHoldBeforePaymentDTO(PutOnHoldBeforePaymentDTO putOnHoldBeforePaymentDTO) {
		this.putOnHoldBeforePaymentDTO = putOnHoldBeforePaymentDTO;
	}

	public IBEPromotionPaymentDTO getIbePromotionPaymentDTO() {
		return ibePromotionPaymentDTO;
	}

	public void setIbePromotionPaymentDTO(IBEPromotionPaymentDTO ibePromotionPaymentDTO) {
		this.ibePromotionPaymentDTO = ibePromotionPaymentDTO;
	}

	/**
	 * @return the contactInfo
	 */
	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public LCCReservationBaggageSummaryTo getBaggageSummaryTo() {
		return baggageSummaryTo;
	}

	public void setBaggageSummaryTo(LCCReservationBaggageSummaryTo baggageSummaryTo) {
		this.baggageSummaryTo = baggageSummaryTo;
	}

	public boolean isExitPopupDisplayed() {
		return isExitPopupDisplayed;
	}

	public void setExitPopupDisplayed(boolean isExitPopupDisplayed) {
		this.isExitPopupDisplayed = isExitPopupDisplayed;
	}

	public String getLoyaltyFFID() {
		return loyaltyFFID;
	}

	public void setLoyaltyFFID(String loyaltyFFID) {
		this.loyaltyFFID = loyaltyFFID;
	}	

	public VoucherCCPaymentDTO getVoucherPaymentDTO() {
		return voucherCCPaymentDTO;
	}

	public void setVoucherCCPaymentDTO(VoucherCCPaymentDTO voucherCCPaymentDTO) {
		this.voucherCCPaymentDTO = voucherCCPaymentDTO;
	}

	/**
	 * @return the voucherDTO
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO
	 *            the voucherDTO to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

	public List<VoucherDTO> getVoucherList() {
		return voucherList;
	}

	public void setVoucherList(List<VoucherDTO> voucherList) {
		this.voucherList = voucherList;
	}
}
