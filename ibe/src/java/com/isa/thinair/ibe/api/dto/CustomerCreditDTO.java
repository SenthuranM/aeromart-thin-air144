package com.isa.thinair.ibe.api.dto;

public class CustomerCreditDTO {

	private String pnr;

	private String fullName;

	private String balance;

	private String dateOfExpiry;

	private String groupPnr;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(String dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}
}
