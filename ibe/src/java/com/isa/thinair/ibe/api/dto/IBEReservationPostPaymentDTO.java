package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.BookingType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankSessionInfo;
import com.isa.thinair.paymentbroker.api.dto.paypal.PAYPALResponse;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

/**
 * Contain reservation data from payment page and post card details page to pages after the payment gw response is
 * received
 * 
 * @author Dilan Anuruddha
 * 
 */

//TODO re-factor with VoucherCCPaymentDTO

public class IBEReservationPostPaymentDTO {

	public IBEReservationPostPaymentDTO() {
		this.selectedExtCharges = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
	}

	private ContactInfoDTO contactInfo;

	private PaymentGateWayInfoDTO paymentGateWay;

	private Map temporyPaymentMap;

	private String ipgRefenceNo;

	private IPGResponseDTO ipgResponseDTO;

	private Collection<ReservationPaxTO> paxList;

	private List<FlightSegmentTO> selectedFlightSegments;

	private Collection<LCCClientReservationSegment> existingAllLccSegments;

	private Collection<LCCClientReservationSegment> modifiedLccSegments;

	private FlightPriceRQ flightPriceRQ;

	private String pnr;

	private boolean addModifyAncillary;

	private boolean isModifySegment;

	private String version;

	private List<LCCInsuranceQuotationDTO> insuranceQuotes;

	private String selectedCurrency;

	private boolean isFlexiQuote;

	private boolean responceReceived;

	// Handle multiple booking request in same session
	private PriceInfoTO priceInfoTO;

	private BigDecimal reservationCredit;

	private boolean noPay = false;

	private BigDecimal creditCardFee;

	private LoyaltyPaymentOption loyaltyPayOption;

	private BigDecimal loyaltyCredit;

	private String outboundExCharge;

	private String inboundExCharge;

	private BookingType bookingType;

	private ReservationBalanceTO lCCClientReservationBalance;

	private Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtCharges;

	private String transactionId;

	private Map<String, BigDecimal> paxCreditMap;

	private boolean addGroundSegment;

	private String selectedPnrSegment;

	private PAYPALResponse paypalResponse;

	private boolean switchToExternalURL;

	private boolean requoteFlightSearch;

	private String balanceQueryData;

	private boolean securePayment3D;

	private boolean isMakePayment;

	private boolean viewPaymentInIframe;

	private String invoiceStatus;

	private String billId;

	private List<ONDSearchDTO> ondList;

	private long timeToSpare;

	private boolean nameChange;

	private Collection<PaxTO> changedPaxNames;

	private boolean onHoldCreated;

	private String errorCode;

	private boolean retryingWithAnotherIPG;

	private Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo;

	private boolean hasBalanceToPay = true;

	private PayByVoucherInfo payByVoucherInfo;


	private boolean payFortStatus;

	private boolean payFortStatusVoucher;

	private boolean groupPnr;

	private BigDecimal promoDiscountTotal = new BigDecimal("0.00");

    private AmeriaBankSessionInfo ameriaBankSessionInfo;

	private boolean isPaymentAdminFee;

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	public boolean isPayFortStatus() {
		return payFortStatus;
	}

	public void setPayFortStatus(boolean payFortStatus) {
		this.payFortStatus = payFortStatus;
	}

	public boolean isPayFortStatusVoucher() {
		return payFortStatusVoucher;
	}

	public void setPayFortStatusVoucher(boolean payFortStatusVoucher) {
		this.payFortStatusVoucher = payFortStatusVoucher;
	}

	public ContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	/**
	 * @return the retryingWithAnotherIPG
	 */
	public boolean isRetryingWithAnotherIPG() {
		return retryingWithAnotherIPG;
	}

	/**
	 * @param retryingWithAnotherIPG
	 *            the retryingWithAnotherIPG to set
	 */
	public void setRetryingWithAnotherIPG(boolean retryingWithAnotherIPG) {
		this.retryingWithAnotherIPG = retryingWithAnotherIPG;
	}

	public void setContactInfo(ContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public PaymentGateWayInfoDTO getPaymentGateWay() {
		return paymentGateWay;
	}

	public void setPaymentGateWay(PaymentGateWayInfoDTO paymentGateWay) {
		this.paymentGateWay = paymentGateWay;
	}

	public Map getTemporyPaymentMap() {
		return temporyPaymentMap;
	}

	public void setTemporyPaymentMap(Map temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	public String getIpgRefenceNo() {
		return ipgRefenceNo;
	}

	public void setIpgRefenceNo(String ipgRefenceNo) {
		this.ipgRefenceNo = ipgRefenceNo;
	}

	public IPGResponseDTO getIpgResponseDTO() {
		return ipgResponseDTO;
	}

	public void setIpgResponseDTO(IPGResponseDTO ipgResponseDTO) {
		this.ipgResponseDTO = ipgResponseDTO;
	}

	public Collection<ReservationPaxTO> getPaxList() {
		return paxList;
	}

	public void setPaxList(Collection<ReservationPaxTO> paxList) {
		this.paxList = paxList;
	}

	public void setSelectedFlightSegments(List<FlightSegmentTO> selectedFlightSegments) {
		this.selectedFlightSegments = selectedFlightSegments;
	}

	public List<FlightSegmentTO> getSelectedFlightSegments() {
		return selectedFlightSegments;
	}

	public FlightPriceRQ getFlightPriceRQ() {
		return this.flightPriceRQ;
	}

	public void setFlightPriceRQ(FlightPriceRQ flightPriceRQ) {
		this.flightPriceRQ = flightPriceRQ;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPnr() {
		return this.pnr;
	}

	public void setAddModifyAncillary(boolean addModifyAncillary) {
		this.addModifyAncillary = addModifyAncillary;
	}

	public boolean isAddModifyAncillary() {
		return addModifyAncillary;
	}

	public String getVersion() {
		return version;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public boolean isModifySegment() {
		return isModifySegment;
	}

	public void setModifySegment(boolean isModifySegment) {
		this.isModifySegment = isModifySegment;
	}

	public List<LCCInsuranceQuotationDTO> getInsuranceQuotes() {
		return insuranceQuotes;
	}

	public void setInsuranceQuotes(List<LCCInsuranceQuotationDTO> insuranceQuotes) {
		this.insuranceQuotes = insuranceQuotes;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	/**
	 * @return the isFlexiQuote
	 */
	public boolean isFlexiQuote() {
		return isFlexiQuote;
	}

	/**
	 * @param isFlexiQuote
	 *            the isFlexiQuote to set
	 */
	public void setFlexiQuote(boolean isFlexiQuote) {
		this.isFlexiQuote = isFlexiQuote;
	}

	public boolean isResponseReceived() {
		return responceReceived;
	}

	public void setResponseReceived(boolean responseReceived) {
		this.responceReceived = responseReceived;
	}

	public PriceInfoTO getPriceInfoTO() {
		return priceInfoTO;
	}

	public void setPriceInfoTO(PriceInfoTO priceInfoTO) {
		this.priceInfoTO = priceInfoTO;
	}

	public BigDecimal getReservationCredit() {
		return reservationCredit;
	}

	public void setReservationCredit(BigDecimal reservationCredit) {
		this.reservationCredit = reservationCredit;
	}

	public boolean isNoPay() {
		return noPay;
	}

	public void setNoPay(boolean noPay) {
		this.noPay = noPay;
	}

	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public LoyaltyPaymentOption getLoyaltyPayOption() {
		return loyaltyPayOption;
	}

	public void setLoyaltyPayOption(LoyaltyPaymentOption loyaltyPayOption) {
		this.loyaltyPayOption = loyaltyPayOption;
	}

	public BigDecimal getLoyaltyCredit() {
		return loyaltyCredit;
	}

	public void setLoyaltyCredit(BigDecimal loyaltyCredit) {
		this.loyaltyCredit = loyaltyCredit;
	}

	public String getOutboundExCharge() {
		return outboundExCharge;
	}

	public void setOutboundExCharge(String outboundExCharge) {
		this.outboundExCharge = outboundExCharge;
	}

	public String getInboundExCharge() {
		return inboundExCharge;
	}

	public void setInboundExCharge(String inboundExCharge) {
		this.inboundExCharge = inboundExCharge;
	}

	public ReservationBalanceTO getlCCClientReservationBalance() {
		return lCCClientReservationBalance;
	}

	public void setlCCClientReservationBalance(ReservationBalanceTO lCCClientReservationBalance) {
		this.lCCClientReservationBalance = lCCClientReservationBalance;
	}

	public BookingType getBookingType() {
		return bookingType;
	}

	public void setBookingType(BookingType bookingType) {
		this.bookingType = bookingType;
	}

	public void addExternalCharge(ExternalChgDTO ccChgDTO) {
		this.selectedExtCharges.put(ccChgDTO.getExternalChargesEnum(), ccChgDTO);
	}

	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getSelectedExternalCharges() {
		return this.selectedExtCharges;
	}

	public void removeExternalCharge(EXTERNAL_CHARGES extChgEnum) {
		this.selectedExtCharges.remove(extChgEnum);
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Map<String, BigDecimal> getPaxCreditMap() {
		return paxCreditMap;
	}

	public void setPaxCreditMap(Map<String, BigDecimal> paxCreditMap) {
		this.paxCreditMap = paxCreditMap;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public String getSelectedPnrSegment() {
		return selectedPnrSegment;
	}

	public void setSelectedPnrSegment(String selectedPnrSegment) {
		this.selectedPnrSegment = selectedPnrSegment;
	}

	public Collection<LCCClientReservationSegment> getExistingAllLccSegments() {
		return existingAllLccSegments;
	}

	public void setExistingAllLccSegments(Collection<LCCClientReservationSegment> existingAllLccSegments) {
		this.existingAllLccSegments = existingAllLccSegments;
	}

	public Collection<LCCClientReservationSegment> getModifiedLccSegments() {
		return modifiedLccSegments;
	}

	public void setModifiedLccSegments(Collection<LCCClientReservationSegment> modifiedLccSegments) {
		this.modifiedLccSegments = modifiedLccSegments;
	}

	/**
	 * @return the paypalResponse
	 */
	public PAYPALResponse getPaypalResponse() {
		return paypalResponse;
	}

	/**
	 * @param paypalResponse
	 *            the paypalResponse to set
	 */
	public void setPaypalResponse(PAYPALResponse paypalResponse) {
		this.paypalResponse = paypalResponse;
	}

	public boolean isSwitchToExternalURL() {
		return switchToExternalURL;
	}

	public void setSwitchToExternalURL(boolean switchToExternalURL) {
		this.switchToExternalURL = switchToExternalURL;
	}

	public boolean isSecurePayment3D() {
		return securePayment3D;
	}

	public void setSecurePayment3D(boolean securePayment3D) {
		this.securePayment3D = securePayment3D;
	}

	public boolean isMakePayment() {
		return isMakePayment;
	}

	public void setMakePayment(boolean isMakePayment) {
		this.isMakePayment = isMakePayment;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public String getBalanceQueryData() {
		return balanceQueryData;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	/**
	 * @return the viewPaymentInIframe
	 */
	public boolean isViewPaymentInIframe() {
		return viewPaymentInIframe;
	}

	/**
	 * @param viewPaymentInIframe
	 *            the viewPaymentInIframe to set
	 */
	public void setViewPaymentInIframe(boolean viewPaymentInIframe) {
		this.viewPaymentInIframe = viewPaymentInIframe;
	}

	public void setOndList(List<ONDSearchDTO> ondList) {
		this.ondList = ondList;
	}

	public List<ONDSearchDTO> getOndList() {
		return ondList;
	}

	public Collection<PaxTO> getChangedPaxNames() {
		return changedPaxNames;
	}

	/**
	 * @return the timeToSpare
	 */
	public long getTimeToSpare() {
		return timeToSpare;
	}

	/**
	 * @param timeToSpare
	 *            the timeToSpare to set
	 */
	public void setTimeToSpare(long timeToSpare) {
		this.timeToSpare = timeToSpare;
	}

	public void setChangedPaxNames(Collection<PaxTO> changedPaxNames) {
		this.changedPaxNames = changedPaxNames;
	}

	public boolean isNameChange() {
		return nameChange;
	}

	public void setNameChange(boolean nameChange) {
		this.nameChange = nameChange;
	}

	/**
	 * @return the invoiceStatus
	 */
	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	/**
	 * @param invoiceStatus
	 *            the invoiceStatus to set
	 */
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	/**
	 * @return the billId
	 */
	public String getBillId() {
		return billId;
	}

	/**
	 * @param billId
	 *            the billId to set
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	/**
	 * @return the onHoldCreated
	 */
	public boolean isOnHoldCreated() {
		return onHoldCreated;
	}

	/**
	 * @param onHoldCreated
	 *            the onHoldCreated to set
	 */
	public void setOnHoldCreated(boolean onHoldCreated) {
		this.onHoldCreated = onHoldCreated;
	}

	public void setBalanceToPay(boolean hasBalanceToPay) {
		this.hasBalanceToPay = hasBalanceToPay;
	}

	public boolean hasNoCCPayment() {
		return (getLoyaltyPayOption() == LoyaltyPaymentOption.TOTAL || !hasBalanceToPay);
	}

	public Map<String, LoyaltyPaymentInfo> getCarrierWiseLoyaltyPaymentInfo() {
		return carrierWiseLoyaltyPaymentInfo;
	}

	public void setCarrierWiseLoyaltyPaymentInfo(Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo) {
		this.carrierWiseLoyaltyPaymentInfo = carrierWiseLoyaltyPaymentInfo;
	}

	public LoyaltyPaymentInfo getCarrierLoyaltyPaymentInfo(String carrierCode) {
		if (carrierWiseLoyaltyPaymentInfo != null && carrierWiseLoyaltyPaymentInfo.containsKey(carrierCode)) {
			return carrierWiseLoyaltyPaymentInfo.get(carrierCode);
		}

		return null;
	}

	public BigDecimal getPromoDiscountTotal() {
		return promoDiscountTotal;
	}

	public void setPromoDiscountTotal(BigDecimal promoDiscountTotal) {
		this.promoDiscountTotal = promoDiscountTotal;
	}

	public AmeriaBankSessionInfo getAmeriaBankSessionInfo() {
		return ameriaBankSessionInfo;
	}

	public void setAmeriaBankSessionInfo(AmeriaBankSessionInfo ameriaBankSessionInfo) {
		this.ameriaBankSessionInfo = ameriaBankSessionInfo;

	}

	public void setPaymentAdminFee(boolean isPaymentAdminFee) {
		this.isPaymentAdminFee = isPaymentAdminFee;
	}


	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}
	
	public boolean isAddCCExternalCharge() {
		return (this.isPaymentAdminFee || !hasNoCCPayment());
	}
}