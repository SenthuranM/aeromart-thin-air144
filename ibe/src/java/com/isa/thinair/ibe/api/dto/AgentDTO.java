package com.isa.thinair.ibe.api.dto;

public class AgentDTO {

	private String agencyName;

	private String licenseNo;

	private String iATACode;

	private String station;

	private String payMeth;

	private String chkPayMeth;

	private String chkCoPayMeth;

	private String address;

	private String city;

	private String country;

	private String state;

	private String postalCode;

	private String email;

	private String telephone;

	private String fax;

	private String comments;

	private String mobile;

	private String companyContactName;

	private String contactDesignation;

	private String companySize;

	private String convenientTime;

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getiATACode() {
		return iATACode;
	}

	public void setiATACode(String iATACode) {
		this.iATACode = iATACode;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getPayMeth() {
		return payMeth;
	}

	public void setPayMeth(String payMeth) {
		this.payMeth = payMeth;
	}

	public String getChkPayMeth() {
		return chkPayMeth;
	}

	public void setChkPayMeth(String chkPayMeth) {
		this.chkPayMeth = chkPayMeth;
	}

	public String getChkCoPayMeth() {
		return chkCoPayMeth;
	}

	public void setChkCoPayMeth(String chkCoPayMeth) {
		this.chkCoPayMeth = chkCoPayMeth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompanyContactName() {
		return companyContactName;
	}

	public void setCompanyContactName(String companyContactName) {
		this.companyContactName = companyContactName;
	}

	public String getContactDesignation() {
		return contactDesignation;
	}

	public void setContactDesignation(String contactDesignation) {
		this.contactDesignation = contactDesignation;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public String getConvenientTime() {
		return convenientTime;
	}

	public void setConvenientTime(String convenientTime) {
		this.convenientTime = convenientTime;
	}

}
