package com.isa.thinair.ibe.api.dto;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants.Mode;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;

public class IBEReservationPostDTO {

	private SYSTEM selectedSystem;

	private Mode mode;

	private String pnr;
	
	// Temporally added
	// TODO Remove
	private IPGTransactionResultDTO ipgTransactionResultDTO = null;

	/**
	 * Indicates if a successful refund has occurred.
	 */
	private boolean refundSuccessful;

	private boolean itineraryEmailGenerated;

	public SYSTEM getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(SYSTEM selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public IPGTransactionResultDTO getIpgTransactionResultDTO() {
		return ipgTransactionResultDTO;
	}

	public void setIpgTransactionResultDTO(
			IPGTransactionResultDTO ipgTransactionResultDTO) {
		this.ipgTransactionResultDTO = ipgTransactionResultDTO;
	}	
	
	public void setRefundSuccessful(boolean refundSuccessful) {
		this.refundSuccessful = refundSuccessful;
	}

	public boolean isRefundSuccessful() {
		return refundSuccessful;
	}

	public boolean isItineraryEmailGenerated() {
		return itineraryEmailGenerated;
	}

	public void setItineraryEmailGenerated(boolean itineraryEmailGenerated) {
		this.itineraryEmailGenerated = itineraryEmailGenerated;
	}

}
