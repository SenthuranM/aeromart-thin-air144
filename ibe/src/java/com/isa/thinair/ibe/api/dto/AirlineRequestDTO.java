package com.isa.thinair.ibe.api.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts.LanguageCode;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants.PAGE;

/**
 * @author pkarunanayake
 * 
 */
public class AirlineRequestDTO {

	private LanguageCode lanaguage;

	private PAGE requestType;

	private String originCode;

	private String destinationCode;

	private Date departueDate;

	private Date returnDate;

	private int departureVariance;

	private int returenVariance;

	private String currency;

	private int adultCount;

	private int childCount;

	private int infantCount;

	private String originName;

	private String destinationName;

	private boolean isReturn;

	private String cabinClass;

	private String fareType;

	// External Link - Manage Booking
	private String pnr;

	private String lastName;

	private String promoCode;

	private Integer bankIdentificationNo;

	private String cookieDetails;

	private String hubTransitDetail;

	private String paxType;
	
	private String alertId;
	
	private String pnrSegId;

	/**
	 * 
	 * @param sysRequestParameters
	 *            - EN^AS^SHJ^CMB^06/10/2010^03/11/2010^0^0^AED^1^1^1^Sharjah^Colombo^true^Y^^FARETYPE^PROMO^123456^
	 * @throws ModuleException
	 */
	public AirlineRequestDTO(String[] paramArr) throws ModuleException {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		if (paramArr.length >= 1) {

			// TODO Set language
			// this.lanaguage = SessionUtil.getLanguage(request);

			if (paramArr.length > 1) {
				this.requestType = PAGE.getEnum(paramArr[1].toUpperCase());
				// For Invalid Request Type
				if (requestType == PAGE.NONE) {
					throw new ModuleException("ibe.invalid.request.type");
				}

				// Search Parameters
				if (requestType == PAGE.AS) {

					if (paramArr.length > 2) {
						this.originCode = paramArr[2];
					}

					if (paramArr.length > 3) {
						this.destinationCode = paramArr[3];
					}

					if (originCode.trim().isEmpty() || destinationCode.trim().isEmpty()) {
						throw new ModuleException("module.invalid.origndestination");
					}

					if (paramArr.length > 4) {
						try {
							this.departueDate = format.parse(paramArr[4]);
						} catch (Exception ex) {
							throw new ModuleException("module.invalid.departuredate");
						}
					}

					if (paramArr.length > 5 && !paramArr[5].trim().isEmpty()) {
						try {
							this.returnDate = format.parse(paramArr[5]);
						} catch (Exception ex) {
							throw new ModuleException("module.invalid.returndate");
						}
					}

					try {
						if (paramArr.length > 6 && !paramArr[6].trim().isEmpty()) {
							this.departureVariance = Integer.parseInt(paramArr[6]);
						}

						if (paramArr.length > 7 && !paramArr[7].trim().isEmpty()) {
							this.returenVariance = Integer.parseInt(paramArr[7]);
						}

						if (paramArr.length > 8) {
							this.currency = paramArr[8];
						}

						if (paramArr.length > 9 && !paramArr[9].trim().isEmpty()) {
							this.adultCount = Integer.parseInt(paramArr[9]);
						}

						if (paramArr.length > 10 && !paramArr[10].trim().isEmpty()) {
							this.childCount = Integer.parseInt(paramArr[10]);
						}

						if (paramArr.length > 11 && !paramArr[11].trim().isEmpty()) {
							this.infantCount = Integer.parseInt(paramArr[11]);
						}

						if (paramArr.length > 12) {
							this.originName = paramArr[12];
						}

						if (paramArr.length > 13) {
							this.destinationName = paramArr[13];
						}

						if (paramArr.length > 14 && !paramArr[14].trim().isEmpty()) {
							this.isReturn = new Boolean(paramArr[14]);
						}

						if (paramArr.length > 15) {
							this.cabinClass = paramArr[15];
						}

						if (paramArr.length > 16) {
							this.paxType = paramArr[16];
						}

						if (paramArr.length > 17) {
							this.fareType = paramArr[17];
						}

						// Promo code
						if (paramArr.length > 18 && !paramArr[18].trim().isEmpty()) {
							this.promoCode = paramArr[18];
						}

						// Bank identification number
						if (paramArr.length > 19 && !paramArr[19].trim().isEmpty()) {
							this.bankIdentificationNo = Integer.parseInt(paramArr[19]);
						}

						// Cookie details
						if (paramArr.length > 20 && !paramArr[20].trim().isEmpty()) {
							this.cookieDetails = paramArr[20];
						}

						// Hub Transit Detail HUBCODE_DAYS
						if (paramArr.length > 21 && !paramArr[21].trim().isEmpty()) {
							this.hubTransitDetail = paramArr[21];
						}

					} catch (Exception ex) {
						throw new ModuleException("ibe.invalid.request.type");
					}

					// TODO Verify for Max variance
					if (departureVariance > WebConstants.MAX_DEPARTURE_VARIANCE
							|| returenVariance > WebConstants.MAX_DEPARTURE_VARIANCE) {
						throw new ModuleException("module.invalid.departurereturn.variance");
					}

					// Max Adult Count
					if (AppSysParamsUtil.getMaxAdultCountIBE() < adultCount) {
						throw new ModuleException("module.invalid.adultchild.count");
					}

				} // Manage Booking Parameters
				else if (requestType == PAGE.ELINK || requestType == PAGE.MB) {
					if (paramArr.length > 2) {
						this.pnr = paramArr[2];
					}

					if (paramArr.length > 3) {
						this.lastName = paramArr[3];
					}

					if (paramArr.length > 4) {
						try {
							this.departueDate = format.parse(paramArr[4]);
						} catch (Exception ex) {
							throw new ModuleException("module.invalid.departuredate");
						}
					}
				}else if(requestType == PAGE.SR){
					
					if (paramArr.length > 2) {
						this.pnr = paramArr[2];
					}

					if (paramArr.length > 3) {
						this.alertId = paramArr[3];
					}
					
					if (paramArr.length > 4) {
						this.pnrSegId = paramArr[4];
					}
					
				}
			}

		} else {
			throw new ModuleException("ibe.invalid.request.type");
		}
	}

	public LanguageCode getLanaguage() {
		return lanaguage;
	}

	public void setLanaguage(LanguageCode lanaguage) {
		this.lanaguage = lanaguage;
	}

	public PAGE getRequestType() {
		return requestType;
	}

	public void setRequestType(PAGE requestType) {
		this.requestType = requestType;
	}

	public String getOriginCode() {
		return originCode;
	}

	public void setOriginCode(String originCode) {
		this.originCode = originCode;
	}

	public String getDestinationCode() {
		return destinationCode;
	}

	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}

	public Date getDepartueDate() {
		return departueDate;
	}

	public void setDepartueDate(Date departueDate) {
		this.departueDate = departueDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public int getDepartureVariance() {
		return departureVariance;
	}

	public void setDepartureVariance(int departureVariance) {
		this.departureVariance = departureVariance;
	}

	public int getReturenVariance() {
		return returenVariance;
	}

	public void setReturenVariance(int returenVariance) {
		this.returenVariance = returenVariance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public boolean isReturn() {
		return isReturn;
	}

	public void setReturn(boolean isReturn) {
		this.isReturn = isReturn;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getFareType() {
		return fareType;
	}

	public void setFareType(String fareType) {
		this.fareType = fareType;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Integer getBankIdentificationNo() {
		return bankIdentificationNo;
	}

	public void setBankIdentificationNo(Integer bankIdentificationNo) {
		this.bankIdentificationNo = bankIdentificationNo;
	}

	public String getCookieDetails() {
		return cookieDetails;
	}

	public void setCookieDetails(String cookieDetails) {
		this.cookieDetails = cookieDetails;
	}

	public String getHubTransitDetail() {
		return hubTransitDetail;
	}

	public void setHubTransitDetail(String hubTransitDetail) {
		this.hubTransitDetail = hubTransitDetail;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getAlertId() {
		return alertId;
	}

	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	public String getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(String pnrSegId) {
		this.pnrSegId = pnrSegId;
	}
}
