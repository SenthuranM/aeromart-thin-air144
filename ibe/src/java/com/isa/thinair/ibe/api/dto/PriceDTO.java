package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class PriceDTO {

	private String pay;

	private String base;

	public PriceDTO() {
		this(AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero());
	}

	public PriceDTO(BigDecimal base, BigDecimal pay) {
		this.base = base.toString();
		this.pay = pay.toString();
	}

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}
}
