package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;

public class PricingDetailDTO {

	private PriceDTO adultFare = null;

	private PriceDTO adultTnS = null;

	private PriceDTO childFare = null;

	private PriceDTO childTnS = null;

	private PriceDTO infantFare = null;

	private PriceDTO infantTnS = null;

	private PriceDTO totalAdultFTS = null;

	private PriceDTO totalChildFTS = null;

	private PriceDTO totalInfantFTS = null;

	private PriceDTO totalAdultChildFTS = null;

	private PriceDTO totalFare = null;

	private PriceDTO totalTnS = null;

	private PriceDTO totalFarePlusTotalTnS = null;

	// getters
	public PriceDTO getAdultFare() {
		return adultFare;
	}

	public PriceDTO getAdultTnS() {
		return adultTnS;
	}

	public PriceDTO getChildFare() {
		return childFare;
	}

	public PriceDTO getChildTnS() {
		return childTnS;
	}

	public PriceDTO getInfantFare() {
		return infantFare;
	}

	public PriceDTO getInfantTnS() {
		return infantTnS;
	}

	public PriceDTO getTotalFare() {
		return totalFare;
	}

	public PriceDTO getTotalTnS() {
		return totalTnS;
	}

	public PriceDTO getTotalFarePlusTotalTnS() {
		return totalFarePlusTotalTnS;
	}

	public PriceDTO getTotalAdultFTS() {
		return totalAdultFTS;
	}

	public PriceDTO getTotalChildFTS() {
		return totalChildFTS;
	}

	public PriceDTO getTotalInfantFTS() {
		return totalInfantFTS;
	}

	public PriceDTO getTotalAdultChildFTS() {
		return totalAdultChildFTS;
	}

	// adders
	public void addAdultFare(BigDecimal base, BigDecimal pay) {
		this.adultFare = new PriceDTO(base, pay);
	}

	public void addAdultTnS(BigDecimal base, BigDecimal pay) {
		this.adultTnS = new PriceDTO(base, pay);
	}

	public void addChildFare(BigDecimal base, BigDecimal pay) {
		this.childFare = new PriceDTO(base, pay);
	}

	public void addChildTnS(BigDecimal base, BigDecimal pay) {
		this.childTnS = new PriceDTO(base, pay);
	}

	public void addInfantFare(BigDecimal base, BigDecimal pay) {
		this.infantFare = new PriceDTO(base, pay);
	}

	public void addInfantTnS(BigDecimal base, BigDecimal pay) {
		this.infantTnS = new PriceDTO(base, pay);
	}

	public void addTotalFare(BigDecimal base, BigDecimal pay) {
		this.totalFare = new PriceDTO(base, pay);
	}

	public void addTotalTnS(BigDecimal base, BigDecimal pay) {
		this.totalTnS = new PriceDTO(base, pay);
	}

	public void addTotalFarePlusTotalTnS(BigDecimal base, BigDecimal pay) {
		this.totalFarePlusTotalTnS = new PriceDTO(base, pay);
	}

	public void addTotalAdultFTS(BigDecimal base, BigDecimal pay) {
		this.totalAdultFTS = new PriceDTO(base, pay);
	}

	public void addTotalChildFTS(BigDecimal base, BigDecimal pay) {
		this.totalChildFTS = new PriceDTO(base, pay);
	}

	public void addTotalInfantFTS(BigDecimal base, BigDecimal pay) {
		this.totalInfantFTS = new PriceDTO(base, pay);
	}

	public void addTotalAdultChildFTS(BigDecimal base, BigDecimal pay) {
		this.totalAdultChildFTS = new PriceDTO(base, pay);
	}
}
