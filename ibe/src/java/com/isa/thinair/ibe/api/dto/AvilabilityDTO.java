package com.isa.thinair.ibe.api.dto;

import java.util.ArrayList;
import java.util.Collection;

public class AvilabilityDTO {

	private Collection<FlightInfoDTO> availableFlights = new ArrayList<FlightInfoDTO>();

	private boolean flightsScheduled = true;

	public Collection<FlightInfoDTO> getAvailableFlights() {
		return availableFlights;
	}

	public void setAvailableFlights(Collection<FlightInfoDTO> availableFlights) {
		this.availableFlights = availableFlights;
	}

	public boolean isFlightsScheduled() {
		return flightsScheduled;
	}

	public void setFlightsScheduled(boolean flightsScheduled) {
		this.flightsScheduled = flightsScheduled;
	}

	public void addFlightInfo(FlightInfoDTO flightInfoDTO) {
		if (this.availableFlights == null)
			this.availableFlights = new ArrayList<FlightInfoDTO>();
		this.availableFlights.add(flightInfoDTO);
	}
}
