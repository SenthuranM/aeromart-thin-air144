package com.isa.thinair.ibe.api.dto;

public class ItineraryPaymentListTO {
	private String totalAirfare;
	private String totalTaxSurcharge;

	public String getTotalAirfare() {
		return totalAirfare;
	}

	public void setTotalAirfare(String totalAirfare) {
		this.totalAirfare = totalAirfare;
	}

	public String getTotalTaxSurcharge() {
		return totalTaxSurcharge;
	}

	public void setTotalTaxSurcharge(String totalTaxSurcharge) {
		this.totalTaxSurcharge = totalTaxSurcharge;
	}

}
