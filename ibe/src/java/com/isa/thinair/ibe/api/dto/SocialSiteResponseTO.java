package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

/**
 * Tranfer Object to hanldle the response from social sites
 * 
 */
public class SocialSiteResponseTO implements Serializable {

	private static final long serialVersionUID = 4587535566621880462L;
	private String email;
	private String firstName;
	private String gender;
	private String id;
	private String lastName;
	private String link;
	private String name;
	private String siteType;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	@Override
	public String toString() {
		return "SocialSiteResponseTO [email=" + email + ", firstName=" + firstName + ", gender=" + gender + ", id=" + id
				+ ", lastName=" + lastName + ", link=" + link + ", name=" + name + ", siteType=" + siteType + "]";
	}

}
