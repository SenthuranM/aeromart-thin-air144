package com.isa.thinair.ibe.api.dto;

import java.util.Locale;

public class IBECommonDTO {

	private String nonSecurePath = "";

	private String securePath = "";

	private String homeURL = "";

	private boolean regCustomer = false;

	private boolean kioskAccessPoint = false;

	private String locale = "en";

	private Locale userLocale;

	private String pointOfSale;
	
	private boolean skipPersianCalendarChange;

	public String getNonSecurePath() {
		return nonSecurePath;
	}

	public void setNonSecurePath(String nonSecurePath) {
		this.nonSecurePath = nonSecurePath;
	}

	public String getSecurePath() {
		return securePath;
	}

	public void setSecurePath(String securePath) {
		this.securePath = securePath;
	}

	public String getHomeURL() {
		return homeURL;
	}

	public void setHomeURL(String homeURL) {
		this.homeURL = homeURL;
	}

	public boolean isRegCustomer() {
		return regCustomer;
	}

	public void setRegCustomer(boolean regCustomer) {
		this.regCustomer = regCustomer;
	}

	public String getLocale() {
		return locale;
	}

	public Locale getUserLocale() {
		return userLocale;
	}

	public void setLocale(String locale) {
		if (locale == null || "".equals(locale)) {
			locale = "en";
		} else {
			this.locale = locale;
		}
		userLocale = new Locale(this.locale);
	}

	public boolean isKioskAccessPoint() {
		return kioskAccessPoint;
	}

	public void setKioskAccessPoint(boolean kioskAccessPoint) {
		this.kioskAccessPoint = kioskAccessPoint;
	}

	public String getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

	public boolean isSkipPersianCalendarChange() {
		return skipPersianCalendarChange;
	}

	public void setSkipPersianCalendarChange(boolean skipPersianCalendarChange) {
		this.skipPersianCalendarChange = skipPersianCalendarChange;
	}

}
