package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

public class JourneyInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8065858838193119892L;

	private String journeyDate;

	// station codes
	private String stationStartCode;
	private String stationEndCode;

	// Airport names,with terminals
	private String airportStartCode;
	private String airportEndCode;

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getStationStartCode() {
		return stationStartCode;
	}

	public void setStationStartCode(String stationStartCode) {
		this.stationStartCode = stationStartCode;
	}

	public String getStationEndCode() {
		return stationEndCode;
	}

	public void setStationEndCode(String stationEndCode) {
		this.stationEndCode = stationEndCode;
	}

	public String getAirportStartCode() {
		return airportStartCode;
	}

	public void setAirportStartCode(String airportStartCode) {
		this.airportStartCode = airportStartCode;
	}

	public String getAirportEndCode() {
		return airportEndCode;
	}

	public void setAirportEndCode(String airportEndCode) {
		this.airportEndCode = airportEndCode;
	}

}
