package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;
import java.util.Collection;

public class AvailableFlightInfoByDateDTO {

	private String departureDate;
	private BigDecimal minFare;
	private boolean selected;
	private Collection<AvailableFlightInfo> availableFlightInfo;

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public BigDecimal getMinFare() {
		return minFare;
	}

	public void setMinFare(BigDecimal minFare) {
		this.minFare = minFare;
	}

	public Collection<AvailableFlightInfo> getAvailableFlightInfo() {
		return availableFlightInfo;
	}

	public void setAvailableFlightInfo(Collection<AvailableFlightInfo> availableFlightInfo) {
		this.availableFlightInfo = availableFlightInfo;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
