package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PromoMetaTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String inboundSegmentCode = "";
	private String inboundSegmentIds = ""; // pnr
	private String outboundSegmentCode = "";
	private String outboundSegmentIds = "";

	private String outboundSegDisplayName = "";
	private String inboundSegDisplayName = "";

	private String outboundDepartureDate;
	private String inboundDepartureDate;

	private boolean inboudDefined = false;
	private boolean outboundDefined = false;

	private List<String> inSegList = new ArrayList<String>();
	private List<String> outSegList = new ArrayList<String>();

	public String getInboundSegmentCode() {
		return inboundSegmentCode;
	}

	public void setInboundSegmentCode(String inboundSegmentCode) {
		this.inboundSegmentCode = inboundSegmentCode;
	}

	public String getInboundSegmentIds() {
		return inboundSegmentIds;
	}

	public void setInboundSegmentIds(String inboundSegmentIds) {
		this.inboundSegmentIds = inboundSegmentIds;
	}

	public String getOutboundSegmentCode() {
		return outboundSegmentCode;
	}

	public void setOutboundSegmentCode(String outboundSegmentCode) {
		this.outboundSegmentCode = outboundSegmentCode;
	}

	public String getOutboundSegmentIds() {
		return outboundSegmentIds;
	}

	public void setOutboundSegmentIds(String outboundSegmentIds) {
		this.outboundSegmentIds = outboundSegmentIds;
	}

	public String getOutboundSegDisplayName() {
		return outboundSegDisplayName;
	}

	public void setOutboundSegDisplayName(String outboundSegDisplayName) {
		this.outboundSegDisplayName = outboundSegDisplayName;
	}

	public String getInboundSegDisplayName() {
		return inboundSegDisplayName;
	}

	public void setInboundSegDisplayName(String inboundSegDisplayName) {
		this.inboundSegDisplayName = inboundSegDisplayName;
	}

	public String getOutboundDepartureDate() {
		return outboundDepartureDate;
	}

	public void setOutboundDepartureDate(String outboundDepartureDate) {
		this.outboundDepartureDate = outboundDepartureDate;
	}

	public String getInboundDepartureDate() {
		return inboundDepartureDate;
	}

	public void setInboundDepartureDate(String inboundDepartureDate) {
		this.inboundDepartureDate = inboundDepartureDate;
	}

	public boolean isInboudDefined() {
		return inboudDefined;
	}

	public void setInboudDefined(boolean inboudDefined) {
		this.inboudDefined = inboudDefined;
	}

	public boolean isOutboundDefined() {
		return outboundDefined;
	}

	public void setOutboundDefined(boolean outboundDefined) {
		this.outboundDefined = outboundDefined;
	}

	public List<String> getInSegList() {
		return inSegList;
	}

	public void setInSegList(List<String> inSegList) {
		this.inSegList = inSegList;
	}

	public List<String> getOutSegList() {
		return outSegList;
	}

	public void setOutSegList(List<String> outSegList) {
		this.outSegList = outSegList;
	}

	@Override
	public String toString() {
		return "PromoMetaTO [inboundSegmentCode=" + inboundSegmentCode + ", inboundSegmentIds=" + inboundSegmentIds
				+ ", outboundSegmentCode=" + outboundSegmentCode + ", outboundSegmentIds=" + outboundSegmentIds
				+ ", outboundSegDisplayName=" + outboundSegDisplayName + ", inboundSegDisplayName=" + inboundSegDisplayName
				+ ", outboundDepartureDate=" + outboundDepartureDate + ", inboundDepartureDate=" + inboundDepartureDate
				+ ", inboudDefined=" + inboudDefined + ", outboundDefined=" + outboundDefined + "]";
	}

}
