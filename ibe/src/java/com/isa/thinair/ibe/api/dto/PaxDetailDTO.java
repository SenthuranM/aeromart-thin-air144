package com.isa.thinair.ibe.api.dto;

import java.util.ArrayList;
import java.util.Collection;

public class PaxDetailDTO {

	private Collection<PassengerDTO> adultList = new ArrayList<PassengerDTO>();

	private Collection<PassengerDTO> childList = new ArrayList<PassengerDTO>();

	private Collection<PassengerDTO> infantList = new ArrayList<PassengerDTO>();

	public Collection<PassengerDTO> getAdultList() {
		return adultList;
	}

	public Collection<PassengerDTO> getChildList() {
		return childList;
	}

	public Collection<PassengerDTO> getInfantList() {
		return infantList;
	}

	public void setAdultList(Collection<PassengerDTO> adultList) {
		this.adultList = adultList;
	}

	public void setChildList(Collection<PassengerDTO> childList) {
		this.childList = childList;
	}

	public void setInfantList(Collection<PassengerDTO> infantList) {
		this.infantList = infantList;
	}

	public void addAdult(PassengerDTO adult) {
		if (adultList == null)
			this.adultList = new ArrayList<PassengerDTO>();
		this.adultList.add(adult);
	}

	public void addChild(PassengerDTO child) {
		if (childList == null)
			this.childList = new ArrayList<PassengerDTO>();
		this.childList.add(child);
	}

	public void addInfant(PassengerDTO infant) {
		if (infantList == null)
			this.infantList = new ArrayList<PassengerDTO>();
		this.infantList.add(infant);
	}
}
