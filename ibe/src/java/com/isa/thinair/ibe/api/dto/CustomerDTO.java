package com.isa.thinair.ibe.api.dto;

import java.util.List;

public class CustomerDTO {

	private int customerID;

	private String emailId;

	private String password;

	private String title;

	private String firstName;

	private String lastName;

	private String gender;

	private String alternativeEmailId;

	private String telephone;

	private String mobile;

	private String officeTelephone;

	private String dateOfBirth;

	private String fax;

	private String addressStreet;

	private String addressLine;

	private String zipCode;

	private String countryCode;

	private String nationalityCode;

	private String status;

	private String secretQuestion;

	private String secretAnswer;

	private String registrationDate;

	private String confirmationDate;

	private List customerQuestionaireDTO;

	private String city;

	private String loyaltyAccountNumber;

	private boolean loyaltyAccountExist;

	// Emergency contact details

	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private String emgnPhoneNo;

	private String emgnEmail;

	private String version;

	private String socialSiteCustId;

	private int socialCustomerTypeId;
	
	private String profilePicUrl;

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}

	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
		
		if(this.mobile == null || "".equals(this.mobile) || "--".equals(this.mobile)){
			this.mobile = this.telephone;
		}
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		if(mobile != null && !"".equals(mobile)){
			this.mobile = mobile;
		}
	}

	public String getOfficeTelephone() {
		return officeTelephone;
	}

	public void setOfficeTelephone(String officeTelephone) {
		this.officeTelephone = officeTelephone;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSecretQuestion() {
		return secretQuestion;
	}

	public void setSecretQuestion(String secretQuestion) {
		this.secretQuestion = secretQuestion;
	}

	public String getSecretAnswer() {
		return secretAnswer;
	}

	public void setSecretAnswer(String secretAnswer) {
		this.secretAnswer = secretAnswer;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(String confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public List getCustomerQuestionaireDTO() {
		return customerQuestionaireDTO;
	}

	public void setCustomerQuestionaireDTO(List customerQuestionaireDTO) {
		this.customerQuestionaireDTO = customerQuestionaireDTO;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLoyaltyAccountNumber() {
		return loyaltyAccountNumber;
	}

	public void setLoyaltyAccountNumber(String loyaltyAccountNumber) {
		this.loyaltyAccountNumber = loyaltyAccountNumber;
	}

	public boolean isLoyaltyAccountExist() {
		return loyaltyAccountExist;
	}

	public void setLoyaltyAccountExist(boolean loyaltyAccountExist) {
		this.loyaltyAccountExist = loyaltyAccountExist;
	}

	public String getEmgnTitle() {
		return emgnTitle;
	}

	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	public String getEmgnLastName() {
		return emgnLastName;
	}

	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	public String getEmgnPhoneNo() {
		return emgnPhoneNo;
	}

	public void setEmgnPhoneNo(String emgnPhoneNo) {
		this.emgnPhoneNo = emgnPhoneNo;
	}

	public String getEmgnEmail() {
		return emgnEmail;
	}

	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}

	public String getSocialSiteCustId() {
		return socialSiteCustId;
	}

	public void setSocialSiteCustId(String socialSiteCustId) {
		this.socialSiteCustId = socialSiteCustId;
	}

	public int getSocialCustomerTypeId() {
		return socialCustomerTypeId;
	}

	public void setSocialCustomerTypeId(int socialCustomerTypeId) {
		this.socialCustomerTypeId = socialCustomerTypeId;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

}
