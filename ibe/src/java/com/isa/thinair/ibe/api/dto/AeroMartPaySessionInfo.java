package com.isa.thinair.ibe.api.dto;

import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayUtil;

public class AeroMartPaySessionInfo {
	private String orderID;
	private String referenceID;
	private String languageCode;
	private String currencyCode;
	private String totalAmount;
	private String returnURL;
	private String errorURL;
	private String merchantID;
	private String merchantName;//aeromartPayOptions.jsp display purpose
	private String paymentAmount;
	private String paymentCurr;
	private String agentType;

	public AeroMartPaySessionInfo(String orderID, String referenceID, String languageCode, String currencyCode,
			String totalAmount, String returnURL, String errorURL, String merchantID, String agentType) {

		setOrderID(orderID);
		setReferenceID(referenceID);
		setCurrencyCode(currencyCode);
		setLanguageCode(languageCode);
		setTotalAmount(totalAmount);
		setReturnURL(returnURL);

		if (!StringUtil.isNullOrEmpty(errorURL)) {
			setErrorURL(errorURL);
		} else {
			setReturnURL(returnURL);
		}
		setMerchantID(merchantID);
		setMerchantName(AeroMartPayUtil.getMerchant(merchantID).getDisplayName());
		setAgentType(agentType);

	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	public String getErrorURL() {
		return errorURL;
	}

	public void setErrorURL(String errorURL) {
		this.errorURL = errorURL;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentCurr() {
		return paymentCurr;
	}

	public void setPaymentCurr(String paymentCurr) {
		this.paymentCurr = paymentCurr;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}
}
