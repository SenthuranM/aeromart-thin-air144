package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayUtil;

/**
 * @author Lahiru Sandeepa
 * 
 */
public class SearchPayOptionDTO {
	
	private String selectedCurrency;
	
	private BigDecimal selectedCurrencyPaymentAmount;
	
	private boolean isOfflineOptionsAllowed;
	
	private String ipAddress;
	
	private String agentType;
	
	private String entityCode;

	public SearchPayOptionDTO(AeroMartPaySessionInfo sessionInfo, String ipAddress) {
		
		this.selectedCurrency =sessionInfo.getCurrencyCode();
		this.selectedCurrencyPaymentAmount = new BigDecimal(sessionInfo.getTotalAmount());
		this.isOfflineOptionsAllowed = false;
		this.agentType = sessionInfo.getAgentType();
		this.ipAddress = ipAddress;
		this.entityCode =AeroMartPayUtil.getMerchant(sessionInfo.getMerchantID()).getEntityCode(); 		
	}

	/**
	 * @return the selectedCurrency
	 */
	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	/**
	 * @param selectedCurrency the selectedCurrency to set
	 */
	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	/**
	 * @return the selectedCurrencyPaymentAmount
	 */
	public BigDecimal getSelectedCurrencyPaymentAmount() {
		return selectedCurrencyPaymentAmount;
	}

	/**
	 * @param selectedCurrencyPaymentAmount the selectedCurrencyPaymentAmount to set
	 */
	public void setSelectedCurrencyPaymentAmount(BigDecimal selectedCurrencyPaymentAmount) {
		this.selectedCurrencyPaymentAmount = selectedCurrencyPaymentAmount;
	}

	/**
	 * @return the isOfflineOptionsAllowed
	 */
	public boolean isOfflineOptionsAllowed() {
		return isOfflineOptionsAllowed;
	}

	/**
	 * @param isOfflineOptionsAllowed the isOfflineOptionsAllowed to set
	 */
	public void setOfflineOptionsAllowed(boolean isOfflineOptionsAllowed) {
		this.isOfflineOptionsAllowed = isOfflineOptionsAllowed;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the agentType
	 */
	public String getAgentType() {
		return agentType;
	}

	/**
	 * @param agentType the agentType to set
	 */
	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	/**
	 * @return the entityCode
	 */
	public String getEntityCode() {
		return entityCode;
	}

	/**
	 * @param entityCode the entityCode to set
	 */
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
	
	
	
	
	

}
