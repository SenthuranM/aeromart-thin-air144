package com.isa.thinair.ibe.api.dto;

public class URLTO {

	private String nonSecurePath = "";
	private String securePath = "";
	private String homeURL = "";
	private String userIp = "";
	private boolean isCustomer = false;

	public String getNonSecurePath() {
		return nonSecurePath;
	}

	public void setNonSecurePath(String nonSecurePath) {
		this.nonSecurePath = nonSecurePath;
	}

	public String getSecurePath() {
		return securePath;
	}

	public void setSecurePath(String securePath) {
		this.securePath = securePath;
	}

	public String getHomeURL() {
		return homeURL;
	}

	public void setHomeURL(String homeURL) {
		this.homeURL = homeURL;
	}

	public String getUserIp() {
		return userIp;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public boolean isCustomer() {
		return isCustomer;
	}

	public void setCustomer(boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

}
