package com.isa.thinair.ibe.api.dto.v2;

public class CardInfo {

	private int cardTypeID;

	private String displayName;

	private String paymentMode;

	public int getCardTypeID() {
		return cardTypeID;
	}

	public void setCardTypeID(int cardTypeID) {
		this.cardTypeID = cardTypeID;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
