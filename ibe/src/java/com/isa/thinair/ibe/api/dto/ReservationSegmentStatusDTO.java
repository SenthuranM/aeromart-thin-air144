package com.isa.thinair.ibe.api.dto;

import java.util.Date;

public class ReservationSegmentStatusDTO {

	private String flightSegmentRefNo;

	private boolean modifiable;

	private String status;

	private Date modifyTillBufferDateTime;

	private Date cancelTillBufferDateTime;

	private Date modifyTillFlightClosureDateTime;

	public String getFlightSegmentRefNo() {
		return flightSegmentRefNo;
	}

	public void setFlightSegmentRefNo(String flightSegmentRefNo) {
		this.flightSegmentRefNo = flightSegmentRefNo;
	}

	public boolean isModifiable() {
		return modifiable;
	}

	public void setModifiable(boolean modifiable) {
		this.modifiable = modifiable;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getModifyTillBufferDateTime() {
		return modifyTillBufferDateTime;
	}

	public void setModifyTillBufferDateTime(Date modifyTillBufferDateTime) {
		this.modifyTillBufferDateTime = modifyTillBufferDateTime;
	}

	public Date getCancelTillBufferDateTime() {
		return cancelTillBufferDateTime;
	}

	public void setCancelTillBufferDateTime(Date cancelTillBufferDateTime) {
		this.cancelTillBufferDateTime = cancelTillBufferDateTime;
	}

	public Date getModifyTillFlightClosureDateTime() {
		return modifyTillFlightClosureDateTime;
	}

	public void setModifyTillFlightClosureDateTime(Date modifyTillFlightClosureDateTime) {
		this.modifyTillFlightClosureDateTime = modifyTillFlightClosureDateTime;
	}

}
