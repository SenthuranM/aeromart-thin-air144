package com.isa.thinair.ibe.api.dto;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class SegInfoDTO implements Comparable<SegInfoDTO> {

	private static final String IMAGE_PATH_SPLITONE = "../images/LogoThumbnail" + AppSysParamsUtil.getStaticFileSuffix();

	private static final String IMAGE_PATH_SPLITTWO = ".jpg";

	private String departureDate;

	private String departureTime;
	
	private String departureDateValue;

	private String arrivalDate;
	
	private String arrivalDateValue;
	
	private String arrivalTime;

	private String flightNumber;

	private String segmentName;

	private String segmentCode;

	private String segmentCodeOnly;

	private String carrierCode;

	private Integer segmentId;

	private String flightRefNumber;

	private String carrierImagePath;

	private long departureTimeLong;

	private long departureTimeZuluLong;

	private String system;

	private long arrivalTimeLong;

	private long arrivalTimeZuluLong;

	private String segmentShortCode;

	private boolean returnFlag;

	private String duration;

	private String departureTerminalName;

	private String arrivalTerminalName;

	private String flightSeqNo;

	private boolean domesticFlight;

	private String depZuluDateTimeJson;

	private String arrZuluDateTimeJson;
	
	private String depLocalDateTimeJson;

	private String arrLocalDateTimeJson;

	private Integer journeySequence;
	
	private String csOcCarrierCode;
	
	private String flightModelNumber;
	
	private String flightStopOverDuration;

	public String getDepartureDate() {
		return departureDate;
	}

	public boolean isDomesticFlight() {
		return domesticFlight;
	}

	public void setDomesticFlight(boolean domesticFlight) {
		this.domesticFlight = domesticFlight;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
		setCarrierImagePath(carrierCode);
	}

	public Integer getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public String getCarrierImagePath() {
		return carrierImagePath;
	}

	public void setCarrierImagePath(String carrierCode) {
		this.carrierImagePath = IMAGE_PATH_SPLITONE + carrierCode + IMAGE_PATH_SPLITTWO;
	}

	public long getDepartureTimeLong() {
		return departureTimeLong;
	}

	public void setDepartureTimeLong(long departureTimeLong) {
		this.departureTimeLong = departureTimeLong;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public long getArrivalTimeLong() {
		return arrivalTimeLong;
	}

	public void setArrivalTimeLong(long arrivalTimeLong) {
		this.arrivalTimeLong = arrivalTimeLong;
	}

	public void setSegmentShortCode(String segmentShortCode) {
		this.segmentShortCode = segmentShortCode;
	}

	public String getSegmentShortCode() {
		return this.segmentShortCode;
	}

	public boolean getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public long getDepartureTimeZuluLong() {
		return departureTimeZuluLong;
	}

	public void setDepartureTimeZuluLong(long departureTimeZuluLong) {
		this.departureTimeZuluLong = departureTimeZuluLong;
	}

	public long getArrivalTimeZuluLong() {
		return arrivalTimeZuluLong;
	}

	public void setArrivalTimeZuluLong(long arrivalTimeZuluLong) {
		this.arrivalTimeZuluLong = arrivalTimeZuluLong;
	}

	public int compareTo(SegInfoDTO other) {
		long diff = (this.getDepartureTimeLong() - other.getDepartureTimeLong());
		// to handle scenario where difference in dates overshoot the int range
		if (diff < 0) {
			return -1;
		} else if (diff > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * @return The arrival terminal name.
	 */
	public String getArrivalTerminalName() {
		return arrivalTerminalName;
	}

	/**
	 * @param arrivalTerminalName
	 *            The arrival terminal name to set.
	 */
	public void setArrivalTerminalName(String arrivalTerminalName) {
		this.arrivalTerminalName = arrivalTerminalName;
	}

	/**
	 * @return the departure terminal name.
	 */
	public String getDepartureTerminalName() {
		return departureTerminalName;
	}

	/**
	 * @param departureTerminalName
	 *            the departure terminal name to set.
	 */
	public void setDepartureTerminalName(String departureTerminalName) {
		this.departureTerminalName = departureTerminalName;
	}

	public String getFlightSeqNo() {
		return flightSeqNo;
	}

	public void setFlightSeqNo(String flightSeqNo) {
		this.flightSeqNo = flightSeqNo;
	}

	public String getDepZuluDateTimeJson() {
		return depZuluDateTimeJson;
	}

	public void setDepZuluDateTimeJson(String depZuluDateTimeJson) {
		this.depZuluDateTimeJson = depZuluDateTimeJson;
	}

	public String getArrZuluDateTimeJson() {
		return arrZuluDateTimeJson;
	}

	public void setArrZuluDateTimeJson(String arrZuluDateTimeJson) {
		this.arrZuluDateTimeJson = arrZuluDateTimeJson;
	}

	public String getSegmentCodeOnly() {
		return segmentCodeOnly;
	}

	public void setSegmentCodeOnly(String segmentCodeOnly) {
		this.segmentCodeOnly = segmentCodeOnly;
	}

	public Integer getJourneySequence() {
		return journeySequence;
	}

	public void setJourneySequence(Integer journeySequence) {
		this.journeySequence = journeySequence;
	}
	
	public String getDepartureDateValue() {
		return departureDateValue;
	}

	public void setDepartureDateValue(String departureDateValue) {
		this.departureDateValue = departureDateValue;
	}

	public String getArrivalDateValue() {
		return arrivalDateValue;
	}

	public void setArrivalDateValue(String arrivalDateValue) {
		this.arrivalDateValue = arrivalDateValue;
	}

	public String getDepLocalDateTimeJson() {
		return depLocalDateTimeJson;
	}

	public void setDepLocalDateTimeJson(String depLocalDateTimeJson) {
		this.depLocalDateTimeJson = depLocalDateTimeJson;
	}

	public String getArrLocalDateTimeJson() {
		return arrLocalDateTimeJson;
	}

	public void setArrLocalDateTimeJson(String arrLocalDateTimeJson) {
		this.arrLocalDateTimeJson = arrLocalDateTimeJson;
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	public String getFlightStopOverDuration() {
		return flightStopOverDuration;
	}

	public void setFlightStopOverDuration(String flightStopOverDuration) {
		this.flightStopOverDuration = flightStopOverDuration;
	}	

}
