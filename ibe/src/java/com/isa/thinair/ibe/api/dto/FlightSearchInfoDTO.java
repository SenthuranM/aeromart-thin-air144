package com.isa.thinair.ibe.api.dto;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.PaxCategoryTO;

public class FlightSearchInfoDTO {

	private List currencyInfo;

	private List airportList;

	private Collection<PaxCategoryTO> paxCategories;

	private Collection<FareCategoryTO> fareTypes;

	private List cos;

	private List groundStationList;

	public List getCurrencyInfo() {
		return currencyInfo;
	}

	public void setCurrencyInfo(List currencyInfo) {
		this.currencyInfo = currencyInfo;
	}

	public List getAirportList() {
		return airportList;
	}

	public void setAirportList(List airportList) {
		this.airportList = airportList;
	}

	public List getCos() {
		return cos;
	}

	public void setCos(List cos) {
		this.cos = cos;
	}

	public Collection<FareCategoryTO> getFareTypes() {
		return fareTypes;
	}

	public void setFareTypes(Collection<FareCategoryTO> fareTypes) {
		this.fareTypes = fareTypes;
	}

	public Collection<PaxCategoryTO> getPaxCategories() {
		return paxCategories;
	}

	public void setPaxCategories(Collection<PaxCategoryTO> paxCategories) {
		this.paxCategories = paxCategories;
	}

	/**
	 * @return the groundStationList
	 */
	public List getGroundStationList() {
		return groundStationList;
	}

	/**
	 * @param groundStationList
	 *            the groundStationList to set
	 */
	public void setGroundStationList(List groundStationList) {
		this.groundStationList = groundStationList;
	}

}
