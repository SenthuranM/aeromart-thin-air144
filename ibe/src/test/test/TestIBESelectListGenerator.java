/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package test;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.velocity.util.StringUtils;

import sun.java2d.pipe.SpanShapeRenderer.Simple;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.IAvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.IFaresChargesAndSeats;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.platform.api.util.PlatformTestCase;

import com.isa.thinair.webplatform.core.commons.MapGenerator;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 *
 */
public class TestIBESelectListGenerator extends PlatformTestCase {

	public static String modulename = "airreservation";
	
	  reservationQDelegate = ModuleServiceLocator.getReservationQueryBD();
	  reservationDelegate = ModuleServiceLocator.getReservationBD();
	  customerDelegate = ModuleServiceLocator.getCustomerBD();
	
	protected void setUp() throws Exception {
		super.setUp();

	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testCreateAirportsListDesc() throws ModuleException {
		System.out.println("testing page Starting ..............................................");
		
		System.out.println("Testing New");
		
		//Test_AvailabilitySearch();
		//Test_CancelReservation();
		//Test_CreateReservation();
		//Test_UpdateReservation();
		//Test_MapGenerator();
		Test_SelectListGenerator();
		//Test_Credit();
		//Test_EarlyReservation();
		//Test_ReservationDetail();
		//Test_Reservation();
		//Test_Customer();
		
		//Test_CalendarSearch();
		
		
		System.out.println("testing page completed..............................................");
	}
	
	// ----------------------------------------------------- testing ---------------------------------------------
	/*
	 * Calendar Search
	 */
	private static void Test_CalendarSearch()throws ModuleException{
		/*
		StringBuffer strbData = new StringBuffer();
		
		System.out.println("test calendar");
		
		int intDVariance = 3;
		int intRVariance = 3;
		String strDeptDate = "01/20/2006";
		String strRetuDate = "01/20/2006";
		
		HashMap hashMapOutCalendar = createHashMap(strDeptDate, intDVariance);
		System.out.println("--- out---- testing ------------------------------------");
		HashMap hashMapRetCalendar = createHashMap(strRetuDate, intRVariance);
		
		// ------------------- Search
		
		Date dateFrom = null;
		Date dateReturn = null;
		
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
		
    	try {
    		SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
    		dateFrom=df.parse("01/20/2006");
    		dateReturn=df.parse("01/20/2006");
	    }
	    catch (Exception err) {
	    	
	    }
	    
	    SimpleDateFormat smpdtDay = new SimpleDateFormat("EEE,");
		//SimpleDateFormat smpdtDate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat smpdtDispDate = new SimpleDateFormat("dd MMM yy");
		//SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
		
	    AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		availableFlightSearchDTO.setAdultCount(1);
		availableFlightSearchDTO.setInfantCount(0);
		availableFlightSearchDTO.setFromAirport("CMB");
		availableFlightSearchDTO.setToAirport("SHJ");
		availableFlightSearchDTO.setDepartureVariance(0);
		availableFlightSearchDTO.setArrivalVariance(0);
		availableFlightSearchDTO.setDepatureDate(dateFrom);
		availableFlightSearchDTO.setReturnDate(dateReturn);
		availableFlightSearchDTO.setReturnFlag(true);
		availableFlightSearchDTO.setCabinClassCode("Y");
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
		
		String strFareID = "";
		int intCount = 0 ; 
		AvailableFlightDTO availableFlightDTO = reservationQDelegate.getAvailableFlightsWithAllFares(availableFlightSearchDTO);
		String strType = "D";
		String ArrayName = "arrData";
		
		
		// -----------------------------
		String strDate = "";
		String strFltNo = "";
		String strFltID = "";
		String strData = "";
		
		if (availableFlightDTO != null){
			smpdtDate = new SimpleDateFormat("MM/dd/yyyy");
			Collection collFlights;
			Iterator iterFlights;
			
			if (strType == "D"){
				collFlights = availableFlightDTO.getOutboundSingleFlights();
			}else{
				collFlights = availableFlightDTO.getInboundSingleFlights();
			}
			if (collFlights != null){
				System.out.println("in ");			
				iterFlights = collFlights.iterator();
				while (iterFlights.hasNext()){
					AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment)iterFlights.next();
					IFaresChargesAndSeats iFaresChargesAndSeats = (IFaresChargesAndSeats)availableFlightSegment;
					//if (availableFlightSegment.isSeatsAvailable()){
						FlightSegmentDTO flightSegmentDTO = availableFlightSegment.getFlightSegmentDTO();
						strDate= smpdtDate.format(flightSegmentDTO.getDepartureDateTime());
						strFltNo = flightSegmentDTO.getFlightNumber();
						strFltID =  flightSegmentDTO.getSegmentId().toString();
						if (iFaresChargesAndSeats.getFareAmount() != null){
							double dblFareAmt = iFaresChargesAndSeats.getFareAmount().doubleValue();
							
							strData = strDate + "," + strFltNo + "," + strFltID + "," + Double.toString(dblFareAmt);
							compareHashValues(strDate, strData, hashMapOutCalendar);
						}
					//}
				}
			}else{
				if (strType == "D"){
					collFlights = availableFlightDTO.getOutboundConnectedFlights();
				}else{
					collFlights = availableFlightDTO.getInboundConnectedFlights();
				}
				iterFlights = collFlights.iterator();
				intCount = 0 ; 
				while (iterFlights.hasNext()){
					
					AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight)iterFlights.next();
					IFaresChargesAndSeats iFaresChargesAndSeats = (IFaresChargesAndSeats)availableConnectedFlight;
					//if (availableConnectedFlight.isSeatsAvailable()){
						if (iFaresChargesAndSeats.getFareAmount() != null){
							double dblFareAmt = iFaresChargesAndSeats.getFareAmount().doubleValue();
							
							Collection fareChargeSegments = iFaresChargesAndSeats.getFareChargeSegmentsCollection();
							Iterator iterfareChagesSegments = fareChargeSegments.iterator();
							while (iterfareChagesSegments.hasNext()){
								FareChargeSegmentsDTO fareChargeSegmentsDTO = (FareChargeSegmentsDTO)fareChargeSegments.iterator().next();
								if (strFareID != ""){strFareID+= "/";}
								strFareID += fareChargeSegmentsDTO.getFare().getFareId();
							}
							
							Collection collAvailableFlightSegments = availableConnectedFlight.getAvailableFlightSegments();
							Iterator iterCollAvailfFlights = collAvailableFlightSegments.iterator();
							while (iterCollAvailfFlights.hasNext()) {
								AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment)iterCollAvailfFlights.next();
								FlightSegmentDTO flightSegmentDTO = availableFlightSegment.getFlightSegmentDTO();
								
								strDate = smpdtDate.format(flightSegmentDTO.getDepartureDateTime());
								strFltNo = flightSegmentDTO.getFlightNumber();
								strFltID =  flightSegmentDTO.getSegmentId().toString();
								break;
							}
							strData = strDate + "," + strFltNo + "," + strFltID + "," + Double.toString(dblFareAmt);
							compareHashValues(strDate, strData, hashMapOutCalendar);
						}
					//}
				}
			}
		}
		
		strbData.append(readHashMap(strDeptDate,intDVariance, hashMapOutCalendar, "arrData"));
		strbData.append(readHashMap(strRetuDate,intRVariance, hashMapRetCalendar, "arrRata"));
		
		System.out.println(strbData.toString());
		*/
	}
	
	/*
	 * Custoemr Information
	 */
	private static void Test_Customer() throws ModuleException{
		Customer cust = customerDelegate.getCustomer("b");
		System.out.println("Email Address " + cust.getFirstName());
		System.out.println("arrProfile = new Array('" + cust.getTitle() + "'," + 
								"'" + cust.getFirstName() + "'," + 
								"'" + cust.getLastName() + "'," + 
								"''," + 
								"''," + 
								"'"  + cust.getOfficeTelephone() + "'," + 
								"'"  + cust.getMobile() + "'," + 
								"''," + 
								"'"  + cust.getFax() + "'," + 
								"'"  + cust.getStatus() + "'," + 
								"'"  + cust.getEmailId() + "'," +
								"'"  + cust.getNationalityCode() + "');)");
	}

	/*
	 * Reservation
	 */
	private static void Test_Reservation() throws ModuleException{
		/*
		Reservation reservation = new Reservation();
		reservation = reservationDelegate.getReservation("123456", false, true);
		System.out.println("In");
		System.out.println(reservation == null);
		
		Collection setSegment = reservation.getSegmentsView();
		if (setSegment != null){
			Iterator iterSegment = setSegment.iterator();
			int intCount = 0 ; 
			while (iterSegment.hasNext()){
				ReservationSegmentDTO reservationSegmentDto = (ReservationSegmentDTO)iterSegment.next();
				System.out.println(" flt No " +reservationSegmentDto.getFlightNo());
				System.out.println(" Segment " + reservationSegmentDto.getSegmentCode());
				System.out.println(" Return Flag " +reservationSegmentDto.getReturnFlag());
				//reservationSegment
				
				System.out.println(ReservationInternalConstants.PassengerType.ADULT.toString());
				System.out.println(ReservationInternalConstants.PassengerType.INFANT.toString());
			}
		}
		*/
	}

	/*
	 * Reservation PNR
	 */
	private static void Test_ReservationDetail() throws ModuleException{
		/*
		Reservation reservation = reservationDelegate.getReservation("123456", false, true);
		Collection collSegment = reservation.getSegmentsView();
		SimpleDateFormat smpdtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm ");
		if (collSegment != null){
			Iterator iterSegment = collSegment.iterator();
			StringBuffer strbOutFlt = new StringBuffer();
			StringBuffer strbRetFlt = new StringBuffer();
			
			int intOCount = 0 ;
			int intRCount = 0 ;
			while (iterSegment.hasNext()){
				ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO)iterSegment.next();
				
				String strSegmentID = reservationSegmentDTO.getPnrSegId().toString();
				String strSegment   = JavaScriptGenerator.nullConvertToString(reservationSegmentDTO.getSegmentCode());
				String strFlightNo  = JavaScriptGenerator.nullConvertToString(reservationSegmentDTO.getFlightNo());
				String strDtDept	= smpdtFormat.format(reservationSegmentDTO.getDepartureDate());
				String strDtArr		= smpdtFormat.format(reservationSegmentDTO.getArrivalDate());
				System.out.println(" Return *" + reservationSegmentDTO.getReturnFlag() + "*");
				
				System.out.println(reservationSegmentDTO.getReturnFlag() == ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE.toString());

				System.out.println(" const *" + ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE + "*");
				
				if (reservationSegmentDTO.getReturnFlag().equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE.toString())){
					strbOutFlt.append("arrRFlt[" + intRCount + "]	= new Array('" + strSegmentID + "'," +
							"'" + strSegment + "'," +
							"'" + strFlightNo + "','D'," + 
							"'" + strDtDept + "', 'A'," +
							"'" + strDtArr +"');");
					intRCount++;
					
System.out.println("R");					
				}else{
System.out.println("o");					
					strbOutFlt.append("arrOFlt[" + intOCount + "]	= new Array('" + strSegmentID + "'," +
																				"'" + strSegment + "'," +
																				"'" + strFlightNo + "','D'," + 
																				"'" + strDtDept + "', 'A'," +
																				"'" + strDtArr +"');");
					intOCount++;
				}
			}
			
			System.out.println("------------------------------------------");
			System.out.println(strbOutFlt.toString());
			System.out.println("------------------------------------------");
			System.out.println(strbRetFlt.toString());
			System.out.println("------------------------------------------");
		}
		
		System.out.println("Out");
		*/
	}

	/*
	 * Early Reservaiton
	 */
	private static void Test_EarlyReservation() throws ModuleException{
		Date dtNow = new Date();
		Collection reservationDtoCols = reservationQDelegate.getAfterReservations(45, dtNow, null);
		if (reservationDtoCols == null) {
			System.out.println("null");
		} else {
			System.out.println("not null");
			System.out.println(reservationDtoCols.size());
			Iterator iterator = reservationDtoCols.iterator();
			ReservationDTO reservationDTO;
			while (iterator.hasNext()) {
				reservationDTO = (ReservationDTO) iterator.next();
				
				Collection coll = reservationDTO.getSegments();
				Iterator iteratorSegement = coll.iterator();
				ReservationSegmentDTO segmentDTO ; 
				while(iteratorSegement.hasNext()){
					segmentDTO = (ReservationSegmentDTO)iteratorSegement.next();
					System.out.println(segmentDTO.getDepartureDate());
					System.out.println(segmentDTO.getArrivalDate());
					System.out.println(segmentDTO.getSegmentCode());
				}
				
				
				//System.out.println(" in reservation " + reservationDTO.gets());
				System.out.println(" in reservation " + reservationDTO.getPnr());
			}
		}
	}

	/*
	 * Credits
	 */
	private static void Test_Credit() throws ModuleException{
		Collection reservationCreditColls = reservationQDelegate.getReservationCredits(45);
		if (reservationCreditColls != null){
			PaxCreditDTO paxCreditDTO;
			for (Iterator iter = reservationCreditColls.iterator(); iter.hasNext();) {
				paxCreditDTO = (PaxCreditDTO) iter.next();
				System.out.println(paxCreditDTO.getPnr());
			}
			System.out.println(reservationCreditColls.size());
		}else{
			System.out.println("Null");
		}
	}

	/*
	 * Select List Generator
	 */
	private static void Test_SelectListGenerator() throws ModuleException{
		//System.out.println(SelectListGenerator.createCurrencyActiveDescList());
		//String html = SelectListGenerator.createAirportODDesc();
		String html = SelectListGenerator.createSecreatQuestion();
		System.out.println("html:" + html);
	}

	/*
	 * Map Generator
	 */
	private static void Test_MapGenerator() throws ModuleException{
		Map mapNational = MapGenerator.getNationalityMap();
		System.out.println("Nationaltiy " + mapNational.get("1"));
	}
	
	/*
	 * Update Reservation
	 */
	private static void Test_UpdateReservation() throws ModuleException{
		/*
		Reservation reservation = reservationDelegate.getReservation("QWERTY", false, false);
		ReservationContactInfo reservationContactInfo = new ReservationContactInfo();
		System.out.println("1");	

		int integer1 = 1783;
		
		reservationContactInfo.setFax("1234567890");
		reservationContactInfo.setFirstName("First Name");
		reservationContactInfo.setLastName("Last Name");
		reservationContactInfo.setCity("City");
		reservationContactInfo.setCountryCode("AE");
		reservationContactInfo.setCustomerId(new Integer(integer1));
		reservationContactInfo.setEmail("sdfsdf@dd.com");
		reservationContactInfo.setMobileNo("123");
		reservationContactInfo.setNationalityCode("2");
		reservationContactInfo.setPhoneNo("1111111111");
		reservationContactInfo.setState("Emirates");
		reservationContactInfo.setStreetAddress1("111111111111111111111111");
		reservationContactInfo.setStreetAddress2("222222222222222");
		
		System.out.println("2");		
		reservation.setContactInfo(reservationContactInfo);
	    //reservation.setPassengers(new HashSet());
		
		System.out.println("3");		
		//reservation.setPnr("QWERTY");
		
		System.out.println("4");		
		//reservation.setVersion(28);
		
		System.out.println("5");		

		try {
			reservationDelegate.updateReservation(reservation);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		System.out.println("6");
		*/
	}

	/*
	 * Create Reservation
	 */
	private static void Test_CreateReservation() throws ModuleException{
		/*
		Date dtNow = null;
		IReservation iReservation = new ReservationAssembler();
		
		// Passengers ----------------------------------------------------------------
		iReservation.addAdult("First Name","Last Name","Title",dtNow,1,1,100,100);
		iReservation.addInfant("FirstN","Last Name","Mr",dtNow,1,1,1,100);
		iReservation.addParent("FirstName","LastName","Mr",dtNow,1,1,1,1,1);
		
		// Contact Information -------------------------------------------------------
		ReservationContactInfo reservationContactInfo = new ReservationContactInfo();
		reservationContactInfo.setFirstName("22");
		reservationContactInfo.setLastName("2");
		reservationContactInfo.setStreetAddress1("3");
		reservationContactInfo.setStreetAddress2("4");
		reservationContactInfo.setState("5");
		reservationContactInfo.setCity("6");
		reservationContactInfo.setCountryCode("AE");
		reservationContactInfo.setMobileNo("8");
		reservationContactInfo.setPhoneNo("9");
		reservationContactInfo.setFax("10");
		reservationContactInfo.setEmail("11");
		reservationContactInfo.setNationalityCode("1");
		iReservation.addContactInfo("", reservationContactInfo);
		
		// Flight Information ------------------------------------------------------
		//iReservation.addOutgoingSegment(2655,1,1);
		//iReservation.addReturnSegment(2656,1,1);
		
		// Fare Information --------------------------------------------------------
		//iReservation.addFares()

		// Payment Information
		Collection paymentInfo = new ArrayList();
		Date dateExpiry =null;
    	try {
    		SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
    		dateExpiry=df.parse("10/01/2005");
	    }
	    catch (Exception err) {
	    	
	    }
	    System.out.println ("Date: "+dateExpiry); // will print the Date object
	    
		CardPaymentInfo cardInfo = new CardPaymentInfo();
		cardInfo.setTotalAmount(200);
		cardInfo.setNo("4111111111111111");
		cardInfo.setSecurityCode("232");
		cardInfo.setEDate(dateExpiry);
		cardInfo.setType("VISA");
		cardInfo.setName("BILL OMAR");
		//cardInfo.setVersion(reservation.getVersion());
		paymentInfo.add(cardInfo);
		//iReservation.  payment information
		 */
	}

	/*
	 * Cancel Reservation
	 */
	private static void Test_CancelReservation() throws ModuleException{
		/*
		System.out.println("Cancel Reservation");
		reservationDelegate.cancelReservation("ABCDEF",0);
		System.out.println("Cancel Reservation done");
		*/
	}

	/*
	 * Availability Search 
	 */
	private static void Test_AvailabilitySearch() throws ModuleException{
		// Availability Search
		StringBuffer strbData = new StringBuffer();
		String ArrayName = "arrOFlt";
		Date dateFrom = null;
		Date dateReturn = null;
		
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
		
    	try {
    		SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
    		dateFrom=df.parse("01/03/2006");
    		dateReturn=df.parse("01/04/2006");
	    }
	    catch (Exception err) {
	    	
	    }
	    
	    AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		availableFlightSearchDTO.setAdultCount(1);
		availableFlightSearchDTO.setInfantCount(0);
		availableFlightSearchDTO.setFromAirport("CMB");
		availableFlightSearchDTO.setToAirport("SHJ");
		availableFlightSearchDTO.setDepartureVariance(0);
		availableFlightSearchDTO.setArrivalVariance(0);
		availableFlightSearchDTO.setDepatureDate(dateFrom);
		availableFlightSearchDTO.setReturnDate(dateReturn);
		availableFlightSearchDTO.setReturnFlag(true);
		availableFlightSearchDTO.setCabinClassCode("Y"); // future use
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
		
		AvailableFlightDTO availableFlightDTO = reservationQDelegate.getAvailableDayFlightsWithSelectedFare(availableFlightSearchDTO);
		if (!availableFlightDTO.equals(null)){
			com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO selectedFlightDTO; 
			selectedFlightDTO = availableFlightDTO.getSelectedFlight();
			strbData.append(createFlightArrayHTML("D", availableFlightDTO, "arrOFlt"));
			//strbData.append(createFlightArrayHTML("R", availableFlightDTO, "arrRFlt"));
			System.out.println("fare quote");
			strbData.append(createFareQuoteHTML(selectedFlightDTO));
		}
		
		System.out.println("*********" + strbData.toString() + "*****************");
	}
	
	
	// Testing ----------------------------------------------------------------------------------------------
	
	public static String createFareQuoteHTML(com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO selectedFlightDTO){
		StringBuffer strbData = new StringBuffer();
		double dblAFare = 0;
		double dblACharges = 0 ;
		double dblIFare = 0 ;
		double dblICharges = 0 ;
		
		if (selectedFlightDTO != null){
			IAvailableFlightSegment iAvailableFlightSegmentOut = selectedFlightDTO.getInboundFlight();
			IFaresChargesAndSeats faresChargesAndSeats = (IFaresChargesAndSeats)selectedFlightDTO;
	}
		
		strbData.append("dblAFare = " + Double.toString(dblAFare) + ";");
		strbData.append("dblACharges = " + Double.toString(dblACharges) + ";");
		strbData.append("dblIFare = " + Double.toString(dblIFare) + ";");
		strbData.append("dblICharges = " + Double.toString(dblICharges) + ";");
		return strbData.toString();
	}
	
	public static String createFlightArrayHTML(String strRouteType, AvailableFlightDTO availableFlightDTO, String arrayName){
		/*
		StringBuffer strbData = new StringBuffer();
		
		Collection collFlights;
		Iterator iterFlights;

		String strValues= "";
		String strDDate = "";
		String strDDisp	= "";
		String strDDay	= "";
		String strDTime	= "";
		String strADay	= "";
		String strADate = "";
		String strADisp	= "";
		String strATime	= "";
		String StrLoc	= "";
		String strFltNo = "";
		String strAFare	= "";
		String strAChg 	= "";
		String strIFare	= "";
		String strIChg 	= "";
		String strSegID = "";
		String strFltID = "";
		String strFareID= "";
		String strPoint = "";
		boolean blnFareAvail = false;
		int intFareID ;
		int intCountCon;
		double dblFareAmt;
		
		SimpleDateFormat smpdtDay = new SimpleDateFormat("EEE,");
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat smpdtDispDate = new SimpleDateFormat("dd MMM yy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
	
		int intCount; 
		
		if (strRouteType.equals("D")){
			collFlights = availableFlightDTO.getOutboundSingleFlights();
		}else{
			collFlights = availableFlightDTO.getInboundSingleFlights();
		}
		Collection collFlights2 = availableFlightDTO.getOutboundConnectedFlights();
		System.out.println(" Single flights " + collFlights == null);
		System.out.println("connecting flight " + collFlights2 == null);
		if (!collFlights.equals(null)){
			iterFlights = collFlights.iterator();
			intCount = 0 ; 
			
			while (iterFlights.hasNext()) {
				strValues	= "";
				strDDay		= "";
				strDDate  	= "";
				strDDisp	= "";
				strDTime	= "";
				strADay		= "";
				strADate 	= "";
				strADisp	= "";
				strATime	= "";
				StrLoc		= "";
				strFltNo 	= "";
				strAFare	= "";
				strAChg 	= "";
				strIFare	= "";
				strIChg 	= "";
				intFareID	= 0;
				strSegID 	= "";
				strFltID 	= "";
				dblFareAmt	= 0;
				intCountCon = 0;
				strPoint 	= "";
				blnFareAvail = false;
				
				AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment)iterFlights.next();
				System.out.println("FLAG ---------------- " + availableFlightSegment.isSeatsAvailable());
				
				//if (availableFlightSegment.isSeatsAvailable()){
					IFaresChargesAndSeats iFaresChargesAndSeats = (IFaresChargesAndSeats)availableFlightSegment;
					if (iFaresChargesAndSeats.getFareType() != FareTypes.NO_FARE) {
						dblFareAmt = iFaresChargesAndSeats.getFareAmount().doubleValue();
						
//						Collection fareChargeSegments = iFaresChargesAndSeats.getFareChargeSegmentsCollection();
//						FareChargeSegmentsDTO fareChargeSegmentsDTO = (FareChargeSegmentsDTO)fareChargeSegments.iterator().next();
//						intFareID = fareChargeSegmentsDTO.getFare().getFareId();
					}
					
					strAFare = Double.toString(dblFareAmt); // new method required
					// strChg = availableFlightSegment.getCharges() // new method required
					
					FlightSegmentDTO flightSegmentDTO = availableFlightSegment.getFlightSegmentDTO();
					
					System.out.println("Departure Date " + flightSegmentDTO.getDepartureDateTime());
					
					strDDay = smpdtDay.format(flightSegmentDTO.getDepartureDateTime());
					strDDate = smpdtDate.format(flightSegmentDTO.getDepartureDateTime());
					strDTime = smpdtTime.format(flightSegmentDTO.getDepartureDateTime());
					strDDisp = smpdtDispDate.format(flightSegmentDTO.getDepartureDateTime());
					
					strADay = smpdtDay.format(flightSegmentDTO.getArrivalDateTime());
					strADate = smpdtDate.format(flightSegmentDTO.getArrivalDateTime());
					strATime = smpdtTime.format(flightSegmentDTO.getArrivalDateTime());
					strADisp = smpdtDispDate.format(flightSegmentDTO.getArrivalDateTime());
					
					strFltNo = flightSegmentDTO.getFlightNumber();
					strValues = Integer.toString(intCount)  ; 
					strSegID  =	flightSegmentDTO.getFlightId().toString(); 
					strFltID  =  flightSegmentDTO.getSegmentId().toString();
					
					StrLoc = ""; //flightSegmentDTO.getSegmentCode();
					String[] arrSegments = flightSegmentDTO.getSegmentCode().split("/");
					strbData.append(arrayName + "[" + intCount + "] = new Array();");
				    strbData.append(arrayName + "[" + intCount + "][0]= '" + strValues + "';");
				    strbData.append(arrayName + "[" + intCount + "][1]= new Array();");
				    		
					for (int i = 0 ; i < arrSegments.length ; i++){
						StrLoc = getSegementDetails(arrSegments[i]);
						strPoint = "Via";
						if (i == 0){strPoint = "Depart";}
						if (i == (arrSegments.length - 1)){strPoint = "Arrive";}
						
						strbData.append(arrayName + "[" + intCount + "][1][" + i + "] = new Array();");
						strbData.append(arrayName + "[" + intCount + "][1][" + i + "][0] = '" + strDDay + "';");					
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][1] = '" + strDDisp + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][2] = '" + strDTime + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][3] = '" + strDDate + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][4] = '" + StrLoc + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][5] = '" + strADay + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][6] = '" + strADisp + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][7] = '" + strATime + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][8] = '" + strADate + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][9] = '" + strFltNo + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][10] = '" + strAFare + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][11] = '" + strAChg + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][12] = '" + strIFare + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][13] = '" + strIChg + "';");
					    strbData.append(arrayName + "[" + intCount + "][1][" + i + "][14] = '" + strPoint + "';");
					}
					
					strbData.append(arrayName + "[" + intCount + "][2]= new Array();");
					strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "] = new Array();");
					strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][0] = '" + strSegID + "';");
					strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][1] = '" + StrLoc + "';");
					strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][2] = '" + intFareID + "';");
					strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][3] = '" + strFltID + "';");
					
					intCountCon++;
				   intCount++;
				//}
			}
		}else{
			if (strRouteType.equals("D")){
				collFlights = availableFlightDTO.getOutboundConnectedFlights();
			}else{
				collFlights = availableFlightDTO.getInboundConnectedFlights();
			}
			iterFlights = collFlights.iterator();
			intCount = 0 ; 
			while (iterFlights.hasNext()){
				
				strValues	= "";
				strDDay		= "";
				strDDate  	= "";
				strDDisp	= "";
				strDTime	= "";
				strADay		= "";
				strADate 	= "";
				strADisp	= "";
				strATime	= "";
				StrLoc		= "";
				strFltNo 	= "";
				strAFare	= "";
				strAChg 	= "";
				strIFare	= "";
				strIChg 	= "";
				intFareID	= 0;
				strSegID 	= "";
				strFltID 	= "";
				dblFareAmt	= 0;
				blnFareAvail = false;
				int intCountSeg = 0 ; 
				intCountCon = 0 ; 
				strPoint = "";

				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight)iterFlights.next();
				//if (availableConnectedFlight.isSeatsAvailable()){

					IFaresChargesAndSeats iFaresChargesAndSeats = (IFaresChargesAndSeats)availableConnectedFlight;
					if (iFaresChargesAndSeats.getFareType() != FareTypes.NO_FARE) {
						//intFareID = iFaresChargesAndSeats.getFare().getFareId();
						dblFareAmt = iFaresChargesAndSeats.getFareAmount().doubleValue();
						
						// add the iteration
						Collection fareChargeSegments = iFaresChargesAndSeats.getFareChargeSegmentsCollection();
						Iterator iterfareChagesSegments = fareChargeSegments.iterator();
						while (iterfareChagesSegments.hasNext()){
							FareChargeSegmentsDTO fareChargeSegmentsDTO = (FareChargeSegmentsDTO)fareChargeSegments.iterator().next();
							if (strFareID != ""){strFareID+= "/";}
							strFareID += fareChargeSegmentsDTO.getFare().getFareId();
						}
						
						blnFareAvail = true;
					}

					Collection collAvailableFlightSegments = availableConnectedFlight.getAvailableFlightSegments();
					Iterator iterCollAvailfFlights = collAvailableFlightSegments.iterator();
					
					strbData.append(arrayName + "[" + intCount + "] = new Array();");
				    strbData.append(arrayName + "[" + intCount + "][0]= '" + strValues + "';");
				    
				    intCountSeg = 0;
				    intCountCon = 0;
					while (iterCollAvailfFlights.hasNext()) {
						AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment)iterCollAvailfFlights.next();
						FlightSegmentDTO flightSegmentDTO = availableFlightSegment.getFlightSegmentDTO();
						
						if ((availableFlightSegment.getFare() != null) && blnFareAvail == false){
							intFareID = availableFlightSegment.getFare().getFareId();
							dblFareAmt = availableFlightSegment.getFare().getFareAmount();
//							 strChg = availableConnectedFlight.getCharges() // new method required
						}
						
						System.out.println("Departure Date " + flightSegmentDTO.getDepartureDateTime());
						
						strDDay = smpdtDay.format(flightSegmentDTO.getDepartureDateTime());
						strDDate = smpdtDate.format(flightSegmentDTO.getDepartureDateTime());
						strDTime = smpdtTime.format(flightSegmentDTO.getDepartureDateTime());
						strDDisp = smpdtDispDate.format(flightSegmentDTO.getDepartureDateTime());
						
						strADay = smpdtDay.format(flightSegmentDTO.getArrivalDateTime());
						strADate = smpdtDate.format(flightSegmentDTO.getArrivalDateTime());
						strATime = smpdtTime.format(flightSegmentDTO.getArrivalDateTime());
						strADisp = smpdtDispDate.format(flightSegmentDTO.getArrivalDateTime());
						
						strFltNo = flightSegmentDTO.getFlightNumber();
						strValues = Integer.toString(intCount)  ; 
						strSegID  =	flightSegmentDTO.getFlightId().toString(); 
						strFltID  =  flightSegmentDTO.getSegmentId().toString();
						
						StrLoc = ""; //flightSegmentDTO.getSegmentCode();
						String[] arrSegments = flightSegmentDTO.getSegmentCode().split("/");
						strbData.append(arrayName + "[" + intCount + "][1]= new Array();");
					    		
						for (int i = 0 ; i < arrSegments.length ; i++){
							StrLoc = getSegementDetails(arrSegments[i]) + "/";
							strPoint = "Via";
							if (i == 0){strPoint = "Depart";}
							if (i == (arrSegments.length - 1)){strPoint = "Arrive";}
							
							strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "] = new Array();");
							strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][0] = '" + strDDay + "';");					
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][1] = '" + strDDisp + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][2] = '" + strDTime + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][3] = '" + strDDate + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][4] = '" + StrLoc + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][5] = '" + strADay + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][6] = '" + strADisp + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][7] = '" + strATime + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][8] = '" + strADate + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][9] = '" + strFltNo + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][10] = '" + strAFare + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][11] = '" + strAChg + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][12] = '" + strIFare + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][13] = '" + strIChg + "';");
						    strbData.append(arrayName + "[" + intCount + "][1][" + intCountSeg + "][14] = '" + strPoint + "';");
						    
						    intCountSeg++;
						}
						strbData.append(arrayName + "[" + intCount + "][2] = new Array();");
						strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "] = new Array();");
						strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][0] = '" + strSegID + "';");
						strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][1] = '" + StrLoc + "';");
						strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][2] = '" + strFareID + "';");
						strbData.append(arrayName + "[" + intCount + "][2][" + intCountCon + "][3] = '" + strFltID + "';");
						
						intCountCon++;
					}
					intCount++;
				//}
			}
		}
		//strbData.toString();
		*/
		return ""; 
	}
	
	private static String getSegementDetails(String strSegement){
		String strSegements = "";
		try{
			Map mapAirports = MapGenerator.getAirportMap();
			String[] strArrSegment = strSegement.split("/");
			strSegements = "";
			for (int i = 0 ; i < strArrSegment.length ; i++){
				if (strSegements != ""){strSegements += "/";}
				strSegements += mapAirports.get(strArrSegment[i]); // + "(" + strSegment[i] + ")";
			}
		}catch (ModuleException ex){}
		return strSegements;
	}
	
	/*
	 * create the hashmap for the calendar
	 */
	private static HashMap createHashMap(String strDate, int intVariance){
		HashMap hashMapCalendar = new HashMap(intVariance * 2);
		
		SimpleDateFormat smpdtFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cl = Calendar.getInstance();
		cl.set(Integer.parseInt(strDate.substring(6,10)), Integer.parseInt(strDate.substring(0,2)) - 1, Integer.parseInt(strDate.substring(3,5)));
		cl.add(Calendar.DATE, -intVariance);
		strDate = smpdtFormat.format(cl.getTime());
		
		for (int i = 0 ; i < (intVariance * 2) ; i++){
			cl.set(Integer.parseInt(strDate.substring(6,10)), Integer.parseInt(strDate.substring(0,2)) - 1, Integer.parseInt(strDate.substring(3,5)));
			cl.add(Calendar.DATE, i);
			System.out.println(smpdtFormat.format(cl.getTime()));
			
			String[] arrData = {"","","",".00"};
			hashMapCalendar.put(smpdtFormat.format(cl.getTime()), arrData);
		}
		return hashMapCalendar;
	}
	
	/*
	 * Compare the hash map values with the current value and update the hash map
	 */
	private static void compareHashValues(String strDate, String strValues, HashMap hashMap){
		String[] strData = StringUtils.split(strValues,",");
		System.out.println("checking format " + strDate);
		String[] strHashData = (String[])hashMap.get(strDate);
		if (strHashData[3] == ".00"){
			hashMap.put(strDate, strValues.split(","));
		}else if (Double.parseDouble(strHashData[3]) > Double.parseDouble(strData[3])){
			hashMap.put(strDate, strValues.split(","));
		}
	}
	
	/*
	 * Reate Hash map data
	 */
	private static String readHashMap(String strDate, int intVariance, HashMap hashMapCalendar, String arrName){
		StringBuffer strbData = new StringBuffer();
		SimpleDateFormat smpdtFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat smpdtArrayFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cl = Calendar.getInstance();
		cl.set(Integer.parseInt(strDate.substring(6,10)), Integer.parseInt(strDate.substring(0,2)) - 1, Integer.parseInt(strDate.substring(3,5)));
		cl.add(Calendar.DATE, -intVariance);
		strDate = smpdtFormat.format(cl.getTime());
		for (int i = 0 ; i < (intVariance * 2) ; i++){
			cl.set(Integer.parseInt(strDate.substring(6,10)), Integer.parseInt(strDate.substring(0,2)) - 1, Integer.parseInt(strDate.substring(3,5)));
			cl.add(Calendar.DATE, i);
			String[] arrData = (String[])hashMapCalendar.get(smpdtFormat.format(cl.getTime()));
			strbData.append(arrName + "[" +  i + "] = new Array('" + smpdtArrayFormat.format(cl.getTime()) + "','" + arrData[3] + "','" + arrData[0] + "','" + arrData[1] + "','" + arrData[2] + "');");
		}
		return strbData.toString();
	}
}



