package com.isa.thinair.messagepasser.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of sent ETL information with code share carriers
 * 
 * @author Rikaz
 * @hibernate.class table = "T_CS_ETL"
 */
public class CodeShareETL extends Persistent {

	private static final long serialVersionUID = -7185867895680222309L;

	/** Holds the etl parsed id */
	private int etlId;

	/** Hold the date downloaded */
	private Date processedDate;

	/** Hold the from address of etl */
	private String toAddress;

	/** Hold the carrier code */
	private String carrierCode;

	/** Hold the etlContent */
	private String etlContent;

	/** Hold the fromAirport */
	private String fromAirport; //

	/** Hold the toAirport */
	private String toAirport;

	/** Hold the proceed status */
	private String status;

	/** Holds the flight number */
	private String flightDesignator;

	/** Holds the Departure Date */
	private Date departureDate;

	/** Holds the partNumber */
	private int partNumber;

	private Integer refMessageID;

	/**
	 * @return Returns the departureDate.
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the carrierCode.
	 * @hibernate.property column = "CARRIER_CODE"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "CS_FLIGHT_NUMBER"
	 */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	/**
	 * @return Returns the fromAirport.
	 * @hibernate.property column = "FROM_AIRPORT"
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            The fromAirport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the etlId.
	 * @hibernate.id column = "CS_ETL_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_CS_ETL"
	 */
	public int getEtlId() {
		return etlId;
	}

	/**
	 * @param etlId
	 *            The etlId to set.
	 */
	public void setEtlId(int etlId) {
		this.etlId = etlId;
	}

	/**
	 * @return Returns the toAirport.
	 * @hibernate.property column = "TO_AIRPORT"
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            The toAirport to set.
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	/**
	 * @return Returns the etlContent.
	 * @hibernate.property column = "ETL_CONTENT"
	 */
	public String getEtlContent() {
		return etlContent;
	}

	/**
	 * @param etlContent
	 *            The etlContent to set.
	 */
	public void setEtlContent(String etlContent) {
		this.etlContent = etlContent;
	}

	/**
	 * @return Returns the partNumber.
	 * @hibernate.property column = "PART_NUMBER"
	 */
	public int getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber
	 *            The partNumber to set.
	 */
	public void setPartNumber(int partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return Returns the partNumber.
	 * @hibernate.property column = "TO_ADDRESS"
	 */
	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * @return Returns the partNumber.
	 * @hibernate.property column = "PROCESSED_DATE"
	 */
	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	/**
	 * @return Returns the partNumber.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the partNumber.
	 * @hibernate.property column = "REF_MESSAGE_ID"
	 */
	public Integer getRefMessageID() {
		return refMessageID;
	}

	public void setRefMessageID(Integer refMessageID) {
		this.refMessageID = refMessageID;
	}

}
