package com.isa.thinair.messagepasser.api.utils;

public interface ReconcileCSVConstants {

	/** Denotes CSV Booking Reconcile process status types */
	public static interface BookingProcessStatus {

		/** Holds the initiated status */
		public static final String INITIATED = "I";

		/** Holds the process status */
		public static final String PROCESSED = "P";

		/** Holds the no processed status */
		public static final String NOT_PROCESSED = "N";

		/** Holds the error occurred status */
		public static final String ERROR_OCCURED = "E";

		/** Holds the state to process the departure date */
		public static final String PROCESS_DEPARTURE_DATE = "V";

	}
}
