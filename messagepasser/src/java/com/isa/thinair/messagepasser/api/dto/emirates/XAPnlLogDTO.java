/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.dto.emirates;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Byorn
 * @TODO : Need to Improvise on this
 */
public class XAPnlLogDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6168148326718305195L;

	private String emailTo;

	private String fileName = null;

	// Holds the Exception Description
	private String exceptionDescription = null;

	// Holds the Stack Trace Elements
	private StackTraceElement[] stackTraceElements = null;

	// Holds the PNL ID
	private String pnlId = null;

	// Holds teh PNL Content
	private String xaPnlContent = null;

	// Holds a collection of String pax details
	private Collection paxWithErrors = null;

	// Holds a collection of String pax details with reservations
	private Collection paxWithReservations = null;

	/**
	 * @return Returns the exceptionDescription.
	 */
	public String getExceptionDescription() {
		return exceptionDescription;
	}

	/**
	 * @param exceptionDescription
	 *            The exceptionDescription to set.
	 */
	public void setExceptionDescription(String exceptionDescription) {
		this.exceptionDescription = exceptionDescription;
	}

	/**
	 * @return Returns the stackTraceElements.
	 */
	public StackTraceElement[] getStackTraceElements() {
		return stackTraceElements;
	}

	/**
	 * @param stackTraceElements
	 *            The stackTraceElements to set.
	 */
	public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
		this.stackTraceElements = stackTraceElements;
	}

	/**
	 * @return Returns the fileName.
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            The fileName to set.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return Returns the emailTo.
	 */
	public String getEmailTo() {
		return emailTo;
	}

	/**
	 * @param emailTo
	 *            The emailTo to set.
	 */
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	/**
	 * @return Returns the xaPnlContent.
	 */
	public String getXaPnlContent() {
		return xaPnlContent;
	}

	/**
	 * @param xaPnlContent
	 *            The xaPnlContent to set.
	 */
	public void setXaPnlContent(String xaPnlContent) {
		this.xaPnlContent = xaPnlContent;
	}

	/**
	 * @return Returns the paxWithErrors.
	 */
	public Collection getPaxWithErrors() {
		return paxWithErrors;
	}

	/**
	 * @param paxWithErrors
	 *            The paxWithErrors to set.
	 */
	public void setPaxWithErrors(Collection paxWithErrors) {
		this.paxWithErrors = paxWithErrors;
	}

	/**
	 * @return Returns the pnlId.
	 */
	public String getPnlId() {
		return pnlId;
	}

	/**
	 * @param pnlId
	 *            The pnlId to set.
	 */
	public void setPnlId(String pnlId) {
		this.pnlId = pnlId;
	}

	/**
	 * @return Returns the paxWithReservations.
	 */
	public Collection getPaxWithReservations() {
		return paxWithReservations;
	}

	/**
	 * @param paxWithReservations
	 *            The paxWithReservations to set.
	 */
	public void setPaxWithReservations(Collection paxWithReservations) {
		this.paxWithReservations = paxWithReservations;
	}

}
