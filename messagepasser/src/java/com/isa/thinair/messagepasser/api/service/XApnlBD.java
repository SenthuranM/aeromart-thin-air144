/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Dhanushka Ranatunga
 */
public interface XApnlBD {

	public static final String SERVICE_NAME = "XAPNLService";

	/**
	 * Reconcile XA PNL Reservations
	 * 
	 * @param xaPnlId
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reconcileXAPnlReservations(int xaPnlId) throws ModuleException;

	/**
	 * Prcoess xa pnl reconcilation
	 */
	public ServiceResponce processXAPnlReconcilation() throws ModuleException;

	/**
	 * Get Failed XA Pnl Ids. Will update all "E" to "N"
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection getFailedXAPnlIds() throws ModuleException;

	/**
	 * Returns XA Pnl entry paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	public Page getPagedXAPNLData(List criteria, int startIndex, int noRecs, List orderByFieldList) throws ModuleException;

	/**
	 * Return XA Pnl Passenger entries
	 * 
	 * @param xaPnlId
	 * @param status
	 * @return
	 * @throws ModuleException
	 */
	public Collection getXAPnlPaxEntries(int xaPnlId, String status) throws ModuleException;

	public void downloadXAPnlMessagesFromMailServer() throws ModuleException;

	public Integer parseXAPnlDocument(Collection msgNamesThatMayHavePartTwoThree, String strMsgName) throws ModuleException;

	public Collection getValidatedXAPnlDocuments();
}
