/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.utils;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.messagepasser.api.service.ETLBD;
import com.isa.thinair.messagepasser.api.service.ETLBD;
import com.isa.thinair.messagepasser.api.service.PFSXmlBD;
import com.isa.thinair.messagepasser.api.service.SSIMBD;
import com.isa.thinair.messagepasser.api.service.XApnlBD;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.msgbroker.api.service.CouponStatusTKCREQServiceBD;
import com.isa.thinair.msgbroker.api.service.ETSTicketCouponStatusUpdateBD;
import com.isa.thinair.msgbroker.api.service.PFSETLServiceBD;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

/**
 * Air Reservation module specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class MessagePasserModuleUtils {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(MessagePasserModuleUtils.class);

	/**
	 * Returns IModule instance for air reservation module
	 * 
	 * @return
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(MessagepasserConstants.MODULE_NAME);
	}

	/**
	 * Returns the MessagePasserConfig instance for air reservation module
	 * 
	 * @return
	 */
	public static MessagePasserConfig getMessagePasserConfig() {
		return (MessagePasserConfig) getInstance().getModuleConfig();
	}

	/**
	 * Return the global confirguration
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}

	/**
	 * Returns air reservation module data source
	 * 
	 * @return
	 */
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	/**
	 * Returns the external data source
	 * 
	 * @return
	 */
	public static XDBConnectionUtil getExternalDBConnectionUtil() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (XDBConnectionUtil) lookup.getBean("isa:base://modules?id=myXDBDataSourceUtil");
	}

	/**
	 * Return transparent reservation business delegate
	 * 
	 * @return
	 */
	public static ReservationBD getReservationBD() {
		return (ReservationBD) MessagePasserModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent reservation query business delegate
	 * 
	 * @return
	 */
	public static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) MessagePasserModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationQueryBD.SERVICE_NAME);
	}

	/**
	 * Returns Bean for air reservation module
	 * 
	 * @param beanName
	 * @return
	 */
	public static Object getBean(String beanName) {
		return LookupServiceFactory.getInstance().getModule(MessagepasserConstants.MODULE_NAME).getLocalBean(beanName);
	}

	/**
	 * Returns transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getMessagePasserConfig(),
				MessagepasserConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getMessagePasserConfig(),
				MessagepasserConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	/**
	 * Return transparent xa-pnl business delegate
	 * 
	 * @return
	 */
	public static XApnlBD getXApnlBD() {
		return (XApnlBD) MessagePasserModuleUtils.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, XApnlBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight inventory business delegate
	 * 
	 * @return
	 */
	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) MessagePasserModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				FlightInventoryBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight business delegate
	 * 
	 * @return
	 */
	public static FlightBD getFlightBD() {
		return (FlightBD) MessagePasserModuleUtils.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	/**
	 * Return transparent payment broker delegate
	 * 
	 * @return
	 */
	public static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) MessagePasserModuleUtils.lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME,
				PaymentBrokerBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight inventory reservation delegate
	 * 
	 * @return
	 */
	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) MessagePasserModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				FlightInventoryResBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight inventory reservation delegate
	 * 
	 * @return
	 */
	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) MessagePasserModuleUtils.lookupEJB3Service(MessagingConstants.MODULE_NAME,
				MessagingServiceBD.SERVICE_NAME);
	}

	/**
	 * Return transparent Crypto delegate
	 * 
	 * @return
	 */
	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) MessagePasserModuleUtils.lookupServiceBD(CryptoConstants.MODULE_NAME,
				CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	/**
	 * Return transparent travel agent finance delegate
	 * 
	 * @return
	 */
	public static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) MessagePasserModuleUtils.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
				TravelAgentFinanceBD.SERVICE_NAME);
	}

	/**
	 * Return transparent travel agent delegate
	 * 
	 * @return
	 */
	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) MessagePasserModuleUtils.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
				TravelAgentBD.SERVICE_NAME);
	}

	/**
	 * Return transparent fare delegate
	 * 
	 * @return
	 */
	public static FareBD getFareBD() {
		return (FareBD) MessagePasserModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	/**
	 * Return transparent charge delegate
	 * 
	 * @return
	 */
	public static ChargeBD getChargeBD() {
		return (ChargeBD) MessagePasserModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	/**
	 * Return transparent auditor delegate
	 * 
	 * @return
	 */
	public static AuditorBD getAuditorBD() {
		return (AuditorBD) MessagePasserModuleUtils.lookupServiceBD(AuditorConstants.MODULE_NAME,
				AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	/**
	 * Return transparent booking class delegate
	 * 
	 * @return
	 */
	public static BookingClassBD getBookingClassBD() {
		return (BookingClassBD) MessagePasserModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BookingClassBD.SERVICE_NAME);
	}

	/**
	 * Return transparent airport class delegate
	 * 
	 * @return
	 */
	public static AirportBD getAirportBD() {
		return (AirportBD) MessagePasserModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	/**
	 * Return transparent common master delegate
	 * 
	 * @return
	 */
	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) MessagePasserModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				CommonMasterBD.SERVICE_NAME);
	}

	/**
	 * Return transparent location class delegate
	 * 
	 * @return
	 */
	public static LocationBD getLocationBD() {
		return (LocationBD) MessagePasserModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent passenger business delegate
	 * 
	 * @return
	 */
	public static PassengerBD getPassengerBD() {
		return (PassengerBD) MessagePasserModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				PassengerBD.SERVICE_NAME);
	}

	/**
	 * Return transparent aircraft business delegate
	 * 
	 * @return
	 */
	public static AircraftBD getAircraftBD() {
		return (AircraftBD) MessagePasserModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	/**
	 * Return Alerting Business delegate
	 * 
	 * @return
	 */
	public static AlertingBD getAlertingBD() {
		return (AlertingBD) MessagePasserModuleUtils.lookupServiceBD(AlertingConstants.MODULE_NAME,
				AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	/**
	 * Return transparent segment business delegate
	 * 
	 * @return
	 */
	public static SegmentBD getSegmentBD() {
		return (SegmentBD) MessagePasserModuleUtils
				.lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	/**
	 * Returns transparent Web Service Client business delegate
	 * 
	 * @return
	 */
	public static WSClientBD getWSClientBD() {
		return (WSClientBD) MessagePasserModuleUtils.lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	public static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) MessagePasserModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}
	
	public static PFSXmlBD getPfsXmlBD() {
		return (PFSXmlBD) MessagePasserModuleUtils.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, PFSXmlBD.SERVICE_NAME);
	}
	
	public static ETLBD getETLBD() {
		return (ETLBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, ETLBD.SERVICE_NAME);
	}
	
	public static PFSETLServiceBD getPFSETLServiceBD() {
		return (PFSETLServiceBD) MessagePasserModuleUtils.lookupEJB3Service(MsgbrokerConstants.MODULE_NAME,
				PFSETLServiceBD.SERVICE_NAME);
	}
	
	public static SSIMBD getSSIMBD() {
		return (SSIMBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, SSIMBD.SERVICE_NAME);
	}
	
	public static ScheduleBD getScheduleBD() {
		return (ScheduleBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}
	
	public final static ETSTicketCouponStatusUpdateBD getETSTicketCouponStatusUpdateBD() {
		return (ETSTicketCouponStatusUpdateBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, ETSTicketCouponStatusUpdateBD.SERVICE_NAME);
	}
	
	public final static CouponStatusTKCREQServiceBD getTKCREQCouponStatusUpdateBD() {
		return (CouponStatusTKCREQServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, CouponStatusTKCREQServiceBD.SERVICE_NAME);
	}
	
	
}
