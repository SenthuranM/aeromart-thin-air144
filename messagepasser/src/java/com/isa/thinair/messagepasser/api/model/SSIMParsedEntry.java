package com.isa.thinair.messagepasser.api.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import org.hibernate.Hibernate;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of SSIM parsed schedule information
 * 
 * @author Rikaz
 * @since 1.0
 * @hibernate.class table = "T_SSIM_PARSED"
 */
public class SSIMParsedEntry extends Persistent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -546317974338360390L;

	private int ssimId;
	private int ssimParsedId;
	private String flightDesignator;
	private Date periodFrom;
	private Date periodTo;
	private String origin;
	private String destination;
	private String scheduleStatus; // [P-PARSED,C-CREATED,E-ERROR]
	private String errorDescription;
	private int noLegs;
	private Blob flightLegContent;
	private String messageText;

	/**
	 * @return Returns the ssimId.
	 * @hibernate.property column = "SSIM_ID"
	 */
	public int getSsimId() {
		return ssimId;
	}

	public void setSsimId(int ssimId) {
		this.ssimId = ssimId;
	}

	/**
	 * @return Returns the ssimParsedId.
	 * @hibernate.id column = "SSIMP_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_SSIM_PARSED"
	 */
	public int getSsimParsedId() {
		return ssimParsedId;
	}

	public void setSsimParsedId(int ssimParsedId) {
		this.ssimParsedId = ssimParsedId;
	}

	/**
	 * @return Returns the airlineDesignator.
	 * @hibernate.property column = "FLIGHT_DESIGNATOR"
	 */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String airlineDesignator) {
		this.flightDesignator = airlineDesignator;
	}

	/**
	 * @return Returns the periodFrom.
	 * @hibernate.property column = "PERIOD_FROM"
	 */
	public Date getPeriodFrom() {
		return periodFrom;
	}

	public void setPeriodFrom(Date periodFrom) {
		this.periodFrom = periodFrom;
	}

	/**
	 * @return Returns the periodTo.
	 * @hibernate.property column = "PERIOD_TO"
	 */
	public Date getPeriodTo() {
		return periodTo;
	}

	public void setPeriodTo(Date periodTo) {
		this.periodTo = periodTo;
	}

	/**
	 * @return Returns the origin.
	 * @hibernate.property column = "ORIGIN_STATION"
	 */
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return Returns the destination.
	 * @hibernate.property column = "DESTINATION_STATION"
	 */
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return Returns the scheduleStatus.
	 * @hibernate.property column = "SCHEDULE_STATUS"
	 */
	public String getScheduleStatus() {
		return scheduleStatus;
	}

	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}

	/**
	 * @return Returns the errorDescription.
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return Returns the noLegs.
	 * @hibernate.property column = "NO_LEGS"
	 */
	public int getNoLegs() {
		return noLegs;
	}

	public void setNoLegs(int noLegs) {
		this.noLegs = noLegs;
	}

	/**
	 * @return Returns the flightLegContent.
	 * @hibernate.property column = "FLIGHT_LEG_CONTENT"
	 */
	public Blob getFlightLegContent() {
		return flightLegContent;
	}

	public void setFlightLegContent(Blob flightLegContent) {
		this.flightLegContent = flightLegContent;
	}

	public String getMessageText() {
		String value = null;

		try {
			if (messageText == null) {
				value = new String(getFlightLegContent().getBytes(1, (int) getFlightLegContent().length()));
				messageText = value;
			}
		} catch (Exception e) {
		}

		return messageText;
	}

	public void setMessageText(String messageText) {
		Blob blob = null;

		this.messageText = messageText;
		blob = Hibernate.createBlob(messageText.getBytes());
		setFlightLegContent(blob);
	}

}
