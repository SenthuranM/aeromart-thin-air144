/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

public interface ReconcileCsvBD {

	public static final String SERVICE_NAME = "ReconcileCSVService";

	public ServiceResponce reconcileCSVReservations() throws ModuleException;

	public ServiceResponce processDepDateText() throws ModuleException;
}
