package com.isa.thinair.messagepasser.api.dto.prl;

/**
 * Holds the ETL atomic record specific data transfer information
 * 
 * @author Ishan
 * @since 1.0
 */
public class PRLPAXRecordDTO {

	public static final String SSR_CODE_For_XAPNR = "TKNA";
	public static final int SPLIT_LENGTH_OF_SEAT_INFO_WITH_STATUS_CODE = 12;
	public static final int SPLIT_LENGTH_OF_SEAT_INFO_WITHOUT_STATUS_CODE = 8;

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the passenger first name with title */
	private String firstNameWithTitle;

	private String ssrCode;

	private String ssrText;

	private String errorDescription;

	/** Holds the ticket number will contain .L/10257445 */
	private String ticketnumber;

	/** Hold the tour Id * */
	private String tourId;

	private String eticketNumber;

	private String coupNumber;

	private String paxStatus;

	private String infEticketNumber;

	private String infCoupNumber;

	private String passportNumber;

	private String passportExpiryDate;

	private String countryOfIssue;

	private String seatInfor;

	private String weightIndicator;

	private String noOfCheckedBaggage;

	private String checkedBaggageWeight;

	private String uncheckedBaggageWeight;

	/**
	 * Add the first Name with title
	 * 
	 * @param fName
	 */
	public void addFName(String fName) {
		if (this.getFirstNameWithTitle() == null) {
			this.setFirstNameWithTitle(fName);
		} else {
			this.setFirstNameWithTitle(this.getFirstNameWithTitle() + " " + fName);
		}
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the title in order to parse
	 * 
	 * @return
	 */
	public String getParseTitle() {
		if (this.getFirstNameWithTitle() == null) {
			return this.getLastName();
		} else {
			return this.getFirstNameWithTitle();
		}
	}

	/**
	 * Clones the RecordDTO object
	 */
	public Object clone() {
		PRLPAXRecordDTO recordDTO = new PRLPAXRecordDTO();
		recordDTO.setFirstNameWithTitle(this.getFirstNameWithTitle());
		recordDTO.setLastName(this.getLastName());

		return recordDTO;
	}

	/**
	 * @return Returns the firstNameWithTitle.
	 */
	public String getFirstNameWithTitle() {
		return firstNameWithTitle;
	}

	/**
	 * @param firstNameWithTitle
	 *            The firstNameWithTitle to set.
	 */
	public void setFirstNameWithTitle(String firstNameWithTitle) {
		this.firstNameWithTitle = firstNameWithTitle;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		if (this.ssrCode == null) {
			this.ssrCode = " ";
		}

		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		if (this.ssrText == null) {
			this.ssrText = "";
		}
		return this.ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {

		this.ssrText = ssrText;
	}

	public String getErrorDescription() {
		if (this.errorDescription == null) {
			this.errorDescription = "";
		}
		return this.errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		if (errorDescription == null) {
			errorDescription = "";
		}

		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}

	/**
	 * @return the ticket number
	 */
	public String getTicketnumber() {
		return ticketnumber;
	}

	/**
	 * @param ticketnumber
	 *            the ticket number to set
	 */
	public void setTicketnumber(String ticketnumber) {
		this.ticketnumber = ticketnumber;
	}

	/**
	 * Returns the XA pnr number
	 * 
	 * @return
	 */
	public String getXAPnrNumber() {
		String pnr = null;
		if (this.getTicketnumber() != null && this.getTicketnumber().length() > 0) {
			pnr = this.getTicketnumber().substring(2);
			return pnr.replaceAll("/", "");
		} else {
			return null;
		}

	}

	/**
	 * @return the tourId
	 */
	public String getTourId() {
		return tourId;
	}

	/**
	 * @param tourId
	 *            the tourId to set
	 */
	public void setTourId(String tourId) {
		this.tourId = tourId;
	}

	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	public String getCoupNumber() {
		return coupNumber;
	}

	public void setCoupNumber(String coupNumber) {
		this.coupNumber = coupNumber;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getInfEticketNumber() {
		return infEticketNumber;
	}

	public void setInfEticketNumber(String infEticketNumber) {
		this.infEticketNumber = infEticketNumber;
	}

	public String getInfCoupNumber() {
		return infCoupNumber;
	}

	public void setInfCoupNumber(String infCoupNumber) {
		this.infCoupNumber = infCoupNumber;
	}

	/**
	 * @return Returns the passport number
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber The passportNumber to set.
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	/**
	 * @return Returns the passport expiry date
	 */
	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	/**
	 * @param passportExpiryDate The passportExpiryDate to set.
	 */
	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	/**
	 * @return Return the country of issue
	 */
	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	/**
	 * @param countryOfIssue
	 */
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	/**
	 * @return Returns the seat information
	 */
	public String getSeatInfor() {
		return seatInfor;
	}

	/**
	 * @param seatInfor The seatInfor to set.
	 */
	public void setSeatInfor(String seatInfor) {
		this.seatInfor = seatInfor;
	}

	/**
	 * Returns the seat number
	 * Seat infor can be come with or without HK1 like .R/SEAT 25C or .R/SEAT HK1 25C
	 * @return
	 */
	public String getSeatNumber() {
		String seatNumber;
		if (this.getSeatInfor() != null) {
			if (this.getSeatInfor().length() > SPLIT_LENGTH_OF_SEAT_INFO_WITH_STATUS_CODE) {
				seatNumber = this.getSeatInfor().substring(SPLIT_LENGTH_OF_SEAT_INFO_WITH_STATUS_CODE);
			} else {
				seatNumber = this.getSeatInfor().substring(SPLIT_LENGTH_OF_SEAT_INFO_WITHOUT_STATUS_CODE);
			}
			return seatNumber;
		} else {
			return null;
		}
	}

	/**
	 * @return Returns the one of the weight indicator from K,L,P
	 */
	public String getWeightIndicator() {
		return weightIndicator;
	}

	/**
	 * The weightIndicator to set.
	 *
	 * @param weightIndicator
	 */
	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	/**
	 * @return Returns the number of checked baggage
	 */
	public String getNoOfCheckedBaggage() {
		return noOfCheckedBaggage;
	}

	/**
	 * The noOfCheckedBaggage to set.
	 *
	 * @param noOfCheckedBaggage
	 */
	public void setNoOfCheckedBaggage(String noOfCheckedBaggage) {
		this.noOfCheckedBaggage = noOfCheckedBaggage;
	}

	/**
	 * @return Returns the checked baggage weight
	 */
	public String getCheckedBaggageWeight() {
		return checkedBaggageWeight;
	}

	/**
	 * The checkedBaggageWeight to set.
	 *
	 * @param checkedBaggageWeight
	 */
	public void setCheckedBaggageWeight(String checkedBaggageWeight) {
		this.checkedBaggageWeight = checkedBaggageWeight;
	}

	/**
	 * @return Returns the unchecked baggage weight
	 */
	public String getUncheckedBaggageWeight() {
		return uncheckedBaggageWeight;
	}

	/**
	 * The uncheckedBaggageWeight to set.
	 *
	 * @param uncheckedBaggageWeight
	 */
	public void setUncheckedBaggageWeight(String uncheckedBaggageWeight) {
		this.uncheckedBaggageWeight = uncheckedBaggageWeight;
	}
}
