package com.isa.thinair.messagepasser.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.dto.ssim.SSIMRecordDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Rikaz
 */
public interface SSIMBD {

	public static final String SERVICE_NAME = "SSIMService";

	public ServiceResponce processScheduleMessage() throws ModuleException;

	public ServiceResponce createSchedulesForReceivedSSIM(SSIMRecordDTO ssimRecordDTO) throws ModuleException;
}
