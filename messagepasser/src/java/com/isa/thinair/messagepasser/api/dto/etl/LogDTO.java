package com.isa.thinair.messagepasser.api.dto.etl;

import java.io.Serializable;
import java.util.Collection;

public class LogDTO implements Serializable {


	private static final long serialVersionUID = -6168148326718305195L;

	private String emailTo;

	private String fileName = null;

	// Holds the Exception Description
	private String exceptionDescription = null;

	// Holds the Stack Trace Elements
	private StackTraceElement[] stackTraceElements = null;

	// Holds the content ID
	private String id = null;

	// Holds the Content
	private String content = null;

	// Holds a collection of String pax details
	private Collection<String> paxWithErrors = null;

	// Holds a collection of String pax details with reservations
	private Collection<String> paxWithReservations = null;

	/**
	 * @return Returns the exceptionDescription.
	 */
	public String getExceptionDescription() {
		return exceptionDescription;
	}

	/**
	 * @param exceptionDescription
	 *            The exceptionDescription to set.
	 */
	public void setExceptionDescription(String exceptionDescription) {
		this.exceptionDescription = exceptionDescription;
	}

	/**
	 * @return Returns the stackTraceElements.
	 */
	public StackTraceElement[] getStackTraceElements() {
		return stackTraceElements;
	}

	/**
	 * @param stackTraceElements
	 *            The stackTraceElements to set.
	 */
	public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
		this.stackTraceElements = stackTraceElements;
	}

	/**
	 * @return Returns the fileName.
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            The fileName to set.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return Returns the emailTo.
	 */
	public String getEmailTo() {
		return emailTo;
	}

	/**
	 * @param emailTo
	 *            The emailTo to set.
	 */
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}


	/**
	 * @return Returns the paxWithErrors.
	 */
	public Collection<String> getPaxWithErrors() {
		return paxWithErrors;
	}

	/**
	 * @param paxWithErrors
	 *            The paxWithErrors to set.
	 */
	public void setPaxWithErrors(Collection<String> paxWithErrors) {
		this.paxWithErrors = paxWithErrors;
	}

	/**
	 * @return Returns the paxWithReservations.
	 */
	public Collection<String> getPaxWithReservations() {
		return paxWithReservations;
	}

	/**
	 * @param paxWithReservations
	 *            The paxWithReservations to set.
	 */
	public void setPaxWithReservations(Collection<String> paxWithReservations) {
		this.paxWithReservations = paxWithReservations;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
