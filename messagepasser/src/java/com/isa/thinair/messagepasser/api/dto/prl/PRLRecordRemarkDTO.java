/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.dto.prl;

import java.util.StringTokenizer;

import com.isa.thinair.messagepasser.api.utils.ParserConstants;

/**
 * Holds the PNL atomic record specific data transfer information
 * 
 * @author Ishan
 * @since 1.0
 */
public class PRLRecordRemarkDTO {

	private String adviceTo;

	private String ssrCode;

	private String ssrText;

	// note: infant firstName will be used to store the child name and last name
	// also
	private String infantfirstName;

	private String infantLastName;

	private String infantSSRCode;

	private String infantSSRText;

	private String errorDescription;

	private boolean isInfantRelatedDTO;

	private boolean isChildRelatedDTO;

	/**
	 * @return Returns the adviceTo.
	 */
	public String getAdviceTo() {
		if (this.adviceTo != null) {
			return this.adviceTo.trim();
		}
		return adviceTo;
	}

	/**
	 * @param adviceTo
	 *            The adviceTo to set.
	 */
	public void setAdviceTo(String adviceTo) {
		this.adviceTo = adviceTo;
	}

	/**
	 * @return Returns the infantSSRCode.
	 */
	public String getInfantSSRCode() {
		return infantSSRCode;
	}

	/**
	 * @param infantSSRCode
	 *            The infantSSRCode to set.
	 */
	public void setInfantSSRCode(String infantSSRCode) {
		this.infantSSRCode = infantSSRCode;
	}

	/**
	 * @return Returns the infantSSRText.
	 */
	public String getInfantSSRText() {
		return infantSSRText;
	}

	/**
	 * @param infantSSRText
	 *            The infantSSRText to set.
	 */
	public void setInfantSSRText(String infantSSRText) {
		this.infantSSRText = infantSSRText;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		if (ssrCode != null) {
			if (ssrCode.startsWith(".R/")) {
				this.ssrCode = ssrCode.substring(3);
			}

		}
	}

	public String getCleanedUpSSRText() {
		String cleanedSSRText = "";
		String theSsrText = "";
		if (this.ssrText == null) {
			this.ssrText = "";
		}
		if (this.ssrText.indexOf("#") != -1) {
			theSsrText = this.ssrText.substring(0, this.ssrText.indexOf("#"));
		} else {
			theSsrText = this.ssrText;
		}
		cleanedSSRText = theSsrText.replaceAll("/", " ").replaceAll("-", " ");

		return cleanedSSRText;

	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		if (this.ssrText == null) {
			this.ssrText = "";
		}

		return this.ssrText;

	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	/**
	 * 
	 */
	public void constructAdviceTo() {
		try {
			if (ssrCode == null) {
				return;
			}
			if (ssrCode.equals(ParserConstants.EMRECORD_REMARK_DTO.INFANT_CODE1)
					|| ssrCode.substring(1).equals(ParserConstants.EMRECORD_REMARK_DTO.INFANT_CODE2)) {
				setInfantRelatedDTO(true);
				findInfantName();
			} else {
				if (ssrCode.equals(ParserConstants.EMRECORD_REMARK_DTO.CHILD)
						|| ssrCode.substring(1).equals(ParserConstants.EMRECORD_REMARK_DTO.CHILD)) {
					setChildRelatedDTO(true);
					if (this.ssrText.indexOf("#") != -1) {
						this.adviceTo = this.ssrText.substring(this.ssrText.indexOf("#") + 1);
					}
					return;
					// findInfantName();
				}
			}

			// find the advice code
			if (this.ssrText.lastIndexOf("#") == -1) {
				// no advice code found
			} else {
				this.adviceTo = this.ssrText.substring(this.ssrText.lastIndexOf("#") + 1);
			}
		} catch (Exception e) {
			setErrorDescription("Exception inside constructAdviceTo");
		}
	}

	/**
	 * 
	 * 
	 */
	private void findInfantName() {
		// Here am assuming that after INFT the frist occurance separated with #
		// will be the infants firstname and lastname
		try {
			if (this.ssrText == null) {
				return;
			}

			StringTokenizer stringTokenizer = new StringTokenizer(this.ssrText, "#");
			int count = stringTokenizer.countTokens();
			if (count >= 2)// if count==2 Text#TextName - Will Assume the
			// Infant Name and Not the advice code{
			{
				String lastNameFirstName[] = new String[count];
				int i = 0;
				while (stringTokenizer.hasMoreTokens()) {
					lastNameFirstName[i] = stringTokenizer.nextToken();
					i++;
				}
				constructInfantFirstNameAndLastName(lastNameFirstName);
			}
		}

		catch (StringIndexOutOfBoundsException iob) {
			setErrorDescription("Index out of Bound inside findInfantName");
		} catch (NullPointerException ne) {
			setErrorDescription("null pointer exception inside findInfantName");
		} catch (Exception e) {
			setErrorDescription("Exception inside findInfantName");
		}

	}

	/**
	 * 
	 * @param lastNameFirstName
	 */
	private void constructInfantFirstNameAndLastName(String[] lastNameFirstName) {

		try {
			String lastName = lastNameFirstName[0];
			String firstName = lastNameFirstName[1];

			// lastName
			if (lastName.lastIndexOf(' ') == -1) {
				this.infantLastName = lastName;
			} else {
				this.infantLastName = lastName.substring(lastName.lastIndexOf(' ') + 1);
			}

			// firstname
			String fName = firstName.substring(firstName.indexOf(' ') + 1);
			if (fName.lastIndexOf(' ') == -1) {
				this.infantfirstName = fName.substring(fName.indexOf(' ') + 1);
			} else {
				String s[] = fName.split(" ");
				this.infantfirstName = s[0];
			}
		} catch (NumberFormatException nfe) {
			setErrorDescription("Num Frmt Xception inside constructInFLName");
		} catch (StringIndexOutOfBoundsException iob) {
			setErrorDescription("Index out of Bound inside constructInFLName");
		} catch (NullPointerException ne) {
			setErrorDescription("null pointer exception inside constructInFLName");
		} catch (Exception e) {
			setErrorDescription("Exception inside constructInFLName");
		}
	}

	/**
	 * @return Returns the infantfirstName.
	 */
	public String getInfantfirstName() {
		return infantfirstName;
	}

	/**
	 * @param infantfirstName
	 *            The infantfirstName to set.
	 */
	public void setInfantfirstName(String infantfirstName) {
		this.infantfirstName = infantfirstName;
	}

	/**
	 * @return Returns the infantLastName.
	 */
	public String getInfantLastName() {
		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            The infantLastName to set.
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	/**
	 * @return Returns the errorDescription.
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 */
	public void setErrorDescription(String errorDescription) {
		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}

	/**
	 * @return Returns the isInfantRelatedDTO.
	 */
	public boolean isInfantRelatedDTO() {
		return isInfantRelatedDTO;
	}

	/**
	 * @param isInfantRelatedDTO
	 *            The isInfantRelatedDTO to set.
	 */
	public void setInfantRelatedDTO(boolean isInfantRelatedDTO) {
		this.isInfantRelatedDTO = isInfantRelatedDTO;
	}

	/**
	 * @return Returns the isChildRelatedDTO.
	 */
	public boolean isChildRelatedDTO() {
		return isChildRelatedDTO;
	}

	/**
	 * @param isChildRelatedDTO
	 *            The isChildRelatedDTO to set.
	 */
	public void setChildRelatedDTO(boolean isChildRelatedDTO) {
		this.isChildRelatedDTO = isChildRelatedDTO;
	}

}
