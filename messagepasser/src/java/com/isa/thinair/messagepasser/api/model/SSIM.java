package com.isa.thinair.messagepasser.api.model;

import java.sql.Blob;
import java.util.Date;

import org.hibernate.Hibernate;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * To keep track of SSIM Header information
 * 
 * @author Rikaz
 * @hibernate.class table = "T_SSIM"
 */
public class SSIM extends Persistent {

	private static final long serialVersionUID = -546317974338360390L;
	private int ssimId;
	private Date processDate;
	private String processStatus; // [P-PARSED,C-COMPLETED,E-ERROR]
	private String airlineCode;
	private Date creationDate;
	private Blob ssimContent;
	private String messageText;
	private String errorDescription;

	/**
	 * @return Returns the ssimId.
	 * @hibernate.id column = "SSIM_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_SSIM"
	 */
	public int getSsimId() {
		return ssimId;
	}

	public void setSsimId(int ssimId) {
		this.ssimId = ssimId;
	}

	/**
	 * @return Returns the processDate.
	 * @hibernate.property column = "PROCESSED_DATE"
	 */
	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * @return Returns the processStatus.
	 * @hibernate.property column = "PROCESSED_STATUS"
	 */
	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	/**
	 * @return Returns the airlineCode.
	 * @hibernate.property column = "AIRLINE_CODE"
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	/**
	 * @return Returns the creationDate.
	 * @hibernate.property column = "CREATION_DATE"
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return Returns the ssimContent.
	 * @hibernate.property column = "SSIM_CONTENT"
	 */
	public Blob getSsimContent() {
		return ssimContent;
	}

	public void setSsimContent(Blob ssimContent) {
		this.ssimContent = ssimContent;
	}

	public String getMessageText() {
		String value = null;

		try {
			if (messageText == null) {
				value = new String(getSsimContent().getBytes(1, (int) getSsimContent().length()));
				messageText = value;
			}
		} catch (Exception e) {
		}

		return messageText;
	}

	public void setMessageText(String messageText) {
		Blob blob = null;
		if (!StringUtil.isNullOrEmpty(messageText)) {
			this.messageText = messageText;
			blob = Hibernate.createBlob(messageText.getBytes());
			setSsimContent(blob);
		}

	}
	
	/**
	 * @return Returns the ssimContent.
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
 
}
