package com.isa.thinair.messagepasser.api.dto.etl;

import java.util.Date;

/**
 * Holds Passenger final sales data transfer information
 * 
 * @author Ishan
 * @since 1.0
 */
public class ETLPaxNameListDTO {

	private String departureAirportCode;

	private String arrivalAirportCode;

	private String cabinClassCode;

	private String title;

	private String firstName;

	private String lastName;

	private String pnr;

	private String day;

	private String month;

	private Date realFlightDate;

	private String flightNumber;

	private int etlId;

	private Date dateDownloaded;

	private String infantFirstName;

	private String infantLastName;

	private String infantTitle;

	private String ssrCode;

	private String ssrRemarks;

	private String paxType;

	private String inboundInfo;

	private String outboundInfo;

	private String infantSSRCode;

	private String infantSSRText;

	private String errorDescription;

	private String eticketNumber;

	private String coupNumber;

	private String paxStatus;

	private String infEticketNumber;

	private String infCoupNumber;

	private String logicalCabinClassCode;

	private String bookingClassCode;
	
	private String marketingFlight;
	
	private String externalRecordLocator;
	
	private String codeShareFlightNo;
	
	private String codeShareBc;
	
	private Integer gdsId;
	
	private String externalEticketNumber;
	
	private Integer externalCouponNo;
	
	private String externalInfEticketNumber;
	
	private Integer externalInfCoupNumber;
	
	private Boolean useAeroMartETS;
	

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return Returns the arrivalAirportCode.
	 */
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	/**
	 * @param arrivalAirportCode
	 *            The arrivalAirportCode to set.
	 */
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	/**
	 * @return Returns the departureAirportCode.
	 */
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	/**
	 * @param departureAirportCode
	 *            The departureAirportCode to set.
	 */
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	/**
	 * @return Returns the firstname.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstname
	 *            The firstname to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastname.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastname
	 *            The lastname to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the etlId.
	 */
	public int getEtlId() {
		return etlId;
	}

	/**
	 * @param etlId
	 *            The etlId to set.
	 */
	public void setEtlId(int etlId) {
		this.etlId = etlId;
	}

	/**
	 * @return Returns the realFlightDate.
	 */
	public Date getRealFlightDate() {
		return realFlightDate;
	}

	/**
	 * @param realFlightDate
	 *            The realFlightDate to set.
	 */
	public void setRealFlightDate(Date realFlightDate) {
		this.realFlightDate = realFlightDate;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return this.cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the dateDownloaded.
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return the xaPnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param xaPnr
	 *            the xaPnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the infantFirstName.
	 */
	public String getInfantFirstName() {
		return infantFirstName;
	}

	/**
	 * @param infantFirstName
	 *            The infantFirstName to set.
	 */
	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	/**
	 * @return Returns the infantLastName.
	 */
	public String getInfantLastName() {
		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            The infantLastName to set.
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	/**
	 * @return Returns the infantTitle.
	 */
	public String getInfantTitle() {
		return infantTitle;
	}

	/**
	 * @param infantTitle
	 *            The infantTitle to set.
	 */
	public void setInfantTitle(String infantTitle) {
		this.infantTitle = infantTitle;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrRemarks.
	 */
	public String getSsrRemarks() {

		return ssrRemarks;
	}

	/**
	 * @param ssrRemarks
	 *            The ssrRemarks to set.
	 */
	public void setSsrRemarks(String ssrRemarks) {
		this.ssrRemarks = ssrRemarks;
	}

	/**
	 * @return Returns the infantSSRCode.
	 */
	public String getInfantSSRCode() {
		return infantSSRCode;
	}

	/**
	 * @param infantSSRCode
	 *            The infantSSRCode to set.
	 */
	public void setInfantSSRCode(String infantSSRCode) {
		this.infantSSRCode = infantSSRCode;
	}

	/**
	 * @return Returns the infantSSRText.
	 */
	public String getInfantSSRText() {
		return infantSSRText;
	}

	/**
	 * @param infantSSRText
	 *            The infantSSRText to set.
	 */
	public void setInfantSSRText(String infantSSRText) {
		this.infantSSRText = infantSSRText;
	}

	/**
	 * @return Returns the inboundInfo.
	 */
	public String getInboundInfo() {
		return inboundInfo;
	}

	/**
	 * @param inboundInfo
	 *            The inboundInfo to set.
	 */
	public void setInboundInfo(String inboundInfo) {
		this.inboundInfo = inboundInfo;
	}

	/**
	 * @return Returns the outboundInfo.
	 */
	public String getOutboundInfo() {
		return outboundInfo;
	}

	/**
	 * @param outboundInfo
	 *            The outboundInfo to set.
	 */
	public void setOutboundInfo(String outboundInfo) {
		this.outboundInfo = outboundInfo;
	}

	/**
	 * 
	 * @return
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * 
	 * @param errorDescription
	 */
	public void setErrorDescription(String errorDescription) {
		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}

	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	public String getCoupNumber() {
		return coupNumber;
	}

	public void setCoupNumber(String coupNumber) {
		this.coupNumber = coupNumber;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getInfEticketNumber() {
		return infEticketNumber;
	}

	public void setInfEticketNumber(String infEticketNumber) {
		this.infEticketNumber = infEticketNumber;
	}

	public String getInfCoupNumber() {
		return infCoupNumber;
	}

	public void setInfCoupNumber(String infCoupNumber) {
		this.infCoupNumber = infCoupNumber;
	}

	public String getMarketingFlight() {
		return marketingFlight;
	}

	public void setMarketingFlight(String marketingFlight) {
		this.marketingFlight = marketingFlight;
	}

	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	public String getCodeShareFlightNo() {
		return codeShareFlightNo;
	}

	public void setCodeShareFlightNo(String codeShareFlightNo) {
		this.codeShareFlightNo = codeShareFlightNo;
	}

	public String getCodeShareBc() {
		return codeShareBc;
	}

	public void setCodeShareBc(String codeShareBc) {
		this.codeShareBc = codeShareBc;
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public String getExternalEticketNumber() {
		return externalEticketNumber;
	}

	public void setExternalEticketNumber(String externalEticketNumber) {
		this.externalEticketNumber = externalEticketNumber;
	}

	public String getExternalInfEticketNumber() {
		return externalInfEticketNumber;
	}

	public void setExternalInfEticketNumber(String externalInfEticketNumber) {
		this.externalInfEticketNumber = externalInfEticketNumber;
	}

	public Integer getExternalCouponNo() {
		return externalCouponNo;
	}

	public void setExternalCouponNo(Integer externalCouponNo) {
		this.externalCouponNo = externalCouponNo;
	}

	public Integer getExternalInfCoupNumber() {
		return externalInfCoupNumber;
	}

	public void setExternalInfCoupNumber(Integer externalInfCoupNumber) {
		this.externalInfCoupNumber = externalInfCoupNumber;
	}
	
	public Boolean getUseAeroMartETS() {
		return useAeroMartETS;
	}
	

	public void setUseAeroMartETS(Boolean useAeroMartETS) {
		this.useAeroMartETS = useAeroMartETS;
	}
}
