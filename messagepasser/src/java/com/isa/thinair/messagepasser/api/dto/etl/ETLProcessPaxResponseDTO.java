package com.isa.thinair.messagepasser.api.dto.etl;

import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;

public class ETLProcessPaxResponseDTO {

	ReservationAudit reservationAudit;
	ETLPaxEntry etlPaxEntry;

	public ReservationAudit getReservationAudit() {
		return reservationAudit;
	}

	public void setReservationAudit(ReservationAudit reservationAudit) {
		this.reservationAudit = reservationAudit;
	}

	public ETLPaxEntry getEtlPaxEntry() {
		return etlPaxEntry;
	}

	public void setEtlPaxEntry(ETLPaxEntry etlPaxEntry) {
		this.etlPaxEntry = etlPaxEntry;
	}

}
