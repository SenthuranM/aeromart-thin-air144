package com.isa.thinair.messagepasser.api.dto.ssim;

import java.io.Serializable;

public class FlightLegDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String departueStation; // 37-39
	private String passengerSTD; // 40-43
	private String aircraftDeparture; // 44-47
	private String departureVariation; // 48-52
	private String departureTerminal; // 53-54
	private String arrivalStation; // 55-57
	private String aircraftArrival; // 58-61
	private String passengerSTA; // 62-65
	private String arrivalVariation; // 66-70
	private String arrivalTerminal; // 71-72

	public String getDepartueStation() {
		return departueStation;
	}

	public void setDepartueStation(String departueStation) {
		this.departueStation = departueStation;
	}

	public String getPassengerSTD() {
		return passengerSTD;
	}

	public void setPassengerSTD(String passengerSTD) {
		this.passengerSTD = passengerSTD;
	}

	public String getAircraftDeparture() {
		return aircraftDeparture;
	}

	public void setAircraftDeparture(String aircraftDeparture) {
		this.aircraftDeparture = aircraftDeparture;
	}

	public String getDepartureVariation() {
		return departureVariation;
	}

	public void setDepartureVariation(String departureVariation) {
		this.departureVariation = departureVariation;
	}

	public String getDepartureTerminal() {
		return departureTerminal;
	}

	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getAircraftArrival() {
		return aircraftArrival;
	}

	public void setAircraftArrival(String aircraftArrival) {
		this.aircraftArrival = aircraftArrival;
	}

	public String getPassengerSTA() {
		return passengerSTA;
	}

	public void setPassengerSTA(String passengerSTA) {
		this.passengerSTA = passengerSTA;
	}

	public String getArrivalVariation() {
		return arrivalVariation;
	}

	public void setArrivalVariation(String arrivalVariation) {
		this.arrivalVariation = arrivalVariation;
	}

	public String getArrivalTerminal() {
		return arrivalTerminal;
	}

	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}

}
