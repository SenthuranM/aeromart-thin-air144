package com.isa.thinair.messagepasser.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

import java.io.Serializable;
import java.util.Date;

/**
 * To keep track of PRL parsed information
 * 
 * @author Ishan
 * @since 1.0
 * @hibernate.class table = "T_PRL_PARSED"
 */
public class PRLPaxEntry extends Persistent implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -546317974338360390L;

	/** Holds the prl pax parsed id */
	private int prlPaxId;

	/** Holds the prl unique id */
	private Integer prlId;

	/** Holds the received time stamp */
	private Date receivedDate;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the flight date */
	private Date flightDate;

	/** Holds the departure airport */
	private String departureAirport;

	/** Holds the pnr */
	private String pnr;

	/** Holds the passenger title */
	private String title;

	/** Holds the passenger first name */
	private String firstName;

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the process status (not processed, processed, error) */
	private String processedStatus;

	/** Holds the cabin class */
	private String cabinClassCode;

	/** Holds the arrival airport */
	private String arrivalAirport;

	/** Holds the error description */
	private String errorDescription;

	/** Holds the passenger type */
	private String paxType;

	private String eticketNumber;

	private Integer coupNumber;

	private String paxStatus;

	private String infEticketNumber;

	private Integer infCoupNumber;

	private String passportNumber;

	private Date passportExpiryDate;

	private String countryOfIssue;

	private String seatNumber;

	private String weightIndicator;

	private Integer noOfCheckedBaggage;

	private Integer checkedBaggageWeight;

	private Integer uncheckedBaggageWeight;

	private String paxCategoryCode;

	private String adultSsrRemarks;


	/**
	 * @return Returns the arrivalAirport.
	 * @hibernate.property column = "DESTINATION_STATION"
	 */
	public String getArrivalAirport() {
		return arrivalAirport;
	}

	/**
	 * @param arrivalAirport
	 *            The arrivalAirport to set.
	 */
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the departureAirport.
	 * @hibernate.property column = "DEP_STN"
	 */
	public String getDepartureAirport() {
		return departureAirport;
	}

	/**
	 * @param departureAirport
	 *            The departureAirport to set.
	 */
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	/**
	 * @return Returns the errorDescription.
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return Returns the firstName.
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightDate.
	 * @hibernate.property column = "FLIGHT_DATE"
	 */
	public Date getFlightDate() {
		return flightDate;
	}

	/**
	 * @param flightDate
	 *            The flightDate to set.
	 */
	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastName.
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the prlId.
	 * @hibernate.property column = "PRL_ID"
	 */
	public Integer getPrlId() {
		return prlId;
	}

	/**
	 * @param prlId
	 *            The prlId to set.
	 */
	public void setPrlId(Integer prlId) {
		this.prlId = prlId;
	}

	/**
	 * @return Returns the prlPaxId.
	 * @hibernate.id column = "PP_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PRL"
	 */
	public int getPrlPaxId() {
		return prlPaxId;
	}

	/**
	 * @param prlPaxId
	 *            The prlPaxId to set.
	 */
	public void setPrlPaxId(int prlPaxId) {
		this.prlPaxId = prlPaxId;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the processedStatus.
	 * @hibernate.property column = "PROC_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	/**
	 * @param processedStatus
	 *            The processedStatus to set.
	 */
	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * @return Returns the receivedDate.
	 * @hibernate.property column = "RECIEVED_DATE"
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	/**
	 * @param receivedDate
	 *            The receivedDate to set.
	 */
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE_CODE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the infantTitle.
	 * @hibernate.property column = "ET_NO"
	 */
	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	/**
	 * @return Returns the coupNumber.
	 * @hibernate.property column = "COUP_NO"
	 */
	public Integer getCoupNumber() {
		return coupNumber;
	}

	public void setCoupNumber(Integer coupNumber) {
		this.coupNumber = coupNumber;
	}

	/**
	 * @return Returns the paxStatus.
	 * @hibernate.property column = "PAX_STATUS"
	 */
	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	/**
	 * @return Returns the infantTitle.
	 * @hibernate.property column = "INF_ET_NO"
	 */
	public String getInfEticketNumber() {
		return infEticketNumber;
	}

	public void setInfEticketNumber(String infEticketNumber) {
		this.infEticketNumber = infEticketNumber;
	}

	/**
	 * @return Returns the coupNumber.
	 * @hibernate.property column = "INF_COUP_NO"
	 */
	public Integer getInfCoupNumber() {
		return infCoupNumber;
	}

	public void setInfCoupNumber(Integer infCoupNumber) {
		this.infCoupNumber = infCoupNumber;
	}

	/**
	 * @return Returns the passport number
	 * @hibernate.property column = "PASSPORT_NUMBER"
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber The passportNumber to set.
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	/**
	 * @return Returns the passport expiry date
	 * @hibernate.property column = "PASSPORT_EXPIRY_DATE"
	 */
	public Date getPassportExpiryDate() {
		return passportExpiryDate;
	}

	/**
	 * @param passportExpiryDate The passportExpiryDate to set.
	 */
	public void setPassportExpiryDate(Date passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	/**
	 * @return Return the country of issue
	 * @hibernate.property column = "COUNTRY_OF_ISSUE"
	 */
	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	/**
	 * @param countryOfIssue
	 */
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	/**
	 * @return Returns the seat information
	 * @hibernate.property column = "SEAT_NUMBER"
	 */
	public String getSeatNumber() {
		return seatNumber;
	}

	/**
	 * @param seatNumber The seatNumber to set.
	 */
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	/**
	 * @return Returns the one of the weight indicator from K,L,P
	 * @hibernate.property column = "WEIGHT_INDICATOR"
	 */
	public String getWeightIndicator() {
		return weightIndicator;
	}

	/**
	 * @param weightIndicator The weightIndicator to set.
	 */
	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	/**
	 * @return Returns the number of checked baggage
	 * @hibernate.property column = "NO_OF_CHECKED_BAGGAGE"
	 */
	public Integer getNoOfCheckedBaggage() {
		return noOfCheckedBaggage;
	}

	/**
	 * @param noOfCheckedBaggage The noOfCheckedBaggage to set.
	 */
	public void setNoOfCheckedBaggage(Integer noOfCheckedBaggage) {
		this.noOfCheckedBaggage = noOfCheckedBaggage;
	}

	/**
	 * @return Returns the checked baggage weight
	 * @hibernate.property column = "CHECKED_BAGGAGE_WEIGHT"
	 */
	public Integer getCheckedBaggageWeight() {
		return checkedBaggageWeight;
	}

	/**
	 * @param checkedBaggageWeight The checkedBaggageWeight to set.
	 */
	public void setCheckedBaggageWeight(Integer checkedBaggageWeight) {
		this.checkedBaggageWeight = checkedBaggageWeight;
	}

	/**
	 * @return Returns the unchecked baggage weight
	 * @hibernate.property column = "UNCHECKED_BAGGAGE_WEIGHT"
	 */
	public Integer getUncheckedBaggageWeight() {
		return uncheckedBaggageWeight;
	}

	/**
	 * @param uncheckedBaggageWeight The uncheckedBaggageWeight to set.
	 */
	public void setUncheckedBaggageWeight(Integer uncheckedBaggageWeight) {
		this.uncheckedBaggageWeight = uncheckedBaggageWeight;
	}

	/**
	 * @return
	 * @hibernate.property column = "PAX_CATEGORY_CODE"
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "ADULT_SSR_REMARKS"
	 */
	public String getAdultSsrRemarks() {
		return adultSsrRemarks;
	}

	public void setAdultSsrRemarks(String adultSsrRemarks) {
		this.adultSsrRemarks = adultSsrRemarks;
	}
}
