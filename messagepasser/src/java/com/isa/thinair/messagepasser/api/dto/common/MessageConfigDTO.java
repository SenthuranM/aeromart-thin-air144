package com.isa.thinair.messagepasser.api.dto.common;

import java.io.Serializable;

public class MessageConfigDTO implements Serializable {

	private String popMailServer;
	private String processPath;
	private String parsedPath;
	private String errorPath;
	private String mailServerUsername;
	private String mailServerPassword;
	private String ttyMailServer;
	private String ttyMailServerUserName;
	private String ttyMailServerPassword;
	private boolean fetchFromMailServer;
	private boolean deleteFromMailServer;
	
	private String popMailServerPort;
	private MAIL_CLIENT preferedmailClient =  MAIL_CLIENT.POP3_CLIENT;
	
	public enum MAIL_CLIENT {
		JAVAX_MAIL,POP3_CLIENT
	};

	public String getPopMailServer() {
		return popMailServer;
	}

	public void setPopMailServer(String popMailServer) {
		this.popMailServer = popMailServer;
	}

	public String getProcessPath() {
		return processPath;
	}

	public void setProcessPath(String processPath) {
		this.processPath = processPath;
	}

	public String getParsedPath() {
		return parsedPath;
	}

	public void setParsedPath(String parsedPath) {
		this.parsedPath = parsedPath;
	}

	public String getErrorPath() {
		return errorPath;
	}

	public void setErrorPath(String errorPath) {
		this.errorPath = errorPath;
	}

	public String getMailServerUsername() {
		return mailServerUsername;
	}

	public void setMailServerUsername(String mailServerUsername) {
		this.mailServerUsername = mailServerUsername;
	}

	public String getMailServerPassword() {
		return mailServerPassword;
	}

	public void setMailServerPassword(String mailServerPassword) {
		this.mailServerPassword = mailServerPassword;
	}

	public boolean isFetchFromMailServer() {
		return fetchFromMailServer;
	}

	public void setFetchFromMailServer(boolean fetchFromMailServer) {
		this.fetchFromMailServer = fetchFromMailServer;
	}

	public boolean isDeleteFromMailServer() {
		return deleteFromMailServer;
	}

	public void setDeleteFromMailServer(boolean deleteFromMailServer) {
		this.deleteFromMailServer = deleteFromMailServer;
	}

	public String getTtyMailServer() {
		return ttyMailServer;
	}

	public void setTtyMailServer(String ttyMailServer) {
		this.ttyMailServer = ttyMailServer;
	}

	public String getTtyMailServerUserName() {
		return ttyMailServerUserName;
	}

	public void setTtyMailServerUserName(String ttyMailServerUserName) {
		this.ttyMailServerUserName = ttyMailServerUserName;
	}

	public String getTtyMailServerPassword() {
		return ttyMailServerPassword;
	}

	public void setTtyMailServerPassword(String ttyMailServerPassword) {
		this.ttyMailServerPassword = ttyMailServerPassword;
	}

	public MAIL_CLIENT getPreferedmailClient() {
		return preferedmailClient;
	}

	public void setPreferedmailClient(MAIL_CLIENT preferedmailClient) {
		this.preferedmailClient = preferedmailClient;
	}

	public String getPopMailServerPort() {
		return popMailServerPort;
	}

	public void setPopMailServerPort(String popMailServerPort) {
		this.popMailServerPort = popMailServerPort;
	}
}
