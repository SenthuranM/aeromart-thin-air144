package com.isa.thinair.messagepasser.api.dto.etl;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * Holds the ETL atomic record specific data transfer information
 * 
 * @author Ishan
 * @since 1.0
 */
public class ETLPAXRecordDTO {

	public static final String SSR_CODE_For_XAPNR = "TKNA";

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the passenger first name with title */
	private String firstNameWithTitle;

	private String ssrCode;

	private String ssrText;

	private String errorDescription;

	/** Holds the ticket number will contain .L/10257445 */
	private String ticketnumber;

	/** Hold the tour Id * */
	private String tourId;

	private String eticketNumber;

	private String coupNumber;

	private String paxStatus;

	private String infEticketNumber;

	private String infCoupNumber;
	
	private String marketingFlight;

	/**
	 * Add the first Name with title
	 * 
	 * @param fName
	 */
	public void addFName(String fName) {
		if (this.getFirstNameWithTitle() == null) {
			this.setFirstNameWithTitle(fName);
		} else {
			this.setFirstNameWithTitle(this.getFirstNameWithTitle() + " " + fName);
		}
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the title in order to parse
	 * 
	 * @return
	 */
	public String getParseTitle() {
		if (this.getFirstNameWithTitle() == null) {
			return this.getLastName();
		} else {
			return this.getFirstNameWithTitle();
		}
	}

	/**
	 * Clones the RecordDTO object
	 */
	public Object clone() {
		ETLPAXRecordDTO recordDTO = new ETLPAXRecordDTO();
		recordDTO.setFirstNameWithTitle(this.getFirstNameWithTitle());
		recordDTO.setLastName(this.getLastName());

		return recordDTO;
	}

	/**
	 * @return Returns the firstNameWithTitle.
	 */
	public String getFirstNameWithTitle() {
		return firstNameWithTitle;
	}

	/**
	 * @param firstNameWithTitle
	 *            The firstNameWithTitle to set.
	 */
	public void setFirstNameWithTitle(String firstNameWithTitle) {
		this.firstNameWithTitle = firstNameWithTitle;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		if (this.ssrCode == null) {
			this.ssrCode = " ";
		}

		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		if (this.ssrText == null) {
			this.ssrText = "";
		}
		return this.ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {

		this.ssrText = ssrText;
	}

	public String getErrorDescription() {
		if (this.errorDescription == null) {
			this.errorDescription = "";
		}
		return this.errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		if (errorDescription == null) {
			errorDescription = "";
		}

		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}

	/**
	 * @return the ticket number
	 */
	public String getTicketnumber() {
		return ticketnumber;
	}

	/**
	 * @param ticketnumber
	 *            the ticket number to set
	 */
	public void setTicketnumber(String ticketnumber) {
		this.ticketnumber = ticketnumber;
	}

	/**
	 * Returns the XA pnr number
	 * 
	 * @return
	 */
	public String getXAPnrNumber() {
		String pnr = null;
		if (this.getTicketnumber() != null && this.getTicketnumber().length() > 0) {
			pnr = this.getTicketnumber().substring(2);
			return pnr.replaceAll("/", "");
		} else {
			return null;
		}

	}

	/**
	 * @return the tourId
	 */
	public String getTourId() {
		return tourId;
	}

	/**
	 * @param tourId
	 *            the tourId to set
	 */
	public void setTourId(String tourId) {
		this.tourId = tourId;
	}

	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	public String getCoupNumber() {
		return coupNumber;
	}

	public void setCoupNumber(String coupNumber) {
		this.coupNumber = coupNumber;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getInfEticketNumber() {
		return infEticketNumber;
	}

	public void setInfEticketNumber(String infEticketNumber) {
		this.infEticketNumber = infEticketNumber;
	}

	public String getInfCoupNumber() {
		return infCoupNumber;
	}

	public void setInfCoupNumber(String infCoupNumber) {
		this.infCoupNumber = infCoupNumber;
	}

	/**
	 * Returns the pnr number
	 * 
	 * @return
	 */
	public String getPnrNumber() {
		if (this.getTicketnumber() != null && this.getTicketnumber().length() > 0) {
			String pnr = this.getTicketnumber().substring(2);
			return pnr.replaceAll("/", "").trim();
		} else {
			return null;
		}
	}
	
	public String getMarketingFlight() {
		if (this.marketingFlight != null && this.marketingFlight.length() > 0) {
			return this.marketingFlight.replaceAll(".M/", "").trim();
		} else {
			return null;
		}
	}

	public void setMarketingFlight(String marketingFlight) {
		this.marketingFlight = BeanUtils.nullHandler(marketingFlight);
	}

}
