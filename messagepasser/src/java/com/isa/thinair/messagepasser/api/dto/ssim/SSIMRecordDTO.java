package com.isa.thinair.messagepasser.api.dto.ssim;

import java.util.ArrayList;
import java.util.List;

public class SSIMRecordDTO {
	private String headerRecord;
	private List<FlightScheduleRecordDTO> flightLegRecords;
	private CarrierRecordDTO carrierRecord;
	private int parsedFlightLegCount;
	private int failedFlightLegCount;
	private int multiLegScheduleCount;
	private int ssimId;

	public String getHeaderRecord() {
		return headerRecord;
	}

	public void setHeaderRecord(String headerRecord) {
		this.headerRecord = headerRecord;
	}

	public CarrierRecordDTO getCarrierRecord() {
		return carrierRecord;
	}

	public void setCarrierRecord(CarrierRecordDTO carrierRecord) {
		this.carrierRecord = carrierRecord;
	}

	public List<FlightScheduleRecordDTO> getFlightLegRecords() {

		if (this.flightLegRecords == null) {
			flightLegRecords = new ArrayList<FlightScheduleRecordDTO>();
		}

		return flightLegRecords;
	}

	public void addFlightLegRecord(FlightScheduleRecordDTO flightLegRecord) {
		this.getFlightLegRecords().add(flightLegRecord);

	}

	public void setFlightLegRecords(List<FlightScheduleRecordDTO> flightLegRecords) {
		this.flightLegRecords = flightLegRecords;
	}

	public int getParsedFlightLegCount() {
		return parsedFlightLegCount;
	}

	public void setParsedFlightLegCount(int parsedFlightLegCount) {
		this.parsedFlightLegCount = parsedFlightLegCount;
	}

	public int getFailedFlightLegCount() {
		return failedFlightLegCount;
	}

	public void setFailedFlightLegCount(int failedFlightLegCount) {
		this.failedFlightLegCount = failedFlightLegCount;
	}

	public int getMultiLegScheduleCount() {
		return multiLegScheduleCount;
	}

	public void setMultiLegScheduleCount(int multiLegScheduleCount) {
		this.multiLegScheduleCount = multiLegScheduleCount;
	}

	public int getSsimId() {
		return ssimId;
	}

	public void setSsimId(int ssimId) {
		this.ssimId = ssimId;
	}

}
