package com.isa.thinair.messagepasser.api.dto.ssim;

public class CarrierRecordDTO {
	private String recordType;// 1
	private String timeMode;// 2
	private String airlineDesignator;// 3-5
	private String spare1;// 6-10
	private String season;// 11-13
	private String spare2;// 14
	private String scheduleValidityFrom;// 15-21
	private String scheduleValidityTo;// 22-28
	private String creationDate;// 29-35
	private String freeText;// 36-200
	private int ssimId;

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public String getAirlineDesignator() {
		return airlineDesignator;
	}

	public void setAirlineDesignator(String airlineDesignator) {
		this.airlineDesignator = airlineDesignator;
	}

	public String getSpare1() {
		return spare1;
	}

	public void setSpare1(String spare1) {
		this.spare1 = spare1;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getSpare2() {
		return spare2;
	}

	public void setSpare2(String spare2) {
		this.spare2 = spare2;
	}

	public String getScheduleValidityFrom() {
		return scheduleValidityFrom;
	}

	public void setScheduleValidityFrom(String scheduleValidityFrom) {
		this.scheduleValidityFrom = scheduleValidityFrom;
	}

	public String getScheduleValidityTo() {
		return scheduleValidityTo;
	}

	public void setScheduleValidityTo(String scheduleValidityTo) {
		this.scheduleValidityTo = scheduleValidityTo;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getFreeText() {
		return freeText;
	}

	public void setFreeText(String freeText) {
		this.freeText = freeText;
	}

	public int getSsimId() {
		return ssimId;
	}

	public void setSsimId(int ssimId) {
		this.ssimId = ssimId;
	}

}
