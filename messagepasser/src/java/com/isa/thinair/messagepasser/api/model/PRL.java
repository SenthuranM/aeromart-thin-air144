package com.isa.thinair.messagepasser.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of PRL header information
 * 
 * @author Ishan
 * @since 1.0
 * @hibernate.class table = "T_PRL"
 */
public class PRL extends Persistent {
	/** Holds the prl parsed id */
	private int prlId;

	/** Hold the date downloaded */
	private Date dateDownloaded;

	/** Hold the from address of prl */
	private String fromAddress;

	/** Hold the carrier code */
	private String carrierCode;

	/** Hold the prlContent */
	private String prlContent;

	/** Hold the fromAirport */
	private String fromAirport; //

	/** Hold the toAirport */
	private String toAirport;

	/** Holds the number of passengers */
	private int numberOfPassengers;

	/** Hold the proceed status */
	private String processedStatus;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the Departure Date */
	private Date departureDate;

	/** Holds the partNumber */
	private int partNumber;

	/**
	 * @return Returns the departureDate.
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the carrierCode.
	 * @hibernate.property column = "CARRIER_CODE"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the dateDownloaded.
	 * @hibernate.property column = "DATE_OF_DOWNLOAD"
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the fromAddress.
	 * @hibernate.property column = "FROM_ADDRESS"
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress
	 *            The fromAddress to set.
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return Returns the fromAirport.
	 * @hibernate.property column = "FROM_AIRPORT"
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            The fromAirport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the numberOfPassengers.
	 * @hibernate.property column = "NUM_OF_PAX"
	 */
	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}

	/**
	 * @param numberOfPassengers
	 *            The numberOfPassengers to set.
	 */
	public void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}

	/**
	 * @return Returns the prlId.
	 * @hibernate.id column = "PRL_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PRL_PARSED"
	 */
	public int getPrlId() {
		return prlId;
	}

	/**
	 * @param prlId
	 *            The prlId to set.
	 */
	public void setPrlId(int prlId) {
		this.prlId = prlId;
	}

	/**
	 * @return Returns the processedStatus.
	 * @hibernate.property column = "PROCESSED_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	/**
	 * @param processedStatus
	 *            The processedStatus to set.
	 */
	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * @return Returns the toAirport.
	 * @hibernate.property column = "TO_AIRPORT"
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            The toAirport to set.
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	/**
	 * @return Returns the prlContent.
	 * @hibernate.property column = "PRL_CONTENT"
	 */
	public String getPrlContent() {
		return prlContent;
	}

	/**
	 * @param prlContent
	 *            The prlContent to set.
	 */
	public void setPrlContent(String prlContent) {
		this.prlContent = prlContent;
	}

	/**
	 * @return Returns the partNumber.
	 * @hibernate.property column = "PART_NUMBER"
	 */
	public int getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber
	 *            The partNumber to set.
	 */
	public void setPartNumber(int partNumber) {
		this.partNumber = partNumber;
	}

}
