package com.isa.thinair.messagepasser.api.dto.prl;

import java.util.Date;

/**
 * Holds Passenger final sales data transfer information
 * 
 * @author Ishan
 * @since 1.0
 */
public class PRLPaxNameListDTO {

	private String departureAirportCode;

	private String arrivalAirportCode;

	private String cabinClassCode;

	private String title;

	private String firstName;

	private String lastName;

	private String xaPnr;

	private String day;

	private String month;

	private Date realFlightDate;

	private String flightNumber;

	private int prlId;

	private Date dateDownloaded;

	private String infantFirstName;

	private String infantLastName;

	private String infantTitle;

	private String ssrCode;

	private String ssrRemarks;

	private String paxType;

	private String inboundInfo;

	private String outboundInfo;

	private String infantSSRCode;

	private String infantSSRText;

	private String errorDescription;

	private String eticketNumber;

	private String coupNumber;

	private String paxStatus;

	private String infEticketNumber;

	private String infCoupNumber;

	private String passportNumber;

	private Date passportExpiryDate;

	private String countryOfIssue;

	private String seatNumber;

	private String weightIndicator;

	private String noOfCheckedBaggage;

	private String checkedBaggageWeight;

	private String uncheckedBaggageWeight;

	/**
	 * @return Returns the arrivalAirportCode.
	 */
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	/**
	 * @param arrivalAirportCode
	 *            The arrivalAirportCode to set.
	 */
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	/**
	 * @return Returns the departureAirportCode.
	 */
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	/**
	 * @param departureAirportCode
	 *            The departureAirportCode to set.
	 */
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	/**
	 * @return Returns the firstname.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstname to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastname.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastname to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the prlId.
	 */
	public int getPrlId() {
		return prlId;
	}

	/**
	 * @param prlId
	 *            The prlId to set.
	 */
	public void setPrlId(int prlId) {
		this.prlId = prlId;
	}

	/**
	 * @return Returns the realFlightDate.
	 */
	public Date getRealFlightDate() {
		return realFlightDate;
	}

	/**
	 * @param realFlightDate
	 *            The realFlightDate to set.
	 */
	public void setRealFlightDate(Date realFlightDate) {
		this.realFlightDate = realFlightDate;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return this.cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the dateDownloaded.
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return the xaPnr
	 */
	public String getXAPnr() {
		if (xaPnr == null) {
			return " ";
		}
		return xaPnr;
	}

	/**
	 * @param xaPnr
	 *            the xaPnr to set
	 */
	public void setXAPnr(String xaPnr) {
		this.xaPnr = xaPnr;
	}

	/**
	 * @return Returns the infantFirstName.
	 */
	public String getInfantFirstName() {
		return infantFirstName;
	}

	/**
	 * @param infantFirstName
	 *            The infantFirstName to set.
	 */
	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	/**
	 * @return Returns the infantLastName.
	 */
	public String getInfantLastName() {
		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            The infantLastName to set.
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	/**
	 * @return Returns the infantTitle.
	 */
	public String getInfantTitle() {
		return infantTitle;
	}

	/**
	 * @param infantTitle
	 *            The infantTitle to set.
	 */
	public void setInfantTitle(String infantTitle) {
		this.infantTitle = infantTitle;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrRemarks.
	 */
	public String getSsrRemarks() {

		return ssrRemarks;
	}

	/**
	 * @param ssrRemarks
	 *            The ssrRemarks to set.
	 */
	public void setSsrRemarks(String ssrRemarks) {
		this.ssrRemarks = ssrRemarks;
	}

	/**
	 * @return Returns the xaPnr.
	 */
	public String getXaPnr() {
		return xaPnr;
	}

	/**
	 * @param xaPnr
	 *            The xaPnr to set.
	 */
	public void setXaPnr(String xaPnr) {
		this.xaPnr = xaPnr;
	}

	/**
	 * @return Returns the infantSSRCode.
	 */
	public String getInfantSSRCode() {
		return infantSSRCode;
	}

	/**
	 * @param infantSSRCode
	 *            The infantSSRCode to set.
	 */
	public void setInfantSSRCode(String infantSSRCode) {
		this.infantSSRCode = infantSSRCode;
	}

	/**
	 * @return Returns the infantSSRText.
	 */
	public String getInfantSSRText() {
		return infantSSRText;
	}

	/**
	 * @param infantSSRText
	 *            The infantSSRText to set.
	 */
	public void setInfantSSRText(String infantSSRText) {
		this.infantSSRText = infantSSRText;
	}

	/**
	 * @return Returns the inboundInfo.
	 */
	public String getInboundInfo() {
		return inboundInfo;
	}

	/**
	 * @param inboundInfo
	 *            The inboundInfo to set.
	 */
	public void setInboundInfo(String inboundInfo) {
		this.inboundInfo = inboundInfo;
	}

	/**
	 * @return Returns the outboundInfo.
	 */
	public String getOutboundInfo() {
		return outboundInfo;
	}

	/**
	 * @param outboundInfo
	 *            The outboundInfo to set.
	 */
	public void setOutboundInfo(String outboundInfo) {
		this.outboundInfo = outboundInfo;
	}

	/**
	 * 
	 *
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * 
	 * @param errorDescription
	 */
	public void setErrorDescription(String errorDescription) {
		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}

	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	public String getCoupNumber() {
		return coupNumber;
	}

	public void setCoupNumber(String coupNumber) {
		this.coupNumber = coupNumber;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getInfEticketNumber() {
		return infEticketNumber;
	}

	public void setInfEticketNumber(String infEticketNumber) {
		this.infEticketNumber = infEticketNumber;
	}

	public String getInfCoupNumber() {
		return infCoupNumber;
	}

	public void setInfCoupNumber(String infCoupNumber) {
		this.infCoupNumber = infCoupNumber;
	}

	/**
	 * @return Returns the passport number
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber The passportNumber to set.
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	/**
	 * @return Returns the passport expiry date
	 */
	public Date getPassportExpiryDate() {
		return passportExpiryDate;
	}

	/**
	 * @param passportExpiryDate The passportExpiryDate to set.
	 */
	public void setPassportExpiryDate(Date passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	/**
	 * @return Return the country of issue
	 */
	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	/**
	 * @param countryOfIssue
	 */
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	/**
	 * @return Returns the seat information
	 */
	public String getSeatNumber() {
		return seatNumber;
	}

	/**
	 * @param seatNumber The seatNumber to set.
	 */
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	/**
	 *
	 * @return Returns the one of the weight indicator from K,L,P
	 */
	public String getWeightIndicator() {
		return weightIndicator;
	}

	/**
	 * The weightIndicator to set.
	 * @param weightIndicator
	 */
	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	/**
	 *
	 * @return Returns the number of checked baggage
	 */
	public String getNoOfCheckedBaggage() {
		return noOfCheckedBaggage;
	}

	/**
	 * The noOfCheckedBaggage to set.
	 * @param noOfCheckedBaggage
	 */
	public void setNoOfCheckedBaggage(String noOfCheckedBaggage) {
		this.noOfCheckedBaggage = noOfCheckedBaggage;
	}

	/**
	 *
	 * @return Returns the checked baggage weight
	 */
	public String getCheckedBaggageWeight() {
		return checkedBaggageWeight;
	}

	/**
	 * The checkedBaggageWeight to set.
	 * @param checkedBaggageWeight
	 */
	public void setCheckedBaggageWeight(String checkedBaggageWeight) {
		this.checkedBaggageWeight = checkedBaggageWeight;
	}

	/**
	 *
	 * @return Returns the unchecked baggage weight
	 */
	public String getUncheckedBaggageWeight() {
		return uncheckedBaggageWeight;
	}

	/**
	 * The uncheckedBaggageWeight to set.
	 * @param uncheckedBaggageWeight
	 */
	public void setUncheckedBaggageWeight(String uncheckedBaggageWeight) {
		this.uncheckedBaggageWeight = uncheckedBaggageWeight;
	}
}
