package com.isa.thinair.messagepasser.api.dto.ssim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FlightScheduleRecordDTO {

	private String recordType; // 1
	private String operationalSuffix; // 2
	/**
	 * 3-9 <=> Airline Designator => 3-5 Flight Number => 6-9
	 */
	private String flightDesignator; // 3-9
	private String itineraryVariationIdentifier; // 10-11
	private String legSequenceNo; // 12-13
	private String serviceType; // 14
	private String operationPeriodFrom; // 15-21
	private String operationPeriodTo; // 22-28
	private String operationDays; // 29-35
	private String frequencyRate; // 36
	private String departueStation; // 37-39
	// private String passengerSTD; // 40-43
	// private String aircraftDeparture; // 44-47
	// private String departureVariation; // 48-52
	// private String departureTerminal; // 53-54
	private String arrivalStation; // 55-57
	// private String aircraftArrival; // 58-61
	// private String passengerSTA; // 62-65
	// private String arrivalVariation; // 66-70
	// private String arrivalTerminal; // 71-72
	private String aircraftType; // 73-75
	private String paxRBD; // 76-95
	private String paxRBM; // 96-100
	private String mealServiceNote; // 101-110
	private String jointOperationAirline; // 111-119
	private String minConnTimeStatus; /* 120-121 <=> D/I */
	private String spare; // 122-127
	private String itineraryVariationIdOverflow; // 128
	private String aircraftOwner; // 129-131
	private String cockpitCrew; // 132-134
	private String cabinCrew; // 135-137
	/**
	 * 138-146 <=> Airline Designator => 138-140 Flight Number => 141-144 Aircraft Rotation Layover=>145 Operational
	 * Suffix=>146
	 */
	private String onwardFlight; // 138-146
	/**
	 * 148 <=> Spare => 147 FlightTransitLayover => 148
	 */
	private String flightTransitLayover; // 148
	/**
	 * 149-172 <=> Code Sharing — Commercial Duplicate/Code Sharing — Shared Air­ line Designation/Wet Lease Airline
	 * Designation => 149 Traffic Restriction Code => 150-160 Traffic Restriction Code Leg Overflow Indicator => 161
	 * (Spare) => 162-172
	 */
	private String skipped; // 149-172
	private String aircraftConfiguration; // 173-192
	private String dateVariation; // 193-194
	private String recordSerialNo; // 195-200
	private List<FlightLegDTO> legs;
	private String flightLegContent;
	private int ssimParsedId;

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getOperationalSuffix() {
		return operationalSuffix;
	}

	public void setOperationalSuffix(String operationalSuffix) {
		this.operationalSuffix = operationalSuffix;
	}

	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	public String getItineraryVariationIdentifier() {
		return itineraryVariationIdentifier;
	}

	public void setItineraryVariationIdentifier(String itineraryVariationIdentifier) {
		this.itineraryVariationIdentifier = itineraryVariationIdentifier;
	}

	public String getLegSequenceNo() {
		return legSequenceNo;
	}

	public void setLegSequenceNo(String legSequenceNo) {
		this.legSequenceNo = legSequenceNo;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getOperationPeriodFrom() {
		return operationPeriodFrom;
	}

	public void setOperationPeriodFrom(String operationPeriodFrom) {
		this.operationPeriodFrom = operationPeriodFrom;
	}

	public String getOperationPeriodTo() {
		return operationPeriodTo;
	}

	public void setOperationPeriodTo(String operationPeriodTo) {
		this.operationPeriodTo = operationPeriodTo;
	}

	public String getOperationDays() {
		return operationDays;
	}

	public void setOperationDays(String operationDays) {
		this.operationDays = operationDays;
	}

	public String getFrequencyRate() {
		return frequencyRate;
	}

	public void setFrequencyRate(String frequencyRate) {
		this.frequencyRate = frequencyRate;
	}

	public String getDepartueStation() {
		return departueStation;
	}

	public void setDepartueStation(String departueStation) {
		this.departueStation = departueStation;
	}

	// public String getPassengerSTD() {
	// return passengerSTD;
	// }
	//
	// public void setPassengerSTD(String passengerSTD) {
	// this.passengerSTD = passengerSTD;
	// }
	//
	// public String getAircraftDeparture() {
	// return aircraftDeparture;
	// }
	//
	// public void setAircraftDeparture(String aircraftDeparture) {
	// this.aircraftDeparture = aircraftDeparture;
	// }
	//
	// public String getDepartureVariation() {
	// return departureVariation;
	// }
	//
	// public void setDepartureVariation(String departureVariation) {
	// this.departureVariation = departureVariation;
	// }
	//
	// public String getDepartureTerminal() {
	// return departureTerminal;
	// }
	//
	// public void setDepartureTerminal(String departureTerminal) {
	// this.departureTerminal = departureTerminal;
	// }

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	// public String getAircraftArrival() {
	// return aircraftArrival;
	// }
	//
	// public void setAircraftArrival(String aircraftArrival) {
	// this.aircraftArrival = aircraftArrival;
	// }
	//
	// public String getPassengerSTA() {
	// return passengerSTA;
	// }
	//
	// public void setPassengerSTA(String passengerSTA) {
	// this.passengerSTA = passengerSTA;
	// }
	//
	// public String getArrivalVariation() {
	// return arrivalVariation;
	// }
	//
	// public void setArrivalVariation(String arrivalVariation) {
	// this.arrivalVariation = arrivalVariation;
	// }
	//
	// public String getArrivalTerminal() {
	// return arrivalTerminal;
	// }
	//
	// public void setArrivalTerminal(String arrivalTerminal) {
	// this.arrivalTerminal = arrivalTerminal;
	// }

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getPaxRBD() {
		return paxRBD;
	}

	public void setPaxRBD(String paxRBD) {
		this.paxRBD = paxRBD;
	}

	public String getPaxRBM() {
		return paxRBM;
	}

	public void setPaxRBM(String paxRBM) {
		this.paxRBM = paxRBM;
	}

	public String getMealServiceNote() {
		return mealServiceNote;
	}

	public void setMealServiceNote(String mealServiceNote) {
		this.mealServiceNote = mealServiceNote;
	}

	public String getJointOperationAirline() {
		return jointOperationAirline;
	}

	public void setJointOperationAirline(String jointOperationAirline) {
		this.jointOperationAirline = jointOperationAirline;
	}

	public String getMinConnTimeStatus() {
		return minConnTimeStatus;
	}

	public void setMinConnTimeStatus(String minConnTimeStatus) {
		this.minConnTimeStatus = minConnTimeStatus;
	}

	public String getSpare() {
		return spare;
	}

	public void setSpare(String spare) {
		this.spare = spare;
	}

	public String getItineraryVariationIdOverflow() {
		return itineraryVariationIdOverflow;
	}

	public void setItineraryVariationIdOverflow(String itineraryVariationIdOverflow) {
		this.itineraryVariationIdOverflow = itineraryVariationIdOverflow;
	}

	public String getAircraftOwner() {
		return aircraftOwner;
	}

	public void setAircraftOwner(String aircraftOwner) {
		this.aircraftOwner = aircraftOwner;
	}

	public String getCockpitCrew() {
		return cockpitCrew;
	}

	public void setCockpitCrew(String cockpitCrew) {
		this.cockpitCrew = cockpitCrew;
	}

	public String getCabinCrew() {
		return cabinCrew;
	}

	public void setCabinCrew(String cabinCrew) {
		this.cabinCrew = cabinCrew;
	}

	public String getOnwardFlight() {
		return onwardFlight;
	}

	public void setOnwardFlight(String onwardFlight) {
		this.onwardFlight = onwardFlight;
	}

	public String getFlightTransitLayover() {
		return flightTransitLayover;
	}

	public void setFlightTransitLayover(String flightTransitLayover) {
		this.flightTransitLayover = flightTransitLayover;
	}

	public String getSkipped() {
		return skipped;
	}

	public void setSkipped(String skipped) {
		this.skipped = skipped;
	}

	public String getAircraftConfiguration() {
		return aircraftConfiguration;
	}

	public void setAircraftConfiguration(String aircraftConfiguration) {
		this.aircraftConfiguration = aircraftConfiguration;
	}

	public String getDateVariation() {
		return dateVariation;
	}

	public void setDateVariation(String dateVariation) {
		this.dateVariation = dateVariation;
	}

	public String getRecordSerialNo() {
		return recordSerialNo;
	}

	public void setRecordSerialNo(String recordSerialNo) {
		this.recordSerialNo = recordSerialNo;
	}

	public Collection<FlightLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<FlightLegDTO> legs) {
		this.legs = legs;
	}

	public void addLeg(FlightLegDTO ssiFlightLeg) {
		if (this.legs == null)
			this.legs = new ArrayList<FlightLegDTO>();
		this.legs.add(ssiFlightLeg);
	}

	public String getFlightLegContent() {
		return flightLegContent;
	}

	public void setFlightLegContent(String flightLegContent) {
		this.flightLegContent = flightLegContent;
	}

	public void addLegContent(String flightLegContent) {
		StringBuilder sb = new StringBuilder();
		if (this.flightLegContent != null)
			sb.append(this.flightLegContent);

		sb.append(flightLegContent + "\n");

		setFlightLegContent(sb.toString());

	}

	public int getSsimParsedId() {
		return ssimParsedId;
	}

	public void setSsimParsedId(int ssimParsedId) {
		this.ssimParsedId = ssimParsedId;
	}

	// public String toString() {
	// return ToStringBuilder.reflectionToString(this);
	// }

}
