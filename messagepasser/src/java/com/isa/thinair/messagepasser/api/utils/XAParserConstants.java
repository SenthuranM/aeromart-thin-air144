package com.isa.thinair.messagepasser.api.utils;

public interface XAParserConstants {

	public static final String MAPKEY_CONT = "CONT";

	public static final String CC_CODE = "Y";

	/**
	 * holds the valid ssr codes in our system
	 */
	public static interface SSR_CODES {

		public static final String CODE_TKNA = "TKNA";

		public static final String CODE_OKOB = "OKOB";

		public static final String CODE_WCHR = "WCHR";

		public static final String CODE_BSCT = "BSCT";

		public static final String CODE_BLND = "BLND";

		public static final String CODE_DEAF = "DEAF";

		public static final String CODE_LANG = "LANG";

		public static final String CODE_MAAS = "MAAS";

		public static final String CODE_WCHP = "WCHP";

		public static final String CODE_INAD = "INAD";

		public static final String CODE_VIP = "VIP";

	}

	public static final String _VIP_CASE = "1VIP";

	public static interface EMRECORD_REMARK_DTO {

		public static final String INFANT_CODE1 = "INFT";

		public static final String INFANT_CODE2 = "INF";

		public static final String CHILD = "CHD";

	}
}
