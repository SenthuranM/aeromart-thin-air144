package com.isa.thinair.messagepasser.api.service;

import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;

public interface PFSXmlBD {
	public static final String SERVICE_NAME = "PFSXmlService";

	/**
	 * Process PFS xml files
	 * 
	 * @param day
	 *            TODO
	 */
	public void processPFSXml(Date day) throws ModuleException;

}
