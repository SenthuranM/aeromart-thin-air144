package com.isa.thinair.messagepasser.api.dto.etl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import com.isa.thinair.messagepasser.api.utils.ParserConstants;

/**
 * Holds the ETL atomic record specific data transfer information
 * 
 * @author Ishan
 * @since 1.0
 */
public class ETLRecordDTO {
	/** Hold the tour Id * */
	// private String tourId;

	/** Holds the passenger number */
	private String paxNumber;

	/** Holds the ticket number will contain .L/10257445 */
	// private String ticketnumber;

	/** Holds the inbound Details .I/G9608Y03ALY */
	private String inboundDetails;

	/** Holds the outbound Details .O/G9608Y03ALY */
	private String outboundDetails;

	/** Holds a collection of EMRecordDTO */
	private Collection<ETLPAXRecordDTO> records;

	/** Hold the collection of EMRecordRemarkDTO */
	/** NOTE: ONLY FOR THE PARSER * */
	/** SEE EMRecordDTO: It has its relavent remarks info */
	private Collection<ETLRecordRemarkDTO> remarks;

	/**
	 * Holds the error Description
	 */
	private String errorDescription;

	/** Holds the destination airport, cccode, totnumberofpax */
	private String desAirportNumPaxCCCode;

	/** Holds the Cabin class code of the flight **/
	private String ccCode;

	/** Hold the Number of passengers Flown **/
	private String totNumberOfPax;

	/** Hold the destination airport **/
	private String destinationAirport;

	private String logicalCabinClassCode;

	private String bookingClassCode;

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * Add Records DTO
	 * 
	 * @param recordDTO
	 */
	public void addRecordDTO(ETLPAXRecordDTO recordDTO) {
		// If the first name is empty
		// This can be for a Goshore or a Noshore reservation
		if (recordDTO.getFirstNameWithTitle() == null) {
			this.addRecords(recordDTO);
		} else {
			StringTokenizer stringTokenizer = new StringTokenizer(recordDTO.getFirstNameWithTitle(), " ", false);

			if (stringTokenizer.countTokens() != 1) {
				int i = 1;
				while (stringTokenizer.hasMoreElements()) {
					String firstName = (String) stringTokenizer.nextElement();

					if (i == 1) {
						recordDTO.setFirstNameWithTitle(firstName);
						this.addRecords(recordDTO);
					} else {
						ETLPAXRecordDTO copyRecordDTO = (ETLPAXRecordDTO) recordDTO.clone();
						copyRecordDTO.setFirstNameWithTitle(firstName);
						this.addRecords(copyRecordDTO);
					}

					i++;
				}
			} else {
				this.addRecords(recordDTO);
			}
		}
	}

	/**
	 * Add Remarks
	 * 
	 * @param recordDTO
	 */
	public void addRemarkRecord(ETLRecordRemarkDTO remarkDTO) {
		if (this.getRemarks() == null) {
			this.setRemarks(new ArrayList<ETLRecordRemarkDTO>());
		}
		String ssrCode = remarkDTO.getSsrCode();
		if (okToADD(ssrCode)) {
			this.getRemarks().add(remarkDTO);
		}
	}

	private boolean okToADD(String ssrCode) {

		if (ssrCode.equals(ParserConstants.SSR_CODES.CODE_OKOB) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_WCHR)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_BSCT) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_BLND)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_DEAF) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_LANG)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_MAAS) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_WCHP)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_INAD) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_VIP)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_TKNA)
				|| ssrCode.equals(ParserConstants.EMRECORD_REMARK_DTO.INFANT_CODE1)
				|| ssrCode.equals(ParserConstants.EMRECORD_REMARK_DTO.INFANT_CODE2)) {
			return true;

		} else {

			if (ssrCode.equals(ParserConstants._VIP_CASE)) {

				return true;
			}

			return false;
		}

	}

	/**
	 * Add Records
	 * 
	 * @param recordDTO
	 */
	private void addRecords(ETLPAXRecordDTO recordDTO) {
		if (this.getRecords() == null) {
			this.setRecords(new ArrayList<ETLPAXRecordDTO>());
		}

		this.getRecords().add(recordDTO);
	}

	public String getInboundDetails() {
		if (this.inboundDetails != null) {
			if (this.inboundDetails.startsWith(".I/")) {
				this.inboundDetails = this.inboundDetails.substring(3);
			}
		}
		return inboundDetails;
	}

	public void setInboundDetails(String inboundDetails) {
		this.inboundDetails = inboundDetails;
	}

	public String getOutboundDetails() {
		if (this.outboundDetails != null) {
			if (this.outboundDetails.startsWith(".O/")) {
				this.outboundDetails = this.outboundDetails.substring(3);
			}
		}
		return outboundDetails;

	}

	public void setOutboundDetails(String outboundDetails) {
		this.outboundDetails = outboundDetails;
	}

	/**
	 * @return Returns the records.
	 */
	public Collection<ETLPAXRecordDTO> getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            The records to set.
	 */
	public void setRecords(Collection<ETLPAXRecordDTO> records) {
		this.records = records;
	}

	/**
	 * @return Returns the paxNumber.
	 */
	public String getPaxNumber() {
		return paxNumber;
	}

	/**
	 * @param paxNumber
	 *            The paxNumber to set.
	 */
	public void setPaxNumber(String paxNumber) {
		this.paxNumber = paxNumber;
	}

	/**
	 * @return Returns the errorDescription.
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return Returns the remarks.
	 */
	public Collection<ETLRecordRemarkDTO> getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(Collection<ETLRecordRemarkDTO> remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the desAirportNumPaxCCCode
	 */
	public String getDesAirportNumPaxCCCode() {
		return desAirportNumPaxCCCode;
	}

	/**
	 * @param desAirportNumPaxCCCode
	 *            the desAirportNumPaxCCCode to set
	 */
	public void setDesAirportNumPaxCCCode(String desAirportNumPaxCCCode) {
		this.desAirportNumPaxCCCode = desAirportNumPaxCCCode;
		this.destinationAirport = desAirportNumPaxCCCode.substring(1, 4);
		StringBuffer paxCount = new StringBuffer("");

		for (int i = 0; i < desAirportNumPaxCCCode.length(); i++) {
			try {
				paxCount.append(String.valueOf(Integer.parseInt(String.valueOf(desAirportNumPaxCCCode.charAt(4 + i)))));

			} catch (NumberFormatException e) {
				this.ccCode = desAirportNumPaxCCCode.substring(4 + paxCount.length());
				this.totNumberOfPax = String.valueOf(paxCount);
				break;
			}
		}

	}

	/**
	 * @return the ccCode
	 */
	public String getCcCode() {
		return ccCode;
	}

	/**
	 * @return the totNumberOfPax
	 */
	public String getTotNumberOfPax() {
		return totNumberOfPax;
	}

	/**
	 * @return the destinationAirport
	 */
	public String getDestinationAirport() {
		return destinationAirport;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

}
