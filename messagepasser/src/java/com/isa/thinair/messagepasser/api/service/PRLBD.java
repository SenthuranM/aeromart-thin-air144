package com.isa.thinair.messagepasser.api.service;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;
import com.isa.thinair.platform.api.ServiceResponce;

import java.util.Collection;
import java.util.List;

/**
 * @author Ishan
 */
public interface PRLBD {

	public static final String SERVICE_NAME = "PRLService";

	/**
	 * Reconcile PRL Reservations
	 * 
	 * @param prlId
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reconcilePRLReservations(int prlId) throws ModuleException;

	/**
	 * Prcoess prl reconcilation
	 */
	public ServiceResponce processPRLMessage() throws ModuleException;

	/**
	 * Returns PRL entry paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	public Page getPagedPRLData(List criteria, int startIndex, int noRecs, List orderByFieldList)
			throws ModuleException;

	/**
	 * Return PRL entries
	 * 
	 * @param prlId
	 * @return
	 * @throws ModuleException
	 */

	public Collection<PRLPaxEntry> getPRLParseEntries(int prlId) throws ModuleException;

	/**
	 * Return Passenger final sales record
	 * 
	 * @param prlID
	 * @return
	 * @throws ModuleException
	 */
	public PRL getPRL(int prlID) throws ModuleException;

	public int getPRLParseEntryCount(int prlId, String processedStatus) throws ModuleException;

	/**
	 * Delete Prl Entry
	 *
	 * @param prl
	 * @throws ModuleException
	 */
	public void deletePrlEntry(PRL prl) throws ModuleException;

	/**
	 * Delete the PRL Pax entry
	 * @param prl
	 * @param prlPaxId
	 * @throws ModuleException
	 */
	public void deletePRLParseEntry(PRL prl, int prlPaxId) throws ModuleException;

	/**
	 * Save PRL Parse Entry
	 * @param prlParsed
	 * @throws ModuleException
	 */
	public void savePRLParseEntry(PRLPaxEntry prlParsed) throws ModuleException;



}
