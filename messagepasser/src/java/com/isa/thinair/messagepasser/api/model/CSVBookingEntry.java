/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.messagepasser.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of PNL parsed information
 * 
 * @author Byorn
 * @since 1.0
 * @hibernate.class table = "t_csv_booking_temp"
 */
public class CSVBookingEntry extends Persistent {

	private static final long serialVersionUID = -546317974338360390L;

	private Integer paxId;
	private String pnr;
	private String accelAeroPnr;
	private String eTicketNo;
	private String contactPerson;
	private String mobileNo;
	private String landPhoneNo;
	private String city;
	private String country;
	private String email;
	private Date bookedDateTime;
	private String bookedStaff;
	private String bookingAgent;
	private String bookingChannel; // web/agent
	private String title; // Mr/Mrs
	private String firstName;
	private String lastName;
	private String paxType; // AD/CH/INF
	private String nationality;
	private Date dateOfBirth;
	private Integer parentPaxId; // for infants
	private String flightNo;
	private String segmentCode;
	private Date departureDateTimeZulu;
	private Date arrivalDateTimeZulu;
	private Date segmentCreatedDateTimeZulu;
	private String segmentStatus; // Confirmed/Cancelled/Onhold
	private Date pnrReleaseDateTimeZulu; // for on hold booking only
	private BigDecimal fareAmount;
	private BigDecimal taxAmount;
	private BigDecimal surCharges;
	private String bookingClass;
	private String fareRule;
	private String originIp;
	private String processedStatus;
	private String errorDescription;
	private String description;
	private String depDateTimeStr;

	/**
	 * @return Returns the paxId.
	 * @hibernate.id column = "PAXID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_XA_PNL"
	 */
	public Integer getPaxId() {
		return paxId;
	}

	public void setPaxId(Integer paxId) {
		this.paxId = paxId;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "ETICKETNO"
	 */
	public String geteTicketNo() {
		return eTicketNo;
	}

	public void seteTicketNo(String eTicketNo) {
		this.eTicketNo = eTicketNo;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "CONTACTPERSON"
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "MOBILENO"
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "LANDPHONENO"
	 */
	public String getLandPhoneNo() {
		return landPhoneNo;
	}

	public void setLandPhoneNo(String landPhoneNo) {
		this.landPhoneNo = landPhoneNo;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "CITY"
	 */
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "COUNTRY"
	 */
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "EMAIL"
	 */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "BOOKEDDATETIME"
	 */
	public Date getBookedDateTime() {
		return bookedDateTime;
	}

	public void setBookedDateTime(Date bookedDateTime) {
		this.bookedDateTime = bookedDateTime;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "BOOKEDSTAFF"
	 */
	public String getBookedStaff() {
		return bookedStaff;
	}

	public void setBookedStaff(String bookedStaff) {
		this.bookedStaff = bookedStaff;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "BOOKINGAGENT"
	 */
	public String getBookingAgent() {
		return bookingAgent;
	}

	public void setBookingAgent(String bookingAgent) {
		this.bookingAgent = bookingAgent;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "BOOKINGCHANNEL"
	 */
	public String getBookingChannel() {
		return bookingChannel;
	}

	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "FIRSTNAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "LASTNAME"
	 */
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "PAXTYPE"
	 */
	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "NATIONALITY"
	 */
	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "DATEOFBIRTH"
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "PARENTPAXID"
	 */
	public Integer getParentPaxId() {
		return parentPaxId;
	}

	public void setParentPaxId(Integer parentPaxId) {
		this.parentPaxId = parentPaxId;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "FLIGHTNO"
	 */
	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "DEPARTUREDATETIMEZULU"
	 */
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "ARRIVALDATETIMEZULU"
	 */
	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "SEGMENTCREATEDDATETIMEZULU"
	 */
	public Date getSegmentCreatedDateTimeZulu() {
		return segmentCreatedDateTimeZulu;
	}

	public void setSegmentCreatedDateTimeZulu(Date segmentCreatedDateTimeZulu) {
		this.segmentCreatedDateTimeZulu = segmentCreatedDateTimeZulu;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "SEGMENTSTATUS"
	 */
	public String getSegmentStatus() {
		return segmentStatus;
	}

	public void setSegmentStatus(String segmentStatus) {
		this.segmentStatus = segmentStatus;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "PNRRELEASEDATETIMEZULU"
	 */
	public Date getPnrReleaseDateTimeZulu() {
		return pnrReleaseDateTimeZulu;
	}

	public void setPnrReleaseDateTimeZulu(Date pnrReleaseDateTimeZulu) {
		this.pnrReleaseDateTimeZulu = pnrReleaseDateTimeZulu;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "FAREAMOUNT"
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "TAXAMOUNT"
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "SURCHARGES"
	 */
	public BigDecimal getSurCharges() {
		return surCharges;
	}

	public void setSurCharges(BigDecimal surCharges) {
		this.surCharges = surCharges;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "BOOKINGCLASS"
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "FARERULE"
	 */
	public String getFareRule() {
		return fareRule;
	}

	public void setFareRule(String fareRule) {
		this.fareRule = fareRule;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "ORIGINIP"
	 */
	public String getOriginIp() {
		return originIp;
	}

	public void setOriginIp(String originIp) {
		this.originIp = originIp;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "SEGMENTCODE"
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "PROCESSSTATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "AA_PNR"
	 */
	public String getAccelAeroPnr() {
		return accelAeroPnr;
	}

	public void setAccelAeroPnr(String accelAeroPnr) {
		this.accelAeroPnr = accelAeroPnr;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "DM_DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "MA_DATE"
	 */
	public String getDepDateTimeStr() {
		return depDateTimeStr;
	}

	public void setDepDateTimeStr(String depDateTimeStr) {
		this.depDateTimeStr = depDateTimeStr;
	}

}
