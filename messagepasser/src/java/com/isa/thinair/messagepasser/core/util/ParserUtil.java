package com.isa.thinair.messagepasser.core.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPFile;

import com.aerospike.client.lua.LuaAerospikeLib.index;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PfsStatus;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.dto.common.MessageConfigDTO;
import com.isa.thinair.messagepasser.api.dto.common.MessageConfigDTO.MAIL_CLIENT;
import com.isa.thinair.messagepasser.api.dto.emirates.EMRecordDTO;
import com.isa.thinair.messagepasser.api.dto.emirates.EMRecordRemarkDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLRecordDTO;
import com.isa.thinair.messagepasser.api.dto.ssim.SSIMRecordDTO;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.MessageTypes;
import com.isa.thinair.messagepasser.core.bl.EmailRetriever;
import com.isa.thinair.messagepasser.core.bl.ETLPRLFileNamer;
import com.isa.thinair.messagepasser.core.bl.FileNamer;
import com.isa.thinair.messagepasser.core.bl.Pop3Client;
import com.isa.thinair.messagepasser.core.bl.etl.ETLDTOParser;
import com.isa.thinair.messagepasser.core.bl.prl.PRLDTOParser;
import com.isa.thinair.messagepasser.core.bl.ssim.SSIMParser;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.sita.api.AirlineType;
import com.sita.api.AirportType;
import com.sita.api.FlightType;
import com.sita.api.PassengerType;
import com.sita.api.PaxnameType;
import com.sita.api.TicketType;

public class ParserUtil {

	private static Log log = LogFactory.getLog(ParserUtil.class);

	/** Holds the pnl file name date format */
	public static final String FILE_NAME_DATE_FORMAT = "ddMMyyyyHHmmss";

	/** Holds the pnl file extension */
	public static final String FILE_EXTENSION = ".txt";

	/** Holds the pfs xml dept date/time format */
	private static final String PFS_XML_DEPT_DATE_TIME_FORMAT = "ddMMMyykkmm";
	private static final String PFS_FTP_FOLDER_DATE_FORMAT = "MMdd";

	private static final SimpleDateFormat pfsDateTimeFormatter = new SimpleDateFormat(PFS_XML_DEPT_DATE_TIME_FORMAT);
	private static final SimpleDateFormat pfsFtpFolderFormat = new SimpleDateFormat(PFS_FTP_FOLDER_DATE_FORMAT);
	private static final SimpleDateFormat sitaFileDateFormat = new SimpleDateFormat("yyyyMMddkkmmss");

	/**
	 * Download messages from mail server
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static void downloadMessagesFromMailServer(MessageConfigDTO msgCofigDTO, String messageType, FileNamer fileNamer)
			throws ModuleException {

		String processPath = msgCofigDTO.getProcessPath();
		String defaultMailServer = msgCofigDTO.getPopMailServer();
		String username = msgCofigDTO.getMailServerUsername();
		String password = msgCofigDTO.getMailServerPassword();
		boolean deleteStatus = msgCofigDTO.isDeleteFromMailServer();
		boolean isFailedLoginToMailServer = false;
		String mailServerPort = msgCofigDTO.getPopMailServerPort();

		String ttyMailServer = msgCofigDTO.getTtyMailServer();
		String ttyUserName = msgCofigDTO.getTtyMailServerUserName();
		String ttyPassword = msgCofigDTO.getTtyMailServerPassword();
		boolean isDefinedTTYMailSrever = isDefinedTTYMailSrever(ttyMailServer, ttyUserName, ttyPassword);

		// This is configured in config file
		if (msgCofigDTO.isFetchFromMailServer()) {
			try {
				if (!defaultMailServer.isEmpty() && !username.isEmpty() && !password.isEmpty()) {
					Pop3Client.getMessages(processPath, defaultMailServer, username, password, messageType, deleteStatus,
							fileNamer);
				}
			} catch (ModuleException ex) {
				isFailedLoginToMailServer = true;
				log.error(" ((o)) ModuleException::downloadETLMessagesFromMailServer ", ex);
				if (!isDefinedTTYMailSrever) {
					throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
				}
			} catch (CommonsDataAccessException cdaex) {
				isFailedLoginToMailServer = true;
				log.error(" ((o)) CommonsDataAccessException::downloadETLMessagesFromMailServer ", cdaex);
				if (!isDefinedTTYMailSrever) {
					throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
				}
			}
			try {
				if (isDefinedTTYMailSrever) {
					if (MAIL_CLIENT.JAVAX_MAIL == msgCofigDTO.getPreferedmailClient()) {
						EmailRetriever.getMessagesAndAttachments(processPath, ttyMailServer, defaultMailServer, ttyUserName,
								ttyPassword, deleteStatus, messageType);
					} else {
						Pop3Client.getMessages(processPath, ttyMailServer, ttyUserName, ttyPassword, messageType, deleteStatus,
								fileNamer);
					}
				}
			} catch (ModuleException ex) {
				log.error(" ((o)) ModuleException::downloadETLMessagesFromMailServer ", ex);
				if (isFailedLoginToMailServer) {
					throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
				}
			} catch (CommonsDataAccessException cdaex) {
				log.error(" ((o)) CommonsDataAccessException::downloadETLMessagesFromMailServer ", cdaex);
				if (isFailedLoginToMailServer) {
					throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
				}
			}

		} else {
			log.info(" Fetch ETL From MailServer IS SET TO FALSE THEREFORE NOT FETCHING ANY EMAILS ");

		}

	}

	/**
	 * Returns validated documents
	 * 
	 * @return
	 */
	public static Collection<String> getValidatedDocuments(String processPath, String errorPath, String partialDownloadPath,
			String messageType) {

		// Validate the messages

		if (messageType.equals("PRL")) {
			validatePRLMessages(processPath, errorPath, partialDownloadPath, messageType);
			// Return the message names
			return getMessageNames(processPath);
		}

		validateMessages(processPath, errorPath, partialDownloadPath, messageType);

		// Return the message names
		return getMessageNames(processPath);
	}

	/**
	 * Parse document
	 * 
	 * @param strMsgName
	 * @return
	 */
	public static Integer parseDocument(Collection<String> msgNamesThatMayHavePartTwoThree, String strMsgName,
			MessageTypes messageType) throws ModuleException, ParseException {
		Integer id = null;

		try {
			switch (messageType) {
			case ETL:
				id = new Integer(ETLDTOParser.insertETLToDatabase(msgNamesThatMayHavePartTwoThree, strMsgName));
				break;
			case PRL:
				id = new Integer(PRLDTOParser.insertPRLToDatabase(msgNamesThatMayHavePartTwoThree, strMsgName));
				break;
			default:
				log.error("################ Invalid messageType ");
			}
		} catch (ModuleException me) {
			// Error documents will there as it is...
			// So later they can correct it and fix it
			log.error(messageType + " ################ Error on the document named " + strMsgName + " will remain as it is... ");
			log.error(messageType + " ################ Error on insert to database ", me);
			throw me;
		} catch (CommonsDataAccessException cdaex) {
			log.error(messageType + " ################ Error on document named " + strMsgName + " will remain as it is... ");
			log.error(messageType + " ################ Error on insert to database ", cdaex);
			throw cdaex;
		}

		return id;
	}

	/**
	 * This method only check if the document has the Main ETL Tag. Returns validated ETL documents
	 * 
	 * @return
	 */
	// private static void validateMessages(String processPath, String errorPath, String messageType) {
	//
	// Collection errorFileNames = new ArrayList();
	// String strHolder;
	// BufferedReader buffer;
	// boolean isETLFormatCompliant;
	// boolean isNextValueUniqueKey;
	// int partNumber = -1;
	// String uniqueKey = null;
	//
	// File directory = new File(processPath);
	// File[] files = directory.listFiles();
	//
	// // Validating the ETL documents
	// if (files != null) {
	// for (int i = 0; i < files.length; i++) {
	// try {
	// buffer = new BufferedReader(new FileReader(files[i]));
	// isETLFormatCompliant = false;
	// isNextValueUniqueKey = false;
	//
	// while ((strHolder = buffer.readLine()) != null) {
	//
	// // Checking for Message text
	// if (strHolder.matches(messageType)) {
	// isETLFormatCompliant = true;
	// isNextValueUniqueKey = true;
	//
	// // Checking for flight number, departure date,
	// // departure station and part number
	// } else if (isNextValueUniqueKey && strHolder.length() > 0) {
	// strHolder = BeanUtils.nullHandler(strHolder);
	// uniqueKey = strHolder.substring(0, strHolder.indexOf("PART"));
	// uniqueKey = BeanUtils.nullHandler(uniqueKey);
	// partNumber = Integer.parseInt(strHolder.substring(strHolder.indexOf("PART") + 4,
	// strHolder.length()));
	// isNextValueUniqueKey = false;
	// }
	//
	// }
	//
	// // If any non ETL format compliant document exist
	// if (isETLFormatCompliant == false) {
	// errorFileNames.add(files[i].getName());
	// }
	// } catch (Exception e) {
	// // Purposly catching the exception to continue with other
	// // documents
	// log.error(" ERROR VALIDATING THE " + messageType + " NAMED : " + files[i].getName() + " "
	// + e.getMessage());
	// }
	// }
	// }
	//
	// // Move the error file to another directory
	// moveErrorFiles(errorFileNames, files, errorPath);
	//
	// }

	/**
	 * Validate the messages
	 * 
	 * @param etlProcessPath
	 * @param etlErrorPath
	 */
	public static Map<String, String> validateMessages(String etlProcessPath, String etlErrorPath, String partialDownloadPath,
			String messageType) {
		Map<String, Map<Integer, String>> uniqueKeyAndEtlPartsMap = new HashMap<String, Map<Integer, String>>();
		Collection<String> errorFileNames = new ArrayList<String>();
		Map<Integer, String> partIdAndEtlDocs;
		String strHolder;
		BufferedReader buffer;
		boolean isEtlFormatCompliant;
		boolean isNextValueUniqueKey;
		boolean isMultipleEtlExist;
		int partNumber = -1;
		String uniqueKey = null;
		StringBuffer text = null;
		Map<String, String> contents = new HashMap<String, String>();
		Map<String, Integer> etlPartCount = new HashMap<String, Integer>();
		List<String> fileNameList = new ArrayList<String>();
		List<String> completeFileList = new ArrayList<String>();

		// Move partially downloaded files during the previous scheduler to process directory
		File pdDirectory = new File(partialDownloadPath);
		moveAllFiles(pdDirectory.listFiles(), etlProcessPath);
		log.info("Files in " + partialDownloadPath + "(" + pdDirectory.listFiles().length + ") : " + getFileNames(pdDirectory));

		File directory = new File(etlProcessPath);
		File[] files = directory.listFiles();
		log.info("Files in " + etlProcessPath + "(" + directory.listFiles().length + ") : " + getFileNames(directory));

		// Validating the etl documents
		for (int i = 0; i < files.length; i++) {
			try {
				FileReader fileReader = new FileReader(files[i]);
				buffer = new BufferedReader(fileReader);
				isEtlFormatCompliant = false;
				isNextValueUniqueKey = false;
				isMultipleEtlExist = false;
				partNumber = -1;
				uniqueKey = null;
				strHolder = null;
				text = new StringBuffer();

				while ((strHolder = buffer.readLine()) != null) {
					text.append(strHolder + "\n");
					// Checking for ETL text
					if (strHolder.matches(messageType)) {
						isEtlFormatCompliant = true;
						isNextValueUniqueKey = true;

						// Checking for flight number, departure date, departure station and part number
					} else if (isNextValueUniqueKey) {
						// strHolder = "G9668/14AUG KRT PART1"
						strHolder = BeanUtils.nullHandler(strHolder);
						uniqueKey = strHolder.substring(0, strHolder.indexOf("PART")); // "G9668/14AUG KRT "
						uniqueKey = BeanUtils.nullHandler(uniqueKey); // "G9668/14AUG KRT"
						partNumber = Integer.parseInt(strHolder.substring(strHolder.indexOf("PART") + 4, strHolder.length())); // 1
						isNextValueUniqueKey = false;
						log.info(" Validating " + messageType + " file " + files[i].getName() + " " + strHolder);
					}

					// This means that etl is spanning for multiple parts
					if (isMultipleEtlExist == false && uniqueKey != null
							&& (strHolder.indexOf("ENDPART") != -1 || partNumber > 1)) {
						isMultipleEtlExist = true;

						// If unique key already exist
						if (uniqueKeyAndEtlPartsMap.containsKey(uniqueKey)) {
							partIdAndEtlDocs = (Map<Integer, String>) uniqueKeyAndEtlPartsMap.get(uniqueKey);
							partIdAndEtlDocs.put(new Integer(partNumber), files[i].getName());
						} else {
							// Initializing a Tree Map inorder to keep the parts sorted
							partIdAndEtlDocs = new TreeMap<Integer, String>();
							partIdAndEtlDocs.put(new Integer(partNumber), files[i].getName());

							uniqueKeyAndEtlPartsMap.put(uniqueKey, partIdAndEtlDocs);
						}
					}
					if (isMultipleEtlExist == false && strHolder.indexOf("ENDETL") != -1 && partNumber == 1) {
						completeFileList.add(files[i].getName());
					}
					if (strHolder.trim().matches("ENDETL")) {
						etlPartCount.put(uniqueKey, partNumber);
					}
				}

				// If any non etl format compliant document exist
				if (isEtlFormatCompliant == false) {
					errorFileNames.add(files[i].getName());
					// mailErrors(files[i].getName() + "\n" + text.toString(), null, "Erroneous ETL received.");
				} else {
					contents.put(files[i].getName(), text.toString());
				}
				fileNameList.add(files[i].getName());
				buffer.close();
				buffer = null;
				fileReader.close();
				fileReader = null;
			} catch (Exception e) {
				// mailErrors(files[i].getName(), e, "Inside EtlFormatUtils.validateMessages");

				// Purposly catching the exception to continue with other documents
				log.error(" ERROR VALIDATING THE " + messageType + " NAMED : " + files[i].getName() + " " + e.getMessage());
			}
		}

		// Move the error file to another directory
		moveFiles(errorFileNames, files, etlErrorPath, true);

		// Handling the multiple etl documents
		Iterator<String> itUniqueKey = uniqueKeyAndEtlPartsMap.keySet().iterator();
		while (itUniqueKey.hasNext()) {
			uniqueKey = (String) itUniqueKey.next();
			partIdAndEtlDocs = (Map<Integer, String>) uniqueKeyAndEtlPartsMap.get(uniqueKey);
			completeFileList.addAll(partIdAndEtlDocs.values());
			// If Mulitple etl document found
			// Otherwise the part will left over till the complete parts are available
			if (partIdAndEtlDocs.values().size() > 1 || etlPartCount.get(uniqueKey) == null || etlPartCount.get(uniqueKey) > 1) {
				if (etlPartCount.get(uniqueKey) != null && etlPartCount.get(uniqueKey) == partIdAndEtlDocs.values().size()) {
					mergeMultipleEtltoOne(etlProcessPath, uniqueKey, partIdAndEtlDocs.values(), contents);
				} else {
					// Move partially downloaded files to another directory
					moveFiles(partIdAndEtlDocs.values(), files, partialDownloadPath, false);
				}
			} else {
				log.info(" No merge or move to partially downloaded path happened to documents of " + uniqueKey);
			}
		}
		fileNameList.removeAll(completeFileList);
		moveFiles(fileNameList, files, partialDownloadPath, false);
		log.info("Files in " + partialDownloadPath + "(" + pdDirectory.listFiles().length + ") : " + getFileNames(pdDirectory));

		return contents;
	}

	public static Map<String, String> validatePRLMessages(String prlProcessPath, String prlErrorPath, String partialDownloadPath,
			String messageType) {
		Map<String, Map<Integer, String>> uniqueKeyAndPrlPartsMap = new HashMap<String, Map<Integer, String>>();
		Collection<String> errorFileNames = new ArrayList<String>();
		Map<Integer, String> partIdAndPrlDocs;
		String strHolder;
		BufferedReader buffer;
		boolean isPrlFormatCompliant;
		boolean isNextValueUniqueKey;
		boolean isMultiplePrlExist;
		int partNumber = -1;
		String uniqueKey = null;
		StringBuffer text = null;
		Map<String, String> contents = new HashMap<String, String>();
		Map<String, Integer> prlPartCount = new HashMap<String, Integer>();

		// Move partially downloaded files during the previous scheduler to process directory
		File pdDirectory = new File(partialDownloadPath);
		moveAllFiles(pdDirectory.listFiles(), prlProcessPath);

		File directory = new File(prlProcessPath);
		File[] files = directory.listFiles();

		// Validating the prl documents
		for (int i = 0; i < files.length; i++) {
			try {
				FileReader fileReader = new FileReader(files[i]);
				buffer = new BufferedReader(fileReader);
				isPrlFormatCompliant = false;
				isNextValueUniqueKey = false;
				isMultiplePrlExist = false;
				partNumber = -1;
				uniqueKey = null;
				strHolder = null;
				text = new StringBuffer();

				while ((strHolder = buffer.readLine()) != null) {
					text.append(strHolder + "\n");
					// Checking for PRL text
					if (strHolder.matches(messageType)) {
						isPrlFormatCompliant = true;
						isNextValueUniqueKey = true;

						// Checking for flight number, departure date, departure station and part number
					} else if (isNextValueUniqueKey) {
						strHolder = BeanUtils.nullHandler(strHolder);
						uniqueKey = strHolder.substring(0, strHolder.indexOf("PART"));
						uniqueKey = BeanUtils.nullHandler(uniqueKey);
						partNumber = Integer.parseInt(strHolder.substring(strHolder.indexOf("PART") + 4, strHolder.length()));
						isNextValueUniqueKey = false;
					}

					// This means that prl is spanning for multiple parts
					if (isMultiplePrlExist == false && uniqueKey != null
							&& (strHolder.indexOf("ENDPART") != -1 || partNumber > 1)) {
						isMultiplePrlExist = true;

						// If unique key already exist
						if (uniqueKeyAndPrlPartsMap.containsKey(uniqueKey)) {
							partIdAndPrlDocs = (Map<Integer, String>) uniqueKeyAndPrlPartsMap.get(uniqueKey);
							partIdAndPrlDocs.put(new Integer(partNumber), files[i].getName());
						} else {
							// Initializing a Tree Map inorder to keep the parts sorted
							partIdAndPrlDocs = new TreeMap<Integer, String>();
							partIdAndPrlDocs.put(new Integer(partNumber), files[i].getName());

							uniqueKeyAndPrlPartsMap.put(uniqueKey, partIdAndPrlDocs);
						}
					}
					if (strHolder.trim().matches("ENDPRL")) {
						prlPartCount.put(uniqueKey, partNumber);
					}
				}

				// If any non prl format compliant document exist
				if (isPrlFormatCompliant == false) {
					errorFileNames.add(files[i].getName());
					// mailErrors(files[i].getName() + "\n" + text.toString(), null, "Erroneous PRL received.");
				} else {
					contents.put(files[i].getName(), text.toString());
				}
				buffer.close();
				buffer = null;
				fileReader.close();
				fileReader = null;
			} catch (Exception e) {
				// mailErrors(files[i].getName(), e, "Inside PrlFormatUtils.validateMessages");

				// Purposly catching the exception to continue with other documents
				log.error(" ERROR VALIDATING THE " + messageType + " NAMED : " + files[i].getName() + " " + e.getMessage());
			}
		}

		// Move the error file to another directory
		moveFiles(errorFileNames, files, prlErrorPath, true);

		// Handling the multiple prl documents
		Iterator<String> itUniqueKey = uniqueKeyAndPrlPartsMap.keySet().iterator();
		while (itUniqueKey.hasNext()) {
			uniqueKey = (String) itUniqueKey.next();
			partIdAndPrlDocs = (Map<Integer, String>) uniqueKeyAndPrlPartsMap.get(uniqueKey);

			// If Mulitple prl document found
			// Otherwise the part will left over till the complete parts are available
			if (partIdAndPrlDocs.values().size() > 1 || prlPartCount.get(uniqueKey) == null || prlPartCount.get(uniqueKey) > 1) {
				if (prlPartCount.get(uniqueKey) != null && prlPartCount.get(uniqueKey) == partIdAndPrlDocs.values().size()) {
					mergeMultiplePrltoOne(prlProcessPath, uniqueKey, partIdAndPrlDocs.values(), contents);
				} else {
					// Move partially downloaded files to another directory
					moveFiles(partIdAndPrlDocs.values(), files, partialDownloadPath, false);
				}
			}
		}
		return contents;
	}

	/**
	 * Merging multiple etl documents to a one etl document
	 * 
	 * @param etlProcessPath
	 * @param uniqueKey
	 * @param colFileNames
	 */
	private static void mergeMultipleEtltoOne(String etlProcessPath, String uniqueKey, Collection<String> colFileNames,
			Map<String, String> etlContents) {
		StringBuffer holder = new StringBuffer();
		boolean isFirstEntry = true;
		boolean started = false;
		BufferedReader buffer;
		String lineText;
		File file;
		Collection<String> filesToBeDeleted = new ArrayList<String>();
		ETLPRLFileNamer fn = new ETLPRLFileNamer(MessageTypes.ETL);
		String fileName =null;
		

		try {
			//[ETL_G9115_16JAN_MCT_PART3.txt, ETL_G9115_16JAN_MCT_PART2.txt, ETL_G9115_16JAN_MCT_PART1.txt]
			log.info("Merging ETL documents of " + uniqueKey);
			// Build Multiple etl file name
			fileName = fn.getCombinedFileName(colFileNames);
			log.info("Merging ETL documents of " + uniqueKey + " to file " + fileName);
			List<String> sortedFileNames = new ArrayList<String>(colFileNames);
			Collections.sort(sortedFileNames);
			int fileIndex=0;
			for (Iterator<String> itFileName = sortedFileNames.iterator(); itFileName.hasNext();) {
				String fileN = (String) itFileName.next();
				log.info("Processing ETL file for concatenation : FILE => "+fileN);
				fileIndex++;
				filesToBeDeleted.add(fileN);
				file = new File(etlProcessPath, fileN);
				FileReader f = new FileReader(file);
				buffer = new BufferedReader(f);

				while ((lineText = buffer.readLine()) != null) {
					// If it's the first etl document we may need the header information too
					if (isFirstEntry) {
						if (lineText.indexOf("ENDPART") == -1 && !lineText.equals("")) {
							holder.append(lineText + "\n");
						} else if (lineText.indexOf("ENDPART") != -1) {
							break;
						}
						// Handling the other etl documents
					} else {
						// Found the exact etl information
						if (lineText.equals("ETL")) {
							started = true;
						} else if (started) {
							if (lineText.indexOf("ENDPART") == -1 && lineText.indexOf("ENDETL") == -1
									&& lineText.indexOf(uniqueKey) == -1 && !lineText.equals("")) {
								holder.append(lineText + "\n");
							} else if (lineText.indexOf("ENDPART") != -1 || lineText.indexOf("ENDETL") != -1) {
								// append endetl part at the end
								//Some occations ENDETL has been ammended to the middle of the message
								if (lineText.indexOf("ENDETL") != -1) {
									// holder.append(lineText);
									if (sortedFileNames != null && sortedFileNames.size() > 1
											&& fileIndex != sortedFileNames.size()) {
										log.info("Trying to ammend ENDETL as per previour behaviour... ");
									}

								}
								break;
							}
						}
					}

				}
				buffer.close();
				buffer = null;
				f.close();
				f = null;
				isFirstEntry = false;
				started = false;
				
			}
			
			// Ammending ENDETL to the end of the file
			if (holder != null && holder.toString() != null && !holder.toString().isEmpty()) {
				log.info("Append ENDETL at end of the file.");
				holder.append("ENDETL");
			}

			// Just in case checking to see if any holder information exist
			if (holder.length() > 0) {
				File f = new File(etlProcessPath, fileName);
				FileWriter ft = new FileWriter(f);
				BufferedWriter out = new BufferedWriter(ft);
				out.write(holder.toString());
				out.close();
				out = null;
				ft.close();
				ft = null;
				deleteFiles(filesToBeDeleted, etlProcessPath);
				// storing etl multi part message in the map, in order to add in error emails
				etlContents.put(fileName, holder.toString());
			}
		} catch (Exception e) {
			// mailErrors("General Error", e, " Inside EtlFormatUtils.mergeMultipleEtltoOne <BR> Files Are : --><BR>"
			// + PlatformUtiltiies.constructINStringForCharactors(colFileNames) + " <BR> " + " Merged File Name : "
			// + fileName);
			log.info(" ERROR MERGING MULTIPLE ETL DOCUMENTS TO : " + fileName + " of " + uniqueKey);
		}
	}

	private static void mergeMultiplePrltoOne(String prlProcessPath, String uniqueKey, Collection<String> colFileNames,
			Map<String, String> prlContents) {
		StringBuffer holder = new StringBuffer();
		boolean isFirstEntry = true;
		boolean started = false;
		BufferedReader buffer;
		String lineText;
		File file;
		Collection<String> filesToBeDeleted = new ArrayList<String>();
		ETLPRLFileNamer fn = new ETLPRLFileNamer(MessageTypes.PRL);
		// Build Multiple prl file name
		String fileName = fn.getCombinedFileName(colFileNames);

		try {
			List<String> sortedFileNames = new ArrayList<String>(colFileNames);
			Collections.sort(sortedFileNames);
			int fileIndex=0;
			for (Iterator<String> itFileName = sortedFileNames.iterator(); itFileName.hasNext();) {
				String fileN = (String) itFileName.next();
				fileIndex++;
				filesToBeDeleted.add(fileN);
				file = new File(prlProcessPath, fileN);
				FileReader f = new FileReader(file);
				buffer = new BufferedReader(f);

				while ((lineText = buffer.readLine()) != null) {
					// If it's the first prl document we may need the header information too
					if (isFirstEntry) {
						if (lineText.indexOf("ENDPART") == -1 && !lineText.equals("")) {
							holder.append(lineText + "\n");
						} else if (lineText.indexOf("ENDPART") != -1) {
							break;
						}
						// Handling the other prl documents
					} else {
						// Found the exact prl information
						if (lineText.equals("PRL")) {
							started = true;
						} else if (started) {
							if (lineText.indexOf("ENDPART") == -1 && lineText.indexOf("ENDPRL") == -1
									&& lineText.indexOf(uniqueKey) == -1 && !lineText.equals("")) {
								holder.append(lineText + "\n");
							} else if (lineText.indexOf("ENDPART") != -1 || lineText.indexOf("ENDPRL") != -1) {
								// append endprl part at the end
								//Some occations ENDPRL has been amended to the middle of the message
								if (lineText.indexOf("ENDPRL") != -1) {
									if (sortedFileNames != null && sortedFileNames.size() > 1
											&& fileIndex != sortedFileNames.size()) {
										log.debug("Trying to ammend ENDPRL as per previous behaviour... ");
									}
								}
								break;
							}
						}
					}

				}
				buffer.close();
				buffer = null;
				f.close();
				f = null;
				isFirstEntry = false;
				started = false;
			}

			// Amending ENDPRL to the end of the file
			if (holder != null && holder.toString() != null && !holder.toString().isEmpty()) {
				log.debug("Append ENDPRL at end of the file.");
				holder.append("ENDPRL");
			}

			// Just in case checking to see if any holder information exist
			if (holder.length() > 0) {
				File f = new File(prlProcessPath, fileName);
				FileWriter ft = new FileWriter(f);
				BufferedWriter out = new BufferedWriter(ft);
				out.write(holder.toString());
				out.close();
				out = null;
				ft.close();
				ft = null;
				deleteFiles(filesToBeDeleted, prlProcessPath);
				// storing prl multi part message in the map, in order to add in error emails
				prlContents.put(fileName, holder.toString());
			}
		} catch (Exception e) {
			// mailErrors("General Error", e, " Inside PrlFormatUtils.mergeMultiplePrltoOne <BR> Files Are : --><BR>"
			// + PlatformUtiltiies.constructINStringForCharactors(colFileNames) + " <BR> " + " Merged File Name : "
			// + fileName);
			log.error(" ERROR MERGING MULTIPLE PRL DOCUMENTS TO : " + fileName);
		}
	}

	private static void deleteFiles(Collection<String> filesToBeDeleted, String etlProcessPath) {
		Iterator<String> iterator = filesToBeDeleted.iterator();
		while (iterator.hasNext()) {
			String file = (String) iterator.next();
			File f = new File(etlProcessPath, file);
			if (f.delete()) {
				continue;
			} else {
				f.delete();
			}
		}
	}

	/**
	 * Build multiple etl file name
	 * 
	 * @param colFileNames
	 * @return
	 */
	private static String buildMultipleEtlFileName(Collection<String> colFileNames) {
		try {
			String fileName = null;

			for (Iterator<String> itFileNames = colFileNames.iterator(); itFileNames.hasNext();) {
				if (fileName != null) {
					String value = (String) itFileNames.next();

					int lastIndex = value.lastIndexOf("-");
					int lastNumber = Integer.parseInt(value.substring(lastIndex + 1, value.indexOf(FILE_EXTENSION)));

					if (fileName.indexOf(FILE_EXTENSION) == -1) {
						fileName = fileName + "," + lastNumber;
					} else {
						fileName = fileName.substring(0, fileName.indexOf(FILE_EXTENSION)) + "," + lastNumber;
					}
				} else {
					fileName = (String) itFileNames.next();
				}
			}

			if (fileName == null) {
				return "error-" + getFileName(new Date(), 0, MessageTypes.ETL.toString());
			} else {
				return fileName + FILE_EXTENSION;
			}
		} catch (Exception e) {
			// mailErrors("General Error", e, " Inside EtlFormatUtils.buildMultipleEtlFileName<BR>Files Are : --><BR>"
			// + PlatformUtiltiies.constructINStringForCharactors(colFileNames));
		}
		return "error-" + getFileName(new Date(), 0, MessageTypes.ETL.toString());
	}

	private static String buildMultiplePrlFileName(Collection<String> colFileNames) {
		try {
			String fileName = null;

			for (Iterator<String> itFileNames = colFileNames.iterator(); itFileNames.hasNext();) {
				if (fileName != null) {
					String value = (String) itFileNames.next();

					int lastIndex = value.lastIndexOf("-");
					int lastNumber = Integer.parseInt(value.substring(lastIndex + 1, value.indexOf(FILE_EXTENSION)));

					if (fileName.indexOf(FILE_EXTENSION) == -1) {
						fileName = fileName + "," + lastNumber;
					} else {
						fileName = fileName.substring(0, fileName.indexOf(FILE_EXTENSION)) + "," + lastNumber;
					}
				} else {
					fileName = (String) itFileNames.next();
				}
			}

			if (fileName == null) {
				return "error-" + getFileName(new Date(), 0, MessageTypes.PRL.toString());
			} else {
				return fileName + FILE_EXTENSION;
			}
		} catch (Exception e) {
			// mailErrors("General Error", e, " Inside PrlFormatUtils.buildMultiplePrlFileName<BR>Files Are : --><BR>"
			// + PlatformUtiltiies.constructINStringForCharactors(colFileNames));
		}
		return "error-" + getFileName(new Date(), 0, MessageTypes.PRL.toString());
	}

	private static void moveAllFiles(File[] files, String etlPath) {

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				File f = files[i];

				File dirError = new File(etlPath);

				// If directory does not exist create one
				if (!dirError.exists()) {
					dirError.mkdirs();
				}

				File copyFile = new File(dirError, f.getName());
				log.info(" Moving file " + f.getName() + " to " + etlPath);
				// Copy and ignore errors
				copyAndIgnoreErrors(f, copyFile);
				log.info("##### " + copyFile.getName() + " is copying to " + etlPath + " #####");
				// Delete the current file
				f.delete();

			}
		}

	}

	/**
	 * Move the error file names
	 * 
	 * @param errorFileNames
	 * @param files
	 * @param etlErrorPath
	 */
	private static void moveFiles(Collection<String> errorFileNames, File[] files, String etlErrorPath, boolean isAddPrefix) {
		// Moving the etl error documents to a error folder
		if (errorFileNames.size() > 0) {
			for (int i = 0; i < files.length; i++) {
				File f = files[i];

				// If file name exist
				if (errorFileNames.contains(f.getName())) {
					File dirError = new File(etlErrorPath);

					// If directory does not exist create one
					if (!dirError.exists()) {
						dirError.mkdirs();
					}

					File copyFile = new File(dirError, (isAddPrefix ? "NON-" : "") + f.getName());
					log.info(" Moving file " + f.getName() + " to " + etlErrorPath);
					// Copy and ignore errors
					copyAndIgnoreErrors(f, copyFile);

					// Delete the current file
					f.delete();
				}
			}
		}
	}

	/**
	 * Move the error file names
	 * 
	 * @param errorFileNames
	 * @param files
	 * @param pnlErrorPath
	 */
	private static void moveErrorFiles(Collection errorFileNames, File[] files, String errorPath) {
		// Moving the pnl error documents to a error folder
		if (errorFileNames.size() > 0) {
			for (int i = 0; i < files.length; i++) {
				File f = files[i];

				// If file name exist
				if (errorFileNames.contains(f.getName())) {
					File dirErrorPnl = new File(errorPath);

					// If directory does not exist create one
					if (!dirErrorPnl.exists()) {
						dirErrorPnl.mkdirs();
					}

					File copyFile = new File(dirErrorPnl, "NON-" + f.getName());
					// Copy and ignore errors
					copyAndIgnoreErrors(f, copyFile);

					// Delete the current file
					f.delete();
				}
			}
		}
	}

	/**
	 * Copies src file to dst file. If the dst file does not exist, it is created
	 * 
	 * @param src
	 * @param dst
	 */
	public static void copyAndIgnoreErrors(File src, File dst) {
		try {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dst);

			// Transfer bytes from in to out
			byte[] buf = new byte[1024];
			int len;

			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			in.close();
			out.close();

		} catch (Exception e) {
			// This can not happen practically
			log.info("################## UNEXCEPECTED ETL COPYING ERROR OCCURED ", e);
		}
	}

	/*
	 * Return message names
	 * 
	 * @param path
	 * 
	 * @return
	 */
	private static Collection<String> getMessageNames(String path) {
		File directory = new File(path);
		File[] files = directory.listFiles();
		Collection<String> colMsgNames = new ArrayList<String>();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				colMsgNames.add(files[i].getName());
			}
		}
		return colMsgNames;
	}

	/**
	 * The below scenario needs to be handled. (1FERNANDO/ESMIMS/DUDEMR .L/J800076 .R/1INF BABY .R/BSCT HK1 PAX NEED
	 * BABY BASSINET/DUDEMR)
	 * 
	 * @param emPnlrecordDTO
	 * @return
	 * @throws ModuleException
	 */
	public static void prepareRemarks(ETLRecordDTO emPnlrecordDTO) throws ModuleException {

		try {
			boolean onlyOnePax = false;

			Collection records = emPnlrecordDTO.getRecords();
			Collection remarks = emPnlrecordDTO.getRemarks();

			if (remarks == null || remarks.size() == 0) {
				return;
			}

			// throw exception if no pax records
			checkParams(records);

			if (records.size() == 1 && remarks.size() > 0) {

				onlyOnePax = true;
			}

			Iterator iterRecords = records.iterator();

			while (iterRecords.hasNext()) {
				EMRecordDTO recordDTO = (EMRecordDTO) iterRecords.next();
				populateEmRecordDTO(recordDTO, remarks, onlyOnePax);

			}

		} catch (Exception e) {
			emPnlrecordDTO.setErrorDescription("Exception occured inside prepareRemarks");
		}
	}

	/**
	 * 
	 * @param records
	 * @throws ModuleException
	 */
	private static void checkParams(Collection records) throws ModuleException {
		if (records == null || records.size() == 0) {

			throw new ModuleException("messagepasser.etl.dtoNoRec");
		}

	}

	/**
	 * 
	 * @param recordDTO
	 * @param remarks
	 */
	private static void populateEmRecordDTO(EMRecordDTO recordDTO, Collection remarks, boolean onlyOnePax) {

		try {

			String firstNameWithTitle = recordDTO.getFirstNameWithTitle();
			Iterator iterRemarks = remarks.iterator();

			while (iterRemarks.hasNext()) {
				EMRecordRemarkDTO recordRemarkDTO = (EMRecordRemarkDTO) iterRemarks.next();
				String adviceTo = recordRemarkDTO.getAdviceTo();
				boolean isChildRemark = recordRemarkDTO.isChildRelatedDTO();
				if (adviceTo != null)
					adviceTo.trim();

				// if there is a matching advice name
				// or if there is only one pax,
				// or if its a childremark dto
				// then okay to add it to link it to the RecordDTO.
				if (onlyOnePax == true || firstNameWithTitle.equals(adviceTo) || isChildRemark) {

					// if no ssr code set the error
					if (recordRemarkDTO.getSsrCode() == null || recordRemarkDTO.getSsrCode().equals("")) {
						recordDTO.setErrorDescription("Remark didnt have infant or an ssr");
					}

					// if its not a infant remark or a child remark thenn set
					// the ssrcode and text
					if (!recordRemarkDTO.isInfantRelatedDTO() && !recordRemarkDTO.isChildRelatedDTO()) {

						recordDTO.setSsrCode(recordRemarkDTO.getSsrCode());
						recordDTO.setSsrText(recordRemarkDTO.getCleanedUpSSRText());
					}

					// if its an infant remark then get the names
					if (recordRemarkDTO.isInfantRelatedDTO()) {
						recordDTO.setInfantName(recordRemarkDTO.getInfantfirstName());
						recordDTO.setInfantLastName(recordRemarkDTO.getInfantLastName());
					}

					if (recordRemarkDTO.isChildRelatedDTO()) {

					}

				}

				recordDTO.setErrorDescription(recordRemarkDTO.getErrorDescription());
			}
		} catch (Exception e) {
			recordDTO.setErrorDescription("Error Occured in populateEmRecordDTO method");
		}
	}

	/**
	 * Returns the pnl file name
	 * 
	 * @param currentDate
	 * @param message
	 * @return
	 */
	public static String getFileName(Date currentDate, int message, String messageType) {
		return messageType + "-" + BeanUtils.parseDateFormat(currentDate, FILE_NAME_DATE_FORMAT) + "-" + message + FILE_EXTENSION;
	}

	public static void moveAndDeleteAnyRemainingFiles(String processPath, String moveToPath, Collection<String> colMsgName) {
		File dirPnlPath = new File(processPath);
		File[] files = dirPnlPath.listFiles();

		// Moving the new files
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (colMsgName != null && colMsgName.contains(file.getName())) {
				// If file name exist

				File dirErrorPathPnl = new File(moveToPath);

				// If directory does not exist create one
				if (!dirErrorPathPnl.exists()) {
					dirErrorPathPnl.mkdirs();
				}

				File copyFile = new File(dirErrorPathPnl, file.getName());
				log.info("Moving remaining file " + file.getName() + " from " + processPath + " to " + moveToPath);
				copyAndIgnoreErrors(file, copyFile);
				file.delete();
			}
		}
	}

	/**
	 * Move and delete pnl documents
	 * 
	 * @param pnlProcessPath
	 * @param pnlParsedPath
	 */
	public static void moveAndDeleteFiles(String processPath, String parsedPath, String strMsgName) {
		File dirPnlPath = new File(processPath);
		File[] files = dirPnlPath.listFiles();

		// Moving the new files
		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			// If file name exist
			if (strMsgName.equals(file.getName())) {
				File dirParsedPnl = new File(parsedPath);

				// If directory does not exist create one
				if (!dirParsedPnl.exists()) {
					dirParsedPnl.mkdirs();
				}

				File copyFile = new File(dirParsedPnl, file.getName());
				log.info("Moving " + file.getName() + " from " + processPath + " to " + parsedPath + " due to exception");
				copyAndIgnoreErrors(file, copyFile);
				file.delete();
			}
		}
	}

	/**
	 * populate Pfs from xml data
	 */
	public static Pfs getPfs(AirlineType airline, FlightType flight, AirportType city)
			throws ModuleException {
		Pfs pfs = new Pfs();

		// flight number ==> host+flight number
		pfs.setFlightNumber(airline.getSubHost() + flight.getFlightNumber());

		// dept date ==> flight date(ddMMMyy) + city.DepartureTime(kkmm)
		try {
			Date deptDate = pfsDateTimeFormatter.parse(flight.getFlightDate() + city.getDepartureTime());
			pfs.setDepartureDate(deptDate);
		} catch (ParseException e) {
			throw new ModuleException(e, e.getMessage());
		}
		pfs.setFromAirport(city.getCityCode());
		pfs.setDateDownloaded(new Date());
		pfs.setFromAddress("SITA_XML");
		pfs.setPfsContent(null);
		pfs.setProcessedStatus(PfsStatus.PARSED);

		return pfs;
	}

	/**
	 * populate Pfs from xml data
	 */
	public static ETL getEtl(AirlineType airline, FlightType flight, AirportType city)
			throws ModuleException {
		ETL etl = new ETL();

		// flight number ==> host+flight number
		etl.setFlightNumber(airline.getSubHost() + flight.getFlightNumber());

		// dept date ==> flight date(ddMMMyy) + city.DepartureTime(kkmm)
		try {
			Date deptDate = pfsDateTimeFormatter.parse(flight.getFlightDate() + city.getDepartureTime());
			etl.setDepartureDate(deptDate);
		} catch (ParseException e) {
			throw new ModuleException(e.fillInStackTrace(), e.getMessage());
		}
		etl.setFromAirport(city.getCityCode());
		etl.setDateDownloaded(new Date());
		etl.setFromAddress(null);
		etl.setProcessedStatus("U");

		return etl;

	}

	/**
	 * populate PfsPaxEntry from xml data
	 */
	public static PfsPaxEntry getPfsPaxEntry(Pfs pfs, PassengerType passenger) {

		PfsPaxEntry paxEntry = new PfsPaxEntry();

		paxEntry.setReceivedDate(new Date());
		paxEntry.setFlightNumber(pfs.getFlightNumber());
		paxEntry.setFlightDate(pfs.getDepartureDate());
		paxEntry.setDepartureAirport(pfs.getFromAirport());

		String name = passenger.getFirstname();
		String title = ReservationInternalConstants.PassengerTitle.MR;
		String paxType = ReservationInternalConstants.PassengerType.ADULT;
		String lastFName = name;

		if (name.endsWith("MR")) {
			title = ReservationInternalConstants.PassengerTitle.MR;
			lastFName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("MS")) {
			title = ReservationInternalConstants.PassengerTitle.MS;
			lastFName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("CHD")) {
			title = ReservationInternalConstants.PassengerTitle.CHILD;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			lastFName = name.substring(0, name.length() - 3);
		} else if (name.endsWith("MRS")) {
			title = ReservationInternalConstants.PassengerTitle.MRS;
			lastFName = name.substring(0, name.length() - 3);
		} else if (name.endsWith("MISS")) {
			title = ReservationInternalConstants.PassengerTitle.MISS;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			lastFName = name.substring(0, name.length() - 4);
		} else if (name.endsWith("MSTR")) {
			title = ReservationInternalConstants.PassengerTitle.MASTER;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			lastFName = name.substring(0, name.length() - 4);
		}

		paxEntry.setTitle(title);
		paxEntry.setFirstName(lastFName);
		paxEntry.setPaxType(paxType);
		paxEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED);

		return paxEntry;

	}

	/**
	 * populate ETLPaxEntry from xml data
	 */
	public static List<ETLPaxEntry> getEtlPaxEntry(Pfs pfs, PaxnameType paxname) {

		List<ETLPaxEntry> etlPaxEntryList = new ArrayList<ETLPaxEntry>();
		List<PassengerType> passengers = paxname.getPassenger();
		for (PassengerType passenger : passengers) {

			ETLPaxEntry paxEntry = new ETLPaxEntry();

			paxEntry.setReceivedDate(new Date());
			paxEntry.setFlightNumber(pfs.getFlightNumber());
			paxEntry.setFlightDate(pfs.getDepartureDate());
			paxEntry.setDepartureAirport(paxname.getDestination());
			// assume one pnr
			if (paxname.getPNR().size() > 0) {
				paxEntry.setPnr(paxname.getPNR().get(0).getRecordLocator());
			}

			String name = passenger.getFirstname();
			String title = ReservationInternalConstants.PassengerTitle.MR;
			String paxType = ReservationInternalConstants.PassengerType.ADULT;
			String lastFName = name;

			if (name.endsWith("MR")) {
				title = ReservationInternalConstants.PassengerTitle.MR;
				lastFName = name.substring(0, name.length() - 2);
			} else if (name.endsWith("MS")) {
				title = ReservationInternalConstants.PassengerTitle.MS;
				lastFName = name.substring(0, name.length() - 2);
			} else if (name.endsWith("CHD")) {
				title = ReservationInternalConstants.PassengerTitle.CHILD;
				paxType = ReservationInternalConstants.PassengerType.CHILD;
				lastFName = name.substring(0, name.length() - 3);
			}

			paxEntry.setTitle(title);
			paxEntry.setFirstName(lastFName);
			paxEntry.setLastName(paxname.getSurname());
			paxEntry.setPaxType(paxType);
			paxEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.PROCESSED);
			paxEntry.setCabinClassCode("Y");// paxname.getClazz()
			paxEntry.setArrivalAirport(paxname.getDestination());

			List<Object> docaOrDOCOOrDOCS = passenger.getDOCAOrDOCOOrDOCS();

			for (Object obj : docaOrDOCOOrDOCS) {
				if (obj instanceof TicketType) {
					TicketType passengerTkt = (TicketType) obj;
					if (passengerTkt.getTicketNumber() != null) {
						paxEntry.setEticketNumber(passengerTkt.getTicketNumber());
						paxEntry.setCoupNumber(passengerTkt.getCouponNumber().intValue());
					}
				}
			}
			etlPaxEntryList.add(paxEntry);
		}

		return etlPaxEntryList;
	}

	/**
	 * Downloading files from FTP server
	 */
	// TODO Plz refactor to PFS XML specific BL. These are not utils
	public static List<String> downloadPfsXmls(Date day) {
		List<String> fileList = new ArrayList<String>();

		try {
			MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
			FTPUtil ftpUtil = new FTPUtil();
			ftpUtil.setRemoteVerificationEnabled(false);
			String carrierCode = AppSysParamsUtil.getCarrierCode();

			// yesterday's downloaded files
			Calendar today = Calendar.getInstance();
			today.setTime(day);
			Date curDay = today.getTime();
			// one day back
			today.add(Calendar.DAY_OF_MONTH, -1);
			Date oneDayBack = today.getTime();

			if (log.isDebugEnabled()) {
				log.debug("Processing startings for CurrentDate[" + curDay + "] OneDayBack[" + oneDayBack + "] ");
				log.debug("Connecting to Ftp with FTPIP[" + messagePasserConfig.getFtpIP() + "] FTPUser["
						+ messagePasserConfig.getFtpUser() + "] FTPPasswordLength["
						+ BeanUtils.nullHandler(messagePasserConfig.getFtpPassword()).length() + "] ");
			}

			GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
			if (globalConfig.getUseProxy()) {
				log.debug("Using Proxy ");
				System.setProperty("http.proxyHost", globalConfig.getHttpProxy());
				System.setProperty("http.proxyPort", String.valueOf(globalConfig.getHttpPort()));
				// System.getProperties().put( "socksProxyPort", globalConfig.getHttpProxy());
				// System.getProperties().put( "socksProxyHost",String.valueOf(globalConfig.getHttpPort()));
			}

			if (log.isDebugEnabled()) {
				log.debug("http.proxyHost " + System.getProperty("http.proxyHost"));
				log.debug("http.proxyPort " + System.getProperty("http.proxyPort"));
			}

			ftpUtil.connectAndLogin(messagePasserConfig.getFtpIP(), messagePasserConfig.getFtpUser(),
					messagePasserConfig.getFtpPassword());

			if (log.isDebugEnabled()) {
				log.debug("FTP connection established");
			}

			// =======================
			// make file list to download
			List<String> filesToDownload = new ArrayList<String>();
			// get yesterday files
			String ftpFolderYesterday = messagePasserConfig.getFtpFolder() + "/" + pfsFtpFolderFormat.format(oneDayBack);

			if (log.isDebugEnabled()) {
				log.debug("ftpFolderYesterday : " + ftpFolderYesterday);
			}

			FTPFile[] ftpFilesYesterday = ftpUtil.listFiles(ftpFolderYesterday);
			for (FTPFile ftpFile : ftpFilesYesterday) {
				filesToDownload.add(ftpFolderYesterday + "/" + ftpFile.getName());
			}
			// get today files
			String ftpFolderToday = messagePasserConfig.getFtpFolder() + "/" + pfsFtpFolderFormat.format(curDay);

			if (log.isDebugEnabled()) {
				log.debug("ftpFolderToday : " + ftpFolderToday);
			}

			FTPFile[] ftpFilesToday = ftpUtil.listFiles(ftpFolderToday);

			for (FTPFile ftpFile : ftpFilesToday) {
				filesToDownload.add(ftpFolderToday + "/" + ftpFile.getName());
			}

			if (log.isDebugEnabled()) {
				log.debug("About to download [" + filesToDownload.size() + "] number of files");
			}

			for (String ftpFile : filesToDownload) {
				String ftpFileName = ftpFile.split("/")[2];
				String prefix = ftpFileName.split("_")[0];

				boolean hasCarrierCode = false;
				if (prefix.indexOf(carrierCode) > -1) {
					hasCarrierCode = true;
				}

				if (ftpFileName.indexOf(".SNT") > -1 || !hasCarrierCode) {
					// skip
					continue;
				}

				Date fileDate = pfsFileDate(ftpFileName);

				boolean exist = MessagePasserModuleUtils.getReservationBD().checkIfPfsAlreadyExists(fileDate);
				if (!exist) {
					// download file
					String localFile = messagePasserConfig.getPfsLocalPath() + ftpFileName;
					boolean status = ftpUtil.downloadFile(ftpFile, localFile);

					if (log.isDebugEnabled()) {
						if (!status) {
							log.debug("Download failed for FtpFileName[" + ftpFileName + "] ");
						}
					}

					// decrypt Now No decrypt :(

					/*
					 * InputStream in = OpenSSL.decrypt( messagePasserConfig.getCipher(),
					 * messagePasserConfig.getCipherPassword().toCharArray() , new FileInputStream(localFile)); String
					 * decryptFileName = localFile.replaceAll(".enc", ""); FileOutputStream out = new
					 * FileOutputStream(decryptFileName); int data = -1; while ((data = in.read()) >-1 ) {
					 * out.write(data); } in.close(); out.close();
					 * 
					 * //unzip Now No Compression too
					 * 
					 * GZIPInputStream inputStream = new GZIPInputStream(new FileInputStream(decryptFileName)); String
					 * xmlName = decryptFileName.replaceAll(".gz", ""); FileOutputStream outXml = new
					 * FileOutputStream(xmlName); int dataXml = -1; while ((dataXml = inputStream.read()) >-1 ) {
					 * outXml.write(dataXml); } inputStream.close(); outXml.close();
					 */

					// add file name to list
					fileList.add(ftpFileName);
				}
			}
		} catch (Exception e) {
			log.error("PFS File Downloading ERROR", e);
			// send error mail
			sendEmailNotification(e, "XML PFS Downloading ERROR");
		}

		if (log.isDebugEnabled()) {
			log.debug("Completed downloading [" + fileList.size() + "] files");
		}
		return fileList;
	}

	// TODO Plz refactor
	public static void sendEmailNotification(Exception e, String subject) {
		try {
			MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
			Properties props = messagePasserConfig.getPfsFtpErrTo();
			Enumeration enumeration = props.elements();

			while (enumeration.hasMoreElements()) {
				String emailAddress = (String) enumeration.nextElement();

				SimpleEmailDTO emailDTO = new SimpleEmailDTO();
				emailDTO.setToEmailAddress(emailAddress);
				emailDTO.setSubject(subject);
				emailDTO.setContent("Message [" + getErrorMsg(e) + "] \n\n Details [" + getStackTrace(e, 5) + "] ");
				emailDTO.setFromAddress(messagePasserConfig.getPfsFtpErrFrom());

				MessagePasserModuleUtils.getMessagingServiceBD().sendSimpleEmail(emailDTO);
			}
		} catch (Exception ex) {
			log.error("PFS Email Failure notify ERROR", ex);
		}
	}
	
	public static void sendEmailNotification( String subject, String body) {
		try {
			MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
			Properties props = messagePasserConfig.getPfsFtpErrTo();
			Enumeration enumeration = props.elements();

			while (enumeration.hasMoreElements()) {
				String emailAddress = (String) enumeration.nextElement();

				SimpleEmailDTO emailDTO = new SimpleEmailDTO();
				emailDTO.setToEmailAddress(emailAddress);
				emailDTO.setSubject(subject);
				emailDTO.setContent(body);
				emailDTO.setFromAddress(messagePasserConfig.getPfsFtpErrFrom());

				MessagePasserModuleUtils.getMessagingServiceBD().sendSimpleEmail(emailDTO);
			}
		} catch (Exception ex) {
			log.error("PFS Email Failure notify ERROR", ex);
		}
	}
	
	

	// TODO Plz refactor
	private static String getErrorMsg(Exception e) {
		String errorMsg = "";

		if (e != null) {
			if (e instanceof ModuleException) {
				ModuleException me = (ModuleException) e;
				errorMsg = BeanUtils.nullHandler(me.getMessageString());
			} else if (e instanceof CommonsDataAccessException) {
				CommonsDataAccessException me = (CommonsDataAccessException) e;
				errorMsg = BeanUtils.nullHandler(me.getMessageString());
			} else {
				errorMsg = BeanUtils.nullHandler(e.getMessage());
			}
		}

		return errorMsg;
	}

	// TODO Plz refactor
	private static String getStackTrace(Exception ex, int lines) {
		StackTraceElement[] stackTrace = ex.getStackTrace();
		StringBuilder stackBuf = new StringBuilder();
		if (stackTrace != null) {
			for (int i = 0; i < lines; i++) {
				stackBuf.append(stackTrace[i].toString()).append(System.getProperty("line.separator"));
			}
		}
		return stackBuf.toString();
	}

	// TODO Plz refactor
	public static Date pfsFileDate(String fileName) {
		String dateTime = fileName.split("_")[1];
		String dateStr = dateTime.split("\\.")[0];
		Date pfsFileDate = null;
		try {
			pfsFileDate = sitaFileDateFormat.parse(dateStr);
		} catch (ParseException e) {
			log.error("pfsFileDate Date pasing error");
		}

		return pfsFileDate;
	}

	private static boolean isDefinedTTYMailSrever(String ttyMailServer, String ttyUserName, String ttyPassword) {
		if (ttyMailServer != null && ttyUserName != null && ttyPassword != null) {
			if (!ttyMailServer.isEmpty() && !ttyUserName.isEmpty() && !ttyPassword.isEmpty()) {
				return true;
			}
			return false;
		}
		return false;
	}

	public static void getEtlPrlFilesViaSitaTex(List<String> messages, String messageType) throws ModuleException {

		MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
		String processPath = "";
		FileNamer fn = null;
		if (messageType.equals(MessageTypes.ETL.toString())) {
			processPath = messagePasserConfig.getEtlProcessPath();
			fn = new ETLPRLFileNamer(MessageTypes.ETL);
		} else if (messageType.equals(MessageTypes.PRL.toString())) {
			processPath = messagePasserConfig.getPrlProcessPath();
			fn = new ETLPRLFileNamer(MessageTypes.PRL);
		}
		if (!messages.isEmpty()) {
			File messagesDir = new File(processPath);
			if (!messagesDir.exists()) {
				boolean status = messagesDir.mkdirs();
				if (!status) {
					throw new ModuleException("airreservations.sitatex.filesCreationError");
				}
			}
			Date currentDate = new Date();
			FileWriter fileWriter = null;
			BufferedWriter bufferWriter = null;
			File file = null;
			int i = 0;

			for (String message : messages) {
				try {
					file = new File(messagesDir, getFileName(currentDate, i, messageType));
					fileWriter = new FileWriter(file);
					bufferWriter = new BufferedWriter(fileWriter);
					bufferWriter.write(message);
					log.info(" ###SITATEXT########### File Created " + file.getName());
					bufferWriter.close();
					fileWriter.close();
					if (fn != null) {
						fn.rename(file, messagesDir);
					}
				} catch (Exception e) {
					log.error(" ###SITATEXT############ File Creation Error File Name " + file.getName(), e);
				} finally {
					if (bufferWriter != null) {
						try {
							bufferWriter.close();
						} catch (IOException e) {
						}
					}
				}
				i++;
			}
		}

	}

	private static String getFileNames(File pdDirectory) {
		String fileNamesInDirectory = "";
		for (File file : pdDirectory.listFiles()) {
			fileNamesInDirectory += file.getName() + ",";
		}
		return fileNamesInDirectory;
	}

	public static Collection<SSIMRecordDTO> validateAndParseSSIMDocuments(String processPath, String errorPath,
			String partialDownloadPath, String parsedPath, String messageType) throws ModuleException {

		Collection<String> errorFileNames = new ArrayList<String>();
		Collection<String> parsedFileNames = new ArrayList<String>();
		String strHolder;
		BufferedReader buffer;
		StringBuffer text = null;

		// Move partially downloaded files during the previous scheduler to process directory
		File pdDirectory = new File(partialDownloadPath);
		moveAllFiles(pdDirectory.listFiles(), processPath);

		File directory = new File(processPath);
		File[] files = directory.listFiles();
		Collection<SSIMRecordDTO> parsedSSIMDTOColl = new ArrayList<SSIMRecordDTO>();
		// Validating the SSIM documents
		for (int i = 0; i < files.length; i++) {
			try {
				FileReader fileReader = new FileReader(files[i]);
				buffer = new BufferedReader(fileReader);
				strHolder = null;
				text = new StringBuffer();
				boolean isSsimFileBegin = false;
				while ((strHolder = buffer.readLine()) != null) {	
					// To Do: Download email without email header  and remove this
					if(!isSsimFileBegin && !StringUtil.isNullOrEmpty(strHolder.toString())){
						if(strHolder.toString().trim().startsWith("1AIRLINE STANDARD SCHEDULE DATA SET")){
							isSsimFileBegin = true;
						}						
					}
					 
					if(isSsimFileBegin){
						text.append(strHolder + "\n");
					}					
				}
				log.info("VALIDATING SSIM MESSAGE : " + files[i].getName());
				String rawMessage = text.toString();
				SSIMParser ssimParser = new SSIMParser();
				if (ssimParser.validSSIMMessageFormat(rawMessage)) {
					log.info("VALIDATED SSIM MESSAGE : " + files[i].getName());
					parsedFileNames.add(files[i].getName());
					parsedSSIMDTOColl.add(ssimParser.parseSSIMMessage(rawMessage));
					log.info("PARSED SSIM MESSAGE : " + files[i].getName());
				} else {
					log.error("ERROR VALIDATING SSIM File NAMED :" + files[i].getName());
					errorFileNames.add(files[i].getName());
				}

				buffer.close();
				buffer = null;
				fileReader.close();
				fileReader = null;
			} catch (Exception e) {
				// Purposly catching the exception to continue with other documents
				log.error(" ERROR VALIDATING THE " + messageType + " NAMED : " + files[i].getName() + " " + e.getMessage());
			}
		}

		// Move the error file to another directory
		moveFiles(errorFileNames, files, errorPath, true);
		moveAndDeleteAnyRemainingFiles(processPath, parsedPath, parsedFileNames);
		return parsedSSIMDTOColl;
	}

}
