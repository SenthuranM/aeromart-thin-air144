package com.isa.thinair.messagepasser.core.audit;

import java.util.Date;
import java.util.LinkedHashMap;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.ProcStdScheduleAuditBase;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

public class ProcSSIMAudit extends ProcStdScheduleAuditBase {

	public static void doAuditProcSSIM(String userID, Integer inMessegeID, Integer scheduleId, FlightSchedule schedule,
			Operation operation, String responseCode, boolean success) throws ModuleException {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (userID != null) {
			audit.setUserId(userID);
		} else {
			audit.setUserId("");
		}

		audit.setTaskCode(String.valueOf(TasksUtil.PROCESS_SISM));

		contents.put("action", nullHandler(SSMASMSUBMessegeDTO.Action.NEW));
		contents.put("in messege id", nullHandler(inMessegeID));
		contents.put("flight designator", nullHandler(schedule.getFlightNumber()));
		contents.put("start date", nullHandler(formatDate(schedule.getStartDate())));
		contents.put("end date", nullHandler(formatDate(schedule.getStopDate())));
		contents.put("schedule id", nullHandler(scheduleId));

		if (success) {
			contents.put("status", ProcStdScheduleAuditBase.nullHandler(getStatus(Status.SUCCESS, operation)));
		} else {
			contents.put("status", ProcStdScheduleAuditBase.nullHandler(getStatus(Status.FAILD, operation)));
			contents.put("remarks", nullHandler(responseCode));
		}

		GDSServicesModuleUtil.getAuditorBD().audit(audit, contents);

	}

}
