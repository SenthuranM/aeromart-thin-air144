/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.bl;

import com.isa.thinair.commons.core.framework.Command;

/**
 * Macro command definitions for air reservation module
 * 
 * @author Nilindra Fernando
 * @isa.module.command.macro-def
 */
public final class MacroCommandDef {

	private MacroCommandDef() {
	}

	/**
	 * @isa.module.command.macro-command name="reconcilePRLReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="reconcilePRLReservation"
	 */
	private Command reconcilePRLReservationMacro;

}
