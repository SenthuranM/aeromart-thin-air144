package com.isa.thinair.messagepasser.core.bl;

import java.io.File;
import java.util.Collection;

public abstract class FileNamer {

	public FileNamer() {
		super();
	}

	public void rename(File sourceFile, File messagesDir) {
		if (sourceFile.isFile() && sourceFile.exists() && messagesDir.isDirectory()) {
			String targetFileName = getUniqueName(sourceFile);
			if (targetFileName != null) {
				File dest = new File(messagesDir, targetFileName);
				sourceFile.renameTo(dest);
			}
		}
	}

	abstract public String getCombinedFileName(Collection<String> fileNames);

	abstract protected String getUniqueName(File sourceFile);

}